<%
/**********************************************************************************
 * File		 		: GmSetBuildTxn.jsp
 * Desc		 		: Set build transaction
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
 <%@ include file="/common/GmHeader.inc" %>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
 
 <%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib prefix="fmtSetBuildTxn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSetBuildTxn.jsp -->
<fmtSetBuildTxn:setLocale value="<%=strLocale%>"/>
<fmtSetBuildTxn:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>
<bean:define id="strConsignmentId" name="frmSetBuildProcess" property="consignID" type="java.lang.String"> </bean:define>
<bean:define id="strRequestId" name="frmSetBuildProcess" property="requestId" type="java.lang.String"> </bean:define>
<bean:define id="strSetId" name="frmSetBuildProcess" property="setID" type="java.lang.String"> </bean:define>
<bean:define id="rollBackAccessFl" name="frmSetBuildProcess" property="rollBackAccessFl" type="java.lang.String"> </bean:define>
<bean:define id="strSetNm" name="frmSetBuildProcess" property="setName" type="java.lang.String"> </bean:define>
<% 
String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("SET_BUILD_TXN");
String strAccessId=rollBackAccessFl; //GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL"));
String strPrevPartNum = "";
String strPartList = "";
String strToken = ",";
String strPNumWithoutComma = "";
String strCounter = GmCommonClass.parseNull((String)request.getAttribute("HCOUNTER"));
String strPartNumIndex = "";
String strBlkQtyId = "";
int intPartCnt = 0;
int intAllTotQty = 0;

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Build Process Screen </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmSetBuildTxn.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmSetBuildCtrlValidate.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>

<script>
var serveletPath = '<%=strServletPath%>';

</script>

<BODY leftmargin="20" topmargin="10" onload="fnPageLoad();" >
<html:form action="/gmSetBuildProcess.do">
<html:hidden  property="hAction" value="" />
<html:hidden  property="strOpt" />
<html:hidden  property="hinputstring" value="" />
<html:hidden property="hTxnId" value="<%=strConsignmentId%>" />
<html:hidden property="hCancelType" value="VDSCN" />
<html:hidden property="requestStatusFlag" />
<html:hidden  property="daysDiff" />
<html:hidden  property="statusflag" />
<input type="hidden" name="RE_FORWARD" value="gmSetBuildProcess">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" value="SETCONSWRAPPER">
<input type="hidden" name="setID" value="<%=strSetId %>">
<input type="hidden" name="hcounter" value="<%=strCounter%>">
<input type="hidden" name="partMaterialType" id="partMaterialType" value= "">
<input type="hidden" name="setName" value="<%=strSetNm%>">

<table class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="1" height="25" class="RightDashBoardHeader"> <fmtSetBuildTxn:message key="LBL_SETBUILDPROCESS"/> </td>
			<fmtSetBuildTxn:message key="LBL_HELP" var="varHelp"/>
			<td colspan="3" height="25" class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>				
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<tr>
			<td colspan="4">
					<jsp:include page="/operations/requests/GmRequestViewHeader.jsp" >
					<jsp:param name="FORMNAME" value="frmSetBuildProcess" />
					</jsp:include>	
			</td>	
		</tr>						
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
<logic:notEqual name="frmSetBuildProcess" property="requestStatusFlag" value="10">
		<tr>
				<td colspan="4" class="RightTableCaptionRed" valign="top" align="center" HEIGHT="24" width="100%"><BR>
				<html:messages id="consignmentdue" message="true" property="requireDateCntDown">
					<bean:write name="consignmentdue"/>
					<logic:greaterThan name="frmSetBuildProcess" property="daysDiff" value="0">
						<bean:write name="frmSetBuildProcess" property="daysDiff" /> <fmtSetBuildTxn:message key="LBL_DAYS"/>
					</logic:greaterThan>
				</html:messages>
				
					<iframe src="/GmCommonLogServlet?hAction=Load&hID=<%=strConsignmentId%>&hType=1220&hMode=INC" scrolling="yes" align="left" marginheight="2" width="100%" height="200"></iframe><BR>
				</TD>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		
        <tr>
			<td colspan="4" align="center" height="30" class="RightTextRed">&nbsp;
				<html:errors />
			</td>
		</tr>	
				<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<tr height="30">
			<td colspan="4" align="center" class="RightTableCaption"> 
				<fmtSetBuildTxn:message key="LBL_RELOADCONTROL"/> # <html:text property="reloadConsignmentId"  size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
				<a href= javascript:fnCallTransactionSearch();><img src="<%=strImagePath%>/action-detail.gif" style="border: none;" height="14"></a>
						<fmtSetBuildTxn:message key="BTN_LOAD" var="varLoad" />
						<gmjsp:button value="${varLoad}" gmClass="button" onClick="fnLoad();" buttonType="Load"  /> 	
				</td>
				
		</tr>
		<tr>
		<td colspan="4"><!-- To add scan control number section -->
			<jsp:include page="/operations/itemcontrol/GmControlNoInclude.jsp" >
			<jsp:param name="FORMNAME" value="frmSetBuildProcess" />
			</jsp:include>
		</td>
     	</tr>
        <tr>
			<td colspan="4" align="center" height="30">&nbsp;
			<div style="overflow:auto; height:252px;">
			<table cellpadding="0" cellspacing="0" width="100%" border="0" bordercolor="gainsboro" class="its" id="PartnPricing">
			  <thead>
				<TR bgcolor="#EEEEEE" height="24" class="RightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
					<TH class="RightText" width="90" align="center"><fmtSetBuildTxn:message key="LBL_PART1"/>#</TH>
					<TH class="RightText" width="270" >&nbsp;<fmtSetBuildTxn:message key="LBL_PARTDESCRIPTION1"/></TH>
					<TH class="RightText" width="80" align="center"><fmtSetBuildTxn:message key="LBL_SETLISTQTY"/></TH>					
					<TH class="RightText" width="100" align="center"><fmtSetBuildTxn:message key="LBL_IN_SET_QTY"/></TH>
					<TH class="RightText" width="15" align="center"><fmtSetBuildTxn:message key="LBL_SCANNED_QTY"/></TH>
					<TH class="RightText" width="140" align="center">&nbsp;&nbsp;&nbsp;&nbsp;<fmtSetBuildTxn:message key="LBL_QTY"/> &nbsp;&nbsp;&nbsp;	<fmtSetBuildTxn:message key="LBL_CONTROL"/> #</TH>
				</TR>	
			  </thead>
			  <TBODY>
<logic:iterate id="consignDetail" name="frmSetBuildProcess" property="alConsignmentDetail" indexId="counter"  >
<bean:define name="consignDetail" property="PNUM" id="currPNum" type="java.lang.String"></bean:define>
<bean:define name="consignDetail" property="QTY" id="qty" type="java.lang.String"></bean:define>
<logic:notEqual name="consignDetail" property="PNUM" value="<%=strPrevPartNum%>">

<% 
// PMT-34784: Handling Plus Part - Logistics (Replace all the special char and space)

 strPNumWithoutComma = "setQty"+currPNum.replaceAll("[^a-zA-Z0-9]","");
 strBlkQtyId = "bulkQty"+currPNum.replaceAll("[^a-zA-Z0-9]","");
 strPartList = currPNum + strToken + strPartList;
 intPartCnt++;
 intAllTotQty += Integer.parseInt(GmCommonClass.parseZero(qty));
  %>
  				<input type="hidden" id="<%=currPNum%>" name="<%=currPNum%>" value="<bean:write name="counter"/>,"/>
				<TR><TD colspan=8 height=1 bgcolor=#eeeeee></TD></TR>
				<tr id="tr<bean:write name="counter"/>" >
					<td >&nbsp; 
					<logic:notEqual name="consignDetail" property="QTY" value="1">  
						<logic:notEqual name="frmSetBuildProcess" property="verifyFlag" value="1">		
							<a tabindex="-1" class="RightText" href="javascript:fnSplit('<bean:write name="counter"/>','<bean:write name="consignDetail" property="PNUM"/>','<bean:write name="consignDetail" property="QTY"/>');">
						</logic:notEqual> 
					</logic:notEqual>	
					<logic:equal name="consignDetail" property="TAGFL" value="Y">
					<a href="javascript:fnOpenTag('<bean:write name="consignDetail" property="PNUM"/>','<%=strConsignmentId%>' ,'51000');">  <img src=<%=strImagePath%>/tag.jpg height='14' width='12' />  </a>	 
					</logic:equal>		
					<bean:write name="consignDetail" property="PNUM"/> 
					</a>
					</td>
					<td id="partDesc<bean:write name="counter"/>" class="RightText">&nbsp;&nbsp;<bean:write name="consignDetail" property="PDESC"/></td>
					<td align="center" id="setQty<bean:write name="counter"/>" ><bean:write name="consignDetail" property="SETQTY"/>
						<htmlel:hidden property="<%=strPNumWithoutComma%>" value="${consignDetail.SETQTY}"  />
					</td>
</logic:notEqual>					
<logic:equal name="consignDetail" property="PNUM" value="<%=strPrevPartNum%>">
				<tr id="tr<bean:write name="counter"/>" >
					<td colspan="3"> &nbsp; </td>
</logic:equal>					
					<htmlel:hidden  property="partNum${counter}" value="${consignDetail.PNUM}" />
					<input type="hidden"  name="hPartNum${counter}" id="hPartNum${counter}" value="${consignDetail.PNUM}" />
					<input type="hidden" id="hControl${counter}" name="hControl${counter}" value="${consignDetail.CONTROLNUM}">
					<td align="center" id="bulkQty<bean:write name="counter"/>" ><bean:write name="consignDetail" property="QTY"/><htmlel:hidden  property="bulkQty${counter}" value="${consignDetail.QTY}" />
						<input type="hidden" name="<%=strBlkQtyId%>" id="<%=strBlkQtyId%>" value="${consignDetail.QTY}"  />
					</td>
					<td width="15" align="center"><label id ="lblPartScan<%=currPNum %>" ></label> </td>
					<td id="Cell<bean:write name="counter"/>" >&nbsp;
						<htmlel:text  property="Txt_Qty${counter}" styleId="Txt_Qty${counter}" value="${consignDetail.QTY}" maxlength="100" size="3" style="InputArea" onfocus="chgTRBgColor('${counter}','#AACCE8');" styleClass="InputArea" />
						&nbsp;&nbsp;<htmlel:text property="controlNum${counter}" styleId="controlNum${counter}" value="${consignDetail.CONTROLNUM}" maxlength="100" size="10" style="InputArea" onblur="fnBlankTBE('${counter}',this.value);fnFetchAllMessages(this,'${consignDetail.PNUM}','PROCESS', '50910','','','','');" onfocus="fnBlankLine('${counter}',this.value);"/>
					</td>
				</tr>
<%  
	strPrevPartNum = currPNum; 
%>					
</logic:iterate>				
<input type="hidden" name="hPartList" value="<%=strPartList%>"/>
<input type="hidden" id="hCnt" name="hCnt" value="<%=intPartCnt-1%>">
<input type="hidden" id="hTotalParts" name="hTotalParts" value="<%=intAllTotQty%>">
<input type="hidden" id="hNewCnt" name="hNewCnt" value="0">
				<!--<tr><td bgcolor="#eeeeee" height="1" colspan="11"></td></tr>-->
			  </TBODY>
			</table>
			</div>
			</td>
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<logic:equal name="frmSetBuildProcess" property="statusflag" value="1.10">	
		<tr>
			<td align="right" class="RightTableCaption" width="44%"><font color="red">*</font>&nbsp;<fmtSetBuildTxn:message key="LBL_TYPE"/>:&nbsp;</td>	
			<td  class="RightTableCaption" width="250" align="left"><gmjsp:dropdown controlName="cbo_Type" SFFormName="frmSetBuildProcess" SFSeletedValue="cbo_Type"
						SFValue="alTypeList" codeId="CODEID" codeName="CODENM"  defaultValue= "[Choose One]" />
			</td>
		</tr>
		<tr></tr>
		<tr></tr>
		<tr></tr>
		</logic:equal>
		<logic:notEqual name="frmSetBuildProcess" property="verifyFlag" value="1">			
		<tr>	
			<logic:equal name="frmSetBuildProcess" property="displayFlag" value="1">
				<td colspan="4" align="center" class="RightTableCaption"> <fmtSetBuildTxn:message key="LBL_COMPLETE"/> ? :&nbsp;&nbsp; <html:checkbox property="completeFlag" /> </td>	
			</logic:equal>
			<%if(strCountryCode.equals("en")){ %>
				<logic:equal name="frmSetBuildProcess" property="statusflag" value="1.10">
				<td colspan="4" align="center" class="RightTableCaption"> <fmtSetBuildTxn:message key="LBL_VERIFIED"/> ? : &nbsp;&nbsp; <html:checkbox property="verifyFlag" /> </td>				
				</logic:equal>
			<%}else{ %>
				<logic:equal name="frmSetBuildProcess" property="statusflag" value="2">
				<td colspan="4" align="center" class="RightTableCaption"> <fmtSetBuildTxn:message key="LBL_VERIFIED"/> ? : &nbsp;&nbsp; <html:checkbox property="verifyFlag" /> </td>
				</logic:equal>
			<%} %>
		</tr>		
		</logic:notEqual>	
		<tr height="30">
			<td colspan="4" align="center" class="RightTextRed"> 
				<html:messages id="setverified" message="true" property="verifyMessage">
					<bean:write name="setverified"/>
				</html:messages>

			</td>
		</tr>	
</logic:notEqual>	
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<tr>
			<td colspan="4">
					<jsp:include page="/operations/logistics/GmBackorderReport.jsp" >
					<jsp:param name="FORMNAME" value="frmSetBuildProcess" />
					<jsp:param name="hRequestId" value="<%=strRequestId%>" />
					</jsp:include>	
			</td>	
		</tr>
	<tr>
		<td colspan="4">
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page="/common/GmRuleDisplayAjaxInclude.jsp"/>
		</td>
	</tr>
	<logic:notEqual name="frmSetBuildProcess" property="requestStatusFlag" value="10">
		<logic:notEqual name="frmSetBuildProcess" property="verifyFlag" value="1">
			<tr><td colspan="4" class="Line" height="1"></td></tr>
			<tr height="30">
				<td colspan="4" align="center"> 
					<fmtSetBuildTxn:message key="BTN_VOID" var="varVoid"/>
					<fmtSetBuildTxn:message key="BTN_PICSLIP" var="varPickslip"/>
					<fmtSetBuildTxn:message key="BTN_SUBMIT" var="varSubmit"/>
					<gmjsp:button value="   ${varVoid}  " gmClass="button"  onClick="fnVoid();" buttonType="Save" />&nbsp;&nbsp;
					<gmjsp:button value="${varPickslip}" gmClass="button"  onClick="fnPicSlip();" buttonType="Save" />&nbsp;&nbsp;
					<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save" /> 
				</td>
			</tr>
		</logic:notEqual>		
	</logic:notEqual>
	<%if(strAccessId.equals("Y")){ %>
	<logic:equal name="frmSetBuildProcess" property="verifyFlag" value="1"> 
			<tr><td colspan="4" class="Line" height="1"></td></tr>
			<tr height="30">
				<td colspan="4" align="center"> 
					<fmtSetBuildTxn:message key="BTN_ROLLBACK" var="varRollback"/>
					<gmjsp:button value="   ${varRollback}" gmClass="button"  onClick="fnRollback();" buttonType="Save" />&nbsp;&nbsp;
				</td>
			</tr>
	</logic:equal>
	<%} %>
	</table>
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>