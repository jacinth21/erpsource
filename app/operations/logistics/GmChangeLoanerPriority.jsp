<%
	/**********************************************************************************
	 * File		 		: GmChangeLoanerPriority.jsp
	 * Desc		 		: This screen is used to display an Change Priority Details
	 * author			: Shanmugapriya
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<!-- \logistics\GmChangeLoanerPriority.jsp -->
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- Properties file -->

<%@ taglib prefix="fmtSetPrioritization"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtSetPrioritization:setLocale value="<%=strLocale%>" />
<fmtSetPrioritization:setBundle
	basename="properties.labels.operations.logistics.GmChangePriority" />
<%
    String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("CHANGE_PRIORITY");
%>
<HTML>
<HEAD>
<title>Change Priority</title>
<link rel="styleSheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script lang="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmChangeLoanerPriority.js"></script>
	
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnPageLoad()">
<html:form action="/gmSetPrioritization.do?method=fetchChangePriority">
<html:hidden property="strOpt" />
<html:hidden name="frmSetPrioritization" property="currentPriorityid" />
<html:hidden name="frmSetPrioritization" property="setid" />

		<table class="DtTable680" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader"><fmtSetPrioritization:message
						key="LBL_CHANGE_PRIORITY" /></td>
				<td class=RightDashBoardHeader>&nbsp;</td>
				<td align="right" class=RightDashBoardHeader><fmtSetPrioritization:message
						key="IMG_ALT_HELP" var="varHelp"></fmtSetPrioritization:message> <img
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>

			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" align="right" height="30px"><fmtSetPrioritization:message
						key="LBL_CONSIGNMENT_ID" /></td>
				<td>&nbsp;<html:text size="30" name="frmSetPrioritization"
						property="consignmentId" styleClass="InputArea"
						onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" maxlength="20"
						onkeypress="fnCallGo()" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<gmjsp:button buttonType="button" value="Load"
						gmClass="button" onClick="fnLoad();"></gmjsp:button></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine" height="1"></td>
			</tr>
			<tr class="oddshade">
				<td class="RightTableCaption" align="right"><fmtSetPrioritization:message
						key="LBL_SET_NAME" /></td>
				<td colspan="2">&nbsp;
					<bean:write name="frmSetPrioritization" property="setName" />
				</td>
			</tr>
			<tr>
				<td colspan="3" class="LLine" height="1"></td>
			</tr>
			<tr class="evenshade">
				<td class="RightTableCaption" align="right"><fmtSetPrioritization:message
						key="LBL_CURRENT_PRIORITY" /></td>
				<td colspan="2">&nbsp;
					<bean:write name="frmSetPrioritization" property="currentPriority" />
				</td>
			</tr>
			
			<tr>
				<td colspan="3" class="LLine" height="1"></td>
			</tr>
			<tr class="oddshade" height="25">
				<td class="RightTableCaption" align="right"><fmtSetPrioritization:message
						key="LBL_CHOOSE_PRIORITY" /></td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="changepriority"
						SFFormName="frmSetPrioritization" SFSeletedValue="changepriority"
						SFValue="alSetPriority" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" seletedValue="[Choose One]" /></td>
			</tr>
			<tr>
				<td colspan="3"><jsp:include page="/common/GmIncludeLog.jsp">
						<jsp:param name="FORMNAME" value="frmSetPrioritization" />
						<jsp:param name="ALNAME" value="alLogReasons" />
						<jsp:param name="LogMode" value="Edit" />
						<jsp:param name="Mandatory" value="yes" />
					</jsp:include></td>
			</tr>
			<tr>
				<td colspan="3" class="LLine" height="1"></td>
			</tr>
			<tr>
				<td colspan="3" align="center" height="30px"><gmjsp:button
						value="Submit" gmClass="button" onClick="fnSubmit();"
						buttonType="Save" /></td>
			</tr>

			<tr>
			</tr>

		</table>

	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

