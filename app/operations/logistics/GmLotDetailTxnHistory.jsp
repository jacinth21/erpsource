<!-- operations\logistics\GmLotDetailTxnHistory.jsp -->
<%
	/*****************************************************************************************
	  * File		 		: GmLotDetailTxnHistory.jsp
      * Desc		 		: This screen is used to display Transaction history for lot track
      * author			    : Agilan Singaravel
	 *****************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.util.Date"%> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<%@ page import = "java.util.*,java.io.*"%>
<%@ taglib prefix="fmtLotDetailRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtLotDetailRpt:setLocale value="<%=strLocale%>"/>
<fmtLotDetailRpt:setBundle basename="properties.labels.operations.logistics.GmLotDetailReport"/>
<bean:define id="strInvType" name="frmLotDetailReport" property="strInvType" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strPartNumber" name="frmLotDetailReport" property="strPartNumber" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strLotNumber" name="frmLotDetailReport" property="strLotNumber" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strPartLiteral" name="frmLotDetailReport" property="strPartLiteral" scope="request" type="java.lang.String"></bean:define>
<bean:define id="companyId" name="frmLotDetailReport" property="companyId" scope="request" type="java.lang.String"></bean:define>
<bean:define id="fieldSalesNm" name="frmLotDetailReport" property="fieldSalesNm" scope="request" type="java.lang.String"></bean:define>
<bean:define id="strSetId" name="frmLotDetailReport" property="strSetId" scope="request" type="java.lang.String"></bean:define>
<% String strLogisticsJsPath= GmCommonClass.parseNull(GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS")); %>

<HTML>
<HEAD>
<TITLE> Globus Medical: Lot Track Report (Non Tissue)</TITLE>
<link rel="styleSheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmLotDetailPopupReport.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>
<script type="text/javascript">
var gCmpDateFmt = "<%=strGCompDateFmt%>";
var strInvType = "<%=strInvType%>";
var strPartNumber = "<%=strPartNumber%>";
var strLotNumber = "<%=strLotNumber%>";
var strPartLiteral = "<%=strPartLiteral%>";
var strCompanyId = "<%=companyId%>";
var strFieldSalesNm = "<%=fieldSalesNm%>";
var strSetId = "<%=strSetId%>";
</script>

<style type="text/css" media="all">
@import url("styles/screen.css"); 
</style>
<style> div.gridbox_dhx_skyblue table.hdr td { text-align: center; vertical-align: middle !important; }
 </style>
</HEAD>
<body leftmargin="15" topmargin="10" onLoad="fnOnLoadPage(); ">
 <html:form action="/gmLotDetailRpt.do?method=LoadLotTransHistory" > 
 <html:hidden name="frmLotDetailReport"  property="strOpt" value="" /> 
 <table class="DtTable800" border="0" cellspacing="0" cellpadding="0">
					
			<tr>
			<% if(strInvType.equals("4000339")){ %>    <!-- field sales -->
				<td  height="25" class="ShadeRightTableCaption" colspan="6">&nbsp;<fmtLotDetailRpt:message key="LOT_FIELDSALES_HIS"/> </td>
			<%}else if(strInvType.equals("4311")){ %>   <!-- Consignment -->
				<td  height="25" class="ShadeRightTableCaption" colspan="6">&nbsp;<fmtLotDetailRpt:message key="LOT_CONSIGNMENT_HIS"/> </td>
			<%}else{ %> <!-- Loaner -->
				<td  height="25" class="ShadeRightTableCaption" colspan="6">&nbsp;<fmtLotDetailRpt:message key="LOT_LOANER_HIS"/> </td>
			<%} %>
			</tr>
        <tr ><td colspan="6" height="1" bgcolor="#000000"></td></tr>
		 <tr class="evenshade"> 
				<td colspan="6"><div id="dataGridTxnHisInfo" style="width:100%;"></div></td>
		</tr>			
  </table>
 </html:form> 
  <%@ include file="/common/GmFooter.inc" %> 
</BODY>
</HTML>