 <%@page import="com.globus.common.beans.GmCommonClass"%>
<%
/**********************************************************************************
 * File		 		: GmLoanerAckPrint.jsp
 * Desc		 		: This screen is used for the
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %> 
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmJasperReport"%>
<%@ page import="java.util.Date"%>

<%@ taglib prefix="fmtInhouseLoanerAckPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmInhouseLoanerAckPrint.jsp -->

<fmtInhouseLoanerAckPrint:setLocale value="<%=strLocale%>"/>
<fmtInhouseLoanerAckPrint:setBundle basename="properties.labels.operations.logistics.GmInhouseLoanerAckPrint"/>

<%

String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
Date dtLDate = null; 
Date dtEDate = null;
HashMap hmCsg = new HashMap();
HashMap hmDetails = new HashMap();
String strHtmlJasperRpt = "";


hmCsg = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmCONSIGNMENT"));
if (hmCsg == null)
{
	hmCsg =  GmCommonClass.parseNullHashMap((HashMap)session.getAttribute("hmCONSIGNMENT"));
}
 
dtLDate = (Date)hmCsg.get("LDATE");
dtEDate = (Date)hmCsg.get("EDATE"); 
String strLnrDate = GmCommonClass.getStringFromDate(dtLDate,strApplnDateFmt);
String strEndDate = GmCommonClass.getStringFromDate(dtEDate,strApplnDateFmt);
String strCompanyId = GmCommonClass.parseNull((String)hmCsg.get("COMPANYID"));
String strCompanyName = GmCommonClass.parseNull((String)GmCommonClass.getString(strCompanyId+"_COMPNAME"));
String strCompanyAddress = GmCommonClass.parseNull((String)GmCommonClass.getString(strCompanyId+"_COMPADDRESS"));
String strCompanyPhone = GmCommonClass.parseNull((String)GmCommonClass.getString(strCompanyId+"_PH"));
String strCompanyFax = GmCommonClass.parseNull((String)GmCommonClass.getString("INHOUSE_LOANER_FAX"));

String strHideButton = GmCommonClass.parseNull(request.getParameter("hHideButton"));
strCompanyAddress = strCompanyAddress.replaceAll("/","<br>");

hmDetails.put("LDATE",strLnrDate);
hmDetails.put("EDATE",strEndDate);
hmDetails.put("COMPID",strCompanyId);
hmDetails.put("COMPNAME",strCompanyName);
hmDetails.put("COMPADDRESS",strCompanyAddress);
hmDetails.put("COMPPHONE",strCompanyPhone);
hmDetails.put("COMPFAX",strCompanyFax);

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Letter</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screenOus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script type="text/javascript">

function fnPrint()
{
    window.print();
}
var tdinnner = "";
var strObject="";

function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<%if(!strHideButton.equals("YES")){ %>
<table align="center" width="100%">
<tr>
	<td align="right" id="button">
		<fmtInhouseLoanerAckPrint:message key="IMG_ALT_CLICKTOPRINT" var = "varPrint"/>
		<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="${varPrint}" onClick="return fnPrint();" />
		<fmtInhouseLoanerAckPrint:message key="IMG_ALT_CLICKTOCLOSE" var = "varClose"/>
		<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="${varClose}" onClick="window.close();" />
 	</td>
 </tr>
 </table>		
 <%} %>
<div id="jasper" style=" max-height: 500px"> 
		<%
			String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();							
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("/GmInhouseLoanerAckPrint.jasper");
			gmJasperReport.setHmReportParameters(hmDetails);							
			gmJasperReport.setReportDataList(null);				
			gmJasperReport.setBlDisplayImage(true);
			strHtmlJasperRpt = gmJasperReport.getHtmlReport();
		%>
		<%=strHtmlJasperRpt %>				
</div>
</FORM>
</BODY>
</HTML>