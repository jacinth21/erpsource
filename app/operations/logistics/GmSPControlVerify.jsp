 <%
/**********************************************************************************
 * File		 		: GmSPControlVerify.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>

<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtSPControlVerify" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSPControlVerify.jsp -->
<fmtSPControlVerify:setLocale value="<%=strLocale%>"/>
<fmtSPControlVerify:setBundle basename="properties.labels.operations.logistics.GmSPControlVerify"/>


<%
String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.logistics.GmSPControlVerify", strSessCompanyLocale);

	HashMap hmTransRules = (HashMap)request.getAttribute("TRANS_RULES");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");	
	
	log.debug("hmTransRules in jsp "+hmTransRules);
	
	String strRuleType =(String) hmTransRules.get("TRANS_RLTXN");
	String strRuleConsignType =(String) hmTransRules.get("TRANS_ID");
	String strTransStatus =(String) hmTransRules.get("ICE_TXN_STATUS");
	String strRuleAjax =(String) hmTransRules.get("ICE_RULE_AJAX");
	String strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
	String strOpt=GmCommonClass.parseNull((String)hmTransRules.get("STR_OPT"));
	String strSkpLotValidFl = GmCommonClass.parseNull((String)request.getAttribute("strSkpLotValidFl"));

	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strConsignId = (String)request.getAttribute("hConsignId") == null?"":(String)request.getAttribute("hConsignId");
	String strExtWReplenish = (String)request.getAttribute("EXTREPLE") == null?"":(String)request.getAttribute("EXTREPLE");
	String strTransTypeID = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_ID"));
	String strPickLocRule = GmCommonClass.parseNull((String) hmTransRules.get("PICK_LOC"));
	String strAccess = GmCommonClass.parseNull((String)request.getAttribute("ACCESSFLAG"));
	log.debug("GMSPControlVeriy JSP strTransTypeID::"+strTransTypeID);
	
	String strDisable=GmCommonClass.parseNull((String)request.getAttribute("disable"));
	String strHeaderRule = "";
	String strLabelRule = "";
	String strTypeRule = "";
	String strChecked = "";
	String strSetId = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";
	String strType =  "";
	String strPurpose = "";
	String strUserName = "";
	String strIniDate = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strQtyId = "";
	String strConsignType = "";
	String strHeader = "";
	boolean showVerifyBtn = false;
	if (strhAction.equals("EditLoad") || strhAction.equals("SaveControl"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CONTROL_NUMBER"));
	}
	else if (strhAction.equals("Verification"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("BTN_VERIFY"));
	}
		
	String strLabel = "Release for Verification";
	String strRequestorName = "";
	String strStockQty = "";
	String strSetCsgQty = "";
	String strQtyRef = "";
	String strTxnId = "";
	String strdisabled = "";
	String strVStatus = GmCommonClass.parseNull((String)request.getAttribute("VerificationStatus"));
	String strControlNoScan = GmCommonClass.parseNull((String)GmCommonClass.getString("SCAN_CONTROL_NO"));
	String strPrice = "";
	String strTransType = "";
	String strItemQty = "";
	String strStatusFl = "";
	String strShipFl = "";
	String strSetName = "";
	String strSetTypeId = "";
	String strRuleTransType = "";
	String strLoanerStatus="";
	String strGridXmlData = "";
	String strPurposeId ="";
	String strMessage="";
	String strWikiTitle = "";
	String strErrorCnt = "";
	String strErrorDtls = "";
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();
	
	if(strTransType.equals("50157") || strTransType.equals("50161") || strTransType.equals("50152"))
	{
		if (!strAccess.equals("Y"))
		{
			 strdisabled = "true";
		}
	}

	int intPriceSize = 0;

	if (hmReturn != null)
	{
			alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
			strGridXmlData = GmCommonClass.parseNull((String)hmReturn.get("GRIDXMLDATA"));
			strSetId = (String)session.getAttribute("hSetId") == null?"":(String)session.getAttribute("hSetId");
		
			hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");

			strConsignId = GmCommonClass.parseNull((String)hmConsignDetails.get("CONSIGNID"));
			strTxnId = GmCommonClass.parseNull((String)hmConsignDetails.get("CID"));
			strSetName=  GmCommonClass.parseNull((String)hmConsignDetails.get("SNAME"));
			strPurpose =  GmCommonClass.parseNull((String)hmConsignDetails.get("PURP"));
			strType =  GmCommonClass.parseNull((String)hmConsignDetails.get("TYPE"));
			strUserName =  GmCommonClass.parseNull((String)hmConsignDetails.get("UNAME"));
			strIniDate =  GmCommonClass.parseNull((String)hmConsignDetails.get("CDATE"));
			strDesc =  GmCommonClass.parseNull((String)hmConsignDetails.get("COMMENTS"));
			strStatusFl =  GmCommonClass.parseNull((String)hmConsignDetails.get("SFL"));
			strTransType =  GmCommonClass.parseNull((String)hmConsignDetails.get("CTYPE"));
			strRequestorName =  GmCommonClass.parseNull((String)hmConsignDetails.get("RNAME"));
			strSetTypeId = GmCommonClass.parseNull((String)hmConsignDetails.get("SETCTYPEID"));
			strLoanerStatus = GmCommonClass.parseNull((String)hmConsignDetails.get("LOANERSTATUS"));
			strPurposeId =  GmCommonClass.parseNull((String)hmConsignDetails.get("PURPOSEID"));
			strErrorCnt = GmCommonClass.parseNull((String)hmConsignDetails.get("ERRCNT"));
			log.debug("strTransType is....................... "+strTransType);
			log.debug("strPurposeId is....................... "+strPurposeId);
			if (strhAction.equals("Verification") )
			{
				strLabel = "Verify";
				strStatusFl = "3";
			}
			if (strLoanerStatus.equals("5") || strLoanerStatus.equals("10") ){ // if loaner is allocated/pending shipping/
				if (strTransType.equals("50155")){//Move from Shelf to Loaner Set
					strMessage="This set may have to go to Shipping/CS directly";
				}
			//parts moving out of the set if the Reason is 'Set List Change'
				else if (strTransType.equals("50156") || strTransType.equals("50157") ||  strTransType.equals("50158") || strTransType.equals("50159")){
					if (strPurposeId.equals("50120") || strPurposeId.equals("50122")){
						strMessage="This set may have to go to Shipping/CS directly";	
					}
				}		
			}
	} 
	
	int intSize = 0;
	HashMap hcboVal = null;
	if(strTransType.equals("50157"))
	{
		strRuleTransType = "50916"; //Loaner to shelf
	}
	else if(strTransType.equals("50158"))
	{
		strRuleTransType = "50917"; //Loaner to quarantine
	}
	else if(strTransType.equals("50155"))
	{
		strRuleTransType = "50906"; //Shelf to Loaner 
	}
	else if(strTransType.equals("50160"))
	{
		strRuleTransType = "50910"; //Shelf to Loaner 
	}
	else if(strTransType.equals("50161"))
	{
		strRuleTransType = "50919"; //Shelf to Loaner 
	}
	else if(strTransType.equals("50162"))
	{
		strRuleTransType = "50918"; //Shelf to Loaner 
	}
	else if(strTransType.equals("4112"))
	{
		strRuleTransType = "50928"; //In house consignment 
	}
	else if(strTransType.equals("40053"))
	{
		strRuleTransType = "50926"; //In house consignment 
	}
	else if(strTransType.equals("100063") || strTransType.equals("1006570") )
	{
		strRuleTransType = "50910"; //Bulk to Loaner 
	}	
	if(strRuleType!=null && !strRuleType.equals("")){
		strRuleTransType = strRuleType;
	}
	if(strRuleConsignType!=null){
		strTransType = strRuleConsignType;
	}
	if(hmTransRules != null &&(strhAction.equals("EditLoad") || strhAction.equals("SaveControl") || strhAction.equals("ReleaseControl")))
	{
	   strHeaderRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_HEADER")); 
	   if(!strHeaderRule.equals(""))
		   strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PENDING_CONTROL"))+" : " +strHeaderRule;
	       strWikiTitle = GmCommonClass.getWikiTitle("PENDING_CONTROL");
	   System.out.println("strHeader===>"+strHeader);
	}
	if(hmTransRules != null && strhAction.equals("Verification"))
	{
	   strHeaderRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_HEADER")); 
	   if(!strHeaderRule.equals(""))
		   strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PENDING_VERIFICATION"))+" : "+ strHeaderRule;
	       strWikiTitle = GmCommonClass.getWikiTitle("PENDING_VERIFICATION");
	}
	
	request.setAttribute("alReturn", alSetLoad);
%>
<HTML>
<HEAD>
<TITLE>Globus Medical:Item Consignment-Enter Control Numbers </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmSPControlVerify.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>

<script>
var pickRule = '<%=strPickLocRule%>';
var objGridData = '<%=strGridXmlData%>';
var cnt = 0;
var prevtr = 0;
var vStatus = '<%=strVStatus%>';
var consignType = '<%=strConsignType%>';
var transStatus  = '<%=strTransStatus%>';
var strRuleAjax='<%=strRuleAjax%>';
var vIncludeRuleEng = '<%=strIncludeRuleEng.toUpperCase()%>';
var ruleTransType = '<%=strRuleTransType%>';
var transTypeID = '<%=strTransTypeID%>';
var servletpath = '<%=strServletPath%>';
var skpLotValidFl = '<%=strSkpLotValidFl%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmSetAddRemPartsServlet">
<input type="hidden" name="hAction" id="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hTxnId" value="<%=strTxnId%>">
<input type="hidden" name="hStatusFl" value="<%=strStatusFl%>">
<input type="hidden" name="hConsignType" value="<%=strConsignType%>">
<input type="hidden" name="hType" id="hType" value="<%=strTransType%>">
<input type="hidden" name="RE_FORWARD" value="gmSetAddRemPartsServlet">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" id="RE_TXN" value="<%=strRuleTransType%>">
<input type="hidden" id="TXN_TYPE_ID" name="TXN_TYPE_ID" value="<%=strTransTypeID%>">
<input type="hidden" name="ruleVerify" value="true">
<input type="hidden" name="hTxnName" value="<%=strTxnId%>">
<input type="hidden" name="hCancelType" value="VINTX">
<input type="hidden" name="hpnumLcnStr" value="">
<input type="hidden" name="partMaterialType" id="partMaterialType" value= "">
<input type="hidden" name="chkRule" id="chkRule" value= "No">
<xml src="<%=strXmlPath%>/<%=strSetTypeId%>.xml" id="xmldso" async="false"></xml>
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<%if (strSetTypeId.equals("4127")){%>
				<td height="25" class="RightDashBoardHeader"><fmtSPControlVerify:message key="LBL_PROCESS_REFILLS_PRODUCT_LOANER"/> : <%=strHeader%></td>
			<%}else if (strSetTypeId.equals("4119")){ %>
				<td height="25" class="RightDashBoardHeader"><fmtSPControlVerify:message key="LBL_PROCESS_REFILLS_IN_HOUSE_LOANER"/> : <%=strHeader%></td>
			<%}else{ %>
				<td height="25" class="RightDashBoardHeader"><span datasrc="#xmldso" datafld="TITLE-REFILL"></span><%=strHeader%></td>
			<%} %>
			<td height="25" class="RightDashBoardHeader" align="right">
			<fmtSPControlVerify:message key="IMG_HELP" var="varHelp"/>
				    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="evenshade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtSPControlVerify:message key="LBL_TRANSACTION_ID"/>:</b></td>
						<td class="RightText">&nbsp;<%=strTxnId%></td>
							<td align="left">
						<jsp:include page="/operations/logistics/GmIncludePriority.jsp">
	              <jsp:param name="CONSIGNID" value="<%=strConsignId%>"/> 
                  </jsp:include>
                  </td> 
						
					
					</tr>
					<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>	
					<tr class="oddshade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtSPControlVerify:message key="LBL_TRANSACTION_TYPE"/>:</b></td>
						<td colspan="2" class="RightText">&nbsp;<%=strType%></td>
					</tr>
					<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>
					<tr class="evenshade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtSPControlVerify:message key="LBL_PURPOSE"/>:</b></td>
						<td class="RightText">&nbsp;<%=strPurpose%></td>
					</tr>
					<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>
					<tr class="oddshade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtSPControlVerify:message key="LBL_REFERENCE_ID"/>:</b></td>
						<td  colspan="2" class="RightText">&nbsp;<%=strConsignId%></td>
					</tr>
					<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>	
					<tr class="evenshade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtSPControlVerify:message key="LBL_SET_NAME"/>:</b></td>
						<td colspan="2" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
					</tr>
					<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>
					<tr class="oddshade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtSPControlVerify:message key="LBL_INITIATED_BY"/>:</b></td>
						<td colspan="2" class="RightText">&nbsp;<%=strUserName%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><fmtSPControlVerify:message key="LBL_DATE_INITIATED"/>:</b>&nbsp;<%=strIniDate%></td>
					</tr>
					<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="40"><b><fmtSPControlVerify:message key="LBL_COMMENTS"/>:</b></td>
						<td colspan="2" class="RightText" valign="top" >&nbsp;<%=strDesc%></td>
					</tr>
					<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>				

<%
				if (!strVStatus.equals("Verified") && !strhAction.equals("Verification"))
				{
%>									
					<tr  class="oddshade">
						<td class="RightText" align="right" HEIGHT="24"><b><%=strLabel%> ?:</b></td>
						<td colspan="2" width="320">&nbsp;<input type="checkbox" size="30" name="Chk_ShipFl" value="" <%=strChecked%> onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=25></td>
					</tr>
					<tr><td colspan="3" height="1" bgcolor="#cccccc"></td></tr>
					<%if(strControlNoScan.equals("YES")){%>
					 <!-- To include the FG BIN Section-->
							<tr>
							<td colspan="3">
								<jsp:include page="/operations/itemcontrol/GmControlNoInclude.jsp" >
								<jsp:param name="FORMNAME" value="frmVendor" />
								</jsp:include>
							</td>
					     </tr>
  					<%} %>
					<tr>
				   		<td colspan="3">
						<jsp:include page="/common/GmIncludeLog.jsp" >
						<jsp:param name="LogType" value="" />
						<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
				  		</td>
			     	</tr>
					
<%
				}
					if (strhAction.equals("LoadSet")|| strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash") || strhAction.equals("Verification")  || strhAction.equals("SaveControl") || strhAction.equals("ReleaseControl"))
					{
						/*  removed condition (50152) for PC-4273 change */
						if (strhAction.equals("Verification") && (strTransType.equals("50161")||strTransType.equals("50157")) )
						{
							intSize = alSetLoad.size();							
	%>
						<tr>
							<td colspan="3">
								<table cellspacing="0" cellpadding="0" border="0" width="100%" class="DtTable700" >
									<tr> 
							        	<td colspan="3">
							          		<div id="dataGridDiv"  style="height : 300px;"></div>
										</td>		
									</tr>								
								</table>
							</td>
						</tr>			
	<%								
						}
							else
						{
	%>
					<tr>
						<td colspan="3">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td align="center" Width="40"><fmtSPControlVerify:message key="LBL_PART_NUMBER"/></td>
									<td width="300">&nbsp;<fmtSPControlVerify:message key="LBL_DESCRIPTION"/></td>
										<% if (strTransType.equals("50150") || strTransType.equals("50155")) {	%>
													<td  width="70" align="center"><fmtSPControlVerify:message key="LBL_QTY_IN_STOCK"/></td>
													<td width="70" align="center"><fmtSPControlVerify:message key="LBL_QTY_TO_ADD"/></td>
										<% } else if (strTransType.equals("50151") || strTransType.equals("50156") || strTransType.equals("50157"))	{ %>
													<td  width="70" align="center"> <fmtSPControlVerify:message key="LBL_QTY_IN_SET"/></td>
													<td  width="70" align="center"><fmtSPControlVerify:message key="LBL_QTY_TO_REMOVE"/> </td>
										<% } else { %>									
													<td align="center" width="70" align="center"><fmtSPControlVerify:message key="LBL_CONSIGN_QTY"/></td>
										<% } %>			
										<td width="80" align="center"><fmtSPControlVerify:message key="LBL_SCAN_QTY"/></td>
										<td width="80" align="center"><fmtSPControlVerify:message key="LBL_TRANS_QTY"/> </td>
										<td width="210"><fmtSPControlVerify:message key="LBL_CONTROL_NUMBER"/></td>
								</tr>
								<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
<%
						intSize = alSetLoad.size();
						StringBuffer sbPartNum = new StringBuffer();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							String strPartNumIndex = "";
							String StrParthidsts = "";
							String strParthidfinalsts = "";
							String strPrevPartNum = "";
							String strPartMaterialType = "";
							double dbAllTotQty = 0.0;
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								if(sbPartNum.indexOf(strPartNum+",") == -1){
									sbPartNum.append(strPartNum);
									sbPartNum.append(",");
								}
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPartMaterialType = GmCommonClass.parseNull((String)hmLoop.get("PARTMTRELTYPE"));								
								strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY_IN_TRANS"));
								
								strQtyId = strPartNum.replaceAll("[^a-zA-Z0-9]","");
								
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("QTYREF"));
								//strQtyRef = GmCommonClass.parseNull((String)hmLoop.get("QTYREF"));
								strStockQty = GmCommonClass.parseZero((String)hmLoop.get("QTYSHELF"));
								strSetCsgQty = GmCommonClass.parseZero((String)hmLoop.get("QTY_IN_SET"));
								strErrorDtls = GmCommonClass.parseNull((String)hmLoop.get("ERRORDTLS"));
								strErrorDtls = strErrorDtls.replaceAll("<B>", "").replaceAll("</B>", "");
								
								strTemp = strPartNum;
								StrParthidsts = "";
								
								if(!strPrevPartNum.equals(strPartNum)&&(i!=0)){									
									StrParthidsts = "<input type=\"hidden\" id=\""+strPrevPartNum+"\" name=\""+strPrevPartNum+"\" value=\""+strPartNumIndex+"\">";
									strPartNumIndex="";
								}
								
								if (strPartNum.equals(strNextPartNum)){
									intCount++;
									strLine = "<TR><TD colspan=6 height=1 class=Line></TD></TR>";									
								}else{
									strLine = "<TR><TD colspan=6 height=1 class=Line></TD></TR>";
									if (intCount > 0){
										strPartNum = "";
										strPartDesc = "";
										strQty = "";
										strLine = "";
									}else{
										//strColor = "";
									}
									intCount = 0;
								}
								strPartNumIndex = strPartNumIndex +i+",";
								if (intCount > 1){
									strPartNum = "";
									strPartDesc = "";
									strQty = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}
								
								if(i==(intSize-1)){
									strParthidfinalsts = "<input type=\"hidden\" id=\""+(strPartNum.equals("")?strPrevPartNum:strPartNum)+"\" name=\""+(strPartNum.equals("")?strPrevPartNum:strPartNum)+"\" value=\""+strPartNumIndex+"\">";
									strPartNumIndex = "";
								}
								
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								dbAllTotQty += Double.parseDouble(GmCommonClass.parseZero(strItemQty));
								strPrevPartNum = strPartNumHidden;
								out.print(strLine);
%>
								
								<tr  <%=strShade%> id="tr<%=i %>" >
<%
								if ((strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash") || strhAction.equals("SaveControl") || !strErrorCnt.equals("0")) && !strStatusFl.equals("4"))
								{
%>
									<% if(strStatusFl.equals("2")){ %>
										<td class="RightText" width="40">&nbsp;<a class="RightText" tabindex=-1 href="javascript:fnSplit('<%=i%>','<%=strPartNum%>');"><%=strPartNum%></a></td>
									<%}else{ %>
										<td class="RightText" width="40">&nbsp;<%=strPartNum%></td>
									<%} %>
									<input type="hidden" name="hPartNum<%=i%>" id="hPartNum<%=i%>"  value="<%=strPartNumHidden%>">
									<input type="hidden" name="hQty<%=strQtyId%>" id="hQty<%=strQtyId%>" value="<%=strQty%>">
									<input type="hidden" id="hPartMaterialType<%=i%>" name="hPartMaterialType<%=i%>" value="<%=strPartMaterialType%>">
									<input type="hidden" id="hControl<%=i%>" name="hControl<%=i%>" value="<%=strControl%>">	
									<%if(StrParthidsts.length()>0){%>
										<%=StrParthidsts%>
									<%}%>
									<%if(strParthidfinalsts.length()>0){%>
										<%=strParthidfinalsts%>
									<%}%>
									<!--<% if (strTransType.equals("50150") || strTransType.equals("50155")) {	%>									
													<input type="hidden" name="hQty<%=strPartNum.replaceAll("\\.","")%>" value="<%=strQty%>">
										<% } else if (strTransType.equals("50151") || strTransType.equals("50156") || strTransType.equals("50157"))	{ %>									
													<input type="hidden" name="hQty<%=strPartNum.replaceAll("\\.","")%>" value="<%=strQty%>">
										<% } else { %>									
													<input type="hidden" name="hQty<%=strPartNum.replaceAll("\\.","")%>" value="<%=strQty%>">
										<% } %>		
									-->
									<input type="hidden" name="hPrice<%=i%>" id="hPrice<%=i%>" value="<%=strPrice%>"></td>
<%
								}
								else if ((strhAction.equals("Verification") || strhAction.equals("ReleaseControl")) && strErrorCnt.equals("0"))
								{
%>
									<td class="RightText" width="60" height="20">&nbsp;<%=strPartNum%>
									<input type="hidden" name="hPartNum<%=i%>" id="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
									<td class="RightText" width="200">&nbsp;<%=strPartDesc%>
									<% if (strTransType.equals("50150") || strTransType.equals("50155")) {	%>									
													<td class="RightText"   width="70" align="center">&nbsp;<%=strStockQty%></td>
													<td class="RightText"   width="70" align="center">&nbsp;<%=strQty%></td>
										<% } else if (strTransType.equals("50151") || strTransType.equals("50156") || strTransType.equals("50157"))	{ %>									
													<td class="RightText"   width="70" align="center">&nbsp;<%=strSetCsgQty%></td>
													<td class="RightText"   width="70" align="center">&nbsp;<%=strQty%></td>
										<% } else { %>									
													<td class="RightText"   width="70" align="center">&nbsp;<%=strQty%></td>
										<% } %>		
									<td>&nbsp;</td>	
									<td  class="RightText" width="80" align="center"><%=strItemQty%></td>
									<td><%=strControl%></td>
<%								}
								else{
%>
									<td class="RightText" height="20"   width="70" align="center">&nbsp;<%=strPartNum%></td>
<%
									}
									
									if ( (!strhAction.equals("Verification") && !strhAction.equals("ReleaseControl")) || !strErrorCnt.equals("0"))
									{
%>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
											<% if (strTransType.equals("50150") || strTransType.equals("50155")) {	%>									
													<td class="RightText"   width="70" align="center">&nbsp;<%=strStockQty%></td>
													<td class="RightText"   width="70" align="center">&nbsp;<%=strQty%></td>
										<% } else if (strTransType.equals("50151") || strTransType.equals("50156") || strTransType.equals("50157"))	{ %>									
													<td class="RightText"   width="70" align="center">&nbsp;<%=strSetCsgQty%></td>
													<td class="RightText"   width="70" align="center">&nbsp;<%=strQty%></td>
										<% } else { %>									
													<td class="RightText"   width="70" align="center">&nbsp;<%=strItemQty%></td>
										<% } %>		
									
<%
								}
								if (strhAction.equals("EditLoad")|| strhAction.equals("EditLoadDash") || strhAction.equals("SaveControl") || !strErrorCnt.equals("0"))
								{
%>									<td align="right"><label id ="lblPartScan<%=strPartNum %>" ></label>   </td>
									<td id="Cell<%=i%>" class="RightText" align="center"><input type="text" size="3" value="<%=strItemQty%>" name="Txt_Qty<%=i%>" id="Txt_Qty<%=i%>" class="InputArea Controlinput" onFocus="fnBlankNOC(this);fnBlankLine(<%=i %>,this.value);changeBgColor(this,'#ffffff');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3></td>
									<td id="Cnum<%=i%>" align="left"><input type="text" size="22" value="<%=strControl%>" name="Txt_CNum<%=i%>" id="Txt_CNum<%=i%>" class="InputArea Controlinput" maxlength="40"  onFocus="fnBlankNOC(this);fnBlankLine(<%=i %>,this.value);changeBgColor(this,'#ffffff');" onBlur="changeBgColor(this,'#ffffff');" onChange="<%if(!strIncludeRuleEng.equalsIgnoreCase("NO")){%>fnFetchMessages(this,'<%=strPartNumHidden %>','PROCESS', '<%=strRuleTransType%>','<%=strTransTypeID%>');<%} %>" tabindex=3>
									<%-- <%if(strErrorDtls.equals("") && !strControl.equals("") && !strControl.equals("TBE")){ %>
									<span id="DivShowCnumAvl<%=i%>" style="vertical-align:middle;"> <img height="16" width="19" title="Control number available" src="<%=strImagePath%>/success.gif"></img></span>
									<%}else--%> <% if(!strErrorDtls.equals("") && !strControl.equals("")){ %>
									<span id="DivShowCnumNotExists<%=i%>" style="vertical-align:middle;"> <img height="12" width="12" title="<%=strErrorDtls %>" src="<%=strImagePath%>/delete.gif"></img></span>
									<%} %>
									</td>
<%
								}
								else if ((strhAction.equals("Verification")  || strhAction.equals("ReleaseControl")) && strErrorCnt.equals("0")) {  }
								
								else {
%>
									<td>&nbsp;</td>
<%
									}
%>
								</tr>
<%
							}//End of FOR Loop
							out.print("<input type=hidden name=pnums value='"+sbPartNum.toString().substring(0,sbPartNum.length()-1)+"'>");
							out.print("<input type=hidden id=hTotalParts name=hTotalParts value="+dbAllTotQty+">");
						}else {
%>
						<tr><td colspan="7" height="50" align="center" class="RightTextRed"><BR><fmtSPControlVerify:message key="BTN_NO_PARTS_AVAILABLE_IN_THIS_TRANSACTION" /></td></tr>
<%
						}		
%>
							</table>
						</td>								
					</tr>
				
<%						}
%>
						<input type="hidden" name="hCnt" id="hCnt" value="<%=intSize-1%>">
						<input type="hidden" id="hNewCnt" name="hNewCnt" value="0">
<%						
					}// close line 350 if
%>					
					

				 </table>
			 </td> </tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
	 	<tr><td colspan="3" class="LLine"></td></tr>
		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td colspan="3">
				<jsp:include page="/common/GmRuleDisplayAjaxInclude.jsp"/>
			</td>
		</tr>
		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td colspan="3">
					<jsp:include page="/operations/itemcontrol/GmValidationDisplayInclude.jsp">
					<jsp:param value="<%=strErrorCnt%>" name="errCnt"/>
					</jsp:include>
			</td>
		</tr>

<%
				if (!strStatusFl.equals("4"))
				{
					if (strhAction.equals("LoadSet"))
					{
%>
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtSPControlVerify:message key="BTN_INITIATE" var="varInitiate"/><gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="13" buttonType="Save" />
						</td>
					</tr>
<%
					}
					else if (strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash") || strhAction.equals("SaveControl") || !strErrorCnt.equals("0"))						
					{
						String strAccessfl=GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL")).equals("")?GmCommonClass.parseNull((String)session.getAttribute("ACCESSFL")): GmCommonClass.parseNull((String)request.getAttribute("ACCESSFL"));
%>              
              
					<tr>
						<td colspan="2" height="30" align="center">
<%						if(strAccessfl.equals("Y")){ %>
							<fmtSPControlVerify:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" disabled="<%=strDisable%>" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
							<fmtSPControlVerify:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" gmClass="button" onClick="fnVRule();" tabindex="14" buttonType="Save" />
					<%}else{
								out.print("<b><center><span style=\"color:red;\">You do not have enough permission to do this action</span><center></b>");	
							}
								%>
								</td>
								</tr>
<%
					}
					else if (strhAction.equals("Verification"))
					{
						showVerifyBtn = true;
					}else if (strhAction.equals("ReleaseControl") && strErrorCnt.equals("0"))
					{
%>
					<tr>
						<td colspan="2" height="30" align="center" class="RightTextBlue"><fmtSPControlVerify:message key="LBL_THIS_TRANSACTION_HAS_BEEN_SAVED_AND_IS_READY_FOR_VERIFICATION"/></td>
					</tr>
<%					}
				}
				else{
%>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td colspan="2" height="30" align="center" class="RightTextRed"><BR><fmtSPControlVerify:message key="LBL_INVENTORY_AND_LOGISTICS_MANAGER_SYSTEM_ADMIN"/>
						<BR><BR>
						</td>
					</tr>
<%               
				}
%>	

<% 					if (showVerifyBtn){
						if (strVStatus.equals("Verified"))
						{
							strdisabled = "true";
%>
					<tr>
						<td  colspan="3" height="30" align="center" class="RightTextBlue"><fmtSPControlVerify:message key="LBL_THIS_TRANSACTION_HAS_BEEN_VERIFIED"/></td>
					</tr>
					
						<% if (!strMessage.equals(""))
							{%>
							<tr>
								<td  colspan="3" height="30" align="center" class="RightTextBlue"><font color="Red"><B><%=strMessage%></B></font></td>
							</tr>
						<%							
							}
						%>
<%							
						}
%>
					<tr>
						<td  colspan="3" height="30" align="center">
							<fmtSPControlVerify:message key="BTN_VERIFY" var="varVerify"/><gmjsp:button value="${varVerify}" disabled="<%=strdisabled%>" gmClass="button" onClick="fnVerify();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
							<fmtSPControlVerify:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" gmClass="button" onClick="fnVRule();" tabindex="14" buttonType="Save" />
						</td>
						
					</tr>
<%
					}
%>
    </table>

<%-- 
<table border="1" cellspacing="0" cellpadding="0">
	
	
	
</table>
--%>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
