<%
/**********************************************************************************
 * File		 		: GmSetStatusReport.jsp
 * Desc		 		: This screen is used to display Set Status Report
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ page language="java" %>
 
 
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="java.util.HashMap"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ include file="/common/GmHeader.inc" %> 

<!-- Imports for Logger -->
 
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>

<%@ taglib prefix="fmtSetStatusReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmSetStatusReport.jsp-->

<fmtSetStatusReport:setLocale value="<%=strLocale%>"/>
<fmtSetStatusReport:setBundle basename="properties.labels.operations.logistics.GmSetStatusReport"/>



<bean:define id="summaryVer" name="frmLogisticsReport" property="summaryVer" type="java.lang.String"></bean:define>

<%
 	 
	String strWikiTitle = GmCommonClass.getWikiTitle("SET_STATUS_REPORT");
	 
			
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Status -- Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
 
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
 <script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> 


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>


function fnGo()
 {
	/*var objCStatus = document.frmDashBoard.Chk_GrpCStatus;
 	var statuslen = objCStatus.length;
 	var cStatus = "";
 	
  fnValidateTxtFld('setID',' Set ID ');
  if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
 	
 for(var i=0;i<statuslen;i++)
	{
		if (objCStatus[i].status == true)
		{
			dStatus = dStatus + objCStatus[i].value + ',';
		}
	}
  */
  // alert("b4 values is " + document.frmLogisticsReport.hreqstatusNotcheck.value);
   //alert("b4 hconstatus values is " + document.frmLogisticsReport.hconstatusNotcheck.value);
   
   fnCheckSelections();
   
  var varsetId = document.frmLogisticsReport.setID.value;
  var varpartNum = document.frmLogisticsReport.partNum.value;
  var varconsignID = document.frmLogisticsReport.consignID.value;
  var varrequestId = document.frmLogisticsReport.requestId.value;
  var varcontrolNum = document.frmLogisticsReport.controlNum.value;
  var varproject = document.frmLogisticsReport.project.value;
  var varsummaryVer = document.frmLogisticsReport.summaryVer.checked;
  
  var ctrlNumOperator = document.frmLogisticsReport.operator.value;
  
  //alert("summaryVer is " + varsummaryVer);
  if(varsetId == '' && varpartNum == '' && varconsignID =='' && varrequestId=='' && varcontrolNum =='' && varproject== '0')
  	Error_Details(message_operations[519]);
  
  if(varcontrolNum == 'TBE' && ctrlNumOperator == '13' && varsetId =='' && varconsignID == '' && varrequestId == '' && varpartNum == '' && varproject=='0')
  	Error_Details(message_operations[520]);
   
  if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
  //alert("values is " + document.frmLogisticsReport.hreqstatusNotcheck.value);
  // alert("hconstatus values is " + document.frmLogisticsReport.hconstatusNotcheck.value);
  document.frmLogisticsReport.strOpt.value="reload";
  fnStartProgress();
  document.frmLogisticsReport.submit();
 }
 
  

 
 
 
function fnCheckSelections()
{
	objCheckConArrLen = 0;
	objCheckReqArrLen = 0;
	reqflag = '';
	conflag = '';
	
	objCheckConArr = document.frmLogisticsReport.checkSelectedCon;
	if(objCheckConArr) {
	 
		objCheckConArrLen = objCheckConArr.length;
		for(i = 0; i < objCheckConArrLen; i ++) 
			{	
			    if(objCheckConArr[i].checked)
			    {				    		   
				conflag = '1';
			//	alert(" loopCons" + conflag);				
				}
			}
			
		 if(conflag=="")
		 {		 
		 document.frmLogisticsReport.hconstatusNotcheck.value="true";
		 }
		 else
		 { 
		  document.frmLogisticsReport.hconstatusNotcheck.value="";
		 }
	}
	
	objCheckReqArr = document.frmLogisticsReport.checkSelectedReq;
	if (objCheckReqArr) {
		// check if there is just one element in checked. if so mark it as selected
	 
		objCheckReqArrLen = objCheckReqArr.length;
		for(i = 0; i < objCheckReqArrLen; i ++) 
			{	
				 
				if(objCheckReqArr[i].checked)
				{
				reqflag = '1';
				//alert(" loopreq" + reqflag);
				}
			}
			 
		 if(reqflag=="")
		 {
		 //alert("Request" + reqflag);
		 document.frmLogisticsReport.hreqstatusNotcheck.value="true";
		 }
		 else
		 {
		  document.frmLogisticsReport.hreqstatusNotcheck.value="";
		 }
	}
	if(conflag=='' && reqflag=='')
	
	Error_Details(message_operations[521]);
}	

 

function fnFetch(strConId)
 {
    
    //  windowOpener("/gmSetBuildProcess.do?consignID="+strConId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
     document.frmDashBoard.action = "/gmSetBuildProcess.do?consignID="+strConId;
  	document.frmDashBoard.submit();	
    }  
    
function fnViewDetails(strReqId)
 {
    
      windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+strReqId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
    }
   
function fnPrintVer(strConId)
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+strConId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}


function fnCallSetMap(setid,setnm)
{
	 var temp = new Array();
		    temp = setnm.split('-');
		    setnm ="";
		    for(i=0; i<temp.length; i++)
		    {
		    	setnm = setnm + temp[i] + " ";
		    	}
	windowOpener("/GmSetReportServlet?hOpt=SETRPT&hAction=Drilldown&hSetId="+setid+ "&hSetNm="+setnm,"PrntInv1","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}


function fnSetList()
{
windowOpener("/GmPageControllerServlet?strPgToLoad=GmSetReportServlet&strOpt=SETRPT","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=680,height=600");
}
 

function fnPartReport()
{
	
	alert(message_operations[522]);
	 
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  >
<html:form action="/gmLogisticsReport.do?method=loadSetStatusReport"  >  
<html:hidden property="strOpt" value=""/>
<html:hidden property="hconstatusNotcheck" />
<html:hidden property="hreqstatusNotcheck" />

<jsp:include page="/common/GmScreenError.jsp" />
<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
 	<tr class=Line >
		<td height=25 class=RightDashBoardHeader colspan=5><fmtSetStatusReport:message key="LBL_SET_STATUS_REPORT"/> </td>
		<td align="right" class=RightDashBoardHeader > 
		<fmtSetStatusReport:message key="IMG_ALT_HELP" var="varHelp"/>	
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
		</td>
	</tr>
	<tr class=Line><td noWrap height=1 colspan=6></td></tr>
	<tr>
       	<td width="8%" class="RightTableCaption" align="right"   HEIGHT="24" ></font>&nbsp;<fmtSetStatusReport:message key="LBL_CONSIGNMENT"/><br><fmtSetStatusReport:message key="LBL_STATUS"/> :</td> 
        <td width="15%">&nbsp;
        	<DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
            	<table>
                   	<logic:iterate id="SelectedConlist" name="frmLogisticsReport" property="alConStatus">
                     	<tr>
                    		<td>
						    	<htmlel:multibox property="checkSelectedCon" value="${SelectedConlist.CODENMALT}" />
							    <bean:write name="SelectedConlist" property="CODENM" />
							</td>
						</tr>	    
					</logic:iterate>
				</table>
			</DIV><BR>
		</td>
        <td width="4%" class="RightTableCaption" align="right"    HEIGHT="24" ></font>&nbsp;<fmtSetStatusReport:message key="LBL_REQUEST"/><br><fmtSetStatusReport:message key="LBL_STATUS"/> :</td> 
        <td width="14%">&nbsp;
        	<DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
            	<table>
                	<logic:iterate id="SelectedReqlist" name="frmLogisticsReport" property="alReqStatus">
                       	<tr>
                       		<td>
						    	<htmlel:multibox property="checkSelectedReq" value="${SelectedReqlist.CODENMALT}" />
							    <bean:write name="SelectedReqlist" property="CODENM" />
							</td>
						</tr>	    
					</logic:iterate>
				</table>
			</DIV><BR>
		</td>
	    <td width="59%">
	    	<table>
	    		<tr>
					<td width="19%" class="RightTableCaption" align="right"  HEIGHT="24" ><fmtSetStatusReport:message key="LBL_SET_ID"/>&nbsp;:&nbsp;</td>
					<td width="11%">
					<!-- Struts tag lib code modified for JBOSS migration changes -->
						<html:text property="setID"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
						<a href= javascript:fnSetList();><img src="<%=strImagePath%>/action-detail.gif" style="border: none;" height="14"></a>
					</td>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<td width="30%" class="RightTableCaption" align="right"  HEIGHT="24" >&nbsp;&nbsp;<fmtSetStatusReport:message key="LBL_PART"/>&nbsp;:&nbsp;</td>						
					<td width="40%">
						<html:text property="partNum"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
						<gmjsp:dropdown controlName="search" SFFormName="frmLogisticsReport" SFSeletedValue="search"
						SFValue="alSearch" codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]" />										
					</td>
				</tr>
				<tr>
					<td width="20%" class="RightTableCaption" align="right"  HEIGHT="24" ><fmtSetStatusReport:message key="LBL_CONID"/>&nbsp;:&nbsp;</td>
					<td width="10%">
						<html:text property="consignID"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
					</td>
					<td width="30%" class="RightTableCaption" align="right"  HEIGHT="24" >&nbsp;&nbsp;<fmtSetStatusReport:message key="LBL_PROJECT_LIST"/>&nbsp;:&nbsp;</td>
					<td width="40%">
						<gmjsp:dropdown controlName="project" SFFormName="frmLogisticsReport" SFSeletedValue="project"
						SFValue="alProject" codeId = "ID"  codeName = "NAME"     defaultValue= "[Choose One]" width="250"/>	
					</td>
				</tr>
				<tr>
					<td class="RightTableCaption" align="right"  HEIGHT="24" ><fmtSetStatusReport:message key="LBL_REQID"/>&nbsp;:&nbsp;</td>
					<td><html:text property="requestId"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;</td>
			    	<td class="RightTableCaption" align="right"  HEIGHT="24" >&nbsp;&nbsp;<fmtSetStatusReport:message key="LBL_CONTROL"/>&nbsp;</td>
					<td>
						<gmjsp:dropdown controlName="operator" SFFormName="frmLogisticsReport" SFSeletedValue="operator"
						SFValue="alCtrlNumOpers" codeId="CODENMALT" codeName="CODENM" />&nbsp;
						<html:text property="controlNum"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtSetStatusReport:message key="BTN_GO" var="varGo"/>
						<gmjsp:button value="&nbsp;${varGo}&nbsp;" onClick="javascript:fnGo();"  gmClass="button" buttonType="Load" /></td>					
				</tr>
				<tr>
					<td align="right">&nbsp;<html:checkbox  property="summaryVer" /></td>
                    <td class="RightTableCaption" align="left" HEIGHT="24"><fmtSetStatusReport:message key="LBL_SUMMARY_VERSION"/></td> 
                    <td></td>
                    <td></td>
        		</tr>
			</table>
		</td>
	</tr>				
	<tr><td colspan="6" class="ELine"></td></tr>
	<tr>
		<td colspan="6">
	    	<display:table  name="requestScope.frmLogisticsReport.ldtResult" class="its" id="currentRowObject" varTotals="totals" requestURI="/gmLogisticsReport.do?method=loadSetStatusReport" 
	    	export="true" defaultsort="1" decorator="com.globus.displaytag.beans.DTSetStatusReportWrapper" freezeHeader="true" paneSize="500"> 
	    		<display:setProperty name="export.excel.filename" value="SetStatusReport.xls" />
				<fmtSetStatusReport:message key="DT_SET_ID" var="varSetID"/>
				<display:column property="SETID" title="${varSetID}" group="1" sortable= "true" class="alignleft"/>
				<fmtSetStatusReport:message key="DT_SET_NAME" var="varSetName"/>
				<display:column property="SETNAME" title="${varSetName}" group="2"  sortable= "true" class="alignleft"/>
				
				<% if (summaryVer.equals("")) { %>
					<fmtSetStatusReport:message key="DT_REQUEST_ID" var="varRequest_ID"/>
					<display:column property="REQID" title="${varRequest_ID}" group="3" sortable= "true" class="aligncenter"/> 
					<fmtSetStatusReport:message key="DT_CONSIGNMENT_ID" var="varConsignment_ID"/>
					<display:column property="CONID" title="${varConsignment_ID}" group="4" maxLength="20" class="alignleft"/>
				<% } %>				 
				
				<fmtSetStatusReport:message key="DT_REQUEST" var="varRequest"/>
				<display:column property="REQSTATUS" title="${varRequest} <br> Status"  class="alignleft"/>
				<fmtSetStatusReport:message key="DT_CONSIGN" var="varConsign"/>
				<display:column property="CONSTATUS" title="${varConsign} <br> Status"  class="alignleft"/>
				<fmtSetStatusReport:message key="DT_PART" var="varPart"/>
				<display:column property="PNUM" title="${varPart}" sortable= "true"   class="aligncenter"/> 
				<fmtSetStatusReport:message key="DT_PART_DESC" var="varPartDesc"/>
				<display:column property="PDESC" escapeXml="true" title="${varPartDesc}" sortable= "true" class="alignleft"/> 
				<fmtSetStatusReport:message key="DT_QTY" var="varQty"/>
				<display:column property="QTY" title="${varQty}" class="alignleft" total="true" format="{0,number,0}"/>
				
				<% if (summaryVer.equals("")) { %>
				    <fmtSetStatusReport:message key="DT_CONTROL" var="varControl"/>
					<display:column property="CNTRLNUM" title="${varControl}" class="alignleft"/>
									<% } %>
				
				<display:footer media="html">
				<% 
					String strColumn;
					String strVal;
								
					if (summaryVer.equals("")) {
						strColumn = "column9";
					}
					else {
						strColumn = "column7";
					}
					
					strVal = ((HashMap)pageContext.getAttribute("totals")).get(strColumn).toString();
					if(strVal != null && strVal.indexOf(".") != -1) {
						strVal = strVal.substring(0, strVal.indexOf("."));
					}
				%>
					<tr>
						<td <% if (summaryVer.equals("")) { %> colspan="10" <% } else { %> colspan="7" <% } %>class="Line"></td>
					</tr>
	  				<tr class = "shade">
			  			<td <% if (summaryVer.equals("")) { %> colspan="8" <% } else { %> colspan="6" <% } %>class="alignright"><B><fmtSetStatusReport:message key="LBL_TOTAL"/>&nbsp;:&nbsp;</B></td>
			  			<td <% if (summaryVer.equals("")) { %> colspan="2" <% } %> class="alignleft" ><B><%=strVal%></B></td>
	  				</tr>
	 			</display:footer>
			</display:table>
		</td>
	</tr> 
 	<tr><td colspan="6" class="ELine"></td></tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
