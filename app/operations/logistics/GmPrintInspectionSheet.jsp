<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtPrintInspectionSheet" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.ArrayList, java.net.URLEncoder" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- operations\logistics\GmPrintInspectionSheet.jsp -->

<fmtPrintInspectionSheet:setLocale value="<%=strLocale%>"/>
<fmtPrintInspectionSheet:setBundle basename="properties.labels.operations.logistics.GmPrintInspectionSheet"/>



<%
String strPartyJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PARTY");
String strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
String strFileStatus=GmCommonClass.parseNull((String)request.getAttribute("FILESTATUS"));
String strPath = strImagePath + "/error.gif";
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Globus Medical: Party Summary</title>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strPartyJsPath%>/GmParty.js"></script>

<script>
var hopt = '<%=strOpt%>';
var fileSts = '<%=strFileStatus%>';
function fnprint()
{	
	if(fileSts != 'N')
	{
		//alert('Click OK once PDF is loaded');
		var pdf = document.getElementById('pdf_content');	
		if(hopt != 'manually'){
			pdf.printAll();
			setTimeout("window.close();",7100);
		}else{
			pdf.print();
		}
	}else
	{
		if(hopt != 'manually'){
		setTimeout("window.close();",2100);
		}
	}
}
</script>

<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
</head>
<%
  String strSetId = GmCommonClass.parseNull(request.getParameter("hSetId"));
  String strType= GmCommonClass.parseNull(request.getParameter("hType"));
  
  //The following code added for passing the company info to append the company information
  String strLanguageId = "103097";
  strLanguageId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLangId"));
    
  String strCompanyInfo ="{\"cmpid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getCmpid())
                        + "\",\"partyid\":\"" + GmCommonClass.parseNull((String)gmDataStoreVO.getPartyid())
                        + "\",\"plantid\":\"" + GmCommonClass.parseNull((String)gmDataStoreVO.getPlantid())
                        + "\",\"cmplangid\":\"" +strLanguageId+"\"}";
                        
  strCompanyInfo = URLEncoder.encode(strCompanyInfo);
 // to append the company info 
  String strURL = "/GmInHouseSetServlet?hAction=OpenDoc&hSetId="+strSetId+"&hType="+strType+"&companyInfo="+strCompanyInfo;
  
%>
<body leftmargin="20" topmargin="10" onload="fnprint();">
<div id="pdf"> 
<% if(strFileStatus.equals("Y")){ %>
 <%-- <OBJECT id = "pdf_content" name="pdf_content" CLASSID="clsid:CA8A9780-280D-11CF-A24D-444553540000" WIDTH="700" HEIGHT="900">
    <PARAM NAME='SRC' VALUE="<%=strURL%>">
</OBJECT>  --%>
 <embed
    type="application/pdf"
    src="<%=strURL%>"
    id="pdf_content"
    width="100%"
    height="100%" /> 
<%}else{ %>
	<table border="0" width="700" height="550" cellpadding="0" cellspacing="0" >
		<tr>
			<td>
		  		<table border="1" cellpadding="0" cellspacing="0" width="50%" bordercolor="red" >    
		      		<tr bgcolor="red">
		        		<td align="center" width="100%" class="RightTableCaption" height="25">
		        			<font color="#FFFFFF"><strong><fmtPrintInspectionSheet:message key="LBL_GLOBUS_MEDICAL_ERROR_MSG"/></strong></font>
		        		</td>
		      		</tr>
		       		<tr>
		        		<td bgcolor="#CCCCCC">
		          			<center><img src="<%=strPath%>" width="32" height="32"></center>
		        		</td>
		      		</tr>
		      		<tr>             
		        		<td colspan="2" height="25" align="center" bgcolor="#CCCCCC" class="RightText"> 
		        			<font color="#663399" size="2">
		        				<b>	<br><fmtPrintInspectionSheet:message key="LBL_INSPECTION_SHEET_FOR"/>  <%=strSetId%><fmtPrintInspectionSheet:message key="LBL_NOT_FOUND"/> . <br> <br> </b>
		        			</font>
		        		</td>
		      		</tr>
				</table>
			</td>
		</tr>
	</table>
<%} %>
 </div> 
</body>
</html>