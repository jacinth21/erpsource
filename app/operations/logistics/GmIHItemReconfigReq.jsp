<%
/**********************************************************************************
 * File		 		: GmIHItemReconfigReq.jsp
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtIHItemReconfigReq" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIHItemReconfigReq:setLocale value="<%=strLocale%>"/>
<fmtIHItemReconfigReq:setBundle basename="properties.labels.operations.logistics.GmIHItemReconfigReq"/>

<!-- operations\logistics\GmIHItemReconfigReq.jsp -->

<bean:define id="xmlGridData" name="frmIHItemRequest" property="xmlGridData" type="java.lang.String"></bean:define>
<bean:define id="strInHouseTxns" name="frmIHItemRequest" property="inHouseTxns" type="java.lang.String"></bean:define>
<bean:define id="strTxns" name="frmIHItemRequest" property="txns" type="java.lang.String"></bean:define>
<bean:define id="alPartNmDtl" name="frmIHItemRequest" property="alPartNmDtl" type="java.util.ArrayList"></bean:define>
<bean:define id="createdDate" name="frmIHItemRequest" property="cdate" type="java.util.Date"></bean:define>
<bean:define id="strStatus" name="frmIHItemRequest" property="hStatus" type="java.lang.String"></bean:define>
<%
String strLogisticsJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
request.setAttribute("alPartNumDtl", alPartNmDtl);
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
String strDisable = "";
if(strStatus.equals("4") || strStatus.equals("3")){
	strDisable = "true";
}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Reconfigure Inhouse Request  </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmIHItemReconfigReq.js"></script>
<script type="text/javascript">
var objGridData = '<%=xmlGridData%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="20" onload="fnOnPageLoad();">
<html:form  action="/gmIHItemRequest.do">
<html:hidden property="strOpt" name="frmIHItemRequest"/>
<html:hidden property="requestID" name="frmIHItemRequest"/>
<html:hidden property="requestTypeID" name="frmIHItemRequest"/>
<html:hidden property="replenish" name="frmIHItemRequest"/>
<html:hidden property="hStatus" name="frmIHItemRequest"/>
<html:hidden property="strFGString" name="frmIHItemRequest" />
<html:hidden property="strBLString" name="frmIHItemRequest" />
<html:hidden property="strIHString" name="frmIHItemRequest" />
<html:hidden property="strVDString" name="frmIHItemRequest" />

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		
		<tr>
			<td height="30" colspan="3" class="RightDashBoardHeader" >&nbsp;<fmtIHItemReconfigReq:message key="LBL_RECONFIGINHOUSE_REQ"/></td>
			<td align="right" colspan="1" class=RightDashBoardHeader><fmtIHItemReconfigReq:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("INHOUSE_REQ_RECONFIG")%>');" />
			</td>  
		</tr>
		<tr><td class="LLine" height="1" colspan="4"></td></tr>
		<tr class="evenshade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="20%"><fmtIHItemReconfigReq:message key="LBL_REQ_ID"/>:</td>
			<td>&nbsp;<bean:write name="frmIHItemRequest" property="requestID"/></td>
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtIHItemReconfigReq:message key="LBL_REQ_TYPE"/>:</td>
			<td>&nbsp;<bean:write name="frmIHItemRequest" property="reqtype"/></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="4"></td></tr>
		<tr class="oddshade">	
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtIHItemReconfigReq:message key="LBL_STATUS"/>:</td>
			<td>&nbsp;<bean:write name="frmIHItemRequest" property="status"/></td>		
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtIHItemReconfigReq:message key="LBL_PURPOSE"/>:</td>
			<td>&nbsp;<bean:write name="frmIHItemRequest" property="purpose"/></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="4"></td></tr>
		<tr class="evenshade">
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtIHItemReconfigReq:message key="LBL_CREATED_DATE"/>:</td>
			<td>&nbsp;<%=GmCommonClass.getStringFromDate(createdDate,strApplDateFmt)%></td>
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtIHItemReconfigReq:message key="LBL_CREATED_BY"/>:</td>
			<td>&nbsp;<bean:write name="frmIHItemRequest" property="cname"/></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="4"></td></tr>
	  	<TR>
			<td colspan="4"> 
			<div>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page="/operations/logistics/GmIHRequestDtlInclude.jsp"/>
			</div>
			</td>
		</TR>
		<%if(xmlGridData.indexOf("cell") != -1){%>
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4">&nbsp;
				<a href="javascript:fnShowHide('tabRepTxn');">
				<IMG id="tabRepTxnimg" border=0 src="<%=strImagePath%>/minus.gif">
				</a>
				&nbsp;<fmtIHItemReconfigReq:message key="LBL_REPLENISH_TRANSACTION"/>
			</td>
		</tr>				
				<tr id="tabRepTxn">
					<td colspan="4">
						<div  id="GridData" style="" height="250px"></div>						
					</td>
				</tr>
			<%}else if(!xmlGridData.equals("")){%>
		<tr>
			<td colspan="4" align="center" class="RightText"><fmtIHItemReconfigReq:message key="MSG_NO_DATA"/>

			</td>
		</tr>
		<%}%>
		<tr>
			<td height="1" colspan="4" class="LLine"></td>
		</tr>
		<tr>
		<td colspan="4">
			<jsp:include page="/common/GmIncludeLog.jsp" >
			<jsp:param name="FORMNAME" value="frmIHItemRequest" />
			<jsp:param name="ALNAME" value="alLogReasons" />
			<jsp:param name="LogMode" value="Edit" />
			</jsp:include>
		</td>
     	</tr>
		<tr height="40">
			<td colspan="4" align="center">
			<fmtIHItemReconfigReq:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button  controlId="btnSubmit" name="btnSubmit" value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" tabindex="25" buttonType="Save" />&nbsp;&nbsp;
				<fmtIHItemReconfigReq:message key="BTN_REVIEW" var="varReview"/>
				<gmjsp:button  controlId="btnReview" name="btnReview" value="&nbsp;${varReview}&nbsp;" disabled="<%=strDisable%>" gmClass="button" onClick="fnReview();" tabindex="25" buttonType="Save" />
			</td>
		</tr>
		<%if((!strInHouseTxns.equals("")) && (strTxns.equals(""))){ %>
		<tr>
		<td height="1" colspan="4" class="LLine"></td>
		</tr>
		<tr height="40">
			<td colspan="4" class="RightText" align="center">
				The following transaction(s) 
				<%
				String[] arryTxn = strInHouseTxns.split(",");
				for(int i=1;i<arryTxn.length;i++){
					String strTxn = arryTxn[i];
				%>
				<a href="#" onclick="javascript:fnPicSlip('<%=strTxn%>');"><%=strTxn%></a>
				<%if(i != arryTxn.length - 1){ %>,<%}%>
				<%} %> 
				 has been initiated and can be viewed in the Inhouse Dashboard.				 
			</td>
		</tr>	
		<%}else if(!strInHouseTxns.equals("") && !strTxns.equals("")){%>
			<tr>
			<td height="1" colspan="4" class="LLine"></td>
			</tr>
			<tr height="40">
				<td colspan="4" class="RightText" align="left">
					The following transaction(s) 
					<%
					String[] arryInHouseTxn = strInHouseTxns.split(",");
					for(int i=1	;i<arryInHouseTxn.length;i++){
						String strInHouseTxn = arryInHouseTxn[i];
					%>
					<a href="#" onclick="javascript:fnPicSlip('<%=strInHouseTxn%>');"><%=strInHouseTxn%></a>
					<%if(i!= arryInHouseTxn.length - 1){ %>,<%}%>
					<%} %> 
					 has been initiated and can be viewed in the Inhouse Dashboard and	
					 <%
					 	String[] arryTxn = strTxns.split(",");
							for(int j=1;j<arryTxn.length;j++){
							String strTxn = arryTxn[j];
					 %>	
					 <a href="#" onclick="javascript:fnPicSlip('<%=strTxn%>');"><%=strTxn%></a>
					 <%if(j != arryTxn.length - 1){ %>,<%}%>
					<%} %> 		 
					has been initiated in Verified status.
				</td>
			</tr>	
		<%}else if((strInHouseTxns.equals("")) && (!strTxns.equals(""))) {%>
			<tr>
			<td height="1" colspan="4" class="LLine"></td>
			</tr>
			<tr height="40">
				<td colspan="4" class="RightText" align="center">
					The following transaction(s) 
					<%
					String[] arryTxn = strTxns.split(",");
					for(int i=1;i<arryTxn.length;i++){
						String strTxn = arryTxn[i];
					%>
					<a href="#" onclick="javascript:fnPicSlip('<%=strTxn%>');"><%=strTxn%></a>
					<%if(i != arryTxn.length - 1){ %>,<%}%>
					<%} %> 
					 has been initiated				 
					 <logic:notEqual property="requestTypeID" name="frmIHItemRequest" value="1006572">
					  in Verified status.	
					  </logic:notEqual>			 
				</td>
			</tr>	
		<%} %>
	</table>		 
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>