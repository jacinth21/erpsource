<%
/**********************************************************************************
 * File		 		: GmSetOverviewReport.jsp
 * Desc		 		: This screen is used to display Set Overview Report
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ page language="java" %>
 
 <%@ include file="/common/GmHeader.inc" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
 

<!-- Imports for Logger -->
 
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>

<%@ taglib prefix="fmtSetOverviewReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmSetOverviewReport.jsp -->

<fmtSetOverviewReport:setLocale value="<%=strLocale%>"/>
<fmtSetOverviewReport:setBundle basename="properties.labels.operations.logistics.GmSetOverviewReport"/>

 
<bean:define id="hmSetOverview" name="frmLogisticsReport" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define> 
<%
	 
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("SET_OVERVIEW");
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	gmCrossTab.setNoDivRequired(true);
	 
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setLinkRequired(false);	
	
	// Added for zero value
	//gmCrossTab.setValueType(0);
	
	
	
	//gmCrossTab.add_RoundOff("Shipped","0");
	//gmCrossTab.add_RoundOff("Pending Shipping","0");
	//gmCrossTab.add_RoundOff("Ready To Consign","0");
	gmCrossTab.add_RoundOff("Avg Net","2");
 
	//gmCrossTab.add_RoundOff("WIP","0");
	//gmCrossTab.add_RoundOff("Initiated","0");
	//gmCrossTab.add_RoundOff("Inhouse Loaner","0");
	//gmCrossTab.add_RoundOff("Product Loaner","0");
	gmCrossTab.add_RoundOff("Owe","0");
	
	
	String strAction = (String)request.getAttribute("hAction");
	strAction = (strAction == null)?"":strAction;
	
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD"); 
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addStyle("Shipped","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("Pending Shipping","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("Ready To Consign","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("Pending Verification","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("WIP","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("Initiated","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("Inhouse Loaner","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Product Loaner","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Owe","ShadeLightGreenTD","ShadeDarkGreenTD") ;
	gmCrossTab.addStyle("Set PAR","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Yet To Build","ShadeLightYellowTD","ShadeMedYellowTD") ;	
	gmCrossTab.setColumnOverRideFlag(true);
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.addLine("Name");
	
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmSetOverviewDecorator");
	log.debug("hamap here is " +hmSetOverview);
	
	int currMonth = GmCalenderOperations.getCurrentMonth()-1;// To get the current month
	
	String strFromdaysDate = GmCalenderOperations.getFirstDayOfMonth(currMonth); //To get the first day of month
	String strTodaysDate = GmCalenderOperations.getLastDayOfMonth(currMonth);//To get the last day of month
	
	String strFromdayDate = GmCalenderOperations.getFirstDayOfMonth(-6);
	String strTodayDate = GmCalenderOperations.getLastDayOfMonth(-1);
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Status -- Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252"> 
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmSetOverviewReport.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
	var fromDaysDate = '<%=strFromdaysDate%>';
	var todaysDate = '<%=strTodaysDate%>';
	var fromDayDate = '<%=strFromdayDate%>';
	var todayDate = '<%=strTodayDate%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  >
<html:form action="/gmLogisticsReport.do?method=loadSetOverview"  >  
<html:hidden property="strOpt" value=""/>
<html:hidden property="hExcel" value="" />
<html:hidden property="hTxnId" value=""/>
<input type="hidden" name="hAction" value="">

<jsp:include page="/common/GmScreenError.jsp" />

<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
  
		  
                 
	<tr class=Line >
	<td height=25 class=RightDashBoardHeader colspan=5><fmtSetOverviewReport:message key="LBL_SET_OVERVIEW"/> </td>
			
			<td align="right" class=RightDashBoardHeader >
			<fmtSetOverviewReport:message key="IMG_ALT_HELP" var = "varHelp"/> 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title="${varHelp}" width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
	</tr>
	<tr class=Line><td noWrap height=1 colspan=6></td></tr>
					 
					 
					<tr>
						 <td bgcolor="gainsboro"> 
									  
		     						</td>
                    	<td class="RightTableCaption" align="right" width="50"  HEIGHT="24" ></font>&nbsp;<fmtSetOverviewReport:message key="LBL_SET"/>:</td> 
                        <td  width="500">&nbsp; &nbsp;
                        <DIV style="display:visible;height: 150px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                        <table> <tr><td bgcolor="gainsboro" > <html:checkbox property="selectAll" onclick="fnSelectAll('toggle');" /><B><fmtSetOverviewReport:message key="LBL_SELECT_ALL"/> </B> </td>
							</tr></table> 
                        
                        <table >
                        	<logic:iterate id="SelectedSetlist" name="frmLogisticsReport" property="alSets">
                        	<tr>
                        		<td>
							    	<htmlel:multibox property="checkSelectedSets" value="${SelectedSetlist.ID}" />
								    <bean:write name="SelectedSetlist" property="IDNAME" />
								</td>
							</tr>	    
							</logic:iterate>
						</table>
						</DIV><BR>
                        </td>
                        
                   
					 	
					 		
						<td><table>
						<tr>
						<!-- Struts tag lib code modified for JBOSS migration changes -->
							<td class="RightTableCaption" align="left"  HEIGHT="24" ><fmtSetOverviewReport:message key="LBL_FORECAST_MONTHS"/>: 
							 &nbsp;<html:text property="forecastPeriod"  size="5" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
							 
							</td>
			    		   
						 
						</tr>
						<tr>
							<td > &nbsp;&nbsp;&nbsp;
							<fmtSetOverviewReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" onClick="return fnGo();"  gmClass="button" buttonType="Load" /></td><br>				
							</tr>
										
						</table></td>
						
						 
					</tr>				
	
 	<tr><td colspan="6" class="ELine"></td></tr>
 	
 	<tr><td colspan="6" class="ELine"></td></tr>
 	</table>
 	
 	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
 	<tr>
							<td colspan ="6" align=center> <%=gmCrossTab.PrintCrossTabReport(hmSetOverview) %></td>
							
	</tr>
	</table>
<%if(strAction.equals("GO")){ %>

<table >
<tr></tr><br><br>
							<tr>
						<td width="100%" align="center">  											
								<fmtSetOverviewReport:message key="BTN_EXCEL_DOWNLOAD" var="varExcelDownload"/><gmjsp:button  value="${varExcelDownload}" gmClass="button" 
								onClick="javascript:fnDownloadVer();" buttonType="Load" />	
						</td>						
						</tr>	  
										
						</table>
												
<%}%>
 			 
<BR><br>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
