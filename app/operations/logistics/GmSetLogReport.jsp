<%
/**********************************************************************************
 * File		 		: GmSetLogReport.jsp
 * Desc		 		: This report will show when and who moved Sets to Inhouse/Product Loaner
 * Version	 		: 1.1
 * author			: Brinal
 * 
************************************************************************************/
%><%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.Date"%>
<%@ page buffer="16kb" autoFlush="true" %>

<%@ taglib prefix="fmtSetLogReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- operations\logistics\GmSetLogReport.jsp -->

<fmtSetLogReport:setLocale value="<%=strLocale%>"/>
<fmtSetLogReport:setBundle basename="properties.labels.operations.logistics.GmSetLogReport"/>


<bean:define id="strFromDt" name="frmSetLogReport" property="frmDate" type="java.lang.String"> </bean:define>
<bean:define id="strToDt" name="frmSetLogReport" property="toDate" type="java.lang.String"> </bean:define>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("SET_LOG_REPORT");
String strApplDateFmt = strGCompDateFmt;
String strDTDateFmt = "{0,date,"+strApplDateFmt+"}";
Date dtFrmDate = null;
Date dtToDate = null;

dtFrmDate = GmCommonClass.getStringToDate(strFromDt, strApplDateFmt);
dtToDate = GmCommonClass.getStringToDate(strToDt, strApplDateFmt);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Set Log Report</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmSetLogReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script>
var format = '<%=strApplDateFmt%>';
</script>

</head>
<body leftmargin="20" topmargin="10" >
<html:form method="post" action="/gmSetLogReport.do">
<html:hidden property="haction" name="frmSetLogReport" value=""/>

	<table border="0" class="DtTable700"  cellspacing="0" cellpadding="0" >	
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="5"><fmtSetLogReport:message key="LBL_SET_LOG_REPORT"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtSetLogReport:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		<tr><td colspan="6" height="1" class="LLine"></td></tr>	
		<tr>
			<td HEIGHT="23"class="RightTableCaption" align="right"><fmtSetLogReport:message key="LBL_SET_ID"/>:</td>
			<!-- Struts tag lib code modified for JBOSS migration changes -->
			<td>&nbsp;
				<html:text property="setId" name="frmSetLogReport" size="10" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
				<a href= javascript:fnSetList();><img src="<%=strImagePath%>/action-detail.gif" style="border: none;" height="14"></a>					 		
			</td>			
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td HEIGHT="23" class="RightTableCaption" align="Right"><fmtSetLogReport:message key="LBL_TYPE"/>:			
				 <gmjsp:dropdown controlName="type"  SFFormName='frmSetLogReport' SFSeletedValue="type"  defaultValue= "[Choose One]"	
				SFValue="alType" codeId="CODENMALT" codeName="CODENM" />  
			</td>			
		</tr>
		<tr><td colspan="6" height="1" class="LLine"></td></tr>	
		<tr  class="Shade">
			<td HEIGHT="23" class="RightTableCaption" align="right">&nbsp;<fmtSetLogReport:message key="LBL_DATE_MOVED"/>:</td>
			<td colspan="4">&nbsp;
			<fmtSetLogReport:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar textControlName="frmDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			&nbsp;<fmtSetLogReport:message key="LBL_TO"/>:&nbsp;<gmjsp:calendar textControlName="toDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td><fmtSetLogReport:message key="BTN_LOAD" var="varLoad"/>
			<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" onClick="javascript:fnReload();" tabindex="" gmClass="button" buttonType="Load" /></td>
		</tr>	
		<tr><td class="LLine" height="1" colspan="6"></td></tr>		
		<tr>
			<td colspan="6">
				 <display:table name="requestScope.frmSetLogReport.lresult" class="its" id="currentRowObject"  export="true" freezeHeader="true" requestURI="/gmSetLogReport.do">
				 	<display:setProperty name="export.excel.filename" value="SetLogReport.xls" /> 
				    <fmtSetLogReport:message key="DT_CN" var="varCN"/>
				    <display:column property="CNID" title="${varCN}" sortable="true" style="width:110"/>
				    <fmtSetLogReport:message key="DT_SET_ID" var="varSetID"/>
				    <display:column property="SETID" title="${varSetID}" sortable="true" />
				    <fmtSetLogReport:message key="DT_SET_NAME" var="varSetName"/>
				    <display:column property="SETNAME" title="${varSetName}" sortable="true" />
				    <fmtSetLogReport:message key="DT_MOVED_BY" var="varMovedBy"/>
				    <display:column property="CRBY" title="${varMovedBy}" sortable="true" style="width:110"/>
				    <fmtSetLogReport:message key="DT_DATE_MOVED" var="varDateMoved"/>
				    <display:column property="CRDATE" title="${varDateMoved}" sortable="true" style="width:110" format="<%=strDTDateFmt%>" />	<!-- Converting date in query itself -->			  	   
				    <fmtSetLogReport:message key="DT_TYPE" var="varType"/>
				    <display:column property="TYPE" title="${varType}" sortable="true" style="width:110"/>				    	 
				 </display:table> 
			</td>
		</tr>	
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</html>