<%@ include file="/common/GmHeader.inc" %> 
 <!-- \operations\logistics\GmIncludePriority.jsp -->
<%
String strConsignId = GmCommonClass.parseNull((String)request.getParameter("CONSIGNID"));
%>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
window.onload=fnMsgLoad(); 
function fnMsgLoad(){
	fnStartProgress('Y');
	var consignId = '<%=strConsignId%>';
	var priority = '';
	var priorityId = '';
	var priorityStr = '';
	var requestid = '';
	var prArray = new Array();
	//103802: Critical; 103803: High; 103804: Medium; 103805: Low
	prArray[103802] = "#CC3399";
	prArray[103803] = "Green";
	prArray[103804] = "Orange";
	prArray[103805] = "Black";
	prArray[103807] = "Black";
	var ajaxUrl = fnAjaxURLAppendCmpInfo('/gmSetPrioritization.do?method=fetchSetPriority&consignmentId='+consignId+ '&ramdomId=' + Math.random());
	dhtmlxAjax.get(ajaxUrl,function(loader){
		var response = loader.xmlDoc.responseText;
		if(response != ''){
			priorityId = response.substring(0,response.indexOf('|'));
			priorityStr = response.substring(response.indexOf('|')+1,response.length);
			priority = priorityStr.substring(0,priorityStr.indexOf('|'));
			priority = priority.fontcolor(prArray[priorityId]);
			requestid = priorityStr.substring(priorityStr.indexOf('|')+1,priorityStr.length);
			document.getElementById("priMsg").innerHTML = "<font size=3/><b>"+priority+"</b></font>";
			document.getElementById("reqId").innerHTML = requestid;
		}
	});
	fnStopProgress('Y');
}
</script>
<BODY leftmargin="20" topmargin="10" onload="fnMsgLoad()">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr><td class="RightCaption"> <div id="priMsg"></div> <div id="reqId" style="display: none;"></div> </td></tr>
</table>
</BODY>
