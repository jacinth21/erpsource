 <%
/**********************************************************************************
 * File		 		: GmLoanerReqEdit.jsp
 * Desc		 		: This screen is used for editing request data.
 * Version	 		: 1.0 
 * author			: Ritesh Shah
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib prefix="fmtIncidentRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

 <!-- \operations\logistics\GmIncidentReport.jsp -->
<fmtIncidentRpt:setLocale value="<%=strLocale%>"/>
<fmtIncidentRpt:setBundle basename="properties.labels.operations.logistics.GmIncidentReport"/>

<bean:define id="strFromDt" name="frmOprIncident" property="fromDate" type="java.lang.String"> </bean:define>
<bean:define id="strToDt" name="frmOprIncident" property="toDate" type="java.lang.String"> </bean:define>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("INCIDENT_RPT");
String strApplDateFmt = strGCompDateFmt;
HashMap hmReturn = new HashMap();
GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));

Date dtFrmDate = null;
Date dtToDate = null;

dtFrmDate = GmCommonClass.getStringToDate(strFromDt, strApplDateFmt);
dtToDate = GmCommonClass.getStringToDate(strToDt, strApplDateFmt);

gmCrossTab.setDecorator("com.globus.crosstab.beans.GmIncidentDecorator");
gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
gmCrossTab.setColumnDivider(true);
gmCrossTab.setFixedHeader(true);
gmCrossTab.setRoundOffAllColumnsPrecision("0");
gmCrossTab.displayZeros(true);
gmCrossTab.setTotalRequired(true);
gmCrossTab.setColumnTotalRequired(true);
gmCrossTab.setNoDivRequired(true);
gmCrossTab.setRowHighlightRequired(true);
gmCrossTab.setColumnLineStyle("borderDark");

gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
gmCrossTab.addStyle("Bio Hazard","ShadeMedBrownTD","ShadeDarkBrownTD") ;
gmCrossTab.addStyle("Damage","ShadeMedBrownTD","ShadeDarkBrownTD") ;
gmCrossTab.addStyle("Late Return","ShadeMedBrownTD","ShadeDarkBrownTD") ;
gmCrossTab.addStyle("No Usage","ShadeMedBrownTD","ShadeDarkBrownTD") ;
gmCrossTab.addStyle("Excess","ShadeMedBrownTD","ShadeDarkBrownTD") ;
gmCrossTab.addStyle("Total","ShadeLightGreenTD","ShadeDarkGreenTD") ;
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Incident Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script> <!--BUG-12108 -->
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmIncidentReport.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
	var format = '<%=strApplDateFmt%>';
</script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmOprIncident.do">
<html:hidden property="strOpt" />
<html:hidden property="incType" />
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtIncidentRpt:message key="TD_LOAN_INCIDENT_HEADER"/></td>
			<fmtIncidentRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="25" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
                    	<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right"></font>&nbsp;<fmtIncidentRpt:message key="LBL_DIST"/>:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<!-- Custom tag lib code modified for JBOSS migration changes -->
									<td>&nbsp;
										<gmjsp:dropdown controlName="repId" SFFormName="frmOprIncident" SFSeletedValue="repId"
											SFValue="alRepList" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" />													
									</td>								
									<td class="RightTableCaption" align="right">&nbsp;<fmtIncidentRpt:message key="LBL_RPT_BY"/>:</td>
									<td width="150px">&nbsp;
										<gmjsp:dropdown controlName="reportBy" SFFormName="frmOprIncident" SFSeletedValue="reportBy"
											SFValue="alRptByList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">
                    	<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right">&nbsp;<fmtIncidentRpt:message key="LBL_FROM_DATE"/>:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<!-- Struts tag lib code modified for JBOSS migration changes -->
									<td>&nbsp;<gmjsp:calendar textControlName="fromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
									 		&nbsp;&nbsp;  
									</td>								
									<td class="RightTableCaption" align="right">&nbsp;<fmtIncidentRpt:message key="LBL_TO_DATE"/>:</td>
									<td width="150px" >&nbsp;<gmjsp:calendar textControlName="toDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtToDate.getTime())%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			                    		&nbsp;&nbsp;                                      
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr>
					<table border="0">
						<tr>
                      		<td width="2%">&nbsp;
						    	<html:checkbox property="selectAll" onclick="fnSelectAll('toggle');" /><B><fmtIncidentRpt:message key="LBL_INCIDENTS"/></B> 
    						</td>    						    						
    					</tr>
						<tr>
							<table border="0">
								<tr>
									<td width="2%">							
										<div style="display: visible; height: 100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
											<table>
												<logic:iterate id="alIncidentList" name="frmOprIncident" property="alIncidentList">
													<tr>
														<td>
															<htmlel:multibox property="selectedIncidents" value="${alIncidentList.CODEID}" />
															<bean:write name="alIncidentList" property="CODENM" />
														</td>
													</tr>
												</logic:iterate>
											</table>
										</div>
										<br/>
									</td>
									<fmtIncidentRpt:message key="BTN_GO" var="varGO"/>					           	
							       	<td width="2%" align="left" valign="top" HEIGHT="25">&nbsp;
										<gmjsp:button value="&nbsp;&nbsp;${varGO}&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnSubmit();" buttonType="Load" />						
									</td>							       	
						       	</tr>
					       	</table>
						</tr>
					<tr><td colspan="3" class="LLine" height="1"></td></tr>	
					<tr>
						<td colspan="3" align="right"> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
					</tr>	                    				
   				</table>
  			   </td>
  		  </tr>	
    </table>
</td>
</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>



