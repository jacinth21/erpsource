
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
	/*******************************************************************************
	 * File		 		: GmLoanerDetailList.jsp
	 * Desc		 		: This screen is used for Loading the Loaner Detail List Paperwork for japan.
	 * Version	 		: 1.0
	 * author			: Ckumar
	 ********************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmJasperReport"%>
<%@ page import="java.util.HashMap,java.util.*,java.text.*"%>
 <!-- \logistics\GmLoanerDetailList.jsp --> 
<%
 
String strHtmlJasperRpt = "";
HashMap hmReturn  = new HashMap();
HashMap hmCompanyAddress = new HashMap();
ArrayList alResult = new ArrayList();
hmReturn = GmCommonClass.parseNullHashMap((HashMap)(HashMap)request.getAttribute("hmCONSIGNMENT"));
ArrayList alSetLoad = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSIGNSETDETAILS"));
if(alSetLoad.size()!=0){
Iterator itrMap = alSetLoad.iterator();
while (itrMap.hasNext()) {
  HashMap hmResult = (HashMap) itrMap.next();
  int strQty = Integer.parseInt(GmCommonClass.parseNull((String) hmResult.get("QTY")));
for(int i=1;i<=strQty;i++){
  alResult.add(hmResult);
  }
}
}
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
//PMT-53539 japan-new-warehouse-address

GmResourceBundleBean gmPaperworkResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
hmCompanyAddress = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmCompanyAddress"));
hmReturn.put("GMCOMPANYNAME",GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME")));
//FLR_ADDRESS,3FLR_PHONE,3FLR_FAX only for loaner and consignment(loan detail sheet) 
hmReturn.put("3FLRADDRESS",GmCommonClass.parseNull(gmResourceBundleBean.getProperty("3FLR_ADDRESS")));
hmReturn.put("3FLRPHONE",GmCommonClass.parseNull(gmResourceBundleBean.getProperty("3FLR_PHONE")));
hmReturn.put("3FLRFAX",GmCommonClass.parseNull(gmResourceBundleBean.getProperty("3FLR_FAX")));
String strCssPaths = GmCommonClass.getString("GMSTYLES");
String strHideButton = GmCommonClass.parseNull(request.getParameter("hHideButton"));
String strBarCodeFlag=GmCommonClass.parseNull((String)request.getAttribute("strBarCodeFlag"));

String strSetRbObjectFl =
    GmCommonClass.parseNull(gmPaperworkResourceBundleBean.getProperty("ADD_RESOURCE_BUNDLE_OBJ"));
%>
<HTML>
<HEAD>
<script>
function fnPrint()
{
    window.print();
}
var tdinnner = "";
var strObject="";
function hidePrint()
{
	strObject = eval("document.all.button_table");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button_table");
	strObject.innerHTML = tdinnner ;
}

</script>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPaths%>/Globus.css"/>
<link rel="stylesheet" href="<%=strCssPaths%>/print.css" type="text/css" media="print" />
</HEAD>
<BODY leftmargin="30" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();"  >
<%if(!strHideButton.equals("YES")){ %>
	<div id="button_table"  width="800px">
		<table width="700px">
			<tr align="center">
			<td align="center" id="button">
				<gmjsp:button value="Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />
		 		<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />
		 	</td>
		    </tr>
		    	 
		    </table>
	</div>
<%} %>
	<table >
	<tr>		
    	<td>
    			<%if(strBarCodeFlag.equals("YES")){ %>
    			<div align="center">
    			<% }else{ %>
    			<div>
    			<%} %>
					<%			
							GmJasperReport gmJasperReport = new GmJasperReport();
							gmJasperReport.setRequest(request);
							gmJasperReport.setResponse(response);
							gmJasperReport.setJasperReportName("/GmLoanerDetailList.jasper");
							gmJasperReport.setHmReportParameters(hmReturn);
							gmJasperReport.setReportDataList(alResult);
							gmJasperReport.setBlDisplayImage(true);
							if (strSetRbObjectFl.equalsIgnoreCase("YES") ) {
						          gmJasperReport.setRbPaperwork(gmPaperworkResourceBundleBean.getBundle());//PMT-53539 japan-new-warehouse-address
						        }
							strHtmlJasperRpt = gmJasperReport.getXHtmlReport();
					%>
					<%=strHtmlJasperRpt %>
					
				</div>
				<%if(strBarCodeFlag.equals("YES")){ %>
					<p STYLE="page-break-after: always"></p>
					<div align="center">
						<jsp:include page="/operations/logistics/GmLoanerPackSlipJapan.jsp"></jsp:include>
					</div>
					
				<% }%>
			
				
				</td>
		</tr>
	</table>
	<div>
				
				</div>
<%@ include file="/common/GmFooter.inc"%>	 
</BODY>
</HTML>