
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="java.util.HashMap,java.text.*"%>
<%@ page import ="java.util.ArrayList"%>
<%@ page import="java.util.Date"%>

<%@ taglib prefix="fmtPrintLoanerPaperWork" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmPrintLoanerPaperWork.jsp -->

<fmtPrintLoanerPaperWork:setLocale value="<%=strLocale%>"/>
<fmtPrintLoanerPaperWork:setBundle basename="properties.labels.operations.logistics.GmPrintLoanerPaperWork"/>


<%	
	
	HashMap hmCsg = new HashMap();
		
	// To get the Consignment information 
	hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
	if (hmCsg == null)
	{
		hmCsg = (HashMap)session.getAttribute("hmCONSIGNMENT");
	}
    String strConsignId = GmCommonClass.parseNull((String)hmCsg.get("CONSIGNID"));
	String strSetName = GmCommonClass.parseNull((String)hmCsg.get("SETNAME"));
	String strSetType = GmCommonClass.parseNull((String)hmCsg.get("SETTYPE"));
	String strEtchId = GmCommonClass.parseNull((String)hmCsg.get("ETCHID"));
	strEtchId = strEtchId.equals("")?"LOANER-":strEtchId;
	String strEtchStatus = GmCommonClass.parseNull(request.getParameter("ETCHSTATUS"));
	String strLoanStatus = GmCommonClass.parseNull((String)hmCsg.get("LOANSFL"));
	String strBillNm = GmCommonClass.parseNull((String)hmCsg.get("BILLNM"));
	Date dtLDate = null; 
	Date dtEDate = null; 
	dtLDate = (Date)hmCsg.get("LDATE");
	dtEDate = (Date)hmCsg.get("EDATE");
	String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strPrintedDate = "";
	strPrintedDate = strApplnDateFmt + " K:m:s a";
	String strType = GmCommonClass.parseNull((String)hmCsg.get("CTYPE"));
	String strLoanTransId = GmCommonClass.parseNull((String)hmCsg.get("LOANTRANSID"));
	String strSetid = GmCommonClass.parseNull((String)hmCsg.get("SETID"));
	SimpleDateFormat df = new SimpleDateFormat(strPrintedDate);
	String strDate = (String)df.format(new java.util.Date());
	String strUserName = (String)session.getAttribute("strSessShName");
	String strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
	ArrayList alResult = (ArrayList)request.getAttribute("alResult");
	int intSize = alResult.size();
	String strLog = GmCommonClass.parseNull((String)request.getAttribute("COMMENTS"));
	String strLogComments = GmCommonClass.parseNull((String)hmCsg.get("LOGCOMMENTS"));
	strLog = strLogComments.trim() + "\n" +strLog.trim();   //Append the comments 
	String heading = strType.equals("4119")?"In-House Loaner Paperwork":"Loaner Paperwork";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Print Loaner PaperWork </TITLE>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>
var hopt = '<%=strOpt%>';
var type = '<%=strType%>'
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
function fnWPrint()
{
	if(hopt != 'manually'){
		setTimeout(fnAutoPrint,1);
	}
}
function fnAutoPrint(){
	var OLECMDID = 6;
	var WebBrowser = '<OBJECT ID="WebBrowser1" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>';
	document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
	WebBrowser1.ExecWB(OLECMDID, 2);
	WebBrowser1.outerHTML = "";
} 
function fnPrintVer(consignid){
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+consignid+"&hType="+type+"&hOpt="+hopt,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
</script>
</head>
<BODY  leftmargin="20" topmargin="0" bottommargin="0" rightmargin="0" onbeforeprint="hidePrint();" onafterprint="showPrint();" onLoad="fnWPrint();">

<input type = "hidden" name="hConsignId" value="<%=strConsignId%>">
<input type = "hidden" name="hType" value="<%=strType%>">
<input type = "hidden" name="hLoanTransId" value="<%=strLoanTransId%>">
<div id="button">
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" colspan="5">
			<fmtPrintLoanerPaperWork:message key="BTN_PRINT" var="varPrint"/>			
			<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
			<fmtPrintLoanerPaperWork:message key="BTN_CLOSE" var="varClose"/>	
			<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />&nbsp;
			</td>
		<tr>
	</table>
</div>	
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tr>
			<td bgcolor="#666666" rowspan="40" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td bgcolor="#666666" colspan="5" height="1"></td>
			<td bgcolor="#666666" rowspan="40" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="150"><img src="<%=strImagePath%>/s_logo.gif" width="138" height="60"></td>
			<td class="RightText" width="110" colspan="2">&nbsp;</td>
			<td align="right" class="RightText" colspan="2"><font size=+2><%=heading%></font>&nbsp;<br><img src='/GmCommonBarCodeServlet?ID=<%=strConsignId%>$50182' height="50" width="150" />&nbsp;</td>
		</tr>
		<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
	<tr>
		<td class="RightTableCaption" HEIGHT="23" align="right" >&nbsp;<fmtPrintLoanerPaperWork:message key="LBL_SET_ID"/>:</td>
		<td width=20%>&nbsp;<%=strSetid%></td>
		<td class="RightTableCaption" HEIGHT="23" align="right" width=15%>&nbsp;<fmtPrintLoanerPaperWork:message key="LBL_SET_NAME"/>:</td>
		<td colspan="2">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
		
	</tr>
	<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
	<tr>
		<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtPrintLoanerPaperWork:message key="LBL_ETCH_ID"/>:</td>
		<td>&nbsp;<%=strEtchId%></td>
		<td class="RightTableCaption" align="right"><fmtPrintLoanerPaperWork:message key="LBL_CONSIGNMENT_ID"/>:</td>
		<td colspan="2">&nbsp;<a class="RightText" href="javascript:fnPrintVer('<%=strConsignId%>');"><%=strConsignId%></a></td>				
	</tr>
	<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
	<tr>
		<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtPrintLoanerPaperWork:message key="LBL_LOAN_DATE"/>:</td>
		<td>&nbsp;<%=GmCommonClass.getStringFromDate(dtLDate,strApplnDateFmt)%></td>
		<td class="RightTableCaption" align="right" rowspan="2">&nbsp;<fmtPrintLoanerPaperWork:message key="LBL_RETURNED_BY"/>:</td>
		<td colspan="2" rowspan="2"><%=strBillNm%></td>
	</tr>
	<tr>
		<td class="RightTableCaption" align="right">&nbsp;<fmtPrintLoanerPaperWork:message key="LBL_EXPECTED_DATE"/>:</td>
		<td>&nbsp;<%=GmCommonClass.getStringFromDate(dtEDate,strApplnDateFmt)%></td>
	</tr>	
	<tr><td  colspan="5"height="1" class="Line"></td></tr>
	<tr>
		<td class="RightTableCaption" height="30" align="left">&nbsp; <img src=<%=strImagePath%>/n.gif>&nbsp; <fmtPrintLoanerPaperWork:message key="LBL_NOTHING_MISSING"/> </td>		
		<td class="RightTableCaption" height="30" align="left">&nbsp; <img src=<%=strImagePath%>/n.gif>&nbsp; <fmtPrintLoanerPaperWork:message key="LBL_INSTRUMENTS_OK"/> </td>
		<td class="RightTableCaption" height="30" align="left">&nbsp; <img src=<%=strImagePath%>/n.gif>&nbsp; <fmtPrintLoanerPaperWork:message key="LBL_IMPLANTS_OK"/> </td>
		<td class="RightTableCaption" height="30" align="left">&nbsp; <img src=<%=strImagePath%>/n.gif>&nbsp; <fmtPrintLoanerPaperWork:message key="LBL_STRAPPED"/></td>
		<td class="RightTableCaption" height="30" align="left">&nbsp; <img src=<%=strImagePath%>/n.gif>&nbsp; <fmtPrintLoanerPaperWork:message key="LBL_DROP-OFF_AFTER_HOURS"/></td>	
	</tr>
	<tr>
			<td class="RightTableCaption" align="left"  height="30" >&nbsp;<fmtPrintLoanerPaperWork:message key="LBL_GM-LN-"/>_____________</td>
			<td class="RightTableCaption" align="left"  height="30" > <fmtPrintLoanerPaperWork:message key="LBL_GM-LN-"/>_____________</td>
			<td class="RightTableCaption" align="left"  height="30" colspan="2" > <fmtPrintLoanerPaperWork:message key="LBL_GM-LN-"/>_____________</td>
			<td class="RightTableCaption" align="left"  height="30" > <fmtPrintLoanerPaperWork:message key="LBL_GM-LN-"/>_____________</td>
			
	</tr>
	<tr><td bgcolor="#666666" colspan="5"></td></tr>
	<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="5">&nbsp;<b><fmtPrintLoanerPaperWork:message key="LBL_COMMENTS"/>:</b></td></tr>
	<tr><td bgcolor="#666666" colspan="5"></td></tr>
	<tr><td  colspan="5" height="145" valign="top"><%=strLog%></td></tr>
	<tr><td bgcolor="#666666" colspan="5"></td></tr>			
	<tr>				
	<td HEIGHT="23" colspan="5"> <fmtPrintLoanerPaperWork:message key="LBL_PRINTED_AT"/>: <%=strDate%>&nbsp;&nbsp; <fmtPrintLoanerPaperWork:message key="LBL_BY"/>:&nbsp;<%=strUserName%><BR> </td>
	</tr>
	<tr><td bgcolor="#666666" colspan="5"></td></tr>
	<% if(intSize > 0){ %>
	<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="5">&nbsp;<b><fmtPrintLoanerPaperWork:message key="LBL_BACK_ORDER_DETAILS"/>:</b></td></tr>
	<tr><td bgcolor="#666666" colspan="5"></td></tr>
	<%}%>
	</table>
	<table border="0" width=100% cellspacing="0" cellpadding="0">
	<tr>
			<td rowspan="2" colspan="1" height="282" valign="top">
			<%if(intSize > 0){%>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<jsp:include
					page="/operations/logistics/GmIncludeLoanerBackOrderParts.jsp"/>
			
			<%} %>
			</td>
			<td colspan="1" valign="top" height="313" style="vertical-align:top; "> 
			<% if(intSize > 20){ %>
			<table width="100%" cellspacing="0" cellpadding="0"  bordercolor="#666666" style="border:thin;" height="100%" border="0">
			<tr><td class="RightText">&nbsp;&nbsp;<fmtPrintLoanerPaperWork:message key="LBL_NOTE_BACK_ORDER_PARTS"/> </td>
			</tr></table>
			<%}%></td></tr>
			<tr>
			<td valign="bottom" align="right">
		 			<jsp:include page="/operations/logistics/GmIncludeLoanerTagPaperwork.jsp"/>
	  		</td>
	  		<tr>
	</tr>	
	</table>
				
</BODY>
</HTML>
