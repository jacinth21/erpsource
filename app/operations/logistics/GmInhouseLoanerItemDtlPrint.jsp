 <%
/**********************************************************************************
 * File		 		: GmInhouseLoanerItemPrint.jsp
 * Desc		 		: This screen is used for the
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>
 <!-- \operations\logistics\GmInhouseLoanerItemDtlPrint.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %> 
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmJasperReport"%>
<%@ page import="java.util.Date"%>

<%
String strAction = "";
String strReturnId = "";
String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
Date dtLDate = null; 
Date dtEDate = null;
HashMap hmResult = new HashMap();
HashMap hmDetails = new HashMap();
String strHtmlJasperRpt = "";

//GmJasperReport gmJasperReport = new GmJasperReport();
hmResult = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMRESULT"));
strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
strReturnId = GmCommonClass.parseNull((String)request.getAttribute("hReturnId"));
 
dtLDate = (Date)hmResult.get("LDATE");
dtEDate = (Date)hmResult.get("EDATE"); 
String strLnrDate = GmCommonClass.getStringFromDate(dtLDate,strApplnDateFmt);
String strEndDate = GmCommonClass.getStringFromDate(dtEDate,strApplnDateFmt);
String strCompanyId = GmCommonClass.parseNull((String)hmResult.get("COMPANYID"));
String strCompanyName = GmCommonClass.parseNull((String)GmCommonClass.getString(strCompanyId+"_COMPNAME"));
String strCompanyAddress = GmCommonClass.parseNull((String)GmCommonClass.getString(strCompanyId+"_COMPADDRESS"));
String strCompanyPhone = GmCommonClass.parseNull((String)GmCommonClass.getString(strCompanyId+"_PH"));
String strCompanyFax = GmCommonClass.parseNull((String)GmCommonClass.getString("INHOUSE_LOANER_FAX"));
String strLogoImagePath = System.getProperty("ENV_PAPERWORKIMAGES");

hmDetails.put("LDATE",strLnrDate);
hmDetails.put("EDATE",strEndDate);
hmDetails.put("COMPID",strCompanyId);
hmDetails.put("COMPNAME",strCompanyName);
hmDetails.put("COMPADDRESS",strCompanyAddress);
hmDetails.put("COMPPHONE",strCompanyPhone);
hmDetails.put("COMPFAX",strCompanyFax);
hmDetails.put("LOGOIMAGE", strLogoImagePath+strCompanyId+".gif");
hmResult.put("SUBREPORT_DIR",request.getSession().getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))+"\\");
hmResult.put("VARIABLECURRENCY", "USD");
hmResult.put("SHIPCOST", "0");
hmResult.put("ADDWATERMARK", "");
hmResult.putAll(hmDetails);

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Letter</TITLE>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<form action="">
<div id="jasper" style=" max-height: 500px"> 
		<%
			String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("/GmInhouseLoanerItemPrint.jasper");
			gmJasperReport.setHmReportParameters(hmResult);							
			gmJasperReport.setReportDataList(null);				
			gmJasperReport.setBlDisplayImage(true);
			strHtmlJasperRpt = gmJasperReport.getHtmlReport();
		%>
		<%=strHtmlJasperRpt %>				
</div>
<!-- <p STYLE="page-break-after: always"></p> -->
</FORM>
</BODY>
</HTML>