<%
/**********************************************************************************
 * File		 		: GmIncludeLoanerBackOrderParts.jsp
 * Desc		 		: This screen is inclluded in the GmPrintLoanerPaperWork.jsp
 * Version	 		: 1.0
 * author			: Dhana Reddy
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtIncludeLoanerBackOrderParts" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!--operations\logistics\GmIncludeLoanerBackOrderParts.jsp-->

<fmtIncludeLoanerBackOrderParts:setLocale value="<%=strLocale%>"/>
<fmtIncludeLoanerBackOrderParts:setBundle basename="properties.labels.operations.logistics.GmIncludeLoanerBackOrderParts"/>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%ArrayList alResult = new ArrayList();
String strPartNm="";
String strQty ="";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Loaner Back order parts</title>
</head>
<body>
	     <table cellpadding="0" cellspacing="0" width="100%"  rules="groups" bordercolor="#666666" style="border:thin;">
	     		 <colgroup></colgroup>
	     		 <colgroup></colgroup>
			  		<thead>
			  			<TR style="background-color: #C0C0C0">
			  				<TH class="RightTableCaption"  align="left" ><fmtIncludeLoanerBackOrderParts:message key="LBL_PART_NUMBER"/></TH>
			  				<TH class="RightTableCaption"  align="left"><fmtIncludeLoanerBackOrderParts:message key="LBL_BO_QTY"/></TH>	
			 	   		</TR>
			  		</thead>
			  		<TBODY>
			  		<%alResult = (ArrayList)request.getAttribute("alResult");
			  		int intSize = 0;
					 HashMap hmResult = new HashMap();
					 for(int i=0;i<alResult.size(); i++){
						 if(intSize <20){
						 hmResult = (HashMap)alResult.get(i);
							 intSize++;
							 strPartNm = GmCommonClass.parseNull((String)hmResult.get("PNUM"));
							 strQty = GmCommonClass.parseNull((String)hmResult.get("BOQTY"));
					%>
				<tr>
					<td  align="left"> <%=strPartNm%></td><td  align="right"> <%=strQty%></td>
				</tr>
					<%
						 }
					 }
						%>
			  		</TBODY>
			  		</table>
</body>
</html>