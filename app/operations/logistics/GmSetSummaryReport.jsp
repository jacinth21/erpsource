 <%
/**********************************************************************************
 * File		 		: GmSalesReportYTDByAccountPer.jsp
 * Desc		 		: This screen is used to display Sales Rep By Account
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtSetSummaryReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\logistics\GmSetSummaryReport.jsp -->

<fmtSetSummaryReport:setLocale value="<%=strLocale%>"/>
<fmtSetSummaryReport:setBundle basename="properties.labels.operations.logistics.GmSetSummaryReport"/>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
try {

	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();
	ArrayList alProjList = new ArrayList();
	
	String strSelected = "";
	String strCodeID = "";
	String strProjNm = "";
	String strProjId = "";
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	gm.checkSession(response,session);


	String strHeader = (String)request.getAttribute("strHeader");
	String strhAction = (String)request.getAttribute("hAction");
	if (strhAction == null)
	{
		strhAction = (String)session.getAttribute("hAction");
	}
	
		
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Sales By Distributor By Month");
	
	gmCrossTab.setLinkType(strhAction);
	gmCrossTab.setLinkRequired(false);	// To Specify the link
	
	
	// To specify if its unit or valye 
	gmCrossTab.setValueType(0);
	
	// Add Lines
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.addLine("Name");
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(91);
	gmCrossTab.setNameWidth(350);
	gmCrossTab.setRowHeight(22);
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	// Page Body Style
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addStyle("Total","ShadeMedBlueTD","ShadeDarkBlueTD") ;
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Inhouse Consignment  #</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
var Cbo_Type ='<%=request.getAttribute("Cbo_Type")%>';
var servletPath ='<%=strServletPath%>';
var excelDownload = '<%=GmCommonClass.getWindowOperner(request,response)%>';
</script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmSetSummaryReport.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmOrder" method="post" action = "<%=strServletPath%>/GmSetSummaryReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">


<TABLE cellSpacing=0 cellPadding=0  border=0 width="900">
<TBODY>
	<tr>
		<td rowspan="11" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="11" class=Line noWrap width=1></td>
	</tr>
	<tr class=Line><td height=25 class=RightDashBoardHeader><%=strHeader %> </td></tr>
	<tr class=borderDark>
		<td noWrap height=1>  </td>
	</tr>
	<tr>
	<td class="RightText" HEIGHT="25">
		&nbsp;&nbsp;<fmtSetSummaryReport:message key="LBL_TYPE"/>
		<select name="Cbo_Type" id="Type" class="RightText" tabindex="10">
			 <option value="0" ><fmtSetSummaryReport:message key="LBL_ALL"/></option>
			 <option value="1" ><fmtSetSummaryReport:message key="LBL_AVAILABLE"/></option>
			 <option value="2" ><fmtSetSummaryReport:message key="LBL_SHIPPED_LOANER"/> </option>
		 </select>&nbsp;&nbsp;
		 <fmtSetSummaryReport:message key="LBL_GO" var="varGo"/>
		<gmjsp:button value="${varGo}" gmClass="button" 
		onClick="javascript:fnGo();" buttonType="Load" />	</td>
	</tr>		
	<tr>
		<td noWrap height=3>  </td>
	</tr>	
	<tr>
		<td align=center> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
	</tr>
</tbody>	
</TABLE> 
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
