<%
/**********************************************************************************
 * File		 		: GmLoanerExtension.jsp
 * Desc		 		: This screen is used to Loaner Extension and replenish In-House Sets
 * Version	 		: 1.0
 * author			: Angela
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.operations.shipping.forms.GmShipDetailsForm"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="java.util.HashMap"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtLoanerExtension" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<!-- operations\logistics\GmLoanerExtension.jsp -->

<fmtLoanerExtension:setLocale value="<%=strLocale%>"/>
<fmtLoanerExtension:setBundle basename="properties.labels.operations.logistics.GmLoanerExtension"/>



<%
	String strLogisticsJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_LOGISTICS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strShowEditPart = GmCommonClass.parseNull((String) request.getAttribute("SHOWEDITPART"));
	String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
	String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_EXTENSION");	
	
	HashMap hmCsg = (HashMap)request.getAttribute("hmCONSIGNMENT");
	String strDistId = GmCommonClass.parseNull((String)hmCsg.get("DISTID"));
	String strConsignmentType = GmCommonClass.parseNull((String)hmCsg.get("CTYPE"));
	
	String strApplDateFmt = GmCommonClass.parseNull((String) request.getAttribute("ApplDateFmt"));
	String strCurrDate = GmCommonClass.parseNull((String) request.getAttribute("CurrDate"));
	String strType = (String) request.getAttribute("TYPE");
	
	HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("hmReturn"));
	HashMap hmDefaultShipVal = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("DEFAULTSHIPMODECARRVAL"));
	
	
	String strShipCarr = "";
	String strShipMode = "";
	String strShipTo = "";
	 
	strShipCarr = GmCommonClass.parseNull((String)hmDefaultShipVal.get("SHIP_CARRIER"));
	strShipMode = GmCommonClass.parseNull((String) hmDefaultShipVal.get("SHIP_MODE")); 
	strShipTo = GmCommonClass.parseNull((String) hmDefaultShipVal.get("SHIP_TO"));
	String strSurgDtAccFl = GmCommonClass.parseNull((String) request.getAttribute("STRSURGERYACCESSFL"));
	request.setAttribute("STRCONSIGNMENTTYPE",strConsignmentType);
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strValidateSurgDate  = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LOANER_EXTN_SURG_DT_VALIDATION"));
	 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Extension </TITLE>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strLogisticsJsPath%>/GmLoanerExtension.js"></script>
</head>
<script>
var currDate = '<%= strCurrDate %>';
var currDateFmt = '<%= strApplDateFmt %>';
var type = '<%= strType %>';
var reasonType; 
var extDate;
var extComment;
var prevtr = 0;
var vshipMode = '<%=strShipMode%>';
var vshipCarrier = '<%=strShipCarr%>';
var vshipTo = '<%=strShipTo%>';
var distID = '<%=strDistId%>';
var surgDtFl = "<%=strSurgDtAccFl%>";
var vconsignmenttype = '<%=strConsignmentType%>';
var validateSurgDtFl = '<%=strValidateSurgDate%>';
</script>
<BODY leftmargin="20" topmargin="10" onload="setDefaultValues();fnonload();">
<FORM name="frmLoanerExtension" method="POST" action="<%=strServletPath%>/GmInHouseSetServlet">
<input type="hidden" name="hAction" value ="">
<input type="hidden" name="hInputStr" value ="">
<input type="hidden" name="hInputRetStr" value ="">
<input type="hidden" name="hInputQuarStr" value ="">
<input type="hidden" name="hInputRepStr" value ="">
<input type="hidden" name="hInputEditPart" value ="">
<input type="hidden" name="hHistoryFl" value ="">
<input type="hidden" name="Cbo_Action" value ="">

<table border="0" width="700"   class="DtTable850" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td rowspan="15" width="1" class="Line"></td>
					<td height="25" class="RightDashBoardHeader" colspan="2"><fmtLoanerExtension:message key="LBL_INHOUSE_LOANER_EXTENSION"/></td>
			  		<td height="25" class="RightDashBoardHeader" align="right"> 
			  		<fmtLoanerExtension:message key="IMG_ALT_HELP" var = "varHelp"/>
			  		 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'  onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		 		  	</td> 
					<td  height="1" width="1" class="Line"></td>
			  	</tr>
				<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<tr>
					<td colspan="3">
						<jsp:include page="/include/GmIncludeConsignment.jsp" />
						
						</td>
				</tr>
			</table>
				<jsp:include page="GmIncludeLESummary.jsp" />
							
				<jsp:include page="GmIncludeLEEdit.jsp" />
				
							
			<% if (!strShowEditPart.equalsIgnoreCase("NotShow")){ %>				
			   <jsp:include page="GmIncludeLEPartEdit.jsp" />
							
				<jsp:include page="/gmIncShipDetails.do" flush="true" >
				<jsp:param name="screenType" value="LoanerXtn" />
				<jsp:param name="hType" value="InhouseLoaner" />
					</jsp:include>					
			<%} %>						 
			<table border="0" width="700"  cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2" class="aligncenter" align="center" height="30" valign="middle">
						<fmtLoanerExtension:message key="BTN_SUBMIT" var="varSubmit"/>					
							<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_loanerExt" gmClass="button" onClick="fnSubmit();" buttonType="Save" />&nbsp;<BR><BR>
						</td>
					</tr>		
					<tr><td colspan="2" height="1" class="Line"></td></tr>
			</table>
		</td>	
	</tr>
</table>
		
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
