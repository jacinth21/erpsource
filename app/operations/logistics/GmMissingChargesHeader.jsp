 <%
/**********************************************************************************
 * File		 		: GmMissingChargesHeader.jsp
 * Desc		 		: This screen is used for Missing Charges Filter conditions
 * Version	 		: 1.1
 * author			: Harinadh Reddi N 
************************************************************************************/
%>
<%@page import="java.net.URLEncoder"%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page buffer="16kb" autoFlush="true" %>
<bean:define id="alDistList" name="frmGmMissingChargesDtl" property="alDistList" type="java.util.List"> </bean:define>
<bean:define id="alRepList" name="frmGmMissingChargesDtl" property="alRepList" type="java.util.ArrayList"> </bean:define>
<bean:define id="ldResult" name="frmGmMissingChargesDtl" property="ldResult" type="java.util.List"> </bean:define>

<%@ taglib prefix="fmtGmMissingChargesHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmMissingChargesHeader.jsp -->
<fmtGmMissingChargesHeader:setLocale value="<%=strLocale%>"/>
<fmtGmMissingChargesHeader:setBundle basename="properties.labels.operations.logistics.GmMissingChargesHeader"/>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("MISSING_CHARGES");
    GmCalenderOperations gmCal = new GmCalenderOperations();
    String strApplnDateFmt = strGCompDateFmt;
    gmCal.setTimeZone(strGCompTimeZone);
	String strTodaysDate = gmCal.getCurrentDate(strGCompDateFmt);
	ArrayList alReportList = new ArrayList();
	alReportList = GmCommonClass.parseNullArrayList((ArrayList) ldResult);
	String strChkTicket 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_TICKET"));
	String strChkRepname 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_REPNAME"));
	String strChkListPrice 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_LSTPRICE"));
	String strChkConsignment 	= GmCommonClass.parseNull((String)request.getAttribute("CHK_CONSIGNMENT"));
	String strChkLastUpdBy 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_LSTUPDBY"));
	String strChkLastUpdDt 		= GmCommonClass.parseNull((String)request.getAttribute("CHK_LSTUPDDT"));
	String strDtTablewidth = "1500";
	
	int intColumnCnt = 0;
	
	if(strChkTicket.equals("on")){
		intColumnCnt++;
	}
	if(strChkRepname.equals("on")){
		intColumnCnt++;
	}
	if(strChkListPrice.equals("on")){
		intColumnCnt++;
	}
	if(strChkConsignment.equals("on")){
		intColumnCnt++;
	}
	if(strChkLastUpdDt.equals("on")){
		intColumnCnt++;
	}
	if(strChkLastUpdBy.equals("on")){
		intColumnCnt++;
	}
		
	if(intColumnCnt <= 3 && intColumnCnt > 0){
		strDtTablewidth = "1600";
	}if(intColumnCnt >= 4){
		strDtTablewidth = "1650";
	}
	request.setAttribute("COLUMNCOUNT",intColumnCnt);
	
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<TITLE> Globus Medical: Charges Approval</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strOperationsJsPath%>/GmMissingChargesDetails.js"></script>
<script>
var todaysDate 	= '<%=strTodaysDate%>';
var format 		= '<%=strApplnDateFmt%>';
var arraySize   = '<%=alReportList.size()%>';
var strCompanyInfo = '<%=strCompanyInfo%>';
function fnDisHiddenField(){
	var form = document.getElementsByName('frmGmMissingChargesDtl')[0];
	//alert(form.length);
	for (var i=0;i<form.length;i++) {
		var obj = form.elements[i];
		var objName = obj.name;
		var objType = obj.type;
		var objValue = obj.value;
		//alert(objValue);
		if((objType == 'hidden') || (objType == 'select')){			
			obj.disabled = true;
		}
	}
	document.frmGmMissingChargesDtl.inputString.disabled = false;
	document.frmGmMissingChargesDtl.strOpt.disabled = false;
}
</script>
<BODY leftmargin="20" topmargin="10" onload="fnDisHiddenField();">
<html:form action="/gmMissingChargesDtlAction.do">
<html:hidden property="strOpt"/>
<html:hidden property="inputString"/>
 <table border="0" class="DtTable<%=strDtTablewidth%>" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" ><fmtGmMissingChargesHeader:message key="LBL_MISSING_CHARGES"/> </td>
			<td  height="25" class="RightDashBoardHeader" align="right" colspan="8">
			<fmtGmMissingChargesHeader:message key="IMG_HELP" var="varHelp"/>			
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('	<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="8"></td></tr>
		<tr>
			<td height="25" class="RightTableCaption" align="right" width="8%">&nbsp;<fmtGmMissingChargesHeader:message key="LBL_DISTRIBUTOR"/> :</td>
			<td>&nbsp;<gmjsp:dropdown controlName="distName"  SFFormName='frmGmMissingChargesDtl' SFSeletedValue="distName"  defaultValue= "[Choose One]"	
						SFValue="alDistList" codeId="ID" codeName="NAME" tabIndex="1" />  
		         			
			</td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td height="25" class="RightTableCaption" align="right" width=10%>&nbsp;<fmtGmMissingChargesHeader:message key="LBL_REP_NAME"/>:</td>
			<td colspan="2">&nbsp;<gmjsp:dropdown controlName="repName"  SFFormName='frmGmMissingChargesDtl' SFSeletedValue="repName"  defaultValue= "[Choose One]"	
						SFValue="alRepList" codeId="ID" codeName="NAME" tabIndex="2" /> 
			</td>
			<td height="25" class="RightTableCaption" align="right" width=15%>&nbsp;<fmtGmMissingChargesHeader:message key="LBL_REQUEST_ID"/>:</td>
			<td colspan="2">&nbsp;<html:text maxlength="50" property="requestID" size="20"   
							styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" /> 
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" height="25" align="right" width="8%"><fmtGmMissingChargesHeader:message key="LBL_DATE_RANGE"/>:</td>
			<td class="RightText">&nbsp;<fmtGmMissingChargesHeader:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar SFFormName="frmGmMissingChargesDtl"  controlName="fromDate" tabIndex="4"  gmClass="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp;To:&nbsp;<gmjsp:calendar SFFormName="frmGmMissingChargesDtl" controlName="toDate"  gmClass="InputArea" 
					onFocus="changeBgColor(this,'#AACCE8');"  tabIndex="5" onBlur="changeBgColor(this,'#ffffff');"/></td>
			<td height="25" class="RightTableCaption" align="right" width=10%>&nbsp;<fmtGmMissingChargesHeader:message key="LBL_RECON_COMMENTS"/>:</td>
			<td colspan="2">&nbsp;<gmjsp:dropdown controlName="reconCommnets"  SFFormName='frmGmMissingChargesDtl' SFSeletedValue="reconCommnets"  defaultValue= "[Choose One]"	
						SFValue="alCommnetsList" codeId="CODEID" codeName="CODENM" tabIndex="6" /> 
			</td>
			<td height="25" align="Right" width=15%>&nbsp;<fmtGmMissingChargesHeader:message key="BTN_LOAD" var="varLoad"/>
			<gmjsp:button value="&nbsp;${varLoad}&nbsp;" style="width: 4em;height: 23px" tabindex="14" name="Btn_Load" gmClass="button" onClick="fnLoad();" buttonType="Load" /></td>	
			<td></td>				
		</tr>
		<tr><td class="LLine" height="1" colspan="8"></td></tr>	
		<tr>
			<td height="25" colspan="8">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="13%"></td>
						<td colspan="7" class="RightTableCaption"><html:checkbox property="chk_ticket" tabindex="7" title="Show Ticket # column"></html:checkbox>&nbsp;<fmtGmMissingChargesHeader:message key="LBL_TICKET"/></td>
						<td class="RightTableCaption"><html:checkbox property="chk_repname" tabindex="8" title="Show Rep Name column"></html:checkbox>&nbsp;<fmtGmMissingChargesHeader:message key="LBL_REP_NAME"/></td>
						<td class="RightTableCaption"><html:checkbox property="chk_assocrepname" tabindex="8" title="Show Assoc Rep Name column"></html:checkbox>&nbsp;<fmtGmMissingChargesHeader:message key="LBL_ASSOC_REP"/></td>
						<td class="RightTableCaption"><html:checkbox property="chk_listprice" tabindex="9" title="Show List Price column"></html:checkbox>&nbsp;<fmtGmMissingChargesHeader:message key="LBL_LIST_PRICE"/></td>
						<td class="RightTableCaption"><html:checkbox property="chk_consignment" tabindex="10" title="Show Consignment # column"></html:checkbox>&nbsp;<fmtGmMissingChargesHeader:message key="LBL_CONSIGNMENT"/></td>
						<td class="RightTableCaption"><html:checkbox property="chk_lastupdby" tabindex="12" title="Show Last Updated By column"></html:checkbox>&nbsp;<fmtGmMissingChargesHeader:message key="LBL_LAST_UPDATED_BY"/></td>
						<td class="RightTableCaption"><html:checkbox property="chk_lastupddt" tabindex="13" title="Show Last Updated Date column"></html:checkbox>&nbsp;<fmtGmMissingChargesHeader:message key="LBL_LAST_UPDATED_DATE"/></td>
					</tr>
				</table>
			</td>	
		</tr>	
		<tr><td class="LLine" height="1" colspan="8"></td></tr>	
		<tr>
			<td colspan="8">	
				<jsp:include page="/include/GmMissingChargesDetails.jsp" >
				<jsp:param name="FORMNAME" value="frmGmMissingChargesDtl" />
				</jsp:include>
			</td>
		</tr>			
		</table>
		</html:form>
		<%@ include file="/common/GmFooter.inc"%>
		</BODY>
		</HTML>
		