
<!--\operations\GmSavedLotCodeStatus.jsp  -->


<%
/**********************************************************************************
 * File		 		: GmGenerateCtrlNum.jsp
 * Desc		 		: This JSP is for the section Generated Control Number and Saved Control Number
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtSavedLotCodeStatus" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSavedLotCodeStatus:setLocale value="<%=strLocale%>"/>
<fmtSavedLotCodeStatus:setBundle basename="properties.labels.operations.GmSavedLotCodeStatus"/>

<bean:define id="strGridData" name="frmDonorInfo" property="strGridData" type="java.lang.String" ></bean:define>
<bean:define id="strBtnDisable" name="frmDonorInfo" property="strBtnDisable" type="java.lang.String" ></bean:define>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: Donor Details </TITLE>

<script>
var objGridData	= '<%=strGridData%>';

</script>

<BODY leftmargin="20" topmargin="10" onload="onLotCodePageLoad()">
<html:hidden property="rejQty" name="frmDonorInfo"/>

	<table class="DtTable500" cellspacing="0" cellpadding="0">
		<tr>
			<td height="24" class="ShadeRightTableCaption" colspan="4">&nbsp;<b><fmtSavedLotCodeStatus:message key="LBL_SAVED_CONTROL_NUMBERS"/></b></td>
		</tr>
		<tr><td class="Line"></td></tr>
		
		<tr><td>
			<div id="dataGrid" height="300"></div>
		</td></tr>
			
		<tr>
			<td height="30px" align="center">
			<fmtSavedLotCodeStatus:message key="LBL_SUBMIT" var="varSubmit"/>
				<gmjsp:button name="Btn_Submit" value="${varSubmit}" gmClass="button" onClick="javascript:fnSubmitStatus(this);" disabled="<%=strBtnDisable%>" tabindex="10" buttonType="Save" />
			<fmtSavedLotCodeStatus:message key="LBL_CLOSE" var="varClose"/>
				<gmjsp:button name="Btn_Close" value="${varClose}" gmClass="button" onClick="javascript:fnClose(this);" tabindex="11" buttonType="Load" />
			</td>
		</tr>
	</table>
	

<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>