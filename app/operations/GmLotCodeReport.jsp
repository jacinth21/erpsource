<%
/**********************************************************************************
 * File		 		: GmLotCodeReport.jsp
 * Desc		 		: This Jsp is the main JSP which contains JSPs for different sections of Lot Code Report
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib prefix="fmtLotCode" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
  <!-- \operations\GmLotCodeReport.jsp--> 
<fmtLotCode:setLocale value="<%=strLocale%>"/>
<fmtLotCode:setBundle basename="properties.labels.operations.GmLotCodeReport"/>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("LOT_CODE_RPT");
String strScreen = GmCommonClass.parseNull((String)request.getAttribute("hscreen"));
//The following code added for passing the company info to the child js
String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: Lot Code Report </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/operations/GmLotCodeReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script>
var screen = '<%=strScreen%>';
var companyInfoObj = '<%=strCompanyInfo%>';
</script>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad()">
	<html:form action="/gmLotCodeRptAction.do?method=loadLotCodeProdInfo">
	<html:hidden property="strOpt" name="frmLotCodeRpt"/>
	<input type="hidden" name="hscreen"/>
	<input type="hidden" name="hdonorNumber"/>
		<table class="DtTable950" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="24" class="RightDashBoardHeader">&nbsp;<b><fmtLotCode:message key="TD_LOT_CODE_HEADER"/></b></td>
						<fmtLotCode:message key="IMG_ALT_HELP" var="varHelp"/>
						<td height="24" class="RightDashBoardHeader" align="right"><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<tr><td class="RightTableCaption" HEIGHT="30" align="right" width="20%">
							<fmtLotCode:message key="LBL_LOT_NUM" var="varLotNum"/>
							<gmjsp:label type="RegularText" SFLblControlName="${varLotNum}:" td="false" /></td>
							<td width="25%">&nbsp; <html:text property="lotNum" styleClass="InputArea" name="frmLotCodeRpt" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onkeypress="javascript:fnGo(this);" tabindex="1" size="15"/>
							</td>
							<fmtLotCode:message key="BTN_GO" var="varLoad"/>
							<td><gmjsp:button name="Btn_Go" value="&nbsp;&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnGo(this);" tabindex="2" buttonType="Load" /></td>
					</tr>
					</table>
				</td>
			</tr>
			<tr id="InfoSection">
			<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<%-- <logic:equal name="frmLotCodeRpt" property="strOpt" value="Load"> --%>	
				<tr>
				<!-- This JSP is for the section "product information" -->
					<td>
						<jsp:include page="/operations/GmLotCodeProdInfo.jsp" >
						<jsp:param name="FORMNAME" value="frmLotCodeRpt" />
						</jsp:include>
					</td>
			     </tr>	
			     <tr>
			     <!-- This JSP is for the section "Donor information" -->
					<td>
						<jsp:include page="/operations/GmLotCodeDonorInfo.jsp" >
						<jsp:param name="FORMNAME" value="frmLotCodeRpt" />
						</jsp:include>
					</td>
			     </tr>
			    <tr>
			     <!-- This JSP is for the section "Lot information" -->
					<td>
						<jsp:include page="/operations/GmLotCodeInfo.jsp" >
						<jsp:param name="FORMNAME" value="frmLotCodeRpt" />
						</jsp:include>
					</td>
			     </tr>
			     <tr>
			     <!-- This JSP is for the section "Receiving Information" -->
					<td>
						<jsp:include page="/operations/GmLotCodeRecInfo.jsp" >
						<jsp:param name="FORMNAME" value="frmLotCodeRpt" />
						</jsp:include>
					</td>
			     </tr>
			    <tr>
			     <!-- This JSP is for the section "Open Transaction" -->
					<td>
						<jsp:include page="/operations/GmLotCodeOpenTxn.jsp" >
						<jsp:param name="FORMNAME" value="frmLotCodeRpt" />
						</jsp:include>
					</td>
			     </tr>
			     <tr>
			     <!-- This JSP is for the section "Inventory Information" -->
					<td>
						<jsp:include page="/operations/GmLotCodeInventoryInfo.jsp" >
						<jsp:param name="FORMNAME" value="frmLotCodeRpt" />
						</jsp:include>
					</td>
			     </tr>
			     <tr>
			     <!-- This JSP is for the section "Lot code transaciton history" -->
					<td>
						<jsp:include page="/operations/GmLotCodeTxnHistory.jsp" >
						<jsp:param name="FORMNAME" value="frmLotCodeRpt" />
						</jsp:include>
					</td>
			     </tr>
			     <tr><td class="LLine"></td></tr> 
			     <tr height="30px">
			     <fmtLotCode:message key="BTN_PRINT" var="varPrint"/>
			     <fmtLotCode:message key="BTN_CLOSE" var="varClose"/>
			     <%if(strScreen.equals("popup")){ %>
			     	 <td align="center"><gmjsp:button name="Btn_Print" value="&nbsp;&nbsp;&nbsp;${varPrint}&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnPrintScreen(this);" tabindex="2" buttonType="Load" />&nbsp;
			            <gmjsp:button name="Btn_Close" value="&nbsp;&nbsp;&nbsp;${varClose}&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="javascript:window.close();" tabindex="3" buttonType="Load" /></td>
			     <%}else{ %>
			     	 <td align="center"><gmjsp:button name="Btn_Print" value="&nbsp;&nbsp;&nbsp;${varPrint}&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnPrintScreen(this);" tabindex="2" buttonType="Load" /></td>
			     <%} %>
			    
			     </tr>
		     </table></td></tr>
		     <%-- </logic:equal> --%>
		</table>

	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>