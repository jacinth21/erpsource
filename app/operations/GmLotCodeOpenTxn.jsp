<%
/**********************************************************************************
 * File		 		: GmLotCodeOpenTxn.jsp
 * Desc		 		: This Jsp is for the Open transaction section
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLotOpenTxn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
  <!-- \operations\GmLotCodeOpenTxn.jsp--> 
<fmtLotOpenTxn:setLocale value="<%=strLocale%>"/>
<fmtLotOpenTxn:setBundle basename="properties.labels.operations.GmLotCodeReport"/>
<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<HTML>
<HEAD>

<script>
</script>
<!-- style added for PC-3659 support Edge alignment -->
 <style>
	#TxnDataGrid .objbox{
		 height: auto !important;
	}
	#TxnDataGrid table
	{
	width:100% !important;
	margin-right:0 !important;
	padding-right:0 !important; 
	}
	#TxnDataGrid
	{
		height: auto !important;
		width: 100% !important; 
		margin:	0 !important;
	}
</style> 
<BODY leftmargin="20" topmargin="10">
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td class="Line" colspan="5"></td></tr> 
			<tr>
					<td height="24" class="ShadeRightTableCaption" colspan="5">&nbsp;<b><fmtLotOpenTxn:message key="TD_LOT_OPEN_TXN_HEADER"/></b></td>
		    </tr>	
		    <tr id="DivTxnGrid">
				<td>
				<div id="TxnDataGrid" height="50"></div>
				</td>
			</tr>
			<tr id="DivTxnNoDataMessage"><td colspan="3" height="20" align="Center"><fmtLotOpenTxn:message key="TD_NO_DATA_FOUND"/></td></tr>
		</table>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>