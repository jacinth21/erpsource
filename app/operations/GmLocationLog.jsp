<%
/*****************************************************************************
 * File		 		: GmLocationLog.jsp
 * Desc		 		: This screen is used to display the Location Log Report
 * Version	 		: 1.0
 * author			: Rick Schmid
******************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtLocationLog" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtLocationLog:setLocale value="<%=strLocale%>"/>
<fmtLocationLog:setBundle basename="properties.labels.operations.GmLocationLog"/>

<!--operations\GmLocationLog.jsp  -->

<bean:define id="gridData" name="frmLocationLog"
	property="gridXmlData" type="java.lang.String">
</bean:define>

<HTML>
<HEAD>
<TITLE>Globus Medical: Location Log</TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmStatusLog.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>	
<script>

var objGridData = '<%=gridData%>';
var countryCode = '<%=strCountryCode%>';

function fnOnPageLoad()
{
	gridObj = initGridWithDistributedParsing('dataGrid',objGridData,'');	
}

function fnSubmit()
{
    var validParams = false;

    if ((document.frmLocationLog.fromDate.value == "" || document.frmLocationLog.toDate.value == "") &&
    		document.frmLocationLog.txnID.value == "" && document.frmLocationLog.locationID.value == "" && countryCode == 'en') 
	{
    	Error_Details(message_operations[376]);
	}
    if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	
	if (document.frmLocationLog.partNum.value != "") {
		validParams = true;
	}
	if (document.frmLocationLog.fromDate.value != "" && document.frmLocationLog.toDate.value != "") {
		validParams = true;
	}
	if (document.frmLocationLog.txnID.value != "") {
		validParams = true;
	}
	if (document.frmLocationLog.locationID.value != "") {
		validParams = true;
	}
	if (document.frmLocationLog.strWarehouseID.value != "0") {
		validParams = true;
	}
	if (validParams) {
		document.frmLocationLog.strOpt.value = 'load';
	} else {
		Error_Details(message_operations[377]);
		Error_Show();
		Error_Clear();
		document.frmLocationLog.strOpt.value = '';
		return false;
	}
	fnStartProgress('Y');
	document.frmLocationLog.submit();
}

function fnExcelExport(){
	    gridObj.editStop();
		gridObj.toExcel('/phpapp/excel/generate.php');
}


</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="gmLocationLog.do?method=fetchLocationLog">
    <html:hidden property="strOpt"  value=""/>
	<table border="0" class="DtTable850"  width = "1020" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="4" height="25" class="RightDashBoardHeader"><fmtLocationLog:message key="LBL_LOC_LOG"/></td>
		</tr>
		<tr>
			<td class="RightTableCaption" HEIGHT="24" align="right"><fmtLocationLog:message key="LBL_PART_NO"/>:</td>
			<td>&nbsp;<html:text
			    property="partNum" size="20" styleClass="InputArea"
			    onfocus="changeBgColor(this,'#AACCE8');" 
			    onblur="changeBgColor(this,'#ffffff');" />
    		<span class="RightTextBlue"><fmtLocationLog:message key="LBL_ENT_PART"/></span>
			<td class="RightTableCaption" HEIGHT="24" align="left"><fmtLocationLog:message key="LBL_WAREHOUSE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="strWarehouseID"
				SFFormName="frmLocationLog" SFSeletedValue="strWarehouseID"
				SFValue="alWarehouseList" codeId="WAREHOUSEID" codeName="WAREHOUSENM"
				defaultValue="[Choose One]" />
			</td>
		</tr>
		<tr><td class="LLine" height="1" colspan="4"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" HEIGHT="24" align="right"><fmtLocationLog:message key="LBL_DATE_RANGE"/>:</td>
			<td class="RightText">&nbsp;<fmtLocationLog:message key="LBL_FROM"/>:&nbsp;
			<gmjsp:calendar SFFormName="frmLocationLog" controlName="fromDate" 
				gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');" />
			&nbsp;&nbsp;<fmtLocationLog:message key="LBL_TO"/>: 
			<gmjsp:calendar SFFormName="frmLocationLog"  controlName="toDate" 
				gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');" />
			</td><td></td><td></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="4"></td></tr>
		<tr>
		    <td class="RightTableCaption" HEIGHT="24" align="right"><fmtLocationLog:message key="LBL_TRANS"/>:</td>
			<td>&nbsp;<html:text
			    property="txnID" size="15" styleClass="InputArea"
			    onfocus="changeBgColor(this,'#AACCE8');" 
			    onblur="changeBgColor(this,'#ffffff');" />
			</td><td></td><td></td>
		</tr>
		<tr><td class="LLine" height="1" colspan="4"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" HEIGHT="24" align="right"><fmtLocationLog:message key="LBL_LOC_CODE"/>:</td>
			<td>&nbsp;<html:text
			    property="locationID" size="20" styleClass="InputArea"
			    onfocus="changeBgColor(this,'#AACCE8');" 
			    onblur="changeBgColor(this,'#ffffff');" />
			    <span class="RightTextBlue"><fmtLocationLog:message key="LBL_ENTER_LOC"/></span>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<fmtLocationLog:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button"
				 onClick="javascript:fnSubmit();" tabindex="25" buttonType="Load" />
		    </td><td></td><td></td>
		</tr>
		<tr><td class="Line" height="1" colspan="4"></td></tr>
		<tr>
			<td colspan="6" width="100%">
						<div id="dataGrid" style="height: 400px; width: 1000px;"></div>
			</td>
		</tr>

		<tr><td colspan="9" class="Line" height="1"></td></tr> 
		<%if(gridData.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="8" align="center" height="25" >
				<div class='exportlinks'><fmtLocationLog:message key="LBL_EXP_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;
					<a href="#" onclick="javascript:fnExcelExport();"><fmtLocationLog:message key="LBL_EXCEL"/> </a>
				</div>
			</td>
		</tr>
		<%}%>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>

</BODY>
</HTML>