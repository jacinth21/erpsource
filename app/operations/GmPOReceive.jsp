<!--\operations\GmPOReceive.jsp  -->
<%
/*******************************************************************************
 * File		 		: GmPOReceive.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
 ******************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="java.applet.Applet"%>
<%@ page import="java.awt.event.*"%>
<%@ page import="java.awt.*"%>
<%@ page import="java.lang.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.net.*"%>
<%@ page import="java.io.*"%>
<%@ page import="java.text.*"%>

<%@ taglib prefix="fmtPOReceive" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtPOReceive:setLocale value="<%=strLocale%>"/>
<fmtPOReceive:setBundle basename="properties.labels.operations.GmPOReceive"/>

<%
    GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strpurchasingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("RECEIVE_SHIPMENT");
	String strAXCompId = GmCommonClass.getString("AX_REC_PO_COMPID");
	String strwebServiceURL = GmCommonClass.getString("AX_REC_PO_WS_URL");
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmPoReceive", strSessCompanyLocale);
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strPO = (String)request.getAttribute("hPO")==null?"":(String)request.getAttribute("hPO");
	String strType = (String)request.getAttribute("hType")==null?"":(String)request.getAttribute("hType");
	String strPoType = GmCommonClass.parseNull((String)request.getAttribute("hPoType"));
	String strSkipLot = GmCommonClass.parseNull(GmCommonClass.getString("SKIP_LOT_VALIDATION"));
	String strErrorLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TITLE"));
	
	String strApplDateFmt = strGCompDateFmt;
	HashMap hmPODetails = new HashMap();
	HashMap hmUdiParams = null;
	ArrayList alItemDetails = new ArrayList();
	ArrayList alUdiDetails = new ArrayList();
	String strPOId = "";
	String strVendorId = "";
	String strVendorName = "";
	String strVendorAdd = "";
	String strPODate = "";
	String strReqdDate = "";
	String strPOTotal = "";
	String strComments = "";
	String strLotCode = "";
	
	String strWOId = "";
	String strPartNum = "";
	String strDesc = "";
	String strPrice = "";
	String strQty = "";
	String strPendQty = "";

	String strUser = "";
	String strTotal = "";
	String strUserName = "";
	boolean bolFl = false;
	String strCriticalFl = "";
	String strWIPQty = "";
	String strFARFl = "";
	String strUOM = "";
	String strPOTypeNm = "";
	String strWORev = "";
	String strPartRev = "";
	String strWOLog = "";
	int intWOStatusFlag = 0;
	String strCntlVadFl = "";
	String strCnumFmt = "";
	String strDupCnum = "";
	String strProdClass = "";
	String strVALDFL = "";
	String strContent = "";
	String strRowColor = "";
	String strJulianRule ="";
	String strUdiNum = "";
	String strShowUdiText = "";
	String strCNUDIId = "";
	String strDIUDIId = "";
	String strEDUDIId = "";
	String strRuleID = "";
	String strRuleValue = "";
	String strKey = "";
	String strProdType ="";
	String strUDIFormat ="";
	String strDINum ="";
	String strVendorRule = "";
	
	HashMap hmNames = new HashMap();
	if (hmReturn != null)
	{
		hmPODetails = (HashMap)hmReturn.get("PODETAILS");
		alItemDetails = (ArrayList)hmReturn.get("ITEMDETAILS");
		alUdiDetails = (ArrayList)hmReturn.get("UDIDETAILS");
		
		
		hmUdiParams = new HashMap();
		for (int i=0;i<alUdiDetails.size();i++){
			hmUdiParams = (HashMap)alUdiDetails.get(i);
			strRuleID = GmCommonClass.parseNull((String)hmUdiParams.get("RULE_ID"));
			strRuleValue = GmCommonClass.parseNull((String)hmUdiParams.get("RULE_VALUE"));
			strKey  = strRuleID + "VALUE";
			hmNames.put(strKey, strRuleValue);
  		}
		strCNUDIId = GmCommonClass.parseNull((String)hmNames.get("CNVALUE"));
		strDIUDIId = GmCommonClass.parseNull((String)hmNames.get("DIVALUE"));
		strEDUDIId = GmCommonClass.parseNull((String)hmNames.get("EDVALUE"));
		strPOId = GmCommonClass.parseNull((String)hmPODetails.get("POID"));
		strVendorId = GmCommonClass.parseNull((String)hmPODetails.get("VID"));
		strVendorName = GmCommonClass.parseNull((String)hmPODetails.get("VNAME"));
		strVendorAdd = GmCommonClass.parseNull((String)hmPODetails.get("VADD"));
		strPODate = GmCommonClass.getStringFromDate((Date)hmPODetails.get("PODATE"),strGCompDateFmt);
		strReqdDate =GmCommonClass.getStringFromDate((Date)hmPODetails.get("RQDATE"),strGCompDateFmt);
		strPOTotal = GmCommonClass.parseNull((String)hmPODetails.get("TOTAL"));
		strUser = GmCommonClass.parseNull((String)hmPODetails.get("CUSER"));
		strLotCode = GmCommonClass.parseNull((String)hmPODetails.get("LCODE"));
		strComments = GmCommonClass.parseNull((String)hmPODetails.get("COMMENTS"));
		strUser = strUser.concat(".gif");
		strUserName = GmCommonClass.parseNull((String)hmPODetails.get("CNAME"));
		strWIPQty = GmCommonClass.parseNull((String)hmPODetails.get("WIP"));
		strPOTypeNm = GmCommonClass.parseNull((String)hmPODetails.get("POTYPENM"));
	}

	int intSize = 0;
	HashMap hcboVal = null;
	String strDate = "";
	String strOnBlurTRBGColor = "";
	String strOnFcsTRBGColor = "";

	GmCalenderOperations gmCal = new GmCalenderOperations();
	 strDate = gmCal.getCurrentDate(strGCompDateFmt);
	strJulianRule = GmCommonClass.parseNull((String)request.getAttribute("JULIANRULE"));
	strVendorRule = GmCommonClass.parseNull((String)request.getAttribute("VENDORRULE"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO Receive </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="STYLESHEET" type="text/css" href="<%=strCssPath%>/displaytag.css"></link>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strpurchasingJsPath %>/GmPOReceive.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script>
var showTextVal;
var cnt = 0;
var statusmsg="";
var julianRule = "<%=strJulianRule%>";
var format = "<%=strApplDateFmt%>";
var cnUdiId = "<%=strCNUDIId%>";
var diUdiId = "<%=strDIUDIId%>";
var edUdiId = "<%=strEDUDIId%>";
var AXPOCompId = "<%=strAXCompId%>";
var AXPOURL    = "<%=strwebServiceURL%>";
var itemdetailsize = "<%=alItemDetails.size()%>";
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hInputString" value="">
<input type="hidden" name="hPOId" value="<%=strPOId%>">
<input type="hidden" name="hVendorId" value="<%=strVendorId%>">
<input type="hidden" name="hLotCode" value="<%=strLotCode%>">
<input type="hidden" name="hType" value="<%=strType%>">
<input type="hidden" name="hPoType" value="<%=strPoType%>">
<input type="hidden" name="hManfDt" value="<%=strDate%>">
<input type="hidden" name="hSkipLot" value="<%=strSkipLot%>"> 
<input type="hidden" name="RE_FORWARD" >
<input type="hidden" name="RE_TXN" >
<input type="hidden" name="txnStatus">
<input type="hidden" name="hSkipVendorLot" value="<%=strVendorRule%>">


	<table border="0" cellspacing="0" cellpadding="0" class="DtTable1300">
		<tr>
			<td width="1280" height="25" colspan="6" class="RightDashBoardHeader">&nbsp;<fmtPOReceive:message key="LBL_RECEIVE_SHIPMENT"/></td>
			<td width="18"  colspan="" class="RightDashBoardHeader">
			<fmtPOReceive:message key="LBL_HELP" var="varHelp"/>
						<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='17' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
	</table>
	<table border="0" class="DtTable1300" cellspacing="0" cellpadding="0">
		<tr>
			<td width="1298" height="30" valign="top" align="center" colspan="6">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" HEIGHT="30" align="right">&nbsp;<font color="red">* </font><fmtPOReceive:message key="LBL_PO_NUMBER"/>:</td>
						<td width="720" class="RightText">
					<table cellspacing="0" cellpadding="0" border="0">
					 <tr> 
					     <td width=20%>
						&nbsp;<input type="text" size="15" value="<%=strPO%>" name="Txt_PO" class="InputArea"  onFocus="changeBgColor(this,'#EEEEEE');" 
						onBlur="changeBgColor(this,'#ffffff');" tabindex=1 onkeypress="javascript:fnCheckKey();" style="text-transform:uppercase;">
						</td><td>&nbsp;
						<fmtPOReceive:message key="LBL_LOAD_PO" var="varLoadPO"/>
						<gmjsp:button value="${varLoadPO}" gmClass="button" onClick="fnLoadPO();" tabindex="13" buttonType="Load" />
						</td>
					</tr>
			       </table>
						</td>
					</tr>
<%
	if (strhAction.equals("LoadPO"))
	{
%>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="Shade">
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtPOReceive:message key="LBL_VENDOR_NAME"/>:</td>
						<td class="RightText">&nbsp;<%=strVendorName%></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr>
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtPOReceive:message key="LBL_PO_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strPODate%></td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="Shade">
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtPOReceive:message key="LBL_PO_TYPE"/>:</td>
						<td class="RightText">&nbsp;<%=strPOTypeNm%></td>
					</tr>
					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr >
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtPOReceive:message key="LBL_RAISED_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>
					</tr>
					
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="Shade">
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtPOReceive:message key="LBL_DATE_RECEIVED"/>:</td>
						<td class="RightText">&nbsp;<%=strDate%><input type="hidden" size="10" value="<%=strDate%>" name="Txt_RecDate"/>											
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr >
					<fmtPOReceive:message key="LBL_PACKING_SLIP_ID" var="varPackingSlipID"/>
						<gmjsp:label type="MandatoryText"  SFLblControlName="${varPackingSlipID}" td="true"/>
						<td class="RightText">&nbsp;<input type="text" size="20" value="" name="Txt_Pack" class="InputArea" onBlur="changeBgColor(this,'#ffffff');fnLoadPartDetails(this);" 
						onFocus="changeBgColor(this,'#AACCE8');fnClearDiv();" onkeypress="javascript:fnEnter();" tabindex=2 style="text-transform:uppercase;">
						&nbsp;<span id="DivShowPackSlipIDExists" style="vertical-align:middle; display: none;"><img title="<%=strErrorLabel%>" src="<%=strImagePath%>/delete.gif"></img></span>
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="Shade">
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtPOReceive:message key="LBL_COMMENTS"/>:</td>
						<td class="RightText">&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3 rows=3 cols=60 value=""></textarea></td>
					</tr>
<%

	}

%>
				 </table>
			 </td>
		</tr>
<%
	if (strhAction.equals("LoadPO"))
	{
%>
		<tr>
			<td colspan="3">
	<table border="0" width="1298" cellspacing="0" cellpadding="0" >
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td width="1298" height="100" valign="top">
				<table border="0"   borderwidth="100%" cellspacing="0" cellpadding="0">
					<tr class="Shade">
						<td width="15" class=RightTableCaption height="24"><fmtPOReceive:message key="LBL_S#"/></td>
						<td width="60" class=RightTableCaption height="24" align="right"><fmtPOReceive:message key="LBL_PART_#"/></td>
						<td width="320" class=RightTableCaption align="center"><fmtPOReceive:message key="LBL_DESCRIPTION"/></td>
						<td width="100" class=RightTableCaption align="center"><fmtPOReceive:message key="LBL_WORK_ORDER"/></td>
						<% if (strCountryCode.equals("en")){
						%>	
						<td align="center" width="60" class=RightTableCaption><fmtPOReceive:message key="LBL_WO"/><br><fmtPOReceive:message key="LBL_REV"/></td>
						<td align="center" width="60" class=RightTableCaption><fmtPOReceive:message key="LBL_PART"/><br><fmtPOReceive:message key="LBL_REV"/></td>
						<%} %>
						<td align="center" width="50" class=RightTableCaption><fmtPOReceive:message key="LBL_QTY"/><br><fmtPOReceive:message key="LBL_ORD"/></td>
						<td align="center" width="60" class=RightTableCaption><fmtPOReceive:message key="LBL_UOM"/>/<BR><fmtPOReceive:message key="LBL_UNITS"/></td>
						<td align="center" width="50" class=RightTableCaption><br><fmtPOReceive:message key="LBL_DHR"/> <br> <fmtPOReceive:message key="LBL_PRI"/>
						<a onmouseover="this.T_WIDTH=220;return escape('DHR Back Order Priority List<br> US Sales Backorder: 0<br>OUS Replenshipment Backorder: 1 <br> Item Shipped Set (US &  OUS): 2 <br> Loaner Item Backorder: 3 <br> One Week Need: 4 <br> Two Week Need: 5 <br>Others: 6');"><img src=<%=strImagePath%>/question.gif border=0></img></a>
						</td>
						<td align="center" width="50" class=RightTableCaption><fmtPOReceive:message key="LBL_QTY"/><br><fmtPOReceive:message key="LBL_PEND"/></td>
						
						
						<td align="center" width="80" class=RightTableCaption><font color="red">* </font><fmtPOReceive:message key="LBL_QTY"/><br><fmtPOReceive:message key="LBL_TO_REC"/></td>
						<td width="100" class=RightTableCaption><font color="red">* </font><fmtPOReceive:message key="LBL_CONTROL"/><br>&nbsp;<fmtPOReceive:message key="LBL_NUMBER"/>
						<a onmouseover="this.T_WIDTH=220;return escape('Please <b>reconfirm</b> the Control Number on the Packing Slip with the etch on the Part before entering the value.<br>The following are the valid formats for control number.<br>1) If this is an assembly Part with no Control #, then enter <b>NOC#</b> as the value.<br>2) Control Number length entered for the part should be equal to 8 characters.<br>3) If Control number validation is skipped, it should  be at least 6 characters and maximum 20 characters.<br>4) Control Number cannot have an entered space/ NOC# entered for the part is not valid.<br>5) Control Number cannot have Incorrect Year/Batch/Revision Number Format.<br>6) Julian Date in the control number cannot have Incorrect Date range or not Numeric.');"><img src=<%=strImagePath%>/question.gif border=0></img></a>
						</td>
						<td width="100" class=RightTableCaption><fmtPOReceive:message key="LBL_DONOR"/><br>&nbsp;<fmtPOReceive:message key="LBL_NUMBER"/>
						
						</td>
						<td width="140" class=RightTableCaption><fmtPOReceive:message key="LBL_EXPIRY_DATE"/>
						<a onmouseover="this.T_WIDTH=120;return escape('Expiry Date is mandatory for sterile parts and disabled for non-sterile parts.');"><img src=<%=strImagePath%>/question.gif border=0></img></a>
						</td>
						<td width="140" class=RightTableCaption><fmtPOReceive:message key="LBL_MANUFACTURE_DATE"/>
						</td>
						<td width="350" class=RightTableCaption ><fmtPOReceive:message key="LBL_UDI"/>
						
						<a onmouseover="this.T_WIDTH=220;return escape('UDI comprises of DI and PI. DI - Device Identifier, PI - Product Identifier. (01) - DI prefix, (17) - Expiry date prefix, (10) - Control No prefix.');"><img src=<%=strImagePath%>/question.gif border=0></img></a></td>
					</tr>
					<tr>
						<td colspan="15" height="1" bgcolor="#666666"></td>
					</tr>
					<%
			  		intSize = alItemDetails.size();
					hcboVal = new HashMap();
					String strItems = "";
					double dbAmount = 0.0;
					double dbTotal = 0.0;
					int intQty = 0;
					String strClass = "";
					int intPendQty = 0;
					int intWIPQty = 0;
					int intDiff = 0;
					double dblWIPQty = 0.0;
					double dblDiff = 0.0;
					String strManufactureDt = "";
					String strExpDt = "";
					String strUDIText = "";
					String strLabelID = "";
					String StrHUDIText = "";
					String strNonLblID = "";
					String strRowColors = "";
					String strControlNumberDisabled = "";
					String strDonorNumberDisabled ="";
					String strSerialNumFl = "";
					int intdhrPriority;
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alItemDetails.get(i);
						strWOId = GmCommonClass.parseNull((String)hcboVal.get("WID"));
			  			strPartNum = GmCommonClass.parseNull((String)hcboVal.get("PNUM"));
			  			strProdClass = GmCommonClass.parseNull((String)hcboVal.get("PRODCLASS"));
			  			strDesc = GmCommonClass.parseNull((String)hcboVal.get("PDESC"));
						strQty = GmCommonClass.parseNull((String)hcboVal.get("QTY"));
						strPendQty = GmCommonClass.parseZero((String)hcboVal.get("PEND"));
						strWIPQty = GmCommonClass.parseZero((String)hcboVal.get("WIP"));
						strCriticalFl = GmCommonClass.parseNull((String)hcboVal.get("CFL"));
						strFARFl = GmCommonClass.parseNull((String)hcboVal.get("FARFL"));
						strUOM = GmCommonClass.parseNull((String)hcboVal.get("UOMANDQTY"));
						strWORev =  GmCommonClass.parseNull((String)hcboVal.get("WOREV"));
						strPartRev =  GmCommonClass.parseNull((String)hcboVal.get("PARTREV"));
						strWOLog =  GmCommonClass.parseNull((String)hcboVal.get("WLOG"));
						intWOStatusFlag = Integer.parseInt(GmCommonClass.parseZero((String)hcboVal.get("STATUSFL")));
						strCntlVadFl =  GmCommonClass.parseNull((String)hcboVal.get("SKIPFL"));
						strCnumFmt=  GmCommonClass.parseNull((String)hcboVal.get("CNUMFMT"));
						strDupCnum=  GmCommonClass.parseNull((String)hcboVal.get("DUPCNUM"));
						strVALDFL = GmCommonClass.parseNull((String)hcboVal.get("VALIDATIONFL"));
						strUdiNum = GmCommonClass.parseNull((String)hcboVal.get("UDINO"));
						strProdType = GmCommonClass.parseNull((String)hcboVal.get("PRODTYPE"));
						strUDIFormat = GmCommonClass.parseNull((String)hcboVal.get("UDIFORMAT"));
						strDINum = GmCommonClass.parseNull((String)hcboVal.get("DINUM"));
						strSerialNumFl = GmCommonClass.parseNull((String)hcboVal.get("SERIALNUMNEEDFL"));
						intdhrPriority=Integer.parseInt(GmCommonClass.parseZero((String)hcboVal.get("DHR_PRIORITY"))); 
						
						//comment the following code, since UDI number get by format instead of rule table
						/*	if(!strUdiNum.equals("")){
							strUdiNum = strDIUDIId + strUdiNum;
						}*/
						 if(strProdType.equals("100845")){  
							strControlNumberDisabled = "disabled";
							strDonorNumberDisabled = "enabled";
						}else{
							strDonorNumberDisabled = "disabled";
							strControlNumberDisabled = "enabled";
						} 
						strShowUdiText = GmCommonClass.parseNull((String)hcboVal.get("SHOW_TXTBOX"));
						strClass = strCriticalFl.equals("Y")?"RightTextRed":"RightText";

						intPendQty = Integer.parseInt(strPendQty);
						//intWIPQty = Integer.parseInt(strWIPQty);
						//It is getting the decimal values from hashmap. So we cannot use parseInt from decimal values.
						dblWIPQty = Double.parseDouble(strWIPQty);
						//intDiff = intPendQty - intWIPQty;
						dblDiff = intPendQty - dblWIPQty;
						strPendQty = intPendQty == 0 || intPendQty < 0?"0":strPendQty;
						//strShade = i == 20?"class=PageBreak":""; //For alternate Shading of rows
						strExpDt = "Txt_ExpDate"+i;
						strManufactureDt = "Txt_Manufacturedt"+i;
						strUDIText = "Txt_UDI"+i;
						strLabelID = "Lbl_UDI"+i;
						strNonLblID = "NonLbl_UDI"+i;
						if(strFARFl.equals("Y")){
							strContent = "&nbsp;&nbsp;<b style ='color:red;'>- (First Article)</b>";
						}else if(!strVALDFL.equals("")){
							strContent = "&nbsp;&nbsp;<b style ='color:red;'>- ("+strVALDFL+")</b>";
						}else {
							strContent = "";
						}
						if(i%2 == 0){
							 strRowColor = "";
							 strRowColors = "";
						}else{
							 strRowColor = "shade";
							 strRowColors = "#E3EFFF";
						}
%>
					<tr id="tr<%=i%>" class="<%=strRowColor%>">
						<td class="RightText">&nbsp;<%=i+1%></td>
<%
						if (intPendQty < 0 || intWOStatusFlag >= 3 )
						{
%>
						<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
<%
						}else{
							bolFl = true;
%>
						<td class="<%=strClass%>" height="20">&nbsp;<a class="<%=strClass%>" tabindex=-1 href="javascript:fnSplit('<%=i%>','<%=strWOId%>','<%=strPartNum%>','<%=strCntlVadFl%>','<%=strCnumFmt%>','<%=strDupCnum%>','<%=strProdClass%>','<%=strUdiNum%>','<%=strShowUdiText%>','<%=strRowColors %>','<%=strProdType%>','<%=strDINum%>','<%=strQty%>','<%=strSerialNumFl%>');"><%=strPartNum%></a>
						<input type="hidden" name="hWO<%=i%>" value="<%=strWOId%>">
						<input type="hidden" name="hPNum<%=i%>" value="<%=strPartNum%>">
						<input type="hidden" name="hQty<%=i%>" value="<%=strQty%>">
						<input type="hidden" name="hSkipFl<%=i%>" value="<%=strCntlVadFl%>">
						<input type="hidden" name="hCnumFmt<%=i%>" value="<%=strCnumFmt%>">
						<input type="hidden" name="hDupCnum<%=i%>" value="<%=strDupCnum%>">
						<input type="hidden" name="hProductClass<%=i%>" value="<%=strProdClass%>">
						<input type="hidden" name="hUDINum<%=i%>" value="<%=strUdiNum%>">
						<input type="hidden" name="hshowText<%=i%>" value="<%=strShowUdiText%>">
						<input type="hidden" name="hprodType<%=i%>" value="<%=strProdType%>">
						<input type="hidden" name="hUDIFormat<%=i%>" value="<%=strUDIFormat%>">
						<input type="hidden" name="hDINum<%=i%>" value="<%=strDINum%>">
						<input type="hidden" name="hPnumRow<%=i%>" value="<%=i%>">
						<input type="hidden" name="hSerialNumNeedFl<%=i%>" value="<%=strSerialNumFl%>">
						</td>
<%
						}
%>
						<td class="RightTextAS">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%><%=strContent%></td>
						<td class="<%=strClass%>" nowrap align="center"><%=strWOId%>
								<img id="imgEdit" style="cursor:hand"  
				 						<% if (strWOLog.equals("N"))
				 						{ %>
				 							src="<%=strImagePath%>/phone_icon.jpg" 
				 						<%} else {%>	
				 							src="<%=strImagePath%>/phone-icon_ans.gif" 
				 						<% }%>
				 						title="Click to Enter/View Comments" width="22" height="20" onClick="javascript:fnOpenLog('<%=strWOId%>' )"/> 
						</td>
						<%if (strCountryCode.equals("en")){  %>
						<td class="<%=strClass%>"  align="center"><%=strWORev%></td>	
						<td class="<%=strClass%>"  align="center"><%=strPartRev%></td>
						<%} %>					
						<td class="<%=strClass%>" align="right"><%=strQty%>&nbsp;</td>
						<td class="<%=strClass%>" align="center"><%=strUOM%></td>
						<td class="<%=strClass%>" align="right"><%=intdhrPriority%>&nbsp;</td>
						
						<td class="<%=strClass%>"  align="right"><%=strPendQty%>&nbsp;</td>
						<%if (intPendQty < 0|| intWOStatusFlag >= 3){%>							
							<td class="RightText" colspan="6" height="20" align="center">&nbsp;<fmtPOReceive:message key="LBL_RECEIVED"/></td>							
						<%}else{
							strOnBlurTRBGColor = "fnAddUDIDetails("+i+");changeTRBgColor("+i+",'"+strRowColors+"');";
							strOnFcsTRBGColor = "changeTRBgColor("+i+",'#CCCCCC');";
						%>	
							<td width="1200" colspan="5" id="Cell<%=i%>" class="RightText" style="white-space: nowrap;">&nbsp;&nbsp;&nbsp;<input type="text" size="3" value="" name="Txt_QtyRec<%=i%>" class="InputArea"  onFocus="changeTRBgColor(<%=i%>,'#CCCCCC');" onBlur="changeTRBgColor(<%=i%>,'<%=strRowColors %>');" tabindex=4>&nbsp;
							<input   type="text" size="8"  value="" name="Txt_CNum<%=i%>" class="InputArea"  <%=strControlNumberDisabled%> style="text-transform:uppercase;" onFocus="changeTRBgColor(<%=i%>,'#CCCCCC');" onBlur="changeTRBgColor(<%=i%>,'<%=strRowColors %>'); fnAddUDIDetails(<%=i%>); fnFetchMessages(this,'<%=strPartNum%>', 'PROCESS','50900')" tabindex=4>&nbsp;&nbsp;
							
							<input type="text" size="8"  value="" name="Txt_DoNum<%=i%>" class="InputArea"  <%=strDonorNumberDisabled%> onFocus="changeTRBgColor(<%=i%>,'#CCCCCC');" onBlur="changeTRBgColor(<%=i%>,'<%=strRowColors %>'); fnAddUDIDetails(<%=i%>); " tabindex=4>&nbsp;&nbsp;&nbsp;&nbsp;
<%                          if (strProdClass.equals("4030")) {
							%> 
    								<gmjsp:calendar textControlName="<%=strExpDt%>" gmClass="InputArea" onFocus="<%=strOnFcsTRBGColor%>" onBlur="<%=strOnBlurTRBGColor%>" tabIndex="4"/>
<%                          } else {
                                // This component needed to keep the Txt_ExpDate count and keep the screen looking balanced
                                // when non-sterile parts are included.  It is always disabled.%>
                                <input type="text" size="9" value="" id="<%=strExpDt%>" name="<%=strExpDt%>" onBlur="changeTRBgColor(<%=i%>,'<%=strRowColors %>');fnAddUDIDetails(<%=i%>); " align="absmiddle" tabindex=4 disabled=true/>&nbsp;<img src=<%=strImagePath%>/nav_calendar.gif onClick="fnAddUDIDetails(<%=i%>);" border=0 align="absmiddle" height=18 width=19></img>
<%                          }%>
							&nbsp;&nbsp;<gmjsp:calendar textControlName="<%=strManufactureDt%>" gmClass="InputArea"  onFocus="<%=strOnFcsTRBGColor%>" onBlur="<%=strOnBlurTRBGColor%>" tabIndex="4"/>					
						</td>



<%					    if(strShowUdiText.equals("Y")){ %>
                             <td width="350"  id="UDICell<%=i%>"><input type="text" class="InputArea" size="45"  maxlength="100" name="<%=strUDIText%>" align="absmiddle" tabindex=4/>&nbsp;</td>
                        <%}else{ 
                        	if(!strUdiNum.equals("")){
                        	%>
                             <td width="350" style="width: 40%;padding-left: 7px;background:<%=strRowColors%>;" align="left" nowrap class="RightText" id="<%=strLabelID%>"><input type="text" size="52" style="width: 78%;background:<%=strRowColors %>;,border:none;" readonly name="lblHtml<%=i%>" value="<%=strUdiNum%>" ></td>
                           <%}else{%>
                             <td width="350" style="background:<%=strRowColors%>;" align="left" nowrap id="<%=strNonLblID%>" class="RightText"><input type="text" size="45" style="background:<%=strRowColors %>;,border:none;" readonly name="nonLblHtml<%=i%>" value="" ></td>
                                             <%        }
                       }
						}
%>
					</tr>
						<tr><td colspan="13" height="1" bgcolor="#eeeeee"></td></tr>							
				<%}%>
				</table>
					<INPUT type="hidden" name="hTextboxCnt" value="<%=intSize-1%>">
			</td>
		<tr>
			<td height="1" bgcolor="#666666"></td>
		</tr>	
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<tr> 
			<td>
   				<jsp:include page="/common/GmRuleDisplayAjaxInclude.jsp"/>
   			</td>
		</tr>
<%
			if (bolFl)
			{
%>
		<tr>
			<td align="center" height="30">
			<fmtPOReceive:message key="LBL_SUBMIT" var="varSubmit"/>
			<gmjsp:button value="${varSubmit}" name="Btn_PlacePO" gmClass="button" onClick="fnSubmit();" tabindex="13" buttonType="Save" /></td>
		</tr>
<%
			}
%>
    </table>
<%
	}
%>

</table>
     
</FORM>

<script language="JavaScript" type="text/javascript" src="<%=strJsPath%>/wz_tooltip.js"></script> 
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
