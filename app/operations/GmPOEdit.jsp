<!-- \operations\GmPOEdit.jsp -->
<%
	/**********************************************************************************
	 * File		 		: GmPOEdit.jsp
	 * Desc		 		: This screen is used for Editing a Purchase Order
	 * Version	 		: 1.0
	 * author			: Dhinakaran James
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>

<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPOEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPOEdit:setLocale value="<%=strLocale%>"/>
<fmtPOEdit:setBundle basename="properties.labels.operations.GmPOEdit"/>






<%

String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	try {
		response.setCharacterEncoding("UTF-8");
		GmServlet gm = new GmServlet();
		gm.checkSession(response, session);	
		GmCommonClass gmCommon = new GmCommonClass();
		
		
	
		String strAccessId = (String) session.getAttribute("strSessAccLvl") == null ? "": (String) session.getAttribute("strSessAccLvl");
		String strDeptId = (String) session.getAttribute("strSessDeptId") == null ? "": (String) session.getAttribute("strSessDeptId");
		String strUserId = (String) session.getAttribute("strSessUserId") == null ? "": (String) session.getAttribute("strSessUserId");
		String strWikiTitle = GmCommonClass.getWikiTitle("PURCHASE_ORDER_EDIT");
		int intAccId = Integer.parseInt(strAccessId);
		
		
		// Changed old calendar to dhtmlx calendar
		String strSessApplDateFmt = strGCompDateFmt;
		String strPOEditFl= GmCommonClass.parseNull((String)request.getAttribute("POEDITACCESSFL"));
		boolean editFl = false;
		// Edit PO of Purchasing Module based on security group (PMT-52276)
		/* if (strDeptId.equals("Z") || strDeptId.equals("W")) {
			editFl = true;
		}
		if (!strCountryCode.equals("en")) {
			editFl = true;
		}  */
		//to enable update button based on the security group mapped to security event
		if(strPOEditFl.equals("Y")){
		  editFl = true;
		}
		HashMap hmReturn = new HashMap();
		hmReturn = (HashMap) request.getAttribute("hmReturn");

		HashMap hmPODetails = new HashMap();

		ArrayList alItemDetails = new ArrayList();
		ArrayList alPoList = new ArrayList();
		ArrayList poTypeList = new ArrayList();

		String strShade = "";

		String strPOId = "";
		String strVendorId = "";
		String strVendorAdd = "";
		String strPODate = "";
		String strReqdDate = "";
		String strComments = "";

		String strWOId = "";
		String strPartNum = "";
		String strDesc = "";
		String strQty = "";
		String strRate = "";
		String strAmount = "";
		String strUser = "";
		String strTotal = "";
		String strUserName = "";
		String strFooter = "";
		String strDocRev = "";
		String strDocActiveFl = "";
		String strFARFl = "";
		String strCriticalFl = "";
		String strStatusFl = "";
		String strRecQty = "";
		String strReadOnly = "disabled";		
		String strWORev = "";
		String strPartRev = "";
		String strWoDueDt = "";
		String strPOTypeNm = "";
		String strPOType = "";
		String strWOPendQty = "";
		String strVoidAllow = "disabled";
		String strWOLog = "";
		String strQtyHistFL = "";
		String strPriceHistFl = "";
		Date dtReqdDate = null;
		String strVendorCurrency = "";
		String strVALDFL = "";
		String strContent = "";
		String strNonChecked = "";
		String strDHRExistFL = "";
		String strPOAction = "";
		String strPublishChecked = "";
		String strPublishFl = "";
		String strPublishedby = "";
		String strStatus = "";
		String strPublishedDt = "";
		String VPAccessfl ="";
		String strPublishDisabled ="disabled";
		String dhrCount="";
		
		
		if (hmReturn != null) {
			hmPODetails = (HashMap) hmReturn.get("PODETAILS");
			alItemDetails = (ArrayList) hmReturn.get("ITEMDETAILS");
			strPOId = (String) hmPODetails.get("POID");
			strVendorId = (String) hmPODetails.get("VID");
			strVendorAdd = (String) hmPODetails.get("VNAME");
			strPODate = GmCommonClass.getStringFromDate((Date) hmPODetails.get("PODATE"),strGCompDateFmt);
			strReqdDate =  GmCommonClass.getStringFromDate((Date) hmPODetails.get("RQDATE"),strGCompDateFmt);
			strUser = (String) hmPODetails.get("CUSER");
			strComments = (String) hmPODetails.get("COMMENTS");
			strUser = strUser.concat(".gif");
			strUserName = (String) hmPODetails.get("CNAME");
			strPOTypeNm = (String) hmPODetails.get("POTYPENM");
			strPOType = (String) hmPODetails.get("POTYPE");
			dtReqdDate = GmCommonClass.getStringToDate(strReqdDate,strGCompDateFmt);
			strVendorCurrency = (String) hmPODetails.get("VENDCURR");
			strPublishFl = GmCommonClass.parseNull((String) hmPODetails.get("PUBFL"));
			strPublishedby = GmCommonClass.parseNull((String) hmPODetails.get("PUBLBY"));
			strStatus = GmCommonClass.parseNull((String) hmPODetails.get("STATUS"));
			strPublishedDt =  GmCommonClass.getStringFromDate((Date) hmPODetails.get("PUBLDT"),strGCompDateFmt);
			VPAccessfl = GmCommonClass.parseNull((String) hmPODetails.get("VENFL"));
			alPoList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALPOLIST"));
			poTypeList = gmCommon.getCodeList("POTYP");
			dhrCount = GmCommonClass.parseZero((String) hmPODetails.get("DHRCOUNT"));
			
			
			
			
			
			
			strPublishChecked = strPublishFl.equals("Y")?"checked":"";
		}
			if(!strPublishFl.equals("Y")){
				strPublishDisabled = "";
			}

		int intSize = 0;
		HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Edit PO</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmPOEdit.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style>
TR.PageBreak {
	page-break-after: always;
}
</style>
<script>

var poid = '<%=strPOId%>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmOrder" method="POST"	action="<%=strServletPath%>/GmWOServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hPOId" value="<%=strPOId%>">
<input type="hidden" name="hVendId"	value="<%=strVendorId%>">
<input type="hidden" name="hAction"	value="">
<input type="hidden" name="hPOTotal" value="">
<input type="hidden" name="hWOString" value="">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hTxnName" value="">
<input type="hidden" name="hCancelType" value="RPORO">
<input type="hidden" name="hPublishFL" value="<%=strPublishFl%>">
<% if(!(dhrCount.equals("0"))) { %>
	<input type="hidden" name="hPOType" value="<%=strPOType%>">
<%} %>

<table border="0" width="802" cellspacing="0" cellpadding="0">
	<tr>
		<td bgcolor="#666666" colspan="3"></td>
	</tr>
	<tr>
		<td bgcolor="#666666" width="1" rowspan="4"></td>
		<td height="25" >
			<table border="0" cellspacing="0">
				<tr>
					<td class="RightDashBoardHeader" width="780"><fmtPOEdit:message key="LBL_PURCHASE_ORDER_EDIT"/></td>
					<td class="RightDashBoardHeader" align="right" width="20">
						<fmtPOEdit:message key="IMG_ALT_HELP" var="varHelp"/>
						<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
					</tr>
			</table>
		</td>
		<td bgcolor="#666666" width="1" rowspan="4">			
		</td>
	</tr>
	<tr><td bgcolor="#666666" colspan="3"></td></tr>
	<tr>
		<td width="800" height="100" valign="top">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOEdit:message key="LBL_PO_ID"/>:&nbsp;</td>
					<td class="RightTableCaption">&nbsp;<%=strPOId%></td>
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="2"></td>
				<tr class="Shade">
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOEdit:message key="LBL_VENDOR"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strVendorAdd%></td>
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="2"></td>
				<tr>
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOEdit:message key="LBL_PO_TYPE"/>:&nbsp;</td>
					<% if(!(dhrCount.equals("0"))) { %>
					<td class="RightText">&nbsp;<%=strPOTypeNm%></td>
					<%} else { %>
					<td><gmjsp:dropdown controlName="hPOType" seletedValue="<%=strPOType%>" value="<%=poTypeList%>" codeId = "CODEID" codeName = "CODENM" /></td>
					<%} %>
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="2"></td>
				<tr class="Shade">
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOEdit:message key="LBL_RAISED_BY"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strUserName%></td>
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="2"></td>
				<tr>
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOEdit:message key="LBL_PO_DATE"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strPODate%></td>
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="2"></td>
					<%
						if (editFl) {
					%>
				
				<tr class="Shade">
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOEdit:message key="LBL_REQUIRED_DATE"/>:&nbsp;</td>
					<td>&nbsp;<gmjsp:calendar textControlName="Txt_ReqDate" gmClass="InputArea" textValue="<%=dtReqdDate==null?null:new java.sql.Date(dtReqdDate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
					</td>
				</tr>
				<% if(VPAccessfl.equals("Y")) { %>
				<tr><td class="Line" height="1" colspan="2"></td></tr>
				<tr class="ShadeRightTableCaption" height="24">
					<td colspan="2">&nbsp;<fmtPOEdit:message key="LBL_PUBLISH_DETAILS"/>:</td>
				</tr>
				<tr><td class="Line" height="1" colspan="2"></td></tr>    
				<tr>
				        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtPOEdit:message key="LBL_PUBLISH"/>?:&nbsp;</td>
                        <td><input type="checkbox" id="Chk_PublishFl" size="30" <%=strPublishChecked%> <%=strPublishDisabled%> name="Chk_PublishFl" value="Y" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >                </td>
                </tr>
                <tr><td class="LLine" height="1" colspan="2"></td></tr>                
				<tr class="Shade">
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOEdit:message key="LBL_PUBLISH_BY"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strPublishedby%></td>
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="2"></td></tr>
				<tr>
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOEdit:message key="LBL_PUBLISH_DT"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strPublishedDt%></td>
				</tr> 
				<tr>
					<td class="LLine" height="1" colspan="2"></td></tr>
				<tr class="Shade">
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOEdit:message key="LBL_STATUS"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strStatus%></td>
				</tr> 		
				<tr>
					<td class="Line" height="1" colspan="2"></td>
				<tr>	
				<%	} else { %>
				
				<tr>
					<td class="LLine" height="1" colspan="2"></td></tr>
				<tr>
				<%	} %>
					<td class="RightTableCaption" valign="top" HEIGHT="25" align="right"><fmtPOEdit:message key="LBL_ADDITIONAL_INSTRUCTIONS"/>&nbsp; <BR>:&nbsp;</td>
					<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');"	onBlur="changeBgColor(this,'#ffffff');" rows=4 cols=80 value=""
						tabindex=24><%=strComments%></textarea><br>
					</td>
				</tr>
				<%	} else { %>
				<tr class="Shade">
					<td class="RightTableCaption" height="24" align="Right" width="250"><fmtPOEdit:message key="LBL_REQUIRED_DATE"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strReqdDate%><Br></td>
				</tr>
									
				<tr>
					<td class="LLine" height="1" colspan="2"></td></tr>
				<tr>
					<td class="RightTableCaption" valign="top" HEIGHT="25" align="right"><fmtPOEdit:message key="LBL_ADDITIONAL_INSTRUCTIONS"/>&nbsp;<BR><fmtPOEdit:message key="LBL_ON_PO"/>:&nbsp;</td>
					<td class="RightText">&nbsp;<%=strComments%><Br></td>
				</tr>
				<%	} %>
				<tr>
					<td class="Line" height="1" colspan="2"></td>
				</tr>
			</table>
			</td>
			</tr>
			<tr>
				<td align="center" colspan="2" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td width="35" align="center">&nbsp;<fmtPOEdit:message key="LBL_S"/></td>
						<td width="80" align="center"><input type="checkbox" name="Chk_selectAll" onClick="fnSelectAll();"> </td>
						<td width="70">&nbsp;<fmtPOEdit:message key="LBL_PART"/></td>
						<td width="400"><fmtPOEdit:message key="LBL_DESCRIPTION"/></td>
						<td align="center" nowrap><fmtPOEdit:message key="LBL_WORK_ORDER"/></td>
						<td align="center" width="60"><fmtPOEdit:message key="LBL_WO_DUE"/></td>
						<td align="center" width="60"><fmtPOEdit:message key="LBL_CURR_REV"/></td>
						<td align="center" width="60"><fmtPOEdit:message key="LBL_WO_REV"/></td>
						<TD class="RightText" align="center" width="50"><fmtPOEdit:message key="LBL_CRIT"/><BR><fmtPOEdit:message key="LBL_FLAG"/></TD>
						<TD class="RightText" align="center" width="40"><fmtPOEdit:message key="LBL_FAR"/><BR><fmtPOEdit:message key="LBL_FLAG"/></TD>
						<td align="center" width="60"><BR><fmtPOEdit:message key="LBL_WO"/><BR><fmtPOEdit:message key="LBL_QTY"/></td>
						<td align="center" width="135"><fmtPOEdit:message key="LBL_PEND"/><br><fmtPOEdit:message key="LBL_QTY"/></td>
						<td align="center" width="130"><fmtPOEdit:message key="LBL_RATE"/></td>
						<td align="center" width="80"><fmtPOEdit:message key="LBL_AMOUNT"/></td>
						<%
							if (strPOType.equals("3101")) {
						%>

						<td align="center" width="80"><fmtPOEdit:message key="LBL_ACTION"/></td>
						<%
							}
						%>
					</tr>
					<tr>
						<td colspan="14" height="1" bgcolor="#666666"></td>
					</tr>
					<%
						intSize = alItemDetails.size();
							hcboVal = new HashMap();
							double dbAmount = 0.0;
							double dbTotal = 0.0;
							int intQty = 0;
							int intPendQty = 0;
							int intStatus = 0;
							String strFARChecked = "";
							String strCriticalChecked = "";

							for (int i = 0; i < intSize; i++) {
								hcboVal = (HashMap) alItemDetails.get(i);
								strWOId = (String) hcboVal.get("WID");
								strPartNum = (String) hcboVal.get("PNUM");
								strDesc = (String) hcboVal.get("PDESC");
								strQty = GmCommonClass.parseZero((String) hcboVal.get("QTY"));
								intQty = Integer.parseInt(strQty);
								strRate = GmCommonClass.parseZero((String) hcboVal.get("RATE"));
								strFARFl = GmCommonClass.parseNull((String) hcboVal.get("FARFL"));
								strFARChecked = strFARFl.equals("Y") ? "checked" : "";
								strCriticalFl = GmCommonClass.parseNull((String) hcboVal.get("CFL"));
								strCriticalChecked = strCriticalFl.equals("Y") ? "checked": "";
								strStatusFl = GmCommonClass.parseZero((String) hcboVal.get("STATUSFL"));
								strRecQty = GmCommonClass.parseZero((String) hcboVal.get("RECQTY"));
								strWORev = GmCommonClass.parseNull((String) hcboVal.get("WOREV"));
								//PC-3447-WO Due Date Update in Purchase Order-Edit
								strWoDueDt = GmCommonClass.parseNull((String) hcboVal.get("WODUEDATE"));
								strPartRev = GmCommonClass.parseNull((String) hcboVal.get("PARTREV"));
								strWOPendQty = GmCommonClass.parseZero((String) hcboVal.get("PEND"));
								strWOLog =  GmCommonClass.parseNull((String)hcboVal.get("WLOG"));
								strQtyHistFL = GmCommonClass.parseNull((String)hcboVal.get("QTYHISTFL"));
								strPriceHistFl = GmCommonClass.parseNull((String)hcboVal.get("PRICEHISTFL"));
								strVALDFL = GmCommonClass.parseNull((String)hcboVal.get("VALIDATIONFL"));	
								strDHRExistFL = GmCommonClass.parseNull((String)hcboVal.get("DHREXISTFL")); 
								
								intPendQty = Integer.parseInt(strWOPendQty);
								intStatus = Integer.parseInt(strStatusFl);
								strReadOnly = intStatus < 3 ? "" : "disabled";
								if(strDHRExistFL.equals("Y") && intStatus != 2 && intPendQty == 0){
									strVoidAllow = "disabled";
								}else{
									strVoidAllow = "";
								}
								
								
								dbAmount = Double.parseDouble(strRate);
								dbAmount = intQty * dbAmount; // Multiply by Qty
								strAmount = "" + dbAmount;

								dbTotal = dbTotal + dbAmount;
								strTotal = "" + dbTotal;
								strShade = (i % 2 != 0) ? "class=Shade" : ""; //For alternate Shading of rows
								if(strFARFl.equals("Y")){
									strContent 		= "&nbsp;&nbsp;<b>- (First Article)</b><input type='hidden' name='hValidateFlag"+i+"' value=''>";
								}else if(!strVALDFL.equals("")){
									strContent		= "&nbsp;&nbsp;<b>- ("+strVALDFL+")</b><input type='hidden' name='hValidateFlag"+i+"' value='Y'>";
									strNonChecked 	=  "disabled";
								}else {
									strContent 		= "<input type='hidden' name='hValidateFlag"+i+"' value=''>";
									strNonChecked 	=  "";
								}								
					%>
					<tr>
							<td class="RightText" align="center" width="35"><%=i + 1%>
							<%if(intStatus == 3){ %>
							<fmtPOEdit:message key="IMG_CLICK_TO_REOPEN_PO" var="varClicktoReopenPO"/>
							<a href="javascript:fnReOpenPO('<%=strWOId%>', '<%=strPOId%>');" title="${varClicktoReopenPO}">
							<img src='<%= strImagePath%>/o.gif'  border='0' height='18' width='18'/></a>
							<% }else{%>&nbsp;&nbsp;<%} %>
							<input type="hidden" name="hWOId<%=i%>" value="<%=strWOId%>">
							<input type="hidden" name="hStatusFl<%=i%>" value="<%=strStatusFl%>">
							<input type="hidden" name="hCurrRev<%=i%>" value="<%=strPartRev%>">
							<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNum%>">
							<input type="hidden" name="hDHRExistFL<%=i%>" value="<%=strDHRExistFL%>">
						   </td>
							<td width="80" align="center"><input <%=strVoidAllow%> type="checkbox" name="Chk_PartUp<%=i%>" onClick="fnSelectCheck();"></td>
						<%
						strFooter = GmCommonClass.parseNull((String) hcboVal.get("FOOTER"));
							String strArr[] = strFooter.split("\\^");
							strFooter = strArr[0];
							strDocRev = strArr[1];
							strDocActiveFl = strArr[2];
						   	%>
						<td height="20">&nbsp;<fmtPOEdit:message key="IMG_CLICK_TO_VIEW_WORK_ORDER" var="varClickviewWorkOrder"/><a class="RightText" title="${varClickviewWorkOrder}-<%=strWOId%>" href="javascript:fnCallWO('<%=strWOId%>','<%=strDocRev%>','<%=strDocActiveFl%>');"><%=strPartNum%></a></td>
						<td class="RightTextAS">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%><%=strContent%></td>
						<td class="RightText" nowrap>&nbsp;<%=strWOId%><fmtPOEdit:message key="IMG_CLICK_VIEW_COMMENTS" var="varClickViewComments"/><img id="imgEdit" style="cursor:hand"  
	 						<% if (strWOLog.equals("N"))
	 						{ %>
	 							src="<%=strImagePath%>/phone_icon.jpg" 
	 						<%} else {%>	
	 							src="<%=strImagePath%>/phone-icon_ans.gif" 
	 						<% }%>
	 						title="${varClickViewComments}" width="22" height="20" onClick="javascript:fnOpenLog('<%=strWOId%>' )"/>
						
						</td>
						<td class="RightText" align="center">&nbsp;<%=strWoDueDt%></td>
						<td class="RightText" align="center"><%=strPartRev%></td>
						<td class="RightText" align="center">
							<input type="text" size="2" value="<%=strWORev%>" <%=strReadOnly%>	name="Txt_Rev<%=i%>" class="InputArea"
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=<%=i%> onkeyup="fnCheckVal('<%=i%>','<%=strWORev%>','<%=strCriticalChecked%>','<%=strFARChecked%>','<%=strWOPendQty%>','<%=GmCommonClass.getStringWithCommas(strRate)%>')"></td>
						<td align="center"><input type="Checkbox" <%=strReadOnly%> <%=strNonChecked %> <%=strCriticalChecked%> name="Chk_Flag<%=i%>" onclick="fnCheckVal('<%=i%>','<%=strWORev%>','<%=strCriticalChecked%>','<%=strFARChecked%>','<%=strWOPendQty%>','<%=GmCommonClass.getStringWithCommas(strRate)%>')"></td>
						<td align="center"><input type="Checkbox" <%=strReadOnly%> <%=strNonChecked %> <%=strFARChecked%> name="Chk_FAR<%=i%>" onclick="fnCheckVal('<%=i%>','<%=strWORev%>','<%=strCriticalChecked%>','<%=strFARChecked%>','<%=strWOPendQty%>','<%=GmCommonClass.getStringWithCommas(strRate)%>')"></td>
						<td align="right" class="RightText"><%=strQty%>&nbsp;</td>
						<td class="RightText" align="center">&nbsp;
							<input type="text" size="4" value="<%=strWOPendQty%>" <%=strReadOnly%> name="Txt_Qty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=<%=i%> onkeyup="fnCheckVal('<%=i%>','<%=strWORev%>','<%=strCriticalChecked%>','<%=strFARChecked%>','<%=strWOPendQty%>','<%=GmCommonClass.getStringWithCommas(strRate)%>')">
							<input type="hidden" name="hOrigQty<%=i%>" value="<%=strQty%>">
							<input type="hidden" name="hPendQty<%=i%>" value="<%=strWOPendQty%>">
							<% if (strQtyHistFL.equals("Y") ){	%>
							<fmtPOEdit:message key="IMG_CLICK_PENDING_QTY_HISTORY" var="varClickPendingQtyhistory"/>
							<img  id='imgEdit' style='cursor:hand' onclick='javascript:fnQtyHistory("<%=strWOId%>")'; title='${varClickPendingQtyhistory}'" src='<%=strImagePath%>/icon_History.gif' align='bottom'  height=13 width=13>
							<%} %>
						</td>
						<td align="right" class="RightText"><input type="text" size="7" value="<%=GmCommonClass.getStringWithCommas(strRate)%>"
							<%=strReadOnly%> name="Txt_Rate<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=<%=i%> onkeyup="fnCheckVal('<%=i%>','<%=strWORev%>','<%=strCriticalChecked%>','<%=strFARChecked%>','<%=strWOPendQty%>','<%=GmCommonClass.getStringWithCommas(strRate)%>')">
							<% if (strPriceHistFl.equals("Y") ){	%>
							<fmtPOEdit:message key="IMG_CLICK_OPEN_RATE_HISTORY" var="varClickopenRatehistory"/>
							<img  id='imgEdit' style='cursor:hand' onclick='javascript:fnPriceHistory("<%=strWOId%>")'; title='${varClickopenRatehistory}'" src='<%=strImagePath%>/icon_History.gif' align='bottom'  height=13 width=13>
							<%} %>
						</td>
						<td align="right" class="RightText"><%=GmCommonClass.getStringWithCommas(strAmount)%>&nbsp;</td>
						
						<%
							if (strPOType.equals("3101")) {
						%>
						<td align="right">
							<select class="RightText" name="Cbo_Action<%=i%>" <%=strReadOnly%>>
							<option value="0"><fmtPOEdit:message key="LBL_NO_ACTION"/></option>
							<option value="R"><fmtPOEdit:message key="LBL_TO_REWORK"/></option>
							<option value="Q"><fmtPOEdit:message key="LBL_TO_QUAR"/></option>
							<option value="S"><fmtPOEdit:message key="LBL_TO_SCRAP"/></option>
							</select>
						</td>
						<%
							} else {
						%>
						<td><input type="hidden" name="Cbo_Action<%=i%>" value="0"></td>
						<%
							}
						%>
					</tr>
					<tr>
						<td colspan="14" height="1" bgcolor="#eeeeee"></td>
					</tr>
					<%
						}
							strFooter = GmCommonClass.parseNull((String) hcboVal.get("FOOTER"));
							String strArr[] = strFooter.split("\\^");
							strFooter = strArr[0];
							strDocRev = strArr[1];
							strDocActiveFl = strArr[2];
						   	String AllWOPrint = "fnAllWOPrint('"+strDocRev+"','"+strDocActiveFl+"')";
					%>
					<tr>
						<td colspan="14" height="1" bgcolor="#666666"></td>
					</tr>
					<tr>
						<td colspan="12" height="25">&nbsp;</td>
						<td class="RightTableCaption" align="center"><fmtPOEdit:message key="LBL_TOTAL"/></td>
						<td class="RightTableCaption" align="right"><%=strVendorCurrency%>&nbsp;<%=GmCommonClass.getStringWithCommas(strTotal)%>&nbsp;
						<input type="hidden" name="hTxtCount" value="<%=intSize%>"></td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="1" bgcolor="#666666"></td>
			</tr>
			<%
				if (editFl) {
			%>
			<tr>
				<td colspan="2"><jsp:include page="/common/GmIncludeLog.jsp">
					<jsp:param name="LogType" value="PLOG" />
				</jsp:include></td>
			</tr>
			<%
				} else {
			%>
			<tr>
				<td class="RightText" HEIGHT="24" align="right"><fmtPOEdit:message key="LBL_ADDITIONAL_INSTRUCTIONS"/> :</td>
				<td class="RightText" width="300">&nbsp;<%=strComments%><br>
				</td>
			</tr>
			<%
				}
			%>
		</td>
	<tr>
		<td colspan="3" height="1" bgcolor="#666666"></td>
	</tr>


</table>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center" height="30" id="button"><fmtPOEdit:message key="LBL_CHOOSE_ACTION"/> :
				<gmjsp:dropdown controlName="Cbo_PO_Action" value="<%=alPoList%>" codeId="CODEID"  codeName="CODENM" defaultValue="[Choose One]" /> 
 				<gmjsp:button value="&nbsp;submit&nbsp;" name="Btn_Submit" gmClass="button" buttonType="Load" onClick="javascript:fnSubmit()" />&nbsp;&nbsp; 
	            
	</tr>
	<tr>
		<td colspan="5" height="1" bgcolor="#666666"></td>
	</tr>
</table>
</table>
<script>
var  docRev='<%=strDocRev%>';
var  docActiveFl='<%=strDocActiveFl%>';
var editFl='<%=editFl%>';
//-- For setting default value as update for Choose action dropdown  
document.frmOrder.Cbo_PO_Action.value='106426';//--updates
</script>
</FORM>
<%
	} catch (Exception e) {
		e.printStackTrace();
	}
%>
</BODY>
<%@ include file="/common/GmFooter.inc"%>
</HTML>

