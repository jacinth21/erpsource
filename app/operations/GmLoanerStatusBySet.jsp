 <%
/**********************************************************************************
 * File		 		: GmLoanerStatusBySet.jsp
 * Desc		 		: This screen is used to display Loaner Status
 * Version	 		: 1.0
 * author			: RichardK
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<!--operations\GmLoanerStatusBySet.jsp  -->
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator,java.lang.*"%>
<%@ taglib prefix="fmtLoanerStatusBySet" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtLoanerStatusBySet:setLocale value="<%=strLocale%>"/>
<fmtLoanerStatusBySet:setBundle basename="properties.labels.operations.GmLoanerStatusBySet"/>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();
	ArrayList alDrillDown = new ArrayList();
	ArrayList alLnTyp	= new ArrayList();
	ArrayList alHeader = new ArrayList();
	GmGridFormat gmCrossTab = new GmGridFormat();
	String strType = ""; 
	gm.checkSession(response,session);

	String strTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	String strStyle = "position:relative; top:expression(this.offsetParent.scrollTop);";
	String strHeader = GmCommonClass.parseNull((String)request.getAttribute("strHeader"));
	strHeader = strHeader + " as of " + strTodaysDate;
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	strhAction =  strhAction.equals("")?"Load":strhAction;
	strType = GmCommonClass.parseNull((String)request.getAttribute("Cbo_lnType"));
	String strLoanerData = GmCommonClass.parseNull((String)request.getAttribute("strLoaner_Data"));
	alLnTyp =  GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("LOANERTYP"));
	
	
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Status Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<script>
var objGridData = '<%=strLoanerData%>';

var lblType = '<fmtLoanerStatusBySet:message key="LBL_TYPE"/>';
</script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLoanerStatusBySet.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>


</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<FORM name="frmLoaner" method="post" action = "<%=strServletPath%>/GmLoanerPartRepServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">


<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hScreenType" value="">
<TABLE cellSpacing=0 cellPadding=0  border=0 class = "DtTable950">
<TBODY>
	<tr class=Line><td height=25 class=RightDashBoardHeader><%=strHeader %> </td></tr>
	<tr class=Line><td noWrap height=1></td></tr>
	<tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
		<td height=25  noWrap class="RightTableCaption" align="left"><font color="red">* </font><fmtLoanerStatusBySet:message key="LBL_TYPE"/>:			
			 <gmjsp:dropdown controlName="Cbo_lnType"  seletedValue="<%=strType%>"  defaultValue= "[Choose One]"	
			value="<%=alLnTyp%>" codeId="CODENMALT" codeName="CODENM" />
			&nbsp;&nbsp;<fmtLoanerStatusBySet:message key="BTN_LOAD" var = "varLoad" /><gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Go" gmClass="button" onClick="fnGo();" buttonType="Load" />
		</td>	
				
	</tr>
	
	<tr>
		<td> <div id="dataGridDiv" style="" height="400"></td>
	</tr>
	<tr class = "oddshade">
		<td class = "oddshade" colspan="3" align="center" height="30">	
			<div ><fmtLoanerStatusBySet:message key="LBL_EXP_OPT"/> : <img src='img/ico_file_excel.png' /> 
				<a href="#" onclick="fnExport('excel');"> <fmtLoanerStatusBySet:message key="LBL_EXCEL"/> </a>			
            </div>			
		</td>
	</tr>
	
	<!-- tr>		
		<td align=center>  <gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;</td>		
	</tr-->		
	
	</TBODY>
</TABLE>
<%@ include file="/common/GmFooter.inc"%>

</FORM>
</BODY>
</HTML>
