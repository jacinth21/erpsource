<%
/**********************************************************************************
 * File		 		: GmLotCodeInfo.jsp
 * Desc		 		: This Jsp is for the lot code information section
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLotCodeInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
  <!-- \operations\GmLotCodeInfo.jsp--> 
<fmtLotCodeInfo:setLocale value="<%=strLocale%>"/>
<fmtLotCodeInfo:setBundle basename="properties.labels.operations.GmLotCodeReport"/>
<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<HTML>
<HEAD>
<BODY leftmargin="20" topmargin="10">
	
		<table width="130%" border="0" cellspacing="0" cellpadding="0">
			<tr><td class="Line" colspan="8"></td></tr> 
			<tr>
					<td height="24" class="ShadeRightTableCaption" colspan="8">&nbsp;<b><fmtLotCodeInfo:message key="TD_LOT_INFO_HEADER"/></b></td>
		    </tr>	
		    <tr>
				<td class="RightText" HEIGHT="20"  align="left" width="8%">&nbsp;<b><fmtLotCodeInfo:message key="LBL_EXP_DATE"/>:&nbsp;</b></td>
				<td width="1%" id="expDate" align="left">&nbsp;<bean:write name="<%=strFormName %>" property="expDate"/></td>
				
				<td class="RightText" HEIGHT="20"  align="right" width="15%">&nbsp;<b><fmtLotCodeInfo:message key="LBL_CURR_STATUS"/>:&nbsp;</b></td>
				<td width="8%" id="currStatus" align="left">&nbsp;<bean:write name="<%=strFormName %>" property="currStatus"/></td>
				
				<td class="RightText" HEIGHT="20"  align="right" width="15%">&nbsp;<b><fmtLotCodeInfo:message key="LBL_CURR_PLANT"/>:&nbsp;</b></td>
				<td width="10%" id="currPlant" align="left">&nbsp;<bean:write name="<%=strFormName %>" property="currPlant"/></td>
				
				<td class="RightText" HEIGHT="20"  align="right" width="15%">&nbsp;<b><fmtLotCodeInfo:message key="LBL_EXCEPTION_STATUS"/>:&nbsp;</b></td>
			    <td width="50%" id="exceptionStatus" align="left">&nbsp;<bean:write name="<%=strFormName %>" property="exceptionStatus"/></td>
			</tr>
			
		</table>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>