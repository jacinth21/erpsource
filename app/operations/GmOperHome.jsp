 <%
/**********************************************************************************
 * File		 		: GmOperHome.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->

<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>

<%@ taglib prefix="fmtOperHome" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtOperHome:setLocale value="<%=strLocale%>"/>
<fmtOperHome:setBundle basename="properties.labels.operations.GmOperHome"/>
<!-- operations\GmOperHome.jsp -->
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	
	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alIniSets = new ArrayList();
	ArrayList alReturnsDashboard = new ArrayList();
	ArrayList alDHRDashboard = new ArrayList();
	ArrayList alNCMRDashboard = new ArrayList();
//	ArrayList alReturns = new ArrayList();
	
	
						
	int intLength = 0;
	int intReturnLength = 0;
	int intDashLength = 0;
	int intNCMRLength = 0;
	int intItemLength = 0;
	int intRLength =0;
	
	if (strDeptId.equals("L") || strDeptId.equals("Z") || strDeptId.equals("C"))
	{
		alIniSets = (ArrayList)hmReturn.get("INISETS");
		alReturnsDashboard = (ArrayList)hmReturn.get("RETURNSDASH");
	//	alReturns = (ArrayList)hmReturn.get("RETURNS");

		intLength = alIniSets.size();
		intReturnLength = alReturnsDashboard.size();
	//	intRLength = alReturns.size();
	}
	if (strDeptId.equals("O") || strDeptId.equals("Z"))
	{
		alDHRDashboard = (ArrayList)hmReturn.get("DHRDASH");
		alNCMRDashboard = (ArrayList)hmReturn.get("NCMRDASH");
		
		intDashLength = alDHRDashboard.size();
		intNCMRLength = alNCMRDashboard.size();
	}
	
	if (strDeptId.equals("Q"))
	{
		alNCMRDashboard = (ArrayList)hmReturn.get("NCMRDASH");
	
		intNCMRLength = alNCMRDashboard.size();
	}	
	ArrayList alItemConsign = new ArrayList();
	alItemConsign = (ArrayList)hmReturn.get("ITEMS");

	intItemLength = alItemConsign.size();

	HashMap hmLoop = new HashMap();
	HashMap hmDHRLoop = new HashMap();
	HashMap hmTempLoop1 = new HashMap();
	String strNextId1 = "";
	int intCount1 = 0;
	String strVendorId = "";
	boolean blFlag2 = false;
	boolean blFlag3 = false;
	int intDisplayCount = 0;
							
	String strConId = "";
	String strSetName = "";
	String strSetId = "";
	String strPerson = "";
	String strDateInitiated = "";
	String strComments = "";

	String strShade = "";
	
	String strVendorName = "";
	String strDHRId = "";
	String strPartNum = "";
	String strDesc = "";
	String strFlag = "";
	String strControlFl = "";
	String strInspFl = "";
	String strPackFl = "";
	String strVeriFl = "";
	String strLine = "";
	String strRAId = "";
	String strReturnType = "";
	String strReturnReason = "";
	String strExpectedDate = "";
	String strNCMRId = "";
	String strCreatedDate = "";
	String strCreditDate = "";
	String strReprocessId = "";

	HashMap hmTempLoop = new HashMap();
	String strNextId = "";
	int intCount = 0;
	boolean blFlag = false;
	String strIniFlag = "";
	String strWIPFlag = "";
	String strTemp = "";
	boolean blFlag1 = false;
	int intDispCount = 0;
	int intInspCount = 0;
	int intPackCount = 0;
	int intVeriCount = 0;
	int intConCount = 0;
	int intCreditCount = 0;
	int intReprocessCount = 0;
	StringBuffer sbTemp = new StringBuffer();
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Operations Dashboard </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

var typeArr = ['50150','50151','50152','50153','50155','50156','50157','50158','50159','50160','50161','50162'];

//This prototype is provided by the Mozilla foundation and
//is distributed under the MIT license.
//http://www.ibiblio.org/pub/Linux/LICENSES/mit.license

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

function fnSubmit(val)
{
	document.frmAccount.hColumn.value = val;
	document.frmAccount.submit();
}

function fnCallEdit(val)
{
	document.frmAccount.action = "GmSetBuildServlet";
	document.frmAccount.hAction.value = 'EditLoadDash';
	document.frmAccount.hId.value = val;
	document.frmAccount.submit();
}

function fnCallCreditReturn(val)
{
	document.frmAccount.hRAId.value = val;
	document.frmAccount.hAction.value = "LoadCredit";
	document.frmAccount.action = "GmReturnCreditServlet";
	document.frmAccount.submit();
}

function fnUpdateDHR(id,val)
{
	document.frmAccount.hId.value = id;
	document.frmAccount.hMode.value = val;
	document.frmAccount.hFrom.value = "DashboardDHR";
	document.frmAccount.hAction.value = "UpdateDHR";
	document.frmAccount.action = "GmPOReceiveServlet";
	document.frmAccount.submit();
}

function fnCallEditReturn(val)
{
	document.frmAccount.action = "GmReturnProcessServlet";
	document.frmAccount.hAction.value = 'Reload';
	document.frmAccount.hId.value = val;
	document.frmAccount.submit();
}

function fnShowNCMR(val)
{
windowOpener("/GmNCMRServlet?hAction=PrintNCMR&hId="+val,"NCMR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnUpdateNCMR(id,val)
{
	document.frmAccount.hNCMRId.value = id;
	document.frmAccount.hMode.value = val;
	document.frmAccount.hAction.value = "UpdateNCMR";
	document.frmAccount.action = "GmNCMRServlet";
	document.frmAccount.submit();
}

function fnCallEditItems(val,type)
{
	document.frmAccount.hId.value = val;
	document.frmAccount.hMode.value = "CONTROL";
	document.frmAccount.hFrom.value = "ItemDashboard";
	document.frmAccount.hAction.value = "EditControl";
	document.frmAccount.hConsignId.value = val;
	
	document.frmAccount.hType.value = type;
	if (typeArr.indexOf(type) != -1)
	{
		document.frmAccount.action = "GmSetAddRemPartsServlet";
	}
	else
	{
		document.frmAccount.action = "GmConsignItemServlet";	
	}
		
	document.frmAccount.submit();
}

function fnCallItemsShip(val,type)
{
	document.frmAccount.hId.value = val;
	document.frmAccount.hFrom.value = "LoadItemShip";
	document.frmAccount.hConsignId.value = val;
	document.frmAccount.hAction.value = "LoadItemShip";
	
	document.frmAccount.hType.value = type;
	document.frmAccount.hOpt.value = type;
	
	if (type == "4114" || type == "4113" || type == "4115" || type == "4116" || type == "4117" || type == "4118")
	{
		document.frmAccount.action = "GmReprocessMaterialServlet";
	}
	else if (typeArr.indexOf(type) != -1)
	{
		document.frmAccount.action = "GmSetAddRemPartsServlet";	
	}
	
	else
	{
		document.frmAccount.action = "GmConsignItemServlet";	
	}
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnCallReturn(val)
{
	document.frmAccount.hRAId.value = val;
	document.frmAccount.hAction.value = "LoadCredit";
	document.frmAccount.action = "/GmReturnCreditServlet";
	document.frmAccount.submit();
}

function fnViewReturns(val)
{
	windowOpener('<%=strServletPath%>/GmPrintCreditServlet?hAction=VIEW&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}

function fnPicSlip(val,type)
{
	if (type == "4114" || type == "4113" || type == "4115" || type == "4116" || type == "4117" || type == "4118")
	{
		windowOpener("<%=strServletPath%>/GmConsignItemServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
	}
	if (typeArr.indexOf(type) != -1)
	{
		windowOpener("<%=strServletPath%>/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");	
	}
}

function fnOpenLog(id,type)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"ConsignLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPgToLoad" value="">
<input type="hidden" name="hSubMenu" value="Trans">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hFrom" value="Dashboard">
<input type="hidden" name="hType" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hConsignId" value="">
<input type="hidden" name="hNCMRId" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hRAId" value="">

<%
	if (strDeptId.equals("O") || strDeptId.equals("Z"))
	{
%>	
	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td valign="top">
				<table border="0" width="898" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="9" height="25" class="RightDashBoardHeader">
							<fmtOperHome:message key="LBL_DASHS_DHR"/>
						</td>
					</tr>
					<tr><td class="Line" colspan="9"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td width="60">&nbsp;<fmtOperHome:message key="LBL_VENDOR_NAME"/></td>
						<td width="100">&nbsp;<fmtOperHome:message key="LBL_DHR_ID"/></td>
						<td HEIGHT="24" align="center" width="60"><fmtOperHome:message key="LBL_PART_NUM"/></td>
						<td width="300"><fmtOperHome:message key="LBL_DESC"/></td>
						<td width="90" align="center"><fmtOperHome:message key="LBL_CREATED_DT"/></td>
						<td width="90" align="center"><fmtOperHome:message key="LBL_PENDING_CNTR"/><BR></td>
						<td width="90" align="center"><fmtOperHome:message key="LBL_PEN_INSP"/></td>
						<td width="90" align="center"><fmtOperHome:message key="LBL_PEN_PACK"/></td>
						<td width="90" align="center"><fmtOperHome:message key="LBL_PEN_VERI"/></td>
					</tr>
<%
						if (intDashLength > 0)
						{

							sbTemp.setLength(0);

							for (int j = 0;j < intDashLength ;j++ )
							{
								hmDHRLoop = (HashMap)alDHRDashboard.get(j);
								
								if (j<intDashLength-1)
								{
									hmTempLoop1 = (HashMap)alDHRDashboard.get(j+1);
									strNextId1 = GmCommonClass.parseNull((String)hmTempLoop1.get("VID"));
								}
							
								strVendorId = GmCommonClass.parseNull((String)hmDHRLoop.get("VID"));
								strVendorName = GmCommonClass.parseNull((String)hmDHRLoop.get("VNAME"));
								strDHRId = GmCommonClass.parseNull((String)hmDHRLoop.get("ID"));
								strPartNum = GmCommonClass.parseNull((String)hmDHRLoop.get("PARTNUM"));
								strDesc = GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String)hmDHRLoop.get("PDESC")));
								strFlag = GmCommonClass.parseNull((String)hmDHRLoop.get("FL"));
								strCreatedDate = GmCommonClass.parseNull((String)hmDHRLoop.get("CDATE"));
	
								if (strFlag.equals("0"))
								{
									strControlFl = "<a class=RightTextRed href= javascript:fnUpdateDHR('"+strDHRId+"','C');>Y</a>";
									strInspFl = "-";
									strPackFl = "-";
									strVeriFl = "-";
									intConCount++;
								}								
								else if (strFlag.equals("1"))
								{
									strControlFl = "-";
									strInspFl = "<a class=RightTextRed href= javascript:fnUpdateDHR('"+strDHRId+"','I');>Y</a>";
									strPackFl = "-";
									strVeriFl = "-";
									intInspCount++;
								}
								else if (strFlag.equals("2"))
								{
									strControlFl = "-";								
									strInspFl = "-";
									strPackFl = "<a class=RightTextRed href= javascript:fnUpdateDHR('"+strDHRId+"','P');>Y</a>";
									strVeriFl = "-";
									intPackCount++;
								}
								else if (strFlag.equals("3"))
								{
									strControlFl = "-";								
									strInspFl = "-";
									strPackFl = "-";
									strVeriFl = "<a class=RightTextRed href= javascript:fnUpdateDHR('"+strDHRId+"','V');>Y</a>";
									intVeriCount++;
								}
								
								if (strVendorId.equals(strNextId1))
								{
									intCount1++;
									strLine = "<TR><TD colspan=9 height=1 class=Line></TD></TR>";
								}
								else
								{
									
									sbTemp.append("document.all.td"+strVendorId+"Con.innerHTML = '<b>"+intConCount+"</b>';");
									sbTemp.append("document.all.td"+strVendorId+"Insp.innerHTML = '<b>"+intInspCount+"</b>';");
									sbTemp.append("document.all.td"+strVendorId+"Pack.innerHTML = '<b>"+intPackCount+"</b>';");
									sbTemp.append("document.all.td"+strVendorId+"Veri.innerHTML = '<b>"+intVeriCount+"</b>';");
									
									intConCount = 0;
									intDispCount = 0;
									intInspCount = 0;
									intPackCount = 0;
									intVeriCount = 0;
								
									blFlag3 = true;
									strLine = "<TR><TD colspan=9 height=1 class=Line></TD></TR>";
									if (intCount1 > 0)
									{
										strVendorName = "";
										strLine = "";
									}
									else
									{
										blFlag2 = true;
									}
									intCount1 = 0;
									//
								}
								if (intCount1 > 1)
								{
									strVendorName = "";
									strLine = "";
								}
							
								out.print(strLine);
								strShade	= (j%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount1 == 1 || blFlag2)
								{
%>
					<tr id="tr<%=strVendorId%>">
						<td colspan="5" height="18" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strVendorId%>')"><%=strVendorName%></a></td>
						<td align="center" id="td<%=strVendorId%>Con" class="RightText">&nbsp;</td>
						<td align="center" id="td<%=strVendorId%>Insp" class="RightText">&nbsp;</td>
						<td align="center" height="18" id="td<%=strVendorId%>Pack" class="RightText">&nbsp;</td>
						<td align="center" height="18" id="td<%=strVendorId%>Veri" class="RightText">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="9"><div style="display:none" id="div<%=strVendorId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strVendorId%>">
<%
								blFlag2 = false;
								
								}
%>


					<tr <%=strShade%>>
						<td width="60" HEIGHT="20" class="RightText">&nbsp;</td>
						<td width="100" HEIGHT="20" class="RightText">&nbsp;<%=strDHRId%></td>
						<td width="60" class="RightText">&nbsp;<%=strPartNum%></td>
						<td width="300" class="RightText">&nbsp;<%=strDesc%></td>
						<td width="90" class="RightText">&nbsp;<%=strCreatedDate%></td>
						<td width="90" class="RightText" align="center">&nbsp;<%=strControlFl%></td>
						<td width="90" class="RightText" align="center">&nbsp;<%=strInspFl%></td>
						<td width="90" class="RightText" align="center">&nbsp;<%=strPackFl%></td>
						<td width="90" class="RightText" align="center">&nbsp;<%=strVeriFl%></td>
					</tr>
					<tr><td colspan="9" bgcolor="#cccccc" height="1"></td></tr>
<%
								if (blFlag3 || j+1==intDashLength)
								{
									if (j+1==intDashLength)
									{									
								//sbTemp.append("document.all.td"+strVendorId+".innerHTML = '<b>"+intDisplayCount+"</b>';");
								sbTemp.append("document.all.td"+strVendorId+"Con.innerHTML = '<b>"+intConCount+"</b>';");
								sbTemp.append("document.all.td"+strVendorId+"Insp.innerHTML = '<b>"+intInspCount+"</b>';");
								sbTemp.append("document.all.td"+strVendorId+"Pack.innerHTML = '<b>"+intPackCount+"</b>';");
								sbTemp.append("document.all.td"+strVendorId+"Veri.innerHTML = '<b>"+intVeriCount+"</b>';");
								}
								
%>
							</table></div>
						</td>
					</tr>
<%
								}
							blFlag3	= false;
							}
							out.print("<script>");
							out.print(sbTemp.toString());
							out.print("</script>");
						}else{
%>
					<tr>
						<td height="30" colspan="9" align="center" class="RightTextBlue"><fmtOperHome:message key="LBL_NO_DHR"/> !</td>
					</tr>
<%
						}
%>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
		
	<BR><BR>
<%
	}

	if (strDeptId.equals("O") || strDeptId.equals("Z") || strDeptId.equals("Q"))
	{	
%>
	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td valign="top">
				<table border="0" width="898" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="6" height="25" class="RightDashBoardHeader"><fmtOperHome:message key="LBL_DASH_DHR"/></td>
					</tr>
					<tr><td class="Line" colspan="6"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td width="100">&nbsp;<fmtOperHome:message key="LBL_VENDOR_NAME"/></td>
						<td width="100">&nbsp;<fmtOperHome:message key="LBL_NCMR_ID"/></td>
						<td HEIGHT="24" align="center" width="60"><fmtOperHome:message key="LBL_PART_NUM"/></td>
						<td width="400"><fmtOperHome:message key="LBL_DESC"/></td>
						<td width="90" align="center"><fmtOperHome:message key="LBL_PEN_ACT"/></td>
						<td width="90" align="center"><fmtOperHome:message key="LBL_PEN_CLOSUR"/></td>
					</tr>
<%
						if (intNCMRLength > 0)
						{
							HashMap hmNCMRLoop = new HashMap();
							HashMap hmTempLoop2 = new HashMap();
							String strNextId2 = "";
							int intCount2 = 0;
							boolean blFlag4 = false;
							boolean blFlag5 = false;
							int intPendCnt = 0;
							int intCloseCnt = 0;
							for (int m = 0;m < intNCMRLength ;m++ )
							{
								hmNCMRLoop = (HashMap)alNCMRDashboard.get(m);

								if (m<intNCMRLength-1)
								{
									hmTempLoop2 = (HashMap)alNCMRDashboard.get(m+1);
									strNextId2 = GmCommonClass.parseNull((String)hmTempLoop2.get("VID"));
								}
							
								strVendorId = GmCommonClass.parseNull((String)hmNCMRLoop.get("VID"));
								strVendorName = GmCommonClass.parseNull((String)hmNCMRLoop.get("VNAME"));
								strNCMRId = GmCommonClass.parseNull((String)hmNCMRLoop.get("NID"));
								strPartNum = GmCommonClass.parseNull((String)hmNCMRLoop.get("PNUM"));
								strDesc = GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String)hmNCMRLoop.get("PDESC")));
								strFlag = GmCommonClass.parseNull((String)hmNCMRLoop.get("SFL"));
								if (strFlag.equals("1"))
								{
									strInspFl = "<a class=RightTextRed href= javascript:fnUpdateNCMR('"+strNCMRId+"','A');>Y</a>";
									strPackFl = "";
									intPendCnt++;
								}
								else if (strFlag.equals("2"))
								{
									strInspFl = "-";
									strPackFl = "<a class=RightTextRed href= javascript:fnUpdateNCMR('"+strNCMRId+"','C');>Y</a>";
									intCloseCnt++;
								}
								
								if (strVendorId.equals(strNextId2))
								{
									intCount2++;
									strLine = "<TR><TD colspan=6 height=1 class=Line></TD></TR>";
								}
								else
								{
									sbTemp.append("document.all.td"+strVendorId+"Act.innerHTML = '<b>"+intPendCnt+"</b>';");
									sbTemp.append("document.all.td"+strVendorId+"Clo.innerHTML = '<b>"+intCloseCnt+"</b>';");
									intPendCnt = 0;
									intCloseCnt = 0;
									
									blFlag5 = true;
									strLine = "<TR><TD colspan=6 height=1 class=Line></TD></TR>";
									if (intCount2 > 0)
									{
										strVendorName = "";
										strLine = "";
									}
									else
									{
										blFlag4 = true;
									}
									intCount2 = 0;
									//
								}
								if (intCount2 > 1)
								{
									strVendorName = "";
									strLine = "";
								}
							
								out.print(strLine);
								strShade	= (m%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount2 == 1 || blFlag4)
								{
%>
					<tr id="trnc<%=strVendorId%>">
						<td colspan="4" height="18" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('nc<%=strVendorId%>')"><%=strVendorName%></a></td>
						<td align="center" class="RightText" id="td<%=strVendorId%>Act"></td>
						<td align="center" class="RightText" id="td<%=strVendorId%>Clo"></td>
					</tr>
					<tr>
						<td colspan="6"><div style="display:none" id="divnc<%=strVendorId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabnc<%=strVendorId%>">
<%
								blFlag4 = false;
								}
%>


					<tr <%=strShade%>>
						<td width="100" HEIGHT="20" class="RightText">&nbsp;</td>
						<td width="100" HEIGHT="20" class="RightText">&nbsp;<A class="RightText"  href="javascript:fnShowNCMR('<%=strNCMRId%>')"><%=strNCMRId%></a></td>
						<td width="60" class="RightText">&nbsp;<%=strPartNum%></td>
						<td width="400" class="RightText">&nbsp;<%=strDesc%></td>
						<td width="90" class="RightText" align="center">&nbsp;<%=strInspFl%></td>
						<td width="90" class="RightText" align="center">&nbsp;<%=strPackFl%></td>
					</tr>
					<tr><td colspan="6" bgcolor="#cccccc" height="1"></td></tr>
<%
								if (blFlag5 || m+1==intNCMRLength)
								{
									if (m+1==intNCMRLength)
									{								
										sbTemp.append("document.all.td"+strVendorId+"Act.innerHTML = '<b>"+intPendCnt+"</b>';");
										sbTemp.append("document.all.td"+strVendorId+"Clo.innerHTML = '<b>"+intCloseCnt+"</b>';");
									}
									intPendCnt = 0;
									intCloseCnt = 0;
%>
							</table></div>
						</td>
					</tr>
<%
								}
							blFlag5	= false;
							}
							out.print("<script>");
							out.print(sbTemp.toString());
							out.print("</script>");
						}else{
%>
					<tr>
						<td height="30" colspan="6" align="center" class="RightTextBlue"><fmtOperHome:message key="LBL_NO_NCMR"/> !</td>
					</tr>
<%
						}
%>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
<%
	}
%>
	 
<%
	if (strDeptId.equals("L") || strDeptId.equals("Z") || strDeptId.equals("C"))
	{
%>
 
<BR><br>
	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="898" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="7" height="25" class="RightDashBoardHeader">
							<fmtOperHome:message key="LBL_DASH_RETRN"/>
						</td>
					</tr>
					<tr><td class="Line" colspan="8"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="80"><fmtOperHome:message key="LBL_RA_ID"/></td>
						<td width="200" align="center"><fmtOperHome:message key="LBL_ACCT_DISTR"/>&nbsp;/ </td>
						<td width="150">Reason</td>
						<td width="70" align="center"><fmtOperHome:message key="LBL_RETURNED_DT"/><br></td>
						<td width="70" align="center"><fmtOperHome:message key="LBL_CREDIT_DT"/><br></td>
						<td width="60" align="center"><fmtOperHome:message key="LBL_PEN_CR"/><br></td>
						<td width="60" align="center"><fmtOperHome:message key="LBL_PEN_REPR"/><br></td>
					</tr>
<%
// log.debug(" Values inside alDashBaord is " + alReturnsDashboard);
 
						if (intReturnLength > 0)
						{
							HashMap hmReturnLoop = new HashMap();
							hmTempLoop = new HashMap();
							String strAccName = "";
							String strStatus = "";
							String strTypeId = "";
							String strDistName = "";
							String strReason = "";
							strNextId = "";
							blFlag = false;
							intCount = 0;
							sbTemp.setLength(0);
							for (int k = 0;k < intReturnLength ;k++ )
							{
								hmReturnLoop = (HashMap)alReturnsDashboard.get(k);
								if (k<intReturnLength-1)
								{
									hmTempLoop = (HashMap)alReturnsDashboard.get(k+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("TYPEID"));
								}
								strTypeId = GmCommonClass.parseNull((String)hmReturnLoop.get("TYPEID"));
								strRAId = GmCommonClass.parseNull((String)hmReturnLoop.get("RAID"));
								strPerson = GmCommonClass.parseNull((String)hmReturnLoop.get("PER"));
								strDateInitiated = GmCommonClass.parseNull((String)hmReturnLoop.get("RDATE"));
								strCreditDate = GmCommonClass.parseNull((String)hmReturnLoop.get("CDATE"));
								strReturnType = GmCommonClass.parseNull((String)hmReturnLoop.get("TYPE"));
								strReturnReason = GmCommonClass.parseNull((String)hmReturnLoop.get("COMMENTS"));
								strExpectedDate = GmCommonClass.parseNull((String)hmReturnLoop.get("EDATE"));
								strAccName = GmCommonClass.parseNull((String)hmReturnLoop.get("ANAME"));
								strStatus = GmCommonClass.parseNull((String)hmReturnLoop.get("STATUS_FL"));
								strSetName = GmCommonClass.parseNull((String)hmReturnLoop.get("SNAME"));
								strDistName = GmCommonClass.parseNull((String)hmReturnLoop.get("DNAME"));
								strAccName = strAccName.equals("")?strDistName:strAccName;
								strReason = GmCommonClass.parseNull((String)hmReturnLoop.get("REASON"));
								if (strStatus.equals("Y")) 
							    {
							    	intCreditCount++;
							    }else{
							    intReprocessCount++;
							    }
							    
								if (strTypeId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
								}
								else
								{
									sbTemp.append("document.all.td"+strTypeId+"Cr.innerHTML = '<b>"+intCreditCount+"</b>';");
									sbTemp.append("document.all.td"+strTypeId+"Pr.innerHTML = '<b>"+intReprocessCount+"</b>';");
									intCreditCount = 0;
									intReprocessCount = 0;
									
									blFlag1 = true;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strReturnType = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
								}
								if (intCount > 1)
								{
									strReturnType = "";
									strLine = "";
								}
							
								out.print(strLine);
								strShade	= (k%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
%>
					<tr id="trret<%=strTypeId%>">
						<td colspan="5" height="18" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('ret<%=strTypeId%>')"><%=strReturnType%></a></td>
						<td align="center" class="RightText" id="td<%=strTypeId%>Cr"></td>
						<td align="center" class="RightText" id="td<%=strTypeId%>Pr"></td>
					</tr>
					<tr>
						<td colspan="7"><div style="display:none" id="divret<%=strTypeId%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tabret<%=strTypeId%>">
<%
								blFlag = false;
								}
%>
						<tr <%=strShade%>>
							<td width="100" HEIGHT="18" class="RightText">&nbsp;&nbsp;<a class=RightText href=javascript:fnViewReturns('<%=strRAId%>');><%=strRAId%></a></td>
							<td width="200" class="RightText"><%=strAccName%></td>
							<td width="150" class="RightText"><%=strReason%></td>
							<td width="70" class="RightText">&nbsp;<%=strDateInitiated%></td>
							<td width="70" class="RightText">&nbsp;<%=strCreditDate%></td>
<%	
							if (strStatus.equals("Y")) 
							{
%>
							<td width="60" class="RightText" align="center">&nbsp;<a class=RightText href= javascript:fnCallReturn('<%=strRAId%>');>Y</td>	
							<td width="60" class="RightText" align="center">&nbsp;-</td>
<% 
							}else{
%>
							<td width="60" class="RightText" align="center">&nbsp;-</td>	
							<td width="60" class="RightText" align="center">&nbsp;<a class=RightText href= javascript:fnCallEditReturn('<%=strRAId%>');>Y</td>
<%							
							}
%>
						</tr>
						<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
<%
								if (blFlag1 || k+1==intReturnLength)
								{
									if (k+1==intReturnLength)
									{								
									sbTemp.append("document.all.td"+strTypeId+"Cr.innerHTML = '<b>"+intCreditCount+"</b>';");
									sbTemp.append("document.all.td"+strTypeId+"Pr.innerHTML = '<b>"+intReprocessCount+"</b>';");
									}
%>
							</table></div>
						</td>
					</tr>
<%
								}
							blFlag1	= false;
							}
							out.print("<script>");
							out.print(sbTemp.toString());
							out.print("</script>");
						} else	{
%>
					<tr>
						<td colspan="7" align="center" class="RightTextBlue" height="30"><fmtOperHome:message key="LBL_NO_RETURNS"/> !</td>
					</tr>
<%
						}
%>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
	</table>
<BR><BR>
<%
	}
%>

				<table border="0" width="900" cellspacing="0" cellpadding="0">
					<tr>
						<td rowspan="<%=intItemLength+17%>" width="1" class="Line"></td>
						<td colspan="7" height="1" bgcolor="#666666"></td>
						<td rowspan="<%=intItemLength+17%>" width="1" class="Line"></td>
					</tr>
					<tr>
						<td colspan="7" height="25" class="RightDashBoardHeader"><fmtOperHome:message key="LBL_DASH_CONSIG"/></td>
					</tr>
					<tr><td class="Line" height="1" colspan="7"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" width="30"><fmtOperHome:message key="LBL_TYPE"/></td>
						<td width="90">&nbsp;<fmtOperHome:message key="LBL_CONSIG_ID"/></td>
						<td width="90">&nbsp;<fmtOperHome:message key="LBL_REF_ID"/></td>
						<td width="100">&nbsp;<fmtOperHome:message key="LBL_REQ_INI"/></td>
						<td width="60" align="center">&nbsp;<fmtOperHome:message key="LBL_PENDING_CNTROL"/>&nbsp;</td>
						<td width="60" align="center">&nbsp;<fmtOperHome:message key="LBL_PENDING_SHIP"/> &nbsp;/<BR></td>
						<td width="350">&nbsp;&nbsp;<fmtOperHome:message key="LBL_COMMENTS"/></td>
					</tr>
					
<%
						String strRedTextShipNoLink = "";
						String strRedTextShip = "";
						String strRedTextControl = "";
						String strTemp1 = "";
						String strTemp2 = "";
						String strShipFl = "";
						String strCntrlFl = "";
						String strConsignType = "";
						String strUserName = "";

						if (intItemLength > 0)
						{
							hmTempLoop = new HashMap();
							strNextId = "";
							intCount = 0;
							blFlag = false;
							strTemp = "";
							blFlag1 = false;
													
							for (int i = 0;i < intItemLength ;i++ )
							{
								hmLoop = (HashMap)alItemConsign.get(i);
								
								if (i<intItemLength-1)
								{
									hmTempLoop = (HashMap)alItemConsign.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("CTYPE"));
								}
																
								strConId = GmCommonClass.parseNull((String)hmLoop.get("CID"));
								strSetName = GmCommonClass.parseNull((String)hmLoop.get("TNAME"));
								strComments = GmCommonClass.parseNull((String)hmLoop.get("COMMENTS"));
								strConsignType = GmCommonClass.parseNull((String)hmLoop.get("CTYPE"));
								strUserName = GmCommonClass.parseNull((String)hmLoop.get("UNAME"));
								strReprocessId	= GmCommonClass.parseNull((String)hmLoop.get("REFID"));
								
								strRedTextShip = "<a class=RightTextRed href= javascript:fnCallItemsShip('"+strConId+"','"+strConsignType+"');>Y</a>";
								strRedTextShipNoLink = "<a class=RightTextRed>Y</a>";
								strRedTextControl = "<a class=RightTextRed href= javascript:fnCallEditItems('"+strConId+"','"+strConsignType+"');>Y</a>";
								strTemp1 = (String)hmLoop.get("SHIPREQFL");
								strTemp2 = (String)hmLoop.get("CONTROLFL");

								strShipFl = strTemp1.equals("1")?strRedTextShip:"--";
								if (strTemp1.equals("1"))
								{
									strShipFl = strTemp2.equals("Y")?strRedTextShipNoLink:strRedTextShip;
								}
								else
								{
									strShipFl = "--";
								}
								strCntrlFl = strTemp2.equals("Y")?strRedTextControl:"--";
								
								if (strConsignType.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
								}
								else
								{
									blFlag1 = true;
									strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strSetName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									//
								}

								if (intCount > 1)
								{
									strSetName = "";
									strLine = "";
								}
							
								out.print(strLine);
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
																
%>
					<tr id="tr<%=strConsignType%>">
						<td colspan="7" height="18" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strConsignType%>')"><%=strSetName%></a></td>
					</tr>
					<tr>
						<td colspan="7"><div style="display:none" id="div<%=strConsignType%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strConsignType%>">
<%
								blFlag = false;
								}
%>
								<tr <%=strShade%>>
									<td width="30" class="RightText" align="center" height="20">&nbsp;<a href="javascript:fnPicSlip('<%=strConId%>','<%=strConsignType%>');"><img src="<%=strImagePath%>/packslip.gif" border=0></a></td>
									<td width="90" class="RightText" height="20">&nbsp;<%=strConId%></td>
									<td width="90" class="RightText" height="20">&nbsp;<%=strReprocessId%></td>
									<td width="100" class="RightText" height="20">&nbsp;<%=strUserName%></td>
									<td width="60" class="RightText" align="center">&nbsp;<%=strCntrlFl%></td>
									<td width="60" class="RightText" align="center">&nbsp;<%=strShipFl%></td>
									<td width="350" class="RightText"><%=strComments%></td>
								</tr>
								<TR><TD colspan=7 height=1 bgcolor=#cccccc></TD></TR>
<%
								if (blFlag1 || i+1==intItemLength)
								{
%>
							</table></div>
						</td>
					</tr>
<%
								}
							blFlag1	= false;
							}
						} else	{
%>
					<tr>
						<td height="30" colspan="7" align="center" class="RightTextBlue"><fmtOperHome:message key="LBL_NO_INI_PEN_ACT"/> !</td>
					</tr>
<%
					}
%>
					<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
				</table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
