 <%
/**********************************************************************************
 * File		 		: GmPartNumSalesReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtPartNumSalesReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartNumSalesReport:setLocale value="<%=strLocale%>"/>
<fmtPartNumSalesReport:setBundle basename="properties.labels.operations.GmPartNumSalesReport"/>
<!-- operations\GmPartNumSalesReport.jsp -->
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmLoop = new HashMap();
	HashMap hcboVal = new HashMap();

	String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");

	String strPartNum = "";
	String strPartDesc = "";
	String strSalesCount = "";
	String strConsgCount = "";
	String strInhouseCount = "";

	String strProjName = "";

	String strShade = "";
	String strCodeID = "";
	String strSelected = "";
	int intLoop = 0;

	ArrayList alProject = new ArrayList();
	alProject = (ArrayList)session.getAttribute("PROJLIST");

	ArrayList alSets = new ArrayList();
	ArrayList alReport = new ArrayList();

	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("REPORT");
	}

	int intProjLength = alProject.size();
	int intSetLength = alSets.size();
	if (alReport != null)
	{
		intLoop = alReport.size();
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Consignment Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnSubmit()
{
	document.frmAccount.hAction.value = "Go";
	fnStartProgress('Y');
	document.frmAccount.submit();
}

function fnPrintVer(val)
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+val,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPartSalesReportServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hAction" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="20" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="20" width="1" class="Line"></td>
		</tr>
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">
				<fmtPartNumSalesReport:message key="LBL_REPORT"/>
			</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartNumSalesReport:message key="LBL_PRO_LIST"/>:</td>
			<td>&nbsp;<select name="Cbo_ProjId" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtPartNumSalesReport:message key="OPT_CHOOSE_ONE"/>
<%
					hcboVal = new HashMap();
			  		for (int i=0;i<intProjLength;i++)
			  		{
			  			hcboVal = (HashMap)alProject.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strProjName = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
						strSelected = strProjId.equals(strCodeID)?"selected":"";
%>
					<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeID%> / <%=strProjName%></option>
<%
			  		}
%>

			</select></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td class="RightText" HEIGHT="24" ><fmtPartNumSalesReport:message key="LBL_FROM_DT"/>:&nbsp;<input type="text" size="10" value="<%=strFromDt%>" name="Txt_FromDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmAccount.Txt_FromDate');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
&nbsp;&nbsp;<fmtPartNumSalesReport:message key="LBL_TO_DT"/>: <input type="text" size="10" value="<%=strToDt%>" name="Txt_ToDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmAccount.Txt_ToDate');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp;
<fmtPartNumSalesReport:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;${varGo}&nbsp;" name="Btn_Go" gmClass="button" onClick="javascript:fnSubmit();" buttonType="Load" />
			</td>
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>
		<tr>
			<td colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" align="center" width="60"><fmtPartNumSalesReport:message key="LBL_PART_NUM"/></td>
						<td width="400">&nbsp;<fmtPartNumSalesReport:message key="LBL_DESCRIPTION"/></td>
						<td width="100" align="center"><fmtPartNumSalesReport:message key="LBL_QTY_SOLD"/><br></td>
						<td width="100" align="center"><fmtPartNumSalesReport:message key="LBL_QTY_CONS"/><br></td>
						<td width="100" align="center"><fmtPartNumSalesReport:message key="LBL_QTY_INHOUSE"/><br></td>
					</tr>
					<tr><td class="Line" colspan="5"></td></tr>
					<tr>
						<td colspan="5">
							<div style="display:visible;width: 697px; height: 460px; overflow: auto;">
								<table border="0" width="100%" cellspacing="0" cellpadding="0">					
<%
			if (intLoop > 0)
			{
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);
					strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
					strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
					strSalesCount = (String)hmLoop.get("SALESCOUNT");
					strConsgCount = (String)hmLoop.get("CONSIGNCOUNT");
					strInhouseCount = (String)hmLoop.get("INHOUSECOUNT");

					if (!strSalesCount.equals("0") || !strConsgCount.equals("0") || !strInhouseCount.equals("0"))
					{
%>
									<tr>
										<td width="60" height="20" class="RightText">&nbsp;<%=strPartNum%></td>
										<td width="400" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
										<td width="100" class="RightText" align="center"><%=strSalesCount%></td>
										<td width="100" class="RightText" align="center"><%=strConsgCount%></td>
										<td width="100" class="RightText" align="center"><%=strInhouseCount%></td>
									</tr>
									<tr><td colspan="5" bgcolor="#eeeeee" height="1"></td></tr>
<%
					}
				}
			} else	{
%>
					<tr>
						<td height="30" colspan="5" align="center" class="RightTextBlue"><fmtPartNumSalesReport:message key="LBL_NO_ITM_SOLD"/> !</td>
					</tr>
<%
		}
%>
								</table>
							</DIV>
						</td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
	</table>
		
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
