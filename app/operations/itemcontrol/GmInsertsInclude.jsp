 <%
/*********************************************************************************************************
 * File		 		: GmInsertsInclude.jsp
 * Desc		 		: This screen is used to show Insert Item Details 
 * author			: Karthik
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.HashMap"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtItemDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmInsertsInclude.jsp -->
<fmtItemDetail:setLocale value="<%=strLocale%>"/>
<fmtItemDetail:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>
<%
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
strFormName = (strFormName.equals(""))? "frmItemControl" : strFormName;
String strChecked="";
%>
<bean:define id="alReturn" name="<%=strFormName%>" property="alInsertDetails" type="java.util.ArrayList"></bean:define>
<bean:define id="alInsertLocDtl" name="<%=strFormName%>" property="alInsertlocdtl" type="java.util.ArrayList"></bean:define>
<%
int alInsertLocSize = alInsertLocDtl.size();
String strOptionString = "";
String strChooseOne = "<option value=\"0\" >[Choose One]</option>";
String strTempPnum = "";
StringBuffer sbInsertPartNum = new StringBuffer();
	for (int i=0; i<alInsertLocSize; i++){
		HashMap hmloc =  GmCommonClass.parseNullHashMap((HashMap)alInsertLocDtl.get(i));
		if(i==0){
			strTempPnum = GmCommonClass.parseNull((String)hmloc.get("PNUM"));
		}
		String strPnum = GmCommonClass.parseNull((String)hmloc.get("PNUM"));
		
		if(!strTempPnum.equals(strPnum)){
			strOptionString = strChooseOne + strOptionString;
%>
			<script>Insertput('<%=strTempPnum%>','<%=strOptionString%>');</script>
<%
			sbInsertPartNum.append(strTempPnum).append(",");
			strTempPnum = strPnum;
			strOptionString = "";
		}
		String strLocID= GmCommonClass.parseNull((String)hmloc.get("LOCATIONID"));
		String strLocCD= GmCommonClass.parseNull((String)hmloc.get("LOCATIONCD")); 
		String strLocType= GmCommonClass.parseNull((String)hmloc.get("LOCATIONTYPE"));
		strOptionString += " <option value=\""+strLocCD+"\">"+strLocType+":"+strLocCD+"</option> ";
		if(i == (alInsertLocSize-1)){
		
			strOptionString = strChooseOne + strOptionString;
%>
			<script>Insertput('<%=strPnum%>','<%=strOptionString%>');</script>
<%
			sbInsertPartNum.append(strPnum).append(",");
		}
%>
<%
	}
%>
<table border="0" width="700px" cellspacing="0" cellpadding="0">
	<tr><td colspan="6" class="Line" height="1"></td></tr>
	<tr><td colspan="6" class="Line" height="1"></td></tr>
	<tr class="ShadeRightTableCaption" style="background:#ccc;border-bottom:1pt solid black;">
		<td Height="25" colspan="6">&nbsp;<fmtItemDetail:message key="LBL_INSERT_HEADER" /></td>
	</tr>
	<tr><td colspan="6" class="Line" height="1"></td></tr>
	<tr class="ShadeRightTableCaption">
		<td width="30" height="24">&nbsp;</td>
		<td width="40" >&nbsp;</td>
		<td width="120" >&nbsp;<fmtItemDetail:message key="LBL_INSERT"/></td>
		<td width="200" >&nbsp;<fmtItemDetail:message key="LBL_INSERT_REV"/></td>
		<td width="100" >&nbsp;<fmtItemDetail:message key="LBL_INSERT_QTY"/></td>
		<td width="150">&nbsp;<fmtItemDetail:message key="LBL_INSERT_LOCATION"/></td>
	</tr>
	<tr><td colspan="6" class="Line" height="1"></td></tr>
	<tr>
		<td colspan="6">
			<table width="699" id='insertInclude' cellspacing="0" cellpadding="0">
				<%
						int intSize = alReturn.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();
							String strInsertID = "";
							String strInsertRev = "";
							String strQty = "";
							String strMapLocId = "";
							String strSugLocId ="";
							String strSelected = "";
							String strWarehouseType = "";
							String strChooseWarehouseTyp= "";
							String strStatusfl= "";

							for (int i=0;i<intSize;i++)
							{
								hmLoop = GmCommonClass.parseNullHashMap((HashMap)alReturn.get(i));
								strInsertID = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strInsertRev = GmCommonClass.parseNull((String)hmLoop.get("REVNUM"));
								strQty = GmCommonClass.parseNull((String)hmLoop.get("PICKQTY"));
								strMapLocId = GmCommonClass.parseNull((String)hmLoop.get("MAPLOCATION"));
								strSugLocId = GmCommonClass.parseNull((String)hmLoop.get("SUGLOCATION"));
								strMapLocId = strMapLocId.equals("")? strSugLocId : strMapLocId;
								strWarehouseType = GmCommonClass.parseNull((String)hmLoop.get("LOCATIONTYPE"));
								strStatusfl = GmCommonClass.parseNull((String)hmLoop.get("STATUSFL"));
								strChecked = strStatusfl.equals("93343")?"checked":"";
				%>
			<tr>
				<td width="30" height="25">&nbsp;&nbsp;<input type="checkbox" <%=strChecked%> name="Chk_insert" id="Chk_insert"
					 class=<%=strInsertID %> onClick="fnsaveInsertDetails(this,'<%=i%>','<%=strInsertID %>','<%=strInsertRev %>','<%=strMapLocId%>');" 
					 onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 ></td>
				<td width="30">&nbsp; <span id="insert_progress" class=<%=strInsertID+"img" %> style="display:none; vertical-align: middle;">
					<img width="20" height="10" src="<%=strImagePath%>/progress.gif"></span></td>
				<td width="120" >&nbsp;<span id="insertid"> <%=strInsertID %> </span> </td>	
				<td width="200" >&nbsp;<span id="insertRev"> <%=strInsertRev %> </span> </td>		
				<td width="80" >&nbsp;<span > <%=strQty %> </span> </td>
				<td width="150">
				<span id="InlocDiv<%=i%>"></span>
					<script>
						fnShowInsertLocDrpDwn('<%=strInsertID%>','<%=i%>','<%=strMapLocId%>');
					</script>
				</td>
			</tr>
			<tr><td colspan="6" class="Line" height="1"></td></tr>
					<%
							}
						} 
					%>
			</table>
		</td>
	</tr>
	
</table>
