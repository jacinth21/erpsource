 <%
/*********************************************************************************************************
 * File		 		: GmItemDetailInclude.jsp
 * Desc		 		: This screen is used to show Item Details 
 * Version	 		: 1.0
 * author			: Gopinathan
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtItemDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmItemDetailInclude.jsp -->
<fmtItemDetail:setLocale value="<%=strLocale%>"/>
<fmtItemDetail:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>
<%
String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>

<%

String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
strFormName = (strFormName.equals(""))? "frmItemControl" : strFormName;

HashMap hmTransRules = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("TRANS_RULES"));
String strPickLocRule = GmCommonClass.parseNull((String)hmTransRules.get("PICK_LOC"));
String strRuleType =GmCommonClass.parseNull((String) hmTransRules.get("TRANS_RLTXN"));
String strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));

String strTransTypeID = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_ID"));

String strRuleTransType = "";
if(strRuleType!=null && !strRuleType.equals("")){
	strRuleTransType = strRuleType;
}
String colwidth = "260";
String colDeswidth = "310";
String strInvTypeShow = "N";
if(strPickLocRule.equals("YES")){
	if(strTransTypeID.equals("4110") || strTransTypeID.equals("4112")){
		strInvTypeShow = "Y";
		colwidth = "255";	
	}else{
		colwidth = "230";
	}
	colDeswidth = "250";
}
String strNOCTxn =  GmCommonClass.parseNull(GmCommonClass.getString("ENABLENOC"));
%>

<script> 
var pickRule = '<%=strPickLocRule%>';
var vIncludeRuleEng = '<%=strIncludeRuleEng.toUpperCase()%>';
var v_enable_noc_txn = '<%=strNOCTxn%>';

function fnBlankLine(varCounter,value)
{
	if(value == 'TBE'){
		obj = eval("document."+ "<%=strFormName%>"+ ".Txt_CNum"+varCounter);
		obj.value = '';
	}
	 
} 

function fnBlankTBE(varCounter,value)
{	 
	value = TRIM(value);
		if(value == ''){
				obj = eval("document."+ "<%=strFormName%>"+ ".Txt_CNum"+varCounter);
				obj.value = 'TBE';
			} 
	}  
</script>
<bean:define id="alReturn" name="<%=strFormName%>" property="aldetailinfo" type="java.util.ArrayList"></bean:define>
<bean:define id="alLocDtl" name="<%=strFormName%>" property="allocdtl" type="java.util.ArrayList"></bean:define>
<bean:define id="strPnums" name="<%=strFormName%>" property="strpnums" type="java.lang.String"></bean:define>
<bean:define id="StrStatus" name="<%=strFormName%>" property="statusfl" type="java.lang.String"></bean:define>
<bean:define id="txnid" name="<%=strFormName%>" property="txnid" type="java.lang.String"></bean:define>
<bean:define id="strTxnType" name="<%=strFormName%>" property="txntype" type="java.lang.String"></bean:define>
<bean:define id="errCnt" name="<%=strFormName%>" property="errCnt" type="java.lang.String"></bean:define>
<%
request.setAttribute("alReturn", alReturn);
int alLocSize = alLocDtl.size();
String strOptionString = "";
String strChooseOne = "<option value=\"0\" >[Choose One]</option>";
String strTempPnum = "";
StringBuffer sbPartNum = new StringBuffer();
if(alLocSize!=0){
	for (int i=0; i<alLocSize; i++){
		HashMap hmloc = (HashMap)alLocDtl.get(i);
		if(i==0){
			strTempPnum = GmCommonClass.parseNull((String)hmloc.get("PNUM"));
		}
		String strPnum = GmCommonClass.parseNull((String)hmloc.get("PNUM"));
		
		if(!strTempPnum.equals(strPnum)){
			strOptionString = strChooseOne + strOptionString;
%>
			<script>put('<%=strTempPnum%>','<%=strOptionString%>');</script>
<%
			sbPartNum.append(strTempPnum).append(",");
			strTempPnum = strPnum;
			strOptionString = "";
		}
		String strLocID= GmCommonClass.parseNull((String)hmloc.get("LOCATIONID"));
		String strLocCD= GmCommonClass.parseNull((String)hmloc.get("LOCATIONCD"));
		String strLocQty= GmCommonClass.parseNull((String)hmloc.get("CURRQTY"));
		strOptionString += " <option value=\""+strLocID+"\">"+strLocCD+" - ("+strLocQty+")"+"</option> ";
		if(i == (alLocSize-1)){
		
			strOptionString = strChooseOne + strOptionString;
%>
			<script>put('<%=strPnum%>','<%=strOptionString%>');</script>
<%
			sbPartNum.append(strPnum).append(",");
		}
%>
<%
	}
}
%>

<html:hidden name="<%=strFormName%>" styleId="strpnums" property="strpnums"/>
<input type="hidden" id="RE_FORWARD" name="RE_FORWARD" value="gmItemCtrl">
<input type="hidden" id="txnStatus" name="txnStatus" >
<input type="hidden" id="RE_TXN" name="RE_TXN" value="<%=strRuleTransType%>">
<input type="hidden" id="TXN_TYPE_ID" name="TXN_TYPE_ID" value="<%=strTransTypeID%>">
<input type="hidden" id="hEnableNOCfl" name="hEnableNOCfl" value="Y">
<input type="hidden" id="hPreviousLot" name="hPreviousLot" value ="">

<table width="700px" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="6" class="Line" height="1"></td>
			</tr>
			<tr class="ShadeRightTableCaption">
				<td width="80" height="25">&nbsp;<fmtItemDetail:message key="LBL_PART"/></td>
				<td width="<%=colDeswidth%>" >&nbsp;<fmtItemDetail:message key="LBL_DESCRIPTION"/></td>
				<%if(strInvTypeShow.equals("Y")){ %>
				<td width="60" >&nbsp;<fmtItemDetail:message key="LBL_INV_TYPE"/></td>
				<%} %>
				<td width="39" align="center"><fmtItemDetail:message key="LBL_ORG"/><br><fmtItemDetail:message key="LBL_QTY"/></td>
				
				<td width="<%=colwidth%>" >&nbsp;<fmtItemDetail:message key="LBL_QTY"/>&nbsp;&nbsp;
				<%-- &nbsp;&nbsp;--%><fmtItemDetail:message key="LBL_CONTROL"/>&nbsp;
				</td>
<%
				if(strPickLocRule.equals("YES")){
%>
					<td width="210" align="center"><fmtItemDetail:message key="LBL_LOCATION"/>
					</td>
<%
				} 
%>
				
			</tr>
			<tr>
				<td colspan="6" class="Line" height="1"></td>
			</tr>
			<tr>
				<td colspan="6">
					<table width="699" id='id_of_table' cellspacing="0" cellpadding="0">
<%
						ArrayList alNewLine = new ArrayList();
						HashMap hmNewLine = null;
						int intSize = alReturn.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strPrevPartNum = "";
							int intCount = 0;
							double dbTotPickQty = 0;
							double dbTotQty = 0;
							String strLine = "";
							String strPartNumHidden = "";
							String strPartDesc = "";
							String strPrice = "";
							String strPartNum = "";
							String strControl = "";
							String strItemQty = "";
							String strItemQtyHidden ="";
							String strSQty = "";
							String strMapLocId = "";
							String strTagFl = "";
							String strWareHouse = "";
							String strWareHouseName = "";
							String strPartNumIndex="";
							String parthidsts="";
							String parthidfinalsts="";
							String strErrorDtls = "";
							String strPartMaterialType = "";
							String strTempPartNum = "";//--Added for PMT-15106 - BUG-8867
							//int intQty = 0;
							//int intPickQty = 0;
							double intAllTotQty = 0.0;
							String strlottrackflag = "";
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alReturn.get(i);
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strPartNumHidden = strPartNum;
								strTempPartNum = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPartMaterialType = GmCommonClass.parseNull((String)hmLoop.get("PARTMTRELTYPE"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strItemQty = strInvTypeShow.equals("Y")? GmCommonClass.parseNull((String)hmLoop.get("PICKQTY")):GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strItemQtyHidden = strItemQty;
								strSQty = GmCommonClass.parseNull((String)hmLoop.get("PICKQTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								strMapLocId = GmCommonClass.parseNull((String)hmLoop.get("LOCATION"));
								strTagFl = GmCommonClass.parseNull((String)hmLoop.get("TAGFL"));
								strWareHouse = GmCommonClass.parseNull((String)hmLoop.get("WAREHOUSE_TYPE"));
								strWareHouseName = GmCommonClass.parseNull((String)hmLoop.get("WAREHOUSE_SH_NAME"));
								strErrorDtls = GmCommonClass.parseNull((String)hmLoop.get("ERRORDTLS"));
								strErrorDtls = strErrorDtls.replaceAll("<B>", "").replaceAll("</B>", "");
								strMapLocId = strMapLocId.equals("")? GmCommonClass.parseZero((String)hmLoop.get("SUGLOC")):strMapLocId;
								strLine = "<TR><TD colspan=6 height=1 bgcolor=#eeeeee></TD></TR>";
								parthidsts = "";
								strlottrackflag =  GmCommonClass.parseNull((String)hmLoop.get("LOTTRACKFL"));
								
								if(!strPrevPartNum.equals(strPartNum)&&(i!=0)){
									if(dbTotPickQty < dbTotQty && dbTotPickQty != 0){
										hmNewLine = new HashMap();
										hmNewLine.put("PNUM",strPrevPartNum);
										hmNewLine.put("INDEX",i-1);
										alNewLine.add(hmNewLine);
										dbTotPickQty = 0;
										dbTotQty = 0;
									}
									parthidsts = "<input type=\"hidden\" id=\""+strPrevPartNum+"\" name=\""+strPrevPartNum+"\" value=\""+strPartNumIndex+"\">";
									strPartNumIndex="";
								}
								strPartNumIndex = strPartNumIndex +i+",";
								if(strPrevPartNum.equals(strPartNum) && !strInvTypeShow.equals("Y")){
									intCount++;
									strPartNum = "";
									strPartDesc = "";
									strItemQty = "";
									strLine = "";										
							  	}else if(i!=0){									
									dbTotPickQty = 0;
									dbTotQty = 0;										
								}
									
								
								dbTotQty= Double.parseDouble(GmCommonClass.parseZero(strItemQtyHidden));
								dbTotPickQty += Double.parseDouble(GmCommonClass.parseZero(strSQty));
								intAllTotQty += Double.parseDouble(GmCommonClass.parseZero(strItemQty));
								
								if(i==(intSize-1)){		
									parthidfinalsts = "<input type=\"hidden\" id=\""+(strPartNum.equals("")?strPrevPartNum:strPartNum)+"\" name=\""+(strPartNum.equals("")?strPrevPartNum:strPartNum)+"\" value=\""+strPartNumIndex+"\">";
									strPartNumIndex="";
									if(dbTotPickQty < dbTotQty && dbTotPickQty != 0){
										hmNewLine = new HashMap();
										hmNewLine.put("PNUM",strPartNumHidden);
										hmNewLine.put("INDEX",i);
										hmNewLine.put("ORGQTY",strItemQtyHidden);
										alNewLine.add(hmNewLine);
										dbTotPickQty = 0;
										dbTotQty = 0;
										
									}
								}
								

								strPrevPartNum = strPartNumHidden;
								out.print(strLine);
%>
								<tr>
						
<%
							if(!StrStatus.equals("3")){
%>
									<td width="80" align="center"><span id="SpPart<%=i%>">&nbsp;
									<%if(strTagFl.equals("Y")){%>
										<a href=javascript:fnOpenTag('<%=strPartNum %>','<%=txnid%>')><img src=<%=strImagePath%>/tag.jpg border="0"></a>
									<%}%>
									<a class="RightText" tabindex=-1 href="javascript:fnSplit('<%=i%>','<%=strPartNum%>','<%=strPickLocRule%>','<%=strItemQty%>');"><%=strPartNum%></a></span></td>
<%
							}else{
%>
									<td width="80" align="center"><span id="SpPart<%=i%>">&nbsp;
										<%if(strTagFl.equals("Y")){%><a href=javascript:fnOpenTag('<%=strPartNum %>','<%=txnid %>')> <img src=<%=strImagePath%>/tag.jpg border="0"></a><%}%><%=strPartNum%></span></td>
<%
							}
%>
									<td width="<%=colDeswidth%>"><span id="SpDes<%=i%>">&nbsp;<%=strPartDesc%></span></td>
									
									<%if(strInvTypeShow.equals("Y")){ %>
										<td width="80" align="center"><%=strWareHouseName%></td>
									<%} %>
									<td width="39"align="center"><span id="SpQty<%=i%>"><%=strItemQty%></span></td>
									<td width="30" align="center"><label id ="lblPartScan<%=strPartNum %>" ></label> </td>
									<td id="Cell<%=i%>" width="<%=colwidth%>">
									<span id="Sp<%=i%>">
									<input type="text" size="2" id="Txt_Qty<%=i%>" name="Txt_Qty<%=i%>" value="<%=strSQty%>" class="InputArea Controlinput" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>&nbsp;
									<input type="text" size="22" id="Txt_CNum<%=i%>" value="<%=strControl%>" name="Txt_CNum<%=i%>" class="InputArea Controlinput" maxlength="40"  onFocus="fnBlankLine('<%=i%>',this.value);" onBlur="fnBlankTBE('<%=i%>',this.value); fnCntrNumValidation('<%=i%>',this,'<%=strTempPartNum%>','<%=strlottrackflag%>');" tabindex=3>
<%-- 									<input type="text" size="22" id="Txt_CNum<%=i%>" value="<%=strControl%>" name="Txt_CNum<%=i%>" class="InputArea Controlinput" maxlength="40"  onFocus="fnBlankLine('<%=i%>',this.value);" onBlur="fnBlankTBE('<%=i%>',this.value);<%if(!strIncludeRuleEng.equalsIgnoreCase("NO")){%>fnFetchMessages(this,'<%=strPartNum%>','PROCESS', '<%=strRuleTransType%>','<%=strTransTypeID%>');<%}%>" tabindex=3>onChange="fnFetchMessages(this,'<%=strPartNum%>','PROCESS', '<%=strRuleTransType%>');"
 --%>									
<%
									if(!strPickLocRule.equals("YES")&& strPartNum.equals("")&&!StrStatus.equals("3") && !strInvTypeShow.equals("Y")){
%>	
										<a style="display:inline-block" href="javascript:fnDelete('<%=strPartNum%>',<%=i%>,'<%=strPickLocRule%>');" ><img src=<%=strImagePath%>/btn_remove.gif border=0></a>
<%
									}
									//if(strErrorDtls.equals("") && !strControl.equals("") && !strControl.equals("TBE")){
%>									<%-- <span id="DivShowCnumAvl<%=i%>" style="vertical-align:middle;"> <img height="16" width="19" title="Control number available" src="<%=strImagePath%>/success.gif"></img></span>
<%
									}else--%> <%if(!strErrorDtls.equals("") && !strControl.equals("") && !strControl.equals("TBE")){
%>
						 			<span id="DivShowCnumNotExists<%=i%>" style="vertical-align:middle;"> <img height="12" width="12" title="<%=strErrorDtls %>" src="<%=strImagePath%>/delete.gif"></img></span>
<%									} %>
									</span> 
									</td>
<%
									if(strPickLocRule.equals("YES")){
%>
										<td id="locCell<%=i%>" width="190">
										<span id="locSp<%=i%>">
										<span id="locDiv<%=i%>">
										</span>
<%
										if(strPartNum.equals("") && !StrStatus.equals("3") && !strInvTypeShow.equals("Y")){
%>	
												&nbsp;<a style="display: inline-block;" href="javascript:fnDelete('<%=strPartNum%>',<%=i%>,'<%=strPickLocRule%>');" ><img src=<%=strImagePath%>/btn_remove.gif border=0></a>
<%
										}
%>	
										</span>
										</td>
<%
									} 
%>
									<%if (parthidsts.length()>0){%>
										<%=parthidsts%>
									<%}%>
									<%if (parthidfinalsts.length()>0){%>
										<%=parthidfinalsts%>
									<%}%>
									<input type="hidden" id="hPartNum<%=i%>" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
									<input type="hidden" id="hPNumReDesig<%=i%>" name="hPNumReDesig<%=i%>" value="<%=strPartNumHidden%>">
									<input type="hidden" id="hQty<%=strPartNum.replaceAll("\\.","")%>" name="hQty<%=strPartNum.replaceAll("\\.","")%>" value="<%=strItemQty%>">
									<input type="hidden" id="hPartMaterialType<%=i%>" name="hPartMaterialType<%=i%>" value="<%=strPartMaterialType%>">
									<input type="hidden" id="hPrice<%=i%>" name="hPrice<%=i%>" value="<%=strPrice%>">
									<input type="hidden" id="hWareHouse<%=i%>" name="hWareHouse<%=i%>" value="<%=strWareHouse%>">
									<input type="hidden" id="hControl<%=i%>" name="hControl<%=i%>" value="<%=strControl%>">
									<input type="hidden" id="hlottrackfl<%=i%>" name="hlottrackfl<%=i%>" value="<%=strlottrackflag%>">
									
								</tr>
<%
								if(strPickLocRule.equals("YES")){
%>
										<script>
											fnShowLocDrpDwn('<%=strPartNumHidden%>','<%=i%>','<%=strMapLocId%>');
										</script>
<%									
								}
							}//for loop end
%>
								<input type="hidden" id="hTotalParts" name="hTotalParts" value="<%=intAllTotQty%>">
								<input type="hidden" id="hCnt" name="hCnt" value="<%=intSize-1%>">
								<input type="hidden" id="hNewCnt" name="hNewCnt" value="0"><%--This hidden field used to keep java script  var cnt value  --%>
<%
							//new row if picked qty less actual pick qty
								if(alNewLine.size()>0){
									for(int j=0;j<alNewLine.size();j++){
										hmNewLine = GmCommonClass.parseNullHashMap((HashMap)alNewLine.get(j));
										int itIdx =(Integer)hmNewLine.get("INDEX");
										String strPart = GmCommonClass.parseNull((String)hmNewLine.get("PNUM"));
										String strQty =  GmCommonClass.parseNull((String)hmNewLine.get("ORGQTY"));
									
%>
										<script>
											fnSplit('<%=itIdx%>','<%=strPart%>','<%=strPickLocRule%>','<%=strQty%>');
										</script>
<%
									}
								}
						}else{
%>
							<tr><td colspan="6" height="50" align="center" class="RightTextRed"><BR><fmtItemDetail:message key="LBL_PARTINORDER"/></td></tr>
<%
						}		
%>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="6" class="Line" height="1"></td>
				</tr>
</table>

<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td td colspan="4">
					<jsp:include page="/common/GmRuleDisplayAjaxInclude.jsp"/>
			</td>
		</tr>
</table>
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<td colspan="4">
					<jsp:include page="/operations/itemcontrol/GmValidationDisplayInclude.jsp">
					<jsp:param value="<%=errCnt%>" name="errCnt"/>
					</jsp:include>
			</td>
		</tr>
		<tr>
			<td colspan="4">
					<jsp:include page="/operations/itemcontrol/GmDisplayMsgInclude.jsp"/>		
			</td>
		</tr>
</table>
