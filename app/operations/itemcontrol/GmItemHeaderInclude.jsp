 <%
/*********************************************************************************************************
 * File		 		: GmItemHeaderInclude.jsp
 * Desc		 		: This screen is used to show item header info
 * Version	 		: 1.0
 * author			: Gopinathan
**********************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@page import="java.util.HashMap"%>
<%@ taglib prefix="fmtItemHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmItemHeaderInclude.jsp -->
<fmtItemHeader:setLocale value="<%=strLocale%>"/>
<fmtItemHeader:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>

<%

String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
strFormName = (strFormName.equals(""))? "frmItemControl" : strFormName;

String strApplDateFmt = strGCompDateFmt;
HashMap hmTransRules = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("TRANS_RULES"));
String strPickLocRule = GmCommonClass.parseNull((String)hmTransRules.get("PICK_LOC"));
String strRelFlDesc = GmCommonClass.parseNull((String)hmTransRules.get("ICE_LABEL"));
String strTxnTable = GmCommonClass.parseNull((String)hmTransRules.get("TRANS_TABLE")); 
String strBillToFl    = GmCommonClass.parseNull((String)hmTransRules.get("BILL_TO_FL"));
String strSetClr="";  
strSetClr ="class='shade'"; //Set Alternate Color for Bill to lable and Release for shipping lable
log.debug("strTxnTable=="+strTxnTable);
%>

<bean:define id="StrAction" name="<%=strFormName%>" property="haction" type="java.lang.String"></bean:define>
<bean:define id="StrComments" name="<%=strFormName%>" property="comments" type="java.lang.String"></bean:define>
<bean:define id="createdDate" name="<%=strFormName%>" property="cdate" type="java.util.Date"></bean:define>
<bean:define id="refid" name="frmItemControl" property="refid" type="java.lang.String"></bean:define>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" HEIGHT="20" align="right" width="35%">&nbsp;<b><fmtItemHeader:message key="LBL_TRANSACTIONID"/>:</b></td>
						<td >&nbsp; <bean:write name="<%=strFormName%>" property="txnid" />   </td>
					 <td colspan="2">
						 <jsp:include page="/operations/logistics/GmIncludePriority.jsp">
	                       <jsp:param name="CONSIGNID" value="<%=refid%>"/> 
                             </jsp:include>
						</td>
						</tr>
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right" >&nbsp;<b><fmtItemHeader:message key="LBL_TRANSACTIONTYPE"/>:</b></td>
						<td colspan="5">&nbsp; <bean:write name="<%=strFormName%>" property="txntypenm" /> </td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<% if(!strTxnTable.equals("ORDERS"))
					{ %>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemHeader:message key="LBL_PURPOSE"/>:</b></td>
					<% } else { %>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemHeader:message key="LBL_ACCOUNTNAME"/>:</b></td>
					<%} %>
						<td colspan="3"> &nbsp; <bean:write name="<%=strFormName%>" property="purpose" />  </td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<% if(!strTxnTable.equals("ORDERS"))
					{ %>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemHeader:message key="LBL_REFERENCEID"/>:</b></td>
						<td colspan="5">&nbsp;<bean:write name="<%=strFormName%>" property="refid" /> </td>
					</tr>
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemHeader:message key="LBL_SETNAME"/>:</b></td>
						<td colspan="3">&nbsp;<bean:write name="<%=strFormName%>" property="setname" /> </td>
					</tr>
					
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
						<%} %>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemHeader:message key="LBL_INITIATEDBY"/>:</b></td>
						<td>&nbsp;  <bean:write name="<%=strFormName%>" property="cname" />  </td>
						<td class="RightText" ><b><fmtItemHeader:message key="LBL_DATEINITIATED"/>: </b>&nbsp;<%=GmCommonClass.getStringFromDate(createdDate,strApplDateFmt)%> </td>
						<% if(strTxnTable.equals("ORDERS"))
						{ %>
						<td class="RightText" HEIGHT="20" align="left">&nbsp;<b><fmtItemHeader:message key="LBL_SURGERY_DATE"/>:</b>&nbsp;<fmtItemHeader:formatDate value="${frmItemControl.surgerydate}" pattern="<%=strApplDateFmt%>"/> </td>

						<%}%>
						
					</tr>
					
					<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="40"><b><fmtItemHeader:message key="LBL_COMMENTS"/>:</b></td>
						<td colspan="3" HEIGHT="40" valign="top" >&nbsp;<bean:write name="<%=strFormName%>" property="comments" /></td>
					</tr>
						<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>				
                     
                     <tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>	
                     	<%
	   if(strBillToFl.equals("YES"))
	   {
	%>
					<tr <%=strSetClr%>>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtItemHeader:message key="LBL_DISTNAME"/>:</b></td>
						<td colspan="3">&nbsp;<bean:write name="<%=strFormName%>" property="distname" /> </td>
					</tr>
	<%
	strSetClr="";
	   }
	%>
					
<%
if(!StrAction.equals("EditVerify")){
%>			
					<tr <%=strSetClr%>>
						<td class="RightText" align="right" HEIGHT="25"><b><%=strRelFlDesc%> ?:</b>
						</td>
<td colspan="5">&nbsp;<html:checkbox name="<%=strFormName%>" styleId="relverfl" property="relverfl" />   </td>
					</tr>
<%
}
%>					
				

				 </table>