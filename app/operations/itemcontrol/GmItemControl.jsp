<%
/*********************************************************************************************************
 * File		 		: GmItemControl.jsp
 * Desc		 		: This screen is used to control items of transaction
 * Version	 		: 1.0
 * author			: Gopinathan
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtItemControl" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmItemControl.jsp -->
<fmtItemControl:setLocale value="<%=strLocale%>"/>
<fmtItemControl:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>

<bean:define id="isTxnVerified" name="frmItemControl" property="isTxnVerified" type="java.lang.String"> </bean:define>
<bean:define id="strTxnIds" name="frmItemControl" property="txnid" type="java.lang.String"></bean:define>
<bean:define id="strTxnType" name="frmItemControl" property="txntype" type="java.lang.String"></bean:define>
<bean:define id="contype" name="frmItemControl" property="contype" type="java.lang.String"></bean:define>
<bean:define id="inserFl" name="frmItemControl" property="inserFl" type="java.lang.String"></bean:define>
<bean:define id="transInsertFl" name="frmItemControl" property="transInsertFl" type="java.lang.String"></bean:define>
<bean:define id="alInsertReturn" name="frmItemControl" property="alInsertDetails" type="java.util.ArrayList"></bean:define>
<% 
String strOperationsItemcntrlJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_ITEMCONTROL");
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO", strSessCompanyLocale);
String strHeaderRule="";
String strHeader="Transaction";
String strSubmit =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUBMIT"));
HashMap hmTransRules = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("TRANS_RULES"));
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
String strTransactionCode =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("VALIDATE_TXN_LIST"));
strCountryCode = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
String strConType = GmCommonClass.parseNull(request.getParameter("contype"));
String strOpenControl = GmCommonClass.parseNull(GmCommonClass.getString("MODIFY_ORDER_OPEN_CONTROL")); 
strHeaderRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_HEADER"));
String strPickLocRule = GmCommonClass.parseNull((String)hmTransRules.get("PICK_LOC"));
String strPutLocRule = GmCommonClass.parseNull((String)hmTransRules.get("PUT_LOC"));
String strCancelType = GmCommonClass.parseNull((String)hmTransRules.get("CANCEL_TYPE"));
String strControlNoScan = GmCommonClass.parseNull((String)GmCommonClass.getString("SCAN_CONTROL_NO"));
String strSubPickLocRule = "fnSubmit('"+strPickLocRule+"');";
String strSubPutLocRule = "fnSubmit('"+strPutLocRule+"');";
String strPrintPick = "fnPrintPick('"+strTxnIds+"');";
if(!strHeaderRule.equals(""))
	   strHeader = strHeaderRule;

/* The following code is added for Disableing the VOID button for IHLN[9110] Transaction Type */
String strDisable   = "";
String strTransType = GmCommonClass.parseNull((String)hmTransRules.get("TRANS_ID"));
if(strTransType.equals("9110")){
	strDisable = "true";
}

//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	

%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Status Log Detail</TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsItemcntrlJsPath%>/GmItemControl.js"></script>
<script language="JavaScript" src="<%=strOperationsItemcntrlJsPath%>/GmInsertProcess.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>
<bean:define id="StrAction" name="frmItemControl" property="haction" type="java.lang.String"></bean:define>
<bean:define id="StrStatus" name="frmItemControl" property="statusfl" type="java.lang.String"></bean:define>
<bean:define id="strFGBinFl" name="frmItemControl" property="strFGBinFl" type="java.lang.String"></bean:define>

<script>
var TransactionCode = '<%=strTransactionCode%>';
var FGBinFl = '<%=strFGBinFl%>';
var statusFl = '<%=StrStatus%>';
var cancelType = '<%=strCancelType%>';
var companyInfoObj = '<%=strCompanyInfo%>';
var strTxnType = '<%=strTxnType%>';
var StrAction = '<%=StrAction%>'; 
var InsertFl = '<%=inserFl%>'; 
var TransInsertFl = '<%=transInsertFl%>'; 
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnPageLoad();">

<html:form styleId="frmItemControl">
	<html:hidden name="frmItemControl" styleId="haction" property="haction" />
	<html:hidden name="frmItemControl" styleId="inputString" property="inputString"/>
	<html:hidden name="frmItemControl" styleId="fgBinString" property="fgBinString"/>
    <html:hidden name="frmItemControl" styleId="txnid" property="txnid" />
    <html:hidden name="frmItemControl" styleId="txntype" property="txntype" />
    <html:hidden name="frmItemControl" styleId="loctype"  property="loctype" />
    <html:hidden name="frmItemControl" styleId="statusfl" property="statusfl" />   
    <html:hidden name="frmItemControl" styleId="pickputfl" property="pickputfl" />
    <html:hidden name="frmItemControl" styleId="contype" property="contype" />
    <html:hidden name="frmItemControl" styleId="refid" property="refid" />
    <html:hidden name="frmItemControl" styleId="chkRule" property="chkRule" value="No"/>
    <input type="hidden" value="<%=strTxnIds%>" name="hConsignId" id="hConsignId">
    <input type="hidden" value="<%=strTxnIds%>" name="hTxnId" id="hTxnId">
    <input type="hidden" name="ruleVerify" value="true">
    <input type="hidden" name="partMaterialType" id="partMaterialType" value= "">
    <input type="hidden" name="TXN_TYPE_ID" value=<bean:write name="frmItemControl" property="txntype"/>>
	<table class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<logic:equal name="frmItemControl" property="haction" value="EditControl">
				<td height="25" class="RightDashBoardHeader"><fmtItemControl:message key="LBL_EDITLOT_HEADER"/>: <%=strHeader%></td>
			</logic:equal>
			<logic:equal name="frmItemControl" property="haction" value="EditVerify">
				<td height="25" class="RightDashBoardHeader"><fmtItemControl:message key="LBL_EDITLOT_HEADER1"/>: <%=strHeader%></td>
			</logic:equal>
			<fmtItemControl:message key="LBL_HELP" var="varHelp"/>
			<td align="right" class=RightDashBoardHeader><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("LOCATION_PART_MAPPING")%>');" />
			</td>
		</tr>

		<tr>
			<td colspan="2">
				<jsp:include page="/operations/itemcontrol/GmItemHeaderInclude.jsp">
				<jsp:param name="FORMNAME" value="frmItemControl" />
				</jsp:include>
			</td>
		</tr>
<%
if(StrAction.equals("EditControl")){
	if(strFGBinFl.equalsIgnoreCase("YES") && !strTransType.equals("4112")){ //4112:In House Consignment, No need to show the FG Bin details for Ih-House Consignments, Needed for Orders, Item Consignment and FGLE
%>
 <!-- To include the FG BIN Section-->
		<tr>
		<td colspan="2">
			<jsp:include page="/operations/itemcontrol/GmFGBINDtlInclude.jsp" >
			<jsp:param name="FORMNAME" value="frmItemControl" />
			</jsp:include>
		</td>
     </tr>
<%
	}if(strControlNoScan.equals("YES")){
%>
 <!-- To include the Control Number Section-->
		<tr>
		<td colspan="2">
			<jsp:include page="/operations/itemcontrol/GmControlNoInclude.jsp" >
			<jsp:param name="FORMNAME" value="frmItemControl" />
			</jsp:include>
		</td>
     </tr>
  <%} %>  
       <tr>
		<td colspan="2">
			<jsp:include page="/common/GmIncludeLog.jsp" >
			<jsp:param name="FORMNAME" value="frmItemControl" />
			<jsp:param name="ALNAME" value="alLogReasons" />
			<jsp:param name="LogMode" value="Edit" />
			</jsp:include>
		</td>
     </tr>
<!-- Insert Section will be availabe only for the company is enabled for the Inserts and the Transactions is enabled for Inserts -->     
 	<%
		int intSize = alInsertReturn.size();
     %>
     <% if(inserFl.equals("Y") && transInsertFl.equals("Y") && intSize > 0){ %>
     	<tr>
			<td colspan="2">
				<jsp:include page="/operations/itemcontrol/GmInsertsInclude.jsp">
				<jsp:param name="FORMNAME" value="frmItemControl" />
				</jsp:include>
			</td>
		</tr>
	 <% } %>
		<tr>
			<td colspan="2">
				<jsp:include page="/operations/itemcontrol/GmItemDetailInclude.jsp">
				<jsp:param name="FORMNAME" value="frmItemControl" />
				</jsp:include>
			</td>
		</tr>
<%
}else if(StrAction.equals("EditVerify")){
%>
		<tr>
			<td colspan="2">
				<jsp:include page="/operations/itemcontrol/GmItemVerifyDtlInclude.jsp">
				<jsp:param name="FORMNAME" value="frmItemControl" />
				</jsp:include>
			</td>
		</tr>
<%
strSubmit = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VERIFY"));
} 
%>
<fmtItemControl:message key="BTN_VOID" var="varBtnVoid"/>
<fmtItemControl:message key="BTN_PICK_CLIP" var="varPickSlip"/>
<fmtItemControl:message key="BTN_PRINT_PAPERWORK" var="varPrintPaperwork"/>
<%if(strCountryCode.equals("en")){ %>
	<%if(!((StrStatus.equals("3") && StrAction.equals("EditControl")) ||(StrStatus.equals("4") && StrAction.equals("EditVerify")))){ %>
		<tr>
			<td colspan="2" align="center" height="30">
			<%if(StrAction.equals("EditControl")){ %>
				<gmjsp:button value="<%=strSubmit%>"  name="Btn_Submit" gmClass="button" onClick="<%=strSubPickLocRule%>" buttonType="Save" />&nbsp;&nbsp;
			<%}else if(StrAction.equals("EditVerify")){ %>
				<gmjsp:button value="<%=strSubmit%>"  name="Btn_Submit" gmClass="button" onClick="<%=strSubPutLocRule%>" buttonType="Save" />&nbsp;&nbsp;
			<%}%>
				<gmjsp:button value="&nbsp;${varBtnVoid}&nbsp;" disabled="<%=strDisable%>" name="Btn_Submit" gmClass="button" onClick="fnVoid();" buttonType="Save" />&nbsp;&nbsp; 
				
		 <% if(!strTxnType.equals("106760") && !StrStatus.equals("1")) {%> <!--106760-Order; Status-PCN -->
	      <gmjsp:button value="UnAssign"  disabled="true" name="UnAssign" gmClass="button" onClick="fnUnAssignTxn()" buttonType="Load"/>
	      <%}else{ %>
				<gmjsp:button value="UnAssign"  disabled="false" name="UnAssign" gmClass="button" onClick="fnUnAssignTxn()" buttonType="Load"/> 
			<%}%>
						
	<% if(strTxnType.equals("50261") && StrStatus.equals("2")) {%>
	<tr>
		<td colspan="2" align="center" height="30">
	    <gmjsp:button value="&nbsp;${varPickSlip}&nbsp;" controlId="Btn_Pick_Slip" name="Btn_Pick_Slip" gmClass="button" onClick="<%=strPrintPick%>" buttonType="Load" /> 
	<%} %>
			</td>
		</tr>
	<%}else if(strTxnType.equals("103932") && StrStatus.equals("4")){%>	
		<tr>
			<td colspan="2" align="center" height="30">
			    <gmjsp:button value="&nbsp;${varPickSlip}&nbsp;" controlId="Btn_Pick_Slip" name="Btn_Pick_Slip" gmClass="button" onClick="fnPaperWork" buttonType="Load" /> 
    <%} %>
			</td>
		</tr>
<!-- 	 The following Block of code added for other than the US portals as part of BUG-3685  -->
<%}else if(!strCountryCode.equals("en")){ %>
    <%if(!isTxnVerified.equals("YES") && (strOpenControl.equalsIgnoreCase("YES") ||  !((StrStatus.equals("3") && StrAction.equals("EditControl")) ||(StrStatus.equals("4") && StrAction.equals("EditVerify"))))){%>
        <tr>
		<td colspan="2" align="center" height="30">
			<%if(StrAction.equals("EditControl")){ %>
					<gmjsp:button value="<%=strSubmit%>"  name="Btn_Submit" gmClass="button" onClick="<%=strSubPickLocRule%>" buttonType="Save" />&nbsp;&nbsp;
					<gmjsp:button value="&nbsp;${varBtnVoid}&nbsp;" disabled="<%=strDisable%>" name="Btn_Submit" gmClass="button" onClick="fnVoid();" buttonType="Save" />
						<%if(strTransType.equals("93341") && StrStatus.equals("3")){ %>	
 							<gmjsp:button value="&nbsp;${varPrintPaperwork}&nbsp;" controlId="Btn_Pick_Slip" name="Btn_Pick_Slip" gmClass="button" onClick="fnPaperWork();" buttonType="Load" />
						<%}%>
			<%}else if(StrAction.equals("EditVerify")){ %>
				<%if(StrStatus.equalsIgnoreCase("3")){ %>
					<gmjsp:button value="<%=strSubmit%>"  name="Btn_Submit" gmClass="button" onClick="<%=strSubPutLocRule%>" buttonType="Save" />&nbsp;&nbsp;
					<gmjsp:button value="&nbsp;${varBtnVoid}&nbsp;" disabled="<%=strDisable%>" name="Btn_Submit" gmClass="button" onClick="fnVoid();" buttonType="Save" />
				<%}%>
				
					<gmjsp:button value="&nbsp;${varPrintPaperwork}&nbsp;" controlId="Btn_Pick_Slip" name="Btn_Pick_Slip" gmClass="button" onClick="fnPaperWork();" buttonType="Load" />
			<%} %>
	<%} %>
<%} %>
<%if(strTxnType.equals("4110") && StrStatus.equals("3")) {
	String strPicSlip = "fnPicSlip('"+strTxnIds+"','"+strTxnType+"','','T');";   //Since this pickslip button is using only for consignment we can send consignment type as 'T'
%>
	<tr>
		<td colspan="2" align="center" height="30">
	    <gmjsp:button value="&nbsp;${varPickSlip}&nbsp;" controlId="Btn_Pick_Slip" name="Btn_Pick_Slip" gmClass="button" onClick="<%=strPicSlip%>" buttonType="Load" /> 
	<%} %>
			</td>
		</tr>
</table>
</html:form>
<!-- To calculate the total parts cnt and Scanned cnt after the page loaded(PMT-39131) -->
<% if(StrAction.equals("EditControl") && strControlNoScan.equals("YES")){%>
    	<script>
    	fnExistScanPartCount();
		</script> 
		
		<%}%>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>