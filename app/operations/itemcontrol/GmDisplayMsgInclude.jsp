<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList,java.util.List,java.util.Vector,java.util.HashMap,com.globus.common.beans.GmCommonClass" %>
<%@ taglib prefix="fmtValidation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmDisplayMsgInclude.jsp -->
<fmtValidation:setLocale value="<%=strLocale%>"/>
<fmtValidation:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>

<%
String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css"> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>

<form>
<table cellspacing="0" cellpadding="0" border="0">
	<tr id="displayMsg" style="display: none;">
		<td colspan="3">		              
			<table>
			<tr><td class="Line" height="1"></td></tr>
			<tr>
				<td class="RightText" bgcolor="#eeeeee"  height="25">
					<b><fmtValidation:message key="LBL_VALIDATIONMESSAGE"/></b>
				</td>
			</tr>
			<tr><td class="Line" height="1"></td></tr>
			<tr>
				<td class="RightText" height="25">
					<fmtValidation:message key="LBL_MESSAGE"/>
				</td>
			</tr>
			</table>                  			
		</td>
	</tr>
</table>
</form>
