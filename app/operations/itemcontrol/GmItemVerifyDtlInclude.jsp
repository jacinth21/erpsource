<%@page import="java.util.StringTokenizer"%>
<%
/*********************************************************************************************************
 * File		 		: GmItemVerifyDtlInclude.jsp
 * Desc		 		: This screen is used to show Item Details 
 * Version	 		: 1.0
 * author			: Gopinathan
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtItemVerify" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmItemVerifyDtlInclude.jsp -->
<fmtItemVerify:setLocale value="<%=strLocale%>"/>
<fmtItemVerify:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>
<%
String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<%

GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO", strSessCompanyLocale);
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
strFormName = (strFormName.equals(""))? "frmItemControl" : strFormName;

HashMap hmTransRules = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("TRANS_RULES"));
String strPickLocRule = GmCommonClass.parseNull((String)hmTransRules.get("PICK_LOC"));
String strPutLocRule = GmCommonClass.parseNull((String)hmTransRules.get("PUT_LOC"));
String strRuleType =GmCommonClass.parseNull((String) hmTransRules.get("TRANS_RLTXN"));
String strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
String strRedesigPartTrans = GmCommonClass.parseNull((String) hmTransRules.get("RE-DESIG PART"));
String strRuleTransType = "";
if(strRuleType!=null && !strRuleType.equals("")){
	strRuleTransType = strRuleType;
}
String strColOrgQtyHead = ""; 
String strColQtyHead = "";
String colwidth = "250";
String colDeswidth = "310";

String strRetMsg = ""; 
if(strPutLocRule.equals("YES")){
	colwidth = "200";
	colDeswidth = "270";
}

%>
<script>
var vIncludeRuleEng = '<%=strIncludeRuleEng.toUpperCase()%>';
</script>

<bean:define id="alReturn"  name="<%=strFormName%>" property="aldetailinfo" type="java.util.ArrayList"></bean:define>
<bean:define id="alLocDtl" name="<%=strFormName%>" property="allocdtl" type="java.util.ArrayList"></bean:define>
<bean:define id="strPnums" name="<%=strFormName%>" property="strpnums" type="java.lang.String"></bean:define>
<bean:define id="StrAction" name="<%=strFormName%>" property="haction" type="java.lang.String"></bean:define>
<bean:define id="StrStatus" name="<%=strFormName%>" property="statusfl" type="java.lang.String"></bean:define>
<bean:define id="txnid" name="<%=strFormName%>" property="txnid" type="java.lang.String"></bean:define>
<bean:define id="retMsg" name="<%=strFormName%>" property="retMsg" type="java.lang.String"></bean:define>
<%
if(StrStatus.equals("4")){
	strColOrgQtyHead = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PUT_QTY")); 
	strColQtyHead = "";
}else{
	strColOrgQtyHead = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PICKED_QTY"));
	strColQtyHead  =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QTY"));
}

int alLocSize = alLocDtl.size();
String strOptionString = "";
String strChooseOne = "<option value=\"0\" >[Choose One]</option>";
String strTempPnum = "";
StringBuffer sbPartNum = new StringBuffer();

if (!retMsg.equals("")) {
		StringTokenizer stTokens = new StringTokenizer(retMsg, ",");
		while (stTokens.hasMoreTokens()) {
			String strToken = GmCommonClass.parseNull((String) stTokens.nextToken());
			if (!strToken.equals("")) {
				strRetMsg += "<a href='#' onclick='javascript:fnPicSlip(\"" + strToken + "\")'>" + strToken + "</a>,"; 
			}
		}
		if (!strRetMsg.equals("")) {
			strRetMsg = strRetMsg.substring(0, strRetMsg.length() - 1);
		}
	}
	
if(alLocSize!=0){
	for (int i=0; i<alLocSize; i++){
		HashMap hmloc = (HashMap)alLocDtl.get(i);
		if(i==0){
			strTempPnum = GmCommonClass.parseNull((String)hmloc.get("PNUM"));
		}
		String strPnum = GmCommonClass.parseNull((String)hmloc.get("PNUM"));
		
		if(!strTempPnum.equals(strPnum)){
			strOptionString = strChooseOne + strOptionString;
%>
			<script>put('<%=strTempPnum%>','<%=strOptionString%>');</script>
<%
			sbPartNum.append(strTempPnum).append(",");
			strTempPnum = strPnum;
			strOptionString = "";
		}
		String strLocID= GmCommonClass.parseNull((String)hmloc.get("LOCATIONID"));
		String strLocCD= GmCommonClass.parseNull((String)hmloc.get("LOCATIONCD"));
		String strLocQty= GmCommonClass.parseNull((String)hmloc.get("CURRQTY"));
		strOptionString += " <option value=\""+strLocID+"\">"+strLocCD+" - ("+strLocQty+")"+"</option> ";
		if(i == (alLocSize-1)){
		
			strOptionString = strChooseOne + strOptionString;
%>
			<script>put('<%=strPnum%>','<%=strOptionString%>');</script>
<%
			sbPartNum.append(strPnum).append(",");
		}
%>
<%
	}
}
%>
<html:hidden name="<%=strFormName%>" styleId="strpnums" property="strpnums"/>
<input type="hidden"  id="RE_FORWARD" name="RE_FORWARD" value="GmItemVerify">
<input type="hidden" id="txnStatus" name="txnStatus" >
<input type="hidden" id="RE_TXN" name="RE_TXN" value="<%=strRuleTransType%>">
<input type="hidden" id="reDesigTrans" name="reDesigTrans" value="<%=strRedesigPartTrans%>">

<table width="900px" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="6" class="Line" height="1"></td>
			</tr>
			<tr class="ShadeRightTableCaption">
				<td width="80" height="25">&nbsp;<fmtItemVerify:message key="LBL_PART"/></td>
				<td width="<%=colDeswidth%>" >&nbsp;<fmtItemVerify:message key="LBL_DESCRIPTION"/></td>
				<td width="39" align="center"><%=strColOrgQtyHead%></td>
				<td width="<%=colwidth%>" >&nbsp;&nbsp;&nbsp;<%=strColQtyHead%>&nbsp;&nbsp;
				 &nbsp;&nbsp;<fmtItemVerify:message key="LBL_CONTROL"/>&nbsp;
				<%-- &nbsp;&nbsp;Location&nbsp;--%>
<%
				if(strPutLocRule.equals("YES")){
%>
					<td width="180" align="center"><fmtItemVerify:message key="LBL_LOCATION"/>
					</td>
<%
				} 
%>
	
			</tr>
			<tr>
				<td colspan="6" class="Line" height="1"></td>
			</tr>
			<tr>
				<td colspan="6">
					<table width="899" cellspacing="0" cellpadding="0">
<%
						int intSize = alReturn.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strPrevPartNum = "";
							int intCount = 0;
							String strLine = "";
							String strPartNumHidden = "";
							String strPartDesc = "";
							String strPrice = "";
							String strPartNum = "";
							String strToPartNum = "";
							String strControl = "";
							String strPrevControl = "";
							String strItemQty = "";
							double dbItemQtyHidden = 0;
							String strSQty = "";
							String strMapLocId = "";
							String strTagFl = "";

							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alReturn.get(i);
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
								strPartNumHidden = strPartNum;
								strToPartNum = GmCommonClass.parseNull((String)hmLoop.get("TOPART"));
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("PICKQTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								strTagFl = GmCommonClass.parseNull((String)hmLoop.get("TAGFL"));
								if(StrAction.equals("EditVerify") && (!StrStatus.equals("4"))){
									strMapLocId = GmCommonClass.parseZero((String)hmLoop.get("SUGLOC"));
								}else{
									strMapLocId = GmCommonClass.parseZero((String)hmLoop.get("LOCATION"));
								}
								strLine = "<TR><TD colspan=6 height=1 bgcolor=#eeeeee></TD></TR>";
								
								if(!strPrevPartNum.equals(strPartNum) && i != 0){
%>
										<input type="hidden" id="hQty<%=strPrevPartNum.replaceAll("\\.","")%>" name="hQty<%=strPrevPartNum.replaceAll("\\.","")%>" value="<%=dbItemQtyHidden%>">
<%
										dbItemQtyHidden = 0;
								}
								dbItemQtyHidden += Double.parseDouble(strItemQty);
								if((i==intSize-1)){
%>
									<input type="hidden" id="hQty<%=strPartNum.replaceAll("\\.","")%>" name="hQty<%=strPartNum.replaceAll("\\.","")%>" value="<%=dbItemQtyHidden%>">
<%
								}
								if((strPrevPartNum.equals(strPartNum)&& strPrevControl.equals(strControl)) || (strPrevPartNum.equals(strPartNum) && StrStatus.equals("4"))){

										intCount++;
										strPartNum = "";
										strPartDesc = "";
										//strItemQty = "";
										strLine = "";
								}

								strPrevPartNum = strPartNumHidden;
								strPrevControl = strControl;
								out.print(strLine);
%>
								<tr>
<%
							if(!StrStatus.equals("4")){
%>
									<td width="80" align="center">&nbsp;
									<%if(strTagFl.equals("Y")){%>
										<a href=javascript:fnOpenTag('<%=strPartNum %>','<%=txnid%>')><img src=<%=strImagePath%>/tag.jpg border="0"></a>
									<%}%><a class="RightText" tabindex=-1 href="javascript:fnSplit('<%=i%>','<%=strPartNum%>','<%=strPutLocRule%>','<%=strItemQty%>');"><%=strPartNum%></a></td>
<%
							}else{
%>
									<td width="60" align="center">&nbsp;<%if(strTagFl.equals("Y")){%><a href=javascript:fnOpenTag('<%=strPartNum %>','<%=txnid %>')> <img src=<%=strImagePath%>/tag.jpg border="0"></a><%}%><%=strPartNum%></td>
<%
							}
%>
									<td width="<%=colDeswidth%>">&nbsp;<%=strPartDesc%></td>
									<td width="75"align="center"><%=strItemQty%></td>
									<td id="Cell<%=i%>" width="<%=colwidth%>">&nbsp;
<%
							if(!StrStatus.equals("4")){
%>									

									&nbsp;&nbsp;<input type="text" size="2" id="Txt_Qty<%=i%>" Style ="width:26px;margin-left:-15px;" name="Txt_Qty<%=i%>" value="<%=strItemQty%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
<%
							}
%>									
									&nbsp;&nbsp;&nbsp;
									&nbsp;<%=strControl%>&nbsp;
									</td>
<%
									if(strPutLocRule.equals("YES")){
%>
										<td id="locCell<%=i%>" width="180"><span id="locDiv<%=i%>">
										</span></td>
<%
									}

	if(!strRedesigPartTrans.equalsIgnoreCase("Y")){
%>
                                    
                                    <input type="hidden" id="hPartNum<%=i%>" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
<%
	}
%>                                
									<input type="hidden" id="hPNumReDesig<%=i%>" name="hPNumReDesig<%=i%>" value="<%=strPartNumHidden%>">
									<input type="hidden" id="hPrice<%=i%>" name="hPrice<%=i%>" value="<%=strPrice%>">
									<input type="hidden" id="Txt_CNum<%=i%>" name="Txt_CNum<%=i%>" value="<%=strControl%>">
									<input type="hidden" id="hControl<%=i%>" name="hControl<%=i%>" value="<%=strControl%>">
								</tr>
<%
								if(strPutLocRule.equals("YES")){
									//As of now PTRD-XXXX Trans need to pass to part num value here.
									if(strRedesigPartTrans.equalsIgnoreCase("Y")){ 
									strPartNumHidden = strToPartNum;
%>
                                   <input type="hidden" id="hPartNum<%=i%>" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
<%
									}
%>                                   
										<script>
											fnShowLocDrpDwn('<%=strPartNumHidden%>','<%=i%>','<%=strMapLocId%>');
										</script>
<%									
								}
							}//for loop end
						}else{
%>
							<tr><td colspan="6" height="50" align="center" class="RightTextRed"><BR><fmtItemVerify:message key="LBL_NOPARTINORDER"/></td></tr>
<%
						}		
%>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="6" class="Line" height="1"></td>
				</tr>
</table>
<input type="hidden" id="hCnt" name="hCnt" value="<%=intSize-1%>">
<input type="hidden" id="hNewCnt" name="hNewCnt" value="0" ><%--This hidden field used to keep java script  var cnt value  --%>
<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">
<%
		if (!retMsg.equals("")) {
	%>
	<tr>
		<td colspan="6" height="30" align="center"
			style="vertical-align: middle;"><fmtItemVerify:message key="LBL_RETMSG1"/> <span style="color: blue;font-weight: bold;"> <%=strRetMsg%> </span><fmtItemVerify:message key="LBL_RETMSG2"/></td>
	</tr>
	<%
		}
	%>
		<tr>
			<td td colspan="4">
					<jsp:include page="/common/GmRuleDisplayAjaxInclude.jsp"/>
			</td>
		</tr>
</table>


