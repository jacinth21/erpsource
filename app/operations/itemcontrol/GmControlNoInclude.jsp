 <%
/*********************************************************************************************************
 * File		 		: GmControlNoInclude.jsp
 * Desc		 		: This is used to show the control no details section
 * Version	 		: 1.0
 * author			: aprasath
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtCtrlNoInc" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmControlNoInclude.jsp -->
<fmtCtrlNoInc:setLocale value="<%=strLocale%>"/>
<fmtCtrlNoInc:setBundle basename="properties.labels.operations.itemcontrol.GmControlNoInclude"/>
<% 
String strOperationsItemcntrlJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_ITEMCONTROL");
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Control No details</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsItemcntrlJsPath%>/GmControlNoInclude.js"></script>
<script language="JavaScript" src="<%=strOperationsItemcntrlJsPath%>/GmControlNoScanCount.js"></script>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<input type="hidden" name="txnid" value="">
	
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	
		<tr>
			<td height="24" class="ShadeRightTableCaption" colspan="5">&nbsp;<b><fmtCtrlNoInc:message key="LBL_CONTROL"/></b></td>
		</tr>
		<tr><td class="Line" colspan="5"><embed src="sounds/beep.wav" autostart="false" width="0" height="0" id="beep" enablejavascript="true"></td></tr> 
		<tr height="30">
			<td class="RightText" HEIGHT="20" align="right" width="30%"><b><fmtCtrlNoInc:message key="LBL_SCAN_ENTER_CONTROL"/>:</b></td>
			<td width="15%">&nbsp;<input type="text" name="controlNo" id="controlNo" class="InputArea" style="text-transform:uppercase;" maxlength="40" onkeypress="javascript:fnAddControlNo(this);" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);"/> </td>
			<td width="15%">&nbsp;
			<fmtCtrlNoInc:message key="BTN_ADD" var="varAdd"/>
			<gmjsp:button name="Btn_Cno_Add" value="&nbsp;&nbsp;&nbsp;${varAdd}&nbsp;&nbsp;&nbsp;" gmClass="Button" buttonType="Load" onClick="javascript:fnAddControlNo(this);"/></td>
			<td width="20%">&nbsp;</td>
			<td width="30%">&nbsp;</td>
		</tr>
		<tr><td colspan="5" align="center"><div id="errormessage" width="100%" ></div></td></tr>
		<tr><td class="LLine" colspan="5"></td></tr> 
		<tr height="30">
			<td class="RightText" HEIGHT="20" align="right" width="20%"><b><fmtCtrlNoInc:message key="LBL_TOTAL_PARTS"/>:</b></td>
			<td width="20%" align="left" nowrap class="RightText" id="">&nbsp; <input type="text" size="10" style="border:none;" readonly id="lblTotalP" name="lblTotalP" value="" ></td>
			<td class="RightText" HEIGHT="20" align="right" width="20%"><b><fmtCtrlNoInc:message key="LBL_TOTAL_SCANNED_PARTS"/>:</b></td>
			<td width="20%" align="left" nowrap class="RightText" id="">&nbsp;<b><input type="text" size="10" style="border:none;" readonly id="lblTotalS" name="lblTotalS" value="" ></b></td>
			<td width="20%">&nbsp;</td> 
		</tr>
	
		<tr><td class="LLine" colspan="5"><input type="hidden" id="hErrFlag" name="hErrFlag" value=""></td></tr> 
							

</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>