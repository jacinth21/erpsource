<%@ include file="/common/GmHeader.inc"%>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList,java.util.List,java.util.Vector,java.util.HashMap,com.globus.common.beans.GmCommonClass" %>

<%@ taglib prefix="fmtValidation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmValidationDisplayInclude.jsp -->
<fmtValidation:setLocale value="<%=strLocale%>"/>
<fmtValidation:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>
<%
String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
String strErrCnt = (String)request.getParameter("errCnt");
int intErrCnt = Integer.parseInt(GmCommonClass.parseZero(strErrCnt));
ArrayList alResult = new ArrayList();
alResult = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alReturn"));

int rows = alResult.size();
HashMap hmRow = new HashMap();
String rowClass = "";
String strPNUM="";
String strCtrlNum="";
String strErrMsg="";
String strValidMsg = "";
String strErrFl = "N";
for(int i=0; i<rows; i++)
{
	hmRow = (HashMap)alResult.get(i);
	strValidMsg = String.valueOf(hmRow.get("ERRORDTLS"));
	if(!strValidMsg.equals("") && !strValidMsg.equals("null")){
	  strErrFl = "Y";
	  break;
	}
}

%>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css"> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>

<script type="text/javascript">
//Function to open the Lot Code Report Screen by click on the Lot Number
function fnViewLotReport(lotNum){
	windowOpener(" /gmLotCodeRptAction.do?method=loadLotCodeProdInfo&hscreen=popup&lotNum="+lotNum,"LotCOdeReport","resizable=yes,scrollbars=yes,top=150,left=150,width=1020,height=600");
}
</script>

<form>
<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td colspan="3">		
<% 
      if(intErrCnt > 0 && strErrFl.equals("Y"))
      {
%>                
			<table><tr><td class="RightText" bgcolor="#eeeeee"  height="25">
			<b><fmtValidation:message key="LBL_VALIDATIONMESSAGE"/></b>
			</td></tr>
			<tr><td  class="Line" height="1"></td></tr>
			</table>                  			
			<div style="overflow:auto; height:250px;width:100%;" id="msgDiv">
			<Table border="0" class="its" cellspacing="0" cellpadding="0" id="rt">
			
			<tr>
				<thead>
					<tr height="25" class="RightDashBoardHeader" style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<th nowrap class="RightText" align="left" width="30px">&nbsp; </th>
						<th nowrap class="RightText" align="left" width="80px">&nbsp;<fmtValidation:message key="LBL_PART"/></th>
						<th nowrap class="RightText" align="left" style="width:250px">&nbsp;<fmtValidation:message key="LBL_LOT"/></th>
						<th nowrap class="RightText" align="center">&nbsp;<fmtValidation:message key="LBL_VALIDATION"/></th>
					</TR>
				</thead>
				<tbody id="ruleBody">
				  <% 
				    for(int i=0; i<rows; i++)
				    {
				    	hmRow = (HashMap)alResult.get(i);
				    	strPNUM	= GmCommonClass.parseNull(String.valueOf(hmRow.get("ID")));
				    	strPNUM = strPNUM.equals("null")?GmCommonClass.parseNull(String.valueOf(hmRow.get("PNUM"))):strPNUM;
				    	strPNUM = strPNUM.equals("null")?"":strPNUM;
				    	strCtrlNum = String.valueOf(hmRow.get("CNUM"));
				    	strErrMsg = String.valueOf(hmRow.get("ERRORDTLS"));
				    	rowClass = i%2 == 0 ? "odd" : "even";
				    	if(!strErrMsg.equals("") && !strErrMsg.equals("null")){
				    %>
				    
				      <tr class="<%=rowClass%>" id="rtrow<%=i%>" height="20">
				        <fmtValidation:message key="IMG_LOTCODEREPORT" var = "varLotCodeReport"/>
				        <td><a href="javascript:fnViewLotReport('<%=strCtrlNum %>')"><img height="15" border="0" title="${varLotCodeReport}" src="<%=strImagePath%>/location.png"></img></a></td>
				        <td><%=strPNUM %></td>
				        <td><%=strCtrlNum %></td>
				        <td><%=strErrMsg.replaceAll(";", "<br>") %></td>
				      </tr>
				      <%	      
				    	}
				    }
				  %>
				 
				</tbody>	
			</tr>
			
			</Table>
			</div>
<%} %>
		</td>
	</tr>
</table>
<script type="text/javascript">

var rowsize = '<%=intErrCnt%>';
var obj = document.getElementById("msgDiv");
if(obj != null)
{
	if(rowsize < 1)
	{	
		obj.style.visibility = 'hidden';
		obj.style.display = 'none';
	}
	else
	{
		obj.style.visibility = 'visible';
		obj.style.display = 'inline';
	}
}
</script>
</form>
