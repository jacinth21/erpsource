 <%
/*********************************************************************************************************
 * File		 		: GmFGBINDtlInclude.jsp
 * Desc		 		: This is used to show the FGBIN details section
 * Version	 		: 1.0
 * author			: arajan
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtFGBIND" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmFGBINDtlInclude.jsp -->
<fmtFGBIND:setLocale value="<%=strLocale%>"/>
<fmtFGBIND:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>

<% 
String strOperationsItemcntrlJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_ITEMCONTROL");
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: FG Bin details</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsItemcntrlJsPath%>/GmFGBINDtlInclude.js"></script>
<bean:define id="strFGBinList" name="<%=strFormName%>" property="strFGBinList" type="java.lang.String"></bean:define> 

<script>
var FGBinIdList = '<%=strFGBinList%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnSetFGBinId();">
<html:hidden name="<%=strFormName%>" styleId="txnid" property="txnid" />
	
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	
		<tr>
			<td height="24" class="ShadeRightTableCaption" colspan="5">&nbsp;<b><fmtFGBIND:message key="LBL_FGBIN"/></b></td>
		</tr>
		<tr><td class="Line" colspan="5"></td></tr> 
		<tr height="40">
			
			<td class="RightText" HEIGHT="20" align="right" width="31%">&nbsp;<b><fmtFGBIND:message key="LBL_SCANFGBIN"/>:</b></td>
			<td width="18%">&nbsp;<html:text property="scanId" styleId="scanId" styleClass="InputArea" style="text-transform:uppercase;" name="<%=strFormName%>" maxlength="20" onkeypress="javascript:fnAddFGBinId(this);" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" tabindex="1"/></td>
			<fmtFGBIND:message key="BTN_ADD" var="varBtnAdd"/>
			<td width="15%">&nbsp;<gmjsp:button name="Btn_Add" value="&nbsp;&nbsp;&nbsp;${varBtnAdd}&nbsp;&nbsp;&nbsp;" gmClass="Button" buttonType="Load" onClick="javascript:fnAddFGBinId(this);" tabindex="2"/></td>
			<td width="23%">&nbsp;<select name="combo" id="AssignedToCombo" size="2" multiple="multiple" style="width: 200px;" tabindex="3"></select></td>
			<fmtFGBIND:message key="BTN_REMOVE" var="varBtnRemove"/>
			<td width="30%">&nbsp;<gmjsp:button name="Btn_Remove" value="${varBtnRemove}" gmClass="Button" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" buttonType="Save" onClick="javascript:fnRemoveFGBinId(this);" tabindex="3"/></td>
			
		</tr>
		<tr><td class="LLine" colspan="5"></td></tr> 
							

</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>