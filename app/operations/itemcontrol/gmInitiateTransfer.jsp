<!-- operations\itemcontrol\gmInitiateTransfer.jsp -->

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtInitiateTransfer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtInitiateTransfer:setLocale value="<%=strLocale%>"/>
<fmtInitiateTransfer:setBundle basename="properties.labels.operations.itemcontrol.gmInitiateTransfer"/>
<%
String strOperationsItemcntrlJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_ITEMCONTROL");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsItemcntrlJsPath%>/GmInitiateReplenishment.js"></script>

<bean:define id="strTxnIds" name="frmGmInitiateTransfer" property="strTxnIds" type="java.lang.String"> </bean:define>
<bean:define id="gridData" name="frmGmInitiateTransfer" property="strXMLGrid" type="java.lang.String"> </bean:define>
<bean:define id="alWareHouse" name="frmGmInitiateTransfer" property="alWareHouse" type="java.util.ArrayList"> </bean:define>
<%
String strOpt = (String)GmCommonClass.parseNull((String)request.getAttribute("strOpt"));
if(!strTxnIds.equals("")){
	strTxnIds = strTxnIds.substring(1, strTxnIds.length());
}
%>
 
<title>Item Control</title>

<script>
var objGridData;
var lblWarehouse = '<fmtInitiateTransfer:message key="LBL_WAREHOUSE"/>';
objGridData ='<%=gridData%>';

var arrValue = new Array(<%=alWareHouse.size()%>);
<%
for(int i=0; i<alWareHouse.size();i++){
	HashMap hmValue = (HashMap)alWareHouse.get(i);
	String strID = GmCommonClass.parseNull((String)hmValue.get("ID"));
	String strType = GmCommonClass.parseNull((String)hmValue.get("WAREHOUSE_TYPE"));
%>
	arrValue[<%=i%>] = "<%=strID%>,<%=strType%>";
<%}%>
</script>
</head>
<form name="frmInitTransfer">
<body onload="javascript:fnOnPageLoad();" leftmargin="20" topmargin="10">
<input type="hidden" name="method" value="saveReplenishment" />
<input type="hidden" name="v_str" value="" />
<input type="hidden" name="hWareHouseType" value="" />
<input type="hidden" id="strOpt" name="strOpt" value="<%=strOpt%>" />
	<table class="DtTable700" cellspacing="0" cellpadding="0" >
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr><td colspan="2" >
			<table width="698" cellspacing="0" cellpadding="0" >
				<tr><td class="RightDashBoardHeader" ><fmtInitiateTransfer:message key="LBL_INITIATE_LOCATION_TRANSFER"/></td></tr>
				<tr><td  class="LLine" height="1"></td></tr>
				<tr id="dropdown"> <!-- Custom tag lib code modified for JBOSS migration changes -->
					
					<td height="25">&nbsp;<b><fmtInitiateTransfer:message key="LBL_WAREHOUSE"/></b>&nbsp;<gmjsp:dropdown controlName="warehouseid" SFFormName="frmGmInitiateTransfer" SFSeletedValue="warehouseid" SFValue="alWareHouse" codeId="ID" codeName="WHNM" onChange="fnRefreshGrid();" defaultValue="[Choose One]" tabIndex="1" /></td>
				</tr>
				<tr><td  class="LLine" height="1"></td></tr>
				<tr>
					<td class="RightTableCaption" align="left" colspan="2" height="20">
						<table cellpadding="1" cellspacing="1" id="addTbl">
							<tr>
								<td class="RightTableCaption">
									&nbsp;<a href="#"><fmtInitiateTransfer:message key="IMG_ALT_ADD_ROW" var="varAddRow"/> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:addRow()"height="14"></a>
									&nbsp;<a href="#"><fmtInitiateTransfer:message key="IMG_ALT_REMOVE_ROW" var="varRemoveRow"/> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
								</td>
							</tr>
												
						</table>
											
					</td>
				</tr>
				<tr id="gridData">
					<td colspan="2">
						<div id="dataGridDiv" height="400px" width="700px"></div>
					</td>
				</tr>
			<%if(!strTxnIds.equals("")){%> 
				<tr>
					<td align="center" colspan="2" >
						<div align="center"><font color="red"><strong><fmtInitiateTransfer:message key="LBL_INITIATED_FG_RW_REPLENISHMENT_TRANSACTIONS"/><%=strTxnIds%> </strong></font></div>
					</td>
				</tr>
			<%}else if(strOpt.equals("save") && strTxnIds.equals("")){%>
				<tr>
					<td align="center" colspan="2" >
						<div align="center"><font color="red"><strong><fmtInitiateTransfer:message key="LBL_FG_RW_REPLENISHMENT_TRANSACTIONS"/></strong></font></div>
					</td>
				</tr>
			<%}else{%>
				<tr id="strSubmit">
				<fmtInitiateTransfer:message key="BTN_SUBMIT" var="varSubmit"/>
				    <td colspan="2" align="center" height="30">&nbsp;<gmjsp:button name="Btn_Submit" style="width: 6em; height: 25" value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();" />
				    </td>
				</tr>
			<%}%>
				</table>
			</td>
		</tr>
	</table>
</body>
</form>
<%@ include file="/common/GmFooter.inc"%>	
</html>