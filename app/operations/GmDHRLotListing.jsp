 <%
/**********************************************************************************
 * File		 		: GmDHRLotListing.jsp  
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.servlets.GmCommonBarCodeServlet"%>


<%@ taglib prefix="fmtDHRLotListing" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRLotListing.jsp -->

<fmtDHRLotListing:setLocale value="<%=strLocale%>"/>
<fmtDHRLotListing:setBundle basename="properties.labels.operations.GmDHRLotListing"/>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	strCssPath = GmCommonClass.getString("GMSTYLES");
	strImagePath = GmCommonClass.getString("GMIMAGES");
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	strCommonPath = GmCommonClass.getString("GMCOMMON");
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmDHRDetails = new HashMap();
	HashMap hmDonorDtls = new HashMap();
	hmDonorDtls = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmDonorDtls"));
	ArrayList alLotNums = new ArrayList();
	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strPTFooter="";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyRej = "";
	String strManufDate = "";
	String strUserName = "";
	String strCreatedDate = "";
	String strQtyOrdered = "";
	String strUDINo = "";
	String strDonorNum = "";
	String strExpirydt = "";
	String strLotNum = "";
	String strAge = "";
	String strSex = "";
	String strInterUse = "";
	String strForUse = "";
	String strHideButton = GmCommonClass.parseNull((String)request.getParameter("HideButton"));
	String strCompanyLogo = GmCommonClass.parseNull((String)request.getParameter("CompanyLogo"));
	strCompanyLogo = strCompanyLogo.equals("")?"100800":strCompanyLogo;
	String strApplDateFmt = strGCompDateFmt;
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		alLotNums = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("LOTLIST"));
		strDHRId = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId = GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName = GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		//strManufDate = GmCommonClass.parseNull((String)hmDHRDetails.get("MDATE"));
		strManufDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("MDATE"),strApplDateFmt);
		strUserName = GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		//strCreatedDate = GmCommonClass.parseNull((String)hmDHRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplDateFmt);
		strQtyOrdered = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYORD"));
		strQtyRej = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ")); 
		strDonorNum = GmCommonClass.parseNull((String)hmDHRDetails.get("DONORNUMBER"));
		//strExpirydt = GmCommonClass.parseNull((String)hmDHRDetails.get("EXPDT")); 
		strExpirydt = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("EXPDT"),strApplDateFmt);
	}
	if (hmDonorDtls != null){
		strAge	= GmCommonClass.parseNull((String)hmDonorDtls.get("AGE"));
		strSex	= GmCommonClass.parseNull((String)hmDonorDtls.get("SEXCODENAME"));
		strInterUse	= GmCommonClass.parseNull((String)hmDonorDtls.get("INTERNATIONALUSENM"));
		strForUse	= GmCommonClass.parseNull((String)hmDonorDtls.get("FORRESEARCHNM"));
	}
	int intSize = 0;
	HashMap hcboVal = null; 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script> 
<style type="text/css">
td.LineBreak{
word-break: break-all;
}
</style>

</HEAD>

<BODY leftmargin="20" topmargin="20"  >
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="170">&nbsp;<img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="100">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+2><fmtDHRLotListing:message key="LBL_LOT_LISTING"/>&nbsp;</font></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="4"></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
 					<tr>
 						<td class="RightText" height="30" align="right" width="18%"><fmtDHRLotListing:message key="LBL_WORK_ORDER_ID"/>:</td>
						<td class="RightText" width="30%">&nbsp;<%=strWOId%></td>
 						<td  height="30" class="RightText" align="right" width="20%"><fmtDHRLotListing:message key="LBL_DHR_ID"/>:</td>
						<td class="RightText" width="50%">&nbsp;<%=strDHRId%></td>
					</tr> 
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRLotListing:message key="LBL_ORIGINATOR"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>
						<td class="RightText" align="right"><fmtDHRLotListing:message key="LBL_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strCreatedDate%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRLotListing:message key="LBL_PART"/>:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightText" align="right"><fmtDHRLotListing:message key="LBL_DESCRIPTION"/>:&nbsp;</td>
						<td class="RightText"><%=GmCommonClass.getStringWithTM(strDesc)%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRLotListing:message key="LBL_DONOR"/>:</td>
						<td class="RightText">&nbsp;<%=strDonorNum%></td>
						<td class="RightText" align="right"><fmtDHRLotListing:message key="LBL_VENDOR_IFAPPLICABLE"/>:</td>
						<td class="RightText">&nbsp;<%=strVendName%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRLotListing:message key="LBL_UDI"/>:&nbsp;</td>
						<td class="LineBreak" ><%=strUDINo%></td>
						<td class="RightText" align="right"><fmtDHRLotListing:message key="LBL_EXPIRATION_DATE"/>:&nbsp;</td>
						<td class="RightText"><%=strExpirydt%></td>
					</tr>					 
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRLotListing:message key="LBL_AGE"/>:&nbsp;</td>
						<td class="LineBreak" ><%=strAge%></td>
						<td class="RightText" align="right"><fmtDHRLotListing:message key="LBL_SEX"/>:&nbsp;</td>
						<td class="RightText"><%=strSex%></td>
					</tr>					 
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRLotListing:message key="LBL_INTERNATIONAL_USAGE"/>:&nbsp;</td>
						<td class="LineBreak" ><%=strInterUse%></td>
						<td class="RightText" align="right"><fmtDHRLotListing:message key="LBL_FOR_RESEARCH"/>:&nbsp;</td>
						<td class="RightText"><%=strForUse%></td>
					</tr>					 
					<tr><td bgcolor="#666666" colspan="4"></td></tr>
 
				</table>
			</td>
		</tr>
	</table>
	<table><tr><td style="height: 2px;"></td></tr></table>
	   
	<table cellpadding="0" cellspacing="0" border="0" width="700" height="90">
		<tr>
			<td bgcolor="#666666" rowspan="1" width="1"></td>
			<td bgcolor="#666666" colspan="6" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="1" width="1"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" rowspan="1" width="1"></td>
			<td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b><fmtDHRLotListing:message key="LBL_LOT_NUMBERS"/></b></td>
			<td bgcolor="#666666" rowspan="1" width="1"></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="7" height="1"></td></tr>  
			<%
			  		intSize = alLotNums.size();
					hcboVal = new HashMap();
					if ( intSize > 0)
					{
				  		for (int i=0;i<intSize;i++)
				  		{
				  			hcboVal = (HashMap)alLotNums.get(i);
				  			strLotNum = (String)hcboVal.get("CTRLNO"); 
			
			%>
							<tr>
								<td bgcolor="#666666" rowspan="1" width="1"></td>
								<td height="18" colspan="6" class="RightText">&nbsp;<%=strLotNum%></td>
								<td bgcolor="#666666" rowspan="1" width="1"></td> 
							</tr>
					 
			<%
						}
					}
					 
			%>
			<tr><td bgcolor="#666666" colspan="7" height="1"></td></tr> 	 
	</table>
	
	<table><tr><td style="height: 2px;"></td></tr></table> 
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
	<%if(!strHideButton.equals("Y"))
			{ %>
	<div id="button">
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30">
			<BR>
			<BR>
			<fmtDHRLotListing:message key="BTN_PRINT" var="varPrint"/>
			<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnPrint();" />&nbsp;&nbsp;
			<fmtDHRLotListing:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" buttonType="Load" onClick="window.close();" />&nbsp;
			</td>
		</tr>
	</table>
	</div>
	<%} %>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
