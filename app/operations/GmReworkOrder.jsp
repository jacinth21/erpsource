
<!-- \operations\GmReworkOrder.jsp -->

 <%
/**********************************************************************************
 * File		 		: GmReworkOrder.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtReworkOrder" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReworkOrder:setLocale value="<%=strLocale%>"/>
<fmtReworkOrder:setBundle basename="properties.labels.operations.GmReworkOrder"/>

<%

	String strWikiTitle = GmCommonClass.getWikiTitle("SALES_REWORK_PURCHASE_ORDER"); 
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	String strSearch = GmCommonClass.parseNull((String)request.getAttribute("hSearch"));
	String strPartNums = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strShade = "";
	
	String strProjId =	"";
	String strPartNum = "";
	String strPartDesc = "";

	String strQtyInStock = "";
	String strVendorId = "";
	String strCostPrice = "";
	String strCostPriceWithComma = "";
	String strValidDate = "";
	String strQtyQuoted = "";
	String strRevNum = "";
	
	String strTemp = "";
	ArrayList alProjList = new ArrayList();
	ArrayList alVendorList = new ArrayList();
	ArrayList alList = new ArrayList();
	int intSize = 0;

	if (strhAction.equals("Load") || strhAction.equals("PriceLoad"))
	{
		alProjList = (ArrayList)session.getAttribute("PROJECTS");
		alVendorList = (ArrayList)session.getAttribute("VENDORS");
		strProjId = (String)request.getAttribute("hProjId")== null?"":(String)request.getAttribute("hProjId");
		strVendorId = (String)request.getAttribute("hVendId")== null?"":(String)request.getAttribute("hVendId");
	}
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Rework Order </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Rework_PO.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script>
var vServletPath = '<%=strServletPath%>';

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmReworkServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<INPUT type="hidden" name="hPOString" value="">
<INPUT type="hidden" name="hWOString" value="">
<INPUT type="hidden" name="Cbo_PoType" value="3101">
<INPUT type="hidden" name="hSearch" value="<%=strSearch%>">


<table border="0" class="dttable900" cellspacing="0" cellpadding="0" onkeypress="">
	<tr>
		<td colspan="3" height="25" class="RightDashBoardHeader">&nbsp;<fmtReworkOrder:message key="LBL_SALES_REWORK_PURCHASE_ORDER"/></td>
		<td height="25" class="RightDashBoardHeader"><fmtReworkOrder:message key="IMG_HELP" var="varHelp"/><img align="right"
			id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
			title='${varHelp}' width='16' height='16'
			onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		</td>
			
	</tr>
	<tr>
		<td height="100" valign="top" colspan="5">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="4">
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<!-- Custom tag lib code modified for JBOSS migration changes -->
							<tr>
								<td class="RightTableCaption" HEIGHT="30"  align="right">&nbsp;<fmtReworkOrder:message key="LBL_PROJECT_NAME"/>:	</td>
								<td>&nbsp;<gmjsp:dropdown controlName="hProjId"  seletedValue="<%= strProjId %>" 	
											width="350" value="<%= alProjList%>" codeId = "ID" codeName = "IDNAME" defaultValue= "[Choose One]" tabIndex="1" />
								</td>
								<td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtReworkOrder:message key="LBL_PART_NUMBER"/>: </td>
								<td>&nbsp;<input type="text" width="100" size="20" value="<%=strPartNums%>" name="Txt_PartNum" class="InputArea" 
											onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="2" >
								
								&nbsp;<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="3">
													<option value="0" ><fmtReworkOrder:message key="LBL_CHOOSE_ONE"/>
													<option value="LIT" ><fmtReworkOrder:message key="LBL_LITERAL"/>
													<option value="LIKEPRE" ><fmtReworkOrder:message key="LBL_LIKE_PREFIX"/>
													<option value="LIKESUF" ><fmtReworkOrder:message key="LBL_LIKE_SUFFIX"/>
										</select>
								</td>
							</tr>
							<tr>
								<td colspan="4" class="ELine" height="1"></td>
							</tr>
							<tr>
								<td class="RightTableCaption" align="right"><fmtReworkOrder:message key="LBL_VENDORS"/>:</td><td>&nbsp;<gmjsp:dropdown controlName="hVendId"  seletedValue="<%=strVendorId%>" 	
									width="250" value="<%= alVendorList%>" codeId = "ID" codeName = "NAME" defaultValue= "[Choose One]" tabIndex="5" />	</td>
								<td></td>
								<td height="30" colspan="3">
								<fmtReworkOrder:message key="LBL_LOAD" var="varLoad"/>
								<gmjsp:button name="Btn_Load" value="${varLoad}" style="width: 5em;height: 22" gmClass="button" onClick="fnLoad();" tabindex="7" buttonType="Load" /></td>
							</tr>
							
						</table>
					</td>
				</tr>
				
				<tr>
						<td>
							<div style="overflow:auto; height:400px;">
							<table style="width:900px;" cellpadding="1"  cellspacing="0" border="0">
							<thead>
							</TR>
								<tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td colspan="7" class="Line" height="1"></td></tr>
								<TR class="ShadeRightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
									<TD class="RightText" width="100"><fmtReworkOrder:message key="LBL_PART_NUMBER"/></TD>
									<TD class="RightText" width="300"><fmtReworkOrder:message key="LBL_DESCRIPTION"/></TD>
									<TD class="RightText" align="center" width="50"><fmtReworkOrder:message key="LBL_REV"/></TD>
									<TD class="RightText" align="center" width="50">&nbsp;&nbsp;<fmtReworkOrder:message key="LBL_QTY_IN"/><BR> <fmtReworkOrder:message key="LBL_QUARANTINE"/></TD>
									<TD class="RightText" align="center" width="100">&nbsp;<fmtReworkOrder:message key="LBL_QTY_TO"/> <BR><fmtReworkOrder:message key="LBL_REWORK"/></TD>
									<TD class="RightText" align="center" width="70"><fmtReworkOrder:message key="LBL_RATE_EA"/> </TD>
									<TD class="RightText" align="center" width="70"><fmtReworkOrder:message key="LBL_CRITICAL"/> ?</TD>
								</TR>
								<tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td colspan="7" class="Line" height="1"></td></tr>
							</thead>
							<tbody>
<%
								if (strhAction.equals("PriceLoad") )
								{
									alList = (ArrayList)hmReturn.get("VENDORPRICELIST");
									if (alList != null)
									{
										intSize = alList.size();
									}
									if (intSize > 0)
									{
										HashMap hmLoop = new HashMap();
			
										for (int i = 0;i < intSize ;i++ )
										{
											hmLoop = (HashMap)alList.get(i);
			
											strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
											strPartDesc = GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String)hmLoop.get("PDESC")));
											strQtyInStock = GmCommonClass.parseZero((String)hmLoop.get("INQUARAN"));
											strShade	= (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
											strRevNum = GmCommonClass.parseNull((String)hmLoop.get("DRAWREV"));
%>
											<TR id="tr<%=i%>">
											<fmtReworkOrder:message key="IMG_ALT_HELP" var = "varViewPartInventoryDetails"/>
											<td class="RightText"><img id="imgEditP"  style='cursor:hand' onclick="javascript:fnPartDetails('<%=strPartNum%>','popup')" 
													title="${varViewPartInventoryDetails}" src="<%=strImagePath%>/product_icon.jpg" style=""/>&nbsp;<%=strPartNum%>
													<INPUT type="hidden" name="hPartNum<%=i%>" value="<%=strPartNum%>">
													<INPUT type="hidden" name="hRev<%=i%>" value="<%=strRevNum%>">
													</td>
												<td nowrap class="RightText">&nbsp;<%=strPartDesc%></td>
												<td nowrap class="RightText" align="center">&nbsp;<%=strRevNum%></td>
												<td class="RightText" align="right"><%=strQtyInStock%></td><input type="hidden" name="hQtyQuaran<%=i%>" value="<%=strQtyInStock%>">
												<td class="RightText"align="center"><input type="text" size="3" class="InputArea" maxlength=""  name="Txt_Qty<%=i%>" 
													onFocus="changeTRBgColor(<%=i%>,'#AACCE8');" onBlur="changeTRBgColor(<%=i%>,'#ffffff');"></td>
												<td class="RightText" align="center"><input type="text" size="5" class="InputArea"  name="Txt_Cost<%=i%>" maxlength=""
													onFocus="changeTRBgColor(<%=i%>,'#AACCE8');" onBlur="changeTRBgColor(<%=i%>,'#ffffff');"></td>
												<td align="center"><input type="Checkbox" name="Chk_Flag<%=i%>"></td>
											<TR>
											<tr><td colspan="7" height="1" bgcolor="#eeeeee"></td></tr>
<%
										}
%>						
							</tbody>
							</table>
								</div>
							</td>
							</TR>
								<td colspan="7" class="Line" height="1"></td>
						<tr>
							<td colspan="7" class="RightTextRed" align="center" height="25"> 
								<INPUT type="hidden" name="hTextboxCnt" value="<%=intSize%>">
								<fmtReworkOrder:message key="LBL_SUBMIT" var="varSubmit"/>
								<gmjsp:button value="${varSubmit}" controlId="Btn_Submit" style="width: 5em;height: 22" gmClass="button" onClick="fnSubmit();" tabindex="16" buttonType="Save" />&nbsp;	
							</td>
						</tr>
<%
						}else{
%>
							<tr><td colspan="7" class="RightTextRed" align="center" height="25"><fmtReworkOrder:message key="LBL_NO_DETAILS_AVAILABLE_FOR_THIS_PROJECT_OR_PART_NUMBER"/> .</td></tr>
<%
							}
						}
%>
			</TABLE>
		</TD>
	</tr>
</table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
