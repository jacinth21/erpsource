 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: GmVoidedDHRReport.jsp
 * Desc		 		: This screen is used for the View Voided DHR Report.
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
  <!-- \operations\GmVoidedDHRReport.jsp-->  
<%@ include file="/common/GmHeader.inc"%>
<bean:define id="xmlStringData" name="frmVoidedDHRReport" property="xmlStringData" type="java.lang.String"> </bean:define>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% 
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	String strWikiTitle = GmCommonClass.getWikiTitle("VOID_DHRREPORT");
	
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strApplDateFmt = strGCompDateFmt;
	String currentdate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
	String stroperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Voided DHR Report </TITLE>

<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel = "stylesheet" type = "text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=stroperationsJsPath%>/GmVoidedDHRReport.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script>

<script>
var objGridData;
objGridData = '<%=xmlStringData%>';
var dateFormat = '<%=strApplDateFmt%>';

</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload= "fnOnLoad();" onkeypress="fnEnter();">
<html:form  action="/GmVoidedDHRReport.do?">
<html:hidden property="strOpt" name="frmVoidedDHRReport" />
<input type="hidden" name="currentdate" value="<%=currentdate %>"/>
	<table border="0" width="100%" class="DtTable1100" cellspacing="0" cellpadding="0">
				<tr>
					<td height="25" class="RightDashBoardHeader" colspan="7">&nbsp;Voided DHR Report</td>
					<td  height="25" class="RightDashBoardHeader">
						<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr>
				 <tr><td class="LLine" colspan="9"></td></tr>
				<tr>
					<td class="RightTableCaption" HEIGHT="25" align="right" width="200">
					<gmjsp:label type="RegularText" SFLblControlName="DHR ID:" td="false" /> </td>
					<td style="display: inline-block;margin-top: 3px;">&nbsp;<html:text property="dhrID"  maxlength="20" size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" /></td>
				 	<td class="RightTableCaption" HEIGHT="25" align="right" width="200" style="white-space: nowrap;display: inline-block;margin-top: 16px;width: 17%;">
					<gmjsp:label type="RegularText" SFLblControlName="PO ID:" td="false" /> </td>
					<td style="padding-left: 5px;display: inline-block;margin-top: 4px;"><html:text property="poID"  maxlength="20" size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" /></td>
					<td class="RightTableCaption" HEIGHT="25" align="right" width="200" style="white-space: nowrap;">
					<gmjsp:label type="RegularText" SFLblControlName="Invoice ID:" td="false" /> </td>
					<td style="padding-left: 5px;"><html:text property="invID"  maxlength="20" size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" /></td>
					<td class="RightTableCaption" HEIGHT="25" align="right" width="200" style="white-space: nowrap;">
					<gmjsp:label type="RegularText" SFLblControlName="Part #:" td="false" /> </td>
					<td style="padding-left: 5px;">&nbsp;<html:text property="pnum"  maxlength="18" size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" />&nbsp;&nbsp;</td>
			   </tr>
			 	<tr><td class="LLine" colspan="9"></td></tr> 
			   	<tr class="shade">
			   		<td class="RightTableCaption" HEIGHT="25" align="right" width="200" style="white-space: nowrap;width:16%;">
					<gmjsp:label type="RegularText" SFLblControlName="Vendor Name:" td="false" /> </td>
					<td class="RightTableCaption" style="padding-left: 5px;"><gmjsp:dropdown controlName="vendorID" SFFormName="frmVoidedDHRReport" 
								SFSeletedValue="vendorID" defaultValue= "[Choose One]" SFValue="alVendorName" codeId="ID" codeName="NAME"  /></td>
				 	<td class="RightTableCaption" HEIGHT="25" align="right" width="200" style="white-space: nowrap;width:4%;padding-left:14px;">
					<gmjsp:label type="RegularText" SFLblControlName="From Date:" td="false" /> </td>
					<td  style="position: absolute;margin-top: 4px;">&nbsp;<gmjsp:calendar SFFormName="frmVoidedDHRReport" controlName="fromDate" gmClass="InputArea" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
					<td class="RightTableCaption" HEIGHT="25" align="right" width="200" style="white-space: nowrap;">
					<gmjsp:label type="RegularText" SFLblControlName="To Date:" td="false" /> </td>					
					<td style="width: 11%;position: absolute;margin-top: 3px;">&nbsp;<gmjsp:calendar SFFormName="frmVoidedDHRReport" controlName="toDate" gmClass="InputArea" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" /></td>
					
					<td class="RightTableCaption" align="center" height="25" colspan="2">
					<gmjsp:button name="Btn_Load" value="&nbsp;Load&nbsp;" style="height: 23px; width: 4em;" gmClass="button" onClick="fnLoad();" buttonType="Load" />
					</td>
			   </tr>
			   <%if(xmlStringData.indexOf("</row>")!=-1){ %>
			   
				<tr>
					<td colspan="8">
						<div  id="VOIDEDDHRDATA" style="" height="500px" width="1200px"></div>
					</td>
				</tr>
					<tr>
												<td colspan="8" align="center" height="25" width="100%">	
													<div class='exportlinks'>Export Options: <img src='img/ico_file_excel.png' /> <a href="#" onclick="javascript:fnExportExcel();"> Excel </a>	
												</td>
										</tr>							
			<%}else{%>
				<tr>
					<td colspan="9" height="25" class="LLine"></td></tr>
				<tr>
					<tr ><td colspan="9" align="center" class="RightText" >Nothing found to display</td></tr>
				</tr> 
			<%}%>

	</table>
	</html:form>
		<%@ include file="/common/GmFooter.inc"%>
	</BODY>
</HTML>
	