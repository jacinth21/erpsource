 <%
/**********************************************************************************
 * File		 		: GmDHRReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page errorPage="/common/GmError.jsp" %>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtDHRReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRReport.jsp -->

<fmtDHRReport:setLocale value="<%=strLocale%>"/>
<fmtDHRReport:setBundle basename="properties.labels.operations.GmDHRReport"/>


<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strGridXmlData = GmCommonClass.parseNull((String)request.getAttribute("XMLGRIDDATA"));
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	System.out.println("hmReturn"+hmReturn);
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strhMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strhOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	
	ArrayList alProject = new ArrayList();
	String strShade = "";

	String strDHRId = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");
	String strPartNums = (String)request.getAttribute("hPartNums")==null?"":(String)request.getAttribute("hPartNums");
	String strProjectId = GmCommonClass.parseNull((String) request.getAttribute("hProjId"));
	String strProjectNm = GmCommonClass.parseNull((String) request.getAttribute("hProjNm"));
	String strSessApplDateFmt= strGCompDateFmt;

	alProject = (ArrayList) request.getAttribute("PROJLIST");

	int intSize = 0;

	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmDHRReport.js"></script>
<%-- <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script> --%>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script  language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
   
</style>
<script>
var gStrServletPath = "<%=strServletPath%>";
var gStrOpt = "<%=strhOpt%>";
var gCmpDateFmt = "<%=strGCompDateFmt%>";
var companyInfoObj = '<%=strCompanyInfo%>';
</script>
<script>
var objGridData;
objGridData ='<%=strGridXmlData%>';
var gridObj;
function fnOnPageLoader() {
	if(document.frmVendor.Txt_PartNum != undefined && document.frmVendor.Txt_PartNum != null){
	   document.frmVendor.Txt_PartNum.focus();
	}
	if (objGridData.value != '') {
		gridObj = initGridData('DHRPartRpt', objGridData);
	}
}
function initGridData(divRef, gridData) {
	
	mygrid = setDefalutGridProperties(divRef);	
	mygrid.enableMultiline(false);	
	mygrid.enableDistributedParsing(true);
	mygrid = initializeDefaultGrid(mygrid,gridData);
	mygrid.groupBy(1);
	mygrid.setColumnHidden(1,true);
	mygrid.attachHeader(',,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	mygrid.enableTooltips("false,false,false,false,false,false,false,false,false,false,false,false,false");
	mygrid.enableBlockSelection(true);
    return mygrid;
}

//To download excel;
function fnExcel() { 
	mygrid.setColumnHidden(1,false);
	mygrid.setColumnHidden(0,true);
	mygrid.toExcel('/phpapp/excel/generate.php');
	mygrid.setColumnHidden(1,true);
	mygrid.setColumnHidden(0,false);
}
</script>
</HEAD>

<BODY leftmargin="15" topmargin="10" onload="fnOnPageLoader();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet?companyInfo=<%=strCompanyInfo %>">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hDHRId" value="">
<input type="hidden" name="hVenId" value="">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="5"  class="RightDashBoardHeader"><fmtDHRReport:message key="LBL_DHR_REPORT_PART_NUMBER"/></td>
		</tr>
		<tr>
		   	<td class="RightTableCaption" align = "right"  HEIGHT="30"><fmtDHRReport:message key="LBL_PROJECT_NAME"/> :&nbsp; </td>
			<td  colspan="4"><%-- <gmjsp:dropdown controlName="Cbo_Project"  seletedValue="<%=strProjectId%>" 	
							tabIndex="1"   value="<%=alProject%>" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"  /> --%>
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Cbo_Project" />
							<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="300" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value="1"/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%=strProjectNm %>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strProjectId %>" />
							<jsp:param name="SHOW_DATA" value="100" />
							<jsp:param name="AUTO_RELOAD" value="" />					
							</jsp:include> 
			</td>
		</tr>
        <tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
		<tr class="shade">
			<td class="RightTableCaption" align = "right"  HEIGHT="30"> <fmtDHRReport:message key="LBL_PART_NUMBER"/>:&nbsp; </td>
			<td colspan="4"><input type="text" size="50" value="<%=strPartNums%>" name="Txt_PartNum" class="InputArea" style="text-transform: uppercase;" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2>
				<span class="RightTextBlue"><fmtDHRReport:message key="LBL_PART_#_SEPARATED"/></span>
			</td>
		</tr>
		<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>		
		<tr>
			<td class="RightTableCaption" HEIGHT="30" align= "Right"><fmtDHRReport:message key="LBL_FROM_DATE"/>:&nbsp;</td>
		    <td class="RightTableCaption" HEIGHT="30"><gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(strFromDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFromDt,strSessApplDateFmt).getTime())))%>"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/></td>
			<td class="RightTableCaption" HEIGHT="30" align= "Right"><fmtDHRReport:message key="LBL_TO_DATE"/>:&nbsp;</td>
			<td class="RightTableCaption" HEIGHT="30"width="200" ><gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(strToDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDt,strSessApplDateFmt).getTime())))%>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/></td>
			<td width="300"><fmtDHRReport:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" name="Btn_Fetch" gmClass="button" onClick="fnPartNumberGo();" buttonType="Load" /></td>
		</tr>
		<tr><td colspan="5" class="Line" height="1"></td></tr>
		
		
		
<%if(!strGridXmlData.equals("")){%> 
<tr>
				<td colspan="5">
					<div id="DHRPartRpt" style="height:590px; width:100%;"></div>
				</td>			
		</tr>

     	<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
     		<tr>
			<td colspan="5" align="center">
			    <div class='exportlinks'><fmtDHRReport:message key="LBL_EXPORT_OPTS"/> : <img src='img/ico_file_excel.png' onclick="fnExcel();"/>&nbsp;<a href="#" onclick="fnExcel();"><fmtDHRReport:message key="LBL_EXCEL"/></a></div>
			</td>
		</tr>
		<%} else{%>
		 <tr>
		 <td height="30" colspan="5" align="center" class="RightTextBlue"><fmtDHRReport:message key="LBL_NO_DIS"/>.</td>
		<!-- <td colspan="5" align="center">No Data Available</td> -->	
		</tr>
		<%} %>

    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
