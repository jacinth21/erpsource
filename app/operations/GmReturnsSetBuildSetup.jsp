<!-- \operations\GmReturnsSetBuildSetup.jsp -->


 <%
/**********************************************************************************
 * File		 		: GmReturnsSetBuildSetup.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtReturnsSetBuildSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReturnsSetBuildSetup:setLocale value="<%=strLocale%>"/>
<fmtReturnsSetBuildSetup:setBundle basename="properties.labels.operations.GmReturnsSetBuildSetup"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");

	String strChecked = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";
	String strRAId = "";
	String strSetName= "";
	String strDistName = "";
	String strUserName = "";

	String strSessDate = (String)request.getAttribute("hRetDate");
	if (strSessDate == null)
	{
		strSessDate = (String)session.getAttribute("strSessTodaysDate");
	}

	strSessDate = strSessDate==null?"":strSessDate;
	
	
	String strItemQty = "";
	String strControl = "";
	String strOrgControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strRetDate = "";
	String strExpDate = "";
	String strType = "";
	String strReason = "";
	String strIniDate = "";
	String strOrderId = "";
	String strAccName = "";
	String strStatusFlag ="";
	String strItemType = "";
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		if (strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash"))
		{
			alSetLoad = (ArrayList)hmReturn.get("RAITEMDETAILS");
			if (strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash"))
			{
				hmConsignDetails = (HashMap)hmReturn.get("RADETAILS");

				strRAId = (String)hmConsignDetails.get("RAID");
				strSetName= (String)hmConsignDetails.get("SNAME");
				strDistName = (String)hmConsignDetails.get("DNAME");
				strDesc = (String)hmConsignDetails.get("COMMENTS");
				strAccName = GmCommonClass.parseNull((String)hmConsignDetails.get("ANAME"));
				strRetDate = GmCommonClass.parseNull((String)hmConsignDetails.get("RETDATE"));
				strRetDate = strRetDate.equals("")?strSessDate:strRetDate;
				strDistName = strDistName.equals("")?strAccName:strDistName;
				strStatusFlag = (String)hmConsignDetails.get("SFL");
				strType = (String)hmConsignDetails.get("TYPE");
				strReason = (String)hmConsignDetails.get("REASON");
				strIniDate = (String)hmConsignDetails.get("CDATE");
				strUserName = (String)hmConsignDetails.get("CUSER");
				strOrderId = (String)hmConsignDetails.get("ORDID");
				strExpDate = (String)hmConsignDetails.get("EDATE");
			}
		}
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Build </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnSubmit()
{
	hcnt = parseInt(document.frmVendor.hCnt.value);
	hcnt = hcnt+cnt;
	document.frmVendor.hAction.value = 'Save';
	document.frmVendor.hCnt.value = hcnt;
	fnStartProgress('Y');
  	document.frmVendor.submit();
}

function fnSetCheck(val)
{
	if (val.checked == true)
	{
		document.frmVendor.hStatusFl.value = "1";
	}
	else
	{
		document.frmVendor.hStatusFl.value = "";
	}
}

function fnCallSplit(val)
{
	var QtyObj = eval("document.all.Qty"+val);
	var CNumObj = eval("document.all.CNum"+val);
	
	var NewQtyHtml = "<input type='text' size='3' value='' name='Txt_Qty' class='InputArea' onFocus='changeBgColor(this,'#AACCE8');' onBlur='changeBgColor(this,'#ffffff');' tabindex=1>";

	var NewCNumHtml = "<input type='text' size='3' value='' name='Txt_CNum' class='InputArea' onFocus='changeBgColor(this,'#AACCE8');' onBlur='changeBgColor(this,'#ffffff');' tabindex=1>";

	//alert(val);
	var ob = document.all.myTable.rows;
	//alert(ob);
	for (var i=0;i<ob.length;i++)
	{
		rownum = ob[i].id;
		//alert(rownum);
		if (rownum == val)
		{
			num = ob[i].rowIndex;
			//alert("ROWINDEX IS:"+num);
	//document.all.myTable.deleteRow(val);
			break;
		}
	}
	document.all.myTable.deleteRow(num);

}
var cnt = 0;
function fnSplit(val,pnum,porgCont)
{
	cnt++;
	var hcnt = parseInt(document.frmVendor.hCnt.value);
	hcnt = hcnt+cnt;

	var itemobj = eval("document.frmVendor.hItemType"+val);
	var itype = itemobj.value;

	var NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hPartNum"+hcnt +"' value='"+pnum + 
					 "'><input type='hidden' name='hItemType"+hcnt+"' value='"+itype+"'><input type='hidden' name='hOrgCNum" + hcnt + "' value='" + porgCont + 
					 "'><input type='text' size='3' value='' name='Txt_Qty"+hcnt+"' class='InputArea' onFocus=changeBgColor(this," + 
					 "'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + 
					 "<input type='text' size='9' value='' name='Txt_CNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this," + 
					 "'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1>&nbsp;<a href=javascript:fnDelete("+cnt + 
					 ");><img src=<%=strImagePath%>/btn_remove.gif border=0></a></span>";
	var QtyObj = eval("document.all.Cell"+val);

	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}

function fnDelete(val)
{
	obj = eval("document.all.Sp"+val);
	obj.innerHTML = "";
}

function fnVoidReturn()
{
		document.frmVendor.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmVendor.hAction.value = "Load";
		document.frmVendor.submit();
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmReturnSetBuildServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hRAId" value="<%=strRAId%>">
<input type="hidden" name="hMode" value="SET">
<input type="hidden" name="hStatusFl" value="">
<input type="hidden" name="hVerifyFl" value="">
<input type="hidden" name="hTxnId" value="<%=strRAId%>">
<input type="hidden" name="hCancelType" value="VDRTN">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader">
<fmtReturnsSetBuildSetup:message key="LBL_RECEIVE_RETURNS"/> : <%=strType%></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtReturnsSetBuildSetup:message key="LBL_RA_ID"/>:</td>
						<td class="RightText" width="100">&nbsp;<%=strRAId%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtReturnsSetBuildSetup:message key="LBL_TYPE"/>:</td>
						<td class="RightText" >&nbsp;<%=strType%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtReturnsSetBuildSetup:message key="LBL_INITIATED_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strIniDate%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtReturnsSetBuildSetup:message key="LBL_REASON"/>:</td>
						<td class="RightText">&nbsp;<%=strReason%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtReturnsSetBuildSetup:message key="LBL_INITIATED_BY"/>Initiated By:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtReturnsSetBuildSetup:message key="LBL_SET_NAME"/>:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtReturnsSetBuildSetup:message key="LBL_ACCOUNT/DISTRIBUTOR_NAME"/>:</td>
						<td class="RightText">&nbsp;<%=strDistName%></td>
						<td class="RightTableCaption" align="right">&nbsp;<fmtReturnsSetBuildSetup:message key="LBL_ORDER_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strOrderId%></td>						
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="40"><fmtReturnsSetBuildSetup:message key="LBL_COMMENTS"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strDesc%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtReturnsSetBuildSetup:message key="LBL_DATE_EXPECTED"/>:</td>
						<td class="RightText">&nbsp;<%=strExpDate%></td>
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtReturnsSetBuildSetup:message key="LBL_DATE_RETURNED"/>:</td>
						<td class="RightText">&nbsp;<input type="text" size="10" value="<%=strRetDate%>" name="Txt_RetDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmVendor.Txt_RetDate');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 /></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtReturnsSetBuildSetup:message key="LBL_COMPLETE"/> ?:</td>
						<td colspan="3"><input type="checkbox" name="Chk_Flag" onFocus="changeBgColor(this,'#AACCE8');" <%=strChecked%> onBlur="changeBgColor(this,'#ffffff');" tabindex=2></td>
					</tr>
					<tr><td class="Line" HEIGHT="1" colspan="4"></td></tr>
					
<%					if(!strStatusFlag.equals("0"))					
					{
										
%>
					<tr>
						<td colspan="4" height="30" align="center" class ="RightTextRed">
							<fmtReturnsSetBuildSetup:message key="LBL_RETURN_HAS_ALREADY_CREDITED"/>
						</td>
					</tr>
<%					}
					else
					{
%>				
					<tr>
						<td class="RightTextBlue" HEIGHT="24" colspan="4"><b><fmtReturnsSetBuildSetup:message key="LBL_INSTRUCTIONS"/>:</b><br>
1. If some parts are missing in the returned set, but are part of the set list, then <b>DO NOT</b> account for the missing parts. Only fill in what has been returned.<br>
2. If for some reason, the Control Number cannot be identified or faded beyond identification (for ex, Assure screws etc), then please fill in the appropriate Qty and enter �NOC#� as the value for the Control Number.<BR><br>
If you have any questions, please contact System Admin
						
						</td>
					</tr>
<%					}
%>

<%					
					if ( strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash"))
					{
%>
					<tr>
						<td colspan="4">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25"><fmtReturnsSetBuildSetup:message key="LBL_PART_NUMBER"/></td>
									<td width="350"><fmtReturnsSetBuildSetup:message key="LBL_DESCRIPTION"/></td>
									<td><fmtReturnsSetBuildSetup:message key="LBL_SET_QTY"/></td>
									<td><fmtReturnsSetBuildSetup:message key="LBL_ORG_CONTROL_NUMBER"/></td>
									<td width="100">&nbsp;&nbsp;&nbsp;&nbsp;<fmtReturnsSetBuildSetup:message key="LBL_QTY"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtReturnsSetBuildSetup:message key="LBL_CONTROL_NUMBER"/></td>
								</tr>
								<tr><td class=Line colspan=5 height=1></td></tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							int intSetQty = 0;
							boolean bolQtyFl = false;
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strOrgControl = GmCommonClass.parseNull((String)hmLoop.get("ORG_CNUM"));
								intSetQty = Integer.parseInt(strQty);
								strItemType = GmCommonClass.parseNull((String)hmLoop.get("ITYPE"));
								//bolQtyFl = intSetQty > 1?true:false;

								strTemp = strPartNum;

								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=4 height=1 bgcolor=#eeeeee></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=4 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										strQty = "";
										//strColor = "bgcolor=white";
										strLine = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									strQty = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
								strItemQty = strItemQty == ""?strQty:strItemQty;
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								if (intSetQty > 1 || intSetQty == 0)
								{
									bolQtyFl = true;
								}
								else
								{
									bolQtyFl = false;
									strItemQty = "1";
								}
								out.print(strLine);
%>
								<tr >
<%
								if ((strhAction.equals("EditLoad")  && bolQtyFl) || (strhAction.equals("EditLoadDash") && bolQtyFl))
								{
%>
									<td class="RightText" height="20">&nbsp;<a class="RightText" tabindex=-1 
									href="javascript:fnSplit('<%=i%>','<%=strPartNum%>','<%=strOrgControl%>');"><%=strPartNum%></a>
									<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>"></td>
<%
								}else{
%>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%>
									<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>"></td>
<%
									}
%>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td align="center" class="RightText"><%=strQty%>
									<input type="hidden" name="hPartNumTotalQty<%=i%>" value="<%=strQty%>"></td>
									<td align="center" class="RightText"><%=strOrgControl%>
									<input type="hidden" name="hOrgCNum<%=i%>" value="<%=strOrgControl%>">
									<input type="hidden" name="hItemType<%=i%>" value="<%=strItemType%>">
									</td>
<%
								if (strhAction.equals("EditLoad")|| strhAction.equals("EditLoadDash"))
								{
%>
									<td id="Cell<%=i%>" class="RightText">&nbsp;&nbsp;&nbsp;<input type="text" size="3" value="<%=strItemQty%>" name="Txt_Qty<%=i%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="text" size="9" value="<%=strControl%>" name="Txt_CNum<%=i%>" class="InputArea" maxlength="8"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
									</td>
<%
								}else{
%>
									<td>&nbsp;</td>
<%
									}
%>
								</tr>
<%
								//out.print(strLine);
							}//End of FOR Loop
						}else {
%>
						<tr><td colspan="5" height="50" align="center" class="RightTextRed"><BR><fmtReturnsSetBuildSetup:message key="LBL_NO_PART_HAVE_BEEN_MAPPED_FOR_THIS_SET"/> !</td></tr>
<%
						}		
%>
							</table>
						</td>								<input type="hidden" name="hCnt" value="<%=intSize-1%>">
					</tr>
<%
					}
%>					
					

					
<%					
						if (!strStatusFlag.equals("0"))
						{
%>
						<tr>
							<td colspan="4" height="30" align="center">
							<fmtReturnsSetBuildSetup:message key="LBL_SUBMIT" var="varSubmit"/>
								<gmjsp:button value="${varSubmit}" tabindex="13" disabled="true" buttonType="Save" />
							</td>
						</tr>										
						
<%						}
						else if (strhAction.equals("LoadSet"))
						{
%>
						<tr>
							<td colspan="4" height="30" align="center">
							<fmtReturnsSetBuildSetup:message key="LBL_INITIATE" var="varInitiate"/>
							<gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="13" buttonType="Save" />
							</td>
						</tr>
<%
						}else if (strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash")){
%>
						<tr>
							<td colspan="4" height="30" align="center">
							<fmtReturnsSetBuildSetup:message key="LBL_VOID" var="varVoid"/>
    							<gmjsp:button value="${varVoid}" gmClass="button"  onClick="fnVoidReturn();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
    							<fmtReturnsSetBuildSetup:message key="LBL_SUBMIT" var="varSubmit"/>
								<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="14" buttonType="Save" />
							</td>
						</tr>
<%
						}

%>				 
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
