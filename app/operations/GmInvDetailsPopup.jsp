 <%
/**********************************************************************************
 * File		 		: GmPartNumInvReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ include file="/common/GmHeader.inc"%>
<!-- operations\GmInvDetailsPopup.jsp -->
<%@ taglib prefix="fmtInvDtlsPopup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtInvDtlsPopup:setLocale value="<%=strLocale%>"/>
<fmtInvDtlsPopup:setBundle basename="properties.labels.operations.GmInvDetailsPopup"/>
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmInvDetailsPopup", strSessCompanyLocale);
	
	String strApplDateFmt = strGCompDateFmt;
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strOpt = (String)request.getAttribute("hOpt");
	HashMap hmLoop = new HashMap();

	String strPartNum = "";
	String strPartDesc = "";
	String strQtyInPO = "";
	String strQtyPend = "";
	String strVendor = "";
	String strWOID = "";
	String strPO = "";
	String strDate = "";
	java.util.Date crDate = null;
	String strWIP = "";
	String strTypeLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_TYPE"));
	String strReasonLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_REASON"));
	double dbQtyPend = 0;
	double dbQtyInWIP = 0;
	double dbQtyPendTotal = 0;
	double dbQtyInWIPTotal = 0;
	double dbQtyInPO =0;	
	
	String strShade = "";

	int intLoop = 0;

	ArrayList alReport = new ArrayList();

	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("REPORT");
	}

	if (alReport != null)
	{
		intLoop = alReport.size();
	}

	if(strOpt.equals("WIPSET")||strOpt.equals("TBESET") || strOpt.equals("BUILTSET")){
		strTypeLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_SET_ID"));
		strReasonLabel = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_SET_NM"));
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Inventory Details</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
var prevtr = 0;

function changeTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPartInvReportServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hAction" value="">

	<table border="0" width="650" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="20" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="20" width="1" class="Line"></td>
		</tr>
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">
			<fmtInvDtlsPopup:message key="LBL_PART_INV_DTLS_RPT"/>
			</td>
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>
<%
	if (strOpt.equals("PO")){
%>		
		<tr>
			<td colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" align="center" width="300"><fmtInvDtlsPopup:message key="LBL_VENDOR"/></td>
						<td width="140" align="center">&nbsp;<fmtInvDtlsPopup:message key="LBL_PO_NUM"/></td>
						<td width="140" align="center">&nbsp;<fmtInvDtlsPopup:message key="LBL_WO_ID"/></td>
						<td width="80" align="center"><fmtInvDtlsPopup:message key="LBL_QTY_ORDED"/></td>
						<td width="80" align="center"><fmtInvDtlsPopup:message key="LBL_QTY_PEND"/></td>
						<td width="80" align="center"><fmtInvDtlsPopup:message key="LBL_QTY_DHR"/></td>
						<td width="100" align="center"><fmtInvDtlsPopup:message key="LBL_DT_RAISED"/></td>
					</tr>
					<tr><td class="Line" colspan="7"></td></tr>
<%
			if (intLoop > 0)
			{
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);					
					strVendor = GmCommonClass.parseNull((String)hmLoop.get("VNAME"));
					strPO = GmCommonClass.parseNull((String)hmLoop.get("PO"));
					strWOID = (String)hmLoop.get("WO");
					crDate  = (java.sql.Date)hmLoop.get("CDATE");
					strDate = GmCommonClass.getStringFromDate(crDate,strApplDateFmt);
					strQtyInPO = GmCommonClass.parseZero((String)hmLoop.get("ORDQTY"));
					dbQtyInPO = Double.parseDouble(strQtyInPO);
					strQtyPend = GmCommonClass.parseZero((String)hmLoop.get("PENDQTY"));
					dbQtyPend = Double.parseDouble(strQtyPend);
					dbQtyPendTotal = dbQtyPendTotal + dbQtyPend; 
					
					strWIP = GmCommonClass.parseZero((String)hmLoop.get("WIP"));
					dbQtyInWIP = Double.parseDouble(strWIP);
					dbQtyInWIPTotal = dbQtyInWIPTotal + dbQtyInWIP;
%>
					<tr id="tr<%=i%>" onClick="changeTRBgColor(<%=i%>,'#AACCE8');">
						<td height="20" class="RightText">&nbsp;<%=strVendor%></td>
						<td class="RightText">&nbsp;<%=strPO%></td>
						<td class="RightText" align="center"><%=strWOID%></td>
						<td class="RightText" align="center"><%=dbQtyInPO%></td>
						<td class="RightText" align="center"><%=dbQtyPend%></td>
						<td class="RightText" align="center"><%=dbQtyInWIP%></td>
						<td class="RightText" align="center"><%=strDate%></td>
					</tr>
					<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
<%
				}
%>
					<tr class="RightTableCaption" bgcolor="#EEEEEE" height="20">
						<td colspan="4"><fmtInvDtlsPopup:message key="LBL_TOTALS"/></td>
						<td align="center"><%=""+dbQtyPendTotal%></td>
						<td align="center"><%=""+dbQtyInWIPTotal%></td>
						<td colspan="2"></td>
<%
			} else	{
%>
					<tr>
						<td height="30" colspan="7" align="center" class="RightTextBlue"><fmtInvDtlsPopup:message key="MSG_NO_DATA"/></td>
					</tr>
<%
		}
%>
					<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
<%
	}else{
%>
		<tr>
			<td colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="24" align="center" nowrap width="85"><fmtInvDtlsPopup:message key="LBL_REF_ID"/></td>
						<td width="150">&nbsp;<fmtInvDtlsPopup:message key="LBL_REF_NM"/></td>
						<td width="100" align="center"><%=strTypeLabel%></td>
						<td width="100" align="center"><%=strReasonLabel%></td>
						<td width="80" align="center"><fmtInvDtlsPopup:message key="LBL_CREATE_BY"/></td>
						<td width="80" align="center"><fmtInvDtlsPopup:message key="LBL_CREATE_DT"/></td>
						<td width="40" align="center"><fmtInvDtlsPopup:message key="LBL_QTY"/></td>
					</tr>
					<tr><td class="Line" colspan="7"></td></tr>
<%
			if (intLoop > 0)
			{
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);
					strVendor = GmCommonClass.parseNull((String)hmLoop.get("REFID"));
					strPO = GmCommonClass.parseNull((String)hmLoop.get("REFNAME"));
					crDate  = (java.sql.Date)hmLoop.get("CDATE");
					strDate = GmCommonClass.getStringFromDate(crDate,strApplDateFmt);
					strWOID = GmCommonClass.parseNull((String)hmLoop.get("REFTYPE"));
					strQtyInPO = GmCommonClass.parseNull((String)hmLoop.get("REFREASON"));
					strQtyPend = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
					strWIP = GmCommonClass.parseZero((String)hmLoop.get("CUSER"));
%>
					<tr id="tr<%=i%>" onClick="changeTRBgColor(<%=i%>,'#AACCE8');">
						<td height="20" class="RightText">&nbsp;<%=strVendor%></td>
						<td class="RightText">&nbsp;<%=strPO%></td>
						<td class="RightText" align="center"><%=strWOID%></td>
						<td class="RightText" align="center"><%=strQtyInPO%></td>
						<td class="RightText" align="center"><%=strWIP%></td>
						<td class="RightText" align="center"><%=strDate%></td>
						<td class="RightText" align="center"><%=strQtyPend%></td>
					</tr>
					<tr><td colspan="7" bgcolor="#eeeeee" height="1"></td></tr>
<%
				}
			}else{
%>
					<tr>
						<td height="30" colspan="7" align="center" class="RightTextBlue"><fmtInvDtlsPopup:message key="MSG_NO_DATA"/></td>
					</tr>
<%
			}
%>
					<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
<%
	}
%>				
	</table>
			<fmtInvDtlsPopup:message key="BTN_CLOSE" var="varClose"/>
			<br>
				<center><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" /></center>
</FORM>

</BODY>

</HTML>
