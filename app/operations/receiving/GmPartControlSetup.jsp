<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%
/********************************************************************************
* File                : GmPartControlSetup.jsp
* Desc                : Used to Save the Control Number for the respective Part
* Version             : 1.0
* Author          	  : Karthik 
*********************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtPartControlSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartControlSetup:setLocale value="<%=strLocale%>"/>
<fmtPartControlSetup:setBundle basename="properties.labels.operations.receiving.GmPartControlSetup"/>
<!-- operations\receiving\GmPartControlSetup.jsp -->
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<bean:define id="partCNId" name="frmPartControlSetup" property="partCNId" type="java.lang.String"></bean:define>
<bean:define id="partNM" name="frmPartControlSetup" property="partNM" type="java.lang.String"></bean:define>
<bean:define id="prodClass" name="frmPartControlSetup" property="prodClass" type="java.lang.String"></bean:define>
<bean:define id="prodType" name="frmPartControlSetup" property="prodType" type="java.lang.String"></bean:define>
<bean:define id="alDonors" name="frmPartControlSetup" property="alDonors" type="java.util.ArrayList"></bean:define>
<bean:define id="alVendors" name="frmPartControlSetup" property="alVendors" type="java.util.ArrayList"></bean:define>
<bean:define id="julianRule" name="frmPartControlSetup" property="julianRule" type="java.lang.String"></bean:define>
<bean:define id="vendorId" name="frmPartControlSetup" property="vendorId" type="java.lang.String"></bean:define>
<bean:define id="cnumfmt" name="frmPartControlSetup"  property="cnumfmt" type="java.lang.String"></bean:define>
<bean:define id="donorNM" name="frmPartControlSetup"  property="donorNM" type="java.lang.String"></bean:define>
<bean:define id="donorID" name="frmPartControlSetup"  property="donorID" type="java.lang.String"></bean:define>
<bean:define id="udi"   name="frmPartControlSetup"  property="udi" type="java.lang.String"></bean:define> 
<bean:define id="hScreenType" name="frmPartControlSetup"  property="hScreenType" type="java.lang.String"></bean:define>
<bean:define id="hAccess" name="frmPartControlSetup"  property="hAccess" type="java.lang.String"></bean:define>
<bean:define id="show_Txtbox" name="frmPartControlSetup"  property="show_Txtbox" type="java.lang.String"></bean:define>
<bean:define id="controlNM" name="frmPartControlSetup"  property="controlNM" type="java.lang.String"></bean:define>
<bean:define id="disableText" name="frmPartControlSetup"  property="disableText" type="java.lang.String"></bean:define>
<bean:define id="successMsg" name="frmPartControlSetup"  property="successMsg" type="java.lang.String"></bean:define> 
<bean:define id="revLevel" name="frmPartControlSetup"  property="revLevel" type="java.lang.String"></bean:define>

<% 
String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
String strWikiTitle = GmCommonClass.getWikiTitle("PART_CONTROLNUM_SETUP");	
HashMap hcboVal = null;
Boolean blDisableDropdown = false;
Boolean blDisableButton = false;
Boolean blUDIText=false;
Boolean blRevNumber = false;
String strApplDateFmt = strGCompDateFmt;
GmCalenderOperations.setTimeZone(strGCompTimeZone);
String strTodayDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);
if(!prodType.equals("100845")){
     blDisableDropdown = true;
}
//The following code added for passing the company info to the child js fnFilterLoad function
String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<script> 
var companyInfoObj = '<%=strCompanyInfo%>';
var format = '<%=strApplDateFmt%>';
var DonorLen = <%=alDonors.size()%>;
var julianRule = '<%=julianRule%>';
var VendorLen = <%=alVendors.size()%>;
var currDate = '<%=strTodayDate%>';
var partCnId = '<%=partCNId%>';
var prodType = '<%=prodType%>';
var prodClass = '<%=prodClass%>';
var vendorId = '<%=vendorId%>';
var donorId = '<%=donorID%>';
var donorNM = '<%=donorNM%>';
var controlFmt = '<%=cnumfmt%>';
var savedUdi = '<%=udi%>';
var savedControlnum = '<%=controlNM%>';
var disableText = '<%=disableText%>';
var successMsg = '<%=successMsg%>';
<%hcboVal = new HashMap();
for (int i=0;i<alDonors.size();i++){
     hcboVal = (HashMap)alDonors.get(i);
%>
var alDonorsArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>,<%=hcboVal.get("VENDORID")%>";
<%}%>
<%
hcboVal = new HashMap();
for (int i=0;i<alVendors.size();i++){
     hcboVal = (HashMap)alVendors.get(i);
%>
var alVendorsArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("LOTCODE")%>";
<%}%>
var hScreenType='<%=hScreenType%>';
var udi='<%=udi%>';
var revLevel='<%=revLevel%>';

$(document).ready(function(){
	var expdt_txtbox = $('#expDate');
	var manfdt_txtbox = $('#manfDate');
	var customsize_txtbox = $('#customSize');
	var btn = $('#Btn_Close');
	// default value of textbox when page loads
	var expdt_defaultValue = expdt_txtbox.val();
	var manfdt_defaultValue = manfdt_txtbox.val();
	var customsize_defaultValue = customsize_txtbox.val();
	var confirmSubmit = true;
	btn.click(function() {
		if ( (expdt_defaultValue != expdt_txtbox.val()) || 
				(manfdt_defaultValue !=manfdt_txtbox.val() ) || 
				  (customsize_defaultValue !=customsize_txtbox.val())) {
			confirmSubmit = confirm(message[5282]);
			if(!confirmSubmit){
				fnStopProgress();
				return false;
			}
			fnclose();
		}
		else {
			fnclose();
		}
	});
});

function fnclose() {
	window.close();
	fnStopProgress();
	if (hScreenType =='popup' && successMsg !=''){
	fnRefreshParent();
	}
}

function fnRefreshParent(){
	if(window.opener != null && !window.opener.closed){
		window.opener.location.reload();
		}
}
</script>

<%
String strSubmitAccess = GmCommonClass.parseNull((String)request.getAttribute("SUBMITCNTRLFL"));
String strBtnDisable = "true";
GmResourceBundleBean gmResourceBundleBeanLbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.receiving.GmPartControlSetup", strSessCompanyLocale);
String strUdiLogEntry = GmCommonClass.parseNull((String)gmResourceBundleBeanLbl.getProperty("LBL_UDI_LOG"));
if(hAccess.equals("N")){
	 strBtnDisable = "true";
}

if(strSubmitAccess.equals("Y") &&  hAccess.equals("Y")){
	strBtnDisable = "false";
}
if(hScreenType.equals("leftLink") && !show_Txtbox.equals("N")) {
blUDIText=false;
}else{ 
blUDIText=true;
}
%>
<script>
var submitBtn = '<%=strSubmitAccess%>';
var udi='<%=udi%>';
var hAccess='<%=hAccess%>';
</script>
<HTML>
<HEAD>
<TITLE> Globus Medical: CPC Setup </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmPartControlSetup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnLoad();">
<html:form action="/gmPartControlSetup.do?" >
<html:hidden property="strOpt" value=""/>
<html:hidden property="partCNId"    name="frmPartControlSetup" />
<html:hidden property="prodClass"   name="frmPartControlSetup" />
<html:hidden property="prodType"    name="frmPartControlSetup" />
<html:hidden property="cnumfmt"     name="frmPartControlSetup" />
<html:hidden property="julianRule"  name="frmPartControlSetup" />
<html:hidden property="vendorId"    name="frmPartControlSetup" />
<html:hidden property="hScreenType" name="frmPartControlSetup" />
<html:hidden property="hAccess"     name="frmPartControlSetup" /> 
<html:hidden property="udiFormat"   name="frmPartControlSetup" /> 
<html:hidden property="diNum"       name="frmPartControlSetup" /> 
<html:hidden property="udiNo"       name="frmPartControlSetup" /> 
<html:hidden property="show_Txtbox" name="frmPartControlSetup" /> 
<html:hidden property="partNum" name="frmPartControlSetup" />  
<html:hidden property="successMsg" name="frmPartControlSetup" /> 
<html:hidden property="disableText" name="frmPartControlSetup" /> 
<html:hidden property="udilog" name="frmPartControlSetup" value="<%=strUdiLogEntry%>"/> 

<input type="hidden" name="hPartType">

<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
     <tr>
          <td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartControlSetup:message key="LBL_NO_OVER"/></td>
          <td  height="25" class="RightDashBoardHeader" align="right">
          <fmtPartControlSetup:message key="IMG_ALT_HELP" var = "varHelp"/>
              <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
          </td>
     </tr>
     <tr><td colspan="4" class="LLine" height="1"></td></tr>
     <logic:notEmpty property="strMsg" name="frmPartControlSetup">
     <tr>
     <td colspan="4" align ="center" height="25"><font color="green" size = "2"><b><bean:write name="frmPartControlSetup" property ="strMsg"/></b></font></td>
     </tr><tr><td class="LLine" height="1" colspan="4"></td></tr>
     </logic:notEmpty>  
     <tr>
       <td class="RightTableCaption" align="right" HEIGHT="24" width = "20%">&nbsp;<font color="red">*</font><fmtPartControlSetup:message key="LBL_PART_NO"/>:</td> 
            <%if(hScreenType.equals("leftLink")) {%>
        <td>&nbsp;<html:text property="partNM" size="16" maxlength="40" style="text-transform: uppercase;" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff'),fnPartNumber();"/>  
              <span id="DivShowPartNoIDExists" style="vertical-align:middle; display: none;"> <fmtPartControlSetup:message key="LBL_PART_NUMBER_ALREADY_EXIST" var="varPartNumberExist"/><img height="23" width="25" title="${varPartNumberExist}" src="<%=strImagePath%>/success.gif"></img></span>
              <span id="DivShowPartNoIDNotExists" style=" vertical-align:middle; display: none;"><fmtPartControlSetup:message key="LBL_PART_NUMBER_DOES_NOT_EXIST" var="varPartDoesNotExist"/><img title="${varPartDoesNotExist}" src="<%=strImagePath%>/delete.gif"></img></span></td> 
            <%}else{ %>
        <td>&nbsp;<bean:write name="frmPartControlSetup" property ="partNM"/><html:hidden property="partNM" name="frmPartControlSetup" /></td>
            <%} %>
     </tr>   
     <tr><td class="LLine" height="1" colspan="4"></td></tr>
     <tr class="Shade">
       <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<font color="red">*</font><fmtPartControlSetup:message key="LBL_LOT_NO"/>:</td> 
        <td>&nbsp;<html:text property="controlNM" styleId="controlNM" size="30" maxlength="40"   onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff'),fnGetRevNumber(),fnEnablefunction(),fnAddUDIDetails();"/>
       </td>  
     </tr>
          <tr><td class="LLine" height="1" colspan="4"></td></tr>
     <tr>
        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtPartControlSetup:message key="LBL_REV"/>:</td>  
        <td>&nbsp;<html:text property="revLevel"  styleId="revLevel" size="4" disabled="<%=blRevNumber%>" maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff')"/>
       </td>  
     </tr>
     <tr><td class="LLine" height="1" colspan="4"></td></tr>
            <tr class="Shade">
        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtPartControlSetup:message key="LBL_UDI#"/>:</td>  
         
        <td>&nbsp;<html:text property="udi" disabled="<%=blUDIText%>" size="60" maxlength="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff')"/></td>
        </tr>
     <tr><td class="LLine" height="1" colspan="4"></td></tr>
     <tr >
        <%if(prodType.equals("100845")) {%>
        <fmtPartControlSetup:message key="LBL_DONOR#" var="varDonor"/>
               <td  class="RightTableCaption"  align="right" HEIGHT="25"><gmjsp:label type="MandatoryText" SFLblControlName="${varDonor}:" td="false" /></td>
        <%}else{ %>
              <td class="RightTableCaption"  align="right" HEIGHT="25"><fmtPartControlSetup:message key="LBL_DONOR#"/>:</td>
        <%} %>
        <td colspan="3">
              <table  width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr><td width="32%">
              &nbsp;<html:text property="donorNM" styleId="donorNM" disabled="<%=blDisableDropdown%>" styleClass="InputArea" name="frmPartControlSetup" onfocus="changeBgColor(this,'#AACCE8');fnClearDiv();" onblur="changeBgColor(this,'#ffffff');fnCheckDonorNum(this);" size="15"/>
              <span id="DivShowDonorIDExists" style="vertical-align:middle; display: none;"> <img height="23" width="25" title="Donor # already exist" src="<%=strImagePath%>/success.gif"></img></span>
              <span id="DivShowDonorIDNotExists" style=" vertical-align:middle; display: none;"><img title="Donor # does not exists" src="<%=strImagePath%>/delete.gif"></img></span></td>
              <%//if(prodType.equals("100845")) {%>
              <td><span id='DonorIcon' style=" vertical-align:middle; display: <%=prodType.equals("100845")?"block":"none"%>;"><img align="middle" id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/d.gif' title='Donor' onclick="javascript:fnCallDonor()" width='14' height='14'/></span></td>
              <%//} %>
              </tr></table></td></tr>
     <tr><td class="LLine" height="1" colspan="4"></td></tr>
     <tr class="Shade">
          <td class="RightTableCaption" align="right" HEIGHT="24" >&nbsp;<fmtPartControlSetup:message key="LBL_EXP_DATE"/>:</td>
          <%if(prodClass.equals("4030") || prodType.equals("100845")){ %>
           <td width="40%"><span id="expDateCal">&nbsp;<gmjsp:calendar SFFormName="frmPartControlSetup" controlName="expDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff'); " /></span></td>          
          <%}else{ %>
          <td width="40%"><span id="expDateCal">&nbsp;<gmjsp:calendar SFFormName="frmPartControlSetup" controlName="expDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff'); " /></span></td>
          <%} %>        
     </tr>
     <tr><td class="LLine" height="1" colspan="4"></td></tr>
     <tr >
          <td class="RightTableCaption" align="right" HEIGHT="24" >&nbsp;<fmtPartControlSetup:message key="LBL_MFG_DATE"/> :</td>
          <td width="40%">&nbsp;<gmjsp:calendar SFFormName="frmPartControlSetup" 
           controlName="manfDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff'); " />
          </td>
     </tr>
     <tr><td class="LLine" height="1" colspan="4"></td></tr>
     <tr class="Shade">
        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtPartControlSetup:message key="LBL_CUS_SIZE"/>:</td>  
        <td>&nbsp;<html:text property="customSize" size="30" maxlength="100" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
       </td>  
     </tr>
     <tr>
          <td colspan="4" align="center" height="24">&nbsp;
             <jsp:include page="/common/GmIncludeLog.jsp" >
              <jsp:param name="FORMNAME" value="frmPartControlSetup" />
              <jsp:param name="ALNAME" value="alLogReasons" />
              <jsp:param name="LogMode" value="Edit" />
              <jsp:param name="Mandatory" value="yes" />           
              </jsp:include> 
          </td>
     </tr>
     <tr>
          <td  height="30" colspan="4" align="center">&nbsp;
          <div>
           <%if(hScreenType.equals("popup") && !successMsg.equals("")) {%>
			<font style="color: red" size="1"><b><fmtPartControlSetup:message key="LBL_MESSAGE"/></b></font>
            <%} %>    
          </div>
              <fmtPartControlSetup:message key="BTN_SUBMIT" var= "varSubmit" /><gmjsp:button style="width: 5em" value="${varSubmit}" disabled="<%=strBtnDisable %>"  name="Btn_Submit"  gmClass="button" onClick="fnSubmit();" buttonType="Save" />&nbsp;
              <fmtPartControlSetup:message key="BTN_RESET" var= "varReset" /><gmjsp:button style="width: 5em" value="${varReset}"  disabled="<%=strBtnDisable %>"  name="Btn_Reset"   gmClass="button" onClick="fnReset();"  buttonType="Cancel" />
             <%if(hScreenType.equals("popup")) {%>
              <fmtPartControlSetup:message key="BTN_CLOSE" var="varClose"/><gmjsp:button  style="width: 5em" value="${varClose}" controlId="Btn_Close" name="Btn_Close" gmClass="button" buttonType="Load" />
               <tr><td class="LLine" height="1" colspan="4"></td></tr>
               <%} %>              
          </td>
     </tr>              
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
