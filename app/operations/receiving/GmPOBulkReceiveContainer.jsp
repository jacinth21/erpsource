<%
/*******************************************************************************
 * File		 		: GmPOBulkReceiveContainer.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Arajan
 ******************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib prefix="fmtPOBulkReceiveContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtPOBulkReceiveContainer:setLocale value="<%=strLocale%>"/>
<fmtPOBulkReceiveContainer:setBundle basename="properties.labels.operations.receiving.GmPOBulkReceiveContainer"/>
<!--operations\receiving\GmPOBulkReceiveContainer.jsp  -->
<%
	String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
	String strWikiTitle = GmCommonClass.getWikiTitle("RECEIVE_BULK_SHIPMENT");
	String strApplDateFmt = GmCommonClass.parseNull((String)strGCompDateFmt);
	String struCurrentdate = GmCommonClass.parseNull((String) GmCalenderOperations.getCurrentDate(strApplDateFmt));
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getCmpid())+"\",\"partyid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPartyid())+"\",\"plantid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPlantid())+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<HEAD>
<TITLE><fmtPOBulkReceiveContainer:message key="LBL_PO_BULK"/></TITLE>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmPOBulkReceive.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>
var format = '<%=strApplDateFmt%>';
var currdate = '<%=struCurrentdate%>';
var companyInfoObj = '<%=strCompanyInfo%>';
//var objGridData ='';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  onload="fnOnPageload()">
<html:form action="/gmPOBulkReceive.do?method=loadPO">
<html:hidden property="strOpt" name="frmPOBulkReceive"/>
<html:hidden property="hShelfLife" name="frmPOBulkReceive"/>
<html:hidden property="hProcessClient" name="frmPOBulkReceive"/>
<html:hidden property="size" name="frmPOBulkReceive"/>
<html:hidden property="prodType" name="frmPOBulkReceive"/>
<html:hidden property="vid" name="frmPOBulkReceive"/>
<html:hidden property="haction" name="frmPOBulkReceive"/>

	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPOBulkReceiveContainer:message key="LBL_REC_PO_BULK"/></td>
			<td  colspan="3" class="RightDashBoardHeader" align="right"><fmtPOBulkReceiveContainer:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor:hand' src='/images/help.gif' title='${varHelp}' width='16' height='17' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>&nbsp;
		    </td>
		</tr>
		<tr>
			<td class="RightTableCaption" HEIGHT="27" align="Right">
			<fmtPOBulkReceiveContainer:message key="LBL_PO_NUMBER" var="varPoNumber"/>
				<gmjsp:label type="RegularText" SFLblControlName="${varPoNumber}" td="false"/></td>
			<td align="left">&nbsp;<html:text property="poNumber" styleClass="InputArea" name="frmPOBulkReceive" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onkeypress="javascript:fnLoad(this)" tabindex="1" size="15"/>&nbsp;	<fmtPOBulkReceiveContainer:message key="BTN_LOAD" var = "varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" onClick="javascript:fnLoad(this);" tabindex="2" buttonType="Load" />
			</td>		
		</tr>
		<logic:notEqual name="frmPOBulkReceive" property="strOpt" value="">
			<tr><td class="LLine" colspan="5"></td></tr>
			<tr>
				<!-- This JSP is for the section "PO Details" -->
					<td colspan="5">
						<jsp:include page="/operations/receiving/GmPODetailInclude.jsp" >
						<jsp:param name="FORMNAME" value="frmPOBulkReceive" />
						</jsp:include>
					</td>
			</tr>
			<tr><td class="Line" colspan="5"></td></tr> 
			<tr>
				<!-- This JSP is for the section "Enter Lot Information" -->
					<td colspan="5">
						<jsp:include page="/operations/receiving/GmScanDonorInfoInclude.jsp" >
						<jsp:param name="FORMNAME" value="frmPOBulkReceive" />
						</jsp:include>
					</td>
			</tr>
			<tr><td class="LLine" colspan="5"></td></tr> 
			<tr>
				<!-- This JSP is for the section "Grid" -->
					<td colspan="5">
						<jsp:include page="/operations/receiving/GmDonorInfoProcessInclude.jsp" >
						<jsp:param name="FORMNAME" value="frmPOBulkReceive" />
						</jsp:include>
					</td>
			</tr>
		</logic:notEqual>	
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
