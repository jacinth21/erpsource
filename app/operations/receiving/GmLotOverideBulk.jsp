<%
/**********************************************************************************
 * File		 		: GmLotOverrideBulk.jsp
 * Desc		 		: Upload Lot Override nos in Bulk 
 * Version	 		: 1.0
 * author			: PrabhuVigneshwaran
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtLotNumFileUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmCommonApplicationBean" %>

<!-- GmLotOverrideBulk.jsp -->
<fmtLotNumFileUpload:setLocale value="<%=strLocale%>"/>

<fmtLotNumFileUpload:setBundle basename="properties.labels.operations.receiving.GmLotOverideBulk"/>
<bean:define id="strRefType" name="frmLotOverrideBulk" property="refType" type="java.lang.String"></bean:define>
<bean:define id="strRefID" name="frmLotOverrideBulk" property="strRefID" type="java.lang.String"></bean:define>
<bean:define id="message" name="frmLotOverrideBulk" property="message" type="java.lang.String"></bean:define>

<% 
String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
String strWikiTitle =  GmCommonClass.getWikiTitle("LOT_NUM_OVERRIDE_UPLOAD");
String strWhiteList =  GmCommonClass.parseNull(GmCommonApplicationBean.getLotExpiryConfig("LOT_OVERRIDE_WHITELIST"));
log.debug("strWikiTitle--"+strWikiTitle);
log.debug("strWhiteList--"+strWhiteList);
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Lot Number Upload </TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmLotOverideBulk.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">
<script>
	
	var messages = '<%=message%>';
	var strWhiteList = '<%=strWhiteList%>'
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmLotOverideBulk.do?method=loadLotUpload" enctype="multipart/form-data">
<html:hidden property="strOpt"  name="frmLotOverrideBulk"/>
<html:hidden property="refType" name="frmLotOverrideBulk"/>

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="5">&nbsp;<fmtLotNumFileUpload:message key="LBL_LOT_OVERRIDE_BULK"/></td>
			<td align="right" class=RightDashBoardHeader><fmtLotNumFileUpload:message key="LBL_HELP" var="varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>		
			<% if(!message.equals("")){ %>			
			<tr>
				<td colspan="7" align="center" height="25">&nbsp;<font style="color: Green;"><b><bean:write name="frmLotOverrideBulk" property="message"/></b></font></td>
			</tr>
		<%}%>
		<tr><td class="LLine" colspan="7" height="1"></td></tr>		
		<tr class="shade">
			<td height="30" colspan="5" class="RightTableCaption" align="right"><fmtLotNumFileUpload:message key="LBL_SELECT_FILE_TO_UPLOAD"/>:</td>
			<td>&nbsp;
		<input type="file" id="file" name="file" size="50"  class="multi" accept="*.xls" value="">
			</td></tr>			
		<tr><td class="LLine" colspan="7" height="1"></td></tr>
		<tr>
			<td colspan="7" height="30" align="center">
			    <fmtLotNumFileUpload:message key="BTN_UPLOAD" var="varUpload"/>
				<gmjsp:button  name="Submit" value="${varUpload}"  gmClass="button" buttonType="Save" onClick="fnUpload(this);"/>
			</td>
		</tr>			
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>