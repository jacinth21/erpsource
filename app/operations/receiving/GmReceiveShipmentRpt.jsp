<%
/**********************************************************************************
 * File		 		: GmReceiveShipmentRpt.jsp
 * Desc		 		: Receiving Shipment Report
 * Version	 		: 1.0
 * author			: Anilkumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>  
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!--operations\receiving\GmReceiveShipmentRpt.jsp  -->
<%@ taglib prefix="fmtReceiveShipmentRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReceiveShipmentRpt:setLocale value="<%=strLocale%>"/>
<fmtReceiveShipmentRpt:setBundle basename="properties.labels.operations.receiving.GmReceiveShipmentRpt"/>
<%
String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
String strApplDateFmt = GmCommonClass.parseNull((String)strGCompDateFmt);
String struCurrentdate = GmCommonClass.parseNull((String)GmCalenderOperations.getCurrentDate(strApplDateFmt));
String strDtFmt = "{0,date,"+strApplDateFmt+"}";

//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);

%>

<bean:define id="strXmlGridData" name="frmReceiveShipRpt" property="xmlGridData" type="java.lang.String"></bean:define>
<bean:define id="strRecShipID" name="frmReceiveShipRpt" property="recShipID" type="java.lang.String"></bean:define>
<bean:define id="strFromDate" name="frmReceiveShipRpt" property="fromDate" type="java.lang.String"></bean:define>
<bean:define id="strToDate" name="frmReceiveShipRpt" property="toDate" type="java.lang.String"></bean:define>
				
<HTML>
<HEAD>
<TITLE> <fmtReceiveShipmentRpt:message key="LBL_RECV_DASH"/> </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmReceiveShipmentRpt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script>
var objGridData = '';
objGridData = '<%=strXmlGridData%>';
var format = '<%=strApplDateFmt%>';
var currdate = '<%=struCurrentdate%>';
var compInfObj = '<%=strCompanyInfo%>'; 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnload()">
<FORM NAME="frmReceiveShipRpt" action="/gmReceiveShipRpt.do?">
<input type="hidden" name="haction"/>
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="6">&nbsp;<b><fmtReceiveShipmentRpt:message key="LBL_RECV_SHIP_REC"/></b></td>
			<td align="right" class=RightDashBoardHeader colspan="2">
			<fmtReceiveShipmentRpt:message key="LBL_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("RECEIVE_SHIPMENT_REPORT")%>');" />
			</td>
		</tr>
		<tr><td class="Line" height="1" colspan="8"></td></tr>			
		<tr>
			<td class="RightTableCaption" height="30" align="right" width="200">
			<fmtReceiveShipmentRpt:message key="LBL_RECEIVE_SHIPMENT_ID" var="varReceiveShipmentId"/>
				<gmjsp:label type="RegularText" SFLblControlName="${varReceiveShipmentId}" td="false" />&nbsp;</td>
			<td width="200" colspan="3" height="30">
				<html:text property="recShipID" styleClass="InputArea" name="frmReceiveShipRpt" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="1" size="30"/>
			</td>
			<td width="715" colspan="5" width="100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtReceiveShipmentRpt:message key="BTN_LOAD" var="varLoad" /><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" tabindex="2" style="width: 7em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" onClick="fnLoad()"  buttonType="Load" />
			</td>
		</tr>
		<tr>
			<td class="Line" height="1" colspan="8"></td>
		</tr>
		<tr class="oddshade">
			<td class="RightRedCaption" align="right" width="200" height="26"><fmtReceiveShipmentRpt:message key="LBL_FROM_DATE"/>:&nbsp;</td>
			<td width="150"> <gmjsp:calendar SFFormName="frmReceiveShipRpt" 
				controlName="fromDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff'); " tabIndex="3"/>
			</td>
			<td class="RightRedCaption" align="left" height="26"><fmtReceiveShipmentRpt:message key="LBL_TO_DATE"/>&nbsp;</td>
			<td colspan="6"><gmjsp:calendar SFFormName="frmReceiveShipRpt" 
				controlName="toDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="4"/>
			</td>			
		</tr>
		<tr><td class="Line" height="1" colspan="8"></td></tr>
		<%if(strXmlGridData.indexOf("cell") != -1){%>
		<tr>
			<td colspan="8">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td><div id="recshipRpt" style="height: 500px;"></div></td>
					</tr>
					<tr>
						<td colspan="8" align="center" height="25">
							<div class='exportlinks'><fmtReceiveShipmentRpt:message key="LBL_EXP_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"> <fmtReceiveShipmentRpt:message key="LBL_EXCEL"/> </a></div>
						</td>                 
					</tr>
					
				</table> 
			</td>
		</tr>				
		<%}else{%>
		<tr>
			<td colspan="8" height="25" align="center" class="RightText"><fmtReceiveShipmentRpt:message key="LBL_NO_DATA"/></td>
		</tr>
		<%}%>
    </table>		     	

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
