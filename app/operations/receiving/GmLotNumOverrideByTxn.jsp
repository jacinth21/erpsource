<%
/**********************************************************************************
 * File		 		: GmLotNumOverrideByTxn.jsp
 * Desc		 		: Lot override by transaction id 
 * Version	 		: 1.0
 * author			: Agilan Singaravel
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLotNumOverrideByTxn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmLotNumOverrideByTxn.jsp -->
<fmtLotNumOverrideByTxn:setLocale value="<%=strLocale%>"/>

<fmtLotNumOverrideByTxn:setBundle basename="properties.labels.operations.receiving.GmLotNumOverrideByTxn"/>
<bean:define id="lotOverrideTxnids" name="frmPartControlSetup" property="lotOverrideTxnids" type="java.lang.String"></bean:define>

<% 
String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
String strWikiTitle =  GmCommonClass.getWikiTitle("LOT_OVERRIDE_BY_TRANSACTION");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Lot Override by Transactions</TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmLotNumOverrideByTxn.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">

</HEAD>
<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmLotOverideBulk.do?method=loadLotUpload" enctype="multipart/form-data">

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="5">&nbsp;<fmtLotNumOverrideByTxn:message key="LBL_LOT_OVERRIDE_TRANSACTION"/></td>
			<td align="right" class=RightDashBoardHeader><fmtLotNumOverrideByTxn:message key="LBL_HELP" var="varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>		
	 <tr>
	 	 <td colspan="6"><div id="successCont" style="text-align: center; font-size: 12px;display:none;height:20px;"><fmtLotNumOverrideByTxn:message key="LBL_SUCESS_MSG"/></div></td>
	 </tr>		
		<tr><td class="LLine" colspan="6" height="1"></td></tr>		
		<tr>
		   <td   class="RightTableCaption" HEIGHT="24" align="right" colspan="1">&nbsp;<fmtLotNumOverrideByTxn:message key="LBL_TRANSACTION_ID"/>: </td>
	       <td   height="40%" width="60%" style="padding-top:18px;">&nbsp;<textarea name="lotOverrideTxnids"  id="lotOverrideTxnids" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
		         onBlur="changeBgColor(this,'#ffffff');"  rows=20 cols=70 ><%=lotOverrideTxnids%></textarea></td> </tr>		
		   <tr> <td colspan="1"></td><td style="padding-top:10px;padding-left:5px;"><fmtLotNumOverrideByTxn:message key="LBL_MESSAGE"/></td></tr>
		<tr><td class="LLine" colspan="7" height="1"></td></tr>
		<tr>
			<td colspan="6" height="30" align="center">
			    <fmtLotNumOverrideByTxn:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button  name="Submit" value="${varSubmit}"  gmClass="button" buttonType="Save" onClick="fnSubmitLotTrans();"/>
			</td>
		</tr>			
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>