<%
/**********************************************************************************
 * File		 		: GmDHRInfoInclude.jsp
 * Desc		 		:  Receive Bulk Shipment - DHR Tab
 * Version	 		: 1.0
 * author			: Anilkumar
************************************************************************************/
%>
<%@ page language="java" %>
  <!-- \operations\purchasing\orderplanning\GmDHRInfoInclude.jsp-->
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtDHRInfoInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtDHRInfoInclude:setLocale value="<%=strLocale%>"/>
<fmtDHRInfoInclude:setBundle basename="properties.labels.operations.receiving.GmDHRInfoInclude"/>
<!--operations\receiving\GmDHRInfoInclude.jsp  -->
<bean:define id="strXmlGridData" name="frmOperDashBoardDispatch" property="strGridXmlData" type="java.lang.String"> </bean:define>
<%
//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);	
%>
<HTML>
<HEAD>
<TITLE><fmtDHRInfoInclude:message key="LBL_RECV_SHIPM"/></TITLE>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmDHRInfoInclude.js"></script>
<script>
var objGridData = '';
objGridData = '<%=strXmlGridData%>';
var compInfObj = '<%=strCompanyInfo%>'; 
</script>
</HEAD>

<BODY  onload="fnOnload()">
<FORM NAME="frmOperDashBoardDispatch" action="/gmOperDashBoardDispatch.do?method=DHRDashForShipment">
<input type="hidden" name="hVenId"/>
<input type="hidden" name="hDHRId"/>
<input type="hidden" name="hAction"/>

	<table border="0" width="100%"  cellspacing="0" cellpadding="0">
		<%if(strXmlGridData.indexOf("cell") != -1){%>
		<tr>
			<td>
			<div id="dhrshipmntRpt" style="height: 150px;width: 1070px"></div>
			</td>
		</tr>
		<%}else{%>
		<tr>
			<td align="center" class="RightText"><fmtDHRInfoInclude:message key="LBL_NO_DATA"/></td>
		</tr>
		<%}%>
    </table>		     	

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
