<%
/**********************************************************************************
 * File		 		: GmReservedLots.jsp
 * Desc		 		: This screen is used fetch the Reserved Lots information
 * Version	 		: 1.0
 * author			: rdinesh
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtReservedLots" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- operations\receiving\GmReservedLots.jsp -->
<bean:define id="gridData" name="frmLotTrackReport" property="gridXmlData" type="java.lang.String"> </bean:define>

<%
	String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
	String strWikiTitle = GmCommonClass.getWikiTitle("RESERVED_LOTS");
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
%>
<fmtReservedLots:setLocale value="<%=strLocale%>"/>
<fmtReservedLots:setBundle basename="properties.labels.operations.receiving.GmReservedLots"/>
<HTML>
<HEAD>
<TITLE>Globus Medical: Reserved Lots Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/GmCommon.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmReservedLots.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/<%=strExtWebPath%>/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
	var objGridData = '<%=gridData%>';
	var format = '<%=strApplDateFmt%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad()" onkeypress="fnEnter()">

<html:form action="/gmReservedLots.do?method=fetchReservedLotsDetails">
<html:hidden property="strOpt" value="frmLotTrackReport"/>
<input type="hidden" name="hSearch" value="<%=strSearch%>">
<table class="DtTable1300" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="8">&nbsp;<fmtReservedLots:message key="LBL_RES_LOT"/></td>
				<td align="right" class=RightDashBoardHeader><fmt:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
 					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
			</tr>
			<tr>
				<td class="RightTableCaption" HEIGHT="30" align="right" width="7%">
				<fmtReservedLots:message key="LBL_PART" var="varPart"/>
						<gmjsp:label type="RegularText" SFLblControlName="${varPart} #:" td="false" /></td>
				<td width="20%">&nbsp;<html:text property="partNum" styleClass="InputArea" name="frmLotTrackReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="1" size="15"/>&nbsp;<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
						<option value="0" ><fmtReservedLots:message key="LBL_CHOOSE_ONE"/>
						<option value="LIT" ><fmtReservedLots:message key="LBL_LITERAL"/>
						<option value="LIKEPRE" ><fmtReservedLots:message key="LBL_LIKPRE"/>
						<option value="LIKESUF" ><fmtReservedLots:message key="LBL_LIKSUF"/>
					</select>
				</td>
				<td class="RightTableCaption" HEIGHT="30" align="right">
				<fmtReservedLots:message key="LBL_CONTROL_NUM" var="varControlNum"/>
						<gmjsp:label type="RegularText" SFLblControlName="${varControlNum}:" td="false" /></td>
				<td>&nbsp;<html:text property="ctrlNum" styleClass="InputArea" name="frmLotTrackReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="2" size="15"/>
						</td>
				<td class="RightTableCaption" HEIGHT="30" align="right" >
				<fmtReservedLots:message key="LBL_DONOR" var="varDonor"/>
						<gmjsp:label type="RegularText" SFLblControlName="${varDonor} #:" td="false" /></td>
				<td>&nbsp;<html:text property="donorNum" styleClass="InputArea" name="frmLotTrackReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" size="15"/>
						</td>
				<td class="RightTableCaption" HEIGHT="30" align="right">
				<fmtReservedLots:message key="LBL_REF_ID" var="varRefId"/>
						<gmjsp:label type="RegularText" SFLblControlName="${varRefId}:" td="false" /></td>
				<td>&nbsp;<html:text property="refId" styleClass="InputArea" name="frmLotTrackReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="2" size="15"/>
						</td>
			</tr>
			<tr><td colspan="9" height="1" class="LLine"></td></tr>
			<tr class="shade">
				<td class="RightTableCaption" HEIGHT="30" align="right" width="7%">
				<fmtReservedLots:message key="LBL_WAREHOUSE" var="varWarehouse"/>
						<gmjsp:label type="RegularText" SFLblControlName="${varWarehouse}:" td="false" /></td>
				<td>&nbsp;<gmjsp:dropdown controlName="warehouse" SFFormName="frmLotTrackReport" SFSeletedValue="warehouse"
						SFValue="alWareHouse" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="5" width="220"/>
						</td>
				<td class="RightTableCaption" HEIGHT="30" align="right">
				<fmtReservedLots:message key="LBL_EXP_DT" var="varExpiryDate"/>
						<gmjsp:label type="RegularText" SFLblControlName="${varExpiryDate}:" td="false" /></td>
				<td>&nbsp;<gmjsp:dropdown controlName="expDateRange" SFFormName="frmLotTrackReport" SFSeletedValue="expDateRange"
						SFValue="alExpDateRange" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="5" width="105"/> 
				<gmjsp:calendar SFFormName="frmLotTrackReport" controlName="expDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="6"/>		
				</td>
				<td class="RightTableCaption" HEIGHT="30" align="right">&nbsp;<fmtReservedLots:message key="LBL_INT_USE" var="varInUse"/><gmjsp:label type="RegularText" SFLblControlName="${varInUse}:" td="false" /></td>
				<td width="15%">&nbsp;<html:checkbox property="internationalUse" name="frmLotTrackReport" /></td>		
				<td colspan="3" align="center"><fmtReservedLots:message key="BTN_LOAD" var = "varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" onClick="javascript:fnLoad();" tabindex="7" buttonType="Load" /></td>				
			</tr>
			<%if(gridData.indexOf("cell") != -1) {%> 
				<tr>
					<td colspan="9"><div  id="dataGridDiv" width="1300" height="400px"></div>
					<div id="pagingArea" width="1300px"></div></td>
				</tr>
				<tr>						
					<td align="center" colspan="9"><br>
	             			<div class='exportlinks'><fmtReservedLots:message key="LBL_EXPORT_OPT" />: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"><fmtReservedLots:message key="LBL_EXCEL"/>  </a>	                         
	       			</td>
       			<tr>
			<%}else  if(!gridData.equals("")){%>
				<tr><td colspan="9" height="1" class="LLine"></td></tr>
				<tr height="25px"><td colspan="9" align="center" class="RightText" ><fmtReservedLots:message key="LBL_NOT_FOUND"/></td></tr>
			<%}else{ %>
				<tr><td colspan="9" height="1" class="LLine"></td></tr>
				<tr height="25px"><td colspan="9" align="center" class="RightText" ><fmtReservedLots:message key="LBL_NO_DATA"/></td></tr>
			<%} %>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
