<%
/*********************************************************************************************************
 * File		 		: GmItemControl.jsp
 * Desc		 		: This screen is used to control items of transaction
 * Version	 		: 1.0
 * author			: Gopinathan
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ include file="/common/GmHeader.inc"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="frmBBARedesignatedash" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmItemControl.jsp -->
<frmBBARedesignatedash:setLocale value="<%=strLocale%>"/>
<frmBBARedesignatedash:setBundle basename="properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO"/>

<bean:define id="isTxnVerified" name="frmBBARedesignatedash" property="isTxnVerified" type="java.lang.String"> </bean:define>
<bean:define id="strTxnIds" name="frmBBARedesignatedash" property="txnid" type="java.lang.String"></bean:define>
<bean:define id="strTxnType" name="frmBBARedesignatedash" property="txntype" type="java.lang.String"></bean:define>
<bean:define id="contype" name="frmBBARedesignatedash" property="contype" type="java.lang.String"></bean:define>
<% 
String strOpItCntrlJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_ITEMCONTROL");
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ModifyOrder.GmModifyOrderEditPO", strSessCompanyLocale);
String strHeaderRule="";
String strHeader="Part Labeling";
String strSubmit =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUBMIT"));
HashMap hmTransRules = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("TRANS_RULES"));
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
strCountryCode = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
String strConType = GmCommonClass.parseNull(request.getParameter("contype"));
String strOpenControl = GmCommonClass.parseNull(GmCommonClass.getString("MODIFY_ORDER_OPEN_CONTROL")); 
strHeaderRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_HEADER"));
String strPickLocRule = GmCommonClass.parseNull((String)hmTransRules.get("PICK_LOC"));
String strPutLocRule = GmCommonClass.parseNull((String)hmTransRules.get("PUT_LOC"));
String strCancelType = GmCommonClass.parseNull((String)hmTransRules.get("CANCEL_TYPE"));
String strControlNoScan = GmCommonClass.parseNull((String)GmCommonClass.getString("SCAN_CONTROL_NO"));
String strSubPickLocRule = "fnSubmit('"+strPickLocRule+"');";
String strSubPutLocRule = "fnSubmit('"+strPutLocRule+"');";
String strPrintPick = "fnPrintPick('"+strTxnIds+"');";


/* The following code is added for Disableing the VOID button for IHLN[9110] Transaction Type */
String strDisable   = "";
String strTransType = GmCommonClass.parseNull((String)hmTransRules.get("TRANS_ID"));
if(strTransType.equals("9110")){
	strDisable = "true";
}

//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	

%> 

<HTML>
<HEAD>
<TITLE>Globus Medical: Status Log Detail</TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOpItCntrlJsPath%>/GmItemControl.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>
<bean:define id="StrAction" name="frmBBARedesignatedash" property="haction" type="java.lang.String"></bean:define>
<bean:define id="StrStatus" name="frmBBARedesignatedash" property="statusfl" type="java.lang.String"></bean:define>
<bean:define id="strFGBinFl" name="frmBBARedesignatedash" property="strFGBinFl" type="java.lang.String"></bean:define>

<script>
var FGBinFl = '<%=strFGBinFl%>';
var statusFl = '<%=StrStatus%>';
var cancelType = '<%=strCancelType%>';
var companyInfoObj = '<%=strCompanyInfo%>';
var strTxnType = '<%=strTxnType%>';
var StrAction = '<%=StrAction%>'; 
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnPageLoad();">

<html:form styleId="frmBBARedesignatedash">
	<html:hidden name="frmBBARedesignatedash" styleId="haction" property="haction" />
	<html:hidden name="frmBBARedesignatedash" styleId="inputString" property="inputString"/>
	<html:hidden name="frmBBARedesignatedash" styleId="fgBinString" property="fgBinString"/>
    <html:hidden name="frmBBARedesignatedash" styleId="txnid" property="txnid" />
    <html:hidden name="frmBBARedesignatedash" styleId="txntype" property="txntype" />
    <html:hidden name="frmBBARedesignatedash" styleId="loctype"  property="loctype" />
    <html:hidden name="frmBBARedesignatedash" styleId="statusfl" property="statusfl" />   
    <html:hidden name="frmBBARedesignatedash" styleId="pickputfl" property="pickputfl" />
    <html:hidden name="frmBBARedesignatedash" styleId="contype" property="contype" />
    <html:hidden name="frmBBARedesignatedash" styleId="refid" property="refid" />
    <html:hidden name="frmBBARedesignatedash" styleId="chkRule" property="chkRule" value="No"/>
    <input type="hidden" value="<%=strTxnIds%>" name="hConsignId" id="hConsignId">
    <input type="hidden" value="<%=strTxnIds%>" name="hTxnId" id="hTxnId">
    <input type="hidden" name="ruleVerify" value="true">
    <input type="hidden" name="partMaterialType" id="partMaterialType" value= "">
    <input type="hidden" name="TXN_TYPE_ID" value=<bean:write name="frmBBARedesignatedash" property="txntype"/>>
	<table class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<logic:equal name="frmBBARedesignatedash" property="haction" value="PartLableEditInspection">
				<td height="25" class="RightDashBoardHeader"><%=strHeader%>: <frmBBARedesignatedash:message key="LBL_PART_LABEL_HEADER"/></td>
			</logic:equal>
			<logic:equal name="frmBBARedesignatedash" property="haction" value="PartLableEditControl">
				<td height="25" class="RightDashBoardHeader"><%=strHeader%>: <frmBBARedesignatedash:message key="LBL_PART_LABEL_HEADER1"/></td>
			</logic:equal>
			<logic:equal name="frmBBARedesignatedash" property="haction" value="PartLableEditVerify">
				<td height="25" class="RightDashBoardHeader"><%=strHeader%>: <frmBBARedesignatedash:message key="LBL_PART_LABEL_HEADER2"/></td>
			</logic:equal>
			<frmBBARedesignatedash:message key="LBL_HELP" var="varHelp"/>
			<td align="right" class=RightDashBoardHeader><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("LOCATION_PART_MAPPING")%>');" />
			</td>
		</tr>

		<tr>
			<td colspan="2">
				<jsp:include page="/operations/itemcontrol/GmItemHeaderInclude.jsp">
				<jsp:param name="FORMNAME" value="frmBBARedesignatedash" />
				</jsp:include>
			</td>
		</tr>
<%
if(StrAction.equals("PartLableEditControl")||StrAction.equals("PartLableEditInspection")){
%>
		<tr>
			<td colspan="2">
				<jsp:include page="/operations/itemcontrol/GmItemDetailInclude.jsp">
				<jsp:param name="FORMNAME" value="frmBBARedesignatedash" />
				</jsp:include>
			</td>
		</tr>
<%
}else if(StrAction.equals("PartLableEditVerify")){
%>
		<tr>
			<td colspan="2">
				<jsp:include page="/operations/itemcontrol/GmItemVerifyDtlInclude.jsp">
				<jsp:param name="FORMNAME" value="frmBBARedesignatedash"/>
				</jsp:include>
			</td>
		</tr>
<%} %>
<tr>
			<%if(StrAction.equals("PartLableEditInspection")){ %>
				<td align="center"><gmjsp:button value="Submit"  name="Btn_Submit" gmClass="button" onClick="" buttonType="Save" />&nbsp;&nbsp;</td>
			<%}else if(StrAction.equals("PartLableEditControl")){ %>
				<td align="center"><gmjsp:button value="Process"  name="Btn_Submit" gmClass="button" onClick="" buttonType="Save" />&nbsp;&nbsp;</td>
			<%}else if(StrAction.equals("PartLableEditVerify")){ %>
			    <td align="center"><gmjsp:button value="Verify"  name="Btn_Submit" gmClass="button" onClick="" buttonType="Save" />&nbsp;&nbsp;</td>
			<%} %>
</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>