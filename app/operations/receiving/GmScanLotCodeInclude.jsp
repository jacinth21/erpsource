 <%
/*********************************************************************************************************
 * File		 		: GmScanLotCodeInclude.jsp
 * Desc		 		: This is used to show the lot code section to scan the lot number
 * Version	 		: 1.0
 * author			: arajan
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtScanLotCodeInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtScanLotCodeInclude:setLocale value="<%=strLocale%>"/>
<fmtScanLotCodeInclude:setBundle basename="properties.labels.operations.receiving.GmScanLotCodeInclude"/>
<!-- operations\receiving\GmScanLotCodeInclude.jsp -->
<% 
String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Control No details</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmScanLotCodeInclude.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmRSBulkReceive.js"></script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onkeypress="fnCallSubmit('enter');">
	
<table style="width: 100%; border: 0" cellspacing="0" cellpadding="0">
	
		<tr>
			<td height="24" class="ShadeRightTableCaption" colspan="5">&nbsp;<b><fmtScanLotCodeInclude:message key="LBL_SCAN_LOT"/></b></td>
		</tr>
		<tr><td class="LLine" colspan="5"><embed src="sounds/beep.wav" autostart="false" width="0" height="0" id="beep" enablejavascript="true"></td></tr> 
		<tr height="30">
			<td class="RightText" HEIGHT="20" align="right" width="20%"><b><fmtScanLotCodeInclude:message key="LBL_LOT_CODE"/>:</b></td>
			<td width="80%">&nbsp;<input type="text" name="scanLotCode" id="scanLotCode" class="InputArea" style="text-transform:uppercase;" maxlength="40" onkeypress="javascript:fnVerifyLotCode(this);" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnUpperCase(this);" tabindex="3"/> </td>
		</tr>
		<tr>
			<td></td>
			<td align="left"><div id="errormessage"></div></td>
		</tr>
		<tr><td class="LLine" colspan="5"></td></tr> 

</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>