<%
/**********************************************************************************
 * File		 		: GmDonorInfoProcessInclude.jsp
 * Desc		 		: This JSP is for the section of generated lot information
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<!-- operations\receiving\GmDonorInfoProcessInclude.jsp -->
<%@ taglib prefix="fmtDonorInfoProcessInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtDonorInfoProcessInclude:setLocale value="<%=strLocale%>"/>
<fmtDonorInfoProcessInclude:setBundle basename="properties.labels.operations.receiving.GmDonorInfoProcessInclude"/>
<bean:define id="gridData" name="frmPOBulkReceive" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="strDisabled" name="frmPOBulkReceive" property="strDisabled" type="java.lang.String"> </bean:define>
<bean:define id="strMsg" name="frmPOBulkReceive" property="strMsg" type="java.lang.String"> </bean:define>
<bean:define id="strbuttonName" name="frmPOBulkReceive" property="strbuttonName" type="java.lang.String"> </bean:define>
<bean:define id="strSbmtBtnDisabled" name="frmPOBulkReceive" property="strSbmtBtnDisabled" type="java.lang.String"> </bean:define>
<bean:define id="strJSFunciton" name="frmPOBulkReceive" property="strJSFunciton" type="java.lang.String"> </bean:define>
<bean:define id="strAllChecked" name="frmPOBulkReceive" property="strAllChecked" type="java.lang.String" ></bean:define>
<%

//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getCmpid())+"\",\"partyid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPartyid())+"\",\"plantid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPlantid())+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE><fmtDonorInfoProcessInclude:message key="LBL_LOT_INFO"/>  </TITLE>

<script>
var objGridData = '<%=gridData%>';
var companyInfoObj = '<%=strCompanyInfo%>';

</script>
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<BODY leftmargin="20" topmargin="10" onload="fnOnPageload()">
<html:form action="/gmPOBulkReceive.do?method=loadBatchshipment">
<html:hidden property="strAllChecked" name="frmPOBulkReceive" styleId="strAllChecked"/>
	<table class="DtTable1100" border="0" cellspacing="0" cellpadding="0">
		<tr><td>
			<div id="dataGrid" style="height:490px;width: 1080px;"></div>
		</td></tr>
		<tr>
			<td>			
			<table id="recPO" width="100%" cellspacing="0" border="1" cellpadding="0" style="display: none;"> 
				<tr height="27"><td align="center" >
						<fmtDonorInfoProcessInclude:message key="BTN_SUBMIT" var="varSubmit" /><gmjsp:button name="Btn_Submit" value="${varSubmit}" gmClass="button" onClick="javascript:fnSubmitBulkDHR(this);" tabindex="14" buttonType="Save" />&nbsp;
						<%-- <gmjsp:button name="Btn_Generate" value="Reset" gmClass="button" onClick="javascript:fnReset(this);" tabindex="12" buttonType="Load" />&nbsp; --%>
				</td></tr>
			</table>
			</td></tr>
			<tr><td>
			<table id="recRS" width="100%" cellspacing="0" cellpadding="0" style="display: none;">
			<logic:equal name="frmPOBulkReceive" property="accessfl" value="Y">
				<logic:equal name="frmPOBulkReceive" property="statusId" value="1">
					<logic:equal name="frmPOBulkReceive" property="strApprRej" value="">
						<tr>
							<td height="24" class="ShadeRightTableCaption" colspan="5">&nbsp;<b><fmtDonorInfoProcessInclude:message key="LBL_REJECTION" /></b></td>
						</tr>
						<tr><td class="LLine" colspan="5"></td></tr> 
						
						<tr>
							<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtDonorInfoProcessInclude:message key="LBL_REJECTION_TYP" />:</b></td>
							<td>&nbsp;<gmjsp:dropdown controlName="rejType" SFFormName="frmPOBulkReceive" SFSeletedValue="rejType"
										SFValue="alrejType" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" /></td>
						
							<td class="RightTableCaption" align="right" height="25"><fmtDonorInfoProcessInclude:message key="LBL_REA_REJ_TYP" />:</td>
							<td height="70px">&nbsp;<html:textarea property="rejReason" styleId="rejReason"  cols="35" style="height:50px"
								styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
							</td>
						</tr> 
						<tr><td class="LLine" colspan="4"></td></tr>
						<%
							if (!strMsg.equals("")) {
						%>
						<tr>
							<td  align="center" colspan="4" height="25"> <font color="red"><b><%=strMsg%></b></font><br/>
							</td>
						</tr>
						<%
							}
						%>
						<tr><td class="Line" colspan="4"></td></tr>
					</logic:equal>
					<logic:notEqual name="frmPOBulkReceive" property="strApprRej" value="">
						<tr>
							<td height="24" class="ShadeRightTableCaption" colspan="4">&nbsp;<b><fmtDonorInfoProcessInclude:message key="LBL_REJECTION"/></b></td>
						</tr>
						<tr><td class="LLine" colspan="4"></td></tr> 
						
						<tr>
							<td class="RightText" HEIGHT="20" align="right" width="15%">&nbsp;<b><fmtDonorInfoProcessInclude:message key="LBL_REJECTION_TYP"/>:</b></td>
							<td width="30%" id="rejTypeLbl">&nbsp;<bean:write name="frmPOBulkReceive" property="rejType"/></td>
						
							<td class="RightTableCaption" align="right" height="25" width="15%"><fmtDonorInfoProcessInclude:message key="LBL_REA_REJ_TYP"/>:</td>
							<td height="70px" width="40%" id="rejReasonLbl">&nbsp;<bean:write name="frmPOBulkReceive" property="rejReason"/>
							</td>
						</tr> 
						<tr><td class="Line" colspan="4"></td></tr>
					</logic:notEqual>
					
				</logic:equal>
			</logic:equal>
			<tr><td class="RightCaption" align="center" colspan="4"> <div id="errMsg"></div> </td></tr>
			<tr><td align="center" height="30" colspan="4">
					<logic:equal name="frmPOBulkReceive" property="statusId" value="1">
						<fmtDonorInfoProcessInclude:message key="BTN_SUBMIT" var="varSubmit" /><gmjsp:button name="Btn_RSSubmit" value="${varSubmit}" gmClass="Button" onClick="javascript:fnSubmitBulkDHR(this.form);" disabled="<%=strSbmtBtnDisabled %>" buttonType="Save" />&nbsp;
					</logic:equal>
					<logic:equal name="frmPOBulkReceive" property="accessfl" value="Y">
					<gmjsp:button name="Btn_Release" value="<%=strbuttonName%>" gmClass="button" onClick="<%=strJSFunciton%>" disabled="<%=strDisabled %>" buttonType="Save" />&nbsp;
					</logic:equal>
					<fmtDonorInfoProcessInclude:message key="BTN_PRINT" var="varPrint" /><gmjsp:button name="Btn_Print" value="${varPrint}" gmClass="Button" onClick="javascript:fnPrint(this);" buttonType="Load" />&nbsp;
			</td></tr>
			</table>
			</td>
		</tr>
	</table>

<%@ include file="/common/GmFooter.inc"%>
</html:form>
</BODY>
</HTML>