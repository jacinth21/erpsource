 <%
/**********************************************************************************
 * File		 		: GmReservedLog.jsp
 * Desc		 		: This screen is used to display the Reserved log 
 * Version	 		: 1.0
 * author			: Anilkumar
************************************************************************************/

%>

<%@ include file="/common/GmHeader.inc" %>
<!-- operations\receiving\GmReservedLog.jsp -->
<%@ taglib prefix="fmtReservedLog" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtReservedLog:setLocale value="<%=strLocale%>"/>
<fmtReservedLog:setBundle basename="properties.labels.operations.receiving.GmReservedLog"/>
<%
String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
%>

<bean:define id="strXmlGridData" name="frmReservedLogRpt" property="xmlGridData" type="java.lang.String"></bean:define>
<bean:define id="strFromDate" name="frmReservedLogRpt" property="fromDate" type="java.lang.String"></bean:define>
<bean:define id="strToDate" name="frmReservedLogRpt" property="toDate" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmReservedLogRpt" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="strInventoryType" name="frmReservedLogRpt" property="inventoryType" type="java.lang.String"></bean:define>

<HTML>
<HEAD>
<TITLE><fmtReservedLog:message key="LBL_REPORT"/>       </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmReservedLog.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script>
var objGridData = '';
objGridData = '<%=strXmlGridData%>';
var format = '<%=strApplDateFmt%>';
var inventoryType = '<%=strInventoryType%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnload()">
<FORM name="frmReservedLogRpt" action="/gmReservedLogRpt.do?">
<input type="hidden" name="strOpt" value="">
<input type="hidden" name="hAction" value="<%=strOpt%>">

	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="26" class="RightDashBoardHeader" colspan="4">&nbsp;<b><fmtReservedLog:message key="LBL_LOT_NO"/></b></td>
			<td align="right" class=RightDashBoardHeader colspan="2"><fmtReservedLog:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("RESERVED_LOG_REPORT")%>');" />
			</td>
		</tr>
		
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		
		<tr>
			<td class="RightTableCaption" height="26" align="right">
			<fmtReservedLog:message key="LBL_PART_NUMBERS" var = "varPartNumbers"/>
				<gmjsp:label type="RegularText" SFLblControlName="${varPartNumbers}" td="false" />&nbsp;
			</td>
			<td height="26" colspan="5">
				<html:text property="partNum" styleClass="InputArea" name="frmReservedLogRpt" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="1" size="30"/>
				<span class="RightTextBlue"><fmtReservedLog:message key="LBL_ENTER_PART#"/></span>
			</td>
		</tr>
		
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		
		<tr class="Shade">
			<td class="RightTableCaption" height="26" align="right">
			<fmtReservedLog:message key="LBL_LOT_NUMBER" var = "varLotNumber"/>
				<gmjsp:label type="RegularText" SFLblControlName="${varLotNumber}" td="false" />&nbsp;
			</td>
			<td height="26" colspan="5">
				<html:text property="lotNum" styleClass="InputArea" name="frmReservedLogRpt" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="2" size="30"/>				
			</td>
		</tr>
		
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		
		<tr>
			<td class="RightTableCaption" HEIGHT="26" align="right"><fmtReservedLog:message key="LBL_DT_RANGE"/>:&nbsp;</td>
			<td class="RightText" colspan="5">
				&nbsp;<fmtReservedLog:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar SFFormName="frmReservedLogRpt" controlName="fromDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  tabIndex="3" /> 
				&nbsp;&nbsp;<fmtReservedLog:message key="LBL_TO"/> <gmjsp:calendar SFFormName="frmReservedLogRpt" controlName="toDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="4" />
			</td>
		</tr>
		
		<tr><td class="LLine" height="1" colspan="6"></td></tr>   
		
		<tr class="Shade">
		<td class="RightTableCaption" height="26" align="right">
		<fmtReservedLog:message key="LBL_TRANSACTION_ID" var = "varTransactionId"/>
			<gmjsp:label type="RegularText" SFLblControlName="${varTransactionId}:" td="false" />&nbsp;</td>
		<td colspan="5" height="26">
			<html:text property="transId" styleClass="InputArea" name="frmReservedLogRpt" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="5" size="30"/>
		</td>
		</tr>
		
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		
		<tr>
			<td class="RightTableCaption" align="right" height="26" ><fmtReservedLog:message key="LBL_INV_TYP"/>:&nbsp;</td>				
			<td>
			<select name="inventoryType" class="RightText" tabindex="6">
									<option value="0"><fmtReservedLog:message key="LBL_CH_ONE"/>
									<option value="90800"><fmtReservedLog:message key="LBL_INV_QTY"/>
									<option value="90813"><fmtReservedLog:message key="LBL_QUARANTINE_INV"/>
									<option value="90815"><fmtReservedLog:message key="LBL_REPACKAGE_QTY"/>
			</select>
			</td>		
			<td colspan="4" align="left"><fmtReservedLog:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" align="left" gmClass="button" style="width: 7em; height: 24px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" onClick="fnSubmit()"  buttonType="Load" tabindex="7" />
			</td>
		</tr>
		
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		
		<%if(strXmlGridData.indexOf("cell") != -1){%>
		<tr>
			<td colspan="6">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td><div id="reservedLogRpt" style="height: 380px;width:1148px;"></div></td>
					</tr>
					<tr>
						<td colspan="6" align="center" height="26">
							<div class='exportlinks'><fmtReservedLog:message key="LBL_EXP_ONE"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"> <fmtReservedLog:message key="LBL_EXCEL"/> </a></div>
						</td>                 
					</tr>
					
				</table> 
			</td>
		</tr>				
		<%}else{%>
		<tr>
			<td colspan="6" height="26" align="center" class="RightText"><fmtReservedLog:message key="LBL_NO_DATA"/></td>
		</tr>
		<%}%>
	</table>
	
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
