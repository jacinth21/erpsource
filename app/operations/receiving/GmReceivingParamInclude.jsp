<%
/**********************************************************************************
 * File		 		: GmReceivingParamInclude.jsp
 * Desc		 		: This screen is used to Map DHR Bulk Parameters.
 * author			: Maikandan Rajasekaran
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmtReceivingParamInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReceivingParamInclude:setLocale value="<%=strLocale%>"/>
<fmtReceivingParamInclude:setBundle basename="properties.labels.operations.receiving.GmReceivingParamInclude"/>
<!-- operations\receiving\GmReceivingParamInclude.jsp -->
<bean:define id="xmlStringData" name="frmReceivingParam" property="xmlStringData" type="java.lang.String"> </bean:define>
<bean:define id="labelprintcheck" name="frmReceivingParam" property="labelprintfl" type="java.lang.String"> </bean:define>
<bean:define id="form42check" name="frmReceivingParam" property="form42fl" type="java.lang.String"> </bean:define>
<bean:define id="qcverifiedcheck" name="frmReceivingParam" property="qcverifiedfl" type="java.lang.String"> </bean:define>
<bean:define id="strBtnDisabled" name="frmReceivingParam" property="strBtnDisabled" type="java.lang.String"> </bean:define>
<bean:define id="preStaging" name="frmReceivingParam" property="preStagingfl" type="java.lang.String"> </bean:define>
<bean:define id="staging" name="frmReceivingParam" property="stagingfl" type="java.lang.String"> </bean:define>
<bean:define id="strDHRStatusId" name="frmReceivingParam" property="strDHRStatusId" type="java.lang.String"> </bean:define>
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>
<% 
String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
%>

<HTML>
<HEAD>
<TITLE><fmtReceivingParamInclude:message key="LBL_REC_PARAM"/>  </TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>	
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmReceivingParamInc.js"></script> 
<script>
var objGridData = '';
var gridObj;
objGridData = '<%=xmlStringData%>';
var qcverifiedchk = '<%=qcverifiedcheck%>' ;
var labelprintchk = '<%=labelprintcheck%>' ;
var form42chk = '<%=form42check%>' ;
var preStagingchk = '<%=preStaging%>' ;
var stagingchk = '<%=staging%>' ;
var dhrstatus = <%=strDHRStatusId%>;
</script>
</HEAD>

<BODY  onload="javascript:fnLoad();" >

<html:form  action="/GmReceivingParamInc.do?">
<input type="hidden" name="strOpt"/>
<input type="hidden" name="inputstr"/>
<input type="hidden" name="attrtype"/>
<input type="hidden" name="labelprintfl"/>
<input type="hidden" name="form42fl"/>
<input type="hidden" name="qcverifiedfl"/>
<input type="hidden" name="preStagingfl"/>
<input type="hidden" name="stagingfl"/>
<html:hidden property="rsid" name="frmReceivingParam" />




<table border="0" class="DtTable1080" cellspacing="0" cellpadding="0">
		<%if(xmlStringData.indexOf("cell") != -1){%>
		<tr>
			<td>
				
							<div id="RECEIVEATTRIBUTEDATA" style="height: 170px;width: 1070px"></div>
							
					
			</td>
		</tr>				
		<%}else{%>
		<tr>
			<td colspan="8" height="20" align="center" class="RightText"><fmtReceivingParamInclude:message key="LBL_NO_DATA"/></td>
		</tr>
		<%}%>
		<tr><td height="12"></td></tr>
		<tr>
			<td align="center" height="25" >
				<fmtReceivingParamInclude:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button name="Btn_Submit" value="${varSubmit}" buttonType="Save" controlId="Submit" gmClass="Button" disabled="<%=strBtnDisabled%>" style="width: 6em" onClick="fnSaveDHRParamter(this.form);" />
			</td>
		</tr>
    </table>		     	



</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

