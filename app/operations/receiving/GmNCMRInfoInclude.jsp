<%
/**********************************************************************************
 * File		 		: GmNCMRInfoInclude.jsp
 * Desc		 		:  Receive Shipment - NCMR Tab
 * Version	 		: 1.0
 * author			: Anilkumar
************************************************************************************/
%>
<%@ page language="java" %>

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtNCMRInfoInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtNCMRInfoInclude:setLocale value="<%=strLocale%>"/>
<fmtNCMRInfoInclude:setBundle basename="properties.labels.operations.receiving.GmNCMRInfoInclude"/>
<!-- operations\receiving\GmNCMRInfoInclude.jsp -->
<bean:define id="strXmlGridData" name="frmOperDashBoardDispatch" property="strGridXmlData" type="java.lang.String"> </bean:define>
<%

String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
%>
<HTML>
<HEAD>
<TITLE><fmtNCMRInfoInclude:message key="LBL_RECV_SHIP"/></TITLE>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmNCMRInfoInclude.js"></script>
<script>
var objGridData = '';
objGridData = '<%=strXmlGridData%>';
</script>
</HEAD>

<BODY  onload="fnOnload()">
<FORM NAME="frmOperDashBoardDispatch" action="/gmOperDashBoardDispatch.do?method=NCMRDashForShipment">
<input type="hidden" name="hNCMRId"/>
<input type="hidden" name="hMode"/>
<input type="hidden" name="hAction"/>

	<table border="0" width="100%"  cellspacing="0" cellpadding="0">
		<%if(strXmlGridData.indexOf("cell") != -1){%>
		<tr>
			<td>
			<div id="recshipmntRpt" style="height:150px;width:1070px;"></div>
			</td>
		</tr>
		<%}else{%>
		<tr>
			<td align="center" class="RightText"><fmtNCMRInfoInclude:message key="LBL_NO_DATA"/></td>
		</tr>
		<%}%>
    </table>		     	

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
