<%
/**********************************************************************************
 * File		 		: GmScanDonorInfoInclude.jsp
 * Desc		 		: This JSP is for the section "Enter Lot Information"
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtScanDonorInfoInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtScanDonorInfoInclude:setLocale value="<%=strLocale%>"/>
<fmtScanDonorInfoInclude:setBundle basename="properties.labels.operations.receiving.GmScanDonorInfoInclude"/>
<!--operations\receiving\GmScanDonorInfoInclude.jsp  -->

<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>

<HTML>
<HEAD>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<TITLE><fmtScanDonorInfoInclude:message key="LBL_LOT_INFO"/>  </TITLE>

<script>
</script>

<BODY leftmargin="20" topmargin="10">
<html:hidden property="hMfgDate" name="frmPOBulkReceive"/>
<html:hidden property="hProdType" name="frmPOBulkReceive"/>
<html:hidden property="hsize" name="frmPOBulkReceive"/>
<html:hidden property="hPartNum" name="frmPOBulkReceive"/>
<html:hidden property="hInputString" name="frmPOBulkReceive"/>
<html:hidden property="hLotString" name="frmPOBulkReceive"/>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		
		<tr>
			<td height="24" class="ShadeRightTableCaption" colspan="8">&nbsp;<b><fmtScanDonorInfoInclude:message key="LBL_ENTER_LOT_INFO"/></b></td>
		</tr>
		<tr><td class="Line" colspan="8"></td></tr>
		
		<tr class="Shade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="10%"><fmtScanDonorInfoInclude:message key="LBL_PART" var="varPart"/><gmjsp:label type="MandatoryText" SFLblControlName="${varPart} #:" td="false" /></td>
			<td width="10%">&nbsp;<html:text property="partNum" styleClass="InputArea" name="<%=strFormName %>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnFetchPartdtl(this)" tabindex="7" size="15"/></td>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="9%"><fmtScanDonorInfoInclude:message key="LBL_DESC" var="varDesc"/><gmjsp:label type="RegularText" SFLblControlName="${varDesc}:" td="false" />&nbsp; </td>
			<td id="pdesclabel" class="RegularText" width="25%"> <bean:write name="<%=strFormName %>" property="partDesc"/> </td>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="8%"><fmtScanDonorInfoInclude:message key="LBL_SIZE" var="varSize"/><gmjsp:label type="RegularText" SFLblControlName="${varSize}:" td="false" />&nbsp;</td>
			<td id="psizelabel" class="RegularText" width="10%"> &nbsp;<bean:write name="<%=strFormName %>" property="size"/></td>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="10%"><fmtScanDonorInfoInclude:message key="LBL_PRO_TYP" var="varProTyp"/><gmjsp:label type="RegularText" SFLblControlName="${varProTyp}:" td="false" />&nbsp;</td>
			<td id="pprodtypelabel" class="RegularText" width="10%"><bean:write name="<%=strFormName %>" property="prodType"/></td>
		</tr>
		
		<tr><td class="LLine" colspan="8"></td></tr> 	
		<tr>
			<td class="RightTableCaption" align="right" HEIGHT="25"><fmtScanDonorInfoInclude:message key="LBL_LOT_CODE" var="varLotCode"/><gmjsp:label type="MandatoryText" SFLblControlName="${varLotCode}:" td="false" /></td>
			<td>&nbsp;<html:text property="lotCode" styleClass="InputArea" name="<%=strFormName %>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');fnFocus('tabout'); fnvalidateLot(this.value);" onkeypress="javascript:fnFocus(this);" tabindex="8" size="15"/></td>
			<td class="RightTableCaption" align="right" HEIGHT="25"><fmtScanDonorInfoInclude:message key="LBL_MFG_DT" var="varMfgDt"/><gmjsp:label type="MandatoryText" SFLblControlName="${varMfgDt}:" td="false" /></td>
			<td>&nbsp;<input type="text" size="10" name="mfgDate" id="mfgDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');fnCalculateExpDate(this);" tabindex=9>
				<img id="Img_Date" style="cursor:hand" onclick="javascript:showSglCalendar('dcalendardiv','mfgDate');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
				<div id="dcalendardiv" style="position: absolute; z-index: 10;"></div>
			</td>
			<td class="RightTableCaption" align="right" HEIGHT="25"><fmtScanDonorInfoInclude:message key="LBL_EXP_DT" var="varExpDt"/><gmjsp:label type="MandatoryText" SFLblControlName="${varExpDt}:" td="false" /></td>
			<td colspan="3">&nbsp;<gmjsp:calendar SFFormName="<%=strFormName %>" controlName="expDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="10"/></td>
		</tr>
		
		<tr><td class="LLine" colspan="8"></td></tr> 	
		<tr class="Shade">
			<td class="RightTableCaption" align="right" HEIGHT="25" colspan="1"><fmtScanDonorInfoInclude:message key="LBL_CUST_SIZE" var="varCustSize"/><gmjsp:label type="RegularText" SFLblControlName="${varCustSize}:" td="false" /></td>
			<td>&nbsp;<html:text property="customSize" styleClass="InputArea" name="<%=strFormName %>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onkeypress="javascript:fnAdd(this);" tabindex="11" size="15"/></td>
			<td colspan="1">&nbsp;<gmjsp:button name="Btn_Add" value="Add" gmClass="button" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" onClick="javascript:fnAdd(this);" tabindex="12" buttonType="Load" />&nbsp;</td>
			<td colspan="5"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><gmjsp:button name="Btn_Delete" value="Delete" gmClass="button" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" onClick="javascript:removeSelectedRow(this);" tabindex="13" buttonType="Save" /></td>
				    <td colspan="5" valign="middle" ><div id="errMsg" style="text-align: left;"></div></td>
				</tr>
			</table>
			</td>			
		</tr>
		
	</table>

<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>