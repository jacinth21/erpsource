<%
/**********************************************************************************
 * File		 		: GmPODetailInclude.jsp
 * Desc		 		: This JSP is for the section PO Details
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@page import="java.util.Date"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib prefix="fmtPODetailInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page isELIgnored="false" %>

<fmtPODetailInclude:setLocale value="<%=strLocale%>"/>
<fmtPODetailInclude:setBundle basename="properties.labels.operations.receiving.GmPODetailInclude"/>
<!--operations\receiving\GmPODetailInclude.jsp  -->
<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<bean:define id="gridData" name="<%=strFormName %>" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="poDate" name="<%=strFormName %>" property="poDate" type="java.util.Date"> </bean:define>
<% 
String strApplDateFmt = GmCommonClass.parseNull((String)strGCompDateFmt);
String strTimeZOne = strGCompTimeZone;
String strPoDate = GmCommonClass.getStringFromDate(poDate, strApplDateFmt);

//The following code added for passing the company info to the child js fnFilterLoad function
String strCompanyId = gmDataStoreVO.getCmpid();
String strPlantId = gmDataStoreVO.getPlantid();
String strPartyId = gmDataStoreVO.getPartyid();
 
String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
strCompanyInfo = URLEncoder.encode(strCompanyInfo);

%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE><fmtPODetailInclude:message key="LBL_PO_DET"/> </TITLE>

<script>
var compInfObj = '<%=strCompanyInfo%>';
</script>

<BODY leftmargin="20" topmargin="10">

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		
		<tr class="Shade">
			<td class="RightTableCaption" align="right" HEIGHT="25" width="20%"><fmtPODetailInclude:message key="LBL_VEN_NAM" var="varVenNam"/><gmjsp:label type="RegularText" SFLblControlName="${varVenNam}:" td="false" /></td>
			<td width="30%">&nbsp;<bean:write name="<%=strFormName %>" property="vname"/></td>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="20%"><fmtPODetailInclude:message key="LBL_PO_DET" var="varPoDet"/><gmjsp:label type="RegularText" SFLblControlName="${varPoDet}:" td="false" /></td>
			<td width="30%">&nbsp;<%=strPoDate%></td>
		</tr>
		<tr><td class="LLine" colspan="5"></td></tr> 	
		<tr>
			<td class="RightTableCaption"  align="right" HEIGHT="25"><fmtPODetailInclude:message key="LBL_PO_TYP" var="varPoTyp"/><gmjsp:label type="RegularText" SFLblControlName="${varPoTyp}:" td="false" /></td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="poTypeNm"/></td>
			<td class="RightTableCaption"  align="right" HEIGHT="25"><fmtPODetailInclude:message key="LBL_RAISED_BY" var="varRaisedBy"/><gmjsp:label type="RegularText" SFLblControlName="${varRaisedBy}:" td="false" /></td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="cname"/></td>
		</tr>
		<tr><td class="LLine" colspan="5"></td></tr> 	
		<tr class="Shade">
			<td class="RightTableCaption"  align="right" HEIGHT="25"><fmtPODetailInclude:message key="LBL_DATE_RECV" var="varDateRecv"/><gmjsp:label type="MandatoryText" SFLblControlName="${varDateRecv}:" td="false" /></td>
			<td>&nbsp;<gmjsp:calendar SFFormName="<%=strFormName %>" controlName="receivedDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/></td>
			<td class="RightTableCaption"  align="right" HEIGHT="25"><fmtPODetailInclude:message key="LBL_PACK_SLIP_ID" var="varPackSlipId"/><gmjsp:label type="MandatoryText" SFLblControlName="${varPackSlipId}:" td="false" /></td>
			<td>&nbsp;<html:text property="packSlipId" styleClass="InputArea" name="<%=strFormName %>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" size="15"/></td>
		</tr>
		<tr><td class="LLine" colspan="5"></td></tr> 	
		<tr>
			<td class="RightTableCaption"  align="right" HEIGHT="25"><fmtPODetailInclude:message key="LBL_DONOR" var="varDonor"/><gmjsp:label type="MandatoryText" SFLblControlName="${varDonor} #:" td="false" /></td>
			<td colspan="3">
			<table  width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td width="20%">
			&nbsp;<html:text property="donorNum" styleClass="InputArea" name="<%=strFormName %>" onfocus="changeBgColor(this,'#AACCE8');fnClearDiv();" onblur="changeBgColor(this,'#ffffff');fnCheckDonorNum(this);" tabindex="4" size="15"/>
			<span id="DivShowDonorIDExists" style="vertical-align:middle; display: none;"> <fmtPODetailInclude:message key="LBL_DONOR_ALREADY_EXIST" var="varDonorAlreadyExist"/><img height="23" width="25" title="${varDonorAlreadyExist}" src="<%=strImagePath%>/success.gif"></img></span>
			<span id="DivShowDonorIDNotExists" style=" vertical-align:middle; display: none;" tabindex="5"><fmtPODetailInclude:message key="LBL_DONOR_DOES_NOT_EXIST" var="varDonorDoesNotExist"/><img title="${varDonorDoesNotExist}" src="<%=strImagePath%>/delete.gif"></img></span></td>
			<td tabindex="6"><img align="middle" id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/d.gif' title='Donor' onclick="javascript:fnCallDonor()" width='14' height='14' tabindex="6"/></td>
			<td class="RightTableCaption" align="right" width="11%" HEIGHT="25"><fmtPODetailInclude:message key="LBL_STAGE_DATE" var="varDonor"/><gmjsp:label type="MandatoryText" SFLblControlName="${varDonor}:"/></td> 
			<td width="38%">&nbsp;<gmjsp:calendar SFFormName="<%=strFormName %>" controlName="stgDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="4"/></td>
			</tr></table></td>
		</tr>
	</table>

<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>