<%
/*******************************************************************************
 * File		 		: GmDonorInfoProcessContainer.jsp
 * Desc		 		: This screen is used for the  receive shipment
 ******************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib prefix="fmtDonorInfoProcessContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtDonorInfoProcessContainer:setLocale value="<%=strLocale%>"/>
<fmtDonorInfoProcessContainer:setBundle basename="properties.labels.operations.receiving.GmDonorInfoProcessContainer"/>
<!-- operations\receiving\GmDonorInfoProcessContainer.jsp -->
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<bean:define id="gridData" name="frmPOBulkReceive" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="strApprRej" name="frmPOBulkReceive" property="strApprRej" type="java.lang.String"> </bean:define>
<bean:define id="alLocations" name="frmPOBulkReceive" property="alLocations" type="java.util.ArrayList" ></bean:define>
<bean:define id="strMsg" name="frmPOBulkReceive" property="strMsg" type="java.lang.String"> </bean:define>
<bean:define id="accessfl" name="frmPOBulkReceive" property="accessfl" type="java.lang.String"> </bean:define>
<bean:define id="strRejfl" name="frmPOBulkReceive" property="strRejectFl" type="java.lang.String"> </bean:define>
<bean:define id="strSelectedTab" name="frmPOBulkReceive" property="selectedTab" type="java.lang.String"> </bean:define>
<bean:define id="rsnumber" name="frmPOBulkReceive" property="rsNumber" type="java.lang.String" ></bean:define>
<bean:define id="strAllChecked" name="frmPOBulkReceive" property="strAllChecked" type="java.lang.String" ></bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("RECEIVE_BULK_SHIPMENT");
	String strApplDateFmt = GmCommonClass.parseNull((String)strGCompDateFmt);
	String struCurrentdate = GmCommonClass.parseNull((String) GmCalenderOperations.getCurrentDate(strApplDateFmt));
	
	HashMap hTxnType = null;
	
	
	String strProcessTab="selected";
	String strParameterTab="";
		
	if(strSelectedTab.equals("parameter")){
		 strProcessTab="";
		 strParameterTab="selected";
	}
	
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
		
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO Bulk Receive </TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">

<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxToolbar/dhtmlxtoolbar.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Operations/receiving/GmRSBulkReceive.js"></script>	

<script>
var companyInfoObj = '<%=strCompanyInfo%>';
var format = '<%=strApplDateFmt%>';
var objGridData = '<%=gridData%>';
var varApprRejFl = '<%=strApprRej%>';
var locationLen = <%= alLocations.size()%>;
var accessFl = '<%=accessfl%>';
var strRejectFl = '<%=strRejfl%>';
var rsnumber = '<%=rsnumber%>';
var currdate = '<%=struCurrentdate%>';
var allParameterCkd = '<%=strAllChecked%>';
//For setting the drop down value of transaction type
<%	
hTxnType = new HashMap();	
for (int i=0;i< alLocations.size();i++)
{
	hTxnType = (HashMap) alLocations.get(i); 
%>
var TypeArr<%=i%> ="<%=GmCommonClass.parseNull((String)hTxnType.get("CODEID"))%>,<%=GmCommonClass.parseNull((String)hTxnType.get("CODENM"))%>";
<%
}
%>

function fnHtmlEditor(){
	tinyMCE.init({
		mode : "specific_textareas",
	    editor_selector : "HtmlTextArea",
		theme : "advanced",
		theme_advanced_path : false,
		height : "300",
		width: "500" 
	});
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"" onload="fnAjaxTabLoad();">
<html:form action="/gmPOBulkReceive.do?method=loadBatchshipment">
<html:hidden property="strOpt" name="frmPOBulkReceive" styleId="strOpt"/>
<html:hidden property="poNumber" name="frmPOBulkReceive"/>
<html:hidden property="hInputString" name="frmPOBulkReceive" styleId="hInputString"/>
<html:hidden property="happrString" name="frmPOBulkReceive" styleId="happrString"/>
<html:hidden property="hrejString" name="frmPOBulkReceive" styleId="hrejString"/>
<html:hidden property="hFGString" name="frmPOBulkReceive" styleId="hFGString"/>
<html:hidden property="hPNString" name="frmPOBulkReceive" styleId="hPNString"/>
<html:hidden property="hQNString" name="frmPOBulkReceive" styleId="hQNString"/>
<html:hidden property="hInvSplitCnt" name="frmPOBulkReceive" styleId="hInvSplitCnt"/>
<html:hidden property="haction" name="frmPOBulkReceive" styleId="haction"/>
<input type="hidden" name="hcurrentTab">
<html:hidden property="statusId" name="frmPOBulkReceive" styleId="statusId"/>
<html:hidden property="hdhrProcessAction" name="frmPOBulkReceive" styleId="hdhrProcessAction"/>

	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0" >
		<tr>
			<td width="1280" height="25" colspan="4" class="RightDashBoardHeader">&nbsp;<fmtDonorInfoProcessContainer:message key="LBL_RECV_SHIP"/></td>
			<td   colspan="3" class="RightDashBoardHeader">
			<fmtDonorInfoProcessContainer:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='17' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
					    </td>
		</tr>
		
		<tr>
			<td class="RightTableCaption" HEIGHT="30" align="right" width="20%">
			<fmtDonorInfoProcessContainer:message key="LBL_BATCH_SHIPMENT_ID" var = "varBatchShipmetnId"/>
				<gmjsp:label type="RegularText" SFLblControlName="${varBatchShipmetnId}" td="false" />&nbsp;</td>
			<td width="20%"><html:text property="rsNumber" styleId="rsNumber" styleClass="InputArea" name="frmPOBulkReceive" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onkeypress="javascript:fnLoad(this)" tabindex="1" size="15"/>
			</td>
			<td width="75%"><fmtDonorInfoProcessContainer:message key="BTN_LOAD" var = "varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" onClick="javascript:fnLoad(this);" tabindex="2" buttonType="Load" /></td>
		</tr>
		<logic:notEqual name="frmPOBulkReceive" property="strOpt" value="">
			<tr><td class="LLine" colspan="5"></td></tr>
			<tr>
				<!-- This JSP is for the section "Donor Details" -->
					<td colspan="5">
						<jsp:include page="/operations/receiving/GmLoadShipmentInfoInclude.jsp" >
						<jsp:param name="FORMNAME" value="frmPOBulkReceive" />
						</jsp:include>
					</td>
			</tr>
			<!-- Included section to scan the lot number for verification -->
			<tr><td class="LLine" colspan="5"></td></tr>
			<logic:equal name="frmPOBulkReceive" property="statusId" value="3">
				<logic:equal name="frmPOBulkReceive" property="accessfl" value="Y">
					<tr>
							<td colspan="5">
								<jsp:include page="/operations/receiving/GmScanLotCodeInclude.jsp" >
								<jsp:param name="FORMNAME" value="frmPOBulkReceive" />
								</jsp:include>
							</td>
					</tr>
					<tr><td class="LLine" colspan="5"></td></tr>
				</logic:equal>
			</logic:equal>
			
			<logic:equal name="frmPOBulkReceive" property="statusId" value="1">
				<logic:equal name="frmPOBulkReceive" property="showChkFl" value="Y">
					<tr id="chkboxtr" style="display: none;">
							<td colspan="5" align="center" class="RightTableCaption" height="25px">
								<input type="checkbox" size="30" name="chkSplitRS" id="chkSplitRS" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >
								<fmtDonorInfoProcessContainer:message key="LBL_SPLIT_CHK"/>
							</td>
					</tr>
					<tr><td class="LLine" colspan="5"></td></tr>
				</logic:equal>
			</logic:equal>
			
			<!-- For the tab -->
			<tr>
			<td colspan="5">
				<ul id="maintab" class="shadetabs">
					<li onclick="fnUpdateTabIndex(this);"><a href="/gmPOBulkReceive.do?method=loadBatchshipment&strOpt=loadRS&rsNumber=<bean:write name="frmPOBulkReceive" property="rsNumber"/>&strMsg=<bean:write name="frmPOBulkReceive" property="strMsg"/>" 
					title="Process" rel="ajaxdivcontentarea" name="Process" class="<%=strProcessTab%>"><fmtDonorInfoProcessContainer:message key="LBL_PROCESS"/></a></li>
					<logic:notEqual name="frmPOBulkReceive" property="rsNumber" value="">
		 				
							<li onclick = "fnUpdateTabIndex(this);"><a href="/GmReceivingParamInc.do?&strOpt=load&rsid=<bean:write name="frmPOBulkReceive" property="rsNumber"/>"
								rel="#iframe" id="parameter" title="Parameters" ><fmtDonorInfoProcessContainer:message key="LBL_PARAMETERS"/></a>
							</li>							
							<li onclick = "fnUpdateTabIndex(this);"><a href="/gmOperDashBoardDispatch.do?method=NCMRDashForShipment&hAction=NCMR&hId=<bean:write name="frmPOBulkReceive" property="rsNumber"/>"
									rel="#iframe" id="ncmr" title="NCMR"><fmtDonorInfoProcessContainer:message key="LBL_NCMR"/></a>
							</li>
							<li onclick = "fnUpdateTabIndex(this);"><a href="/gmOperDashBoardDispatch.do?method=DHRDashForShipment&hAction=DHR&hId=<bean:write name="frmPOBulkReceive" property="rsNumber"/>"
									rel="#iframe" id="dhr" title="DHR"><fmtDonorInfoProcessContainer:message key="LBL_DHR"/></a>
							</li>
					<!--		<li><a href="/gmPOBulkReceive.do?method=loadLog"
									rel="#iframe" id="log" title="Log">Log</a>
							</li>
					-->	
					</logic:notEqual>
				</ul>
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<div id="ajaxdivcontentarea" class="contentstyle" style="display:visible;height:300px;width: 1070px;" >
				</div>
			</td>
		</tr>
		</logic:notEqual>	
	</table>
	</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
