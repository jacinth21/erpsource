<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Vector" %>
<%@ taglib prefix="fmtDonorInfoPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtDonorInfoPrint:setLocale value="<%=strLocale%>"/>
<fmtDonorInfoPrint:setBundle basename="properties.labels.operations.receiving.GmDonorInfoPrint"/>
<!--operations\receiving\GmDonorInfoPrint.jsp  -->
<%
	HashMap hmHeader = new HashMap();
	String strHtmlJasperRpt = "";
	ArrayList alRSHeader 		= new ArrayList();
	ArrayList alRSDtls 			= new ArrayList();
	String strReportName = "";
	strReportName = "/GmDonorInfoProcess.jasper";
	
	alRSHeader = (ArrayList)request.getAttribute("ALRSHEADER");
	hmHeader=(HashMap)alRSHeader.get(0);
	alRSDtls = (ArrayList)request.getAttribute("ALRSDTLS");
	
	int intRsdtlsSize = alRSDtls.size();
	hmHeader.put("RSDTLSSIZE",alRSDtls.size());
	
%>
<script>
function fnPrint()
{
	window.print();
	return false;
}
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
	strObject1 = eval("document.all.button1");
	tdimginnner = strObject1.innerHTML;
	strObject1.innerHTML = "";	
}
function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
	strObject1 = eval("document.all.button1");
	strObject1.innerHTML = tdimginnner ;
}
</script>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<table>
		<tr>
			<td align="right" height="30" id="button1">
			<fmtDonorInfoPrint:message key="LBL_PRINT_PAGE" var="varPrintPage"/>
			<fmtDonorInfoPrint:message key="LBL_CLOSE_WINDOW" var="varCloseWindow"/>
				<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="${varPrintPage}" onClick="return fnPrint();" />
				<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="${varCloseWindow}" onClick="window.close();" />
			</td>
		</tr>
		
		
	 	<tr>	
			<td>
				<%
				
				GmJasperReport gmJasperReport = new GmJasperReport();			
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strReportName);
				gmJasperReport.setHmReportParameters(hmHeader);
				gmJasperReport.setReportDataList(alRSDtls);
				
				gmJasperReport.setBlDisplayImage(true);
				strHtmlJasperRpt = gmJasperReport.getHtmlReport();				
				%>	
				<%=strHtmlJasperRpt%>
			</td>
		</tr>
		
		<tr>
		<td align="center" colspan="6" height="30" id="button">
			<fmtDonorInfoPrint:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="${varPrint}" name="Btn_Print" style="width: 4em; height: 23;"  gmClass="button" buttonType="Load" onClick="fnPrint();" />&nbsp;&nbsp;&nbsp;&nbsp;
			<fmtDonorInfoPrint:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="${varClose}" name="Btn_Close" style="width: 4em; height: 23;"  gmClass="button" buttonType="Load" onClick="window.close();" />
			</td>
		</tr>
	
	  
</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>