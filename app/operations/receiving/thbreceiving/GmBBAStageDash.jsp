<%
/**********************************************************************************
 * File		 		: GmBBAStageDash.jsp
 * Desc		 		: Staging DashBoard
 * Version	 		: 1.0
 * author			: Angeline
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib uri="com.corda.taglib" prefix="ctl" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- operations\receiving\GmBBAStageDash.jsp -->
<%@ taglib prefix="fmtBBAStageDash" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtBBAStageDash:setLocale value="<%=strLocale%>"/>
<fmtBBAStageDash:setBundle basename="properties.labels.operations.receiving.thbreceiving.GmBBAStageDash"/>
<bean:define id="strXmlGridData" name="frmBBAStagingDashBoard" property="strXmlGridData" type="java.lang.String"></bean:define>
<%
	String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
	String strWikiTitle = GmCommonClass.getWikiTitle("STAGE_DASHBOARD");
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<HEAD>
<TITLE>Stage Dashboard</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css"	href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmBBAStagingDash.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmReceiveShipDash.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>
<!--  Added new Js Script to support calender edit -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script>
var objGridData = '';
objGridData = '<%=strXmlGridData%>';
var compInfObj = '<%=strCompanyInfo%>'; 
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnPageOnload()">
<html:form action="/gmBBAStageDash.do?method=fetchStagingDashboard">
<input type="hidden" name="hAction"/>
<input type="hidden" name="hId"/>
<input type="hidden" name="hFrom"/>
<input type="hidden" name="hConsignId"/>
<input type="hidden" name="hMode"/>
<input type="hidden" name="hType"/>
<input type="hidden" name="hOpt"/>
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="26" class="RightDashBoardHeader" colspan="4">&nbsp;<b><fmtBBAStageDash:message key="LBL_STAGE_DASH_BOARD"/></b></td>
			<td align="right" class=RightDashBoardHeader colspan="2">
			<fmtBBAStageDash:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
			</td>
		</tr>
		<tr>
			<td colspan="6">
			<div id="BBAStageDashBoardRpt" style="height: 550px; width: 996px;" ></div>
			</td>
	    </tr>	
	    <tr>
	 		<td align="center" colspan="5">
			<div id="divNoExcel" class='exportlinks'><fmtBBAStageDash:message key="LBL_EXPORT"/>:<a href="#" onclick="fnExport();"><img src='img/ico_file_excel.png' />&nbsp; <fmtBBAStageDash:message key="LBL_EXCEL"/></a></div>   
			</td>
	    </tr> 
		<tr>
			<td colspan="5" align="center"><div id="DivNoMsg"><fmtBBAStageDash:message key="LBL_NO_DATA_AVAILABLE" />
			</div></td>
	    </tr>     
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
