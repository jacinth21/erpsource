
<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ page isELIgnored="false"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTHBLoadPendDash" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/common/GmHeader.inc"%>

 <bean:define id="strJSONGridData" name="frmTHBLoadPendDash" property="strJSONGridData" type="java.lang.String"></bean:define> 
 <bean:define id="strLockGenerateAccess" name="frmTHBLoadPendDash" property="strLockGenerateAccess" type="java.lang.String"></bean:define> 
<fmtTHBLoadPendDash:setBundle basename="properties.labels.operations.receiving.thbreceiving.GmTHBPendAnalysisDash"/>
<!-- operations\receiving\thbreceiving\GmTHBPendAnalysisDash.jsp  -->
 <%
String strWikiTitle = GmCommonClass.getWikiTitle("THB_LOAD_PEND_ANA_DASH");
String strOpRecvJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
strLockGenerateAccess = (strLockGenerateAccess.equals("") ? "true" : "false");
%> 
<HTML>
<HEAD>
<TITLE> Globus Medical: THB Receiving DashBoard</TITLE>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script> 
<script language="JavaScript" src="<%=strOpRecvJsPath%>/thbreceiving/GmTHBPendAnalysisDash.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

 <script>
var objGridData = '';
objGridData = '<%=strJSONGridData%>';
</script> 


</HEAD>
 <BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad()"> 
<html:form action="/gmTHBLoadPendDash.do?method=loadTHBPendAnalysisDashBoard">
 	<table class="DtTable600" cellspacing="0" cellpadding="0">
		<tr>
		<td colspan="5">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
				<td height="25" class="RightDashBoardHeader">&nbsp;<fmtTHBLoadPendDash:message key="LBL_ANA_DASHBOARD_HDR"/></td>
				<td  height="25" class="RightDashBoardHeader" align="right" colspan="4">
					<fmtTHBLoadPendDash:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			      		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			      </td>
		      	</tr>
		      </table>
	      	</td>
	      	</tr>
 			 <tr>
			<td align="left" colspan="5">
				<div id="thbdashRpt" class="grid" style="height: 500px;width: 100%"></div>
			</td>
			</tr> 
			
			 <tr height="30" id="thblockbtn">
			<td align="center" colspan="5" >
			  <div id="thblockbtn" style="width: 100%"><gmjsp:button style="width: 12em" value=" Lock and Generate " gmClass="button" onClick="fnLockGenerateData();" disabled="<%=strLockGenerateAccess %>" buttonType="Save" name="Btn_Lock"/></div> 
				<%-- <div id="thblockbtn" style="width: 100%"><gmjsp:button style="width: 12em" value=" Lock and Generate " gmClass="button" onClick="fnLockGenerateData();" disabled="true" buttonType="Save" name="Btn_Lock"/></div> --%>
			</td></tr>
			<tr><td class="LLine" colspan="5" height="1"></td></tr>
			<tr>
				<td align="center" colspan="5">
			<div id="divNoExcel" class='exportlinks'><fmtTHBLoadPendDash:message key="LBL_EXPORT"/>:<a href="#" onclick="fnDownloadXLS();"><img src='img/ico_file_excel.png' />&nbsp; <fmtTHBLoadPendDash:message key="LBL_EXCEL"/></a></div>   
			 	</td>
			</tr> 
			<tr>
				<td colspan="5" align="center"><div id="DivNoMsg"><fmtTHBLoadPendDash:message key="LBL_NO_DATA_AVAILABLE" />
					</div></td>
			</tr>              
	</table>
</html:form>  
<%@ include file="/common/GmFooter.inc"%>
</BODY> 
</HTML>
