<!-- operations\receiving\thbreceiving\GmTHBAnalysisDetails.jsp -->

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTHBAnalysisDtls"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<fmtTHBAnalysisDtls:setLocale value="<%=strLocale%>" />
<fmtTHBAnalysisDtls:setBundle
	basename="properties.labels.operations.receiving.thbreceiving.GmTHBAnalysisDetails" />
<bean:define id="analysisId" name="frmTHBAnalysisDetails"
	property="analysisId" type="java.lang.String"></bean:define>
<bean:define id="strJSONGridData" name="frmTHBAnalysisDetails"
	property="strJSONGridData" type="java.lang.String"></bean:define>
<bean:define id="pendingQty" name="frmTHBAnalysisDetails" property="pendingQty" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmTHBAnalysisDetails" property="status" type="java.lang.String"></bean:define>	
<bean:define id="completeBtnAccess" name="frmTHBAnalysisDetails" property="completeBtnAccess" type="java.lang.String"></bean:define>	
<%
	String strWikiTitle = GmCommonClass
			.getWikiTitle("THB_ANALYSIS_DETAILS_REPORT");
	String strOpRecvJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_OPERATIONS_RECEIVING");
	completeBtnAccess = (!completeBtnAccess.equals("Y") ? "true" : "false");
%>
<html>
<head>
<title>Globus Medical : THB Analysis Details Report</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript"
	src="<%=strOpRecvJsPath%>/thbreceiving/GmTHBAnalysisDetails.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>



<script type="text/javascript">
var objGridJSONData ='';
objGridJSONData='<%=strJSONGridData%>';
var pendingQty = '<%=pendingQty%>';
var status = '<%=status%>';
var completeBtnAccess ='<%=completeBtnAccess%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form
		action="/gmTHBAnalysisDetails.do?method=fetchTHBAnalysisDetails">
		<html:hidden property="hBasePartStr" name="frmTHBAnalysisDetails" />
		<html:hidden property="hPrivatePartStr" name="frmTHBAnalysisDetails" />
		<html:hidden property="hColumnFilterStr" name="frmTHBAnalysisDetails" />
		<html:hidden property="analysisId" name="frmTHBAnalysisDetails" />
		<table class="DtTable1300" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="26" class="RightDashBoardHeader" colspan="7">&nbsp;<fmtTHBAnalysisDtls:message
						key="LBL_THB_ANALYSIS_REPORT" /></td>
				<td align="right" class=RightDashBoardHeader colspan="3"><fmtTHBAnalysisDtls:message
						key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr height="30">
				<td class="RightTableCaption" HEIGHT="30" align="right"
					style="width: 100px;"><fmtTHBAnalysisDtls:message
						key="LBL_ANALYSIS_ID" />:&nbsp;</td>
				<td><bean:write name="frmTHBAnalysisDetails"
						property="analysisId" /></td>

			</tr>
			<tr>
				<td colspan="10" class="LLine" height="1"></td>
			</tr>
			<tr>
				<td align="left" colspan="10">
					<div id="dataGridDiv" class="grid" style="width: 100%"></div> 
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="10" height="1"></td>
			</tr>
			<%if(pendingQty.equals("0")){ %>
			<tr id="completeBtnRow">
			 <td  colspan="10" align="center" >
			<fmtTHBAnalysisDtls:message key="BTN_COMPLETE_ANALAYSIS" var="varLoad" /> 
			    <gmjsp:button name="btn_Complete_Analysis" value="${varLoad}" gmClass="button" onClick="javascript:fnCompleteAnalysis();" 
			    buttonType="Save" disabled="<%=completeBtnAccess%>"/>
				<span class="RightTextGreen" id="msgDiv" style="display: inline-block;"><fmtTHBAnalysisDtls:message key="LBL_ANALYSIS_COMPLETED"/></span>
			</td>
			</tr>
			<tr>
				<td class="LLine" colspan="10" height="1"></td>
			</tr>
			<%} %>
			
			<tr>
				<td align="center" colspan="10">
					<div class='exportlinks' id="DivExportExcel">
						<fmtTHBAnalysisDtls:message key="LBL_EXPORT_OPTIONS" />
						:<img src='img/ico_file_excel.png' />&nbsp; <a href="#"
							onclick="fnDownloadXLS();"><fmtTHBAnalysisDtls:message
								key="LBL_EXCEL" /></a>
					</div>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="10" height="1"></td>
			</tr>
			<tr id="DivNothingMessage">
				<td colspan="10" align="center" height="25" class="RegularText"><div>
						<fmtTHBAnalysisDtls:message key="NO_DATA_FOUND" />
					</div></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>