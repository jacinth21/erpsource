
<%
	/*************************************************************
	 * File		 		: GmInventoryLookUpDetails.jsp
	 * Desc		 		: Displays when PTRD Libnk is clicked
	 * Author           : Shiny
	 *************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ page isELIgnored="false"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtInventoryLookUpDetails"
	uri="http://java.sun.com/jsp/jstl/fmt"%>

<fmtInventoryLookUpDetails:setBundle
	basename="properties.labels.operations.receiving.thbreceiving.GmInventoryLookUpDetails" />
<bean:define id="strJSONGridData" name="frmInventoryLookUpDetails"
	property="strJSONGridData" type="java.lang.String"></bean:define>
<bean:define id="strFromLink" name="frmInventoryLookUpDetails"
	property="strFromLink" type="java.lang.String"></bean:define>
<!-- operations\receiving\thbreceiving\GmInventoryLookUpDetails.jsp -->

<%
	String strWikiTitle = GmCommonClass.getWikiTitle("PTRD_DETAILS");
	String strWikiTitle1 = GmCommonClass.getWikiTitle("BACK_ORDER_DETAILS");
	String strOpRecvJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_OPERATIONS_RECEIVING");
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: InventoryLookUp details</TITLE>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript"
	src="<%=strOpRecvJsPath%>/thbreceiving/GmInventoryLookUpDetails.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script>
var objGridData = '';
objGridData = '<%=strJSONGridData%>';		
var gridObj;
var header ='<%=strFromLink%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form
		action="/gmInventoryLookUpDetails.do?method=fetchInventoryLookUpDetails">
		<table border="0" class="DtTable700" width="700" cellspacing="0"
			cellpadding="0"
			style="border-width: 1px; border-style: solid; border-color: #000000">
			<tr>
				<td colspan="5">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<%
					if (strFromLink.equals("PTRD")) {
					%>
						<tr>
							<td height="25" class="RightDashBoardHeader">&nbsp;<fmtInventoryLookUpDetails:message
									key="LBL_PTRD_HDR" /></td>
							<td height="25" class="RightDashBoardHeader" align="right"
								colspan="4"><fmtInventoryLookUpDetails:message
									key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit'
								align="right" style='cursor: hand'
								src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
								height='16'
								onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
							</td>
						</tr>
					<%
					} else {
					%>
					<tr>
							<td height="25" class="RightDashBoardHeader">&nbsp;<fmtInventoryLookUpDetails:message
									key="LBL_BACK_ORDER_HDR" /></td>
							<td height="25" class="RightDashBoardHeader" align="right"
								colspan="4"><fmtInventoryLookUpDetails:message
									key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit'
								align="right" style='cursor: hand'
								src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
								height='16'
								onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle1%>');" />
							</td>
						</tr>
					<%
						}
					%>
					</table>
				</td>
			</tr>
			<%
				if (strFromLink.equals("PTRD")) {
			%>
			<tr>
				<td align="left">
					<div id="inventoryLookUpRptDiv" class="grid" width=800px
						height=500px style="display: none;"></div>
				</td>
			</tr>
			<%
				} else {
			%>
			<tr>
				<td align="left">
					<div id="backOrderRptDiv" class="grid" width=800px height=500px></div>
				</td>
			</tr>
			<%
				}
			%>
			<tr>
				<td align="center" colspan="5">
					<div id="excelDiv" class='exportlinks'>
						<fmtInventoryLookUpDetails:message key="LBL_EXPORT" />
						:<img src='img/ico_file_excel.png' />&nbsp; <a href="#"
							onclick="fnDownloadXLS();"><fmtInventoryLookUpDetails:message
								key="LBL_EXCEL" /></a>
					</div>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="16"></td>
			</tr>
			<tr height="30">
				<td align="center"  colspan="2">
					<div id="Close">
					<gmjsp:button style="width: 10em" value="Close" gmClass="button"
							onClick="fnClose();" buttonType="Save" name="Btn_Close" />
									</div>
				</td>
			</tr>
			<tr id="nothingToMsgDiv">
				<td align="center" height="25" class="RegularText"><fmtInventoryLookUpDetails:message
						key="LBL_NO_DATA_FOUND" /></td>
			</tr>
		</table>
	</html:form>
</BODY>

</HTML>