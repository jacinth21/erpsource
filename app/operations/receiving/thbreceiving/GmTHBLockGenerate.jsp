<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page isELIgnored="false"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTHBLoadPendDash"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/common/GmHeader.inc"%>

<bean:define id="strLockGenerateAccess" name="frmTHBLoadPendDash"
	property="strLockGenerateAccess" type="java.lang.String"></bean:define>
<bean:define id="hLoadNumStr" name="frmTHBLoadPendDash"
	property="hLoadNumStr" type="java.lang.String"></bean:define>
<bean:define id="strAnalysisId" name="frmTHBLoadPendDash"
	property="strAnalysisId" type="java.lang.String"></bean:define>
	<bean:define id="strLoadStatus" name="frmTHBLoadPendDash"
	property="strLoadStatus" type="java.lang.String"></bean:define>
	<bean:define id="strScreen" name="frmTHBLoadPendDash"
	property="strScreen" type="java.lang.String"></bean:define>
	
<fmtTHBLoadPendDash:setBundle
	basename="properties.labels.operations.receiving.thbreceiving.GmTHBPendAnalysisDash" />

<!-- \operations\receiving\thbreceiving\GmTHBLockGenerate.jsp-->

<%
	String strWikiTitle = GmCommonClass
			.getWikiTitle("THB_LOAD_PEND_ANA_DASH");
	String strOpRecvJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_OPERATIONS_RECEIVING");
	String strLoadStatusfl = (strLoadStatus.equals("108220") ? "true" : "false");
	String strCurrDate = GmCommonClass.parseNull(GmCalenderOperations.getCurrentDate(strGCompDateFmt));
	String strCompanyInfo ="{\"cmpid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getCmpid())+"\",\"partyid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPartyid())+"\",\"plantid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPlantid())+"\"}";
    strCompanyInfo = URLEncoder.encode(strCompanyInfo);

%>

<HTML>
<HEAD>
<TITLE>Globus Medical: THB Receiving DashBoard</TITLE>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/thbreceiving/GmTHBPendAnalysisDash.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/thbreceiving/GmTHBLockGenerate.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script type="text/javascript">
var format = '<%=strGCompDateFmt%>';
var strAnalysisId = '<%=strAnalysisId%>';
var strLoadStatus = '<%=strLoadStatus%>';
var currDate = '<%=strCurrDate%>';
var companyInfoObj = '<%=strCompanyInfo%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="onload()">
<html:form action="/gmTHBLoadPendDash.do?method=loadLockAndGenerate">
		<html:hidden property="strLoadIdStr" />
		<html:hidden property="strLoadNumStr" />
		<html:hidden property="hLoadNumStr" />
		<html:hidden property="strScreen" />
		<html:hidden property="strLoadStatus" value="<%=strLoadStatus%>"/>
		<html:hidden property="strAnalysisId" value="<%=strAnalysisId%>"/>
<table class="DtTable500" cellspacing="0" cellpadding="0">
<tr>
		<td colspan="5">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
			<td height="25" class="RightDashBoardHeader"  colspan="2">&nbsp;<fmtTHBLoadPendDash:message
						key="LBL_LOCK_SCREEN_HDR" /></td>
			</tr></table></td></tr>
																		
<% if(!strAnalysisId.equals("")) {
%>	<!-- If the screen is called from THB Analysis List report by selecting the Edit Processing Dates	 -->
	<tr height="30">
				<fmtTHBLoadPendDash:message key="LBL_ANALYSIS_ID" var="varAnalysisID" />
				<gmjsp:label type="BoldText" SFLblControlName="${varAnalysisID}:"
					td="true" width="40%" />
				<td colspan="2">&nbsp;
						 <bean:write name="frmTHBLoadPendDash" property="strAnalysisId" />&nbsp;
				</td>
	</tr>
<% }else {
%><!-- If the screen is called from THB Analysis Dashboard  by selecting the Load number -->
	<tr height="30">
				<fmtTHBLoadPendDash:message key="LBL_LOAD_NUM" var="varLoadNum" />
				<gmjsp:label type="BoldText" SFLblControlName="${varLoadNum}:"
					td="true" width="39%" />
				<td colspan="2" style="max-width: 90%;display: inline-block;word-break: break-all;">
						 <bean:write name="frmTHBLoadPendDash" property="hLoadNumStr" />&nbsp;
				</td>
	</tr>
<% }
%>
	<tr>
		<td class="Empty" height="1"  colspan="2"></td>
	</tr>
	<tr height="30">
				<td class="RightTableCaption"  align="right" >&nbsp;<fmtTHBLoadPendDash:message
						key="LBL_PROC_START_DT" />:
				</td>
				<td  colspan="2">&nbsp; <gmjsp:calendar SFFormName="frmTHBLoadPendDash"
						controlName="fromDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" /> &nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td class="Empty" height="1"></td>
			</tr>
				<tr height="30">
				<td class="RightTableCaption" HEIGHT="24" align="right" width="100"> &nbsp;<fmtTHBLoadPendDash:message
						key="LBL_PROC_END_DT" />:
				</td>
				<td  colspan="2">&nbsp; <gmjsp:calendar SFFormName="frmTHBLoadPendDash"
						controlName="toDate" gmClass="InputArea"
						onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" /> &nbsp;&nbsp;
				</td>
			</tr>
			
			<%if (strLoadStatusfl.equals("true")){%>
			<div id="DivLockMsg">
					<tr><td colspan="16" class="Line" height="1"></td></tr>
						<tr ><td align="center" colspan="16" class="RightTableCaption">
							<font color="red"><fmtTHBLoadPendDash:message key="LBL_LOCK_PROG" /></font></td>
						</tr></div>
			<%} %>
			<tr>
				<td class="Empty" height="1" ></td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="16"></td>
			</tr>
			<tr height="30">
				<td align="center"  colspan="2">
					<div id="lockbtn">
						<gmjsp:button style="width: 10em" value="Cancel" gmClass="button"
							onClick="fnCancel();" buttonType="Save" name="Btn_cancel" />
						<gmjsp:button style="width: 10em" value="Submit" gmClass="button"
							onClick="fnSubmit();" buttonType="Save" name="Btn_submit" disabled="<%=strLoadStatusfl%>"/>
					</div>
				</td>
			</tr>
			<tr>
				<td class="Empty" height="1" ></td>
			</tr>
</table>
</html:form>
</BODY>
</HTML>
