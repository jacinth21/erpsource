<!-- operations\receiving\thbreceiving\GmDonorSchedulingReport.jsp-->
<%@ page language="java"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ taglib uri="com.corda.taglib" prefix="ctl"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page
	import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtDonorSchedulingReport"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<fmtDonorSchedulingReport:setLocale value="<%=strLocale%>" />
<fmtDonorSchedulingReport:setBundle
	basename="properties.labels.operations.receiving.thbreceiving.GmDonorLoadDtlsReport" />

<bean:define id="loadNum" name="frmDonorLoadDtlsRpt" property="loadNum"
	type="java.lang.String">
</bean:define>
<bean:define id="donorNum" name="frmDonorLoadDtlsRpt"
	property="donorNum" type="java.lang.String">
</bean:define>
<bean:define id="strOpt" name="frmDonorLoadDtlsRpt"
	property="strOpt" type="java.lang.String">
</bean:define>
<bean:define id="strJSONGridData" name="frmDonorLoadDtlsRpt" property="strJSONGridData" type="java.lang.String"></bean:define>
<%
	String strWikiTitle = GmCommonClass
			.getWikiTitle("DONOR_SCHEDULE_REPORT");
	String strOpRecvJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_OPERATIONS_RECEIVING");
	String strApplDateFmt = strGCompDateFmt;
%>
<html>
<head>
<title>Globus Medical: Donor Scheduling Report</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript"
	src="<%=strOpRecvJsPath%>/thbreceiving/GmDonorLoadDtlsReport.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
	
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">	

<script>
var objGridData = '<%=strJSONGridData%>';
var format = '<%=strApplDateFmt%>';
</script> 

</head>
<body leftmargin="20" topmargin="10" onload="fnOnLoad();">
	<%-- <html:form action="/gmDonorLoadDtlsRpt.do?method=loadDonorLoadDtls&hFromLink=donorSchedule"> --%>
	<html:form action="/gmDonorLoadDtlsRpt.do?method=loadDonorLoadDtls">
		<html:hidden property="strOpt" name="frmDonorLoadDtlsRpt" />
		<html:hidden property="hLoadNumStr" name="frmDonorLoadDtlsRpt" />
		<html:hidden property="hDonorNumStr" name="frmDonorLoadDtlsRpt" />
		<html:hidden property="hFromLink" name="frmDonorLoadDtlsRpt" value="donorSchedule"/>
		
		<table class="DtTable850" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="26" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtDonorSchedulingReport:message
						key="LBL_LOAD_DONOR_DETAILS_REPORT" /></td>
				<td align="right" class=RightDashBoardHeader colspan="3"><fmtDonorSchedulingReport:message
						key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="7" height="1"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" HEIGHT="30" align="right" width="10%"><fmtDonorSchedulingReport:message
						key="LBL_LOAD_NUM" />:&nbsp;</td>
				<td width="15%"><html:text property="loadNum"
						styleClass="InputArea" name="frmDonorLoadDtlsRpt"
						value='<%=loadNum%>' onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');"
						tabindex="1" size="15" />
				</td>
				<td class="RightTableCaption" HEIGHT="30" align="right" width="10%"><fmtDonorSchedulingReport:message
						key="LBL_DONOR_NUM" />:&nbsp;</td>
				<td width="15%"><html:text property="donorNum"
						styleClass="InputArea" name="frmDonorLoadDtlsRpt"
						value='<%=donorNum%>' onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');"
						tabindex="2" size="15" />
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="7" height="1"></td>
			</tr>
			<tr class="shade">
				<td class="RightTableCaption" HEIGHT="30" align="right" width="10%"><fmtDonorSchedulingReport:message
						key="LBL_STG_DT_FRM"/>:&nbsp;</td>
				<td width="15%"><gmjsp:calendar
						SFFormName="frmDonorLoadDtlsRpt" controlName="stageDateFrom"  
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff'); " />
				</td>
				<td class="RightTableCaption" HEIGHT="30" align="right" width="10%"><fmtDonorSchedulingReport:message
						key="LBL_STG_DT_TO" />:&nbsp;</td>
				<td width="15%"><gmjsp:calendar
						SFFormName="frmDonorLoadDtlsRpt" controlName="stageDateTo" 
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff'); " />
				</td>
				<td width="10%" colspan="3"><fmtDonorSchedulingReport:message
						key="BTN_LOAD" var="varLoad" /> <gmjsp:button name="Btn_Load"
						value="${varLoad}" gmClass="button"
						onClick="javascript:fnFetchDonorLoadDtls(this);" tabindex="3"
						buttonType="Load" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="7" height="1"></td>
			</tr>
			<tr>
				<td colspan="7"><div id="dataGridDiv" style="width: 850px"></div></td>
			</tr>
			<tr>
				<td colspan="7" align="center">
					<div class='exportlinks' id="DivExportExcel">
						<fmtDonorSchedulingReport:message key="LBL_EXPORT_OPTIONS" />
						:<img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnDownloadXLS();"><fmtDonorSchedulingReport:message
								key="LBL_EXCEL" /></a>
					</div>
				</td>
			</tr>
			<tr height="30" id="DivNothingMessage">
				<td colspan="7" align="center"><div>
						<fmtDonorSchedulingReport:message key="LBL_NO_DATA_AVAILABLE" />
					</div></td>
			</tr>
		</table>
	</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>