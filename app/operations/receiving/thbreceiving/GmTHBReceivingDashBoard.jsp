<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ page isELIgnored="false"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTHBReceivingDashboard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="/common/GmHeader.inc"%>

<bean:define id="strXmlGridData" name="frmTHBReceivingReport" property="strXmlGridData" type="java.lang.String"></bean:define>
<fmtTHBReceivingDashboard:setBundle basename="properties.labels.operations.receiving.thbreceiving.GmTHBReceivingDashboard"/>
<!-- operations\receiving\GmTHBReceivingDashBoard.jsp -->
 <%
String strWikiTitle = GmCommonClass.getWikiTitle("THB_RECEIVING_DASHBOARD");
String strOpRecvJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
%> 
<HTML>
<HEAD>
<TITLE> Globus Medical: THB Receiving DashBoard</TITLE>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script> 
<script language="JavaScript" src="<%=strOpRecvJsPath%>/thbreceiving/GmTHBReceivingDashboard.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<script>
var objGridData = '';
objGridData = '<%=strXmlGridData%>';
</script>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/displaytag.css");
	  @import url("<%=strCssPath%>/screen.css");
	  table.DtTable550 {
		margin: 0 0 0 0;
		width: 550px;
		border: 1px solid  #676767;
	}  
	</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnload()"> 
 <html:form action="/gmTHBReceivingDashBoard.do?method=fetchTHBReceivingDashBoard">
	<table class="DtTable550" cellspacing="0" cellpadding="0">
		<tr>
		<td colspan="5">
			<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
				<td height="25" class="RightDashBoardHeader">&nbsp;<fmtTHBReceivingDashboard:message key="LBL_THB_RECEIVING_DASHBOARD"/></td>
				<td  height="25" class="RightDashBoardHeader" align="right" colspan="4">
					<fmtTHBReceivingDashboard:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			      		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			      </td>
		      	</tr>
		      </table>
	      	</td>
	      	</tr>
 			<tr>
			<td align="left" colspan="5">
				<div id="thbdashRpt" class="grid" style="height: 500px;width: 100%"></div>
			</td>
			</tr>
			<tr>
				<td align="center" colspan="5">
			<div class='exportlinks'><fmtTHBReceivingDashboard:message key="LBL_EXCEL"/>:<img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnDownloadXLS();"><fmtTHBReceivingDashboard:message key="LBL_EXCEL"/></a></div>   
			 	</td>
			</tr>               
	</table>
</html:form> 
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

