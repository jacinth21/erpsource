<!-- operations\receiving\thbreceiving\GmTHBDonorScheduling.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtTHBDonorScheduling"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtTHBDonorScheduling:setLocale value="<%=strLocale%>" />
<fmtTHBDonorScheduling:setBundle
	basename="properties.labels.operations.receiving.thbreceiving.GmTHBDonorScheduling" />

<bean:define id="alActionList" name="frmTHBDonorSchedule" property="alActionList" type="java.util.ArrayList"></bean:define>
<bean:define id="strScheduleGridData" name="frmTHBDonorSchedule"
	property="strScheduleGridData" type="java.lang.String"></bean:define>
<bean:define id="strScheduleDtGridData" name="frmTHBDonorSchedule"
	property="strScheduleDtGridData" type="java.lang.String"></bean:define>
<bean:define id="strLoadNum" name="frmTHBDonorSchedule"
    property="strScheduleDtGridData" type="java.lang.String"></bean:define>    
<%
  String strWikiTitle = GmCommonClass
					.getWikiTitle("THB_DONOR_STAGE_SCHEDULING");
			String strOpRecvJsPath = GmFilePathConfigurationBean
					.getFilePathConfig("JS_OPERATIONS_RECEIVING");
			String strUserId = GmCommonClass.parseNull((String) session
					.getAttribute("strSessUserId"));
			String strShUserName = GmCommonClass.parseNull((String) session
					.getAttribute("strSessShName"));
			String strSessCurrSign = GmCommonClass.parseNull((String) session
					.getAttribute("strSessCurrSymbol"));
%>
<html>
<head>
<title>Globus Medical : THB Analysis Details Report</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">

<link rel="STYLESHEET" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css">
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxcontainer.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/patterns/dhtmlxlayout_pattern4w.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/thbreceiving/GmTHBDonorScheduling.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>

<style>
.dhtmlxInfoBarLabel {
	border: 1px;
	background-color: #CDE2FD;
	height: 21px;
	padding-top: 5px;
}
.dhtmlxLayoutSinglePoly {
	height: 194px;
}
</style>
<script type="text/javascript">
var objScheduleJSONData ='';
objScheduleJSONData='<%=strScheduleGridData%>';
var format = '%m/%d/%Y';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form
		action="/gmTHBDonorScheduling.do?method=fetchTHBDonorSchedulingDtls">

		<table class="DtTable1055" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="26" class="RightDashBoardHeader" colspan="7">&nbsp;<fmtTHBDonorScheduling:message
						key="LBL_THB_DONOR_STAGE_SCHEDULING_HDR" /></td>
				<td align="right" class=RightDashBoardHeader colspan="3"><fmtTHBDonorScheduling:message
						key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="10" class="LLine" height="1"></td>
			</tr>
			 <tr>
			<td class="RightTableCaption" HEIGHT="30" align="center" colspan="3"><fmtTHBDonorScheduling:message
						key="LBL_LOAD" />:&nbsp;
			<html:text property="strLoadNum"
						styleClass="InputArea" name="frmDonorLoadDtlsRpt"
						value='<%=strLoadNum%>' onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" 
						tabindex="1" size="15" onkeypress="fnCallGo();"/> &nbsp;&nbsp;
				
				<fmtTHBDonorScheduling:message
						key="BTN_LOAD" var="varLoad" /> <gmjsp:button name="Btn_Load"
						value="${varLoad}" gmClass="button"
						onClick="javascript:fnGetDonorSchedDtls();" tabindex="3"  style="width: 5em; height: 23px; font-size: 9pt;"
						buttonType="Load" /></td>
			</tr>
			<tr>
				<td colspan="10" class="LLine" height="1"></td>
			</tr>
			<tr>
				<td colspan="7" style="vertical-align: top; position:relative; top:3px;" >
				   <div id="donorSchedulidGridDiv"  class="grid" width="800px" height="700px"></div>
				</td>
				<td style="vertical-align: top;" colspan="3">
					<table>
						<tr>
								<td>
									<div id="thbDash_1" style="height: 348px; width:250px; border: #B5CDE4 1px solid; "></div>	
								</td>
						</tr>	              
					</table>
				</td>
			</tr>
			<tr id="thbchoose">
				<td colspan="16" class="RightText" align="Center"><BR><fmtTHBDonorScheduling:message key="LBL_CHOOSE_ACTION"/>:&nbsp;
				 <gmjsp:dropdown controlName="Choose_Action" SFFormName="frmTHBDonorSchedule" SFSeletedValue="Choose_Action" 
						codeId="CODEID" codeName="CODENM" tabIndex="1" SFValue="alActionList" defaultValue="[Choose One]"/>
						<fmtTHBDonorScheduling:message key="BTN_SUBMIT" var="varSubmit"/> 
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnChooseAction();" buttonType="Save" />&nbsp;<BR><BR>
			</td>
			</tr>
			
			 <tr>
				<td class="LLine" colspan="10" height="1"></td>
			</tr> 
			<tr>
				<td align="center" colspan="10">
					<div class='exportlinks' id="DivExportExcel">
						<fmtTHBDonorScheduling:message key="LBL_EXPORT_OPTIONS" />
						:<a href="#" onclick="fnDownloadXLS();"><img src='img/ico_file_excel.png' /></a>&nbsp; <a href="#"
							onclick="fnDownloadXLS();"><fmtTHBDonorScheduling:message
								key="LBL_EXCEL" /></a>
					</div>
				</td>
			</tr>
			 <tr>
				<td class="LLine" colspan="10" height="1"></td>
			</tr>
			<tr id="DivNothingMessage">
				<td colspan="8" align="center" height="25" class="RegularText"><div>
						<fmtTHBDonorScheduling:message key="NO_DATA_FOUND" />
					</div></td>
			</tr>
		</table>
	</html:form>
</body>
</html>