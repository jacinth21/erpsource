<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtTHBLoad" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtTHBLoad:setLocale value="<%=strLocale%>"/>
<fmtTHBLoad:setBundle basename="properties.labels.operations.receiving.thbreceiving.GmTHBReceivingTrans"/>
<bean:define id="strGridXmlData" name="frmTHBLoad" property="strGridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="alTHBHead" name="frmTHBLoad" property="alTHBHead" type="java.util.ArrayList"></bean:define>
<bean:define id="strLoadNo" name="frmTHBLoad" property="strLoadNo" type="java.lang.String"></bean:define>
<bean:define id="strCtrlNo" name="frmTHBLoad" property="strCtrlNo" type="java.lang.String"></bean:define>
<bean:define id="strTotAllo" name="frmTHBLoad" property="strTotAllo" type="java.lang.String"></bean:define>
<bean:define id="strScanAllo" name="frmTHBLoad" property="strScanAllo" type="java.lang.String"></bean:define> 
<bean:define id="strLoadId" name="frmTHBLoad" property="strLoadId" type="java.lang.String"></bean:define>
<bean:define id="strFrom" name="frmTHBLoad" property="strFrom" type="java.lang.String"></bean:define>
<bean:define id="strStatus" name="frmTHBLoad" property="strStatus" type="java.lang.String"></bean:define>
 	
<%
String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: </TITLE>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/thbreceiving/GmTHBReceivingTrans.js"></script>
<script>
var objGridData = '<%=strGridXmlData%>' ;
</script>
<%-- <style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style> --%>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/displaytag.css");
	  @import url("<%=strCssPath%>/screen.css");
	  table.DtTable580 {
		margin: 0 0 0 0;
		width: 580px;
		border: 1px solid  #676767;
	}  
	</style>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmTHBReceivingLoadTran.do?" >
<html:hidden property="hAction" value=""/>
<html:hidden property="hLoadNo" value="<%=strLoadNo%>"/>
<html:hidden property="strStatus" value="<%=strStatus%>"/>
<html:hidden property="strTotAllo" value="<%=strTotAllo%>"/>
<html:hidden property="strScanAllo" value="<%=strScanAllo%>"/>
<table class="DtTable580" border="0" cellspacing="0" cellpadding="0">
<tr>
		<td height="28" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtTHBLoad:message key="LBL_HEAD"/></td>
		<td  height="28" class="RightDashBoardHeader" align="right">
		<fmtTHBLoad:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		     height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("THB_RECEIVE_LOAD")%>');"/>
		</td>
	</tr>
	<tr><td colspan="6" class="LLine" height="1"></td></tr>
	<tr height="28">
	 <fmtTHBLoad:message key="LBL_THB_LOAD_NO" var="varLoadNo"/>
		<gmjsp:label type="BoldText"  SFLblControlName="${varLoadNo}" />&nbsp;<td>
		<input type="text" size="20" value="<%=strLoadNo%>" name="strLoadNo" class="InputArea"  onkeypress="javascript:fnCheckKey();"
		onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;<fmtTHBLoad:message key="BTN_LOAD" var="varSubmit"/>
			<gmjsp:button style="width: 5em" value="${varSubmit}" gmClass="button" onClick="fnFetchBoxDtls();" buttonType="Load" name="Btn_Load"/>	
	</td><td></td>
	</tr>
	<tr><td colspan="6" class="LLine" height="1"></td></tr>
	<logic:notEqual name="frmTHBLoad" property="strLoadNo" value="">
	 <tr> <td colspan="6"> 
	<table border="0" cellspacing="0" cellpadding="0"> 
	<tr height="28" class="Shade">
	<fmtTHBLoad:message key="LBL_SYNC_BY" var="varSyncBy"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varSyncBy}" width="150px"/>
			<td>&nbsp;<bean:write name="frmTHBLoad" property ="strSyncBy"/></td>
     <fmtTHBLoad:message key="LBL_SYNC_DT" var="varSyncDt"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varSyncDt}" width="100px"/>	
			<td>&nbsp;<bean:write name="frmTHBLoad" property ="strSyncDt"/></td>		
	</tr>
	</table></td></tr>
	<tr><td colspan="6" class="LLine" height="1"></td></tr>
	<tr height="28" >
	<fmtTHBLoad:message key="LBL_BOX_NO" var="varBoxNo"/>
			<gmjsp:label type="BoldText"  SFLblControlName="${varBoxNo}"/>
			<td>&nbsp;<gmjsp:dropdown controlName="strLoadId" SFFormName="frmTHBLoad" SFSeletedValue="strLoadId" 
			SFValue="alTHBHead" codeId = "CODEID"  codeName = "CODENM" onChange="fnFetchLoadDtls();"  defaultValue= "[Choose One]" />													
			</td>
	</tr>
	<tr><td colspan="6" class="LLine" height="1"></td></tr>
	 <tr>
	<td colspan="6">
	<table id="scanContId" border="0" cellspacing="0" cellpadding="0" style="display: none;">  <!-- style="display: none;"  -->
	<tr>
				<td height="25" class="ShadeRightTableCaption" colspan="5">&nbsp;<b><fmtTHBLoad:message key="LBL_SCAN_CTRL_SEC"/></b></td>
	</tr>
	<tr><td colspan="6" class="LLine" height="1"></td></tr>
			<tr height="28">
			<fmtTHBLoad:message key="LBL_SCAN_CTRL_NO" var="varScanCtrl"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varScanCtrl}" />
				<td>&nbsp;<input type="text" size="20" value="<%=strCtrlNo%>" name="strCtrlNo" class="InputArea" onkeypress="fnScanGraft(this);"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
					&nbsp;<fmtTHBLoad:message key="BTN_ADD" var="varSubmit"/>
					<gmjsp:button style="width: 5em" value="${varSubmit}" gmClass="button" onClick="fnAddScanKey(this);" buttonType="Save" name="Btn_Add"/>									
				</td>
			</tr>
	<tr><td colspan="6" class="LLine" height="1"></td></tr>
		 <tr> <td colspan="6"> 
	<table border="0" cellspacing="0" cellpadding="0"> 
	<tr height="28" class="Shade">
	<fmtTHBLoad:message key="LBL_TOT_ALLO" var="varTotAllo"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varTotAllo}" width="150px"/>
				<td id="totAlloId" width="175px">&nbsp;<bean:write name="frmTHBLoad" property ="strTotAllo"/></td>
				<fmtTHBLoad:message key="LBL_TOT_SCN_PRT" var="varScanAllo"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varScanAllo}" width="120px"/>
				<td id="scanAlloId" >&nbsp;<bean:write name="frmTHBLoad" property ="strScanAllo"/></td>		
	</tr></table></td></tr>
	<tr><td colspan="6" class="LLine" height="1"></td></tr>
	<tr height="28">
	<td></td>
			<td>
				&nbsp;<fmtTHBLoad:message key="BTN_SCAN" var="varSubmit"/>
				<gmjsp:button style="width: 10em" value="${varSubmit}" gmClass="button" onClick="fnUpdateLoadDtls();" buttonType="Save" name="Btn_Scan"/>
			</td>
			</tr>
		<tr><td colspan="6" class="LLine" height="1"></td></tr>
		<%if(strGridXmlData  != null) { %>  
				<tr>
					<td colspan="7">
						<div id="THBReceive" style="height:680px; width:100%;"></div>
					</td>
			
				</tr>
				  <% }else{ %>	
				<tr><td colspan="2" align="center" class="RightTextRed"><fmtTHBLoad:message key="LBL_NO_DATA_FOUND"/> </td></tr>
			<%}%> 
	 </table>
	 </td>
	</tr> 
	 </logic:notEqual> 
</table>
</html:form>
<script>
var strFrom='<%=strFrom%>';
if(strFrom=='dash'){
	document.getElementById("scanContId").style.display='BLOCK';
}
</script>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
