<!-- operations\receiving\thbreceiving\GmTHBDonorSchedulingPO.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtTHBDonorScheduling"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtTHBDonorScheduling:setLocale value="<%=strLocale%>" />
<fmtTHBDonorScheduling:setBundle
	basename="properties.labels.operations.receiving.thbreceiving.GmTHBDonorScheduling" />

<bean:define id="alActionList" name="frmTHBDonorSchedule" property="alActionList" type="java.util.ArrayList"></bean:define>
<bean:define id="strDonorNum" name="frmTHBDonorSchedule" property="strDonorNum" type="java.lang.String"></bean:define>
<bean:define id="strLoadNum" name="frmTHBDonorSchedule" property="strLoadNum" type="java.lang.String"></bean:define>

<bean:define id="strScheduleGridData" name="frmTHBDonorSchedule"
	property="strScheduleGridData" type="java.lang.String"></bean:define>
<bean:define id="strScheduleDtGridData" name="frmTHBDonorSchedule"
	property="strScheduleDtGridData" type="java.lang.String"></bean:define>
<%

  String strWikiTitle = GmCommonClass
					.getWikiTitle("THB_DONOR_STAGE_SCHEDULING");
			String strOpRecvJsPath = GmFilePathConfigurationBean
					.getFilePathConfig("JS_OPERATIONS_RECEIVING");
			String strUserId = GmCommonClass.parseNull((String) session
					.getAttribute("strSessUserId"));
			String strShUserName = GmCommonClass.parseNull((String) session
					.getAttribute("strSessShName"));
			String strSessCurrSign = GmCommonClass.parseNull((String) session
					.getAttribute("strSessCurrSymbol"));
%>
<html>
<head>
<title>Globus Medical : THB Analysis Details Report</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">

<link rel="STYLESHEET" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxcontainer.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/patterns/dhtmlxlayout_pattern4w.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/thbreceiving/GmTHBDonorScheduling.js"></script>
<style>
.dhtmlxInfoBarLabel {
	border: 1px;
	background-color: #CDE2FD;
	height: 21px;
	padding-top: 5px;
}
.dhtmlxLayoutSinglePoly {
	height: 194px;
}
#successCont {
	color:red;
}
</style>
<script type="text/javascript">
var objScheduleJSONData ='';
objScheduleJSONData='<%=strScheduleGridData%>';
var format = '%m/%d/%Y';

var donornumber = "<%=strDonorNum%>";
var loadnumber = "<%=strLoadNum%>";
</script>
</head>
<body leftmargin="20" topmargin="10" onload="">
	<html:form
		action="/gmTHBDonorScheduling.do?method=loadTHBDonorPO">

		<table class="DtTable955" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="26" class="RightDashBoardHeader" colspan="7">&nbsp;<fmtTHBDonorScheduling:message
						key="LBL_THB_DONOR_STAGE_SCHEDULING_HDR" /></td>
				<td align="right" class=RightDashBoardHeader colspan="3"><fmtTHBDonorScheduling:message
						key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="10" class="LLine" height="1"></td>
			</tr>
			 <tr>
			<td class="RightTableCaption" HEIGHT="30" align="center" colspan="3"><fmtTHBDonorScheduling:message
						key="LBL_PO" />:&nbsp;
			<html:text property="strPoNum"
						styleClass="InputArea" name="frmTHBDonorSchedule" 
						 onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" 
						tabindex="1" size="15" onkeypress="fnCallGo();"/> &nbsp;&nbsp;
				
				<fmtTHBDonorScheduling:message key="BTN_SUBMIT" var="varLoad" />
						<gmjsp:button name="Btn_Load"
						value="${varLoad}" gmClass="button"
						onClick="fnSubmit();" tabindex="3"  style="width: 5em; height: 23px; font-size: 9pt;"
						buttonType="Submit" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="10" height="1"></td>
			</tr>
			<tr>
	 	 		<td colspan="7"><div id="successCont" style="text-align:center;"></div></td>
			 </tr> 
		</table>
	</html:form>
</body>
</html>