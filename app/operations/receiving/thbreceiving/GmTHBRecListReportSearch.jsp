<?xml version="1.0" encoding="ISO-8859-1" ?>
<%
/**********************************************************************************
 * File		 		: GmTHBRecListReportSearch.jsp
 * Desc		 		: This screen in used to display thb receiving report
 * author			: Shiny
************************************************************************************/
%>
<!-- operations\receiving\thbreceiving\GmTHBRecListReportSearch.jsp
 -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="fmtTHBRecListRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmtTHBRecListRpt:setLocale value="<%=strLocale%>"/>
<fmtTHBRecListRpt:setBundle basename="properties.labels.operations.receiving.thbreceiving.GmTHBRecListReportSearch"/>

<bean:define id="strLoadStatus" name="frmTHBRecListRpt" property="strLoadStatus" type="java.lang.String"></bean:define>
<bean:define id="strXmlGridData" name="frmTHBRecListRpt" property="strXmlGridData" type="java.lang.String"></bean:define>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("THB_REC_LIST_RPT");
String strOpRecvJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
String strApplDateFmt = strGCompDateFmt;
%>
<HTML>
<head>
<title>List Report </title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script type="text/javascript" src="<%=strOpRecvJsPath%>/thbreceiving/GmTHBRecListReportSearch.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>

var objGridData = '';
objGridData = '<%=strXmlGridData%>'; 
var format = '<%=strApplDateFmt%>';
</script>
	
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<body leftmargin="20" topmargin="10"  onload = "fnOnPageLoad()" onkeyup="fnEnter();">
<html:form action="/gmTHBRecListRpt.do?method=fetchTHBListReportDetail"> 
<html:hidden property="strOpt"/>
<table cellspacing="0" cellpadding="0" class="DtTable700">


<tr>
<td colspan="6" height="25" class="RightDashBoardHeader"><fmtTHBRecListRpt:message key="LBL_THB_REC_LIST_RPT"/></td>
<td colspan="6"  align="right" class="RightDashBoardHeader"><fmtTHBRecListRpt:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor: hand'
	src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
</td>
</tr>

<tr>
<td class="LLine" colspan="12" height="1"></td>
</tr>

<tr class="oddshade" height="28">
<td class="RightTableCaption" colspan="1" width="150"  align="right"><fmtTHBRecListRpt:message key="LBL_THB_LOAD_NUM"/>:&nbsp;</td>
<td colspan="4" ><html:text size="18" name="frmTHBRecListRpt" property="strThbLoadNum" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
</td>
<td colspan="1"  width="150"  class="RightTableCaption"  align="right"><fmtTHBRecListRpt:message key="LBL_BOX_NUM" />:&nbsp;</td>
<td colspan="2" ><html:text size="18" name="frmTHBRecListRpt" property="strBoxNum" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
</td>
<td colspan="1" width="100px" class="RightTableCaption" align="right"><fmtTHBRecListRpt:message key="LBL_LOAD_STATUS"/>:&nbsp;</td>
<td colspan="2" width="150px">&nbsp;<gmjsp:dropdown controlName="strLoadStatus" SFFormName="frmTHBRecListRpt" SFSeletedValue="strLoadStatus" SFValue="alLoadStatusType" 
codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
</td>
</tr>
<tr>
<td class="LLine" colspan="12" height="1"></td>
</tr>
   
<tr height="28">
<td colspan="1"  class="RightTableCaption" align="right"><fmtTHBRecListRpt:message key="LBL_FROM"/>:&nbsp;</td>
<td colspan="4" ><gmjsp:calendar SFFormName="frmTHBRecListRpt" controlName="strFromDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"  />
</td>

<td colspan="1"  class="RightTableCaption" align="right"><fmtTHBRecListRpt:message key="LBL_TO"/>:&nbsp;</td>
<td colspan="2" ><gmjsp:calendar SFFormName="frmTHBRecListRpt" controlName="strToDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" />
</td>

<td colspan="2"  align="center"><fmtTHBRecListRpt:message key="BTN_LOAD" var="varLoad"/>
<gmjsp:button name="Btn_Load" value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnLoad();" buttonType="Load" />
</td>
</tr>
<tr>
<td class="LLine" colspan="12" height="1"></td>
</tr>
<tr><td colspan="12" height="30" id="SuccessMsg"  style="color:green;font-weight:bold;display: none;"></td></tr>
<tr>
	<%
		if (strXmlGridData.indexOf("cell") != -1) {
	%>						
				<tr>
				<td colspan="12" ><div  id="dataGridDiv" height="500px"></div></td></tr>
				<tr>						
						<tr>
				<td colspan="12" align="center">
					<div class='exportlinks'>
						<fmtTHBRecListRpt:message key="LBL_EXPORT_OPTIONS" />
						: <a href="#" onclick="fnDownloadXLS();"><img src='img/ico_file_excel.png' /></a>&nbsp;<a href="#"
							onclick="fnDownloadXLS();"><fmtTHBRecListRpt:message
								key="LBL_EXCEL" /> </a>
					</div>
				</td></tr>
       			<%} else /* if (!gridData.equals("")) */ {
			%>
			<tr>
               <td class="LLine" colspan="12" height="1"></td>
            </tr>
			<tr>
				<td  colspan="12"  height="25" align="center" class="RightText"><fmtTHBRecListRpt:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/>
					</td></tr>
	<%}%>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>
</HTML>
