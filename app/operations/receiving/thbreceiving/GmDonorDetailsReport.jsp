<!-- operations\receiving\thbreceiving\GmDonorDetailsReport.jsp-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtRadRunRpt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<fmtRadRunRpt:setLocale value="<%=strLocale%>" />
<fmtRadRunRpt:setBundle
	basename="properties.labels.operations.receiving.thbreceiving.GmDonorDetailsReport" />
<bean:define id="strJSONGridData" name="frmDonorDtlsRpt" property="strJSONGridData" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmDonorDtlsRpt" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="strFromLink" name="frmDonorDtlsRpt" property="strFromLink" type="java.lang.String"></bean:define>
<bean:define id="alLocations" name="frmDonorDtlsRpt" property="alLocations" type="java.util.ArrayList" ></bean:define>
<bean:define id="alRedesignate" name="frmDonorDtlsRpt" property="alRedesignate" type="java.util.ArrayList" ></bean:define>
<bean:define id="analysisId" name="frmDonorDtlsRpt" property="analysisId" type="java.lang.String"></bean:define>
<bean:define id="scheduleAccessFl" name="frmDonorDtlsRpt" property="scheduleAccessFl" type="java.lang.String"></bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("DONOR_DETAILS_REPORT");
    String strWikiTitleAllocation = GmCommonClass.getWikiTitle("THB_DONOR_ALLOCATION");

	String strOpRecvJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
	String strApplDateFmt = strGCompDateFmt;
	HashMap hLocType = null;
	HashMap hRedesignateType = null;
	scheduleAccessFl = (scheduleAccessFl.equals("") ? "true" : "false");
%>
<html>
<head>
<title>Globus Medical: Donor Details Report</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.css">

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/thbreceiving/GmDonorDetailsReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>

<script type="text/javascript">
var format = '<%=strApplDateFmt%>';
var objGridData = '<%=strJSONGridData%>';
var header ='<%=strFromLink%>';
var strAnalysisId='<%=analysisId%>';
var locationLen = <%= alLocations.size()%>;
var redesignateLen =<%=alRedesignate.size()%>;

//For setting the drop down value of Invetory Location(Move to)
<%	
hLocType = new HashMap();	
for (int i=0;i< alLocations.size();i++)
{
	hLocType = (HashMap) alLocations.get(i); 
%>
var TypeArr<%=i%> ="<%=GmCommonClass.parseNull((String)hLocType.get("CODEID"))%>,<%=GmCommonClass.parseNull((String)hLocType.get("CODENM"))%>";
<%
}
%>
//For setting the drop down value of Redesignate As
<% 
hRedesignateType = new HashMap();
for (int i=0;i< alRedesignate.size();i++)
{
	hRedesignateType = (HashMap) alRedesignate.get(i); 
%>
var RedesinateArr<%=i%> = "<%=GmCommonClass.parseNull((String)hRedesignateType.get("PARTNUMID"))%>";
<%
}
%>
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnLoad();">
	<html:form action="/gmDonorDetailsRpt.do?method=loadRadRunRpt">
	<html:hidden property="strOpt" name="frmDonorDtlsRpt" />
	<html:hidden property="privatePart" name="frmDonorDtlsRpt" />


		<table class="DtTable1000" border="0" cellspacing="0" cellpadding="0">
		<%
				if (strFromLink.equals("radRunQty")) {
			%>
			<tr>
				<td height="26" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtRadRunRpt:message key="LBL_THB_DONOR_ALLOCATION_HDR" /></td>
				<td align="right" class=RightDashBoardHeader colspan="3"><fmtRadRunRpt:message key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiTitleAllocation%>','<%=strWikiTitleAllocation%>');" />
				</td>
				</tr>
		
			<tr>
				<td align="left" colspan="7">
					<div id="dataGridDiv" class="grid" width=1000px height=s500px style="width:1000px;"></div>
				</td>
			</tr>
			<tr height="30" id="btnRow">
				<td colspan="7" align="center"><div>
						<gmjsp:button style="width: 10em" value="Schedule" gmClass="button"
							onClick="fnSchduling();" buttonType="Save" name="Btn_submit" disabled="<%=scheduleAccessFl%>"/>
					</div></td>
			</tr>
            
            <tr><td class="RightCaption" align="center" colspan="4"> <div id="errMsg"></div> </td></tr>

			<tr>
				<td class="LLine" colspan="7" height="1"></td>
			</tr>
			<tr>
				<td colspan="7" align="center">
					<div class='exportlinks' id="DivExportExcel">
						<fmtRadRunRpt:message key="LBL_EXPORT_OPTIONS" />
						:<img src='img/ico_file_excel.png' />&nbsp; <a href="#"
							onclick="fnDownloadXLS();"><fmtRadRunRpt:message key="LBL_EXCEL" /></a>
					</div>
				</td>
			</tr>
			<tr height="30" id="DivNothingMessage">
				<td colspan="7" align="center"><div>
						<fmtRadRunRpt:message key="LBL_NO_DATA_AVAILABLE" />
					</div></td>
			</tr>
			
		<%
				} else {
			%>	
			
			<tr>
				<td height="26" class="RightDashBoardHeader" colspan="4">&nbsp;<fmtRadRunRpt:message key="LBL_DONOR_DETAILS_REPORT" /></td>
				<td align="right" class=RightDashBoardHeader colspan="3"><fmtRadRunRpt:message key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="7" height="1"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" HEIGHT="30" align="right" width="10%"><fmtRadRunRpt:message
						key="LBL_LOAD_NUM" />:&nbsp;</td>
				<td width="15%"><html:text property="loadNum"
						styleClass="InputArea" name="frmDonorDtlsRpt"onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');"
						tabindex="1"/>
				</td>

				<td class="RightTableCaption" HEIGHT="30" align="right" width="10%"><fmtRadRunRpt:message
						key="LBL_DONOR_NUM" />:&nbsp;</td>
				<td colspan="4"><html:text
						property="donorNum" styleClass="InputArea" name="frmDonorDtlsRpt"onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');"
						tabindex="2" />
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="7" height="1"></td>
			</tr>
			<tr class="Shade" height="30">
				<td class="RightTableCaption" align="right" width="10%"><fmtRadRunRpt:message
						key="LBL_PROCESS_DATE"  var="varProcessedDt"/><gmjsp:label
						type="RegularText" SFLblControlName="${varProcessedDt}:" td="false" />&nbsp;</td>
				<td>
			    <gmjsp:calendar SFFormName="frmDonorDtlsRpt" controlName="processedDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
				</td>

				<td class="RightTableCaption" HEIGHT="30" align="right" width="10%"><fmtRadRunRpt:message
						key="LBL_BASE_PART" />:&nbsp;</td>
				<td width="15%"><html:text property="basePart"
						styleClass="InputArea" name="frmDonorDtlsRpt" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');"
						tabindex="4"/>
				</td>
				<td width="10%" colspan="3"><fmtRadRunRpt:message
						key="BTN_LOAD" var="varLoad" /> <gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button"
						onClick="javascript:fnFetchDonorLoadDtls(this);" tabindex="5" buttonType="Load"/></td>
			</tr>
			<tr>
				<td class="LLine" colspan="7" height="1"></td>
			</tr>
			<tr>
				<td colspan="7" ><div id="dataGridDiv" class="grid" style="width:100%;"></div></td>
			</tr>
			<tr>
				<td colspan="7" align="center">
					<div class='exportlinks' id="DivExportExcel">
						<fmtRadRunRpt:message key="LBL_EXPORT_OPTIONS" />
						:<img src='img/ico_file_excel.png' />&nbsp; <a href="#"
							onclick="fnDownloadXLS();"><fmtRadRunRpt:message key="LBL_EXCEL" /></a>
					</div>
				</td>
			</tr>
			<tr height="30" id="DivNothingMessage">
				<td colspan="7" align="center"><div>
						<fmtRadRunRpt:message key="LBL_NO_DATA_AVAILABLE" />
					</div></td>
			</tr>
			<%
				}
			%>
		</table>

	</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>