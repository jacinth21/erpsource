<%@ page language="java"%>
<%@ page isELIgnored="false"%>
<%@page import="java.util.ArrayList"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtTHBAnalysisListRpt"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="/common/GmHeader.inc"%>

<bean:define id="strJSONGridData" name="frmTHBAnalyListRpt" property="strJSONGridData" type="java.lang.String"></bean:define>
<bean:define id="alStatList" name="frmTHBAnalyListRpt" property="alStatList" type="java.util.ArrayList"></bean:define>
<bean:define id="alActionList" name="frmTHBAnalyListRpt" property="alActionList" type="java.util.ArrayList"></bean:define>
<bean:define id="strScheduleAccess" name="frmTHBAnalyListRpt" property="strScheduleAccess" type="java.lang.String"></bean:define> 
<fmtTHBAnalysisListRpt:setBundle basename="properties.labels.operations.receiving.thbreceiving.GmTHBAnalysisListReport" />
<!-- operations\receiving\GmTHBAnalysisListReport.jsp-->


<%
	String strWikiTitle = GmCommonClass
			.getWikiTitle("THB_ANA_LIST_REPORT");
	String strOpRecvJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_OPERATIONS_RECEIVING");
	String strApplDateFmt = strGCompDateFmt;
	strScheduleAccess = (strScheduleAccess.equals("") ? "true" : "false");
%>


<HTML>
<HEAD>
<TITLE>Globus Medical: THB Receiving DashBoard Report</TITLE>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/operations/receiving/thbreceiving/GmTHBAnalysisListReport.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>	
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/thbreceiving/GmTHBAnalysisListReport.js"></script>


<script>
var objGridData = '';
objGridData = '<%=strJSONGridData%>';
var format = '<%=strApplDateFmt%>';
var gStrServletPath = "<%=strServletPath%>";
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad()">
	<form name="frmTHBAnalyListRpt" action="/gmTHBRecListRpt.do?method=fetchTHBListReportDetail" method="post"> 
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hCancelType" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hCancelTypeFromMenu" value="">

		<table class="DtTable1000" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td colspan="16">
					<table width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td height="25" class="RightDashBoardHeader">&nbsp;<fmtTHBAnalysisListRpt:message
									key="LBL_ANA_LIST_RPT_HDR" /></td>
							<td height="25" class="RightDashBoardHeader" align="right"><fmtTHBAnalysisListRpt:message
									key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit'
								align="right" style='cursor: hand'
								src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
								height='16'
								onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
							</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr height="30">
				<fmtTHBAnalysisListRpt:message key="LBL_ANALYSIS_ID"
					var="varAnalysisID" />
				<gmjsp:label type="BoldText" SFLblControlName="${varAnalysisID}" />
				<td colspan="6">&nbsp;<html:text property="analysisId"
						name="frmTHBAnalyListRpt" styleClass="InputArea"
						onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');
								<%-- fnGenerateAnalysisStr(this); --%>" /> 
				</td>
				<fmtTHBAnalysisListRpt:message key="LBL_LOAD_ID" var="varloadId" />
				<gmjsp:label type="BoldText" SFLblControlName="${varloadId}" />
				<td colspan="6">&nbsp;<html:text property="loadId"
						name="frmTHBAnalyListRpt" styleClass="InputArea"
						onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');
						<%-- fnGenerateLoadNumStr(this); --%>" /></td>
				<fmtTHBAnalysisListRpt:message key="LBL_STATUS" var="varstatus" />
				<gmjsp:label type="BoldText" SFLblControlName="${varstatus}" />
				<td colspan="6">&nbsp;<gmjsp:dropdown controlName="status" SFFormName="frmTHBAnalyListRpt" SFSeletedValue="status" 
						codeId="CODEID" codeName="CODENM" tabIndex="1" SFValue="alStatList" defaultValue="[Choose One]" />
			</tr>
			 <tr><td class="LLine" height="1" colspan="16"></td></tr> 
			<tr height="30" class="shade">
				<fmtTHBAnalysisListRpt:message key="LBL_PROC_START_ST"
					var="varProcStDT" />
				<gmjsp:label type="BoldText" SFLblControlName="${varProcStDT}" />
				<td colspan="6">&nbsp;<gmjsp:calendar
						SFFormName="frmTHBAnalyListRpt" controlName="procStartDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" />
				</td>
				<fmtTHBAnalysisListRpt:message key="LBL_PROC_END_DT"
					var="varProcEndDT" />
				<gmjsp:label type="BoldText" SFLblControlName="${varProcEndDT}" />
				<td colspan="6">&nbsp;<gmjsp:calendar
						SFFormName="frmTHBAnalyListRpt" controlName="procEndDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" /></td>
				<td colspan="6">&nbsp;<fmtTHBAnalysisListRpt:message
						key="BTN_LOAD" var="varSubmit" /> <gmjsp:button name="Btn_Load"
						value="${varSubmit}" gmClass="button" buttonType="Load"
						onClick="fnFetchListRptDtls(this);" />&nbsp;
				</td>
			</tr>

		 	 <tr><td class="LLine" height="1" colspan="16"></td></tr> 
		
			<tr id="AnalysisPartTr" style="display: none;">
				<td colspan="16" height="30px">&nbsp; <b style="font-size: 1em"><fmtTHBAnalysisListRpt:message
							key="LBL_ANALYSIS_ID" /></b>&nbsp;
					<div id="AnalysisPartDiv">
						<ul id="ulAnalysisPart" style="display: inline-block; -ms-word-break: break-all; max-width: 90%;" ></ul>
					</div>
				</td>
			</tr>
			 <tr id="AnalysisPartTrLine" style="display: none;"><td class="LLine" height="1" colspan="16"></td></tr> 
			<tr id="LoadPartTr" style="display: none;">
				<td colspan="16" height="30px">&nbsp; <b style="font-size: 1em"><fmtTHBAnalysisListRpt:message
							key="LBL_LOAD_ID" /></b>&nbsp;
					<div id="LoadPartDiv">
						<ul id="ulLoadPart"></ul>
					</div>
				</td>
			</tr>
		 <tr id="LoadPartTrLine" style="display: none;"><td class="LLine" height="1" colspan="16"></td></tr> 
			<tr>
				<td align="left" colspan="16">
					<div id="thbListRpt" class="grid"
						style="height: 500px; width: 100%"></div>
				</td>
			</tr>
			
			<tr id="thbchoose">
				<td colspan="16" class="RightText" align="Center"><BR><fmtTHBAnalysisListRpt:message key="LBL_CHOOSE_ACTION"/>:&nbsp;
				<gmjsp:dropdown controlName="Cbo_Action1" SFFormName="frmTHBAnalyListRpt" SFSeletedValue="Cbo_Action1" 
						codeId="CODEID" codeName="CODENM" tabIndex="1" SFValue="alActionList" defaultValue="[Choose One]" />
						<fmtTHBAnalysisListRpt:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnChooseAction();" disabled="<%=strScheduleAccess%>" buttonType="Save" />&nbsp;<BR><BR>
			</td>
			</tr>
			 <tr id="divNoExcelLine"><td class="LLine" height="1" colspan="16"></td></tr> 
			<tr>
				<td align="center" colspan="16">
					<div id="divNoExcel" class='exportlinks'>
						<fmtTHBAnalysisListRpt:message key="LBL_EXPORT_OPTION" />
						:<img src='img/ico_file_excel.png' />&nbsp; <a href="#"
							onclick="fnDownloadXLS();"><fmtTHBAnalysisListRpt:message
								key="LBL_EXCEL" /></a>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="16" align="center"><div id="DivNoMsg">
						<fmtTHBAnalysisListRpt:message key="LBL_NO_DATA_AVAILABLE" />
					</div></td>
			</tr>
		</table>
		</form>
</BODY>
</HTML>