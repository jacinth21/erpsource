<%
/**********************************************************************************
 * File		 		: GmReceiveShipDash.jsp
 * Desc		 		: Receiving DashBoard
 * Version	 		: 1.0
 * author			: Anilkumar
************************************************************************************/
%>
<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib uri="com.corda.taglib" prefix="ctl" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- operations\receiving\GmReceiveShipDash.jsp -->
<%@ taglib prefix="fmtReceiveShipDash" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReceiveShipDash:setLocale value="<%=strLocale%>"/>
<fmtReceiveShipDash:setBundle basename="properties.labels.operations.receiving.GmReceiveShipDash"/>


<bean:define id="strXmlGridData" name="frmReceiveShipDash" property="strXmlGridData" type="java.lang.String"></bean:define>
<%
	String strOpRecvJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_RECEIVING");
   //The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);

%>
<HTML>
<HEAD>
<TITLE> <fmtReceiveShipDash:message key="LBL_RECV_DASH"/></TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strOpRecvJsPath%>/GmReceiveShipDash.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script>
var objGridData = '';
objGridData = '<%=strXmlGridData%>';
var compInfObj = '<%=strCompanyInfo%>'; 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnload()">
<FORM NAME="frmReceiveShipDash" action="/gmReceiveShipDash.do?">
<input type="hidden" name="haction"/>
	<table border="0" class="DtTable950" cellspacing="0" cellpadding="0">
		<tr>
			<td height="26" class="RightDashBoardHeader" colspan="4">&nbsp;<b><fmtReceiveShipDash:message key="LBL_DASH"/></b></td>
			<td align="right" class=RightDashBoardHeader colspan="2">
			<fmtReceiveShipDash:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("RECEIVE_DASHBOARD")%>');" />
			</td>
		</tr>
		<%if(strXmlGridData.indexOf("cell") != -1){%>
		<tr>
			<td colspan="6">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td><div id="recdashRpt" style="height: 550px;"></div></td>
					</tr>
					<tr>
						<td colspan="6" align="center" height="25">
							<div class='exportlinks'><fmtReceiveShipDash:message key="LBL_EXP_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"> Excel </a></div>
						</td>                 
					</tr>
				</table> 
			</td>
		</tr>				
		<%}else{%>
		<tr>
			<td colspan="6" height="20" align="center" class="RightText"><fmtReceiveShipDash:message key="LBL_NO_DATA"/></td>
		</tr>
		<%}%>
    </table>		     	

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
