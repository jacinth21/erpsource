<%
/**********************************************************************************
 * File		 		: GmLoadShipmentInfoInclude.jsp
 * Desc		 		: This JSP is for the section Bulk Shipment Details 
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLoadShipmentInfoInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtLoadShipmentInfoInclude:setLocale value="<%=strLocale%>"/>
<fmtLoadShipmentInfoInclude:setBundle basename="properties.labels.operations.receiving.GmLoadShipmentInfoInclude"/>
<!-- operations\receiving\GmLoadShipmentInfoInclude.jsp -->
<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<bean:define id="gridData" name="frmPOBulkReceive" property="gridXmlData" type="java.lang.String"> </bean:define>

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: PO Details </TITLE>

<script>
</script>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmPOBulkReceive.do?method=loadBatchshipment">
<html:hidden property="receivedDt" name="frmPOBulkReceive"/>
<html:hidden property="status" name="frmPOBulkReceive"/>
<html:hidden property="packSlipId" name="frmPOBulkReceive"/>
<html:hidden property="donorNum" name="frmPOBulkReceive"/>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		
		<tr class="Shade">
		<fmtLoadShipmentInfoInclude:message key="LBL_DATE_RECEIVED" var = "varDateReceived"/>
		<fmtLoadShipmentInfoInclude:message key="LBL_STATUS" var = "varStatus"/>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="20%"><gmjsp:label type="RegularText" SFLblControlName="${varDateReceived}" td="false" /></td>
			<td width="30%">&nbsp;<bean:write name="frmPOBulkReceive" property="receivedDt"/></td>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="20%"><gmjsp:label type="RegularText" SFLblControlName="${varStatus}" td="false" /></td>
			<td width="30%">&nbsp;<bean:write name="frmPOBulkReceive" property="status"/></td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr> 	
		<tr>
		<fmtLoadShipmentInfoInclude:message key="LBL_PACKING_SLIP_ID" var = "varPackingSlipId"/>
		<fmtLoadShipmentInfoInclude:message key="LBL_DONOR" var = "varDonor"/>
			<td class="RightTableCaption"  align="right" HEIGHT="25"><gmjsp:label type="RegularText" SFLblControlName="${varPackingSlipId}" td="false" /></td>
			<td>&nbsp;<bean:write name="frmPOBulkReceive" property="packSlipId"/></td>
			<td class="RightTableCaption"  align="right" HEIGHT="25"><gmjsp:label type="RegularText" SFLblControlName="${varDonor}" td="false" /></td>
			<td>&nbsp;<bean:write name="frmPOBulkReceive" property="donorNum"/></td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr> 	
		<tr class="Shade">
			<fmtLoadShipmentInfoInclude:message key="LBL_COMMENTS" var = "varComments"/>
			<fmtLoadShipmentInfoInclude:message key="LBL_MSG" var = "varMessage"/>
			<td class="RightTableCaption"  align="right" HEIGHT="25"><gmjsp:label type="RegularText" SFLblControlName="${varComments}" td="false" /></td>
			<td colspan="3"> &nbsp;<bean:write name="frmPOBulkReceive" property="rejRSId"/>
			</td>
		</tr>	  

	</table>
	
<%@ include file="/common/GmFooter.inc"%>
</html:form>
</BODY>
</HTML>