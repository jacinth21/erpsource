
 <%
/**********************************************************************************
 * File		 		: GmVendorSetup.jsp
 * Desc		 		: This screen is used for the Vendor Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<!--operations\GmVendorSetup.jsp  -->
<%@ taglib prefix="fmtVendorSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtVendorSetup:setLocale value="<%=strLocale%>"/>
<fmtVendorSetup:setBundle basename="properties.labels.operations.GmVendorSetup"/>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%
String strVendorCostTypeAccess = GmCommonClass.parseNull((String)request.getAttribute("strAccesFlag"));
String strButtonAccess = GmCommonClass.parseNull((String)request.getAttribute("hstrSetupAccessflag"));   /*PMT-26730-->System Manager Changes*/
String strVendorAccess = GmCommonClass.parseNull((String)request.getAttribute("hstrVendorAccessFlag")); /*PMT-35869-->Enable Vendor Portal Access to Vendor*/
String strOperationsJsPath = GmCommonClass.parseNull((String)GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS"));
String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_SETUP");
try {
	response.setCharacterEncoding("UTF-8");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	String strhAction = "Add";
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	ArrayList alStatus = new ArrayList();
	
	ArrayList alState = new ArrayList();
	ArrayList alCountry = new ArrayList();
	ArrayList alPayment = new ArrayList();
	ArrayList alCurrency = new ArrayList();
	ArrayList alVendorList = new ArrayList();
	ArrayList alCostType = new ArrayList();
	ArrayList alPurchasingAgent = new ArrayList();
	
	String strVendorId = GmCommonClass.parseNull((String)request.getAttribute("hId"));

	//out.println("strVendorId ** "+ strVendorId);
	String strVendorNm = "";
	String strVendorShNm = "";
	String strLotCode = "";
	String strStatus = "";
	String strCostType="";
	String strContactPerson = "";
	String strAddress1 = "";
	String strAddress2 = "";
	String strCity = "";
	String strState = "";
	String strCountry = "";
	String strPinCode = "";
	String strPayment = "";
	String strPhone = "";
	String strFax = "";
	String strActiveFl = "N";
	String strCurrency = "";

	String strSec199Fl = "Y";
    String strSec199Checked = "";
    String strPercentage = "" ;
    String strRMInvoiceFl = "Y";
    String strRMInvoiceChecked = "";
    
    String strVPAccessFl = "";
    String strVPAccessChecked = "";
    String strAutoPublishFl = "";
    String strAutoPublishChecked = "";
    String strSelectedVendorId="";
    
    String strPurAgent = "";
    String strEmail = "";
   
	if (hmReturn != null)
	{
		
		alStatus = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("STATUS"));
		alState = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("STATE"));
		alCountry = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("COUNTRY"));
		alPayment = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PAYMENT"));
		alCurrency = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("CURRENCY"));
		alVendorList = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("VENDORLIST"));
		alCostType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("COSTTYPE"));
		alPurchasingAgent = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("PURCHASINGAGENT"));/*PMT-50795-->Capture Purchasing Agent - Vendor Setup screen*/
		
	}
	
	HashMap hmVendorDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("VENDORDETAILS"));

	if (!hmVendorDetails.isEmpty())
	{
		strVendorNm = GmCommonClass.parseNull((String)hmVendorDetails.get("VNAME"));
		strVendorShNm = GmCommonClass.parseNull((String)hmVendorDetails.get("VSHNAME"));
		strLotCode = GmCommonClass.parseNull((String)hmVendorDetails.get("LCODE"));
		strStatus = GmCommonClass.parseNull((String)hmVendorDetails.get("STATUS"));
		strCurrency = GmCommonClass.parseNull((String)hmVendorDetails.get("CURR"));
		strContactPerson = GmCommonClass.parseNull((String)hmVendorDetails.get("CPERSON"));
		strAddress1 = GmCommonClass.parseNull((String)hmVendorDetails.get("ADD1"));
		strAddress2 = GmCommonClass.parseNull((String)hmVendorDetails.get("ADD2"));
		strCity = GmCommonClass.parseNull((String)hmVendorDetails.get("CITY"));
		strState = GmCommonClass.parseNull((String)hmVendorDetails.get("STATE"));
		strCountry = GmCommonClass.parseNull((String)hmVendorDetails.get("COUNTRY"));
		strPinCode = GmCommonClass.parseNull((String)hmVendorDetails.get("ZIP"));
		strPayment = GmCommonClass.parseNull((String)hmVendorDetails.get("PAYMENT"));
		strPhone = GmCommonClass.parseNull((String)hmVendorDetails.get("PHONE"));
		strFax = GmCommonClass.parseNull((String)hmVendorDetails.get("FAX"));
		strCostType = GmCommonClass.parseZero((String)hmVendorDetails.get("COSTTYPE"));
		strActiveFl = GmCommonClass.parseNull((String)hmVendorDetails.get("AFLAG"));
	    strVPAccessFl = GmCommonClass.parseNull((String)hmVendorDetails.get("VENDORPORTALMAP"));
	    strAutoPublishFl = GmCommonClass.parseNull((String)hmVendorDetails.get("AUTOPUBLISHFL"));
	    strSelectedVendorId = GmCommonClass.parseNull((String)hmVendorDetails.get("VENDORID"));
		strChecked = strActiveFl.equals("N")?"checked":"";
		strSec199Fl = GmCommonClass.parseNull((String)hmVendorDetails.get("SEC199"));
		strSec199Checked = strSec199Fl.equals("Y")?"checked":"";
		strPercentage = GmCommonClass.parseNull((String)hmVendorDetails.get("SPLITPERCENT"));
		strSec199Fl  = GmCommonClass.parseNull((String)hmVendorDetails.get("RMINVOICE"));
		strRMInvoiceChecked = strSec199Fl.equals("Y")?"checked":"";
	    strVPAccessChecked = strVPAccessFl.equals("Y")?"checked":"";
	    strAutoPublishChecked = strAutoPublishFl.equals("Y")?"checked":"";
		strhAction = "Edit";
		strPurAgent = GmCommonClass.parseNull((String)hmVendorDetails.get("PUR_AGENT"));/*PMT-50795-->Capture Purchasing Agent - Vendor Setup screen*/
	    strEmail = GmCommonClass.parseNull((String)hmVendorDetails.get("EMAIL"));
	}

	int intSize = 0;
	HashMap hcboVal = null;
	// to disable/enable the vendor cost type drop down.
	String strDisable = "";
    String strVendorBtnAcc = "true";
	strVendorCostTypeAccess = strVendorCostTypeAccess.equals("Y")?"":"disabled=disabled";
	if (strCostType.equals("0") || strCostType.equals("")){
	  strVendorCostTypeAccess = ""; 
	}
	  /*PMT-26730-->System Manager Changes*/
	if(!strButtonAccess.equals("Y")){
		strDisable = "true";
	} 
	/*PMT-35869-->Enable Vendor Portal Access to Vendor*/
	if(strVendorAccess.equals("Y") && strVPAccessFl.equals("Y")){
		strVendorBtnAcc = "false";
	}
	 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Vendor Setup </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmVendorSetup.js"></script>

<script>
var lblSplitPercent='<fmtVendorSetup:message key="LBL_SPLIT_PERCENT"/>';
var lblVendorName='<fmtVendorSetup:message key="LBL_VEN_NAME"/>';
var lblShortName='<fmtVendorSetup:message key="LBL_SHORT_NAME"/>';
var lblLotCode='<fmtVendorSetup:message key="LBL_LOT_CODE"/>';
var lblContactPerson='<fmtVendorSetup:message key="LBL_CONTACT_PERSON"/>';
var lblPhone='<fmtVendorSetup:message key="LBL_PHONE"/>';
var lblFax='<fmtVendorSetup:message key="LBL_FAX"/>';
var lblCurrency='<fmtVendorSetup:message key="LBL_CURRENCY"/>';
var lblStatus='<fmtVendorSetup:message key="LBL_STATUS"/>';
var lblComments='<fmtVendorSetup:message key="LBL_COMMENTS"/>';
var lblCostType='<fmtVendorSetup:message key="LBL_VENDOR_TYPE"/>';
var lblPurchasingAgent='<fmtVendorSetup:message key="LBL_PURCHASING_AGENT"/>';/*PMT-50795-->Capture Purchasing Agent - Vendor Setup screen*/
var lblEmail='<fmtVendorSetup:message key="LBL_EMAIL"/>';
</script>

</HEAD>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<BODY leftmargin="20" topmargin="10" onLoad="fnEnableSEC99(); fnAutoPublish();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmVendorServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hId" value="<%=strVendorId%>">
<input type="hidden" name="hstrSetupAccessflag" value="<%=strButtonAccess%>">
<input type="hidden" name="hstrVendorAccessFlag" value="<%=strVendorAccess%>">
<input type="hidden" name="strSelectedVendorId" value="<%=strSelectedVendorId%>">
 
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td width="598" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td height="25" class="RightDashBoardHeader">&nbsp;<fmtVendorSetup:message key="LBL_VEN_SETUP"/></td>
						<td align="right" class=RightDashBoardHeader >
						<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
						</td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_VEN_LIST"/>:</td>
						<td width="320">&nbsp;<select name="Cbo_Vendor" onChange="javascript:fnReload(this.value)" class="RightText" tabindex="1" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%
			  		intSize = alVendorList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = GmCommonClass.parseNullHashMap((HashMap)alVendorList.get(i));
			  			strCodeID = (GmCommonClass.parseNull((String)hcboVal.get("VID")));
						strSelected = strVendorId.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("VNAME")%></option>
<%
			  		}
%>

						</select></td>
					</tr>				
					<tr>
						<td class="RightTextBlue" colspan="2" align="center">
						<fmtVendorSetup:message key="LBL_TO_EDIT"/>
					</td>
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>						
					<tr class="Shade">
						<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtVendorSetup:message key="LBL_VEN_NAME"/>:</td>
						<td>&nbsp;<input type="text" size="35" value="<%=strVendorNm%>" name="Txt_VendNm" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2></td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtVendorSetup:message key="LBL_SHORT_NAME"/>:</td>
						<td class="RightTableCaption">&nbsp;<input type="text" size="15" value="<%=strVendorShNm%>" name="Txt_VendShNm" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>&nbsp;&nbsp;&nbsp;<font color="red">*</font>&nbsp;<fmtVendorSetup:message key="LBL_LOT_CODE_NAME"/>:&nbsp;<input type="text" maxlength=2 size="2" value="<%=strLotCode%>" name="Txt_LotCode" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=4></td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtVendorSetup:message key="LBL_CURRENCY"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Curr"  seletedValue="<%= strCurrency %>"	
							tabIndex="5"  width="100" value="<%= alCurrency%>" codeId = "CODEID"  codeName = "CODENMALT"  defaultValue= "[Choose One]" /></td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*&nbsp;</font><fmtVendorSetup:message key="LBL_CONTACT_PERSON"/>:</td>
						<td>&nbsp;<input type="text" size="30" value="<%=strContactPerson%>" name="Txt_ContPer" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=6></td>
					</tr>
					
					<tr><td class="LLine" colspan="2"></td></tr>						
					<tr class="Shade">
						<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtVendorSetup:message key="LBL_EMAIL"/>:</td>
						<td>&nbsp;<input type="text" size="35" value="<%=strEmail%>" name="Txt_Email" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=6></td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
				
								<tr>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_ADDRE1"/>:</td>
									<td width="478">&nbsp;<input type="text" size="30" value="<%=strAddress1%>" name="Txt_Add1" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=7></td>
								</tr>
								<tr><td class="LLine" colspan="2"></td></tr>
								<tr class="Shade">
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_ADDRE2"/>:</td>
									<td>&nbsp;<input type="text" size="30" value="<%=strAddress2%>" name="Txt_Add2" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=8></td>
								</tr>
								<tr><td class="LLine" colspan="2"></td></tr>
								<tr>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_CITY"/>:</td>
									<td>&nbsp;<input type="text" size="23" value="<%=strCity%>" name="Txt_City" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=9></td>
								</tr>
								<tr><td class="LLine" colspan="2"></td></tr>
								<tr class="Shade">
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_STATE"/>:</td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_State"  seletedValue="<%= strState %>"
							tabIndex="10"  width="150" value="<%= alState%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
								</tr>
								<tr><td class="LLine" colspan="2"></td></tr>
								<tr>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_COUNTRY"/>:</td>
									<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Country"  seletedValue="<%= strCountry %>"
							tabIndex="11"  width="150" value="<%= alCountry%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
									</td>
								</tr>
								<tr><td class="LLine" colspan="2"></td></tr>
								<tr class="Shade">
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_ZIP_CODE"/>:</td>
									<td>&nbsp;<input type="text" size="10" value="<%=strPinCode%>" name="Txt_PinCode" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=12></td>
					
					</tr>
					<tr><td class="Line" colspan="2"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_PAY_TERM"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Payment"  seletedValue="<%= strPayment %>"
							tabIndex="13"  width="150" value="<%= alPayment%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*&nbsp;</font><fmtVendorSetup:message key="LBL_PHONE"/>:</td>
						<td>&nbsp;<input type="text" size="20" value="<%=strPhone%>" name="Txt_Phn" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=14></td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*&nbsp;</font><fmtVendorSetup:message key="LBL_FAX"/>:</td>
						<td>&nbsp;<input type="text" size="20" value="<%=strFax%>" name="Txt_Fax" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=15></td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
				 	<tr class="Shade">
						<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtVendorSetup:message key="LBL_STATUS"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Status"  seletedValue="<%= strStatus %>"
							tabIndex="16"  width="150" value="<%= alStatus%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
					</tr> 
				
					   <tr class="Shade"><td class="LLine" colspan="2"></td></tr>
					   
					  	<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*&nbsp;</font><fmtVendorSetup:message key="LBL_VENDOR_TYPE"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="vendor_Type"  seletedValue="<%= strCostType %>"
							tabIndex="16"  width="150" value="<%= alCostType%>" codeId = "CODEID" codeName = "CODENM" disabled="<%=strVendorCostTypeAccess %>"  defaultValue= "[Choose One]" /></td>
					    </tr> 						
				
					  <tr class="Shade"><td class="LLine" colspan="2"></td></tr>
					<tr class="Shade">
					
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_SEC"/>?:</td>
						<td class="RightTableCaption"><input type="checkbox" size="30" <%=strSec199Checked%> name="Chk_Sec199Fl" value="<%=strSec199Fl%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onClick="fnEnableSEC99();" tabindex=17>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtVendorSetup:message key="LBL_RAW_MATERIAL_SPLIT"/>:&nbsp;<input type="text" size="3" value="<%=strPercentage%>" name="Txt_percentage" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=18>%</td>
					</tr>
					<tr >
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_VEN_ACCESS"/>?:</td>
                        <td><input type="checkbox" size="30" <%=strVPAccessChecked%> name="Chk_VPAccessFl" value="<%=strVPAccessFl%>" onClick="fnAutoPublish();" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >                </td></tr>
                    <tr class="Shade">
                    <td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_AUTO_PUBLISH"/>?:</td>
						<td><input type="checkbox" size="30" <%=strAutoPublishChecked%> name="Chk_AutoPublishFl" value="<%=strAutoPublishFl%>" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >				</td></tr>
					<tr >
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_RAW_MAT"/>?:</td>
						<td><input type="checkbox" size="30" <%=strRMInvoiceChecked%> name="Chk_RMinvoiceFl" value="<%=strRMInvoiceFl%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >				</td></tr>
			
			       
					
					
					<tr><td class="LLine" colspan="2"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorSetup:message key="LBL_INACTIVE"/> ?:</td>
						<td><input type="checkbox" size="30" <%=strChecked%> name="Chk_ActiveFl" value="<%=strActiveFl%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  ></td>
					</tr>
					<tr><td class="LLine" colspan="2"></td></tr>
				 	<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtVendorSetup:message key="LBL_PURCHASING_AGENT"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Purchasing_agent"  seletedValue="<%=strPurAgent%>"
							width="150" value="<%=alPurchasingAgent%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" /></td>
					</tr> 
							<tr>
						<td colspan="2"> 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							   
								<jsp:param name="LogType" value="" />
								<jsp:param name="LogMode" value="Edit" />
								<jsp:param name="Mandatory" value="yes"/>
							
								
								
								
							</jsp:include>
						</td>
					</tr>					
				   <tr>
						<td  height="30" colspan="2" align="center">&nbsp;
						<%-- <gmjsp:button value="Address" gmClass="button" onClick="fnOpenAddress();" tabindex="19" buttonType="Save" />&nbsp;&nbsp;&nbsp;--%>
						<fmtVendorSetup:message key="BTN_MAPPING" var="varMapping"/><gmjsp:button value="${varMapping}" gmClass="button" onClick="fnOpenVendorMap();" tabindex="20" buttonType="Save" />&nbsp;&nbsp;&nbsp;
						<%--  /*PMT-26730-->System Manager Changes*/--%>
						<fmtVendorSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}"  disabled="<%=strDisable%>"  gmClass="button" onClick="fnSubmit();" tabindex="21" buttonType="Save" />&nbsp;
						<%--  /*PMT-35869-->Enable Vendor Portal Access to Vendor*/--%>
						<fmtVendorSetup:message key="BTN_VENDOR" var="varVendor"/><gmjsp:button value="${varVendor}" disabled="<%=strVendorBtnAcc%>" gmClass="button" onClick="fnOpenVendorPortalMap();" tabindex="21" buttonType="Save"  />&nbsp;
						</td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>		
    </table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
