 <%
/**********************************************************************************
 * File		 		: GmDHRPrint.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.servlets.GmCommonBarCodeServlet"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page import ="org.apache.commons.beanutils.BasicDynaBean"%> 
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ taglib prefix="fmtDHRPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRPrint.jsp -->

<fmtDHRPrint:setLocale value="<%=strLocale%>"/>
<fmtDHRPrint:setBundle basename="properties.labels.operations.GmDHRPrint"/>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);	
	
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	RowSetDynaClass rsBackOrderFlag =(RowSetDynaClass)  request.getAttribute("rsBackOrderFlag");
	ArrayList alList = (ArrayList)rsBackOrderFlag.getRows();

	String strFifteenDaysTrendQty ="0";
	String strShelfQty = "0";
	HashMap hmResultTrendReport = new HashMap();
	hmResultTrendReport = (HashMap)request.getAttribute("hmResultTrendReport");
	if(hmResultTrendReport!= null){
		HashMap hmMain = (HashMap)hmResultTrendReport.get("Main");
		ArrayList alDetails = (ArrayList)hmMain.get("Details");
	    if(alDetails.size() > 0){
    		HashMap hmDetails = (HashMap)alDetails.get(0);
    		strFifteenDaysTrendQty=GmCommonClass.parseZero((String)hmDetails.get("1 - 15"));
    		strShelfQty=GmCommonClass.parseZero((String)hmDetails.get("Shelf")); 
        }
   	}
	
    String strBackOrderSales = "0";
    String strBackOrderSets = "0";
    String strBackOrderItem = "0";
    if (alList.size() > 0)
	{
		BasicDynaBean basicBean = (BasicDynaBean)alList.get(0);

		strBackOrderSales= basicBean.get("SALESCOUNT").toString();
		strBackOrderSets= basicBean.get("SETITEMCOUNT").toString();
		strBackOrderItem= basicBean.get("ITEMCOUNT").toString();
	}

	HashMap hmDHRDetails = new HashMap();
	ArrayList alSubDHR = new ArrayList();
	ArrayList alLotNums = new ArrayList();

	String strShade = "";
    String strPTFooter="";
    String strPTFileName="";
	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyRej = "";
	String strManufDate = "";
	String strUserName = "";
	String strCreatedDate = "";
	String strQtyOrdered = "";
	String strProjId = "";
	String strPackPrim = "";
	String strPackSec = "";
	String strLabel = "";
	String strInsert = "";
	String strSubPartNum = "";
	String strSubPartDesc = "";
	String strSubControl = "";
	String strSubManfDt = "";
	String strSterFl = "";
	String strWoType = "";
	String strUOM = "";
	String strDHRVendorName = "";
	String strReceivedDate = "";
	String strSkipPackFlg ="";
	String strRmUpdateFl="";	
	boolean blQty = false;
	String strFlgChk = "";
	String strDHRNote="";
	String strNewInsert ="";
	String strINSFooter ="";
	String strFooter ="";
	String strVALDFL = "";
	String strValidFlAlertContent = "";
	String strDocRev = "";
	String strDocActiveFl = "";
	String strWOJSP = "";
	String strDRFooter="";
	String strDRFileName="";
	String strPackFileName = "";
	String strApplnDateFmt = strGCompDateFmt;
	GmResourceBundleBean gmResourceBundleBean = null;
	String strCompanyLogo = GmCommonClass.parseNull((String) hmReturn.get("LOGO"));
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.operations.GmDHRPrint", strSessCompanyLocale);
	
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		gmResourceBundleBean = (GmResourceBundleBean) hmReturn.get("gmResourceBundleBean");
		alLotNums = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("LOTLIST"));

		strDHRId = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId = GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName = GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		//strManufDate = GmCommonClass.parseNull((String)hmDHRDetails.get("MDATE"));
		strManufDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("MDATE"),strApplnDateFmt);
		strUserName = GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		//strCreatedDate = GmCommonClass.parseNull((String)hmDHRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplnDateFmt);
		strQtyOrdered = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYORD"));
		strProjId = GmCommonClass.parseNull((String)hmDHRDetails.get("PROJID"));
		strPackPrim = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKPRIM"));
		strPackSec = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKSEC"));
		strInsert = GmCommonClass.parseNull((String)hmDHRDetails.get("INSID"));
		strQtyRej = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ"));
		strLabel = strPartNum.replaceAll("\\.","");
		strSterFl = GmCommonClass.parseNull((String)hmDHRDetails.get("STERFL"));
		strWoType = GmCommonClass.parseNull((String)hmDHRDetails.get("WOTYPE"));
		strUOM = GmCommonClass.parseNull((String)hmDHRDetails.get("UOMANDQTY"));
		strDHRVendorName = GmCommonClass.parseNull((String)hmDHRDetails.get("DHRVENDORNAME"));
		//strReceivedDate = GmCommonClass.parseNull((String)hmDHRDetails.get("RECEIVEDDATE"));
		strReceivedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("RECEIVEDDATE"),strApplnDateFmt);
		strSkipPackFlg = GmCommonClass.parseNull((String)hmDHRDetails.get("SKIPPACKFLG"));
		strRmUpdateFl = GmCommonClass.parseNull((String)hmDHRDetails.get("RMUPDATEFL"));
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
		blQty = !strQtyOrdered.equals(strQtyRec)?true:false;
		strFlgChk = GmCommonClass.parseNull((String)hmDHRDetails.get("FLGPNUMCHK"));
		strPTFooter = GmCommonClass.parseNull ((String)hmDHRDetails.get("PTFOOTER"));
		strPTFileName = GmCommonClass.parseNull ((String)hmDHRDetails.get("PTFILENAME"));
		strPackFileName = GmCommonClass.parseNull ((String)hmDHRDetails.get("LPFILENAME"));
		strDHRNote = GmCommonClass.parseNull ((String)hmDHRDetails.get("DHRNOTE"));
		strINSFooter = GmCommonClass.parseNull ((String)hmDHRDetails.get("INSFOOTER"));
		strVALDFL = GmCommonClass.parseNull ((String)hmDHRDetails.get("VALDFL"));
		strDRFooter = GmCommonClass.parseNull ((String)hmDHRDetails.get("DRFOOTER"));
		strDRFileName = GmCommonClass.parseNull ((String)hmDHRDetails.get("DRFILENAME"));
	}
	
	HashMap hmWoDetails = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturnWODetails"));
	hmWoDetails = GmCommonClass.parseNullHashMap((HashMap)hmWoDetails.get("WODETAILS"));
	String strF = GmCommonClass.parseNull((String) hmWoDetails.get("FOOTER"));
	int intWoSize = hmWoDetails.size();
	int intSize = 0;
	int intLotNumSize = alLotNums.size();
	String strImg = "s_old_logo.gif";
	int intWidth = 170;
	HashMap hcboVal = null;
	if(!strINSFooter.equals("")){
		 strNewInsert  ="/ GI001A";
		 String strArr[] = strINSFooter.split("\\^");
		 strFooter = strArr[0];
		 strImg ="s_logo1.png";
		 intWidth =270;
		}
	if(strVALDFL.equals("Y")){
		strValidFlAlertContent = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("DHR.GMDHRVALIDFLALERT"));
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Print Version </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmDHRPrint.js"></script>

</HEAD>

<BODY leftmargin="20" topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hPOId" value="<%=strDHRId%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="hAction" value="">

<% if (!strWoType.equals("3103") && !strWoType.equals("3104"))
{
%>
<BR>
<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
	  	<tr>
			<td bgcolor="#666666" rowspan="20" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td bgcolor="#666666" colspan="3" height="1"></td>
			<td bgcolor="#666666" rowspan="20" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td align="right" class="RightText"><font size=+2><fmtDHRPrint:message key="LBL_DHR"/>&nbsp;<fmtDHRPrint:message key="LBL_COVER_SHEET"/>&nbsp;</font></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="3"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="3"></td></tr>
		<tr><td bgcolor="#666666" colspan="3"></td></tr>
		<tr><td class="RightTableCaption" colspan="3" height="6" align="left"></td></tr>
		<tr>
			<td class="RightTableCaption" colspan="2">&nbsp;&nbsp;<font size=+1><%=strDHRId%></font></td>
			<td class="RightText" align="right"><img src='/GmCommonBarCodeServlet?ID=<%=strDHRId%>&type=2D' height="50" width="50" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td align="right">&nbsp;&nbsp;</td>
		</tr>
		<%--<tr><td>&nbsp;</td></tr>
		 <tr>
		<td colspan="3"><img align="right" src='/GmCommonBarCodeServlet?ID=<%=strDHRId%>&type=2d' height="50" width="50" /></td>
		</tr>--%>
		<tr><td class="RightTableCaption"  height="6" align="left"></td></tr>
		<tr width="100%">
			<td width="100%" colspan="3">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr height="25" class="shade">		
						<td class="RightTableCaption"  height="25" valign="middle">&nbsp;<fmtDHRPrint:message key="LBL_DETAILS"/>:</td>
						<td class="RightTableCaption" colspan="3" >&nbsp;</td>
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr height="25">		
						<td class="RightTableCaption" align="right" height="25" valign="middle"><fmtDHRPrint:message key="LBL_PART_NUMBER"/>:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightTableCaption" align="right" height="25" valign="middle"><fmtDHRPrint:message key="LBL_RECEIVED_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strReceivedDate%></td>
					</tr>
					<tr class="shade">
						<td class="RightTableCaption" height="25" align="right"><fmtDHRPrint:message key="LBL_DESCRIPTION"/>:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
						<td class="RightText" colspan="3" >&nbsp;</td>
					</tr>
					<tr>
						<td class="RightTableCaption" align="right" height="25" valign="middle"><fmtDHRPrint:message key="LBL_QTY_RECIEVED"/>:</td>
						<td class="RightText">&nbsp;<%=strQtyRec%></td>
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b><fmtDHRPrint:message key="LBL_ALERTS"/>:</b></td></tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
					<td class="RightTableCaption" height="100" align="left" colspan="6"><font size=+1>
<%
					int intFifteenDaysTrendQty = new Double(strFifteenDaysTrendQty).intValue();
					//int intShelfQty = Integer.parseInt(strShelfQty);
					double dblShelfQty = Double.parseDouble(strShelfQty); // It is getting the decimal values when we have UOM Case in purchase order
					int intSalesCount = Integer.parseInt(strBackOrderSales);
					int intSetsCount = Integer.parseInt(strBackOrderSets);
					int intItemCount = Integer.parseInt(strBackOrderItem);
					if (intSalesCount > 0){%>
						* BACK ORDER - SALES<BR>
					<%}%>	

					<%	if (intSetsCount>0){%>
						* BACK ORDER - ITEMS FROM SHIPPED SETS<BR>
					<%} %>

					<%	if (intItemCount>0){%>
						* BACK ORDER - ITEMS<BR>
					<%} %> 
					<%	if (strDHRVendorName.equals("Y")){%>
						* PROCESS FIRST - ACCOUNTING <BR>
					<%} %>
					<% if(strWoType.equals("3101")) //3101 Rework
				    {%>
				       * RE-WORK PO, CHECK FOR SPECIAL PROCESSING   <BR>
				    <%}%>
				    <% if(strSkipPackFlg.equals("Y")) 
				    {%>
				       * DO NOT PACKAGE/LABEL<BR>
				    <%}%>
				    <% if(strRmUpdateFl.equals("Y")) 
				    {%>
				       * PARTS TO BE DELIVERED TO RM STORE<BR>
				    <%}%>
				    <%if(intFifteenDaysTrendQty <= 0 )
				    { %>
				       * LOW SHELF STOCK<BR>
				    <%}%>
				    <%if(dblShelfQty == 0){ %>
				       * ZERO SHELF <BR>
				    <%}%>
				     <% if(strVALDFL.equals("Y")) 
				    {%>
				       **** <%=strValidFlAlertContent%> ****<BR>
				    <%}%>
						</font>
					</td> 
						
					</tr>
					
					
					
					
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b><fmtDHRPrint:message key="LBL_RECIEVING_NOTES"/>:</b></td></tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr><td  colspan="6"> <%=strDHRNote %>  </td></tr>
					<tr>
						<td bgcolor="#eeeeee" colspan="6">
							<jsp:include page="/common/GmRuleDisplayInclude.jsp">
							<jsp:param name="Show" value="true" />
							<jsp:param name="Fonts" value="true" /></jsp:include>
						</td>
					</tr>
					<tr>
						<td class="RightText" height="120" align="right"></td>
						
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b><fmtDHRPrint:message key="LBL_COMMENTS"/>:</b></td></tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td class="RightTableCaption" height="100" align="left" colspan="6">
						<%if(strFlgChk.equals("Y")){ 
							GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PARTTOBE_STERILIZED"));
						} %>
						</td>
						
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
				</table>
			</td>
		</tr>
	</table>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td class="RightText" bgcolor="#eeeeee" height="20" align="center"><fmtDHRPrint:message key="LBL_INTERNAL_USE_NOT_DHR"/></td>
		</tr>
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>		
	</table>
<%} %>
<!-- end off add new page -->
	<p STYLE="page-break-after: always"></p>
	<BR>
	<BR>
<%	if (!strPTFileName.equals(""))
{ 
	strPTFileName = "/operations/"+strPTFileName;
%>
				<jsp:include page="<%=strPTFileName%>" >
				<jsp:param name="HideButton" value="Y"/>
				<jsp:param name="CompanyLogo" value="<%=strCompanyLogo %>"/>
				</jsp:include>

<%}else
{ %>
              <jsp:include page="/GmDHRPrintProductTravelerD.jsp" >
              <jsp:param name="HideButton" value="Y"/>
              <jsp:param name="CompanyLogo" value="<%=strCompanyLogo %>"/>
			  </jsp:include>

<%} 
%>
<!-- end off Product Traveler page -->
<% if (!strWoType.equals("3103") && !strWoType.equals("3104"))
{
%>

	<p STYLE="page-break-after: always"></p>
	<BR>
	<BR>
	
	<%	if (!strPackFileName.equals(""))
		{ 
			strPackFileName = "/operations/"+strPackFileName;
		%>
						<jsp:include page="<%=strPackFileName%>" >
						<jsp:param name="HideButton" value="Y"/>
						<jsp:param name="CompanyLogo" value="<%=strCompanyLogo %>"/>
						</jsp:include>
		
		<%}else
		{ %>
		              <jsp:include page="/GmLabelPackagingE.jsp" >
		              <jsp:param name="HideButton" value="Y"/>
		              <jsp:param name="CompanyLogo" value="<%=strCompanyLogo %>"/>
					  </jsp:include>
		
		<%} 
		%>
	
<!-- end off Labeling/Packaging page -->

<p STYLE="page-break-after: always"></p>
<BR>
<BR>
<%	if (!strDRFileName.equals(""))
{ 
	strDRFileName = "/operations/"+strDRFileName;
%>
				<jsp:include page="<%=strDRFileName%>" >
				<jsp:param name="HideButton" value="Y"/>
				<jsp:param name="CompanyLogo" value="<%=strCompanyLogo %>"/>
				</jsp:include>

<%}else
{ %>
              <jsp:include page="/operations/GmDHRPrintReviewFormC.jsp" >
              <jsp:param name="HideButton" value="Y"/>
              <jsp:param name="CompanyLogo" value="<%=strCompanyLogo %>"/>
			  </jsp:include>

<%} 
%>
<!-- end off Review Form page -->	
<%} 

// When the WO Details is not coming back, we should not include page break.
if (intWoSize>0){ 

	//check the Document Version and the Active Flag for the WO and launch the appropriate JSP based on that. 
	strFooter = GmCommonClass.parseNull((String) hmWoDetails.get("FOOTER"));
	String strArr[] = strFooter.split("\\^");
	strFooter = strArr[0];
	strDocRev = strArr[1];
	strDocActiveFl = strArr[2];
	
	//If its the active Document, no need to apped the revision, as we have to launch the latest WO. 
	if (strDocActiveFl.equals("Y"))
			strDocRev="";
	
	strWOJSP = "/operations/GmWOPrint".concat(strDocRev).concat(".jsp");
	
%>
 <!-- end of  Device History Record page  -->

	<p STYLE="page-break-after: always"></p>
	
				<jsp:include page="<%=strWOJSP%>" >
				<jsp:param name="PrintDHR" value="Y"/>
				<jsp:param name="CompanyLogo" value="<%=strCompanyLogo %>"/>
				</jsp:include>
		<%	
} %>


<% 
if (intLotNumSize>0){ 
	%> 
	<!-- The code is moved inside the if statement to avoid printing extra blank page on DHR print, changes added for PMT-6681 -->
<p STYLE="page-break-after: always"></p>
<BR>
<BR>
	  <jsp:include page="/operations/GmDHRLotListing.jsp" >
              <jsp:param name="HideButton" value="Y"/>
              <jsp:param name="CompanyLogo" value="<%=strCompanyLogo %>"/>
			  </jsp:include>
<%} %>		  
	<BR>
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" id="button">
			<fmtDHRPrint:message key="BTN_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtDHRPrint:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close()"  buttonType="Load" />&nbsp;&nbsp;			
			</td>
		<tr>
	</table>
</FORM>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
