 <%
/**********************************************************************************
 * File		 		: GmPartNumInvStatus.jsp
 * Desc		 		: This screen is used for displaying inventory status for 
 					  a part as well as for Manual adjustments
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtPartNumInvStatus" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartNumInvStatus:setLocale value="<%=strLocale%>"/>
<fmtPartNumInvStatus:setBundle basename="properties.labels.operations.GmPartNumInvStatus"/>

<!-- operations\GmPartNumInvStatus.jsp -->
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmLoop = new HashMap();
	HashMap hcboVal = new HashMap();
	
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strPartNum = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	String strOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	
	String strPartDesc = "";
	String strQtyInStock = "";
	String strQtyInShelf = "";
	String strQtyInShelfAlloc = "";
	
	String strQtyInBulk = "";
	String strQtyInBulkAlloc = "";
	
	String strQtyInQuarantine = "";
	String strQtyInQuarantineAlloc = "";
	
	String strQtyInReturnsHold = "";
	String strQtyInPO = "";
	String strQtyInWIP = "";
	String strQtyInFA = "";
	String strQtyInFAWIP = "";

	String strProjName = "";

	String strShade = "";
	String strCodeID = "";
	String strSelected = "";
	int intLoop = 0;

	ArrayList alReport = new ArrayList();

	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("REPORT");
	}

	if (alReport != null)
	{
		intLoop = alReport.size();
	}

			if (intLoop > 0)
			{
				int intStock = 0;
				int intBulk = 0;
				int intReturns = 0;
				int intPO = 0;
				int intDHR = 0;
				
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);
					strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
					strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));

					strQtyInShelf = GmCommonClass.parseZero((String)hmLoop.get("INSHELF"));
					strQtyInShelfAlloc = GmCommonClass.parseZero((String)hmLoop.get("SHELFALLOC"));
					
					strQtyInQuarantine = GmCommonClass.parseZero((String)hmLoop.get("INQUARAN"));
					strQtyInQuarantineAlloc = GmCommonClass.parseZero((String)hmLoop.get("QUARALLOC"));
					
					strQtyInBulk = GmCommonClass.parseZero((String)hmLoop.get("INBULK"));
					strQtyInBulkAlloc = GmCommonClass.parseZero((String)hmLoop.get("BULKALLOC"));
					
					strQtyInReturnsHold = GmCommonClass.parseZero((String)hmLoop.get("INRETURNS"));
					
					strQtyInPO = GmCommonClass.parseZero((String)hmLoop.get("POPEND"));
					intPO = Integer.parseInt(strQtyInPO);
					strQtyInWIP = GmCommonClass.parseZero((String)hmLoop.get("POWIP"));
					intDHR = Integer.parseInt(strQtyInWIP);
					intPO = intPO - intDHR;
					strQtyInPO = ""+intPO;
				}
			}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Inventory Status</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnSubmit()
{
    if (document.frmAccount.Txt_LogReason.value == '')
    {
       Error_Details(message[51]);
    }
    if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}   
	document.frmAccount.hAction.value = "Adjust";
	fnStartProgress('Y');
	document.frmAccount.submit();
}

function fnLoad()
{
	document.frmAccount.hAction.value = "Go";
	fnStartProgress('Y');
	document.frmAccount.submit();
}

function fnViewDetails(pnum,act)
{
windowOpener("/GmPartInvReportServlet?hAction="+act+"&hPartNum="+encodeURIComponent(pnum),"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=670,height=200");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPartInvReportServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hOpt" value="<%=strOpt%>">

	<table border="0" width="650" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="20" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="20" width="1" class="Line"></td>
		</tr>
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">
				<fmtPartNumInvStatus:message key="LBL_REPORT"/>
			</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartNumInvStatus:message key="LBL_PART_NUM"/>:</td>
			<td width="450">&nbsp;
				<input type="text" size="10" value="<%=strPartNum%>" name="hPartNum" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
				&nbsp;&nbsp;<fmtPartNumInvStatus:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="button" onClick="fnLoad();" tabindex="26" buttonType="Load" />&nbsp;
			</td>
		</tr>
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartNumInvStatus:message key="LBL_DESC"/>:</td>
			<td width="350" class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>
		<tr>
			<td colspan="2">
				<table border="0" width="100%" bgcolor="#CCCCCC" cellspacing="1" cellpadding="0">
					<tr height="18" bgcolor="#eeeeee" class="RightTableCaption">
						<td align="center" colspan="2"><fmtPartNumInvStatus:message key="LBL_SHELF_INV"/></td>
						<td width="100" rowspan="10" bgcolor="white">&nbsp;</td>
						<td align="center" colspan="2"><fmtPartNumInvStatus:message key="LBL_BULK_INV"/></td>
					</tr>
					<tr height="16" bgcolor="white">
						<td align="right" width="100" class="RightText"><fmtPartNumInvStatus:message key="LBL_AVAIL"/></td>
						<td width="120" class="RightText">&nbsp;<%=strQtyInShelf%></td>
						<td align="right" width="100" class="RightText"><fmtPartNumInvStatus:message key="LBL_AVAIL"/></td>
						<td width="120" class="RightText">&nbsp;<%=strQtyInBulk%></td>
					</tr>
					<tr height="16" bgcolor="white">
						<td align="right" class="RightText"><fmtPartNumInvStatus:message key="LBL_ALLOC"/>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strQtyInShelfAlloc%></td>
						<td align="right" class="RightText"><fmtPartNumInvStatus:message key="LBL_ALLOC"/>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strQtyInBulkAlloc%></td>
					</tr>
					<tr height="18" bgcolor="#eeeeee" class="RightTableCaption">
						<td align="center" colspan="2"><fmtPartNumInvStatus:message key="LBL_QUN_INV"/></td>
						<td align="center" colspan="2"><fmtPartNumInvStatus:message key="LBL_RET_INV"/></td>
					</tr>
					<tr height="16" bgcolor="white">
						<td align="right" class="RightText"><fmtPartNumInvStatus:message key="LBL_AVAIL"/>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strQtyInQuarantine%></td>
						<td align="right" class="RightText"><fmtPartNumInvStatus:message key="LBL_AVAIL"/>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strQtyInReturnsHold%></td>
					</tr>
					<tr height="16" bgcolor="white">
						<td align="right" class="RightText"><fmtPartNumInvStatus:message key="LBL_ALLOC"/>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strQtyInQuarantineAlloc%></td>
						<td colspan="2">&nbsp;</td>
					</tr>
					<tr height="18" bgcolor="#eeeeee" class="RightTableCaption">
						<td align="center" colspan="2"><fmtPartNumInvStatus:message key="LBL_QTY_PEN"/></td>
						<td align="center" colspan="2"><fmtPartNumInvStatus:message key="LBL_QTY_WIP_DHR"/></td>
					</tr>
					<tr height="16" bgcolor="white">
						<td align="right" class="RightText"><fmtPartNumInvStatus:message key="LBL_AVAIL"/>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strQtyInPO%></td>
						<td align="right" class="RightText"><fmtPartNumInvStatus:message key="LBL_AVAIL"/>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strQtyInWIP%></td>
					</tr>															
				</table>
			</td>
		</tr>
<%
	if (strOpt.equals("Manual") && !strAction.equals("Load")){
%>
		<tr><td class="Line" colspan="2"></td></tr>
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartNumInvStatus:message key="LBL_QTY_ADJ"/>:</td>
			<td width="350" class="RightText">
				&nbsp;&nbsp;<input type="text" size="10" value="" name="Txt_QtyAdj" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
				&nbsp;&nbsp;<fmtPartNumInvStatus:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="26" buttonType="Save" />&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="LogType" value="PLOG" />
				</jsp:include>
			</td>
		</tr>
<%
	}
%>		
		
		<tr><td class="Line" colspan="2"></td></tr>
	</table>
		
</FORM>
<%@ include file="/common/GmFooter.inc" %>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
