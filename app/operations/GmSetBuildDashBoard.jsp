<!-- \operations\GmSetBuildDashBoard.jsp -->
<%
/**********************************************************************************
 * File		 		: GmSetBuildDashBoard.jsp
 * Desc		 		: This screen is used to display Dashboard Set building
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>

<%@ taglib prefix="fmtSetBuildDashBoard" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSetBuildDashBoard:setLocale value="<%=strLocale%>"/>
<fmtSetBuildDashBoard:setBundle basename="properties.labels.operations.GmSetBuildDashBoard"/>

<%
	GmServlet gm = new GmServlet();
		 
	ArrayList alDashStatus 	= new ArrayList();

	 
	String strStaInputs = "";
	String strSetID ="";
	String strConsignID="";
	String  str_ReprocessID="";
	 
	String strWikiTitle = GmCommonClass.getWikiTitle("DASHBOARD_SET_BUILDING");
	
		 	
	String strhAction =  GmCommonClass.parseNull((String)request.getAttribute("hAction"));	
		 
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	
	if(hmReturn !=null)
	 {
	    
		alDashStatus = (ArrayList) hmReturn.get("DASHSTATUS");
  
	   strStaInputs = GmCommonClass.parseNull((String) request.getParameter("staInputs"));
	   strSetID = GmCommonClass.parseNull((String)request.getParameter("setID"));   
	   strConsignID = GmCommonClass.parseNull((String)request.getParameter("consignID"));   
	   str_ReprocessID = GmCommonClass.parseNull((String)request.getParameter("reprocessID"));  
	   log.debug("str_Reprocess*******"+str_ReprocessID);
	 }

	  
	HashMap hmDashStatus = new HashMap();
	hmDashStatus.put("ID","");
	hmDashStatus.put("PID","CODENMALT");
	hmDashStatus.put("NM","CODENM");
	
	
String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	
	 
	ArrayList alIniSets = new ArrayList();
						
	int intLength = 0;
	
	alIniSets = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("INISETS"));
 
	intLength = alIniSets.size();
	

	HashMap hmLoop = new HashMap();
 							
	String strConId = "";
	String strSetName = "";
	String strSetId = "";
	String strPerson = "";
	String strDateInitiated = "";
	String strStatus = "";
	String strSource = "";
	String strShade = "";
	String strReqId = "";
	String strReqdDate ="";
	 
	String strFlag = "";
	 
	String strVeriFl = "";
	String strLine = "";
	 
	String strReprocessId = "";

	HashMap hmTempLoop = new HashMap();
	String strNextId = "";
	int intCount = 0;
	boolean blFlag = false;
	String strIniFlag = "";
	String strWIPFlag = "";
	String strTemp = "";
	boolean blFlag1 = false;
	 
	StringBuffer sbTemp = new StringBuffer();
			
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Dashboard - Set Building</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmSetBuildDashBoard.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> 
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnLoad();">
<FORM name="frmDashBoard" method="post" action = "<%=strServletPath%>/GmOperDashBoardServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hinputStr">
<input type="hidden" name="staInputs" value="<%=strStaInputs%>">
<input type="hidden" name="dStatus" value="<%=strStaInputs%>">
<input type="hidden" name="varRows" value="<%=intLength%>">
<input type="hidden" name="RE_FORWARD" >
<input type="hidden" name="RE_TXN" >
<input type="hidden" name="txnStatus" >
<input type="hidden" name="hChkCnt" >
<jsp:include page="/common/GmScreenError.jsp" />
<TABLE cellSpacing=0 cellPadding=0  border=0 class="DtTable900">
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1 colspan=3></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr class=Line >
	<td height=25 class=RightDashBoardHeader ><fmtSetBuildDashBoard:message key="LBL_DASHBOARD_-_BUILT_SET"/> </td>
			
			<td align="right" class=RightDashBoardHeader > 	
			<fmtSetBuildDashBoard:message key="IMG_ALT_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
	</tr>
	<tr class=Line><td noWrap height=1 colspan=3></td></tr>
					 
					<tr>
						<td><table><tr>
							<td class="RightText" nowrap align="right" HEIGHT="24"><b><fmtSetBuildDashBoard:message key="LBL_STATUS"/>:</b></td>
							<td><%=GmCommonControls.getChkBoxGroup("",alDashStatus,"DStatus",hmDashStatus)%></td>						
						</tr></table></td>	
					 
						<td><table><tr>
							<td class="RightText" nowrap align="right" HEIGHT="24"><b><fmtSetBuildDashBoard:message key="LBL_SET_ID"/>:</b></td>
							<td>&nbsp;<input type="text" size="15" value="<%=strSetID%>" name="setID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;</td>
							 
							<td class="RightText" nowrap align="right" HEIGHT="24">&nbsp;&nbsp;<b><fmtSetBuildDashBoard:message key="LBL_CONSIGN_ID"/>:</b></td>
							<td>&nbsp;<input type="text" size="15" value="<%=strConsignID%>" name="consignID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2>&nbsp;</td>	
							
							<td class="RightText" nowrap align="right" HEIGHT="24">&nbsp;&nbsp;<b><fmtSetBuildDashBoard:message key="LBL_REPROCESS_ID"/>:</b></td>
							<td>&nbsp;<input type="text" size="15" value="<%=str_ReprocessID%>" name="reprocessID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>&nbsp;</td>	
												
							<td > &nbsp;&nbsp;&nbsp;
							<fmtSetBuildDashBoard:message key="LBL_LOAD" var="varLoad"/>
							<gmjsp:button value="&nbsp;${varLoad}&nbsp;" onClick="javascript:fnGo();"  gmClass="button" buttonType="Load" /></td>					
						</tr></table></td>
					</tr>				
			 
	<tr class=Line><td noWrap height=1 colspan=3></td></tr>


<%
	if (strhAction.equals("LoadDashBoard"))
	{
%>
		
		<tr>
			<td width="1050" valign="top" colspan=3>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				 
				 
					<tr class="ShadeRightTableCaption">
						<td width="30"><fmtSetBuildDashBoard:message key="LBL_SET_NAME"/></td>
						<td width="10"> </td>
						<td HEIGHT="24" width="90"><fmtSetBuildDashBoard:message key="LBL_CONSIGN"/>.<BR><fmtSetBuildDashBoard:message key="LBL_ID"/></td>
				 		<td HEIGHT="24" width="90"><fmtSetBuildDashBoard:message key="LBL_REQUEST"/>.<BR><fmtSetBuildDashBoard:message key="LBL_ID"/></td>	
						<td width="130" align="center"><fmtSetBuildDashBoard:message key="LBL_INITIATED"/><BR><fmtSetBuildDashBoard:message key="LBL_BY"/></td>
						<td width="90" align="center"><fmtSetBuildDashBoard:message key="LBL_INITIATED"/><br><fmtSetBuildDashBoard:message key="LBL_DATE"/></td>
						<td width="90" align="center"><fmtSetBuildDashBoard:message key="LBL_REQUIRED"/><br><fmtSetBuildDashBoard:message key="LBL_DATE"/></td>
						
						<td width="130" align="center"><fmtSetBuildDashBoard:message key="LBL_STATUS"/></td>
						<td width="130" align="center"><fmtSetBuildDashBoard:message key="LBL_SOURCE"/></td>
						<td width="80" align="center"><fmtSetBuildDashBoard:message key="LBL_COMPLETE"/></td>
						<td width="80" align="center"><fmtSetBuildDashBoard:message key="LBL_VERIFY"/></td>
					 	<td width="80" align="center"><!--Release --></td>	
					</tr>
<%
						if (intLength > 0)
						{
							
							hmTempLoop = new HashMap();
							strNextId = "";
							intCount = 0;
							blFlag = false;
							strIniFlag = "";
							strWIPFlag = "";
							strTemp = "";
							blFlag1 = false;
							int intIniCnt = 0;
							int intWipCnt = 0;
							int intVerCnt = 0;
							sbTemp.setLength(0);
							for (int i = 0;i < intLength ;i++ )
							{
								hmLoop = (HashMap)alIniSets.get(i);

								if (i<intLength-1)
								{
									hmTempLoop = (HashMap)alIniSets.get(i+1);
									strNextId = GmCommonClass.parseNull((String)hmTempLoop.get("SID"));
								}

								strSetId = GmCommonClass.parseNull((String)hmLoop.get("SID"));
								strConId = GmCommonClass.parseNull((String)hmLoop.get("CID"));
								strSetName = GmCommonClass.parseNull((String)hmLoop.get("SNAME"));
								strTemp = strSetId;
								strTemp = strTemp.replaceAll("\\.","");
								strPerson = GmCommonClass.parseNull((String)hmLoop.get("PER"));
								strDateInitiated = GmCommonClass.parseNull((String)hmLoop.get("IDATE"));
								strReqdDate = GmCommonClass.parseNull((String)hmLoop.get("REQDATE"));
								strStatus = GmCommonClass.parseNull((String)hmLoop.get("STATUS"));
								strSource = GmCommonClass.parseNull((String)hmLoop.get("REQSRC"));
								strReprocessId = GmCommonClass.parseNull((String)hmLoop.get("REPROCID"));
								strReprocessId = strReprocessId.equals("")?"":"Reprocess ID:".concat(strReprocessId).concat("<BR>");
                                //	strComments = strReprocessId.concat(strComments);
							     strReqId = GmCommonClass.parseNull((String)hmLoop.get("REQID"));

								strFlag = GmCommonClass.parseNull((String)hmLoop.get("SFL"));
								strVeriFl = GmCommonClass.parseNull((String)hmLoop.get("VFL"));
								
								if (strFlag.equals("0"))
								{
									strIniFlag = "-";
									strWIPFlag = "-";
									strVeriFl = "-";
								 
								}
								if (strFlag.equals("1"))
								{
									strIniFlag = "<input type=checkbox name=Chk_Comp"+i+" onclick = chkBoxCount(this);>";
									strWIPFlag = "-";
									strVeriFl = "-";
									intIniCnt++;

								}
								else if (strFlag.equals("2")&& !strVeriFl.equals("1"))
								{
									strIniFlag = "-";
									strWIPFlag = "<input type=checkbox name=Chk_Verify"+i+" onclick = chkBoxCount(this);>";
									strVeriFl = "-";
									intWipCnt++;
								}
								else if (strFlag.equals("2") && strVeriFl.equals("1"))
								{
									strIniFlag = "-";
									strWIPFlag = "-";
									strVeriFl = "<input type=checkbox name=Chk_Release"+i+" onclick = chkBoxCount(this);>";
									intVerCnt++;
								}	


								if (strSetId.equals(strNextId))
								{
									intCount++;
									strLine = "<TR><TD colspan=12 height=1 class=Line></TD></TR>";
								}
								else
								{
									blFlag1 = true;
									sbTemp.append("document.getElementById('td"+strTemp+"Ini').innerHTML = '<b>"+intIniCnt+"</b>';");
									sbTemp.append("document.getElementById('td"+strTemp+"Wip').innerHTML = '<b>"+intWipCnt+"</b>';");
									//sbTemp.append("document.all.td"+strTemp+"Ver.innerHTML = '<b>"+intVerCnt+"</b>';");
									
									intIniCnt = 0;
									intWipCnt = 0;
									intVerCnt = 0;
									
									strLine = "<TR><TD colspan=12 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strSetName = "";
										strLine = "";
									}
									else
									{
										blFlag = true;
									}
									intCount = 0;
									//
								}

								if (intCount > 1)
								{
									strSetName = "";
									strLine = "";
								}
							
								out.print(strLine);
								strShade	= (i%2 != 0)?"class=ShadeBlue":""; //For alternate Shading of rows
								if (intCount == 1 || blFlag)
								{
%>
					<tr id="tr<%=strTemp%>">
						<td colspan="9" height="18" class="RightText">&nbsp;<A class="RightText" title="Click to Expand" href="javascript:Toggle('<%=strTemp%>')"><%=GmCommonClass.getStringWithTM(strSetName)%></a></td>
						<td align="center" class="RightText" id="td<%=strTemp%>Ini"></td>
						<td align="center"  class="RightText" id="td<%=strTemp%>Wip"></td>
						<td align="center"  ></td>
					     <% // <td align="center" class="RightText" id="td%=strTemp%Ver"></td>	%>
					</tr>
					<tr>
						<td colspan="12"><div style="display:none" id="div<%=strTemp%>">
							<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tab<%=strTemp%>">
<%
								blFlag = false;
								}
%>
								<tr <%=strShade%>>
									<td width="30" class="RightText" height="20">&nbsp;
									<fmtSetBuildDashBoard:message key="IMG_CLICK_TO_ADD_COMMENTS" var="varClicktoaddComments"/>
									<img id="imgEdit" style="cursor:hand" src="<%=strImagePath%>/phone_icon.jpg" title="${varClicktoaddComments}" width="20" height="17" onClick="javascript:fnOpenLog('<%=strConId%>',1220 )"/> 
									</td>
									<td width="10" class="RightText" align="center"><img style='cursor:hand' src="<%=strImagePath%>/packslip.gif" "border='0' onClick="javascript:fnPrintVer('<%=strConId%>')"/></td>
									<td width="90" class="RightText" height="20" align="center">&nbsp; <a href="javascript:fnFetch('<%=strConId%>')"><%=strConId%></a>
									<input type=hidden value='<%=strConId%>' name="Chk_ConsignId<%=i%>">
									</td>
								 	<td width="90" class="RightText" align="right"><!--  <a href="javascript:fnViewDetails('<%=strReqId%>')">--><%=strReqId%></td>   
									<td width="130" class="RightText" align="right">&nbsp;<%=strPerson%></td>
									<td width="90" class="RightText" align="right"><%=strDateInitiated%></td>
									<td width="90" class="RightText" align="right"><%=strReqdDate%></td>
									<td width="130" class="RightText" align="center">&nbsp;<%=strStatus%></td>
									<td width="130" class="RightText" align="center">&nbsp;<%=strSource%></td>
									<td width="80" class="RightText" align="right">&nbsp;<%=strIniFlag%></td>
									<td width="80" class="RightText" align="right">&nbsp;<%=strWIPFlag%></td>
									<td width="80" class="RightText" align="right">&nbsp;<!-- <%=strVeriFl%> --></td>	
								</tr>
								<TR><TD colspan=12 height=1 bgcolor=#cccccc></TD></TR>
<%
								if (blFlag1 || i+1==intLength)
								{
									if (i+1==intLength)
									{								
										sbTemp.append("document.getElementById('td"+strTemp+"Ini').innerHTML = '<b>"+intIniCnt+"</b>';");
										sbTemp.append("document.getElementById('td"+strTemp+"Wip').innerHTML = '<b>"+intWipCnt+"</b>';");
										//sbTemp.append("document.all.td"+strTemp+"Ver.innerHTML = '<b>"+intVerCnt+"</b>';");
									}
									intIniCnt = 0;
									intWipCnt = 0;
									intVerCnt = 0;
%>
							</table></div>
						</td>
					</tr>
<%
								}
							blFlag1	= false;
							}
							out.print("<script>");
							out.print(sbTemp.toString());
							out.print("</script>");
%>						
				<tr>
						<td align="center" colspan="12"> &nbsp;
						<fmtSetBuildDashBoard:message key="LBL_SUBMIT" var="varSubmit"/> 
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" onClick="javascript:fnSubmit();"  gmClass="button" buttonType="Save" /></td>	
					</tr>
						
<%  						} else	{
%>
					<tr>
						<td height="30" colspan="12" align="center" class="RightTextBlue">
						<fmtSetBuildDashBoard:message key="LBL_THERE_ARE_NO_INITIATED_SETS_FOR_BUILDING_AT_THIS_MOMENT"/> !</td>
					</tr>
<%
					}
%>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
				
		 <tr> <td colspan="3">
		 <!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		    <jsp:include page="/common/GmRuleDisplayInclude.jsp"/>
		    </td>
		 </tr>
		</table>
   
<BR><br>
<%
	}
%>	
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
