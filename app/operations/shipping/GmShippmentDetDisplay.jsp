 <%
/**********************************************************************************
 * File		 		: GmShippmentDetDisplay.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Velu 
 ***********************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %> 
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmtShipDisplay" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmShippmentDetDisplay.jsp -->
<fmtShipDisplay:setLocale value="<%=strLocale%>"/>
<fmtShipDisplay:setBundle basename="properties.labels.custservice.Shipping.GmShippingReport"/>
<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	String strApplnDateFmt = strGCompDateFmt;
	Date dtToday = GmCommonClass.getCurrentDate(strApplnDateFmt);
	String strTodaysDate = GmCommonClass.parseNull(GmCommonClass.getStringFromDate(dtToday,strApplnDateFmt));
	
	ArrayList alShipType = new ArrayList();
	ArrayList alShipVal = new ArrayList();
	double lngTotalShipVal = 0.00;
	String strPrice = "";
	String strShipmentType = "";
	String strConsignIds = "";
	String strCodeID = "";
	String strSelected = "";
	
	String strSourceSelected = ""; 
	String strRefIDEntered = "";
	String strReqIDEntered = "";
	String strStatusSelected = "";
	String strPendingForEntered = "";
	String strCarrierSelected = "";
	String strShipToSelected= "";
	String strNamesEntered ="";
	String strFromDateEntered = "";
	String strToDateEntered = "";
	String strDtTypSelected="";
	String strHoldSts = "";
	String strCurrency = "";
	String strTotalShipVal = "";
	String strTotal = "";
	String strFromDt = "";
	String strToDt = "";
	String strPlannedDate = "";
	String strPlShipDtSubmitFl = "";
	
	HashMap hmLoop = new HashMap();
	java.util.Date dtFrom = null;
	java.util.Date dtTo	= null;
	java.util.Date DtPlannedDate = null;
	if (hmReturn != null){
		// For Display total shipment value value only completed status
		alShipVal = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SHIPMENTVAL"));
		// Ship type disaplaying in Drop down. 
		alShipType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SHIPTYPE")); 
		strShipmentType = GmCommonClass.parseNull((String)hmReturn.get("hShipmentType"));
		strConsignIds = GmCommonClass.parseNull((String)hmReturn.get("STRCONSIGNIDS"));
		strPlannedDate = GmCommonClass.parseNull((String)hmReturn.get("STRPLANNEDDATE"));
		strPlShipDtSubmitFl = GmCommonClass.parseNull((String)hmReturn.get("HPLSHIPDTSUBMITFL"));
		DtPlannedDate =   GmCommonClass.getStringToDate(strPlannedDate,strApplnDateFmt);
		
		strSourceSelected = GmCommonClass.parseNull((String)hmReturn.get("hSource"));
		strRefIDEntered = GmCommonClass.parseNull((String)hmReturn.get("hRefID"));
		strReqIDEntered = GmCommonClass.parseNull((String)hmReturn.get("hReqID"));
		strStatusSelected = GmCommonClass.parseNull((String)hmReturn.get("hStatus"));
		strPendingForEntered = GmCommonClass.parseNull((String)hmReturn.get("hPendingFor"));
		strCarrierSelected = GmCommonClass.parseNull((String)hmReturn.get("hCarrier"));
		strShipToSelected = GmCommonClass.parseNull((String)hmReturn.get("hShipTo"));
		strNamesEntered = GmCommonClass.parseNull((String)hmReturn.get("hNames"));
		dtFrom	= (Date) hmReturn.get("hFromDate");
		dtTo = (Date) hmReturn.get("hToDate");
		strFromDt = GmCommonClass.getStringFromDate(dtFrom,strApplnDateFmt);
		strToDt = GmCommonClass.getStringFromDate(dtTo,strApplnDateFmt);
		strDtTypSelected = GmCommonClass.parseNull((String)hmReturn.get("hDtTyp"));
		strHoldSts = GmCommonClass.parseNull((String)hmReturn.get("hHoldSts"));
		strHoldSts = strHoldSts.equals("on")?"checked":"";
		
		if(alShipVal.size() > 0){
			for (int i = 0; i < alShipVal.size(); i++){
				hmLoop = (HashMap)alShipVal.get(i);
				strTotal = GmCommonClass.parseNull((String)hmLoop.get("TOTAL"));
				
				if(strCurrency.equals(""))
					strCurrency = GmCommonClass.parseNull((String)hmLoop.get("NEWCURRENCY"));
				lngTotalShipVal = lngTotalShipVal +  Double.parseDouble(strTotal);	
			}
			
		}
		strTotalShipVal = lngTotalShipVal + "";
	} 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Shipment Details Display</TITLE> 
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmShippingReport.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmCommonShippingReport.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
</HEAD>
<script>
var dateFmt = '<%=strApplnDateFmt%>';
var todaysdate = '<%=strTodaysDate%>'
var shipmentSubFl = '<%=strPlShipDtSubmitFl%>'

function fnSubmit(obj){
	strPlnShipDt = document.frmAccount.Txt_PlannedDate;
	// When select planned ship date , should be current date/Future date.
	CommonDateValidation(strPlnShipDt,dateFmt,message[10589]);
	planShipDtDiff = dateDiff(todaysdate,strPlnShipDt.value,dateFmt);
	if (planShipDtDiff < 0){
		Error_Details(message[10590]);
	}
	if (ErrorCount > 0)	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmAccount.hPlShipDtSubmitFl.value = true;
	document.frmAccount.hAction.value = "UpdShipDate";
	fnStartProgress('Y');
	document.frmAccount.submit();
}

function fnClose(){
	parent.parent.RightFrame.fnEnableScreen();
	if(shipmentSubFl) parent.window.fnReload();
	parent.window.fnClearShipmentType();
	CloseDhtmlxWindow();
	return false;
}
</script>
<style>
table.GtTable550 {
	margin: 0 0 0 0;
	width: 550px;
	border: 1px solid  #676767;
}
</style>

<BODY>
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmShippingReportServlet">
<input type="hidden" name="hAction" value="UpdShipDate">
<input type="hidden" name="strConsignIds" value="<%=strConsignIds%>">
<input type="hidden" name="Cbo_ShippingSource" value="<%=strSourceSelected%>">
<input type="hidden" name="Txt_RefId" value="<%=strRefIDEntered%>">
<input type="hidden" name="Txt_ReqId" value="<%=strReqIDEntered%>">
<input type="hidden" name="hStatus" value="<%=strStatusSelected%>">
<input type="hidden" name="Txt_PendingDays" value="<%=strPendingForEntered%>">
<input type="hidden" name="Cbo_ShippingCarrier" value="<%=strCarrierSelected%>">
<input type="hidden" name="Cbo_ShipTo" value="<%=strShipToSelected%>">
<input type="hidden" name="Txt_Names" value="<%=strNamesEntered%>">
<input type="hidden" name="Txt_FromDate" value="<%=strFromDt%>">
<input type="hidden" name="Txt_ToDate" value="<%=strToDt%>">
<input type="hidden" name="Cbo_Date" value="<%=strDtTypSelected%>">
<input type="hidden" name="Cbo_HoldSts" value="<%=strHoldSts%>">
<input type="hidden" name="Cbo_Shippment_Type" value="<%=strShipmentType%>">
<input type="hidden" name="hPlShipDtSubmitFl" value="<%=strPlShipDtSubmitFl%>">

	<table border="0" class= "GtTable550" cellspacing="0" cellpadding="0">
		<%if(strShipmentType.equals("102540")){ %>
			<tr class="shade"> 
              <td class="RightTableCaption" height="50" width="275" align="right">&nbsp;<fmtShipDisplay:message key="LBL_SEL_SHIPMENT_EQUAL"/></td>
              <td class="RightTableCaption" align="left">&nbsp;&nbsp;<%= GmCommonClass.getStringWithCommas(strTotalShipVal) %>&nbsp;<%=strCurrency%></td>
         	</tr>
         	<tr><td colspan="7" height="1" bgcolor="#cccccc"></td></tr>
    	<%}%>
    	<%if(strShipmentType.equals("102541")){%>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
		   	<tr class="shade">
               <td HEIGHT="50" class="RightTableCaption" align="Right"><fmtShipDisplay:message key="LBL_PLANED_SHIP_DATE"/>:&nbsp;</td>
               <td class="RightText" colspan="2"><gmjsp:calendar textControlName="Txt_PlannedDate" textValue="<%=(DtPlannedDate==null)?null:new java.sql.Date(DtPlannedDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
    	 	   <fmtShipDisplay:message key="BTN_SUBMIT" var="varSubmit"/>
    	 	   <td colspan="4">&nbsp;<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="javascript:fnSubmit(this);" buttonType="Save" /></td>
			</tr>
			<tr><td colspan="7" height="1" bgcolor="#cccccc"></td></tr>
		<%}%>
		<fmtShipDisplay:message key="BTN_CLOSE" var="varClose"/>
	 	<tr><td colspan="5" height="40" align="center"><gmjsp:button name="Close_button" value="${varClose}" gmClass="button" onClick="fnClose();" buttonType="Load" /></td></tr>		 
	 </table>
 </FORM>
 <%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
