
<%
/**********************************************************************************
 * File		 		: GmEditShipDetails.jsp
 * Desc		 		: Modify Shipping details and ShipOut screen
 * Version	 		: 1.0
 * author			: Brinal
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*"%>
<%@ page import="com.globus.operations.shipping.forms.GmShipDetailsForm"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ taglib prefix="fmtShipDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmEditShipDetails.jsp -->
<fmtShipDetails:setLocale value="<%=strLocale%>"/>
<fmtShipDetails:setBundle basename="properties.labels.custservice.ModifyOrder.GmEditShipDetails"/>

<bean:define id="shipDate" name="frmShipDetails" property="shipDate" type="java.lang.String"> </bean:define>
<bean:define id="msgFlag" name="frmShipDetails" property="msgFlag" 	type="java.lang.String"> </bean:define>
<bean:define id="names" name="frmShipDetails" property="names" type="java.lang.String"> </bean:define>
<bean:define id="shipFlag" name="frmShipDetails" property="shipFlag" type="java.lang.String"></bean:define>
<bean:define id="repid" name="frmShipDetails" property="repID" type="java.lang.String"></bean:define>
<bean:define id="distid" name="frmShipDetails" property="distID" type="java.lang.String"></bean:define>
<bean:define id="accid" name="frmShipDetails" property="accID" type="java.lang.String"></bean:define>
<bean:define id="accCurrSymb" name="frmShipDetails" property="accCurrSymb" type="java.lang.String"></bean:define>
<bean:define id="addressid" name="frmShipDetails" property="addressid" type="java.lang.String"></bean:define>
<bean:define id="strRMAID" name="frmShipDetails" property="rmaID" type="java.lang.String"></bean:define>
<bean:define id="strPurpose" name="frmShipDetails" property="strPurpose" type="java.lang.String"></bean:define>
<bean:define id="asscTxnID" name="frmShipDetails" property="asscTxnID" type="java.lang.String"></bean:define>
<bean:define id="refId" name="frmShipDetails" property="refId" type="java.lang.String"></bean:define>
<bean:define id="thirdParty" name="frmShipDetails" property="thirdParty" type="java.lang.String"></bean:define>
<bean:define id="shipcost" name="frmShipDetails" property="freightAmt" type="java.lang.String"></bean:define>
<bean:define id="strRBTxn" name="frmShipDetails" property="strRBTxn" type="java.lang.String"></bean:define>
<bean:define id="status" name="frmShipDetails" property="status" type="java.lang.String"></bean:define>
<bean:define id="source" name="frmShipDetails" property="source" type="java.lang.String"></bean:define>
<bean:define id="ctype" name="frmShipDetails" property="ctype" type="java.lang.String"></bean:define>
<bean:define id="strOrderType" name="frmShipDetails" property="strOrderType" type="java.lang.String"></bean:define>
<bean:define id="holdFlag" name="frmShipDetails" property="holdFlag" type="java.lang.String"></bean:define>
<bean:define id="strSubmitButtonAccess" name="frmShipDetails" property="strSubmitButtonAccess" type="java.lang.String"></bean:define>   
<bean:define id="strDisableButtonAccess" name="frmShipDetails" property="strDisableButtonAccess" type="java.lang.String"></bean:define>
<bean:define id="tissuefl" name="frmShipDetails" property="tissuefl" type="java.lang.String"></bean:define>
<bean:define id="loanerReqId" name="frmShipDetails" property="loanerReqId" type="java.lang.String"></bean:define> 


<% String strWikiTitle = GmCommonClass.getWikiTitle("MODIFY_SHIP_DETAILS");%>
 
<% 
int intSize = 0;
ArrayList alReturn = new ArrayList();
alReturn = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSEQUENCES"));
intSize = alReturn.size();
GmCalenderOperations.setTimeZone(strGCompTimeZone);
String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
String strPurposeVal = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPurpose, "INHOUSEPUR")); // OUS sales replenishment
HashMap hmFedex_Track_Strip =  (HashMap)(GmCommonClass.getShippingProperties()); 
String strAsscID = asscTxnID.equals("")?" is Open.":"<a href=javascript:fnAsscTxn('" + asscTxnID +"','100406','" +refId + "');>" + asscTxnID + "</a>" + " is Open.";
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean1 = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
String strShowCommercialInvoice = GmCommonClass.parseNull( gmResourceBundleBean1.getProperty("SHIP.SHOW_COMMERCIAL_INVOICE"));
String strPrintCountFl = GmCommonClass.parseNull( gmResourceBundleBean1.getProperty("PACK_SLIP.PRINT_COUNT"));
String strShowCommercialInvoiceOus = GmCommonClass.parseNull( gmResourceBundleBean1.getProperty("SHIP.SHOW_COMMERCIAL_INVOICE"+strOrderType));

%>
<HTML>
<HEAD>

<TITLE>Globus Medical: Shipping Include</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmEditShipDetails.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<script> 
var shipFlg = '<%=shipFlag%>';
var distid ='<%=distid%>';
var repid ='<%=repid%>';
var accid ='<%=accid%>';
var addid = '<%=addressid%>';
var rbTxns = '<%=strRBTxn%>';
var btnDisableTxns = rbTxns.split(',');
var status = '<%=status%>';
var strPrintCountFl = '<%=strPrintCountFl%>';
var strPSFGHoldFl = '<%=holdFlag%>';
var asscTxnID = '<%=asscTxnID%>';
//PMT#40555
var tissueflag = '<%=tissuefl%>';
<%
Set keySet = hmFedex_Track_Strip.keySet();
int i = 0;
for (Iterator keys = keySet.iterator(); keys.hasNext();) {
	String key = (String) keys.next();
	String value = (String) hmFedex_Track_Strip.get(key); 
%>
var	 FexArr<%=i%> =  "<%=key %>,<%=value %>" ;
<%
i++;
}
%> 
var len = '<%=i %>';
var strPurpose = '<%=strPurposeVal%>';
 
 var loanerReqID = '<%=loanerReqId%>';  /* added loanerReqId for PMT-42950 */
</script>

</head>
<!-- 'Y' is passing to fnGetAddressList() to get only active addresses to the Modify Shipping details screen -->
<body
	onload="fnFormLoad();fnGetAddressList(document.all.names,'<bean:write name="frmShipDetails" property="addressid"/>','Y');"
	leftmargin="20" topmargin="10">
	<html:form action="/gmOPEditShipDetails.do">
		<html:hidden property="strOpt" name="frmShipDetails" />
		<html:hidden property="status" name="frmShipDetails" />
		<html:hidden property="hAction" value="" />
		<html:hidden property="hTxnId" value="" />
		<html:hidden property="hTxnName" value="" />
		<html:hidden property="hCancelType" value="" />
		<html:hidden property="hrefId" />
		<html:hidden property="shipId" />
		<html:hidden property="hreqid" />
		<html:hidden property="ctype" />
		<html:hidden property="hopt" value="" />
		<html:hidden property="returnID" />
		<input type="hidden" name="RE_FORWARD" value="gmOPEditShipDetails">
		<!-- modifying for PMT-3011 Address Change Issue -->
		<input type="hidden" name="txnStatus" value="">
		<input type="hidden" name="RE_TXN" value="SHIPOUTWRAPPER">
		<input type="hidden" name="hshipTo"
			value='<bean:write name="frmShipDetails" property="shipTo"/>' />
		<input type="hidden" name="hnames"
			value='<bean:write name="frmShipDetails" property="names"/>' />
		<input type="hidden" name="hdtype"
			value='<bean:write name="frmShipDetails" property="dtype"/>' />
		<input type="hidden" name="haddressid"
			value='<bean:write name="frmShipDetails" property="addressid"/>' />
		<input type="hidden" name="hshipCarrier"
			value='<bean:write name="frmShipDetails" property="shipCarrier"/>' />
		<input type="hidden" name="hshipMode"
			value='<bean:write name="frmShipDetails" property="shipMode"/>' />
		<input type="hidden" name="htrackingNo"
			value='<bean:write name="frmShipDetails" property="trackingNo"/>' />
		<input type="hidden" name="hshipId"
			value='<bean:write name="frmShipDetails" property="shipId"/>' />
		<input type="hidden" name="hholdFl"
			value='<bean:write name="frmShipDetails" property="holdFlag"/>' />
		<input type="hidden" name="hoverrideAttnTo"
			value='<bean:write name="frmShipDetails" property="overrideAttnTo"/>' />
		<input type="hidden" name="hfreightAmt"
			value='<bean:write name="frmShipDetails" property="freightAmt"/>' />
		<input type="hidden" name="hParentNm" value="EditShip"/>
		<input type="hidden" name="hSubmitAcc" value='<bean:write name="frmShipDetails" property="strSubmitButtonAccess"/>' />
		
		<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
			<tr>
				<td height="30" class="RightDashBoardHeader" colspan="3"><fmtShipDetails:message key="LBL_HEADER"/></td>
				<fmtShipDetails:message key="IMG_HELP" var = "varHelp"/>
				<td height="25" class="RightDashBoardHeader" align="right"><img
					id='imgEdit' align="right" style='cursor: hand'
					src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>

			<tr>
				<td colspan="4" height="1" bgcolor="#cccccc"></td>
			</tr>
			<logic:equal name="frmShipDetails" property="holdFlag" value="Y">
				<tr>
					<td height="25" colspan="4" class="errorMsg"><bean:message
							key="message.shipping.hold.status" /> <%=strAsscID%>
					</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</logic:equal>
			<logic:equal name="frmShipDetails" property="msgFlag" value="Y">
				<tr>
					<td height="25" colspan="4" align="center" class=""><font style="color: green" size="2"><b><fmtShipDetails:message key="LBL_MESSAGE"/></b></font>
					</td>
				</tr>
				<tr>
				<td height="1" bgcolor="#cccccc" colspan="4"></td>
			</tr>
			</logic:equal>
			<logic:notEqual name="frmShipDetails" property="rmaID" value="">
				<tr>
					<td height="25" colspan="4" align="center" class=""><fmtShipDetails:message key="LBL_MESSAGE1"/> <b><font style="color: blue" size="2"><a href="javascript:fnPrintRAInfo('<%=strRMAID%>');"><bean:write name="strRMAID"/></a></font></b>
									<fmtShipDetails:message key="LBL_MESSAGE2"/>
					</td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
				<td height="1" bgcolor="#cccccc" colspan="4"></td>
			</tr>
			</logic:notEqual>
			<tr class="Shade">
				<td width="12%" class="RightTableCaption" HEIGHT="30" align="right"><font
					color="red" >*</font>&nbsp; <fmtShipDetails:message key="LBL_REFID"/>:</td>
					<fmtShipDetails:message key="LBL_STATUSDETAILS" var = "varStatusDetails"/>
				<td width="250">&nbsp;<html:text property="refId" name="frmShipDetails" size="30"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" onkeypress="fnCallGo()" />
					<a href="javascript:fnStatusDetails();" tabindex="-1"> <img
						width='15' height='15' title='${varStatusDetails}'
						style='cursor: hand' src='<%=strImagePath%>/d2.gif' align="middle"/> </a></td>
				<td class="RightTableCaption" HEIGHT="23" align="right"><font
					color="red">*</font>&nbsp; <fmtShipDetails:message key="LBL_SOURCE"/>:</td>
				<td width=300>
				   <table cellpadding="0" cellspacing="0" border="0">
				   <!-- Custom tag lib code modified for JBOSS migration changes -->
				   <fmtShipDetails:message key="LAB_CHOOSEONE" var = "varChooseOne"/>
				   <fmtShipDetails:message key="BTN_PICSLIP" var = "varBtnPickSlip"/>
					<tr><td width="40%" align="left">&nbsp;<gmjsp:dropdown controlName="source" SFFormName='frmShipDetails' SFSeletedValue="source" defaultValue="${varChooseOne}" SFValue="alSource" codeId="CODEID" codeName="CODENM" onChange="disableRollback();" />
						</td>	
						<fmtShipDetails:message key="BTN_LOAD" var = "varLoad"/>
						<td>&nbsp;&nbsp;<gmjsp:button value="${varLoad}" gmClass="button" name="Btn_Go" onClick="fnGo();" buttonType="Load" /> &nbsp;<gmjsp:button value="${varBtnPickSlip}" gmClass="button" onClick="fnPicSlip();" buttonType="Save" />
						</td>
				  </tr></table>
			 </td>
			</tr>
			<tr>
				<td class="line" colspan="4"></td>
			</tr>
			<tr>
				<td colspan="4"><jsp:include page="/operations/shipping/GmIncShipDetails.jsp">
								<jsp:param name="screenType" value="EditShip" />
								<jsp:param name="hType" value="InhouseLoaner" />	
								<jsp:param name="ThirdParty" value="<%=thirdParty%>" />
								</jsp:include>
				</td>
			</tr>
			<tr>
				<td height="1" bgcolor="#cccccc" colspan="4"></td>
			</tr>
			<tr>
				<td width="12%" class="RightTablecaption" HEIGHT="23" align="right"><fmtShipDetails:message key="LBL_SHIPPINGCHARGES"/>:</td>
				 <td width="37%">&nbsp;<gmjsp:currency currSymbol="<%=accCurrSymb%>" type="CurrTextSign"  textValue="<%=shipcost%>" td="false"/>
				 <input type="hidden" name="freightAmt" value="<%=shipcost%>" />
				<%-- <html:text property="freightAmt"
						name="frmShipDetails" size="10" maxlength="5"
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');" /> --%></td>
				<td width="10%" class="RightTablecaption" HEIGHT="23" align="right" width="19%"><fmtShipDetails:message key="LBL_TRACKING"/> #:</td>
				<td>&nbsp;<html:text property="trackingNo"
						name="frmShipDetails" size="30" 
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');"
						onkeypress="validateTrackingNum();"/></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#cccccc" colspan="4"></td>
			</tr>
			<tr class="Shade">
				<td width="12%" class="RightTablecaption" HEIGHT="23" align="right"><fmtShipDetails:message key="LBL_PACKINGSTATION"/>:</td>
				<td width="37%">&nbsp;<html:text property="pickStationId" name="frmShipDetails" size="25" maxlength="20"
					styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
				</td>
				<td width="10%" class="RightTablecaption" HEIGHT="23" align="right" width="19%"><fmtShipDetails:message key="LBL_TOTE"/>:</td>
				<td>&nbsp;<html:text property="toteId" name="frmShipDetails" size="25" maxlength="20" styleClass="InputArea"
					onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
				</td>
			</tr>
			<tr>
				<td height="1" bgcolor="#cccccc" colspan="4"></td>
			</tr>
			<tr>
			<td colspan=4> <table cellpadding="0" cellspacing="0" border="0"> <tr>	
				<logic:equal name="frmShipDetails" property="source" value="50181">
					<logic:notEqual name="frmShipDetails" property="interPartyId"
						value="0">
						<logic:notEqual name="frmShipDetails" property="status" value="15">
							<td width="80"  class="RightTablecaption" HEIGHT="23" align="right">&nbsp;<fmtShipDetails:message key="LBL_CUSTOMERPO"/>:</td>
							<td><html:text property="custPO" maxlength="999"
									name="frmShipDetails" size="35"
									onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
									onblur="changeBgColor(this,'#ffffff');" /></td>
						</logic:notEqual>
					</logic:notEqual>
				</logic:equal>
				<logic:equal name="frmShipDetails" property="interPartyId" value="0">
					<td   colspan =2></td>
				</logic:equal>
				<% if (shipDate.equals(strTodaysDate) || shipDate.equals("")) { %>
				<logic:equal name="frmShipDetails" property="isMaster" value="Y">
					<td  class="RightTablecaption" HEIGHT="23" align="right"><fmtShipDetails:message key="LBL_ACTION"/>:</td>
					<td width = 43% >&nbsp; <gmjsp:dropdown controlName="updAction"
							SFFormName='frmShipDetails' SFSeletedValue="updAction"
							defaultValue="${varChooseOne}" SFValue="alUpdAction" codeId="CODEID"
							codeName="CODENM" /></td>
				</logic:equal>
				<logic:equal name="frmShipDetails" property="isMaster" value="N">
					<logic:notEmpty name="frmShipDetails" property="alTracknos">
						<td class="RightTablecaption" HEIGHT="23" align="right"><fmtShipDetails:message key="LBL_OR"/>:</td>
						<td width = 43%> &nbsp; <gmjsp:dropdown controlName="tracknos"
								SFFormName='frmShipDetails' SFSeletedValue="tracknos"
								defaultValue="${varChooseOne}" SFValue="alTracknos" codeId="ID"
								codeName="NAME" onChange="cpyToTxtBx(this.value);" /></td>
					</logic:notEmpty>
					<logic:empty name="frmShipDetails" property="alTracknos">
					<td></td>
					<td></td>
					</logic:empty>
				</logic:equal>
				<% } %>
			</tr>
			</table> </td> </tr>
			<tr class="Shade" id="rollbackStatus" style="display: none">
				<td class="RightTableCaption" height="23" colspan="4">
					<font color="red" >&nbsp;&nbsp;<fmtShipDetails:message key="LBL_ROLLBACKSTATUS"/>? 
						&nbsp;<input type=radio value="33" name="shipStatus" onclick="fnEnableRollback();"></input>
						&nbsp;<fmtShipDetails:message key="LBL_PACKINGINPROGRESS"/>
						&nbsp;<input type=radio value="30" name="shipStatus" onclick="fnEnableRollback();"></input>
						&nbsp;<fmtShipDetails:message key="LBL_PENDINGSHIPPING"/>
					</font>
				</td>
			</tr>
			<tr>
				<td height="1" bgcolor="#cccccc" colspan="4"></td>
			</tr>
			<logic:equal name="frmShipDetails" property="loanerReqFlag" value="Y">					
			<tr >
			 	<td colspan="4">
			 		<table>
			 			<tr>			
			 				<td align="left" width="25">
			 				<fmtShipDetails:message key="LBL_LOANERTITLE" var="varLoanerTitle"/>
			 				<html:checkbox property="chk_loaner" name="frmShipDetails" title="${varLoanerTitle}"></html:checkbox>
			 				</td>
			 				<td class="RightTableCaption"  HEIGHT="25"> <fmtShipDetails:message key="LBL_MESSAGE3"/></td>
			 			</tr>
			 		</table>
				</td> 				
			</tr>
			<tr>
				<td colspan="4" height="1" bgcolor="#cccccc"></td>
			</tr>
			</logic:equal>
			<tr>
				<td colspan="4" align="center">
				<jsp:include page="/accounts/GmCreditHoldInclude.jsp" >
					<jsp:param name="ALERT" value="N"/>
					<jsp:param name="SUBTYPE" value="104966"/>
					</jsp:include>
				</td>
			</tr>	
			<tr>
				<td colspan="4" height="50" class="aligncenter">
				    <fmtShipDetails:message key="BTN_SUBMIT" var = "varSubmit"/>
				    <fmtShipDetails:message key="BTN_ROLLBACK" var = "varRollBack"/>
					<gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" controlId="Btn_Submit" disabled="<%=strSubmitButtonAccess%>" buttonType="Save" />&nbsp;&nbsp;&nbsp;
					<gmjsp:button value="${varRollBack}" controlId="Btn_Rollback" name="Rollback"	gmClass="button" onClick="fnRollback();" buttonType="Save" />
				
					<%if(strShowCommercialInvoice.equals("YES") || strShowCommercialInvoiceOus.equals("YES") ){ %>
					<logic:equal name="frmShipDetails" property="status" value="40">
					<logic:equal name="frmShipDetails" property="source" value="50180">
					&nbsp;&nbsp;&nbsp;
					<fmtShipDetails:message key="BTN_COMMERCIALINVOICE" var = "varCommerInvoice"/>
					<gmjsp:button value="${varCommerInvoice}" controlId="Btn_Commercial_Inv" name="Commercial_Invoice"
					gmClass="button" onClick="fnCommercialInv();" buttonType="Save" />&nbsp;&nbsp;
					</logic:equal>
					</logic:equal>
					<%} %>
					<logic:equal
						name="frmShipDetails" property="status" value="40">
						<logic:equal name="frmShipDetails" property="source" value="50182">
							<fmtShipDetails:message key="BTN_PRINTVERAGRE" var = "varPrintVerAgre"/>
							<gmjsp:button value="${varPrintVerAgre}"
								gmClass="button" onClick="fnPrintVersionLetter();" buttonType="Load" />
							<fmtShipDetails:message key="BTN_PRINTLETTER" var="varPrintLetter"/>
							<gmjsp:button value="${varPrintLetter}" gmClass="button"
								onClick="fnPrintLetter();" buttonType="Load" />&nbsp;&nbsp;&nbsp;
					<fmtShipDetails:message key="BTN_PRINTVERSION" var = "varPrintVersion"/>
            		<gmjsp:button value="${varPrintVersion}" gmClass="button"
								onClick="fnPrintVersion();" buttonType="Load" />
						</logic:equal>
						<logic:notEqual name="frmShipDetails" property="source"
							value="50182">
							<logic:equal name="frmShipDetails" property="pkSlipNm"
								value="SHIPPINGLIST">
								<fmtShipDetails:message key="BTN_SHIPPINGLIST" var="varShippingList"/>
								<gmjsp:button value="${varShippingList}" gmClass="button"
									onClick="fnPackSlip();" buttonType="Save" />&nbsp;&nbsp;&nbsp;
            		</logic:equal>
            	
            
							<logic:notEqual name="frmShipDetails" property="pkSlipNm"
								value="SHIPPINGLIST">
								<fmtShipDetails:message key="BTN_PACKSLIP" var="varPackSlip"/>
								<gmjsp:button value="${varPackSlip}" gmClass="button"
									onClick="fnPackSlip('manually');" buttonType="Save" />&nbsp;&nbsp;&nbsp;
            		</logic:notEqual>
						</logic:notEqual>
		
					</logic:equal> 
					<fmtShipDetails:message key="BTN_DISABLE" var = "varDisable"/><gmjsp:button value="${varDisable}" gmClass="button" disabled="<%=strDisableButtonAccess%>"
					onClick="fnDisableEmail()" controlId="Btn_Disable" buttonType="Save" />&nbsp;&nbsp;&nbsp;
				
						  <logic:equal
						name="frmShipDetails" property="status" value="40">  
						
						  <%if(source.equals("4000518") || (source.equals("50181")&& ctype.equals("26240144"))){ %>  
							<fmtShipDetails:message key="BTN_PRINTVERSION" var = "varPrintVersion"/>
							<gmjsp:button value="${varPrintVersion}"
								gmClass="button" onClick="fnPrintForReturnType('manually');" buttonType="Load" />
						  <%}else if(source.equals("26240435")){ //26240435 - Stock Transfer %> 
			
							<fmtShipDetails:message key="BTN_PRINT" var = "varPrint"/>
							<gmjsp:button value="${varPrint}"
							gmClass="button" onClick="fnPrintSlip();" buttonType="Load" /> 
							<%} %>
						  </logic:equal>   
							
							
							
			
					  <logic:equal
						name="frmShipDetails" property="source" value="50181"> 
						<logic:equal name="frmShipDetails" property="status" value="40">
						<logic:notEqual name="frmShipDetails" property="ctype" value="26240144">
							<fmtShipDetails:message key="BTN_PRINTVERAGRE" var = "varPrintVerAgre"/>
							<gmjsp:button value="${varPrintVerAgre}"
								gmClass="button" onClick="fnPrintVersionLetter('manually');" buttonType="Load" />
								</logic:notEqual>
						</logic:equal>
						<fmtShipDetails:message key="BTN_VIEWCONSIGNDETAIL" var="varViewConsignDetails"/>
						<gmjsp:button value="${varViewConsignDetails}" gmClass="button"
							onClick="fnConsignDetails();" buttonType="Save" />
					 </logic:equal>
					 <logic:equal name="frmShipDetails" property="source" value="50186"><!--50186 Inhouse Loaner source --> 
						<logic:equal name="frmShipDetails" property="status" value="40">
							<fmtShipDetails:message key="BTN_PRINTVERAGRE" var = "varPrintVerAgre"/>
							<gmjsp:button value="${varPrintVerAgre}"
								gmClass="button" onClick="fnPrintVersionLetter();" buttonType="Load" />
							<fmtShipDetails:message key="BTN_PRINTLETTER" var="varPrintLetter"/>
							<gmjsp:button value="${varPrintLetter}" gmClass="button"
								onClick="fnPrintLetter();" buttonType="Load" />&nbsp;&nbsp;&nbsp;
							<fmtShipDetails:message key="BTN_PRINTVERSION" var="varPrintVersion"/>
            				<gmjsp:button value="${varPrintVersion}" gmClass="button"
								onClick="fnPrintVersion();" buttonType="Load" />
						</logic:equal>
				 	</logic:equal> 
					</td>
			</tr>
			<tr>
				<td class="lline" colspan="4"></td>
			</tr>
			<tr>
				<td colspan="4"><jsp:include page="/common/GmIncludeLog.jsp">
						<jsp:param name="FORMNAME" value="frmShipDetails" />
						<jsp:param name="ALNAME" value="alLogReasons" />
					</jsp:include></td>
			</tr>
			<%
        if(intSize > 0)
        {
        %>
			<tr>
				<td class="line" colspan="4" height="1"></td>
			</tr>
			<tr>
				<td colspan="4"><jsp:include
						page="/common/GmRuleDisplayInclude.jsp"></jsp:include></td>
			</tr>
			<%} %>
		</table>

	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>