<%
/**********************************************************************************
 * File		 		: GmIncTissueShipDetails.jsp
 * Desc		 		: Common Include for Tissue Parts Shipping Details
 * owner			: tramasamy
************************************************************************************/
%>

<!-- \operations\purchasing\shipping\GmIncTissueShipDetails.jsp-->  
   
   

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.operations.shipping.forms.GmShipDetailsForm"%>
<%@ taglib prefix="fmtShipDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtShipDtls:requestEncoding value="UTF-8" />
<fmtShipDtls:setLocale value="<%=strLocale%>"/>
<fmtShipDtls:setBundle basename="properties.labels.operations.shipping.GmIncShipDetails"/>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<bean:define id="tissueAddressId" name="frmShipDetails" property="tissueAddressId" type="java.lang.String"> </bean:define>
<bean:define id="tissueNames" name="frmShipDetails" property="tissueNames" type="java.lang.String"> </bean:define>
<bean:define id="tissueShipTo" name="frmShipDetails" property="tissueShipTo" type="java.lang.String"> </bean:define>
<bean:define id="screenType" name="frmShipDetails" property="screenType" type="java.lang.String"> </bean:define>
<bean:define id="tissueShipMode" name="frmShipDetails" property="tissueShipMode" type="java.lang.String"> </bean:define>
<bean:define id="tissueShipCarrier" name="frmShipDetails" property="tissueShipCarrier" type="java.lang.String"> </bean:define>
<bean:define id="alShipMode" name="frmShipDetails" property="alShipMode" type="java.util.ArrayList"> </bean:define>
<bean:define id="strEditChargeFl" name="frmShipDetails" property="editShipChrgFl" type="java.lang.String"> </bean:define>
<%
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strscreenType ="";
String strscrType ="";
HashMap hmNames = new HashMap();
hmNames =	(HashMap)request.getAttribute("NAMES");
	ArrayList alDistributor = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alAccList = new ArrayList();
	ArrayList alEmpList = new ArrayList();
	ArrayList alPlantList = new ArrayList();
	ArrayList alDealerList = new ArrayList();
	
	ArrayList alFEDModeList = new ArrayList();
	ArrayList alUPSModeList = new ArrayList();
	ArrayList alCurrierModeList = new ArrayList();
	ArrayList alOtherModeList = new ArrayList();
	ArrayList alTNTModeList = new ArrayList();
	ArrayList alDHLModeList = new ArrayList();
	
	String strFedExDefltMode = "";
	String strUPSDefltMode = "";
	String strCourierDefltMode = "";
	String strTNTDefltMode = "";
	String strDHLDefltMode = "";
	String strOtherDefltMode = "";
	
	ArrayList alAssociateSalesRepList = new ArrayList();
	ArrayList alShipacctList = new ArrayList();
	String strDisplayfl = GmCommonClass.parseNull(request.getParameter("DISPLAYFL")) == "" ? "Y" :GmCommonClass.parseNull(request.getParameter("DISPLAYFL"));
	String strThirdParty = GmCommonClass.parseNull(request.getParameter("ThirdParty"));	
	// To know whether to show the Calculated Shipping charge field or not
	String strShpChrgFl = GmCommonClass.parseNull((String)request.getParameter("SHIPCHRG"));
	String strCurrencyFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	if(screenType.equals("")){
		strscreenType = GmCommonClass.parseNull(request.getParameter("screenType"));
	}else{
		strscreenType =screenType;
	}
	strscrType = GmCommonClass.parseNull(request.getParameter("hType"));
	HashMap hcboVal = null;
	
	if (hmNames != null)
	{
		alDistributor = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("DISTLIST"));
		alRepList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("REPLIST"));
		alAccList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("ACCLIST"));
		alEmpList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("EMPLIST"));
		alPlantList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("PLANTLIST"));
		alDealerList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("DEALERLIST"));
		alAssociateSalesRepList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("ASSOCIATEREPLIST"));
		alShipacctList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("SHIPACCTLIST"));
		// Getting Ship Mode values for the Different SHip Carriers 
		alFEDModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("FEDEXMODELIST"));
		alUPSModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("UPSMODELIST"));
		alCurrierModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("COURIERMODELIST"));
		alOtherModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("OTHERMODELIST"));
		alTNTModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("TNTMODELIST"));
		alDHLModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("DHLMODELIST"));
		// Getting Default Ship Mode values for the Different SHip Carriers 
		strFedExDefltMode = GmCommonClass.parseNull((String)hmNames.get("FEDEXDEFMODE"));
		strUPSDefltMode = GmCommonClass.parseNull((String)hmNames.get("UPSDEFMODE"));
		strCourierDefltMode = GmCommonClass.parseNull((String)hmNames.get("COURIERDEFMODE"));
		strTNTDefltMode = GmCommonClass.parseNull((String)hmNames.get("TNTDEFMODE"));
		strDHLDefltMode = GmCommonClass.parseNull((String)hmNames.get("DHLDEFMODE"));
		strOtherDefltMode = GmCommonClass.parseNull((String)hmNames.get("OTHERDEFMODE"));	
	}

%> 
<HTML>
<HEAD>

<TITLE> Globus Medical: Shipping Include </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
 <script language="JavaScript" src="<%=strJsPath%>/GmIncTissueShipDetails.js"></script>  

<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script>
var DistLen = <%=alDistributor.size()%>;
var RepLen = <%=alRepList.size()%>;
var AccLen = <%=alAccList.size()%>;
var EmpLen = <%=alEmpList.size()%>;
var PlantLen = <%=alPlantList.size()%>;
var DealerLen = <%=alDealerList.size()%>;
var shipModeLength = <%=alShipMode.size()%>;

// Setting the resulted Ship Mode array length to the javascript variables...
var FedExModeLen = <%=alFEDModeList.size()%>;
var UPSModeLen = <%=alUPSModeList.size()%>;
var CurrierModeLen = <%=alCurrierModeList.size()%>;
var OtherModeLen = <%=alOtherModeList.size()%>;
var TNTModeLen = <%=alTNTModeList.size()%>;
var DHLModeLen = <%=alDHLModeList.size()%>;

var AssociateSalesRepLen = <%=alAssociateSalesRepList.size()%>;
var ShipacctLen = <%=alShipacctList.size()%>;
var addid = '<%=tissueAddressId%>';
var tissueNamesid = '<%=tissueNames%>';
var tissueShipto = '<%=tissueShipTo%>';
var scrtype = '<%=strscrType%>';
var tissueShipmode = '<%=tissueShipMode%>';
var tissueShipCarrier = '<%=tissueShipCarrier%>';
var screenName = '<%=strscreenType%>';

//Setting the resulted default Ship Mode values to the javascript variables...
var fedExMode = '<%=strFedExDefltMode%>';
var upsMode = '<%=strUPSDefltMode%>';
var tntMode = '<%=strTNTDefltMode%>';
var dhlMode = '<%=strDHLDefltMode%>';
var courierExMode = '<%=strCourierDefltMode%>';
var otherMode = '<%=strOtherDefltMode%>';
var currencyFmt = '<%=strCurrencyFmt%>';
var editChrgFl = '<%=strEditChargeFl%>';
<%
hcboVal = new HashMap();
for (int i=0;i<alShipacctList.size();i++)
{
	hcboVal = (HashMap)alShipacctList.get(i);
%>
var ShipAcctArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>
<%
	hcboVal = new HashMap();
	for (int i=0;i<alDistributor.size();i++)
	{
		hcboVal = (HashMap)alDistributor.get(i);
%>
	var DistArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

<%	
	hcboVal = new HashMap();
//Adding dealer id for ship to type as dealer
	for (int i=0;i<alAccList.size();i++)
	{
		hcboVal = (HashMap)alAccList.get(i);
%>
<%if(strscreenType.equals("HospitalInitiate")){%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("ID")%>";
<%}else{%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("DID")%>,<%=hcboVal.get("DEALID")%>";	
<%}%>

<%
	}
%>
<%
	hcboVal = new HashMap();
	for (int i=0;i<alEmpList.size();i++)
	{
		hcboVal = (HashMap)alEmpList.get(i);
%>
	var EmpArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("PARTYID")%>";
<%
	}
%>


<%
	hcboVal = new HashMap();
	for (int i=0;i<alRepList.size();i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("PARTYID")%>,<%=hcboVal.get("DID")%>";
<%
	}
%>

<%
	hcboVal = new HashMap();
	for (int i=0;i<alAssociateSalesRepList.size();i++)
	{
		hcboVal = (HashMap)alAssociateSalesRepList.get(i);
%>
	var AssoRepArr<%=i%> ="<%=hcboVal.get("ASSOCREPNM")%>,<%=hcboVal.get("ASSOCREPID")%>,<%=hcboVal.get("REPID")%>,<%=hcboVal.get("REPNM")%>";
<%
}
%>	
// The Following Code is Added to Form the Plant Array List values 
<%
hcboVal = new HashMap();
for (int i=0;i<alPlantList.size();i++)
{
	hcboVal = (HashMap)alPlantList.get(i);
%>
var PlantArr<%=i%> ="<%=hcboVal.get("PLANTID")%>,<%=hcboVal.get("PLANTNM")%>"; 

<%
}
%>	

//The Following Code is Added to Form the Dealer Array List values 
<%
hcboVal = new HashMap();
for (int i=0;i<alDealerList.size();i++)
{
	hcboVal = (HashMap)alDealerList.get(i);
%>
var dlrar<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>"; 
<%
}
%>	

// The Following Code is Added to Form the SHIP MODE Array List values based on
// the Corresponding SHIP CARRIER VALUES to make use in javascript.

<% hcboVal = new HashMap();
for (int i=0;i<alFEDModeList.size();i++){
	hcboVal = (HashMap)alFEDModeList.get(i); %>
var FedModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>

<% hcboVal = new HashMap();
for (int i=0;i<alUPSModeList.size();i++){
	hcboVal = (HashMap)alUPSModeList.get(i); %>
var UPSModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>

<% hcboVal = new HashMap();
for (int i=0;i<alCurrierModeList.size();i++){
	hcboVal = (HashMap)alCurrierModeList.get(i); %>
var CurrierModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% }%>

<% hcboVal = new HashMap();
for (int i=0;i<alOtherModeList.size();i++){
	hcboVal = (HashMap)alOtherModeList.get(i); %>
var OtherModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>

<% hcboVal = new HashMap();
for (int i=0;i<alTNTModeList.size();i++){
	hcboVal = (HashMap)alTNTModeList.get(i); %>
var TNTModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>

<% hcboVal = new HashMap();
for (int i=0;i<alDHLModeList.size();i++){
	hcboVal = (HashMap)alDHLModeList.get(i); %>
var DHLModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>

<% hcboVal = new HashMap();
for (int i=0;i<alShipMode.size();i++){
	hcboVal = (HashMap)alShipMode.get(i); %>
var AllModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>
</script>

</HEAD>


<input type="hidden" name="addIdReturned" />
<%-- <html:hidden property="screenType" name="frmShipDetails" value="<%=strscreenType%>"/> --%>
<html:hidden property="tissueShipId" name="frmShipDetails" />
<html:hidden property="hTissueAddrId" name="frmShipDetails" />
<table border="0" width="100%"  cellspacing="0" cellpadding="0">	
		<tr class="ShadeRightTableCaption">
			<td height="25"  colspan="4"><fmtShipDtls:message key="LBL_SHIP_DTL_TISSUE_PARTS"/>:</td>			
		</tr>	
		<tr><td class="line" colspan="4"></td></tr>
			
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<%if(strDisplayfl.equals("Y")||strDisplayfl.equals("N")){ %>
		<tr class="Shade">
			<td colspan="4">
			<table border="0" width="100%"  cellspacing="0" cellpadding="0">
			<tr>
				<td width="12%" class="RightTableCaption" height="25" align="right">&nbsp;<fmtShipDtls:message key="LBL_TO"/> :</td>
				<td width="30%">&nbsp;<gmjsp:dropdown controlName="tissueShipTo"  SFFormName='frmShipDetails' SFSeletedValue="tissueShipTo"  defaultValue= "[Choose One]"	
					width="230" SFValue="alShipTo" codeId="CODEID" codeName="CODENM" onChange="javascript:fnGetAssocRepNamesTissue(this,'ship');"/>
				</td>
				<!-- Struts tag lib code modified for JBOSS migration changes -->
				<td class="RightTableCaption" align="right"><fmtShipDtls:message key="LBL_TEMP_OVER_ATTN_TO"/>:  </td>
				<td>&nbsp;<html:text property="tissueOverridenAttnTo" name="frmShipDetails" size="30" styleClass="InputArea"
						 onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>
				</td>
			</tr>
			</table>
		    </td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
		    <td  width="12%"  class="RightTableCaption" height="25" align="right"><fmtShipDtls:message key="LBL_NAMES"/>:</td>
    
				 <td colspan="3">&nbsp;<gmjsp:dropdown controlName="tissueNames"  SFFormName='frmShipDetails' SFSeletedValue="tissueNames"  defaultValue= "[Choose One]" optionId="PARTYID"
						 SFValue="alShipToId" codeId="ID" codeName="NAME" onChange="javascript:fnGetAddressListTissue(this,'')"/> 
						&nbsp;<input type="checkbox" name="tissueChkNames" onclick="javascript:fnGetAllNamesTissue();" title="Load All Names"/>&nbsp;<fmtShipDtls:message key="LBL_SHOW_ALL"/>
					</td>
		     	
		</tr>
		<tr><td  colspan="4" height="1" bgcolor="#cccccc"></td></tr>		
		<tr class="Shade">		
			<td  width="12%"  class="RightTableCaption" HEIGHT="25" align="right"><fmtShipDtls:message key="LBL_ADDRESS"/>:</td>
			<td width="61%" colspan="1" valign="middle">&nbsp;<span style="vertical-align:middle;"><select id="tissueAddressId" name="tissueAddressId" style="width:400px" onchange="fnChangeAddressTissue(this)"></select>
				</span> 
				<%if(!strDisplayfl.equals("Y")) {
				%>
				 <img style='cursor:hand' align="middle" onClick="<%=strDisplayfl.equals("N")?"fnAreasetOpenAddress();":"fnOpenAddressTissue();"%>" src='<%=strImagePath%>/Address-Book-icon.png' title="<fmtShipDtls:message key="IMG_ALT_ADDR_LOOKUP"/>" height="26" width="26">	
			<%}else{ %>	          
		          <img style='cursor:hand' align="middle" onClick="<%=strDisplayfl.equals("N")?"fnAreasetOpenAddress();":"fnOpenAddressTissue();"%>" src='<%=strImagePath%>/Address-Book-icon.png' title="<fmtShipDtls:message key="IMG_ALT_ADDR_LOOKUP"/>" height="26" width="26">
		    <%}%>
		    </td>
		<%if(strDisplayfl.equals("N")) { %>
			<td  width="12%"  class="RightTableCaption" HEIGHT="25" align="right"><fmtShipDtls:message key="LBL_MODE"/>:</td>
			<td >&nbsp;<gmjsp:dropdown controlName="tissueShipMode"  SFFormName='frmShipDetails' SFSeletedValue="tissueShipMode"  defaultValue= "[Choose One]"	
						SFValue="alShipMode" codeId="CODEID" codeName="CODENM"/>      
		    </td>
		<%} else{%>
		<td colspan="2"></td>
		<%} %>		    		    
		</tr>
		<%if(strDisplayfl.equals("N")) { %>
			<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
			<tr class="Shade">
				<td height="1" HEIGHT="25" align="right">&nbsp;<html:checkbox property="chkAppFl"></td>
				<td colspan="3" HEIGHT="25" >&nbsp;&nbsp;<fmtShipDtls:message key="LBL_APPLY_ALL"/>&nbsp;&nbsp;</html:checkbox></td>
			</tr>
		<%} %>
	
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<tr> <td colspan =4> <table border="0" width="100%"  cellspacing="0" cellpadding="0"> <tr>
		<%}if(strDisplayfl.equals("Y")||strDisplayfl.equals("S")){ %>
			<td width="12%" class="RightTableCaption" HEIGHT="25" align="right"><fmtShipDtls:message key="LBL_CARRIER"/>:</td>
			<td width="37%">&nbsp;<gmjsp:dropdown controlName="tissueShipCarrier"  SFFormName='frmShipDetails' SFSeletedValue="tissueShipCarrier"  defaultValue= "[Choose One]"	
						SFValue="alShipCarr" codeId="CODEID" codeName="CODENM" onChange="javascript:fnGetShipModesTissue(this)"/>  
		    </td>
		    <%if(!strDisplayfl.equals("S")){ %>
		    <td  width="10%"  class="RightTableCaption" HEIGHT="25" align="right"><fmtShipDtls:message key="LBL_MODE"/>:</td>
		    <td  >&nbsp;<gmjsp:dropdown controlName="tissueShipMode"  SFFormName='frmShipDetails' SFSeletedValue="tissueShipMode"  defaultValue= "[Choose One]"	
						SFValue="alShipMode" codeId="CODEID" codeName="CODENM" onChange="javascript:fnCalcShipChargeTissue()"/>      
		    </td>
		    <%}else{%>
		    <td  width="10%"  class="RightTableCaption" HEIGHT="25" align="right">&nbsp; <fmtShipDtls:message key="LBL_TRACK"/> #:</td>
			<td>&nbsp;
		           <html:text property="trackingNo"
						name="frmShipDetails" size="30" 
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');"
						onkeypress="validateTrackingNum();"/>      
		    </td>
		    <%}%>
		</tr> </table> </td> </tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<tr class="shade" height="40">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="200">&nbsp;<fmtShipDtls:message key="LBL_SHIP_INSTR"/>:</td>
			<td colspan="3">&nbsp;<html:textarea property="tissueShipInstruction" cols="80" name="frmShipDetails"
					styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>	
		</tr>
		<%if(strShpChrgFl.equalsIgnoreCase("Y")){ %>
		<tr><td colspan="4"><table border="0" width="100%"  cellspacing="0" cellpadding="0"><tr>
			<td class="RightTableCaption" align="right" HEIGHT="24" width="59%">&nbsp;<fmtShipDtls:message key="LBL_SHIP_CAHRGE"/>:&nbsp;</td>
			
			<%if(strEditChargeFl.equalsIgnoreCase("Y")){ %>
			<td width="41%">
				<html:text property="shipCharge" name="frmShipDetails" styleClass="InputArea"
							 onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
			<%}else{ %>
			<td width="21%" id="lblShipCharge" class="RightText"></td>
			<td width="20%"><input type="hidden" name="shipCharge"></td>
			<%} %>
			</tr></table></td>
		</tr>
  <%}else{%>
		<input type="hidden" name="shipCharge">
   <% } %>
     <%} if(!strDisplayfl.equals("Y")) { %>
		<tr>
				<td colspan="4" height="50" align="center">
				<fmtShipDtls:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button name="Btn_Submit" value="${varSubmit}" gmClass="button" buttonType="Load" onClick="fnGo();" />
				&nbsp;&nbsp;&nbsp;
				<fmtShipDtls:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="${varClose}" gmClass="button" buttonType="Load" onClick="fnClose()" />&nbsp;&nbsp;&nbsp;  
				</td>
		</tr>
			<%} %>
</table>
<%@ include file="/common/GmFooter.inc"%>