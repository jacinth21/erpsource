 <%
/**********************************************************************************
 * File		 		: GmShippingTrans.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Dhinakaran James
 *-----------------------------------------
 * File (Renamed)   : GmShippingReport.jsp 
 * version          : 1.1
 * author           : bvidyasankar
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtShippingReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmShippingReport.jsp -->
<fmtShippingReport:setLocale value="<%=strLocale%>"/>
<fmtShippingReport:setBundle basename="properties.labels.custservice.Shipping.GmShippingReport"/>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %> 
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@page import="java.util.Date"%>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strApplnDateFmt = strGCompDateFmt;
	
	String strWikiTitle = GmCommonClass.getWikiTitle("SHIP_DASHBOARD");
	String strTodaysDate = "";
	Date dtToday = GmCommonClass.getCurrentDate(strApplnDateFmt);
	strTodaysDate = GmCommonClass.getStringFromDate(dtToday,strApplnDateFmt);	
	HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	String gridData = (String)request.getAttribute("REPORT")==null?"":(String)request.getAttribute("REPORT");
	String strDeptId = (String)request.getAttribute("DEPTID")==null?"":(String)request.getAttribute("DEPTID");
	String strhCntR = (String)request.getAttribute("hCntRows");
	ArrayList alShippingSource = new ArrayList();
	ArrayList alShippingCarrier = new ArrayList();
	ArrayList alShipTo = new ArrayList();
	ArrayList alStatus = new ArrayList();
	ArrayList alDtTyp = new ArrayList();	
	ArrayList alShipType = new ArrayList();
	ArrayList alShipVal = new ArrayList();
	
	String strUserId = (String) session.getAttribute("strSessUserId");
	String strCodeID = "";
	String strSelected = "";
	
	String strSourceSelected = ""; 
	String strRefIDEntered = "";
	String strReqIDEntered = "";
	String strStatusSelected = "";
	String strPendingForEntered = "";
	String strCarrierSelected = "";
	String strShipToSelected= "";
	String strNamesEntered ="";
	String strFromDateEntered = "";
	String strToDateEntered = "";
	String strDtTypSelected="";
	String strHoldSts="";
	int intCount = 0;
	int intSize = 0;
	HashMap hcboVal;
	double lngTotalShipVal = 0;
	HashMap hmLoop = new HashMap();
	String strPrice = "";
	java.util.Date dtFrom = null;
	java.util.Date dtTo	= null; 
	String strOptCol = "";
	
	if (hmReturn != null) 
	{
		alShippingSource = (ArrayList)hmReturn.get("SOURCELIST");
		alShippingCarrier = (ArrayList)hmReturn.get("CARRIERLIST");
		alShipTo = (ArrayList)hmReturn.get("SHIPTOLIST");
		alStatus = (ArrayList)hmReturn.get("STATUSLIST");
		alDtTyp = (ArrayList)hmReturn.get("DTTYPE");
		// For Display total shipment value value only completed status
		alShipVal = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SHIPMENTVAL"));
		// Ship type disaplaying in Drop down.
		alShipType = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SHIPTYPE")); 
		
		if(alShipVal.size() > 0){
			for (int i = 0; i < alShipVal.size(); i++){
				hmLoop = (HashMap)alShipVal.get(i);
				strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
				lngTotalShipVal = lngTotalShipVal +  Integer.parseInt(strPrice);
			}
		}
		 
		if (strAction.equals("Reload"))
		{	 
			strSourceSelected = GmCommonClass.parseNull((String)hmReturn.get("hSource"));
			strRefIDEntered = GmCommonClass.parseNull((String)hmReturn.get("hRefID"));
			strReqIDEntered = GmCommonClass.parseNull((String)hmReturn.get("hReqID"));
			strPendingForEntered = GmCommonClass.parseNull((String)hmReturn.get("hPendingFor"));
			strCarrierSelected = GmCommonClass.parseNull((String)hmReturn.get("hCarrier"));
			strShipToSelected = GmCommonClass.parseNull((String)hmReturn.get("hShipTo"));
			strNamesEntered = GmCommonClass.parseNull((String)hmReturn.get("hNames"));
			dtFrom	= (Date) hmReturn.get("hFromDate");
			dtTo = (Date) hmReturn.get("hToDate");  
			
			strDtTypSelected = GmCommonClass.parseNull((String)hmReturn.get("hDtTyp"));
			strDtTypSelected = GmCommonClass.parseNull((String)hmReturn.get("hDtTyp"));
			strHoldSts = GmCommonClass.parseNull((String)hmReturn.get("hHoldSts"));
			strHoldSts = strHoldSts.equals("on")?"checked":"";
		}
	}
	String strCookieSource = GmCommonClass.parseNull((String)request.getAttribute("SOURCE_COOKIE"));
	if (!strCookieSource.equals("")) {
	strSourceSelected = strCookieSource;
	}
	
	strOptCol = (String)request.getAttribute("OPTCOL");
	
	strStatusSelected = GmCommonClass.parseNull((String)request.getAttribute("status"));
	HashMap hmMapStatus = new HashMap();
	hmMapStatus.put("ID","");
	hmMapStatus.put("PID","CODENMALT");
	hmMapStatus.put("NM","CODENM");
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Search</TITLE> 
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script language="JavaScript" src="<%=strOperationsJsPath%>/GmCommonShippingReport.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmShippingReport.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">


<!-- MNTTASK-3518 Stack Overflow commented below line -->
<%-- <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script> --%>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script>
var todaysdate = '<%=strTodaysDate%>'
var strServletPath =  '<%=strServletPath%>';
var strAction =  '<%=strAction%>';
var dateFmt = '<%=strApplnDateFmt%>';
var CboShipStatus = '';
var ChkboxRefId = '';
var objGridData = '<%=gridData%>';
var deptid = '<%=strDeptId%>';
var lblShipValue = '<fmtShippingReport:message key="LBL_SHIP_VAL_DISPLAY"/>';
var lblPlanShipDt = '<fmtShippingReport:message key="LBL_CHANGE_SHIP_DT"/>';
</script>
</HEAD>
<style>
table.GtTable1100 {
	margin: 0 0 0 0;
	width: 1125px;
	border: 1px solid  #676767;
}
</style>
<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" onLoad="fnOnPageLoad()">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmShippingReportServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hRowCount" value="">
<input type="hidden" name="strConsignIds" value="">
<input type="hidden" name="hStatus" value="<%=strStatusSelected%>">
<input type="hidden" name="hchkOptCol" value="<%=strOptCol%>">

	<table border="0" class= "GtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtShippingReport:message key="LBL_SHIPPING_DASHBOARD"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
				<fmtShippingReport:message key="LBL_HELP" var="varHelp"/>
			    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		<tr>
			<td colspan="5" height="20">
			<jsp:include page="/sales/GmSalesFilters.jsp" >
				<jsp:param name="FRMNAME" value="frmAccount" />
				<jsp:param name="HACTION" value="Reload"/>
				<jsp:param name="HIDE" value="SYSTEM" />				
			</jsp:include>
			</td>
		</tr>		
		<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
		<tr> 
		    <td colspan="2">
		        <table border="0" width = "100%" cellspacing="0" cellpadding="0">
			<!-- Custom tag lib code modified for JBOSS migration changes -->
		            <tr>
		            <!--Added as part of Shipping Dashboard modification by bvidyasankar on 12/10/2008-->
		                <td class="RightTableCaption" height="25" align="right" valign="top">&nbsp;<fmtShippingReport:message key="LBL_SOURCE"/>:</td>
						<td valign="top">&nbsp;<gmjsp:dropdown controlName="Cbo_ShippingSource"  
							   value="<%=alShippingSource%>" seletedValue="<%=strSourceSelected%>" codeId = "CODEID"   codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="1"/>
						</td>
						<td class="RightTableCaption" height="25" align="right" valign="top"><fmtShippingReport:message key="LBL_REF_ID"/>:</td>
					 	<td valign="top">&nbsp;<input type="text" size="20" value="<%=strRefIDEntered%>"  onBlur="changeBgColor(this,'#ffffff');"
					 			name="Txt_RefId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onkeypress="fnCallGo()" tabindex="2"/>
						</td>
						<td class="RightTableCaption" height="25" align="right" valign="top">&nbsp;<fmtShippingReport:message key="LBL_STATUS"/>:</td>
						<td><table border="0" cellspacing="5" cellpadding="0" width="100%">
							<tr>
								<td><%=GmCommonControls.getChkBoxGroup ("setDate(this)",alStatus,"Status",hmMapStatus)%></td>
							</tr>
							</table>
						</td>
		            </tr>
		           <tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>		
		            <tr class="shade">
		                <td class="RightTableCaption" height="25" align="right" >&nbsp;<fmtShippingReport:message key="LBL_PENDING_FOR"/>:</td>
					 	<td>&nbsp;<input type="text" size="5" value="<%=strPendingForEntered%>"  width="250" onBlur="changeBgColor(this,'#ffffff');"
					 			name="Txt_PendingDays" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" tabindex="4"/><font class="RightText">&nbsp;<fmtShippingReport:message key="LBL_NO_OF_DAYS"/></font>
						</td>
						<td class="RightTableCaption" height="25" align="right" ><fmtShippingReport:message key="LBL_CARRIER"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_ShippingCarrier"  
							   value="<%=alShippingCarrier%>" seletedValue="<%=strCarrierSelected%>" codeId = "CODEID"   codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="5"/>
						</td>
						<td class="RightTableCaption" height="25" align="right" >&nbsp;<fmtShippingReport:message key="LBL_HOLD_TXN"/>:</td>
						<td >&nbsp;<input type="checkbox" name="Cbo_HoldSts" <%=strHoldSts%>></input>
						</td>
		            </tr>
		           <tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
		            <tr>
		                <td class="RightTableCaption" height="25" align="right">&nbsp;<fmtShippingReport:message key="LBL_SHIP_TO"/>:</td>
		                <td>&nbsp;<gmjsp:dropdown controlName="Cbo_ShipTo"  
							   value="<%=alShipTo%>" codeId = "CODEID"  seletedValue="<%=strShipToSelected%>" codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="6"/>
						</td>
						<td class="RightTableCaption" height="30" align="right"><fmtShippingReport:message key="LBL_NAMES"/>:</td>
					 	<td>&nbsp;<input type="text" size="20" value="<%=strNamesEntered%>"  width="250" onBlur="changeBgColor(this,'#ffffff');"
					 			name="Txt_Names" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" tabindex="7"/>
						</td>
						<td class="RightTableCaption" height="25" align="right"><fmtShippingReport:message key="LBL_REQUEST_ID"/>:</td>
						<td>&nbsp;<input type="text" size="20" value="<%=strReqIDEntered%>"  onBlur="changeBgColor(this,'#ffffff');"
							name="Txt_ReqId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" tabindex="8"/>
						<fmtShippingReport:message key="BTN_PRINT_ALL" var="varPrintAll"/>	
						<gmjsp:button value="${varPrintAll}" gmClass="Button" onClick="javascript:fnPrintAll();" tabindex="9" buttonType="Load" />
						
		            </tr>
		           <tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
		            <tr class="shade">
		                <td HEIGHT="50" class="RightTableCaption" align="Right"><fmtShippingReport:message key="LBL_DATE_RANGE"/>:</td>
		                <td class="RightText" colspan="2">&nbsp;<fmtShippingReport:message key="LBL_TYPE"/>:&nbsp;
			                <gmjsp:dropdown controlName="Cbo_Date" value="<%=alDtTyp%>" codeId = "CODEID"  seletedValue="<%=strDtTypSelected%>" onChange = "fnSetDate(this.value)" codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="10"/>&nbsp;		                		                
								   <BR><BR>&nbsp;<fmtShippingReport:message key="LBL_FROM"/>:
								<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(dtFrom==null)?null:new java.sql.Date(dtFrom.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>    
								  
					 		&nbsp;<fmtShippingReport:message key="LBL_TO"/>:		
					 		<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(dtTo==null)?null:new java.sql.Date(dtTo.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/> 		 	
					 		 	 &nbsp;&nbsp;
					 	</td>
					 	<td colspan="4">&nbsp;	
					 	<fmtShippingReport:message key="BTN_LOAD" var="varLoad"/>			
			 			<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="Button" onClick="javascript:fnReload(this);" tabindex="13" buttonType="Load" /><BR>				
					 	</td>
		            </tr>
		            <tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
		            <tr>
		            	<td class="RightTableCaption" height="25" align="right">&nbsp;<fmtShippingReport:message key="LBL_OPTIONAL_COLUMNS"/>:</td>
		            	<td colspan="5">
		            		&nbsp;<input type="checkbox" name="chkOptCol" value="chk_source">&nbsp;<fmtShippingReport:message key="LBL_SOURCE"/>
		            		&nbsp;<input type="checkbox" name="chkOptCol" value="chk_shipCarrier">&nbsp;<fmtShippingReport:message key="LBL_SHIP_CARRIER"/>
		            		&nbsp;<input type="checkbox" name="chkOptCol" value="chk_initiatDate">&nbsp;<fmtShippingReport:message key="LBL_INTIATED_DATE"/>
		            		&nbsp;<input type="checkbox" name="chkOptCol" value="chk_crBy">&nbsp;<fmtShippingReport:message key="LBL_CR._BY"/>
		            		&nbsp;<input type="checkbox" name="chkOptCol" value="chk_compBy">&nbsp;<fmtShippingReport:message key="LBL_COMPLETED_BY"/>
		            		&nbsp;<input type="checkbox" name="chkOptCol" value="chk_master">&nbsp;<fmtShippingReport:message key="LBL_MASTER"/>
		            	</td>
		            </tr>		          
		        </table>
		    </td> 
		</tr>
		<%if(!strhCntR.equals("") && !strhCntR.equals("0")){ %>
		</table> 
		<table  border="0" class= "GtTable1100" cellspacing="0" cellpadding="0">
			<tr>
				<td><div id="grpData" style="grid" height="500px" width="1125">&nbsp;</div></td> 
				</tr>  <tr class="shade">
		                <td colspan="1" class="RightTableCaption" height="25" align="center" >&nbsp;<fmtShippingReport:message key="LBL_CHOOSE_ACTION"/>: 
						&nbsp;<gmjsp:dropdown controlName="Cbo_Shippment_Type"  
							   value="<%=alShipType%>" seletedValue="<%=strCarrierSelected%>" codeId = "CODEID"   codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="5"/>
						<fmtShippingReport:message key="BTN_SUBMIT" var="varSubmit"/>
						&nbsp;<gmjsp:button value="&nbsp;${varSubmit}&nbsp;"  gmClass="Button" onClick="javascript:fnSubmit();" tabindex="13" buttonType="Save" /></td>
		            </tr>
		            <tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr> 
			<tr>
	              <td colspan="6" align="center">&nbsp;
	              <div class='exportlinks'><fmtShippingReport:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
	                              onclick="fnExportExcel();"> <fmtShippingReport:message key="LBL_EXCEL"/> </a>
	                            </div>
	              </td>
			</tr>
			<tr><td>&nbsp;</td></tr>			
		</table>
		<%}else{%>
			<table border="0" class= "GtTable1100" cellspacing="0" cellpadding="0">
			<tr><td colspan="6" align="center" height="30" class="RightTableCaption"><font color="red"><fmtShippingReport:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/></font></td></tr></table>
		<%}%>
<input type="hidden" name="hCnt" value="<%=intCount%>"/>
<input type="hidden" name="hCntR" value="<%=strhCntR%>"/>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
