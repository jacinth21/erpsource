                                                                               <%
	/**********************************************************************************
	 * File		 		: GmFGBINTransList.jsp
	 * Desc		 		: This screen is used to view the details of FG BIN transactions
	 * Version	 		: 
	 * author			: arajan
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtFGBINTransList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\shipping\GmFGBINTransList.jsp -->

<fmtFGBINTransList:setLocale value="<%=strLocale%>"/>
<fmtFGBINTransList:setBundle basename="properties.labels.operations.shipping.GmFGBINTransList"/>

<bean:define id="gridData" name="frmFGBINTrans" property="gridXmlData" type="java.lang.String"> </bean:define>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("FGBIN_DETAILS");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: FG BIN Details</TITLE>

</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmFGBINTransList.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>

<script>
	var objGridData;
	objGridData = '<%=gridData%>';
</script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">

	<html:form action='/gmFGBINTrans.do?method=fetchFGBINTransactions'>
		<html:hidden property="strOpt"/>

		<table class="DtTable680" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader">&nbsp;<fmtFGBINTransList:message key="LBL_FG_BIN_DETAILS"/></td>
				<td align="right" class=RightDashBoardHeader><fmtFGBINTransList:message key="IMG_ALT_HELP" var = "varHelp"/>
				<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<!-- Struts tag lib code modified for JBOSS migration changes --> 
						<tr>
							<td class="RightTableCaption" HEIGHT="30" align="right" width="45%">
								<fmtFGBINTransList:message key="LBL_SCAN_ID" var="varScanId"/>
								<gmjsp:label type="RegularText" SFLblControlName="${varScanId}" td="false" />
								<html:text property="scanId" styleClass="InputArea" name="frmFGBINTrans"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" 
								onkeypress="javascript:fnLoad(this)" tabindex="1"/>
							</td> 
							<td>&nbsp;
								<fmtFGBINTransList:message key="BTN_LOAD" var="varLoad"/>
								<gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" onClick="javascript:fnLoad(this);" tabindex="2" buttonType="Load" />
							</td>
						</tr>
					</table>
				</td>
			</tr>			
			
			<tr><td class="LLine" height="1" colspan="2"></td></tr>	
			<%if(gridData.indexOf("<row>") != -1){ %>
				<tr>
					<td colspan="2"><div  id="dataGridDiv" style="" height="400px"></div></td>
				</tr>						
			<%}else {%>
				<tr class="Shade"><td colspan="2" align="center" class="RightText" ><fmtFGBINTransList:message key="MSG_NO_DATA"/></td></tr>
			<%} %>

		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

