<%
/**********************************************************************************
 * File		 		: GmIncShipDetails.jsp
 * Desc		 		: Common Include for Shipping
 * This JSP is a common include JSP used in multiple screens. when this file is modified,
 * we have to verify the same in all the places.
 *    
 * Following is the link, that explains all the screens that is using this file.
 *	http://confluence.spineit.net/display/ITAPP/Screens+that+has+common+shipping+Include
 *    
 * The above link also explains the design of the shipping functionality.
 *    and also, please update the above link with the details of the change that you 
 *    are making in this file.
 * 
 * owner			: vprasath
************************************************************************************/
%>

<!-- \operations\purchasing\shipping\GmIncShipDetails.jsp-->  
   
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.operations.shipping.forms.GmShipDetailsForm"%>
<%@ taglib prefix="fmtShipDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtShipDtls:requestEncoding value="UTF-8" />
<fmtShipDtls:setLocale value="<%=strLocale%>"/>
<fmtShipDtls:setBundle basename="properties.labels.operations.shipping.GmIncShipDetails"/>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<bean:define id="addid" name="frmShipDetails" property="addressid" type="java.lang.String"> </bean:define>
<bean:define id="namesid" name="frmShipDetails" property="names" type="java.lang.String"> </bean:define>
<bean:define id="shipto" name="frmShipDetails" property="shipTo" type="java.lang.String"> </bean:define>
<bean:define id="screenType" name="frmShipDetails" property="screenType" type="java.lang.String"> </bean:define>
<bean:define id="shipMode" name="frmShipDetails" property="shipMode" type="java.lang.String"> </bean:define>
<bean:define id="shipCarrier" name="frmShipDetails" property="shipCarrier" type="java.lang.String"> </bean:define>
<bean:define id="alShipMode" name="frmShipDetails" property="alShipMode" type="java.util.ArrayList"> </bean:define>
<bean:define id="strEditChargeFl" name="frmShipDetails" property="editShipChrgFl" type="java.lang.String"> </bean:define>
<%
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strscreenType ="";
String strscrType ="";
HashMap hmNames = new HashMap();
hmNames =	(HashMap)request.getAttribute("NAMES");
	ArrayList alDistributor = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alAccList = new ArrayList();
	ArrayList alEmpList = new ArrayList();
	ArrayList alPlantList = new ArrayList();
	ArrayList alDealerList = new ArrayList();
	
	ArrayList alFEDModeList = new ArrayList();
	ArrayList alUPSModeList = new ArrayList();
	ArrayList alCurrierModeList = new ArrayList();
	ArrayList alOtherModeList = new ArrayList();
	ArrayList alTNTModeList = new ArrayList();
	ArrayList alDHLModeList = new ArrayList();
	
	String strFedExDefltMode = "";
	String strUPSDefltMode = "";
	String strCourierDefltMode = "";
	String strTNTDefltMode = "";
	String strDHLDefltMode = "";
	String strOtherDefltMode = "";
	
	
	ArrayList alAssociateSalesRepList = new ArrayList();
	ArrayList alShipacctList = new ArrayList();
	String strDisplayfl = GmCommonClass.parseNull(request.getParameter("DISPLAYFL")) == "" ? "Y" :GmCommonClass.parseNull(request.getParameter("DISPLAYFL"));
	String strThirdParty = GmCommonClass.parseNull(request.getParameter("ThirdParty"));	
	// To know whether to show the Calculated Shipping charge field or not
	String strShpChrgFl = GmCommonClass.parseNull((String)request.getParameter("SHIPCHRG"));
	String strCurrencyFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	if(screenType.equals("")){
		strscreenType = GmCommonClass.parseNull(request.getParameter("screenType"));
	}else{
		strscreenType =screenType;
	}
	strscrType = GmCommonClass.parseNull(request.getParameter("hType"));
	HashMap hcboVal = null;
	
	if (hmNames != null)
	{
		alDistributor = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("DISTLIST"));
		alRepList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("REPLIST"));
		alAccList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("ACCLIST"));
		alEmpList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("EMPLIST"));
		alPlantList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("PLANTLIST"));
		alDealerList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("DEALERLIST"));
		alAssociateSalesRepList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("ASSOCIATEREPLIST"));
		alShipacctList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("SHIPACCTLIST"));
		// Getting Ship Mode values for the Different SHip Carriers 
		alFEDModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("FEDEXMODELIST"));
		alUPSModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("UPSMODELIST"));
		alCurrierModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("COURIERMODELIST"));
		alOtherModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("OTHERMODELIST"));
		alTNTModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("TNTMODELIST"));
		alDHLModeList = GmCommonClass.parseNullArrayList((ArrayList)hmNames.get("DHLMODELIST"));
		// Getting Default Ship Mode values for the Different SHip Carriers 
		strFedExDefltMode = GmCommonClass.parseNull((String)hmNames.get("FEDEXDEFMODE"));
		strUPSDefltMode = GmCommonClass.parseNull((String)hmNames.get("UPSDEFMODE"));
		strCourierDefltMode = GmCommonClass.parseNull((String)hmNames.get("COURIERDEFMODE"));
		strTNTDefltMode = GmCommonClass.parseNull((String)hmNames.get("TNTDEFMODE"));
		strDHLDefltMode = GmCommonClass.parseNull((String)hmNames.get("DHLDEFMODE"));
		strOtherDefltMode = GmCommonClass.parseNull((String)hmNames.get("OTHERDEFMODE"));	
	}

%> 
<HTML>
<HEAD>

<TITLE> Globus Medical: Shipping Include </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmIncShipDetails.js"></script>

<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script>
var DistLen = <%=alDistributor.size()%>;
var RepLen = <%=alRepList.size()%>;
var AccLen = <%=alAccList.size()%>;
var EmpLen = <%=alEmpList.size()%>;
var PlantLen = <%=alPlantList.size()%>;
var DealerLen = <%=alDealerList.size()%>;
var shipModeLength = <%=alShipMode.size()%>;

// Setting the resulted Ship Mode array length to the javascript variables...
var FedExModeLen = <%=alFEDModeList.size()%>;
var UPSModeLen = <%=alUPSModeList.size()%>;
var CurrierModeLen = <%=alCurrierModeList.size()%>;
var OtherModeLen = <%=alOtherModeList.size()%>;
var TNTModeLen = <%=alTNTModeList.size()%>;
var DHLModeLen = <%=alDHLModeList.size()%>;

var AssociateSalesRepLen = <%=alAssociateSalesRepList.size()%>;
var ShipacctLen = <%=alShipacctList.size()%>;
var addid = '<%=addid%>';
var namesid = '<%=namesid%>';
var shipto = '<%=shipto%>';
var scrtype = '<%=strscrType%>';
var shipmode = '<%=shipMode%>';
var shipCarrier = '<%=shipCarrier%>';
var screenName = '<%=strscreenType%>';

//Setting the resulted default Ship Mode values to the javascript variables...
var fedExMode = '<%=strFedExDefltMode%>';
var upsMode = '<%=strUPSDefltMode%>';
var tntMode = '<%=strTNTDefltMode%>';
var dhlMode = '<%=strDHLDefltMode%>';
var courierExMode = '<%=strCourierDefltMode%>';
var otherMode = '<%=strOtherDefltMode%>';
var currencyFmt = '<%=strCurrencyFmt%>';
var editChrgFl = '<%=strEditChargeFl%>';
<%
hcboVal = new HashMap();
int intAlShipacctList = alShipacctList.size();
for (int i=0;i<intAlShipacctList;i++)
{
	hcboVal = (HashMap)alShipacctList.get(i);
%>
var ShipAcctArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
}
%>
<%
	hcboVal = new HashMap();
int intAlDistributor = alDistributor.size();
	for (int i=0;i<intAlDistributor;i++)
	{
		hcboVal = (HashMap)alDistributor.get(i);
%>
	var DistArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

<%	
	hcboVal = new HashMap();
//Adding dealer id for ship to type as dealer	
int intAlAccList = alAccList.size();
	for (int i=0;i<intAlAccList;i++)
	{
		hcboVal = (HashMap)alAccList.get(i);
%>
<%if(strscreenType.equals("HospitalInitiate")){%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("ID")%>";
<%}else{%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("DID")%>,<%=hcboVal.get("DEALID")%>"; 
<%}%>

<%
	}
%>

<%
	hcboVal = new HashMap();
  int intAlEmpList = alEmpList.size();
	for (int i=0;i<intAlEmpList;i++)
	{
		hcboVal = (HashMap)alEmpList.get(i);
%>
	var EmpArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("PARTYID")%>";
<%
	}
%>

<%
	hcboVal = new HashMap();
  int intRepList = alRepList.size();
	for (int i=0;i<intRepList;i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("PARTYID")%>,<%=hcboVal.get("DID")%>";
<%
	}
%>

<%
	hcboVal = new HashMap();
  int intAssociateSalesRepList = alAssociateSalesRepList.size();
	for (int i=0;i<intAssociateSalesRepList;i++)
	{
		hcboVal = (HashMap)alAssociateSalesRepList.get(i);
%>
	var AssoRepArr<%=i%> ="<%=hcboVal.get("ASSOCREPNM")%>,<%=hcboVal.get("ASSOCREPID")%>,<%=hcboVal.get("REPID")%>,<%=hcboVal.get("REPNM")%>";
<%
}
%>	
// The Following Code is Added to Form the Plant Array List values 
<%
hcboVal = new HashMap();

int intPlantList = alPlantList.size();
for (int i=0;i<intPlantList;i++)
{
	hcboVal = (HashMap)alPlantList.get(i);
%>
var PlantArr<%=i%> ="<%=hcboVal.get("PLANTID")%>,<%=hcboVal.get("PLANTNM")%>"; 

<%
}
%>	

//The Following Code is Added to Form the Dealer Array List values 
<%
hcboVal = new HashMap();
int intDealerList = alDealerList.size();
for (int i=0;i<intDealerList;i++)
{
	hcboVal = (HashMap)alDealerList.get(i);
%>
var dlrar<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>"; 
<%
}
%>	

// The Following Code is Added to Form the SHIP MODE Array List values based on
// the Corresponding SHIP CARRIER VALUES to make use in javascript.

<% hcboVal = new HashMap();
int intFEDModeList = alFEDModeList.size();
for (int i=0;i<intFEDModeList;i++){
	hcboVal = (HashMap)alFEDModeList.get(i); %>
var FedModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>

<% hcboVal = new HashMap();
int intUPSModeList = alUPSModeList.size();
for (int i=0;i<intUPSModeList;i++){
	hcboVal = (HashMap)alUPSModeList.get(i); %>
var UPSModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>

<% hcboVal = new HashMap();
int intCurrierModeList = alCurrierModeList.size();
for (int i=0;i<intCurrierModeList;i++){
	hcboVal = (HashMap)alCurrierModeList.get(i); %>
var CurrierModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% }%>

<% hcboVal = new HashMap();
int intOtherModeList = alOtherModeList.size();
for (int i=0;i<intOtherModeList;i++){
	hcboVal = (HashMap)alOtherModeList.get(i); %>
var OtherModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>

<% hcboVal = new HashMap();
int intAlTNTModeList = alTNTModeList.size();
for (int i=0;i<intAlTNTModeList;i++){
	hcboVal = (HashMap)alTNTModeList.get(i); %>
var TNTModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>

<% hcboVal = new HashMap();
int intAlDHLModeList = alDHLModeList.size();
for (int i=0;i<intAlDHLModeList;i++){
	hcboVal = (HashMap)alDHLModeList.get(i); %>
var DHLModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>

<% hcboVal = new HashMap();
int intAlShipMode = alShipMode.size();
for (int i=0;i<intAlShipMode;i++){
	hcboVal = (HashMap)alShipMode.get(i); %>
var AllModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>
</script>

</HEAD>


<input type="hidden" name="addIdReturned" />
<html:hidden property="screenType" name="frmShipDetails" value="<%=strscreenType%>"/>
<html:hidden property="shipId" name="frmShipDetails" />
<html:hidden property="hAddrId" name="frmShipDetails" />
<table border="0" width="100%"  cellspacing="0" cellpadding="0">	
		<tr class="ShadeRightTableCaption">
			<td height="25"  colspan="4"><fmtShipDtls:message key="LBL_SHIP_DTLS"/>:</td>			
		</tr>	
		<tr><td class="line" colspan="4"></td></tr>
			
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<%if(strDisplayfl.equals("Y")||strDisplayfl.equals("N")){ %>
		<tr class="Shade">
			<td colspan="4">
			<table border="0" width="100%"  cellspacing="0" cellpadding="0">
			<tr>
				<td width="12%" class="RightTableCaption" height="25" align="right">&nbsp;<fmtShipDtls:message key="LBL_TO"/> :</td>
				<td width="30%">&nbsp;<gmjsp:dropdown controlName="shipTo"  SFFormName='frmShipDetails' SFSeletedValue="shipTo"  defaultValue= "[Choose One]"	
					width="230" SFValue="alShipTo" codeId="CODEID" codeName="CODENM" onChange="javascript:fnGetAssocRepNames(this,'ship');"/>
				</td>
				<!-- Struts tag lib code modified for JBOSS migration changes -->
				<td class="RightTableCaption" align="right"><fmtShipDtls:message key="LBL_TEMP_OVER_ATTN_TO"/>:  </td>
				<td>&nbsp;<html:text property="overrideAttnTo" name="frmShipDetails" size="30" styleClass="InputArea"
						 onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>
				</td>
			</tr>
			</table>
		    </td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
		    <td  width="12%"  class="RightTableCaption" height="25" align="right"><fmtShipDtls:message key="LBL_NAMES"/>:</td>			    
		     <%if(shipto.equals("4122") && strscreenType.equals("EditShip")){ %>
					<td colspan="4">
						<table border="0" bordercolor="red" cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td >&nbsp;<gmjsp:dropdown controlName="names"  SFFormName='frmShipDetails' SFSeletedValue="names"  defaultValue= "[Choose One]" optionId="PARTYID"
								 SFValue="alShipToId" codeId="ID" codeName="NAME" onChange="javascript:fnGetAddressList(this,'')"/> </td>				
							<td width="10%">&nbsp;<input type="checkbox" name="chkNames" onclick="javascript:fnGetAllNames();" title="Load All Names"/>&nbsp;<fmtShipDtls:message key="LBL_SHOW_ALL"/></td>							
							<td id="thirdPartyField" class="RightTableCaption" HEIGHT="25" align="right" width="18%"><div id="partyAccountFiled"><fmtShipDtls:message key="LBL_PARTY_ACCT"/>:&nbsp;</div></td>
							<td id="thirdPartyValue" width="32%"><div id="partyAccount" style="text-align:justify;" > <%=strThirdParty%></div></td>
						 </tr>
						 </table>
					 </td>
				<%}else if(shipto.equals("26240419")){%>
					<td colspan="3">&nbsp;<gmjsp:dropdown controlName="names"  SFFormName='frmShipDetails' SFSeletedValue="names"  defaultValue= "[Choose One]" optionId="PARTYID"
						 SFValue="alShipToId" codeId="PLANTID" codeName="PLANTNM" onChange="javascript:fnGetAddressList(this,'')"/> 
					</td>
			  <%}else{%>
				 <td colspan="3">&nbsp;<gmjsp:dropdown controlName="names"  SFFormName='frmShipDetails' SFSeletedValue="names"  defaultValue= "[Choose One]" optionId="PARTYID"
						 SFValue="alShipToId" codeId="ID" codeName="NAME" onChange="javascript:fnGetAddressList(this,'')"/> 
						&nbsp;<input type="checkbox" name="chkNames" onclick="javascript:fnGetAllNames();" title="Load All Names"/>&nbsp;<fmtShipDtls:message key="LBL_SHOW_ALL"/>
					</td>
		      <%}%>	
		</tr>
		<tr><td  colspan="4" height="1" bgcolor="#cccccc"></td></tr>		
		<tr class="Shade">		
			<td  width="12%"  class="RightTableCaption" HEIGHT="25" align="right"><fmtShipDtls:message key="LBL_ADDRESS"/>:</td>
			<td width="61%" colspan="1" valign="middle">&nbsp;<span style="vertical-align:middle;"><select id="addressid" name="addressid" style="width:400px" onchange="fnChangeAddress(this)"></select>
				</span> 
			<%if(!strDisplayfl.equals("Y")) {
				%>
				 <img style='cursor:hand; vertical-align:middle;'  onClick="<%=strDisplayfl.equals("N")?"fnAreasetOpenAddress();":"fnOpenAddress();"%>" src='<%=strImagePath%>/Address-Book-icon.png' title="<fmtShipDtls:message key="IMG_ALT_ADDR_LOOKUP"/>" height="26" width="26">	
			<%}else{ %>	          
		          <img style='cursor:hand; vertical-align:middle;'  onClick="<%=strDisplayfl.equals("N")?"fnAreasetOpenAddress();":"fnOpenAddress();"%>" src='<%=strImagePath%>/Address-Book-icon.png' title="<fmtShipDtls:message key="IMG_ALT_ADDR_LOOKUP"/>" height="26" width="26">
		    <%}%>
		    </td>
		<%if(strDisplayfl.equals("N")) { %>
			<td  width="12%"  class="RightTableCaption" HEIGHT="25" align="right"><fmtShipDtls:message key="LBL_MODE"/>:</td>
			<td >&nbsp;<gmjsp:dropdown controlName="shipMode"  SFFormName='frmShipDetails' SFSeletedValue="shipMode"  defaultValue= "[Choose One]"	
						SFValue="alShipMode" codeId="CODEID" codeName="CODENM"/>      
		    </td>
		<%} else{%>
		<td colspan="2"></td>
		<%} %>		    		    
		</tr>
		<%if(strDisplayfl.equals("N")) { %>
			<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
			<tr class="Shade">
				<td height="1" HEIGHT="25" align="right">&nbsp;<html:checkbox property="chkAppFl"></td>
				<td colspan="3" HEIGHT="25" >&nbsp;&nbsp;<fmtShipDtls:message key="LBL_APPLY_ALL"/>&nbsp;&nbsp;</html:checkbox></td>
			</tr>
		<%} %>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<%}if(strDisplayfl.equals("Y")||strDisplayfl.equals("S")){ %>
		<tr> <td colspan =4> <table border="0" width="100%"  cellspacing="0" cellpadding="0"> <tr>
			<td width="12%" class="RightTableCaption" HEIGHT="25" align="right"><fmtShipDtls:message key="LBL_CARRIER"/>:</td>
			<td width="37%">&nbsp;<gmjsp:dropdown controlName="shipCarrier"  SFFormName='frmShipDetails' SFSeletedValue="shipCarrier"  defaultValue= "[Choose One]"	
						SFValue="alShipCarr" codeId="CODEID" codeName="CODENM" onChange="javascript:fnGetShipModes(this)"/>  
		    </td>
		    <%if(!strDisplayfl.equals("S")){ %>
		    <td  width="10%"  class="RightTableCaption" HEIGHT="25" align="right"><fmtShipDtls:message key="LBL_MODE"/>:</td>
		    <td  >&nbsp;<gmjsp:dropdown controlName="shipMode"  SFFormName='frmShipDetails' SFSeletedValue="shipMode"  defaultValue= "[Choose One]"	
						SFValue="alShipMode" codeId="CODEID" codeName="CODENM" onChange="javascript:fnCalcShipCharge()"/>      
		    </td>
		    <%}else{%>
		    <td  width="10%"  class="RightTableCaption" HEIGHT="25" align="right">&nbsp; <fmtShipDtls:message key="LBL_TRACK"/> #:</td>
			<td>&nbsp;
		           <html:text property="trackingNo"
						name="frmShipDetails" size="30" 
						onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
						onblur="changeBgColor(this,'#ffffff');"
						onkeypress="validateTrackingNum();"/>      
		    </td>
		    <%}%>
		</tr> </table> </td> </tr>
		<tr class="shade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="200">&nbsp;<fmtShipDtls:message key="LBL_SHIP_INSTR"/>:</td>
			<td colspan="3">&nbsp;<html:textarea property="shipInstruction" cols="80" name="frmShipDetails"
					styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
			</td>	
		</tr>
		
<%if(strShpChrgFl.equalsIgnoreCase("Y")){ %>
		<tr><td colspan="4"><table border="0" width="100%"  cellspacing="0" cellpadding="0"><tr>
			<td class="RightTableCaption" align="right" HEIGHT="24" width="59%">&nbsp;<fmtShipDtls:message key="LBL_SHIP_CAHRGE"/>:&nbsp;</td>
			
			<%if(strEditChargeFl.equalsIgnoreCase("Y")){ %>
			<td width="41%">
				<html:text property="shipCharge" name="frmShipDetails" styleClass="InputArea"
							 onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/></td>
			<%}else{ %>
			<td width="21%" id="lblShipCharge" class="RightText"></td>
			<td width="20%"><input type="hidden" name="shipCharge"></td>
			<%} %>
			</tr></table></td>
		</tr>
<%}else{
%>
		<input type="hidden" name="shipCharge">
<% } %>
		
		<%} if(!strDisplayfl.equals("Y")) { %>
		<tr>
				<td colspan="4" height="50" align="center">
				<fmtShipDtls:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button name="Btn_Submit" value="${varSubmit}" gmClass="button" buttonType="Load" onClick="fnGo();" />
				&nbsp;&nbsp;&nbsp;
				<fmtShipDtls:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button value="${varClose}" gmClass="button" buttonType="Load" onClick="fnClose()" />&nbsp;&nbsp;&nbsp;  
				</td>
		</tr>
			<%} %>
</table>
<%@ include file="/common/GmFooter.inc"%>
