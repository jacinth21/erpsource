<!-- \operations\GmPOReportByPart.jsp -->
 <%
/**********************************************************************************
 * File		 		: GmPOReportByPart.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ taglib prefix="fmtPOReportByPart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPOReportByPart:setLocale value="<%=strLocale%>"/>
<fmtPOReportByPart:setBundle basename="properties.labels.operations.GmPOReportByPart"/>

<%
try {
	response.setCharacterEncoding("UTF-8");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strGridXmlData = GmCommonClass.parseNull((String)request.getAttribute("GRIDXMLDATA"));
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strhMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strhOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	String strApplDateFmt = strGCompDateFmt;
	
	String strShade = "";

	String stroperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strPOId = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");
	String strPartNums = (String)request.getAttribute("hPartNums")==null?"":(String)request.getAttribute("hPartNums");

	Date dtFrom=null;
	Date dtTo=null;
	dtFrom  = GmCommonClass.getStringToDate(strFromDt, strApplDateFmt);
	dtTo  = GmCommonClass.getStringToDate(strToDt, strApplDateFmt);
	
	int intSize = 0;
	
	String strNextId = "";
	int intCount = 0;
	String strLine = "";
	boolean blFlag = false;
	String strPartNumId = "";
	String strLog = "";
	String strWORev = "";
	String strImg = "";
	String strVendorCurrency = "";

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<%-- <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css"> --%>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=stroperationsJsPath%>/GmPOReportByPart.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>

<!-- <style type="text/css" media="all">
      table{
	margin: 0 0 0 0;
	width: 895px;
	border: 1px solid  #676767;
} 
</style> -->
<script>

var objGridData;
objGridData ='<%=strGridXmlData%>';
var gridObj;
function fnOnPageLoad() {

	if(document.frmVendor.Txt_PartNum != undefined && document.frmVendor.Txt_PartNum != null){
	   document.frmVendor.Txt_PartNum.focus();
	}
	
	// to set auto height
    if($(document).height() !=undefined || $(document).height() !=null){
	        gridHeight = $(document).height() - 200;
	    }
	
	if (objGridData.value != '') {
		gridObj = initGridData('PoPartRpt', objGridData);
	}
}
function initGridData(divRef, gridData) {
    mygrid = setDefalutGridProperties(divRef);	
	//mygrid.setNumberFormat("$0,000.00",4,".",",");
	mygrid.enableMultiline(false);	
	mygrid.enableDistributedParsing(true);
	mygrid = initializeDefaultGrid(mygrid,gridData);
	mygrid.enableAutoHeight(true, gridHeight, true);
//	mygrid.enableAutoHeight(true);
	mygrid.enableHeaderMenu();
	mygrid.attachHeader('#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter');
	mygrid.enableTooltips("false,false,false,false,false,false,false,false,false,false,false,false,false,false");
	mygrid.enableBlockSelection(true);
    return mygrid;
}

//To download excel;
function fnExcel() { 
	mygrid.toExcel('/phpapp/excel/generate.php');
}
</script> 
</HEAD>

<BODY leftmargin="15" topmargin="10" onload="fnOnPageLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPOReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hDHRId" value="">
<input type="hidden" name="hPOId" value="">
<input type="hidden" name="hVenId" value="">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">

	<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td class="Line" width="1" rowspan="5"></td>
			<td height="25" colspan="2"  class="RightDashBoardHeader"><fmtPOReportByPart:message key="LBL_PO_REPORT_BY_PART_NUMBER"/></td>
			<td class="Line" width="1" rowspan="5"></td>
		</tr>
		<tr>
			<td class="RightText" colspan="2" HEIGHT="30">
				&nbsp;<fmtPOReportByPart:message key="LBL_PART_NUMBER"/>:&nbsp;<input type="text" size="50" value="<%=strPartNums%>" name="Txt_PartNum" class="InputArea" style="text-transform: uppercase;" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
				<span class="RightTextBlue"><fmtPOReportByPart:message key="LBL_ENTER_PART_SEPERATED_BY_COMMAS"/></span>
			</td>
		</tr>
		<tr>
			 <!-- <td width="16">&nbsp;</td> -->
			<td class="RightText" HEIGHT="24" ><fmtPOReportByPart:message key="LBL_FROM_DATE"/>:&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(dtFrom==null)?null:new java.sql.Date(dtFrom.getTime())%>"  
				gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
				 onBlur="changeBgColor(this,'#ffffff');"/>
			
			&nbsp;&nbsp;<fmtPOReportByPart:message key="LBL_TO_DATE"/>:
			<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(dtTo==null)?null:new java.sql.Date(dtTo.getTime())%>"  
				gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
				 onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
				&nbsp;&nbsp; 
			
			<fmtPOReportByPart:message key="LBL_GO" var="varGo"/>
			<gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" onClick="fnGo();" tabindex="25" buttonType="Load" />
			</td>
		</tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr> 

<%if(!strGridXmlData.equals("")) { %> 
		<tr>				
				<td >
					<div id="PoPartRpt" style="height:690px; width:100%"></div>
				</td>			
		</tr>

     	<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
     		<tr>
			<td colspan="2" align="center">
			    <div class='exportlinks'><fmtPOReportByPart:message key="LBL_EXPORT_OPTS"/> : <img src='img/ico_file_excel.png' onclick="fnExcel();" />&nbsp;<a href="#" onclick="fnExcel();"><fmtPOReportByPart:message key="LBL_EXCEL"/></a></div>
			</td>
		</tr>
		<% }else{ %>	
					<tr><td colspan="2" align="center" class="RightTextRed"><fmtPOReportByPart:message key="LBL_NO_POS_HAVE_BEEN_PLACED_FOR_THIS_PART_NUMBER"/> !</td></tr>
<%}%>	 
		 <tr><td colspan="3" class="Line" height="1"></td></tr> 
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
