
<%
	/***************************************************************************************************************************************
	 * File		 		: GmFieldSalesWarehouseRpt.jsp
	 * Desc		 		: This screen is used for Loading the Field sales warehouse details.
	 * Version	 		: 1.0
	 * author			: 
	 ****************************************************************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtFSWarehouse" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- \operations\purchasing\operations\GmAccountNetRptByLot.jsp-->  
<fmtFSWarehouse:setLocale value="<%=strLocale%>"/>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtFSWarehouse:setBundle basename="properties.labels.operations.GmFieldSalesWarehouseRpt"/>
<bean:define id="gridData" name="frmfieldSalesWarehouse" property="gridData" type="java.lang.String"></bean:define>
<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");

	String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("FIELD_SALES_NET_QUANTITY"));
	String strApplDateFmt = strGCompDateFmt;
	String strCurDate = GmCommonClass.parseNull((String) session.getAttribute("strSessTodaysDate"));
%>
<HTML>
<HEAD>
<TITLE>GlobusOne Enterprise Portal: Field Sales Net Qty Reports</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
 <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">   
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmFieldSalesWarehouseRpt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>


<script>
var objGridData = '';
objGridData = '<%=gridData%>';
var date_format = '<%=strApplDateFmt%>';
</script>
</HEAD>
<!-- Custom tag lib code modified for JBOSS migration changes -->
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeypress="fnEnter();">
	<html:form action="/gmFieldSalesWarehouseRpt.do">
		<html:hidden property="strOpt"/>
		<html:hidden property="warehouseId" value=""/>
		<html:hidden property="locationId" value="" />
		<html:hidden property="partNumber" value="" />
		<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="7" height="25" class="RightDashBoardHeader">
				<logic:equal name="frmfieldSalesWarehouse" property="locationType" value="4000338"><fmtFSWarehouse:message key="TD_FS_NET_QTY_HEADER"/></logic:equal>
				<logic:equal name="frmfieldSalesWarehouse" property="locationType" value="26230710"><fmtFSWarehouse:message key="TD_ACCT_NET_QTY_BY_LOT_HEADER"/></logic:equal>
				
					</td>
				<fmtFSWarehouse:message key="IMG_ALT_HELP" var="varHelp"/>
				<td height="25" class="RightDashBoardHeader"><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>
			<tr height="25">
			<fmtFSWarehouse:message key="LBL_LOCATION_TYPE" var="varLocType"/> 
				<td class="RightTableCaption" colspan="2" align="right" width="15%">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varLocType}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="locationType" SFFormName="frmfieldSalesWarehouse" SFSeletedValue="locationType" 
				SFValue="alLocationType" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="2" onChange="javascript:fnLoadNames(this);"/>  
			</td>
			<fmtFSWarehouse:message key="LBL_NAME" var="varName"/>
			<td class="RightTableCaption" colspan="2" align="right">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varName}:" td="false" />
			</td>
			<td colspan="2" id="fieldSales">&nbsp;<gmjsp:dropdown controlName="fieldSales" SFFormName="frmfieldSalesWarehouse" SFSeletedValue="fieldSales" 
				SFValue="alFieldSales" codeId = "ID" codeName = "NAME" defaultValue= "[Choose One]" tabIndex="2"/>  
			</td>
			<td id="accounts">&nbsp;<gmjsp:dropdown controlName="account" SFFormName="frmfieldSalesWarehouse" SFSeletedValue="account" 
				SFValue="alAccounts" codeId = "ID" codeName = "NAME" defaultValue= "[Choose One]" tabIndex="2"/>  
			</td>
			</tr>
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>
			<tr height="30" class="Shade">
				<fmtFSWarehouse:message key="LBL_PART" var="varPart"/> 
				<td class="RightTableCaption" colspan="2" align="right">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varPart}:" td="false" />
				</td>
				<td colspan="2">&nbsp;<html:text property="pNum" size="40"
									onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
									onblur="changeBgColor(this,'#ffffff');"
									tabindex="3"/></td>
			<fmtFSWarehouse:message key="LBL_QTY" var="varQty"/> 
			<td class="RightTableCaption" colspan="2" align="right">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="${varQty}:" td="false" />
				</td>
				<td colspan="2">
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="50%">&nbsp;<gmjsp:dropdown controlName="fsQtyVal" SFFormName="frmfieldSalesWarehouse" SFSeletedValue="fsQtyVal" SFValue="alQtySearchList" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]"  />
                        	<html:text property="fsQuantity" name="frmfieldSalesWarehouse" size="5" tabindex="21" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
                        	<fmtFSWarehouse:message key="BTN_LOAD" var="varLoad"/>
                        	<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="Button" onClick="fnRptLoad();" tabindex="5" buttonType="Load" />
                        	</td>
                        </tr>
                      </table>
                </td>
			</tr>
				<%
					if (gridData.indexOf("cell") != -1) {
				%>
				
			<tr>
				<td colspan="8">
					<div id="acctivityRpt" style="height: 320px; width: 1050px"></div>
					<div id="pagingArea" style=" width:800px"></div>
					</td>
					
			</tr>
				
				<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>
			<tr height="25">
			<td colspan="8" align="center">
			    <div class='exportlinks'><fmtFSWarehouse:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"><fmtFSWarehouse:message key="DIV_EXCEL"/> 
				</a></div>
			</td>
		</tr>
				<%
					}else{
				%>
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>	
			<tr>
			<td colspan="8" align="center" class="RegularText"><fmtFSWarehouse:message key="TD_NO_DATA_FOUND"/></td>
			</tr>
			<%} %>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
