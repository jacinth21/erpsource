 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@page import="com.globus.common.beans.GmCommonBean"%>
<%
/**********************************************************************************
 * File		 		: GmLoanerReests.jsp
 * Desc		 		: This screen is used for the displaying loaner request report
 * Version	 		: 1.1
 * author			: Xun
 * 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtLoanerRequest" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmLoanerRequests.jsp -->
<fmtLoanerRequest:setLocale value="<%=strLocale%>"/>
<fmtLoanerRequest:setBundle basename="properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest"/>
<%
	
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strSessApplDateFmt= strGCompDateFmt;//GmCommonClass.parseNull((String)request.getSession().getAttribute("strSessApplDateFmt"));
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");	
	String strDistId = GmCommonClass.parseNull((String)request.getAttribute("hDistId"));
	String strSetName = GmCommonClass.parseNull((String)request.getAttribute("hSetName"));
	String strDtTyp = GmCommonClass.parseNull((String)request.getAttribute("hDtType"));
	String strFromDt = GmCommonClass.parseNull((String)request.getAttribute("hFromDt"));
	String strToDt = GmCommonClass.parseNull((String)request.getAttribute("hToDt"));
	String strStatus = GmCommonClass.parseNull((String)request.getAttribute("status"));
	String strInHouseType =  GmCommonClass.parseNull((String)request.getAttribute("hInHouseType"));
	String strEventId = GmCommonClass.parseNull((String)request.getAttribute("hEventId"));
	String strEmpId = GmCommonClass.parseNull((String)request.getAttribute("hEmpId"));
	String strAccessFl = GmCommonClass.parseNull((String)request.getAttribute("AccessFl"));
	String gridData = GmCommonClass.parseNull((String)request.getAttribute("REPORT"));
	String strInputParam = GmCommonClass.parseNull((String)request.getAttribute("hInputParam"));
	String strHInputParam = GmCommonClass.parseNull((String)request.getAttribute("hHInputParam"));
	String strStatusParam = GmCommonClass.parseNull((String)request.getAttribute("htStatusParam"));
	String strSelectedStatus = GmCommonClass.parseNull((String)request.getAttribute("hSelectedStatus"));
	String strHSelectedStatus = GmCommonClass.parseNull((String)request.getAttribute("hhselectedstatus"));
	
	ArrayList alDistributorList = (ArrayList)request.getAttribute("DISTLIST");
	ArrayList alSetList = (ArrayList)request.getAttribute("SETLIST");
	ArrayList alStatus = (ArrayList)request.getAttribute("STATUSLIST");
	ArrayList alEmpList = (ArrayList)request.getAttribute("EMPLIST");
	ArrayList alEventName = (ArrayList)request.getAttribute("EVENTNAME");
	
	ArrayList alRequest = (ArrayList)request.getAttribute("LOANERREQUESTS");
	//log.debug("alSetList is " +  alSetList);
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest", strSessCompanyLocale);
	String strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REPORT_LOANER_REQUESTS"));
	
	if(strInHouseType.equals("InhouseLoanerRequests")){
		strHeader =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INHOUSE_LOANER_REPORT"));
	}
	
	int rowsize = alRequest.size();
 
	log.debug("Loaner Request called "+rowsize);
	HashMap hmMapStatus = new HashMap();
	hmMapStatus.put("ID","");
	hmMapStatus.put("PID","CODENMALT");
	hmMapStatus.put("NM","CODENM");
%>
<% 
	boolean showGrid=true;	
	if(gridData!=null && gridData.indexOf("cell")==-1){
	   showGrid = false;			
	}
%>
<HTML>
<TITLE> Globus Medical: Open Loaner Requests</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="javascript" src="<%=strOperationsJsPath%>/GmLoanerRequests.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
var strdtType  = "<%=strDtTyp%>";
var strInHouseType = "<%=strInHouseType%>";
var gridObjData='<%=gridData%>';
var mygrid ='';
var format = '<%=strSessApplDateFmt%>';
	
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnOnLoad();">
<FORM name="frmLoaner" method="POST" action="<%=strServletPath%>/GmLoanerPartRepServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOpt" value="<%=strhAction%>">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hPrdReqId" value="">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hStatus" value="<%=strStatus%>">
<input type="hidden" name="hInHouseType" value="<%=strInHouseType %>">
<input type="hidden" name="hCancelType" value="VDLON"/>
<input type="hidden" name="hAccessFl" value="<%=strAccessFl%>"/>
<input type="hidden" name="hinputparam" value="<%=strInputParam%>"/>
<input type="hidden" name="hstatusparam" value="<%=strStatusParam%>"/>
<input type="hidden" name="hselectedstatus" value="<%=strSelectedStatus%>"/>
<input type="hidden" name="hHinputparam" value="<%=strHInputParam%>"/>
<input type="hidden" name="hhselectedstatus" value="<%=strHSelectedStatus%>"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><%=strHeader%> </td>
			<td  class="RightDashBoardHeader" align="right" colspan="2"></td>
		    <td align="right" class=RightDashBoardHeader>
		    <fmtLoanerRequest:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("LOANER_REQUESTS_REPORT")%>');" />
			</td> 
		</tr>
		<tr><td class="Line" height="1" colspan="5"></td></tr>	
		<% if(strInHouseType.equals("InhouseLoanerRequests")){ %>
		<tr class="evenshade">
			<td height="25" class="RightTableCaption" align="Right" width=15%><fmtLoanerRequest:message key="LBL_EMPLOYEENAME"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_empId"  seletedValue="<%=strEmpId%>"  defaultValue= "[Choose One]"	
				value="<%=alEmpList%>" codeId="ID" codeName="NAME" />
			</td>
			<td height="25" class="RightTableCaption" align="Right" width=14%><fmtLoanerRequest:message key="LBL_EVENTNAME"/>:&nbsp;</td>
			<td><gmjsp:dropdown controlName="Cbo_eventId"  seletedValue="<%=strEventId%>"  defaultValue= "[Choose One]"	
				value="<%=alEventName%>" codeId="CID" codeName="ENAME" />
			</td>
		</tr>
		<tr class="evenshade">
			<td height="25" class="RightTableCaption" align="Right"><fmtLoanerRequest:message key="LBL_SETNAME"/>:</td>
			<td colspan="4">&nbsp;<input type="text" name ="txt_SetName" Value="<%=strSetName%>" TabIndex="2" 
				class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />					 		
			</td>
		</tr>
		<%}else{ %>
		<tr class="evenshade">
			<td height="25" class="RightTableCaption" align="right">&nbsp;<fmtLoanerRequest:message key="LBL_DISTRIBUTORNAME"/>:</td>
			<td>&nbsp;<gmjsp:autolist controlName="Cbo_DistId"  seletedValue="<%=strDistId%>" 	
					tabIndex="1" width="250"  value="<%=alDistributorList%>"   defaultValue= "[Choose One]"  />&nbsp;&nbsp;&nbsp;&nbsp;
					 			
			</td>
			<td height="25" class="RightTableCaption" align="right"><fmtLoanerRequest:message key="LBL_SETNAME"/>:</td>
			<td colspan="2">&nbsp;<input type="text" name ="txt_SetName" Value="<%=strSetName%>" TabIndex="2" 
				class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />					 		
			</td>
		</tr>
		<%} %>
		<tr><td class="Line" height="1" colspan="5"></td></tr>
		<tr class="oddShade">
			<td HEIGHT="30" class="RightTableCaption" align="right">&nbsp;<fmtLoanerRequest:message key="LBL_DATERANGE"/>:</td>
			<td class="RightTableCaption">&nbsp;<fmtLoanerRequest:message key="LBL_TYPE"/>:&nbsp;<select name="dtType">
									 <option value='0' >[Choose One]									 
								<% if(!strInHouseType.equals("InhouseLoanerRequests")){ %>
									 <option value='SG'><fmtLoanerRequest:message key="LBL_SURGERYDATE"/>
								<%} %>
									 <option value='PS'><fmtLoanerRequest:message key="LBL_PLANNEDSHIPDATE"/>
									 <option value='RD'><fmtLoanerRequest:message key="LBL_REQUESTEDDATE"/>
								<% if(strInHouseType.equals("InhouseLoanerRequests")){ %>
									 <option value='ESD'><fmtLoanerRequest:message key="LBL_EVENTSTARTDATE"/>
									 <option value='EED'><fmtLoanerRequest:message key="LBL_EVENTENDDATE"/>
								<%} %>
									 </select>&nbsp;
					<BR><BR>&nbsp;<fmtLoanerRequest:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar textControlName="fromDt" SFFormName='frmLoaner' 
			 	textValue="<%=(strFromDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFromDt,strSessApplDateFmt).getTime())))%>"  
			 	onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
			&nbsp;<fmtLoanerRequest:message key="LBL_TO"/>:&nbsp;<gmjsp:calendar textControlName="toDt" SFFormName='frmLoaner'  
				textValue="<%=(strToDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDt,strSessApplDateFmt).getTime())))%>"  
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
			</td>
			<td class="RightTableCaption" align="right">&nbsp;<fmtLoanerRequest:message key="LBL_STATUS"/>:&nbsp;</td>
			<td height="60">&nbsp;&nbsp;<%=GmCommonControls.getChkBoxGroup("",alStatus,"Status",hmMapStatus)%></td> 
			<fmtLoanerRequest:message key="BTN_LOAD" var="varLoad"/>   
			<td width="150"><gmjsp:button name="Btn_Go" value="${varLoad}" style="width: 6em; height:2.5em" onClick="return fnReload();" tabindex="3" gmClass="button" buttonType="Load" /></td>
		</tr>
		<%// if (strhAction.equals("Reload")) { %>
		<tr><td class="Line" height="1" colspan="6"></td></tr>		
		<tr>
		<%if(showGrid){%>
			<td colspan="5">
					 <div  id="dataGridDiv" style="height:550px ; "></div>
			</td>
				   <tr><td  colspan="4"><div class='exportlinks'><fmtLoanerRequest:message key="LBL_EXPORTOPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
	               onclick="fnDownloadXLS();"> <fmtLoanerRequest:message key="LBL_EXCEL"/> </a></div></td> </tr>
		<%}else{ %>
			<td class="RightText" colspan="5">
			  <fmtLoanerRequest:message key="LBL_NOTHING_TO_FOUND_TO_DISPLAY"/>
			</td>	
			<%}%>
       </tr>


		 <% if (rowsize >0) { %> 
                   <tr height = "35">
                   
                    	<td colspan="5" class="RightTableCaption" align="center"><fmtLoanerRequest:message key="LBL_CHOOSEACTION"/>:&nbsp;<select name="Cbo_Action" class="RightText" 
						tabindex="5" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
							 
							<option value="RVOD"><fmtLoanerRequest:message key="LBL_VOIDREQUEST"/></option>
							<option value="EDIT"><fmtLoanerRequest:message key="LBL_EDITREQUEST"/></option>
 
						</select>&nbsp;&nbsp;
						<fmtLoanerRequest:message key="BTN_SUBMIT" var="varSubmit"/> 
						<gmjsp:button name="Btn_Submit" value="${varSubmit}" style="width:6em; height:2.5em;" gmClass="button" onClick="return fnSubmit();" buttonType="Save" /> 
		                </td>
                    </tr>
                    <% } %>
                    
              <% //} %>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc"%>	
</BODY>

</HTML>
