<!-- \operations\GmSetBuildSetup.jsp -->

 <%
/**********************************************************************************
 * File		 		: GmSetBuildSetup.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtSetBuildSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSetBuildSetup:setLocale value="<%=strLocale%>"/>
<fmtSetBuildSetup:setBundle basename="properties.labels.operations.GmSetBuildSetup"/>


<%
try {
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strMode = (String)request.getAttribute("hMode") == null?"":(String)request.getAttribute("hMode");
	String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	
	String strWikiTitle = GmCommonClass.getWikiTitle("SET_BUILD_INITITATE");
	
	log.debug(" strOpt is " + strOpt + " strAction is " + strhAction);
	
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strProjId =	"";
	String strSetId = "";
	String strSetNm = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";
	String strConsignId = "";
	String strSetName= "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strFlag = "";
	String strCheckedOpt = "";
	String strVerFlag = "";
	String strLastUpdNm = "";
	String strLastUpdDt = "";
	String strQtyInBulk = "";
	String strCritFl = "";
	String strCritQty = "";
	String strCritType = "";

	ArrayList alSetList = new ArrayList();
	ArrayList alSetLoad = new ArrayList();
	ArrayList alMissParts = new ArrayList();
	ArrayList alSimilarSets = new ArrayList();

	HashMap hmConsignDetails = new HashMap();

	int intMissPartsSize = 0;
	int intSimSize = 0;

	if (hmReturn != null)
	{
		alSetList = (ArrayList)hmReturn.get("SETLIST");
		if (strhAction.equals("LoadSet")|| strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash"))
		{
			alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
			alMissParts = (ArrayList)hmReturn.get("MISSINGPARTS");
			alSimilarSets = (ArrayList)hmReturn.get("SIMILARSETS");
			if (alMissParts != null)
			{
				intMissPartsSize = alMissParts.size();
			}

			if (alSimilarSets != null)
			{
				intSimSize = alSimilarSets.size();
			}
			
			strSetId = (String)request.getAttribute("hSetId") == null?"":(String)request.getAttribute("hSetId");
			if (strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash"))
			{
				hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
				strConsignId = GmCommonClass.parseNull((String)hmConsignDetails.get("CID"));
				strSetName= GmCommonClass.parseNull((String)hmConsignDetails.get("SNAME"));
				strSetId = GmCommonClass.parseNull((String)hmConsignDetails.get("SETID"));
				strAccName = GmCommonClass.parseNull((String)hmConsignDetails.get("ANAME"));
				strUserName = GmCommonClass.parseNull((String)hmConsignDetails.get("UNAME"));
				strIniDate = GmCommonClass.parseNull((String)hmConsignDetails.get("CDATE"));
				strDesc = GmCommonClass.parseNull((String)hmConsignDetails.get("COMMENTS"));
				strLastUpdNm = GmCommonClass.parseNull((String)hmConsignDetails.get("LUNAME"));
				strLastUpdDt = GmCommonClass.parseNull((String)hmConsignDetails.get("LUDATE"));
				strFlag = GmCommonClass.parseNull((String)hmConsignDetails.get("SFL"));
				strChecked = strFlag.equals("2")?"checked":"";
				strVerFlag = GmCommonClass.parseNull((String)hmConsignDetails.get("VFL"));
				if (strVerFlag.equals("1"))
				{
					strChecked = "checked";
				}
				else
				{
					strChecked = "";
				}
				log.debug(" setName " + strSetName);
			}
		}
	}

	int intSize = 0;
	HashMap hcboVal = null;
	
	String strAccessId = GmCommonClass.parseNull((String)session.getAttribute("strSessAccLvl"));
	String strDeptId = 	GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	int intAccId = Integer.parseInt(strAccessId);
	boolean editFl = false;
	if (strDeptId.equals("Z") || (strDeptId.equals("L") && intAccId > 2 ))
	{
		editFl = true;
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Build </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var cnt = 0;
function fnSubmit()
{
	Error_Clear();
	
	var consID = document.frmVendor.hConsignId.value;
	var consignId = document.frmVendor.consignID.value;
	 
	if(consID != consignId)
	{	 
		alert("Please load first if you change consignment ID before Submitting!");
		return false;
	}
	hcnt = parseInt(document.frmVendor.hCnt.value);
	hcnt = hcnt+cnt;
	document.frmVendor.hAction.value = 'Save';
	document.frmVendor.hCnt.value = hcnt;
  	val = document.frmVendor.hEditFl.value;
	val1 = document.frmVendor.hStatusFl.value;
	var strErrorMore = '';
	var theinputs = document.getElementsByTagName("input");
    var arr = document.frmVendor.pnums.value.split(',');
    var blk = '';
    var cnum = '';
    var qtycnt = 0;
    hcnt++;
    for(var i=0;i<arr.length;i++)
    {
       qid = arr[i];
       //pnum1 = qid.replace('.','');
       pnum1 = qid.replace(/\./g,"");
       blk = eval("document.frmVendor.Qty_Blk"+pnum1);
              
		    for(var j=0;j<hcnt;j++)
		    {
				pnumobj = eval("document.frmVendor.hPartNum"+j);
				if (pnumobj)
				{
					if (pnumobj.value == qid)
					{
						qtyobj = eval("document.frmVendor.Txt_Qty"+j);
						cnumobj = eval("document.frmVendor.Txt_CNum"+j);
						if(!isNaN(qtyobj.value))
						{
							qty = parseInt(qtyobj.value);
							cnum = cnumobj.value;
							if (qty >= 0 && cnum != '')
							{
								qtycnt = qtycnt + qty;
							}
						}
						else
						{
							alert("Non Numeric");
						}
					}
				}
			}

		if (qtycnt > parseInt(blk.value) )
		{
			//alert("More than bulk:"+qid+":"+qtycnt);
			strErrorMore  = strErrorMore + ","+ qid;
		}
		qtycnt = 0;
	}
	
	if (strErrorMore != '')
	{
		Error_Details("Qty entered cannot be more than Qty Available for the following Parts: "+strErrorMore);
	}

	if (val1 == '2')
	{
		if (val == 'false')
		{
			alert("You do not have sufficient permissions to verify Sets.");
			return false;
		}
		else
		{
			if (ErrorCount > 0)
			{
				Error_Show();
				return false;
			}else{		
			document.frmVendor.submit();
			}
		}
	}else{
		if (ErrorCount > 0)
		{
			Error_Show();
			return false;
		}else{	
			fnStartProgress('Y');
			document.frmVendor.submit();
		}
	}
}

function fnSetCheck(val)
{
	if (val.checked == true)
	{
		document.frmVendor.hStatusFl.value = "2";
	}
}

function fnSetCheckVer(val)
{
	if (val.checked == false)
	{
		document.frmVendor.hVerifyFl.value = "0";
	}
	else
	{
		document.frmVendor.hVerifyFl.value = "1";
	}
}


function fnLoadSet(obj)
{
	document.frmVendor.hAction.value = 'LoadSet';
	document.frmVendor.hSetId.value = obj.value;
  	document.frmVendor.submit();
}


function fnSplit(val,pnum)
{
	cnt++;
	var hcnt = parseInt(document.frmVendor.hCnt.value);
	hcnt = hcnt+cnt;

	var NewQtyHtml = "<span id='Sp"+cnt+"'><BR>&nbsp;&nbsp;&nbsp;<input type='hidden' name='hPartNum"+hcnt+"' value='"+pnum+"'><input type='text' size='3' value='' name='Txt_Qty"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' size='9' value='' name='Txt_CNum"+hcnt+"' class='InputArea' onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); tabindex=1>&nbsp;<a href=javascript:fnDelete("+cnt+");><img src=<%=strImagePath%>/btn_remove.gif border=0></a></span>";
	val = val + 1;
	var QtyObj = eval("document.all.Cell"+val);
	QtyObj.insertAdjacentHTML("beforeEnd",NewQtyHtml);
}

function fnDelete(val)
{
	obj = eval("document.all.Sp"+val);
	obj.innerHTML = "";
}

function fnInitiate()
{
	document.frmVendor.hAction.value = 'Initiate';
	fnStartProgress('Y');
  	document.frmVendor.submit();
}

function fnPicSlip()
{
	val = document.frmVendor.hConsignId.value;	windowOpener("<%=strServletPath%>/GmSetBuildServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=670,height=500");
}

function fnCallOptional()
{
	document.frmVendor.hAction.value = 'EditLoad';

	if (document.frmVendor.Chk_Opt.checked == false)
	{
		document.frmVendor.hMode.value = 'SET';
	}
	else
	{
		document.frmVendor.hMode.value = 'OPT';
	}
  	document.frmVendor.submit();
}

function fnReconfigure()
{
	document.frmVendor.hAction.value = 'Reconfigure';
	fnStartProgress('Y');
  	document.frmVendor.submit();
}

function fnReload()
{
	document.frmVendor.hAction.value = 'Reload';
	fnStartProgress('Y');
  	document.frmVendor.submit();
}


function fnVoidSetCsg()
{
		document.frmVendor.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmVendor.hAction.value = "Load";
		document.frmVendor.submit();
}

function fnCallEdit()
{
	var consignId = document.frmVendor.consignID.value;
	document.frmVendor.hAction.value = 'EditLoadDash';
	document.frmVendor.hId.value = consignId;
	//alert("congid is  " + consignId);
	fnStartProgress('Y');
	document.frmVendor.submit();
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmSetBuildServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hSetId" value="<%=strSetId%>">
<input type="hidden" name="hMode" value="SET">
<input type="hidden" name="hStatusFl" value="<%=strFlag%>">
<input type="hidden" name="hVerifyFl" value="">
<input type="hidden" name="hEditFl" value="<%=editFl%>">
<input type="hidden" name="hTxnId" value="<%=strConsignId%>">
<input type="hidden" name="hCancelType" value="VDSCN">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hOpt" value="<%=strOpt%>">


	<table border="0" width="750" class="border" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtSetBuildSetup:message key="LBL_SET_BUILD"/></td>
			<td height="25" class="RightDashBoardHeader">
			<fmtSetBuildSetup:message key="IMG_ALT_HELP" var="varHelp"/>
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
		</tr>
		<tr>
			<td width="748" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
					if (!strhAction.equals("EditLoad") && !strhAction.equals("EditLoadDash") && !strOpt.equals("BuildSet"))
					{
%>
					<tr>
						<td colspan="2" class="RightTextBlue"><fmtSetBuildSetup:message key="LBL_PLEASE_CHOOSE_A_SET_THAT_YOU_WANT_TO_BUILD"/>
						</td>
					</tr>
					<tr>
						<td class="RightText" HEIGHT="30" align="right">&nbsp;<fmtSetBuildSetup:message key="LBL_SET_NAME"/>:</td>
						<td>&nbsp;<select name="Cbo_Proj" id="Region" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnLoadSet(this);" tabindex=1><option value="0" >[Choose One]
<%
						intSize = alSetList.size();
						hcboVal = new HashMap();
						for (int i=0;i<intSize;i++)
						{
							hcboVal = (HashMap)alSetList.get(i);
							strCodeID = (String)hcboVal.get("ID");
							strTemp = (String)hcboVal.get("NAME");
							strSelected = strSetId.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeID%>::<%=GmCommonClass.getStringWithTM(strTemp)%></option>
<%
			  			}
%>
						</select>
						</td>
					</tr>
<%
					}
						if(strOpt.equals("BuildSet")){
%>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetBuildSetup:message key="LBL_CONSIGNMENT_ID"/>:</td>
						<td>&nbsp;<input type="text" size="15" value="<%=strConsignId%>" name="consignID" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2>&nbsp; 					
							  &nbsp;&nbsp;
							  <fmtSetBuildSetup:message key="LBL_LOAD" var="varLoad"/>
							  <gmjsp:button value="&nbsp;${varLoad}&nbsp;" onClick="javascript:fnCallEdit();"  gmClass="button" buttonType="Load" /></td>	
					</tr>
					
<%}
						if (strhAction.equals("EditLoad") ||strhAction.equals("EditLoadDash") || strOpt.equals("BuildSet"))
						{
						%>
					<tr class="shade">
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetBuildSetup:message key="LBL_SET_NAME"/>:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
					</tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetBuildSetup:message key="LBL_INITIATED_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><fmtSetBuildSetup:message key="LBL_INITIATED_DATE"/>Initiated Date:</B>&nbsp;<%=strIniDate%></td>
					</tr>
					<tr class="shade">
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtSetBuildSetup:message key="LBL_LAST_UPDATED_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strLastUpdNm%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<B><fmtSetBuildSetup:message key="LBL_LAST_UPDATED_DATE"/>:</B>&nbsp;<%=strLastUpdDt%></td>
					</tr>
					<tr>
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="24" colspan="2"><BR>
							<iframe src="/GmCommonLogServlet?hAction=Load&hID=<%=strConsignId%>&hType=1220&hMode=INC" scrolling="yes" align="left" marginheight="2" width="100%" height="200"></iframe><BR>
						</TD>
					</tr>
					<!--				
					<tr>
						<td class="RightTableCaption" valign="top" align="right" HEIGHT="24">Comments:</td>
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5 cols=40 value="<%=strDesc%>"><%=strDesc%></textarea></td>
					</tr>
					-->
<%
					if (strFlag.equals("2"))
					{
%>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtSetBuildSetup:message key="LBL_VERIFIED"/> ?:</td>
						<td><input type="checkbox" name="Chk_VerFlag" onFocus="changeBgColor(this,'#AACCE8');" <%=strChecked%> onBlur="changeBgColor(this,'#ffffff');" tabindex=2 onSelect="javascript:fnSetCheckVer(this)"></td>
					</tr>
<%
					}else{
%>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtSetBuildSetup:message key="LBL_COMPLETE"/>?:</td>
						<td><input type="checkbox" name="Chk_Flag" onFocus="changeBgColor(this,'#AACCE8');" <%=strChecked%> onBlur="changeBgColor(this,'#ffffff');" tabindex=2 onSelect="javascript:fnSetCheck(this)"></td>
					</tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtSetBuildSetup:message key="LBL_RELOAD_CONTROL_#'S_FROM"/>:</td>
						<td>&nbsp;<select name="Cbo_SimSet" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1><option value="0" ><fmtSetBuildSetup:message key="" var="var"/>[Choose One]
<%
						hcboVal = new HashMap();
						for (int i=0;i<intSimSize;i++)
						{
							hcboVal = (HashMap)alSimilarSets.get(i);
							strCodeID = (String)hcboVal.get("CONID");
%>
							<option value="<%=strCodeID%>"><%=strCodeID%></option>
<%
			  			}
%>
						</select>&nbsp;&nbsp;
						<fmtSetBuildSetup:message key="LBL_RELOAD" var="varReload"/>
						<gmjsp:button value="${varReload}" gmClass="button"  onClick="fnReload();" tabindex="15" buttonType="Save" />
						</td>
					</tr>
<%
					}
%>
<%
						if (intMissPartsSize > 0)
						{
%>
					
					<tr>
						<td colspan="2" class="RightText" HEIGHT="20" align="center">
							<span class="RightTextBlue">&nbsp;<fmtSetBuildSetup:message key="LBL_SOME_PARTS_ADDED/REMOVED_FROM_THIS_SET_LIST_SINCE_IT_WAS_INITIATED"/>. <BR><fmtSetBuildSetup:message key="LBL_CLICK_TO_RECONFIGURE_THIS_SET_WTIH_LATEST_CONFIGURATION"/>.</span><BR>
							<fmtSetBuildSetup:message key="LBL_RECONFIGURE_SET" var="varReconfigureSet"/>
							<gmjsp:button value="${varReconfigureSet}" gmClass="button" onClick="fnReconfigure();" tabindex="26" buttonType="Save" />&nbsp;<BR><BR>						
						</td>
					</tr>
<%
						}
					}
					if (strhAction.equals("LoadSet")|| strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash"))
					{
%>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="7" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td width="70" height="25"><fmtSetBuildSetup:message key="LBL_PART_NUMBER"/></td>
									<td width="350"><fmtSetBuildSetup:message key="LBL_DESCRIPTION"/></td>
									<td width="50" align="center"><fmtSetBuildSetup:message key="LBL_SET"/><br><fmtSetBuildSetup:message key="LBL_QTY"/></td>
									<td width="50" align="center"><fmtSetBuildSetup:message key="LBL_CRIT"/>.<br><fmtSetBuildSetup:message key="LBL_QTY" /></td>
									<td width="50" align="center"><fmtSetBuildSetup:message key="LBL_CRIT"/><br><fmtSetBuildSetup:message key="LBL_TYPE"/>Type</td>
									<td width="50" align="center"><fmtSetBuildSetup:message key="LBL_QTY"/><br> <fmtSetBuildSetup:message key="LBL_IN_BULK"/></td>
									<td align="right" width="120">&nbsp;&nbsp;&nbsp;&nbsp;<fmtSetBuildSetup:message key="LBL_QTY"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtSetBuildSetup:message key="LBL_CONTROL"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Number&nbsp;&nbsp;</td>
								</tr>
								<tr><td class=Line colspan=7 height=1></td></tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							int intSetQty = 0;
							boolean bolQtyFl = false;
							String strReadOnly = "";
							StringBuffer sbPartNum = new StringBuffer();
							String strQtyUsed = "";
							int intQtyUsed = 0;
							int intQtyBulk = 0;
							
							for (int i=0;i<intSize;i++)
							{
								String strTDColorClass = new String("RightText");
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								sbPartNum.append(strPartNum);
								sbPartNum.append(",");
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQtyUsed = GmCommonClass.parseZero((String)hmLoop.get("QTYUSED"));
								intQtyUsed = Integer.parseInt(strQtyUsed);

								strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
								intSetQty = Integer.parseInt(strQty);
								//bolQtyFl = intSetQty > 1?true:false;
								strQtyInBulk = GmCommonClass.parseZero((String)hmLoop.get("QTYBULK"));
								intQtyBulk = Integer.parseInt(strQtyInBulk);
								intQtyBulk = intQtyBulk + intQtyUsed; // Adding the current Set's allocated qty
								strQtyInBulk = ""+intQtyBulk;
								strTemp = strPartNum;
								strCritQty = GmCommonClass.parseZero((String)hmLoop.get("CRITQTY"));
								strTDColorClass = intQtyBulk < intSetQty ? "RightTextRed":strTDColorClass;
//								log.debug("intQtyBulk " + (intQtyBulk < intSetQty) + " intSetQty  " +intSetQty + " strTDColorClass : " +strTDColorClass);
								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR ><TD colspan=7 height=1 bgcolor=#eeeeee></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=7 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										strQty = "";
										strQtyInBulk = "";
										//strColor = "bgcolor=white";
										strLine = "";
										strCritQty = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									strQty = "";
									strQtyInBulk = "";
									//strColor = "bgcolor=white";
									strLine = "";
									strCritQty = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
								strItemQty = strItemQty == ""?strQty:strItemQty;
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								if (intSetQty > 1 || intSetQty == 0)
								{
									bolQtyFl = true;
									strReadOnly = "";
								}
								else
								{
									bolQtyFl = false;
									//strItemQty = "1";
									strReadOnly = "ReadOnly";
								}
								out.print(strLine);
%>
								<tr class=ShadeRed >
<%
								if ((strhAction.equals("EditLoad")  && bolQtyFl) || (strhAction.equals("EditLoadDash") && bolQtyFl))
								{
%>
									<td class="<%=strTDColorClass%>" height="20">&nbsp;<a class="RightText" tabindex=-1 href="javascript:fnSplit('<%=i%>','<%=strPartNum%>');"><%=strPartNum%></a>
									<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>"></td>
<%
								}else{
%>
									<td class="<%=strTDColorClass%>" height="20">&nbsp;<%=strPartNum%>
									<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>"></td>
<%
									}
%>
									<td class="<%=strTDColorClass%>"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td align="center" class="<%=strTDColorClass%>"><%=strQty%></td>
									<td align="center" class="<%=strTDColorClass%>"><%=strCritQty%></td>
									<td align="center" class="<%=strTDColorClass%>"></td>
									<td align="center" class="<%=strTDColorClass%>"><%=strQtyInBulk%><input type="hidden" value="<%=""+intQtyBulk%>" name="Qty_Blk<%=strPartNum.replaceAll("\\.","")%>"/></td>
<%
								if (strhAction.equals("EditLoad")|| strhAction.equals("EditLoadDash"))
								{
%>
									<td id="Cell<%=i%>" class="<%=strTDColorClass%>">&nbsp;&nbsp;&nbsp;<input type="text" size="3" value="<%=strItemQty%>" name="Txt_Qty<%=i%>" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="text" size="9" value="<%=strControl%>" name="Txt_CNum<%=i%>" class="InputArea" maxlength="8"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>&nbsp;
									</td>
<%
								}else{
%>
									<td>&nbsp;</td>
<%
									}
%>
								</tr>
<%
								//out.print(strLine);
							}//End of FOR Loop
							out.print("<input type=hidden name=pnums value="+sbPartNum.toString().substring(0,sbPartNum.length()-1)+">");
						}else {
%>
						<tr><td colspan="7" height="50" align="center" class="RightTextRed"><BR><fmtSetBuildSetup:message key="LBL_NO_PART_NUMBERS_HAVE_BEEN_MAPPED_FOR_THIS_SET"/> !</td></tr>
<%
						}		
%>
							</table>
						</td>								<input type="hidden" name="hCnt" value="<%=intSize-1%>">
					</tr>
<%
					}
%>					
					
<%
				if (!strVerFlag.equals("1"))
				{				
					if (strhAction.equals("LoadSet"))
					{
%>
					<tr>
						<td colspan="2" height="30" align="center">
						<fmtSetBuildSetup:message key="LBL_INITIATE" var="varInitiate"/>
							<gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" tabindex="13" buttonType="Save" />
						</td>
					</tr>
<%
					}else if (strhAction.equals("EditLoad") || strhAction.equals("EditLoadDash")){
%>
					<tr>
						<td colspan="2" height="30" align="center">
						<fmtSetBuildSetup:message key="LBL_VOID" var="varVoid"/>
							<gmjsp:button value="${varVoid}" gmClass="button"  onClick="fnVoidSetCsg();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
							<fmtSetBuildSetup:message key="LBL_PIC_SLIP" var="varPICSlip"/>
							<gmjsp:button value="${varPICSlip}" gmClass="button"  onClick="fnPicSlip();" tabindex="14" buttonType="Save" />&nbsp;&nbsp;
							<fmtSetBuildSetup:message key="LBL_SUBMIT" var="varSubmit"/>
							<gmjsp:button value="Submit" gmClass="button"  onClick="fnSubmit();" tabindex="15" buttonType="Save" />
						</td>
					</tr>
<%
					}
				}else{
%>
					<tr>
						<td colspan="2" height="30" align="center" class="RightTextRed"> <fmtSetBuildSetup:message key="LBL_CONSIGNMENT_HAS_BEEN_VERIFIED_AND_TAKEN_OUT_OF_INVENTORY_YOU_MUST_NOT_MAKE_MORE_CHARGES_TO_THE_CONTENT_OF_THIS_SET_IF_YOU_NEED_TO_MAKE_CHANGES_PLEASE_CONTACT_THE_LOGISTICS_MANAGER/SYSTEM_ADMIN"/>
						</td>
					</tr>
<%
				}

%>
				 </table>
			 </td>
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
