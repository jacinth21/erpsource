<%
/**********************************************************************************
 * File		 		: GmGenerateCtrlNum.jsp
 * Desc		 		: This JSP is for the section Generated Control Number and Saved Control Number
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtGenerateCtrlNum" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmGenerateCtrlNum.jsp -->

<fmtGenerateCtrlNum:setLocale value="<%=strLocale%>"/>
<fmtGenerateCtrlNum:setBundle basename="properties.labels.operations.GmGenerateCtrlNum"/>

<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
HashMap hFormVal = null;
HashMap hTxnType = null;
HashMap hSplitDtl = null;
%>

<bean:define id="alCtrlDtls" name="<%=strFormName%>" property="alCtrlDtls" type="java.util.ArrayList"> </bean:define>
<bean:define id="hMode" name="<%=strFormName%>" property="hMode" type="java.lang.String"> </bean:define>
<bean:define id="strGridData" name="<%=strFormName%>" property="strGridData" type="java.lang.String" ></bean:define>
<bean:define id="screenType" name="<%=strFormName%>" property="screenType" type="java.lang.String" ></bean:define>
<bean:define id="alTxnType" name="<%=strFormName%>" property="alTxnType" type="java.util.ArrayList" ></bean:define>
<bean:define id="alDHRSplitdtl" name="<%=strFormName%>" property="alDHRSplitdtl" type="java.util.ArrayList" ></bean:define>
<bean:define id="strDhrstatus" name="<%=strFormName%>" property="dhrstatus" type="java.lang.String"> </bean:define>
<bean:define id="strMsg" name="<%=strFormName%>" property="strMsg" type="java.lang.String"> </bean:define>

<%
Boolean blEditCtrlNum;
// 3: Pending Verification, 4: Verified
blEditCtrlNum = strDhrstatus.equals("3") || strDhrstatus.equals("4") ? false : true;
%>

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: Donor Details </TITLE>

<script>

var ctrlListLen = <%= alCtrlDtls.size()%>;
var txntypeLen = <%= alTxnType.size()%>;
var dhrSplitLen = <%= alDHRSplitdtl.size()%>;
var objGridData	= '<%=strGridData%>';
var screenType	= '<%=screenType%>';
var dhrStatus = '<%=strDhrstatus%>';

<%	
hFormVal = new HashMap();
//For setting the value of control number details
for (int i=0;i< alCtrlDtls.size();i++)
{
	hFormVal = (HashMap) alCtrlDtls.get(i);
%>
var CtrlArr<%=i%> ="<%=GmCommonClass.parseNull((String)hFormVal.get("CTRLNO"))%>,<%=GmCommonClass.parseNull((String)hFormVal.get("CUSTSIZE"))%>";
<%
}
%>

//For setting the drop down value of transaction type
<%	
hTxnType = new HashMap();	
for (int i=0;i< alTxnType.size();i++)
{
	hTxnType = (HashMap) alTxnType.get(i); 
%>
var TypeArr<%=i%> ="<%=GmCommonClass.parseNull((String)hTxnType.get("CODENM"))%>,<%=GmCommonClass.parseNull((String)hTxnType.get("TRANS_ID"))%>,<%=GmCommonClass.parseNull((String)hTxnType.get("QTY"))%>";
<%
}
%>

// For setting the value in grid after save
<%	
hSplitDtl = new HashMap();	
for (int i=0;i< alDHRSplitdtl.size();i++)
{
	hSplitDtl = (HashMap) alDHRSplitdtl.get(i);
%>
var SplitArr<%=i%> ="<%=GmCommonClass.parseNull((String)hSplitDtl.get("TXNTYPE"))%>,<%=GmCommonClass.parseNull((String)hSplitDtl.get("TXNID"))%>";
<%
}
%>
</script>

<BODY leftmargin="20" topmargin="10" onload="onPageLoad()">
<html:hidden property="dhrstatus" name="frmDonorInfo"/>

	<%
	if(blEditCtrlNum || (!hMode.equals("Report")&& !hMode.equals("DHRSplitReport"))){ //No need to show this part for Reports
	%>
	<table class="DtTable500" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="24" class="ShadeRightTableCaption" colspan="4">&nbsp;<b><fmtGenerateCtrlNum:message key="LBL_GENERATE_CONTROL_NUMBER"/></b></td>
			
		</tr>
		<tr><td class="Line" colspan="4"></td></tr>
		<tr>
			<td width="25%"></td>
			<td class="RightText" HEIGHT="20" align="left">&nbsp;<font color="red">* </font><b><fmtGenerateCtrlNum:message key="LBL_PREFIX"/></b></td>
			<td class="RightText" HEIGHT="20" align="left">&nbsp;<font color="red">* </font><b><fmtGenerateCtrlNum:message key="LBL_SERIAL_#"/></b></td>
			<td class="RightText" HEIGHT="20" align="left"><b>Suffix</b></td>
		</tr> 
		<tr>
			<td class="RightText" HEIGHT="20"  align="right" width="25%">&nbsp;<b><fmtGenerateCtrlNum:message key="LBL_ASSIGN_PATTERN"/>:</b></td>
			<td>&nbsp;<html:text property="prefix" styleId="prefix" styleClass="InputArea" name="<%=strFormName %>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="4" size="10"/></td>
			<td>&nbsp;<html:text property="serialNum" styleId="serialNum" styleClass="InputArea" name="<%=strFormName %>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="5" size="30"/></td>
			<td>&nbsp;<html:text property="suffix" styleId="suffix" styleClass="InputArea" name="<%=strFormName %>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="6" size="7"/></td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr> 	
		<tr  class="Shade">
			<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtGenerateCtrlNum:message key="LBL_SCAN_HERE"/>:</b></td>
			<td colspan="3">&nbsp;<html:text property="scan" styleId="scan" styleClass="InputArea" name="<%=strFormName %>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" onkeypress="javascript:fnGenerate(this)" tabindex="7" size="25"/></td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr>
			<td align="center" colspan="4" height="30px"><fmtGenerateCtrlNum:message key="BTN_GENERATE" var="varGenerate"/><gmjsp:button name="Btn_Generate" value="${varGenerate}" gmClass="button" onClick="javascript:fnGenerate(this);" tabindex="8" buttonType="Load" /></td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
	</table>
	<%
	}
	%>
	<table id="tableData" class="DtTable500" cellspacing="0" cellpadding="0" height = "300" style="display: none; border: 1px solid  #676767;">
		<tr>
			<td height="24" class="ShadeRightTableCaption" colspan="4">&nbsp;<b><fmtGenerateCtrlNum:message key="LBL_SAVED_CONTROL_NUMBER"/></b></td>
		</tr>
		<tr><td class="Line"></td></tr>
		<%
		if(blEditCtrlNum || (!hMode.equals("Report") && !hMode.equals("DHRSplitReport"))){
		%>
		<tr><td>
			<div id="dataGrid" height="300"></div>
		</td></tr>
		<%
		}
		else {
		%>
		<tr><td ><div id="dataGridDiv"  style="height:300"></div></td></tr>
		<%
		} 
		%>	
		<%
			if (!strMsg.equals("")) {
		%>
		<tr>
			<td  align="center" height="25"> <font color="red"><b><%=strMsg%></b></font><br/>
			</td>
		</tr>
		<%
			}
		%>
		<tr>
			<td height="30px" align="center">
				<% if((blEditCtrlNum || !hMode.equals("Report") || screenType.equals("DHRDetail"))&&!strDhrstatus.equals("4")){ %>
				<fmtGenerateCtrlNum:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button name="Btn_Submit" value="${varSubmit}" gmClass="button" onClick="javascript:fnSubmit(this);" tabindex="10" buttonType="Save" />
				<%}if(blEditCtrlNum  || !hMode.equals("Report") && !hMode.equals("DHRSplitReport")){ %>
				<fmtGenerateCtrlNum:message key="BTN_DELETE" var="varDelete"/>
				<gmjsp:button name="Btn_Submit" value="${varDelete}" gmClass="button" onClick="javascript:removeSelectedRow();" tabindex="11" buttonType="Save" />
				<fmtGenerateCtrlNum:message key="BTN_RESET" var="varReset"/>
				<gmjsp:button name="Btn_Generate" value="${varReset}" gmClass="button" onClick="javascript:fnReset(this);" tabindex="12" buttonType="Load" />
				<%} %>
				<fmtGenerateCtrlNum:message key="BTN_CLOSE" var="varClose"/>
				<gmjsp:button name="Btn_Generate" value="${varClose}" gmClass="button" onClick="javascript:fnClose(this);" tabindex="13" buttonType="Load" />
			</td>
		</tr>
	</table>
	

<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>