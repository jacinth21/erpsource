 <!-- \GmLoanerReqPrint --> 
 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<bean:define id="alShippingDetails" name="frmLoanerReqEdit" property="alShippingDetails" type="java.util.ArrayList"></bean:define>
<bean:define id="hmReportHeaderDetails" name="frmLoanerReqEdit" property="hmReportHeaderDetails" type="java.util.HashMap"></bean:define>
<%
	String strHtmlJasperRpt = "";
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmPaperworkResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
String strSetRbObjectFl =
GmCommonClass.parseNull(gmPaperworkResourceBundleBean.getProperty("ADD_RESOURCE_BUNDLE_OBJ"));//PMT-53539 japan-new-warehouse-address
%>
<script>
function fnPrint()
{
	window.print();
	return false;
}
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";	
}
function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<table>
		<tr>
			<td align="right" height="30" id="button">
				<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="Click here to print page" onClick="return fnPrint();" />
				<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="Click here to close window" onClick="window.close();" />
			</td>
		</tr>
		
		
	 	<tr>	
			<td>
				<%
				String strJasperName 	= "/GMLoanerRequestPrintPaperWork.jasper";
				GmJasperReport gmJasperReport = new GmJasperReport();			
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strJasperName);
				gmJasperReport.setHmReportParameters(hmReportHeaderDetails);
				gmJasperReport.setReportDataList(alShippingDetails);
				gmJasperReport.setBlDisplayImage(true);
				if (strSetRbObjectFl.equalsIgnoreCase("YES") ) {
			          gmJasperReport.setRbPaperwork(gmPaperworkResourceBundleBean.getBundle());//PMT-53539 japan-new-warehouse-address
			        }
				strHtmlJasperRpt = gmJasperReport.getHtmlReport();				
				%>	
				<%=strHtmlJasperRpt %>			
			</td>
		</tr>
	
	  
</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>