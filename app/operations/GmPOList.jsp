<!-- \operations\GmPOList.jsp -->
 <%@page import="java.util.Date"%>
<%
/**********************************************************************************
 * File		 		: GmPOList.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
x
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtPOList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPOList:setLocale value="<%=strLocale%>"/>
<fmtPOList:setBundle basename="properties.labels.operations.GmPOList"/>

<%
	response.setCharacterEncoding("UTF-8");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);


	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strShade = "";

	String strPOId = "";
	String strVendorNm = "";
	String strVendorId = "";
	String strPOTotal = "";
	String strReqdDate = "";
	String strRaisedBy = "";
	String strVendorCurr  = "";
	ArrayList alPOList = new ArrayList();

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO List </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnCallPO(po,vid)
{
	document.frmVendor.hAction.value = 'ViewPO';
	document.frmVendor.hPOId.value = po;
	document.frmVendor.hVenId.value = vid;
  	document.frmVendor.submit();
}

function fnGo()
{
	document.frmVendor.hAction.value = 'Go';
	document.frmVendor.submit();
}


</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPOServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hPOId" value="">
<input type="hidden" name="hVenId" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3"><fmtPOList:message key="LBL_DASHBOARD_RAISED_PO"/></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="698" height="70" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<TR>
						<td colspan="2">
							<TABLE cellpadding="1" cellspacing="0" border="0" width="100%">
								<TR class="Shade">
									<TD class="RightText" width="100"><b><fmtPOList:message key="LBL_PO"/></b></TD>
									<TD class="RightText" width="250"><b><fmtPOList:message key="LBL_VENDOR"/></b></TD>
									<TD class="RightText" align="center" width="100"><b>&nbsp;<fmtPOList:message key="LBL_PO_TOTAL"/></b></TD>
									<TD class="RightText" width="100" align="center"><b><fmtPOList:message key="LBL_REQ_DATE"/></b></TD>
									<TD class="RightText" align="center" width="120"><b><fmtPOList:message key="LBL_RAISED_BY"/></b></TD>
								</TR>
								<tr><td colspan="5" class="Line" height="1"></td></tr>
<%
					if (strhAction.equals("Load"))
					{
						HashMap hmLoop = new HashMap();
						HashMap hmTempLoop = new HashMap();
						ArrayList alList = (ArrayList)hmReturn.get("POLIST");
						intSize = alList.size();

						for (int i = 0;i < intSize ;i++ )
						{
							hmLoop = (HashMap)alList.get(i);

							strPOId = GmCommonClass.parseNull((String)hmLoop.get("POID"));
							strVendorNm = GmCommonClass.parseNull((String)hmLoop.get("VNAME"));
							strVendorId = GmCommonClass.parseNull((String)hmLoop.get("VID"));
							strPOTotal = GmCommonClass.parseNull((String)hmLoop.get("TOTAL"));
							strReqdDate = GmCommonClass.getStringFromDate((Date)hmLoop.get("REQDT"),strGCompDateFmt);
							strRaisedBy = GmCommonClass.parseNull((String)hmLoop.get("CUSER"));
							strVendorCurr = GmCommonClass.parseNull((String)hmLoop.get("VENDCURR"));
							strShade	= (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows

%>
								<TR <%=strShade%>>
									<td height="20"><a class="RightText" href="javascript:fnCallPO('<%=strPOId%>','<%=strVendorId%>')"><%=strPOId%></td>
									<td class="RightText"><%=strVendorNm%></td>
									<td class="RightText"align="right"><%=strVendorCurr%>&nbsp;<%=GmCommonClass.getStringWithCommas(strPOTotal)%></td>
									<td class="RightText" align="center"><%=strReqdDate%></td>
									<td class="RightText" align="center"><%=strRaisedBy%></td>
								<TR>
<%
						}
					}
%>
							</TABLE>
						</TD>
				   </TR>
				</TABLE>
			</TD>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
