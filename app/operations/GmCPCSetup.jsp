
<%
/**********************************************************************************
 * File		 		: GmCPCSetup.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Thamarai selvan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<bean:define id="alCpcList" name="frmCpcSetup" property="alCpcList" type="java.util.ArrayList"> </bean:define>


<%@ taglib prefix="fmtCPCSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<!-- operations\GmCPCSetup.jsp -->

<fmtCPCSetup:setLocale value="<%=strLocale%>"/>
<fmtCPCSetup:setBundle basename="properties.labels.operations.GmCPCSetup"/>

<% 

String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("CPC_SETUP");
HashMap hcboVal = null;
%>

<script language="JavaScript"> 
var lblCPCName ='<fmtCPCSetup:message key="LBL_NAME"/>';
var lblActiveFlag='<fmtCPCSetup:message key="LBL_ACTIVE_FLAG"/>';
var lblComments='<fmtCPCSetup:message key="LBL_COMMENTS"/>';
var CPCLen = <%=alCpcList.size()%>;
<%
hcboVal = new HashMap();
for (int i=0;i<alCpcList.size();i++)
{
	hcboVal = (HashMap)alCpcList.get(i);
%>
var alCpcArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<%
}
%>

</script>

<HTML>
<HEAD>
<TITLE> Globus Medical: CPC Setup </TITLE>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmCPCSetup.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>
<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmCpcSetup.do"  >
<html:hidden property="strOpt" value=""/>
<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<fmtCPCSetup:message key="LBL_CPC_SETUP"/></td>
		<td  height="25" class="RightDashBoardHeader" align="right">
		<fmtCPCSetup:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		     height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		</td>
	</tr>
	<tr><td colspan="4" class="LLine" height="1"></td></tr>
	<logic:notEmpty property="strMsg" name="frmCpcSetup">
	<tr>
	<td colspan="4" align ="center" height="25"><font color="green" size = "2"><b><bean:write name="frmCpcSetup" property ="strMsg"/></b></font></td>
	</tr>
	</logic:notEmpty>
	<tr>
	<fmtCPCSetup:message key="LBL_NAME_LIST" var="varName"/>
		<gmjsp:label type="BoldText"  SFLblControlName="${varName}:" td="true" width="15%"/>
		<td width="40%" >&nbsp;<gmjsp:dropdown controlName="cpcId" SFFormName="frmCpcSetup" SFSeletedValue="cpcId" 
		SFValue="alCpcList" codeId = "CODEID"  codeName = "CODENM" onChange="fnLoadCPC();"  defaultValue= "[Choose One]" />													
		</td>
	</tr>
     <tr>
     <td class="RightTextBlue" colspan="2" align="center"><fmtCPCSetup:message key="LBL_EDIT_DETAILS_NEW"/>  </td> 
     </tr>
	<tr><td colspan="4" class="LLine"></td></tr>		
	<tr class="Shade">
    <td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;<font color="red">*</font><fmtCPCSetup:message key="LBL_NAME"/> :</td> 
	<td>&nbsp;<html:text property="cpcName" size="30" maxlength="200" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
    </td>
    </tr>  
	<tr><td class="LLine" height="1" colspan="4"></td></tr>
	<tr>
       <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp; <fmtCPCSetup:message key="LBL_ADDRESS"/>:</td>  
	   	<td>&nbsp;<html:textarea property="cpcAddr"  rows="6" cols="75" style="height:90px" 
		styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
		</td>                                                       
     </tr>   
	<tr><td class="LLine" height="1" colspan="4"></td></tr>
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;<fmtCPCSetup:message key="LBL_BARCODE_LEVEL"/> :</td> 
		<td>&nbsp;<html:text property="cpcBarcode" size="30" maxlength="100" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
	    </td>
	</tr>
	<tr><td class="LLine" height="1" colspan="4"></td></tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;<fmtCPCSetup:message key="LBL_FINAL_LEVEL"/> :</td> 
		<td>&nbsp;<html:text property="cpcFinal" size="30" maxlength="100" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
	    </td>
	</tr>
	<tr><td class="LLine" height="1" colspan="4"></td></tr>
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;<font color="red">*</font><fmtCPCSetup:message key="LBL_ACTIVE_FLAG"/> :</td>
		<td width="40%" >&nbsp;<gmjsp:dropdown controlName="activeFl" SFFormName="frmCpcSetup" SFSeletedValue="activeFl"
		SFValue="alActiveFl" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
		</td>
	</tr>
	<tr><td class="LLine" height="1" colspan="4"></td></tr>
	<tr>
		<td colspan="4" align="center" height="24">&nbsp;
	        <jsp:include page="/common/GmIncludeLog.jsp" >
			<jsp:param name="FORMNAME" value="frmCpcSetup" />
			<jsp:param name="ALNAME" value="alLogReasons" />
			<jsp:param name="LogMode" value="Edit" />
			</jsp:include>	
		</td>
	</tr>
	<tr>
		<td  height="30" colspan="4" align="center">&nbsp;
		<fmtCPCSetup:message key="BTN_SUBMIT" var="varSubmit"/>
			<gmjsp:button style="width: 5em" value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save" />&nbsp;
			<fmtCPCSetup:message key="BTN_VOID" var="varVoid"/>
			<gmjsp:button style="width: 5em" value="${varVoid}" gmClass="button" onClick="fnVoid();" buttonType="Save" />
		</td>
	</tr>			
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
