 <%
/**********************************************************************************
 * File		 		: GmVendorPriceSetup.jsp
 * Desc		 		: This screen is used for the Vendor Price Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*"%>


<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtVendorPriceSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtVendorPriceSetup:setLocale value="<%=strLocale%>"/>
<fmtVendorPriceSetup:setBundle basename="properties.labels.operations.GmVendorPriceSetup"/>
<!--operations\GmVendorPriceSetup.jsp  -->

<%
String strpurchasingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	response.setCharacterEncoding("UTF-8");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strProjId =	"";
	String strPartNum = "";
	String strProjNm = "";
	String strPartNumDesc = "";
	String strVendId = "";
	String stVendNm = "";
	String strCost = "";
	String strQuoteQty = "";
	String strDelFrame = "";
	String strQuoteId = "";
	String strUOM = "";
	String strLockFl = "";
	String strActiveFl = "";
	String strActiveFlSel = "";
	String strUOMQty = "";
	String strUOMId = "";
	String strSelectedIds = "";
	String strSelectedUOMIds = "";
	String strValidatedFl = "";
	String strVerifiedFl = "";	
	String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_PRICING_SETUP");
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmVendorPriceSetup", strSessCompanyLocale);
	String strlblchooseone = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CHOOSE"));
	HashMap hmLoop = new HashMap();

	ArrayList alProjList = new ArrayList();
	ArrayList alPartList = new ArrayList();
	ArrayList alPriceList = new ArrayList();
	ArrayList alVendorList = new ArrayList();
	ArrayList alUOMList = new ArrayList();
	StringBuffer sbVendHtml = new StringBuffer();
	StringBuffer sbUOMHtml = new StringBuffer();
	
	int intPriceSize = 0;
	int intSize = 0;
	
	if (hmReturn != null)
	{
		alProjList = (ArrayList)hmReturn.get("PROJECTS");
		strProjId = (String)request.getAttribute("hProjId") == null?"":(String)request.getAttribute("hProjId");
		strPartNum = (String)request.getAttribute("hPartNum") == null?"":(String)request.getAttribute("hPartNum");
		if (strhAction.equals("Part")||strhAction.equals("Go"))
		{
			alPartList = (ArrayList)hmReturn.get("PARTNUMS");
		}
		if (strhAction.equals("Go"))
		{
			alPriceList = (ArrayList)hmReturn.get("PRICELIST");
			alVendorList = (ArrayList)hmReturn.get("VENDORLIST");
			alUOMList = (ArrayList)hmReturn.get("UOMLIST");
			intPriceSize = alPriceList.size();
		}
	}

	HashMap hcboVal = null;
	
	sbVendHtml.append("<SELECT name=Cbo_Vendor class=InputArea style=WIDTH: 200px; onChange=fnPartVendorStatus(this.value,rowid)>");
	sbVendHtml.append("<option value=0 >"+strlblchooseone+"");
	
	intSize = alVendorList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alVendorList.get(i);
  		sbVendHtml.append("<option value='");
  		sbVendHtml.append(hcboVal.get("ID"));
  		sbVendHtml.append("'>");
  		sbVendHtml.append(hcboVal.get("NAME"));
  		sbVendHtml.append("</option>");
  	}	
	sbVendHtml.append("</SELECT>");
	
	sbUOMHtml.append("<SELECT name=Cbo_UOM class=InputArea style=WIDTH: 70px;>");
	sbUOMHtml.append("<option value=0>"+strlblchooseone+"");

	intSize = alUOMList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alUOMList.get(i);
		sbUOMHtml.append("<option value='");
		sbUOMHtml.append(hcboVal.get("CODEID"));
		sbUOMHtml.append("'>");
		sbUOMHtml.append(hcboVal.get("CODENM"));
		sbUOMHtml.append("</option>");
	}
	sbUOMHtml.append("</SELECT>");
			
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Vendor Pricing Setup </TITLE>


<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strpurchasingJsPath%>/GmVendorPriceSetup.js"></script>
<script>
var lblProjectName = '<fmtVendorPriceSetup:message key="LBL_PRO_NAME"/>';
var lblPartNum = '<fmtVendorPriceSetup:message key="LBL_PART_NUM"/>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnSetSelections();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmVendorPricingServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<INPUT type="hidden" name="inputString" value="">
<INPUT type="hidden" name="hLockStr" value="">
<INPUT type="hidden" name="hPartNum" value="<%=strPartNum%>">

<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">		
		<tr>
			<td colspan="5" height="25" class="RightDashBoardHeader">&nbsp;<fmtVendorPriceSetup:message key="LBL_VEN_PRICE"/></td>
				<td height="25" class="RightDashBoardHeader"><fmtVendorPriceSetup:message key="IMG_HELP" var="varHelp"/><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			
		</tr>
</table>	
<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">	
		<tr>
		<td>				
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" align="Right" height="30">&nbsp;<fmtVendorPriceSetup:message key="LBL_PRO_NAME"/>:</td>
						<td align="left">&nbsp;<select name="Cbo_Proj" id="Region" class="RightText" tabindex="3" onChange="javascript:fnCallProject(this);" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtVendorPriceSetup:message key="LBL_CHOOSE"/>
<%
			  		intSize = alProjList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alProjList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strProjId.equals(strCodeID)?"selected":"";
						strProjNm = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=strProjNm%></option>
<%
			  		}
%>
						</select></td>

<%
					if (strhAction.equals("Part") || strhAction.equals("Go"))
					{
%>
						<td  class="RightTableCaption" align="Right"><fmtVendorPriceSetup:message key="LBL_PART_NUM"/>: </td><td>&nbsp;<select name="Cbo_Part" id="Region" class="RightText" tabindex="3" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtVendorPriceSetup:message key="LBL_CHOOSE"/>
<%
			  			intSize = alPartList.size();
						hcboVal = new HashMap();
				  		for (int i=0;i<intSize;i++)
				  		{
				  			hcboVal = (HashMap)alPartList.get(i);
			  				strCodeID = (String)hcboVal.get("ID");
							if (strPartNum.equals(strCodeID))
							{
								strSelected = "selected";
								strPartNumDesc = (String)hcboVal.get("NAME");
							}
							else
							{
								strSelected = "";
							}
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("ID")%></option>
<%
			  			}
%>
						</select></td>
						<td width=20% align="center"><fmtVendorPriceSetup:message key="BTN_LOAD" var="varLoad"/><gmjsp:button style="width: 5em;height: 22" value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnGo();" buttonType="Load" tabindex="16" />
<%
			  		}
%>
						</td>
					</tr>
<%
					if (strhAction.equals("Go"))
					{
%>
					<tr>
						<td  align="right" class="RightTableCaption" height="30"><fmtVendorPriceSetup:message key="LBL_PART_DESC"/>:</td><td align="left" colspan="3">&nbsp;<%=GmCommonClass.getStringWithTM(strPartNumDesc)%></td></tr>
							<TABLE cellspacing="0" border="0" width="100%" id="PriceTable">
							  <tbody>
								<tr><td colspan="11" class="Line" height="1"></td></tr>	
								<TR class="ShadeRightTableCaption">
									<TD width="150" align="right"><fmtVendorPriceSetup:message key="LBL_VENDOR"/></TD>
									<TD align="center"><fmtVendorPriceSetup:message key="LBL_COST"/></TD>
									<TD align="center"><fmtVendorPriceSetup:message key="LBL_QTY_QUOTED"/><br></TD>
									<TD align="center"><fmtVendorPriceSetup:message key="LBL_DEL_TIM"/><br></TD>
									<TD align="center"><fmtVendorPriceSetup:message key="LBL_QUOTE_ID"/><BR></TD>
									<TD align="center"><fmtVendorPriceSetup:message key="LBL_UOM"/></TD>
									<TD align="center"><fmtVendorPriceSetup:message key="LBL_UOM_QTY"/><BR></TD>
									<TD align = "Center"><fmtVendorPriceSetup:message key="LBL_LOCK"/>?</TD>
									<TD align = "Center"><fmtVendorPriceSetup:message key="LBL_ACTIVE"/>?</TD>
									<TD align = "Center"><fmtVendorPriceSetup:message key="LBL_VALIDATED"/>?</TD>
									<TD align = "Center"><fmtVendorPriceSetup:message key="LBL_VERIFIED"/>?</TD>
								</TR>
							<tr><td colspan="11" class="Line" height="1"></td></tr>	
<%
						hmLoop = new HashMap();
						int j = 0;
						if (intPriceSize > 0)
						{
						   for (int i=0;i<intPriceSize;i++)
						   {
								hmLoop = (HashMap)alPriceList.get(i);
								strVendId = GmCommonClass.parseNull((String)hmLoop.get("VID"));
								stVendNm = GmCommonClass.parseNull((String)hmLoop.get("VNM"));
								strCost = GmCommonClass.parseNull((String)hmLoop.get("CPRICE"));
								strQuoteQty = GmCommonClass.parseNull((String)hmLoop.get("QTYQ"));
								strDelFrame = GmCommonClass.parseNull((String)hmLoop.get("DFRAME"));;
								strQuoteId = GmCommonClass.parseNull((String)hmLoop.get("QID"));
								
								strLockFl = GmCommonClass.parseNull((String)hmLoop.get("LFL"));
								strActiveFl = GmCommonClass.parseNull((String)hmLoop.get("AFL"));
								strActiveFlSel = strActiveFl.equals("Y")?"checked":"";
								strUOM = GmCommonClass.parseNull((String)hmLoop.get("UOM"));
								strUOMQty = GmCommonClass.parseNull((String)hmLoop.get("UOMQTY"));
								strUOMId = GmCommonClass.parseNull((String)hmLoop.get("UOMID"));
								strValidatedFl = GmCommonClass.parseNull((String)hmLoop.get("VALIDATE_FL"));
								strVerifiedFl = GmCommonClass.parseNull((String)hmLoop.get("VERIFIED_FL"));
								
								if (!strLockFl.equals("Y"))
								{
									strSelectedIds = strSelectedIds.concat(strVendId).concat(",");
									strSelectedUOMIds = strSelectedUOMIds.concat(strUOM).concat(",");
									
									stVendNm = sbVendHtml.toString().replaceAll("rowid",String.valueOf(i));
									strCost = "<input type=text size=5 maxlength=17 class=InputArea name=Txt_Cost"+i+" value='"+strCost+"'>";
									strQuoteQty = "<input type=text size=3 class=InputArea name=Txt_QtyQ"+i+" value='"+strQuoteQty+"'>";
									strDelFrame = "<input type=text size=15 maxlength=72 class=InputArea name=Txt_Delv"+i+" value='"+strDelFrame+"'>";
									strQuoteId = "<input type=text size=7 maxlength=20 class=InputArea name=Txt_QiD"+i+" value='"+strQuoteId+"'>";
									strUOM = sbUOMHtml.toString();
									strUOMQty = "<input type=text size=3 maxlength=7 class=InputArea name=Txt_UOMQ"+i+" value='"+strUOMQty+"'>";
									strLockFl = "<input type=checkbox name=Chk_Lock"+i+">";
									j++;
								}
								else
								{
									stVendNm = stVendNm.concat(" <input type=hidden name=hVendor"+i+" value='"+strVendId+"'>");
									strCost = strCost.concat("<input type=hidden name=Txt_Cost"+i+" value='"+strCost+"'>");
									strQuoteQty = strQuoteQty.concat("<input type=hidden name=Txt_QtyQ"+i+" value='"+strQuoteQty+"'>");
									strDelFrame = strDelFrame.concat("<input type=hidden name=Txt_Delv"+i+" value='"+strDelFrame+"'>");
									strQuoteId = strQuoteId.concat("<input type=hidden name=Txt_QiD"+i+" value='"+strQuoteId+"'>");
									strUOM = strUOM.concat(" <input type=hidden name=hUOM"+i+" value='"+strUOMId+"'>");
									strUOMQty = strUOMQty.concat("<input type=hidden name=Txt_UOMQ"+i+" value='"+strUOMQty+"'>");
									strLockFl = strLockFl.concat("<input type=hidden name=Chk_Lock"+i+" value='"+strLockFl+"'>");
								}
									strActiveFl = "<input type=checkbox name=Chk_Active"+i+" "+strActiveFlSel+">";
%>
								<tr height="18">
									<td class="RightText">&nbsp;<%=stVendNm%>
									<input type="hidden" name="hId<%=i%>" value="<%=hmLoop.get("PID")%>"></td>
									<td class="RightText" align="right"><%=strCost%>&nbsp;</td>
									<td class="RightText" align="center"><%=strQuoteQty%></td>
									<td class="RightText"><%=strDelFrame%></td>
									<td class="RightText"><%=strQuoteId%></td>
									<td class="RightText">&nbsp;<%=strUOM%></td>
									<td class="RightText" align="center"><%=strUOMQty%></td>
									<td class="RightText" align="center"><%=strLockFl%></td>
									<td class="RightText" align="center"><%=strActiveFl%></td>
									<td class="RightText" align="center"><div id='Validate_Fl_<%=i%>'><%=strValidatedFl%></div></td>
									<td class="RightText" align="center"><div id='Verified_Fl_<%=i%>'><%=strVerifiedFl%></div></td>
								</TR>
								<TR><td bgcolor="#eeeeee" height="1" colspan="11"></td></TR>
<%
							}

						}%>
						<tbody>
						</table>
						</td>
				   </tr>
				    <tr>
						<td colspan="2" align="center" height="35">
							<INPUT type="hidden" name="hCnt" value="<%=intPriceSize%>">
							<input type="hidden" name="strSelectedVendIds" value="<%=strSelectedIds%>">
							<input type="hidden" name="hSelectedUOMIds" value="<%=strSelectedUOMIds%>">
							<fmtVendorPriceSetup:message key="BTN_ADD_ROW" var="varAddRow"/><gmjsp:button value="${varAddRow}" gmClass="button" buttonType="Save" onClick="fnAddRow('PriceTable');" tabindex="16" />&nbsp;&nbsp;
							<fmtVendorPriceSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" buttonType="Save" onClick="fnSubmit();" tabindex="16" />&nbsp;
						</td>
					</tr>
<%
					}else{
%>
				<tr height="50"><td></td></tr>

<%} %>
				</table>
			</td>
		</tr>
		
    </table>
    
    <DIV id="Vend" style="visibility:hidden">
<%
		out.print(sbVendHtml.toString());
%>
    </DIV>
    
    <DIV id="UOM" style="visibility:hidden">
<%
		out.print(sbUOMHtml.toString());
%>
    </DIV>
    
</FORM>

<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
