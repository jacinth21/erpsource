<%
/**********************************************************************************
 * File		 		: GmTTPMonthlySummary.jsp
 * Desc		 		: TTP Monthly Summary
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<!-- \operations\forecast\GmTTPMonthlySummary.jsp -->
<%@ include file="/common/GmHeader.inc" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="forecastMonths" name="frmTTPMonthlySummary" property="ttpForecastMonths" type="java.lang.String"> </bean:define>
<bean:define id="yearId" name="frmTTPMonthlySummary" property="yearId" type="java.lang.String"> </bean:define>
<bean:define id="monthId" name="frmTTPMonthlySummary" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="inventoryId" name="frmTTPMonthlySummary" property="inventoryId" type="java.lang.String"> </bean:define>
<bean:define id="demandSheetIds" name="frmTTPMonthlySummary" property="demandSheetIds" type="java.lang.String"> </bean:define>
<bean:define id="ttpId" name="frmTTPMonthlySummary" property="ttpId" type="java.lang.String"> </bean:define>
<bean:define id="strOpt" name="frmTTPMonthlySummary" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="ttpType" name="frmTTPMonthlySummary" property="ttpType" type="java.lang.String"> </bean:define>
<bean:define id="ttpName" name="frmTTPMonthlySummary" property="ttpName" type="java.lang.String"> </bean:define>

<% 
	//Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger
	//String strWikiPath = GmCommonClass.getString("GWIKI");
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("TTP_MONTHLY_SUMMARY");
	HashMap hmReportValue = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMVALUES"));
	String strStatus = GmCommonClass.parseNull((String)hmReportValue.get("STATUS")) ;
	String strDSStatus = GmCommonClass.parseNull((String)hmReportValue.get("DSSTATUS")) ; // For Submit button to display.
	log.debug("strstrDSStatus = " + strDSStatus);
	ArrayList alDrillDown = new ArrayList();
	ArrayList alSheetList = new ArrayList();
	if(forecastMonths.equals(""))
	{
		forecastMonths = "4";
	}	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: TTP Monthly Summary </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/DemandSheet.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmTTPMonthlySummary.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnFilter()
{
var qtyOper = document.frmTTPMonthlySummary.qtyOper.value;
var orderQty = document.frmTTPMonthlySummary.orderQty.value;
var pnum = document.frmTTPMonthlySummary.pnum.value;
var ordOper = document.frmTTPMonthlySummary.ordOper.value;
var orderCost = document.frmTTPMonthlySummary.orderCost.value;
var summaryTotal = document.frmTTPMonthlySummary.summaryTotal.checked;
var quarAvail = document.frmTTPMonthlySummary.quarAvail.checked;
var quarAlloc = document.frmTTPMonthlySummary.quarAlloc.checked;
var pdesc = document.frmTTPMonthlySummary.pdesc.checked;

loadajaxpage('/gmTTPMonthlySummary.do?strOpt=reportDetails&ttpId=<%=ttpId%>&yearId=<%=yearId%>&monthId=<%=monthId%>&inventoryId=<%=inventoryId%>&ttpForecastMonths=<%=forecastMonths%>&demandSheetIds=<%=demandSheetIds%>&qtyOper='+qtyOper+'&orderQty='+orderQty+'&pnum='+encodeURIComponent(pnum)+'&ordOper='+ordOper+'&orderCost='+orderCost+'&summaryTotal='+summaryTotal+'&pdesc='+pdesc+'&quarAlloc='+quarAlloc+'&quarAvail='+quarAvail,'ajaxreportarea'); 
}
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  >
<html:form action="/gmTTPMonthlySummary.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="demandSheetIds"/>
<html:hidden property="inventoryId" />
<html:hidden property="httpId"/>
<html:hidden property="ttpForecastMonths"/>
<html:hidden property="strPnumOrd"/>
<html:hidden property="ttpName"/>
 
 
<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">TTP Monthly Summary</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td width="1000" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" >
                    <tr><td colspan="7" class="ELine"></td></tr> 
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;TTP Name List:</td>
						<td>
                            <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					        <jsp:param name="CONTROL_NAME" value="ttpId" />
					        <jsp:param name="METHOD_LOAD" value="loadTTPList" />
					        <jsp:param name="WIDTH" value="300" />
					        <jsp:param name="CSS_CLASS" value="search" />
					        <jsp:param name="TAB_INDEX" value=""/>
					        <jsp:param name="CONTROL_NM_VALUE" value="<%=ttpName %>" />
					        <jsp:param name="CONTROL_ID_VALUE" value="<%=ttpId %>" />
					        <jsp:param name="SHOW_DATA" value="50" />
				            </jsp:include>	
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;Month:</td>
						<td >&nbsp;
							<gmjsp:dropdown controlName="monthId" SFFormName="frmTTPMonthlySummary" SFSeletedValue="monthId"
								SFValue="alMonth" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;Year:</td>
						<td >&nbsp;
							<gmjsp:dropdown controlName="yearId" SFFormName="frmTTPMonthlySummary" SFSeletedValue="yearId"
								SFValue="alYear" codeId = "CODENM"  codeName = "CODENM"  defaultValue= "[Select]" />														
						</td>		
						<td>
							<gmjsp:button name="loadReport" value="&nbsp;Load&nbsp;&nbsp;" gmClass="button" onClick="fnReport();" buttonType="Load" />
						</td>			
					</tr>
                    <tr><td colspan="7" class="Line"></td></tr> 
                    <% if(!strOpt.equals("")) { %>
					<tr>
						<td colspan ="7" align=center>
						<ul id="maintab" class="shadetabs">
		 			 
    					<li><a href="/gmTTPMonthlySummary.do?strOpt=reportSummary&ttpId=<%=ttpId%>&yearId=<%=yearId%>&monthId=<%=monthId%>&inventoryId=<%=inventoryId%>&ttpForecastMonths=<%=forecastMonths%>&demandSheetIds=<%=demandSheetIds%>" rel="ajaxcontentarea" title="Summary"  class="selected">Summary</a></li>												
						<%						
							HashMap hmapValue = new HashMap();
							alSheetList = (ArrayList)hmReportValue.get("SHEETNAME");
							String strName = "";
							String strAltName = "";
							String strID = "";
							String strDMID = ""; 
							String strDemandSheetIds = ""; 
							int Headersize = alSheetList.size();
							int cnt = 0; 
						 
							for (int i = 0; i < Headersize; i++)
							{ 
								hmapValue = (HashMap) alSheetList.get(i);
								strName = (String)hmapValue.get("NAME");
								strDMID = (String)hmapValue.get("DMID");
								strAltName = new String(strName);
								strName = strName.replaceAll("(?i)"+ttpName, "");  //replace ignore case
							 	if ( Headersize >= 5 )
								{
									strName = strName.length() > 20 ? strName.substring(0,14) : strName;
								}
							 	strName = strName.replaceAll("-", "");
								strID = (String)hmapValue.get("ID");
								strDemandSheetIds += strID + ",";
							  //log.debug("strID : "+ strID);
						%> 
						
					
							<li><a href="/gmDSSummary.do?demandSheetId=<%=strDMID%>&demandSheetMonthId=<%=strID%>&strOpt=ttpfetch&forecastMonths=<%=forecastMonths%>&yearId=<%=yearId%>&monthId=<%=monthId%>"  
							 	rel="ajaxcontentarea" title="<%=strAltName%>"><%=strName%></a></li>
						<% }%>	
						
						</ul>
						  <script> 						  
						 	 document.frmTTPMonthlySummary.demandSheetIds.value='<%=strDemandSheetIds%>';
						  </script>
				<div id="ajaxdivcontentarea" class="contentstyle" style="display:visible;height: 650px; overflow: auto;" >							
<%
				if (strStatus.equals("50581"))
				{
%>				
					<tr>
						<td colspan ="7" align=center height="35"><gmjsp:button name="lock" value="&nbsp;Lock & Generate&nbsp;" gmClass="button" disabled="true" onClick="fnLock();" buttonType="Save" /></td>
					</tr>
<%
				}
%>				</div>
				</td></tr>
				
				<script type="text/javascript">				    
						var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
					    maintab.setpersist(false);								
					    maintab.init();					    
					    maintab.onajaxpageload=function(pageurl){					    
					     if (pageurl.indexOf("gmDSSummary.do")!=-1){					    
					     var demandtab=new ddajaxtabs("demandtab", "ajaxdivtabarea");
						 demandtab.setpersist(true);	
						 demandtab.setselectedClassTarget("link") //"link" or "linkparent"
						 demandtab.init();
						}
						else if(pageurl.indexOf("gmTTPMonthlySummary.do")!=-1) {					
						var summarytab=new ddajaxtabs("summarytab", "ajaxdivtabarea");				
					    	summarytab.setpersist(true);		
					    	summarytab.setselectedClassTarget("link") //"link" or "linkparent"						
					    	summarytab.init();			
						}
						}											    
					</script>			
					<% }%>					
			   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
