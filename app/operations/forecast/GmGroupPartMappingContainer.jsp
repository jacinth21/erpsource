<%
/**********************************************************************************
 * File		 		: GmGroupPartMapping.jsp
 * Desc		 		: Map the parts to correspoding Groups
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList" %>
<bean:define id="groupName" name="frmGroupPartMapping" property="groupName" type="java.lang.String"></bean:define> 
<bean:define id="groupId" name="frmGroupPartMapping" property="groupId" type="java.lang.String"></bean:define> 
<bean:define id="alGroupList" name="frmGroupPartMapping" property="alGroupList" type="java.util.ArrayList"></bean:define>


<%
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROUP_PART_MAP");
String strTDHeight = "800px";
String strTDWidth = "1100px";
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Group Part Mapping Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
<link rel="STYLESHEET" type="text/css"href="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.css">
<style type="text/css">
.dhx_tabcontent_zone{
  	min-width:1000px;
  	min-height:800px;
    /* overflow:hidden; */
}  
</style>

<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTabbar/dhtmlxtabbar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmGroupPartDetail.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmGroupPartMapping.js"></script>
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">

 
var trSize = '<%=strTDHeight%>';
//Function will call on page load. It will append companyInfo in each tab href and initiate ajaxtab.
function fnAjaxTabLoad(){
    var groupid = document.getElementById("groupId").value
	myTab = new dhtmlXTabBar("my_tabbar");
	myTab.setSkin("winscarf");
	myTab.setImagePath("<%=strJsPath%>/dhtmlx/dhtmlxTabbar/imgs/");
	myTab.addTab("details",message_prodmgmnt[466]);
	/* Language and Uploads tab should load only if set id is there */
	if(groupid != 0 && groupid != ''){
		myTab.addTab("language", message_prodmgmnt[467]);
		myTab.addTab("uploads", message_prodmgmnt[468]);
	}
	
	myTab.attachEvent("onSelect", function(id,last_id){	
	
		global_param = "&companyInfo="+encodeURIComponent(JSON.stringify(top.gCompanyInfo));
		
	   if(id=='details'){
		   
	    	document.getElementById("my_tabbar").style.height = trSize;	
	    	href="/gmGroupPartMap.do?haction=edit&groupId="+groupid+"&hTabName=details"+global_param;
	    	
	   }else if(id=='language' && groupid != 0 && groupid != ''){
	    	document.getElementById("my_tabbar").style.height = "800px";
	    	href="/gmLanguageDetail.do?typeID=103094&hType=103094&refID="+groupid+"&strOpt=report&hTabName=language"+global_param;
		}else if(id=='uploads' && groupid != 0 && groupid != ''){
			  document.getElementById("my_tabbar").style.height = "700px";
			  href="/gmUploadDetail.do?refID="+groupid+"&strOpt=report&typeID=103094&hTabName=uploads"+global_param;
	    }
 
	   myTab.cells(id).attachURL(href); //Loading the contents in tab
	   return true;
	   });
	
}
function fnOnloadpage(){
	
	groupid = document.getElementById("groupId").value;
	myTab.setTabInActive(); 
	myTab.setTabActive("details"); //To set details tab active when loaing the page
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnAjaxTabLoad();fnOnloadpage();">
<html:form action="/gmGroupPartMap.do">

<html:hidden property="strForward" />
<html:hidden property="strOpt" />
<html:hidden property="hcurrentTab" />

	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Group Part Mapping Setup</td>
			<td colspan="3" align="right" class=RightDashBoardHeader > 	
			<img id='imgEdit' style='cursor:hand' src='/images/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
						
		<tr>
			<td width="100%" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
			<td class="RightTableCaption" align="right" HEIGHT="24" width="26%">&nbsp;Group List:</td>
			
			<td colspan="3" height="30" align="left">
			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="groupId" />
			 <jsp:param name="METHOD_LOAD" value="loadDemandGroupList&searchType=PrefixSearch" /> 
				<jsp:param name="WIDTH" value="510" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="CONTROL_NM_VALUE" value="<%=groupName %>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=groupId%>" />
				<jsp:param name="SHOW_DATA" value="100" />
				<jsp:param name="AUTO_RELOAD" value="fnFetch(this.form)" />					
			</jsp:include>  
											
						</td> </tr>
					<tr>
                        <td class="RightTextBlue" colspan="2" align="center">Choose one from the list above to edit</td> 
                    </tr>
                    <tr><td colspan="2" class="Line"></td></tr>
</html:form>

                    <tr><td colspan="3" id="my_tabbar" style=" height:'<%=strTDHeight%>'; width:'<%=strTDWidth%>';" ></td></tr>
                   
   				</table>
  			   </td>
  		  </tr>	
    </table>
	     	
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

