 <%
/**********************************************************************************
 * File		 		: GmMonthlySheetReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<!-- \operations\forecast\GmMonthlySheetReport.jsp --> 
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%

String strOperationsPurchasingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_PURCHASING");
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	
	String strAction = (String)request.getAttribute("hAction");
	
	String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_REPORT");
	
	//Read control values for reload
	String strSheetName = GmCommonClass.parseNull((String)request.getAttribute("SHEETNM"));
	String strStatus=GmCommonClass.parseNull((String)request.getAttribute("STATUS"));
	String strSheetType=GmCommonClass.parseNull((String)request.getAttribute("SHEETTYPE"));
	String strMonth=GmCommonClass.parseNull((String)request.getAttribute("MONTH"));
	String strYear=GmCommonClass.parseNull((String)request.getAttribute("YEAR"));
	String strTTP = GmCommonClass.parseNull((String)request.getAttribute("TTP"));
	String strTTPNm = GmCommonClass.parseNull((String)request.getAttribute("TTPNM"));
	String strSheetOwner = GmCommonClass.parseNull((String)request.getAttribute("SHEETOWNER"));
	
	HashMap hmLists = (HashMap)request.getAttribute("HMLISTS");
	
	ArrayList alStatus 	  = (ArrayList)hmLists.get("ALSTATUS");
	ArrayList alSheetType = (ArrayList)hmLists.get("ALSHEETTYPE");
	ArrayList alMonth     = (ArrayList)hmLists.get("ALMONTH");
	ArrayList alYear      = (ArrayList)hmLists.get("ALYEAR");
	
	ArrayList alSheetOwner    = (ArrayList)hmLists.get("ALSHEETOWNER"); 	
	
	String strLimitedAccess = GmCommonClass.parseNull((String)request.getAttribute("DMNDSHT_LIMITED_ACC"));
		
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Monthly Sheet Summary</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsPurchasingJsPath%>/GmMonthlySheetReport.js"></script>  
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmMonthlySheetReport" method="POST" action="<%=strServletPath%>/GmTTPMonthlySheetServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hTxnName" value="">
<input type="hidden" name="hCancelType" value="EXEDS">

<!-- Custom tag lib code modified for JBOSS migration changes -->
<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="4" height="25" class="RightDashBoardHeader">Monthly Sheet Summary</td>
		
		<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
	</tr>	
	
	<tr>
	<td class="RightTableCaption" align="right"  height="20" nowrap >&nbsp;Sheet Name:&nbsp;</td>
	<td> 
      	<input type="text" size="40"   value="<%=strSheetName%>" name="txt_SheetName" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
    </td>
	
	<td class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Status:&nbsp;</td>
	<td><gmjsp:dropdown controlName="Cbo_Status"  seletedValue="<%= strStatus %>"  tabIndex="2" 	 
						    value="<%= alStatus%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"    />
	</td>
	</tr>
	
	<tr>
	<td   class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Sheet Type:&nbsp;</td>
	<td><gmjsp:dropdown controlName="Cbo_SheetType"  seletedValue="<%= strSheetType %>"  tabIndex="3" 	 
						    value="<%= alSheetType%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"    />
	</td>
	<td   class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Demand Period:&nbsp;</td>
	
	<td><gmjsp:dropdown controlName="Cbo_Month"  seletedValue="<%= strMonth %>" 	 tabIndex="4"
						    value="<%= alMonth%>" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]"    />
						    
						    
	<gmjsp:dropdown controlName="Cbo_Year"  seletedValue="<%= strYear %>" 	 tabIndex="5"
						    value="<%= alYear%>" codeId = "CODENM"  codeName = "CODENM"  defaultValue= "[Choose One]"    />					    
						    
	</td>
	
	</tr>
	
	<tr>
		
	    <td class="RightTableCaption" align="right"  height="20" nowrap >&nbsp;TTP Name:&nbsp;</td>
	    <td>
            <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
			<jsp:param name="CONTROL_NAME" value="Cbo_ttp" />
			<jsp:param name="METHOD_LOAD" value="loadTTPList" />
			<jsp:param name="WIDTH" value="280" />
			<jsp:param name="CSS_CLASS" value="search" />
			<jsp:param name="TAB_INDEX" value="6"/>
			<jsp:param name="CONTROL_NM_VALUE" value="<%=strTTPNm %>" />
			<jsp:param name="CONTROL_ID_VALUE" value="<%=strTTP %>" />
			<jsp:param name="SHOW_DATA" value="50" />				
			</jsp:include>		
	    </td>
	    
	    <td   class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Sheet owner:&nbsp;</td>
	
	<td><gmjsp:dropdown controlName="Cbo_SheetOwner"  seletedValue="<%= strSheetOwner %>" 	 tabIndex="7"
						    value="<%= alSheetOwner%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"    />
	    
		 
		&nbsp;&nbsp;&nbsp;<gmjsp:button value="&nbsp;&nbsp;Go&nbsp;&nbsp;" name="Btn_Go" tabindex="8" gmClass="button" onClick="javascript:fnGo()" buttonType="Load" />
		 
	</tr>
	
	<tr> 
		<td colspan="4" >
			<display:table name="requestScope.results.rows" class="its" id="currentRowObject"   decorator="com.globus.displaytag.beans.DTMonthlyDemandSheetWrapper"> 
				<display:column property="DMSHEETID" title="" sortable="true" class="alignleft" style="width:68px" />
				<display:column property="DEMNM" title="Sheet Name" style="text-align:left"  />
				<display:column property="DTYPE" title="Sheet Type" sortable="true"    />
				<display:column property="CUSER" title="Sheet Owner" sortable="true" maxLength="15" />				
				<display:column property="PERIODDATE" title="Demand Period <br> Date" sortable="true"  class="aligncenter"/>
				<display:column property="DPER" title="Demand <br> Period" sortable="true" class="aligncenter"/>
				<display:column property="FPER" title="Forecast <br>Period" sortable="true"  class="aligncenter"/>
				<display:column property="DSTAT" title="Status" sortable="true" style="text-align:left" />
				<display:column property="LUSER" title="Last Updated<br> By" sortable="true" maxLength="15" style="text-align:left"/>
			</display:table> 	
		</td>
	</tr>
</table>

</FORM>
<%
}
catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
