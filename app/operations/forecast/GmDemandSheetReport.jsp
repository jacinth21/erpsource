<%
/**********************************************************************************
 * File		 		: GmDemandSheetMap.jsp
 * Desc		 		: mapping Group / Set / Region / InHouse
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<!-- \operations\forecast\GmDemandSheetReport.jsp -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ include file="/common/GmHeader.inc" %>

 
<% 
//Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger 
//String strServletPath = GmCommonClass.getString("GMSERVLETPATH"); 

//String strWikiPath = GmCommonClass.getString("GWIKI");
String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_REPORT");

try { %>

<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnSubmit()
{
	fnStartProgress('Y');
	document.frmDemandSheetReport.submit();
}



 
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmDemandSheetReport.do?method=demandSheetReport"  >
 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Demand Sheet Report</td>
			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='/images/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />  
				</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="3" class="Line"></td></tr>
                      
                    <tr>
                    	<td class="RightTableCaption" width="50" align="right" HEIGHT="30" ></font>&nbsp;Type :</td> 
                        <td width="120" >&nbsp;
                        <table>
                        	<logic:iterate id="demandSheetTypeList" name="frmDemandSheetReport" property="alDemandSheetType">
                        	<tr>
                        		<td>
							    	<htmlel:multibox property="chkDemandSheetType" value="${demandSheetTypeList.CODEID}" />
								    <bean:write name="demandSheetTypeList" property="CODENM" />
								</td>
							</tr>	    
							</logic:iterate> 
						</table>
                        </td>
                        <td>
			                  <gmjsp:button value="&nbsp;&nbsp;&nbsp;Go&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnSubmit();" buttonType="Load" /> 
			            </td>
                            <tr><td colspan="3" class="Line"></td></tr>
                    <tr>
                    	<td colspan="3" align="center">
                    		<display:table name="requestScope.RDDSREPORT.rows" requestURI="/gmDemandSheetReport.do?method=demandSheetReport" class="its" id="currentRowObject" > 
							<display:column property="DEMANDSHEETNAME" title="Demand Sheet Name" sortable="true" class="alignleft"/>
							<display:column property="DEMANDSHEETTYPENAME" title="Type" sortable="true" class="alignleft"/> 
							<display:column property="DEMANDPERIOD" title="Demand Period" sortable="true"  />
							<display:column property="FORECASTPERIOD" title="Forecast Period" sortable="true"  />
							<display:column property="INCLUDECURRMONTH" title="Include Curr Month" sortable="true"  />
							<display:column property="ACTIVEFLAG" title="Active Flag" sortable="true" class="alignleft" />
							</display:table> 
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>

<% 
} 
catch (Exception e) {
e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

