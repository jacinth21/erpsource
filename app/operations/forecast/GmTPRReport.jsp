<%@ include file="/common/GmHeader.inc" %>
<!-- \operations\forecast\GmTPRReport.jsp-->
<%
String strWikiTitle = GmCommonClass.getWikiTitle("TPR_REPORT");
%>

<%@page import="java.util.HashMap"%>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
 
function fnConsgPrint(strConId)
{
	windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+strConId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnViewDetails(strReqId)
{       
	 windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+strReqId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}
       
function fnViewOrder(val)
{
	windowOpener('/GmEditOrderServlet?hMode=PrintPrice&hOrdId='+val,"Pack","resizable=yes,scrollbars=yes,top=250,left=250,width=810,height=450");	
}     
function fnViewInhouseDetails(strReqId)
{       
	 windowOpener("/GmReprocessMaterialServlet?hAction=ViewInHouse&hId="+strReqId,"Inhouse_Details","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
}
       
</script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<bean:define id="pnum" name="frmTTPMonthlyForm" property="pnum" type="java.lang.String"></bean:define>
<bean:define id="pdesc" name="frmTTPMonthlyForm" property="pdesc" type="java.lang.String"></bean:define>

<html:form action="/gmTPRReport.do">
 
<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
<tr class=Line >
	<td height=25 class=RightDashBoardHeader colspan=5>Open Request Report </td>
			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
	</tr>
<tr>
  <td ><b>Part #:<b> <%=pnum%></b></b></td>
   </tr>
  <tr>
  <td ><b>Part Description:<b> <%=pdesc%></b></b></td>
  </tr>
	<tr><td colspan="11" class="ELine"></td></tr>	 				 
                    <tr>
	                     <td COLSPAN="8">
	                     	<display:table  name="requestScope.frmTTPMonthlyForm.alTPRReport" class="its" id="currentRowObject" requestURI="/gmTPRReport.do" export="true" defaultsort="1"  varTotals="totals" decorator="com.globus.displaytag.beans.DTTPRWrapper"> 
							<display:column property="REQID" title="Request ID"  sortable= "true" class="alignleft" style="width:90" />
							<display:column property="CONS_ID" title="Consignment ID"   sortable= "true" class="alignleft"/>
							<display:column property="SHEET" title="Demand Sheet"  class="alignleft"/>
							<display:column property="SOURCE" title="Source"  class="alignleft"/>
							<display:column property="REQ_TO" title="Request To"  class="alignleft"/>									
							<display:column property="REQUEST_DATE" title="Request Date"  sortable= "true" class="aligncenter"/> 
							<display:column property="REQUIRED_DATE" title="Required Date"  sortable= "true" class="aligncenter"/>											 
							<display:column property="SFL" title="Status"  class="alignleft"/>
							<display:column property="QTY" title="Qty"  total = "true" class="alignright"/>
							<display:column property="UPDATED_BY" title="Updated By"  class="alignleft"/>
							<display:column property="UPDATED_DATE" title="Updated Date"  class="aligncenter"/>								
							<display:footer media="html">
					<% 
							String strVal ;
							strVal = ((HashMap)pageContext.getAttribute("totals")).get("column9").toString();					  		
					%>
								<tr><td colspan="11" class="Line"></td></tr>
				  				<tr class = "shade">
						  			<td colspan="8" class = "alignright"> <B> Total: </B></td>
						  			<td class = "alignright" ><B><%=strVal%></B></td>
						  			<td colspan="2" ></td>
				  				</tr>
				  				<tr><td colspan="11" class="Line"></td></tr>
	 					</display:footer>						
							</display:table>
    		             </td>
                    </tr> 
                    <tr class=Line >
	<td height=25 class=RightDashBoardHeader colspan=11>Sales Back Orders </td>
	</tr>
	<tr><td colspan="6" class="ELine"></td></tr>
	                   <tr>
	                     <td COLSPAN="8">
	                     	<display:table  name="requestScope.frmTTPMonthlyForm.alTPRSalesReport" class="its" id="currentRowObject" requestURI="/gmTPRReport.do" export="true" defaultsort="1"  varTotals="totals" decorator="com.globus.displaytag.beans.DTTPRWrapper"> 
							<display:column property="ORDID" title="Order ID"  sortable= "true" class="alignleft" style="width:90" />																					
							<display:column property="ACCID" title="Account"  class="alignleft"/>	
						   	<display:column property="REQUESTED_NAME" title="Requested Name"  class="aligncenter"/> 
							<display:column property="REQUEST_DATE" title="Request Date"  class="aligncenter"/>
							<display:column property="QTY" title="Qty"  total = "true" class="alignright"/> 
															
							<display:footer media="html">
							
					<% 
							String strVal ;
							strVal = ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();					  		
					%>
								<tr><td colspan="8" class="Line"></td></tr>
				  				<tr class = "shade">
				  					<td colspan="2" ></td>
						  			<td colspan="2" class = "alignright"> <B> Total: </B></td>
						  			<td class = "alignright" ><B><%=strVal%></B></td>
						  			
				  				</tr>
				  				<tr><td colspan="8" class="Line"></td></tr>
	 					</display:footer>						
							</display:table>
    		             </td>
                    </tr> 
  <tr class=Line >
	<td height=25 class=RightDashBoardHeader colspan=11>Loaner Item Back Order </td>
	</tr>
	<tr><td colspan="6" class="ELine"></td></tr>
	                   <tr>
	                     <td COLSPAN="6">
	                     	<display:table  name="requestScope.frmTTPMonthlyForm.alTPRBoLoanerReport" class="its" id="currentRowObject" requestURI="/gmTPRReport.do" export="true" defaultsort="1"  varTotals="totals" decorator="com.globus.displaytag.beans.DTTPRWrapper"> 
							<display:column property="INHOUSE_TRANS_ID" title="Request ID"  sortable= "true" class="alignleft" style="width:90" />																					
							<display:column property="REFID" title="Ref ID"  class="alignleft"/>	
						   	<display:column property="QTY" title="Qty"  total = "true" class="alignright"/> 
															
							<display:footer media="html">
							
					<% 
							String strVal ;
							strVal = ((HashMap)pageContext.getAttribute("totals")).get("column3").toString();					  		
					%>
								<tr><td colspan="8" class="Line"></td></tr>
				  				<tr class = "shade">
				  					<td ></td>
						  			<td  class = "alignright"> <B> Total: </B></td>
						  			<td class = "alignright" ><B><%=strVal%></B></td>
						  			
				  				</tr>
				  				<tr><td colspan="6" class="Line"></td></tr>
	 					</display:footer>						
							</display:table>
    		             </td>
                    </tr>                     
<tr class=Line >
	<td height=25 class=RightDashBoardHeader colspan=11>Voided Requests and Consignments </td>
	</tr>
	<tr><td colspan="6" class="ELine"></td></tr>	 				 
                    <tr>
	                     <td colspan="6">
	                     	<display:table  name="requestScope.frmTTPMonthlyForm.alTPRVoidReport" class="its" id="currentRowObject" requestURI="/gmTPRReport.do" export="true" defaultsort="1" varTotals="totals" decorator="com.globus.displaytag.beans.DTTPRWrapper"> 
							<display:column property="REQID" title="Request ID"  sortable= "true" class="alignleft"/>
							<display:column property="CONS_ID" title="Consignment ID"   sortable= "true" class="alignleft"/>
							<display:column property="SHEET" title="Demand Sheet"  class="alignleft"/>
							<display:column property="SOURCE" title="Source"  class="alignleft"/>
							<display:column property="REQ_TO" title="Request To"  class="alignleft"/>							
							<display:column property="REQUEST_DATE" title="Request Date"  sortable= "true" class="aligncenter"/> 
							<display:column property="REQUIRED_DATE" title="Required Date"  sortable= "true" class="aligncenter"/>											 
							<display:column property="SFL" title="Status"  class="alignleft"/>
							<display:column property="QTY" title="Qty" total = "true" class="alignleft"/>
							<display:column property="UPDATED_BY" title="Updated By"  class="alignleft"/>
							<display:column property="UPDATED_DATE" title="Updated Date"  class="aligncenter"/>							
							<display:footer media="html">
					<% 
							String strVal ;
							strVal = ((HashMap)pageContext.getAttribute("totals")).get("column9").toString();					  		
					%>
								<tr><td colspan="11" class="Line"></td></tr>
				  				<tr class = "shade">
						  			<td colspan="8" class = "alignright"> <B> Total: </B></td>
						  			<td class = "alignright" ><B><%=strVal%></B></td>
						  			<td colspan="2" ></td>
				  				</tr>
				  				<tr><td colspan="11" class="Line"></td></tr>
	 					</display:footer>										
							</display:table>
    		             </td>
                    </tr>       
</table>
</html:form>