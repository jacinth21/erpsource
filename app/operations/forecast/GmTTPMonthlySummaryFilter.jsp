
<!-- \operations\forecast\GmTTPMonthlySummaryFilter.jsp -->
 <%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>


<% 
Object bean = pageContext.getAttribute("frmTTPMonthlySummary", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>

    <table>
    				<!-- Custom tag lib code modified for JBOSS migration changes -->
				<tr>	
         				<td class="RightTableCaption" align="right" HEIGHT="30" width="10%"></font>&nbsp;Order Qty:</td>
					   <td width="20%">
					    	&nbsp;
							<gmjsp:dropdown controlName="qtyOper" SFFormName="frmTTPMonthlySummary" SFSeletedValue="qtyOper"
								SFValue="alOperators" codeId = "CODENMALT"  codeName= "CODENM"  defaultValue= "[Choose One]" />
							&nbsp;
							 <html:text property="orderQty"  size="5" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>									
					   </td>	
					   <td class="RightTableCaption" align="right" HEIGHT="30"  ></font>Part Number :</td>					   
				      <td align="left" width="60%">&nbsp; 
				         <html:text property="pnum"  size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
				      </td>					      					
					</tr>
					<tr>
					<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;Order Cost:</td>
					   <td>
					    	&nbsp;
							<gmjsp:dropdown controlName="ordOper" SFFormName="frmTTPMonthlySummary" SFSeletedValue="ordOper"
								SFValue="alOperators" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						    &nbsp;
						    <html:text property="orderCost"  size="5" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 										
					   </td>						   					   
				      <td align="left" colspan = 2 class="RightTableCaption">
					    	<html:checkbox property="summaryTotal"/>Summary
							&nbsp;								
							<html:checkbox property="quarAvail"/> Quar Avail
							&nbsp;
							<html:checkbox property="quarAlloc"/> Quar Alloc
							&nbsp;
							<html:checkbox property="pdesc"/> Description
							&nbsp;<gmjsp:button value="Apply Filter" gmClass="button" onClick="fnFilter();" buttonType="Save" />
					   </td>
					   </tr>    										    	
				  </table>		 			  
				  