   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
   <%@ page import ="com.globus.common.servlets.GmServlet"%>
   <%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>    
<!-- \operations\forecast -->
<%@ include file="/common/GmHeader.inc" %>

<HEAD>
<script>

 function fnCallSetNumber(val)
{
	alert(val);
}


 function fnCallSetNumber(val, type)
{	
	alert(val);
}

</script>

</HEAD>

<bean:define id="demandmonthid" name="frmDemandSheetSummary" property="demandSheetMonthId" type="java.lang.String"></bean:define>
<bean:define id="strStatus" name="frmDemandSheetSummary" property="hstatus" type="java.lang.String"></bean:define>

    <%
    GmServlet gm = new GmServlet();
	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);

		
    try
    {	    
		GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();

		HashMap hmReportValue = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMVALUES"));		
		String strTypeId = GmCommonClass.parseNull((String)request.getParameter("hTypeId"));
		ArrayList alDrillDown = new ArrayList();		
		
		if (hmReportValue.size() == 0) 
		{
			return;
		}
		

		// To specify if its unit not to round by zero  
		gmCrossTab.setValueType(0);
		
		// Line Style information
		//gmCrossTab.setColumnDivider(false);
		gmCrossTab.setColumnLineStyle("line");
		
		gmCrossTab.setColumnOverRideFlag(true);
		gmCrossTab.addLine("Name");
		
		gmCrossTab.setGeneralHeaderStyle("ShadeDarkYellowTD");

		gmCrossTab.setTotalRequired(false);
		gmCrossTab.setColumnTotalRequired(false);
		gmCrossTab.setRowHighlightRequired(true);

		gmCrossTab.addStyle("Name","ShadeLightYellowTD","ShadeDarkYellowTD");
		
		gmCrossTab.setDivHeight(150);
		// Setting Display Parameter
		gmCrossTab.setAmountWidth(70);
		gmCrossTab.setNameWidth(400);
		gmCrossTab.setRowHeight(17);
		
		gmCrossTab.setLinkRequired(false); //
		alDrillDown.add("Group");
		//gmCrossTab.setDrillDownDetails(alDrillDown); // 
		
		gmCrossTab.setTableId("growthSummary");
		if (strStatus.equals("50550"))
		{
			gmCrossTab.setAllColDrillDown(true);
		}
		
		gmCrossTab.setColumnDrillDownScript("fnCallMnGrowth");
		
		gmCrossTab.setAttribute("DSID",demandmonthid);
		gmCrossTab.setDecorator("com.globus.crosstab.beans.GmDemandSheetGrowthSummaryDecorator");
		gmCrossTab.setDivHeight(120);
		out.println("<table border=1 cellspacing=0 cellpadding=0>");
		out.println("<tr><td class=Line  height=1></td></tr>");
        out.println("<tr><td>");
		out.println(gmCrossTab.PrintCrossTabReport(hmReportValue)) ;
		out.println("</td></tr>");
		out.println("<tr><td class=Line height=1></td></tr>");
		out.println("</table>");
		    
		
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	%>
