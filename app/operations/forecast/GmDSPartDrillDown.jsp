<%
/**********************************************************************************
 * File		 		: GmDSPartDrillDown.jsp
 * Desc		 		: Demand Sheet Part Drill Down
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<!-- \operations\forecast\GmDSPartDrillDown.jsp -->
<%@ include file="/common/GmHeader.inc" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
 
<% Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger 
try{ 

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Part Drill Down </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnPrintConsignVer(val)
{
    windowOpener("/GmConsignSetServlet?hAction=LoadConsign&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}


function fnPrintPack(val)
{
	windowOpener("/GmEditOrderServlet?hMode=PrintPack&hOrdId="+val,"Pack","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");	
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmDSSummary.do"  >
<html:hidden property="strOpt" />

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Part Drill Down</td>
		</tr>
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<td colspan="2">
					<display:table name="requestScope.frmDemandSheetSummary.ldtResult" class="its"
						id="currentRowObject" requestURI="/gmDSSummary.do"  decorator="com.globus.displaytag.beans.DTPartDrillDownWrapper" 
					 	style="height:35" >
							<display:column property="PNUM" title="Part Num" group="1" sortable="true" />
							<display:column property="PDESC" title="Part Description" group="2"style="width:320" />							
							<display:column property="ORDMON" title="Month" style="width:75" group="1" sortable="true" />
							<display:column property="DISTNAME" title="Name" sortable="true" style="width:200" />
							<display:column property="CSGID" title="Id" sortable="true" style="width:105" />
							<display:column property="QTY" title="Qty" sortable="true" class="alignright" format="{0,number,0}" headerClass="r" total="true" />							
							<display:column property="CSGDATE" title="Date" style="align:right"  />
						</display:table>  </td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
</BODY>

</HTML>

