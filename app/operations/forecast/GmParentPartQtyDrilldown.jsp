<%@ include file="/common/GmHeader.inc" %>
<!-- \operations\forecast\GmParentPartQtyDrilldown.jsp -->
<%
String strWikiTitle = GmCommonClass.getWikiTitle("PARENT_PART_QTY_DETAILS");
%>

<%@page import="java.util.HashMap"%>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<html:form action="/gmPPQtyDrillDownAction.do">
 
<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
<tr class=Line >
	<td height=25 class=RightDashBoardHeader COLSPAN="5">Parent Part Qty Details </td>
			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
	</tr>
			 <tr>
  <td COLSPAN="2" align="right" ><b>Part Number:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
  <td><b><bean:write name="frmDrillDown" property="partNumber" /></b></td>
  </tr>
  <tr>
  <td COLSPAN="2" align="right"><b>Description:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
  <td><b><bean:write name="frmDrillDown" property="partDescription" /></b></td>
  </tr>
  <tr>
  <td COLSPAN="2" align="right"><b>Parent Part Qty:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
  <td><b><bean:write name="frmDrillDown" property="parentPartQty" /></b></td>
  </tr>
  
	<tr><td colspan="8" class="Line"></td></tr>	 				 
                    <tr>
	                     <td COLSPAN="8">
	                     	<display:table  name="requestScope.frmDrillDown.alParentPartDetails" class="its" id="currentRowObject" requestURI="/gmPPQtyDrillDownAction.do" export="true" defaultsort="1"  varTotals="totals" decorator="com.globus.displaytag.beans.DTParentPartQtyDetailWrapper"> 
							<display:column property="PNUM" title="Part Number"  sortable= "true" class="alignleft" style="width:90" />
							<display:column property="DES" title="Description"   sortable= "true" class="alignleft"/>
							<display:column property="INSTOCK" title="Instock"  class="alignright"/>
							<display:column property="OPENDHR" title="DHR"  class="alignright"/>
							<display:column property="SUB_PART_QTY" title="Sub Part Qty"  class="alignright"/>									
							<display:column property="TOTAL_INSTOCK" title="Total Instock"  sortable= "true" class="alignright"/> 
							<display:column property="TOTAL_OPENDHR" title="Total DHR"  sortable= "true" class="alignright"/>											 
							<display:column property="TOTAL" title="Total"   total="true" class="alignright"/>							
							<display:footer media="html">
				
								<tr><td colspan="8" class="Line"></td></tr>
				  				<tr class = "shade">
				  				<% 
				  						String strVal="";
										strVal = ((HashMap)pageContext.getAttribute("totals")).get("column8").toString();
					%>
						  			<td colspan="7" class = "alignright"> <B> Total: </B></td>
						  			<td class = "alignright" ><B><%=strVal%></B></td>
				  				</tr>
				  				<tr><td colspan="8" class="Line"></td></tr>
	 					</display:footer>						
							</display:table>
    		             </td>
                    </tr> 
                  	 
</table>
</html:form>