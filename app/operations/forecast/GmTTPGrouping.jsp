<%
/**********************************************************************************
 * File		 		: GmTTPGrouping.jsp
 * Desc		 		: mapping TTP with the Demand Sheet
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
 <!-- \operations\forecast\GmTTPGrouping.jsp --> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="strttpId" name="frmTTPGroupMap"  property="ttpId" type="java.lang.String"></bean:define>
<bean:define id="strttpName" name="frmTTPGroupMap"  property="ttpName" type="java.lang.String"></bean:define>
<% 
/*Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger 
String strServletPath = GmCommonClass.getString("GMSERVLETPATH"); 

String strWikiPath = GmCommonClass.getString("GWIKI");*/
String strOperationsPurchasingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("TTP_DEMAND_SHEET_GROUPING");

try { %>

<HTML>
<HEAD>
<TITLE> Globus Medical: TTP - Demand Sheet Grouping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsPurchasingJsPath%>/GmTTPGrouping.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
var servletpath ="<%=strServletPath%>";
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnCheckSelections();"> 
<html:form action="/gmTTPGroupMap.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDTTP"/> 
<html:hidden property="hOld_ttpName" value="<%=strttpName%>"/>
 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"> TTP - Demand Sheet Grouping</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                     <!-- Custom tag lib code modified for JBOSS migration changes -->		
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;TTP Name List :</td> 
	                     <td colspan="3">							
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					        <jsp:param name="CONTROL_NAME" value="ttpId" />
					        <jsp:param name="METHOD_LOAD" value="loadTTPList" />
					        <jsp:param name="WIDTH" value="280" />
					        <jsp:param name="CSS_CLASS" value="search" />
					        <jsp:param name="TAB_INDEX" value=""/>
					        <jsp:param name="CONTROL_NM_VALUE" value="<%=strttpName %>" />
					        <jsp:param name="CONTROL_ID_VALUE" value="<%=strttpId %>" />
					        <jsp:param name="SHOW_DATA" value="50" />
					        <jsp:param name="AUTO_RELOAD" value="fnFetch ();" />				
				            </jsp:include>																
    		             </td>
                    </tr>  
                    <tr>
                        <td class="RightTextBlue" colspan="4" align="center"> Select from list to edit or enter details for a new TTP </td> 
                    </tr>
                    <tr><td colspan="4" class="Line"></td></tr>
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"></font>&nbsp;TTP Name:</td>
						<td width="70%" colspan="3">&nbsp;
							<html:text property="ttpName" maxlength="100" size="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
						</td>
					</tr>
                    <tr><td colspan="4" class="Line"></td></tr>
                    <tr>
                    	<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;Available Sheets :</td> 
                        <td width="350">&nbsp;
                       <DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                       
                        <table>
                        	<logic:iterate id="unSelectedDSlist" name="frmTTPGroupMap" property="alUnSelectedDS">
                        	<tr>
                        		<td>
							    	<htmlel:multibox property="checkUnSelectedDS" value="${unSelectedDSlist.ID}" />
								    <bean:write name="unSelectedDSlist" property="NAME" />
								</td>
							</tr>	    
							</logic:iterate>
						</table>
						</DIV><BR>
                        </td>
                        <td class="RightTableCaption" align="right" HEIGHT="30" width="120" ></font>&nbsp;Associated Sheets :</td> 
                         <td width="350">&nbsp;
                         <DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                         <table>
                        	<logic:iterate id="selectedDSlist" name="frmTTPGroupMap" property="alSelectedDS">
                        	<tr>
                        		<td>
								    <htmlel:multibox property="checkSelectedDS" value="${selectedDSlist.ID}" />
								    <bean:write name="selectedDSlist" property="NAME" />
	     						</td>
							</tr>	    
							</logic:iterate>
							</table>
							</DIV><BR>
                        </td>
                    </tr>  
                    <tr><td colspan="4" class="Line"></td></tr>
                    <tr>
    	                <td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;TTP Owner :</td> 
	                    <td colspan="3" class="RightTextBlue" >&nbsp;
        		             <gmjsp:dropdown controlName="ttpOwner" SFFormName="frmTTPGroupMap" SFSeletedValue="ttpOwner"
							SFValue="alTTPOwner" codeId="ID"  codeName="NAME" defaultValue="[Choose One]" />	
							&nbsp;	Owner's name appears on the PO 											
  		            	</td>
                    </tr>  
                    <tr><td colspan="4" class="Line"></td></tr>
                    <tr>
                    	<td colspan="4" align="center" height="30">
							<gmjsp:button value="&nbsp;Void&nbsp;" name="Btn_Void" gmClass="button" onClick="fnVoid();" buttonType="Save" />
		                    <gmjsp:button value="Submit" gmClass="button" onClick="fnSubmit();" buttonType="Save" /> 
		                    <gmjsp:button value="Reset" gmClass="button" onClick="fnReset();" buttonType="Save" /> 
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
<% 
} 
catch (Exception e) {
e.printStackTrace();
}
%>
</BODY>

</HTML>

