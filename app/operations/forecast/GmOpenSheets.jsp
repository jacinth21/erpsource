<%
/**********************************************************************************
 * File		 		: GmOpenSheets.jsp
 * Desc		 		: This screen is used for displaying the Open Sheets 
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="demandSheetMonthId" name="frmDemandSheetAjax" property="demandSheetMonthId" type="java.lang.String"></bean:define>
<!-- \operations\forecast\GmOpenSheets.jsp-->

<BODY>
<input type="hidden" name="demandsheetmonthId" value="<%=demandSheetMonthId%>">
       <table class="border"><tr><td>
		    <display:table name="requestScope.frmDemandSheetAjax.alResult" class = "its"  id="currentRowObject"  decorator="com.globus.displaytag.beans.DTOpenSheetsWrapper">
		        <display:column property="ID" title=" " style="width:50" />		     
				<display:column property="NAME" title=" Sheet Name" />		
				<display:column property="STATUS" title="Status" class="alignleftt"  sortable="true" />					
 			</display:table> 
 			</td> 			
 		</tr> 		
 		<logic:equal name="frmDemandSheetAjax" property="statusId" value="50550">
 		<tr><td class="Line" width="1"></td><tr> 
 		<tr> 		
 		<td align="center" height ="30">  			
 		  		<gmjsp:button name="pull" value="&nbsp;&nbsp;Pull Sheets&nbsp;&nbsp;" gmClass="button" onClick="fnPullOpenSheets(this.form);" buttonType="Save" /> 		    
 		  </td> 
 		</tr> 		
 		</logic:equal>
 	 </table>
</BODY>
