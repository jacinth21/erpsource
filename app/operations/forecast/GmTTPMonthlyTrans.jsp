<%
/**********************************************************************************
 * File		 		: GmTTPMonthlyTrans.jsp
 * Desc		 		: Groups the TTP with Demand Sheet for a specific month
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<!-- \operations\forecast\GmTTPMonthlyTrans.jsp -->
<%@ include file="/common/GmHeader.inc" %>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="strttpId" name="frmTTPMonthly"  property="ttpId" type="java.lang.String"></bean:define>
<bean:define id="strttpName" name="frmTTPMonthly"  property="searchttpId" type="java.lang.String"></bean:define>
<% 
/*Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger 
String strServletPath = GmCommonClass.getString("GMSERVLETPATH"); 

String strWikiPath = GmCommonClass.getString("GWIKI");*/
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("TTP_DEMANDSHEET_MAPPING");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmTTPMonthlyTrans.js"></script>


<script>
function fnVoid()
{
		document.frmTTPMonthly.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmTTPMonthly.hAction.value = "Load";
		document.frmTTPMonthly.hTxnId.value = document.frmTTPMonthly.refId.value
		document.frmTTPMonthly.submit();
}		
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmTTPMonthlyTrans.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="ttpName"/>
 
	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">TTP - Demand Sheet Mapping</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="100%" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="20%">&nbsp;TTP Name:</td>
						<td class="RightTableCaption"  HEIGHT="30" style="padding-left: 7px;">	
	                     <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					     <jsp:param name="CONTROL_NAME" value="ttpId" />
					     <jsp:param name="METHOD_LOAD" value="loadTTPList&searchType=PrefixSearch" />
					     <jsp:param name="WIDTH" value="250" />
					     <jsp:param name="CSS_CLASS" value="search" />
					     <jsp:param name="TAB_INDEX" value=""/>
					     <jsp:param name="CONTROL_NM_VALUE" value="<%=strttpName %>" />
					     <jsp:param name="CONTROL_ID_VALUE" value="<%=strttpId %>" />
					     <jsp:param name="SHOW_DATA" value="50" />			
				         </jsp:include>				
						</td>
                        <td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;Month:&nbsp;<gmjsp:dropdown controlName="monthId" SFFormName="frmTTPMonthly" SFSeletedValue="monthId"
							SFValue="alMonth" codeId = "CODENMALT"  codeName = "CODENM" defaultValue= "[Choose One]" />													
							&nbsp;Year:&nbsp;<gmjsp:dropdown controlName="yearId" SFFormName="frmTTPMonthly" SFSeletedValue="yearId"
							SFValue="alYear" codeId = "CODENM"  codeName = "CODENM"  defaultValue= "[Choose One]" />
							&nbsp;<gmjsp:button value="&nbsp;&nbsp;&nbsp;Go&nbsp;&nbsp;" gmClass="button" onClick="fnFetch();" buttonType="Load" /> &nbsp;
    		             </td>
                    </tr>  
                    <tr><td colspan="3" class="Line"></td></tr>
                     
                    <tr>
                    	<td class="RightTableCaption" align="right" HEIGHT="30" >&nbsp;Linked Sheets :</td> 
                         <td colspan="2">&nbsp;
                         

							
                          <DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                                                   <table >
	                        	<tr bgcolor="gainsboro">
	                        		<td> 
									    <html:checkbox property="selectAll" onclick="fnSelectAll('toggle');" /><B>Select All </B> 
		     						</td>
								</tr>	    
							</table>
                         <table>
                        	<logic:iterate id="selectedDS" name="frmTTPMonthly" property="alSelectedSheet">
                        	<tr>
                        		<td>
								    <htmlel:multibox property="checkSelectedSheet"  onclick="fnMasterCheckbox();" value="${selectedDS.ID}"   alt="${selectedDS.STATUSID}"/>
								    <bean:write name="selectedDS" property="NAME" />
	     						</td>
							</tr>	    
							</logic:iterate>
							</table>
							</DIV>
                        </td>
                      </tr>
                      <tr>
                      <td class="RightTableCaption" align="right" HEIGHT="30" >&nbsp;Other Sheets :</td> 
                        <td colspan="2">&nbsp;
                        <DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                        <table>
                        	<logic:iterate id="unSelectedDS" name="frmTTPMonthly" property="alUnSelectedSheet">
                        	<tr>
                        		<td>
							    	<htmlel:multibox property="checkUnSelectedSheet" value="${unSelectedDS.ID}" />
								    <bean:write name="unSelectedDS" property="NAME" />
								</td>
							</tr>	    
							</logic:iterate>
						</table>
						</DIV>
                        </td>
                    </tr>  
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;Inventory Id :</td> 
	                     <td colspan="2">&nbsp;
	                     <gmjsp:dropdown controlName="inventoryId" SFFormName="frmTTPMonthly" SFSeletedValue="inventoryId"
							SFValue="alInventoryIdList" codeId = "ID"  codeName = "NAME" />													
    		             </td>
                    </tr>  
                    <tr><td colspan="3" class="ELine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"><font color="red">*</font>&nbsp;Forecast Months :</td> 
	                     <td colspan="2">&nbsp;
	                     <html:text property="ttpForecastMonths"  size="4"  maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 													
    		             </td>
                    </tr>  
                            <tr><td colspan="3" class="Line"></td></tr>
                    <tr>
                    	<td colspan="3" align="center" height="30">
	                    	<gmjsp:button value="View Summary" name="ViewSummary" gmClass="button" onClick="fnViewSummary();" buttonType="Load" />
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

