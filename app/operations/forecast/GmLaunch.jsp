<%
/**********************************************************************************
 * File		 		: GmLaunch.jsp
 * Desc		 		: To save the launch date and move the launch date
 * Version	 		: 1.0
 * author			: Brinal G
************************************************************************************/
%>
<!-- \operations\forecast\GmLaunch.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="org.apache.commons.beanutils.DynaBean"%> 
<bean:define id="ldtResult" name="frmDemandSheetSetup" property="ldtResult" type="java.util.List"></bean:define>
<%  
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT"); 
String strWikiTitle = GmCommonClass.getWikiTitle("GM_LAUNCH");
ArrayList alList = new ArrayList();
String strSessTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
alList = GmCommonClass.parseNullArrayList((ArrayList)ldtResult);
 
int intRows = 0;
intRows = alList.size();

StringBuffer sbInput = new StringBuffer();
String strSetID = "";
String strInput = "";
 
DynaBean db ;

	for (int i=0;i<intRows;i++)
	{
		db = (DynaBean)alList.get(i);
		strSetID = String.valueOf(db.get("ID"));
		sbInput.append(strSetID);
		sbInput.append("^");
	}
	 strInput = sbInput.toString();
	// System.out.println("string" + strInput);
	
//String strpop = (String)request.getAttribute("PopType");
 

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/gmLaunch.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script> 
 var size="<%=intRows%>";
 var varInput = "<%=strInput%>";
 var todayDate = "<%=strSessTodaysDate%>";
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
<html:form action="/gmLaunch.do"  >
<html:hidden property="strOpt" />
<html:hidden property="hinputStr" />
<html:hidden property="demandSheetId" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp; Product Launch Setup</td>
			<td align="right" class=RightDashBoardHeader  colspan="3"> 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="25"  colspan="2" width="300"> </td>			 
			<td align="left" >&nbsp;
				<gmjsp:dropdown controlName="launchdate"  SFFormName='frmDemandSheetSetup' defaultValue= "[Choose One]"	
						SFValue="alDates" codeId="MONTH" codeName="MMNAME" onChange="fnApplyAllLaunchDTs(this.selectedIndex);" />
			</td>
		<td height="25" > &nbsp;&nbsp;
				<gmjsp:dropdown controlName="newlaunchdate"  SFFormName='frmDemandSheetSetup' defaultValue= "[Choose One]"	
		SFValue="alDates" codeId="MONTH" codeName="MMNAME" onChange="fnApplyAllNewDTs(this.selectedIndex);" />
			</td>		 
		</tr>
		

		
		<tr><td colspan="4" height="1" class="line"></td></tr>
		
		 <tr>
             <td colspan="4">
	               	<display:table  name="requestScope.frmDemandSheetSetup.ldtResult" class="its" id="currentRowObject" requestURI="/gmLaunch.do" decorator="com.globus.operations.forecast.displaytag.beans.DTLaunchWrapper"   > 
							<display:column property="ID" title="ID"    class="alignleft"  style="width:70" />
							<display:column property="NAME" title="Name"   class="alignleft" style="width:300"/>							 
							<display:column property="LAUNCHDT" title="Launch Date"   class="alignleft"/>
							<display:column property="NEWDT" title="New Launch Date"   class="alignleft"/>							 
					</display:table>
    	     </td>
                    </tr> 
         <tr><td colspan="4" class="ELine"></td></tr> 
                    <tr>
                    	<td colspan="4" align="center">
		                     <gmjsp:button value="Submit" gmClass="button" onClick="fnSubmit();" buttonType="Save" />&nbsp;&nbsp;
		                     <gmjsp:button value="Close" gmClass="button" onClick="fnClose();" buttonType="Load" />  
		                </td>
                    </tr>
		 
    </table>		     	
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

