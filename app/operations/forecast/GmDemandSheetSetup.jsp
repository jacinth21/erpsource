<%
/**********************************************************************************
 * File		 		: GmDemandSheetSetup.jsp
 * Desc		 		: Setting up Demand Sheet
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- \operations\forecast\GmDemandSheetSetup.jsp -->

<bean:define id="strDemandSheetType" name="frmDemandSheetSetup"  property="demandSheetType" type="java.lang.String"></bean:define>
<bean:define id="strDemandSheetId" name="frmDemandSheetSetup"  property="demandSheetId" type="java.lang.String"></bean:define>
<bean:define id="strDemandSheetName" name="frmDemandSheetSetup"  property="demandSheetName" type="java.lang.String"></bean:define>

<% 
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_SETUP");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmDemandSheetSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>


</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/demandSheetSetup.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="demandSheetType" />
<html:hidden property="demandSheetTypeName" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDDMS"/>
<html:hidden property="hDemandSheetName" value="<%=strDemandSheetName %>"/>

 <!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Demand Sheet Setup</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="Shade">
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"></font>&nbsp;Demand
							Sheet Name :</td>
						<td width="70%" style="padding-left: 5px;">
						<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="demandSheetId" />
					<jsp:param name="METHOD_LOAD" value="loadDemandSheetList&searchType=PrefixSearch" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="1"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strDemandSheetName %>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strDemandSheetId %>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="fnFetch ();" />
					
				</jsp:include>				
						
						
																			
						</td>
					</tr>
					<tr>
                        <td class="RightTextBlue" colspan="2" align="center"> Select from list to edit or enter details for a new DemandSheet </td> 
                    </tr>
                    <tr><td colspan="2" class="Line"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;<font color="red">*</font> Demand Sheet Name :</td> 
	                     <td style="padding-left: 9px; width:90%;">
    		               <html:text property="demandSheetName"  size="70" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" style="width:75%;"/>
    		             </td>
                    </tr>  
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;Demand Sheet Type:</td> 
                        <td>&nbsp; 
                        	<bean:write name="frmDemandSheetSetup" property="demandSheetTypeName" />
                        </td>
                    </tr>  
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<font color="red">*</font> Demand Period:</td> 
                        <td>&nbsp;
    		               <html:text property="demandPeriod"  size="3" maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> &nbsp;(months)                                                                 
    		             </td>
                    </tr>   
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<font color="red">*</font> Forecast Period:</td> 
                        <td>&nbsp;
    		               <html:text property="forecastPeriod"  size="3" maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>  &nbsp;(months)                                                                 
    		             </td>
                    </tr>   
                    
                     <tr><td colspan="2" class="lline"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;Set Build Forecast:</td> 
                        <td>&nbsp;
                        	<bean:write name="frmDemandSheetSetup" property="requestPeriod"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(months)
                        </select>
                        </td>
                    </tr>  
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr class="Shade">
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"></font>&nbsp;Team :</td>
						<td width="70%">&nbsp;
						<gmjsp:dropdown controlName="hierarchyId" SFFormName="frmDemandSheetSetup" SFSeletedValue="hierarchyId"
							SFValue="alHierarchyList" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]" />													
						</td>
					</tr>
                    <tr><td colspan="2" class="lline"></td></tr>
                     <tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"></font>&nbsp;Sheet Owner :</td>
						<td width="70%">&nbsp;
						<gmjsp:dropdown controlName="demandSheetOwner" SFFormName="frmDemandSheetSetup" SFSeletedValue="demandSheetOwner"
							SFValue="alDSOwner" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]" />													
						</td>
					</tr>
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Job Setup:</td> 
                        <td>&nbsp;
                       <%if (strDemandSheetType.equals("40023")){ %>
                        	 <gmjsp:button value="Job Setup" gmClass="button" disabled="true" buttonType="Save" /> 
                        	 <% } else {%>
                        	 <gmjsp:button value="Job Setup" gmClass="button" onClick="fnJobSetup();" buttonType="Save" /> 
                        	 <% } %>
    		             </td>
                    </tr>   
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Include
                        	Current Month:</td> 
                        <td>&nbsp;<html:checkbox property="includeCurrMonth" disabled="true" />
    		             </td>
                    </tr>   
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Demand
                        	Sheet Mapping:</td> 
                        <td>&nbsp;
    		            <% if (strDemandSheetType.equals("40023")){ %>    
                        	 <gmjsp:button value="Mapping" gmClass="button" disabled="true" onClick="fnMapping();" buttonType="Save" /> 
                        <%} else {%> 
                         <gmjsp:button value="Mapping" gmClass="button" onClick="fnMapping();" buttonType="Save" /> 
                        <%} %>	 
    		               &nbsp;
    		             <% if (strDemandSheetType.equals("40020")){ %>   
    		               <gmjsp:button value="Set Action" gmClass="button" disabled="true" onClick="fnSetMapping();" buttonType="Save" />   
    		              <%} else {%>  
    		               <gmjsp:button value="Set Action" gmClass="button" onClick="fnSetMapping();" buttonType="Save" /> 
    		              <%} %>
    		              &nbsp;
    		              <gmjsp:button value="Launch Setup" gmClass="button" onClick="fnOpenLaunch();" buttonType="Save" />    		              
    		             </td>	             
                    </tr>   
                    <tr><td colspan="2" class="lline"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">InActive Flag:</td> 
                        <td>&nbsp;<html:checkbox property="activeflag" />
                         </td>
                    </tr>
                    	<tr>
						<td colspan="2" align="center" height="24">&nbsp;
	                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmDemandSheetSetup" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>	
						</td>
						</tr>
					 <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
                    	<td colspan="2" align="center" height="24">
		                    <gmjsp:button value="&nbsp;Reset&nbsp;" gmClass="button" onClick="fnReset();" buttonType="Save" /> 
		                    <gmjsp:button value="Submit" gmClass="button" onClick="fnSubmit();" buttonType="Save" /> 
		                    
		                 <% if (strDemandSheetType.equals("40023")){ %>  
		                    <gmjsp:button value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" gmClass="button" disabled="true" onClick="fnVoid();" buttonType="Save" />
		                  <%} else {%> 
                         <gmjsp:button value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" gmClass="button"  onClick="fnVoid();" buttonType="Save" />
                        <%} %>	 
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>

<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

