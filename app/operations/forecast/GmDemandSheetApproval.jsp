
<%@ include file="/common/GmHeader.inc" %>
<!-- \sales\AreaSet\GmAddInfo.jsp -->

<table border="0" cellspacing="0" cellpadding="0">
<%
  String strMessage = GmCommonClass.parseNull((String)request.getAttribute("ErrorMessage"));
  if(!strMessage.equals(""))
  {
%>
  <tr><td class="Line" width="1"></td></tr>  
  <tr><td height="120" valign="middle" align="center"><b><font  style="color: red;size: 15;"><%=strMessage%></font></b></td></tr>
    <tr><td class="Line" width="1"></td></tr>  
 <%} else { %> 
<tr><td class="Line" width="1"></td><td> 
<table border="0" cellspacing="0" cellpadding="0">
<logic:equal name="frmDemandSheetSummary" property="statusId" value="50550">
	<tr><td colspan="7" class="Line" height="1"></td><tr>
	<tr >
		<td align="left" colspan="6" width="80%">
			<textarea name="appcomments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');"  	
			onBlur="changeBgColor(this,'#ffffff');" style="height:150px; width: 80%;"  value=""></textarea>
		</td>
		<td valign="middle">
		<gmjsp:button value="&nbsp;&nbsp;Approve&nbsp;&nbsp;&nbsp;" gmClass="button" disabled="true" onClick="fnApprove(this.form);" buttonType="Save" />
		</td>
	</tr>
	<tr><td colspan="7" class="Line" height="1"></td><tr>
</logic:equal> 
<logic:notEqual name="frmDemandSheetSummary" property="statusId" value="50550">
	<tr><td colspan="7" class="Line" height="1"></td><tr> 
	<tr >
		<td class="RightBlueCaption"  colspan="7" align="center" height="20">Approver Comments</td>
	</tr>
	<tr><td colspan="7" class="Line" width="1"></td><tr> 
	<tr >
		<td colspan="7" align="left" height="60"><bean:write name="frmDemandSheetSummary" property="appcomments"/></td>
	</tr>
	<tr><td colspan="7" class="Line" width="1"></td><tr> 
	<tr height=21>	
	<td class="RightBlueCaption"  align="left" >&nbsp;Approved By</td>
	<td class="Line" width="1"></td>
	<td class="RightTableCaption" align="left" >&nbsp;<bean:write name="frmDemandSheetSummary" property="approvedby"/></td>
	<td class="Line" width="1"></td>
	<td class="RightBlueCaption" align="left" >&nbsp;Approved Date</td>
	<td class="Line" width="1"></td>
	<td class="RightTableCaption"  align="left" >&nbsp;<bean:write name="frmDemandSheetSummary" property="approveddate"/></td>	  
	</tr>
	<tr><td colspan="7" class="Line" width="1"></td><tr> 
</logic:notEqual>

</table></td><td class="Line" width="1"></td></tr>
<% } %>
</table>
<%@ include file="/common/GmFooter.inc" %>
