   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
   <%@ page import ="com.globus.common.servlets.GmServlet"%>
   <%@ page import ="com.globus.common.beans.GmCrossTabFormat"%> 
   <!-- WEB-INF path corrected for JBOSS migration changes -->   
  <!-- \operations\forecast\GmSubPartQtyDetails.jsp --> 
  <%@ include file="/common/GmHeader.inc" %>
  
   <script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
	 
    <bean:define id="hmReportSub" name="frmDemandSheetSummary"  property="hmSubPartDetail" type="java.util.HashMap"></bean:define>
     <bean:define id="hmReportParent" name="frmDemandSheetSummary"  property="hmParentPartDetail" type="java.util.HashMap"></bean:define>
     <bean:define id="hmReportValue" name="frmDemandSheetSummary"  property="hmDemandSheetDetail" type="java.util.HashMap"></bean:define>
    <%
    GmServlet gm = new GmServlet();
	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);
	
 	
    try
    {	    
		GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
		ArrayList alForecastHeader = new ArrayList();
		ArrayList alDrillDown = new ArrayList();		
		HashMap hmapValue = new HashMap();
		String strFooterStyle = "";  	
		String strStatus = "";
		int r = 0;			
 
		if (hmReportValue.size() == 0) 
		{
			return;
		} 
		// Line Style information
		gmCrossTab.setColumnDivider(false);
		gmCrossTab.setColumnLineStyle("borderDark");
		 
		alForecastHeader = (ArrayList) hmReportValue.get("FORECASTHEAD");
		 
		int Headersize = alForecastHeader.size();
		
		//gmCrossTab.setSelectedColDrillDown(true);
		for (int i = 0; i < Headersize; i++)
        {
            hmapValue = (HashMap) alForecastHeader.get(i);
            r = i%2;
            if (r == 0) 
            {
            	strFooterStyle = "ShadeMedBrownTD";
            }
            else
            {
            	strFooterStyle = "ShadeLightBrownTD";
            }
			gmCrossTab.addStyle((String)hmapValue.get("PERIOD"),strFooterStyle,"ShadeDarkBrownTD") ;
		 
        }
		gmCrossTab.addStyle("Name","ShadeLightBlueTD","ShadeDarkBlueTD");
	 
		gmCrossTab.addStyle("PBO","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("PBL","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("PCS","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("Total","ShadeLightBlueTD","ShadeDarkBlueTD");
		gmCrossTab.setGeneralHeaderStyle("aaTopHeader");		 
		gmCrossTab.addLine("Name");
	 	
		gmCrossTab.addLine("PCS");
		
		gmCrossTab.setRoundOffAllColumnsPrecision("0");
		 
		gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
		gmCrossTab.setTotalRequired(false);
 		gmCrossTab.setColumnTotalRequired(false);
 		gmCrossTab.setDivHeight(150);
//		gmCrossTab.setGroupingRequired(true);
		gmCrossTab.setNoDivRequired(true);
 		gmCrossTab.setRowHighlightRequired(true);
		gmCrossTab.setLinkRequired(false);		
		gmCrossTab.setDecorator("com.globus.crosstab.beans.GmPartQtyDecorator"); 
	  //gmCrossTab.setDrillDownDetails(alDrillDown); 
	//	gmCrossTab.setAttribute("DMID",demandsheetid);
	//	gmCrossTab.setAttribute("DSID",demandmonthid);
  
		out.println(gmCrossTab.PrintCrossTabReport(hmReportSub)) ;

    }catch(Exception e)
	{
		e.printStackTrace();
	}
	%>