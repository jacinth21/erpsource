<%
/**********************************************************************************
 * File		 		: GmDemandSheetParHistory.jsp
 * Desc		 		: This screen is used to display Part Detail and history
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import="org.apache.commons.beanutils.BeanMap, org.apache.commons.beanutils.RowSetDynaClass, org.apache.commons.beanutils.BasicDynaBean,org.apache.commons.beanutils.DynaBean"%>
<!-- \operations\forecast\GmDemandSheetParHistory.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%
try {
		 	 
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strPart ="";
	String strPDesc ="";
	String strDemandSheet ="";
	 
	
	if(hmReturn !=null)
	 {
		strDemandSheet = GmCommonClass.parseNull((String)hmReturn.get("DEMANDSHEETNAME"));
		strPart = GmCommonClass.parseNull((String)hmReturn.get("PNUM"));
		strPDesc = GmCommonClass.parseNull((String)hmReturn.get("PDESC"));
		 
	 }
 	ArrayList alList = (ArrayList)request.getAttribute("ALCOMMENTS");
 	HashMap hmValue = new HashMap();
	
 	DynaBean db ;
 	ArrayList alValueList = (ArrayList)request.getAttribute("ALDEMANDPAR");
 	 
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Detail</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
 
</HEAD>

<BODY leftmargin="20" topmargin="10" >
 
<TABLE cellSpacing=0 cellPadding=0  border=0 width="500" class="its">
<TBODY>
	<tr>
		<td rowspan="16" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1  ></td>
		<td rowspan="16" class=Line noWrap width=1></td>
	</tr>
	<tr class=Line >
	<td height=25 class=RightDashBoardHeader >Part Revision History Details </td>
			
			 
	</tr>
	<tr class=Line><td noWrap height=1 ></td></tr>
					<tr >
						<td ><table ><tr >
							<td class="RightTableCaption"  width=150 HEIGHT="24" >Demand Sheet: </td>
							 
							<td>&nbsp; <%=strDemandSheet%>
							</td>
						</tr></table></td>
					</tr>
				 <tr><td   class="LLine"></td></tr>		
				 <tr class="even">
						<td ><table><tr>
							<td class="RightTableCaption" width=150 HEIGHT="24" >Part #:</td>
							 
							<td>&nbsp; <%=strPart%>
							</td>
						</tr></table></td>
					</tr>
					
				 <tr><td   class="LLine"></td></tr>		
				 <tr>
						<td ><table><tr>
							<td class="RightTableCaption" width=150 HEIGHT="24">Part Desc: </td>
							 
							<td>&nbsp; <%=strPDesc%>
							</td>
						</tr></table></td>
					</tr>
									 
			<tr>
			  <td></td>
	</tr>			
 	 
	 
	<tr class=Line><td noWrap height=1 colspan=2></td></tr>
	
	  <tr> 
            <td colspan="2">
            
            <display:table name="ALDEMANDPAR" requestURI="/gmDSParSetup.do"  class="its" id="currentRowObject" > 
			 
			 <display:column property="PVALUE" title="Value"   class="aligncenter" style="width:100px" /> 
			<display:column property="UBY" title="By"   class="aligncenter"/> 
			<display:column property="UDATE" title="Date Time"  class="aligncenter"/> 
			 </display:table> 
		 	
		</td>
    </tr> 
    
     <tr><td   class="Line"></td></tr>	
</table>	
 
 
 
<TABLE cellSpacing=0 cellPadding=0  border=0  >
<TBODY>
<tr>
<td>&nbsp;</td>
</tr>
</TABLE>

<TABLE cellSpacing=0 cellPadding=0  border=0 width="500" class="its">
<TBODY>
	<tr>
		<td rowspan="16" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1  ></td>
		<td rowspan="16" class=Line noWrap width=1></td>
	</tr>
	<tr class=Line >
	<td height=25 class=RightDashBoardHeader >Comments </td>
			
			 
	</tr>
	
	
	<%
	
	int rows = alList.size();
	for( int i= 0; i< rows; i++)
	{		
			
			hmValue = (HashMap) alList.get(i);
				 
	
	%>		
					<tr >
						<td ><table ><tr >
							<td class="RightTableCaption"  width=150 HEIGHT="24" ><%=(String)hmValue.get("UNAME") %> </td>
							 
							<td >&nbsp; <%=(String)hmValue.get("DT") %>
							</td>
							<td  > <%=(String)hmValue.get("COMMENTS") %>
							</td>
						</tr></table></td>
					</tr>
				 <tr><td   class="LLine"></td></tr>		
		<% } %>		  
					
				 
		<tr class=Line><td noWrap height=1 ></td></tr>							 
		 
 </table>
 	 
</BODY>

</HTML>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>