/* Formatted on 2009/07/21 18:39 (Formatter Plus v4.8.0) */
-- Create Statements

CREATE TABLE t9700_rule ( c9700_rule_id NUMBER NOT NULL
						, c9700_rule_name VARCHAR2(4000) NULL
						, c9700_expiry_date DATE NULL
						, c9700_initiated_by VARCHAR2(10) NULL
						, c9700_initiated_date DATE NULL
						, c9700_active_fl VARCHAR2(1) NULL
						, c9700_created_by VARCHAR2(10) NULL
						, c9700_created_date DATE NULL
						, c9700_last_updated_by VARCHAR2(10) NULL
						, c9700_last_updated_date DATE NULL
						, c9700_void_fl VARCHAR2(1) NULL
						, c9700_rule_level NUMBER NULL
						, c9700_comment VARCHAR2(4000) NULL
						, c9700_email_notify_fl VARCHAR2(1) NULL
						, c9700_expiry_history_fl VARCHAR2(1) NULL
						, c9700_active_history_fl VARCHAR2(1) NULL
						, c9700_email_notify_history_fl VARCHAR2(1) NULL
						, c9700_condition_history_fl VARCHAR2(1) NULL
						, c9700_txn_history_fl VARCHAR2(1) NULL
						, c9700_hold_txn_history_fl VARCHAR2(1) NULL );

CREATE TABLE t9701_condition ( c9701_condition_id NUMBER NOT NULL
							, c9701_condition_name VARCHAR2(100) NULL
							, c9701_void_fl VARCHAR2(1) NULL
							, c9701_active_fl VARCHAR2(1) NULL
							, c9701_condition_level NUMBER NULL );

CREATE TABLE t9702_rule_condition ( c9702_rule_condition_id NUMBER NOT NULL
								  , c9702_condition_value VARCHAR2(4000) NULL
								  , c9702_void_fl VARCHAR2(1) NULL
								  , c9702_created_by VARCHAR2(10) NULL
								  , c9702_created_date DATE NULL
								  , c9702_last_updated_by VARCHAR2(10) NULL
								  , c9702_last_updated_date DATE NULL
								  , c9700_rule_id NUMBER NULL
								  , c9701_condition_id NUMBER NULL
								  , c901_operator_id NUMBER NULL
								  , c901_condition_type NUMBER NULL );

CREATE TABLE t9703_rule_transaction ( c9703_rule_transaction_id NUMBER NOT NULL
									, c9703_void_fl VARCHAR2(1) NULL
									, c9703_created_by VARCHAR2(10) NULL
									, c9703_created_date DATE NULL
									, c9700_rule_id NUMBER NULL
									, c901_txn_id NUMBER NULL );

CREATE TABLE t9704_rule_consequence ( c9704_rule_consequence_id NUMBER NOT NULL
									, c9704_void_fl VARCHAR2(1) NULL
									, c9704_created_by VARCHAR2(10) NULL
									, c9704_created_date DATE NULL
									, c9704_last_updated_by VARCHAR2(10) NULL
									, c9704_last_updated_date DATE NULL
									, c9700_rule_id NUMBER NULL
									, c901_consequence_id NUMBER NULL );

CREATE TABLE t9705_consequence_details ( c9705_consequence_details_id NUMBER NOT NULL
				   , c9705_consequence_value VARCHAR2(4000) NULL
				   , c9705_void_fl VARCHAR2(1) NULL
				   , c9705_created_by VARCHAR2(20) NULL
				   , c9705_created_date DATE NULL
				   , c9704_rule_consequence_id NUMBER NULL
				   , c901_param_id NUMBER NULL );

CREATE TABLE t9001_filter_lookup ( c9001_filter_id NUMBER NOT NULL
								 , c9001_ref_id VARCHAR2(100) NULL
								 , c9001_filter_grp VARCHAR2(100) NULL
								 , c9001_table_alias_name VARCHAR2(100) NULL
								 , c9001_column_name VARCHAR2(100) NULL
								 , c9001_procedure_name VARCHAR2(100) NULL
								 , c9001_active_fl VARCHAR2(1) NULL
								 , c901_parameter_type_id NUMBER NULL
								 , c9001_proc_in_param VARCHAR2(4000 BYTE) NULL);

CREATE TABLE t9002_filter_operator ( c9002_filter_operator_id NUMBER NOT NULL
								   , c9002_active_fl VARCHAR2(1) NULL
								   , c9001_filter_id NUMBER NULL
								   , c901_operator_id NUMBER NULL);

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

-- Alter & Index Statements
-- t9700

ALTER TABLE t9700_rule ADD ( PRIMARY KEY (c9700_rule_id));

ALTER TABLE t9702_rule_condition ADD (
  FOREIGN KEY (c9700_rule_id)
	REFERENCES t9700_rule (c9700_rule_id));

ALTER TABLE t9703_rule_transaction ADD (
  FOREIGN KEY (c9700_rule_id)
	REFERENCES t9700_rule (c9700_rule_id));

ALTER TABLE t9704_rule_consequence ADD (
  FOREIGN KEY (c9700_rule_id)
	REFERENCES t9700_rule (c9700_rule_id));

-- t9701

ALTER TABLE t9701_condition ADD ( PRIMARY KEY (c9701_condition_id));

-- T9702

CREATE INDEX xif2t9702_rule_condition ON t9702_rule_condition (c9700_rule_id);

CREATE INDEX xif3t9702_rule_condition ON t9702_rule_condition (c9701_condition_id);

CREATE INDEX xif4t9702_rule_condition ON t9702_rule_condition (c901_operator_id);

ALTER TABLE t9702_rule_condition ADD ( PRIMARY KEY (c9702_rule_condition_id));

ALTER TABLE t9702_rule_condition ADD ( FOREIGN KEY (c901_operator_id) REFERENCES t901_code_lookup (c901_code_id));

ALTER TABLE t9702_rule_condition ADD ( FOREIGN KEY (c9701_condition_id) REFERENCES t9701_condition (c9701_condition_id));

ALTER TABLE t9702_rule_condition ADD ( FOREIGN KEY (c901_condition_type) REFERENCES t901_code_lookup (c901_code_id));

-- T9703

CREATE INDEX xif1t9703_rule_transaction ON t9703_rule_transaction (c9700_rule_id);

CREATE INDEX xif2t9703_rule_transaction ON t9703_rule_transaction (c901_txn_id);

ALTER TABLE t9703_rule_transaction ADD ( PRIMARY KEY (c9703_rule_transaction_id));

ALTER TABLE t9703_rule_transaction ADD ( FOREIGN KEY (c901_txn_id) REFERENCES t901_code_lookup (c901_code_id));

--T9704

ALTER TABLE t9704_rule_consequence ADD ( PRIMARY KEY (c9704_rule_consequence_id));

ALTER TABLE t9704_rule_consequence ADD ( FOREIGN KEY (c901_consequence_id) REFERENCES t901_code_lookup (c901_code_id));

ALTER TABLE t9705_consequence_details ADD (
  FOREIGN KEY (c9704_rule_consequence_id)
	REFERENCES t9704_rule_consequence (c9704_rule_consequence_id));
	
-- T9705

CREATE INDEX xif1t9705_consequence_details ON t9705_consequence_details (c9704_rule_consequence_id);

CREATE INDEX xif2t9705_consequence_details ON t9705_consequence_details (c901_param_id);

ALTER TABLE t9705_consequence_details ADD ( PRIMARY KEY (c9705_consequence_details_id) );

ALTER TABLE t9705_consequence_details ADD ( FOREIGN KEY (c901_param_id) REFERENCES t901_code_lookup (c901_code_id));

--T9001

CREATE INDEX xif1t9001_filter_lookup ON t9001_filter_lookup (c901_parameter_type_id);

ALTER TABLE t9001_filter_lookup ADD ( PRIMARY KEY (c9001_filter_id));

ALTER TABLE t9001_filter_lookup ADD ( FOREIGN KEY (c901_parameter_type_id) REFERENCES t901_code_lookup (c901_code_id));

ALTER TABLE t9002_filter_operator ADD (
  FOREIGN KEY (c9001_filter_id)
	REFERENCES t9001_filter_lookup (c9001_filter_id));

-- T9002


CREATE INDEX xif1t9002_filter_operator ON t9002_filter_operator (c9001_filter_id);

CREATE INDEX xif2t9002_filter_operator ON t9002_filter_operator (c901_operator_id);

ALTER TABLE t9002_filter_operator ADD ( PRIMARY KEY (c9002_filter_operator_id) );

ALTER TABLE t9002_filter_operator ADD ( FOREIGN KEY (c901_operator_id) REFERENCES t901_code_lookup (c901_code_id));

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Comments

COMMENT ON TABLE t9700_rule IS 'Will contail the master data of the rules';

COMMENT ON TABLE t9702_rule_condition IS 'Conditions for the rules should be stored in this table.';

COMMENT ON TABLE t9703_rule_transaction IS 'Transactions for the rules should be mapped in this table( Transactions are always delete and insert )';

COMMENT ON TABLE t9701_condition IS 'Master Table for conditions';

COMMENT ON TABLE t9702_rule_condition IS 'Conditions for the rules should be stored in this table.';

----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Sequences

CREATE SEQUENCE s9700_rule
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

CREATE SEQUENCE s9701_condition
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

CREATE	 SEQUENCE s9702_rule_condition
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

CREATE	 SEQUENCE s9703_rule_transaction
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

CREATE	 SEQUENCE s9704_rule_consequence
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

CREATE	 SEQUENCE s9705_consequence_details
  START WITH 37
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

CREATE	 SEQUENCE s9001_filter_lookup
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

CREATE	 SEQUENCE s9002_filter_operator
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;
