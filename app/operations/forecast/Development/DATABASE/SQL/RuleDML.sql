/* Formatted on 2009/07/21 18:41 (Formatter Plus v4.8.0) */
/****insert for condition****/

INSERT INTO t9701_condition
			(c9701_condition_id, c9701_condition_name, c9701_void_fl, c9701_active_fl, c9701_condition_level
			)
	 VALUES (1, 'PARTNUMBER', NULL, 'Y', 1
			);
INSERT INTO t9701_condition
			(c9701_condition_id, c9701_condition_name, c9701_void_fl, c9701_active_fl, c9701_condition_level
			)
	 VALUES (2, 'REVISION', NULL, 'Y', 2
			);
INSERT INTO t9701_condition
			(c9701_condition_id, c9701_condition_name, c9701_void_fl, c9701_active_fl, c9701_condition_level
			)
	 VALUES (3, 'COUNTRY', NULL, 'Y', 2
			);
INSERT INTO t9701_condition
			(c9701_condition_id, c9701_condition_name, c9701_void_fl, c9701_active_fl, c9701_condition_level
			)
	 VALUES (4, 'VENDOR', NULL, 'Y', 1
			);
INSERT INTO t9701_condition
			(c9701_condition_id, c9701_condition_name, c9701_void_fl, c9701_active_fl, c9701_condition_level
			)
	 VALUES (5, 'LOT', NULL, 'Y', 2
			);
INSERT INTO t9701_condition
			(c9701_condition_id, c9701_condition_name, c9701_void_fl, c9701_active_fl, c9701_condition_level
			)
	 VALUES (6, 'MANUFACTUREDDATE', NULL, 'Y', 2
			);
INSERT INTO t9701_condition
			(c9701_condition_id, c9701_condition_name, c9701_void_fl, c9701_active_fl, c9701_condition_level
			)
	 VALUES (7, 'SETID', NULL, 'Y', 1
			);
INSERT INTO t9701_condition
			(c9701_condition_id, c9701_condition_name, c9701_void_fl, c9701_active_fl, c9701_condition_level
			)
	 VALUES (8, 'QTYINSPECTED(%)', NULL, 'Y', 2
			);

/***For Filter******/

INSERT INTO t9001_filter_lookup
			(c9001_filter_id, c9001_ref_id, c9001_filter_grp, c9001_table_alias_name, c9001_column_name
		   , c9001_procedure_name, c9001_active_fl, c901_parameter_type_id, c9001_proc_in_param
			)
	 VALUES (2, '1', 'CM_RULE', NULL, NULL
		   , NULL, 'Y', 6101, NULL
			);
INSERT INTO t9001_filter_lookup
			(c9001_filter_id, c9001_ref_id, c9001_filter_grp, c9001_table_alias_name, c9001_column_name
		   , c9001_procedure_name, c9001_active_fl, c901_parameter_type_id, c9001_proc_in_param
			)
	 VALUES (3, '2', 'CM_RULE', NULL, NULL
		   , NULL, 'Y', 6101, NULL
			);
INSERT INTO t9001_filter_lookup
			(c9001_filter_id, c9001_ref_id, c9001_filter_grp, c9001_table_alias_name, c9001_column_name
		   , c9001_procedure_name, c9001_active_fl, c901_parameter_type_id, c9001_proc_in_param
			)
	 VALUES (4, '3', 'CM_RULE', NULL, NULL
		   , 'gm_pkg_cm_codelookup_util.gm_cm_fch_allcodelist', 'Y', 6100, 'CNTY'
			);
INSERT INTO t9001_filter_lookup
			(c9001_filter_id, c9001_ref_id, c9001_filter_grp, c9001_table_alias_name, c9001_column_name
		   , c9001_procedure_name, c9001_active_fl, c901_parameter_type_id, c9001_proc_in_param
			)
	 VALUES (5, '4', 'CM_RULE', NULL, NULL
		   , 'gm_pkg_cm_rule_conditions.gm_op_fch_vendorlist', 'Y', 6100, NULL
			);
INSERT INTO t9001_filter_lookup
			(c9001_filter_id, c9001_ref_id, c9001_filter_grp, c9001_table_alias_name, c9001_column_name
		   , c9001_procedure_name, c9001_active_fl, c901_parameter_type_id, c9001_proc_in_param
			)
	 VALUES (6, '5', 'CM_RULE', NULL, NULL
		   , NULL, 'Y', 6101, NULL
			);
INSERT INTO t9001_filter_lookup
			(c9001_filter_id, c9001_ref_id, c9001_filter_grp, c9001_table_alias_name, c9001_column_name
		   , c9001_procedure_name, c9001_active_fl, c901_parameter_type_id, c9001_proc_in_param
			)
	 VALUES (7, '6', 'CM_RULE', NULL, NULL
		   , NULL, 'Y', 6101, NULL
			);
INSERT INTO t9001_filter_lookup
			(c9001_filter_id, c9001_ref_id, c9001_filter_grp, c9001_table_alias_name, c9001_column_name
		   , c9001_procedure_name, c9001_active_fl, c901_parameter_type_id, c9001_proc_in_param
			)
	 VALUES (8, '7', 'CM_RULE', NULL, NULL
		   , 'gm_pkg_cm_rule_conditions.gm_pd_fch_setlist', 'Y', 6100, NULL
			);
INSERT INTO t9001_filter_lookup
			(c9001_filter_id, c9001_ref_id, c9001_filter_grp, c9001_table_alias_name, c9001_column_name
		   , c9001_procedure_name, c9001_active_fl, c901_parameter_type_id, c9001_proc_in_param
			)
	 VALUES (9, '8', 'CM_RULE', NULL, NULL
		   , NULL, 'Y', 6101, NULL
			);

/***t9002*******/
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (1, 'Y', 2, 50600
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (2, 'Y', 3, 50600
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (3, 'Y', 3, 50601
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (4, 'Y', 3, 50602
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (5, 'Y', 3, 50603
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (6, 'Y', 3, 50604
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (7, 'Y', 3, 50606
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (8, 'Y', 4, 50600
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (9, 'Y', 4, 50606
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (10, 'Y', 5, 50600
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (11, 'Y', 6, 50600
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (12, 'Y', 6, 50606
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (13, 'Y', 7, 50600
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (14, 'Y', 7, 50601
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (15, 'Y', 7, 50602
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (16, 'Y', 7, 50603
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (17, 'Y', 7, 50604
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (18, 'Y', 8, 50600
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (19, 'Y', 8, 50600
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (20, 'Y', 9, 50600
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (21, 'Y', 9, 50601
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (22, 'Y', 9, 50602
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (23, 'Y', 9, 50603
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (24, 'Y', 9, 50604
			);
INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (25, 'Y', 2, 50605
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no
			)
	 VALUES (50607, 'IN', 'OPERS', NULL, 8
			);

INSERT INTO t9002_filter_operator
			(c9002_filter_operator_id, c9002_active_fl, c9001_filter_id, c901_operator_id
			)
	 VALUES (26, 'Y', 2, 50607
			);


/**** fOR CONSEQUENCES ****/
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
			)
	 VALUES (91346, 'Message', 'CONSQ', 1, 1, 'CQMSG'
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
			)
	 VALUES (91347, 'Picture', 'CONSQ', 1, 3, 'CQPIC'
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
			)
	 VALUES (91348, 'Email', 'CONSQ', 1, 5, 'CQEML'
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (91351, 'Message', 'CQMSG', 1, 1, ''
		   , 'TXTARA'
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (91352, 'Hold Transaction', 'CQMSG', 1, 3, ''
		   , 'CHKBOX'
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (91356, 'Upload Picture', 'CQPIC', 1, 1, ''
		   , 'FILE'
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (91357, 'Comments', 'CQPIC', 1, 3, ''
		   , 'TXTARA'
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (91365, 'To', 'CQEML', 1, 1, ''
		   , 'TXTBOX'
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (91366, 'Subject', 'CQEML', 1, 3, ''
		   , 'TXTBOX'
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (91367, 'Body', 'CQEML', 1, 5, ''
		   , 'TXTARA'
			);


/*******Void Consequences*******/

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (90892, 'Void Consequence', 'CNCLT', 1, 35, 'VCNSQ'
		   , NULL
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no
			)
	 VALUES (92280, 'Reason 1 Voiding Consequences', 'VCNSQ', 1, 1
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no
			)
	 VALUES (92281, 'Reason 2 Voiding Consequences', 'VCNSQ', 1, 2
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no
			)
	 VALUES (92282, 'Reason 3 Voiding Consequences', 'VCNSQ', 1, 3
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (90893, 'Void Rule', 'CNCLT', 1, 36, 'VRULE'
		   , NULL
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no
			)
	 VALUES (92290, 'Reason 1 Voiding Rule', 'VRULE', 1, 1
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no
			)
	 VALUES (92291, 'Reason 2 Voiding Rule', 'VRULE', 1, 2
			);

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no
			)
	 VALUES (92292, 'Reason 3 Voiding Rule', 'VRULE', 1, 3
			);
/**code lookup **/

INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50900, 'DHR-Pending Receiving', 'RLTXN', 1, 1, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50901, 'DHR-Pending Inspection', 'RLTXN', 1, 2, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50902, 'DHR-Pending Packaging', 'RLTXN', 1, 3, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50903, 'DHR-Pending Verification', 'RLTXN', 1, 4, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50904, 'RN-Consignment', 'RLTXN', 1, 5, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50905, 'RN-Packaging', 'RLTXN', 1, 6, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50906, 'FG-Shelf to Product Loaner', 'RLTXN', 1, 7, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50907, 'FG-Shelf to Quarantine', 'RLTXN', 1, 8, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50908, 'FG-Shelf to Raw Material', 'RLTXN', 1, 9, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50909, 'FG-Shelf to Item Sales Consignment', 'RLTXN', 1, 10, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50910, 'FG-Shelf to Built Sets', 'RLTXN', 1, 11, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50911, 'FG-Shelf to Packaging', 'RLTXN', 1, 12, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50912, 'FG-Shelf to Item Inhouse Consignment', 'RLTXN', 1, 13, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50913, 'FG-Shelf to Inhouse Loaners', 'RLTXN', 1, 14, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50914, 'QN-Quarantine to Shelf', 'RLTXN', 1, 15, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50915, 'QN-Quarantine to Scrap', 'RLTXN', 1, 16, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50916, 'LN-Loaner to Shelf', 'RLTXN', 1, 17, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50917, 'LN-Loaner to Quarantine', 'RLTXN', 1, 18, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50918, 'SB-Built Sets to Quarantine', 'RLTXN', 1, 19, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50919, 'SB-Built Sets to Shelf', 'RLTXN', 1, 20, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50920, 'SB-Built Sets to Product Loaner', 'RLTXN', 1, 21, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50921, 'SB-Built Sets to InHouse Loaner', 'RLTXN', 1, 22, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50922, 'SB-Built Sets to Hospital', 'RLTXN', 1, 23, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50923, 'ILN-Loaner to Shelf', 'RLTXN', 1, 24, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50924, 'ILN-Loaner to Quarantine', 'RLTXN', 1, 25, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50925, 'OR-Orders', 'RLTXN', 1, 26, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50926, 'BM-RM to FG', 'RLTXN', 1, 27, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50927, 'Sales Item Consignment', 'RLTXN', 1, 28, NULL
		   , NULL
			);
INSERT INTO t901_code_lookup
			(c901_code_id, c901_code_nm, c901_code_grp, c901_active_fl, c901_code_seq_no, c902_code_nm_alt
		   , c901_control_type
			)
	 VALUES (50928, 'Inhouse Item Consignment', 'RLTXN', 1, 29, NULL
		   , NULL
			);


/*******access code lookup entry******/

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50900
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50901
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50902
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50903
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50904
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50905
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50906
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50907
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50908
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50909
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50910
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50911
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50912
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50913
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50914
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50915
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50916
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50917
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50918
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50919
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50920
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50921
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50922
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50923
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50924
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50925
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50926
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50927
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2004, 50928
			);


/****For 2009 - quality***/

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50900
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50901
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50902
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50903
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50904
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50905
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50906
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50907
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50908
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50909
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50910
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50911
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50912
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50913
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50914
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50915
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50916
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50917
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50918
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50919
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50920
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50921
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50922
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50923
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50924
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50925
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50926
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50927
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2009, 50928
			);


/*****2014 for Logistic*****/
INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50900
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50901
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50902
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50903
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50904
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50905
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50906
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50907
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50908
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50909
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50910
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50911
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50912
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50913
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50914
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50915
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50916
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50917
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50918
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50919
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50920
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50921
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50922
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50923
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50924
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50925
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50926
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50927
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2014, 50928
			);

/***2013 for receiving****/
INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50900
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50901
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50902
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50903
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50904
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50905
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50906
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50907
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50908
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50909
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50910
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50911
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50912
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50913
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50914
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50915
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50916
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50917
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50918
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50919
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50920
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50921
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50922
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50923
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50924
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50925
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50926
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50927
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2013, 50928
			);

/****2003..****/
INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50900
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50901
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50902
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50903
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50904
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50905
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50906
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50907
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50908
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50909
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50910
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50911
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50912
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50913
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50914
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50915
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50916
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50917
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50918
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50919
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50920
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50921
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50922
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50923
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50924
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50925
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50926
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50927
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2003, 50928
			);

/****2006**/
INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50900
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50901
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50902
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50903
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50904
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50905
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50906
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50907
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50908
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50909
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50910
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50911
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50912
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50913
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50914
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50915
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50916
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50917
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50918
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50919
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50920
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50921
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50922
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50923
			);


INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50924
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50925
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50926
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50927
			);

INSERT INTO t901a_lookup_access
			(c901a_code_access_id, c901_access_code_id, c901_code_id
			)
	 VALUES (s901a_lookup_access.NEXTVAL, 2006, 50928
			);

INSERT INTO T901_CODE_LOOKUP ( C901_CODE_ID, C901_CODE_NM, C901_CODE_GRP, C901_ACTIVE_FL,
C901_CODE_SEQ_NO, C902_CODE_NM_ALT, C901_CONTROL_TYPE ) VALUES ( 
91183, 'Consequence Image', 'PTYFLT', 1, 2, NULL, NULL); 
