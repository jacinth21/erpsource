/* Formatted on 2008/08/08 16:32 (Formatter Plus v4.8.0) */
CREATE OR REPLACE PACKAGE BODY gm_pkg_pd_growth
IS
--
/*******************************************************
 * Purpose: Package holds Growth Save and Fetch Methods
 * Created By VPrasath
 *******************************************************/
--
/*******************************************************
 * Purpose: This Procedure is used to fetch the
 * growth references for the given part/set/group
 *******************************************************/
--
	PROCEDURE gm_pd_fch_growth_ref (
		p_demand_id   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_ref_type	  IN	   t4021_demand_mapping.c901_ref_type%TYPE
	  , p_ref_ids	  OUT	   TYPES.cursor_type
	)
--
	AS
	BEGIN
-- FOR GROUP
		IF p_ref_type = 20297
		THEN
			OPEN p_ref_ids
			 FOR
				 SELECT c4010_group_id refid, c4010_group_nm refnm
				   FROM t4010_group
				  WHERE c4010_group_id IN (SELECT c4021_ref_id
											 FROM t4021_demand_mapping
											WHERE c4020_demand_master_id = p_demand_id AND c901_ref_type = '40030');
		END IF;

-- FOR PARTS IN GROUPS
		IF p_ref_type = 20295
		THEN
			OPEN p_ref_ids
			 FOR
				 SELECT   t4011.c205_part_number_id refid
						, t4011.c205_part_number_id || ' : ' || get_partnum_desc (t4011.c205_part_number_id) refnm
					 FROM t4011_group_detail t4011
					WHERE c4010_group_id IN (SELECT c4021_ref_id
											   FROM t4021_demand_mapping
											  WHERE c4020_demand_master_id = p_demand_id AND c901_ref_type = '40030')
				 ORDER BY t4011.c205_part_number_id;
		END IF;

-- FOR PARTS IN SETS
		IF p_ref_type = 20298
		THEN
			OPEN p_ref_ids
			 FOR
				 SELECT DISTINCT c205_part_number_id refid
							   , c205_part_number_id || ' : ' || get_partnum_desc (c205_part_number_id) refnm
							FROM t208_set_details
						   WHERE c207_set_id IN (
												 SELECT c4021_ref_id
												   FROM t4021_demand_mapping
												  WHERE c4020_demand_master_id = p_demand_id
														AND c901_ref_type = '40031')
						ORDER BY c205_part_number_id;
		END IF;

-- FOR SET
		IF p_ref_type = 20296
		THEN
			OPEN p_ref_ids
			 FOR
				 SELECT c207_set_id refid, (c207_set_id || ' : ' || c207_set_nm) refnm
				   FROM t207_set_master
				  WHERE c207_set_id IN (SELECT c4021_ref_id
										  FROM t4021_demand_mapping
										 WHERE c4020_demand_master_id = p_demand_id AND c901_ref_type = '40031');
		END IF;
	END gm_pd_fch_growth_ref;

--
/*******************************************************
 * Purpose: This Procedure is used to save the
 * growth details
 *******************************************************/
--
	PROCEDURE gm_pd_sav_growth_details (
		p_demand_id   IN	   t4020_demand_master.c4020_demand_master_id%TYPE
	  , p_ref_type	  IN	   t4021_demand_mapping.c901_ref_type%TYPE
	  , p_ref_id	  IN	   t4021_demand_mapping.c4021_ref_id%TYPE
	  , p_year		  IN	   VARCHAR2
	  , p_inputstr	  IN	   VARCHAR2
	  , p_userid	  IN	   t4030_demand_growth_mapping.c4030_created_by%TYPE
	  , p_growth_id   OUT	   VARCHAR
	)
	AS
		v_cnt		   NUMBER;
		v_dmi_cnt	   NUMBER := 0;
		v_gid		   t4030_demand_growth_mapping.c4030_demand_growth_id%TYPE;
		v_string	   VARCHAR2 (30000) := p_inputstr;
		v_substring    VARCHAR2 (1000);
		v_start_date   VARCHAR2 (20);
		v_end_date	   DATE;
		v_growthvalue  VARCHAR2 (20);
		v_month 	   VARCHAR2 (20);
		v_growth_ref   VARCHAR2 (20);
		v_year		   VARCHAR2 (10);
	BEGIN
		IF p_ref_type = 20295
		THEN
			--
			SELECT COUNT (t4011.c205_part_number_id)
			  INTO v_cnt
			  FROM t4011_group_detail t4011, t4021_demand_mapping t4021
			 WHERE c4010_group_id = c4021_ref_id
			   AND c4020_demand_master_id = p_demand_id
			   AND c901_ref_type = '40030'
			   AND c205_part_number_id = p_ref_id;
		--
		END IF;

		IF v_cnt = 0
		THEN
			--
			raise_application_error (-20029, '');
		--
		END IF;

		SELECT c901_code_nm
		  INTO v_year
		  FROM t901_code_lookup
		 WHERE c901_code_id = p_year;

		SELECT COUNT (c4030_demand_growth_id)
		  INTO v_cnt
		  FROM t4030_demand_growth_mapping t4030
		 WHERE c4020_demand_master_id = p_demand_id
		   AND c901_ref_type = p_ref_type
		   AND c4030_ref_id = p_ref_id
		   AND c4030_demand_growth_id IN (
				   SELECT c4030_demand_growth_id
					 FROM t4031_growth_details t4031
					WHERE t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
					  AND TO_CHAR (c4031_start_date, 'YYYY') = v_year);

		IF v_cnt > 0
		THEN
			SELECT c4030_demand_growth_id
			  INTO v_gid
			  FROM t4030_demand_growth_mapping t4030
			 WHERE c4020_demand_master_id = p_demand_id
			   AND c901_ref_type = p_ref_type
			   AND c4030_ref_id = p_ref_id
			   AND c4030_demand_growth_id IN (
					   SELECT c4030_demand_growth_id
						 FROM t4031_growth_details t4031
						WHERE t4031.c4030_demand_growth_id = t4030.c4030_demand_growth_id
						  AND TO_CHAR (c4031_start_date, 'YYYY') = v_year);

			raise_application_error (-20042, v_gid);
		END IF;

		IF v_cnt = 0
		THEN
			SELECT COUNT (c4030_demand_growth_id)
			  INTO v_dmi_cnt
			  FROM t4030_demand_growth_mapping t4030
			 WHERE c4020_demand_master_id = p_demand_id AND c901_ref_type = p_ref_type AND c4030_ref_id = p_ref_id;
		END IF;

		IF v_dmi_cnt = 0
		THEN
			SELECT s4030_demand_growth_mapping.NEXTVAL
			  INTO v_gid
			  FROM DUAL;

			INSERT INTO t4030_demand_growth_mapping
						(c4030_demand_growth_id, c4020_demand_master_id, c901_ref_type, c4030_ref_id, c4030_created_by
					   , c4030_created_date
						)
				 VALUES (v_gid, p_demand_id, p_ref_type, p_ref_id, p_userid
					   , SYSDATE
						);
		ELSE
			SELECT c4030_demand_growth_id
			  INTO v_gid
			  FROM t4030_demand_growth_mapping t4030
			 WHERE c4020_demand_master_id = p_demand_id AND c901_ref_type = p_ref_type AND c4030_ref_id = p_ref_id;
		END IF;

		p_growth_id := v_gid;

		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			--
			v_month 	:= NULL;
			v_growthvalue := NULL;
			v_growth_ref := NULL;
			--
			v_month 	:= SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_growthvalue := SUBSTR (v_substring, 1, INSTR (v_substring, '^') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, '^') + 1);
			v_growth_ref := v_substring;
			--
			v_start_date := v_month || '/01/' || v_year;

			SELECT LAST_DAY (TO_DATE (v_start_date, 'MM/DD/YYYY'))
			  INTO v_end_date
			  FROM DUAL;

			INSERT INTO t4031_growth_details
						(c4031_growth_details_id, c4030_demand_growth_id, c4031_value, c901_ref_type
					   , c4031_start_date, c4031_end_date, c4031_last_updated_by, c4031_last_updated_date
						)
				 VALUES (s4031_growth.NEXTVAL, v_gid, TO_NUMBER (v_growthvalue), v_growth_ref
					   , TO_DATE (v_start_date, 'mm/dd/yyyy'), v_end_date, p_userid, SYSDATE
						);
		END LOOP;
	END gm_pd_sav_growth_details;

--
/*******************************************************
 * Purpose: This Procedure is used to fetch the
 * growth details for the given demand growth id
 *******************************************************/
--
	PROCEDURE gm_pd_fch_growth_details (
		p_demand_master_id	 IN 	  t4030_demand_growth_mapping.c4020_demand_master_id%TYPE
	  , p_ref_type			 IN 	  t4030_demand_growth_mapping.c901_ref_type%TYPE
	  , p_ref_id			 IN 	  t4030_demand_growth_mapping.c4030_ref_id%TYPE
	  , p_year				 IN 	  VARCHAR2
	  , p_ref				 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_ref
		 FOR
			 SELECT   t4030.c4030_demand_growth_id growthid, TO_CHAR (t4031.c4031_start_date, 'MM') refmonth
					, t4031.c4031_value refvalue, t4031.c901_ref_type refmonthtype
					, DECODE
						  (SIGN (  TO_NUMBER (TO_CHAR (t4031.c4031_start_date, 'YYYYMM'))
								 - TO_NUMBER (TO_CHAR (SYSDATE, 'YYYYMM'))
								)
						 , -1, 'D'
						 , 0, DECODE (NVL (gm_pkg_op_sheet.get_demand_sheet_status (p_demand_master_id
																				  , t4031.c4031_start_date
																				   )
										 , 50550
										  )
									, 50550, 'E'
									, 'D'
									 )
						 , 'E'
						  ) controlvalue
				 FROM t4030_demand_growth_mapping t4030, t4031_growth_details t4031
				WHERE t4030.c4030_demand_growth_id = t4031.c4030_demand_growth_id
				  AND t4030.c4020_demand_master_id = p_demand_master_id
				  AND t4030.c901_ref_type = p_ref_type
				  AND t4030.c4030_ref_id = p_ref_id
				  AND t4030.c4030_void_fl IS NULL
				  AND t4031.c4031_void_fl IS NULL
				  AND TO_CHAR (c4031_start_date, 'YYYY') = get_code_name (p_year)
			 ORDER BY refmonth;
	END gm_pd_fch_growth_details;
END gm_pkg_pd_growth;
/
