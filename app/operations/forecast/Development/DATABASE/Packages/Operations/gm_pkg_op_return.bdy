/* Formatted on 2009/03/09 17:20 (Formatter Plus v4.8.0) */
-- @ "C:\database\Packages\Operations\gm_pkg_op_return.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_op_return
IS
--
 /*******************************************************
 * Purpose: This Procedure is used to fetch the
 * open transactions for the given distributor
 *******************************************************/
--
	PROCEDURE gm_op_fch_distopentrans (
		p_distid	   IN		t701_distributor.c701_distributor_id%TYPE
	  , p_cons_cur	   OUT		TYPES.cursor_type
	  , p_return_cur   OUT		TYPES.cursor_type
	  , p_trans_cur    OUT		TYPES.cursor_type
	)
--
	AS
	BEGIN
		OPEN p_cons_cur
		 FOR
			 SELECT c504_consignment_id ID, get_code_name (c504_type) TYPE
				  , TO_CHAR (NVL (c504_last_updated_date, c504_created_date), 'MM/DD/YYYY') cdate
				  , DECODE (c504_status_fl
						  , 6, 'Pending Transfer'
						  , 0, 'Back Order'
						  , 1, 'Initated'
						  , 2, 'Pending Control Number'
						  , 3, 'Pending Shipment'
						   ) status
				  , c207_set_id setid
			   FROM t504_consignment
			  WHERE c701_distributor_id = p_distid AND c504_status_fl < 4 AND c504_void_fl IS NULL AND c504_type = 4110;

		OPEN p_return_cur
		 FOR
			 SELECT c506_rma_id ID, get_code_name (c506_type) TYPE
				  , TO_CHAR (NVL (c506_last_updated_date, c506_created_date), 'MM/DD/YYYY') cdate
				  , DECODE (c506_status_fl, 0, 'Initiated', 1, 'Pending Credit') status, c207_set_id setid
			   FROM t506_returns
			  WHERE c701_distributor_id = p_distid
				AND c506_type IN (3301, 3302)
				AND c506_status_fl = 1
				AND c506_void_fl IS NULL
				AND c506_reason <> 3311;

		OPEN p_trans_cur
		 FOR
			 SELECT c920_transfer_id ID, get_code_name (c901_transfer_mode) TYPE
				  , TO_CHAR (c920_transfer_date, 'MM/DD/YYYY') cdate, DECODE (c920_status_fl, 1, 'Initiated') status
			   FROM t920_transfer
			  WHERE c920_status_fl < 2
				AND ((c920_from_ref_id = p_distid AND c901_transfer_type = 90300) OR c920_to_ref_id = p_distid)
				AND c920_void_fl IS NULL;
	--
	END gm_op_fch_distopentrans;

	---
/*******************************************************
 * Purpose: Procedure is used to load the open consignment
 * for the given distributor
 *******************************************************/
--
	PROCEDURE gm_op_fch_distopencons (
		p_distid	  IN	   t701_distributor.c701_distributor_id%TYPE
	  , p_consg_cur   OUT	   TYPES.cursor_type
	  , p_rule_cur	  OUT	   TYPES.cursor_type
	  , p_ra_cur	  OUT	   TYPES.cursor_type
	)
--
	AS
	BEGIN
		OPEN p_consg_cur
		 FOR
			 SELECT pnum, partdesc, price, cons_qty, ret_qty, pending_qty, total_value
			   FROM (SELECT c205_part_number_id pnum, c205_part_num_desc partdesc, t205.c205_equity_price price
						  , NVL (consign, 0) cons_qty, NVL (cons_return, 0) ret_qty
						  , (NVL (consign, 0) - NVL (cons_return, 0)) pending_qty
						  , ((NVL (consign, 0) - NVL (cons_return, 0)) * t205.c205_equity_price) total_value
					   FROM t205_part_number t205
						  , (SELECT   t505.c205_part_number_id partnum, SUM (t505.c505_item_qty) consign
								 FROM t504_consignment t504, t505_item_consignment t505
								WHERE t504.c504_consignment_id = t505.c504_consignment_id
								  AND t504.c701_distributor_id = p_distid
								  AND c504_type = 4110
								  AND t504.c504_status_fl = 4
								  AND c504_void_fl IS NULL
								  AND TRIM (t505.c505_control_number) IS NOT NULL
							 GROUP BY t505.c205_part_number_id) cons
						  , (SELECT   t507.c205_part_number_id partnum, SUM (c507_item_qty) cons_return
								 FROM t506_returns t506, t507_returns_item t507
								WHERE t506.c506_rma_id = t507.c506_rma_id
								  AND t506.c701_distributor_id = p_distid
								  AND t506.c506_type IN (3301, 3302, 3306)
								  AND t506.c506_void_fl IS NULL
								  AND t506.c506_status_fl = 2
								  AND TRIM (t507.c507_control_number) IS NOT NULL
								  AND t507.c507_status_fl IN ('C', 'W')
							 GROUP BY t507.c205_part_number_id) retn
					  WHERE t205.c205_part_number_id = cons.partnum AND t205.c205_part_number_id = retn.partnum(+))
			  WHERE pending_qty > 0;

		OPEN p_rule_cur
		 FOR
			 SELECT c906_rule_value emailto
			   FROM t906_rules
			  WHERE c906_rule_id = 'INIRA';

		OPEN p_ra_cur
		 FOR
			 SELECT t506.c506_rma_id raid, t506.c506_reason reason, TO_CHAR (c506_expected_date, 'mm/dd/yyyy') edate
				  , t506.c506_status_fl sfl
			   FROM t506_returns t506
			  WHERE t506.c701_distributor_id = p_distid
				AND t506.c506_type = 3306
				AND c506_void_fl IS NULL
				AND c506_status_fl IN (0, 1);
	END gm_op_fch_distopencons;

--
/*******************************************************
 * Purpose: Procedure is used to initiate a
 * Return for the distributor closure
 *******************************************************/
--
	PROCEDURE gm_op_sav_distclosure (
		p_distid	IN		 t701_distributor.c701_distributor_id%TYPE
	  , p_expdate	IN		 VARCHAR2
	  , p_reason	IN		 t506_returns.c506_reason%TYPE
	  , p_userid	IN		 t101_user.c101_user_id%TYPE
	  , p_rma_id	OUT 	 t506_returns.c506_rma_id%TYPE
	)
--
	AS
		v_ra_id 	   NUMBER;
		v_cnt		   NUMBER;
		v_id		   NUMBER;
		v_string	   VARCHAR2 (20);
		v_id_string    VARCHAR2 (20);
		v_empname	   NUMBER;
		subject 	   VARCHAR2 (100);
		mail_body	   VARCHAR2 (3000);
		crlf		   VARCHAR2 (2) := CHR (13) || CHR (10);
		to_mail 	   t906_rules.c906_rule_value%TYPE;
		v_user_name    VARCHAR2 (100);
		v_dist_name    VARCHAR2 (100);

		CURSOR pop_val
		IS
			SELECT pnum, pending_qty, price
			  FROM (SELECT c205_part_number_id pnum, (NVL (consign, 0) - NVL (cons_return, 0)) pending_qty
						 , t205.c205_equity_price price
					  FROM t205_part_number t205
						 , (SELECT	 t505.c205_part_number_id partnum, SUM (t505.c505_item_qty) consign
								FROM t504_consignment t504, t505_item_consignment t505
							   WHERE t504.c504_consignment_id = t505.c504_consignment_id
								 AND t504.c701_distributor_id = p_distid
								 AND c504_type = 4110
								 AND t504.c504_status_fl = 4
								 AND c504_void_fl IS NULL
								 AND TRIM (t505.c505_control_number) IS NOT NULL
							GROUP BY t505.c205_part_number_id) cons
						 , (SELECT	 t507.c205_part_number_id partnum, SUM (c507_item_qty) cons_return
								FROM t506_returns t506, t507_returns_item t507
							   WHERE t506.c506_rma_id = t507.c506_rma_id
								 AND t506.c701_distributor_id = p_distid
								 AND t506.c506_type IN (3301, 3302)
								 AND t506.c506_void_fl IS NULL
								 AND t506.c506_status_fl = 2
								 AND TRIM (t507.c507_control_number) IS NOT NULL
								 AND t507.c507_status_fl IN ('C', 'W')
							GROUP BY t507.c205_part_number_id) retn
					 WHERE t205.c205_part_number_id = cons.partnum AND t205.c205_part_number_id = retn.partnum(+))
			 WHERE pending_qty > 0;
	BEGIN
		-- moving this up to lock the distributor selection which will lock the count calculation as well. updated by Joe.
		SELECT	   c701_distributor_name
			  INTO v_dist_name
			  FROM t701_distributor
			 WHERE c701_distributor_id = p_distid
		FOR UPDATE;

		SELECT COUNT (t506.c506_rma_id)
		  INTO v_cnt
		  FROM t506_returns t506
		 WHERE t506.c701_distributor_id = p_distid
		   AND t506.c506_type = 3306
		   AND t506.c506_void_fl IS NULL
		   AND t506.c506_status_fl IN (1);

		IF v_cnt <> 0
		THEN
			--
			SELECT t506.c506_rma_id
			  INTO p_rma_id
			  FROM t506_returns t506
			 WHERE t506.c701_distributor_id = p_distid AND t506.c506_type = 3306;

			raise_application_error (-20031, p_rma_id);
		--
		END IF;

		UPDATE t506_returns
		   SET c506_void_fl = 'Y'
		 WHERE c701_distributor_id = p_distid
		   AND c506_type IN (3301, 3302)
		   AND c506_status_fl = 0
		   AND c506_void_fl IS NULL
		   AND c506_reason <> 3311;

		SELECT 'GM-RA-' || s506_return.NEXTVAL
		  INTO v_string
		  FROM DUAL;

		INSERT INTO t506_returns
					(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date, c506_status_fl
				   , c506_created_by, c506_created_date
					)
			 VALUES (v_string, p_distid, 3306, p_reason, TO_DATE (p_expdate, 'mm/dd/yyyy'), '0'
				   , p_userid, SYSDATE
					);

		FOR rad_val IN pop_val
		LOOP
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.pending_qty, rad_val.price
					   , 'Q'
						);

			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.pending_qty, rad_val.price
					   , 'R'
						);
		END LOOP;

		UPDATE t701_distributor
		   SET c701_active_fl = 'N'
		 WHERE c701_distributor_id = p_distid;

		to_mail 	:= get_rule_value ('INIRA', 'EMAIL');
		v_user_name := get_user_name (p_userid);
		subject 	:= 'Distributor closure - ' || v_dist_name;
		mail_body	:=
			   'RETURN INITIATED FOR DISTRIBUTOR CLOSURE'
			|| crlf
			|| 'DISTTRIBUTOR NAME :- '
			|| v_dist_name
			|| crlf
			|| 'RA NO :- '
			|| v_string
			|| crlf
			|| 'INITIATED BY :- '
			|| v_user_name
			|| crlf
			|| 'INITIATED DATE	:- '
			|| TO_CHAR (SYSDATE, 'dd/mm/yyyy hh24:mi:ss');
		gm_com_send_email_prc (to_mail, subject, mail_body);
		p_rma_id	:= v_string;
		-- Save Return status : Distclosure  using 91150 initiate
		gm_save_status_details (v_string, '', p_userid, NULL, SYSDATE, '91150', '91100');
	END gm_op_sav_distclosure;

--
/*******************************************************
 * Purpose: Procedure is used to fetch
 * parts for the given sets, set quantities and part numbers
 *******************************************************/
--
	PROCEDURE gm_op_fch_return_info (
		p_distid	 IN 	  t701_distributor.c701_distributor_id%TYPE
	  , p_set_id	 IN 	  VARCHAR2
	  , p_qty		 IN 	  VARCHAR2
	  , p_part_id	 IN 	  VARCHAR2
	  , p_emp_id	 IN 	  t504_consignment.c704_account_id%TYPE
	  , p_part_cur	 OUT	  TYPES.cursor_type
	)
--
	AS
	BEGIN
-- To assign set and part information
		my_context.set_my_double_inlist_ctx (p_set_id, p_qty);
-- To assign part information
		my_context.set_my_inlist_ctx (p_part_id);

		OPEN p_part_cur
		 FOR
			 SELECT pnum, partdesc, set_qty, pending_qty, '1' chkbox, DECODE (set_qty, NULL, 0, set_qty) retqty
			   FROM (SELECT c205_part_number_id pnum, c205_part_num_desc partdesc, NVL (consign, 0) cons_qty
						  , (NVL (consign, 0) - NVL (cons_return, 0)) pending_qty
						  , DECODE (gm_pkg_op_return.get_op_fch_set_qty (c205_part_number_id)
								  , 0, (NVL (consign, 0) - NVL (cons_return, 0))
								  , NVL (consign, 0)
								   ) retqty
						  , gm_pkg_op_return.get_op_fch_set_qty (c205_part_number_id) set_qty
					   FROM t205_part_number t205
						  , (SELECT   t505.c205_part_number_id partnum, SUM (t505.c505_item_qty) consign
								 FROM t504_consignment t504, t505_item_consignment t505
								WHERE t504.c504_consignment_id = t505.c504_consignment_id
								  -- AND t504.c701_distributor_id = p_distid
								  AND DECODE (p_distid, '01', t504.c704_account_id, c701_distributor_id) = p_distid
								  AND NVL (t504.c504_ship_to_id, -999) =
													  DECODE (p_distid
															, '01', p_emp_id
															, NVL (t504.c504_ship_to_id, -999)
															 )
								  AND c504_type IN (4110, 4112)
								  AND t504.c504_status_fl = 4
								  AND c504_void_fl IS NULL
								  AND TRIM (t505.c505_control_number) IS NOT NULL
							 GROUP BY t505.c205_part_number_id) cons
						  , (SELECT   t507.c205_part_number_id partnum, SUM (c507_item_qty) cons_return
								 FROM t506_returns t506, t507_returns_item t507
								WHERE t506.c506_rma_id = t507.c506_rma_id
								  -- AND t506.c701_distributor_id = p_distid
								  AND DECODE (p_distid, '01', t506.c704_account_id, t506.c701_distributor_id) = p_distid
								  AND DECODE (p_distid, '01', t506.c101_user_id, '01') =
																				 DECODE (p_distid
																					   , '01', p_emp_id
																					   , '01'
																						)
								  AND t506.c506_type IN (3301, 3302)
								  AND t506.c506_void_fl IS NULL
								  AND t506.c506_status_fl = 2
								  AND TRIM (t507.c507_control_number) IS NOT NULL
								  AND t507.c507_status_fl IN ('C', 'W')
							 GROUP BY t507.c205_part_number_id) retn
					  WHERE t205.c205_part_number_id = cons.partnum(+) AND t205.c205_part_number_id = retn.partnum(+))
			  WHERE (pnum IN (SELECT c205_part_number_id
								FROM t208_set_details, v_double_in_list
							   WHERE c207_set_id = token) OR pnum IN (SELECT token
																		FROM v_in_list));
	END gm_op_fch_return_info;

--
/*******************************************************
 * Purpose: function is used to fetch
 * the quantity of part in a set for the given part number
 *******************************************************/
--
	FUNCTION get_op_fch_set_qty (
		p_part_id	t205_part_number.c205_part_number_id%TYPE
	)
		RETURN NUMBER
	IS
--
		v_set_qty	   NUMBER;
	BEGIN
		SELECT SUM (t208.c208_set_qty * vdi.tokenii)
		  INTO v_set_qty
		  FROM t208_set_details t208, v_double_in_list vdi
		 WHERE t208.c207_set_id = vdi.token AND t208.c205_part_number_id = p_part_id;

		RETURN v_set_qty;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_op_fch_set_qty;

--
/*******************************************************
 * Purpose: Procedure is used to initiate a
 * RA for the Consignment Set / Item Return
 *******************************************************/
--
	PROCEDURE gm_op_sav_return (
		p_distid	 IN 	  t701_distributor.c701_distributor_id%TYPE
	  , p_expdate	 IN 	  VARCHAR2
	  , p_reason	 IN 	  t506_returns.c506_reason%TYPE
	  , p_userid	 IN 	  t101_user.c101_user_id%TYPE
	  , p_type		 IN 	  t506_returns.c506_type%TYPE
	  , p_emp_id	 IN 	  t506_returns.c101_user_id%TYPE
	  , p_part_qty	 IN 	  VARCHAR2
	  , p_part_id	 IN 	  VARCHAR2
	  , p_set_id	 IN 	  t506_returns.c207_set_id%TYPE
	  , p_rma_id	 OUT	  t506_returns.c506_rma_id%TYPE
	)
--
	AS
		v_string	   VARCHAR2 (20);
		v_dist_id	   VARCHAR2 (20);
		v_acc_id	   VARCHAR2 (20) := '';
		v_clsr_count   NUMBER;
		v_dist_name    t701_distributor.c701_distributor_name%TYPE;

		CURSOR v_part_cur
		IS
			SELECT token pnum, tokenii qty, c205_equity_price price
			  FROM v_double_in_list vlist, t205_part_number t205
			 WHERE t205.c205_part_number_id = vlist.token;
	BEGIN
		-- this code to lock the distributor selection which will lock the count calculation as well. updated by Joe.
		IF p_distid <> '01'
		THEN
			SELECT	   c701_distributor_name
				  INTO v_dist_name
				  FROM t701_distributor
				 WHERE c701_distributor_id = p_distid
			FOR UPDATE;

			SELECT COUNT (1)
			  INTO v_clsr_count
			  FROM t506_returns
			 WHERE c701_distributor_id = p_distid AND c506_type = '3306' AND c506_void_fl IS NULL
				   AND c506_status_fl <> 2;

			IF v_clsr_count > 0
			THEN
				raise_application_error (-20041, '');
			END IF;
		END IF;

-- To assign set and part information
		my_context.set_my_double_inlist_ctx (p_part_id, p_part_qty);

		SELECT 'GM-RA-' || s506_return.NEXTVAL
		  INTO v_string
		  FROM DUAL;

		IF p_distid = '01'
		THEN
			v_dist_id	:= '';
			v_acc_id	:= '01';
		ELSE
			v_dist_id	:= p_distid;
		END IF;

		INSERT INTO t506_returns
					(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date, c506_status_fl
				   , c704_account_id, c101_user_id, c207_set_id, c506_created_by, c506_created_date
					)
			 VALUES (v_string, v_dist_id, p_type, p_reason, TO_DATE (p_expdate, 'mm/dd/yyyy'), '0'
				   , v_acc_id, p_emp_id, p_set_id, p_userid, SYSDATE
					);

		FOR rad_val IN v_part_cur
		LOOP
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.qty, rad_val.price
					   , 'Q'
						);

			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, v_string, rad_val.pnum, rad_val.qty, rad_val.price
					   , 'R'
						);
		END LOOP;

		--Save Return status: Initiate
		gm_save_status_details (v_string, '', p_userid, NULL, SYSDATE, '91150', '91100');	--91100 'Return'; 91150 Initiate
		p_rma_id	:= v_string;
	END gm_op_sav_return;

/********************************************************************************************
 * Description	:This procedure is used for processing Returns
 *********************************************************************************************/
--
	PROCEDURE gm_op_sav_return_accp (
		p_raid		IN		 t506_returns.c506_rma_id%TYPE
	  , p_str		IN		 VARCHAR2
	  , p_retdate	IN		 VARCHAR2
	  , p_flag		IN		 VARCHAR2
	  , p_userid	IN		 t506_returns.c506_last_updated_by%TYPE
	  , p_errmsg	OUT 	 VARCHAR2
	)
	AS
--
		v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
		v_string	   VARCHAR2 (30000) := p_str;
		v_pnum		   VARCHAR2 (20);
		v_qty		   NUMBER;
		v_control	   VARCHAR2 (20);
		v_orgcontrol   VARCHAR2 (20);
		v_substring    VARCHAR2 (1000);
		v_msg		   VARCHAR2 (1000);
		v_price 	   VARCHAR2 (20);
		v_verify_date  DATE;
		v_fl		   VARCHAR2 (10);
		v_partqtyorig  NUMBER;
		v_partqty	   NUMBER;
		v_partnum	   VARCHAR2 (20);
		v_diffinnum    NUMBER;
		v_itemtype	   NUMBER;
--
		v_order_id	   t506_returns.c501_order_id%TYPE;
		v_reason	   t506_returns.c506_reason%TYPE;
		v_items 	   t507_returns_item%ROWTYPE;
--
		e_void_exp	   EXCEPTION;	-- Exception for checking if the Void flag is NULL
		v_void_fl	   VARCHAR2 (10);
		v_status_fl    VARCHAR2 (10);
		v_cnt_qty	   NUMBER;
		v_type		   t507_returns_item.c901_type%TYPE;
		v_dist_id	   t506_returns.c701_distributor_id%TYPE;
	BEGIN
		p_errmsg	:= 'RA NOT SAVED';

		--
		SELECT	   c506_void_fl, c506_status_fl, c207_set_id, c501_order_id, c506_reason, c701_distributor_id
			  INTO v_void_fl, v_status_fl, v_fl, v_order_id, v_reason, v_dist_id
			  FROM t506_returns
			 WHERE c506_rma_id = p_raid
		FOR UPDATE;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Returns already Voided.
			raise_application_error (-20021, '');
		END IF;

		IF v_status_fl = 1
		THEN
			raise_application_error (-20036, '');
		END IF;

		IF (v_strlen = 0)
		THEN
			RETURN;
		END IF;

		--
		DELETE FROM t507_returns_item
			  WHERE c506_rma_id = p_raid AND c507_status_fl <> 'Q';

		--insert new value into returns table
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_orgcontrol := NULL;
			v_itemtype	:= NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_orgcontrol := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_itemtype	:= TO_NUMBER (v_substring);

			--gm_procedure_log ('itemtype', v_itemtype);

			--
				-- If Consignment get the Equity Value
				-- or get the order value
			IF (v_order_id IS NULL)
			THEN
				v_price 	:= get_part_price (v_pnum, 'E');
			ELSE
				SELECT c502_item_price
				  INTO v_price
				  FROM t502_item_order
				 WHERE c501_order_id = v_order_id AND c205_part_number_id = v_pnum AND ROWNUM = 1;
			END IF;

			--
			IF (v_qty > 0)
			THEN
				INSERT INTO t507_returns_item
							(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number
						   , c507_item_qty, c507_item_price, c507_status_fl, c901_type
							)
					 VALUES (s507_return_item.NEXTVAL, p_raid, v_pnum, v_control
						   , v_qty, v_price, 'R', v_itemtype
							);

				UPDATE t507_returns_item
				   SET c507_control_number = v_control
				 WHERE c506_rma_id = p_raid AND c205_part_number_id = v_pnum AND c507_status_fl = 'Q';
			END IF;
		END LOOP;

		-- to check if the order qty or inhouse qty matchs the init qty
		IF v_order_id IS NOT NULL OR v_dist_id IS NULL
		THEN
			IF p_flag = 1
			THEN
				SELECT COUNT (*)
				  INTO v_cnt_qty
				  FROM (SELECT SUM (DECODE (c507_status_fl, 'Q', c507_item_qty, 0)) ini_qty
							 , SUM (DECODE (c507_status_fl, 'R', c507_item_qty, 0)) ret_qty
							 , SUM (DECODE (c507_status_fl, 'R', (DECODE (TRIM (c507_control_number), NULL, 1, 0)), 0)
								   ) cntrl_number
						  FROM t507_returns_item
						 WHERE c506_rma_id = p_raid)
				 WHERE ini_qty = ret_qty AND cntrl_number = 0;

				IF v_cnt_qty = 0
				THEN
					raise_application_error (-20039, '');
				END IF;
			END IF;
		END IF;

		UPDATE t506_returns
		   SET c506_return_date = TO_DATE (p_retdate, 'mm/dd/yyyy')
			 , c506_last_updated_by = p_userid
			 , c506_last_updated_date = SYSDATE
		 WHERE c506_rma_id = p_raid;

		/*SELECT c901_type
			INTO v_type
			FROM t507_returns_item
		 WHERE c506_rma_id = p_raid AND c507_status_fl = 'Q' AND ROWNUM = 1;


		UPDATE t507_returns_item
			 SET c901_type = v_type
		 WHERE c506_rma_id = p_raid AND c507_status_fl <> 'Q';
		 */
		--
		-- If the uses checks the 'completed' checkbox, then the following code is executed
		--
		IF p_flag = '1'
		THEN
			--
			UPDATE t506_returns
			   SET c506_status_fl = 1
				 , c506_last_updated_by = p_userid
				 , c506_last_updated_date = SYSDATE
			 WHERE c506_rma_id = p_raid;

			-- Save "Return" status: Completed
			gm_save_status_details (p_raid, ' ', p_userid, NULL, SYSDATE, '91151', '91100');   --91151 completed; 91100 return

			--
			DELETE		t507_returns_item
				  WHERE c506_rma_id = p_raid AND TRIM (c507_control_number) IS NULL AND c507_status_fl <> 'Q';
		--
		ELSE
			-- if unchecks the 'completed' checkbox, that's mean 'Control'
			--Save "Return" status: control
			gm_save_status_details (p_raid, ' ', p_userid, NULL, SYSDATE, '91155', '91100');   --91155 Control; 91100 return
		END IF;

		-- Changed the status to Payment Received
		-- This is applicable only if its a duplicate order
		-- 3312 maps to Duplicate order
		IF ((v_order_id IS NOT NULL) AND (v_reason = 3312))
		THEN
			UPDATE t501_order
			   SET c501_status_fl = '3'
			 WHERE c501_order_id = v_order_id;
		END IF;

		--IF p_flag <> '1'
		--THEN
		gm_pkg_op_return.gm_op_sav_check_qr (p_raid, v_order_id);
		--END IF;
		p_errmsg	:= 'RA SAVED SUCCESSFULLY';
	--
	END gm_op_sav_return_accp;

/********************************************************************************************
 * Description	: This procedure is used to check the diff between Q and R and
 *				 and create missing R record
 *********************************************************************************************/
--
	PROCEDURE gm_op_sav_check_qr (
		p_raid		 IN   t506_returns.c506_rma_id%TYPE
	  , p_order_id	 IN   t506_returns.c501_order_id%TYPE
	)
	AS
		CURSOR v_returns_cur
		IS
			SELECT pnum, ra.iniqty - ra.rqty qty, 'R' sfl
			  FROM (SELECT	 c205_part_number_id pnum, SUM (DECODE (c507_status_fl, 'Q', c507_item_qty, 0)) iniqty
						   , SUM (DECODE (c507_status_fl, 'R', c507_item_qty, 0)) rqty
						FROM t507_returns_item
					   WHERE c506_rma_id = p_raid
					GROUP BY c205_part_number_id) ra
			 WHERE ra.iniqty - ra.rqty > 0;

		--
		v_price 	   VARCHAR2 (20);
	BEGIN
		--
		FOR rad_val IN v_returns_cur
		LOOP
			IF (p_order_id IS NULL)
			THEN
				v_price 	:= get_part_price (rad_val.pnum, 'E');
			ELSE
				SELECT c502_item_price
				  INTO v_price
				  FROM t502_item_order
				 WHERE c501_order_id = p_order_id AND c205_part_number_id = rad_val.pnum AND ROWNUM = 1;
			END IF;

			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
					   , c507_status_fl
						)
				 VALUES (s507_return_item.NEXTVAL, p_raid, rad_val.pnum, rad_val.qty, v_price
					   , rad_val.sfl
						);
		END LOOP;
	--
	END gm_op_sav_check_qr;

/********************************************************************************************
 * Description	: This procedure is used for creating Child RA's
 *********************************************************************************************/
--
	PROCEDURE gm_op_sav_reconf_accp (
		p_parent_raid	IN		 t506_returns.c506_rma_id%TYPE
	  , p_str			IN		 VARCHAR2
	  , p_retdate		IN		 VARCHAR2
	  , p_flag			IN		 VARCHAR2
	  , p_userid		IN		 t506_returns.c506_last_updated_by%TYPE
	  , p_cra_id		IN		 t506_returns.c506_rma_id%TYPE
	  , p_set_id		IN		 t506_returns.c207_set_id%TYPE
	  , p_raid			OUT 	 VARCHAR2
	)
	AS
--
		v_strlen	   NUMBER := NVL (LENGTH (p_str), 0);
		v_string	   VARCHAR2 (30000) := p_str;
		v_pnum		   VARCHAR2 (20);
		v_qty		   NUMBER;
		v_control	   VARCHAR2 (20);
		v_substring    VARCHAR2 (1000);
		v_price 	   VARCHAR2 (20);
		v_fl		   VARCHAR2 (10);
		v_partnum	   VARCHAR2 (20);
		v_itemtype	   NUMBER;
--
		v_order_id	   t506_returns.c501_order_id%TYPE;
		v_reason	   t506_returns.c506_reason%TYPE;
		v_items 	   t507_returns_item%ROWTYPE;
--
		e_void_exp	   EXCEPTION;	-- Exception for checking if the Void flag is NULL
		e_status_exp   EXCEPTION;
		v_void_fl	   VARCHAR2 (10);
		v_ra_id 	   t506_returns.c506_rma_id%TYPE;
		v_dist_id	   t506_returns.c701_distributor_id%TYPE;
		v_type		   t506_returns.c506_type%TYPE;
		v_status_fl    t506_returns.c506_status_fl%TYPE;
		v_accid 	   t506_returns.c704_account_id%TYPE;
		v_org_qty	   NUMBER;
		v_qty_sum	   NUMBER;
	BEGIN
		--
		SELECT	   c506_void_fl, c506_status_fl
			  INTO v_void_fl, v_status_fl
			  FROM t506_returns
			 WHERE c506_rma_id = p_parent_raid
		FOR UPDATE;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Returns already Voided.
			RAISE e_void_exp;
		END IF;

		IF v_status_fl <> 0
		THEN
			RAISE e_status_exp;
		END IF;

		IF v_status_fl = 1
		THEN
			raise_application_error (-20036, '');
		END IF;

		IF (v_strlen = 0)
		THEN
			RETURN;
		END IF;

		SELECT c501_order_id, c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_status_fl, c704_account_id
		  INTO v_order_id, v_ra_id, v_dist_id, v_type, v_reason, v_status_fl, v_accid
		  FROM t506_returns
		 WHERE c506_rma_id = p_parent_raid;

		SELECT 'GM-CRA-' || s506_return.NEXTVAL
		  INTO v_ra_id
		  FROM DUAL;

		INSERT INTO t506_returns
					(c506_rma_id, c701_distributor_id, c506_type, c506_reason, c506_expected_date
				   , c506_status_fl, c704_account_id, c506_created_by, c506_created_date, c506_parent_rma_id
				   , c207_set_id
					)
			 VALUES (v_ra_id, v_dist_id, v_type, v_reason, TO_DATE (p_retdate, 'mm/dd/yyyy')
				   , DECODE (p_flag, '1', 1, 0), v_accid, p_userid, SYSDATE, p_parent_raid
				   , DECODE (TRIM (p_set_id), NULL, NULL, p_set_id)
					);

		--insert new value into returns table
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_itemtype	:= NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_itemtype	:= TO_NUMBER (v_substring);

				--
			-- If Consignment get the Equity Value
			-- or get the order value
			IF (v_order_id IS NULL)
			THEN
				v_price 	:= get_part_price (v_pnum, 'E');
			ELSE
				SELECT c502_item_price
				  INTO v_price
				  FROM t502_item_order
				 WHERE c501_order_id = v_order_id AND c205_part_number_id = v_pnum AND ROWNUM = 1;
			END IF;

			--
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number, c507_item_qty
					   , c507_item_price, c507_status_fl, c901_type
						)
				 VALUES (s507_return_item.NEXTVAL, v_ra_id, v_pnum, v_control, v_qty
					   , v_price, 'R', v_itemtype
						);

			UPDATE t507_returns_item
			   SET c507_item_qty =
							  DECODE (SIGN (c507_item_qty - TO_NUMBER (v_qty))
									, 1, c507_item_qty - TO_NUMBER (v_qty)
									, 0
									 )
			 WHERE c506_rma_id = p_parent_raid AND c507_status_fl IN ('Q', 'R') AND c205_part_number_id = v_pnum;
		END LOOP;

		INSERT INTO t507_returns_item
					(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
				   , c507_status_fl)
			SELECT s507_return_item.NEXTVAL, v_ra_id, c205_part_number_id, item_qty, c507_item_price, 'Q'
			  FROM (SELECT	 c205_part_number_id, c507_item_price, SUM (c507_item_qty) item_qty
						FROM t507_returns_item
					   WHERE c506_rma_id = v_ra_id AND c507_status_fl = 'R'
					GROUP BY c205_part_number_id, c507_item_price);

		SELECT SUM (c507_item_qty)
		  INTO v_qty_sum
		  FROM t507_returns_item
		 WHERE c506_rma_id = p_parent_raid;

		IF v_qty_sum = 0
		THEN
			UPDATE t506_returns
			   SET c506_status_fl = '2'
				 , c506_last_updated_by = p_userid
				 , c506_last_updated_date = SYSDATE
			 WHERE c506_rma_id = p_parent_raid;

			--
			DELETE FROM t507_returns_item t507
				  WHERE c506_rma_id = p_parent_raid AND c507_status_fl = 'Q';
		END IF;

		p_raid		:= v_ra_id;
	--
	EXCEPTION
		WHEN e_void_exp
		THEN
			-- Raising Application error with msg "Consignment already Voided."
			raise_application_error (-20021, '');
		WHEN e_status_exp
		THEN
			raise_application_error (-20022, '');
	END gm_op_sav_reconf_accp;

--
/********************************************************************
 * Description : This function is used to get the Initiated part Qty
 * for the	given Returns id and part number.
 ********************************************************************/
--
	FUNCTION get_ra_part_init_qty (
		p_raid		IN	 t506_returns.c506_rma_id%TYPE
	  , p_part_id	IN	 t205_part_number.c205_part_number_id%TYPE
	)
		RETURN NUMBER
	IS
		v_item_qty	   NUMBER;
--
	BEGIN
		SELECT c507_item_qty
		  INTO v_item_qty
		  FROM t507_returns_item
		 WHERE c506_rma_id = p_raid AND c507_status_fl IN ('Q') AND c205_part_number_id = p_part_id;

		RETURN v_item_qty;
	EXCEPTION
		WHEN NO_DATA_FOUND
		THEN
			RETURN 0;
	END get_ra_part_init_qty;

/********************************************************************************************
 * Description	: This Procedure is used to fetch the part details to process Returns
 *********************************************************************************************/
--
	PROCEDURE gm_op_fch_return_accp (
		p_raid		   IN		t506_returns.c506_rma_id%TYPE
	  , p_consign_id   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_part_nos	   IN		VARCHAR2
	  , p_part_cur	   OUT		TYPES.cursor_type
	)
	--
	AS
		v_id		   t701_distributor.c701_distributor_id%TYPE;
		v_user_id	   t506_returns.c101_user_id%TYPE;
	BEGIN
		SELECT NVL (c701_distributor_id, c704_account_id), c101_user_id
		  INTO v_id, v_user_id
		  FROM t506_returns
		 WHERE c506_rma_id = p_raid;

		-- To assign part information
		my_context.set_my_inlist_ctx (p_part_nos);

		OPEN p_part_cur
		 FOR
			 SELECT   c205_part_number_id pnum, get_partnum_desc (c205_part_number_id) pdesc, SUM (c507_item_qty) qty
					, get_csg_part_qty_for_dist (v_id, v_user_id, c205_part_number_id) cons_qty
					, gm_pkg_op_return.get_ra_part_init_qty (p_raid, c205_part_number_id) iqty
					, DECODE (p_consign_id
							, '', c507_control_number
							, get_control_number (p_consign_id, c205_part_number_id, c507_item_qty)
							 ) cnum
					, TO_CHAR (c901_type) itype
				 FROM t507_returns_item
				WHERE c506_rma_id = p_raid AND c507_status_fl IN ('C', 'R', 'W')
			 GROUP BY c205_part_number_id, c507_control_number, c507_item_qty, c901_type
			 UNION
			 SELECT   t205.c205_part_number_id pnum, get_partnum_desc (t205.c205_part_number_id) pdesc, 0 qty
					, get_csg_part_qty_for_dist (v_id, v_user_id, t205.c205_part_number_id) cons_qty, 0 iqty, '' cnum
					, '' itype
				 FROM t205_part_number t205
				WHERE t205.c205_part_number_id IN (SELECT token
													 FROM v_in_list)
				  AND t205.c205_part_number_id NOT IN (SELECT x.c205_part_number_id
														 FROM t507_returns_item x
														WHERE x.c506_rma_id = p_raid AND x.c507_status_fl <> 'Q')
			 ORDER BY pnum;
	END gm_op_fch_return_accp;

/********************************************************************************************
 * Description	: This Procedure is used to fetch the part details to reconfigure set
 *********************************************************************************************/
--
	PROCEDURE gm_op_fch_reconf_accp (
		p_distid	   IN		t701_distributor.c701_distributor_id%TYPE
	  , p_raid		   IN		t506_returns.c506_rma_id%TYPE
	  , p_consign_id   IN		t504_consignment.c504_consignment_id%TYPE
	  , p_part_nos	   IN		VARCHAR2
	  , p_set_id	   IN		t207_set_master.c207_set_id%TYPE
	  , p_part_cur	   OUT		TYPES.cursor_type
	)
	--
	AS
		e_void_exp	   EXCEPTION;
		-- Exception for checking if the Void flag is NULL
		e_status_exp   EXCEPTION;
		v_void_fl	   VARCHAR2 (10);
		v_status_fl    t506_returns.c506_status_fl%TYPE;
		v_qty_sum	   NUMBER;
		v_id		   t701_distributor.c701_distributor_id%TYPE;
		v_user_id	   t506_returns.c101_user_id%TYPE;
	BEGIN
		SELECT NVL (c701_distributor_id, c704_account_id), c101_user_id, c506_void_fl, c506_status_fl
		  INTO v_id, v_user_id, v_void_fl, v_status_fl
		  FROM t506_returns
		 WHERE c506_rma_id = p_raid;

		SELECT SUM (c507_item_qty)
		  INTO v_qty_sum
		  FROM t507_returns_item
		 WHERE c506_rma_id = p_raid;

		IF v_void_fl IS NOT NULL
		THEN
			-- Error message is Returns already Voided.
			RAISE e_void_exp;
		END IF;

		IF v_status_fl <> 0 AND v_qty_sum = 0
		THEN
			RAISE e_status_exp;
		END IF;

		-- To assign part information
		my_context.set_my_inlist_ctx (p_part_nos);
		OPEN p_part_cur FOR
            SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
                   , get_set_qty (p_set_id, t205.c205_part_number_id) qty
                   , get_csg_part_qty_for_dist (v_id, v_user_id, t205.c205_part_number_id) cons_qty
                   , gm_pkg_op_return.get_ra_part_init_qty (t507.c506_rma_id, t507.c205_part_number_id) iqty
                   , c507_item_qty pending_qty
                   , DECODE (p_consign_id
                           , '', t507.c507_control_number
                           , get_control_number (p_consign_id
                                               , t205.c205_part_number_id
                                               , get_set_qty (p_set_id, t205.c205_part_number_id)
                                                )
                            ) cnum
                FROM t507_returns_item t507, t205_part_number t205
               WHERE t507.c506_rma_id = p_raid
                 AND t507.c507_status_fl = 'R'
                 AND t507.c205_part_number_id = t205.c205_part_number_id
                 AND (   t205.c205_part_number_id IN (SELECT c205_part_number_id
                                                        FROM t208_set_details
                                                       WHERE c207_set_id = p_set_id)
                      OR t205.c205_part_number_id IN (SELECT token
                                                        FROM v_in_list)
                     )
            UNION
            SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc, 0 qty, 0 cons_qty, 0 iqty
                   , 0 pending_qty, '' cnum
                FROM t205_part_number t205, t208_set_details t208
               WHERE t208.c205_part_number_id = t205.c205_part_number_id AND t208.c207_set_id = p_set_id
               AND t205.c205_part_number_id NOT IN (SELECT x.c205_part_number_id
                                                        FROM t507_returns_item x
                                                       WHERE x.c506_rma_id = p_raid AND x.c507_status_fl <> 'Q')
            UNION
            SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc, 0 qty
                   , get_csg_part_qty_for_dist (v_id, v_user_id, t205.c205_part_num_desc) cons_qty, 0 iqty
                   , 0 pending_qty, '' cnum
                FROM t205_part_number t205
               WHERE t205.c205_part_number_id IN (SELECT *
                                                    FROM v_in_list)
                 AND t205.c205_part_number_id NOT IN (SELECT x.c205_part_number_id
                                                        FROM t507_returns_item x
                                                       WHERE x.c506_rma_id = p_raid AND x.c507_status_fl <> 'Q')
            ORDER BY pnum;

	EXCEPTION
		WHEN e_void_exp
		THEN
			-- Raising Application error with msg "Consignment already Voided."
			raise_application_error (-20021, '');
		WHEN e_status_exp
		THEN
			raise_application_error (-20022, '');
	END gm_op_fch_reconf_accp;

--
/********************************************************************************************
 * Description	: This Procedure is used to fetch the details of a RA
 * (To display in the Credit RA Screen)
 *********************************************************************************************/
--
	PROCEDURE gm_op_fch_return_forcredit (
		p_raid		 IN 	  t506_returns.c506_rma_id%TYPE
	  , p_part_cur	 OUT	  TYPES.cursor_type
	)
--
	AS
		v_id		   t701_distributor.c701_distributor_id%TYPE;
		v_user_id	   t506_returns.c101_user_id%TYPE;
	BEGIN
		SELECT NVL (c701_distributor_id, c704_account_id), c101_user_id
		  INTO v_id, v_user_id
		  FROM t506_returns
		 WHERE c506_rma_id = p_raid;

		OPEN p_part_cur
		 FOR
			 SELECT   t205.c205_part_number_id pnum, t205.c205_part_num_desc pdesc
					, get_csg_part_qty_for_dist (v_id, v_user_id, t205.c205_part_number_id) cons_qty
					, SUM (DECODE (c507_status_fl, 'Q', c507_item_qty, 0)) init_qty
					, SUM (DECODE (c507_status_fl
								 , 'Q', 0
								 , DECODE (TRIM (t507.c507_control_number), NULL, 0, c507_item_qty)
								  )
						  ) return_qty
				 FROM t507_returns_item t507, t205_part_number t205
				WHERE c506_rma_id = p_raid AND t205.c205_part_number_id = t507.c205_part_number_id
			 GROUP BY t205.c205_part_number_id, t205.c205_part_num_desc;
	END gm_op_fch_return_forcredit;

--
	--
/********************************************************************************************
 * Description	: This Procedure is used to credit a RA
 *********************************************************************************************/
--
	PROCEDURE gm_op_sav_return_forcredit (
		p_raid		   IN	t506_returns.c506_rma_id%TYPE
	  , p_creditdate   IN	VARCHAR2
	  , p_userid	   IN	t506_returns.c506_last_updated_by%TYPE
	)
--
	AS
		v_dist_id	   t506_returns.c701_distributor_id%TYPE;
		v_status_fl    t506_returns.c506_status_fl%TYPE;
		v_inventory_fl t506_returns.c506_status_fl%TYPE;
		v_msg		   VARCHAR2 (1000);
		v_reason	   t506_returns.c506_reason%TYPE;
		v_order_id	   t506_returns.c501_order_id%TYPE;
		v_dist_type    t701_distributor.c901_distributor_type%TYPE;
		v_upd_inv_type VARCHAR2 (4) := 'R';
	BEGIN
		SELECT	   c506_status_fl, c506_update_inv_fl, c701_distributor_id, c506_reason, c501_order_id
				 , get_distributor_type (c701_distributor_id)
			  INTO v_status_fl, v_inventory_fl, v_dist_id, v_reason, v_order_id
				 , v_dist_type
			  FROM t506_returns
			 WHERE c506_rma_id = p_raid
		FOR UPDATE;

		--gm_procedure_log('DISTID',v_dist_id);
		IF v_status_fl <> 1
		THEN
			raise_application_error (-20024, '');
		END IF;

		IF (TRIM (v_dist_id) IS NOT NULL)
		THEN
			-- Lock the distributor for update
			SELECT	   c701_distributor_id
				  INTO v_dist_id
				  FROM t701_distributor
				 WHERE c701_distributor_id = v_dist_id
			FOR UPDATE;
		END IF;

		-- gm_procedure_log('Order ID',v_order_id);
		-- To remove all the missing R record
		DELETE FROM t507_returns_item t507
			  WHERE c506_rma_id = p_raid AND c507_status_fl = 'R' AND TRIM (c507_control_number) IS NULL;

		-- Update the C507_STATUS_FL to C (Credited)
		UPDATE t507_returns_item t507
		   SET c507_status_fl = 'C'
		 WHERE c506_rma_id = p_raid AND c507_status_fl = 'R' AND TRIM (c507_control_number) IS NOT NULL;

		-- save credit order
		IF (v_reason = 3316 OR v_reason = 3317)
		THEN
			gm_save_credit_order_swap_type (p_raid, v_reason, v_order_id, SYSDATE, '', p_userid);
		END IF;

		-- To mark the M Record
		INSERT INTO t507_returns_item
					(c507_returns_item_id, c507_item_qty, c507_item_price, c205_part_number_id	 --, c507_control_number
				   , c506_rma_id, c507_status_fl)
			SELECT s507_return_item.NEXTVAL, iqty - rqty, iprice, pnum, rma_id, status
			  FROM (SELECT	 c506_rma_id rma_id, c205_part_number_id pnum, 0 iprice
						   , SUM (DECODE (c507_status_fl, 'Q', c507_item_qty, 0)) iqty
						   , SUM (DECODE (c507_status_fl, 'C', c507_item_qty, 0)) rqty	 --, c507_control_number cnum
						   , 'M' status
						FROM t507_returns_item t507
					   WHERE c506_rma_id = p_raid
					GROUP BY c506_rma_id, c205_part_number_id)
			 WHERE (iqty - rqty) > 0;

		-- delete the Q record
		DELETE FROM t507_returns_item t507
			  WHERE c506_rma_id = p_raid AND c507_status_fl = 'Q';

		-- update the returns for the RA
		UPDATE t506_returns
		   SET c506_credit_date = TO_DATE (p_creditdate, 'mm/dd/yyyy')
			 -- Setting reprocess date since the Loaner to Consign doesnt need to be reprocessed: James 3/4/09
		,	   c501_reprocess_date = DECODE (v_reason, 3317, TO_DATE (p_creditdate, 'mm/dd/yyyy'), NULL)
			 , c506_status_fl = 2
			 , c506_last_updated_by = p_userid
			 , c506_last_updated_date = SYSDATE
		 WHERE c506_rma_id = p_raid;

		-- Save Excess Quantity
		IF v_dist_id IS NOT NULL
		THEN
			gm_pkg_op_excess_return.gm_op_save_excess_qty (p_raid, p_userid, v_dist_id);
		END IF;

		-- For ICT returns
		IF v_dist_type = 70104 OR v_dist_type = 70103
		THEN
			UPDATE t506_returns
			   SET c506_type = 3307
			 WHERE c506_rma_id = p_raid;

			v_upd_inv_type := 'ICR';
		END IF;

		-- code to perform posting
		--
		IF v_inventory_fl IS NULL
		THEN
			gm_update_inventory (p_raid, v_upd_inv_type, '', p_userid, v_msg);

			--
			UPDATE t506_returns
			   SET c506_update_inv_fl = '1'
			 WHERE c506_rma_id = p_raid AND c506_void_fl IS NULL;
		--
		END IF;

		-- For ICT returns FD or ICT
		IF v_dist_type = 70104 OR v_dist_type = 70103
		THEN
			gm_pkg_op_ict.gm_po_sav_ict_returns (p_raid, v_dist_id, p_userid, v_dist_type);
		END IF;

		-- save "Return" status: Credit
		gm_save_status_details (p_raid, ' ', p_userid, NULL, SYSDATE, '91152', '91100');
	END gm_op_sav_return_forcredit;

/********************************************************************************************
 * Description	: This Procedure will be called to initiate order return
 *********************************************************************************************/
	PROCEDURE gm_op_sav_order_return (
		p_type			   IN		t506_returns.c506_type%TYPE
	  , p_reason		   IN		t506_returns.c506_reason%TYPE
	  , p_distid		   IN		VARCHAR2
	  , p_comments		   IN		t506_returns.c506_comments%TYPE
	  , p_expdate		   IN		VARCHAR2
	  , p_orderid		   IN		t506_returns.c501_order_id%TYPE
	  , p_userid		   IN		t506_returns.c506_created_by%TYPE
	  , p_parent_orderid   IN		t506_returns.c501_order_id%TYPE
	  , p_empname		   IN		t101_user.c101_user_id%TYPE
	  , p_str			   IN		VARCHAR2
	  , p_raid			   OUT		VARCHAR2
	)
	AS
--
		v_id		   NUMBER;
		v_string	   VARCHAR2 (30000);
		v_id_string    VARCHAR2 (20);
		v_price 	   VARCHAR2 (20);
		v_date		   VARCHAR2 (10);
		v_empname	   NUMBER;
		v_status	   CHAR (1) := 0;
		v_ret_date	   DATE;
		v_rep_date	   DATE;
		-- Loop variable declaration
		v_strlen	   NUMBER := 0;
		v_substring    VARCHAR2 (1000);
		v_pnum		   VARCHAR2 (20);
		v_qty		   NUMBER;
		v_control	   VARCHAR2 (20);
		v_orgcontrol   VARCHAR2 (20);
		v_itemtype	   NUMBER;
		v_order_id	   t506_returns.c501_order_id%TYPE;
--
	BEGIN
		--
		SELECT 'GM-RA-' || s506_return.NEXTVAL
		  INTO p_raid
		  FROM DUAL;

		--
		IF LENGTH (p_expdate) < 1
		THEN
			v_date		:= TO_CHAR (SYSDATE + 4, 'mm/dd/yyyy');
		ELSE
			v_date		:= p_expdate;
		END IF;

		--
		-- Mark the order as duplicate order or as Item Return consignment
		gm_order_return (p_orderid, p_type, p_reason, p_parent_orderid);

		--
		INSERT INTO t506_returns
					(c506_rma_id, c704_account_id, c501_order_id, c506_type, c506_reason, c506_comments
				   , c506_expected_date, c506_status_fl, c506_created_by, c506_created_date, c506_return_date
				   , c501_reprocess_date
					)
			 VALUES (p_raid, p_distid, p_orderid, p_type, p_reason, p_comments
				   , TO_DATE (v_date, 'mm/dd/yyyy'), v_status, p_userid, SYSDATE, v_ret_date
				   , v_rep_date
					);

		--
		-- Load item information
		v_string	:= p_str;
		v_strlen	:= NVL (LENGTH (p_str), 0);

		--
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			--
			v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_pnum		:= NULL;
			v_qty		:= NULL;
			v_control	:= NULL;
			v_orgcontrol := NULL;
			v_itemtype	:= NULL;
			v_pnum		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_qty		:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_control	:= SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_orgcontrol := SUBSTR (v_substring, 1, INSTR (v_substring, ',') - 1);
			v_substring := SUBSTR (v_substring, INSTR (v_substring, ',') + 1);
			v_itemtype	:= TO_NUMBER (v_substring);

			--
			SELECT c502_item_price
			  INTO v_price
			  FROM t502_item_order
			 WHERE c501_order_id = p_orderid
			   AND c205_part_number_id = v_pnum
			   AND c502_control_number = v_orgcontrol
			   AND ROWNUM = 1;

			--
			INSERT INTO t507_returns_item
						(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_control_number, c507_item_qty
					   , c507_item_price, c507_status_fl, c507_org_control_number, c901_type
						)
				 VALUES (s507_return_item.NEXTVAL, p_raid, v_pnum, v_control, v_qty
					   , v_price, 'R', NVL (v_orgcontrol, '-'), v_itemtype
						);
		END LOOP;

		INSERT INTO t507_returns_item
					(c507_returns_item_id, c506_rma_id, c205_part_number_id, c507_item_qty, c507_item_price
				   , c507_status_fl)
			SELECT s507_return_item.NEXTVAL, p_raid, c205_part_number_id, item_qty, c507_item_price, 'Q'
			  FROM (SELECT	 c205_part_number_id, 0 c507_item_price, SUM (c507_item_qty) item_qty
						FROM t507_returns_item
					   WHERE c506_rma_id = p_raid AND c507_status_fl = 'R'
					GROUP BY c205_part_number_id);

		SELECT COUNT (1)
		  INTO v_qty
		  FROM (SELECT *
				  FROM t507_returns_item
				 WHERE c506_rma_id = p_raid AND c507_status_fl = 'R');

--
		-- 3317 means Parts Swap (Loaner to Consign)
		-- used by loaner program
		IF p_reason = '3317'
		THEN
			UPDATE t506_returns
			   SET c506_status_fl = 1
				 , c506_return_date = SYSDATE
			 WHERE c506_rma_id = p_raid;

			--
			UPDATE t507_returns_item
			   SET c507_status_fl = 'C'
				 , c507_control_number = 'NOC#'
			 WHERE c506_rma_id = p_raid AND c507_status_fl = 'R';
		END IF;
	END gm_op_sav_order_return;
--
END gm_pkg_op_return;
/
