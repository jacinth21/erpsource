/* Formatted on 2009/07/22 18:11 (Formatter Plus v4.8.0) */
--@"C:\database\Packages\Common\gm_pkg_cm_rules.bdy";

CREATE OR REPLACE PACKAGE BODY gm_pkg_cm_rules
IS
/*******************************************************
	* Description : Procedure to save rule details
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_sav_rule_condition (
		p_rulenm		  IN	   t9700_rule.c9700_rule_name%TYPE
	  , p_expdt 		  IN	   VARCHAR2
	  , p_initiatedby	  IN	   t9700_rule.c9700_initiated_by%TYPE
	  , p_initiateddt	  IN	   VARCHAR2
	  , p_actvfl		  IN	   t9700_rule.c9700_active_fl%TYPE
	  , p_emlnotifyfl	  IN	   t9700_rule.c9700_email_notify_fl%TYPE
	  , p_comment		  IN	   t9700_rule.c9700_comment%TYPE
	  , p_inputstrtrans   IN	   VARCHAR2
	  , p_inputstrcond	  IN	   VARCHAR2
	  , p_userid		  IN	   t9700_rule.c9700_last_updated_by%TYPE
	  , p_ruleid		  IN OUT   t9700_rule.c9700_rule_id%TYPE
	)
	AS
		v_cnt		   NUMBER;
		v_select_cnt   NUMBER;
		v_ruleid	   t9700_rule.c9700_rule_id%TYPE;
		v_string	   VARCHAR2 (4000) := p_inputstrcond;
		v_substr	   VARCHAR2 (4000);
		v_value 	   VARCHAR2 (4000);
		v_cond		   NUMBER;
		v_operator	   NUMBER;
		v_join		   NUMBER;
		v_initiatedby  NUMBER;
		v_txn_id	   t9703_rule_transaction.c901_txn_id%TYPE;
		v_txn_log	   t941_audit_trail_log.c941_value%TYPE;
		v_inputstrtrans VARCHAR2 (4000);
		v_stringtrans  VARCHAR2 (4000) := p_inputstrtrans;
		v_condition_string VARCHAR2 (4000);
		v_condition_log VARCHAR2 (4000);
	BEGIN
		my_context.set_my_inlist_ctx (p_inputstrtrans);
		v_ruleid	:= p_ruleid;
	
		SELECT COUNT (token)
		  INTO v_cnt
		  FROM v_in_list
		 WHERE token IS NOT NULL;
		UPDATE t9700_rule
		   SET c9700_rule_name = p_rulenm
			 , c9700_expiry_date = TO_DATE (p_expdt, 'MM/DD/YYYY')
			 , c9700_active_fl = p_actvfl
			 , c9700_last_updated_by = p_userid
			 , c9700_last_updated_date = SYSDATE
			 , c9700_email_notify_fl = p_emlnotifyfl
			 , c9700_comment = p_comment
		 WHERE c9700_rule_id = v_ruleid;
		IF (SQL%ROWCOUNT = 0)
		THEN		 
			SELECT s9700_rule.NEXTVAL
			  INTO v_ruleid
			  FROM DUAL;

			INSERT INTO t9700_rule
						(c9700_rule_id, c9700_rule_name, c9700_expiry_date, c9700_initiated_by, c9700_initiated_date
					   , c9700_active_fl, c9700_created_by, c9700_created_date, c9700_email_notify_fl, c9700_comment
						)
				 VALUES (v_ruleid, p_rulenm, TO_DATE (p_expdt, 'MM/DD/YYYY'), p_initiatedby, TRUNC (SYSDATE)
					   , p_actvfl, p_userid, SYSDATE, p_emlnotifyfl, p_comment
						);
		END IF;
		
		DELETE FROM t9703_rule_transaction
			  WHERE c9700_rule_id = v_ruleid;
		
		IF v_cnt > 0
		THEN
		
			INSERT INTO t9703_rule_transaction
						(c9703_rule_transaction_id, c9703_created_by, c9703_created_date, c9700_rule_id, c901_txn_id)
				SELECT s9703_rule_transaction.NEXTVAL, p_userid, SYSDATE, v_ruleid, token
				  FROM v_in_list
				 WHERE token IS NOT NULL;
		END IF;

------------------------------------------
		WHILE INSTR (v_stringtrans, ',') <> 0
		LOOP
			--
			v_txn_id	:= NULL;
			v_substr	:= SUBSTR (v_stringtrans, 1, INSTR (v_stringtrans, ',') - 1);
			v_stringtrans := SUBSTR (v_stringtrans, INSTR (v_stringtrans, ',') + 1);
			v_txn_id	:= TO_NUMBER (v_substr);
			v_inputstrtrans := v_inputstrtrans || get_trans_name (v_txn_id) || ',';
		END LOOP;
		BEGIN
			SELECT t941.c941_value
			  INTO v_txn_log
			  FROM t941_audit_trail_log t941
			 WHERE t941.c941_audit_trail_log_id IN (SELECT MAX (c941_audit_trail_log_id)
													  FROM t941_audit_trail_log
													 WHERE c941_ref_id = v_ruleid AND c940_audit_trail_id = 1015);
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (v_ruleid, v_inputstrtrans, 1015, p_userid, SYSDATE);
			 
				UPDATE t9700_rule t9700
				   SET c9700_txn_history_fl = 'Y'
				 WHERE t9700.c9700_rule_id = v_ruleid;

				v_txn_log	:= v_inputstrtrans;
		END;
	 	IF v_txn_log <> v_inputstrtrans
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (v_ruleid, v_inputstrtrans, 1015, p_userid, SYSDATE);
		END IF;

------------------------------------------------
		DELETE FROM t9702_rule_condition
			  WHERE c9700_rule_id = v_ruleid;

		/*need to write a logic to insert in t9702*/
		WHILE INSTR (v_string, '|') <> 0
		LOOP
			v_substr	:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
			v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
			v_cond		:= SUBSTR (v_substr, 1, INSTR (v_substr, '^') - 1);
			v_substr	:= SUBSTR (v_substr, INSTR (v_substr, '^') + 1);
			v_operator	:= SUBSTR (v_substr, 1, INSTR (v_substr, '^') - 1);
			v_substr	:= SUBSTR (v_substr, INSTR (v_substr, '^') + 1);
			v_value 	:= SUBSTR (v_substr, 1, INSTR (v_substr, '^') - 1);
			v_substr	:= SUBSTR (v_substr, INSTR (v_substr, '^') + 1);
			v_join		:= SUBSTR (v_substr, 1, INSTR (v_substr, '^') - 1);

			INSERT INTO t9702_rule_condition
						(c9702_rule_condition_id, c9702_condition_value, c9702_void_fl, c9702_created_by
					   , c9702_created_date, c9700_rule_id, c9701_condition_id, c901_operator_id, c901_condition_type
						)
				 VALUES (s9702_rule_condition.NEXTVAL, v_value, NULL, p_userid
					   , SYSDATE, v_ruleid, v_cond, v_operator, v_join
						);

			v_condition_string :=
				   v_condition_string
				|| gm_pkg_cm_rule_conditions.get_condition_name (v_cond)
				|| ' ' || get_code_name (v_operator) || ' '
				|| v_value
				|| ' AND ';
		END LOOP;
		
------------------------------------------------------------------------------
		BEGIN
			SELECT t941.c941_value
			  INTO v_condition_log
			  FROM t941_audit_trail_log t941
			 WHERE t941.c941_audit_trail_log_id IN (SELECT MAX (c941_audit_trail_log_id)
													  FROM t941_audit_trail_log
													 WHERE c941_ref_id = v_ruleid AND c940_audit_trail_id = 1014);
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN 
				gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (v_ruleid, v_condition_string, 1014, p_userid, SYSDATE);

				UPDATE t9700_rule t9700
				   SET c9700_condition_history_fl = 'Y'
				 WHERE t9700.c9700_rule_id = v_ruleid;

				v_condition_log := v_condition_string;
		END;
		IF v_condition_log != v_condition_string
		THEN
			gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (v_ruleid, v_condition_string, 1014, p_userid, SYSDATE);
		END IF;

------------------------------------------------------------------------------
		UPDATE t9700_rule
		   SET c9700_rule_level =
						 (SELECT MAX (t9701.c9701_condition_level)
							FROM t9701_condition t9701, t9702_rule_condition t9702
						   WHERE t9702.c9701_condition_id = t9701.c9701_condition_id AND t9702.c9700_rule_id = v_ruleid)
		 WHERE c9700_rule_id = v_ruleid;

		p_ruleid	:= v_ruleid;

		BEGIN
			/*
				SELECT COUNT(ID) into v_select_cnt FROM
				(SELECT ID, SUBSTR(CONDITION, NVL(LENGTH(PRIOR CONDITION)+2,2)) CON
				FROM
				(
				SELECT ID, MAX(SYS_CONNECT_BY_PATH (CON, ';')) CONDITION, ROW_NUMBER () OVER (ORDER BY ID) AS curr_t
									FROM
									(SELECT ID, CONDITION || OPR || VALUE CON, ROW_NUMBER () OVER (ORDER BY ID, CONDITION || OPR || VALUE) AS curr
									 FROM (select C9702_RULE_CONDITION_ID ID,C9701_CONDITION_ID CONDITION, C901_OPERATOR_ID OPR, C9702_CONDITION_VALUE VALUE from t9702_rule_condition where c9702_void_fl is null) rinfo
									 )
								GROUP BY ID
								 CONNECT BY curr - 1 = PRIOR curr
								START WITH curr = 1
								)
				CONNECT BY curr_t - 1 = PRIOR curr_t
				START WITH curr_t = 1)
				GROUP BY CON
				HAVING COUNT(ID) > 1;
				*/
			SELECT	 COUNT (ID)
				INTO v_select_cnt
				FROM (SELECT	 ID, SUBSTR (condition, NVL (LENGTH (PRIOR  condition) + 2, 2)) con
							FROM (SELECT	 ID, MAX (SYS_CONNECT_BY_PATH (con, ';')) condition
										   , ROW_NUMBER () OVER (ORDER BY ID) AS curr_t
										FROM (SELECT ID, condition || opr || VALUE con
												   , ROW_NUMBER () OVER (ORDER BY ID
													, condition || opr || VALUE) AS curr
												FROM (SELECT c9700_rule_id ID, c9701_condition_id condition
														   , c901_operator_id opr, c9702_condition_value VALUE
														FROM t9702_rule_condition
													   WHERE c9702_void_fl IS NULL) rinfo)
									GROUP BY ID
								  CONNECT BY curr - 1 = PRIOR curr
								  START WITH curr = 1)
					  CONNECT BY curr_t - 1 = PRIOR curr_t
					  START WITH curr_t = 1)
			GROUP BY con
			  HAVING COUNT (ID) > 1;

			IF v_select_cnt > 1
			THEN
				raise_application_error (-20158, '');
			END IF;
		EXCEPTION
			WHEN NO_DATA_FOUND
			THEN
				p_ruleid	:= v_ruleid;
		END;
	END gm_sav_rule_condition;

	/*******************************************************
	* Description : Procedure to fetch rule details
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_rule (
		p_ruleid		 IN 	  t9700_rule.c9700_rule_id%TYPE
	  , p_out_rule_cur	 OUT	  TYPES.cursor_type
	  , p_strtrans		 OUT	  VARCHAR2
	)
	AS
		v_str		   VARCHAR2 (2000);

		CURSOR c_translist
		IS
			SELECT t9703.c9703_rule_transaction_id ruletransactionid, t9703.c901_txn_id transactionid
			  FROM t9703_rule_transaction t9703
			 WHERE t9703.c9700_rule_id = p_ruleid AND t9703.c9703_void_fl IS NULL;
	BEGIN
		OPEN p_out_rule_cur
		 FOR
			 SELECT t9700.c9700_rule_id ruleid, t9700.c9700_rule_name rulename
				  , TO_CHAR (t9700.c9700_expiry_date, 'MM/DD/YYYY') expirydate
				  , get_user_name (t9700.c9700_initiated_by) initiatedby
				  , TO_CHAR (t9700.c9700_initiated_date, 'MM/DD/YYYY') initiateddate, t9700.c9700_active_fl activefl
				  , c9700_email_notify_fl ruleemailnotify, c9700_comment rulecomment
				  , c9700_expiry_history_fl exp_hisfl, c9700_active_history_fl act_hisfl
				  , c9700_email_notify_history_fl email_hisfl, c9700_condition_history_fl con_hisfl
				  , c9700_txn_history_fl txn_hisfl
			   FROM t9700_rule t9700
			  WHERE t9700.c9700_rule_id = p_ruleid AND t9700.c9700_void_fl IS NULL;

		FOR rec IN c_translist
		LOOP
			v_str		:= v_str || rec.transactionid || ',';
		END LOOP;

		p_strtrans	:= v_str;
	END gm_fch_rule;

	/*******************************************************
	* Description : Procedure to fetch rule conditions details
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_rulecondition (
		p_ruleid			  IN	   t9700_rule.c9700_rule_id%TYPE
	  , p_out_condition_cur   OUT	   TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_condition_cur
		 FOR
			 SELECT t9702.c9702_rule_condition_id ruleconditionid, t9702.c9702_condition_value conditionval
				  , t9702.c9701_condition_id conditionid, t9702.c901_operator_id operatorid
				  , get_code_name (t9702.c901_operator_id) operatorname, t9702.c901_condition_type jointype
				  , get_code_name (t9702.c901_condition_type) joinname
			   FROM t9702_rule_condition t9702
			  WHERE t9702.c9700_rule_id = DECODE (p_ruleid, 0, t9702.c9700_rule_id, p_ruleid)
				AND t9702.c9702_void_fl IS NULL;
	END gm_fch_rulecondition;

	/*******************************************************
		* Description : Procedure to save consequences
		* Author		: Ritesh Shah
		*******************************************************/
	PROCEDURE gm_sav_consequences (
		p_ruleid	   IN	t9700_rule.c9700_rule_id%TYPE
	  , p_ruleconsid   IN	t9704_rule_consequence.c9704_rule_consequence_id%TYPE
	  , p_consgrpid    IN	t9704_rule_consequence.c901_consequence_id%TYPE
	  , p_inputstr	   IN	VARCHAR2
	  , p_userid	   IN	t9704_rule_consequence.c9704_created_by%TYPE
	)
	AS
		CURSOR cons_cur
		IS
			SELECT token, tokenii
			  FROM v_double_in_list
			 WHERE token IS NOT NULL;

		v_seq_no	   NUMBER;
		v_rulecons_id  NUMBER;
		v_initiatedby  NUMBER;
		v_seq_detail_no NUMBER;
		v_previous_seq NUMBER;
		v_string	   VARCHAR2 (4000) := p_inputstr;
		v_substring    VARCHAR2 (4000);
		v_type		   VARCHAR2 (20);
		v_hldtxn	   t941_audit_trail_log.c941_value%TYPE;
		v_hold_txn_log t941_audit_trail_log.c941_value%TYPE;
		v_ruleid	   NUMBER;
	BEGIN
		my_context.set_double_inlist_ctx (p_inputstr, ';');

-----------------------------------------------------------------------
		IF p_consgrpid = 91346
		THEN
			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substring := SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_type		:= NULL;
				v_hldtxn	:= NULL;
				v_type		:= SUBSTR (v_substring, 1, INSTR (v_substring, ';') - 1);
				v_substring := SUBSTR (v_substring, INSTR (v_substring, ';') + 1);

				IF (v_type = '91352')
				THEN
					v_hldtxn	:= v_substring;
				END IF;
			END LOOP;

			BEGIN
				SELECT t941.c941_value
				  INTO v_hold_txn_log
				  FROM t941_audit_trail_log t941
				 WHERE t941.c941_audit_trail_log_id = (SELECT MAX (c941_audit_trail_log_id)
														 FROM t941_audit_trail_log
														WHERE c941_ref_id = p_ruleid AND c940_audit_trail_id = 1016);
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (p_ruleid, v_hldtxn, 1016, p_userid, SYSDATE);

					UPDATE t9700_rule t9700
					   SET c9700_hold_txn_history_fl = 'Y'
					 WHERE t9700.c9700_rule_id = p_ruleid;

					v_hold_txn_log := v_hldtxn;
			END;

			IF v_hold_txn_log != v_hldtxn
			THEN
				gm_pkg_cm_audit_trail.gm_cm_sav_audit_log (p_ruleid, v_hldtxn, 1016, p_userid, SYSDATE);
			END IF;
		END IF;

		DELETE FROM t9705_consequence_details
			  WHERE c9704_rule_consequence_id = p_ruleconsid;

		DELETE FROM t9704_rule_consequence
			  WHERE c9700_rule_id = p_ruleid AND c9704_rule_consequence_id = p_ruleconsid;

		SELECT s9704_rule_consequence.NEXTVAL
		  INTO v_seq_no
		  FROM DUAL;

		INSERT INTO t9704_rule_consequence
					(c9704_rule_consequence_id, c9704_created_by, c9704_created_date, c9700_rule_id, c901_consequence_id
					)
			 VALUES (v_seq_no, p_userid, SYSDATE, p_ruleid, p_consgrpid
					);

		UPDATE t9700_rule
		   SET c9700_last_updated_by = p_userid
			 , c9700_last_updated_date = SYSDATE
		 WHERE c9700_rule_id = p_ruleid;

		FOR current_row IN cons_cur
		LOOP
			SELECT s9705_consequence_details.NEXTVAL
			  INTO v_seq_detail_no
			  FROM DUAL;

			IF (p_consgrpid = 91347)
			THEN
				IF (current_row.token = 91356)
				THEN
					INSERT INTO t903_upload_file_list
								(c903_upload_file_list, c903_ref_id, c903_file_name, c903_created_by
							   , c903_created_date, c901_ref_type
								)
						 VALUES (s903_upload_file_list.NEXTVAL, v_seq_detail_no, current_row.tokenii, p_userid
							   , SYSDATE, 91183
								);

					v_previous_seq := v_seq_detail_no;
				END IF;

				IF (current_row.token = 91357)
				THEN
					INSERT INTO t9705_consequence_details
								(c9705_consequence_details_id, c9705_consequence_value, c9705_created_by
							   , c9705_created_date, c9704_rule_consequence_id, c901_param_id
								)
						 VALUES (v_previous_seq, current_row.tokenii, p_userid
							   , SYSDATE, v_seq_no, current_row.token
								);
				END IF;
			END IF;

			IF (p_consgrpid <> 91347)
			THEN
				INSERT INTO t9705_consequence_details
							(c9705_consequence_details_id, c9705_consequence_value, c9705_created_by
						   , c9705_created_date, c9704_rule_consequence_id, c901_param_id
							)
					 VALUES (v_seq_detail_no, current_row.tokenii, p_userid
						   , SYSDATE, v_seq_no, current_row.token
							);
			END IF;
		END LOOP;
	END gm_sav_consequences;

	/*******************************************************
	* Description : Procedure to fetch consequences
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_consequences (
		p_ruleid		 IN 	  t9700_rule.c9700_rule_id%TYPE
	  , p_consgrpid 	 IN 	  t9704_rule_consequence.c901_consequence_id%TYPE
	  , p_out_cons_cur	 OUT	  TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_cons_cur
		 FOR
			 SELECT t9705.c9705_consequence_details_id seqid, t9705.c9704_rule_consequence_id ruleseqid
				  , t9705.c901_param_id paramid, t9705.c9705_consequence_value consvalue
				  , DECODE (t9705.c901_param_id, 91352, t9700.c9700_hold_txn_history_fl, 'N') hld_txn_history_fl
			   FROM t9705_consequence_details t9705, t9704_rule_consequence t9704, t9700_rule t9700
			  WHERE t9704.c901_consequence_id = p_consgrpid
				AND t9704.c9700_rule_id = p_ruleid
				AND t9700.c9700_rule_id = t9704.c9700_rule_id
				AND t9704.c9704_rule_consequence_id = t9705.c9704_rule_consequence_id
				AND t9705.c9705_void_fl IS NULL
				AND t9704.c9704_void_fl IS NULL;
	END gm_fch_consequences;

	/*******************************************************
	* Description : Procedure to fetch pic consequences
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_allpic_consequences (
		p_ruleid			IN		 t9700_rule.c9700_rule_id%TYPE
	  , p_consgrpid 		IN		 t9704_rule_consequence.c901_consequence_id%TYPE
	  , p_out_piccons_cur	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_piccons_cur
		 FOR
			 SELECT   ruleid, ruleconsqid, SUBSTR (picture, INSTR (picture, '\', -1) + 1) picture, MESSAGE, createdby
					, TO_CHAR (createddt, 'MM/DD/YYYY') createddt, fileid
				 FROM (SELECT DECODE (t9705.c901_param_id, 91357, t9705.c9705_consequence_value, '') MESSAGE
							, t9705.c9705_created_date createddt, get_user_name (t9705.c9705_created_by) createdby
							, t9705.c9704_rule_consequence_id ruleconsqid, t9704.c9700_rule_id ruleid
							, t903.c903_file_name picture, t903.c903_upload_file_list fileid
						 FROM t9704_rule_consequence t9704, t9705_consequence_details t9705, t903_upload_file_list t903
						WHERE t9704.c9700_rule_id = p_ruleid
						  AND t9704.c901_consequence_id = p_consgrpid
						  AND t9704.c9704_rule_consequence_id = t9705.c9704_rule_consequence_id
						  AND t9705.c9705_consequence_details_id = t903.c903_ref_id
						  AND t903.c901_ref_type = 91183
						  AND t9704.c9704_void_fl IS NULL
						  AND t9705.c9705_void_fl IS NULL)
			 ORDER BY ruleconsqid;
	END gm_fch_allpic_consequences;

		/*******************************************************
	* Description : Procedure to fetch pic consequences based on consequence id
	* Author		: Ritesh Shah
	*******************************************************/
	PROCEDURE gm_fch_pic_consequences (
		p_ruleconsid		IN		 t9700_rule.c9700_rule_id%TYPE
	  , p_out_piccons_cur	OUT 	 TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_piccons_cur
		 FOR
			 SELECT t9705.c9705_consequence_details_id seqid, t9705.c9704_rule_consequence_id ruleseqid
				  , t9705.c901_param_id paramid, t9705.c9705_consequence_value consvalue, t903.c903_file_name filenm
			   FROM t9705_consequence_details t9705, t903_upload_file_list t903
			  WHERE t9705.c9704_rule_consequence_id = p_ruleconsid
				AND t9705.c9705_void_fl IS NULL
				AND t903.c901_ref_type = 91183
				AND TO_CHAR (t9705.c9705_consequence_details_id) = t903.c903_ref_id;
	END gm_fch_pic_consequences;

	 /*******************************************************
	* Description : Procedure to fetch rule report based on filters
	* Author		: Xun
	*******************************************************/
	PROCEDURE gm_fch_rule_report (
		p_txnid 		 IN 	  t9703_rule_transaction.c901_txn_id%TYPE
	  , p_consequence	 IN 	  t9704_rule_consequence.c901_consequence_id%TYPE
	  , p_initiatedby	 IN 	  t9700_rule.c9700_initiated_by%TYPE
	  , p_status		 IN 	  t901_code_lookup.c901_code_id%TYPE
	  , p_inputstrcond	 IN 	  VARCHAR2
	  , p_out_cur		 OUT	  TYPES.cursor_type
	)
	AS
		v_string	   VARCHAR2 (4000) := p_inputstrcond;
		v_substr	   VARCHAR2 (4000);
		v_value 	   VARCHAR2 (4000);
		v_cond		   NUMBER;
		v_operator	   NUMBER;
		v_oprvalue	   VARCHAR2 (20);
		v_join		   NUMBER;
		v_condition_id NUMBER (10);
		v_condition_cnt NUMBER := 0;
		v_str		   VARCHAR2 (32767);
		v_condition_join_param VARCHAR2 (10);
		v_operator_join_param VARCHAR2 (10);
		v_val		   VARCHAR2 (1000);
		v_val_temp	   VARCHAR2 (1000);
		v_open_braces  VARCHAR2 (1);
		v_close_braces VARCHAR2 (1);
		v_rule_id	   VARCHAR2 (20);
		v_rule_ids	   VARCHAR2 (1000);
		v_rule_cur	   TYPES.cursor_type;
		v_opr_cur	   TYPES.cursor_type;
		v_tmp_string   VARCHAR2 (4000) := p_inputstrcond;
		v_operator_id  VARCHAR2 (20);
	BEGIN
		IF v_string IS NOT NULL
		THEN
			v_str		:= ' select DISTINCT c9700_rule_id RULE_ID ';
			v_str		:= v_str || 'from t9702_rule_condition t9702 ';
			v_str		:= v_str || 'where (';
			v_close_braces := ')';

			WHILE INSTR (v_string, '|') <> 0
			LOOP
				v_substr	:= SUBSTR (v_string, 1, INSTR (v_string, '|') - 1);
				v_string	:= SUBSTR (v_string, INSTR (v_string, '|') + 1);
				v_cond		:= SUBSTR (v_substr, 1, INSTR (v_substr, '^') - 1);
				v_substr	:= SUBSTR (v_substr, INSTR (v_substr, '^') + 1);
				v_operator	:= SUBSTR (v_substr, 1, INSTR (v_substr, '^') - 1);
				v_substr	:= SUBSTR (v_substr, INSTR (v_substr, '^') + 1);
				v_value 	:= SUBSTR (v_substr, 1, INSTR (v_substr, '^') - 1);
				v_substr	:= SUBSTR (v_substr, INSTR (v_substr, '^') + 1);
				v_join		:= SUBSTR (v_substr, 1, INSTR (v_substr, '^') - 1);
				v_condition_cnt := v_condition_cnt + 1;
				v_condition_join_param := '(';
				v_oprvalue	:= get_code_name (v_operator);

				IF v_condition_cnt <> 1
				THEN
					v_condition_join_param := ' AND (';
					v_open_braces := '';
				END IF;

				v_str		:=
							v_str || v_condition_join_param || v_open_braces || ' t9702.c9701_condition_id = ' || v_cond;
				v_condition_id := v_cond;
				v_val		:= v_value;
				v_operator_join_param := 'AND ( ';

				--	put_long_line ('v_val :' || v_value);
				IF v_operator = '50600'
				THEN
					OPEN v_opr_cur
					 FOR
						 SELECT get_code_name (t9002.c901_operator_id) opr
						   FROM t9001_filter_lookup t9001, t9002_filter_operator t9002, t9701_condition t9701
						  WHERE t9701.c9701_condition_id = v_condition_id
							AND t9001.c9001_filter_id = t9002.c9001_filter_id
							AND t9001.c9001_ref_id = t9701.c9701_condition_id
							AND t9001.c9001_filter_grp = 'CM_RULE';

					LOOP
						FETCH v_opr_cur
						 INTO v_operator_id;

						EXIT WHEN v_opr_cur%NOTFOUND;

						IF v_operator_id = 'LIKE'
						THEN
							v_val_temp	:= '%' || v_val || '%';
						ELSIF v_operator_id = 'IN'
						THEN
							v_val_temp	:= '%' || v_val || '%';
						ELSE
							v_val_temp	:= v_val;
						END IF;

						IF v_condition_id = '6'
						THEN
							v_str		:=
								   v_str
								|| v_operator_join_param
								|| ' TO_DATE(t9702.c9702_condition_value,'
								|| ''''
								|| 'MM/DD/YYYY'
								|| ''''
								|| ')'
								|| v_operator_id
								|| ' TO_DATE('
								|| ''''
								|| v_val
								|| ''''
								|| ','
								|| ''''
								|| 'MM/DD/YYYY'
								|| ''''
								|| ')';
						ELSE
							v_str		:=
								   v_str
								|| v_operator_join_param
								|| ' t9702.c9702_condition_value '
								|| v_operator_id
								|| ''''
								|| v_val_temp
								|| '''';
						END IF;

						v_operator_join_param := ' OR ';
					END LOOP;
				ELSE
					IF v_condition_id = '6'
					THEN
						v_str		:=
							   v_str
							|| v_operator_join_param
							|| ' TO_DATE(t9702.c9702_condition_value,'
							|| ''''
							|| 'MM/DD/YYYY'
							|| ''''
							|| ')'
							|| v_oprvalue
							|| ' TO_DATE('
							|| ''''
							|| v_val
							|| ''''
							|| ','
							|| ''''
							|| 'MM/DD/YYYY'
							|| ''''
							|| ')';
					ELSIF v_oprvalue = 'LIKE' OR v_oprvalue = 'IN'
					THEN
						v_str		:=
							   v_str
							|| v_operator_join_param
							|| ' t9702.c9702_condition_value '
							|| v_oprvalue
							|| ''''
							|| '%'
							|| v_val
							|| '%'
							|| '''';
					ELSE
						v_str		:=
							   v_str
							|| v_operator_join_param
							|| ' t9702.c9702_condition_value '
							|| v_oprvalue
							|| ''''
							|| v_val
							|| '''';
					END IF;
				END IF;

				v_operator_join_param := ' OR ';
				v_str		:= v_str || v_close_braces || v_close_braces;
			END LOOP;

			v_str		:= v_str || ')';

			OPEN v_rule_cur
			 FOR v_str;

			LOOP
				FETCH v_rule_cur
				 INTO v_rule_id;

				EXIT WHEN v_rule_cur%NOTFOUND;
				v_rule_ids	:= v_rule_ids || v_rule_id || ',';
			END LOOP;

			CLOSE v_rule_cur;
		END IF;

		my_context.set_my_inlist_ctx (v_rule_ids);

		OPEN p_out_cur
		 FOR
			 SELECT   ID, NAME, initby, initdate
					, DECODE (con_min, con_max, con_max, con_min || ' AND ' || con_max || '...') condition, active_fl
				 FROM (SELECT	ID, NAME, initby, initdate, MIN (con) con_min, MAX (con) con_max, active_fl
						   FROM (SELECT t9700.c9700_rule_id ID, t9700.c9700_rule_name NAME
									  , get_user_name (t9700.c9700_initiated_by) initby
									  , TO_CHAR (t9700.c9700_initiated_date, 'mm/dd/yyyy') initdate
									  ,    t9701.c9701_condition_name
										|| ' '
										|| get_code_name (t9702.c901_operator_id)
										|| ' '
										|| t9702.c9702_condition_value con
									  , NVL (t9700.c9700_active_fl, 'N') active_fl
								   FROM t9700_rule t9700, t9702_rule_condition t9702, t9701_condition t9701
								  WHERE t9700.c9700_rule_id = t9702.c9700_rule_id
									AND t9700.c9700_void_fl IS NULL
									AND t9701.c9701_void_fl IS NULL
									AND t9702.c9702_void_fl IS NULL
									AND t9701.c9701_condition_id = t9702.c9701_condition_id
									AND t9700.c9700_rule_id IN (
											 SELECT t9703.c9700_rule_id
											   FROM t9703_rule_transaction t9703
											  WHERE t9703.c901_txn_id =
																	   DECODE (p_txnid
																			 , '0', t9703.c901_txn_id
																			 , p_txnid
																			  ))
									AND t9700.c9700_initiated_by =
											DECODE (p_initiatedby
												  , '0', t9700.c9700_initiated_by
												  , p_initiatedby
												   )   --craig Billman
									AND t9700.c9700_rule_id IN (
											SELECT t9704.c9700_rule_id
											  FROM t9704_rule_consequence t9704
											 WHERE t9704.c901_consequence_id =
													   DECODE (p_consequence
															 , '0', t9704.c901_consequence_id
															 , p_consequence
															  ))   --message
									AND NVL (t9700.c9700_active_fl, '-999') =
											(CASE
												 WHEN p_status = '91371'
													 THEN 'Y'
												 WHEN p_status = '91372'
													 THEN '-999'
												 ELSE NVL (t9700.c9700_active_fl, '-999')
											 END
											)
									--AND decode() t9700.c9700_rule_id IN (SELECT token FROM v_in_list)
									AND (	'-999' = DECODE (v_tmp_string, NULL, '-999', '-9999')
										 OR t9700.c9700_rule_id IN (SELECT token
																	  FROM v_in_list)
										))
					   GROUP BY ID, NAME, initby, initdate, active_fl)
			 ORDER BY active_fl;
	END gm_fch_rule_report;

	 /*******************************************************
	* Description : Procedure to fetch shipping details of parts for refid
	* Author		: Ritesh
	*******************************************************/
	PROCEDURE gm_fch_rule_shipout_details (
		p_refid 	IN		 t907_shipping_info.c907_ref_id%TYPE
	  , p_source	IN		 t907_shipping_info.c901_source%TYPE
	  , p_txnid 	OUT 	 VARCHAR2
	  , p_out_cur	OUT 	 TYPES.cursor_type
	)
	AS
		v_set_id	   VARCHAR2 (20);
		v_type		   NUMBER;
	BEGIN
		IF p_source = 50181 OR p_source = 50182   --for consignment/loaners
		THEN
			BEGIN
				SELECT t504.c207_set_id, t504.c504_type
				  INTO v_set_id, v_type
				  FROM t504_consignment t504
				 WHERE c504_consignment_id = p_refid;

				IF v_set_id IS NOT NULL
				THEN
					p_txnid 	:= '50918,50919';
				ELSE
					p_txnid 	:= 50927;
				END IF;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					p_txnid 	:= '';
			END;

			IF p_source = 50182
			THEN
				IF v_type = 4127
				THEN   --Product Loaner/Inhouse Loaner
					p_txnid 	:= '50916,50917';
				ELSE
					p_txnid 	:= '50923,50924';
				END IF;
			END IF;

			OPEN p_out_cur
			 FOR
				 SELECT t504.c207_set_id setid, t505.c205_part_number_id pnum, t505.c505_control_number cnum
					  , t505.c505_item_qty qty
				   FROM t505_item_consignment t505, t504_consignment t504
				  WHERE t505.c504_consignment_id = t504.c504_consignment_id
					AND t504.c504_consignment_id = p_refid
					AND t504.c504_void_fl IS NULL
					AND t505.c505_void_fl IS NULL;
		ELSIF p_source = 50180	 -- for orders
		THEN
			OPEN p_out_cur
			 FOR
				 SELECT t502.c205_part_number_id pnum, t502.c502_control_number cnum, t502.c502_item_qty
				   FROM t502_item_order t502, t501_order t501
				  WHERE t501.c501_order_id = t502.c501_order_id
					AND t501.c501_order_id = p_refid   --'015-061212-1'
					AND t501.c501_void_fl IS NULL
					AND t502.c502_void_fl IS NULL;

			p_txnid 	:= 50925;
		ELSIF p_source = 50183	 -- for loaners extn
		THEN
			OPEN p_out_cur
			 FOR
				 SELECT t413.c205_part_number_id pnum, t413.c413_control_number cnum, t413.c413_item_qty qty
				   FROM t413_inhouse_trans_items t413, t412_inhouse_transactions t412
				  WHERE t413.c412_inhouse_trans_id = t412.c412_inhouse_trans_id
					AND t412.c412_inhouse_trans_id = p_refid
					AND t412.c412_void_fl IS NULL
					AND t413.c413_void_fl IS NULL;

			BEGIN
				SELECT t504.c504_type
				  INTO v_type
				  FROM t504_consignment t504, t412_inhouse_transactions t412
				 WHERE t412.c412_inhouse_trans_id = p_refid AND t412.c412_ref_id = t504.c504_consignment_id;
			EXCEPTION
				WHEN NO_DATA_FOUND
				THEN
					v_type		:= '';
			END;

			IF v_type = 4127
			THEN   --Product Loaner/Inhouse Loaner
				p_txnid 	:= '50916,50917';
			ELSE
				p_txnid 	:= '50923,50924';
			END IF;
		END IF;
	END gm_fch_rule_shipout_details;

	/**************************************************************************
			* Description : Procedure to fetch rule summary
			author: Xun
			*************************************************************************/
	PROCEDURE gm_fch_rule_summary (
		p_ruleid			   IN		t9700_rule.c9700_rule_id%TYPE
	  , p_out_header_details   OUT		TYPES.cursor_type
	  , p_out_consequences	   OUT		TYPES.cursor_type
	  , p_out_conditions	   OUT		TYPES.cursor_type
	  , p_out_transactions	   OUT		TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_header_details
		 FOR
			 SELECT t9700.c9700_rule_name rule_name, t9700.c9700_expiry_date expiry_date
				  , t9700.c9700_email_notify_fl email_fl, t9700.c9700_comment rule_comment
				  , get_user_name (t9700.c9700_initiated_by) init_by
				  , TO_CHAR (t9700.c9700_initiated_date, 'MM/DD/YYYY') init_date
				  , get_user_name (t9700.c9700_last_updated_by) updated_by
				  , TO_CHAR (t9700.c9700_last_updated_date, 'MM/DD/YYYY') updated_date
				  , t9700.C9700_ACTIVE_FL activefl
				  , t9700.C9700_EXPIRY_HISTORY_FL exp_hisfl
				  , t9700.C9700_ACTIVE_HISTORY_FL act_hisfl
				  , t9700.C9700_EMAIL_NOTIFY_HISTORY_FL email_hisfl
				  , t9700.C9700_CONDITION_HISTORY_FL con_hisfl
				  , t9700.C9700_TXN_HISTORY_FL txn_hisfl
				  , t9700.C9700_HOLD_TXN_HISTORY_FL holdtxn_hisfl
			   FROM t9700_rule t9700
			  WHERE t9700.c9700_rule_id = p_ruleid AND t9700.c9700_void_fl IS NULL;

		OPEN p_out_consequences
		 FOR
			 SELECT MAX (DECODE (t9705.c901_param_id, 91351, t9705.c9705_consequence_value, '')) MESSAGE
				  , MAX (DECODE (t9705.c901_param_id, 91352, t9705.c9705_consequence_value, '')) hold_txn
				  , MAX (DECODE (t9704.c901_consequence_id, 91347, 'Y', 'N')) picture
				  , MAX (DECODE (t9705.c901_param_id, 91365, t9705.c9705_consequence_value, '')) email_to
				  , MAX (DECODE (t9705.c901_param_id, 91366, t9705.c9705_consequence_value, '')) email_sub
				  , MAX (DECODE (t9705.c901_param_id, 91367, t9705.c9705_consequence_value, '')) email_body
			   FROM t9704_rule_consequence t9704, t9705_consequence_details t9705
			  WHERE t9704.c9704_rule_consequence_id = t9705.c9704_rule_consequence_id
				AND t9704.c9700_rule_id = p_ruleid
				AND t9704.c9704_void_fl IS NULL
				AND t9705.c9705_void_fl IS NULL;

		OPEN p_out_conditions
		 FOR
			 SELECT 	 t9701.c9701_condition_name
					  || ' '
					  || get_code_name (t9702.c901_operator_id)
					  || ' '
					  || t9702.c9702_condition_value conditions
				 FROM t9702_rule_condition t9702, t9701_condition t9701
				WHERE t9701.c9701_condition_id = t9702.c9701_condition_id
				  AND t9702.c9700_rule_id = p_ruleid
				  AND t9702.c9702_void_fl IS NULL
			 ORDER BY t9701.c9701_condition_id, t9701.c9701_condition_level;

		OPEN p_out_transactions
		 FOR
			 SELECT get_code_name (t9703.c901_txn_id) transactions
			   FROM t9703_rule_transaction t9703
			  WHERE t9703.c9700_rule_id = p_ruleid AND t9703.c9703_void_fl IS NULL;
	END gm_fch_rule_summary;

	/******************************************************************
	* Description : Procedure to fetch rule name and expiry date
	* Author		: Xun
	****************************************************************/
	PROCEDURE gm_fch_rule_expiry_details (
		p_out_rule_dtls   OUT	TYPES.cursor_type
	)
	AS
	BEGIN
		OPEN p_out_rule_dtls
		 FOR
			 SELECT DISTINCT t9700.c9700_rule_id ruleid, t9700.c9700_rule_name rule_name
						   , TO_CHAR (t9700.c9700_expiry_date, 'mm/dd/yyyy') expdate
						   , get_user_emailid (t9700.c9700_initiated_by) to_email
						   , get_user_emailid (t9700.c9700_last_updated_by) cc_email
						FROM t9700_rule t9700, t9704_rule_consequence t9704
					   WHERE t9700.c9700_rule_id = t9704.c9700_rule_id
						 AND t9704.c901_consequence_id = 91348
						 AND t9700.c9700_email_notify_fl = 'Y'
						 AND TRUNC (t9700.c9700_expiry_date) = TRUNC (SYSDATE) + 2
						 AND t9700.c9700_active_fl = 'Y'
						 AND t9700.c9700_void_fl IS NULL
						 AND t9704.c9704_void_fl IS NULL;
	END gm_fch_rule_expiry_details;

	/*******************************************************
 * Purpose: function is used to get Transaction name
   author Xun
 *******************************************************/
--
	FUNCTION get_trans_name (
		p_txnid   IN   t9703_rule_transaction.c901_txn_id%TYPE
	)
		RETURN VARCHAR2
	IS
		v_txn_name	   VARCHAR2 (100);
	BEGIN
		SELECT DECODE (p_txnid
					 , 50900, 'DHR-Pending Receiving'
					 , 50901, 'DHR-Pending Inspection'
					 , 50902, 'DHR-Pending Packaging'
					 , 50903, 'DHR-Pending Verification'
					 , 50904, 'RN-Consignment'
					 , 50905, 'RN-Packaging'
					 , 50906, 'FG-Shelf to Product Loaner'
					 , 50907, 'FG-Shelf to Quarantine'
					 , 50908, 'FG-Shelf to Raw Material'
					 , 50909, 'FG-Shelf to Item Sales Consignment'
					 , 50910, 'FG-Shelf to Built Sets'
					 , 50911, 'FG-Shelf to Packaging'
					 , 50912, 'FG-Shelf to Item Inhouse Consignment'
					 , 50913, 'FG-Shelf to Inhouse Loaners'
					 , 50914, 'QN-Quarantine to Shelf'
					 , 50915, 'QN-Quarantine to Scrap'
					 , 50916, 'LN-Loaner to Shelf'
					 , 50917, 'LN-Loaner to Quarantine'
					 , 50918, 'SB-Built Sets to Quarantine'
					 , 50919, 'SB-Built Sets to Shelf'
					 , 50920, 'SB-Built Sets to Product Loaner'
					 , 50921, 'SB-Built Sets to InHouse Loaner'
					 , 50922, 'SB-Built Sets to Hospital'
					 , 50923, 'ILN-Loaner to Shelf'
					 , 50924, 'ILN-Loaner to Quarantine'
					 , 50925, 'OR-Orders'
					 , 50926, 'BM-RM to FG'
					 , 50927, 'Inhouse Consignment'
					 , 50928, 'Sales Item Consignment'
					  )
		  INTO v_txn_name
		  FROM DUAL;

		RETURN v_txn_name;
	END get_trans_name;
END gm_pkg_cm_rules;
/
