<%
/**********************************************************************************
 * File		 		: GmGroupPartMapping.jsp
 * Desc		 		: Map the parts to correspoding Groups
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<!-- \operations\forecast\GmGroupPartMapping.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<!-- Removed taglib, try-catch and some common included files since GmHeader.inc is included -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="org.apache.commons.beanutils.BeanMap, org.apache.commons.beanutils.RowSetDynaClass, org.apache.commons.beanutils.BasicDynaBean"%>
<%@ page import="java.util.ArrayList,java.util.Iterator" %>


<bean:define id="strEnableGrpType" name="frmGroupPartMapping" property="strEnableGrpType" type="java.lang.String"></bean:define>
<bean:define id="publishfl" name="frmGroupPartMapping" property="publishfl" type="java.lang.String"></bean:define> 
<bean:define id="enableGrpPartMapChange" name="frmGroupPartMapping" property="enableGrpPartMapChange" type="java.lang.String"></bean:define>
<bean:define id="setID" name="frmGroupPartMapping" property="setID" type="java.lang.String"></bean:define> 
<bean:define id="setName" name="frmGroupPartMapping" property="setName" type="java.lang.String"></bean:define> 
<!-- MNTTASK-6106 - After Publish group, this party only should change below bean define. -->
<bean:define id="strDisablePricedType" name="frmGroupPartMapping" property="strDisablePricedType" type="java.lang.String"></bean:define>
<bean:define id="strDisableGroupName" name="frmGroupPartMapping" property="strDisableGroupName" type="java.lang.String"></bean:define>

<% 
String strProdMgmntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
//String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROUP_PART_MAP");
boolean blDisableGroupName = false;

ArrayList alReturn = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("RDGROUPPARTMAP"));
		String strJSONString= "";
int intParts = alReturn.size();

String strCollapseImage = strImagePath+"/plus.gif";

if(strDisableGroupName.equals("disabled"))
	blDisableGroupName = true;
String strSetId = "";
String strSetNm="";
String strGrpNm ="";
String strGrpId= (String)request.getAttribute("GROUPID");
HashMap hmResult = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMVALUES"));
strGrpNm= (String)hmResult.get("GROUPNAME");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Group Part Mapping Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strProdMgmntJsPath%>/GmGroupPartMapping.js"></script>
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script type="text/javascript">
function fnHtmlEditor(){
	
	tinyMCE.init({
		mode : "specific_textareas",
	    editor_selector : "HtmlTextArea",
		theme : "advanced" ,
		theme_advanced_path : false,
		height : "300",
		width: "500" 
	});
}
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnCallOnLoad();fnHtmlEditor();">
<html:form action="/gmGroupPartMap.do">

<html:hidden property="strOpt" />
<html:hidden property="haction" />
<input type="hidden" name="hvalidgrpnm"/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDGRP"/>
<html:hidden property="groupId" />
<html:hidden property="hinputPrimaryParts" value=""/> 
<html:hidden property="inputString" value=""/>
<html:hidden property="hactiveflag" value=""/> 
<html:hidden property="deptId"/>
<html:hidden property="strForward" />
<input type="hidden" name="hOld_GrpId" id="hOld_GrpId" value ="<%=strGrpNm%>">
	<table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
		
		<tr>
			<td width="100%" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption">
					<td height="25" colspan="2" ><a href="javascript:fnShowFilters('tabGroup');" tabindex="-1" style="text-decoration:none; color:black"  title="Click to toggle the Group Details">
						<IMG id="tabGroupimg" border=0 src="<%=strCollapseImage%>">&nbsp;Group Details</a>
					</td>
					</tr>
					<tr><td colspan="2">
					<table border="0" bordercolor="lightblue" width="100%" cellspacing="0" cellpadding="0" id="tabGroup" style="display:none">						
        	        <tr class="Shade">
                    	<td class="RightTableCaption" align="right" HEIGHT="24"> &nbsp;<font color="red">*</font>Group Name:&nbsp;</td> 
                        <td>
                        <table>
                        <tr>
                        <td width="20%"><html:text property="groupName" styleId="groupName" disabled="<%=blDisableGroupName%>" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff'); "onchange="javascript:fnCheckGroupNm('CHECKGROUPNAME',this.form);"/>                                        
                 
                        </td>
                        <td><IMG align="left" id="validateName" border=0
								height="20" width="20" src="" style="display: none" alt=""/></td>
						</tr>
						</table>
						
                    </tr>  
                    <tr><td colspan="2" class="LLine"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="210"> &nbsp;Group Description:&nbsp;</td> 
                        <td><textarea name="groupDesc" class="HtmlTextArea" rows="3" cols="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"><bean:write name="frmGroupPartMapping" property="groupDesc"/></textarea>                                       
                        </td>
                    </tr>  
                    <tr><td colspan="2" class="LLine"></td></tr> 
                     
                    <tr>
                    	<td height="24" class="RightTableCaption" align="right"><font color="red">*</font>System:&nbsp;</td>
		<td colspan="3" height="30" align="left">
			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="setID" />
			 <jsp:param name="METHOD_LOAD" value="loadSystemList&searchType=PrefixSearch" /> 
				<jsp:param name="WIDTH" value="510" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="CONTROL_NM_VALUE" value="<%=setName%>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=setID%>" />
				<jsp:param name="SHOW_DATA" value="50" />
	            
			</jsp:include>  
			</td> 
						<%-- <td><gmjsp:dropdown controlName="setID" SFFormName="frmGroupPartMapping" SFSeletedValue="setID"
										SFValue="alSetID" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"/> 		
						</td> --%>	
                    </tr>
                    <tr><td colspan="2" class="LLine"></td></tr> 
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24"> Active Flag:&nbsp;</td> 
                        <td><html:checkbox property="activeflag" styleId="activeflag"/>
                         </td>
                    </tr>
                    
                    <tr><td colspan="2" class="LLine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">Enable Subcomponents:&nbsp;</td> 
                        <td><html:checkbox property="enableSubComponent" styleId="enableSubComponent" /></td>
                    </tr> 
                 	<tr><td colspan="2" class="LLine"></td></tr> 
                     
                   	<tr class="Shade">
                     <td height="24" class="RightTableCaption" align="right"><font color="red">*</font>Group Type:&nbsp;</td>
						<td  <%=strEnableGrpType %> ><gmjsp:dropdown controlName="groupType" SFFormName="frmGroupPartMapping" SFSeletedValue="groupType"
										SFValue="alGroupType" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/> 
							<logic:equal name="frmGroupPartMapping" property="strEnableGrpType" value="disabled">	
						<a title="Please contact Rick W or sales analyst to change the group type"><img src=<%=strImagePath%>/question.gif border=0</a>
						</logic:equal>
											
						</td>	
                     </tr>
				   	<tr><td colspan="2" class="LLine"></td></tr> 
                     
				    <tr>
                    	<td height="24" class="RightTableCaption" align="right"><font color="red">*</font>Priced Type:&nbsp;</td>
						<td><gmjsp:dropdown controlName="pricedType" SFFormName="frmGroupPartMapping" SFSeletedValue="pricedType"
										SFValue="alPricedType" disabled ="<%=strDisablePricedType%>"  codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/> 	
						</td>	
                    </tr> 
                     <tr><td colspan="2" class="LLine"></td></tr> 
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24"> Speciality Group:&nbsp;</td> 
                        <td><html:checkbox property="speclGroupFlag"/>
                         </td>
                    </tr>
			 		<tr><td colspan="11" class="ELine"></td></tr>
				 		</table></td>
				 	</tr>
		  			<tr>
						<td colspan="11">
							<div>
							
            				<iframe src="/gmGroupPartMap.do?strPartListFlag=Y&strDisableBtn=Y&groupId=<bean:write name="frmGroupPartMapping" property="groupId"></bean:write>&haction=loadParts&enableSubComponent=<bean:write name="frmGroupPartMapping" property="enableSubComponent"></bean:write>&partNumbers=<bean:write name="frmGroupPartMapping" property="partNumbers"></bean:write>" 
						        id="PartListFrame" scrolling="auto" align="center" marginheight="0"  WIDTH="742" HEIGHT="300"></iframe>

							</div>
						</td>
					</tr>	
						<%
								String strDisableBtnVal = enableGrpPartMapChange;
							if(strDisableBtnVal.equals("disabled"))
							{
								strDisableBtnVal = "true";
							}
						%>
    				<tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
                    	<td colspan="2" align="center">
		                    <gmjsp:button disabled="<%=strDisableBtnVal%>"  value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoid(this.form);"/>
		                    <gmjsp:button disabled="<%=strDisableBtnVal%>" name="Btn_Submit" value="Submit" gmClass="button" buttonType="Save"  onClick="fnSubmit(this.form);" /> 
		                </td>
                    </tr>
 					<tr>
                    	<td class="RightText" colspan="2" align="left">
                    		 Notes:</br>
		             		<font color="red">*</font>If a check box appears in the Primary Column this part is used in more than one project. Check the box to make the part primary for your Group.</br>
		             		<li><img src='<%=strImagePath%>/uncheck.jpg'/>Primary in a different group.</br></li>
		             		<li><img src='<%=strImagePath%>/check.jpg'/>Primary part for this group.</br></li>
		             		<font color="red">*</font>To switch which group a part is primary in:
		             		<li>1.Unlock the primary lock checkmark in the original group</li>
   		             		<li>2.Go to the new group - you can now check off the part as primary in this new group</br></li>          		
		                </td>
                    </tr>                    
				</table>
			</td>
		</tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

