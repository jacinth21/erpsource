<%@ include file="/common/GmHeader.inc"%>				

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

 <!-- \operations\forecast\GmTTPMonthlySummaryTab.jsp -->


<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>

 
<% 
Object bean = pageContext.getAttribute("frmTTPMonthlySummary", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<bean:define id="demandSheetIds" name="frmTTPMonthlySummary" property="demandSheetIds" type="java.lang.String"> </bean:define>
<bean:define id="ttpId" name="frmTTPMonthlySummary" property="ttpId" type="java.lang.String"> </bean:define>
<bean:define id="strMonthId" name="frmTTPMonthlySummary" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="strYearId" name="frmTTPMonthlySummary" property="yearId" type="java.lang.String"> </bean:define>
<BODY >
				<table>						
						<tr><td>					
						<ul id="summarytab" class="shadetabs">
						<li><a href="/gmTTPMonthlySummary.do?strOpt=fetchFilter" rel="ajaxcontentarea"  class="selected">Filter</a></li>
						<li><a href="/gmTTPMonthlySummary.do?strOpt=reportFCRequest&ttpId=<%=ttpId%>&monthId=<%=strMonthId%>&yearId=<%=strYearId%>&demandSheetIds=<%=demandSheetIds%>" rel="ajaxcontentarea">Forecast Request</a></li>						
						</ul>
						
						<div id="ajaxdivtabarea" class="contentstyle" style="display:visible;height: 75px; overflow: auto;">												
						</div>						
					</td></tr> 
				 </table>
				<div id="ajaxreportarea" class="contentstyle" style="display:visible;height: 300px; ">
				<table>				
				<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<tr> <td>						
						    <jsp:include page="GmTTPMonthlySummaryReportInc.jsp"/>
						</td>
					</tr>
				</table>
			    </div>	
			    <%@ include file="/common/GmFooter.inc"%>
</BODY>
