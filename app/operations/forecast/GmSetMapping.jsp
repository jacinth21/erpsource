<%
/**********************************************************************************
 * File		 		: GmSetmapping.jsp
 * Desc		 		: display Set Mapping info
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<!-- \operations\forecast\GmSetMapping.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="org.apache.commons.beanutils.DynaBean"%> 
<bean:define id="ldtResult" name="frmDemandSheetSetup" property="ldtResult" type="java.util.List"></bean:define>
<%  
 
String strWikiTitle = GmCommonClass.getWikiTitle("SET_ACTION_MAPPING");
ArrayList alList = new ArrayList();
 
alList = GmCommonClass.parseNullArrayList((ArrayList)ldtResult);
 
int intRows = 0;
intRows = alList.size();

StringBuffer sbInput = new StringBuffer();
String strSetID = "";
String strInput = "";
 
DynaBean db ;

	for (int i=0;i<intRows;i++)
	{
		db = (DynaBean)alList.get(i);
		strSetID = String.valueOf(db.get("SETID"));
		sbInput.append(strSetID);
		sbInput.append("^");
	}
	 strInput = sbInput.toString();
	// System.out.println("string" + strInput);
	
//String strpop = (String)request.getAttribute("PopType");
 

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script> 

function fnSubmit()
{
	var varRows = <%=intRows%>;
    var varDMId =  document.frmDemandSheetSetup.demandSheetId.value; 
	  
	var inputString = ''; 
	var varInput = "<%=strInput%>";
	
	var arrPar = varInput.split('^');
	if(varDMId!=null)
	{
	for (var i =0; i < varRows; i++) 
	{
		obj = eval("document.frmDemandSheetSetup.setactiontype"+i);
		varPar = obj.value;
		obj1 = eval("document.frmDemandSheetSetup.stackmonth"+i);
		varMonth = obj1.value;
	    inputString = inputString + arrPar[i] + '^'+ varDMId +'^'+ varPar +  '^'+ varMonth + '|';
	 	   
	}
	//	 alert("input is " + inputString);
	}
	// alert("varDMId is " + inputString);
	document.frmDemandSheetSetup.strOpt.value = 'save';
	document.frmDemandSheetSetup.hinputSetAction.value = inputString;
	fnStartProgress('Y');
	 document.frmDemandSheetSetup.submit();
}
 
 
function fnApplyAll()
{
 // alert("selectedApplyAll is " + document.frmDemandSheetSetup.cbo_allAction.selectedIndex);
  
 var selectedApplyAll = document.frmDemandSheetSetup.cbo_allAction.selectedIndex;
 var size="<%=intRows%>";
 for (var i=0;i<size ;i++ )
	{
		 
		obj = eval("document.frmDemandSheetSetup.setactiontype"+i);			
		 
		   obj.options[selectedApplyAll].selected = true;
		 
		}
	 
 obj2 = eval("document.frmDemandSheetSetup.cbo_allAction");
        
      if(  obj2.value == "50292") 
      { 
            for (var i=0;i<size ;i++ )
      		{
      		 obj1 = eval("document.frmDemandSheetSetup.stackmonth"+i);
             obj1.disabled = false;  
           
            }
       
      }     
      if(  obj2.value == "50291" ||obj2.value == "" ) 
      { 
            for (var i=0;i<size ;i++ )
      		{
      		 obj1 = eval("document.frmDemandSheetSetup.stackmonth"+i);
      		 //MNTTASK-6106. When select NoStack in drop down, stackmonth text box should empty.
      		 eval("document.frmDemandSheetSetup.stackmonth"+i).value = '';
             obj1.disabled = true;  
           
            }
       
      }    
}

function fnUpdate(v )
{
	//var varmattyepid = document.frmManualAdjTxn.maTypeId.value;
	var len = "setactiontype".length ; 
	
	index  = v.substring ( len ) ;
 
	ob = eval("document.frmDemandSheetSetup.stackmonth"+index);
	obj = eval("document.frmDemandSheetSetup.setactiontype"+index);
		
	 
	if(obj.value =="50292" ) 
	 	ob.disabled = false;  

	if(obj.value =="50291" || obj.value ==""){
		//MNTTASK-6106. When select NoStack in drop down, stackmonth text box should empty.
		 eval("document.frmDemandSheetSetup.stackmonth"+index).value = '';
	 	ob.disabled = true;
	 }
}
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmSetMapping.do"  >
<html:hidden property="strOpt" />
<html:hidden property="hinputSetAction" />
<html:hidden property="demandSheetId" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Set Action Mapping </td>
			<td align="right" class=RightDashBoardHeader  colspan="3"> 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		
		<tr>
			<td height="25" width=400 > </td>
			 
			<td align="right" width=150  ><select name="cbo_allAction" id="" class="RightText" tabindex=0  onChange="fnApplyAll()"  > <option value=0 id=0 >[Choose One]<option  value= 50291   >No Stack</option><option  value= 50292   >Stack</option></select>
				   </td>
			<td height="25" width=150 colspan="2"  > &nbsp;</td>		 
		</tr>
		
		<tr><td colspan="4" height="1" class="line"></td></tr>
		
		 <tr>
	                     <td colspan="4">
	                     	<display:table  name="requestScope.frmDemandSheetSetup.ldtResult" class="its" id="currentRowObject" requestURI="/gmSetMapping.do"  decorator="com.globus.displaytag.beans.DTSetMappingWrapper"  > 
							<display:column property="SETID" title="Set ID"    class="alignleft"/>
							<display:column property="SETNAME" title="Set Name"   class="alignleft"/>
							 
							<display:column property="SETACTIONTYPE" title="Action Type"   class="aligncenter"/> 
							<display:column property="MONTHID" title="Months"   class="alignleft"/>  
							 
							</display:table>

    		             </td>
                    </tr> 
                
         <tr><td colspan="4" class="ELine"></td></tr> 
                    <tr>
                    	<td colspan="4" align="center">
		                     <gmjsp:button value="Submit" gmClass="button" onClick="fnSubmit();" buttonType="Save" />  
		                </td>
                    </tr>
		 
    </table>		     	
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

