    <%@ include file="/common/GmHeader.inc" %>
   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %> 
   <%@ page import ="com.globus.common.servlets.GmServlet"%>    
   <%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>    
   <script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
   <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">

 <!-- \operations\forecast\GmIncludePartQtyDetails.jsp -->
	<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
	</style>
	 
    
    <bean:define id="hmPartDetail" name="frmDemandSheetSummary"  property="hmPartDetail" type="java.util.HashMap"></bean:define>
    <bean:define id="hmReportValue" name="frmDemandSheetSummary"  property="hmDemandSheetDetail" type="java.util.HashMap"></bean:define>
     
    <%
    
    try
    {	    
		GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
		ArrayList alForecastHeader = new ArrayList();
		ArrayList alDrillDown = new ArrayList();		
		HashMap hmapValue = new HashMap();
		String strFooterStyle = "";  	
		String strStatus = "";
		int r = 0;			

		 

		if (hmReportValue.size() == 0) 
		{
			return;
		}
 
		
		// Line Style information
		gmCrossTab.setColumnDivider(false);
		gmCrossTab.setColumnLineStyle("borderDark");
		 
		alForecastHeader = (ArrayList) hmReportValue.get("FORECASTHEAD");
		 
		int Headersize = alForecastHeader.size();
		
		//gmCrossTab.setSelectedColDrillDown(true);
		for (int i = 0; i < Headersize; i++)
        {
            hmapValue = (HashMap) alForecastHeader.get(i);
            r = i%2;
            if (r == 0) 
            {
            	strFooterStyle = "ShadeMedBrownTD";
            }
            else
            {
            	strFooterStyle = "ShadeLightBrownTD";
            }
			gmCrossTab.addStyle((String)hmapValue.get("PERIOD"),strFooterStyle,"ShadeDarkBrownTD") ;
			 
        }
		gmCrossTab.addStyle("Name","ShadeLightBlueTD","ShadeDarkBlueTD");
	 
		gmCrossTab.addStyle("PBO","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("PBL","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("PCS","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("Total","ShadeLightBlueTD","ShadeDarkBlueTD");
		gmCrossTab.setGeneralHeaderStyle("aaTopHeader");		 
		gmCrossTab.addLine("Name");
	 
		gmCrossTab.addLine("PCS");
		
		gmCrossTab.setRoundOffAllColumnsPrecision("0");
		 
		gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
	 	gmCrossTab.setTotalRequired(false);
	 	gmCrossTab.setColumnTotalRequired(false);
	 	gmCrossTab.setDivHeight(150);
//		gmCrossTab.setGroupingRequired(true);
		gmCrossTab.setNoDivRequired(true);
 		gmCrossTab.setRowHighlightRequired(true);
		gmCrossTab.setLinkRequired(false);		 
		gmCrossTab.setDecorator("com.globus.crosstab.beans.GmPartQtyDecorator");
		
		out.println(gmCrossTab.PrintCrossTabReport(hmPartDetail)) ;

    }catch(Exception e)
	{
		e.printStackTrace();
	}
	%>