
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

 <!-- \operations\forecast\GmTTPMonthlySummaryReportInc.jsp -->
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ include file="/common/GmHeader.inc" %>
 
 <% 
Object bean = pageContext.getAttribute("frmTTPMonthlySummary", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>

<%
	String strlinkID = GmCommonClass.parseNull((String)request.getParameter("linkID"));
 	strlinkID = strlinkID.equals("") ? "Summary" : strlinkID;
 	
%>

<bean:define id="strMonthId" name="frmTTPMonthlySummary" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="strYearId" name="frmTTPMonthlySummary" property="yearId" type="java.lang.String"> </bean:define>
<bean:define id="strInvtoryID" name="frmTTPMonthlySummary" property="inventoryId" type="java.lang.String"> </bean:define>
<bean:define id="strTtpType" name="frmTTPMonthlySummary" property="ttpType" type="java.lang.String"> </bean:define>

<bean:define id="strQuarAvail" name="frmTTPMonthlySummary" property="quarAvail" type="java.lang.String"> </bean:define>
<bean:define id="strQuarAlloc" name="frmTTPMonthlySummary" property="quarAlloc" type="java.lang.String"> </bean:define>
<bean:define id="strDesc" name="frmTTPMonthlySummary" property="pdesc" type="java.lang.String"> </bean:define>

<% 
	//Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger 
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	HashMap hmReportValue = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMVALUES"));
	String strStatus = GmCommonClass.parseNull((String)hmReportValue.get("STATUS")) ;
	String strFDate = GmCommonClass.parseNull((String)hmReportValue.get("FINALDATE")) ;
	String strDemandSheetID = GmCommonClass.parseNull((String)request.getAttribute("DEMANDSHEETID"));
	String strDSStatus = GmCommonClass.parseNull((String)hmReportValue.get("DSSTATUS")) ; // For Submit button to display.
	log.debug("strStatus *******" + strStatus);
	log.debug("strDesc: "+strDesc+" strQuarAlloc : "+ strQuarAlloc + "  strQuarAvail : "+ strQuarAvail );
	ArrayList alDrillDown = new ArrayList();
	ArrayList alSheetList = new ArrayList();

	if(!strDesc.equals("true"))
	{
		gmCrossTab.setHideColumn("Description");
	}
	
	if(!strQuarAlloc.equals("true"))
	{
		gmCrossTab.setHideColumn("Quar. Alloc");
	}
	
	if(!strQuarAvail.equals("true"))
	{
		gmCrossTab.setHideColumn("Quar. Avail");
	}
	
	if(strQuarAlloc.equals("true"))
	{
		gmCrossTab.addLine("Quar. Alloc");
	}
	else if (strQuarAvail.equals("true"))
	{
		gmCrossTab.addLine("Quar. Avail");
	}
	
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setSortRequired(false);

	// To specify if its unit not to round by zero  
	gmCrossTab.setValueType(0);
		
	// Line Style information
	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	//gmCrossTab.setNameWidth(1300);

	// Between lines
	gmCrossTab.addLine(strFDate);
	gmCrossTab.addLine("Unit Cost $");
	gmCrossTab.addLine("Total Inventory");
	gmCrossTab.addLine("Total Req");
	//gmCrossTab.addLine("Name");
	gmCrossTab.addLine("Description");
	//gmCrossTab.addLine("PBO");
	//gmCrossTab.setLinkRequired(true);
	//alDrillDown.add("DSReport");
	//gmCrossTab.setDrillDownDetails(alDrillDown);
	
		//gmCrossTab.reNameColumn("PDESC", "Description");
	// Row color 
	gmCrossTab.addStyle("Name","ShadeMedBlueTD","ShadeDarkBlueTD");
	gmCrossTab.addStyle("Unit Cost $","ShadeLightBlueTD","ShadeDarkBlueTD");
	
	gmCrossTab.addStyle("In Stock","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	//gmCrossTab.addStyle("PO + DHR","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("PO","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("DHR","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Build Sets","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Total Inventory","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	
	gmCrossTab.addStyle("Quar. Avail","ShadeMedGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("Quar. Alloc","ShadeLightGreenTD","ShadeDarkGreenTD");
	
	gmCrossTab.addStyle("Parent Need","ShadeMedBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("TPR","ShadeLightBrownTD","ShadeDarkBrownTD");
	
	gmCrossTab.addStyle("Forecast Qty","ShadeMedBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("Total Req","ShadeLightBrownTD","ShadeDarkBrownTD");
	
	
	gmCrossTab.addStyle("Order Qty","ShadeMedYellowTD","ShadeDarkYellowTD") ;
	gmCrossTab.addStyle("Order Cost$","ShadeLightYellowTD","ShadeDarkYellowTD");
	
	gmCrossTab.addStyle("PBO","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("PBL","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("PCS","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	
	gmCrossTab.setNoDivRequired(true);
	//gmCrossTab.addDivMandatory("Unit Cost $");
	gmCrossTab.add_RoundOff("Unit Cost $","2");
	gmCrossTab.add_RoundOff("Order Cost$","2");
	//gmCrossTab.setRoundOffAllColumnsPrecision("0");

	//gmCrossTab.addSkip_Div_Round("Order Cost$");
	
	//Currently no override fal will be added in the future
	//if (strStatus.equals("50581"))
	//{
	//	gmCrossTab.setSelectedColDrillDown(true);
	//	gmCrossTab.addSpecificCollDrillDown("Order Qty");
	//}	
	//gmCrossTab.setAllColDrillDown(true);
		
	gmCrossTab.setColumnOverRideFlag(true);
	gmCrossTab.setColumnWidth("Description",1000);
	gmCrossTab.setNameWidth(250);

	if(!strTtpType.equals("50310"))
	{
		gmCrossTab.setLinkRequired(true);
		alDrillDown.add("Actual");
		alDrillDown.add("Part");
		gmCrossTab.setDrillDownDetails(alDrillDown);
	}
	
	
	gmCrossTab.setTableId(strlinkID);
	gmCrossTab.setDivHeight(470);
	// Adding Cross Over
	gmCrossTab.setExport(true, pageContext,"/gmTTPMonthlySummary.do","TTP_Summary.xls");
	
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmTTPSummaryDecorator");
	gmCrossTab.setAttribute("INVENTORYID",strInvtoryID);
	gmCrossTab.setAttribute("TTPTYPE",strTtpType);
	gmCrossTab.setAttribute("MONYEAR",strMonthId+" "+ strYearId);
	
%>



<table border="0" cellspacing="0" cellpadding="0"><tr><td>	
							
 <%=gmCrossTab.PrintCrossTabReport(hmReportValue) %>
</tr></td>
<%				
					if (strDSStatus.equals("false"))
					{
					%>				
						<tr>
							<td colspan ="7" align=center height="35"><gmjsp:button value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" gmClass="button" disabled="true" onClick="fnSubmit();" buttonType="Save" /></td>
						</tr>
					<%
					}
				%>	
</table>
<%@ include file="/common/GmFooter.inc" %>
				 