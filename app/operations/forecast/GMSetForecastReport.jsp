 <%
/**********************************************************************************
 * File		 		: GMSetForecastReport.jsp
 * Desc		 		: This screen is used for displaying Forecast Report
 * Version	 		: 1.0
 * author			: Lakshmi Madabhushanam
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
 <!-- \operations\forecast\GMSetForecastReport.jsp --> 
 <%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->


<%
try {
 
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Forecast Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
 

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmDSSummary.do"  >
<html:hidden property="strOpt" />


	<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;Set Forecast</td>
		</tr>
		
				
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="requestScope.frmDemandSheetSummary.ldtResult" requestURI="/gmDSSummary.do"   class="its" id="currentRowObject"> 
			 	<display:column group="1" property="SET_ID" title="Set ID" class="aligncenter" sortable="true" />
				<display:column group="2" property="SET_NM" title="Set Name" class="alignleft"  />
				<display:column property="DEMSHTNAME" title="Demand Sheet Type" sortable="true" class="alignleft" />
				<display:column property="COUNT" title="Count" sortable="true" class="alignright" />
				
			</display:table>
			</td>
		</tr>
    <table>
    
     

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>