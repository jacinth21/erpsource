 <%
/**********************************************************************************
 * File		 		: GmTTPReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
 <!-- \sales\AreaSet\GmAddInfo.jsp --> 
 <%@ include file="/common/GmHeader.inc" %>
<% 

String strWikiTitle = GmCommonClass.getWikiTitle("TTP_REPORT");

try{
 %>

<HTML>
<HEAD>
<TITLE> Globus Medical: TTP Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
 
</script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<BODY leftmargin="20" topmargin="10">
	<table border="0" class="DtTable850" width="800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">
				TTP Report 
			</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<tr><td colspan="2">

	<display:table name="requestScope.results.rows" requestURI="/gmTTPReport.do?method=demandTTPReport" export="true"  class="its" id="currentRowObject" > 
	<display:column property="TTPNM" title="TTP Name" sortable="true" />
	<display:column property="DMNAME" title="Demand Sheet Name" sortable="true" />
  	<display:column property="MAPTYPE" title="Demand Sheet Map Type" sortable="true"  />
  	<display:column property="MAPDATA" title="Demand Sheet Map data" sortable="true"  />
	<display:column property="STACKFL" title="Stack/NoStack" sortable="true"  />
 </display:table> 
		</td>
	</tr>
</table>
<% } catch(Exception exp) { exp.printStackTrace(); } %>
</BODY>

</HTML>
