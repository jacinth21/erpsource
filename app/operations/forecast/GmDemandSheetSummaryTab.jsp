 <!-- \operations\forecast\GmDemandSheetSummaryTab.jsp -->
 <%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>


<bean:define id="strDemandMasterId" name="frmDemandSheetSummary" property="demandSheetId" type="java.lang.String"> </bean:define>
<bean:define id="strDemandSheetId" name="frmDemandSheetSummary" property="demandSheetMonthId" type="java.lang.String"> </bean:define>
<bean:define id="strMonthId" name="frmDemandSheetSummary" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="strYearId" name="frmDemandSheetSummary" property="yearId" type="java.lang.String"> </bean:define>
<bean:define id="strUnitRPrice" name="frmDemandSheetSummary" property="unitrprice" type="java.lang.String"> </bean:define>
<bean:define id="strtypeId" name="frmDemandSheetSummary"  property="demandTypeId" type="java.lang.String"></bean:define>
<bean:define id="strstatusId" name="frmDemandSheetSummary"  property="statusId" type="java.lang.String"></bean:define>
<bean:define id="strDemandTypeId" name="frmDemandSheetSummary"  property="demandTypeId" type="java.lang.String"></bean:define>
<bean:define id="strstatus" name="frmDemandSheetSummary"  property="statusId" type="java.lang.String"></bean:define>

<%
Object bean = pageContext.getAttribute("frmDemandSheetSummary", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger
String strGeneralCommentsID = strDemandMasterId+strMonthId+strYearId;

String strLimitedAccess = GmCommonClass.parseNull(request.getParameter("bolLimitedAccess"));
boolean bolLimitedAccess = false;
if(strLimitedAccess!=null && strLimitedAccess.equalsIgnoreCase("true")){
	bolLimitedAccess = true;
}
%>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script>
function load()
 {  
  if(document.frmDemandSheetSummary.unitrprice !=null)
   {
  if("<%=strUnitRPrice%>" == "Unit")
   {
  	   document.frmDemandSheetSummary.unitrprice[0].checked="yes";
  	}
  else
  {
     document.frmDemandSheetSummary.unitrprice[1].checked="yes";
  }	
  }      
 }			
 </script>
 <html:hidden property="demandSheetMonthId" />
 <html:hidden property="demandTypeId" />
 <input type="hidden" name="hDemandMasterID" value="<%=strDemandMasterId%>">
 <input type="hidden" name="hMonthID" value="<%=strMonthId%>">
 <input type="hidden" name="hYearId" value="<%=strYearId%>">
 
						<table>						
						<tr><td>
						<% // load only if any consignment is loaded 
							if (!strstatusId.equals(""))
						{ %> 
						<ul id="demandtab" class="shadetabs">
						<li><a href="/gmDSSummary.do?demandSheetMonthId=<%=strDemandSheetId%>&demandSheetId=<%=strDemandMasterId%>&strOpt=filterfetch" rel="ajaxcontentarea"  class="selected" title="Filter">Filter</a></li>						
						<li><a href="/gmDSRegionInfo.do?demandSheetMonthId=<%=strDemandSheetId%>&demandSheetId=<%=strDemandMasterId%>"
							 rel="ajaxcontentarea" title="Summary">Summary</a></li>	
							 <%if(!strDemandTypeId.equals("40023")){ %>					
						<li><a href="/gmDSGrowthSummary.do?demandSheetMonthId=<%=strDemandSheetId%>&hTypeId=<%=strtypeId%>"
							 rel="ajaxcontentarea" id="growthinfo" title="Growth Info">Growth Info</a></li>
						<li><a href="/gmDSPartHistory.do?demandSheetMonthId=<%=strDemandSheetId%>"
							 rel="ajaxcontentarea" title="Override Summary">Override Summary</a></li>	
						<li><a href="/gmDSRequestDetail.do?demandSheetMonthId=<%=strDemandSheetId%>&demandTypeId=50632"
							 rel="ajaxcontentarea" title="Open Request" >Open R </a></li>								
						<li><a href="/gmDSRequestDetail.do?demandSheetMonthId=<%=strDemandSheetId%>&demandTypeId=50631"
							 rel="ajaxcontentarea" title="New Request" >New R </a></li>	
						<li><a href="/gmDSGrowthRequestDetail.do?demandSheetMonthId=<%=strDemandSheetId%>"
							 rel="ajaxcontentarea" title="Growth vs Request" >Growth vs Request</a></li>	 
							 <%} %>  					
						<%	// If approved then hide general comment section 
							strstatusId = !strstatusId.equals("50550")?"Y":""; %>							 
						<li><a href="/operations/forecast/GmDemandSheetGComment.jsp?demandSheetMonthId=<%=strDemandSheetId%>&hID=<%=strDemandMasterId%>&hStatus=<%=strstatusId%>"
							 rel="ajaxcontentarea" title="Sheet Comments">Sheet Comments</a></li>
						<li><a href="/operations/forecast/GmDemandSheetGComment.jsp?demandSheetMonthId=<%=strDemandSheetId%>&hID=<%=strGeneralCommentsID%>&hStatus=<%=strstatusId%>"
							 rel="ajaxcontentarea" title="Monthly Comments">Monthly Comments</a></li>
						<%if(strDemandTypeId.equals("40023")){ %>
						 <li><a href="/gmOprOpenSheets.do?demandSheetMonthId=<%=strDemandSheetId%>&strOpt=opensheets&statusId=<%=strstatus%>"
							 rel="ajaxcontentarea" title="Multipart Open Sheets">Multipart Open Sheets</a></li>
							<%} if(!bolLimitedAccess){%> 	 							 	 
						<li><a href="/gmDSSummary.do?demandSheetMonthId=<%=strDemandSheetId%>&strOpt=approvefetch"
							 rel="ajaxcontentarea" title="Approval">Approval</a></li>
						 <%} %>
						</ul>
						
						<div id="ajaxdivtabarea" class="contentstyle" style="display:visible;height: 130px; overflow: auto;">												
						</div>
						<%} //end of ajax condition
						%>						
					</td></tr> 
					
					
					<!-- step 1. add div tag start and end, call it ajaxreportarea   -->
				<tr><td colspan ="7" align=center> 
				<div id="ajaxreportarea" class="contentstyle" style="display:visible;height: 130px; ">
				<table>
				<!-- 	<tr><td colspan="7" class="Line" width="1"></td></tr>   --> 
					<tr> <td>
						<!-- <td colspan ="7" align=center>  --> 
							<!--  step 2 : remove include jsp from here   --> 
							<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
							<jsp:include page= "/operations/forecast/GmIncludeSheetSummary.jsp" />
							<!-- //step 3 - copy ajaxpage method from their sample code to ajaxtabs.js or separate js and include at top  -->
						</td>
					</tr>
				</table>
			    </div></td></tr>			    
				</table>
					

