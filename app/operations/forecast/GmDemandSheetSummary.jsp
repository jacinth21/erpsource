<%
/**********************************************************************************
 * File		 		: GmDemandSheetSummary.jsp
 * Desc		 		: Report for Demand Sheet
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
 <!-- \operations\forecast\GmDemandSheetSummary.jsp -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ include file="/common/GmHeader.inc" %>


<bean:define id="strstatusId" name="frmDemandSheetSummary"  property="statusId" type="java.lang.String"></bean:define>
<bean:define id="strDemandSheetId" name="frmDemandSheetSummary"  property="demandSheetId" type="java.lang.String"></bean:define>
<bean:define id="strDemandSheetName" name="frmDemandSheetSummary"  property="searchdemandSheetId" type="java.lang.String"></bean:define>

<% //Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger
//String strWikiPath = GmCommonClass.getString("GWIKI");
String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_SUMMARY");

String strLimitedAccess= GmCommonClass.parseNull((String)request.getAttribute("DMNDSHT_LIMITED_ACC"));
boolean bolLimitedAccess = false;
if(strLimitedAccess!=null && strLimitedAccess.equalsIgnoreCase("true")){
	bolLimitedAccess = true;
}
try{
  	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet Summary</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/DemandSheet.js"></script>
<script>
function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmDemandSheetSummary.strOpt.value = 'save';
	document.frmDemandSheetSummary.submit();
}

function fnLoad()
{
	document.frmDemandSheetSummary.selectAll.checked = false;
	document.frmDemandSheetSummary.strOpt.value = "";
	document.frmDemandSheetSummary.action = "/gmRptByTreatment.do?method=expectedFollowUpReport";
	document.frmDemandSheetSummary.submit();
}

function fnSheetFetch()
{
	document.frmDemandSheetSummary.strOpt.value = "fetch";
	document.frmDemandSheetSummary.demandSheetMonthId.value = "";
	fnStartProgress('Y');
	document.frmDemandSheetSummary.submit();
}

	

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmDSSummary.do">
<html:hidden property="strOpt" value = ""/>



	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Demand Sheet Summary</td>
			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='/images/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="7" class="ELine"></td></tr> 
                    <!-- Custom tag lib code modified for JBOSS migration changes -->
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;Sheet Name:</td>
						<td><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="demandSheetId" />
					<jsp:param name="METHOD_LOAD" value="loadDemandSheetList&searchType=PrefixSearch" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strDemandSheetName %>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strDemandSheetId %>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="" />
			     	</jsp:include>
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;Month:</td>
						<td >&nbsp;
							<gmjsp:dropdown controlName="monthId" SFFormName="frmDemandSheetSummary" SFSeletedValue="monthId"
								SFValue="alMonth" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;Year:</td>
						<td >&nbsp;
							<gmjsp:dropdown controlName="yearId" SFFormName="frmDemandSheetSummary" SFSeletedValue="yearId"
								SFValue="alYear" codeId = "CODENM"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
						<td>
							<gmjsp:button value="&nbsp;Load&nbsp;&nbsp;" gmClass="button" onClick="fnSheetFetch();" buttonType="Load" />
						</td>
					</tr>	
					<tr><td colspan="7">				
					<jsp:include page= "/operations/forecast/GmDemandSheetSummaryTab.jsp" >
					<jsp:param name="bolLimitedAccess" value="<%=bolLimitedAccess%>" />
					</jsp:include>
					  <% if (!strstatusId.equals(""))  { %>
						<script type="text/javascript">								    		
							var demandtab=new ddajaxtabs("demandtab", "ajaxdivtabarea");				
					    	demandtab.setpersist(false);								
					    	demandtab.init();			
					    <% } %>		    	
						</script>
					</tr></td>
				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
</BODY>

</HTML>

