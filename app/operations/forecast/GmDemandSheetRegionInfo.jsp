 <%
/**********************************************************************************
 * File		 		: GmDemandSheetRegionInfo.jsp
 * Desc		 		: Report for Demand Sheet Region Details
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
  <!-- \operations\forecast\GmDemandSheetRegionInfo.jsp -->
  <%@ include file="/common/GmHeader.inc" %>
 
<!-- WEB-INF path corrected for JBOSS migration changes -->
  
    
  <%
     
	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);
	
	 
    try
    {	
	   
  %>
  <html> 

	<head>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	</head>
  	<body>
  	 <TABLE  cellSpacing=0 cellPadding=0  border=0> 
  	 <tr> <td width="20%"> <table>
  	 	<tr><td class=borderDark colspan="3" noWrap width=1></td></tr>
  	 	<tr> <td class=borderDark width=1></td>    
  	 	<td>
  	 	<!--  display:Table tag here --> 
		<display:table cellspacing="0" cellpadding="0" class="its" name="requestScope.frmDemandSheetSummary.alRegionList"  requestURI="/gmDSRegionInfo.do"  >				    																		     							
		 																
		<display:column property="REF_NAME" title="Mapping Details" style="text-align:left"  />								
		 
		</display:table>
		
		</td><td class=borderDark width=1></td></tr>
		<tr><td class=borderDark colspan="3" noWrap width=1></td></tr>    
		</table> </td> 
		<td width="20%" valign="top" ><table class="border" cellspacing="1">
		<tr>
							<td class="RightBlueCaption" height="20" >&nbsp;Demand Period:</td>
							<td class="RightTableCaption">&nbsp;<bean:write name="frmDemandSheetSummary" property="demandPeriod"/>
							&nbsp;months</td>
						</tr>	
						<tr>	
							<td  class="RightBlueCaption" height="20">&nbsp;Forecast Period:</td>
							<td class="RightTableCaption">&nbsp;<bean:write name="frmDemandSheetSummary" property="forecastPeriod"/>
							&nbsp;months</td>
						</tr>
						<tr>
							<td  class="RightBlueCaption" height="20" >&nbsp;Set Build Forecast:</td>
							<logic:notEqual  name="frmDemandSheetSummary" property="requestPeriod" value="">
							<td class="RightTableCaption">&nbsp;<bean:write name="frmDemandSheetSummary" property="requestPeriod"/> &nbsp;months</td>	
							</logic:notEqual>
							<logic:equal  name="frmDemandSheetSummary" property="requestPeriod" value="">	
							<td class="RightTableCaption">&nbsp;<bean:write name="frmDemandSheetSummary" property="requestPeriod"/>  N/A</td>	
							</logic:equal>
						</tr>
						<tr>
							<td  class="RightBlueCaption" height="20" >&nbsp;Load Date:</td>
							<td class="RightTableCaption">&nbsp;<bean:write name="frmDemandSheetSummary" property="loadDate"/></td>		
						</tr><tr>
							<td  class="RightBlueCaption" height="20" >&nbsp;Status:</td>			
							<td class="RightTableCaption">&nbsp;<bean:write name="frmDemandSheetSummary" property="status"/>
							</td>					
						</tr>
						<tr>
							<td  class="RightBlueCaption" height="20" >&nbsp;Type:</td>
							<td class="RightTableCaption">&nbsp;<bean:write name="frmDemandSheetSummary" property="demandTypeNm"/>
							</td>					
						</tr>					
	</table></td></tr>					
		</TABLE>
		</body>
</html>		
<%	}catch(Exception e)
	{
		e.printStackTrace();
	}
	%>
