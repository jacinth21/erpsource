<%
/**********************************************************************************
 * File		 		: GmDemandSheetParSetup.jsp
 * Desc		 		: Map the part to Demand Sheet
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<!-- \operations\forecast\GmDemandSheetParSetup.jsp -->

<%@ include file="/common/GmHeader.inc" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import="org.apache.commons.beanutils.BeanMap, org.apache.commons.beanutils.RowSetDynaClass, org.apache.commons.beanutils.BasicDynaBean,org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.util.ArrayList,java.util.Iterator" %>
<bean:define id="strDemandSheetId" name="frmDemandSheetSetup"  property="demandMasterId" type="java.lang.String"></bean:define>
<bean:define id="strDemandSheetName" name="frmDemandSheetSetup"  property="searchdemandMasterId" type="java.lang.String"></bean:define>
<% 
// Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger 
//String strWikiPath = GmCommonClass.getString("GWIKI");
String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_PAR_SETUP");
 
try { 
	//String strServletPath = GmCommonClass.getString("GMSERVLETPATH"); 

ArrayList alList = new ArrayList();
 
alList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("RDDEMANDPAR"));
 
int intRows = 0;
intRows = alList.size();

StringBuffer sbInput = new StringBuffer();
String strPar = "";
String strInput = "";
 
DynaBean db ;

	for (int i=0;i<intRows;i++)
	{
		db = (DynaBean)alList.get(i);
		strPar = String.valueOf(db.get("PNUM"));
		sbInput.append(strPar);
		sbInput.append("^");
	}
	 strInput = sbInput.toString();
	// System.out.println("string" + strInput);
	
//String strpop = (String)request.getAttribute("PopType");
 

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet PAR Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
 

function fnFetch()
{	
	
	document.frmDemandSheetSetup.strOpt.value = 'edit';
	fnStartProgress('Y');
	document.frmDemandSheetSetup.submit();
	
}

function fnLoad()
{
	var strpop = document.frmDemandSheetSetup.popType.value; 
	if(strpop=="pop")
	{ 
		document.frmDemandSheetSetup.Load.disabled = true;
		document.frmDemandSheetSetup.demandMasterId.disabled = true;
		document.frmDemandSheetSetup.partNumbers.disabled = true;
		}
}

 
function fnSubmit()
{
	var varRows = <%=intRows%>;
    var strpop = document.frmDemandSheetSetup.popType.value; 
	var varDMId =  document.frmDemandSheetSetup.demandMasterId.value;
	var varDSId =  document.frmDemandSheetSetup.demandSheetId.value; 
	 
	var inputString = ''; 
	var varInput = "<%=strInput%>";
	
	var arrPar = varInput.split('^');
	if(varDMId!=null)
	{
	for (var i =0; i < varRows; i++) 
	{
		obj = eval("document.frmDemandSheetSetup.parvalue"+i);
		varPar = obj.value;
	 
	 
		inputString = inputString + arrPar[i] + '^'+ varDMId +'^'+ varPar +  '|';
		if(strpop=="pop") 
		{
			document.frmDemandSheetSetup.hpartnumber.value = arrPar[i];
			document.frmDemandSheetSetup.hparvalue.value = varPar;
		}
		   
	}
	//alert("input is " + inputString);
	}
	
	 
	document.frmDemandSheetSetup.strOpt.value =  'save'; //'save';
	document.frmDemandSheetSetup.hinputStr.value = inputString;
	fnStartProgress('Y');
	document.frmDemandSheetSetup.submit();	
	
	if(strpop=="pop")
	{
		fnClose();
		}
}

 		
function fnLog(type,id)
{
	var varDSId = document.frmDemandSheetSetup.demandMasterId.value;
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id+"-"+varDSId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
 

function fnParHistory(partid, logtype)
{
	var varDSId = document.frmDemandSheetSetup.demandMasterId.value;
	windowOpener("/gmDSParHistory.do?demandMasterId="+varDSId+"&partNumbers="+encodeURIComponent(partid)+"&logType="+logtype+"&logId="+partid+"-"+varDSId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnClose()
{
 var url = window.opener.document.URL;
  if(url.indexOf("gmDSSummary") != -1)
   {
    
     window.opener.fnSheetFetch();
   }
  else if (url.indexOf("gmTTPMonthlySummary") != -1)
   {
     window.opener.fnReport();
   }  
  window.close();
}
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  onLoad="fnLoad()" >
<html:form action="/gmDSParSetup.do">

<html:hidden property="strOpt" />
<html:hidden property="haction" />
<html:hidden property="hinputStr" />
<html:hidden property="logType" /> 
<html:hidden property="logId" />  
<html:hidden property="popType" />
<html:hidden property="demandSheetId" />
<html:hidden property="hpartnumber" />
<html:hidden property="hparvalue" /> 

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Demand Sheet PAR Setup</td>
			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"></font>&nbsp;Demand
							Sheet Name :</td>
						<td width="70%" style="padding-left: 8px;">
					<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="demandMasterId" />
					<jsp:param name="METHOD_LOAD" value="loadDemandSheetList&searchType=PrefixSearch" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strDemandSheetName %>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strDemandSheetId%>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="" />
					</jsp:include>
			
						</td>
					</tr>
				 
                    <tr><td colspan="2" class="ELine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Part Numbers :</td> 
                        <td>&nbsp;
	                         <html:text property="partNumbers"  size="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 
	                         &nbsp;<a title="You can perform wildcard search. For eg: type 101 to pull up all parts that has 101 in it"><img src=<%=strImagePath%>/question.gif border=0/></a>
	                        &nbsp;&nbsp;&nbsp; <gmjsp:button name="Load" value="&nbsp;&nbsp;Load&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnFetch();" buttonType="Load" />
	                        &nbsp;&nbsp;
	                    
                        </td> 
                    </tr>       
                    <tr><td colspan="2" class="ELine"></td></tr>
           <tr> 
            <td colspan="2">
            
            <display:table name="RDDEMANDPAR" requestURI="/gmDSParSetup.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.displaytag.beans.DTParWrapper"> 
			 
			 <display:column property="PNUM" title="Part Number" sortable="true" class="aligncenter" style="width:100px" /> 
			<display:column property="PDESC" title="Part Description" sortable="true" class="alignleft"/> 
			<display:column property="PARVALUE" title="Par Value"  class="aligncenter"/> 
			<display:column property="HISTORY"  title=""  class="aligncenter"/> 
			<display:column property="COMMENTS" title="Comments"  class="aligncenter"/> 
			</display:table> 
			
		</td>
    </tr> 
    <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
                    	<td colspan="2" align="center">
		                     <gmjsp:button value="Submit" gmClass="button" onClick="fnSubmit();" buttonType="Save" />  
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
<% 
} catch (Exception exp) {
exp.printStackTrace();
}
%>
</BODY>

</HTML>

