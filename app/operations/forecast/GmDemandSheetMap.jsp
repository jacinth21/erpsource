<%
/**********************************************************************************
 * File		 		: GmDemandSheetMap.jsp
 * Desc		 		: mapping Group / Set / Region / InHouse
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ include file="/common/GmHeader.inc" %>
  <!-- \operations\forecast\GmDemandSheetMap.jsp -->
<% 
// Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger 
// String strServletPath = GmCommonClass.getString("GMSERVLETPATH"); 

// String strWikiPath = GmCommonClass.getString("GWIKI");
String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_MAPPING");
try { %>

<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
function fnReset()
{
	
}	

function fnFetch()
{	
	document.frmDemandSheetMap.strOpt.value = "edit";
	document.frmDemandSheetMap.submit();
}

function fnSubmit()
{
	var val = document.frmDemandSheetMap.requestPeriod.value;
	var DStype = document.frmDemandSheetMap.demandSheetTypeId.value;
	if (val != "" && isNaN(val))
    	{   
    	  Error_Details(message[400]);
		 
   		}
   	if( DStype =='40023')
	{
		 Error_Details('This sheet cannot be associated with the type "Multi Part"');
	}	
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
		 
	document.frmDemandSheetMap.strOpt.value = 'save';
	fnStartProgress('Y');
	document.frmDemandSheetMap.submit();
}

function fnVoid()
{
		document.frmDemandSheetMap.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmDemandSheetMap.hAction.value = "Load";
		document.frmDemandSheetMap.hTxnId.value = document.frmDemandSheetMap.refId.value
		document.frmDemandSheetMap.submit();
}

function fnCheckSelections()
{
	objCheckGSArrLen = 0;
	objCheckRIArrLen = 0;
	
	objCheckGSArr = document.frmDemandSheetMap.checkSelectedGS;
	if(objCheckGSArr) {
	if (objCheckGSArr.type == 'checkbox')
	{
		objCheckGSArr.checked = true;
	}
	else {
		objCheckGSArrLen = objCheckGSArr.length;
		for(i = 0; i < objCheckGSArrLen; i ++) 
			{	
				objCheckGSArr[i].checked = true;
			}
		}
	}
	
	objCheckRIArr = document.frmDemandSheetMap.checkSelectedRI;
	if (objCheckRIArr) {
		// check if there is just one element in checked. if so mark it as selected
	if (objCheckRIArr.type == 'checkbox')
	{
		objCheckRIArr.checked = true;
	}
	else {
		objCheckRIArrLen = objCheckRIArr.length;
		for(i = 0; i < objCheckRIArrLen; i ++) 
			{	
				objCheckRIArr[i].checked = true;
			}
		}
	}
	
	var DStype = document.frmDemandSheetMap.demandSheetTypeId.value;
	//alert(" test is " + test);
	if(DStype =='40020')
	{
		document.frmDemandSheetMap.requestPeriod.disabled = true;
		}
		
	var varRP = document.frmDemandSheetMap.requestPeriod.value;
	if( varRP == '' && DStype !='40020')
	{
		document.frmDemandSheetMap.requestPeriod.value = 2;
		}
	if( varRP == '' && DStype =='40020')
	{
		document.frmDemandSheetMap.requestPeriod.value = 0;
		}	
}	

function fnClose()
{
window.opener.fnFetch();
window.close();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnCheckSelections();">
<html:form action="/gmDemandGroupMap.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="demandSheetId" />
<html:hidden property="demandSheetName" />
 
	<table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Demand Sheet Mapping</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="723" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="15%"></font>&nbsp;Demand	Sheet Name:</td>
						<td width="75%" colspan="2">&nbsp;
							<bean:write name="frmDemandSheetMap" property="demandSheetName" />
						</td>
					</tr>
                    <tr><td colspan="3" class="Line"></td></tr>
                    <!-- Custom tag lib code modified for JBOSS migration changes -->
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"  width="15%"></font>&nbsp;Demand Sheet Type :</td> 
	                     <td colspan="2">&nbsp;
	                     <gmjsp:dropdown controlName="demandSheetTypeId" SFFormName="frmDemandSheetMap" SFSeletedValue="demandSheetTypeId"
							SFValue="alDemandSheetType" codeId = "CODEID"  codeName = "CODENM" onChange="fnFetch();"  defaultValue= "[Choose One]" />													
    		             </td>
                    </tr>  
                      <tr><td colspan="3" class="Line"></td></tr>
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="25%"></font>&nbsp;Set Build Forecast:</td>
						<td width="75%" colspan="2">&nbsp;
							<html:text property="requestPeriod"  size="5" maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;(months)
						</td>
					</tr>
                    
                    <tr><td colspan="3" class="Line"></td></tr>
                    <tr>
                    	<td class="RightTableCaption" align="right" HEIGHT="30"  width="15%"></font>&nbsp;Group / Set :</td> 
                        <td width="250">&nbsp;Available
                        <DIV style="display:visible;height: 160px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                        <table>
                        	<logic:iterate id="unSelectedGSlist" name="frmDemandSheetMap" property="alUnSelectedGS">
                        	<tr>
                        		<td>
							    	<htmlel:multibox property="checkUnSelectedGS" value="${unSelectedGSlist.ID}" />
								    <bean:write name="unSelectedGSlist" property="NAME" />
								</td>
							</tr>	    
							</logic:iterate>
						</table>
						</DIV><BR>
                        </td>
                         <td>&nbsp;Associated
                          <DIV style="display:visible;height: 160px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                         <table>
                        	<logic:iterate id="selectedGSlist" name="frmDemandSheetMap" property="alSelectedGS">
                        	<tr>
                        		<td>
								    <htmlel:multibox property="checkSelectedGS" value="${selectedGSlist.ID}" />
								    <bean:write name="selectedGSlist" property="NAME" />
	     						</td>
							</tr>	    
							</logic:iterate>
							</table>
							</DIV><BR>
                        </td>
                    </tr>  
                    <tr><td colspan="3" class="Line"></td></tr>
                    <tr>
                    	<td class="RightTableCaption" align="right" HEIGHT="30"  width="15%"></font>&nbsp;Regions / InHouse :</td> 
                        <td>&nbsp;Available
                         <DIV style="display:visible;height: 160px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                        <table>
                        	<logic:iterate id="unSelectedRIlist" name="frmDemandSheetMap" property="alUnSelectedRI">
                        	<tr>
                        		<td>
							    	<htmlel:multibox property="checkUnSelectedRI" value="${unSelectedRIlist.ID}" />
								    <bean:write name="unSelectedRIlist" property="NAME" />
								</td>
							</tr>	    
							</logic:iterate>
						</table>
						</DIV><BR>
                        </td>
                         <td>&nbsp;Associated
                          <DIV style="display:visible;height: 160px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                         <table>
                        	<logic:iterate id="selectedRIlist" name="frmDemandSheetMap" property="alSelectedRI">
                        	<tr>
                        		<td>
								    <htmlel:multibox property="checkSelectedRI" value="${selectedRIlist.ID}" />
								    <bean:write name="selectedRIlist" property="NAME" />
	     						</td>
							</tr>	    
							</logic:iterate>
							</table>
							</DIV><BR>
                        </td>
                    </tr>  
                            <tr><td colspan="3" class="Line"></td></tr>
                    <tr>
                    	<td colspan="3" align="center" height="30">
		                    <gmjsp:button value="Submit" gmClass="button" onClick="fnSubmit();" buttonType="Save" /> 
							<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="fnClose();" buttonType="Load" />
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
<% 
} 
catch (Exception e) {
e.printStackTrace();
}
%>
</BODY>

</HTML>

