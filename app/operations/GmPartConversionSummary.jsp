<%
/**********************************************************************************
 * File		 		: GmPCSummary.jsp
 * Desc		 		: Report for PC Summary
 * Version	 		: 1.0
 * author			: Rakhi G
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmtPCSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPCSummary:setLocale value="<%=strLocale%>"/>
<fmtPCSummary:setBundle basename="properties.labels.operations.GmPartConversionSummary"/>
<!-- operations\GmPartConversionSummary.jsp -->
<bean:define id="strFromDate" name="frmPCSummary" property="fromDate" type="java.lang.String"> </bean:define>
<bean:define id="strTodate" name="frmPCSummary" property="toDate" type="java.lang.String"> </bean:define>

<% 
try{

	
	String strApplDateFmt = strGCompDateFmt;
	
	// to draw lines for every other second column
	
	Date dtFrmDate = null;
	Date dtToDate = null;
			
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFromDate,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strTodate,strApplDateFmt);
			
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Part Conversion Summary </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnSubmit()
{
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmPCSummary.strOpt.value = "Report";
	document.frmPCSummary.submit();
}


function fnReport()
{
	if( TRIM( eval(document.all.pcID).value ) == ''  && 
	    TRIM( eval(document.all.fromDate).value ) == '' &&
	    TRIM( eval(document.all.toDate).value ) == '' && 
	    TRIM( eval(document.all.fromPartNumber).value ) == '' && 
	    TRIM( eval(document.all.toPartNumber).value ) == '' &&
	    (eval(document.all.typeId)).value == '0'  )	
		Error_Details("Please select anyone of the filter condition.");
		
	document.frmPCSummary.action = "/gmPCSummary.do?method=reportPCSummary";
	fnStartProgress('Y');
	fnSubmit();
}	

function fnPrint(){
	window.print();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" > <html:form action="/gmPCSummary.do"  > 
<html:hidden property="strOpt" value="" />
<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
	<tr> 
		<td height="25" class="RightDashBoardHeader"><fmtPCSummary:message key="LBL_PART_CONV_SUM"/></td> </tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="100" valign="top">
				<table border="0" cellspacing="0" cellpadding="0" >
				<tr>
	            	<td class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;<fmtPCSummary:message key="LBL_PC#"/>:</td>
	            	<td>&nbsp;
    		        	<html:text property="pcID"  size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
    		         </td>
	                 <td class="RightTableCaption"  height="30" align="right" nowrap><fmtPCSummary:message key="LBL_PC_TYPE"/> :</td>
   					<td>&nbsp;
						<gmjsp:dropdown controlName="typeId" SFFormName="frmPCSummary" SFSeletedValue="typeId"
						SFValue="alType" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" />													
					 </td>
				</tr>
				<tr><td colspan="4" class="ELine"></td></tr>
				<tr>
					<td class="RightTableCaption"  height="30"  align="right"  nowrap>&nbsp;<fmtPCSummary:message key="LBL_FRM_PART_NUM"/> :</td> 
    	            <td>&nbsp;
    		        	<html:text property="fromPartNumber"  size="10" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
    		        </td>
    		        <td class="RightTableCaption"  height="30"  align="right"  nowrap>&nbsp;<fmtPCSummary:message key="LBL_TO_PART_NUM"/> :</td> 
    	            <td>&nbsp;
    		        	<html:text property="toPartNumber"  size="10" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
    		        </td>
				</tr>
                  <tr><td colspan="4" class="ELine"></td></tr>
				<tr>
				<!-- Struts tag lib code modified for JBOSS migration changes -->
                  <td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtPCSummary:message key="LBL_FROM_DATE"/>:</td> 
                  <td>&nbsp;
						<gmjsp:calendar textControlName="fromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex=""/>						
    		        </td>
   		            <td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtPCSummary:message key="LBL_TO_DATE"/>:</td> 
                    <!-- Struts tag lib code modified for JBOSS migration changes -->
                    <td>&nbsp;
                    	<gmjsp:calendar textControlName="toDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex=""/>
						<fmtPCSummary:message key="BTN_GO" var="varGo"/><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" onClick="fnReport();" buttonType="Load" />&nbsp;&nbsp;
    		        </td></tr>
                    <tr><td colspan="4" class="ELine"></td></tr>
        <tr><td colspan="4">           
		<!--  display:Table tag here --> 
		<display:table cellspacing="0" cellpadding="0" class="its" name="requestScope.frmPCSummary.ldtResult" export="true" requestURI="/gmPCSummary.do?method=reportPCSummary"  >				    																		     							
		<fmtPCSummary:message key="DT_TXN_ID" var="varTxnId"/><display:column property="PCID" title="${varTxnId}"   sortable="true"   href="gmPCDetail.do?method=reportPCDetail"  paramId="pcID"  />	<!-- form should have field PCID - what paramId has. -->																	
		<fmtPCSummary:message key="DT_PART#" var="varPart"/><display:column property="FROMPNUM" title="${varPart}" style="text-align:left"    sortable="true" />								
		<fmtPCSummary:message key="DT_PC_DESC" var="varPcDesc"/><display:column property="FROMPNUMDESC" escapeXml="true" title="${varPcDesc}"     maxLength="40" />
		<fmtPCSummary:message key="DT_TO_PART#" var="varToPart"/><display:column property="TOPNUM" title="${varToPart}" style="text-align:left"    sortable="true" />								
		<fmtPCSummary:message key="DT_TO_PART_DESC" var="varToPartDesc"/><display:column property="TOPNUMDESC"  escapeXml="true" title="${varToPartDesc}"     maxLength="40" />
		<fmtPCSummary:message key="DT_PC_TYPE" var="varPcType"/><display:column property="PCTYPE" title="${varPcType}"    sortable="true" />
		<fmtPCSummary:message key="DT_PC_QTY" var="varPcQty"/><display:column property="PCQTY" title="${varPcQty}" style="text-align:right"   />
		<fmtPCSummary:message key="DT_CREATED_BY" var="varCreatedBy"/><display:column property="CREATEDBY" title="${varCreatedBy}" style="text-align:center" />
		</display:table>
		
		</td></tr>		
</table>										
</td></tr></table>			   	
</html:form>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>

