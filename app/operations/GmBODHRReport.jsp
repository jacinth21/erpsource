<%
/**********************************************************************************
 * File		 		: GmBODHRReport.jsp
 * Desc		 		: This screen is used for displaying the part details 
 * Version	 		: 1.0
 * author			: Brinal
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtBODHRReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmBODHRReport.jsp -->

<fmtBODHRReport:setLocale value="<%=strLocale%>"/>
<fmtBODHRReport:setBundle basename="properties.labels.operations.GmBODHRReport"/>


<bean:define id="dhrPartResult" name="frmLogisticsReport" property="ldhrPartResult" type="java.util.List"> </bean:define>
 
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("BO_DHR");
%>

<HTML>
<HEAD>
<TITLE> DHR Report - Backorder Part Details </TITLE> 
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnGo()
 { 
 	var varpartNum = document.frmLogisticsReport.partNum.value;
 	
 	 document.frmLogisticsReport.fulfillBoFlag.value = "true";
   if (fnCheckSelections()=='' && varpartNum=='' ){   
   		Error_Details(message_operations[500]);
   		Error_Show();
		Error_Clear();
		return false;
   }   
  
  document.frmLogisticsReport.go.disabled = true;		
  document.body.style.cursor = 'wait'; 
  fnStartProgress();
  document.frmLogisticsReport.submit();
 }
 
 function fnCheckSelections()
{
	objCheckArrLen = 0;
	 
	flag = '';
	 
	objCheckArr = document.frmLogisticsReport.checkSelectedProjects;
	if(objCheckArr) {
	 
		objCheckArrLen = objCheckArr.length;
		for(i = 0; i < objCheckArrLen; i ++) 
			{	
			    if(objCheckArr[i].checked==true)
				flag = '1';
			}
		 
	}
	return flag
}	
 
 function fnSelectAll(varCmd)
{
				objSelAll = document.all.selectAll;
				if (objSelAll ) {
				objCheckSheetArr = document.all.checkSelectedProjects;
//				objCheckSiteArrLen = objCheckSiteArr.length;
				
					
				if ((objSelAll.checked || varCmd == 'selectAll') && objCheckSheetArr)
				{
					objSelAll.checked = true;
					objCheckSheetArrLen = objCheckSheetArr.length;
					
					objCheckSheetArr1 = document.frmLogisticsReport.checkSelectedProjects;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = true;
						}
													
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = true;
					}
				 
				 
				}
				else if (!objSelAll.checked  && objCheckSheetArr ){
					objCheckSheetArrLen = objCheckSheetArr.length;
					
					objCheckSheetArr1 = document.frmLogisticsReport.checkSelectedProjects;
					if (objCheckSheetArr1.type == 'checkbox')
						{
							objCheckSheetArr1.checked = false;
						}
					
					for(i = 0; i < objCheckSheetArrLen; i ++) 
					{	
						objCheckSheetArr[i].checked = false;
					}
			
					}
				}
			 	 
	}
	
	
	
  
</script>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmBODHRReport.do?method=loadBODHRReport" >
<html:hidden property="strOpt" value=""/>
<table  class="DtTable850" cellspacing="0" cellpadding="0" width="100%">

		<tr>			
			<td height="25" class="RightDashBoardHeader" colspan=6><fmtBODHRReport:message key="LBL_DHR_REPOERT"/></td>
			<td align="right" class=RightDashBoardHeader><fmtBODHRReport:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img id='imgEdit'
				style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
				height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>		
		</tr>			 
					<tr>
						<td bgcolor="gainsboro"></td>
                    	<td class="RightTableCaption" align="right"   HEIGHT="24" ></font>&nbsp;<fmtBODHRReport:message key="LBL_PROJECT_LIST"/>:</td> 
                        <td  width="340">&nbsp; &nbsp;
                        <table> <tr><td width="300">&nbsp;&nbsp;<html:checkbox property="selectAll" onclick="fnSelectAll('toggle');" /><B><fmtBODHRReport:message key="LBL_SELECT_ALL"/> </B> </td>
							</tr></table> 
                        <DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >                     
                        <table>
                        	<logic:iterate id="SelectedSetlist" name="frmLogisticsReport" property="alProject" >
                        	<tr>
                        		<td>
							    	<htmlel:multibox property="checkSelectedProjects" value="${SelectedSetlist.ID}" />
							    	<bean:write name="SelectedSetlist" property="ID" /> /
								    <bean:write name="SelectedSetlist" property="NAME" />
								</td>
							</tr>	    
							</logic:iterate>
						</table>
						</DIV><BR>
                        </td>
						<td>
							<table>
							<!-- Struts tag lib code modified for JBOSS migration changes -->
							<tr>
								<td class="RightTableCaption" align="left"  HEIGHT="100" ><fmtBODHRReport:message key="LBL_PART_#"/>:&nbsp;
								<html:text property="partNum" size="30" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;							 
								</td>
							</tr>
							<tr>
								<td align="left"><html:checkbox	property="fulfillBoFlag" /> <fmtBODHRReport:message key="LBL_BO_QTY_FLAG"/> &nbsp;&nbsp;
								 <fmtBODHRReport:message key="BTN_GO" var="varGo"/>								 
								 <gmjsp:button  name="go" value="&nbsp;${varGo}&nbsp;" onClick="javascript:fnGo();" gmClass="button" buttonType="Load"/></td>
													
							</tr>							
							</table>
						</td>					
					</tr>	
					
		<tr class=Line>
			<td noWrap height=1 colspan=8></td>
		</tr>
					<tr> <td colspan="8">				
					<jsp:include page="/operations/GmBODHRReportInc.jsp">
						<jsp:param name="FORMNAME" value="frmLogisticsReport" />
					</jsp:include></td>
					</tr>	
</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</html:form>
