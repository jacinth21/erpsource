<%
/**********************************************************************************
 * File		 		: GmLotCodeTxnHistory.jsp
 * Desc		 		: This Jsp is for the Trasaction history
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLotTxnHist" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
  <!-- \operations\GmLotCodeTxnHistory.jsp--> 
<fmtLotTxnHist:setLocale value="<%=strLocale%>"/>
<fmtLotTxnHist:setBundle basename="properties.labels.operations.GmLotCodeReport"/>
<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<HTML>
<HEAD>

<script>
</script>
<!-- style added for PC-3659 support Edge alignment -->
 <style>
#HistDataGrid .objbox{
	 height: auto !important;
}
#HistDataGrid table
	{
	width:100% !important;
	margin-right:0 !important;
	padding-right:0 !important; 
	}
#HistDataGrid
{
	height: auto !important;
	width: 100% !important;
	margin:	0 !important;
}
</style> 

<BODY leftmargin="20" topmargin="10">
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td class="Line" colspan="5"></td></tr> 
			<tr>
					<td height="24" class="ShadeRightTableCaption" colspan="5">&nbsp;<b><fmtLotTxnHist:message key="TD_LOT_TXN_HIST_HEADER"/></b></td>
		    </tr>	
		    <tr id="DivHistGrid">
				<td>
				<div id="HistDataGrid" height="50"></div>
				</td>
			</tr>
			<tr id="DivHistNoDataMessage"><td colspan="3" height="20" align="Center"><fmtLotTxnHist:message key="TD_NO_DATA_FOUND"/></td></tr>
		</table>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>