 <%
/**********************************************************************************
 * File		 		: GmItemQuarantineReport.jsp
 * Desc		 		: This screen is used for the DashBoard Report
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Date"%>
<!-- Imports for Logger -->
<%@ include file="/common/GmHeader.inc"%>

 
<%@ page buffer="16kb" autoFlush="true" %>

<%@ taglib prefix="fmtItemQuarantineReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmItemQuarantineReport.jsp -->

<fmtItemQuarantineReport:setLocale value="<%=strLocale%>"/>
<fmtItemQuarantineReport:setBundle basename="properties.labels.operations.GmItemQuarantineReport"/>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	 
	String strApplDateFmt = strGCompDateFmt;//GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmParam = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMPARAM"));
	log.debug(" values inside Param " + hmParam);
	String strType = (String)request.getAttribute("hType") == null?"":(String)request.getAttribute("hType");
	String strFromDate = GmCommonClass.parseNull((String)hmParam.get("FROMDT"));
    String strToDate = GmCommonClass.parseNull((String)hmParam.get("TODT"));
    String strPartNum = GmCommonClass.parseNull((String)hmParam.get("PNUM"));
    String strTransactionId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
    String strRefId = GmCommonClass.parseNull((String)hmParam.get("REFID"));
    String strCreatedBy = GmCommonClass.parseNull((String)hmParam.get("CREATEDBY"));
    String strStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));
    String strControlNum = GmCommonClass.parseNull((String)hmParam.get("CONTROLNUM"));
    String strEtchId = GmCommonClass.parseNull((String)hmParam.get("ETCHID"));
	String strShowTransDetails = GmCommonClass.parseNull((String)request.getAttribute("showTransDetails"));
	String strHeader = "";
	String strLabel = "";
	String strLabel2 = "";
	String strDateFmt = "{0,date,"+ strGCompDateFmt +"}";


	String strId = "";
	String strName = "";

	strId = "PID";
	strName = "REASON";
	strLabel = "Purpose";
	strLabel2 = "Employee";

	ArrayList alCodeList = new ArrayList();
	ArrayList alReport = new ArrayList();
	ArrayList alStatus = new ArrayList();
	ArrayList alCreatedBy = new ArrayList(); 

	if (hmReturn != null) 
	{
		alCodeList = (ArrayList)hmReturn.get("CODELIST");
		alReport = (ArrayList)hmReturn.get("REPORT");
		alStatus = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALSTATUS"));
		alCreatedBy = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALCREATEDBY"));
	}
	
	int intSetLength = 0;
	if (alReport != null)
	{
		intSetLength = alReport.size();
	}
	log.debug(" Set Len  " + intSetLength);
	log.debug(" alReport is  " + alReport);
		
	int intLength = alCodeList.size();

	HashMap hmLoop = new HashMap();

	String strConsignee = "";

	String strConsignId = "";
	String strDate = "";
	String strComments = "";
	String strEmp = "";
	String strShade = "";
	String strCodeID = "";
	String strSelected = "";
	int intType = 0;
	
	String strWikiTitle = GmCommonClass.getWikiTitle("REPROCESS_MATERIAL_REPORT");
	
	Date dtFrmDate = null;
	Date dtToDate = null;
	
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFromDate,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDate,strApplDateFmt);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Item Consign Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>   
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>


<script>
var strApplDateFmt = '<%=strApplDateFmt%>';
var lblReportType = '<fmtItemQuarantineReport:message key="LBL_REPORT_TYPE"/>';
function fnLoadType()
{
//  Date different between From date and To date
var dateDiffVal = dateDiff(document.frmAccount.Txt_FromDate.value,document.frmAccount.Txt_ToDate.value,strApplDateFmt);
// if status is closed only fetch the value form 180 days.
if(document.frmAccount.Cbo_Status.value == 4){
	if (dateDiffVal > 180 || dateDiffVal < 0){
		Error_Details(message_operations[528]);
		Error_Show();
		Error_Clear();
		return false;
	}
}
if ( TRIM( eval(document.frmAccount.Cbo_Type).value) == '0' &&
TRIM( eval(document.frmAccount.Cbo_Status).value) == '0' &&
TRIM( eval(document.frmAccount.Txt_FromDate).value) == '' &&
TRIM( eval(document.frmAccount.Txt_ToDate).value) == '' &&
TRIM( eval(document.frmAccount.Txt_PartNum).value) == '' &&
TRIM( eval(document.frmAccount.Txt_TxnId).value) == '' &&
TRIM( eval(document.frmAccount.Txt_RefId).value) == '' &&
TRIM( eval(document.frmAccount.Txt_EtchId).value) == '' &&
TRIM( eval(document.frmAccount.Txt_ControlNum).value) == '' &&
TRIM( eval(document.frmAccount.Cbo_CreatedBy).value) == '0'
){
	Error_Details(message_operations[529]);
	}
if(document.frmAccount.chkDetails.checked &&  TRIM( eval(document.frmAccount.Cbo_Type).value) == '0')
{	
	fnValidateDropDn('Cbo_Type',lblReportType);
}
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	fnStartProgress('Y');
	document.frmAccount.submit();
}

function Toggle(val)
{
	
	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		trobj.className="";
		tabobj.style.background="#ffffff";
	}
}

function fnPrintVer(val,varType,conType)
{
	intType = parseInt(varType);
	/*
	if (intType > 4116){
	windowOpener("/GmReprocessMaterialServlet?hAction=ViewInHouse&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=250,left=200,width=800,height=700");
  }else{
	windowOpener("/GmConsignSetServlet?hAction=LoadConsign&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=250,left=200,width=800,height=300");
	}*/
	if(conType=='C'){
		windowOpener("/GmConsignSetServlet?hAction=LoadConsign&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=250,left=200,width=800,height=300");		
	}else{
		windowOpener("/GmReprocessMaterialServlet?hAction=ViewInHouse&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=250,left=200,width=800,height=700");		
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmReprocessMaterialServlet">
<input type="hidden" name="hAction" value="Report">
<input type="hidden" name="hType" value="<%=strType%>">

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="5" class="RightDashBoardHeader">
				<fmtItemQuarantineReport:message key="LBL_REPORT"/>
			</td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			 <fmtItemQuarantineReport:message key="IMG_ALT_HELP" var = "varHelp"/>
			  <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr><td class="Line" colspan="6"></td></tr>
		<tr>
			<td class="RightTableCaption" HEIGHT="24" align="right" width="15%"><fmtItemQuarantineReport:message key="LBL_REPORT_TYPE"/>&nbsp;:</td>
			<td width="30%">&nbsp;<gmjsp:dropdown controlName="Cbo_Type"  seletedValue="<%= strType %>" 	
			  value="<%=alCodeList%>" codeId = "CODEID"  codeName = "CODENM"  onChange="" defaultValue= "[Choose One]" />
			</td>
			<td class="RightTableCaption" HEIGHT="24" align="right" width="10%"><fmtItemQuarantineReport:message key="LBL_STATUS"/>&nbsp;:</td>
			<td width="20%">&nbsp;<gmjsp:dropdown controlName="Cbo_Status"  seletedValue="<%=strStatus%>" 	
			  value="<%=alStatus%>" codeId = "CODENMALT"  codeName = "CODENM"   defaultValue= "[All]" />
			</td>
			<td class="RightTableCaption" align="right" width="10%"><fmtItemQuarantineReport:message key="LBL_SHOW_DETAILS"/>&nbsp;:</td>
			<td width="15%">
			<% if (strShowTransDetails.equals("Y")) { %>
				&nbsp;<input type="checkbox" name="chkDetails" checked="true"/>
			<% } else { %>
				&nbsp;<input type="checkbox" name="chkDetails"/>
			<% } %>
			</td>
		</tr>	
		<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>	
		<tr> 
			<td class="RightTableCaption" height="24" align="right" ><fmtItemQuarantineReport:message key="LBL_FROM_DATE"/>&nbsp;:</td>
			<td>&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/></td>
			<td class="RightTableCaption" height="24" align="right" ><fmtItemQuarantineReport:message key="LBL_TO_DATE"/>&nbsp;:</td>	
			<td>	
			&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="3"/>	&nbsp;&nbsp; 
						
			</td>
			<td class="RightTableCaption" HEIGHT="24" align="right" width="100"><fmtItemQuarantineReport:message key="LBL_PART"/>&nbsp;: 
			<td>&nbsp;<input type="text" size="12" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">	&nbsp;&nbsp;
			</td>
		</tr>		
		<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td class="RightTableCaption" HEIGHT="24" align="right" ><fmtItemQuarantineReport:message key="LBL_TRANSACTION_ID"/>&nbsp;: 
			<td>&nbsp;<input type="text" size="18" value="<%=strTransactionId%>" name="Txt_TxnId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">	&nbsp;&nbsp;
			</td>
			<td class="RightTableCaption" HEIGHT="24" align="right" > <fmtItemQuarantineReport:message key="LBL_REFERENCE_ID"/>&nbsp;: 
			<td>&nbsp;<input type="text" size="18" value="<%=strRefId%>" name="Txt_RefId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">	&nbsp;&nbsp;
			</td>
			<td class="RightTableCaption" HEIGHT="24" align="right" ><fmtItemQuarantineReport:message key="LBL_ETCH_ID"/>&nbsp;: 
			<td>&nbsp;<input type="text" size="18" value="<%=strEtchId%>" name="Txt_EtchId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">	&nbsp;&nbsp;
			</td>
		</tr>	
		<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td class="RightTableCaption" HEIGHT="24" align="right" ><fmtItemQuarantineReport:message key="LBL_CONTROL"/>&nbsp;: 
			<td>&nbsp;<input type="text" size="10" value="<%=strControlNum%>" name="Txt_ControlNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">	&nbsp;&nbsp;
			</td>
			<td class="RightTableCaption" HEIGHT="24" align="right" ><fmtItemQuarantineReport:message key="LBL_CREATED_BY"/>&nbsp;: 
			<td >&nbsp;<gmjsp:dropdown controlName="Cbo_CreatedBy"  seletedValue="<%= strCreatedBy %>" 	
			  value="<%= alCreatedBy%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" />
			 </td>
			 <td colspan="2" align="center"> <fmtItemQuarantineReport:message key="BTN_GO" var="varGo"/> <gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" onClick="fnLoadType();" buttonType="Load" /> </td>
		</tr>	
				
		<tr>
			<td colspan="6">
				<display:table name="requestScope.alReportData" class = "its" id="currentRowObject" varTotals="totals" export="true" 
				decorator="com.globus.operations.displaytag.beans.DTItemQuarantineWrapper" freezeHeader="true" paneSize="500">
					<display:setProperty name="export.excel.filename" value="LogReprocessMaterialRePort.xls" /> 
					<fmtItemQuarantineReport:message key="DT_TYPE" var="varType"/>
					<display:column property="TYPE" title="${varType}" sortable="true" group="1" />
					<fmtItemQuarantineReport:message key="DT_REASON" var="varReason"/>
					<display:column property="REASON" title="${varReason}" class="alignleft"  sortable="true" />
				  	<fmtItemQuarantineReport:message key="DT_TRANS_ID" var="varTransId"/>
				  	<display:column property="CONID" title="${varTransId}" class="alignleft" sortable="true" group="1"/>
				  	<% if (strShowTransDetails.equals("Y")) { %>
				  		<fmtItemQuarantineReport:message key="DT_PART_NUMBER" var="varPartNumber"/>
				  		<display:column property="PNUM" title="${varPartNumber}" class="alignleft" sortable="true" /> 
				  		<fmtItemQuarantineReport:message key="DT_DESCRIPTION" var="varDescription"/>
				  		<display:column property="PDESC" escapeXml="true"  title="${varDescription}" class="alignleft" sortable="true" /> 
				  		<fmtItemQuarantineReport:message key="DT_QUANTITY" var="varQuantity"/>
				  		<display:column property="IQTY" title="${varQuantity}" class="alignleft" sortable="true" total="true" format="{0,number,0}"/>  
				  	<% } %>
				  	<fmtItemQuarantineReport:message key="DT_REF_ID" var="varRefID"/>
				  	<display:column property="REPROCESSID" title="${varRefID}" />
				  	<fmtItemQuarantineReport:message key="DT_CLOSED_DATE" var="varClosedDate"/>
				  	<display:column property="SDATE" title="${varClosedDate}" class="aligncenter"  sortable="true" format="<%= strDateFmt %>" />
				  	<fmtItemQuarantineReport:message key="DT_CREATED_BY" var="varCreatedBy"/>
				  	<display:column property="CUSER" title="${varCreatedBy}" />
				  	<fmtItemQuarantineReport:message key="DT_VERIFIED_BY" var="varVerifiedBy"/>
				  	<display:column property="VUSER" title="${varVerifiedBy}" />
				  	<fmtItemQuarantineReport:message key="DT_STATUS" var="varStatus"/>
				  	<display:column property="SFL" title="${varStatus}" />
				  	<% if (strShowTransDetails.equals("Y")) { %>
				  		<display:footer media="html">
							<% 
								String strVal ;
								strVal = ((HashMap)pageContext.getAttribute("totals")).get("column6").toString();
								if(strVal != null && strVal.indexOf(".") != -1) {
									strVal = strVal.substring(0, strVal.indexOf("."));
								}
							%>
							<tr><td colspan="11" class="Line"></td></tr>
	  						<tr class = "shade">
			  					<td colspan="5" class="alignright"><B><fmtItemQuarantineReport:message key="LBL_TOTAL"/>&nbsp;:&nbsp;</B></td>
			  					<td colspan="6" class="alignleft" ><B><%=strVal%></B></td>
	  						</tr>
	 					</display:footer>
	 				<% } %>
		 		</display:table> 
			</td>
		</tr>
	</table>


</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
