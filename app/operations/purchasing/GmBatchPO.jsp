 <%
/**********************************************************************************
 * File		 		: GmBatchPO.jsp
 * Desc		 		: This screen is used for the creatin po for around 400 parts
 * Version	 		: 1.0
 * author			: Manoj
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
  <!-- \operations\purchasing\orderplanning\GmBatchPO.jsp--> 

<%@ page import="com.globus.common.beans.GmResourceBundleBean" %> 
<%
String strOperationsPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_PURCHASING");
HashMap hmReturn = new HashMap();
String strDisabled = "";
String strTitle     = "";
ArrayList alPoType = new ArrayList();
String strVendorId = GmCommonClass.parseNull((String)request.getAttribute("VENDORID"));
ArrayList alVendorList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("VENDORLIST"));
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmOrderPlan", strSessCompanyLocale);
 String strPoType = GmCommonClass.parseNull((String)request.getAttribute("hPoType"));

 String strShName = GmCommonClass.parseNull((String) request.getAttribute("VSHNAME"));
 String strContactPerson = GmCommonClass.parseNull((String) request.getAttribute("CPERSON"));
 String strLotCode = GmCommonClass.parseNull((String) request.getAttribute("LCODE"));
 String strCurrency = GmCommonClass.parseNull((String) request.getAttribute("CURR"));
 String strPhone = GmCommonClass.parseNull((String) request.getAttribute("PHONE"));
 String strFax = GmCommonClass.parseNull((String) request.getAttribute("FAX"));
 alPoType = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALPOTYPE"));
 
 String strPartBatch = "";
 String strWikiTitle = GmCommonClass.getWikiTitle("BATCH_PO");
 String strPoTypeVal = "3100";
  if(strPoType.equals("3100")){		
	 strDisabled = "";
	strTitle     = "";
	}else{		
		strDisabled = "disabled";
		strTitle     = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_AVAILABLE_FOR_PO_TYPE_IS_PRODUCT"));
	} 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Order Planning </TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsPurchasingJsPath%>/GmBatchPO.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmOrderPlanServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">
 <input type="hidden" name="hPoType" value="<%=strPoType%>"> 
<!-- <input type="hidden" name="Txt_Batch" value="">-->
 <input type="hidden" name="hInputStr" value=""> 
 
	<table border="0"  class="DtTable950" cellspacing="0" cellpadding="0"><tr>
			<td colspan="5" height="25" class="RightDashBoardHeader">&nbsp;Batch - Raise PO</td>
				<td height="25" class="RightDashBoardHeader"><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='Help' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			<tr align = "center" > 
				 <td colspan="1" class="RightTableCaption" HEIGHT="30" align="right" width="20%"><font color="red">*</font>&nbsp;Vendor List: </td>
				 <td  colspan="5" align="left">&nbsp;<gmjsp:dropdown  controlName="Cbo_VendId"  seletedValue="<%=strVendorId%>"	
						width="350" value="<%= alVendorList%>" codeId = "ID" codeName = "NAME" defaultValue= "[Choose One]" tabIndex="1" onChange="javascript:fnReload();"/></td>						
		    </tr>
		    <%if(!strVendorId.equals("0")&&!strVendorId.equals("") ){ %>
			<tr><td class="LLine" height="1" colspan="6"></td></tr>		
			<tr class="shade"  height="25">
				<td class="RightTableCaption" align="Right">Contact:</td>
				<td  colspan="5">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr>
							<td width="30%">&nbsp;<%=strContactPerson%></td>
							<td class="RightTableCaption" align="Right">Lot Code Name:</td>
							<td width="30%">&nbsp;<%=strLotCode%></td>
							<td class="RightTableCaption" align="Right">Currency:</td>
							<td  width="20%">&nbsp;<%=strCurrency%></td>
						</tr>
					</table>
					</td>
					</tr>
					  </tr>
			<tr><td class="LLine" height="1" colspan="6"></td></tr>
				<td class="RightTableCaption" align="Right" height="25">Short Name:</td>
				<td  colspan="5" >
					<table border="0" width="100%"  cellspacing="0" cellpadding="0">
						<tr>
							<td width="30%"  >&nbsp;<%=strShName%></td>
							<td class="RightTableCaption" align="Right">Phone:</td>
							<td width="30%">&nbsp;<%=strPhone%></td>
							<td class="RightTableCaption" align="Right">Fax:</td>
							<td  width="20%">&nbsp;<%=strFax%></td>
						</tr>
					</table>
					</td>
					</tr>
					
				<%} %>	
					
			<tr><td class="LLine" height="1" colspan="6"></td></tr>
				<tr class="shade"  height="25">
			 <td class="RightTableCaption" align="right"><font color="red">*</font> PO Type: </td>
		   <td colspan="5" align = "left">&nbsp;<gmjsp:dropdown controlName="Cbo_Po_Type"  seletedValue="<%= strPoTypeVal %>" 	
									width="250" value="<%= alPoType%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="2" /></td></tr>
	       <tr><td class="LLine" height="1" colspan="6"></td></tr>	
	       <tr><td height="10"></td></tr>	
	    <tr>
	    <td colspan="2" align = "left"></td>
		<td class="RightTextBlue">Enter the data in format Part #, Quantity, FAR Flag</td>
		</tr>
			    <tr><td height="10"></td></tr>
		   <tr>
		   <td   class="RightTableCaption" HEIGHT="24" align="right" colspan="2">&nbsp;Enter Data For Batch Upload: </td>
	       <td   height="50%" width="40%" ><textarea name="Txt_Batch"  id="Txt_Batch" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
		          onBlur="changeBgColor(this,'#ffffff');"  rows=25 cols=40 ><%=strPartBatch%></textarea></td> </tr>
	        
	    <tr><td height="35"></td></tr>
		<TR>
		     <td colspan="3"></td>
		</tr>
		<tr><td colspan="13" class="Line" height="1"></td></tr>
		<tr><td colspan="13" class="RightTextRed" align="center" height="50"> 
			<INPUT type="hidden" name="hTextboxCnt" value="">
			<gmjsp:button value="&nbsp;Submit&nbsp;" controlId="Btn_Submit" gmClass="button" onClick="fnBatchUpdate();" tabindex="8" buttonType="Save" />&nbsp;	
			<gmjsp:button value="&nbsp;Reset&nbsp;" controlId="Btn_reset" gmClass="button" onClick="fnReset();" tabindex="8" buttonType="Save" />&nbsp;	
			</td>
		</tr>
		</TABLE>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
