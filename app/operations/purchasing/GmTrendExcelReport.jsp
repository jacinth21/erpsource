<%
/**********************************************************************************
 * File		 		: GmTrendExcelReport.jsp
 * Desc		 		: This screen is used to display Trend Excel Report
 * Version	 		: 1.0
 * author			: Velu
************************************************************************************/
%>
<%@ page language="java" %>
 
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>

 
<!-- Imports for Logger -->
 
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>
 
<bean:define id="hmTrendSumm" name="frmLogisticsReport" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define> 

<%
	HashMap hmReturn = new HashMap();
	ArrayList alDemandHeader = new ArrayList();
	ArrayList alForecastHeader = new ArrayList(); 
	ArrayList alCurrentHeader = new ArrayList(); 
	ArrayList alPdetail = new ArrayList();
	String strWikiTitle = GmCommonClass.getWikiTitle("TREND_REPORT");
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	hmReturn = GmCommonClass.parseNullHashMap((HashMap)hmTrendSumm.get("Main"));
	
	alForecastHeader = GmCommonClass.parseNullArrayList((ArrayList)hmTrendSumm.get("ForecastHeader"));
	alDemandHeader = GmCommonClass.parseNullArrayList((ArrayList)hmTrendSumm.get("DemandHeader"));
	alCurrentHeader = GmCommonClass.parseNullArrayList((ArrayList)hmTrendSumm.get("CurrentHeader"));
	alPdetail = GmCommonClass.parseNullArrayList((ArrayList)hmTrendSumm.get("Pdetail"));
	 
	
	gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setNoDivRequired(true);
	 
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setLinkRequired(false);	
	gmCrossTab.setHideColumn("Name");
	
	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	
	// Added for zero value
	gmCrossTab.setValueType(0);		
	
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD"); 
	gmCrossTab.addStyle("Description","ShadeMedGrayTD","ShadeDarkGrayTD");
	
	gmCrossTab.addStyle("Total Demand","ShadeMedGreenTD","ShadeDarkGreenTD") ;
	gmCrossTab.addStyle("Average","ShadeLightGreenTD","ShadeDarkGreenTD") ;
	gmCrossTab.addStyle("Shelf","ShadeMedYellowTD","ShadeMedYellowTD") ;
	gmCrossTab.addStyle("On PO","ShadeLightYellowTD","ShadeMedYellowTD") ;
	gmCrossTab.addStyle("BO","ShadeMedBlueTD","ShadeDarkBlueTD") ;
	gmCrossTab.addStyle("OTHER","ShadeMedOrangeTD","ShadeDarkOrangeTD");
	gmCrossTab.addStyle("On DHR","ShadeLightYellowTD","ShadeDarkYellowTD");
	gmCrossTab.addStyle("US Average","ShadeMedGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("OUS Average","ShadeLightGreenTD","ShadeLightGreenTD");
	
	for(int i=0; i< alForecastHeader.size(); i++){
		gmCrossTab.addStyle((String)alForecastHeader.get(i),"ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	}
	
	for(int i=0; i< alDemandHeader.size(); i++){
		gmCrossTab.addStyle((String)alDemandHeader.get(i),"ShadeMedBrownTD","ShadeDarkBrownTD") ;
	}
	for(int i=0; i< alCurrentHeader.size(); i++){
		gmCrossTab.addStyle((String)alCurrentHeader.get(i),"ShadeLightBlueTD","ShadeDarkBlueTD") ;
	}
	 
	gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setNoDivRequired(true);
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setLinkRequired(false);	
	gmCrossTab.setValueType(0);
	gmCrossTab.add_RoundOff("Avg Net","2");
	gmCrossTab.setDisableLine(true);
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Trend -- Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<style>


.aaTopHeader {
	FONT-WEIGHT: bold; 
	FONT-SIZE: 9px; 
	COLOR: white; 
	FONT-FAMILY: verdana, Helvetica,sans-serif; 
	BACKGROUND-COLOR: #666666;
}
.borderDark {
	FONT-SIZE: 12px; 
	FONT-FAMILY: Helvetica,sans-serif; 
	BACKGROUND-COLOR: #7c816a;
	TEXT-ALIGN: right;
}

.ShadeDarkGrayTD{
	background-color: #c3c5b8;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: left;
}

.ShadeMedGrayTD{
	background-color: #dee0d2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeDarkBlueTD{
	background-color: #a0b3de;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeLightBlueTD{
	background-color: #f1f2f7;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeMedBlueTD{
	background-color: #e4e6f2;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}


.ShadeDarkGreenTD{
	background-color: #ccdd99;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedGreenTD{
	background-color: #e1f3a6;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeLightGreenTD {
	background-color: #ecffaf;
	color : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}


.ShadeMedYellowTD{
	background-color: #ffff99;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeLightYellowTD {
	background-color: #ffffcc;
	color : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeDarkOrangeTD{
	background-color: #feb469;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	font-weight: bold;
	height: 20px;
}

.ShadeMedOrangeTD{
	background-color: #ffecb9;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}


.ShadeDarkBrownTD{
	background-color: #BEFFFF;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}

.ShadeMedBrownTD{
	background-color: #D2FFFF;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}

.ShadeRightTableCaptionBlueTD{
	FONT-SIZE: 9px; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #AACCE8;
	TEXT-ALIGN: right;
}

.ShadeBlueTDSmall{
	background-color: #ecf6fe;
	COLOR : Black;
	font-family: sans-serif;
	font-size : 11px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}
 
.CrossRightText {
	FONT-SIZE: 9px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
	TEXT-ALIGN: right;
}

.RightTextRedSmall { 
	FONT-SIZE: 11px; 
	COLOR: #FF0000; 
	FONT-FAMILY:sans-serif;
	text-decoration: None;
	TEXT-ALIGN: right;
}

.ShadeMedGreenTD{
	background-color: #e1f3a6;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}
.ShadeLightGreenTD {
	background-color: #ecffaf;
	color : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}
.ShadeLightYellowTD {
	background-color: #ffffcc;
	color : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
	TEXT-ALIGN: right;
}
.ShadeDarkYellowTD{
	background-color: #ffff66;
	COLOR : Black;
	font-family: verdana, arial, sans-serif;
	font-size : 9px;
	font-style: normal;
	height: 20px;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10"  >
<table border="0" class="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan ="6" align=center> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
	</tr>
</table>
</BODY>
</HTML>
