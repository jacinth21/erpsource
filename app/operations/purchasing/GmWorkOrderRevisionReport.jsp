<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--operations\purchasing\GmWorkOrderRevisionReport.jsp  -->
<!-- PMT-32450 - Work Order Revision Update Dashboard  -->
<%@ page language="java" %>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtWORevisionRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtWORevisionRpt:setLocale value="<%=strLocale%>"/>
<fmtWORevisionRpt:setBundle basename="properties.labels.operations.purchasing.GmWorkOrderRevision"/>
<%
String strOperationsPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("WO_REVISION_REPORT");
String strApplDateFmt = strGCompDateFmt;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Work Order Revision Update Report</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<!-- Dhtmlx Import -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOperationsPurchasingJsPath%>/GmWorkOrderRevision.js"></script>
<script type="text/javascript">
var format = '<%=strApplDateFmt%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();" onkeyup="fnEnter();">
<html:form action="/gmWoRevision.do">
<table border="0" class="DtTable1200" cellspacing="0" cellpadding="1">
 	<tr>
			<td height=25 class=RightDashBoardHeader colspan="5"><fmtWORevisionRpt:message key="LBL_WO_REVISION_UPDATE_RPT_HEADER"/></td>
			<td align="right" class=RightDashBoardHeader><fmtWORevisionRpt:message key="IMG_HELP" var="varHelp"/><img id='imgEdit'
				style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
				height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		 <tr>
		  <td colspan="6">
		    <jsp:include page="/operations/purchasing/GmIncWorkOrderRevisionHeader.jsp" >
			<jsp:param value="FORMNAME" name="frmWORevision"/>
			<jsp:param value="108243,108244,108245" name="STATUS"/>
			</jsp:include>
		</td>
		</tr>
		<tr>
              <td align="left" colspan="6">
	         <div id="dataGridDiv" style="width: 100%;" height="400px" width="1200px"></div></td>
        </tr> 
        <tr><td align="center" colspan="6">
            <div class='exportlinks' id="DivExportExcel"><fmtWORevisionRpt:message key="LBL_EXPORT_OPTIONS" />:<img src='img/ico_file_excel.png' />&nbsp;
			  <a href="#"onclick="fnDownloadXLS();"><fmtWORevisionRpt:message key="LBL_EXCEL" /></a>
			</div>
 	      </td>
       </tr>    
	   <tr><td colspan="6" align="center"  class="RegularText"><div  id="DivNothingMessage"><fmtWORevisionRpt:message key="NO_DATA_FOUND"/></div></td></tr>
	</table>
</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>