<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- operations\purchasing\GmWorkOrderRevisionDashboard.jsp -->
<!-- PMT-32450 - Work Order Revision Update Dashboard  -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtWORevisionDash" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtWORevisionDash:setBundle basename="properties.labels.operations.purchasing.GmWorkOrderRevision"/>
<bean:define id="woJsonString" name="frmWORevision" property="woJsonString" type="java.lang.String"> </bean:define>
<%
String strOperationsPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("WO_REVISION_DASHBOARD");
String strApplDateFmt = strGCompDateFmt;
%>
<html>
<head>
<title>Work Order Revision Update Report</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script> 
<script language="JavaScript" src="<%=strOperationsPurchasingJsPath%>/GmWorkOrderRevision.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>


<script type="text/javascript">
var format = '<%=strApplDateFmt%>';
var objGridData ='<%=woJsonString%>';
</script>
</head>
<body>

<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();" onkeyup="fnEnter();">
<html:form action="/gmWoRevision.do">
<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
 	<tr>
			<td height=25 class=RightDashBoardHeader colspan="5"><fmtWORevisionDash:message key="LBL_WO_REVISION_DASH_HEADER"/></td>
			<td align="right" class=RightDashBoardHeader><fmtWORevisionDash:message key="IMG_HELP" var="varHelp"/><img id='imgEdit'
				style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
				height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
 <tr>
		  <td colspan="6">
		    <jsp:include page="/operations/purchasing/GmIncWorkOrderRevisionHeader.jsp" >
			<jsp:param value="FORMNAME" name="frmWORevision"/>
			<jsp:param value="108242" name="STATUS"/>
			</jsp:include>
		</td>
		</tr>
		<tr>
              <td align="left" colspan="6">
	         <div id="dataGridDiv" style="width:100%;" width="1200px"  height="400px"></div></td>
        </tr> 
        <tr><td align="center" colspan="6">
            <div class='exportlinks' id="DivExportExcel"><fmtWORevisionDash:message key="LBL_EXPORT_OPTIONS" />:<img src='img/ico_file_excel.png' />&nbsp;
			  <a href="#"onclick="fnDownloadXLS();"><fmtWORevisionDash:message key="LBL_EXCEL" /></a>
			</div>
 	      </td>
       </tr>    
	   <tr><td colspan="6" align="center"  class="RegularText"><div  id="DivNothingMessage"><fmtWORevisionDash:message key="NO_DATA_FOUND"/></div></td></tr>
	    <tr><td colspan="6" align="center"><div id="buttonDiv">
			     <fmtWORevisionDash:message key="LBL_UPDATE" var="varUpdate"/>
	            <gmjsp:button value="${varUpdate}" gmClass="button" onClick="fnRevisionUpdate();" tabindex="3" buttonType="Load"/>
	            <fmtWORevisionDash:message key="LBL_IGNORE" var="varIgnore"/>
	            <gmjsp:button value="${varIgnore}" gmClass="button" onClick="fnRevisionIgnore();" tabindex="3" buttonType="Load"/>
			</div>
			</td>
			
			</tr>  
	</table>
			
	
		</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>