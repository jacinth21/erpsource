
<%
/**********************************************************************************
 * File		 		: GmSetPriorityReport.jsp
 * Desc		 		: Set Priority Report
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- operations\purchasing\setpriority\GmSetPriorityReport.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtSetPriorityReport"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<fmtSetPriorityReport:setLocale value="<%=strLocale%>" />
<fmtSetPriorityReport:setBundle
	basename="properties.labels.operations.purchasing.setpriority.GmSetPriorityReport" />

<%

	String strPurchasingJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_PURCHASING");
	String strWikiTitle = GmCommonClass
			.getWikiTitle("SET_PRIORITY_RPT");

%>

<html>
<head>
<title>Set Priority Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript"
	src="<%=strPurchasingJsPath%>/setpriority/GmSetPriorityReport.js"></script>
</head>
<body leftmargin="20" topmargin="10">
	<html:form
		action="/gmSetPriorityReport.do?method=fetchSetPriorityReports">
		<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><fmtSetPriorityReport:message
						key="LBL_SET_PRIORITY_REPORT" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtSetPriorityReport:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>

			<tr>
				<td class="RightTableCaption" align="right" height="25"><fmtSetPriorityReport:message
						key="LBL_SET_PRIORITY_ID" var="varSetPriorityId" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varSetPriorityId}:"
						td="false" /></td>

				<td>&nbsp; <html:text property="priorityId"
						styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="1" size="14"
						maxlength="15" />
				</td>
				<td class="RightTableCaption" align="right" height="25">&nbsp;<fmtSetPriorityReport:message
						key="LBL_STATUS" var="varSetPriorityStatus" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varSetPriorityStatus}:"
						td="false" />
				</td>
				<td>&nbsp; <gmjsp:dropdown controlName="spStatusId"
						SFFormName="frmSetPriorityRpt" SFSeletedValue="spStatusId"
						SFValue="alSpStatus" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="2" /></td>

				<td class="RightTableCaption" align="right" height="25"><fmtSetPriorityReport:message
						key="LBL_TYPE" var="varSetPriorityType" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varSetPriorityType}:"
						td="false" /></td>
				<td>&nbsp; <gmjsp:dropdown controlName="spTypeId"
						SFFormName="frmSetPriorityRpt" SFSeletedValue="spTypeId"
						SFValue="alSpType" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="3" /></td>
			</tr>

			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>

			<tr class="Shade" height="30">

				<td class="RightTableCaption" align="right" height="25"><fmtSetPriorityReport:message
						key="LBL_QUARTER" var="varSetPriorityQuarter" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varSetPriorityQuarter}:"
						td="false" /></td>

				<td>&nbsp; <gmjsp:dropdown controlName="spQuarter"
						SFFormName="frmSetPriorityRpt" SFSeletedValue="spQuarter"
						SFValue="alSpQuarter" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="4" />
				</td>

				<td class="RightTableCaption" align="right" height="25"><fmtSetPriorityReport:message
						key="LBL_YEAR" var="varSetPriorityYear" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varSetPriorityYear}:"
						td="false" /> </td>
						
					<td>&nbsp;&nbsp;<gmjsp:dropdown controlName="spYear"
						SFFormName="frmSetPriorityRpt" SFSeletedValue="spYear"
						SFValue="alSpYear" codeId="CODENM" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="5" /></td>

				<td colspan="2" align="center">&nbsp;&nbsp;&nbsp;<fmtSetPriorityReport:message
						key="BTN_LOAD" var="varLoad" /> <gmjsp:button
						value="&nbsp;${varLoad}&nbsp;" gmClass="button"
						onClick="fnLoad();" buttonType="Load" tabindex="6" /></td>

			</tr>

			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>


			<tr>
				<td colspan="6">
					<div id="dataGridDiv" height="500px" width="1100"></div>
				</td>
			</tr>

			<tr>
				<td class="LLine" colspan="6" height="1"></td>
			</tr>
			<tr>
				<td colspan="6" align="center">
					<div class='exportlinks' style="display: none" id="DivExportExcel">
						<fmtSetPriorityReport:message key="LBL_EXPORT_OPTIONS" />
						:<img src='img/ico_file_excel.png' />&nbsp; <a href="#"
							onclick="fnExportSetPriority();"><fmtSetPriorityReport:message
								key="LBL_EXCEL" /></a>
					</div>
				</td>
			</tr>

			<tr>
				<td colspan="6" align="center" id="DivNothingFoundColumn"><div
						id="DivNothingFound" style="display: none">
						<fmtSetPriorityReport:message key="LBL_NOTHING_FOUND_TO_DISPLAY" />
					</div></td>
			</tr>

		</table>

	</html:form>

</body>
<%@ include file="/common/GmFooter.inc"%>
</html>