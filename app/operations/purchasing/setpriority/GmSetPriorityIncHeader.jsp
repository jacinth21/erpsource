<%
/**********************************************************************************
 * File		 		: GmSetPriorityIncHeader.jsp
 * Desc		 		: This Screen describes how a user will load the Set Priority Upload screen 
 					  for a Set Priority transaction 
 * Version	 		: 
 * author			: Prabhu vigneshwaran M D 
************************************************************************************/
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtSetPriorityUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtSetPriorityUpload:setLocale value="<%=strLocale%>"/>
<fmtSetPriorityUpload:setBundle basename="properties.labels.operations.purchasing.setpriority.GmSetPriorityUpload"/>

 <!--operations\purchasing\setpriority\GmSetPriorityIncHeader.jsp -->
<%
String strPurchasingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<!-- Method allows user to add new row from clipboard. New row is being added to the last position in the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<!-- paste content of clipboard into block selection of grid -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<!-- The library provides support for different keyboard commands that let users to navigate through the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script language="JavaScript" src="<%=strPurchasingJsPath%>/setpriority/GmSetPriorityIncHeader.js"></script>

</head>
<body leftmargin="20" topmargin="10" onload="fnLoadHeaderDtls();">
<html:form action="/gmSetPriorityUpload.do">
<table cellspacing="0" cellpadding="0">

	    <tr height="25">
		<td class="RightTableCaption"align="right">
		<fmtSetPriorityUpload:message key="LBL_SET_PRIORITY_ID" var="varsetPriorityId"/><gmjsp:label type="RegularText" SFLblControlName="${varsetPriorityId}" td="false" />:&nbsp;
		</td>
        <td align="left" id="lbl_static_set_priorityid">
		<html:text  name="frmSetPriorityUpload" property="priorityId" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onchange="fnload(this.value);" />
		</td>
		<td class="RightTableCaption" align="right">
		<fmtSetPriorityUpload:message key="LBL_SET_PRIORITY_NAME" var="varsetPriorityNm"/><gmjsp:label type="RegularText" SFLblControlName="${varsetPriorityNm}" td="false" />:&nbsp;
		</td>
		<td id="lbl_set_priority_nm" width="30%" align="left"></td> 
		</tr>
		<tr>
		   <td class="LLine" colspan="8" height="1"></td>
		</tr>
		
		<tr height="25" class="Shade">
		<td class="RightTableCaption" align="right">
		<fmtSetPriorityUpload:message key="LBL_PREDEFINED_QTY" var="varPredefinedQty"/><gmjsp:label type="RegularText" SFLblControlName="${varPredefinedQty}" td="false" />:&nbsp;
		</td>
		<td id="lbl_set_priority_qty" align="left">
		</td>
		
		<td class="RightTableCaption" align="right" >
		<fmtSetPriorityUpload:message key="LBL_PROCESS_SEQ" var="varProcessSequence"/><gmjsp:label type="RegularText" SFLblControlName="${varProcessSequence}" td="false" />:&nbsp;
		</td>
		<td id="lbl_set_priority_seq" align="left">
		</td>
		<td></td>
		</tr>
		<tr>
		   <td class="LLine" colspan="8" height="1"></td>
		</tr>
		<tr  height="25">
		<td class="RightTableCaption" align="right">
		<fmtSetPriorityUpload:message key="LBL_QUARTER" var="varQuarter"/><gmjsp:label type="RegularText" SFLblControlName="${varQuarter}" td="false" />:&nbsp;
		</td>
		<td id="lbl_set_priority_quarter" align="left">
		</td>
		
		<td class="RightTableCaption" align="right" >
		<fmtSetPriorityUpload:message key="LBL_YEAR" var="varYear"/><gmjsp:label type="RegularText" SFLblControlName="${varYear}" td="false" />:&nbsp;
		</td>
		<td id="lbl_set_priority_year" align="left">
		</td>
		<td></td>
		</tr>
		<tr>
		  <td class="LLine" colspan="8" height="1"></td>
		</tr>
		<tr height="25" class="Shade">
		<td class="RightTableCaption" align="right">
		<fmtSetPriorityUpload:message key="LBL_REGION" var="varRegion"/><gmjsp:label type="RegularText" SFLblControlName="${varRegion}" td="false" />:&nbsp;
		</td>
		<td id="lbl_set_priority_region" align="left">
		</td>
		
		<td class="RightTableCaption" align="right" >
		<fmtSetPriorityUpload:message key="LBL_STATUS" var="varStatus"/><gmjsp:label type="RegularText" SFLblControlName="${varStatus}" td="false" />:&nbsp;
		</td>
		<td id="lbl_set_priority_status" align="left">
		</td>
		<td></td>
		</tr>
		<tr>
		  <td class="LLine" colspan="8" height="1"></td>
		</tr>
		<tr>
		<td class="RightTableCaption" align="right">
		<fmtSetPriorityUpload:message key="LBL_UPDATED_BY" var="varCreatedBy"/><gmjsp:label type="RegularText" SFLblControlName="${varCreatedBy}" td="false" />:&nbsp;
		</td>
		<td id="lbl_updated_by" align="left">
		</td>
		
		<td class="RightTableCaption" align="right" >
		<fmtSetPriorityUpload:message key="LBL_UPDATED_DATE" var="varCreatedDt"/><gmjsp:label type="RegularText" SFLblControlName="${varCreatedDt}" td="false" />:&nbsp;
		</td>
		<td id="lbl_updated_date" align="left" height="25">
		</td>
		<td></td>
		</tr>
	</table>
</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>