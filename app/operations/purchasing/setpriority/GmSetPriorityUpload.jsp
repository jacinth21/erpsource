<%
/**********************************************************************************
 * File		 		: GmSetPriorityUpload.jsp
 * Desc		 		: This Screen describes how a user will load the Set Priority Upload screen 
 					  for a Set Priority transaction 
 * Version	 		: 
 * author			: Prabhu vigneshwaran M D 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtSetPriorityUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page buffer="16kb" autoFlush="true" %>
<fmtSetPriorityUpload:setBundle basename="properties.labels.operations.purchasing.setpriority.GmSetPriorityUpload"/>
<!-- operations\purchasing\GmSetPriorityUpload.jsp -->
<bean:define id="priorityId" name="frmSetPriorityUpload" property="priorityId" type="java.lang.String"> </bean:define>
<bean:define id="strMaxUploadData" name="frmSetPriorityUpload" property="strMaxUploadData" type="java.lang.String"> </bean:define>
<bean:define id="buttonAccessFl" name="frmSetPriorityUpload" property="buttonAccessFl" type="java.lang.String"> </bean:define>


<%
response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
response.setHeader("Pragma", "no-cache"); //HTTP 1.0
response.setDateHeader("Expires", 0); //prevents caching at the proxy server 

String strPurchasingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("SET_PRIORITY_UPLOAD");
%>
<html>
<head>
<title>Set Priority Upload</title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>


<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<!-- Method allows user to add new row from clipboard. New row is being added to the last position in the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<!-- paste content of clipboard into block selection of grid -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<!-- The library provides support for different keyboard commands that let users to navigate through the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script language="JavaScript" src="<%=strPurchasingJsPath%>/setpriority/GmSetPriorityIncHeader.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/setpriority/GmSetPriorityUpload.js"></script>


<script>
var setPriorityShow='';
var strPriorityId = '<%=priorityId%>';
var maxUploadData = '<%=strMaxUploadData%>';
var buttonAccessFl = '<%=buttonAccessFl%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmSetPriorityUpload.do?method=loadSetPriorityUpload">
<html:hidden property="spPredefinedQty"/>
<html:hidden property="spStatusId"/>

	<table border="0" class="DtTable950"  cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" class="RightDashBoardHeader"  height="24" width="780"><fmtSetPriorityUpload:message key="LBL_SET_PRIORITY_UPL"/></td>
			<td colspan="1" class="RightDashBoardHeader" align="right" width="20">
			<fmtSetPriorityUpload:message key="IMG_ALT_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
		   <td class="LLine" colspan="4" height="1"></td>
		</tr>
	
		<tr>
		   <td colspan="4">
		    <jsp:include page="/operations/purchasing/setpriority/GmSetPriorityIncHeader.jsp" >
			<jsp:param value="FORMNAME" name="frmSetPriorityUpload"/>
			</jsp:include>
		</td>
		</tr>
		<tr>
		  <td class="LLine" colspan="4" height="1"></td>
		</tr>
		<tr style="display: none" id="trImageShow"  class="Shade" height="25">
		<td colspan="4">
		<table>	
				<tr  height="25">		
						<td  width="28" colspan="1" tabIndex="-1"><fmtSetPriorityUpload:message key="IMG_ALT_COPY" var="varCopy"/><a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="${varCopy}" style="border: none;" height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="-1"><fmtSetPriorityUpload:message key="IMG_ALT_PASTE" var="varpaste"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="${varpaste}" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a></td>	
						<td  width="28" colspan="1" tabIndex="-1"><fmtSetPriorityUpload:message key="IMG_ALT_ADD_ROW" var="varAddRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:fnAddRow()"height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="-1"><fmtSetPriorityUpload:message key="IMG_ALT_REMOVE_ROW" var="varRemoveRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:fnRemoveSetPriorityRow()" height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="-1"><fmtSetPriorityUpload:message key="IMG_ALT_ADD_ROWS_CLIPBOARD" var="varAddRowsClipboard"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="${varAddRowsClipboard}" style="border: none;" onClick="javascript:addRowFromClipboard()" height="14"></a></td>
					
						<td  colspan="2" align="right" >
						<div id="progress" style="display:none;"><img src="<%=strImagePath%>/success_y.gif" height="15"> </div></td>
						<td><div id="msg"  align="center"  style="display:none;color:green;font-weight:bold;"></div> 
						<div id="erromsg" align="center" style="display:none; color:red;font-weight:bold;"></div>
					</td>
			   </tr>	
					<tr height="25" id="trLabelShow"><td colspan="8"><font color="#6699FF">&nbsp;
                    <fmtSetPriorityUpload:message key="LBL_MAXIMUM"/>&nbsp;<%=strMaxUploadData%>&nbsp;<fmtSetPriorityUpload:message key="LBL_LESS"/> </font></td>
		            </tr>											
		</table>
		</td>
				</tr>
			<tr id="trDiv">
              <td colspan="4">
	         <div id="dataGridDiv"  height="700px"></div></td>
        </tr> 
        <tr style="display: none" id="exportOptions" height="25">
			<td colspan="4" align="center">
			    <div class='exportlinks'><fmtSetPriorityUpload:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"><fmtSetPriorityUpload:message key="DIV_EXCEL"/> 
				</a></div>
			</td>
		</tr>		
           <tr style="display: none" id="buttonshow" height="30"><td colspan="4"  align="center">
           
           <div id="buttonDiv">
           <logic:equal name="frmSetPriorityUpload" property="buttonAccessFl" value="Y">	
			     <fmtSetPriorityUpload:message key="LBL_VOID" var="varVoid"/>
	            <gmjsp:button value="${varVoid}" gmClass="button" name="LBL_VOID" onClick="fnVoidUploadDtls();"  buttonType="Save"/>
	            <fmtSetPriorityUpload:message key="LBL_SAVE" var="varSave"/>
	            <gmjsp:button value="${varSave}" gmClass="button" name="LBL_SAVE" onClick="fnSave();"  buttonType="Save"/>
	            <fmtSetPriorityUpload:message key="LBL_ALLOCATE" var="varAllocate"/>
	            <gmjsp:button value="${varAllocate}" gmClass="button" name="LBL_ALLOCATE"  onClick="fnAllocateRequest();"  buttonType="Save"/>
			</logic:equal>
			</div>
			
			</td>
			
			</tr>  
        
	</table>
</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>