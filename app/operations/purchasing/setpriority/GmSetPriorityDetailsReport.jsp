<!--operations\purchasing\setpriority\GmSetPriorityDetailsReport.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtSPDtlsReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtSPDtlsReport:setLocale value="<%=strLocale%>"/>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtSPDtlsReport:setBundle basename="properties.labels.operations.purchasing.setpriority.GmSetPriorityUpload"/>
<bean:define id="priorityId" name="frmSetPriorityRpt" property="priorityId" type="java.lang.String"> </bean:define>
<bean:define id="buttonAccessFl" name="frmSetPriorityRpt" property="buttonAccessFl" type="java.lang.String"> </bean:define>
<%
String strPurchasingJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("SET_PRIORITY_DETAIL_REPORT");
%>
<html>
<head>
<title>Globus Medical: Set Priority Details Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/setpriority/GmSetPriorityIncHeader.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/setpriority/GmSetPriorityDetailsReport.js"></script>
<script>
var strPriorityId='<%=priorityId%>';
var setPriorityShow = 'Y';
var buttonAccessFl = '<%=buttonAccessFl%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnpageLoad();">
<html:form action="/gmSetPriorityReport.do" >
<html:hidden property="spPredefinedQty"/>
<html:hidden property="spStatusId"/>
<table border="0" class="DtTable1100"  cellspacing="0" cellpadding="0">
      <tr>
			<td colspan="3" class="RightDashBoardHeader"  height="24" width="780"><fmtSPDtlsReport:message key="LBL_SET_PRIORITY_DTL_RPT"/></td>
			<td colspan="1" class="RightDashBoardHeader" align="right" width="20">
			<fmtSPDtlsReport:message key="IMG_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
		   <td class="LLine" colspan="4" height="1"></td>
		</tr>
		
		<tr>
		   <td colspan="4">
		    <jsp:include page="/operations/purchasing/setpriority/GmSetPriorityIncHeader.jsp">
			<jsp:param value="FORMNAME" name="frmSetPriorityRpt"/>
			</jsp:include>
		</td>
		
		</tr>
		<tr>
		  <td class="LLine" colspan="4" height="1"></td>
		</tr>
					
			<tr id="trDiv">
              <td colspan="4">
	         <div id="dataGridDiv"  style="width: 100%"></div></td>
        </tr> 
        <tr id="DivExportExcel" height="25">
			<td colspan="6" align="center">
			    <div class='exportlinks'><fmtSPDtlsReport:message key="DIV_EXPORT_OPT"/> :<img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExportSetPriorityDtls();"><fmtSPDtlsReport:message key="DIV_EXCEL"/> 
				</a></div>
			</td>
		</tr>
		 <tr><td colspan="4" align="center"  class="RegularText"><div  id="DivNothingMessage"><fmtSPDtlsReport:message key="NO_DATA_FOUND"/></div></td></tr>
		 <tr height="30" id="trBtn"><td colspan="4" align="center">
			     <fmtSPDtlsReport:message key="BTN_EDIT" var="varEdit"/>
	            <gmjsp:button name="BTN_EDIT" value="${varEdit}" gmClass="button" onClick="fnEditSetPriorityDtls();"  buttonType="Load"/>
	            <logic:equal name="frmSetPriorityRpt" property="buttonAccessFl" value="Y">	
	            <fmtSPDtlsReport:message key="BTN_SUBMIT" var="varSubmit"/>
	            <gmjsp:button name="BTN_SUBMIT" value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save"/>
	            </logic:equal>
			</td>
			
			</tr>  
</table>
</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>