<%
/**********************************************************************************
 * File		 		: GmSubPartOrder.jsp
 * Desc		 		: Sub Part Mapping Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
  <!-- \operations\purchasing\orderplanning\GmSubPartOrder.jsp-->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>

<bean:define id="returnList" name="frmSubPartOrder" property="returnList" type="java.util.List"></bean:define>
 
 
<%
	ArrayList alSubList = new ArrayList();
	 
	alSubList = GmCommonClass.parseNullArrayList((ArrayList) returnList);
 
	int rowsize = alSubList.size();
	 
	StringBuffer sbInput = new StringBuffer();
	String strPnum = "";
	String strInput = "";
	 
	DynaBean db ;

		for (int i=0;i<rowsize;i++)
		{
			db = (DynaBean)alSubList.get(i);
			strPnum = String.valueOf(db.get("PNUM"));
			sbInput.append(strPnum);
			sbInput.append("^");
		}
		 strInput = sbInput.toString();
	
	String strWikiTitle = GmCommonClass.getWikiTitle("SUB_PART_ORDER_MAPPING");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Sub Part Order Mapping</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"
	media="print" />

<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnReload()
{    
	if(document.frmSubPartOrder.pnum.value == ""&&document.frmSubPartOrder.projectListID.value == 0){
		Error_Details(" Please select at least one of Part number / Project List");
		}	
		if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmSubPartOrder.strOpt.value = "reload"; 
	fnStartProgress('Y');
	document.frmSubPartOrder.submit();   
} 
 
 
function fnSelectAll(){
  TotalRows = <%=rowsize%>;
 for (var i=0;i<TotalRows ;i++ )
	{
		obj = eval("document.frmSubPartOrder.Chk_sub"+i);	
		obj.checked = document.frmSubPartOrder.Chk_selectAll.checked;		 
	}
}
 
  
function fnSubmit()
{ 	
	var varRows = <%=rowsize%>;
    var varProjId =  document.frmSubPartOrder.projectListID.value; 
	  
	var inputString = ''; 
	if(varProjId!=null)
	{
		for (var i =0; i < varRows; i++) 
		{
			var checkedFl = '';
			var varPnum='';
			//The input part numbers are taken from Hidden field(wrapper).
			obj = eval("document.frmSubPartOrder.pnum"+i);
			varPnum = obj.value;
			obj = eval("document.frmSubPartOrder.Chk_sub"+i);
			varStatus = obj.value;
			if(obj.checked){
				checkedFl = 'Y';	           
	        }
			inputString = inputString + varPnum +','+checkedFl+'^';	
		}
	 
	}
 	document.frmSubPartOrder.strOpt.value = 'save';
 	document.frmSubPartOrder.hinputStr.value = inputString;
 	fnStartProgress('Y');
	document.frmSubPartOrder.submit();
	 
}

//This function will call the press the enter button.
function fnEnter(){
	if(event.keyCode == 13)
	{	
		var rtVal = fnReload();
//below code is when fnReload() return false, we need to stop the jsp, from submitting.
		if(rtVal == false){
			if(!e) var e = window.event; 
	        e.cancelBubble = true;
	        e.returnValue = false; 
	        
	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}
} 
 
 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeypress="fnEnter();">
<html:form action="/gmOPSubPartOrder.do">
	<html:hidden property="strOpt" value="" />
	<html:hidden property="hinputStr" />
	
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4">Sub Part Order Mapping</td>
			<td  height="25" class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			 
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="5"></td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="5">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td height="30" class="RightTableCaption" align="right">Project List:</td>
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="projectListID" SFFormName="frmSubPartOrder" SFSeletedValue="projectListID"
							SFValue="projectList" codeId = "ID"  codeName = "IDNAME"  defaultValue= "[Choose One]"/> 		
						</td>
					</tr>
				 
                    <tr><td colspan="5" class="ELine"></td></tr>
                    <tr class="shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Part Numbers:</td> 
                        <td>&nbsp;
	                         <html:text property="pnum"  size="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 
	                        
	                        &nbsp;&nbsp;&nbsp; <gmjsp:button name="Load" value="&nbsp;&nbsp;Load&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnReload();" buttonType="Load" />
	                        &nbsp;&nbsp;
	                    
                        </td> 
                    </tr>       
                    <tr><td colspan="5" class="ELine"></td></tr>
           <tr> 
		
		<tr>
			<td class="Line" height="1" colspan="5"></td>
		</tr>
		<%
			if (rowsize > 0) {
		%>
		 <tr>
  			<td colspan="5" align="right"  >Select All <input type="checkbox" name="Chk_selectAll" onClick="fnSelectAll();"></td>
 		</tr> 
 		<%
			}
		%>
		<tr>
			<td colspan="5">
			<display:table name="requestScope.frmSubPartOrder.returnList" requestURI="/gmOPSubPartOrder.do"   class="its" id="currentRowObject"  decorator="com.globus.operations.displaytag.beans.DTSubPartOrderWrapper"> 
 				<display:column property="PNUM" title="Part #"   sortable="true" />
				<display:column property="DESCRIPTION" title="Part Description"  class="alignleft"  sortable="true" />
				<display:column property="STATUS" title=""  class="alignright" />
				 
			</display:table>
			</td>
		</tr>

		<tr>
			<td class="LLine" height="1" colspan="5"></td>
		</tr>
		 
		<%
			if (rowsize > 0) {
		%> 
		</tr> 
		<tr>
			<td colspan="5" align="center">&nbsp;&nbsp;    <p>&nbsp;</p>
                         <gmjsp:button value="Submit" gmClass="button"   onClick="fnSubmit();" buttonType="Save" /> <p>&nbsp;</p>
         </td>
		</tr>               
	 	
		<% 
			}
		%> 
		
		 
	</table>
	</FORM>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>