<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--operations\purchasing\GmIncWorkOrderRevisionHeader.jsp  -->
<!-- PMT-32450 - Work Order Revision Update Dashboard  -->
<%@ page language="java" %>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtWORevisionHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtWORevisionHeader:setLocale value="<%=strLocale%>"/>
<fmtWORevisionHeader:setBundle basename="properties.labels.operations.purchasing.GmWorkOrderRevision"/>
<bean:define id="projectName" name="frmWORevision" property="projectName" type="java.lang.String"> </bean:define>
<bean:define id="projectId" name="frmWORevision" property="projectId" type="java.lang.String"> </bean:define> 
<bean:define id="woJsonString" name="frmWORevision" property="woJsonString" type="java.lang.String"> </bean:define>
<%
String strOperationsPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_PURCHASING");
String strStatus = GmCommonClass.parseNull(request.getParameter("STATUS"));
String strApplDateFmt = strGCompDateFmt;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strOperationsPurchasingJsPath%>/GmWorkOrderRevision.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script type="text/javascript">
var status ='<%=strStatus%>';
var format = '<%=strApplDateFmt%>';
var objGridData ='<%=woJsonString%>';
</script>

</head>
<body leftmargin="20" topmargin="10">
<html:form action="/gmWoRevision.do">
<table cellspacing="0" cellpadding="0">
    <%if(strStatus.equals("108242")){%>
		<tr>
		<td height="24" class="RightTableCaption" align="right"><fmtWORevisionHeader:message key="LBL_PROJECT"/>:</td>
		<td><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="projectId" />
				<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=LikeSearch" />
				<jsp:param name="WIDTH" value="300" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="TAB_INDEX" value="1"/>
				<jsp:param name="CONTROL_NM_VALUE" value="<%=projectName%>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=projectId%>" />
 				<jsp:param name="SHOW_DATA" value="50" />
 				<jsp:param name="AUTO_RELOAD" value="" />                                       
 	 	 	    </jsp:include>  
 	 	 	 </td>
		<td height="24" class="RightTableCaption" align="right"><fmtWORevisionHeader:message key="LBL_PART"/>:&nbsp;</td>
		<td><html:text property="pnum" name="frmWORevision"></html:text>
		          <html:select property ="pnumSuffix"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" >
				 <html:option value="0" ><fmtWORevisionHeader:message key="LBL_CHOOSEONE"/></html:option>
				<html:option value="LIT" ><fmtWORevisionHeader:message key="LBL_LITERAL"/></html:option>
				<html:option value="LIKEPRE" ><fmtWORevisionHeader:message key="LBL_LIKE_PREFIX"/></html:option>
				<html:option value="LIKESUF" ><fmtWORevisionHeader:message key="LBL_LIKE_SUFFIX"/></html:option>
		</html:select>
		</td>
		<td height="24" class="RightTableCaption" align="right"><fmtWORevisionHeader:message key="LBL_PO"/>:</td>
		<td>&nbsp;<html:text property="poId" name="frmWORevision" tabindex="2"></html:text></td>
		<td><fmtWORevisionHeader:message key="BTN_LOAD" var="varLoad"/>
	<gmjsp:button value="${varLoad}" gmClass="button" onClick="fnLoad();" tabindex="3" buttonType="Load"/></td>
	</tr>
	
<%}else{%>	
		<tr>
		<td height="24" class="RightTableCaption" align="right"><fmtWORevisionHeader:message key="LBL_PROJECT"/>:</td>
		<td style="padding-left: 2px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="projectId" />
				<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=LikeSearch" />
				<jsp:param name="WIDTH" value="300" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="TAB_INDEX" value="1"/>
				<jsp:param name="CONTROL_NM_VALUE" value="<%=projectName%>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=projectId%>" />
 				<jsp:param name="SHOW_DATA" value="50" />
 				<jsp:param name="AUTO_RELOAD" value="" />                                       
 	 	 	    </jsp:include>  
 	 	 	 </td>
		<td height="24" class="RightTableCaption" align="right"><fmtWORevisionHeader:message key="LBL_PART"/>:&nbsp;</td>
		<td><html:text property="pnum" name="frmWORevision"></html:text>
		          <html:select property ="pnumSuffix"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" >
				 <html:option value="0" ><fmtWORevisionHeader:message key="LBL_CHOOSEONE"/></html:option>
				<html:option value="LIT" ><fmtWORevisionHeader:message key="LBL_LITERAL"/></html:option>
				<html:option value="LIKEPRE" ><fmtWORevisionHeader:message key="LBL_LIKE_PREFIX"/></html:option>
				<html:option value="LIKESUF" ><fmtWORevisionHeader:message key="LBL_LIKE_SUFFIX"/></html:option>
		</html:select>
		</td>
		<td height="24" class="RightTableCaption" align="right"><fmtWORevisionHeader:message key="LBL_PO"/>:</td>
		<td>&nbsp;<html:text property="poId" name="frmWORevision"></html:text></td>
	<tr><td colspan="7" height="1" class="LLine"></td></tr>
	<tr class="Shade"> 
	<td height="24" class="RightTableCaption" align="right"><fmtWORevisionHeader:message key="LBL_ACTION"/>:</td>
		<td> &nbsp;<gmjsp:dropdown controlName="woActionId" SFFormName="frmWORevision" SFSeletedValue="woActionId"
							SFValue="alWoAction" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/></td>
	
	<td height="24" class="RightTableCaption" align="right"><fmtWORevisionHeader:message key="LBL_DATE_RANGE"/>:</td>
	
	<td colspan="2">&nbsp;
	   <gmjsp:calendar SFFormName="frmWORevision" SFDtTextControlName="dtWOFromDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
		<gmjsp:calendar SFFormName="frmWORevision" SFDtTextControlName="dtWOToDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />					
	</td>						
	<td><fmtWORevisionHeader:message key="BTN_LOAD" var="varLoad"/>
	<gmjsp:button value="${varLoad}" gmClass="button" onClick="fnLoad();" buttonType="Load"/></td>
	<td></td>
	</tr>  
<%}%>	  
	</table>
</html:form>
</body>
</html>