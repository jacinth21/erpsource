<%
	/************************************************************************
	 * File		 		: GmWODueDateBulkUpload.jsp
	 * Desc		 		: This JSP is used to upload multiple WO due date
	 * Version	 		: 1.0
	 * author			: Agilan Singaravel
	 *************************************************************************/
%>

<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*, java.net.URLEncoder"%>

<%@ taglib prefix="fmtWODueDateBulkUpload" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<!-- Operations/Purchasing\GmWODueDateBulkUpload.jsp-->
<fmtWODueDateBulkUpload:setLocale value="<%=strLocale%>" />
<fmtWODueDateBulkUpload:setBundle basename="properties.labels.operations.purchasing.GmWODueDateBulkUpload" />

<%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_PURCHASING");
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("BULK_WO_DUE_DATE_UPLOAD"));
	String strBtnDisable ="true";
	String strApplDateFmt = strGCompDateFmt;
	 //The following code added for passing the company info
    String strCompanyInfo ="{\"cmpid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getCmpid())+"\",\"partyid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPartyid())+"\",\"plantid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPlantid())+"\"}";
    strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<html>
<head>
<title>Globus Medical: WO Due Date Bulk Upload</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script> 
 
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmWODueDateBulkUpload.js"></script>

<script>
var objGridData;
objGridData ='';
var gCmpDateFmt = '<%=strApplDateFmt%>';
var strCompanyInfo = '<%=strCompanyInfo%>';
</script>
<style type="text/css">
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;;
}
		
div.gridbox table.hdr td {
   color: #000000 !important;
   background-color: #d2e9fc !important;
}

div.gridbox_material.gridbox .xhdr {
	background:#dcedfc !important;
}

</style>

</head>
<body leftmargin="20" topmargin="10"  onload="fnOnPageLoad();">
	<html:form action="/gmWODueDateBulkUpload.do?">
		<html:hidden property="strOpt"/>
		<html:hidden property="haction"/>
		<html:hidden property="inputStr" value="" />
		
		<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" width="90%">&nbsp;<fmtWODueDateBulkUpload:message key="LBL_WO_DUEDT_BULK_UPD"/></td>
			<td  align="right" class="RightDashBoardHeader" width="10%">
				<fmtWODueDateBulkUpload:message key="IMG_ALT_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:pointer' src='<%=strImagePath%>/help.gif' title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
			</td>
		</tr>		
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
		<tr height="30" class="Shade">
			<td class="RightTableCaption" align="left" height="30" colspan="2">
				<div id="rfs_grid" style="display:inline" >
					<table cellpadding="1" width="100%" cellspacing="1" border="0">
						<tr>
							<td class="RightTableCaption">
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="copy" style="border: none;" height="14"></a>
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="Add Row" style="border: none;" onClick="javascript:fnAddRows()"height="14"></a>
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="paste" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a>
							</td>									
						</tr>
						<tr class="Shade">
							<td height="17"><font color="#800000">&nbsp;<fmtWODueDateBulkUpload:message key="LBL_MAXIMUM_ROWS_TO_PASTE"/></font></td>
						</tr>
					</table>
				</div>
			</td>									
		</tr>		
		<tr><td colspan="2" ><div id="dataGridDiv"  style="height:450px;"></div></td></tr>		
		 <tr style="display: table-row" id="buttonshow" height="30" class="Shade"><td colspan="2"  align="center">
          <div id="buttonDiv">            
	            <fmtWODueDateBulkUpload:message key="BTN_PRINT" var="varSave"/>
	            <gmjsp:button value="${varSave}" controlId="BTN_PRINT" gmClass="button"  name="BTN_PRINT" onClick="fnPrint();" disabled="<%=strBtnDisable%>" buttonType="Save"/>			
		  </div>
			</td>
		</tr>
		</table>
		<%@ include file="/common/GmFooter.inc"%>
	</html:form>
</body>
</html>