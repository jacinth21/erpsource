<%
/**********************************************************************************
 * File		 		: GmDemandSheetMap.jsp
 * Desc		 		: mapping Group / Set / Region / InHouse
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 
 <!-- \operations\purchasing\orderplanning\GmOPDemandSheetReport.jsp-->
<bean:define id="alTemplates" name="frmOPDemandSheetReport" property="alTemplates" type="java.util.ArrayList"></bean:define>
<bean:define id="ldResult" name="frmOPDemandSheetReport" property="ldResult" type="java.util.List"></bean:define>

<%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	ArrayList alReportList = new ArrayList();
	alReportList = GmCommonClass.parseNullArrayList((ArrayList) ldResult);
	String strWikiTitle = GmCommonClass.getWikiTitle("TEMPLATE_MAPPING_REPORT");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPDemandSheetReport.js"></script>
<style type="text/css" media="all">@import url("<%=strCssPath%>/screen.css");</style>

<script> 
</script>
<style>
table.itss {
	width: 100%;
}

table.itss thead tr {
	background-color: #cccccc;
	height: 25px;
}

table.itss tr.even {
	background-color: #ecf6fe;
}

table.itss thead th {
	FONT-SIZE: 11px; 
	text-align: left;
}
</style>

</HEAD>
<BODY leftmargin="20" topmargin="10" onkeydown="fnOnHitEnter();">
<html:form action="/gmOPDemandSheetReport.do?method=loadTemplateReport" >
<html:hidden property="strOpt" />

<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->


	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Template Mapping Report</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />  
				</td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="3">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">                                       
                    <tr>
                      	<td class="RightTableCaption" align="Right" width="18%" height="25">Template Name:</td>
                    	<td class="RightTablecaption" align="left">&nbsp;<gmjsp:dropdown controlName="templateName" SFFormName="frmOPDemandSheetReport" SFSeletedValue="templateName" SFValue="alTemplates" defaultValue= "[Choose One]" 
								 codeId="DMID" codeName="DMNM"/>
						
						</tr>
                   	<tr><td class="LLine" height="1" colspan="4"></td></tr>
                   	<tr class="shade">             
                   	<td class="RightTableCaption" align="Right" width="18%" height="25">Set ID:</td>
                    <td class="RightTablecaption" align="left">
                    	<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    		<tr>
                    			<td>&nbsp;<html:text maxlength="50" property="templateSetId" size="15"   
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
								</td>      	
								<td class="RightTableCaption" align="Right" height="25" width="25%">Set/Group Name:</td>						
								<td width="30%" style="padding-left: 2px;"><html:text maxlength="50" property="templateSetGrpNm" size="30"   
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
								</td>				
								<td align="center"><gmjsp:button value="Load" gmClass="button" name="btn_Load" style="width: 4em;height: 23" onClick="fnLoad();" buttonType="Load" /></td>
								</tr>
								</table>	                  			
                   		</td>
                   		</tr>
                   		<tr><td class="LLine" height="1" colspan="4"></td></tr>
                    <tr>
                    	<td colspan="4" align="center" >
                    		<display:table name="requestScope.frmOPDemandSheetReport.ldResult" export="true" requestURI="/gmOPDemandSheetReport.do?method=loadTemplateReport" decorator="com.globus.displaytag.DTSheetSetPARWrapper" class="itss" id="currentRowObject" >
                            <display:setProperty name="export.excel.filename" value="Template Mapping Report.xls" /> 
                    		<display:setProperty name="export.excel.decorator"  value="com.globus.displaytag.DTSheetSetPARWrapper" /> 
                    		<display:column property="DEMANDSHEETNAME" title="Template Name" sortable="true" style="text-align:left;" group="1"/>
							<display:column property="DEMANDPERIOD" title="Demand Period" sortable="true" style="text-align:center;" group="1"/>
							<display:column property="FORECASTPERIOD" title="Forecast Period" sortable="true" style="text-align:center;" group="1" />							
							<display:column property="SETID" title="Set ID" sortable="true" media="html"/>
							<display:column property="EXCELSETID" title="Set ID" sortable="true" media="excel"/>
							<display:column property="SETGRPNM" title="Set/Group Name" sortable="true" class="alignleft"/>
							</display:table> 
		                </td>
                    </tr>
   				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
</BODY>
 <%@ include file="/common/GmFooter.inc" %> 
</HTML>
