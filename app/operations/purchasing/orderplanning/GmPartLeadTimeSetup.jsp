<%
/**********************************************************************************
 * File		 		: GmOPPartLeadTimeSetup.jsp
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>
  <!-- \operations\purchasing\orderplanning\GmPartLeadTimeSetup.jsp-->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="returnList" name="frmPartLeadTimeSetup" property="returnList" type="java.util.List"></bean:define>
<bean:define id="strAccessFl" name="frmPartLeadTimeSetup" property="accessFl" type="java.lang.String" />
<bean:define id="defaultLeadWeek" name="frmPartLeadTimeSetup" property="defaultLeadWeek" type="java.lang.String" />  
<bean:define id="cbo_search" name="frmPartLeadTimeSetup" property="cbo_search" type="java.lang.String" />  
<%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	ArrayList alSubList = new ArrayList();
	alSubList = GmCommonClass.parseNullArrayList((ArrayList) returnList);
	int rowsize = alSubList.size();
	String strSearch = cbo_search;
	String strWikiTitle = GmCommonClass.getWikiTitle("PART_LEAD_TIME_SETUP");
	String	strDisable = strAccessFl.equals("Y")?"":"Disabled";
	String strDefaultLeadWeek = defaultLeadWeek;
	String strJNDIConnection = GmCommonClass.parseNull(GmCommonClass.getString("JNDI_CONNECTION")); 
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Lead Time Override Setup</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmPartLeadTimeSetup.js"></script>

<script> 
var varRows = <%=rowsize%>;
var accessfl = '<%=strAccessFl%>';
var JNDI_CONNECTION = '<%=strJNDIConnection%>';
</script>
<style>
table.itss {
	width: 100%;
}

table.itss thead th {
	FONT-SIZE: 11px; 
	text-align: left;
}

table.itss thead tr {
	background-color: #cccccc;
	height: 25px;
}

table.itss tr.even {
	background-color: #ecf6fe;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeypress="fnOnHitEnter();" onload="fnLoad();">
<html:form action="/gmPartLeadTimeSetup.do">
<html:hidden property="strOpt" value="" />
<html:hidden property="hinputStr" />
<html:hidden property="accessFl" />
<input type="hidden" name="hSearch" value="<%=strSearch%>">

	<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" >Lead Time Override Setup</td>
			<td  height="25" class="RightDashBoardHeader" align="right"> 
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td height="100" valign="top" colspan="3" width="100%">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="evenshade" height="30">
						<td height="25" class="RightTableCaption" width="20%" align="right">Project List:</td>
						<td colspan="2">&nbsp;<gmjsp:dropdown controlName="projectListID" SFFormName="frmPartLeadTimeSetup" SFSeletedValue="projectListID"
							SFValue="projectList" codeId = "ID" onChange="fnOnChenge();" codeName = "IDNAME"  defaultValue= "[Choose One]"/> 		
						</td>
					</tr>
                    <tr><td colspan="3" class="LLine" height="1"></td></tr>
                    <tr class="oddshade" height="30">
                        <td class="RightTableCaption" align="right" height="25">&nbsp;Part Numbers:</td> 
                        <td width="55%">&nbsp;<html:text property="pnum"  size="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        &nbsp;<gmjsp:dropdown controlName="cbo_search" SFFormName="frmPartLeadTimeSetup" SFSeletedValue="cbo_search" SFValue="alWildSearch" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />		 
                        </td>
                        <td width="15%" align="left"><gmjsp:button name="Load" value="Load" style="height: 23; width: 5em;" gmClass="button" onClick="fnReload();" buttonType="Load" /></td> 
                    </tr>       
                    <tr><td colspan="3" class="LLine" height="1"></td></tr>
					<tr>
						<td colspan="3" align="center">
						<display:table name="requestScope.frmPartLeadTimeSetup.returnList" requestURI="/gmPartLeadTimeSetup.do" class="itss" id="currentRowObject" 
										 export="true" decorator="com.globus.operations.purchasing.orderplanning.displaytag.DTLeadTimeWrapper" > 
			 				<display:column property="PNUM" title="Part #" sortable="true"/>
							<display:column property="DESCRIPTION" escapeXml="true" title="Part Description" sortable="true" />
							<display:column property="LEADTIME" title="Lead Time (weeks)" style="text-align: left; width:90;"/>
							<display:column property="LASTUPDATEDBY" title="Updated By"  sortable="true"/>
							<display:column property="LASTUPDATEDDATE" title="Updated Date"  sortable="true"/>
							<display:column property="COMMENTS" title="Comments" style="text-align: center;" media="html" />
						</display:table>
						</td>
					</tr>
					
					<%	if (rowsize > 0) { %> 
					<tr><td class="LLine" height="1" colspan="3"></td></tr>
					<tr></tr> 
					<tr>
						<td colspan="3" align="center">
							<gmjsp:button name="Btn_Submit" value="Submit" gmClass="button" disabled="true" onClick="fnSubmit();" buttonType="Save" />
			         	</td>
					</tr>  
					<tr><td colspan="3">* Default Value is <%=strDefaultLeadWeek %> weeks</td></tr>             
					<% } %> 
				</table>
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>