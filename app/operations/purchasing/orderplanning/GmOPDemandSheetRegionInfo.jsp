 <%
/**********************************************************************************
 * File		 		: GmDemandSheetRegionInfo.jsp
 * Desc		 		: Report for Demand Sheet Region Details
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
   <!-- \sales\AreaSet\GmAddInfo.jsp -->
    <%@ include file="/common/GmHeader.inc" %>
 
<!-- WEB-INF path corrected for JBOSS migration changes -->
  
  <html> 
	<head>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	</head>
  	<body>
  	 <TABLE  cellSpacing=0 cellPadding=0  border=0> 
  	 <tr><td width="30%" valign="top" ><table class="border" cellspacing="1">
		<tr>
							<td class="RightBlueCaption" height="20" >&nbsp;Demand Period:</td>
							<td class="RightTableCaption">&nbsp;<bean:write name="frmOPDemandSheetSummary" property="demandPeriod"/>
							&nbsp;months</td>
						</tr>	
						<tr>	
							<td  class="RightBlueCaption" height="20">&nbsp;Forecast Period:</td>
							<td class="RightTableCaption">&nbsp;<bean:write name="frmOPDemandSheetSummary" property="forecastPeriod"/>
							&nbsp;months</td>
						</tr>
						<tr>
							<td  class="RightBlueCaption" height="20" >&nbsp;Set Build Forecast:</td>
							<logic:notEqual  name="frmOPDemandSheetSummary" property="requestPeriod" value="">
							<td class="RightTableCaption">&nbsp;<bean:write name="frmOPDemandSheetSummary" property="requestPeriod"/> &nbsp;months</td>	
							</logic:notEqual>
							<logic:equal  name="frmOPDemandSheetSummary" property="requestPeriod" value="">	
							<td class="RightTableCaption">&nbsp;<bean:write name="frmOPDemandSheetSummary" property="requestPeriod"/>  N/A</td>	
							</logic:equal>
						</tr>
						<tr>
							<td  class="RightBlueCaption" height="20" >&nbsp;Load Date:</td>
							<td class="RightTableCaption">&nbsp;<bean:write name="frmOPDemandSheetSummary" property="loadDate"/></td>		
						</tr><tr>
							<td  class="RightBlueCaption" height="20" >&nbsp;Status:</td>			
							<td class="RightTableCaption">&nbsp;<bean:write name="frmOPDemandSheetSummary" property="status"/>
							</td>					
						</tr>
						<tr>
							<td  class="RightBlueCaption" height="20" >&nbsp;Type:</td>
							<td class="RightTableCaption">&nbsp;<bean:write name="frmOPDemandSheetSummary" property="demandTypeNm"/>
							</td>					
						</tr>					
	</table></td>
	<td width="70%">&nbsp;</td>
	</tr>					
		</TABLE>
		</body>
</html>		
