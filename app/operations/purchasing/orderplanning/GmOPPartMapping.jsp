<%
/**********************************************************************************
 * File		 		: GmPartMapping.jsp
 * Desc		 		: Part Mapping Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
 
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("PART_MAPPING");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Part Mapping</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"
	media="print" />

<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnReload()
{    
	 if(document.frmOPSubPartOrder.pnum.value == ""&&document.frmOPSubPartOrder.projectListID.value == 0){
		Error_Details(" Please select at least one of Part number / Project List");
		}	
		if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	 
	if(!document.frmOPSubPartOrder.displaySubParts.checked)
	document.frmOPSubPartOrder.hsubpartNotcheck.value ="true";
	else document.frmOPSubPartOrder.hsubpartNotcheck.value ="false";
	if(!document.frmOPSubPartOrder.displayParentParts.checked)
	document.frmOPSubPartOrder.hparentpartNotcheck.value ="true";
	else document.frmOPSubPartOrder.hparentpartNotcheck.value ="false";
	document.frmOPSubPartOrder.strOpt.value = "reload"; 
	fnStartProgress('Y');
	document.frmOPSubPartOrder.submit();   
} 
 
  
 
 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmOPOPPartMapping.do">
	<html:hidden property="strOpt" value="" />
	<html:hidden property="hsubpartNotcheck" />
 	<html:hidden property="hparentpartNotcheck" />
	 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4">Part Mapping</td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="5"></td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="5">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td height="24" class="RightTableCaption" align="right">Project List :</td>
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="projectListID" SFFormName="frmOPSubPartOrder" SFSeletedValue="projectListID"
							SFValue="projectList" codeId = "ID"  codeName = "IDNAME"  defaultValue= "[Choose One]"/> 		
						</td>
					</tr>
				 
                    <tr><td colspan="5" class="ELine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Parent Part Numbers :</td> 
                        <td>&nbsp;
	                         <html:text property="pnum"  size="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 
	                        
	                        &nbsp;&nbsp;&nbsp; <gmjsp:button name="Load" value="&nbsp;&nbsp;Load&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnReload();" buttonType="Load" />
	                        &nbsp;&nbsp;
	                    
                        </td> 
                    </tr>       
                    <tr><td colspan="5" class="ELine"></td></tr>
                    
                    
                    <tr>
                    	 <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp; Show :</td> 
                    	<td  colspan="4" class="RightTableCaption"  > &nbsp; <html:checkbox  property="displayOrderParts" />
						 
						Order Parts &nbsp;&nbsp;&nbsp;&nbsp;
						 <html:checkbox  property="displaySubParts" />
						 Sub Parts &nbsp;&nbsp;&nbsp;&nbsp;
						  <html:checkbox  property="displayParentParts" />
						 Parent Parts</td>								
					</td>
			 		 
				</tr>
           <tr> 
		 
		<tr>
			<td class="Line" height="1" colspan="5"></td>
		</tr>
		  
		<tr>
			<td colspan="5">
			<display:table name="requestScope.frmOPSubPartOrder.returnList" requestURI="/gmOPOPPartMapping.do"  export="true" class="its" id="currentRowObject"  decorator="com.globus.operations.purchasing.orderplanning.displaytag.DTOPPartMappingWrapper" freezeHeader="true"> 
                <display:setProperty name="export.excel.filename" value="Part Mapping Report.xls" />
 				<display:column property="PNUM" title="Part #"   />
				<display:column property="DESCRIPTION"  escapeXml="true" title="Part Description"  class="alignleft" />
				<display:column property="TOORDER" title="To Order"  class="alignright" />
				<display:column property="QTY" title="Qty"  class="alignright" />
				 
			</display:table>
			</td>
		</tr>

		<tr>
			<td class="LLine" height="1" colspan="5"></td>
		</tr>
		 
		 
		</tr> 
		           
	 
		 
		
		 
	</table>
	</FORM>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>