<%
/**********************************************************************************
 * File		 		: GmGrowthReport.jsp
 * Desc		 		: This screen is used to display Growth Report
 * Version	 		: 1.1
 * author			: VPrasath
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<!-- \operations\purchasing\orderplanning\GmOPGrowthReport.jsp-->
<%@ page import ="com.globus.common.servlets.GmServlet"%>				


<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>

<%

	GmServlet gm = new GmServlet();
	
	String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROWTH_REPORT");
	
	ArrayList alDemandSheetIds = new ArrayList();
	ArrayList alRefTypes 	= new ArrayList();

	String strDemandSheetId = "";
	String strRefInputs = "";
	String strFromDate ="";
	String strToDate="";
	String strDemandSheetName="";
		
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	String strhAction =  GmCommonClass.parseNull((String)request.getAttribute("hAction"));	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	
	if(hmReturn !=null)
	 {
	   alDemandSheetIds = (ArrayList)hmReturn.get("DEMANDSHEETS");
	   alRefTypes = (ArrayList) hmReturn.get("REFTYPES");
	   strDemandSheetName = (String) hmReturn.get("DEMAND_SHEET_NAME");
	 
	   strDemandSheetId = GmCommonClass.parseNull((String) request.getParameter("demandSheetId"));
	   strRefInputs = GmCommonClass.parseNull((String) request.getParameter("refInputs"));
	   strFromDate = GmCommonClass.parseNull((String)request.getParameter("Txt_FromDate"));   
	   strToDate = GmCommonClass.parseNull((String)request.getParameter("Txt_ToDate"));   
	 }

	 strFromDate = strFromDate.equals("") ? GmCalenderOperations.getCurrentMonth()+"/01/"+GmCalenderOperations.getCurrentYear() : strFromDate; 
     strToDate =  strToDate.equals("") ? GmCalenderOperations.getCurrentMonth()+"/01/"+(GmCalenderOperations.getCurrentYear() + 1) : strToDate;
		
	HashMap hmRefType = new HashMap();
	hmRefType.put("ID","");
	hmRefType.put("PID","CODEID");
	hmRefType.put("NM","CODENM");
			
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Growth Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnGo()
 {
	var objRefTypes = document.frmGrowth.Chk_GrpRefType;
 	var reflen = objRefTypes.length;
 	var refTypes = "";
 	
 for(var i=0;i<reflen;i++)
	{
		if (objRefTypes[i].checked == true)
		{
			refTypes = refTypes + objRefTypes[i].value + ',';
		}
	}
  document.frmGrowth.refInputs.value = refTypes;	
  document.frmGrowth.hAction.value="LoadReport";
  fnStartProgress();
  document.frmGrowth.submit();
 }
 
 function fnLoad()
{
	
	var reftypes = '<%=strRefInputs%>';	
	var objgrp = '';
	
	if (reftypes != '')
	{
		objgrp = document.frmGrowth.Chk_GrpRefType;
		fnCheckSelections(reftypes,objgrp);
	}		
}


 // Marks the checkboxes as 'checked'
function fnCheckSelections(val,objgrp)
{
	var valobj = val.split(",");
	var sval = '';
	var arrlen = objgrp.length;
	
	if (arrlen >= 0)
	{
		for (var j=0;j< valobj.length;j++ )
		{
			sval = valobj[j];
			for (var i=0;i< arrlen;i++ )
			{
				if (objgrp[i].value == sval)
				{
					objgrp[i].checked = true;
					break;
				}
			}
		}
	}
	else
	{
		objgrp.checked = true;
	}
}

 

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnLoad();">
<FORM name="frmGrowth" method="post" action = "<%=strServletPath%>/GmOPGrowthReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="refInputs">


<TABLE cellSpacing=0 cellPadding=0  border=0 width="1000">
<TBODY>
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1 colspan=2></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr class=Line >
	<td height=25 class=RightDashBoardHeader >Growth Report </td>
			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
	</tr>
	<tr class=Line><td noWrap height=1 colspan=2></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td ><table><tr>
							<td class="RightText" nowrap align="right" HEIGHT="24">Demand Sheet Name:</td>
							<%-- <td>&nbsp;<gmjsp:dropdown controlName="demandSheetId" value= "<%=alDemandSheetIds%>" seletedValue="<%=strDemandSheetId%>"
								 	width="250" codeId="DMID" codeName="DMNM" defaultValue="[Choose One]"/>
							</td> --%>
						<td>
						<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="demandSheetId" />
					<jsp:param name="METHOD_LOAD" value="loadDemandSheetList&GOPLOAD=Y&searchType=PrefixSearch&GOPLOADGRW=Y" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="1"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strDemandSheetName %>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strDemandSheetId %>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="" />
						</jsp:include>				
						</td>
						</tr></table></td>
					</tr>
					<tr>
						<td><table><tr>
							<td class="RightText" nowrap align="right" HEIGHT="24">Reference Type:</td>
							<td><%=GmCommonControls.getChkBoxGroup("",alRefTypes,"RefType",hmRefType)%></td>						
						</tr></table></td>	
					</tr>				
			<tr>
			  <td><table><tr>
			<td class="RightTableCaption" >From Date:</td>
			<td >&nbsp;<input type="text" size="10" value="<%=strFromDate%>" name="Txt_FromDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmGrowth.Txt_FromDate');" title="Click to open Calendar"  
						src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp; 
			</td>
			<td class="RightTableCaption" >To Date:</td>
			<td >&nbsp;<input type="text" size="10" value="<%=strToDate%>" name="Txt_ToDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmGrowth.Txt_ToDate');" title="Click to open Calendar"  
						src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp; 
					   <gmjsp:button value="&nbsp;&nbsp;Go&nbsp;&nbsp;" buttonType="Load" onClick="javascript:fnGo();" gmClass="button" />						
			</td>
			<!-- 
			<td>  <input type=button value="Excel Download"  class=button >	</td>
			 -->					
			</tr> </table></td>
	</tr>			
	<!-- Addition Filter Information -->
	<tr >
		<td noWrap colspan=2>  </td>
	</tr>
	
<%
// To Generate the Crosstab report
	gmCrossTab.setHeader("Growth Report");
			
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(70);
	
	// Line Style information
	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	
		
	
	// Sort Functionality 
	// gmCrossTab.setSortRequired(true);
	// gmCrossTab.addSortType("TYPE","String");
	gmCrossTab.setRoundOffAllColumnsPrecision("0");	
	gmCrossTab.setNoDivRequired(true);	
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.displayZeros(true);
%>		
	<tr>
		<td align=center  colspan=2> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
	</tr>
	<tr class=Line><td noWrap height=1 colspan=2></td></tr>
</table>	
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
