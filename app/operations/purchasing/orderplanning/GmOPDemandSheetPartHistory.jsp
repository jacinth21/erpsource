   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
  
   <%@ page import ="com.globus.common.servlets.GmServlet"%>
   <%@ include file="/common/GmHeader.inc" %>
 <!-- \operations\purchasing\orderplanning\GmOPDemandSheetPartHistory.jsp --> 
<!-- WEB-INF path corrected for JBOSS migration changes -->
  
   
   <bean:define id="alReport" name="frmOPDemandSheetSummary"  property="alPartList" type="java.util.ArrayList"></bean:define>

  <%
    GmServlet gm = new GmServlet();
	
	String strCurPartID = "";
	String strPrePartID = "";	
	String strCurPeriod = "";
	String strPrePeriod = "";	
	String strCSS = "";
    try
    {	
	    HashMap hmapValue = new HashMap();    
        
  %>
  <html> 

	<head></head>
  	<body>
  	 <TABLE  cellSpacing=0 cellPadding=0  border=0> 
  	 	<tr><td class=borderDark colspan="3" noWrap width=1></td></tr>
  	 	<tr> <td class=borderDark width=1></td>    
  	 	<td>
  	 	<TABLE  cellSpacing=0 cellPadding=0  border=0>      
  		<tr>
  			<th class=ShadeDarkOrangeTD >Part #</th>
  			<th	class=ShadeDarkOrangeTD >Desc</th>
  			<td class=borderDark width=1></td> 
  			<th class=ShadeDarkOrangeTD >Period</th>
  			<th class=ShadeDarkOrangeTD  >Value</th>
  			<th class=ShadeDarkOrangeTD >By</th>
  			<th class=ShadeDarkOrangeTD >Date</th>
  	 	</tr>
  	 	
 <% //*******************************************	
 	// Part History list 
 	//*******************************************	
 		int Headersize = alReport.size();   
 		 
 	 	for (int i = 0; i < Headersize; i++)
        {
            hmapValue = (HashMap) alReport.get(i);
            strCurPartID = (String)hmapValue.get("PID");
            strCurPeriod = (String)hmapValue.get("PERIOD");

  			// Part Number check 
  			if (!strCurPartID.equals(strPrePartID)) {
  					strPrePartID = strCurPartID;
  					strPrePeriod = "";
%>
  				<tr><td class=borderDark colspan="7" noWrap height=1></td></tr>
  				<tr height=20>
		  		<td class=ShadeMedOrangeTD><%=(String)hmapValue.get("PID")%></td>
				<td class=ShadeMedOrangeTD><%=(String)hmapValue.get("PNAME")%></td>
				<td class=borderDark width=1>
<%			} 

			// Period check 
			if (!strCurPeriod.equals(strPrePeriod)) {
  				if (!strPrePeriod.equals(""))
  				{ %>
  					<tr><td colspan="2" noWrap height=1> <td class=borderDark colspan="5" noWrap height=1></td></tr>
  					<tr height=20><td></td><td></td>
					<td class=borderDark width=1></td> 
<%  			}
  				strPrePeriod = strCurPeriod;
  				strCSS = "ShadeWhite";
  				%>
				<td class=ShadeLightOrangeTD><%=(String)hmapValue.get("PERIOD")%></td>
<%			} else {  
				strCSS = "strikethruRedTD";
%>		  		<td></td><td></td><td class=borderDark width=1></td><td></td>
<%			}%>		
				<td class=<%=strCSS%> ><%=(String)hmapValue.get("QVALUE")%></td>
				<td class=<%=strCSS%> ><%=(String)hmapValue.get("UBY")%></td>
				<td class=<%=strCSS%> ><%=(String)hmapValue.get("UDATE")%></td>
		</tr>		
<%		} // End of for loop %>
		</table>
		</td><td class=borderDark width=1></td></tr>
		<tr><td class=borderDark colspan="3" noWrap width=1></td></tr>    
		</table>
		</body>
</html>		
<%	}catch(Exception e)
	{
		e.printStackTrace();
	}
	%>
