<%
/**********************************************************************************
 * File		 		: GmOPTTPFailedReport.jsp
 * Desc		 		: TTP Failed Report screen 
 * Version	 		: 1.0
 * author			: Prabhu vigneshwaran M D 
************************************************************************************/
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

 <!-- \operations\purchasing\orderplanning\GmOPTTPFailedReport.jsp-->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtrebateMgnt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtTTPApproval" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtTTPApproval:setLocale value="<%=strLocale%>"/>
<fmtTTPApproval:setBundle basename="properties.labels.operations.purchasing.GmOPTTPApproval" />

 <bean:define id="strXmlGridData" name="frmTTPApproval" property="strXmlGridData" type="java.lang.String"></bean:define>
 <%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	String strWikiTitle = GmCommonClass.getWikiTitle("TTP_FAILED_SUMMARY");

 %>
 
 <html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">	
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmTTPFailedReport.js"></script>	
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script>
var objGridData='';
objGridData = '<%=strXmlGridData%>';
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form
		action="/gmTTPSummaryApprove.do?method=loadFailedTTPInformation">
	
		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader"><fmtTTPApproval:message
						key="FAILED_TTP_INFORMATION" /></td>
				<td height="25" class="RightDashBoardHeader"><fmtTTPApproval:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>	
			    <%
					if (strXmlGridData.indexOf("cell") != -1) {
			%>  
			<tr>
				<td colspan="4">
					<div id="dataGridDiv" class="" height="400px"></div>
				</td>
			</tr>
		
	
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
				<%
					}else{
		        %> 
			<tr>
			<td colspan="4" align="center" height="25" class="RegularText"><div id="DivNothingMessage"><fmtTTPApproval:message key="NO_DATA_FOUND"/></div></td>
			</tr>
		<%} %>
			<tr>
				 <td colspan="4" height="25" align="center"><gmjsp:button value="Close"
							name="BTN_CLOSE" gmClass="button" buttonType="Load"
							onClick="fnClose();" />&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4" class="LLine" height="1"></td>
			</tr>	
		</table>
	</html:form>
</body>
<%@ include file="/common/GmFooter.inc" %>
</html>