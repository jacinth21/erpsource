<%
/**********************************************************************************
 * File		 		: GmTTPMonthlyTrans.jsp
 * Desc		 		: Groups the TTP with Demand Sheet for a specific month
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
 <!-- \operations\purchasing\orderplanning\GmOPTTPMonthlyTrans.jsp--> 
<bean:define id="alTTPList" name="frmOPTTPMonthly" property="alTTPList" type="java.util.ArrayList"></bean:define>
<bean:define id="alTTPLevel" name="frmOPTTPMonthly" property="alTTPLevel" type="java.util.ArrayList"></bean:define>
<bean:define id="alSelectedSheet" name="frmOPTTPMonthly" property="alSelectedSheet" type="java.util.ArrayList"></bean:define>
<bean:define id="submitSts" name="frmOPTTPMonthly" property="submitSts" type="java.lang.String"> </bean:define>
<bean:define id="strttpId" name="frmOPTTPMonthly"  property="ttpId" type="java.lang.String"></bean:define>
<bean:define id="strttpName" name="frmOPTTPMonthly"  property="searchttpId" type="java.lang.String"></bean:define>
<bean:define id="strShowForecastFl" name="frmOPTTPMonthly"  property="strShowForecastFl" type="java.lang.String"></bean:define>
<%
String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("TTP_DEMANDSHEET_MAPPING");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPTTPMonthlyTrans.js"></script>

<script>
var TTPListLen = <%= alTTPList.size()%>;
var TTPLevelLen = <%= alTTPLevel.size()%>;
var TTPSheetCount = <%= alSelectedSheet.size()%>;
var arrTTPMapping  = new Array();
var arrLVLMapping  = new Array(); 
//Creating 2D arrays for TTPLIST and TTP Level.
if (TTPListLen != undefined && TTPListLen !=0){
	arrTTPMapping = [
	<%           HashMap hmTTPList = new HashMap();  
				 for(int i=0;i<alTTPList.size();i++){  
				    hmTTPList = (HashMap)alTTPList.get(i);
				    String strTTPId = GmCommonClass.parseNull((String)hmTTPList.get("CODEID"));
					String strCompanyId = GmCommonClass.parseNull((String)hmTTPList.get("COMPANY_ID"));
	%>               ["<%=strTTPId%>","<%=strCompanyId%>"]
	<%					 if(i!=(alTTPList.size()-1)){   
	%>                 ,
	<%				    } 
	              } 
	%>               ];
}
if (TTPLevelLen != undefined && TTPLevelLen !=0){
	arrLVLMapping = [
	<%           for(int i=0;i<alTTPLevel.size();i++){  
				    hmTTPList = (HashMap)alTTPLevel.get(i);
				    String strLVLId = GmCommonClass.parseNull((String)hmTTPList.get("LVL_ID"));
					String strInventory  = GmCommonClass.parseNull((String)hmTTPList.get("INVENTORY"));
	%>               ["<%=strLVLId%>","<%=strInventory%>"]
	<%					 if(i!=(alTTPLevel.size()-1)){   
	%>                 ,
	<% }  } %>];
}
if (TTPLevelLen != undefined && TTPLevelLen !=0){
	arrHCHYMapping = [
	<%           for(int i=0;i<alTTPLevel.size();i++){  
				    hmTTPList = (HashMap)alTTPLevel.get(i);
				    String strLVLId = GmCommonClass.parseNull((String)hmTTPList.get("LVL_ID"));
					String strInventory  = GmCommonClass.parseNull((String)hmTTPList.get("LVL_HIERARCHY"));
	%>               ["<%=strLVLId%>","<%=strInventory%>"]
	<%					 if(i!=(alTTPLevel.size()-1)){   
	%>                 ,
	<% }  } %>];
}
//alert("arrTTPMapping::onload"+arrTTPMapping);
//alert("arrLVLMapping::onload"+arrLVLMapping);
function fnVoid()
{
		document.frmOPTTPMonthly.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmOPTTPMonthly.hAction.value = "Load";
		document.frmOPTTPMonthly.hTxnId.value = document.frmOPTTPMonthly.refId.value
		document.frmOPTTPMonthly.submit();
}		
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnPageOnLoad();" >
<html:form action="/gmOPTTPMonthlyTrans.do"  >
<html:hidden property="strOpt" />
<html:hidden property="ttpName"/>
<html:hidden property="hCompanyID"/> 
<html:hidden property="hLvlHierarchy"/>
<html:hidden property="lblInventory"/> 
<html:hidden property="hInventoryId"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">TTP - Finalizing</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td colspan="4" height="1" class="line"></td></tr>
		<tr>
			<td width="1100" height="100%" valign="top" colspan="4">
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
					<tr height="5"><td colspan="4"></td></tr>
					<tr class="evenshade" >
						<td class="RightTableCaption" align="right" HEIGHT="30" width="10%">&nbsp;<font color="red">*&nbsp;</font>TTP Name:</td>
						<td class="RightTableCaption"   HEIGHT="30" width="22%" style="padding-left: 5px;display: inline-block;position: absolute;margin-top: 6px;">
                         <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					     <jsp:param name="CONTROL_NAME" value="ttpId" />
					     <jsp:param name="METHOD_LOAD" value="loadTTPList&STROPT=GOP"/>
					     <jsp:param name="WIDTH" value="250" />
					     <jsp:param name="CSS_CLASS" value="search" />
					     <jsp:param name="TAB_INDEX" value=""/>
					     <jsp:param name="CONTROL_NM_VALUE" value="<%=strttpName %>" />
					     <jsp:param name="CONTROL_ID_VALUE" value="<%=strttpId %>" />
					     <jsp:param name="SHOW_DATA" value="50" />	
					      <jsp:param name="AUTO_RELOAD" value="fetchLevel(this.value);" />		
				         </jsp:include>			
						 </td>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="28%">&nbsp;
						  <span id="lvldrpdn"><font color="red">*&nbsp;</font>Level:&nbsp;<gmjsp:dropdown controlName="levelId" SFFormName="frmOPTTPMonthly" SFSeletedValue="levelId"
							SFValue="alTTPLevel" codeId = "LVL_ID"  codeName = "LVL_NAME" defaultValue= "[Choose One]" onChange="setInventory(this.value)"/></span></td>
                        <td class="RightTableCaption" align="right" width="40%"  HEIGHT="30" valign="top">&nbsp;
                        	<font color="red">*&nbsp;</font>Month:&nbsp;<gmjsp:dropdown controlName="monthId" SFFormName="frmOPTTPMonthly" SFSeletedValue="monthId"
							SFValue="alMonth" codeId = "CODENMALT"  codeName = "CODENM" defaultValue= "[Choose One]" />													
							&nbsp;<font color="red">*&nbsp;</font>Year:&nbsp;<gmjsp:dropdown controlName="yearId" SFFormName="frmOPTTPMonthly" SFSeletedValue="yearId"
							SFValue="alYear" codeId = "CODENM"  codeName = "CODENM"  defaultValue= "[Choose One]" />
							&nbsp;<gmjsp:button controlId="button"  align="bottom" value="&nbsp;&nbsp;&nbsp;Load&nbsp;&nbsp;" gmClass="button" onClick="fnFetch();" buttonType="Load"/> &nbsp;
    		             </td>
                    </tr>  
                    <tr><td colspan="4" class="LLine"></td></tr>
<span id="lblFilterdata">
                    <tr class="evenshade">
                    	<td class="RightTableCaption" align="right" HEIGHT="30" >&nbsp;Linked Sheets:</td> 
                         <td colspan="3" width="820">&nbsp;
                          <DIV style="display:visible;height: 200px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                                                   <table >
	                        	<tr bgcolor="gainsboro">
	                        		<td> 
									    <html:checkbox property="selectAll" onclick="fnSelectAll('toggle');" /><B>Select All </B> 
		     						</td>
								</tr>	    
							</table>
                         <table>
                        	<logic:iterate id="selectedDS" name="frmOPTTPMonthly" property="alSelectedSheet">
                        	<tr>
                        		<td>
								    <htmlel:multibox property="checkSelectedSheet" value="${selectedDS.ID}"   alt="${selectedDS.STATUSID}"/>
								    <bean:write name="selectedDS" property="NAME" />
	     						</td>
							</tr>	    
							</logic:iterate>
							</table>
							</DIV>&nbsp;
                        </td>
                      </tr>
                       <tr><td colspan="4" class="LLine"></td></tr> 
<!--                 <tr>
                     <td class="RightTableCaption" align="right" HEIGHT="30" >&nbsp;Other Sheets :</td> 
                        <td colspan="4">&nbsp;
                        <DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                        <table>
                        	<logic:iterate id="unSelectedDS" name="frmOPTTPMonthly" property="alUnSelectedSheet">
                        	<tr>
                        		<td>
							    	<htmlel:multibox property="checkUnSelectedSheet" value="${unSelectedDS.ID}" />
								    <bean:write name="unSelectedDS" property="NAME" />
								</td>
							</tr>	    
							</logic:iterate>
						</table>
						</DIV>
                        </td>
                       </tr>  --> 
                    <tr class="oddshade">
                        <td class="RightTableCaption" align="right" HEIGHT="30" width="120">&nbsp;Inventory Id:</td> 
	                     <td width="280">&nbsp;
	                     <gmjsp:dropdown controlName="inventoryId" SFFormName="frmOPTTPMonthly" SFSeletedValue="inventoryId"
							SFValue="alInventoryIdList" codeId = "ID"  codeName = "NAME" />													
    		             </td>
    		             <td class="RightTableCaption" align="left" HEIGHT="30" width="200">&nbsp;Inventory:
    		             <span id="lblInventoryNm"><bean:write name="frmOPTTPMonthly" property="lblInventory" /></sapn>
	                   	</td>
    		             <td width="400">&nbsp;</td>
                    </tr>  
                    <tr><td colspan="4" class="LLine"></td></tr>
                    <!-- Readonly the ttpforecastmonths field-PMT28391 -->
				       	<logic:equal name="frmOPTTPMonthly" property="strShowForecastFl" value="Y"> 
                       <tr class="evenshade">
                        <td class="RightTableCaption" align="right" HEIGHT="30"><font color="red">*</font>&nbsp;Forecast Months:</td> 
	                     <td colspan="2">&nbsp;
	                     <html:text property="ttpForecastMonths"  size="4"  maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" readonly="true"/> 													
    		             </td>
                        </tr> 
                        </logic:equal> 
                            <tr><td colspan="4" class="LLine"></td></tr>
                            <tr>
                    <% String strDisableBtnVal = submitSts;
						if(strDisableBtnVal.equals("disabled")){
							strDisableBtnVal ="true";
						}
					 %>
                    	<td colspan="4" align="center" height="30">
	                    	<gmjsp:button disabled="<%=strDisableBtnVal%>" value="View Summary" name="ViewSummary" gmClass="button" onClick="fnViewSummary();" buttonType="Load" />
		                </td>
                    </tr></span>
                   
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>

