<%
/**********************************************************************************
 * File		 		: GmTTPMonthlySummary.jsp
 * Desc		 		: TTP Monthly Summary
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
 <!-- \operations\purchasing\orderplanning\GmOPTTPMonthlySummary.jsp-->
<bean:define id="forecastMonths" name="frmOPTTPMonthlySummary" property="ttpForecastMonths" type="java.lang.String"> </bean:define>
<bean:define id="yearId" name="frmOPTTPMonthlySummary" property="yearId" type="java.lang.String"> </bean:define>
<bean:define id="monthId" name="frmOPTTPMonthlySummary" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="inventoryId" name="frmOPTTPMonthlySummary" property="inventoryId" type="java.lang.String"> </bean:define>
<bean:define id="demandSheetIds" name="frmOPTTPMonthlySummary" property="demandSheetIds" type="java.lang.String"> </bean:define>
<bean:define id="ttpId" name="frmOPTTPMonthlySummary" property="ttpId" type="java.lang.String"> </bean:define>
<bean:define id="strOpt" name="frmOPTTPMonthlySummary" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="ttpType" name="frmOPTTPMonthlySummary" property="ttpType" type="java.lang.String"> </bean:define>
<bean:define id="ttpName" name="frmOPTTPMonthlySummary" property="ttpName" type="java.lang.String"> </bean:define>
<bean:define id="alTTPList" name="frmOPTTPMonthlySummary" property="alTTPList" type="java.util.ArrayList"></bean:define>
<bean:define id="alTTPLevel" name="frmOPTTPMonthlySummary" property="alTTPLevel" type="java.util.ArrayList"></bean:define>
<bean:define id="levelId" name="frmOPTTPMonthlySummary" property="levelId" type="java.lang.String"> </bean:define>
<bean:define id="hLvlHierarchy" name="frmOPTTPMonthlySummary" property="hLvlHierarchy" type="java.lang.String"> </bean:define>
<bean:define id="alFilters" name="frmOPTTPMonthlySummary" property="alFilters" type="java.util.ArrayList"></bean:define>
<bean:define id="hCompanyID" name="frmOPTTPMonthlySummary" property="hCompanyID" type="java.lang.String"> </bean:define>
<bean:define id="strITGOPFl" name="frmOPTTPMonthlySummary" property="strITGOPFl" type="java.lang.String"> </bean:define>
<% 
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	String strWikiTitle = GmCommonClass.getWikiTitle("TTP_MONTHLY_SUMMARY");
	HashMap hmReportValue = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMVALUES"));
	String strStatus = GmCommonClass.parseNull((String)hmReportValue.get("STATUS")) ;
	String strDSStatus = GmCommonClass.parseNull((String)hmReportValue.get("DSSTATUS")) ; // For Submit button to display.
	log.debug("strstrDSStatus = " + strDSStatus);
	ArrayList alDrillDown = new ArrayList();
	ArrayList alSheetList = new ArrayList();
	if(forecastMonths.equals(""))
	{
		forecastMonths = "4";
	}	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: TTP Monthly Summary </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPDemandSheet.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPTTPMonthlySummary.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var yearIDVal='<%=yearId%>';
var monthIDVal='<%=monthId%>';
var invIDVal='<%=inventoryId%>';

var TTPListLen = <%= alTTPList.size()%>;
var TTPLevelLen = <%= alTTPLevel.size()%>;
var TTPFiltersLen = <%= alFilters.size()%>;
var arrTTPMapping  = new Array();
var arrLVLMapping  = new Array();
var arrFilters  = new Array();
//Creating 2D arrays for TTPLIST and TTP Level.
if (TTPListLen != undefined && TTPListLen !=0){
	arrTTPMapping = [
	<%           HashMap hmTTPList = new HashMap();  
				 for(int i=0;i<alTTPList.size();i++){  
				    hmTTPList = (HashMap)alTTPList.get(i);
				    String strTTPId = GmCommonClass.parseNull((String)hmTTPList.get("CODEID"));
					String strCompanyId = GmCommonClass.parseNull((String)hmTTPList.get("COMPANY_ID"));
	%>               ["<%=strTTPId%>","<%=strCompanyId%>"]
	<%					 if(i!=(alTTPList.size()-1)){   
	%>                 ,
	<%				    } 
	              } 
	%>               ];
}
if (TTPLevelLen != undefined && TTPLevelLen !=0){
	arrLVLMapping = [
	<%           for(int i=0;i<alTTPLevel.size();i++){  
				    hmTTPList = (HashMap)alTTPLevel.get(i);
				    String strLVLId = GmCommonClass.parseNull((String)hmTTPList.get("LVL_ID"));
					String strInventory  = GmCommonClass.parseNull((String)hmTTPList.get("INVENTORY"));
	%>               ["<%=strLVLId%>","<%=strInventory%>"]
	<%					 if(i!=(alTTPLevel.size()-1)){   
	%>                 ,
	<% }  } %>];
}
if (TTPLevelLen != undefined && TTPLevelLen !=0){
	arrHCHYMapping = [
	<%           for(int i=0;i<alTTPLevel.size();i++){  
				    hmTTPList = (HashMap)alTTPLevel.get(i);
				    String strLVLId = GmCommonClass.parseNull((String)hmTTPList.get("LVL_ID"));
					String strInventory  = GmCommonClass.parseNull((String)hmTTPList.get("LVL_HIERARCHY"));
	%>               ["<%=strLVLId%>","<%=strInventory%>"]
	<%					 if(i!=(alTTPLevel.size()-1)){   
	%>                 ,
	<% }  } %>];
}

//Creating array for filters.
if (TTPFiltersLen != undefined && TTPFiltersLen !=0){
	arrFilters = [
	<%           HashMap hmFilters = new HashMap();  
				 for(int i=0;i<alFilters.size();i++){  
					 hmFilters = (HashMap)alFilters.get(i);
				    String strFilterId = GmCommonClass.parseNull((String)hmFilters.get("CODEID"));
					
	%>               ["<%=strFilterId%>"]
	<%					 if(i!=(alFilters.size()-1)){   
	%>                 ,
	<%				    } 
	              } 
	%>               ];
}

function fnFilter()
{
var queryString = '';
var foreCastMonth = document.frmOPTTPMonthlySummary.tempForecastMonths.value;
var qtyOper = document.frmOPTTPMonthlySummary.qtyOper.value;
var orderQty = document.frmOPTTPMonthlySummary.orderQty.value;
var pnum = TRIM(document.frmOPTTPMonthlySummary.pnum.value);
var ordOper = document.frmOPTTPMonthlySummary.ordOper.value;
var orderCost = document.frmOPTTPMonthlySummary.orderCost.value;

for(var i=0;i<TTPFiltersLen;i++){  
	 var codeid = arrFilters[i];	 
	 obj = eval("document.frmOPTTPMonthlySummary.chk_"+codeid);
	 //adding filters into request parameter
	 if(obj!=null && obj!='undefined'){ 
	 queryString += '&chk_'+codeid+'='+obj.checked;
	 }
}

fnStartProgress('Y');
var url='/gmOPTTPMonthlySummary.do?strOpt=reportDetails'+queryString+'&ttpId=<%=ttpId%>&yearId=<%=yearId%>&monthId=<%=monthId%>&inventoryId=<%=inventoryId%>&ttpForecastMonths='+foreCastMonth+'&tempForecastMonths='+foreCastMonth+'&demandSheetIds=<%=demandSheetIds%>&levelId=<%=levelId%>&hLvlHierarchy=<%=hLvlHierarchy%>&hCompanyID=<%=hCompanyID%>&qtyOper='+qtyOper+'&orderQty='+orderQty+'&pnum='+pnum+'&ordOper='+ordOper+'&orderCost='+orderCost;
dhtmlxAjax.get(url+'&ramdomId='+Math.random(),function(loader){
	if (loader.xmlDoc.responseText != null)
	{
		var data = loader.xmlDoc.responseText;
		document.getElementById("ajaxreportarea").innerHTML = data;
		fnStopProgress();	
	}
	});	
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  onLoad="fnSetLevel()">
<html:form action="/gmOPTTPMonthlySummary.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="demandSheetIds"/>
<html:hidden property="httpId"/>
<html:hidden property="ttpForecastMonths"/>
<html:hidden property="strPnumOrd"/>
<html:hidden property="ttpName"/>
<html:hidden property="hCompanyID"/>
<html:hidden property="hLvlHierarchy"/>
<html:hidden property="lblInventory"/>  
 <!-- Custom tag lib code modified for JBOSS migration changes -->
<table border="0" class="DtTable1300" cellspacing="0" cellpadding="0" id='tbcontid'>
		<tr>
			<td height="25" class="RightDashBoardHeader">TTP Monthly Summary</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr>
			<td width="1020" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" >
                    <tr><td colspan="7" class="ELine"></td></tr> 
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="15%"><font color="red">*</font>&nbsp;TTP Name List:</td>
						<td width="14%" style="padding-left: 2px;">
						    <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					        <jsp:param name="CONTROL_NAME" value="ttpId" />
					        <jsp:param name="METHOD_LOAD" value="loadTTPList&STROPT=GOP" />
					        <jsp:param name="WIDTH" value="240" />
					        <jsp:param name="CSS_CLASS" value="search" />
					        <jsp:param name="TAB_INDEX" value=""/>
					        <jsp:param name="CONTROL_NM_VALUE" value="<%=ttpName %>" />
					        <jsp:param name="CONTROL_ID_VALUE" value="<%=ttpId %>" />
					        <jsp:param name="SHOW_DATA" value="50" />
					        <jsp:param name="AUTO_RELOAD" value="fetchLevel();" />	
				            </jsp:include>	
						</td>
						<td class="RightTableCaption" align="left" HEIGHT="30" width="26%">&nbsp;
						  <span id="lvldrpdn"><font color="red">*&nbsp;</font>Level:&nbsp;<gmjsp:dropdown controlName="levelId" SFFormName="frmOPTTPMonthlySummary" SFSeletedValue="levelId"
							SFValue="alTTPLevel" codeId = "LVL_ID"  codeName = "LVL_NAME" defaultValue= "[Choose One]" onChange="setInventory(this.value)" width="155"/></span></td>
						<td class="RightTableCaption" align="left" HEIGHT="30" width="16%"><span id="labelInventory">&nbsp;Inv:</span>
    		             <span id="lblInventoryNm"><bean:write name="frmOPTTPMonthlySummary" property="lblInventory" /></span>
	                   	</td>	
						<td class="RightTableCaption" align="right" HEIGHT="30" width="5%"></font>&nbsp;Month:</td>
						<td width="8%" style="padding-left: 2px;"><gmjsp:dropdown controlName="monthId" SFFormName="frmOPTTPMonthlySummary" SFSeletedValue="monthId"
								SFValue="alMonth" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]"  width="105"/>													
						</td>
						
						<td class="RightTableCaption" align="right" HEIGHT="30" width="5%"></font>&nbsp;Year:</td>
						<td width="6%">&nbsp;<gmjsp:dropdown controlName="yearId" SFFormName="frmOPTTPMonthlySummary" SFSeletedValue="yearId"
								SFValue="alYear" codeId = "CODENM"  codeName = "CODENM"  defaultValue= "[Select]" width="68" />													
						</td>		
						<td width="10%">
							&nbsp;&nbsp;<gmjsp:button name="loadReport" value="&nbsp;Load&nbsp;" gmClass="button" onClick="fnReport();" buttonType="Load" />
						</td>			
					</tr>
                    <tr><td colspan="15" class="Line"></td><td colspan="7" class="Line"></td></tr> 
                    <%if(!strOpt.equals("")) { %>
					<tr>
						<td colspan ="15" align=center>
						<ul id="maintab" class="shadetabs">
						<!--PMT-28391 TTP Summary screen performance-Screen -Add new Tab to view the summary screen -->
		 			    <li onclick="fnGridStyle()"><a href="/gmOPTTNewSummary.do?method=loadTTPNewSummaryDtls&ttpId=<%=ttpId%>&yearId=<%=yearId%>&monthId=<%=monthId%>&inventoryId=<%=inventoryId%>&ttpForecastMonths=<%=forecastMonths%>&demandSheetIds=<%=demandSheetIds%>&levelId=<%=levelId%>&hCompanyID=<%=hCompanyID%>&lvlHierarchy=<%=hLvlHierarchy%>&ttpName=<%=ttpName%>" rel="#iframe" title="NewSummary" class="selected">Summary</a></li>
    					<% if (strITGOPFl.equals("Y")){%>
    					<li onclick="fnDisplayTagStyle()"><a href="/gmOPTTPMonthlySummary.do?strOpt=reportSummary&ttpId=<%=ttpId%>&yearId=<%=yearId%>&monthId=<%=monthId%>&inventoryId=<%=inventoryId%>&ttpForecastMonths=<%=forecastMonths%>&demandSheetIds=<%=demandSheetIds%>&levelId=<%=levelId%>&hLvlHierarchy=<%=hLvlHierarchy%>&hCompanyID=<%=hCompanyID%>" rel="ajaxcontentarea" title="Summary">Old Summary</a></li>												
    					<% }%>											
						<%						
							HashMap hmapValue = new HashMap();
							alSheetList = (ArrayList)hmReportValue.get("SHEETNAME");
							String strName = "";
							String strAltName = "";
							String strID = "";
							String strDMID = ""; 
							String strDemandSheetIds = ""; 
							int Headersize = alSheetList.size();
							int cnt = 0; 
						 
							for (int i = 0; i < Headersize; i++)
							{ 
								hmapValue = (HashMap) alSheetList.get(i);
								strName = (String)hmapValue.get("NAME");
								strDMID = (String)hmapValue.get("DMID");
								strAltName = new String(strName);
								strName = strName.replaceAll("(?i)"+ttpName, "");  //replace ignore case
							 	if ( Headersize >= 5 )
								{
									strName = strName.length() > 20 ? strName.substring(0,14) : strName;
								}
							 	strName = strName.replaceAll("-", "");
								strID = (String)hmapValue.get("ID");
								strDemandSheetIds += strID + ",";
							  //log.debug("strID : "+ strID);
						%> 
						
					
							<li onclick="fnDisplayTagStyle()"><a href="/gmOPDSSummary.do?demandSheetId=<%=strDMID%>&demandSheetMonthId=<%=strID%>&strOpt=ttpfetch&forecastMonths=<%=forecastMonths%>&yearId=<%=yearId%>&monthId=<%=monthId%>"  
							 	rel="ajaxcontentarea" title="<%=strAltName%>"><%=strName%></a></li>
						<% }%>	
						
						</ul>
						  <script> 						  
						 	 document.frmOPTTPMonthlySummary.demandSheetIds.value='<%=strDemandSheetIds%>';
						  </script>
				<div id="ajaxdivcontentarea" class="contentstyle" style="display:visible; overflow: auto;" >							
<%
				if (strStatus.equals("50581"))
				{
%>				
					<tr>
						<td colspan ="15" align=center height="30"><gmjsp:button name="lock" value="&nbsp;Lock & Generate&nbsp;" gmClass="button" onClick="fnLock();" buttonType="Save" /></td>
					</tr>
<%
				}
%>				</div>
				</td></tr>
				
				<script type="text/javascript">		
				        fnStartProgress('Y');
						var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
					    maintab.setpersist(false);								
					    maintab.init();					    
					    maintab.onajaxpageload=function(pageurl){					    
					     if (pageurl.indexOf("gmOPDSSummary.do")!=-1){					    
					     var demandtab=new ddajaxtabs("demandtab", "ajaxdivtabarea");
						 demandtab.setpersist(true);	
						 demandtab.setselectedClassTarget("link") //"link" or "linkparent"
						 demandtab.init();
						}
						else if(pageurl.indexOf("gmOPTTPMonthlySummary.do")!=-1) {					
						var summarytab=new ddajaxtabs("summarytab", "ajaxdivtabarea");				
					    	summarytab.setpersist(true);		
					    	summarytab.setselectedClassTarget("link") //"link" or "linkparent"						
					    	summarytab.init();			
						}
					     fnStopProgress();
						}
					   	
					</script>			
					<% }%>					
			   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
