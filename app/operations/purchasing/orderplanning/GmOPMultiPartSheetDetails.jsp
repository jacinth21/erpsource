<%
/**********************************************************************************
 * File		 		: GmPartSheetDetails
 * Desc		 		: Sheet Details for a part
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
  <!-- \operations\purchasing\orderplanning\GmOPMultiPartSheetDetails.jsp-->
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<% 
      ArrayList alDrillDown = new ArrayList();
      GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
      String strFormName = GmCommonClass.parseNull((String)request.getAttribute("FORMNAME"));
      strFormName =strFormName.equals("")?"frmOPDrillDown":strFormName;
      
      gmCrossTab.setGeneralHeaderStyle("ShadeDarkGrayTD");
      gmCrossTab.setTotalRequired(false);
      gmCrossTab.setRowHighlightRequired(true);
      gmCrossTab.setSortRequired(false);
      gmCrossTab.setDivHeight(260);
      // To specify if its unit not to round by zero  
      gmCrossTab.setValueType(0);
            
      // Line Style information
      gmCrossTab.setColumnDivider(false);
      gmCrossTab.setColumnLineStyle("borderDark");
      gmCrossTab.setColumnOverRideFlag(true);
      
      gmCrossTab.addStyle("Name","ShadeLightGrayTD","ShadeDarkGrayTD");
      gmCrossTab.addLine("Name");
      
      gmCrossTab.addLine("PAR");
      gmCrossTab.addStyle("PAR","ShadeLightYellowTD","ShadeDarkGrayTD");
      
      gmCrossTab.addLine("STATUS");
      gmCrossTab.addStyle("STATUS","ShadeLightYellowTD","ShadeDarkGrayTD");
      
      
      GmCrossTabFormat gmCrossTabParent = new GmCrossTabFormat();
      strFormName =strFormName.equals("")?"frmOPDrillDown":strFormName;
      
      gmCrossTabParent.setGeneralHeaderStyle("ShadeDarkGrayTD");
      gmCrossTabParent.setTotalRequired(false);
      gmCrossTabParent.setRowHighlightRequired(true);
      gmCrossTabParent.setSortRequired(false);
      gmCrossTabParent.setDivHeight(180);
      // To specify if its unit not to round by zero  
      gmCrossTabParent.setValueType(0);
            
      // Line Style information
      gmCrossTabParent.setColumnDivider(false);
      gmCrossTabParent.setColumnLineStyle("borderDark");
      gmCrossTabParent.setColumnOverRideFlag(true);
      
      gmCrossTabParent.addStyle("Name","ShadeLightGrayTD","ShadeDarkGrayTD");
      gmCrossTabParent.addLine("Name");
      
      gmCrossTabParent.addLine("PAR");
      gmCrossTabParent.addStyle("PAR","ShadeLightYellowTD","ShadeDarkGrayTD");
      
      gmCrossTabParent.addLine("STATUS");
      gmCrossTabParent.addStyle("STATUS","ShadeLightYellowTD","ShadeDarkGrayTD"); 
      
%>
<bean:define id="hmReport" name="<%=strFormName%>" property="hmReport" type="java.util.HashMap"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Multi Part Sheet Details </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmOPTTPDrillDownAction.do"  >
<html:hidden property="demandSheetIds"/>

<% 
HashMap hmDetails = new HashMap();
HashMap hmChildDetails = new HashMap();
HashMap hmParentDetails =new HashMap();
HashMap hmFinal =new HashMap();

hmDetails = GmCommonClass.parseNullHashMap((HashMap)hmReport);
hmChildDetails = (HashMap)hmDetails.get("CHILDDETAILS");
hmParentDetails = (HashMap)hmDetails.get("PARENTDETAILS");

ArrayList alParentDetails = GmCommonClass.parseNullArrayList((ArrayList)hmParentDetails.get("Details"));
ArrayList alParentHeader = GmCommonClass.parseNullArrayList((ArrayList)hmParentDetails.get("Header"));
HashMap hmTotalResult = (HashMap)hmParentDetails.get("MultiFooterTotal");
hmTotalResult.remove("Total");
alParentHeader.remove((String)"Total");

%>
    <table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
            <tr>
               <td height="25" class="RightDashBoardHeader">Demand Sheet List for <i> <bean:write name="<%=strFormName%>" property="partDetails" /> </i> </td>
            </tr>
            <tr>
               <td width="848" height="100" valign="top">
                    <%=gmCrossTab.PrintCrossTabReport(hmChildDetails) %>
               </td>
            </tr>
            <tr><td>&nbsp;&nbsp;</td></tr>     
    </table>      
<%if(alParentDetails.size()>0){ %>
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
            <tr>
               <td height="25" class="RightDashBoardHeader">Demand Sheet List for <i> - Parent Parts(Cummulative) </i> </td>
            </tr>
            <tr>
               <td width="848" height="100" valign="top">
                    <%=gmCrossTabParent.PrintCrossTabReport(hmParentDetails) %>
               </td>
            </tr>
            <tr><td>&nbsp;</td></tr> 
	</table> 
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
     <TR bgcolor="#666666"><TH noWrap colSpan=20 height=1></TH></TR>
     <tr  height =22>
	    <%if(hmTotalResult.size()>0){
	    	 String strAtribName = "";
   			 String strStyleClass ="";
	    	 String strStylealign ="right";
   			 String strStylewidth ="";
	   		 int intParentHeaderSize= alParentHeader.size();
	   		for(int i=0; i <intParentHeaderSize; i++){
		    	 String strTotalValue ="";
	    		 if(i == 0){
	    			 strStyleClass = "ShadeLightGrayTD";
	    			 strStylewidth = "250";
	    			 strStylealign = "left";
	    		 }else if(i == 1){
	    			 strStyleClass = "ShadeLightYellowTD";
	    			 strStylewidth = "60";
	    		 }else{
	    			 strStyleClass = "RightTextSmallCT";
	    			 strStylewidth = "60";	    			
	    		 }
				strTotalValue = GmCommonClass.parseNull((String)hmTotalResult.get((String)alParentHeader.get(i)));
	    		
		        if(i == 1){%>
			    <td class=borderDark noWrap width=1></td>
			    <td width="<%=strStylewidth%>"  class="<%=strStyleClass%>"  style='FONT-WEIGHT: bold' align="<%=strStylealign%>" ><%=  GmCommonClass.getStringWithCommas(strTotalValue,0)%></td>
			    <td class=borderDark noWrap width=1></td>
			    <%}
			    if(!strTotalValue.equals("") && i != 1){%>
	        	<td width="<%=strStylewidth%>"  class="<%=strStyleClass%>"  style='FONT-WEIGHT: bold' align="<%=strStylealign%>" ><%=  GmCommonClass.getStringWithCommas(strTotalValue,0)%></td>
	            <%
			    }
            } 
	    }%>
     </tr>
     <TR bgcolor="#666666"><TH noWrap colSpan=20 height=1></TH></TR>
     <tr><td>&nbsp;</td></tr>
    </table>
<%}%> 
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0"> 
            <tr>
                <td height="25" align="center"><gmjsp:button gmClass="button" value="Close" onClick="window.close()" buttonType="Load" /> </td>
            </tr>
    </table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>