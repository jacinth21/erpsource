 <%
/**********************************************************************************
 * File		 		: GmDSGrowthRequestDetail.jsp
 * Desc		 		: Report for Demand Sheet Request Details
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
  
 !-- \sales\AreaSet\GmAddInfo.jsp --> <%@ include file="/common/GmHeader.inc" %>
 

 
  <%
     	Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);
    try
    {	
	   
  %>
  <html> 

	<head>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	</head>
  	<body>
  	 <TABLE  cellSpacing=0 cellPadding=0  border=0 > 
  	 	<tr><td class=borderDark colspan="3" noWrap width=1></td></tr>
  	 	<tr> <td class=borderDark width=1></td>    
  	 	<td>
  	 	<!--  display:Table tag here --> 
		<display:table cellspacing="0" cellpadding="0" name="requestScope.frmOPDemandSheetSummary.alForeastGrowth"  requestURI="/gmOPDSGrowthRequestDetail.do" class="its" id="currentRowObject" decorator="com.globus.operations.purchasing.orderplanning.displaytag.DTOPRequestGrowthWrapper" >				    																		     							
			<display:column property="REF_ID" group="1" title="ID" class="aligncenter"  />	
			<display:column property="NAME" group="2" title="Name"   class="alignleft"  />		
			<display:column property="REQUIRED_MONTH" title="Month"   class="alignright"  />		
			<display:column property="GROWTH_QTY" title="Growth Qty" class="alignright"  />
			<display:column property="REQUEST_QTY" title="Request qty" class="alignright"  />		
		</display:table>
		
		</td><td class=borderDark width=1></td></tr>
		<tr><td class=borderDark colspan="3" noWrap width=1></td></tr>    
		</table>
		</body>
</html>		
<%	}catch(Exception e)
	{
		e.printStackTrace();
	}
	%>
