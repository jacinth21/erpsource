 <%
/**********************************************************************************
 * File		 		: GmDemandSheetRequestDetail.jsp
 * Desc		 		: Report for Demand Sheet Request Details
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
  <!-- \operations\purchasing\orderplanning\GmOPDemandSheetRequestDetail.jsp -->
   <%@ include file="/common/GmHeader.inc" %>
  <%
     
	String formName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
     formName = formName.equals("") ?  "requestScope.frmOPDemandSheetSummary.alRequest" : "requestScope."+formName+".alRequest" ;	 
     log.debug("formName :"+ formName);
    try
    {		   
    	
  %>
  <html> 

	<head>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	 
	</head>
  	<body>
  	 <TABLE  cellSpacing=0 cellPadding=0  border=0 > 
  	 	<tr><td class=borderDark colspan="3" noWrap width=1></td></tr>
  	 	<tr> <td class=borderDark width=1></td>    
  	 	<td>
  	 	<!--  display:Table tag here --> 
		<display:table cellspacing="0" cellpadding="0" name="<%=formName%>"  requestURI="/gmOPDSRequestDetail.do" class="its" id="currentRowObject" decorator="com.globus.operations.purchasing.orderplanning.displaytag.DTOPRequestDetailWrapper">				    																		     									 																
		<display:column property="REQUEST_ID" title="Request ID" class="alignleft"  />	
		<display:column property="CONSIGNMENT_ID" title="Consignment ID" class="alignleft"  />		
		<display:column property="REQUEST_DATE" title="Request <br> Date" class="aligncenter"  />		
		<display:column property="REQUIRED_DATE" title="Required <br> Date" class="aligncenter"  />			
		<display:column property="REQUEST_TO" title="Request To" class="aligncenter"  />			
		<%if(formName.equals("requestScope.frmOPDemandSheetSummary.alRequest")) {%>
		<display:column property="SHEETNM" title="Sheet Name" class="aligncenter"  />	
		<display:column property="SET_ID" title="ID" class="aligncenter"  />
		<display:column property="LOCK_STATUS" title="Lock Status" class="aligncenter"  />
		<%} 
		else
		{%>
			<display:column property="PNUM" title="Set/Part ID" class="aligncenter"  />
			<display:column property="LOCK_STATUS" title="Lock Status" class="aligncenter"  />
		<%} %>
		<display:column property="CURRENT_STATUS" title="Current Status " class="aligncenter"  />										 
		</display:table>
		
		</td><td class=borderDark width=1></td></tr>
		<tr><td class=borderDark colspan="3" noWrap width=1></td></tr>    
		</table>
		</body>
</html>		
<%	}catch(Exception e)
	{
		e.printStackTrace();
	}
	%>
