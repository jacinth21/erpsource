 <%
/**********************************************************************************
 * File		 		: GmOPGrowthRequestDrillDownDetails.jsp
 * Desc		 		: This screen is used for displayimg the Growth DrillDown Details
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.operations.purchasing.orderplanning.forms.GmOPDemandSheetSummaryForm"%>
<bean:define id="xmlGridData" name="frmOPDemandSheetSummary" property="xmlGridData" type="java.lang.String"> </bean:define>
<bean:define id="setID" name="frmOPDemandSheetSummary" property="setID" type="java.lang.String"> </bean:define>
<%

String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
%>


<!-- \operations\purchasing\orderplanning\GmOPGrowthRequestDrillDownDetail.jsp -->

<HTML>
<HEAD>
<TITLE> Globus Medical: Growth Drilldown Detais</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPDemandSheet.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>
var objGridData;
objGridData = '<%=xmlGridData%>';
var gridObj = '';
function initGridData(divRef,gridData)
{
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.enableDistributedParsing(true);
	gObj.init();	
	gObj.loadXMLString(gridData);	
	return gObj;	
}
function fnOnLoad() {
	if (objGridData != '') {
		gridObj = initGridData('growthdrilldownreport',objGridData);
	}
}
 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();" >
<html:form action="/gmOPDSGrowthRequestDetail.do">
	<table class="DtTable800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;Growth Info Drill Down <%=setID%></td>
		</tr>	
			<%if(xmlGridData.indexOf("cell") != -1){%>
				<tr>
					<td colspan="8">
						<div  id="growthdrilldownreport" style="" height="300px"></div>
					</td>
				</tr>
			<%}else if(!xmlGridData.equals("")){%>
				<tr>
					<td colspan="8" align="center" class="RightText">Nothing found to display</td>
				</tr>
				<tr>
					<td colspan="8" class="LLine" height="1"></td>
				</tr>
			<%}else{%>
				<tr>
					<td colspan="8" align="center" class="RightText">No data available</td>
				</tr>
			<%} %>
		<tr>
			<td align="center" height="30" colspan="2"><gmjsp:button value="&nbsp;Close&nbsp;" gmClass="button" onClick="window.close();" buttonType="Load" /></td>
		</tr>			
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
