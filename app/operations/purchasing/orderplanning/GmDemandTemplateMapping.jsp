
<%
	/**********************************************************************************
	 * File		 		: GmDemandTemplateMapping.jsp
	 * Desc		 		: This jsp will setup the Demand Template Mapping
	 * Version	 		: 1.0
	 * author			: Gopinathan Saravanan
	 ************************************************************************************/
%>
<!-- \operations\purchasing\orderplanning\GmDemandTemplateMapping.jsp --> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel"%>
<bean:define id="submitFlag" name="frmGmDemandTemplate" property="submitFlag" type="java.lang.String"></bean:define>
<bean:define id="strSheetStatusId" name="frmGmDemandTemplate" property="sheetStatusId" type="java.lang.String"></bean:define>
<bean:define id="listGroupAssoc" name="frmGmDemandTemplate"	property="alGroupAssoc" type="java.util.List"></bean:define>
<bean:define id="listConSetAssoc" name="frmGmDemandTemplate" property="alConSetAssoc" type="java.util.List"></bean:define>
<bean:define id="listInhSetAssoc" name="frmGmDemandTemplate" property="alInhSetAssoc" type="java.util.List"></bean:define>
<%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	String strDisable = "";
	if(!submitFlag.equals("Y")|| strSheetStatusId.equals("102642")){ //inactive 
		strDisable = "true";
	}
	
	int intGroupAssoc = listGroupAssoc.size();
	int intConSetAssoc = listConSetAssoc.size();
	int intInhSetAssoc = listInhSetAssoc.size();
%>
<HTML>


<head>
<TITLE>Globus Medical: Demand Sheet Template Setup</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmDemandTemplateSetup.js"></script>
<script type="text/javascript">
var v_group_avail_cnt = 0;
var v_group_assoc_cnt = eval("<%=intGroupAssoc%>");
var v_conset_avail_cnt = 0;
var v_conset_assoc_cnt = eval("<%=intConSetAssoc%>");
var v_inhouse_avail_cnt = 0;
var v_inhouse_assoc_cnt = eval("<%=intInhSetAssoc%>");

</script>
</head>

<BODY leftmargin="20" topmargin="10" onLoad="fnPageLoadTempMapping();">
	<html:form action="/gmDemandTemplate.do">
		<html:hidden property="strOpt" />
		<html:hidden property="demandTempltId" />
		<html:hidden property="inputString" />
		<html:hidden property="submitFlag" />
		<input type="hidden" name="group_avail_cnt" value="0" />	
		<input type="hidden" name="group_assoc_cnt" value="<%=intGroupAssoc%>" />
		<input type="hidden" name="conset_avail_cnt" value="0" />	
		<input type="hidden" name="conset_assoc_cnt" value="<%=intConSetAssoc%>" />
		<input type="hidden" name="inhouse_avail_cnt" value="0" />	
		<input type="hidden" name="inhouse_assoc_cnt" value="<%=intInhSetAssoc%>" />			
		
<!-- Custom tag lib code modified for JBOSS migration changes -->
		<table class="DtTable800" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;Demand
					Sheet Template Mapping</td>
				<td align="right" class=RightDashBoardHeader><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help'
					width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass
						.getWikiTitle("DEMAND_SHEET_TEMPLATE_MAPPING")%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="3" class="LLine"></td>
			</tr>
			<tr>
				<td width="798" height="100" valign="top" colspan="3">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr class="evenShade">
							<gmjsp:label type="BoldText" SFLblControlName="Template Name:"
								td="true" />
							<td colspan="2" width="700">&nbsp; <bean:write
									name="frmGmDemandTemplate" property="demandTempltNm" />
							</td>
						</tr>
						<td colspan="3" class="LLine"></td>
						<tr>
							<gmjsp:label type="BoldText" SFLblControlName="&nbsp;Groups:" td="true" />
							<td width="300">&nbsp;&nbsp;Available
								<DIV
									style="display: visible; height: 180px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
									<table>
										<logic:iterate id="groupAvailList" name="frmGmDemandTemplate"
											property="alGroupAvail">
											<tr>
												<td><htmlel:multibox property="checkGroupAvail"
														value="${groupAvailList.ID}" onclick="fnChkChange(this,'group_avail_cnt');"/><bean:write
														name="groupAvailList" property="NAME" />
												</td>
											</tr>
										</logic:iterate>
									</table>
								</DIV> <BR></td>
							<td width="300">&nbsp;&nbsp;Associated
								<DIV
									style="display: visible; height: 180px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
									<table>
										<logic:iterate id="groupAssocList" name="frmGmDemandTemplate"
											property="alGroupAssoc">
											<tr>
												<td><htmlel:multibox property="checkGroupAssoc"
														value="${groupAssocList.ID}" onclick="fnChkChange(this,'group_assoc_cnt');"/><bean:write
														name="groupAssocList" property="NAME" />
												</td>
											</tr>
										</logic:iterate>
									</table>
								</DIV> <BR></td>
						</tr>
						<tr>
							<td colspan="3" class="LLine"></td>
						</tr>
						<tr>
							<gmjsp:label type="BoldText"
								SFLblControlName="&nbsp;Consignment&nbsp;Sets:" td="true" />
							<td width="300">&nbsp;&nbsp;Available
								<DIV
									style="display: visible; height: 180px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
									<table>
										<logic:iterate id="conSetList" name="frmGmDemandTemplate"
											property="alConSetAvail">
											<tr>
												<td><htmlel:multibox property="checkConSetAvail"
														value="${conSetList.ID}" onclick="fnChkChange(this,'conset_avail_cnt');"/> <bean:write name="conSetList"
														property="ID" />&nbsp;-&nbsp;<bean:write name="conSetList"
														property="NAME" />
												</td>
											</tr>
										</logic:iterate>
									</table>
								</DIV> <BR></td>
							<td width="300">&nbsp;&nbsp;Associated
								<DIV
									style="display: visible; height: 180px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
									<table>
										<logic:iterate id="conSetAssocList" name="frmGmDemandTemplate"
											property="alConSetAssoc">
											<tr>
												<td><htmlel:multibox property="checkConSetAssoc"
														value="${conSetAssocList.ID}" onclick="fnChkChange(this,'conset_assoc_cnt');"/> <bean:write
														name="conSetAssocList" property="ID" />&nbsp;-&nbsp;<bean:write
														name="conSetAssocList" property="NAME" />
												</td>
											</tr>
										</logic:iterate>
									</table>
								</DIV> <BR></td>
						</tr>
						<tr>
							<td colspan="3" class="LLine"></td>
						</tr>
						<tr>
							<gmjsp:label type="BoldText"
								SFLblControlName="&nbsp;InHouse&nbsp;Sets:" td="true" />
							<td width="300">&nbsp;&nbsp;Available
								<DIV
									style="display: visible; height: 180px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
									<table>
										<logic:iterate id="inhSetList" name="frmGmDemandTemplate"
											property="alInhSetAvail">
											<tr>
												<td><htmlel:multibox property="checkInhSetAvail"
														value="${inhSetList.ID}" onclick="fnChkChange(this,'inhouse_avail_cnt');"/> <bean:write name="inhSetList"
														property="ID" />&nbsp;-&nbsp;<bean:write name="inhSetList"
														property="NAME" />
												</td>
											</tr>
										</logic:iterate>
									</table>
								</DIV> <BR></td>
							<td width="300">&nbsp;&nbsp;Associated
								<DIV
									style="display: visible; height: 180px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
									<table>
										<logic:iterate id="inhSetAssocList" name="frmGmDemandTemplate"
											property="alInhSetAssoc">
											<tr>
												<td><htmlel:multibox property="checkInhSetAssoc"
														value="${inhSetAssocList.ID}" onclick="fnChkChange(this,'inhouse_assoc_cnt');"/> <bean:write
														name="inhSetAssocList" property="ID" />&nbsp;-&nbsp;<bean:write
														name="inhSetAssocList" property="NAME" />
												</td>
											</tr>
										</logic:iterate>
									</table>
								</DIV> <BR></td>
						</tr>
						<tr>
							<td colspan="3" class="LLine"></td>
						</tr>						
						<tr>
							<td colspan="3" align="center" height="30">
								<gmjsp:button value="Back" gmClass="button" style="width: 7em"
								controlId="btnMappingBack" name="btnMappingBack"
								onClick="fnLoadDemandTemplate();" buttonType="Load" />&nbsp;
								
								<gmjsp:button controlId="btnMappingSub" name="btnMappingSub"  value="Submit" disabled="<%=strDisable%>" gmClass="button" style="width: 7em"
								onClick="fnSubmitTempMapping();" buttonType="Save" />&nbsp;<span id="loadSumryBtn" style="visibility:hidden;"><gmjsp:button controlId="btnMappingNext" name="btnMappingNext"
								 value="Next" gmClass="button" style="width: 7em"
								onClick="fnLoadTempSummary();"  buttonType="Load" /></span></td>
						</tr>
					</table></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>