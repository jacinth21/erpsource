<%
/**********************************************************************************
 * File		 		: GmGroupPartMapping.jsp
 * Desc		 		: Map the parts to correspoding Groups
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<!-- \operations\purchasing\orderplanning\GmOPGroupPartMapping.jsp -->
<%@ page import="org.apache.commons.beanutils.BeanMap, org.apache.commons.beanutils.RowSetDynaClass, org.apache.commons.beanutils.BasicDynaBean"%>
<%@ page import="java.util.ArrayList,java.util.Iterator" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="strEnableGrpType" name="frmOPGroupPartMapping" property="strEnableGrpType" type="java.lang.String"></bean:define>
<bean:define id="publishfl" name="frmOPGroupPartMapping" property="publishfl" type="java.lang.String"></bean:define> 
<bean:define id="enableGrpPartMapChange" name="frmOPGroupPartMapping" property="enableGrpPartMapChange" type="java.lang.String"></bean:define>

<!-- MNTTASK-6106 - After Publish group, this party only should change below bean define. -->
<bean:define id="strDisablePricedType" name="frmOPGroupPartMapping" property="strDisablePricedType" type="java.lang.String"></bean:define>
<bean:define id="strDisableGroupName" name="frmOPGroupPartMapping" property="strDisableGroupName" type="java.lang.String"></bean:define>

<% 
String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROUP_PART_MAP");
boolean blDisableGroupName = false;
try {

ArrayList alReturn = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("RDGROUPPARTMAP"));

int intParts = alReturn.size();

if(strDisableGroupName.equals("disabled"))
	blDisableGroupName = true;
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Group Part Mapping Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPGroupPartMapping.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="">
<html:form action="/gmOPGroupPartMap.do">

<html:hidden property="strOpt" />
<html:hidden property="haction" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDGRP"/>

<html:hidden property="hinputPrimaryParts" value=""/> 
<html:hidden property="inputString" value=""/>
<html:hidden property="hactiveflag" value=""/> 
<html:hidden property="deptId"/>
 
<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Group Part Mapping Setup</td>
			<td align="right" class=RightDashBoardHeader > 	
			<img id='imgEdit' style='cursor:hand' src='/images/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="30%"></font>&nbsp;Group List:</td>
						<td width="70%">&nbsp;
						<gmjsp:dropdown controlName="groupId" onChange="fnFetch();" SFFormName="frmOPGroupPartMapping" SFSeletedValue="groupId"
								SFValue="alGroupList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />												
						</td>
					</tr>
					<tr>
                        <td class="RightTextBlue" colspan="2" align="center">Choose one from the list above to edit</td> 
                    </tr>
                    <tr><td colspan="2" class="Line"></td></tr>
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24"> &nbsp;<font color="red">*</font>Group Name:</td> 
                        <td>&nbsp;
		                    <html:text property="groupName" disabled="<%=blDisableGroupName%>" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>                                        
                        </td>
                    </tr>  
                     <tr><td colspan="2" class="LLine"></td></tr> 
                     
                      <tr>
                     <td height="24" class="RightTableCaption" align="right"><font color="red">*</font>System :</td>
						<td> &nbsp;&nbsp;<gmjsp:dropdown controlName="setID" SFFormName="frmOPGroupPartMapping" SFSeletedValue="setID"
										SFValue="alSetID" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"/> 		
						</td>	
                     </tr>
                     <tr><td colspan="2" class="LLine"></td></tr> 
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24"> Active Flag:</td> 
                        <td>&nbsp;<html:checkbox property="activeflag" />
                         </td>
                    </tr>
                    
                    <tr><td colspan="2" class="LLine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">Enable Subcomponents:</td> 
                        <td>&nbsp;<html:checkbox property="enableSubComponent" /></td>
                    </tr> 
                 <tr><td colspan="2" class="LLine"></td></tr> 
                     
                   <tr class="Shade">
                     <td height="24" class="RightTableCaption" align="right"><font color="red">*</font>Group Type :</td>
						<td  <%=strEnableGrpType %> > 	&nbsp;&nbsp;<gmjsp:dropdown controlName="groupType" SFFormName="frmOPGroupPartMapping" SFSeletedValue="groupType"
										SFValue="alGroupType" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/> 
							<logic:equal name="frmOPGroupPartMapping" property="strEnableGrpType" value="disabled">	
						<a title="Please contact Rick W or sales analyst to change the group type"><img src=<%=strImagePath%>/question.gif border=0</a>
						</logic:equal>
											
						</td>	
                     </tr>
				   <tr><td colspan="2" class="LLine"></td></tr> 
                     
				    <tr  >
                     <td height="24" class="RightTableCaption" align="right"><font color="red">*</font>Priced Type :</td>
						<td> 	&nbsp;&nbsp;<gmjsp:dropdown controlName="pricedType" SFFormName="frmOPGroupPartMapping" SFSeletedValue="pricedType"
										SFValue="alPricedType" disabled ="<%=strDisablePricedType%>"  codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/> 	
						</td>	
                     </tr> 
			 <tr><td colspan="11" class="ELine"></td></tr>
		  <tr>
					<td colspan="11">
			<div  >
            <iframe src="/gmOPGroupPartMap.do?strPartListFlag=Y&groupId=<bean:write name="frmOPGroupPartMapping" property="groupId"></bean:write>&haction=loadParts&enableSubComponent=<bean:write name="frmOPGroupPartMapping" property="enableSubComponent"></bean:write>&partNumbers=<bean:write name="frmOPGroupPartMapping" property="partNumbers"></bean:write>" 
						        id="PartListFrame" scrolling="no" align="center" marginheight="0"  WIDTH="852" HEIGHT="300"></iframe>

			</div>
			</td></tr>	
    <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
                     	<% String strDisableBtnVal = enableGrpPartMapChange;
							if(strDisableBtnVal.equals("disabled")){
								strDisableBtnVal ="true";
							}
						 %>
                    	<td colspan="2" align="center">
		                    <gmjsp:button disabled="<%=strDisableBtnVal%>"  value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnVoid();" buttonType="Save"/>
		                    <gmjsp:button disabled="<%=strDisableBtnVal%>"  value="Submit" gmClass="button" onClick="fnSubmit();" buttonType="Save" /> 
		                </td>
                    </tr>
 					<tr>
                    	<td class="RightText" colspan="2" align="left">
                    		 Notes:</br>
		             		<font color="red">*</font>If a check box appears in the Primary Column this part is used in more than one project. Check the box to make the part primary for your Group.</br>
		             		<li><img src='/images/uncheck.jpg'/>Primary in a different group.</br></li>
		             		<li><img src='/images/check.jpg'/>Primary part for this group.</br></li>
		             		<font color="red">*</font>To switch which group a part is primary in:
		             		<li>1.Unlock the primary lock checkmark in the original group</li>
   		             		<li>2.Go to the new group - you can now check off the part as primary in this new group</br></li>          		
		                </td>
                    </tr>                    
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
<% 
} catch (Exception exp) {
exp.printStackTrace();
}
%>
</BODY>

</HTML>

