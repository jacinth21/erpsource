<%
/**********************************************************************************
 * File		 		: GmGroupPartDetail.jsp 
 * Desc		 		: display Group part detail
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<!-- \operations\purchasing\orderplanning\GmOPGroupPartDetail.jsp -->
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<bean:define id="groupId" name="frmOPGroupPartMapping" property="groupId" type="java.lang.String"></bean:define>  
<bean:define id="strHPartPriceFl" name="frmOPGroupPartMapping" property="strHPartPriceFl" type="java.lang.String"></bean:define>
<bean:define id="groupNM" name="frmOPGroupPartMapping" property="groupNM" type="java.lang.String"></bean:define>  
 <% 
 String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
ArrayList alReturn = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("RDGROUPPARTMAP"));
int intParts = alReturn.size();
%>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Multiple Part Group </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
 <script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> 
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPGroupPartDetail.js"></script> 


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var groupid = <%=groupId%>; 
 
var rowsize = <%=intParts%>;  

 
 
</script>
</HEAD>

<BODY  onload="fnCheckSelections();">
<html:form> 
<html:hidden property="haction" />

<html:hidden property="groupId" />
<html:hidden property="hpnum" />
<html:hidden property="groupNM" />
<html:hidden property="hviewEdit" />

	<table border="0"  class="DtTable850" cellspacing="0" cellpadding="0">
	
	<logic:equal  name="frmOPGroupPartMapping" property="hviewEdit" value="view">
		<tr>
			<td colspan="4" height="25" class="RightDashBoardHeader">Group Part Mapping
		<!--  	  <bean:write name="frmOPGroupPartMapping" property="groupNM"></bean:write> -->
			</td> 
			
		</tr>
		
	</logic:equal>	
	  <logic:notEqual  name="frmOPGroupPartMapping" property="hviewEdit" value="view">
             <tr class="ShadeRightTableCaption">
						<td Height="24" colspan="3">&nbsp;Group Part Detail</td>
					</tr>
             <tr><td colspan="4" class="Line"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Part Numbers :</td> 
                        <td>&nbsp;
	                         <html:text property="partNumbers"  size="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onkeypress="javascript:fnChangeFocus('submit');" onblur="changeBgColor(this,'#ffffff');"/> 
	                         &nbsp;<a title="You can perform wildcard search. For eg: type 101 to pull up all parts that have 101 in it"><img src=<%=strImagePath%>/question.gif border=0</a>
	                        &nbsp;&nbsp;&nbsp; <gmjsp:button style="width: 5em; height: 2em; font-size: 13px" value="Load" gmClass="button"   onClick="fnLoad();"buttonType="Load" />&nbsp;&nbsp;&nbsp;&nbsp;
								<A class="RightText" title="Click to see all group part history" href="javascript:fnAllPartHistory()">View All Part History</a>	
                        </td>
                    </tr>    </logic:notEqual> 
           <tr><td colspan="4" height="1" class="line"></td></tr>          
            <tr>      
             
            <td colspan="4">
          
            <display:table name="RDGROUPPARTMAP" requestURI="/gmGroupPartMap.do" excludedParams="haction" class="its" id="currentRowObject" freezeHeader="true" paneSize="250" decorator="com.globus.operations.purchasing.orderplanning.displaytag.DTOPGroupPartMappingWrapper" > 
			 <logic:notEqual  name="frmOPGroupPartMapping" property="hviewEdit" value="view">
			<display:column media="html"  title="<input type='checkbox' name='selectAll' onclick='fnSelectAll();' />" style="width:5px" >  
						<htmlel:multibox property="checkPartNumbers"  value="${currentRowObject.PNUM}" > </htmlel:multibox>  
			 </display:column>
			 </logic:notEqual>
			 <logic:equal  name="frmOPGroupPartMapping" property="hviewEdit" value="view">
			 <display:column property="GROUPNM" title="Group Name" class="alignleft" group="1"  >						 
			 </display:column>
			 </logic:equal> 
			 <display:column property="PNUM" title="Part Number" sortable="true" class="alignleft" style="width:100px" /> 
			  <logic:notEqual  name="frmOPGroupPartMapping" property="hviewEdit" value="view">
			 <display:column property="PRICING_TYPE" title=" Primary&nbsp;" class="aligncenter" style="width:5px" >						 
			 </display:column>
			 </logic:notEqual>
			 
			 <logic:notEqual  name="frmOPGroupPartMapping" property="hviewEdit" value="view">
			 <display:column property="PRIMARYLOCK" title=" Primary Lock" class="aligncenter" style="width:5px" >						 
			 </display:column>
			 </logic:notEqual>
			 <!-- PMT-758 - When click P icon in Group Part Pricing screen, should be display List & TW price. -->
			 <display:column property="PDESC" title="Part Description" sortable="true" class="alignleft"/> 
			 <logic:equal  name="frmOPGroupPartMapping" property="strHPartPriceFl" value="Y">
			 <display:column property="LISTP" title="List Price($)" sortable="true" class="alignright" style="width:100px" />
			 <display:column property="TW" title="Trip Wire($)" sortable="true" class="alignright" style="width:100px"/>						 
			 </logic:equal>
			</display:table>  
			  
			</td>
			</tr>
	     	<tr><td class="LLine" height="1" colspan="4"></td></tr>
			<tr>
			    <td align="center" colspan="4" class="RightRedCaption">
           			<gmjsp:button style="width: 8em; height: 2em; font-size: 13px" value="Close" gmClass="button" onClick="fnClose();" buttonType="Load" />&nbsp;
			    </td>			                				                    
		    </tr>
    </table>		     	
 </html:form> 
 <%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>

