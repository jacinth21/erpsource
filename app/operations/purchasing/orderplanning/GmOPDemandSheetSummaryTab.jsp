
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<!-- \operations\purchasing\orderplanning\GmOPDemandSheetSummaryTab.jsp -->
 <%@ include file="/common/GmHeader.inc" %>

<bean:define id="strDemandMasterId" name="frmOPDemandSheetSummary" property="demandSheetId" type="java.lang.String"> </bean:define>
<bean:define id="strDemandSheetId" name="frmOPDemandSheetSummary" property="demandSheetMonthId" type="java.lang.String"> </bean:define>
<bean:define id="strMonthId" name="frmOPDemandSheetSummary" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="strYearId" name="frmOPDemandSheetSummary" property="yearId" type="java.lang.String"> </bean:define>
<bean:define id="strUnitRPrice" name="frmOPDemandSheetSummary" property="unitrprice" type="java.lang.String"> </bean:define>
<bean:define id="strtypeId" name="frmOPDemandSheetSummary"  property="demandTypeId" type="java.lang.String"></bean:define>
<bean:define id="strstatusId" name="frmOPDemandSheetSummary"  property="statusId" type="java.lang.String"></bean:define>
<bean:define id="strDemandTypeId" name="frmOPDemandSheetSummary"  property="demandTypeId" type="java.lang.String"></bean:define>
<bean:define id="strhideSheetCommentsTab" name="frmOPDemandSheetSummary"  property="hideSheetCommentsTab" type="java.lang.String"></bean:define>
<bean:define id="strhideMonthlyCommentsTab" name="frmOPDemandSheetSummary"  property="hideMonthlyCommentsTab" type="java.lang.String"></bean:define>
<bean:define id="strhideGrowthTab" name="frmOPDemandSheetSummary"  property="hideGrowthTab" type="java.lang.String"></bean:define>
<bean:define id="strhideApprovalTab" name="frmOPDemandSheetSummary"  property="hideApprovalTab" type="java.lang.String"></bean:define>
<bean:define id="strstatus" name="frmOPDemandSheetSummary"  property="statusId" type="java.lang.String"></bean:define>

<%
Object bean = pageContext.getAttribute("frmOPDemandSheetSummary", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger
String strGeneralCommentsID = strDemandMasterId+strMonthId+strYearId;

String strLimitedAccess = GmCommonClass.parseNull(request.getParameter("bolLimitedAccess"));
boolean bolLimitedAccess = false;
if(strLimitedAccess!=null && strLimitedAccess.equalsIgnoreCase("true")){
	bolLimitedAccess = true;
}
%>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script>
	function load(){  
		if(document.frmOPDemandSheetSummary.unitrprice !=null){
			if("<%=strUnitRPrice%>" == "Unit"){
				document.frmOPDemandSheetSummary.unitrprice[0].checked="yes";
			}else{
				document.frmOPDemandSheetSummary.unitrprice[1].checked="yes";
			}	
		}      
	}

//Below function's fnPageLoad, parseMessage is copied to GmOPTTPMonthlySummary.js to avoid JS error while
//loading the screen from TTP finalizing screen. If changes done here, need to consider that place also.
	function fnPageLoad(url){	
		dhtmlxAjax.get(url+'&ramdomId='+Math.random(),function(loader){
			if (loader.xmlDoc.responseText != null){
				var datagridXML = loader.xmlDoc.responseText;
				parseMessage(datagridXML);					
			}
			});	
		var divReference=eval(document.getElementById('ajaxdivtabarea'));
		divReference.style.overflow="";
	}
	
	function parseMessage(objGridData){
		gridObj = initGridWithDistributedParsing('ajaxdivtabarea',objGridData);
		document.getElementById("growthRequest").setAttribute("class", "selected");
	}
	
	function setDivStyle(val){
		if(val == 'Y'){
	  		var divReference=eval(document.getElementById('ajaxdivtabarea'));
	 		divReference.style.overflow="auto";
		}
	}
 </script>
 <html:hidden property="demandSheetMonthId" />
 <html:hidden property="demandTypeId" />
 <input type="hidden" name="hDemandMasterID" value="<%=strDemandMasterId%>">
 <input type="hidden" name="hMonthID" value="<%=strMonthId%>">
 <input type="hidden" name="hYearId" value="<%=strYearId%>">

 
						<table>						
						<tr><td>
						<% // load only if any consignment is loaded 
							if (!strstatusId.equals(""))
						{ %> 
						<ul id="demandtab" class="shadetabs">
						<!-- PMT-54128, Added year,month values in url to fetch filter values by month-->
						<li><a href="/gmOPDSSummary.do?demandSheetMonthId=<%=strDemandSheetId%>&demandSheetId=<%=strDemandMasterId%>&strOpt=filterfetch&monthId=<%=strMonthId%>&yearId=<%=strYearId%>&demandTypeId=<%=strDemandTypeId%>" 
						rel="ajaxcontentarea"  class="selected" title="Filter">Filter</a></li>						
						<li><a href="/gmOPDSRegionInfo.do?demandSheetMonthId=<%=strDemandSheetId%>&demandSheetId=<%=strDemandMasterId%>"
							 rel="ajaxcontentarea" title="Summary">Summary</a></li>	
							 <%if(!strDemandTypeId.equals("40023")){ 
							 		   	if (!strhideGrowthTab.equals("YES"))  { %> 					
						<li><a href="/gmOPDSGrowthSummary.do?demandSheetMonthId=<%=strDemandSheetId%>&hTypeId=<%=strtypeId%>"
							 rel="ajaxcontentarea" id="growthinfo" title="Growth Info">Growth Info</a></li>
							 <%} %>  
						<li><a href="/gmOPDSPartHistory.do?demandSheetMonthId=<%=strDemandSheetId%>"
							 rel="ajaxcontentarea" title="Override Summary">Override Summary</a></li>	
						 <%if(!strDemandTypeId.equals("40020")){   %> 	<!-- hide open R, New R and Growth vs Request -->
						<li><a href="/gmOPDSRequestDetail.do?demandSheetMonthId=<%=strDemandSheetId%>&demandTypeId=50632"
							 rel="ajaxcontentarea" title="Open Request" >Open R </a></li>								
						<li><a href="/gmOPDSRequestDetail.do?demandSheetMonthId=<%=strDemandSheetId%>&demandTypeId=50631"
							 rel="ajaxcontentarea" title="New Request" >New R </a></li>	
						<li><a href="/gmOPDSGrowthRequestDetail.do?strOpt=growthvsrequest&demandSheetMonthId=<%=strDemandSheetId%>"
							 rel="ajaxcontentarea" title="Growth vs Request" >Growth vs Request</a></li>	 
							 <%} 
							 }%>  					
						<%	// If approved then hide general comment section 
							strstatusId = !strstatusId.equals("50550")?"Y":"";   
							if (!strhideSheetCommentsTab.equals("YES"))
							{
							%>							 
						<li><a href="/operations/purchasing/orderplanning/GmOPDemandSheetGComment.jsp?demandSheetMonthId=<%=strDemandSheetId%>&hID=<%=strDemandMasterId%>&hStatus=<%=strstatusId%>"
							 rel="ajaxcontentarea" title="Sheet Comments">Sheet Comments</a></li>
							 <%  }  	if (!strhideMonthlyCommentsTab.equals("YES"))  { %>
						<li><a href="/operations/purchasing/orderplanning/GmOPDemandSheetGComment.jsp?demandSheetMonthId=<%=strDemandSheetId%>&hID=<%=strGeneralCommentsID%>&hStatus=<%=strstatusId%>"
							 rel="ajaxcontentarea" title="Monthly Comments">Monthly Comments</a></li>
						<%  }
						if(strDemandTypeId.equals("40023")){ %>
						 <li><a href="/gmOPOprOpenSheets.do?demandSheetMonthId=<%=strDemandSheetId%>&strOpt=opensheets&statusId=<%=strstatus%>"
							 rel="ajaxcontentarea" title="Multipart Open Sheets">Multipart Open Sheets</a></li>
							<%} if(!bolLimitedAccess){
							 	if (!strhideApprovalTab.equals("YES"))  { %> 					 							 	 
						<li><a href="/gmOPDSSummary.do?demandSheetMonthId=<%=strDemandSheetId%>&strOpt=approvefetch"
							 rel="ajaxcontentarea" title="Approval">Approval</a></li>
						 		<%} 
							}
						 %>
						</ul>
						
						<div id="ajaxdivtabarea" class="contentstyle" style="display:visible;height: 130px; overflow: auto;">												
						</div>
						<%} //end of ajax condition
						%>						
					</td></tr> 
					
					
					<!-- step 1. add div tag start and end, call it ajaxreportarea   -->
				<tr><td colspan ="7" align=center> 
				<div id="ajaxreportarea" class="contentstyle" style="display:visible;height: 130px; ">
				<table>
				<!-- 	<tr><td colspan="7" class="Line" width="1"></td></tr>   --> 
					<tr> <td>
						<!-- <td colspan ="7" align=center>  --> 
							<!--  step 2 : remove include jsp from here   --> 
							<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
							<jsp:include page= "/operations/purchasing/orderplanning/GmOPIncludeSheetSummary.jsp" />
							<!-- //step 3 - copy ajaxpage method from their sample code to ajaxtabs.js or separate js and include at top  -->
						</td>
					</tr>
				</table>
			    </div></td></tr>			    
				</table>
					

