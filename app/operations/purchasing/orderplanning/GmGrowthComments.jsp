<%
/**********************************************************************************
 * File		 		: GmGrowthComments.jsp
 * Desc		 		: Growth Comments from Demand Sheet Summary Screen
 * Version	 		: 1.0
 * author			: HReddi
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<bean:define id="partNumbers" name="frmOPDemandSheetSummary" property="partNumbers" type="java.lang.String"> </bean:define>
<bean:define id="ldCommentList" name="frmOPDemandSheetSummary" property="ldCommentList" type="java.util.List"></bean:define>

<% 
	String strPartNumber = "";
	strPartNumber = partNumbers;
	ArrayList alReportList = new ArrayList();
	alReportList = GmCommonClass.parseNullArrayList((ArrayList) ldCommentList);
	String strApplDateFmt = GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
	String strRptFmt = "{0,date," + strApplDateFmt + "}";
%>
<!-- \operations\purchasing\orderplanning\GmGrowthComments.jsp-->
<HTML>
<HEAD>
<TITLE> Globus Medical: Growth Comments </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style type="text/css" media="all"> @import url("<%=strCssPath%>/screen.css");</style>
<style>
table.itss {
	width: 100%;
}

table.itss thead tr {
	background-color: #cccccc;
	height: 25px;
}

table.itss tr.even {
	background-color: #ecf6fe;
}

table.itss thead th {
	FONT-SIZE: 11px; 
	text-align: left;
}
</style>
<script>
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmOPDSSummary.do"  >
<html:hidden property="strOpt" />
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Demand Sheet Comments (<%=strPartNumber%>) </td>
		</tr>
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<display:table name="requestScope.frmOPDemandSheetSummary.ldCommentList" class="itss" id="currentRowObject" requestURI="/gmOPDSSummary.do" style="height:35" >
							<display:column property="DMDSHETNM" title="Demand Sheet Name" group="1" sortable="true" style="width:150" />
							<display:column property="DMDMONTH" title="Month" group="2" style="width:100" />							
							<display:column property="CREATEDBY" title="User Name" sortable="true" style="width:120" />
							<display:column property="CREATEDDT" title="Date Entered" sortable="true" style="width:150" format="<%=strRptFmt%>"/>
							<display:column property="COMMENTS" title="Comments" sortable="true" style="width:200" />							
							</display:table>  
						</td>
					</tr>
			   	</table>
  			 </td>
  		  </tr>	
  		  <tr>
  		  	<td class="LLine" height="1" colspan="2"></td>
  		  </tr>
  		  <tr>
  		  	<td align="center" height="30" colspan="2">
					<gmjsp:button value="&nbsp;Close&nbsp;" gmClass="button"
					onClick="window.close();" buttonType="Load" />
				</td>
  		  </tr>
    </table>		     	
</html:form>
</BODY>
 <%@ include file="/common/GmFooter.inc" %> 
</HTML>
