<%
/**********************************************************************************
 * File		 		: GmPartSheetDetails
 * Desc		 		: Sheet Details for a part
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
 <!-- \operations\purchasing\orderplanning\GmOPPartSheetDetails.jsp --> 
<% 
	ArrayList alDrillDown = new ArrayList();
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	String strFormName = GmCommonClass.parseNull((String)request.getAttribute("FORMNAME"));
	strFormName =strFormName.equals("")?"frmOPDrillDown":strFormName;
	
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkGrayTD");
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setSortRequired(false);
	gmCrossTab.setDivHeight(180);
	// To specify if its unit not to round by zero  
	gmCrossTab.setValueType(0);
		
	// Line Style information
	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.setColumnOverRideFlag(true);
	
	gmCrossTab.addStyle("Name","ShadeLightGrayTD","ShadeDarkGrayTD");
	gmCrossTab.addLine("Name");
	
	gmCrossTab.addLine("PAR");
	gmCrossTab.addStyle("PAR","ShadeLightYellowTD","ShadeDarkGrayTD");
	
	gmCrossTab.addLine("STATUS");
	gmCrossTab.addStyle("STATUS","ShadeLightYellowTD","ShadeDarkGrayTD");

	
try { %>
<bean:define id="hmReport" name="<%=strFormName%>" property="hmReport" type="java.util.HashMap"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Sheet Details </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmOPTTPDrillDownAction.do"  >
<html:hidden property="demandSheetIds"/>
 
<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
		
			<td height="25" class="RightDashBoardHeader">Demand Sheet List for <i> <bean:write name="<%=strFormName%>" property="partDetails" /> </i> </td>
		</tr>
		<tr>
			<td width="848" height="90" valign="top">
					 <%=gmCrossTab.PrintCrossTabReport(hmReport) %>
  			   </td>
  		  </tr>	
  		  <tr>
			<td height="25" align="center"><gmjsp:button gmClass="button" value="Close" onClick="window.close()" buttonType="Load" /> </td>
		</tr>
    </table>		
</html:form>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
