<%
/**********************************************************************************
 * File		 		: GmDemandAvgOverride.jsp
 * Desc		 		: Setting up Override Demand Values
 * Version	 		: 1.0
 * author			: Elango
************************************************************************************/
%>
<%@ include file ="/common/GmHeader.inc"%>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- \operations\purchasing\orderplanning\GmDemandAvgOverride.jsp -->

<bean:define id="demandSheetNm" name="frmDemandAvgOverride" property="demandSheetNm" type="java.lang.String"></bean:define>
<bean:define id="strDescription" name="frmDemandAvgOverride" property="strDescription" type="java.lang.String"></bean:define>
<bean:define id="refTypeNm" name="frmDemandAvgOverride" property="refTypeNm" type="java.lang.String"></bean:define>
<bean:define id="systemWeigthedAvg" name="frmDemandAvgOverride" property="systemWeigthedAvg" type="java.lang.String"></bean:define>
<bean:define id="overrideWeigthedAvg" name="frmDemandAvgOverride" property="overrideWeigthedAvg" type="java.lang.String"></bean:define>
<bean:define id="historyFl" name="frmDemandAvgOverride" property="historyFl" type="java.lang.String"></bean:define>
<bean:define id="strAccessFl" name="frmDemandAvgOverride" property="strAccessFl" type="java.lang.String"></bean:define>
<%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
    String strDisable = "";
	String strOverrideDisable = "";
	
	//when overrideWeigthedAvg is NULL, the remove override button should be disabled.
	strOverrideDisable = overrideWeigthedAvg.equals("") ? "true":"";
	
	//Based on access override, remove override button should be enabled/disabled.
	if(!strAccessFl.equals("Y")){
		strDisable = "true";
		strOverrideDisable = "true";
	}	
	
	if(systemWeigthedAvg.equals("-")){
		systemWeigthedAvg="";
	}
	String strJNDIConnection = GmCommonClass.parseNull((String) GmCommonClass.getString("JNDI_CONNECTION"));
%>
<HTML>


<head>
<TITLE> Globus Medical: Override System </TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmDemandAvgOverride.js"></script>

<script>
var JNDI_CONNECTION= '<%=strJNDIConnection%>';

</script>
</head>

<BODY leftmargin="20" topmargin="10">
<html:form  action="/gmDemandAvgOverride.do&strOpt=load">
<html:hidden property="strOpt"/>
<html:hidden property="demandSheetId"/>
<html:hidden property="demandMasterId"/>
<html:hidden property="demandOverrideId"/>
<html:hidden property="systemWeigthedAvg"/>
<html:hidden property="refID"/>
<html:hidden property="refType"/>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<table class="DtTable700" border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader">&nbsp;Override System Weighted Average</td>			
		</tr>
		<tr  class="evenShade"><td colspan="2" class="LLine"></td></tr>
		<tr>
			<gmjsp:label type="BoldText" width="200" align="right" SFLblControlName="&nbsp;Sheet Name:" td="true" />
			<td align="left" >&nbsp;<%=demandSheetNm%></td> 
		</tr>
		<tr><td colspan="2" class="LLine"></td></tr>
		<tr class="oddShade">
			<gmjsp:label type="BoldText" width="200" align="right" SFLblControlName="&nbsp;Description:" td="true" />
			<td align="left" >&nbsp;<%=strDescription%></td> 
		</tr>
		<tr><td colspan="2" class="LLine"></td></tr>
		<tr  class="evenShade">
			<gmjsp:label type="BoldText" width="200" align="right" SFLblControlName="&nbsp;Type:" td="true" />
			<td align="left" >&nbsp;<%=refTypeNm%></td> 
		</tr>
		<tr><td colspan="2" class="LLine"></td></tr>
		<tr class="oddShade">
			<gmjsp:label type="BoldText" width="200" align="right" SFLblControlName="&nbsp;System Weighted Avg:" td="true" />
			<td align="left" >&nbsp;<%=systemWeigthedAvg%></td> 
		</tr>
		<tr><td colspan="2" class="LLine"></td></tr>
		<tr class="evenShade" height="25">
			<gmjsp:label type="BoldText" width="200" align="right" SFLblControlName="&nbsp;Overridden Weighted Avg:" td="true" />
			<td align="left" width="500" >
			<table><tr><td>&nbsp;<%=overrideWeigthedAvg%>&nbsp;
			<%if(historyFl.equals("Y")){%>
				&nbsp;</td><td><img style='cursor:hand' src='<%=strImagePath%>/icon_History.gif' border='0' title='Click to see History details.' onClick="fnOverrideHistory();" />				
			<%}%></td></tr></table>
			</td> 
		</tr>
		<tr><td colspan="2" class="LLine"></td></tr>
		<tr class="oddShade">
			<gmjsp:label  width="200" align="right"   type="MandatoryText" SFLblControlName="&nbsp;Override Weighted Avg:"  td="true" />
			<td align="left" width="500" >&nbsp;<input type="text" size="5"  name="txtWeigthedAvg" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" >
			</td> 
		</tr>
		<tr><td colspan="2" class="LLine"></td></tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="2">
	            <jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="FORMNAME" value="frmDemandAvgOverride" />
				<jsp:param name="ALNAME" value="alLogReasons" />
				<jsp:param name="LogMode" value="Edit" />
				</jsp:include>	
			</td>
		</tr>
		<tr><td colspan="2" class="ELine"></td></tr> 
        <tr>
            <td colspan="2" align="center" height="30">
		        <gmjsp:button controlId="btnOverride" name="btnOverride" value="Override and Reload Sheet" disabled="<%=strDisable%>" gmClass="button" style="width: 14em" onClick="fnOverrideAvg();" buttonType="Save" />&nbsp;
		        <gmjsp:button controlId="btnRemOverride" name="btnRemOverride" value="Remove Override and Reload Sheet" disabled="<%=strOverrideDisable%>" gmClass="button" style="width: 18em" onClick="fnRemoveOverride();" buttonType="Save" />&nbsp;
		        <gmjsp:button controlId="btnClose" name="btnClose" value="Close" gmClass="button" style="width: 4em" onClick="fnClose();" buttonType="Load" />&nbsp;</td>
		</tr>
</table>

</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>