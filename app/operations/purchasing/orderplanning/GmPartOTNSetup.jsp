<%
/**********************************************************************************
 * File		 		: GmPartOTNSetup.jsp
 * Desc		 		: demand sheet otn setup
 * Version	 		: 1.0
 * author			: Arockia Prasath
************************************************************************************/
%>

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
  <!-- \operations\purchasing\orderplanning\GmPartOTNSetup.jsp--> 
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>
<bean:define id="accessFl" name="frmPartOTNSetup" property="accessFl" type="java.lang.String"></bean:define>
<bean:define id="alOTNList" name="frmPartOTNSetup" property="alOTNList" type="java.util.List"></bean:define>


<%  
String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_OTN_SETUP");
String strJNDIConnection = GmCommonClass.parseNull(GmCommonClass.getString("JNDI_CONNECTION")); 
int intRows = 0;
intRows = alOTNList.size();

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet OTN Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmPartOTNSetup.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
var varRows = <%=intRows%>;
var accessfl = '<%=accessFl%>';
var JNDI_CONNECTION = '<%=strJNDIConnection%>';
</script>
<style>
table.itss {
	width: 100%;
}

table.itss thead th {
	FONT-SIZE: 11px; 
	text-align: left;
}

table.itss thead tr {
	background-color: #cccccc;
	height: 25px;
}

table.itss tr.even {
	background-color: #ecf6fe;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeypress="fnEnter();">
<html:form action="/gmPartOTNSetupAction.do">
<html:hidden property="strOpt" />
<html:hidden property="hinputStr" />

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Demand Sheet OTN Setup</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td colspan="4" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="4">
			
			<!-- Custom tag lib code modified for JBOSS migration changes -->
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="Shade" height="30">
						<td class="RightTableCaption" nowrap align="right" HEIGHT="25">Project:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="templateId" SFFormName="frmPartOTNSetup" SFSeletedValue="templateId" tabIndex="1"
								SFValue="alTemplates" onChange="javascript:fnTemplateFilterSheet(); " codeId="DMID" codeName="DMNM" defaultValue="[Choose One]"/>
						</td>
						<td class="RightTableCaption" nowrap align="right" HEIGHT="25">Sheet Type:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="sheetTypeId" SFFormName="frmPartOTNSetup" SFSeletedValue="sheetTypeId" tabIndex="2"
								SFValue="alSheetTypes" onChange="javascript:fnTypeFilterSheet();" width="160" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
						</td>
					</tr>
					<tr><td colspan="5" class="LLine"></td></tr>
					<tr height="30">
						<td class="RightTableCaption" nowrap align="right" HEIGHT="25">Country:</td>
						<td colspan="3">&nbsp;<gmjsp:dropdown controlName="regionId" SFFormName="frmPartOTNSetup" SFSeletedValue="regionId" tabIndex="3"
								SFValue="alRegions" onChange="javascript:fnRegionFilterSheet(); " width="200" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
						</td>
					</tr>
					<tr><td colspan="4" class="LLine"></td></tr>
					<tr class="Shade" HEIGHT="30">
						<td class="RightTableCaption" align="right" HEIGHT="30" ><font color="red">*</font>&nbsp;Demand Sheet Name:</td>
						<td width="81%" colspan="3">&nbsp;<gmjsp:dropdown controlName="demandMasterId" SFFormName="frmPartOTNSetup" SFSeletedValue="demandMasterId"
							SFValue="alDemandSheetName" codeId = "DMID" codeName = "DMNM"  defaultValue= "[Choose One]" onChange="javascript:fnReload(); "/>													
						</td>
					</tr>
                    <tr><td colspan="4" class="LLine"></td></tr>
                    <tr HEIGHT="30">
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Part Numbers:</td> 
                        <td colspan="3">&nbsp;<html:text property="partNumbers"  size="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 
	                         &nbsp;<a title="You can perform wildcard search. For eg: type 101 to pull up all parts that has 101 in it"><img src="<%=strImagePath%>/question.gif" border="0"/></a>
	                        &nbsp;&nbsp;&nbsp; <gmjsp:button name="Load" value="Load" gmClass="button" style="width: 4em;height: 21" onClick="fnLoad();"buttonType="Load" />
                        </td> 
                    </tr>       
                    <tr><td colspan="4" class="LLine"></td></tr>
		           	<tr>
		           		<td colspan="4" align="center">
			            <display:table name="requestScope.frmPartOTNSetup.alOTNList" requestURI="/gmPartOTNSetupAction.do" excludedParams="hinputStr" class="itss" id="currentRowObject" export="true" decorator="com.globus.operations.purchasing.orderplanning.displaytag.DTPartOTNWrapper"> 
							<display:column property="PNUM" title="Part Number" sortable="true" class="alignleft"/> 
							<display:column property="PDESC" escapeXml="true"  title="Part Description" sortable="true" class="alignleft" /> 
							<display:column property="OTNVALUE" title="OTN Value"  class="alignleft" style="text-align:left;width:90;"/> 
							<display:column property="LASTUPDATEDBY" title="Updated By"  class="alignleft" sortable="true"/>
							<display:column property="LASTUPDATEDDATE" title="Updated Date"  class="aligncenter" sortable="true"/>
							<display:column property="COMMENTS" title="Comments"  class="aligncenter" style="text-align:center;" media="html"/> 
						</display:table>  
						</td>
		    	  	</tr> 
			    	<%if(intRows>0) {%>
			    	<tr><td colspan="4" class="LLine"></td></tr>
			    	<tr></tr>
			        <tr>
			           	<td colspan="4" align="center">
					         <gmjsp:button name="Btn_Submit" value="Submit" gmClass="button" disabled="true" onClick="fnSubmit();" />  
					    </td>
			        </tr>
			        <tr></tr>
			        <%} %>
   				</table>
  			</td>
  		 </tr>	
    </table>		     	
</html:form>

</BODY>
 <%@ include file="/common/GmFooter.inc" %> 
</HTML>

