<%
/**********************************************************************************
 * File		 		: GmOPDemandSheetSetup.jsp
 * Desc		 		: Setting up Demand Sheet
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

 <!-- \operations\purchasing\orderplanning\GmOPDemandSheetSetup.jsp-->

<bean:define id="strDemandSheetType" name="frmOPDemandSheetSetup"  property="demandSheetType" type="java.lang.String"></bean:define>
<bean:define id="strEditAccess" name="frmOPDemandSheetSetup"  property="editAccess" type="java.lang.String"></bean:define>
<bean:define id="strTempltInactiveFl" name="frmOPDemandSheetSetup"  property="templtInactiveFl" type="java.lang.String"></bean:define>
<bean:define id="strDemandSheetId" name="frmOPDemandSheetSetup"  property="demandSheetId" type="java.lang.String"></bean:define>
<bean:define id="strDemandSheetName" name="frmOPDemandSheetSetup"  property="demandSheetName" type="java.lang.String"></bean:define>

<% 
String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_SETUP");
String strDisable = "";
String strSubmitDisable = "";
if(strDemandSheetType.equals("40023")){
	strDisable = "true";
}
if(strTempltInactiveFl.equals("Y")||!strEditAccess.equals("Y")){
	strSubmitDisable = "true";
}

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPDemandSheetSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>


</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmOPDemandSheetSetup.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="demandSheetType" />
<html:hidden property="demandSheetTypeName" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hEnableEdit" value="Y"/>
<html:hidden property="hCancelType" value="VDDMS"/>
<html:hidden property="forecastPeriod" />
<html:hidden property="demandPeriod" />
<html:hidden property="demandSheetName" /> 
<input type="hidden" name="hActiveFl" value="<bean:write name='frmOPDemandSheetSetup' property='activeflag'/>" />
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Demand Sheet Setup</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td width="698" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="Shade">
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"></font>&nbsp;Demand
							Sheet Name :</td>
						<td width="70%" style="padding-left: 2px;">
						<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="demandSheetId" />
					<jsp:param name="METHOD_LOAD" value="loadDemandSheetList&GOPLOAD=Y&searchType=PrefixSearch" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="1"/>
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strDemandSheetName %>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strDemandSheetId %>" />
					<jsp:param name="SHOW_DATA" value="150" />
					<jsp:param name="AUTO_RELOAD" value="fnFetch ();" />
					
				</jsp:include>				
						
						
																			
						</td>
					</tr>
					<tr>
                        <td class="RightTextBlue" colspan="2" align="center"> Select from list to edit or enter details for a new DemandSheet </td> 
                    </tr>
                    <tr><td colspan="2" class="Line"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;Demand Sheet Name:</td> 
	                     <td>&nbsp;<bean:write name="frmOPDemandSheetSetup" property="demandSheetName" />
    		             </td>
                    </tr>  
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;Demand Sheet Type:</td> 
                        <td>&nbsp;<bean:write name="frmOPDemandSheetSetup" property="demandSheetTypeName" />
                        </td>
                    </tr>  
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Demand Period:</td> 
                        <td>&nbsp;<bean:write name="frmOPDemandSheetSetup" property="demandPeriod"/>&nbsp;(months)
    		                                                                                
    		             </td>
                    </tr>   
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Forecast Period:</td> 
                        <td>&nbsp;<bean:write name="frmOPDemandSheetSetup" property="forecastPeriod"/>&nbsp;(months)
    		             </td>
                    </tr>   
                    
                     <tr><td colspan="2" class="lline"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;Set Build Forecast:</td> 
                        <td>&nbsp;<bean:write name="frmOPDemandSheetSetup" property="requestPeriod"/>&nbsp;(months)
                        </select>
                        </td>
                    </tr>  
                    <tr><td colspan="2" class="lline"></td></tr>
                     <tr class="Shade">
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%">&nbsp;Sheet Owner:</td>
						<logic:equal name="frmOPDemandSheetSetup" property="accessTypeId" value="102622">
							<td width="70%">&nbsp;<gmjsp:dropdown controlName="demandSheetOwner" SFFormName="frmOPDemandSheetSetup" SFSeletedValue="demandSheetOwner"
							SFValue="alDSOwner" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]" disabled="disabled" />
							</td>
						</logic:equal>
							<logic:notEqual name="frmOPDemandSheetSetup" property="accessTypeId" value="102622">
							<td width="70%">&nbsp;<gmjsp:dropdown controlName="demandSheetOwner" SFFormName="frmOPDemandSheetSetup" SFSeletedValue="demandSheetOwner" 
							SFValue="alDSOwner" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]" />
							</td>
						</logic:notEqual>
					</tr>
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Level:</td> 
                        <td>&nbsp;<bean:write name="frmOPDemandSheetSetup" property="levelId"/>
    		             </td>
                    </tr>   
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Value:</td> 
                        <td>&nbsp;<bean:write name="frmOPDemandSheetSetup" property="levelValue"/>
    		             </td>
                    </tr>
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Access Type:</td> 
                        <td>&nbsp;<bean:write name="frmOPDemandSheetSetup" property="accessType"/>
    		             </td>
                    </tr>                          
                    <tr><td colspan="2" class="lline"></td></tr>
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Demand Sheet Mapping:</td>                          
                        <td>&nbsp;<gmjsp:button value="Mapping" gmClass="button" disabled="<%=strDisable%>" onClick="fnMapping();" buttonType="Save" />&nbsp;
    		                      <gmjsp:button value="Launch Setup" gmClass="button" onClick="fnOpenLaunch();" buttonType="Save"/>    		              
    		            </td>	             
                    </tr>   
                    <tr><td colspan="2" class="lline"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">InActive Flag:</td> 
                        <td>&nbsp;<html:checkbox property="activeflag" />
                         </td>
                    </tr>
                     <tr>
						<td colspan="2" align="center" height="24">&nbsp;
	                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmOPDemandSheetSetup" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>	
						</td>
						</tr>
					 <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
                    	<td colspan="2" align="center" height="24">
		                    <gmjsp:button value="Submit" disabled="<%=strSubmitDisable%>" gmClass="button" onClick="fnSubmit();" buttonType="Save" /> 
		                </td>
                    </tr>
                    <tr></tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>

<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

