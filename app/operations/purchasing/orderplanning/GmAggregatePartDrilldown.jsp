    <%@ include file="/common/GmHeader.inc" %>
   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
   <%@ page import ="com.globus.common.servlets.GmServlet"%>
   
   <%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
   <!-- WEB-INF path corrected for JBOSS migration changes -->    
   <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
   <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css"> 
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>  
	<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
 
 
    <bean:define id="hmReportValue" name="frmOPDemandSheetSummary"  property="hmAggrPartDetail" type="java.util.HashMap"></bean:define>
    <bean:define id="demandsheetid" name="frmOPDemandSheetSummary" property="demandSheetId" type="java.lang.String"></bean:define>
    <bean:define id="demandmonthid" name="frmOPDemandSheetSummary" property="demandSheetMonthId" type="java.lang.String"></bean:define>
    <bean:define id="partNumber" name="frmOPDemandSheetSummary" property="partNumbers" type="java.lang.String"> </bean:define>
    <bean:define id="demandTypeId" name="frmOPDemandSheetSummary" property="demandTypeId" type="java.lang.String"> </bean:define> 
    <bean:define id="strUnitRPrice" name="frmOPDemandSheetSummary" property="unitrprice" type="java.lang.String"> </bean:define>
    <bean:define id="groupID" name="frmOPDemandSheetSummary" property="groupInfo" type="java.lang.String"> </bean:define>
     <!-- \operations\purchasing\orderplanning\GmAggregatePartDrilldown.jsp --> 
    <input type="hidden" name="hExport" /> 
     
    <%
    GmServlet gm = new GmServlet();
	
	log.debug("demandmonthid: "+demandmonthid+"demandsheetid: "+ demandsheetid);
	     
		GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
		ArrayList alForecastHeader = new ArrayList();
		ArrayList alHeader = new ArrayList();
		ArrayList alDrillDown = new ArrayList();		
		ArrayList alDtls = new ArrayList();
		HashMap hmapValue = new HashMap();
		String strFooterStyle = "";  	
		String strStatus = "";
		String strForecastCol = "";
		String strPdesc = "";
		int r = 0;			

		if(strUnitRPrice.equals("Price"))
		{
			gmCrossTab.setValueType(1);
			gmCrossTab.setNoDivRequired(true);
		}
		else
		{
			// To specify if its unit not to round by zero  
			gmCrossTab.setValueType(0);	
		}

		if (hmReportValue.size() == 0) 
		{
			return;
		}
		
		//Below code is to display the forecast header section sync with summary page.
		alHeader = GmCommonClass.parseNullArrayList((ArrayList) hmReportValue.get("Header"));
		if(alHeader.size() >0){
			int intForecastHdrIndex = alHeader.indexOf("Variance %");
			int intHeadersize = alHeader.size();
			for (int i = intForecastHdrIndex; i < intHeadersize-2; i++)
	        {
				strForecastCol = GmCommonClass.parseNull((String)alHeader.get(i+1));
				if((r = i%2) == 0){
	            	strFooterStyle = "ShadeMedBrownTD";
	            }else {
	            	strFooterStyle = "ShadeLightBrownTD";
	            }
				 gmCrossTab.addStyle(strForecastCol,strFooterStyle,"ShadeDarkBrownTD") ;	
	        }
		}
		
		alDtls = GmCommonClass.parseNullArrayList((ArrayList) hmReportValue.get("Details"));
		if(alDtls.size() >0){
			HashMap hmDtls =  (HashMap)(alDtls.get(0));
			strPdesc = " - " +GmCommonClass.parseNull((String)hmDtls.get("PDESC"));
		} 
		// Line Style information
		gmCrossTab.setColumnDivider(false);
		gmCrossTab.setColumnLineStyle("borderDark");
		gmCrossTab.setRowHighlightRequired(true);
		 
        
		gmCrossTab.addStyle("Name","ShadeLightBlueTD","ShadeDarkBlueTD");
		gmCrossTab.addStyle("Demand Weighted Avg","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("Demand Avg","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("Forecast Avg","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("Overridden Weighted Avg","ShadeLightOrangeTD","ShadeDarkOrangeTD");
		//gmCrossTab.addStyle("Variance %","ShadeLightOrangeTD","ShadeDarkOrangeTD") ; 
		gmCrossTab.addStyle("PAR","ShadeLightYellowTD","ShadeDarkYellowTD") ;
		gmCrossTab.addStyle("PBO","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("PBL","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("PCS","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("Set PAR","ShadeMedGreenTD","ShadeDarkGreenTD") ;
	 	gmCrossTab.addStyle("OTN","ShadeLightGreenTD","ShadeDarkGreenTD") ;  
		 
		if(!demandTypeId.equals("40023"))
		{
			gmCrossTab.setColumnOverRideFlag(true);
		}
		gmCrossTab.addLine("Name");
		gmCrossTab.addLine("PAR");
		gmCrossTab.addLine("PCS");  
		gmCrossTab.addLine("OTN");
		 
		//gmCrossTab.addLine("Variance %"); 
		gmCrossTab.addLine("Overridden Weighted Avg") ;
		gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
		gmCrossTab.setTotalRequired(false);
		gmCrossTab.setColumnTotalRequired(false);
		gmCrossTab.setGroupingRequired(true);
		gmCrossTab.setRowHighlightRequired(true);
		gmCrossTab.setLinkRequired(false);		
        gmCrossTab.setDecorator("com.globus.operations.purchasing.orderplanning.displaytag.GmDemandDrilldownDecorator");
		 
		gmCrossTab.setAttribute("DMID",demandsheetid);
		gmCrossTab.setAttribute("DSID",demandmonthid);
		gmCrossTab.setAttribute("DEMANDTYPE",demandTypeId); 
		 
		%>
		<table border="1" class="DtTable1000" cellspacing="0" cellpadding="0">
		 		
		<tr>
			<td height="25" class="RightDashBoardHeader"> DrillDown <% if(alDtls.size() >0){ %> -  
				 <logic:equal name="frmOPDemandSheetSummary" property="partNumbers" value=""> <%=groupID  %>   <%=strPdesc  %>
				 </logic:equal>
				 <logic:notEqual name="frmOPDemandSheetSummary" property="partNumbers" value=""> <%=partNumber  %>   <%=strPdesc  %>
				 </logic:notEqual>
				 <% } %>
			 </td>
		 
		</tr>
		<tr>
			<td > 
		 <%	 
			out.println(gmCrossTab.PrintCrossTabReport(hmReportValue)) ;  %>
		 </td>
			</tr>
			<tr>
			<td align="center" height="30"  ><gmjsp:button value="&nbsp;Close&nbsp;" gmClass="button" onClick="window.close();" buttonType="Load" /></td>
		</tr>	
	    <table>	
	   <%@ include file="/common/GmFooter.inc" %>
	   