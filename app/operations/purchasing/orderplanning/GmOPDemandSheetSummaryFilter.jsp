<%@ include file="/common/GmHeader.inc"%>
 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
 <!-- \operations\purchasing\orderplanning\GmOPDemandSheetSummaryFilter.jsp -->

<% 
Object bean = pageContext.getAttribute("frmOPDemandSheetSummary", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
String strLimitedAccess= GmCommonClass.parseNull((String)pageContext.getRequest().getAttribute("DMNDSHT_LIMITED_ACC"));
%>


<bean:define id="strDemandMasterId" name="frmOPDemandSheetSummary" property="demandSheetId" type="java.lang.String"> </bean:define>
<bean:define id="strDemandSheetId" name="frmOPDemandSheetSummary" property="demandSheetMonthId" type="java.lang.String"> </bean:define>
<bean:define id="strMonthId" name="frmOPDemandSheetSummary" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="strYearId" name="frmOPDemandSheetSummary" property="yearId" type="java.lang.String"> </bean:define>
<bean:define id="strUnitRPrice" name="frmOPDemandSheetSummary" property="unitrprice" type="java.lang.String"> </bean:define>
<bean:define id="strtypeId" name="frmOPDemandSheetSummary"  property="demandTypeId" type="java.lang.String"></bean:define>
<bean:define id="strstatusId" name="frmOPDemandSheetSummary"  property="statusId" type="java.lang.String"></bean:define> 
<bean:define id="alGroupList" name="frmOPDemandSheetSummary"  property="alGroupList" type="java.util.ArrayList"></bean:define>
<Script>
	var setGrupSize = <%=alGroupList.size()%> ;
</script>
<table class="border" width="500" cellspacing="1" cellpadding="0">
						<tr >
						<% 
							if (strtypeId.equals("40020"))
							{ 
						%> 
		                    <td class="RightBlueCaption" height=20>&nbsp;Groups List:</td>
						<% 
							}else{ 
						%> 
		                    <td class="RightBlueCaption" height=20>&nbsp;Sets List:</td>
						<% 
							}
						%>
		                    <td rowspan="6" class="Line" width="1"></td>
		                    <td rowspan="6"  width="3"></td>
		                    <td rowspan="6" class="Line" width="1"></td>
							<td class="RightTableCaption">&nbsp;<html:radio property="unitrprice" value="Unit"  /> &nbsp; Unit</td>
							<td class="RightTableCaption">
							<%if(strLimitedAccess!=null && !strLimitedAccess.equalsIgnoreCase("true")){ %>
							<html:radio property="unitrprice" value="Price" /> &nbsp; Cost &nbsp;
							<%} %>
						<% if (!strtypeId.equals("40020") || !strtypeId.equals("40023")) { %> 	
							<html:checkbox property="checkSetDtl" />&nbsp; Set Details&nbsp;
						<% } %>	
							<html:checkbox property="checkDesc" />&nbsp; Description
							</td>
							<% 
								String strFetch = "fnFetch('"+strDemandSheetId+"','"+strDemandMasterId+"','"+strYearId+"','"+strMonthId+"','"+alGroupList.size()+"');";
							%>  
							<td rowspan="6"   width="5%"></td>
							<td rowspan="5" align="left" width="25%">
							&nbsp;<gmjsp:button value="&nbsp;&nbsp;View&nbsp;&nbsp;" gmClass="button" onClick="<%=strFetch%>" buttonType="Save" />
							</td>
	                    </tr>
	                    <tr>
							<td rowspan="6" >
		                        <div style="display:visible;height: 80px; overflow: auto;">
		                        <table  border="0"  cellspacing="0" cellpadding="0">
		                        	<logic:iterate id="groupList" name="frmOPDemandSheetSummary" property="alGroupList">
		                        	<tr><td>
										<htmlel:multibox property="checkedGroupId" value="${groupList.ID}" />
										<bean:write name="groupList" property="NAME" />
			     					</td></tr>	    
									</logic:iterate>
								</table></div>
							</td>
						</tr>
						<tr><td height="10"></td></tr>
						<!--  	<tr>
		                     	<td  colspan="2" valign ="top" class="RightTableCaption">&nbsp;Show Overriden values : 
		                    	&nbsp;&nbsp;<html:checkbox  property="overrideFlag" /> </td>
		                    </tr> -->
		                    <tr>
		                     	<td  colspan="2" valign ="top" class="RightTableCaption">&nbsp;Forecast Months : 
		                    	&nbsp;&nbsp;<html:text property="forecastMonths" size="5" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" /> </td>
		                    </tr>		                    
		                    <tr>	
	    	                	<td class="RightTableCaption">&nbsp;Part #(s) :</td> 
	        	            	<td> <html:text property="partNumbers" name="frmOPDemandSheetSummary" size="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 
	            	             &nbsp;<a title="You can perform wildcard search. For eg: type 101 to pull up all parts that has 101 in it"><img src=<%=strImagePath%>/question.gif border=0</a>
	                	        &nbsp;&nbsp;&nbsp;
	                    	    </td>
							</tr>	
						</table>
						<%@ include file="/common/GmFooter.inc"%>
						