<%@ include file="/common/GmHeader.inc" %>
  
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Iterator" %>

<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

 <!-- \operations\purchasing\orderplanning\GmOPTTPMonthlySummaryFilter.jsp--> 
<% 
Object bean = pageContext.getAttribute("frmOPTTPMonthlySummary", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<bean:define id="levelId" name="frmOPTTPMonthlySummary" property="levelId" type="java.lang.String"> </bean:define>
<bean:define id="hLvlHierarchy" name="frmOPTTPMonthlySummary" property="hLvlHierarchy" type="java.lang.String"> </bean:define>
<bean:define id="alFilters" name="frmOPTTPMonthlySummary" property="alFilters" type="java.util.ArrayList"></bean:define>
<bean:define id="strShowForecastFl" name="frmOPTTPMonthlySummary" property="strShowForecastFl" type="java.lang.String"> </bean:define>
<table width="100%">
   			<tr><td width="5%"></td>
<td >
<table style="border: 1px solid  #676767;">
<!-- <tr><td width="170"><a href="javascript:fnShowFilters('tabFilterDetails');"><IMG id="tabVoidReqDetailsimg" border=0 src="<%=strImagePath%>/minus.gif"></td></tr> -->
<tr><td align="left" >
					    	<div id="filterarea" class="contentstyle" style="display:visible;height:65px;overflow:auto">
					    	<%//GmCommonControls.getChkBoxGroup("fnSelectInv()",alInventory,"Inventory",hmInventory,"")%>
							 <table width="100%">	<tr> <td id="tabFilterDetails">	
							 <table>
							 <% Iterator itr=alFilters.iterator();
							 while (itr.hasNext()){
								 HashMap hmValue=(HashMap)itr.next();
								 String  CODEID = (String)hmValue.get("CODEID");
								 String  CODENM = (String)hmValue.get("CODENM");
								 String CODENMALT = (String)hmValue.get("CODENMALT");
								 %>								 
								<tr> <td align="left">
								<input type="checkbox" name="chk_<%=CODEID%>"> <%=CODENM%>
								</td></tr>								 
							<% } %>	
							</table>												
							   					   					   
				      		</td></tr>		
					  
					  
							</table>
							</div>
	</td></tr></table>						
					   </td>	<td width="3%"></td><td>
   <table width="100%" style="border: 1px solid  #676767;">
			       <!-- Custom tag lib code modified for JBOSS migration changes -->
   				<tr>	
   				<td class="RightTableCaption" align="right" HEIGHT="30" width="3%"></td>
         				<td class="RightTableCaption" align="right" HEIGHT="30" width="15%">&nbsp;Order Qty:</td>
					   <td width="32%">
					    	&nbsp;
							<gmjsp:dropdown controlName="qtyOper" SFFormName="frmOPTTPMonthlySummary" SFSeletedValue="qtyOper"
								SFValue="alOperators" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]" />
							&nbsp;
							 <html:text property="orderQty"  size="5" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>									
					   </td>
					    <td class="RightTableCaption" align="right" HEIGHT="30"  >Part Number :</td>					   
				      <td align="left" width="10%">&nbsp; 
				         <html:text property="pnum"  size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
				      </td>	
				      <td class="RightTableCaption" align="right" HEIGHT="30" width="25%"></td>				      					
					</tr>
					
					<tr>
					<td class="RightTableCaption" align="right" HEIGHT="30" width="3%"></td>
					<td class="RightTableCaption" align="right" HEIGHT="30" width="15%" >&nbsp;Order Cost:</td>
					   <td width="32%">
					    	&nbsp;
							<gmjsp:dropdown controlName="ordOper" SFFormName="frmOPTTPMonthlySummary" SFSeletedValue="ordOper"
								SFValue="alOperators" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						    &nbsp;
						    <html:text property="orderCost"  size="5" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 										
					   </td>						   					   
				       	<!-- Add the new text box - Forecast Month Based on Rule values - to show/hide the text box-->
				       	<logic:equal name="frmOPTTPMonthlySummary" property="strShowForecastFl" value="Y"> 
				       	<td class="RightTableCaption" align="right" HEIGHT="30" width="100">ForeCast Month :</td>					   
				         <td align="left">&nbsp;<html:text property="tempForecastMonths"  size="4" maxlength="2"  onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
				         </td>	
				        </logic:equal>
					    <td class="RightTableCaption" align="right" HEIGHT="30"  > </td>					   
				      <td align="left" width="10%">&nbsp;
				        &nbsp;<gmjsp:button value="Apply Filter" gmClass="button" onClick="fnFilter();" buttonType="Save" />
				      </td>	
				      <td class="RightTableCaption" align="right" HEIGHT="30" width="25%"></td>
					   </tr>
					 										    	
				  </table>		 			  
			</td>	</tr>	 </table> 
			<%@ include file="/common/GmFooter.inc" %>
			