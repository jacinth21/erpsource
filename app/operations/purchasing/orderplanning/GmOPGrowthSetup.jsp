<%
/**********************************************************************************
 * File		 		: GmGrowthSetup.jsp
 * Desc		 		: Setting up Growth
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<%@ page import ="java.util.HashMap"%>
<%@ page import ="java.util.ArrayList"%>
<bean:define id="strRefType" name="frmOPGrowthSetup" property="refType" type="java.lang.String"> </bean:define>
<!-- \operations\purchasing\orderplanning\GmOPGrowthSetup.jsp --> 
<bean:define id="prjStatus" name="frmOPGrowthSetup" property="prjStatus" type="java.lang.String"> </bean:define>
<bean:define id="launchDt" name="frmOPGrowthSetup" property="launchDt" type="java.lang.String"> </bean:define>
<bean:define id="validateMonth" name="frmOPGrowthSetup" property="validateMonth" type="java.lang.Integer"> </bean:define>
<bean:define id="validateYear" name="frmOPGrowthSetup" property="validateYear" type="java.lang.Integer"> </bean:define>
<bean:define id="alRefIds" name="frmOPGrowthSetup" property="alRefIds" type="java.util.ArrayList"> </bean:define>
<bean:define id="refType" name="frmOPGrowthSetup" property="refType" type="java.lang.String"> </bean:define>
<bean:define id="hmGrowthData" name="frmOPGrowthSetup" property="growthDetailsMap" type="java.util.HashMap"> </bean:define>
<bean:define id="strAccessFl" name="frmOPGrowthSetup" property="accessFl" type="java.lang.String" />
<% 
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	String strWikiTitle = GmCommonClass.getWikiTitle("OP_GROWTH_SETUP");
	String	strDisable = strAccessFl.equals("Y")?"":"true";
	String disabledenabled = "";
	String strJNDIConnection = GmCommonClass.parseNull(GmCommonClass.getString("JNDI_CONNECTION"));
	String strTodaysDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate")); 
	GmGridFormat gmGridFormat = new GmGridFormat();
	//HashMap hmGrowthData = (HashMap)request.getAttribute("GROWTH_DATA");
	gmGridFormat.setHideColumn("Total");	
	String strGridData = "";
	String strDivHeight = "500";
	String strDivWidth = "1000";
	int listSize=0;
	boolean showGrid=false;
	if(hmGrowthData!=null && hmGrowthData.size()>0){
		
		if(hmGrowthData.get("Header")!=null && ((ArrayList)hmGrowthData.get("Header")).size()>0){
			showGrid = true;			
		}
		if(hmGrowthData.get("Details")!=null){
			listSize = ((ArrayList)hmGrowthData.get("Details")).size();
		}
		System.out.println("listSize== "+listSize);
		if(listSize >=0 && listSize <=5){
			strDivHeight="150";		
		}else if(listSize >=6 && listSize <=15){
			strDivHeight="200";		
		}else if(listSize >=16 && listSize <=25){
			strDivHeight="225";		
		}else if(listSize >=26 && listSize <=50){
			strDivHeight="250";		
		}else if(listSize >=51 && listSize <=100){
			strDivHeight="275";
		}else if(listSize >=101 && listSize <=125){
			strDivHeight="600";
		}
		
		gmGridFormat.setDecorator("com.globus.crosstab.beans.GmGrowthGridDecorator");
		
		//gmGridFormat.reNameColumn("Name","RAJ");
		gmGridFormat.setColMasterList("Name",alRefIds);
		gmGridFormat.setColMasterKey("Name","REFID","REFNM","false");
		gmGridFormat.setColumnWidth("Name",255);
		gmGridFormat.setColumnWidth("Description",175);
		gmGridFormat.addColumnSortType("Name","str");
		gmGridFormat.addColumnAlign("Name","left");
		gmGridFormat.addColumnSortType("Description","str");
		gmGridFormat.addColumnAlign("Description","left");
		gmGridFormat.addColumnType("Description","ro");
		// If the Ref Type is Group ID on Sales Sheet.
		if(refType!=null && !refType.equals("20297")){
			gmGridFormat.setAddColumn("Description",1); // Adding the column to 2nd postion.
			strDivWidth = "1000";
			gmGridFormat.setColumnWidth("Name",80);
			gmGridFormat.setColMasterKey("Name","REFID","REFNM","true");
		}	
		strGridData = gmGridFormat.generateGridXML(hmGrowthData);
	}
	
	String strNameDropDownValues = gmGridFormat.getDropDownMasterValue("Name");
	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Growth Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     
    table.DtTable1000 {
	width: 1000px;
	}
</style>
<%-- <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css"> --%>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/skins/dhtmlxmenu_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxCalendar/codebase/skins/dhtmlxcalendar_dhx_skyblue.css">
<%-- <link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/skins/dhtmlxmenu_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css"> --%>
<%-- <script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script> --%>
<%-- <script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> --%>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPGrowthSetup.js"></script>
<%-- <script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxWindowPopup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxMenu/dhtmlxmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_undo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script> --%>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxCalendar/codebase/dhtmlxcalendar.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxCalendar/codebase/dhtmlxcalendar_deprecated.js"></script>



<style type="text/css">
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;
}
div.gridbox table.hdr td {
    color: #000000 !important;
    background-color: #d2e9fc !important;
    border: 1px solid white;
    }
div.gridbox_material.gridbox .xhdr {
	background:#dcedfc !important;
}
</style>

<script>
var year = '<%=validateYear%>';
var month = '<%=validateMonth%>';
var prjSt = '<%=prjStatus%>';
var launchdt = '<%=launchDt%>';
var varNameDropDownValues = '<%=strNameDropDownValues%>';
var hFromDate = '<%=request.getAttribute("hidden_FromDate")%>';
var hToDate = '<%=request.getAttribute("hidden_ToDate")%>';
var JNDI_CONNECTION = '<%=strJNDIConnection%>';
var todayDate = '<%=strTodaysDate%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad()">
<html:form action="/gmOPGrowthSetup.do"  >
<html:hidden  property="hAction" value="" />
<html:hidden property="strOpt" value=""/>
<html:hidden property="growthId" />
<html:hidden property="hModifiedRowID" value="" />
<html:hidden property="msg" />
<html:hidden property="inputString" />
<html:hidden property="overrideTypeId" />
<html:hidden property="refIdForLogReason"/>
<input type="hidden" name="hCancelType" value="VDREQT">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hJNDIConnection" value="<%=strJNDIConnection%>">

<!-- Custom tag lib code modified for JBOSS migration changes -->
<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
	<tr>
		<td height="100" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr height="30">
					<td colspan="4" height="25" class="RightDashBoardHeader">&nbsp;Growth Setup</td>
					<td align="right" class=RightDashBoardHeader > 	
						<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
					</td>
				</tr>
				<logic:notEqual name="frmOPGrowthSetup" property="msg" value="">
				<tr><td colspan="5" height="20" align="center" class="RightBlueText"><bean:write name="frmOPGrowthSetup" property="msg"/></td></tr>
				<tr><td class="LLine" colspan="5"></td></tr>
				</logic:notEqual>
				<tr class="Shade" height="30">
					<td class="RightTableCaption" nowrap align="right" HEIGHT="25">Project:</td>
					<td>&nbsp;<gmjsp:dropdown controlName="templateId" SFFormName="frmOPGrowthSetup" SFSeletedValue="templateId" tabIndex="1"
							SFValue="alTemplates" onChange="javascript:fnTemplateFilterSheet(); " codeId="DMID" codeName="DMNM" defaultValue="[Choose One]"/>
					</td>
					<td class="RightTableCaption" nowrap align="right" HEIGHT="25">Sheet Type:</td>
					<td colspan="2">&nbsp;<gmjsp:dropdown controlName="sheetTypeId" SFFormName="frmOPGrowthSetup" SFSeletedValue="sheetTypeId" tabIndex="2"
							SFValue="alSheetTypes" onChange="javascript:fnTypeFilterSheet();" width="160" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
					</td>
				</tr>
				<tr><td colspan="5" class="LLine"></td></tr>
				<tr height="30">
					<td class="RightTableCaption" nowrap align="right" HEIGHT="25">Country:</td>
					<td colspan="4">&nbsp;<gmjsp:dropdown controlName="regionId" SFFormName="frmOPGrowthSetup" SFSeletedValue="regionId" tabIndex="3"
							SFValue="alRegions" onChange="javascript:fnRegionFilterSheet(); " width="200" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
					</td>
				</tr>
				<tr><td colspan="5" class="LLine"></td></tr>
				<tr class="Shade" height="30">
					<td class="RightTableCaption" nowrap align="right" HEIGHT="25"> Demand Sheet Name:</td>
					<td colspan="4">&nbsp;<gmjsp:dropdown controlName="demandSheetId"  SFFormName="frmOPGrowthSetup" SFSeletedValue="demandSheetId" tabIndex="4"
						SFValue="alDemandSheets" onChange="fnReloadReferences()" codeId="DMID" codeName="DMNM" defaultValue="[Choose One]"/>
					</td>
				</tr>
					<%if( !strRefType.equals("20295") && !strRefType.equals("20298")){
								 	log.debug("The value...."+strRefType);
							 		disabledenabled = "true";
					}%>
				<tr><td colspan="5" class="LLine"></td></tr>
				<tr>
					<td align="right" HEIGHT="30" class="RightTableCaption"><font color="red">*</font> Ref Type:</td>
					<td colspan="4">&nbsp;<gmjsp:dropdown controlName="refType" SFFormName="frmOPGrowthSetup" SFSeletedValue="refType" tabIndex="5"
							SFValue="alRefTypes" onChange="javascript:fnReload();" width="150" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
							&nbsp;&nbsp;<gmjsp:button value="View" tabindex="6" gmClass="button" onClick="javascript:fnOpenValidate();" buttonType="Load" />&nbsp;&nbsp;&nbsp;
							<gmjsp:button disabled="<%=disabledenabled%>" value="Void Part Growth" tabindex="7" style="width:100px" gmClass="button" onClick="javascript:fnVoid();" buttonType="Save" /> 
							&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td></td>
				</tr>
				<tr><td colspan="5" class="LLine"></td></tr>
			 	<tr class="Shade">
					<td class="RightTableCaption" nowrap colspan="1" align="right" HEIGHT="30"><font color="red">*</font> From Date:</td>
					<td class="RightTableCaption" align="left"  height="30">
						<table cellpadding="1" cellspacing="1" border="0">
							<TR class="Shade">
								<td class="RightTableCaption">&nbsp;<html:text styleId="fromDate" property="fromDate" size="10" maxlength="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="8"/>
									&nbsp;&nbsp;<!-- <img src="<%=strImagePath%>/nav_calendar.gif" style="cursor:hand"  tabindex="5" onclick="javascript:show_calendar('frmOPGrowthSetup.fromDate','','','MON/YY');" height="14">-->				
									&nbsp;<font color="red">*</font>To Date:&nbsp;
									<html:text styleId="toDate" property="toDate" size="10" maxlength="6" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="9"/>
									&nbsp;&nbsp;<img src="<%=strImagePath%>/nav_calendar.gif"  style="cursor:hand" onclick="javascript:showHideCalendar(hFromDate,hToDate)"  height="14">
								</td>										
							</TR>
							<tr>
								<td colspan="5"><div id="dhtmlxDblCalendar" style="position:absolute"></div></td>
							</tr>
						</table>
					</td>
					<td align="right" HEIGHT="30" class="RightTableCaption"> Part #:</td>
					<td colspan="2">&nbsp;<html:text property="pnum" size="35" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="11"/>
						&nbsp;&nbsp;<gmjsp:button value="Load" tabindex="12" gmClass="button" style="width: 4em;height: 22" onClick="javascript:fnReloadPnum();" buttonType="Load" />
					</td>				
			 	</tr>
			 	<tr>
					<td colspan="5"><div id="dhtmlxDblCalendar" style="position:absolute"></div></td>
				</tr>
				<tr><td colspan="5" class="LLine"></td></tr>
			
				<%if(showGrid){ %>
				<tr>
					<td class="RightTableCaption" align="left" colspan="5" height="20">
					<div id="growth_data" style="display:inline" >
						<table cellpadding="1" cellspacing="1" border="0" width="100%" >
							<TR >
									<td class="RightTableCaption">
									&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="copy" style="border: none;" height="14"></a>	
									&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="paste" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a>	
									&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/redo.gif" alt="redo" onClick="javascript:doredo();" style="border: none;" height="14"></a>
									&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/undo.gif" alt="Undo" onClick="javascript:doundo();" style="border: none;" height="14"></a>	
									&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="Add Row" style="border: none;" onClick="javascript:addRow()"height="14"></a>
									&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="Add Rows From Clipboard" style="border: none;" onClick="javascript:addRowFromClipboard()" height="14"></a>
									&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
									&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/undo_delete.gif" alt="Undo Remove Row" style="border: none;" onClick="javascript:enableSelectedRow()" height="14"></a>
									&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/highlight.gif" alt="Enable Marking" style="border: none;" onClick="javascript:enableMarker()"  height="14"></a>
									&nbsp;<a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/disable-highlight.gif" alt="Disable Marking" style="border: none;" onClick="javascript:disableMarker()" height="14"></a>
									&nbsp;<html:text property="applyToMarked" size="4" maxlength="3"  onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="12"/>
										&nbsp;&nbsp;<gmjsp:button value="Apply To Marked" tabindex="13" gmClass="button" buttonType="Save" onClick="javascript:applyToMarkedCell();" />
									</td>										
								</TR>
						</table>
					</div>
					</td>				
			 	</tr>
			 	<tr>
					<td colspan="5">
						<div id="mygrid_container" style="width:<%=strDivWidth%>px;height:<%=strDivHeight%>px"></div>			
					</td>
				</tr>
				<script>
				<% 
				long startTime = System.currentTimeMillis();
				log.debug("Grid Start Time"+startTime); 
				%>
				  var gridData= '<%=strGridData%>';
				  if(gridData.length>0){ 
				  
					initializeGrid();
				}
				</script>
				
				<%
				log.debug("End Time"+System.currentTimeMillis() +"  Time Taken : "+(System.currentTimeMillis()-startTime));
				 %>
				<tr>
					<td colspan="5" align="center" height="35">&nbsp;
					<gmjsp:button value="Submit" name="submitbtn" tabindex="14" gmClass="button" disabled="<%=strDisable%>" onClick="javascript:fnSubmit();" buttonType="Save" />&nbsp;
					<%-- <gmjsp:button name="reload" value="Reload" tabindex="15" gmClass="button" onClick="javascript:fnReloadPnum()" buttonType="Load" disabled="true" /> --%>
					<gmjsp:button  value="Reload Sheet" tabindex="15" gmClass="button" onClick="javascript:fnScheduleSheet()" buttonType="Load"  />
					</td>
				</tr>
				<%} %>
			</table>
		</td>
	</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
