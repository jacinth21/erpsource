<%
/**********************************************************************************
 * File		 		: GmOPTTPGrouping.jsp
 * Desc		 		: mapping TTP with the Demand Sheet
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
  <!-- \operations\purchasing\orderplanning\\GmOPTTPGrouping.jsp--> 
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
 
<bean:define id="strttpId" name="frmOPTTPGroupMap"  property="ttpId" type="java.lang.String"></bean:define>
<bean:define id="strttpName" name="frmOPTTPGroupMap"  property="ttpName" type="java.lang.String"></bean:define>
<% 
String strWikiTitle = GmCommonClass.getWikiTitle("TTP_DEMAND_SHEET_TEMPLATE_MAPPING");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: TTP - Demand Sheet Template Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
function fnReset()
{
	document.frmOPTTPGroupMap.searchttpId.value = "";
	document.frmOPTTPGroupMap.ttpId.value = "";
	document.frmOPTTPGroupMap.ttpName.value = "";
	document.frmOPTTPGroupMap.ttpOwner.value = "0";
	document.frmOPTTPGroupMap.category.value = "107821";//PMT-49826-Set the Category dropdown value = 107821(Launch)
	//To uncheck all the check boxes.
	var objCheckDSArr = document.frmOPTTPGroupMap.checkUnSelectedDS;
	var objCheckDSArrLen = 0;
	if(objCheckDSArr){
		if(objCheckDSArr.type == 'checkbox'){
			objCheckDSArr.checked = false;
		}else{
			objCheckDSArrLen = objCheckDSArr.length;
			for(i = 0; i < objCheckDSArrLen; i++){
				objCheckDSArr[i].checked = false;
			}
		}
	}
	document.frmOPTTPGroupMap.submit();	
}	

function fnFetch()
{	
	var ttpid = document.frmOPTTPGroupMap.ttpId.value;
	if(ttpid == ''){
		fnReset();
	}else{
		fnStartProgress('Y');
		document.frmOPTTPGroupMap.strOpt.value = "edit";
		document.frmOPTTPGroupMap.submit();
	}
}

function fnSubmit()
{
	fnValidateTxtFld('ttpName',' TTP Name ');
	fnValidateDropDn('ttpOwner',' TTP Owner ');
	fnValidateDropDn('category',' Category ');//PMT-49826-TTP Category mapping Validate the TTP Category dropdown
	
	varcheckcnt = 0;
	objCheckRIArr = document.frmOPTTPGroupMap.checkSelectedDS;
	objCheckRIArr1 = document.frmOPTTPGroupMap.checkUnSelectedDS
	if(objCheckRIArr) {	 
		 
		 	objCheckRIArrLen = objCheckRIArr.length;
			for(i = 0; i < objCheckRIArrLen; i ++){	
					if(objCheckRIArr[i].checked){
						varcheckcnt ++;
						break;
					}
				}
				if (objCheckRIArr.type == 'checkbox' && objCheckRIArr.checked)
				{
		 			varcheckcnt ++;
				}
		 }
		 
	if(objCheckRIArr1) {	 
		 
		 	objCheckRIArrLen = objCheckRIArr1.length;
			for(i = 0; i < objCheckRIArrLen; i ++){	
					if(objCheckRIArr1[i].checked){
						varcheckcnt ++;
						break;
					}
				}
				if (objCheckRIArr1.type == 'checkbox' && objCheckRIArr1.checked)
				{
		 			varcheckcnt ++;
				}
		 }
		 
				if (varcheckcnt == 0){
				 
					Error_Details("Please check at least one available sheet or associated sheet.");
				}
				
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmOPTTPGroupMap.strOpt.value = 'save';
	fnStartProgress('Y');
	document.frmOPTTPGroupMap.submit();
}

function fnVoid()
{
		fnValidateDropDn('ttpId',' TTP List ');
		if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
		document.frmOPTTPGroupMap.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmOPTTPGroupMap.hTxnId.value = document.frmOPTTPGroupMap.ttpId.value;
		document.frmOPTTPGroupMap.hTxnName.value = document.frmOPTTPGroupMap.ttpName.value;
		document.frmOPTTPGroupMap.submit();
}

function fnCheckSelections()
{
	var ttp_id = document.frmOPTTPGroupMap.ttpId.value;//PMT-49826-TTP Category mapping
	
	objCheckDSArrLen = 0;
	
	objCheckDSArr = document.frmOPTTPGroupMap.checkSelectedDS;
	if(objCheckDSArr) {
	if (objCheckDSArr.type == 'checkbox')
	{
		objCheckDSArr.checked = true;
	}
	else {
		objCheckDSArrLen = objCheckDSArr.length;
		for(i = 0; i < objCheckDSArrLen; i ++) 
			{	
				objCheckDSArr[i].checked = true;
			}
		}
	}
	//PMT-49826-TTP Category mapping
	//Check the document.frmOPTTPGroupMap.ttpId.value and if its empty or null set the Category dropdown value = 107821  ( Launch) 
	if (ttp_id == '' || ttp_id == null){
		document.frmOPTTPGroupMap.category.value = "107821";//launch	
	} 

}	

 

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnCheckSelections();"> 
<html:form action="/gmOPTTPGroupMap.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDTTP"/>
<html:hidden property="hOld_ttpName" value="<%=strttpName%>"/>
 
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0" HEIGHT="660"> 
		<tr>
			<td height="25" class="RightDashBoardHeader"> TTP - Demand Sheet Template Mapping</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td valign="top" colspan="2">
				<table border="0" class="DtTable1000"  width="100%" cellspacing="0" cellpadding="0" HEIGHT="650">
		    <!-- Custom tag lib code modified for JBOSS migration changes -->		
                    <tr class="evenshade">
                        <td class="RightTableCaption" align="right" width="120" valign="bottom" HEIGHT="30"></font>&nbsp;TTP Name List:</td> 
	                     <td colspan="3"  valign="bottom" style="padding-left: 8px;">
                            <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					        <jsp:param name="CONTROL_NAME" value="ttpId" />
					        <jsp:param name="METHOD_LOAD" value="loadTTPList&STROPT=GOP" />
					        <jsp:param name="WIDTH" value="280" />
					        <jsp:param name="CSS_CLASS" value="search" />
					        <jsp:param name="TAB_INDEX" value=""/>
					        <jsp:param name="CONTROL_NM_VALUE" value="<%=strttpName %>" />
					        <jsp:param name="CONTROL_ID_VALUE" value="<%=strttpId %>" />
					        <jsp:param name="SHOW_DATA" value="50" />
					        <jsp:param name="AUTO_RELOAD" value="fnFetch();" />				
				            </jsp:include>	
    		             </td>
                    </tr>  
                    <tr>
                        <td class="RightTextBlue" width=535" colspan="4" align="center" HEIGHT="10"> Select from list to edit or enter details for a new TTP </td> 
                    </tr>
                    <tr><td colspan="4" class="LLine"></td></tr>
                    <tr class="oddshade">
						<td class="RightTableCaption" align="right" width="120"  HEIGHT="40"></font>&nbsp;TTP Name:</td>
						<td colspan="3">&nbsp;
							<html:text property="ttpName" maxlength="100" size="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
						</td>
					</tr>
                    <tr><td colspan="4" class="LLine"></td></tr>
                    <tr  class="evenshade">
                    	<td class="RightTableCaption"  align="right" width="120" HEIGHT="40" ></font>&nbsp;Available Template:</td> 
                        <td width="490">&nbsp;
                       <DIV style="display:visible;height: 450px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                        <table>
                        	<logic:iterate id="unSelectedDSlist" name="frmOPTTPGroupMap" property="alUnSelectedDS">
                        	<tr>
                        		<td>
							    	<htmlel:multibox property="checkUnSelectedDS" value="${unSelectedDSlist.ID}" />
								    <bean:write name="unSelectedDSlist" property="NAME" />
								</td>
							</tr>	    
							</logic:iterate>
						</table>
						</DIV><BR>
                        </td>
                        <td class="RightTableCaption" align="right" width="80" HEIGHT="40"  ></font>&nbsp;Associated Template:</td> 
                         <td width="490">&nbsp;
                         <DIV style="display:visible;height: 450px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                         <table>
                        	<logic:iterate id="selectedDSlist" name="frmOPTTPGroupMap" property="alSelectedDS">
                        	<tr>
                        		<td>
								    <htmlel:multibox property="checkSelectedDS" value="${selectedDSlist.ID}" />
								    <bean:write name="selectedDSlist" property="NAME" />
	     						</td>
							</tr>	    
							</logic:iterate>
							</table>
							</DIV><BR>
                        </td>
                    </tr>  
                    <tr><td colspan="4" class="LLine"></td></tr>
                    <tr class="oddshade">
    	                <td class="RightTableCaption" align="right" width="120"  HEIGHT="40"></font>&nbsp;TTP Owner:</td> 
	                    <td colspan="3" class="RightTextBlue" >&nbsp;
        		             <gmjsp:dropdown controlName="ttpOwner" SFFormName="frmOPTTPGroupMap" SFSeletedValue="ttpOwner"
							SFValue="alTTPOwner" codeId="ID"  codeName="NAME" defaultValue="[Choose One]" />	
							&nbsp;	Owner's name appears on the PO 											
  		            	</td>
                    </tr>  
                    <tr><td colspan="4" class="LLine"></td></tr>
                              <tr>
    	                <td class="RightTableCaption" align="right" width="120"  HEIGHT="40"><gmjsp:label type="MandatoryText" td="false"/>Category:</td> 
	                    <td colspan="3" class="RightTextBlue" >&nbsp;
        		             <gmjsp:dropdown controlName="category" SFFormName="frmOPTTPGroupMap" SFSeletedValue="category"
							SFValue="alCategory" codeId="CODEID"  codeName="CODENM" defaultValue="[Choose One]" />									
  		            	</td>
                    </tr> 
                    <tr><td colspan="4" class="LLine"></td></tr> 
                    <tr>
                    	<td colspan="4" align="center" height="40">
							<gmjsp:button value="&nbsp;Void&nbsp;" name="Btn_Void" gmClass="button" onClick="fnVoid();"  buttonType="Save" />
		                    <gmjsp:button value="Submit" gmClass="button" onClick="fnSubmit();" buttonType="Save"  /> 
		                    <gmjsp:button value="Reset" gmClass="button" onClick="fnReset();" buttonType="Save" /> 
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>


</BODY>

</HTML>

