
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<!-- \operations\purchasing\orderplanning\GmOPTTPMonthlySummaryReportInc.jsp-->
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ include file="/common/GmHeader.inc" %>

 <% 
Object bean = pageContext.getAttribute("frmOPTTPMonthlySummary", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>

<%
	String strlinkID = GmCommonClass.parseNull((String)request.getParameter("linkID"));
 	strlinkID = strlinkID.equals("") ? "Summary" : strlinkID;
%>

<bean:define id="strMonthId" name="frmOPTTPMonthlySummary" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="strYearId" name="frmOPTTPMonthlySummary" property="yearId" type="java.lang.String"> </bean:define>
<bean:define id="strInvtoryID" name="frmOPTTPMonthlySummary" property="inventoryId" type="java.lang.String"> </bean:define>
<bean:define id="strTtpType" name="frmOPTTPMonthlySummary" property="ttpType" type="java.lang.String"> </bean:define>

<bean:define id="strQuarAvail" name="frmOPTTPMonthlySummary" property="quarAvail" type="java.lang.String"> </bean:define>
<bean:define id="strQuarAlloc" name="frmOPTTPMonthlySummary" property="quarAlloc" type="java.lang.String"> </bean:define>
<bean:define id="strDesc" name="frmOPTTPMonthlySummary" property="pdesc" type="java.lang.String"> </bean:define>

<bean:define id="levelId" name="frmOPTTPMonthlySummary" property="levelId" type="java.lang.String"> </bean:define>
<bean:define id="hLvlHierarchy" name="frmOPTTPMonthlySummary" property="hLvlHierarchy" type="java.lang.String"> </bean:define>
<bean:define id="strDemandSheetID" name="frmOPTTPMonthlySummary" property="demandSheetIds" type="java.lang.String"> </bean:define>
<bean:define id="strAccessID" name="frmOPTTPMonthlySummary" property="accessType" type="java.lang.String"> </bean:define>
<bean:define id="strLockGenAccess" name="frmOPTTPMonthlySummary" property="strLockGenAccess" type="java.lang.String"> </bean:define>

<% 
	
	String strDisable = "";
	if(!strLockGenAccess.equals("Y")){
		strDisable = "true";
	}
	
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	HashMap hmReportValue = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMVALUES"));
	String strStatus = GmCommonClass.parseNull((String)hmReportValue.get("STATUS")) ;
	String strFDate = GmCommonClass.parseNull((String)hmReportValue.get("FINALDATE")) ;
	String strDSStatus = GmCommonClass.parseNull((String)hmReportValue.get("DSSTATUS")) ; // For Submit button to display.
	String strForecastMonth = GmCommonClass.parseNull((String)hmReportValue.get("FORECASTMONTH")) ; 
	
	
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setGroupingRequired(true);
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setSortRequired(false);

	// To specify if its unit not to round by zero  
	gmCrossTab.setValueType(0);
		
	// Line Style information
	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");

	// Between lines
	gmCrossTab.addLine(strFDate);
	gmCrossTab.addLine("Name");
	gmCrossTab.addLine("Description");
	gmCrossTab.addLine("Lead Time");
	//adding line before In Stock
	ArrayList alHeader=GmCommonClass.parseNullArrayList((ArrayList)hmReportValue.get("Header"));
	int val=alHeader.indexOf("In Stock");
	if(val >= 0){
		gmCrossTab.addLine((String)alHeader.get(val-1));
	}
	if(alHeader.indexOf("Description")!=-1){
		gmCrossTab.setNameWidth(400);
	}
	
	String strType=GmCommonClass.parseNull((String)hmReportValue.get("TTPCOLUMNTYPE"));
	if(strType.equals("102580"))
	{
		gmCrossTab.addLine("Unit Cost $");
		gmCrossTab.addLine("RW Inventory");
		gmCrossTab.addLine("Set PAR");
		gmCrossTab.addLine("Total Required");
		gmCrossTab.addLine("Parent LTA");
		gmCrossTab.addLine("Forecast Qty");
		gmCrossTab.addLine("LTA");
		gmCrossTab.addLine("Parent Need");
		gmCrossTab.addLine("TPR");
	}else if(strType.equals("102580SPLIT"))
	{
		gmCrossTab.addLine("Unit Cost $");
		gmCrossTab.addLine("RW Inventory");
		gmCrossTab.addLine("Forecast Qty OUS");
		gmCrossTab.addLine("LTA OUS");
		gmCrossTab.addLine("Parent Need");
		gmCrossTab.addLine("LTA OUS");
		gmCrossTab.addLine("TPR OUS");
		gmCrossTab.addLine("Set PAR");
		gmCrossTab.addLine("Parent LTA OUS");
		gmCrossTab.addLine("Total Required");
	}else if(strType.equals("OTHERS"))
	{
		gmCrossTab.addLine("Unit Cost $");
		gmCrossTab.addLine("TPR");
		gmCrossTab.addLine("Total Inventory");
		gmCrossTab.addLine("Total Required");
	}
	
	gmCrossTab.setHideColumn("ID");
	
	if(strQuarAlloc.equals("Y"))
	{
		gmCrossTab.addLine("Quar. Alloc");
	}	
	else if(strQuarAvail.equals("Y"))
	{
		gmCrossTab.addLine("Quar. Avail");
	}
	
	gmCrossTab.addStyle("Name","ShadeMedBlueTD","ShadeDarkBlueTD");
	gmCrossTab.addStyle("Description","ShadeMedBlueGrayLeftTD","ShadeDarkBlueTD");
	gmCrossTab.addStyle("Unit Cost $","ShadeLightBlueTD","ShadeDarkBlueTD");
	gmCrossTab.addStyle("Lead Time","ShadeMedBlueGrayTD","ShadeDarkBlueTD");
	gmCrossTab.addStyle("Calc Lead Time","ShadeLightBlueTD","ShadeDarkBlueTD");
	
	gmCrossTab.addStyle("In Stock","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("PO","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("DHR","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Built Sets","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("Total Inventory","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
	gmCrossTab.addStyle("RW Inventory","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
	
	gmCrossTab.addStyle("Quar. Avail","ShadeLightMarronTD","ShadeDarkMarronTD");
	gmCrossTab.addStyle("Quar. Alloc","ShadeMedMarronTD","ShadeDarkMarronTD");
	
	gmCrossTab.addStyle("Parent Need","ShadeMedBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("Parent Need US","ShadeLightBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("Parent Need OUS","ShadeMedBrownTD","ShadeDarkBrownTD");
	
	gmCrossTab.addStyle("Parent LTA","ShadeMedBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("Parent LTA US","ShadeLightBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("Parent LTA OUS","ShadeMedBrownTD","ShadeDarkBrownTD");
	
	gmCrossTab.addStyle("OTN","ShadeLightGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("OTN US","ShadeMedGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("OTN OUS","ShadeLightGreenTD","ShadeDarkGreenTD");
	
	
	gmCrossTab.addStyle("Set PAR","ShadeMedGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("Total Required","ShadeLightPurpleTD","ShadeDarkPurpleTD");
	
	gmCrossTab.addStyle("TPR","ShadeLightBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("TPR US","ShadeLightBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("TPR OUS","ShadeMedBrownTD","ShadeDarkBrownTD");
	
	gmCrossTab.addStyle("LTA","ShadeLightBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("LTA US","ShadeLightBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("LTA OUS","ShadeMedBrownTD","ShadeDarkBrownTD");
	
	gmCrossTab.addStyle("Forecast Qty","ShadeMedBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("Forecast Qty US","ShadeLightBrownTD","ShadeDarkBrownTD");
	gmCrossTab.addStyle("Forecast Qty OUS","ShadeMedBrownTD","ShadeDarkBrownTD");
					
	gmCrossTab.addStyle("Order Qty","ShadeMedYellowTD","ShadeDarkYellowTD") ;
	gmCrossTab.addStyle("Order Cost $","ShadeLightYellowTD","ShadeDarkYellowTD");
	
	gmCrossTab.setNoDivRequired(true);
	gmCrossTab.add_RoundOff("Unit Cost $","2");
	gmCrossTab.add_RoundOff("Order Cost $","2");

	gmCrossTab.setColumnOverRideFlag(true);
	gmCrossTab.setColumnWidth("Description",250); 
	gmCrossTab.setNameWidth (150);
	
	gmCrossTab.setTableId(strlinkID);
	gmCrossTab.setDivHeight(470);
	// Adding Cross Over
	gmCrossTab.setExport(true, pageContext,"/gmOPTTPMonthlySummary.do","TTP_Summary.xls");
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmOPTTPSummaryDecorator");
	gmCrossTab.setAttribute("INVENTORYID",strInvtoryID);
	gmCrossTab.setAttribute("TTPTYPE",strTtpType);
	gmCrossTab.setAttribute("MONYEAR",strMonthId+" "+ strYearId);
	gmCrossTab.setAttribute("LEVELID",levelId);
	gmCrossTab.setAttribute("HLVLHIERARCHY",hLvlHierarchy);
	gmCrossTab.setAttribute("DMID",strDemandSheetID);
	gmCrossTab.setAttribute("ACCESSID",strAccessID);	
	gmCrossTab.setAttribute("FORECASTMONTH",strForecastMonth);	
%>


<html:hidden property="inventoryId" value='<%=strInvtoryID%>'/>
<table border="0" cellspacing="0" cellpadding="0"><tr><td>	
							
 <%=gmCrossTab.PrintCrossTabReport(hmReportValue) %>
</tr></td>
<%				
					if (strDSStatus.equals("false") && ( hLvlHierarchy.equals("102580") || (hLvlHierarchy.equals("") && levelId.equals("")) ) )
					{
					%>				
						<tr>
							<td colspan ="7" align=center height="35"><gmjsp:button disabled="<%=strDisable%>" value="&nbsp;Lock & Generate&nbsp;" gmClass="button" onClick="fnSubmit();" buttonType="Save" /></td>
						</tr>
					<%
					}
				%>	
</table>
<%@ include file="/common/GmFooter.inc" %>

				 