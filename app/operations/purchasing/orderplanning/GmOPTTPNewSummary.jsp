<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- operations\purchasing\orderplanning\GmOPTTPNewSummary.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtTTPNewSummary" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtTTPNewSummary:setBundle basename="properties.labels.operations.purchasing.GmOPTTPNewSummary" />
<bean:define id="gridData" name="frmOPTTPNewSummary" property="gridData" type="java.lang.String"> </bean:define>
<bean:define id="demandSheetIds" name="frmOPTTPNewSummary" property="demandSheetIds" type="java.lang.String"> </bean:define>
<bean:define id="inventoryId" name="frmOPTTPNewSummary" property="inventoryId" type="java.lang.String"> </bean:define>
<bean:define id="accessId" name="frmOPTTPNewSummary" property="accessId" type="java.lang.String"> </bean:define>
<bean:define id="levelId" name="frmOPTTPNewSummary" property="levelId" type="java.lang.String"> </bean:define>
<bean:define id="levelFl" name="frmOPTTPNewSummary" property="levelFl" type="java.lang.String"> </bean:define>
<bean:define id="ttpForecastMonths" name="frmOPTTPNewSummary" property="ttpForecastMonths" type="java.lang.String"> </bean:define>
 <%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
 %>
 
<HTML>
<head>
<title>Globus Medical:TTP New Summary</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<style type="text/css" media="all"> 
@import url("<%=strCssPath%>/screen.css");</style>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPTTPNewSummary.js"></script>
<script>
var objGridData = '';
objGridData = '<%=gridData%>';
var demandSheetIds = '<%=demandSheetIds%>';
var invIDVal='<%=inventoryId%>';
var access='<%=accessId%>';
var levelID='<%=levelId%>';
var forecastmonth='<%=ttpForecastMonths%>';
var levelFl = '<%=levelFl%>';
</script>
<html:hidden property="inventoryId" id="inventoryId" styleId="inventoryId" value="<%=inventoryId %>"/>
 </head>
 <BODY onload = "fnTTPSummaryPageLoad()">
<table border="0"  width="100%" height="60" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="4">
					<div id="dataGridDiv" style="" height="560px" width="*"></div>
				</td>
			</tr>		
		<tr>
			<td colspan="4" align="center">
			    <div class='exportlinks'><fmtTTPNewSummary:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"><fmtTTPNewSummary:message key="DIV_EXCEL"/> 
				</a></div>
			</td>
		</tr>			
</table>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>