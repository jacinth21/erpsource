
<%
	/**********************************************************************************
	 * File		 		: GmDemandTemplateSummary.jsp
	 * Desc		 		: This jsp will display associated group and sets for the Demand Template
	 * Version	 		: 1.0
	 * author			: Gopinathan Saravanan
	 ************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel"%>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel"%>
<HTML>
<bean:define id="groupAssocList" name="frmGmDemandTemplate"
	property="alGroupAssoc" type="java.util.List"></bean:define>
<bean:define id="conSetAssocList" name="frmGmDemandTemplate"
	property="alConSetAssoc" type="java.util.List"></bean:define>
<bean:define id="inhSetAssocList" name="frmGmDemandTemplate"
	property="alInhSetAssoc" type="java.util.List"></bean:define>
<bean:define id="submitFlag" name="frmGmDemandTemplate" 
	property="submitFlag" type="java.lang.String"></bean:define>
	<!-- \operations\purchasing\orderplanning\GmDemandTemplateSummary.jsp --> 
<%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	int intListSize = 0;
	HashMap hmLoop = new HashMap();
	String strId = "";
	String strName = "";
	String strClassShade = "";
	String strDisable = "";
	if(!submitFlag.equals("Y")){
		strDisable = "true";
	}	
%>
<head>
<TITLE>Globus Medical: Demand Sheet Template Setup</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript"
	src="<%=strPurchasingJsPath%>/GmDemandTemplateSetup.js"></script>
</head>

<!-- Custom tag lib code modified for JBOSS migration changes -->
<BODY leftmargin="20" topmargin="10" onLoad="fnPageLoadTempMapping();">
	<html:form action="/gmDemandTemplate.do">
		<html:hidden property="strOpt" />
		<html:hidden property="demandTempltId" />
		<html:hidden property="inputString" />

		<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="3" class="RightDashBoardHeader">&nbsp;Demand
					Sheet Template Summary</td>
				<td align="right" class=RightDashBoardHeader><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help'
					width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass
						.getWikiTitle("DEMAND_SHEET_TEMPLATE_SUMMARY")%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="4" class="LLine"></td>
			</tr>
			<tr>
				<td width="698" height="100" valign="top" colspan="4">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr class="evenShade">
							<gmjsp:label type="BoldText" SFLblControlName="Template Name:"
								td="true" />
							<td width="220">&nbsp;<bean:write name="frmGmDemandTemplate"
									property="demandTempltNm" />
							</td>
							<gmjsp:label type="BoldText" SFLblControlName="Demand Period:"
								td="true" />
							<td width="220">&nbsp;<bean:write name="frmGmDemandTemplate"
									property="demandPeriod" />&nbsp;(months)</td>
						</tr>
						<tr>
							<td colspan="4" class="LLine"></td>
						</tr>
						<tr class="oddShade">
							<gmjsp:label type="BoldText" SFLblControlName="Forecast Period:"
								td="true" />
							<td>&nbsp;<bean:write name="frmGmDemandTemplate"
									property="forecastPeriod" />&nbsp;(months)</td>
							<gmjsp:label type="BoldText"
								SFLblControlName="Set Build Forecast:" td="true" />
							<td>&nbsp;<bean:write name="frmGmDemandTemplate"
									property="setBuild" />&nbsp;(months)</td>
						</tr>
						<tr>
							<td colspan="4" class="LLine"></td>
						</tr>
						<tr class="evenShade">
							<gmjsp:label type="BoldText" SFLblControlName="Company:"
								td="true" />
							<td>&nbsp;<bean:write name="frmGmDemandTemplate"
									property="companyNm" />
							</td>
							<gmjsp:label type="BoldText" SFLblControlName="Sheet Owner:"
								td="true" />
							<td>&nbsp;<bean:write name="frmGmDemandTemplate"
									property="ownerNm" />
							</td>
						</tr>
						<tr>
							<td colspan="4" class="LLine"></td>
						</tr>
						<tr class="oddShade">
							<gmjsp:label type="BoldText" SFLblControlName="Template Status:"
								td="true" />
							<td colspan="3">&nbsp;<bean:write
									name="frmGmDemandTemplate" property="sheetStatus" />
							</td>
						</tr>
						<tr>
							<td colspan="4" class="Line"></td>
						</tr>
						<tr class="ShadeRightTableCaption">
							<td class="RightTableCaption" colspan="4">&nbsp;Group
								Details&nbsp;</td>
						</tr>
						<tr>
							<td colspan="4" class="Line"></td>
						</tr>
						<%
							intListSize = groupAssocList.size();
								if (intListSize > 0) {
									for (int j = 0; j < intListSize; j++) {
										hmLoop = (HashMap) groupAssocList.get(j);
										strId = GmCommonClass.parseNull((String) hmLoop
												.get("ID"));
										strName = GmCommonClass.parseNull((String) hmLoop
												.get("NAME"));
										strClassShade = "oddshade";
										if (j % 2 == 0) {
											strClassShade = "evenshade";
										}
						%>
						<tr class="<%=strClassShade%>">
							<td colspan="4">&nbsp;&nbsp;-&nbsp;<%=strName%></td>
						</tr>
						<%
							}
								} else {
						%>
						<tr class="evenshade">
							<td colspan="2">&nbsp;&nbsp;<font color="red">- No
									Groups are selected</font></td>
						</tr>
						<%
							}
						%>
						<tr>
							<td colspan="4" class="Line"></td>
						</tr>
						<tr class="ShadeRightTableCaption">
							<td class="RightTableCaption" colspan="4">&nbsp;Consignment
								Sets&nbsp;</td>
						</tr>
						<tr>
							<td colspan="4" class="Line"></td>
						</tr>
						<%
							intListSize = conSetAssocList.size();
								if (intListSize > 0) {
									for (int j = 0; j < intListSize; j++) {
										hmLoop = (HashMap) conSetAssocList.get(j);
										strId = GmCommonClass.parseNull((String) hmLoop
												.get("ID"));
										strName = GmCommonClass.parseNull((String) hmLoop
												.get("NAME"));
										strClassShade = "oddshade";
										if (j % 2 == 0) {
											strClassShade = "evenshade";
										}
						%>
						<tr class="<%=strClassShade%>">
							<td colspan="4">&nbsp;&nbsp;-&nbsp;<%=strId%>&nbsp;-&nbsp;<%=strName%>
							</td>
						</tr>
						<%
							}
								} else {
						%>
						<tr class="evenshade">
							<td colspan="2">&nbsp;&nbsp;<font color="red">- No
									Consignment Sets are selected</font></td>
						</tr>
						<%
							}
						%>
						<tr>
							<td colspan="4" class="Line"></td>
						</tr>
						<tr class="ShadeRightTableCaption">
							<td class="RightTableCaption" colspan="4">&nbsp;InHouse
								Sets&nbsp;</td>
						</tr>
						<tr>
							<td colspan="4" class="Line"></td>
						</tr>
						<%
							intListSize = inhSetAssocList.size();
								if (intListSize > 0) {
									for (int j = 0; j < intListSize; j++) {
										hmLoop = (HashMap) inhSetAssocList.get(j);
										strId = GmCommonClass.parseNull((String) hmLoop
												.get("ID"));
										strName = GmCommonClass.parseNull((String) hmLoop
												.get("NAME"));
										strClassShade = "oddshade";
										if (j % 2 == 0) {
											strClassShade = "evenshade";
										}
						%>
						<tr class="<%=strClassShade%>">
							<td colspan="4">&nbsp;&nbsp;-&nbsp;<%=strId%>&nbsp;-&nbsp;<%=strName%>
							</td>
						</tr>
						<%
							}
								} else {
						%>
						<tr class="evenshade">
							<td colspan="2">&nbsp;&nbsp;<font color="red">- No
									InHouse Sets are selected</font></td>
						</tr>
						<%
							}
						%>
						<logic:equal name="frmGmDemandTemplate" property="sheetStatus" value="Active">
						<tr>
							<td colspan="4" class="Line"></td>
						</tr>
						<tr class="ShadeRightTableCaption">
							<td class="RightTableCaption" colspan="4">&nbsp;Demand Sheet Names&nbsp;</td>
						</tr>
						<tr>
							<td colspan="4" class="Line"></td>
						</tr>
						</logic:equal>						
						<tr>
							<td colspan="4" >
								<!-- <DIV
									style="display: visible; height: 300px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;"> -->
										<logic:iterate id="demandSheetsList" name="frmGmDemandTemplate"
											property="alDemandSheets">
<!-- 											<tr>
												<td> -->
												&nbsp; 
													<bean:write name="demandSheetsList" property="DEMANDTEMPLTNM" /><BR>
<!-- 												</td>
											</tr> -->
										</logic:iterate>
<!-- 								</DIV> -->
							</td>
						</tr>
						<tr>
							<td colspan="4" class="Line"></td>
						</tr>							
						<tr>
							<td colspan="4" align="center" height="30">
							<logic:equal name="frmGmDemandTemplate" property="sheetStatus" value="Draft">
							<gmjsp:button style="width: 7em"
								 value="&nbsp;Back&nbsp;" gmClass="button"
								onClick="fnLoadTempMapping();" buttonType="Load" />&nbsp; <gmjsp:button style="width: 7em"
								value="&nbsp;Submit&nbsp;"  controlId="btnSumrySub" name="btnSumrySub" disabled="<%=strDisable%>" gmClass="button" onClick="fnSaveDemandSheets();" buttonType="Save"/>
							</logic:equal>
							<logic:equal name="frmGmDemandTemplate" property="sheetStatusId" value="102641">
							<gmjsp:button style="width: 8em" controlId="btnSumryEditTempl" 
							 value="&nbsp;Edit&nbsp;Template&nbsp;" gmClass="button"
								onClick="fnLoadDemandTemplate();" buttonType="Save" />
							</logic:equal>							
							</td>
						</tr>
					</table></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>