   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
   <%@ page import ="com.globus.common.servlets.GmServlet"%>
    <%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>    
   <!-- WEB-INF path corrected for JBOSS migration changes -->
    <!-- \operations\purchasing\orderplanning\GmOPIncludeSheetSummary.jsp--> 
    <%@ include file="/common/GmHeader.inc" %>

   
  <script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
	<script><!--
		function fnRowOverride(id, type)
		{
			var dsheetID = <bean:write name="frmOPDemandSheetSummary" property="demandSheetMonthId"/> + id ;
//			alert(" dsheetID" + dsheetID);
			windowOpener("/GmCommonLogServlet?hType=1228&hID="+dsheetID+"&hHideComment=Y","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
			
		}
		
		function fnCallConsD(id, type)
		{
			var formattedid = id.replace('^','-');
			var dsheetID = <bean:write name="frmOPDemandSheetSummary" property="demandSheetMonthId"/> + formattedid ;
			windowOpener("/GmCommonLogServlet?hType=1228&hID="+dsheetID,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
		}
		
			function fnCallEditPAR(id, dsid, dmid)
		{
		var temp = new Array();
		    temp = id.split('^');
		    var partNum = temp[0];
		windowOpener("/gmOPDSParSetup.do?partNumbers="+partNum+"&demandMasterId="+dmid+"&demandSheetId="+dsid+"&strOpt=edit&popType=pop","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");		
		}
		
		function fnCallPPP(id, pstatus, dsid, dmid)
		{
		var temp = new Array();
		    temp = id.split('^');
		    var partNum = temp[0];
		    var setID = temp[1];
		windowOpener("/gmOPDSSummary.do?partNumbers="+partNum+"&pstatus="+pstatus+"&demandSheetMonthId="+dsid+"&demandSheetId="+dmid+"&setId="+setID+"&strOpt=fetchPPP&popType=pop","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=925,height=300");		
		}
		
	--></script>
    <bean:define id="hmReportValue" name="frmOPDemandSheetSummary"  property="hmDemandSheetDetail" type="java.util.HashMap"></bean:define>
    <bean:define id="demandsheetid" name="frmOPDemandSheetSummary" property="demandSheetId" type="java.lang.String"></bean:define>
    <bean:define id="demandmonthid" name="frmOPDemandSheetSummary" property="demandSheetMonthId" type="java.lang.String"></bean:define>
    <bean:define id="strUnitRPrice" name="frmOPDemandSheetSummary" property="unitrprice" type="java.lang.String"> </bean:define>
    <bean:define id="demandTypeId" name="frmOPDemandSheetSummary" property="demandTypeId" type="java.lang.String"> </bean:define>
    <bean:define id="monthId" name="frmOPDemandSheetSummary" property="monthId" type="java.lang.String"> </bean:define>
    <bean:define id="yearId" name="frmOPDemandSheetSummary" property="yearId" type="java.lang.String"> </bean:define>
    <bean:define id="accessType" name="frmOPDemandSheetSummary" property="accessType" type="java.lang.String"> </bean:define>
    <bean:define id="levelID" name="frmOPDemandSheetSummary" property="levelID" type="java.lang.String"> </bean:define>
    <bean:define id="forecastMonths" name="frmOPDemandSheetSummary" property="forecastMonths" type="java.lang.String"> </bean:define>
   
    <input type="hidden" name="hExport" />
    <input type="hidden" name="hAccessType" value="<%=accessType%>">
    <input type="hidden" name="hLevelID" value="<%=levelID%>">
    <%
    GmServlet gm = new GmServlet();
	String strMonthYear = monthId + "/" + yearId;
	
	log.debug("::demandmonthid: "+demandmonthid+"demandsheetid: "+ demandsheetid);
	
    try
    {	    
		GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
		ArrayList alForecastHeader = new ArrayList();
		ArrayList alDrillDown = new ArrayList();		
		HashMap hmapValue = new HashMap();
		String strFooterStyle = "";  	
		String strStatus = "";
		int r = 0;			

		if(strUnitRPrice.equals("Price"))
		{
			gmCrossTab.setValueType(1);
			gmCrossTab.setNoDivRequired(true);
		}
		else
		{
			// To specify if its unit not to round by zero  
			gmCrossTab.setValueType(0);	
		}

		if (hmReportValue.size() == 0) 
		{
			return;
		}

		// If sheet still open for comments then enable comments drill down 
		strStatus = (String) hmReportValue.get("SHEETSTATUS");
				
		gmCrossTab.setAttribute("SHEETSTATUS",hmReportValue.get("SHEETSTATUS"));
		
		// Line Style information
		gmCrossTab.setColumnDivider(false);
		gmCrossTab.setColumnLineStyle("borderDark");
		//gmCrossTab.setRowHighlightRequired(true);
		
		alForecastHeader = (ArrayList) hmReportValue.get("FORECASTHEAD");
		int Headersize = alForecastHeader.size();
		
		//gmCrossTab.setSelectedColDrillDown(true);
		for (int i = 0; i < Headersize; i++)
        {
            hmapValue = (HashMap) alForecastHeader.get(i);
            r = i%2;
            if (r == 0) 
            {
            	strFooterStyle = "ShadeMedBrownTD";
            }
            else
            {
            	strFooterStyle = "ShadeLightBrownTD";
            }
			gmCrossTab.addStyle((String)hmapValue.get("PERIOD"),strFooterStyle,"ShadeDarkBrownTD") ;
			//if (i == 0 && strStatus.equals("50550"))
			//{
				//gmCrossTab.addSpecificCollDrillDown((String)hmapValue.get("PERIOD"));
			//}
        }
		gmCrossTab.addStyle("Name","ShadeLightBlueTD","ShadeDarkBlueTD");
		gmCrossTab.addStyle("Demand Weighted Avg","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("Demand Avg","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("Forecast Avg","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		//gmCrossTab.addStyle("Variance %","ShadeLightOrangeTD","ShadeDarkOrangeTD") ; 
		gmCrossTab.addStyle("Overridden Weighted Avg","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("PAR","ShadeLightYellowTD","ShadeDarkYellowTD") ;
		gmCrossTab.addStyle("PBO","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("PBL","ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("PCS","ShadeLightOrangeTD","ShadeDarkOrangeTD") ;
		gmCrossTab.addStyle("Set PAR","ShadeMedGreenTD","ShadeDarkGreenTD") ;
	 	gmCrossTab.addStyle("OTN","ShadeLightGreenTD","ShadeDarkGreenTD") ;  
	
		if(!demandTypeId.equals("40023"))
		{
			gmCrossTab.setColumnOverRideFlag(true);
		}
		gmCrossTab.addLine("Name");
		gmCrossTab.addLine("PAR");
		gmCrossTab.addLine("PCS");
		gmCrossTab.addLine("OTN");
		
		//gmCrossTab.addLine("Demand Weighted Avg");
		//gmCrossTab.addLine("Forecast Avg"); 
		
		gmCrossTab.addLine((String) hmReportValue.get("DEMANDEND"));
		//gmCrossTab.addLine("Variance %");
		gmCrossTab.addLine("Forecast Avg");
		gmCrossTab.addLine("Overridden Weighted Avg");
		//gmCrossTab.add_RoundOff("Demand Weighted Avg","1");

		
		gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");
		gmCrossTab.setTotalRequired(false);
		gmCrossTab.setColumnTotalRequired(false);
		gmCrossTab.setGroupingRequired(true);
		gmCrossTab.setRowHighlightRequired(true);
		gmCrossTab.setLinkRequired(false);
		gmCrossTab.setDecorator("com.globus.operations.purchasing.orderplanning.displaytag.GmOPDemandSheetSummaryDecorator");
		//gmCrossTab.setDrillDownDetails(alDrillDown); 
		gmCrossTab.setAttribute("DMID",demandsheetid);
		gmCrossTab.setAttribute("DSID",demandmonthid);
		gmCrossTab.setAttribute("DEMANDTYPE",demandTypeId);
		gmCrossTab.setAttribute("DEMANDMONTH",strMonthYear);
		gmCrossTab.setAttribute("FORECASTMONTHS",forecastMonths);
		//gmCrossTab.setExport(true,pageContext,"", "");
		out.println(gmCrossTab.PrintCrossTabReport(hmReportValue)) ;

    }catch(Exception e)
	{
		e.printStackTrace();
	}
	%>