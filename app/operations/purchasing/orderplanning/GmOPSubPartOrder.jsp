<%
/**********************************************************************************
 * File		 		: GmOPSubPartOrder.jsp
 * Desc		 		: Sub Part Mapping Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<!-- \operations\purchasing\orderplanning\GmOPSubPartOrder.jsp -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<bean:define id="returnList" name="frmOPSubPartOrder" property="returnList" type="java.util.List"></bean:define>
<%
	ArrayList alSubList = new ArrayList();	 
	alSubList = GmCommonClass.parseNullArrayList((ArrayList) returnList); 
	int rowsize = alSubList.size();	 
	StringBuffer sbInput = new StringBuffer();
	String strPnum = "";
	String strInput = "";	 
	DynaBean db ;
	for(int i=0;i<rowsize;i++){
		db = (DynaBean)alSubList.get(i);
		strPnum = String.valueOf(db.get("PNUM"));
		sbInput.append(strPnum);
		sbInput.append("^");
	}
	strInput = sbInput.toString();	
	String strWikiTitle = GmCommonClass.getWikiTitle("SUB_PART_ORDER_MAPPING");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Sub Part Order Mapping</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"
	media="print" />

<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnReload(){    
	if(document.frmOPSubPartOrder.pnum.value == "" && document.frmOPSubPartOrder.projectListID.value == 0){
		Error_Details(" Please choose a value from <B>Project List</B> dropdown to proceed.");
	}	
	if (ErrorCount > 0){
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmOPSubPartOrder.strOpt.value = "reload"; 
	fnStartProgress('Y');
	document.frmOPSubPartOrder.submit();   
}

function fnSelectAll(){
  TotalRows = <%=rowsize%>;
  	for (var i=0;i<TotalRows ;i++ ){
		obj = eval("document.frmOPSubPartOrder.Chk_sub"+i);	
		obj.checked = document.frmOPSubPartOrder.Chk_selectAll.checked;		 
	}
} 
  
function fnSubmit(){ 	
	var varRows = <%=rowsize%>;
    var varProjId =  document.frmOPSubPartOrder.projectListID.value; 	  
	var inputString = ''; 
		if(varProjId!=null)	{
			for (var i =0; i < varRows; i++){
				var checkedFl = '';
				var varPnum='';
				//The input part numbers are taken from Hidden field(wrapper).
				obj = eval("document.frmOPSubPartOrder.pnum"+i);
				varPnum = obj.value;
				obj = eval("document.frmOPSubPartOrder.Chk_sub"+i);
				varStatus = obj.value;
				if(obj.checked){
					checkedFl = 'Y';
		        }
				inputString = inputString + varPnum +','+checkedFl+'^';	
			}	 
		}
 	document.frmOPSubPartOrder.strOpt.value = 'save';
 	document.frmOPSubPartOrder.hinputStr.value = inputString;
 	fnStartProgress('Y');
	document.frmOPSubPartOrder.submit();	 
} 
//This function will call the press the enter button.
function fnEnter(){
	if(event.keyCode == 13)
	{	 
		var rtVal = fnReload();
//below code is when fnReload() return false, we need to stop the jsp, from submitting.
		if(rtVal == false){
			if(!e) var e = window.event;
 
	        e.cancelBubble = true;
	        e.returnValue = false;
 
	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeypress="fnEnter();">
<html:form action="/gmGOPSubPartOrder.do">
	<html:hidden property="strOpt" value="" />
	<html:hidden property="hinputStr" />
	
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4">Sub Part Order Mapping</td>
			<td  height="25" class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' 
	       		 height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />			 
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="5"></td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="5">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td height="30" class="RightTableCaption" align="right">Project List:</td>
						<td style="padding-left: 4px;"><gmjsp:dropdown controlName="projectListID" SFFormName="frmOPSubPartOrder" SFSeletedValue="projectListID"
							SFValue="projectList" codeId = "ID"  codeName = "IDNAME"  defaultValue= "[Choose One]"/> 		
						</td>
					</tr>				 
                    <tr><td colspan="5" class="LLine"></td></tr>
                    <tr class="shade">
                       <td class="RightTableCaption" align="right" height="25">&nbsp;Part Numbers:</td> 
                       <td width="25%">&nbsp;<html:text property="pnum"  size="40" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 
                       </td>
                       <td><gmjsp:button name="Load" value="Load" style="height: 23; width: 5em;" gmClass="button" onClick="fnReload();" buttonType="Load" /></td>                         
                    </tr>       
                    <tr><td colspan="5" class="LLine"></td></tr>         
				<%if (rowsize > 0) {%>
		 			<tr>
  						<td colspan="5" align="right"  >Select All <input type="checkbox" name="Chk_selectAll" onClick="fnSelectAll();">&nbsp;</td>
 					</tr> 
 				<%}%>
					<tr>
						<td colspan="5" align="center">
							<display:table name="requestScope.frmOPSubPartOrder.returnList" requestURI="/gmGOPSubPartOrder.do"   class="its" id="currentRowObject"  decorator="com.globus.operations.displaytag.beans.DTSubPartOrderWrapper"> 
 							<display:column property="PNUM" title="Part #"  sortable="true" />
							<display:column property="DESCRIPTION" title="Part Description"  class="alignleft" sortable="true" />
							<display:column property="STATUS" title=""  class="alignright" />
							</display:table>
						</td>
					</tr>
					<tr>
						<td class="LLine" height="1" colspan="5"></td>
					</tr>
				<%if (rowsize > 0) {%> 
					<tr></tr>
					<tr>
						<td colspan="5" align="center"><gmjsp:button value="Submit" gmClass="button"   onClick="fnSubmit();" buttonType="Save" /></td>
					</tr>               
	 				<tr></tr>
				<% }%> 		 
				</table>	 
			</td>
		</tr>
	</table>
	</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
