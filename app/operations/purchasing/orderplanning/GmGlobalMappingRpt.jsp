<%
/**********************************************************************************
 * File		 		: GmGlobalMappingRpt.jsp
 * Desc		 		: This jsp will load Global Mapping details
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<HTML>
<bean:define id="xmlString" name="frmGlobalMappingReport" property="xmlString" type="java.lang.String" />

<%
String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
%>

 <!-- \operations\purchasing\orderplanning\GmGlobalMappingRpt.jsp -->


<head>
<TITLE> Globus Medical: Global Mapping Report</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmGlobalMappingRpt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script type="text/javascript">
var gridObj;
var objGridData = '<%=xmlString%>';
</script>
</head>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmGlobalMappingReport.do">
<html:hidden property="strOpt"  />

<!-- Custom tag lib code modified for JBOSS migration changes -->
<table class="DtTable700" border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" colspan="5" class="RightDashBoardHeader">&nbsp;Global Mapping Report</td>
			<td align="right" colspan="1" class=RightDashBoardHeader>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("GLOBAL_MAPPING_REPORT")%>');" />
			</td> 
		</tr>
		<tr><td colspan="6" class="line"></td></tr>
		<tr class="evenshade">
			<gmjsp:label type="BoldText"  SFLblControlName="Company:" td="true"/>
			<td height="30">&nbsp;<gmjsp:dropdown controlName="companyId"
				SFFormName="frmGlobalMappingReport" SFSeletedValue="companyId" 
				SFValue="alCompanyId" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" /></td>
			<gmjsp:label type="BoldText"  SFLblControlName="Level:" td="true"/>
			<td height="30">&nbsp;<gmjsp:dropdown controlName="levelId"
				SFFormName="frmGlobalMappingReport" SFSeletedValue="levelId" 
				SFValue="alLevelId" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" /></td>
			<gmjsp:label type="BoldText"  SFLblControlName="Value:" td="true"/>
			<td height="30">&nbsp;<gmjsp:dropdown controlName="valueId"
				SFFormName="frmGlobalMappingReport" SFSeletedValue="valueId" 
				SFValue="alValueId" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" /></td>								
		</tr>
				<tr><td colspan="6" class="LLine"></td></tr>
		<tr class="oddshade">
			<gmjsp:label type="BoldText"  SFLblControlName="Access&nbsp;Type:" td="true"/>
			<td height="30">&nbsp;<gmjsp:dropdown controlName="accessId"
				SFFormName="frmGlobalMappingReport" SFSeletedValue="accessId" 
				SFValue="alAccessId" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" /></td>
			<gmjsp:label type="BoldText"  SFLblControlName="Inventory:" td="true"/>
			<td height="30">&nbsp;<gmjsp:dropdown controlName="inventoryId"
				SFFormName="frmGlobalMappingReport" SFSeletedValue="inventoryId" 
				SFValue="alInventoryId" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" /></td>
			<td height="30"></td>
			<td height="30">&nbsp;
				<gmjsp:button controlId ="loadbtn"	value="&nbsp;Load&nbsp;" name="loadbtn" gmClass="button" onClick="javascript:fnLoad();" tabindex="0" buttonType="Load" />
			</td>								
		</tr>
		<tr>
			<td colspan="6">
				<div id="dataGrid" height="400px" width="699px"></div>
			</td>
		</tr>
		<%if(xmlString.indexOf("cell") != -1) {%>
		<tr>
			<td colspan="8" align="center" height="25" >
				<div class='exportlinks'>
					Export Options : <img src='img/ico_file_excel.png' />&nbsp;
					<a href="#" onclick="fnExcelExport();"> Excel </a>
				</div>
			</td>
		</tr>
		<%} %>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>