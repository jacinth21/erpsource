<%
/**********************************************************************************
 * File		 		: GmPartQtyDetails.jsp
 * Desc		 		: Report for Part QTY details
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- \operations\purchasing\orderplanning\GmOPPartQtyDetails.jsp -->

<bean:define id="hmReportValue" name="frmOPDemandSheetSummary"  property="hmDemandSheetDetail" type="java.util.HashMap"></bean:define> 
<%  
String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_SUMMARY");
 String strPartDesc = (String) hmReportValue.get("PARTDESC");
  	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Part Qty</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css"> 
<script language="JavaScript" src="<%=strJsPath%>GlobusCommonScript.js"></script>  
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPDemandSheet.js"></script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmOPDSSummary.do">
<html:hidden property="strOpt" value = ""/>

 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Part Drill Down Details</td>
			
			<td align="right" class=RightDashBoardHeader > 	
				  
				</td>
		</tr>
		<tr>
			<td width="848"   valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="4" class="ELine"></td></tr> 
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;Part Number:</td>
						<td >&nbsp;
							<bean:write name="frmOPDemandSheetSummary" property="partNumbers" />												
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="30" ></font>&nbsp;Description:</td>
						<td >&nbsp;<%=strPartDesc%>
						 									
						</td>
						 
					</tr>	
					</td>
				</table>
  			   </td>
  		  </tr>	 
    </table>
    <p> &nbsp;</p>
  
 <table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		 		
		<tr>
			<td height="25" class="RightDashBoardHeader"> Part Details</td>
		 
		</tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<tr>
			<td > 	
					<jsp:include page= "/operations/purchasing/orderplanning/GmOPIncludePartQtyDetails.jsp" />
			</td>
		</tr>
    <table>	
    
    <p> &nbsp;</p>
  
 <table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		 		
		<tr>
			<td height="25" class="RightDashBoardHeader">Parent Part Details</td>
		 
		</tr>
		<tr>
			<td > 	<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->	
					<jsp:include page= "/operations/purchasing/orderplanning/GmOPParentPartQtyDetails.jsp" />
			</td>
		</tr>
    <table>
   
  
   <p> &nbsp;</p>
  
 <table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		 		
		<tr>
			<td height="25" class="RightDashBoardHeader">Sub Part Details</td>
		 
		</tr>
		<tr>
			<td >
					<jsp:include page= "/operations/purchasing/orderplanning/GmOPSubPartQtyDetails.jsp" />
			</td>
		</tr>
    <table> 
    	     	
</html:form>
 
</BODY>

</HTML>

