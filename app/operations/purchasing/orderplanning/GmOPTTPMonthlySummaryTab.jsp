				
<!-- \operations\purchasing\orderplanning\GmOPTTPMonthlySummaryTab.jsp--> 
 <%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>


<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>

 
<% 
Object bean = pageContext.getAttribute("frmOPTTPMonthlySummary", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<bean:define id="demandSheetIds" name="frmOPTTPMonthlySummary" property="demandSheetIds" type="java.lang.String"> </bean:define>
<bean:define id="ttpId" name="frmOPTTPMonthlySummary" property="ttpId" type="java.lang.String"> </bean:define>
<bean:define id="strMonthId" name="frmOPTTPMonthlySummary" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="strYearId" name="frmOPTTPMonthlySummary" property="yearId" type="java.lang.String"> </bean:define>
<bean:define id="levelId" name="frmOPTTPMonthlySummary" property="levelId" type="java.lang.String"> </bean:define>
<bean:define id="hLvlHierarchy" name="frmOPTTPMonthlySummary" property="hLvlHierarchy" type="java.lang.String"> </bean:define>
<bean:define id="ttpForecastMonths" name="frmOPTTPMonthlySummary" property="ttpForecastMonths" type="java.lang.String"> </bean:define>

				<table>						
						<tr><td>					
						<ul id="summarytab" class="shadetabs">
						<li><a href="/gmOPTTPMonthlySummary.do?strOpt=fetchFilter&levelId=<%=levelId%>&hLvlHierarchy=<%=hLvlHierarchy%>&ttpForecastMonths=<%=ttpForecastMonths %>" rel="ajaxcontentarea"  class="selected">Filter</a></li>
						<!-- <li><a href="/gmOPTTPMonthlySummary.do?strOpt=reportFCRequest&ttpId=<%=ttpId%>&monthId=<%=strMonthId%>&yearId=<%=strYearId%>&demandSheetIds=<%=demandSheetIds%>" rel="ajaxcontentarea">Forecast Request</a></li> -->						
						</ul>
						
						<div id="ajaxdivtabarea" class="contentstyle" style="display:visible;height: 75px; overflow: auto;">												
						</div>						
					</td></tr> 
				 </table>
				<div id="ajaxreportarea" class="contentstyle" style="display:visible;height: 300px; ">
				<table>				
					<tr> <td>	<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->					
						    <jsp:include page="GmOPTTPMonthlySummaryReportInc.jsp"/>
						</td>
					</tr>
				</table>
			    </div>	