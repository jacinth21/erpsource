<%
/**********************************************************************************
 * File		 		: GmDemandSheetMap.jsp
 * Desc		 		: mapping Group / Set / Region / InHouse
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
  <!-- \operations\purchasing\orderplanning\GmSheetSetPAR.jsp--> 
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%> 

<bean:define id="alDemandSheetList" name="frmSheetSetPAR" property="alDemandSheetList" type="java.util.ArrayList"></bean:define>
<bean:define id="returnReport" name="frmSheetSetPAR" property="returnReport" type="java.util.List"></bean:define>
<bean:define id="updatefl" name="frmSheetSetPAR" property="updatefl" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmSheetSetPAR" property="strOpt" type="java.lang.String"></bean:define>
<%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	String strWikiTitle = GmCommonClass.getWikiTitle("SET_PAR_SETUP");
	String strJNDIConnection = GmCommonClass.parseNull(GmCommonClass.getString("JNDI_CONNECTION")); 
	String strDisable = "";
	ArrayList alReportList = new ArrayList();
	alReportList = GmCommonClass.parseNullArrayList((ArrayList) returnReport);
	int rowsize = alReportList.size();
	
	if(!updatefl.equals("Y")){
		strDisable = "true";
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmSheetSetPAR.js"></script>
<style type="text/css" media="all">@import url("<%=strCssPath%>/screen.css");</style>

<script> 
var varRows = <%=rowsize%>;
var accessfl = '<%=updatefl%>';
var JNDI_CONNECTION = '<%=strJNDIConnection%>';
</script>
<style>
table.itss {
	width: 100%;
}

table.itss thead tr {
	background-color: #cccccc;
	height: 25px;
}

table.itss tr.even {
	background-color: #ecf6fe;
}

table.itss thead th {
	FONT-SIZE: 11px; 
	text-align: left;
}
</style>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnpageLoad();" onkeyup="fnEnter();" >
<html:form action="/gmSheetSetPAR.do?method=sheetSetPARUpdReport" >
<html:hidden property="strOpt" />
<html:hidden property="updatefl" />
<html:hidden property="hinputStr" />
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Set PAR-Setup</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />  
				</td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="3">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">                                       
		    <!-- Custom tag lib code modified for JBOSS migration changes -->		
                    <tr>
                      	<td class="RightTableCaption" align="Right" width="18%" height="25">Demand Sheet:</td>
                    	<td class="RightTablecaption" align="left" style="padding-left: 4px;"><gmjsp:dropdown controlName="demandSheetName" SFFormName="frmSheetSetPAR" SFSeletedValue="demandSheetName" SFValue="alDemandSheetList" defaultValue= "[Choose One]" 
								 codeId="DMID" codeName="DMNM"/>
						
						</tr>
                   	<tr><td class="LLine" height="1" colspan="4"></td></tr>
                   	<tr class="shade">             
                   	<td class="RightTableCaption" align="Right" width="18%" height="25">Set ID:</td>
                    <td class="RightTablecaption" align="left">
                    	<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Struts tag lib code modified for JBOSS migration changes -->
                    		<tr>
                    			<td>&nbsp;<html:text maxlength="50" property="demandSetId" size="20"   
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
								</td>      	
								<td class="RightTableCaption" align="Right" height="25">Set Name:</td>						
								<td width="30%"><html:text maxlength="50" property="demandSetNm" size="35"   
									styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
								</td>				
								<td align="center"><gmjsp:button value="Load" gmClass="button" name="btn_Load" style="width: 4em;height: 23" onClick="fnLoad();" buttonType="Load" /></td>
								</tr>
								</table>	                  			
                   		</td>
                   		</tr>
                   	<tr><td class="LLine" height="1" colspan="4"></td></tr>
                    <tr>
                    	<td colspan="4" align="center">
                    		<display:table name="requestScope.frmSheetSetPAR.returnReport" requestURI="/gmSheetSetPAR.do?method=sheetSetPARUpdReport" export="true" decorator="com.globus.displaytag.DTSheetSetPARWrapper" class="itss" id="currentRowObject" >
                    		<display:setProperty name="export.excel.decorator" value="com.globus.displaytag.DTSheetSetPARWrapper" />  
							<display:column property="SETID" title="Set ID" sortable="true" class="alignleft" media="html"/>
							<display:column property="EXCELSETID" title="Set ID" sortable="true" class="alignleft" media="excel"/>
							<display:column property="SDESC" title="Set Name" sortable="true" class="alignleft" />
							<display:column property="PARVALUE" title="PAR value" media="html" style="text-align:left;width:100;"/>
							<display:column property="EXCELPAR" title="PAR value" class="alignleft" media="excel" />
							<display:column property="UPDATEDBY" title="Updated By" sortable="true"  />
							<display:column property="UPDATEDDATE" title="Updated Date" sortable="true" class="aligncenter" />
							<display:column property="COMMENTS" title="Comments" class="aligncenter" media="html"/>
							</display:table> 
		                </td>
                    </tr>
                     <%if(rowsize > 0) { %>
                     <tr><td colspan="4" class="LLine" height="1"></td></tr>
                     <tr></tr>
                     <tr><td align="center" colspan="4"><gmjsp:button value="Submit" disabled="<%=strDisable%>" gmClass="button" name="Btn_Submit" style="width: 4em;height: 23" onClick="fnSubmit();" buttonType="Save" /></td></tr>
                     <tr></tr>
                     <%} %>
   				</table>
  			   </td>
  		  </tr>	  		 
    </table>		     	
</html:form>
</BODY>
 <%@ include file="/common/GmFooter.inc" %> 
</HTML>
