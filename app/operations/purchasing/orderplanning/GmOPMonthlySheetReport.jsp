 <%
/**********************************************************************************
 * File		 		: GmOPMonthlySheetReport.jsp
 * Desc		 		: This screen is used for the Viewing the Monthly Sheet Report
 * Version	 		: 1.0
 * author			: APrasath
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<!-- \operations\purchasing\orderplanning\GmOPMonthlySheetReport.jsp -->
<%
    String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strAction = (String)request.getAttribute("hAction");	
	String strWikiTitle = GmCommonClass.getWikiTitle("MONTHLY_SHEET_SUMMARY");	
	//Read control values for reload
	String strSheetName = GmCommonClass.parseNull((String)request.getAttribute("SHEETNM"));
	String strStatus=GmCommonClass.parseNull((String)request.getAttribute("STATUS"));
	String strSheetType=GmCommonClass.parseNull((String)request.getAttribute("SHEETTYPE"));
	String strMonth=GmCommonClass.parseNull((String)request.getAttribute("MONTH"));
	String strYear=GmCommonClass.parseNull((String)request.getAttribute("YEAR"));
	String strTTP = GmCommonClass.parseNull((String)request.getAttribute("TTP"));
	String strSheetOwner = GmCommonClass.parseNull((String)request.getAttribute("SHEETOWNER"));
	String strLevel=GmCommonClass.parseNull((String)request.getAttribute("LEVEL"));
	String strValue= GmCommonClass.parseNull((String)request.getAttribute("VALUE"));
	String strAccessType = GmCommonClass.parseNull((String)request.getAttribute("ACCESSTYPE"));
	String strJNDIConnection = GmCommonClass.parseNull(GmCommonClass.getString("JNDI_CONNECTION"));
	String strTTPNm = GmCommonClass.parseNull((String)request.getAttribute("TTPNM"));
	
	HashMap hmLists = (HashMap)request.getAttribute("HMLISTS");
	
	ArrayList alStatus 	  	  = (ArrayList)hmLists.get("ALSTATUS");
	ArrayList alSheetType	  = (ArrayList)hmLists.get("ALSHEETTYPE");
	ArrayList alMonth    	  = (ArrayList)hmLists.get("ALMONTH");
	ArrayList alYear      	  = (ArrayList)hmLists.get("ALYEAR");
	ArrayList alLevel     	  = (ArrayList)hmLists.get("ALLEVEL");
	ArrayList alValue         = (ArrayList)hmLists.get("ALVALUE");
	ArrayList alAccessType    = (ArrayList)hmLists.get("ALACCESSTYPE");	
	ArrayList alSheetOwner    = (ArrayList)hmLists.get("ALSHEETOWNER"); 	
	
	String strLimitedAccess   = GmCommonClass.parseNull((String)request.getAttribute("DMNDSHT_LIMITED_ACC"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Monthly Sheet Summary</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmOPMonthlySheetReport.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeydown="javaScript:return fnEnter();">
<FORM name="frmMonthlySheetReport" method="POST" action="<%=strServletPath%>/GmOPTTPMonthlySheetServlet">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hTxnName" value="">
<input type="hidden" name="hCancelType" value="EXEDS">
<input type="hidden" name="hJNDIConnection" value="<%=strJNDIConnection%>">

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="6" height="25" class="RightDashBoardHeader">Monthly Sheet Summary</td>		
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
		</tr>	
		<tr>
			<td class="RightTableCaption" align="right"  height="20" nowrap >&nbsp;Sheet Name:</td>
			<td>&nbsp;<input type="text" size="40"   value="<%=strSheetName%>" name="txt_SheetName" class="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td>	
			<td class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Status:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Status"  seletedValue="<%= strStatus %>"  tabIndex="2" 
				value="<%= alStatus%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/></td>
			<td class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Sheet Type:</td>
			<td colspan="2">&nbsp;<gmjsp:dropdown controlName="Cbo_SheetType"  seletedValue="<%= strSheetType %>"  
				tabIndex="3" value="<%= alSheetType%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/></td>	
		</tr>
		<tr>
			<td colspan="8" class="LLine" height="1"></td>
		</tr>
		<tr class="Shade">
			<td class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Level:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Level"  seletedValue="<%= strLevel %>"  tabIndex="4" 	 
					value="<%= alLevel%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
			<td class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Value:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Value"  seletedValue="<%= strValue %>"  tabIndex="5" 	 
					value="<%= alValue%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
			<td class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Access Type:</td>
			<td colspan="2">&nbsp;<gmjsp:dropdown controlName="Cbo_AccessType"  seletedValue="<%= strAccessType %>"  tabIndex="6" 	 
				    value="<%= alAccessType%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>	
		</tr>
		<tr>
			<td colspan="8" class="LLine" height="1"></td>
		</tr>
		<tr>
	    	<td class="RightTableCaption" align="right"  height="20" nowrap >&nbsp;TTP Name:</td>
	    	<td style="padding-left: 3px;">
            <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
			<jsp:param name="CONTROL_NAME" value="Cbo_ttp" />
			<jsp:param name="METHOD_LOAD" value="loadTTPList&STROPT=GOP" />
			<jsp:param name="WIDTH" value="280" />
			<jsp:param name="CSS_CLASS" value="search" />
			<jsp:param name="TAB_INDEX" value="7"/>
			<jsp:param name="CONTROL_NM_VALUE" value="<%=strTTPNm %>" />
			<jsp:param name="CONTROL_ID_VALUE" value="<%=strTTP %>" />
			<jsp:param name="SHOW_DATA" value="50" />				
			</jsp:include>	
			</td> 
	    	<td class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Sheet owner:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_SheetOwner"  seletedValue="<%= strSheetOwner %>" tabIndex="8"
					value="<%= alSheetOwner%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" /> </td>
	  		<td class="RightTableCaption" align="right"  height="30" nowrap >&nbsp;Demand Period:</td>
			<td colspan="2">&nbsp;<gmjsp:dropdown controlName="Cbo_Month"  seletedValue="<%= strMonth %>" tabIndex="9" width="85"
					value="<%= alMonth%>" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]" />
					<gmjsp:dropdown controlName="Cbo_Year"  seletedValue="<%= strYear %>" 	 tabIndex="10" width="85"
					value="<%= alYear%>" codeId = "CODENM"  codeName = "CODENM"  defaultValue= "[Choose One]"/>&nbsp;&nbsp;
					<gmjsp:button value="Load" style="height: 23; width: 5em;" name="Btn_Go" tabindex="11" gmClass="button" onClick="javascript:fnGo()"  buttonType="Load" /></td>
			</tr>
			<tr>
				<td colspan="8" class="LLine" height="1"></td>
			</tr>
		<tr> 
			<td colspan="7" align="center">
				<display:table name="requestScope.results.rows" export="true" id="currentRowObject" class="its" requestURI="GmOPTTPMonthlySheetServlet" 
								decorator="com.globus.operations.purchasing.orderplanning.displaytag.DTOPMonthlyDemandSheetWrapper">
                <display:setProperty name="export.excel.filename" value="Monthly Sheet Summary Report.xls" /> 
				<display:setProperty name="export.excel.decorator" value="com.globus.operations.purchasing.orderplanning.displaytag.DTOPMonthlyDemandSheetWrapper" /> 
					<display:column property="DMSHEETID" title="" sortable="true" class="alignleft" style="width:68px" media="html"/>
					<display:column property="DEMNM" title="Sheet Name" style="text-align:left" />
					<display:column property="DTYPE" title="Sheet Type" sortable="true"    />
					<display:column property="CUSER" title="Sheet Owner" sortable="true" maxLength="15" />
					<display:column property="LEVELID" title="Level" sortable="true" />
					<display:column property="LEVELVALUE" title="Value" sortable="true" />
					<display:column property="ACCESSTYPE" title="Access Type" sortable="true"/>				
					<display:column property="PERIODDATE" title="Demand Period Date" sortable="true"  class="aligncenter" style="text-align:center;width:100;"/>
					<display:column property="DPER" title="Demand Period" sortable="true" class="aligncenter" style="text-align:center;width:65;"/>
					<display:column property="FPER" title="Forecast Period" sortable="true"  class="aligncenter" style="text-align:center;width:65;"/>
					<display:column property="DSTAT" title="Status" sortable="true" style="text-align:left" />
					<display:column property="LUSER" title="Last Updated By" sortable="true" maxLength="15" style="text-align:left;width:90;"/>
				</display:table>
			</td>
		</tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
