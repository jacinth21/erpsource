<%
/**********************************************************************************
 * File		 		: GmDemandTemplateSetup.jsp
 * Desc		 		: This jsp will setup the Demand Template
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="submitFlag" name="frmGmDemandTemplate" property="submitFlag" type="java.lang.String"></bean:define>
<%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	String strDisable = "";
	if(!submitFlag.equals("Y")){
		strDisable = "true";
	}	
%>
<HTML>


<head>
<TITLE> Globus Medical: Demand Sheet Template Setup</TITLE>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmDemandTemplateSetup.js"></script>
</head>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnLoad();">
<html:form action="/gmDemandTemplate.do">
<html:hidden property="strOpt"/>
<html:hidden property="demandTempltId"/>
<html:hidden property="demandRuleVal"/>
<html:hidden property="forecastRuleVal"/>
<html:hidden property="setBuildRuleVal"/>
<html:hidden property="sheetStatusId"/>
<html:hidden property="submitFlag" />
<input type="hidden" name="hInActiveFl" value="<bean:write name='frmGmDemandTemplate' property='inactiveFlag'/>" />
<!-- \operations\purchasing\orderplanning\GmDemandTemplateSetup.jsp --> 
<!-- Custom tag lib code modified for JBOSS migration changes -->
<table class="DtTable700" border="0" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25"  class="RightDashBoardHeader">&nbsp;Demand Sheet Template Setup</td>
			<td align="right" class=RightDashBoardHeader>
				<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("DEMAND_SHEET_TEMPLATE_SETUP")%>');" />
			</td> 
		</tr>
		<tr><td colspan="2" class="LLine"></td></tr>
		<tr>
			<td width="698" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="oddShade">
							<gmjsp:label type="BoldText"  SFLblControlName="Template List:" td="true"/>
						<td width="70%">&nbsp;<gmjsp:dropdown controlName="demandTempltdrpdwn" SFFormName="frmGmDemandTemplate" SFSeletedValue="demandTempltId"
							SFValue="alDemandTemplt" codeId = "CODEID"  codeName = "CODENM" onChange="fnFetch();"  defaultValue= "[Choose One]" />													
						</td>
					</tr>
					<tr>
                        <td class="RightTextBlue" colspan="2" align="center"> Select from list to edit or enter details for a new Demand Sheet Template</td> 
                    </tr>
                    <tr><td colspan="2" class="LLine"></td></tr>
                    <tr class="evenShade">
                    	<gmjsp:label type="MandatoryText"  SFLblControlName="Template Name:" td="true"/>
	                     <td>&nbsp;<html:text property="demandTempltNm"  size="40" maxlength="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');fnSetDefault();"/>
    		             </td>
                    </tr>  
                    <tr><td colspan="2" class="LLine"></td></tr>
                    <tr class="oddShade">
                    	<gmjsp:label type="MandatoryText"  SFLblControlName="Demand Period:" td="true"/>
                        <td>&nbsp;<html:text property="demandPeriod"  size="3" maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" onkeypress="return fnNumbersOnly(event);"/> &nbsp;(months)                                                                 
    		             </td>
                    </tr>   
                    <tr><td colspan="2" class="LLine"></td></tr>
                    <tr class="evenShade">
                    	<gmjsp:label type="MandatoryText"  SFLblControlName="Forecast Period:" td="true"/>
                        <td>&nbsp;<html:text property="forecastPeriod"  size="3" maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" onkeypress="return fnNumbersOnly(event);"/>  &nbsp;(months)                                                                 
    		             </td>
                    </tr>   
                    <tr><td colspan="2" class="LLine"></td></tr>
                    <tr class="oddShade">
                    	<gmjsp:label type="MandatoryText"  SFLblControlName="Set Build Forecast:" td="true"/>
                        <td>&nbsp;<html:text property="setBuild"  size="3" maxlength="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" onkeypress="return fnNumbersOnly(event);"/>  &nbsp;(months)
                        </td>
                    </tr>
                    <tr><td colspan="2" class="LLine"></td></tr>
                    <tr class="evenShade">
                    	<gmjsp:label type="MandatoryText"  SFLblControlName="Company:" td="true"/>
                        <td>&nbsp;<gmjsp:dropdown controlName="companyId" SFFormName="frmGmDemandTemplate" SFSeletedValue="companyId"
							SFValue="alCompany" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" />													
						</td>
                    </tr>                      
                    <tr><td colspan="2" class="LLine"></td></tr>
                     <tr class="oddShade">
                     	<gmjsp:label type="MandatoryText"  SFLblControlName="Sheet Owner:" td="true"/>
						<td width="70%">&nbsp;<gmjsp:dropdown controlName="demandSheetOwner" SFFormName="frmGmDemandTemplate" SFSeletedValue="demandSheetOwner"
							SFValue="alDSOwner" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]" />													
						</td>
					</tr>
                    <tr><td colspan="2" class="LLine"></td></tr> 
                    <tr class="evenShade">
                        <gmjsp:label type="BoldText"  SFLblControlName="Inactive Flag:" td="true"/> 
                        <td>&nbsp;<html:checkbox property="inactiveFlag"/>
                        </td>
                    </tr>
                    	<tr>
						<td colspan="2" align="center" height="24">&nbsp;
	                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmGmDemandTemplate" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>	
						</td>
						</tr>
					 <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
                    	<td colspan="2" align="center" height="30">
		                    <gmjsp:button controlId="btnTemptSub" name="btnTemptSub" value="Submit" disabled="<%=strDisable%>" gmClass="button" style="width: 7em" onClick="fnSubmit();" buttonType="Save"/>&nbsp;<span id="spnBtn"></span>
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>			
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>