 <%
/**********************************************************************************
 * File		 		: GmForecastRequest.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
  <!-- \\operations\purchasing\orderplanning\GmOPForecastRequest.jsp --> 
  <%@ include file="/common/GmHeader.inc" %>
 
<% 
try{	
Logger log = GmLogger.getInstance(GmCommonConstants.COMMON);  // Instantiating the Logger 
Object bean = pageContext.getAttribute("frmOPTTPMonthlySummary", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Forecast Request </TITLE>

<BODY leftmargin="20" topmargin="10">
	<display:table name="requestScope.frmOPTTPMonthlySummary.alForeCastReport" export="false"  class="its" id="currentRowObject" > 
	<display:column property="DS_NAME" title="Demand Sheet Name" group="1" />
	<display:column property="REQ_PERIOD" title="Request Period" />	
  	<display:column property="ID" title="ID" group="2" />
  	<display:column property="NAME" title="Name" group="3" />  	
  	<display:column property="REQUIRED_DATE" title="Month-Year"  />
  	<display:column property="REQUIRED_QTY" title="Qty"  />
 </display:table> 

<% } catch(Exception exp) { exp.printStackTrace(); } %>
</BODY>

</HTML>
