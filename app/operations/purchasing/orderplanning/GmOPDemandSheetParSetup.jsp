<%
/**********************************************************************************
 * File		 		: GmOPDemandSheetParSetup.jsp
 * Desc		 		: Map the part to Demand Sheet
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="org.apache.commons.beanutils.BeanMap, org.apache.commons.beanutils.RowSetDynaClass, org.apache.commons.beanutils.BasicDynaBean,org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.util.ArrayList,java.util.Iterator" %>

<bean:define id="updatefl" name="frmOPDemandSheetSetup" property="updatefl" type="java.lang.String"></bean:define>
<bean:define id="strdemandMasterId" name="frmOPDemandSheetSetup"  property="demandMasterId" type="java.lang.String"></bean:define>
<bean:define id="strdemandMasterName" name="frmOPDemandSheetSetup"  property="searchdemandMasterId" type="java.lang.String"></bean:define>

<%
String strWikiTitle = GmCommonClass.getWikiTitle("DEMAND_SHEET_PAR_SETUP"); 
ArrayList alList = new ArrayList();
alList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("RDDEMANDPAR")); 
String strJNDIConnection = GmCommonClass.parseNull(GmCommonClass.getString("JNDI_CONNECTION")); 
int intRows = 0;
intRows = alList.size();
StringBuffer sbInput = new StringBuffer();
String strPar = "";
String strInput = ""; 
String strDisabled="";
DynaBean db ;
	for (int i=0;i<intRows;i++)	{
		db = (DynaBean)alList.get(i);
		strPar = String.valueOf(db.get("PNUM"));
		sbInput.append(strPar);
		sbInput.append("^");
	}
	 strInput = sbInput.toString();
	 
	if(!updatefl.equals("Y")){
		strDisabled = "true";
	}
%>
 <!-- \operations\purchasing\orderplanning\GmOPDemandSheetParSetup.jsp --> 
<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet PAR Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
var JNDI_CONNECTION = '<%=strJNDIConnection%>';
function fnFetch(){
	fnValidateDropDn("demandMasterId", " Demand Sheet Name");
	if (ErrorCount > 0) {
		Error_Show();
		Error_Clear();
		return false;
	}
	fnStartProgress('Y');
	document.frmOPDemandSheetSetup.strOpt.value = 'edit';	
	document.frmOPDemandSheetSetup.Load.disabled = true;
	document.frmOPDemandSheetSetup.submit();	
}

//Enter key functionality.
function fnEnter(){	
	if(event.keyCode == 13)
	{	 
		var rtVal = fnFetch();
//below code is when fnFetch() return false, we need to stop the jsp, from submitting.
		if(rtVal == false){
			if(!e) var e = window.event;

	        e.cancelBubble = true;
	        e.returnValue = false;

	        if (e.stopPropagation) {
	                e.stopPropagation();
	                e.preventDefault();
	        }
		}
	}		
}
function fnLoad(){
	var strpop = document.frmOPDemandSheetSetup.popType.value; 
	if(strpop=="pop"){ 
		document.frmOPDemandSheetSetup.Load.disabled = true;
		document.frmOPDemandSheetSetup.demandMasterId.disabled = true;
		document.frmOPDemandSheetSetup.partNumbers.disabled = true;
	}
}
 
function fnSubmit(){
	var varRows = <%=intRows%>;
    var strpop = document.frmOPDemandSheetSetup.popType.value; 
	var varDMId =  document.frmOPDemandSheetSetup.demandMasterId.value;
	var varDSId =  document.frmOPDemandSheetSetup.demandSheetId.value; 
	
	var varParValue = '';
	var varpnum = '';
	var oldParval = ''; 
	var errPart = '';
	var inputString = ''; 
	var varInput = "<%=strInput%>";
	
	var arrPar = varInput.split('^');	
	if(varDMId!=null){
		for (var i =0; i < varRows; i++){
			objPartID = eval("document.frmOPDemandSheetSetup.partID"+i);
			objParValue = eval("document.frmOPDemandSheetSetup.parvalue"+i);
			objOldParVal = eval("document.frmOPDemandSheetSetup.hPARVal"+i);

			if(objPartID != null || objPartID != undefined){
				varpnum = objPartID.value;
			}
			if(objParValue != null || objParValue != undefined){
				varParValue = TRIM(objParValue.value);
			}
			oldParval = TRIM(objOldParVal.value); 
			if( varParValue != oldParval){
				if(!isNaN(varParValue)){
					inputString = inputString + varpnum + '^'+ varDMId +'^'+ varParValue +  '|';
				}else{
					errPart = errPart + "<br>" + varpnum;
				}
			}
			if(strpop=="pop"){
				document.frmOPDemandSheetSetup.hpartnumber.value = varpnum;
				document.frmOPDemandSheetSetup.hparvalue.value = varParValue;
			}		   
		}
	}
	if(strpop=="pop"){
		fnClose();
	}	
	if(errPart != ''){
		Error_Details("<B>Par Value</B> should be numeric for the following part(s)<B>"+errPart+ "</B>");
		Error_Show();
		Error_Clear();
		return false;
	}else{
		fnStartProgress('Y');
		document.frmOPDemandSheetSetup.strOpt.value =  'save'; //'save';
		document.frmOPDemandSheetSetup.hinputStr.value = inputString;
		document.frmOPDemandSheetSetup.Btn_submit.disabled = true;
		document.frmOPDemandSheetSetup.submit();	
	}
}
 		
function fnLog(type,id){
	var varDSId = document.frmOPDemandSheetSetup.demandMasterId.value;
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id+"-"+varDSId+"&hJNDIConnection="+JNDI_CONNECTION,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
} 

function fnParHistory(partid){
	var varDSId = document.frmOPDemandSheetSetup.demandMasterId.value;
	//windowOpener("/gmDSParHistory.do?demandMasterId="+varDSId+"&partNumbers="+partid+"&logType="+logtype+"&logId="+partid+"-"+varDSId+"&hHistoryForward=audittrail&hJNDIConnection="+JNDI_CONNECTION,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
	windowOpener("/gmAuditTrail.do?auditId=1005&txnId="+ partid+"&hJNDIConnection="+JNDI_CONNECTION,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnClose(){
 var url = window.opener.document.URL; 
  if(url.indexOf("gmDSSummary") != -1)   {
     window.opener.fnSheetFetch();
  }else if (url.indexOf("gmTTPMonthlySummary") != -1){
     window.opener.fnReport();
  }  
 window.close();
}
</script>
<style>
table.itss {
	width: 100%;
}

table.itss thead tr {
	background-color: #cccccc;
	height: 25px;
}

table.itss tr.even {
	background-color: #ecf6fe;
}

table.itss thead th {
	FONT-SIZE: 11px; 
	text-align: left;
}
</style>
</HEAD>
<BODY leftmargin="20" topmargin="10"  onLoad="fnLoad()"  onkeypress="fnEnter();" >
<html:form action="/gmOPDSParSetup.do">
<html:hidden property="strOpt" />
<html:hidden property="haction" />
<html:hidden property="hinputStr" />
<html:hidden property="logType" /> 
<html:hidden property="logId" />  
<html:hidden property="popType" />
<html:hidden property="demandSheetId" />
<html:hidden property="hpartnumber" />
<html:hidden property="hparvalue" /> 

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Demand Sheet PAR Setup</td>
			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr height="25">
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"><font color="red">*</font>&nbsp;Demand Sheet Name:</td>
						<%-- <td width="70%" style="padding-left: 8px;">&nbsp;<gmjsp:dropdown controlName="demandMasterId" SFFormName="frmOPDemandSheetSetup" SFSeletedValue="demandMasterId"
							SFValue="alDemandSheetName" codeId = "DMID"  codeName = "DMNM"    defaultValue= "[Choose One]" />													
						</td> --%>
						<td style="padding-left: 2px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="demandMasterId" />
					<jsp:param name="METHOD_LOAD" value="loadDemandMasterSheetList&searchType=PrefixSearch" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="CONTROL_NM_VALUE" value="<%=strdemandMasterName %>" />
					<jsp:param name="CONTROL_ID_VALUE" value="<%=strdemandMasterId %>" />
					<jsp:param name="SHOW_DATA" value="100" />
					<jsp:param name="AUTO_RELOAD" value="" />
			     	</jsp:include>
						</td>
					</tr>
				 
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="shade" height="25"> 
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Part Numbers:</td> 
                        <td style="padding-left: 2px;"><table border="0"  cellspacing="0" cellpadding="0">
                        		<tr>
                        			<td width="30%"><html:text property="partNumbers"  size="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
                        			<td>&nbsp;<a title="You can perform wildcard search. For eg: type 101 to pull up all parts that has 101 in it"><img src=<%=strImagePath%>/question.gif border=0</a></td>
                        			<td><gmjsp:button name="Load"  style="height: 23;width: 5em;" value="&nbsp;&nbsp;Load&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnFetch();" buttonType="Load"/></td>
                        		</tr>
                        	</table>
                        </td>
                    </tr>
                    <tr><td height="1" colspan="2" class="LLine"></td></tr>
                    <tr>
                    	<td colspan="2" align="center">
                    		<display:table name="RDDEMANDPAR" requestURI="/gmDSParSetup.do" excludedParams="haction" class="itss" id="currentRowObject" decorator="com.globus.operations.purchasing.orderplanning.displaytag.DTOPParWrapper">
                    			<display:column property="PNUM" title="Part Number" sortable="true" class="aligncenter" style="width:100px" />
                    			<display:column property="PDESC" title="Part Description" style="width:500px" sortable="true" class="alignleft"/> 
								<display:column property="PARVALUE" title="Par Value" style="width:100px"  class="alignleft"/> 
								<display:column property="HISTORY"  title="" class="aligncenter"/> 
								<display:column property="COMMENTS" title="Comments" style="width:100px" class="aligncenter"/> 
                    		</display:table>
                    	</td>
                    </tr>
                     <%if(intRows > 0) { %>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr></tr>
                    <tr>
                    	<td colspan="2" align="center">
		                     <gmjsp:button value="Submit"  disabled="<%=strDisabled%>" gmClass="button" name="Btn_submit" onClick="fnSubmit();" buttonType="Save" />  
		                </td>
                    </tr>
                    <tr></tr>
                    <%} %>
                 </table>
              </td>
           </tr>
        </table>
        </html:form>
        <%@ include file="/common/GmFooter.inc" %>
      </BODY>
  </HTML>
