<%
/**********************************************************************************
 * File		 		: GmMultiProjectPartMap.jsp
 * Desc		 		: Enables the part to be available for multiple projects. 
 * Version	 		: 1.0
 * author			: Rajeshwaran Varatharajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<!-- \operations\purchasing\orderplanning\GmOPMultiProjectPartMap.jsp --> 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page errorPage="/common/GmError.jsp" %>
<%@ page import="java.util.ArrayList"%>

<%@ page import="java.util.List"%>


<%@ page import="org.apache.commons.beanutils.RowSetDynaClass, org.apache.commons.beanutils.BasicDynaBean"%>

 <bean:define id="ldtPartNumberDetails" name="frmOPMultiProjectPart" property="ldtPartNumberDetails" type="java.util.List"></bean:define>

<%

ArrayList alSubList = new ArrayList();

alSubList = GmCommonClass.parseNullArrayList((ArrayList) ldtPartNumberDetails);

int rowsize=0;
if(alSubList!=null)
	rowsize=alSubList.size();

String strWikiTitle = GmCommonClass.getWikiTitle("PM_MULTIPRJ_PART_MAP");

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Multiple Project Part Mapping </TITLE> 
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>t/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
function fnSelectAll(){
  TotalRows = <%=rowsize%>;
 for (var i=0;i<TotalRows ;i++ )
	{	

		if(document.frmOPMultiProjectPart.Chk_selectAll.checked){
			document.frmOPMultiProjectPart.checkMultiProject[i].checked=true;
		}
		else{
			document.frmOPMultiProjectPart.checkMultiProject[i].checked=false;
			}	 
	}
}
function fnMasterCheckBox() {
	document.frmOPMultiProjectPart.Chk_selectAll.checked = true;
	var TotalRows =<%=rowsize%>;	
	for (var i = 0; i < TotalRows; i++) {
		if (document.frmOPMultiProjectPart.checkMultiProject[i].checked == false) {
			document.frmOPMultiProjectPart.Chk_selectAll.checked = false;			
		}
	}
}

function fnLoad()
{
	if(TRIM(document.frmOPMultiProjectPart.partNumbers.value) == ""){
		Error_Details(" Part Number Cannot be Empty. Please enter a Part Number and click on Load.");
		}	
		if (ErrorCount > 0)  
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress('Y'); 
	document.frmOPMultiProjectPart.haction.value = 'loadParts';
	document.frmOPMultiProjectPart.submit();
}

function fnSubmit()
{
	var actionval = document.frmOPMultiProjectPart.haction.value;
	
	document.frmOPMultiProjectPart.haction.value =  'save';
	
	var hiddenLength = document.frmOPMultiProjectPart.checkMultiProject.length;

	var inputString =''; 
	
	if(hiddenLength==undefined)
	{
		var hpnum = eval("document.frmOPMultiProjectPart.hpartnum0").value;
		if(document.frmOPMultiProjectPart.checkMultiProject.checked){
				inputString+=hpnum+'#'+'Y';	
			}else{			
				inputString+=hpnum+'#'+'N';
			}
	}else{
		for(var i=0;i<hiddenLength;i++){
			var hpnum = eval("document.frmOPMultiProjectPart.hpartnum"+i).value;
			if(document.frmOPMultiProjectPart.checkMultiProject[i].checked){
				inputString+=hpnum+'#'+'Y';	
			}else{			
				inputString+=hpnum+'#'+'N';
			}
			if(i!=hiddenLength-1){
				inputString+=',';
			}
		}
	}
	fnStartProgress('Y');
	document.frmOPMultiProjectPart.hinputString.value=inputString;
	document.frmOPMultiProjectPart.submit();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"">
<html:form action="/gmGOPMultiProjPartMap.do">

<html:hidden property="strOpt" />
<html:hidden property="haction" value="loadParts" />
<html:hidden property="hinputString" value=""/>

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">MultiProject Part Mapping</td>
			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> <!-- Question -->
				</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">Enable Sub Component:</td> 
                        <td>&nbsp;<html:checkbox property="enablesubcomponent" />
                         </td>
                    </tr>
                    <tr><td colspan="2" class="ELine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;Part Numbers :</td> 
                        <td>&nbsp;
	                         <html:text property="partNumbers"  size="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 
	                        &nbsp;&nbsp;&nbsp; 
	                        
	                         <gmjsp:dropdown controlName="search" SFFormName="frmOPMultiProjectPart" SFSeletedValue="search" SFValue="partNumberSearch" codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]" />	
	                        
	                        
	                        <gmjsp:button value="&nbsp;&nbsp;Load&nbsp;&nbsp;&nbsp;" gmClass="button" onClick="fnLoad();" buttonType="Load" />
                        </td>
                    </tr>       
                    <tr><td colspan="2" class="ELine"></td></tr>
                    
		<%
			if (rowsize > 1) {
		%>
		 <tr>
  			<td colspan="5" align="left"  ><input type="checkbox" name="Chk_selectAll" onClick="fnSelectAll();">Select All </td>
 		</tr> 
 		<%
			}
		%>
           <tr> 
            <td colspan="2">
            
            <display:table name="requestScope.frmOPMultiProjectPart.ldtPartNumberDetails" requestURI="/gmGOPMultiProjPartMap.do" 
			class="its" id="currentRowObject" 
			decorator="com.globus.prodmgmnt.displaytag.DTMultiProjectPartWrapper"> 
			<display:column property="MPRJFLG" title="Multi Project"  class="aligncenter" style="width:100px"/>
			 <display:column property="PNUM" title="Part Number" sortable="true" class="aligncenter" style="width:100px" /> 
			<display:column property="PDESC" title="Part Description" sortable="true" class="alignleft"/> 
			
			</display:table> 
			
		</td>
    </tr> 
    <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
                    	<td colspan="2" align="center">		                    
		                    <gmjsp:button value="Submit" gmClass="button" onClick="fnSubmit();" buttonType="Save" /> 
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
<script>

	if (document.frmOPMultiProjectPart.Chk_selectAll != undefined) {
		fnMasterCheckBox();
	}
	
</script>
</BODY>

</HTML>

