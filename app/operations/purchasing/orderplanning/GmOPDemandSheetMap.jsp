<%
/**********************************************************************************
 * File		 		: GmDemandSheetMap.jsp
 * Desc		 		: mapping Group / Set / Region / InHouse
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>
<HTML>
<HEAD>
<TITLE> Globus Medical: Demand Sheet Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
 <!-- \operations\purchasing\orderplanning\GmOPDemandSheetMap.jsp -->
<script>
function fnReset()
{
	
}	

function fnFetch()
{	
	document.frmOPDemandSheetMap.strOpt.value = "edit";
	document.frmOPDemandSheetMap.submit();
}

function fnSubmit()
{
	var val = document.frmOPDemandSheetMap.requestPeriod.value;
	var DStype = document.frmOPDemandSheetMap.demandSheetTypeId.value;
	if (val != "" && isNaN(val))
    	{   
    	  Error_Details(message[400]);
		 
   		}
   	if( DStype =='40023')
	{
		 Error_Details('This sheet cannot be associated with the type "Multi Part"');
	}	
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
		 
	document.frmOPDemandSheetMap.strOpt.value = 'save';
	document.frmOPDemandSheetMap.submit();
}

function fnVoid()
{
		document.frmOPDemandSheetMap.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmOPDemandSheetMap.hAction.value = "Load";
		document.frmOPDemandSheetMap.hTxnId.value = document.frmOPDemandSheetMap.refId.value
		document.frmOPDemandSheetMap.submit();
}

function fnCheckSelections()
{
	objCheckGSArrLen = 0;
	objCheckRIArrLen = 0;
	
	objCheckGSArr = document.frmOPDemandSheetMap.checkSelectedGS;
	if(objCheckGSArr) {
	if (objCheckGSArr.type == 'checkbox')
	{
		objCheckGSArr.checked = true;
	}
	else {
		objCheckGSArrLen = objCheckGSArr.length;
		for(i = 0; i < objCheckGSArrLen; i ++) 
			{	
				objCheckGSArr[i].checked = true;
			}
		}
	}
	
	objCheckRIArr = document.frmOPDemandSheetMap.checkSelectedRI;
	if (objCheckRIArr) {
		// check if there is just one element in checked. if so mark it as selected
	if (objCheckRIArr.type == 'checkbox')
	{
		objCheckRIArr.checked = true;
	}
	else {
		objCheckRIArrLen = objCheckRIArr.length;
		for(i = 0; i < objCheckRIArrLen; i ++) 
			{	
				objCheckRIArr[i].checked = true;
			}
		}
	}
	
	var DStype = document.frmOPDemandSheetMap.demandSheetTypeId.value;
	//alert(" test is " + test);
	if(DStype =='40020')
	{
		document.getElementById("spnGroupSet").innerHTML = "Group:";
	}else if (DStype =='40021'){
		document.getElementById("spnGroupSet").innerHTML = "Consignment Set:";
	}else if (DStype =='40022'){
		document.getElementById("spnGroupSet").innerHTML = "InHouse Set:";	
	}else{
		document.getElementById("spnGroupSet").innerHTML = "Group / Set:";
	}
}	

function fnClose(){
//window.opener.fnFetch();
window.close();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnCheckSelections();">
<html:form action="/gmOPDemandGroupMap.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="demandSheetId" />
<html:hidden property="demandSheetName" />
<html:hidden property="demandSheetTypeId" />

 
	<table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Demand Sheet Mapping</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("DEMAND_SHEET_MAPPING")%>');" /> 
				</td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="723" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="20%"></font>&nbsp;Demand	Sheet Name:</td>
						<td width="75%" colspan="2">&nbsp;
							<bean:write name="frmOPDemandSheetMap" property="demandSheetName" />
						</td>
					</tr>
                    <tr><td colspan="3" class="Line"></td></tr>
                    <tr>
                    	<td class="RightTableCaption" align="right" HEIGHT="30"  width="15%"></font>&nbsp;<span id="spnGroupSet"></span></td> 
                          <td>&nbsp;
                          <DIV style="display:visible;height: 160px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                         <table>
                        	<logic:iterate id="selectedGSlist" name="frmOPDemandSheetMap" property="alSelectedGS">
                        	<tr>
                        		<td> 
								    <logic:equal name="frmOPDemandSheetMap" property="demandSheetTypeId" value="40020">
								    	<bean:write name="selectedGSlist" property="NAME" />
								    </logic:equal>
								    <logic:equal name="frmOPDemandSheetMap" property="demandSheetTypeId" value="40021">
								    	<bean:write name="selectedGSlist" property="ID" />&nbsp;-&nbsp;<bean:write name="selectedGSlist" property="NAME" />
								    </logic:equal>
								    <logic:equal name="frmOPDemandSheetMap" property="demandSheetTypeId" value="40022">
								    	<bean:write name="selectedGSlist" property="ID" />&nbsp;-&nbsp;<bean:write name="selectedGSlist" property="NAME" />
								    </logic:equal>									    								    
	     						</td>
							</tr>	    
							</logic:iterate>
							</table>
							</DIV><BR>
                        </td>
                    </tr>  
                    <tr><td colspan="3" class="Line"></td></tr>
                    <tr>
                    	<td colspan="3" align="center" height="30">
							<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" onClick="fnClose();"  buttonType="Load" />
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

