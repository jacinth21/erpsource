<%
/**********************************************************************************
 * File		 		: GmOpenSheets.jsp
 * Desc		 		: This screen is used for displaying the Open Sheets 
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<!-- \operations\purchasing\orderplanning\GmOPOpenSheets.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<bean:define id="demandSheetMonthId" name="frmOPDemandSheetAjax" property="demandSheetMonthId" type="java.lang.String"></bean:define>
<bean:define id="alResult" name="frmOPDemandSheetAjax" property="alResult" type="java.util.ArrayList"></bean:define>
<bean:define id="strMsg" name="frmOPDemandSheetAjax" property="strMsg" type="java.lang.String"></bean:define>
<%
int rowsize=0;
if(alResult != null){
	rowsize = alResult.size();
}
%>
<input type="hidden" name="demandsheetmonthId" value="<%=demandSheetMonthId%>">
<BODY>
       <table class="border">
       
       
       <%if (rowsize > 1) {	%>
	   <tr>
  			<td colspan="5" align="left"  ><input type="checkbox" name="Chk_selectAll" onClick="fnPullSheetsSelectAll('<%=rowsize%>');" >Select All </td>
 	   </tr> 
 	   <%	}	%>
       <tr><td>
		    <display:table name="requestScope.frmOPDemandSheetAjax.alResult" class = "its"  id="currentRowObject"  decorator="com.globus.operations.purchasing.orderplanning.displaytag.DTOPOpenSheetsWrapper">
		        <display:column property="ID" title=" " style="width:50" />		     
				<display:column property="NAME" title=" Sheet Name" />		
				<display:column property="STATUS" title="Status" class="alignleftt"  sortable="true" />					
 			</display:table> 
 			</td> 			
 		</tr> 		
 		<logic:equal name="frmOPDemandSheetAjax" property="statusId" value="50550">
 		<tr><td class="Line" width="1"></td><tr> 
 		<tr> 		
 		<td align="center" height ="30">  			
 		  		<gmjsp:button name="pull" value="&nbsp;&nbsp;Pull Sheets&nbsp;&nbsp;" gmClass="button" onClick="fnPullOpenSheets(this.form);" buttonType="Save" /> 		    
 		  </td> 
 		</tr> 		
 		</logic:equal> 
 		<logic:equal name="frmOPDemandSheetAjax" property="strOpt" value="pullopensheets">		
	 		<tr><td class="Line" width="1"></td><tr> 
	 	   <tr><td align="center" height ="30" ><font size="1+" color="green">Demand Sheet are scheduled for Pull. Email will be send out upon successful completion of the pull.</font></td></tr>
	 		<logic:notEqual name="frmOPDemandSheetAjax" property="strMsg" value="">
		 	   <tr><td class="Line" width="1"></td><tr> 
		 	   <tr><td align="center" height ="30" ><font size="1+" color="red"><%=strMsg%></font></td></tr> 		
	 	   </logic:notEqual>
 		</logic:equal>
 	 </table>
 	 <%@ include file="/common/GmFooter.inc" %>
</BODY>
