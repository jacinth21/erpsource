<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--operations\purchasing\GmPurchasingClassificationReport.jsp-->
<!-- PMT-35390 - Purchasing ABC Ranking Report -->
<%@ page language="java" %>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtABCReportHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtABCReportHeader:setLocale value="<%=strLocale%>"/>
<fmtABCReportHeader:setBundle basename="properties.labels.operations.purchasing.GmPurchaseClassificationReport"/>
<bean:define id="projectName" name="frmPurchaseClassification" property="projectName" type="java.lang.String"> </bean:define>
<bean:define id="projectId" name="frmPurchaseClassification" property="projectId" type="java.lang.String"> </bean:define> 

<%
String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("PUR_CLASSIFICATION_RPT");
String strApplDateFmt = strGCompDateFmt;
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmPurchasingClassificationReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxGrid/codebase/ext/dhtmlxgrid_export.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxGrid/codebase/excells/dhtmlxgrid_excell_link.js"></script>

<style type="text/css">
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;;
}
div.gridbox table.hdr td {
   color: #000000 !important;
   background-color: #d2e9fc !important;
}
div.gridbox_material.gridbox .xhdr {
	background:#dcedfc !important;
}
</style>
</head>
<body leftmargin="20" topmargin="10"  onkeyup="fnEnter();">
<html:form action="/gmPurchaseClassificationRpt.do?method=loadPartClassificationRpt">
<table border="0" class=DtTable1200 cellspacing="0" cellpadding="0">

    <tr height="28">
			<td height="24" class="RightDashBoardHeader" colspan="8"><fmtABCReportHeader:message key="LBL_PUR_CLASSIFICATION_RPT"/></td>
			<td align="right" colspan="2" class=RightDashBoardHeader ><fmtABCReportHeader:message key="IMG_ALT_HELP" var = "varHelp"/> 	
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
	</tr>

	<tr height="28">
		<td height="24" class="RightTableCaption" align="right"><fmtABCReportHeader:message key="LBL_PART"/>:&nbsp;</td>
		<td colspan="3" ><html:text property="pnum" name="frmPurchaseClassification" tabindex="1" onfocus="changeBgColor(this,'#AACCE8');"></html:text>
		        <html:select property ="pnumSuffix" tabindex="2" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" >
				<html:option value="0" ><fmtABCReportHeader:message key="LBL_CHOOSEONE"/></html:option>
				<html:option value="LIT" ><fmtABCReportHeader:message key="LBL_LITERAL"/></html:option>
				<html:option value="LIKEPRE" ><fmtABCReportHeader:message key="LBL_LIKE_PREFIX"/></html:option>
				<html:option value="LIKESUF" ><fmtABCReportHeader:message key="LBL_LIKE_SUFFIX"/></html:option>
		</html:select>
		</td>
		
		<td height="24" class="RightTableCaption" align="right"><fmtABCReportHeader:message key="LBL_PROJECT"/>:</td>
		<td style="width: 200px;" style="padding-left: 2px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="projectId" />
				<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=LikeSearch" />
				<jsp:param name="WIDTH" value="250" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="TAB_INDEX" value="3"/>
				<jsp:param name="CONTROL_NM_VALUE" value="<%=projectName%>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=projectId%>" />
 				<jsp:param name="SHOW_DATA" value="100" />
 				<jsp:param name="AUTO_RELOAD" value="" />                                     
 	 	 	    </jsp:include>  
 	 	 	 </td>
	
		<td height="24" class="RightTableCaption" align="right"><fmtABCReportHeader:message key="LBL_DIVISION"/>:</td>
		<td>&nbsp;<gmjsp:dropdown controlName="divisionId" SFFormName="frmPurchaseClassification" SFSeletedValue="divisionId"
		SFValue="alDivision" codeId = "DIVISION_ID"  codeName = "DIVISION_NAME"  defaultValue= "[Choose One]" tabIndex="4"/></td>
	    <td height="24" class="RightTableCaption" align="right"><fmtABCReportHeader:message key="LBL_CLASSIFICATION"/>:</td>
		<td> &nbsp;<gmjsp:dropdown controlName="rankId" SFFormName="frmPurchaseClassification" SFSeletedValue="rankId"
		SFValue="alClassiffication" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="5"/></td>
	</tr>
	
	<tr><td colspan="10" class="LLine" height="1"></td></tr>
	
	<tr class="Shade" height="28"> 
		<td class="RightTableCaption"  align="right"  HEIGHT="25"><gmjsp:label type="MandatoryText" SFLblControlName="${varMonth}" td="false"/><fmtABCReportHeader:message key="LBL_MONTH"/>:&nbsp;</td>
		<td>
		<gmjsp:dropdown controlName="abcMonth"  SFFormName='frmPurchaseClassification' SFSeletedValue="abcMonth"
		SFValue="alMonth" codeId="CODENMALT" codeName="CODENM"  defaultValue= "[Choose One]" tabIndex="6"/></td>
		<td  class="RightTableCaption"  align="right"  HEIGHT="25"><gmjsp:label type="MandatoryText" SFLblControlName="${varYear}" td="false"/><fmtABCReportHeader:message key="LBL_YEAR"/>:&nbsp;</td>
		<td><gmjsp:dropdown controlName="abcYear"  SFFormName='frmPurchaseClassification' SFSeletedValue="abcYear"
		SFValue="alYear"	 codeId="CODENM" codeName="CODENM"  defaultValue= "[Choose One]" tabIndex="7" /></td>								
	    <td><fmtABCReportHeader:message key="BTN_LOAD" var="varLoad"/>
	     <gmjsp:button value="${varLoad}" gmClass="button" onClick="fnLoad();" buttonType="Load" tabindex="8"/></td>
         <td colspan="5"></td> 
	</tr> 
	 	  
	 <tr id="trDiv"  style="display: none">
	 <td  colspan="10">
	<div id="dataGridDiv"  class="" height="500px" width="1200px"></div>
	</td>
	</tr>
	   <tr style="display: none" id="exportOptions"><td  align="center" colspan="10">
            <div class='exportlinks'><fmtABCReportHeader:message key="LBL_EXPORT_OPTIONS" />:<img src='img/ico_file_excel.png'/>&nbsp;
			  <a href="#"onclick="fnDownloadXLS();" tabindex="9"><fmtABCReportHeader:message key="LBL_EXCEL"/></a>
			</div>
 	      </td>
       </tr>    
	   <tr style="display: none" id="NothingMsg" height="28">
	   		<td colspan="10" align="center">
	   			<div><fmtABCReportHeader:message key="NO_DATA_FOUND"/></div>
	   		</td>
	   </tr>
	</table>
</html:form>
</body>
</html>