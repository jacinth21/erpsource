<!-- \operations\GmPOReportVendorEdit.jsp -->

<%
	/**********************************************************************************
	 * File		 		: GmPOReportVendorEdit.jsp
	 * Desc		 		: This screen is used for Editing Vendor's Order
	 * Version	 		: 1.0
	 * author			: T.S Ramachandiran
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtPOReportVendorEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPOReportVendorEdit:setLocale value="<%=strLocale%>"/>
<fmtPOReportVendorEdit:setBundle basename="properties.labels.operations.purchasing.GmWOVendorCommitEdit"/>
<bean:define id="gridData" name="frmVendorCommit" property="gridData" type="java.lang.String"></bean:define>
<bean:define id="strAccessfl" name="frmVendorCommit"  property="strAccessfl" type="java.lang.String"></bean:define>
<bean:define id="hmReturn" name="frmVendorCommit"  property="hmReturn" type="java.util.HashMap"></bean:define>
<%
String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_COMMIT_TRANSACTION");
String strApplDateFmt = "";
GmCalenderOperations.setTimeZone(strGCompTimeZone);//Get time Zone
String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);

String strSubmitDisable="";
if(!strAccessfl.equals("Y")){
	strSubmitDisable = "true";
}	

	strApplDateFmt = strGCompDateFmt;
	HashMap hmPODetails = new HashMap();
	hmPODetails = (HashMap)hmReturn.get("PODETAILS");

	String strPoId = "";
	String strVendorNm = "";
	String strPoTypeNm = "";
	String strRaisedBy = "";
	String strPODate = "";
	String strRequiredDate = "";
	
	
	Date POdate = null;
	Date requiredDt = null;
	
	strPoId = GmCommonClass.parseNull((String)hmPODetails.get("POID"));
	strVendorNm = GmCommonClass.parseNull((String)hmPODetails.get("VNAME"));
	strPoTypeNm = GmCommonClass.parseNull((String)hmPODetails.get("POTYPENM"));
	strRaisedBy = GmCommonClass.parseNull((String)hmPODetails.get("CNAME"));
	POdate = (Date)hmPODetails.get("PODATE");
	requiredDt = (Date)hmPODetails.get("RQDATE");
	strPODate = GmCommonClass.getStringFromDate(POdate, strApplDateFmt);
	strRequiredDate = GmCommonClass.getStringFromDate(requiredDt, strApplDateFmt);
	
%>


<script>
var objGridData = '';
objGridData = '<%=gridData%>';
var POId = '<%=strPoId%>';
var today_dt = '<%=strTodaysDate%>';
var format = '<%=strApplDateFmt%>';

</script>


<HTML>
<HEAD><TITLE>Globus Medical: Vendor CommitEdit PO</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmWOVendorCommitEdit.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>

<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css">


</HEAD>


<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmVendorCommit.do?method=loadVendorCommitEdit">

<html:hidden property="inputString" value="" />
<html:hidden property="vendorId" value="" />
	
				<table border="0" class="DtTable700"  cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="3" class="RightDashBoardHeader"  height="24" width="780"><fmtPOReportVendorEdit:message key="LBL_VENDOR_COMMIT_EDIT"/></td>
					<td colspan="1" class="RightDashBoardHeader" align="right" width="20">
						<fmtPOReportVendorEdit:message key="IMG_ALT_HELP" var="varHelp"/>
						<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
					</tr>
			
				<tr><td bgcolor="#666666" colspan="4"></td></tr>
					<tr >
					<td colspan="1" class="RightTableCaption" height="24" align="Right" width="350"><fmtPOReportVendorEdit:message key="LBL_VPO_ID"/>&nbsp;:</td>
					 <td colspan="3" class="RightText">&nbsp;<%=strPoId%></td> 
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="4"></td>
				</tr>	
				<tr class="Shade">
					<td colspan="1" class="RightTableCaption" height="24" align="Right" width="350"><fmtPOReportVendorEdit:message key="LBL_VVENDOR"/>&nbsp;:</td>
					<td colspan="3" class="RightText">&nbsp;<%=strVendorNm%></td> 
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="4"></td>
				</tr>
				<tr>
					<td colspan="1" class="RightTableCaption" height="24" align="Right" width="350"><fmtPOReportVendorEdit:message key="LBL_VPO_TYPE"/>&nbsp;:</td>
				<td colspan="3" class="RightText">&nbsp;<%=strPoTypeNm%></td>
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="4"></td>
				</tr>
				<tr class="Shade"> 
					<td colspan="1" class="RightTableCaption" height="24" align="Right" width="350"><fmtPOReportVendorEdit:message key="LBL_VRAISED_BY"/>&nbsp;:</td>
					<td colspan="3" class="RightText">&nbsp;<%=strRaisedBy%></td> 
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="4"></td>
				</tr>
				<tr>
					<td colspan="1" class="RightTableCaption" height="24" align="Right" width="350"><fmtPOReportVendorEdit:message key="LBL_VPO_DATE"/>&nbsp;:</td>
					<td colspan="3" class="RightText">&nbsp;<%=strPODate%></td> 
				</tr>
				<tr>
					<td class="LLine" height="1" colspan="4"></td>
				</tr>
				<tr class="Shade">
					<td colspan="1" class="RightTableCaption" height="24" align="Right" width="350"><fmtPOReportVendorEdit:message key="LBL_VREQUIRED_DATE"/>&nbsp;:</td>
					<td colspan="3" class="RightText">&nbsp;<%=strRequiredDate%></td> 
				</tr>

			<tr>
				<td colspan="4">
					<div id="dataGridDiv" class="" height="450px"></div>
				</td>
			</tr>
		<tr>
		<td class="RightTableCaption" align="center" colspan="4"><fmtPOReportVendorEdit:message key="LBL_SUBMIT" var="varSubmit"/>
		<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" disabled="<%=strSubmitDisable%>" gmClass="button" onClick="fnSubmit();" buttonType="Save" />
		</td>
	</tr>
	 <tr>
	 <td class="LLine" height="1" colspan="4"></td> 
	</tr> 
	
	</table>

</html:form>

</BODY>

<%@ include file="/common/GmFooter.inc"%>

</HTML>

