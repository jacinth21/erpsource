<!-- \operations\purchasing\GmVendorInfoReport.jsp -->
<%
/**********************************************************************************
 * File		 		: GmVendorInfoReport
 * Desc		 		: This screen is used to display Vendor Information Report 
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<%@ page language="java" %> 

<%@ include file="/common/GmHeader.inc" %> 

<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>



<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

  <!-- \operations\purchasing\orderplanning\GmVendorInfoReport.jsp-->

<%@ taglib prefix="fmtVendorInfoReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtVendorInfoReport:setLocale value="<%=strLocale%>"/>
<fmtVendorInfoReport:setBundle basename="properties.labels.operations.purchasing.GmVendorInfoReport"/>


<% String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_INFO_REPORT"); %>


<bean:define id="month" property="month" name="frmVendorInfoReport" type="java.lang.String"></bean:define>
<bean:define id="gridData" name="frmVendorInfoReport" property="strXmlGridData" type="java.lang.String"></bean:define>

<%
  String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Vendor Information Report</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css"> 
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script> 
 <script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script> 
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmVendorInfoReport.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var objGridData = '';
objGridData = '<%=gridData%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:load();"> 
<html:form action="/gmOPVendorInfoReport.do">
<html:hidden property="strOpt" value="" />

<!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable1000" cellspacing="0"
		cellpadding="1">
		<tr>
			<td height=25 class=RightDashBoardHeader colspan=5><fmtVendorInfoReport:message key="LBL_VENDOR_INFORMATION_REPORT"/></td>
			<td align="right" class=RightDashBoardHeader><fmtVendorInfoReport:message key="IMG_HELP" var="varHelp"/><img id='imgEdit'
				style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
				height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
	<tr>
			<td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtVendorInfoReport:message key="LBL_VENDOR"/> :&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="vendorId"
						SFFormName="frmVendorInfoReport" SFSeletedValue="vendorId"
						SFValue="alVendorList" defaultValue="[Choose One]" codeId="ID"
						codeName="NAME" /></td>
						
		<td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtVendorInfoReport:message key="LBL_CURRENT_IMPLANTS"/> :&nbsp;</td>
			<td align="left" ><gmjsp:dropdown controlName="curImplantOpr"
						SFFormName="frmVendorInfoReport" SFSeletedValue="curImplantOpr"
						SFValue="alOperatorsList" defaultValue="[Choose One]" codeId="CODENMALT"
						codeName="CODENM" /></td>
			<td align="left" style="padding-left: 5px;"><html:text name="frmVendorInfoReport" property="curImplant" size="5" ></html:text></td>	
	</tr>
	<tr class="Shade">
	<td class="RightTableCaption" align="right" HEIGHT="24" ></font>&nbsp;<fmtVendorInfoReport:message key="LBL_LOCATION"/>:&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="location"
						SFFormName="frmVendorInfoReport" SFSeletedValue="location"
						SFValue="alLocationList" defaultValue="[Choose One]" codeId="CODEID"
						codeName="CODENM" /></td>
	<td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtVendorInfoReport:message key="LBL_CURRRENT_INSTRUMENTS"/> :&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="curInsOpr"
						SFFormName="frmVendorInfoReport" SFSeletedValue="curInsOpr"
						SFValue="alOperatorsList" defaultValue="[Choose One]" codeId="CODENMALT"
						codeName="CODENM" /></td>
			<td align="left" colspan="2">&nbsp;<html:text name="frmVendorInfoReport" property="curIns" size="5"></html:text></td>	
	</tr>
	<tr>
	<td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp; <fmtVendorInfoReport:message key="LBL_DURATION"/> :&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="month"
						SFFormName="frmVendorInfoReport" SFSeletedValue="month"
						SFValue="alMonthList" codeId="CODENM"
						codeName="CODENM" /> &nbsp; <b> <fmtVendorInfoReport:message key="LBL_MONTHS"/></b> </td>
	<td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtVendorInfoReport:message key="LBL_OF_CORE_SET_IMPLANTS"/> :&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="coreImplantOpr"
						SFFormName="frmVendorInfoReport" SFSeletedValue="coreImplantOpr"
						SFValue="alOperatorsList" defaultValue="[Choose One]" codeId="CODENMALT"
						codeName="CODENM" /></td>
			<td align="left">&nbsp;<html:text name="frmVendorInfoReport" property="coreImplant" size="5"></html:text></td>	
			 <td align="center" valign="middle" style="padding-left: 2px; padding-right: 4px;">&nbsp;&nbsp;
		 	 <fmtVendorInfoReport:message key="BTN_GO" var="varGo"/>
		 	 	<gmjsp:button value="&nbsp;${varGo}&nbsp;"  gmClass="button" onClick="javascript:fnGo();" name="go" buttonType="Load" />&nbsp;&nbsp;
		 	 </td>
	</tr>
	<tr class="Shade">
	<td  class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtVendorInfoReport:message key="LBL_PROJECT"/> :&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="projectId"
						SFFormName="frmVendorInfoReport" SFSeletedValue="projectId"
						SFValue="alProjectList" defaultValue="[Choose One]" codeId="ID"
						codeName="NAME" /></td>
	<td class="RightTableCaption" align="right" HEIGHT="24" colspan="1" width="350px"></font>&nbsp;<fmtVendorInfoReport:message key="LBL_OF_CORE_SET_INSTRUMENTS"/>:&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="coreInsOpr"
						SFFormName="frmVendorInfoReport" SFSeletedValue="coreInsOpr"
						SFValue="alOperatorsList" defaultValue="[Choose One]" codeId="CODENMALT"
						codeName="CODENM" /></td>
			<td align="left" colspan="2">&nbsp;<html:text name="frmVendorInfoReport" property="coreIns" size="5"></html:text></td>	
	</tr>
	<tr>
	<td class="RightTableCaption" align="right" HEIGHT="24" width="70px"></font>&nbsp;<fmtVendorInfoReport:message key="LBL_PO_TYPE"/> :&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="poType"
						SFFormName="frmVendorInfoReport" SFSeletedValue="poType"
						SFValue="alPOTypeList" defaultValue="[Choose One]" codeId="CODEID"
						codeName="CODENM" /></td>
	<td class="RightTableCaption" align="right" HEIGHT="24" width="350px"></font>&nbsp;<fmtVendorInfoReport:message key="LBL_OF_NON_CORE_SET_IMPLANTS"/> :&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="ncoreImplantOpr"
						SFFormName="frmVendorInfoReport" SFSeletedValue="ncoreImplantOpr"
						SFValue="alOperatorsList" defaultValue="[Choose One]" codeId="CODENMALT"
						codeName="CODENM" /></td>
			<td align="left" >&nbsp;<html:text name="frmVendorInfoReport" property="ncoreImplant" size="5"></html:text></td>	
	</tr>
	<tr class="Shade">
			<td class="RightTableCaption" align="right" HEIGHT="24" width="350px" colspan="3"></font>&nbsp;<fmtVendorInfoReport:message key="LBL_OF_NON_CORE_SET_INSTRUMENTS"/> :&nbsp;</td>
			<td align="left"><gmjsp:dropdown controlName="ncoreInsOpr"
						SFFormName="frmVendorInfoReport" SFSeletedValue="ncoreInsOpr"
						SFValue="alOperatorsList" defaultValue="[Choose One]" codeId="CODENMALT"
						codeName="CODENM" /></td>
			<td align="left" colspan="2">&nbsp;<html:text name="frmVendorInfoReport" property="ncoreIns" size="5"></html:text></td>
			
		 </tr>
		 <tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
				<%
				if (gridData.indexOf("cell") != -1) {
			%>
		 <tr>
				<td colspan="6">
					<div id="dataGridDiv" class="" height="450px" width="1067px"></div>
				</td>
		</tr>
		<tr>						
				<td align="center" colspan="6"><br>
					
	             	<div class='exportlinks'><fmtVendorInfoReport:message key="LBL_EXPORT_OPTIONS"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnDownloadXLS();"><fmtVendorInfoReport:message key="LBL_EXCEL"/>  </a></div>                         
	       		</td>
       	</tr>
       	<% 
			}else{
				%>
				<tr>
					<td colspan="6" HEIGHT="24"><fmtVendorInfoReport:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/>.</td>
				</tr>					
							<%		
						}
		%>
		</table>
	</html:form>
			<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
