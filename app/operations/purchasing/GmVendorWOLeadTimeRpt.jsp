<!-- \operations\Purchasing\GmVendorWOLeadTimeRpt.jsp  -->
<%
	/*************************************************************************************
	 * File		 		: GmVendorWOLeadTimeRpt.jsp
	 * Desc		 		: This screen is used to show work order lead time based on vendor 
	 * Version	 		: 1.0
	 * author			:
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtVendorWOLeadTimeRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtVendorWOLeadTimeRpt:setLocale value="<%=strLocale%>"/>
<fmtVendorWOLeadTimeRpt:setBundle basename="properties.labels.operations.purchasing.GmVendorWOLeadTimeRpt"/>

<bean:define id="projectID" name="frmVendorWOLeadTimeRpt" property="projectID" type="java.lang.String"></bean:define>
<bean:define id="vendorId" name="frmVendorWOLeadTimeRpt" property="vendorId" type="java.lang.String"></bean:define>
<bean:define id="searchprojectID" name="frmVendorWOLeadTimeRpt" property="searchprojectID" type="java.lang.String"></bean:define>
<bean:define id="searchvendorId" name="frmVendorWOLeadTimeRpt" property="searchvendorId" type="java.lang.String"></bean:define>

<%
String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_WO_LEAD_TIME_REPORT");
String strApplDateFmt = strGCompDateFmt; 
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Part Lead Time Report </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmVendorWOLeadTimeRpt.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmVendorWOLaedTimeRpt.do?method=loadVendorWOLeadTimeRpt">
<table border="0"  height="60" class=DtTable1500 cellspacing="0" cellpadding="0">
	<tr height="25">
		<td class="RightDashBoardHeader" colspan="6" ><fmtVendorWOLeadTimeRpt:message key="LBL_PART_TIME_LEAD_RPT"/></td>
		<td colspan = "3" class="RightDashBoardHeader" align="right"><fmtVendorWOLeadTimeRpt:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/></td>
	</tr>

	<tr height="28">
		<td class="RightTableCaption" colspan="1" width="200" align="right"><fmtVendorWOLeadTimeRpt:message key="LBL_VENDOR_NAME"/>:</td>
		<td class="RightTableCaption" colspan="5">
			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
			<jsp:param name="CONTROL_NAME" value="vendorId" />
			<jsp:param name="METHOD_LOAD" value="loadVendorNameList&searchType=LikeSearch" />
			<jsp:param name="WIDTH" value="300" />
			<jsp:param name="CSS_CLASS" value="search" />
			<jsp:param name="TAB_INDEX" value="1"/>
			<jsp:param name="CONTROL_NM_VALUE" value="<%=searchvendorId%>" /> 
			<jsp:param name="CONTROL_ID_VALUE" value="<%=vendorId%>" />
 			<jsp:param name="SHOW_DATA" value="100" />
 			<jsp:param name="AUTO_RELOAD" value="" />                                       
 	 	    </jsp:include>  
		</td> 
		<td class="RightTableCaption" colspan="2" align="right"><fmtVendorWOLeadTimeRpt:message key="LBL_PART_NUMBER"/>:&nbsp; </td>
		<td class="RightText" id="partNum" > 
		   <html:text property="partNums" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="2"/>&nbsp;&nbsp; 			
	       <gmjsp:dropdown controlName="strPartSearch" SFFormName="frmVendorWOLeadTimeRpt" SFSeletedValue="strPartSearch"
				SFValue="alPartSearch" codeId = "CODENMALT"  codeName = "CODENM" defaultValue= "[Choose One]" />
		</td>			 	
	</tr>
	<tr><td class="LLine" colspan="9" height="1"></td></tr>	
	
	<tr class="oddshade" height="28">
		<td class="RightTableCaption" align="right" HEIGHT="24" colspan="1"><fmtVendorWOLeadTimeRpt:message key="LBL_PURCHASE_AGENT"/>:&nbsp;</td>
		<td colspan="5"><gmjsp:dropdown controlName="strPurAgent"  SFFormName="frmVendorWOLeadTimeRpt" SFSeletedValue="strPurAgent"
			 SFValue="alPurchaseAgent" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" />
		</td>
		<td class="RightTableCaption" align="right" HEIGHT="24" colspan="1"><fmtVendorWOLeadTimeRpt:message key="LBL_DIVISION"/>:&nbsp;</td>
		<td colspan="2"><gmjsp:dropdown controlName="strDivision"  SFFormName="frmVendorWOLeadTimeRpt" SFSeletedValue="strDivision"
				SFValue="alDivision" codeId = "DIVISION_ID"  codeName = "DIVISION_NAME"  defaultValue= "[Choose One]" />
	</tr>
	<tr><td class="LLine" colspan="9" height="1"></td></tr>
	
	<tr height="28">
		<td class="RightTableCaption" width="165" colspan="1" align="right"><fmtVendorWOLeadTimeRpt:message key="LBL_PROJECT_LIST"/>:</td>
		<td valign="middle" colspan="5" align="left">
			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
			<jsp:param name="CONTROL_NAME" value="projectID" />
			<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=LikeSearch&searchBy=ProjectId" />
			<jsp:param name="WIDTH" value="300" />
			<jsp:param name="CSS_CLASS" value="search" />
			<jsp:param name="TAB_INDEX" value="6"/>
			<jsp:param name="CONTROL_NM_VALUE" value="<%=searchprojectID%>" /> 
			<jsp:param name="CONTROL_ID_VALUE" value="<%=projectID%>" />
			<jsp:param name="SHOW_DATA" value="100" />
			<jsp:param name="AUTO_RELOAD" value="" />					
			</jsp:include>
		</td>
		<td colspan="3" class="RightTableCaption" style="padding-left:60px;">		
		 	<fmtVendorWOLeadTimeRpt:message key="BTN_LOAD" var="varLoad"/>
			<gmjsp:button value="${varLoad}" gmClass="button" onClick="javascript:fnReportLoad();" tabindex="7" buttonType="Load"/> 	
		</td>
	</tr>
	
	<tr id="trDiv"  style="display: none">
		<td colspan="9"><div  id="dataGridDiv" height="400px"></div></td>
	</tr>
	
	<tr style="display:none;" id="ExcelExport">						
		<td align="center" colspan=9><br>
             <div class='exportlinks'><fmtVendorWOLeadTimeRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp; 
				<a href="#" onclick="fnExport();"> <fmtVendorWOLeadTimeRpt:message key="LBL_EXCEL"/> </a></div>                         
        </td>
    </tr>
    
	<tr><td colspan="9" height="1" class="LLine"></td></tr>
	
	<tr style="display:none;" id="NothingFound">
		<td colspan="9" height="25" align="center" class="RightText"><fmtVendorWOLeadTimeRpt:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/>
		</td>
	</tr>
	
</table>
</html:form> 
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>