<!-- \operations\purchasing\GmTrendReport.jsp -->
<%
/**********************************************************************************
 * File		 		: GmTrendReport.jsp
 * Desc		 		: This screen is used to display Trend Report
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>
<%@ page language="java" %>
 <!-- \operations\purchasing\orderplanning\GmTrendReport.jsp--> 

 <%@page import="java.net.URLEncoder"%>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>
 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ include file="/common/GmHeader.inc" %> 


<%@ taglib prefix="fmtTrendReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtTrendReport:setLocale value="<%=strLocale%>"/>
<fmtTrendReport:setBundle basename="properties.labels.operations.purchasing.orderplanning.GmTrendReport"/>

<!-- Imports for Logger -->
 
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>
 
<bean:define id="hmTrendSumm" name="frmLogisticsReport" property="hmCrossTabReport" type="java.util.HashMap"> </bean:define> 
<bean:define id="backorderCheck" name="frmLogisticsReport" property="backorderCheck" type="java.lang.String"></bean:define>

<%
	HashMap hmReturn = new HashMap();
	ArrayList alDemandHeader = new ArrayList();
	ArrayList alForecastHeader = new ArrayList(); 
	ArrayList alCurrentHeader = new ArrayList(); 
	ArrayList alPdetail = new ArrayList();
	String strWikiTitle = GmCommonClass.getWikiTitle("TREND_REPORT");
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	// Added for the PMT-18670 by suganthi
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	String strCompDtFmt = gmDataStoreVO.getCmpdfmt();
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\",\"cmpdfmt\":\""+strCompDtFmt+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	// Added for the PMT-18670 by suganthi
	
	hmReturn = GmCommonClass.parseNullHashMap((HashMap)hmTrendSumm.get("Main"));
	  
	alForecastHeader = GmCommonClass.parseNullArrayList((ArrayList)hmTrendSumm.get("ForecastHeader"));
	log.debug("text alForecastHeader here is" + alForecastHeader);

	alDemandHeader = GmCommonClass.parseNullArrayList((ArrayList)hmTrendSumm.get("DemandHeader"));
	alCurrentHeader = GmCommonClass.parseNullArrayList((ArrayList)hmTrendSumm.get("CurrentHeader"));
	alPdetail = GmCommonClass.parseNullArrayList((ArrayList)hmTrendSumm.get("Pdetail"));
	log.debug("alPdetail is " +alPdetail.size());
	 
	
	gmCrossTab.setGeneralHeaderStyle("aaTopHeader");
	gmCrossTab.setTotalRequired(false);
	gmCrossTab.setColumnTotalRequired(false);
	gmCrossTab.setNoDivRequired(true);
	 
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setLinkRequired(false);	
	gmCrossTab.setHideColumn("Name");
	
	
	gmCrossTab.setColumnWidth("Description",250);
	
	gmCrossTab.setColumnDivider(false);
	gmCrossTab.setColumnLineStyle("borderDark");
	
	gmCrossTab.addLine("Description");
	gmCrossTab.addLine("Current");
	gmCrossTab.addLine("Average");
	gmCrossTab.addLine("On PO");
	gmCrossTab.addLine("RW QTY");
	gmCrossTab.addLine("On DHR");
	gmCrossTab.addLine("OTHER");
	
	// Added for zero value
	gmCrossTab.setValueType(0);		
	
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD"); 
	gmCrossTab.addStyle("Description","ShadeMedGrayTD","ShadeDarkGrayTD");
	
	gmCrossTab.addStyle("Total Demand","ShadeMedGreenTD","ShadeDarkGreenTD") ;
	gmCrossTab.addStyle("Average","ShadeLightGreenTD","ShadeDarkGreenTD") ;
	gmCrossTab.addStyle("Shelf","ShadeMedYellowTD","ShadeDarkYellowTD") ;
	gmCrossTab.addStyle("On PO","ShadeMedBrownTD","ShadeDarkBrownTD") ;
	gmCrossTab.addStyle("BO","ShadeMedBlueTD","ShadeDarkBlueTD") ;
	gmCrossTab.addStyle("OTHER","ShadeMedOrangeTD","ShadeDarkOrangeTD");
	gmCrossTab.addStyle("On DHR","ShadeLightYellowTD","ShadeDarkYellowTD");
	gmCrossTab.addStyle("US Average","ShadeMedGreenTD","ShadeDarkGreenTD");
	gmCrossTab.addStyle("OUS Average","ShadeLightGreenTD","ShadeLightGreenTD");
	
	for(int i=0; i< alForecastHeader.size(); i++){
		 
		gmCrossTab.addStyle((String)alForecastHeader.get(i),"ShadeMedOrangeTD","ShadeDarkOrangeTD") ;
		 
	}
	
	for(int i=0; i< alDemandHeader.size(); i++){
		log.debug("alDemandHeader is " +(String)alDemandHeader.get(i));
		gmCrossTab.addStyle((String)alDemandHeader.get(i),"ShadeMedBrownTD","ShadeDarkBrownTD") ;
		if( i < alDemandHeader.size()-3)
			gmCrossTab.addLine((String)alDemandHeader.get(i+3));
		
	}
	for(int i=0; i< alCurrentHeader.size(); i++){
		 
		gmCrossTab.addStyle((String)alCurrentHeader.get(i),"ShadeLightBlueTD","ShadeDarkBlueTD") ;
	}
	 gmCrossTab.setDecorator("com.globus.crosstab.beans.GmTrendDecorator");
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Trend -- Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
 
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnGo();
		}
}


function fnDownloadExcel()
{
	document.frmLogisticsReport.strOpt.value="reload";
	document.frmLogisticsReport.hExcel.value="Excel";
	document.frmLogisticsReport.action = "/gmLogisticsReport.do?method=loadTrendReport"; 
	document.frmLogisticsReport.submit();
}
function fnGo()
 {
  	 
   
  fnCheckSelections();
   
  var varpartNum = TRIM(document.frmLogisticsReport.partNum.value);
  var varSetID = TRIM(document.frmLogisticsReport.setNumSearch.value);
  var varsalesGrpId = document.frmLogisticsReport.salesGrpId.value;
  var varsalesGrpType = document.frmLogisticsReport.salesGrpType.value;
  var vartrendType = document.frmLogisticsReport.trendType.value;
  
  varpartNum = varpartNum.replace(/\s/g, '');  // to remove space from the part number
  
  if (varpartNum.length >= 1 && varpartNum.length < 3) 
  {
  Error_Details (message_operations[523]);
  Error_Show();
  Error_Clear();
  return false;
  }

  
  if(varSetID =='' && varpartNum == '' && varsalesGrpId== '0' || varsalesGrpType== '0' || varsalesGrpType== '0' ){
  Error_Details(message_operations[524]);
  }
  if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}	 
  document.frmLogisticsReport.hExcel.value="";
  document.frmLogisticsReport.strOpt.value="reload";
  fnStartProgress('Y');  
  document.frmLogisticsReport.submit();
 }
 
   
 
function fnCheckSelections()
{
	objCheckTrendArrLen = 0;
	 
	conflag = '';
	
	objCheckTrendArr = document.frmLogisticsReport.checkSelectedTrend;
	if(objCheckTrendArr) {
	 
		objCheckTrendArrLen = objCheckTrendArr.length;
		for(i = 0; i < objCheckTrendArrLen; i ++) 
			{	
			    if(objCheckTrendArr[i].checked)
			    {				    		   
				conflag = '1';
			//	alert(" loopCons" + conflag);				
				}
			}
	 
	}
	
	 
	if(conflag=='')
	{
	Error_Details(message_operations[525]);
	Error_Show();
	Error_Clear();
}	
}
function fnCallInv(pnum)
		{
			windowOpener("/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hPartNum="+encodeURIComponent(pnum),"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=1010,height=300");
		}
		
function fnCallPO(pnum)
		{
	 /* Added for the PMT-18670 by suganthi */
	var companyInfoObj = '<%=strCompanyInfo%>';
			//windowOpener("/GmPOReportServlet?hAction=Go&hOpt=PendVend&Cbo_ProjId=0&Cbo_Search=0&hVendorId=0&hPartNum="+pnum,"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=1110,height=300");// Added for the PMT-18670 by suganthi
	windowOpener("/GmPOReportServlet?hAction=Go&hOpt=PendVend&Cbo_ProjId=0&Cbo_Search=0&hVendorId=0&hPartNum="+encodeURIComponent(pnum)+"&companyInfo="+companyInfoObj,"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=1110,height=300");
			
		}
 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeyup="javascript:fnEnter();" >
<html:form action="/gmLogisticsReport.do?method=loadTrendReport"  >  
<html:hidden property="strOpt" value=""/>
<html:hidden property="hExcel" value=""/>
 

<jsp:include page="/common/GmScreenError.jsp" />

<!-- Custom tag lib code modified for JBOSS migration changes -->
<!-- Struts tag lib code modified for JBOSS migration changes -->


<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
 	  
                 
	<tr class=Line >
	<td height=25 class=RightDashBoardHeader colspan=5><fmtTrendReport:message key="LBL_TREND_REPORT"/></td>
			
			<td align="right" class=RightDashBoardHeader > 	
			<fmtTrendReport:message key="IMG_ALT_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
	</tr>
	<tr class=Line><td noWrap height=1 colspan=6></td></tr>
					 
					<tr>
					
				 
                    	<td class="RightTableCaption" align="right"   HEIGHT="24" ></font>&nbsp;<fmtTrendReport:message key="LBL_CALCUALTE_TREND_FOR"/>:</td> 
                        <td  >&nbsp;
                        <DIV style="display:visible;height: 100px; overflow: auto; border-width:1px; border-style:solid; margin-left:8px; margin-right:8px;" >
                        <table>
                        	<logic:iterate id="SelectedTrendlist" name="frmLogisticsReport" property="alTrendStatus">
                        	<tr>
                        		<td>
							    	<htmlel:multibox property="checkSelectedTrend" value="${SelectedTrendlist.CODEID}" />
								    <bean:write name="SelectedTrendlist" property="CODENM" />
								</td>
							</tr>	    
							</logic:iterate>
						</table>
						</DIV><BR>
                        </td>
                                           
					 	
					 		
						<td><table><tr>
							 
			    		   
							<td class="RightTableCaption" align="right"  HEIGHT="24"  width="170"> <fmtTrendReport:message key="LBL_PART_#"/>:&nbsp; </td>						
						 <td  ><html:text property="partNum"  size="40" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;</td>
			    		 <!--     
						   <td class="RightTableCaption" align="left"  HEIGHT="24" >   <gmjsp:dropdown controlName="search" SFFormName="frmLogisticsReport" SFSeletedValue="search"
							SFValue="alSearch" codeId = "CODENMALT"  codeName = "CODENM"     defaultValue= "[Choose One]" /></td>	-->   		
						</tr>
						
						<tr>
							 
			    		   
							<td class="RightTableCaption" align="right"  HEIGHT="24"  width="170"><fmtTrendReport:message key="LBL_SET_ID"/> :&nbsp; </td>						
						 <td  ><html:text property="setNumSearch"  size="40" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;</td>
			    		</tr>
						
						<tr>
							 
							<td class="RightTableCaption" align="right"  HEIGHT="24" width="170"> <fmtTrendReport:message key="LBL_PROJECT_GROUP_NAME"/>:&nbsp;</td>
							 <td  >  <gmjsp:dropdown controlName="salesGrpId" SFFormName="frmLogisticsReport" SFSeletedValue="salesGrpId"
							SFValue="alSalesGrpId" codeId = "ID"  codeName = "NAME"     defaultValue= "[Choose One]" />	 
							    <gmjsp:dropdown controlName="salesGrpType" SFFormName="frmLogisticsReport" SFSeletedValue="salesGrpType"
							SFValue="alSalesGrpType" codeId = "CODEID"  codeName = "CODENM"        /></td>
						</tr>
						
						
						
						<tr>
							<td class="RightTableCaption" align="right"  HEIGHT="24" width="170"><fmtTrendReport:message key="LBL_TREND_TYPE"/>:&nbsp;</td>
							<td class="RightTableCaption" align="left"  HEIGHT="24" >   <gmjsp:dropdown controlName="trendType" SFFormName="frmLogisticsReport" SFSeletedValue="trendType"
							SFValue="alTrendType" codeId = "CODEID"  codeName = "CODENM"      /> 
							 &nbsp;  &nbsp;
							  &nbsp;<html:checkbox  property="backorderCheck" /> &nbsp;<fmtTrendReport:message key="LBL_ONLY_SHOW_BACK_ORDER"/>
							 </td>					
						</tr>
						
						<tr>
							<td class="RightTableCaption" align="right"  HEIGHT="24" width="170"><fmtTrendReport:message key="LBL_DURATION"/> :&nbsp;</td>
							<td class="RightTableCaption" align="left"  HEIGHT="24" >   <gmjsp:dropdown controlName="trendDuration" SFFormName="frmLogisticsReport" SFSeletedValue="trendDuration"
							SFValue="alTrendDuration" codeId = "CODEID"  codeName = "CODENM"      /> &nbsp;<fmtTrendReport:message key="LBL_PERIOD"/>: &nbsp;<html:text property="trendPeriod"  size="5" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
							 
							 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							 <fmtTrendReport:message key="LBL_LOAD" var="varLoad"/>
			    		  	   <gmjsp:button value="&nbsp;${varLoad}&nbsp;" onClick="javascript:fnGo();"  gmClass="button" buttonType="Load" /></td>					
						</tr> 
						
						
						</table></td>
						
						 
					</tr>				
			 
	 
	 	 <% if  (alPdetail.size() != 0) { %>
	
	<tr><td colspan="6" class="Line"></td></tr>
	 				 	<tr><td colspan="6" class="ELine"></td></tr>
 			<tr>
				<td colspan ="6" align=center> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
			</tr>
     <tr>
			<td width="100%" align="center" colspan="6">  
			<fmtTrendReport:message key="LBL_EXCEL_DOWNLOAD" var="varExcelDownload"/>											
					<gmjsp:button  value="${varExcelDownload}" gmClass="button" 
					onClick="javascript:fnDownloadExcel();" buttonType="Load" />	
			</td>						
	</tr>
 	  <%} %>

</table>

</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
