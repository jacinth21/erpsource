
<%
/**********************************************************************************
 * File		 		: GmVendorPriceSetup.jsp
 * Desc		 		: This screen is used for the  Vendor Price Bulk Setup copy/paste functionality 
 * Version	 		: 1.0
 * author			: Anilkumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtVendorPricingSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \operations\purchasing\GmVendorPricingSetup.jsp -->
<fmtVendorPricingSetup:setLocale value="<%=strLocale%>"/>
<fmtVendorPricingSetup:setBundle basename="properties.labels.operations.purchasing.GmVendorPricingSetup"/>








<bean:define id="strhAction" name="frmVendorPricingSetup" property="haction" type="java.lang.String"> </bean:define>
<bean:define id="strXmlStringData" name="frmVendorPricingSetup" property="xmlString" type="java.lang.String"> </bean:define>
<bean:define id="strProjectId" name="frmVendorPricingSetup" property="projectId" type="java.lang.String"> </bean:define>
<bean:define id="strSubmitAccFl" name="frmVendorPricingSetup" property="submitAccFl" type="java.lang.String"> </bean:define>
<bean:define id="alAllVendorList" name="frmVendorPricingSetup" property="alAllVendorList" type="java.util.ArrayList"> </bean:define>
<bean:define id="alUOMList" name="frmVendorPricingSetup" property="alUOMList" type="java.util.ArrayList"> </bean:define>
<bean:define id="alPartsList" name="frmVendorPricingSetup" property="alPartsList" type="java.util.ArrayList"> </bean:define>
<bean:define id="strSearch" name="frmVendorPricingSetup" property="search" type="java.lang.String"> </bean:define>
<bean:define id="strLocked" name="frmVendorPricingSetup" property="locked" type="java.lang.String"> </bean:define>
<bean:define id="strActive" name="frmVendorPricingSetup" property="active" type="java.lang.String"> </bean:define>
<bean:define id="strMsgFl" name="frmVendorPricingSetup" property="msgFl" type="java.lang.String"> </bean:define>
<bean:define id="strMaxCount" name="frmVendorPricingSetup" property="maxCount" type="java.lang.Integer"> </bean:define> 






<%
	String strOperationsPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_PURCHASING");
	String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_PRICING_SETUP");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
    
	HashMap hmVend = new HashMap();
	HashMap hActVend = new HashMap();
	HashMap hInActVend = new HashMap();
	String  strCompanyId = gmDataStoreVO.getCmpid();
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Vendor Pricing Setup </TITLE>

<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/sources/dhtmlxGrid/codebase/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/skins/skyblue/dhtmlx.css">


<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script> 

<script language="JavaScript" src="<%=strOperationsPurchasingJsPath%>/GmVendorPricingSetup.js"></script>

<script>
var objGridData = '<%=strXmlStringData%>';
var selLocked = '<%=strLocked%>';  
var selActive = '<%=strActive%>';
var selSearch = '<%=strSearch%>';
var msgFl = '<%=strMsgFl%>';
var maxCount = '<%=strMaxCount%>'; 
</script>

<script language="JavaScript">

	var actVendLen = <%=alAllVendorList.size()%>;
	
	<%
	hActVend = new HashMap();
	int j=0;
	for (int i=0,k=0;i<alAllVendorList.size();i++)
	{
		hActVend = (HashMap)alAllVendorList.get(i);
		String strActFl = GmCommonClass.parseNull((String)hActVend.get("ACTFL"));
		if(!strActFl.equals("N")){
	%>
		var actVendArr<%=j%> ="<%=hActVend.get("ID")%>^<%=hActVend.get("NAME")%>";
		<%
		j++;
		}
		%>		
		var allVendArr<%=k%> ="<%=hActVend.get("ID")%>^<%=hActVend.get("NAME")%>";			
		<% k++; 
	}%>	
	var actVendArrLen = <%=j%>;
	
</script>

<style>
.dhx_combo_select{
height:120px;
}
#projectId{
        width:370px !important;
}
#vendorId{
        width:370px !important;
}
</style>
<style type="text/css">
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;;
}
		
div.gridbox table.hdr td {
   color: #000000 !important;
   background-color: #d2e9fc !important;
}

div.gridbox_material.gridbox .xhdr {
	background:#dcedfc !important;
}

</style>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeypress="fnEnter();">
<html:form action="/gmVendorPricingSetup.do">
<html:hidden property="strOpt" name="frmVendorPricingSetup" value=""/>
<html:hidden property="inputString" />
<html:hidden property="msgFl"  value="<%=strMsgFl%>"/>
<html:hidden property="haction" value="<%=strhAction%>"/>
<html:hidden property="companyId" value="<%=strCompanyId%>"/>

<table border="0" class="DtTable1300" cellspacing="0" cellpadding="0">		
		<tr class="shade" style="width:100%">
						<td height="25" class="RightDashBoardHeader" colspan="5">&nbsp;<fmtVendorPricingSetup:message key="LBL_VENDOR_PRICING_SETUP"/></td>
						<td  height="25" class="RightDashBoardHeader">
						<fmtVendorPricingSetup:message key="IMG_HELP" var="varHelp"/>
					<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
		</tr>
</table>	
<table border="0"  class="DtTable1300" cellspacing="0" cellpadding="0">	
		<tr>
		<td colspan="5">				
			<table  border="0" width="100%" cellspacing="0" cellpadding="0">
								
				<tr>
			  		<td class="RightTableCaption" align="right" height="28" ><font color="red">*</font>&nbsp;<fmtVendorPricingSetup:message key="LBL_PROJECT_NAME"/>:&nbsp;</td>					
					<td width="330"><gmjsp:dropdown controlName="projectId" SFFormName="frmVendorPricingSetup" SFSeletedValue="projectId" 
							SFValue="alProjectList" codeId="ID"  codeName="NAME"  defaultValue="[Choose One]" width="320" tabIndex="1" />
					</td>
			    </tr>
				<tr>
					<td height="1" colspan="5" class="LLine"></td>
				</tr>
				<tr class="shade">
					<td class="RightTableCaption" align="right" height="28" ><fmtVendorPricingSetup:message key="LBL_VENDOR"/>:&nbsp;</td>
				    <td width="330"><gmjsp:dropdown controlName="vendorId"  SFFormName="frmVendorPricingSetup" SFSeletedValue="vendorId" SFValue="alAllVendorList"
					       codeId="ID"  codeName="NAME" defaultValue="[Choose One]"  width="320" tabIndex="2"/>
			        </td>
			        
			        <td class="RightTableCaption" align="left" height="28">&nbsp;<html:checkbox property="showInactiveVendor" onclick="fnShowInactVend(this); " tabindex="3"/>
					&nbsp;<fmtVendorPricingSetup:message key="LBL_SHOW_INACTIVE_VENDORS"/></td> 
                         
					<td align="left" colspan="2" height="28" width="350">
					<fmtVendorPricingSetup:message key="LBL_LOAD" var="varLoad"/>
					<gmjsp:button style="width: 5em;height: 22" value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnLoad();" buttonType="Load" tabindex="9" />
					</td>
			  </tr>
			  <tr>
				  <td height="1" colspan="5" class="LLine"></td>
			  </tr>
				<tr>
					<td class="RightTableCaption" align="right" HEIGHT="25" ><fmtVendorPricingSetup:message key="LBL_PART_NUMBER"/>:&nbsp;</td>
			    		  <td align="left" HEIGHT="25" width="329"><html:text property="partNumber"  size="50"  onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" tabindex="4" onblur="changeBgColor(this,'#ffffff');"/>
    						</td>
					<td><select name="search" class="RightText" tabIndex="5" >
							<option value="0" ><fmtVendorPricingSetup:message key="OPT_CHOOSE_ONE"/>
							<option value="LIT" ><fmtVendorPricingSetup:message key="OPT_LITERAL"/>
							<option value="LIKEPRE" ><fmtVendorPricingSetup:message key="OPT_LIKE_PREFIX"/>
							<option value="LIKESUF" ><fmtVendorPricingSetup:message key="OPT_LIKE_SUFFIX"/>
						</select>
								       
					 &nbsp;&nbsp;<html:checkbox property="showSubComponentFl" tabindex="6"/>&nbsp;<fmtVendorPricingSetup:message key="LBL_SHOW_SUBCOMPONENTS"/>
					 </td> 
                        
				</tr>
				<tr>
					<td height="1" colspan="5" class="LLine"></td>
				</tr>
				<tr class="shade">
				
					<td class="RightTableCaption" height="28" colspan="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtVendorPricingSetup:message key="LBL_LOCKED"/>:
					<select name=locked class="RightText" tabIndex="7" >
							<option value="0" ><fmtVendorPricingSetup:message key="OPT_CHOOSE_ONE"/>
							<option value="Y" ><fmtVendorPricingSetup:message key="OPT_YES"/>
							<option value="N" ><fmtVendorPricingSetup:message key="OPT_NO"/>
						</select>
					<fmtVendorPricingSetup:message key="LBL_ACTIVE_PRICE"/>:
				    <select name=active class="RightText" tabIndex="8" >
							<option value="0" ><fmtVendorPricingSetup:message key="OPT_CHOOSE_ONE"/>
							<option value="Y" selected="selected"><fmtVendorPricingSetup:message key="OPT_YES"/>
							<option value="N" ><fmtVendorPricingSetup:message key="OPT_NO"/>
					</select>
			      </td>
				</tr>
			
				</table>
			</td>
		</tr>
		<tr>
			<td height="1" colspan="5" class="LLine"></td>
		</tr>
			<%if(!strProjectId.equals("")){%>
				 <tr>
				    <td colspan="9">
					<table>	
					<tr  height="25">			
						<td  width="28" colspan="1" tabIndex="10"><fmtVendorPricingSetup:message key="IMG_ALT_COPY" var="varCopy"/><a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="${varCopy}" style="border: none;" height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="11"><fmtVendorPricingSetup:message key="IMG_ALT_PASTE" var="varpaste"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="${varpaste}" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="12"><fmtVendorPricingSetup:message key="IMG_ALT_ADD_ROW" var="varAddRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:addRow()"height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="13"><fmtVendorPricingSetup:message key="IMG_ALT_ADD_ROWS_CLIPBOARD" var="varAddRowsClipboard"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/row_add_after_1.gif" alt="${varAddRowsClipboard}" style="border: none;" onClick="javascript:addRowFromClipboard()" height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="14"><fmtVendorPricingSetup:message key="IMG_ALT_REMOVE_ROW" var="varRemoveRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="14"><fmtVendorPricingSetup:message key="IMG_ALT_UNDO_REMOVE_ROW" var="varUndoRemoveRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/undo_delete.gif" alt="${varUndoRemoveRow}" style="border: none;" onClick="javascript:enableSelectedRow()" height="14"></a></td>
						<td width="180" colspan="1" tabIndex="15"><a href="#" onClick="javascript:filterData(this);">
						  <img src="<%=strImagePath%>/page_range.gif" style="border: none;float:left;"  height="14">						
						  <div id="filter_Lbl" style="display: inline;color:black;font-weight:bold; vertical-align: top"><fmtVendorPricingSetup:message key="LBL_SHOW_MODIFIED_ROWS"/></div></a>
						 </td>                   
						
						<td  colspan="2" align="right" >
						<div id="progress" style="display:none;"><img src="<%=strImagePath%>/success_y.gif" height="15"> </div></td>
						<td><div id="msg" style="display:none;color:green;font-weight:bold;"></div> 
							<div id="processimg" style="display:none; align="middle">
								<img border=0 height="25" width="20" align="middle"  src="<%=strImagePath%>/process.gif">
								<font color="red" align="middle"><b>&nbsp;&nbsp;<fmtVendorPricingSetup:message key="LBL_PLS_WAIT_DATA_IS_COPYTING_FORM_EXCEL"/>...</b></font>
							</div>
						</td>
					</tr>
					<tr>
			<td colspan="9" height="17"><font color="#800000" size="2">&nbsp;<b><fmtVendorPricingSetup:message key="LBL_MAX_ROWS_TO_PASTE_FROM_EXCEL"/></b></font></td>
		</tr>		 										
					</table>
					</td>
				</tr> 
				<tr>
					 <td colspan="9" >
						<div  id="divGrid" height="350px" width="1298px"></div>
					</td>
				</tr>
				<tr><td colspan="9" align="center" height="25" >
						<div class='exportlinks' ><fmtVendorPricingSetup:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" tabindex="15" onclick="fnDownloadXLS();"><fmtVendorPricingSetup:message key="LBL_EXCEL"/>  </a></div>
					</td>                 
				</tr>
				<tr>
					<td height="1" colspan="9" class="LLine"></td>
				</tr>
				<%if(strSubmitAccFl.equals("Y")){ %>
				<tr>
					<td class="RightTableCaption"  colspan="9" align="center" height="30">					
					<logic:equal name="frmVendorPricingSetup" property="submitAccFl" value="Y">
					<fmtVendorPricingSetup:message key="BTN_SUBMIT" var="varSubmit"/>
				      	<gmjsp:button value="${varSubmit}" controlId="Btn_Submit" name="Btn_Submit" gmClass="button" buttonType="Save" tabindex="16" onClick="fnSave();"/>&nbsp;&nbsp;
					</logic:equal>			   			
					</td>	
				</tr>
			    <%} }else{%>
				<tr>
					<td colspan="7" height="25" class="LLine"></td>
				</tr>
				<tr >
				<td colspan="9" align="center" class="RightText" ><fmtVendorPricingSetup:message key="LBL_MSG_NO_DATA"/></td>
				</tr>
			 	<%}%>
    </table>

</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
