<!-- \operations\GmVendorCommitReport.jsp  -->
<%
	/**********************************************************************************
	 * File		 		: GmVendorCommitReport.jsp
	 * Desc		 		: This screen is used for Editing Vendor's Order
	 * Version	 		: 1.0
	 * author			:
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtVendorCommitReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtVendorCommitReport:setLocale value="<%=strLocale%>"/>
<fmtVendorCommitReport:setBundle basename="properties.labels.operations.purchasing.GmWOVendorCommitReport"/>

<bean:define id="gridData" name="frmVendorCommit" property="gridData" type="java.lang.String"/> 
<bean:define id="projectID" name="frmVendorCommit" property="projectID" type="java.lang.String"></bean:define>
<bean:define id="vendorId" name="frmVendorCommit" property="vendorId" type="java.lang.String"></bean:define>
<bean:define id="searchprojectID" name="frmVendorCommit" property="searchprojectID" type="java.lang.String"></bean:define>
<bean:define id="searchvendorId" name="frmVendorCommit" property="searchvendorId" type="java.lang.String"></bean:define>
<%
String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("VENDOR_COMMIT_REPORT");
String strApplDateFmt = strGCompDateFmt;
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: PO Vendor Commit Report </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strPurchasingJsPath%>/GmWOVendorCommitReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
	var objGridData = '<%=gridData%>';
	var format = '<%=strApplDateFmt%>';
</script> 
</HEAD>
<BODY leftmargin="20" topmargin="10"  onload = "fnOnPageLoad()">
<html:form action="/gmVendorCommit.do?method=loadVendorCommitRpt">
<html:hidden property="strOpt"/>
<table border="0"  height="60" class=DtTable1055 cellspacing="0" cellpadding="0">
				<tr height="25">
					<td class="RightDashBoardHeader" colspan="6" ><fmtVendorCommitReport:message key="LBL_VENDOR_COMMIT_REPORT"/></td>
					<td colspan = "6" class="RightDashBoardHeader" align="right"><fmtVendorCommitReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
						height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/></td>
				</tr>

				<tr height="28">
					<td class="RightTableCaption" colspan="1" width="200" align="right"><fmtVendorCommitReport:message key="LBL_VENDOR_NAME"/>:</td>
					<td class="RightTableCaption" colspan="6" style="padding-left: 2px;">
					<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="vendorId" />
				<jsp:param name="METHOD_LOAD" value="loadVendorNameList&searchType=LikeSearch" />
				<jsp:param name="WIDTH" value="300" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="TAB_INDEX" value="3"/>
				<jsp:param name="CONTROL_NM_VALUE" value="<%=searchvendorId%>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=vendorId%>" />
 				<jsp:param name="SHOW_DATA" value="50" />
 				<jsp:param name="AUTO_RELOAD" value="" />                                       
 	 	 	    </jsp:include>  
					</td>
				
					<td class="RightTableCaption" width="165" colspan="2" align="right"><fmtVendorCommitReport:message key="LBL_PROJECT_LIST"/>:</td>
					<td valign="middle" colspan="3" align="left" style="padding-left: 2px; width: 26%;">
		 			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
								<jsp:param name="CONTROL_NAME" value="projectID" />
								<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=LikeSearch&searchBy=ProjectId" />
								<jsp:param name="WIDTH" value="250" />
								<jsp:param name="CSS_CLASS" value="search" />
								<jsp:param name="TAB_INDEX" value="4"/>
								<jsp:param name="CONTROL_NM_VALUE" value="<%=searchprojectID%>" /> 
								<jsp:param name="CONTROL_ID_VALUE" value="<%=projectID%>" />
								<jsp:param name="SHOW_DATA" value="100" />
								<jsp:param name="AUTO_RELOAD" value="" />					
					</jsp:include> 
	
		
					</td>
				 	
			</tr>
			    <tr>
				<td class="LLine" colspan="12" height="1"></td>
			    </tr>	
			 	<tr class="oddshade" height="28">
					<td class="RightTableCaption" colspan="1" align="right"><fmtVendorCommitReport:message key="LBL_PART_NUMBER"/>:</td>
					 <td class="RightTableCaption" colspan="6" align="left" style="white-space: nowrap;"> &nbsp;<html:text name="frmVendorCommit" property="partNums" size="45" styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>				
					<html:select property ="likeSearch"  onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" >
							<html:option value="" ><fmtVendorCommitReport:message key="LBL_CHOOSE_ONE"/></html:option>
							<html:option value="LIT" ><fmtVendorCommitReport:message key="LBL_LITERAL"/></html:option>
							<html:option value="LIKEPRE" ><fmtVendorCommitReport:message key="LBL_LIKE_PREFIX"/></html:option>
							<html:option value="LIKESUF" ><fmtVendorCommitReport:message key="LBL_LIKE_SUFFIX"/></html:option>
					</html:select>
					</td>

					<td class="RightTableCaption" colspan="2"  align="right"><fmtVendorCommitReport:message key="LBL_COMMIT_DUE_DAYS"/>:</td>
					 <td valign="middle" colspan="3" align="left"> &nbsp;<gmjsp:dropdown controlName="dueDays" SFFormName="frmVendorCommit" SFSeletedValue="dueDays" SFValue="alDueDays" codeId="CODENMALT"  codeName="CODENM"  defaultValue= "[Choose One]"/>
				 <html:text name="frmVendorCommit" property="noOfDays" size="10" styleClass="InputArea"  onfocus="changeBgColor(this,'#AACCE8');" onkeypress="return isNumber(event)" onblur="changeBgColor(this,'#ffffff');"/></td>	
				</tr>
				<tr>
				<td class="LLine" colspan="12" height="1"></td>
			    </tr>
				<tr height="28">
					 <fmtVendorCommitReport:message key="LBL_FROM_DATE" var="varFromDate"/>
		<td colspan="1" class="RightTableCaption" align="right" height="25"><gmjsp:label type="RegularText"  SFLblControlName="${varFromDate}:" td="false"/></td>
         <td colspan="1"> &nbsp;<gmjsp:calendar SFFormName="frmVendorCommit" controlName="fromDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/> </td>
		 <fmtVendorCommitReport:message key="LBL_TO_DATE" var="varToDate"/>
		<td colspan="1" class="RightTableCaption"  align="left" height="25" ><gmjsp:label type="RegularText"  SFLblControlName="${varToDate}:" td="false"/>
		 <gmjsp:calendar SFFormName="frmVendorCommit" controlName="toDate"   gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="6"/>
		 </td>
					 <td colspan="2" class="RightTableCaption" align="right">	
					 	<fmtVendorCommitReport:message key="LBL_SHOW_CLOSED_WO"/>:
					  </td><td colspan="3" class="RightTableCaption"> 
						<html:checkbox property="showClosedWOFl" name="frmVendorCommit" />	
					 </td>
					 <td colspan="4" class="RightTableCaption" align="left">		
					 	<fmtVendorCommitReport:message key="BTN_LOAD" var="varLoad"/>
						<gmjsp:button value="${varLoad}" gmClass="button" onClick="javascript:fnLoad()" tabindex="3" buttonType="Load"/> 	
					 </td>
				</tr>

					<%
				if (gridData.indexOf("cell") != -1) {
			%>
								

				<tr>
				<td colspan="12"><div  id="dataGridDiv" height="500px"></div></td></tr>
				<tr>						
					<td align="center" colspan="12"><br>
					
	             			<div class='exportlinks'><fmtVendorCommitReport:message key="LBL_EXCEL"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnDownloadXLS();"><fmtVendorCommitReport:message key="LBL_EXCEL"/>  </a></div>                         
	       			</td>
       			</tr><%} else /* if (!gridData.equals("")) */ {
			%>
			<tr>
				<td colspan="12" height="1" class="LLine"></td>
			</tr>
			<tr>
			<tr>
				<td colspan="12" height="25" align="center" class="RightText"><fmtVendorCommitReport:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/>
					</td>
			</tr>
	
			<%}%>
</table>
</html:form> 
</BODY>
<%@ include file="/common/GmFooter.inc" %>

</HTML>

