<%
/**********************************************************************************
 * File		 		: GmTTPbyVendorDashBoard.jsp
 * Desc		 		: TTP By Vendor Dashboard to see which Sheets are ready to have the Vendor Split reviewed 
 * Version	 		: 1.0
 * author			: Prabhu vigneshwaran M D 
************************************************************************************/
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import ="com.globus.common.beans.GmCalenderOperations"%>
<%@ taglib prefix="fmtTTPViewByVendor" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtTTPViewByVendor:setLocale value="<%=strLocale%>"/>
<fmtTTPViewByVendor:setBundle basename="properties.labels.operations.purchasing.GmTTPbyVendorDashBoard" />
 <!-- \operations\purchasing\ttpbyvendor\GmTTPbyVendorDashBoard.jsp-->
<bean:define id ="strttpId" name="frmTTPViewByVendor" property="ttpId" type="java.lang.String"></bean:define>
 <bean:define id="strttpName" name="frmTTPViewByVendor" property="searchttpId" type="java.lang.String"> </bean:define>
 <bean:define id="alTTPList" name="frmTTPViewByVendor" property="alTTPList" type="java.util.ArrayList"></bean:define>
  <bean:define id="strAccessFl" name="frmTTPViewByVendor" property="strAccessFl" type="java.lang.String"> </bean:define>
  <%
	String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
	String strWikiTitle = GmCommonClass.getWikiTitle("LBL_TTP_VIEW_BY_VENDOR");
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
	String strApplDateFmt = strGCompDateFmt;
 %>
 
 <HTML>
 <head>
 <title>Globus Medical:TTP View By Vendor</title>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<%-- <style type="text/css" media="all"> @import url("<%=strCssPath%>/screen.css");</style> --%>
<!-- Screen Js -->
<script language="JavaScript" src="<%=strPurchasingJsPath%>/ttpbyvendor/GmTTPbyVendorDashboard.js"></script>
<!-- Dhtmlx Import -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
var today_dt = '<%=strTodaysDate%>';
var format = '<%=strApplDateFmt%>';	
var lockAccFl = '<%=strAccessFl%>';
</script>
 </head>
<BODY leftmargin="20" topmargin="10"  onkeyup="fnEnter();">

<html:form action="/gmTTPbyVendorDashboard.do?method=loadTTPbyVendorDashboard">

<table border="0"  height="60" class=DtTable1000 cellspacing="0" cellpadding="0">
    <tr>
			<td height="25" class="RightDashBoardHeader" colspan="2"><fmtTTPViewByVendor:message key="LBL_TTP_VIEW_BY_VENDOR"/></td>
			<td align="right" colspan="2" class=RightDashBoardHeader ><fmtTTPViewByVendor:message key="IMG_ALT_HELP" var = "varHelp"/> 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
	<tr>
	<td width="1000" height="100" valign="top" colspan="4">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" >
	 <tr><td colspan="4" class="ELine"></td></tr> 
	     <tr>
						<td width ="100" class="RightTableCaption" align="right" HEIGHT="25">&nbsp;<fmtTTPViewByVendor:message key="LBL_TTP_NAME"/>:</td>
						<td style="padding-left: 2px;">
						    <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					        <jsp:param name="CONTROL_NAME" value="ttpId" />
					        <jsp:param name="METHOD_LOAD" value="loadTTPList&STROPT=GOP" />
					        <jsp:param name="WIDTH" value="240" />
					        <jsp:param name="CSS_CLASS" value="search" />
					        <jsp:param name="TAB_INDEX" value="1"/>
					        <jsp:param name="CONTROL_NM_VALUE" value="<%=strttpName%>" />
					        <jsp:param name="CONTROL_ID_VALUE" value="<%=strttpId%>" />
					        <jsp:param name="SHOW_DATA" value="50" />
					        <jsp:param name="AUTO_RELOAD" value="" />	
				            </jsp:include>	
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;<fmtTTPViewByVendor:message key="LBL_STATUS"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="status" SFFormName="frmTTPViewByVendor" SFSeletedValue="status"
								SFValue="alStatus" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="2" />													
						</td>
					</tr>
				<tr>
				<td class="LLine" height="1" colspan="4"></td>
			    </tr>
					<tr class="oddshade" height="30">
				<td width ="100" class="RightTableCaption"  align="right"  HEIGHT="25"><gmjsp:label type="MandatoryText" SFLblControlName="${varLockPeriod}" td="false"/><fmtTTPViewByVendor:message key="LBL_LOCK_PERIOD"/>:&nbsp;</td>
						<td>
						<gmjsp:dropdown controlName="ttpMonth"  SFFormName='frmTTPViewByVendor' SFSeletedValue="ttpMonth"
				       	SFValue="alMonth" width="105"	 codeId="CODENMALT" codeName="CODENM"  defaultValue= "[Choose One]" tabIndex="3" onChange="fnEnableDisableBtn ();"/>&nbsp;
				       	<gmjsp:dropdown controlName="ttpYear"  SFFormName='frmTTPViewByVendor' SFSeletedValue="ttpYear"
						SFValue="alYear" width="105" 	 codeId="CODENM" codeName="CODENM"  defaultValue= "[Choose One]" tabIndex="4" onChange="fnEnableDisableBtn ();"/></td>
						<td colspan="2">
							<gmjsp:button name="BTN_LOAD" value="Load" gmClass="button" onClick="fnLoad()" buttonType="Load" tabindex="6" /> </td>
				</tr>	
					<tr>
				<td class="LLine" height="1" colspan="4"></td>
			    </tr>
			
			     <tr id="trDiv">
			    	<td colspan="4">
					<div id="dataGridDiv" class="" height="400px"></div>
				</td>
				</tr>
						<tr style="display: none" id="DivExportExcel" height="25">
			<td colspan="4" align="center">
			    <div class='exportlinks'><fmtTTPViewByVendor:message key="DIV_EXPORT_OPT"/> : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="fnExport();"><fmtTTPViewByVendor:message key="DIV_EXCEL"/> 
				</a></div>
			</td>
		</tr>
		
		<tr>
				<td class="LLine" colspan="4" height="1"></td>
			  </tr>
				  <tr style="display: none" id="DivButton" height="30">
				<td colspan="8" align="center" height="30" class="RegularText">
					<logic:equal name="frmTTPViewByVendor" property="strAccessFl" value="Y">	
					<gmjsp:button value="Lock"
							name="LBL_LOCK_AND_GENERATE" gmClass="button" buttonType="save"
							onClick="fnTTPbyVendorLock();" tabindex="8" />&nbsp;
				</logic:equal>
					</td>
			</tr>	 
			<tr><td colspan="4" align="center"  class="RegularText"><div  id="DivNothingMessage"><fmtTTPViewByVendor:message key="NO_DATA_FOUND"/></div></td></tr>		
	</table>
	</td>


</table>
</html:form>
</BODY> 
<%@ include file="/common/GmFooter.inc" %>
 </HTML>