 <%
/**********************************************************************************
 * File		 		: GmPartConversion.jsp
 * Version	 		: 1.0
 * author			: Joe Prasanna Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPartConversion" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartConversion:setLocale value="<%=strLocale%>"/>
<fmtPartConversion:setBundle basename="properties.labels.operations.GmPartConversion"/>

<!-- operations\GmPartConversion.jsp -->
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	try{
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Part Conversion Screen </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/partconversioncart.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnFetchFromPartDetails()
{
	fnValidateTxtFld('partNumber',' Part Number ');
	fnValidateDropDn('fromInvType',' Type ');
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	fnStartProgress("Y");
	document.frmPartConversion.submit();
}

function fnConfirm()
 {
   return confirm("Are you sure you want to submit ?");   
 }

function fnSubmit()
{
	document.frmPartConversion.hinputStr.value = fnCreateOrderString();
	fnValidateDropDn('fromInvType',' Type ');
	fnValidateTxtFld('partNumber',' Part Number ');
	fnValidateTxtFld('partConversionComments',' Part Conversion Comments ');
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	//alert(document.frmPartConversion.hinputStr.value);
	document.frmPartConversion.strOpt.value = "save";
	
	if(fnConfirm())
		 {
		fnStartProgress('Y');
			document.frmPartConversion.submit();
		 }	
}

function fnAddRow(){
	varTotal = document.frmPartConversion.counterValue.value;
	alert(varTotal);
}

function fnInventoryDetails(varIndex){
	objpnum =  eval("document.frmPartConversion.toPartNum"+varIndex);
	varpnum = objpnum.value; 
	windowOpener("/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hPartNum="+encodeURIComponent(varpnum),"PO2","resizable=yes,scrollbars=yes,top=40,left=50,width=850,height=200");
}

</script>	

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmPartConversion.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="hinputStr" value = ""/>
<html:hidden property="counterValue" />
<html:hidden property="invQOH" />
<html:hidden property="cogsCost" />
<html:hidden property="totalPrice" value=""/>
<html:hidden property="totalConversionQty" value=""/>
<table border="0" class="border" width="850" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
				<td colspan="4" height="25" class="RightDashBoardHeader"><fmtPartConversion:message key="LBL_PART_CONV"/> </td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="25" class="RightTableCaption"  align="right"><font color="red">*</font><fmtPartConversion:message key="LBL_PART_NUM"/>:</td>
			<td class="RightTableCaption" colspan="3">&nbsp;<html:text property="partNumber"  size="12" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
			&nbsp;&nbsp;<font color="red">*</font><fmtPartConversion:message key="LBL_TYPE"/>:
			&nbsp;&nbsp;<gmjsp:dropdown controlName="fromInvType" SFFormName="frmPartConversion" SFSeletedValue="fromInvType"
							SFValue="alInvType" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]" />
			&nbsp;<fmtPartConversion:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="button" onClick="fnFetchFromPartDetails();" buttonType="Load" />				
			</td>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Struts tag lib code modified for JBOSS migration changes -->
		<tr>					
			<td height="25" class="RightTableCaption" align="right"><font color="red">*</font><fmtPartConversion:message key="LBL_PART_CONV"/> :</td>
			<td colspan="3">&nbsp;<html:textarea property="partConversionComments" cols="50" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<tr>					
			<td height="25" class="RightTableCaption" align="right"><fmtPartConversion:message key="LBL_PART_DESC"/>:</td>
			<td colspan="3">&nbsp;<bean:write name="frmPartConversion" property="partDescription"/></td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td height="25" class="RightTableCaption" align="right"><fmtPartConversion:message key="LBL_OPR_QOH"/> :</td>
			<td colspan="3">&nbsp;<bean:write name="frmPartConversion" property="invQOH"/></td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<tr>	
    		<td height="25" class="RightTableCaption" align="right"><fmtPartConversion:message key="LBL_COST_EA"/>:</td>
			<td colspan="3">&nbsp;<bean:write name="frmPartConversion" property="cogsCost"/></td>
		</tr>
	
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr> 
        <tr>
			<td colspan="4" align="center" height="30">&nbsp;
			<div style="overflow:auto; height:252px;">
			<table cellpadding="0" cellspacing="0" width="100%" border="0" bordercolor="gainsboro" id="PartnPricing">
			  <thead>
				<TR bgcolor="#EEEEEE" height="24" class="RightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
					<TH class="RightText" width="70" align="center"><fmtPartConversion:message key="LBL_TO_PART"/></TH>
					<TH class="RightText" width="70" align="center"><fmtPartConversion:message key="LBL_QTY"/></TH>
					<TH class="RightText" width="380" >&nbsp;<fmtPartConversion:message key="LBL_PART_DESC"/></TH>
					<TH class="RightText" width="80" align="center"><fmtPartConversion:message key="LBL_COST_EA"/></TH>
					<TH class="RightText" width="100" align="center"><fmtPartConversion:message key="LBL_EXT_COST"/></TH>
				</TR>	
				<tr><th class="Line" height="1" colspan="11"></th></tr>
			  </thead>
			  <TBODY>
<logic:iterate id="partDetailsList" name="frmPartConversion" property="alPartDetails" indexId="counter" >
				<tr>
					<td >&nbsp; <bean:write name="partDetailsList" property="TOPARTNUM"/> 
					<htmlel:hidden  property="toPartNum${counter}" value="${partDetailsList.TOPARTNUM}" />
					<htmlel:hidden  property="toInvType${counter}" value="90802" />
					</td>
					<td align="center" id="Txt_Qty<bean:write name="counter"/>" >&nbsp;
						<htmlel:text  property="toPartQty${counter}"  value="${partDetailsList.TOPARTQTY}" maxlength="100" size="3" style="InputArea" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="javascript:fnCalExtPrice('${counter}');"/>
					</td>
					<td id="Lbl_Desc<bean:write name="counter"/>" class="RightText"><bean:write name="partDetailsList" property="TOPARTDESCRIPTION"/></td>
					<td id="Txt_Price<bean:write name="counter"/>" class="RightText" align="left">
						<htmlel:text property="toCogsCost${counter}" value="${partDetailsList.TOCOGSCOST}" maxlength="100" size="10" style="InputArea" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="javascript:fnCalExtPrice('${counter}');" />
					</td>
					<td id="Lbl_Amount<bean:write name="counter"/>" class="RightText" align="right"></td>
				</tr>
</logic:iterate>				
				<!--<tr><td bgcolor="#eeeeee" height="1" colspan="11"></td></tr>-->
			  </TBODY>
			</table>
			</div>
			</td>
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<tr class="shade" height="23">
			<td class="RightText" width="30%" align="right" ><B><fmtPartConversion:message key="LBL_TOT_QTY"/>:</B>&nbsp;</td>
			<td class="RightCaption" width="20%" id="Lbl_TotalQty"><B></B>&nbsp;</td>
			<td class="RightText" width="30%" align="right" ><B><fmtPartConversion:message key="LBL_TOT_PRICE"/>:</B>&nbsp;</td>
			<td class="RightCaption" width="20%" id="Lbl_Total"></td>
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<tr height="30">
			<td colspan="4" align="center"> <fmtPartConversion:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save" /> </td>
		</tr>
		
	</table>
</html:form>		
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>