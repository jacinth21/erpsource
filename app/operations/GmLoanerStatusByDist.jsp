 <%
/**********************************************************************************
 * File		 		: GmLoanerStatusByDist.jsp
 * Desc		 		: This screen is used for the displaying details of a specific loaner set
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtLoanerStatusByDist" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtLoanerStatusByDist:setLocale value="<%=strLocale%>"/>
<fmtLoanerStatusByDist:setBundle basename="properties.labels.operations.GmLoanerStatusByDist"/>
<!-- operations\GmLoanerStatusByDist.jsp -->
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strApplDateFmt = strGCompDateFmt;
	String strDTDateFmt = "{0,date,"+strApplDateFmt+"}";
	
	String strSetName = GmCommonClass.parseNull((String)request.getAttribute("SETNAME"));
	String strLnTyp = GmCommonClass.parseNull((String)request.getAttribute("hLnTyp"));
	
%>


<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Status Report By Distributor </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10">
	<table border="0" class="DtTable850" width="800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">
				<fmtLoanerStatusByDist:message key="LBL_REPORT_DETAILS"/> - <%=strSetName%> 
			</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<tr>
			<td colspan="2">
		 		<display:table name="ALRESULT" class="its" id="currentRowObject" > 
					<fmtLoanerStatusByDist:message key="DT_CONID" var="varConId"/><display:column property="CONID" title="${varConId}" sortable="true" />
					<fmtLoanerStatusByDist:message key="DT_ETCH_ID" var="varEtchId"/><display:column property="ETCHID" title="${varEtchId}" class="alignleft"/>
				  	<fmtLoanerStatusByDist:message key="DT_STATUS" var="varStatus"/><display:column property="LOANSFL" title="${varStatus}" class="alignleft" sortable="true"/>
				  	<fmtLoanerStatusByDist:message key="DT_LOANEDTO" var="varLoaned"/><display:column property="LOANNM" title="${varLoaned}" class="alignleft"/>
				  	<fmtLoanerStatusByDist:message key="DT_LOANEDON" var="varLoanedOn"/><display:column property="LDATE" title="${varLoanedOn}" class="alignleft" format="<%=strDTDateFmt%>"/>
				  	<fmtLoanerStatusByDist:message key="DT_DAYS_ELP" var="varDaysElp"/><display:column property="ELPDAYS" title="${varDaysElp}" class="alignright"/>
				  	<fmtLoanerStatusByDist:message key="DT_EXP_DATE" var="varExpDate"/><display:column property="EDATE" title="${varExpDate}" class="alignleft" format="<%=strDTDateFmt%>"/>
				  	<fmtLoanerStatusByDist:message key="DT_DAYS_OVERDUE" var="varDaysOverdue"/><display:column property="OVERDUE" title="${varDaysOverdue}" class="alignright"/>
				 </display:table> 
			</td>
		</tr>
		<% if (strLnTyp.equals("4127")){ %>
		<tr>
			<td height="25" class="RightDashBoardHeader">
				<fmtLoanerStatusByDist:message key="LBL_REPORT_REQUEST"/> - <%=strSetName%> 
			</td>
		</tr>
		
		<tr><td class="Line" height="1" colspan="2"></td></tr>		
		<tr>
			<td colspan="2">
				 <display:table name="LOANERREQUESTS" class="its" id="currentRowObject"> 
				   <fmtLoanerStatusByDist:message key="DT_REQID" var="varReqId"/> <display:column property="PDTREQID" title="${varReqId}" sortable="true" />
				   <fmtLoanerStatusByDist:message key="DT_REQDT" var="varReqDt"/> <display:column property="REQSDT" title="${varReqDt}" sortable="true" class="aligncenter" />
				    <fmtLoanerStatusByDist:message key="DT_DIST" var="varDist"/><display:column property="DNAME" title="${varDist}" sortable="true" class="aligncenter"  />
				    <fmtLoanerStatusByDist:message key="DT_SHIPTO" var="varShipTo"/><display:column property="SHIPTONM" title="${varShipTo}" sortable="true" class="aligncenter"  />
					<fmtLoanerStatusByDist:message key="DT_REQUDT" var="varRequDt"/><display:column property="REQRDT" title="${varRequDt}" sortable="true" class="aligncenter"  />
					<fmtLoanerStatusByDist:message key="DT_CREATED_BY" var="varCreatedBy"/><display:column property="CUSER" title="${varCreatedBy}" sortable="true" class="aligncenter"  />
			 
				 </display:table> 
			</td>
		</tr>	
		<% } %>
</table>

</BODY>

</HTML>
