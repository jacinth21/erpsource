<%
/**********************************************************************************
 * File		 		: GmLotCodeProdInfo.jsp
 * Desc		 		: This Jsp is for the Product information section
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLotProdInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
  <!-- \operations\GmLotCodeProdInfo.jsp--> 
<fmtLotProdInfo:setLocale value="<%=strLocale%>"/>
<fmtLotProdInfo:setBundle basename="properties.labels.operations.GmLotCodeReport"/>
<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<HTML>
<HEAD>

<script>
</script>

<BODY leftmargin="20" topmargin="10">
	
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td class="Line" colspan="5"></td></tr> 
			<tr>
					<td height="24" class="ShadeRightTableCaption" colspan="5" style="padding-right: 818px;">&nbsp;<b><fmtLotProdInfo:message key="TD_LOT_PROD_INFO_HEADER"/></b></td>
		    </tr>	
		    <tr>
				<td class="RightText" HEIGHT="20"  align="right" width="20%">&nbsp;<b><fmtLotProdInfo:message key="LBL_PART_NUM"/>:&nbsp;</b></td>
				<td width="20%" id="partnum">&nbsp;<a href="javascript:fnQueryByStudy();"><bean:write name="<%=strFormName %>" property="partNum"/></a></td>
				<td class="RightText" HEIGHT="20"  align="right" width="15%">&nbsp;<b><fmtLotProdInfo:message key="LBL_PART_DESC"/>:&nbsp;</b></td>
				<td width="50%" id="pdesc">&nbsp;<bean:write name="<%=strFormName %>" property="partDesc"/></td>
			</tr>
			
			<tr class="Shade">
				<td class="RightText" HEIGHT="20"  align="right" >&nbsp;<b><fmtLotProdInfo:message key="LBL_SIZE"/>:&nbsp;</b></td>
				<td id="psize">&nbsp;<bean:write name="<%=strFormName %>" property="partSize"/></td>
				<td class="RightText" HEIGHT="20"  align="right" >&nbsp;<b><fmtLotProdInfo:message key="LBL_CPC_CLIENT"/>:&nbsp;</b></td>
				<td id="cpclient">&nbsp;<bean:write name="<%=strFormName %>" property="cpcClient"/></td>
			</tr>
		     
		</table>

	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>