 <%
/**********************************************************************************
 * File		 		: GmTransactionLog.jsp
 * Desc		 		: This screen is used to display the transaction log for the 
 *					  selected filter condition
 * Version	 		: 1.0
 * author			: Richardk
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page import="java.util.Date"%>
<!--operations\GmTransactionLog.jsp  -->
<%@ taglib prefix="fmtTransactionLog" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtTransactionLog:setLocale value="<%=strLocale%>"/>
<fmtTransactionLog:setBundle basename="properties.labels.operations.GmTransactionLog"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");
	String strGridXmlData = GmCommonClass.parseNull((String)request.getAttribute("XMLGRIDDATA"));
	String strFromDt 	= GmCommonClass.parseNull((String)request.getAttribute("hFromDate")) ;
	String strToDt 		= GmCommonClass.parseNull((String)request.getAttribute("hToDate"));
	String strPartNums 	= GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	String strTransId 	= GmCommonClass.parseNull((String)request.getAttribute("hTransId"));
	String strTID 	= "";
	String strSortCName = GmCommonClass.parseNull((String)request.getAttribute("hSort"));
	String strSortType 	= GmCommonClass.parseNull((String)request.getAttribute("hSortAscDesc"));

	String strSeletedType 	= GmCommonClass.parseNull((String)request.getAttribute("hSelectedType"));
	ArrayList alPType = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("halPTType"));
	ArrayList alTransType = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("halTType"));
	String strApplDateFmt = strGCompDateFmt;
	
	Date dtFrmDate = null;
	Date dtToDate = null;
	
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDt,strApplDateFmt);
	
	HashMap hmLoop = new HashMap();
	int intLoop = 0;
	
	
	if (alReturn != null)
	{
		intLoop = alReturn.size();
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Report - Transaction Log</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmTransactionLog.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>

var objGridData;
objGridData ='<%=strGridXmlData%>';
var gridObj;
function fnOnPageLoad() {
	
	if (objGridData.value != '') {
		gridObj = initGridData('TransLogRpt', objGridData);
	}
}
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.attachHeader('#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#select_filter,#text_filter,#select_filter');
	gObj.init();
	gObj.loadXMLString(gridData);
   // gObj.groupBy(0);
	gObj.enableTooltips("false,false,false,false,false,false,false,false,false");
    return gObj;
}

//To download excel;
function fnExcel() { 
	gridObj.toExcel('/phpapp/excel/generate.php');
}
var format = '<%=strApplDateFmt%>';

function fnSort(val)
{
	document.frmAccount.hSort.value = val;

	if (val == '<%=strSortCName%>' )
	{

		if (document.frmAccount.hSortAscDesc.value == '1') 
		{
			document.frmAccount.hSortAscDesc.value = '2'
		}
		else
		{
			document.frmAccount.hSortAscDesc.value = '1'
		}
	}
	else
	{
		document.frmAccount.hSortAscDesc.value = '1';
	}
	
	document.frmAccount.hAction.value = "LoadNCMRDetails";
	document.frmAccount.submit();
}

var prevtr = 0;
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmTranLogReportServlet">
<input type="hidden" name="hId" value="">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hSort" value="<%=strSortCName%>">
<input type="hidden" name="hSortAscDesc" value="<%=strSortType%>">


	<table border="0" width="900" cellspacing="0" cellpadding="0">

		<tr>
			<td class="Line" width="1" rowspan="20"></td>
			<td height="25" colspan="2"  class="RightDashBoardHeader"><fmtTransactionLog:message key="LBL_REPORT"/></td>
			<td class="Line" width="1" rowspan="20"></td>
		</tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="24" align="right"><fmtTransactionLog:message key="LBL_PART_NUM"/>:</td>
						<td>&nbsp;<input type="text" size="50" value="<%=strPartNums%>" name="Txt_PartNum" 
							class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
							<span class="RightTextBlue"><fmtTransactionLog:message key="LBL_PART_SEPARATED"/></span>
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" HEIGHT="24" align="right"><fmtTransactionLog:message key="LBL_DT_RANGE"/>:</td>
						<td class="RightText">&nbsp;<fmtTransactionLog:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/> 
							&nbsp;&nbsp;<fmtTransactionLog:message key="LBL_TO"/>: <gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr>
						<td class="RightTableCaption" HEIGHT="24" align="right"><fmtTransactionLog:message key="LBL_TRANS_ID"/>:</td>
						<td>&nbsp;<input type="text" size="15" 
							value="<%=strTransId%>" name="Txt_TransId" class="InputArea" 
							onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" 
							tabindex=2>
						</td>
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="Shade">
						<td class="RightTableCaption" HEIGHT="24" align="right"><fmtTransactionLog:message key="LBL_INV_TYP"/>:</td>
						<td>&nbsp;<gmjsp:dropdown controlName="cboType"  seletedValue="<%= strSeletedType %>" 	
						codeId = "CODEID"  codeName = "CODENM" tabIndex="1"  width="250" value="<%= alPType%>"   />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<fmtTransactionLog:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="return fnSubmit();" tabindex="25" buttonType="Load" /></td>
					</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>

	<%if(intLoop >0){%> 
<tr>
				<td colspan="2">
					<div id="TransLogRpt" style="height: 640px; width:100%"></div>
				</td>			
		</tr>

     	<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
     		<tr>
			<td colspan="2" align="center">
			    <div class='exportlinks'><fmtTransactionLog:message key="LBL_EXPORT_OPTS"/> : <img src='img/ico_file_excel.png' onclick="fnExcel();" />&nbsp;<a href="#" onclick="fnExcel();"><fmtTransactionLog:message key="LBL_EXCEL"/></a></div>
			</td>
		</tr>
		<% } else	{%>	

					<tr>
						<td height="30" colspan="13" align="center" class="RightTextBlue"><fmtTransactionLog:message key="LBL_NO_TRANS"/>!</td>
					</tr>
<%}%>			

		<tr><td colspan="9" class="Line" height="1"></td></tr>
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
