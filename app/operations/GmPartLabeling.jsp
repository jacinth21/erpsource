<%
/*********************************************************************************************************
 * File		 		: GmPartLabeling.jsp
 * Desc		 		: This screen is used to process PTRD transaction
 * Version	 		: 1.0
 * author			: Arajan
**********************************************************************************************************/
%>
<%@ page language="java"%>

<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtPartLabel" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPartLabeling.jsp -->
<fmtPartLabel:setLocale value="<%=strLocale%>"/>
<fmtPartLabel:setBundle basename="properties.labels.operations.GmPartLabel"/>
<bean:define id="strAction" name="frmPartLabel" property="haction" type="java.lang.String"></bean:define>
<bean:define id="action" name="frmPartLabel" property="action" type="java.lang.String"></bean:define>
<bean:define id="statusfl" name="frmPartLabel" property="statusfl" type="java.lang.String"></bean:define>
<bean:define id="gridData" name="frmPartLabel" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="txnid" name="frmPartLabel" property="txnid" type="java.lang.String"></bean:define>
<bean:define id="txntypenm" name="frmPartLabel" property="txntypenm" type="java.lang.String"></bean:define> 
<bean:define id="QCFlag" name="frmPartLabel" property="QC" type="java.lang.String"></bean:define>
<bean:define id="printLabelsFlg" name="frmPartLabel" property="labels" type="java.lang.String"></bean:define>
<bean:define id="accessFlg" name="frmPartLabel" property="accessFlg" type="java.lang.String"></bean:define>
<bean:define id="QCAccessFlg" name="frmPartLabel" property="QCAccessFlg" type="java.lang.String"></bean:define>
<bean:define id="processBtnAccess" name="frmPartLabel" property="strProcessAccess" type="java.lang.String"></bean:define>
<bean:define id="verifyBtnAccess" name="frmPartLabel" property="strVerifyAccess" type="java.lang.String"></bean:define>
<bean:define id="pre_staging" name="frmPartLabel" property="lbl_pre_stage" type="java.lang.String"></bean:define>
<bean:define id="staging" name="frmPartLabel" property="lbl_stage" type="java.lang.String"></bean:define>
<% 
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strDisabled = "";
String strStatusFl = statusfl;
if(strStatusFl.equals("4"))
{
	strDisabled = "true";
}
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmPartLabel", strSessCompanyLocale);
String strHeaderRule="";
String strHeader="Transaction";
String strSubmit =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SUBMIT"));
String strProcess =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PROCESS"));
String strVerify =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VERIFY"));
String strVoid =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VOID"));
String strSaveComments =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SAVE_COMMENTS"));

//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);

%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Status Log Detail</TITLE>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">

<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmPartLabeling.js"></script>

<style type="text/css">
</style>
<script>
var companyInfoObj = '<%=strCompanyInfo%>';
var objGridData = '<%=gridData%>';
var txnid = '<%=txnid%>';
var txntypenm = '<%=txntypenm%>';
var statusfl = '<%=statusfl%>';
var verifyBtnAccess = '<%=verifyBtnAccess%>';
var processBtnAccess = '<%=processBtnAccess%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnPageLoad();">

<html:form styleId="frmPartLabel">
<html:hidden name="frmPartLabel" styleId="txnid" property="txnid" />
<html:hidden name="frmPartLabel" styleId="txntype" property="txntype" />
<html:hidden name="frmPartLabel" styleId="loctype"  property="loctype" />
<html:hidden name="frmPartLabel" styleId="haction" property="haction" />
<html:hidden name="frmPartLabel" styleId="hApprString" property="hApprString" />
<html:hidden name="frmPartLabel" styleId="hRejString" property="hRejString" />
<html:hidden name="frmPartLabel" styleId="action" property="action" />

	<table class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<logic:equal name="frmPartLabel" property="action" value="1">
				<td height="25" class="RightDashBoardHeader"><fmtPartLabel:message key="LBL_EDITLOT_HEADER"/></td>
			</logic:equal>
			<logic:equal name="frmPartLabel" property="haction" value="PP">
				<logic:notEqual name="frmPartLabel" property="statusfl" value="1">
					<td height="25" class="RightDashBoardHeader"><fmtPartLabel:message key="LBL_EDITLOT_HEADER1"/></td>
				</logic:notEqual>
			</logic:equal>
			<logic:equal name="frmPartLabel" property="haction" value="PV">
				<td height="25" class="RightDashBoardHeader"><fmtPartLabel:message key="LBL_EDITLOT_HEADER2"/></td>
			</logic:equal>
			<logic:equal name="frmPartLabel" property="haction" value="PP">
				<logic:equal name="frmPartLabel" property="statusfl" value="1">
					<td height="25" class="RightDashBoardHeader"><fmtPartLabel:message key="LBL_PEND_PARAM"/></td>
				</logic:equal>
			</logic:equal>
		    <fmtPartLabel:message key="LBL_HELP" var="varHelp"/>
			<td align="right" class=RightDashBoardHeader><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("PART_LABELING")%>');" />
			</td>
		</tr>
		<logic:notEqual name="frmPartLabel" property="pnqnTxnId" value="">
		<tr>
			<td height="25" align="center" colspan="2" class="RightTableCaption">
				<font color="green"> <fmtPartLabel:message key="LBL_MSG"/><bean:write name="frmPartLabel" property="pnqnTxnId" /></font>
			</td>
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
		</logic:notEqual>
		<tr>
			<td colspan="2">
				<jsp:include page="/operations/GmPartLabelHeaderInc.jsp">
				<jsp:param name="FORMNAME" value="frmPartLabel" />
				</jsp:include>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<jsp:include page="/operations/GmPartLabelDetailInc.jsp">
				<jsp:param name="FORMNAME" value="frmPartLabel" />
				</jsp:include>
			</td>
		</tr>
		<tr>
			<td colspan="2"> 
				<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="FORMNAME" value="frmPartLabel" />
					<jsp:param name="ALNAME" value="alLogReasons" />
					<jsp:param name="LogMode" value="Edit" />
				</jsp:include>
			</td>
		</tr>
        <%if(statusfl.equals("3") && strAction.equals("PP")){%>
			<tr>
			      <td colspan="2" align="center" height="30" style = "color:red"><span><%=txnid%> <fmtPartLabel:message key="LBL_VERIFICARION_MESSAGE"/></span></td>
		   </tr>
		 <%}%>
		 <%if(statusfl.equals("4") && strAction.equals("PV")){%>
		    <tr>
			   <td colspan="2" align="center" height="30"  style = "color:red"> <fmtPartLabel:message key="LBL_VERIFICARION_MESSAGE1"/>&nbsp;<span><%=txnid%></span></td>
		    </tr>
	   <%}%>
		<tr>
		<tr>
			<td colspan="2" align="center" height="30">
				<%if((statusfl.equals("2") && !strAction.equals("PP")) || (statusfl.equals("1") && strAction.equals("PP"))){%>
				        <gmjsp:button value="<%=strSubmit%>"  name="Btn_Submit" gmClass="button" onClick="fnSubmit()" buttonType="Save" disabled = "true"/>&nbsp;&nbsp;
				         <!--PMT-36523- Stage DashBoard  to enable the submit button if all the parameters are checked -->
				<%} else if(statusfl.equals("1") && QCFlag.equals("Y") && printLabelsFlg.equals("Y") && pre_staging.equals("Y") && staging.equals("Y")){%>
	                   <gmjsp:button value="<%=strSubmit%>"  name="Btn_Submit" gmClass="button" onClick="fnSubmit()" buttonType="Save"/>&nbsp;&nbsp;
	         
		        <%}else {%>
		        		<gmjsp:button value="<%=strSubmit%>"  name="Btn_Submit" gmClass="button" onClick="fnSubmit()" buttonType="Save" disabled = "true"/>&nbsp;&nbsp;
		        <%} %>
	            <%if(strAction.equals("PP")){%>
	            	     <gmjsp:button value="<%=strSaveComments%>"  name="Btn_SaveCommt" gmClass="button" buttonType="Save" onClick="fnSaveComments();"/>&nbsp;&nbsp;     
	            	 <%if(statusfl.equals("3")){%>
	                          <gmjsp:button value="<%=strProcess%>"  name="Btn_Submit" gmClass="button" buttonType="Save" onClick="fnSave();" disabled="true"/>&nbsp;&nbsp;
		              <%}else {%>
	                        <%if(statusfl.equals("2") && processBtnAccess.equals("Y")){%>
	                                <gmjsp:button value="<%=strProcess%>"  name="Btn_Submit" gmClass="button" buttonType="Save" onClick="fnSave();"/>&nbsp;&nbsp;
	                        <%}else {%>
	                                <gmjsp:button value="<%=strProcess%>"  name="Btn_Submit" gmClass="button" buttonType="Save" onClick="fnSave();" disabled="true"/>&nbsp;&nbsp;
		                    <%}
		               }%>
                <%}%>
                <%if(strAction.equals("PV")){%>
	                   <%if(verifyBtnAccess.equals("Y")){%>
	                         <gmjsp:button value="<%=strVerify%>"  name="Btn_submit" gmClass="button" buttonType="Save" onClick="fnVerify();" disabled="true"/>&nbsp;&nbsp;
	                   <%}else {%>
	                         <gmjsp:button value="<%=strVerify%>"  name="Btn_submit"  gmClass="button" buttonType="Save" onClick="fnVerify();" disabled="true"/>&nbsp;&nbsp;
		               <%}%>
                  <%}%>
		     <gmjsp:button value="<%=strVoid%>" name="Btn_Submit" gmClass="button" onClick="fnVoid()" buttonType="Save" disabled = "<%=strDisabled%>"/>&nbsp;&nbsp;
		</td>		
</tr>
</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>