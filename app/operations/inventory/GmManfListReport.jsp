 <%
/**********************************************************************************
 * File		 		: GmManfListReport.jsp
 * Desc		 		: This screen is used for displaying Manufaturing Parts count
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>

<!-- \operations\inventory\GmManfListReport.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->


<%@ taglib prefix="fmtManfListReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtManfListReport:setLocale value="<%=strLocale%>"/>
<fmtManfListReport:setBundle basename="properties.labels.operations.inventory.GmManfListReport"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	 strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	 strCssPath = GmCommonClass.getString("GMSTYLES");
	 strImagePath = GmCommonClass.getString("GMIMAGES");
	 strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	 strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	String strApplDateFmt = strGCompDateFmt;
	//
	final String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	final String strPartNumber = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	final String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	
	
	HashMap hmTemp = new HashMap();
	strApplDateFmt = strApplDateFmt + " K:m:s a";
	SimpleDateFormat df = new SimpleDateFormat(strApplDateFmt);
	//String strDate = GmCommonClass.getStringFromDate((new java.util.Date()),strGCompDateFmt);
	String strDate = GmCommonClass.getStringFromDate(new java.util.Date(),strApplDateFmt,strGCompTimeZone);
	String strUserName = (String)session.getAttribute("strSessShName");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Raw Materials Inventory </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>

<!-- fngo() form -->
<!-- Step5 verify strhOpt is read at top from hOpt -->
 
function fnGo()
{
	
	document.frmInv.hOpt.value = '<%=strhOpt%>'; 
	var pnum = document.frmInv.hPartNum.value;
	
	//if (pnum != '')
	//{
		//!document.frmInv.hOpt.value = "MultipleParts";
		//!document.frmInv.Cbo_ProjId.selectedIndex = 0;
	//}
	document.frmInv.hAction.value = "Go";
	document.frmInv.submit();
}



</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmInv" method="POST" action="<%=strServletPath%>/GmInvListReportServlet">

<!---->
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">


	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
		
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtManfListReport:message key="LBL_RAW_MATERIALS_REPORT"/></td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="ALREPORT" export="true" class="its" id="currentRowObject"> 
			
			
			<display:caption><fmtManfListReport:message key="LBL_REPORT_AS_OF"/>: <%=strDate%>   <fmtManfListReport:message key="LBL_BY"/>:<%=strUserName%></display:caption>
				
				<fmtManfListReport:message key="LBL_PART_NUMBER" var="varPartNumber"/>
				<display:column property="PNUM" title="${varPartNumber}" class="aligncenter" sortable="true" />
				
				<fmtManfListReport:message key="LBL_DESCRIPTION" var="varDescription"/>
				<display:column property="PDESC" escapeXml="true" title="${varDescription}" class="alignleft"  />
			  	
			  	<fmtManfListReport:message key="LBL_SHELF_QTY" var="varShelfQty"/>
			  	<display:column property="AQTY" title="${varShelfQty}" class="alignright" sortable="true" />
			  	
			  	<fmtManfListReport:message key="LBL_QUARANTINE_QTY" var="varQuarantineQty"/>
			  	<display:column property="INQUARAN" title="${varQuarantineQty}" class="alignright" sortable="true" />
			</display:table>
			</td>
		</tr>
    <table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>