 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: GmReturnsPartsReport.jsp
 * Desc		 		: This screen is used for displaying Returns count
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>

<!--\operations\inventory\GmReturnsPartsReport.jsp  -->

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>


<%@ taglib prefix="fmtReturnsPartsReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReturnsPartsReport:setLocale value="<%=strLocale%>"/>
<fmtReturnsPartsReport:setBundle basename="properties.labels.operations.inventory.GmReturnsPartsReport"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	
	final String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	final String strPartNumber = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	final String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	
	
	HashMap hmTemp = new HashMap();

	String strApplDateFmt = strGCompDateFmt;
	GmCalenderOperations gmCal = new GmCalenderOperations();
	gmCal.setTimeZone(strGCompTimeZone);
    String strDate = "";
	strDate = gmCal.getCurrentDate(strApplDateFmt+" K:m:s a");
	String strDateFmt = "{0,date,"+strApplDateFmt+"}";
	String strUserName = (String)session.getAttribute("strSessShName");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Returns Inventory </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>




 
function fnGo()
{
	
	document.frmInv.hOpt.value = '<%=strhOpt%>'; 
	var pnum = document.frmInv.hPartNum.value;
	
	//if (pnum != '')
	//{
		//!document.frmInv.hOpt.value = "MultipleParts";
		//!document.frmInv.Cbo_ProjId.selectedIndex = 0;
	//}
	document.frmInv.hAction.value = "Go";
	fnStartProgress();
	document.frmInv.submit();
}


</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmInv" method="POST" action="<%=strServletPath%>/GmInvListReportServlet">

<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtReturnsPartsReport:message key="LBL_RETURNS_PARTS_LIST_REPORT"/></td>
		</tr>
		
		<tr>
			<td colspan="2"> 
				<jsp:include page="/operations/inventory/include/GmPartFilter.jsp" >
				<jsp:param name="hPartNum" value="<%=strPartNumber%>" />
				<jsp:param name="hSearch" value="<%=strSearch%>" />
				<jsp:param name="hProjId" value="<%=strProjId%>" />
			</jsp:include>
			</td>
		</tr>
		
		
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="requestScope.REPORT.rows" export="true" class="its" id="currentRowObject"> 
			<display:setProperty name="export.excel.filename" value="ReturnsPartsReport.xls" />
			<display:caption><fmtReturnsPartsReport:message key="LBL_REPORT_AS_OF"/> <%=strDate%> <fmtReturnsPartsReport:message key="LBL_BY"/>:<%=strUserName%></display:caption>
			
			<fmtReturnsPartsReport:message key="LBL_TYPE" var="varType"/>
				<display:column property="TYPE" title="${varType}" group="1" class="alignleft" sortable="true" />
			
			<fmtReturnsPartsReport:message key="LBL_RA_ID" var="varRAID"/>
				<display:column property="RAID" title="${varRAID}" group="1" class="alignleft" sortable="true" />
			
			<fmtReturnsPartsReport:message key="LBL_CREDIT_DATE" var="varCreditDate"/>
				<display:column property="CDATE" title="${varCreditDate}" group="1" class="alignleft" sortable="true" format="<%=strDateFmt%>"/>
			
			<fmtReturnsPartsReport:message key="LBL_PART_NUMBER" var="varPartNumber"/>
				<display:column property="PNUM" title="${varPartNumber}" sortable="true" class="alignleft" />
			
			<fmtReturnsPartsReport:message key="LBL_QUANTITY" var="varQuantity"/>
			 	<display:column property="QTY" title="${varQuantity}" sortable="true" class="aligncenter" />
			</display:table>
			</td>
		</tr>
    <table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>