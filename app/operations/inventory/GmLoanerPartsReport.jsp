 <%
/**********************************************************************************
 * File		 		: GmLoanerPartsReport.jsp
 * Desc		 		: This screen is used for displaying InHouse count
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>

<!-- \operations\inventory\GmLoanerPartsReport.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtLoanerPartsReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtLoanerPartsReport:setLocale value="<%=strLocale%>"/>
<fmtLoanerPartsReport:setBundle basename="properties.labels.operations.inventory.GmLoanerPartsReport"/>
<%

try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	
	final String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	final String strPartNumber = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	final String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	
	String strOptVal = GmCommonClass.parseNull(request.getParameter("optVal"));
	HashMap hmTemp = new HashMap();
	strApplDateFmt = strApplDateFmt + " K:m:s a";
	SimpleDateFormat df = new SimpleDateFormat(strApplDateFmt);
	String strDate = (String)df.format(new java.util.Date());
	String strUserName = (String)session.getAttribute("strSessShName");
	boolean blExport = false;
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: InHouse Inventory </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>





 
function fnGo()
{
	
	document.frmInv.hOpt.value = '<%=strhOpt%>'; 
	var pnum = document.frmInv.hPartNum.value;
	
	//if (pnum != '')
	//{
		//!document.frmInv.hOpt.value = "MultipleParts";
		//!document.frmInv.Cbo_ProjId.selectedIndex = 0;
	//}
	document.frmInv.hAction.value = "Go";
	fnStartProgress();
	document.frmInv.submit();
}




</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmInv" method="POST" action="<%=strServletPath%>/GmInvListReportServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtLoanerPartsReport:message key="LBL_LOANER_PARTS_REPORTS"/></td>
		</tr>
		<%
		if(!strOptVal.equalsIgnoreCase("maininv")){
			blExport = true;
		%>
		
		<tr>
			<td colspan="2"> 
				<jsp:include page="/operations/inventory/include/GmPartFilter.jsp" >
				<jsp:param name="hPartNum" value="<%=strPartNumber%>" />
				<jsp:param name="hSearch" value="<%=strSearch%>" />
				<jsp:param name="hProjId" value="<%=strProjId%>" />
			</jsp:include>
			</td>
		</tr>
		<%} %>
		
		
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="requestScope.REPORT.rows" export="<%=blExport%>" class="its" id="currentRowObject">
			<display:setProperty name="export.excel.filename" value="LoanerPartsReport.xls" />
			
			<display:caption><fmtLoanerPartsReport:message key="DT_REPORT_AS_OF"/>  : <%=strDate%>  <fmtLoanerPartsReport:message key="DT_BY"/> :<%=strUserName%></display:caption>
			
			
			<fmtLoanerPartsReport:message key="DT_SET_ID" var="varSetID"/>
				<display:column property="SETID" title="${varSetID}" group="1" class="alignleft" sortable="true" />
				
				<fmtLoanerPartsReport:message key="DT_SET_NAME" var="varSetName"/>
				<display:column property="SNAME" title="${varSetName}" group="1" class="alignleft" sortable="true" />
				
				<fmtLoanerPartsReport:message key="DT_CONSIGN_ID" var="varConsignID"/>
				<display:column property="CONID" title="${varConsignID}" group="1" class="alignleft" sortable="true" />
				
				<!-- PMT-22758 -->
				<fmtLoanerPartsReport:message key="DT_TAG_ID" var="varTagID"/>
				<display:column property="TAG_ID" title="${varTagID}" group="1" class="alignleft" sortable="true" />
				
				<fmtLoanerPartsReport:message key="DT_ETCH_ID" var="varEtchID"/>
				<display:column property="ETCHID" title="${varEtchID}" group="1" sortable="true" class="alignleft" />
				
				<fmtLoanerPartsReport:message key="DT_PART_NUMBER" var="varPartNumber"/>
				<display:column property="PNUM" title="${varPartNumber}"sortable="true" class="alignleft" />
				
				<fmtLoanerPartsReport:message key="DT_QUANTITY" var="varQuantity"/>
				<display:column property="QTY" title="${varQuantity}" sortable="true" class="aligncenter" />
			</display:table>
			</td>
		</tr>
    <table>
    
   <%if(strOptVal.equalsIgnoreCase("maininv")){ %>
   	<br>
		<center><fmtLoanerPartsReport:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" /></center>
 		<%} %>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>