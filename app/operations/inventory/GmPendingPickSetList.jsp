
<%
	/**********************************************************************************
	 * File		 		: GmPendingPickSetList.jsp
	 * Desc		 		: This screen is used to view the Pending Pick Dashboard
	 * Version	 		: 
	 * author			: 
	 ************************************************************************************/
%>

<!-- \operations\inventory\GmPendingPickSetList.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="gridData" name="frmPendingPickSet" property="gridXmlData" type="java.lang.String" />

<%@ taglib prefix="fmtPendingPickSetList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPendingPickSetList:setLocale value="<%=strLocale%>"/>
<fmtPendingPickSetList:setBundle basename="properties.labels.operations.inventory.GmPendingPickSetList"/>





<%
    String strOperationsInventoryJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");
	String strWikiTitle = GmCommonClass.getWikiTitle("SET_PENDING_PICK");

%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Pending Pick Dashboard</TITLE>

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>


<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strOperationsInventoryJsPath%>/GmPendingPickSet.js"></script>

<script>
var objGridData = '<%=gridData%>';
var rowID = 0;		
var objFl = '<%=gridData.indexOf("cell") != -1 ? "Y" : "N"%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad('PICK_DASH');">

	<html:form action="/gmPendingPickSet.do?method=loadPendingPickSets">
		<html:hidden property="strOpt" />
		<html:hidden property="strInputString" value="" />
		<html:hidden property="pType" />
		<html:hidden property="locationType" />

		<table class="DtTable800" cellspacing="0" cellpadding="0">
			<tr>
		
				<td height="25" width="40%" class="RightDashBoardHeader"><fmtPendingPickSetList:message key="LBL_PENDING_PICK_DASHBOARD"/></td>
				
<fmtPendingPickSetList:message key="LBL_HELP" var="varHelp"/>
				<td align="right" class=RightDashBoardHeader><img id='imgEdit'
					align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title="${varHelp}"
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>

			<%
				if (gridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="2" width="100%">

					<div id="dataGrid" style="height: 400px; width: 800px;"></div></td>
			</tr>

			<%
				} else {
			%>
			<tr>
			
				<td align="center" colspan="2" height="30" class="RightTableCaption"><fmtPendingPickSetList:message key="LBL_MSG_NO_DATA"/></td>
			</tr>
			<%
				}
			%>

		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>