 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: GmBuiltSetPartsReport.jsp
 * Desc		 		: This screen is used for displaying Built Sets Parts
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>

<!--\operations\inventory\GmBuiltSetPartsReport.jsp  -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"  %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>



<%@ taglib prefix="fmtBuiltSetPartsReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtBuiltSetPartsReport:setLocale value="<%=strLocale%>"/>
<fmtBuiltSetPartsReport:setBundle basename="properties.labels.operations.inventory.GmBuiltSetPartsReport"/>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	
	//
	final String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	final String strPartNumber = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	final String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	String strApplDateFmt = strGCompDateFmt;

	
	
	
	String strOptVal = GmCommonClass.parseNull(request.getParameter("optVal"));
	HashMap hmTemp = new HashMap(); 
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt+" K:m:s a");
	String strUserName = (String)session.getAttribute("strSessShName");
	boolean blExport = false;
	
	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Built Set Parts Inventory </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<!-- fngo() -->

<script>
function fnGo()
{
	
	document.frmInv.hOpt.value = '<%=strhOpt%>'; 
	var pnum = document.frmInv.hPartNum.value;
	
	//if (pnum != '')
	//{
		//!document.frmInv.hOpt.value = "MultipleParts";
		//!document.frmInv.Cbo_ProjId.selectedIndex = 0;
	//}
	document.frmInv.hAction.value = "Go";
	fnStartProgress();
	document.frmInv.submit();
}
</script>


<BODY leftmargin="20" topmargin="10">
<FORM name="frmInv" method="POST" action="<%=strServletPath%>/GmInvListReportServlet">
<!--    -->
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">




	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
	<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtBuiltSetPartsReport:message key="LBL_BUILD_SETS_BY_PARTS_REPORT"/></td>
		</tr>
		
		
		<tr>
			<td colspan="2"> 
				<jsp:include page="/operations/inventory/include/GmPartFilter.jsp" >
				<jsp:param name="hPartNum" value="<%=strPartNumber%>" />
				<jsp:param name="hSearch" value="<%=strSearch%>" />
				<jsp:param name="hProjId" value="<%=strProjId%>" />
			</jsp:include>
			</td>
		</tr>
	
		
		<tr><td bgcolor="#666666" height="1"></td></tr>
	<tr>
			<td colspan="2">
			<display:table name="ALREPORT" export="true" class="its" id="currentRowObject">
			<display:setProperty name="export.excel.filename" value="BuiltSetPartReport.xls" />
			<display:caption><fmtBuiltSetPartsReport:message key="LBL_REPORT_AS_OF"/>  : <%=strDate%>  <fmtBuiltSetPartsReport:message key="LBL_BY"/> :<%=strUserName%></display:caption>
			
			 <fmtBuiltSetPartsReport:message key="LBL_SET_ID" var="varSetID"/>
			 <display:column property="SET_ID" title="${varSetID}"  class="alignleft" sortable="true" />
			 
			  <fmtBuiltSetPartsReport:message key="LBL_SET_NAME" var="varSetName"/>
			 <display:column property="SET_NAME" title="${varSetName}"  class="alignleft" sortable="true" />
			
			 <fmtBuiltSetPartsReport:message key="LBL_CONSIGN_ID" var="varConsignID"/>
			 <display:column property="CN_ID" title="${varConsignID}"  class="alignleft" sortable="true" />
			
			<!-- PMT-22758 -->
			 <fmtBuiltSetPartsReport:message key="LBL_TAG_ID" var="varTagID"/>
			 <display:column property="TAG_ID" title="${varTagID}" class="alignleft" sortable="true" />
			
			 <fmtBuiltSetPartsReport:message key="LBL_PART_NUMBER" var="varPartNumber"/>
			 <display:column property="PNUM" title="${varPartNumber}"  class="alignleft" sortable="true" />
			
			 <fmtBuiltSetPartsReport:message key="LBL_DESCRIPTION" var="varDescription"/>
			 <display:column property="PDESC" title="${varDescription}" class="alignleft" sortable="true" />
			 
			 <fmtBuiltSetPartsReport:message key="LBL_QTY" var="varQuantity"/>
			 <display:column property="QTY" title="${varQuantity}"  class="alignleft" sortable="true"  />
			
			</display:table>
			</td>
		</tr>
    <table>
       
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc"  %>
</BODY>

</HTML>