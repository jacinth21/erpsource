 <%
/*********************************************************************************************************
 * File		 		: GmInvLocationPartMappingReport.jsp
 * Desc		 		: This screen is used to display Location and Part Mapping
 * Version	 		: 1.0
 * author			: Vamsi Kiran Nagavarapu
**********************************************************************************************************/
%>

<!-- \operations\inventory\GmInvLocationPartMappingReport.jsp -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ taglib prefix="fmtInvLocPartMapRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtInvLocPartMapRpt:setLocale value="<%=strLocale%>"/>
<fmtInvLocPartMapRpt:setBundle basename="properties.labels.operations.inventory.GmInvLocationPartMappingReport"/>



<bean:define id="gridData" name="frmLocation" property="gridXmlData" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmLocation" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="currQtyFlag" name="frmLocation" property="chkCurrQtyFlag" type="java.lang.String"></bean:define>
<bean:define id="bldFlag" name="frmLocation" property="bldFlag" type="java.lang.String"></bean:define>

<%
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	String strWikiTitle = GmCommonClass.getWikiTitle("LOCATION_PART_MAPPING");
	String strOperationsInventoryJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Status Log Detail</TITLE>

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>




<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxCombo.js"></script>
<script language="JavaScript" src="<%=strOperationsInventoryJsPath%>/GmInvLocationPartSetMappingRpt.js"></script>
<script>

var objGridData = '<%=gridData%>';
var gridObj;
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeypress="fnEnterPress();">

<html:form action="/gmlocation.do?method=fetchInvLocationPartMapping">
	<html:hidden property="strOpt" />
	<html:hidden property="strInputString" value=""/>
	<html:hidden property="haction" value="rptReload" />
	<html:hidden property="locMap" />
	<html:hidden property="hshowFilters" />

	<table border="0" width="1100" cellspacing="0" cellpadding="0" style="border: 1px solid  #676767;">
		<tr>
	
	

		
			<td colspan ="12" height="25" class="RightDashBoardHeader"><fmtInvLocPartMapRpt:message key="LBL_LOCATION_PART_MAPPING"/></td>
			<td align="right" class=RightDashBoardHeader>
			<fmtInvLocPartMapRpt:message key="IMG_ALT_HELP" var="varHelp"/>
			<img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
			
			<tr>
			<td colspan="13" class="Line" height="1"></td>
			</tr>
				<!-- Custom tag lib code modified for JBOSS migration changes -->
				<tr id="filRow1" height="30"> 
				
				<fmtInvLocPartMapRpt:message key="LBL_WAREHOUSE" var="varWarehouse"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varWarehouse}" td="true"/>
				<td>&nbsp;<gmjsp:autolist comboType= "DhtmlXCombo" controlName="warehouseid"
					SFFormName="frmLocation" SFSeletedValue="warehouseid"
					SFValue="alWareHouse" codeId="ID" codeName="WHNM"
					defaultValue="[Choose One]" tabIndex="1"/> 
					</td>
					<fmtInvLocPartMapRpt:message key="LBL_LOCATION" var="varLocation"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varLocation}" td="true"/>
				<td>&nbsp;<html:text
					property="locationID" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" 
					onblur="changeBgColor(this,'#ffffff');" tabindex="2"/></td>
					
					<fmtInvLocPartMapRpt:message key="LBL_PART" var="varPart"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varPart}" td="true"/>
				<td>&nbsp;<html:text
					property="partNum" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');" tabindex="3"/>
				</td>
				<fmtInvLocPartMapRpt:message key="LBL_ZONE" var="varzone"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varzone}" td="true"/>
				<td colspan="2">&nbsp;<gmjsp:autolist comboType= "DhtmlXCombo" controlName="zoneNum"
					SFFormName="frmLocation" SFSeletedValue="zoneNum"
					SFValue="alZones" codeId="CODEID" codeName="CODENMALT"
					defaultValue="[Choose One]" tabIndex="4"/> 
					</td>
				<fmtInvLocPartMapRpt:message key="LBL_SHELF" var="varShelf"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varShelf}" td="true"/>
				<td>&nbsp;<html:text
					property="shelf" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');fnCheck(this.value);" tabindex="5"/>	
				</td>
				
			</tr>
			<tr>
				<td colspan="13" class="LLine" height="1"></td>
			</tr>
			
			<tr class  = "Shade" id="filRow2" height="30">
			<fmtInvLocPartMapRpt:message key="LBL_TYPE" var="varType"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varType}" td="true"/>
				<td>&nbsp;<gmjsp:autolist comboType= "DhtmlXCombo" controlName="typeNum"
					SFFormName="frmLocation" SFSeletedValue="typeNum"
					SFValue="alInvLocTypes" codeId="CODEID" codeName="CODENM"
					defaultValue="[Choose One]" tabIndex="6"/> 
					</td>	
					<fmtInvLocPartMapRpt:message key="LBL_STATUS" var ="varStatus"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varStatus}" td="true"/>
				<td>&nbsp;<gmjsp:autolist comboType= "DhtmlXCombo" controlName="statusNum" 
					SFFormName="frmLocation" SFSeletedValue="statusNum"
					SFValue="alStatus" codeId="CODEID" codeName="CODENM"
					defaultValue="[Choose One]" tabIndex="7"/> 
				</td>
				
				<fmtInvLocPartMapRpt:message key="LBL_AISLE" var ="varAisle"/>
				<gmjsp:label type="BoldText"  SFLblControlName="${varAisle}" td="true"/>
				<td>&nbsp;<html:text
					property="aisleNum" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');fnAisleCheck(this.value);" onchange="fnUpper(this)" onkeypress="fnUpper(this)"  tabindex="8"/>
				</td>
				
				<td colspan="3"><b><fmtInvLocPartMapRpt:message key="LBL_CURRQTY"/>=0</b>&nbsp;<html:checkbox property="chkCurrQtyFlag" onclick="fnRptReload()" tabindex="9"></html:checkbox></td>
				
				<%if(bldFlag.equals("Y")){ %>
				<fmtInvLocPartMapRpt:message key="LBL_BUILDING" var="varBuilding"/>
				<gmjsp:label type="BoldText" SFLblControlName="${varBuilding}" td="true"/>
				<td>&nbsp;<gmjsp:dropdown controlName="buildingID"
					SFFormName="frmLocation" SFSeletedValue="buildingID"
					SFValue="alBuildingID" codeId="BID" codeName="BNM"
					defaultValue="[Choose One]" tabIndex="1" />
				<% } %>
				
				<fmtInvLocPartMapRpt:message key="LBL_LOAD" var="varLoad"/>
				
				<td align="center" colspan="4" height="25"><gmjsp:button
					value="&nbsp;${varLoad}&nbsp;" name="Btn_Submit" gmClass="button"
					onClick="fnRptReload();" tabindex="10" buttonType="Load" /></td>	
			
			</tr>
			<tr>
				<td colspan="13" class="Line" height="1"></td>
			</tr>
    <%if(gridData.indexOf("cell")!=-1){%>
		
		       <tr>
			
				<td colspan="15">

						<div id="dataGrid" width = "1100" height ="400" style="width: 100%">
						
						</div>

				</td>
				
				</tr>
				
					<tr>
					
					
					
				            <td colspan="13" align="center" height="25" >
				                <div class='exportlinks'><fmtInvLocPartMapRpt:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
				                                onclick="fnExcelExport();"> <fmtInvLocPartMapRpt:message key="LBL_EXCEL"/> </a></div>
				            </td>
	
					</tr>
					
			<tr>
				<td colspan="13" class="Line" height="1"></td>
			</tr>
			<tr class="Shade"  id="saveBtnRow">
			
			<fmtInvLocPartMapRpt:message key="LBL_PRINT" var="varPrint"/>
			
				<td class="Shade" colspan="13" height="30" >
					<center>
						<gmjsp:button value="&nbsp;${varPrint}&nbsp;" gmClass="button" onClick="fnPrint();" tabindex="10" buttonType="Load" />
					</center>
				</td>
			</tr>
			
	<%}else if(strOpt.equals("view")){%>
	<tr>
		<td class="Shade" colspan="11" height="30" align="Center">
				<font color="red"><fmtInvLocPartMapRpt:message key="LBL_PART_NUM_NOT_MAPPED"/> </font> 
		</td>
	</tr>
	<%}%>
	<tr class="Shade" id="closeBtnRow">
	
	<fmtInvLocPartMapRpt:message key="LBL_CLOSE" var="varClose"/>
	
			<td class="Shade" colspan="13" height="30" >
				<center>
					<gmjsp:button controlId ="closebtn"	value="&nbsp;${varClose}&nbsp;" name="closebtn" gmClass="button" onClick="javascript:window.close();" tabindex="10" buttonType="Load" />
				</center>
			</td>
        <td></td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>