<%
/**********************************************************************************
 * File		 		: GmLotTrackReport.jsp
 * Desc		 		: This screen is used fetch the lot tracking information
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>
<!--\operations\inventory\GmLotTrackReport.jsp  -->


<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="gridData" name="frmLotTrackReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<%@ taglib prefix="fmtLotTrackReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@page import="java.util.Date"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<fmtLotTrackReport:setLocale value="<%=strLocale%>"/>
<fmtLotTrackReport:setBundle basename="properties.labels.operations.inventory.GmLotTrackReport"/>

<%
    String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");

	String strWikiTitle = GmCommonClass.getWikiTitle("LOT_TRACK_REPORT");
	String strApplDateFmt = strGCompDateFmt;
	String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	
    Date dtToday = GmCommonClass.getCurrentDate(strApplDateFmt);
    String strTodaysDate = "";
	strTodaysDate = GmCommonClass.getStringFromDate(dtToday,strApplDateFmt);
	   //The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getCmpid())+"\",\"partyid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPartyid())+"\",\"plantid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPlantid())+"\"}";
    strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Lot Tracking Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
	table.DtTable1400 {
		margin: 0 0 0 0;
		width: 1350px;
		border: 1px solid  #676767;
	}
</style>
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLotTrackReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<style type="text/css" media="all">
    html, body {
    width: 99%;
    height: 94%;
    padding: 0px;
}
</style> 
<script>
	var objGridData = '<%=gridData%>';
	var format = '<%=strApplDateFmt%>';
	var todaysdate = '<%=strTodaysDate%>';
	var companyInfoObj = '<%=strCompanyInfo%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad()"  onkeypress="fnEnter();">

<html:form action="/gmLotTrackReport.do?method=fetchLotTrackingDetails">
	<html:hidden property="strOpt" name="frmLotTrackReport"/>
	<input type="hidden" name="hSearch" value="<%=strSearch%>">
	<table class="DtTable1400" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="8"><table cellspacing="0" cellpadding="0" width="100%"><tr>
					<td height="25" class="RightDashBoardHeader" >&nbsp;<fmtLotTrackReport:message key="LBL_LOT_TRACKING_REPORT"/></td>
					<td align="right" class=RightDashBoardHeader><fmtLotTrackReport:message key="IMG_ALT_HELP" var="varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
						title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
					</td>
				</tr></table></td>
			</tr>
			<tr>
				<td class="RightTableCaption" HEIGHT="30" align="right" width="8%">
						<fmtLotTrackReport:message key="LBL_PART" var="varPart"/><gmjsp:label type="RegularText" SFLblControlName="${varPart} #:" td="false" />&nbsp;</td>
				<td width="20%"><html:text property="partNum" styleClass="InputArea" name="frmLotTrackReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="1" size="15"/>&nbsp;
				<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="2">
							<option value="0" ><fmtLotTrackReport:message key="OPT_CHOOSE_ONE"/>
							<option value="LIT" ><fmtLotTrackReport:message key="LBL_LITERAL"/>
							<option value="LIKEPRE" ><fmtLotTrackReport:message key="LBL_LIKE_PREFIX"/>
							<option value="LIKESUF" ><fmtLotTrackReport:message key="LBL_LIKE_SUFFIX"/>
						</select>
						</td>
				<td class="RightTableCaption" HEIGHT="30" align="right" width="10%">
						<fmtLotTrackReport:message key="LBL_CONT_NUM" var="varContNum"/><gmjsp:label type="RegularText" SFLblControlName="${varContNum}:" td="false" />&nbsp;</td>
				<td width="20%"><html:text property="ctrlNum" styleClass="InputArea" name="frmLotTrackReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="3" size="15"/>
						</td>
				<td class="RightTableCaption" HEIGHT="30" align="right" width="10%">
						<fmtLotTrackReport:message key="LBL_DONOR" var="varDonor"/><gmjsp:label type="RegularText" SFLblControlName="${varDonor} #:" td="false" />&nbsp;</td>
				<td width="10%"><html:text property="donorNum" styleClass="InputArea" name="frmLotTrackReport" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="4" size="15"/>
						</td>
				<td class="RightTableCaption" HEIGHT="30" align="right" width="8%">
						<fmtLotTrackReport:message key="LBL_STATUS" var="varSatus"/><gmjsp:label type="RegularText" SFLblControlName="${varSatus}:" td="false" />&nbsp;</td>
				<td width="14%"><gmjsp:dropdown controlName="lotType" SFFormName="frmLotTrackReport" SFSeletedValue="lotType"
						SFValue="alStatus" codeId = "CODEID"  codeName = "CODENM" tabIndex="5"/>
						</td>
			</tr>
			<tr><td class="LLine" height="1" colspan="8"></td></tr>	
			<tr class="shade">
				<td class="RightTableCaption" HEIGHT="30" align="right">
						<fmtLotTrackReport:message key="LBL_WAREHOUSE" var="varWarehouse"/><gmjsp:label type="RegularText" SFLblControlName="${varWarehouse}:" td="false" />&nbsp;</td>
				<td><gmjsp:dropdown controlName="warehouse" SFFormName="frmLotTrackReport" SFSeletedValue="warehouse"
						SFValue="alWareHouse" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="6" width="220" onChange="javascript:fnChangeConsignee(this)"/>
						</td>
				<td class="RightTableCaption" HEIGHT="30" align="right">
						<fmtLotTrackReport:message key="LBL_EXP_DT" var="varExpDt"/><gmjsp:label type="RegularText" SFLblControlName="${varExpDt}:" td="false" />&nbsp;</td>
				<td><gmjsp:dropdown controlName="expDateRange" SFFormName="frmLotTrackReport" SFSeletedValue="expDateRange"
						SFValue="alExpDateRange" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="7" width="105"/> 
				<gmjsp:calendar SFFormName="frmLotTrackReport" controlName="expDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="7"/>		
				</td>
				<td class="RightTableCaption" HEIGHT="30" align="right">
						<fmtLotTrackReport:message key="LBL_INT_USE" var="varIntUse"/><gmjsp:label type="RegularText" SFLblControlName="${varIntUse}:" td="false" />&nbsp;</td>
				<td><html:checkbox property="internationalUse" name="frmLotTrackReport" tabindex="8"/></td>		
				<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<fmtLotTrackReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button" style="width: 5em; height: 23px; font-family: Trebuchet MS, arial, sans-serif;font-size : 9pt" onClick="javascript:fnLoad();" tabindex="10" buttonType="Load" /></td>
			</tr>
			
			<tr><td class="LLine" height="1" colspan="8"></td></tr>	
			<tr>
				<td class="RightTableCaption" HEIGHT="30" align="right">
						<fmtLotTrackReport:message key="LBL_CONSIG_NAM" var="varConsigNam"/><gmjsp:label type="RegularText" SFLblControlName="${varConsigNam}:" td="false" />&nbsp;</td>
				<td id="chooseOne_blk"  style="display: block" colspan="7"><select name = "consigneeNm" class = "RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="9">
							<option value="0" ><fmtLotTrackReport:message key="OPT_CHOOSE_ONE"/></option></select>		
				</td>
				<td id="consignee_blk" style="display: none" colspan="7"><gmjsp:dropdown controlName="fieldSalesNm" SFFormName="frmLotTrackReport" SFSeletedValue="fieldSalesNm"
						SFValue="alFieldSales" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]" tabIndex="9" /> 		
				</td>
				<td id="account_blk"  style="display: none" colspan="7"><gmjsp:dropdown controlName="accountNm" SFFormName="frmLotTrackReport" SFSeletedValue="accountNm"
						SFValue="alAccounts" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]" tabIndex="9"/> 		
				</td>
				
			</tr>
			<tr><td class="LLine" height="1" colspan="8"></td></tr>	
			<tr class="shade" id="shipping_tr">
			  <td class="RightTableCaption" HEIGHT="30" align="left" colspan="8">
				&nbsp;&nbsp;<fmtLotTrackReport:message key="LBL_SHIPPED_DATE_RANGE" var="varShippedDateRange"/><gmjsp:label type="RegularText" SFLblControlName="${varShippedDateRange}:" td="false" />
				 &nbsp;<fmtLotTrackReport:message key="LBL_FROM" var="varShippedFromDate"/><gmjsp:label type="RegularText" SFLblControlName="${varShippedFromDate}:" td="false" />
                       <gmjsp:calendar SFFormName="frmLotTrackReport" controlName="shippedFromDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="7"/>
				 &nbsp;<fmtLotTrackReport:message key="LBL_TO" var="varShippedToDate"/><gmjsp:label type="RegularText" SFLblControlName="${varShippedToDate}:" td="false" />
				       <gmjsp:calendar SFFormName="frmLotTrackReport" controlName="shippedToDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="7"/>
			  </td>
            </tr>
			<tr><td class="LLine" height="1" colspan="8"></td></tr>
			<%if(gridData.indexOf("cell") != -1) {%> 
				<tr>
					<td colspan="8"><div  id="dataGridDiv" style="" height="400px"></div>
					<div id="pagingArea" width="870px"></div></td>
				</tr>
				<tr>						
					<td align="center" colspan="8"><br>
	             			<div class='exportlinks'><fmtLotTrackReport:message key="LBL_EXP_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"> <fmtLotTrackReport:message key="LBL_EXCEL"/> </a>	                         
	       			</td>
       			<tr>
			<%}else  if(!gridData.equals("")){%>
				<tr height="25px"><td colspan="8" align="center" class="RightText" ><fmtLotTrackReport:message key="LBL_NOTHING"/></td></tr>
			<%} %>
	</table>
<%@ include file="/common/GmFooter.inc"%>
</html:form>
</BODY>
</HTML>