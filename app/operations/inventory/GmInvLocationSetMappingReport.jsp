 <!-- \operations\inventory\GmInvLocationSetMappingReport.jsp -->
 <%
/*********************************************************************************************************
 * File		 		: GmInvLocationSetMappingReport.jsp
 * Desc		 		: This screen is used to display Location and Set Mapping
 * Version	 		: 1.0
 * author			: 
**********************************************************************************************************/
%>

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtInvLocationSetMappingReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtInvLocationSetMappingReport:setLocale value="<%=strLocale%>"/>
<fmtInvLocationSetMappingReport:setBundle basename="properties.labels.operations.inventory.GmInvLocationSetMappingReport"/>

<bean:define id="gridData" name="frmLocation" property="gridXmlData" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmLocation" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="currQtyFlag" name="frmLocation" property="chkCurrQtyFlag" type="java.lang.String"></bean:define>
   
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("LOCATION_SET_MAPPING");
    String strOperationsInventoryJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Status Log Detail</TITLE>

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>



<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDhtmlxCombo.js"></script>
<script language="JavaScript" src="<%=strOperationsInventoryJsPath%>/GmInvLocationPartSetMappingRpt.js"></script>
<script>

var objGridData = '<%=gridData%>';
var gridObj;

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();" onkeypress="fnEnterPress();">

<html:form action="/gmlocation.do?method=fetchInvLocationPartMapping">
	<html:hidden property="strOpt" />
	<html:hidden property="strInputString" value=""/>
	<html:hidden property="haction" />
	<html:hidden property="locMap" />
	<html:hidden property="hshowFilters" />

	<table border="0" class="DtTable950"  cellspacing="0" cellpadding="0" style="border: 1px solid  #676767;">
		<tr>
			<td colspan ="8" height="25" class="RightDashBoardHeader"><fmtInvLocationSetMappingReport:message key="LBL_LOC_SET_MAP"/></td>
			<td align="right" class=RightDashBoardHeader><fmtInvLocationSetMappingReport:message key="IMG_ALT_HELP" var ="varHelp"/><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
			
			<tr>
			<td colspan="9" class="Line" height="1"></td>
			</tr>
				<!-- Custom tag lib code modified for JBOSS migration changes -->
				<tr id="filRow1" height="30">
				<fmtInvLocationSetMappingReport:message key="LBL_WAREHOUSE" var="varWarehouse"/><gmjsp:label type="BoldText"  SFLblControlName="${varWarehouse}:" td="true"/>
				<td>&nbsp;<gmjsp:autolist comboType= "DhtmlXCombo" controlName="warehouseid"
					SFFormName="frmLocation" SFSeletedValue="warehouseid"
					SFValue="alWareHouse" codeId="ID" codeName="WHNM"
					defaultValue="[Choose One]" tabIndex="1"/>&nbsp; 
					</td>
				 
				<fmtInvLocationSetMappingReport:message key="LBL_LOCATION" var="varLocation"/><gmjsp:label type="BoldText"  SFLblControlName="${varLocation}:" td="true"/>
				<td>&nbsp;<html:text
					property="locationID" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" 
					onblur="changeBgColor(this,'#ffffff');" tabindex="1" /></td>
				<fmtInvLocationSetMappingReport:message key="LBL_TAG_ID" var="varTagId"/><gmjsp:label type="BoldText"  SFLblControlName="${varTagId}:" td="true"/>
				<td class="RightTableCaption" >&nbsp;<html:text
					property="tagID" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');" tabindex="2"/>&nbsp;</td>
					<fmtInvLocationSetMappingReport:message key="LBL_SET_ID" var="varSetId"/><gmjsp:label type="BoldText"  SFLblControlName="${varSetId}:" td="true"/>
					<td>&nbsp;<html:text
					property="setID" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');" tabindex="3"/>
				</td>
				
			</tr>
			<tr>
				<td colspan="9" class="LLine" height="1"></td>
			</tr>
			
			<tr class  = "Shade" id="filRow2" height="30">
			<fmtInvLocationSetMappingReport:message key="LBL_ZONE" var="varZone"/><gmjsp:label type="BoldText"  SFLblControlName="${varZone}:" td="true"/>
				<td>&nbsp;<gmjsp:autolist comboType= "DhtmlXCombo" controlName="zoneNum"
					SFFormName="frmLocation" SFSeletedValue="zoneNum"
					SFValue="alZones" codeId="CODEID" codeName="CODENMALT"
					defaultValue="[Choose One]" tabIndex="4"/> 
					</td>
			
				<fmtInvLocationSetMappingReport:message key="LBL_TYPE" var="varType"/><gmjsp:label type="BoldText"  SFLblControlName="${varType}:" td="true"/>
				<td>&nbsp;<gmjsp:autolist comboType= "DhtmlXCombo" controlName="typeNum"
					SFFormName="frmLocation" SFSeletedValue="typeNum"
					SFValue="alInvLocTypes" codeId="CODEID" codeName="CODENM"
					defaultValue="[Choose One]" tabIndex="5"/> 
					</td>	
				<fmtInvLocationSetMappingReport:message key="LBL_SHELF" var="varShelf"/><gmjsp:label type="BoldText"  SFLblControlName="${varShelf}:" td="true"/>
				<td>&nbsp;<html:text
					property="shelf" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');fnCheck(this.value);" tabindex="6"/>	
				</td>
				<fmtInvLocationSetMappingReport:message key="LBL_AISLE" var="varAisle"/><gmjsp:label type="BoldText"  SFLblControlName="${varAisle}:" td="true"/>
				<td>&nbsp;<html:text
					property="aisleNum" size="15"
					onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
					onblur="changeBgColor(this,'#ffffff');fnAisleCheck(this.value);" tabindex="7"/>
				</td>
				
				<td align="center" colspan="2" height="25">&nbsp;<fmtInvLocationSetMappingReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button
					value="&nbsp;${varLoad}&nbsp;" name="Btn_Submit" gmClass="button"
					onClick="fnRptReload();" tabindex="8" buttonType="Load" />&nbsp;</td>	
			
			</tr>
			
<%if(gridData.indexOf("cell")!=-1){%>
		
		       <tr>
			
				<td colspan="9">

						<div id="dataGrid" width = "900" height ="400">
						
						</div>

				</td>
				
				</tr>
				
					<tr>
				            <td colspan="9" align="center" height="25" >
				                <div class='exportlinks'><fmtInvLocationSetMappingReport:message key="LBL_EXP_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
				                                onclick="fnExcelExport();"><fmtInvLocationSetMappingReport:message key="LBL_EXCEL"/>  </a></div>
				            </td>
	
					</tr>
					
			<tr>
				<td colspan="9" class="Line" height="1"></td>
			</tr>
			<tr class="Shade"  id="saveBtnRow">
				<td class="Shade" colspan="9" height="30" >
					<center>
						<fmtInvLocationSetMappingReport:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="&nbsp;${varPrint}&nbsp;" gmClass="button" onClick="fnPrint();" tabindex="9" buttonType="Load" />
					</center>
				</td>
			</tr>
			
	<%}else if(strOpt.equals("view")){%>
	<tr>
		<td class="Shade" colspan="9" height="30" align="Center">
				<font color="red"><fmtInvLocationSetMappingReport:message key="LBL_SET_ID_MAPPED"/></font> 
		</td>
	</tr>
	<%}%>
	<tr class="Shade" id="closeBtnRow">
			<td class="Shade" colspan="9" height="30" >
				<center>
					<fmtInvLocationSetMappingReport:message key="BTN_CLOSE" var="varClose"/><gmjsp:button controlId ="closebtn"	value="&nbsp;${varClose}&nbsp;" name="closebtn" gmClass="button" onClick="javascript:window.close();" tabindex="9" buttonType="Load"/>
				</center>
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>