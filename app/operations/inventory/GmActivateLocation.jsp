
<%
	/**********************************************************************************
 * File		 		: GmActivateLocation.jsp
 * Desc		 		: This screen is used to Add the Location
 * Version	 		: 1.0
 * author			: MSelvamani
************************************************************************************/
%>

<!--\operations\inventory\GmActivateLocation.jsp  -->
<%@ page language="java"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtActivateLocation"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<fmtActivateLocation:setLocale value="<%=strLocale%>" />
<fmtActivateLocation:setBundle
	basename="properties.labels.operations.inventory.GmActivateLocation" />
<bean:define id="strWareHouseId" name="frmActionLocation"
	property="strWareHouseId" type="java.lang.String" />
<bean:define id="strLocationId" name="frmActionLocation"
	property="strLocationId" type="java.lang.String" />
<bean:define id="strStatusType" name="frmActionLocation"
	property="strStatusType" type="java.lang.String" />
<bean:define id="strSuccess" name="frmActionLocation"
	property="strSuccess" type="java.lang.String" />
<bean:define id="strError" name="frmActionLocation" property="strError"
	type="java.lang.String" />
	<bean:define id="alWareHouse" name="frmActionLocation" property="alWareHouse" type="java.util.ArrayList"/>
<bean:define id="alStatus" name="frmActionLocation" property="alStatus" type="java.util.ArrayList"/>



<%
	String strOperationsInventoryJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");
String strWikiTitle = GmCommonClass.getWikiTitle("LOCATION_EDIT");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Activate/Inactivate Location</TITLE>

<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript"
	src="<%=strOperationsInventoryJsPath%>/GmActivateLocation.js"></script>
<script>
	var lblWarehouse = '<fmtActivateLocation:message key="LBL_WAREHOUSE"/>';
	var lblLocId = '<fmtActivateLocation:message key="LBL_LOCID"/>';
	var lblStatus = '<fmtActivateLocation:message key="LBL_STATUS"/>';
</script>
<style>
input[type="text"]:disabled {
	background: #dddddd ! important;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10">

	<html:form action="/gmActivateLocation.do?method=saveLocationStatus">

		<table width="700" cellspacing="0" cellpadding="0"
			style="border: 1px solid #676767;">
			<tr>
				<td height="25" class="RightDashBoardHeader"><fmtActivateLocation:message
						key="LBL_ACTIVATE_LOCATION" /></td>
				<td align="right" class=RightDashBoardHeader><fmtActivateLocation:message
						key="IMG_ALT_HELP" var="varHelp" /><img id='imgEdit' align="right"
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td colspan="7" class="Line" height="1"></td>

			</tr>
			<tr>
				<td colspan="7" class="LLine" height="1"></td>

			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->

			<tr class="Shade" height="25">
				<fmtActivateLocation:message key="LBL_WAREHOUSE" var="varWarehouse" />
				<gmjsp:label type="MandatoryText" SFLblControlName="${varWarehouse}"
					td="true" />
				<td width="70%">&nbsp;<gmjsp:dropdown
						controlName="strWareHouseId" SFFormName="frmActionLocation"
						SFSeletedValue="strWareHouseId" SFValue="alWareHouse" codeId="ID"
						codeName="WHNM" defaultValue="[Choose One]" tabIndex="1" />
				</td>
			</tr>

			<tr>
				<td colspan="7" class="LLine" height="1"></td>

			</tr>

			<tr class="Shade" height="25">
				<fmtActivateLocation:message key="LBL_LOCID" var="varlocid" />
				<gmjsp:label type="MandatoryText" SFLblControlName="${varlocid}"
					td="true" />
				<td width="70%">&nbsp;<html:textarea property="strLocationId"
						rows="5" cols="45" onfocus="changeBgColor(this,'#AACCE8');"
						styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"
						tabindex="2" /></td>
			</tr>
			<tr>
				<td colspan="7" class="LLine" height="1"></td>

			</tr>
			<tr class="Shade" height="25">
				<fmtActivateLocation:message key="LBL_STATUS" var="varType" />
				<gmjsp:label type="MandatoryText" SFLblControlName="${varType}"
					td="true" />
				<td height="25">&nbsp;<gmjsp:dropdown controlName="strStatusType"
						SFFormName="frmActionLocation" SFSeletedValue="strStatusType"
						SFValue="alStatus" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="6" /></td>
			</tr>

			<tr>
				<td colspan="7" class="LLine" height="1"></td>

			</tr>

			<tr height="25" class="Shade">
				<td align="center" colspan="7" height="30"><fmtActivateLocation:message
						key="BTN_SUBMIT" var="varSubmit" />
					<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="loadbtn"
						tabindex="8" gmClass="button" onClick="javascript:fnSubmit();"
						buttonType="submit" /></td>

			</tr>
			<logic:notEmpty name="frmActionLocation" property="strSuccess">
				<tr>
					<td class="Line" colspan="2"></td>
				</tr>
				<tr>
					<td height="30" align="center" colspan="2"><font color="green"><b><bean:write
									name="frmActionLocation" property="strSuccess" /></b></font></td>
				</tr>
			</logic:notEmpty>
			<logic:notEmpty name="frmActionLocation" property="strError">
				<tr>
					<td class="Line" colspan="2"></td>
				</tr>
				<tr>
					<td height="30" align="center" colspan="2"><font color="red"><b><bean:write
									name="frmActionLocation" property="strError" /></b></font></td>
				</tr>
			</logic:notEmpty>

		</table>



		</div>

		</center>
		</td>
		</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>