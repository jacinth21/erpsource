 <%
/**********************************************************************************
 * File		 		: GmRerunTxnSplit.jsp
 * Desc		 		: This screen is used to split the transactions manually 
 * Version	 		: 1.0
 * author			: Swetha SampathKumar
************************************************************************************/
%>

<!--\operations\inventory\GmRerunTxnSplit.jsp  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtRerunTxnSplit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtRerunTxnSplit:setLocale value="<%=strLocale%>"/>
<fmtRerunTxnSplit:setBundle basename="properties.labels.operations.inventory.GmRerunTxnSplit"/>

<%
    String strOperationsInventoryJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");
	String strWikiTitle = GmCommonClass.getWikiTitle("RERUN_TXN_SPLIT");
%>

<bean:define id="strTxnId" name="frmRerunTxnSplit" property="strTxnId" type="java.lang.String"></bean:define>
<bean:define id="strTxnType" name="frmRerunTxnSplit" property="strTxnType" type="java.lang.String"></bean:define>
<bean:define id="alTxnTypeList" name="frmRerunTxnSplit" property="alTxnTypeList" type="java.util.ArrayList"></bean:define>


<HTML>
<HEAD>
<TITLE>Globus Medical: Rerun Transaction Split</TITLE>

<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsInventoryJsPath%>/GmRerunTxnSplit.js"></script> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">

<style>
#successCont {
	color:red;
}
</style>


</HEAD>

<BODY leftmargin="20" topmargin="10">
<form name="frmRerunTxnSplit" action="/gmRerunTxnSplitAction.do?method=fetchRerunTxnDetails">
	<table  width="600" cellspacing="0" cellpadding="0" style="border: 1px solid  #676767;">
		<tr>
			<td colspan ="2" style="width: 70%;" height="25" class="RightDashBoardHeader"><fmtRerunTxnSplit:message key="LBL_RERUN_TRANSACTION_SPLIT"/></td>
			<td align="right" class=RightDashBoardHeader><fmtRerunTxnSplit:message key="IMG_ALT_HELP" var="varHelp"/><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<td  colspan="7" class="Line" height="1"></td>
			
		</tr>
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
			
		</tr>
		
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="Shade" height="25" style="width: 30%;"><fmtRerunTxnSplit:message key="LBL_TRANSACTION_ID" var="varTransactionId"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varTransactionId}" td="true"/>
			<td colspan="2">:
			 <input type="text" size="30" value="<%=strTxnId%>" name="strTxnId" class="InputArea" style="text-transform: uppercase;" tabIndex="1" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"></td>
		</tr>
					
      <tr height="25" style="width: 30%;"><fmtRerunTxnSplit:message key="LBL_TRANSACTION_TYPE" var="varTransactionType"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varTransactionType}" td="true"/>
			<td>: 
			<gmjsp:dropdown controlName="strTxnType"
				SFFormName="frmRerunTxnSplit" SFSeletedValue="strTxnType"
				SFValue="alTxnTypeList" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" tabIndex="2" />
		   </td>
		</tr>		
				
	   <tr height="30" colspan="2" >
		<td align="center" colspan="7" height="30">
		<fmtRerunTxnSplit:message key="BTN_SUBMIT" var="varSubmit"/>
		<gmjsp:button
					value="&nbsp;${varSubmit}&nbsp;" name="btn_Submit" tabindex="3"
						gmClass="Button" onClick="fnSubmit();" buttonType="Save" /></td>
	 </tr>
	 <tr>
	 	 <td colspan="7"><div id="successCont" style="text-align:center;"></div></td>
	 
	 </tr>
 </table>
 
</form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>