 <%
/**********************************************************************************
 * File		 		: GmInvLocationAdd.jsp
 * Desc		 		: This screen is used to Add the Location
 * Version	 		: 1.0
 * author			: Vamsi Kiran Nagavarapu
************************************************************************************/
%>

<!--\operations\inventory\GmInvLocationAdd.jsp  -->
<%@ page language="java"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtInvLocationAdd" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtInvLocationAdd:setLocale value="<%=strLocale%>"/>
<fmtInvLocationAdd:setBundle basename="properties.labels.operations.inventory.GmInvLocationAdd"/>

<bean:define id="gridData" name="frmLocation"
	property="gridXmlData" type="java.lang.String">
</bean:define>
<bean:define id="bldFlag" name="frmLocation"
	property="bldFlag" type="java.lang.String">
</bean:define>
<%
    String strOperationsInventoryJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");
	String strWikiTitle = GmCommonClass.getWikiTitle("LOCATION_ADD");
	String	strDisable ="disabled";
	String bFlag = bldFlag;
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Status Log Detail</TITLE>

<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsInventoryJsPath%>/GmInvLocationAdd.js"></script>
<script>
var lblCol = '<fmtInvLocationAdd:message key="LBL_COL"/>';
var lblRow = '<fmtInvLocationAdd:message key="LBL_ROW"/>';
var lblColumns = '<fmtInvLocationAdd:message key="LBL_COLUMNS"/>';
var lblRows = '<fmtInvLocationAdd:message key="LBL_ROWS"/>';
var lblWarehouse = '<fmtInvLocationAdd:message key="LBL_WAREHOUSE"/>';
var lblBuilding = '<fmtInvLocationAdd:message key="LBL_BUILDING"/>';
var lblZone = '<fmtInvLocationAdd:message key="LBL_ZONE"/>';
var lblAisle = '<fmtInvLocationAdd:message key="LBL_AISLE"/>';
var lblShelf = '<fmtInvLocationAdd:message key="LBL_SHELF"/>';
var lblColumn ='<fmtInvLocationAdd:message key="LBL_COLUMN"/>';
var lblType = '<fmtInvLocationAdd:message key="LBL_TYPE"/>';
</script>
<style>
input[type="text"]:disabled {
  background: #dddddd ! important;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeypress="fnEnterPress();">

<html:form action="/gmlocation.do?method=addInvLocation">
	<html:hidden property="strOpt"  value=""/>
	<html:hidden property="strInputString" value=""/> 
	<html:hidden property="haction" value="G" />
	<html:hidden property="haction" value="G" />
	<input type="hidden" name="bldFlag" value="<%=bldFlag%>">

	<table  width="700" cellspacing="0" cellpadding="0" style="border: 1px solid  #676767;" >
		<tr>
			<td  height="25" class="RightDashBoardHeader"><fmtInvLocationAdd:message key="LBL_ADD_NEW_LOCATION"/></td>
			<td align="right" class=RightDashBoardHeader><fmtInvLocationAdd:message key="IMG_ALT_HELP" var="varHelp"/><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<td  colspan="7" class="Line" height="1"></td>
			
		</tr>
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
			
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<%if(bldFlag.equals("Y")){ %>
		<tr height="25"><fmtInvLocationAdd:message key="LBL_BUILDING" var="varBuilding"/>  <!-- Added for PMT-33507 Add or Edit location -->
		
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varBuilding}" td="true"/>
			<td width="70%">&nbsp;<gmjsp:dropdown controlName="buildingID"
				SFFormName="frmLocation" SFSeletedValue="buildingID"
				SFValue="alBuildingID" codeId="BID" codeName="BNM"
				defaultValue="[Choose One]" tabIndex="1" />
		   </td>
		</tr>
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
			
		</tr>
		<tr class="Shade" height="25"><fmtInvLocationAdd:message key="LBL_WAREHOUSE" var="varWarehouse"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varWarehouse}" td="true"/>
			<td width="70%">&nbsp;<gmjsp:dropdown controlName="warehouseid"
				SFFormName="frmLocation" SFSeletedValue="warehouseid"
				SFValue="alWareHouse" codeId="ID" codeName="WHNM"
				defaultValue="[Choose One]" tabIndex="1" />
		   </td>
		</tr>
		<%} else { %>
		<tr class="Shade" height="25"><fmtInvLocationAdd:message key="LBL_WAREHOUSE" var="varWarehouse"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varWarehouse}" td="true"/>
			<td width="70%">&nbsp;<gmjsp:dropdown controlName="warehouseid"
				SFFormName="frmLocation" SFSeletedValue="warehouseid"
				SFValue="alWareHouse" codeId="ID" codeName="WHNM"
				defaultValue="[Choose One]" tabIndex="1" />
		   </td>
		</tr>
		<%}%>
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
			
		</tr>
		<tr height="25">
		<fmtInvLocationAdd:message key="LBL_ZONE" var="varZone"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varZone}" td="true"/>
			<td width="70%">&nbsp;<gmjsp:dropdown controlName="zoneNum"
				SFFormName="frmLocation" SFSeletedValue="zoneNum"
				SFValue="alZones" codeId="CODEID" codeName="CODENMALT"
				defaultValue="[Created New Zone]" tabIndex="1" onChange="fnDisabledNewZone(this.value);"/>
				<html:text
				property="newZone" styleId="newZone" size="3" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" tabindex="2"/>
		   </td>
		</tr>
		<tr>
			<td  colspan="7" class="LLine" height="1"></td>
			
		</tr>		
		<tr class="Shade" height="25"><fmtInvLocationAdd:message key="LBL_AISLE" var="varAisle"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varAisle}" td="true"/>
			<td width="70%">&nbsp;<html:text
				property="aisleNum" size="30" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" tabindex="2" /></td>
		</tr>	
		<tr>
			<td  colspan="7" class="LLine" height="1"></td>
			
		</tr>
			
		<tr height="25"><fmtInvLocationAdd:message key="LBL_SHELF" var="varShelf"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varShelf}" td="true"/>
			<td width="70%">&nbsp;<html:text
				property="shelf" styleId="shelf" size="30" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" tabindex="3"/></td>
		</tr>	
		<tr>
			<td  colspan="7" class="LLine" height="1"></td>
			
		</tr>
			
		<tr class="Shade" height="25"><fmtInvLocationAdd:message key="LBL_ROWS" var="varRows"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varRows}" td="true"/>
			<td width="70%">&nbsp;<html:text
				property="rowNum" size="30" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" tabindex="4" /> 
			</td>
		</tr>
		<tr>
			<td  colspan="7" class="LLine" height="1"></td>
			
		</tr>
		
		<tr height="25"><fmtInvLocationAdd:message key="LBL_COLUMNS" var="varColumns"/>
			<gmjsp:label type="MandatoryText"  SFLblControlName="${varColumns}" td="true"/>
			<td width="70%">&nbsp;<html:text
				property="columnNum" size="30" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" tabindex="5"/> 
			</td>
		</tr>
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
			
		</tr>
		<tr class="Shade" height="25"><!-- Code is Commenting since this SPP Project is not Moving to Production-->
		<fmtInvLocationAdd:message key="LBL_TYPE" var="varType"/>
		<gmjsp:label type="MandatoryText"  SFLblControlName="${varType}" td="true"/>
			<td height="25">&nbsp;<gmjsp:dropdown controlName="typeNum"
				SFFormName="frmLocation" SFSeletedValue="typeNum" 
				SFValue="alInvLocTypes" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" tabIndex="6" /></td>
		</tr>
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
			
		</tr>
		
	<tr height="25">
		<fmtInvLocationAdd:message key="LBL_ACTFL" var="varActFl"/>
			<gmjsp:label SFLblControlName="${varActFl}" type="BoldText"/>
			<td>&nbsp;<html:checkbox name="frmLocation" property="activeFl" tabindex="7"></html:checkbox>
			</td>
	</tr>	
		<tr>
			<td colspan="7" class="LLine" height="1"></td>
		</tr>
		
	<tr height="25" class="Shade">
		<td align="center" colspan="7" height="30"><fmtInvLocationAdd:message key="BTN_LOAD" var="varLoad"/><gmjsp:button
					value="&nbsp;${varLoad}&nbsp;" name="loadbtn" tabindex="8"
						gmClass="button" onClick="javascript:fnGrid();" buttonType="Load" /></td>
					
	</tr>
	
</table>
 
<table id="tableData" width="700" cellspacing="0" cellpadding="0" style="display: none; border: 1px solid  #676767;">
<tr>
<td  height="15" class="RightDashBoardHeader"><fmtInvLocationAdd:message key="LBL_LOCATION_GRID"/></td>
</tr>
<tr>
<td>
<center>
<div id="divData"  align="center" width="690">

	<table>
		<tr>
			<td>
				<div id="dataGrid" height="200" width="690"></div>
			</td>
		</tr>
	
		<tr>
			<td align="center" height="30"><fmtInvLocationAdd:message key="BTN_SAVE" var="varSave"/>
			<gmjsp:button controlId ="submitbtn"
								value="&nbsp;${varSave}&nbsp;" name="submitbtn" tabindex="8"
									gmClass="button" onClick="javascript:fnSubmit();" buttonType="Save" />
			</td>
		</tr>
	
	</table>
  
</div>

</center>
</td>
</tr>
</table>	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>