<%@ page language="java"%>

<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>


<%@ taglib prefix="fmtAllocationListReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \operations\inventory\GmAllocationListReport.jsp -->

<fmtAllocationListReport:setLocale value="<%=strLocale%>"/>
<fmtAllocationListReport:setBundle basename="properties.labels.operations.inventory.GmAllocationListReport"/>


<bean:define id="gridData" name="frmAllocationList"
	property="gridXmlData" type="java.lang.String">
</bean:define>

<%
	String strWikiTitle = GmCommonClass.getWikiTitle("STATUS_LOG_REPORT");
	try {
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Status Log Detail</TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmStatusLog.js"></script>
<script>

var objGridData = '<%=gridData%>';
	alert(objGridData);
	
function fnOnPageLoad()
{
alert(" Im here");
	gridObj = initGrid('dataGrid',objGridData);
	 gridObj.setDateFormat("%m/%d/%Y");
}

function fnRptReload()
{
	document.frmAllocationList.haction.value = "rptReload";
	fnStartProgress('Y');
	document.frmAllocationList.submit();
}



function fnSubmit(){
	
		gridObj.editStop();// return value from editor(if presents) to cell and closes editor
		var gridrowid;
		var void_flag;
		var finalData='';
		var gridrows =gridObj.getChangedRows(",");
		
		if(gridrows.length==0){
			Error_Clear();	
			Error_Details(message_operations[407]);
			Error_Show();
			Error_Clear();
		}
		else
		{
		
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();
		
			
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
			
				void_flag = gridObj.getRowAttribute(gridrowid,"row_deleted");
				if(void_flag ==  undefined){
					void_flag='';
				}
			
				
				
				for(var col=0;col<columnCount;col++){
					
					finalData+=gridObj.cellById(gridrowid, col).getValue()+'^';
					
				}
				
				finalData+='|';
			}
			finalData+="$";
		
		
		document.frmAllocationList.strOpt.value = 'save';
		document.frmAllocationList.strFinalData.value = finalData;
		
		fnStartProgress('Y');
		document.frmAllocationList.submit();
		
		}
		
		
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">

<html:form action="/gmallocationlist.do">
	<html:hidden property="strOpt" />
	<html:hidden property="strFinalData" />
	<html:hidden property="haction" value="G" />

	<table border="0" class="DtTable850" width="700" cellspacing="0"
		cellpadding="0">
		<tr>
			<td colspan="5" height="25" class="RightDashBoardHeader"> <fmtAllocationListReport:message key="LBL_PICKQUEUEREPORT"/></td>
			
<fmtAllocationListReport:message key="IMG_ALT_HELP" var="varHelp"/>
			
			<td align="right" class=RightDashBoardHeader><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}' width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<td colspan="6" class="Line" height="1"></td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="30" class="RightTableCaption">&nbsp;&nbsp;<fmtAllocationListReport:message key="LBL_PICKTYPE"/>:</td>
			<td><gmjsp:dropdown controlName="typeNum"
				SFFormName="frmAllocationList" SFSeletedValue="typeNum"
				SFValue="alTypes" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" /></td>

			<td height="30" class="RightTableCaption"><fmtAllocationListReport:message key="LBL_ASSIGNEDTO"/>:</td>
			<td><gmjsp:dropdown controlName="userNum"
				SFFormName="frmAllocationList" SFSeletedValue="userNum"
				SFValue="alUsers" codeId="ID" codeName="NAME"
				defaultValue="[Choose One]" /></td>
			<td height="30" class="RightTableCaption">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtAllocationListReport:message key="LBL_STATUS"/>:&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><select name="statusNum" class="RightText"
				onFocus="changeBgColor(this,'#AACCE8');"
				onBlur="changeBgColor(this,'#ffffff');">
				
				
				<option value="0"><fmtAllocationListReport:message key="LBL_CHOOSE_ONE"/>
				
				
				<option value="93003"><fmtAllocationListReport:message key="LBL_ASSIGNED"/>
				
				
				<option value="Other"><fmtAllocationListReport:message key="LBL_OTHER"/>
			</select></td>
		</tr>
		<tr class="Shade">

			<td height="30" class="RightTableCaption">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtAllocationListReport:message key="LBL_TYPE"/>:&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><select name="dateType" class="RightText"
				onFocus="changeBgColor(this,'#AACCE8');"
				onBlur="changeBgColor(this,'#ffffff');">
				
				
				<option value="0"><fmtAllocationListReport:message key="LBL_CHOOSE_ONE"/>
				
				
				<option value="ALLOCATED"><fmtAllocationListReport:message key="LBL_ALLOCATED_DATE"/>
				
				
				<option value="PROCESSED"><fmtAllocationListReport:message key="LBL_PROCESSED_DATE"/>
			</select></td>


			<td height="25" class="RightTableCaption"><fmtAllocationListReport:message key="LBL_FORM"/>:&nbsp;</td>
			<td height="30" class="RightTableCaption"><html:text
				property="startDate" size="15"
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" /> <img id="Img_Date"
				style="cursor: hand"
				onclick="javascript:show_calendar('frmAllocationList.startDate');"
				title="Click to open Calendar"
				src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle"
				height=18 width=19 /></td>
			<td height="25" class="RightTableCaption">&nbsp;&nbsp;&nbsp; <fmtAllocationListReport:message key="LBL_TO"/>:</td>
			<td height="30" class="RightTableCaption"><html:text
				property="endDate" size="15"
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" /> <img id="Img_Date"
				style="cursor: hand"
				onclick="javascript:show_calendar('frmAllocationList.endDate');"
				title="Click to open Calendar"
				src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle"
				height=18 width=19 /></td>


		</tr>



		<tr  class="Shade">
		<fmtAllocationListReport:message key="LBL_LOAD" var="varLoad"/>
			<td align="center" colspan="7" height="30"><gmjsp:button
				value="&nbsp;${varLoad}&nbsp;" name="Btn_Submit" gmClass="button"
				onClick="fnRptReload();" buttonType="Load" /></td>
		</tr>
		<tr>
			<td colspan="6" class="Line" height="1"></td>
		</tr>
		
			<tr>
				<td colspan="6"></td>
			</tr>
			<tr>
				<td colspan="6" width="100%">

						<div id="dataGrid" style="height: 400px; width: 900px;"></div>

				</td></tr>
<tr class="Shade">
<fmtAllocationListReport:message key="LBL_SUBMIT" var="varSubmit"/>
	<td align="center" colspan="7" height="30"><gmjsp:button
				value="&nbsp;${varSubmit}&nbsp;" name="submitbtn" tabindex="11"
					gmClass="button" onClick="javascript:fnSubmit();" buttonType="Save" /></td>
				
				
			</tr>
		
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
<%
	} catch (Exception e) {
		e.printStackTrace();
	}
%>
</BODY>
</HTML>