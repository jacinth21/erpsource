 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: GmReworkListReport.jsp
 * Desc		 		: This screen is used for displaying Rework count
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>


<!--  \operations\inventory\GmReworkListReport.jsp-->

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtReworkListReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReworkListReport:setLocale value="<%=strLocale%>"/>
<fmtReworkListReport:setBundle basename="properties.labels.operations.inventory.GmReworkListReport"/>





<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
		
	final String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	final String strPartNumber = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	final String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	String strApplDateFmt = strGCompDateFmt;
	
	
	HashMap hmTemp = new HashMap();
	GmCalenderOperations gmCal = new GmCalenderOperations();
	gmCal.setTimeZone(strGCompTimeZone);
    String strDate = "";
	strDate = gmCal.getCurrentDate(strApplDateFmt+" K:m:s a");
	String strUserName = (String)session.getAttribute("strSessShName");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Built Set Inventory </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>




 
function fnGo()
{
	
	document.frmInv.hOpt.value = '<%=strhOpt%>'; 
	var pnum = document.frmInv.hPartNum.value;
	
	//if (pnum != '')
	//{
		//!document.frmInv.hOpt.value = "MultipleParts";
		//!document.frmInv.Cbo_ProjId.selectedIndex = 0;
	//}
	
	document.frmInv.hAction.value = "Go";
	fnStartProgress();
	document.frmInv.submit();
}



</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmInv" method="POST" action="<%=strServletPath%>/GmInvListReportServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">


	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtReworkListReport:message key="LBL_REWORK_LIST_REPORT"/></td>
		</tr>
		
		
		<tr>
			<td colspan="2"> 
				<jsp:include page="/operations/inventory/include/GmPartFilter.jsp" >
				<jsp:param name="hPartNum" value="<%=strPartNumber%>" />
				<jsp:param name="hSearch" value="<%=strSearch%>" />
				<jsp:param name="hProjId" value="<%=strProjId%>" />
			</jsp:include>
			</td>
		</tr>
		
		
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="requestScope.REPORT.rows" export="true" class="its" id="currentRowObject" decorator="org.displaytag.decorator.TotalTableDecorator"> 
			<display:setProperty name="export.excel.filename" value="ReworkListReport.xls" />
			<display:caption><fmtReworkListReport:message key="LBL_REPORT_AS_OF"/><%=strDate%> <fmtReworkListReport:message key="LBL_BY"/>:<%=strUserName%></display:caption>
				
				<fmtReworkListReport:message key="LBL_PART_NUMBER" var="varPartNumber"/>
				<display:column property="PNUM" title="${varPartNumber}" class="aligncenter" sortable="true" />
				
				<fmtReworkListReport:message key="LBL_DESCRIPTION" var="varDescription"/>
				<display:column property="PDESC" escapeXml="true" title="${varDescription}" class="alignleft"  />
				
				<fmtReworkListReport:message key="LBL_COUNT" var="varCount"/>
				<display:column property="PEND_CNT" title="${varCount}" sortable="true" class="alignright" />
				
				<fmtReworkListReport:message key="LBL_VENDOR" var="varVendor"/>
				<display:column property="VENDOR_NAME" title="${varVendor}" sortable="true" class="alignleft" />
				
				<fmtReworkListReport:message key="LBL_PURCHASE_ORDER_ID" var="varPurchaseOrderId"/>
				<display:column property="PURCHASE_ID" title="${varPurchaseOrderId}" sortable="true" class="alignleft" />
				
				<fmtReworkListReport:message key="LBL_WORK_ORDER_ID" var="varWorkOrderId"/>
				<display:column property="WORK_ID" title="${varWorkOrderId}" sortable="true" class="alignleft" />
			</display:table>
			</td>
		</tr>
    <table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>