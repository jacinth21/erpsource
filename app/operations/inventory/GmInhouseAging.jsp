<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.globus.common.servlets.GmServlet"%>



<!--\operations\inventory\GmInhouseAging.jsp  -->

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtInhouseAging" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtInhouseAging:setLocale value="<%=strLocale%>"/>
<fmtInhouseAging:setBundle basename="properties.labels.operations.inventory.GmInhouseAging"/>


<bean:define id="gridData" name="frmInhouseAgingReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
//strGridXmlData
GmServlet gm = new GmServlet();
gm.checkSession(response,session);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Globus Medical: Inhouse-Aging Report</title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmInhouseAging.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
var objGridData;
objGridData ='<%=gridData%>';
</script>


</head>
<body leftmargin="20" topmargin="10" onload ="javascript:fnOnPageLoad();">
<html:form action="/gmInhouseAging.do">
  <table border="0" cellspacing="0" cellpadding="0" class="DtTable765">
  	<tr>
  	
		<td colspan="5" height="25" class="Header">&nbsp;<fmtInhouseAging:message key="LBL_IN_HOUSE_AGING"/></td>
	</tr>
	<tr>
    	<td colspan="5"> 
        	<div  id ="dataGridDiv" style="grid" height="400px" width="850px"></div>
		</td>	
	</tr>
	<tr>
		<td height="50" colspan="5">
		
			<div align="center" height="100"><fmtInhouseAging:message key="LBL_EXPORT_OPTION"/>: 
			<fmtInhouseAging:message key="DIV_EXCEL"/>
         		<img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> 				
         	</div>
        </td>
   </tr>
  </table>
</html:form> 
</body>
</html>
<%@ include file="/common/GmFooter.inc" %>