 <%
/**********************************************************************************
 * File		 		: GmPartNumLockInvReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!-- \operations\inventory\GmPartNumLockInvReport.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>


<%@ taglib prefix="fmtPartNumLockInvReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartNumLockInvReport:setLocale value="<%=strLocale%>"/>
<fmtPartNumLockInvReport:setBundle basename="properties.labels.operations.inventory.GmPartNumLockInvReport"/>



<%
String strWikiTitle = GmCommonClass.getWikiTitle("LOCKED_INVENTORY_QUANTITY");
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	String strPartNum = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	String strInvId = (String)request.getAttribute("hInvId")==null?"":(String)request.getAttribute("hInvId");;
	String strType = (String)request.getAttribute("Type")==null?"":(String)request.getAttribute("Type");;
	ArrayList alProject = new ArrayList();
	alProject = (ArrayList)session.getAttribute("PROJLIST");

	ArrayList alInvTypes = new ArrayList();
	ArrayList alLockIds = new ArrayList();

	if (hmReturn != null) 
	{
		alInvTypes = (ArrayList)hmReturn.get("INVTYPES");
		alLockIds = (ArrayList)hmReturn.get("LOCKLIST");
	}
	
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt+" K:m:s a");
	String strUserName = (String)session.getAttribute("strSessShName");
	String strApplDateFmt = strGCompDateFmt;
	String strDateFmt = "{0,date,"+strApplDateFmt+"}";
	
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Loaner Status Report ");
	
	gmCrossTab.setValueType(0);
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(80);
	gmCrossTab.setNameWidth(400);
	gmCrossTab.setRowHeight(22);
	
	gmCrossTab.setRowHighlightRequired(true);
	gmCrossTab.setTotalRequired(false);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Locked Inventory Quantity</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
function fnGo()
{
	if(document.frmAccount.hPartNum.value == 0 && document.frmAccount.Cbo_ProjId.value == 0){
		Error_Details(message_operations[382]);
		}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	var pnum = document.frmAccount.hPartNum.value;
	if (pnum != '')
	{
		document.frmAccount.hOpt.value = "MultipleParts";
		document.frmAccount.Cbo_ProjId.selectedIndex = 0;
	}
	document.body.style.cursor = 'wait';
	fnStartProgress();
	document.frmAccount.submit();
}

var prevtr = 0;

function changeTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
}

function fnLoad()
{
	var sel = document.frmAccount.hSearch.value;
	if (sel != '')
	{
		document.frmAccount.Cbo_Search.value = sel;
	}
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  onLoad="javascript:fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPartInvReportServlet">
<input type="hidden" name="hId" value="<%=strProjId%>">
<input type="hidden" name="hAction" value="ReloadLock">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hSearch" value="">
<input type="hidden" name="Type" value="<%=strType%>">

	<table border="0" width="900" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="20" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="20" width="1" class="Line"></td>
		</tr>
		<tr>
		
		
			<td   height="25" class="RightDashBoardHeader"><fmtPartNumLockInvReport:message key="LBL_LOCKED_INVENTORY_QUANTITY"/></td>
	<td  height="25" class="RightDashBoardHeader" align="right">
	<fmtPartNumLockInvReport:message key="LBL_IMG_ALT_HELP" var = "varHelp"/>
		  	<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />  
	       </td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
		
		<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartNumLockInvReport:message key="LBL_INVENTORY_LIST"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_InvId"  seletedValue="<%=strInvId%>"  defaultValue= "[Choose One]"	
						tabIndex="1"  value="<%=alLockIds%>" codeId="LOCKID" codeName="LOCKDT"/>
			</td>
		</tr>		
		<tr>
		
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartNumLockInvReport:message key="LBL_PART_NUMBER"/>:</td>
			<td>&nbsp;<input type="text" size="57" value="<%=strPartNum%>" name="hPartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
							
			&nbsp;&nbsp;<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
							
							
							<option value="0" ><fmtPartNumLockInvReport:message key="LBL_CHOOSE_ONE"/>
							
							
							<option value="LIT" ><fmtPartNumLockInvReport:message key="LBL_LITERAL"/>
							
							
							<option value="LIKEPRE" ><fmtPartNumLockInvReport:message key="LBL_LIKE_PREFIX"/>
							
							
							<option value="LIKESUF" ><fmtPartNumLockInvReport:message key="LBL_LIKE_SUFFIX"/>
						</select>
			</td>
		</tr>			
		<tr>
		
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartNumLockInvReport:message key="LBL_PROJECT_LIST"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_ProjId"  seletedValue="<%=strProjId%>"  defaultValue= "[Choose One]"	
						tabIndex="1"  value="<%=alProject%>" codeId="ID" codeName="NAME"/>
			&nbsp;
			<fmtPartNumLockInvReport:message key="LBL_G0" var="varGo"/>
			<gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" name="Btn_Go" gmClass="button" onClick="javascript:fnGo()" tabindex="3" buttonType="Load" />
			</td>
		</tr>
<%
		if (strAction.equals("ReloadLock"))
		{
		HashMap hmReport = (HashMap)request.getAttribute("hmReport");
%>
		<tr><td class="Line" colspan="2"></td></tr>
		<tr>
			<td colspan="2" align="center">
				<fmtPartNumLockInvReport:message key="LBL_REPORT_AS_OF"/>: <%=strDate%>   <fmtPartNumLockInvReport:message key="LBL_BY"/>:<%=strUserName%><BR>
				<%=gmCrossTab.PrintCrossTabReport(hmReport)%>
			</td>
		</tr>
<%
		}
%>		
		<tr><td class="Line" colspan="2"></td></tr>
	</table>
		
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
