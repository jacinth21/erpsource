 <%
/**********************************************************************************
 * File		 		: GmQuarantineListReport.jsp
 * Desc		 		: This screen is used for displaying Quarantine count
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>

<!--  \operations\inventory\GmQuarantineListReport.jsp-->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtQuarantineListReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtQuarantineListReport:setLocale value="<%=strLocale%>"/>
<fmtQuarantineListReport:setBundle basename="properties.labels.operations.inventory.GmQuarantineListReport"/>





<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	
	//
	final String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	final String strPartNumber = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	final String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	
	HashMap hmTemp = new HashMap();
	strApplDateFmt = strApplDateFmt + " K:m:s a";
	SimpleDateFormat df = new SimpleDateFormat(strApplDateFmt);
	String strDate = (String)df.format(new java.util.Date());
	String strUserName = (String)session.getAttribute("strSessShName");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Built Set Inventory </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>

<!-- fngo() form -->
<!-- Step5 verify strhOpt is read at top from hOpt -->
 
function fnGo()
{
	
	document.frmInv.hOpt.value = '<%=strhOpt%>'; 
	var pnum = document.frmInv.hPartNum.value;
	
	//if (pnum != '')
	//{
		//!document.frmInv.hOpt.value = "MultipleParts";
		//!document.frmInv.Cbo_ProjId.selectedIndex = 0;
	//}
	document.frmInv.hAction.value = "Go";
	fnStartProgress();
	document.frmInv.submit();
}



</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmInv" method="POST" action="<%=strServletPath%>/GmInvListReportServlet">

<!---->
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">


	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
		
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtQuarantineListReport:message key="LBL_RETURNS_INVENTORY_LIST_REPORT"/></td>
		</tr>
		
		<!-- step1 includedjsp -->
		<tr>
			<td colspan="2"> 
				<jsp:include page="/operations/inventory/include/GmPartFilter.jsp" >
				<jsp:param name="hPartNum" value="<%=strPartNumber%>" />
				<jsp:param name="hSearch" value="<%=strSearch%>" />
				<jsp:param name="hProjId" value="<%=strProjId%>" />
			</jsp:include>
			</td>
		</tr>
		
		
		
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="requestScope.REPORT.rows" export="true" class="its" id="currentRowObject" decorator="org.displaytag.decorator.TotalTableDecorator"> 
			<display:setProperty name="export.excel.filename" value="QuarantineListReport.xls" />
			
			<display:caption><fmtQuarantineListReport:message key="DT_REPORT_AS_OF"/><%=strDate%>   <fmtQuarantineListReport:message key="DT_BY"/>:<%=strUserName%></display:caption>
				
				<fmtQuarantineListReport:message key="DT_PART_NUMBER" var="varPartNumber"/>
				<display:column property="PNUM" title="${varPartNumber}" class="aligncenter" sortable="true" />
				
				<fmtQuarantineListReport:message key="DT_DESCRIPTION" var="varDescription"/>
				<display:column property="PDESC" escapeXml="true" title="${varDescription}" class="alignleft"  />
				
				<fmtQuarantineListReport:message key="DT_PROJECT_ID" var="varProjectID"/>
				<display:column property="PROJID" title="${varProjectID}" group="1" sortable="true" class="alignleft" />
			  	
			  	<fmtQuarantineListReport:message key="DT_COUNT" var="varCount"/>
			  	<display:column property="QTY" title="${varCount}" class="alignright" sortable="true" total="true" format="{0,number,0}" />
			</display:table>
			</td>
		</tr>
    <table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>