 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
 <%
/**********************************************************************************
 * File		 		: GmBuiltSetListReport.jsp
 * Desc		 		: This screen is used for displaying Built Sets count
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>

<!--operations\inventory\GmBuiltSetListReport.jsp  -->
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->


<%@ taglib prefix="fmtGmBuiltSetListReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtGmBuiltSetListReport:setLocale value="<%=strLocale%>"/>
<fmtGmBuiltSetListReport:setBundle basename="properties.labels.operations.inventory.GmBuiltSetListReport"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	strCssPath = GmCommonClass.getString("GMSTYLES");
	strImagePath = GmCommonClass.getString("GMIMAGES");
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	String strApplDateFmt = strGCompDateFmt;
	String strDTDateFmt = "{0,date,"+strApplDateFmt+"}";
	HashMap hmTemp = new HashMap();
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt+" K:m:s a");
	String strUserName = (String)session.getAttribute("strSessShName");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Built Set Inventory </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>

</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmInv" method="POST" action="<%=strServletPath%>/GmInvListReportServlet">

	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
		 
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtGmBuiltSetListReport:message key="LBL_SETINVENTORYREPORT"/></td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="requestScope.REPORT.rows" export="true" class="its" id="currentRowObject" decorator="org.displaytag.decorator.TotalTableDecorator"> 
			<display:setProperty name="export.excel.filename" value="BuiltSetListReport.xls" />
			<display:caption><fmtGmBuiltSetListReport:message key="LBL_REPORT_AS_OF"/>: <%=strDate%> <fmtGmBuiltSetListReport:message key="LBL_BY"/>:<%=strUserName%></display:caption>
			<fmtGmBuiltSetListReport:message key="DT_SET_ID" var="varSetID"/>
				<display:column property="SET_ID" title="${varSetID}" class="aligncenter" group="1" sortable="true" />
				<fmtGmBuiltSetListReport:message key="DT_SET_NAME" var="varSetName"/>
				<display:column property="SET_NAME" title="${varSetName}" class="alignleft" group="1" sortable="true" />
				<fmtGmBuiltSetListReport:message key="DT_SET_CONSIGN_ID" var="varConsignID"/>
				<display:column property="CN_ID" title="${varConsignID}" class="alignleft" sortable="true" />
				<!-- PMT-22758 -->
				<fmtGmBuiltSetListReport:message key="DT_TAG_ID" var="varTagID" />
				<display:column property="TAG_ID" title="${varTagID}" class="alignleft" sortable="true" />
			    <fmtGmBuiltSetListReport:message key="DT_COUNT" var="varCount"/>
			  	<display:column property="CNT" title="${varCount}" class="alignright" total="true" format="{0,number,0}" />
			  	<fmtGmBuiltSetListReport:message key="DT_VERIFIED_DATE" var="varVerifiedDate"/>
			  	<display:column property="VDATE" title="${varVerifiedDate}" class="aligncenter" sortable="true" format="<%=strDTDateFmt%>" />
			  	<fmtGmBuiltSetListReport:message key="DT_VERIFIED_BY" var="varVerifiedBy"/>
			  	<display:column property="VERBY" title="${varVerifiedBy}" class="alignleft" sortable="true" />
			  	<fmtGmBuiltSetListReport:message key="DT_STATUS" var="varStatus"/>
			  	<display:column property="STATUS" title="${varStatus}"class="alignleft" sortable="true" />
			  	<fmtGmBuiltSetListReport:message key="DT_TYPE" var="varType"/>
			  	<display:column property="TYPE" title="${varType}" class="alignleft" sortable="true" />
			  	<fmtGmBuiltSetListReport:message key="DT_DESTINATION" var="varDestination"/>
			  	<display:column property="PURPOSE" title="${varDestination}" class="alignleft" sortable="true" />
			</display:table>
			</td>
		</tr>
    <table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>