
<%
	/**********************************************************************************
	 * File		 		: GmPendingPickSetDtls.jsp
	 * Desc		 		: This screen is used to view the Location information
	 * Version	 		: 
	 * author			: Jignesh Shah
	 ************************************************************************************/
%>


<!-- \operations\inventory\GmPendingPickSetDtls.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="gridData" name="frmPendingPickSet" property="gridXmlData" type="java.lang.String" />
<bean:define id="strAccessFl" name="frmPendingPickSet" property="accessFlag" type="java.lang.String" />
<bean:define id="buttonName" name="frmPendingPickSet" property="buttonName" type="java.lang.String" />
<bean:define id="strlocationType" name="frmPendingPickSet" property="locationType" type="java.lang.String" />

<%@ taglib prefix="fmtPendingPickSetDtls" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPendingPickSetDtls:setLocale value="<%=strLocale%>"/>
<fmtPendingPickSetDtls:setBundle basename="properties.labels.operations.inventory.GmPendingPickSetDtls"/>

<%
    String strOperationsInventoryJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");
	String strWikiTitle = GmCommonClass.getWikiTitle("SET_PENDING_PICK_DTLS");
	String strDisable = strAccessFl.equals("Y") ? "" : "true";
	String method = "";
	String strTableWidth = "";
    if(strlocationType.equals("4127")){
		method = "/gmPendingPickSet.do?method=loadLoanerSetsDtls";
		strTableWidth = "DtTable850";
    }else if(strlocationType.equals("4110")){
		method = "/gmPendingPickSet.do?method=loadConsignedSetsDtls";
		strTableWidth = "DtTable700";
    }else{
		method = "/gmPendingPickSet.do?method=loadInhouseLoanerSetsDtls";
		strTableWidth = "DtTable850";
    }
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Pending Pick</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">



<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>



<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strOperationsInventoryJsPath%>/GmPendingPickSet.js"></script>

<script>
var objGridData = '<%=gridData%>';
var objFl = '<%=gridData.indexOf("cell") != -1 ? "Y" : "N"%>';
var rowID = 0;
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad('PICK_DTLS');">

	<html:form action='<%=method%>'>
		<html:hidden property="strOpt" />
		<html:hidden property="strInputString" value="" />
		<html:hidden property="pType" />
		<html:hidden property="locationType" />
		<html:hidden property="pickLocationID" />
		<html:hidden property="pickLocationName" />
		<html:hidden property="operationVal" value="PICK"/>

		<table class='<%=strTableWidth%>' cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader"><fmtPendingPickSetDtls:message key="LBL_PENDING_PICK"/>(<bean:write name="frmPendingPickSet" property="pickLocationName" />)</td>
				<fmtPendingPickSetDtls:message key="LBL_IMG_ALT_HELP" var="varHelp"/>
				<td align="right" class=RightDashBoardHeader><img id='imgEdit'
					align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title="${varHelp}"
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<!-- Struts tag lib code modified for JBOSS migration changes -->
			<tr>
			
				<td class="RightTablecaption" HEIGHT="25" align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;<fmtPendingPickSetDtls:message key="LBL_SCAN"/>&nbsp;<html:text property="tagId" name="frmPendingPickSet" size="25"
					maxlength="20"styleClass="InputArea" tabindex="1" onfocus="changeBgColor(this,'#AACCE8');" 
					onblur="changeBgColor(this,'#ffffff'); fnScanIdOnBlur();" onkeypress="fnScanIdOnEnter();" />
				</td>
			</tr>
			
			<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
			<tr id="message_1" style="display: block;"><td colspan="2" HEIGHT="25">&nbsp;&nbsp;&nbsp;&nbsp;<fmtPendingPickSetDtls:message key="LBL_SCAN_A_TAG_ID"/></td></tr>
			<tr id="message_2" style="display: none;"><td colspan="2" HEIGHT="25">
				<font color="red">&nbsp;&nbsp;&nbsp;&nbsp;<fmtPendingPickSetDtls:message key="LBL_INVALID_TAG_ID_PLEASE_SCAN_A_CALID_TAG_ID"/></font></td>
			</tr>
			<%
				if (gridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="2" width="100%">

					<div id="dataGrid" style="height: 400px; width: 100%;"></div></td>
			</tr>
			<tr>
			<fmtPendingPickSetDtls:message key="BTN_SUBMIT" var="varSubmit"/>
					<td align="center" colspan="2" height="30"><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="btn_submit"
						
						tabindex="2" gmClass="Button" onClick="javascript:fnSubmit();" disabled="<%=strDisable %>" buttonType="Save" />&nbsp;&nbsp;
						<gmjsp:button value="<%=buttonName%>" name="btn_close"
						tabindex="3" gmClass="Button" onClick="javascript:fnClose();" buttonType="Load" />
					</td>
				</tr>
			<%
				} else {
			%>
			<tr>
			
                 
			
				<td align="center" colspan="2" height="30" class="RightTableCaption"><fmtPendingPickSetDtls:message key="LBL_MSG_NO_DATA"/></td>
			</tr>
			<%
				}
			%>

				
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>