<%
/**********************************************************************************
 * File		 		: GmPartReserveReport.jsp
 * Desc		 		: This screen is used fetch the Part Reserved for the Ack Order
 * Version	 		: 1.0
 * author			: HReddi
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtPartReserveReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- operations\inventory\GmPartReserveReport.jsp.jsp -->
<fmtPartReserveReport:setLocale value="<%=strLocale%>"/>
<fmtPartReserveReport:setBundle basename="properties.labels.custservice.GmOrderItemControl"/>
<bean:define id="gridData" name="frmLotTrackReport" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="releaseQty" name="frmLotTrackReport" property="releaseQty" type="java.lang.String"> </bean:define>
<bean:define id="shelfQty" name="frmLotTrackReport" property="shelfQty" type="java.lang.String"> </bean:define>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("PART_RESERVE_REPORT");
	String strApplDateFmt = strGCompDateFmt;
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Lot Tracking Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: Lot Tracking Report </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmPartReserveReport.js"></script>

<script>
	var objGridData = '<%=gridData%>';
	var format = '<%=strApplDateFmt%>';
	var releaseQty = <%=releaseQty%>;
	var shelfQty = <%=shelfQty%>;
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad()"  onkeypress="fnEnter();">
<html:form action="/gmLotTrackReport.do?method=fetchLotTrackingDetails">
	<html:hidden property="strOpt" name="frmLotTrackReport"/>
	<html:hidden property="transId" name="frmLotTrackReport"/>
	<html:hidden property="inputString" name="frmLotTrackReport"/>
	<html:hidden property="releaseQty" name="frmLotTrackReport"/>
	<html:hidden property="shelfQty" name="frmLotTrackReport"/>
	<table class="DtTable1200" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="5">&nbsp;<fmtPartReserveReport:message key="LBL_RESERVE_INVENTORY"/></td>
			<fmtPartReserveReport:message key="LBL_HELP" var="varHelp"/>
			<td align="right" class=RightDashBoardHeader><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>					
		<tr><td class="LLine" height="1" colspan="6"></td></tr>	
		<%if( gridData.indexOf("cell") != -1){%>
			<tr>
				<td colspan="6"><div  id="dataGridDiv" style="" height="400px" width="1250px"></div></td>
			</tr>						
		<%}else if(!gridData.equals("")){%>
			<tr><td colspan="6" align="center" class="RightText"><fmtPartReserveReport:message key="LBL_NOTHING_TOFOUND_DISPLAY"/></td></tr>
		<%}else{%>
			<tr><td colspan="6" align="center" class="RightText"><fmtPartReserveReport:message key="LBL_NO_DATA_AVAILABLE"/></td></tr>
		<%} %>			
		<tr><td></td></tr>
		<fmtPartReserveReport:message key="BTN_SUBMIT" var="varSubmit"/>
		<fmtPartReserveReport:message key="BTN_CLOSE" var="varClose"/>
		<%if( gridData.indexOf("cell") != -1) {%>
			<TR><TD colspan="6" align="Center">
			
				<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" style="width: 6em; height: 22px"  name="Btn_Submit" buttonType="Save" gmClass="button" onClick="fnSubmit();" />&nbsp;
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" style="width: 6em; height: 22px"  name="Btn_Close" buttonType="Save" gmClass="button" onClick="fnClose();" />&nbsp;				
			</TD></TR>
			<tr><td></td></tr>
		<%}else{ %>
			<TR height="50"><TD colspan="6" align="Center">
			<gmjsp:button value="&nbsp;${varClose}&nbsp;" style="width: 6em; height: 22px"  name="Btn_Close" buttonType="Save" gmClass="button" onClick="window.close();" />&nbsp;
			</TD></TR>
			<tr><td></td></tr>
		<%} %>
	</table>
<%@ include file="/common/GmFooter.inc"%>
</html:form>
</BODY>
</HTML>