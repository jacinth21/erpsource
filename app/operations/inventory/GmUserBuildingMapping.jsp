<%
/**********************************************************************************
 * File		 		: GmMultiUserGroupBuilding.jsp
 * Desc		 		: This screen is used for the Building to User Mapping
 * author			: N Raja 
************************************************************************************/
%>
<!-- \user\GmMultiUserGroupBuilding.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="frmtUserBuildingMapping" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page isELIgnored="false"%>
<frmtUserBuildingMapping:setLocale value="<%=strLocale%>"/>
<frmtUserBuildingMapping:setBundle basename="properties.labels.operations.inventory.GmUserBuildingMapping"/>

<bean:define id="strOpt" name="frmUserBuildingMapping" property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="showUser" name ="frmUserBuildingMapping" property="showUser"  type="java.lang.String"></bean:define>
<bean:define id="alUserList" name="frmUserBuildingMapping" property="alUserList" type="java.util.ArrayList"></bean:define>
<bean:define id="alSecGrp" name="frmUserBuildingMapping" property="alSecGrp" type="java.util.ArrayList"></bean:define>
<bean:define id="alCompany" name="frmUserBuildingMapping" property="alCompany" type="java.util.ArrayList"></bean:define>
<bean:define id="alBuildingList" name="frmUserBuildingMapping" property="alBuildingList" type="java.util.ArrayList"></bean:define>
<%
String strOperationsInventoryJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");
%>
<%
	HashMap hmUser = new HashMap();
	hmUser.put("ID","");
	hmUser.put("PID","PARTYID");
	hmUser.put("NM","USERNAME");
	GmCommonControls.setHeight(300);
	GmCommonControls.setWidth(300);
	
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: User List To Building</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery.min.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strOperationsInventoryJsPath%>/GmUserBuildingMapping.js"></script>

</HEAD>
<BODY leftmargin="20" topmargin="10" onload="onLoad('<%=strOpt%>');">
<form name="frmUserBuildingMapping" action="/gmUserBuildingMappingAction.do" >
<html:hidden property="strOpt" name="frmUserBuildingMapping" value="<%=strOpt%>"/>
<html:hidden property="haction" name="frmUserBuildingMapping" value=""/>
<html:hidden property="hinputstring" name="frmUserBuildingMapping" value=""/>      
                                  
		<table class="DtTable700" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" width="250">&nbsp;<frmtUserBuildingMapping:message key="LBL_HEADING" /></td>  
						<td align="right" class=RightDashBoardHeader><frmtUserBuildingMapping:message
						    key="IMG_HELP" var="varHelp" /><img id='imgEdit'
					        style='cursor: hand' src='<%=strImagePath%>/help.gif'
					        title='${varHelp}' width='16' height='16'
				        	onClick="javascript:fnHelp('<%=strWikiPath%>','<%=GmCommonClass.getWikiTitle("MULTI_USER_BUILDING")%>');" />
				       </td>
					</tr>	
					<table class="DtTable700" border="0" cellspacing="0" cellpadding="0"><tr><td>	 				
					<tr><td height="1" colspan="2"></td></tr>			
					
					<tr height="30">
					
						<td  align="right" width="180px"><b><font color="red">*</font> <frmtUserBuildingMapping:message key="LBL_GROUPS" />:&nbsp;</b></td>				
						 <td><gmjsp:dropdown controlName="secGrp" SFFormName="frmUserBuildingMapping" SFSeletedValue="secGrp"
							SFValue="alSecGrp" codeId = "CODENMALT"  codeName = "CODENM"  width="300" defaultValue= "[Choose One]" />	
						</td> 
					
					</tr>
					<tr><td class="LLine" height="1" colspan="2"></td></tr>	 
						<tr height="30" class="Shade">			 
							<td  align="right" width="180px"><font color="red"><b>*</font><frmtUserBuildingMapping:message key="LBL_COMPANY" />:&nbsp;</b></td>
							 <td><gmjsp:dropdown controlName="companyId" SFFormName="frmUserBuildingMapping" SFSeletedValue="companyId"  SFValue="alCompany"
								codeId="COMPANYID" codeName="COMPANYNM" defaultValue="[Choose One]"  width="300"/>&nbsp;&nbsp;
							</td>						
						</tr>
						<tr height="30">
						<td  align="right" width="180px"><b><font color="red">*</font><frmtUserBuildingMapping:message key="LBL_BUILDING" />:&nbsp;</b></td>				
						 <td><gmjsp:dropdown controlName="buildingId" SFFormName="frmUserBuildingMapping" SFSeletedValue="buildingId"
							SFValue="alBuildingList" codeId = "BID"  codeName = "BNM"  width="300" defaultValue= "[Choose One]" />	
						 <gmjsp:button value="Load" onClick="fnload();" buttonType="Load" gmClass="button" />	
						</td> 
					
					</tr>
					</table>
					<%if(showUser.equals("USER")){ %>			
					<table class="DtTable700" border="0" cellspacing="0" cellpadding="0"><tr><td>				 
						<tr class="Shade">
						<td align="Right" width="180px" valign="top"><font color="red">*</font><b><frmtUserBuildingMapping:message key="LBL_USER_LIST" />:&nbsp;</b></td> 
						<td>
     			  	  	<%=GmCommonControls.getChkBoxGroup("",alUserList,"user",hmUser)%> 
						</td>
						</tr>
				
					<tr><td class="LLine" height="1" colspan="2"></td></tr>
					<tr>
						<td  height="50" align="center" colspan="2">&nbsp;
							<gmjsp:button value="Submit" buttonType="Save" onClick="fnSubmit();" gmClass="button" />&nbsp;
							</tr>
					<tr><td height="1" colspan="2"></td></tr>
					</table>
					<%}%>
				</table>
</form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>