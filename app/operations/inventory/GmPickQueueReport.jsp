 <%
/**********************************************************************************
 * File		 		: GmPickQueueReport.jsp
 * Desc		 		: This screen is used to display orders and Can change the Assigned User (Manually)
 * Version	 		: 1.0
 * author			: Vamsi Kiran Nagavarapu
************************************************************************************/
%>

<!-- \operations\inventory\GmPickQueueReport.jsp -->

<%@ page language="java"%>

<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>

 

<bean:define id="gridData" name="frmPickQueueList"
	property="gridXmlData" type="java.lang.String">
</bean:define>
<bean:define id="dateTypeVal" name="frmPickQueueList" property="dateType" type="java.lang.String"> </bean:define>

<%@ taglib prefix="fmtPickQueueReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPickQueueReport:setLocale value="<%=strLocale%>"/>
<fmtPickQueueReport:setBundle basename="properties.labels.operations.inventory.GmPickQueueReport"/>


<%
	String strWikiTitle = GmCommonClass.getWikiTitle("PICK_QUEUE_REPORT");
	 
	String strApplDateFmt = strGCompDateFmt;
		
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Status Log Detail</TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>


<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmStatusLog.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script>
var dateFmt = '<%=strApplDateFmt%>';
var objGridData = '<%=gridData%>';
		
function fnOnPageLoad()
{
	gridObj = initGrid('dataGrid',objGridData);
	
	 //if(objGridData.indexOf("cell", 0)==-1){
	  	//	alert('No Data Found');
	//}
	
	 
}

function fnRptReload()
{
	var objFromDT = document.frmPickQueueList.startDate.value;
	var objToDT = document.frmPickQueueList.endDate.value;
	 	
    if(objFromDT != ""){		 
		CommonDateValidation(document.frmPickQueueList.startDate,dateFmt, message[611]);	 
	}

	if(objToDT != ""){ 
		CommonDateValidation(document.frmPickQueueList.endDate,dateFmt, message[611]);	 
	} 
	
	if (ErrorCount > 0)  
	{
		Error_Show();
		Error_Clear();
		return false;
					} 
	document.frmPickQueueList.haction.value = "rptReload";
	fnStartProgress();
	document.frmPickQueueList.submit();
}


function fnSubmit()
{
		gridObj.editStop();// retun value from editor(if presents) to cell and closes editor
		var gridrowid;
		var void_flag;
		var finalData='';
		var gridrows =gridObj.getChangedRows(",");
		
		if(gridrows.length==0){
			Error_Clear();	
			Error_Details(message_operations[381]);
			Error_Show();
			Error_Clear();
		}
		else
		{
		
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();
		
			
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
			
				void_flag = gridObj.getRowAttribute(gridrowid,"row_deleted");
				if(void_flag ==  undefined){
					void_flag='';
				}
				
				for(var col=0;col<columnCount;col++){
					
					finalData+=gridObj.cellById(gridrowid, col).getValue()+'^';
					
				}
				
				finalData+='|';
			}
			finalData+="$";
		
		document.frmPickQueueList.strOpt.value = 'save';
		document.frmPickQueueList.strFinalData.value = finalData;
		fnStartProgress('Y');
		document.frmPickQueueList.submit();
		
		}	
}

function fnPrintPick(id, type){
	if(type=='93001') // orders
		windowOpener('<%=strServletPath%>/GmOrderItemServlet?hAction=PICSlip&hId='+id,"Order","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
	else if(type=='93002') // Consigned Items
		windowOpener('<%=strServletPath%>/GmConsignItemServlet?hAction=PicSlip&hConsignId='+id,"Consignment","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
	else if (type == '93008') // In-House Sets
		windowOpener('<%=strServletPath%>/GmConsignInHouseServlet?hAction=PicSlip&hConsignId='+id,"InHouse","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">

<html:form action="/gmpickqueuedetails.do">
	<html:hidden property="strOpt"  value=""/>
	<html:hidden property="strFinalData" value=""/>
	<html:hidden property="haction" value="G" />

	<table border="0" class="DtTable1000"   cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="7" height="25" class="RightDashBoardHeader"><fmtPickQueueReport:message key="LBL_PICK_QUEUE_REPORT"/></td>
			
               <fmtPickQueueReport:message key="IMG_ALT_HELP" var="varHelp"/>
			<td align="right" class=RightDashBoardHeader><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title="${varHelp}" width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<td colspan="8" class="Line" height="1"></td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td colspan="1" height="30" align="right" class="RightTableCaption"><fmtPickQueueReport:message key="LBL_PICK_TYPE"/>:</td>
			<td colspan="1" align="left"><gmjsp:dropdown controlName="typeNum"
				SFFormName="frmPickQueueList" SFSeletedValue="typeNum"
				SFValue="alTypes" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" /></td>

			<td colspan="1" align="right" height="30" class="RightTableCaption"><fmtPickQueueReport:message key="LBL_ASSIGNED_TO"/>:</td>
			<td colspan="1" align="left"><gmjsp:dropdown controlName="userNum"
				SFFormName="frmPickQueueList" SFSeletedValue="userNum"
				SFValue="alUsers" codeId="ID" codeName="NAME"
				defaultValue="[Choose One]" /></td>
			<td colspan="1" align="right" height="30" class="RightTableCaption"><fmtPickQueueReport:message key="LBL_STATUS"/>:</td>
			<td colspan="3" align="left"><gmjsp:dropdown controlName="statusNum"
				SFFormName="frmPickQueueList" SFSeletedValue="statusNum"
				SFValue="alStatus" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" /> 
				<gmjsp:dropdown controlName="statusOper" SFFormName="frmPickQueueList" SFSeletedValue="statusOper"
				SFValue="alStatus1" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/>
				<b><fmtPickQueueReport:message key="LBL_OR"/></b>&nbsp;
				
								<gmjsp:dropdown controlName="statusOther" SFFormName="frmPickQueueList" SFSeletedValue="statusOther"
				SFValue="alStatusOther" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/>
				</td>
			</tr>
		<tr class="Shade">

			<td height="30" align="right" colspan="1" class="RightTableCaption"><fmtPickQueueReport:message key="LBL_TYPE"/>:</td>
			<td colspan="1" align="left"><select name="dateType" class="RightText"
				onFocus="changeBgColor(this,'#AACCE8');"
				onBlur="changeBgColor(this,'#ffffff');">
				<option value="0"><fmtPickQueueReport:message key="LBL_CHOOSE_ONE"/>
				<%if (dateTypeVal.equals("ALLOCATED")){%>
				
				
					<option value="ALLOCATED" selected><fmtPickQueueReport:message key="LBL_ALLOCATED_DATE"/>
				
				<%} else { %>
				 
				 <option value="ALLOCATED"><fmtPickQueueReport:message key="LBL_ALLOCATED_DATE"/>
				 <%} %>
				 <%if (dateTypeVal.equals("PROCESSED")){%>
				
				 
					<option value="PROCESSED" selected><fmtPickQueueReport:message key="LBL_PROCESSED_DATE"/>
				<%} else { %>
				 
				  	<option value="PROCESSED"><fmtPickQueueReport:message key="LBL_PROCESSED_DATE"/>
				 <%}%>
				 <%if (dateTypeVal.equals("CREATEDDATE")){%>
				
				 
					<option value="CREATEDDATE" selected><fmtPickQueueReport:message key="LBL_CREATED_DATE"/>
				
				<%} else { %>
				 
				 <option value="CREATEDDATE"> <fmtPickQueueReport:message key="LBL_CREATED_DATE"/>
				 <%} %>
					</select>
			</td>


			<td colspan="1" align="right" height="25" class="RightTableCaption"> <fmtPickQueueReport:message key="LBL_FROM"/>:</td>
			<td colspan="1" align="left" height="30" class="RightTableCaption"> 
				<gmjsp:calendar SFFormName="frmPickQueueList" controlName="startDate" gmClass="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
				</td>
			<td colspan="1" align="right" height="25" class="RightTableCaption"><fmtPickQueueReport:message key="LBL_TO"/>:</td>
			<td colspan="3" align="left" height="30" class="RightTableCaption">
			<gmjsp:calendar SFFormName="frmPickQueueList" controlName="endDate" gmClass="InputArea" 
				onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
			 </td>


		</tr>



		<tr >
		
		<fmtPickQueueReport:message key="LBL_LOAD" var="varLoad"/>
		
			<td align="center" colspan="8" height="30"><gmjsp:button
				value="&nbsp;${varLoad}&nbsp;" name="Btn_Submit" gmClass="button"
				onClick="fnRptReload();" buttonType="Load" /></td>
		</tr>
		<tr>
			<td colspan="8" class="Line" height="1"></td>
		</tr>
		<logic:notEqual name="frmPickQueueList" property="haction" value="initiating"> 
			<tr>
				<td colspan="8"></td>
			</tr>
			<tr>
				<td colspan="8" width="100%">

						<div id="dataGrid" style="height: 400px; width: 1000px;"></div>

				</td></tr>
	<tr class="Shade">
	<fmtPickQueueReport:message key="LBL_SAVE" var="varSave"/>
		<td align="center" colspan="8" height="30"><gmjsp:button
					value="&nbsp;${varSave}&nbsp;" name="submitbtn" tabindex="11"
						gmClass="button" onClick="javascript:fnSubmit();" buttonType="Save" /></td>
					
					
				</tr>
	</logic:notEqual>	
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
 
</BODY>
</HTML>