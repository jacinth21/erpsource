<!-- \operations\inventory\GmReturnsListReport.jsp -->
 <%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: GmReturnsListReport.jsp
 * Desc		 		: This screen is used for displaying Returns count
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>

 <!-- \operations\inventory\GmReturnsListReport.jsp-->

<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->

<%@ taglib prefix="fmtReturnsListReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtReturnsListReport:setLocale value="<%=strLocale%>"/>
<fmtReturnsListReport:setBundle basename="properties.labels.operations.inventory.GmReturnsListReport"/>




<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	
	HashMap hmTemp = new HashMap();

	String strApplDateFmt = strGCompDateFmt;
	GmCalenderOperations gmCal = new GmCalenderOperations();
	gmCal.setTimeZone(strGCompTimeZone);
    String strDate = "";
	strDate = gmCal.getCurrentDate(strApplDateFmt+" K:m:s a");
	String strDateFmt = "{0,date,"+strApplDateFmt+"}";
	String strUserName = (String)session.getAttribute("strSessShName");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Returns Inventory </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>

</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmInv" method="POST" action="<%=strServletPath%>/GmInvListReportServlet">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
		
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtReturnsListReport:message key="LBL_RETURNS_LIST_REPORT"/></td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="requestScope.REPORT.rows" export="true" class="its" id="currentRowObject">
			<display:setProperty name="export.excel.filename" value="ReturnsListReport.xls" />
			<display:caption><fmtReturnsListReport:message key="LBL_REPORT_AS_OF"/> <%=strDate%> <fmtReturnsListReport:message key="LBL_BY"/> : <%=strUserName%></display:caption>
				
			<fmtReturnsListReport:message key="LBL_TYPE" var="varType"/>	
				<display:column property="TYPE" title="${varType}" group="1" class="alignleft" sortable="true" />
				
				<fmtReturnsListReport:message key="LBL_RA_ID" var="varRAID"/>
				<display:column property="RAID" title="${varRAID}" group="1" class="alignleft" sortable="true" />
				
				<fmtReturnsListReport:message key="LBL_CREDIT_DATE" var="varCreditDate"/>
				<display:column property="CDATE" title="${varCreditDate}" group="1" class="alignleft" sortable="true" format="<%=strDateFmt%>"/>
			</display:table>
			</td>
		</tr>
    <table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>