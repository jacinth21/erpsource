
<%
	/**********************************************************************************
	 * File		 		: GmPendingPutSetList.jsp
	 * Desc		 		: This screen is used to Show the Pending Putaway Location
	 * Version	 		: 2.0
	 * author			: Jignesh Shah
	 ************************************************************************************/
%>

<!-- \operations\inventory\GmPendingPutSetList.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="gridData" name="frmPendingPutSet" property="gridXmlData" type="java.lang.String" />
<bean:define id="strAccessFl" name="frmPendingPutSet" property="accessFlag" type="java.lang.String" />
<bean:define id="strType" name="frmPendingPutSet" property="pType" type="java.lang.String" />

<%@ taglib prefix="fmtPendingPutSetList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPendingPutSetList:setLocale value="<%=strLocale%>"/>
<fmtPendingPutSetList:setBundle basename="properties.labels.operations.inventory.GmPendingPutSetList"/>



<%
	String strOperationsInventoryJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");
	String strWikiTitle = GmCommonClass.getWikiTitle("SET_PENDING_PUTAWAY");
	String strDisable = strAccessFl.equals("Y") ? "" : "true";
	String strTableWidth = "";
	if(strType.equals("CNLN")){
		strTableWidth = "DtTable800";
	}else{
		strTableWidth = "DtTable1100";
	}
	
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Pending Putaway Detail</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"	media="print" />



<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>


<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strOperationsInventoryJsPath%>/GmPendingPutSetList.js"></script>
<script>
	var objGridData = '<%=gridData%>';
	var accFl = '<%=strAccessFl%>';
	var objFl = '<%=gridData.indexOf("cell") != -1 ? "Y" : "N"%>';
	var rowID = 0;
	
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();"
	onbeforeprint="hidePrint();" onafterprint="showPrint();">

	<html:form action="/gmPendingPutSet.do?method=loadPendingPutSets">
		<html:hidden property="strOpt" />
		<html:hidden property="strInputString" value="" />
		<html:hidden property="pType" />
		<html:hidden property="wareHouseID" />
		<html:hidden property="hTagID" />
		<html:hidden property="hGridRowId" />
		<html:hidden property="hScanLocID" />
		
		<table class='<%=strTableWidth%>' cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" width="40%" class="RightDashBoardHeader"><fmtPendingPutSetList:message key="LBL_PENDING_PUT_AWAY"/></td>
				
				<fmtPendingPutSetList:message key="IMG_ALT_HELP" var="varHelp"/>
				
				<td align="right" class=RightDashBoardHeader><img id='imgEdit'
					align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title="${varHelp}"
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<!-- Struts tag lib code modified for JBOSS migration changes -->
			<tr>
			
				<td class="RightTablecaption" HEIGHT="25" align="left">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtPendingPutSetList:message key="LBL_SCAN"/>&nbsp;<html:text property="tagID" name="frmPendingPutSet" size="30"
					maxlength="30" styleClass="InputArea" tabindex="1" onfocus="changeBgColor(this,'#AACCE8');" 
					onblur="changeBgColor(this,'#ffffff'); fnScanIdOnBlur();" onkeypress="fnScanIdOnEnter();" />
				</td>
			</tr>
			<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
			<!-- <tr><td colspan="2" nowrap HEIGHT="25" id="Message"></td></tr> -->
			<tr>
				<td colspan="2" HEIGHT="25">
				<table cellspacing="0" cellpadding="0" width="100%" border="0">
					<tr>
						<td width="20"></td>
						<td nowrap HEIGHT="25" id="Message"></td>
					</tr>
				</table>
				</td>
			</tr>
			<%
				if (gridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="2" width="100%">

					<div id="dataGrid" style="height: 400px; width: 100%;"></div>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="LLine" height="1"></td>
			</tr>
			<tr id="buttonTR">
			
			
			
				<td align="center" colspan="2" height="30">
					<fmtPendingPutSetList:message key="BTN_SUBMIT" var="varSubmit"/>
					<gmjsp:button value="${varSubmit}" name="submitbtn" style="width: 5em; height: 22"
					tabindex="2" gmClass="Button" onClick="javascript:fnSubmit();" disabled="<%=strDisable%>" buttonType="Save" />&nbsp;&nbsp;
					<fmtPendingPutSetList:message key="BTN_PRINT" var="varPrint"/>
					<gmjsp:button value="${varPrint}" name="submitbtn" style="width: 5em; height: 22"
					tabindex="3" gmClass="Button" onClick="javascript:fnPrint();" disabled="<%=strDisable%>" buttonType="Load" />
				</td>
			</tr>
			<%
				} else {
			%>
			<tr>
				<td colspan="2" class="LLine" height="1"></td>
			</tr>
			<tr>
			
				<td align="center" colspan="2" height="30" class="RightTableCaption"><fmtPendingPutSetList:message key="LBL_MSG_NO_DATA"/></td>
			</tr>
			
			


			<%
				}
			%>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>