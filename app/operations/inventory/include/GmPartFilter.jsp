<!-- WEB-INF path corrected for JBOSS migration changes -->

<!--\operations\inventory\include\GmPartFilter.jsp  -->
 <%@ include file="/common/GmHeader.inc" %>

<%@ page import="java.util.ArrayList,java.util.HashMap" %>

<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>

<%@ taglib prefix="fmtPartFilter" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartFilter:setLocale value="<%=strLocale%>"/>
<fmtPartFilter:setBundle basename="properties.labels.operations.inventory.include.GmPartFilter"/>


<%	
try {

	final String strPartNum = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	final String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	final String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	
	HashMap hcboVal;
	int intProjLength ;
	String strCodeID ;
	String strSelected ;
	String strProjName ;
	
	
	//Create Search dropdown
	ArrayList alSearch = GmCommonClass.getCodeList("LIKES");
	
	
	//Create PRojectList Dropdown
	ArrayList alProject = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALPROJECT"));
	
	/* GmProjectBean gmProj = new GmProjectBean();
	HashMap hmTemp = new HashMap();
    hmTemp.put("COLUMN","ID");
    hmTemp.put("STATUSID","20301"); // Filter all approved projects
    alProject = gmProj.reportProject(hmTemp); */
	
    
	
%>	
<table border=0 Width="100%" cellspacing=0 cellpadding=0>

<tr><td class="Line" height="1" colspan="2"></td></tr>

<!-- Struts tag lib code modified for JBOSS migration changes -->	
<tr>
	<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartFilter:message key="LBL_PART_FILTER"/>:</td>
	<td>&nbsp;<input type="text" size="57" value="<%=strPartNum%>" name="hPartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
							
	&nbsp;&nbsp;

			<gmjsp:dropdown controlName="Cbo_Search"  seletedValue="<%= strSearch %>" 	tabIndex="2" 
						    value="<%= alSearch%>" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]"    />
						
						
				
	</td>
</tr>


<tr>

	<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartFilter:message key="LBL_PROJECT_LIST"/>:</td>
	
	<td>

	
	&nbsp;<select name="Cbo_ProjId" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtPartFilter:message key="OPT_CHOOSE_ONE"/>
<%
					hcboVal = new HashMap();
					intProjLength = alProject.size();
					
			  		for (int i=0;i<intProjLength;i++)
			  		{
			  			hcboVal = (HashMap)alProject.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strProjName = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
						strSelected = strProjId.equals(strCodeID)?"selected":"";
%>
					<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeID%> / <%=strProjName%></option>
<%
			  		}
%>

			</select>
						
						
	


<fmtPartFilter:message key="BTN_LOAD" var="varLoad"/>
	 &nbsp;&nbsp;&nbsp;<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnGo()" tabindex="3" buttonType="Load"/> 
	 
	 </td>

</tr>

</table>

 	


<%
}
catch(Exception e)
{
	e.printStackTrace();
}

%> 

<!--

<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
					<option value="0" >[Choose One]
					<option value="LIT" >Literal
					<option value="LIKEPRE" >Like - Prefix
					<option value="LIKESUF" >Like - Suffix
</select>
				
-->