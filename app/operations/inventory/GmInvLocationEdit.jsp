<%
/**********************************************************************************
 * File		 		: GmInvLocationEdit.jsp
 * Desc		 		: This screen is used to Edit the Location
 * Version	 		: 1.0
 * author			: Vamsi Kiran Nagavarapu
************************************************************************************/
%>
<!-- \operations\inventory\GmInvLocationEdit.jsp -->

<%@ page language="java"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ taglib prefix="fmtInvLocationEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtInvLocationEdit:setLocale value="<%=strLocale%>"/>
<fmtInvLocationEdit:setBundle basename="properties.labels.operations.inventory.GmInvLocationEdit"/>






<bean:define id="gridData" name="frmLocation" property="gridXmlData" type="java.lang.String" />
<bean:define id="strAccessFl" name="frmLocation" property="accessFlag" type="java.lang.String" />
<bean:define id="currQtyFlag" name="frmLocation" property="chkCurrQtyFlag" type="java.lang.String" />
<bean:define id="strTypeNum" name="frmLocation" property="typeNum" type="java.lang.String" />
<bean:define id="strStatusNum" name="frmLocation" property="statusNum" type="java.lang.String"></bean:define>
<bean:define id="strWarehouseId" name="frmLocation" property="warehouseid" type="java.lang.String"></bean:define>
<bean:define id="strLocationID" name="frmLocation" property="locationID" type="java.lang.String"></bean:define>
<bean:define id="strLocMap" name="frmLocation" property="locMap" type="java.lang.String"></bean:define>
<bean:define id="buildid" name="frmLocation" property="buildid" type="java.lang.String"></bean:define>
<bean:define id="bldFlag" name="frmLocation" property="bldFlag" type="java.lang.String"></bean:define>


<%
    String strOperationsInventoryJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");
	String strWikiTitle = GmCommonClass.getWikiTitle("LOCATION_EDIT");
	String	strDisable =strAccessFl.equals("Y")?"":"Disabled";
	boolean strCurrQtyDisable = (strTypeNum.equals("93336") && strStatusNum.equals("93310"))?false:true;
	log.debug("EDIT........");
	String bFlag = bldFlag;
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Status Log Detail</TITLE>

<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>


<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strOperationsInventoryJsPath%>/GmInvLocationEdit.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<script>
var objGridData = '<%=gridData%>';
var rowID = 0;		
var voidRowIdStr = '';
var hType = '<%=strTypeNum%>';
var hTypeVal = '<%=strTypeNum%>';
var hwarehouseid = '<%=strWarehouseId%>';
var locationid = '<%=strLocationID%>';
var locMap = '<%=strLocMap%>';
var lblLocationID = '<fmtInvLocationEdit:message key="LBL_LOC_ID"/>';
var lblType = '<fmtInvLocationEdit:message key="LBL_TYPE"/>';
var lblStatus = '<fmtInvLocationEdit:message key="LBL_STATUS"/>';
var lblWareHouse = '<fmtInvLocationEdit:message key="LBL_WAREHOUSE"/>';
var lblBuilding = '<fmtInvLocationEdit:message key="LBL_BUILDING"/>';
var lblNoPartNum = '<fmtInvLocationEdit:message key="LBL_NO_PART_NUMBER"/>';
var lblPartNumDuplicated = '<fmtInvLocationEdit:message key="LBL_PART_NUM_DUPLICATED"/>';

var lblStatus = '<fmtInvLocationEdit:message key="LBL_STATUS"/>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">

<html:form action="/gmlocation.do?method=saveInvLocationPartByID&locationId =''">
	<html:hidden property="strOpt"  value=""/>
	<html:hidden property="strInputString" value=""/>
	<html:hidden property="strVoidRowIds" value=""/>
	<html:hidden property="haction" value="G" />
	<html:hidden property="hlocationId"/>	
	<html:hidden property="ruleType" />
	<html:hidden property="locMap" />
	<input type="hidden" name="bldFlag" value="<%=bldFlag%>">
	<table class="DtTable700" cellspacing="0" cellpadding="0" border="0" style="border: 1px solid  #676767;">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtInvLocationEdit:message key="LBL_ED_LOC"/></td>
			<td align="right" class=RightDashBoardHeader colspan="3"><fmtInvLocationEdit:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<td colspan="4" class="LLine" height="1"></td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<fmtInvLocationEdit:message key="LBL_WAREHOUSE" var="varWarehouse"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varWarehouse}:" td="true"/>
			<td height="25">&nbsp;<gmjsp:dropdown controlName="warehouseid"
				SFFormName="frmLocation" SFSeletedValue="warehouseid"
				SFValue="alWareHouse" codeId="ID" codeName="WHNM"
				defaultValue="[Choose One]" tabIndex="1" onChange="fnTypeWarehouse();"/></td>
				
			<fmtInvLocationEdit:message key="LBL_LOC_ID" var="varLocId"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varLocId}:" td="true"/>
			<td height="25">&nbsp;<html:text
				property="locationID" size="15" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff'); fnValidateLocationId();" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtInvLocationEdit:message key="BTN_LOAD" var="varLoad"/><gmjsp:button
					value="&nbsp;${varLoad}&nbsp;" name="Btn_Load" style="width: 5em; height: 20" tabindex="11"
						gmClass="button" onClick="javascript:fnLoad();" buttonType="Load" />
			</td>
		</tr>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr class="Shade">
			<fmtInvLocationEdit:message key="LBL_TYPE" var="varType"/><gmjsp:label type="BoldText"  SFLblControlName="${varType}:" td="true"/>
			<td height="25">&nbsp;<gmjsp:dropdown controlName="typeNum"
				SFFormName="frmLocation" SFSeletedValue="typeNum" disabled="<%=strDisable%>"
				SFValue="alInvLocTypes" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" onChange="fnTypeChange();" tabIndex="3"/>
			</td>
			<fmtInvLocationEdit:message key="LBL_STATUS" var="varStatus"/><gmjsp:label type="BoldText"  SFLblControlName="${varStatus}:" td="true"/>
			<td height="25">&nbsp;<gmjsp:dropdown controlName="statusNum"
				SFFormName="frmLocation" SFSeletedValue="statusNum" disabled="<%=strDisable%>"
				SFValue="alStatus" codeId="CODEID" codeName="CODENM"
				defaultValue="[Choose One]" /> 
			</td>
		</tr>
		<tr>
			<td colspan="4" class="LLine" height="1"></td>
		</tr>
		<%if(bldFlag.equals("Y")){ %>
		<tr>
			<fmtInvLocationEdit:message key="LBL_BUILDING" var="varBuilding"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varBuilding}:" td="true"/>
			<td height="25">&nbsp;<gmjsp:dropdown controlName="buildingID"
				SFFormName="frmLocation" SFSeletedValue="buildingID"
				SFValue="alBuildingID" codeId="BID" codeName="BNM"
				defaultValue="[Choose One]" tabIndex="1"/></td>
		</tr>
		<%}%>
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr>
			<td colspan="4">
				<table style="display: none;" id='tab_Disable'>
					<tr>
						<td colspan="4">
						<table>
							<TR>
								<td class="RightTableCaption">
								<fmtInvLocationEdit:message key="LBL_ADD" var="varAdd"/><gmjsp:label type="BoldText"  SFLblControlName="${varAdd}:" td="false"/>
								&nbsp;<a href="#"> <fmtInvLocationEdit:message key="IMG_ADD_ROW" var="varAddRow"/><img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="addRow()"height="14"></a>&nbsp;&nbsp;
								<fmtInvLocationEdit:message key="LBL_DEL" var="varDel"/><gmjsp:label type="BoldText"  SFLblControlName="${varDel}:" td="false"/>
								&nbsp;<a href="#"> <fmtInvLocationEdit:message key="IMG_REMOVE_ROW" var="varRemoveRow"/><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
								<fmtInvLocationEdit:message key="LBL_CURR_QTY" var="varCurrQty"/><gmjsp:label type="BoldText"  SFLblControlName="${varCurrQty}=0" td="false" />
								&nbsp;&nbsp;<html:checkbox property="chkCurrQtyFlag" onclick="fnRptReload()" disabled="<%=strCurrQtyDisable%>"></html:checkbox>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" width="100%">
							<div id="dataGrid" style="height: 400px; width: 700px;"></div>
	
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<logic:equal name="frmLocation" property="accessFlag" value="Y">
		<tr id="savebtn_Disable"  style="display: table-row;">
			<td align="center" colspan="4" height="30"><fmtInvLocationEdit:message key="BTN_SAVE" var="varSave"/><gmjsp:button
				value="&nbsp;${varSave}&nbsp;" name="Btn_Submit" style="width: 5em; height: 22" tabindex="11"
				gmClass="button" onClick="javascript:fnSubmit();" buttonType="Save" />
			</td>
		</tr>
		</logic:equal>
		<%if ( (strLocMap.equals("") && !strLocationID.equals("") && strTypeNum.equals("")) ||
				 (!strLocMap.equals("") && !strLocationID.equals("") && strTypeNum.equals(""))){%>
		<tr>
			<td colspan="4" height="30" align="center"> No Data Available </td>
		</tr>
		<% } %>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>