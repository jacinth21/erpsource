<%
/**********************************************************************************
 * File		 		: GmLocationTransfer.jsp
 * Version	 		: 
 * author			:
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtLocationTransfer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtLocationTransfer:setBundle basename="properties.labels.operations.inventory.GmLocationTransfer"/>
<%@ page isELIgnored="false" %>
<%@ page buffer="16kb" autoFlush="true" %>

<bean:define id="alWareHouse" name="frmLocationTransfer" property="alWareHouse" type="java.util.ArrayList"> </bean:define>

<%
response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
response.setHeader("Pragma", "no-cache"); //HTTP 1.0
response.setDateHeader("Expires", 0); //prevents caching at the proxy server 

String strOperationsInventoryJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_INVENTORY");

%> 

<html>
<head>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<title>Bulk Location Transfer</title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all"> @import url("<%=strCssPath%>/screen.css");</style>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_fast.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_form.js"></script>
<!-- Method allows user to add new row from clipboard. New row is being added to the last position in the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<!-- paste content of clipboard into block selection of grid -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script type="text/javascript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_markers.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<!-- The library provides support for different keyboard commands that let users to navigate through the grid. -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strOperationsInventoryJsPath%>/GmLocationTransfer.js"></script>

<script>
var objGridData;
var lblWarehouse = '<fmtLocationTransfer:message key="LBL_WAREHOUSE"/>';

var arrValue = new Array(<%=alWareHouse.size()%>);
<%
for(int i=0; i<alWareHouse.size();i++){
	HashMap hmValue = (HashMap)alWareHouse.get(i);
	String strID = GmCommonClass.parseNull((String)hmValue.get("ID"));
	String strType = GmCommonClass.parseNull((String)hmValue.get("WAREHOUSE_TYPE"));
%>
	arrValue[<%=i%>] = "<%=strID%>,<%=strType%>";
<%}%>
</script>
</head>
<form name="frmLocationTransfer">
<body onload="javascript:fnOnPageLoad();" leftmargin="20" topmargin="10">

<input type="hidden" name="method" value="saveReplenishment" />

<input type="hidden" name="v_str" value="" />

<input type="hidden" name="hWareHouseType" value="" />


	<table border="0" class="DtTable700"  cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="4">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
						<td height="25" class="RightDashBoardHeader" colspan="2">&nbsp;<fmtLocationTransfer:message key="LBL_BULK_LOCATION_TRANSFER"/></td>
						<%-- <td  height="25" class="RightDashBoardHeader" align="right" colspan="2">
							<fmtStandardCostBulkUpload:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			      			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>',' <%=strWikiTitle%>');" />
			      			</td> --%>
		      		</tr></table>
	      		</td>
			</tr>
			<tr>
				<td class="RightTableCaption"  colspan="2" align="right" height="30" width="100">&nbsp;<gmjsp:label type="MandatoryText" SFLblControlName="${varVendorName}" td="false"/><fmtLocationTransfer:message key="LBL_WAREHOUSE"/></td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="warehouseid" SFFormName="frmLocationTransfer" SFSeletedValue="warehouseid" SFValue="alWareHouse" codeId="ID" codeName="WHNM" onChange="fnRefreshGrid();" defaultValue="[Choose One]" tabIndex="1" />
				&nbsp;&nbsp;&nbsp;	
			</tr>
			<tr><td class="LLine" colspan="4" height="1"></td></tr>
		<tr  style="display: none" id="trImageShow"  class="Shade" height="25">
		<td colspan="4" style="padding-right: 94%;">
		<table>	
				<tr  height="25">		
						<td  width="20" colspan="1" tabIndex="-1" ><fmtLocationTransfer:message key="IMG_ALT_ADD_ROW" var="varAddRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:addRow()"height="14"></a></td>
						<td  width="20" colspan="1" tabIndex="-1"><fmtLocationTransfer:message key="IMG_ALT_REMOVE_ROW" var="varRemoveRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a></td>
					</td>
			   </tr>
			   										
		</table>
		</td>
		</tr>	
		<tr><td class="LLine" colspan="4" height="1"></td></tr>
		<tr id="trDiv">
              <td colspan="4">
	         <div id="dataGridDiv"  height="400px"></div></td>
        </tr> 
		<tr><td class="LLine" colspan="4" height="1"></td></tr>
		
		   <tr style="display: none" id="buttonshow" height="30"><td colspan="4"  align="center">
           
           <div id="buttonDiv">
	            <fmtLocationTransfer:message key="BTN_SUBMIT" var="varSubmit"/>
	            <gmjsp:button value="${varSubmit}" gmClass="button" name="LBL_SUBMIT" onClick="fnSubmit();"  buttonType="Save"/>
			</div>
			
			</td>
			
			</tr> 
        		
		</table>

</body>
</form>
<%@ include file="/common/GmFooter.inc"%>	
</html>