<!-- \operations\inventory\GmInHouseListReport.jsp -->
 <%
/**********************************************************************************
 * File		 		: GmInHouseListReport.jsp
 * Desc		 		: This screen is used for displaying InHouse count
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.*,java.text.*" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->

<%@ include file="/common/GmHeader.inc" %>



<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>

<%@ taglib prefix="fmtInHouseListReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtInHouseListReport:setLocale value="<%=strLocale%>"/>
<fmtInHouseListReport:setBundle basename="properties.labels.operations.inventory.GmInHouseListReport"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	
	
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	HashMap hmTemp = new HashMap();
	strApplDateFmt = strApplDateFmt + " K:m:s a";
	SimpleDateFormat df = new SimpleDateFormat(strApplDateFmt);
	String strDate = (String)df.format(new java.util.Date());
	String strUserName = (String)session.getAttribute("strSessShName");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: InHouse Inventory </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>

</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmInv" method="POST" action="<%=strServletPath%>/GmInvListReportServlet">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
		
			<td height="25" class="RightDashBoardHeader">&nbsp;<%=strhOpt%> <fmtInHouseListReport:message key="LBL_SETS_LIST_REPORT"/> </td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="requestScope.REPORT.rows" export="true" class="its" id="currentRowObject"> 
			<display:setProperty name="export.excel.filename" value="InHouseListReport.xls" />
			
			<display:caption><fmtInHouseListReport:message key="DT_REPORT_AS_OF"/> <%=strDate%>   <fmtInHouseListReport:message key="DT_BY"/>:<%=strUserName%></display:caption>
			
			<fmtInHouseListReport:message key="DT_SET_ID" var="varSetID"/>
				<display:column property="SETID" title="${varSetID}" group="1" class="alignleft" sortable="true" />
				
				<fmtInHouseListReport:message key="DT_SET_NAME" var="varSetName"/>
				<display:column property="SNAME" title="${varSetName}" group="1" class="alignleft" sortable="true" />
				
				<fmtInHouseListReport:message key="DT_CONSIGN_ID" var="varConsignID"/>
				<display:column property="CONID" title="${varConsignID}" class="alignleft" sortable="true" />
				
				<!-- PMT-22758 -->
				<fmtInHouseListReport:message key="DT_TAG_ID" var="varTagID"/>
				<display:column property="TAG_ID" title="${varTagID}" class="alignleft" sortable="true" />
				
				<fmtInHouseListReport:message key="DT_ETCH_ID" var="varEtchID"/>
				<display:column property="ETCHID" title="${varEtchID}" sortable="true" class="alignleft" />
				
				<fmtInHouseListReport:message key="DT_STATUS" var="varStatus"/>
				<display:column property="LOANSFL" title="${varStatus}" sortable="true" class="alignleft" />
			</display:table>
			</td>
		</tr>
    <table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>