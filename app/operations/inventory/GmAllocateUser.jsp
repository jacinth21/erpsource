 <!-- \operations\inventory\GmAllocateUser.jsp -->
 <%
/*********************************************************************************************************
 * File		 		: GmAllocateUser.jsp
 * Desc		 		: This screen is used to display Inventory User's and Can change the Status (Manually)
 * Version	 		: 1.0
 * author			: Vamsi Kiran Nagavarapu
**********************************************************************************************************/
%>

<%@ page language="java"%>

<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtAllocateUser" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtAllocateUser:setLocale value="<%=strLocale%>"/>
<fmtAllocateUser:setBundle basename="properties.labels.operations.inventory.GmAllocateUser"/>

<bean:define id="gridData" name="frmAllocateUser"
	property="gridXmlData" type="java.lang.String">
</bean:define>


<%
	String strWikiTitle = GmCommonClass.getWikiTitle("ALLOCATE_USER");
	try {
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Status Log Detail</TITLE>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmStatusLog.js"></script>
<script>

var objGridData = '<%=gridData%>';
		
function fnOnPageLoad()
{
	var lbltotal = '<fmtAllocateUser:message key="LBL_TOTAL"/>';
	gridObj = initGrid('dataGrid',objGridData);
	gridObj.attachFooter(""+lbltotal+",#cspan,<div align='center'>-</div>,<div id='assigned' align = 'left'>0</div>,<div id='initiated' align = 'left'>0</div>,<div id='wip' align = 'left'>0</div>,<div id='suspended' align = 'left'>0</div>,<div id='close'  align = 'left'>0</div>", ["text-align:center;"]);
	var totalAssigned = document.getElementById("assigned");
    totalAssigned.innerHTML = sumColumn(3);
	var totalInitiated = document.getElementById("initiated");
    totalInitiated.innerHTML = sumColumn(4);
    var totalWip = document.getElementById("wip");
    totalWip.innerHTML = sumColumn(5);
    var totalSuspended = document.getElementById("suspended");
    totalSuspended.innerHTML = sumColumn(6);
    var totalClose = document.getElementById("close");
    totalClose.innerHTML = sumColumn(7);
	
	
}

function sumColumn(ind) {
    var out = 0;
    for (var i = 0; i < gridObj.getRowsNum(); i++) {
        out += parseFloat(gridObj.cells2(i, ind).getValue());
    }
    return out;
}

function fnRptReload()
{
	document.frmAllocateUser.haction.value = "rptReload";
	fnStartProgress('Y');
	document.frmAllocateUser.submit();
}

function addRow(){
	gridObj.addRow(gridObj.getUID(),'');
}

function removeSelectedRow(){

	var gridrows=gridObj.getSelectedRowId();
	if(gridrows!=undefined){
		var NameCellValue ;
		var gridrowsarr = gridrows.toString().split(",");
		var added_row;
		for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
			gridrowid = gridrowsarr[rowid];
			
			NameCellValue = gridObj.cellById(gridrowid, 0).getValue();
			added_row = gridObj.getRowAttribute(gridrowid,"row_added");
			
			if(TRIM(NameCellValue).length==0 || (added_row!=undefined &&added_row=="Y")){
				gridObj.deleteRow(gridrowid);
			}else{	
			Error_Clear();
			Error_Details(message_operations[378]);				
			Error_Show();
			Error_Clear();
			}
			setRowAsModified(gridrowid,true);
		}
	}else{
		Error_Clear();
		Error_Details(message_operations[379]);				
		Error_Show();
		Error_Clear();
	}
}



function fnSubmit()
{
		gridObj.editStop();// return value from editor(if presents) to cell and closes editor
		var gridrowid;
		var void_flag;
		var finalData='';
		var gridrows =gridObj.getChangedRows(",");
		
		if(gridrows.length==0){
			Error_Clear();	
			Error_Details(message_operations[380]);
			Error_Show();
			Error_Clear();
		}
		else
		{
		
			var gridrowsarr = gridrows.toString().split(",");
			var columnCount = gridObj.getColumnsNum();
		
			
			for (var rowid = 0; rowid < gridrowsarr.length; rowid++) {
				gridrowid = gridrowsarr[rowid];
			
				for(var col=0;col<columnCount;col++){
					
					finalData+=gridObj.cellById(gridrowid, col).getValue()+'^';
					
				}
				
				finalData+='|';
			}
			finalData+="$";
		
		
		  
		document.frmAllocateUser.strOpt.value = 'save';
		document.frmAllocateUser.strFinalData.value = finalData;
		fnStartProgress('Y');
		document.frmAllocateUser.submit();
		
		}	
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">

<html:form action="/gmallocateuser.do">
	<html:hidden property="strOpt"  value=""/>
	<html:hidden property="strFinalData" value=""/>
	<html:hidden property="haction" value="rptReload" />

	<table border="0" class="DtTable700"   cellspacing="0"
		cellpadding="0">
		<tr>
			<td colspan="5" height="25" class="RightDashBoardHeader"><fmtAllocateUser:message key="LBL_ALLOCATEUSER"/></td>
			<fmtAllocateUser:message key="IMG_ALT_HELP" var="varHelp"/>
			<td align="right" class=RightDashBoardHeader><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}' width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<td colspan="6" class="Line" height="1"></td>
		</tr>
		
		
		<tr>
		<td>
		<!--<logic:notEqual name="frmAllocateUser" property="haction" value="initiating">--> 
			<table  >
						<TR >
								<td class="RightTableCaption">
							
								<fmtAllocateUser:message key="LBL_ADD"/>
								&nbsp;<a href="#"><fmtAllocateUser:message key="IMG_ADD_ROW" var="varAddRow"/> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="addRow()"height="14"></a>&nbsp;&nbsp;
							
								<fmtAllocateUser:message key="LBL_DEL"/>
								&nbsp;<a href="#"> <fmtAllocateUser:message key="IMG_REMOVE_ROW" var="varRemoveRow"/><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>&nbsp;&nbsp;
								</td>										
							</TR>
				
					</table>
</td>
</tr>			
			<tr>
			
				<td colspan="6" width="100%">

						<div id="dataGrid" height ="400"></div>

				</td></tr>
		
		<tr class="Shade">
		
		
		
		<td align="center" colspan="6" height="30">
		<fmtAllocateUser:message key="BTN_SAVE" var="varSave"/>
		<gmjsp:button
					value="&nbsp;${varSave}&nbsp;" name="submitbtn" tabindex="11"
						gmClass="button" onClick="javascript:fnSubmit();" buttonType="Save"/> &nbsp;  
						<fmtAllocateUser:message key="LBL_LOAD" var="varLoad"/>
						<gmjsp:button
				value="&nbsp;${varLoad}&nbsp;" name="Btn_Submit" gmClass="button"
				onClick="fnRptReload();" buttonType="Load" /></td>
					
					
				</tr>
<!-- 	</logic:notEqual> -->	
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
<%
	} catch (Exception e) {
		e.printStackTrace();
	}
%>
</BODY>
</HTML>