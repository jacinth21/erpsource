<!--\operations\inventory\GmInvLocationPrint.jsp  -->
<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*" %>

<%@ taglib prefix="fmtInvLocationPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtInvLocationPrint:setLocale value="<%=strLocale%>"/>
<fmtInvLocationPrint:setBundle basename="properties.labels.operations.inventory.GmInvLocationPartMappingReport"/>
<bean:define id="alResult" name="frmLocation" property="alInvLocList" type="java.util.ArrayList"></bean:define>
<%
	String strHtmlJasperRpt = "";	
%>



<HTML>
<HEAD>
<TITLE> Globus Medical: Inventory Location Print </TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
    table.DtTable1000 {
	width: 1200px;
	}
</style>

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script>

function fnPrint()
{
/* Commenting the below code for fixing the Location Print screen issues.
var prtContent = document.getElementById("jasper");
var WinPrint = document.getElementById("ifmPrintContents").contentWindow;
WinPrint.document.open();
WinPrint.document.write(prtContent.innerHTML);
WinPrint.document.close();
WinPrint.focus();
WinPrint.print(); */

 var divElements = document.getElementById('jasper').innerHTML;
 var oldPage = document.body.innerHTML;
 document.body.innerHTML =   "<html><head><title></title></head><body>" +  divElements + "</body>";
 window.print();
 document.body.innerHTML = oldPage;
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
 
<html:form  action="/gmlocation.do"  >
 <table align="center" height="40" width="100%"><tr><td align="center">
 <fmtInvLocationPrint:message key="LBL_PRINT" var="varPrint"/>
 <gmjsp:button value="${varPrint}" gmClass="button" onClick="fnPrint();" buttonType="Load" />
 </td></tr></table>	
 <iframe id="ifmPrintContents" style="height: 0px; width: 0px; position: absolute"></iframe>
<div id="jasper" style="position: absolute" align="center"> 
		<%
		String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
			GmJasperReport gmJasperReport = new GmJasperReport();								
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("/GmInvLocationPrintReport.jasper");
			//gmJasperReport.setHmReportParameters(hmPrintParam);							
			gmJasperReport.setReportDataList(alResult);	
			gmJasperReport.setBlDisplayImage(true);
			strHtmlJasperRpt = gmJasperReport.getHtmlReport();	
		%>
		<%=strHtmlJasperRpt %>				
</div>	

</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>