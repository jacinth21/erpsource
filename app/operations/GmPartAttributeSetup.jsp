<%
/**********************************************************************************
 * File		 		: GmPartAttributeSetup.jsp
 * Desc		 		: This screen is used for setting partnumber attrbutes 
 * Version	 		: 1.0
 * author			: DhanaReddy
************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtPartAttributeSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtPartAttributeSetup:setLocale value="<%=strLocale%>"/>
<fmtPartAttributeSetup:setBundle basename="properties.labels.operations.GmPartAttributeSetup"/>

<!--  operations\GmPartAttributeSetup.jsp-->

<bean:define id ="alAttrType" name="frmPartAttributeSetup" property="alAttrType" type="java.util.ArrayList"/>
<bean:define id ="hmAttrVal" name="frmPartAttributeSetup" property="hmAttrVal" type="java.util.HashMap"/>
<bean:define id ="attributevalue" name="frmPartAttributeSetup" property="attributevalue" type="java.lang.String"/>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: PartAttribute Setup</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmPartAttributeSetup.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<%
    String strWikiTitle = GmCommonClass.getWikiTitle("PART_ATTRIBUTE_SETUP_BULK");
	ArrayList alAttrVal = new ArrayList();
	HashMap hmAttrType = new HashMap();
	HashMap hmAtrVal = new HashMap();
	for (int i=0;i<alAttrType.size();i++)
	{
		hmAttrType = (HashMap)alAttrType.get(i);
		alAttrVal =(ArrayList)hmAttrVal.get(hmAttrType.get("CODENMALT"));
		int arrLen = alAttrVal.size();
		String strCodeid = (String)hmAttrType.get("CODEID");
		%>
	<script>put('<%=strCodeid%>','<%=arrLen%>');</script>	
		<%
		for(int j=0; j<alAttrVal.size();j++){
			hmAtrVal = (HashMap)alAttrVal.get(j);
			String strCode = (String)hmAtrVal.get("CODEID");
			String strCodenm = (String)hmAtrVal.get("CODENM");
%>
<script>put('<%=strCodeid%><%=j%>','<%=strCode%>,<%=strCodenm%>');</script>
<%
	}
	}
%> 
<script>
var selectID = '<%=attributevalue%>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnPageLoad();">
<FORM name="frmPartAttributeSetup" action="/gmPartAttribute.do" >
<html:hidden property="strOpt" name ="frmPartAttributeSetup"/>

<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader" width="300">&nbsp;<fmtPartAttributeSetup:message key="LBL_PART_ATTR"/></td>
		<td height="25" class=RightDashBoardHeader align="right"  ><fmtPartAttributeSetup:message key="IMG_HELP" var="varHelp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
					                  onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
		<td align="right" class=RightDashBoardHeader></td>
	</tr>
	<tr class="Shade">
		<td colspan="2" bgcolor="#CCCCCC" height="1"></td>
	</tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtPartAttributeSetup:message key="LBL_PART_NUM"/>:</td>
		<td > &nbsp;<html:text property="partnumber" name="frmPartAttributeSetup" size="20" onkeypress="javascript:fnClearMessage();"/></td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtPartAttributeSetup:message key="LBL_ATTR_TYPE"/>:</td>
		<td align="left">&nbsp;<gmjsp:dropdown width="200"	controlName="attributetype" SFFormName="frmPartAttributeSetup" SFSeletedValue="attributetype" SFValue="alAttrType" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" onChange="javascript:fnAttributeVal(this.value);fnClearMessage();"/></td>
	</tr>
	<tr><td colspan="2" height="1" class="LLine"></td></tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtPartAttributeSetup:message key="LBL_ATTR_VALUE"/>:</td>
		<td align="left">&nbsp;<gmjsp:dropdown width="200"	controlName="attributevalue" SFFormName="frmPartAttributeSetup" SFSeletedValue="attributevalue" SFValue="alAttrVal" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"  onChange="javascript:fnClearMessage();"/></td>
	</tr>
	<tr>
		<td colspan="2" height="1" class="LLine"></td>
	</tr>
	<tr>
		<td colspan="2" align="center" height="30">
		<fmtPartAttributeSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" buttonType="Save" />
		<fmtPartAttributeSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button value="&nbsp;${varReset}&nbsp;" gmClass="button" onClick = "fnReset();" buttonType="Save" />
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
		<div id="msg_text">
			<html:messages id="msg" message="true" property="success">
				<font style="color: green"><b><bean:write name="msg"/></b></font><br>
			</html:messages>
		</div>
		</td>	
	</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
