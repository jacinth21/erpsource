 <%
/**********************************************************************************
 * File		 		: GmSterilizeOrder.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<!--operations\GmSterilizeOrder.jsp  -->
<%@ taglib prefix="fmtSterilizeOrder" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSterilizeOrder:setLocale value="<%=strLocale%>"/>
<fmtSterilizeOrder:setBundle basename="properties.labels.operations.GmSterilizeOrder"/>


<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	String strVendorId = (String)request.getAttribute("hVendId")== null?"":(String)request.getAttribute("hVendId");
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strShade = "";
	String strTemp = "";
	ArrayList alVendList = (ArrayList)hmReturn.get("VENDORLIST");

	String strDHRId = "";
	String strVendorNm = "";
	String strPartNumber = "";
	String strQty = "";
	String strControl = "";
	String strDesc = "";
	String strCost = "";

	ArrayList alVendorList = new ArrayList();

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR''s for Sterilization </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnCallReport(vid)
{
	document.frmVendor.hAction.value = 'SterlizeLoad';
	document.frmVendor.hVenId.value = vid;
	document.frmVendor.action = "<%=strServletPath%>/GmReworkServlet";
  	document.frmVendor.submit();
}

function fnSubmit()
{
	var len = document.frmVendor.hTextboxCnt.value;
	var vVend = '<%=strVendorId%>';
	var vPart = '';
	var vQty = 0;
	var vCost = 0;
	var str = '';
	var temp = 0;
	var vRev = '';
	var vFl = '';
	var vDhr = '';
	var vFAR = 'N';
	
	for (var i=0;i<len;i++)
	{
		val = eval("document.frmVendor.hPartNum"+i);
		vPart = val.value;
		val = eval("document.frmVendor.hCost"+i);
		vCost = val.value;
		val = eval("document.frmVendor.Txt_Qty"+i);
		vQty = val.value;
		vRev = '-';
		val = eval("document.frmVendor.Chk_Flag"+i);
		vFl = val.checked;
		vDhr = eval("document.frmVendor.hDHR"+i);
		vDhr = vDhr.value;

		if (vFl)
		{
			vFl = 'Y';
		}
		else
		{
			vFl = 'N';
		}

		if (vCost != '' && vQty !='' && vRev != '')
		{
			temp = temp + vCost*vQty;
			str = str + vPart+"^"+vRev+"^"+vVend+"^"+vQty+"^"+vCost+"^"+vFl+"^"+vDhr+"^"+vFAR+"|";
			//str = str + vPart+"^"+vRev+"^"+vVend+"^"+vQty+"^"+vCost+"^"+vFl+"^"+vDhr+"|";
		}
	}

	document.frmVendor.hPOString.value = vVend +',' +temp+'|';
	document.frmVendor.hWOString.value = str.substring(0,str.length-1);
	document.frmVendor.hAction.value = "Initiate";
	//alert("WO: "+document.frmVendor.hWOString.value);
	//alert("PO: "+document.frmVendor.hPOString.value);
	document.frmVendor.action = "<%=strServletPath%>/GmOrderPlanServlet";
	fnStartProgress('Y');
	document.frmVendor.submit();
}
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPOServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hVenId" value="">
<INPUT type="hidden" name="hPOString" value="">
<INPUT type="hidden" name="hWOString" value="">
<INPUT type="hidden" name="hType" value="3102">

	<table border="0" width="800" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3"><fmtSterilizeOrder:message key="LBL_DHR"/></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="798" height="70" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<TR>
						<td class="RightText" align="right" height="30">&nbsp;<fmtSterilizeOrder:message key="LBL_STR_VEN"/>:</td>
						<td width="200">
						&nbsp;<select name="hVendId" onChange="javascript:fnCallReport(this.value);" class="RightText" tabindex="3"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtSterilizeOrder:message key="LBL_CHOOSE"/>
<%
			  		intSize = alVendList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alVendList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strVendorId.equals(strCodeID)?"selected":"";
						strTemp = (String)hcboVal.get("NAME");
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=GmCommonClass.getStringWithTM(strTemp)%></option>
<%
			  		}
%>
						</select></td>
					</tr>
					<tr>
						<td colspan="2">
							<TABLE cellpadding="1" cellspacing="0" border="0" width="100%">
								<tr><td colspan="9" class="Line" height="1"></td></tr>
								<TR class="ShadeRightTableCaption">
									<TD class="RightText" width="120"><fmtSterilizeOrder:message key="LBL_DHR_ID"/></TD>
									<TD class="RightText" width="60"><fmtSterilizeOrder:message key="LBL_PART_NUM"/></TD>
									<TD class="RightText" width="350"><fmtSterilizeOrder:message key="LBL_DESC"/></TD>
									<TD class="RightText" width="170"><fmtSterilizeOrder:message key="LBL_MANF_VEN"/></TD>
									<TD class="RightText" width="50" align="center"><fmtSterilizeOrder:message key="LBL_QTY_DHR"/><BR></TD>
									<TD class="RightText" align="center"><fmtSterilizeOrder:message key="LBL_CONTRL_NUM"/></TD>
									<TD class="RightText" align="center"><fmtSterilizeOrder:message key="LBL_COST"/></TD>
									<TD class="RightText" align="center" width="50"><fmtSterilizeOrder:message key="LBL_QTY_STR"/> <BR></TD>
									<TD class="RightText" align="center" width="50"><fmtSterilizeOrder:message key="LBL_CRITICAL"/></TD>
								</TR>
								<tr><td colspan="9" class="Line" height="1"></td></tr>
<%
					if (strhAction.equals("SterlizeLoad"))
					{
						HashMap hmLoop = new HashMap();
						ArrayList alList = (ArrayList)hmReturn.get("DHRREPORT");
						intSize = alList.size();

						for (int i = 0;i < intSize ;i++ )
						{
							hmLoop = (HashMap)alList.get(i);

							strDHRId = GmCommonClass.parseNull((String)hmLoop.get("DHRID"));
							strPartNumber = GmCommonClass.parseNull((String)hmLoop.get("PARTNUM"));
							strDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
							strVendorNm = GmCommonClass.parseNull((String)hmLoop.get("VNAME"));
							strVendorId = GmCommonClass.parseNull((String)hmLoop.get("VID"));
							strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
							strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
							strCost = GmCommonClass.parseZero((String)hmLoop.get("COST"));
%>
								<TR id="tr<%=i%>">
									<td height="20" class="RightText"><%=strDHRId%>
									<INPUT type="hidden" name="hDHR<%=i%>" value="<%=strDHRId%>"></td>
									<td class="RightText"><%=strPartNumber%>
									<INPUT type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumber%>"></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strDesc)%></td>
									<td class="RightText" ><%=strVendorNm%></td>
									<td class="RightText" align="center"><%=strQty%></td>
									<td class="RightText" align="center"><%=strControl%></td>
									<td class="RightText" align="right"><%=strCost%>
									<INPUT type="hidden" name="hCost<%=i%>" value="<%=strCost%>"></td>
									<td class="RightText"align="center"><input type="text" size="3" class="InputArea"  name="Txt_Qty<%=i%>" onFocus="changeTRBgColor(<%=i%>,'#AACCE8');" onBlur="changeTRBgColor(<%=i%>,'#ffffff');"></td>
									<td align="center"><input type="Checkbox" name="Chk_Flag<%=i%>"></td>
								<TR>
								<tr><td colspan="9" bgcolor="#eeeeee" height="1"></td></tr>
<%
						}
					}
%>
								<tr>
									<td colspan="7" class="RightTextRed" align="center" height="25"> 
									 <fmtSterilizeOrder:message key="LBL_SUBMIT"/>
										<INPUT type="hidden" name="hTextboxCnt" value="<%=intSize%>">
										<fmtSterilizeOrder:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;${varSubmit}&nbsp;" controlId="Btn_Submit" gmClass="button" onClick="fnSubmit();" tabindex="16" buttonType="Save" />&nbsp;	
									</td>
								</tr>
							</TABLE>
						</TD>
				   </TR>
				</TABLE>
			</TD>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
