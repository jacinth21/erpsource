<%
/**********************************************************************************
 * File		 		: GmPartConversionDetails.jsp
 * Desc		 		: Report for Part Conversion Detail
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib uri="/WEB-INF/struts-logic-el.tld" prefix="logicel" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPartConversionDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartConversionDetails:setLocale value="<%=strLocale%>"/>
<fmtPartConversionDetails:setBundle basename="properties.labels.operations.GmPartConversionDetails"/>
<!-- operations.GmPartConversionDetails.jsp -->
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%> 
<bean:define id="strOpt" name="frmPartConversion" property="strOpt" type="java.lang.String"> </bean:define>

<%
// Set to expire far in the past.
response.setHeader("Expires", "Sat, 6 May 1995 12:00:00 GMT");
// Set standard HTTP/1.1 no-cache headers.
response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
// Set IE extended HTTP/1.1 no-cache headers (use addHeader).
response.addHeader("Cache-Control", "post-check=0, pre-check=0");
// Set standard HTTP/1.0 no-cache header.
response.setHeader("Pragma", "no-cache");
%>

<% //Logger log = GmLogger.getInstance(GmCommonConstants.CLINICAL);  // Instantiating the Logger 
try{
	//String strCssPath = GmCommonClass.getString("GMSTYLES");
	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Part Conversion Detail </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>

function fnPrint()
{
	window.print();
}

var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

function formKeyValidate() {
// keycode for F5 function
if (window.event && window.event.keyCode == 116) {

window.event.cancelBubble = true;
window.event.returnValue = false;
cancelRefresh();
}
}

function DisablingBackFunctionality()
{
var URL;
var i ;
var QryStrValue;
URL=window.location.href ;
i=URL.indexOf("?");
QryStrValue=URL.substring(i+1);
if (QryStrValue!='X')
{
window.location=URL + "?X";
}
}

function fnLoad(){
	document.frmPartConversion.strOpt.value = "report";
	document.frmPartConversion.action = "/gmPCDetail.do?method=reportPCDetail"
		fnStartProgress('Y');
	document.frmPartConversion.submit();
}
// javascript:window.history.forward(-1);


function fnRedirect(){
	var varOpt = document.frmPartConversion.strOpt.value;
	if (varOpt == 'save'){
		setTimeout('afterFiveSeconds()',5000)
	}
}

function afterFiveSeconds(){
	window.location.href = "/gmManualAdjTxn.do";
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10"  onKeyDown="formKeyValidate()" > 

<html:form action="/gmPCDetail.do"  >
<html:hidden property="strOpt"/> 

<!-- --> <table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
<!--  -->	<tr> 
				<td height="25" class="RightDashBoardHeader"><fmtPartConversionDetails:message key="LBL_PART_CONV_DET"/></td> 
			</tr>
		
<!--  -->	<tr>
			  <td height="100" valign="top">
				
<!-- i -->		<table border="0" cellspacing="0" cellpadding="0" >
<!-- i1 -->		<tr>
	            	<td class="RightTableCaption" align="right"  height="30" nowrap ><fmtPartConversionDetails:message key="LBL_PC_ID"/>:&nbsp;</td>
	            	<td> 
<% if(strOpt.equals("FromMenu")){ %>
	            		<html:text property="pcID"  size="22" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
	            		&nbsp;&nbsp;<fmtPartConversionDetails:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" name="Btn_Load" gmClass="button" onClick="fnLoad();" buttonType="Load"  />
<%} else { %>
	            		<bean:write name="frmPartConversion" property="pcID" />
<%}%>	            		 
	            	</td>
	            	
	                 <td class="RightTableCaption"  height="30" align="right" nowrap><fmtPartConversionDetails:message key="LBL_PC_DESC"/> :&nbsp;</td>
   					<td> <bean:write name="frmPartConversion" property="partConversionComments" /></td>
				</tr>
<!-- i2 -->		<tr><td colspan="4" class="LLine"></td></tr>
<!-- i3 -->		<tr>
					<td class="RightTableCaption"  height="30"  align="right"  nowrap><fmtPartConversionDetails:message key="LBL_PC_TYPE"/>:&nbsp;</td>
    	            <td>  <bean:write name="frmPartConversion" property="fromInvType" /> </td>
    	            
    	            <td class="RightTableCaption"  height="30"  align="right" nowrap  ><fmtPartConversionDetails:message key="LBL_PART_DET"/>:&nbsp;</td>
    	            <td >  <bean:write name="frmPartConversion" property="partDescription" /> </td>
    	            
				</tr>
<!-- i4 -->     <tr><td colspan="4" class="LLine"></td></tr>
<!-- i5 -->		<tr>
                    <td class="RightTableCaption" HEIGHT="24" align="right"><fmtPartConversionDetails:message key="LBL_CREATED_BY"/>:&nbsp;</td>                     
                    <td> <bean:write name="frmPartConversion" property="createdDate" /> </td>
   		            <td class="RightTableCaption" HEIGHT="24" align="right">&nbsp;<fmtPartConversionDetails:message key="LBL_CREATED_BY"/>:&nbsp;</td>
                    <td><bean:write name="frmPartConversion" property="createdBy" />   </td>
    		     </tr>
    		     
<!-- i6 -->      <tr><td colspan="4" class="Line"></td></tr>
<!-- i7 -->      <tr>
					<td colspan="4">           

					<!--  display:Table tag here --> 
					<display:table cellspacing="0" cellpadding="0" class="its" name="requestScope.frmPartConversion.ldtResult"  varTotals="totals" export="false"  requestURI="/gmPCDetail.do?method=reportPCDetail"  >				    																		     							
					<fmtPartConversionDetails:message key="DT_PART#" var="varPart"/><display:column property="PNUM" title="${varPart}" style="text-align:left"  sortable="true"   />								
					<fmtPartConversionDetails:message key="DT_PART_DESC" var="varPartDesc"/><display:column property="PARTDESC" title="${varPartDesc}"     maxLength="80" sortable="true" />
					<fmtPartConversionDetails:message key="DT_QTY" var="varQty"/><display:column property="PCQTY" title="${varQty}" style="text-align:right" />					
					<fmtPartConversionDetails:message key="DT_COST_EA" var="varCostEa"/><display:column property="PCPRICE" title="${varCostEa}" class="alignright" format="{0,number,$#,###,###.00}"  />
					<fmtPartConversionDetails:message key="DT_EXT_COST" var="varExtCost"/><display:column property="EXTCOST" title="${varExtCost}" class="alignright"  total="true" />
					<display:footer media="html">
					<% 
							String strVal ;
							strVal = ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
						
					  		//String strCurVal = " $ " + GmCommonClass.getStringWithCommas(strVal); //before red total
					  		
					  		String strCurVal = " $ " +  GmCommonClass.getRedText( GmCommonClass.getStringWithCommas(strVal)  ); 
					  	
								
					%>
					<tr><td colspan="5" class="Line"></td></tr>
	  				<tr class = "shade">
			  			<td colspan="4" class = "alignright"> <B><fmtPartConversionDetails:message key="LBL_TOT_COST"/> : </B></td>
			  			<td class = "alignright" ><B><%=strCurVal%></B></td>
	  				</tr>
	 				</display:footer>
					</display:table>		

					</td></tr>		
				 <tr><td colspan="4" class="Line"></td></tr>
			</table>
																
<!--  -->		</td>
<!-- o2 -->	</tr>
	
<!-- o3  --><tr>
				<td align="center" height="30" id="button">
				<logic:notEqual  name="frmPartConversion" property="strOpt" value="FromMenu">
					<fmtPartConversionDetails:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				</logic:notEqual>				
				</td>
				<tr>
				
				<tr>
				<td align="center" height="30" >
				<logic:equal  name="frmPartConversion" property="strOpt" value="save">
					<B> <font color="red"> <i> </B> </font> </i>
				</logic:equal>				
				</td>
				<tr>
	
	
</table>
			   	
</html:form>

<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>

