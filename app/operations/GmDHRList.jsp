 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%
/**********************************************************************************
 * File		 		: GmDHRList.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtDHRList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRList.jsp -->

<fmtDHRList:setLocale value="<%=strLocale%>"/>
<fmtDHRList:setBundle basename="properties.labels.operations.GmDHRList"/>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strhMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strhOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	String strhFromPage = GmCommonClass.parseNull((String)request.getAttribute("hFromPage"));
	String strWikiTitle = GmCommonClass.getWikiTitle("DASHBOARD_GENERATE_DHR");
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.operations.GmDHRList", strSessCompanyLocale);
	
	String strShade = "";
	String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DASHBOARD_GENERATED_DHR"));

	String strDHRId = GmCommonClass.parseNull((String)request.getAttribute("hDHRId"));
	String strVendorId = (String)request.getAttribute("hVendId")==null?"":(String)request.getAttribute("hVendId");
	String strPartNum = "";
	String strPartDesc = "";
	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");
	String strPartNums = (String)request.getAttribute("hPartNums")==null?"":(String)request.getAttribute("hPartNums");
	String strRollBackAccess = GmCommonClass.parseNull((String)(String)request.getAttribute("strRollBackAccess"));

	int intSize = 0;
	HashMap hcboVal = null;
	ArrayList alVendorList = new ArrayList();

	if (strhMode.equals("Report"))
	{
		alVendorList = (ArrayList)session.getAttribute("VENDORLIST");
	}
	String strSelected = "";
	String strCodeID = GmCommonClass.parseNull((String) request.getAttribute("hVendId"));
	strCodeID = (String)request.getAttribute("hVenId")==null?"":(String)request.getAttribute("hVenId");
	String strCodeNM = GmCommonClass.parseNull((String) request.getAttribute("hVendNM"));
	String strQtyRec = "";
	String strControl = "";
	String strDate = "";
	String strPackSlip = "";
	String strWorkId = "";
	String strQtyShelf = "";
	String strPOId = "";
	String strVendor = "";
	String strStatus = "";
	String strNCMRId = "";
	String strUOM = "";
	String strExpiryDt = "";
	String strDonorNumber = "";
	String strProdtype = "";
	String strDHRstatus = "";
	String strSerialNumFl = "";
	
	if (strhMode.equals("Report"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_MODIFY_DHR"));
		strWikiTitle = GmCommonClass.getWikiTitle("DASHBOARD_MODIFY_DHR");
		if(strhFromPage.equals("ByVendor")){
			strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DHR_REPORT_BY_VENDOR"));
			strWikiTitle = GmCommonClass.getWikiTitle("DHR_REPORT_BY_VENDOR");
		}
	}
	String strMODE = (String)session.getAttribute("MODE")==null?"":(String)session.getAttribute("MODE");
	
	String strPnum = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	String strControlNum = GmCommonClass.parseNull((String)request.getAttribute("hControlNum"));
	String strSessApplDateFmt= strGCompDateFmt;
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR List </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmDHRList.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var gStrServletPath = "<%=strServletPath%>";
var gStrOpt = "<%=strhOpt%>";
var rollBackAccess = '<%=strRollBackAccess%>';
var gCmpDateFmt = "<%=strGCompDateFmt%>";
var companyInfoObj = '<%=strCompanyInfo%>';
var strVendorId = '<%=strVendorId%>'
function fnLoad()
{
	document.frmVendor.hVendorId.value = strVendorId;
	if(document.frmVendor.Txt_DHRID != undefined){		
		document.frmVendor.Txt_DHRID.focus();
	}
}
</script>
</HEAD>

<BODY leftmargin="15" topmargin="10" onLoad=fnLoad()>
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hDHRId" value="">
<input type="hidden" name="hVenId" value="<%=strVendorId%>">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">
<input type="hidden" name="hFromPage" value="<%=strhFromPage%>">
	<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">		
		<tr>
			<td colspan="7">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" colspan="6"  class="RightDashBoardHeader"> <%=strHeader%></td>
						<td height="25" class="RightDashBoardHeader" align="right"><fmtDHRList:message key="IMG_ALT_HELP" var = "varHelp"/>
						<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
					</tr>
				</table>
			</td>
		</tr>
<%
		if (strhMode.equals("Report"))
		{
%>
		<tr class="shade">
			 
			<td class="RightTableCaption" align="right" HEIGHT="30"><fmtDHRList:message key="LBL_DHR_ID"/>:&nbsp;</td>
			<td class="RightText">
			    <input type="text" size="20" value="<%=strDHRId%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" 
			    name="Txt_DHRID" style="text-transform:uppercase;">&nbsp;&nbsp;
			</td>  
			<td class="RightTableCaption" align="right" HEIGHT="30"><fmtDHRList:message key="LBL_PART_NUMBER"/>:&nbsp;</td>
			<td class="RightText">
			    <input type="text" size="20" value="<%=strPnum%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_PartNum">&nbsp;&nbsp;
			</td> 
			<td class="RightTableCaption" align="right" HEIGHT="30"><fmtDHRList:message key="LBL_CONTROL_NUMBER"/>:&nbsp;</td>
			<td class="RightText">
			    <input type="text" size="20" value="<%=strControlNum%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_ControlNum">&nbsp;&nbsp;
			</td>   
			 <td> </td> 
		</tr>
		<tr><td colspan="7" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  >
			<td class="RightTableCaption" align="right" HEIGHT="30"><fmtDHRList:message key="LBL_VENDOR_NAME"/>:&nbsp;</td>
			<td> <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="hVendorId" />
				<jsp:param name="METHOD_LOAD" value="loadVendorNameList&searchType=LikeSearch" />
				<jsp:param name="WIDTH" value="300" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="TAB_INDEX" value="3"/>
				<jsp:param name="CONTROL_NM_VALUE" value="<%=strCodeNM %>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=strCodeID %>" />
 				<jsp:param name="SHOW_DATA" value="50" />
 				<jsp:param name="AUTO_RELOAD" value="" />                                       
 	 	 	    </jsp:include>  
			<%--  <select name="hVendorId" id="Region" class="RightText" tabindex="3"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >
			<fmtDHRList:message key="LBL_CHOOSE_ONE"/>
<%
				intSize = alVendorList.size();
				hcboVal = new HashMap();
				for (int i=0;i<intSize;i++)
				{
					hcboVal = (HashMap)alVendorList.get(i);
					strCodeID = (String)hcboVal.get("ID");
					strSelected = strVendorId.equals(strCodeID)?"selected":"";
%>
				<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
	  			}
	  			intSize = 0;
%>
				</select>  --%>
			</td>
			<td class="RightTableCaption" HEIGHT="24" align="right"><fmtDHRList:message key="LBL_FROM_DATE"/>:&nbsp;</td>
			<td class="RightTableCaption"><gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(strFromDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFromDt,strSessApplDateFmt).getTime())))%>"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/></td>
			<td class="RightTableCaption" HEIGHT="24" align="right"><fmtDHRList:message key="LBL_TO_DATE"/> :&nbsp; </td>
			<td class="RightTableCaption"><gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(strToDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDt,strSessApplDateFmt).getTime())))%>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/></td>
			<td class="RightTableCaption"><fmtDHRList:message key="BTN_GO" var="varGo"/>
			<gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnGoDHRList();" tabindex="25" buttonType="Load" />
			</td> 
		</tr>
		 
		<tr><td colspan="7" class="Line" height="1"></td></tr>
<%
		}
%>
		<tr>
			<td width="1200" height="70" valign="top" colspan="7">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<TR>
						<td>
							<TABLE cellpadding="1" cellspacing="0" border="0"   width="100%">
								<TR class="Shade">
									<TD class="RightTableCaption" width="80"><fmtDHRList:message key="LBL_DHR_ID"/></TD>
									<TD class="RightTableCaption" width="70" align="center"><fmtDHRList:message key="LBL_PART"/><br><fmtDHRList:message key="LBL_NUMBER"/></TD>
									<TD class="RightTableCaption" width="400"><fmtDHRList:message key="LBL_DESCRIPTION"/></TD>
									<TD class="RightTableCaption" align="center" width="50"><fmtDHRList:message key="LBL_UOM"/>/<BR><fmtDHRList:message key="LBL_UNITS"/></TD>
									<TD class="RightTableCaption" align="center"><fmtDHRList:message key="LBL_QTY_REC"/><br><fmtDHRList:message key="LBL_EXPANDED"/></TD>
									<TD class="RightTableCaption" align="center" width="50"><fmtDHRList:message key="LBL_QTY_IN"/><br><fmtDHRList:message key="LBL_SHELF"/></TD>
									<TD class="RightTableCaption" align="center" width="70"><fmtDHRList:message key="LBL_CONTROL"/></TD>
									<TD class="RightTableCaption" align="center" width="80"><fmtDHRList:message key="LBL_DATE"/></TD>
									<TD class="RightTableCaption" align="center" width="150"><fmtDHRList:message key="LBL_PACK_SLIP"/></TD>
									<TD class="RightTableCaption" align="center" width="80"><fmtDHRList:message key="LBL_PURCHASE"/><BR><fmtDHRList:message key="LBL_ORDER"/></TD>
									<TD class="RightTableCaption" align="center" width="80"><fmtDHRList:message key="LBL_EXPIRY"/><BR><fmtDHRList:message key="LBL_DATE"/></TD>
									<TD class="RightTableCaption" width="120"><fmtDHRList:message key="LBL_NCMR_ID"/></TD>
									<TD class="RightTableCaption" align="center" width="140" colspan="2"><fmtDHRList:message key="LBL_DONOR"/></TD>
								</TR>
								<tr><td colspan="14" class="Line" height="1"></td></tr>
<%
					if (hmReturn != null)
					{
						HashMap hmLoop = new HashMap();
						HashMap hmTempLoop = new HashMap();
						ArrayList alList = (ArrayList)hmReturn.get("DHRLIST");
						intSize = alList.size();

						for (int i = 0;i < intSize ;i++ )
						{
							hmLoop = (HashMap)alList.get(i);

							strDHRId = GmCommonClass.parseNull((String)hmLoop.get("ID"));
							strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
							strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
							strQtyRec = GmCommonClass.parseNull((String)hmLoop.get("QTYREC"));
							strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
							//strDate = GmCommonClass.getStringFromDate((Date)hmLoop.get("CDATE"),strGCompDateFmt);
							strDate = GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("CDATE"),strGCompDateFmt);
							strPackSlip = GmCommonClass.parseNull((String)hmLoop.get("PSLIP"));
							strWorkId = GmCommonClass.parseNull((String)hmLoop.get("WORKID"));
							strQtyShelf = GmCommonClass.parseNull((String)hmLoop.get("SHELFQTY"));
							strPOId = GmCommonClass.parseNull((String)hmLoop.get("POID"));
							strVendor = GmCommonClass.parseNull((String)hmLoop.get("VENDNM"));
							strVendorId = GmCommonClass.parseNull((String)hmLoop.get("VID"));
							strStatus = GmCommonClass.parseNull((String)hmLoop.get("SFL"));
							strNCMRId = GmCommonClass.parseNull((String)hmLoop.get("NCMRID"));
							strUOM = GmCommonClass.parseNull((String)hmLoop.get("UOMANDQTY"));
							//strExpiryDt = GmCommonClass.parseNull((String)hmLoop.get("EXPDT"));
							strExpiryDt = GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("EXPDT"),strGCompDateFmt);
							strDonorNumber = GmCommonClass.parseNull((String)hmLoop.get("DONORNUMBER"));
							strProdtype = GmCommonClass.parseNull((String)hmLoop.get("PRODTYPE"));
							strDHRstatus = GmCommonClass.parseNull((String)hmLoop.get("SFL"));
							strSerialNumFl = GmCommonClass.parseNull((String)hmLoop.get("SERIALNUMNEEDFL"));
							strShade	= (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows

%>
								<TR <%=strShade%>>
<%
			if (strMODE.equals("EDIT")){
%>
									<td class="RightText" height="20" nowrap><input  name="dhr" value="<%=strDHRId%>" id="<%=strStatus%>" type="radio">&nbsp;<a class="RightText" href="javascript:fnCallDHR('<%=strDHRId%>','<%=strVendorId%>')"><%=strDHRId%></a></td>
<%
			}else{
%>
									<td height="20" nowrap><a class="RightText" href="javascript:fnCallDHR('<%=strDHRId%>','<%=strVendorId%>')"><%=strDHRId%></a></td>
<%
			}
%>
									<td class="RightText" align="center">&nbsp;<%=strPartNum%>&nbsp;</td>
									<td class="RightTextAS">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center">&nbsp;<%=strUOM%></td>
									<td class="RightText" align="center">&nbsp;<%=strQtyRec%></td>
									<td class="RightText" align="center">&nbsp;<%=strQtyShelf%></td>
									<td class="RightText" align="center">&nbsp;<%=strControl%></td>
									<td class="RightText" align="center">&nbsp;<%=strDate%></td>
									<td class="RightText" align="center">&nbsp;<%=strPackSlip%></td>
									<td class="RightTextAS" nowrap>&nbsp;<%=strPOId%></td>
									<td class="RightText" align="center" nowrap><%=strExpiryDt%></td>
									<td class="RightTextAS">&nbsp;<%=strNCMRId%><input type="hidden" name="hNcmrId<%=i%>" value="<%=strNCMRId%>"></td>
									<%-- <td class="RightText" align="center" nowrap><%=strDonorNumber%></td> --%>
									
									<td  class="RightText" align="center"  >&nbsp;<%=strDonorNumber%></td>
<%-- 									<%if((strProdtype.equals("100845") && !strDonorNumber.equals("")) || strSerialNumFl.equals("Y")){ // 100845: tissue, "D" icon should show only if the part is of tissue type%> --%>
									<td  class="RightText">
									<%if((strProdtype.equals("100845") && !strDonorNumber.equals("")) || strSerialNumFl.equals("Y")){ // 100845: tissue, "D" icon should show only if the part is of tissue type%>
									<img align="right" id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/d.gif' title='Donor' onclick="javascript:fnCallDonor('<%=strDHRId%>','<%=strhMode%>','<%=strVendorId%>','<%=strDHRstatus%>')" width='14' height='14'/>
									<%} %>
									</td>
									
								<TR>
<%
						}
					}
					if(intSize==0){
%>
								<tr>
									<td colspan="14" align="center" class="RightTextRed"> <fmtDHRList:message key="LBL_DHR_NOT_GENERATED"/> !</td>
								</tr>
<%
					}
%>
							</TABLE>
						</TD>
				   </TR>
<%
			if (strMODE.equals("EDIT") && intSize > 0){
%>				   
					<tr>
						<td colspan="12" class="RightTableCaption" align="Center"><BR><fmtDHRList:message key="LBL_CHOOSE_ACTION"/>:&nbsp;
						<select name="Cbo_Action" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" 
							onBlur="changeBgColor(this,'#ffffff');">
							<option value="0" ><fmtDHRList:message key="LBL_CHOOSE_ONE"/></option>
							<option value="Rec"><fmtDHRList:message key="LBL_EDIT_RECIEVING_INFO"/></option>
							<option value="Rollback"><fmtDHRList:message key="LBL_ROLLBACK_DHR"/></option>
							<option value="voidDHR"><fmtDHRList:message key="LBL_VOID_DHR"/></option>
							</select>
							<fmtDHRList:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnCallEdit();" 
						tabindex="25" buttonType="Save" />&nbsp;<BR><BR><input type="hidden" name="hLen" value="<%=intSize%>">
						</td>
					</tr>
<%
			}
%>				
				</TABLE>
			</TD>
		</tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
