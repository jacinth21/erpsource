 <%
/**********************************************************************************
 * File		 		: GmDHRPrintProductTravelerG.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Bala
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.servlets.GmCommonBarCodeServlet"%>


<%@ taglib prefix="fmtDHRPrintProductTravelerG" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRPrintProductTravelerG.jsp -->

<fmtDHRPrintProductTravelerG:setLocale value="<%=strLocale%>"/>
<fmtDHRPrintProductTravelerG:setBundle basename="properties.labels.operations.GmDHRPrintProductTravelerG"/>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	strCssPath = GmCommonClass.getString("GMSTYLES");
	strImagePath = GmCommonClass.getString("GMIMAGES");
	strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	strCommonPath = GmCommonClass.getString("GMCOMMON");
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmDHRDetails = new HashMap();
	ArrayList alSubDHR = new ArrayList();
	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strPTFooter="";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyRej = "";
	String strManufDate = "";
	String strUserName = "";
	String strCreatedDate = "";
	String strQtyOrdered = "";
	String strUDINo = "";
	String strHideButton = GmCommonClass.parseNull((String)request.getParameter("HideButton"));
	String strCompanyLogo = GmCommonClass.parseNull((String)request.getParameter("CompanyLogo"));
	strCompanyLogo = strCompanyLogo.equals("")?"100800":strCompanyLogo;
	String strApplDateFmt = strGCompDateFmt;
	
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");

		strDHRId = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId = GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName = GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		//strManufDate = GmCommonClass.parseNull((String)hmDHRDetails.get("MDATE"));
		strManufDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("MDATE"),strApplDateFmt);
		strUserName = GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		//strCreatedDate = GmCommonClass.parseNull((String)hmDHRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplDateFmt);
		strQtyOrdered = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYORD"));
		strQtyRej = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ"));
		strPTFooter = GmCommonClass.parseNull ((String)hmDHRDetails.get("PTFOOTER"));
		strUDINo = GmCommonClass.parseNull((String)hmDHRDetails.get("UDINO"));
		strUDINo = strUDINo.replaceAll("<","&lt;");
		strUDINo = strUDINo.replaceAll(">","&gt;");
			alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
		if(strUDINo.equals("")){
			strUDINo = "N/A"; 
		}
	}
	int intSize = 0;
	HashMap hcboVal = null;
	String strArr[] = strPTFooter.split("\\^");
	String strFooter = strArr[0];
	String strDocRev = strArr[1];
	String strDocActiveFl = strArr[2];
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnPrint()
{
	window.print();
}

var tdinnner = "";

function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>
<style type="text/css">
td.LineBreak{
word-break: break-all;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="170">&nbsp;<img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="100">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+2><fmtDHRPrintProductTravelerG:message key="LBL_PRODUCT_TRAVELER"/>&nbsp;</font></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="4"></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
 					<tr>
 						<td class="RightText" height="30" align="right" width="18%"><fmtDHRPrintProductTravelerG:message key="LBL_WORK_ORDER_ID"/>:</td>
						<td class="RightText" width="30%">&nbsp;<%=strWOId%></td>
 						<td  height="30" class="RightText" align="right" width="20%"><fmtDHRPrintProductTravelerG:message key="LBL_DHR_ID"/>:</td>
						<td class="RightText" width="50%">&nbsp;<%=strDHRId%></td>
					</tr> 
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintProductTravelerG:message key="LBL_ORIGINATOR"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>
						<td class="RightText" align="right"><fmtDHRPrintProductTravelerG:message key="LBL_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strCreatedDate%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintProductTravelerG:message key="LBL_PART_#"/>:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightText" align="right"><fmtDHRPrintProductTravelerG:message key="LBL_DESCRIPTION"/>:&nbsp;</td>
						<td class="RightText"><%=GmCommonClass.getStringWithTM(strDesc)%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintProductTravelerG:message key="LBL_LOT_#"/>:</td>
						<td class="RightText">&nbsp;<%=strControlNum%></td>
						<td class="RightText" align="right"><fmtDHRPrintProductTravelerG:message key="LBL_VENDOR"/>:</td>
						<td class="RightText">&nbsp;<%=strVendName%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintProductTravelerG:message key="LBL_UDI"/>:&nbsp;</td>
						<td class="LineBreak"><%=strUDINo%></td>			
						<td class="RightText" align="right"><fmtDHRPrintProductTravelerG:message key="LBL_PACKAGING_LIST_ENCLOSED"/>:</td>
						<td class="RightText">&nbsp;<input type="checkbox">&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_YES"/>&nbsp;&nbsp;<input type="checkbox">&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_NO"/></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>

					<tr><td bgcolor="#666666" colspan="4"></td></tr>

					<tr>
						<td colspan="4" height="15" align="center">
							<font face="verdana" size="-4"><b><fmtDHRPrintProductTravelerG:message key="LBL_VENDOR_ATTACH_CERTIFICATION"/></b>
							</font>
						</td>
					</tr>
					<tr><td bgcolor="#666666" colspan="4"></td></tr>
				</table>
			</td>
		</tr>
	</table>
	<table><tr><td style="height: 2px;"></td></tr></table>
	<table cellpadding="0" cellspacing="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="13" width="1"></td>
			<td bgcolor="#666666" colspan="6" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="13" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="6">&nbsp;<b><fmtDHRPrintProductTravelerG:message key="LBL_RECEIPT"/></b></td></tr>
		<tr><td bgcolor="#666666" colspan="6"></td></tr>
		<tr>
			
			<td class="RightText" width="50" align="right">&nbsp;&nbsp;<%=strQtyOrdered%></td>
			<td class="RightText" width="60" height="30" align="left"><fmtDHRPrintProductTravelerG:message key="LBL_QTY_SPECIFIED"/>&nbsp;</td>
			<td class="RightText" width="50">&nbsp;&nbsp;<%=strQtyRec%></td>
			<td class="RightText" width="60" align="left"><fmtDHRPrintProductTravelerG:message key="LBL_QTY_RECIEVED"/>&nbsp;</td>
			<td width="100">&nbsp;</td>		
			<td width="300" height="30"><b><fmtDHRPrintProductTravelerG:message key="LBL_CLEANING_PERFORMED"/></b>&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_YES"/>&nbsp;&nbsp;
			<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_NO"/>&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_N/A"/></td>
		</tr>
		<tr>
			<td height="15" colspan="5"></td>
			<td width="300" valign="top"><fmtDHRPrintProductTravelerG:message key="LBL_IMPLANTS_ONLY"/></td>
		</tr>
		<tr>
		 <td  colspan="5"></td>
		 <td bgcolor="#666666" colspan="2"></td></tr>
		<tr>
		<tr>
			<td height="30" colspan="6">&nbsp;</td>	
		</tr>
		<tr>
		 <td  colspan="5"></td>
		 <td bgcolor="#666666" colspan="2"></td></tr>
		<tr>
		<tr>
			<td height="30" colspan="6">&nbsp;</td>	
		</tr>		
		<tr>
			<td colspan="6">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td rowspan="1" width="20" height="1"></td>
						<td bgcolor="#666666" height="1"></td>
						<td rowspan="1" width="20" height="1"></td>
						<td bgcolor="#666666" height="1"></td>
						<td rowspan="1" width="40" height="1"></td>
						<td bgcolor="#666666" height="1"></td>
						<td rowspan="1" width="20" height="1"></td>
						<td bgcolor="#666666" height="1"></td>
						
					</tr>
					<tr>
						<td rowspan="1" width="20" height="1"></td>
						<td class="RightText" align="center" height="15"><fmtDHRPrintProductTravelerG:message key="LBL_INITIALS"/></td>
						<td rowspan="1" width="20" height="1"></td>
						<td class="RightText" align="center">Date</td>
						<td rowspan="1" width="40" height="1"></td>
						<td class="RightText" align="center" height="15"><fmtDHRPrintProductTravelerG:message key="LBL_INITIALS"/></td>
						<td rowspan="1" width="20" height="1"></td>
						<td class="RightText" align="center"><fmtDHRPrintProductTravelerG:message key="LBL_DATE"/></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="6"></td></tr>
	</TABLE>

<table><tr><td style="height: 2px;"></td></tr></table>
	<table cellpadding="0" cellspacing="0" border="0" width="700" height="90">
		<tr>
			<td bgcolor="#666666" rowspan="10" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="10" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b><fmtDHRPrintProductTravelerG:message key="LBL_INSPECTION"/></b></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td class="RightText" height="20" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_YES"/></td>
			<td class="RightText" width="200">&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_SHEET_ATTACHED"/></td>
			<td class="RightText" align="right" width="200"><fmtDHRPrintProductTravelerG:message key="LBL_INITIALS"/>:</td>
			<td class="RightText" >&nbsp;</td>
		</tr>
        <tr>
			<td  colspan="3"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>
		<tr>
			<td class="RightText" rowspan="2" valign="top" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_NO"/></td>
			<td class="RightText">&nbsp;</td>
		</tr>
		<tr>
			<td class="RightText" rowspan="2" height="20" height="30"></td>
			<td class="RightText" align="right" width="200"><fmtDHRPrintProductTravelerG:message key="LBL_DATE"/>:</td>
			<td class="RightText" width="200">&nbsp;</td>
		</tr>
		<tr>
			<td  colspan="3"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
	</table>
<table><tr><td style="height: 2px;"></td></tr></table>
	<table cellpadding="0" cellspacing="0" border="0" width="700" height="150">
		<tr>
			<td bgcolor="#666666" rowspan="25" width="1"></td>
			<td bgcolor="#666666" colspan="6" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="25" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="6">&nbsp;<b><fmtDHRPrintProductTravelerG:message key="LBL_INVENTORY_ALLOCATION_PACKAGE_LABEL"/></b></td></tr>
		<tr><td bgcolor="#666666" colspan="6"></td></tr>
		<tr>
			<td class="RightText" width="375">&nbsp;</td>
			<td class="RightText" width="300"  align="middle" ><fmtDHRPrintProductTravelerG:message key="LBL_PACKAGING_REQ"/>.</td>
			<td class="RightText" align="middle">&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_QUANTITY"/></td>
		</tr>
		<tr>
			<td class="RightText" height="20">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<b><fmtDHRPrintProductTravelerG:message key="LBL_FINISHED_GOODS_DHFG"/></td>
			<td class="RightText" align="middle" ><fmtDHRPrintProductTravelerG:message key="LBL_YES"/></td>
		</tr>
		<tr>
			<td  colspan="2"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>
		<tr>
			<td class="RightText" height="20"  width="300">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_BULK_DHBL"/></td>
			<td class="RightText" align="middle" ><fmtDHRPrintProductTravelerG:message key="LBL_NO"/></td>
		</tr>
		<tr>
			<td  colspan="2"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>
		<tr>
			<td class="RightText" height="20" >&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_RAW_MATERIAL_DHRM"/></td>
			<td class="RightText" align="middle"><fmtDHRPrintProductTravelerG:message key="LBL_NO"/></td>
		</tr>
		<tr>
			<td  colspan="2"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>
		<tr>
			<td class="RightText" height="20" >&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_FIRST_ARTICLE_DHFA"/></td>
			<td class="RightText" align="middle"><fmtDHRPrintProductTravelerG:message key="LBL_NO"/></td>
			<td class="RightText" align="right" >&nbsp;</td>
			<td class="RightTableCaption" rowspan="2" align="right" width="180"><fmtDHRPrintProductTravelerG:message key="LBL_INITIALS"/>:</td>
			<td class="RightText" align="right" width="200" >&nbsp;</td>
		</tr>
		<tr>
			<td  colspan="2"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>
		<tr>
			<td class="RightText" height="20" >&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_PACKAGING_HOLD_DHPN"/></td>
			<td class="RightText" align="middle" ><fmtDHRPrintProductTravelerG:message key="LBL_NO"/></td>
		</tr>
		<tr>
			<td  colspan="2"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>
		<tr>
			<td td class="RightText" height="20" >&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_QCSAMPLE_SHQC"/></td>
			<td class="RightText" align="middle"><fmtDHRPrintProductTravelerG:message key="LBL_NO"/></td>
			<td class="RightText" align="right" >&nbsp;</td>
			<td class="RightTableCaption" rowspan="2" align="right" width="180"><fmtDHRPrintProductTravelerG:message key="LBL_DATE"/>:</td>
			<td class="RightText" align="right" width="180" >&nbsp;</td>
		</tr>
		<tr>
			<td  colspan="2"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>
		<tr><td td class="RightText" height="20" >&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_SUPPLY_RAW_MATERIALS_SHRM"/></td>
			<td class="RightText" align="middle"><fmtDHRPrintProductTravelerG:message key="LBL_NO"/></td>
		</tr>
		<tr>
			<td  colspan="2"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>				
		<tr><td td class="RightText" height="20" >&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_RESTRICTED_WAREHOUSE_DHRW"/></td>
			<td class="RightText" align="middle"><fmtDHRPrintProductTravelerG:message key="LBL_YES"/></td>
		</tr>
		<tr>
			<td  colspan="2"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>		
		<tr><td colspan="6"> &nbsp;</td></tr>
		<tr><td bgcolor="#666666" colspan="6"></td></tr>
		
	</table>
<table><tr><td style="height: 2px;"></td></tr></table>
	<table cellpadding="0" cellspacing="0" border="0" width="700" height="90">
		<tr>
			<td bgcolor="#666666" rowspan="10" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="10" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b><fmtDHRPrintProductTravelerG:message key="LBL_INVENTORY_DISPOSITION_DHR_VERIFICATION"/> </b></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td class="RightText" height="20" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_YES"/></td>
			<td class="RightText" align="right" ><fmtDHRPrintProductTravelerG:message key="LBL_INVENTORY_DISPOSTIONED"/>.</td>
			<td class="RightTableCaption" align="right" width="200"><fmtDHRPrintProductTravelerG:message key="LBL_INITIALS"/>:</td>
			<td class="RightText" >&nbsp;</td>
		</tr>
         <tr>
			<td  colspan="3"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>
		<tr>
			<td class="RightText" rowspan="2" valign="top" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_NO"/></td>
			<td class="RightText">&nbsp;</td>
		</tr>
		<tr>
			<td class="RightText" rowspan="2" height="20" height="30"></td>
			<td class="RightTableCaption" align="right" width="200"><fmtDHRPrintProductTravelerG:message key="LBL_DATE"/>:</td>
			<td class="RightText" width="200">&nbsp;</td>
		</tr>
		<tr>
			<td  colspan="3"></td>
			<td  height="1" bgcolor="#666666" align="left"></td>
		</tr>
		<tr>
			<td class="RightTableCaption" height="20" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_QTY_ACCEPTED"/>:&nbsp;[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtDHRPrintProductTravelerG:message key="LBL_QTY_REJECTED"/>:&nbsp;[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
	</table>
		<table><tr><td style="height: 2px;"></td></tr></table>
	<span class="RightText"><%=strFooter%></span>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
	<%if(!strHideButton.equals("Y"))
			{ %>
	<div id="button">
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30">
			<BR>
			<BR>
			<fmtDHRPrintProductTravelerG:message key="BTN_PRINT" var="varPrint"/>
			<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnPrint();" />&nbsp;&nbsp;
			<fmtDHRPrintProductTravelerG:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" buttonType="Load" onClick="window.close();" />&nbsp;
			</td>
		</tr>
	</table>
	</div>
	<%} %>
</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
