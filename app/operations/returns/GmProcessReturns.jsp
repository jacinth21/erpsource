 <%
/**********************************************************************************
 * File		 		: GmProcessReturns.jsp
 * Desc		 		: This screen is used for the Territory Maintenance
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations" %>
<%@ include file="/common/GmHeader.inc" %>
<%@page import="java.util.Date"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>

<%@ taglib prefix="fmtProcessReturns" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\returns\GmProcessReturns.jsp -->

<fmtProcessReturns:setLocale value="<%=strLocale%>"/>
<fmtProcessReturns:setBundle basename="properties.labels.operations.returns.GmProcessReturns"/>

<%
	String strReturnsPath = GmCommonClass.getString("GMRETURNS");	
	String strApplDateFmt = strGCompDateFmt;
	
	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");//
	int defaultSetCount = request.getParameter("hRowCnt") == null ? 5 : Integer.parseInt((String)request.getParameter("hRowCnt"));
	defaultSetCount = defaultSetCount < 5 ? 5 : defaultSetCount;
	
	HashMap hmLoad = (HashMap)request.getAttribute("hmLoad");
	// log.debug(" Value inside hmLoad is  " + hmLoad);
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	
	if (strhAction == null)
	{
		strhAction = (String)request.getParameter("hAction");
	}
	strhAction = (strhAction == null)?"Load":strhAction;

	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	
//	String strDistId = "";
//	String strType = (String)session.getAttribute("TYPE")==null?"":(String)session.getAttribute("TYPE");//
//	String strReason = (String)session.getAttribute("REASON")==null?"":(String)session.getAttribute("REASON");
	String strAccId = (String)session.getAttribute("ID")==null?"":(String)session.getAttribute("ID");
//	String strInvId = (String)request.getAttribute("hInvId")==null?"":(String)request.getAttribute("hInvId");
//	String strExpDate = (String)session.getAttribute("EDATE")==null?"":(String)session.getAttribute("EDATE");
//	String strComments = (String)session.getAttribute("COMMENTS")==null?"":(String)session.getAttribute("COMMENTS");
//	String strEmpName = GmCommonClass.parseZero((String)session.getAttribute("EMPNAME"));
	
	
	
	

	ArrayList alDistributor = new ArrayList();
	ArrayList alReturnType = new ArrayList();
	ArrayList alReturnReasons = new ArrayList();
	ArrayList alAccounts = new ArrayList();
	ArrayList alSets = new ArrayList();
	ArrayList alEmpName = new ArrayList();
	ArrayList alRepList = new ArrayList();
	ArrayList alAccountList = new ArrayList();

	if (hmLoad != null)
	{
		//log.debug(" Value of alDistributor Before getting the value is is " + alDistributor.size());
		//log.debug(" HashValue of alDistributor Before getting the value is " + alDistributor.hashCode());
		alDistributor = (ArrayList)hmLoad.get("DISTLIST");
		//log.debug(" Value of alDistributor is " + alDistributor.size());
		//log.debug(" HashValue of alDistributor is " + alDistributor.hashCode());
		HashMap hmTemp = new HashMap();
		hmTemp.put("NAME","Globus Med-In House");
    	hmTemp.put("ID","01");
    	alDistributor.add(0,hmTemp);
		alReturnType = (ArrayList)hmLoad.get("RETTYPES");
		alReturnReasons = (ArrayList)hmLoad.get("RETREASONS");
		alRepList = (ArrayList)hmLoad.get("ALREPLIST"); 
		alSets = (ArrayList)hmLoad.get("SETLIST");
		alEmpName = (ArrayList)hmLoad.get("EMPNAME");
		alAccountList = GmCommonClass.parseNullArrayList((ArrayList)hmLoad.get("ALACCOUNTLIST"));
	//	log.debug(" HashValue of alDistributor after adding element  is " + alDistributor.hashCode());
	}


	int intSize = 0;
	HashMap hcboVal = null;
	HashMap hcboDistVal = null;
	HashMap hcboEmpVal = null;
	
	// This section for Item Consignment
 //	String strPartNums = (String)session.getAttribute("strPartNums") == null?"":(String)session.getAttribute("strPartNums");
	String strNewPartNums = "";
	String strPartDesc = "";
	HashMap hmTemp = new HashMap();
	HashMap hmParam =  new HashMap();
	String strItemQty = "";
	String strRAID = "";
	String strMessage ="";
	String strOrderTypevalue = "";
	HashMap hmRADetails = new HashMap();
	hmRADetails = (HashMap)request.getAttribute("RADETAILS");
	if(hmRADetails != null)
	{
	strRAID = GmCommonClass.parseNull((String)hmRADetails.get("RAID"));
	strMessage = GmCommonClass.parseNull((String)hmRADetails.get("MESSAGE"));
	}
	
	String strType = GmCommonClass.parseNull((String)request.getParameter("Cbo_Type"));
	String strReason = GmCommonClass.parseNull((String)request.getParameter("Cbo_Reason"));
	String strDistID = GmCommonClass.parseNull((String)request.getParameter("Cbo_DistId"));
	String strAccountID = GmCommonClass.parseNull((String)request.getParameter("Cbo_accountId"));
	
	String strEmpName = GmCommonClass.parseNull((String)request.getParameter("cbo_EmpName"));
	String strPartNums = GmCommonClass.parseNull((String)request.getParameter("Txt_PartNums"));
	String strExpDate = GmCommonClass.parseNull((String)request.getParameter("Txt_ExpDate"));
	Date dtExpDate = GmCommonClass.getStringToDate(strExpDate,strApplDateFmt);//Converting to date format
	dtExpDate =(dtExpDate == null)?GmCommonClass.getCurrentDate(strApplDateFmt,strGCompTimeZone):dtExpDate;
	String strSetNums = GmCommonClass.parseNull((String)request.getParameter("hSetNums"));
	String strSetQty = GmCommonClass.parseNull((String)request.getParameter("hSetQty"));
	
	String strEdate = GmCommonClass.parseNull((String)request.getParameter("Txt_ExpDate"));
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strTodayDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
	strEdate = strEdate.equals("") ? strTodayDate: strEdate;//Need to show todays date by default
//    strEdate = strEdate.equals("") ? GmCalenderOperations.addDays(0) : strEdate;
    
    strReason = strReason.equals("") ? "3313" : strReason;
	strOrderTypevalue = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strType,"SHOW-ICT-RETRUNS")); 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Process Returns </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Return.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
 var partNos='';
 var lblReason = '<fmtProcessReturns:message key="LBL_REASON"/>';
 var OrderTypevalue = '<%=strOrderTypevalue%>';
function fnValidateInitiateInput()
{	
	 partNos='';
	 var len =   document.frmTerritory.length.value;
	 var setNoLen =   document.frmTerritory.hRowCnt.value;
     var setNumTxtFeild;
     var setNum;
     var setCnt = 0;
     var type = document.frmTerritory.Cbo_Type.value;
     
	 for(var count = 0; count < setNoLen ; count++)
	 {
		setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+count);
		if(setNumTxtFeild.value != '')
		{
			setCnt++;
			setNum = setNumTxtFeild.value;
		}
	 }
	 if(setCnt == 1)
	  {
	     document.frmTerritory.hSetNo.value = setNum;	    
	  }
	 var objDist = document.frmTerritory.Cbo_DistId;
	 var objAcct = document.frmTerritory.Cbo_accountId;
	 var pendingQty;
     var rtnQty;
     var partNo;
	 var check;
	 if(objDist.value != "01")

	 // Removed code that validated excess returns from Inhouse. This is not needed as IH posting is done to a new acct PD Expense.
	// PD Expense is like scrap acct.
	 {
		 for(var count = 0; count < len ; count++)
		  {
		    pendingQty = eval("document.frmTerritory.hPendingQty"+count);
            rtnQty = eval("document.frmTerritory.Txt_RetQty"+count);
            partNo = eval("document.frmTerritory.hPartNo"+count);
            check = eval("document.frmTerritory.rad"+count)
            if(parseInt(rtnQty.value) > parseInt(pendingQty.value) && check.checked == true)
             {              
               partNos =  partNos+partNo.value+", ";
             }
          }   	   
	 }
	 if((objDist.value == '0'&& type != '3304') || (objAcct.value == '0'&& type == '3304'))
	 {//3304: Account consignment
		 Error_Details(message_operations[527]);
	 }else if (objDist.value != '<%=strDistID%>' || objAcct.value != '<%=strAccountID%>'){
		 Error_Details(message_operations[526]);
	 }
	 
	fnValidateDropDn('Cbo_Reason',lblReason);

	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	return true;	
}
function fnLoad()
{
	document.frmTerritory.hRowCnt.value = '<%=defaultSetCount%>';
	if (document.frmTerritory.hAction.value == 'PopOrd')
	{
		fnLoadList('<%=strType%>');
	}
	document.frmTerritory.Txt_ExpDate.value = '<%=strEdate%>';
	fnCallEmp();
	document.frmTerritory.cbo_EmpName.value='<%=strEmpName%>';
	fnParseFields();
	fnChangeDisplay();	
}

function fnViewPartActual(val)
{	
	document.frmTerritory.Cbo_DistId.value = '<%=strDistID%>';	
	if(document.frmTerritory.Cbo_DistId.value != '01') {
			windowOpener("/GmSalesConsignSearchServlet?hAction=Reload&hOpt=BYPARTDETAIL&Cbo_DistId='<%=strDistID%>'&hPartID="+val,"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
	}
}

function fnOpenPart()
{
	var varAccId = '<%=strAccId%>';
	var varPartNum = document.frmTerritory.Txt_PartNums.value;
	var varCount=0;
	windowOpener("/GmSearchServlet?hAction=SearchPart&Txt_PartNum="+encodeURIComponent(varPartNum)+"&hCount="+varCount+"&Txt_AccId="+varAccId,"Search","resizable=yes,scrollbars=yes,top=340,left=420,width=600,height=435");
}
function fnParseFields()
{
 var setNums = '<%=strSetNums%>';
 var setQty = '<%=strSetQty%>';
 if(setNums != '')
 {
 	var arrSetNums = setNums.split(",");
 	var arrSetQty = setQty.split(",");
	var setNumTxtFeild;
	var setQtyTxtFeild;
	
 	for(var i=0; i< arrSetNums.length -1;i++)
  	{
	    setNumTxtFeild = eval("document.frmTerritory.Txt_SetID"+i);
	    setQtyTxtFeild = eval("document.frmTerritory.Txt_Qty"+i);
	    setNumTxtFeild.value = arrSetNums[i];
		setQtyTxtFeild.value = arrSetQty[i];
		////alert(setNumTxtFeild.value + ", " + setQtyTxtFeild.value);
  	} 
  }
} 

 	 var EmpLen = <%=alEmpName.size()%>;
	<%
		 hcboEmpVal = new HashMap();
		for (int i=0;i<alEmpName.size();i++)
		{
			hcboEmpVal = (HashMap)alEmpName.get(i);
%>
	var EmpArr<%=i%> ="<%=hcboEmpVal.get("ID")%>,<%=hcboEmpVal.get("NAME")%>";
<%
	}
%>

 var RepLen = <%=alRepList.size()%>;
<%
	hcboVal = new HashMap();
	for (int i=0;i<alRepList.size();i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("PARTYID")%>,<%=hcboVal.get("DID")%>";
<%
	}
%>

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();">
<FORM name="frmTerritory" method="POST" action="<%=strServletPath%>/GmProcessConsRtnServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hDelPartNum" value="">
<input type="hidden" name="hId" value="<%=strAccId%>">
<input type="hidden" name="hRowCnt">
<input type="hidden" name="hSetNums">
<input type="hidden" name="hSetQty">
<input type="hidden" name="hPartID">
<input type="hidden" name="hPartQty">
<input type="hidden" name="hOpt">
<input type="hidden" name="hSetNo" value="">
<input type="hidden" name="hDeptID" value="<%=strDeptId%>">

	<table border="0" class="DTTable700" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td width="100%" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td class="Line" colspan="4"></td></tr>
					<tr>
						<td colspan="4" height="25" class="RightDashBoardHeader">&nbsp;<fmtProcessReturns:message key="LBL_PROCESS_RETURNS"/></td>
					</tr>
					<tr><td class="Line" colspan="4"></td></tr>
<%
				if(!strRAID.equals(""))
				 {
%>					
					<tr>
						<td  align="center" colspan="2"><%=strMessage%></td>
					</td>
					</tr>					
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><fmtProcessReturns:message key="LBL_RA_ID"/>:</td>
					    <td><%=strRAID%></td>
					</td>
					</tr>
<%
				 }
%>					
					<tr>
						<td class="RightText" align="right" HEIGHT="24" width="23%"><font color="red">*</font>&nbsp;<fmtProcessReturns:message key="LBL_TYPE_OF_RETURN"/>:</td>
						<td>&nbsp;<select name="Cbo_Type" id="Region" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onChange="fnChangeDisplay()" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtProcessReturns:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alReturnType.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize-2;i++)
			  		{
			  			hcboVal = (HashMap)alReturnType.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strType.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>

						</select></td>
					</tr>
					<tr class="shade">
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtProcessReturns:message key="LBL_REASON"/>:</td>
						<td>&nbsp;<select name="Cbo_Reason" id="Region" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtProcessReturns:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alReturnReasons.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alReturnReasons.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strReason.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
	     		</select></td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtProcessReturns:message key="LBL_CONSIGNEE_NAME"/>:</td>
						<td class="RightText">
						
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
						<tr><td id="consignee_blk" style="display: block" height="24">
						&nbsp;<select name = "Cbo_DistId" class = "RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onChange="fnCallEmp()">
							<option value="0" ><fmtProcessReturns:message key="LBL_CHOOSE_ONE"/>
						
						<%
						  		intSize = alDistributor.size();
						  		//log.debug(" Size val is " + intSize);
								hcboDistVal = new HashMap();
						  		for (int i=0;i<intSize;i++)
						  			{
						  			hcboDistVal = (HashMap)alDistributor.get(i);
						  			// log.debug(" Value of hcboDistVal is  " +hcboDistVal);
						  			strCodeID = (String)hcboDistVal.get("ID");
						  			String strCodeName = (String)hcboDistVal.get("NAME");
									strSelected = strDistID.equals(strCodeID)?"selected":"";
									// log.debug (" Val of Acc Id  is " + strAccId + " Val of Code id is  " + strCodeID + " Code Name is " + strCodeName);
							%>
										<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeName%></option>
							<%
								  		}
								%>
								</select>
							</td>
							<td id="account_blk"  style="display: none" height="24">
							&nbsp;<select name = "Cbo_accountId" class = "RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" style="width: 400px">
							<option value="0" ><fmtProcessReturns:message key="LBL_CHOOSE_ONE"/>
						
							<%
						  		intSize = alAccountList.size();
								hcboDistVal = new HashMap();
						  		for (int i=0;i<intSize;i++)
						  			{
						  			hcboDistVal = (HashMap)alAccountList.get(i);
						  			strCodeID = (String)hcboDistVal.get("ID");
						  			String strCodeName = (String)hcboDistVal.get("NAME");
									strSelected = strAccountID.equals(strCodeID)?"selected":"";
							%>
										<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeName%></option>
							<%
								  		}
							%>
							</select>
							</td>
							<tr> <td>
						<font color="red">*</font>&nbsp;<fmtProcessReturns:message key="LBL_EMP/SALES_REP"/>:&nbsp;
			
				
					&nbsp;<select name="cbo_EmpName" class="RightText" disabled ><option value="0" ><fmtProcessReturns:message key="LBL_CHOOSE_ONE"/></select>
					</td> </tr></tr>
					</table>
					</td>			
							</td> 
					</tr>

					<tr class="shade">
						<td class="RightText" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtProcessReturns:message key="LBL_PART_NUMBERS"/>:</td>
						<td >&nbsp;<input type="text" size="40" value="<%=strPartNums%>" name="Txt_PartNums" class="InputArea">
						&nbsp;&nbsp;
						<fmtProcessReturns:message key="LBL_PART_LOOKUP" var="varPartLookup"/>
						<gmjsp:button gmClass="button" name="Btn_PartLookUp" value="${varPartLookup}"  accesskey="P" tabindex="-1" onClick="fnOpenPart();" buttonType="Load" buttonTag="True"/>
						</td>
					</tr>

					
					<tr class="Shade">
						<td class="RightText" valign="middle" align="right" HEIGHT="24"><fmtProcessReturns:message key="LBL_EXP_DATE_RETURN"/>:</td>
						<td>&nbsp;<gmjsp:calendar textControlName="Txt_ExpDate" 	textValue="<%=(dtExpDate==null)?null:new java.sql.Date(dtExpDate.getTime())%>" 
	 					gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
						</td>
					</tr>
					<tr><td class="Line"  colspan="2" height="1"></td></tr>
                    <tr>
                     <td align = "center" colspan="2">                      
                       <table width="100%">
                         <tr>
                           <td width="60%">
                      <DIV style="display:visible;height: 100px; overflow: auto;">
	                     <TABLE cellpadding="1" cellspacing="1" style="border=1;color=black" align ="center" id="PartnPricingHeader">
							<TR bgcolor="#EEEEEE" class="RightTableCaption">
								<TD  class="RightText"  align="center"><fmtProcessReturns:message key="LBL_SET_NUMBER"/></TD>
								<TD  class="RightText"  align="center"><fmtProcessReturns:message key="LBL_SET_QTY"/></TD>
							</TR>						
						</TABLE>
					    
						
							<TABLE width="40%" cellpadding="1" align="center" cellspacing="0" border="0" id="PartnPricing">							
								<tbody align="center">	
								
<%
								
							for(int count =0;count < defaultSetCount; count++)
							    {
							    String txtSetID = "Txt_SetID" + count;
							    String txtSetQty = "Txt_Qty" + count;
%>															
								     <tr>								     
										<td><input type="text" class=InputArea size="7" name=<%=txtSetID%>></td>
										<td><input type="text" class=InputArea size="5" name="<%=txtSetQty%>"></td>
									</tr>
<%
    							}
%>									
								</tbody>
							 </TABLE>					     
					  </DIV>
					  </td>	
					     <td>
						<TABLE width="40%" cellpadding="1" cellspacing="1" border="0" align="center">
							<tr>
								<td align="center">	<fmtProcessReturns:message key="BTN_ADD_ROW" var="varAdd_Row"/>					
									<gmjsp:button value="${varAdd_Row}" gmClass="button" onClick="fnAddRow('PartnPricing');" name="Btn_AddRow" buttonType="Load" />&nbsp;&nbsp;												
								</td>						
							</tr>
							<tr>
								<td align="center"><fmtProcessReturns:message key="BTN_SET_LOOKUP" var="varSetLookup"/>							
									<gmjsp:button value="${varSetLookup}" gmClass="button" onClick="fnSetLookup();" name="Btn_SetLookUp" buttonType="Load" />&nbsp;&nbsp;												
								</td>						
							</tr>
						</TABLE>
						</td></tr></table>
						
                      </td>                      
                    </tr>
                    
                    <tr><td class="Line" colspan="2" height="1"></td></tr>
                    <tr><td  colspan="2" align="center">
                    <fmtProcessReturns:message key="BTN_LOAD" var="varLoad"/>
	                    <gmjsp:button value="${varLoad}" gmClass="button" onClick="fnReload();" buttonType="Load"  />
					 </td></tr>
<%
					 if(strhAction.equals("Reload") || strhAction.equals("Initiate"))
					  {
%>
					 	<tr>
					<td colspan="2"> 
					<TABLE cellpadding="1" cellspacing="1" border="0" align="center">
					<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					 <tr><td>
					<DIV style="display:visible;height: 200px; overflow: auto;">
						<jsp:include page= "/operations/returns/GmIncludePartReturn.jsp" />
					</DIV>
					</td></tr>
					</table>
					</td>
					</tr>
<%
					}
%>					
		
					 <tr>
         <td colspan="2"> 
				<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="LogType" value="" />
				<jsp:param name="LogMode" value="Edit" />
				</jsp:include>
			</td>      		
		</tr>	
					<tr>
						<td colspan="2">

						</td>
					</tr>	
					<% 
					String strPrint = "fnPrint('"+strRAID+"')";
					%>
		      	   <tr>
						<td colspan="2" align="Center" height="30">&nbsp;
											
<%				
			if(strRAID.equals("") && strhAction.equals("Reload"))
			  {
%>		
								<fmtProcessReturns:message key="BTN_INITIATE" var="varInitiate"/>
								<gmjsp:button value="${varInitiate}" gmClass="button" onClick="fnInitiate();" buttonType="Save"/>&nbsp;
				
<%
			  } else if(!strRAID.equals("")) {			  	
%>					
                      <fmtProcessReturns:message key="BTN_PRINT" var="varPrint"/>
                      <gmjsp:button value="&nbsp;${varPrint}&nbsp;" gmClass="button" onClick="<%=strPrint%>" buttonType="Load" />&nbsp;&nbsp;

<%
		}
%>              
						</td>
					</tr>        
				</table>
			</td>
			<td bgcolor="#666666" width="1"> <img src="<%=strImagePath%>/spacer.gif" width="1"> </td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

<div style="visibility:hidden" id="Dist">
	&nbsp;<select name="Cbo_Id" id="Region" class="RightText" onFocus="changeBgColor(this,'#AACCE8');"  onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtProcessReturns:message key="LBL_CHOOSE_ONE" />
<%
		intSize = alDistributor.size();
		hcboVal = new HashMap();
		for (int i=0;i<intSize;i++)
		{
			hcboVal = (HashMap)alDistributor.get(i);
			strCodeID = (String)hcboVal.get("ID");
			strSelected = strAccId.equals(strCodeID)?"selected":"";
%>
			<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
  		}
%>
	</select>
</div>

<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
