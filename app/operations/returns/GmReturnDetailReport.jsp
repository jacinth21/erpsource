 <%
/**********************************************************************************
 * File		 		: GmReturnDetailReport.jsp
 * Desc		 		: This screen is used for the Returns report
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtReturnDetailReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\returns\GmReturnDetailReport.jsp -->

<fmtReturnDetailReport:setLocale value="<%=strLocale%>"/>
<fmtReturnDetailReport:setBundle basename="properties.labels.operations.returns.GmReturnDetailReport"/>

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page buffer="16kb" autoFlush="true" %>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strApplCurrFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplCurrFmt"));
	String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
	String strCurrFmt = "{0,number,"+strCurrSign+"}";
	
	HashMap hmParam = (HashMap)request.getAttribute("HMPARAM");
	String strhAction = (String)request.getAttribute("hAction");
	String strRAID = GmCommonClass.parseNull(request.getParameter("Txt_RAID"));
    String strType = GmCommonClass.parseZero(request.getParameter("Cbo_Type"));
    String strProdFamily = GmCommonClass.parseZero(request.getParameter("Cbo_ProdFamily"));
    String strProjectId = GmCommonClass.parseNull(request.getParameter("Cbo_Project"));
    String strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));

    ArrayList alType = new ArrayList();
    ArrayList alProdFamily = new ArrayList();
    ArrayList alProject = new ArrayList();	
    
    alType = (ArrayList)request.getAttribute("ALTYPE");
    alProdFamily = (ArrayList)request.getAttribute("ALPRODFAMILY");
    alProject = (ArrayList)request.getAttribute("ALPROJECT");
    
    if (hmParam != null)
    {
    	strRAID = GmCommonClass.parseZero((String)hmParam.get("RAID"));
        strType = GmCommonClass.parseZero((String)hmParam.get("TYPE"));
        strProdFamily = GmCommonClass.parseZero((String)hmParam.get("PRODFAMILY"));
        strProjectId = GmCommonClass.parseNull((String)hmParam.get("PROJECTID"));
        strPartNum = GmCommonClass.parseNull((String)hmParam.get("PARTNUM"));
    }
    
    
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Returns Details List </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style> 
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnCallEditReturn(val)
{
	windowOpener("/GmReturnSetBuildServlet?hAction=EditLoadDash&hRAId="+val,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=730,height=700");
}

function fnViewReturns(val)
{
	windowOpener('<%=strServletPath%>/GmPrintCreditServlet?hAction=VIEW&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}

function fnGo()
{
	document.frmReturnDetails.hAction.value = "Report";
	fnStartProgress('Y');
	document.frmReturnDetails.submit();
}


</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmReturnDetails" method="POST" action="<%=strServletPath%>/GmReturnDetailsServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hRAId" value="<%=strRAID%>">

	<table border="0"  class="DtTable850" cellspacing="0" cellpadding="0">
	<tr><td bgcolor="#666666" height="1" colspan="6"></td></tr>
		<tr>
				<td colspan="6" height="25" class="RightDashBoardHeader"> <fmtReturnDetailReport:message key="LBL_RETURNS_DETAIL_LIST"/> <%=strRAID%> </td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="6"></td></tr>
		<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
		<tr>
			<td colspan="6" height="1">
				<jsp:include page="/operations/returns/GmIncludeReturnHeader.jsp" >
				<jsp:param name="hMode" value="View" />
				</jsp:include>
			</td>
		</tr>	
			
		<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="25" class="RightTableCaption"><fmtReturnDetailReport:message key="LBL_TYPE"/>:</td>
			<td >&nbsp;<gmjsp:dropdown controlName="Cbo_StatusType"  seletedValue="<%=strType%>" 	
							 value="<%=alType%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[All]" />
			</td>
				<td height="25" class="RightTableCaption" ><fmtReturnDetailReport:message key="LBL_PRODUCT_FAMILY"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_ProdFamily"  seletedValue="<%=strProdFamily%>" 	
					    value="<%=alProdFamily%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[All]"/>
			</td>
			</td>
				<td height="25" class="RightTableCaption" ><fmtReturnDetailReport:message key="LBL_PROJECT"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Project"  seletedValue="<%=strProjectId%>" 	
					   value="<%= alProject%>" codeId = "ID" codeName = "NAME"  defaultValue= "[All]"/>
			</td>
		</tr>
		<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td height="25" class="RightTableCaption" ><fmtReturnDetailReport:message key="LBL_PART_#"/>:</td>
			<td colspan="4">&nbsp;<input type = "text" name="Txt_PartNum" value="<%=strPartNum%>" size = "20"></td>
			<td><fmtReturnDetailReport:message key="BTN_GO" var="varGo"/>
			<gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnGo();" buttonType="Load" /> </td>
		</tr>
		<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td colspan="6" height="25" class="RightDashBoardHeader"><fmtReturnDetailReport:message key="LBL_DETAILS_LIST"/>  <%=strRAID%> </td>
		</tr>
<tr>
	<td colspan="6">
	<DIV style="display:visible;height: 200px; overflow: auto;">
			<display:table name="requestScope.rsHeaderResultSet.rows" requestURI="/GmReturnDetailsServlet" class="its" id="currentRowObject" export="true" varTotals="returnTotal" >
				<display:setProperty name="export.excel.filename" value="LogReturnRePort.xls" /> 
				<fmtReturnDetailReport:message key="DT_PART" var="varPart"/>
				<display:column property="PNUM" title="${varPart}" sortable="true" style="text-align: left;" />
				<fmtReturnDetailReport:message key="DT_PART_DESCRIPTION" var="varPartDescription"/>
				<display:column property="PDESC" title="${varPartDescription}" sortable="true" style="text-align: left;width:280px" />
				<fmtReturnDetailReport:message key="DT_QTY_INIT" var="varQtyInit"/>
				<display:column property="QTY_PCD" title="${varQtyInit}" sortable="true" style="text-align: right;" total="true"  />
				<fmtReturnDetailReport:message key="DT_QTY_RETURN" var="varQtyReturn"/>
				<display:column property="QTY_RTN" title="${varQtyReturn}" sortable="true" style="text-align: right;" total="true"  />
				<fmtReturnDetailReport:message key="DT_QTY_PND_CREDIT" var="varQtyCredit"/>
				<display:column property="QTY_PCRD" title="${varQtyCredit}"  sortable="true" style="text-align: right;" total="true"  />
			  	<fmtReturnDetailReport:message key="DT_QTY_WRITTEN_OFF" var="varWrittenOff"/>
			  	<display:column property="QTY_WRF" title="${varWrittenOff}" sortable="true" style="text-align: right;" total="true" />
			  	<fmtReturnDetailReport:message key="DT_QTY_PND_RETURN" var="varQtypReturn"/>
			  	<display:column property="QTY_PRTN" title="${varQtypReturn}" sortable="true" style="text-align: right;" total="true" />
			  	<fmtReturnDetailReport:message key="DT_QTY_EXCESS" var="varQtyExcess"/>
			  	<display:column property="EXCESS" title="${varQtyExcess}" sortable="true" style="text-align: right;" total="true" />
			  	<fmtReturnDetailReport:message key="DT_PRICE" var="varPrice"/>
			  	<display:column property="EPRICE" title="${varPrice}" sortable="true" style="text-align: right; width:70px" format="<%=strCurrFmt%>" total="true" />
			  	<fmtReturnDetailReport:message key="DT_PEND_RETURN_VAL" var="varRetValue"/>
			  	<display:column property="PENDRETVAL" title="${varRetValue}" sortable="true" style="text-align: right;" format="<%=strCurrFmt%>" total="true" />
			  	
			  		<display:footer media="html"> 
						<% 
							HashMap temp = ((HashMap)pageContext.getAttribute("returnTotal"));
								String strVal ;
						  		String strQtyInit = GmCommonClass.getStringWithCommas(((HashMap)pageContext.getAttribute("returnTotal")).get("column3").toString(),0);
						  		String strQtyReturned = GmCommonClass.getStringWithCommas(((HashMap)pageContext.getAttribute("returnTotal")).get("column4").toString(),0);
						  		String strQtyPndCredit = GmCommonClass.getStringWithCommas(((HashMap)pageContext.getAttribute("returnTotal")).get("column5").toString(),0);
						  		String strQtyWrittenOff = GmCommonClass.getStringWithCommas(((HashMap)pageContext.getAttribute("returnTotal")).get("column6").toString(),0);
						  		String strQtyPndReturn = GmCommonClass.getStringWithCommas(((HashMap)pageContext.getAttribute("returnTotal")).get("column7").toString(),0);
						  		String strExcessQty = GmCommonClass.getStringWithCommas(((HashMap)pageContext.getAttribute("returnTotal")).get("column8").toString(),0);
						  		strVal = ((HashMap)pageContext.getAttribute("returnTotal")).get("column9").toString();
						  		String strPrice = strCurrSign + GmCommonClass.getStringWithCommas(strVal);
						  		strVal = ((HashMap)pageContext.getAttribute("returnTotal")).get("column10").toString();
						  		String strPndRtnValue = strCurrSign + GmCommonClass.getStringWithCommas(strVal);
						%>
						
						  	<tr class = shade>
								  		<td colspan="2"> <B> <fmtReturnDetailReport:message key="LBL_TOTAL" />  </B></td>
								  		<td class = "alignright" ><B><%=strQtyInit %></B></td>
								  		<td class = "alignright" ><B><%=strQtyReturned %></B></td>
								  		<td class = "alignright" ><B><%=strQtyPndCredit %></B></td>
								  		<td class = "alignright" ><B><%=strQtyWrittenOff %></B></td>
								  		<td class = "alignright" ><B><%=strQtyPndReturn %></B></td>
								  		<td class = "alignright" ><B><%=strExcessQty %></B></td>
								  		<td class = "alignright" ><B><%=strPrice %></B></td>
								  		<td class = "alignright" ><B><%=strPndRtnValue %></B></td>
							</tr>
					 </display:footer>
			</display:table> 
			</DIV>
		</td>
	</tr>	
			<tr><td colspan="6" height="1" bgcolor="#cccccc"></td></tr>	
			<tr>
				<td colspan="6" height="25" class="RightDashBoardHeader"><fmtReturnDetailReport:message key="LBL_SUPPORT_CHILD_RA" />  </td>
			</tr>		
			<tr>
				<td colspan="6">
					<DIV style="display:visible;height: 50px; overflow: auto;">
					<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
						<jsp:include page="/operations/returns/GmIncludeReturnFooter.jsp" />
						
					</DIV>
				</td>
			</tr>
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
