<%
/**********************************************************************************
 * File		 		: GmReturnSummaryList.jsp
 * Desc		 		: This screen is used for the Returns report
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@page import="java.util.Date"%>
<%@ page buffer="16kb" autoFlush="true" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<%@ taglib prefix="fmtReturnSummaryList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\returns\GmReturnSummaryList.jsp -->

<fmtReturnSummaryList:setLocale value="<%=strLocale%>"/>
<fmtReturnSummaryList:setBundle basename="properties.labels.operations.returns.GmReturnSummaryList"/>


<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String gridData = (String)request.getAttribute("rsResultSet")==null?"":(String)request.getAttribute("rsResultSet");
	String strApplDateFmt = strGCompDateFmt;
	String strWikiTitle = GmCommonClass.getWikiTitle("RECEIVE_RETURNS");
	String strHtmlPath = GmCommonClass.getString("GMHTML");

	HashMap hmParam = (HashMap)request.getAttribute("HMPARAM");
	String strhAction = (String)request.getAttribute("hAction");
	String strDistId = "0";
    String strType = "0";
    String strStatus = "0";
    String strAcctId = "0";
    String strRAID = "";
    String strRefID ="";
    String strParentRAID = "";
    String strElapse = "";
    String strComparison = "";
    String strReason = GmCommonClass.parseNull((String)request.getParameter("Cbo_Reason"));
    String strDateType = "";
    String strFromDate = "";
    String strToDate = "";
    String strSetID = "";
    String strPartNum = "";
	String strRepID="";
	Date dtFromDate= null;
	Date dtToDate = null;
	String strChangeReason = "";
	String strQAAccess = "";
	String strRRAccess = "";
	String strPrcessFl = "";
    String strPrcessID = "";
    String strRaCount = "";
    ArrayList alDistList = new ArrayList();
    ArrayList alType = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alComparison = new ArrayList();	
    ArrayList alReason	= new ArrayList();
    ArrayList alRepList	= new ArrayList();
    ArrayList alAcctList	= new ArrayList();
    ArrayList alChangeList = new ArrayList();
    
    alDistList = (ArrayList)request.getAttribute("ALDISTLIST");
    alType = (ArrayList)request.getAttribute("ALTYPE");
    alStatus = (ArrayList)request.getAttribute("ALSTATUS");
    alComparison = (ArrayList)request.getAttribute("ALCOMPARISON");
    alReason = (ArrayList)request.getAttribute("ALREASON");
    alRepList = (ArrayList)request.getAttribute("ALREPLIST"); 
    alAcctList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALACCOUNTLIST"));
    alChangeList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALCHANGELIST"));
    strPrcessFl =GmCommonClass.parseNull((String)request.getAttribute("STRPRCESSFL"));
    strPrcessID = GmCommonClass.parseNull((String)request.getAttribute("STRPRCESSID"));
    strRaCount = GmCommonClass.parseNull((String)request.getAttribute("STRRACOUNT"));
    if (hmParam != null)
    {
    	strDistId = GmCommonClass.parseZero((String)hmParam.get("DISTID"));
        strType = GmCommonClass.parseZero((String)hmParam.get("TYPE"));
        strStatus = GmCommonClass.parseZero((String)hmParam.get("STATUS"));
        strRAID = GmCommonClass.parseNull((String)hmParam.get("RAID"));
        strParentRAID = GmCommonClass.parseNull((String)hmParam.get("PARENTRAID"));
        strRefID = GmCommonClass.parseNull((String)hmParam.get("REFID"));
        strElapse = GmCommonClass.parseNull((String)hmParam.get("ELAPSE"));
        strComparison = GmCommonClass.parseNull((String)hmParam.get("ELAPSECOMP"));
        strDateType = GmCommonClass.parseZero((String)hmParam.get("DATETYPE"));
        strFromDate = GmCommonClass.parseNull((String)hmParam.get("FRMDATE"));
        strToDate = GmCommonClass.parseNull((String)hmParam.get("TODATE"));
        dtFromDate = GmCommonClass.getStringToDate(strFromDate,strApplDateFmt);
        dtToDate = GmCommonClass.getStringToDate(strToDate,strApplDateFmt);
        strSetID = GmCommonClass.parseNull((String)hmParam.get("SETID"));
        strPartNum = GmCommonClass.parseNull((String)hmParam.get("PARTNUM"));
        strRepID = GmCommonClass.parseNull((String)hmParam.get("REPID"));
        strAcctId = GmCommonClass.parseNull((String)hmParam.get("ACCOUNTID"));
        strQAAccess = GmCommonClass.parseNull((String)hmParam.get("QAACCESS"));
        strRRAccess = GmCommonClass.parseNull((String)hmParam.get("RRACCESS"));
    }
    // To set the defalut status as - Open, but select the status - All (0) then don't set the status (Open).
   if(gridData.equals("")){
	   strStatus = strStatus.equals("0") ? "50520" : strStatus;
   }
    //From process transaction screen load RA ID with multiple sets and multiple CRA's deault status should be All (0)
    if(!strRaCount.equals("0")){
      strStatus="0"; //[All]
    }
  
   HashMap hmTemp = new HashMap();
   hmTemp.put("CODENM","Globus Med-In House");
   hmTemp.put("CODEID","01");
   alDistList.add(0,hmTemp);
	%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Returns Summary List </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strJsPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style> 
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>


<script language="JavaScript" src="<%=strOperationsJsPath%>/GmReturnSummaryList.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
 var repLen = <%=alRepList.size()%>;
 var stDtTyp= '<%=strDateType%>';
 var stRepID= '<%=strRepID%>';
 var refID= '<%=strRefID%>';
 var format = '<%=strApplDateFmt%>';
 var strPrcessFl = '<%=strPrcessFl%>';
 var objGridData;
	objGridData = '<%=gridData%>';
	
	
<%
	HashMap hcboVal = new HashMap();
	for (int i=0;i<alRepList.size();i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("PARTYID")%>,<%=hcboVal.get("DID")%>";
<%
	}
%>

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnSetValues();fnOnPageLoad();">
<FORM name="frmReturnReport" method="POST" action="<%=strServletPath%>/GmReturnSummaryServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hCancelType" value="">
<input type="hidden" name="hTxnId" value="<%=strRAID%>">
<input type="hidden" name="hRAId" value="<%=strRAID%>">
<input type="hidden" name="hParentRAId">
<input type="hidden" name="hOpt">
<input type="hidden" name="hMode">
<input type="hidden" name="hChgReason" value="<%=strChangeReason%>">

	<table border="0"  class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><fmtReturnSummaryList:message key="LBL_RETURN_SUMMARY"/> </td>
				<td align="right" class=RightDashBoardHeader >
				<fmtReturnSummaryList:message key="IMG_ALT_HELP" var = "varHelp"/>				 	
	<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
 					title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />  
	</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="6"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="25" class="RightTableCaption" align="Right" width="115"><fmtReturnSummaryList:message key="LBL_TYPE"/>:</td>
			<td width="238">&nbsp;<gmjsp:dropdown controlName="Cbo_Type"  seletedValue="<%=strType%>" 	
					    value="<%=alType%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[All]" onChange="fnLoadDrpDwn()"/></td>
			<td class="RightTableCaption" align="Right" nowrap width="13%"><fmtReturnSummaryList:message key="LBL_REASON"/>:</td>
			<td width="238">&nbsp;<gmjsp:dropdown controlName="Cbo_Reason"  seletedValue="<%= strReason %>" 	
						  value="<%= alReason%>" codeId = "CODEID"  codeName = "CODENM" defaultValue="[Choose One]"/></td>
			<td rowspan="12" class="LLine" width="1"></td>
			<td rowspan="12" align="center" width="90"><fmtReturnSummaryList:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnGo();" buttonType="Load" /> </td>
		</tr>		
		<tr><td colspan="4" height="1" class="LLine"></td></tr>	
		<tr  class="Shade">
			<td height="25" class="RightTableCaption" align="Right"><fmtReturnSummaryList:message key="LBL_CONSIGNEE_NAME"/>:</td>
			<td  id="dist_blk" style="display: block">&nbsp;<gmjsp:dropdown controlName="Cbo_DistId"  seletedValue="<%=strDistId%>" 	
							 value="<%=alDistList%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[All]" onChange="fnCallRep()"/></td>
			<td id="acct_blk" style="display: none">&nbsp;<gmjsp:dropdown controlName="Cbo_AcctId"  seletedValue="<%=strAcctId%>" 	
							 value="<%=alAcctList%>" codeId = "ID"  codeName = "NAME"  defaultValue= "[All]" width="334"/></td>
			<td height="25" class="RightTableCaption" align="Right"><fmtReturnSummaryList:message key="LBL_EMP/SALES_REP"/>:</td>
			<td >&nbsp;<select name="Cbo_RepName" class="RightText" style="width: 210px"><option value="0" ><fmtReturnSummaryList:message key="LBL_CHOOSE_ONE"/></select>
			</td>				 
			
		</tr>
		<tr><td colspan="4" height="1" class="LLine"></td></tr>
		<tr >
			<td height="25" class="RightTableCaption" align="Right"><fmtReturnSummaryList:message key="LBL_RA"/>:</td>
			<td >&nbsp;<%if(strPrcessFl.equals("YES") && strRaCount.equals("0")){ %><input type = "text" name="Txt_RAID" value="<%=strPrcessID%>" size = "20"><%}else{%>
			<input type = "text" name="Txt_RAID" value="<%=strRAID%>" size = "20">
			<%}%>
			<%if(strQAAccess.equals("Y") || strRRAccess.equals("Y")){ %>
			&nbsp;<fmtReturnSummaryList:message key="IMG_ALT_NO_PERMISSION" var = "varNoPermission"/><img src=<%=strImagePath%>/icon_sales.gif title="${varNoPermission}">
			&nbsp;<img src=<%=strImagePath%>/consignment.gif title="${varNo_Permission}"></td>
			<%}else{%>
			&nbsp;<a href= javascript:fnCallEditReturn('txt');><fmtReturnSummaryList:message key="IMG_ALT_PROCESS_RA" var = "varProcessRA"/><img src=<%=strImagePath%>/icon_sales.gif title="${varProcessRA}"></a> 
			&nbsp;<a href= javascript:fnReconfigReturn('txt');><fmtReturnSummaryList:message key="IMG_ALT_RECONFIGURE_RA" var = "varReconfigRA"/><img src=<%=strImagePath%>/consignment.gif title="${varReconfigRA}"></a></td>
			<%}%>
			<td height="25" class="RightTableCaption" align="Right" nowrap> <fmtReturnSummaryList:message key="LBL_PARENT_RA"/>:</td>
			<td >&nbsp;<%if(!strRaCount.equals("0")){ %><input type = "text" name="Txt_ParentRAID" value="<%=strPrcessID%>" size = "20"><%}else{%>
			<input type = "text" name="Txt_ParentRAID" value="<%=strParentRAID%>" size = "20">
			<%}%></td>
		</tr>
		<tr><td colspan="4" height="1" class="LLine"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" align="Right" nowrap><fmtReturnSummaryList:message key="LBL_ELAPSED_DAYS"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Comparison"  seletedValue="<%= strComparison %>" 	
						  value="<%= alComparison%>" codeId = "CODEID"  codeName = "CODENM" />
						  &nbsp;<input type = "text" name="Txt_Elapse" value="<%=strElapse%>" size = "10"></td>
			<td class="RightTableCaption" align="Right"><fmtReturnSummaryList:message key="LBL_REF_ID"/>:</td>
			<td>&nbsp;<input type = "text" name="Txt_RefID" value="<%=strRefID%>" size = "20"></td>			  
			
		</tr>
		<tr><td colspan="4" height="1" class="LLine"></td></tr>
			
		<tr>
			<td HEIGHT="50" class="RightTableCaption" align="right"><fmtReturnSummaryList:message key="LBL_DATE_RANGE"/>:</td>
			<td class="RightText">&nbsp;<fmtReturnSummaryList:message key="LBL_TYPE"/>:&nbsp;<select name="Cbo_DateType" class="RightText">
									 <option value='0'><fmtReturnSummaryList:message key="LBL_CHOOSE_ONE"/>
									 <option value='IN'><fmtReturnSummaryList:message key="LBL_INITAIATED_DATE"/>	
									 <option value='RT'><fmtReturnSummaryList:message key="LBL_RETURNED_DATE"/>
									 <option value='CR'><fmtReturnSummaryList:message key="LBL_CREDITED_DATE"/>
									 <option value='RP'><fmtReturnSummaryList:message key="LBL_REPROCESSED_DATE"/></select>&nbsp;<BR><BR>&nbsp;<fmtReturnSummaryList:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar textControlName="Txt_FrmDate" 
	                                 textValue="<%=(dtFromDate==null)?null:new java.sql.Date(dtFromDate.getTime())%>" 
	                                  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>						
					  &nbsp;<fmtReturnSummaryList:message key="LBL_TO"/>:&nbsp;<gmjsp:calendar textControlName="Txt_ToDate" 
	                           textValue="<%=(dtToDate==null)?null:new java.sql.Date(dtToDate.getTime())%>" 
	                           gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td height="25" class="RightTableCaption" align="Right"><fmtReturnSummaryList:message key="LBL_STATUS"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Status"  seletedValue="<%=strStatus%>" 	
					   value="<%= alStatus%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[All]" /> </td>
		</tr>
		<tr><td colspan="4" height="1" class="LLine"></td></tr>	
		<tr class="Shade">
			<td height="25" class="RightTableCaption" align="right"><fmtReturnSummaryList:message key="LBL_SET_ID"/>:</td>
			<td class="RightText">&nbsp;<input type = "text" name="Txt_SetID" value="<%=strSetID%>" size = "20"></td>
			<td class="RightTableCaption" align="right"><fmtReturnSummaryList:message key="LBL_PART_NUMBER"/>:</td>
			<td class="RightText">&nbsp;<input type = "text" name="Txt_PartNum" value="<%=strPartNum%>" size = "20"></td>
		</tr>
		</table>
		 <table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		     	
		<%if(gridData.equals("")){ %>
				<tr><% %></tr>
					<%}
				   else if(!gridData.equals("")){%>
				 
					<tr>
           	<td colspan="4" height="50" >
				<div id="employeedata" style="grid" height="400px"></div>
		    </td>	
		</tr>
				<%if( gridData.indexOf("cell") != -1) {%>
				<tr>
			                <td colspan="6" align="center">
			                <div class='exportlinks'><fmtReturnSummaryList:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp; <a href="#"
			                                onclick="fnDownloadXLS();"> <fmtReturnSummaryList:message key="DIV_EXCEL"/> </a>

			                </div></td>
			    </tr>
			    
			    	<tr>
							<td colspan="6" align="Center"><fmtReturnSummaryList:message key="LBL_CHOOSE_ACTION"/>:
							<gmjsp:dropdown controlName="Cbo_ChangeReason"  seletedValue="<%=strChangeReason%>" 	
							value="<%=alChangeList%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /> &nbsp;&nbsp;
							<fmtReturnSummaryList:message key="LBL_SUBMIT" var="varSubmit"/>
							<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="Button" onClick="fnLoadRAReport();"  buttonType="Save" /></td>
					</tr>
				
					<tr><td colspan="11" height="1" bgcolor="#CCCCCC"></td></tr>
			    <%}%>
		<%}%>
		</table>
			
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
