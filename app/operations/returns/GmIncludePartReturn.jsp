
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ page import="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@include file="/common/GmCommonInclude.jsp" %>

<%@ taglib prefix="fmtIncludePartReturn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\returns\GmIncludePartReturn.jsp -->

<fmtIncludePartReturn:setLocale value="<%=strLocale%>"/>
<fmtIncludePartReturn:setBundle basename="properties.labels.operations.returns.GmIncludePartReturn"/>



<%
	RowSetDynaClass dynaClass = (RowSetDynaClass)request.getAttribute("PARTDETAILS");	
%>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<input type="hidden" name="length" value='<%=dynaClass.getRows().size()%>'>  

           <div style='overflow:auto; height =180' /> 
             <display:table name="requestScope.PARTDETAILS.rows" class="its" id="currentRowObject" requestURI="/GmProcessConsRtnServlet" defaultsort="2" varTotals="totals"  decorator="com.globus.displaytag.beans.DTProcessConsReturnWrapper">              
             
             <display:column property="CHKBOX" class="aligncenter" title="<input type=checkbox name=Chk_SelectAll  onClick=fnSelectAll(this);>"  />
			 <fmtIncludePartReturn:message key="DT_PART_#" var="varPart"/>
			 <display:column property="PNUM" title="${varPart}" sortable="true" class="alignleft"/>
			 <fmtIncludePartReturn:message key="DT_PART_DESC" var="varPartDesc"/>
			 <display:column property="PARTDESC" title="${varPartDesc}" class="alignleft"/>			
			 <fmtIncludePartReturn:message key="DT_PENDING_QTY_RETURN" var="varPendingReturn"/>
			 <display:column property="PENDING_QTY" title="${varPendingReturn}"  sortable="true" class="alignright" />
			 <fmtIncludePartReturn:message key="DT_SET_QTY" var="varSetQty"/>
			 <display:column property="SET_QTY" title="${varSetQty}" sortable="true"  class="alignright" />
			<fmtIncludePartReturn:message key="DT_RETURN_QTY" var="varReturnQty"/>
			 <display:column property="RETQTY" title="${varReturnQty}" class="alignright" />
			 </display:table>
			 </div>
