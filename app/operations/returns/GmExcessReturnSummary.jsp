<%
/**********************************************************************************
 * File		 		: GmExcessReturnSummary.jsp
 * Desc		 		: Report for Excess Return Summary
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtExcessReturnSummary" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<!-- GmExcessReturnSummary.jsp -->
<fmtExcessReturnSummary:setLocale value="<%=strLocale%>" />
<fmtExcessReturnSummary:setBundle basename="properties.labels.operations.returns.GmExcessReturnSummary"/>
 
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page buffer="16kb" autoFlush="true" %>


<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	HashMap hmParam = new HashMap();
	
	ArrayList alDistributorList = (ArrayList)request.getAttribute("ALDISTLIST");
	ArrayList alStatus = (ArrayList)request.getAttribute("ALSTATUS");
	log.debug("alDistributorList size is "+alDistributorList.size( ));
	hmParam = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMPARAM"));
	
	log.debug(" values inside hmParam " + hmParam);
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	String strDistributorId = "";
	String strStatus = "0";
	String strRAID = "";
	String strConsignId = "";

    strDistributorId =  GmCommonClass.parseNull((String)hmParam.get("DISTID"));
    strStatus  =  GmCommonClass.parseNull((String)hmParam.get("STATUS"));      
    strRAID = GmCommonClass.parseNull((String)hmParam.get("RAID"));         
    strConsignId = GmCommonClass.parseNull((String)hmParam.get("CONSIGNID"));
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Excess Return Summary </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>     
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script>

function fnGo()
{
	fnStartProgress();
	document.frmExReturnSummary.submit();
}

function fnCallCSGExcess(val) {
	document.frmExReturnSummary.action ="<%=strServletPath%>/GmExcessReturnDetailsServlet";
	document.frmExReturnSummary.hConsignId.value = val;
	document.frmExReturnSummary.submit();
}	

function fnPrintReturnVer(val)
{
	fnPrintRA(val);
}

function fnPrintConsignVer(val)
{
	windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}


function fnLog(type,id)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
function fnCallEditTransfer(val)
{
  	windowOpener("/GmAcceptTransferServlet?hAction=Load&hTransferId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}	
</script>	

<BODY leftmargin="20" topmargin="10" >
<FORM name="frmExReturnSummary" method="POST" action="<%=strServletPath%>/GmExcessReturnSummaryServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hConsignId" value="">

	<table border="0"  class="DtTable850" cellspacing="0" cellpadding="0">
	<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
				<td colspan="4" height="25" class="RightDashBoardHeader"><fmtExcessReturnSummary:message key="LBL_CONSIGNMENT_RECONCILIATION_SUMMARY" /></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td height="25" class="RightTableCaption" align="right"><fmtExcessReturnSummary:message key="LBL_DISTRIBUTOR" /> :</td>
			<td >&nbsp;<gmjsp:dropdown controlName="Cbo_DistId"  seletedValue="<%= strDistributorId %>" 	
					 value="<%= alDistributorList%>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= " All " />
			</td>
				<td height="25" class="RightTableCaption" align="right"><fmtExcessReturnSummary:message key="LBL_STATUS" /> :</td>
			<td>&nbsp;<gmjsp:dropdown controlName="Cbo_Status"  seletedValue="<%= strStatus %>" 	
					  value="<%= alStatus%>" codeId = "CODEID" codeName = "CODENM" />
				&nbsp&nbsp
				<fmtExcessReturnSummary:message key="BTN_GO" var="varGo" />
				<gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnGo();" buttonType="Load" />					  
			</td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td height="25" class="RightTableCaption" align="right" ><fmtExcessReturnSummary:message key="LBL_RAID" /> :</td>
			<td>&nbsp;<input type="text" size="20" value="<%=strRAID%>" name="Txt_RAID" class="InputArea"   onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" > 
			</td>
			<td class="RightTableCaption" align="right" ><fmtExcessReturnSummary:message key="LBL_CONSIGNMENT_ID" /> :</td>
			<td>&nbsp;<input type="text" size="20" value="<%=strConsignId%>" name="Txt_ConsignId" class="InputArea"   onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" > 
			</td>
		</tr>
		
<tr>
	<td colspan="4">
			<display:table name="requestScope.RDEXCESSRETURNSUMMARY.rows"  class="its" id="currentRowObject" export="true" decorator="com.globus.displaytag.beans.DTExsReturnSummaryWrapper" requestURI="/GmExcessReturnSummaryServlet"> 
				<fmtExcessReturnSummary:message key="DT_CONSIGNMENT_ID" var="varConsignmentID"/><display:column property="CONSIGNID" title="${varConsignmentID}"  class="alignleft"/>
				<fmtExcessReturnSummary:message key="DT_DISTRIBUTOR_NAME" var="varDistributorName"/><display:column property="DISTNAME" title="${varDistributorName}" class="alignleft"  />
				<fmtExcessReturnSummary:message key="DT_RA_ID" var="varRAID"/><display:column property="RMAID" title="${varRAID}"  class="alignleft"/>
				<fmtExcessReturnSummary:message key="DT_DATE_RETURNED" var="varDateReturned"/><display:column property="RETURNDATE" title="${varDateReturned}" class="aligncenter"   />
			  	<fmtExcessReturnSummary:message key="DT_CREDITED_BY" var="varCreditedBy"/><display:column property="CREDITEDBY" title="${varCreditedBy}" class="alignleft"  />
			  	<fmtExcessReturnSummary:message key="DT_RA_LOG" var="varRALog"/><display:column property="RLOG" title="${varRALog}" class="aligncenter"  />
			  	<fmtExcessReturnSummary:message key="DT_CONSIGN_LOG" var="varConsignlog"/><display:column property="CLOG" title="${varConsignlog}" class="aligncenter"   />
			  	<%if (strStatus.equals("90176")) {%>
			  	<fmtExcessReturnSummary:message key="DT_REASON" var="varReason"/><display:column property="REASON" title="${varReason}" class="aligncenter"   />
			  	<fmtExcessReturnSummary:message key="DT_TRANSFER_ID" var="varTransferID"/><display:column property="TID" title="${varTransferID}" class="aligncenter"   />
			  	<%} %>
			</display:table> 
		</td>
	</tr>	
			<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	

	</table>
</FORM>	
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>