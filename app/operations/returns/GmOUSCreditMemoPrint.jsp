 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/******************************************************************************************
 * File		 		: GmOUSCreditMemoPrint.jsp
 * Desc		 		: This screen is used for the displaying GOP-OUS Returns Details.
 * Version	 		: 1.0
 * author			: Elango
********************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" autoFlush="true" %>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtOUSCreditMemoPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\returns\GmOUSCreditMemoPrint.jsp -->

<fmtOUSCreditMemoPrint:setLocale value="<%=strLocale%>"/>
<fmtOUSCreditMemoPrint:setBundle basename="properties.labels.operations.returns.GmOUSCreditMemoPrint"/>


<%
try {
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	int pageSize = 29; // Need to be a prime number

	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
	String strApplnDateFmt = strGCompDateFmt;

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strNativeCurrency  = GmCommonClass.parseNull((String)hmReturn.get("COMPCURRENCYSYMBOL"));
	strNativeCurrency = strNativeCurrency.equals("")? "$":strNativeCurrency;
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strHideBtn = GmCommonClass.parseNull((String)request.getAttribute("hideBtn"));

	String strAdd = "";
	String strTemp = "";
	String strShade = "";
	String strRAId = "";
	String strSetName = "";
	String strCreditId = "";
	String strInvId = "";
	String strPO = "";

	String strDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strCreditDate = "";
	java.sql.Date dtRetDate = null;
	String strOrdComments  = "";
	String strShipCost = "";
	String strOrdType = "";
	
	String strType = "";
	String strStatusFl = "";
	String strOrderID = "";
	String strTitle = "";
	String strRAType = "";
	String strRADate = "";
	String strLine = "";
	String strRate = "";
	String strAmount = "";
	String strPrice = "";
	String strTotal = "";
	double dbPageTotal=0.0;
	
	String strStatus="";
	
	double dbAmount = 0.0;
	double dbTotal = 0.0;
	int pageCount = 1;
	
	ArrayList alReturnsItems = new ArrayList();
	ArrayList alChildRAs = new ArrayList();
	HashMap hmReturnDetails = new HashMap();
	
	boolean bolFl = false;
	boolean bFlag = false;

	int intPriceSize = 0;
	//int rowSpan = 6;

	String strCompanyLocale = GmCommonClass.parseNull((String)hmReturn.get("COMPANYLOCALE"));
	String strCurrencyFmt = GmCommonClass.parseNull((String)hmReturn.get("COMPCURRENCYSYMBOL"));
	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), "");
	String strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME"));
	String strAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYADDRESS"));
	if(!strAddress.equals("")){
		strAddress = strAddress.replaceAll("/", "<br>&nbsp;&nbsp;");
	}
	strAddress = "<b>"+ strCompanyName + "</b><br>&nbsp;&nbsp;&nbsp;" + strAddress;
	
	GmResourceBundleBean rbCreditMemoPrint = GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	
	String strCustomerLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.CUSTOMER");
	String strCustomerPOLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.CUSTOMER_PO");
	String strCreditNoteLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.CREDIT_NO");
	String strCreditDateLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.CREDIT_DATE");
	String strInvoiceLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.INVOICE");
	String strRAIDLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.RA_ID");
	String strRADateLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.RA_DATE");
	String strPnumLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.PART_NUMBER");
	String strDescLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.DESCRIPTION");
	String strQtyLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.QTY");
	String strRateLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.RATE_EA");
	String strStatusLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.STATUS");
	String strAmountLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.AMOUNT");
	String strFooterLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.FOOTER");
	String strShippingChargLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.SHIPPING_CHARGES");
	String strTotalLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.TOTAL");
	String strContinuedLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.CONTINUED");
	String strPageTotalLbl = rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.PAGE_TOTAL");


	if (hmReturn != null)
	{
		hmReturnDetails = (HashMap)hmReturn.get("RADETAILS");
		alReturnsItems = (ArrayList)hmReturn.get("RAITEMDETAILS");
		alChildRAs     = (ArrayList)hmReturn.get("CHILDRADETAILS");
		strRAId = GmCommonClass.parseNull((String)hmReturnDetails.get("RAID"));
		strSetName = GmCommonClass.parseNull((String)hmReturnDetails.get("SNAME"));
		strDate = GmCommonClass.getStringFromDate((java.sql.Date)hmReturnDetails.get("UDATE"),strApplnDateFmt);
		strAdd = GmCommonClass.parseNull((String)hmReturnDetails.get("ACCADD"));
		strCreditId = GmCommonClass.parseNull((String)hmReturnDetails.get("CREDITINVID"));
		strPO = GmCommonClass.parseNull((String)hmReturnDetails.get("PO"));
		strCreditDate =GmCommonClass.getStringFromDate((java.sql.Date)hmReturnDetails.get("CREDITDATE"),strApplnDateFmt);
		strRADate = GmCommonClass.getStringFromDate((java.sql.Date)hmReturnDetails.get("RETDATE"),strApplnDateFmt);
		strInvId = GmCommonClass.parseNull((String)hmReturnDetails.get("INVID"));
		strOrdComments = GmCommonClass.parseNull((String)hmReturnDetails.get("ORDER_COMMENTS"));	
		strShipCost	= GmCommonClass.parseZero(((String)hmReturnDetails.get("SCOST")));	
		strOrderID = GmCommonClass.parseNull(((String)hmReturnDetails.get("ORDER_ID")));
		strOrdType = GmCommonClass.parseNull(((String)hmReturnDetails.get("ORDTYPE")));
		strRAType = GmCommonClass.parseNull(((String)hmReturnDetails.get("RETTYPE")));
		strStatus = GmCommonClass.parseNull(((String)hmReturnDetails.get("STATUS")));
		dbTotal = Double.parseDouble(strShipCost);
	//	log.debug(" OrdType is " + strOrdType + " Returns Type is " + strRAType)	;
		if(strRADate.equals(""))
		{
			strRADate = "NA";
		}
	}
	
	if(!strOrderID.equals(""))
	{
	if (strRAType.equals("3300"))
	{
				if (strOrdType.equals("2528"))
				{
					strTitle =(String)rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.TITLE_3300_2528");	
				}
				else
					strTitle = (String)rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.TITLE_3300");//"Sales Adjustment" ;
	}
	else
	  { 
		strTitle = (String)rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.TITLE_ORD_ID");//"Credit Memo" ;
	  }
	}
	
 else if(strStatus.equals("2"))
  {		
  strTitle = (String)rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.TITLE_STAT_2"); //"Credit Summary List";
    strPO = "NA";
    strInvId = "NA";
    strCreditId="NA";
    strCreditDate="NA";
  }
  else
  {
    strTitle = (String)rbCreditMemoPrint.getProperty("CREDIT_SUMMARY.TITLE_DEFAULT");//"Pending Returns Summary List";
    strPO = "NA";
    strInvId = "NA";
    strCreditId="NA";
    strCreditDate="NA";
  }
  		
// log.debug(" strTitle is " + strTitle);
 int intSize = 0;
 HashMap hcboVal = null;
 String strStyle="display: run-in";
 
 StringBuffer sbHtmlCode = new StringBuffer();
							sbHtmlCode.append("<br><br><table border='0' width='700' cellspacing='0' cellpadding='0' align='center'>");
							sbHtmlCode.append("<tr><td bgcolor='#666666' height='1' colspan='3'></td></tr>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='6'></td>");
							sbHtmlCode.append("<td>");
							sbHtmlCode.append("<table cellpadding='0' cellspacing='0' border='0' width='100%'>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td class='RightText' valign='top' width='270'><img src='"+strImagePath);
							sbHtmlCode.append("/100800.gif' width='138' height='60'><br>&nbsp;&nbsp;&nbsp;");
							sbHtmlCode.append(strAddress);
							sbHtmlCode.append("</td>");
							sbHtmlCode.append("<td class='RightText' align='right'>");
							sbHtmlCode.append("	<img src='/GmCommonBarCodeServlet?ID=");//Added Linear barcode
							sbHtmlCode.append(strRAId); 
							sbHtmlCode.append("' height=20 width=170 />&nbsp;Paperwork");
							sbHtmlCode.append("<font size='+2' style='"+strStyle+"'>"+strTitle+"</font>&nbsp;</td>");						
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("</table>");
							sbHtmlCode.append("</td>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='6'></td>");
							sbHtmlCode.append("</tr>");
							
							sbHtmlCode.append("<tr><td bgcolor='#666666'></td></tr>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td width='698' height='100' valign='top'>");
							sbHtmlCode.append("<table border='0' width='100%' cellspacing='0' cellpadding='0'>");
							sbHtmlCode.append("<tr><td bgcolor='#666666' height='1' colspan='8'></td></tr>");
							sbHtmlCode.append("<tr bgcolor='#eeeeee' class='RightTableCaption'>");
							sbHtmlCode.append("<td height='25' align='center' width='350'>&nbsp;"+strCustomerLbl+"</td>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='7'></td>");
							sbHtmlCode.append("<td height='25' align='center'>&nbsp;"+strCustomerPOLbl+"</td>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='7'></td>");
							sbHtmlCode.append("<td width='110' align='center'>&nbsp;"+strCreditNoteLbl+"</td>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='7'></td>");
							sbHtmlCode.append("<td width='110' align='center'>&nbsp;"+strCreditDateLbl+"</td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr><td bgcolor='#666666' height='1' colspan='8'></td></tr>");
							
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td class='RightText' rowspan='7' valign='top'>");
							sbHtmlCode.append("<table>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td width='25'>&nbsp;</td>");
							sbHtmlCode.append("<td width='300' class='RightTableCaption'>&nbsp;"+strAdd);
							sbHtmlCode.append("</td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("</table>&nbsp;<BR>&nbsp;<BR>");
							sbHtmlCode.append("</td>");
							sbHtmlCode.append("<td class='RightText' align='center'>&nbsp;"+strPO+"</td>");
							sbHtmlCode.append("<td height='25' class='RightText' align='center'>&nbsp;"+strCreditId+"</td>");
							sbHtmlCode.append("<td class='RightText' align='center'>&nbsp;"+strCreditDate+"</td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='2'></td>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='2'></td>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='2'></td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr bgcolor='#eeeeee' class='RightTableCaption'>");
							sbHtmlCode.append("<td class='RightText' align='center' height='20'>"+strInvoiceLbl+"</td>");
							sbHtmlCode.append("<td colspan='2' align='center'>"+strRAIDLbl+"</td>");
							sbHtmlCode.append("<td colspan='2' align='center'>"+strRADateLbl+"</td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='2'></td>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='2'></td>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='2'></td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td class='RightText' rowspan='3' align='center'>&nbsp;"+strInvId+"<b></b></td>");
							sbHtmlCode.append("<td align='center' colspan='2' height='25'  class='RightText'>&nbsp;"+strRAId+"</td>");
							sbHtmlCode.append("<td align='center' colspan='2' height='25'  class='RightText'>&nbsp;"+strRADate+"</td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("</table>");
							sbHtmlCode.append("</td>");
							sbHtmlCode.append("</tr>");
													
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td width='700' height='100' valign='top'>");
							sbHtmlCode.append("<table border='0' width='100%' cellspacing='0' cellpadding='0'>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td>");
							sbHtmlCode.append("<table cellspacing='0' cellpadding='0' border='0' width='100%' id='myTable'>");
							sbHtmlCode.append("<tr><td colspan='7' height='1' bgcolor='#666666'></td></tr>");
							sbHtmlCode.append("<tr bgcolor='#eeeeee' class='RightTableCaption'>								");
							sbHtmlCode.append("<td height='25' width='100' align='center'>"+strPnumLbl+"</td>");
							sbHtmlCode.append("<td width='275'>"+strDescLbl+"</td>");
							sbHtmlCode.append("<td width='50' align='center'>"+strQtyLbl+"</td>");
							sbHtmlCode.append("<td align='center'>"+strRateLbl+"</td>");
							sbHtmlCode.append("<td>"+strStatusLbl+"</td>									");
							sbHtmlCode.append("<td align='center'>"+strAmountLbl+"</td>									");							
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr><td colspan='7' height='1' bgcolor='#666666'></td></tr> ");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Consign </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/watermark.js"></script>
<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
var tdinnnerTopPrintBtn = "";
var strObject = "";
var strTopPrintBtnObj = "";

//MNTTASK-8359. Using two print buttons. So use unique id name for button.   
function hidePrint()
{
	strObject = eval('document.all.button');
	strTopPrintBtnObj = eval('document.all.TopPrintButton');
	tdinnner = strObject.innerHTML;
	tdinnnerTopPrintBtn = strTopPrintBtnObj.innerHTML;
	strObject.innerHTML = "";
	strTopPrintBtnObj.innerHTML = "";
}

function showPrint()
{
	strObject = eval('document.all.button');
	strObject.innerHTML = tdinnner ;
	strTopPrintBtnObj = eval('document.all.TopPrintButton');
	strTopPrintBtnObj.innerHTML = tdinnnerTopPrintBtn ;
}
</script>
</HEAD>

<BODY topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmReportCreditsServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strRAId%>">
		<%if(!strHideBtn.equals("Y")){ %>
<table border='0' width='700' cellspacing='0' cellpadding='0' align='center'>
	<tr>
		<td colspan='3' height='30' align='center' id='TopPrintButton'>
		<fmtOUSCreditMemoPrint:message key="BTN_PRINT" var="varPrint"/>
			<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnPrint();" />
&nbsp;
<fmtOUSCreditMemoPrint:message key="BTN_CLOSE" var="varClose"/>
<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close2" gmClass="button" onClick="window.close();" buttonType="Load" />		</td>

	</tr>
</table>
		<%} %>	
<%=sbHtmlCode.toString()%>
	<%
						intSize = alReturnsItems.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();

							double intQty = 0.0;
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alReturnsItems.get(i);

								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strStatusFl = GmCommonClass.parseNull((String)hmLoop.get("SFL"));

								intQty = Double.parseDouble(strQty);
								dbAmount = Double.parseDouble(strPrice);
								dbAmount = intQty * dbAmount; // Multiply by Qty
								strAmount = ""+dbAmount;

								dbTotal = dbTotal + dbAmount;
								strTotal = ""+dbTotal;
								dbPageTotal = dbPageTotal + dbAmount;

								out.print(strLine);
%>
								<tr>									
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center">&nbsp;<%=GmCommonClass.getRedText(strQty)%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice))%>&nbsp;&nbsp;</td>
									<td class="RightText">&nbsp;<%=strStatusFl%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strAmount))%>&nbsp;&nbsp;&nbsp;</td>																		
								</tr>
<%
							if ((i > 0) && (i % pageSize) == 0)
								{
								pageCount++;
%>								
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr>
									<td  height="35"></td>
									<td class="RightTableCaption"><%=strPageTotalLbl%></td>
									<td colspan="4" class="RightTableCaption" align="right"><%=strCurrencyFmt%><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(String.valueOf(dbPageTotal)))%>&nbsp;&nbsp;&nbsp;</td>
								</tr>									
								<tr><td colspan="6" height="20" class="RightText" align="right"><%=strContinuedLbl%></td></tr>		
								<tr><td colspan="6" height="1" bgcolor='#666666'></td></tr>																									
							</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
</table>
<p STYLE="page-break-after: always"></p>								
<%									
								dbPageTotal = 0;%>
<br/>
								<%out.println(sbHtmlCode);																		

							     }		
 if(i > 0 && i % pageSize == 0 && i == (intSize -1))	
  {
   bFlag = true;						     
%>

					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%							     
    }
							}//End of FOR Loop
 if(!bFlag)
  {
   if(pageCount > 1 && intSize == pageSize)
     {							
%>
						<tr>	
						<td  height="35" ></td>
							<td class="RightTableCaption"><%=strPageTotalLbl%></td>						
							<td colspan="4" class="RightTableCaption" align="right"><%=strCurrencyFmt%><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(String.valueOf(dbPageTotal)))%>&nbsp;&nbsp;&nbsp;</td>
						</tr>	
 <%
     }
 %>						
								
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<% } %>
	<table border='0' width='100%' cellspacing='0' cellpadding='0'>
		<tr>
			<td class="RightText" width="90" height="35" align="center">&nbsp;</td>
			<td class="RightText" width="690"><b><%=strShippingChargLbl%></b></td>
			<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strShipCost))%>&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr> 
			<td colspan="4"  class="RightText" align="left">&nbsp;<%=strOrdComments%> </td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td class="RightText" height="35">&nbsp;</td>
			<td class="RightTableCaption"><%=strTotalLbl%></td>
			<td class="RightTableCaption" align="right"><%=strNativeCurrency%>&nbsp;<%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTotal))%>&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
		<%if(!strHideBtn.equals("Y")){ %>
		<tr>
			<td colspan="4" height="30" align="center" id="button">
			<fmtOUSCreditMemoPrint:message key="BTN_PRINT" var="varPrint"/>
			<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />
			&nbsp;<fmtOUSCreditMemoPrint:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />			
			</td>
		</tr>
		<%} %>
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
   </table> 
<%
}else {
%>
						<tr><td colspan="6" height="50" align="center" class="RightTextRed"><BR><%=strFooterLbl%></td></tr>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="4" height="0" bgcolor="#666666"></td></tr>
</table>						
<%
}
			
%>
<% %>
 <tr><td colspan="4" height="0" bgcolor="#666666"></td></tr> 
</FORM>
<% /* Below code to display bookmark value 
	* 2522 maps to duplicate order 
	* If Credited before invoice then display the Water mark
	* so the 
	*/  
//	System.out.println("strPO '" + strPO + "'");
if (strPO.equals("-") && !strOrderID.equals(""))
{
%>

<div id="waterMark" style="position:absolute; z-Index: 0; top: 340; left: 340">
	<img src="<%=strImagePath%>/credit_bef_invoice.jpg"  border=0>
	</div>
<% } else {%>
<div id="waterMark"></div>
<% }%>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
