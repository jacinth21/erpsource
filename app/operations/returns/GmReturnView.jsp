 <%
/**********************************************************************************
 * File		 		: GmReturnView.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.1
 * author			: Dhinakaran James
 * 					  File moved from Customer to return folder and 
 *					  additional columns added (One RA Changes) 	
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtReturnView" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmReturnView.jsp -->
<fmtReturnView:setLocale value="<%=strLocale%>"/>
<fmtReturnView:setBundle basename="properties.labels.operations.returns.GmReturnView"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strApplnDateFmt = strGCompDateFmt;

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	log.debug(" Values inside hmReturn is " + hmReturn);
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");

	String strDesc = "";
	String strRAId = "";
	String strSetName = "";
	String strReason = "";
	String strType = "";
	String strCreatedBy = "";
	String strDistName = "";
	String strAcctName = "";
	String strOrgOrderId = "";
	
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	
	String strStatusFl = "";
	String strSetId = "";
	String strChildRA = "";
	
	String strRetDate = "";
	String strExpDate = "";
	String strCreatedDate = "";
	
	ArrayList alReturnsItems = new ArrayList();
	HashMap hmReturnDetails = new HashMap();
	
	boolean bolFl = false;

	int intPriceSize = 0;
	if (hmReturn != null)
	{
		hmReturnDetails = (HashMap)hmReturn.get("RADETAILS");
		alReturnsItems = (ArrayList)hmReturn.get("RAITEMDETAILS");
	    strSetId =GmCommonClass.parseNull((String) hmReturn.get("SETID"));

		strRAId = GmCommonClass.parseNull((String)hmReturnDetails.get("RAID"));
		strSetName = GmCommonClass.parseNull((String)hmReturnDetails.get("SNAME"));
		strType = GmCommonClass.parseNull((String)hmReturnDetails.get("TYPE"));
		strReason = GmCommonClass.parseNull((String)hmReturnDetails.get("REASON"));
		
		strCreatedBy = GmCommonClass.parseNull((String)hmReturnDetails.get("PER"));
		//strCreatedDate = GmCommonClass.parseNull((String)hmReturnDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmReturnDetails.get("CDATE"),strApplnDateFmt);
		strDesc = GmCommonClass.parseNull((String)hmReturnDetails.get("COMMENTS"));
		strDistName = GmCommonClass.parseNull((String)hmReturnDetails.get("DNAME"));
		strAcctName = GmCommonClass.parseNull((String)hmReturnDetails.get("ANAME"));
		//strRetDate = GmCommonClass.parseNull((String)hmReturnDetails.get("RETDATE"));
		strRetDate = GmCommonClass.getStringFromDate((java.util.Date)hmReturnDetails.get("RETDATE"),strApplnDateFmt);
		//strExpDate = GmCommonClass.parseNull((String)hmReturnDetails.get("EDATE"));
		strExpDate = GmCommonClass.getStringFromDate((java.util.Date)hmReturnDetails.get("EDATE"),strApplnDateFmt);
		strOrgOrderId = GmCommonClass.parseNull((String)hmReturnDetails.get("ORDER_ID"));
		//added child RA for PMT-234
		strChildRA = GmCommonClass.parseNull((String)hmReturnDetails.get("CHILDRAID"));
		
		strDistName = strDistName.equals("")?strAcctName:strDistName;
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Veiw Returns </TITLE>

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnPrintCreditMemo(){
	windowOpener('/GmPrintCreditServlet?hAction=PRINT&hRAID=<%=strRAId%>',"Credit","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmVendor" method="POST">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strRAId%>">

	<table border="0" width="600" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader"><fmtReturnView:message key="LBL_RETURNS_VIEW_DETAILS"/></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="598" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr>
						<td class="RightText" HEIGHT="20" width="100" align="right">&nbsp;<b><fmtReturnView:message key="LBL_RA" var="varRA"/><gmjsp:label type="RegularText"  SFLblControlName="${varRA}" td="false"/></b>:</td>
						<td class="RightText">&nbsp;<%=strRAId%></td>
						<td class="RightText" HEIGHT="20" width="200" align="right">&nbsp;<fmtReturnView:message key="LBL_RETURN_FROM" var="varReturnFrom"/><gmjsp:label type="RegularText"  SFLblControlName="${varReturnFrom}" td="false"/></td>
						<td class="RightText">&nbsp;<%=strDistName%></td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnView:message key="LBL_INITIATED_DATE" var="varInitiatedDate"/><gmjsp:label type="RegularText"  SFLblControlName="${varInitiatedDate}" td="false"/></td>
						<td class="RightText">&nbsp;<%=strCreatedDate%></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnView:message key="LBL_INITIATED_BY" var="varInitiatedBy"/><gmjsp:label type="RegularText"  SFLblControlName="${varInitiatedBy}" td="false"/></td>
						<td class="RightText">&nbsp;<%=strCreatedBy%></td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnView:message key="LBL_RETURN_TYPE" var="varRetrunType"/><gmjsp:label type="RegularText"  SFLblControlName="${varRetrunType}" td="false"/></td>
						<td class="RightText">&nbsp;<%=strType%></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnView:message key="LBL_RETURN_REASON" var="varReturnReason"/><gmjsp:label type="RegularText"  SFLblControlName="${varReturnReason}" td="false"/></td>
						<td class="RightText">&nbsp;<%=strReason%></td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnView:message key="LBL_EXPECTED_DATE" var="varExpectedDate"/><gmjsp:label type="RegularText"  SFLblControlName="${varExpectedDate}" td="false"/></td>
						<td class="RightText">&nbsp;<%=strExpDate%></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnView:message key="LBL_RETURNED_DATE" var="varReturnedDate"/><gmjsp:label type="RegularText"  SFLblControlName="${varReturnedDate}" td="false"/></td>
						<td class="RightText">&nbsp;<%=strRetDate%></td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnView:message key="LBL_SET_NAME" var="varSetName"/><gmjsp:label type="RegularText"  SFLblControlName="${varSetName}" td="false"/></td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnView:message key="LBL_ORIGINAL_ORDER" var="varOriginalOrder"/><gmjsp:label type="RegularText"  SFLblControlName="${varOriginalOrder}" td="false"/></td>
						<td class="RightText">&nbsp;<%=strOrgOrderId%></td>
					</tr>
					<tr><td bgcolor="#CCCCCC" height="1" colspan="4"></td></tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right"><fmtReturnView:message key="LBL_COMMENTS" var="varComments"/><gmjsp:label type="RegularText"  SFLblControlName="${varComments}" td="false"/></td>
						<td class="RightText" >&nbsp;<%=strDesc%></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtReturnView:message key="LBL_CHILD_RA" var="varChildRA"/><gmjsp:label type="RegularText"  SFLblControlName="${varChildRA}" td="false"/></td>
						<td class="RightText">&nbsp;<%=strChildRA%></td>						
					</tr>

					<tr>
						<td colspan="4">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25" width="70" align="center"><fmtReturnView:message key="LBL_PART_NUMBER"/></td>
									<td width="400"><fmtReturnView:message key="LBL_DESCRIPTION"/></td>
									<td width="50" align="center"><fmtReturnView:message key="LBL_QTY"/></td>
									<td width="50" align="center"><fmtReturnView:message key="LBL_STATUS"/></td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
<%
						intSize = alReturnsItems.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();

							String strLine = "";
							String strRate = "";
							String strAmount = "";
							String strPrice = "";
							log.debug(" strSetId is -- |" +strSetId +"|");
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alReturnsItems.get(i);
								log.debug(" hmLoop values are " + hmLoop);
									if (strSetId.equals("0") || !strSetId.equals(""))
									{
											strQty = GmCommonClass.parseZero((String)hmLoop.get("IQTY"));	
									}
									else
									strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
									
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPrice = GmCommonClass.parseZero((String)hmLoop.get("PRICE"));
								strStatusFl = GmCommonClass.parseNull((String)hmLoop.get("SFL"));

%>
								<tr>
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center">&nbsp;<%=GmCommonClass.getRedText(strQty)%></td>
									<td class="RightText" align="center">&nbsp;<%=strStatusFl%></td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}//End of FOR Loop
						}else {
%>
						<tr><td colspan="4" height="50" align="center" class="RightTextRed"><BR><fmtReturnView:message key="LBL_NO_PART_NUMBERS_RETURNED"/></td></tr>
<%
						}
%>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="4" height="30" align="center">
<%	
	if (strhAction.equals("ReturnDetails"))
	{
%>						<fmtReturnView:message key="BTN_PRINT" var="varPrint"/>
							<gmjsp:button value="${varPrint}" gmClass="button" onClick="javascript:window.print()" tabindex="13" buttonType="Load" />
						</td>
<%
	}else if (strhAction.equals("PrintMemo"))
	{
%>						<fmtReturnView:message key="BTN_PRINT_CREDIT_MEMO" var="varPrintCreditMemo"/>
							<gmjsp:button value="${varPrintCreditMemo}" gmClass="button" onClick="fnPrintCreditMemo();" tabindex="13" buttonType="Load" />
<%
	}
%>
					</tr>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
