 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmCreditMemoPrint.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" autoFlush="true" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- Imports for Logger -->



<%@ taglib prefix="fmtCreditMemoPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\returns\GmCreditMemoPrint.jsp -->

<fmtCreditMemoPrint:setLocale value="<%=strLocale%>"/>
<fmtCreditMemoPrint:setBundle basename="properties.labels.operations.returns.GmCreditMemoPrint"/>


<%
try {
	
	response.setCharacterEncoding("UTF-8");
	
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	int pageSize = 29; // Need to be a prime number

	
	String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
	String strApplnDateFmt = strGCompDateFmt;

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strHideBtn = GmCommonClass.parseNull((String)request.getAttribute("hideBtn"));
	//Getting currency format to show along with price
	String strCurrencyFmt = GmCommonClass.parseNull((String)hmReturn.get("COMPCURRENCYSYMBOL"));
	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), "");
	String strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYNAME"));
	String strAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("GMCOMPANYADDRESS"));
	strCountryCode = GmCommonClass.parseNull((String)hmCompanyAddress.get("COUNTRYCODE"));
	String strComapnyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMP_LOGO"));
	String strAccountCurrName = "";
	String strFontSize = "+3";
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.operations.returns.GmCreditMemoPrint", strSessCompanyLocale);
	if(!strCountryCode.equals("en")){
	  strFontSize = "+2";
	}
	
	
		
		//PC-5463
		String strCompId = "";
		strCompId = GmCommonClass.parseNull(gmDataStoreVO.getCmpid()); 
		if(!strAddress.equals("") && !strCompId.equals("1026")){
			strAddress = strAddress.replaceAll("/", "<br>&nbsp;&nbsp;");
		}
		if(!strAddress.equals("") && strCompId.equals("1026")){
            strAddress = strAddress.replaceAll("/", "");
			strAddress = strAddress.replaceAll("<BR>", "<br>&nbsp;&nbsp;&nbsp;");
		} 
		strAddress = "<b> &nbsp;&nbsp;&nbsp;"+ strCompanyName + "</b><br>&nbsp;&nbsp;&nbsp;" + strAddress;
	
	String strAdd = "";
	String strTemp = "";
	String strShade = "";
	String strRAId = "";
	String strRefId = "";
	String strSetName = "";
	String strCreditId = "";
	String strInvId = "";
	String strPO = "";

	String strDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strCreditDate = "";
	java.sql.Date dtRetDate = null;
	String strOrdComments  = "";
	String strShipCost = "";
	String strOrdType = "";
	
	String strType = "";
	String strStatusFl = "";
	String strOrderID = "";
	String strTitle = "";
	String strRAType = "";
	String strRADate = "";
	String strLine = "";
	String strRate = "";
	String strAmount = "";
	String strPrice = "";
	String strTotal = "";
	double dbPageTotal=0.0;
	
	String strStatus="";
	
	double dbAmount = 0.0;
	double dbTotal = 0.0;
	int pageCount = 1;
	
	ArrayList alReturnsItems = new ArrayList();
	ArrayList alChildRAs = new ArrayList();
	HashMap hmReturnDetails = new HashMap();
	
	boolean bolFl = false;
	boolean bFlag = false;

	int intPriceSize = 0;
	//int rowSpan = 6;

	if (hmReturn != null)
	{
		hmReturnDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("RADETAILS"));
		alReturnsItems = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("RAITEMDETAILS"));
		alChildRAs     = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("CHILDRADETAILS"));
		strRAId = GmCommonClass.parseNull((String)hmReturnDetails.get("RAID"));
		strRefId = GmCommonClass.parseNull((String)hmReturnDetails.get("REFID"));
		strSetName = GmCommonClass.parseNull((String)hmReturnDetails.get("SNAME"));
		strDate = GmCommonClass.getStringFromDate((java.sql.Date)hmReturnDetails.get("UDATE"),strApplnDateFmt);
		strAdd = GmCommonClass.parseNull((String)hmReturnDetails.get("ACCADD"));
		strCreditId = GmCommonClass.parseNull((String)hmReturnDetails.get("CREDITINVID"));
		strPO = GmCommonClass.parseNull((String)hmReturnDetails.get("PO"));
		strCreditDate =GmCommonClass.getStringFromDate((java.sql.Date)hmReturnDetails.get("CREDITDATE"),strApplnDateFmt);
		strRADate = GmCommonClass.getStringFromDate((java.sql.Date)hmReturnDetails.get("RETDATE"),strApplnDateFmt);
		strInvId = GmCommonClass.parseNull((String)hmReturnDetails.get("INVID"));
		strOrdComments = GmCommonClass.parseNull((String)hmReturnDetails.get("ORDER_COMMENTS"));	
		strShipCost	= GmCommonClass.parseZero(((String)hmReturnDetails.get("SCOST")));	
		strOrderID = GmCommonClass.parseNull(((String)hmReturnDetails.get("ORDER_ID")));
		strOrdType = GmCommonClass.parseNull(((String)hmReturnDetails.get("ORDTYPE")));
		strRAType = GmCommonClass.parseNull(((String)hmReturnDetails.get("RETTYPE")));
		strStatus = GmCommonClass.parseNull(((String)hmReturnDetails.get("STATUS")));
		strAccountCurrName  = GmCommonClass.parseNull((String)hmReturnDetails.get("ACCOUNT_CURR_NM"));
		dbTotal = Double.parseDouble(strShipCost);
	//	log.debug(" OrdType is " + strOrdType + " Returns Type is " + strRAType)	;
		//strRADate = GmCommonClass.getStringFromDate(dtRetDate,strApplnDateFmt);
		if(strRADate.equals(""))
		{
			strRADate = "NA";
		}
	}
	
	if(!strOrderID.equals(""))
	{
	if (strRAType.equals("3300"))
	{
				if (strOrdType.equals("2528"))
				{
					strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CREDIT_MEMO"));
				}
				else
					strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SALES_ADJUSTMENT"));
	}
	else
	  { 
		strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CREDIT_MEMO"));
	  }
	}
	
 else if(strStatus.equals("2"))
  {		
  strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CREDIT_SUMMARY_LIST"));
    strPO = "NA";
    strInvId = "NA";
    strCreditId="NA";
    strCreditDate="NA";
  }
  else
  {
    strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PENDITNG_RETURN_LIST"));
    strPO = "NA";
    strInvId = "NA";
    strCreditId="NA";
    strCreditDate="NA";
  }
	ArrayList alLogReason = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("LOGREASON"));
	int intLog = 0;
	HashMap hmLog = new HashMap();
// log.debug(" strTitle is " + strTitle);
 int intSize = 0;
 HashMap hcboVal = null;
 String strStyle="display: run-in";
 
 StringBuffer sbHtmlCode = new StringBuffer();
							sbHtmlCode.append("<table border='0' width='700' cellspacing='0' cellpadding='0' align='center'>");
							sbHtmlCode.append("<tr><td bgcolor='#666666' colspan='3'></td></tr>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='6'></td>");
							sbHtmlCode.append("<td>");
							sbHtmlCode.append("<table cellpadding='0' cellspacing='0' border='0' width='100%'>");
							sbHtmlCode.append("<tr><td colspan='7' height='1' bgcolor='#666666'></td></tr>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td class='RightText' valign='top'><img src='"+strImagePath);
							sbHtmlCode.append("/"+strComapnyLogo+".gif' width='138' height='60'><br>");
									sbHtmlCode.append(strAddress);							
									
							sbHtmlCode.append("<td class='RightText' align='right'>");
							sbHtmlCode.append("	<img src='/GmCommonBarCodeServlet?ID=");
							sbHtmlCode.append(strRAId); 
							sbHtmlCode.append("' height=20 width=170 />&nbsp;Paperwork");
							if(strCompId.equals("1026")){
								sbHtmlCode.append("<br><font size='"+strFontSize+"' style='"+strStyle+"'>"+strTitle+"</font>&nbsp;</td>");
							}else{
								sbHtmlCode.append("<font size='"+strFontSize+"' style='"+strStyle+"'>"+strTitle+"</font>&nbsp;</td>");
							}
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("</table>");
							sbHtmlCode.append("</td>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='6'></td>");
							sbHtmlCode.append("</tr>");
							
							sbHtmlCode.append("<tr><td bgcolor='#666666'></td></tr>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td width='698' height='100' valign='top'>");
							sbHtmlCode.append("<table border='0' width='100%' cellspacing='0' cellpadding='0'>");
							sbHtmlCode.append("<tr><td colspan='9'  height='1' bgcolor='#666666'></td></tr>");
							sbHtmlCode.append("<tr bgcolor='#eeeeee' class='RightTableCaption'>");
							sbHtmlCode.append("<td height='25' align='center' width='300'>&nbsp;Customer</td>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='8'></td>");
							sbHtmlCode.append("<td height='25'width='130' align='center'>&nbsp;Ref ID</td>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='8'></td>");
							sbHtmlCode.append("<td width='110' align='center'>&nbsp;Customer PO #</td>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='4'></td>");
							sbHtmlCode.append("<td width='110' align='center'>&nbsp;Credit No.</td>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='4'></td>");
							sbHtmlCode.append("<td width='110' align='center'>&nbsp;Credit Date</td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr><td bgcolor='#666666' height='1' colspan='9'></td></tr>");
							
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td class='RightText' rowspan='7' valign='top'>");
							sbHtmlCode.append("<table>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td width='25'>&nbsp;</td>");
							sbHtmlCode.append("<td width='250' class='RightTableCaption'>&nbsp;"+strAdd);
							sbHtmlCode.append("</td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("</table>&nbsp;<BR>&nbsp;<BR>");
							sbHtmlCode.append("</td>");
							sbHtmlCode.append("<td class='RightText' align='center'>&nbsp;"+strRefId+"</td>");
							sbHtmlCode.append("<td class='RightText' align='center'>&nbsp;"+strPO+"</td>");
							sbHtmlCode.append("<td height='25' class='RightText' align='center'>&nbsp;"+strCreditId+"</td>");
							sbHtmlCode.append("<td class='RightText' align='center'>&nbsp;"+strCreditDate+"</td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='3'></td>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='3'></td>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='3'></td>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='3'></td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr bgcolor='#eeeeee' class='RightTableCaption'>");
						
							sbHtmlCode.append("<td class='RightText' align='center' height='20'>Invoice #</td>");
							sbHtmlCode.append("<td colspan='3' align='center'>RA ID</td>");
							sbHtmlCode.append("<td bgcolor='#666666' width='1' rowspan='4'></td>");
							sbHtmlCode.append("<td colspan='3' align='center'>RA Date</td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr>");
							
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='3'></td>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='3'></td>");
							sbHtmlCode.append("<td bgcolor='#666666' height='1' colspan='3'></td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr>");
							
							sbHtmlCode.append("<td class='RightText' rowspan='3' align='center'>&nbsp;"+strInvId+"<b></b></td>");
							sbHtmlCode.append("<td align='center' colspan='3' height='25'  class='RightText'>&nbsp;"+strRAId+"</td>");
							sbHtmlCode.append("<td align='center' colspan='3' height='25'  class='RightText'>&nbsp;" +strRADate+"</td>");
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("</table>");
							sbHtmlCode.append("</td>");
							sbHtmlCode.append("</tr>");
													
							sbHtmlCode.append("<tr>");
							sbHtmlCode.append("<td>");
							sbHtmlCode.append("<table cellspacing='0' cellpadding='0' border='0' width='100%' id='myTable'>");
							sbHtmlCode.append("<tr><td colspan='8' height='1' bgcolor='#666666'></td></tr>");
							sbHtmlCode.append("<tr bgcolor='#eeeeee' class='RightTableCaption'>								");
							sbHtmlCode.append("<td height='25' width='70' align='center'>Part Number</td>");
							sbHtmlCode.append("<td width='300'>Description</td>");
							sbHtmlCode.append("<td width='50' align='center'>Qty</td>");
							sbHtmlCode.append("<td align='center'>Rate EA</td>");
							sbHtmlCode.append("<td>Status</td>									");
							sbHtmlCode.append("<td align='center'>Amount</td>									");							
							sbHtmlCode.append("</tr>");
							sbHtmlCode.append("<tr><td colspan='8' height='1' bgcolor='#666666'></td></tr> ");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Consign </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/watermark.js"></script>
<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
var tdinnnerTopPrintBtn = "";
function hidePrint()
{
	strObject = eval('document.all.button');
	strTopPrintBtnObj = eval('document.all.TopPrintButton');
	tdinnner = strObject.innerHTML;
	tdinnnerTopPrintBtn = strTopPrintBtnObj.innerHTML;
	strObject.innerHTML = "";
	strTopPrintBtnObj.innerHTML = "";
}

function showPrint()
{
	strObject = eval('document.all.button');
	strObject.innerHTML = tdinnner ;
	strTopPrintBtnObj = eval('document.all.TopPrintButton');
	strTopPrintBtnObj.innerHTML = tdinnnerTopPrintBtn ;
}
function fnLoad()
{
	if(window.opener){
		document.all.Btn_Close.disabled=false;
		document.all.Btn_Close2.disabled=false;
	}else{
		document.all.Btn_Close.disabled=true;
		document.all.Btn_Close2.disabled=true;
	}
	markMe();
}
</script>
</HEAD>

<BODY topmargin="10" onload="return fnLoad();" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmReportCreditsServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hConsignId" value="<%=strRAId%>">
		<%if(!strHideBtn.equals("Y")){ %>
<table border='0' width='700' cellspacing='0' cellpadding='0' align='center'>
	<tr>
		<td colspan='3' height='30' align='center' id='TopPrintButton'>
		<fmtCreditMemoPrint:message key="BTN_PRINT" var="varPrint"/>
			<gmjsp:button  value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnPrint();" />
&nbsp;<fmtCreditMemoPrint:message key="BTN_CLOSE" var="varClose"/>
<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close2" gmClass="button" onClick="window.close();" buttonType="Load" />		</td>

	</tr>
</table>
		<%} %>	
<%=sbHtmlCode.toString()%>
	<%
						intSize = alReturnsItems.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();

							double dbQty = 0.0;
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alReturnsItems.get(i);

								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strStatusFl = GmCommonClass.parseNull((String)hmLoop.get("SFL"));

								dbQty = Double.parseDouble(strQty); 
								
								dbAmount = Double.parseDouble(strPrice);
								dbAmount = dbQty * dbAmount; // Multiply by Qty
								strAmount = ""+dbAmount;
								
								dbTotal = dbTotal + dbAmount;
								strTotal = ""+dbTotal;
								dbPageTotal = dbPageTotal + dbAmount;

								out.print(strLine);
%>
								<tr>									
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText" align="center">&nbsp;<%=GmCommonClass.getRedText(Double.toString(dbQty))%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strPrice))%>&nbsp;&nbsp;</td>
									<td class="RightText">&nbsp;<%=strStatusFl%></td>
									<td class="RightText" align="right"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strAmount))%>&nbsp;&nbsp;&nbsp;</td>																		
								</tr>
<%
							if ((i > 0) && (i % pageSize) == 0)
								{
								pageCount++;
%>								
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr>
									<td  height="35"></td>
									<td class="RightTableCaption"><fmtCreditMemoPrint:message key="LBL_PG_TOTAL"/></td>
									<td colspan="4" class="RightTableCaption" align="right"><%=strAccountCurrName%><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(String.valueOf(dbPageTotal)))%>&nbsp;&nbsp;&nbsp;</td>
								</tr>									
								<tr><td colspan="6" height="20" class="RightText" align="right"><fmtCreditMemoPrint:message key="LBL_CONTPG_PAGE"/> -></td></tr>		
								<tr><td colspan="6" height="2" bgcolor="#666666"></td></tr>																						
							</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
</table>
<p STYLE="page-break-after: always"></p>								
<%									
								dbPageTotal = 0;%>
<br/>
								<% out.println(sbHtmlCode);																		

							     }		
 if(i > 0 && i % pageSize == 0 && i == (intSize -1))	
  {
   bFlag = true;						     
%>

					</td>
				</tr>
			</table>

<%							     
    }
							}//End of FOR Loop
 if(!bFlag)
  {
   if(pageCount > 1 && intSize == pageSize)
     {							
%>
						<tr>	
						<td  height="35" ></td>
							<td class="RightTableCaption"><fmtCreditMemoPrint:message key="LBL_PG_TOTAL"/></td>						
							<td colspan="4" class="RightTableCaption" align="right"><%=strAccountCurrName%><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(String.valueOf(dbPageTotal)))%>&nbsp;&nbsp;&nbsp;</td>
						</tr>	
 <%
     }
 %>						
								
					</table>								
				</td>		
			</tr>
<% } %>
	<tr>
	<td>
	<table border='0' width='100%' cellspacing='0' cellpadding='0'>
		<tr>
			<td class="RightText" width="10%" height="35" align="center">&nbsp;</td>
			<td class="RightText" width="60%"><b><fmtCreditMemoPrint:message key="LBL_SHIPPING_CHARGES"/></b></td>
			<td class="RightText" align="right" width="20%"><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strShipCost))%>&nbsp;&nbsp;&nbsp;</td>
		</tr>
		<tr> 
			<td colspan="4"  class="RightText" align="left">&nbsp;<%=strOrdComments%> </td>
		</tr>
		<%
		if(alLogReason != null){ 
		intLog = alLogReason.size();
		}
		if (intLog > 0) {		
		for (int i=0;i<intLog;i++)
		{	hmLog = (HashMap)alLogReason.get(i);
		%>
			<tr><td colspan=4 class="RightText" align="left">&nbsp;<%=(String)hmLog.get("COMMENTS")%></td></tr>
		<%}} %>		
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td class="RightText" width="10%" height="35">&nbsp;</td>
			<td class="RightTableCaption" width="60%"><fmtCreditMemoPrint:message key="LBL_TOTAL"/></td>
			<td class="RightTableCaption" align="right" width="20%"><%=strAccountCurrName%><%=GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strTotal))%>&nbsp;&nbsp;&nbsp;</td>
		</tr>		
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
		<%if(!strHideBtn.equals("Y")){ %>
		<tr>
			<td colspan="4" height="30" align="center" id="button">
			<fmtCreditMemoPrint:message key="BTN_PRINT" var="varPrint"/>
			<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />
			&nbsp;<fmtCreditMemoPrint:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close();" buttonType="Load" />			
			</td>
		</tr>
		</td>
		</tr>
		</table>
		<%} %>
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
   </table>
<%int intAlSize = alChildRAs.size();
	HashMap hmChilRa = new HashMap();
	String strChildRa="";
	String strProjectNm="";
    for(int i=0; i<intAlSize ; i++){ 
    hmChilRa = (HashMap)alChildRAs.get(i);
    strChildRa = GmCommonClass.parseNull((String)hmChilRa.get("CRAID"));
    strProjectNm = GmCommonClass.parseNull((String)hmChilRa.get("PROJNM"));
    %>
    	<b><%=strChildRa%> created for <%=strProjectNm%></b><br>	
    	
   <% 
   }
	%>
 
<%
						}else {
%>
						<tr><td colspan="6" height="50" align="center" class="RightTextRed"><BR><fmtCreditMemoPrint:message key="MSG_NO_DATA"/> !</td></tr>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="4" height="0" bgcolor="#666666"></td></tr>
</table>						
<%
						}
			
%>
<% %>
 <tr><td colspan="4" height="0" bgcolor="#666666"></td></tr> 
</FORM>
<% /* Below code to display bookmark value 
	* 2522 maps to duplicate order 
	* If Credited before invoice then display the Water mark
	* so the 
	*/  
//	System.out.println("strPO '" + strPO + "'");
if (strPO.equals("") && !strOrderID.equals(""))
{
%>

<div id="waterMark" style="position:absolute; z-Index: 0; top: 340; left: 340">
	<img src="<%=strImagePath%>/credit_bef_invoice.jpg"  border=0>
	</div>
<% } else {%>
<div id="waterMark"></div>
<% }%>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
