 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmIncludeReturnReceivePart.jsp
 * Desc		 		: This is a include file for diplaying the par cons wty and ret qty
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@include file="/common/GmCommonInclude.jsp" %>
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtIncludeReturnReceivePart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\returns\GmIncludeReturnReceivePart.jsp -->

<fmtIncludeReturnReceivePart:setLocale value="<%=strLocale%>"/>
<fmtIncludeReturnReceivePart:setBundle basename="properties.labels.operations.returns.GmIncludeReturnReceivePart"/>

<%
    String strChecked = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";
	String strItemQty = "";
	String strControl = "";
	String strOrgControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strItemType="";
	String strItemPrice = "";
	String strPartNumArr = "";
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.operations.returns.GmIncludeReturnReceivePart", strSessCompanyLocale);
	
	String strType = "";
	String strColHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CONS_QTY"));
	ArrayList alSetLoad = new ArrayList();
	HashMap hmRaDetails = new HashMap();
	
	int colspan = 6;
	String strPickLocRule = "";
	String strIncludeRuleEng = "";
	String strRAId = GmCommonClass.parseNull((String)request.getParameter("hRAId"));
	String strhAction = GmCommonClass.parseNull((String)request.getParameter("hAction"));
	

	if(strhAction.equals("LoadReconf") || strhAction.equals("ReloadReconf") || strhAction.equals("SaveReconf") )
	{
		colspan = 7;
	}
	
	 HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	 	if( hmReturn != null)
	 	 {
			 alSetLoad = (ArrayList)hmReturn.get("RAITEMDETAILS");
			 hmRaDetails = (HashMap)hmReturn.get("RADETAILS");
			 if(hmRaDetails !=null){
				 strType = GmCommonClass.parseNull((String)hmRaDetails.get("TYPEID"));
			 }
	 	 }	 
	 	// 3308 - IH Loaner Item and 3309 - Product Loaner Item 
	if(strType.equals("3308") || strType.equals("3309")){
		strColHeader = "Loaned<br>Qty";
	}
	int intSize = 0;
	request.setAttribute("alReturn", alSetLoad);
%>
<script language="JavaScript" src="<%=strJsPath%>/prodmgmnt/GmRule.js"></script>
<script>
	var vIncludeRuleEng = '<%=strIncludeRuleEng.toUpperCase()%>';
	var pickRule = '<%=strPickLocRule%>';
	var strAction = '<%=strhAction%>';
</script>


				<table border="0" width="700" cellspacing="0" cellpadding="0">
				<input type="hidden" value="<%=strType %>" id="hRetType" name="hRetType"/>
					<tr>
						<td>
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan='<%=colspan%>' height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25"><fmtIncludeReturnReceivePart:message key="LBL_PART_#"/></td>
									<td width="350"><fmtIncludeReturnReceivePart:message key="LBL_DESCRIPTION"/></td>
									<td width="40"><fmtIncludeReturnReceivePart:message key="LBL_INIT"/>. <br><fmtIncludeReturnReceivePart:message key="LBL_QTY"/> </td>
<%
									if(strhAction.equals("LoadReconf") || strhAction.equals("ReloadReconf") || strhAction.equals("SaveReconf"))
									 {
%>								
											<td width="55"><fmtIncludeReturnReceivePart:message key="LBL_PEND"/>. <br><fmtIncludeReturnReceivePart:message key="LBL_QTY"/> </td>
<%
									 }
%>									
									<td width="55"><%=strColHeader %></td>
									<td width="50"><fmtIncludeReturnReceivePart:message key="LBL_SCAN"/>. <br> <fmtIncludeReturnReceivePart:message key="LBL_QTY"/></td>
									<td width="70"><fmtIncludeReturnReceivePart:message key="LBL_RET.QTY"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtIncludeReturnReceivePart:message key="LBL_CONTROL"/></td>
								</tr>
								<tr><td class=Line colspan='<%=colspan%>' height=1></td></tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
						//  System.out.println(alSetLoad);
						  
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							String strPrevPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							String strConsQty ="";
							String strPendingQty="";
							int intSetQty = 0;
							String strTagFl = "";
							String strSetQty = "";
							boolean bolQtyFl = false;							
							String strPartNumIndex = "";
							String strParthidsts = "";
							String strParthidfinalsts = "";
							String strPartMaterialType = "";
							double intAllTotQty = 0;
							String strUnitPrice = "";
							String strUnitPriceAdjValue = "";
							String strUnitAdjCode = "";
							String strErrorDtls = "";
							for (int i=0;i<intSize;i++){
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1){
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strPartMaterialType = GmCommonClass.parseNull((String)hmLoop.get("PARTMTRELTYPE"));
								strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strSetQty = GmCommonClass.parseNull((String)hmLoop.get("SETQTY"));
								strSetQty = strSetQty.equals("")?GmCommonClass.parseNull((String)hmLoop.get("QTY")):strSetQty;
								strConsQty =  GmCommonClass.parseNull((String)hmLoop.get("CONS_QTY"));
								strOrgControl = GmCommonClass.parseNull((String)hmLoop.get("ORG_CNUM"));
								//intSetQty = Integer.parseInt(strQty);
								strItemType = GmCommonClass.parseNull((String)hmLoop.get("ITYPE"));
								strItemPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strPendingQty = GmCommonClass.parseNull((String)hmLoop.get("PENDING_QTY"));
								strPendingQty = strPendingQty.equals("")?"0" :strPendingQty;
								strTagFl = GmCommonClass.parseNull((String)hmLoop.get("TAGFL"));
								strParthidsts = "";
								//bolQtyFl = intSetQty > 1?true:false;
								
								strTemp = strPartNum;
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								strUnitPrice = GmCommonClass.parseZero((String)hmLoop.get("UPRICE"));
								strUnitPriceAdjValue = GmCommonClass.parseZero((String)hmLoop.get("UADJVAL"));
								strUnitAdjCode = GmCommonClass.parseNull((String)hmLoop.get("UADJCODE"));
								strPartNumArr = strPartNumArr + strPartNum + ','+strItemQty+ ',' +strConsQty+'|'; 
								strErrorDtls = GmCommonClass.parseNull((String)hmLoop.get("ERRORDTLS"));
								strErrorDtls = strErrorDtls.replaceAll("<B>", "").replaceAll("</B>", "");
								
								if(!strPrevPartNum.equals(strPartNum)&&(i!=0)){
									strParthidsts = "<input type=\"hidden\" id=\""+strPrevPartNum+"\" name=\""+strPrevPartNum+"\" value=\""+strPartNumIndex+"\">";
									strPartNumIndex = "";
								}
								if(!strPrevPartNum.equals(strPartNum)){
								  	intAllTotQty += Double.parseDouble(GmCommonClass.parseZero(strItemQty));
								}
								
								strPartNumIndex = strPartNumIndex +i+",";
								
								if(strPartNum.equals(strNextPartNum)){
									intCount++;
									strLine = "<TR><TD colspan=4 height=1 bgcolor=#eeeeee></TD></TR>";
								}else{
									strLine = "<TR><TD colspan=4 height=1 bgcolor=#eeeeee></TD></TR>";
									if (intCount > 0){
										strPartNum = "";
										strPartDesc = "";
										strItemQty = "";
									    strConsQty = "";
									 //	strQty = "";
									 //strColor = "bgcolor=white";
										strLine = "";
									}else{
										//strColor = "";
									}
									intCount = 0;
								}						
								
								if (intCount > 1){
									strPartNum = "";
									strPartDesc = "";
									strItemQty = "";
									strConsQty = "";
									//strQty = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}
								
								if(i==(intSize-1)){		
									strParthidfinalsts = "<input type=\"hidden\" id=\""+(strPartNum.equals("")?strPrevPartNum:strPartNum)+"\" name=\""+(strPartNum.equals("")?strPrevPartNum:strPartNum)+"\" value=\""+strPartNumIndex+"\">";
									strPartNumIndex="";									
								}
								
								strPrevPartNum = strPartNumHidden;
								out.print(strLine);
%><tr><td height="1" bgcolor="#cccccc" colspan='<%=colspan%>'></td></tr>

								<tr <%=strShade%> id="tr<%=i%>">
<%
					 		if ((strhAction.equals("Load")  && bolQtyFl) || (strhAction.equals("EditLoadDash") && bolQtyFl) || strhAction.equals("Reload") || strhAction.equals("Save") || strhAction.equals("LoadReconf") || strhAction.equals("ReloadReconf") || strhAction.equals("SaveReconf"))
					     		{
%>
									<td class="RightText" height="20">&nbsp;<%if(strTagFl.equals("Y")){%><a href=javascript:fnOpenTag('<%=strPartNum %>','<%=strRAId %>')> <img border="0" src=<%=strImagePath%>/tag.jpg ></a><%}%><a class="RightText" tabindex=-1 
									href="javascript:fnSplit('<%=i%>','<%=strPartNum%>','<%=strControl%>','<%=strTagFl%>','<%=strItemPrice%>','<%=strUnitPrice%>','<%=strUnitPriceAdjValue%>','<%=strUnitAdjCode%>');"><%=strPartNum%></a>
									<input type="hidden" name="strTagFl<%=i%>" value="<%=strTagFl%>">
									<input type="hidden" name="hPartNum<%=i%>" id="hPartNum<%=i%>" value="<%=strPartNumHidden%>"></td>
<%
								}else{
%>
									<td class="RightText" height="20">&nbsp;<%if(strTagFl.equals("Y")){%><a href=javascript:fnOpenTag('<%=strPartNum %>','<%=strRAId%>')> <img border="0" src=<%=strImagePath%>/tag.jpg ></a><%}%><%=strPartNum%>
									<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>"></td>
<%
									}
%>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									
									<td align="center" class="RightText"><%=strItemQty%>
									<input type="hidden" name="hPartNumTotalQty<%=i%>" value="<%=strItemQty%>"></td>
									<input type="hidden" id="hQty<%=strPartNumHidden.replaceAll("\\.","")%>" name="hQty<%=strPartNumHidden.replaceAll("\\.","")%>" value="<%=strItemQty%>"></td>
									<input type="hidden" id="hPartMaterialType<%=i%>" name="hPartMaterialType<%=i%>" value="<%=strPartMaterialType%>">
<%
									if(strhAction.equals("LoadReconf") || strhAction.equals("ReloadReconf") || strhAction.equals("SaveReconf"))
									 {
%>									
											<td align="center" class="RightText"><%=strPendingQty%></td>
<%									 }	
%>									
									
									<td align="center" class="RightText"><%=strConsQty%>
									<td align="center"><label id ="lblPartScan<%=strPartNum %>" ></label>  </td>
									<input type="hidden" name="hConsQty<%=i%>" value="<%=strConsQty%>">
									<input type="hidden" name="hItemType<%=i%>" value="<%=strItemType%>">
									<input type="hidden" name="hItemPrice<%=i%>" value="<%=strItemPrice%>">
                                    <input type="hidden" name="hUnitPrice<%=i%>" value="<%=strUnitPrice%>">
									<input type="hidden" name="hUnitPriceAdj<%=i%>" value="<%=strUnitPriceAdjValue%>">
									<input type="hidden" name="hAdjCode<%=i%>" value="<%=strUnitAdjCode%>">
									</td>
<%
								if (strhAction.equals("EditLoad")|| strhAction.equals("EditLoadDash") || strhAction.equals("Reload") || strhAction.equals("Save") || strhAction.equals("LoadReconf") || strhAction.equals("ReloadReconf") || strhAction.equals("SaveReconf"))
								{  	
									if (i%2 == 0){
%>
									<td id="Cell<%=i%>" class="RightText"><input type="text" size="3" value="<%=strSetQty%>" name="Txt_Qty<%=i%>" id="Txt_Qty<%=i%>" class="InputArea" onFocus="changeTRBgColor('<%=i%>','#AACCE8');" onBlur="changeTRBgColor('<%=i%>','#ffffff');" >&nbsp;&nbsp;
									<input type="text" size="10" value="<%=strControl%>" name="Txt_CNum<%=i%>" id="Txt_CNum<%=i%>" value="<%=strControl%>" class="InputArea" maxlength="20"  onFocus="changeTRBgColor('<%=i%>','#AACCE8');fnBlankLine(<%=i%>,this.value);" onBlur="changeTRBgColor('<%=i%>','#ffffff');fnChangeToCaps('<%=i%>');fnFetchMessages(this,'<%=strPartNumHidden %>','PROCESS', '','RETURN');" >
									<%-- <%if(strErrorDtls.equals("") && !strControl.equals("") && !strControl.equals("TBE")){
%>									<span id="DivShowCnumAvl<%=i%>" style="vertical-align:middle;"> <img height="16" width="19" title="Control number available" src="<%=strImagePath%>/success.gif"></img></span>
<%
									}else--%> <% if(!strErrorDtls.equals("") && !strControl.equals("") && !strControl.equals("TBE")){
%>
						 			<span id="DivShowCnumNotExists<%=i%>" style="vertical-align:middle;"> <img height="12" width="12" title="<%=strErrorDtls %>" src="<%=strImagePath%>/delete.gif"></img></span>
<%									} %> 
									</td>
									<%}else{ %>
									<td id="Cell<%=i%>" class="RightText"><input type="text" size="3" value="<%=strSetQty%>" name="Txt_Qty<%=i%>" id="Txt_Qty<%=i%>" class="InputArea" onFocus="changeTRBgColor('<%=i%>','#AACCE8');" onBlur="changeTRBgColor('<%=i%>','#eeeeee');" >&nbsp;&nbsp;
									<input type="text" size="10" value="<%=strControl%>" name="Txt_CNum<%=i%>" id="Txt_CNum<%=i%>" value="<%=strControl%>" class="InputArea" maxlength="20"  onFocus="changeTRBgColor('<%=i%>','#AACCE8');fnBlankLine(<%=i%>,this.value);" onBlur="changeTRBgColor('<%=i%>','#eeeeee');fnChangeToCaps('<%=i%>');fnFetchMessages(this,'<%=strPartNumHidden %>','PROCESS', '','RETURN');" >
									<%-- <%if(strErrorDtls.equals("") && !strControl.equals("") && !strControl.equals("TBE")){
%>									<span id="DivShowCnumAvl<%=i%>" style="vertical-align:middle;"> <img height="16" width="19" title="Control number available" src="<%=strImagePath%>/success.gif"></img></span>
<%
									}else--%><% if(!strErrorDtls.equals("") && !strControl.equals("") && !strControl.equals("TBE")){
%>
						 			<span id="DivShowCnumNotExists<%=i%>" style="vertical-align:middle;"> <img height="12" width="12" title="<%=strErrorDtls %>" src="<%=strImagePath%>/delete.gif"></img></span>
<%									} %> 
									</td>
									<%} %>
									<%if (strParthidsts.length()>0){%>
										<%=strParthidsts%>
									<%}%>
									<%if (strParthidfinalsts.length()>0){%>
										<%=strParthidfinalsts%>
									<%}%>
									
<%
								}else{
%>
									<td>&nbsp;</td>
<%
								}
%>
								</tr>
								<input type="hidden" id="hControl<%=i%>"  name="hControl<%=i%>" value="<%=strControl%>"/>
								<input type="hidden" name="hCnt" id="hCnt" value="<%=intSize-1%>">
								<input type="hidden" id="hNewCnt" name="hNewCnt" value="0"><%--This hidden field used to keep java script  var cnt value  --%>
<%
								//out.print(strLine);
							}//End of FOR Loop
%><input type="hidden" id="hPartNumArr" name="hPartNumArr" value="<%=strPartNumArr%>">
  <input type="hidden" id="hTotalParts" name="hTotalParts" value="<%=intAllTotQty%>">
<tr><td height="1" bgcolor="#cccccc" colspan='<%=colspan%>'></td></tr>
<%						}else {
%>
						<tr><td colspan="5" height="50" align="center" class="RightTextRed"><BR><fmtIncludeReturnReceivePart:message key="MSG_NO_DATA"/> !</td></tr>
<%
							request.setAttribute("PSIZE","0");
						}		
%>
							</table>
						</td>
					</tr>
				</table>	