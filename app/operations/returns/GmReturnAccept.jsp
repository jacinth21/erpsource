
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@page import="com.globus.common.beans.GmCalenderOperations"%>

<%@page import="java.util.StringTokenizer"%>
<%
	/**********************************************************************************
	 * File		 		: GmReturnAccept.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 1.0
	 * author			: Dhinakaran James
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>

<%@include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtReturnAccept" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\returns\GmReturnAccept.jsp -->

<fmtReturnAccept:setLocale value="<%=strLocale%>"/>
<fmtReturnAccept:setBundle basename="properties.labels.operations.returns.GmReturnAccept"/>

<%
		String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
		String strWikiTitle = GmCommonClass.getWikiTitle("RECEIVE_RETURNS_REPORT");
		HashMap hmReturn = (HashMap) request.getAttribute("hmReturn");
		String strhAction = (String) request.getAttribute("hAction") == null ? ""
				: (String) request.getAttribute("hAction");
		String strItemType = "";
		String strRAId = "";
		String strStatusFlag = "";
		String strType = "";
		int intPriceSize = 0;
		GmResourceBundleBean gmResourceBundleBeanlbl = 
		    GmCommonClass.getResourceBundleBean("properties.labels.operations.returns.GmReturnAccept", strSessCompanyLocale);

		String strRAComplete = GmCommonClass.parseNull((String) request
				.getParameter("Chk_RAComplete"));
		String strPartNums = GmCommonClass.parseNull((String) request
				.getParameter("Txt_PartNums"));
		String strOpt = GmCommonClass.parseNull((String) request
				.getParameter("hOpt"));
		String strSelectedSetID = GmCommonClass
				.parseNull((String) request.getParameter("Cbo_SetID"));
		String strOrderID = GmCommonClass.parseNull((String) request
				.getAttribute("ORDERID"));
		String strLoadDisabled = strOrderID.equals("") ? ""
				: "disabled";
		
		String strApplDateFmt = strGCompDateFmt;
		String strControlNoScan = GmCommonClass.parseNull((String)GmCommonClass.getString("SCAN_CONTROL_NO"));
		GmCalenderOperations.setTimeZone(strGCompTimeZone);
		String strCurrDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt);

		ArrayList alSets = request.getAttribute("SETS") == null ? new ArrayList()
				: (ArrayList) request.getAttribute("SETS");
		HashMap hmReturnDetails = (HashMap) request
				.getAttribute("hmReturnDetails");
		String strMsg = "";
		String strIHMsg = "";
		String strHMode = GmCommonClass.parseNull(request
				.getParameter("hMode"));
		strHMode = strHMode.equals("") ? "View" : strHMode;
		if (hmReturnDetails != null) {
			strMsg = GmCommonClass.parseNull((String) hmReturnDetails
					.get("MESSAGE"));
			if(strMsg.indexOf("|")>0){
				StringTokenizer st = new StringTokenizer(strMsg,"|");
				strMsg = GmCommonClass.parseNull(st.nextToken());
				strIHMsg = GmCommonClass.parseNull(st.nextToken());
			}
		}
		String strDisabled = "";
		String strVoidDisabled = "";
		String strTypeId = "";
		String strErrorCnt = "";
		if( hmReturn != null)
	 	 {
			 HashMap hmConsignDetails = (HashMap)hmReturn.get("RADETAILS");
			 strTypeId = GmCommonClass.parseNull((String)hmConsignDetails.get("TYPEID"));
			 strErrorCnt = GmCommonClass.parseNull((String)hmConsignDetails.get("ERRCNT"));
		 }
		if(strTypeId.equals("3308") || strTypeId.equals("3309")){
			strLoadDisabled = "disabled";
		}
		String strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RECEIVE_RETURNS"));
		if (strhAction.equals("LoadReconf")
				|| strhAction.equals("ReloadReconf")
				|| strhAction.equals("SaveReconf")) {
			strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PARTIAL_RETURNS"));
		}
		
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Receive Returns</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
	<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmReturnAccept.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var RaComplete = '<%=strRAComplete%>';
var format = '<%=strApplDateFmt%>';
var compCurrDt = '<%=strCurrDate%>';
var typeId = '<%=strTypeId%>';
var errCnt = '<%=strErrorCnt%>';
</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad()">
<FORM name="frmVendor" method="POST"
	action="<%=strServletPath%>/GmReturnReceiveServlet"><input
	type="hidden" name="hAction" value="<%=strhAction%>"> <input
	type="hidden" name="hStatusFl" value=""> <input type="hidden"
	name="hVerifyFl" value=""> <input type="hidden" name="hTxnId">
<input type="hidden" name="hCancelType" value="VDRTN"> <input
	type="hidden" name="hOpt">
<input type="hidden" name="RE_FORWARD" value="gmReturnReceiveServlet">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" value="RECEIVERETURNSWRAPPER">
<input type="hidden" name="TXN_TYPE_ID" value="RETURN">
<input type="hidden" name="chkRule" value= "No">
<input type="hidden" name="hInputString">
<input type="hidden" name="hReturnType">
<input type="hidden" name="partMaterialType"  id="partMaterialType" value= "">
 
<table border="0" class="DtTable800" cellspacing="0" cellpadding="0" >
	<tr>
		<td height="25"  class="RightDashBoardHeader"><%=strTitle%></td>
		<td height="25" class="RightDashBoardHeader" align="right" >
				    <fmtReturnAccept:message key="IMG_ALT_HELP" var = "varHelp"/>
				    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
	</tr>	
	<%
		if (!strMsg.equals("")) {
	%>
	<tr>
		<td  align="center" colspan="2"> <font color="red"><b><%=strMsg%></b></font><br/>
		<%=strIHMsg %>
		</td>
	</tr>
	<%
		}
	%>
	
	<tr>
		<td height="1" colspan="2"><jsp:include
			page="/operations/returns/GmIncludeReturnHeader.jsp">
			<jsp:param name="hMode" value="<%=strHMode%>" />
		</jsp:include></td>
	</tr>
	<%
		if (strhAction.equals("LoadReconf")
					|| strhAction.equals("ReloadReconf")
					|| strhAction.equals("SaveReconf")) {
	%>

	<tr>
		<td colspan="2">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">

			<tr class="ShadeRightTableCaption">
				<td height="25" colspan="2"><fmtReturnAccept:message key="LBL_RECONFIGURE"/></td>
			</tr>
			<tr>
				<td bgcolor="#666666" height="1" colspan="2"></td>
			</tr>
			<tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
				<td class="RightTableCaption" align="right" height="25" width="12%"><fmtReturnAccept:message key="LBL_SET"/>:</td>
				<td valign="middle">&nbsp;<gmjsp:dropdown controlName="Cbo_SetID"
					seletedValue="<%=strSelectedSetID%>" value="<%= alSets%>"
					codeId="ID" codeName="IDNAME" defaultValue="[Choose One]" /></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#cccccc" colspan=2></td>
			</tr>
		</table>
		</td>
	</tr>
	<%
		}
	%>
	<tr>
		<td colspan="2">
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr class=Shade>
				<td class="RightTableCaption" align="right" height="25"><fmtReturnAccept:message key="LBL_RELOAD_CONTROL_FORM"/>:</td>
				<td valign="middle">&nbsp;<input type="text" name="Txt_ConsgID"
					class="InputArea"> <a
					href=javascript:fnCallTransactionSearch();><img
					src="<%=strImagePath%>/action-detail.gif" style="border: none;" height="14"></a></td>
			</tr>
			<tr>
				<td height="1" bgcolor="#cccccc" colspan=2></td>
			</tr>
			<tr>
				<td class="RightTableCaption" align="right" height="25"><fmtReturnAccept:message key="LBL_ADD_PARTS"/>
				:</td>
				<td>&nbsp;<input type="text" name="Txt_PartNums" size="40"
					value='<%=strPartNums%>' <%=strLoadDisabled%> class="InputArea">&nbsp;&nbsp;&nbsp;
					<%
							String strDisableBtnVal = strLoadDisabled;
						if(strDisableBtnVal.equals("disabled"))
						{
							strDisableBtnVal = "true";
						}
					%>
					<fmtReturnAccept:message key="BTN_LOAD" var="varLoad"/>
					<gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" name="Btn_Load"
					disabled="<%=strDisableBtnVal%>" onClick="fnReload();" buttonType="Load" /></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
	<%if(!strControlNoScan.equals("")){ %>
		<tr>
		<td colspan="2">	
			<jsp:include page="/operations/itemcontrol/GmControlNoInclude.jsp" >
			<jsp:param name="FORMNAME" value="frmVendor" />
			</jsp:include>
		</td>
     	</tr>
     <%} %>
	<%
		if (!strOrderID.equals("")) {
	%>
	<tr>
		<td align="center" class="RightTextBlue" colspan="2"><fmtReturnAccept:message key="LBL_CANT_ADD"/>
		</td>
	</tr>
	<%
		}
	%>
	<%
		if (strhAction.equals("EditLoad")
					|| strhAction.equals("EditLoadDash")
					|| strhAction.equals("Reload")
					|| strhAction.equals("Save")
					|| strhAction.equals("ReloadReconf")
					|| strhAction.equals("SaveReconf")) {
	%>
	<tr>
		<td colspan="2"><jsp:include
			page="/operations/returns/GmIncludeReturnReceivePart.jsp">
			<jsp:param name="hAction" value="<%=strhAction%>" />
		</jsp:include></td>
	</tr>
	<%
		if (request.getAttribute("PSIZE") != null) {
					strDisabled = "true";
				}
	%>
	<tr>
		<td height="30" align="center" colspan="2"><fmtReturnAccept:message key="BTN_VOID" var="varVoid"/>
		<gmjsp:button
			name="Btn_Void" value="${varVoid}" gmClass="button"
			onClick="fnVoidReturn();" buttonType="Save" />&nbsp;&nbsp;
			<fmtReturnAccept:message key="BTN_SUBMIT" var="varSubmit"/> <gmjsp:button
			name="Btn_Submit" value="${varSubmit}" gmClass="button" disabled="<%=strDisabled%>"
			onClick="fnSubmit();" buttonType="Save" /></td>
	</tr>
	<%
		}
	%>
	<tr>
		<td colspan="2">
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
			<jsp:include page="/common/GmRuleDisplayAjaxInclude.jsp"/>
		</td>
	</tr>
<%if(!strhAction.equals("LoadReconf")){ %>
	<tr>
	<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<td colspan="4">
				<jsp:include page="/operations/itemcontrol/GmValidationDisplayInclude.jsp">
				<jsp:param value="<%=strErrorCnt%>" name="errCnt"/>
				</jsp:include>
		</td>
	</tr>
<%} %>
</table>

</FORM>
<%@include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
