<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.List"%>

<%@ taglib prefix="fmtTagReconciliation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\returns\GmTagReconciliation.jsp -->

<fmtTagReconciliation:setLocale value="<%=strLocale%>"/>
<fmtTagReconciliation:setBundle basename="properties.labels.operations.returns.GmTagReconciliation"/>

<bean:define id="rdReport" name="frmTagReconciliation" property="rdReport" type="org.apache.commons.beanutils.RowSetDynaClass"></bean:define>
 
<%

GmServlet gm = new GmServlet();
gm.checkSession(response,session);

List rdReportList = rdReport.getRows();
int listSize = 0;
if(rdReportList!=null){
	listSize = rdReportList.size();
	
}

String strWikiTitle = GmCommonClass.getWikiTitle("MISSING_TAG_REPORT");
String strApplDateFmt = strGCompDateFmt;
String strRptFmt = "{0,date,"+strApplDateFmt+"}";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Tag History</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>


function fnLoad()
{
	var companyObj = document.frmTagReconciliation.companyInfo.value;	
	document.frmTagReconciliation.strOpt.value='Load';
	document.frmTagReconciliation.action = "/gmTagReconciliation.do?companyInfo="+companyObj;
	document.frmTagReconciliation.inputString.value='';
	fnStartProgress();
	document.frmTagReconciliation.submit();
}

function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnLoad();
		}
}
function fnViewReturns(val)
{
	windowOpener('/GmPrintCreditServlet?hAction=VIEW&hRAID='+val,"Returns","resizable=yes,scrollbars=yes,top=150,left=200,width=660,height=400");
}
function fnPrintConsignVer(val)
{
	windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}	
function fnSubmit(){

	var listSize = '<%=listSize%>';
	var inputString='';
	var strExNum ='';
	var strRAID = '';
	for(var i=0;i<listSize;i++){
		if(eval("document.frmTagReconciliation.cbo_reason"+i).value==54002 && 
				(eval("document.frmTagReconciliation.hexcessid"+i).value==undefined ||eval("document.frmTagReconciliation.hexcessid"+i).value=='')){
			strExNum = strExNum + '<LI>' + eval("document.frmTagReconciliation.hraid"+i).value;
			
		}
		if(eval("document.frmTagReconciliation.tagID"+i).value!=undefined && eval("document.frmTagReconciliation.tagID"+i).value.length>0
				&& eval("document.frmTagReconciliation.cbo_reason"+i).value!=0){
			strRAID = strRAID + '<LI>' + eval("document.frmTagReconciliation.hraid"+i).value;
		}
	}
	if(strRAID.length > 0){
		Error_Details(Error_Details_Trans(message_operations[575],strRAID));
	}
	if(strExNum.length > 0){
		Error_Details(Error_Details_Trans(message_operations[576],strExNum));
	}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	for(var i=0;i<listSize;i++){
			var htagid = eval("document.frmTagReconciliation.tagID"+i).value;
			var hreason = eval("document.frmTagReconciliation.cbo_reason"+i).value;
			
			if(TRIM(htagid).length>0 || (TRIM(hreason).length>0 && hreason!=0)){
				if(hreason==0)
					hreason='';
				inputString =inputString+ eval("document.frmTagReconciliation.hraid"+i).value+'^'+
							  eval("document.frmTagReconciliation.htypeId"+i).value+'^'+
							  htagid+'^'+
							  eval("document.frmTagReconciliation.hexcessid"+i).value+'^'+
							  eval("document.frmTagReconciliation.hpnum"+i).value+'^'+
							  eval("document.frmTagReconciliation.hctrlnum"+i).value+'^'+
							  hreason+'^'+
							  eval("document.frmTagReconciliation.hsetId"+i).value+'^'+
							  eval("document.frmTagReconciliation.hdistId"+i).value+'^'+
							  eval("document.frmTagReconciliation.hQty"+i).value+'^|';
				
			}
		}
	var companyObj = document.frmTagReconciliation.companyInfo.value;	
	if(inputString.length>0){
		//alert('inputString:'+inputString);
		document.frmTagReconciliation.inputString.value=inputString;
		// PC-5380 - encode URI added
		document.frmTagReconciliation.action = "/gmTagReconciliation.do?companyInfo="+encodeURIComponent(companyObj);		
		document.frmTagReconciliation.strOpt.value='Save';
		document.frmTagReconciliation.submit();
	}

}
function fnOpenRALog(id,type)
{
	windowOpener("/GmCommonLogServlet?hType="+type+"&hID="+id,"MissingReconLog","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=300");
}
function fnChangeStatus(){
	var status = document.frmTagReconciliation.statusID.value;
	if(status == 4581  && SwitchUserFl != 'Y'){ // Reconciled - 4581
		document.frmTagReconciliation.save.disabled = false;
	}else{
		document.frmTagReconciliation.save.disabled = true;
	}  
}

function fnDisHiddenField(){
	var form = document.getElementsByName('frmTagReconciliation')[0];
	//alert(form.length);
	for (var i=0;i<form.length;i++) {
		var obj = form.elements[i];
		var objName = obj.name;
		var objType = obj.type;
		var objValue = obj.value;
		
		if(objType == 'hidden')
		{
			obj.disabled = true;
		}
	}
	document.frmTagReconciliation.inputString.disabled = false;
	document.frmTagReconciliation.strOpt.disabled = false;
}
</script>
</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" onload="fnChangeStatus();fnDisHiddenField();">
 
<html:form  action="/gmTagReconciliation.do"  >
<html:hidden property="inputString"/>
<html:hidden property="strOpt" />


	<table border="0" class="DtTable1200"  cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="5" height="25" class="RightDashBoardHeader">
				<fmtTagReconciliation:message key="LBL_MISSING_TAG_REPORT"/>
			</td>
			<td  height="25" class="RightDashBoardHeader">
			<fmtTagReconciliation:message key="IMG_ALT_HELP" var = "varHelp"/>
			<img align="right" id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td colspan="6" class="Line" height="1"></td></tr>
		<!-- Struts tag lib code modified for JBOSS migration changes -->		
		<tr  class="shade" >
			<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtTagReconciliation:message key="LBL_RA_ID"/>:</td>
			<td align="left">&nbsp;<html:text property="raID"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
			<td height="30"  class="RightTableCaption" align="Right">&nbsp;<fmtTagReconciliation:message key="LBL_PART_NUMBER"/>:</td>
			<td align="left">&nbsp;<html:text property="partNum"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
			<td colspan="2"> </td>
		</tr>
		<tr><td colspan="6" class="Line" height="1"></td></tr>
		<tr>
			<td height="30"  class="RightTableCaption" align="right">&nbsp; <fmtTagReconciliation:message key="LBL_SET_ID"/>:&nbsp;</td>
			<td align ="left">&nbsp;<html:text property="setID"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>		
			<td height="30"  class="RightTableCaption" align="Right">&nbsp;<fmtTagReconciliation:message key="LBL_EXCESS_TRANSACTION"/> #:</td>
			<td align ="left">&nbsp;<html:text property="excessID"  size="15" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
			<td colspan="2"> </td>
		</tr>
		<tr><td colspan="6" class="Line" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  class="shade" >
		<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtTagReconciliation:message key="LBL_DISTRIBUTOR"/> :&nbsp;</td>
		<td   align ="left">&nbsp;<gmjsp:dropdown controlName="distID" SFFormName="frmTagReconciliation" SFSeletedValue="distID" 
							SFValue="alDistList" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]"  /></td>
		<td height="30"  class="RightTableCaption" align="right">&nbsp;<fmtTagReconciliation:message key="LBL_STATUS"/> :&nbsp;</td>
		<td   align ="left">&nbsp;<gmjsp:dropdown controlName="statusID" SFFormName="frmTagReconciliation" SFSeletedValue="statusID" 
							SFValue="alStatus" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" onChange="fnChangeStatus(this);"  /></td>
		<td colspan="2">
		<fmtTagReconciliation:message key="BTN_LOAD" var="varLoad"/>
		<gmjsp:button name="load" value = "&nbsp;${varLoad}&nbsp;"  gmClass="button" onClick="javascript:fnLoad()" buttonType="Load" /></td>
		</tr>		
		</table>
		<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
	    <tr>
			<td colspan="6" width="100%">
				<display:table name="requestScope.frmTagReconciliation.rdReport.rows"  freezeHeader="true" class="its" requestURI="/gmTagReconciliation.do" id="currentRowObject" 
				decorator="com.globus.operations.returns.displaytag.beans.DTTagReconciliationWrapper" export="true"> 
                <display:setProperty name="export.excel.filename" value="Missing Tag Reconciliation.xls" />     
 				<fmtTagReconciliation:message key="DT_RA-ID" var="varRAID"/>
 				<display:column property="RAID" style="width:160px" sortable="true" title="${varRAID}" class="alignleft"/>
 				<fmtTagReconciliation:message key="DT_TYPE" var="varType"/>
 				<display:column property="TYPE" style="width:140;" sortable="true" title="${varType}" class="alignleft" />
 				<fmtTagReconciliation:message key="DT_SET_ID" var="varSetID"/>
 				<display:column property="SETID" style="width:50;" sortable="true" title="${varSetID}" class="alignleft" />
 				<fmtTagReconciliation:message key="DT_EXCESS_TRANS" var="varExTrans"/>
 				<display:column property="EXCESS_ID" style="width:100;" sortable="true" title="${varExTrans}<br>" class="alignleft" />
				<fmtTagReconciliation:message key="DT_PART_#" var="varPart"/>
				<display:column property="PNUM" style="width:70;" title="${varPart}"  sortable="true" class="alignleft" />
				<fmtTagReconciliation:message key="DT_PART_DES" var="varPartDes"/>
				<display:column property="PDESC"  escapeXml="true" title="${varPartDes}"  sortable="true" style="width:160;" class="alignleft" />
				<fmtTagReconciliation:message key="DT_CONTROL" var="varControl"/>
				<display:column property="CTRL_NUM" style="width:50;" title="${varControl}"   class="alignleft" />
				<fmtTagReconciliation:message key="DT_DIST" var="varDist"/>
				<display:column property="DIST_NAME" style="width:130;" title="${varDist}" sortable="true"   class="alignleft" />				
				<fmtTagReconciliation:message key="DT_DATE_RETURNED" var="varDtReturn"/>
				<display:column property="RDATE" style="width:80;" title="${varDtReturn}" sortable="true"   class="alignleft" format="<%=strRptFmt%>"/>
				<fmtTagReconciliation:message key="DT_LAST_UPDATED" var="varLastup"/>
				<display:column property="LASTUPNAME" title="${varLastup}" style="width:130;" class="alignleft" sortable="true"  />
				<fmtTagReconciliation:message key="DT_TAG_ID" var="varTagID"/>
				<display:column property="TAGID" title="${varTagID}" style="width:50;" class="alignleft" media="html" />
				<fmtTagReconciliation:message key="DT_MISS_REASON" var="varMissReason"/>
				<display:column property="REASON" title="${varMissReason}" style="width:50;" class="alignleft" media="html" />
				<display:column property="MISSING_REASON" title="${varMissReason}" style="width:50;" class="alignleft" media="excel" />
				</display:table>
			</td>
		</tr>
		<% if(listSize>0){ %> 
		<tr><td   colspan="2" align ="center">
		<fmtTagReconciliation:message key="BTN_SUBMIT" var="varSubmit"/>
		<gmjsp:button name="save" value = "&nbsp;${varSubmit}&nbsp;"  gmClass="button" onClick="javascript:fnSubmit()" buttonType="Save" /></td>
		<%} %>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>