 <%
/**********************************************************************************
 * File		 		: GmCLRInitiate.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Vprasath 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file ="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.Date"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations" %>
<%@include file="/common/GmCommonInclude.jsp" %>
<%@ taglib prefix="fmtCLRInitiate" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<!-- GmCLRInitiate.jsp -->
<fmtCLRInitiate:setLocale value="<%=strLocale%>" />
<fmtCLRInitiate:setBundle
	basename="properties.labels.operations.returns.GmCLRInitiate"/>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
try {

	int intDivHeight = 400;	
    String strDivStyle = "";

	//List Box
	ArrayList alDistList = new ArrayList();
    alDistList = (ArrayList)request.getAttribute("DISTLIST");
    ArrayList alReasonList = new ArrayList();
    alReasonList = (ArrayList)request.getAttribute("REASONLIST");
    
    //Result Data    
    ArrayList alRules = new ArrayList();
    alRules = (ArrayList)request.getAttribute("RULES");
    HashMap hmRADetails = new HashMap();
    RowSetDynaClass rPendingTxns = (RowSetDynaClass)request.getAttribute("PENTXNS");
    hmRADetails = (HashMap)request.getAttribute("RADETAILS");
    String strRAID = "";
    String strMessage = "";
    
	//Parameters in Request
	String strDistID="";
	String 	strOpt="";	
	String strAction="";
	String strReason="";
	String strEDate="";
	String strDate="";
	String strApplDateFmt = strGCompDateFmt;
	String strCurrFmt="";
	
	HashMap hmParam = (HashMap)request.getAttribute("PARAM");
	
	if(hmParam != null)
	{
		 strDistID= GmCommonClass.parseNull((String)hmParam.get("DISTID"));
		 strReason = hmParam.get("REASON") == null ? "50500" : GmCommonClass.parseNull((String)hmParam.get("REASON"));
	     strOpt	 = GmCommonClass.parseNull((String)hmParam.get("OPT"));
		 strAction = GmCommonClass.parseNull((String)hmParam.get("ACTION"));		  
		 strEDate =  GmCommonClass.parseNull((String)hmParam.get("EDATE"));
	}
	
	if(hmRADetails != null)
	{
	   strRAID =  GmCommonClass.parseNull((String)hmRADetails.get("RAID"));
    }
    
    
	 if(!strRAID.equals(""))
     {
      	strMessage = GmCommonClass.parseNull((String)hmRADetails.get("MESSAGE"));
      	strReason = strReason.equals("") ?  GmCommonClass.parseNull((String)hmRADetails.get("REASON")) : strReason;
      	strEDate =  strEDate.equals("") ?  GmCommonClass.parseNull((String)hmRADetails.get("EDATE")) : strEDate;
     }
	Date dtFrmDate = null;
    strDate = GmCommonClass.parseNull((String)request.getParameter("Txt_ExpDate"));
  
    GmCalenderOperations.setTimeZone(strGCompTimeZone);
    String strTodaysDate  = GmCalenderOperations.getCurrentDate(strGCompDateFmt);
    strDate = strDate.equals("") ? strTodaysDate : strDate;
    dtFrmDate = (Date)GmCommonClass.getStringToDate(strEDate,strApplDateFmt);
	//Currency chagne
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSign =(String)hmCurrency.get("CMPCURRSMB");
	String strApplCurrFmt = (String)hmCurrency.get("CMPCURRFMT");
	strCurrFmt = "{0,number,"+strApplCurrFmt+" }";
        
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Distributor Closure - Initiate Return</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     textarea {
	width: 60%;
	height: 60px;
	}
	
	table.DtTable1000 {
	width: 1100px;
	}
	
	a.red {
	FONT-SIZE: 11px; 
	COLOR: red; 
	FONT-FAMILY: verdana, arial, sans-serif;
	}
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmCLRInitiate.js"></script>


<script>

var edate = '<%=strEDate%>';
var todayDate = '<%=strDate%>';
var distId = '<%=strDistID%>';
var servletPath = '<%=strServletPath%>';

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnLoad();" >
<FORM name="frmCLRValidate" method="POST" action="<%=strServletPath%>/GmInitClosureServlet">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hRAID" value="<%=strRAID%>">
<input type="hidden" name="hPartID">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtCLRInitiate:message key="LBL_INITIATE_RETURN" /> </span>			
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		
		<tr><td>
			 <table>
<%
			if(!strRAID.equals(""))
			 {
%>
			 <tr><td width="20%" align="right"><b><fmtCLRInitiate:message key="LBL_RA_ID" />: </b></td>
			 <td align="left">&nbsp;<b> <%=strRAID%> </b></td> </tr>
<%
			 }
%>			 
			<!-- Custom tag lib code modified for JBOSS migration changes -->			 
			 <tr><td width="20%" align="right"><b><fmtCLRInitiate:message key="LBL_DISTRIBUTOR" /> : </b></td>
				<td width="35%">
				&nbsp;<gmjsp:dropdown controlName="Cbo_DistId"  seletedValue="<%= strDistID%>" 	
					  value="<%=alDistList%>" codeId = "CODEID" codeName = "CODENM" 
					defaultValue= "[Choose One]"  />
         </td>
         <td width="45%" align="left"><fmtCLRInitiate:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnReload();" buttonType="Load" /></td>
         </tr>
        </table></td></tr> 
        <tr><td>
			 <table>
			 <tr><td width="20%" align="right"><b><fmtCLRInitiate:message key="LBL_REASON" />: </b></td>
				<td width="80%">
				&nbsp;<gmjsp:dropdown controlName="Cbo_Reason"  seletedValue="<%= strReason%>" 	
					 value="<%=alReasonList%>" codeId = "CODEID" codeName = "CODENM" 
					  />
         </td>        
         </tr>
        </table></td></tr> 
        <tr><td>
			 <table>
			 <tr><td width="20%" align="right"><b><fmtCLRInitiate:message key="LBL_EXPECTED_DATE_OF_RETURN" />: </b></td>
				<td width="80%">
				 &nbsp;<gmjsp:calendar textControlName="Txt_EDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp; 
			</td>
		 </td>         
         </tr>
        </table></td></tr> 
         <tr><td class="Line" height="1"></td></tr>
<%        if(!strMessage.equals(""))
			{
%>
		        <tr><td><table>
		                <tr><td align="center" class="RightTextBrown"><b><%=strMessage%></b></td></tr>		                
		        </table></td></tr>
<%
			}
%>         
    
        <tr>
         <td>  
        
			 <display:table name="requestScope.PENTXNS.rows" class="its" id="currentRowObject"  varTotals="totals"   decorator="com.globus.displaytag.beans.DTProcessReturnWrapper" freezeHeader="true" paneSize="180"> 
			 <fmtCLRInitiate:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" sortable="true" class="alignleft"/>
			  <fmtCLRInitiate:message key="DT_PART_DESC" var="varPartDesc"/><display:column property="PARTDESC" title="${varPartDesc}" class="alignleft"/>
			  <fmtCLRInitiate:message key="DT_VALUE" var="varValue"/><display:column property="PRICE" title="${varValue}" sortable="true"  class="alignright"  format="<%=strCurrFmt%>" />
			  <fmtCLRInitiate:message key="DT_CONSIGNED_QTY" var="varConsignedQty"/><display:column property="CONS_QTY" title="${varConsignedQty}"  sortable="true"   class="alignright" />
			  <fmtCLRInitiate:message key="DT_RETURN_QTY" var="varReturnQty"/><display:column property="RET_QTY" title="${varReturnQty}" sortable="true"    class="alignright" />
			  <fmtCLRInitiate:message key="DT_PENDING_RETURN" var="varPendingReturn"/><display:column property="PENDING_QTY" title="${varPendingReturn}" sortable="true" class="alignright"  />
			  <fmtCLRInitiate:message key="DT_PENDING_RETURN_VALUE" var="varPendingReturnValue"/><display:column property="TOTAL_VALUE" title="${varPendingReturnValue}" sortable="true"   class="alignright" total="true" format="<%=strCurrFmt%>"   />
			 <display:footer media="html"> 
<% 
					String strVal ;
					strVal = ((HashMap)pageContext.getAttribute("totals")).get("column7").toString();
					String strTotal =  GmCommonClass.getStringWithCommas(strVal);
			
%>
				  	<tr class = shade>
				  	    <td colspan="6" class = "alignright"><b><fmtCLRInitiate:message key="LBL_TOTAL" />: </b></td>
		    	 		<td class = "alignright"><B> <%=strTotal%></B></td>
				   </tr>
	 	 </display:footer>
       </display:table>
		 
         </td>
         </tr>     
               
         <tr><td class="Line" height="1"></td></tr>
         <tr>
         <td> 
				<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="LogType" value="" />
				<jsp:param name="LogMode" value="Edit" />
				</jsp:include>
			</td>      		
		</tr>	
<%
						 HashMap hmRule = (HashMap) alRules.get(0);
%>
		   <tr>
						<td align="center" class="RightTextBrown" height="30"><fmtCLRInitiate:message key="LBL_NOTIFIED" />
						<br><b>
						<%=hmRule.get("EMAILTO")%>
						</b>
						</td>
			</tr>
			 <tr><td class="Line" height="1"></td></tr>
			 	<% 
					String strPrint = "fnPrint('"+strRAID+"')";
				%>
			<tr>
						<td height="30"align="right">
<%
		  if(!strRAID.equals(""))
    		{
%>			<fmtCLRInitiate:message key="BTN_PRINT" var="varPrint"/>
		 <gmjsp:button value="&nbsp;${varPrint}&nbsp;" gmClass="button" onClick="<%=strPrint%>" buttonType="Load" />&nbsp;&nbsp;
<%
			}
		  else if(rPendingTxns.getRows().size() > 0) {	
%>         <fmtCLRInitiate:message key="BTN_INITIATE" var="varInitiate"/>  
		   <gmjsp:button name="initiate" value="&nbsp;${varInitiate}&nbsp;" gmClass="button" onClick="fnInitiate();" buttonType="Save" />&nbsp;&nbsp;
<%
			}	
%>		   
						</td>
			</tr>
			
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
