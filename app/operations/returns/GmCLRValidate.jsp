 <%
/**********************************************************************************
 * File		 		: GmCLRValidate.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: Vprasath 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<!-- WEB-INF path corrected for JBOSS migration changes -->
						
<%@ include file="/common/GmCommonInclude.jsp" %>
<%@ taglib prefix="fmtCLRValidate" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
 <!-- GmCLRValidate.jsp-->
<fmtCLRValidate:setLocale value="<%=strLocale%>"/>
<fmtCLRValidate:setBundle basename="properties.labels.operations.returns.GmCLRValidate"/>


<%
try {

	//List Box
	ArrayList alDistList = new ArrayList();
    alDistList = (ArrayList)request.getAttribute("DISTLIST");
   
   //Result Data Attributes
	HashMap hmOpenTxns = new HashMap();
	ArrayList alOpenConsg = new ArrayList();
	ArrayList alOpenRet = new ArrayList();
	ArrayList alOpenTrans = new ArrayList();
	
	int intConsLength = 0;
	int intRetLength = 0;
	int intTransLength = 0;

	hmOpenTxns = (HashMap)request.getAttribute("OPENTXNS");
	if(hmOpenTxns != null)
	{
	 alOpenConsg = (ArrayList)hmOpenTxns.get("OPENCONS");
	 intConsLength  = alOpenConsg.size();
	 alOpenRet = (ArrayList)hmOpenTxns.get("OPENRET");
	 intRetLength  = alOpenRet.size();
	 alOpenTrans = (ArrayList)hmOpenTxns.get("OPENTRANS");; 	 
	 intTransLength  = alOpenTrans.size();
	}
	
	//System.out.println("Data in hmOpenTxns"+hmOpenTxns);

	//Parameters in Request
	String strDistID="";
	String 	strOpt="";	
	String strAction="";
	String strApplDateFmt = strGCompDateFmt;
	
	strDistID= GmCommonClass.parseNull((String)request.getParameter("Cbo_DistId"));
    strOpt	 = GmCommonClass.parseNull((String)request.getParameter("hOpt"));
    strAction = GmCommonClass.parseNull((String)request.getParameter("hAction"));	
    

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Distributor Closure - Validate Return Validation</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
     textarea {
	width: 60%;
	height: 60px;
	}
	
	table.DtTable1000 {
	width: 1100px;
	}
	
	a.red {
	FONT-SIZE: 11px; 
	COLOR: red; 
	FONT-FAMILY: verdana, arial, sans-serif;
	}
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script>
function fnReload()
{
  document.frmCLRValidate.hAction.value= 'VALIDATE';
  fnStartProgress();
  document.frmCLRValidate.submit();
}

function fnCallEditConsg(val,status)
{
	if(status == "PendingShipment")
	{
		document.frmCLRValidate.hId.value = val;
		document.frmCLRValidate.hAction.value = "LoadSetShip";
		document.frmCLRValidate.action = "/GmConsignSetServlet";
		document.frmCLRValidate.submit();
	}
	else if(status == "PendingControlNumber")
	{
		document.frmCLRValidate.hConsignId.value = val;
		document.frmCLRValidate.hAction.value = "EditControl";
		document.frmCLRValidate.action = "/GmConsignItemServlet";	
		document.frmCLRValidate.submit();
	}
}

function fnCallEditRet(val,status)
{
	if(status == "Initiated")
	{
	    document.frmCLRValidate.hRAId.value = val;
	    document.frmCLRValidate.hAction.value = "EditLoadDash";
		document.frmCLRValidate.hMode.value="Process";
	    document.frmCLRValidate.action = "/GmReturnReceiveServlet";	    	    			
	    document.frmCLRValidate.submit();
	}
	else if(status == "PendingCredit")
	{
	    document.frmCLRValidate.hRAId.value = val;
		document.frmCLRValidate.hAction.value = "LoadCredit";
		document.frmCLRValidate.action = "/GmReturnCreditServlet";
		document.frmCLRValidate.submit();
	}	
}

function fnCallEditTransfer(val)
{
	document.frmCLRValidate.hTransferId.value = val;
	document.frmCLRValidate.hMode.value="Load";
	document.frmCLRValidate.action = "/GmAcceptTransferServlet";	
	document.frmCLRValidate.submit();	
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<FORM name="frmCLRValidate" method="POST" action="<%=strServletPath%>/GmInitClosureServlet">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hTransferId">
<input type="hidden" name="hMode">
<input type="hidden" name="hId">
<input type="hidden" name="hConsignId">
<input type="hidden" name="hRAId">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0" >
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtCLRValidate:message key="LBL_PROCESS_CLOSURE_HEADER"/></span>			
			</td>
		</tr>
		<tr><td class="Line" height="1"></td></tr>
		<tr><td height=5>  </td></tr>	
		  
			<tr><td>
			 <table>
			 <!-- Custom tag lib code modified for JBOSS migration changes -->
			 <tr><td width="10%" align="right"><b><fmtCLRValidate:message key="LBL_DISTRIBUTOR"/>: </b></td>
				<td width="35%">
				&nbsp;<gmjsp:dropdown controlName="Cbo_DistId"  seletedValue="<%= strDistID%>" 	
					value="<%=alDistList%>" codeId = "CODEID" codeName = "CODENM" 
					defaultValue= "[Choose One]"  />
         </td>
         <td width="55%" align="left"><fmtCLRValidate:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" gmClass="button" buttonType="Load" onClick="fnReload();"/></td>
         </tr>
        </table></td></tr> 
         <tr><td height="5"></td></tr>
     
<%    if(!strDistID.equals(""))  {  %>
        
        
         <tr>
            <td align="center">
             <table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="100%" valign="top">
				<table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="5" height="17" class="RightDashBoardHeader">
						<fmtCLRValidate:message key="LBL_OPEN_CONSIGNMENT"/>
						</td>
					</tr>
					<tr><td class="Line" colspan="5"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="17" width="20%"><fmtCLRValidate:message key="LBL_TRANS_ID"/></td>						
						<td width="20%" ><fmtCLRValidate:message key="LBL_TYPE"/></td>
						<td width="20%" align="center"><fmtCLRValidate:message key="LBL_DATE"/></td>
						<td width="20%" align="center"><fmtCLRValidate:message key="LBL_STATUS"/></td>
						<td width="20%"><fmtCLRValidate:message key="LBL_SET_ID"/></td>
					</tr>
					<tr><td class="Line" colspan="5"></td></tr>
<%			if( intConsLength > 0) {
							HashMap hmOpenConsignment = new HashMap();
							for (int k = 0;k < intConsLength ;k++ )
							{
								hmOpenConsignment = (HashMap)alOpenConsg.get(k);

								String strID = GmCommonClass.parseNull((String)hmOpenConsignment.get("ID"));
								String strType = GmCommonClass.parseNull((String)hmOpenConsignment.get("TYPE"));
								String strDate = GmCommonClass.getStringFromDate((java.util.Date) hmOpenConsignment.get("CDATE"), strApplDateFmt);
								String strStatus = GmCommonClass.parseNull((String)hmOpenConsignment.get("STATUS"));
								String strSetID = GmCommonClass.parseNull((String)hmOpenConsignment.get("SETID"));
%>
					<tr>
					<td HEIGHT="20" class="RightText">&nbsp;<a class=RightText href= javascript:fnCallEditConsg('<%=strID%>','<%=strStatus.replaceAll(" ","")%>');><%=strID%></td>						
						<td class="RightText"><%=strType%></td>
						<td class="RightText" align="center"><%=strDate%></td>
						<td class="RightText" align="center"><%=strStatus%></td>
						<td class="RightText">&nbsp;<%=strSetID%></td>						
					</tr>
					<tr><td colspan="5" bgcolor="#eeeeee" height="1"></td></tr>
<%						}
						}else{
%>												
					<tr>
						<td colspan="5" align="center" class="RightTextBlue" height="30"><fmtCLRValidate:message key="LBL_NO_OPEN_CONSIGNMENTS"/></td>
					</tr>
<%                  }
%>					
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
	</table>
	
	<tr><td height="5"></td></tr>
	
 <tr>
            <td>
             <table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="100%" valign="top">
				<table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="5" height="17" class="RightDashBoardHeader">
						<fmtCLRValidate:message key="LBL_OPEN_RETURNS"/>
						</td>
					</tr>
					<tr><td class="Line" colspan="5"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="17" width="20%"><fmtCLRValidate:message key="LBL_TRANS_ID"/></td>						
						<td width="20%" ><fmtCLRValidate:message key="LBL_TYPE"/></td>
						<td width="20%" align="center"><fmtCLRValidate:message key="LBL_DATE"/></td>
						<td width="20%" align="center"><fmtCLRValidate:message key="LBL_STATUS"/></td>
						<td width="20%"><fmtCLRValidate:message key="LBL_SET_ID"/></td>
					</tr>
					<tr><td class="Line" colspan="5"></td></tr>
<%			if( intRetLength > 0) {
							HashMap hmOpenReturn = new HashMap();
							for (int k = 0;k < intRetLength ;k++ )
							{
								hmOpenReturn = (HashMap)alOpenRet.get(k);

								String strID = GmCommonClass.parseNull((String)hmOpenReturn.get("ID"));
								String strType = GmCommonClass.parseNull((String)hmOpenReturn.get("TYPE"));
								String strDate = GmCommonClass.getStringFromDate((java.util.Date) hmOpenReturn.get("CDATE"), strApplDateFmt);
								String strStatus = GmCommonClass.parseNull((String)hmOpenReturn.get("STATUS"));
								String strSetID = GmCommonClass.parseNull((String)hmOpenReturn.get("SETID"));
%>
					<tr>
					<td HEIGHT="20" class="RightText">&nbsp;<a class=RightText href= javascript:fnCallEditRet('<%=strID%>','<%=strStatus.replaceAll(" ","")%>');><%=strID%></td>						
						<td class="RightText" ><%=strType%></td>
						<td class="RightText" align="center"><%=strDate%></td>
						<td class="RightText" align="center"><%=strStatus%></td>
						<td class="RightText">&nbsp;<%=strSetID%></td>						
					</tr>
					<tr><td colspan="5" bgcolor="#eeeeee" height="1"></td></tr>
<%						}
						}else{
%>												
					<tr>
						<td colspan="5" align="center" class="RightTextBlue" height="30"><fmtCLRValidate:message key="LBL_NO_OPEN_RETURNS"/></td>
					</tr>
<%                  }
%>					
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
	<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
	</table>
		
		<tr><td height="5"></td></tr>

 <tr>
            <td>
             <table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td width="100%" valign="top">
				<table border="0" width="100%" align="center" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="4" height="17" class="RightDashBoardHeader">
						<fmtCLRValidate:message key="LBL_OPEN_TRANSFERS"/>
						</td>
					</tr>
					<tr><td class="Line" colspan="4"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td HEIGHT="17" width="25%"><fmtCLRValidate:message key="LBL_TRANS_ID"/></td>						
						<td width="25%" ><fmtCLRValidate:message key="LBL_TYPE"/></td>
						<td width="25%" align="center"><fmtCLRValidate:message key="LBL_DATE"/></td>
						<td width="25%" align="center"><fmtCLRValidate:message key="LBL_STATUS"/></td>
					</tr>
					<tr><td class="Line" colspan="4"></td></tr>
<%			if( intTransLength > 0) {
							HashMap hmOpenTransfer = new HashMap();
							for (int k = 0;k < intTransLength ;k++ )
							{
								hmOpenTransfer = (HashMap)alOpenTrans.get(k);

								String strID = GmCommonClass.parseNull((String)hmOpenTransfer.get("ID"));
								String strType = GmCommonClass.parseNull((String)hmOpenTransfer.get("TYPE"));
								
								String strDate = GmCommonClass.getStringFromDate((java.util.Date) hmOpenTransfer.get("CDATE"), strApplDateFmt);

								String strStatus = GmCommonClass.parseNull((String)hmOpenTransfer.get("STATUS"));
%>
					<tr>
					<td HEIGHT="20" class="RightText">&nbsp;<a class=RightText href= javascript:fnCallEditTransfer('<%=strID%>');><%=strID%></td>						
						<td class="RightText" ><%=strType%></td>
						<td class="RightText" align="center"><%=strDate%></td>
						<td class="RightText" align="center"><%=strStatus%></td>					
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee" height="1"></td></tr>
<%						}
						}else{
%>												
					<tr>
						<td colspan="4" align="center" class="RightTextBlue" height="30"><fmtCLRValidate:message key="LBL_NO_OPEN_TRANSFERS"/></td>
					</tr>
<%                  }
%>					
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
	<tr>
			<td colspan="3" bgcolor="#666666"></td>
		</tr>
	</table>
	
			<tr>
						<td colspan="4" align="center" class="RightTextBrown" height="30"><fmtCLRValidate:message key="LBL_CLOSED_BEFORE_INITIATING_DISTRIBUTOR_CLOSURE"/>
						</td>
			</tr>
			
            </td>
         </tr>
<% } %>         		
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
