<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %> 

<%@ taglib prefix="fmtIncludeReturnHeader" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<!-- GmIncludeReturnHeader.jsp -->
<fmtIncludeReturnHeader:setLocale value="<%=strLocale%>" />
<fmtIncludeReturnHeader:setBundle
	basename="properties.labels.operations.returns.GmIncludeReturnHeader" />
<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	 strImagePath = GmCommonClass.getString("GMIMAGES");
	 strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	HashMap hmConsignDetails = new HashMap();
	hmConsignDetails = (HashMap)request.getAttribute("HMRADETAILS");
	String strhMode = GmCommonClass.parseNull(request.getParameter("hMode"));
	String strHAction = GmCommonClass.parseNull(request.getParameter("hAction"));
	String strApplDateFmt = strGCompDateFmt;
	//System.out.println("strhMode : "+strhMode);
	
	HashMap hmReturnDetails = (HashMap)request.getAttribute("hmReturnDetails");
	String strChildRAID = "";
	
	if(hmConsignDetails == null)
	{
	 HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	 	if( hmReturn != null)
	 	 {
			 hmConsignDetails = (HashMap)hmReturn.get("RADETAILS");
		 } 	 
	}
	
	if(hmReturnDetails !=null)
	{
	 	strChildRAID = GmCommonClass.parseNull((String)hmReturnDetails.get("CHILDRAID"));	 	 
	}
	
	 
	
	

	String strRAId = GmCommonClass.parseNull((String)request.getParameter("hRAId"));
	String strDistID = "";
	String strSetName= "";
	String strDistName = "";
	String strUserName = "";
	String strDisabled = "";
	String strLogType = "";
	String strParentLogType = "";
	
	String strLoanTo = "";
	String strLoanToName = ""; 
	
	String strSessDate = (String)request.getAttribute("hRetDate");
	if (strSessDate == null)
	{
		strSessDate = (String)session.getAttribute("strSessTodaysDate");
	}

	strSessDate = strSessDate==null?"":strSessDate;
	
	java.sql.Date dtRetDate = null;
	java.sql.Date dtExpDate = null;
	java.sql.Date dtSysDate = null;
	String strType = "";
	String strTypeId = "";
	String strRefId = "";
	String strReason = "";
	java.sql.Date dtIniDate = null;
	String strOrderId = "";
	String strAccName = "";
	String strStatusFlag ="";
	String strComments = "";
	String strPRAID="";
	String strParentRAID = GmCommonClass.parseNull((String)request.getParameter("hParentRAId"));
	String strRepName="";
	String strRAPartNums = "";
	
	if (hmConsignDetails != null)
	{
				strRAId = GmCommonClass.parseNull((String)hmConsignDetails.get("RAID"));
				strPRAID = GmCommonClass.parseNull((String)hmConsignDetails.get("PRAID"));
				strDistID = GmCommonClass.parseNull((String)hmConsignDetails.get("DISTID"));
				strSetName= GmCommonClass.parseNull((String)hmConsignDetails.get("SNAME"));
				strLoanTo = GmCommonClass.parseNull((String)hmConsignDetails.get("LOANTO"));
				strLoanToName = GmCommonClass.parseNull((String)hmConsignDetails.get("LOANTOID"));
				strDistName = GmCommonClass.parseNull((String)hmConsignDetails.get("DNAME"));
				strAccName = GmCommonClass.parseNull((String)hmConsignDetails.get("ANAME"));
				dtRetDate = (java.sql.Date)hmConsignDetails.get("RETDATE");
				dtSysDate = (java.sql.Date)hmConsignDetails.get("SYSDT");
				dtRetDate=(dtRetDate==null?dtSysDate:dtRetDate);
				//strRetDate = strRetDate.equals("")?strSessDate:strRetDate;
				strDistName = strDistName.equals("")?strAccName:strDistName;
				strStatusFlag = GmCommonClass.parseNull((String)hmConsignDetails.get("SFL"));
				strType = GmCommonClass.parseNull((String)hmConsignDetails.get("TYPE"));
				strTypeId = GmCommonClass.parseNull((String)hmConsignDetails.get("TYPEID"));
				strRefId = GmCommonClass.parseNull((String)hmConsignDetails.get("REFID"));
				strReason = GmCommonClass.parseNull((String)hmConsignDetails.get("REASON"));
					dtIniDate = (java.sql.Date)hmConsignDetails.get("CDATE");
				strUserName = GmCommonClass.parseNull((String)hmConsignDetails.get("CUSER"));
				strOrderId = GmCommonClass.parseNull((String)hmConsignDetails.get("ORDID"));
				dtExpDate = (java.sql.Date)hmConsignDetails.get("EDATE");
				strLogType = GmCommonClass.parseNull((String)hmConsignDetails.get("LOG"));
				strParentLogType = GmCommonClass.parseNull((String)hmConsignDetails.get("PLOG"));
				strComments = GmCommonClass.parseNull((String)hmConsignDetails.get("COMMENTS"));	
				strRepName = GmCommonClass.parseNull((String)hmConsignDetails.get("REPNAME"));
				strRAPartNums = GmCommonClass.parseNull((String)hmConsignDetails.get("RAPARTNUMS"));
			}
		if(strHAction.equals("SaveReconf"))
				{				
					strParentRAID = strRAId;
					strRAId = strChildRAID;
					
					//System.out.println("Parent RA ID:"+strParentRAID);
					//System.out.println("RA ID:"+strRAId);
				}		
				if(strHAction.equals("ReloadReconf") || strHAction.equals("LoadReconf"))
				{
				 strParentRAID = strRAId;
					strRAId = "";
				}
	
	    if(!strPRAID.equals("") && strParentRAID.equals(""))
	    {
	     strParentRAID = strPRAID;
	    }
	if (strhMode.equals("View"))			
	{
		strDisabled = "disabled";
	}

%>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var strTypeid = '<%=strTypeId%>';
var raPartNums = '<%=strRAPartNums%>';
function fnLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1216&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
</script>
<input type="hidden" name="hMode" value="<%=strhMode%>">
<input type="hidden" name="hDistID" value="<%=strDistID%>">
<input type="hidden" name="hRAId" value="<%=strRAId%>">
<input type="hidden" name="hParentRAId" value="<%=strParentRAID%>">

				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_RA_Id" />:</td>
						<td class="RightText" >&nbsp;<%=strRAId%> &nbsp; 
<%	
   if(!strRAId.equals(""))
    {   
   if (strLogType.equals("Y") )
        {
%>        <!-- BUG FIX 11704 UI Issue -->
            <img id='imgEditA' style='cursor:hand' src="<%=strImagePath%>/phone-icon_ans.gif" width='20' height='17' onClick="javascript:fnLog('<%=strRAId%>')" />
<%        }
        else
        {
%>        	<img id='imgEditA' style='cursor:hand' src="<%=strImagePath%>/phone_icon.jpg" width='20' height='17' onClick="javascript:fnLog('<%=strRAId%>')" />
<%        } 
   }
%>
						</td>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_PARENT_RA_ID" />:</td>
						<td class="RightText" >&nbsp;<%=strParentRAID%>
<%	
					   if(!strParentRAID.equals(""))
							    {   
							   if (strParentLogType.equals("Y") )
							        {
%>        
							            <img id='imgEditA' style='cursor:hand' src='<%=strImagePath%>/phone-icon_ans.gif' width='20' height='17' onClick="javascript:fnLog('<%=strParentRAID%>')" />
<%        							}
						       else	
							        {
%>        								<img id='imgEditA' style='cursor:hand' src='<%=strImagePath%>/phone_icon.jpg' width='20' height='17' onClick="javascript:fnLog('<%=strParentRAID%>')" />
<%        							} 
   								}
%>						
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr class="Shade" HEIGHT="20" >
						<td class="RightTableCaption" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_TYPE" />:</td>
						<td class="RightText" >&nbsp;<%=strType%></td>
						
						<td class="RightTableCaption" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_REASON" />:</td>
						<td class="RightText">&nbsp;<%=strReason%></td>
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr HEIGHT="20" >
					<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_INITIATED_DATE" />:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringFromDate(dtIniDate,strApplDateFmt)%></td>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_INITIATED_BY" />:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>						
					</tr>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="Shade" HEIGHT="20" >
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtIncludeReturnHeader:message key="LBL_DATE_EXPECTED" />:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringFromDate(dtExpDate,strApplDateFmt)%></td>
						<td HEIGHT="23" class="RightTableCaption" align="right"><fmtIncludeReturnHeader:message key="LBL_DATE_RETURNED" />:</td>
						<td class="RightText">&nbsp;<gmjsp:calendar controlName="Txt_RetDate" textValue="<%=(dtRetDate==null)?null:new java.sql.Date(dtRetDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
						<%-- <td class="RightText">&nbsp;<input type="text" size="10" <%=strDisabled%> value="<%=GmCommonClass.getStringFromDate(dtRetDate,strApplDateFmt)%>" name="Txt_RetDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >--%>
						</td>
					</tr>
					
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<%if(strTypeId.equals("3308") || strTypeId.equals("3309")){ %>
					<tr HEIGHT="20" >
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_LOAN_TO" />:</td>
						<td class="RightText">&nbsp;<%=strLoanTo%></td>
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_LOAN_TO_NAME" />:</td>
						<td class="RightText">&nbsp;<%=strLoanToName%></td>
					</tr>
					<%}else{ %>
						<tr HEIGHT="20" >
							<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_ACCOUNT_DISTRIBUTOR_NAME" />:</td>
							<td class="RightText">&nbsp;<%=strDistName%></td>
							<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_REP_NAME" />:</td>
							<td class="RightText">&nbsp;<%=strRepName%></td>
						</tr>					
					<%} %>
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr class="Shade" HEIGHT="20" >
						<td class="RightTableCaption" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_SET_NAME" />:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSetName)%></td>
						<%if(strTypeId.equals("3308") || strTypeId.equals("3309")){%>
							<td class="RightTableCaption" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_REF_ID" />:</td>
							<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strRefId)%></td>						
						<%}else{ %>
							<td></td>
							<td></td>
						<%} %>	
					</tr>				
					
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr>
					
						<td class="RightTableCaption" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_ORDER_ID" />:</td>
						<td class="RightText">&nbsp;<%=strOrderId%></td>	
						<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_COMPLETE" />:</td>
						<td class="RightText">&nbsp;<input type="checkbox" name="Chk_RAComplete" ></td>
					</tr>
					
					<tr><td height="1" bgcolor="#cccccc" colspan="4"></td></tr>
					<tr class="Shade">
						<td class="RightTableCaption" HEIGHT="30" align="right">&nbsp;<fmtIncludeReturnHeader:message key="LBL_COMMENTS" />:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strComments%></td>
					</tr>	
					<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>				
														
</table>

</BODY>

</HTML>
