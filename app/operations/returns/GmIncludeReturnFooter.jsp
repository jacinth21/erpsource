<%@ include file="/common/GmHeader.inc" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale="_"+strLocale;
}
%>

<%@ taglib prefix="fmtIncludeReturnFooter" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\returns\GmIncludeReturnFooter.jsp -->

<fmtIncludeReturnFooter:setLocale value="<%=strLocale%>"/>
<fmtIncludeReturnFooter:setBundle basename="properties.labels.operations.returns.GmIncludeReturnFooter"/>


<% 
String strMode = GmCommonClass.parseNull(request.getParameter("LISTMODE"));
String strPage = GmCommonClass.parseNull(request.getParameter("PAGE"));
%>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>   
<!-- GmIncludeReturnFooter.jsp. -->
			<display:table name="requestScope.rsFooterResultSet.rows"  class="its" id="currentRowObject" decorator="com.globus.displaytag.beans.DTReturnsFooterWrapper"> 
				<fmtIncludeReturnFooter:message key="DT_RETURN_ID" var="varReturnId"/>
				<display:column property="RID" title="${varReturnId}" class="aligncenter"  />
				<fmtIncludeReturnFooter:message key="DT_SET_ID" var="varSetID"/>
				<display:column property="SID" title="${varSetID}" sortable="true"  />
				<fmtIncludeReturnFooter:message key="DT_SET_NAME" var="varSetName"/>
				<display:column property="SNM" title="${varSetName}" sortable="true"  />
				<fmtIncludeReturnFooter:message key="DT_STATUS" var="varStatus"/>
				<display:column property="STATUS" title="${varStatus}" />
			  	<fmtIncludeReturnFooter:message key="DT_INITIATED_BY" var="varInitBy"/>
			  	<display:column property="INIT_BY" title="${varInitBy}" sortable="true"   />
			  	<fmtIncludeReturnFooter:message key="DT_INITAITED_DATE" var="varInitDate"/>
			  	<display:column property="INIT_DT" title="${varInitDate}"   />
			</display:table> 
			