<%
/**********************************************************************************
 * File		 		: GmExcessReturnDetails.jsp
 * Desc		 		: Excess Return Details
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Date"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtExcessReturnDetails" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<!-- GmExcessReturnDetails.jsp -->
<fmtExcessReturnDetails:setLocale value="<%=strLocale%>" />
<fmtExcessReturnDetails:setBundle basename="properties.labels.operations.returns.GmExcessReturnSummary"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strCustTransferPath = GmCommonClass.getString("GMTRANSFER");
	String strSessApplDateFmt = strGCompDateFmt;
	Date dtClosedDate = null;
	
	ArrayList alReason = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALREASON"));
	HashMap hmHeaderInfo = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMHEADERINFO"));
	HashMap hmParam = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HMPARAM"));
	log.debug(" values inside hmParam " + hmParam);

	String strAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	String strReason = GmCommonClass.parseNull((String)hmParam.get("REASON"));
	String strCloseDate = GmCommonClass.parseNull((String)hmParam.get("CLOSEDATE"));
    String strConsignId = GmCommonClass.parseNull((String)hmHeaderInfo.get("CONSIGNID"));
    String strRAID = GmCommonClass.parseNull((String)hmHeaderInfo.get("RMAID"));         
    String strType = GmCommonClass.parseNull((String)hmHeaderInfo.get("RLOG"));         
    String strDistName =  GmCommonClass.parseNull((String)hmHeaderInfo.get("DISTNAME"));
    String strCreditedBy  =  GmCommonClass.parseNull((String)hmHeaderInfo.get("CREDITEDBY"));      
	String strTransferType = GmCommonClass.parseNull((String)hmParam.get("TRANSFERTYPE"));
	String strTransferFrom = GmCommonClass.parseNull((String)hmParam.get("TRANSFERFROM"));
	boolean flag = true;
	
	dtClosedDate = GmCommonClass.getStringToDate(strCloseDate,strSessApplDateFmt);
	
	if(strReason.equals(""))
	 {
	 	 strReason = GmCommonClass.parseNull((String)hmHeaderInfo.get("REASON"));
	 	 flag = strReason.equals("50530") ? false : true;
	 }
	log.debug(" In JSP, Reason is " + strReason + "Action: " +strAction);
	// to get the company currency format
	HashMap hmCurrency = new HashMap();
	hmCurrency= GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO.getCmpid());
	String strCurrSign = GmCommonClass.parseNull((String)hmCurrency.get("CMPCURRSMB"));
	String strApplCurrFmt = GmCommonClass.parseNull((String)hmCurrency.get("CMPCURRFMT"));
	String strCurrencyFmt = "{0,number,"+strCurrSign+" "+strApplCurrFmt+"}";
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Excess Return Details </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>     
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<script>
var lblReconReason = '<fmtExcessReturnDetails:message key="LBL_RECONCILIATION_REASON" />';
</script>
<script>

function fnGo()
{
	var theinputs=document.getElementsByTagName('input');	
	var len = theinputs.length;
	var str ='';
	var partstr = '';
	var cntCheck = 0;
	
				for(var n=0;n<len;n++)
			{
					objpart = eval("document.frmTransfer.chk"+n); // The checkboxes would be like rad0, rad1 and so on
					
					if(objpart)
					{			
						part = objpart.value; // value would the CN or RA #
						tsfqty = objpart.id;
						tsfqtyint = parseInt(objpart.id);
							if (objpart.checked)
							{
										cntCheck ++;
										str = str +  part +'^'+tsfqty+ '|'; 
										partstr = partstr + part +',';
							}
					}
		  	}
		  		if (cntCheck == 0)
		  		{
		  			Error_Details(message[10026]);
		  		}
		  		fnValidateDropDn('Cbo_Reason',lblReconReason);
				document.frmTransfer.hInputStr.value = str;
				document.frmTransfer.hPartInputStr.value = partstr;		
				
				
			
			   if(document.frmTransfer.Cbo_Reason.value == "50530")
			    {
					var varTsfFrom = document.frmTransfer.hTransferFrom.value;
					var varTsfTo = document.frmTransfer.Cbo_TransferTo.value;
					var varTsfReason = document.frmTransfer.Cbo_TransferReason.value;
					
					//alert(varTsfReason+", "+ varTsfTo);
					
					if (varTsfFrom == varTsfTo)
					{
								Error_Details(message[726]);
					}
					
					if (varTsfFrom == "")
					{
								Error_Details(message[716]);
					}	
					if(varTsfReason == "0")
					{
								Error_Details(message[723]);
					}	
				}
				fnSubmit();		  	

}

function fnFetchReason()
{
	document.frmTransfer.submit();
}


function fnPrintReturnVer(val)
{
	windowOpener("/GmReportCreditsServlet?hAction=PrintVersion&hId="+val,"Con1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnLog(id)
{
	windowOpener("/GmCommonLogServlet?hType=1216&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

function fnSubmit()
{
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}	
     document.frmTransfer.hAction.value = "Save";
     fnStartProgress('Y');
	 document.frmTransfer.submit();
}
	
</script>	

<BODY leftmargin="20" topmargin="10" >
<FORM name="frmTransfer" method="POST" action="<%=strServletPath%>/GmExcessReturnDetailsServlet" >
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hRAID" value="<%=strRAID%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hPartInputStr" value="">
		<input type="hidden" name="hTransferFrom" value="<%=strTransferFrom%>">

	<table border="0"  class="DtTable850" cellspacing="0" cellpadding="0">
	<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
				<td colspan="4" height="25" class="RightDashBoardHeader"><fmtExcessReturnDetails:message key="LBL_CONSIGNMENT_RECONCILIATION_DETAILS" /></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
		<tr>
			<td height="25" class="RightTableCaption" align="right"><fmtExcessReturnDetails:message key="LBL_CONSIGNMENT" /> :</td>
			<td >&nbsp; <%=strConsignId%>
			</td>
				<td height="25" class="RightTableCaption" align="right"><fmtExcessReturnDetails:message key="DT_RA_ID" /> :</td>
			<td>&nbsp;<a href= javascript:fnPrintReturnVer('<%=strRAID%>')><%=strRAID%></a>
				&nbsp;&nbsp;
<%				 if (strType.equals("Y") )
        {
%>        
            <img id='imgEditA' style='cursor:hand' src='<%=strImagePath%>/phone-icon_ans.gif' width='20' height='17'onClick="javascript:fnLog('<%=strRAID%>')" />
<%        }
        else
        {
%>        
			<img id='imgEditA' style='cursor:hand' src='<%=strImagePath%>/phone_icon.jpg' width='20' height='17'onClick="javascript:fnLog('<%=strRAID%>')" />        
<%		}
%>
			</td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td height="25" class="RightTableCaption" align="right" ><fmtExcessReturnDetails:message key="LBL_CONSIGNEE_NAME" /> :</td>
			<td>&nbsp; <%=strDistName%>
			</td>
			<td class="RightTableCaption" align="right" ><fmtExcessReturnDetails:message key="LBL_CREATED_BY" />  :</td>
			<td>&nbsp;<%=strCreditedBy%>
			</td>
		</tr>
			<tr>
			<td colspan="4" height="25" class="RightDashBoardHeader"><fmtExcessReturnDetails:message key="LBL_ACTION" /> </td>
		</tr>
	<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr>
			<td height="25" class="RightTableCaption" align="right" >  &nbsp;<fmtExcessReturnDetails:message key="LBL_RECONCILIATION_REASON" />  :</td>
			<td >&nbsp;<gmjsp:dropdown controlName="Cbo_Reason"  seletedValue="<%= strReason %>" onChange="javascript:fnFetchReason();"	
					  value="<%=alReason%>" codeId = "CODEID" codeName = "CODENM" defaultValue= " [Choose One] " />
			</td>
			<td class="RightTableCaption"  align="right"><fmtExcessReturnDetails:message key="LBL_CLOSE_DATE" />:</td>
			<td >&nbsp;<gmjsp:calendar textControlName="Txt_CloseDate" gmClass="InputArea" textValue="<%=dtClosedDate==null?null:new java.sql.Date(dtClosedDate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
			</td>
	</tr>
<% if(strReason.equals("50530") && flag)  { //50530 is Transfer%>	
	<tr>
		<td colspan="4"> 
<script>		
function fnLoad()
{
		val = '<%=strTransferType%>';
		if (val != '')
		{
			var obj = eval("document.all.div"+val);
			document.all.idFrom.innerHTML = obj.innerHTML;
		}
		
			else 
		{
				document.frmTransfer.Cbo_TransferType.value = "90300";
				fnSetFromValues("90300");
		}
}
</script>
<BODY onLoad="fnLoad();">

				<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
				<jsp:include page="/custservice/transfer/GmIncludeTransferHeader.jsp" />
				
</BODY>		
		</td>
	</tr>
<% } %>	
		
<tr>
	<td colspan="4">
			<display:table name="requestScope.RDEXCESSRETURNDETAILS.rows"  class="its" varTotals="totals" id="currentRowObject" export="false" decorator="com.globus.displaytag.beans.DTExsReturnDetailsWrapper"> 
				<fmtExcessReturnDetails:message key="DT_PART" var="varPart"/><display:column property="PNUM" title="${varPart}" sortable="true"  />
				<fmtExcessReturnDetails:message key="DT_PART_DESCRIPTION" var="varPartDescription"/><display:column property="PDESC" title="${varPartDescription}" sortable="true"  />
				<fmtExcessReturnDetails:message key="DT_QTY" var="varQty"/><display:column property="QTY" title="${varQty}" class="aligncenter"  sortable="true" total="true" />
				<fmtExcessReturnDetails:message key="DT_CONTROL" var="varControl"/><display:column property="CONTROLNUM" title="${varControl}"  sortable="true" />
			  	<fmtExcessReturnDetails:message key="DT_PRICE" var="varPrice"/><display:column property="PRICE" title="${varPrice}" class="alignright"  sortable="true" format="<%=strCurrencyFmt %>" total="true" />
			  	<fmtExcessReturnDetails:message key="DT_AMOUNT" var="varAmount"/><display:column property="AMOUNT" title="${varAmount}" class="alignright" format="<%=strCurrencyFmt %>" total="true" />
			  	
			  	<display:footer media="html"> 
<% 
		String strVal;
		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column3").toString();
		String strQty = GmCommonClass.parseZero(String.valueOf(Math.round(Float.parseFloat(strVal))));
		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column5").toString();
		String strPrice =   strCurrSign +" " + GmCommonClass.getStringWithCommas(strVal);
		strVal = ((HashMap)pageContext.getAttribute("totals")).get("column6").toString();
		String strAmount =  strCurrSign + " " + GmCommonClass.getStringWithCommas(strVal);
%>
				  	<tr class = shade>
						  		<td colspan="2"> <B> <fmtExcessReturnDetails:message key="LBL_TOTAL"/> : </B></td>
						  		<td class = "aligncenter" ><B><%=strQty%></B></td>
					  			<td></td>
				  		  		<td class = "alignright"> <B><%=strPrice%></B></td>
				    	    	<td class = "alignright"> <B><%=strAmount%></B></td>
				  	</tr>
				 </display:footer>
			</display:table> 
		</td>
	</tr>	
	<tr>
		<td colspan="4" > 
				<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="LogType" value="" />
				<jsp:param name="LogMode" value="Edit" />
				</jsp:include>
		</td>
	</tr>
	<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
	<tr><td colspan="4" align="center"> <fmtExcessReturnDetails:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="&nbsp;&nbsp;${varSubmit}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnGo();" buttonType="Save" /> </td></tr>
		</table>
</form>	
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>
</HTML>