 <%
/**********************************************************************************
 * File		 		: GmSetRequests.jsp
 * Desc		 		: Report for Set Request Detail
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
   <%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
  <%@ include file="/common/GmHeader.inc" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>  
   <%@ taglib prefix="fmtSetRequests" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- \GmSetRequests.jsp-->
<fmtSetRequests:setLocale value="<%=strLocale%>"/>
<fmtSetRequests:setBundle basename="properties.labels.operations.GmReturnsReprocess"/>
  
  
  <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
  	
<script>
	function fnGetRequestID()
	{
		var request = document.all.request;
		var request_id = "";
	
		if (request.length == undefined)
		{
		   request_id = request.id;   
		}
		else {
				for (i=request.length-1; i > -1; i--) {
					if (request[i].checked) {
							request_id = request[i].id; 
					}
				}
			}	 	   	
	return request_id;	
	}
</script>
   
  <%
    try
    {		       	
  %>  	 
 		<display:table  cellspacing="0" cellpadding="0" class="its" name="requestScope.REQUESTS" id="currentRowObject" decorator="com.globus.displaytag.beans.DTSetRequestWrapper">
 		<display:column property="ID" title="" class="alignleft"  />				    																		     									 																
		<fmtSetRequests:message key="BTN_ID" var="varID"/><display:column property="REQUEST_ID" title="${varID}" class="alignleft"  />	
		<fmtSetRequests:message key="BTN_TYPE" var="varType"/><display:column property="TYPE" title="${varType}" class="alignleft"  />				
		<fmtSetRequests:message key="BTN_REQUIRED_DATE" var="varRequiredDate"/><display:column property="REQUIRED_DATE" title="${varRequiredDate}" class="aligncenter" />			
		<fmtSetRequests:message key="BTN_REQUESTED_BY" var="varRequestedBy"/><display:column property="REQUEST_BY" title="${varRequestedBy}" class="alignleft"  />			
		<fmtSetRequests:message key="BTN_SOURCE" var="varSource"/><display:column property="REQ_SOURCE" title="${varSource}" class="alignleft"  />										 
		</display:table>				
<%	}catch(Exception e)
	{
		e.printStackTrace();
	}
	%>
