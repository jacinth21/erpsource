 <%
/**********************************************************************************
 * File		 		: GmNCMRExcelReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Harinadh Reddi N
************************************************************************************/
%>
<%@ page import="java.util.Locale"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%> 

<%@ page language="java" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*,java.lang.String,java.util.StringTokenizer" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Date"%>
<%
Locale locale = null;
String strLocale = "";

String strJSLocale = "";

String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));

if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>

<%@ taglib prefix="fmtNCMRExcelReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtNCMRExcelReport:setLocale value="<%=strLocale%>"/>
<fmtNCMRExcelReport:setBundle basename="properties.labels.operations.GmNCMRExcelReport"/>
<!--operations\GmNCMRExcelReport.jsp  -->
<%
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH"); 	
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String format = request.getParameter("hAction");
	if ((format != null) && (format.equals("Excel"))) {
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-disposition","attachment;filename=NCMRExcelReport.xls");
	}
%>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");	
	String strFromDt 	= GmCommonClass.parseNull((String)request.getAttribute("hFromDate")) ;
	String strToDt 		= GmCommonClass.parseNull((String)request.getAttribute("hToDate"));
	String strPartNums 	= GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	String strPartName 	= GmCommonClass.parseNull((String)request.getAttribute("hPartName"));
	String strVendorName = GmCommonClass.parseNull((String)request.getAttribute("hVendorName"));
	String strNCMRID 	= "";
	String strSortCName = GmCommonClass.parseNull((String)request.getAttribute("hSort"));
	String strSortType 	= GmCommonClass.parseNull((String)request.getAttribute("hSortAscDesc"));
	String strNCMRId 	= GmCommonClass.parseNull((String)request.getAttribute("hNCMRId"));	
	String strEVALID 	= GmCommonClass.parseNull((String)request.getAttribute("hEVALId"));
	String strNonConf 	= GmCommonClass.parseNull((String)request.getAttribute("hNonConf"));	
	String strDHRId		= GmCommonClass.parseNull((String)request.getAttribute("hDHRId"));	
	String strType 		= GmCommonClass.parseNull((String)request.getAttribute("Cbo_Type"));
	String strTypeNew 		= GmCommonClass.parseNull((String)request.getAttribute("Cbo_TypeNew"));
	HashMap hmLoop = new HashMap();	
	Date dtFrmDate = null;
	Date dtToDate = null;	
	dtFrmDate = (Date)GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	dtToDate = (Date)GmCommonClass.getStringToDate(strToDt,strApplDateFmt);	
	int intLoop = 0;	
	if (alReturn != null){
		intLoop = alReturn.size();
	}	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: NCMR Excel Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style>
.RightTableCaption {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
}
.Line{
	background-color: #676767;
}
TR.RightTableCaption{
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	font-weight: bold;
	FONT-FAMILY: verdana, arial, sans-serif;
	background-color: #eeeeee;
}
.RightText {
	FONT-SIZE: 11px; 
	COLOR: #000000; 
	FONT-FAMILY: verdana, arial, sans-serif;
}
</style>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<FORM name="frmAccount" method="POST">
	<table border="1" width="1100px" cellspacing="0" cellpadding="0">		
		<tr>
			<td height="25" align="center"><B><fmtNCMRExcelReport:message key="LBL_EXC_REP"/>  </B>  </td>
		</tr>		
		<tr>
			<td>
				<table border="1" width="100%" cellspacing="0" cellpadding="0" valign="top">
					<tr class="RightTableCaption">
						<td  HEIGHT="24" align="center" width="60"><fmtNCMRExcelReport:message key="LBL_DATE"/></td>
						<td  width="80" align="center" ><fmtNCMRExcelReport:message key="LBL_NCMR_ID"/></td>
						<td  width="95" align="center" ><fmtNCMRExcelReport:message key="LBL_DHR_ID"/></td>
						<td  width="60" align="center" ><fmtNCMRExcelReport:message key="LBL_PART#"/></td>
						<td  width="400"><fmtNCMRExcelReport:message key="LBL_PART_NAME"/></td>
						<td  width="160" align="center" ><fmtNCMRExcelReport:message key="LBL_CNTR_NO"/><br></td>
						<td  width="90" align="center" ><fmtNCMRExcelReport:message key="LBL_WO_ID"/> </td>
						<td  width="45" align="center" ><fmtNCMRExcelReport:message key="LBL_REJ_QTY"/><br></td>
						<td  width="45" align="center" ><fmtNCMRExcelReport:message key="LBL_CLOSE_QTY"/>.<br></td>
						<td  width="70" align="center" ><fmtNCMRExcelReport:message key="LBL_VEN_NAME"/></td>
						<td  width="70" align="center" ><fmtNCMRExcelReport:message key="LBL_ACTION_TAKEN"/><BR></td>
						<td  width="60" align="center" ><fmtNCMRExcelReport:message key="LBL_REJ_TYP"/><br> </td>
						<td  width="70" align="center" ><fmtNCMRExcelReport:message key="LBL_RTS"/></td>
						<td  width="50" align="center" ><fmtNCMRExcelReport:message key="LBL_DISP_DATE"/>.<br></td>
						<td  width="50" align="center" ><fmtNCMRExcelReport:message key="LBL_CL_DATE"/></td>
						<td  width="50" align="center" ><fmtNCMRExcelReport:message key="LBL_CL_BY"/></td>
					</tr>
<%
			if (intLoop > 0){
				int intStock = 0;
				int intWIP = 0;
				for (int i = 0;i < intLoop ;i++ ){
					hmLoop = (HashMap)alReturn.get(i);
					strNCMRID = (String)hmLoop.get("NCMRID");
%>
					<tr>
						<td height="24" width="50"  class="RightText" align="center"><%=GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("CDATE"),strApplDateFmt)%></td>
						<td width="70" class="RightText" nowrap="nowrap"><%=strNCMRID %></td>
						<td width="80" class="RightText"><%=(String)hmLoop.get("DHRID")%></td>
						<td width="60" class="RightText" nowrap align="right"><%=(String)hmLoop.get("PNUM")%></td>
						<!-- <%=(String)hmLoop.get("PDESC")%> -->
						<td width="400" nowrap class="RightText"><%=(String)hmLoop.get("PDESC")%></td> 
						<td width="100" class="RightText"><%=(String)hmLoop.get("CNUM")%></td>
						<td width="70" class="RightText"><%=(String)hmLoop.get("WOID")%></td>
						<td width="45" class="RightText" align="right"><%=(String)hmLoop.get("QTYREJ")%></td>
						<td width="45" class="RightText" align="right"><%=(String)hmLoop.get("CLOSEQTY")%></td>
						<td width="60" class="RightText"><%=(String)hmLoop.get("VNAME")%></td>
						<td width="60" class="RightText"><%=(String)hmLoop.get("DISP")%></td>
						<td width="50" class="RightText"><%=(String)hmLoop.get("REASON_TYPE")%></td>
						<td width="60" class="RightText"><%=(String)hmLoop.get("RESPON")%></td>
						<td width="50" class="RightText" align="center"><%=GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("DDATE"),strApplDateFmt)%></td>
						<td width="50" class="RightText" align="center"><%=GmCommonClass.getStringFromDate((java.util.Date)hmLoop.get("CLOSEDATE"),strApplDateFmt)%></td>
						<td width="40" class="RightText"><%=(String)hmLoop.get("CLSBY")%></td>
					</tr>
<%
				}
			}else{
%>
			<tr>
				<td height="30" colspan="16" align="center" class="RightTextBlue"><fmtNCMRExcelReport:message key="LBL_NO_NCMR"/>!</td>
			</tr>
<%		}
%>			
	<!-- 	</table></DIV></td></tr> --> 
				</table>
			</td>
		</tr>
	</table>
</FORM>
<%
}catch(Exception e){
	e.printStackTrace();
}
%>
</BODY>
</HTML>
