<%
/**********************************************************************************
 * File		 		: GmRequiredDateHistory.jsp
 * Desc		 		: display required date history info
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ taglib prefix="fmtRequiredDateHistory" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\requests\GmRequiredDateHistory.jsp -->

<fmtRequiredDateHistory:setLocale value="<%=strLocale%>"/>
<fmtRequiredDateHistory:setBundle basename="properties.labels.operations.requests.GmRequiredDateHistory"/>
  
<bean:define id="ldtResult" name="frmRequestEdit" property="ldtResult" type="java.util.List"></bean:define>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

 

</HEAD>

<BODY leftmargin="20" topmargin="10">
 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	 
		<tr><td colspan="3" height="1" class="line"></td></tr>
		
		 <tr>
	                     <td colspan="3">
	                     	<display:table  name="requestScope.frmRequestEdit.ldtResult" class="its" id="currentRowObject" requestURI="/gmRequestEdit.do"   > 
							<fmtRequiredDateHistory:message key="DT_TRANSCATION_ID" var="varTransID"/>
							<display:column property="REQID" title="${varTransID}"  sortable= "true" class="alignleft"/>
							<fmtRequiredDateHistory:message key="DT_VALUE" var="varValue"/>
							<display:column property="RVALUE" title="${varValue}"  sortable= "true" class="alignleft"/>
							<fmtRequiredDateHistory:message key="DT_UPDATE_DATE" var="varUpdateDate"/>
							<display:column property="UPDATE_DATE" title="${varUpdateDate}"   sortable= "true" class="alignleft"/>
							 <fmtRequiredDateHistory:message key="DT_UPDATE_BY" var="varUpdateBy"/>
							<display:column property="UPDATEDBY" title="${varUpdateBy}"   class="alignleft"/> 
							 
							 
							</display:table>

    		             </td>
                    </tr> 
        
		 
    </table>		     	
 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

