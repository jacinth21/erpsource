<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.operations.beans.GmOperationsBean"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
 
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %>

<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtMaterialReconfigure" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\requests\GmMaterialReconfigure.jsp -->

<fmtMaterialReconfigure:setLocale value="<%=strLocale%>"/>
<fmtMaterialReconfigure:setBundle basename="properties.labels.operations.requests.GmMaterialReconfigure"/>


<bean:define id="partList" name="frmMaterialRequest" property="alDetails" type="java.util.ArrayList"> </bean:define>
<bean:define id="reqFor" name="frmMaterialRequest" property="reqFor" type="java.lang.String"></bean:define> 
<bean:define id="requestId" name="frmMaterialRequest" property="requestId" type="java.lang.String"></bean:define> 


<%
int intcount=0;
String strHeaderLbl = "";
//final String strConsignId =  GmCommonClass.parseNull(request.getParameter("hConsignmentId"));
   
int totalCount = 0;

//String strCssPath = GmCommonClass.getString("GMSTYLES");
//String strWikiPath = GmCommonClass.getString("GWIKI");
String strWikiTitle = "";
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.requests.GmMaterialReconfigure", strSessCompanyLocale);
if(reqFor.equals("26240420")){//Stock Transfer
   strHeaderLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RECONFIG_REQ_STOCK_TRANSFER"));
    strWikiTitle = GmCommonClass.getWikiTitle("RECONFIG_REQ_STOCK_TRANSFER");
}
else{
   strHeaderLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RECONFIG_REQ_CONSIGNMENT"));
    strWikiTitle = GmCommonClass.getWikiTitle("RECONFIG_REQ");
}

%>
<BODY leftmargin="20" topmargin="10" >
 


<html:form action="/gmRequestDetail.do"  >
<input type="hidden" name="haction" value="">
<input type="hidden" name="hinputstring" value="">
<input type="hidden" name="hcnt" value="">

<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
 
var requestType = '<%=reqFor%>';
var requestId = '<%=requestId%>';
function fnLoad()
{	
	//alert("size is " + "<%=partList.size()%>");
    document.frmMaterialRequest.haction.value = 'Go' ;
    fnStartProgress('Y');
	document.frmMaterialRequest.submit();
}	

function fnSubmit(val)
{	
 	
 	document.frmMaterialRequest.hcnt.value =   val ;
 	//alert(document.frmMaterialRequest.hcnt.value);
 	
 	
 	hcnt = document.frmMaterialRequest.hcnt.value ;
 	
 	var inputString = '' ;
 	
 	for( var j=0; j < hcnt; j++)
	{
 	
 		pnumobj = eval("document.frmMaterialRequest.partNum"+j);
 	
 	    if (pnumobj)
		{
						
				reconfqtyobj = eval("document.frmMaterialRequest.txt_reconfigPartQty"+j);
 				actionobj    = eval("document.frmMaterialRequest.cbo_reconfAction"+j);

 					 if ( actionobj.value != '0' || requestType == '26240420') 
 						inputString  = inputString + pnumobj.value + ',' + reconfqtyobj.value + ',' +  	actionobj.value +'|' ;

 		}		
 	}
 	
 	//alert( inputString );
 	
 	document.frmMaterialRequest.hinputstring.value = inputString; 

  	if(requestType == '26240420'){
 		document.frmMaterialRequest.action="/gmStockRequestInitiate.do?strOpt=saveReconfig&hinputstring"+inputString+"&requestId="+requestId;
 	}else{ 
 		  document.frmMaterialRequest.haction.value = 'Submit' ;
 	 }  	
  
    fnStartProgress('Y');
	document.frmMaterialRequest.submit();
}	

function fnApplyAll()
{
 // alert("selectedApplyAll is " + document.frmMaterialRequest.cbo_allAction.selectedIndex);
  
 var selectedApplyAll = document.frmMaterialRequest.cbo_allAction.selectedIndex;
 var size="<%=partList.size()%>";
 for (var i=0;i<size ;i++ )
	{
		 
		obj = eval("document.frmMaterialRequest.cbo_reconfAction"+i);			
		 
		   obj.options[selectedApplyAll].selected = true;
		 
		}
	 
}

</script>



 
<table  class="DtTable850"  cellspacing="0" cellpadding="0" >		
		<tr>
			<td rowspan="1" bgcolor="#666666" colspan="2">
			<table width="850" cellspacing="0" cellpadding="0">
			<td height="25" class="RightDashBoardHeader"><%=strHeaderLbl%></td>
			<td align="right"   class=RightDashBoardHeader > 
			<fmt:message key="IMG_ALT_HELP" var = "varHelp"/>	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>			
			</table>
			</td>			
		</tr>						
		<tr>
			<td colspan="2"> 			
				<jsp:include page="/operations/requests/GmRequestViewHeader.jsp">
				<jsp:param name="FORMNAME" value="frmMaterialRequest" />
				<jsp:param name="hViewEdit" value="View" />
				</jsp:include>
			</td>
		</tr>
<%if(!reqFor.equals("26240420")){ %>
<tr><td  bgcolor="#666666" colspan="2"></td></tr>	
<tr><td colspan="2" height="25" class="RightDashBoardHeader"><fmtMaterialReconfigure:message key="LBL_REQUEST_DETAILS"/></td></tr>
<tr><td  bgcolor="#666666" colspan="2"></td></tr>	
<tr  >
<td class="RightTableCaption" HEIGHT="20" width="25%" align="right">&nbsp;<fmtMaterialReconfigure:message key="LBL_BACKORDER_REQ_ID"/>:</td>
<td width="75%">&nbsp;<bean:write name="frmMaterialRequest" property="boreqId" />   </td>

</tr>
<tr class="shade" >
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtMaterialReconfigure:message key="LBL_BACKORDER_REQ_STATUS"/>:</td>
<td>&nbsp;<bean:write name="frmMaterialRequest" property="bostatus" />   </td>

</tr>
<tr >
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtMaterialReconfigure:message key="LBL_DATE_CREATED"/>:</td>
<td>&nbsp;<bean:write name="frmMaterialRequest" property="bocreateddate" />   </td>

</tr>

<tr class="shade">
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtMaterialReconfigure:message key="LBL_CREATED_BY"/>:</td>
<td>&nbsp;<bean:write name="frmMaterialRequest" property="bocreatedby" />   </td>

</tr>
<%} %>
 <tr>
			<td colspan="4" align="center" >&nbsp;
<table cellpadding="0" cellspacing="0"   border="0"  bordercolor="gainsboro"  >
<% if(partList.size() > 0) { %>
<thead>
		<TR bgcolor="#EEEEEE" height="24" class="RightTableCaption"  >
		<TH class="RightDashBoardHeader" width="70" align="center"><fmtMaterialReconfigure:message key="LBL_PART"/></TH>		
		<TH class="RightDashBoardHeader" width="380" >&nbsp;<fmtMaterialReconfigure:message key="LBL_PART_DESCRIPTION"/></TH>
		<TH class="RightDashBoardHeader" width="70" align="center"><fmtMaterialReconfigure:message key="LBL_QTY_PENDING"/></TH>
		<TH class="RightDashBoardHeader" width="70" align="center"><fmtMaterialReconfigure:message key="LBL_QTY_AVAILABLE"/></TH>
		<TH class="RightDashBoardHeader" width="70" align="center"><fmtMaterialReconfigure:message key="LBL_QTY_TO_PROCESS"/></TH>	
		<TH class="RightDashBoardHeader" width="70" align="center"><fmtMaterialReconfigure:message key="LBL_ACTION_TO_PROCESS"/></TH>

		 
		</TR>	
		<TR bgcolor="#EEEEEE" height="24" class="RightTableCaption"  >
		<TH class="RightDashBoardHeader" width="70" align="center">  </TH>		
		<TH class="RightDashBoardHeader" width="380" >&nbsp; </TH>
		<TH class="RightDashBoardHeader" width="70" align="center"> </TH>
		<TH class="RightDashBoardHeader" width="70" align="center">  </TH>
		<TH class="RightDashBoardHeader" width="70" align="center"> </TH>		
		 <%if(reqFor.equals("26240420")){ %>
		<TH class="RightDashBoardHeader" width="70" align="center"><select name="cbo_allAction" id="" class="RightText" tabindex=0  onChange="fnApplyAll()"  > <option value=0 id=0 ><fmtMaterialReconfigure:message key="LBL_CHOOSE_ONE"/><option  value= 106720  id=null><fmtMaterialReconfigure:message key="LBL_EDIT"/></option><option  value= 106721  id=null><fmtMaterialReconfigure:message key="LBL_VOID"/></option></select>
					</TH>
			<%}else{ %>
			<TH class="RightDashBoardHeader" width="70" align="center"><select name="cbo_allAction" id="" class="RightText" tabindex=0  onChange="fnApplyAll()"  > <option value=0 id=0 ><fmtMaterialReconfigure:message key="LBL_CHOOSE_ONE"/><option  value= 90809  id=null><fmtMaterialReconfigure:message key="LBL_RELEASE_TO_CN"/></option><option  value= 90810  id=null><fmtMaterialReconfigure:message key="LBL_VOID"/></option></select>
					</TH>
			<%} %>
		 		
		</TR>	
		<tr><th class="Line" height="1" colspan="11"></th></tr>
</thead>
<% } %>
<%   intcount=0;
	String strName = "cbo_reconfAction";
%>

			  <TBODY>
<logic:iterate id="partDetailsList" name="frmMaterialRequest" property="alDetails" indexId="counter" >
				<tr>
					<td >&nbsp; <bean:write name="partDetailsList" property="PNUM"/> 
					<htmlel:hidden  property="partNum${counter}" value="${partDetailsList.PNUM}" />					
					</td>
				
					<td id="Lbl_Desc<bean:write name="counter"/>" class="RightText"><bean:write name="partDetailsList" property="PDESC"/></td>
					<td><bean:write name="partDetailsList" property="QTY"/>
					<htmlel:hidden  property="pendingQty${counter}" value="${partDetailsList.QTY}" />
					</td>
					
					<td><bean:write name="partDetailsList" property="AVAILABLE"/>
					<htmlel:hidden  property="availQty${counter}" value="${partDetailsList.AVAILABLE}" />
					</td>
					
					
					<td align="center" id="Txt_Qty<bean:write name="counter"/>" >&nbsp;
						<htmlel:text  property="txt_reconfigPartQty${counter}"  value="${partDetailsList.QTY}" maxlength="100" size="3" style="InputArea" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" />
					</td>
					
<% 
   strName = "cbo_reconfAction" + intcount ;
   intcount++;
   
  %>				
					<td>	
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<gmjsp:dropdown controlName="<%=strName%>" SFFormName="frmMaterialRequest" 
							SFValue="alAction" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]" />
					</td>
					
				</tr>
</logic:iterate>

<% totalCount = intcount;  %> 
				</TBODY>
		
			
	
</table>
</td>
</tr>
	<% 
		String strSubmit = "fnSubmit('"+totalCount+"')";
	%>	
		<tr height="30">
		<fmtMaterialReconfigure:message key="BTN_SUBMIT" var="varSubmit"/>
			<td colspan="4" align="center"> <gmjsp:button value="${varSubmit}" gmClass="button" onClick="<%=strSubmit%>" buttonType="Save" /> </td>
		</tr>
</table>



<%@ include file="/common/GmFooter.inc" %>

</html:form>