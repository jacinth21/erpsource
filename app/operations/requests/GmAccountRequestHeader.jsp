 <%@ include file="/common/GmHeader.inc" %>
 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import ="java.util.HashMap"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ taglib prefix="fmtAccountReqHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>
<!-- GmAccountRequestHeader.jsp -->
<fmtAccountReqHeader:setLocale value="<%=strLocale%>"/>
<fmtAccountReqHeader:setBundle basename="properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest"/>
<%	/*********************************
	 * GmRequestHeader.jsp
	 *********************************/ 
	String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
	String strSetViewFl = GmCommonClass.parseNull((String)request.getParameter("SETVIEW")); 
	//log.debug(" FormName is " + strFormName);
	int intSize = 0;
	HashMap hmValue = new HashMap();
	ArrayList alRemoveCodeIDs = new ArrayList();
	
	String strShipCarrDef = GmCommonClass.parseNull(request.getParameter("SHIPCARR"));
	String strShipModeDef = GmCommonClass.parseNull(request.getParameter("SHIPMODE"));
	String strShipToDef = GmCommonClass.parseNull(request.getParameter("SHIPTO"));
	String strShowLoanAtrb = GmCommonClass.countryCode;
				
%>
<bean:define id="alInHousePurpose" name="<%=strFormName%>" property="alInHousePurpose" type="java.util.ArrayList"> </bean:define>
<bean:define id="alRepList" name="<%=strFormName%>" property="alRepList" type="java.util.ArrayList"> </bean:define>
<bean:define id="alAccList" name="<%=strFormName%>" property="alAccountList" type="java.util.ArrayList"> </bean:define>
<bean:define id="alEmpList" name="<%=strFormName%>" property="alEmployeeList" type="java.util.ArrayList"> </bean:define>
<bean:define id="strType" name="<%=strFormName%>" property="consignmentType" type="java.lang.String"> </bean:define>
<bean:define id="strShipTo" name="<%=strFormName%>" property="shipTo" type="java.lang.String"> </bean:define>
<bean:define id="strShipToId" name="<%=strFormName%>" property="employeeId" type="java.lang.String"> </bean:define>
<bean:define id="alDistributorList" name="<%=strFormName%>" property="alDistributorList" type="java.util.ArrayList"> </bean:define>
<bean:define id="hmPurpose" name="<%=strFormName%>"  property="hmPurposes" type="java.util.HashMap"></bean:define>

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmAccountSetInitiateHeader.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var vshipMode = '<%=strShipModeDef%>';
var vshipCarrier = '<%=strShipCarrDef%>';
var vshipTo = '<%=strShipToDef%>';
var showloan = '<%=strShowLoanAtrb%>'; 
<%
	ArrayList alInhousePurpose = new ArrayList();
	ArrayList alConsignmentPurpose = new ArrayList();
	HashMap hcboVal = null;
	
	if (hmPurpose!= null)
	{  
		alInhousePurpose = (ArrayList) hmPurpose.get("INHOUSEPUR");
		alConsignmentPurpose = (ArrayList) hmPurpose.get("CONSIGNPUR");
	}

%>
 
var consignLen= <%=alConsignmentPurpose.size()%>;
var inhouseLen = <%=alInhousePurpose .size()%>;
var distLen = <%=alDistributorList.size()%>;
var hospiLen = <%=alAccList.size()%>;

<%
	hcboVal = new HashMap();
	for (int i=0;i<alConsignmentPurpose.size();i++)
	{
		hcboVal = (HashMap)alConsignmentPurpose.get(i);
%>
	var ConArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<%
	}
%>

<%	
	hcboVal = new HashMap();
	for (int i=0;i<alInhousePurpose.size();i++)
	{
		hcboVal = (HashMap)alInhousePurpose.get(i);
%>
	var InhouseArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%> ";
<%
	}
%>
 
<%	
hcboVal = new HashMap();
for (int i=0;i<alDistributorList.size();i++)
{
	hcboVal = (HashMap)alDistributorList.get(i);
%>
var DistDivArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("DDIV")%> ";
<%
}
%>

<%	
hcboVal = new HashMap();
for (int i=0;i<alAccList.size();i++)
{
	hcboVal = (HashMap)alAccList.get(i);
%>
var AccountArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>,<%=hcboVal.get("DEALID")%>";// Adding dealer id for ship to type as dealer
<%
}
%>

<%	
hcboVal = new HashMap();
for (int i=0;i<alDistributorList.size();i++)
{
	hcboVal = (HashMap)alDistributorList.get(i);
%>
var DistArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%> ";
<%
}
%>

function fnLoad()
{		document.all.assocRep.style.display = "none";
		var objfrm = document.getElementById("frmCart").contentWindow; //To support Edge Browser Hide the Date Req field.
		var InHouseFl = '<%=strType%>';
		var ShipToFl = '<%=strShipTo%>';
		document.all.inHousePurpose.value = '50060';  //50060-Regular		
		if (InHouseFl == '40022')
		{
			document.all.inHousePurpose.disabled = false;
			document.all.names.disabled = false;
			document.all.distributorId.selectedIndex = 0;
			fnCallType('');
			fnSetValue();
		}
		
		if (InHouseFl == '40021' || InHouseFl == '40025' || InHouseFl == '102930'){ // 40021 : Consignment 102930 - ICT
			// MNTTASK - 8878: When click left link or when select type consignment it will be select Regular in Purpose drop down.
			document.all.inHousePurpose.value = 50060; // 50060 - Regular
		}
		
		if(InHouseFl == '4127')
		{
			fnCallType('');
			document.all.shipCarrier.value="<%=strShipCarrDef%>";
			document.all.shipMode.value="<%=strShipModeDef%>";
			document.all.shipTo.value="<%=strShipToDef%>";
						
		}
		if (ShipToFl != '4120')
		{
			fnCallShip();
		//	fnSetValue();
		}	

		if(InHouseFl == '40021' || InHouseFl == '40025')
		{
			
			objfrm.fnRemoveDateReq();    //Remove the Date Req field.
			//if OUS, set default value for ship mode, ship carrier and ship to
			if (showloan != 'en'){
				document.all.shipCarrier.value="<%=strShipCarrDef%>";
				document.all.shipMode.value="<%=strShipModeDef%>";
			}
			 
		}
		else if(InHouseFl == '40022')
		{                           
			
			objfrm.fnShowDateReq();//Display the Date Req field
			//if OUS, set default value for ship mode, ship carrier and ship to
			if (showloan != 'en'){
				document.all.shipCarrier.value="<%=strShipCarrDef%>";
				document.all.shipMode.value="<%=strShipModeDef%>";
				document.all.shipTo.value="<%=strShipToDef%>";
			}
		}
		//fnDisable(document.all.consignmentType.value);
		//fnLoadList(InHouseFl);
}
		

function fnSetValue()
{
	var ShipToId = '<%=strShipToId%>';

	var obj5 = document.all.names;
	for (var j=0;j<obj5.length;j++)
	{
		if (obj5.options[j].value == ShipToId)
		{
			obj5.options[j].selected = true;
			break;
		}
	}
}

window.onbeforeunload = function () {
	document.all.consignmentType.value = '40025';
	document.all.inHousePurpose.value = '0';
	document.all.distributorId.value = '0';
	document.all.shipTo.value = '0';
	document.all.names.value = '0';
}

</script>
<html:hidden property="deptId" />
<BODY onLoad="javascript:fnLoad();">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
	<tr>
		<td colspan="2">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
				<tr>
					<td class="RightTableCaption" HEIGHT="23" align="right" width="20%">&nbsp;<font color="red">* </font><fmtAccountReqHeader:message key="LBL_REQUESTTYPE"/>:</td>
					<fmtAccountReqHeader:message key="LBL_CHOOSEONE" var="varChooseone"/>
					<td width="40%">&nbsp;<gmjsp:dropdown controlName="consignmentType" SFFormName="<%=strFormName%>" SFSeletedValue="consignmentType" onChange="javascript:return fnCallType(this);"
							SFValue="alConsignmentType" codeId = "CODEID"  codeName = "CODENM" defaultValue= "${varChooseone}"  />
					</td>
					<td class="RightTableCaption" align="right"><fmtAccountReqHeader:message key="LBL_PURPOSE"/>:</td>
					<td width="290">&nbsp;<gmjsp:dropdown controlName="inHousePurpose" SFFormName="<%=strFormName%>" SFSeletedValue="inHousePurpose"
							SFValue="alInHousePurpose" codeId = "CODEID"  codeName = "CODENM" defaultValue= "${varChooseone}" onChange="javascript:fnSelectPurpose();"/>
					</td>
				</tr>
				<tr><td colspan="4" height="1" class="LLine"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" HEIGHT="23" align="right"><div id="date1" style="height:18"  > &nbsp;<fmtAccountReqHeader:message key="LBL_REQUIREDDATE"/>:</div></td>
					<td>&nbsp;<gmjsp:calendar SFFormName="<%=strFormName%>" controlName="requiredDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					</td>
					<td class="RightTableCaption" HEIGHT="23" align="right" width="25%"><div id="date1" style="height:18"> &nbsp;<fmtAccountReqHeader:message key="LBL_PLANNEDSHIPDATE"/>:</div></td>
					<td>&nbsp;<gmjsp:calendar SFFormName="<%=strFormName%>" controlName="plannedDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					</td>									
				</tr>				
				<tr><td colspan="4" height="1" class="LLine"></td></tr>
				<tr class="">
					<td class="RightTableCaption" HEIGHT="23" align="right"><div id="blto" style="height:18">&nbsp;<fmtAccountReqHeader:message key="LBL_BILLTO"/>:</div></td>
					<td colspan="3">
					<table>
						<tr><td><gmjsp:dropdown controlName="distributorId" SFFormName="<%=strFormName%>" SFSeletedValue="distributorId"
							SFValue="alAccountList" codeId = "ID"  codeName = "NAME" defaultValue= "${varChooseone}" onChange="javascript:fnSetShipTo();"/></td></tr></table></td>
				</tr>	
				<tr><td colspan="4" height="1" class="LLine"></td></tr>
				<tr class="shade">
					<td class="RightTableCaption" HEIGHT="23" align="right"><div style="height:18">&nbsp;<fmtAccountReqHeader:message key="LBL_INIT_RA"/>:</div></td>
					<td>
					<table>
						<tr><td><fmtAccountReqHeader:message key="LBL_INIT_RA" var="varRATitle"/><input type="checkbox" name="strRAFlag" title="varRATitle"  value="Y"/></td></tr></table></td>
						<!-- PMT-32450 - Add new Column in account item initiate Screen  -->
						<td class="RightTableCaption" HEIGHT="23" align="right"><fmtAccountReqHeader:message key="LBL_LOANER_REQ_ID"/>:&nbsp;</td>
						<td><html:text property="loanerReqID" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8')" onblur="changeBgColor(this,'#ffffff'),fnvalidateLoanerRefID();" /></td>
				</tr>	
			</table>
		</td>
	</tr>
</table>
</BODY>
