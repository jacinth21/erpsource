<%
/**********************************************************************************
 * File		 		: GmProductReqSummary.jsp
 * Desc		 		: Loaner Requests Summary Report
 * Version	 		: 1.0
 * author			:
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtProductReqSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!--GmProductReqSummary.jsp  -->
<fmtProductReqSummary:setLocale value="<%=strLocale%>"/>
<fmtProductReqSummary:setBundle basename="properties.labels.operations.requests.GmProductReqSummary"/>


<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
String strApplnDateFmt = strGCompDateFmt;
%>
<bean:define id="gridData" name="frmProductReqSummary" property="gridData" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Requests Summary </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script language="JavaScript" src="<%=strOperationsJsPath%>/GmProductReqSummary.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmCommonShippingReport.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">


<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">

<script type="text/javascript">
   var objGridData;
   objGridData ='<%=gridData%>';
   var todaysDate = '<%=strTodaysDate%>';
   var format = '<%=strApplnDateFmt%>';
</script>
</HEAD>


<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">

<html:form  action="/gmProductReqSummaryAction.do">
<html:hidden property="strOpt" name="frmProductReqSummary"/>
<table border="0" class="GtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtProductReqSummary:message key="LBL_REQUESTS_SUMMARY"/></td>
			
			<td class="RightDashBoardHeader"></td>
			<td  class="RightDashBoardHeader" align="right" colspan="3">
			   
		    </td>
		</tr>
			<tr class="Shade">
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtProductReqSummary:message key="LBL_FIELD_SALES"/>:</td>
			<td >&nbsp;&nbsp;<gmjsp:dropdown controlName="dist"  SFFormName='frmProductReqSummary' SFSeletedValue="dist"  defaultValue= "[Choose One]"	
				SFValue="alDist" codeId="ID" codeName="NAME" /></td>
						
			<td height="25" class="RightTableCaption" align="Right"><fmtProductReqSummary:message key="LBL_SALES_REP"/>:</td>	
			<td >&nbsp;&nbsp;<gmjsp:dropdown controlName="rep"  SFFormName='frmProductReqSummary' SFSeletedValue="rep"  defaultValue= "[Choose One]"	
				SFValue="alRepList" codeId="ID" codeName="NM"/></td>
				
			<td height="25" class="RightTableCaption" align="Right"><fmtProductReqSummary:message key="LBL_STATUS"/>:</td>
			<td >&nbsp; <gmjsp:dropdown controlName="status"  SFFormName='frmProductReqSummary' SFSeletedValue="status"  defaultValue= "[All]"	
				SFValue="alStatus" codeId="CODEID" codeName="CODENM" />  
		   </td>	
		</tr>
		
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<tr>
			<td height="25" class="RightTableCaption" align="Right">&nbsp;<fmtProductReqSummary:message key="LBL_FROM_DATE"/>:</td>
			<td >&nbsp;
			<gmjsp:calendar SFFormName="frmProductReqSummary" controlName="fromDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<%-- <td>&nbsp;<html:text property="fromDt" name="frmProductReqSummary"  size="10"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
			<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmLoanerReconciliation.fromDt');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif"  align="absmiddle" border=0  height=18 width=19 />
			</td>--%>
			
			<td height="25" class="RightTableCaption" align="Right"><fmtProductReqSummary:message key="LBL_TO_DATE"/>:</td>
			<td>&nbsp;
			<gmjsp:calendar SFFormName="frmProductReqSummary" controlName="toDt" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<%--<td>&nbsp;<html:text property="toDt" name="frmProductReqSummary"   size="10" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />
			<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmLoanerReconciliation.toDt');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif"  align="absmiddle" border=0  height=18 width=19 />
			</td>--%>
			
			<td colspan="2" align="center" ><fmtProductReqSummary:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;"  name="Btn_Submit" gmClass="button"  onClick=" onReload();" buttonType="Load"  /></td>		
		</tr>
	
</table>	

<table  border="0" width="900" cellspacing="0" cellpadding="0">

			<%if(!gridData.equals("")){%>
		
			<tr>  <td><div id="grpData"  style="height:450;width :900" ></div></td> </tr>
			<tr>
			      <td colspan="6" align="center">
			          <div class='exportlinks'><fmtProductReqSummary:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
			                              onclick="gridObj.toExcel('/phpapp/excel/generate.php');"><fmtProductReqSummary:message key="LBL_EXCEL"/>   </a>&nbsp;
			          </div>
			     </td>

			</tr>
			<%} %>		
</table>	

</html:form>
<%@ include file="/common/GmFooter.inc" %>

</BODY>
</HTML>

