<%
/**********************************************************************************
 * File		 		: GmInHouseTransactionSetup.jsp
 * Desc		 		: Creating Inhouse FG/QN/REPACK/BULK/Inhouse/RW Transaction
 * Version	 		: 
 * author			: Tamizhthangam
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.net.URLEncoder"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtInhouseTxn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page isELIgnored="false" %>
<!-- GmInHouseTransactionSetup.jsp -->
<fmtInhouseTxn:setLocale value="<%=strLocale%>"/>
<fmtInhouseTxn:setBundle basename="properties.labels.custservice.ProcessRequest.GmInhouseTransaction"/>
<%@ page isELIgnored="false" %>

<bean:define id="empId" name="frmInhouseTransaction" property="empId" type="java.lang.String"> </bean:define>
<bean:define id="empName" name="frmInhouseTransaction" property="empName" type="java.lang.String"> </bean:define>
<bean:define id="IHTransType" name="frmInhouseTransaction" property="IHTransType" type="java.lang.String"> </bean:define>
<bean:define id="gridData" name="frmInhouseTransaction" property="gridXmlData" type="java.lang.String" />


<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("PRODUCT_REQUEST_INITIATE");
String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
strCompanyInfo = URLEncoder.encode(strCompanyInfo);
GmResourceBundleBean gmResourceBundleBeanlbl = 
GmCommonClass.getResourceBundleBean("properties.labels.custservice.ProcessRequest.GmInhouseTransaction", strSessCompanyLocale);
String strHeaderName ="";
if(IHTransType.equals("111800")){
     strHeaderName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HEADER_QNIN"));
}else if(IHTransType.equals("111801")){
	strHeaderName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HEADER_BLIN"));
}else if(IHTransType.equals("111802")){
	strHeaderName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HEADER_FGIN"));
}else if(IHTransType.equals("111803")){
	strHeaderName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HEADER_PNIN"));
}else if(IHTransType.equals("111804")){
	strHeaderName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HEADER_IHIN"));
}else if(IHTransType.equals("111805")){
	strHeaderName =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_HEADER_RWIN"));
}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: In-house Consignment Transaction based on Inventory Type</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" href="<%=strCssPath%>/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=strJsPath%>/common/bootstrap.min.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmInHouseTransactionSetup.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/common/bootstrap.min.js"></script>
<%-- <style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style> --%>
<script>
var objGridData = '<%=gridData%>';
var inhouseType ='<%=IHTransType%>';
</script>
<style type="text/css">
.gridbox,.gridbox_dhx_skyblue{
   width: 800px!important;
   height: 400px!important;
}
</style>
</HEAD>

<body leftmargin="20" topmargin="10" onload="fnOnPageLoad()">
<html:form action="/gmInHouseTransactionSetup.do?method=loadInHouseTxnInfo">
<html:hidden property="strOpt"/>
<html:hidden property="txt_LogReason"/>
<input type="hidden" name="RE_FORWARD" value="gmInHouseTransactionSetup">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" value="REQUESTINITWRAPPER">
<input type="hidden" name="RE_SRC" value="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmInHouseTransactionSetup&txnStatus=PROCESS&RE_TXN=REQUESTINITWRAPPER">
<input type="hidden" name="hWarehouseType">
<input type="hidden" name="hDisplayRW" value="N">
<input type="hidden" name="haddInfo" value=""> 
<input type="hidden" name="hinputString" value=""> 
<input type="hidden" name="IHTransType" value="<%=IHTransType%>"> 
<table  border="0"  cellspacing="0" cellpadding="0" class="DtTable800" style="margin-top: 10px; margin-left: 20px;" >	
<tr>	
			<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<%=strHeaderName %></td>
				<fmtInhouseTxn:message key="LBL_HELP" var="varHelp"/>
				<td height="15" width="10%"class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
			</tr>
			
			<tr>
					<td width="15%" class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<font color="red">* </font><fmtInhouseTxn:message key="LBL_REQUESTTYPE"/>:</td>
					<td width="25%">&nbsp;<gmjsp:dropdown controlName="consignmentType" SFFormName="frmInhouseTransaction" SFSeletedValue="consignmentType" onChange="javascript:fnCallType(this);fnDisable(this.value);"
							SFValue="alRequestFor" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" />
					</td>
					<td width="15%" class="RightTableCaption" align="right" ><fmtInhouseTxn:message key="LBL_PURPOSE" />:</td>
					<td width="33%">&nbsp;<gmjsp:dropdown controlName="inHousePurpose" SFFormName="frmInhouseTransaction" SFSeletedValue="inHousePurpose"
							SFValue="alInHousePurpose" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" onChange="javascript:fnSelectPurpose();" tabIndex="1"/>
					</td>
				</tr>
				<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
				<tr  class="Shade">
					<td class="RightTableCaption" HEIGHT="23" align="right"><div id="blto" style="height=23">&nbsp;<fmtInhouseTxn:message key="LBL_BILLTO" />:&nbsp;</div></td>
					<td colspan="3">
							 <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="empId" />
							<jsp:param name="METHOD_LOAD" value="loadEmployeeList&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="250" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value="2"/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%=empName%>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=empId %>" />
							<jsp:param name="SHOW_DATA" value="50" />
							<jsp:param name="AUTO_RELOAD" value="" />
					     	</jsp:include>
					</td>
				</tr>
			<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
			<tr class="shade">
			<td height="30" class="RightTableCaption" width="102" align="right">&nbsp;<fmtInhouseTxn:message key="LBL_PART_NUMBER"/>:&nbsp;</td>
			<td> <input type="text" size="40" value="" id="Txt_PartNum" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  tabindex="3">
			</td>
			<fmtInhouseTxn:message key="BTN_ADDTOCART" var="varAddToCart"/>
			<td colspan="2">
			<gmjsp:button value="&nbsp;${varAddToCart}&nbsp;" gmClass="btn btn-primary btn-sm" buttonType="Load" onClick="fnGoCart()" style="margin-left: 20px;" tabindex="4"/>
			 <gmjsp:button gmClass="btn btn-primary btn-sm" style="margin-right:40px;width:35%" buttonType="Load"  onClick="fnOpenPart();" value="Part Lookup"/></td>
		    </tr>
		<tr ><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
			<TR height="40" class="Shade">
			<td class="RightTableCaption" colspan="2">
			<fmtInhouseTxn:message key="LBL_ADD" var="varAdd"/><gmjsp:label type="BoldText"  SFLblControlName="${varAdd}:" td="false"/>
			&nbsp;<fmtInhouseTxn:message key="IMG_ADD_ROW" var="varAddRow"/><img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="addRow()"height="14" align="middle">&nbsp;&nbsp;
			<fmtInhouseTxn:message key="LBL_DEL" var="varDel"/><gmjsp:label type="BoldText"  SFLblControlName="${varDel}:" td="false"/>
			&nbsp;<fmtInhouseTxn:message key="IMG_REMOVE_ROW" var="varRemoveRow"/><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14" align="middle">
			</td>
			<td align="left" colspan="2"></td>
			</TR>
			<tr>
			<td colspan="4">
			<!-- <div id="dataGrid" style="height: 400px; width: 700px;" ></div> -->
			<div  id="dataGrid" height="400px">
		 <%--  <iframe src="/GmCommonCartServlet?hAction=ReqCart&hScreen=gmInHouseTransactionSetup&companyInfo=<%=strCompanyInfo%>" scrolling="no" id="frmCart" marginheight="0" width="100%" height="380" frameborder="0" HIDEFOCUS="true"></iframe><BR> --%>
			</div>
			</td>
		</tr> 				
		<tr> 
             <td colspan="4"><div id="comments" >
               	<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="ALNAME" value="alLogReasons" />						
				</jsp:include></div>
			</td>
        </tr> 
         
       
        <tr>
			<td colspan="4" align="center" height="35">
				<fmtInhouseTxn:message key="BTN_SUBMIT" var ="varSubmit"/>
				 <gmjsp:button value="&nbsp;${varSubmit}&nbsp;"gmClass="btn btn-primary btn-sm" buttonType="Save" onClick="fncreateIHTransaction()" />
			</td>
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		  <tr>
			<td  colspan="4" > 
			 <div id="divf" style="display:none;">
			<iframe src="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmInHouseTransactionSetup&txnStatus=PROCESS&RE_TXN=REQUESTINITWRAPPER" scrolling="yes" id="iframe1" marginheight="0" width="100%" height="100" frameborder="0"  ></iframe><BR>
			</div>  
	  
			</td></tr>
	</table>
</html:form>
</body>
</HTML>