<%
/**********************************************************************************
 * File		 		: GmRequestPrint.jsp
 * Desc		 		: This screen is used to display  Detail of 
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>
 <%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmCommonControls"%>

<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>
<%@ taglib prefix="fmtRequestPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!--operations\requests\GmRequestPrint.jsp  -->

<fmtRequestPrint:setLocale value="<%=strLocale%>"/>
<fmtRequestPrint:setBundle basename="properties.labels.operations.requests.GmRequestPrint"/>

<bean:define id="requestId" name="frmRequestMaster" property="requestId" type="java.lang.String"></bean:define> 
<bean:define id="consignID" name="frmRequestMaster" property="consignID" type="java.lang.String"></bean:define> 
<HTML>
<HEAD>
<TITLE> Globus Medical: Request Edit</TITLE>

<% Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);

Object bean = pageContext.getAttribute("frmRequestMaster", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
try {		 	 
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strRequestId = requestId; 
	String strConsignmentId = consignID;
	 log.debug("strRequestId here is......... " + strRequestId);
	%> 
	
<script> 
function fnPrint()
{
	window.print();
} 
</script> 
 
</HEAD>


<BODY leftmargin="20" topmargin="10" >
 
<jsp:include page="/operations/requests/GmRequestViewHeader.jsp">
		  <jsp:param name="FORMNAME" value="frmRequestMaster" />
			</jsp:include>
			 	 
<p> &nbsp;</p>
 
<jsp:include page="/operations/logistics/GmBackorderReport.jsp" >
					 <jsp:param name="FORMNAME" value="frmRequestMaster" />
					<jsp:param name="hRequestId" value="<%=strRequestId%>" />
					<jsp:param name="hConsignmentId" value="<%=strConsignmentId%>" />
					</jsp:include>	 
 <p align=center> &nbsp;
 <fmtRequestPrint:message key="BTN_PRINT" var="varPrint"/>
  <gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnPrint();"/>&nbsp;&nbsp;
</p>
 <%
}catch(Exception e)
{
	e.printStackTrace();
}
%> 
  
  
</BODY>

</HTML>
