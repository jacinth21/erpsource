<%@ include file="/common/GmHeader.inc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%> 
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>

<%@ taglib prefix="fmtLoanerSetPickSlip" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\requests\GmLoanerSetPickSlip.jsp -->

<fmtLoanerSetPickSlip:setLocale value="<%=strLocale%>"/>
<fmtLoanerSetPickSlip:setBundle basename="properties.labels.operations.requests.GmLoanerSetPickSlip"/>


<bean:define id="alReqSlipList" name="frmLoanerReqEdit" property="alReqSlipList" type="java.util.ArrayList"></bean:define>
<bean:define id="hmReq" name="frmLoanerReqEdit" property="hmRequest" type="java.util.HashMap"></bean:define>
<bean:define id="requestId" name="frmLoanerReqEdit" property="requestId" type="java.lang.String"></bean:define>
<bean:define id="strhMode" name="frmLoanerReqEdit" property="strMode" type="java.lang.String"></bean:define>
<bean:define id="alAttrib" name="frmLoanerReqEdit" property="alAttrib" type="java.util.ArrayList"/>
<html>
<%
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

String strCompanyNm = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYNAME"));
String strCompanyAddress = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYADDRESS"));
strCompanyAddress = strCompanyAddress.replaceAll("/", "<br>");
String strCountryCode1 = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
String strLoanedTo = strCountryCode1.equalsIgnoreCase("en")?GmCommonClass.parseNull((String)hmReq.get("SHIPTOADD")):GmCommonClass.parseNull((String)hmReq.get("ACCOUNTNM"));

String strReqDate = GmCommonClass.parseNull((String)hmReq.get("REQUESTEDDT"));
String strSurgeryDt = GmCommonClass.parseNull((String)hmReq.get("SURGERYDT"));
String strCompanyLogo = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COMP_LOGO"));

//***PC_5542**
String strHeadAddrSticker = "";
String strStickCompanyLocale  = "";
String strCompStickerId = "";
String strPlantId = "";

String strClass="DtRuleTable700";
String strHeader  =	"";
String strSubHeader = "";
String strSubChar = "";
String strUserName = "";
String strQty = "";
HashMap hmSetDetails = new HashMap();
ArrayList alAttribute=(ArrayList)alAttrib;

strPlantId = GmCommonClass.parseNull(gmDataStoreVO.getPlantid());
strCompStickerId = GmCommonClass.parseNull(gmDataStoreVO.getCmpid()); 
if(strCompStickerId.equals("1010") && strPlantId.equals("3003"))
{
    strStickCompanyLocale = GmCommonClass.getCompanyLocale(strCompStickerId);
    GmResourceBundleBean gmStkCompResourceBundleBean =
    GmCommonClass.getResourceBundleBean("properties.Company", strStickCompanyLocale);
    strHeadAddrSticker = "<br>"+GmCommonClass.parseNull((String)gmStkCompResourceBundleBean.getProperty("COMPANY_ADDRESS_STICKER"));
}

if(strhMode.equals("Del"))
{
	strHeader = "Delivery Note";
	strSubHeader = "Delivery";
	strSubChar = "D";
}
else if(strhMode.equals("Coll"))
{
	strHeader = "Collection Note"; 
	strSubHeader = "Collection";
	strSubChar = "C";
}
else
{
	strHeader = "Loaner Pick Slip";
	strSubHeader = "Request";
	strHeadAddrSticker = "";
}

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<TITLE> GlobusOne Enterprise Portal: Loaner Request Edit </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<style>
TR.PageBreak {
     page-break-after: always;
}
</style>

<script>
function fnPrint()
{
	window.print();
}
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";	
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>
</head>
<body leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="right" height="30" id="button">
			<fmtLoanerSetPickSlip:message key="IMG_ALT_CLICK_PRINT_PAGE" var = "varPrint"/>
				<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="${varPrint}" onClick="fnPrint();" />
				<fmtLoanerSetPickSlip:message key="IMG_ALT_CLICK_CLOSE_WINDOW" var = "varClose"/>
				<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="${varClose}" onClick="window.close();" />
			</td>
		<tr>
	</table>
<table border=0 class="DtRuleTable700" cellspacing=0 cellpadding=0 align="center">
	<tr>
		<td bgcolor=#666666 colspan=3 height=1></td>
	</tr>
	<tr>
		<td bgcolor=#666666 width=1></td>
		<td>
			<table cellpadding=0 cellspacing=0 border=0 >
				<tr>
					<td width=170><br><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
					<td class=RightText width=450><b><%=strHeadAddrSticker%><%=strCompanyNm%></b><br><%=strCompanyAddress%><br></td>
					<td align=right class=RightText width=400><font size=+2><%=strHeader%></font></td>
					<td colspan=3></td>	
				</tr> 
				<tr>
					<td colspan=3><br></td>
					<td bgcolor=#666666></td>
				</tr>
						<tr>
					<td bgcolor=#666666 colspan=3 height=1></td>
				</tr>
				<tr>
					<td colspan=3>
						<table border=0 cellspacing=0 cellpadding=0>
							<tr bgcolor=#eeeeee class=RightTableCaption>
								<td height=24 align=center colspan =2>&nbsp;<fmtLoanerSetPickSlip:message key="LBL_LOANED_TO"/></td>
								<td width=100 align=center>&nbsp;<%=strSubHeader%> <fmtLoanerSetPickSlip:message key="LBL_DATE"/></td>
								<td width=400 align=center>&nbsp;<%=strSubHeader%> #</td>
							</tr>
							<tr>
								<td bgcolor=#666666 colspan=7 height=1></td>
							</tr>
							<tr>
								<td class=RightText rowspan=5 valign=top>&nbsp;<%=strLoanedTo%></td>
								<td rowspan=5 class=RightText valign=top></td>
								<td height=25 class=RightText align=center>&nbsp;</td>
								<td class=RightText align=center>&nbsp;<%=requestId%><%=strSubChar%></td>
							</tr>
							<tr class=RightTableCaption>
								<td align=center>&nbsp;</td>
								<td align=center></td>
							</tr>
						</table>
					</td>
				</tr>
				<%if(alAttribute.size()>0){ %>
				<tr>
					<td bgcolor=#666666 colspan=3></td>
				</tr>
						<tr>
							<td height="23"  colspan="3"><b><fmtLoanerSetPickSlip:message key="LBL_CASE_DETAILS"/>:</b></td>			
						</tr>			
						<tr><td class="line" colspan="3"></td></tr>
								<tr><td colspan=3>
								<table border="0" cellspacing="0" cellpadding="0" align="center"><tr>
						<%
							for(int cnt=0;cnt<alAttribute.size();cnt++){
								HashMap hmParam=(HashMap)alAttribute.get(cnt);
								if(cnt%2==0 && cnt!=0){
						%>
							<td></td></tr><tr>
							<%} %>
							<td HEIGHT="25" align="right" width="200">
								<b><%=hmParam.get("ATTBNAME") %>:</b> &nbsp;
							</td>
							<td width="200"><%=hmParam.get("ATTBVAL") %></td>
							<%} %>
						</tr>
						<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
						<%} %>
						<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
						</table></td></tr>
				<tr>
					<td bgcolor=#666666 colspan=3 height=1></td>
				</tr>
				<tr id="displaytable"> 							
				            <td colspan="3">			            
					            <display:table name="requestScope.frmLoanerReqEdit.alReqSlipList" requestURI="/gmOprLoanerReqEdit.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.operations.requests.displaytag.beans.DTLoanerEditReqSlipWrapper">						
								<fmtLoanerSetPickSlip:message key="DT_SNO" var="varSno"/>
								<display:column property="NO" title="${varSno}" class="aligncenter"  style="width:30px" />
								<fmtLoanerSetPickSlip:message key="DT_SET_ID" var="varSetId"/>
								<display:column property="SETID" title="${varSetId}" class="alignleft" style="width:60px" /> 
								<fmtLoanerSetPickSlip:message key="DT_SET_DESCRIPTION" var="varDescription"/>
								<display:column property="SETNM" title="${varDescription}" class="alignleft"  style="width:150px" /> 
								<fmtLoanerSetPickSlip:message key="DT_QTY" var="varQty"/>
								<display:column property="QTY" title="${varQty}<br>Dispatched" class="aligncenter" style="width:30px" /> 
								<fmtLoanerSetPickSlip:message key="DT_TAG" var="varTag"/>
								<display:column property="ETCHID1" title="${varTag}" class="alignleft" style="width:80px" /> 							 
								<display:column property="BARCODE" title="" class="aligncenter" style="width:60px" />					
								</display:table>
							</td>											
			    </tr>
				<tr>
					<td  colspan=3>
						<BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
						<table border=0 cellspacing=0 cellpadding=0 width="100%">
							<tr  class=RightTableCaption>
									<td height=40 valign=bottom align=center>________________________</td>
									<td width=200 valign=bottom align=center>________________________</td>
									<td width=200 valign=bottom align=center>____________</td>
							</tr>
							<tr  class=RightTableCaption>
									<td height=24 align=center>&nbsp;<fmtLoanerSetPickSlip:message key="LBL_PRINT_NAME"/></td>
									<td width=200 align=center>&nbsp;<fmtLoanerSetPickSlip:message key="LBL_SIGNATURE"/></td>
									<td width=200 align=center>&nbsp;<fmtLoanerSetPickSlip:message key="LBL_DATE"/></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td bgcolor=#666666 colspan=3 height=1></td>
				</tr>
		</table>
	</td>
	<td bgcolor=#666666 width=1></td>
</tr>
</table>

<%//out.print(sbStartSection);
//out.print(sbEndSection);
%>
<BR><BR><BR><BR>
<table border=0 class="DtTable700" cellspacing=0 cellpadding=0 align=center>
	<tr>
		<td>
			<jsp:include page="/common/GmRuleDisplayInclude.jsp">
			<jsp:param name="Show" value="true" />
			<jsp:param name="Fonts" value="false" />
			</jsp:include>
			<jsp:include page="/common/GmIncludeDateStamp.jsp" />
		</td>
	</tr>
</table>

</body>
</html>