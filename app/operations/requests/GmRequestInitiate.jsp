<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@page import="java.net.URLEncoder"%>
<%
/**********************************************************************************
 * File		 		: GmRequestInitiate.jsp
 * Desc		 		: This screen is used to display 
 * Version	 		: 1.0
 * author			: James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>


<%@ taglib prefix="fmtRequestInit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRequestInitiate.jsp -->
<fmtRequestInit:setLocale value="<%=strLocale%>"/>
<fmtRequestInit:setBundle basename="properties.labels.custservice.ProcessRequest.GmItemInitiate"/>

<bean:define id="distList" name="frmRequestInitiate" property="distList" type="java.lang.String"> </bean:define>
<bean:define id="alAttribute" name="frmRequestInitiate" property="alAttribute" type="java.util.ArrayList"> </bean:define>
<bean:define id="hmReturn" name="frmRequestInitiate" property="hmReturn" type="java.util.HashMap"> </bean:define>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strWikiTitle = GmCommonClass.getWikiTitle("PRODUCT_REQUEST_INITIATE");
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");  
	String strscreentyp = (String)request.getAttribute("screentyp")==null?"":(String)request.getAttribute("screentyp");
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt); 
	String strApplDateFmt = strGCompDateFmt;
	
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean =
	    GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

	String strLoanerReqDay   =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHIP_REQ_DAY"));
	String strShipSurgeryDay   =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHIP_SURGERY_DAY"));
	String strShowLoanAtrbs = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_LOANER_ATRB")); 
	strCountryCode = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
	String strShipCarr = "";
	String strShipMode = "";
	String strShipTo = "";
	if (hmReturn != null && hmReturn.size() > 0) {  		 
		strShipCarr = GmCommonClass.parseNull((String)hmReturn.get("SHIP_CARRIER"));
		strShipMode = GmCommonClass.parseNull((String) hmReturn.get("SHIP_MODE"));
		strShipTo = GmCommonClass.parseNull((String) hmReturn.get("SHIP_TO"));
		 
	}

	// The default value is getting from DB, if there are not value in the rules table, then need to select choose one for other than US company
	//5001: Fedex; 5004: Priority Overnight; 4121:Sales Rep;
	strShipCarr = strShipCarr.equals("") && strCountryCode.equals("en")?"5001" : strShipCarr;
	strShipMode = strShipMode.equals("") && strCountryCode.equals("en")?"5004" : strShipMode;
	strShipTo = strShipTo.equals("") && strCountryCode.equals("en")?"4121" : strShipTo;
	
	if(strLoanerReqDay.equals("")){
		strLoanerReqDay="100000000000";
	}
	if(strShipSurgeryDay.equals("")){
		strShipSurgeryDay="100000000000";
	}
	
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Material Request Initiate </TITLE>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmRequestInitiate.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<SCRIPT>

var Distlist = '<%=distList%>'
var todaysDate = '<%=strTodaysDate%>';
var format = '<%=strApplDateFmt%>';
var reqday ='<%=strLoanerReqDay%>';
var shipday = '<%=strShipSurgeryDay%>';
var attCnt = <%=alAttribute.size()%>;
var strErrorWhType = '';
var strErrorRwQty = '';

var strErrorFG = '';
var strErrorRW = '';

function fnPrintVer()
{
	val = '';
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintLetter()
{
	val = '';
	windowOpener("/GmInHouseSetServlet?hAction=ViewLetter&hConsignId="+val,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPicSlip(val)
{
	windowOpener("<%=strServletPath%>/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
} 
 
</SCRIPT>
</head>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmRequestInitiate.do">
<html:hidden property="strOpt"/>
<html:hidden property="strAction" value="" />
<html:hidden property="hinputString"/> 
<html:hidden property="hrefName"/>
<html:hidden property="hinputstr"/>
<html:hidden property="tissueFl"/>
<%-- <html:hidden property="hWarehouseType"/> --%>
<input type="hidden" name="RE_FORWARD" value="gmRequestInitiate">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" value="REQUESTINITWRAPPER">
<input type="hidden" name="RE_SRC" value="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmRequestItemInitiate&txnStatus=PROCESS&RE_TXN=REQUESTINITWRAPPER">
<input type="hidden" name="hWarehouseType">
<input type="hidden" name="hDisplayRW" value="N">
<input type="hidden" name="haddInfo" value=""> 
	<table  border="0" borderwidth="830" cellspacing="0" cellpadding="0" class="border">		
		<tr>
		<td height="15" colspan="4" class="RightDashBoardHeader">
			<table border="0" width="100%" >
			<tr>
				<td colspan="3" height="15" width="90%" class="RightDashBoardHeader"><fmtRequestInit:message key="LBL_HEADER"/></td>
				<fmtRequestInit:message key="LBL_HELP" var="varHelp"/>
				<td height="15" width="10%"class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
			</tr></table>					
		</td>			
			</tr>
		<tr><td colspan="4" height="1" class="Line"></td></tr>	
		<tr>
		
			<td colspan="4">
				<jsp:include page="/operations/requests/GmRequestHeader.jsp">
				<jsp:param name="FORMNAME" value="frmRequestInitiate" />
				<jsp:param name="SHIPCARR" value="<%=strShipCarr%>" />
				<jsp:param name="SHIPMODE" value="<%=strShipMode%>" />
				<jsp:param name="SHIPTO" value="<%=strShipTo%>" />
				</jsp:include>
			</td>
		</tr>
		<tr>
             <td colspan="4">
             	<jsp:include page="GmIncludeCNAttribute.jsp" >
             		<jsp:param name="FORMNAME" value="frmRequestInitiate" />
             		<jsp:param name="ALATTRIB" value="alAttribute" />
				</jsp:include>
			</td>
        </tr> 
		
		 <tr class="shade" rowspan="2">
		 	 <td width="400px">
		 	 	<table  borderwidth="350" cellspacing="0" cellpadding="0" >
		 	 	<tr >
		 	 		<%if(strCountryCode.equals("en")){ %>
						<td class="RightTableCaption" HEIGHT="25" align="right" width="102" ><font color="red">*</font>&nbsp;<fmtRequestInit:message key="LBL_SALESREP"/>:</td>
					<%}else{ %>
						<td class="RightTableCaption" HEIGHT="25" align="right" width="102" >&nbsp;<fmtRequestInit:message key="LBL_SALESREP"/>:</td>
					<%} %>
		 	 		<td>&nbsp;<select name="Cbo_LoanToRep" tabindex="2" disabled style="width:280px" onChange="javascript:fnSetAssocReps(this.value);"><option value="0" ><fmtRequestInit:message key="LBL_CHOOSEONE" /></select>
					</td>
		 	 	</tr>
		 	 	<tr id="assocRep">
		 	 		<td class="RightTableCaption" HEIGHT="25" align="right" width="102" >&nbsp;<fmtRequestInit:message key="LBL_ASSOCREP"/>:</td>
					<td>&nbsp;<select name="Cbo_LoanToASSORep" tabindex="2"  style="width:280px" onChange="javascript:fnGetAssocRepNames(this,'init');" ><option value="0" ><fmtRequestInit:message key="LBL_CHOOSEONE" /></select>
					</td>
		 	 	</tr>
		 	 	</table>
		 	 </td>
		 	 	
		 	 <%if(strCountryCode.equals("en")){ %>
				<td with="100px" class="RightTableCaption" HEIGHT="25" align="right" >&nbsp;<font color="red">*</font>&nbsp;<fmtRequestInit:message key="LBL_ACCOUNT"/>:</td>
			<%}else{ %>
				<td class="RightTableCaption" HEIGHT="25" align="right" >&nbsp;<fmtRequestInit:message key="LBL_ACCOUNT"/>:</td>
			<%} %>
			<td>&nbsp;<select name="Cbo_LoanToAcc" tabindex="2" disabled style="width:250px" onchange="fnsetDealVal(this.value)"> <option value="0" ><fmtRequestInit:message key="LBL_CHOOSEONE" /></select>
			<input type="checkbox" name="chk_loadAllAcc" tabindex="50" onclick="fnSetAllAccValues(Cbo_LoanToAcc)" title="Load All Accounts" disabled /> 
			</td>
		 </tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		<tr rowspan="2">
			<td width="200px">
				<table  borderwidth="350" cellspacing="0" cellpadding="0" >
					<tr>
					<td class="RightTableCaption" HEIGHT="25" align="right" width="102" ><font color="red">*</font>&nbsp;<fmtRequestInit:message key="LBL_PLANNED"/> <BR> <fmtRequestInit:message key="LBL_SHIPDATE"/>:</td>
					<td >&nbsp;<gmjsp:calendar SFFormName="frmRequestInitiate" controlName="plannedDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>
					
					</tr>
					</table> 
					</td>
				
			<td class="RightTableCaption" HEIGHT="25" align="right" >&nbsp;<fmtRequestInit:message key="LBL_ACCOUNTVALIDATION"/>:</td>
		     <td>&nbsp;<input type="checkbox" name="chk_account" title="Skip Account Validation" tabindex="51"/></td>
				
	</tr>
		<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td colspan="4">
			<div tabIndex="-1">
				<iframe src="/GmCommonCartServlet?hAction=ReqCart&hScreen=<%=strscreentyp%>&companyInfo=<%=strCompanyInfo%>" scrolling="no" id="frmCart" marginheight="0" width="100%" height="380" frameborder="0" HIDEFOCUS="true"></iframe><BR>
			</div>
			</td>
		</tr> 
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<%if(strShowLoanAtrbs.equalsIgnoreCase("yes")) {%>
		<tr><td colspan="4">
			<jsp:include page="/operations/requests/GmAttributeDetails.jsp">	
			<jsp:param name="screenType" value="Initiate" />		
			</jsp:include>	
		</td></tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<%} %>
		<tr><td class="line" colspan="4"></td></tr>
		<tr><td colspan="4"> 
						<jsp:include page="/operations/shipping/GmIncShipDetails.jsp">	
						<jsp:param name="screenType" value="Initiate" />		
								
					</jsp:include>	
				</td></tr>		
              <tr><td class="line" colspan="4"></td></tr>
				<tr><td colspan="4"> <!-- PMT-40240 - Tissue PArt Shipping Details JSP -->
						<jsp:include page="/operations/shipping/GmIncTissueShipDetails.jsp">	
						<jsp:param name="screenType" value="Initiate" />		
								
					</jsp:include>	
				</td></tr>	 
				 <tr><td class="line" colspan="4"></td></tr>
		<tr> 
             <td colspan="4"><div id="comments" >
               	<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="FORMNAME" value="frmRequestInitiate" />
					<jsp:param name="ALNAME" value="alLogReasons" />						
				</jsp:include></div>
			</td>
        </tr> 
         
       
        <tr>
			<td colspan="4" align="center" height="35">
				<fmtRequestInit:message key="BTN_SUBMIT" var = "varSubmit"/>
				<gmjsp:button name="Btn_Submit" value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" buttonType="Save" />&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<tr>
			<td  colspan="4" > 
			 <div id="divf" style="display:none;">
			<iframe src="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmRequestItemInitiate&txnStatus=PROCESS&RE_TXN=REQUESTINITWRAPPER" scrolling="yes" id="iframe1" marginheight="0" width="100%" height="100" frameborder="0"  ></iframe><BR>
			</div>  
	  
			</td></tr>	
	</table>
	<div>
	 
	</div>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
