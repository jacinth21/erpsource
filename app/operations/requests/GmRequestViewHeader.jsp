<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
 
<%@ page import ="com.globus.common.servlets.GmServlet"%>


<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtRequestViewHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRequestViewHeader.jsp -->
<fmtRequestViewHeader:setLocale value="<%=strLocale%>"/>
<fmtRequestViewHeader:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>
<%
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
String strLimitedAccess = GmCommonClass.parseNull(request.getParameter("bolLimitedAccess"));

boolean bolLimitedAccess = false;
if(strLimitedAccess!=null && strLimitedAccess.equalsIgnoreCase("true")){
	bolLimitedAccess = true;
}
%>
<bean:define id="strRequestStatusFlag" name="<%=strFormName%>" property="requestStatusFlag" type="java.lang.String"></bean:define> 
<%
 strFormName = strFormName == "" ? "frmRequestHeader" : strFormName;
 /* Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);  // Instantiating the Logger
 String strCssPath = GmCommonClass.getString("GMSTYLES");*/
 log.debug(" requestStatusFlag " +strRequestStatusFlag +" # # # # "+strLimitedAccess);
 
 String  strViewEdit =  GmCommonClass.parseNull(request.getParameter("hViewEdit"));
 strViewEdit = strViewEdit == "" ? "View" : strViewEdit;
 log.debug(" view edit " + strViewEdit); 
 String  strHeaderView =  GmCommonClass.parseNull(request.getParameter("hRequestView"));
 boolean  blReadOnlyFlag = strViewEdit.equals("Edit") ? false : true;
 strViewEdit = strRequestStatusFlag.equals("40") ? "View" : strViewEdit;
// if(strStatus.equals("Completed")) { strViewEdit = "View"; blReadOnlyFlag = true;}

//ArrayList alDistributor = new ArrayList();
//ArrayList alRepList = new ArrayList();
//ArrayList alAccList = new ArrayList();
ArrayList alSelectedList = new ArrayList();

int intSize = 0;
HashMap hcboVal = null;

String strValueID = "ID";
String strShipDisplableValue = "";

%>
<bean:define id="alDistributor" name="<%=strFormName%>" property="alShipDistributor" type="java.util.ArrayList"></bean:define>
<bean:define id="alRepList" name="<%=strFormName%>" property="alShipSalesRep" type="java.util.ArrayList"></bean:define>
<bean:define id="alAccList" name="<%=strFormName%>" property="alShipHospital" type="java.util.ArrayList"></bean:define>
<bean:define id="alEmpList" name="<%=strFormName%>" property="alShipEmployee" type="java.util.ArrayList"></bean:define>

<bean:define id="selectShipto" name="<%=strFormName%>" property="requestShipTo" type="java.lang.String"></bean:define>
<bean:define id="selectShiptoID" name="<%=strFormName%>" property="requestShipToID" type="java.lang.String"></bean:define>
<bean:define id="historyfl" name="<%=strFormName%>" property="historyfl" type="java.lang.String"></bean:define>
<% 	
	//Sales Rep
	if (selectShipto.equals("4121")) {
		alSelectedList = alRepList;
		 strValueID = "REPID";

	}
	//Hospital
	if (selectShipto.equals("4122")) {
		alSelectedList = alAccList;
	}
	//Employee
	if (selectShipto.equals("4123")) {
		alSelectedList = alEmpList;
	}
	
	// If Distributor , N/A, Choose one default to Disabled
	if (selectShipto.equals("4120") || selectShipto.equals("4124") || selectShipto.equals("0")) {
		strShipDisplableValue = "Disabled";
	}
	String strMandatoryField = "";
	
%>


<BODY leftmargin="20" topmargin="10" > 


<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script>

var RepLen = <%=alRepList.size()%>;
<%
	intSize = alRepList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alRepList.get(i);
%>
	var RepArr<%=i%> ="<%=hcboVal.get("REPID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

var AccLen = <%=alAccList.size()%>;
<%
	intSize = alAccList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alAccList.get(i);
%>
	var AccArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

var EmpLen = <%=alEmpList.size()%>;
<%
	intSize = alEmpList.size();
	hcboVal = new HashMap();
	for (int i=0;i<intSize;i++)
	{
		hcboVal = (HashMap)alEmpList.get(i);
%>
	var EmpArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("NAME")%>";
<%
	}
%>

	function fnLoadReqHeader(obj)
	{
	  	document.<%=strFormName%>.submit();
	}

	function fnCallShip()
	{
		obj = document.all.requestShipTo;
		var obj2 = document.all.requestShipToID;
		if (obj.options[obj.selectedIndex].text == 'Sales Rep')
		{
			obj2.disabled = false;
			obj2.options.length = 0;
			obj2.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<RepLen;i++)
			{
				arr = eval("RepArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				obj2.options[i+1] = new Option(name,id);
			}
			obj2.focus();
		}
		else if (obj.options[obj.selectedIndex].text == 'Hospital')
		{
			obj2.disabled = false;
			obj2.options.length = 0;
			obj2.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<AccLen;i++)
			{
				arr = eval("AccArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				obj2.options[i+1] = new Option(name,id);
			}
			obj2.focus();
		}
		else if (obj.options[obj.selectedIndex].text == 'Employee')
		{
			obj2.disabled = false;
			obj2.options.length = 0;
			obj2.options[0] = new Option("[Choose One]","0");
			for (var i=0;i<EmpLen;i++)
			{
				arr = eval("EmpArr"+i);
				arrobj = arr.split(",");
				id = arrobj[0];
				name = arrobj[1];
				obj2.options[i+1] = new Option(name,id);
			}
			obj2.focus();
		}
		else
		{
			obj2.options.length = 0;
			obj2.options[0] = new Option("[Choose One]","0");
			obj2.disabled = true;
		}
	}
 
function fnReqHistory(reqId)
{
	  
	<%--  varRequestId = document.<%=strFormName%>.requestId.value; --%>
	  
	windowOpener("/GmPageControllerServlet?strPgToLoad=/gmAuditTrail.do&auditId=1006&txnId="+reqId,"","resizable=yes,scrollbars=yes,top=250,left=300,width=785,height=300,status=1");
  }


 
</script>
 
<table width="100%" >
<%  if ( !strHeaderView.equals("headerView") ) { %>
<tr>
	<td class="RightTableCaption" HEIGHT="20" width="15%" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_REQID"/>:</td>
	<!-- Struts tag lib code modified for JBOSS migration changes -->
	<td>&nbsp;<html:text property="requestId"  size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> </td> 
	<td class="RightTableCaption" align="right">  <fmtRequestViewHeader:message key="LBL_ORCNIND"/>: </td>
	<td>
		<table cellspacing="0" cellpadding="0" width="100%" border="0" >
			<tr>
				<td >&nbsp;<html:text property="consignID"  size="30" onfocus="changeBgColor(this,'#AACCE8');"  styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
	<%if(!bolLimitedAccess){ %>
	<fmtRequestViewHeader:message key="LBL_GO" var="varGo"/>
	<td>&nbsp;<gmjsp:button value="  ${varGo }   " gmClass="button"  onClick="fnLoad();" buttonType="Load" />&nbsp;&nbsp;</td>	
	<%} %>		
	</tr>
	</table>
	</td>
<tr><td  bgcolor="#666666" colspan="6"></td></tr>				
</tr>
<% } %>	
</table>
<table cellspacing="0" cellpadding="0" width="100%">
<tr class="shade">
<td class="RightTableCaption" HEIGHT="20" width="25%" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_REQUESTDATE"/>:</td>
<td width="25%">&nbsp;<bean:write name="<%=strFormName%>" property="requestDate" />   </td>

<td colspan="2">&nbsp;</td>
</tr>

<tr>
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_SOURCE"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="requestSource" />   </td>

<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_REQUESTTRANSACTIONID"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="requestTxnID" />   </td>

</tr>

<tr class="shade">
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_SETID"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="setID" />   </td>

<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_SETNAME"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="setName" />   </td>

</tr>


<tr>
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_REQUESTTYPE"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="requestType" />   </td>
<%if(!bolLimitedAccess){ %>
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_REQUESTTO"/>:</td>

<%  if ( strViewEdit.equals("View") ) { %>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="requestTo" />   </td>
<% } %>

<%  if ( strViewEdit.equals("Edit") ) {
	strMandatoryField = "<font color='red'>*</font>"; %>
<td>&nbsp;<gmjsp:dropdown controlName="reqTo" SFFormName="<%=strFormName%>" SFSeletedValue="reqTo" width="250"
							SFValue="alRequestTo" codeId = "ID" onChange="javascript:fnSetShipTo();" codeName = "NAME" defaultValue= "[Choose One]" />	
							  </td>
<% }} %>
</tr>



<tr class="shade">
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_REQUESTSTATUS"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="requestStatus" />   </td>
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_REQUESTFOR"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="requestFor" />   </td>

</tr>

<tr>
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_PLANNEDSHIPDATE"/>:</td>
<%  if ( strViewEdit.equals("Edit") ) { %>
<td>&nbsp;<gmjsp:calendar SFFormName="<%=strFormName%>" controlName="planShipDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					</td>
 										
<% }else {%>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="planShipDate" /></td>
<% } %>
<td class="RightTableCaption" HEIGHT="20" width="25%" align="right"><%=strMandatoryField%>&nbsp;<fmtRequestViewHeader:message key="LBL_REQUIREDDATE"/>:</td>
<% if ( strViewEdit.equals("Edit") ) { %>
<td width="25%">&nbsp;<gmjsp:calendar SFFormName="<%=strFormName%>" controlName="requiredDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp;
<% }else {%>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="requiredDate" />
<% } %>
<%  if ( historyfl.equals("Y") ) { %>
<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnReqHistory('<bean:write name="<%=strFormName%>" property="requestId" />');" 
	title="Click to open required date history" src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=18 width=18 />&nbsp;	
<% } %>
 </td>
</tr>



<logic:notEqual name="<%=strFormName%>" property="requestStatusFlag" value="10">
<%  if ( strViewEdit.equals("View") ) { %>

<tr><td  bgcolor="#666666" colspan="6"></td></tr>
<tr>
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_CONSIGNMENTID"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="consignID" />   </td>
<td></td>
<td></td>
</tr>
<tr class="shade">
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_SETNAME"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="setName" />   </td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>


<tr>
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_INITIATEDBY"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="iniBy" />   </td>
<td  class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_INITIATEDDATE"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="iniDate" />   </td>

</tr>
<tr class="shade">
<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_LASTUPDATEDBY"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="lastUpdNm" />   </td>

<td class="RightTableCaption" HEIGHT="20" align="right">&nbsp;<fmtRequestViewHeader:message key="LBL_LASTUPDATEDDATE"/>:</td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="lastUpdDt" />   </td>

</tr>

<tr>
<td class="RightTableCaption" HEIGHT="20" align="right" > &nbsp;<fmtRequestViewHeader:message key="LBL_CONSIGNMENTSTATUS"/>: </td>
<td>&nbsp;<bean:write name="<%=strFormName%>" property="consignFlag" /></td>
</tr>



<% } %>
</logic:notEqual>
</table>
<%@ include file="/common/GmFooter.inc"%>
