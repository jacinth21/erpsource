<%
	/**********************************************************************************
	 * File		 		: GmRequestSwap.jsp
	 * Desc		 		: This screen is used for the displaying requests which need to be swapped
	 * Version	 		: 1.0
	 * author			: Xun
	 * 
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>

<%@ taglib prefix="fmtRequestSwap" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\requests\GmRequestSwap.jsp -->

<fmtRequestSwap:setLocale value="<%=strLocale%>"/>
<fmtRequestSwap:setBundle basename="properties.labels.operations.requests.GmRequestSwap"/>

<bean:define id="returnList" name="frmRequestSwap" property="returnList" type="java.util.List"></bean:define>
<bean:define id="childList" name="frmRequestSwap" property="childList" type="java.util.List"></bean:define>
 
<%
	ArrayList alList = new ArrayList();
	ArrayList alchildList = new ArrayList();
	alList = GmCommonClass.parseNullArrayList((ArrayList) returnList);
	alchildList = GmCommonClass.parseNullArrayList((ArrayList) childList);
	int rowsize = alList.size();
	int rowchildsize = alchildList.size();
	String strWikiTitle = GmCommonClass.getWikiTitle("REQUEST_SWAP");
	String strApplDateFmt = strGCompDateFmt;
	String strDateFmt = "{0,date,"+strApplDateFmt+"}";
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Create MA</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"
	media="print" />

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
var lblFromRequestId = '<fmtRequestSwap:message key="LBL_FROM_REQUEST_ID"/>';
var lblToRequestId = '<fmtRequestSwap:message key="LBL_REQ_ID"/>';
function fnReload()
{  
    
    fnValidateTxtFld('requestFrom',lblFromRequestId);
    fnValidateTxtFld('requestTo',lblToRequestId);
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	} 
	document.frmRequestSwap.strOpt.value = "reload";
	fnStartProgress('Y');
	document.frmRequestSwap.submit();  
  
}

 
  
function fnSubmit()
{
	fnValidateTxtFld('txt_LogReason',message_operations[151]); 
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	} 
 	document.frmRequestSwap.strOpt.value = 'save';
 	fnStartProgress('Y');
	document.frmRequestSwap.submit();
	 
}

function fnPrintVer(strConId)
{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+strConId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnViewDetails(strReqId,strConId)
 {
    
      windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+strReqId+"&consignID="+strConId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
    }
 
 function fnOpenOrdLogInv(id)
{
	
	windowOpener("/GmCommonLogServlet?hType=1235&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}   
 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmRequestSwap.do">
	<html:hidden property="strOpt" value="" />

	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtRequestSwap:message key="LBL_REQ_SWAP_VALIDATION"/></td>
			<td  height="25" class="RightDashBoardHeader" align="right">
			<fmtRequestSwap:message key="IMG_ALT_HELP" var = "varHelp"/>
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
			<td bgcolor="#666666" height="1" colspan="5"></td>
		</tr>
		<tr>
			<td height="35" class="RightTableCaption">&nbsp;&nbsp;&nbsp;&nbsp;<fmtRequestSwap:message key="LBL_FROM_REQUEST_ID"/>:</td>
			<td>&nbsp;<html:text property="requestFrom"  size="20" tabindex="1" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>
			<td height="35" class="RightTableCaption"><fmtRequestSwap:message key="LBL_REQ_ID"/>:</td>
			<td>&nbsp;<html:text property="requestTo"  size="20" tabindex="2" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/></td>

			<TD width=100 align="center"><fmtRequestSwap:message key="BTN_GO" var="varGo"/>
			<gmjsp:button buttonType="Load"	value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" onClick="javascript:fnReload();"
				tabindex="3" gmClass="button" /></TD>
		</tr>
		<%
			// PC-2319: Request swap changes.
			// if from request voided, we need to show to request information.
			// So, added the rowchildsize > 0 condition
			
			if (rowsize > 0 || rowchildsize > 0) {
		%>
		<tr>
			<td class="Line" height="1" colspan="5"></td>
		</tr>
		 
		<tr>
			<td colspan="5"><display:table	name="requestScope.frmRequestSwap.returnList" class="its" id="currentRowObject"     requestURI="/gmRequestSwap.do" decorator="com.globus.displaytag.beans.DTRequestSwapWrapper">				
				<display:column property="MASTER_CHILD" title=""   class="alignleft" />
				<fmtRequestSwap:message key="DT_REQ_ID" var="varReqId"/>
				<display:column property="REQID" title="${varReqId}"  style="width: 110px;" class="alignleft" />
				<fmtRequestSwap:message key="DT_CONSIGN_ID" var="varConsignID"/>
				<display:column property="CONSID" title="${varConsignID}" style="width: 90px;" class="alignleft" />
				<fmtRequestSwap:message key="DT_SET_ID" var="varSetID"/>
				<display:column property="SETID" title="${varSetID}" class="alignleft" />
				<fmtRequestSwap:message key="DT_SET_NAME" var="varSetName"/>
				<display:column property="SETNM" title="${varSetName}" style="width: 220px;" class="alignleft" />
				<fmtRequestSwap:message key="DT_SHEET_NAME" var="varSheetName"/>				
		        <display:column property="TXNNM" title="${varSheetName}" style="width: 150px;" class="alignleft" />
				<fmtRequestSwap:message key="DT_REQ_DATE" var="varReqDate"/>
				<display:column property="REQTDATE" title="${varReqDate}" class="alignleft" format="<%=strDateFmt%>" />
				<fmtRequestSwap:message key="DT_REQUIRED_DATE" var="varRequiredDate"/>
				<display:column property="REQDATE" title="${varRequiredDate}" class="alignleft" format="<%=strDateFmt%>" /> 
				<fmtRequestSwap:message key="DT_SOURCE" var="varSource"/>
				<display:column property="REQSOURCE" title="${varSource}"  	class="alignleft"  />
			    <fmtRequestSwap:message key="DT_STATUS" var="varStatus"/>
			    <display:column property="REQSTATUS" title="${varStatus}" class="alignleft" />
			 
			</display:table></td>
		</tr>

		<tr>
			<td class="LLine" height="1" colspan="5"></td>
		</tr>
		 
		<tr>
			<td colspan="5" align="center">&nbsp;&nbsp; <p>&nbsp;</p>  
                    
         </td>
		</tr>               
		 <%
			if (rowchildsize > 0) {
		%>
		<tr>
			<td class="LLine" height="1" colspan="5"></td>
		</tr>
		<tr>
			<td colspan="5"><display:table	name="requestScope.frmRequestSwap.childList" class="its" id="currentRowObject" requestURI="/gmRequestSwap.do"  decorator="com.globus.displaytag.beans.DTRequestSwapWrapper">
				<display:column property="MASTER_CHILD" title=""   class="alignleft" />
				<fmtRequestSwap:message key="DT_REQ_ID" var="varReqID"/>
				<display:column property="REQID" title="${varReqID}"  style="width: 110px;"  class="alignleft" />
			 	<fmtRequestSwap:message key="DT_CONSIGN_ID" var="varConsignID"/>
			 	<display:column property="CONSID" title="${varConsignID}" style="width: 90px;"  class="alignleft" />
				<fmtRequestSwap:message key="DT_SET_ID" var="varSetID"/>
				<display:column property="SETID" title="${varSetID}" class="alignleft" />
				<fmtRequestSwap:message key="DT_SET_NAME" var="varSetName"/>
				<display:column property="SETNM" title="${varSetName}" style="width: 220px;" class="alignleft" />
				<fmtRequestSwap:message key="DT_SHEET_NAME" var="varSheetName"/>
				<display:column property="TXNNM" title="${varSheetName}" style="width: 150px;" class="alignleft" />
				<fmtRequestSwap:message key="DT_REQ_DATE" var="varReqDate"/>
				<display:column property="REQTDATE" title="${varReqDate}" class="alignleft" format="<%=strDateFmt%>" />
				<fmtRequestSwap:message key="DT_REQUIRED_DATE" var="varRequiredDate"/>
				<display:column property="REQDATE" title="${varRequiredDate}" class="alignleft" format="<%=strDateFmt%>" /> 
				<fmtRequestSwap:message key="DT_SOURCE" var="varSource"/>
				<display:column property="REQSOURCE" title="${varSource}"  	class="alignleft"  />
			    <fmtRequestSwap:message key="DT_STATUS" var="varStatus"/>
			    <display:column property="REQSTATUS" title="${varStatus}" class="alignleft" />
			 
			</display:table></td>
		</tr>
		
		<%
			}
		 
			}
		%> 
		
		<%
			if (rowsize > 0) {
		%>
		<logic:notEqual  name="frmRequestSwap" property="swapflag" value="Y">
		<tr><td colspan="5" class="ELine"></td></tr> 
                    	<tr>
						<td colspan="5" align="center" height="30">&nbsp;
	                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmRequestSwap" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>	
						</td>
					</tr>
		
		<tr>
			<td colspan="5" align="center">&nbsp;&nbsp; <p>&nbsp;</p>  
			<fmtRequestSwap:message key="BTN_SUBMIT" var="varSubmit"/>
                         <gmjsp:button value="${varSubmit}" gmClass="button"   onClick="fnSubmit();" buttonType="Save" /> 
         </td>
		</tr> 
		</logic:notEqual>	
		<%
			}
		  
		%>    
	</table>
	</FORM>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>