<%@ include file="/common/GmHeader.inc" %>
 
<%@ page import ="java.util.HashMap"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtRequestHeader" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRequestHeader.jsp -->
<fmtRequestHeader:setLocale value="<%=strLocale%>"/>
<fmtRequestHeader:setBundle basename="properties.labels.custservice.ProcessRequest.GmItemInitiate"/>

<%	/*********************************
	 * GmRequestHeader.jsp
	 *********************************/ 
	String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
	String strSetViewFl = GmCommonClass.parseNull((String)request.getParameter("SETVIEW"));
	String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
	//log.debug(" FormName is " + strFormName);
	String strJSONString = "{\"STROPT\":\""+strOpt+"\"}";
	int intSize = 0;
	HashMap hmValue = new HashMap();
	ArrayList alRemoveCodeIDs = new ArrayList();
	String strShipCarrDef = GmCommonClass.parseNull(request.getParameter("SHIPCARR"));
	String strShipModeDef = GmCommonClass.parseNull(request.getParameter("SHIPMODE"));
	String strShipToDef = GmCommonClass.parseNull(request.getParameter("SHIPTO"));
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean =
	    GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	
	String strShowLoanAtrb = gmResourceBundleBean.getProperty("COUNTRYCODE");
				
%>
<bean:define id="alInHousePurpose" name="<%=strFormName%>" property="alInHousePurpose" type="java.util.ArrayList"> </bean:define>
<%
// GOP-4: When create Restock from ETL , Restock po type should not display in PO type drop down in Raise PO screen only.
if(!strFormName.equals("frmRequestInitiate")){
	alRemoveCodeIDs.add("4000097"); // Restock Type: 4000095 
	alInHousePurpose = GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(alRemoveCodeIDs,alInHousePurpose));
}
%>
<bean:define id="alRepList" name="<%=strFormName%>" property="alRepList" type="java.util.ArrayList"> </bean:define>
<bean:define id="alAccList" name="<%=strFormName%>" property="alAccountList" type="java.util.ArrayList"> </bean:define>
<bean:define id="alEmpList" name="<%=strFormName%>" property="alEmployeeList" type="java.util.ArrayList"> </bean:define>
<bean:define id="strType" name="<%=strFormName%>" property="consignmentType" type="java.lang.String"> </bean:define>
<bean:define id="strShipTo" name="<%=strFormName%>" property="shipTo" type="java.lang.String"> </bean:define>
<bean:define id="strShipToId" name="<%=strFormName%>" property="employeeId" type="java.lang.String"> </bean:define>
<bean:define id="alDistributorList" name="<%=strFormName%>" property="alDistributorList" type="java.util.ArrayList"> </bean:define>
<bean:define id="strBillToId" name="<%=strFormName%>"  property="distId" type="java.lang.String"></bean:define>
<bean:define id="strBillToNm" name="<%=strFormName%>"  property="searchdistributorId" type="java.lang.String"></bean:define>
<bean:define id="strSetId" name="<%=strFormName%>"  property="setId" type="java.lang.String"></bean:define>
<bean:define id="strSetNm" name="<%=strFormName%>"  property="searchsetId" type="java.lang.String"></bean:define>
<bean:define id="hmPurpose" name="<%=strFormName%>"  property="hmPurposes" type="java.util.HashMap"></bean:define>

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmSetInitiateHeader.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var vshipMode = '<%=strShipModeDef%>';
var vshipCarrier = '<%=strShipCarrDef%>';
var vshipTo = '<%=strShipToDef%>';
var showloan = '<%=strShowLoanAtrb%>'; 
<%
	ArrayList alInhousePurpose = new ArrayList();
	ArrayList alConsignmentPurpose = new ArrayList();
	HashMap hcboVal = null;
	
	if (hmPurpose!= null)
	{  
		alInhousePurpose = (ArrayList) hmPurpose.get("INHOUSEPUR");
		alConsignmentPurpose = (ArrayList) hmPurpose.get("CONSIGNPUR");
	}

%>
 
var consignLen= <%=alConsignmentPurpose.size()%>;
var inhouseLen = <%=alInhousePurpose .size()%>;
var distLen = <%=alDistributorList.size()%>;

<%
	hcboVal = new HashMap();
	for (int i=0;i<alConsignmentPurpose.size();i++)
	{
		hcboVal = (HashMap)alConsignmentPurpose.get(i);
%>
	var ConArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<%
	}
%>

<%	
	hcboVal = new HashMap();
	for (int i=0;i<alInhousePurpose.size();i++)
	{
		hcboVal = (HashMap)alInhousePurpose.get(i);
%>
	var InhouseArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%> ";
<%
	}
%>
 
<%	
hcboVal = new HashMap();
for (int i=0;i<alDistributorList.size();i++)
{
	hcboVal = (HashMap)alDistributorList.get(i);
%>
var DistDivArr<%=i%> ="<%=hcboVal.get("ID")%>,<%=hcboVal.get("DDIV")%> ";
<%
}
%>

function fnLoad()
{

	document.all.assocRep.style.display = "none";
	var obj = document.all.consignmentType;
 	var searchdistIdobj = document.all.searchdistId;
	if (obj.options[obj.selectedIndex].text == 'In-House Consignment')
	{
		searchdistIdobj.value = 'Globus Med-In House';
		document.all.distId.value = '01';
			if(document.frmSetInitiate.distributorId != undefined ){
					document.frmSetInitiate.distributorId.value = '01';
			}
	}
		var objfrm =document.getElementById("frmCart").contentWindow;
		var InHouseFl = '<%=strType%>';
		var ShipToFl = '<%=strShipTo%>';
		document.all.inHousePurpose.value = '50060';  //50060-Regular
		if (InHouseFl == '40022')
		{
			document.all.inHousePurpose.disabled = false;
			document.all.names.disabled = false;
			document.all.distributorId.selectedIndex = 0;
			fnCallType('');
			fnSetValue();
			document.frmRequestInitiate.strRaFlag.checked = false;
			document.frmRequestInitiate.strRaFlag.disabled = true;
		}
		
		if (InHouseFl == '40021' || InHouseFl == '102930'){ // 40021 : Consignment 102930 - ICT
			// MNTTASK - 8878: When click left link or when select type consignment it will be select Regular in Purpose drop down.
			document.all.inHousePurpose.value = 50060; // 50060 - Regular
		}
		
		if(InHouseFl == '4127')
		{
			fnCallType('');
			document.all.shipCarrier.value="<%=strShipCarrDef%>";
			document.all.shipMode.value="<%=strShipModeDef%>";
			document.all.shipTo.value="<%=strShipToDef%>";
			fnGetShipModes(document.all.shipCarrier,''); //To get the ship mode of selected carrier
			document.frmRequestInitiate.strRaFlag.checked = false;
			document.frmRequestInitiate.strRaFlag.disabled = true;
						
		}
		if (ShipToFl != '4120')
		{
			fnCallShip();
		//	fnSetValue();
		}	

		if(InHouseFl == '40021')
		{
			
			objfrm.fnRemoveDateReq();    //Remove the Date Req field.
			//if OUS, set default value for ship mode, ship carrier and ship to
			if (showloan != 'en'){
				document.all.shipCarrier.value="<%=strShipCarrDef%>";
				document.all.shipMode.value="<%=strShipModeDef%>";
			}
			 
		}
		else if(InHouseFl == '40022')
		{                           
			
			objfrm.fnShowDateReq();//Display the Date Req field
			//if OUS, set default value for ship mode, ship carrier and ship to
			if (showloan != 'en'){
				document.all.shipCarrier.value="<%=strShipCarrDef%>";
				document.all.shipMode.value="<%=strShipModeDef%>";
				document.all.shipTo.value="<%=strShipToDef%>";
			}
		}
		fnDisable(document.all.consignmentType.value);
}
		

function fnSetValue()
{
	var ShipToId = '<%=strShipToId%>';

	var obj5 = document.all.names;
	for (var j=0;j<obj5.length;j++)
	{
		if (obj5.options[j].value == ShipToId)
		{
			obj5.options[j].selected = true;
			break;
		}
	}
}

window.onbeforeunload = function () {
	document.all.consignmentType.value = '40021';
	document.all.inHousePurpose.value = '0';
	document.all.distributorId.value = '0';
	document.all.shipTo.value = '0';
	document.all.names.value = '0';
}
function fnDisable(strVal){
	//1006420
	var attrib = eval("document.frmRequestInitiate.Txt_1006420");
	if(attrib != undefined){
		if(strVal == '40021'){
			attrib.disabled = false;
		}else{
			attrib.disabled = true;
		}
	} 
}
//fnloadAcc - once call the auto complete - to update the account, currency and collector information. 
function fnloadDist() {
	var distid = document.all.distId.value;	
	if (distid != "0" && distid != '') {
		document.all.distributorId.value = distid;		
	} 
}

</script>
<html:hidden property="deptId" />
<BODY onLoad="javascript:fnLoad();">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
	<tr>
		<td colspan="2">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<!-- Custom tag lib code modified for JBOSS migration changes -->
				<tr>
					<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<font color="red">* </font><fmtRequestHeader:message key="LBL_REQUESTTYPE"/>:</td>
					<td>&nbsp;<gmjsp:dropdown controlName="consignmentType" SFFormName="<%=strFormName%>" SFSeletedValue="consignmentType" onChange="javascript:fnCallType(this);fnDisable(this.value);"
							SFValue="alConsignmentType" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" />
					</td>
					<td class="RightTableCaption" align="right"><fmtRequestHeader:message key="LBL_PURPOSE" />:</td>
					<td width="290">&nbsp;<gmjsp:dropdown controlName="inHousePurpose" SFFormName="<%=strFormName%>" SFSeletedValue="inHousePurpose"
							SFValue="alInHousePurpose" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" onChange="javascript:fnSetSelectPurpose();"/>
					</td>
				</tr>
				<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
				<tr  class="Shade">
					<td class="RightTableCaption" HEIGHT="23" align="right"><div id="blto" style="height=23">&nbsp;<fmtRequestHeader:message key="LBL_BILLTO" />:</div></td>
					<td style="margin-top: -10px; position:absolute;">&nbsp;
							<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="distId" />
							<jsp:param name="METHOD_LOAD" value="loadFieldSalesList&searchType=LikeSearch" />
							<jsp:param name="WIDTH" value="300" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value=""/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%=strBillToNm%>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strBillToId%>" />
							<jsp:param name="SHOW_DATA" value="100" />
							<jsp:param name="AUTO_RELOAD" value="fnloadDist();" />
							</jsp:include>
							  
					</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>

					<tr>
					<% if (strSetViewFl.equals("Y")) { %>
						<td class="RightTableCaption" HEIGHT="23" align="right">&nbsp;<fmtRequestHeader:message key="LBL_PLANNEDSHIPDATE" />:
						</td>
						<td>&nbsp;<gmjsp:calendar SFFormName="<%=strFormName%>"
								controlName="planShipDate" gmClass="InputArea"
								onFocus="changeBgColor(this,'#AACCE8');"
								onBlur="changeBgColor(this,'#ffffff');" /></td>
					<% } %>
						<td class="RightTableCaption" HEIGHT="23" align="right"><div id="date1" style="height=23"> &nbsp;<fmtRequestHeader:message key="LBL_REQUIREDDATE" />:</div></td>
					    <td>&nbsp;<gmjsp:calendar SFFormName="<%=strFormName%>" controlName="requiredDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					    </td>
					    <% if (!strSetViewFl.equals("Y")) { %>	
					    <td class="RightTableCaption" colspan="2">Initiate RA &nbsp;<input type="checkbox" name="strRaFlag" title="Initiate RA"  value="Y"/></td>
						<%} %>
					</tr>
					<%
						if (strSetViewFl.equals("Y")) {
					%>
					<tr class="Shade">
						<td class="RightTableCaption" HEIGHT="23" 
							align="right">&nbsp;<fmtRequestHeader:message key="LBL_SETID" />:</td>
						<td style="margin-top: -10px; position:absolute;">&nbsp;
						<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
						<jsp:param name="CONTROL_NAME" value="setId" />
						<jsp:param name="PARAMETER" value="<%= strJSONString%>" />
						<jsp:param name="METHOD_LOAD" value="loadSetIdNameList&searchType=LikeSearch" />
						<jsp:param name="WIDTH" value="500" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value=""/>
						<jsp:param name="CONTROL_NM_VALUE" value="<%=strSetNm %>" /> 
						<jsp:param name="CONTROL_ID_VALUE" value="<%=strSetId %>" />
						<jsp:param name="SHOW_DATA" value="100" />
						<jsp:param name="AUTO_RELOAD" value="" />					
						</jsp:include>  
								<%-- <gmjsp:dropdown controlName="setId" SFFormName="<%=strFormName %>" SFSeletedValue="setId" SFValue="alSetList"
								codeId="ID" codeName="IDNAME" defaultValue="[Choose One]"/> --%>
						</td>
						<td></td>
						<td></td>
						<td><gmjsp:button value="&nbsp;Load&nbsp;" gmClass="button" onClick="javascript:fnFetchSetInfo();" buttonType="Load" />
						&nbsp;<span style="display: none;"><gmjsp:dropdown width="0" controlName="distributorId" SFFormName="<%=strFormName%>" SFSeletedValue="distributorId"
							SFValue="alDistributorList" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]" onChange="javascript:fnSetShipTo(this);"/>
							</span></td>
					<td colspan="2">&nbsp;</td>
					</tr>
					<%} %>
				<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
				
			</table>
		</td>
	</tr>
</table>
</BODY>