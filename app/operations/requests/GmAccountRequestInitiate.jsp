<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmRequestInitiate.jsp
 * Desc		 		: This screen is used to display 
 * Version	 		: 1.0
 * author			: James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>

<%@ taglib prefix="fmtAccountInitiate" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmAccountRequestInitiate.jsp -->
<fmtAccountInitiate:setLocale value="<%=strLocale%>"/>
<fmtAccountInitiate:setBundle basename="properties.labels.custservice.ProcessRequest.GmLoanerProcessRequest"/>

<bean:define id="distList" name="frmAccountRequestInitiate" property="distList" type="java.lang.String"> </bean:define>
<bean:define id="alAttribute" name="frmAccountRequestInitiate" property="alAttribute" type="java.util.ArrayList"> </bean:define>
<bean:define id="hmReturn" name="frmAccountRequestInitiate" property="hmReturn" type="java.util.HashMap"> </bean:define>
<%@page import="java.net.URLEncoder"%>
<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strWikiTitle = GmCommonClass.getWikiTitle("PRODUCT_REQUEST_INITIATE");
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strApplDateFmt = strGCompDateFmt;
	String strTodaysDate = GmCalenderOperations.getCurrentDate(strGCompDateFmt); 
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strLoanerReqDay   =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHIP_REQ_DAY"));
	String strShipSurgeryDay   =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHIP_SURGERY_DAY"));
	String strShowLoanAtrbs = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_LOANER_ATRB")); 
	strCountryCode = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COUNTRYCODE"));
	String strShipCarr = "";
	String strShipMode = "";
	String strShipTo = "";
	if (hmReturn != null && hmReturn.size() > 0) {  		 
		strShipCarr = GmCommonClass.parseNull((String)hmReturn.get("SHIP_CARRIER"));
		strShipMode = GmCommonClass.parseNull((String) hmReturn.get("SHIP_MODE"));
		strShipTo = GmCommonClass.parseNull((String) hmReturn.get("SHIP_TO"));
		 
	}

	strShipCarr = strShipCarr.equals("")?"5001" : strShipCarr;
	strShipMode = strShipMode.equals("")?"5004" : strShipMode;
	strShipTo = strShipTo.equals("")?"4121" : strShipTo;
	
	if(strLoanerReqDay.equals("")){
		strLoanerReqDay="100000000000";
	}
	if(strShipSurgeryDay.equals("")){
		strShipSurgeryDay="100000000000";
	}
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Material Request Initiate </TITLE>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmAccountRequestInitiate.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<SCRIPT>

 
var Distlist = '<%=distList%>'
var todaysDate = '<%=strTodaysDate%>';
var format = '<%=strApplDateFmt%>';
var reqday ='<%=strLoanerReqDay%>';
var shipday = '<%=strShipSurgeryDay%>';
var attCnt = <%=alAttribute.size()%>;
var strErrorWhType = '';
var strErrorRwQty = '';

var strErrorFG = '';
var strErrorRW = '';

function fnPrintVer()
{
	val = '';
	windowOpener("/GmInHouseSetServlet?hAction=ViewPrint&hConsignId="+val,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPrintLetter()
{
	val = '';
	windowOpener("/GmInHouseSetServlet?hAction=ViewLetter&hConsignId="+val,"LoanPrint","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnPicSlip(val)
{
	windowOpener("<%=strServletPath%>/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
} 
 
</SCRIPT>
</head>
<BODY leftmargin="20" topmargin="10">
<html:form action="/gmAccountRequestInitiate.do">
<html:hidden property="strOpt"/>
<html:hidden property="strAction" value="" />
<html:hidden property="hinputString"/> 
<html:hidden property="hrefName"/>
<html:hidden property="hinputstr"/>
<%-- <html:hidden property="hWarehouseType"/> --%>
<input type="hidden" name="RE_FORWARD" value="gmAccountRequestInitiate">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" value="REQUESTINITWRAPPER">
<input type="hidden" name="RE_SRC" value="/gmRuleEngine.do?companyInfo=<%=strCompanyInfo%>&RE_FORWARD=gmRequestItemInitiate&txnStatus=PROCESS&RE_TXN=REQUESTINITWRAPPER">
<input type="hidden" name="hWarehouseType">
<input type="hidden" name="hDisplayRW" value="N">
<input type="hidden" name="haddInfo" value=""> 

	<table  border="0" borderwidth="830" cellspacing="0" cellpadding="0" class="border">		
		<tr>
		<td height="15" colspan="4" class="RightDashBoardHeader">
			<table border="0" width="100%" >
			<tr>
				<td colspan="3" height="15" width="90%" class="RightDashBoardHeader"><fmtAccountInitiate:message key="LBL_MATERIALREQUEST"/></td>
				<fmtAccountInitiate:message key="LBL_HELP" var="varHelp"/>
				<td height="15" width="10%"class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
			</tr></table>					
		</td>			
			</tr>
		<tr><td colspan="4" height="1" class="Line"></td></tr>	
		<tr>
		
			<td colspan="4">
				<jsp:include page="/operations/requests/GmAccountRequestHeader.jsp">
				<jsp:param name="FORMNAME" value="frmAccountRequestInitiate" />
				<jsp:param name="SHIPCARR" value="<%=strShipCarr%>" />
				<jsp:param name="SHIPMODE" value="<%=strShipMode%>" />
				<jsp:param name="SHIPTO" value="<%=strShipTo%>" />
				</jsp:include>
			</td>
		</tr>
		<tr>
            <td colspan="4">
             	<jsp:include page="GmIncludeCNAttribute.jsp" >
             		<jsp:param name="FORMNAME" value="frmAccountRequestInitiate" />
             		<jsp:param name="ALATTRIB" value="alAttribute" />
				</jsp:include>
	        </td>
        </tr>
				
		 <tr class="shade">
		 	 <td>
		 	 	<table  border="0" cellspacing="0" cellpadding="0" width="100%">		 	 	
		 	 	<tr id="assocRep" >		 	 		
		 	 	</tr>
		 	 	</table>
		 	 </td>		 	
		 </tr>
		<tr><td colspan="4" height="1" class="LLine"></td></tr>		
		<tr>
			<td colspan="4">
			<div tabIndex="-1">
				<iframe src="/GmCommonCartServlet?hAction=ReqCart&hScreen=Account&companyInfo=<%=strCompanyInfo%>" scrolling="no" id="frmCart" marginheight="0" width="100%" height="380" frameborder="0" HIDEFOCUS="true"></iframe><BR>
			</div>
			</td>
		</tr> 
		<tr><td colspan="4" class="Line" height="1"></td></tr>		
		<tr><td colspan="4"> 
						<jsp:include page="/operations/shipping/GmIncShipDetails.jsp">	
						<jsp:param name="screenType" value="Initiate" />		
								
					</jsp:include>	
				</td></tr>		
		<tr><td class="line" colspan="4"></td></tr>
		<tr>
             <td colspan="4"><div id="comments" >
               	<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="FORMNAME" value="frmAccountRequestInitiate" />
					<jsp:param name="ALNAME" value="alLogReasons" />						
				</jsp:include></div>
			</td>
        </tr> 
         
       
        <tr>
			<td colspan="4" align="center" height="35">
			<fmtAccountInitiate:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button name="Btn_Submit" value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" buttonType="Save" />&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<tr>
			<td  colspan="4" > 
			 <div id="divf" style="display:none;">
			<iframe src="/gmRuleEngine.do?RE_FORWARD=gmRequestItemInitiate&txnStatus=PROCESS&RE_TXN=REQUESTINITWRAPPER" scrolling="yes" id="iframe1" marginheight="0" width="100%" height="100" frameborder="0"  ></iframe><BR>
			</div>  
	  
			</td></tr>	
	</table>
	<div>
	 
	</div>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
