<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: GmSetInitiate.jsp
 * Desc		 		: Set Initiate transaction
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ taglib prefix="fmtSetInitiate" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmSetInitiate.jsp -->
<fmtSetInitiate:setLocale value="<%=strLocale%>"/>
<fmtSetInitiate:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>
<bean:define id="distList" name="frmSetInitiate" property="distList" type="java.lang.String"> </bean:define>
<bean:define id="hmReturn" name="frmSetInitiate" property="hmReturn" type="java.util.HashMap"> </bean:define>
<% 
String strPrevPartNum = "";

int intSize = 0;
String strWikiTitle = GmCommonClass.getWikiTitle("SET_BUILD_INITITATE");
String strApplDateFmt = strGCompDateFmt;//GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
GmCalenderOperations.setTimeZone(strGCompTimeZone);
String strTodays = GmCalenderOperations.getCurrentDate(strGCompDateFmt);//GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
ArrayList alReturn = new ArrayList();
alReturn = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSEQUENCES"));
intSize = alReturn.size();

String strShipCarr = "";
String strShipMode = "";
String strShipTo   = "";
String strOpt = (String)request.getAttribute("strOpt")==null?"":(String)request.getAttribute("strOpt");

if (hmReturn != null && hmReturn.size() > 0) {
	strShipCarr = GmCommonClass.parseNull((String)hmReturn.get("SHIP_CARRIER"));
	strShipMode = GmCommonClass.parseNull((String)hmReturn.get("SHIP_MODE"));
	strShipTo   = GmCommonClass.parseNull((String)hmReturn.get("SHIP_TO"));
	 
}

strShipCarr = strShipCarr.equals("")?"5001" : strShipCarr; // 5001 FEDEX
strShipMode = strShipMode.equals("")?"5004" : strShipMode; // 5004 PRIORITY OVERNIGHT
strShipTo   = strShipTo.equals("")?"4121" : strShipTo; // 4121 DISTRIBUTOR

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Build Initiate Screen </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
var Distlist = '<%=distList%>';
var date_format = '<%=strApplDateFmt%>';
var today_Dt = '<%=strTodays%>';

function fnFetchSetInfo(){
	
	if(document.frmSetInitiate.setId.value == '' )
		{
		Error_Details(message[5298]);
		}
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	document.frmSetInitiate.strOpt.value = "fetch";
	document.frmSetInitiate.haction.value = document.frmSetInitiate.consignmentType.value;
	document.frmSetInitiate.action = "/gmIncShipDetails.do?screenType=Initiate&RE_FORWARD=gmSetInitiate";  
	fnStartProgress(); 
	document.frmSetInitiate.submit();
}

function fnInitiate(){
	requestTypeVal = document.frmSetInitiate.consignmentType.value;
	fnValidateDropDn('consignmentType',message[5542]);
	fnValidateDropDn('inHousePurpose',message[5543]);
	var req_dateObj = document.frmSetInitiate.requiredDate;
	var plan_dateObj = document.frmSetInitiate.planShipDate;
	// validate the Date format
	CommonDateValidation(req_dateObj,date_format,message[5784]+ message[611]);
	CommonDateValidation(plan_dateObj,date_format,message[5785]+ message[611]);

	// to get the date Different () Required Date
	var date_diff = dateDiff(today_Dt, req_dateObj.value, date_format);

	if(date_diff <0){
		Error_Details(message[5054]);
		}
	// to get the date Different () Planned Ship Date
	date_diff = dateDiff(today_Dt, plan_dateObj.value, date_format);
	if(date_diff <0){
		Error_Details(message[5055]);
		}
	// to get the date Different () Requried Date and Planned Ship Date
	if(req_dateObj.value !=''){
		date_diff = dateDiff(req_dateObj.value, plan_dateObj.value, date_format);
		if(date_diff >0){
			Error_Details(message[5056]);
			}
		}
	// if planned ship date is not empty Bill to is mandatory.
	if(plan_dateObj.value != ''){
		billTo = document.frmSetInitiate.distributorId.value;
		if (billTo == '0'){
			fnValidateDropDn('distributorId',message[5647]);
		}
	}
	if(requestTypeVal == '40021'){		 
		billTo = document.frmSetInitiate.distributorId.value;
		if(billTo == '01'){
			Error_Details(message[5057]);
		}
		
	purpose = document.frmSetInitiate.inHousePurpose.value; 
	flg = 'true'
	if (purpose == 50061){ // ICT sameple
		var DistlistArr = Distlist.split(",");		
		for (var j=0;j< DistlistArr.length;j++ )
			{			
				if (billTo != DistlistArr[j]){
					flg = 'false';
				}
				else{
					flg = 'true';
					break;
					}
			}
		if (flg == 'false'){		
			Error_Details(message[5058]);
			}			
		}			
	}
	 
	if(requestTypeVal == '40022'){ 
		fnValidateDropDn('inHousePurpose',message[5543]);
	//	fnValidateDropDn('employeeId',' Value ');
		
		billTo = document.frmSetInitiate.distributorId.value;
		shipTo = document.frmSetInitiate.shipTo.value;
		 
		if(billTo != '01'){
			Error_Details(message[5059]);
		}
		
		if(shipTo != '4123'){
			Error_Details(message[5060]);
		}
	}
	fnValidateShippingDetails();
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	if (document.all.names.value != 0 ){
		document.all.names.disabled = false;
	} 
	 
	document.frmSetInitiate.strOpt.value = "initiate";		
  	document.frmSetInitiate.action = "/gmRuleEngine.do";
  	fnStartProgress();  
 	document.frmSetInitiate.submit();
}

 

function fnValidateShippingDetails(){
	billTo = document.all.distributorId.value;
	if (billTo != ''&& billTo != '0')
	{			
			fnValidateDropDn('shipTo',message[5620]);
			fnValidateDropDn('names',message[5564]);
			//fnValidateTxtFld('requiredDate',' Required Date ');
			//fnValidateTxtFld('planShipDate',' Planned Ship Date ');
			var shipto = document.all.shipTo.value;
			var shiptoidid = document.all.addressid.value;
			if (shipto == '4121' && shiptoidid == '')
			{
				Error_Details(message[32]);
			}
	}
	if (billTo == ''|| billTo == '0')
	{
		fnDisableShip();
	}

}

function fnOnLoad(){

	billTo = document.all.distributorId.value;
 
	if (billTo != '0'){
	fnSetShipTo();
	}
	

}
</script>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
<html:form action="/gmSetInitiate.do">
<html:hidden property="strOpt" value="<%=strOpt%>"/>
<html:hidden property="haction"/>
<input type="hidden" name="RE_FORWARD" value="gmSetIni">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" value="SETINITWRAPPER">

 

<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="4"></td></tr>
		<tr>
			<td height="25" class="RightDashBoardHeader"> <fmtSetInitiate:message key="LBL_SETBUILD"/> </td>
			<fmtSetInitiate:message key="LBL_HELP" var="varHelp"/>
			<td height="25" class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
	</tr>
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
		<tr>
			<td colspan="2">	
				<jsp:include page="/operations/requests/GmSetRequestHeader.jsp">
				<jsp:param name="FORMNAME" value="frmSetInitiate" />
				<jsp:param name="SETVIEW" value="Y"/>
				<jsp:param name="SHIPCARR" value="<%=strShipCarr%>" />
				<jsp:param name="SHIPMODE" value="<%=strShipMode%>" />
				<jsp:param name="SHIPTO" value="<%=strShipTo%>" />
				<jsp:param name="strOpt" value="<%=strOpt%>" />
				</jsp:include>
			</td>
		</tr>
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
<logic:notEmpty name="frmSetInitiate" property="strOpt">
		<tr>
			<td colspan="4">
				<display:table   cellspacing="0" cellpadding="0"  name="requestScope.frmSetInitiate.rdSetListReport" requestURI="/gmSetInitiate.do" export="false" excludedParams="strOpt" 
				class="its DtTable1000" id="currentRowObject" decorator="com.globus.displaytag.beans.DTSetInitiateWrapper" style="width: 1000px;" >
					<fmtSetInitiate:message key="LBL_PARTNUMBER" var="varPartNumber"/>
					<display:column property="PNUM"  title="${varPartNumber}" />
					<fmtSetInitiate:message key="LBL_DESCRIPTION" var="varDescription"/>
			 		<display:column property="PDESC" title="${varDescription}"   />	
			 		<fmtSetInitiate:message key="LBL_SETQTY" var="varSetQty"/>				 		
					<display:column property="QTY" title="${varSetQty}"  class="alignright" />
					<fmtSetInitiate:message key="LBL_BULKQTY" var="varBulkQty"/>
					<display:column property="QTYBULK" title="${varBulkQty}" class="alignright" />
				</display:table>						
			</td>
		</tr>		
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
		<tr><td colspan="2"> 
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
						<jsp:include page="/operations/shipping/GmIncShipDetails.jsp" />				
					
				</td></tr>
		 <%
        if(intSize > 0)
        {
        %>
        <tr><td class="line" colspan="2" height="1"></td></tr>     
		<tr>
			<td colspan="2">
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmRuleDisplayInclude.jsp" />
			</td>
		</tr>		
		<%} %>		
		<tr>
			<td colspan="2" height="30" align="center">
				<fmtSetInitiate:message key="BTN_INITIATE" var="varInitiate"/>
				<gmjsp:button name="initiate" value="${varInitiate}" gmClass="button" onClick="fnInitiate();" buttonType="Save" />
			</td>
		</tr>
</logic:notEmpty>					
</table>
</html:form>		
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>