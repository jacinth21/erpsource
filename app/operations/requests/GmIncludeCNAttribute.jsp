<%@page import="java.util.HashMap"%>
<%@page import="org.apache.commons.beanutils.PropertyUtils"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtRequestEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtRequestEdit:setLocale value="<%=strLocale%>"/>
<fmtRequestEdit:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>


  <!-- \operations\purchasing\orderplanning\requests\GmIncludeCNAttribute.jsp-->  
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
String strArrayListName ="";
String strCustPo = "";
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
ArrayList alAttribute = new ArrayList();
String strLoanReqId ="";
String strReqFor="";
String strRequestId="";
if(!strFormName.equals(""))
{

	strArrayListName = GmCommonClass.parseNull(request.getParameter("ALATTRIB"));
	Object objTemp = pageContext.getRequest().getAttribute(strFormName);
	alAttribute  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListName));
	if(strFormName.trim().equals("frmRequestEdit")){
		strCustPo = GmCommonClass.parseNull(request.getParameter("CUSTPO"));
		objTemp = pageContext.getRequest().getAttribute(strFormName);
		strCustPo = GmCommonClass.parseNull((String)PropertyUtils.getProperty(objTemp,strCustPo));
		//PMT-32450 - To add new column in Request edit Screen only for item consignment 
		strLoanReqId = GmCommonClass.parseNull(request.getParameter("LOANERREQID"));
		strLoanReqId = GmCommonClass.parseNull((String)PropertyUtils.getProperty(objTemp,strLoanReqId));
		strReqFor = GmCommonClass.parseNull(request.getParameter("REQUESTFOR"));
		strReqFor = GmCommonClass.parseNull((String)PropertyUtils.getProperty(objTemp,strReqFor));
		strRequestId = GmCommonClass.parseNull(request.getParameter("REQUESTFORID"));
		strRequestId = GmCommonClass.parseNull((String)PropertyUtils.getProperty(objTemp,strRequestId));
	}
}
//PMT-45058 Ability to link Loaner Request to Item consignment transaction
String strLoanerReqDispFl =  GmCommonClass.getString("LOANER_REQ_DISP_FL");
String strDisable = "";
String strWidth = "";
String strValue = "";
String strMaxLength = "";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Request Attribute</title>
</head>
<body>
<%if(alAttribute.size()>0) {%>
		<tr height="2"><td bgcolor="#666666" height="1" colspan="4" ></td></tr>	
		<tr>
                 	<td colspan="2" HEIGHT="25" align="left">
                 <table cellpadding="0" cellspacing="0" border="0" align="left">
				<tr>
		<%
			for(int cnt=0;cnt<alAttribute.size();cnt++){
				HashMap hmParam=(HashMap)alAttribute.get(cnt); 
				String strAttName	= GmCommonClass.parseNull((String)hmParam.get("CODENM"));
				String strAttType	= GmCommonClass.parseNull((String)hmParam.get("CODEID"));
				strDisable ="";
				strWidth = "";
				strValue ="";
				strMaxLength = "";
				if(strAttType.equals("1006420")){
					strWidth = "style=\"width: 200px;\"";
					strValue = strCustPo;
					strMaxLength = "maxlength=999";
				}else{
					strDisable = "";
				}
				if(cnt%2==0 && cnt!=0){
		%>
			<td></tr>
			<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
			<tr>
			<%} %>
			<td class="RightTableCaption" align="right" HEIGHT="25" width="105">
			<% if((cnt+1)%2==0) {%>
				<%=strAttName %>: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<%}else{ %>
				<%=strAttName %>:&nbsp;
			<%} %>
				</td>
				<td align="left">
				<input type="text"  name="Txt_<%=strAttType%>" <%=strMaxLength%> <%=strDisable%> <%=strWidth%> value="<%=strValue%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
				<input type="hidden" value="<%=strAttType%>" name="h<%=cnt%>">
			</td><!--PMT-45058 40025: Account Consignment, 40021: Consignment -->
			<%if(strFormName.trim().equals("frmRequestEdit") && strLoanerReqDispFl.indexOf(strRequestId)!=-1){%>
		     <td class="RightTableCaption" align="right"><fmtRequestEdit:message key="LBL_LOANER_REQ_ID"/>:</td>
			<td><input type="text" name="loanerReqID" value="<%=strLoanReqId%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff'),fnvalidateLoanerRefID();"/></td>
			<%} %>
			<%} %>
		</tr>
		</table></td>
		</tr>
                 <tr><td colspan="4" class="LLine" height="1"></td></tr>
<%} %>		
</body>
</html>