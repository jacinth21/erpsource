
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
 <%@ include file="/common/GmHeader.inc" %>

 

  <bean:define id="alAttrib" name="frmRequestInitiate" property="alAttrib" type="java.util.ArrayList"/>
<%@ taglib prefix="fmtAttributeDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
    strLocale = GmCommonClass.parseNull((String)locale.toString());
}
%>
<!-- GmAttributeDetails.jsp -->
<fmtAttributeDetails:setLocale value="<%=strLocale%>"/>
<fmtAttributeDetails:setBundle basename="properties.labels.custservice.ProcessRequest.GmItemInitiate"/>
  <%
  	ArrayList alAttribute=(ArrayList)alAttrib;
    String strShade = "Shade";
    Boolean blFl = false;
  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 
<title>Insert title here</title>
</head>
<body>
<table border="0" width="100%"  cellspacing="0" cellpadding="0">	
		<tr class="ShadeRightTableCaption">
			<td height="23"  colspan="4"><fmtAttributeDetails:message key="LBL_SUGERYDETAILS"/> 
:</td>			
		</tr>			
		<tr><td class="line" colspan="4"></td></tr>
				<tr class="<%=strShade%>">
		<%
			for(int cnt=0;cnt<alAttribute.size();cnt++){
				HashMap hmParam=(HashMap)alAttribute.get(cnt);
				if(blFl==false){
					strShade = "Shade";
				}else{
					strShade = "";
				}
				blFl =(cnt%4==0 && cnt!=0)?false:true;
				if(cnt%2==0 && cnt!=0){
		%>
			</tr>
			<tr><td colspan="4" height="1" bgcolor="#cccccc"></td></tr>
			<tr class="<%=strShade%>">
			<%} %>
			<td HEIGHT="25" align="right">
				<b><%=hmParam.get("CODENM") %>:</b> &nbsp;
			</td>
			<td>
				<input type="text" name="ATTRIBVAL<%=(cnt+1)%>" onfocus="changeBgColor(this,'#AACCE8');" Class="InputArea" onblur="changeBgColor(this,'#ffffff');">
				<input type="hidden" name="HATTRIBID<%=(cnt+1) %>" value='<%=hmParam.get("CODEID")%>'>
			</td>
			<%} %>
		</tr>
</table>
</body>
</html>