<%
/**********************************************************************************
 * File		 		: GmRequestMasterAction.jsp
 * Desc		 		: REquest Master report
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ taglib prefix="fmtProcReqItems" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRequestMasterAction.jsp -->
<fmtProcReqItems:setLocale value="<%=strLocale%>"/>
<fmtProcReqItems:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>
<bean:define id="alReport" name="frmRequestMaster" property="alRequestReport" type="java.util.List"></bean:define>
<bean:define id="deptId" name="frmRequestMaster" property="deptId" type="java.lang.String"></bean:define>
<% 
	session = request.getSession(false);

	Object bean = pageContext.getAttribute("frmRequestMaster", PageContext.REQUEST_SCOPE);
	pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
	String strWikiTitle = GmCommonClass.getWikiTitle("REQUEST_MODIFY_VIEW");
	String strApplDateFmt = strGCompDateFmt;
	String strDTDateFmt = "{0,date,"+strApplDateFmt+"}";
	 int rowsize = alReport.size();
	 String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	 GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	 String strProcessLoaner = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("PROCESS_LOANER"));
	 String strBuiltSetToRtn = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("BUILT_SET_TO_RETUN"));
	 String strAccessfl= GmCommonClass.parseNull((String)request.getAttribute("UPDFL"));
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Request Master Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
var strStatus  ;
var reqStatus = '';
var strAccessfl ='<%=strAccessfl%>';
// NSP-504 Changes
function fnSubmit()
{
	  requestId = document.frmRequestMaster.hId.value ;
	  reqStatus = document.frmRequestMaster.reqStatus.value ; 
	var action =    document.frmRequestMaster.Cbo_Action.value ;
    if(action == '0')
    { 
      Error_Details(message[406]); 
    }     
        
    if(requestId == '')
    { 
      Error_Details(message[407]); 
    }   
    else
    {
    	if(reqStatus == '60'){ // Transferred.
       	 Error_Details(message[5043]);
        }
    if( action == 'RCON' )
    {    	
  	  var PurposeType = fnGetPurpose(requestId);
  	  var CNStatus = fnGetCNStatus(requestId);
	  if (CNStatus == '1.20' || CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
		  Error_Details(message[409]); 
	  }
	  else
	  {		  	  
	  document.frmRequestMaster.strRequestPurpose.value = PurposeType;
      document.frmRequestMaster.hAction.value = "ReloadReconfig";
      document.frmRequestMaster.action = "<%=strServletPath%>/GmSetBuildServlet";
	  } 
    }
	
	
	else if( action == 'SBDP' )
    {
	  var CNStatus = fnGetCNStatus(requestId);
	  if (CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
		  Error_Details(message[409]); 
	  }
	  else
	  {	
      document.frmRequestMaster.strOpt.value = "";
 	  var PurposeType = fnGetPurpose(requestId);
  	  document.frmRequestMaster.strRequestPurpose.value = PurposeType;
      document.frmRequestMaster.action = "/gmSetBuildProcess.do?requestId="+requestId;
	  }
    }  
    
    else if( action == 'PROCCSG' || action =='PROCLOANER'|| action=='BSTR' )
    {
    	var CNStatus = fnGetCNStatus(requestId);
    	if(action=='BSTR' && CNStatus != '2')
    		{
    		Error_Details(message[410]); 
    		}
		if (CNStatus == '1.20' || CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
			Error_Details(message[409]); 
		}
		else
		{	
		document.frmRequestMaster.hMode.value = 'CON';
		document.frmRequestMaster.hCboAction.value = action;
    	var PurposeType = fnGetPurpose(requestId);
		document.frmRequestMaster.strRequestPurpose.value = PurposeType; 
		document.frmRequestMaster.hAction.value = "EditLoad";
		document.frmRequestMaster.hRequestId.value = requestId;
		document.frmRequestMaster.RE_FORWARD.value = "gmRequestHeader";
		document.frmRequestMaster.RE_RE_FORWARD.value = "gmConsignSetServlet";
		document.frmRequestMaster.FORWARD.value = "gmIncShipDetails";
		//document.frmRequestMaster.action = "/gmRequestHeader.do?requestId="+requestId+"&FORMNAME=frmRequestHeader&hRequestView=headerView";
		document.frmRequestMaster.action = "/gmRequestEdit.do?refId="+requestId+"&source=50184&strOpt=fetch&screenType=Consign&requestId="+requestId+"&FORMNAME=frmRequestHeader&hRequestView=headerView";
		//document.frmRequestMaster.action ="<%=strServletPath%>/GmConsignSetServlet";
		//document.frmRequestMaster.submit();    
		}
	}  
   
   
   if( action == 'RREQ' )
    {
		var CNStatus = fnGetCNStatus(requestId);
		if (CNStatus == '1.20' || CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
			Error_Details(message[409]); 
		}
		else
		{
      //document.frmRequestMaster.hAction.value = "ReloadReconfig";
      document.frmRequestMaster.action = "gmRequestDetail.do?requestId="+requestId;
		}
    }
   
   	
   	if( action == 'RQED' )
    {
      //document.frmRequestMaster.hAction.value = "ReloadReconfig";
      document.frmRequestMaster.action = "gmRequestEdit.do?requestId="+requestId;
    }
   	
    if (action == 'RSTAT' )  
    {  
    	var CNStatus = fnGetCNStatus(requestId);
		if (CNStatus == '1.20' || CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
			Error_Details(message[409]); 
		}
		else
		{
    	strStatus = document.frmRequestMaster.reqStatus.value;
		var PurposeType = fnGetPurpose(requestId);
    	document.frmRequestMaster.strRequestPurpose.value = PurposeType;
    	if (strStatus == 30){	    	
	    	document.frmRequestMaster.hTxnId.value = requestId;
			document.frmRequestMaster.action ="/GmCommonCancelServlet";
			document.frmRequestMaster.hAction.value = "Load";		
			document.frmRequestMaster.hCancelType.value = 'STSRB'
			document.frmRequestMaster.submit();
		}
		else{
		 	Error_Details(message[27]); 
		}
      //Error_Details(" Under Construction... ");
		}
       
    }
    
    if  (action == 'RVOD' ) 
    {
    	var CNStatus = fnGetCNStatus(requestId);
		if (CNStatus == '1.20' || CNStatus == '2.20' || CNStatus == '3.30' || CNStatus == '3.60'){
			Error_Details(message[409]); 
		}
		else
		{	
    	var reqSoucre = fnGetSourceId(requestId);   //To get the source Id for selected req ID.
		if(reqSoucre == '4000122'){       //For Set Par Source, cannot allow to void.
			Error_Details(message[5566]); 
		}else{
	     	var PurposeType = fnGetPurpose(requestId);
	    	document.frmRequestMaster.strRequestPurpose.value = PurposeType; 
	     	document.frmRequestMaster.action ="<%=strServletPath%>/GmCommonCancelServlet";
			document.frmRequestMaster.hTxnId.value =  requestId;
			
			document.frmRequestMaster.hAction.value = 'Load';
		}
		}       
    }
    if(action == 'ROLLBACKPENPICK'){
    	var CNStatus = fnGetCNStatus(requestId);	
    	strStatus = document.frmRequestMaster.reqStatus.value;
    	var strconId =document.frmRequestMaster.hConId.value;
    	
    	if(strAccessfl != 'Y') {    //check access flag here
            Error_Details(message_operations[597]); 
        }
    	if(CNStatus == '2.20' && strStatus== '20' ){
    		var PurposeType = fnGetPurpose(requestId);
    		document.frmRequestMaster.strRequestPurpose.value = PurposeType; 
    		document.frmRequestMaster.action ="/GmCommonCancelServlet";
    		document.frmRequestMaster.hTxnId.value =  strconId;
    		document.frmRequestMaster.hCancelType.value = 'RLBPIC';
    		document.frmRequestMaster.hAction.value = 'Load';
    	}
    	else{
    		Error_Details(message[409]); 
    	}
    }
    
    }
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}

	fnStartProgress();
	document.frmRequestMaster.submit();
}

//To get source id from the wrapper class hidden field.
function fnGetSourceId(reqId){
	var sourceId = '';
	reqId = reqId.replace(/\-/g,'_');
	var objSoucre = eval("document.frmRequestMaster.hreqsourceid"+reqId);
	if(objSoucre != undefined) sourceId = objSoucre.value;
	
	return sourceId;
}
function fnReload()
{
	// PC-2272 Auto Capitalize the Set ID while Typing
	if(document.frmRequestMaster.setID != undefined){
    	if(document.frmRequestMaster.setID.value != undefined && document.frmRequestMaster.setID.value != ""){
    	document.frmRequestMaster.setID.value = TRIM(document.frmRequestMaster.setID.value.toUpperCase());
    	}
    }
	// PC-2272 Auto Capitalize the Consignment ID Field while Typing
    if(document.frmRequestMaster.consignID != undefined){
    	if(document.frmRequestMaster.consignID.value != undefined && document.frmRequestMaster.consignID.value != ""){
    	document.frmRequestMaster.consignID.value = TRIM(document.frmRequestMaster.consignID.value.toUpperCase());
    	}
    }
    
	var varMoC =  document.frmRequestMaster.mainOrChild.value ;
	var varMreq =  document.frmRequestMaster.masterReqID.value ;
	 
	if(varMoC == '50638' && varMreq !='')
	{	 
		 Error_Details(message[408]); 
		 }
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmRequestMaster.strOpt.value = 'reload';
		
	if(document.frmRequestMaster.requestStatusoper.value != "0")
	{	
 	document.frmRequestMaster.requestStatusval.value=frmRequestMaster.requestStatusoper.options[frmRequestMaster.requestStatusoper.selectedIndex].text;
 	}
 	fnStartProgress('Y');
	document.frmRequestMaster.submit();
}

function fnSelectPart(reqId,status,reqFor,cnId)
{
fnDisableOptions(reqFor,'Y');

//  requestId = obj.value ; 
  document.frmRequestMaster.hId.value = reqId ;
  document.frmRequestMaster.hRequestFor.value = reqFor;
  strStatus = status;  
  document.frmRequestMaster.reqStatus.value = status ;
  document.frmRequestMaster.hConId.value = cnId;
}

function fnFetch()
{	 
	document.frmRequestMaster.submit();
}


 function fnViewDetails(strReqId,strConId)
 {
    
      windowOpener("/gmRequestMaster.do?strOpt=print&requestId="+strReqId+"&consignID="+strConId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=920,height=600");
    }
    
function fnInitiate()
{
	 document.frmRequestMaster.action = "/gmSetInitiate.do";
	 document.frmRequestMaster.submit();
}
  
function fnInitiateItem()
{
	 document.frmRequestMaster.action = "/GmPageControllerServlet?strPgToLoad=gmIncShipDetails.do?screenType=Initiate&RE_FORWARD=gmRequestInitiate&strOpt=";
	 document.frmRequestMaster.submit();
}  
      
function fnPrintVer(strConId)

{
windowOpener("/GmConsignSetServlet?hAction=PrintVersion&hId="+strConId,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnSetList()
{
windowOpener("/GmPageControllerServlet?strPgToLoad=GmSetReportServlet&strOpt=SETRPT","Con","resizable=yes,scrollbars=yes,top=150,left=200,width=680,height=600");
}
function fnOpenOrdLogInv(id)
{
	
	windowOpener("/GmCommonLogServlet?hType=1235&hID="+id,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
function fnOpenTag(strCNId)
{
 var strPnum="";
	windowOpener("/gmTag.do?strOpt=reload&pnum="+encodeURIComponent(strPnum)+"&refID="+strCNId+"&refType=51000","PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=1070,height=600");
} 
//When get request id from the wrapper class we cannt get the value.
function fnGetPurpose(strInHousePurpId){
	var  PurposeType = '';
	strInHousePurpId = strInHousePurpId.replace(/\-/g,'_')
	var objPurpose = eval("document.frmRequestMaster.InHousePurose"+strInHousePurpId);
	if(objPurpose != undefined) PurposeType = objPurpose.value;
	return PurposeType;
}

//When get request id from the wrapper class we cannt get the value.
function fnGetCNStatus(requestId){
	var  cnstatus = '';
	requestId = requestId.replace(/\-/g,'_');
	var objCNStatus = eval("document.frmRequestMaster.hCNStatusfl"+requestId);
	if(objCNStatus != undefined) cnstatus = objCNStatus.value;
	return cnstatus;
}
//this function is used to disable the options based on the Req For while reloading
 function fnSelectOption(){
	var reqFor=  document.frmRequestMaster.hRequestFor.value ;
	
	  fnDisableOptions(reqFor,'N');
	
 }

 //this function is used to disable some of the options in the choose action drop down
  function fnDisableOptions(reqFor,resetFl){
	 
	 var optionSize = document.getElementById("Cbo_Action").length;  
		for(var i=0;i<optionSize;i++){
			
			var objVal = document.getElementById("Cbo_Action").options[i];

			if(reqFor == '26240420'){//stock transfer
				if(objVal.value == 'RREQ' || objVal.value == 'RVOD'){
					objVal.disabled = false;
					if(resetFl == 'Y'){//to avoid default choose one for onclick on action dropdown
						document.getElementById("Cbo_Action").value='0';
					}
				}
				else{
					objVal.disabled = true;		
				}
			}else{
				objVal.disabled = false;
			}
			
		} 
 }
// PC-2272 Auto Focus on the Set ID field
  function fnOnPageLoad()
  {
  	if(document.frmRequestMaster.setID != undefined && document.frmRequestMaster.setID != null){		
  		document.frmRequestMaster.setID.focus();
  	}
  }
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<html:form action="/gmRequestMaster.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="hMode" value=""/>
<html:hidden property="requestStatusval" value=""/>
<input type="hidden" name="hAction" value="">
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hCancelType" value="VDREQ"/> 
<input type="hidden" name="hRequestId" value=""></input>
<input type="hidden" name="RE_FORWARD" value=""></input>
<input type="hidden" name="RE_RE_FORWARD" value=""></input>
<input type="hidden" name="FORWARD" value=""></input>
<input type="hidden" name="strRequestPurpose" value=""></input>
<input type="hidden" name="reqStatus"  ></input>
<html:hidden property="hCboAction" value=""/> 
	<input type="hidden" name="hId"  ></input>
	<input type="hidden" name="hRequestFor"  ></input>
	<input type="hidden" name="hConId" ></input>
	
 

	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtProcReqItems:message key="LBL_HEADER"/> </td>
			<td align="right" class=RightDashBoardHeader > 	
				<fmtProcReqItems:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>	
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="1098" height="100" valign="top" colspan="3">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<html:errors />
						</td>
					</tr>
                    <tr><td colspan="2" class="ELine"></td></tr>
                    <tr>
                    	<td>
                    		<table border="0" cellspacing="0" cellpadding="0">
					<!-- Custom tag lib code modified for JBOSS migration changes -->
			                    <tr>
			                     
			                        <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_REQUESTID"/> :</td> 
				                     <td>&nbsp;<html:text property="requestId"  size="20" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
			    		             </td>
			    		            
			    		              <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_REQTRANSID"/> :</td> 
				                     <td>&nbsp;
				                     <fmtProcReqItems:message key="LBL_CHOOSEONE" var = "varChooseOne"/>
				                       <gmjsp:dropdown controlName="requestTxnID" SFFormName="frmRequestMaster" SFSeletedValue="requestTxnID"
							SFValue="alRequestTxnID" codeId = "CODEID"  codeName = "CODENM"    defaultValue= "${varChooseOne}" />
				                  
			    		             </td>
			    		          
			                    </tr>
			                     <tr><td colspan="4" class="ELine"></td></tr>
			                     
			                     <tr class="Shade">
			                        <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_REQDATEFROM"/>:</td> 
			                        <!-- Struts tag lib code modified for JBOSS migration changes -->
				                     <td>&nbsp;<gmjsp:calendar  SFFormName='frmRequestMaster'   controlName="requestDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 	onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
				                     <fmtProcReqItems:message key="LBL_TO"/>:
							<gmjsp:calendar  SFFormName='frmRequestMaster'   controlName="requestDateTo" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 	onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
							 
			    		             </td>
			    		             <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_REQUDATEFROM"/> :</td> 
				                     <td>&nbsp;<gmjsp:calendar  SFFormName='frmRequestMaster'   controlName="requiredDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 	onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
				                     <fmtProcReqItems:message key="LBL_TO"/>:
				                     <gmjsp:calendar  SFFormName='frmRequestMaster'   controlName="requiredDateTo" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 	onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
							
			    		             </td>
			                    </tr>
			                    <tr><td colspan="4" class="ELine"></td></tr>
			                     
			                     <tr>
			                        <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_SOURCE"/> :</td> 
				                     <td>&nbsp;
				                     <gmjsp:dropdown controlName="requestSource" SFFormName="frmRequestMaster" SFSeletedValue="requestSource"
							SFValue="alRequestSource" codeId = "CODEID"  codeName = "CODENM"    defaultValue= "${varChooseOne}" />
				                      </td>
			    		             <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_STATUS"/> :</td> 
			    		          
				                     <td>&nbsp;
				                        <gmjsp:dropdown controlName="requestStatus" SFFormName="frmRequestMaster" SFSeletedValue="requestStatus"
							SFValue="alRequestStatus" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "${varChooseOne}"/>
							
							<gmjsp:dropdown controlName="requestStatusoper" SFFormName="frmRequestMaster" SFSeletedValue="requestStatusoper"
							SFValue="alRequestStatus1" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "${varChooseOne}"/>
							
							&nbsp;<b class="RightTableCaption" ><fmtProcReqItems:message key="LBL_COUNTRY"/> : </b>
							&nbsp;<gmjsp:dropdown controlName="teamId" SFFormName="frmRequestMaster" SFSeletedValue="teamId"
							SFValue="alTeamList" codeId = "ID"  codeName = "NAME" defaultValue= "${varChooseOne}" />
							
				                     </td>
			                    </tr>
			                    
			                     <tr><td colspan="4" class="ELine"></td></tr>
			                     
			                     <tr class="Shade">
			                        <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_REQUESTTYPE"/> :</td> 
				                     <td>&nbsp;
				                     <gmjsp:dropdown controlName="requestFor" SFFormName="frmRequestMaster" SFSeletedValue="requestFor"
							SFValue="alRequestFor" codeId = "CODEID"  codeName = "CODENM"  onChange="fnFetch();"  defaultValue= "${varChooseOne}" />
				                      </td>
			    		             <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_REQUESTTO"/>:</td> 
				                     <td>&nbsp;
				                        <gmjsp:dropdown controlName="requestTo" SFFormName="frmRequestMaster" SFSeletedValue="requestTo"
							SFValue="alRequestTo" codeId = "ID"  codeName = "NAME"    defaultValue= "${varChooseOne}" />
				                     </td>
			                    </tr>
			                     <tr><td colspan="4" class="ELine"></td></tr>
			                    
			                      <tr>
			                        <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_SETPART"/> :</td> 
				                     <td>&nbsp;
				                     <gmjsp:dropdown controlName="setOrPart" SFFormName="frmRequestMaster" SFSeletedValue="setOrPart"
							SFValue="alSetPart" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "[Choose One]" />
				                      </td>
			    		             <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_PARTNUMBER"/>:</td> 
				                     <td>
				                        &nbsp;<html:text property="partNum"  size="50" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
				                     </td>
			                    </tr>
			                    
			                        <tr><td colspan="4" class="ELine"></td></tr>
			                    
			                      <tr class="Shade">
			                        <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_REQUEST"/> :</td> 
				                     <td>&nbsp;
				                     <gmjsp:dropdown controlName="mainOrChild" SFFormName="frmRequestMaster" SFSeletedValue="mainOrChild"
							SFValue="alMainOrChild" codeId = "CODEID"  codeName = "CODENM"   defaultValue= "${varChooseOne}" />
				                      </td>
			    		             <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_MASTERREQID"/> :</td> 
				                     <td>
				                        &nbsp;<html:text property="masterReqID"  size="15" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
				                     </td>
			                    </tr>
			                    
			                     <tr><td colspan="4" class="ELine"></td></tr>
			                     
			                     <tr>
			                        <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtProcReqItems:message key="LBL_SETID"/> :</td> 
				                     <td>&nbsp;<html:text property="setID"  size="15" styleClass="InputArea" style="text-transform: uppercase;" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;
				                     <a href= javascript:fnSetList();><img src="<%=strImagePath%>/action-detail.gif" style="border: none;" height="14"></a>
				                     </td>
			    		             <td class="RightTableCaption" align="right" >&nbsp;<fmtProcReqItems:message key="LBL_CONSIGNID"/> :</td>
			    		             <td>&nbsp;<html:text property="consignID"  size="15" styleClass="InputArea" style="text-transform: uppercase;" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
			    		             </tr>
			    		             <tr><td colspan="4" class="ELine"></td></tr>
			    		             <tr class="Shade">
			    		            <td class="RightTableCaption" align="right">&nbsp;<fmtProcReqItems:message key="LBL_REPROCESSID"/> :</td>
			    		             <td>&nbsp;<html:text property="reprocessId"  size="15" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
			    		             <%-- <html:text name="reprocessID"   size="15" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /> --%>
                                     </td>
			    		             <td class="RightTableCaption" align="right">&nbsp;<fmtProcReqItems:message key="LBL_TAGID"/> :</td>
			    		             <td>&nbsp;<html:text property="tagId"  size="15" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
			    		             <fmtProcReqItems:message key="BTN_LOAD" var="varLoad"/>
			    		             <gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="fnReload();" buttonType="Load" /></td>
			    		             </tr>
			    		             
			                    
			                    
			                  </table>
			             </td>
			         </tr>  
					 <tr><td colspan="2" class="ELine"></td></tr>
					 
					 <logic:notEqual  name="frmRequestMaster" property="strOpt" value="">
					 
                    <tr>
	                     <td colspan="2" height="24" align="center">
	                     	<display:table name="<%=alReport%>" class="its" id="currentRowObject" export="true" requestURI="/gmRequestMaster.do"    decorator="com.globus.displaytag.beans.DTRequestMasterWrapper">
	                     	<display:setProperty name="export.excel.filename" value="LogProcessRqstReport.xls" />
	                     	<fmtProcReqItems:message key="LBL_NO" var="varNo"/><display:column title="${varNo}"> <%=pageContext.getAttribute("currentRowObject_rowNum")%> </display:column>
	                     	<fmtProcReqItems:message key="LBL_REQID" var="varReqId"/>
							<display:column property="REQID" title="${varReqId}"  sortable= "true" class="RightTextSmall"/>
							<fmtProcReqItems:message key="LBL_CONSINGID" var="varConsignId"/>
							<display:column property="CONID" title="${varConsignId}" maxLength="25" sortable= "true" class="RightTextSmall"/>
							<fmtProcReqItems:message key="LBL_SETID" var="varSetId"/>
							<display:column property="SETID" title="${varSetId}" sortable= "true" class="RightTextSmall"/> 
							<fmtProcReqItems:message key="LBL_SETNAME" var="varSetName"/>
							<display:column property="NAME" title="${varSetName}" maxLength="25" class="RightTextSmall" sortable= "true"/>
							<fmtProcReqItems:message key="LBL_SHEETNAME" var="varSheetName"/>
							<display:column property="SHEETNM" title="${varSheetName}" maxLength="35" class="RightTextSmall" sortable= "true"/>
							<!-- Add Set Priority Id column to display Priority Id In Request - PMT-36376 - 7-Add Priority in 'Process Request Set Screen'-->
							<fmtProcReqItems:message key="LBL_PRIORITY" var="varPriority"/>
							<display:column property="PRIORITYID" title="${varPriority}" maxLength="10" class="RightTextSmall" sortable= "true"/>
							<fmtProcReqItems:message key="LBL_REQUESTFOR" var="varRequestFor"/>
							<display:column property="REQFOR" title="${varRequestFor}" maxLength="10" class="RightTextSmall" sortable= "true"/>
							<fmtProcReqItems:message key="LBL_REQUESTTO" var="varRequestTo"/>
							<display:column property="REQTO" title="${varRequestTo}" maxLength="10" class="RightTextSmall" sortable= "true"/>
							<fmtProcReqItems:message key="LBL_REQUEST" var="varRequest"/>
							<fmtProcReqItems:message key="LBL_DATE" var="varDate"/>
							<display:column property="REQDT" title="${varRequest}<br> ${varDate}" sortable= "true" class="RightTextSmall" format="<%=strDTDateFmt%>"/> 
							<fmtProcReqItems:message key="LBL_REQUIRED" var="varRequired"/>
							<display:column property="REQUDT" title="${varRequired} <br> ${varDate}" sortable= "true" class="RightTextSmall" format="<%=strDTDateFmt%>"/> 
						 	<fmtProcReqItems:message key="LBL_SOURCE" var="varSource"/>
							<display:column property="REQSRC" title="${varSource}" maxLength="5" class="RightTextSmall" sortable= "true"/>
							<fmtProcReqItems:message key="LBL_RQSTATUS" var="varRqStatus"/>
							<display:column property="RQSTATUS" title="${varRqStatus}" maxLength="5" class="RightTextSmall" sortable= "true"/>
							<fmtProcReqItems:message key="LBL_CNSTATUS" var="varCnStatus"/>
							<display:column property="CNSTATUS" title="${varCnStatus}" maxLength="5" class="RightTextSmall" sortable= "true"/>
							<fmtProcReqItems:message key="LBL_CRT" var="varCRT"/>
							<display:column property="CRTQTY" title="${varCRT}" class="alignright" sortable= "true"/>
							<fmtProcReqItems:message key="LBL_CMP" var="varCMP"/>
							<display:column property="CMPQTY" title="${varCMP}" class="alignright" sortable= "true"/>
							<fmtProcReqItems:message key="LBL_SET_WIP_LOC" var="varSETWIPLOC"/>
							<display:column property="SETWIPLOCATION" title="${varSETWIPLOC}" class="RightTextSmall" style="width:85px;" sortable= "true"/>
							
							</display:table>

    		             </td>
                    </tr> 
                     <% if (rowsize >0) { %> 
                   <tr>
                    	<td colspan="2" align="center"><fmtProcReqItems:message key="LBL_CHOOSE_ACTION"/>:&nbsp;<select id="Cbo_Action" name="Cbo_Action" class="RightText" 
						tabindex="5" onFocus="changeBgColor(this,'#AACCE8');fnSelectOption();" onBlur="changeBgColor(this,'#ffffff');" ><option value="0" ><fmtProcReqItems:message key="LBL_CHOOSEONE"/>							
							<option value="RSTAT"><fmtProcReqItems:message key="LBL_CHANGESTATUS"/></option>
							<option value="PROCCSG"><fmtProcReqItems:message key="LBL_PROCESSCONSIGNMENT"/></option>
							<!-- OUS need show Process Loaner action  -->
							<%  if (strProcessLoaner.equals("YES")) { %>	
							<option value="PROCLOANER"><fmtProcReqItems:message key="LBL_PROCESSLOANER"/></option>  
							 <% } %>
							<option value="RCON"><fmtProcReqItems:message key="LBL_RECONFIGURECONSIGNMENT"/></option>
							<option value="RREQ"><fmtProcReqItems:message key="LBL_RECONFIGUREREQUEST"/></option>
 							<option value="SBDP"><fmtProcReqItems:message key="LBL_SETBUILDPROCESSING"/></option>							
 							<%  if (!strBuiltSetToRtn.equals("NO")) { %>	
 							<option value="BSTR"><fmtProcReqItems:message key="LBL_BUILTSETTORETURN"/></option>
 							<% } %>
 							<option value="RVOD"><fmtProcReqItems:message key="LBL_VOIDREQUEST"/></option>
 							<option value="ROLLBACKPENPICK"><fmtProcReqItems:message key="LBL_ROLLBACK"/></option>								
  
					<!--  	<option value="RQED">Request Edit</option>			-->					
 
						</select>
							<fmtProcReqItems:message key="BTN_SUBMIT" var="varSubmit"/>
							<fmtProcReqItems:message key="BTN_INITIATENEWSET" var="varItemNew"/>
							<fmtProcReqItems:message key="BTN_INITIATEITEMCONSI" var="varInitiateItem"/>
		                    <gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" buttonType="Save" /> 
		                     &nbsp;&nbsp;<gmjsp:button value="${varItemNew}" gmClass="button" onClick="fnInitiate();" buttonType="Save" /> 
		                    &nbsp;<gmjsp:button value="${varInitiateItem}" gmClass="button" onClick="fnInitiateItem();" buttonType="Save" /> 	 
		                </td>
                    </tr>
                    <% } %>
                    </logic:notEqual>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

