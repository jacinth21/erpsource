<%@ page import="com.globus.common.beans.GmJasperReport"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Date"%>

<%@ taglib prefix="fmtRequestSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\requests\GmRequestSummary.jsp -->

<fmtRequestSummary:setLocale value="<%=strLocale%>"/>
<fmtRequestSummary:setBundle basename="properties.labels.operations.requests.GmRequestSummary"/>

<bean:define id="hmData" name="frmRequestMaster" property="hmData" type="java.util.HashMap"></bean:define>
<bean:define id="alLogReasons" name="frmRequestMaster" property="alLogReasons" type="java.util.ArrayList"></bean:define>
<bean:define id="consignID" name="frmRequestMaster" property="consignID" type="java.lang.String"></bean:define>
<HTML>
<HEAD>
<TITLE>Globus Medical: Request Summary</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var cnID = '<%=consignID%>';
function fnPrintPick(){
	windowOpener("/GmConsignItemServlet?hAction=PicSlip&hConsignId="+cnID+"&ruleSource=50183","SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=600");
}

</script>
<%HashMap hmShipping = new HashMap();
hmShipping = (HashMap)hmData.get("SHIP_DTLS");
String strHideNoData = "";
if(alLogReasons.size() == 0){
	strHideNoData = "YES";
}

String strReqFor = GmCommonClass.parseNull((String)hmData.get("REQFOR"));
String strShipDt = GmCommonClass.parseNull((String)hmShipping.get("SDATE"));//GmCommonClass.getStringFromDate((Date)hmShipping.get("SDATE"),"MM/dd/yyyy");
String strShipMode = GmCommonClass.parseNull((String)hmShipping.get("SMODE"));
String strShipCar = GmCommonClass.parseNull((String)hmShipping.get("SCARR"));
String strShipAdd = GmCommonClass.parseNull((String)hmShipping.get("SHIPADD"));
String strTrack = GmCommonClass.parseNull((String)hmShipping.get("TRACK"));
String strShipChar = GmCommonClass.parseZero((String)hmShipping.get("SHIPCOST"));
String strHtmlJasperRpt = "";

%>

</HEAD>

<BODY leftmargin="20" topmargin="10">
	<html:form action="/gmRequestMaster.do?">
		<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="6">
					<table>
						<tr>
							<td>
								<%
									String strJasperPath = GmCommonClass
												.getString("GMJASPERLOCATION");
										GmJasperReport gmJasperReport = new GmJasperReport();

										gmJasperReport.setRequest(request);
										gmJasperReport.setResponse(response);
										gmJasperReport.setPageHeight(40);
										hmData.put("LASTPAGE","N");
										hmData.put("NODATA",strHideNoData);
										hmData.put("COMPANYDFMT",strGCompDateFmt);
										gmJasperReport.setJasperReportName("/GmMRPrint.jasper");
										gmJasperReport.setHmReportParameters(hmData);
										if(alLogReasons.size() == 0){
											gmJasperReport.setReportDataList(null);
										}else{
											gmJasperReport.setReportDataList(alLogReasons);
										}
										gmJasperReport.setBlDisplayImage(true);
										strHtmlJasperRpt = gmJasperReport.getHtmlReport();
								%>
								<%=strHtmlJasperRpt %>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr height="30">
				<td colspan="6" class="RightText" align="Center">
				<%if(!strReqFor.equals("26240420")){ %> 
				 <fmtRequestSummary:message key="BTN_PRINT_SLIP" var="varPrintSlip"/>
					<gmjsp:button value="&nbsp;${varPrintSlip}&nbsp;" gmClass="button"
					onClick="fnPrintPick();" tabindex="5" buttonType="Load" /></td> 
				<%} %> 
			</tr>
			 
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>