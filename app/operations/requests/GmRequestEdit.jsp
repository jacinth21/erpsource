<%
/**********************************************************************************
 * File		 		: GmRequestEdit.jsp
 * Desc		 		: This screen is used to display Request Edit
 * Version	 		: 1.0
 * author			: Xun Qu
 ---------------------------------------------------------------------------------
 * Version	 		: 2.0
 * Desc		 		: changed to disable the name validation in Shipping Details
 *author		    : pramaraj
 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>
 
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>

<%@ taglib prefix="fmtRequestEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRequestEdit.jsp -->
<fmtRequestEdit:setLocale value="<%=strLocale%>"/>
<fmtRequestEdit:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>
<bean:define id="requestStatus" name="frmRequestEdit" property="requestStatus" type="java.lang.String"></bean:define>   
 <bean:define id="requestId" name="frmRequestEdit" property="requestId" type="java.lang.String"> </bean:define>
 <bean:define id="reqFor" name="frmRequestEdit" property="reqFor" type="java.lang.String"> </bean:define>
 <bean:define id="custPo" name="frmRequestEdit" property="custPo" type="java.lang.String"> </bean:define>
 <bean:define id="alAttribute" name="frmRequestEdit" property="alAttribute" type="java.util.ArrayList"> </bean:define>
 <bean:define id="loanerReqID" name="frmRequestEdit" property="loanerReqID" type="java.lang.String"> </bean:define>
 <bean:define id="requestFor" name="frmRequestEdit" property="requestFor" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>
<TITLE> Globus Medical: Request Edit</TITLE>

<%
     String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	 String strWikiTitle = GmCommonClass.getWikiTitle("REQUEST_MODIFY_EDIT");
	 GmCalenderOperations.setTimeZone(strGCompTimeZone);
	 String strApplDateFmt = strGCompDateFmt; //PMT-32450- Referencing Loaner Request in Item Consignment 
	 String strTodays = GmCommonClass.parseNull(GmCalenderOperations.getCurrentDate(strGCompDateFmt));  //PMT-32450- Referencing Loaner Request in Item Consignment
	 String strLimitedAccess= GmCommonClass.parseNull((String)request.getAttribute("DMNDSHT_LIMITED_ACC"));
	 String strInitiatedFrom= GmCommonClass.parseNull((String)request.getAttribute("initiatedFrom"));
	 String strDemandSheetMonthID= GmCommonClass.parseNull((String)request.getAttribute("demandSheetMonthID"));
	 String strEnableComPolBtn = GmCommonClass.parseNull((String)request.getAttribute("ENABLE_COMPOOL_BTN")).equals("YES")?"":"true";
	 String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
     GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	 String strCompCountryCode = gmResourceBundleBean.getProperty("COUNTRYCODE");
	 boolean bolShowComPolBtn  = GmCommonClass.parseNull((String)request.getAttribute("SHW_COMPOOL_BTN")).equals("YES")?true:false;
	 
		boolean bolLimitedAccess = false;
		if(strLimitedAccess!=null && strLimitedAccess.equalsIgnoreCase("true")){
			bolLimitedAccess = true;
		}
	%> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script><!-- PMT-36862 js file  -->
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="javascript" src="<%=strOperationsJsPath%>/GmRequestEdit.js"></script>  
	
<script> 

var attCnt = <%=alAttribute.size()%>;
var distid;
var repid;
var date_format = '<%=strApplDateFmt%>';
var today_Dt = '<%=strTodays%>';
var compCountryCode = '<%=strCompCountryCode%>';
var response ="";
var lonerReqIdValidFl = true;
var loanerReqId='';
var editReqForId= '<%=reqFor%>';

function fnSubmit()
{
	/*var strShipto = document.frmRequestEdit.requestShipTo.value;
 
	if(	strShipto == "0")
	{
	 	document.frmRequestEdit.requestShipTo.value = "";
	}
	var strShiptoID = document.frmRequestEdit.requestShipToID.value;
	
	if(	strShiptoID == "0")
	{	 	
	 	document.frmRequestEdit.requestShipToID.value = "";	  
	 }
	 */
	/*if(document.all.shipTo.value=='0')
	{
		document.all.shipTo.value='4124';//for NA
	}*/
	/*fnValidateDropDn('shipTo','ShipTo');*/
	
	/*if (document.all.shipTo.value != '4120'){
		fnValidateDropDn('names',' Names');
	}*/
	
	var strReqTo = document.frmRequestEdit.reqTo.value;
	
	if(	strReqTo == "0")
	{	 	
	 	document.frmRequestEdit.reqTo.value = "";	  
	 } 
	document.frmRequestEdit.strOpt.value = 'save';
	
	fnValidateTxtFld('requiredDate',message[5545]);
	// remove plan ship date as it is not mandatory
	//	if(strReqTo !='0' && strReqTo !='')
	//	fnValidateTxtFld('planShipDate',' Planned Ship Date');

	var req_dateObj = document.frmRequestEdit.requiredDate;
	var plan_dateObj = document.frmRequestEdit.planShipDate;
	
	// validate the Date format
	CommonDateValidation(req_dateObj,date_format,message[5571]+ message[611]);
	CommonDateValidation(plan_dateObj,date_format,message[5572]+ message[611]);

	// to get the date Different () Required Date
	var date_diff = dateDiff(today_Dt, req_dateObj.value, date_format);
	//no need required date validation here, it's just info, otherwise it will be issue affect GOP sheet lock
	/*if(date_diff <0){
		Error_Details("Required Date cannot be earlier than today.");
		}
		*/
	// to get the date Different () Planned Ship Date
	date_diff = dateDiff(today_Dt, plan_dateObj.value, date_format);
	if(date_diff <0){
		Error_Details(message[5570]);
		}
	// to get the date Different () Requried Date and Planned Ship Date
	/*if(req_dateObj.value !=''){
		date_diff = dateDiff(req_dateObj.value, plan_dateObj.value, date_format);
		if(date_diff >0){
			Error_Details("Planned Ship Date should not later than Required Date.");
			}
	}	
	*/ 
	//PMT0-32450 Referencing Loaner Request Id 
	  var loanerReqIdObj  = document.frmRequestEdit.loanerReqID;
	  if(loanerReqIdObj != undefined){
		    loanerReqId = loanerReqIdObj.value;
		   if(!lonerReqIdValidFl && loanerReqId !=''){
				Error_Details(message[10607]);
			}
	   }
	if(response != "") {
 		Error_Details(response);
    }
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	else{
		document.all.names.disabled = false;
		document.frmRequestEdit.hAction.value = "EditLoad";
		document.frmRequestEdit.hRequestId.value = "<%=requestId%>";
		var str='';
		for(var i=0;i<attCnt;i++){
			var attrib= eval("document.frmRequestEdit.h"+i);
			attType = attrib.value;
			var attrib1 = eval("document.frmRequestEdit.Txt_"+attType);
			attVal = attrib1.value;
			str += attType + '^' + attVal + '|';  
		}
		
		document.frmRequestEdit.hinpStr.value = str;
		
		document.frmRequestEdit.RE_FORWARD.value = "gmRequestHeader";
		document.frmRequestEdit.RE_RE_FORWARD.value = "gmConsignSetServlet";
		document.frmRequestEdit.FORWARD.value = "gmIncShipDetails";
		document.frmRequestEdit.action = "/gmRequestEdit.do?hMode=CON&refId="+"<%=requestId%>"+"&source=50184&screenType=Consign&FORMNAME=frmRequestHeader&hRequestView=headerView";
		fnStartProgress('Y');
		document.frmRequestEdit.submit();
		
		}
}
function fnSetShipTo(){
	var obj = document.all.reqTo;
	var distId = obj.options[obj.selectedIndex].value;
	var reqForId= '<%=reqFor%>';
	
	if(reqForId==40022)// 40022 - InHouse 
	{
		document.all.shipTo.value='4123'; // employee
		document.all.shipCarrier.value='5040';
		document.all.shipMode.value='5030';
	}
	else if(reqForId == 40025){
		fnEnableShip();
		document.all.shipTo.value = 4122;
		document.all.names.value = distId;
		document.getElementById('addressid').disabled = true
		distid=distId;
	}
	else
	{
		document.all.shipTo.value = 4120; // distributor
		distid=distId;
	}
		
		document.all.names.disabled = false;
		if (reqForId != 40025 && (distId != '01' || distId != 0))
		{		
			fnGetNames(document.all.shipTo);
			//document.all.names.value=distId;
			//document.all.names.disabled = true;
		}	
			if (distId == '01' || distId == 0) {		
			document.all.shipTo.value = 0;
			document.all.names.options.length = 0;
			document.all.names.options[0] = new Option("[Choose One]","0");
			document.all.names.value =0;
			document.all.names.disabled = false;
		}
			//PMT0-32450 Referencing Loaner Request Id 
		 var loanerReqIdObj = document.frmRequestEdit.loanerReqID;
	   if(loanerReqIdObj != undefined){
		    loanerReqId = loanerReqIdObj.value;
		   if(loanerReqId !=''){
			   fnvalidateLoanerRefID();
			}
	   }
		
}

	

 function fnLoad()
{	
	document.frmRequestEdit.submit();
}	
 
 function onLoad(){
 	if(document.all.shipTo!=null){
 		var obj = document.all.reqTo;
 		distid = obj.options[obj.selectedIndex].value;
	 	if (document.all.shipTo.value == '4120'  || document.all.shipTo.value == '4124'){
	 		fnGetNames(document.all.shipTo);
	 		document.all.names.disabled = true;
			
		}
		if (document.all.shipTo.value == '4121' || document.all.shipTo.value == '4000642'){ //4000642:Shipping Account
			repid = document.all.names.value;
			fnGetNames(document.all.shipTo);
			
			//fnGetAddressList (document.all.names,addid);	
		}
	}
 	if(compCountryCode == 'en'){
 		if(document.all.shipCarrier!=null){
 			if (document.all.shipCarrier.value == 0 || document.all.shipCarrier.value == 5040  ) {
 					document.all.shipCarrier.value= 5001;//defaulted to fedex
 				}
 		}
 		if(document.all.shipMode!=null){
 			if (document.all.shipMode.value == 0 || document.all.shipMode.value == 5031) {
 				document.all.shipMode.value= 5008;// defaulted to Ground
 			}
 		}
 	}
	
 }
function fnVoid()
{
		 
		document.frmRequestEdit.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmRequestEdit.hTxnId.value = document.frmRequestEdit.requestId.value;
		
		document.frmRequestEdit.hAction.value = 'Load';
		document.frmRequestEdit.submit();
}

function fnMoveToCommonPool(){
	document.frmRequestEdit.action = "/gmRequestEdit.do?FORWARD=gmIncShipDetails&RE_FORWARD=gmRequestHeader&RE_RE_FORWARD=gmConsignSetServlet&hAction=moveToCommonPool&hMode=CON&source=50184&screenType=Consign&FORMNAME=frmRequestHeader&hRequestView=headerView&requestId="+"<%=requestId%>"+"&requestFor=40021&hRequestId="+"<%=requestId%>"+"&demandSheetMonthID="+"<%=strDemandSheetMonthID%>";
	document.frmRequestEdit.submit();	
}

function fnEnableShip(){
	document.all.shipTo.disabled = false;
	document.all.names.disabled = false;
	document.all.shipCarrier.disabled = false;
	document.all.shipMode.disabled = false;	
	document.all.shipCarrier.value= 5001; //defaulted to fedex
	if (document.all.setId)
	{
		document.all.shipMode.value = 5008; // defaulted to Ground
	}
	else{
		document.all.shipMode.value = 5004; // defaulted to overnight
	}
}

</script>
 
</HEAD>


<BODY leftmargin="20" topmargin="10"  onload="onLoad();">
 
<html:form action="/gmRequestEdit.do"  >
<html:hidden property="strOpt" value=""/> 
<html:hidden property="requestId" value="<%=requestId%>"/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hAction" value=""/>
<html:hidden property="hinpStr" value=""/>
<html:hidden property="requestTxnID" /> 
<html:hidden property="hCancelType" value="VDREQ"/>
<input type="hidden" name="hRequestId" value=""></input>
<input type="hidden" name="RE_FORWARD" value=""></input>
<input type="hidden" name="RE_RE_FORWARD" value=""></input>
<input type="hidden" name="FORWARD" value=""></input>
<input type="hidden" name="initiatedFrom" value="<%=strInitiatedFrom%>"></input>

 
 <table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
		<tr>
			<td height="20" class="RightDashBoardHeader">&nbsp;<fmtRequestEdit:message key="LBL_MODIFY_REQUEST_EDIT"/></td>
			<td align="right" class=RightDashBoardHeader > 	
			<fmtRequestEdit:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr>	<td colspan="2">						
<jsp:include page="/operations/requests/GmRequestViewHeader.jsp">
		<jsp:param name="FORMNAME" value="frmRequestEdit" />
		<jsp:param name="hViewEdit" value="Edit" />
		<jsp:param name="bolLimitedAccess" value="<%=bolLimitedAccess%>" />
</jsp:include>
	<tr>
             <td colspan="2">
             	<jsp:include page="GmIncludeCNAttribute.jsp" >
             		<jsp:param name="FORMNAME" value="frmRequestEdit" />
             		<jsp:param name="ALATTRIB" value="alAttribute" />
             		<jsp:param name="CUSTPO" value="custPo" />
             		<jsp:param name="LOANERREQID" value="loanerReqID"/>
             		<jsp:param name="REQUESTFOR" value="requestFor"/>
             		<jsp:param name="REQUESTFORID" value="reqFor"/>
				</jsp:include>
			</td>
        </tr> 
	<logic:notEqual name="frmRequestEdit" property="requestStatus" value="Completed"> 
	<%if(!bolLimitedAccess){ %>
	<tr><td colspan="2"> 
		<jsp:include page="/operations/shipping/GmIncShipDetails.jsp">	 
		<jsp:param name="screenType" value="Consign" />
		</jsp:include>	
	</td></tr>
	<%} %>
	   <tr>
					<td colspan="4" align="center" height="40">&nbsp;
					
					<%if(bolShowComPolBtn){ %>
					<fmtRequestEdit:message key="LBL_MOVE_TO_COMMON_POOL" var="varMoveToCommonPool"/>
					<gmjsp:button value="${varMoveToCommonPool}" tabindex="35" gmClass="button" disabled="<%=strEnableComPolBtn%>" onClick="javascript:fnMoveToCommonPool();" buttonType="Save" />&nbsp;
					<%} %>
					<%if(!bolLimitedAccess){ %>
					<fmtRequestEdit:message key="LBL_SUBMIT" var="varSubmit"/>
					<gmjsp:button value="${varSubmit}" tabindex="35" gmClass="button" onClick="javascript:fnSubmit();" buttonType="Save" />&nbsp;
					<%} %>
					<fmtRequestEdit:message key="LBL_VOID" var="varVoid"/>
		  		<gmjsp:button value="${varVoid}" tabindex="36" gmClass="button" onClick="fnVoid();" buttonType="Save" />	 
					</td>
				</tr>	
	</logic:notEqual>	 
	
<p> &nbsp;</p>
 
 
 <jsp:include page="/operations/requests/GmRequestViewDetail.jsp" >
	<jsp:param name="FORMNAME" value="frmRequestEdit" />
 		</jsp:include>	
 		 </td></tr>
 </table>
 
  <jsp:include page="/operations/requests/GmRequestViewAsscTxn.jsp" >
 	<jsp:param name="FORMNAME" value="frmRequestEdit" />
 		</jsp:include> 
 		
 	
  </html:form> 
  <%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
