 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmLoanerReqEdit.jsp
 * Desc		 		: This screen is used for editing request data.
 * Version	 		: 1.0 
 * author			: Ritesh Shah
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ taglib prefix="fmtLoanerEditReq" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmLoanerReqEdit.jsp -->
<fmtLoanerEditReq:setLocale value="<%=strLocale%>"/>
<fmtLoanerEditReq:setBundle basename="properties.labels.custservice.ProcessRequest.GmLoanerReqEdit"/>


<bean:define id="alVoidRsn" name="frmLoanerReqEdit" property="alVoidReasons" type="java.util.ArrayList"></bean:define>
<bean:define id="DataFlag" name="frmLoanerReqEdit" property="dataFlag" type="java.lang.String"></bean:define>
<bean:define id="voidDataFlag" name="frmLoanerReqEdit" property="voidDataFlag" type="java.lang.String"></bean:define>
<bean:define id="txnType" name="frmLoanerReqEdit" property="txnType" type="java.lang.String"></bean:define>
<bean:define id="requestTxnType" name="frmLoanerReqEdit" property="requestTxnType" type="java.lang.String"></bean:define>
<bean:define id="requestId" name="frmLoanerReqEdit" property="requestId" type="java.lang.String"></bean:define>
<bean:define id="alAttrib" name="frmLoanerReqEdit" property="alAttrib" type="java.util.ArrayList"/>
<bean:define id="strEsclAccessFl" name="frmLoanerReqEdit" property="strEsclAccessFl" type="java.lang.String"></bean:define>
<bean:define id="strEsclRemAccessFl" name="frmLoanerReqEdit" property="strEsclRemAccessFl" type="java.lang.String"></bean:define> 

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_RECONCILIATION");
GmCalenderOperations gmCal = new GmCalenderOperations();
String strTodaysDate = gmCal.getCurrentDate(strGCompDateFmt); 
StringBuffer sbVoidRsnHtml = new StringBuffer();
String strApplnDateFmt = strGCompDateFmt;
String strApplJSDateFmt  = GmCommonClass.parseNull((String)session.getAttribute("strSessJSDateFmt"));
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean =
    GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.custservice.ProcessRequest.GmLoanerReqEdit", strSessCompanyLocale);

String strLoanerReqDay   =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHIP_REQ_DAY"));
String strShipSurgeryDay   =GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHIP_SURGERY_DAY"));
String strShowLoanAtrb = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_LOANER_ATRB"));
String strRptFmt = "{0,date,"+strApplnDateFmt+"}";


ArrayList alAttribute=(ArrayList)alAttrib;

if(strLoanerReqDay.equals("")){
	strLoanerReqDay="100000000000";
}
if(strShipSurgeryDay.equals("")){
	strShipSurgeryDay="100000000000";
}
sbVoidRsnHtml.append("<SELECT name=Cbo_VOIDRSN class=InputArea style=WIDTH: 10px;>");
sbVoidRsnHtml.append("<option value=0>[Choose One]");
int intSize = alVoidRsn.size();
HashMap hcboVal = new HashMap();
for (int i=0;i<intSize;i++)
{
	hcboVal = (HashMap)alVoidRsn.get(i);
	sbVoidRsnHtml.append("<option value='");
	sbVoidRsnHtml.append(hcboVal.get("CODEID"));
	sbVoidRsnHtml.append("'>");
	sbVoidRsnHtml.append(hcboVal.get("CODENM"));
	sbVoidRsnHtml.append("</option>");
}
sbVoidRsnHtml.append("</SELECT>");
String strHeaderLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_EDIT_LOANER_REQUEST"));
String strFwdLblNm = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PENDING_LOANER_REQUEST"));
if(txnType.equals("4119")){
	strHeaderLbl = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_EDIT_INHOUSE_LOANER_REQUEST"));
	strFwdLblNm =  GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PENDING_INHOUSE_LOANER_REQUEST"));
	
}
String strCompanyId = GmCommonClass.parseNull((String) gmDataStoreVO.getCmpid());
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Request Edit </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLoanerReqEdit.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>


<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var todaysDate = '<%=strTodaysDate%>';
var format = '<%=strApplnDateFmt%>';
var jsformat = '<%=strApplJSDateFmt%>';
var reqday ='<%=strLoanerReqDay%>';
var shipday = '<%=strShipSurgeryDay%>';
var txnType = '<%=txnType%>';
var requestTxnType = '<%=requestTxnType%>';
var strEscFl = '<%=strEsclAccessFl%>'; 
var strRemEscFl = '<%=strEsclRemAccessFl%>';

</script>
</HEAD>

 <BODY leftmargin="20" topmargin="10" onload="fnPageLoad()" onkeydown="javaScript:return fnLoadRequset();"> 
<html:form action="/gmOprLoanerReqEdit.do">
<html:hidden property="strOpt" />
<html:hidden property="hinputString" />
<html:hidden property="etchidInputStr" />
<html:hidden property="voidInputStr" />
<html:hidden property="haction" value="visible"/>
<html:hidden property="txnType" />
<html:hidden property="requestTxnType" />
<html:hidden property="eventStartDate" />
<html:hidden property="hiddenReqID" value="<%=requestId%>"/>
	<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><%=strHeaderLbl%></td>
			<td align="right" class=RightDashBoardHeader > 	
			<fmtLoanerEditReq:message key="LBL_HELP" var = "varHelp"/>
				<img id='imgEdit' style='cursor:hand' src="<%=strImagePath%>/help.gif" title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="1098" height="25" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
	 
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>                    	
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;<font color="red">* </font><fmtLoanerEditReq:message key="LBL_REQUEST_ID"/>:</td>
						<td>
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="35%">&nbsp;<html:text property="requestId" maxlength="20" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
									</td>	
									<fmtLoanerEditReq:message key="BTN_LOAD" var="varLoad"/>
									<td align="left"><gmjsp:button name="Btn_Load" value="${varLoad}" gmClass="button"  onClick="fnLoadReq();" buttonType="Load" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<% if (DataFlag.equals("N")){  
						if (requestTxnType.equals("4127")){
					  %>													
						<tr class="Shade">
							<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;<fmtLoanerEditReq:message key="LBL_REQUESTED_DATE"/>:</td>
							<td>&nbsp;<bean:write name="frmLoanerReqEdit" property="requestedDt"/></td>
						</tr>
					
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
	                    	<td HEIGHT="25">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
									<td class="RightTableCaption" align="right"></font>&nbsp;<fmtLoanerEditReq:message key="LBL_LOAN_TO"/>:</td>																
									</tr>
								</table>
							</td>
							<td HEIGHT="25">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td width="300px">&nbsp;<bean:write name="frmLoanerReqEdit" property="requestForNm"/> 
										</td>								
										<td class="RightTableCaption" align="right" >&nbsp;<fmtLoanerEditReq:message key="LBL_REP_ID"/>:</td>
										<td width="300px" align="left">
				                    		&nbsp;<bean:write name="frmLoanerReqEdit" property="salesRepNm"/>                                        
		                        		</td>
									</tr>
								</table>
							</td>
						</tr>
	                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
	                    <tr>
	                    	<td HEIGHT="25">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
									<td class="RightTableCaption" align="right"></font>&nbsp;<fmtLoanerEditReq:message key="LBL_ACCOUNT"/>:</td>																
									</tr>
								</table>
							</td>
							<td HEIGHT="25">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td width="300px">&nbsp;<bean:write name="frmLoanerReqEdit" property="accountNm"/> 
										</td>								
										<td class="RightTableCaption" align="right" >&nbsp;<fmtLoanerEditReq:message key="LBL_ASSOC_REP"/>:</td>
										<td width="300px" align="left">
				                    		&nbsp;<bean:write name="frmLoanerReqEdit" property="assocrepnm"/>                                        
		                        		</td>
									</tr>
								</table>
							</td>
						</tr>
	                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
	                    <tr>
	                        <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;<fmtLoanerEditReq:message key="LBL_SURGERY_DATE"/>:</td>
	                        <!-- Custom tag lib code modified for JBOSS migration changes -->
	                        <td>&nbsp;<gmjsp:calendar SFFormName="frmLoanerReqEdit" controlName="surgeryDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					        </td>
	                    </tr> 
	                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
	                    
	                  <% }else if(requestTxnType.equals("4119")){  %> 
					  
					    <tr class="Shade">
							<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;<fmtLoanerEditReq:message key="LBL_EVENT_NAME"/>:</td>
							<td>&nbsp;<bean:write name="frmLoanerReqEdit" property="eventName"/></td>
						</tr>					
						<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
	                    	<td HEIGHT="25">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr><td class="RightTableCaption" align="right" >&nbsp;<fmtLoanerEditReq:message key="LBL_EVENT_START_DATE"/>:</td></tr>
								</table>
							</td>
							<td HEIGHT="25">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td width="300px">&nbsp;<bean:write name="frmLoanerReqEdit" property="eventStartDate"/></td>								
										<td class="RightTableCaption" align="right" >&nbsp<fmtLoanerEditReq:message key="LBL_EVENT_END_DATE"/>;:</td>
										<td width="300px" align="left">&nbsp;<bean:write name="frmLoanerReqEdit" property="eventEndDate"/></td>
									</tr>
								</table>
							</td>
						</tr>
	                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
	                    <tr class="Shade">
	                       <td HEIGHT="25">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr><td class="RightTableCaption" align="right" >&nbsp;<fmtLoanerEditReq:message key="LBL_LOAN_TO"/>:</td></tr>
								</table>
							</td>
							<td HEIGHT="25">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td width="300px">&nbsp;<bean:write name="frmLoanerReqEdit" property="loanToType"/></td>								
										<td class="RightTableCaption" align="right" >&nbsp;<fmtLoanerEditReq:message key="LBL_LOAN_TO_NAME"/>:</td>
										<td width="300px" align="left">&nbsp;<bean:write name="frmLoanerReqEdit" property="loanToName"/>                                      
		                        		</td>
									</tr>
								</table>
							</td>
	                   </tr> 
	                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
					  <% }  %>
					  
					  
					     <%if(strShowLoanAtrb.equalsIgnoreCase("yes") && alAttribute.size()>0) {%>
	                    <tr class="Shade">
							<td class="RightTableCaption" align="left" HEIGHT="25" colspan="2"><fmtLoanerEditReq:message key="LBL_SURGERY_DETAILS"/>:</td>			
						</tr>		
							<tr><td colspan="2" class="LLine" height="1"></td></tr>
						<tr>
	                    	<td colspan="2" HEIGHT="25" align="left">
	                    <table cellpadding="0" cellspacing="0" border="0" align="left">
								<tr>
						<%
							for(int cnt=0;cnt<alAttribute.size();cnt++){
								HashMap hmParam=(HashMap)alAttribute.get(cnt);
								String strAttName	= GmCommonClass.parseNull((String)hmParam.get("ATTBNAME"));
								String strAttVal	= GmCommonClass.parseNull((String)hmParam.get("ATTBVAL"));
								
								if(cnt%2==0 && cnt!=0){
						%>
							<td></tr>
							<tr><td colspan="5" height="1" bgcolor="#cccccc"></td></tr>
							<tr>
							<%} %>
							<td class="RightTableCaption" align="right" HEIGHT="25" width="220">
							<% if((cnt+1)%2==0) {%>
								<%=strAttName %>: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<%}else{ %>
								<%=strAttName %>: 
							<%} %>
								</td>
								<td align="left" width="150">
								<%if(strAttVal.equals("")){%>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<%}else{ %>
									<%=strAttVal %>
								<%} %>
							</td>
							<%} %>
						</tr>
						</table></td>
						</tr>
	                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
	                    <%} %>
	                    
	                    <tr id="displaytable"> 							
				            <td colspan="2">			            
					            <display:table name="requestScope.frmLoanerReqEdit.alReqList" requestURI="/gmOprLoanerReqEdit.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.operations.requests.displaytag.beans.DTLoanerEditRequestWrapper">																			
						 <% if (strCompanyId.equals("1000")){  %> 
					  	   <fmtLoanerEditReq:message key="LBL_FAM_ESCL_LOG" var="varFamEsclLog"/><display:column property="ASCFL" title="${varFamEsclLog}" class="alignleft" style="width:81px"/> 
							<fmtLoanerEditReq:message key="LBL_FAM_ESCL" var="varFamEscl"/><display:column property="ESCLLOGFLAG" title="${varFamEscl}" class="alignleft" style="width:69px"/>	
							 <%} %>		 								
							<fmtLoanerEditReq:message key="LBL_ID" var="varID"/>	<display:column property="LOGIMG" title="${varID}" class="aligncenter" style="width:80px"/>
							<fmtLoanerEditReq:message key="LBL_VOID_REASON" var="varVoidReason"/>	<display:column property="VOIDFL" title="&nbsp;<input type='checkbox' name='voidAll' onClick='fnVoidAll();' />   ${varVoidReason}" class="alignleft"  style="width:210px"/> 
							<fmtLoanerEditReq:message key="LBL_CONSIGNMENT_ID" var="varConsignId"/>	<display:column property="CONSGID" title="${varConsignId}" class="alignleft" style="width:120px"/>
							<fmtLoanerEditReq:message key="LBL_SET_ID" var="varSetId"/><fmtLoanerEditReq:message key="LBL_PART" var="varPart"/>	<display:column property="SETID" title="${varSetId}/<BR>${varPart}" class="alignleft" style="width:65px"/> 
							<fmtLoanerEditReq:message key="LBL_DESCRIPTION" var="varDescription"/>	<display:column property="SETNM" title="${varDescription}" class="alignleft" style="width:110px"/> 
							<fmtLoanerEditReq:message key="LBL_QTY" var="varQty"/>	<display:column property="QTY" title="${varQty}" class="alignleft" style="width:40px"/> 
							<fmtLoanerEditReq:message key="LBL_PLANNED" var="varPlanned"/><fmtLoanerEditReq:message key="LBL_SHIP_DATE" var="varShipDate"/>	<display:column property="DATE" title=" ${varPlanned}<br> &nbsp;${varShipDate}&nbsp; <img id='Img_Date'  style='cursor:hand' onClick='fnCopyDate();' title='Click to open Calendar' src='/images/nav_calendar.gif' border=0 align='absmiddle' height='16' width='19' /> &nbsp;<div id='divDate' style='position: absolute; z-index: 10;'></div>" class="alignleft" style="width:100px" format="<%=strRptFmt%>"/>
							<fmtLoanerEditReq:message key="LBL_ETCHID" var="varEtchId"/>	<display:column property="ETCHID" title="${varEtchId}" class="alignleft" style="width:85px" />							
							<fmtLoanerEditReq:message key="LBL_REQ_STATUS" var="varReqStatus"/>	<display:column property="REQ_STATUS" title="${varReqStatus}" class="alignleft" style="width:90px"/>
							<fmtLoanerEditReq:message key="LBL_CN_STATUS" var="varCnStatus"/>	<display:column property="CN_STATUS" title="${varCnStatus}" class="alignleft" style="width:90px"/>	
	                          </display:table>													
							</td>											
			    	   </tr>	
			    	   <tr><td colspan="2" class="LLine" height="1"></td></tr>
			    	   <tr id="displaytable">
							<td height="25" class="ShadeRightTableCaption" colspan="2"><a
									href="javascript:fnShowFilters('tabVoidReqDetails');"><IMG
									id="tabVoidReqDetailsimg" border=0 src="<%=strImagePath%>/plus.gif"></a>&nbsp;<fmtLoanerEditReq:message key="LBL_VOID_REQUEST_DETAILS"/></td>
						</tr>
						<tr id="tabVoidReqDetails" style="display:none;">
						<td colspan="2">			            
					            <display:table name="requestScope.frmLoanerReqEdit.alVoidRequestDetails" requestURI="/gmOprLoanerReqEdit.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.operations.requests.displaytag.beans.DTLoanerEditRequestWrapper">						
								<fmtLoanerEditReq:message key="LBL_ID" var="varID"/><display:column property="LOGIMG" title="${varID}" class="aligncenter" style="width:80px"/>       
								<fmtLoanerEditReq:message key="LBL_VOID_REASON" var="varVoidReason"/><display:column property="VOIDREASON" title="&nbsp;${varVoidReason}" class="alignleft" style="width:250px" /> 
								<fmtLoanerEditReq:message key="LBL_CONSIGNMENT_ID" var="varConsignId"/><display:column property="REQCNID" title="${varConsignId}" class="alignleft" style="width:120px"/>
								<fmtLoanerEditReq:message key="LBL_SET_ID" var="varSetId"/><fmtLoanerEditReq:message key="LBL_PART" var="varPart"/><display:column property="SETID" title="${varSetId}/<BR>${varPart}" class="alignleft" style="width:65px"/> 
								<fmtLoanerEditReq:message key="LBL_DESCRIPTION" var="varDescription"/><display:column property="SETNM" title="${varDescription}" class="alignleft"  style="width:140px"/> 
								<fmtLoanerEditReq:message key="LBL_QTY" var="varQty"/><display:column property="QTY" title="${varQty}" class="alignleft" style="width:40px"/> 
								<fmtLoanerEditReq:message key="LBL_PLANNED" var="varPlanned"/><fmtLoanerEditReq:message key="LBL_SHIP_DATE" var="varShipDate"/><display:column property="REQUIREDDATE" title="${varPlanned} <br> ${varShipDate}" class="alignleft" style="width:120px" format="<%=strRptFmt%>"/>
								<fmtLoanerEditReq:message key="LBL_ETCHID" var="varEtchId"/><display:column property="LOANERETCHID" title="${varEtchId}" class="alignleft" style="width:85px"/>							
								<fmtLoanerEditReq:message key="LBL_REQ_STATUS" var="varReqStatus"/><display:column property="STATUS" title="${varReqStatus}" class="alignleft"  style="width:80px"/>								
								</display:table>													
							</td>			
						</tr>													   	   	
				  		<tr><td height="1" colspan="2" bgcolor="#666666"></td></tr>	
				  		<tr></tr>
                        <tr id="displaytable">
                            <td height="25" class="ShadeRightTableCaption" colspan="2" ><a
                                                      href="javascript:fnShowFilters('tabShipDetails');"><IMG
                                                      id="tabShipDetailsimg" border=0 src="<%=strImagePath%>/plus.gif"></a>&nbsp;<fmtLoanerEditReq:message key="LBL_SHIPPINGDETAILS"/></td>
                        </tr>
                           <tr id="tabShipDetails" style="display:none;">
                              <td colspan="2">                          
                                <display:table name="requestScope.frmLoanerReqEdit.alShippingDetails" requestURI="/gmOprLoanerReqEdit.do" excludedParams="haction" class="its" id="currentRowObject" decorator="com.globus.operations.requests.displaytag.beans.DTLoanerEditRequestWrapper">
                               <fmtLoanerEditReq:message key="LBL_CONSIGNMENT_ID" var="varConsignId"/>> <display:column property="EDITREQ" title="${varConsignId}" class="alignleft" style="width:150px"/>
                               <fmtLoanerEditReq:message key="LBL_SET_ID" var="varSetId"/><fmtLoanerEditReq:message key="LBL_PART" var="varPart"/> <display:column property="SETID" title="${varSetId}/<BR>${varPart}" class="alignleft"/> 
                               <fmtLoanerEditReq:message key="LBL_ETCHID" var="varEtchId"/> <display:column property="LOANERETCHID" title="${varEtchId}" class="alignleft" style="width:80px" />                                               
                               <fmtLoanerEditReq:message key="LBL_SHIP_DATE" var="varShipDate"/>  <display:column property="SHIPDATE" title="${varShipDate}" class="alignleft" style="width:80px"  format="<%=strRptFmt%>"/>
                               <fmtLoanerEditReq:message key="LBL_SHIP_MODE" var="varShipmode"/>  <display:column property="DELMODEALT" title="${varShipmode}" class="alignleft" style="width:70px" />
                               <fmtLoanerEditReq:message key="LBL_SHIP_CARRIER" var="varShipCarrier"/>  <display:column property="DELCARRIER" title="${varShipCarrier}" class="alignleft" style="width:70px" />
                               <fmtLoanerEditReq:message key="LBL_SHIPPING_ADDRESS" var="varShippingAddress"/>   <display:column property="SHIPADDRESS" title="${varShippingAddress}" class="alignleft" style="width:170px">&nbsp;</display:column>
                               <fmtLoanerEditReq:message key="LBL_TRACKING" var="varTracking"/>   <display:column property="TRACKINGNO" title="${varTracking}" class="alignleft" style="width:100px" />                                                                                     
                                </display:table>                                                                          
                              </td>             
                        </tr>   <tr><td height="1" colspan="2" bgcolor="#666666"></td></tr>	                     	  		
				  		<tr class="Shade">
		                    	<jsp:include page="/common/GmIncludeLog.jsp" >
			                    	<jsp:param name="FORMNAME" value="frmLoanerReqEdit" />
									<jsp:param name="ALNAME" value="alLogReasons" />
									<jsp:param name="LogMode" value="Edit" />																					
								</jsp:include>	
							</td>
						</tr>
				  		<tr>
							<td colspan="2" align="center" HEIGHT="35">&nbsp;
								<fmtLoanerEditReq:message key="BTN_SUBMIT" var="varSubmit"/> 
								<fmtLoanerEditReq:message key="BTN_OVERRIDE" var="varOverride"/> 
								<fmtLoanerEditReq:message key="BTN_PRINT" var="varPrint"/> 
							    <gmjsp:button controlId="Btn_Submit" value="${varSubmit}"  gmClass="button" onClick="fnSubmit();" buttonType="Save" />
								<gmjsp:button name ="Btn_Over" controlId="Btn_Over" value="${varOverride}"  gmClass="button" onClick="fnOverride();" style="visibility: hidden;" buttonType="Save" />
								<gmjsp:button value="${varPrint}"  controlId="Btn_Print" gmClass="button" onClick="fnPrint();" disabled="true" buttonType="Load" />
								&nbsp;<a class="RightText" title="Click to view Loaner Request Approval" href="javaScript:fnPendingApproval();" ><%=strFwdLblNm%></a>	
							</td>
						</tr>	
						<%if(strShowLoanAtrb.equalsIgnoreCase("yes") && alAttribute.size()>0) {%>
						<tr><td colspan="2" height="3"></td></tr>  
						<tr><td colspan="2" class="LLine" height="1"></td></tr> 	
						<tr><td colspan="2" height="3"></td></tr>  
						
						<tr>
						<td  class="RightText" align="Left">&nbsp;</td>
						<td  class="RightText" align="Left">
						<fmtLoanerEditReq:message key="LBL_CHOOSE_REPORT"/>&nbsp;						
						<select name="Cbo_Report" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtLoanerEditReq:message key="LBL_CHOOSE_ONE"/></option>
							<option value="DELSLIP"><fmtLoanerEditReq:message key="LBL_DELIVERYSLIP"/></option>
							<option value="COLSLIP"><fmtLoanerEditReq:message key="LBL_COLLECTIONSLIP"/></option> 
						</select>
						<gmjsp:button value="&nbsp;Load&nbsp;" gmClass="button" onClick="fnLoadReport();" buttonType="Load" />&nbsp;<BR><BR>
						</td>
						</tr> 			
						 <%} %>		  			
			  				<%}else if((voidDataFlag.equals("N")) && (DataFlag.equals("Y"))){ %>
			  					<tr>
			  						<td align="center" colspan="2" class="RightTableCaption"> 
			  							<font color="RED"><fmtLoanerEditReq:message key="LBL_VOID_REQUEST_ID"/></font>
			  						</td>
			  					</tr>	
			  				  	<tr><td colspan="2" class="LLine" height="1"></td></tr>
			    	   		  	<tr id="displaytable">
									<td height="25" class="ShadeRightTableCaption" colspan="2">&nbsp;<fmtLoanerEditReq:message key="LBL_VOID_REQEUST_DETAILS"/></td>
								</tr>						
								<tr id="tabVoidReqDetails" style="display:table-row;">
									<td colspan="2">			            
							            <display:table name="requestScope.frmLoanerReqEdit.alVoidRequestDetails" requestURI="/gmOprLoanerReqEdit.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.operations.requests.displaytag.beans.DTLoanerEditRequestWrapper">						
										<fmtLoanerEditReq:message key="LBL_ID" var="varID"/><display:column property="LOGIMG" title="${varID}" class="aligncenter"/>       
										<fmtLoanerEditReq:message key="LBL_VOID_REASON" var="varVoidReason"/><display:column property="VOIDREASON" title="&nbsp;${varVoidReason}" class="alignleft" style="width:210px" /> 
										<fmtLoanerEditReq:message key="LBL_CONSIGNMENT_ID" var="varConsignId"/><display:column property="REQCNID" title="${varConsignId}" class="alignleft" style="width:110px"/>
										<fmtLoanerEditReq:message key="LBL_SET_ID" var="varSetId"/><fmtLoanerEditReq:message key="LBL_PART" var="varPart"/><display:column property="SETID" title="${varSetId}<BR>${varPart}" class="alignleft"/> 
										<fmtLoanerEditReq:message key="LBL_DESCRIPTION" var="varDescription"/><display:column property="SETNM" title="${varDescription}" class="alignleft"  style="width:120px"/> 
										<fmtLoanerEditReq:message key="LBL_QTY" var="varQty"/><display:column property="QTY" title="${varQty}" class="alignleft"/> 
										<fmtLoanerEditReq:message key="LBL_PLANNED" var="varPlanned"/><fmtLoanerEditReq:message key="LBL_SHIP_DATE" var="varShipDate"/><display:column property="REQUIREDDATE" title="${varPlanned} <br> ${varShipDate}" class="alignleft" format="<%=strRptFmt%>"/>
										<fmtLoanerEditReq:message key="LBL_ETCHID" var="varEtchId"/><display:column property="LOANERETCHID" title="${varEtchId}" class="alignleft"/>							
										<fmtLoanerEditReq:message key="LBL_REQ_STATUS" var="varReqStatus"/><display:column property="STATUS" title="${varReqStatus}" class="alignleft" style="width:80px"/>								
										</display:table>													
									</td>
								</tr>
				  		<%}else{ %>
			  				<tr class="Shade">
			  					<td colspan="2" align="center" > <fmtLoanerEditReq:message key="LBL_NOTHING_FOUND_DISPLAY"/></td>
			  				</tr>
			  			<%}%>
   				</table>
  			   </td>
  		  </tr>	
    </table>
    <DIV id="VOIDRSNHIDDEN" style="visibility:hidden">
    	<%out.print(sbVoidRsnHtml.toString()); %>
    </DIV>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
