<%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmRequestViewAsscTxn.jsp
 * Desc		 		: This screen is used to display Request Edit
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>
<%@ include file="/common/GmHeader.inc" %>

 <%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
 Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS);
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
String strTableName = "requestScope." + strFormName + ".alAsscDlist";
 
%>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<%@ taglib prefix="fmtRequestViewAsscTxn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRequestViewAsscTxn.jsp -->



<fmtRequestViewAsscTxn:setLocale value="<%=strLocale%>"/>
<fmtRequestViewAsscTxn:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>	
<bean:define id="childrequestId" name="<%=strFormName%>" property="childrequestId" type="java.lang.String"></bean:define>  

<% String strChildId = GmCommonClass.parseNull(childrequestId);
 if (!strChildId.equals(""))
		 {
	%>
	
<table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
		<tr> <td> 
		
<!-- <logic:notEqual name="<%=strFormName%>" property="childrequestId" value="">  -->
 <table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="4" height="20" class="RightDashBoardHeader">&nbsp;<fmtRequestViewAsscTxn:message key="LBL_ASSOCIATED_TRANSACTION"/></td>
		</tr>
		
				
		<tr><td colspan="4" bgcolor="#666666" height="1"></td></tr>
		 
			  <tr> 
							<td class="RightTableCaption" width=150 HEIGHT="24"><fmtRequestViewAsscTxn:message key="LBL_CHILD_REQUEST_ID"/>: </td>
							 	<td>&nbsp; <bean:write name="<%=strFormName%>" property="childrequestId" /> 
							</td>
							
							<td class="RightTableCaption" width=150 HEIGHT="24"><fmtRequestViewAsscTxn:message key="LBL_STATUS"/>: </td>
							 
							<td>&nbsp; <bean:write name="<%=strFormName%>" property="status" /> 
							</td>
						  
					</tr> 
    <table>
 
 <p> &nbsp;</p>
 
  
 <table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
		 
				
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="<%=strTableName%>" requestURI="/gmRequestEdit.do"   class="its"  > 
 				<fmtRequestViewAsscTxn:message key="LBL_PNUM" var="varPnum"/><display:column property="PNUM" title="${varPnum} #"  class="alignleft" />
				<fmtRequestViewAsscTxn:message key="LBL_PDESC" var="varPdesc"/><display:column property="PDESC" title="${varPdesc}"  class="alignleft" />
				<fmtRequestViewAsscTxn:message key="LBL_QTY" var="varQty"/><display:column property="QTY" title="${varQty}"  class="aligncenter" />
				 
			</display:table>
			</td>
		</tr>
    <table>
<!-- </logic:notEqual>	 -->

	 </td></tr>
 </table>
 <% }/*
}catch(Exception e)
{
	e.printStackTrace();
}*/
%>
  
 

 
