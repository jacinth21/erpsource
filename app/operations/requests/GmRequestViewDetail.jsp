
<%
/**********************************************************************************
 * File		 		: GmRequestViewDetail.jsp
 * Desc		 		: This screen is used to display Request Edit Detail
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>
 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap, java.util.Calendar" %>
<%@ include file="/common/GmHeader.inc" %>
<!-- WEB-INF path corrected for JBOSS migration changes --> 

<%@ taglib prefix="fmtRequestViewDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRequestViewDetail.jsp -->

<%
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
String strTableName = "requestScope." + strFormName + ".alRequestDetaillist";
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%> 
 <fmtRequestViewDetails:setLocale value="<%=strLocale%>"/>
<fmtRequestViewDetails:setBundle basename="properties.labels.custservice.ProcessRequest.GmProcessRequestItems"/>
 <table border="0" class="DtTable725" cellspacing="0" cellpadding="0">
		 	
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td colspan="2">
			<display:table name="<%=strTableName%>" requestURI="/gmRequestEdit.do"   class="its"  > 
 				<fmtRequestViewDetails:message key="LBL_PNUM" var="varPnum"/><display:column property="PNUM" title="${varPnum} #"  class="alignleft" />
				<fmtRequestViewDetails:message key="LBL_PDESC" var="varPdesc"/><display:column property="PDESC" title="${varPdesc}"  class="alignleft" />
				<fmtRequestViewDetails:message key="LBL_QTY" var="varQty"/><display:column property="QTY" title="${varQty}"  class="aligncenter" />
				 
			</display:table>
			</td>
		</tr>
    <table>
 
 <p> &nbsp;</p>
 
  
 