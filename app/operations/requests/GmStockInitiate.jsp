<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@page import="java.net.URLEncoder"%>
<%
/**********************************************************************************
 * File		 		: GmStockInitiate.jsp
 * Desc		 		: This screen is used to display Stock Initiate
 * Version	 		: 1.0
 * author			: ppandiyan
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>


<%@ taglib prefix="fmtRequestInit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmStockInitiate.jsp -->
<fmtRequestInit:setLocale value="<%=strLocale%>"/>
<fmtRequestInit:setBundle basename="properties.labels.custservice.ProcessRequest.GmStockInitiate"/>
<bean:define id="alAssocPlantList" name="frmStockInitiate" property="alAssocPlantList" type="java.util.List"></bean:define>

<%

	String strWikiTitle = GmCommonClass.getWikiTitle("STOCK_TRANSFER_INITIATE");
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));  
	String strscreentyp =GmCommonClass.parseNull((String)request.getAttribute("screentyp"));
	String strPlantId = GmCommonClass.parseNull((String)gmDataStoreVO.getPlantid());
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	String strApplDateFmt = GmCommonClass.parseNull(strGCompDateFmt);
	GmCalenderOperations.setTimeZone(strGCompTimeZone);
	String strTodays = GmCommonClass.parseNull(GmCalenderOperations.getCurrentDate(strGCompDateFmt));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Stock Initiate </TITLE>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmStockInitiate.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<SCRIPT>
var plantId='<%=strPlantId%>';
var PlantLength = <%=alAssocPlantList.size()%>;
var dateFmt = '<%=strApplDateFmt%>';
var todayDate = '<%=strTodays%>';
var fulfillComplbl='<fmtRequestInit:message key="LBL_FULFILL_COMPANY"/>';
<%
HashMap hcboVal = new HashMap();
for (int i=0;i<alAssocPlantList.size();i++)
{
	hcboVal = (HashMap)alAssocPlantList.get(i);
%>
 var PlantArray<%=i%> ="<%=hcboVal.get("PLANTID")%>,<%=hcboVal.get("PLANTNM")%>"; 
<%
}
%>	
</SCRIPT>
</head>
<BODY leftmargin="20" topmargin="10" onload="javascript:fnOnPageLoad();">
<html:form action="/gmStockRequestInitiate.do">
<html:hidden property="strOpt"/>
<html:hidden property="strAction" value="" />
<html:hidden property="hinputStr"/> 
<html:hidden property="requestType"/> 

	<table  border="0" borderwidth="830" cellspacing="0" cellpadding="0" class="border">		
		<tr>
		<td height="15" colspan="2" class="RightDashBoardHeader">
			<table border="0" width="100%" >
			<tr>
				<td colspan="3" height="15" width="90%" class="RightDashBoardHeader"><fmtRequestInit:message key="LBL_HEADER"/></td>
				<fmtRequestInit:message key="LBL_HELP" var="varHelp"/>
				<td height="15" width="10%"class="RightDashBoardHeader" align="right"> <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> </td>
			</tr></table>					
		</td>			
			</tr>
		<tr><td colspan="2" height="1" class="Line"></td></tr>	
		<tr>
		
			<td colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
			
				<tr>
					<td class="RightTableCaption" HEIGHT="23" align="right" >&nbsp;<fmtRequestInit:message key="LBL_REQUESTTYPE"/>:</td>
					<td  HEIGHT="23" align="left">&nbsp;<bean:write name="frmStockInitiate" property="requestTypeName" /> 
					</td>
										<td class="RightTableCaption" HEIGHT="23" align="right" colspan="1">&nbsp;<font color="red">* </font><fmtRequestInit:message key="LBL_FULFILL_COMPANY"/>:</td>
					<td>&nbsp;<gmjsp:dropdown controlName="fulfillCompId" SFFormName="frmStockInitiate" SFSeletedValue="fulfillCompId"
							SFValue="alFulfillCompany" codeId = "A_COMP_ID"  codeName = "A_COMP_NM" defaultValue= "[Choose One]" />
					</td>
					
				</tr>
				<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>


					<tr class="Shade">

						<td class="RightTableCaption" HEIGHT="23" align="right"><div id="date1" style="height=23"> &nbsp;<fmtRequestInit:message key="LBL_REQUIREDDATE"/>:</div></td>
					    <td>&nbsp;<gmjsp:calendar SFFormName="frmStockInitiate" controlName="requiredDate"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
					    </td>
					    
					 <td></td>
					<td></td>

					</tr>
					
				<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
				
			</table>
			</td>
		</tr>
		


		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
		<tr>
			<td colspan="2">
			<div tabIndex="-1">
				<iframe src="/GmCommonCartServlet?hAction=ReqCart&hScreen=<%=strscreentyp%>&companyInfo=<%=strCompanyInfo%>" scrolling="no" id="frmCart" marginheight="0" width="100%" height="320" frameborder="0" HIDEFOCUS="true"></iframe><BR>
			</div>
			</td>
		</tr> 
		<tr><td colspan="2" class="Line" height="1"></td></tr>

		
		<tr><td colspan="2"> 
						<jsp:include page="/operations/shipping/GmIncShipDetails.jsp">	
						<jsp:param name="screenType" value="Initiate" />		
								
					</jsp:include>	
				</td></tr>		
		<tr><td class="line" colspan="2"></td></tr>
		<tr>
             <td colspan="2"><div id="comments" >
               	<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="FORMNAME" value="frmStockInitiate" />
					<jsp:param name="ALNAME" value="alLogReasons" />						
				</jsp:include></div>
			</td>
        </tr> 
         
       
        <tr>
			<td colspan="2" align="center" height="35">
				<fmtRequestInit:message key="BTN_SUBMIT" var = "varSubmit"/>
				<gmjsp:button name="Btn_Submit" value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnStockReqSubmit();" buttonType="Save" />&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr><td colspan="2" class="Line" height="1"></td></tr>

	</table>
	<div>
	 
	</div>
</html:form>
<%@ include file="/common/GmFooter.inc" %>

</HTML>
