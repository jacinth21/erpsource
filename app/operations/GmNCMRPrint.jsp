 <%
/**********************************************************************************
 * File		 		: GmNCMRPrint.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ taglib prefix="fmtNCMRPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtNCMRPrint:setLocale value="<%=strLocale%>"/>
<fmtNCMRPrint:setBundle basename="properties.labels.operations.GmNCMRPrint"/>
<!--operations\GmNCMRPrint.jsp  -->

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmNCMRDetails = new HashMap();

	String strShade = "";

	String strNCMRId = "";
	String strDHRId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyInsp = "";
	String strQtyRej = "";
	String strUserName = "";
	String strCreatedDate = "";
	String strRejReason = "";
	String strEvalReason = "";
	String strApplnDateFmt = strGCompDateFmt;
	
	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid() ,"");
	String strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMP_LOGO"));
	if (hmReturn != null)
	{
		hmNCMRDetails = (HashMap)hmReturn.get("NCMRDETAILS");

		strNCMRId = GmCommonClass.parseNull((String)hmNCMRDetails.get("NCMRID"));
		strDHRId = GmCommonClass.parseNull((String)hmNCMRDetails.get("ID"));
		strVendName = GmCommonClass.parseNull((String)hmNCMRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmNCMRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmNCMRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmNCMRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmNCMRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmNCMRDetails.get("QTYREC"));
		strQtyInsp = GmCommonClass.parseNull((String)hmNCMRDetails.get("QTYINSP"));
		strQtyRej = GmCommonClass.parseNull((String)hmNCMRDetails.get("QTYREJ"));
		strUserName = GmCommonClass.parseNull((String)hmNCMRDetails.get("UNAME"));
		//strCreatedDate = GmCommonClass.parseNull((String)hmNCMRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmNCMRDetails.get("CDATE"),strApplnDateFmt);
		strRejReason = GmCommonClass.parseNull((String)hmNCMRDetails.get("REJREASON"));
		strEvalReason = GmCommonClass.parseNull((String)hmNCMRDetails.get("EVALREMARKS"));
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: NCMR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY  topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder">
<BR>
<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+2><fmtNCMRPrint:message key="LBL_NON_MAT_REC"/>&nbsp;<br>&nbsp;</font></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="4"></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" height="25" align="right"><fmtNCMRPrint:message key="LBL_NCMR_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strNCMRId%></td>
						<td class="RightTableCaption" align="right"><fmtNCMRPrint:message key="LBL_DHR_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strDHRId%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="25" align="right"><fmtNCMRPrint:message key="LBL_ORIGINATOR"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>
						<td class="RightText" height="25" align="right"><fmtNCMRPrint:message key="LBL_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strCreatedDate%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="25" align="right"><fmtNCMRPrint:message key="LBL_PART_NO"/>:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightText" align="right"><fmtNCMRPrint:message key="LBL_LOT#"/>:</td>
						<td class="RightText">&nbsp;<%=strControlNum%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="25" align="right"><fmtNCMRPrint:message key="LBL_DESC"/>:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
						<td class="RightText" align="right">Dept/Vendor <span class="RightTextSmall">(if Appl.)</span>:</td>
						<td class="RightText">&nbsp;<%=strVendName%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="25" align="right"><fmtNCMRPrint:message key="LBL_WORK_ORDER"/>:</td>
						<td class="RightText">&nbsp;<%=strWOId%></td>
						<td class="RightText" align="center"><fmtNCMRPrint:message key="LBL_QUANTITY"/></td>
						<td class="RightText">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td class="RightText"><fmtNCMRPrint:message key="LBL_RECVD"/></td>
									<td class="RightText"><fmtNCMRPrint:message key="LBL_INSPECTED"/></td>
									<td class="RightText"><fmtNCMRPrint:message key="LBL_REJ"/></td>
								</tr>
								<tr><td colspan="3" bgcolor=#eeeeee height="1"></td></tr>
								<tr height="18">
									<td align="center" class="RightText"><%=strQtyRec%></td>
									<td align="center" class="RightText"><%=strQtyInsp%></td>
									<td align="center" class="RightText"><%=strQtyRej%></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
	</table>
<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
			<td bgcolor="#666666" colspan="2" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
		</tr>
		<tr><td bgcolor="#eeeeee" height="18" colspan="2" class="RightTableCaption" align="center"><fmtNCMRPrint:message key="LBL_DESC_NON_CON"/></td></tr>
		<tr><td bgcolor="#eeeeee" colspan="2" height="1"></td></tr>
		<tr><td height="50" class="RightTExt" valign="top" colspan="2">&nbsp;<%=strRejReason%></td></tr>
		<tr><td bgcolor="#eeeeee" colspan="2" height="1"></td></tr>
		<tr>
			<td height="40" class="RightText" valign="top">&nbsp;<fmtNCMRPrint:message key="LBL_ORIGINATOR"/></td>
			<td height="40" width="100">&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="2" height="1"></td></tr>
	</table>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td bgcolor="#666666" height="1"></td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td bgcolor="#eeeeee" height="18" colspan="4" class="RightTableCaption" align="center"><fmtNCMRPrint:message key="LBL_DISPOSITION"/></td>
					</tr>
					<tr height="25">
						<td class="RightTExt" width="150">&nbsp;<input type="checkbox">&nbsp;<fmtNCMRPrint:message key="LBL_ACCEPT"/></td>
						<td class="RightTExt" width="150">&nbsp;<input type="checkbox">&nbsp;<fmtNCMRPrint:message key="LBL_USE_AS"/></td>
						<td class="RightTExt" width="150">&nbsp;<input type="checkbox">&nbsp;<fmtNCMRPrint:message key="LBL_REWORK"/></td>
						<td class="RightTExt" width="150">&nbsp;<input type="checkbox">&nbsp;<fmtNCMRPrint:message key="LBL_SCRAP"/></td>
					</tr>
					<tr><td bgcolor="#eeeeee" width="1" colspan="4"></td></tr>
					<tr height="25">
						<td class="RightTExt" width="200" nowrap>&nbsp;<input type="checkbox">&nbsp;<fmtNCMRPrint:message key="LBL_RETURN"/></td>
						<td class="RightTExt" width="150">&nbsp;</td>
						<td class="RightTExt" colspan="2">&nbsp;<fmtNCMRPrint:message key="LBL_RESPONSIBILITY"/>&nbsp;&nbsp;<input type="checkbox"><fmtNCMRPrint:message key="LBL_IN_HOUSE"/><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"><fmtNCMRPrint:message key="LBL_SUPPLIER"/><br>
						</td>
					</tr>
					<tr><td bgcolor="#eeeeee" width="1" colspan="4"></td></tr>
					<tr height="25">
						<td class="RightTExtSmall">&nbsp;<fmtNCMRPrint:message key="LBL_DATE"/>&nbsp;</td>
						<td class="RightTExt" colspan="3" width="150">&nbsp;</td>
					</tr>
					<tr><td bgcolor="#666666" width="1" colspan="4"></td></tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
	</table>
<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
		</tr>
		<tr><td bgcolor="#eeeeee" height="18" colspan="4" class="RightTableCaption" align="center"><fmtNCMRPrint:message key="LBL_REMARKS"/></td></tr>
		<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
		<tr><td height="50" class="RightTExt" valign="top" colspan="4">&nbsp;<%=strEvalReason%></td></tr>
		<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
		<tr><td height="18" colspan="4">&nbsp;</td></tr>
		<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
		<tr>
			<td height="40" class="RightTextSmall" valign="top">&nbsp;<fmtNCMRPrint:message key="LBL_TIT_NAME"/></td>
			<td height="40" class="RightTextSmall" valign="top">&nbsp;<fmtNCMRPrint:message key="LBL_SIGNATURE_DT"/></td>
			<td height="40" class="RightTextSmall" valign="top">&nbsp;<fmtNCMRPrint:message key="LBL_TIT_NAME"/></td>
			<td height="40" class="RightTextSmall" valign="top">&nbsp;<fmtNCMRPrint:message key="LBL_SIGNATURE_DT"/></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>
	</table>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700" align="center">
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td bgcolor="#666666" height="1"></td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td bgcolor="#eeeeee" height="18" colspan="4" class="RightTableCaption" align="center"><fmtNCMRPrint:message key="LBL_CLOSE"/></td>
					</tr>
					<tr height="25">
						<td class="RightTExt" align="right">&nbsp;<fmtNCMRPrint:message key="LBL_DUE_DT"/>:</td>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr><td bgcolor="#eeeeee" width="1" colspan="4"></td></tr>
					<tr height="25">
						<td class="RightTExt" align="right" nowrap>&nbsp;<fmtNCMRPrint:message key="LBL_REINS"/>:</td>
						<td class="RightTExt">&nbsp;<input type="checkbox">Yes&nbsp;<input type="checkbox"><fmtNCMRPrint:message key="LBL_NO"/></td>
						<td class="RightTExt" width="300" align="right" >&nbsp;<fmtNCMRPrint:message key="LBL_INS_SHEET"/>:</td>
						<td class="RightTExt" width="250">&nbsp;<input type="checkbox">Yes&nbsp;<input type="checkbox"><fmtNCMRPrint:message key="LBL_NO"/></td>
					</tr>
					<tr><td bgcolor="#eeeeee" width="1" colspan="4"></td></tr>
					<tr height="25">
						<td class="RightTExt" align="right" nowrap>&nbsp;<fmtNCMRPrint:message key="LBL_ORIGINATOR"/><fmtNCMRPrint:message key="LBL_CAPA"/>:</td>
						<td class="RightTExt"">&nbsp;<input type="checkbox">Yes&nbsp;<input type="checkbox"><fmtNCMRPrint:message key="LBL_NO"/></td>
						<td class="RightTExt" width="300" align="right" >&nbsp;<fmtNCMRPrint:message key="LBL_CAPA#"/>:</td>
						<td class="RightTExt" width="250">&nbsp;</td>
					</tr>
					<tr><td bgcolor="#eeeeee" width="1" colspan="4"></td></tr>
					<tr height="15"><td class="RightTExt" colspan="4"></td></tr>
					<tr><td bgcolor="#eeeeee" width="1" colspan="4"></td></tr>
					<tr>
						<td colspan="4">
						  <table cellpadding="0" cellspacing="0" border="0" width="100%">
							<tr>
								<td height="40" class="RightTextSmall" valign="top">&nbsp;<fmtNCMRPrint:message key="LBL_TIT_NAME"/></td>
								<td height="40" class="RightTextSmall" valign="top">&nbsp;<fmtNCMRPrint:message key="LBL_SIGNATURE_DT"/></td>
								<td height="40" class="RightTextSmall" valign="top">&nbsp;<fmtNCMRPrint:message key="LBL_TIT_NAME"/></td>
								<td height="40" class="RightTextSmall" valign="top">&nbsp;<fmtNCMRPrint:message key="LBL_SIGNATURE_DT"/></td>
							</tr>
						  </table>
						</td>
					</tr>
					<tr><td bgcolor="#666666" width="1" colspan="4"></td></tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1"></td>
		</tr>
	</table>

	<BR>
	<BR>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="RightText"><fmtNCMRPrint:message key="LBL_GM"/></span>

	<div id="button">
	<table border="0" width="700" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center" height="30">
			<BR>
			<BR>
			<fmtNCMRPrint:message key="BTN_PRINT" var="varPrint"/><gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" buttonType="Load" gmClass="button" onClick="fnPrint();"/>&nbsp;&nbsp;
			<fmtNCMRPrint:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" buttonType="Load" gmClass="button" onClick="window.close();"/>&nbsp;
			</td>
		<tr>
	</table>
	</div>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
