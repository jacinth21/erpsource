<!-- \operations\GmSetPicSlip.jsp -->

 <%
/**********************************************************************************
 * File		 		: GmSetPicSlip.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtSetPicSlip" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtSetPicSlip:setLocale value="<%=strLocale%>"/>
<fmtSetPicSlip:setBundle basename="properties.labels.operations.GmSetPicSlip"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strProjId =	"";
	String strSetId = "";
	String strSetNm = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";
	String strConsignId = "";
	String strSetName= "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strSource = "";
	String strId = "";
	
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
		//strSetId = (String)session.getAttribute("hSetId") == null?"":(String)session.getAttribute("hSetId");
		
		hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
		strSetId =  (String)hmConsignDetails.get("SETID");
		strConsignId = (String)hmConsignDetails.get("CID");
		strSetName= (String)hmConsignDetails.get("SNAME");
		strAccName = (String)hmConsignDetails.get("ANAME");
		strUserName = (String)hmConsignDetails.get("UNAME");
		strIniDate = (String)hmConsignDetails.get("CDATE");
		strDesc = (String)hmConsignDetails.get("COMMENTS"); 
		strSource = GmCommonClass.parseNull((String)hmConsignDetails.get("CTYPE"));
	}
	strId = strConsignId+"$"+strSource;

	int intSize = 0;
	HashMap hcboVal = null;

	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<tr><td class=Line colspan=5 height=1></td></tr>");
	sbStartSection.append("<table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr><td class=RightText HEIGHT=20 align=right>&nbsp;Consignment Id:</td>");
	sbStartSection.append("<td class=RightText>&nbsp;<b>");sbStartSection.append(strConsignId);
	sbStartSection.append("</b></td></tr><tr class=shade><td class=RightText HEIGHT=20 align=right>&nbsp;Set Name:</td> ");
	sbStartSection.append("<td class=RightText>&nbsp;<b>");sbStartSection.append(GmCommonClass.getStringWithTM(strSetName));
	sbStartSection.append("</b></td></tr>");
	sbStartSection.append("<tr><td colspan=2><table cellspacing=0 cellpadding=0 border=0 width=100% id=myTable>");
	sbStartSection.append("<tr><td colspan=5 height=1 bgcolor=#666666></td></tr><tr class=ShadeRightTableCaption>");
	sbStartSection.append("<td width=20 height=25>S #</td><td height=25  width=60>Part #</td>");
	sbStartSection.append("<td width=300>Description</td><td>Set Qty</td>");
	sbStartSection.append("<td width=200>&nbsp;&nbsp;&nbsp;&nbsp;Qty&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Control Number</td></tr>");
	sbStartSection.append("<tr><td class=Line colspan=5 height=1></td></tr>");


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set PIC Slip </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>

function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmVendor">
<table border="0" width="100" cellspacing="0" cellpadding="0" >
	<tr>
		<td align="right" HEIGHT="50">
			<img align="middle" src='/GmCommonBarCodeServlet?ID=<%=strId%>&type=2d' height="50" width="50" />
		</td>
		<td HEIGHT="50" style=" padding-left:5px;">
			<table border="1" width="500" cellspacing="0" cellpadding="0" bordercolorlight="black">
				<tr>
					<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtSetPicSlip:message key="LBL_CONSIGNMNET_ID"/>:</td>
					<td class="RightText">&nbsp;<b><%=strConsignId%></b></td>
				</tr>
				<tr>
					<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtSetPicSlip:message key="LBL_SET_NAME"/>:</td>
					<td class="RightText">&nbsp;<b><%=strSetId%> / <%=GmCommonClass.getStringWithTM(strSetName)%></b></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
	<span class="RightText">---------------------------------------  <font size="-2"><fmtSetPicSlip:message key="LBL_TEAR_HERE_AND_AFFIX_TOP_PORTION_TO_SET"/> </font>----------------------------------------</span>
	<BR>
<BR>
	<table border="0" width="600" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
			<td height="25" class="RightDashBoardHeader"><fmtSetPicSlip:message key="LBL_SET_PIC_SLIP"/></td>
			<td bgcolor="#666666" width="1" rowspan="3"><img src="<%=strImagePath%>/spacer.gif" width="1" height="1"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td width="598" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtSetPicSlip:message key="LBL_CONSIGNMNET_ID"/>:</td>
						<td class="RightText">&nbsp;<b><%=strConsignId%></b></td>
					</tr>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtSetPicSlip:message key="LBL_SET_NAME"/>:</td>
						<td class="RightText">&nbsp;<b><%=GmCommonClass.getStringWithTM(strSetName)%></b></td>
					</tr>
					<!--<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;Account Name:</td>
						<td class="RightText">&nbsp;<%=strAccName%></td>
					</tr>-->
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtSetPicSlip:message key="LBL_BUILT_BY"/>:</td>
						<td class="RightText">&nbsp;____________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtSetPicSlip:message key="LBL_DATE_INITIATED"/>:&nbsp;<%=strIniDate%></td>
					</tr>
					<tr class="shade">
						<td class="RightText" valign="top" align="right" HEIGHT="60"><fmtSetPicSlip:message key="LBL_COMMENTS"/>:</td>
						<td class="RightText" valign="top" >&nbsp;<%=strDesc%></td>
					</tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<fmtSetPicSlip:message key="LBL_VERIFIED_BY"/>:</td>
						<td class="RightText">&nbsp;____________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtSetPicSlip:message key="DATE_VERIFIED"/>:&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" id="myTable">
								<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td width="20" height="25"><fmtSetPicSlip:message key="LBL_S_#"/></td>
									<td  width="60" height="25"><fmtSetPicSlip:message key="LBL_PART_#"/></td>
									<td width="300"><fmtSetPicSlip:message key="LBL_DESCRIPTION"/></td>
									<td><fmtSetPicSlip:message key="LBL_SE_QTY"/></td>
									<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;<fmtSetPicSlip:message key="LBL_QTY"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtSetPicSlip:message key="LBL_CONTROL_NUMBER"/></td>
								</tr>
<%
						intSize = alSetLoad.size();
						int intLoop = 29;
						if (intSize > 0)
						{
							hcboVal = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alSetLoad.get(i);
%>
								<tr>
									<td width="20" class="RightText" height="20">&nbsp;<%=i+1%></td>
									<td width="60" class="RightText" height="20">&nbsp;<%=(String)hcboVal.get("PNUM")%></td>
									<td class="RightText" width="300" wrap><%=GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"))%></td>
									<td align="center" class="RightText"><%=(String)hcboVal.get("QTY")%></td>
									<td class="RightText">&nbsp;&nbsp;&nbsp;_ _ _ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_ _ _ _ _ _ _ _ _ _
									</td>
								</tr>
								<tr><td bgcolor="#eeeeee" colspan="5" height="1"></td></tr>
<%
								if (i==intLoop)
								{
									intLoop = intLoop + 29;
%>
								<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
								<tr><td colspan="5" height="20" class="RightText" align="right"><fmtSetPicSlip:message key="LBL_CONT_ON_PAGE"/> <%=intLoop/29%> -></td></tr>
								</table>
								<p STYLE="page-break-after: always"></p>
								<span class="RightText">Page <%=intLoop/29%></span>
<%
								out.print(sbStartSection);
%>
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
								} //end of IF
							} // end of FOR
						}else {
%>
						<tr><td colspan="5" height="50" align="center" class="RightTextRed"><BR><fmtSetPicSlip:message key="LBL_NO_PART_NUMBERS_HEVE_BEEN_MAPPED_FOR_THIS_SET"/> !</td></tr>
<%
						}		
%>
							</table>
						</td>
					</tr>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>
	<table border="0" width="600" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" id="button">
			<fmtSetPicSlip:message key="LBL_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtSetPicSlip:message key="LBL_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close()" buttonType="Load" />&nbsp;&nbsp;
			</td>
		<tr>
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
