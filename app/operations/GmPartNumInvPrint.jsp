
<%
	/**********************************************************************************
	 * File		 		: GmPartNumInvPrint.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 1.0
	 * author			: Dhinakaran James
	 ************************************************************************************/
%>

<%@ page language="java"%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*,java.lang.String,java.util.StringTokenizer" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!--operations\GmPartNumInvPrint.jsp  -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>


<%@ taglib prefix="fmtPartNumInvPrint" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartNumInvPrint:setLocale value="<%=strLocale%>"/>
<fmtPartNumInvPrint:setBundle basename="properties.labels.operations.GmPartNumInvPrint"/>


<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	try {
		GmServlet gm = new GmServlet();
		gm.checkSession(response, session);

		

		DecimalFormat doublef = new DecimalFormat("#.##");

		HashMap hmReturn = (HashMap) request.getAttribute("hmReturn");

		HashMap hmLoop = new HashMap();
		HashMap hcboVal = new HashMap();

		String strProjId = (String) request.getAttribute("hProjId") == null ? "" : ((String) request.getAttribute("hProjId")).equals("0") ? "":" - "+(String) request.getAttribute("hProjId") ;

		String strDHRDetailFlag = GmCommonClass
				.parseNull((String) request
						.getAttribute("hDHRDetailFlag")) == "Y" ? "checked"
				: "";
		String strMFGFlag = GmCommonClass.parseNull((String) request
				.getAttribute("hMFGFlag")) == "Y" ? "checked" : "";
		String strBuiltSetFlag = GmCommonClass
				.parseNull((String) request
						.getAttribute("hBuiltSetFlag")) == "Y" ? "checked"
				: "";
		String strShippingFlag = GmCommonClass
				.parseNull((String) request
						.getAttribute("hEnableShipp")) == "Y" ? "checked"
				: "";
		String strIHFlag = GmCommonClass.parseNull((String) request
				.getAttribute("hEnableIH")) == "Y" ? "checked" : "";
		
		String strSkipLoanerSection = GmCommonClass.parseNull(GmCommonClass.getString("LOAD_LOANER_INV_SECTION"));

		String strLoanQty = "";
		String strPartNum = "";
		String strPartDesc = "";
		String strQtyInStock = "";
		String strQtyInShelf = "";
		String strQtyInShelfAlloc = "";
		String strShelfInAlloc = "";
		String strQtyShipAlloc = "";
		String strQtyRMAlloc = "";
		String strQtyMFGTBR = "";
		String strQtyMFGRelease = "";
		String strQtyDHRWIP = "";
		String strQtyDHR_PEN_QC_INS_ = "";
		String strQtyDHR_PEN_QC_REL = "";
		String strQtyDHR_QC_APP = "";
		String strQtyBUILTSET = "";

		String strQtyInBulk = "";
		String strQtyInWIPSets = "";
		String strQtyInBulkAlloc = "";
		String strDHRAlloc = "";
		String strQtyInQuarantine = "";
		String strQtyInQuarantineAlloc = "";

		String strRepackAvail = "";
		String strRepackAlloc = "";

		String strQtyInReturnsHold = "";
		String strQtyInReturnsHoldAlloc = "";
		String strQtyInPO = "";
		String strQtyInWIP = "";
		String strQtyInFA = "";
		String strQtyInFAWIP = "";
		String strTBE = "";
		String strRawMatQty = "";

		String strIhAVailQty = "";
		String strIhAllocQty = "";
		String strIhSetQty = "";
		String strIhItemQty = "";
		
		String strQtyInRW = "";
		String strQtyInRWAlloc = "";
		
		String strProjName = "";

		String strShade = "";
		String strIncludedInvID = "";
		int intLoop = 0;
		
		String strShowFG = "";
		String strShowRM = "";
		String strShowBulk = "";
		String strShowBS = "";
		String strShowPN = "";
		String strShowQN = "";
		String strShowRtn = "";
		String strShowPO = "";
		String strShowMFG = "";
		String strShowDHR = "";
		String strShowShip = "";
		String strShowIH = "";
		String strShowLN = "";
		String strShowRW = "";		
		String strShowIT = "";
		String strQtyInIntransit = "";
		String strQtyInIntransitAlloc = "";
		
		ArrayList alReport = new ArrayList();
		if (hmReturn != null) {
			alReport = (ArrayList) hmReturn.get("REPORT");
		}
		if (alReport != null) {
			intLoop = alReport.size();
		}	
		GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmPartNumInvPrint", strSessCompanyLocale);
		String strlblPartNumber =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PART_NUM"));
		String strlblFG =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_FG"));
		String strlblInventory =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INVENTORY"));
		String strlblDescription =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DESCRIPTION"));
		String strlblRM =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RM"));
		String strlblBulk =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_BULK"));
		String strlblBuilt =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_BUILT"));
		String strlblSet =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SET"));
		String strlblrepacking =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RE_PACK"));
		String strlblQuanInv =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUA_INV"));
		String strlblReturnhold =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RET_HOLD"));
		String strlblQtyInPO =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QTY_IN_PO"));
		String strlblMGRuns =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_MFG_RET"));
		String strlblDHR =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DHR"));
		String strlblshipping =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QTY_IN_SHIPPING"));
		String strlblinhouse =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_INHOUSE"));
		String strlblloaner =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LOANER"));
		String strlblRW =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RW"));
		String strlblAvail =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_AVAIL"));
		String strlblAlloc =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ALLOC"));
		String strlblOnShelf =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ON_SHELF"));
		String strlblWipSet =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_WIP_SETS"));
		String strlblTBESet =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TBE_SETS"));
		String strlblloose =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LOOSE"));
		String strlblTBR =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TBR"));
		String strlblWIP =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_WIP"));
		String strlblReleased =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RELEASED"));
		String strlblRec =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REC"));
		String strlblQc =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QC"));
		String strlblQcReleased =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QC_RELEASE"));
		String strlblSets =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SETS"));
		String strlblItems =GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ITEMS"));

		
		
			
		String strIncludedInv = GmCommonClass.parseNull((String)request.getAttribute("INCLUDEINV"));
		String strExcludedInv = GmCommonClass.parseNull((String)request.getAttribute("EXCLUDEINV"));	
		StringBuffer sbStartSection = new StringBuffer();
		sbStartSection
		.append("<table border=0 width=100% cellspacing=0 cellpadding=0 valign=top>");
		sbStartSection
		.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
		sbStartSection
		.append("<td HEIGHT=24 align=center width=60>"+strlblPartNumber+"</td>");
		sbStartSection.append("<td width=350>&nbsp;"+strlblDescription+"</td>");
		
		StringTokenizer strIncludeInvs = new StringTokenizer(strIncludedInv,",");
		while(strIncludeInvs.hasMoreTokens())
		{
			strIncludedInvID = strIncludeInvs.nextToken();
			if(strIncludedInvID.equals("60001"))
			{
				strShowFG = "Y";
				sbStartSection
				.append("<td colspan=2 align=center>"+strlblFG+"<BR>"+strlblInventory+"</td>");
			}
			if(strIncludedInvID.equals("60002"))
			{
				strShowRM = "Y";	
				sbStartSection
				.append("<td colspan=2 align=center>"+strlblRM+"<BR>"+strlblInventory+"</td>");
			}
			if(strIncludedInvID.equals("60003"))
			{
				strShowBulk = "Y";	
				sbStartSection
				.append("<td colspan=4 align=center>"+strlblBulk+"<BR>"+strlblInventory+"</td>");
			}
			if(strIncludedInvID.equals("60004"))
			{
				strShowBS = "Y";
				sbStartSection
				.append("<td align=center>"+strlblBuilt+"<BR>"+strlblSet+"</td>");
			}
			if(strIncludedInvID.equals("60005"))
			{
				strShowPN = "Y";	
				sbStartSection
				.append("<td colspan=2 align=center>"+strlblrepacking+"</td>");
			}
			if(strIncludedInvID.equals("60006"))
			{
				strShowQN = "Y";
				sbStartSection
				.append("<td colspan=2 align=center>"+strlblQuanInv+"</td>");
			}
			if(strIncludedInvID.equals("60007"))
			{
				strShowRtn = "Y";	
				sbStartSection
				.append("<td colspan=2 align=center>"+strlblReturnhold+"</td>");
			}
			if(strIncludedInvID.equals("60008"))
			{
				strShowPO = "Y";
				sbStartSection.append("<td colspan=1 align=center>"+strlblQtyInPO+"</td>");
			}
			if(strIncludedInvID.equals("60009"))
			{
				strShowMFG = "Y";
				sbStartSection.append("<td colspan=2 align=center>"+strlblMGRuns+"</td>");
			}
			if(strIncludedInvID.equals("60010"))
			{
				strShowDHR = "Y";
				sbStartSection.append("<td colspan=5 align=center>"+strlblDHR+"</td>");
			}
			if(strIncludedInvID.equals("60011"))
			{
				strShowShip = "Y";
				sbStartSection
				.append("<td align=center>"+strlblshipping+"</td>");
			}
			if(strIncludedInvID.equals("60012"))
			{
				strShowIH = "Y";
				sbStartSection.append("<td colspan=4 align=center>"+strlblinhouse+"<BR>"+strlblInventory+"</td>");
			}
			if(strIncludedInvID.equals("60013"))
			{
				strShowLN = "Y";
				sbStartSection.append("<td align=center>"+strlblloaner+"<BR>"+strlblInventory+"</td>");
			}
			if(strIncludedInvID.equals("60014"))
			{
				strShowRW = "Y";
				sbStartSection
				.append("<td colspan=2 align=center>"+strlblRW+"<BR>"+strlblInventory+"</td>");
			}
			if(strIncludedInvID.equals("60015"))
			{
				strShowIT = "Y";
				sbStartSection
				.append("<td colspan=2 align=center>"+strlblFG+"<BR>"+strlblInventory+"</td>");
			}
			
		}
		sbStartSection.append("</tr>");
		sbStartSection
				.append("<tr><td class=Line colspan=34></td></tr>");
		sbStartSection
				.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
		sbStartSection.append("<td colspan=2>&nbsp;</td>");
		
			if(!strShowFG.equals("")){
				sbStartSection.append("<td width=60 align=center>"+strlblAvail+".</td>");
				sbStartSection.append("<td width=60 align=center>"+strlblAlloc+"</td>");
				
			}
			if(!strShowRW.equals("")){
				sbStartSection.append("<td width=50 align=center>"+strlblAvail+"</td>");
				sbStartSection.append("<td width=50 align=center>"+strlblAlloc+".</td>");
			}
			if(!strShowRM.equals("")){
				sbStartSection.append("<td width=60 align=center>"+strlblOnShelf+"</td>");
				sbStartSection.append("<td width=60 align=center>"+strlblAlloc+".</td>");
			}
			if(!strShowBulk.equals("")){
				sbStartSection.append("<td width=60 align=center>"+strlblWipSet+"</td>");
				sbStartSection.append("<td width=60 align=center>"+strlblTBESet+"</td>");
				sbStartSection.append("<td width=60 align=center>"+strlblloose+"</td>");
				sbStartSection.append("<td width=60 align=center>"+strlblAlloc+".</td>");
			}
			if(!strShowBS.equals("")){
				sbStartSection.append("<td width=60 align=center>&nbsp; </td>");
			}
			if(!strShowPN.equals("")){
				sbStartSection.append("<td width=60 align=center>"+strlblAvail+".</td>");
				sbStartSection.append("<td width=60 align=center>"+strlblAlloc+"</td>");
			}
			if(!strShowQN.equals("")){
				sbStartSection.append("<td width=60 align=center>"+strlblAvail+".</td>");
				sbStartSection.append("<td width=60 align=center>"+strlblAlloc+".</td>");
			}
			if(!strShowRtn.equals("")){
				sbStartSection.append("<td width=60 align=center>"+strlblAvail+".</td>");
				sbStartSection.append("<td width=60 align=center>"+strlblAlloc+"</td>");
			}
			if(!strShowPO.equals("")){
				sbStartSection.append("<td width=60 align=center>&nbsp;</td>");
			}
			if(!strShowMFG.equals("")){
				sbStartSection.append("<td width=50 align=center>"+strlblTBR+"</td>");
				sbStartSection.append("<td width=50 align=center>"+strlblReleased+"</td>");
			}
			if(!strShowDHR.equals("")){
				sbStartSection.append("<td width=50 align=center>"+strlblWIP+"</td>");
				sbStartSection.append("<td width=50 align=center>"+strlblRec+"</td>");
				sbStartSection.append("<td width=50 align=center>"+strlblQc+"</td>");
				sbStartSection.append("<td width=50 align=center>"+strlblQcReleased+"</td>");
				sbStartSection.append("<td width=50 align=center>"+strlblAlloc+"</td>");
			}
			if(!strShowShip.equals("")){
				sbStartSection.append("<td width=60 align=center>"+strlblAlloc+"</td>");
			}
			if(!strShowIH.equals("")){
				sbStartSection.append("<td width=50 align=center>"+strlblAvail+"</td>");
				sbStartSection.append("<td width=50 align=center>"+strlblAlloc+"</td>");
				sbStartSection.append("<td width=50 align=center>"+strlblSets+"</th>");
				sbStartSection.append("<td width=50 align=center>"+strlblItems+"</th>");
			}
			if(!strShowLN.equals("")){
				sbStartSection.append("<td width=50 align=center>"+strlblAvail+"</td>");
			}	
			if(!strShowIT.equals("")){
				sbStartSection.append("<td width=60 align=center>"+strlblAvail+".</td>");
				sbStartSection.append("<td width=60 align=center>"+strlblAlloc+"</td>");
				
			}
		
		sbStartSection.append("</tr>");
		sbStartSection.append("<tr><td class=Line colspan=34></td></tr>");
		
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Inventory Report</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript"
	src="<%=strOperationsJsPath%>/GmPartNumInvReport.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
	<FORM name="frmAccount" method="POST">
		<table border="0" class="DTTable1000" cellspacing="0" cellpadding="0">			
			<tr>
				<td colspan="2" height="25" class="RightDashBoardHeader"><fmtPartNumInvPrint:message key="LBL_REPORT"/> <%=strProjId%></td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="2"></td>
			</tr>
			<tr>
				<td colspan="2" valign="top">
					<table border="0" width="100%" cellspacing="0" cellpadding="0"
						valign="top">
						<tr bgcolor="#eeeeee" class="RightTableCaption">
							<td HEIGHT="24" align="center" width="60"><fmtPartNumInvPrint:message key="LBL_PART_NUM"/></td>
							<td width="350">&nbsp;<fmtPartNumInvPrint:message key="LBL_DESCRIPTION"/></td>
									<% if(!strShowFG.equals("")){%>
						  			<th colspan="2" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvPrint:message key="LBL_FG_INV"/><BR></th>	
						            <%} %>
						            <% if(!strShowRW.equals("")){%>
						            <th colspan="2" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvPrint:message key="LBL_RW_INV"/><BR></th>
						            <%} %>	
						            <% if(!strShowRM.equals("")){%>
						  			<th colspan="2" align="center" class="ShadeDarkGrayTD"><fmtPartNumInvPrint:message key="LBL_RM_INV"/><BR></th>		
						            <%} %>
						            <% if(!strShowBulk.equals("")){%>
						  			<th colspan="4" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_BULK_LOOSE"/><BR></th>	
						            <%} %>
						             <% if(!strShowBS.equals("")){%>
						            <th align="center" class="ShadeDarkBrownTD">&nbsp;<fmtPartNumInvPrint:message key="LBL_BUILT_SET"/>&nbsp;<br>&nbsp;</th>
						            <%} %>
						            <% if(!strShowPN.equals("")){%>
						  			<th width="100" colspan="2" align="center" class="ShadeDarkYellowTD"><fmtPartNumInvPrint:message key="LBL_RE_PACK"/></th>	
						            <%} %>		
						            <% if(!strShowQN.equals("")){%>
						  			<th align="center" colspan="2" class="ShadeDarkOrangeTD"><fmtPartNumInvPrint:message key="LBL_QUA_INV"/></th>	
						            <%} %>				           
						            <% if(!strShowRtn.equals("")){%>
						  			<th width="100" colspan="2" align="center" class="ShadeDarkYellowTD"><fmtPartNumInvPrint:message key="LBL_RET_HOLD"/><BR></th>	
						            <%} %>						           
						            <% if(!strShowPO.equals("")){%>
						            <th width="60" class="ShadeDarkBrownTD"><fmtPartNumInvPrint:message key="LBL_QTY_IN"/><br></th>						  				
						            <%} %>
						            <% if(!strShowMFG.equals("")){%>
						            <th colspan="2" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvPrint:message key="LBL_MFG_RET"/></th>						  					
						            <%} %>						           
						            <% if(!strShowDHR.equals("")){%>
						  			<th colspan="5" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_DHR"/></th>	
						            <%} %>
						            <% if(!strShowShip.equals("")){%>
						  			<th align="center" class="ShadeDarkBrownTD"><fmtPartNumInvPrint:message key="LBL_SHIPPING_HOLD"/></th>
						            <%} %>						           
						            <% if(!strShowIH.equals("")){%>
						            <th colspan="4" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvPrint:message key="LBL_IH_INV"/><BR></th>						  				
						            <%} %>		
						            <% if(!strShowLN.equals("")){%>
						            <th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvPrint:message key="LBL_LOANER_INV"/><br></th>
						            <%} %>	
						            <% if(!strShowIT.equals("")){%>
						  			<th colspan="2" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvPrint:message key="LBL_IN_TRANSIT"/><br></th>	
						            <%} %>					            			  				
								
						</tr>
						<tr>
							<td class="Line" colspan="35"></td>
						</tr>
						<tr bgcolor="#eeeeee" class="RightTableCaption">
							<td colspan="2">&nbsp;</td>
								
						    <% if(!strShowFG.equals("")){%>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_AVAIL"/>.</td>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_ALLOC"/></td>							
							<%} %>
							<% if(!strShowRW.equals("")){%>
							<td width="50" align="center"><fmtPartNumInvPrint:message key="LBL_AVAIL"/></td>
							<td width="50" align="center"><fmtPartNumInvPrint:message key="LBL_ALLOC"/>.</td>
							<%} %>
							<% if(!strShowRM.equals("")){%>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_ON_SHELF"/></td>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_ALLOC"/>.</td>
							<%} %>
							<% if(!strShowBulk.equals("")){%>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_WIP_SETS"/></td>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_TBE_SETS"/></td>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_LOOSE"/></td>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_ALLOC"/>.</td>
							<%} %>
							<% if(!strShowBS.equals("")){%>							
							<td width="60" align="center">&nbsp;</td>
							<%}%>
							<% if(!strShowPN.equals("")){%>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_AVAIL"/>.</td>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_ALLOC"/></td>
							<%} %>
							<% if(!strShowQN.equals("")){%>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_AVAIL"/>.</td>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_ALLOC"/></td>
							<%} %>
							<% if(!strShowRtn.equals("")){%>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_AVAIL"/>.</td>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_ALLOC"/></td>
							<%} %>
							<% if(!strShowPO.equals("")){%>
							<td width="60" align="center">&nbsp;</td>
							<%} %>
							<% if(!strShowMFG.equals("")){%>
							<td width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvPrint:message key="LBL_TBR"/></td>
							<td width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvPrint:message key="LBL_RELEASED"/></td>
							<%}%>
							<% if(!strShowDHR.equals("")){%>
							<td width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_WIP"/></td>
							<td width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_REC"/></td>
							<td width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_QC"/></td>
							<td width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_QC_RELS"/><br></td>
							<td width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_ALLOC"/></td>							
							<%}%>
							<% if(!strShowShip.equals("")){%>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_ALLOC"/></td>
							<%}%>
							<% if(!strShowIH.equals("")){%>
							<td width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_AVAIL"/></td>
							<td width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_ALLOC"/></td>
							<td width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_SETS"/></td>
							<td width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvPrint:message key="LBL_ITEMS"/></td>
							<%}%>							
							<% if(!strShowLN.equals("")){%>
							<td width="50" align="center"><fmtPartNumInvPrint:message key="LBL_AVAIL"/></td>
							<%} %>	
							 <% if(!strShowIT.equals("")){%>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_AVAIL"/>.</td>
							<td width="60" align="center"><fmtPartNumInvPrint:message key="LBL_ALLOC"/></td>							
							<%} %>						
						
						</tr>
						<tr>
							<td class="Line" colspan="35"></td>
						</tr>
						<%
							int intBreak = 45;
								if (intLoop > 0) {
									int intStock = 0;
									int intBulk = 0;
									int intReturns = 0;
									int intPO = 0;
									int intDHR = 0;

									for (int i = 0; i < intLoop; i++) {
										hmLoop = (HashMap) alReport.get(i);
										strPartNum = GmCommonClass.parseNull((String) hmLoop
												.get("PNUM"));
										strPartDesc = GmCommonClass.parseNull((String) hmLoop
												.get("PDESC"));

										strQtyInShelf = GmCommonClass.parseZero((String) hmLoop
												.get("INSHELF"));
										strQtyInShelfAlloc = GmCommonClass
												.parseZero((String) hmLoop.get("SHELFALLOC"));
										strQtyInIntransit = GmCommonClass.parseZero((String)hmLoop.get("INTRANS_AVAIL_QTY"));
										strQtyInIntransitAlloc = GmCommonClass.parseZero((String)hmLoop.get("INTRANS_ALLOC_QTY"));
										strShelfInAlloc = GmCommonClass
												.parseZero((String) hmLoop
														.get("INV_ALLOC_TO_PACK"));
										strQtyInRW = GmCommonClass
										.parseZero((String) hmLoop.get("INRW"));
										strQtyInRWAlloc = GmCommonClass
										.parseZero((String) hmLoop.get("RWALLOC"));

										strRawMatQty = GmCommonClass.parseZero((String) hmLoop
												.get("RMQTY"));
										double dRawMatQty = Double.parseDouble(strRawMatQty);
										strRawMatQty = doublef.format(dRawMatQty);

										strRepackAvail = GmCommonClass
												.parseZero((String) hmLoop.get("REPACKAVAIL"));
										strRepackAlloc = GmCommonClass
												.parseZero((String) hmLoop.get("REPACKALLOC"));

										strQtyInQuarantine = GmCommonClass
												.parseZero((String) hmLoop.get("INQUARAN"));
										strQtyInQuarantineAlloc = GmCommonClass
												.parseZero((String) hmLoop.get("QUARALLOC"));

										strQtyInBulk = GmCommonClass.parseZero((String) hmLoop
												.get("INBULK"));
										strQtyInWIPSets = GmCommonClass
												.parseZero((String) hmLoop.get("IN_WIP_SET"));
										strQtyInBulkAlloc = GmCommonClass
												.parseZero((String) hmLoop.get("BULKALLOC"));
										strTBE = GmCommonClass.parseZero((String) hmLoop
												.get("C205_TBE_ALLOC"));

										//strQtyInReturnsHold = GmCommonClass.parseZero((String)hmLoop.get("INRETURNS"));
										strQtyInReturnsHold = GmCommonClass
												.parseZero((String) hmLoop.get("RETURNAVAIL"));
										strQtyInReturnsHoldAlloc = GmCommonClass
												.parseZero((String) hmLoop.get("RETURNALLOC"));

										strQtyInPO = GmCommonClass.parseZero((String) hmLoop
												.get("POPEND"));

										strQtyInWIP = GmCommonClass.parseZero((String) hmLoop
												.get("POWIP"));

										strQtyBUILTSET = GmCommonClass
												.parseZero((String) hmLoop.get("BUILD_SET_QTY"));
										strQtyRMAlloc = GmCommonClass.parseZero((String) hmLoop
												.get("RM_ALLOC_QTY"));
										strQtyMFGTBR = GmCommonClass.parseZero((String) hmLoop
												.get("MFG_TBR"));
										strQtyMFGRelease = GmCommonClass
												.parseZero((String) hmLoop.get("MFG_RELEASE"));
										double dQtyMFGTBR = Double.parseDouble(strQtyMFGTBR);
										double dQtyMFGRelease = Double
												.parseDouble(strQtyMFGRelease);
										strQtyMFGTBR = doublef.format(dQtyMFGTBR);
										strQtyMFGRelease = doublef.format(dQtyMFGRelease);

										strQtyBUILTSET = GmCommonClass
												.parseZero((String) hmLoop.get("BUILD_SET_QTY"));
										strQtyDHRWIP = GmCommonClass.parseZero((String) hmLoop
												.get("DHR_WIP"));
										strQtyDHR_PEN_QC_INS_ = GmCommonClass
												.parseZero((String) hmLoop
														.get("DHR_PEN_QC_INS"));
										strQtyDHR_PEN_QC_REL = GmCommonClass
												.parseZero((String) hmLoop
														.get("DHR_PEN_QC_REL"));
										strQtyDHR_QC_APP = GmCommonClass
												.parseZero((String) hmLoop.get("DHR_QC_APPR"));
										strDHRAlloc = GmCommonClass.parseZero((String) hmLoop
												.get("DHRALLOC"));
										strQtyShipAlloc = GmCommonClass
												.parseZero((String) hmLoop.get("SHIPALLOC"));
										
										strIhAVailQty = GmCommonClass.parseZero((String)hmLoop.get("IH_AVAIL_QTY"));
										strIhAllocQty = GmCommonClass.parseZero((String)hmLoop.get("IH_ALLOC_QTY"));
										strIhSetQty = GmCommonClass.parseZero((String)hmLoop.get("IH_SET_QTY"));
										strIhItemQty = GmCommonClass.parseZero((String)hmLoop.get("IH_ITEM_QTY"));
										
										strLoanQty = GmCommonClass.parseZero((String)hmLoop.get("LOANQTY"));
						%>
						<tr>
							<td width="60" height="20" class="RightText">&nbsp;<%=strPartNum%></td>
							<td width="350" class="RightTextAS">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
							
							<% if(!strShowFG.equals("")){%>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInShelf%></td>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInShelfAlloc%></td>							
							<%} %>
							<% if(!strShowRW.equals("")){%>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInRW%>&nbsp;</td>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInRWAlloc%>&nbsp;</td>
							<%} %>
							<% if(!strShowRM.equals("")){%>
							<td width="50" class="RightTextAS" align="center"><%=strRawMatQty%>&nbsp;</td>
							<td width="50" class="RightTextAS" align="center"><%=strQtyRMAlloc%>&nbsp;</td>
							<%} %>
							<% if(!strShowBulk.equals("")){%>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInWIPSets%></td>
							<td width="50" class="RightTextAS" align="center"><%=strTBE%></td>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInBulk%></td>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInBulkAlloc%></td>
							<%} %>
							<% if(!strShowBS.equals("")){%>
							<td width="60" class="RightTextAS" align="center"><%=strQtyBUILTSET%>&nbsp;</td>
							<%}%>
							<% if(!strShowPN.equals("")){%>
							<td width="50" class="RightTextAS" align="center"><%=strRepackAvail%></td>
							<td width="50" class="RightTextAS" align="center"><%=strRepackAlloc%></td>
							<%} %>
							<% if(!strShowQN.equals("")){%>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInQuarantine%></td>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInQuarantineAlloc%></td>
							<%} %>
							<% if(!strShowRtn.equals("")){%>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInReturnsHold%></td>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInReturnsHoldAlloc%></td>
							<%} %>
							<% if(!strShowPO.equals("")){%>
							<td width="50" class="RightTextAS" align="center"><%=strQtyInPO%></td>
							<%} %>
							<% if(!strShowMFG.equals("")){%>
							<td width="60" class="RightTextAS" align="center"><%=strQtyMFGTBR%>&nbsp;</td>
							<td width="60" class="RightTextAS" align="center"><%=strQtyMFGRelease%>&nbsp;</td>
							<%}%>
							<% if(!strShowDHR.equals("")){%>
							<td width="60" class="RightTextAS" align="center"><%=strQtyDHRWIP%>&nbsp;</td>
							<td width="60" class="RightTextAS" align="center"><%=strQtyDHR_PEN_QC_INS_%>&nbsp;</td>
							<td width="60" class="RightTextAS" align="center"><%=strQtyDHR_PEN_QC_REL%>&nbsp;</td>
							<td width="60" class="RightTextAS" align="center"><%=strQtyDHR_QC_APP%>&nbsp;</td>
							<td width="60" class="RightTextAS" align="center"><%=strDHRAlloc%></td>
							<%}	%>							
							<% if(!strShowShip.equals("")){%>
							<td width="60" class="RightTextAS" align="center"><%=strQtyShipAlloc%>&nbsp;</td>
							<%}%>
							<% if(!strShowIH.equals("")){%>
							<td width="60" class="RightTextAS" align="center"><%=strIhAVailQty%>&nbsp;</td>	
							<td width="60" class="RightTextAS" align="center"><%=strIhAllocQty%>&nbsp;</td>
							<td width="60" class="RightTextAS" align="center"><%=strIhSetQty%>&nbsp;</td>
							<td width="60" class="RightTextAS" align="center"><%=strIhItemQty%></td>
							<%} %>
							<% if(!strShowLN.equals("")){%>
							<td width="60" class="RightTextAS" align="center"><%=strLoanQty%>&nbsp;</td>
							<%} %>
							<% if(!strShowIT.equals("")){%>
							<td width="60" class="RightText" align="right"><%=strQtyInIntransit%></td>
							<td width="60" class="RightText" align="right"><%=strQtyInIntransitAlloc%></td>							
							<%} %>							
							
						</tr>
						<tr>
							<td colspan="35" bgcolor="#eeeeee" height="1"></td>
						</tr>
						<%
							if (i == intBreak) {
											intBreak = intBreak + 45;
						%>
						<tr>
							<td colspan="35" height="1" bgcolor="#666666"></td>
						</tr>
						<tr>
							<td colspan="35" height="20" class="RightText" align="right"><fmtPartNumInvPrint:message key="LBL_CONT_ON_PAGE"/> <%=intBreak / 45%>-></td>
						</tr>
					</table>
					<p STYLE="page-break-after: always"></p> <span class="RightText"><fmtPartNumInvPrint:message key="LBL_PAGE"/> <%=intBreak / 45%></span> <%
 	out.print(sbStartSection);
 				} //end of IF
 			}
 		} else {
 %>
				
			<tr>
				<td height="30" colspan="35" align="center" class="RightTextBlue"><fmtPartNumInvPrint:message key="LBL_NO_PART_NUMBERS"/></td>
			</tr>
			<tr>
				<td colspan="35" height="1" bgcolor="#666666"></td>
			</tr>
			<%
				}
			%>
			
		</table>
	</FORM>
	<%
		} catch (Exception e) {
			e.printStackTrace();
		}
	%>
</BODY>

</HTML>
