 
 <!-- \operations\GmPOReport.jsp -->
 
 <%
/**********************************************************************************
 * File		 		: GmPOReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@page import="java.util.Date"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>



<!-- WEB-INF path corrected for JBOSS migration changes -->


<%@ taglib prefix="fmtPOReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPOReport:setLocale value="<%=strLocale%>"/>
<fmtPOReport:setBundle basename="properties.labels.operations.GmPOReport"/>






<% 
	


	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	// Based on application date format we need to pick date from calendar
	String strApplDateFmt = strGCompDateFmt;
	

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmParam = (HashMap)request.getAttribute("hmParam");
	
	String stroperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strhOpt = (String)request.getAttribute("hOpt");
	String strhAction = (String)request.getAttribute("hAction");
	strhAction = strhAction == null?"Load":strhAction;
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strShade = "";
	
	String strFromDt = GmCommonClass.parseNull((String)hmParam.get("FRMDT"));
	String strToDt = GmCommonClass.parseNull((String)hmParam.get("TODT"));
	
	Date dtfromdate=GmCommonClass.getStringToDate(strFromDt,strApplDateFmt);
	Date dttodate=GmCommonClass.getStringToDate(strToDt,strApplDateFmt);
		
	String strPOType = GmCommonClass.parseNull((String)hmParam.get("POTYPE"));
	String strPoAmt	= GmCommonClass.parseNull((String)hmParam.get("POAMT"));
	String strVendorId = GmCommonClass.parseNull((String)hmParam.get("VENDID"));
	String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
	String strPurchId = GmCommonClass.parseNull((String)hmParam.get("POID"));
	String strGridXmlData = (String)request.getAttribute("GRIDXMLDATA");
	String strPartNum = "";
	String strPartDesc = "";
	String strVendorNm = "";
	String strCostPrice = "";
	String strValidDate = "";
	String strQtyQuoted = "";

	String strPOId 		= "";
	String strPODate 	= "";
	String strPOTotal 	= "";
	String strReqDate 	= "";
	String strUserName 	= "";
	String strCallFlag 	= "";
	String strPOTypeNm = "";
	String strLatestComments = "";

	ArrayList alVendorList = new ArrayList();
	ArrayList alUsers = new ArrayList();
	ArrayList alTypes = new ArrayList();
	ArrayList alPOAction = new ArrayList();
	
	if (strhAction.equals("Load") || strhAction.equals("Go"))
	{
		alVendorList = (ArrayList)hmReturn.get("VENDORLIST");
		alUsers = (ArrayList)hmReturn.get("USERLIST");
		alTypes = (ArrayList)hmReturn.get("POTYPE");
		alPOAction = (ArrayList)hmReturn.get("POACTION");
	}

	int intSize = 0;
	HashMap hcboVal = null;
	double dbTotal = 0.0;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/OrderPlan.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=stroperationsJsPath%>/GmPOReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script>
var objGridData;
objGridData ='<%=strGridXmlData%>';
var gStrServletPath = "<%=strServletPath%>";
var gCmpDateFmt = "<%=strGCompDateFmt%>";
</script>


</HEAD>

<BODY leftmargin="20" topmargin="10" onload="javascript:fnOnPageLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPOReportServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">
<INPUT type="hidden" name="hPOString" value="">
<INPUT type="hidden" name="hWOString" value="">
<INPUT type="hidden" name="inputString" value="">
<input type="hidden" name="hPOId" value="">
<input type="hidden" name="hVenId" value="">
<input type="hidden" name="hType" value="<%=strPOType%>">
<input type="hidden" name="hMode" value="">
<input type="hidden" name="hTxnId" value="">
<input type="hidden" name="hCancelType" value="VDPPO">

<!-- Custom tag lib code modified for JBOSS migration changes -->
    <!-- width adjust for existing screen border issue(PC-3255) -->
	<table border="0" width="DtTable950" cellspacing="0" cellpadding="0">
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPOReport:message key="LBL_PO_REPORT"/></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td width="848" height="80" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr >
						<td class="RightTableCaption" align="right"  width="160" HEIGHT="30">&nbsp;<fmtPOReport:message key="LBL_VENDOR_NAME"/>:&nbsp;</td>
						<td>
						<gmjsp:dropdown controlName="hVendorId"  seletedValue="<%=strVendorId%>"  defaultValue= "[All Vendors]"	
							tabIndex="1"  value="<%=alVendorList%>" codeId="ID" codeName="NAME"/>
						</td>
						<td  class="RightTableCaption" align="right"  width="160">&nbsp;<fmtPOReport:message key="LBL_PO_ID"/>:&nbsp;</td>
						<td><input type="text" size="20" value="<%=strPurchId%>" name="Txt_POId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');fnRemoveFromDt(this.value);" tabindex=1>&nbsp;</td>
					 
					</tr>
					 
					
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr  class = "Shade" >
						<td class="RightTableCaption" align="right"  HEIGHT="24"   width="160">&nbsp;<fmtPOReport:message key="LBL_FROM_DATE"/>:&nbsp;
						</td>
						<td class="RightTableCaption">
						<gmjsp:calendar textControlName="Txt_FromDate" gmClass="InputArea" textValue="<%=dtfromdate==null?null:new java.sql.Date(dtfromdate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
						&nbsp;&nbsp;<fmtPOReport:message key="LBL_TO_DATE"/> 
						<gmjsp:calendar textControlName="Txt_ToDate" gmClass="InputArea" textValue="<%=dttodate==null?null:new java.sql.Date(dttodate.getTime())%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
						</td>
						<td  class="RightTableCaption" align="right"  width="160">&nbsp;<fmtPOReport:message key="LBL_TYPE_OF_PO"/>:&nbsp;</td>
						<td>
						<gmjsp:dropdown controlName="hPOType"  seletedValue="<%=strPOType%>" defaultValue= "[All Types]"	tabIndex="1"  value="<%=alTypes%>" codeId="CODEID" codeName="CODENM"/>
						</td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right"  HEIGHT="30"  width="160">&nbsp;<fmtPOReport:message key="LBL_EMP_NAME"/>:&nbsp;</td>
						<td>
						<gmjsp:dropdown controlName="Cbo_UserId"  seletedValue="<%=strUserId%>" defaultValue= "[All Employees]" tabIndex="1"   value="<%=alUsers%>" codeId="ID" codeName="NAME"/>
						</td>
						<td class="RightTableCaption" align="right"   width="200">&nbsp;<fmtPOReport:message key="LBL_PO_AMT"/>:&nbsp;</td>		
						<td><input type="text" size="10" value="<%=strPoAmt%>" name="Txt_POAmt" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;
						&nbsp;&nbsp;
						<fmtPOReport:message key="LBL_LOAD" var="varLoad"/>
						<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="fnGo();" tabindex="25" buttonType="Load" />
						</td>
					</tr>					
					<tr><td colspan="4" class="Line" height="1"></td></tr>
					<%
					ArrayList alList = (ArrayList)hmReturn.get("REPORT");
					intSize = alList.size(); 
					%> 
					<tr><td colspan="4" >
					<div id="dataGridDiv" style="" height="470px" width="940px"></div>
								<input type="hidden" name="hLen" value="<%=intSize%>">					
					</td></tr>	
					<tr>
        			 <td align="center" colspan="4">
          				<div class='exportlinks'><fmtPOReport:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
               					onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> <fmtPOReport:message key="LBL_EXCEL"/> </a></div>
           			 </td>
				   </tr>		
					<tr>
					<td colspan="4" class="RightText" align="Center"><BR><fmtPOReport:message key="LBL_CHOOSE_ACTION"/>:&nbsp;
						
						<gmjsp:dropdown controlName="Cbo_Action" seletedValue="<%=strPOType%>" defaultValue= "[Choose One]" tabIndex="2"   value="<%=alPOAction%>" codeId="CODEID" codeName="CODENM"/>
						<fmtPOReport:message key="LBL_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnCallEdit();" tabindex="25" buttonType="Save" />&nbsp;<BR><BR>
						</td>
					</tr>
					 
				</TABLE>
			</TD>
		</tr>
		<tr>
			<td colspan="4" height="1" bgcolor="#666666"></td>
		</tr>
    </table>

</FORM>

<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
