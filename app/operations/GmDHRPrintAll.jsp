
<%@ page language="java" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ include file="/common/GmHeader.inc" %>
<%
	if(!strSessCompanyLocale.equals("")){
		locale = new Locale("en", strSessCompanyLocale);
		strLocale = GmCommonClass.parseNull((String)locale.toString());
		strJSLocale = "_"+strLocale;
	}
%>


<%@ taglib prefix="fmtDHRPrintAll" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRPrintAll.jsp -->

<fmtDHRPrintAll:setLocale value="<%=strLocale%>"/>
<fmtDHRPrintAll:setBundle basename="properties.labels.operations.GmDHRPrintAll"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	log = GmLogger.getInstance(GmCommonConstants.CUSTOMERSERVICE);
	
	HashMap hmReturn = new HashMap();
	log.debug("hmReturn value is "+hmReturn);
	String strRuleSource="";
	String strShade = "";
	String strSource=(String)request.getAttribute("source");
	String strConsignId="";
	String strShipAdd="";
	String strDate="";
	String strConsignAdd="";
	String strUserName="";
	String strTrackno="";
	String strCarrier="";
	String strType="";
	String strInHousePurpose="";
	String strSetName="";
	String strRefId="";
	String strdetails="Transaction Details";
	String strConsignStatusFlag="";
	String strSetId="";
	String strCtype="";
	String strEtchId="";
	String strMode="";
	String strComment="";
	String strSlipHeader="Pick Slip";
	String strRuleId ="";
	String strId="";
	boolean flg =false;
	int j=0;
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConDetails = new HashMap();
	//HashMap hmSetLoad = new HashMap();
	int intPriceSize = 0;
    ArrayList alResult=new ArrayList();
        alResult=(ArrayList)request.getAttribute("ALRETURN");
        log.debug("alResult Size"+alResult.size());
     for(j=0;j<alResult.size();j++)
     {
        	hmReturn=(HashMap)alResult.get(j);
        	
	if (hmReturn != null)
	{
		     strRuleSource = (String)request.getAttribute("source"+j);
		    alSetLoad = (ArrayList)hmReturn.get("SETLOAD"+j);
			hmConDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("SETDETAILS"+j));
			strConsignId = GmCommonClass.parseNull((String)hmConDetails.get("CID"));
			strConsignAdd = GmCommonClass.parseNull((String)hmConDetails.get("ANAME"));
			strShipAdd = GmCommonClass.parseNull((String)hmConDetails.get("SHIPADD"));	
			strDate = GmCommonClass.parseNull((String)hmConDetails.get("CDATE"));
			strUserName = GmCommonClass.parseNull((String)hmConDetails.get("UNAME"));
			strTrackno = GmCommonClass.parseNull((String) hmConDetails.get("TRACKNO"));
			strCarrier= GmCommonClass.parseNull((String) hmConDetails.get("CARRIER"));
			strComment= GmCommonClass.parseNull((String) hmConDetails.get("COMMENTS"));
			strMode= GmCommonClass.parseNull((String) hmConDetails.get("SHIPMODE"));
			strConsignStatusFlag = GmCommonClass.parseNull((String)hmConDetails.get("SFL"));
			strSetId = GmCommonClass.parseNull((String)hmConDetails.get("SETID"));
			strSetName= GmCommonClass.parseNull((String)hmConDetails.get("SETNM"));
			strRefId = GmCommonClass.parseNull((String)hmConDetails.get("REFID"));
			strEtchId = GmCommonClass.parseNull((String)hmConDetails.get("ETCHID"));
			strType = GmCommonClass.parseNull((String)hmConDetails.get("TYPE"));
			strCtype = GmCommonClass.parseNull((String)hmConDetails.get("CTYPE"));
			strInHousePurpose = (String)hmConDetails.get("PURP");
		}

	
	int intSize = 0;
	HashMap hcboVal = null;
	
	StringBuffer sbStartSection = new StringBuffer();
	sbStartSection.append("<tr><td colspan=2>");
	sbStartSection.append("<table border=0 width=100% cellspacing=0 cellpadding=0>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
	sbStartSection.append("<td height=20 align=center width=250>&nbsp;Consigned To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=250 align=center>&nbsp;Ship To</td>");
	sbStartSection.append("<td bgcolor=#666666 width=1 rowspan=11></td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Date</td>");
	sbStartSection.append("<td width=100 align=center>&nbsp;Transaction #</td>");
	sbStartSection.append("</tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
	sbStartSection.append("<tr><td class=RightText rowspan=5 valign=top>&nbsp;");
	sbStartSection.append(strConsignAdd);
	sbStartSection.append("</td><td rowspan=5 class=RightText valign=top>&nbsp;");
	sbStartSection.append(strShipAdd);
	sbStartSection.append("</td>");
	sbStartSection.append("<td height=25 class=RightText align=center>&nbsp;");
	sbStartSection.append(strDate);
	sbStartSection.append("</td><td class=RightText align=center>&nbsp;");
	sbStartSection.append(strConsignId);
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption><td align=center>Ref ID</td><td align=center>Initiated By</td></tr>");
	sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=2></td></tr>");
	sbStartSection.append("<tr><td align=center class=RightText>&nbsp;");sbStartSection.append(strRefId);
	sbStartSection.append("<Br></td><td align=center height=25 nowrap class=RightText>&nbsp;");
	sbStartSection.append(strUserName);
	/*if (flg == true){
		sbStartSection.append("<tr bgcolor=#eeeeee class=RightTableCaption>");
		sbStartSection.append("<td  width=250 height=20 align=center >&nbsp;Carrier </td>");
		sbStartSection.append("<td width=250 height=20 align=center >&nbsp;Mode</td>");
		sbStartSection.append("<td width=250 colspan=3 height=20 align=center >&nbsp;Tracking No</td>");		
		sbStartSection.append("<tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");
		
		sbStartSection.append("<tr> <td  height=20 align=center >&nbsp;");		
		sbStartSection.append(strCarrier);
		sbStartSection.append("</td>");
		sbStartSection.append("<td  height=20 align=center >&nbsp;");
		sbStartSection.append(strMode);
		sbStartSection.append("</td>");
		sbStartSection.append("<td colspan=3 height=20 align=center >&nbsp");
		sbStartSection.append(strTrackno);
		sbStartSection.append("</td>");		
		sbStartSection.append("<tr><td  bgcolor=#666666 height=1 colspan=6></td></tr>");
		
	}*/
	
	sbStartSection.append("</td></tr><tr><td bgcolor=#666666 height=1 colspan=6></td></tr>");	
	sbStartSection.append("</table></td></tr>");
	strId = strConsignId+"$"+strSource;
	strRuleId = strConsignId + "$" + strRuleSource;
	  //log.debug("strId"+j+"==>"+strId+"strRuleId==>"+strRuleId);
%>

<HTML>
<HEAD>
<TITLE> GlobusOne Enterprise Portal: Pick Slip </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>

function fnPrint()
{

	window.print();

}
var tdinnner = "";
function hidePrint()
{
	strObject = document.getElementById("button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
	strObject = document.getElementById("button1");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = document.getElementById("button");
	strObject.innerHTML = tdinnner ;
	strObject = document.getElementById("button1");
	strObject.innerHTML = tdinnner ;
}

</script>
</HEAD>
 
<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmDHR">
<%if(j==0){%>
	<div id="button">
	  <table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td align="right" height="30" id="button">
			<fmtDHRPrintAll:message key="IMG_ALT_PRINT" var = "varPrint"/>
				<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="${varPrint}" onClick="fnPrint();" />
				<fmtDHRPrintAll:message key="IMG_ALT_CLOSE" var = "varClose"/>
				<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="${varClose}" onClick="window.close();" />
			</td>
	   <tr>
	</table>
	</div>
	<%
	} %>
	
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td height="80" width="170"><img src="<%=strImagePath%>/s_logo.gif" width="160" height="60"></td>
						<td align="left" class="RightText"><font size="+3"><%=strSlipHeader%></font>&nbsp;</td>
						<td rowspan="2">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td height="30"><img src='/GmCommonBarCodeServlet?ID=<%=strId%>' height="20" width="220" /></td>
									<td>&nbsp;<fmtDHRPrintAll:message key="LBL_FEDEX_MODULE"/></td>
								</tr>
								<tr>
									<td height="30" align="right"><img src='/GmCommonBarCodeServlet?ID=<%=strConsignId%>' height="20" width="170" /></td>
									<td>&nbsp;<fmtDHRPrintAll:message key="LBL_PAPERWORK"/></td>
								</tr>
								<tr>
									<td height="30" align="right"><img src='/GmCommonBarCodeServlet?ID=<%=strRuleId%>&type=2d' height="40"	width="40" /></td>
									<td>&nbsp;<fmtDHRPrintAll:message key="LBL_MOTROLA_DEVICE"/></td>
								</tr>
							</table>			
						</td>					
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" cellspacing="0" cellpadding="0">
<!--StartSection starts here-->
<%out.print(sbStartSection);%>
<!--StartSection ends here-->				
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" colspan = "2" HEIGHT="20"><%=strdetails%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr>
							<td colspan="2" class="RightText">
								<table border="0" width="100%" cellspacing="0" cellpadding="0">
									<tr height="20">
										<td align="right" class="RightTableCaption"><fmtDHRPrintAll:message key="LBL_TYPE"/></td>
										<td>:&nbsp;<%=strType%></td>
										<td align="right" class="RightTableCaption"><fmtDHRPrintAll:message key="LBL_REASON"/></td>
										<td colspan="3">:&nbsp;<%=strInHousePurpose%></td>
									</tr>
<%
			if (!strSetId.equals(""))
			{
%><%-- 
									<tr><td colspan="6" height="1" class="LLine"></td></tr>
									<tr height="20">
										<td align="right" class="RightTableCaption">Set</td>
										<td>:&nbsp;<%=strSetId%>&nbsp;-&nbsp;<%=strSetName%></td>
										<td align="right" class="RightTableCaption">CN ID</td>
										<td>:&nbsp;<%=strRefId%></td>
										<td align="right" class="RightTableCaption">Etch ID</td>
										<td>:&nbsp;<%=strEtchId%></td>
									</tr>--%>
<%
			}
%>
								</table>
							</td>
					</tr>
					<tr>
						<td colspan="2"  height="300" valign="top">
							<table cellspacing="0" cellpadding="0" border="0" id="myTable">
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
								<tr bgcolor="#eeeeee" class="RightTableCaption">
									<td height="25"><fmtDHRPrintAll:message key="LBL_PART_NUMBER"/></td>
									<td width="200"><fmtDHRPrintAll:message key="LBL_DESCRIPTION"/></td>
									<td><fmtDHRPrintAll:message key="LBL_REQ_QTY"/></td>
									<td width="200">&nbsp;&nbsp;&nbsp;&nbsp;<fmtDHRPrintAll:message key="LBL_QTY"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtDHRPrintAll:message key="LBL_CONTROL_NUMBER"/></td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							hcboVal = new HashMap();
							for (int i=0;i<intSize;i++)
							{
								hcboVal = (HashMap)alSetLoad.get(i);
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
%>
								<tr <%=strShade%>>
									<td class="RightText" height="20">&nbsp;<%=(String)hcboVal.get("PNUM")%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM((String)hcboVal.get("PDESC"))%></td>
									<td align="center" class="RightText"><%=(String)hcboVal.get("IQTY")%></td>
									<td class="RightText">&nbsp;&nbsp;&nbsp;_ _ _ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=(String)hcboVal.get("CNUM")%>
									</td>
								</tr>
								<tr><td colspan="4" height="1" bgcolor="#eeeeee"></td></tr>
<%
							}
						}else {
%>
						<tr><td colspan="4" height="50" align="center" class="RightTextRed"><BR><fmtDHRPrintAll:message key="MSG_NO_DATA"/>!</td></tr>
<%
						}		
%>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="1"></td></tr>
					<tr>
						<td bgcolor="#eeeeee" colspan="2">
							<jsp:include page="/common/GmRuleDisplayInclude.jsp">
							<jsp:param name="Show" value="false" />
							<jsp:param name="Fonts" value="false" />
							</jsp:include>
						</td>
					</tr>
				
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
					<tr valign="top" height="60">
						<td colspan="2" class="RightText"><b><fmtDHRPrintAll:message key="LBL_COMMENTS"/>:</b><BR>&nbsp;<%=strComment%></td>
					</tr>
					<%-- 
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>					
					<tr><td class="RightText" colspan="2"><u>Please Initial here:</u></td></tr>					
					<tr><td colspan="2" height="50">&nbsp;</td></tr>
					<tr>
						<td class="RightText">&nbsp;&nbsp;&nbsp;Initiated/Requested By</td>
						<td class="RightText" align="right">Verified By&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
				--%>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1"></td></tr>		
    </table>
	<table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
	
</FORM>
<%if(j!=alResult.size()-1) {%>
<p><br style="page-break-before: always;" clear="all" /></p>

<%
}
 }
%>
<div id="button1">
     <table border="0" class="DtRuleTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" height="30" id="button">
				<fmtDHRPrintAll:message key="IMG_ALT_PRINT" var = "varPrint"/>
				<img style="cursor:hand" src='<%=strImagePath%>/print-icon.png' height="25" width="25" alt="${varPrint}" onClick="fnPrint();" />
				<fmtDHRPrintAll:message key="IMG_ALT_CLOSE" var = "varClose"/>
				<img style="cursor:hand" src='<%=strImagePath%>/close_icon.png' height="20" width="20" alt="${varClose}" onClick="window.close();" />
			</td>
	   <tr>
	</table>
	</div>
 <% 
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>