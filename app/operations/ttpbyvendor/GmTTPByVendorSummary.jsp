
<%
	/**********************************************************************************
	 * File		 		: GmTTPByVendorSummary.jsp
	 * Desc		 		: TTP By Vendor Summary Report
	 * Version	 		: 1.0
	 * author			: Tamizhthangam Ramasamy
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- app\operations\ttpbyvendor\GmTTPByVendorSummary.jsp -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTTPByVenRpt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtTTPByVenRpt:setLocale value="<%=strLocale%>" />
<fmtTTPByVenRpt:setBundle basename="properties.labels.operations.forecast.GmTTPByVendorSummary" />

<bean:define id="ttpYear" name="frmTTPByVendorRpt" property="ttpYear" type="java.lang.String"></bean:define>
<bean:define id="ttpMonth" name="frmTTPByVendorRpt" property="ttpMonth"type="java.lang.String"></bean:define>
<bean:define id="ttpId" name="frmTTPByVendorRpt" property="ttpId" type="java.lang.String"></bean:define>
<bean:define id="ttpName" name="frmTTPByVendorRpt" property="ttpName" type="java.lang.String"></bean:define>
<bean:define id="vendorId" name="frmTTPByVendorRpt" property="vendorId" type="java.lang.String"></bean:define>
<bean:define id="vendorName" name="frmTTPByVendorRpt" property="vendorName" type="java.lang.String"></bean:define>
<bean:define id="saveBtnAccessFl" name="frmTTPByVendorRpt" property="saveBtnAccessFl" type="java.lang.String"></bean:define>
<bean:define id="approveBtnAccessFl" name="frmTTPByVendorRpt" property="approveBtnAccessFl" type="java.lang.String"></bean:define>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("TTP_BY_VENDOR_SUMMARY_DTL_RPT"));
	String strCollapseImage = strImagePath + "/plus.gif";
%>
<html>
<head>
<title>Globus Medical: TTP By Vendor Summary Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmTTPByVendorSummary.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script type="text/javascript" src="https://erp.spineit.net/GlobusCRM/lib/fusionchart/fusioncharts.js"></script>  
<script type="text/javascript">
var saveBtnAccessFl ='<%=saveBtnAccessFl%>';
var approveBtnAccessFl = '<%=approveBtnAccessFl%>';
</script>
</head>
<body leftmargin="20" topmargin="10"  onload="fnOnpageLoad();" onkeyup="fnEnter();">
	<html:form action="/gmTTPByVendorSummary.do">
		<table border="0" class="DtTable1500" cellspacing="0" cellpadding="0">
			<tr height="25">
				<td colspan="7" class="RightDashBoardHeader"><fmtTTPByVenRpt:message key="LBL_TTP_BY_VENDOR_RPT_HEADER" /></td>
				<td colspan="1" class="RightDashBoardHeader" align="right" width="20"><fmtTTPByVenRpt:message key="IMG_HELP" var="varHelp" />
				 <img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr height="30">
				<td width="100" class="RightTableCaption" align="right" style="padding-right: 2px;">&nbsp;<font color="red">*&nbsp;</font><fmtTTPByVenRpt:message
						key="LBL_TTP_NAME" />:
				</td>
				<td width="100" style="padding-left: 2px;"><jsp:include
						page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="ttpId" />
						<jsp:param name="METHOD_LOAD" value="loadTTPList&STROPT=GOP" />
						<jsp:param name="WIDTH" value="240" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="1" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=ttpName%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="<%=ttpId%>" />
						<jsp:param name="SHOW_DATA" value="50" />
					</jsp:include></td>
				<td width="100" class="RightTableCaption" align="right" HEIGHT="25">&nbsp;<font color="red">*&nbsp;</font><fmtTTPByVenRpt:message
						key="LBL_VENDOR" />:
				</td>
				<td width="100" style="padding-left: 2px;"><jsp:include
						page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="vendorId" />
						<jsp:param name="METHOD_LOAD"
							value="loadVendorNameList&searchType=LikeSearch" />
						<jsp:param name="WIDTH" value="300" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="2" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=vendorName%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="<%=vendorId%>" />
						<jsp:param name="SHOW_DATA" value="50" />
						<jsp:param name="AUTO_RELOAD" value="" /></jsp:include></td>
				<td class="RightTableCaption" width="30" colspan="2">&nbsp;
					&nbsp;<fmtTTPByVenRpt:message key="LBL_MONTH" var="varMonth" /> <gmjsp:label
						type="MandatoryText" SFLblControlName="${varMonth}:" td="false" />
					<gmjsp:dropdown controlName="ttpMonth"
						SFFormName='frmTTPByVendorRpt' SFSeletedValue="ttpMonth"
						SFValue="alMonth" width="105" codeId="CODENMALT" codeName="CODENM"
						defaultValue="" tabIndex="3" />&nbsp;&nbsp; <fmtTTPByVenRpt:message
						key="LBL_YEAR" var="varYear" />
					<gmjsp:label type="MandatoryText" SFLblControlName="${varYear}:"
						td="false" /> <gmjsp:dropdown controlName="ttpYear"
						SFFormName='frmTTPByVendorRpt' SFSeletedValue="ttpYear"
						SFValue="alYear" width="105" codeId="CODENM" codeName="CODENM"
						defaultValue="" tabIndex="4" />
				</td>

				<td class="RightTableCaption" width="135" colspan="2" align="left" style="white-space: nowrap;">
					<fmtTTPByVenRpt:message key="LBL_HISTOR_DATA_BY" />: <input
					type="radio" name="historicalData" value="receipt" checked
					tabindex="5" />
				<fmtTTPByVenRpt:message key="LBL_RECEIPT" /> <input type="radio"
					name="historicalData" value="poQty" tabindex="6" />
				<fmtTTPByVenRpt:message key="LBL_PO_QTY" />&nbsp;&nbsp; <gmjsp:button
						name="BTN_LOAD" value="Load" gmClass="button"
						onClick="fnSummaryLoad();" buttonType="Load" tabindex="7" />
				</td>
			</tr>
			<!-- Success Message TR -->
				<tr id="succesMsg" style="display: none;">
						<td align="center" colspan="8">&nbsp;<font style="color:Green;" size="2"><b><fmtTTPByVenRpt:message key="LBL_DATA_SAVED_SUCCESSFULLY"/></b></font></td>
				</tr>
			<!-- Rolled Back Message TR -->
				<tr id="rolledBackMsg" style="display: none;">
						<td align="center" colspan="8">&nbsp;<font style="color:Green;" size="2"><b><fmtTTPByVenRpt:message key="LBL_DATA_ROLLED_BACK_SUCCESSFULLY"/></b></font></td>
				</tr>	
			<tr>
				<td colspan="8"><div id="dataGridDiv" style="width: 100%;display: none"></div></td>
			</tr>
        <!-- Expand Chart Div -->
			<tr class="ShadeRightTableCaption">
					<td colspan="8" >
						<a id="Expandchart" href="javascript:fnExpandChart('tabChart');" tabindex="8"
							style="text-decoration: none; color: black;display: none;"
							title="Click to toggle the Group Details"> <IMG
							id="chartDivimg" border=0 src="<%=strCollapseImage%>">&nbsp;<fmtTTPByVenRpt:message
								key="LBL_EXPAND_CHART" /></a><div id="chartHdrDiv" style="display: none;">
					</div></td> 

			</tr>
			<tr>
				<td class="LLine" colspan="8" height="1"></td>
			</tr>
			<tr>
				<td colspan="8"><div id="chartDiv" style="display: none; height: 25px;"></div></td>
			</tr>
			<tr>
				<td colspan="8" align="center">
					<div class='exportlinks' id="DivExportExcel" style="display: none;">
						<fmtTTPByVenRpt:message key="DIV_EXPORT_OPT" />
						:<img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnExport();" tabindex="9"> <fmtTTPByVenRpt:message
								key="DIV_EXCEL" />
						</a>
					</div>
				</td>
			</tr>

			<tr>
				<td class="LLine" colspan="8" height="1"></td>
			</tr>
			<tr >
				<td colspan="8" align="center" class="RegularText"><div id="DivNothingMessage" style="display: none;">
						<fmtTTPByVenRpt:message key="NO_DATA_FOUND" />
					</div></td>
			</tr>
			<tr >
				<td colspan="8" align="center"><div id="DivApprove" style="display: none;">
						<gmjsp:button value="Chart By Vendor" name="BTN_CHART_BY_VENDOR" gmClass="button"
							buttonType="Load" onClick="fnLoadChartByVendor();" tabindex="10"/>
						<gmjsp:button value="Save" name="BTN_SAVE" gmClass="button"
							buttonType="Save" onClick="fnSubmitSummary();" tabindex="11"/>
						<gmjsp:button value="Approve" name="BTN_APPROVE" gmClass="button"
							buttonType="Save" onClick="fnApproval();" tabindex="12" />
						<gmjsp:button value="RollBack" name="BTN_ROLL_BACK" gmClass="button"
							buttonType="Save" onClick="fnRollback();" tabindex="13" />	
					</div></td>
			</tr>
			
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>

</html>