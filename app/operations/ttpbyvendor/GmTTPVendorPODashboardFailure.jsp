<%
/**********************************************************************************
 * File		 		: GmTTPVendorPODashboardFailure.jsp
 * Desc		 		: To show PO Generation Failed status hyperlink Report
 * Version	 		: 1.0
 * author			: Prabhu vigneshwaran M D 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@page import="java.net.URLEncoder"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTTPVendorPOFailure" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtTTPVendorPOFailure:setLocale value="<%=strLocale%>"/>
<fmtTTPVendorPOFailure:setBundle basename="properties.labels.operations.ttpbyvendor.GmTTPVendorPOPopUpFailure" />
<!-- operations\ttpbyvendor\GmTTPVendorPODashboardFailure.jsp -->
 <bean:define id="capaDtlsId" name="frmTTPVendorPODashboard" property="capaDtlsId" type="java.lang.String"> </bean:define>
 <% 
 String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_TTPVENDOR");
 String strWikiTitle = GmCommonClass.parseNull((String) GmCommonClass.getWikiTitle("TTP_VENDOR_PO_FAILURE_RPT"));					
%>
<HTML>
<head>
<title>Globus Medical:PO Generation Failed status hyperlink Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<style type="text/css" media="all"> @import url("<%=strCssPath%>/screen.css");</style>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmTTPVendorPODashboardFailure.js"></script>
<script>
var capaDtlsId =  '<%=capaDtlsId%>';
</script>
</head>
<BODY leftmargin="20" topmargin="10"  onload = "fnOnPageLoad()">
<html:form action="/gmTTPVendorPOPopUp.do?method=fetchVendorPOFailure">
	<table border="0"  height="60" class="DtTable950" cellspacing="0" cellpadding="0" style="table-layout:fixed;">
	    <tr>
			<td height="25" class="RightDashBoardHeader" colspan="2"><fmtTTPVendorPOFailure:message key="LBL_TTP_VENDOR_PO_FAILURE"/></td>
			<td align="right" colspan="2" class=RightDashBoardHeader ><fmtTTPVendorPOFailure:message key="IMG_ALT_HELP" var = "varHelp"/> 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
		</tr>
		<tr height="25">
		<td class="RightTableCaption" align="right">
		<fmtTTPVendorPOFailure:message key="LBL_VENDOR_NAME" var="varVendorName"/><gmjsp:label type="RegularText" SFLblControlName="${varVendorName}" td="false" />:&nbsp;
		</td>
		<td id="lbl_vendor_nm" align="left" colspan="3">
		</td>
		</tr>
		<tr>
		<td class="LLine" height="1" colspan="4"></td>
		</tr>
		<tr height="25" class="Shade">
		<td class="RightTableCaption" align="right" >
		<fmtTTPVendorPOFailure:message key="LBL_LOAD_DATE" var="varLoadDt"/><gmjsp:label type="RegularText" SFLblControlName="${varLoadDt}" td="false" />:&nbsp;
		</td>
		<td id="lbl_load_date" align="left" height="25">
		</td>
		<td></td>
		<td></td>
		</tr>
	     <tr>
		 <td class="LLine" height="1" colspan="4"></td>
		</tr>
		<tr height="25">
		<td class="RightTableCaption" colspan="4">
		<fmtTTPVendorPOFailure:message key="LBL_FAILURE_REASON" var="varFailureReason"/><gmjsp:label type="RegularText" SFLblControlName="${varFailureReason}" td="false" />:&nbsp;
		</td>
		</tr>
		<tr>
		<td class="LLine" height="1" colspan="4"></td>
		</tr>
		<tr>
		<td  colspan="4" align="left" id="lbl_Failure_Reason" style="word-wrap: break-word;">
		</td>
		</tr>
	   <tr>
		<td class="LLine" height="1" colspan="4"></td>
		</tr>
		<tr>
		<tr height="30">
		<td colspan="4" align="center">
		   <div id="DivClose" ><gmjsp:button value="Close" name="Btn_Close" gmClass="button" buttonType="Load" onClick="fnClose();" style="width: 5em"/></div>
		</td>
		</tr>
	
	</table>
</html:form> 
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
