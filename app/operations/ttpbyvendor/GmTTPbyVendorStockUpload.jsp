<%
	/*******************************************************************
	 * File		 		: GmTTPbyVendorStockUpload.jsp
	 * Desc		 		: This JSP is used to upload safety stock data
	 * Version	 		: 1.0
	 * author			: Agilan Singaravel
	 ********************************************************************/
%>

<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*, java.net.URLEncoder"%>

<%@ taglib prefix="fmtTTPbyVendorStockUpload" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>

<!-- Operations/ttpbyvendor/GmTTPbyVendorStockUpload.jsp-->
<fmtTTPbyVendorStockUpload:setLocale value="<%=strLocale%>" />
<fmtTTPbyVendorStockUpload:setBundle basename="properties.labels.operations.ttpbyvendor.GmTTPbyVendorStockUpload" />

<%
	String strOperationsJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("SAFETY_STOCK_UPLOAD"));
	String strBtnDisable ="true";
	String strApplDateFmt = strGCompDateFmt;
	 //The following code added for passing the company info
    String strCompanyInfo ="{\"cmpid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getCmpid())+"\",\"partyid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPartyid())+"\",\"plantid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPlantid())+"\"}";
    strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<html>
<head>
<title>Globus Medical: Safety Stock Upload</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">

<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script> 
 
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmTTPbyVendorStockUpload.js"></script>

<script>
var objGridData;
objGridData ='';
var gCmpDateFmt = '<%=strApplDateFmt%>';
var strCompanyInfo = '<%=strCompanyInfo%>';
</script>

<style type="text/css">
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;;
}
		
div.gridbox table.hdr td {
   color: #000000 !important;
   background-color: #d2e9fc !important;
}

div.gridbox_box table.hdr {
background-color:#dcedfc !important;
}

div.gridbox_material.gridbox .xhdr {
	background:#dcedfc !important;
}
</style>

</head>
<body leftmargin="20" topmargin="10"  onload="fnOnPageLoad();">
	<html:form action="/gmSafetyStockUpload.do?">
		<html:hidden property="inputStr" value="" />
		
		<table border="0" class="DtTable600" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" width="90%">&nbsp;<fmtTTPbyVendorStockUpload:message key="LBL_SAFETY_STOCK_UPD"/></td>
			<td  align="right" class="RightDashBoardHeader" width="10%">
				<fmtTTPbyVendorStockUpload:message key="IMG_ALT_HELP" var="varHelp"/>
				<img align="right" id='imgEdit' style='cursor:pointer' src='<%=strImagePath%>/help.gif' title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>
			</td>
		</tr>		
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>	
		<tr height="30" class="Shade">
			<td class="RightTableCaption" align="left" height="30" colspan="2">
				<div id="rfs_grid" style="display:inline" >
					<table cellpadding="1" width="100%" cellspacing="1" border="0">
						<tr>
							<td class="RightTableCaption">
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="copy" style="border: none;" height="14"></a>
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="Add Row" style="border: none;" onClick="javascript:fnAddRows()"height="14"></a>
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="Remove Row" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
								&nbsp;<a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="paste" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a>
							</td>									
						</tr>
						<tr class="Shade">
							<td height="17"><font color="#800000">&nbsp;<fmtTTPbyVendorStockUpload:message key="LBL_MAXIMUM_ROWS_TO_PASTE"/></font></td>
						</tr>
						<tr class="Shade" id="datasuc" style="display:none;">
							<td><font color="green" style="padding-left: 220px;">&nbsp;<fmtTTPbyVendorStockUpload:message key="LBL_DATA_SUCCESS"/></font></td>
						</tr>
					</table>
				</div>
			</td>									
		</tr>		
		<tr><td colspan="2" ><div id="dataGridDiv"  style="height:450px;"></div></td></tr>		
		  <tr style="display: table-row" id="buttonshow" height="30" class="Shade"><td colspan="2"  align="center">
          <div id="buttonDiv">            
	            <fmtTTPbyVendorStockUpload:message key="BTN_SAVE" var="varSave"/>
	            <gmjsp:button value="${varSave}" controlId="BTN_SAVE" gmClass="button"  name="BTN_SAVE" onClick="fnSave();"  buttonType="Save"/>			
		  </div>
			</td>
		</tr> 
		</table>
		<%@ include file="/common/GmFooter.inc"%>
	</html:form>
</body>
</html>