
<%
	/**********************************************************************************
	 * File		 		: GmTTPVendorPODashboard.jsp
	 * Desc		 		: Vendor Dashboard PO Report
	 * Version	 		: 1.0
	 * author			: T.S. Ramachandiran
	 ************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- app\operations\ttpbyvendor\GmTTPVendorPODashboard.jsp -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="fmtTTPVenPODashboard"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmtTTPVenPODashboard:setLocale value="<%=strLocale%>" />
<fmtTTPVenPODashboard:setBundle
	basename="properties.labels.operations.ttpbyvendor.GmTTPVendorPODashboard" />


<bean:define id="vendorId" name="frmTTPVendorPODashboard"
	property="vendorId" type="java.lang.String"></bean:define>
<bean:define id="vendorName" name="frmTTPVendorPODashboard"
	property="vendorName" type="java.lang.String"></bean:define>
<bean:define id="strSaveBtnAccessFl" name="frmTTPVendorPODashboard"
	property="strSaveBtnAccessFl" type="java.lang.String"></bean:define>
<bean:define id="strGeneratePOBtnAccessFl"
	name="frmTTPVendorPODashboard" property="strGeneratePOBtnAccessFl"
	type="java.lang.String"></bean:define>
<bean:define id="strPOHtml"
	name="frmTTPVendorPODashboard" property="strPOHtml"
	type="java.lang.String"></bean:define>	

<%
	String strOperationsJsPath = GmFilePathConfigurationBean
			.getFilePathConfig("JS_OPERATIONS_TTPVENDOR");
	String strWikiTitle = GmCommonClass
			.parseNull((String) GmCommonClass
					.getWikiTitle("TTP_VENDOR_PO_DASHBOARD"));
%>

<html>
<head>
<title>Globus Medical: TTP Vendor PO Dashboard</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript"
	src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript"
	src="<%=strOperationsJsPath%>/GmTTPVendorPODashboard.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script type="text/javascript">
var saveBtnAccessFl ='<%=strSaveBtnAccessFl%>';
var generateBtnAccessFl = '<%=strGeneratePOBtnAccessFl%>';
// Create Array Object and initialize array values for PO dropdown
var POArr = new Array(1);
<%=strPOHtml%>;
</script>
</head>
<body leftmargin="20" topmargin="10">
	<html:form action="/gmTTPVendorPODashboard.do?method=loadVendorPODashboard">
		<input type="hidden" name="hPrimaryKey" value="" />
		<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" colspan="2" class="RightDashBoardHeader"><fmtTTPVenPODashboard:message
						key="LBL_PO_GENERATION_BY_VENDOR_HEADER" /></td>
				<td colspan="2" class="RightDashBoardHeader" align="right"
					width="20"><fmtTTPVenPODashboard:message key="IMG_HELP"
						var="varHelp" /> <img id='imgEdit' style='cursor: hand'
					src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td width="100" class="RightTableCaption" HEIGHT="25" align="right"><fmtTTPVenPODashboard:message
						key="LBL_VENDOR_NAME" var="varName" /> <gmjsp:label
						type="RegularText" SFLblControlName="${varName}:" td="false" /></td>
				<td style="padding-left: 2px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="vendorId" />
						<jsp:param name="METHOD_LOAD"
							value="loadVendorNameList&searchType=LikeSearch" />
						<jsp:param name="WIDTH" value="300" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="2" />
						<jsp:param name="CONTROL_NM_VALUE" value="<%=vendorName%>" />
						<jsp:param name="CONTROL_ID_VALUE" value="<%=vendorId%>" />
						<jsp:param name="SHOW_DATA" value="50" />
						<jsp:param name="AUTO_RELOAD" value="" />
					</jsp:include></td>

				<td class="RightTableCaption" align="right" HEIGHT="25"><fmtTTPVenPODashboard:message
						key="LBL_STATUS" />:</td>
				<td>&nbsp;<gmjsp:dropdown controlName="statusId"
						SFFormName="frmTTPVendorPODashboard" SFSeletedValue="statusId"
						SFValue="alStatus" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" tabIndex="3" width="180" />
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="4"></td>
			</tr>
			<tr class="oddshade" height="30">

				<td class="RightTableCaption" align="right" HEIGHT="30">&nbsp;<fmtTTPVenPODashboard:message
						key="LBL_PERIOD" var="varPeriod" /> <gmjsp:label
						type="MandatoryText" SFLblControlName="${varPeriod}:" td="false" /></td>
				<td style="padding-left: 2px;"><gmjsp:dropdown controlName="monthId"
						SFFormName="frmTTPVendorPODashboard" SFSeletedValue="monthId"
						SFValue="alMonth" codeId="CODENMALT" codeName="CODENM"
						defaultValue="" onChange="fnEnableDisableBtn();" tabIndex="4" width="105" /> &nbsp;<gmjsp:dropdown
						controlName="yearId" SFFormName="frmTTPVendorPODashboard"
						SFSeletedValue="yearId" SFValue="alYear" codeId="CODENM"
						codeName="CODENM" defaultValue="" onChange="fnEnableDisableBtn();" tabIndex="5" width="105" /></td>
				<td colspan="2" align="center"><gmjsp:button name="BTN_LOAD"
						value="Load" gmClass="button" onClick="fnPODashboardLoad();"
						buttonType="Load" tabindex="6" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
            <tr>
            <td colspan="4" align="center"><div style="display: none;" id="progress">
				<img src="<%=strImagePath%>/success_y.gif" height="15"></div>
				<div id="poSuccessMsg"  style="color:green;font-weight:bold;display: none;"></div>
			</td>
			</tr>
			
			<tr>
				<td colspan="4"><div id="dataGridDiv" class="" height="400px"></div></td>
			</tr>
			<tr style="display: none" id="DivExportExcel" height="25">
				<td colspan="4" align="center">
					<div class='exportlinks'>
						<fmtTTPVenPODashboard:message key="DIV_EXPORT_OPT" />
						: <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
							onclick="fnExport();"><fmtTTPVenPODashboard:message
								key="DIV_EXCEL" /> </a>
					</div>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="4" height="1"></td>
			</tr>
			<tr style="display: none" id="DivButton" height="40">
				<td colspan="4" align="center" class="RegularText"><logic:equal
						name="frmTTPVendorPODashboard" property="strGeneratePOBtnAccessFl"
						value="Y">
						<gmjsp:button value="Generate PO" name="BTN_GENERATE"
							gmClass="button" buttonType="save" onClick="fnPoGenerate()" tabindex="7" />&nbsp;
				</logic:equal></td>
			</tr>
			<tr>
				<td colspan="4" align="center" class="RegularText" id="DivData"
					HEIGHT="30"><div id="DivNothingMessage">
						<fmtTTPVenPODashboard:message key="NO_DATA_FOUND" />
					</div></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>