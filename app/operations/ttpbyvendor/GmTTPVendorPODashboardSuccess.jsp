<%
/**********************************************************************************
 * File		 		: GGmTTPVendorPODashboardSuccess.jsp
 * Desc		 		: To show PO Generation Success status hyperlink Report
 * Version	 		: 1.0
 * author			: Tamizhthangam Ramasamy 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTTPPODashPopup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtTTPPODashPopup:setLocale value="<%=strLocale%>"/>
<fmtTTPPODashPopup:setBundle basename="properties.labels.operations.ttpbyvendor.GmTTPVendorPODashPopUpSuccess"/>
<bean:define id="capaDtlsId" name="frmTTPVendorPODashboard" property="capaDtlsId" type="java.lang.String"> </bean:define>
<bean:define id="vendorId" name="frmTTPVendorPODashboard" property="vendorId" type="java.lang.String"> </bean:define>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_TTPVENDOR");
String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("TTP_VENDOR_PO_SUCCESS_RPT"));
%>
<html>
<head>
<title>PO Generated Success report  </title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_cntr.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmTTPVendorPODashboardSuccess.js"></script>
<script>
var vendorCapaId='<%=capaDtlsId%>';
var vendorId ='<%=vendorId%>';
var gStrServletPath = "<%=strServletPath%>";
</script>

</head>

<body leftmargin="20" topmargin="10" onload="fnOnPageSuccessLoad();">
<html:form action="/gmTTPVendorPOPopUp.do?method=fetchVendorPOSuccess" >
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hPOId" value="">
<input type="hidden" name="hVenId" value="">
<table cellspacing="0" cellpadding="0" border="0" class="DtTable700">
<tr height="25">
				<td  class="RightDashBoardHeader"><fmtTTPPODashPopup:message key="LBL_PO_SUCCESS_HEADER" /></td>
				<td  class="RightDashBoardHeader" align="right" ><fmtTTPPODashPopup:message key="IMG_HELP" var="varHelp" />
				 <img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
	    <tr height="25">
		<td class="RightTableCaption"align="right" >
		<fmtTTPPODashPopup:message key="LBL_VENDOR_NAME" var="varVendorNm"/>
		<gmjsp:label type="RegularText" SFLblControlName="${varVendorNm}" td="false" />:&nbsp;
		</td>
        <td id="lbl_vendor_nm" align="left"width="400">
		</td>
		</tr>
		<tr>
		   <td class="LLine" colspan="2" height="1"></td>
		</tr>
		
		<tr height="25" class="Shade">
		<td class="RightTableCaption" align="right">
		<fmtTTPPODashPopup:message key="LBL_LOCK_PERIOD" var="varLockPeriod"/>
		<gmjsp:label type="RegularText" SFLblControlName="${varLockPeriod}" td="false" />:&nbsp;
		</td>
		<td id="lbl_lock_period" align="left">
		</td>
		
		</tr>
		<tr>
		   <td class="LLine" colspan="2" height="1"></td>
		</tr>
		<tr  height="25">
		<td class="RightTableCaption" align="right">
		<fmtTTPPODashPopup:message key="LBL_PO_CREATED_BY" var="varPOCreatedBy"/>
		<gmjsp:label type="RegularText" SFLblControlName="${varPOCreatedBy}" td="false" />:&nbsp;
		</td>
		<td id="lbl_po_created_by" align="left">
		</td>
		</tr>
		<tr>
		  <td class="LLine" colspan="2" height="1"></td>
		</tr>
		<tr height="25" class="Shade">
		<td class="RightTableCaption" align="right">
		<fmtTTPPODashPopup:message key="LBL_PO_CREATED_DATE" var="varPOCreatedDate"/>
		<gmjsp:label type="RegularText" SFLblControlName="${varPOCreatedDate}" td="false" />:&nbsp;
		</td>
		<td id="lbl_po_created_date" align="left">
		</td>
		</tr>
		<tr>
			<td class="Line" colspan="2" height="1"></td>
		</tr>
		<tr>
			<td colspan="2"><div id="dataGridDiv" style="width: 100%;"></div></td>
		</tr>
		<tr>
		<td colspan="2" align="center" >
				<div class='exportlinks' id="DivExportExcel"><img src='img/ico_file_excel.png' />&nbsp;
				<a href="#" onclick="fnExport();"> <fmtTTPPODashPopup:message key="DIV_EXCEL" /></a>
				</div>
			</td>
		</tr>
		<tr>
			<td class="Line" colspan="2" height="1"></td>
		</tr>
		<tr height="30">
		<td colspan="2" align="center">
		   <div id="DivClose" ><gmjsp:button value="Close" name="Btn_Close" gmClass="button" buttonType="Load" onClick="fnClose();" style="width: 5em"/></div>
		</td>
		</tr>
		<tr>
			<td colspan="2" align="center" class="RegularText"><div id="DivNothingMessage"><fmtTTPPODashPopup:message key="NO_DATA_FOUND" /></div></td>
		</tr> 
	</table>
</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>