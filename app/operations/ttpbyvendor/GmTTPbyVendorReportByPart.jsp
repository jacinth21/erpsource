<%
/**********************************************************************************
 * File		 		: GmTTPByVendorReportByPart.jsp
 * Desc		 		: TTP By Vendor Details Report
 * Version	 		: 1.0
 * author			: MuthuKumar Kosalram
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- app\operations\forecast\GmTTPByVendorSummary.jsp -->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTTPByVenParDet" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtTTPByVenParDet:setLocale value="<%=strLocale%>"/>
<fmtTTPByVenParDet:setBundle basename="properties.labels.operations.forecast.GmTTPByVendorPartDetails"/>

<bean:define id="yearId" name="frmTTPByVendorParDet" property="yearId" type="java.lang.String"> </bean:define>
<bean:define id="monthId" name="frmTTPByVendorParDet" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="ttpId" name="frmTTPByVendorParDet" property="ttpId" type="java.lang.String"> </bean:define>
<bean:define id="ttpName" name="frmTTPByVendorParDet" property="ttpName" type="java.lang.String"> </bean:define>
<bean:define id="vendorId" name="frmTTPByVendorParDet" property="vendorId" type="java.lang.String"> </bean:define>
<bean:define id="vendorName" name="frmTTPByVendorParDet" property="vendorName" type="java.lang.String"> </bean:define>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("TTP_BY_VENDOR_REPORT_PART_DET");
String strCollapseImage = strImagePath+"/plus.gif";
%>
<html>
<head>
<title>Globus Medical: TTP By Vendor Summary Report</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmTTPbyVendorReportByPart.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script type="text/javascript">

</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnOnpageLoad();">
<html:form action="/gmTTPByVendorPartDetails.do" >
<%-- <html:hidden property="hmHistoricalMonth" />
<html:hidden property="hmForecastlMonth" /> --%>
<table border="0" class="DtTable1200"  cellspacing="0" cellpadding="0">
   <tr height="25">
	  <td colspan="9" class="RightDashBoardHeader"><fmtTTPByVenParDet:message key="LBL_TTP_BY_VENDOR_RPT_HEADER"/></td>
	  <td colspan="2" class="RightDashBoardHeader" align="right" width="200">
	  <fmtTTPByVenParDet:message key="IMG_HELP" var="varHelp"/>
	  <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	  </td>
   </tr>
   <tr height="30">
   <td width ="125" class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;<fmtTTPByVenParDet:message key="LBL_TTP_NAME"/>:</td>
   <td width="23%" style="padding-left: 2px;">
	      <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
	      <jsp:param name="CONTROL_NAME" value="ttpId" />
	      <jsp:param name="METHOD_LOAD" value="loadTTPList&STROPT=GOP" />
	      <jsp:param name="WIDTH" value="240" />
	      <jsp:param name="CSS_CLASS" value="search" />
	      <jsp:param name="TAB_INDEX" value=""/>
	      <jsp:param name="CONTROL_NM_VALUE" value="<%=ttpName%>" />
	      <jsp:param name="CONTROL_ID_VALUE" value="<%=ttpId%>" />
	      <jsp:param name="SHOW_DATA" value="50" />
	      </jsp:include>	
	  </td>
	 <td width ="90" class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;<fmtTTPByVenParDet:message key="LBL_VENDOR"/>:</td>
	    <td width="23%" style="padding-left: 2px;">
	      <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="vendorId" />
				<jsp:param name="METHOD_LOAD" value="loadVendorNameList&searchType=LikeSearch" />
				<jsp:param name="WIDTH" value="300" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="TAB_INDEX" value="3"/>
				<jsp:param name="CONTROL_NM_VALUE" value="<%=vendorName%>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=vendorId%>" />
 				<jsp:param name="SHOW_DATA" value="50" />
 				<jsp:param name="AUTO_RELOAD" value="" /></jsp:include>
	  </td>
	 
	  
	  <td class="RightTableCaption" HEIGHT="25" width ="70" colspan="2" align="right">
	    <fmtTTPByVenParDet:message key="LBL_MONTH" var="varMonth" /> <gmjsp:label type="MandatoryText" SFLblControlName="${varMonth}:" td="false"/></td>
	      <td  width ="100" align="left"> &nbsp;<gmjsp:dropdown controlName="monthId"  SFFormName='frmTTPByVendorParDet' SFSeletedValue="monthId" SFValue="alMonth" width="90"	 
	              codeId="CODENMALT" codeName="CODENM"  defaultValue= "[Choose One]" tabIndex="3" /></td>    
	          
	   <td class="RightTableCaption" HEIGHT="25" width ="65" colspan="2" align="right"><fmtTTPByVenParDet:message key="LBL_YEAR" var="varYear"/><gmjsp:label type="MandatoryText" SFLblControlName="${varYear}:" td="false"/></td>
	      <td  width ="100" align="left">&nbsp;<gmjsp:dropdown controlName="yearId"  SFFormName='frmTTPByVendorParDet' SFSeletedValue="yearId" SFValue="alYear" width="90" 	
	             codeId="CODENM" codeName="CODENM"  defaultValue= "[Choose One]" tabIndex="4" /></td>
	    <td class="RightTableCaption" width ="50" align="center" colspan="1">         
	     <gmjsp:button name="BTN_LOAD" value="Load" gmClass="button" onClick="fnPartDtdLoad();" buttonType="Load" tabindex="5" /></td>        
   </tr>
    <tr id="trDiv"  style="display: none">
    <td height="5" colspan="11"></td></tr>
   <tr><td colspan="11"><div id="dataGridDiv" class="" height="500px" width="1200px" ></div></td></tr>
   
	<tr><td colspan="11" align="center">
		<div class='exportlinks' id="DivExportExcel"><fmtTTPByVenParDet:message key="DIV_EXPORT_OPT"/> :<img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnExport();">
		<fmtTTPByVenParDet:message key="DIV_EXCEL"/> </a></div></td>
	</tr>
		<tr>
		<td class="LLine" colspan="11" height="1"></td>
		</tr>
	<tr><td colspan="11" align="center" class="RegularText"><div id="DivNothingMessage" style="display: none;">
	      <fmtTTPByVenParDet:message key="NO_DATA_FOUND" /></div></td>
	</tr>
		<tr>
		<td class="LLine" colspan="11" height="1"></td>
		</tr>
	
</table>
</html:form>
<%@ include file="/common/GmFooter.inc" %>
</body>

</html>