<%
/**********************************************************************************
 * File		 		: GmTTPbyVendorMOQBulk.jsp
 * Desc		 		: Uploading Part Minimum Order Quantity by Vendor 
 * Version	 		: 
 * author			: Prabhu vigneshwaran M D 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.*,java.text.*"%>
<%@ taglib prefix="fmtVendorMOQBulkUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtVendorMOQBulkUpload:setBundle basename="properties.labels.operations.forecast.GmTTPbyVendorMOQBulkUpload"/>
<%@ page isELIgnored="false" %>
<%@ page buffer="16kb" autoFlush="true" %>

<bean:define id="editAccess" name="frmVendorMoq"  property="strEditAccess" type="java.lang.String"></bean:define>
<bean:define id="strMaxUploadData" name="frmVendorMoq" property="strMaxUploadData" type="java.lang.String"> </bean:define>
<bean:define id="vendorId" name="frmVendorMoq" property="vendorId" type="java.lang.String"> </bean:define>
<bean:define id="vendorName" name="frmVendorMoq" property="vendorName" type="java.lang.String"> </bean:define>
<%
response.setHeader("Cache-Control", "no-cache, post-check=0, pre-check=0"); //HTTP 1.1
response.setHeader("Pragma", "no-cache"); //HTTP 1.0
response.setDateHeader("Expires", 0); //prevents caching at the proxy server 

String strPurchasingJsPath= GmFilePathConfigurationBean.getFilePathConfig("JS_PURCHASING");
String strWikiTitle = GmCommonClass.getWikiTitle("VEN_MOQ_BULK_UPLOAD");
%> 

<html>
<head>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"> 
<title>Vendor MOQ Bulk Upload</title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all"> @import url("<%=strCssPath%>/screen.css");</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/fonts/font_roboto/roboto.css"/>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Prodmgmnt<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/suite/5.2.0/codebase/dhtmlx_deprecated.js"></script> 

<script language="JavaScript" src="<%=strPurchasingJsPath%>/ttpbyvendor/GmTTPbyVendorMOQBulk.js"></script>
<script>
var maxUploadData = '<%=strMaxUploadData%>';
var lblvendorname = '<fmtVendorMOQBulkUpload:message key="LBL_VENDOR_NAME"/>';
</script>
<style type="text/css">
.even{
	background-color:#ffffff !important;
}
.uneven{
background-color:#dcedfc !important;;
}
		
div.gridbox table.hdr td {
   color: #000000 !important;
   background-color: #d2e9fc !important;
}
div.gridbox_material.gridbox .xhdr {
	background:#dcedfc !important;
}
</style>
</head>
<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<html:form action="/gmTTPbyVendorMoq.do?method=loadTTPbyVendorMOQBulk">
<html:hidden property="inputString" name ="frmVendorMoq"/>
<html:hidden property="strPartNumbers" name ="frmVendorMoq"/>

		<table border="0" class="DtTable700"  cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="4">
					<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
						<td height="25" class="RightDashBoardHeader" colspan="2">&nbsp;<fmtVendorMOQBulkUpload:message key="LBL_VEN_MOQ_BULK_UPLOAD"/></td>
						<td  height="25" class="RightDashBoardHeader" align="right" colspan="2">
							<fmtVendorMOQBulkUpload:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
			      			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>',' <%=strWikiTitle%>');" />
			      			</td>
		      		</tr></table>
	      		</td>
			</tr>
			<tr>
		<td  colspan="2" class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;<fmtVendorMOQBulkUpload:message key="LBL_VENDOR_NAME"/></td>
	    <td  colspan="2" style="padding-left: 5px;">
	      <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="vendorId" />
				<jsp:param name="METHOD_LOAD" value="loadVendorNameList&searchType=LikeSearch" />
				<jsp:param name="WIDTH" value="300" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="TAB_INDEX" value="1"/>
				<jsp:param name="CONTROL_NM_VALUE" value="<%=vendorName%>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=vendorId%>" />
 				<jsp:param name="SHOW_DATA" value="50" />
 				<jsp:param name="AUTO_RELOAD" value="fnChange(this);" /></jsp:include>
	  </td>
			</tr>
			<tr><td class="LLine" colspan="4" height="1"></td></tr>
		<tr  style="display: none" id="trImageShow"  class="Shade" height="25">
		<td colspan="4">
		<table>	
				<tr  height="25">		
						<td  width="28" colspan="1" tabIndex="-1"><fmtVendorMOQBulkUpload:message key="IMG_ALT_COPY" var="varCopy"/><a href="#"><img src="<%=strImagePath%>/dhtmlxGrid/copy.gif" onClick="javascript:docopy()" alt="${varCopy}" style="border: none;" height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="-1"><fmtVendorMOQBulkUpload:message key="IMG_ALT_PASTE" var="varpaste"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/paste.gif" alt="${varpaste}" style="border: none;" onClick="javascript:pasteToGrid()" height="14"></a></td>	
						<td  width="28" colspan="1" tabIndex="-1"><fmtVendorMOQBulkUpload:message key="IMG_ALT_ADD_ROW" var="varAddRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="javascript:fnAddRows()"height="14"></a></td>
						<td  width="28" colspan="1" tabIndex="-1"><fmtVendorMOQBulkUpload:message key="IMG_ALT_REMOVE_ROW" var="varRemoveRow"/><a href="#"> <img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:fnRemoveSetUploadRow()" height="14"></a></td>
						
						<td  colspan="2" align="right" >
						<div id="progress" style="display:none;"><img src="<%=strImagePath%>/success_y.gif" height="15"> </div></td>
						<td><div id="msg"  align="center"  style="display:none;color:green;font-weight:bold;"></div> 
					</td>
			   </tr>
			   <tr height="25" id="trLabelShow"><td colspan="8"><font color="#6699FF">&nbsp;
                    <fmtVendorMOQBulkUpload:message key="LBL_MAXIMUM"/>&nbsp;<%=strMaxUploadData%>&nbsp;<fmtVendorMOQBulkUpload:message key="LBL_LESS"/> </font></td>
		            </tr>													
		</table>
		</td>
		</tr>	
		<tr><td class="LLine" colspan="4" height="1"></td></tr>
		<tr id="trDiv">
              <td colspan="4">
	         <div id="dataGridDiv"  height="400px"></div></td>
        </tr> 
		<tr><td class="LLine" colspan="4" height="1"></td></tr>
		
		  		<tr  id="trComments">
				    <td colspan="8" > 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmVendorMoq" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="txt_LogReason" />
							<jsp:param name="LogMode" value="Edit" />
							<jsp:param name="Mandatory" value="no" />
							<jsp:param name="TabIndex" value="2" />
							</jsp:include>
					</td>
		          </tr>
		
		<tr><td class="LLine" colspan="4" height="1"></td></tr>
		   <tr style="display: none" id="buttonshow" height="30"><td colspan="4"  align="center">
           
           <div id="buttonDiv">
            <logic:equal name="frmVendorMoq" property="strEditAccess" value="Y"> 
	            <fmtVendorMOQBulkUpload:message key="LBL_SAVE" var="varSave"/>
	            <gmjsp:button value="${varSave}" gmClass="button"  tabindex="3" name="LBL_SAVE" onClick="fnSave();"  buttonType="Save"/>
			 </logic:equal>
			</div>
			
			</td>
			
			</tr> 
        		
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>