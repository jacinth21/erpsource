<%
/**********************************************************************************
 * File		 		: GmTTPbyVendorReport.jsp
 * Desc		 		: Vendor TTP Period Summary Report
 * Version	 		: 1.0
 * author			: Agilan Singaravel
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 <!-- \operations\purchasing\orderplanning\GmTTPbyVendorReport.jsp-->
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%-- <%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel" %> --%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtTTPByVenRpt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtTTPByVenRpt:setLocale value="<%=strLocale%>" />
<fmtTTPByVenRpt:setBundle basename="properties.labels.operations.forecast.GmTTPbyVendorReport" />

<%-- <%@ page import ="com.globus.common.beans.GmCrossTabFormat"%> --%>

<bean:define id="yearId" name="frmTTPbyVendorReport" property="yearId" type="java.lang.String"> </bean:define>
<bean:define id="monthId" name="frmTTPbyVendorReport" property="monthId" type="java.lang.String"> </bean:define>
<bean:define id="ttpId" name="frmTTPbyVendorReport" property="ttpId" type="java.lang.String"> </bean:define>
<bean:define id="ttpName" name="frmTTPbyVendorReport" property="ttpName" type="java.lang.String"> </bean:define>
<bean:define id="vendorId" name="frmTTPbyVendorReport" property="vendorId" type="java.lang.String"></bean:define>
<bean:define id="vendorName" name="frmTTPbyVendorReport" property="vendorName" type="java.lang.String"></bean:define>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("TTP_BY_VENDOR_PERIOD_SUMMARY_REPORT");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: TTP By Vendor Summary Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmTTPByVendorReport.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<%-- <style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style> --%>
</HEAD>

<body leftmargin="20" topmargin="10" onload="fnOnpageLoad();">
<html:form action="/gmTTPbyVendorReport.do">

 <!-- Custom tag lib code modified for JBOSS migration changes -->
<table border="0" class="DtTable1300" cellspacing="0" cellpadding="0">
		<tr height="25">
			<td colspan="7" class="RightDashBoardHeader"><fmtTTPByVenRpt:message key="LBL_TTP_BY_VENDOR_RPT_HEADER" /></td>
				<td colspan="1" class="RightDashBoardHeader" align="right" width="20"><fmtTTPByVenRpt:message key="IMG_HELP" var="varHelp" />
				 <img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr> 
		<tr>
			<td width="1000" valign="top" colspan="2"> 
				<table border="0" width="100%" cellspacing="0" cellpadding="0" >
                    <tr><td colspan="7" class="ELine"></td></tr> 
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="8%"><font color="red">*</font><fmtTTPByVenRpt:message key="LBL_TTP_NAME"/>:</td>  
						<td style="padding-left: 2px;">
						    <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					        <jsp:param name="CONTROL_NAME" value="ttpId" />
					        <jsp:param name="METHOD_LOAD" value="loadTTPList&STROPT=GOP" />
					        <jsp:param name="WIDTH" value="240" />
					        <jsp:param name="CSS_CLASS" value="search" />
					        <jsp:param name="TAB_INDEX" value="1"/>
					        <jsp:param name="CONTROL_NM_VALUE" value="<%=ttpName %>" />
					        <jsp:param name="CONTROL_ID_VALUE" value="<%=ttpId %>" />
					        <jsp:param name="SHOW_DATA" value="50" />
					        <jsp:param name="AUTO_RELOAD" value="" />	
				            </jsp:include>	
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="30" ><font color="red">*</font><fmtTTPByVenRpt:message key="LBL_VENDOR"/>:</td>
							<td style="padding-left: 2px;">
						    <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					        <jsp:param name="CONTROL_NAME" value="vendorId" />
					        <jsp:param name="METHOD_LOAD" value="loadVendorNameList&searchType=LikeSearch" />
					        <jsp:param name="WIDTH" value="300" />
					        <jsp:param name="CSS_CLASS" value="search" />
					        <jsp:param name="TAB_INDEX" value="2"/>
					        <jsp:param name="CONTROL_NM_VALUE" value="<%=vendorName%>" />
					        <jsp:param name="CONTROL_ID_VALUE" value="<%=vendorId%>" />
					        <jsp:param name="SHOW_DATA" value="50" />
					        <jsp:param name="AUTO_RELOAD" value="" />	
				            </jsp:include>	
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="5%"><font color="red">*</font><fmtTTPByVenRpt:message key="LBL_MONTH"/>:</td>
						<td width="8%" style="padding-left: 2px;"><gmjsp:dropdown controlName="monthId" SFFormName="frmTTPbyVendorReport" SFSeletedValue="monthId"
								SFValue="alMonth" codeId = "CODENMALT"  codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="3" width="105"/>													
						</td>
						
						<td class="RightTableCaption" align="right" HEIGHT="30" width="5%"><font color="red">*</font><fmtTTPByVenRpt:message key="LBL_YEAR"/>:</td>
						<td width="6%" style="padding-left: 2px;"><gmjsp:dropdown controlName="yearId" SFFormName="frmTTPbyVendorReport" SFSeletedValue="yearId"
								SFValue="alYear" codeId = "CODENM"  codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="4" width="95" />													
						</td>		
						<td align="right" width="10%">
							<gmjsp:button name="loadReport" value="&nbsp;Load&nbsp;" gmClass="button" onClick="fnVendorReportLoad();" buttonType="Load"  tabindex="7" />
						</td>			
					</tr>					
			   	</table>
			  </td>
		</tr>
		<tr>
			<td class="Line" colspan="9" height="1"></td>
		</tr>
		<tr>
			<td colspan="9"><div id="dataGridDiv" style="width: 100%;"></div></td>
		</tr>
		<tr>
			<td colspan="9" align="center">
				<div class='exportlinks' id="DivExportExcel"><fmtTTPByVenRpt:message key="DIV_EXPORT_OPT" />:<img src='img/ico_file_excel.png' />&nbsp;
				<a href="#" onclick="fnExport();"> <fmtTTPByVenRpt:message key="DIV_EXCEL" /></a>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="9" align="center" class="RegularText"><div id="DivNothingMessage"><fmtTTPByVenRpt:message key="NO_DATA_FOUND" /></div></td>
		</tr> 
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc" %>

</BODY>
</HTML>
