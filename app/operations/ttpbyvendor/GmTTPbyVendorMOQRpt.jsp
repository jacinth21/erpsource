<%
/**********************************************************************************
 * File		 		: GmTTPbyVendorMOQRpt.jsp
 * Desc		 		: To view the TTP by vendor MOQ details
 * Version	 		: 1.0
 * author			: Tamizhthangam Ramasamy 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ taglib prefix="fmtVendorMOQRpt"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtVendorMOQRpt:setLocale value="<%=strLocale%>" />
<fmtVendorMOQRpt:setBundle basename="properties.labels.operations.ttpbyvendor.GmTTPByVendorMOQReport" />

<%
	String strOperTTPVenJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_TTPVENDOR");
	String strWikiTitle = GmCommonClass.parseNull((String) GmCommonClass.getWikiTitle("TTP_BY_VENDOR_MOQ_REPORT"));
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<!-- Dhtmlx Import -->
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strOperTTPVenJsPath%>/GmTTPbyVendorMOQRpt.js"></script>

<title>Globus Medical:Vendor MOQ Report</title>
</head>
<body leftmargin="20" topmargin="10">
	<html:form action="/gmTTPbyVendorMoq.do">
		<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">
			<tr height="25">
				<td colspan="5" class="RightDashBoardHeader"><fmtVendorMOQRpt:message key="LBL_VENDOR_MOQ_REPORT_HDR" /></td>
				<td colspan="2" class="RightDashBoardHeader" align="right" width="200">
				   <fmtVendorMOQRpt:message key="IMG_HELP" var="varHelp" />
				    <img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td width="100" class="RightTableCaption" HEIGHT="25" align="right">
				   <fmtVendorMOQRpt:message key="LBL_VENDOR_NAME" var="varName" /> 
				   <gmjsp:label type="RegularText" SFLblControlName="${varName}:" td="false" />
				</td>
				<td width="320" style="padding-left: 3px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="vendorId" />
						<jsp:param name="METHOD_LOAD" value="loadVendorNameList&searchType=LikeSearch" />
						<jsp:param name="WIDTH" value="300" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="1" />
						<jsp:param name="CONTROL_NM_VALUE" value="" />
						<jsp:param name="CONTROL_ID_VALUE" value="" />
						<jsp:param name="SHOW_DATA" value="50" />
						<jsp:param name="AUTO_RELOAD" value="" />
					</jsp:include>
				</td>
				<td class="RightTableCaption" align="right" width="40">
				   <fmtVendorMOQRpt:message key="LBL_DIVISION" var="varName" /> 
				   <gmjsp:label type="RegularText" SFLblControlName="${varName}:" td="false" />
				</td>
				<td width="100" style="padding-left: 3px;">
				     <gmjsp:dropdown controlName="divisionId" SFFormName="frmVendorMoq" SFSeletedValue="divisionId" 
				         SFValue="alDivisionList" codeId="DIVISION_ID" codeName="DIVISION_NAME" defaultValue="[All]" tabIndex="2" />
				 </td>
				<td class="RightTableCaption" align="right" width="60" style="padding-left: 3px; white-space: nowrap;"><fmtVendorMOQRpt:message key="LBL_PART_NUMBER" />:</td>
				<td colspan="2">&nbsp;<html:text name="frmVendorMoq" property="partNum" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" tabindex="3" /> 
						<html:select property="pnumSuffix" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="4">
						<html:option value="0"><fmtVendorMOQRpt:message key="LBL_CHOOSEONE" /></html:option>
						<html:option value="LIT"><fmtVendorMOQRpt:message key="LBL_LITERAL" /></html:option>
						<html:option value="LIKEPRE"><fmtVendorMOQRpt:message key="LBL_LIKE_PREFIX" /></html:option>
						<html:option value="LIKESUF"><fmtVendorMOQRpt:message key="LBL_LIKE_SUFFIX" /></html:option>
					</html:select>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="7" height="1"></td>
			</tr>
			<tr class="Shade" height="30">
				<td class="RightTableCaption" align="right">&nbsp;<fmtVendorMOQRpt:message key="LB_PROJECT_NAME" />:</td>
				<td colspan="" style="padding-left: 3px;"><jsp:include page="/common/GmAutoCompleteInclude.jsp">
						<jsp:param name="CONTROL_NAME" value="projectId" />
						<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=Search&searchBy=ProjectId" />
						<jsp:param name="WIDTH" value="300" />
						<jsp:param name="CSS_CLASS" value="search" />
						<jsp:param name="TAB_INDEX" value="5" />
						<jsp:param name="CONTROL_NM_VALUE" value="" />
						<jsp:param name="CONTROL_ID_VALUE" value="" />
						<jsp:param name="SHOW_DATA" value="100" />
						<jsp:param name="AUTO_RELOAD" value="" />
					</jsp:include>
				</td>
				<td class="RightTableCaption" align="right" width="80" style="white-space: nowrap;">
				   <fmtVendorMOQRpt:message key="LBL_PURCHASE_AGENT" var="varName" /> 
				   <gmjsp:label type="RegularText" SFLblControlName="${varName}:" td="false" />
				</td>
				<td  colspan="2">
				     <gmjsp:dropdown controlName="purchaseAgent" SFFormName="frmVendorMoq" SFSeletedValue="purchaseAgent" 
				         SFValue="alPurchaseAgentList" codeId="ID" codeName="NAME" defaultValue="[Choose One]" tabIndex="2" />
				 </td>
				<td align="left" colspan="2" height="30">
				    <gmjsp:button name="BTN_LOAD" value="Load" gmClass="button" onClick="fnLoad();"	buttonType="Load" tabindex="6" /></td>
			</tr>
			<tr>
				<td class="LLine" colspan="7" height="1"></td>
			</tr>
			<tr>
				<td colspan="7"><div id="dataGridDiv" style="display: none; width: 100%"></div></td>
			</tr>
			<tr>
				<td colspan="7" align="center">
					<div class='exportlinks' id="DivExportExcel" style="display: none;">
						<fmtVendorMOQRpt:message key="DIV_EXPORT_OPT" />:
						<img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnExport();"> 
						<fmtVendorMOQRpt:message key="DIV_EXCEL" /></a>
					</div>
				</td>
			</tr>
			<tr>
				<td class="LLine" colspan="7" height="1"></td>
			</tr>
			<tr>
				<td colspan="7" align="center" class="RegularText"><div id="DivNothingMessage" style="display: none;">
						<fmtVendorMOQRpt:message key="NO_DATA_FOUND" /></div>
				</td>
			</tr>
		</table>
	</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>