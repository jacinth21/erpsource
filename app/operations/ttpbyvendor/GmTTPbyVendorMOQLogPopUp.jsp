
<%
/**********************************************************************************
 * File		 		: GmTTPbyVendorMOQLogPopUp.jsp
 * Desc		 		: To view the TTP by vendoe MOQ Log details
 * Version	 		: 1.0
 * author			: Tamizhthangam Ramasamy 
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtVendorMOQLog"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<fmtVendorMOQLog:setBundle
	basename="properties.labels.operations.ttpbyvendor.GmTTPByVendorMOQReport" />
<fmtVendorMOQLog:setLocale value="<%=strLocale%>" />

<bean:define id="vendorMOQId" name="frmVendorMoq"
	property="vendorMOQId" type="java.lang.String"></bean:define>
<%
String strOperTTPVenJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS_TTPVENDOR");
String strWikiTitle = GmCommonClass.parseNull((String)GmCommonClass.getWikiTitle("TTP_BY_VENDOR_MOQ_LOG_REPORT"));
%>
<html>
<head>
<title>PO Generated Success report</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_srnd.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript"
	src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_cntr.js"></script>
<script language="JavaScript"
	src="<%=strOperTTPVenJsPath%>/GmTTPbyVendorMOQLogPopUp.js"></script>
<script>
var vendorMOQId='<%=vendorMOQId%>';
</script>

</head>

<body leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form action="/gmTTPbyVendorMoq.do">
		<table cellspacing="0" cellpadding="0" border="0" class="DtTable700">
			<tr height="25">
				<td class="RightDashBoardHeader"><fmtVendorMOQLog:message
						key="LBL_LOG_HISTORY_HEADER" /></td>
				<td class="RightDashBoardHeader" align="right"><fmtVendorMOQLog:message
						key="IMG_HELP" var="varHelp" /> <img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<!-- <tr><td colspan="2" height="5"></td></tr> -->
			<tr>
				<td colspan="2"><div id="dataGridDiv"
						style="display: none; width: 100%"></div></td>
			</tr>
			<tr height="30">
				<td colspan="2" align="center">
					<div id="DivClose" style="display: none;">
						<gmjsp:button value="Close" name="Btn_Close" gmClass="button"
							buttonType="Load" onClick="fnClose();" style="width: 5em" />
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" class="RegularText"><div
						id="DivNothingMessage" style="display: none;">
						<fmtVendorMOQLog:message key="NO_DATA_FOUND" />
					</div></td>
			</tr>
		</table>
	</html:form>
</body>
<%@ include file="/common/GmFooter.inc"%>
</html>