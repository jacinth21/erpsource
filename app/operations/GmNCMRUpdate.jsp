 <%
/**********************************************************************************
 * File		 		: GmNCMRUpdate.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib prefix="fmtNCMRUpdate" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtNCMRUpdate:setLocale value="<%=strLocale%>"/>
<fmtNCMRUpdate:setBundle basename="properties.labels.operations.GmNCMRUpdate"/>

<!-- operations\GmNCMRUpdate.jsp -->

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	String strEVLWikiTitle = GmCommonClass.getWikiTitle("EVALUATION_NCMR_CREATION");
	String strNCMRWikiTitle = GmCommonClass.getWikiTitle("NCMR_UPDATE");
	String strWikiTitle ="";
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strNCMRList	= (String)request.getAttribute("hNCMRLIST")==null?"":(String)request.getAttribute("hNCMRLIST");
	String strApplDateFmt = strGCompDateFmt;
	HashMap hmNCMRDetails = new HashMap();
	ArrayList alEmpList = new ArrayList();
	ArrayList alActionList = new ArrayList();
	ArrayList alResponList = new ArrayList();

	String strShade = "";
	String strNCMRId = "";
	String strDHRId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyInsp = "";
	String strQtyRej = "";
	String strUserName = "";
	String strCloseoutName = "";
	String strCreatedDate = "";
	String strRejReason = "";
	String strInspectedBy = "";
	String strCodeID = "";
	String strCodeName = "";
	String strSelected = "";

	String strDispAction = "";
	String strRtsFl = "";
	String strRtsChecked = "";
	String strRespon = "";
	String strRemarks = "";
	String strAuthBy = "";
	String strCloseQty = "";
	String strDate = "";
	String strCAPAFl = "";
	String strCAPAChecked = "";
	String strCloseDate = "";
	String strEvalReason="";
	String strEvalDate="";
	String strStatus="";
	String strProdType = "";
	String strDonorNum = "";
	String strVendorId = "";
	String strSerialNumFl = "";
	
	Date dtDate = null;
	Date dtClosureDate = null;
	Date dtEvalDate = null;
	
	if (hmReturn != null)
	{
		hmNCMRDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("NCMRDETAILS"));
		strNCMRId = GmCommonClass.parseNull((String)hmNCMRDetails.get("NCMRID"));
		strDHRId = GmCommonClass.parseNull((String)hmNCMRDetails.get("ID"));
		strVendName = GmCommonClass.parseNull((String)hmNCMRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmNCMRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmNCMRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmNCMRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmNCMRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmNCMRDetails.get("QTYREC"));
		strQtyInsp = GmCommonClass.parseNull((String)hmNCMRDetails.get("QTYINSP"));
		strQtyRej = GmCommonClass.parseNull((String)hmNCMRDetails.get("QTYREJ"));
		strUserName = GmCommonClass.parseNull((String)hmNCMRDetails.get("UNAME"));
		strCloseoutName = GmCommonClass.parseNull((String)hmNCMRDetails.get("CLSBY"));
		//strCreatedDate = GmCommonClass.parseNull((String)hmNCMRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date) hmNCMRDetails.get("CDATE"), strApplDateFmt);
		strRejReason = GmCommonClass.parseNull((String)hmNCMRDetails.get("REJREASON"));
		strDispAction = GmCommonClass.parseNull((String)hmNCMRDetails.get("DISP"));
		strRtsFl = GmCommonClass.parseNull((String)hmNCMRDetails.get("RTSFL"));
		strRtsChecked = strRtsFl.equals("1")?"Checked":"";
		strRtsFl = strRtsFl.equals("1")?"Yes":"No";		
		strRespon = GmCommonClass.parseNull((String)hmNCMRDetails.get("RESPON"));
		strRemarks = GmCommonClass.parseNull((String)hmNCMRDetails.get("REMARKS"));
		strAuthBy = GmCommonClass.parseNull((String)hmNCMRDetails.get("DISPBY"));
		strCloseQty = GmCommonClass.parseNull((String)hmNCMRDetails.get("CLOSEQTY"));
		dtDate = (java.util.Date) hmNCMRDetails.get("DDATE");
		strDate = GmCommonClass.getStringFromDate(dtDate, strApplDateFmt);
		strCAPAFl = GmCommonClass.parseNull((String)hmNCMRDetails.get("CAPAFL"));
		strCAPAChecked = strCAPAFl.equals("1")?"Checked":"";
		strCAPAFl = strCAPAFl.equals("1")?"Yes":"No";
		dtClosureDate =(java.util.Date) hmNCMRDetails.get("CLOSEDATE");
		strCloseDate = GmCommonClass.getStringFromDate(dtClosureDate, strApplDateFmt);
		strEvalReason = GmCommonClass.parseNull((String)hmNCMRDetails.get("EVALREMARKS"));
		dtEvalDate = (java.util.Date) hmNCMRDetails.get("EVALDATE");
		strEvalDate = GmCommonClass.getStringFromDate(dtEvalDate, strApplDateFmt);
		strStatus = GmCommonClass.parseNull((String)hmNCMRDetails.get("STATUS"));
		strProdType 	= GmCommonClass.parseNull((String)hmNCMRDetails.get("PRODTYPE"));
		strDonorNum 	= GmCommonClass.parseNull((String)hmNCMRDetails.get("DONORNUMBER"));
		strVendorId		= GmCommonClass.parseNull((String)hmNCMRDetails.get("VID"));
		strSerialNumFl = GmCommonClass.parseNull((String)hmNCMRDetails.get("SERIALNUMNEEDFL"));
		
		if (strMode.equals("A"))
		{
			alEmpList = (ArrayList)hmReturn.get("EMP");
			alActionList = (ArrayList)hmReturn.get("ACTION");
			alResponList = (ArrayList)hmReturn.get("RESPON");
		}
	}
	
	int intSize = 0;
	HashMap hcboVal = null;
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: NCMR Update </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var companyInfoObj = '<%=strCompanyInfo%>';
var format = '<%=strApplDateFmt%>';
var lblDate = '<fmtNCMRUpdate:message key="LBL_DATE"/>';
function fnSubmit4()
{
	objEValDt = document.frmOrder.Txt_EvalDate;
	CommonDateValidation(objEValDt,format,Error_Details_Trans(message_operations[509],format));
	ValidationNull(document.frmOrder.Txt_EvalRemarks.value,message_operations[510]);
	fnValidateTxtFld('Txt_EvalDate',lblDate);
	objAction = document.frmOrder.Cbo_Action.value;
	if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}	
	if (objAction==0)
	{
		document.frmOrder.hAction.value = 'CreateNCMR';
		document.frmOrder.hMode.value='E';
		document.frmOrder.hEvalRemarks.value=document.frmOrder.Txt_EvalRemarks.value;
		document.frmOrder.hEvalDate.value=document.frmOrder.Txt_EvalDate.value;
		fnStartProgress('Y');
		document.frmOrder.submit();  
	}
	else if(objAction==1)
	{
		document.frmOrder.hAction.value = 'Cancel';
		document.frmOrder.hMode.value='E';
		document.frmOrder.hEvalRemarks.value=document.frmOrder.Txt_EvalRemarks.value;
		document.frmOrder.hEvalDate.value=document.frmOrder.Txt_EvalDate.value;
		fnStartProgress('Y');
  		document.frmOrder.submit();  
	}


}
document.onkeypress = function(){
	if(event.keyCode == 13 && document.frmOrder.hMode.value == 'Q' ){
		var test = fnUpdate();
		return test;
		}
	}
function fnUpdate()
{
	ValidationNull(TRIM(document.frmOrder.Txt_RejectionReason.value),message_operations[511]);
	var strRejectionReason =  document.frmOrder.Txt_RejectionReason.value;
	  if(strRejectionReason.length > 500 )
	  {
		  Error_Details(message_operations[512]);
	  } 
	
	<%
	if(strStatus.equals("0") || strStatus.equals("1")){
	%>
	if(document.frmOrder.hMode.value == 'Q' && (document.frmOrder.hQtyRej.value=='' || parseInt(document.frmOrder.hQtyRej.value)==0)){
		Error_Details(message_operations[513]);
	}
	if(document.frmOrder.hMode.value == 'Q' && (parseInt(document.frmOrder.hQtyInsp.value) < parseInt(document.frmOrder.hQtyRej.value))){
		Error_Details(message[2010]);
	//message : "Rejected quantity cannot be more than the inspected quantity"
	}
	
	
	<% }%>
<%
if (strNCMRList.equals("YES"))
{			
%>
	if (document.frmOrder.Txt_LogReason.value == "")
	{
		Error_Details(message[59]);
	}
	
<%}%>
	
	if (document.frmOrder.hMode.value == 'A')
	{
		var closeoutqty = parseInt(document.frmOrder.Txt_CloseQty.value,10);
		var rejQty = parseInt(document.frmOrder.hQtyRej.value,10);

		if (closeoutqty > rejQty)
		{
			Error_Details(message_operations[514]);
		}
	}
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}
	document.frmOrder.hAction.value = '<%=strMode%>';
	fnStartProgress('Y');
  	document.frmOrder.submit();  	
}

//To open the popup when click on "D" icon
function fnCallDonor(dhrId, vendorId){
	windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDonorInfo&strOpt=load&hDhrId="+dhrId+"&hMode=Report&screenType=NcmrUpdate&hVendorId="+vendorId+"&ncmrStatus="+<%=strStatus%>,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmNCMRServlet">
<input type="hidden" name="hNCMRId" value="<%=strNCMRId%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hMode" value="<%=strMode%>">
<input type="hidden" name="hEvalRemarks" value="<%=strEvalReason%>">
<input type="hidden" name="hEvalDate" value="<%=strEvalDate%>">
<input type="hidden" name="hDHRId" value="<%=strDHRId%>">
<input type="hidden" name="hQtyInsp" value="<%=strQtyInsp%>">


	<table cellpadding="0" cellspacing="0" border="0" class="DtTable800">
		
		<tr><td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="0"><tr>
			<%if(strMode.equals("E")){
				strWikiTitle = strEVLWikiTitle;
					%>
						<td height="25" class="RightDashBoardHeader"><fmtNCMRUpdate:message key="LBL_EVAL_NCMR"/></td>
						<%}else{ 
							strWikiTitle = strNCMRWikiTitle;
						%>
						<td height="25" class="RightDashBoardHeader"><fmtNCMRUpdate:message key="LBL_NCMR_UPD"/></td>
						<%} %>
					<td align="right" class=RightDashBoardHeader > 
					<fmtNCMRUpdate:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
					</td>
					 </tr></table>
			</td>
		</tr>
		<tr><td bgcolor="#666666"></td><tr>		
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<%if(strMode.equals("E")){
					%>
						<td class="RightTableCaption" height="25" align="right"><fmtNCMRUpdate:message key="LBL_EVAL_ID"/>:</td>
						<%}else{ %>
						<td class="RightTableCaption" height="25" align="right"><fmtNCMRUpdate:message key="LBL_NCMR_ID"/>:</td>
						<%} %>
						<td class="RightText">&nbsp;<%=strNCMRId%></td>
						<td class="RightTableCaption" align="right"><fmtNCMRUpdate:message key="LBL_DHR_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strDHRId%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr class="oddshade">
						<td class="RightText" height="25" align="right"><fmtNCMRUpdate:message key="LBL_ORIGINA"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>
						<td class="RightText" height="25" align="right"><fmtNCMRUpdate:message key="LBL_CRE_DT"/>:</td>
						<td class="RightText">&nbsp;<%=strCreatedDate%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="25" align="right"><fmtNCMRUpdate:message key="LBL_PART_NO"/>:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightText" align="right"><fmtNCMRUpdate:message key="LBL_CNTR_NO"/>:</td>
						<td class="RightText">&nbsp;<%=strControlNum%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr class="oddshade">
						<td class="RightText" height="25" align="right"><fmtNCMRUpdate:message key="LBL_DESC"/>:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
						<td class="RightText" align="right"><fmtNCMRUpdate:message key="LBL_DEPT_VENDOR"/> <span class="RightTextSmall"><fmtNCMRUpdate:message key="LBL_IF_APPL"/></span>:</td>
						<td class="RightText">&nbsp;<%=strVendName%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="25" align="right"><fmtNCMRUpdate:message key="LBL_WORK_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strWOId%></td>
						<td class="RightText" align="center"><fmtNCMRUpdate:message key="LBL_QTY"/></td>
						<td class="RightText">
							<table cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
									<td class="RightText"><fmtNCMRUpdate:message key="LBL_RECVD"/></td>
									<td class="RightText"><fmtNCMRUpdate:message key="LBL_INSPECTED"/></td>
									<td class="RightText"><fmtNCMRUpdate:message key="LBL_REJE"/></td>
								</tr>
								<tr><td colspan="3" bgcolor=#eeeeee height="1"></td></tr>
								<tr height="18">
									<td align="center" class="RightText"><%=strQtyRec%></td>
									<td align="center" class="RightText"><%=strQtyInsp%></td>
									<%if((strStatus.equals("0") || strStatus.equals("1")) && strMode.equals("Q")){ %>
										<td><input type="text" size="4" onFocus="changeBgColor(this,'#AACCE8');" 
										onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="hQtyRej" value="<%=strQtyRej%>"></td>
									<%}else{ %>
									<td align="center" class="RightText"><%=strQtyRej%>
									<input type="hidden" name="hQtyRej" value="<%=strQtyRej%>"></td>
									<%}%>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_DONOR#"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strDonorNum%> 
						<%if((!strDonorNum.equals("") || strSerialNumFl.equals("Y")) && !strStatus.equals("0") && !strMode.equals("E")){//'D' icon should not show on "Evaluation NCMR - Creation" screen%>
							&nbsp;<fmtNCMRUpdate:message key="BTN_DONOR" var="varDonor"/><img align="top" id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/d.gif' title='${varDonor}' onclick="javascript:fnCallDonor('<%=strDHRId%>','<%=strVendorId%>')" width='14' height='14'/>
						<%} %>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666"></td></tr>
		<tr class="ShadeRightTableCaption">
			<td Height="24"><fmtNCMRUpdate:message key="LBL_DESC_NON"/></td>
		</tr>	
		<td bgcolor="#666666"></td></tr>
		<tr>
	   <% if (strAction.equals("UpdateNCMR"))
		{
  		%>
		<td class="RightText" Height="40">
		<textarea name="Txt_RejectionReason"  class="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
			onBlur="changeBgColor(this,'#ffffff');" rows=4 cols=100><%=strRejReason%></textarea>	
		</td><%}
	   else{
		%>
		<td><%=strRejReason%></td>
		<%} %>
		
		</tr>
		<td>
<%		if (strMode.equals("E"))
{
%>	
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr class="ShadeRightTableCaption">
			<td colspan="2" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_DESC_EVAL"/></td>
		</tr>
			<tr><td colspan="2" HEIGHT="1" class="Line"></td></tr>
			<tr>
			<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_REMARKS"/>:&nbsp;</td>
			<%		if (strAction.equals("UpdateNCMREVAL"))
		{
%>		
			<td>
			<textarea name="Txt_EvalRemarks" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
			onBlur="changeBgColor(this,'#ffffff');" rows=4 cols=45 ></textarea></td>
			</tr>
			<%}else{ %>
			<td><%=strEvalReason%></td>
			<%} %>
			<tr><td colspan="2" HEIGHT="1" bgcolor="#eeeeee"></td></tr>
			<tr class="oddshade">
			<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_DATE"/>:&nbsp;</td>
			<td class="RightText" width="500">
<%		if (strAction.equals("UpdateNCMREVAL")){%>	        
			<gmjsp:calendar textControlName="Txt_EvalDate" textValue="<%=dtEvalDate==null?null:new java.sql.Date(dtEvalDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>
<%		}else{
%>			<%=strEvalDate%>
<%		}
%>		</td></tr>
		<tr><td colspan="2" HEIGHT="1" class="Line"></td></tr>
			
		</table>
<% }else{%>	
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr class="ShadeRightTableCaption">
			<td colspan="2" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_DIS_DT"/></td>
		</tr>
			<tr><td colspan="2" HEIGHT="1" class="Line"></td></tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_ACTN_TAKN"/>:</td>
				<td class="RightText" width="500">&nbsp;
<%		if (strMode.equals("A"))
		{
%>			<select name="Cbo_Action" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%	  		intSize = alActionList.size();
			hcboVal = new HashMap();
	  		for (int i=0;i<intSize;i++)
	  		{
	  			hcboVal = (HashMap)alActionList.get(i);
	  			strCodeID = (String)hcboVal.get("CODEID");
	  			strCodeName = (String)hcboVal.get("CODENM");
				strSelected = strDispAction.equals(strCodeName)?"selected":"";
%>				<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeName%></option>
<%	  		}
%>		</select>
<%		}
		else
		{ 
			out.println(strDispAction);
		}
%>		</td></tr>
		<tr><td colspan="2" HEIGHT="1" bgcolor="#eeeeee"></td></tr>
		<tr class="oddshade">
			<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_RET_TO"/> ?:</td>
			<td class="RightText" width="400">&nbsp;
<%		if (strMode.equals("A"))
		{
%>			<input type="Checkbox" name="Chk_RTS" <%=strRtsChecked%> >
<%		}else{
%>				<%=strRtsFl%>
<%		}
%>		</td></tr>
		<tr><td colspan="2" HEIGHT="1" bgcolor="#eeeeee"></td></tr>
		<tr>
			<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_RESP"/>:</td>
			<td class="RightText" width="400">&nbsp;
<%		if (strMode.equals("A"))
		{
%>			<select name="Cbo_Respon" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" 
			onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%		  		intSize = alResponList.size();
				hcboVal = new HashMap();
		  		for (int i=0;i<intSize;i++)
		  		{
		  			hcboVal = (HashMap)alResponList.get(i);
		  			strCodeID = (String)hcboVal.get("CODEID");
		  			strCodeName = (String)hcboVal.get("CODENM");
					strSelected = strRespon.equals(strCodeName)?"selected":"";
%>						<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%			  		}
%>					</select>
<%		}else{
%>					<%=strRespon%>
<%		}
%>			</td></tr>
			<tr><td colspan="2" HEIGHT="1" bgcolor="#eeeeee"></td></tr>
			<tr class="oddshade">
						<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_REMARKS"/>:</td>
						<td class="RightText" width="400">&nbsp;
<%		if (strMode.equals("A"))
		{
%>			<textarea name="Txt_Remarks" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
			 onBlur="changeBgColor(this,'#ffffff');" rows=4 cols=45 tabindex=24><%=strRemarks%></textarea>
<%		}else{
%>				<%=strRemarks%>
<%		}
%>		</td></tr>
		<tr><td colspan="2" HEIGHT="1" bgcolor="#eeeeee"></td></tr>
		<tr>
			<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_AUTH_BY"/>:</td>
			<td class="RightText" width="400">&nbsp;
<%		if (strMode.equals("A"))
		{
%>			<select name="Cbo_AuthEmp" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" 
			onBlur="changeBgColor(this,'#ffffff');"><option value="0" >[Choose One]
<%			intSize = alEmpList.size();
			hcboVal = new HashMap();
			for (int i=0;i<intSize;i++)
			{
				hcboVal = (HashMap)alEmpList.get(i);
			  	strCodeID = (String)hcboVal.get("ID");
			  	strCodeName = (String)hcboVal.get("NAME");
				strSelected = strAuthBy.equals(strCodeName)?"selected":"";
%>				<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%	  		}
%>			</select>
<%		}else{
%>		<%=strAuthBy%>
<%		}
%>		</td></tr>
		<tr><td colspan="2" HEIGHT="1" bgcolor="#eeeeee"></td></tr>
		<tr class="oddshade">
			<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_CLOS_QTY"/>:</td>
			<td class="RightText" width="400">&nbsp;
<%		if (strMode.equals("A"))
		{
%>			<input type="text" size="4" onFocus="changeBgColor(this,'#AACCE8');" 
			onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_CloseQty"
			value="<%=strCloseQty%>">

<%		}else{
%>			<%=strCloseQty%>
<%		}
%>			</td></tr>
		<tr><td colspan="2" HEIGHT="1" bgcolor="#eeeeee"></td></tr>
		<tr>
			<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_DATE"/>:</td>
			<td class="RightText" width="400">&nbsp;
<%		if (strMode.equals("A"))
		{
%>			
			<gmjsp:calendar textControlName="Txt_Date" textValue="<%=dtDate==null?null:new java.sql.Date(dtDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			
<%		}else{
%>			<%=strDate%>
<%		}
%>		</td></tr>
		<tr><td colspan="2" HEIGHT="1" class="Line"></td></tr>	
<%
		if (strMode.equals("C") || strMode.equals("D"))
		{
%>			<tr class="ShadeRightTableCaption"><td colspan="2" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_CLOSE_DT"/></td></tr>
			<tr><td colspan="2" HEIGHT="1" class="Line"></td></tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_CAPA"/> ?:</td>
				<td class="RightText" width="400">&nbsp;
<%			if (strMode.equals("C"))
			{
%>				<input type="Checkbox" name="Chk_CAPA" <%=strCAPAChecked%>>
<%			}else{
%>					<%=strCAPAFl%>
<%			}
%>			</tr>
			<tr><td colspan="2" HEIGHT="1" bgcolor="#eeeeee"></td></tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_CLS_DATE"/>:</td>
				<td class="RightText" width="400">&nbsp;
<%			if (strMode.equals("C"))
			{ 
%>				<gmjsp:calendar textControlName="Txt_CloseDate" textValue="<%=dtClosureDate==null?null:new java.sql.Date(dtClosureDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
<%			}else{
%>				<%=strCloseDate%>
<%			}
%>
				</td></tr>			

			<tr>
				<td class="RightText" align="right" HEIGHT="24"><fmtNCMRUpdate:message key="LBL_CLS_OUT"/>:</td>
				<td class="RightText" width="400">&nbsp;<%=strCloseoutName%></td>
			</tr>
			<tr><td colspan="2" HEIGHT="1" class="Line"></td></tr>
<%		}
%>

<%		if (strNCMRList.equals("YES"))
		{			
%>			<input type="hidden" name="hNCMRLIST" value="<%=strNCMRList%>">
			<tr><td colspan="2">
				<jsp:include page="/common/GmIncludeLog.jsp" flush="true" >
				<jsp:param name="LogType" value="CHANGE" />
				</jsp:include>
			</td></tr>  
<%		}
%>		</table>
<%
}
%>
<%		if (strAction.equals("UpdateNCMREVAL")){
%>		
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr class="ShadeRightTableCaption">
			
		</tr>
	<tr><td width="360">
		<select name="Cbo_Action" align="right" class="center" onFocus="changeBgColor(this,'#AACCE8');" 
			onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtNCMRUpdate:message key="LBL_CRT_NCMR"/></option>
			<option value="1" ><fmtNCMRUpdate:message key="LBL_CRC_NCMR"/></option>
		</select>
		</td>
		<td HEIGHT="30" align="left">&nbsp;<fmtNCMRUpdate:message key="BTN_SUBMIT" var="varSubmit"/>
		<gmjsp:button value="${varSubmit}" name="Btn_Submit" 
					gmClass="button" onClick="return fnSubmit4();" buttonType="Save" /></td>
		</tr>
		<tr><td colspan="2" HEIGHT="1" class="Line"></td></tr>	
		</table>	
<%		} if (!strAction.equals("UPD") && !strMode.equals("E")){
%>		
		<tr>
					<td HEIGHT="30" align="center" colspan="2"><fmtNCMRUpdate:message key="BTN_SUBMIT" var="varSubmit"/>
					<gmjsp:button value="${varSubmit}" name="Btn_Update" 
					gmClass="button" onClick="fnUpdate();" buttonType="Save" /></td>
			</tr>
			<tr><td colspan="2" HEIGHT="1" class="Line"></td></tr>	
<%		}
%>		</table>
	</td></tr>
	<tr><td bgcolor="#666666"></td></tr>
	</table>
	
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
