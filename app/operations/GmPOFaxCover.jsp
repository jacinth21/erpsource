<!--  \operations\GmPOFaxCover.jsp-->
 <%@page import="java.util.Date"%>
<%
/**********************************************************************************
 * File		 		: GmPOFaxCover.jsp
 * Desc		 		: This screen is used for the Fax Cover sheet in PO
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>


<%@ taglib prefix="fmtPOFaxCover" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPOFaxCover:setLocale value="<%=strLocale%>"/>
<fmtPOFaxCover:setBundle basename="properties.labels.operations.GmPOFaxCover"/>






<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmPODetails = new HashMap();

	String strShade = "";

	String strPOId = "";
	String strVendorName = "";
	String strVendorFirstName = "";
	String strPODate = "";
	String strFax = "";
	String strPhone = "";
	String strUserPhone = "";
	String strEmail = "";
	String strUser = "";
	String strUserName = "";
	
	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid() ,"2000");
	String strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("LOGO"));
	String strCompanyName = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPNAME"));
	String strAddress = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMPADDRESS"));
	String strGMphone = GmCommonClass.parseNull((String)hmCompanyAddress.get("PH"));
	String strGMFax = GmCommonClass.parseNull((String)hmCompanyAddress.get("PO_FAX"));
	if(!strAddress.equals("")){
		strAddress = strAddress.replaceAll("/", "<br>");
	}
	strAddress = "<b>" + strCompanyName + "</b>" + "<br>" + strAddress;
	strAddress = (!strGMphone.equals(""))? strAddress+"<br><font size=\"-2\">"+strGMphone+"</font>": strAddress;
	strAddress = (!strGMFax.equals(""))? strAddress+" | <font size=\"-2\">"+strGMFax+"</font>": strAddress; 

	if (hmReturn != null)
	{
		hmPODetails = (HashMap)hmReturn.get("FAX");
		strPOId = (String)hmPODetails.get("POID");
		strVendorName = (String)hmPODetails.get("VPERSON");
		strPODate = GmCommonClass.getStringFromDate((Date)hmPODetails.get("PDATE"),strGCompDateFmt);
		strUser = (String)hmPODetails.get("ID");
		strFax = (String)hmPODetails.get("VFAX");
		strPhone = (String)hmPODetails.get("VPHONE");
		strUserPhone = (String)hmPODetails.get("UPHONE");
		strUser = strUser.concat(".gif");
		strUserName = (String)hmPODetails.get("UNAME");
		strEmail = (String)hmPODetails.get("EMAIL");
	}
	String[] strArray = strVendorName.split(" ");
	strVendorFirstName = strArray[0];

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: PO FAX Cover </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="40" onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder">
<BR><BR>
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr><td bgcolor="#666666" height="1" colspan="6"></td></tr>
					<tr>
						<td width="170" height="70"><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
						<td class="RightText" nowrap><%=strAddress %>
						<td>
						<td align="right" class="RightText"><font size="+5"><b><fmtPOFaxCover:message key="LBL_FAX"/></b></font>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
				</table>
			</td>
			<td bgcolor="#666666" width="1" rowspan="6"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" ></td><tr>
		<tr><td height="30" class="RightText" align="center"><font size="+1"><b><u><fmtPOFaxCover:message key="LBL_CONFIDENTIAL"/></u></b></font></td><tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table cellspacing="0" cellpadding="0" width="100%" border="0">
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td><tr>
					<tr>
						<td height="28" width="100" align="right" class="RightTableCaption"><fmtPOFaxCover:message key="LBL_TO"/>:</td>
						<td width="200" class="RightText">&nbsp;<%=strVendorName%></td>
						<td width="100" align="right" class="RightTableCaption"><fmtPOFaxCover:message key="LBL_FROM"/>:</td>
						<td width="200" class="RightText">&nbsp;<%=strUserName%></td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td><tr>
					<tr>
						<td height="28" width="100" align="right" class="RightTableCaption"><fmtPOFaxCover:message key="LBL_FAX"/>:</td>
						<td width="200" class="RightText">&nbsp;<%=strFax%></td>
						<td width="100" align="right" class="RightTableCaption"><fmtPOFaxCover:message key="LBL_PAGES"/>:</td>
						<td width="200" class="RightText">&nbsp;___&nbsp; <fmtPOFaxCover:message key="LBL_INCLUDING_COVER"/></td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td><tr>
					<tr>
						<td height="28" width="100" align="right" class="RightTableCaption"><fmtPOFaxCover:message key="LBL_PHONE"/>:</td>
						<td width="200" class="RightText">&nbsp;<%=strPhone%></td>
						<td width="100" align="right" class="RightTableCaption"><fmtPOFaxCover:message key="LBL_DATE"/>:</td>
						<td width="200" class="RightText">&nbsp;<%=strPODate%></td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td><tr>
					<tr>
						<td height="28" width="100" align="right" class="RightTableCaption"><fmtPOFaxCover:message key="LBL_RE"/>:</td>
						<td width="200" class="RightText">&nbsp;<fmtPOFaxCover:message key="LBL_PURCHASE_ORDER"/></td>
						<td width="100" align="right" class="RightTableCaption"><fmtPOFaxCover:message key="LBL_CONFIDENTIAL"/>:</td>
						<td width="200" class="RightText">&nbsp;</td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td><tr>
					<tr>
						<td height="40" class="RightTableCaption" colspan="4">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;<fmtPOFaxCover:message key="LBL_URGENT"/>
						&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;<fmtPOFaxCover:message key="LBL_FOR_REVIEW"/>
						&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;<fmtPOFaxCover:message key="LBL_PLEASE_RECONFIRM"/>
						&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;<fmtPOFaxCover:message key="LBL_PLEASE_COMMENT"/>
						&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;<fmtPOFaxCover:message key="LBL_PLEASE_REPLY"/>
						</td>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#cccccc"></td><tr>
					<tr>
						<td colspan="4" class="RightText"><BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<b><%=strVendorFirstName%></b>,
					<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<fmtPOFaxCover:message key="LBL_I_AM_AM_FAXING_PO"/> <b><%=strPOId%></b><fmtPOFaxCover:message key="LBL_ALONG_WITH_THE_WORK_ORDERS"/> .
					<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<fmtPOFaxCover:message key="LBL_CALL_ME_AT"/> <%=strUserPhone%><fmtPOFaxCover:message key="LBL_OR_EMAIL_ME_AT"/>  <%=strEmail%> <fmtPOFaxCover:message key="LBL_IF_I_CAN_BE"/>
					<BR><BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<fmtPOFaxCover:message key="LBL_ANY_FIRTHER_ASSISTANCE"/>.
					<BR><BR><BR><BR><BR><BR>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><%=strUserName%></b>
					<BR><BR><BR><BR><font size="-2">
					<fmtPOFaxCover:message key="LBL_PLS_NOTIFY_BY_FAX_OR_TELEPHONE_OR_FAX"/>

</font>
					</td><tr>
				</table>
			</td>
		</tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>

    </table>

	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" id="button">
			<fmtPOFaxCover:message key="LBL_PRINT" var="varPrint"/>
				<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
				<fmtPOFaxCover:message key="LBL_CLOSE" var="varClose"/>
				<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" onClick="window.close()" buttonType="Load" />&nbsp;&nbsp;
			</td>
		<tr>
	</table>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
