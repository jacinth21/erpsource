<%
/*********************************************************************
 * File		 		: GmDonorLotReport.jsp
 * Desc		 		: Lot Report details based on the Donor Number
 * Version	 		: 1.0
 * author			: HReddi
*********************************************************************/
%>
<!-- \operations\purchasing\operations\GmDonorLotReport.jsp-->  
<%@ include file="/common/GmHeader.inc"%>
<bean:define id="donorId" name="frmDonorLotRpt" property="donorId" type="java.lang.String"> </bean:define>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ taglib prefix="fmtDonorLotInfo" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<fmtDonorLotInfo:setLocale value="<%=strLocale%>"/>
<fmtDonorLotInfo:setBundle basename="properties.labels.operations.GmDonorLotReport"/>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("DONOR_LOT_REPORT"));
String strScreen = GmCommonClass.parseNull((String)request.getAttribute("hscreen"));
//The following code added for passing the company info to the child js
String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: Donor Lot Information Details </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmDonorLotReport.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>

<script>
var companyInfoObj = '<%=strCompanyInfo%>';
var screen = '<%=strScreen%>';
</script>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
	<html:form action="/gmDonorLotRptAction.do?method=loadDonorLotInfo">
	<html:hidden property="strOpt" name="frmDonorLotRpt"/>
		<table class="DtTable1200" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader">&nbsp;<fmtDonorLotInfo:message key="TD_DONOR_LOT_RPT_HEADER"/></td>
				<fmtDonorLotInfo:message key="TD_HELP" var="varHelp"/>
				<td align="right" class="RightDashBoardHeader"><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
						title='${varHelp}' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>					
			<tr>
				<td colspan="3">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="RightTableCaption" HEIGHT="30" align="right" width="15%">
							<fmtDonorLotInfo:message key="LBL_DONOR_NUM" var="varDonorNum"/>
							<gmjsp:label type="RegularText" SFLblControlName="${varDonorNum}:" td="false" /></td>								
							<td width="8%">&nbsp;<html:text property="donorId" styleClass="InputArea" name="frmDonorLotRpt" value='<%=donorId %>' onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="1" size="15"/>&nbsp;
							</td>
							<fmtDonorLotInfo:message key="BTN_LOAD" var="varLoad"/>
							<td width="20%"><gmjsp:button value="&nbsp;${varLoad}&nbsp;"  onClick="javascript:fnLoad();" buttonType="Load" tabindex="2"/></td>							
						</tr>
					</table>
				</td>
			</tr>
			<tr id="DivGridData">
				<td colspan="3"><div id="DonorLotData" width="1200px"></div>
				</td>
			</tr>
			<tr id="DivNoDataMessage"><td colspan="3" height="20" align="Center"><fmtDonorLotInfo:message key="TD_NO_DATA_FOUND"/></td></tr>
			<tr id="DivNothingMessage"><td colspan="3"  align="Center"><fmtDonorLotInfo:message key="TD_NOTHING_FOUND"/></td></tr>
			<tr id="DivPrintButton">
				<fmtDonorLotInfo:message key="BTN_PRINT" var="varPrint"/>
				<fmtDonorLotInfo:message key="BTN_CLOSE" var="varClose"/>
				<td colspan="3" height="30" align="center" style="width:100%;display:inline-block;margin-top: 8px;"><gmjsp:button value="&nbsp;${varPrint}&nbsp;"  onClick="javascript:fnPrintReport();" buttonType="Load"/>
				<%if(strScreen.equals("popup")){ %>
					&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varClose}&nbsp;"  onClick="javascript:window.close();" buttonType="Load"/>
				<%} %>
				</td>
			</tr>
			<tr id="DivExportExcel">
				<td colspan="3" align="center" style="width:100%;display:inline-block;">
			    	<div class='exportlinks'><fmtDonorLotInfo:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="fnDownloadXLS();"><fmtDonorLotInfo:message key="DIV_EXCEL"/></a></div>
				</td>
			</tr>			
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>