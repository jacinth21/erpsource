 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@page import="java.util.Date"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%
/**********************************************************************************
 * File		 		: GmDHRReportByVendor.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Agilan S
************************************************************************************/
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtDHRList" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRReportByVendor.jsp -->

<fmtDHRList:setLocale value="<%=strLocale%>"/>
<fmtDHRList:setBundle basename="properties.labels.operations.GmDHRList"/>

<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	String strGridXmlData = GmCommonClass.parseNull((String)request.getAttribute("XMLGRIDDATA"));
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strhMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strhOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	String strhFromPage = GmCommonClass.parseNull((String)request.getAttribute("hFromPage"));
	String strWikiTitle = GmCommonClass.getWikiTitle("DASHBOARD_GENERATE_DHR");
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.operations.GmDHRList", strSessCompanyLocale);
	
	String strShade = "";
	String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DASHBOARD_GENERATED_DHR"));

	String strDHRId = GmCommonClass.parseNull((String)request.getAttribute("hDHRId"));
	String strVendorId = (String)request.getAttribute("hVendId")==null?"":(String)request.getAttribute("hVendId");
	String strPartNum = "";
	String strPartDesc = "";
	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");
	String strPartNums = (String)request.getAttribute("hPartNums")==null?"":(String)request.getAttribute("hPartNums");
	String strRollBackAccess = GmCommonClass.parseNull((String)(String)request.getAttribute("strRollBackAccess"));

	int intSize = 0;
	HashMap hcboVal = null;
	ArrayList alVendorList = new ArrayList();
	if(hmReturn != null){
	ArrayList alList = (ArrayList)hmReturn.get("DHRLIST");
	intSize = alList.size();
	}
	if (strhMode.equals("Report"))
	{
		alVendorList = (ArrayList)session.getAttribute("VENDORLIST");
	}
	String strSelected = "";
	String strCodeID = GmCommonClass.parseNull((String) request.getAttribute("hVendId"));
	strCodeID = (String)request.getAttribute("hVenId")==null?"":(String)request.getAttribute("hVenId");
	String strCodeNM = GmCommonClass.parseNull((String) request.getAttribute("hVendNM"));
	
	if (strhMode.equals("Report"))
	{
		strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_MODIFY_DHR"));
		strWikiTitle = GmCommonClass.getWikiTitle("DASHBOARD_MODIFY_DHR");
		if(strhFromPage.equals("ByVendor")){
			strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_DHR_REPORT_BY_VENDOR"));
			strWikiTitle = GmCommonClass.getWikiTitle("DHR_REPORT_BY_VENDOR");
		}
	}
	String strMODE = (String)session.getAttribute("MODE")==null?"":(String)session.getAttribute("MODE");
	
	String strPnum = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	String strControlNum = GmCommonClass.parseNull((String)request.getAttribute("hControlNum"));
	String strSessApplDateFmt= strGCompDateFmt;
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);	
	log.debug("strhMode==>"+strhMode+" strMODE==>"+strMODE);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR List </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmDHRList.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var gStrServletPath = "<%=strServletPath%>";
var gStrOpt = "<%=strhOpt%>";
var rollBackAccess = '<%=strRollBackAccess%>';
var gCmpDateFmt = "<%=strGCompDateFmt%>";
var companyInfoObj = '<%=strCompanyInfo%>';
</script>
<script>
var strVendorId = '<%=strVendorId%>'
function fnLoad()
{
	document.frmVendor.hVendorId.value = strVendorId;
}
var objGridData;
objGridData ='<%=strGridXmlData%>';
var gridObj;
function fnOnPageLoad() {
	
	if (objGridData.value != '') {
		gridObj = initGridData('DHRVendorRpt', objGridData);
	}
}
function initGridData(divRef, gridData) {
	var gObj = new dhtmlXGridObject(divRef);
	gObj.setSkin("dhx_skyblue");
	gObj.setImagePath("/extweb/dhtmlx/dhtmlxGrid/imgs/");
	gObj.init();
	gObj.loadXMLString(gridData);
    gObj.attachHeader('#text_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter');   
   // gObj.groupBy(0);
	gObj.enableTooltips("false,false,false,false,false,false,false,false,false,false,false,false,false");
    return gObj;
}

//To download excel;
function fnExcel() { 
	gridObj.toExcel('/phpapp/excel/generate.php');
}
</script>
</HEAD>

<BODY leftmargin="15" topmargin="10" onload="fnOnPageLoad();fnLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hDHRId" value="">
<input type="hidden" name="hVenId" value="<%=strVendorId%>">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">
<input type="hidden" name="hFromPage" value="<%=strhFromPage%>">
	<table border="0" class="DtTable1200" cellspacing="0" cellpadding="0">		
		<tr>
			<td colspan="7">
				<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
					<tr>
						<td height="25" colspan="6"  class="RightDashBoardHeader"> <%=strHeader%></td>
						<td height="25" class="RightDashBoardHeader" align="right"><fmtDHRList:message key="IMG_ALT_HELP" var = "varHelp"/>
						<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
					</tr>
			</table> 
		 	</td> 
		 </tr> 
<%
		if (strhMode.equals("Report"))
		{
%>
		<tr class="shade">
			 
			<td class="RightTableCaption" align="right" HEIGHT="30"><fmtDHRList:message key="LBL_DHR_ID"/>:&nbsp;</td>
			<td class="RightText">
			    <input type="text" size="20" value="<%=strDHRId%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_DHRID">&nbsp;&nbsp;
			</td>  
			<td class="RightTableCaption" align="right" HEIGHT="30"><fmtDHRList:message key="LBL_PART_NUMBER"/>:&nbsp;</td>
			<td class="RightText">
			    <input type="text" size="20" value="<%=strPnum%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_PartNum">&nbsp;&nbsp;
			</td> 
			<td class="RightTableCaption" align="right" HEIGHT="30"><fmtDHRList:message key="LBL_CONTROL_NUMBER"/>:&nbsp;</td>
			<td class="RightText">
			    <input type="text" size="20" value="<%=strControlNum%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_ControlNum">&nbsp;&nbsp;
			</td>   
			 <td> </td> 
		</tr>
		<tr><td colspan="7" height="1" bgcolor="#cccccc"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr  >
			<td class="RightTableCaption" align="right" HEIGHT="30"><fmtDHRList:message key="LBL_VENDOR_NAME"/>:&nbsp;</td>
			<td> <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
				<jsp:param name="CONTROL_NAME" value="hVendorId" />
				<jsp:param name="METHOD_LOAD" value="loadVendorNameList&searchType=LikeSearch" />
				<jsp:param name="WIDTH" value="300" />
				<jsp:param name="CSS_CLASS" value="search" />
				<jsp:param name="TAB_INDEX" value="3"/>
				<jsp:param name="CONTROL_NM_VALUE" value="<%=strCodeNM %>" /> 
				<jsp:param name="CONTROL_ID_VALUE" value="<%=strCodeID %>" />
 				<jsp:param name="SHOW_DATA" value="50" />
 				<jsp:param name="AUTO_RELOAD" value="" />                                       
 	 	 	    </jsp:include>  

			</td>
			<td class="RightTableCaption" HEIGHT="24" align="right"><fmtDHRList:message key="LBL_FROM_DATE"/>:&nbsp;</td>
			<td class="RightTableCaption"><gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(strFromDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFromDt,strSessApplDateFmt).getTime())))%>"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/></td>
			<td class="RightTableCaption" HEIGHT="24" align="right"><fmtDHRList:message key="LBL_TO_DATE"/> :&nbsp; </td>
			<td class="RightTableCaption"><gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(strToDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDt,strSessApplDateFmt).getTime())))%>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/></td>
			<td class="RightTableCaption"><fmtDHRList:message key="BTN_GO" var="varGo"/>
			<gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" onClick="javascript:fnGoDHRList();" tabindex="25" buttonType="Load" />
			</td> 
		</tr>
		 
		<tr><td colspan="7" class="Line" height="1"></td></tr>
<%
		}
%>
<%-- <%if(!strGridXmlData.equals("")){ %>  --%>

<% if (strGridXmlData.indexOf("cell") != -1) { %>
<tr>
				<td colspan="7">
					<!-- <div id="DHRVendorRpt" style="height: 1030px; width:100%"></div> -->
					
					 <div id="DHRVendorRpt" style="height:640px;"></div>
				</td>			
		</tr>
     	<tr><td colspan="7" height="1" bgcolor="#cccccc"></td></tr>
     		<tr>
			<td colspan="7" align="center">
			    <div class='exportlinks'><fmtDHRList:message key="LBL_EXPORT_OPTS"/> : <img src='img/ico_file_excel.png' onclick="fnExcel();"/>&nbsp;<a href="#" onclick="fnExcel();"><fmtDHRList:message key="LBL_EXCEL"/></a></div>
			</td>
		</tr>

		<% }else{%>
		 <tr>
		 <td height="30" colspan="7" align="center" class="RightTextBlue"><fmtDHRList:message key="LBL_NO_DIS"/>.</td>
		</tr>
		<%} %>	

    </table>

</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
