 <%
/**********************************************************************************
 * File		 		: GmPartNumBulkInvReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtPartNumBulkInvReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtPartNumBulkInvReport:setLocale value="<%=strLocale%>"/>
<fmtPartNumBulkInvReport:setBundle basename="properties.labels.operations.GmPartNumBulkInvReport"/>
<!-- operations\GmPartNumBulkInvReport.jsp -->

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmLoop = new HashMap();
	HashMap hcboVal = new HashMap();

	String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	String strPartNum = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	String strSearch = (String)request.getAttribute("hSearch")==null?"":(String)request.getAttribute("hSearch");
	
	String strPartDesc = "";
	String strQtyInBulk = "";
	String strQtyInBulkAlloc = "";
	String strQtyInReturnsHold = "";
	String strQtyInWIP = "";

	String strProjName = "";

	String strShade = "";
	String strCodeID = "";
	String strSelected = "";
	int intLoop = 0;

	ArrayList alProject = new ArrayList();
	alProject = (ArrayList)session.getAttribute("PROJLIST");

	ArrayList alSets = new ArrayList();
	ArrayList alReport = new ArrayList();

	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("REPORT");
	}

	int intProjLength = alProject.size();
	int intSetLength = alSets.size();
	if (alReport != null)
	{
		intLoop = alReport.size();
	}

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Bulk Inventory Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnSubmit()
{
	var pnum = document.frmAccount.hPartNum.value;
	if (pnum != '')
	{
		document.frmAccount.hOpt.value = "MultipleParts";
		document.frmAccount.Cbo_ProjId.selectedIndex = 0;
	}
	
	document.frmAccount.hAction.value = "GoBulk";
	fnStartProgress('Y');
	document.frmAccount.submit();
}

var prevtr = 0;

function changeTRBgColor(val,BgColor) {
   var obj1 = eval("document.all.tr"+prevtr);
   obj1.style.background = "";
   var obj = eval("document.all.tr"+val);
   prevtr = val;
   obj.style.background = BgColor;
}

function fnViewDetails(pnum,act)
{
windowOpener("/GmPartInvReportServlet?hAction="+act+"&hPartNum="+encodeURIComponent(pnum),"INVPOP","resizable=yes,scrollbars=yes,top=300,left=300,width=670,height=200");
}

function fnPrintVer(projid)
{
	windowOpener("/GmPartInvReportServlet?hAction=Print&hProjId="+projid,"INVPRINT","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");
}

function fnDownloadVer(projid)
{
	windowOpener("/GmPartInvReportServlet?hAction=Excel&hProjId="+projid,"INVEXCEL","resizable=yes,scrollbars=yes,top=300,left=200,width=800,height=490");
}

function fnLoad()
{
	var sel = document.frmAccount.hSearch.value;
	if (sel != '')
	{
		document.frmAccount.Cbo_Search.value = sel;
	}
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"  onLoad="javascript:fnLoad();">
<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPartInvReportServlet">
<input type="hidden" name="hId" value="<%=strProjId%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hSearch" value="<%=strSearch%>">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="20" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="20" width="1" class="Line"></td>
		</tr>
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">
				Report - Bulk Inventory Report
			</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>		
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartNumBulkInvReport:message key="LBL_PART_NUM"/>:</td>
			<td>&nbsp;<input type="text" size="57" value="<%=strPartNum%>" name="hPartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3>
							
			&nbsp;&nbsp;<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
							<option value="0" ><fmtPartNumBulkInvReport:message key="LBL_CHOOSE"/>
							<option value="LIT" ><fmtPartNumBulkInvReport:message key="LBL_LITREAL"/>
							<option value="LIKEPRE" ><fmtPartNumBulkInvReport:message key="LBL_PRE"/>
							<option value="LIKESUF" ><fmtPartNumBulkInvReport:message key="LBL_SUF"/>
						</select>
			</td>
		</tr>		
		<tr>
			<td class="RightText" align="right" HEIGHT="24">&nbsp;<fmtPartNumBulkInvReport:message key="LBL_PROJECT_LIST"/>:</td>
			<td>&nbsp;<select name="Cbo_ProjId" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
					<option value="0" ><fmtPartNumBulkInvReport:message key="LBL_CHOOSE"/>
<%
					hcboVal = new HashMap();
			  		for (int i=0;i<intProjLength;i++)
			  		{
			  			hcboVal = (HashMap)alProject.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strProjName = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
						strSelected = strProjId.equals(strCodeID)?"selected":"";
%>
					<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeID%> / <%=strProjName%></option>
<%
			  		}
%>

			</select>
			&nbsp;
			<gmjsp:button value="&nbsp;&nbsp;Go&nbsp;&nbsp;" name="Btn_Go" gmClass="button" onClick="javascript:fnSubmit()" tabindex="3" buttonType="Load" />			
			</td>
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>
		<tr>
			<td colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr bgcolor="#eeeeee" class="RightTableCaption">
						<td HEIGHT="24" align="center" width="60"><fmtPartNumBulkInvReport:message key="LBL_PART_NUM"/></td>
						<td width="350">&nbsp;<fmtPartNumBulkInvReport:message key="LBL_DESC"/></td>
						<td colspan="2" align="center"><fmtPartNumBulkInvReport:message key="LBL_BULK_INV"/><BR></td>
						<td width="100" align="center"><fmtPartNumBulkInvReport:message key="LBL_QTY_RETRN"/><br></td>
					</tr>
					<tr><td class="Line" colspan="6"></td></tr>
					<tr bgcolor="#eeeeee" class="RightTableCaption">
						<td colspan="2">&nbsp;</td>
						<td width="60" align="center"><fmtPartNumBulkInvReport:message key="LBL_WIP_SETS"/></td>
						<td width="60" align="center"><fmtPartNumBulkInvReport:message key="LBL_LOOSE"/></td>
						<td>&nbsp;</td>
					</tr>
					<tr><td class="Line" colspan="5"></td></tr>					
					<tr>
						<td colspan="6">
							<div style="display:visible;width: 697px; height: 477px; overflow: auto;">
								<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
			if (intLoop > 0)
			{
				int intStock = 0;
				int intBulk = 0;
				int intReturns = 0;
				int intPO = 0;
				int intDHR = 0;
				
				for (int i = 0;i < intLoop ;i++ )
				{
					hmLoop = (HashMap)alReport.get(i);
					strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
					strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));

					strQtyInBulk = GmCommonClass.parseZero((String)hmLoop.get("INBULK"));
					intBulk = Integer.parseInt(strQtyInBulk);
					strQtyInBulkAlloc = GmCommonClass.parseZero((String)hmLoop.get("BULKALLOC"));
					
					strQtyInReturnsHold = GmCommonClass.parseZero((String)hmLoop.get("INRETURNS"));
					
					strQtyInWIP = GmCommonClass.parseZero((String)hmLoop.get("FAWIP"));
					intDHR = Integer.parseInt(strQtyInWIP);
					
					intBulk = intBulk - intDHR;
					strQtyInBulk = ""+intBulk;
%>
									<tr id="tr<%=i%>" onClick="changeTRBgColor(<%=i%>,'#AACCE8');">
										<td width="60" height="20" class="RightText">&nbsp;<%=strPartNum%></td>
										<td width="350" class="RightTextAS">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
										<td width="60" class="RightText" align="center"><%=strQtyInWIP%></td>
										<td width="60" class="RightText" align="center"><%=strQtyInBulk%></td>
										<td width="140" class="RightText" align="center"><%=strQtyInReturnsHold%></td>
									</tr>
									<tr><td colspan="5" bgcolor="#eeeeee" height="1"></td></tr>
<%
				}
			} else	{
%>
					<tr>
						<td height="30" colspan="6" align="center" class="RightTextBlue"><fmtPartNumBulkInvReport:message key="LBL_NO_PART"/> !</td>
					</tr>
<%
		}
%>
								</table>
							</DIV>
						</td>
					</tr>
					<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
				</table>
			</td>
		</tr>
	</table>
		
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
