<%
/**********************************************************************************
 * File		 		: GmDonorParam.jsp
 * Desc		 		: This JSP is for the section Donor Parameters
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtDonorParam" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDonorParam.jsp -->

<fmtDonorParam:setLocale value="<%=strLocale%>"/>
<fmtDonorParam:setBundle basename="properties.labels.operations.GmDonorParam"/>

<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>

<bean:define id="hMode" name="<%=strFormName%>" property="hMode" type="java.lang.String"> </bean:define>
<bean:define id="strDhrstatus" name="<%=strFormName%>" property="dhrstatus" type="java.lang.String"> </bean:define>
<bean:define id="screenType" name="frmDonorInfo" property="screenType" type="java.lang.String" ></bean:define>
<bean:define id="submitAccess" name="frmDonorInfo" property="submitAccess" type="java.lang.String" ></bean:define>
<bean:define id="hAccess" name="frmDonorInfo" property="hAccess" type="java.lang.String" ></bean:define>


<%
Boolean blEditCtrlNum;
// 3: Pending Verification, 4: Verified
blEditCtrlNum =   strDhrstatus.equals("4") ? false : true;

// Submit Button should be enable only for Lot Number Override Transaction Screen
String strBtnDisable = "false";
if(submitAccess.equals("lotNumberOverrideRpt") || screenType.equals("lotNumberOverrideRpt") || screenType.equals("popup")){
	strBtnDisable = "true";
}
%>

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: Donor Details </TITLE>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmDonorInfo.js"></script>

<script>
var strSubmitAccess = '<%=submitAccess%>';
var screenType = '<%=screenType%>';
</script>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmDonorInfo.do?method=fetchDonorParamDtls">
<html:hidden property="hDonorNum" name="frmDonorInfo" />
<html:hidden property="hVendorId" name="frmDonorInfo"/>
<html:hidden property="dhrstatus" name="frmDonorInfo"/>
<html:hidden property="hAccess" name="frmDonorInfo"/>
	<table class="DtTable500" border="0" cellspacing="0" cellpadding="0">
		
		<tr>
			<td height="24" class="ShadeRightTableCaption" colspan="2">&nbsp;<b><fmtDonorParam:message key="LBL_DONOR_PARAMETER"/></b></td>
			
		</tr>
		<tr><td class="Line" colspan="2"></td></tr>
		<%
		if((blEditCtrlNum && !screenType.equals("NcmrUpdate")) || (!hMode.equals("Report") && !hMode.equals("DHRSplitReport"))){
		%> 
			<tr class="Shade">
				<td class="RightText" HEIGHT="20"  align="right" width="40%">&nbsp;<b><fmtDonorParam:message key="LBL_DONOR_ID"/>:</b></td>
				<td width="60%">&nbsp;<bean:write name="<%=strFormName %>" property="donorNo"/></td>
			</tr>
			<tr>
				<td class="RightText" HEIGHT="20"  align="right" width="40%">&nbsp;<b><fmtDonorParam:message key="LBL_AGE"/>:</b></td>
				<td width="60%">&nbsp;<html:text property="age" styleClass="InputArea" name="<%=strFormName %>" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" tabindex="1" size="12"/></td>
			</tr>
			<tr><td class="LLine" colspan="2"></td></tr> 	
			<tr  class="Shade">
				<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtDonorParam:message key="LBL_SEX"/>:</b></td>
				<td>&nbsp;<gmjsp:dropdown controlName="sex" SFFormName="<%=strFormName %>" SFSeletedValue="sex"
							SFValue="alGender" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="2"/></td>
			</tr>
			<tr><td class="LLine" colspan="2"></td></tr> 	
			<tr>
				<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtDonorParam:message key="LBL_INTERNATIONAL_USE"/>:</b></td>
				<td>&nbsp;<gmjsp:dropdown controlName="internationalUse" SFFormName="<%=strFormName %>" SFSeletedValue="internationalUse"
							SFValue="alInternationalUse" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="3"/></td>
			</tr>
			<tr><td class="LLine" colspan="2"></td></tr> 	
			<tr class="Shade">
				<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtDonorParam:message key="LBL_FOR_RESEARCH"/>:</b></td>
				<td>&nbsp;<gmjsp:dropdown controlName="forResearch" SFFormName="<%=strFormName %>" SFSeletedValue="forResearch"
							SFValue="alResearch" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" tabIndex="3"/></td>
			</tr>
		<%}else{ %>
			<tr class="Shade">
				<td class="RightText" HEIGHT="20"  align="right" width="40%">&nbsp;<b><fmtDonorParam:message key="LBL_DONOR_ID"/>:</b></td>
				<td width="60%">&nbsp;<bean:write name="<%=strFormName %>" property="donorNo"/></td>
			</tr>
			<tr>
				<td class="RightText" HEIGHT="20"  align="right" width="40%">&nbsp;<b><fmtDonorParam:message key="LBL_AGE"/>:</b></td>
				<td width="60%">&nbsp;<bean:write name="<%=strFormName %>" property="age"/></td>
			</tr>
			<tr><td class="LLine" colspan="2"></td></tr> 	
			<tr  class="Shade">
				<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtDonorParam:message key="LBL_SEX"/>:</b></td>
				<td>&nbsp;<bean:write name="<%=strFormName %>" property="sexCodeName"/></td>
			</tr>
			<tr><td class="LLine" colspan="2"></td></tr> 	
			<tr>
				<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtDonorParam:message key="LBL_INTERNATIONAL_USE"/>:</b></td>
				<td>&nbsp;<bean:write name="<%=strFormName %>" property="internationalUsenm"/></td>
			</tr>
			<tr><td class="LLine" colspan="2"></td></tr> 	
			<tr class="Shade">
				<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtDonorParam:message key="LBL_FOR_RESEARCH"/>:</b></td>
				<td>&nbsp;<bean:write name="<%=strFormName %>" property="forResearchnm"/></td>
			</tr>
		<%} %>
		
		<%if(hMode.equals("recShip") || !screenType.equals("")){ %>
			<tr><td colspan="2"></td></tr>
			<tr><td class="Line" colspan="2"></td></tr>
			<tr><td align="center" height="30" colspan="2">
			<fmtDonorParam:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button name="Btn_Submit" value="${varSubmit}" disabled="<%=strBtnDisable %>" gmClass="button" onClick="javascript:fnParamSubmit(this);" tabindex="10" buttonType="Save" />&nbsp;
			</td></tr>
		<%} %>
			
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>