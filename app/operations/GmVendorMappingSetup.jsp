
<!-- Added GmHeader.inc and removed taglibs for JBOSS migration -->

<%@ include file="/common/GmHeader.inc"%>
<!--operations\GmVendorMappingSetup.jsp  -->
<%@ taglib prefix="fmtVendorMappingSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtVendorMappingSetup:setLocale value="<%=strLocale%>"/>
<fmtVendorMappingSetup:setBundle basename="properties.labels.operations.GmVendorMappingSetup"/>
<bean:define id="strAccessfl" name="frmVendMappingSetup" property="strAccessfl" type="java.lang.String"></bean:define>
<%
/*PMT-26730-->System Manager Changes*/
String strDisable="";
if(!strAccessfl.equals("Y")){
	strDisable = "true";
}
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Vendor Mapping Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
function fnReset()
{
	document.frmVendMappingSetup.vendorType.value = '0';
	document.frmVendMappingSetup.vendorCategory.value = '0';
	document.frmVendMappingSetup.hvendCatMapId.value = '';
	document.frmVendMappingSetup.inActiveFlag.checked = false;
}	

function fnFetch()
{	
	fnReset();
	document.frmVendMappingSetup.submit();
}

function fnEditId(val)
{	
	document.frmVendMappingSetup.strOpt.value = 'edit';
	document.frmVendMappingSetup.hvendCatMapId.value = val;
	document.frmVendMappingSetup.submit();
}

function fnSubmit()
{
    if(document.frmVendMappingSetup.inActiveFlag.checked) {
        document.frmVendMappingSetup.inActiveFlag.value = "Y";
    }
    else 
    {
        document.frmVendMappingSetup.inActiveFlag.value = "";
    }

	document.frmVendMappingSetup.strOpt.value = 'save';
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	fnStartProgress('Y');
	document.frmVendMappingSetup.submit();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/vendorMapping.do">

<html:hidden property="strOpt" value=""/>
<html:hidden property="hvendCatMapId" name="frmVendMappingSetup"/>
<bean:define id="activeFlagValue" name="frmVendMappingSetup" property="inActiveFlag" type="java.lang.String"> </bean:define>
 <!-- Custom tag lib code modified for JBOSS migration changes -->
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtVendorMappingSetup:message key="LBL_VEN_MAP_SET"/></td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"></font>&nbsp;<fmtVendorMappingSetup:message key="LBL_VEN_LIST"/>:</td>
						<td width="70%">&nbsp;
						<gmjsp:dropdown controlName="vendorId" onChange="fnFetch();" SFFormName="frmVendMappingSetup" SFSeletedValue="vendorId"
							tabIndex="1" controlId="vlist"  SFValue="alVendorList" codeId = "VID"  codeName = "VNAME"  defaultValue= "[Choose One]" />													
						</td>
					</tr>
					<tr>
                        <td class="RightTextBlue" colspan="2" align="center"><fmtVendorMappingSetup:message key="LBL_CHOOSE"/></td> 
                    </tr>
                    <tr><td colspan="2" class="Line"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;<fmtVendorMappingSetup:message key="LBL_TYPE"/>:</td> 
                        <td>&nbsp;
                    <gmjsp:dropdown controlName="vendorType"  SFFormName="frmVendMappingSetup" SFSeletedValue="vendorType" 
						tabIndex="1"   SFValue="alVendorType" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />                                                
                        </select>
                        </td>
                    </tr>  
                    <tr><td colspan="2" class="Line"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtVendorMappingSetup:message key="LBL_CATEGORY"/>:</td> 
                        <td>&nbsp;
                        <gmjsp:dropdown controlName="vendorCategory"  SFFormName="frmVendMappingSetup" SFSeletedValue="vendorCategory" 
						tabIndex="1"   SFValue="alVendorCategory" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />                                                                  
                        </td>
                    </tr>       
                    <tr><td colspan="2" class="Line"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtVendorMappingSetup:message key="LBL_INACT_FLAG"/>:</td> 
                        <td>&nbsp;
                        <input type="checkbox" name="inActiveFlag" <%=activeFlagValue.equals("Y")?"checked":""%>>
                         </td>
                    </tr>
                    <tr><td colspan="2" class="Line"></td></tr> 
                    
                    <tr>
                    	<td colspan="2" align="center">
		                    <fmtVendorMappingSetup:message key="BTN_RESET" var="varReset"/><gmjsp:button value="${varReset}" disabled="<%=strDisable%>"  gmClass="button" onClick="fnReset();" buttonType="Save" />
		                    <fmtVendorMappingSetup:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" disabled="<%=strDisable%>"  gmClass="button" onClick="fnSubmit();" buttonType="Save" /> 
		                </td>
                    </tr>
           <tr> 
            <td colspan="2">
            <display:table name="requestScope.RDVENDMAPLIST.rows" requestURI="/vendorMapping.do" excludedParams="strOpt" class="its" id="currentRowObject" decorator="com.globus.displaytag.beans.GmCommonSetupWrapper" > 
			<display:column property="ID" title="" sortable="true" class="alignleft"/>
			<fmtVendorMappingSetup:message key="DT_VEN_TYP" var="varVendorType"/><display:column property="VENDTYPE" title="${varVendorType}" sortable="true" class="alignleft"/> 
			<fmtVendorMappingSetup:message key="DT_VEN_CAT" var="varVenCat"/><display:column property="VENDCAT" title="${varVenCat}" sortable="true"  />
			<fmtVendorMappingSetup:message key="DT_ACT_FLAG" var="varActFlag"/><display:column property="ACTIVEFL" title="${varActFlag}" sortable="true" class="alignleft" />
			</display:table> 
		</td>
    </tr> 
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

