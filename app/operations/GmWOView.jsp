 <%
/**********************************************************************************
 * File		 		: GmWOView.jsp
 * Desc		 		: This screen is used for the Work Order
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%@ taglib prefix="fmtWOView" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmWOView.jsp -->

<fmtWOView:setLocale value="<%=strLocale%>"/>
<fmtWOView:setBundle basename="properties.labels.operations.GmWOView"/>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmWODetails = new HashMap();
	ArrayList alSubWorkOrder = new ArrayList();

	String strShade = "";

	String strWOId = "";
	String strPOId = "";
	String strPartNum = "";
	String strDesc = "";
	String strQty = "";
	String strMatSpec = "";
	String strStartDate = "";
	String strNeedByDate = "";
	String strVendorNm = "";
	String strLotCode = "";
	String strUser = "";
	String strDraw = "";
	String strDrawRev = "";
	
	String strSubPartNum = "";
	String strSubPartDesc = "";
	String strVendorType = "";
	String strFooter = "";
	String strDocRev = "";
	String strDocActiveFl = "";
	String strVALDFL = "";
	String strContent = "";
	String strValidFLContent = "";
	String strValidateFLContent = "";
	
	String strYes = "<td><img src=<%=strImagePath%>/y.gif></td><td><img src=<%=strImagePath%>/n.gif></td><td><img src=<%=strImagePath%>/n.gif></td>";
	String strNo = "<td><img src=<%=strImagePath%>/n.gif></td><td><img src=<%=strImagePath%>/y.gif></td><td><img src=<%=strImagePath%>/n.gif></td>";
	String strNA = "<td><img src=<%=strImagePath%>/n.gif></td><td><img src=<%=strImagePath%>/n.gif></td><td><img src=<%=strImagePath%>/y.gif></td>";
	String strNoData = "<td><img src=<%=strImagePath%>/n.gif></td><td><img src=<%=strImagePath%>/n.gif></td><td><img src=<%=strImagePath%>/n.gif></td>";
	String strInsp = "<td><img src=<%=strImagePath%>/n.gif></td><td><img src=<%=strImagePath%>/y.gif></td><td></td>";
	String strSter = "";
	boolean bolSter = false;
	
	String strCompCertFl = "";
	String strMatCertFl = "";
	String strHardTestFl = "";
	String strLog="";
	String strLog1="";

	if (hmReturn != null)
	{
		hmWODetails = (HashMap)hmReturn.get("WODETAILS");

		strWOId = GmCommonClass.parseNull((String)hmWODetails.get("WID"));
		strPOId = GmCommonClass.parseNull((String)hmWODetails.get("POID"));
		strPartNum = GmCommonClass.parseNull((String)hmWODetails.get("PARTNUM"));
		strDesc = GmCommonClass.parseNull((String)hmWODetails.get("PDESC"));
		strQty = GmCommonClass.parseNull((String)hmWODetails.get("QTY"));
		strMatSpec = GmCommonClass.parseNull((String)hmWODetails.get("MSPEC"));
		strStartDate = GmCommonClass.parseNull((String)hmWODetails.get("CDATE"));
		strNeedByDate = GmCommonClass.parseNull((String)hmWODetails.get("RDATE"));
		strVendorNm = GmCommonClass.parseNull((String)hmWODetails.get("VNAME"));
		strLotCode = GmCommonClass.parseNull((String)hmWODetails.get("LC"));
		strCompCertFl = GmCommonClass.parseNull((String)hmWODetails.get("CCFL"));
		strMatCertFl = GmCommonClass.parseNull((String)hmWODetails.get("MCFL"));
		strHardTestFl = GmCommonClass.parseNull((String)hmWODetails.get("HTFL"));
		strDraw = GmCommonClass.parseNull((String)hmWODetails.get("DRAW"));
		strDrawRev = GmCommonClass.parseNull((String)hmWODetails.get("DRAWREV"));
		strSter = GmCommonClass.parseNull((String)hmWODetails.get("STERFL"));
		strFooter = GmCommonClass.parseNull((String)hmWODetails.get("FOOTER"));
		strLog1 = GmCommonClass.parseNull((String)hmWODetails.get("WLOG"));
		strVALDFL = GmCommonClass.parseNull((String)hmWODetails.get("VALIDATIONFL"));	
		
		if(strVALDFL.equals("Y")){
			strValidFLContent = "\t- <b>("+GmCommonClass.getString("GMVALIDATIONFL")+")</b>";
			strValidateFLContent = "\t<b>("+GmCommonClass.getString("GMVALIDATIONFLCONTENT")+")</b>";
		}else{
			strValidFLContent = "";
			strValidateFLContent ="";
		}
		
		
		String strArr[] = strFooter.split("\\^");
		strFooter = strArr[0];
		strDocRev = strArr[1];
		strDocActiveFl = strArr[2];
		
		alSubWorkOrder = (ArrayList)hmReturn.get("SUBWODETAILS");
	
		if (strCompCertFl.equals("Y"))
		{
			strCompCertFl = strYes;
		}
		else if (strCompCertFl.equals("N"))
		{
			strCompCertFl = strNo;
		}
		else if (strCompCertFl.equals("A"))
		{
			strCompCertFl = strNA;
		}
		else
		{
			strCompCertFl = strNoData;
		}

		if (strMatCertFl.equals("Y"))
		{
			strMatCertFl = strYes;
		}
		else if (strMatCertFl.equals("N"))
		{
			strMatCertFl = strNo;
		}
		else if (strMatCertFl.equals("A"))
		{
			strMatCertFl = strNA;
		}
		else
		{
			strMatCertFl = strNoData;
		}
		
		if (strHardTestFl.equals("Y"))
		{
			strHardTestFl = strYes;
		}
		else if (strHardTestFl.equals("N"))
		{
			strHardTestFl = strNo;
		}
		else if (strHardTestFl.equals("A"))
		{
			strHardTestFl = strNA;
		}
		else
		{
			strHardTestFl = strNoData;
		}
		
		if (strSter.equals("1"))
		{
			strSter = strYes;
			bolSter = true;
			strMatCertFl = strNA;
		}
		else
		{
			strSter = strNA;
		}		
		

		strUser = (String)hmWODetails.get("CUSER");
		strUser = strUser.concat(".gif");
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: WO </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnViewPrint()
{
	vid = 'All';
windowOpener("/GmWOServlet?hAction=PrintWO&hWOId=<%=strWOId%>&hDocRev=<%=strDocRev%>&hDocFl=<%=strDocActiveFl%>","WO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

 function fnOpenOrdLogwo(val)
{
	windowOpener("/GmCommonLogServlet?hType=1237&hID="+val,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmWOServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hAction" value="">

	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" rowspan="20" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="20" width="1"></td>
		</tr>
		<tr>
			<td height="25" colspan="4" class="RightDashBoardHeader"> <fmtWOView:message key="LBL_WORK_ORDER"/></td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td class="RightText" align="right" height="25">
			<%if(strLog1.equals("N"))
					{%>
					
					<fmtWOView:message key="IMG_ALT_COMMENTS" var = "varComments"/>
					<img style='cursor:hand' width='22' height='20' src='<%=strImagePath%>/phone_icon.jpg' 
					title='${varComments}' border='0' onClick="javascript:fnOpenOrdLogwo('<%=strWOId%>');" >
					<%}
			else
			{%>
			        <fmtWOView:message key="IMG_ALT_COMMENTS" var = "varComments"/>
					<img style='cursor:hand' width='22' height='20' src='<%=strImagePath%>/phone-icon_ans.gif' 
					title='${varComments}' border='0' onClick="javascript:fnOpenOrdLogwo('<%=strWOId%>');" >
		   <%} %>
			
			<B><fmtWOView:message key="LBL_WORK_ORDER_#"/></B>:</td>
			<td class="RightText" width="220">&nbsp;<%=strWOId%><%=strValidFLContent %></td>
			<td class="RightText" align="right"><B><fmtWOView:message key="LBL_PO#"/></B>:</td>
			<td class="RightText">&nbsp;<%=strPOId%></td>
		<tr>
		<tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr>
			<td class="RightText"  height="25" align="right"><B><fmtWOView:message key="LBL_PART_NUMBER"/></B>:</td>
			<td class="RightText">&nbsp;<%=strPartNum%></td>
			<td class="RightText"  height="25" align="right"><B><fmtWOView:message key="LBL_DESCRIPTION"/></B>:</td>
			<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
		</tr>
		</tr><td colspan="4" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr>
			<td class="RightText" height="25" align="right"><B><fmtWOView:message key="LBL_START_DATE"/></B>:</td>
			<td class="RightText">&nbsp;<%=strStartDate%></td>
			<td class="RightText" align="right"><B><fmtWOView:message key="LBL_NEED_BY"/></B>:</td>
			<td class="RightText">&nbsp;<%=strNeedByDate%></td>
		</tr>
		<tr>
			<td colspan="4" height="1" bgcolor="#CCCCCC"></td>
		</tr>
		<tr>
			<td class="RightText" height="25"  align="right"><B><fmtWOView:message key="LBL_VENDOR"/></B>:</td>
			<td class="RightText">&nbsp;<%=strVendorNm%></td>
			<td class="RightText" align="right"><B><fmtWOView:message key="LBL_CONTROL_#"/></B>:</td>
			<td class="RightText">&nbsp;<%=strLotCode%>[A-Z][0-3][0-9][0-9][A-Z]<%=strDrawRev%></td>
		</tr>
		</tr>
			<td colspan="4" height="1" bgcolor="#CCCCCC"></td>
		</tr>
		<tr>
			<td class="RightText"  align="right"><B><fmtWOView:message key="LBL_QUANTITY"/></B>:</td>
			<td class="RightText">&nbsp;<%=strQty%><%=strValidateFLContent %></td>
			<td colspan="2" rowspan="7" align="center">
				<table width="270" border="0" cellspacing="0">
					<tr>
						<td class="RightText" align="right">&nbsp;</td>
						<td class="RightText" width="30" align=center><fmtWOView:message key="LBL_YES"/></td>
						<td class="RightText" width="30" align=center><fmtWOView:message key="LBL_NO"/></td>
						<td class="RightText" width="30" align=center><fmtWOView:message key="LBL_N/A"/></td>
					</tr>
					<tr align="center">
						<td class="RightText" height="20" align="right"><B><fmtWOView:message key="LBL_MATERIAL_CERTIFICATION"/></B>:</td>
						<%=strMatCertFl%>
					</tr>
					<tr align="center">
						<td class="RightText" nowrap height="20" align="right"><B><fmtWOView:message key="LBL_CERTIFICATION_COMPLIANCE"/></B>:</td>
						<%=strCompCertFl%>
					</tr>
					<tr align="center">
						<td class="RightText" nowrap height="20" align="right"><B><fmtWOView:message key="LBL_HARDNESS_TEST_REQUIRED"/> ?</B>:</td>
						<%=strHardTestFl%>
					</tr>
					<tr align="center">
						<td class="RightText" nowrap height="20" align="right"><B><fmtWOView:message key="LBL_INSPECTION_SHEET_ATTACHED"/> ?</B>:</td>
						<%=strInsp%>
					</tr>
					<tr align="center">
						<td class="RightText" nowrap height="20" align="right"><B><fmtWOView:message key="LBL_STERILIZATION_REQUIRED"/> ?</B>:</td>
						<%=strSter%>
					</tr>					
				</table>
			</td>
		</tr>
		</tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
		<tr>
			<td class="RightText" align="right"><B><fmtWOView:message key="LBL_DRAWING_#"/></B>:</td>
			<td class="RightText">&nbsp;<%=strDraw%></td>
			</td>
		</tr>
		</tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>		
		<tr>
			<td class="RightText"  nowrap align="right"><B><fmtWOView:message key="LBL_DRAWING_REV_#"/></B>:</td>
			<td class="RightText">&nbsp;<%=strDrawRev%></td>
		</tr>
		</tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>		
		<tr>
			<td class="RightText" height="20" nowrap align="right"><B><fmtWOView:message key="LBL_MATERIAL_SPECIFICATION"/></B>:</td>
			<td class="RightText">&nbsp;<%=strMatSpec%></td>
		</tr>
		<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
	</table>
<BR>
<span class="RightText"><b><fmtWOView:message key="LBL_SUB_ASSEMBLY_DETAILS"/>:</b></span><BR>
	<table width="700" cellspacing="0" cellpadding="0" border="0">
		<tr>
			<td rowspan="25" width="1" bgcolor="#666666"></td>
			<td colspan="5" height="1" bgcolor="#666666"></td>
			<td rowspan="25" width="1" bgcolor="#666666"></td>
		</tr>
		<tr class="RightTableCaption" bgcolor="#cccccc">
			<td height="18"><fmtWOView:message key="LBL_SUB_COMP_ID"/></td>
			<td widht="450"><fmtWOView:message key="LBL_DESCRIPTION"/></td>
			<td><fmtWOView:message key="LBL_DRAWING_#"/></td>
			<td><fmtWOView:message key="LBL_REV#"/></td>
			<td><fmtWOView:message key="LBL_MATERIAL_SPEC"/></td>
		</tr>
		<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
<%
  		intSize = alSubWorkOrder.size();
		hcboVal = new HashMap();
		if ( intSize > 0)
		{
	  		for (int i=0;i<intSize;i++)
	  		{
	  			hcboVal = (HashMap)alSubWorkOrder.get(i);
	  			strSubPartNum = (String)hcboVal.get("PNUM");
				strSubPartDesc = (String)hcboVal.get("PDESC");
				strDraw = (String)hcboVal.get("DRAW");
				strDrawRev = (String)hcboVal.get("REV");
				strMatSpec = (String)hcboVal.get("MSPEC");
				strLog = GmCommonClass.parseNull((String)hcboVal.get("WLOG"));
%>
				<tr>
					
					<%if(strLog.equals("N"))
					{%>
					
					<td height="18" class="RightText">&nbsp;
					<fmtWOView:message key="IMG_ALT_COMMENTS" var = "varComments"/>
					<img style='cursor:hand' width='22' height='20' src='<%=strImagePath%>/phone_icon.jpg' 
					title='${varComments}' border='0' onClick="javascript:fnOpenOrdLogwo('<%=strSubPartNum%>');" >
					<%=strSubPartNum%></td>
					
					<%}
					else
					{%>
		
					<td height="18" class="RightText">&nbsp;
					<fmtWOView:message key="IMG_ALT_COMMENTS" var = "varComments"/>
					<img style='cursor:hand' width='22' height='20' src='<%=strImagePath%>/phone-icon_ans.gif' 
					title='${varComments}' border='0' onClick="javascript:fnOpenOrdLogwo('<%=strSubPartNum%>');" >
					<%=strSubPartNum%></td>
					<%} %>
					<td class="RightText">&nbsp;<%=strSubPartDesc%></td>
					<td class="RightText">&nbsp;<%=strDraw%></td>
					<td class="RightText">&nbsp;<%=strDrawRev%></td>
					<td class="RightText">&nbsp;<%=strMatSpec%></td>
				</tr>
				<tr><td colspan="5" height="1" bgcolor="#eeeeee"></td></tr>
<%
			}
		}else{
%>
		<tr><td colspan="5" height="30" align="center" class="RightText"><fmtWOView:message key="MSG_NO_DATA"/></td></tr>
<%
			}
%>		
		<tr><td colspan="5" height="1" bgcolor="#666666"></td></tr>
	</table>

<BR>

	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="4" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="4" width="1"></td>
		</tr>
		<tr>
			<td width="170"><img src="<%=strImagePath%>/<%=strUser%>"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="center" class="RightText"><%=strStartDate%>&nbsp;</td>
		</tr>
		<tr>
			<td width="170" class="RightTableCaption"><fmtWOView:message key="LBL_VP_OPERATIONS_DESIGNEE"/></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="center" class="RightTableCaption"><fmtWOView:message key="LBL_DATE"/>&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
	</table>

	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" id="button">
				<fmtWOView:message key="BTN_RETURN" var="varReturn"/>
				<gmjsp:button value="&nbsp;${varReturn}&nbsp;" name="Btn_Return" gmClass="button" onClick="history.go(-1)" buttonType="Load" />&nbsp;&nbsp;
				<fmtWOView:message key="BTN_PRINT" var="varPrint"/>
				<gmjsp:button value="${varPrint}" name="Btn_Print" gmClass="button" onClick="fnViewPrint();" buttonType="Load" />&nbsp;&nbsp;
			</td>
		</tr>
	</table>
	<span class="RightText"><%=strFooter%></span>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
