<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: LoanerTransferReport.jsp
 * Desc		 		: Loaner Transfer Report
 * Version	 		: 1.0
 * author			: Gopinathan
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLoanTransRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- operations\GmLoanerTransferReport.jsp -->
<fmtLoanTransRpt:setLocale value="<%=strLocale%>"/>
<fmtLoanTransRpt:setBundle basename="properties.labels.operations.GmLoanerTransferReport"/>

<bean:define id="gridData" name="frmLoanerTransfer" property="gridXmlData" type="java.lang.String"> </bean:define>
<bean:define id="accessFl" name="frmLoanerTransfer" property="accessFl" type="java.lang.String"> </bean:define>
<% 
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_TRANSFER_REPORT"); 
String strApplDateFmt = strGCompDateFmt;
String strTodaysDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner Transfer Report </TITLE>

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLoanerTransferReport.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmCommonShippingReport.js"></script>


<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxWindows/dhtmlxwindows.css"></link> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/skins/dhtmlxcalendar_yahoolike.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/GmDhtmlxWindowPopup.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_dhxcalendar.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<script>
	var objGridData;
	var gridObj;
	objGridData = '<%=gridData%>';
	var strTodaysDate = '<%=strTodaysDate%>';
	var format = '<%=strApplDateFmt%>';
</script>
</HEAD>

<BODY  leftmargin="20" topmargin="10" onLoad="javascript:fnOnPageLoad()" onkeyup="fnEnter();">
<html:form action="/gmLoanerTransferReport.do">
<html:hidden name="frmLoanerTransfer" property="strOpt"/>
<html:hidden name="frmLoanerTransfer" property="inputString"/>
<table border="0" class= "GtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader">&nbsp;<fmtLoanTransRpt:message key="TD_LOAN_TRANS_RPT_HEADER"/> </td>
			<fmtLoanTransRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
			<td  height="25" class="RightDashBoardHeader" align="right">
			    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td class="RightTableCaption" align="right"><fmtLoanTransRpt:message key="LBL_FROM_DIST"/>:&nbsp;</td>
			<td><gmjsp:dropdown controlName="fromDistributor"  SFFormName="frmLoanerTransfer" SFSeletedValue="fromDistributor" 	
					 SFValue="alFromDistributor" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"  />										
			</td>
			<td height="25" class="RightTableCaption" align="right"><fmtLoanTransRpt:message key="LBL_TO_DIST"/>:&nbsp;</td>
			<td><gmjsp:dropdown controlName="toDistributor" SFFormName="frmLoanerTransfer" SFSeletedValue="toDistributor" 	
					 SFValue="alToDistributor" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"  />										
			</td>
		</tr>
		<tr class="Shade">
				<td class="RightTableCaption"  align="right" ><fmtLoanTransRpt:message key="LBL_SET_ID"/>:&nbsp;</td>
				<td colspan="3"><gmjsp:dropdown controlName="setId" SFFormName="frmLoanerTransfer" SFSeletedValue="setId"
								SFValue="alSetId" codeId = "ID"  codeName = "IDNAME"  defaultValue= "[Choose One]" />
				</td>
		</tr>
		<tr>
				<td class="RightTableCaption"  align="right" ><fmtLoanTransRpt:message key="LBL_STATUS"/>:&nbsp;</td>
				<td colspan="3"><gmjsp:dropdown controlName="strStatusFl" SFFormName="frmLoanerTransfer" SFSeletedValue="strStatusFl"
								SFValue="alStatus" codeId = "CODEID"  codeName = "CODENM" />
				</td>
		</tr>
		<tr class="Shade">						
				<td class="RightTableCaption"  align="right" ><fmtLoanTransRpt:message key="LBL_TRANS_DATE"/>:&nbsp;</td>
				<td colspan="2">
                <gmjsp:calendar SFFormName="frmLoanerTransfer" controlName="transferDtFrom" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
                
				       <b>&nbsp;&nbsp;<fmtLoanTransRpt:message key="LBL_TO"/>:&nbsp;</b>
 		 		<gmjsp:calendar SFFormName="frmLoanerTransfer" controlName="transferDtTo" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
		</td>
		<fmtLoanTransRpt:message key="BTN_LOAD" var="varLoad"/>
		<td>
		      <gmjsp:button value="&nbsp;${varLoad}&nbsp;" gmClass="button" onClick="fnLoad();" buttonType="Load" />
	    </td> 				
		</tr>
         <%if( gridData.indexOf("cell") != -1) {%>             
		<table  border="0" class= "GtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2"><div id="grpData" style="grid" height="500px" width="898">&nbsp;</div></td> 
		</tr>
		
		<tr>		
        <td align="right"><br>
             <div class='exportlinks'><fmtLoanTransRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp; 
			<a href="#" onclick="fnExport();"> <fmtLoanTransRpt:message key="DIV_EXCEL"/> </a>	                         
        </td>
       <% String strDisableBtnVal = accessFl;
			if(strDisableBtnVal.equals("disabled")){
				strDisableBtnVal ="true";
		    }
	   %>
	   <fmtLoanTransRpt:message key="BTN_ROLLBACK" var="varRollBk"/>
	   <fmtLoanTransRpt:message key="BTN_SUBMIT" var="varSubmit"/>
        <td align="left"><br> &nbsp; &nbsp; &nbsp; <gmjsp:button value="&nbsp;${varRollBk}&nbsp;" gmClass="button" onClick="fnRollback();" disabled="<%=strDisableBtnVal%>" buttonType="Save" />
         <gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" disabled="<%=strDisableBtnVal%>" buttonType="Save"/> &nbsp;</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		 <%}else if(!gridData.equals("")){%>
		 <tr><td colspan="5" align="Center">&nbsp;<fmtLoanTransRpt:message key="MSG_NOTHING_FOUND"/></td> </tr>
		  <%}else{ %>
		  <tr><td colspan="5" align="Center">&nbsp;<fmtLoanTransRpt:message key="MSG_NO_DATA"/></td> </tr>
		  <%} %>
		</table> 
		
		</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>