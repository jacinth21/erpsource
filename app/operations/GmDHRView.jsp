 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmDHRView.jsp
 * Desc		 		: This screen is used for viewing the DHR details
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>

<%@ taglib prefix="fmtDHRView" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRView.jsp -->

<fmtDHRView:setLocale value="<%=strLocale%>"/>
<fmtDHRView:setBundle basename="properties.labels.operations.GmDHRView"/>


<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	/* System.out.println(" Str Mode inside DHR View JSP is " + strMode);
	System.out.println(" hashMap inside JSP is  " + hmReturn); */
	String strWikiTitle = "";
	ArrayList alReturnReasons = (ArrayList)request.getAttribute("hNCMRReason");
	HashMap hmDHRDetails = new HashMap();
	ArrayList alEmpList = new ArrayList();
	ArrayList alSubDHR = new ArrayList();
	String strApplnDateFmt = strGCompDateFmt;
	String strShade = "";
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.operations.GmDHRView", strSessCompanyLocale);
	
	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyInspected = "";
	String strQtyReject = "";
	String strActionTaken = "";
	String strCodeID = "";
	String strSelected = "";
	String strReceivedBy = "";
	String strInspectedBy = "";
	String strInspectedByNm = "";
	String strQtyPackaged = "";
	String strPackagedBy = "";
	String strPackagedByNm = "";
	String strQtyInShelf = "";
	String strVerifiedBy = "";
	String strVerifiedByNm = "";
	String strNCMRId = "";
	String strCriticalFl = "";
	String strCreatedDate = "";
	String strRejReason = "";
	String strReasonType = "";
	String strReasonTypeID = "";
	String strCloseQty = "";
	String strNCMRFl = "";
	String strReceivedTS = "";
	String strInspectedTS = "";
	String strPackagedTS = "";
	String strVerifiedTS = "";
	String strSubComFl = "";
	String strSterFl = "";
	String strChecked = "";
	String strLabelFl = "";
	String strPackFl = "";
	String strPartSterFl = "";
	String strFooter = "";
	String strDocRev = "";
	String strDocActiveFl = "";
	String strFARFl = "";
	String strBackOrderFl = "";	
	String strNCMRAction = "";
	String strNCMRRemarks = "";
	String strNCMRAuthBy = "";
	String strNCMRAuthDt = "";
	String strDisabled = "";
	String strStatusFl = "";
	String strSessErrMsg = "";
	String strStatusErrFl = "NULL";
	String strFromPage = "";
	String strTitle = "";
	String strQtyShipped = "";
	String strUOM = "";
	String strInsertID="";
	String strPrimaryPack="";
	String strSecondaryPack="";
	String strUserId = 	(String)session.getAttribute("strSessUserId");
	String strXmlData=GmCommonClass.parseNull((String)request.getAttribute("XMLDATA"));
	String strProdType = "";
	String strDonorNum = "";
	String strSerialNumFl = "";
	int intNCMRFl = 0;

	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");

		strDHRId 		= GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum 		= GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc 		= GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId 		= GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum 	= GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec 		= GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		strReceivedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		strQtyInspected = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYINSP"));
		strQtyReject 	= GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ"));
		strActionTaken 	= GmCommonClass.parseNull((String)hmDHRDetails.get("ACTTKN"));
		strInspectedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("INSPBY"));
		strInspectedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("INSPBYNM"));
		strPackagedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("PACKBY"));
		strPackagedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKBYNM"));
		strQtyPackaged 	= GmCommonClass.parseNull((String)hmDHRDetails.get("QTYPACK"));
		strQtyInShelf 	= GmCommonClass.parseNull((String)hmDHRDetails.get("QTYSHELF"));
		strVerifiedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VERIFYBY"));
		strVerifiedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("VERIFYBYNM"));
		strNCMRId 		= GmCommonClass.parseNull((String)hmDHRDetails.get("NCMRID"));
		strCriticalFl 	= GmCommonClass.parseNull((String)hmDHRDetails.get("CFL"));
		//strCreatedDate 	= GmCommonClass.parseNull((String)hmDHRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplnDateFmt);
		strRejReason 	= GmCommonClass.parseNull((String)hmDHRDetails.get("REJREASON"));
		strReasonType 	= GmCommonClass.parseNull((String)hmDHRDetails.get("REASON_TYPE"));
		strReasonTypeID = GmCommonClass.parseNull((String)hmDHRDetails.get("REASON_ID"));
		strNCMRFl 		= GmCommonClass.parseZero((String)hmDHRDetails.get("NCMRFL"));
		strCloseQty 	= GmCommonClass.parseNull((String)hmDHRDetails.get("CLOSEQTY"));
		strReceivedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("RECTS"));
		strInspectedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("INSTS"));
		strPackagedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("PACKTS"));
		strVerifiedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VERTS"));
		strSubComFl 	= GmCommonClass.parseNull((String)hmDHRDetails.get("SUBFL"));
		strSterFl 		= GmCommonClass.parseNull((String)hmDHRDetails.get("STERFL"));
		strChecked 		= GmCommonClass.parseNull((String)hmDHRDetails.get("SKIPLBLFL"));
		strNCMRAction 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DISP"));
		strNCMRRemarks 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DISPREMARKS"));
		strNCMRAuthBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DISPBY"));
		strNCMRAuthDt 	= GmCommonClass.getStringFromDate((java.util.Date) hmDHRDetails.get("DISPDATE"),strApplnDateFmt);
		strBackOrderFl	= GmCommonClass.parseNull((String)hmDHRDetails.get("BACKORDER_FLAG"));
		strUOM 			= GmCommonClass.parseNull((String)hmDHRDetails.get("UOMANDQTY"));
		strQtyShipped 	= GmCommonClass.parseNull((String)hmDHRDetails.get("SHIPQTY"));
		strInsertID 	= GmCommonClass.parseNull((String)hmDHRDetails.get("INSID"));
		strPrimaryPack 	= GmCommonClass.parseNull((String)hmDHRDetails.get("PACKPRIM"));
		strSecondaryPack= GmCommonClass.parseNull((String)hmDHRDetails.get("PACKSEC"));		
		strStatusFl 	= GmCommonClass.parseNull((String)hmDHRDetails.get("SFL"));
		strProdType 	= GmCommonClass.parseNull((String)hmDHRDetails.get("PRODTYPE"));
		strDonorNum 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DONORNUMBER"));
		strSerialNumFl = GmCommonClass.parseNull((String)hmDHRDetails.get("SERIALNUMNEEDFL"));
		
		strLabelFl = strChecked.equals("1")?"Yes":"-";
		strChecked = strChecked.equals("1")?"checked":"";
		strPackFl = GmCommonClass.parseNull((String)hmDHRDetails.get("SKIPPACKFL"));
		strPackFl = strPackFl.equals("1")?"Yes":"-";
		
		// System.out.println(" Value of Status flag from Backend " + strStatusFl);
			
		strPartSterFl = GmCommonClass.parseNull((String)hmDHRDetails.get("PCLASS"));
		strPartSterFl = strPartSterFl.equals("4030")?"1":"";
		
		strFooter = GmCommonClass.parseNull((String)hmDHRDetails.get("FOOTER"));
		String strArr[] = strFooter.split("\\^");
		strFooter = strArr[0];
		strDocRev = strArr[1];
		strDocActiveFl = strArr[2];

		strFARFl = GmCommonClass.parseNull((String)hmDHRDetails.get("FARFL"));
								
		intNCMRFl = Integer.parseInt(strNCMRFl);
		
		alEmpList = (ArrayList)hmReturn.get("EMPLIST");
	}

	int intSize = 0;
	HashMap hcboVal = null;
	
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR View </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/watermark.js"></script>
</HEAD>

<script>
var companyInfoObj = '<%=strCompanyInfo%>';
var tdinnner = "";
var strObject = "";
var objGridData = '<%=strXmlData%>';
var strDHRId = '<%=strDHRId%>';
function fnViewPrint()
{
	windowOpener("/GmPOReceiveServlet?companyInfo=<%=strCompanyInfo%>&hAction=ViewDHRPrint&hDHRId=<%=strDHRId%>&hVenId=<%=strVendorId%>&hWOId=<%=strWOId%>","DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnViewPrintWO()
{
windowOpener("/GmWOServlet?companyInfo=<%=strCompanyInfo%>&hAction=PrintWO&hWOId=<%=strWOId%>&hDocRev=<%=strDocRev%>&hDocFl=<%=strDocActiveFl%>","WO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnRollback()
{
// Comments
            if (document.frmOrder.Txt_LogReason.value == '')
            {
                        Error_Details(message[51]);
            }         
	document.frmOrder.hAction.value = 'UpdateRollback';
	document.frmOrder.action = "/GmDHRModifyServlet?companyInfo="+companyInfoObj;
	fnSubmit();
}

function fnVoid()
{
  // Comments
  if(confirm("Are you sure to void the "+strDHRId+"")){
  fnStartProgress();
            if (document.frmOrder.Txt_LogReason.value == '')
            {
                        Error_Details(message[51]);
            }           

	document.frmOrder.hAction.value = 'UpdateVoid';
	document.frmOrder.action = "/GmDHRModifyServlet?companyInfo="+companyInfoObj;
	fnSubmit();
}
}
function fnSubmit()
{
if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			fnStopProgress();
			return false;	
	}
	
	document.frmOrder.submit();
}

function fnPrint()
{
	window.print();
}

function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
function fnOnPageLoad()
{
	if(objGridData.indexOf("cell", 0)==-1){
		  return true;
	}
	var gridObj = initGrid('dataGridDiv',objGridData);
	gridObj.setEditable(false);
}

//To open the popup when click on "D" icon
function fnCallDonor(dhrId, vendorId){
	windowOpener("/gmDonorInfo.do?companyInfo="+companyInfoObj+"&method=fetchDonorInfo&strOpt=load&hDhrId="+dhrId+"&hMode=Report&hVendorId="+vendorId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=685,height=600");
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onbeforeprint="hidePrint();" onafterprint="showPrint();" onload="fnOnPageLoad();">
<FORM name="frmOrder" method="POST">
<input type="hidden" name="hDHRId" value="<%=strDHRId%>">
<input type="hidden" name="hAction" value="<%=strMode%>">
<input type="hidden" name="hPartNum" value="<%=strPartNum%>">
<input type="hidden" name="hWOId" value="<%=strWOId%>">
<input type="hidden" name="hRecQty" value="<%=strQtyRec%>">
<input type="hidden" name="hRejQty" value="<%=strQtyReject%>">
<input type="hidden" name="hInputString" value="">
<input type="hidden" name="hSterFl" value="<%=strSterFl%>">
<input type="hidden" name="hPartSterFl" value="<%=strPartSterFl%>">
<input type="hidden" name="hNCMRFl" value="<%=intNCMRFl%>">
<input type="hidden" name="hStatusFl" value="<%=strStatusFl%>">

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">		
		<tr>
			<td>	
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader" > <fmtDHRView:message key="LBL_DEVICE_HISTORY_RECORD"/> - 
						<%
							if (strMode.equals ("Load"))
							{
								strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VIEW"));
								strWikiTitle = GmCommonClass.getWikiTitle("DEVICE_HISTORY_RECORD_VIEW");
							}
							
							else if (strMode.equals ("Rollback"))
							{
								strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ROLLBACK"));
								strWikiTitle = GmCommonClass.getWikiTitle("DEVICE_HISTORY_RECORD_ROLLBACK");
							}
							
							else if (strMode.equals ("voidDHR") || strMode.equals ("UpdateVoid"))
							{
								strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VOID_DHR"));
								strWikiTitle = GmCommonClass.getWikiTitle("DEVICE_HISTORY_RECORD_VOID_DHR");
							}
							
							else 
							{
								strTitle = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VIEW"));
								strWikiTitle = GmCommonClass.getWikiTitle("DEVICE_HISTORY_RECORD_VIEW");
							}
							%>
						<%=strTitle%> </td>	
						<td height="25" class="RightDashBoardHeader" align="right"><fmtDHRView:message key="IMG_ALT_HELP" var = "varHelp"/>
						<img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="4">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td width="120" class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_DHR_ID"/>:</td>
									<td width="140" class="RightText">&nbsp;<b><%=strDHRId%></b></td>
									<td width="100" class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_BIN_#"/>:</td>
									<td width="360" class="RightText">&nbsp;</td>
								</tr>
								<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_PART_NUMBER"/>:</td>
									<td class="RightText">&nbsp;<%=strPartNum%></td>
									<td class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_DESCRIPTION"/>:</td>
									<td class="RightText"  colspan="3">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>									
								</tr>
								<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_CONTROL_NUMBER"/>:</td>
									<td class="RightText">&nbsp;<%=strControlNum%></td>
									<td class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_VENDOR"/>:</td>
									<td class="RightText"  colspan="3">&nbsp;<%=strVendName%></td>
								</tr>
								<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_WORK_ORDER"/>:</td>
									<td class="RightText">&nbsp;<%=strWOId%></td>
									<td class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_UOM/UNITS"/>:</td>
									<td class="RightText"  colspan="3">&nbsp;<%=strUOM%></td>
								</tr>								
								<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>								
								<tr>
									<td class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_CREATED_DATE"/>:</td>
									<td class="RightText" >&nbsp;<%=strCreatedDate%></td>
									<td class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_DONOR_#"/>:</td>
									<td class="RightText" colspan="3">&nbsp;<%=strDonorNum%> 
									<%if((strProdType.equals("100845") && !strDonorNum.equals("")) || strSerialNumFl.equals("Y")){%>
									&nbsp;<fmtDHRView:message key="IMG_ALT_DONOR" var = "varDonor"/><img align="top" id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/d.gif' title='${varDonor}' onclick="javascript:fnCallDonor('<%=strDHRId%>','<%=strVendorId%>')" width='14' height='14'/>
									<%} %>
									</td>
								</tr>
							</table>
						 </td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;<fmtDHRView:message key="LBL_SUB_ASSEMBLY_DETAILS"/>
					</tr>
<%
				if (strSubComFl.equals("1")){
%>
					<tr>
						<td colspan="4">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="RightText" HEIGHT="18">&nbsp;<fmtDHRView:message key="LBL_SUB_COMP_ID"/></td>
									<td class="RightText" width="350"><fmtDHRView:message key="LBL_DESCRIPTION"/></td>
									<td class="RightText"><fmtDHRView:message key="LBL_CONTROL_NUMBER"/></td>
								</tr>
								<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%
				  		intSize = alSubDHR.size();
						hcboVal = new HashMap();
						String strSubDHRId = "";
						String strSubCompId = "";
						String strSubDesc = "";
						String strControl = "";
						
				  		for (int s=0;s<intSize;s++)
				  		{
				  			hcboVal = (HashMap)alSubDHR.get(s);
				  			strSubDHRId = GmCommonClass.parseNull((String)hcboVal.get("SUBDHRID"));
				  			strSubCompId = GmCommonClass.parseNull((String)hcboVal.get("SUBASMBID"));
				  			strSubDesc = GmCommonClass.parseNull((String)hcboVal.get("PDESC"));
				  			strControl = GmCommonClass.parseNull((String)hcboVal.get("CNUM"));
%>
								<tr>
									<td class="RightText" HEIGHT="18">&nbsp;&nbsp;<%=strSubCompId%></td>
									<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSubDesc)%></td>
									<td class="RightText">&nbsp;<%=strControl%></td>
<%
						}
%>									
								</tr>
							</table><input type="hidden" name="hSubCnt" value="<%=intSize%>">
						</td>
					</tr>					
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%
				}else{
%>
					<tr><td colspan="4" class="RightText" height="30" align="center"><fmtDHRView:message key="LBL_NOT_APPLICABLE"/></td></tr>
<%
				}
%>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;<fmtDHRView:message key="LBL_RECEIVING"/></td>
					</tr>
					<tr>
						<td class="RightText" width="170" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_PO_QTY_SHIPPED"/>:</td>
						<td class="RightText" width="100">&nbsp;<%=strQtyShipped%></td>
						<td class="RightText" width="250" align="right">&nbsp;<fmtDHRView:message key="LBL_QTY_RECIEVED"/>:</td>
						<td class="RightText" width="170">&nbsp;<%=strQtyRec%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightText" align="right">&nbsp;<fmtDHRView:message key="LBL_RECIEVED_BY"/>:</td>
						<td class="RightText" width="300">&nbsp;<%=strReceivedBy%></td>
						<td class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_RECEIVED_TIMESTAMP"/>:</td>
						<td class="RightText">&nbsp;<%=strReceivedTS%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>		
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;<fmtDHRView:message key="LBL_INSPECTION"/></td>
					</tr>					
						<td class="RightText" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_QTY_INSPECTED"/>:</td>
						<td class="RightText">&nbsp;<%=strQtyInspected%></td>
						<td class="RightText" align="right">&nbsp;<fmtDHRView:message key="LBL_INSPECTED_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strInspectedByNm%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"> <fmtDHRView:message key="LBL_INSPECTED_TIMESTAMP"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strInspectedTS%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"> <fmtDHRView:message key="LBL_QTY_REJECTED"/>:</td>
						<td class="RightText">&nbsp;<%=strQtyReject%></td>
						<td class="RightText" align="right" HEIGHT="24"> <fmtDHRView:message key="LBL_RAISED_NCMR_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strNCMRId%></td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee"></td></tr>
					<td class="RightText" align="right" HEIGHT="24"> <fmtDHRView:message key="LBL_REJECTION_TYPE"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strReasonType%></td>				
					<tr><td colspan="2" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24"> <fmtDHRView:message key="LBL_REASON_FOR_REJECTION"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strRejReason%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightTableCaption" colspan="3" HEIGHT="24">&nbsp;&nbsp;<u> <fmtDHRView:message key="LBL_NCMR_DISPOSITION_DETAILS"/></u></td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="22"><fmtDHRView:message key="LBL_ACTION_TAKEN"/> :</td>
						<td class="RightText">&nbsp;<%=strNCMRAction%></td>
						<td class="RightText" align="right" valign="top" HEIGHT="22"><fmtDHRView:message key="LBL_REMARKS"/>:</td>
						<td class="RightText">&nbsp;<%=strNCMRRemarks%></td>						
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="22"><fmtDHRView:message key="LBL_AUTHORIZED_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strNCMRAuthBy%></td>
						<td class="RightText" align="right" HEIGHT="22"><fmtDHRView:message key="LBL_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strNCMRAuthDt%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="22"><fmtDHRView:message key="LBL_NCMR_CLOSEDOUT_QTY"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strCloseQty%>
						<input type="hidden" name="hCloseQty" value="<%=strCloseQty%>"></td>						
					</tr>			
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;<fmtDHRView:message key="LBL_PROCESSING"/></td>
					</tr>
					<tr>
						<td colspan="8"><div id="dataGridDiv" class="grid" style="height : 150px;"></div></td>
					</tr>
						<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>

								<td class="RightTableCaption" align="right" HEIGHT="24" width="200"> <fmtDHRView:message key="LBL_SKIP_LABELLING"/> ?:</td>
	                        <%if(strPackagedByNm!="") {%>
	                            <td class="RightText" HEIGHT="24" width="100">&nbsp;<%=strLabelFl%>
	                        <%}else { %>
								<td class="RightText"  HEIGHT="24" width="100"><input type="checkbox" <%=strChecked%> name="Chk_Skip"></td>
				            <%}%>	
								<td class="RightTableCaption"  align="right"><fmtDHRView:message key="LBL_PROCESSED_BY"/>:</td>
				            <%if(strPackagedByNm!="") {%>		
								<td class="RightText" width="250">&nbsp;<%=strPackagedByNm%></td>
				            <%}else { %>
								<td class="RightText" width="170">
				            <%}%>	
						</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" ><fmtDHRView:message key="LBL_PRIMARY_PACKAGING"/>:</td>
						<td class="RightText">&nbsp;<%=strPrimaryPack%></td>
						<td class="RightTableCaption" align="right" ><fmtDHRView:message key="LBL_SECONDARY_PACKAGING"/>:</td>
						<td class="RightText">&nbsp;<%=strSecondaryPack%></td>						
					</tr>
				   <tr><td colspan="4" bgcolor="#cccccc"></td></tr>
				   <tr>
						<td class="RightTableCaption" align="right" ><fmtDHRView:message key="LBL_INSERT_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strInsertID%></td>
						<td class="RightTableCaption" align="right" ></td>
						<td class="RightText">&nbsp;</td>						
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;<fmtDHRView:message key="LBL_VERIFICATION"/></td>
					</tr>		
					<tr>
					<%if(strQtyInShelf==""){%>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_QTY_TO_VERIFY"/>:</td>
						<td class="RightText" width="100">&nbsp;<input type="text" size="4" value="<%=strQtyInShelf%>"  class="InputArea" name="Txt_Qty"></td>
					 <%}else { %>
					   <td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_QTY_VERIFIED"/>:</td>
					   <td class="RightText">&nbsp;<%=strQtyInShelf%></td>
					 <%}%>
						<td class="RightTableCaption" align="right"><fmtDHRView:message key="LBL_VERIFIED_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strVerifiedByNm%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRView:message key="LBL_VERIFIED_TIMESTAMP"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strVerifiedTS%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
				</table>
			</td>
		</tr>
		</table>
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0" >
		<tr>
			<td>
<%		if (strMode.equals("Rollback") || strMode.equals("voidDHR") || strMode.equals("UpdateVoid"))
		{
%>				<jsp:include page="/common/GmIncludeLog.jsp" >
					<jsp:param name="LogType" value="PLOG" />
					</jsp:include>
<%
		}
%>				
			</td>
		</tr>
		<tr border = 1 >
			<td align="center" height="30" id="button">
<%		if (strMode.equals("Load"))
		{
%>			<fmtDHRView:message key="BTN_RETURN" var="varReturn"/>
            <gmjsp:button value="&nbsp;${varReturn}&nbsp;" name="Btn_Return" gmClass="button" onClick="history.go(-1)" buttonType="Load" />&nbsp;&nbsp;
<%
		}
       else if (strMode.equals("voidDHR"))
		{
%>
					<fmtDHRView:message key="BTN_VOID_DHR" var="varVoidDHR"/>
					<gmjsp:button value="${varVoidDHR}" name="Btn_Void" gmClass="button"  onClick="fnVoid();" buttonType="Save" />&nbsp;&nbsp;
<%
		}
		else if (strMode.equals("UpdateVoid"))
		{
%>	
					<fmtDHRView:message key="BTN_PRINT" var="varPrint"/>
					<gmjsp:button value="${varPrint}" name="Btn_PrintVoid" gmClass="button"  onClick="fnPrint();" buttonType="Load" />&nbsp;&nbsp;
<%
		}
		else if (strMode.equals("Rollback") )
		{
%>
					<fmtDHRView:message key="BTN_ROLLBACK" var="varRollback"/>
					<gmjsp:button value="${varRollback}" name="Btn_RollBack" gmClass="button"  onClick="fnRollback();" buttonType="Save" />&nbsp;&nbsp;
<%
	}
%>
			<fmtDHRView:message key="BTN_PRINT_DHR" var="varPrintDHR"/>
			<gmjsp:button value="${varPrintDHR}" name="Btn_ViewPrint" gmClass="button"  onClick="fnViewPrint();" buttonType="Load" />&nbsp;&nbsp;
			<!-- <gmjsp:button value="Print WO" name="Btn_ViewPrint" gmClass="button" onClick="fnViewPrintWO();" buttonType="Load" />&nbsp;&nbsp; --> 
			</td>
		<tr>
	</table>
	
</FORM>
<div id="waterMark" style="position:absolute; z-Index: 0; top: 340; left: 340">
<% /* Below code to display bookmark value 
					* 2522 maps to duplicate order 
					* If Credited before invoice then display the Water mark
					* so the 
					*/  
											
				if (strMode.equals ("UpdateVoid"))
					{
%>
					<img src="<%=strImagePath%>/void.gif"  border=0>
				
<% }%>
</div>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>

</BODY>

</HTML>
