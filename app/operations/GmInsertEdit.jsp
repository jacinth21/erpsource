<%
/****************************************************************************************
 * File		 		: GmInsertEdit.jsp
 * Desc		 		: This screen is used to Edit the Insert Details for the Transaction
 * author			: Karthik
*****************************************************************************************/
%>
<!-- \operations\GmInsertEdit.jsp -->

<%@ page language="java"%>

<%@ include file="/common/GmHeader.inc"%>



<%@ taglib prefix="fmtInsertEdit" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtInsertEdit:setLocale value="<%=strLocale%>"/>
<fmtInsertEdit:setBundle basename="properties.labels.operations.GmInsertEdit"/>

<bean:define id="gridData" name="frmInsertEdit" property="gridXmlData" type="java.lang.String" />
<bean:define id="alInsertDetail" name="frmInsertEdit" property="alInsertDetail" type="java.util.ArrayList"></bean:define>
<bean:define id="strAccessFl" name="frmInsertEdit" property="accessFlag" type="java.lang.String" />
<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	String strWikiTitle = GmCommonClass.getWikiTitle("ADD_EDIT_INSERTS");
	String strDisable = strAccessFl.equals("Y") ? "" : "true";
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Add/Edit Insert</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmInsertEdit.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<script>
var objGridData = '<%=gridData%>';
var alResultSize = '<%=alInsertDetail%>';
var voidRowIdStr = '';
var lblTxnID = '<fmtInsertEdit:message key="LBL_TXN_ID"/>';
var lblNoPartNum = '<fmtInsertEdit:message key="LBL_NO_INSERT_NUMBER"/>';
var lblPartNumDuplicated = '<fmtInsertEdit:message key="LBL_INSERT_DUPLICATED"/>';
</script>
</HEAD>
<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">

<html:form action="/gmInsertEditAction.do?method=saveInsertDetail&txnID=''">
	<html:hidden property="strInputString" value=""/>
	<html:hidden property="strVoidRowIds" value=""/>
	<table class="DtTable700" cellspacing="0" cellpadding="0" border="0" >
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtInsertEdit:message key="LBL_ED_INSERT"/></td>
			<td align="right" class=RightDashBoardHeader colspan="3"><fmtInsertEdit:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit'
				align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
				title='${varHelp}'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr>
			<td colspan="4" class="LLine" height="1"></td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<fmtInsertEdit:message key="LBL_TXN_ID" var="varTxnId"/><gmjsp:label type="MandatoryText"  SFLblControlName="${varTxnId}:" td="true"/>
			<td height="25">&nbsp;<html:text
				property="txnID" size="15" 
				onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
				onblur="changeBgColor(this,'#ffffff');" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtInsertEdit:message key="BTN_LOAD" var="varLoad"/><gmjsp:button
					value="&nbsp;${varLoad}&nbsp;" name="Btn_Load" style="width: 5em; height: 20" tabindex="1"
						gmClass="button" onClick="javascript:fnLoad();" buttonType="Load" />
			</td>
		</tr>
		<logic:equal property="statusFlag" name="frmInsertEdit" value="Y">
		<tr><td class="LLine" colspan="4"></td></tr>
		<tr>
			<td colspan="4">
				<table  id='tab_Disable'>
					<tr>
						<td colspan="4">
						<table>
							<TR>
								<td class="RightTableCaption">
								<fmtInsertEdit:message key="LBL_ADD" var="varAdd"/><gmjsp:label type="BoldText"  SFLblControlName="${varAdd}:" td="false"/>
								&nbsp;<a href="#"> <fmtInsertEdit:message key="IMG_ADD_ROW" var="varAddRow"/><img src="<%=strImagePath%>/dhtmlxGrid/add.gif" alt="${varAddRow}" style="border: none;" onClick="addRow()"height="14"></a>&nbsp;&nbsp;
								<fmtInsertEdit:message key="LBL_DEL" var="varDel"/><gmjsp:label type="BoldText"  SFLblControlName="${varDel}:" td="false"/>
								&nbsp;<a href="#"> <fmtInsertEdit:message key="IMG_REMOVE_ROW" var="varRemoveRow"/><img src="<%=strImagePath%>/dhtmlxGrid/delete.gif" alt="${varRemoveRow}" style="border: none;" onClick="javascript:removeSelectedRow()" height="14"></a>
								</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" width="100%">
							<div id="dataGrid" style="height: 400px; width: 700px;"></div>
	
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr id="savebtn_Disable"  style="display: table-row;">
			<td align="center" colspan="4" height="30"><fmtInsertEdit:message key="BTN_SAVE" var="varSave"/><gmjsp:button
				value="&nbsp;${varSave}&nbsp;" name="Btn_Submit" disabled="<%=strDisable%>" style="width: 5em; height: 22" tabindex="11"
				gmClass="button" onClick="javascript:fnSubmit();" buttonType="Save" />
			</td>
		</tr>
		</logic:equal>
		<%if (alInsertDetail.size() == 0 ){%>
		<tr>
			<td colspan="4" height="30" align="center" class="RightText"> 
				<fmtInsertEdit:message key="LBL_NO_DATA_AVAILABLE"/>
				<%-- <logic:equal property="statusFlag" name="frmInsertEdit" value="N">
					<fmtInsertEdit:message key="LBL_INSERT_MSG" var = "varInsertHelpMsg" />
					<img id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varInsertHelpMsg}'/>
				</logic:equal> --%>
			</td>
		</tr>
		<% } %> 
 		<logic:equal property="statusFlag" name="frmInsertEdit" value="N">
		<tr>
			<td colspan="4" height="30" align="center" class="RightText"> 
				<font size = "2px" color="red"><fmtInsertEdit:message key="LBL_INSERT_MSG"/></font>
			</td>
		</tr>
		</logic:equal>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>