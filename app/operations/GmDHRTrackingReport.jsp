 <%
/**********************************************************************************
 * File		 		: GmDHRTrackingReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ taglib prefix="fmtDHRTrackingReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRTrackingReport.jsp -->

<fmtDHRTrackingReport:setLocale value="<%=strLocale%>"/>
<fmtDHRTrackingReport:setBundle basename="properties.labels.operations.GmDHRTrackingReport"/>


<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strhMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	String strhOpt = (String)request.getAttribute("hOpt")==null?"":(String)request.getAttribute("hOpt");
	
	String strShade = "";
	ArrayList alProject = new ArrayList();
	ArrayList alDHRStatus = new ArrayList();

	String strDHRId = GmCommonClass.parseNull((String)request.getAttribute("hDHRId"));
	String strPartNum = GmCommonClass.parseNull((String)request.getAttribute("hPartNum"));
	String strProjectId = GmCommonClass.parseNull((String) request.getAttribute("hProjId"));
	String strDHRStatus = GmCommonClass.parseNull((String)request.getAttribute("hStatus"));

	String strPartDesc = "";
	String strFromDt = (String)request.getAttribute("hFrmDt")==null?"":(String)request.getAttribute("hFrmDt");
	String strToDt = (String)request.getAttribute("hToDt")==null?"":(String)request.getAttribute("hToDt");
	String strPartNums = (String)request.getAttribute("hPartNums")==null?"":(String)request.getAttribute("hPartNums");
	
	alProject = (ArrayList) request.getAttribute("PROJLIST");
	alDHRStatus = (ArrayList) request.getAttribute("DHRSTATUS");

	int intSize = 0;

	String strQtyRec = "";
	String strControl = "";
	
	String strPackSlip = "";
	String strWorkId = "";
	String strQtyShelf = "";
	String strPOId = "";
	String strVendor = "";
	
	String strNextId = "";
	int intCount = 0;
	String strLine = "";
	boolean blFlag = false;
	
	String strRecDate = "";
	String strRecQty = "";
	String strRecInitial = "";
	String strInspDate = "";
	String strInspQty = "";
	String strInspInitial = "";
	String strPackDate = "";
	String strPackQty = "";
	String strPackInitial = "";
	String strVerDate = "";
	String strVerQty = "";
	String strVerInitial = "";	
	String strApplDateFmt = strGCompDateFmt;
	String strDTDateFmt = "{0,date,"+strApplDateFmt+"}";
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmDHRTrackingReport.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var gStrServletPath = "<%=strServletPath%>";
var gStrOpt = "<%=strhOpt%>";
var gCmpDateFmt = "<%=strGCompDateFmt%>";
var companyInfoObj = '<%=strCompanyInfo%>';
</script>
</HEAD>

<BODY leftmargin="15" topmargin="10">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hDHRId" value="">
<input type="hidden" name="hVenId" value="">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">

	<table border="0" cellspacing="0" cellpadding="0" class="DtTable1100">
		<tr>
			<td height="25" colspan="7"  class="RightDashBoardHeader"><fmtDHRTrackingReport:message key="LBL_DHR_REPORT_DATA_RANGE"/></td>
		</tr>
		<tr class="shade">
			 
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRTrackingReport:message key="LBL_DHR_ID"/>:&nbsp;</td>
			<td class="RightText">
			    <input type="text" size="20" value="<%=strDHRId%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_DHRID">&nbsp;&nbsp;
			</td>  
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRTrackingReport:message key="LBL_PART_NUMBER"/>:&nbsp;</td>
			<td class="RightText">
			    <input type="text" size="20" value="<%=strPartNum%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_PartNum">&nbsp;&nbsp;
			</td> 
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRTrackingReport:message key="LBL_STATUS"/>:&nbsp;</td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<td class="RightText">
			    <gmjsp:dropdown controlName="Cbo_Status"  seletedValue="<%=strDHRStatus%>" 	
							tabIndex="1"   value="<%=alDHRStatus%>" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" />&nbsp;&nbsp;
			</td>   
			 <td> </td> 
		</tr> 
		<tr  >
			 
			 <td class="RightTableCaption" align = "right"  HEIGHT="30"><fmtDHRTrackingReport:message key="LBL_PROJECT_NAME"/> :&nbsp; </td>
			 <!-- Custom tag lib code modified for JBOSS migration changes -->
			<td><gmjsp:dropdown controlName="Cbo_Project"  seletedValue="<%=strProjectId%>" 	
					width="250"		tabIndex="1"   value="<%=alProject%>" codeId = "ID"  codeName = "NAME" defaultValue= "[Choose One]"  />
			</td>
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRTrackingReport:message key="LBL_FROM_DATE"/>:&nbsp;</td>
			<td class="RightText">
			<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=(strFromDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strFromDt,strApplDateFmt).getTime())))%>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/>
			   <%-- <input type="text" size="10" value="<%=strFromDt%>" name="Txt_FromDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmVendor.Txt_FromDate');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 /> --%>
			</td> 
			<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRTrackingReport:message key="LBL_TO_DATE"/>:&nbsp;</td>
			<td class="RightText">
			<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=(strToDt.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strToDt,strApplDateFmt).getTime())))%>" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;&nbsp;
			   <%--  <input type="text" size="10" value="<%=strToDt%>" name="Txt_ToDate" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmVendor.Txt_ToDate');" title="Click to open Calendar"  src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp; --%>
			<fmtDHRTrackingReport:message key="BTN_LOAD" var="varLoad"/>
			<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" onClick="fnGo();" tabindex="25" buttonType="Load" /> </td>   
			 <td></td> 
		</tr> 
		 
		<tr><td colspan="7" class="Line" height="1"></td></tr>
		<tr>
		    <td height="50" align="center" colspan="7">
			    <display:table name="requestScope.DHRLIST.rows" class = "its" id="currentRowObject" export="true" freezeHeader="true" decorator="com.globus.operations.displaytag.beans.DTDHRTrackingRptWrapper">
			    	<display:setProperty name="export.excel.filename" value="DhrTrackingReport.xls" /> 
					<fmtDHRTrackingReport:message key="DT_PART_NO" var="varPartno"/>
					<display:column property="PNUM" title="${varPartno}" style="width:60"/>
					<fmtDHRTrackingReport:message key="DT_DESCRIPTION" var="varDescription"/>
					<display:column property="PDESC" escapeXml="true" title="${varDescription}" />
					<fmtDHRTrackingReport:message key="DT_DHR_ID" var="varDHRID"/>
					<display:column property="ID" title="${varDHRID}" class="alignLeft" />
					<fmtDHRTrackingReport:message key="DT_CONTROL" var="varControl"/>
					<display:column property="CNUM" title="${varControl}" class="alignLeft" />
					<fmtDHRTrackingReport:message key="DT_REC_DATE" var="varRecDate"/>
					<display:column property="RECVTS" title="${varRecDate}" class="ShadeMedGreenTD" headerClass ="ShadeDarkGreenTD" sortable="true" format="<%=strDTDateFmt%>"/>
					<fmtDHRTrackingReport:message key="DT_REC_TIME" var="varRecTime"/>
					<display:column property="RECVTIME" title="${varRecTime}" class="ShadeLightGreenTD" style="width:60" headerClass ="ShadeDarkGreenTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_REC_QTY" var="varRecQty"/>
					<display:column property="QTYREC" title="${varRecQty}" class="ShadeMedGreenTD" headerClass ="ShadeDarkGreenTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_REC_INIT" var="varRecInit"/>
					<display:column property="RECVBY" title="${varRecInit} " class="ShadeLightGreenTD" headerClass ="ShadeDarkGreenTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_INSP_DATE" var="varInspDate"/>
					<display:column property="INSPTS" title="${varInspDate}" class="ShadeMedBlueTD" headerClass ="ShadeDarkBlueTD" sortable="true" format="<%=strDTDateFmt%>"/>
					<fmtDHRTrackingReport:message key="DT_INSP_TIME" var="varInspTime"/>
					<display:column property="INSPTIME" title="${varInspTime}" class="ShadeLightBlueTD" style="width:60" headerClass ="ShadeDarkBlueTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_INSP_QTY" var="varInspQty"/>
					<display:column property="INSPQTY" title="${varInspQty}" class="ShadeMedBlueTD" headerClass ="ShadeDarkBlueTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_INSP_INIT" var="varInspInit"/>
					<display:column property="INSPBY" title="${varInspInit}" class="ShadeLightBlueTD" headerClass ="ShadeDarkBlueTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_PACK_DATE" var="varPackDate"/>
					<display:column property="PACKTS" title="${varPackDate}" class="ShadeMedOrangeTD" headerClass ="ShadeDarkOrangeTD" sortable="true" format="<%=strDTDateFmt%>"/>
					<fmtDHRTrackingReport:message key="DT_PACK_TIME" var="varPackTime"/>
					<display:column property="PACKTIME" title="${varPackTime}" class="ShadeLightOrangeTD" style="width:60" headerClass ="ShadeDarkOrangeTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_PACK_QTY" var="varPackQty"/>
					<display:column property="PACKQTY" title="${varPackQty}" class="ShadeMedOrangeTD" headerClass ="ShadeDarkOrangeTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_PACK_INIT" var="varPackInit"/>
					<display:column property="PACKBY" title="${varPackInit}" class="ShadeLightOrangeTD" headerClass ="ShadeDarkOrangeTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_VER_DATE" var="varVerDate"/>
					<display:column property="VERTS" title="${varVerDate}" class="ShadeMedBrownTD" headerClass ="ShadeDarkBrownTD" sortable="true" format="<%=strDTDateFmt%>"/>
					<fmtDHRTrackingReport:message key="DT_VER_TIME" var="varVerTime"/>
					<display:column property="VERTIME" title="${varVerTime}" class="ShadeLightBrownTD" style="width:60" headerClass ="ShadeDarkBrownTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_VER_QTY" var="varVerQty"/>
					<display:column property="SHELFQTY" title="${varVerQty}" class="ShadeMedBrownTD" headerClass ="ShadeDarkBrownTD" sortable="true"/>
					<fmtDHRTrackingReport:message key="DT_VER_INIT" var="varVerInit"/>
					<display:column property="VERBY" title="${varVerInit}" class="ShadeLightBrownTD" headerClass ="ShadeDarkBrownTD" sortable="true"/>
	 			</display:table> 
			</td>
		</tr>    
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
