<!-- \operations\GmProcessTrans.jsp -->

<%
/**********************************************************************************
 * File                      : GmConsignedItems.jsp
 * Desc                      : Dashboard - Consigned Items
 * Version             : 1.0
 * author               : Tarika Chandure
************************************************************************************/
%><%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList"%>

<%@ taglib prefix="fmtProcessTrans" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<fmtProcessTrans:setLocale value="<%=strLocale%>"/>
<fmtProcessTrans:setBundle basename="properties.labels.operations.GmProcessTrans"/>

<%
String stroperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
String gridData=(String)request.getAttribute("strGridXmlData") ;
ArrayList dbResult=(ArrayList)request.getAttribute("dbResult") ;
ArrayList alResult=(ArrayList)request.getAttribute("alResult") ;
String strhScrnFrom = (String)request.getAttribute("hScreenFrom") ;
String strConsignId = (String)request.getAttribute("strConsignId") ;
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Process Transaction - Consigned Items</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link<%=strExtWebPath%> rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>



<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=stroperationsJsPath%>/GmProcessTrans.js"></script>

<script>
var objGridData;
objGridData ='<%=gridData%>';
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onload="javascript:fnOnPageLoad();">
<form name="frmOperDashBoardDispatch">
<input type="hidden" name="hId" value="" /> 
<input type="hidden" name="hFrom" value="" /> 
<input type="hidden" name="hConsignId" value="" /> 
<input type="hidden" name="hAction" value="" /> 
<input type="hidden" name="hType" value="" />
<input type="hidden" name="hOpt" value="" /> 
<input type="hidden" name="hMode" value="" />


<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
	<%if(dbResult !=null && dbResult.size()>0){ %>
	<tr>
		<td colspan="6" width="800px">
		<div id="dataGridDiv" style="" height="320px" width="850px"></div>
		</td>
	</tr>

	<tr>
		<td height="50" colspan="6">
			<div align="center" height="40"><fmtProcessTrans:message key="DIV_EXPORT_OPT"/> : 
				<img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="gridObj.toExcel('/phpapp/excel/generate.php');"><fmtProcessTrans:message key="DIV_EXCEL"/>  
			</div>
		</td>
	</tr>
	<%} %>
	<tr>
	  <td colspan="6"> 
	  <!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<jsp:include page="/operations/logistics/GmIncludeOpenBackOrder.jsp" />
	  </td>
	</tr>
</table>
<%if(dbResult.size() == 0 && alResult.size() > 0 && strhScrnFrom.equals("PROCESS_TXN_SCN")){ %>
	<div>
	<a href="#" onclick="fnMoveSetToPendProcess('<%=strConsignId%>','Load','MVPROC')"><font color="#3BAEF7"><u><fmtProcessTrans:message key="LBL_MOVE_PEND_PROCESS"/></u></font></a>
	</div>
<%} %>
</form>
</BODY>
</HTML>