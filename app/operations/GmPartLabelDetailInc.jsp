<%
/*********************************************************************************************************
 * File		 		: GmPartLabelDetailInc.jsp
 * Desc		 		: This screen is used to show Item Details of PTRD 
 * Version	 		: 1.0
 * author			: Arajan
**********************************************************************************************************/
%>
<%@ page language="java"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="frmPartLabelDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPartLabelDetailInc.jsp -->
<frmPartLabelDetail:setLocale value="<%=strLocale%>"/>
<frmPartLabelDetail:setBundle basename="properties.labels.operations.GmPartLabel"/>

<%
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<bean:define id="hmTransRules" name="<%=strFormName%>" property="hmTransRules" type="java.util.HashMap"></bean:define>
<bean:define id="StrStatus" name="<%=strFormName%>" property="statusfl" type="java.lang.String"></bean:define>
<bean:define id="txnid" name="<%=strFormName%>" property="txnid" type="java.lang.String"></bean:define>
<bean:define id="strTxnType" name="<%=strFormName%>" property="txntype" type="java.lang.String"></bean:define>
<bean:define id="gridData" name="<%=strFormName%>" property="gridXmlData" type="java.lang.String"> </bean:define>

<%

//HashMap hmTransRules = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("TRANS_RULES"));
HashMap hmTemp = new HashMap();
String strDropdown = "";
String strPickLocRule = GmCommonClass.parseNull((String)hmTransRules.get("PICK_LOC"));
String strRuleType =GmCommonClass.parseNull((String) hmTransRules.get("TRANS_RLTXN"));
String strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));

String strTransTypeID = GmCommonClass.parseNull((String) hmTransRules.get("TRANS_ID"));

String strRuleTransType = "";
if(strRuleType!=null && !strRuleType.equals("")){
	strRuleTransType = strRuleType;
}

%>

<script> 
var formname = '<%=strFormName%>';
</script>

<input type="hidden" id="RE_TXN" name="RE_TXN" value="<%=strRuleTransType%>">
<input type="hidden" id="TXN_TYPE_ID" name="TXN_TYPE_ID" value="<%=strTransTypeID%>">

<table width="100%" cellspacing="0" cellpadding="0">
	<tr><td>
			<div id="dataGridDiv" class="grid" ></div>
	</td></tr>
</table>
