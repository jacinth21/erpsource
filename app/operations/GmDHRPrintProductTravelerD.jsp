 <%
/**********************************************************************************
 * File		 		: GmDHRPrintProductTravelerD.jsp
 * Desc		 		: 
 * Version	 		: 1.0
 * author			: 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.servlets.GmCommonBarCodeServlet"%>
<%@ page import ="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page import ="org.apache.commons.beanutils.BasicDynaBean"%> 
<%@ include file="/common/GmHeader.inc"%>


<%@ taglib prefix="fmtDHRPrintProductTravelerD" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDHRPrintProductTravelerD.jsp -->

<fmtDHRPrintProductTravelerD:setLocale value="<%=strLocale%>"/>
<fmtDHRPrintProductTravelerD:setBundle basename="properties.labels.operations.GmDHRPrintProductTravelerD"/>

<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	 log = GmLogger.getInstance(GmCommonConstants.JSP);  // Instantiating the Logger  - write it to temp

	 strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	 strCssPath = GmCommonClass.getString("GMSTYLES");
	 strImagePath = GmCommonClass.getString("GMIMAGES");
	 strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	 strCommonPath = GmCommonClass.getString("GMCOMMON");
	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmDHRDetails = new HashMap();
	ArrayList alSubDHR = new ArrayList();
	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyRej = "";
	String strUserName = "";
	String strCreatedDate = "";
	String strQtyOrdered = "";
	boolean blQty = false;
	String strFlgChk = "";
	String strHideButton = GmCommonClass.parseNull((String)request.getParameter("HideButton"));
	String strCompanyLogo = GmCommonClass.parseNull((String)request.getParameter("CompanyLogo"));
	strCompanyLogo = strCompanyLogo.equals("")?"100800":strCompanyLogo;
	String strApplDateFmt = strGCompDateFmt;
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");

		strDHRId = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId = GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName = GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		strUserName = GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		//strCreatedDate = GmCommonClass.parseNull((String)hmDHRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplDateFmt);
		strQtyOrdered = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYORD"));
		strQtyRej = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ"));
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
		blQty = !strQtyOrdered.equals(strQtyRec)?true:false;
		strFlgChk = GmCommonClass.parseNull((String)hmDHRDetails.get("FLGPNUMCHK"));
	}
	int intSize = 0;
	HashMap hcboVal = null;
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hPOId" value="<%=strDHRId%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="hAction" value="">

	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+2><fmtDHRPrintProductTravelerD:message key="LBL_PRODUCT_TRAVELER"/>&nbsp;</font></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="4"></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td  height="30" class="RightText" align="right" colspan="3"><b><fmtDHRPrintProductTravelerD:message key="LBL_DHR_ID"/>:</b></td>
						<td class="RightText">&nbsp;<%=strDHRId%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintProductTravelerD:message key="LBL_ORIGINATOR"/>:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>
						<td class="RightText" align="right"><fmtDHRPrintProductTravelerD:message key="LBL_DATE"/>:</td>
						<td class="RightText">&nbsp;<%=strCreatedDate%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintProductTravelerD:message key="LBL_PART_NUMBER"/>:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightText" align="right">Description:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintProductTravelerD:message key="LBL_LOT_#"/>:</td>
						<td class="RightText">&nbsp;<%=strControlNum%></td>
						<td class="RightText" align="right">Vendor (if appl.):</td>
						<td class="RightText">&nbsp;<%=strVendName%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right"><fmtDHRPrintProductTravelerD:message key="LBL_WORK_ORDER_ID"/>:</td>
						<td class="RightText">&nbsp;<%=strWOId%></td>
						<td class="RightText" align="right"><fmtDHRPrintProductTravelerD:message key="LBL_PACKAGING_LIST_ENCLOSED"/>:</td>
						<td class="RightText">&nbsp;<input type="checkbox"><fmtDHRPrintProductTravelerD:message key="LBL_YES"/>&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintProductTravelerD:message key="LBL_NO"/></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4"></td></tr>

					<tr><td bgcolor="#666666" colspan="4"></td></tr>

					<tr>
						<td colspan="4" height="15" align="center">
							<font face="verdana" size="-4"><fmtDHRPrintProductTravelerD:message key="LBL_VENDOR_SUPPLIED_ATTACH_CERTIFICATION"/>
							</font>
						</td>
					</tr>
					<tr><td bgcolor="#666666" colspan="4"></td></tr>
				</table>
			</td>
		</tr>
	</table>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="13" width="1"></td>
			<td bgcolor="#666666" colspan="6" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="13" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="6">&nbsp;<b><fmtDHRPrintProductTravelerD:message key="LBL_RECEIPT"/></b></td></tr>
		<tr><td bgcolor="#666666" colspan="6"></td></tr>
		<tr>
			
			<td class="RightText" width="60" align="right">&nbsp;&nbsp;<%=strQtyOrdered%></td>
			<td class="RightText" width="80" height="23" align="left"><fmtDHRPrintProductTravelerD:message key="LBL_QTY_SPECIFIED"/></td>	
			<td class="RightText" width="40">&nbsp;&nbsp;<%=strQtyRec%>&nbsp;</td>
			<td class="RightText" width="100" align="left"><fmtDHRPrintProductTravelerD:message key="LBL_QTY_RECEIVED"/></td>		
			<td class="RightText" align="left"><fmtDHRPrintProductTravelerD:message key="LBL_QTY_DIFFERENT"/>:&nbsp;&nbsp;</td>
			<td class="RightText">&nbsp;</td>
		</tr>
		 <tr>
		 <td  colspan="4"></td>
		 <td bgcolor="#666666" colspan="2"></td></tr>
		<tr>
			<td height="24" colspan="6">&nbsp;</td>	
		</tr>
		<tr>
			<td  colspan="4"></td>
			<td  height="1" bgcolor="#666666" colspan="2" align="left"></td>
		</tr>
		
		<tr>
			<td height="30" colspan="4"></td>
			<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<fmtDHRPrintProductTravelerD:message key="LBL_CLEANINIG_PERFORMED"/>&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerD:message key="LBL_YES"/>&nbsp;&nbsp;
			<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerD:message key="LBL_NO"/>&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;<fmtDHRPrintProductTravelerD:message key="LBL_N/A"/></td>
		</tr>
		<tr>
			<td height="15" colspan="4"></td>
			<td colspan="2" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtDHRPrintProductTravelerD:message key="LBL_IMPLANTS_ONLY"/></td>
		</tr>
		<tr>
		 <td  colspan="4"></td>
		 <td bgcolor="#666666" colspan="2"></td></tr>
		<tr>
		<tr>
			<td height="30" colspan="6">&nbsp;</td>	
		</tr>
		<tr>
			<td colspan="6">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td rowspan="1" width="20" height="1"></td>
						<td bgcolor="#666666" height="1"></td>
						<td rowspan="1" width="20" height="1"></td>
						<td bgcolor="#666666" height="1"></td>
						<td rowspan="1" width="40" height="1"></td>
						<td bgcolor="#666666" height="1"></td>
						<td rowspan="1" width="20" height="1"></td>
						<td bgcolor="#666666" height="1"></td>
						
					</tr>
					<tr>
						<td rowspan="1" width="20" height="1"></td>
						<td class="RightText" align="center" height="15"><fmtDHRPrintProductTravelerD:message key="LBL_INITIALS"/></td>
						<td rowspan="1" width="20" height="1"></td>
						<td class="RightText" align="center"><fmtDHRPrintProductTravelerD:message key="LBL_DATE"/></td>
						<td rowspan="1" width="40" height="1"></td>
						<td class="RightText" align="center" height="15"><fmtDHRPrintProductTravelerD:message key="LBL_INITIALS"/></td>
						<td rowspan="1" width="20" height="1"></td>
						<td class="RightText" align="center"><fmtDHRPrintProductTravelerD:message key="LBL_DATE"/></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="6"></td></tr>
	</TABLE>

	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700" height="90">
		<tr>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b><fmtDHRPrintProductTravelerD:message key="LBL_INSPECTION"/></b></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td class="RightText" height="23" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintProductTravelerD:message key="LBL_YES"/></td>
			<td class="RightText" width="200">&nbsp;<fmtDHRPrintProductTravelerD:message key="LBL_SHEET_ATTACHED"/></td>
			<td class="RightText" align="right" rowspan="2" width="200"><fmtDHRPrintProductTravelerD:message key="LBL_INITIALS"/>:</td>
			<td class="RightText" rowspan="2">&nbsp;</td>
		</tr>

		<tr>
			<td class="RightText" rowspan="2" valign="top" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintProductTravelerD:message key="LBL_NO"/></td>
			<td class="RightText">&nbsp;</td>
		</tr>
		<tr>
			<td class="RightText" rowspan="2" height="23" height="30"></td>
			<td class="RightText" align="right" width="200"><fmtDHRPrintProductTravelerD:message key="LBL_DATE"/>:</td>
			<td class="RightText" width="200">&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
	</table>
	<BR>

	<table cellpadding="0" cellspacing="0" border="0" width="700" height="90">
		<tr>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b><fmtDHRPrintProductTravelerD:message key="LBL_PACKAGE_LABEL"/></b></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td class="RightText" height="23" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintProductTravelerD:message key="LBL_YES"/></td>
			<td class="RightText"width="200">&nbsp;</td>
			<td class="RightText" align="right" rowspan="2" width="200"><fmtDHRPrintProductTravelerD:message key="LBL_INITIALS"/>:</td>
			<td class="RightText" rowspan="2">&nbsp;</td>
		</tr>

		<tr>
			<td class="RightText" rowspan="2" valign="top" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintProductTravelerD:message key="LBL_NO"/></td>
			<td class="RightText">&nbsp;</td>
		</tr>
		<tr>
			<td class="RightText" rowspan="2" height="23" height="30"></td>
			<td class="RightText" align="right" width="200"><fmtDHRPrintProductTravelerD:message key="LBL_DATE"/>:</td>
			<td class="RightText" width="200">&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
	</table>
	<BR>

	<table cellpadding="0" cellspacing="0" border="0" width="700" height="90">
		<tr>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b><fmtDHRPrintProductTravelerD:message key="LBL_UPDATE_DHR_/_FINAL_INSPECTION"/></b></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td class="RightText" height="23" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintProductTravelerD:message key="LBL_YES"/></td>
			<td class="RightText"width="200">&nbsp;</td>
			<td class="RightText" align="right" rowspan="2" width="200"><fmtDHRPrintProductTravelerD:message key="LBL_INITIALS"/>:</td>
			<td class="RightText" rowspan="2">&nbsp;</td>
		</tr>

		<tr>
			<td class="RightText" rowspan="2" valign="top" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintProductTravelerD:message key="LBL_NO"/></td>
			<td class="RightText">&nbsp;</td>
		</tr>
		<tr>
			<td class="RightText" height="23" height="30"></td>
			<td class="RightText" align="right" width="200"><fmtDHRPrintProductTravelerD:message key="LBL_DATE"/>:</td>
			<td class="RightText" width="200">&nbsp;</td>
		</tr>
		<tr>
			<td class="RightText" height="23" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;<fmtDHRPrintProductTravelerD:message key="LBL_QTY_ACCEPTED"/>:&nbsp;[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtDHRPrintProductTravelerD:message key="LBL_QTY_REJECTED"/>:&nbsp;[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
	</table>
	<BR>

	<table cellpadding="0" cellspacing="0" border="0" width="700" height="90">
		<tr>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b><fmtDHRPrintProductTravelerD:message key="LBL_PLACED_INVENTORY"/></b></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td class="RightText" height="23" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintProductTravelerD:message key="LBL_YES"/></td>
			<td class="RightText"width="300">&nbsp;<fmtDHRPrintProductTravelerD:message key="LBL_SHEET_ATTACHED"/>()&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtDHRPrintProductTravelerD:message key="LBL_QTY"/> [&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]</td>
			<td class="RightText" align="right" rowspan="2"><fmtDHRPrintProductTravelerD:message key="LBL_SIGNATURE"/>:</td>
			<td class="RightText" rowspan="2">&nbsp;</td>
		</tr>

		<tr>
			<td class="RightText" rowspan="2" valign="top" height="30">&nbsp;&nbsp;&nbsp;<input type="checkbox"><fmtDHRPrintProductTravelerD:message key="LBL_NO"/></td>
			<td class="RightText">&nbsp;</td>
		</tr>
		<tr>
			<td class="RightText" rowspan="2" height="23" height="30"></td>
			<td class="RightText" align="right"><fmtDHRPrintProductTravelerD:message key="LBL_DATE"/>:</td>
			<td class="RightText" width="200">&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
	</table>
	<BR>
	<span class="RightText"><fmtDHRPrintProductTravelerD:message key="LBL_GMREVD"/></span>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td>
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmIncludeDateStamp.jsp" />
			</td>	
		</tr>
	</table>
	<% if(!strHideButton.equals("Y"))
			{ %>
	<div id="button">
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30">
			<BR>
			<BR>
			<fmtDHRPrintProductTravelerD:message key="BTN_PRINT" var="varPrint"/>
			<gmjsp:button value="&nbsp;${varPrint}&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnPrint();" />&nbsp;&nbsp;
			<fmtDHRPrintProductTravelerD:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Close" gmClass="button" buttonType="Load" onClick="window.close();" />&nbsp;
			</td>
		</tr>
	</table>
	<%} %>
	</div>
	

</FORM>
</BODY>

</HTML>
