<%
/**********************************************************************************
 * File		 		: GmControlNumberExceptionSetup.jsp
 * Desc		 		: This screen is used for save Control Number Exception 
 * Version	 		: 1.0
 * author			: Gopinathan Saravanan
************************************************************************************/
%>
 
<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtControlNumberExceptionSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>


<!-- operations\GmControlNumberExceptionSetup.jsp -->

<fmtControlNumberExceptionSetup:setLocale value="<%=strLocale%>"/>
<fmtControlNumberExceptionSetup:setBundle basename="properties.labels.operations.GmControlNumberExceptionSetup"/>

<%

String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");

String strVoidStatus=GmCommonClass.parseNull((String)request.getAttribute("lockfl"));
%>
<bean:define id="partNM" name="frmControlNumberException" property="partNM" type="java.lang.String"> </bean:define>
<bean:define id="strOpt" name="frmControlNumberException" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="txnId" name="frmControlNumberException" property="txnId" type="java.lang.String"> </bean:define>
<bean:define id="ctrlAsd_ID" name="frmControlNumberException" property="ctrlAsd_ID" type="java.lang.String"> </bean:define>
<bean:define id="leftLink" name="frmControlNumberException" property="leftLink" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>

<TITLE>Control Number Exception Setup</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmControlNumberException.js"></script>

<script>



//document.frmControlNumberException.leftLink.value = '<%=leftLink%>';
</script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("CONTROL_NUMBER_EXCEPTION_SETUP");
	boolean strDisabled=false;
	//String strDisableVoid="";
	if (leftLink.equals("false")  ){
		strDisabled = true;
	}	
	
	/* if (!strOpt.equals("edit")){
		strDisableVoid="true";
	} */
	
%>
</HEAD>
<BODY leftmargin="20" topmargin="10" onload="fnSetupLoad();">
<form name="frmControlNumberException" method="post" action="/gmControlNumber.do">
<html:hidden name="frmControlNumberException" property="strOpt" value="<%=strOpt%>"/>
<html:hidden name="frmControlNumberException" property="txnId" value="<%=txnId%>"/>  
<html:hidden name="frmControlNumberException" property="ctrlAsd_ID" value="<%=ctrlAsd_ID%>" />
<html:hidden name="frmControlNumberException" property="strMsg" />
<html:hidden name="frmControlNumberException" property="leftLink" value='<%=leftLink%>'/>
<table border="0" class="DtTable680" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="RightDashBoardHeader">&nbsp;<fmtControlNumberExceptionSetup:message key="LBL_CN_OVERRIDES"/></td>
		<td align="right" class="RightDashBoardHeader"><fmtControlNumberExceptionSetup:message key="IMG_ALT_HELP" var = "varHelp"/>
		<img id='imgEdit' style='cursor: hand'
				src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16'
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<html:messages id="msg" message="true" property="success">
				<font style="color:green"><b><bean:write name="msg"/></b></font><br>
			</html:messages>
		</td>	
	</tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="30"><font color="red">*</font><fmtControlNumberExceptionSetup:message key="LBL_PART_NUMBER"/>:&nbsp;</td>
		<td width="60%" align="left">&nbsp;<html:text name="frmControlNumberException" property="partNM"  size="31" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" disabled="<%=strDisabled%>" /></td>
	</tr>
	<tr class="Line"></tr>
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="30"><font color="red">*</font><fmtControlNumberExceptionSetup:message key="LBL_CONTROL_NUMBER"/>:&nbsp;</td>
		<td width="60%" align="left">&nbsp;<html:text name="frmControlNumberException" property="controlNM"  size="31" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" maxlength="15" /></td>
	</tr>
	<tr>
			<td colspan="2" align="center" height="25">&nbsp;
               <jsp:include page="/common/GmIncludeLog.jsp" >
                	<jsp:param name="FORMNAME" value="frmControlNumberException"/>
					<jsp:param name="ALNAME" value="alLogReasons" />
					<jsp:param name="LogMode" value="Edit" />																					
				</jsp:include>	
			</td>
	</tr>
			<%-- <%
				String strCtrlAsd_ID="fnVoid('"+ctrlAsd_ID+"');";
			%> --%>
	<tr>
		<td colspan="2" align="center" height="30">
		<fmtControlNumberExceptionSetup:message key="BTN_SUBMIT" var="varSubmit"/>
		<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" onClick="fnSubmit();" buttonType="Save"/>
		<fmtControlNumberExceptionSetup:message key="BTN_CLOSE" var="varClose"/>
		<gmjsp:button value="&nbsp;${varClose}&nbsp;" gmClass="button" onClick="fnClose();" controlId="btn_close" buttonType="Save" />
		<!-- Commenting the code, since the control number is saving in T2550_PART_CONTROL_NUMBER table hereafter, which is not having void flag -->		
		<%-- <gmjsp:button value="&nbsp;Void&nbsp;" gmClass="button" onClick="<%=strCtrlAsd_ID%>" controlId="btn_void" disabled="<%=strDisableVoid%>" buttonType="Save"/> --%>
		</td>	
	</tr>
	<logic:notEqual name="frmControlNumberException" property="strMsg" value="">
	<tr>
	<td colspan="2" align="center" height="30">
	<bean:write name="frmControlNumberException" property="strMsg"/>
	</td>
	</tr>
	</logic:notEqual>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>