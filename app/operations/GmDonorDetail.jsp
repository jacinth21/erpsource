<%
/**********************************************************************************
 * File		 		: GmDonorDetail.jsp
 * Desc		 		: This JSP is for the section Donor Details
 * Version	 		: 1.0
 * author			: arajan
************************************************************************************/
%>

<%@ include file="/common/GmHeader.inc"%>

<%@ taglib prefix="fmtDonorDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmDonorDetail.jsp -->

<fmtDonorDetail:setLocale value="<%=strLocale%>"/>
<fmtDonorDetail:setBundle basename="properties.labels.operations.GmDonorDetail"/>

<% 
String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>

<bean:define id="qtyRec" name="<%=strFormName %>" property="qtyRec" type="java.lang.String"> </bean:define>
<bean:define id="qtypack" name="<%=strFormName %>" property="qtypack" type="java.lang.String"> </bean:define>
<bean:define id="totalqty" name="<%=strFormName %>" property="totalqty" type="java.lang.String"> </bean:define>
<bean:define id="screenType" name="<%=strFormName%>" property="screenType" type="java.lang.String" ></bean:define>

<% 
	String strWikiTitle = "";
	if(!screenType.equals("DHRDetail")){
		strWikiTitle = GmCommonClass.getWikiTitle("DONOR_DETAILS");
	}else{
		strWikiTitle = GmCommonClass.getWikiTitle("DHR_SPLIT");
	}
%>

<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<TITLE> Globus Medical: Donor Details </TITLE>

<script>
var recQty = '<%=qtyRec%>';
var packQty = '<%=qtypack%>' ;
var totlalQty = '<%=totalqty%>'; /* Total qty received after NCMR closeout */
</script>

<BODY leftmargin="20" topmargin="10">
<html:hidden property="partNum" name="<%=strFormName %>"/>
<html:hidden property="qtyRec" name="<%=strFormName %>"/>
<html:hidden property="donorNo" name="<%=strFormName %>"/>
<html:hidden property="expDate" name="<%=strFormName %>"/>
<html:hidden property="hMode" name="<%=strFormName %>"/>

	<table class="DtTable500" border="0" cellspacing="0" cellpadding="0">
		
		<tr>
		<%if(!screenType.equals("DHRDetail")){ %>
			<td colspan="5">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="24" class="RightDashBoardHeader" colspan="6">&nbsp;<b><fmtDonorDetail:message key="LBL_DONOR_DETAILS"/></b></td>
						<td height="24" class="RightDashBoardHeader" align="right"><fmtDonorDetail:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
					</tr>
				</table>
			</td>
		<%}else{ %>
			<td colspan="5">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="24" class="RightDashBoardHeader" colspan="6">&nbsp;<b><fmtDonorDetail:message key="LBL_DHR_SPLIT"/></b></td>
						<td height="24" class="RightDashBoardHeader" align="right"><fmtDonorDetail:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
					</tr>
				</table>
			</td>
		<%} %>
		</tr>
		<tr><td class="Line" colspan="6"></td></tr> 
		<tr>
			<td class="RightText" HEIGHT="20"  align="right" width="30%">&nbsp;<b><fmtDonorDetail:message key="LBL_PART_NUMBER"/>:</b></td>
			<td width="20%">&nbsp;<bean:write name="<%=strFormName %>" property="partNum"/></td>
			<td class="RightText" HEIGHT="20" align="right" width="30%">&nbsp;<b><fmtDonorDetail:message key="LBL_QTY_TO_REC"/>:</b></td>
			<td width="20%">&nbsp;<bean:write name="<%=strFormName %>" property="qtyRec"/></td>
		</tr>
		<tr><td class="LLine" colspan="6"></td></tr> 	
		<tr  class="Shade">
			<%-- <td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtDonorDetail:message key="LBL_DONOR_#"/>:</b></td>
			<td>&nbsp;<bean:write name="<%=strFormName %>" property="donorNo"/></td> --%>
			<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtDonorDetail:message key="LBL_EXPIRY_DATE"/>:</b></td>
			<td colspan="3">&nbsp;<bean:write name="<%=strFormName %>" property="expDate"/></td>
		</tr>
				
	</table>

<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>