 <%
/**********************************************************************************
 * File		 		: GmLoanerByDist.jsp
 * Desc		 		: This screen is used to display display Loaner details by Distributor
 * Version	 		: 1.0
 * author			: Joe P Kumar 
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%> 
<!-- \operations\GmLoanerByDist.jsp--> 

<%

	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	HashMap hmReturn = new HashMap();
	ArrayList alDrillDown = new ArrayList();
	ArrayList alLnTyp	= new ArrayList();
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	
	gm.checkSession(response,session);
	
	String strHeader = (String)request.getAttribute("strHeader");
	String strhAction = (String)request.getAttribute("hAction");
	String strSetId = GmCommonClass.parseNull((String)request.getAttribute("hSetId"));
	String strDistId = GmCommonClass.parseNull((String)request.getAttribute("hDistId"));
	strhAction = (strhAction == null)?"Load":strhAction;
	String strType = GmCommonClass.parseNull((String)request.getAttribute("Cbo_lnType"));
	
	String strWikiTitle = "";
	if (strhAction.equals("LoanerByDistBySet")){
		strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("USAGE_BY_SET"));
	}else {
		strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("USAGE_BY_DISTRIBUTOR"));
	}
	alLnTyp = (ArrayList)request.getAttribute("LOANERTYP");
	// To Generate the Crosstab report
	gmCrossTab.setHeader("Loaner By Distributor");
	
	gmCrossTab.setValueType(0);
	
	// Setting Display Parameter
	gmCrossTab.setAmountWidth(80);
	gmCrossTab.setNameWidth(400);
	gmCrossTab.setRowHeight(22);
	gmCrossTab.setLinkRequired(true);	// To Specify the link
	gmCrossTab.setGeneralHeaderStyle("ShadeDarkBlueTD");	
	gmCrossTab.addStyle("Name","ShadeMedGrayTD","ShadeDarkGrayTD");
	gmCrossTab.setColumnLineStyle("borderDark");
	gmCrossTab.addLine("Name");
	gmCrossTab.reNameColumn("Open_Requests","Open Requests");
	
	if (strhAction.equals("LoanerByDistBySet"))
	alDrillDown.add("LoanerDist");	
	else
	alDrillDown.add("LoanerSet");
	
	//if (strhAction.equals("LoanerByDist"))
	gmCrossTab.setDecorator("com.globus.crosstab.beans.GmLoanerUsageDetailsDecorator");
	gmCrossTab.setDrillDownDetails(alDrillDown);
	gmCrossTab.setRowHighlightRequired(true);
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Loaner By Distributor</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmLoanerByDist.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function fnLoad()
{
	document.frmLoaner.Cbo_FromMonth.value = '<%=request.getAttribute("Cbo_FromMonth")%>';
	document.frmLoaner.Cbo_ToMonth.value = '<%=request.getAttribute("Cbo_ToMonth")%>';
	document.frmLoaner.Cbo_FromYear.value = '<%=request.getAttribute("Cbo_FromYear")%>';
	document.frmLoaner.Cbo_ToYear.value = '<%=request.getAttribute("Cbo_ToYear")%>';
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<FORM name="frmLoaner" method="post" action = "<%=strServletPath%>/GmLoanerPartRepServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hDistId" value="<%=strDistId%>">
<input type="hidden" name="hSetId" value="<%=strSetId%>">
<input type="hidden" name="hLnTyp" value="<%=strType%>">

<TABLE cellSpacing=0 cellPadding=0  border=0 class="DtTable800">
<TBODY>
	<tr>
		<td rowspan="10" class=Line noWrap width=1></td>
		<td class=Line noWrap height=1></td>
		<td rowspan="10" class=Line noWrap width=1></td>
	</tr>
	<tr><td>
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr class=Line><td height=25 class=RightDashBoardHeader><%=strHeader %> </td>
	<td  height="25" class="RightDashBoardHeader" align="right">
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	</td> 
	</tr> </table> </td> </tr>
	<tr class=Line><td noWrap height=1></td></tr>
	<!-- Addition Filter Information -->
	<% 
	if (strhAction.equals("LoanerByDistBySet")){
	%>
	<tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
		<td height=25  noWrap class="RightTableCaption" align="left"><font color="red">*</font>Type:			
			 <gmjsp:dropdown controlName="Cbo_lnType"  seletedValue="<%=strType%>"  defaultValue= "[Choose One]"	
			value="<%=alLnTyp%>" codeId="CODENMALT" codeName="CODENM" />
		</td>			
	</tr>
	<% 
	}
	%>
	<tr>
		<td><jsp:include page="/include/GmFromToDateConsign.jsp" />  </td>
	</tr>
	<tr class=borderDark>
		<td noWrap height=1>  </td>
	</tr>
		
	<tr>
		<td> <%=gmCrossTab.PrintCrossTabReport(hmReturn) %></td>
	</tr>		
	<tr class=borderDark>
			<td noWrap height=1>  </td>
	</tr>
</TBODY>
</TABLE>
	<%@ include file="/common/GmFooter.inc"%>
</FORM>
	
</BODY>

</HTML>
