 <%
/**********************************************************************************
 * File		 		: GmDHRModify.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
 * ---------------------------------------------------
 * Version	 		: 1.1
 * author			: Basudev T Vidyasankar
 * Date				: 12/18/2008 
************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Calendar,java.util.TimeZone" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%> 
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ taglib prefix="fmtDHRModify" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- operations\GmDHRModify.jsp -->

<fmtDHRModify:setLocale value="<%=strLocale%>"/>
<fmtDHRModify:setBundle basename="properties.labels.operations.GmDHRModify"/>


<%
	String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strApplDateFmt = strGCompDateFmt;
	
	String strSkipFl = GmCommonClass.parseNull((String)request.getAttribute("hSkipFl"));
	String strLotCode = GmCommonClass.parseNull((String)request.getAttribute("hLotCode"));
	if(strLotCode.equals(""))
		strLotCode=GmCommonClass.parseNull((String)request.getParameter("hLotCode"));
	HashMap hmDHRDetails = new HashMap();
	ArrayList alEmpList = new ArrayList();
	ArrayList alSubDHR = new ArrayList();

	String strShade = "";

	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyInspected = "";
	String strQtyReject = "";
	String strActionTaken = "";
	String strCodeID = "";
	String strSelected = "";
	String strReceivedBy = "";
	String strInspectedBy = "";
	String strInspectedByNm = "";
	String strQtyPackaged = "";
	String strPackagedBy = "";
	String strPackagedByNm = "";
	String strQtyInShelf = "";
	String strVerifiedBy = "";
	String strVerifiedByNm = "";
	String strNCMRId = "";
	String strCriticalFl = "";
	String strCreatedDate = "";
	String strRejReason = "";
	String strCloseQty = "";
	String strNCMRFl = "";
	String strReceivedTS = "";
	String strInspectedTS = "";
	String strPackagedTS = "";
	String strVerifiedTS = "";
	String strSubComFl = "";
	String strSterFl = "";
	String strChecked = "";
	String strLabelFl = "";
	String strPackFl = "";
	String strPartSterFl = "";
	String strFooter = "";
	String strDocRev = "";
	String strDocActiveFl = "";
	String strFARFl = "";
	String strPackSlip = "";
	String strComments = "";
	String strStatusFl = "";
	String strWOType = "";
	String strCnumFmt = "";
	String strExpiryDate = "";
	String strToday = "";
	String strJulianRule="";
	String strProdClass="";
	String strProdType = "";
	String strControlNumberDisabled = "";
	String strPrevQty = "";
	String strDonorNum = "";
	String strSerialNumFl = "";
	String strUDI = "";
	String strVDfl = "";
	String strDI = "";
	String strUDIDisabled = "disabled";
	String strUDIFormat = "";
	
	int intNCMRFl = 0;
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
	
		strDHRId = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId = GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName = GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		strReceivedBy = GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		strQtyInspected = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYINSP"));
		strQtyReject = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ"));
		strActionTaken = GmCommonClass.parseNull((String)hmDHRDetails.get("ACTTKN"));
		strInspectedBy = GmCommonClass.parseNull((String)hmDHRDetails.get("INSPBY"));
		strInspectedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("INSPBYNM"));
		strPackagedBy = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKBY"));
		strPackagedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKBYNM"));
		strQtyPackaged = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYPACK"));
		strQtyInShelf = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYSHELF"));
		strVerifiedBy = GmCommonClass.parseNull((String)hmDHRDetails.get("VERIFYBY"));
		strVerifiedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("VERIFYBYNM"));
		strNCMRId = GmCommonClass.parseNull((String)hmDHRDetails.get("NCMRID"));
		strCriticalFl = GmCommonClass.parseNull((String)hmDHRDetails.get("CFL"));
		//strCreatedDate = GmCommonClass.parseNull((String)hmDHRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strGCompDateFmt);
		strRejReason = GmCommonClass.parseNull((String)hmDHRDetails.get("REJREASON"));
		strNCMRFl = GmCommonClass.parseZero((String)hmDHRDetails.get("NCMRFL"));
		strCloseQty = GmCommonClass.parseNull((String)hmDHRDetails.get("CLOSEQTY"));
		strReceivedTS = GmCommonClass.parseNull((String)hmDHRDetails.get("RECTS"));
		strInspectedTS = GmCommonClass.parseNull((String)hmDHRDetails.get("INSTS"));
		strPackagedTS = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKTS"));
		strVerifiedTS = GmCommonClass.parseNull((String)hmDHRDetails.get("VERTS"));
		strSubComFl = GmCommonClass.parseNull((String)hmDHRDetails.get("SUBFL"));
		strSterFl = GmCommonClass.parseNull((String)hmDHRDetails.get("STERFL"));
		strChecked = GmCommonClass.parseNull((String)hmDHRDetails.get("SKIPLBLFL"));
		strPackSlip = GmCommonClass.parseNull((String)hmDHRDetails.get("PKSLIP"));
		strComments = GmCommonClass.parseNull((String)hmDHRDetails.get("COMMENTS"));
		strCnumFmt = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUMFMT"));
		//strExpiryDate = GmCommonClass.parseNull((String)hmDHRDetails.get("EXPDT"));
		strExpiryDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("EXPDT"),strGCompDateFmt);
		strProdType = GmCommonClass.parseNull((String)hmDHRDetails.get("PRODTYPE"));
		strPrevQty = GmCommonClass.parseNull((String)hmReturn.get("hPrevQty"));
		strDonorNum 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DONORNUMBER"));
		strSerialNumFl = GmCommonClass.parseNull((String)hmDHRDetails.get("SERIALNUMNEEDFL"));
		strUDI = GmCommonClass.parseNull((String)hmDHRDetails.get("UDINO"));
		strVDfl = GmCommonClass.parseNull((String)hmDHRDetails.get("VENDRDSNFL"));
		strDI = GmCommonClass.parseNull((String)hmDHRDetails.get("DINUMBR"));
		strUDIFormat = GmCommonClass.parseNull((String)hmDHRDetails.get("UDIFORMAT"));
		 if(strProdType.equals("100845")){  
				strControlNumberDisabled = "disabled";
		}
		 
		strToday = GmCommonClass.parseNull((String)request.getAttribute("hTodaysDate"));
		
		GmPurchaseBean gmPurchaseBean= new GmPurchaseBean(gmDataStoreVO);

		/***Added as part of DHR Issue 1366****Start***/
		strStatusFl = GmCommonClass.parseNull((String)hmDHRDetails.get("SFL"));
		/***Added as part of DHR Issue 1366****End***/
		
		/***Added as part of DHR Issue 1438****Start***/
		strWOType = GmCommonClass.parseNull((String)hmDHRDetails.get("WOTYPE"));
		/***Added as part of DHR Issue 1438****End***/
		
		strLabelFl = strChecked.equals("1")?"Yes":"-";
		strChecked = strChecked.equals("1")?"checked":"";
		strPackFl = GmCommonClass.parseNull((String)hmDHRDetails.get("SKIPPACKFL"));
		strPackFl = strPackFl.equals("1")?"Yes":"-";
		
		strPartSterFl = GmCommonClass.parseNull((String)hmDHRDetails.get("PCLASS"));
		strProdClass= strPartSterFl;
		strPartSterFl = strPartSterFl.equals("4030")?"1":"";

		strFooter = GmCommonClass.parseNull((String)hmDHRDetails.get("FOOTER"));
		String strArr[] = strFooter.split("\\^");
		strFooter = strArr[0];
		strDocRev = strArr[1];
		strDocActiveFl = strArr[2];
		
		strFARFl = GmCommonClass.parseNull((String)hmDHRDetails.get("FARFL"));
								
		intNCMRFl = Integer.parseInt(strNCMRFl);
		
		alEmpList = (ArrayList)hmReturn.get("EMPLIST");
	}

	int intSize = 0;
	HashMap hcboVal = null;
	
    /* Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    String DATE_FORMAT = "MM/dd/yyyy";
    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);
    sdf.setTimeZone(TimeZone.getDefault()); 
    String strDate = sdf.format(cal.getTime());*/
	
    // For OUS code merge
    	
	GmCalenderOperations gmCal = new GmCalenderOperations();
    String strDate = "";
	strDate = gmCal.getCurrentDate(strGCompDateFmt);


	
	strJulianRule = GmCommonClass.parseNull((String)request.getAttribute("JULIANRULE"));
	/*String strReadOnly="";
	if(strDHRId.indexOf("MWO")>0){
		strReadOnly="disabled";
	}*/
	//The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);	
	if(strVDfl.equals("N")) {
		strUDIDisabled = "";
	}
%>

<%@page import="com.globus.operations.beans.GmPurchaseBean"%><HTML>
<HEAD>
<TITLE> Globus Medical: DHR Update </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmDHRModify.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>

<script>
var gStrDHRId = "<%=strDHRId%>";
var gStrVendorId = "<%=strVendorId%>";
var gStrAction = "<%=strAction%>";
var gStrWOId = "<%=strWOId%>";
var gStrDocRev = "<%=strDocRev%>";
var gStrDocActiveFl = "<%=strDocActiveFl%>";
var gStrNCMRId = "<%=strNCMRId%>";
var gStrWOType = "<%=strWOType%>";
var gStrDate = "<%=strDate%>";
var gStrLotCode = "<%=strLotCode%>";
var gStrSkipFl = "<%= strSkipFl %>"; 
var gStrCnumFmt = "<%=strCnumFmt%>";
var julianRule = "<%=strJulianRule%>";
var countryCode = "<%=strCountryCode%>";
var format = "<%=strApplDateFmt%>";
var prodType = "<%=strProdType%>";
var qtyflag = false;
var companyInfoObj = '<%=strCompanyInfo%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad()">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmDHRModifyServlet?companyInfo=<%=strCompanyInfo %>">
<input type="hidden" name="hDHRId" value="<%=strDHRId%>">
<input type="hidden" name="hAction" value="<%=strAction%>">
<input type="hidden" name="hPartNum" value="<%=strPartNum%>">
<input type="hidden" name="hWOId" value="<%=strWOId%>">
<input type="hidden" name="hRecQty" value="<%=strQtyRec%>">
<input type="hidden" name="hRejQty" value="<%=strQtyReject%>">
<input type="hidden" name="hManfDt" value="">
<input type="hidden" name="hToDt" value="">
<input type="hidden" name="hTodaysDate"value="<%=strDate%>">
<input type="hidden" name="hSterFl" value="<%=strSterFl%>">
<input type="hidden" name="hPartSterFl" value="<%=strPartSterFl%>">
<input type="hidden" name="hLotCode" value="<%=strLotCode%>">
<input type="hidden" name="hProdclass" value="<%=strProdClass%>">
<input type="hidden" name="hPrevQty" value="<%=strPrevQty%>">
<input type="hidden" name="hDINum" value="<%=strDI%>">
<input type="hidden" name="hUDINum" value="<%=strUDI%>">
<input type="hidden" name="hUDIFormat" value="<%=strUDIFormat%>">

<!-- Added as part of DHR Issue 1366 -->
<input type="hidden" name="hStatusFl" value="<%=strStatusFl%>">


	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
			<td height="25" class="RightDashBoardHeader"><fmtDHRModify:message key="LBL_DEVICE_HISTORY_RECORD"/></td>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
		</tr>
			<td bgcolor="#666666" colspan="3"></td>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="4">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td width="120" class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_DHR_ID"/>:</td>
									<td width="140" class="RightText">&nbsp;<b><%=strDHRId%></b></td>
									<td width="100" class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_BIN_#"/>:</td>
									<td width="300" class="RightText">&nbsp;</td>
								</tr>
								<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
								<tr class="shade">
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_PART_NUMBER"/>:</td>
									<td class="RightText">&nbsp;<%=strPartNum%></td>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_DESCRIPTION"/>:</td>
									<td class="RightText"  colspan="3">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>									
								</tr>
								<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
								<tr>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_CREATED_DATE"/>:</td>
									<td class="RightText">&nbsp;<%=strCreatedDate%></td>
									<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_VENDOR"/>:</td>
									<td class="RightText">&nbsp;<%=strVendName%></td>
								</tr>
							</table>
						 </td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;<u><fmtDHRModify:message key="LBL_EDIT_RECIEVING_INFO"/></u></td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_QTY_RECIEVED"/>:</td>
<%if(strDHRId.indexOf("MWO")>0){ %>
						<td class="RightText">&nbsp;<%=strQtyRec%>
						<input class="RightText" size="4" type="text" name="Txt_QtyRec" value="<%=strQtyRec%>" style="visibility: hidden;" >
						</td>
<%
}else{ %> 
						<td class="RightText">&nbsp;<input class="RightText" size="4" type="text" name="Txt_QtyRec" value="<%=strQtyRec%>" ></td>
<%} %>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_CONTROL_NUMBER"/>:</td>
						<td class="RightText">&nbsp;<input class="RightText" size="10" type="text" <%=strControlNumberDisabled%> name="Txt_CNum" onBlur="fnAddUDIDetails();" value="<%=strControlNum%>"></td>						
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<!-- Custom tag lib code modified for JBOSS migration changes -->
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_PACKING_SLIP"/>:</td>
						<td class="RightText">&nbsp;<input class="RightText" size="15" type="text" name="Txt_PkSlip" value="<%=strPackSlip%>"></td>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_EXPIRY_DATE"/>:</td>
						<%if (strPartSterFl.equals("1")) {%>
                            <td class="RightText">&nbsp;<gmjsp:calendar textControlName="Txt_ExpDate" textValue="<%=(strExpiryDate.length()==0?null: (new java.sql.Date(GmCommonClass.getStringToDate(strExpiryDate,strApplDateFmt).getTime())))%>"  onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');fnAddUDIDetails();"/></td>
                        <%} else {%>
                            <td class="RightText">&nbsp;<input type="text" size="10" value="" id="Txt_ExpDate" name="Txt_ExpDate" disabled onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff'); fnAddUDIDetails();" align="absmiddle"/>&nbsp;&nbsp;<img src=<%=strImagePath%>/nav_calendar.gif onClick="fnAddUDIDetails();"  border=0 align="absmiddle" height=18 width=19></img></td>
                        <%}%>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_UDI"/>:</td>
						<td class="RightText">&nbsp;<input class="RightText" size="46" readonly type="text" id="udi" name="Txt_UDI" value="<%=strUDI%>"></td>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_REG_UDI"/>:</td>
						<td height="25" width="12%">&nbsp;<input type="checkbox" size="50" onclick="fnCheck(this);" <%=strUDIDisabled%> name="Chk_VDfl" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');">
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr class="shade">
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_COMMENTS"/>:</td>
						<td class="RightText" colspan="3">&nbsp;<textarea class="RightText" rows=3 cols=80 name="Txt_Comments" value="<%=strComments%>"><%=strComments%></textarea></td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>					
					<tr>
						<td class="RightTableCaption" align="right">&nbsp;<fmtDHRModify:message key="LBL_RECEIVED_BY"/>:</td>
						<td class="RightText">&nbsp;<%=strReceivedBy%></td>
						<td class="RightTableCaption" align="right" HEIGHT="24"><fmtDHRModify:message key="LBL_RECIEVED_TIMESTAMP"/>:</td>
						<td class="RightText" width="300">&nbsp;<%=strReceivedTS%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
				</table>
			</td>
		</tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
		<tr>
			<td colspan="3" align="center" height="30">
			<fmtDHRModify:message key="BTN_SUBMIT" var="varSubmit"/>			
				<gmjsp:button value="${varSubmit}" name="Btn_Update" gmClass="button" onClick="fnUpdate();" buttonType="Save" />&nbsp;&nbsp;
				
				<%if(strAction.equals("UpdateRec") && ((strProdType.equals("100845") && !strDonorNum.equals("")) || strSerialNumFl.equals("Y"))){ //Button should show only once we submit the form and also if the part is of tissue type %>
					<fmtDHRModify:message key="BTN_REGENERATE_LOT" var="varRegenerate"/>	
					<gmjsp:button value="${varRegenerate}" name="Btn_Regenerate" gmClass="button" onClick="javascript:fnCallDonor()" buttonType="Load" />&nbsp;&nbsp;
				<%} %>
			</td>
		<tr>
		</tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>		
    </table>

</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
