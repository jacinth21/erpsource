<%
/**********************************************************************************
 * File		 		: GmBODHRReportInc.jsp
 * Desc		 		: This screen is used for displaying the part details 
 * Version	 		: 1.0
 * author			: Brinal
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass" %>

<%@ taglib prefix="fmtBODHRReportInc" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<!-- operations\GmBODHRReportInc.jsp -->

<fmtBODHRReportInc:setLocale value="<%=strLocale%>"/>
<fmtBODHRReportInc:setBundle basename="properties.labels.operations.GmBODHRReportInc"/>

<%
String formName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
formName = formName.equals("") ?  "requestScope.ldhrPartResult" : "requestScope."+formName+".ldhrPartResult" ;	 
log.debug("formName :"+ formName);
boolean sort = GmCommonClass.parseNull(request.getParameter("FORMNAME")).equals("") ?false:true;
%>

<HTML>

<HEAD>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
	 
<TITLE> DHR Report - Backorder Part Details </TITLE> 
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script>

function fnopenGmPartInvReport(value){
	var strurl = "/GmPartInvReportServlet?hAction=Go&hOpt=MultipleParts&hPartNum="+encodeURIComponent(value);
	windowOpener(strurl,"GmPartInvReportServlet","resizable=yes,scrollbars=yes,top=300,left=200,width=1000,height=600");
}

function fnopengmOperDashBoard(value){
	var strurl = "/gmOprDashBoard.do?method=openDashboardDHR&PartNum="+value;	
	windowOpener(strurl,"DashBoard","resizable=yes,scrollbars=yes,top=300,left=200,width=1000,height=600");
}
function fnopener(value){
	var strurl = "/gmOperDashBoardDispatch.do?method=dashboardDHR&PartNum="+value+"&ajaxFlag=false";	
	windowOpener(strurl,"DashBoard","resizable=yes,scrollbars=yes,top=300,left=200,width=1000,height=600");
}	
</script>
<BODY>			
		    <display:table name="<%=formName%>" class = "its" id="currentRowObject" export="<%=sort%>" requestURI="/gmBODHRReport.do?method=loadBODHRReport" decorator="com.globus.displaytag.beans.DTBODHRWrapper">		     
				<fmtBODHRReportInc:message key="DT_PART_#" var="varPart"/>
				<display:column property="PNUM" title="${varPart}" style="width:100"/>		
				<fmtBODHRReportInc:message key="DT_DESC" var="varDesc"/>
				<display:column property="DESCRIPTION" title="${varDesc}" class="alignleftt"  sortable="true" />
				<fmtBODHRReportInc:message key="DT_DHR_QTY" var="varDHRQty"/>
				<display:column property="DHRQTY" title="${varDHRQty}" class="alignleftt"  sortable="true" />
				<fmtBODHRReportInc:message key="DT_FORECAST_MONTH" var="varForecastMonth"/>
				<display:column property="FORECAST_QTY" title="${varForecastMonth}" class="alignright" format="{0,number,#,###,###}" style="width:50" sortable="true"/>
			  	<fmtBODHRReportInc:message key="DT_SET_NEED_MONTH" var="varSetNeedMonth"/>
			  	<display:column property="SET_NEED" title="${varSetNeedMonth}" class="alignright" style="width:60" sortable="true"/>			  	
			  	<fmtBODHRReportInc:message key="DT_TO_SHELF" var="varToShelf"/>
			  	<display:column property="TO_SHELF" title="${varToShelf}" class="alignright" sortable="true" />
			  	<fmtBODHRReportInc:message key="DT_TO_BULK" var="varToBulk"/>
			  	<display:column property="TO_BULK" title="${varToBulk}" class="alignright" sortable="true" />
			  	<fmtBODHRReportInc:message key="DT_TO_RM" var="varToRM"/>
			  	<display:column property="TO_RM" title="${varToRM}" class="alignright" sortable="true" />		
 			</display:table> 
</body>
</html>