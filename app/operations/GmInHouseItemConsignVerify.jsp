 <%
/**********************************************************************************
 * File		 		: GmInHouseItemConsignVerify.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtInHouseItemConsignVerify" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<!-- GmInHouseItemConsignVerify.jsp -->
<fmtInHouseItemConsignVerify:setLocale value="<%=strLocale%>"/>
<fmtInHouseItemConsignVerify:setBundle basename="properties.labels.operations.GmInHouseItemConsignVerify"/>


<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.operations.GmInHouseItemConsignVerify", strSessCompanyLocale);
	String strPendingVerification = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PENDING_VERIFICATION"));
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmTransRules = (HashMap)request.getAttribute("TRANS_RULES");
	String strRuleType =GmCommonClass.parseNull((String) hmTransRules.get("TRANS_RLTXN"));
	String strRuleAjax =(String) hmTransRules.get("ICE_RULE_AJAX");
	String strIncludeRuleEng = GmCommonClass.parseNull((String) hmTransRules.get("INCLUDE_RULE_ENGINE"));
	String strRuleConsignType =GmCommonClass.parseNull((String) hmTransRules.get("TRANS_ID"));
	String strRulVerfyMsg=GmCommonClass.parseNull((String)hmTransRules.get("IC_VERIFY_MSG"));
	String strhAction = (String)request.getAttribute("hAction") == null?"":(String)request.getAttribute("hAction");
	String strConsignId = (String)request.getAttribute("hConsignId") == null?"":(String)request.getAttribute("hConsignId");
	String strTransTypeId = GmCommonClass.parseNull((String)request.getAttribute("TxnTypeID"));
	String strOpt=GmCommonClass.parseNull((String)hmTransRules.get("STR_OPT"));
	String strVerfyMsg="This consignment has been verified and taken out of Inventory. You must NOT make any more changes to the contents of this consignment. If you do need to make changes, please contact the Logistics Manager/System Admin.";
	String strSelected = "";
	String strCodeID = "";
	String strChecked = "";
	String strProjId =	"";
	String strSetId = "";
	String strSetNm = "";
	String strDesc = "";
	String strTemp = "";
	String strShade = "";

	String strSetName= "";
	String strAccName = "";
	String strUserName = "";
	String strIniDate = "";
	String strItemQty = "";
	String strControl = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strQty = "";
	String strConsignType = "";
	String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REPACKAGE_ITEMS_VERIFY"));
	String strLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_REPACKAGING"));
	String strComments = "";
	String strType ="";
	String strRAId = "";
	String strFlag = (String)request.getAttribute("hShipFl") == null?"":(String)request.getAttribute("hShipFl");
	strChecked = strFlag.equals("3")?"checked":"";
	String strPrice = "";

	String strStatusFl = "";
	String strShipFl = "";
	String strGridXmlData = "";
	
	String strHeaderRule = "";
	String strLabelRule = "";
	String strTypeRule = "";
	double dbQty= 0;
	
	ArrayList alSetLoad = new ArrayList();
	HashMap hmConsignDetails = new HashMap();
	ArrayList alEmpList = new ArrayList();

	int intPriceSize = 0;

	if (hmReturn != null)
	{
		alSetLoad = (ArrayList)hmReturn.get("SETLOAD");
		strGridXmlData = GmCommonClass.parseNull((String)hmReturn.get("GRIDXMLDATA"));
		strSetId = (String)session.getAttribute("hSetId") == null?"":(String)session.getAttribute("hSetId");
	
		hmConsignDetails = (HashMap)hmReturn.get("CONDETAILS");
		strConsignId = (String)hmConsignDetails.get("CID");
		strSetName= (String)hmConsignDetails.get("SNAME");
		strAccName = (String)hmConsignDetails.get("ANAME");
		String strDistName = (String)hmConsignDetails.get("DNAME");
		strAccName = strAccName.equals("")?strDistName:strAccName;
		strUserName = (String)hmConsignDetails.get("UNAME");
		strIniDate = (String)hmConsignDetails.get("CDATE");
		strDesc = (String)hmConsignDetails.get("COMMENTS");
		strStatusFl = (String)hmConsignDetails.get("SFL");
		strShipFl = (String)hmConsignDetails.get("SHIPFL");
		strConsignType = (String)hmConsignDetails.get("CTYPE");
		strComments = (String)hmConsignDetails.get("FCOMMENTS");
		strRAId = GmCommonClass.parseNull((String)hmConsignDetails.get("RAID"));
		if(strRAId.equals("") || strRAId == null){
			strRAId = GmCommonClass.parseNull((String)hmConsignDetails.get("REFID"));
		}
		if (strhAction.equals("EditVerify"))
		{
			alEmpList = (ArrayList)hmReturn.get("EMPLIST");
		}
	}
				
	int intSize = 0;
	HashMap hcboVal = null;
	String strVerifiedBy = 	(String)session.getAttribute("strSessUserId");
	String strVerifiedByNm = 	(String)session.getAttribute("strSessShName");
	
	if (strConsignType.equals("4113"))
	{
		strHeader = strPendingVerification+" : "+ GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SHELF_TO_QUARANTINE"));
		strLabel = "Quarantine";
	}
    else if(strConsignType.equals("4112")){
		
    	strHeader =strPendingVerification+" : "+GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_IN_HOUSE_CONSIGNMENT"));
	}
    else if(strConsignType.equals("4119")){
		
    	strHeader =strPendingVerification+" :  "+ GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_IN_HOUSE_LOANER"));  
	}
    else if(strConsignType.equals("4115")){
		
		strHeader = strPendingVerification+" :  "+ GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUARANTINE_TO_SCRAP")); 
	}
	else if(strConsignType.equals("4116")){
		
		strHeader = strPendingVerification+" :  "+ GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUARANTINE_TO_SHELF")); 
	}
	else if (strConsignType.equals("4117"))
	{
		strHeader = strPendingVerification+" :  "+ GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SHELF_TO_BULK")); 
		strLabel = "Bulk";	
	}
	else if (strConsignType.equals("4118"))
	{
		strHeader = strPendingVerification+" :  "+ GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_BULK_TO_SHELF")); 
		strLabel = "Bulk";	
	}
	else if (strConsignType.equals("4114"))
	{
		strHeader = strPendingVerification+" :  "+ GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_SHELF_TO_RE_PACKAGING")); 
		strType = "50911"; //Re-package
	}
	if (strConsignType.equals("4113"))
	{
		strType = "50907";
	}else if (strConsignType.equals("4116"))
	{
		strType = "50914";
	}
	else{
		strType= "INHOUSETRANS";
	}
	if(!strRuleType.equals("")){
		strType = strRuleType;
	}
	if(!strRuleConsignType.equals("")){
		strConsignType = strRuleConsignType;
	}
	if(!strRulVerfyMsg.equals("")){
		strVerfyMsg=strRulVerfyMsg;
	}
	
// Same variable strType is used to set the TRANS_RULE AND ICE_TYPE to avoid that introduced a new variable for TRANS_RULE
	
	String strReTxn = strType;
	
	if(hmTransRules != null)
	{
	   strHeaderRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_HEADER"));
	   strLabelRule = GmCommonClass.parseNull((String)hmTransRules.get("ICE_LABEL"));
	   strTypeRule= GmCommonClass.parseNull((String)hmTransRules.get("TRANS_RLTXN"));
	   
	   if(!strHeaderRule.equals(""))
		   strHeader = strPendingVerification+" : "+strHeaderRule;
	   if(!strLabelRule.equals(""))
		   strLabel = "";
	   if(!strTypeRule.equals(""))
		   strType = strTypeRule;
	}
%>
<HTML>
<HEAD>
<TITLE>Globus Medical:InHouse Item Consignment </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strOperationsJsPath%>/GmInhouseItemConsignVerify.js"></script>
<script>
var objGridData = '<%=strGridXmlData%>';
var vStatus = '<%=strStatusFl%>';
var vIncludeRuleEng = '<%=strIncludeRuleEng.toUpperCase()%>';
function fnVRule()
{
	document.frmVendor.hAction.value ="Load";
	document.frmVendor.action ="<%=strServletPath%>/GmCommonCancelServlet";
	document.frmVendor.submit();
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad();">
<FORM name="frmVendor" method="POST" action="<%=strServletPath%>/GmReprocessMaterialServlet">
<input type="hidden" name="hAction" value="Verify">
<input type="hidden" name="hConsignId" value="<%=strConsignId%>">
<input type="hidden" name="hConsignType" value="<%=strConsignType%>">
<input type="hidden" name="RE_FORWARD" value="gmReprocessMaterialServlet">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" value="<%=strReTxn%>">
<input type="hidden" name="hTxnId" value="<%=strConsignId%>">
<input type="hidden" name="hTxnName" value="<%=strConsignId%>">
<input type="hidden" name="hCancelType" value="VINTX">
<input type="hidden" name="hOpt" value="<%=strOpt%>">
<input type="hidden" name="hpnumLcnStr" value="">
<input type="hidden" name="TXN_TYPE_ID" value="<%=strTransTypeId%>">

	<table border="0" width="600" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader"><%=strHeader%></td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="598" height="100" valign="top">
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
					<tr class="shade">
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr> 
								 
								<td class="RightText" HEIGHT="20" align="right">&nbsp;<%--<%=strLabel%> --%><B><fmtInHouseItemConsignVerify:message key="LBL_TRANSACTION_ID"/>:</B></td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr >
									<td width="0%">&nbsp;<%=strConsignId%></td>								
									<td class="RightText" align="right" width="0%">&nbsp;<b><fmtInHouseItemConsignVerify:message key="LBL_REFERENCE_ID"/>:</b></td>
									<td width= "55%">&nbsp;<%=strRAId%></td>
								</tr>
							</table>
						</td>					
					</tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtInHouseItemConsignVerify:message key="LBL_ACCOUNT_NAME"/>:</b></td>
						<td class="RightText">&nbsp;<%=strAccName%></td>
					</tr>
					<tr class="shade">
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtInHouseItemConsignVerify:message key="LBL_INITIATED_BY"/>:</b></td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="0%">&nbsp;<%=strUserName%></td>								
									<td class="RightText" align="right" width="0%">&nbsp;<b><fmtInHouseItemConsignVerify:message key="LBL_DATE_INITIATED"/>:</b></td>
									<td width= "55%">&nbsp;<%=strIniDate%></td>
								</tr>
							</table>
						</td>						
					</tr>
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="30"><b><fmtInHouseItemConsignVerify:message key="LBL_INITIAL_COMMENTS"/>:</b></td>
						<td class="RightText" valign="top" >&nbsp;<%=strDesc%></td>
					</tr>
					<tr class="shade">
						<td class="RightText" HEIGHT="20" align="right">&nbsp;<b><fmtInHouseItemConsignVerify:message key="LBL_VERIFIED_BY"/>:</b></td>
<%
		if (strhAction.equals("EditVerify"))
		{
%>						
						<td class="RightText">&nbsp;<select name="Cbo_VerEmp" class="RightText" tabindex="2" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"><option value="0" ><fmtInHouseItemConsignVerify:message key="OPT_CHOOSE_ONE"/>
<%
			  		intSize = alEmpList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alEmpList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strVerifiedBy.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>
<%
			  		}
%>
						</td>
<%
		}else{
%>
						<td class="RightText">&nbsp;<%=strVerifiedByNm%></td>
<%
		}
%>						
					</tr>					
					<tr>
						<td class="RightText" valign="top" align="right" HEIGHT="60"><B><fmtInHouseItemConsignVerify:message key="LBL_FINAL_COMMENTS"/>:</B></td>
<%
		if (!strhAction.equals("ViewInHouse"))
		{
%>							
						<td>&nbsp;<textarea name="Txt_Comments" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1 rows=5 cols=40 value=""><%=strComments%></textarea></td>
<%
		}else{
%>				
						<td class="RightText">&nbsp;<%=strComments%></td>		
<%
	}
%>
					</tr>
<%					if (strConsignType.equals("4116")||strConsignType.equals("4114") || strConsignType.equals("4118") )
					{
%>
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" class="DtTable700" >
								<tr> 
						        	<td colspan="4">
						          		<div id="dataGridDiv" style="height : 300px;"></div>
									</td>		
								</tr>								
							</table>
						</td>
					</tr>			
<%	
					}else
					{
%>						
					<tr>
						<td colspan="2">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" class="DtTable700" id="myTable">
								<tr><td colspan="4" height="1" ></td></tr>
								<tr class="ShadeRightTableCaption">
									<td height="25"><fmtInHouseItemConsignVerify:message key="LBL_PART_NUMBER"/></td>
									<td width="300"><fmtInHouseItemConsignVerify:message key="LBL_DESCRIPTION"/></td>
									<td width="50"><fmtInHouseItemConsignVerify:message key="LBL_QTY"/></td>
									<td width="100"><fmtInHouseItemConsignVerify:message key="LBL_CONTROL_NUMBER"/></td>
								</tr>
<%
						intSize = alSetLoad.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
							HashMap hmTempLoop = new HashMap();

							String strNextPartNum = "";
							int intCount = 0;
							String strColor = "";
							String strLine = "";
							String strPartNumHidden = "";
							
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alSetLoad.get(i);
								if (i<intSize-1)
								{
									hmTempLoop = (HashMap)alSetLoad.get(i+1);
									strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PNUM"));
								}
								strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
								strPartNumHidden = strPartNum;
								strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));
								strQty = GmCommonClass.parseNull((String)hmLoop.get("QTY"));
								strPrice = GmCommonClass.parseNull((String)hmLoop.get("PRICE"));
								strTemp = strPartNum;

								if (strPartNum.equals(strNextPartNum))
								{
									intCount++;
									strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
								}

								else
								{
									strLine = "<TR><TD colspan=4 height=1 class=Line></TD></TR>";
									if (intCount > 0)
									{
										strPartNum = "";
										strPartDesc = "";
										strQty = "";
										//strColor = "bgcolor=white";
										strLine = "";
									}
									else
									{
										//strColor = "";
									}
									intCount = 0;
								}

								if (intCount > 1)
								{
									strPartNum = "";
									strPartDesc = "";
									strQty = "";
									//strColor = "bgcolor=white";
									strLine = "";
								}
							
								strShade = (i%2 != 0)?"class=Shade":""; //For alternate Shading of rows
								strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
								strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
								out.print(strLine);
								dbQty = Double.parseDouble(strItemQty);
								
%>
								<tr >
									<td class="RightText" height="20">&nbsp;<%=strPartNum%></td>
									<td class="RightText"><%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<td class="RightText">&nbsp;<%=dbQty%></td>
									<td class="RightText">&nbsp;<%=strControl%></td>
								</tr>
<%
							}//End of FOR Loop
						}else {
%>
						<tr><td colspan="4" height="50" align="center" class="RightTextRed"><BR><fmtInHouseItemConsignVerify:message key="LBL_NO_PARTS_HAVE_BEEN_TAKEN_FOR_REPACKAGING"/></td></tr>
<%
						}		
%>
							</table>
						</td>
					</tr>
<%					} %>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" ></td></tr>
<%
	//if(strConsignType.equals("4114")||strConsignType.equals("4113")||strConsignType.equals("4116"))
//{
%>
		<tr>
			<td colspan="3">
			<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<jsp:include page="/common/GmRuleDisplayInclude.jsp"/>
			</td>
		</tr>		
<%//} %>
<%
				if (!strStatusFl.equals("4"))
				{
					if(strConsignType.equals("4113"))
					{						
%>
					<tr>
						<td colspan="3" id="log"> 
							<jsp:include page="/common/GmIncludeLog.jsp" >
								<jsp:param name="LogType" value="" />
								<jsp:param name="LogMode" value="Edit" />
							</jsp:include>
						</td>
					</tr>
					<%} 
					
					
					%>
					<tr>
						<td colspan="2" height="30" align="center">
							<fmtInHouseItemConsignVerify:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" gmClass="button" onClick="fnSubmit();" tabindex="13" buttonType="Save" />&nbsp;&nbsp;
							<fmtInHouseItemConsignVerify:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" gmClass="button" onClick="fnVRule();" tabindex="14" buttonType="Save" />
						</td>
					</tr>
<%
				}else{
%>
<%-- 
					<tr>
						<td colspan="2" height="30" align="center" class="RightTextRed"><BR><%=strVerfyMsg%>
						<BR><BR>
						</td>
					</tr>
--%>
<%
					}
%>		
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
