
<%@page import="com.globus.common.beans.GmCommonClass"%>
<%
/**********************************************************************************
 * File		 		: GmPartNumInvReport.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java"%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="com.globus.common.beans.GmCommonControls"%>
<%@ page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page
	import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.text.*,java.lang.String,java.util.StringTokenizer"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<!-- operations\GmPartNumInvReport.jsp -->
<%@ taglib prefix="fmtPartNumInvReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtPartNumInvReport:setLocale value="<%=strLocale%>"/>
<fmtPartNumInvReport:setBundle basename="properties.labels.operations.GmPartNumInvReport"/>


<%
String strOperationsJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session); 

	String strWikiTitle = GmCommonClass.getWikiTitle("PART_NUMBER_INVENTORY_REPORT");
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strTitle = GmCommonClass.parseNull((String)request.getAttribute("TITLE"));
	DecimalFormat doublef = new DecimalFormat("#.##");

	HashMap hmLoop = new HashMap();
	HashMap hcboVal = new HashMap();

	String strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
	String strPartNum = (String)request.getAttribute("hPartNum")==null?"":(String)request.getAttribute("hPartNum");
	String strSearch = (String)request.getAttribute("hSearch")==null?"LIKEPRE":(String)request.getAttribute("hSearch");
	String strSubComponentFlag = GmCommonClass.parseNull((String)request.getAttribute("hSubComponentFlag")) == "Y" ? "checked" : "" ;
	String strEnableObsolete = GmCommonClass.parseNull((String)request.getAttribute("hEnableObsolete")) == "Y" ? "checked" : "" ;
	String strPageType = (String)request.getAttribute("hPageType")==null?"":(String)request.getAttribute("hPageType");
	String strSkipLoanerSection = GmCommonClass.parseNull(GmCommonClass.getString("LOAD_LOANER_INV_SECTION"));
	String strProjName = (String)request.getAttribute("hProjNm")==null?"":(String)request.getAttribute("hProjNm");
    String strSetId = (String)request.getAttribute("hSetId")==null?"":(String)request.getAttribute("hSetId");
	String strSetName = (String)request.getAttribute("hSetName")==null?"":(String)request.getAttribute("hSetName");
	String strPartDesc = "";
	String strQtyInStock = "";
	String strQtyInShelf = "";
	String strQtyInShelfAlloc = "";
	String strShipAlloc= "";
	String strQtyRMAlloc = "";
	String strQtyMFGTBR = "";
	String strQtyMFGRelease = "";
	String strQtyDHRWIP = "";
	String strQtyDHR_PEN_QC_INS_ = "";
	String strQtyDHR_PEN_QC_REL = "";
	String strQtyDHR_QC_APP = "";
	String strQtyBUILTSET = "";	
	  
	String strQtyInBulk = "";
	String strQtyInWIPSets = "";
	String strQtyInBulkAlloc = "";
	
	String strQtyInQuarantine = "";
	String strQtyInQuarantineAlloc = "";
	
	String strQtyInReturnsHold = "";
	String strQtyInReturnsHoldAlloc = "";
	String strRepackAvail ="";
	String strRepackAlloc ="";
	String strQtyInPO = "";
	String strQtyInWIP = "";
	String strQtyInFA = "";
	String strQtyInFAWIP = "";
	String strShelfInAlloc = "";
	String strRawMatQty = "";
	String strTBE = "";
	String strDHRAlloc="";
	
	String strIhAVailQty = "";
	String strIhAllocQty = "";
	String strIhSetQty = "";
	String strIhItemQty = "";
	
	String strQtyInRW = "";
	String strQtyInRWAlloc = "";	
	
	String strLoanQty = "";
	
	String strInTransAvailQty = "";
	String strInTransAllocQty = "";
	
	String strShade = "";
	String strCodeID = "";
	String strSelected = "";
	String strIncludeColID = "";
	
	String strShowFG = "";
	String strShowRM = "";
	String strShowBulk = "";
	String strShowBS = "";
	String strShowPN = "";
	String strShowQN = "";
	String strShowRtn = "";
	String strShowPO = "";
	String strShowMFG = "";
	String strShowDHR = "";
	String strShowShip = "";
	String strShowIH = "";
	String strShowLN = "";
	String strShowRW = "";
	String strShowInTransit ="";
	
	int intLoop = 0;

	ArrayList alProject = new ArrayList();
	alProject = GmCommonClass.parseNullArrayList((ArrayList)session.getAttribute("PROJLIST"));
	ArrayList alInventory = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("INVENTORY"));	
	String strIncludedInv = GmCommonClass.parseNull((String)request.getAttribute("INCLUDEINV"));
	
	String strExcludedInv = GmCommonClass.parseNull((String)request.getAttribute("EXCLUDEINV"));
	
	ArrayList alSets = new ArrayList();
	ArrayList alReport = new ArrayList();
	HashMap hmInventory = new HashMap();
	HashMap hmDefaultCols = new HashMap();
	HashMap hmAllInvCols = new HashMap();
	hmInventory.put("ID","CODEID");
	hmInventory.put("PID","ID");
	hmInventory.put("NM","CODENM");	
	if (hmReturn != null) 
	{
		alReport = (ArrayList)hmReturn.get("REPORT");
		
	}
	int intProjLength = alProject.size();
	int intSetLength = alSets.size();
	if (alReport != null)
	{
		intLoop = alReport.size();
	}	
	String strApplDateFmt = strGCompDateFmt;
	GmCalenderOperations gmCal = new GmCalenderOperations();
	gmCal.setTimeZone(strGCompTimeZone);
    String strDate = "";
	strDate = gmCal.getCurrentDate(strApplDateFmt+" K:m:s a");
	String strUserName = (String)session.getAttribute("strSessShName");
	String strAccessId = (String)session.getAttribute("strSessAccLvl") ==null?"":(String)session.getAttribute("strSessAccLvl");
	String strDeptId = 	(String)session.getAttribute("strSessDeptId") == null?"":(String)session.getAttribute("strSessDeptId");
	int intAccId = Integer.parseInt(strAccessId);
	boolean viewAllFl = false;
	if (strDeptId.equals("Z") || (strDeptId.equals("O") && intAccId > 3))
	{
		viewAllFl = true;
	}	
    StringTokenizer strIncludeInvs = new StringTokenizer(strIncludedInv,",");
	
	while(strIncludeInvs.hasMoreTokens())
	{
		strIncludeColID = strIncludeInvs.nextToken();
		if(strIncludeColID.equals("60001"))
		{
			strShowFG = "Y";	
		}
		if(strIncludeColID.equals("60002"))
		{
			strShowRM = "Y";	
		}
		if(strIncludeColID.equals("60003"))
		{
			strShowBulk = "Y";	
		}
		if(strIncludeColID.equals("60004"))
		{
			strShowBS = "Y";
		}
		if(strIncludeColID.equals("60005"))
		{
			strShowPN = "Y";	
		}
		if(strIncludeColID.equals("60006"))
		{
			strShowQN = "Y";
		}
		if(strIncludeColID.equals("60007"))
		{
			strShowRtn = "Y";	
		}
		if(strIncludeColID.equals("60008"))
		{
			strShowPO = "Y";
		}
		if(strIncludeColID.equals("60009"))
		{
			strShowMFG = "Y";
		}
		if(strIncludeColID.equals("60010"))
		{
			strShowDHR = "Y";
		}
		if(strIncludeColID.equals("60011"))
		{
			strShowShip = "Y";
		}
		if(strIncludeColID.equals("60012"))
		{
			strShowIH = "Y";
		}
		if(strIncludeColID.equals("60013"))
		{
			strShowLN = "Y";
		}
		if(strIncludeColID.equals("60014"))
		{
			strShowRW = "Y";
		}
		if(strIncludeColID.equals("60015"))
		{
			strShowInTransit = "Y";
		}
	}
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Inventory Report</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript"
	src="<%=strOperationsJsPath%>/GmPartNumInvReport.js"></script>

<script>

function fnEnter()
{
		if (event.keyCode == 13)
		{ 
			fnGo();
		}
}


var prevtr = 0;
var arrayLength = '<%=alInventory.size()%>';
var excludedInvs = '<%=strExcludedInv%>';
function fnViewLoaners(id,strOpt){
	id = id.substring(0,12);
	windowOpener("/GmInvListReportServlet?hAction=Go&hOpt=" + strOpt + "&optVal=MainInv&Cbo_ProjId=0&Cbo_Search=0&hPartNum="+encodeURIComponent(id),"LoanInv","resizable=yes,scrollbars=yes,top=250,left=300,width=965,height=480");
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();" onkeyup="javascript:fnEnter();">
	<FORM name="frmAccount" method="POST" action="<%=strServletPath%>/GmPartInvReportServlet">
		<input type="hidden" name="hId" value="<%=strProjId%>"> 
		<input type="hidden" name="hAction" value=""> 
		<input type="hidden" name="hOpt" value=""> 
		<input type="hidden" name="hPageType" value="<%=strPageType%>"> 
		<input type="hidden" name="hSearch" value="<%=strSearch%>"> 
		<input type="hidden" name="hViewAll" value="<%=viewAllFl%>"> 
		<input type="hidden" name="hIncludedColumns" value="<%=strIncludedInv%>"> 
		<input type="hidden" name="hExcludedColumns" value="<%=strExcludedInv%>">
 	
		<table border="0" width="1200" cellspacing="0" cellpadding="0">
			<tr>
				<td rowspan="20" width="1" class="Line"></td>
				<td colspan="2" height="1" bgcolor="#666666"></td>
				<td rowspan="20" width="1" class="Line"></td>
			</tr>
			<tr>
				<td width="300" height="25" class="RightDashBoardHeader"><%=strTitle%></td>
				<td align="right" class=RightDashBoardHeader><fmtPartNumInvReport:message key="IMG_HELP" var = "varHelp"/><img id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="2"></td>
			</tr>
			<tr height="25">
				<td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtPartNumInvReport:message key="LBL_PART_NUM"/>:</td>
				<td>&nbsp;<input type="text" size="57" value="<%=strPartNum%>"
					name="hPartNum" class="InputArea"
					style="text-transform: uppercase;"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');" tabindex="1"> &nbsp;
					<select name="Cbo_Search" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="2">
						<option value="0"><fmtPartNumInvReport:message key="LBL_CHOOSE"/>
						<option value="LIT"><fmtPartNumInvReport:message key="LBL_LITREAL"/>
						<option value="LIKEPRE"><fmtPartNumInvReport:message key="LBL_PREFIX"/>
						<option value="LIKESUF"><fmtPartNumInvReport:message key="LBL_SUFFIX"/>
				</select> &nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="2"></td>
			</tr>
			<tr class="shade" >
			    <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtPartNumInvReport:message key="LBL_SET_ID"/>:</td>
				<td>&nbsp;<input type="text" size="57" value="<%=strSetId%>" name="hSetId" class="InputArea" style="text-transform: uppercase;"
					onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="3">
                    &nbsp;<fmtPartNumInvReport:message key="LBL_SET_NAME"/>:
					&nbsp;<label><%=strSetName%></label>
                </td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="2"></td>
			</tr>
			<tr height="25">
				<td class="RightTableCaption" align="right" >&nbsp;<fmtPartNumInvReport:message key="LBL_PRO_LIST"/>:</td>
				<td>&nbsp;
				<table> <tr><td>
				<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
							<jsp:param name="CONTROL_NAME" value="Cbo_ProjId" />
							<jsp:param name="METHOD_LOAD" value="loadProjectNameList&searchType=LikeSearch&searchBy=ProjectId" />
							<jsp:param name="WIDTH" value="350" />
							<jsp:param name="CSS_CLASS" value="search" />
							<jsp:param name="TAB_INDEX" value="4"/>
							<jsp:param name="CONTROL_NM_VALUE" value="<%=strProjName %>" /> 
							<jsp:param name="CONTROL_ID_VALUE" value="<%=strProjId %>" />
							<jsp:param name="SHOW_DATA" value="100" />
							<jsp:param name="AUTO_RELOAD" value="" />					
							</jsp:include>
				</td><td>
			&nbsp; <fmtPartNumInvReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="${varLoad}" style="height:22;width:5em;"
						name="Btn_Go" gmClass="button" onClick="javascript:fnGo()"
						tabindex="5" buttonType="Load" /> </td><%
			if (intLoop > 0)
			{
%> 						<td>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<fmtPartNumInvReport:message key="BTN_PRINT_VER" var="varPrintVersion"/><gmjsp:button style="height:22;width:9em;" value="${varPrintVersion}"
						gmClass="button" onClick="javascript:fnPrintDownVer('Print');"
						buttonType="Load" /> &nbsp;&nbsp;
						</td><td>
						<fmtPartNumInvReport:message key="BTN_EXCL_DOWN" var="varExclDown"/><gmjsp:button
						value="${varExclDown}" gmClass="button"
						style="height:22;width:10em;"
						onClick="javascript:fnPrintDownVer('Excel');" buttonType="Load" /> </td>
					<%
			}
%>				</tr>
				 <tr>
					<td height="13" ></td>
				</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td class="LLine" height="1" colspan="2"></td>
			</tr>
			<tr>
				<td align="right" class="RightTableCaption"><a
					href="javascript:fnShowMoreFilters();"><fmtPartNumInvReport:message key="LBL_MORE_FILTERS"/></a> >></td>
				<td align="left">
					<table border="0" id="InvFilter" style="display: none">
						<tr>
							<td><input type="checkbox" name="subComponentFl"
								<%=strSubComponentFlag%>> <fmtPartNumInvReport:message key="LBL_SHOW_SUB"/> &nbsp;<input
								type="checkbox" name="enableObsolete" <%=strEnableObsolete%>>
								<fmtPartNumInvReport:message key="LBL_SHOW_OBSOL"/></td>
						</tr>
						<tr>
							<td><input type="checkbox" name="selectAllInventories"
								onclick="fnSelectAllInventories(this,'Chk_GrpInventory','toggle');">&nbsp;<B><fmtPartNumInvReport:message key="LBL_SELECT_ALL"/> </B></td>
						</tr>
						<tr>
							<td align="left"><%=GmCommonControls.getChkBoxGroup("fnSelectInv()",alInventory,"Inventory",hmInventory,"")%></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
			</tr>
			<tr>
				<td class="Line" colspan="2"></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><fmtPartNumInvReport:message key="LBL_REPORT"/> <%=strDate%> <fmtPartNumInvReport:message key="LBL_BY"/>:<%=strUserName%><BR>
					<div style="overflow: auto; height: 400px;">
						<table border="0" width="100%" cellspacing="0" cellpadding="0"
							id='myHighlightTable'>
							<thead>
								<tr bgcolor="#eeeeee" class="RightTableCaption"
									style="position: relative; top: expression(this.offsetParent.scrollTop);">
									<th HEIGHT="24" align="center" width="120"><fmtPartNumInvReport:message key="LBL_PART_NUM"/></th>
									<th width="340" align="left">&nbsp;<fmtPartNumInvReport:message key="LBL_DESC"/></th>
									<% if(!strShowFG.equals("")){%>
									<th colspan="2" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_FG_INV"/><BR>
									</th>
									<%} %>
									<% if(!strShowRW.equals("")){%>
									<th colspan="2" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_RW_INV"/><BR>
									</th>
									<%} %>
									<% if(!strShowRM.equals("")){%>
									<th colspan="2" align="center" class="ShadeDarkGrayTD"><fmtPartNumInvReport:message key="LBL_RM_INV"/><BR>
									</th>
									<%} %>
									<% if(!strShowBulk.equals("")){%>
									<th colspan="4" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_BULK_LOOSE"/><BR>
									</th>
									<%} %>
									<% if(!strShowBS.equals("")){%>
									<th align="center" width="60" class="ShadeDarkBrownTD">&nbsp;<fmtPartNumInvReport:message key="LBL_BUILT_SET"/>&nbsp;<br>&nbsp;
									</th>
									<%} %>
									<% if(!strShowPN.equals("")){%>
									<th width="100" colspan="2" align="center"
										class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_RE_PACK"/></th>
									<%} %>
									<% if(!strShowQN.equals("")){%>
									<th align="center" width="100" colspan="2"
										class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_QUA_INV"/></th>
									<%} %>
									<% if(!strShowRtn.equals("")){%>
									<th width="100" colspan="2" align="center"
										class="ShadeDarkYellowTD"><fmtPartNumInvReport:message key="LBL_RET_HOLD"/><BR>
									</th>
									<%} %>
									<% if(!strShowPO.equals("")){%>
									<th width="60" align="center" class="ShadeDarkBrownTD"><fmtPartNumInvReport:message key="LBL_QTY_IN"/><br>
									</th>
									<%} %>
									<% if(!strShowMFG.equals("")){%>
									<th colspan="2" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_MFG"/></th>
									<%} %>
									<% if(!strShowDHR.equals("")){%>
									<th colspan="5" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_DHR"/></th>
									<%} %>
									<% if(!strShowShip.equals("")){%>
									<th colspan="2" align="center" class="ShadeDarkBrownTD"><fmtPartNumInvReport:message key="LBL_SHIP_HOLD"/></th>
									<%} %>
									<% if(!strShowIH.equals("")){%>
									<th colspan="4" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_IH_INV"/><BR>
									</th>
									<%} %>
									<% if(!strShowLN.equals("")){%>
									<th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_LOANER_INV"/><br>
									</th>
									<%} %>
									<% if(!strShowInTransit.equals("")){%>
									<th width="100" colspan="2" align="center" class="ShadeDarkYellowTD"><fmtPartNumInvReport:message key="LBL_INTRANSIT_INV"/><br>
									</th>
									<%} %>
									

								</tr>
								<tr style="position: relative;">
									<th colspan="2" height="1">
									<th colspan="32" class="Line" height="1"></th>
								</tr>
								<tr bgcolor="#eeeeee" class="RightTableCaption"
									style="position: relative; top: expression(this.offsetParent.scrollTop);">
									<th colspan="2">&nbsp;</th>
									<% if(!strShowFG.equals("")){%>
									<th width="50" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_AVAIL"/></th>
									<th width="50" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<%}%>
									<% if(!strShowRW.equals("")){%>
									<th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_AVAIL"/></th>
									<th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<%} %>
									<% if(!strShowRM.equals("")){%>
									<th width="50" align="center" class="ShadeDarkGrayTD"><fmtPartNumInvReport:message key="LBL_AVAIL"/></th>
									<th width="50" align="center" class="ShadeDarkGrayTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<%} %>
									<% if(!strShowBulk.equals("")){%>
									<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_WIP"/>
										Sets</th>
									<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_TBE"/>
										Sets</th>
									<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_LOOSE"/></th>
									<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<%} %>
									<% if(!strShowBS.equals("")){%>
									<th class="ShadeDarkBrownTD">&nbsp;</th>
									<%} %>
									<% if(!strShowPN.equals("")){%>
									<th width="50" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_AVAIL"/></th>
									<th width="50" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<%} %>
									<% if(!strShowQN.equals("")){%>
									<th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_AVAIL"/></th>
									<th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<%} %>
									<% if(!strShowRtn.equals("")){%>
									<th width="50" align="center" class="ShadeDarkYellowTD"><fmtPartNumInvReport:message key="LBL_AVAIL"/></th>
									<th width="50" align="center" class="ShadeDarkYellowTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<%} %>
									<% if(!strShowPO.equals("")){%>
									<th class="ShadeDarkBrownTD">&nbsp;</th>
									<%} %>
									<% if(!strShowMFG.equals("")){%>
									<th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_TBR"/></th>
									<th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_RELEASED"/></th>
									<%} %>
									<% if(!strShowDHR.equals("")){%>
									<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_WIP"/></th>
									<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_REC"/></th>
									<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_QC_INS"/>
										</th>
									<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_QC_REL"/><br>
									</th>
									<th width="50" align="center" class="ShadeDarkBlueTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<%}%>
									<% if(!strShowShip.equals("")){%>
									<th colspan="2" width="50" align="center"
										class="ShadeDarkBrownTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<%} %>
									<% if(!strShowIH.equals("")){%>
									<th width="50" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_AVAIL"/></th>
									<th width="50" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<th width="50" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_SETS"/></th>
									<th width="50" align="center" class="ShadeDarkGreenTD"><fmtPartNumInvReport:message key="LBL_ITEMS"/></th>
									<%} %>
									<% if(!strShowLN.equals("")){%>
									<th width="50" align="center" class="ShadeDarkOrangeTD"><fmtPartNumInvReport:message key="LBL_AVAIL"/></th>
									<%} %>
									<% if(!strShowInTransit.equals("")){%>
									<th width="50" align="center" class="ShadeDarkYellowTD"><fmtPartNumInvReport:message key="LBL_AVAIL"/></th>
									<th width="50" align="center" class="ShadeDarkYellowTD"><fmtPartNumInvReport:message key="LBL_ALLOC"/></th>
									<%} %>

								</tr>
								<tr
									style="position: relative; top: expression(this.offsetParent.scrollTop);">
									<td colspan="34" class="Line" height="1"></td>
								</tr>
							</thead>
							<tbody>
								<%
			if (intLoop > 0)
			{
				int intStock = 0;
				int intBulk = 0;
				int intReturns = 0;
				int intPO = 0;
				int intDHR = 0;
				
				for (int i = 0;i < intLoop ;i++ )	
				{
					hmLoop = (HashMap)alReport.get(i);
					strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PNUM"));
					strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PDESC"));

					strQtyInShelf = GmCommonClass.parseZero((String)hmLoop.get("INSHELF"));
					strQtyInShelfAlloc = GmCommonClass.parseZero((String)hmLoop.get("SHELFALLOC"));					
					
					strQtyInRW = GmCommonClass.parseZero((String)hmLoop.get("INRW"));
					strQtyInRWAlloc = GmCommonClass.parseZero((String)hmLoop.get("RWALLOC"));
					strQtyInRWAlloc = strQtyInRWAlloc.equals("0")?strQtyInRWAlloc:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','RWALLOC')\";>"+strQtyInRWAlloc+"</a>";
									
					strQtyRMAlloc = GmCommonClass.parseZero((String)hmLoop.get("RM_ALLOC_QTY"));					
					strQtyRMAlloc = strQtyRMAlloc.equals("0")?strQtyRMAlloc:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','RMALLOC')\";>"+strQtyRMAlloc+"</a>";
					
					
					strQtyInShelfAlloc = strQtyInShelfAlloc.equals("0")?strQtyInShelfAlloc:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','SALLOC')\";>"+strQtyInShelfAlloc+"</a>";
					strShelfInAlloc = GmCommonClass.parseZero((String)hmLoop.get("INV_ALLOC_TO_PACK"));
					strShelfInAlloc = strShelfInAlloc.equals("0")?strShelfInAlloc:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','PALLOC')\";>"+strShelfInAlloc+"</a>";
					
					strRawMatQty = GmCommonClass.parseZero((String)hmLoop.get("RMQTY"));
					double dRawMatQty = Double.parseDouble(strRawMatQty);
					strRawMatQty = doublef.format(dRawMatQty);
					
					strQtyInQuarantine = GmCommonClass.parseZero((String)hmLoop.get("INQUARAN"));
					strQtyInQuarantineAlloc = GmCommonClass.parseZero((String)hmLoop.get("QUARALLOC"));
					strQtyInQuarantineAlloc = strQtyInQuarantineAlloc.equals("0")?strQtyInQuarantineAlloc:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','QALLOC')\";>"+strQtyInQuarantineAlloc+"</a>";
					
					strRepackAvail = GmCommonClass.parseZero((String)hmLoop.get("REPACKAVAIL"));
					strRepackAlloc = GmCommonClass.parseZero((String)hmLoop.get("REPACKALLOC"));
					strRepackAlloc = strRepackAlloc.equals("0")?strRepackAlloc:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','REPACKALLOC')\";>"+strRepackAlloc+"</a>";
					
					strQtyInBulk = GmCommonClass.parseZero((String)hmLoop.get("INBULK"));
					strQtyInWIPSets = GmCommonClass.parseZero((String)hmLoop.get("IN_WIP_SET"));
					strQtyInWIPSets = strQtyInWIPSets.equals("0")?strQtyInWIPSets:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','WIPSET')\";>"+strQtyInWIPSets+"</a>";
					strTBE = GmCommonClass.parseZero((String)hmLoop.get("C205_TBE_ALLOC"));
					strTBE = strTBE.equals("0")?strTBE:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','TBESET')\";>"+strTBE+"</a>";
					strQtyInBulkAlloc = GmCommonClass.parseZero((String)hmLoop.get("BULKALLOC"));
					strQtyInBulkAlloc = strQtyInBulkAlloc.equals("0")?strQtyInBulkAlloc:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','BALLOC')\";>"+strQtyInBulkAlloc+"</a>";
					
					strQtyInReturnsHold = GmCommonClass.parseZero((String)hmLoop.get("RETURNAVAIL"));
					strQtyInReturnsHoldAlloc = GmCommonClass.parseZero((String)hmLoop.get("RETURNALLOC"));
					strQtyInReturnsHoldAlloc = strQtyInReturnsHoldAlloc.equals("0")?strQtyInReturnsHoldAlloc:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','RHOLD')\";>"+strQtyInReturnsHoldAlloc+"</a>";
					
					strQtyMFGTBR = GmCommonClass.parseZero((String)hmLoop.get("MFG_TBR"));	
					strQtyMFGRelease = GmCommonClass.parseZero((String)hmLoop.get("MFG_RELEASE"));
					double dQtyMFGTBR = Double.parseDouble(strQtyMFGTBR);
					double dQtyMFGRelease = Double.parseDouble(strQtyMFGRelease);
					strQtyMFGTBR = doublef.format(dQtyMFGTBR);
					strQtyMFGRelease = doublef.format(dQtyMFGRelease);
					strQtyMFGTBR = strQtyMFGTBR.equals("0")?strQtyMFGTBR:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','MFGTBR')\";>"+strQtyMFGTBR+"</a>";
					strQtyMFGRelease = strQtyMFGRelease.equals("0")?strQtyMFGRelease:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','MFGREL')\";>"+strQtyMFGRelease+"</a>";
					
					strQtyBUILTSET = GmCommonClass.parseZero((String)hmLoop.get("BUILD_SET_QTY"));
					strQtyBUILTSET = strQtyBUILTSET.equals("0")?strQtyBUILTSET:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','BUILTSET')\";>"+strQtyBUILTSET+"</a>";
					 
					strQtyInPO = GmCommonClass.parseZero((String)hmLoop.get("POPEND"));
					strQtyInWIP = GmCommonClass.parseZero((String)hmLoop.get("POWIP"));
					strQtyInPO = strQtyInPO.equals("0")?strQtyInPO:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','PO')\";>"+strQtyInPO+"</a>";
					
					strQtyDHRWIP = GmCommonClass.parseZero((String)hmLoop.get("DHR_WIP"));	
					strQtyDHR_PEN_QC_INS_ = GmCommonClass.parseZero((String)hmLoop.get("DHR_PEN_QC_INS"));	
					strQtyDHR_PEN_QC_REL = GmCommonClass.parseZero((String)hmLoop.get("DHR_PEN_QC_REL"));	
					strQtyDHR_QC_APP = GmCommonClass.parseZero((String)hmLoop.get("DHR_QC_APPR"));	
					strDHRAlloc = GmCommonClass.parseZero((String)hmLoop.get("DHRALLOC"));
					strQtyDHRWIP = strQtyDHRWIP.equals("0")?strQtyDHRWIP:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','DHRWIP')\";>"+strQtyDHRWIP+"</a>";
					strQtyDHR_PEN_QC_INS_ = strQtyDHR_PEN_QC_INS_.equals("0")?strQtyDHR_PEN_QC_INS_:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','DHRREC')\";>"+strQtyDHR_PEN_QC_INS_+"</a>";
					strQtyDHR_PEN_QC_REL = strQtyDHR_PEN_QC_REL.equals("0")?strQtyDHR_PEN_QC_REL:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','DHRQCINS')\";>"+strQtyDHR_PEN_QC_REL+"</a>";
					strQtyDHR_QC_APP = strQtyDHR_QC_APP.equals("0")?strQtyDHR_QC_APP:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','DHRQCR')\";>"+strQtyDHR_QC_APP+"</a>";
					strDHRAlloc=strDHRAlloc.equals("0")?strDHRAlloc:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','DHRALLOC')\";>"+strDHRAlloc+"</a>";
					strShipAlloc = GmCommonClass.parseZero((String)hmLoop.get("SHIPALLOC"));
					strShipAlloc=strShipAlloc.equals("0")?strShipAlloc:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','SHIPALLOC')\";>"+strShipAlloc+"</a>";
					
					strIhAVailQty = GmCommonClass.parseZero((String)hmLoop.get("IH_AVAIL_QTY"));
					strIhAllocQty = GmCommonClass.parseZero((String)hmLoop.get("IH_ALLOC_QTY"));
					strIhAllocQty = strIhAllocQty.equals("0")?strIhAllocQty:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','IHALLOC')\";>"+strIhAllocQty+"</a>";
					strIhSetQty = GmCommonClass.parseZero((String)hmLoop.get("IH_SET_QTY"));
					strIhSetQty = strIhSetQty.equals("0")?strIhSetQty:"<a class=RightText href=\"javascript:fnViewLoaners('"+strPartNum+"','InHousePart')\";>"+strIhSetQty+"</a>";
					strIhItemQty = GmCommonClass.parseZero((String)hmLoop.get("IH_ITEM_QTY"));
					strIhItemQty = strIhItemQty.equals("0")?strIhItemQty:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','IHITEM')\";>"+strIhItemQty+"</a>";

					strLoanQty = GmCommonClass.parseZero((String)hmLoop.get("LOANQTY"));
					strLoanQty=strLoanQty.equals("0")?strLoanQty:"<a class=RightText href=\"javascript:fnViewLoaners('"+strPartNum+"','LoanerPart')\";>"+strLoanQty+"</a>";
					
					strInTransAvailQty = GmCommonClass.parseZero((String)hmLoop.get("INTRANS_AVAIL_QTY"));
					strInTransAllocQty = GmCommonClass.parseZero((String)hmLoop.get("INTRANS_ALLOC_QTY"));
					strInTransAllocQty = strInTransAllocQty.equals("0")?strInTransAllocQty:"<a class=RightText href=\"javascript:fnViewDetails('"+strPartNum+"','INTRANSALLOC')\";>"+strInTransAllocQty+"</a>";

%>
								<tr id="tr<%=i%>" onmouseover=fnstripedbyid('myHighlightTable','lightBrown');  >
									<td height="20" width="15%" class="RightText">&nbsp; <fmtPartNumInvReport:message key="IMG_ALT_VIEW_PART_DETAILS" var="varViewPartDetails"/><img
										id="imgEditP" style='cursor: hand'
										onclick="javascript:fnCallDisp('<%=strPartNum%>','Print')"
										title="${varViewPartDetails}"
										src="<%=strImagePath%>/product_icon.jpg" style="" /> <fmtPartNumInvReport:message key="IMG_ALT_VIEW_LOCATIONS_DETAILS" var="varViewLocationsDetails"/><img
										id="imgLoction" style='cursor: hand'
										onclick="javascript:fnViewLocations('<%=strPartNum%>')"
										title="${varViewLocationsDetails}"
										src="<%=strImagePath%>/location.png" style="" /> <%=strPartNum%>
									</td>
									<td class="RightTextAS" width="50%">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
									<% if(!strShowFG.equals("")){%>
									<td align="center" class="ShadeMedGreenTD"><%=strQtyInShelf%>&nbsp;</td>
									<td align="center" class="ShadeLightGreenTD"><%=strQtyInShelfAlloc%>&nbsp;</td>
									<%} %>
									<% if(!strShowRW.equals("")){%>
									<td class="ShadeMedOrangeTD" align="right"><%=strQtyInRW%>&nbsp;</td>
									<td class="ShadeLightOrangeTD" align="right"><%=strQtyInRWAlloc%>&nbsp;</td>
									<%} %>
									<% if(!strShowRM.equals("")){%>
									<td class="ShadeMedGrayTD" align="right"><%=strRawMatQty%>&nbsp;</td>
									<td class="ShadeLightGrayTD" align="right"><%=strQtyRMAlloc%>&nbsp;</td>
									<%} %>
									<% if(!strShowBulk.equals("")){%>
									<td class="ShadeMedBlueTD"><%=strQtyInWIPSets%>&nbsp;</td>
									<td class="ShadeLightBlueTD"><%=strTBE%>&nbsp;</td>
									<td class="ShadeMedBlueTD"><%=strQtyInBulk%>&nbsp;</td>
									<td class="ShadeLightBlueTD"><%=strQtyInBulkAlloc%>&nbsp;</td>
									<%} %>
									<% if(!strShowBS.equals("")){%>
									<td class="ShadeMedBrownTD" align="center"><%=strQtyBUILTSET%>&nbsp;</td>
									<%} %>
									<% if(!strShowPN.equals("")){%>
									<td class="ShadeMedGreenTD" align="center"><%=strRepackAvail%>&nbsp;</td>
									<td class="ShadeLightGreenTD" align="center"><%=strRepackAlloc%>&nbsp;</td>
									<%} %>
									<% if(!strShowQN.equals("")){%>
									<td class="ShadeMedOrangeTD"><%=strQtyInQuarantine%>&nbsp;</td>
									<td class="ShadeLightOrangeTD"><%=strQtyInQuarantineAlloc%>&nbsp;</td>
									<%} %>
									<% if(!strShowRtn.equals("")){%>
									<td class="ShadeMedYellowTD" align="center"><%=strQtyInReturnsHold%>&nbsp;</td>
									<td class="ShadeLightYellowTD" align="center"><%=strQtyInReturnsHoldAlloc%>&nbsp;</td>
									<%} %>
									<% if(!strShowPO.equals("")){%>
									<td class="ShadeMedBrownTD" align="center"><%=strQtyInPO%>&nbsp;</td>
									<%} %>
									<% if(!strShowMFG.equals("")){%>
									<td class="ShadeMedOrangeTD"><%=strQtyMFGTBR%>&nbsp;</td>
									<td class="ShadeLightOrangeTD"><%=strQtyMFGRelease%>&nbsp;</td>
									<%} %>
									<% if(!strShowDHR.equals("")){%>
									<td class="ShadeMedBlueTD"><%=strQtyDHRWIP%>&nbsp;</td>
									<td class="ShadeLightBlueTD"><%=strQtyDHR_PEN_QC_INS_%>&nbsp;</td>
									<td class="ShadeMedBlueTD"><%=strQtyDHR_PEN_QC_REL%>&nbsp;</td>
									<td class="ShadeLightBlueTD"><%=strQtyDHR_QC_APP%>&nbsp;</td>
									<td class="ShadeMedBlueTD"><%=strDHRAlloc%></td>
									<%} %>
									<% if(!strShowShip.equals("")){%>
									<td colspan="2" align="center" class="ShadeMedBrownTD"><%=strShipAlloc%>&nbsp;</td>
									<%} %>
									<% if(!strShowIH.equals("")){%>
									<td class="ShadeMedGreenTD"><%=strIhAVailQty%>&nbsp;</td>
									<td class="ShadeLightGreenTD"><%=strIhAllocQty%>&nbsp;</td>
									<td class="ShadeMedGreenTD"><%=strIhSetQty%>&nbsp;</td>
									<td class="ShadeLightGreenTD"><%=strIhItemQty%></td>
									<%} %>
									<% if(!strShowLN.equals("")){%>
									<td align="center" class="ShadeMedOrangeTD"><%=strLoanQty%>&nbsp;</td>
									<%} %>
									<% if(!strShowInTransit.equals("")){%>
									<td class="ShadeMedYellowTD" align="right"><%=strInTransAvailQty%>&nbsp;</td>
									<td class="ShadeLightYellowTD" align="right"><%=strInTransAllocQty%>&nbsp;</td>
									<%} %>

								</tr>
								<tr>
									<td colspan="35" bgcolor="#cccccc" height="1"></td>
								</tr>
								<%
				}
			} else	{
%>
								<tr>
									<td height="30" colspan="35" align="center"
										class="RightTextBlue"><fmtPartNumInvReport:message key="LBL_NO_DATA"/>!</td>
								</tr>
								<%
		}
%>
							</tbody>
						</table>
					</div>
				</td>
			</tr>
			<tr>
				<td class="Line" colspan="2"></td>
			</tr>
		</table>
		<% if (strPageType.equals("popup")){ %>
		<table border="0" width="1200" cellspacing="0" cellpadding="0">
			<tr>
				<br>
				<td align="center" colspan="2"><fmtPartNumInvReport:message key="BTN_CLOSE" var="varClose"/><gmjsp:button
						value="&nbsp;${varClose}&nbsp;" style="width: 6em" name="Btn_Close"
						gmClass="button" buttonType="Load" onClick="window.close();" /></td>
			</tr>
		</table>
		<%} %>
	</FORM>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
