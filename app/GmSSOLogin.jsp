
<%
	/**********************************************************************************
	 * File		 		: GmSSOLogin.jsp
	 * Desc		 		: This screen is used for the Keycloak Login Process
	 * Version	 		: 1.0 
	 * author			: Sathish John
	 ************************************************************************************/
%>
<%@ include file="/common/GmPiwif.inc"%>
<%

request.getRequestDispatcher("/GmLogonServlet").forward(request,response);
 
%>
