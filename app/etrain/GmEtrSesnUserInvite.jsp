<%
/**********************************************************************************
 * File		 		: GmEtrSesnUserInvite.jsp
 * Desc		 		: For inviting user for session
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>

<!-- etrain\GmEtrSesnUserInvite.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel"%>
<bean:define id="alRequestActvUser" name="frmEtrSesnUserInvite" property="invitedUserList" type="java.util.ArrayList"></bean:define>
<bean:define id="alRequestUser" name="frmEtrSesnUserInvite" property="userDetailList" type="java.util.ArrayList"></bean:define>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%
String strWikiTitle = GmCommonClass.getWikiTitle("USER_INVITE");

String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: User Activation/Invitation </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrSesnUserInvite.js"></script>
<script>
function fnPrintRecord(){
document.frmEtrSesnUserInvite.strOpt.value = 'printrecords';
	document.frmEtrSesnUserInvite.submit();

}
function fnPrintSummary(){
document.frmEtrSesnUserInvite.strOpt.value = 'printsummary';
	document.frmEtrSesnUserInvite.submit();

}
</script>

<style type="text/css" media="all">
   @import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmETSesnUserInvite.do">
<html:hidden property="strOpt" />
<html:hidden property="sessionId" />
<html:hidden property="hinputString" />
<html:hidden property="hinviteList" />
<html:hidden property="sessionOwner" />
<html:hidden property="statusId" />
<html:hidden property="userSeq" />
<html:hidden property="userId" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VSUMP"/>
<html:hidden property="strUserId"/>

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">User Activation/Invitation</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
                        <td class="RightTableCaption" align="right" HEIGHT="25"></font>&nbsp;Session Id:</td> 
                        <td>&nbsp;<bean:write name="frmEtrSesnUserInvite" property="sessionId"/></td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="25"></font>&nbsp;Max User:</td> 
                        <td>&nbsp;<bean:write name="frmEtrSesnUserInvite" property="maxuser"/></td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr>
                        <td class="RightTableCaption" align="right" HEIGHT="25"></font>&nbsp;Description:</td> 
                        <td>&nbsp;<bean:write name="frmEtrSesnUserInvite" property="sessionDesc"/></td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr class="Shade">
                    	<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%"></font>&nbsp;Start Date:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="0%">&nbsp;<bean:write name="frmEtrSesnUserInvite" property="sessionStartDt"/> 
									</td>								
									<td class="RightTableCaption" align="right" width="40%"></font>&nbsp;End Date:</td>
									<td width= "55%">&nbsp;
			                    		<bean:write name="frmEtrSesnUserInvite" property="sessionEndDt"/>                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr>
                        <td class="RightTableCaption" align="right" HEIGHT="25"></font>&nbsp;Owner:</td> 
                       <td>&nbsp;<bean:write name="frmEtrSesnUserInvite" property="sessionOwnerNm"/></td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr class="Shade">
                    	<td class="RightTableCaption" align="right" HEIGHT="25" width="30%"></font>&nbsp;Location Type:</td>
						<td>&nbsp;<bean:write name="frmEtrSesnUserInvite" property="sessionLcnTypeNm"/></td>						
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr><td height="20" class="RightDashBoardHeader">Active Users:-</td><td align="right" class=RightDashBoardHeader ></td></tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 
					<%if(alRequestActvUser.size()>0)
					{									
							boolean freezeHead = alRequestActvUser.size() > 8 ? true:false;
							int size = freezeHead?250:(256/8)*alRequestActvUser.size();							
					%>
														
					<tr> 							
			            <td colspan="2">			            
				            <display:table name="requestScope.frmEtrSesnUserInvite.invitedUserList" requestURI="/gmETSesnUserInvite.do" freezeHeader= "<%=freezeHead %>" paneSize="<%=Integer.toString(size)%>" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">						
							<display:column property="USRMAPSEQ" title="Details" class="aligncenter" style="width:100px" /> 
							<display:column property="USRID" title="<input type='checkbox' name='inviteAll' onclick='fnInviteAll();' /> User ID" class="alignleft" style="width:100px" />
							<display:column property="USERNM" title="User Name" class="alignleft"/> 
							<display:column property="COMPLTNDT" title="Completed Date" class="alignleft" style="width:80px" />
							<display:column property="STATUS" title="Status" class="alignleft" style="width:80px" />							
							</display:table>
												
						</td>											
		    	   </tr>
		    	  
		    	   <tr><td colspan="2" class="LLine" height="1"></td></tr> 
		    	   <%} else
					{
			  		%>  		
					<tr>
			  		<td height="20">No Active Users Found</td>
			  		</tr>
			  		<%} %>	
		    	   <tr><td height="20" class="RightDashBoardHeader">Add/Search Users:-</td><td align="right" class=RightDashBoardHeader ></td></tr>					
					<tr>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%"></font>&nbsp;First Name:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td>&nbsp;
	                         		<html:text property="fname"  size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>	                         
                        		</td>								
								<td class="RightTableCaption" align="right" HEIGHT="25"></font>&nbsp;Last Name:</td>
								<td>&nbsp;
	                         		<html:text property="lname"  size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>	                         
                        		</td> 
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr class="Shade">
                    	<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%"></font>&nbsp;Department:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td width="59%">&nbsp;
									<gmjsp:dropdown controlName="deptList" SFFormName="frmEtrSesnUserInvite" SFSeletedValue="deptList"
										SFValue="alDeptList" codeId = "CODEID"  codeName = "CODENMALT"  defaultValue= "[Choose One]" />		                         
                        		</td>								
								<td class="RightTableCaption" align="left" HEIGHT="25"></td>
								<td>&nbsp;&nbsp;&nbsp;
	                         		<input type="button" value="&nbsp;&nbsp;Load&nbsp;&nbsp;" class="button" onCLick="fnLoad();" />	                         
                        		</td> 
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<%if(alRequestUser.size()>0)
					{					
							boolean freezeHead = alRequestUser.size() > 8 ? true:false;	
							int size = freezeHead?250:(256/8)*alRequestUser.size();
					%>									
					<tr>									
			            <td colspan="2">			            	 
				            <display:table name="requestScope.frmEtrSesnUserInvite.userDetailList" requestURI="/gmETSesnUserInvite.do" freezeHeader= "<%=freezeHead%>" paneSize="<%=Integer.toString(size)%>" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">
				            <display:column property="USERID" title="<input type='checkbox' name='selectAll' onclick='fnSelectAll();' /> User ID" class="alignleft" style="width:100px" /> 
							<display:column property="USERNM" title="User Name" class="alignleft" style="width:100px" />
							<display:column property="DEPTNM" title="Department" class="alignleft" style="width:100px"/>																					
							</display:table> 						
						</td>												
		    	   </tr>
		    	   
		    	   <tr><td colspan="2" class="LLine" height="1"></td></tr> 		
		    	   <%} else {
			  		%>  		
					<tr>
			  		<td height="20">No Search Users Found</td>
			  		</tr>
			  		<tr><td colspan="2" class="LLine" height="1"></td></tr>
			  		<%} %>			
				   <tr>
						<td colspan="2" align="center" HEIGHT="25">&nbsp;
							<input type="button" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();" />						
						</td>
					</tr>
					<tr><td height="20" class="RightDashBoardHeader">Invite Users:-</td><td align="right" class=RightDashBoardHeader ></td></tr>
		    	   <tr><td colspan="2" class="LLine" height="1"></td></tr> 					
				    <tr>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%"></font>&nbsp;Select Template:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td width="59%">&nbsp;
									<select class="RightText" name="Cbo_Opt" onChange="javascript:fnTemplate(this)">
							  			<option value="Type1">Type1
										<option id="Type2" value="Type2">Type2
										<option id="Type3" value="Type3">Type3																	
							  		</select>
							  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	                         		<input type="button" value="Preview" class="button" onCLick="fnPreview();" />&nbsp;&nbsp;		                         
                        		</td>								
								<td class="RightTableCaption" align="left" HEIGHT="25"></td>
								<td>
                        		</td> 
								</tr>
							</table>
						</td>
					</tr>		
				    <tr><td colspan="2" class="LLine" height="1"></td></tr>
   				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
</BODY>

</HTML>

