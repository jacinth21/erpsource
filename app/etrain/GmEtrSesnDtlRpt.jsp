<%
/**********************************************************************************
 * File		 		: GmEtrSesnDtlRpt.jsp
 * Desc		 		: For Session Detail Report
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>

<!-- etrain\GmEtrSesnDtlRpt.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<bean:define id="alRequest" name="frmEtrSesnDtlRpt" property="alSesnDtlRptList" type="java.util.ArrayList"></bean:define>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("SESN_RPT");
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Session Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrSesnDtlRpt.js"></script>
<script>

</script>
<style type="text/css" media="all">
  @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmETSesnDtlRpt.do">
<html:hidden property="strOpt" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VSESN"/>
<html:hidden property="strSessionId" />
<html:hidden property="strSesnOwner" />
<html:hidden property="strSessionStartDt" />
<html:hidden property="strStatus" />
<html:hidden property="userId" />

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Session Detail Report</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25" width="20%"></font>&nbsp;Owner:</td>
						<td width="25%">&nbsp;
						<gmjsp:dropdown controlName="sessionOwner" SFFormName="frmEtrSesnDtlRpt" SFSeletedValue="sessionOwner"
								SFValue="alSessionOwnerList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="25" width="20%"></font>&nbsp;Module List:</td>
						<td width="20%">&nbsp;
						<gmjsp:dropdown controlName="moduleId" SFFormName="frmEtrSesnDtlRpt" SFSeletedValue="moduleId"
								SFValue="alModuleList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="25" width="40%"></font>&nbsp;Program List:</td>	
						<td width="50%">&nbsp;
						<gmjsp:dropdown controlName="courseId" SFFormName="frmEtrSesnDtlRpt" SFSeletedValue="courseId"
								SFValue="alCourseList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>					
					</tr>
					<tr><td colspan="6" class="LLine" height="1"></td></tr> 					 
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="25" width="20%"></font>&nbsp;Start Date:</td>
                        <td width="25%">&nbsp;
                        <html:text property="sessionStartDt"  size="10" styleClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmEtrSesnDtlRpt.sessionStartDt');" title="Click to open Calendar"  
						src="<%=strExtImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp; 
                        </td>
                        <td class="RightTableCaption" align="right" HEIGHT="25" width="20%"></font>&nbsp;End Date:</td>
                        <td width="20%">&nbsp;
                        <html:text property="sessionEndDt"  size="10" styleClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmEtrSesnDtlRpt.sessionEndDt');" title="Click to open Calendar"  
						src="<%=strExtImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp; 
                        </td>
                        <td class="RightTableCaption" align="right" HEIGHT="25" width="50%"></font>&nbsp;Status:</td>
						<td width="50%">&nbsp;
						<gmjsp:dropdown controlName="status" SFFormName="frmEtrSesnDtlRpt" SFSeletedValue="status"
								SFValue="alStatusList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
                    </tr>
                    <tr><td colspan="6" class="LLine" height="1"></td></tr>  
                    <tr>
                    	<td class="RightTableCaption" align="right" width="20%"></font>&nbsp;Location Type:</td>
                    	<td width="25%">&nbsp;
						<gmjsp:dropdown controlName="sessionLcnType" SFFormName="frmEtrSesnDtlRpt" SFSeletedValue="sessionLcnType"
								SFValue="alSessionLcnTypeList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
						<td class="RightTableCaption" align="right" width="20%"></font>&nbsp;</td>
                    	<td width="20%">&nbsp;																		
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="25" width="50%"></td>
						<td width="50%">&nbsp;
							<input type="button" value="&nbsp;&nbsp;Load&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnLoad();" />
						</td>
                    </tr>                    
					
					<%if (alRequest.size() > 0) 
					{
					%>
					<tr><td colspan="6" class="LLine" height="1"></td></tr> 	
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr> 
			            <td colspan="2">
				            <display:table name="requestScope.frmEtrSesnDtlRpt.alSesnDtlRptList" requestURI="/gmETSesnDtlRpt.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">						
							<display:column property="SESNID" title="Session" class="alignleft" style="width:150px" sortable="true"/>
							<display:column property="SESSIONDESC" title="Description" class="alignleft" sortable="true"/>
							<display:column property="SESSIONOWNER" title="Session Owner" class="alignleft" sortable="true"/>
							<display:column property="SESSIONSTARTDT" title="Start Date" class="alignleft" style="width:80px" sortable="true" />
							<display:column property="SESSIONENDDT" title="End Date" class="alignleft" style="width:80px" sortable="true" />
							<display:column property="SESSIONLCNTYPE" title="Location Type" class="alignleft" style="width:80px" />
							<display:column property="MAXUSER" title="Total User" class="alignleft" style="width:80px" />
							<display:column property="STATUS" title="Status" class="alignleft" style="width:80px" sortable="true" />							
							</display:table> 						
						</td>
		    	   </tr>
		    	   </table>
		    	   <tr><td colspan="6" class="LLine" height="1"></td></tr>
		    	   <tr class="Shade"> 
		    	   <tr>
						<td HEIGHT="25" >
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="center" width="5%"></font>&nbsp;</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25" >
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="center" width="20%"></font>&nbsp;Select Action:</td>
								<td width="40%">&nbsp;
									<select class="RightText" name="Cbo_Opt">
										<option id="Action0" value="0">[Choose One]
							  			<option id="Action1" value="void">Void Session
										<option id="Action2" value="load">Edit Session
										<option id="Action3" value="manageUser">Manage User
										<option id="Action4" value="manageMod">Manage Module
										<option id="Action5" value="close">Close Session																	
							  		</select>		                         
                        		</td>
                        		<td class="RightTableCaption" align="left" HEIGHT="25"></td>
								<td>
	                         		<input type="button" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();" />                        		                        
                        		</td> 
                        		<td width="25%">&nbsp;&nbsp;&nbsp;</td>	<td>&nbsp;&nbsp;&nbsp;</td>							
								
								</tr>
							</table>
						</td>						
					</tr>
					<tr><td colspan="6" class="LLine" height="1"></td></tr>	
								          
   				</table>
  			   </td>
  		  </tr>	
  		  <%}else
			{%>  
		  		<tr><td colspan="6" class="LLine" height="1"></td></tr>							  					          
   				</table>
  			   </td>
  		  </tr>	
  		  <tr>
		  		<td height="20">No Session Detail Found.</td>
		  </tr>
  		  <%} %>	
    </table>		     	
</html:form>
</BODY>

</HTML>

