



<%
/**********************************************************************************
 * File		 		: GmEtrUserMatMap.jsp
 * Desc		 		: For User material mapping
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>


<!-- etrain\GmEtrUserMatMap.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="alRequest" name="frmEtrUserMatMap" property="alUserMatMapList" type="java.util.ArrayList"></bean:define>
<bean:define id="alRequestMat" name="frmEtrUserMatMap" property="alMatList" type="java.util.ArrayList"></bean:define>
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("USER_MAT_MAP"); 
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
	String strFileNm = "";
	String strFileId = "";
	String strUserMatTrackId = GmCommonClass.parseZero((String)request.getParameter("userMatTrackId"));
	String flag = GmCommonClass.parseNull((String)request.getParameter("flag"));
	if("0".equals(strUserMatTrackId))
	{
	strFileNm = GmCommonClass.parseNull((String)request.getParameter("fileNm"));
	strFileId = GmCommonClass.parseNull((String)request.getParameter("fileId"));
	}
	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: User Material Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
       @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrUserMatMap.js"></script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmETUserMatMap.do">
<html:hidden property="strOpt" />
<html:hidden property="moduleId" />
<html:hidden property="sessionId" />
<html:hidden property="fileNm" />
<html:hidden property="fileId" />
<html:hidden property="sesnUserMatId" />
<html:hidden property="userMatTrackId" />
<html:hidden property="matCmpltnDt" />
<html:hidden property="hinputString" /> 
<html:hidden property="hinputString1" />
<html:hidden property="trcmpltn" value=""/>
<html:hidden property="flag" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">User Material Mapping</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%">&nbsp;Session ID:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="20%">&nbsp;&nbsp;<bean:write name="frmEtrUserMatMap" property="sessionId"/> 
									</td>								
									<td class="RightTableCaption" align="right" width="50%">&nbsp;Session Owner:</td>
									<td width= "55%">&nbsp;
			                    		<bean:write name="frmEtrUserMatMap" property="sessionownernm"/>                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="Shade">
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%">&nbsp;Module ID:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="20%">&nbsp;&nbsp;<bean:write name="frmEtrUserMatMap" property="moduleId"/> 
									</td>								
									<td class="RightTableCaption" align="right" width="50%">&nbsp;Module Name:</td>
									<td width= "55%">&nbsp;
			                    		<bean:write name="frmEtrUserMatMap" property="moduleName"/>                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>                     
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Module Owner:</td>
						<td>&nbsp;&nbsp;<bean:write name="frmEtrUserMatMap" property="ownername"/></td>										
					</tr>									
					<%
						if (alRequest.size() > 0 && !flag.equals("matList")){ 
							//System.out.println("file....1...."+alRequest.size()+"..."+strFileId);	
						%> 		
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade"> 
			            <td colspan="2">
				            <display:table name="requestScope.frmEtrUserMatMap.alUserMatMapList" requestURI="/gmETUserMatMap.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">
				            <display:column property="HLINK" title="File Name" class="alignleft" style="width:185px" sortable="true"/> 
							<display:column property="FILETYPE" title="File Type" class="alignleft" sortable="true" style="width:60px"/> 
							<display:column property="MODTRAINER" title="Trainer" class="alignleft" style="width:120px" sortable="true" />
							<display:column property="CRDATE" title="Created Date" class="alignleft" style="width:75px" sortable="true" />
							<display:column property="CNT" title="Count" class="alignleft" style="width:40px"/>
							<display:column property="DURATION" title="Duration" class="alignleft" style="width:40px"/>
							<display:column property="MANDATORY" title="Mandatory" class="aligncenter" style="width:45px"   />
							<display:column property="MATCMPLTNDT" title="Completion Date" class="aligncenter" style="width:45px"   />
														
							</display:table> 						
						</td>
		    	   </tr>		    	   
					<%
					}else if (alRequestMat.size() > 0 && flag.equals("matList"))
					{
					%>	
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade"> 
			            <td colspan="2">
				            <display:table name="requestScope.frmEtrUserMatMap.alMatList" requestURI="/gmETUserMatMap.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">
				            <display:column property="MAT" title="Add<input type='checkbox' name='selectAll' onclick='fnSelectAll();' /> Remove<input type='checkbox' name='applyAll' onclick='fnApplyAll();' /> Material Name" class="alignleft" style="width:450px" sortable="true"/> 
							<display:column property="MATSTAT" title="Material Status" class="alignleft" sortable="true" style="width:50px"/>
							<display:column property="MATTYPE" title="Material Type" class="alignleft" sortable="true" style="width:50px"/>
							</display:table> 						
						</td>
    	  			</tr>
    	  			<tr><td colspan="2" class="LLine" height="1"></td></tr>
	    	  		<tr>
						<td colspan="2" align="center" HEIGHT="25">&nbsp;
							<input type="button" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();" />						
						</td>
					</tr>			    	   
					<%
						}else
						{
				  		%> 
				  		<tr><td colspan="2" class="LLine" height="1"></td></tr> 		
						<tr class="Shade">
				  		<td height="20">No Material Found</td><td></td>
				  		</tr>
				  		<%} %>							          
   				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
</BODY>

</HTML>

