<%
/**********************************************************************************
 * File		 		: GmEtrCourseSetup.jsp
 * Desc		 		: For Course Detail Setup
 * Version	 		: 1.0
 * author			: angela
************************************************************************************/
%>

<!-- etrain\GmEtrCourseSetup.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%

 
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
String strWikiTitle = GmCommonClass.getWikiTitle("COURSE_DETAIL_SETUP");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Program Detail Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
 <script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrCourseSetup.js"></script>
<script>

</script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad=fnChngStatus();>
<html:form action="/gmETCourseSetup.do">
<html:hidden property="haction" />
<html:hidden property="strOpt" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VCOSE"/>
<html:hidden property="userId" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr >
			<td height="25" class="RightDashBoardHeader">Program Setup</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="LLine"></td></tr>
		<tr >
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">				
				 <tr>
                        <td class="RightTableCaption" align="right" height="25"></font>&nbsp;Program List:</td>
                       <td>&nbsp;
                       <gmjsp:dropdown controlName="courseId" SFFormName="frmEtrCourseSetup" SFSeletedValue="courseId"
							SFValue="alCourseList" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" onChange="fnLoad(this.value)"/>    &nbsp;&nbsp; 
                        </td>
                    </tr> 
				<tr><td colspan="4" class="LLine" height="1"></td></tr>
                    <tr class="Shade">
						<td class="RightTableCaption" align="right" height="25"></font>&nbsp;Program ID:</td>
						<td>&nbsp;
						<bean:write name="frmEtrCourseSetup" property="courseId"/></td>										
					</tr>
					  <tr><td colspan="4" class="LLine" height="1"></td></tr> 
					<tr>
						<td class="RightTableCaption" align="right" height="24"><font color="red">*</font>&nbsp;Program Name:</td>
						<td>&nbsp;
						<html:text property="courseName" size="50" maxlength="100" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>   
						</td>										
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr>	
					<tr class="Shade">
                        <td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;Program Description:</td> 
                        <td>&nbsp;
		                    <html:textarea property="description" cols="50" rows ="3"></html:textarea>                                        
                        </td>
                    </tr>  
                    <tr><td colspan="4" class="LLine" height="1"></td></tr> 
					<tr>
						<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;Program Owner:</td>
						<td>&nbsp;
						<gmjsp:dropdown controlName="courseOwner" SFFormName="frmEtrCourseSetup" SFSeletedValue="courseOwner"
								SFValue="alCourseOwnerList" codeId="CODEID" codeName ="CODENM"  defaultValue= "[Choose One]" />	
						</td>										
					</tr>	
					  <tr><td colspan="4" class="LLine" height="1"></td></tr>                  
                    <tr class="Shade">
						<td class="RightTableCaption" align="right" height="25" width="30%"><font color="red">*</font>&nbsp;Program Status:</td>
						<td width="70%">&nbsp;
						<gmjsp:dropdown controlName="courseStatus" SFFormName="frmEtrCourseSetup" SFSeletedValue="courseStatus"
								SFValue="alStatusList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>						
					</tr> 
					<tr><td colspan="2" class="LLine" height="1"></td></tr>                    
                   	<tr>
						<td colspan="2" align="center" height="25">&nbsp;
	                    	<jsp:include page="/common/GmIncludeLog.jsp" >
		                    	<jsp:param name="FORMNAME" value="frmEtrCourseSetup" />
								<jsp:param name="ALNAME" value="alLogReasons" />
								<jsp:param name="LogMode" value="Edit" />																					
							</jsp:include>	
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1">
					<tr>
						<td colspan="2" align="center" height="25">&nbsp;
						<input type="button" id="mapDept" value="&nbsp;&nbsp;Map Dept&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnMapDept();" />									
						<input type="button" id="voidCourse" value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnVoid();" />
						<input type="button" id="submitCourse" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();" />	
						<input type="button" value="&nbsp;&nbsp;Reset&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnReset();" >					
						</td>
					</tr>					          
   				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
</BODY>

</HTML>

