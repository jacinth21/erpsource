<%
/**********************************************************************************
 * File		 		: GmEtrSesnSetup.jsp
 * Desc		 		: For Session Detail Setup
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>


<!-- etrain\GmEtrSessionSetup.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("SESN_SETUP");
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Session Detail Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
       @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrSesnSetup.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmETSessionSetup.do">
<html:hidden property="haction" />
<html:hidden property="strOpt" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VSESN"/>
<html:hidden property="sessionId" />
<html:hidden property="sesnlcnval" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Session Setup</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr><td colspan="2" class="LLine" height="1"></td></tr>					
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Session ID:</td>
						<td>&nbsp;&nbsp;<bean:write name="frmEtrSessionSetup" property="sessionId"/></td>										
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Session Description:</td> 
                        <td>&nbsp;
		                    <html:text property="sessionDesc" maxlength="255" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>                                        
                        </td>
                    </tr> 
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Start Date:</td>
                        <td>&nbsp;
                        <html:text property="sessionStartDt"  size="10" styleClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmEtrSessionSetup.sessionStartDt');" title="Click to open Calendar"  
						src="<%=strExtImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp; 
                        </td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">                    
                        <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;End Date:</td>
                        <td>&nbsp;
                        <html:text property="sessionEndDt"  size="10" styleClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmEtrSessionSetup.sessionEndDt');" title="Click to open Calendar"  
						src="<%=strExtImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp; 
                        </td>
                    </tr>  
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="25" width="30%"><font color="red">*</font>&nbsp;Owner:</td>
						<td width="70%">&nbsp;
						<gmjsp:dropdown controlName="sessionOwner" SFFormName="frmEtrSessionSetup" SFSeletedValue="sessionOwner"
								SFValue="alOwnerList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>						
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">  
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%"><font color="red">*</font>&nbsp;Location Type:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="0%">&nbsp;
										<gmjsp:dropdown controlName="sessionLcnType" onChange="fnSelLcn(this);" SFFormName="frmEtrSessionSetup" SFSeletedValue="sessionLcnType"
										SFValue="alSessionLcnList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /> 
									</td>								
									<td class="RightTableCaption" align="right" width="0%">&nbsp;Location:</td>
									<td width= "55%">&nbsp;
										<logic:equal name="frmEtrSessionSetup" property="lcnFlag" value="true">
											<gmjsp:dropdown controlName="sessionLcn" SFFormName="frmEtrSessionSetup" SFSeletedValue="sesnlcnval"
												SFValue="alSessionLcnVal" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
										</logic:equal>	
			                    		<logic:notEqual name="frmEtrSessionSetup" property="lcnFlag" value="true">
			                    			<html:text property="sessionLcn" maxlength="255" size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
										</logic:notEqual>
			                    		                                        		
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>					
					<tr>
                        <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Duration:</td> 
                        <td>&nbsp;
		                    <html:text property="duration"  size="10" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;(Hrs)                                        
                        </td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">  
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%">&nbsp;No of User:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td>&nbsp;
	                         		<html:text property="minUser"  size="10" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;(MIN)<font color="red">*</font>	                         
                        		</td>								
								<td class="RightTableCaption" align="right" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td width= "55%">&nbsp;
	                         		<html:text property="maxUser"  size="10" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;(MAX)<font color="red">*</font>	                         
                        		</td> 
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>					                   
                    <tr>
						<td class="RightTableCaption" align="right" HEIGHT="25" width="30%"><font color="red">*</font>&nbsp;Status:</td>
						<td width="70%">&nbsp;
						<gmjsp:dropdown controlName="status" SFFormName="frmEtrSessionSetup" SFSeletedValue="status"
								SFValue="alStatusList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>						
					</tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                   	<tr>
						<td colspan="2" align="center" HEIGHT="25">&nbsp;
	                    	<jsp:include page="/common/GmIncludeLog.jsp" >
		                    	<jsp:param name="FORMNAME" value="frmEtrSessionSetup" />
								<jsp:param name="ALNAME" value="alLogReasons" />
								<jsp:param name="LogMode" value="Edit" />																					
							</jsp:include>	
						</td>
					</tr>									
					<tr>
						<td colspan="2" align="center" HEIGHT="25">&nbsp;
						<input type="button" value="&nbsp;&nbsp;Map Module&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnMapMod();" />									
						<input type="button" value="&nbsp;&nbsp;Add User&nbsp;&nbsp;&nbsp;" class="button"  onCLick="fnAddUser();"/>
						<input type="button" value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnVoid();" />
						<input type="button" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();" />						
						</td>
					</tr>					          
   				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
</BODY>

</HTML>

