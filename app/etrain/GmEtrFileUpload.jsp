
<%
	/**********************************************************************************
	 * File		 		: GmEtrFileUpload.jsp
	 * Desc		 		: Upload - Upload page
	 * Version	 		: 1.0
	 * author			: Ritesh Shah
	 ************************************************************************************/
%>


<!-- etrain\GmEtrFileUpload.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<html>
<%
    String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
	String strAction = GmCommonClass.parseNull((String) request.getAttribute("hAction"));	
	String strFileTypeList = GmCommonClass.parseNull((String) request.getAttribute("strFileTypeList"));
	String strId = GmCommonClass.parseNull((String) request.getAttribute("strId"));
	strId = strId == "" ? GmCommonClass.parseNull((String) request.getParameter("strId")) : strId;
	
	ArrayList alUploadinfo = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alUploadinfo"));
	ArrayList alFileTypeList = GmCommonClass.parseNullArrayList((ArrayList) request.getAttribute("alFileTypeList"));	
%>
<head>
<title>Upload File</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
</head>
<BODY>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<style type="text/css" media="all">
        @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrFileUpload.js"></script>
<form method="post" name="frmEtrFileUpload" enctype="multipart/form-data" action="/GmEtrFileUploadServlet">
<input type="hidden" name="hAction" value"<%=strAction%>"> 
<input type="hidden" name="strId" value="<%=strId %>">  
<input type="hidden" name="fileNm">

	<table border="0" class="DtTable698" cellspacing="0" cellpadding="0">
		<tr height="24">
			<td class="RightTableCaption" width="50%" align="right">Material Type&nbsp;:&nbsp;</td>
			<td width="50%" align="left">&nbsp;
				<gmjsp:dropdown controlName="strFileTypeList" seletedValue="<%=strFileTypeList %>"
				value="<%=alFileTypeList%>" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"
				onChange="fnUpReload(this.form)" />
			</td>
		</tr>		
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<%		
		//if ((strAction.equals("reload") || strAction.equals("upload")) && !strFileTypeList.equals("")) 
		{
		%>
		<tr>
			<td colspan="2" width="100%" height="100" valign="top">
				<div align="center"><jsp:include page="/common/GmUpload.jsp"></jsp:include></div>
			</td>
		</tr>
		<tr><td colspan="2" class="LLine" height="1"></td></tr>
		<tr>
			<td colspan="2" >
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="TableCaption" colspan="6" align="left">&nbsp;Previous Upload</td>
					</tr>
					<tr><td colspan="6" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" height="20" align="center">&nbsp;&nbsp;</td>
						<td class="RightTableCaption">&nbsp;Material ID</td>
						<td class="RightTableCaption">&nbsp;Material Name</td>
						<td class="RightTableCaption">&nbsp;Material Type</td>
						<td class="RightTableCaption">&nbsp;Uploaded Date</td>
						<td class="RightTableCaption">&nbsp;Uploaded By</td>
					</tr>
					<tr><td colspan="6" class="LLine" height="1"></td></tr>
					<%
						for (int i = 0; i < alUploadinfo.size(); i++) {
							HashMap hmLoop = (HashMap) alUploadinfo.get(i);
							String fileName = (String)hmLoop.get("NAME");
							String source = "";
							if(fileName.endsWith(".pdf"))
								source = "<%=strExtImagePath%>/pdf_icon.jpg";
							else if(fileName.endsWith(".doc") || fileName.endsWith(".DOC"))
								source = "<%=strExtImagePath%>/word_icon.jpg";
							else if(fileName.endsWith(".rtf") || fileName.endsWith(".RTF"))
								source = "<%=strExtImagePath%>/word_icon.jpg";
							else if(fileName.endsWith(".jpg") || fileName.endsWith(".JPG") || fileName.endsWith(".bmp"))
								source = "<%=strExtImagePath%>/jpg_icon.jpg";
							else if (fileName.endsWith(".html") || fileName.endsWith(".htm"))
								source = "<%=strExtImagePath%>/html.gif";
							else if (fileName.endsWith(".txt") || fileName.endsWith(".TXT"))
								source = "<%=strExtImagePath%>/txt.gif";
							else if (fileName.endsWith(".ppt") || fileName.endsWith(".pptx"))
								source = "<%=strExtImagePath%>/ppt_icon.jpg";
							else 
								source = "<%=strExtImagePath%>/edit_icon.gif";							
					%>
					<tr>
						<td align="center" height="20" style="width: 45">
							<a href="javascript:fnVoid(<%=hmLoop.get("ID")%>);">
								Void
							</a>
						</td>
						<td align="RightText" height="25" style="width: 100">
							<a href="javascript:fnOpenForm(<%=hmLoop.get("ID")%>);">
								<img border="0" src="<%=source%>" height="20px" width="20px"></img>								
							</a>
							File No-<%=hmLoop.get("ID")%>
						</td>
						<td class="RightText" width="80px">&nbsp;<%=hmLoop.get("NAME")%></td>
						<td class="RightText" width="100px">&nbsp;<%=hmLoop.get("MATTYPE")%></td>
						<td class="RightText" width="40px">&nbsp;<%=hmLoop.get("CDATE")%></td>
						<td class="RightText" width="100px">&nbsp;<%=hmLoop.get("UNAME")%></td>
					</tr>
					<tr><td colspan="6" class="LLine" height="1"></td></tr>
					<%
						}
					%>
				</table>
			</td>
		</tr>
	<%} %>
	</table>
</form>
</body>
</html>
