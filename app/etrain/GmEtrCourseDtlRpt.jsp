<%
/**********************************************************************************
 * File		 		: GmEtrCourseDtlRpt.jsp
 * Desc		 		: For Course Detail Report
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>

<!-- etrain\GmEtrCourseDtlRpt.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel"%>

<bean:define id="alRequest" name="frmEtrCourseDtlRpt" property="alCourseDetailRpt" type="java.util.ArrayList"></bean:define>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<% 
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
 
String strWikiTitle = GmCommonClass.getWikiTitle("ETR_COURDTL_RPT");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Program Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>Globus.css">
<script language="JavaScript" src="<%=strJsPath%>GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrCourseDtlRpt.js"></script>

<script>

</script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmETCourseDtlRpt.do">
<html:hidden property="strOpt" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VCOSE"/>
<html:hidden property="courseId" />
<html:hidden property="courseName" />
<html:hidden property="strUserId" />
<html:hidden property="strSessionId" />
<html:hidden property="moduleId" />
<html:hidden property="strOwner" />
<html:hidden property="userId" />

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Program Detail Report</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>			
		</tr>
		<tr>
			<td width="848" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="4" class="LLine" height="1"></td></tr> 
					<tr>
					<td class="RightTableCaption" align="right" HEIGHT="25" width="20%"></font>&nbsp;Department ID:</td>
					<td width="25%">&nbsp;
									<gmjsp:dropdown controlName="courseDeptId" SFFormName="frmEtrCourseDtlRpt" SFSeletedValue="courseDeptId"
										SFValue="alDept" codeId = "CODEID"  codeName = "CODENMALT"  defaultValue= "[Choose One]" />		                         
                        		</td>		
						<td class="RightTableCaption" align="right" HEIGHT="25" width="20%"></font>&nbsp;Owner:</td>
						<td width="25%">&nbsp;
						<gmjsp:dropdown controlName="ownerId" SFFormName="frmEtrCourseDtlRpt" SFSeletedValue="ownerId"
								SFValue="alOwners" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />&nbsp;&nbsp;&nbsp;													
						</td>
					</tr>
					<tr><td colspan="4" class="LLine" height="1"></td></tr> 					 
                    <tr class="Shade" >
                        <td class="RightTableCaption" align="right" HEIGHT="25" width="50%"></font>&nbsp;Status:</td>
						<td width="50%">&nbsp;
						<gmjsp:dropdown controlName="statusId" SFFormName="frmEtrCourseDtlRpt" SFSeletedValue="statusId"
								SFValue="alStatus" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
						<td width="50%" colspan="2">&nbsp;
							<input type="button" value="&nbsp;&nbsp;GO&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnLoad();" />
						</td>
                    </tr>
                    <%if (alRequest.size() > 0) 
					{
					%>
					<tr><td colspan="6" class="LLine" height="1"></td></tr> 	
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr> 
			            <td colspan="2">
							<display:table name="requestScope.frmEtrCourseDtlRpt.alCourseDetailRpt" requestURI="/gmETCourseDtlRpt.do" excludedParams="haction" class="its" id="currentRowObject" decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">
							<display:column property="PROGRAMID" title="Program ID" class="alignleft" style="width:150px" />
							<display:column property="COURSENAME" title="Program Name" class="alignleft"/>
							<display:column property="OWNERNAME" title="Owner" class="alignleft" style="width:100px" />
							<display:column property="COURSEDEPTNAME" title="Department" class="alignleft" style="width:100px" />
							<display:column property="STATUSNAME" title="Status" class="alignleft" style="width:100px" />						
							</display:table> 						
						</td>
		    	   </tr>
		    	   </table>
		    	   <tr><td colspan="6" class="LLine" height="1"></td></tr>
		    	   <tr class="Shade"> 
		    	   <tr>
						<td HEIGHT="25" >
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="center" width="5%"></font>&nbsp;</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25" >
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="center" width="20%"></font>&nbsp;Select Action:</td>
								<td>&nbsp;
									<select class="RightText" name="Cbo_Opt">
										<option id="Action0" value="0">[Choose One]
							  			<option id="Action1" value="void">Void Action
										<option id="Action2" value="load">Edit Action																	
							  		</select>
								&nbsp;&nbsp;&nbsp;
	                         		<input type="button" value="Submit" class="button" onCLick="fnSubmit();" />&nbsp;&nbsp;	                         		                        
                        		</td> 
                        		<td>&nbsp;&nbsp;&nbsp;</td>	
								</tr>
							</table>
						</td>						
					</tr>
					<tr><td colspan="6" class="LLine" height="1"></td></tr>			
									          
   				</table>
  			   </td>
  		  </tr>	
					<%}else
					{
				  		%>  
				  		<tr><td colspan="6" class="LLine" height="1"></td></tr>							  					          
   				</table>
  			   </td>
  		  </tr>	
  		  <tr>
				  		<td height="20">No Program Detail Found.</td>
				  		</tr>
  		  <%} %>	
    </table>		     	
</html:form>
</BODY>

</HTML>

