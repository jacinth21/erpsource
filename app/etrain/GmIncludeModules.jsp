<%
/**********************************************************************************************
 * File		 		: GmIncludeModules.jsp
 * Desc		 		: JSP file to display the module details
 * Version	 		: 1.0
 * author			: Ritesh Shah
/***********************************************************************************************/
%>


<!-- etrain\GmIncludeModules.jsp -->

<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

    
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

	<% 
		String formName = request.getParameter("FORMNAME");
	String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
		String listName = request.getParameter("LISTNAME");
		String displayType = request.getParameter("TYPE");
		String tableName = "requestScope." + formName + "." + listName;
		String sessionId = request.getParameter("sessionId");		
		String href = "/gmETSesnMatMap.do?sessionId=" + sessionId;
	%>
<bean:define id="alRequestReport" name="<%=formName %>" property="<%=listName %>" type="java.util.ArrayList"></bean:define>
<script>
var temp = <%=alRequestReport.size()%>;
document.<%=formName %>.alLength.value = temp;
</script>	
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/lib/displaytag.tld" prefix="display" %>
</head>

<body leftmargin="20" topmargin="10">

	<%
	if(alRequestReport.size()>0) 
	{
	%>
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<display:table name="<%=tableName%>" requestURI="/gmETSesnModMap.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">
	            <display:column property="EDTID" title="Edit" class="alignleft" style="width:200px" />
				 <display:column property="MAPREASON" title="Reason" class="alignleft" style="width:150px" />
				 <display:column property="MAPDURATION" title="Duration" class="aligncenter" style="width:50px" />
				 <display:column property="MAPTRAINER" title="Trainer" class="alignleft" style="width:100px" />
				 <display:column property="MAPSTATUS" title="Status" class="alignleft" style="width:80px" />
				</display:table>				
			</td>
		</tr>
	</table>
	<%}else
		{
  		%>  		
		<tr class="Shade">
  		<td height="20">No Modules Found For Session</td><td></td>
  		</tr>
  		<tr><td colspan="2" class="LLine"></td></tr>
  		<%} %>	
</body>
</html>