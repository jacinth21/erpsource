<%
/**********************************************************************************
 * File		 		: GmEtrSesnDtlRpt.jsp
 * Desc		 		: For Session Detail Report
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>


<!-- etrain\GmEtrUserDtlRpt.jsp -->
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ include file="/common/GmHeader.inc" %>
<bean:define id="alRequest" name="frmEtrUserDtlRpt" property="alUserDtlRptList" type="java.util.ArrayList"></bean:define>
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("USER_RPT");
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: User Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrUserDtlRpt.js"></script>
<script>

</script>
<style type="text/css" media="all">
      @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmETUserDtlRpt.do">
<html:hidden property="strOpt" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VSESN"/>
<html:hidden property="strUserId" />
<html:hidden property="strSessionId" />
<html:hidden property="strTraineeName" />
<html:hidden property="trneeDeptName" />
<html:hidden property="trneeStartDate" />

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">User Detail Report</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 
					<tr height="24">
						<td class="RightTableCaption" align="right" HEIGHT="24" width="20%"></font>&nbsp;Trainee:</td>
						<td width="25%">&nbsp;
						<gmjsp:dropdown controlName="traineeId" SFFormName="frmEtrUserDtlRpt" SFSeletedValue="traineeId"
								SFValue="alTraineeIdList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="20%"></font>&nbsp;Module List:</td>
						<td width="20%">&nbsp;
						<gmjsp:dropdown controlName="moduleId" SFFormName="frmEtrUserDtlRpt" SFSeletedValue="moduleId"
								SFValue="alModuleList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="40%"></font>&nbsp;Program List:</td>	
						<td width="50%">&nbsp;
						<gmjsp:dropdown controlName="courseId" SFFormName="frmEtrUserDtlRpt" SFSeletedValue="courseId"
								SFValue="alCourseList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>					
					</tr>
					<tr><td colspan="6" class="LLine" height="1"></td></tr> 					 
                    <tr class="Shade" height="24">
                        <td class="RightTableCaption" align="right" HEIGHT="24" width="20%"></font>&nbsp;Start Date:</td>
                        <td width="25%">&nbsp;
                        <html:text property="startDt"  size="10" styleClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmEtrUserDtlRpt.startDt');" title="Click to open Calendar"  
						src="<%=strExtImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp; 
                        </td>
                        <td class="RightTableCaption" align="right" HEIGHT="24" width="20%"></font>&nbsp;End Date:</td>
                        <td width="20%">&nbsp;
                        <html:text property="endDt"  size="10" styleClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
						<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmEtrUserDtlRpt.endDt');" title="Click to open Calendar"  
						src="<%=strExtImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp; 
                        </td>
                        <td class="RightTableCaption" align="right" HEIGHT="24" width="50%"></font>&nbsp;Status:</td>
						<td width="50%">&nbsp;
						<gmjsp:dropdown controlName="status" SFFormName="frmEtrUserDtlRpt" SFSeletedValue="status"
								SFValue="alStatusList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
                    </tr>
                    <tr><td colspan="6" class="LLine" height="1"></td></tr>  
                    <tr height="24">
                    	<td class="RightTableCaption" align="right" width="20%"></font>&nbsp;Department:</td>
                    	<td width="25%">&nbsp;
							<gmjsp:dropdown controlName="userDeptId" SFFormName="frmEtrUserDtlRpt" SFSeletedValue="userDeptId"
								SFValue="alUserDeptIdList" codeId = "CODEID"  codeName = "CODENMALT"  defaultValue= "[Choose One]" />													
						</td>
						<td class="RightTableCaption" align="right" width="20%"></font>&nbsp;Reason</td>
                    	<td width="20%">&nbsp;																		
                    		<gmjsp:dropdown controlName="reason" SFFormName="frmEtrUserDtlRpt" SFSeletedValue="reason"
								SFValue="alReasonList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						</td>
						<td class="RightTableCaption" align="right" HEIGHT="24" width="50%"></td>
						<td width="50%">&nbsp;
							<input type="button" value="&nbsp;&nbsp;Load&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnLoad();" />
						</td>
                    </tr>
                    <%if (alRequest.size() > 0) 
					{
					%>                    
					<tr><td colspan="6" class="LLine" height="1"></td></tr> 	
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr> 
			            <td colspan="2">
				            <display:table name="requestScope.frmEtrUserDtlRpt.alUserDtlRptList" requestURI="/gmETUserDtlRpt.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">						
							<display:column property="RPTID" title="Trainee" class="alignleft" style="width:190px" sortable="true"/>
							<display:column property="MODULENM" title="Module Name" class="alignleft" sortable="true"/>
							<display:column property="TRAINER" title="Trainer" class="alignleft" sortable="true"/>
							<display:column property="COURSENM" title="Program Name" class="alignleft" sortable="true"/>
							<display:column property="STARTDT" title="Start Date" class="alignleft" style="width:100px" sortable="true" />
							<display:column property="ENDDT" title="End Date" class="alignleft" style="width:100px"  sortable="true" />
							<display:column property="STATUS" title="Status" class="alignleft" style="width:100px" sortable="true" />
							<display:column property="REASON" title="Reason" class="alignleft" style="width:100px" sortable="true" />
							</display:table> 						
						</td>
		    	   </tr>
		    	   </table>
		    	   <tr><td colspan="6" class="LLine" height="1"></td></tr>			    	     	   					          
   				</table>
  			   </td>
  		  </tr>	
  		   <%}else
			{%>  
		  		<tr><td colspan="6" class="LLine" height="1"></td></tr>							  					          
   				</table>
  			   </td>
  		  </tr>	
  		  <tr>
		  		<td height="20">No User Detail Found.</td>
		  </tr>
  		  <%} %>	
    </table>		     	
</html:form>
</BODY>

</HTML>

