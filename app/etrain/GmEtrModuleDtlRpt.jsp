<%
/**********************************************************************************
 * File		 		: GmEtrModuleDtlRpt.jsp
 * Desc		 		: For Module Detail Report
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>


<!-- etrain\GmEtrModuleDtlRpt.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel"%>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="alRequest" name="frmEtrModuleDtlRpt" property="alModuleDtlRptList" type="java.util.ArrayList"></bean:define>
<% 
String strWikiTitle = GmCommonClass.getWikiTitle("ETR_MODULEDTL_RPT");
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Module Report </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrModuleDtlRpt.js"></script>

<script>

</script>
<style type="text/css" media="all">
         @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmETModuleDtlRpt.do">
<html:hidden property="strOpt" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VMODU"/>
<html:hidden property="moduleId" />
<html:hidden property="moduleName" />
<html:hidden property="strSessionId" />
<html:hidden property="strUserId" />
<html:hidden property="userId" />
<html:hidden property="strOwner" />


	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Module Detail Report</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<td class="RightTableCaption" align="right" HEIGHT="25" width="20%"></font>&nbsp;Department ID:</td>
					<td width="25%">&nbsp;
									<gmjsp:dropdown controlName="moduleDeptId" SFFormName="frmEtrModuleDtlRpt" SFSeletedValue="moduleDeptId"
										SFValue="alModuleDeptList" codeId = "CODEID"  codeName = "CODENMALT"  defaultValue= "[Choose One]" />		                         
                        		</td>		
						<td class="RightTableCaption" align="right" HEIGHT="25" width="15%"></font>&nbsp;Owner:</td>
						<td width="20%">&nbsp;
						<gmjsp:dropdown controlName="moduleOwnerId" SFFormName="frmEtrModuleDtlRpt" SFSeletedValue="moduleOwnerId"
								SFValue="alModuleOwnerList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />&nbsp;&nbsp;&nbsp;													
						</td>
						<td width="10%">&nbsp;</td>
					</tr>
					<tr><td colspan="6" class="LLine" height="1"></td></tr> 					 
                    <tr class="Shade" >
                     <td class="RightTableCaption" align="right" height="25"></font>Program List:</td>  
                       <td >&nbsp;&nbsp;<gmjsp:dropdown controlName="courseId" SFFormName="frmEtrModuleDtlRpt" SFSeletedValue="courseId"
							SFValue="alCourseList" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" />   
                        </td>
                        <td class="RightTableCaption" align="right" HEIGHT="25" ></font>&nbsp;Status:</td>
						<td >&nbsp;
						<gmjsp:dropdown controlName="moduleStatusId" SFFormName="frmEtrModuleDtlRpt" SFSeletedValue="moduleStatusId"
								SFValue="alModuleStatusList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
						<td >
							<input type="button" value="&nbsp;&nbsp;GO&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnLoad();" />
						</td>
                    </tr>
                    <%if (alRequest.size() > 0) 
					{
					%>
					<tr><td colspan="6" class="LLine" height="1"></td></tr> 	
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr> 
			            <td colspan="2">
							<display:table name="requestScope.frmEtrModuleDtlRpt.alModuleDtlRptList" requestURI="/gmETModuleDtlRpt.do" excludedParams="haction" class="its" id="currentRowObject" decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">
							<display:column property="MODULEID" title="Module ID" class="alignleft" style="width:150px" />
							<display:column property="MODULENAME" title="Module Name" class="alignleft"/>
							<display:column property="OWNERNAME" title="Owner" class="alignleft" style="width:100px" />
							<display:column property="MODULEDEPTNAME" title="Department" class="alignleft" style="width:100px" />
							<display:column property="COURSENAME" title="Program Name" class="alignleft" style="width:100px" />
							<display:column property="MODULESTATUSNAME" title="Status" class="alignleft" style="width:100px" />						
							</display:table> 						
						</td>
		    	   </tr>
		    	   </table>
		    	   <tr><td colspan="6" class="LLine" height="1"></td></tr>
		    	   <tr class="Shade"> 
		    	   <tr>
						<td HEIGHT="30" >
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="center" width="5%"></font>&nbsp;</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="24" >
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="center" width="20%"></font>&nbsp;Select Action:</td>
								<td>&nbsp;
									<select class="RightText" name="Cbo_Opt">
										<option id="Action0" value="0">[Choose One]
							  			<option id="Action1" value="void">Void Action
										<option id="Action2" value="load">Edit Action																	
							  		</select>
								&nbsp;&nbsp;&nbsp;
	                         		<input type="button" value="Submit" class="button" onCLick="fnSubmit();" />&nbsp;&nbsp;	                         		                        
                        		</td> 
                        		<td>&nbsp;&nbsp;&nbsp;</td>	
								</tr>
							</table>
						</td>						
					</tr>
					
					<tr><td colspan="6" class="LLine" height="1"></td></tr>										          
   				</table>
  			   </td>
  		  </tr>	
  		  	<%}else
					{
				  		%>  
				  		<tr><td colspan="6" class="LLine" height="1"></td></tr>							  					          
   				</table>
  			   </td>
  		  </tr>	
  		  <tr>
				  		<td height="20">No Module Detail Found.</td>
				  		</tr>
  		  <%} %>	
  		  
    </table>		     	
</html:form>
</BODY>

</HTML>

