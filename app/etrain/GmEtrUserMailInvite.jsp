 <%
/**********************************************************************************
 * File		 		: GmEtrUserMailInvite.jsp
 * Desc		 		: This screen is used to mail the selected users.
 * Version	 		: 1.0 
 * author			: Ritesh Shah
************************************************************************************/
%>


<!-- etrain\GmEtrUserMailInvite.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page buffer="16kb" autoFlush="true" %>

<%
	String strType 	= GmCommonClass.parseNull(request.getParameter("hType")) ;
	String strID 	= GmCommonClass.parseNull(request.getParameter("hID"));
	String strMode = GmCommonClass.parseNull(request.getParameter("hMode"));
	String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
	
	String strHeaderFL = GmCommonClass.parseNull(request.getParameter("hHeader"));
	String strHeight = GmCommonClass.parseNull(request.getParameter("htxtHeight"));
	
	// Used to align submit function
	String strSkipRefID = GmCommonClass.parseNull(request.getParameter("hSkipRefID"));
	// Set default height
	strHeight = strHeight.equals("") ? "300":strHeight;	
%>
<html>

<head>
<title>Globus Medical: Mail Template</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache-Control" content="no-cache" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%= strEtrainJsPath%>/GmEtrUserMailInvite.js"></script>
<!-- tinyMCE -->
<script language="javascript" type="text/javascript" src="<%=strExtWebPath%>/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	// Notice: The simple theme does not use all options some of them are limited to the advanced theme
	tinyMCE.init({
		mode : "textareas",
		theme : "simple" ,
		height : "<%=strHeight%>",
		width: "100%" 
	});
</script>
<!-- /tinyMCE -->
</head>

<body leftmargin="0" topmargin="5" onUnload=fnUnload();>

<html:form action="/gmETUserMailInvite.do">
<html:hidden property="strOpt" />
<html:hidden property="hID" value="<%=strID%>" />
<html:hidden property="hType" value="<%=strType%>"/>
<html:hidden property="hMode" value="<%=strMode%>" />
<html:hidden property="hHeader" value="<%=strHeaderFL%>" />
<html:hidden property="htxtHeight" value="<%=strHeight%>" />
<html:hidden property="hSkipRefID" value="<%=strSkipRefID%>"/>
<html:hidden property="strInviteList" />

<table border="0" width="100%" cellspacing="0" cellpadding="0" style="">

	<tr>
		<td class="RightDashBoardHeader" height="20" > Mail Details</td></tr>
	<tr><td height="1" bgcolor="#666666"></td></tr>
	<tr><td  height="1"> </td></tr>
	<tr>		
		<td HEIGHT="25">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="RightTableCaption" align="right" width="10%"></font>&nbsp;To:</td>	
					<td>&nbsp;
	              		<html:text property="toMail"  size="130" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>	                         
	           		</td>															
				</tr>
			</table>
		</td>			
	</tr>
	<tr>		
		<td HEIGHT="25">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="RightTableCaption" align="right" width="10%"></font>&nbsp;Cc:</td>	
					<td>&nbsp;
	              		<html:text property="ccMail"  size="130" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" />
	              	</td>															
				</tr>
			</table>
		</td>			
	</tr>
	<tr>		
		<td HEIGHT="25">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="RightTableCaption" align="right" width="10%"></font>&nbsp;Subject:</td>	
					<td>&nbsp;
	              		<html:text property="subMail"  size="130" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>	                         
	           		</td>															
				</tr>
			</table>
		</td>			
	</tr>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="RightTableCaption" align="right" width="10%"></td>	
					<td align="left" width="90%">&nbsp;&nbsp; 
						<html:textarea property="textReason" styleClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"  	
						onBlur="changeBgColor(this,'#ffffff');" />						
					</td>															
				</tr>
			</table>
		</td>			
	</tr>	
	<tr><td  height="2"> </td></tr>
	<tr>
		<td  class="aligncenter" align="center">
			<input tabindex=3 type="button" value="Send" class="button" onClick="fnSubmit();">&nbsp;
	
			<input tabindex=4 type="button" value="Close" class="button" onClick="fnClose();"">&nbsp;
		</td>
	</tr>
</table>
</html:form>
</body>
</html>

