<%
/**********************************************************************************
 * File		 		: GmEtrCourseDeptMap.jsp
 * Desc		 		: Map the Department to correspoding course
 * Version	 		: 1.0
 * author			: Angela
************************************************************************************/
%>

<!-- etrain\GmEtrCourseDeptMap.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ taglib uri="/WEB-INF/struts-bean-el.tld" prefix="beanel"%>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@page import="com.globus.common.beans.GmResourceBundleBean"%>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="alRequest" name="frmEtrCourseDeptMap" property="alCourseDeptMapList" type="java.util.ArrayList"></bean:define>
<% 
String strWikiTitle = GmCommonClass.getWikiTitle("ETR_COURSE_DEPT_MAP"); 
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Program Department Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>Globus.css">
<script language="JavaScript" src="<%=strJsPath%>GlobusCommonScript.js"></script>
<script language="JavaScript" src=<%=strJsPath%>Message.js"></script>
<script language="JavaScript" src=<%=strJsPath%>Error.js"></script>
<script language="JavaScript" src=<%=strEtrainJsPath%>/GmEtrCourseDeptMap.js"></script>

<script>

</script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnVoidChk();">
<html:form action="/gmETCourseDeptMap.do">
<html:hidden property="haction" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDMAP"/>
<html:hidden property="strOpt" />
<html:hidden property="courseMapSeq" />
<html:hidden property="courseMapId" />
<html:hidden property="courseName" />


	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Program Department Mapping</td>			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>				
		</tr>
		<tr><td colspan="3" height="1" class="LLine"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">									
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25"></font>&nbsp;Program:</td>
						<td>&nbsp;&nbsp;<bean:write name="frmEtrCourseDeptMap" property="courseMapId"/>-<bean:write name="frmEtrCourseDeptMap" property="courseName"/></td>										
					</tr>					
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Department:</td> 
                        <td width="70%">&nbsp;
						<gmjsp:dropdown controlName="deptMapId" SFFormName="frmEtrCourseDeptMap" SFSeletedValue="deptMapId"
								SFValue="alDeptList" codeId = "CODEID"  codeName = "CODENMALT"  defaultValue= "[Choose One]" />														
						</td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>	
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">Is Mandatory?:</td> 
                        <td>&nbsp;<html:checkbox property="mandFlType" />                                                
                         </td>
                    </tr>				       
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
				  <tr>
					<td colspan="2" align="center" height="25">&nbsp;		
					<input type="button" name = "voidBtn" value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnVoid();" />							
					<input type="button" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();">						
					</td>
				  </tr>   
				  <tr><td colspan="2" class="LLine" height="1"></td></tr>	
				  <%if (alRequest.size() > 0) 
					{
					%>
				  <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr> 
                        <td colspan="2">
							<display:table name="requestScope.frmEtrCourseDeptMap.alCourseDeptMapList" requestURI="/gmETCourseDeptMap.do" excludedParams="haction" class="its" id="currentRowObject" decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">
							<display:column property="CSDEPTSEQ" title="Department" class="alignleft" style="width:190px" />							
							<display:column property="MANDFLTYPE" title="Mandatory" class="alignleft" style="width:100px" />					
							</display:table> 						
						</td>
		    	   </tr>
		    	   </table>  
   				</table>
  			   </td>
  		  </tr>	
  		   <%}else
					{%>  						  					          
  		 				<tr><td colspan="2" height="20">No Program Department Mapping Found for this Program.</td>
				  		</tr>
  		  <%} %>	               
						 
    </table>		     	
</html:form>
</BODY>

</HTML>

