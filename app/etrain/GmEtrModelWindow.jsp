<%
/**********************************************************************************
 * File		 		: GmEtrUserMatMap.jsp
 * Desc		 		: For User material mapping
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>


<!-- etrain\GmEtrModelWindow.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%
//String strFileNm = GmCommonClass.parseNull((String)request.getParameter("fileNm"));
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
String strFileId = GmCommonClass.parseNull((String)request.getParameter("fileId"));
String strSessionId = GmCommonClass.parseNull((String)request.getParameter("sessionId"));
String strModuleId = GmCommonClass.parseNull((String)request.getParameter("moduleId"));
String strUserId = GmCommonClass.parseNull((String)request.getParameter("userId"));
String strMatCmpltnDt = GmCommonClass.parseNull((String)request.getParameter("matCmpltnDt"));
String strSesnUserMatId = GmCommonClass.parseNull((String)request.getParameter("sesnUserMatId"));
//String strSesnOwner = GmCommonClass.parseNull((String)request.getParameter("sesnOwner"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Material Closing Agreement </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
   @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script>
	function fnOpenModelDialog()
	{
		document.frmEtrModelWindow.sessionId.value = "<%=strSessionId%>";	
		document.frmEtrModelWindow.fileId.value = "<%=strFileId%>";
		document.frmEtrModelWindow.moduleId.value = "<%=strModuleId%>";
		document.frmEtrModelWindow.userId.value = "<%=strUserId%>";
		document.frmEtrModelWindow.matCmpltnDt.value = "<%=strMatCmpltnDt%>";
		document.frmEtrModelWindow.sesnUserMatId.value = "<%=strSesnUserMatId%>";
		document.frmEtrModelWindow.submit();	
	}
function fnClose()
{
	var chkbox = document.frmEtrModelWindow.trcmpltn.checked;
	if(chkbox)
		document.frmEtrModelWindow.trcmpltn.value = "on";
	else
		document.frmEtrModelWindow.trcmpltn.value = "";
	document.frmEtrModelWindow.actionFlag.value = "true";	
	document.frmEtrModelWindow.submit();
	//For forcing a refresh without asking Retrieve/Cancle button
	//window.opener.history.go(0);
	window.opener.location=window.opener.location;
	window.close();
	
}

function keepFcsOn()
{
	window.focus(); 
	mytimer = setTimeout('keepFcsOn()', 10);
}	

function fnUnloadPopUp()
{
	if(document.frmEtrModelWindow.actionFlag.value != "true")
	{
		document.frmEtrModelWindow.submit();
		//For forcing a refresh without asking Retrieve/Cancle button
		//window.opener.history.go(0);
		window.opener.location=window.opener.location;
	}
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnOpenModelDialog();mytimer = setTimeout('keepFcsOn()', 500);" onUnload="fnUnloadPopUp();">
<html:form action="/gmETModelWindow.do">
<html:hidden property="sessionId" />
<html:hidden property="moduleId" />
<html:hidden property="userId" />
<html:hidden property="fileId" />
<html:hidden property="matCmpltnDt" />
<html:hidden property="sesnUserMatId" />
<html:hidden property="actionFlag" value=""/>
<html:hidden property="userMatTrackId" />

	<table border="0" height="10" cellspacing="0" cellpadding="0">
		<tr>
			<td width="500" height="10" valign="bottom" colspan="2">
				<table border="0" width="80%" cellspacing="0" cellpadding="0">
					<%--<tr><td colspan="2" valign="bottom">					 
						 <IFRAME src="/GmCommonFileOpenServlet?sId=<%=strFileId %>&uploadString=ETRUPLOADHOME" application=yes width="50%" height=1% marginwidth=0
							marginheight=0 frameborder=0>Iframes not supported</IFRAME> </td> 
					</tr> --%>
					<tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr>					
					<tr>
						<td colspan="2" valign="bottom">&nbsp;<input type=checkbox name="trcmpltn" />&nbsp;I Agree on Completion of Training
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="button" value="&nbsp;&nbsp;Close&nbsp;&nbsp;&nbsp;" class="button" onClick="fnClose();" /></td>			
					</tr>												          
   				</table>
  			   </td>
  		  </tr>	
    </table>
   </html:form>		     	
</BODY>

</HTML>

