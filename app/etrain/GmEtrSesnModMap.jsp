<%
/**********************************************************************************
 * File		 		: GmEtrSesnModMap.jsp
 * Desc		 		: Map the modules to correspoding Session
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>

<!-- etrain\GmEtrSesnModMap.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page import="org.apache.commons.beanutils.BeanMap, org.apache.commons.beanutils.RowSetDynaClass, org.apache.commons.beanutils.BasicDynaBean"%>
<%@ page import="java.util.ArrayList,java.util.Iterator" %>
<bean:define id="alRequest" name="frmEtrSesnModMap" property="sessModMapDT" type="java.util.ArrayList"></bean:define>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("SESN_MOD_MAP");
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Session Module Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrSesnModMap.js"></script>
<style type="text/css" media="all">
      @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnVoidChk();">
<html:form action="/gmETSesnModMap.do">
<html:hidden property="haction" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VSMAP"/>
<html:hidden property="strOpt" />
<html:hidden property="sessModSeq"/>
<html:hidden property="sessionId"/>
<html:hidden property="alLength" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Session Module Mapping</td>			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>				
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" class="LLine" height="1"></td></tr>					
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25"></font>&nbsp;Session ID:</td>
						<td>&nbsp;&nbsp;<bean:write name="frmEtrSesnModMap" property="sessionId"/></td>										
					</tr>					
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Module List:</td> 
                        <td width="70%">&nbsp;
						<gmjsp:dropdown controlName="moduleId" SFFormName="frmEtrSesnModMap" SFSeletedValue="moduleId"
								SFValue="alModuleList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/>
						<input type="button" value="&nbsp;&nbsp;ModLookup&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnLookup();" />															
						</td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>					
                     <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Reason:</td> 
                        <td width="70%">&nbsp;
						<gmjsp:dropdown controlName="reason" SFFormName="frmEtrSesnModMap" SFSeletedValue="reason"
								SFValue="alReasonList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Trainer:</td> 
                        <td width="70%">&nbsp;
						<gmjsp:dropdown controlName="trainerId" SFFormName="frmEtrSesnModMap" SFSeletedValue="trainerId"
								SFValue="alTrainerList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
                        <td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Details:</td> 
                        <td>&nbsp;
		                    <html:text property="details" maxlength="4000" size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>                                        
                        </td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">
					    <td class="RightTableCaption" align="right" HEIGHT="25"><font color="red">*</font>&nbsp;Duration:</td> 
                        <td>&nbsp;
		                    <html:text property="durationHrs"  size="10" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>&nbsp;(Hrs)                                        
                        </td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25" width="30%"><font color="red">*</font>&nbsp;Status:</td>
						<td width="70%">&nbsp;
						<gmjsp:dropdown controlName="statusMap" SFFormName="frmEtrSesnModMap" SFSeletedValue="statusMap"
								SFValue="alStatusList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>						
					</tr>  
					  <%if (alRequest.size() > 0) 
					{
					%>                                 
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                   
					<tr class="Shade"> 
		            <td colspan="2">
		            	<jsp:include page="/etrain/GmIncludeModules.jsp" >
						<jsp:param name="FORMNAME" value="frmEtrSesnModMap" />
						<jsp:param name="LISTNAME" value="sessModMapDT" />
						</jsp:include>				            						
					</td>
		    	  </tr>	
		    	  <%}else
			{%>  
		  		<tr><td colspan="2" class="LLine" height="1"></td></tr>							  					          
  		  <tr>
		  		<td colspan="2" height="20">No Session Module Mapping Found for this Session.</td>
		  </tr>
		  <tr><td colspan="2" class="LLine" height="1"></td></tr>	
  		  <%} %>	    	  
				  <tr>
					<td colspan="2" align="center" HEIGHT="25">&nbsp;		
					<input type="button" name = "voidBtn" value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnVoid();" />							
					<input type="button" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();">						
					</td>
				  </tr>   
				  <tr><td colspan="2" class="LLine" height="1"></td></tr>	                 				 
   	</table>
  			   </td>
  		  </tr>	
  		  
    </table>		     	
</html:form>
</BODY>

</HTML>

