<%
/**********************************************************************************
 * File		 		: GmEtrModuleSetup.jsp
 * Desc		 		: For Module Detail Setup
 * Version	 		: 1.0
 * author			: angela
************************************************************************************/
%>


<!-- etrain\GmEtrModuleSetup.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<bean:define id="modDesc" name="frmEtrModuleSetup" property="description" type="java.lang.String"></bean:define>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("MODULE_DETAIL_SETUP");	

String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Course Detail Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath %>/GmEtrModuleSetup.js"></script>

<style type="text/css" media="all">
    @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad=fnChngStatus();>
<html:form action="/gmETModuleSetup.do">
<html:hidden property="strOpt" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VMODU"/>
<html:hidden property="userId" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr >
			<td height="25" class="RightDashBoardHeader">Module Setup</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="LLine"></td></tr>
		<tr >
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">				
				 	<tr>
                        <td class="RightTableCaption" align="right" height="25"></font>&nbsp;Module List:</td>
                       <td>&nbsp;
                       <gmjsp:dropdown controlName="moduleId" SFFormName="frmEtrModuleSetup" SFSeletedValue="moduleId"
							SFValue="alModuleList" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" onChange="fnLoad(this.value)"/>    &nbsp;&nbsp;
						<input type="button" value="&nbsp;&nbsp;Load&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnLoad(document.frmEtrModuleSetup.moduleId.value);" />	 
                        </td>
                        
                    </tr> 
					<tr><td colspan="4" class="LLine"></td></tr>
                    <tr class="Shade">
						<td class="RightTableCaption" align="right" height="25"></font>&nbsp;Module ID:</td>
						<td>&nbsp;  <bean:write name="frmEtrModuleSetup" property="moduleId"/></td>										
					</tr>
					  <tr><td colspan="4" class="LLine"></td></tr> 
					<tr>
						<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;Module Name:</td>
						<td>&nbsp;
						<html:text property="moduleName" size="50" maxlength="100" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>   
						</td>										
					</tr>
					<tr><td colspan="4" class="LLine"></td></tr>	
					<tr class="Shade">
                        <td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;Module Description:</td> 
                        <td>&nbsp;
		                    <html:textarea property="description" cols="50" rows ="3"></html:textarea>                                        
                        </td>
                    </tr>  
                    <tr><td colspan="4" class="LLine"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" height="25"><font color="red">*</font>Program List:</td>
                  		<td>&nbsp;
                  			<gmjsp:dropdown controlName="courseId" SFFormName="frmEtrModuleSetup" SFSeletedValue="courseId"
								SFValue="alCourseList" codeId = "CODEID"  codeName = "CODENM" defaultValue= "[Choose One]" /> 
                        </td>  
                    </tr>					
					<tr><td colspan="4" class="LLine"></td></tr> 	
					<tr class="Shade">
						<td class="RightTableCaption" align="right" height="25"><font color="red">*</font>&nbsp;Module Owner:</td>
						<td>&nbsp;
						<gmjsp:dropdown controlName="moduleOwner" SFFormName="frmEtrModuleSetup" SFSeletedValue="moduleOwner"
								SFValue="alModuleOwnerList" codeId="CODEID" codeName="CODENM"  defaultValue= "[Choose One]" />	
						</td>										
					</tr>
                    <tr><td colspan="4" class="LLine"></td></tr> 	                  
                    <tr>                    
						<td class="RightTableCaption" align="right" height="25" width="30%"><font color="red">*</font>Module Status:</td>
						<td width="70%">&nbsp;
						<gmjsp:dropdown controlName="moduleStatus" SFFormName="frmEtrModuleSetup" SFSeletedValue="moduleStatus"
								SFValue="alStatusList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>						
					</tr>
                    <tr><td colspan="2" class="LLine"></td></tr> 
                    	<tr>
						<td colspan="2" align="center" HEIGHT="25">&nbsp;
	                    	<jsp:include page="/common/GmIncludeLog.jsp" >
		                    	<jsp:param name="FORMNAME" value="frmEtrModuleSetup" />
								<jsp:param name="ALNAME" value="alLogReasons" />
								<jsp:param name="LogMode" value="Edit" />																					
							</jsp:include>	
						</td>
					</tr>
					<tr><td colspan="2" class="LLine"></td></tr>								
					<tr>
						<td colspan="2" align="center" HEIGHT="25">&nbsp;							
						<input type="button" id="voidModule" value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnVoid();" />
						<input type="button" value="&nbsp;&nbsp;Reset&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnReset();" >						
						<input type="button" id="submitModule" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();" />	
						</td>
					</tr>
					<%					
					if(!GmCommonClass.parseNull(modDesc).equals(""))
					{						
					%>
					 <tr><td colspan="2" class="LLine"></td></tr> 
					<tr>
					<td height="25" class="RightDashBoardHeader">Materials Mapped:-</td><td align="right" class=RightDashBoardHeader ></td>	
					</tr>
					 <tr><td colspan="2" class="LLine"></td></tr> 
					<tr>
						<td colspan="2">
							<iframe src="/GmEtrFileUploadServlet?strId=<bean:write name="frmEtrModuleSetup" property="moduleId"/>" width="100%" height="300x" frameborder="0" scrolling="yes"></iframe>
						</td>
					</tr>
					<%				    
					}
					%>
   				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
</BODY>

</HTML>

