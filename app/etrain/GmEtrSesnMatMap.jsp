<%
/**********************************************************************************
 * File		 		: GmEtrSesnMatMap.jsp
 * Desc		 		: For Session Material Mapping
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>

<!-- etrain\GmEtrSesnMatMap.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<bean:define id="alRequest" name="frmEtrSesnMatMap" property="alSesnMatList" type="java.util.ArrayList"></bean:define>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("SESN_MAT_MAP"); 
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Session Material </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
          @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrSesnMatMap.js"></script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="fnVoidChk();">
<html:form action="/gmETSesnMatMap.do">
<html:hidden property="strOpt" />
<html:hidden property="seqNo" />
<html:hidden property="moduleId" />
<html:hidden property="sessionId" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Session Material</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Session ID:</td>
						<td>&nbsp;&nbsp;<bean:write name="frmEtrSesnMatMap" property="sessionId"/></td>										
					</tr>					
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr class="Shade">
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%">&nbsp;Module ID:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="20%">&nbsp;&nbsp;<bean:write name="frmEtrSesnMatMap" property="moduleId"/> 
									</td>								
									<td class="RightTableCaption" align="right" width="50%">&nbsp;Module Name:</td>
									<td width= "55%">&nbsp;
			                    		<bean:write name="frmEtrSesnMatMap" property="moduleName"/>                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>                     
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Trainer:</td>
						<td>&nbsp;&nbsp;<bean:write name="frmEtrSesnMatMap" property="trainer"/></td>										
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%">&nbsp;File Name:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="40%">&nbsp;
										<bean:write name="frmEtrSesnMatMap" property="fileNm"/>										
									</td>								
									<td class="RightTableCaption" align="right" width="30%">&nbsp;File Type:</td>
									<td width= "55%">&nbsp;
										<bean:write name="frmEtrSesnMatMap" property="fileType"/>			                    		                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">Is Required?:</td> 
                        <td>&nbsp;<html:checkbox property="mandatory" />                                                
                         </td>
                    </tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<%if (alRequest.size() > 0) 
					{
					%>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>					
					<tr class="Shade"> 
			            <td colspan="2">
				            <display:table name="requestScope.frmEtrSesnMatMap.alSesnMatList" requestURI="/gmETSesnMatMap.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">
				            <display:column property="SEQ" title="Edit" class="aligncenter" style="width:90px" /> 						
							<display:column property="FILENM" title="File Name" class="alignleft" style="width:70px" sortable="true"/> 
							<display:column property="FILETYPE" title="File Type" class="alignleft" sortable="true"/> 
							<display:column property="MATOWNER" title="Owner" class="alignleft" style="width:70px" sortable="true" />
							<display:column property="CRDATE" title="Created Date" class="alignleft" style="width:120px" sortable="true" />
							<display:column property="MANDATORY" title="Mandatory" class="aligncenter" style="width:100px"   />							
							</display:table> 						
						</td>
		    	   </tr>
		    	   <tr><td colspan="2" class="LLine" height="1"></td></tr>		    	   
					<tr>   
						<td colspan="2" align="center" HEIGHT="25">&nbsp;
						<input type="button" name = "voidBtn" value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnVoid();" />						
						<input type="button" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();" />						
						</td>
					</tr>
					<%} else
					{
			  		%>  		
					<tr>
			  		<td height="20">No Session Material Found</td>
			  		</tr>
			  		<%} %>					          
   				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
</BODY>

</HTML>

