<%
/**********************************************************************************
 * File		 		: GmEtrUserDashboard.jsp
 * Desc		 		: List 0f My Trainings
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>

<!-- etrain\GmEtrUserDashboard.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%@ page import="org.apache.commons.beanutils.BeanMap, org.apache.commons.beanutils.RowSetDynaClass, org.apache.commons.beanutils.BasicDynaBean"%>
<%@ page import="java.util.ArrayList,java.util.Iterator" %>
<bean:define id="alRequest" name="frmEtrUserDashboard" property="alOpenTrainings" type="java.util.ArrayList"></bean:define>
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("USER_DASHBOARD");
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: My Open Trainings </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrUserDashboard.js"></script>
<style type="text/css" media="all">
   @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmETUserDashboard.do">

<html:hidden property="strOpt" />
<html:hidden property="strSessionId" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">My Open Trainings</td>			
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>			
		</tr>
		<%if (alRequest.size() > 0) 
		{
		%>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td colspan="2" class="LLine"></td></tr>
		            <tr> 
			            <td colspan="2">
				            <display:table name="requestScope.frmEtrUserDashboard.alOpenTrainings" requestURI="/gmETUserDashboard.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">				
							 <display:column property="ID" title="Pending" class="alignleft" style="width:100px" sortable="true" />  
							 <display:column property="SESSIONDESC" title="Session Desctiption" class="aligncenter" style="width:100px" sortable="true" />
							 <display:column property="TRAINER" title="Trainer" class="aligncenter" style="width:100px" sortable="true" />
							 <display:column property="STARTDATE" title="Start Date" class="aligncenter" style="width:100px" sortable="true"/>
							 <display:column property="ENDDATE" title="End Date" class="aligncenter" style="width:100px" sortable="true" />							 
							</display:table> 						
						</td>           
					 </tr>	 
   				</table>
  			</td>
  		</tr>
  		<%}
		else
		{
  		%>  		
		<tr>
  		<td height="20">No Open Trainings</td>
  		</tr>
  		<%} %>	
    </table>		     	
</html:form>
</BODY>

</HTML>

