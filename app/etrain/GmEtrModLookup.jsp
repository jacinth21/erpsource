<%
/**********************************************************************************
 * File		 		: GmEtrModLookup.jsp
 * Desc		 		: List 0f Modules
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>


<!-- etrain\GmEtrModLookup.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page import="org.apache.commons.beanutils.BeanMap, org.apache.commons.beanutils.RowSetDynaClass, org.apache.commons.beanutils.BasicDynaBean"%>
<%@ page import="java.util.ArrayList,java.util.Iterator" %>
<bean:define id="alRequest" name="frmModLookup" property="alModLookupList" type="java.util.ArrayList"></bean:define>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% 
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
	String strWikiTitle = GmCommonClass.getWikiTitle("MOD_LOOKUP");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Module Lookup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%= strEtrainJsPath%>/GmEtrModLookup.js"></script>
<style type="text/css" media="all">
        @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmETModLookup.do">

<html:hidden property="strOpt" />
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Module Lookup</td>
			
			<td align="right" class=RightDashBoardHeader > 	
				 
				</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				    <tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%"></font>&nbsp;Module ID:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="0%">&nbsp;
										<html:text property="moduleId" size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 
									</td>								
									<td class="RightTableCaption" align="right" width="40%"></font>&nbsp;Module Name:</td>
									<td width= "55%">&nbsp;
			                    		<html:text property="moduleNm" size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>					
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="25"></font>&nbsp;Module Owner:</td> 
                        <td width="70%">&nbsp;
						<gmjsp:dropdown ControlName="moduleOwner" SFFormName="frmModLookup" SFSeletedValue="moduleOwner"
								SFValue="alModuleOwnerList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />													
						</td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>					
                    <tr>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%"></font>&nbsp;Course/Program:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="0%">&nbsp;
										<gmjsp:dropdown controlName="courseId" SFFormName="frmModLookup" SFSeletedValue="courseId"
											SFValue="alCourseList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /> 
									</td>								
									<td class="RightTableCaption" align="right" width="40%"></font>&nbsp;</td>
									<td width= "55%">&nbsp;&nbsp;&nbsp;
			                    		<input type="button" value="&nbsp;&nbsp;Load&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnLoad();">                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>
					  <%if (alRequest.size() > 0) 
					{
					%>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>					
                    <tr class="Shade">                    
                    	<td colspan="2">
			            	<display:table name="requestScope.frmModLookup.alModLookupList" requestURI="/gmETModLookup.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">				
							 	<display:column property="ID" title="Select" class="alignleft" style="width:100px" />  
							 	<display:column property="MODULENM" title="Module Name" class="alignleft" style="width:100px" />
							 	<display:column property="COURSEID" title="Course ID" class="alignleft" style="width:100px" />
							 	<display:column property="COURSENM" title="Course Name" class="alignleft" style="width:100px" />
							 	<display:column property="MODULEOWNER" title="Owner" class="alignleft" style="width:100px" />
							</display:table> 						
						</td>	    	                     
					<tr><td colspan="2" class="LLine" height="1"></td></tr>	 
	   			  </table>
  			   </td>
  		 	 </tr>	
  		   <%}else
			{%>  
		  		<tr><td colspan="6" class="LLine" height="1"></td></tr>							  					          
   				</table>
  			   </td>
  		  </tr>	
  		  <tr>
		  		<td height="20">No Module Found.</td>
		  </tr>
  		  <%} %>	
    </table>		     	
</html:form>
</BODY>

</HTML>

