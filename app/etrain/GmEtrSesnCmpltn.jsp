<%
/**********************************************************************************
 * File		 		: GmEtrSesnCmpltn.jsp
 * Desc		 		: For Session Completion
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>

<!-- etrain\GmEtrSesnCmpltn.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<bean:define id="alRequest" name="frmEtrSesnCmpltn" property="alUsrDtlMapList" type="java.util.ArrayList"></bean:define>
<bean:define id="sesnLcn" name="frmEtrSesnCmpltn" property="sessionLcn" type="java.lang.String"></bean:define>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("SESN_CMPLTN");
	String strSessionId = (String)request.getParameter("strSessionId");
	String href = "/gmETUserMatMap.do?sessionId="+strSessionId;

	String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Session Complition </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrSesnCmpltn.js"></script>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmETSesnCmpltn.do">
<html:hidden property="strOpt" />
<html:hidden property="sessionId" />
<html:hidden property="hinputString" />
<html:hidden property="strSesnOwner" />
<html:hidden property="strTempDate" />
<html:hidden property="sessionStartDt" /> <%--for date validation only --%>
<html:hidden property="userId" />


	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">Session Completion</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Session ID:</td>
						<td>&nbsp;&nbsp;<bean:write name="frmEtrSesnCmpltn" property="sessionId"/></td>										
					</tr>					
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Session Description:</td> 
                        <td>&nbsp;&nbsp;<bean:write name="frmEtrSesnCmpltn" property="sessionDesc"/></td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr>
                    <tr>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%">&nbsp;Start Date:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="20%">&nbsp;&nbsp;<bean:write name="frmEtrSesnCmpltn" property="sessionStartDt"/> 
									</td>								
									<td class="RightTableCaption" align="right" width="50%">&nbsp;End Date:</td>
									<td width= "55%">&nbsp;
			                    		<bean:write name="frmEtrSesnCmpltn" property="sessionEndDt"/>                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>                     
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%">&nbsp;Location Type:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="20%">&nbsp;
										<bean:write name="frmEtrSesnCmpltn" property="sessionLcnTypeNm"/>										
									</td>								
									<td class="RightTableCaption" align="right" width="50%">&nbsp;Location:</td>
									<% 										
									if(!sesnLcn.equals("") && sesnLcn.substring(0,5).matches("http:"))
										{												
									%>																	
									<td width= "55%">&nbsp;	
										<a href=<bean:write name="frmEtrSesnCmpltn" property="sessionLcn"/> target="_blank"><bean:write name="frmEtrSesnCmpltn" property="sessionLcn"/></a>
									</td>
									<%
										} 
										else
										{												
									%>	 
									<td width= "55%">&nbsp;	
										<bean:write name="frmEtrSesnCmpltn" property="sessionLcn"/>
									</td>                       		
									<%} %>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25" width="30%">&nbsp;Owner:</td>
						<td width="70%">&nbsp;
						<bean:write name="frmEtrSesnCmpltn" property="sessionOwnerNm"/>													
						</td>						
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr class="Shade">
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%">&nbsp;Status:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="30%">&nbsp;
										<bean:write name="frmEtrSesnCmpltn" property="sesnUsrMapStatus"/>										
									</td>								
									<td class="RightTableCaption" align="right" width="40%">&nbsp;&nbsp;Session Completion Date:</td>
									<td width= "55%">&nbsp;
										<bean:write name="frmEtrSesnCmpltn" property="cmpltnDt"/>			                    		                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
					<tr>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="RightTableCaption" align="right" HEIGHT="25" width="30%">&nbsp;Feedback:</td>
								<td width="100%">&nbsp;<html:textarea property="feedback"  maxlength="255" size="70" styleClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"  	
								onBlur="changeBgColor(this,'#ffffff');" /></td>
							</tr>
						</table>		
					</tr>
					<%if (alRequest.size() > 0) 
					{
					%>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>					
					<tr class="Shade"> 
			            <td colspan="2">
				            <display:table name="requestScope.frmEtrSesnCmpltn.alUsrDtlMapList" requestURI="/gmETSesnCmpltn.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">
				            <display:column property="USRMODNM" title="Trainee" class="alignleft" style="width:90px" sortable="true"/> 						
							<display:column property="MODID" title="Module ID" class="alignleft" style="width:70px" sortable="true"/> 
							<display:column property="PENDING" title="Pending" class="alignleft" style="width:70px" />  
							<display:column property="MODULENM" title="Module Name" class="alignleft" sortable="true"/> 
							<display:column property="ENDDATE" title="End Date" class="alignleft" style="width:70px" sortable="true" />
							<display:column property="TRAINERID" title="Trainer" class="alignleft" style="width:100px" sortable="true" />
							<display:column property="ISREQUIRED" title="Mandatory" class="aligncenter" style="width:100px"  sortable="true" />
							<display:column property="DATE" title="Completion Date <input type='text' name='applyAllDt'size='9'/> <img id='Img_Date'  style='cursor:hand' onclick=javascript:show_calendar('frmEtrSesnCmpltn.applyAllDt'); src='<%=strExtImagePath%>/nav_calendar.gif' border='0' align='absmiddle' height='18' width='19' /> <input type='checkbox' name='selectAll' onclick='fnSelectAll();' />" class="alignleft" style="width:130px" sortable="true" />
							</display:table> 						
						</td>
		    	   </tr>
		    	   <tr><td colspan="2" class="LLine" height="1"></td></tr>		    	   
					<tr>   
						<td colspan="2" align="center" HEIGHT="25">&nbsp;	
						<input type="button" value="&nbsp;Manage User&nbsp;&nbsp;" class="button" onCLick="fnManageUser();" />&nbsp;					
						<input type="button" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();" />						
						</td>
					</tr>
					<%} else
					{
			  		%> 
			  		<tr><td colspan="2" class="LLine" height="1"></td></tr> 		
					<tr class="Shade">
			  		<td height="20">No Training Module Found</td><td></td>
			  		</tr>
			  		<%} %>					          
   				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
</BODY>

</HTML>

