<%
/**********************************************************************************
 * File		 		: GmEtrSesnUserInvite.jsp
 * Desc		 		: For inviting user for session
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>

<!-- etrain\GmEtrUserModMap.jsp -->

<%@ include file="/common/GmHeader.inc" %> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<bean:define id="alRequest" name="frmEtrUserModMap" property="alUsrDtlMapList" type="java.util.ArrayList"></bean:define>
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("USER_MOD_MAP");
String strEtrainJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ETRAIN");
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: User Module Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strEtrainJsPath%>/GmEtrUserModMap.js"></script>


<style type="text/css" media="all">
 @import url("<%=strCssPath%>/screen.css");
</style>

</HEAD>
<script>
function fnBtnChk()
{
	var	strUserSeq=document.frmEtrUserModMap.userSeq.value;
	document.frmEtrUserModMap.voidBtn.disabled = true;
	document.frmEtrUserModMap.subBtn.disabled = true;
	if (strUserSeq != '')
	{
		document.frmEtrUserModMap.voidBtn.disabled = false;
		document.frmEtrUserModMap.subBtn.disabled = false;
	}

}
</script>


<BODY leftmargin="20" topmargin="10" onLoad="fnBtnChk();">
<html:form action="/gmETUserModMap.do">
<html:hidden property="strOpt" />
<html:hidden property="sessionId" />
<html:hidden property="sesnUserId" />
<html:hidden property="sesnUserNm" />
<html:hidden property="userSeq" />
<html:hidden property="modStatus" />
<html:hidden property="moduleNm" />

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">User Module Mapping</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strExtImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
			
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="848" height="100" valign="top" colspan="2">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr>
                        <td class="RightTableCaption" align="right" HEIGHT="25"></font>&nbsp;Session Id:</td> 
                        <td>&nbsp;<bean:write name="frmEtrUserModMap" property="sessionId"/></td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr class="Shade">
                    	<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%"></font>&nbsp;Start Date:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="20%">&nbsp;<bean:write name="frmEtrUserModMap" property="sessionStartDt"/> 
									</td>								
									<td class="RightTableCaption" align="right" width="40%"></font>&nbsp;End Date:</td>
									<td width= "55%">&nbsp;
			                    		<bean:write name="frmEtrUserModMap" property="sessionEndDt"/>                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%"></font>&nbsp;User ID:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="20%">&nbsp;<bean:write name="frmEtrUserModMap" property="sesnUserId"/> 
									</td>								
									<td class="RightTableCaption" align="right" width="40%"></font>&nbsp;User Name:</td>
									<td width= "55%">&nbsp;
			                    		<bean:write name="frmEtrUserModMap" property="sesnUserNm"/>                                        
	                        		</td>
								</tr>
							</table>
						</td>
					</tr>                    
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="25"></font>&nbsp;Module Name:</td> 
                       <td>&nbsp;<bean:write name="frmEtrUserModMap" property="moduleNm"/></td>
                    </tr>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td class="RightTableCaption" align="right" width="100%"></font>&nbsp;Reason:</td>																
								</tr>
							</table>
						</td>
						<td HEIGHT="25">
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
								<td width="20%">&nbsp;
									<gmjsp:dropdown controlName="reason" SFFormName="frmEtrUserModMap" SFSeletedValue="reason"
										SFValue="alReasonList" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />		                         
                        		</td>								
								<td class="RightTableCaption" align="right" width="35%"></font>&nbsp;Status:</td>	
								<td width= "55%">&nbsp;
	                         		<bean:write name="frmEtrUserModMap" property="modStatus"/> 	                         
                        		</td> 
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24">Is Mandatory?:</td> 
                        <td>&nbsp;<html:checkbox property="isRequired" />                                                
                         </td>
                    </tr>
                    <%if (alRequest.size() > 0) 
					{
					%>
                    <tr><td colspan="2" class="LLine" height="1"></td></tr> 					
					<tr> 
			            <td colspan="2">
				            <display:table name="requestScope.frmEtrUserModMap.alUsrDtlMapList" requestURI="/gmETUserModMap.do" excludedParams="haction" class="its" id="currentRowObject"  decorator="com.globus.etrain.displaytag.beans.DTEtrWrapper">						
							<display:column property="USERSEQ" title="Edit" class="alignleft" style="width:180px" /> 
							<display:column property="MODULENM" title="Module Name" class="alignleft" style="width:110px"/> 
							<display:column property="REASON" title="Reason" class="alignleft" style="width:70px" />
							<display:column property="STATUS" title="Status" class="alignleft" style="width:70px" />
							<display:column property="COMPLTNDT" title="Completion Date" class="alignleft" style="width:45px" />
							<display:column property="ISREQUIRED" title="Mandatory" class="alignleft" style="width:45px" />
							</display:table> 						
						</td>
		    	   </tr> 				
				   <tr><td colspan="2" class="LLine" height="1"></td></tr> 					
				   <tr>
						<td colspan="2" align="center" HEIGHT="25">&nbsp;
							<%-- <input type="button" name="voidBtn" value="&nbsp;&nbsp;Void&nbsp;&nbsp;&nbsp;" class="button"  onCLick="fnVoid();"/>--%>
							<input type="button" name="subBtn" value="&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnSubmit();"/>
							<input type="button" name="backBtn" value="&nbsp;&nbsp;Back&nbsp;&nbsp;&nbsp;" class="button" onCLick="fnBack();"/>						
						</td>
					</tr>
					<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<%} else
				{
		  		%>  
		  		<tr><td colspan="2" class="LLine" height="1"></td></tr>		
				<tr>
		  		<td height="20">No Module Mapping Found For User</td>
		  		</tr>
		  		<%} %>							     
   				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
</BODY>

</HTML>

