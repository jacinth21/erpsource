
<%
	/***********************************************************************************************
	 * File		 		: GmManufCostBulkUpdate.jsp
	 * Desc		 		: This screen is used for the setting the Cost to the particular part No's
	 * Version	 		: 1.0
	 * author			: 
	 ************************************************************************************************/
%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<%@ page import="com.globus.manufact.forms.GmManufCostForm"%>
<%@ page import="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtGmManufactPartCostEdit"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false"%>
<!-- manufact\GmManufCostBulkUpdate.jsp-->
<fmtGmManufactPartCostEdit:setLocale value="<%=strLocale%>" />
<fmtGmManufactPartCostEdit:setBundle
	basename="properties.labels.manufact.GmManufactPartCostEdit" />
<%
	String strManuFactJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_MANUFACT");
    String strWikiTitle = GmCommonClass.getWikiTitle("PART_MANUF_COST_BATCH");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Manufacturing Cost Update</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strManuFactJsPath%>/GmManufCostBulkUpdate.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
</HEAD>
<BODY leftmargin="20" topmargin="10">
	<html:form action="/gmManufCost.do?method=manufBulkUpdate">
		<html:hidden property="hInputStr" value="" />
		<html:hidden property="hPartInputStr" value="" />
		<html:hidden property="strOpt" value="" />

		<table class="DtTable900" cellspacing="0" cellpadding="0">
		     
		     <tr><td colspan="4"></td></tr>   
			<tr>
				<td height="25" class="RightDashBoardHeader" colspan="3">&nbsp;<fmtGmManufactPartCostEdit:message
						key="LBL_MANUFACT_COST_BATCH_UPDATE" /></td>
				<td class="RightDashBoardHeader"><fmtGmManufactPartCostEdit:message
						key="IMG_HELP" var="varHelp" /><img align="right" id='imgEdit'
					style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}'
					width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
			</tr>
			<tr>
				<td colspan="4" class="LLine"></td>
			</tr>
			   <tr><td height="10"></td></tr>
		       <tr>
				<td colspan="4" class="RightTextBlue" align="center"><fmtGmManufactPartCostEdit:message
							key="LBL_ENTER_THE_DATA" /></td>
			</tr>
			 <tr><td height="10"></td></tr>
			<tr>
				<td class="RightTableCaption" align="right"><font color="red">*</font>
					<fmtGmManufactPartCostEdit:message
						key="LBL_ENTER_DATA_FOR_BATCH_UPLOAD" />:</td>
				<td valign="absmiddle">&nbsp;
				<textarea name="batchData"  id="batchData" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
		          onBlur="changeBgColor(this,'#ffffff');"  rows=25 cols=40 ></textarea>
				</td>
			</tr>
			<tr><td colspan="4" height="35"></td></tr>
            <tr>
				<td colspan="4" class="LLine"></td>
			</tr>
			<tr>
				<td height="30" width="1200" align="center" colspan="2"><fmtGmManufactPartCostEdit:message
						key="BTN_SUBMIT" var="varSubmit" /> <gmjsp:button
						value="&nbsp;${varSubmit}&nbsp;" style="width: 6em"
						name="Btn_Submit" buttonType="Save" gmClass="button"
						onClick="fnBulkUpdate();" />&nbsp; <fmtGmManufactPartCostEdit:message
						key="BTN_RESET" var="varReset" /> <gmjsp:button
						value="&nbsp;${varReset}&nbsp;" style="width: 6em"
						name="Btn_Reset" gmClass="button" buttonType="Save"
						onClick="fnReset();" /></td>
			</tr>
			<tr>
				<td colspan="4" class="LLine"></td>
			</tr>
		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>