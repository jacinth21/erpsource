<%@page import="java.util.Date"%>
<%
  /**********************************************************************************
			 * File		 		: GmManfHome.jsp
			 * Desc		 		: This screen is used for the Manufacturing Dashboard 
			 * Version	 		: 1.0
			 * author			: Richard K
			 ************************************************************************************/
%>


<!-- manufact\GmManfHome.jsp -->


<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<!-- Imports for Logger -->
<%@ page import="com.globus.common.beans.GmLogger"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%>
<%@ page import="org.apache.log4j.Logger"%>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%
String strManuFactJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_MANUFACT");
  request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			try {
				GmServlet gm = new GmServlet();
				gm.checkSession(response, session);
				String strApplDateFmt = strGCompDateFmt;

				HashMap hmReturn = (HashMap) request.getAttribute("hmReturn");

				ArrayList alSupplyDash = new ArrayList();
				alSupplyDash = (ArrayList) hmReturn.get("SUPPLYDASH");

				ArrayList alManfDash = new ArrayList();
				alManfDash = (ArrayList) hmReturn.get("DHRDASH");

				ArrayList alWorkOrdDash = new ArrayList();
				alWorkOrdDash = (ArrayList) hmReturn.get("PENDWODASH");

				ArrayList alItemConsign = new ArrayList();
				alItemConsign = (ArrayList) hmReturn.get("TRANSDASH");

				int intSupplyDashLength = 0;
				int intManfDashLength = 0;
				int intWorkDashLength = 0;
				int intItemLength = 0;

				if (alSupplyDash != null) {
					intSupplyDashLength = alSupplyDash.size();
				}

				if (alManfDash != null) {
					intManfDashLength = alManfDash.size();
				}

				if (alWorkOrdDash != null) {
					intWorkDashLength = alWorkOrdDash.size();
				}

				if (alItemConsign != null) {
					intItemLength = alItemConsign.size();
				}

				HashMap hmLoop = new HashMap();

				String strShade = "";
				String strVendorName = "";
				String strDHRId = "";
				String strPartNum = "";
				String strDesc = "";
				String strFlag = "";
				String strQCAcceptFl = "";
				String strInspIniFl = "";
				String strInspCompFl = "";
				String strVeriFl = "";
				String strControlFl = "";
				String strInspFl = "";
				String strPackFl = "";
				String strPendProcFl = "";
				String strLine = "";
				String strRAId = "";
				String strReturnType = "";
				String strReturnReason = "";
				String strExpectedDate = "";
				String strNCMRId = "";
				String strCreatedDate = "";
				String strCreditDate = "";
				String strReprocessId = "";
				String strLastUpdatedDt = "";

				HashMap hmTempLoop = new HashMap();
				String strNextId = "";
				int intCount = 0;
				boolean blFlag = false;
				String strIniFlag = "";
				String strWIPFlag = "";
				String strTemp = "";
				boolean blFlag1 = false;
				int intDispCount = 0;
				int intInspCount = 0;
				int intPackCount = 0;
				int intVeriCount = 0;
				int intProcCount = 0;
				int intConCount = 0;
				int intCreditCount = 0;
				int intMatCount = 0;

				String strWorkOrdId = "";
				String strQtyOrd = "";
				String strQtyPend = "";
				String strMatStatusFl = "";
				StringBuffer sbTemp = new StringBuffer();
				String strApplnDateFmt = strGCompDateFmt;
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Manufacturing Dashboard</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strManuFactJsPath%>/GmManfHome.js"></script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10">
	<FORM name="frmAccount" method="POST"
		action="<%=strServletPath%>/GmPOReceiveServlet">
		<input type="hidden" name="hId" value=""> <input type="hidden"
			name="hMode" value=""> <input type="hidden" name="hFrom"
			value="Dashboard"> <input type="hidden" name="hType" value="">
		<input type="hidden" name="hAction" value=""> <input
			type="hidden" name="hPortal" value=""> <input type="hidden"
			name="hConsignId" value=""> <input type="hidden" name="hOpt"
			value="">

		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<table border="0" width="798" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="10" height="25" class="RightDashBoardHeader">
								Dashboard - Supply Receipts</td>
						</tr>
						<tr>
							<td class="Line" colspan="10"></td>
						</tr>
						<tr class="ShadeRightTableCaption">
							<td width="60">&nbsp;Vendor Name</td>
							<td width="100">&nbsp;Trans ID</td>
							<td HEIGHT="24" align="center" width="60">Part Number</td>
							<td width="300">Description</td>
							<td width="90" align="center">Created Date</td>
							<td width="90" align="center">Pending<BR>QC Accept
							</td>
							<td width="90" align="center">Pending Inspect <BR>Initiation
							</td>
							<td width="90" align="center">Pending Inspect <BR>Completion
							</td>
							<td width="90" align="center">Pending Processing</td>
							<td width="90" align="center">Pending Verification</td>
						</tr>
						<%
						  if (intSupplyDashLength > 0) {
						      HashMap hmDHRLoop = new HashMap();
						      HashMap hmTempLoop1 = new HashMap();
						      String strNextId1 = "";
						      int intCount1 = 0;
						      String strVendorId = "";
						      boolean blFlag2 = false;
						      boolean blFlag3 = false;
						      int intDisplayCount = 0;
						      sbTemp.setLength(0);

						      for (int j = 0; j < intSupplyDashLength; j++) {
						        hmDHRLoop = (HashMap) alSupplyDash.get(j);

						        if (j < intSupplyDashLength - 1) {
						          hmTempLoop1 = (HashMap) alSupplyDash.get(j + 1);
						          strNextId1 = GmCommonClass.parseNull((String) hmTempLoop1.get("VID"));
						        }

						        strVendorId = GmCommonClass.parseNull((String) hmDHRLoop.get("VID"));
						        strVendorName = GmCommonClass.parseNull((String) hmDHRLoop.get("VNAME"));
						        strDHRId = GmCommonClass.parseNull((String) hmDHRLoop.get("ID"));
						        strPartNum = GmCommonClass.parseNull((String) hmDHRLoop.get("PARTNUM"));
						        strDesc =
						            GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String) hmDHRLoop
						                .get("PDESC")));
						        strFlag = GmCommonClass.parseNull((String) hmDHRLoop.get("FL"));
						        //strCreatedDate = GmCommonClass.parseNull((String)hmDHRLoop.get("CDATE"));
						        strCreatedDate =
						            GmCommonClass.getStringFromDate((java.util.Date) hmDHRLoop.get("CDATE"),
						                strApplnDateFmt);
						        //strLastUpdatedDt = GmCommonClass.parseNull((String)hmDHRLoop.get("LDATE"));
						        strLastUpdatedDt =
						            GmCommonClass.getStringFromDate((java.util.Date) hmDHRLoop.get("LDATE"),
						                strApplnDateFmt);

						        if (strFlag.equals("0") && strLastUpdatedDt.equals("")) {
						          strQCAcceptFl =
						              "<a class=RightTextRed href= javascript:fnUpdateSupplyReceipts('" + strDHRId
						                  + "','Q');>Y</a>";
						          strInspIniFl = "-";
						          strInspCompFl = "-";
						          strPendProcFl = "-";
						          strVeriFl = "-";
						          intConCount++;
						        } else if (strFlag.equals("0") && !strLastUpdatedDt.equals("")) {
						          strQCAcceptFl = "-";
						          strInspIniFl =
						              "<a class=RightTextRed href= javascript:fnUpdateSupplyReceipts('" + strDHRId
						                  + "','II');>Y</a>";
						          strInspCompFl = "-";
						          strPendProcFl = "-";
						          strVeriFl = "-";
						          intInspCount++;
						        } else if (strFlag.equals("1")) {
						          strQCAcceptFl = "-";
						          strInspIniFl = "-";
						          strInspCompFl =
						              "<a class=RightTextRed href= javascript:fnUpdateSupplyReceipts('" + strDHRId
						                  + "','IC');>Y</a>";
						          strPendProcFl = "-";
						          strVeriFl = "-";
						          intPackCount++;
						        } else if (strFlag.equals("2")) {
						          strQCAcceptFl = "-";
						          strInspIniFl = "-";
						          strInspCompFl = "-";
						          strPendProcFl =
						              "<a class=RightTextRed href= javascript:fnUpdateSupplyReceipts('" + strDHRId
						                  + "','P');>Y</a>";
						          strVeriFl = "-";
						          intProcCount++;
						        } else if (strFlag.equals("3")) {
						          strQCAcceptFl = "-";
						          strInspIniFl = "-";
						          strInspCompFl = "-";
						          strPendProcFl = "-";
						          strVeriFl =
						              "<a class=RightTextRed href= javascript:fnUpdateSupplyReceipts('" + strDHRId
						                  + "','V');>Y</a>";
						          intVeriCount++;
						        }

						        if (strVendorId.equals(strNextId1)) {
						          intCount1++;
						          strLine = "<TR><TD colspan=10 height=1 class=Line></TD></TR>";
						        } else {
						          sbTemp.append("document.all.td" + strVendorId + "Con.innerHTML = '<b>" + intConCount
						              + "</b>';");
						          sbTemp.append("document.all.td" + strVendorId + "Insp.innerHTML = '<b>"
						              + intInspCount + "</b>';");
						          sbTemp.append("document.all.td" + strVendorId + "Pack.innerHTML = '<b>"
						              + intPackCount + "</b>';");
						          sbTemp.append("document.all.td" + strVendorId + "Proc.innerHTML = '<b>"
						              + intProcCount + "</b>';");
						          sbTemp.append("document.all.td" + strVendorId + "Veri.innerHTML = '<b>"
						              + intVeriCount + "</b>';");
						          intConCount = 0;
						          intDispCount = 0;
						          intInspCount = 0;
						          intPackCount = 0;
						          intProcCount = 0;
						          intVeriCount = 0;
						          blFlag3 = true;
						          strLine = "<TR><TD colspan=10 height=1 class=Line></TD></TR>";
						          if (intCount1 > 0) {
						            strVendorName = "";
						            strLine = "";
						          } else {
						            blFlag2 = true;
						          }
						          intCount1 = 0;
						          //
						        }
						        if (intCount1 > 1) {
						          strVendorName = "";
						          strLine = "";
						        }

						        out.print(strLine);
						        strShade = (j % 2 != 0) ? "class=ShadeBlue" : ""; //For alternate Shading of rows
						        if (intCount1 == 1 || blFlag2) {
						%>
						<tr id="tr<%=strVendorId%>">
							<td colspan="5" height="18" class="RightText">&nbsp;<A
								class="RightText" title="Click to Expand"
								href="javascript:Toggle('<%=strVendorId%>')"><%=strVendorName%></a></td>
							<td align="center" id="td<%=strVendorId%>Con" class="RightText">&nbsp;</td>
							<td align="center" id="td<%=strVendorId%>Insp" class="RightText">&nbsp;</td>
							<td align="center" height="18" id="td<%=strVendorId%>Pack"
								class="RightText">&nbsp;</td>
							<td align="center" height="18" id="td<%=strVendorId%>Proc"
								class="RightText">&nbsp;</td>
							<td align="center" height="18" id="td<%=strVendorId%>Veri"
								class="RightText">&nbsp;</td>

						</tr>
						<tr>
							<td colspan="10"><div style="display: none"
									id="div<%=strVendorId%>">
									<table width="100%" cellpadding="0" cellspacing="0" border="0"
										id="tab<%=strVendorId%>">
										<%
										  blFlag2 = false;
										        }
										%>
										<tr <%=strShade%>>
											<td width="20" HEIGHT="20" class="RightText">&nbsp;</td>
											<td width="100" HEIGHT="20" class="RightText">&nbsp;<%=strDHRId%></td>
											<td width="60" class="RightText">&nbsp;<%=strPartNum%></td>
											<td width="200" class="RightText">&nbsp;<%=strDesc%></td>
											<td width="90" class="RightText">&nbsp;<%=strCreatedDate%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strQCAcceptFl%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strInspIniFl%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strInspCompFl%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strPendProcFl%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strVeriFl%></td>
										</tr>
										<tr>
											<td colspan="10" bgcolor="#cccccc" height="1"></td>
										</tr>
										<%
										  if (blFlag3 || j + 1 == intSupplyDashLength) {
										          if (j + 1 == intSupplyDashLength) {
										            //sbTemp.append("document.all.td"+strVendorId+".innerHTML = '<b>"+intDisplayCount+"</b>';");
										            sbTemp.append("document.all.td" + strVendorId + "Con.innerHTML = '<b>"
										                + intConCount + "</b>';");
										            sbTemp.append("document.all.td" + strVendorId + "Insp.innerHTML = '<b>"
										                + intInspCount + "</b>';");
										            sbTemp.append("document.all.td" + strVendorId + "Pack.innerHTML = '<b>"
										                + intPackCount + "</b>';");
										            sbTemp.append("document.all.td" + strVendorId + "Proc.innerHTML = '<b>"
										                + intProcCount + "</b>';");
										            sbTemp.append("document.all.td" + strVendorId + "Veri.innerHTML = '<b>"
										                + intVeriCount + "</b>';");
										          }
										%>
									</table>
								</div></td>
						</tr>
						<%
						  }
						        blFlag3 = false;
						      }
						      out.print("<script>");
						      out.print(sbTemp.toString());
						      out.print("</script>");
						    } else {
						%>
						<tr>
							<td height="30" colspan="10" align="center" class="RightTextBlue">There
								are no Supply Receipts pending at this moment !</td>
						</tr>
						<%
						  }
						%>
					</table>
				</td>
			</tr>
		</table>

		<BR> <BR>

		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<table border="0" width="798" cellspacing="0" cellpadding="0"
						border="0">
						<tr>
							<td colspan="9" height="25" class="RightDashBoardHeader">Dashboard
								- Manufacturing DHRs</td>
						</tr>
						<tr>
							<td class="Line" colspan="9"></td>
						</tr>
						<tr class="ShadeRightTableCaption">
							<td HEIGHT="24" align="center" width="60">Part Number</td>
							<td width="200">Description</td>
							<td width="100">&nbsp;Lot Number</td>
							<td width="90" align="center">Created Date</td>
							<td width="90" align="center">Pending<BR>Supply Release
							</td>
							<td width="90" align="center">Pending<BR>Supply Recon.
							</td>
							<td width="90" align="center">Pending QC Inspect</td>
							<!--<td width="90" align="center">Pending Labeling</td>-->
							<!--<td width="90" align="center">Inventory Transfer</td>-->
							<td width="90" align="center">Pending Processing</td>
							<td width="90" align="center">Pending Verification</td>
						</tr>
						<%
						  intConCount = 0;
						    intDispCount = 0;
						    intInspCount = 0;
						    intPackCount = 0;
						    intVeriCount = 0;
						    if (intManfDashLength > 0) {
						      HashMap hmDHRLoop = new HashMap();
						      HashMap hmTempLoop1 = new HashMap();
						      String strNextId1 = "";
						      int intCount1 = 0;
						      String strConNum = "";
						      String strPartNumId = "";
						      boolean blFlag2 = false;
						      boolean blFlag3 = false;
						      int intDisplayCount = 0;
						      sbTemp.setLength(0);

						      for (int j = 0; j < intManfDashLength; j++) {
						        hmDHRLoop = (HashMap) alManfDash.get(j);

						        if (j < intManfDashLength - 1) {
						          hmTempLoop1 = (HashMap) alManfDash.get(j + 1);
						          strNextId1 = GmCommonClass.parseNull((String) hmTempLoop1.get("PARTNUM"));
						        }
						        strDHRId = GmCommonClass.parseNull((String) hmDHRLoop.get("ID"));
						        strPartNum = GmCommonClass.parseNull((String) hmDHRLoop.get("PARTNUM"));
						        strPartNumId = strPartNum.replaceAll("\\.", "");
						        strDesc =
						            GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String) hmDHRLoop
						                .get("PDESC")));
						        strFlag = GmCommonClass.parseNull((String) hmDHRLoop.get("FL"));
						        //strCreatedDate = GmCommonClass.parseNull((String)hmDHRLoop.get("CDATE"));
						        strCreatedDate =
						            GmCommonClass.getStringFromDate((java.util.Date) hmDHRLoop.get("CDATE"),
						                strApplnDateFmt);
						        strConNum = GmCommonClass.parseNull((String) hmDHRLoop.get("CNUM"));
						        strMatStatusFl = GmCommonClass.parseNull((String) hmDHRLoop.get("MATSFL"));

						        if (strFlag.equals("0") && !strMatStatusFl.equals("40")) //40 released
						        {
						          strMatStatusFl = "<a class=RightTextRed>Y</a>";
						          strControlFl = "-";
						          strInspFl = "-";
						          strPackFl = "-";
						          strVeriFl = "-";
						          intMatCount++;
						        } else if (strFlag.equals("0") && strMatStatusFl.equals("40")) {
						          strMatStatusFl = "-";
						          strControlFl = "<a class=RightTextRed>Y</a>";
						          strInspFl = "-";
						          strPackFl = "-";
						          strVeriFl = "-";
						          intConCount++;
						        } else if (strFlag.equals("1")) {
						          strMatStatusFl = "-";
						          strControlFl = "-";
						          strInspFl =
						              "<a class=RightTextRed href= javascript:fnUpdateDHR('" + strDHRId
						                  + "','I');>Y</a>";
						          strPackFl = "-";
						          strVeriFl = "-";
						          intInspCount++;
						        } else if (strFlag.equals("2")) {
						          strMatStatusFl = "-";
						          strControlFl = "-";
						          strInspFl = "-";
						          strPackFl =
						              "<a class=RightTextRed href= javascript:fnUpdateDHR('" + strDHRId
						                  + "','P');>Y</a>";
						          strVeriFl = "-";
						          intPackCount++;
						        } else if (strFlag.equals("3")) {
						          strMatStatusFl = "-";
						          strControlFl = "-";
						          strInspFl = "-";
						          strPackFl = "-";
						          strVeriFl =
						              "<a class=RightTextRed href= javascript:fnUpdateDHR('" + strDHRId
						                  + "','V');>Y</a>";
						          intVeriCount++;
						        }
						        if (strPartNum.equals(strNextId1)) {
						          intCount1++;
						          strLine = "<TR><TD colspan=9 height=1 class=Line></TD></TR>";
						        } else {
						          sbTemp.append("document.getElementById('td" + strPartNumId + "Mat').innerHTML = '<b>" + intMatCount
						              + "</b>';");
						          sbTemp.append("document.getElementById('td" + strPartNumId + "Con').innerHTML = '<b>" + intConCount
						              + "</b>';");
						          sbTemp.append("document.getElementById('td" + strPartNumId + "Insp').innerHTML = '<b>"
						              + intInspCount + "</b>';");
						          sbTemp.append("document.getElementById('td" + strPartNumId + "Pack').innerHTML = '<b>"
						              + intPackCount + "</b>';");
						          sbTemp.append("document.getElementById('td" + strPartNumId + "Veri').innerHTML = '<b>"
						              + intVeriCount + "</b>';");
						          intMatCount = 0;
						          intConCount = 0;
						          intDispCount = 0;
						          intInspCount = 0;
						          intPackCount = 0;
						          intVeriCount = 0;

						          blFlag3 = true;
						          strLine = "<TR><TD colspan=9 height=1 class=Line></TD></TR>";
						          if (intCount1 > 0) {
						            strPartNum = "";
						            strDesc = "";
						            strLine = "";
						          } else {
						            blFlag2 = true;
						          }
						          intCount1 = 0;
						          //
						        }
						        if (intCount1 > 1) {
						          strPartNum = "";
						          strDesc = "";
						          strLine = "";
						        }

						        out.print(strLine);
						        strShade = (j % 2 != 0) ? "class=ShadeBlue" : ""; //For alternate Shading of rows
						        if (intCount1 == 1 || blFlag2) {
						%>
						<tr id="tr<%=strPartNumId%>">
							<td colspan="4" height="18" class="RightText">&nbsp;<A
								class="RightText" title="Click to Expand"
								href="javascript:Toggle('<%=strPartNumId%>')"><%=strPartNum%>&nbsp;&nbsp;&nbsp;<%=strDesc%></a></td>
							<td align="center" height="18" id="td<%=strPartNumId%>Mat"
								class="RightText">&nbsp;</td>
							<td align="center" id="td<%=strPartNumId%>Con" class="RightText">&nbsp;</td>
							<td align="center" id="td<%=strPartNumId%>Insp" class="RightText">&nbsp;</td>
							<td align="center" height="18" id="td<%=strPartNumId%>Pack"
								class="RightText">&nbsp;</td>
							<td align="center" height="18" id="td<%=strPartNumId%>Veri"
								class="RightText">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="9"><div style="display: none"
									id="div<%=strPartNumId%>">
									<table width="100%" cellpadding="0" cellspacing="0" border="0"
										id="tab<%=strPartNumId%>">
										<%
										  blFlag2 = false;

										        }
										%>


										<tr <%=strShade%>>
											<td width="200" HEIGHT="20" class="RightText">&nbsp;</td>
											<td width="100" HEIGHT="20" class="RightText">&nbsp;<%=strConNum%></td>
											<td width="90" class="RightText">&nbsp;<%=strCreatedDate%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strMatStatusFl%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strControlFl%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strInspFl%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strPackFl%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strVeriFl%></td>
										</tr>
										<tr>
											<td colspan="9" bgcolor="#cccccc" height="1"></td>
										</tr>
										<%
										  if (blFlag3 || j + 1 == intManfDashLength) {
										          if (j + 1 == intManfDashLength) {
										            //sbTemp.append("document.all.td"+strPartNumId+".innerHTML = '<b>"+intDisplayCount+"</b>';");
										            sbTemp.append("document.getElementById('td" + strPartNumId + "Mat').innerHTML = '<b>"
										                + intMatCount + "</b>';");
										            sbTemp.append("document.getElementById('td" + strPartNumId + "Con').innerHTML = '<b>"
										                + intConCount + "</b>';");
										            sbTemp.append("document.getElementById('td" + strPartNumId + "Insp').innerHTML = '<b>"
										                + intInspCount + "</b>';");
										            sbTemp.append("document.getElementById('td" + strPartNumId + "Pack').innerHTML = '<b>"
										                + intPackCount + "</b>';");
										            sbTemp.append("document.getElementById('td" + strPartNumId + "Veri').innerHTML = '<b>"
										                + intVeriCount + "</b>';");
										          }
										%>
									</table>
								</div></td>
						</tr>
						<%
						  }
						        blFlag3 = false;
						      }
						      out.print("<script>");
						      out.print(sbTemp.toString());
						      out.print("</script>");
						    } else {
						%>
						<tr>
							<td height="30" colspan="9" align="center" class="RightTextBlue">There
								are no DHR's pending at this moment !</td>
						</tr>
						<%
						  }
						%>
					</table>
				</td>
			</tr>
		</table>

		<BR> <BR>

		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<table border="0" width="798" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="7" height="25" class="RightDashBoardHeader">
								Dashboard - Pending Work Orders</td>
						</tr>
						<tr>
							<td class="Line" colspan="7"></td>
						</tr>
						<tr class="ShadeRightTableCaption">
							<td HEIGHT="24" align="center" width="60">Part Number</td>
							<td width="200">Description</td>
							<td width="100">&nbsp;Work<BR> Order
							</td>
							<td width="90" align="center">Created<BR> Date
							</td>
							<td width="90" align="center">Qty<BR> Ordered
							</td>
							<td width="90" align="center">Qty<BR> Pending
							</td>
							<td width="90" align="center">Status</td>
						</tr>
						<%
						  if (intWorkDashLength > 0) {
						      HashMap hmDHRLoop = new HashMap();
						      HashMap hmTempLoop1 = new HashMap();
						      String strNextId1 = "";
						      int intCount1 = 0;
						      String strConNum = "";
						      String strPartNumId = "";
						      boolean blFlag2 = false;
						      boolean blFlag3 = false;
						      int intDisplayCount = 0;
						      sbTemp.setLength(0);

						      for (int j = 0; j < intWorkDashLength; j++) {
						        hmDHRLoop = (HashMap) alWorkOrdDash.get(j);

						        if (j < intWorkDashLength - 1) {
						          hmTempLoop1 = (HashMap) alWorkOrdDash.get(j + 1);
						          strNextId1 = GmCommonClass.parseNull((String) hmTempLoop1.get("PNUM"));
						        }
						        strWorkOrdId = GmCommonClass.parseNull((String) hmDHRLoop.get("WOID"));
						        strPartNum = GmCommonClass.parseNull((String) hmDHRLoop.get("PNUM"));
						        strPartNumId = strPartNum.replaceAll("\\.", "");
						        strDesc =
						            GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String) hmDHRLoop
						                .get("PDESC")));
						        strFlag = GmCommonClass.parseNull((String) hmDHRLoop.get("SFL"));
						        strCreatedDate =
						            GmCommonClass.getStringFromDate((Date) hmDHRLoop.get("CDATE"), strApplDateFmt);
						        strQtyOrd = GmCommonClass.parseZero((String) hmDHRLoop.get("QTYORD"));
						        strQtyPend = GmCommonClass.parseZero((String) hmDHRLoop.get("QTYPEND"));

						        if (strPartNum.equals(strNextId1)) {
						          intCount1++;
						          strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
						        } else {
						          blFlag3 = true;
						          strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
						          if (intCount1 > 0) {
						            strPartNum = "";
						            strDesc = "";
						            strLine = "";
						          } else {
						            blFlag2 = true;
						          }
						          intCount1 = 0;
						        }
						        if (intCount1 > 1) {
						          strPartNum = "";
						          strDesc = "";
						          strLine = "";
						        }

						        out.print(strLine);
						        strShade = (j % 2 != 0) ? "class=ShadeBlue" : ""; //For alternate Shading of rows
						        if (intCount1 == 1 || blFlag2) {
						%>
						<tr id="trWO<%=strPartNumId%>">
							<td colspan="4" height="18" class="RightText">&nbsp;<A
								class="RightText" title="Click to Expand"
								href="javascript:Toggle('WO<%=strPartNumId%>')"><%=strPartNum%>&nbsp;&nbsp;&nbsp;<%=strDesc%></a></td>
							<td align="center" class="RightText">&nbsp;</td>
							<td align="center" class="RightText">&nbsp;</td>
							<td align="center" height="18" class="RightText">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="7"><div style="display: none"
									id="divWO<%=strPartNumId%>">
									<table width="100%" cellpadding="0" cellspacing="0" border="0"
										id="tab<%=strPartNumId%>">
										<%
										  blFlag2 = false;
										        }
										%>


										<tr <%=strShade%>>
											<td width="200" HEIGHT="20" class="RightText">&nbsp;</td>
											<td width="100" HEIGHT="20" class="RightText">&nbsp;<%=strWorkOrdId%></td>
											<td width="90" class="RightText">&nbsp;<%=strCreatedDate%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strQtyOrd%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strQtyPend%></td>
											<td width="90" class="RightText" align="center">&nbsp;<%=strFlag%></td>
										</tr>
										<tr>
											<td colspan="7" bgcolor="#cccccc" height="1"></td>
										</tr>
										<%
										  if (blFlag3 || j + 1 == intWorkDashLength) {
										%>
									</table>
								</div></td>
						</tr>
						<%
						  }
						        blFlag3 = false;
						      }
						    } else {
						%>
						<tr>
							<td height="30" colspan="7" align="center" class="RightTextBlue">There
								are no Work Orders pending at this moment !</td>
						</tr>
						<%
						  }
						%>
					</table>
				</td>
			</tr>
		</table>
		<BR> <BR>

		<table border="0" class="DtTable800" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="7" height="25" class="RightDashBoardHeader">Dashboard
					- Consigned Items</td>
			</tr>
			<tr>
				<td class="Line" height="1" colspan="7"></td>
			</tr>
			<tr class="ShadeRightTableCaption">
				<td HEIGHT="24" width="30">Type</td>
				<td width="90">&nbsp;Consign. Id</td>
				<td width="90">&nbsp;Ref. Id</td>
				<td width="100">&nbsp;Req./Ini. By</td>
				<td width="60" align="center">&nbsp;Pending &nbsp;Control</td>
				<td width="60" align="center">&nbsp;Pending &nbsp;Shipping/<BR>Verification
				</td>
				<td width="350">&nbsp;&nbsp;Comments</td>
			</tr>

			<%
			  String strRedTextShipNoLink = "";
			    String strRedTextShip = "";
			    String strRedTextControl = "";
			    String strTemp1 = "";
			    String strTemp2 = "";
			    String strShipFl = "";
			    String strCntrlFl = "";
			    String strConsignType = "";
			    String strUserName = "";
			    String strConId = "";
			    String strSetName = "";
			    String strComments = "";
			    String strConType = "";

			    if (intItemLength > 0) {
			      hmTempLoop = new HashMap();
			      strNextId = "";
			      intCount = 0;
			      blFlag = false;
			      strTemp = "";
			      blFlag1 = false;

			      for (int i = 0; i < intItemLength; i++) {
			        hmLoop = (HashMap) alItemConsign.get(i);

			        if (i < intItemLength - 1) {
			          hmTempLoop = (HashMap) alItemConsign.get(i + 1);
			          strNextId = GmCommonClass.parseNull((String) hmTempLoop.get("CTYPE"));
			        }

			        strConId = GmCommonClass.parseNull((String) hmLoop.get("CID"));
			        strSetName = GmCommonClass.parseNull((String) hmLoop.get("TNAME"));
			        strComments = GmCommonClass.parseNull((String) hmLoop.get("COMMENTS"));
			        strConsignType = GmCommonClass.parseNull((String) hmLoop.get("CTYPE"));
			        strUserName = GmCommonClass.parseNull((String) hmLoop.get("UNAME"));
			        strReprocessId = GmCommonClass.parseNull((String) hmLoop.get("REFID"));
			        strConType = GmCommonClass.parseNull((String) hmLoop.get("CON_TYPE"));

			        strRedTextShip =
			            "<a class=RightTextRed href= javascript:fnCallItemsShip('" + strConId + "','"
			                + strConsignType + "','" + strConType + "');>Y</a>";
			        strRedTextShipNoLink = "<a class=RightTextRed>Y</a>";
			        strRedTextControl =
			            "<a class=RightTextRed href= javascript:fnCallEditItems('" + strConId + "','"
			                + strConsignType + "','" + strConType + "');>Y</a>";
			        strTemp1 = (String) hmLoop.get("SHIPREQFL");
			        strTemp2 = (String) hmLoop.get("CONTROLFL");

			        strShipFl = strTemp1.equals("1") ? strRedTextShip : "--";
			        if (strTemp1.equals("1")) {
			          strShipFl = strTemp2.equals("Y") ? strRedTextShipNoLink : strRedTextShip;
			        } else {
			          strShipFl = "--";
			        }
			        strCntrlFl = strTemp2.equals("Y") ? strRedTextControl : "--";

			        if (strConsignType.equals(strNextId)) {
			          intCount++;
			          strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
			        } else {
			          blFlag1 = true;
			          strLine = "<TR><TD colspan=7 height=1 class=Line></TD></TR>";
			          if (intCount > 0) {
			            strSetName = "";
			            strLine = "";
			          } else {
			            blFlag = true;
			          }
			          intCount = 0;
			          //
			        }

			        if (intCount > 1) {
			          strSetName = "";
			          strLine = "";
			        }

			        out.print(strLine);
			        strShade = (i % 2 != 0) ? "class=ShadeBlue" : ""; //For alternate Shading of rows
			        if (intCount == 1 || blFlag) {
			%>
			<tr id="tr<%=strConsignType%>">
				<td colspan="7" height="18" class="RightText">&nbsp;<A
					class="RightText" title="Click to Expand"
					href="javascript:Toggle('<%=strConsignType%>')"><%=strSetName%></a></td>
			</tr>
			<tr>
				<td colspan="7"><div style="display: none"
						id="div<%=strConsignType%>">
						<table width="100%" cellpadding="0" cellspacing="0" border="0"
							id="tab<%=strConsignType%>">
							<%
							  blFlag = false;
							        }
							%>
							<tr <%=strShade%>>
								<td width="30" class="RightText" align="center" height="20">&nbsp;<a
									href="javascript:fnPicSlip('<%=strConId%>','<%=strConsignType%>');"><img
										src="<%=strImagePath%>/packslip.gif" border=0></a></td>
								<td width="90" class="RightText" height="20">&nbsp;<%=strConId%></td>
								<td width="90" class="RightText" height="20">&nbsp;<%=strReprocessId%></td>
								<td width="100" class="RightText" height="20">&nbsp;<%=strUserName%></td>
								<td width="60" class="RightText" align="center">&nbsp;<%=strCntrlFl%></td>
								<td width="60" class="RightText" align="center">&nbsp;<%=strShipFl%></td>
								<td width="350" class="RightText"><%=strComments%></td>
							</tr>
							<TR>
								<TD colspan=7 height=1 bgcolor=#cccccc></TD>
							</TR>
							<%
							  if (blFlag1 || i + 1 == intItemLength) {
							%>
						</table>
					</div></td>
			</tr>
			<%
			  }
			        blFlag1 = false;
			      }
			    } else {
			%>
			<tr>
				<td height="30" colspan="7" align="center" class="RightTextBlue">There
					are no initiated Items pending action at this moment !</td>
			</tr>
			<%
			  }
			%>
		</table>

	</FORM>
	<%
	  } catch (Exception e) {
	    e.printStackTrace();
	  }
	%>
</BODY>
<%@ include file="/common/GmFooter.inc"%>
</HTML>
