



<%
	/**********************************************************************************
	 * File		 		: GmManufCostRpt.jsp
	 * Desc		 		: This screen is used for the Manufacturing Cost Reports
	 * author			: Mihir Patel
	 ***********************************************************************************
	 */
%>

<!-- manufact\GmManufCostRpt.jsp -->


<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="gridData" name="frmManufCost" property="gridData" type="java.lang.String"> </bean:define>
<%
String strManuFactJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_MANUFACT");
	String strWikiTitle = GmCommonClass.getWikiTitle("MANUF_COST_RPT");
	String strApplDateFmt = GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
%>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script language="JavaScript" src="<%=strManuFactJsPath%>/GmManufCostRpt.js"></script>

<HTML>
<HEAD>
<TITLE>Globus Medical: Manufacturing Cost Report</TITLE>

</HEAD>
<script>
    var objGridData;
	objGridData = '<%=gridData%>';

</script>
<BODY leftmargin="20" topmargin="10" onkeyup="fnEnter();" onload="fnOnPageLoad();">
	<html:form action="/gmManufCost.do?method=fetchAccountActivityRpt">
		<html:hidden property="strOpt" value="" />
		<table border="0" class="DtTable850" width="850" cellspacing="0" cellpadding="0" 
		style="border-width: 1px ; border-style: solid;border-color: #000000">
			<tr>
				<td colspan="7" height="25" class="RightDashBoardHeader">Manufacturing Cost Reports</td>
				<td align="right" class="RightDashBoardHeader"><img
					id='imgEdit' align="right" style='cursor: hand'
					src='<%=strImagePath%>/help.gif' title='Help' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr>
				<td height="25" class="RightTableCaption" align="right" colspan="2">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="Project Name:" td="false" />
				</td>
				<td colspan="2">&nbsp;<gmjsp:dropdown controlName="projectId"
						SFFormName="frmManufCost" SFSeletedValue="projectId"
						SFValue="alProject" codeId="ID" codeName="IDNAME" width="300"
						defaultValue="[Choose One]" /></td>
				<td class="RightTableCaption" align="right" colspan="2">&nbsp;<gmjsp:label
						type="RegularText" SFLblControlName="Part Number:" td="false" />
				</td>
				<td colspan="2">&nbsp;<html:text property="pnum"></html:text></td>

			</tr>
			<tr>
				<td height="1" colspan="8" class="LLine"></td>
			</tr>
			<tr>
				<td height="30" class="RightTableCaption" align="right" colspan="2">&nbsp;<html:checkbox
						property="strShowZeroCost" />
				</td>
				<td class="RightTableCaption" colspan="2">&nbsp;Open POs with zero cost(Manuf. FG)</td>
				<td height="30" colspan="4" align="center"><gmjsp:button
					value="&nbsp;Load&nbsp;" gmClass="button" buttonType="Load" onClick="fnLoad();"/>
				</td>
			</tr>
			<%if(!gridData.equals("")){%>

			<tr>
				<td colspan="8">
				<div  id ="manufCostRpt" style="" height="500px" width="900px"></div>
				</td>
			</tr>
<tr>
				<td height="1" colspan="8" class="LLine"></td>
			</tr>
			<tr>
			                <td colspan="8" align="center">
			                <div class='exportlinks'>Export Options : <img
					src='img/ico_file_excel.png' />&nbsp;<a href="#"
					onclick="gridObj.toExcel('/phpapp/excel/generate.php');"> Excel
				</a></div>
			                </td>

			</tr>
			<%}else{ %>
					<tr><td colspan="8" height="1" class="LLine"></td></tr>
					<tr>
						<td colspan="8" align="center" height="30" class="RightText">
							No Data Available
						</td>
					</tr>
					<%} %>
		</table>

	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>