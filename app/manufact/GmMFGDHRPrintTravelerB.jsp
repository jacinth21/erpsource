 <%
/**********************************************************************************
 * File		 		: GmMFGDHRPrintTravelerB.jsp
 * Desc		 		: This screen is used for the printing DHR sheets for the Manufacturing process
 * Version	 		: 1.0
 * author			: Karthik
************************************************************************************/
%>

<!-- manufact\GmMFGDHRPrintTravelerB.jsp -->



<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmDHRDetails = new HashMap();
	ArrayList alSubDHR = new ArrayList();

	String strShade = "";

	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyRej = "";
	String strManufDate = "";
	String strUserName = "";
	String strCreatedDate = "";
	String strQtyOrdered = "";
	String strProjId = "";
	String strPackPrim = "";
	String strPackSec = "";
	String strLabel = "";
	String strInsert = "";
	String strSubPartNum = "";
	String strSubPartDesc = "";
	String strSubControl = "";
	String strSubManfDt = "";
	String strSterFl = "";
	String strPRTFooter = "";
	String strApplDateFmt = strGCompDateFmt;
	String strCompanyLogo = "";
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		strCompanyLogo = GmCommonClass.parseNull((String) hmReturn.get("LOGO"));
		
		strDHRId = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId = GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName = GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		strManufDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("MDATE"),strApplDateFmt); 
		strUserName = GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplDateFmt); 
		strQtyOrdered = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYORD"));
		strProjId = GmCommonClass.parseNull((String)hmDHRDetails.get("PROJID"));
		strPackPrim = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKPRIM"));
		strPackSec = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKSEC"));
		strInsert = GmCommonClass.parseNull((String)hmDHRDetails.get("INSID"));
		strQtyRej = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ"));
		strLabel = strPartNum.replaceAll("\\.","");
		strSterFl = GmCommonClass.parseNull((String)hmDHRDetails.get("STERFL"));
		strPRTFooter = GmCommonClass.parseNull((String)hmDHRDetails.get("PRTFOOTER"));

		
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
	}

	int intSize = 0; 
	String strArr[] = strPRTFooter.split("\\^");
	String strFooter = strArr[0]; 
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Mfg. DHR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hPOId" value="<%=strDHRId%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hFooter" value="<%=strFooter%>">


<table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
         <tr>
			<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1" bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+2>IN-HOUSE MANUFACTURING &nbsp;&nbsp;&nbsp;<br>PRODUCT TRAVELER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
		</tr>
    </table>
    <BR>
     <table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
       	 <tr height="30" >
		    <td class="RightText" align="left" >Originator/Date:</td>
			<td class="RightText" >&nbsp;<%=strUserName%></td>
		    <td class="RightText" align="left" >DHR ID:</td>
			<td class="RightText" >&nbsp;<%=strDHRId%></td>
		</tr>
		<tr><td   colspan="8"  class="LLine"></td></tr>
		 <tr height="30">
		    <td class="RightText" align="left" >Part#:</td>
			<td class="RightText" >&nbsp;<%=strPartNum%></td>
		    <td class="RightText" align="left" >Description:</td>
			<td class="RightText" >&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
		</tr>
		<tr><td   colspan="8"  class="LLine"></td></tr>
		 <tr height="30">
		    <td class="RightText" align="left">Lot Number:</td>
			<td class="RightText">&nbsp;<%=strControlNum%></td>
		    <td class="RightText" align="left">Work Order#:</td>
			<td class="RightText">&nbsp;<%=strWOId%></td>
		</tr>
     </table>
     <BR>
     <table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
     <tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="8">&nbsp;<b>ISSUE MATERIALS</b>&nbsp;(verify Bill of Materials is complete & approved)</td></tr>
      <tr><td   colspan="8"  class="Line"></td></tr>
     <tr height="30" >
		    <td class="RightText" align="center"  width="30%">BILL OF MATERIALS#:</td>
			<td align="left" colspan="4"></td>
		    <td class="RightText" align="left"  width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature:</td>
		    <td colspan="5"align="left"  ></td>
		</tr>
		<tr><td   colspan="8"  class="LLine"></td></tr>
		<tr height="30">
		    <td class="RightText" align=center width="30%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ISSUE DATE:</td>
			<td colspan="4" align="left"></td>
		    <td class="RightText" align="left" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		    <td colspan="5" align="left"></td>
		</tr>
	</table>
	<BR>
    <table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
     <tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="8">&nbsp;<b>INSPECTION</b>&nbsp;(verify Inspection Form is complete & approved)</td></tr>
     <tr><td   colspan="8"  class="Line"></td></tr>
     <tr height="30" colspan="4">
		  <td class="RigntText" align="left" width="47%">
		 &nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;PASS</td>
		  <td width="12%"></td>
		  <td class="RigntText" align="left" colspan="1" >Signature:</td>
		</tr>
		<tr><td   colspan="8"  class="LLine"></td></tr>
		<tr height="30">
		    <td class="RigntText" align="left" width="47%">
		    &nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;FAIL(NCMR #__________)</td>
		  <td width="12%"></td>
		  <td class="RigntText" align="left" colspan="1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		   
		</tr>
	</table>
	<BR>
	 <table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
     <tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="8">&nbsp;<b>LABELING</b>&nbsp;(verify Label Reconciliation Form is complete & approved)</td></tr>
     <tr><td   colspan="8"  class="Line"></td></tr>
     <tr height="30" colspan="4">
		    <td class="RightText" align="left"  width="30%">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;YES</td>
			<td align="left" colspan="4"></td>
		    <td class="RightText" align="left"  width="30%">Signature:</td>
		    <td colspan="1"align="left" colspan="5" ></td>
		</tr>
		<tr><td   colspan="8"  class="LLine"></td></tr>
		<tr height="30">
		    <td class="RightText" align="left" width="40%">
		    &nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;NO</td>
			<td colspan="4" align="left" colspan="5"></td>
		    <td class="RightText" align="left" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		    <td colspan="4" align="left" colspan="5"></td>
		</tr>
	</table>
	<BR>
	 <table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
     <tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="8">&nbsp;<b>STERILIZATION</b>&nbsp;(attach radiation certification form)</td></tr>
     <tr><td   colspan="8"  class="Line"></td></tr>
     <tr height="30" colspan="4">
		    <td class="RightText" align="left"  width="30%">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;GAMMA STERILIZATION</td>
			<td align="left" colspan="4"></td>
		    <td class="RightText" align="left"  width="30%">Signature:</td>
		    <td colspan="1"align="left" colspan="5" ></td>
		</tr>
		<tr><td   colspan="8"  class="LLine"></td></tr>
		<tr height="30">
		    <td class="RightText" align="left" width="40%">
		    &nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;ASEPTIC PROCESSING</td>
			<td colspan="4" align="left" colspan="5"></td>
		    <td class="RightText" align="left" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		    <td colspan="4" align="left" colspan="5"></td>
		</tr>
		<tr><td   colspan="8"  class="LLine"></td></tr>
		<tr height="30">
		 <td class="RightText" align="left" colspan="1">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;ETO STERILIZATION</td>
		</tr>
	</table>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
     <tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="8">&nbsp;<b>QUANTITY PLACED IN INVENTORY</b>&nbsp;(verify work order form is complete &approved)</td></tr>
     <tr><td   colspan="8"  class="Line"></td></tr>
     <tr height="30" colspan="4">
		    <td class="RightText" align="left" width="30%">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;RAW MATERIALS</td>
			<td align="left" colspan="4"></td>
		    <td class="RightText" align="left"  width="30%">Signature:</td>
		    <td colspan="1"align="left" colspan="5" ></td>
		</tr>
		<tr><td   colspan="8"  class="LLine"></td></tr>
		<tr height="30">
		    <td class="RightText" align="left" width="40%">
		   &nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;FINISHED GOODS</td>
			<td colspan="4" align="left" colspan="5"></td>
		    <td class="RightText" align="left" width="30%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</td>
		    <td colspan="4" align="left" colspan="5"></td>
		</tr>
	</table>
	<BR>
	 <table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
     <tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="8">&nbsp;<b>UPDATE DHR / FINAL INSPECTION</b>&nbsp;(verify DHR Review Form is complete & approved)</td></tr>
     <tr><td   colspan="8"  class="Line"></td></tr>
     <tr height="30" colspan="4">
		    <td class="RightText" align="left" >&nbsp;&nbsp;DATE DHR COMPLETED</td>
			<td align="left" width="20%"></td>
		    <td class="RightText" align="left">Signature:</td>
		    <td align="left" width="30%" ></td>
		</tr>
		<tr><td   colspan="8"  class="LLine"></td></tr>
		<tr height="30">
		    <td class="RightText" align="left" width="43%">&nbsp;&nbsp;ACCEPTED/REJECTED &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; QUANTITY ACCEPTED</td>
			<td align="left" width="20%" ></td>
		    <td class="RightText" align="left" width="30%">QUANTITY REJECTED</td>
		    <td  align="left" width="30%" ></td>
		</tr>
	</table>
	<BR>
	<span class="RightText"><%=strFooter%></span>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
