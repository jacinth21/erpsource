 <%
/**********************************************************************************
 * File		 		: GmMFGTransMat.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>

<!-- manufact\GmMFGTransMat.jsp -->


<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strhAction = (String)request.getAttribute("hAction");
	String strWOId = "";
	String strMapId = "";
	String strQty = "";
	String strUser =	"";
	String strDate = "";
	String strToPartNum = "";
	String strFromPartNum = "";
	int intSize = 0;
	ArrayList alReport = new ArrayList();

	if (hmReturn != null)
	{
		alReport = (ArrayList)hmReturn.get("REPORT");
	}

	if (alReport != null)
	{
		intSize = alReport.size();
	}
	
	HashMap hmLoop = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Manufacturing - Transfer Materials </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function fnSubmit()
{
	document.frmManf.hAction.value = 'Transfer';
	var cnt = document.frmManf.hCnt.value;
	var obj = '';
	var ids = '';
	for (i=0;i<cnt;i++)
	{
		obj = eval("document.frmManf.hMapId"+i);
		if(obj.checked)
		{
			ids = ids + obj.value + ',';
		}
	}
	if (ids == '')
	{
		alert("Choose atleast one Work Order ID to accept Qty into Mfg Inventory");
		return false;
	}else{
		document.frmManf.hIds.value = ids.substr(0,ids.length-1);
		//alert(document.frmManf.hIds.value);
		fnStartProgress('Y');
		document.frmManf.submit();
	}
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmManf" method="POST" action="<%=strServletPath%>/GmMFGMatTransServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hIds" value="">
	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td height="25" class="RightDashBoardHeader">Globus Materials' Transfers</td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td height="1" bgcolor="#666666"></td></tr>
		<tr>
			<td width="698" height="100" valign="top" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="ShadeRightTableCaption" height="25">
						<td width="100">&nbsp;Globus WO</td>
						<td width="100">&nbsp;Qty to Accept</td>
						<td width="100">&nbsp;Globus Part #</td>
						<td width="100">&nbsp;Mfg Part #</td>
						<td width="100">&nbsp;Created By</td>
						<td width="100">&nbsp;Created Date</td>
					</tr>
					<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
<%
			  	if (intSize > 0)
			  	{
					hmLoop = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hmLoop = (HashMap)alReport.get(i);
			  			strWOId = (String)hmLoop.get("WOID");
						strMapId = (String)hmLoop.get("MAPID");
						strQty = (String)hmLoop.get("QTY");
						strUser = (String)hmLoop.get("CUSER");
						strDate = (String)hmLoop.get("CDATE");
						strToPartNum = (String)hmLoop.get("TOPARTNUM");
						strFromPartNum = (String)hmLoop.get("FROMPARTNUM");
%>
					<tr>
						<td class="RightText" HEIGHT="22"><input type="checkbox" name="hMapId<%=i%>" value="<%=strMapId%>"/>&nbsp;<%=strWOId%>
						</td>
						<td class="RightText" align="center"><%=strQty%></td>
						<td class="RightText"><%=strFromPartNum%></td>
						<td class="RightText"><%=strToPartNum%></td>
						<td class="RightText"><%=strUser%></td>
						<td class="RightText"><%=strDate%></td>
					</tr>
					<tr><td colspan="6" height="1" bgcolor="#eeeeee"></td></tr>
<%
			  		} // END of FOR Loop
%>
					<tr>
						<td colspan="6" height="30" align="center">
							<input type="hidden" name="hCnt" value="<%=intSize%>"/>
							<gmjsp:button value="Submit" gmClass="button" buttonType="Save" onClick="fnSubmit();" tabindex="13"/>
						</td>
					</tr>
<%			  		
			  	}else{
%>
					<tr>
						<td colspan="6" height="30" class="RightTextRed" align="center"> No Pending Material Transfers from Globus' Main Inventory !</td>
					</tr>
<%			  		
			  	}
%>
				 </table>
			 </td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
    </table>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
