<%
/**********************************************************************************
 * File		 		: GmMFGDHRPrintReviewFormA.jsp
 * Desc		 		: This screen is used for the printing DHR sheets for the Manufacturing process
 * Version	 		: 1.0
 * author			: Karthik
************************************************************************************/
%>


<!-- manufact\GmMFGDHRPrintReviewFormA.jsp -->



<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmDHRDetails = new HashMap();
	ArrayList alSubDHR = new ArrayList();

	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyRej = "";
	String strManufDate = "";
	String strUserName = "";
	String strCreatedDate = "";
	String strQtyOrdered = "";
	String strProjId = "";
	String strPackPrim = "";
	String strPackSec = "";
	String strLabel = "";
	String strInsert = "";
	String strSubPartNum = "";
	String strSubPartDesc = "";
	String strSubControl = "";
	String strSubManfDt = "";
	String strSterFl = "";
	String strRFFooter  = "";
	String strApplDateFmt = strGCompDateFmt;
	String strCompanyLogo = "";

	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		strCompanyLogo = GmCommonClass.parseNull((String) hmReturn.get("LOGO"));
		
		strDHRId = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId = GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName = GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		strManufDate =GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("MDATE"),strApplDateFmt);
		strUserName = GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		strCreatedDate =GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplDateFmt); 
		strQtyOrdered = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYORD"));
		strProjId = GmCommonClass.parseNull((String)hmDHRDetails.get("PROJID"));
		strPackPrim = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKPRIM"));
		strPackSec = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKSEC"));
		strInsert = GmCommonClass.parseNull((String)hmDHRDetails.get("INSID"));
		strQtyRej = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ"));
		strLabel = strPartNum.replaceAll("\\.","");
		strSterFl = GmCommonClass.parseNull((String)hmDHRDetails.get("STERFL"));
		strRFFooter  = GmCommonClass.parseNull((String)hmDHRDetails.get("RFFOOTER"));

		
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
	}

	int intSize = 0; 
	String strArr[] = strRFFooter.split("\\^");
	String strFooter = strArr[0]; 
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Mfg. DHR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hPOId" value="<%=strDHRId%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hFooter" value="<%=strFooter%>">


	<table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+2>IN-HOUSE MANUFACTURING&nbsp;&nbsp;<br>DEVICE HISTORY RECORD&nbsp;&nbsp;&nbsp;<br>REVIEW FORM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="4"></td></tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>
		
		<tr>
			<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
          	  <tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
				<tr height="30">
					<td class="RightTableCaption" align="left" width=15%;>Part Number:</td>
	                <td bgcolor="#666666" width="1" rowspan="10"></td>
	                <td class="RightText" width=20%;>&nbsp;<%=strPartNum%></td>
	                <td bgcolor="#666666" width="1" rowspan="10"></td>
					<td class="RightTableCaption" align="left">Description: <span  class="RightText" style="font-weight:normal" align="left">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></span> </td>
				</tr>  	
				<tr><td bgcolor="#666666" width="1" class="Line" colspan="6"></td></tr>
	            <tr height="30">
				    <td class="RightTableCaption" align="left" width=15%;>Work Order#:</td>
				    						<td class="RightText">&nbsp;<%=strWOId%></td>
				    
				</tr>
				<tr><td bgcolor="#666666" width="1" class="Line" colspan="6"></td></tr>
				</table>
				
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="8">&nbsp;<b>Receipts Routing</b><font size="-2"> (from Product Traveler)</font>:</td></tr>
		        <tr><td bgcolor="#666666" colspan="8"  height="1"></td></tr>
				<tr height="30">
					<td class="RightTableCaption" align="right" width=20%;>Date of Manufacture:</td>
						<td class="RightText">&nbsp;<%=strManufDate%></td>
	                
					<td class="RightTableCaption" align="right" width=20%;>Quantity:</td>
						<td class="RightText">&nbsp;<%=strQtyRec%></td>
				</tr>
				<tr><td bgcolor="#666666" width="1" class="Line" colspan="8"></td></tr>
				 <tr height="30">
					<td class="RightTableCaption" align="right" width=15%;>Lot Number:</td>
						<td class="RightText">&nbsp;<%=strControlNum%></td>
					<td class="RightTableCaption" align="right" width=15%;>Quantity Released:</td>
				</tr>
				<tr><td bgcolor="#666666" width="1" class="Line" colspan="8"></td></tr>
				</table>
				
			   <table border="0" width="100%" cellspacing="0" cellpadding="0">
			   <tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="12">&nbsp;<b>Acceptance Records</b><font size="-2"> (attached)</font>:</td></tr>
			   <tr><td bgcolor="#666666" colspan="12"  height="1"></td></tr>
			   <tr height="30">
				<td class="RightTableCaption" align="right" width=20%;>Bill of Materials:&nbsp;&nbsp;</td>
					<td bgcolor="#666666" rowspan="10" width="1" bgcolor="#666666"></td>
					<td class="RightText">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;Yes&nbsp;&nbsp;<input type="checkbox">&nbsp;No</td>
	                <td bgcolor="#666666" rowspan="10" width="1" bgcolor="#666666"></td>
					<td class="RightTableCaption" align="right" width=36%;>Batch Record:&nbsp;&nbsp;</td>
					<td bgcolor="#666666" rowspan="10" width="1" bgcolor="#666666"></td>
					<td class="RightText">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;Yes&nbsp;&nbsp;<input type="checkbox">&nbsp;No</td>
				</tr>
				<tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
			    <tr height="30">
					<td class="RightTableCaption" align="right" width=20%;>Work Order Form:&nbsp;&nbsp;</td>
	                <td class="RightText">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;Yes&nbsp;&nbsp;<input type="checkbox">&nbsp;No</td>
	                <td class="RightTableCaption" align="right" width=36%;>label Reconciliation Form:&nbsp;&nbsp;</td>
	                <td class="RightText">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;Yes&nbsp;&nbsp;<input type="checkbox">&nbsp;No</td>
                </tr>
                <tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
			    <tr height="30">
					<td class="RightTableCaption" align="right" width=20%;>Product Traveler:&nbsp;&nbsp;</td>
	                <td class="RightText">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;Yes&nbsp;&nbsp;<input type="checkbox">&nbsp;No</td>
	                <td class="RightTableCaption" align="right" width=36%;>Product Specification Inspection Form:&nbsp;&nbsp;</td>
	                <td class="RightText">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;Yes&nbsp;&nbsp;<input type="checkbox">&nbsp;No</td>
                </tr>
                <tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
                </table>
                
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b>Review Checklist</b>:</td></tr>
				<tr><td bgcolor="#666666" colspan="12"  height="1"></td></tr>
			    <tr height="78">
			       <td width="78%"></td>
			       <td bgcolor="#666666" rowspan="10" width="1" bgcolor="#666666"></td>
				   <td class="RightText" align="center" style="transform:rotate(90deg);">PERFORMED</td>   <!-- Rotate property  not support in IE8 -->
                </tr>	
                <tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
                <tr height="40">
			       <td width="78%"  class="RightText" align="right" >Bill of Materials is complete&nbsp;</td>							
                   <td class="RightText" align="center"><img src="<%=strImagePath%>/n.gif></td>
                </tr>
                <tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
                <tr height="40">
			       <td width=""  class="RightText" align="right" >Batch Record is complete and fully approved&nbsp;</td>							
                   <td class="RightText" align="center"><img src="<%=strImagePath%>/n.gif></td>
                </tr>
                <tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
                <tr height="40">
			       <td width="78%"  class="RightText" align="right" >Correct labels and inserts were used&nbsp;</td>							
                   <td class="RightText" align="center"><img src="<%=strImagePath%>/n.gif></td>
                </tr>
                <tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
                <tr height="40">
			       <td width="78%"  class="RightText" align="right" >Finished device inspection was performed&nbsp;</td>							
                   <td class="RightText" align="center"><img src="<%=strImagePath%>/n.gif></td>
                </tr>
                <tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
               <tr height="40">
			       <td width="78%"  class="RightText" align="right" >Count was performed&nbsp;</td>	
		           <td bgcolor="#666666" rowspan="10" width="1" bgcolor="#666666"></td>
                   <td class="RightText" align="center"><img src="<%=strImagePath%>/n.gif></td>
               </tr>
               <tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
                <tr height="40">
			       <td width="78%"  class="RightText" align="right" >Device History Record is complete and accurate&nbsp;</td>
                   <td class="RightText" align="center"><img src="<%=strImagePath%>/n.gif></td>
                </tr>
                <tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
                <tr height="40">
			       <td width="78%"  class="RightText" align="right" >Work Order is complete and approved&nbsp;</td>	
                   <td class="RightText" align="center"><img src="<%=strImagePath%>/n.gif></td>
                </tr>
                <tr><td bgcolor="#666666" width="1" class="Line" colspan="12"></td></tr>
				</table>
       </td>
	</tr>	
	</table>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" class="DtTable700">
	<tr><td>
	     <table cellpadding="0" cellspacing="0" border="0" width="100%">
             <tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="12">&nbsp;<b>This production lot has met all quality requirements.</b>:</td></tr>
             <tr><td bgcolor="#666666" colspan="12"  height="1"></td></tr></table>
    <BR>
	<BR>
	<BR>
	<BR>
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
		    <tr>
				<td width="250"bgcolor="#666666" height="1"></td>
				<td height="1" width="200"></td>
				<td  width="150" bgcolor="#666666" height="1"></td>
		   </tr>
           <tr>
				<td class="RightText" align="center" height="10"> Authorized Signature for Release</td>
				<td class="RightText">&nbsp;</td>
				<td class="RightText" align="center">Date</td>
		   </tr>
		</table>
	</td>
  </tr>
  	
  </table>
	<BR>
	<span class="RightText"><%=strFooter%></span>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>