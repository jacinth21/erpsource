 <%
/**********************************************************************************
 * File		 		: GmMFGReconWO.jsp
 * Desc		 		: This screen is used for the reconciling WO released Quantities at the end of the Job
 * Version	 		: 1.0
 * author			: D James
************************************************************************************/
%>

<!-- manufact\GmMFGReconWO.jsp -->


<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%
String strManuFactJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_MANUFACT");
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("OPT"));
	
	String strMfgLotCodeId = "";
	String strPartDesc = "";
	String strOrdComplete = "";
	String strOrdPending = "";
	String strPartNum = "";
	String strMfgQty = "";
	String strBOMid = "";
	String strVendorId = "";
	String strLotCodeReconcFlag = "";
	String strMatReqId = GmCommonClass.parseNull((String)request.getAttribute("MATREQID"));
	String strMfgLotCodeMessage = "";
	String strDisabled = "";
	String strReconcTxnIdMarkUp = "";
	String strLotReconcilationStatus = "";
	String strQtyPlanned = "";
	String strWoId	= "";
	int intSize = 0;
	int intDHRStatFl = 0;
	String strRWChkFl = "";
	String strMfgLotCode="";
	String strMfgWODhrId="";
	
	HashMap hmMfgWO = (HashMap)request.getAttribute("HMLOADWO");
	HashMap hmPartDetails = (HashMap)request.getAttribute("PARTDETAILS");
    strMfgLotCode = GmCommonClass.parseNull((String)request.getAttribute("MFGWODHRID"));
	strMfgLotCodeId = GmCommonClass.parseNull((String)request.getAttribute("MFGLOTCODEID"));
	strLotCodeReconcFlag = GmCommonClass.parseNull((String)request.getAttribute("LOTCODERECONCFLAG"));
	strhAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	ArrayList alMatReqItemDetails = (ArrayList)request.getAttribute("MATREQITEMDETAILS");
	strReconcTxnIdMarkUp = GmCommonClass.parseNull((String)request.getAttribute("RECONCTXNIDSMARKUP"));
	strLotReconcilationStatus = GmCommonClass.parseNull((String)request.getAttribute("LOTRECONCSTATUS"));
	String strrawfl = GmCommonClass.parseNull((String)request.getAttribute("RWFLAG"));
	
	// Checking if the Reconciliation Status is flag is ON. If it is ON, then materials cant be reconciled.
	if (strLotCodeReconcFlag.equals("ON"))
	{
		strMfgLotCodeMessage = " Cannot reconcile  since one or more Material Requests are Pending for this lot .";
		strDisabled = "true";
	}
	
	// Checking if Reconciliation has already happened for the specified Lot
	if (strLotReconcilationStatus.equals("ON"))
	{
		strMfgLotCodeMessage = " Cannot reconcile as this Lot has already been reconciled .";
		strDisabled = "true";
	}
	
	if (hmMfgWO != null)
	{
		strOrdComplete = GmCommonClass.parseNull((String)hmMfgWO.get("QTYDONE"));
		strOrdPending = GmCommonClass.parseNull((String)hmMfgWO.get("QTYPDG"));
		strBOMid = GmCommonClass.parseNull((String)hmMfgWO.get("BOMID"));
		strVendorId =  GmCommonClass.parseNull((String)hmMfgWO.get("VENDID"));
	}
	
	if (hmPartDetails!=null)
	{
		strMfgWODhrId = GmCommonClass.parseNull((String)hmPartDetails.get("DHRID"));
		strPartNum = GmCommonClass.parseNull((String)hmPartDetails.get("PARTNUM"));
		strPartDesc = GmCommonClass.parseNull((String)hmPartDetails.get("PARTDESC"));
		strQtyPlanned = GmCommonClass.parseNull((String)hmPartDetails.get("RQTY"));
		strRWChkFl = GmCommonClass.parseNull((String)hmPartDetails.get("RWFL"));
		strWoId = GmCommonClass.parseNull((String)hmPartDetails.get("WOID"));
		intDHRStatFl = Integer.parseInt(GmCommonClass.parseNull((String)hmPartDetails.get("SFL")));
	}
	strrawfl = strRWChkFl.equals("Y")?"Yes":"No";
	
	if (alMatReqItemDetails!=null)
	{	
		intSize = alMatReqItemDetails.size();
	}
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Manufacturing Work Order Reconciliation </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strManuFactJsPath%>/GmMFGReconWO.js"></script>

<script>
var rawfl = '<%=strRWChkFl%>';
var qtyplanned = '<%=strQtyPlanned%>';
var strWOId = '<%=strWoId%>';
var strMfgWODhrId = '<%=strMfgWODhrId%>';

</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmMfg" method="POST" action="<%=strServletPath%>/GmMFGReconWOServlet">
<input type="hidden" name="hType" value="">
<input type="hidden" name="hPartNum" value="<%=strPartNum%>">
<input type="hidden" name="hOrdQty" value="<%=strPartDesc%>">
<input type="hidden" name="hOrdComplete" value="<%=strOrdComplete%>">
<input type="hidden" name="hOrdPending" value="<%=strOrdPending%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="hMatReqId" value="<%=strMatReqId%>">
<input type="hidden" name="hReturnStr" value="">
<input type="hidden" name="hScrapStr" value="">
<input type="hidden" name="hCancelType">
<input type="hidden" name="hTxnId">
<input type="hidden" name="hWOId" value="<%=strWoId%>">
<input type="hidden" name="hMfgWODHRId" value="<%=strMfgWODhrId%>">


	<table border="0" width="700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="9"></td>
			<td height="25" class="RightDashBoardHeader">&nbsp;Supply Reconciliation</td>
			<td bgcolor="#666666" width="1" rowspan="9"></td>
		</tr>
		
		<tr><td bgcolor="#666666" height="1"></td></tr>
		<tr>
			<td width="698" height="30" valign="top" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" HEIGHT="25" align="right">&nbsp;Lot Number:</td>
						<td class="RightText" width="520">&nbsp;<input type="text" size="15" value="<%=strMfgLotCodeId%>" name="Txt_MfgLotCodeId" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;
						<gmjsp:button value="Load" gmClass="button" buttonType="Load" onClick="fnLoadLotCode();" tabindex="13"/>
						</td>
					</tr>

					<%=strReconcTxnIdMarkUp%>
			
			
<%
	if (strhAction.equals("Reload"))
	{
		if (!hmPartDetails.isEmpty())
		{ 
%>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;Part Number:</td>
						<td class="RightText">&nbsp;<%=strPartNum%>
						</td>
					</tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;Part Description:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
					</tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;Qty Planned :</td>
						<td>&nbsp;<%=strQtyPlanned%></td>
					</tr>
					<tr>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;Qty Manufactured:</td>
						<td>
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
						<td>&nbsp;<input type="text" name="Txt_QtyComp" size="5"  class="RightText"></td>
						<td class="RightText" HEIGHT="20" align="right">&nbsp;Move to Restricted Warehouse:</td>
					<td class="RightText" width="27%">&nbsp;<%=strrawfl%></td>
							</tr>
						</table>
								</td>				
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
			    </table>
			</td>
		</tr>
				
					
		<tr>
			<td align = "center" height="30">
<%
			if (intSize > 0)
			{
%>			
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr class="ShadeRightTableCaption">
						<td colspan="8" height="20">Released Parts for Lot : <%=strMfgLotCodeId%></td>
					</tr>
					<tr><td colspan="8" height="1" bgcolor="#666666"></td></tr>
<%					if (!strMfgLotCodeMessage.equals(""))
								{
%>
					<tr>
						<td colspan="8" height="20" class="RightTextRed" align="center"> <%=strMfgLotCodeMessage%></td>
					</tr>
					<tr><td colspan="8" height="1" bgcolor="#666666"></td></tr>
<% 							}				if (!strLotReconcilationStatus.equals("ON"))
	{	%>					
					<tr bgcolor="#eeeeee">
						<td height="20" width="100" class="RightText">Part Number</td>
						<td width="300" class="RightText">Description</td>
						<td width="100" class="RightText">Qty Released</td>
						<td width="100" class="RightText">Lot Number</td>
						<td width="90" class="RightText">Qty to Return</td>
						<td width="90" class="RightText">Qty to Scrap</td>
					</tr>
					<tr><td colspan="8" height="1" bgcolor="#666666"></td></tr>
<%					
					HashMap hcboVal = new HashMap();
					String strQtyInBulk = "";

					HashMap hmLoop = new HashMap();
					HashMap hmTempLoop = new HashMap();

					String strNextPartNum = "";
					int intCount = 0;
					String strColor = "";
					String strLine = "";
					String strPartNumHidden = "";
					int intSetQty = 0;
					boolean bolQtyFl = false;
					String strReadOnly = "";
					StringBuffer sbPartNum = new StringBuffer();
					String strQty = "";
					String strControl = "";


			  		for (int i=0;i<intSize;i++)
			  		{
						hmLoop = (HashMap)alMatReqItemDetails.get(i);
						if (i<intSize-1)
						{
							hmTempLoop = (HashMap)alMatReqItemDetails.get(i+1);
							strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PARTNUM"));
						}
						strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PARTNUM"));
						sbPartNum.append(strPartNum);
						sbPartNum.append(",");
						strPartNumHidden = strPartNum;
						strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PARTDESC"));
						strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
						strControl = GmCommonClass.parseNull((String)hmLoop.get("CRLNUM"));
						
						if (strPartNum.equals(strNextPartNum))
						{
							intCount++;
							strLine = "<TR><TD colspan=5 height=1 bgcolor=#eeeeee></TD></TR>";
						}
						else
						{
							strLine = "<TR><TD colspan=5 height=1 bgcolor=#eeeeee></TD></TR>";
								if (intCount > 0)
								{
									strPartNum = "";
									strPartDesc = "";
									strLine = "";
								}
								else
								{
									//strColor = "";
								}
								intCount = 0;
							}

							if (intCount > 1)
							{
								strPartNum = "";
								strPartDesc = "";
								strLine = "";
							}
							
							out.print(strLine);
%>
					<tr height="18">													
						<td class="RightText">&nbsp;<%=strPartNum%>
						<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden%>">
						</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
						<td align="center" class="RightText"><%=strQty%>
						<input type="hidden" name="hQty<%=i%>" value="<%=strQty%>"></td>
						<td align="center" class="RightText"><%=strControl%>
						<input type="hidden" name="hCnum<%=i%>" value="<%=strControl%>"></td>
						<td align="center"><input type="text" size="4" name="Txt_Ret<%=i%>" class="RightText" value=""/></td>
						<td align="center"><input type="text" size="4" name="Txt_Scr<%=i%>" class="RightText" value=""/></td>					
					</tr>
					<tr><td colspan="8" bgcolor="#cccccc"></td></tr>
<%
					} // End of For
%>					
				<input type="hidden" name="hCnt" value="<%=intSize-1%>">
				</table>
				<BR><gmjsp:button disabled="<%=strDisabled%>" value="Reconcile" gmClass="button" buttonType="Save" onClick="javascript:fnReconcile();"/>
					<%
		 if(intDHRStatFl == 0 ) {
				%>
				 <gmjsp:button value="Void DHR" gmClass="button" buttonType="Save" onClick="fnVoidDHR();"/>
				 <%
				}
			%>	
<%
			}
			}else{
%>				
				<span class="RightTextBlue">Cannot reconcile this Work Order as Parts have not yet been released! </span>
<%
			}
%>
			</td>
			
		</tr>
<%	
	 		}else{
%>
					<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
					<tr>
						<td colspan="3" height="30"  align="center" class="RightTextblue">&nbsp;No data available for this Work Order!</td>
					</tr>
					
<%

			}
	}
%>
			
<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
</table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>