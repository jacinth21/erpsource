 <%
/**********************************************************************************
 * File		 		: GmManfDashBrdMatReq.jsp
 * Desc		 		: This screen is used for the displaying all Pending Material Requests
 * Version	 		: 1.0
 * author			: d James
************************************************************************************/
%>

<!-- manufact\GmManfDashBrdMatReq.jsp -->



<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
 <%@ include file="/common/GmHeader.inc" %>
<%

try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	GmLogger log = GmLogger.getInstance();
	
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("OPT"));

%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Pending Manufacturing Material Requests </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>
function fnCallReqId(val,wo)
{
	document.frmMfg.hMatReqId.value = val;
	document.frmMfg.Txt_MfgLotCodeId.value = wo;
	document.frmMfg.hAction.value = 'Reload';
	document.frmMfg.hOpt.value = 'REQREL';
  	document.frmMfg.submit();
}


</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmMfg" method="POST" action="<%=strServletPath%>/GmMFGInitiateWOServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hOpt" value="">
<input type="hidden" name="hMatReqId" value="">
<input type="hidden" name="Txt_MfgLotCodeId" value="">
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
			<td height="25" class="RightDashBoardHeader">&nbsp;Dashboard - Pending Material Requests</td>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="698" height="30" valign="top" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
						<display:table name="MATREQUEST"  class = "its" id="currentRowObject" decorator="com.globus.displaytag.beans.DTMfgMaterialRequestWrapper"> 
						<display:column property="PNUM" title="Part Number" class="aligncenter" />
					  	<display:column property="CNUM" title="Lot Number" class="aligncenter" />
						<display:column property="MATREQID" title="Request Id" class="aligncenter"  sortable="true" />
						<display:column property="REQDATE" title="Request Date" class="aligncenter"  />
					  	<display:column property="REQUESTOR" title="Requested By" class="aligncenter" sortable="true"  />
					  	<display:column property="STATUSFL" title="Status" class="aligncenter" />
						</display:table>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
			    </table>
			</td>
		</tr>
    <table>

<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>