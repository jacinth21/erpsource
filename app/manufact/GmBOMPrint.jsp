 <%
/**********************************************************************************
 * File		 		: GmBOMPrint.jsp
 * Desc		 		: This screen is used for printing BOM
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- manufact\GmBOMPrint.jsp -->



<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%
String strManuFactJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_MANUFACT");
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	HashMap hmBOMProdDetails = new HashMap();
	hmBOMProdDetails = (HashMap)request.getAttribute("hmProductInfo");
	String strMatReqId = GmCommonClass.parseNull((String)request.getAttribute("hMatReqId"));
	String strShade = "";

	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyRej = "";
	String strManufDate = "";
	String strUserName = "";
	String strCreatedDate = "";
	String strQtyOrdered = "";
	String strProjId = "";
	String strPackPrim = "";
	String strPackSec = "";
	String strLabel = "";
	String strInsert = "";
	String strSubPartNum = "";
	String strSubPartDesc = "";
	String strSubControl = "";
	String strSubManfDt = "";
	String strSterFl = "";
	String strLotCode = "";
	String strUDINo = "";
	String strTxnId = "";

	HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid() ,"");
	String strCompanyLogo = GmCommonClass.parseNull((String)hmCompanyAddress.get("COMP_LOGO"));
	strDHRId = GmCommonClass.parseNull((String)hmBOMProdDetails.get("ID"));
	strPartNum = GmCommonClass.parseNull((String)hmBOMProdDetails.get("PNUM"));
	strDesc = GmCommonClass.parseNull((String)hmBOMProdDetails.get("PDESC"));
	strWOId = GmCommonClass.parseNull((String)hmBOMProdDetails.get("WOID"));
    strLotCode = GmCommonClass.parseNull((String)hmBOMProdDetails.get("LOTCODE"));
    String strQtyToManf = GmCommonClass.parseNull((String)request.getAttribute("hQtyToManf"));
    String strOpt = GmCommonClass.parseNull((String)request.getAttribute("hOpt"));
    String strManfQty = GmCommonClass.parseNull((String)hmBOMProdDetails.get("RQTY"));
    strUDINo = GmCommonClass.parseNull((String)hmBOMProdDetails.get("UDINO"));
    strUDINo = strUDINo.equals("")?"N/A":strUDINo; 
    if(strOpt.equals("MATREQDET")){
    	strMatReqId = "N/A";
    	strQtyToManf = strManfQty;
    }
	int intSize = 0;
	HashMap hcboVal = null;
	
	strTxnId = strDHRId+"$"+strLotCode;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: DHR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strManuFactJsPath%>/GmBOMPrint.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<style type="text/css">
td.LineBreak{
word-break: break-all;
}
</style>
</HEAD>

<BODY leftmargin="20" topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmMFGInitateWOServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hPOId" value="<%=strDHRId%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="hAction" value="">
<BR>
<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="right" class="RightText">
				<img align="middle" src='/GmCommonBarCodeServlet?ID=<%=strTxnId%>&type=2d'  />
				<font size=+2>&nbsp;&nbsp;Bill Of Materials&nbsp;</font>
			</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="4"></td></tr>
		<tr><td bgcolor="#666666" colspan="4"></td></tr>
		<tr>
			<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" align="right">Part Number:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightTableCaption" height="30" align="right">Part Description:</td>
						<td  class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
						</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
						<td class="RightTableCaption" height="30" align="right">Work Order Id:</td>
						<td class="RightText">&nbsp;<%=strWOId%></td>
						<td class="RightTableCaption" align="right">DHR Id:</td>
						<td colspan="3" class="RightText">&nbsp;<%=strDHRId%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="6"></td></tr>
					<tr>
					    <td class="RightTableCaption" height="30" align="right">Qty to Manufacture:</td>
					    <td class="RightText">&nbsp;<%=strQtyToManf%></td>
					    <td class="RightTableCaption" align="right">UDI:</td>
						<td colspan="3" class="LineBreak" width="350">&nbsp;<%=strUDINo%></td>
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					<tr>
						<td class="RightTableCaption" height="30" align="right">Material Request Id:</td>
						<td class="RightText">&nbsp;<%=strMatReqId%></td>
						<td class="RightTableCaption" align="right">Lot #:</td>
						<td colspan="3" class="RightText">&nbsp;<%=strLotCode%></td>
					</tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b>Bill Of Materials :</b></td></tr>
					<tr><td bgcolor="#666666" colspan="6"></td></tr>
					
						<tr>
						<td colspan="6">
						<display:table name="requestScope.BOMREPORT.rows"  class = "its" id="currentRowObject" > 
						<display:column property="PNUM" title="Part Number" class="alignleft" sortable="true" />
						<display:column property="PDESC" title="Part Description" class="alignleft" sortable="true" />
						<display:column property="CONTROLNUM" title="Control Number" class="alignright"  />
					  	<display:column property="QTY" title="Quantity" class="alignright" sortable="true"  />
						</display:table>
						</td>
					</tr>
			<tr><td bgcolor="#666666" colspan="6"></td></tr>
			<tr></tr>
			<tr>
				<td colspan="4" align="center" id="buttondiv"> 
					<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="javascript:fnPrint();"/> &nbsp;&nbsp;
					<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" buttonType="Load" onClick="window.close();"/> </td>
			</tr>
			<tr></tr>
			<tr><td bgcolor="#666666" colspan="6"></td></tr>
	</table>
	</div>

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
			