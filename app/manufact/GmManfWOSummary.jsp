 <%
/**********************************************************************************
 * File		 		: GmManfWOSummary.jsp
 * Desc		 		: This screen is used for the displaying Work Order summary
 * Version	 		: 1.0
 * author			: d James
************************************************************************************/
%>

<!-- manufact\GmManfWOSummary.jsp -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtManfWOSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtManfWOSummary:setLocale value="<%=strLocale%>"/>
<fmtManfWOSummary:setBundle basename="properties.labels.accounts.manufact.GmManfWOSummary"/>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%
	String strManuFactJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_MANUFACT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	ArrayList alTypes = (ArrayList)request.getAttribute("TRANSTYPES");
	ArrayList alReturn = (ArrayList)request.getAttribute("alResult");
	HashMap hmParam = (HashMap)request.getAttribute("PARAMS");
	
	String strPNum = "";
	String strLNum = "";
	String strWOID = "";
	String strDHRID = "";
	String strTransID = "";
	String strWOStat = "00";
	String strDHRStat = "00";
	String strTransStat = "00";
	String strTransType = "0";
	String strWOType = "00";
	int intSize = 0;
	String strGridXmlData = GmCommonClass.parseNull((String)request.getAttribute("GRIDXMLDATAWO"));

	if (hmParam!=null)
	{
		strPNum = GmCommonClass.parseNull((String)hmParam.get("PNUM"));
		strLNum = GmCommonClass.parseNull((String)hmParam.get("LNUM"));
		strWOID = GmCommonClass.parseNull((String)hmParam.get("WOID"));
		strDHRID = GmCommonClass.parseNull((String)hmParam.get("DHRID"));
		strTransID = GmCommonClass.parseNull((String)hmParam.get("TRANSID"));
		strWOStat = GmCommonClass.parseNull((String)hmParam.get("WOSTAT"));
		strDHRStat = GmCommonClass.parseNull((String)hmParam.get("DHRSTAT"));
		strTransStat = GmCommonClass.parseNull((String)hmParam.get("TRANSTAT"));
		strTransType = GmCommonClass.parseNull((String)hmParam.get("TRANSTYPE"));
		strWOType = GmCommonClass.parseNull((String)hmParam.get("WOTYPE"));
	}
	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Work Order Summary </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strManuFactJsPath%>/GmManfWOSummary.js"></script>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
WOStat = '<%=strWOStat%>';
Cbo_DHRStat = '<%=strDHRStat%>';
Cbo_TransStat = '<%=strTransStat%>';
Cbo_TransType = '<%=strTransType%>';
Cbo_WOType = '<%=strWOType%>';

var objGridData;
objGridData ='<%=strGridXmlData%>';
var gridObj;

</script>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnSetValues();fnOnPageLoad();">  
<FORM name="frmMfg" method="POST" action="<%=strServletPath%>/GmMFGReconWOServlet">
<input type="hidden" name="hAction" value="Reload">
<input type="hidden" name="hOpt" value="LoadWOSum">

	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;Work Order Summary</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td align="right">Part Number:</td>
						<td>&nbsp;<input class="InputArea" type="text" size="30" name="Txt_PNum" value="<%=strPNum%>"></td>
						<td align="right">WO ID:</td>
						<td>&nbsp;<input class="InputArea" type="text" size="15" name="Txt_WOID" value="<%=strWOID%>"></td>
						<td align="right">DHR ID:</td>
						<td>&nbsp;<input class="InputArea" type="text" size="15" name="Txt_DHRID" value="<%=strDHRID%>"></td>
						<td align="right">Trans ID:</td>
						<td>&nbsp;<input class="InputArea" type="text" size="15" name="Txt_TransID" value="<%=strTransID%>"></td>
						<td align="center" rowspan="2"><BR>&nbsp;&nbsp;<gmjsp:button value="Reload" gmClass="button" buttonType="Load" onClick="fnLoadReport();" tabindex="13"/><BR><BR>
						<gmjsp:button value="Reset" gmClass="button" buttonType="Load" onClick="javascript:fnReset();" tabindex="13"/>
						</td>
					</tr>
					<tr>
						<td align="right">Lot Number:</td>
						<td>&nbsp;<input class="InputArea" type="text" size="10" name="Txt_LNum" value="<%=strLNum%>"></td>
						<td align="right">WO Status:</td>
						<td>&nbsp;<select name="Cbo_WOStat">>
									<option value="00">[Choose One]
									<option value="1">Open
									<option value="2">Closed
								  </select>
						</td>
						<td align="right">DHR Status:</td>
						<td>&nbsp;<select name="Cbo_DHRStat">>
									<option value="00">[Choose One]
									<option value="0">Initiated
									<option value="1">Reconciled
									<option value="3">Inspected
									<option value="4">Closed
								  </select></td>
						<td align="right">Trans Status:</td>
						<td>&nbsp;<select name="Cbo_TransStat">>
									<option value="00">[Choose One]
									<option value="0">Initiated
									<option value="1">In Progress
									<option value="2">Closed
								  </select></td>
					</tr>
					<tr>
						<td align="right"></td>
						<td>&nbsp;</td>
						<td align="right">WO Type:</td>
						<td>&nbsp;<select name="Cbo_WOType">>
									<option value="00">[Choose One]
									<option value="3104">Modified Supply
									<option value="3105">BioMat Product
								  </select></td>
						<td align="right">&nbsp;</td>
						<td>&nbsp;</td>
						<td align="right">Trans Type:</td>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<td>&nbsp;<gmjsp:dropdown controlName="Cbo_TransType"  seletedValue="" 	
						   value="<%= alTypes %>" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" /></td>
					</tr>		
					<tr></tr>
				</table>
			</td>		
		</tr>
<%if (strAction.equals("Reload")){%>		
<%intSize = alReturn.size();%> 
	<%if (intSize > 0){%>	
		<tr>
				<td colspan="3">
					<div id="manfWOSummaryRpt" style="height: 620px; width: 100%"></div>
			</td>
					
			</tr>
				<tr><td class="Line" height="1" colspan="2"></td></tr>
				
			<tr>
			<td colspan="17" align="center">
			 <div class='exportlinks'><fmtManfWOSummary:message key="LBL_EXPORT_OPTS"/> : <img src='img/ico_file_excel.png' onclick='fnExcel();' />&nbsp;<a href="#" onclick="fnExcel();"><fmtManfWOSummary:message key="LBL_EXCEL"/></a></div>
			</td>
		</tr>
<%}else{ %>
	<tr><td class="Line" height="1" colspan="2"></td></tr>
 	<tr><td colspan="5" class="RightTextBlue" align="center"><fmtManfWOSummary:message key="LBL_NO_DIS"/>.</td></tr>
<%}} %>
	
</table>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>