<%
	/**********************************************************************************
	 * File		 		: GmManfDHRUpdate.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 1.0
	 * author			: Dhinakaran James
	 ************************************************************************************/
%>

<!-- manufact\GmManfDHRUpdate.jsp -->


<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%
String strManuFactJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_MANUFACT");
	try {
		GmServlet gm = new GmServlet();
		gm.checkSession(response, session);

		HashMap hmReturn = new HashMap();
		hmReturn = (HashMap) request.getAttribute("hmReturn");
		String strMode = (String) request.getAttribute("hMode") == null
				? ""
				: (String) request.getAttribute("hMode");
		// System.out.println(" Str Mode is " + strMode);

		ArrayList alReturnReasons = (ArrayList) request
				.getAttribute("hNCMRReason");
		HashMap hmDHRDetails = new HashMap();
		ArrayList alEmpList = new ArrayList();
		ArrayList alSubDHR = new ArrayList();

		String strShade = "";

		String strDHRId = "";
		String strVendorId = "";
		String strVendName = "";
		String strPartNum = "";
		String strDesc = "";
		String strWOId = "";
		String strControlNum = "";
		String strQtyRec = "";
		String strQtyInspected = "";
		String strQtyReject = "";
		String strActionTaken = "";
		String strCodeID = "";
		String strSelected = "";
		String strReceivedBy = "";
		String strInspectedBy = "";
		String strInspectedByNm = "";
		String strQtyPackaged = "";
		String strPackagedBy = "";
		String strPackagedByNm = "";
		String strQtyInShelf = "";
		String strVerifiedBy = "";
		String strVerifiedByNm = "";
		String strNCMRId = "";
		String strCriticalFl = "";
		String strCreatedDate = "";
		String strRejReason = "";
		String strReasonType = "";
		String strReasonTypeID = "";
		String strCloseQty = "";
		String strNCMRFl = "";
		String strReceivedTS = "";
		String strInspectedTS = "";
		String strPackagedTS = "";
		String strVerifiedTS = "";
		String strSubComFl = "";
		String strSterFl = "";
		String strChecked = "";
		String strLabelFl = "";
		String strPackFl = "";
		String strPartSterFl = "";
		String strFooter = "";
		String strDocRev = "";
		String strDocActiveFl = "";
		String strFARFl = "";
		String strBackOrderFl = "";
		String strNCMRAction = "";
		String strNCMRRemarks = "";
		String strNCMRAuthBy = "";
		String strNCMRAuthDt = "";
		String strDisabled = "";
		String strStatusFl = "";
		String strSessErrMsg = "";
		String strStatusErrFl = "NULL";
		String strFromPage = "";
		String strDHRVoidFl = "";
		String strQtyShipped = "";
		String strUOM = "";
		String strQtyRecd="0";
		String strrwfl = "";
		NumberFormat nf = NumberFormat.getInstance();
		double vTotalQty = 0;
		
		String strPortal = (String) request.getAttribute("hPortal") == null
				? ""
				: (String) request.getAttribute("hPortal");
		String strPOType = "";

		String strUserId = (String) session
				.getAttribute("strSessUserId");

		int intNCMRFl = 0;

		if (hmReturn != null) {
			hmDHRDetails = (HashMap) hmReturn.get("DHRDETAILS");
			alSubDHR = (ArrayList) hmReturn.get("SUBDHRDETAILS");

			strDHRId = GmCommonClass.parseNull((String) hmDHRDetails
					.get("ID"));
			strVendorId = GmCommonClass.parseNull((String) hmDHRDetails
					.get("VID"));
			strVendName = GmCommonClass.parseNull((String) hmDHRDetails
					.get("VNAME"));
			strPartNum = GmCommonClass.parseNull((String) hmDHRDetails
					.get("PNUM"));
			strDesc = GmCommonClass.parseNull((String) hmDHRDetails
					.get("PDESC"));
			strWOId = GmCommonClass.parseNull((String) hmDHRDetails
					.get("WOID"));
			strControlNum = GmCommonClass
					.parseNull((String) hmDHRDetails.get("CNUM"));
			strQtyRec = GmCommonClass.parseZero((String) hmDHRDetails
					.get("QTYREC"));
			strReceivedBy = GmCommonClass
					.parseNull((String) hmDHRDetails.get("UNAME"));
			strQtyInspected = GmCommonClass
					.parseNull((String) hmDHRDetails.get("QTYINSP"));
			strQtyReject = GmCommonClass
					.parseZero((String) hmDHRDetails.get("QTYREJ"));
			strActionTaken = GmCommonClass
					.parseNull((String) hmDHRDetails.get("ACTTKN"));
			strInspectedBy = GmCommonClass
					.parseNull((String) hmDHRDetails.get("INSPBY"));
			strInspectedByNm = GmCommonClass
					.parseNull((String) hmDHRDetails.get("INSPBYNM"));
			strPackagedBy = GmCommonClass
					.parseNull((String) hmDHRDetails.get("PACKBY"));
			strPackagedByNm = GmCommonClass
					.parseNull((String) hmDHRDetails.get("PACKBYNM"));
			strQtyPackaged = GmCommonClass
					.parseNull((String) hmDHRDetails.get("QTYPACK"));
			strQtyInShelf = GmCommonClass
					.parseNull((String) hmDHRDetails.get("QTYSHELF"));
			strVerifiedBy = GmCommonClass
					.parseNull((String) hmDHRDetails.get("VERIFYBY"));
			strVerifiedByNm = GmCommonClass
					.parseNull((String) hmDHRDetails.get("VERIFYBYNM"));
			strNCMRId = GmCommonClass.parseNull((String) hmDHRDetails
					.get("NCMRID"));
			strCriticalFl = GmCommonClass
					.parseNull((String) hmDHRDetails.get("CFL"));
			strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date) hmDHRDetails.get("CDATE"),strGCompDateFmt);
			strRejReason = GmCommonClass
					.parseNull((String) hmDHRDetails.get("REJREASON"));
			strReasonType = GmCommonClass
					.parseNull((String) hmDHRDetails.get("REASON_TYPE"));
			strReasonTypeID = GmCommonClass
					.parseNull((String) hmDHRDetails.get("REASON_ID"));
			strNCMRFl = GmCommonClass.parseZero((String) hmDHRDetails
					.get("NCMRFL"));
			strCloseQty = GmCommonClass.parseZero((String) hmDHRDetails
					.get("CLOSEQTY"));
			strReceivedTS = GmCommonClass
					.parseNull((String) hmDHRDetails.get("RECTS"));
			strInspectedTS = GmCommonClass
					.parseNull((String) hmDHRDetails.get("INSTS"));
			strPackagedTS = GmCommonClass
					.parseNull((String) hmDHRDetails.get("PACKTS"));
			strVerifiedTS = GmCommonClass
					.parseNull((String) hmDHRDetails.get("VERTS"));
			strSubComFl = GmCommonClass.parseNull((String) hmDHRDetails
					.get("SUBFL"));
			strSterFl = GmCommonClass.parseNull((String) hmDHRDetails
					.get("STERFL"));
			strChecked = GmCommonClass.parseNull((String) hmDHRDetails
					.get("SKIPLBLFL"));
			strNCMRAction = GmCommonClass
					.parseNull((String) hmDHRDetails.get("DISP"));
			strNCMRRemarks = GmCommonClass
					.parseNull((String) hmDHRDetails.get("DISPREMARKS"));
			strNCMRAuthBy = GmCommonClass
					.parseNull((String) hmDHRDetails.get("DISPBY"));
			strNCMRAuthDt = GmCommonClass.getStringFromDate((java.util.Date) hmDHRDetails.get("DISPDATE"),strGCompDateFmt);
			strBackOrderFl = GmCommonClass
					.parseNull((String) hmDHRDetails
							.get("BACKORDER_FLAG"));
			strStatusFl = GmCommonClass.parseNull((String) hmDHRDetails
					.get("SFL"));
			// strDHRVoidFl = GmCommonClass.parseNull((String)hmDHRDetails.get("DHRVFL");
			strUOM = GmCommonClass.parseNull((String) hmDHRDetails
					.get("UOMANDQTY"));
			strQtyShipped = GmCommonClass
					.parseNull((String) hmDHRDetails.get("SHIPQTY"));
			strPOType = GmCommonClass.parseNull((String) hmDHRDetails
					.get("POTYPE"));
			log.debug(" PO Type is " + strPOType);
			strLabelFl = strChecked.equals("1") ? "Yes" : "-";
			strChecked = strChecked.equals("1") ? "checked" : "";
			strPackFl = GmCommonClass.parseNull((String) hmDHRDetails
					.get("SKIPPACKFL"));
			strPackFl = strPackFl.equals("1") ? "Yes" : "-";
			
			strQtyRec = GmCommonClass.parseZero(strQtyRec);
			strQtyReject = GmCommonClass.parseZero(strQtyReject);
			strCloseQty = GmCommonClass.parseZero(strCloseQty);
			strrwfl = GmCommonClass.parseNull((String) hmDHRDetails.get("RWFL"));
					
			
			try {				
				vTotalQty = (Integer.parseInt(strQtyRec) - Integer.parseInt(strQtyReject) + Integer.parseInt(strCloseQty));
				nf = java.text.NumberFormat.getIntegerInstance();				
				strQtyRecd= ""+ nf.format(vTotalQty);			
			} catch (NumberFormatException nfe) {
				vTotalQty = (Double.parseDouble(strQtyRec) - Double.parseDouble(strQtyReject) + Double.parseDouble(strCloseQty));
				strQtyRecd= ""+ nf.format(vTotalQty);				
			}
			
			String strMod = "";
			strFromPage = (String) request.getAttribute("FromPage") == null
					? ""
					: (String) request.getAttribute("FromPage");

			if (strStatusFl.equals("0")) {
				strMod = "Q";
			} else if (strStatusFl.equals("1")) {
				strMod = "I";
			} else if (strStatusFl.equals("2")) {
				strMod = "P";
			} else if (strStatusFl.equals("3")) {
				strMod = "V";
			}

			if (!(strMod.equals(strMode))) {
				strSessErrMsg = "This data has already been filled. Please visit the DashBoard";
				strStatusErrFl = strMode.concat(strFromPage);
				strDisabled = "true";
				strMode = strMod;
			}

			strPartSterFl = GmCommonClass
					.parseNull((String) hmDHRDetails.get("PCLASS"));
			strPartSterFl = strPartSterFl.equals("4030") ? "1" : "";

			if (!strFooter.equals("")) {
				String strArr[] = strFooter.split("\\^");
				strFooter = strArr[0];
				strDocRev = strArr[1];
				strDocActiveFl = strArr[2];
			}

			strFARFl = GmCommonClass.parseNull((String) hmDHRDetails
					.get("FARFL"));

			intNCMRFl = Integer.parseInt(strNCMRFl);

			alEmpList = (ArrayList) hmReturn.get("EMPLIST");
		}
		String strXmlData = GmCommonClass.parseNull((String) request
				.getAttribute("XMLDATA"));
		
		int intSize = 0;
		HashMap hcboVal = null;

		Calendar cal = Calendar.getInstance(TimeZone.getDefault());
		String DATE_FORMAT = "MM/dd/yyyy";
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				DATE_FORMAT);
		sdf.setTimeZone(TimeZone.getDefault());
		String strDate = sdf.format(cal.getTime());
%>

<%@page import="java.text.NumberFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.util.Calendar"%><HTML>
<HEAD>
<TITLE>Globus Medical: DHR Update</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strManuFactJsPath%>/GmManfDHRUpdate.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>
<script>

var objGridData = '<%=strXmlData%>';
var gridObj;
var	InspectedBY = '<%=strInspectedBy%>' ;
var	PackedBY = '<%=strPackagedBy%>' ;
var	VerifiedBy = '<%=strUserId%>' ;
var gStrNCMRFl = '<%=strNCMRFl%>';

var mode = '<%=strMode%>';
var strServletPath = '<%=strServletPath%>';
var strDhrId = '<%=strDHRId%>';
var strVendorId = '<%=strVendorId%>';
var srtWoId = '<%=strWOId%>';
var strDocRev = '<%=strDocRev%>';
var strDocActiveFl= '<%=strDocActiveFl%>';
var strNcmrId = '<%=strNCMRId%>';
var strDate = '<%=strDate%>';
var qtyPackages = '<%=strQtyRec%>';
var strrwfl = '<%=strrwfl%>';
var strLotNum = '<%=strControlNum%>';
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<FORM name="frmOrder" method="POST"><input type="hidden"
	name="hDHRId" value="<%=strDHRId%>"> <input type="hidden"
	name="hAction" value="<%=strMode%>"> <input type="hidden"
	name="hPartNum" value="<%=strPartNum%>"> <input type="hidden"
	name="hWOId" value="<%=strWOId%>"> <input type="hidden"
	name="hRecQty" value="<%=strQtyRec%>"> <input type="hidden"
	name="hRejQty" value="<%=strQtyReject%>"> <input type="hidden"
	name="hInputString" value=""> <input type="hidden"
	name="hSterFl" value="1"> <input type="hidden"
	name="hPartSterFl" value="<%=strPartSterFl%>"> <input
	type="hidden" name="hNCMRFl" value="<%=intNCMRFl%>"> <input
	type="hidden" name="hStatusFl" value="<%=strStatusFl%>"> <input
	type="hidden" name="hPortal" value="<%=strPortal%>"> <input
	type="hidden" name="hInpStr">
<table border="0"  class="DtTable700" cellspacing="0" cellpadding="0" >
	<tr>
		<td bgcolor="#666666" colspan="3"></td>
	</tr>
	<tr>
		<td bgcolor="#666666" width="1" rowspan="4"></td>
		<td height="25" class="RightDashBoardHeader">Manf. DHR Record -
		Update</td>
		<td bgcolor="#666666" width="1" rowspan="4"></td>
	</tr>
	<td bgcolor="#666666" colspan="3"></td>
	<tr>
		<td width="698" height="100" valign="top">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<%
				if (strCriticalFl.equals("Y")) {
			%>
			<tr>
				<td colspan="4" class="RightTextRed" align="center" HEIGHT="24">
				This Part has been flagged as <b>Critical</b> and requires immediate
				processing. <BR>
				Please inform Product Engineer / Logistics Manager. <script>
							alert("This part has been flagged as Critical and requires immediate processing.");
						</script></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<%
				}
					if (strFARFl.equals("Y")) {
			%>
			<tr>
				<td colspan="4" class="RightTextRed" align="center" HEIGHT="24">
				This Part has been flagged as a <b>First Article</b> and requires
				immediate processing. <BR>
				Please inform Product Engineer / Operations Manager. <script>
							alert("This part has been flagged as First Article and requires immediate processing.");
						</script></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<%
				}
					if (strBackOrderFl.equals("Y")) {
			%>
			<tr>
				<td colspan="4" class="RightTextRed" align="center" HEIGHT="24">
				This Part has been flagged as a <b>Back Ordered Item</b> and
				requires immediate processing. <BR>
				Please inform Customer Service. <script>
							alert("This part has been flagged as Back Order and requires immediate processing.");
						</script></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<%
				}
			%>
			<tr>
				<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" align="right" HEIGHT="24">Part Number:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightText" align="right" HEIGHT="24">Description:</td>
						<td class="RightText" colspan="3">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
					</tr>
					<tr>
						<td colspan="4" bgcolor="#eeeeee"></td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24">Lot Number:</td>
						<td class="RightText">&nbsp;<%=strControlNum%></td>
						<td class="RightText" align="right" HEIGHT="24">Globus Work
						Order:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strWOId%></td>
					</tr>
					<tr>
						<td colspan="4" bgcolor="#eeeeee"></td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24">Created Date:</td>
						<td class="RightText">&nbsp;<%=strCreatedDate%></td>
						<td class="RightText" align="right" HEIGHT="24"></td>
						<td class="RightText" colspan="3">&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#cccccc"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18"
					colspan="4">&nbsp;Sub-Assembly Details
			</tr>
			<%
				if (strSubComFl.equals("1")) {
			%>
			<tr>
				<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" HEIGHT="18">&nbsp;Sub-Comp ID</td>
						<td class="RightText" width="350">Description</td>
						<td class="RightText">Control Number</td>
					</tr>
					<tr>
						<td colspan="4" bgcolor="#eeeeee"></td>
					</tr>
					<%
						intSize = alSubDHR.size();
								hcboVal = new HashMap();
								String strSubDHRId = "";
								String strSubCompId = "";
								String strSubDesc = "";
								String strControl = "";

								for (int s = 0; s < intSize; s++) {
									hcboVal = (HashMap) alSubDHR.get(s);
									strSubDHRId = GmCommonClass.parseNull((String) hcboVal
											.get("SUBDHRID"));
									strSubCompId = GmCommonClass.parseNull((String) hcboVal
											.get("SUBASMBID"));
									strSubDesc = GmCommonClass.parseNull((String) hcboVal
											.get("PDESC"));
									strControl = GmCommonClass.parseNull((String) hcboVal
											.get("CNUM"));
					%>
					<tr>
						<td class="RightText" HEIGHT="18">&nbsp;&nbsp;<%=strSubCompId%></td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strSubDesc)%></td>
						<%
							if (strMode.equals("C")) {
						%>
						<td class="RightText">&nbsp;<input name="Txt_CNum<%=s%>"
							value="<%=strControl%>" type="text" size="9"
							onFocus="changeBgColor(this,'#AACCE8');"
							onBlur="changeBgColor(this,'#ffffff');" class="InputArea">
						<input type="hidden" name="hSubDHR<%=s%>" value="<%=strSubDHRId%>">
						<input type="hidden" name="hSubComp<%=s%>"
							value="<%=strSubCompId%>"></td>
						<%
							} else {
						%>
						<td class="RightText">&nbsp;<%=strControl%></td>
						<%
							}
						%>
					</tr>
					<%
						}
					%>
				</table>
				<input type="hidden" name="hSubCnt" value="<%=intSize%>"></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<%
				} else {
			%>
			<tr>
				<td colspan="4" class="RightText" height="30" align="center">Not
				Applicable</td>
			</tr>
			<%
				}
			%>
			<tr>
				<td colspan="4" bgcolor="#cccccc"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18"
					colspan="4">&nbsp;Manufacturing</td>
			</tr>
			<tr>
				<td class="RightText" width="170" align="right" HEIGHT="24">Qty
				Initiated:</td>
				<td class="RightText" width="100">&nbsp;<%=strQtyShipped%></td>
				<td class="RightText" align="right">Initiated Timestamp:</td>
				<td class="RightText">&nbsp;<%=strReceivedTS%></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<tr>
				<td class="RightText" align="right">&nbsp;Qty Manufactured:</td>
				<td class="RightText">&nbsp;<%=strQtyRec%></td>
				<td class="RightText" HEIGHT="24" align="right">&nbsp;Initiated
				by:</td>
				<td class="RightText">&nbsp;<%=strReceivedBy%></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#cccccc"></td>
			</tr>
			<%
				if (strMode.equals("I")) {
			%>
			<tr>
				<td class="RightTableCaption" bgcolor="#9AADFC" HEIGHT="18"
					colspan="4">&nbsp;Inspection</td>
			</tr>
			<td class="RightText" align="right" HEIGHT="24">Qty Inspected:</td>
			<td class="RightText">&nbsp;<input type="text" size="4"
				value="<%=strQtyInspected%>"
				onFocus="changeBgColor(this,'#AACCE8');"
				onBlur="changeBgColor(this,'#ffffff');" class="InputArea"
				name="Txt_QtyInsp"></td>
			<td class="RightText" align="right">&nbsp;Inspected by:</td>
			<td class="RightText">&nbsp; <input type="hidden"
				name="Cbo_InspEmp" value="<%=strUserId%>"> <%
 	intSize = alEmpList.size();
 			hcboVal = new HashMap();
 			for (int i = 0; i < intSize; i++) {
 				hcboVal = (HashMap) alEmpList.get(i);
 				strCodeID = (String) hcboVal.get("ID");
 				if (strUserId.equals(strCodeID)) {
 					out.println(hcboVal.get("NAME"));
 					break;
 				}
 			}
 %>
			</td>
			<%
				} else {
						if (strStatusErrFl.equals("IDashboardDHR")) {
			%>
			<tr>
				<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18"
					colspan="4">&nbsp;Inspection</td>
			</tr>
			<tr>
				<td class="RightTextRed" align="center" HEIGHT="24" colspan="4">
				<%=strSessErrMsg%></td>
			</tr>
			<%
				} else {
			%>
			<tr>
				<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18"
					colspan="4">&nbsp;Inspection</td>
			</tr>
			<%
				}
			%>
			<td class="RightText" align="right" HEIGHT="24">Qty Inspected:</td>
			<td class="RightText">&nbsp;<%=strQtyInspected%></td>
			<td class="RightText" align="right">&nbsp;Inspected by:</td>
			<td class="RightText">&nbsp;<%=strInspectedByNm%></td>
			<%
				}
			%>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="24">Inspected
				Timestamp:</td>
				<td class="RightText">&nbsp;<%=strInspectedTS%></td>
				<td class="RightText" align="right" HEIGHT="24">DHR Approved
				by:</td>
				<td class="RightText"></td>
			</tr>

			<tr>
				<td colspan="4" bgcolor="#cccccc"></td>
			</tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="24">Qty Rejected:</td>
				<%
					if (strMode.equals("I")) {
				%>
				<td class="RightText">&nbsp;<input type="text"
					value="<%=strQtyReject%>" size="4"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');" class="InputArea"
					name="Txt_QtyRej"></td>
				<%
					} else {
				%>
				<td class="RightText">&nbsp;<%=strQtyReject%></td>
				<%
					}
				%>
				<td class="RightText" align="right" HEIGHT="24">Raised NCMR Id:</td>
				<td class="RightText">&nbsp;<%=strNCMRId%></td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#eeeeee"></td>
			</tr>
			<td class="RightText" align="right" HEIGHT="24">Rejection Type:</td>
			<%
				if (strMode.equals("I")) {
			%>
			<td class="RightText">&nbsp;<select name="Cbo_ReasonType"
				id="Region" class="RightText">
				<option value="0">[Choose One] <%
					intSize = alReturnReasons.size();
							hcboVal = new HashMap();
							for (int i = 0; i < intSize; i++) {
								hcboVal = (HashMap) alReturnReasons.get(i);
								strCodeID = (String) hcboVal.get("CODEID");
								strSelected = strReasonTypeID.equals(strCodeID)
										? "selected"
										: "";
				%>
				<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
				<%
					}
				%>
				
			</select></td>
			<%
				} else {
			%>
			<td class="RightText" colspan="3">&nbsp;<%=strReasonType%></td>
			<%
				}
			%>


			<tr>
				<td colspan="2" bgcolor="#eeeeee"></td>
			</tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="24">Reason for
				Rejection:</td>
				<%
					if (strMode.equals("I")) {
				%>
				<td class="RightText" colspan="3">&nbsp;<textarea
					name="Txt_RejReason" class="InputArea"
					onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');" rows=4 cols=45 value=""><%=strRejReason%></textarea></td>
				<%
					} else {
				%>
				<td class="RightText" colspan="3">&nbsp;<%=strRejReason%></td>
				<%
					}
				%>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<%			
				if ((intNCMRFl >0 && intNCMRFl < 2) || (strNCMRId.startsWith("GM-EVAL") && intNCMRFl == 0 ) ) {
					if(!strQtyRec.equals(strQtyReject)){
						if (!strQtyReject.equals("0")){
						strDisabled = "true";
						}
			%>
			<tr>
				<td class="RightTextRed" align="center" HEIGHT="24" colspan="4">This
				DHR has an associated NCMR that has to be closed (Disposition
				Details) before proceeding with this DHR. <br>
				Please contact Quality Inspector/QA Director</td>
			</tr>
			<%
					}
				}
			%>
			<tr>
				<td class="RightTableCaption" colspan="4" HEIGHT="24">&nbsp;&nbsp;<u>NCMR
				Disposition Details</u></td>
			</tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="22">Action Taken:</td>
				<td class="RightText">&nbsp;<%=strNCMRAction%></td>
				<td class="RightText" align="right" valign="top" HEIGHT="22">Remarks:</td>
				<td class="RightText">&nbsp;<%=strNCMRRemarks%></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="22">Authorized By:</td>
				<td class="RightText">&nbsp;<%=strNCMRAuthBy%></td>
				<td class="RightText" align="right" HEIGHT="22">Date:</td>
				<td class="RightText">&nbsp;<%=strNCMRAuthDt%></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="22">NCMR Closeout
				Qty:</td>
				<td class="RightText" colspan="3">&nbsp;<%=strCloseQty%> <input
					type="hidden" name="hCloseQty" value="<%=strCloseQty%>"></td>
			</tr>

			<tr>
				<td colspan="4" bgcolor="#cccccc"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" bgcolor="#9AADFC" HEIGHT="18"
					colspan="4">&nbsp;Pending Processing</td>
			</tr>
			<tr>
				<td colspan="4">
				<div id="dataGridDiv" class="grid" style="height: 150px;"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#cccccc"></td>
			</tr>
			<%
				if (strMode.equals("P")) {
			%>
			<tr>
				<td class="RightTableCaption" align="right" HEIGHT="24">Skip
				Labeling ?:&nbsp; <input
					type="checkbox" <%=strChecked%> name="Chk_Skip"></td>
				<td class="RightTableCaption" align="right" width="250">&nbsp;Processed
				By:&nbsp; <input type="hidden"
					name="Cbo_Emp" value="<%=strUserId%>"> <%
 	intSize = alEmpList.size();
 			hcboVal = new HashMap();
 			for (int i = 0; i < intSize; i++) {
 				hcboVal = (HashMap) alEmpList.get(i);
 				strCodeID = (String) hcboVal.get("ID");
 				if (strUserId.equals(strCodeID)) {
 					out.println(hcboVal.get("NAME"));
 					break;
 				}
 			}
 			%>
 			</td>
			</tr>
			<%
 		} else {
 %>

				<tr>
					<td class="RightTableCaption" align="right" width="250">Processed
					By:</td>
					<td class="RightText" width="170">&nbsp;<%=strPackagedByNm%></td>
					<td class="RightTableCaption" align="right" HEIGHT="24">Skip
					Labeling ?:</td>
					<td class="RightText" HEIGHT="24">&nbsp;<%=strLabelFl%></td>
				</tr>
				<%
					}
						log.debug(" strMode is " + strMode + " strPOType is "
								+ strPOType);
						if (strMode.equals("V")) {
							if (strPOType.equals("3104")) {
				%>
				<tr>
					<td class="RightTextRed" align="center" HEIGHT="24" colspan="4">
					Verification process should happen at the store room</td>
				</tr>
				<%
					} else {
				%>
				
			<tr>
				<td colspan="4" bgcolor="#cccccc"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" bgcolor="#9AADFC" HEIGHT="18"
					colspan="4">&nbsp;Pending Verification</td>
			</tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="24">Qty to Verify:</td>
				<td class="RightText">&nbsp;<input type="text" size="4"
					value="<%=strQtyRecd%>" onFocus="changeBgColor(this,'#AACCE8');"
					onBlur="changeBgColor(this,'#ffffff');" class="InputArea"
					name="Txt_Qty"></td>
				<td class="RightText" align="right">Verified By:</td>
				<td class="RightText">&nbsp; <input type="hidden"
					name="Cbo_Emp" value="<%=strUserId%>"> <%
 	intSize = alEmpList.size();
 				hcboVal = new HashMap();
 				for (int i = 0; i < intSize; i++) {
 					hcboVal = (HashMap) alEmpList.get(i);
 					strCodeID = (String) hcboVal.get("ID");
 					if (strUserId.equals(strCodeID)) {
 						out.println(hcboVal.get("NAME"));
 						break;
 					}
 				}
 %>
				</td>
			</tr><%-- 
			<tr>
				<td colspan="4" class="RightTextBlue" align="center">Note:
				Please deduct the Qty taken as QC Samples and then enter actual Qty</td>
			</tr>--%>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<%
				}
					} else {
			%>
			<tr>
				<td colspan="4" bgcolor="#cccccc"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18"
					colspan="4">&nbsp;Pending Verification</td>
			</tr>
			<%
				if (strStatusErrFl.equals("PDashboardDHR")) {
			%>
			<tr>
				<td class="RightTextRed" align="center" HEIGHT="24" colspan="4">
				<%=strSessErrMsg%></td>
			</tr>
			<%
				}
			%>
			<%
				if (strStatusErrFl.equals("PDashboardDHR")
								|| strStatusFl.equals("4")) {
			%>
			<tr>
				<td class="RightText" align="right" HEIGHT="24">Qty Verified</td>
				<td class="RightText">&nbsp;<%=strQtyInShelf%></td>
				<td class="RightText" align="right">Verified By:</td>
				<td class="RightText">&nbsp;<%=strVerifiedByNm%></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<tr>
				<td class="RightText" align="right" HEIGHT="24">Verified
				Timestamp:</td>
				<td class="RightText" colspan="3">&nbsp;<%=strVerifiedTS%></td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
			<%
				}
			%>
			<%
				//	}
					}
			%>

			<tr>
				<td colspan="4" bgcolor="#eeeeee"></td>
			</tr>
		</table>
		</td>
	</tr>
	<td colspan="3"  bgcolor="#666666"></td>
	</tr>

</table>

<table border="0" width="700" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center" height="30" id="button">
		<%
			if (strMode.equals("Load")) {
		%> <gmjsp:button value="&nbsp;Return&nbsp;" name="Btn_Return"
			gmClass="button" buttonType="Save" onClick="history.go(-1)"/>&nbsp;&nbsp; <%
 	} else if (strMode.equals("C") || strMode.equals("I")
 				|| strMode.equals("P") || strMode.equals("V")) {
 			if (((strStatusErrFl.substring(0, 1)).equals("I") && !strNCMRId
 					.equals(""))) {
 %> <gmjsp:button value="Print NCMR" name="Btn_ViewPrint"
			gmClass="button" buttonType="Load" onClick="fnViewPrintNCMR();"/>&nbsp;&nbsp; <%
 	} else {
 %> <gmjsp:button value="Update" name="Btn_Update"
			disabled="<%=strDisabled%>" gmClass="button" buttonType="Save" onClick="fnUpdate();"/>&nbsp;&nbsp;
		<%
			}
				}
		%> <gmjsp:button value="Print DHR" name="Btn_ViewPrint"
			gmClass="button" buttonType="Load" onClick="fnViewPrint();"/>&nbsp;&nbsp; <!--<gmjsp:button value="Print WO" name="Btn_ViewPrint" gmClass="button" buttonType="Load" onClick="fnViewPrintWO();" />&nbsp;&nbsp;-->
		
		<%
	
			if((strMode.equals("V")|| strVerifiedByNm!="") && vTotalQty > 0){
		%>
			<gmjsp:button value="Print All" name="Btn_ViewPrintAll" gmClass="button" buttonType="Load" onClick="fnViewPrintAll();" />&nbsp;&nbsp;
		<%} %>
		</td>
		<tr>
</table>
</FORM>
<%
	} catch (Exception e) {
		e.printStackTrace();
	}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
