 <%@page import="com.globus.common.beans.GmJasperReport"%>
 <%
/**********************************************************************************
 * File		 		: GmMFGDhrPrint.jsp
 * Desc		 		: This screen is used for the printing DHR sheets for the Manufacturing process
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!-- manufact\GmMFGDhrPrint.jsp -->


<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%
HashMap hmReturn = new HashMap();
hmReturn = (HashMap)request.getAttribute("hmReturn");
HashMap hmDHRDetails = new HashMap();

String strDRFileName="";
String strPTFileName="";
String strPTFooter="";
String strUser = "";
String strPartNum ="";
String strConNum ="";
String strProjId ="";
String strManufDate ="";
String strExpDate ="";
String strDataMatrix ="";

if (hmReturn != null)
{
	hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
	strDRFileName = GmCommonClass.parseNull ((String)hmDHRDetails.get("RFFILENAME"));
	strPTFileName = GmCommonClass.parseNull ((String)hmDHRDetails.get("PRTFILENAME"));
}

strUser = (String)hmDHRDetails.get("CUSER");
strUser = strUser.concat(".gif");
hmDHRDetails.put("IMGPATH",strImagePath);
hmDHRDetails.put("USERIMG",strUser);

strPartNum = GmCommonClass.parseNull ((String)hmDHRDetails.get("PNUM"));
strConNum = GmCommonClass.parseNull ((String)hmDHRDetails.get("CNUM"));
strProjId = GmCommonClass.parseNull ((String)hmDHRDetails.get("PROJID"));
strManufDate = GmCommonClass.getStringFromDate((java.util.Date) hmDHRDetails.get("MDATE"), strGCompDateFmt);
strExpDate = GmCommonClass.getStringFromDate((java.util.Date) hmDHRDetails.get("EXPDT"), strGCompDateFmt);
strDataMatrix = strPartNum+"^"+strConNum+"^"+strProjId+"^"+strManufDate+"^"+strExpDate;
hmDHRDetails.put("DATAMATRIX",strDataMatrix);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Mfg. DHR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hReviewForm" value="<%=strDRFileName%>">
<input type="hidden" name="hProductTraveler" value="<%=strPTFileName%>">
     <BR>
	 <BR>
	 
	 <% String strHtmlJasperRpt = "";	
	 if (strDRFileName.contains(".jasper"))
{ 
	 		 GmJasperReport gmJasperReport = new GmJasperReport();								
				gmJasperReport.setRequest(request);
				gmJasperReport.setResponse(response);
				gmJasperReport.setJasperReportName(strDRFileName);
				gmJasperReport.setHmReportParameters(hmDHRDetails);
				gmJasperReport.setReportDataList(null);
				gmJasperReport.setBlDisplayImage(true);
				/* gmJasperReport.setPageHeight(791);   */ //PMT-30557 : (Page height avoid blank page at end of report);
				// to set the dynamic resource bundle object
		        strHtmlJasperRpt = gmJasperReport.getHtmlReport();

			%>				
			<%=strHtmlJasperRpt %>
			
	  <%}else if (!strDRFileName.equals(""))
{ 
	strDRFileName = "/manufact/"+strDRFileName;
%>
   		<jsp:include page="<%=strDRFileName%>" >
		<jsp:param name="HideButton" value="Y"/>
		</jsp:include> 
		<%}else
{ %>
              <jsp:include page="/manufact/GmMFGDHRPrintReviewForm0.jsp" >
              <jsp:param name="HideButton" value="Y"/>
			  </jsp:include>

<%}
%>

<%
 	// to avoid print product travaler (while jasper)
	 if (!strPTFileName.contains(".jasper"))
	{ 
%>
	 <p STYLE="page-break-after: always"></p>
	 <BR>
	 <BR>
			
	   <% if (!strPTFileName.equals(""))
{ 
		   strPTFileName = "/manufact/"+strPTFileName;  
%>
		<jsp:include page="<%=strPTFileName%>" >
		<jsp:param name="HideButton" value="Y"/>
		</jsp:include>
				<%}else
{ %>
              <jsp:include page="/manufact/GmMFGDHRPrintTraveler0.jsp" >
              <jsp:param name="HideButton" value="Y"/>
			  </jsp:include>

<%}
	 
}
%>
	<BR>
	<BR> 
	<div id="button">
	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30">
			<BR>
			<BR>
			<gmjsp:button value="&nbsp;Print&nbsp;" name="Btn_Print" gmClass="button" buttonType="Load" onClick="fnPrint();"/>&nbsp;&nbsp;
			<gmjsp:button value="&nbsp;Close&nbsp;" name="Btn_Close" gmClass="button" buttonType="Load" onClick="window.close();"/>&nbsp;
			</td>
		<tr>
	</table>
	</div>
</FORM>

</BODY>

</HTML>
