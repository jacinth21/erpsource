




<%
	/**********************************************************************************
	 * File		 		: GmManufEditCost.jsp
	 * Desc		 		: This screen is used for the 
	 * Version	 		: 
	 * author			: 
	 ************************************************************************************/
%>


<!-- manufact\GmManufEditCost.jsp -->


<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.beans.GmCommonControls"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%
String strManuFactJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_MANUFACT");
	String strWikiTitle = GmCommonClass.getWikiTitle("MANUF_COST_EDIT");
%>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strManuFactJsPath%>/GmManufEditCost.js"></script>
<HTML>
<HEAD>
<TITLE>GlobusOne Enterprise Portal: Manufacturing Edit Cost</TITLE>
</HEAD>

<BODY leftmargin="20" topmargin="10" onkeyup="fnEnter();" onload="fnOnPageLoad();">
	<html:form action="/gmManufCost.do?method=fetchAccountActivityRpt">
		<html:hidden property="strOpt" value="" />
		<html:hidden property="partDes" />
		<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader">Manufacturing Edit Cost</td>
				<td align="right" class="RightDashBoardHeader"><img
					id='imgEdit' align="right" style='cursor: hand'
					src='<%=strImagePath%>/help.gif' title='Help' width='16'
					height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<tr>
				<td class="Line" colspan="2"></td>
			</tr>
			<tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
				<td height="30" class="RightTableCaption" align="right"><gmjsp:label
							type="MandatoryText" SFLblControlName="Part #:" td="false" />
				</td>
				<td>&nbsp;<html:text property="pnum"></html:text> &nbsp;&nbsp;
					<gmjsp:button value="&nbsp;Load&nbsp;" gmClass="button" buttonType="Load"
					onClick="fnLoad();"/> &nbsp;&nbsp;&nbsp; <logic:equal
						name="frmManufCost" property="historyfl" value="Y">
						<img id="imgEdit" style="cursor: hand"
							onclick="javascript:fnPartCostHistory();"
							title="Click to open Part history"
							src="<%=strImagePath%>/icon_History.gif" align="bottom" height=18
							width=18 />&nbsp;</logic:equal></td>

			</tr>
			<tr>
				<td class="Line" colspan="2"></td>
			</tr>
			<tr class="Shade">
				<td height="25" class="RightTableCaption" align="right">Part Description:</td>
				<td>&nbsp;<span id="partDesc"></span>
				</td>
			</tr>
			<tr>
				<td class="Line" colspan="2"></td>
			</tr>
			<tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
				<td height="25" class="RightTableCaption" align="right"><gmjsp:label
						type="RegularText" SFLblControlName="Cost:" td="false" /></td>
				<td>&nbsp;<html:text property="cost" onkeypress="return isNumberKey(event);"></html:text></td>
			</tr>
			<tr>
				<td class="Line" colspan="2"></td>
			</tr>
			<tr>
				<td height="30" align="center" colspan="2"><gmjsp:button
					value="&nbsp;Submit&nbsp;" gmClass="button" buttonType="Save" onClick="fnSubmit();"/>
					&nbsp;&nbsp;<gmjsp:button value="&nbsp;Reset&nbsp;"
					gmClass="button" buttonType="Save" onClick="fnReset();"/>
				</td>
			</tr>
			<logic:notEmpty name="frmManufCost" property="strSuccess">
				<tr>
					<td class="Line" colspan="2"></td>
				</tr>
				<tr>
					<td height="30" align="center" colspan="2">
					 <font color="green"><b><bean:write name="frmManufCost" property="strSuccess" /></b></font></td>
				</tr>
			</logic:notEmpty>

		</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
