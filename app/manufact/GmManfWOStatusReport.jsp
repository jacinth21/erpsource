<%
/**********************************************************************************
 * File		 		: GmManfWOStatusReport.jsp
 * Desc		 		: This screen is used for the displaying Work Order Status
 * Version	 		: 1.0
 * author			: d James
************************************************************************************/
%>

<!-- manufact\GmManfWOStatusReport.jsp -->


<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>


<%
	String strApplDateFmt = strGCompDateFmt;
	String strDTDateFmt = "{0,date,"+strApplDateFmt+"}";
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Work Order Summary </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmMfg" method="POST" action="<%=strServletPath%>/GmMFGMaterialReconcServlet">
<input type="hidden" name="hAction" value="LoadWOSum">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;Work Order Status Report</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="100%" height="30" valign="top" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
						<display:table name="alResult"  class = "its" id="currentRowObject"> 
							<display:column property="WID" title="Work Order" class="alignleft" sortable="true" />
							<display:column property="PNUM" title="Part Number" class="alignleft" sortable="true" />
							<display:column property="PDESC" title="Description" class="alignleft" />
						  	<display:column property="QTYORD" title="Qty<br>Ordered" class="alignright" sortable="true" />
							<display:column property="COST" title="Cost" class="alignright"  sortable="true" format="${0,number,#,###.00}" />
							<display:column property="WDATE" title="WO Date" class="alignleft" sortable="true" format="<%=strDTDateFmt%>"/>
						  	<display:column property="QTYPEND" title="Qty<br>Pending" class="alignright" sortable="true"  />
						  	<display:column property="QTYWIP" title="Qty<br>in DHR" class="alignright" />
						</display:table>
						</td>
					</tr>
			    </table>
			</td>
		</tr>
    <table>
</BODY>
</HTML>