 <%
/**********************************************************************************
 * File		 		: GmMFGDHRPrintTraveler0.jsp
 * Desc		 		: This screen is used for the printing DHR sheets for the Manufacturing process
 * Version	 		: 1.0
 * author			: Karthik
************************************************************************************/
%>

<!-- manufact\GmMFGDHRPrintTraveler0.jsp -->



<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmDHRDetails = new HashMap();
	ArrayList alSubDHR = new ArrayList();

	String strShade = "";

	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyRej = "";
	String strManufDate = "";
	String strUserName = "";
	String strCreatedDate = "";
	String strQtyOrdered = "";
	String strProjId = "";
	String strPackPrim = "";
	String strPackSec = "";
	String strLabel = "";
	String strInsert = "";
	String strSubPartNum = "";
	String strSubPartDesc = "";
	String strSubControl = "";
	String strSubManfDt = "";
	String strSterFl = "";
	String strApplDateFmt = strGCompDateFmt;
	String strCompanyLogo = "";
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		strCompanyLogo = GmCommonClass.parseNull((String) hmReturn.get("LOGO"));

		strDHRId = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId = GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName = GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		strManufDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("MDATE"),strApplDateFmt);
		strUserName = GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplDateFmt);
		strQtyOrdered = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYORD"));
		strProjId = GmCommonClass.parseNull((String)hmDHRDetails.get("PROJID"));
		strPackPrim = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKPRIM"));
		strPackSec = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKSEC"));
		strInsert = GmCommonClass.parseNull((String)hmDHRDetails.get("INSID"));
		strQtyRej = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ"));
		strLabel = strPartNum.replaceAll("\\.","");
		strSterFl = GmCommonClass.parseNull((String)hmDHRDetails.get("STERFL"));
		
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Mfg. DHR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hPOId" value="<%=strDHRId%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="hAction" value="">


	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1" bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+2>In-House Manufacturing <br>Product Traveler&nbsp;</font></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="4"></td></tr>
		<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
		<tr>
			<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td  height="30" class="RightText" align="right" colspan="3"><b>DHR Id:</b></td>
						<td class="RightText">&nbsp;<%=strDHRId%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right">Originator:</td>
						<td class="RightText">&nbsp;<%=strUserName%></td>
						<td class="RightText" align="right">Date:</td>
						<td class="RightText">&nbsp;<%=strCreatedDate%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right">Part Number:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightText" align="right">Description:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right">Lot Number:</td>
						<td class="RightText">&nbsp;<%=strControlNum%></td>
						<td class="RightText" align="right">Work Order ID:</td>
						<td class="RightText">&nbsp;<%=strWOId%></td>
					</tr>
					<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
				</table>
			</td>
		</tr>
	</table>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b>ISSUE MATERIALS</b>&nbsp;(verify Bill of Materials is complete & approved)</td></tr>
		<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="150" align="right">Bill of Materials #:</td>
			<td class="RightText" width="50">&nbsp;</td>
			<td class="RightText" align="right" width="200">Signature:</td>
			<td class="RightText" width="100">&nbsp;</td>
		</tr>
		<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="150" align="right">Issue Date:</td>
			<td class="RightText">&nbsp;</td>
			<td class="RightText" width="200" align="right">Date:</td>
			<td class="RightText" >&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
	</TABLE>

	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b>INSPECTION</b>&nbsp;(verify Inspection Form is complete & approved)</td></tr>
		<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="150">&nbsp;&nbsp;&nbsp;<input type="checkbox">Pass</td>
			<td class="RightText" width="50">&nbsp;</td>
			<td class="RightText" align="right" width="200">Signature:</td>
			<td class="RightText" width="100">&nbsp;</td>
		</tr>
		<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
		<tr>
			<td class="RightText" height="23">&nbsp;&nbsp;&nbsp;<input type="checkbox">Fail (NCMR #)_____</td>
			<td class="RightText">&nbsp;</td>
			<td class="RightText" width="200" align="right">Date:</td>
			<td class="RightText" >&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
	</table>
	<BR>

	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b>LABELING</b>&nbsp;(verify Label Reconciliation Form is complete & approved)</td></tr>
		<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="150">&nbsp;&nbsp;&nbsp;<input type="checkbox">Yes</td>
			<td class="RightText" width="50">&nbsp;</td>
			<td class="RightText" align="right" width="200">Signature:</td>
			<td class="RightText" width="100">&nbsp;</td>
		</tr>
		<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="150">&nbsp;&nbsp;&nbsp;<input type="checkbox">No</td>
			<td class="RightText">&nbsp;</td>
			<td class="RightText" width="200" align="right">Date:</td>
			<td class="RightText" >&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
	</table>
	<BR>

	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b>STERILIZATION</b>&nbsp;(attach radiation certification form)</td></tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="300" colspan="2">&nbsp;&nbsp;&nbsp;<input type="checkbox">Gamma Sterilization (DOSE:_______)</td>
			<td class="RightText" align="right" width="200">Signature:</td>
			<td class="RightText" width="90">&nbsp;</td>
		</tr>
		<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="150">&nbsp;&nbsp;&nbsp;<input type="checkbox">Aseptic Processing</td>
			<td class="RightText">&nbsp;</td>
			<td class="RightText" width="200" align="right">Date:</td>
			<td class="RightText" >&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>		
	</table>
	<BR>
	
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="12" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="12" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b>UPDATE DHR / FINAL INSPECTION</b>&nbsp;(verify DHR Review Form is complete & approved)</td></tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="150" align="right">Date DHR Completed:</td>
			<td class="RightText" width="50">&nbsp;</td>
			<td class="RightText" align="right" width="200">Signature:</td>
			<td class="RightText" width="100">&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>
		<tr class="RightTableCaption">
			<td height="23" colspan="4">&nbsp;Placed in QC Finished Goods Inventory</td>
		</tr>		
		<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="150">&nbsp;&nbsp;&nbsp;<input type="checkbox">Yes&nbsp;&nbsp;(Qty:____)</td>
			<td class="RightText" width="50">&nbsp;</td>
			<td class="RightText" align="right" width="200">Date:</td>
			<td class="RightText" width="100">&nbsp;</td>
		</tr>		
		<tr>
			<td class="RightText" height="23" colspan="4">&nbsp;&nbsp;&nbsp;<input type="checkbox">No&nbsp;&nbsp;(Placed directly in warehouse inventory)</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>		
		<tr>
			<td class="RightText" height="23" width="150" align="right">Quantity Accepted:</td>
			<td class="RightText">&nbsp;</td>
			<td class="RightText" width="200" align="right">Quantity Rejected:</td>
			<td class="RightText" >&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>
	</table>
	<BR>

	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
			<td bgcolor="#666666" colspan="4" width="698" height="1"></td>
			<td bgcolor="#666666" rowspan="8" width="1"></td>
		</tr>
		<tr><td class="RightText" bgcolor="#eeeeee" height="20" colspan="4">&nbsp;<b>PLACED IN WAREHOUSE INVENTORY</b>&nbsp;(verify work Order form is complete & approved)</td></tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="150" align="right">Quantity:</td>
			<td class="RightText" width="50">&nbsp;</td>
			<td class="RightText" align="right" width="200">Signature:</td>
			<td class="RightText" width="100">&nbsp;</td>
		</tr>
		<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
		<tr>
			<td class="RightText" height="23" width="150" align="right">Requested Amount:</td>
			<td class="RightText">&nbsp;</td>
			<td class="RightText" width="200" align="right">Date:</td>
			<td class="RightText" >&nbsp;</td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4" height="1"></td></tr>
	</table>
	<BR>
	<span class="RightText">GM-G007; Rev. 0; DCO#06-000215</span>


</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
