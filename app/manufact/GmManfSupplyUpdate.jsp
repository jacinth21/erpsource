 <%
/**********************************************************************************
 * File		 		: GmManfSupplyUpdate.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!-- manufact\GmManfSupplyUpdate.jsp -->



<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%>
<%@ include file="/common/GmHeader.inc"%>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");
	String strMode = (String)request.getAttribute("hMode")==null?"":(String)request.getAttribute("hMode");
	// System.out.println(" Str Mode is " + strMode);

	int intSize = 0;
	ArrayList alReturnReasons = (ArrayList)request.getAttribute("hNCMRReason");
	if (alReturnReasons !=null)
		{
		intSize = alReturnReasons.size();
		log.debug(" alReturnReasons size is " + alReturnReasons.size());
		}
	HashMap hmDHRDetails = new HashMap();
	ArrayList alEmpList = new ArrayList();
	ArrayList alSubDHR = new ArrayList();

	String strShade = "";
	String strApplDateFmt = strGCompDateFmt;
	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyInspected = "";
	String strQtyReject = "";
	String strActionTaken = "";
	String strCodeID = "";
	String strSelected = "";
	String strReceivedBy = "";
	String strInspectedBy = "";
	String strInspectedByNm = "";
	String strQtyPackaged = "";
	String strPackagedBy = "";
	String strPackagedByNm = "";
	String strQtyInShelf = "";
	String strVerifiedBy = "";
	String strVerifiedByNm = "";
	String strNCMRId = "";
	String strCriticalFl = "";
	String strCreatedDate = "";
	String strRejReason = "";
	String strReasonType = "";
	String strReasonTypeID = "";
	String strCloseQty = "";
	String strNCMRFl = "";
	String strReceivedTS = "";
	String strInspectedTS = "";
	String strPackagedTS = "";
	String strVerifiedTS = "";
	String strSubComFl = "";
	String strSterFl = "";
	String strChecked = "";
	String strLabelFl = "";
	String strPackFl = "";
	String strPartSterFl = "";
	String strFooter = "";
	String strDocRev = "";
	String strDocActiveFl = "";
	String strFARFl = "";
	String strBackOrderFl = "";	
	String strNCMRAction = "";
	String strNCMRRemarks = "";
	String strNCMRAuthBy = "";
	String strNCMRAuthDt = "";
	String strDisabled = "";
	String strStatusFl = "";
	String strSessErrMsg = "";
	String strStatusErrFl = "NULL";
	String strFromPage = "";
	String strDHRVoidFl = "";
	String strQtyShipped = "";
	String strUOM = "";
	String strPortal = (String)request.getAttribute("hPortal")==null?"":(String)request.getAttribute("hPortal");
	String strLastUpdDate = "";
	String strUserId = 	(String)session.getAttribute("strSessUserId");
	String strXmlData = GmCommonClass.parseNull((String) request
			.getAttribute("XMLDATA"));
	String strQtyRecd="0";
	System.out.println("strXmlData==>"+strXmlData);

	int intNCMRFl = 0;

	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");

		strDHRId 		= GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum 		= GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc 		= GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId 		= GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum 	= GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec 		= GmCommonClass.parseZero((String)hmDHRDetails.get("QTYREC"));
		strReceivedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		strQtyInspected = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYINSP"));
		strQtyReject 	= GmCommonClass.parseZero((String)hmDHRDetails.get("QTYREJ"));
		strActionTaken 	= GmCommonClass.parseNull((String)hmDHRDetails.get("ACTTKN"));
		strInspectedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("INSPBY"));
		strInspectedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("INSPBYNM"));
		strPackagedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("PACKBY"));
		strPackagedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKBYNM"));
		strQtyPackaged 	= GmCommonClass.parseNull((String)hmDHRDetails.get("QTYPACK"));
		strQtyInShelf 	= GmCommonClass.parseNull((String)hmDHRDetails.get("QTYSHELF"));
		strVerifiedBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VERIFYBY"));
		strVerifiedByNm = GmCommonClass.parseNull((String)hmDHRDetails.get("VERIFYBYNM"));
		strNCMRId 		= GmCommonClass.parseNull((String)hmDHRDetails.get("NCMRID"));
		strCriticalFl 	= GmCommonClass.parseNull((String)hmDHRDetails.get("CFL"));
		//strCreatedDate 	= GmCommonClass.parseNull((String)hmDHRDetails.get("CDATE"));
		strCreatedDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplDateFmt);
		strRejReason 	= GmCommonClass.parseNull((String)hmDHRDetails.get("REJREASON"));
		strReasonType 	= GmCommonClass.parseNull((String)hmDHRDetails.get("REASON_TYPE"));
		strReasonTypeID = GmCommonClass.parseNull((String)hmDHRDetails.get("REASON_ID"));
		strNCMRFl 		= GmCommonClass.parseZero((String)hmDHRDetails.get("NCMRFL"));
		strCloseQty 	= GmCommonClass.parseZero((String)hmDHRDetails.get("CLOSEQTY"));
		strReceivedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("RECTS"));
		strInspectedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("INSTS"));
		strPackagedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("PACKTS"));
		strVerifiedTS 	= GmCommonClass.parseNull((String)hmDHRDetails.get("VERTS"));
		strSubComFl 	= GmCommonClass.parseNull((String)hmDHRDetails.get("SUBFL"));
		strSterFl 		= GmCommonClass.parseNull((String)hmDHRDetails.get("STERFL"));
		strChecked 		= GmCommonClass.parseNull((String)hmDHRDetails.get("SKIPLBLFL"));
		strNCMRAction 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DISP"));
		strNCMRRemarks 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DISPREMARKS"));
		strNCMRAuthBy 	= GmCommonClass.parseNull((String)hmDHRDetails.get("DISPBY"));
		strNCMRAuthDt 	= GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("DISPDATE"),strApplDateFmt);
		strBackOrderFl	= GmCommonClass.parseNull((String)hmDHRDetails.get("BACKORDER_FLAG"));
		strStatusFl = GmCommonClass.parseNull((String)hmDHRDetails.get("SFL"));
		// strDHRVoidFl = GmCommonClass.parseNull((String)hmDHRDetails.get("DHRVFL");
		strUOM = GmCommonClass.parseNull((String)hmDHRDetails.get("UOMANDQTY"));
		strQtyShipped = GmCommonClass.parseNull((String)hmDHRDetails.get("SHIPQTY"));
		//strLastUpdDate = GmCommonClass.parseNull((String)hmDHRDetails.get("LDATE"));
		strLastUpdDate = GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("LDATE"),strApplDateFmt);
		//strQtyRecd=""+ (Integer.parseInt(strQtyRec) - Integer.parseInt(strQtyReject) + Integer.parseInt(strCloseQty)); --PMT-48866
		
		strLabelFl = strChecked.equals("1")?"Yes":"-";
		strChecked = strChecked.equals("1")?"checked":"";
		strPackFl = GmCommonClass.parseNull((String)hmDHRDetails.get("SKIPPACKFL"));
		strPackFl = strPackFl.equals("1")?"Yes":"-";
		
		String strMod = "";
		strFromPage = (String)request.getAttribute("FromPage")==null?"":(String)request.getAttribute("FromPage");
		if (strStatusFl.equals("0") && strLastUpdDate.equals("")){
			strMod = "Q";
		}
		else if (strStatusFl.equals("0") && !strLastUpdDate.equals("")){
			strMod = "II";
		}		
		else if (strStatusFl.equals("1")){
		strMod = "IC";
		}
		else if (strStatusFl.equals("2")){
		strMod = "P";
		}
		else if (strStatusFl.equals("3")){
		strMod = "V";
		}
		
		if (!(strMod.equals(strMode)))
		{
		strSessErrMsg = "This data has already been filled. Please visit the DashBoard";
		strStatusErrFl = strMode.concat(strFromPage);
		strDisabled = "true";
		strMode = strMod;
		}
		
		strPartSterFl = GmCommonClass.parseNull((String)hmDHRDetails.get("PCLASS"));
		strPartSterFl = strPartSterFl.equals("4030")?"1":"";
		
		if (!strFooter.equals(""))
		{
		String strArr[] = strFooter.split("\\^");
		strFooter = strArr[0];
		strDocRev = strArr[1];
		strDocActiveFl = strArr[2];
		}
		
		strFARFl = GmCommonClass.parseNull((String)hmDHRDetails.get("FARFL"));
								
		intNCMRFl = Integer.parseInt(strNCMRFl);
		
		alEmpList = (ArrayList)hmReturn.get("EMPLIST");
	}


	HashMap hcboVal = null;
	
    Calendar cal = Calendar.getInstance(TimeZone.getDefault());
    String DATE_FORMAT = "MM/dd/yyyy";
    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);
    sdf.setTimeZone(TimeZone.getDefault());
	String strDate = sdf.format(cal.getTime());
	NumberFormat nf = NumberFormat.getInstance(); //PMT-48866
	strQtyRec = GmCommonClass.parseZero(strQtyRec);
	strQtyReject = GmCommonClass.parseZero(strQtyReject);
	strCloseQty = GmCommonClass.parseZero(strCloseQty);
	try {
		strQtyRecd = ""+(Integer.parseInt(strQtyRec)-Integer.parseInt(strQtyReject) + Integer.parseInt(strCloseQty));
		}
		catch(NumberFormatException nfe)
		{
		strQtyRecd = ""+(Double.parseDouble(strQtyRec)-Double.parseDouble(strQtyReject) + Double.parseDouble(strCloseQty));					
		}
%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="org.apache.commons.lang.NumberUtils"%><HTML>
<HEAD>
<TITLE> Globus Medical: Manf Supply Update </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCombo/dhtmlxcombo.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonGrid.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_keymap_access.js"></script>

<script>
var objGridData = '<%=strXmlData%>';
var gridObj;
var	InspectedBY = '<%=strInspectedBy%>' ;
var	PackedBY = '<%=strPackagedBy%>' ;
var	VerifiedBy = '<%=strUserId%>' ;
var gStrNCMRFl = '<%=strNCMRFl%>';
function fnViewPrint()
{
	windowOpener("/GmPOReceiveServlet?hAction=ViewDHRPrint&hDHRId=<%=strDHRId%>&hVenId=<%=strVendorId%>","DHR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}
function fnViewPrintAll()
{
var dhrTransIds='';
var gridrows =gridObj.getAllRowIds();
var gridrowsarr = gridrows.toString().split(",");
var arrLen = gridrowsarr.length;
var rowId='';
var gridval='';
for (var i=0;i<arrLen;i++)
{
    rowId=gridrowsarr[i];
    gridval=gridObj.cellById(rowId,2).getValue();
    
    if (gridval != '')
     {
	    dhrTransIds=dhrTransIds+ gridval;
    	dhrTransIds=dhrTransIds + "$" + rowId + ',';
     }	
    
}
windowOpener("/GmDHRUpdateServlet?hAction=DhrPrintAll&hId="+dhrTransIds,"Con","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=600");
}

function fnOnPageLoad()
{
	objAction = document.frmOrder.hAction.value;
	
	if(objAction == 'V' || objAction == '')
    {
		if(objGridData.indexOf("cell", 0)==-1){
			  return true;
		}
		gridObj = initGrid('dataGridDiv',objGridData);
		gridObj.setEditable(false);
		
		if(gridObj != '')
    	{
			qtyremains = document.getElementById("qty_remains");
    	}
    }else if(objAction == 'P' && (gStrNCMRFl != '1' || gStrNCMRFl == '0')){
    	if(objGridData.indexOf("cell", 0)==-1){
			  return true;
		}
	  	gridObj = setDefalutGridProperties('dataGridDiv');
	  	gridObj.enableEditTabOnly(true);
	  	initializeDefaultGrid(gridObj,objGridData);
	  	gridObj.enableEditEvents(false, true, true);
	  	gridObj.enableLightMouseNavigation(true);
		gridObj.selectCell(0,1);
		gridObj.editCell();
		gridObj.attachEvent("onEditCell", function(stage, rowId, cellInd) {
	    	    if (stage == 0) {
	    	        //User starting cell editing: row id is" + rowId + ", cell index is " + cellInd
	    	    } else if (stage == 1) {
	    	        //Cell editor opened
	    	    } else if (stage == 2) {
	    	        //Cell editor closed
	    	    	var strCellValue=gridObj.cellById(rowId, cellInd).getValue();
	    	    	if(!IsNumeric(strCellValue)){
	    	    		Error_Details(message[46] + '<b>' + gridObj.cellById(rowId, 0).getValue()+'</b>');
	    	    	}
	    	    	if (ErrorCount > 0)
	    	    	{
	    	    		gridObj.selectCell(rowId,cellInd);
	    	    		gridObj.editCell();	    	    	
	    	    		gridObj.cellById(rowId, cellInd).setValue('');
	    	    		Error_Show();
	    	    		Error_Clear();
	    	    		return false;
	    	    	}
	    	    }
	    	    return true;
	    	});
    	}else{
    		if(objGridData.indexOf("cell", 0)==-1){
			  return true;
			}
			gridObj = initGrid('dataGridDiv',objGridData);
			gridObj.setEditable(false);
    	}
}
function IsNumeric(strString) //  check for valid numeric strings	
{
	if(!/\D/.test(strString)) return true;//IF NUMBER
	else if(/^\d+\.\d+$/.test(strString)) return true;//IF A DECIMAL NUMBER HAVING AN INTEGER ON EITHER SIDE OF THE DOT(.)
	else return false;
}
function fnUpdate()
{
	Error_Clear();	
	obj = document.frmOrder.hAction;
	var qtyrec = parseFloat(document.frmOrder.hRecQty.value);

	if (obj.value == 'IC')
	{
		var qtyrej = parseFloat(document.frmOrder.Txt_QtyRej.value);
		var qtyinsp = parseFloat(document.frmOrder.Txt_QtyInsp.value);

		obj1 = parseFloat(document.frmOrder.Txt_QtyInsp.value);
		if (isNaN(qtyinsp))
		{
			Error_Details(message[10]);
		}
		obj2 = document.frmOrder.Cbo_InspEmp.value;
		if (obj2 == '0')
		{
			Error_Details(message[11]);
		}

		if (isNaN(qtyrej))
		{
			Error_Details(message[12]);
		}
		if (qtyinsp > qtyrec)
		{
			Error_Details(message[17]);
		}
		if (qtyrej > qtyrec)
		{
			Error_Details(message[18]);
		}		
		obj4 = document.frmOrder.Cbo_ReasonType.value;
		if (qtyrej > 0 && obj4 == '0')
		{
			Error_Details(message[25]);
		}
	}else if (obj.value == 'P')
	{
		/*var strSterFl=document.frmOrder.hSterFl.value;
		var skip =false;
		if(strSterFl!='1')
			skip = document.frmOrder.Chk_Skip.status;
		
		if (!skip)
		{*/
			var qtyrej = parseFloat(document.frmOrder.hRejQty.value);
			
			if(isNaN(qtyrej)){
				qtyrej = 0;
			}
			
			var qtyclose = parseFloat(document.frmOrder.hCloseQty.value);
			if(isNaN(qtyclose)){
				qtyclose = 0;
			}
			var gridrows =gridObj.getAllRowIds();
			var gridrowsarr = gridrows.toString().split(",");
			var arrLen = gridrowsarr.length;
			var gridval;				
			var obj1=0;
			var inpStr="";
			for (var i=0;i<arrLen;i++)
			{
		        rowId=gridrowsarr[i];
		        gridval=gridObj.cellById(rowId,1).getValue();
		        
		        if(gridval!='' && gridval!='0' && gridval!=undefined && gridval!=null){
		        	inpStr=inpStr + rowId;
		        	inpStr = inpStr +',';
		        	inpStr=inpStr + gridval;
		        	inpStr=inpStr + '|'; 
		        	obj1+=parseFloat(gridval);
		        }
			}
			//obj1	 = Math.round(obj1*Math.pow(10,2))/Math.pow(10,2); //No Need to Round off the Input
			if (qtyrec + qtyclose != qtyrej){  
				if (obj1 == '')
				{
					Error_Details(message[13]);
				}
			
				else if (obj1 > qtyrec-qtyrej+qtyclose)
				{
					Error_Details(message[20]);
				}
				
				else if (obj1 < qtyrec-qtyrej+qtyclose)
				{
					Error_Details(message[19]);
				}				
			}
		//}
		document.frmOrder.hInpStr.value=inpStr;
	}else if (obj.value == 'V')
	{
		var qtyrej = parseFloat(document.frmOrder.hRejQty.value);
		
		if(isNaN(qtyrej)){
				qtyrej = 0;
			}
			
		var qtyclose = parseFloat(document.frmOrder.hCloseQty.value);
		
		if(isNaN(qtyclose)){
			qtyclose = 0;
		}
		obj1 = parseFloat(document.frmOrder.Txt_Qty.value);

		if (isNaN(obj1))
		{
			Error_Details(message[15]);
		}
		if (obj1 > qtyrec-qtyrej+qtyclose)
		{
			Error_Details(message[22]);
		}
		if (obj1 < qtyrec-qtyrej+qtyclose)
		{
			Error_Details(message[23]);
		}
	}
	
	if (ErrorCount > 0)
	{
		Error_Show();
		Error_Clear();
		return false;
	}

	document.frmOrder.hAction.value = '<%=strMode%>';
  	document.frmOrder.action="<%=strServletPath%>/GmMFGSupplyUpdateServlet";
  	fnStartProgress('Y');
  	document.frmOrder.submit();
}

function fnViewPrintWO()
{
	windowOpener("/GmWOServlet?hAction=PrintWO&hWOId=<%=strWOId%>&hDocRev=<%=strDocRev%>&hDocFl=<%=strDocActiveFl%>","WO1","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnViewPrintNCMR()
{
	windowOpener("/GmNCMRServlet?hAction=PrintNCMR&hId=<%=strNCMRId%>","NCMR","resizable=yes,scrollbars=yes,top=150,left=200,width=800,height=640");
}

function fnCalDate(val)
{
	var year = '';
	var month = '';
	var NormalYr = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var LeapYr = new Array(31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	var cal = 0;
	var rem = 0;
	var cal1 = 0;
	var date = '';
	var num = val.substring(3,6);

	//Hardcoded till Year 2008
	if (val.charAt(2)=='D') // For 2003
	{
		year = '2003';
		days = NormalYr;
	}
	else if (val.charAt(2)=='E') // For 2004
	{
		year = '2004';
		days = LeapYr;
	}
	else if (val.charAt(2)=='F') // For 2005
	{
		year = '2005';
		days = NormalYr;
	}
	else if (val.charAt(2)=='G') // For 2006
	{
		year = '2006';
		days = NormalYr;
	}
	else if (val.charAt(2)=='H') // For 2007
	{
		year = '2007';
		days = NormalYr;
	}
	else if (val.charAt(2)=='J') // For 2008
	{
		year = '2008';
		days = LeapYr;
	}

	for (var i=0;i<days.length;i++ )
	{
		cal = cal + days[i];
		if (cal>=num)
		{
			cal1=cal-days[i];
			rem = num-cal1;
			if (rem<10)
			{
				rem = "0"+rem;
			}
			month = i+1;
			if (month<10)
			{
				month="0"+month;
			}
			date = month+"/"+rem+"/"+year;
			break;
		}
	}
	return date;
}

</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnPageLoad();">
<FORM name="frmOrder" method="POST">
<input type="hidden" name="hDHRId" value="<%=strDHRId%>">
<input type="hidden" name="hAction" value="<%=strMode%>">
<input type="hidden" name="hPartNum" value="<%=strPartNum%>">
<input type="hidden" name="hWOId" value="<%=strWOId%>">
<input type="hidden" name="hRecQty" value="<%=strQtyRec%>">
<input type="hidden" name="hRejQty" value="<%=strQtyReject%>">
<input type="hidden" name="hInputString" value="">
<input type="hidden" name="hSterFl" value="<%=strSterFl%>">
<input type="hidden" name="hPartSterFl" value="<%=strPartSterFl%>">
<input type="hidden" name="hNCMRFl" value="<%=intNCMRFl%>">
<input type="hidden" name="hStatusFl" value="<%=strStatusFl%>">
<input type="hidden" name="hPortal" value="<%=strPortal%>">
<input type="hidden" name="hInpStr"/>
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			
			<td height="25" class="RightDashBoardHeader" colspan="3">Manf. Supply Receipt - Update</td>
			
		</tr>
		<tr>
			<td bgcolor="#666666" colspan="3"></td>
		</tr>
		<tr>
			<td width="100%" height="100" valign="top" colspan="3">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%
				if (strCriticalFl.equals("Y")){
%>
					<tr>
						<td colspan="4" class="RightTextRed" align="center" HEIGHT="24">
						This Part has been flagged as <b>Critical</b> and requires immediate processing. <BR>Please inform Product Engineer / Logistics Manager.
						<script>
							alert("This part has been flagged as Critical and requires immediate processing.");
						</script>
						</td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%
				}
				if (strFARFl.equals("Y")){
%>
					<tr>
						<td colspan="4" class="RightTextRed" align="center" HEIGHT="24">
						This Part has been flagged as a <b>First Article</b> and requires immediate processing. <BR>Please inform Product Engineer / Operations Manager.
						<script>
							alert("This part has been flagged as First Article and requires immediate processing.");
						</script>
						</td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%
				}
				if (strBackOrderFl.equals("Y")){
%>
					<tr>
						<td colspan="4" class="RightTextRed" align="center" HEIGHT="24">
						This Part has been flagged as a <b>Back Ordered Item</b> and requires immediate processing. <BR>Please inform Manf. Quality Team.
						<script>
							alert("This part has been flagged as Back Order and requires immediate processing.");
						</script>
						</td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%
				}
%>
					<tr>
						<td colspan="4">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td width="120" class="RightText" align="right" HEIGHT="24">Transaction Id:</td>
									<td width="140" class="RightText">&nbsp;<b><%=strDHRId%></b></td>
									<td width="100" class="RightText" align="right" HEIGHT="24">Bin #:</td>
									<td width="360" class="RightText">&nbsp;</td>
								</tr>
								<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td class="RightText" align="right" HEIGHT="24">Part Number:</td>
									<td class="RightText">&nbsp;<%=strPartNum%></td>
									<td class="RightText" align="right" HEIGHT="24">Description:</td>
									<td class="RightText" colspan="3">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>									
								</tr>
								<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td class="RightText" align="right" HEIGHT="24">Lot Number:</td>
									<td class="RightText">&nbsp;<%=strControlNum%></td>
									<td class="RightText" align="right" HEIGHT="24">Vendor:</td>
									<td class="RightText"  colspan="3">&nbsp;<%=strVendName%></td>
								</tr>
								<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
								<tr>
									<td class="RightText" align="right" HEIGHT="24">Created Date:</td>
									<td class="RightText">&nbsp;<%=strCreatedDate%></td>
									<td class="RightText" align="right" HEIGHT="24">UOM / Units:</td>
									<td class="RightText"  colspan="3">&nbsp;<%=strUOM%></td>
								</tr>
							</table>
						 </td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>

					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;Receiving</td>
					</tr>
					<tr>
						<td class="RightText" width="170" align="right" HEIGHT="24">PO Qty Shipped:</td>
						<td class="RightText" width="100">&nbsp;<%=strQtyShipped%></td>
						<td class="RightText" width="250" align="right">&nbsp;Qty Received (Expanded):</td>
						<td class="RightText" width="170">&nbsp;<%=strQtyRec%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightText" align="right">&nbsp;Received by:</td>
						<td class="RightText" width="300">&nbsp;<%=strReceivedBy%></td>
						<td class="RightText" align="right" HEIGHT="24">Received Timestamp:</td>
						<td class="RightText">&nbsp;<%=strReceivedTS%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>

<%					
			if (strMode.equals("Q"))
			{
%>
					<tr>
						<td class="RightTableCaption" bgcolor="#9AADFC" HEIGHT="18" colspan="4">&nbsp;QC Inspection</td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24">Qty verified By QC:</td>
						<td class="RightText">&nbsp;<%=strQtyRec%><input type="hidden" value="<%=strQtyRec%>" name="Txt_Qty"></td>
						<td class="RightText" align="right">&nbsp;Verified by:</td>
						<td class="RightText">&nbsp;
							<input type="hidden" name="Cbo_Emp" value="<%=strUserId%>">
<%			  		intSize = alEmpList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alEmpList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
			  			if (strUserId.equals(strCodeID))
			  			{
			  				out.println("<input type=text class=InputArea size=25 readonly value="+ hcboVal.get("NAME")+">");
			  				break;	
			  			}
			  		}
%>
						</td>
<%		
			}

			if (strMode.equals("II"))
			{
%>
					<tr>
						<td class="RightTableCaption" bgcolor="#9AADFC" HEIGHT="18" colspan="4">&nbsp;QC Inspection(Initiated)</td>
					</tr>
					<tr>
						<td class="RightText" align="right">&nbsp;Initiated by:</td>
						<td class="RightText">&nbsp;
							<input type="hidden" name="Cbo_InspEmp" value="<%=strUserId%>">
<%			  		intSize = alEmpList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alEmpList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
			  			if (strUserId.equals(strCodeID))
			  			{
			  				out.println("<input type=text class=InputArea size=25 readonly value="+ hcboVal.get("NAME")+">");
			  				break;	
			  			}
			  		}
%>
						</td>
						<td class="RightText" colspan="2" >&nbsp;</td>
<%		
			}

			if (strMode.equals("IC"))
			{
%>
					<tr>
						<td class="RightTableCaption" bgcolor="#9AADFC" HEIGHT="18" colspan="4">&nbsp;QC Inspection</td>
					</tr>
				<td class="RightText" align="right" HEIGHT="24">Qty Inspected:</td>
						<td class="RightText">&nbsp;<input type="text" size="4" value="<%=strQtyInspected%>" 
						onFocus="changeBgColor(this,'#AACCE8');"  
						onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_QtyInsp"></td>
						<td class="RightText" align="right">&nbsp;Inspected by:</td>
						<td class="RightText">&nbsp;
							<input type="hidden" name="Cbo_InspEmp" value="<%=strUserId%>">
<%			  		intSize = alEmpList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alEmpList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
			  			if (strUserId.equals(strCodeID))
			  			{
			  				out.println(hcboVal.get("NAME"));
			  				break;	
			  			}
			  		}
%>				
						</td>
<%		
			}else{
				if (strStatusErrFl.equals("IDashboardDHR"))
					{
%>		
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;QC Inspection</td>
					</tr>
					<tr>
						<td class="RightTextRed" align="center" HEIGHT="24" colspan="4"> <%=strSessErrMsg%> </td>
					</tr>
<%
					}
					else {
%>
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;QC Inspection</td>
					</tr>
<%				
						}
%>						
						<td class="RightText" align="right" HEIGHT="24">Qty Inspected:</td>
						<td class="RightText">&nbsp;<%=strQtyInspected%></td>
						<td class="RightText" align="right">&nbsp;Inspected by:</td>
						<td class="RightText">&nbsp;<%=strInspectedByNm%></td>
<%		
			}
%>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24">Inspected Timestamp:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strInspectedTS%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24">Qty Rejected:</td>
<%		
			if (strMode.equals("IC"))
			{
%>
						<td class="RightText">&nbsp;<input type="text" value="<%=strQtyReject%>" size="4" 
						onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" 
						class="InputArea"  name="Txt_QtyRej"></td>
<%		
			}else{
%>
						<td class="RightText">&nbsp;<%=strQtyReject%></td>
<%		
			}
%>
						<td class="RightText" align="right" HEIGHT="24">Raised NCMR Id:</td>
						<td class="RightText">&nbsp;<%=strNCMRId%></td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee"></td></tr>
					<tr><td class="RightText" align="right" HEIGHT="24">Rejection Type:</td>
<%		
			if (strMode.equals("IC")){
				
%>					<td class="RightText"> &nbsp;
							<!-- Custom tag lib code modified for JBOSS migration changes -->
								<gmjsp:dropdown controlName="Cbo_ReasonType" 	   seletedValue="<%= strReasonTypeID %>" 	
									tabIndex="1"  width="100" value="<%= alReturnReasons%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "[Choose One]"  />

					</td>
<%		
			}else{
%>
						<td class="RightText" colspan="3">&nbsp;<%=strReasonType%></td>
<%		
			}
%>
           <tr>
					
					<tr><td colspan="2" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24">Reason for Rejection:</td>
<%		
			if (strMode.equals("IC")){
%>	
						<td class="RightText" colspan="3">&nbsp;<textarea name="Txt_RejReason" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');" rows=4 cols=45 value="" ><%=strRejReason%></textarea></td>
<%		
			}else{
%>
						<td class="RightText" colspan="3">&nbsp;<%=strRejReason%></td>
<%		
			}
%>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%		
			if ((intNCMRFl >0 && intNCMRFl < 2) || (strNCMRId.startsWith("GM-EVAL") && intNCMRFl == 0 ) ) {
			if (!strQtyReject.equals("0")){
			strDisabled = "true";
			}
%>
					<tr>
						<td class="RightTextRed" align="center" HEIGHT="24" colspan="4">This Supply transaction has an associated NCMR that has to be closed (Disposition Details) before proceeding with this DHR. <br>Please contact Quality Inspector/QA Director</td>
					</tr>
<%
}
%>
					<tr>
						<td class="RightTableCaption" colspan="4" HEIGHT="24">&nbsp;&nbsp;<u>NCMR Disposition Details</u></td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="22">Action Taken:</td>
						<td class="RightText">&nbsp;<%=strNCMRAction%></td>
						<td class="RightText" align="right" valign="top" HEIGHT="22">Remarks:</td>
						<td class="RightText">&nbsp;<%=strNCMRRemarks%></td>						
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="22">Authorized By:</td>
						<td class="RightText">&nbsp;<%=strNCMRAuthBy%></td>
						<td class="RightText" align="right" HEIGHT="22">Date:</td>
						<td class="RightText">&nbsp;<%=strNCMRAuthDt%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="22">NCMR Closeout Qty:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strCloseQty%>
						<input type="hidden" name="hCloseQty" value="<%=strCloseQty%>"></td>						
					</tr>			
<tr>
				<td colspan="4" bgcolor="#cccccc"></td>
			</tr>
			<tr>
				<td class="RightTableCaption" bgcolor="#9AADFC" HEIGHT="18"
					colspan="4">&nbsp;Pending Processing</td>
			</tr>
			<tr>
				<td colspan="4">
				<div id="dataGridDiv" class="grid" style="height: 150px;"></div>
				</td>
			</tr>
			<tr>
				<td colspan="4" bgcolor="#cccccc"></td>
			</tr>
			<%
				if (strMode.equals("P")) {
			%>
			<tr>
				<td class="RightTableCaption" align="right" HEIGHT="24">Skip
				Labeling ?:&nbsp; <input
					type="checkbox" <%=strChecked%> name="Chk_Skip"></td>
				<td class="RightTableCaption" align="right" width="250">&nbsp;Processed
				By:&nbsp; <input type="hidden"
					name="Cbo_Emp" value="<%=strUserId%>"> <%
 	intSize = alEmpList.size();
 			hcboVal = new HashMap();
 			for (int i = 0; i < intSize; i++) {
 				hcboVal = (HashMap) alEmpList.get(i);
 				strCodeID = (String) hcboVal.get("ID");
 				if (strUserId.equals(strCodeID)) {
 					out.println(hcboVal.get("NAME"));
 					break;
 				}
 			}
 			%>
 			</td>
			</tr>
			<%
 		} else {
 %>

				<tr>
					<td class="RightTableCaption" align="right" width="250">Processed
					By:</td>
					<td class="RightText" width="170">&nbsp;<%=strPackagedByNm%></td>
					<td class="RightTableCaption" align="right" HEIGHT="24">Skip
					Labeling ?:</td>
					<td class="RightText" HEIGHT="24">&nbsp;<%=strLabelFl%></td>
				</tr>
				<%
					}		
			if (strMode.equals("V"))
			{
%>
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" bgcolor="#9AADFC" HEIGHT="18" colspan="4">&nbsp;Pending Verification
							</td>
					</tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24">Qty to Verify:</td>
						<td class="RightText">&nbsp;<input type="text" size="4" value="<%=strQtyRecd%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_Qty"></td>
						<td class="RightText" align="right">Verified By:</td>
						<td class="RightText">&nbsp;
						<input type="hidden" name="Cbo_Emp" value="<%=strUserId%>" >
<%
					
		  		intSize = alEmpList.size();
				hcboVal = new HashMap();
		  		for (int i=0;i<intSize;i++)
		  		{
		  			hcboVal = (HashMap)alEmpList.get(i);
		  			strCodeID = (String)hcboVal.get("ID");
		  			if (strUserId.equals(strCodeID))
		  			{
		  				out.println(hcboVal.get("NAME"));
		  				break;	
		  			}
		  		}
%>
						</td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%		
			}else{
%>			
					<tr><td colspan="4" bgcolor="#cccccc"></td></tr>
					<tr>
						<td class="RightTableCaption" bgcolor="#eeeeee" HEIGHT="18" colspan="4">&nbsp;Verification</td>
					</tr>		
<%

				if (strStatusErrFl.equals("PDashboardDHR"))
				{
%>		
					<tr>
					<td class="RightTextRed" align="center" HEIGHT="24" colspan="4"> <%=strSessErrMsg%> </td>
					</tr>
<%
				}
%>

					<tr>
						<td class="RightText" align="right" HEIGHT="24">Qty Verified:</td>
						<td class="RightText">&nbsp;<%=strQtyInShelf%></td>
						<td class="RightText" align="right">Verified By:</td>
						<td class="RightText">&nbsp;<%=strVerifiedByNm%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightText" align="right" HEIGHT="24">Verified Timestamp:</td>
						<td class="RightText" colspan="3">&nbsp;<%=strVerifiedTS%></td>
					</tr>
					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
<%		
			}
%>

					<tr><td colspan="4" bgcolor="#eeeeee"></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>

    </table>

	<table border="0" width="700" cellspacing="0" cellpadding="0" >
		<tr>
			<td align="center" height="30" id="button">
<%		if (strMode.equals("Load"))
		{
%>			<gmjsp:button value="&nbsp;Return&nbsp;" name="Btn_Return" buttonType="Save" gmClass="button" onClick="history.go(-1)" />&nbsp;&nbsp;
<%
		}else if (strMode.equals("Q") || strMode.equals("II") || strMode.equals("IC") || strMode.equals("V") || strMode.equals("P")){
				if (((strStatusErrFl.substring(0,1)).equals("I") && !strNCMRId.equals(""))){
	%>			<gmjsp:button value="Print NCMR" name="Btn_ViewPrint" gmClass="button" buttonType="Load" onClick="fnViewPrintNCMR();" />&nbsp;&nbsp;
<%				}else{
	
%>				<gmjsp:button value="Update" name="Btn_Update" disabled="<%=strDisabled%>" gmClass="button" buttonType="Save" onClick="fnUpdate();" />&nbsp;&nbsp;
<%				}		
		}
%>
		<%
			if(strMode.equals("V")|| strVerifiedByNm!=""){
		%>
			<gmjsp:button value="Print All" name="Btn_ViewPrintAll" gmClass="button" buttonType="Load" onClick="fnViewPrintAll();" />&nbsp;&nbsp;
		<%} %>

			</td>
		<tr>
	</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
