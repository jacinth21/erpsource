 <%
/**********************************************************************************
 * File		 			: GmSetDocument.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- manufact\GmSetDocument.jsp -->


<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmParam = (HashMap)request.getAttribute("PARAM");
	log.debug(" CValues inside hmParam IN JSP >>  is "  + hmParam);
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
	String strSelected = "";
	String strCodeID = "";
	String strDocCategory =	"";
	String strDocType = "";
	String strDocId = "";
	String strSetNm = "";
	String strDesc = "";
	String strTemp = "";
	String strDrawRev = "";
	String strActiveFlag = "";
	String strDocSeqNo = "";
	String strChecked = "off";

	ArrayList alProjList = new ArrayList();
	ArrayList alSetCategory = new ArrayList();
	ArrayList alSetType = new ArrayList();
	alProjList = (ArrayList)request.getAttribute("results");
	
	int intPriceSize = 0;

	if (hmReturn != null)
	{
		alSetCategory = (ArrayList)hmReturn.get("DOCCATEGORY");
		alSetType = (ArrayList)hmReturn.get("DOCTYPE");

	}
	
	if (hmParam != null)
	{
			strDocId 					 = 	GmCommonClass.parseNull((String)hmParam.get("DOCID"));
			strDesc					 = 	GmCommonClass.parseNull((String)hmParam.get("DOCDESC"));
			strDocCategory   = 	GmCommonClass.parseNull((String)hmParam.get("DOCCAT"));
			strDocType 			 =		GmCommonClass.parseNull((String)hmParam.get("DOCTYPE"));
			strActiveFlag 		 =		GmCommonClass.parseNull((String)hmParam.get("ACTIVEFLAG"));
			strDrawRev			 = 	GmCommonClass.parseNull((String)hmParam.get("REVNUM"));
			strDocSeqNo 		=  	GmCommonClass.parseNull((String)hmParam.get("DOCSEQNO"));
		}
		if (strActiveFlag.equals("Y"))
				strChecked = "checked";
	log.debug(" DoC SEQ NO inside JSP is " + strDocSeqNo);
	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Manufacturing - Set Documents (like BOM, Prod Spec, Supply Spec, Drawing etc </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnSubmit()
{
	document.frmManf.hAction.value = 'Save';
	fnStartProgress('Y');
  	document.frmManf.submit();
}

function fnUpdate()
{
	document.frmManf.hAction.value = 'Update';
	fnStartProgress('Y');
  	document.frmManf.submit();
}


function fnCallLookUp()
{
	document.frmManf.hAction.value = 'Lookup';
	fnStartProgress('Y');
	document.frmManf.submit();
}


</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmManf" method="POST" action="<%=strServletPath%>/GmDocumetSetUpServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hType" value="1602">
<input type="hidden" name="hDocSeqNo" value="<%=strDocSeqNo%>">

	<table border="0" width="600" cellspacing="0" cellpadding="0">
		<tr>
			<td bgcolor="#666666" width="1" rowspan="2"></td>
			<td height="25" class="RightDashBoardHeader">Set Master Setup</td>
			<td bgcolor="#666666" width="1" rowspan="2"></td>
		</tr>
		<tr>
			<td width="598" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="shade">
						<td class="RightText" align="right" HEIGHT="24">Doc Id:</td>
						<td>&nbsp;<input type="text" size="10" value="<%=strDocId%>" name="Txt_DocId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
						<td  height="30" align="center">
							<gmjsp:button value="Lookup" gmClass="button" buttonType="Load" onClick="fnCallLookUp();" tabindex="8"/>
					</tr>
					<tr class="shade">
						<td class="RightText" valign="top" align="right" HEIGHT="24">Doc Description:</td>
						<td>&nbsp;<textarea name="Txt_DocDesc" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=2 rows=1 cols=40 value="<%=strDesc%>"><%=strDesc%></textarea></td>
					</tr>
						<tr>					
						<td class="RightText" HEIGHT="30" align="right">&nbsp;Category:</td>
						<td width="320">
						&nbsp;<select name="Cbo_Cat" id="Region" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3><option value="0" >[Choose One]
<%
			  		intSize = alSetCategory.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alSetCategory.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strDocCategory.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
						</select>
						</td>
					</tr>
					<tr class="Shade">
						<td class="RightText" HEIGHT="30" align="right">&nbsp;Type:</td>
						<td width="320">
						&nbsp;<select name="Cbo_Type" id="Region" class="RightText" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=4><option value="0" >[Choose One]
<%
			  		intSize = alSetType.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alSetType.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strDocType.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
						</select>
						</td>
					</tr>
					<tr>
						<td class="RightText" align="right" height="24">Active ?:</td>
						<td><input type="checkbox" <%=strChecked%>  name="Txt_AFl"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=5></td>
						</tr>
						<tr>
							<td class="RightText" width="200" align="right" HEIGHT="24">Rev Number:</td>
							<td>&nbsp;<input type="text" size="3" value="<%=strDrawRev%>" name="Txt_Rev" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=6>
						</td>
					</tr>
					
					<tr>
						<td colspan="2" height="30" align="center">
						<% 
						if (strhAction.equals("Update"))
						{
						%>
							<gmjsp:button value="Update" gmClass="button" buttonType="Save" onClick="fnUpdate();" tabindex="7"/>
						<%
						}
						else {
						%>
								<gmjsp:button value="Save" gmClass="button" buttonType="Save" onClick="fnSubmit();" tabindex="7"/>
						<%
								}
						%>
						</td>
					</tr>
				 </table>
			 </td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
							<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
							<jsp:include page="/common/GmUpdationHistory.jsp" />

</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>
