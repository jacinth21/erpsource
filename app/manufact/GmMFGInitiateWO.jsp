
<%@page import="java.util.Date"%>
<%
/**********************************************************************************
 * File		 			: GmMFGInitiateWO.jsp
 * Desc		 		: This screen is used for the Initiating Manufacturing Work Order
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- manufact\GmMFGInitiateWO.jsp -->


<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>

<%

	String strManuFactJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_MANUFACT");

	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("OPT"));
	String strMfgWO = "";
	String strOrdQty = "";
	String strOrdComplete = "";
	String strOrdPending = "";
	String strQtyInProcess = "";
	String strPartNum = "";
	String strPartDesc = "";
	String strMfgQty = "";
	String strBOMid = "";
	String strVendorId = "";
	String strProdClass = "";
	String strRwFl = "";
	String strHeader = strhOpt.equals("WOINI")?"Initiate New Manufacturing Run":"Work Order Summary";
	String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
	String strDate = GmCommonClass.parseNull((String)session.getAttribute("strSessTodaysDate"));
	java.sql.Date dtExpDate = null;
	String strDonorNum = "";
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("INTITIATE_MANUFACTURING"));;
	
	String strTimeZone = strGCompTimeZone;
	strApplDateFmt = strGCompDateFmt;
	String strDTDateFmt = "{0,date,"+strApplDateFmt+"}";
	//String strDate = GmCommonClass.getStringFromDate(new Date(),strApplDateFmt, strTimeZone);
	
	HashMap hmMfgWO = (HashMap)request.getAttribute("HMLOADWO");
	strMfgWO = GmCommonClass.parseNull((String)request.getAttribute("MFGWO"));
	
	ArrayList alBOMDtls = new ArrayList();
	alBOMDtls = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("BOMDETAILS"));
	int rowsize = alBOMDtls.size();
	if (hmMfgWO != null)
	{
	 	strOrdQty = GmCommonClass.parseNull((String)hmMfgWO.get("QTYORD"));
		strOrdComplete = GmCommonClass.parseNull((String)hmMfgWO.get("QTYDONE"));
		strOrdPending = GmCommonClass.parseNull((String)hmMfgWO.get("QTYPDG"));
		strPartNum = GmCommonClass.parseNull((String)hmMfgWO.get("PARTNUM"));
		strBOMid = GmCommonClass.parseNull((String)hmMfgWO.get("BOMID"));
		strVendorId =  GmCommonClass.parseNull((String)hmMfgWO.get("VENDID"));
		strPartDesc = GmCommonClass.parseNull((String)hmMfgWO.get("PDESC"));
		strQtyInProcess = GmCommonClass.parseZero((String)hmMfgWO.get("QTYWIP"));
		strProdClass = GmCommonClass.parseNull((String)hmMfgWO.get("PRODCLASS"));
		dtExpDate = (java.sql.Date)hmMfgWO.get("EXPDATE");
	}
	strRwFl = GmCommonClass.parseNull((String)request.getAttribute("RWFLAG"));
	
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Manufacturing Work Order Initiate </TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script> 
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strManuFactJsPath%>/GmMFGInitiateWO.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script>
var format = "<%=strApplDateFmt%>";
var rowsize = "<%=rowsize%>";
var vProdClass = "<%=strProdClass%>";
var rwcheckFl = '<%=strRwFl%>';
function fnSetDefaults(){
	if(rwcheckFl){
		document.frmMfg.rwflag.checked = true;
	}	
}
</script>

<BODY leftmargin="20" topmargin="10" onload="fnSetDefaults();">
<FORM name="frmMfg" method="POST" action="<%=strServletPath%>/GmMFGInitiateWOServlet">
<input type="hidden" name="hType" value="">
<input type="hidden" name="hPartNum" value="<%=strPartNum%>">
<input type="hidden" name="hOrdQty" value="<%=strOrdQty%>">
<input type="hidden" name="hOrdComplete" value="<%=strOrdComplete%>">
<input type="hidden" name="hOrdPending" value="<%=strOrdPending%>">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="Txt_BOMid" value="<%=strBOMid%>"> 
 <input type="hidden" name="hManfDt" value="<%=strDate%>">
<input type="hidden" name="hboinputStr" value="">
<input type="hidden" name="hinputStr" value="">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25" class="RightDashBoardHeader">&nbsp;<%=strHeader%></td>
						<%if(strhOpt.equals("WOINI")){ //Wikilink should show for Initiate New Manufacturing Run screen%>
						<td align="right" class="RightDashBoardHeader"><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif'
									title='Help' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
						</td>
						<%} %>
					</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="2"></td></tr>
		<tr>
			<td width="850" height="30" valign="top" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="alignright" HEIGHT="35" width="40%">&nbsp;Globus' Work Order Id:</td>
						<td width="60%" class="RightText">&nbsp;<input type="text" size="15" value="<%=strMfgWO%>" name="Txt_MfgWO" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;
						<gmjsp:button value="Load WO" gmClass="button" buttonType="Load" onClick="fnLoadWO();" tabindex="13"/>
						</td>
					</tr>
<%
	if (strhAction.equals("Reload"))
	{
		if (!hmMfgWO.isEmpty())
		{
%>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr class="Shade">
						<td class="alignright" HEIGHT="25">&nbsp;Part Number:</td>
						<td width="520" class="RightText">&nbsp;<%=strPartNum%>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="alignright" HEIGHT="25">&nbsp;Description:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr class="Shade">
						<td colspan="2">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td width="190" class="alignright" HEIGHT="25">&nbsp;Quantity Ordered:</td>
									<td width="50" class="RightText">&nbsp;<%=strOrdQty%></td>
									<td rowspan="2" width="100">&nbsp;</td>
									<td class="alignright" HEIGHT="25">&nbsp;Quantity Complete:</td>
									<td width="220" class="RightText">&nbsp;<%=strOrdComplete%>
									</td>
								</tr>
								<tr>
									<td class="alignright" HEIGHT="25">&nbsp;Quantity In-Process:</td>
									<td class="RightText">&nbsp;<%=strQtyInProcess%></td>
									<td class="alignright" HEIGHT="25">&nbsp;Quantity Pending:</td>
									<td class="RightText">&nbsp;<%=strOrdPending%>
									</td>
								</tr>	
							</table>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
<%
			if (strhOpt.equals("WOINI"))
			{
%>					
					<tr>
					<td colspan="2">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td width="40%" class="alignright" HEIGHT="25">&nbsp;Quantity to Manufacture:</td>
						<td width="20%" class="RightText">&nbsp;<input type="text" size="4" value="<%=strMfgQty%>" name="Txt_MfgQty" class="InputArea"  onChange="javascript:fnCalcReqAmt();" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;
						</td>
						<td class="alignright" width="22%" HEIGHT="25">&nbsp;Move to Restricted Warehouse:</td>
						<td width="28%" >&nbsp;<input type="checkbox" value="" name="rwflag" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" ></td>
					</tr>
					</table></td></tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr class="Shade">
						<td class="alignright" HEIGHT="25">&nbsp;Expiry Date:</td>					
						<td width="520" class="RightText">
	<%                  if (strProdClass.equals("4030")) {
	                                // Calendar object used for sterile parts
	%>
							&nbsp;<gmjsp:calendar textControlName="Txt_ExpDate" gmClass="InputArea" textValue="<%=dtExpDate%>" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="2"/>
	<%                          } else {  
	%>
	                        &nbsp;<input type="text" size="9" value="" name="Txt_ExpDate" align="absmiddle" disabled=true/>&nbsp;<img src=<%=strImagePath%>/nav_calendar.gif border=0 align="absmiddle" height=18 width=19></img>
	<%                          }
	%>						
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<%-- <tr>
						<td class="alignright" HEIGHT="20" >&nbsp;Date of Manufacture:</td>
						<td width="520" class="RightText">&nbsp;<%=strDate%></td>
					</tr> --%>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr class="Shade">
						<td class="alignright" HEIGHT="20">&nbsp;Donation ID number:</td>
						<td width="520" class="RightText">&nbsp;<input type="text" size="20" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" class="InputArea" name="Txt_DonorNum"></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="alignright" HEIGHT="25">&nbsp;Bill of Material:</td>
						<td width="520" class="RightText">&nbsp;<%=strBOMid%></td>
					</tr>	
					<tr><td colspan="2" class="Line"></td></tr> 				
				<!-- 	<tr>
						<td colspan="2" class="aligncenter" height="30"> 
							<gmjsp:button value="Calculator" gmClass="button" buttonType="Save" onClick="fnCalculate();" tabindex="13"/>
						</td>	
					</tr> -->
					<tr>
						<td colspan="2">
						<display:table name="BOMDETAILS"  class = "its" id="currentRowObject" decorator="com.globus.displaytag.beans.DTMfgInitateWOWrapper">
							<display:column property="FORCEBO" title="Force BO"   />
						  	<display:column property="PNUM" title="Part Number" class="aligncenter" sortable="true"  />						 
						  	<display:column property="PDESC" title="Part Description" class="alignLeft" sortable="true"  />						 
							<display:column property="SETQTY" title="BOM Qty" class="alignright"  />
							<display:column property="UNITNM" title="Unit of <br>Measurement" class="aligncenter"   />
							<display:column property="TAG" title="Tag" class="aligncenter"   />	
							<display:column property="REQQTY" title="BOM Req Qty" class="alignright"    />	
						  	<display:column property="INVQTY" title="Inventory Qty" class="alignright" />
						</display:table>
						</td>
					</tr>	
					<tr>
						<td colspan="2" class="aligncenter" height="30">
							<gmjsp:button value="Initiate" gmClass="button" buttonType="Save" onClick="fnInitate();" tabindex="13"/>&nbsp;&nbsp; 
						</td>
					</tr>					
<%
			}else if (strhOpt.equals("WOQRY")){
%>
					<tr>
						<td colspan="2">
						<display:table name="ALWOSTATUS"  class = "its" id="currentRowObject">
							<display:column property="CNUM" title="Lot Number" class="aligncenter" sortable="true"  />						 
							<display:column property="QTY" title="Qty" class="alignright"  />
						  	<display:column property="SFL" title="Status" class="alignright" />
						  	<display:column property="CDATE" title="Created Date" class="aligncenter" format="<%=strDTDateFmt%>"  />
						  	<display:column property="CUSER" title="Created By" class="aligncenter" />
						  	<display:column property="DHRID" title="Accounting Txn Id" class="aligncenter" sortable="true"  />						 
						</display:table>
						</td>
					</tr>						
<%
			}
		}else{
%>
					<tr>
						<td class="aligncenter" HEIGHT="25" colspan="2">&nbsp;No Date available for the Work Order specified!</td>
					</tr>
<%
		}
	 }
%>
		</td>
		</tr>
    </table>

</td></tr></table></FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>