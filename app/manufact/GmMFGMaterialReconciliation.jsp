 <%
/**********************************************************************************
 * File		 			: GmMFGMaterialReconciliation.jsp
 * Desc		 		: This screen is used for Manufacturing Material reconciliation
 								Users would search on a MFG Work Order and update the quantity of supply parts used 
 								for that specific work order. 
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- manufact\GmMFGMaterialReconciliation.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ include file="/common/GmHeader.inc" %>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
		
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	String strMfgWO = "";
	String strOrdQty = "";
	String strOrdComplete = "";
	String strOrdPending = "";
	String strPartNum = "";
	String strMfgQty = "";
	String strBOMid = "";
	String strVendorId = "";
	String strPartDesc = "";
	String strSupPartNum = "";
	String strSupPartDesc = "";
	String strSupQtyOrd = "";
	String strSupControlNumber = "";
	String strQtyUsed = "";
	String strQtyReturned = "";
	
	
	HashMap hmMfgWO = (HashMap)request.getAttribute("HMLOADWO");
	ArrayList alSuppRecon =  (ArrayList)request.getAttribute("ALSUPRECON");
   strMfgWO = GmCommonClass.parseNull((String)request.getAttribute("MFGWO"));
   strhAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	
	if (hmMfgWO != null)
	{
 	strOrdQty = GmCommonClass.parseNull((String)hmMfgWO.get("QTYORD"));
	strOrdComplete = GmCommonClass.parseNull((String)hmMfgWO.get("QTYDONE"));
	strPartNum = GmCommonClass.parseNull((String)hmMfgWO.get("PARTNUM"));
	strBOMid = GmCommonClass.parseNull((String)hmMfgWO.get("BOMID"));
	strVendorId =  GmCommonClass.parseNull((String)hmMfgWO.get("VENDID"));
	strPartDesc =  GmCommonClass.parseNull((String)hmMfgWO.get("PARTDESC"));
	}
		
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Manufacturing Material Reconciliation </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>

function fnLoadWO()
{
	document.frmMfg.hAction.value = 'LoadWO';
	fnStartProgress('Y');
  	document.frmMfg.submit();
}

function fnInitate()
{
	document.frmMfg.hAction.value = 'InitiateWO';
  	document.frmMfg.submit();
}

</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmMfg" method="POST" action="<%=strServletPath%>/GmMFGMaterialReconcServlet">
<input type="hidden" name="hType" value="">
<input type="hidden" name="hPartNum" value="<%=strPartNum%>">
<input type="hidden" name="hOrdQty" value="<%=strOrdQty%>">
<input type="hidden" name="hOrdComplete" value="<%=strOrdComplete%>">
<input type="hidden" name="hOrdPending" value="<%=strOrdPending%>">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">

	<table border="0" width="800" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
			<td height="25" class="RightDashBoardHeader">&nbsp;Material Reconciliation</td>
			<td bgcolor="#666666" width="1" rowspan="5"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="798" height="30" valign="top" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" HEIGHT="25" align="right">&nbsp;Globus Internal WO Number:</td>
						<td width="520" class="RightText">&nbsp;<input type="text" size="15" value="<%=strMfgWO%>" name="Txt_MfgWO" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"  tabindex=1>&nbsp;
						<gmjsp:button value="Load WO" gmClass="button" buttonType="Load" onClick="fnLoadWO();" tabindex="13"/>
						</td>
					</tr>
<%
	if (strhAction.equals("LoadWO"))
	{
%>
					<tr>
						<td class="RightText" HEIGHT="25" align="right">&nbsp;Part Number:</td>
						<td width="520" class="RightText">&nbsp;<%=strPartNum%>
						</td>
					</tr>
					
					<tr>
						<td class="RightText" HEIGHT="25" align="right">&nbsp;Part Description:</td>
						<td width="520" class="RightText">&nbsp;<%=strPartDesc%>
						</td>
					</tr>
					
					<tr>
						<td class="RightText" HEIGHT="25" align="right">&nbsp;Quantity Ordered:</td>
						<td width="520" class="RightText">&nbsp;<%=strOrdQty%>
						</td>
					</tr>
					
					<tr>
						<td class="RightText" HEIGHT="25" align="right">&nbsp;Quantity to Manufacture:</td>
						<td width="520" class="RightText">&nbsp;<input type="text" size="15" value="<%=strMfgQty%>" name="Txt_MfgQty" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;
						</td>
					</tr>
<%	
	 }
%>
		</td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
    
    	<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td align="center" width="80" height="25">Part Number</td>
									<td align="center" width="200">Description</td>
									<td align="center" width="70">Ordered<BR>Qty</td>
									<td align="center" width="80">Qty <br>Used</td>
									<td align="center" width="80">Qty<BR>Returned</td>
									<td align="center" width="80">Control Number</td>
								</tr>
								<tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
		<%
		int cnt = alSuppRecon.size();
		for (int i = 0; i < cnt; i ++)
		{
		HashMap hmSuppRecon = (HashMap)alSuppRecon.get(i);
		strSupPartNum = (String)hmSuppRecon.get("PARTNUM");
		strSupPartDesc =  (String)hmSuppRecon.get("PARTDESC");
		strSupQtyOrd = (String) hmSuppRecon.get("QTY");
		strSupControlNumber = (String) hmSuppRecon.get("CRLNUM");
		%>
									<tr class="RightText" >
									<td align="center" width="80" height="25"><%=strSupPartNum%></td>
									<td align="center" width="200"><%=strSupPartDesc%></td>
									<td align="center" width="70"><%=strSupQtyOrd%></td>
									<td align="center" width="80">
									<input type="text" size="15" value="<%=strQtyUsed%>" name="Txt_QtyUsed"<%=cnt%> class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
									</td>
									<td align="center" width="80">
									<input type="text" size="15" value="<%=strQtyReturned%>" name="Txt_QtyReturned"<%=cnt%> class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
									</td>
									<td align="center" width="80"><%=strSupControlNumber%></td>
								</tr>
	<% 
	}
	 %>
	 	 </tr>
	 	 <tr><td colspan="6" height="1" bgcolor="#666666"></td></tr>
</table>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>