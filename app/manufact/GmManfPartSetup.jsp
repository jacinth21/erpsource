 <%
/**********************************************************************************
 * File		 		: GmManfPartSetup.jsp
 * Desc		 		: This screen is used for the setting up Part Number needs for Manufacturing team
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!-- manufact\GmManfPartSetup.jsp -->



<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>

<%

try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	String strhAction = "";

	strhAction = (String)request.getAttribute("hAction")==null?"Add":(String)request.getAttribute("hAction");

	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmLoadDetails = (HashMap)hmReturn.get("LOADDETAILS");

	ArrayList alDevClass = new ArrayList();
	alDevClass = (ArrayList)hmLoadDetails.get("CLASS");
	ArrayList alProdMat = new ArrayList();
	alProdMat = (ArrayList)hmLoadDetails.get("PRODMAT");
	ArrayList alProjList = new ArrayList();
	alProjList = (ArrayList)hmLoadDetails.get("PROJLIST");
	ArrayList alProdFly = new ArrayList();
	alProdFly = (ArrayList)hmLoadDetails.get("PRODFLY");
	ArrayList alMeasDev = new ArrayList();
	alMeasDev = (ArrayList)hmLoadDetails.get("MEASDEV");
	ArrayList alMaterialList = new ArrayList();
	alMaterialList = (ArrayList)hmLoadDetails.get("MATSPEC");
	ArrayList alPackaging = new ArrayList();
	alPackaging = (ArrayList)hmLoadDetails.get("PACK");

	int intSize = 0;

	String strCodeID = "";
	HashMap hcboVal = null;
	String strSelected = "";
	String strClassId = "4031";
	String strProjId = "";
	String strProdMat = "";
	String strProdFly = "";
	String strMeasId = "4080";
	String strProjName = "";
	String strPartNum = "";
	String srtPartDesc = "";
	String strParentProj = "";
	String strParentProjId = "";
	String strMatSp = "";
	String strDraw = "";
	String strDrawRev = "";
	String strPrimPack = "";
	String strSecPack = "";
	String strMatCert = "";
	String strCompCert = "";
	String strInspectRev = "";
	boolean bolFl = false;

	if (strhAction.equals("Lookup"))
	{
		HashMap hmPartNumDetails = (HashMap)hmReturn.get("PARTNUMDETAILS");
		if (!hmPartNumDetails.isEmpty())
		{
			strPartNum = (String)hmPartNumDetails.get("PARTNUM");
			srtPartDesc = (String)hmPartNumDetails.get("PDESC");
			strParentProjId = (String)hmPartNumDetails.get("PROJID");
			strProdFly = GmCommonClass.parseNull((String)hmPartNumDetails.get("PFLY"));
			strProdMat = GmCommonClass.parseNull((String)hmPartNumDetails.get("PMAT"));
			strClassId = GmCommonClass.parseNull((String)hmPartNumDetails.get("PCLASS"));
			strMeasId = GmCommonClass.parseNull((String)hmPartNumDetails.get("MDEV"));
			strMatSp = GmCommonClass.parseNull((String)hmPartNumDetails.get("MSPEC"));
			strDraw = GmCommonClass.parseNull((String)hmPartNumDetails.get("DRAW"));
			strDrawRev = GmCommonClass.parseNull((String)hmPartNumDetails.get("REV"));
			strPrimPack = GmCommonClass.parseNull((String)hmPartNumDetails.get("PRIM"));
			strSecPack = GmCommonClass.parseNull((String)hmPartNumDetails.get("SEC"));
			strMatCert = GmCommonClass.parseNull((String)hmPartNumDetails.get("MATCERT"));
			strCompCert = GmCommonClass.parseNull((String)hmPartNumDetails.get("COMPCERT"));
			strInspectRev = GmCommonClass.parseNull((String)hmPartNumDetails.get("INSPREV"));
			
			
			strProjId = (String)request.getAttribute("hProjId")==null?"":(String)request.getAttribute("hProjId");
			bolFl = true;
		}
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Part Number Setup </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>
function fnCreatePart()
{
	var obj = document.Main;
	var fld1 = obj.Cbo_Field1.value;
	var fld2 = obj.Txt_Field2.value;
	var fld3 = obj.Cbo_Field3.value;
	var fld4 = obj.Txt_Field4.value;
	var partNum = fld1+fld2+"."+fld3+fld4;
	obj.Txt_PartNum.value = partNum;
}

function fnShowCreator()
{
	if (document.all.PerfDimDiv.style.display == "block")
	{
		document.all.PerfDimDiv.style.display = "none";
	}
	else
	{
		document.all.PerfDimDiv.style.display = "block";
	}
}

function fnSetProjNum(obj)
{
	var val = obj.value;
	val = val.substring(5,7);
	document.Main.Txt_Field2.value = val;
}

function fnSubmit()
{
	if (document.Main.Txt_AFl.checked==true)
	{
		document.Main.Txt_AFl.value="Y";
	}
	fnStartProgress('Y');
	document.Main.submit();
}

function fnEdit()
{
	if (document.Main.Txt_AFl.checked==true)
	{
		document.Main.Txt_AFl.value="Y";
	}
	document.Main.hAction.value = "Edit";
	document.Main.submit();
}

function fnCallLookup()
{
	document.Main.hAction.value = "Lookup";
	fnStartProgress('Y');
	document.Main.submit();
}

function fnDuplicate()
{
	document.Main.hAction.value = "Duplicate";
	document.Main.submit();
}

function fnReset()
{
	document.Main.hAction.value = "Load";
	document.Main.submit();
}

function fnLoad()
{
	var obj = document.Main.Chk_MatCert;
	var val = '<%=strMatCert%>';
	if (val == 'Y')
	{
		obj.selectedIndex = '1';
	}
	else if (val == 'N')
	{
		obj.selectedIndex = '2';
	}
	else if (val == 'A')
	{
		obj.selectedIndex = '3';
	}

	var obj1 = document.Main.Chk_CompCert;
	var val = '<%=strCompCert%>';
	if (val == 'Y')
	{
		obj1.selectedIndex = '1';
	}
	else if (val == 'N')
	{
		obj1.selectedIndex = '2';
	}
	else if (val == 'A')
	{
		obj1.selectedIndex = '3';
	}
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad();">
<FORM name="Main" method="POST" action="<%=strServletPath%>/GmPartNumServlet">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hParentProjId" value="<%=strParentProjId%>">
<input type="hidden" name="hMatCert" value="<%=strMatCert%>">
<input type="hidden" name="hCompCert" value="<%=strCompCert%>">

	<table border="0" width="650" cellspacing="0" cellpadding="0">
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
			<td class="RightDashBoardHeader" height="28">&nbsp;Part Number Setup</td>
			<td bgcolor="#666666" width="1" rowspan="3"></td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="648" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightText" align="right" height="24">Choose Project:</td>
						<td>&nbsp;<select name="Cbo_AllProj" class="RightText" onChange="javascript:fnSetProjNum(this)">
								<option value="0" selected>[Choose One]
<%
			  		intSize = alProjList.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alProjList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strProjId.equals(strCodeID)?"selected":"";
						strProjName = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
						if (strParentProjId.equals(strCodeID))
						{
							strParentProj = GmCommonClass.getStringWithTM((String)hcboVal.get("NAME"));
						}


%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=strCodeID%> / <%=strProjName%></option>
<%
			  		}

%>
							</select>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24">New Part Number:</td>
						<td width="448">&nbsp;<input type="text" size="10" value="<%=strPartNum%>" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;&nbsp;<gmjsp:button value="Lookup" gmClass="button" tabindex="-1" buttonType="Load" onClick="fnCallLookup();"/>
						&nbsp;&nbsp;<gmjsp:button value="Creator" buttonType="Save" onClick="fnShowCreator();" gmClass="button" tabindex="-1"/>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
<%
	if (strhAction.equals("Lookup") && bolFl)
	{	
%>
					<tr>
						<td height="24" align="right" class="RightTextBlue">Parent Project: </td>
						<td class="RightTextBlue" id="parproj">&nbsp;<%=strParentProj%></td>
					<tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
<%}%>
					<tr>
						<td colspan="2">
						  <DIV id="PerfDimDiv" style="display:none;">
							<table width="100%" border="0" cellspacing="0">
								<tr><td class="Line" colspan="6"></td></tr>
								<tr class="ShadeRightTableCaption">
									<td  HEIGHT="24" colspan="6"><b>Part Number Creator</b></td>
								</tr>
								<tr>
									<td class="RightText">Part Type: <br>
										<SELECT name="Cbo_Field1" class="InputArea">
											<OPTION value="1">Titanium Implants
											<OPTION value="2">Stainless Steel Implants
											<OPTION value="3">PEEK Implants
											<OPTION value="4">Polymer Implants
											<OPTION value="5">Co-Cr-Mb
											<OPTION value="6">Instruments
											<OPTION value="9">Graphic Cases
										</SELECT>
									</td>
									<td class="RightText">Project Number<br>
										<input class="RightText" readonly type="text" name="Txt_Field2" size="3" value="">
									</td>
									<td class="RightText">Family Number<br>
										<SELECT name="Cbo_Field3" class="InputArea">
											<OPTION value="0">0
											<OPTION value="1">1
											<OPTION value="2">2
											<OPTION value="3">3
											<OPTION value="4">4
											<OPTION value="5">5
											<OPTION value="6">6
											<OPTION value="7">7
											<OPTION value="8">8
											<OPTION value="9">9
										</SELECT>
									</td>
									<td class="RightText">Dimension/Size Indicator<br>
										<input class="RightText" type="text" name="Txt_Field4" size="3" maxlength="2" value="">
									</td>
									<td>
										<gmjsp:button gmClass="InputArea" name="Btn_Create" value="Create" buttonType="Save" onClick ="fnCreatePart()"  style="WIDTH: 50px; HEIGHT: 16px"/>
									</td>
									<td width="10%"></td>
								</tr>
								<tr><td class="Line" colspan="6"></td></tr>
							</table>
						 </DIV>
						</td>
					</tr>

					<tr>
						<td class="RightText" align="right" valign="top" height="24">Description:</td>
						<td>&nbsp;<TEXTAREA type="text" class="RightText" rows=3 cols=50 name="Txt_PartDesc"><%=srtPartDesc%></TEXTAREA></td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" align="right" height="24">Product Family:</td>
						<td>&nbsp;<select name="Cbo_ProdFly" class="RightText">
								<option value="0" selected>[Choose One]
<%
			  		intSize = alProdFly.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alProdFly.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strProdFly.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" align="right" height="24">Product Material:</td>
						<td>&nbsp;<select name="Cbo_ProdMat" class="RightText">
								<option value="0" selected>[Choose One]
<%
			  		intSize = alProdMat.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alProdMat.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strProdMat.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" align="right" height="24">Classification:</td>
						<td>&nbsp;<select name="Cbo_Class" class="RightText">
								<option value="0" selected>[Choose One]
<%
			  		intSize = alDevClass.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alDevClass.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strClassId.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" align="right" height="24">Active ?:</td>
						<td><input type="checkbox" value="" name="Txt_AFl" checked onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=3></td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" align="right" height="24">Measuring Device:</td>
						<td>&nbsp;<select name="Cbo_MesDev" class="RightText">
								<option value="0" selected>[Choose One]
<%
			  		intSize = alMeasDev.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alMeasDev.get(i);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strMeasId.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
%>
							</select>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24">Material Specification:</td>
						<td>&nbsp;<SELECT name="Cbo_Mat" class="InputArea">
										<option value="0" >[Choose One]
<%
			  			intSize = alMaterialList.size();
						hcboVal = new HashMap();
				  		for (int i=0;i<intSize;i++)
				  		{
				  			hcboVal = (HashMap)alMaterialList.get(i);
			  				strCodeID = (String)hcboVal.get("CODEID");
							strSelected = strMatSp.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  			}
%>
										</SELECT>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24">Drawing Number:</td>
						<td>&nbsp;<input type="text" size="10" value="<%=strDraw%>" name="Txt_Draw" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24">Rev Number:</td>
						<td>&nbsp;<input type="text" size="3" value="<%=strDrawRev%>" name="Txt_Rev" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24">Primary Packaging:</td>
						<td>&nbsp;<SELECT name="Cbo_Prim" class="InputArea">
										<option value="0" >[Choose One]
<%
			  			intSize = alPackaging.size();
						hcboVal = new HashMap();
				  		for (int i=0;i<intSize;i++)
				  		{
				  			hcboVal = (HashMap)alPackaging.get(i);
			  				strCodeID = (String)hcboVal.get("CODEID");
							strSelected = strPrimPack.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  			}
%>
										</SELECT>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24">Secondary Packaging:</td>
						<td>&nbsp;<SELECT name="Cbo_Sec" class="InputArea" >
										<option value="0" >[Choose One]
<%
			  			intSize = alPackaging.size();
						hcboVal = new HashMap();
				  		for (int i=0;i<intSize;i++)
				  		{
				  			hcboVal = (HashMap)alPackaging.get(i);
			  				strCodeID = (String)hcboVal.get("CODEID");
							strSelected = strSecPack.equals(strCodeID)?"selected":"";
%>
							<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  			}
%>
										</SELECT>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" align="right" height="24">Material Cert Required ?:</td>
						<td>&nbsp;<SELECT name="Chk_MatCert" class="InputArea" >
										<option value="0" >[Choose One]
										<option value="Y" >Yes
										<option value="N" >No
										<option value="A" >N/A
							</select>
						</td>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" align="right" height="24">Compliance Cert Required ?:</td>
						<td>&nbsp;<SELECT name="Chk_CompCert" class="InputArea" >
										<option value="0" >[Choose One]
										<option value="Y" >Yes
										<option value="N" >No
										<option value="A" >N/A
							</select>
					</tr>
					<tr><td colspan="2" bgcolor="#eeeeee" height="1"></td></tr>
					<tr>
						<td class="RightText" width="200" align="right" HEIGHT="24">Inspection Rev Number:</td>
						<td>&nbsp;<input type="text" size="3" value="<%=strInspectRev%>" name="Txt_InspRev" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>
						</td>
					</tr>
				   <tr>
						<td colspan="2" align="center" height="30">&nbsp;
<%
	if (strhAction.equals("Lookup") && bolFl)
	{	
%>
						<gmjsp:button value="Add to Current Project" gmClass="button" buttonType="Save" onClick="javascript:fnDuplicate();" tabindex="15"/>&nbsp;		
						<gmjsp:button value="&nbsp;Edit&nbsp;" gmClass="button" buttonType="Save" onClick="javascript:fnEdit();" tabindex="15"/>&nbsp;
<%}else{%>
						<gmjsp:button value="Submit" gmClass="button" buttonType="Save" onClick="javascript:fnSubmit();" tabindex="15"/>&nbsp;
<%}%>
						<gmjsp:button value="Reset" gmClass="button" tabindex="16" buttonType="Save" onClick="javascript:fnReset();"/>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="1" bgcolor="#666666"></td>
		</tr>
    </table>
</FORM>

<%@ include file="/common/GmFooter.inc" %>
</BODY>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</HTML>
