 <%
/**********************************************************************************
 * File		 		: GmMFGDHRPrintReviewForm0.jsp
 * Desc		 		: This screen is used for the printing DHR sheets for the Manufacturing process
 * Version	 		: 1.0
 * author			: Karthik 
************************************************************************************/
%>

<!-- manufact\GmMFGDHRPrintReviewForm0.jsp -->



<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);

	HashMap hmReturn = new HashMap();
	hmReturn = (HashMap)request.getAttribute("hmReturn");

	HashMap hmDHRDetails = new HashMap();
	ArrayList alSubDHR = new ArrayList();

	String strDHRId = "";
	String strVendorId = "";
	String strVendName = "";
	String strPartNum = "";
	String strDesc = "";
	String strWOId = "";
	String strControlNum = "";
	String strQtyRec = "";
	String strQtyRej = "";
	String strManufDate = "";
	String strUserName = "";
	String strCreatedDate = "";
	String strQtyOrdered = "";
	String strProjId = "";
	String strPackPrim = "";
	String strPackSec = "";
	String strLabel = "";
	String strInsert = "";
	String strSubPartNum = "";
	String strSubPartDesc = "";
	String strSubControl = "";
	String strSubManfDt = "";
	String strSterFl = "";
	String strApplDateFmt = strGCompDateFmt;
	String strCompanyLogo = "";
	if (hmReturn != null)
	{
		hmDHRDetails = (HashMap)hmReturn.get("DHRDETAILS");
		strCompanyLogo = GmCommonClass.parseNull((String) hmReturn.get("LOGO"));

		strDHRId = GmCommonClass.parseNull((String)hmDHRDetails.get("ID"));
		strVendorId = GmCommonClass.parseNull((String)hmDHRDetails.get("VID"));
		strVendName = GmCommonClass.parseNull((String)hmDHRDetails.get("VNAME"));
		strPartNum = GmCommonClass.parseNull((String)hmDHRDetails.get("PNUM"));
		strDesc = GmCommonClass.parseNull((String)hmDHRDetails.get("PDESC"));
		strWOId = GmCommonClass.parseNull((String)hmDHRDetails.get("WOID"));
		strControlNum = GmCommonClass.parseNull((String)hmDHRDetails.get("CNUM"));
		strQtyRec = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREC"));
		strManufDate =GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("MDATE"),strApplDateFmt);
		strUserName = GmCommonClass.parseNull((String)hmDHRDetails.get("UNAME"));
		strCreatedDate =GmCommonClass.getStringFromDate((java.util.Date)hmDHRDetails.get("CDATE"),strApplDateFmt); 
		strQtyOrdered = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYORD"));
		strProjId = GmCommonClass.parseNull((String)hmDHRDetails.get("PROJID"));
		strPackPrim = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKPRIM"));
		strPackSec = GmCommonClass.parseNull((String)hmDHRDetails.get("PACKSEC"));
		strInsert = GmCommonClass.parseNull((String)hmDHRDetails.get("INSID"));
		strQtyRej = GmCommonClass.parseNull((String)hmDHRDetails.get("QTYREJ"));
		strLabel = strPartNum.replaceAll("\\.","");
		strSterFl = GmCommonClass.parseNull((String)hmDHRDetails.get("STERFL"));
		alSubDHR = (ArrayList)hmReturn.get("SUBDHRDETAILS");
	}

	int intSize = 0;
	HashMap hcboVal = null;
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Mfg. DHR Print Version </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script>
function fnPrint()
{
	window.print();
}
var tdinnner = "";
function hidePrint()
{
	strObject = eval("document.all.button");
	tdinnner = strObject.innerHTML;
	strObject.innerHTML = "";
}

function showPrint()
{
	strObject = eval("document.all.button");
	strObject.innerHTML = tdinnner ;
}
</script>
</HEAD>

<BODY leftmargin="20" topmargin="20"  onbeforeprint="hidePrint();" onafterprint="showPrint();">
<FORM name="frmOrder" method="POST" action="<%=strServletPath%>/GmPOReceiveServlet">
<input type="hidden" name="hWOId" value="">
<input type="hidden" name="hPOId" value="<%=strDHRId%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="hAction" value=""> 

	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td bgcolor="#666666" rowspan="15" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
			<td bgcolor="#666666" colspan="4" height="1"></td>
			<td bgcolor="#666666" rowspan="15" width="1"><img src="<%=strImagePath%>/spacer.gif" width="1"></td>
		</tr>
		<tr>
			<td height="80" width="170"><img src="<%=strImagePath%>/<%=strCompanyLogo %>.gif" width="138" height="60"></td>
			<td class="RightText" width="130">&nbsp;</td>
			<td class="RightText" width="1"bgcolor="#666666"></td>
			<td align="right" class="RightText"><font size=+2>In-House Manufacturing <br>Device History Record&nbsp;<br>Review Form&nbsp;</font></td>
		</tr>
		<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
		<tr><td bgcolor="#eeeeee" height="8" colspan="4"></td></tr>
		<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
		<tr>
			<td colspan="4">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td class="RightTableCaption" align="right">Part Number:</td>
						<td class="RightText">&nbsp;<%=strPartNum%></td>
						<td class="RightTableCaption" align="right">DHR Id:</td>
						<td class="RightText">&nbsp;<%=strDHRId%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right">Part Description:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strDesc)%></td>
						<td class="RightText" height="30" align="right">Work Order Id:</td>
						<td class="RightText">&nbsp;<%=strWOId%></td>
					</tr>
					<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b>Receipts Routing</b><font size="-2"> (from Product Traveler)</font>:</td></tr>
					<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right">Date of Manufacturing:</td>
						<td class="RightText">&nbsp;<%=strManufDate%></td>
						<td class="RightText" height="30" align="right">Quantity:</td>
						<td class="RightText">&nbsp;<%=strQtyRec%></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right">Lot Number:</td>
						<td class="RightText">&nbsp;<%=strControlNum%></td>
						<td class="RightText" height="30" align="right">Quantity Released:</td>
						<td class="RightText">&nbsp;</td>
					</tr>
					<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b>Acceptance Records</b><font size="-2"> (attached)</font>:</td></tr>
					<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right">Bill of Materials:</td>
						<td class="RightText">&nbsp;<input type="checkbox">Yes&nbsp;&nbsp;<input type="checkbox">No</td>
						<td class="RightText" height="30" align="right">Batch Record<font size="-2"> (if Appl)</font>:</td>
						<td class="RightText">&nbsp;<input type="checkbox">Yes&nbsp;&nbsp;<input type="checkbox">No</td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right">Work Order Forms:</td>
						<td class="RightText">&nbsp;<input type="checkbox">Yes&nbsp;&nbsp;<input type="checkbox">No</td>
						<td class="RightText" height="30" align="right">Label Reconciliation Form:</td>
						<td class="RightText">&nbsp;<input type="checkbox">Yes&nbsp;&nbsp;<input type="checkbox">No</td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td class="RightText" height="30" align="right">Product Traveler:</td>
						<td class="RightText">&nbsp;<input type="checkbox">Yes&nbsp;&nbsp;<input type="checkbox">No</td>
						<td class="RightText" height="30" align="right">Product Specification Inspection Form:</td>
						<td class="RightText">&nbsp;<input type="checkbox">Yes&nbsp;&nbsp;<input type="checkbox">No</td>						
					</tr>
					<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
					<tr><td class="RightText" bgcolor="#eeeeee" height="25" colspan="6">&nbsp;<b>Review Checklist</b>:</td></tr>
					<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
					<tr>
						<td colspan="3" height="40"></td>
						<td class="RightText">PERFORMED</td>
					</tr>
					<tr>
						<td colspan="3" class="RightText" height="30" align="right">Bill of Materials is complete:</td>
						<td class="RightText"><img src="<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td colspan="3" class="RightText" height="30" align="right">Batch Record is complete and fully approved:</td>
						<td class="RightText"><img src="<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td colspan="3" class="RightText" height="30" align="right">Correct labels and inserts were used:</td>
						<td class="RightText"><img src="<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td colspan="3" class="RightText" height="30" align="right">Finished device inspection was performed:</td>
						<td class="RightText"><img src="<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td colspan="3" class="RightText" height="30" align="right">Count was performed:</td>
						<td class="RightText"><img src="<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td colspan="3" class="RightText" height="30" align="right">Device History Record is complete and accurate:</td>
						<td class="RightText"><img src="<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#eeeeee" colspan="4" height="1"></td></tr>
					<tr>
						<td colspan="3" class="RightText" height="30" align="right">Work Order is complete and approved:</td>
						<td class="RightText"><img src="<%=strImagePath%>/n.gif></td>
					</tr>
					<tr><td bgcolor="#666666" colspan="4"  height="1"></td></tr>
				</table>
			</td>
		</tr>
	</table>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td class="RightText" bgcolor="#eeeeee" height="20" align="center">This production lot has met all quality requirements.</td>
		</tr>
	</table>
	<BR>
	<BR>
	<BR>
	<BR>
	<table cellpadding="0" cellspacing="0" border="0" width="700">
		<tr>
			<td width="250"bgcolor="#666666" height="1"></td>
			<td height="1" width="200"></td>
			<td  width="150" bgcolor="#666666" height="1"></td>
		</tr>
		<tr>
			<td class="RightText" align="center" height="10"> Authorized Signature for Release</td>
			<td class="RightText">&nbsp;</td>
			<td class="RightText" align="center">Date</td>
		</tr>
	</table>
	<BR>
	<BR>
	<span class="RightText">GM-G008; Rev. 0; DCO#06-000215</span>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>

</HTML>
