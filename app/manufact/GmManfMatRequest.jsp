 <%@page import="java.util.Date"%>
<%
/**********************************************************************************
 * File		 		: GmManfMatRequest.jsp
 * Desc		 		: This screen is used for the requesting Material for a Manufacturing Work Order / DHR
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!-- manufact\GmManfMatRequest.jsp -->



<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
 <%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%
String strManuFactJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_MANUFACT");
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
		
	String strhAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	String strhOpt = GmCommonClass.parseNull((String)request.getAttribute("OPT"));
	String strErrorMessage = GmCommonClass.parseNull((String)request.getAttribute("ERRORMESSAGE"));
	
	String strHeader = strhOpt.equals("REQREL")?"Issue Materials":"Add Supplies";
	
	String strApplDateFmt = strGCompDateFmt;
	String strDTDateFmt = "{0,date,"+strApplDateFmt+"}";
	String strMfgWODhrId = "";
	String strPartDesc = "";
	String strOrdComplete = "";
	String strOrdPending = "";
	String strPartNum = "";
	String strControlNum = "";
	String strWoId	= "";
	String strCDate = "";
	String strCUser = "";
	String strMfgQty = "";
	String strBOMid = "";
	String strVendorId = "";
	String strMfgLotCodeId = "";
	String strFetchPartNum = "";
	String strFetchPartDesc = "";
	String strFetchPartQty = "";
	String strInvQty = "";
	String strRelDisabled = "true";
	HashMap hmTemp = new HashMap();
	String strDisabled = "";
	String strRWChkFl = "";
	String strUpdBtnDisabled = "";
	
	
	String strMatReqId = GmCommonClass.parseNull((String)request.getAttribute("MATREQID"));
	// String strLotCode = GmCommonClass.parseNull((String)request.getAttribute("LOTCODE"));
	int intSize = 0;
	int intStatusFl = 0;
	int intSizeFchPartDetails = 0;
	int intRowCnt = -1;
	int intDHRStatusFl = 0;
	int intDHRStatFl = 0; 
	
	HashMap hmMfgWO = (HashMap)request.getAttribute("HMLOADWO");
	HashMap hmPartDetails = (HashMap)request.getAttribute("PARTDETAILS");
	//strMfgWODhrId = GmCommonClass.parseNull((String)request.getAttribute("MFGWODHRID"));
	strMfgLotCodeId = GmCommonClass.parseNull((String)request.getAttribute("MFGLOTCODEID"));
	
	log.debug(" Lot Code ID is " +strMfgLotCodeId );
	
	strhAction = GmCommonClass.parseNull((String)request.getAttribute("ACTION"));
	String strrawfl = GmCommonClass.parseNull((String)request.getAttribute("RWFLAG"));	
	ArrayList alMatReqItemDetails = (ArrayList)request.getAttribute("MATREQITEMDETAILS");
	HashMap hmFchAddPart = (HashMap)request.getAttribute("hmFCHADDPART");
	ArrayList alFetchPartDetails = new ArrayList();
	HashMap hmInputPartInfo = new HashMap();
	log.debug(" hmFchAddPart values are " +hmFchAddPart);
		
	if (hmFchAddPart !=null && (hmFchAddPart.size() > 0))
	{
	 	alFetchPartDetails = (ArrayList)hmFchAddPart.get("ALPARTDETAILS");
	 	intSizeFchPartDetails = alFetchPartDetails.size();
	 	hmInputPartInfo = (HashMap) hmFchAddPart.get("HMINPUTVAL");
	 	intRowCnt = intSizeFchPartDetails;
	}
	
	log.debug(" value of intRowCnt --  "+intRowCnt);

	if (hmMfgWO != null)
	{
		strOrdComplete = GmCommonClass.parseNull((String)hmMfgWO.get("QTYDONE"));
		strOrdPending = GmCommonClass.parseNull((String)hmMfgWO.get("QTYPDG"));
		strBOMid = GmCommonClass.parseNull((String)hmMfgWO.get("BOMID"));
		strVendorId =  GmCommonClass.parseNull((String)hmMfgWO.get("VENDID"));
	}
	
	if (hmPartDetails!=null)
	{
		log.debug(" hmPartDetails values are " + hmPartDetails);
		strMfgWODhrId = GmCommonClass.parseNull((String)hmPartDetails.get("DHRID"));
		strPartNum = GmCommonClass.parseNull((String)hmPartDetails.get("PARTNUM"));
		strPartDesc = GmCommonClass.parseNull((String)hmPartDetails.get("PARTDESC"));
		strControlNum = GmCommonClass.parseNull((String)hmPartDetails.get("CNUM"));
		strWoId = GmCommonClass.parseNull((String)hmPartDetails.get("WOID"));
		//strCDate = GmCommonClass.getStringFromDate((Date)hmPartDetails.get("CDATE"),strApplDateFmt);
		strCDate = GmCommonClass.getStringFromDate((java.util.Date)hmPartDetails.get("CDATE"),strApplDateFmt);
		strCUser = GmCommonClass.parseNull((String)hmPartDetails.get("CUSER"));
		strMfgQty = GmCommonClass.parseNull((String)hmPartDetails.get("RQTY"));
		strRWChkFl = GmCommonClass.parseNull((String)hmPartDetails.get("RWFL"));
		intDHRStatFl = Integer.parseInt(GmCommonClass.parseNull((String)hmPartDetails.get("SFL")));
	}
	strrawfl = strRWChkFl.equals("Y")?"Yes":"No";

	if (alMatReqItemDetails!=null)
	{	
		intSize = alMatReqItemDetails.size();
	}
	 
%>
<%
String strWikiIssueMaterial = GmCommonClass.getWikiTitle("ISSUE_MAT");
String strWikiAddSupply = GmCommonClass.getWikiTitle("ADD_SUPPLIES");
String strWikiTitle = strhOpt.equals("REQREL") ? strWikiIssueMaterial : strWikiAddSupply;
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Manufacturing Work Order Initiate </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strManuFactJsPath%>/GmManfMatRequest.js"></script>
<script>

var objPartCell;
var varUpdateFlag;
var rawfl = '<%=strRWChkFl%>';
var strMfgWODhrId = '<%=strMfgWODhrId%>';
var strMatReqId = '<%=strMatReqId%>';
var strVendorId = '<%=strVendorId%>';
var strWOId = '<%=strWoId%>';

</script>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmMfg" method="POST" action="<%=strServletPath%>/GmMFGInitiateWOServlet">
<input type="hidden" name="hType" value="">
<input type="hidden" name="hPartNum" value="<%=strPartNum%>">
<input type="hidden" name="hOrdQty" value="<%=strPartDesc%>">
<input type="hidden" name="hOrdComplete" value="<%=strOrdComplete%>">
<input type="hidden" name="hOrdPending" value="<%=strOrdPending%>">
<input type="hidden" name="hAction" value="<%=strhAction%>">
<input type="hidden" name="hOpt" value="<%=strhOpt%>">
<input type="hidden" name="hVendId" value="<%=strVendorId%>">
<input type="hidden" name="hMatReqId" value="<%=strMatReqId%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hMfgWODHRId" value="<%=strMfgWODhrId%>">
<input type="hidden" name="hMfgQty" value="<%=strMfgQty%>">
<input type = "hidden" name="hAddPartCnt" value="<%=intRowCnt%>" >
<input type="hidden" name="hTxnId">
<input type="hidden" name="hCancelType">

	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="24" class="RightDashBoardHeader">&nbsp;<%=strHeader%></td>
					<td height="24" class="RightDashBoardHeader" align="right"><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /></td>
				</tr>
				</table>
			</td>
		</tr>
		<tr><td bgcolor="#666666" height="1" colspan="3"></td></tr>
		<tr>
			<td width="848" height="30" valign="top" align="center">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr><td colspan="2" height="3" ></td></tr>
					<tr HEIGHT="30" >
						<td align="right" class="RightTableCaption" valign="center">&nbsp;Lot Number:</td>
						<td class="RightText" width="620" valign="top">&nbsp;<input type="text" size="15" value="<%=strMfgLotCodeId%>" name="Txt_MfgLotCodeId" class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex=1>&nbsp;&nbsp;&nbsp;
						<gmjsp:button value="&nbsp;Load&nbsp;" gmClass="button" buttonType="Load" onClick="fnLoadLotCode();" tabindex="13"/>					</td>
					 </tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
<%
	if (strhAction.equals("Reload"))
	{ 
%>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Part Number:</td>
						<td class="RightText">&nbsp;<%=strPartNum%>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Part Description:</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Qty to Manufacture:</td>
						<td>
						<table border="0" width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td class="RightText" width="10%">&nbsp;<%=strMfgQty%></td>
								<td class="RightTableCaption" width="15%" align="right" HEIGHT="25">&nbsp;Move to Restricted Warehouse:</td>
								<td class="RightText" width="15%">&nbsp;<%=strrawfl%></td>
							</tr>
						</table>
								</td>				
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Globus' Work Order ID:</td>
						<td class="RightText">&nbsp;<%=strWoId%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Created Date:</td>
						<td class="RightText">&nbsp;<%=strCDate%></td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#eeeeee"></td></tr>
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="25">&nbsp;Created By:</td>
						<td class="RightText">&nbsp;<%=strCUser%></td>
					</tr>															
					<tr>
						<td colspan="2">
						<display:table name="MATREQUEST"  class = "its" id="currentRowObject" decorator="com.globus.displaytag.beans.DTMfgMaterialRequestWrapper"> 
						<display:column property="MATREQID" title="Request Id" class="alignright"  sortable="true" />
						<display:column property="REQDATE" title="Request Date" class="alignright" format="<%=strDTDateFmt%>" />
					  	<display:column property="REQUESTOR" title="Requested By" class="alignleft" sortable="true"  />
					  	<display:column property="STATUSFL" title="Status" class="alignleft" />
						</display:table>
						</td>
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
			    </table>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="aligncenter">
<%
				if (intSize > 0)
				{
%>			
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr class="ShadeRightTableCaption">
						<td colspan="8" height="20">Request Details for <%=strMatReqId%></td>
					</tr>
					<tr><td colspan="8" height="1" bgcolor="#666666"></td></tr>
					<tr class="ShadeRightTableCaption">
						<td height="20" width="100" class="RightText">Part Number</td>
						<td width="300" class="RightText">Description</td>
						<td width="60" class="RightText">Qty Req</td>
						
<%
			if (strhOpt.equals("REQREL"))
			{
%>
						<td width="100" align="center" class="RightText">Qty <BR>On Hand</td>
						<td width="100" class="RightText">Qty To Release </td>
                        <td width="100" class="RightText">Control Number</td>

<%
			}else{
%>						
						<td width="200">&nbsp;</td>
						<td width="60"></td>
						<td width="100"></td>
<%
			}
%>
					</tr>
					<tr><td colspan="8" height="1" bgcolor="#666666"></td></tr>
<%					
					HashMap hcboVal = new HashMap();
					String strQtyInBulk = "";

					HashMap hmLoop = new HashMap();
					HashMap hmTempLoop = new HashMap();

					String strNextPartNum = "";
					int intCount = 0;
					String strColor = "";
					String strLine = "";
					String strPartNumHidden = "";
					int intSetQty = 0;
					boolean bolQtyFl = false;
					String strReadOnly = "";
					StringBuffer sbPartNum = new StringBuffer();
					String strItemQty = "";
					String strQty = "";
					String strFlag = "";
					String strCheckedOpt = "";
					String strVerFlag = "";
					String strLastUpdNm = "";
					String strLastUpdDt = "";
					String strControl = "";
					String strStatusFl = "";
					String strQtyInBulkWhole = "";
					String strDHRStatusFl = ""; 
					double dbQty = 0.0;
					double dbBulkQty = 0.0;
					
			  		for (int i=0;i<intSize;i++)
			  		{
						hmLoop = (HashMap)alMatReqItemDetails.get(i);
						if (i<intSize-1)
						{
							hmTempLoop = (HashMap)alMatReqItemDetails.get(i+1);
							strNextPartNum = GmCommonClass.parseNull((String)hmTempLoop.get("PARTNUM"));
						}
						strPartNum = GmCommonClass.parseNull((String)hmLoop.get("PARTNUM"));
						//single quotes added for partnum before appending for PC-307
						sbPartNum.append("'"+strPartNum+"'");
						sbPartNum.append(",");
						strPartNumHidden = strPartNum;
						strPartDesc = GmCommonClass.parseNull((String)hmLoop.get("PARTDESC"));
						strQty = GmCommonClass.parseZero((String)hmLoop.get("QTY"));
						dbQty = Double.parseDouble(strQty);
				  		strQtyInBulk = GmCommonClass.parseZero((String)hmLoop.get("QTYONHAND"));
						strQtyInBulkWhole = strQtyInBulk;
						dbBulkQty = Double.parseDouble(strQtyInBulk);
					//	dbBulkQty = dbBulkQty + dbQty;
						if (strhOpt.equals("REQREL"))
						{
							strQtyInBulk = GmCommonClass.getStringWithCommas(String.valueOf(dbBulkQty),4);
							strQtyInBulkWhole = ""+dbBulkQty; 
						}
						
						if (strPartNum.equals(strNextPartNum))
						{
							intCount++;
							strLine = "<TR><TD colspan=5 height=1 bgcolor=#eeeeee></TD></TR>";
						}
						else
						{
							strLine = "<TR><TD colspan=5 height=1 bgcolor=#eeeeee></TD></TR>";	
								if (intCount > 0)
								{
									strPartNum = "";
									strPartDesc = "";
									strQty = "";
									strQtyInBulk = "";
									strLine = "";
								}
								else
								{
									//strColor = "";
								}
								intCount = 0;
							}

							if (intCount > 1)
							{
								strPartNum = "";
								strPartDesc = "";
								strQty = "";
								strQtyInBulk = "";
								strLine = "";
							}
							
							strItemQty = GmCommonClass.parseNull((String)hmLoop.get("IQTY"));
							strControl = GmCommonClass.parseNull((String)hmLoop.get("CNUM"));
							strStatusFl = GmCommonClass.parseZero((String)hmLoop.get("STATUSFL"));
							strDHRStatusFl = GmCommonClass.parseZero((String)hmLoop.get("DHRSTATUSFL"));
							intStatusFl = Integer.parseInt(strStatusFl);
							intDHRStatusFl= Integer.parseInt(strDHRStatusFl);
							out.print(strLine);
%>
					<tr height="18">
<%
						if (strhOpt.equals("REQREL") && intStatusFl <= 40 && intDHRStatusFl == 0)
						{
							if(intStatusFl == 10 ) 
								{	strDisabled = "disabled";  
									}
							else {	strDisabled = "";
									}
%>
					<% if(intStatusFl < 40) { %>
						<td class="RightText">&nbsp;
							<a class="RightText" tabindex=-1 href="javascript:fnSplit('<%=i%>','<%=strPartNum%>');"><%=strPartNum%></a>
							<!-- replaceAll and hPartNumber added for PC-307 -->
							<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden.replaceAll("[^a-zA-Z0-9]", "")%>">
							<input type="hidden" name="hPartNumber<%=i%>" value="<%=strPartNumHidden%>"></td>
					<% } else{%>
							<td class="RightText">&nbsp;<%=strPartNum%>
							<!-- replaceAll and hPartNumber added for PC-307 -->
							<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden.replaceAll("[^a-zA-Z0-9]", "")%>">
							<input type="hidden" name="hPartNumber<%=i%>" value="<%=strPartNumHidden%>"></td>
						</td>
					<%} %>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
						<!-- replaceAll changed for PC-307 -->
						<td align="right" class="RightText"><%=GmCommonClass.getStringWithCommas(strQty,4)%>&nbsp;&nbsp;<input type="hidden" value="<%=strQty%>" name="Qty_Req<%=strPartNum.replaceAll("[^a-zA-Z0-9]", "")%>"/></td>
						<td align="right" class="RightText"><%=strQtyInBulk%>&nbsp;&nbsp;<input type="hidden" value="<%=strQtyInBulkWhole%>" name="Qty_Blk<%=strPartNum.replaceAll("[^a-zA-Z0-9]", "")%>"/></td>
					<% if(intStatusFl < 40) { %>
						<td id="Cell<%=i%>">&nbsp;&nbsp;&nbsp;&nbsp;<input class='InputArea' style="margin:3px 0" <%=strDisabled%> value="<%=strItemQty%>" type="text" onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); name="Txt_Qty<%=i%>" size="7"></td>
					<% } else{%>
						<td align="center" width="60"><%=strItemQty%></td>
						<input type="hidden" name="hQtyToRelease<%=i%>" value="<%=strItemQty%>"></td>		
					<%} %>
                        <td id="CellCnum<%=i%>"><input class='InputArea'  style="margin:3px 0"  <%=strDisabled%> value="<%=strControl%>" type="text" onFocus=changeBgColor(this,'#AACCE8'); onBlur=changeBgColor(this,'#ffffff'); name="Txt_CNum<%=i%>" id="Txt_CNum<%=i%>" size="10">
                        </td>

<%
						}else{
%>														
						<td class="RightText">&nbsp;<%=strPartNum%>
						<!-- replaceAll and hPartNumber added for PC-307 -->
							<input type="hidden" name="hPartNum<%=i%>" value="<%=strPartNumHidden.replaceAll("[^a-zA-Z0-9]", "")%>">
							<input type="hidden" name="hPartNumber<%=i%>" value="<%=strPartNumHidden%>">
						</td>
						<td class="RightText">&nbsp;<%=GmCommonClass.getStringWithTM(strPartDesc)%></td>
						<td align="center" class="RightText"><%=strQty%></td>
						<td align="center" class="RightText">&nbsp;</td>
						<td align="center" width="60"><%=strItemQty%></td>
						<td align="left" width="100"><%=strControl%></td>
<%
						}
%>
					</tr>
					<tr><td colspan="8" bgcolor="#cccccc"></td></tr>
<%
					} // End of For
					out.print("<input type=hidden name=pnums value="+sbPartNum.toString().substring(0,sbPartNum.length()-1)+">");
%>					
				<input type="hidden" name="hCnt" value="<%=intSize-1%>">
				</table>
<%
				}
			
			if (strhOpt.equals("REQNEW") || strhOpt.equals("FCHUPDPART"))
			{
%>
				<table cellspacing="0" cellpadding="0" border="0" width="100%" id="PartTable">
					<tbody>
					<tr class="ShadeRightTableCaption">
						<td colspan="4" height="20">Request Additional Parts</td>
					</tr>
					<tr>
						<td colspan="4" height="20" class="RightTextRed" align="center"><%=strErrorMessage%></td>
					</tr>
					<tr>
							<TD class="RightText"  align="center">Part Number</TD>
							<TD class="RightText"  align="center">Part Description</TD>
							<TD class="RightText"  align="center">Qty Required</TD>
							<TD class="RightText"  align="center">Shelf Qty</TD>
					</tr>
					<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
<% 
				if(strhOpt.equals("FCHUPDPART"))
				{
					 strRelDisabled = "";
					for (intRowCnt = 0; intRowCnt <= intSizeFchPartDetails-1 ; intRowCnt ++)
					{
						hmTemp = (HashMap)alFetchPartDetails.get(intRowCnt);
						log.debug(" values inside hmTemo is " + hmTemp);
						strFetchPartNum = GmCommonClass.parseNull((String)hmTemp.get("PARTNUM"));
						strFetchPartDesc = GmCommonClass.parseNull((String)hmTemp.get("PARTDESC"));
						strFetchPartQty = GmCommonClass.parseNull((String)hmInputPartInfo.get(strFetchPartNum));
						strInvQty = GmCommonClass.parseNull((String)hmTemp.get("QTYONHAND"));
						
%>			
								<tr>
								<td align="center"  class="RightText" > 	<input type=text size=12  class=InputArea   name="Txt_Part<%=intRowCnt%>" value=<%=strFetchPartNum%> > </td>
								<td  class="RightText" align="center" >&nbsp;<%=strFetchPartDesc%></td>
								<td  class="RightText"  align="center"><input type=text size=4  class=InputArea   name="Txt_QtyQ<%=intRowCnt%>"  value=<%=strFetchPartQty%> ></td>
								<td  class="RightText"  align="center"><input type=text size=4  class=InputArea   name="Txt_INVQtyQ<%=intRowCnt%>"  value=<%=strInvQty%> READONLY></td>
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
								</tr>
<%			
					}   

%>
								<tr><td colspan="4" height="1" bgcolor="#666666"></td></tr>
<%			
				}
%>
					</tbody>
				</table>
				<BR>
				<gmjsp:button value="Add Row" gmClass="button" buttonType="Save" onClick="javascript:fnAddRow('PartTable');"/>&nbsp;&nbsp;
				<gmjsp:button value="Part Search" gmClass="button" buttonType="Load" onClick="javascript:fnPartSearch();"/>&nbsp;&nbsp;
				<gmjsp:button value="Update" gmClass="button" buttonType="Save" onClick="javascript:fnUpdate();"/>&nbsp;&nbsp;
				<gmjsp:button value="Release" gmClass="button" buttonType="Save" disabled="<%=strRelDisabled%>"  onClick="javascript:fnRelease();"/>&nbsp;&nbsp;
				<BR><BR>
<%
			}//else if (strhOpt.equals("REQREL") && !strMatReqId.equals("") && (intStatusFl < 2||intStatusFl == 3))
			else if (strhOpt.equals("REQREL") && !strMatReqId.equals("") &&  intStatusFl < 40)
			{
				//if(intStatusFl < 2 ) {
				if(intStatusFl < 40 && intStatusFl != 10) {
%>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td height="20" class="alignright">Complete Request?</td>
						<td width="450">:&nbsp;<input type="checkbox" name="Chk_Complete">
					</tr>
					<tr><td colspan="2" height="1" bgcolor="#666666"></td></tr>
				</table>
<%
				}
				if(intStatusFl == 10 ) {	
%>				
				<BR><gmjsp:button value="Release" gmClass="button" buttonType="Save" onClick="javascript:fnReleaseBO();"/><BR><BR>
<%				}
			}
%>
				<BR>
				<%
				if(intStatusFl <=40 && intStatusFl != 10 && intDHRStatusFl == 0){
					strUpdBtnDisabled = "";  
				}
				else {
					strUpdBtnDisabled = "true";
				}
				%>					
				<% if( strhOpt.equals("REQREL") && !strMatReqId.equals("") ) { 
				%>
				<gmjsp:button value="Update" gmClass="button"  buttonType="Save" disabled="<%=strUpdBtnDisabled%>" onClick="javascript:fnCompleteReq();"/>
				<% } 
				%>
				<gmjsp:button value="Print WO" name="Btn_ViewPrintWO" gmClass="button" buttonType="Load" onClick="fnViewPrintWO();" />
				<gmjsp:button value="Print DHR" name="Btn_ViewPrint" gmClass="button" buttonType="Load" onClick="fnViewPrint();" />
				<gmjsp:button value="Print BOM" name="Btn_PrintBOM" gmClass="button" buttonType="Load" onClick="fnPrintBOM();" />
				<% 
				if(intStatusFl == 10 || intStatusFl == 20) { 
			%>		
				<gmjsp:button value="Void" name="Btn_PrintBOM" gmClass="button" buttonType="Save" onClick="fnVoid();" />
				
			<%	
				 }
			%>
			<%
			if(intDHRStatFl == 0 ) {
				%>
				 <gmjsp:button value="Void DHR" gmClass="button" buttonType="Save" onClick="javascript:fnVoidDHR();"/>
				 <%
				}
			%>
			
			
			<% 
				if(intStatusFl == 30 ) { 
			%>		
				<gmjsp:button value="Rollback" name="Btn_PrintBOM" gmClass="button" buttonType="Save" onClick="fnRollback();" />
				
			<%	
				 }
			%>	
				
			</td>
		</tr>
<%	
	 }
%>
    <table>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>