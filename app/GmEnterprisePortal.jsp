


<!--globusMedApp\GmEnterprisePortal.jsp-->

<%@page import="com.globus.common.util.GmCookieManager"%>
<%@page import="com.globus.common.beans.GmFramework"%>
<%@page import="java.util.HashMap"%>
<%
/**********************************************************************************
 * File		 		: GmEnterprisePortal.jsp
 * Desc		 		: This screen is used for the all Portals  - Frameset
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%
	if ((String)session.getAttribute("strSessUserId") == null)
	{
		response.sendRedirect(GmCommonClass.getString("GMCOMMON").concat("/GmSessionExpiry.jsp"));
		return;
	}
// to store session values in cookies
GmCookieManager gmCookieManager = new GmCookieManager();
String strCokCompId = GmCommonClass.parseNull(gmCookieManager.getCookieValue(request,"cCompId"));
String strCokJsDateFmt =  GmCommonClass.parseNull(gmCookieManager.getCookieValue(request,"cJsDateFmt"));
String strCokPartyId =  GmCommonClass.parseNull(gmCookieManager.getCookieValue(request,"cPartyId"));
String strCokPlantId =  GmCommonClass.parseNull(gmCookieManager.getCookieValue(request,"cPlantId"));
String strCokPgToLoad =  GmCommonClass.parseNull(gmCookieManager.getCookieValue(request,"strPgToLoad"));

gmCookieManager.deleteCookie(response, "cCompId");
gmCookieManager.deleteCookie(response, "cJsDateFmt");
gmCookieManager.deleteCookie(response, "cPartyId");
gmCookieManager.deleteCookie(response, "cPlantId");
gmCookieManager.deleteCookie(response, "strPgToLoad");

	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strMenuPath = GmCommonClass.getString("GMMENU");
	String strOperPath = GmCommonClass.getString("GMCUSTSERVICE");
	
	String strPgToLoad = (String)session.getAttribute("strSessPgToLoad");
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");
	String strColSize  = "0,*";
	//Once the GmLogonServlet request foward issue fixed then the below attribute will be get from request attribute if cookies string is empty

 	String strCompanyId = strCokCompId.equals("")?GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId")):strCokCompId;
	String strJsDateFmt = strCokJsDateFmt.equals("")? GmCommonClass.parseNull((String) session.getAttribute("strSessJSDateFmt")):strCokJsDateFmt;
	String strPartyId =  strCokPartyId.equals("")? GmCommonClass.parseNull((String) session.getAttribute("strSessPartyId")):strCokPartyId;
	String strPlantId = strCokPlantId.equals("")?  GmCommonClass.parseNull((String) session.getAttribute("strSessPlantId")):strCokPlantId;
	//The below Language id is used to show the Japan Master data in english on sales sales reports and in dropdowns for Non Globus Medical Japan as primary company 
	String strLanguageId = "103097";
	strLanguageId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLangId"));
	
	strTopMenu = (strTopMenu == null)?"Home":strTopMenu;
	strPgToLoad = (strPgToLoad == null)?"Blank":strPgToLoad;

	if ( strTopMenu.equals("Home"))
	{
		strColSize = "205,*";
	}
	else
	{
		strPgToLoad = strPgToLoad.equals("Blank")?"/common/Gm_Blank.jsp":strPgToLoad;
		strColSize = "205,*";
	}
	session.setAttribute("strSessPgToLoad",strPgToLoad);
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\",\"cmplangid\":\""+strLanguageId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	
	String strLeftLinkURL = "/gmUserNavigation.do?companyInfo="+strCompanyInfo+"&strPgToLoad="+strPgToLoad;
	String strRightFrameURL = ""; 
	strPgToLoad = !strCokPgToLoad.equals("")?strCokPgToLoad:strPgToLoad;
	//If default page is not empty then append companyinfo in URL. 
	//Otherwise chrome browser will redirect infinite times enterprise portal page.
	if(!strPgToLoad.equals("")){
	 strRightFrameURL =strPgToLoad+GmFramework.getURLDelimiter(strPgToLoad)+"companyInfo="+strCompanyInfo;
	}
	 
%>

<html>
<head>
<jsp:include page="/GmIncPortalTitle.jsp"/>
  <script language="javascript">
  var partyId= <%=strPartyId%>;
    if (self != top)
    top.location.replace(self.location.href);
    //declaring company info. JSON variable
    //global variables are start with g chartacter.
    var gCompanyInfo = {};
    gCompanyInfo.cmpid = "<%=strCompanyId%>";
    gCompanyInfo.plantid = "<%=strPlantId%>";
    gCompanyInfo.token = "";
    var gCompJSDateFmt = "<%=strJsDateFmt%>";
    var gPageToLoad = "<%=strPgToLoad%>";
    gCompanyInfo.partyid="<%=strPartyId%>";
    gCompanyInfo.cmplangid="<%=strLanguageId%>";
    
    
</script>
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">

</head>

<FRAMESET border=1 frameSpacing=0 rows=80,*  MARGINWIDTH="0" MARGINHEIGHT="0" LEFTMARGIN="0" TOPMARGIN="0">
   <FRAME border=0 id=TopFrame name=TopFrame marginWidth=0 marginHeight=0
      src= "<%=strMenuPath%>/GmPortalHeader.jsp?companyInfo=<%=strCompanyInfo%>"
      frameBorder=no noResize scrolling=no LEFTMARGIN="0" TOPMARGIN="0" tabindex=-1>

 <FRAMESET border=1 id=fstMain name=fstMain framespacing=0 cols ="<%=strColSize%>">
    <FRAME border=1 id=LeftFrame name=LeftFrame
	src="<%=strLeftLinkURL%>" frameBorder=0 bordercolor="#8B9ACF"
	scrolling="no"	marginHeight=0 tabindex=-1>
    <FRAME border=0 id=RightFrame name=RightFrame bordercolor="#8B9ACF" src="<%=strRightFrameURL%>" frameBorder=0 
	scrolling="Auto" marginHeight=0 frameborder=no >
 </FRAMESET>
</FRAMESET>
</HTML>
