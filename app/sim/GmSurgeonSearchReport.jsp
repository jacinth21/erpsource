
<!-- \sim\GmSurgeonSearchReport.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>


<%
String strWikiTitle = GmCommonClass.getWikiTitle("SURGEON_SEARCH");
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Surgeon Search Report</title>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<script>
function fnSubmit() {
	var form = document.getElementsByName('frmSurgeonSearchReport')[0];
	form.strOpt.value = 'fetch';
	fnStartProgress();
	document.frmSurgeonSearchReport.submit();
	
}

</script>
</head>
<body leftmargin="20" topmargin="10">

<html:form action="/gmSurgeonSearchReport.do">
<html:hidden property="strOpt"/>
	<table class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2" height="25" class="RightDashBoardHeader">&nbsp;Surgeon Rep Search</td>
			<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='Help' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>
		</tr>
        <tr><td colspan="3" height="15"></td></tr> 
        <tr><td colspan="3" class="LLine" height="1"></td></tr>			
		<tr height="24" class="Shade">
			<td class="RightTableCaption" colspan="3">
			<!-- Struts tag lib code modified for JBOSS migration changes -->
				&nbsp;&nbsp;Last Name&nbsp;:&nbsp;
				<html:text property="lastName" size="20" name="frmSurgeonSearchReport" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
				&nbsp;&nbsp;First Name&nbsp;:&nbsp;
				<html:text property="firstName" size="20" name="frmSurgeonSearchReport" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
				&nbsp;&nbsp;Phone&nbsp;:&nbsp;
				<html:text property="phone" size="20" name="frmSurgeonSearchReport" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>				
			</td>
		</tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr>	
		<tr height="24" class="RightTableCaption">
			<td width="45%">&nbsp;&nbsp;Region&nbsp;:</td>
			<td width="45%">&nbsp;&nbsp;State&nbsp;:</td>
			<td width="10%"></td>
		</tr>
		<tr>
			<td width="45%">
				<div style="display: visible; height: 100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
					<table>
						<logic:iterate id="alRegions" name="frmSurgeonSearchReport" property="alRegions">
							<tr>
								<td>
									<htmlel:multibox property="selectedRegions" value="${alRegions.CODEID}" />
									<bean:write name="alRegions" property="CODENM" />
								</td>
							</tr>
						</logic:iterate>
					</table>
				</div>
				<br/>
			</td>
			<td width="45%">
				<div style="display: visible; height: 100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 8px; margin-right: 8px;">
					<table>
						<logic:iterate id="alStates" name="frmSurgeonSearchReport" property="alStates">
							<tr>
								<td>
									<htmlel:multibox property="selectedStates" value="${alStates.CODEID}" /> 
									<bean:write name="alStates" property="CODENM" />
								</td>
							</tr>
						</logic:iterate>
					</table>
				</div>
				<br/>
			</td>
           	<td width="10%" align="center" valign="center">
	           <gmjsp:button name="btnSubmit" value="&nbsp;&nbsp;Go&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnSubmit();"/>
	       	</td>
		</tr>
		<tr><td colspan="3" class="LLine" height="1"></td></tr>
		<tr>
			<td  colspan="3" width="100%">
				<display:table name="requestScope.frmSurgeonSearchReport.lresult" class="gmits" id="currentRowObject" export="true"
				decorator="com.globus.sim.displaytag.DTSurgeonSearchReportWrapper" requestURI="/gmSurgeonSearchReport.do" freezeHeader="true" paneSize="450">
					<display:column property="NAME" title="Name" sortable="true" class="alignleft"/>
					<display:column property="REP" title="Rep" sortable="true" class="alignleft"/>
					<display:column property="ADNAME" title="Area Director" sortable="true" class="alignleft"/>
					<display:column property="REGION" title="Region" sortable="true" class="alignleft"/>
					<display:column property="STATE" title="State" sortable="true" class="alignleft"/>
					<display:column property="STATUS" title="Status" sortable="true" class="aligncenter"/>
				</display:table>
			</td>
		</tr>
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>