<%@page import="java.net.URLEncoder"%>
<%
/**********************************************************************************
 * File		 		: GmClinicalPortal.jsp
 * Desc		 		: This screen is used for the Clinical Portals  - Frameset
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>

<!--clinical\querysystem\GmClinicalPortal.jsp -->

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.servlets.GmServlet" %>
<%
	GmServlet gmServlet = new GmServlet();
	gmServlet.checkSession(response,session);
	String strMenuPath = GmCommonClass.getString("GMMENU");
	String strColSize  = "0,*";
	strColSize = "205,*";
	String strStudyId = "";
	String strSiteId = "";
	String strPatientId = "";
    String strPgToLoad = "";
    String strDhtmlx = "";
    String strMethod = "";
    String strPeriodId = ""; 
    String strOpt="";
    String strTxnId = "";
    String strStudySiteId = "";
    String strPatientPkey = "";
    String strReqContext = "";
    String strSessPg = "";
    StringBuffer strActionUrl = new StringBuffer();
    
    strPgToLoad = GmCommonClass.parseNull((String)session.getAttribute("strSessPgToLoad"));
    strSessPg = "/gmClinicalStudyAction.do?";
    if (strPgToLoad.equals("") || (strPgToLoad.indexOf("common") > 0) || (strPgToLoad.indexOf("Servlet") > 0) || (strPgToLoad.indexOf("Dashboard") > 0)) 
    {
    	strPgToLoad = strSessPg;
    }
    strStudyId = GmCommonClass.parseNull((String)session.getAttribute("strSessStudyId"));
	//strPgToLoad = GmCommonClass.parseNull((String)session.getAttribute("strSessPgToLoad"));
	strSiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessSiteId"));
	strPatientId = GmCommonClass.parseNull((String)session.getAttribute("strSessPatientId"));
	strDhtmlx = GmCommonClass.parseNull((String)session.getAttribute("strSessDhtmlx"));
	strMethod = GmCommonClass.parseNull((String)session.getAttribute("strSessMethod"));
	strPeriodId = GmCommonClass.parseNull((String)session.getAttribute("strSessPeriodId"));
	strOpt = GmCommonClass.parseNull((String)session.getAttribute("strSessStrOpt"));
	strTxnId = GmCommonClass.parseNull((String)session.getAttribute("strSessStrTxnId"));
	strStudySiteId = GmCommonClass.parseNull((String)session.getAttribute("strSessStudySiteId"));
	strPatientPkey = GmCommonClass.parseNull((String)session.getAttribute("strSessPatientPkey"));
	strReqContext = GmCommonClass.parseNull((String)session.getAttribute("strSessReqContext"));
	String strPartyId =  GmCommonClass.parseNull((String) session.getAttribute("strSessPartyId"));
	String strPlantId =  GmCommonClass.parseNull((String) session.getAttribute("strSessPlantId"));
	//Once the GmLogonServlet request foward issue fixed then the below attribute will be get from request attribute.
 	String strCompanyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
	String strJsDateFmt =  GmCommonClass.parseNull((String) session.getAttribute("strSessJSDateFmt"));
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	
	strActionUrl.append(strMenuPath + "/GmClinicalLayout.jsp?strLocation=top");
	strActionUrl.append("&strPgToLoad="+strPgToLoad);
	strActionUrl.append("&strStudyId="+strStudyId);
	strActionUrl.append("&strSiteId="+strSiteId);
	strActionUrl.append("&strPatientId="+strPatientId);
	strActionUrl.append("&strDhtmlx="+strDhtmlx);
	strActionUrl.append("&strMethod="+strMethod);
	strActionUrl.append("&strPeriodId="+strPeriodId);
	strActionUrl.append("&strOpt="+strOpt);
	strActionUrl.append("&strTxnId="+strTxnId);
	strActionUrl.append("&strStudySiteId="+strStudySiteId);
	strActionUrl.append("&strPatientPkey="+strPatientPkey);
	strActionUrl.append("&strContext="+strReqContext);
	strActionUrl.append("&companyInfo="+strCompanyInfo);
%>

<html>
<head>
<title>Globus Medical: Enterprise Portal</title>
  <script language="javascript">
    //if (self != top)
    //top.location.replace(self.location.href);
    var gCompanyInfo = {};
    gCompanyInfo.cmpid = "<%=strCompanyId%>";
    gCompanyInfo.plantid = "";
    gCompanyInfo.token = "";
    var gCompJSDateFmt = "<%=strJsDateFmt%>";
    gCompanyInfo.partyid="<%=strPartyId%>";    
    
</script>
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">

</head>

<FRAMESET border=1 frameSpacing=0 rows=80,*  MARGINWIDTH="0" MARGINHEIGHT="0" LEFTMARGIN="0" TOPMARGIN="0">
   <FRAME border=0 name=TopFrame marginWidth=0 marginHeight=0
      src= "<%=strMenuPath%>/GmPortalHeader.jsp"
      frameBorder=no noResize scrolling=no LEFTMARGIN="0" TOPMARGIN="0" tabindex=-1>

 <FRAMESET border=1 name=fstMain framespacing=0>
    <FRAME border=1 name=LeftFrame
	src="<%=strActionUrl.toString()%>" frameBorder=0 bordercolor="#8B9ACF"
	scrolling="Yes"	marginHeight=0 tabindex=-1>    
 </FRAMESET>
</FRAMESET>
<%-- 
<FRAMESET border=1 frameSpacing=0 MARGINWIDTH="0" MARGINHEIGHT="0" LEFTMARGIN="0" TOPMARGIN="0">
    <FRAME border=0 name=layoutFrame bordercolor="#8B9ACF" src="/GmClinicalLayout.jsp" frameBorder=0
	scrolling="Auto" marginHeight=0 frameborder=no >
 </FRAMESET>
 
 
--%>
</HTML>
