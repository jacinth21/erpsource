
<!--globusMedApp\GmProdMgmnt.jsp-->
<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>

<%
	
	String strMenuPath = GmCommonClass.getString("GMMENU");
	String strOperPath = GmCommonClass.getString("GMOPERATIONS");
	
	String strPgToLoad = (String)session.getAttribute("strSessPgToLoad");
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");
	String strColSize  = "0,*";

	strTopMenu = (strTopMenu == null)?"Home":strTopMenu;
	strPgToLoad = (strPgToLoad == null)?"Blank":strPgToLoad;

	if ( strTopMenu.equals("Home"))
	{
		strPgToLoad = strServletPath.concat("/common/GmBlank.jsp");	
	}
	else
	{
		strPgToLoad = strPgToLoad.equals("Blank")?"/common/Gm_Blank.jsp":strPgToLoad;
		strColSize = "200,*";
	}

	session.setAttribute("strSessPgToLoad",strPgToLoad);

%>

<html>
<head>
<title>Globus Medical: Product Management</title>
  <script language="javascript">
    if (self != top)
    top.location.replace(self.location.href);
</script>
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
</head>

<FRAMESET border=1 frameSpacing=0 rows=113,*  MARGINWIDTH="0" MARGINHEIGHT="0" LEFTMARGIN="0" TOPMARGIN="0">
   <FRAME border=0 name=TopFrame marginWidth=0 marginHeight=0
      src= "<%=strMenuPath%>/GmProdHeader.jsp"
      frameBorder=no noResize scrolling=no LEFTMARGIN="0" TOPMARGIN="0" tabindex=-1>

 <FRAMESET border=1 name=fstMain framespacing=0 cols ="<%=strColSize%>">
    <FRAME border=1 name=LeftFrame
	src="/menu/GmProdLeftMenu.jsp" frameBorder=0 bordercolor="#8B9ACF"
	scrolling="Yes"	marginHeight=0 tabindex=-1>
    <FRAME border=0 name=RightFrame bordercolor="#8B9ACF" src="<%=strPgToLoad%>" frameBorder=0
	scrolling="Auto" marginHeight=0 frameborder=no >
 </FRAMESET>
</FRAMESET>

</HTML>
