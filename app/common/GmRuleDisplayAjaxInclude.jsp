

<!--common\GmRuleDisplayAjaxInclude.jsp -->


<%@ include file="/common/GmHeader.inc"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.List,java.util.Vector,java.util.HashMap,com.globus.common.beans.GmCommonClass" %>
<%@ taglib prefix="fmtRulesAjax" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRuleDisplayAjaxInclude.jsp -->
<fmtRulesAjax:setLocale value="<%=strLocale%>"/>
<fmtRulesAjax:setBundle basename="properties.labels.common.GmRuleDisplayAjaxInclude"/>


<%

String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
ArrayList alResult = new ArrayList();
alResult = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSEQUENCES"));
String strOverrideAcccess = GmCommonClass.parseNull((String)request.getAttribute("OVERRIDE_ACCESS"));
String strStatus = GmCommonClass.parseNull((String)request.getParameter("txnStatus"));
String strShowLink = GmCommonClass.parseNull((String)request.getAttribute("SHOWLINK"));
String strTxnId = GmCommonClass.parseNull((String)request.getParameter("txnid"));
int rows = alResult.size();
HashMap hmRow = new HashMap();
String rowClass = "";
String strRuleId="",strRuleName="", strPNUM="",strMessage="",strPicture="",strHoldTransaction="",strInitiatedBy="",strInitiatedDate="",strUpdatedBy="",strUpdatedDate="", strRulePnum="", strExpType="";

//Following code is used to get the column no of Part# column
String strTableHeaderNames = GmCommonClass.getString("RULETABLETEMPLATE");
String strHeader[] = strTableHeaderNames.split(",");
int strPartColumn=0;
for(int k=0;k<strHeader.length; k++)
{
	if(strHeader[k].equals("PNUM"))
		strPartColumn  = k+1;	
}
%>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css"> 

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
<script type="text/javascript">
var pnumCol = '<%=strPartColumn%>';
var txnId = '<%=strTxnId%>';
var rows = '<%=rows%>';
</script>

<form>
<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td colspan="3">		
<% 
if(rows > 0)
      {
%>                
			<table><tr><td class="RightText" bgcolor="#eeeeee"  height="25">
			<b><fmtRulesAjax:message key="LBL_RULE_MESSAGE"/></b>
			</td></tr>
			<tr><td  class="Line" height="1"></td></tr>
			</table>
<%} %>                  			
			<div style="overflow:auto; height:250px;width:700px;" id="ruleDiv">
			<Table border="0" class="its" cellspacing="0" cellpadding="0" id="rt">
			
			<tr>
				<thead>
					<tr height="25" class="RightDashBoardHeader" style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<th nowrap class="RightText" align="left">&nbsp; </th>
						<th nowrap class="RightText" align="left">&nbsp;<fmtRulesAjax:message key="LBL_RULE_NAME"/></th>
						<th nowrap class="RightText" align="left" width="50">&nbsp;<fmtRulesAjax:message key="LBL_PART"/></th>
						<th nowrap class="RightText" align="left" style="width:250px">&nbsp;<fmtRulesAjax:message key="LBL_MESSAGE"/></th>
						<th nowrap class="RightText" align="center">&nbsp;<fmtRulesAjax:message key="LBL_PICTURE"/></th>
						<th nowrap class="RightText" align="center"><fmtRulesAjax:message key="LBL_HOLD_TXN"/></th>
					<!-- 	<th nowrap class="RightText" align="left"> Initiated<br> By</th>
						<th nowrap class="RightText" align="center"> Initiated <br>Date</th>	 -->
						<th nowrap class="RightText" align="left"><fmtRulesAjax:message key="LBL_UPDATED_BY"/></th>
						<th nowrap class="RightText" align="center"><fmtRulesAjax:message key="LBL_UPDATED_DATE"/></th>					
					</TR>
				</thead>
				<tbody id="ruleBody">
				  <% 
				    for(int i=0; i<rows; i++)
				    {
				    	hmRow = (HashMap)alResult.get(i);
				    	strRuleId = String.valueOf(hmRow.get("ID"));
				    	strRuleName = String.valueOf(hmRow.get("NAME"));
				    	strPNUM	= String.valueOf(hmRow.get("PNUM"));
				    	strPNUM = strPNUM.equals("null")?strPNUM="":strPNUM;
				    	strMessage = String.valueOf(hmRow.get("MESSAGE"));
				    	strPicture = String.valueOf(hmRow.get("PICTURE"));
				    	strHoldTransaction = String.valueOf(hmRow.get("HOLDTXN"));
				    	strHoldTransaction = strHoldTransaction.equals("Y")?"YES":"NO";
				    	strInitiatedBy = String.valueOf(hmRow.get("INITBY"));
				    	strInitiatedDate = String.valueOf(hmRow.get("INITDATE"));
				    	strUpdatedBy = String.valueOf(hmRow.get("UPDBY"));
				    	strUpdatedDate = String.valueOf(hmRow.get("UPDDATE"));
				    	strExpType = GmCommonClass.parseNull((String)hmRow.get("EXPTYPE"));
				    	rowClass = i%2 == 0 ? "odd" : "even";
				    	strRulePnum = strRuleId + strPNUM;
				    %>
				    <script>
				    	rulePnumArray[<%=i%>]= '<%=strRulePnum%>';
				    </script>
				      <tr class="<%=rowClass%>" id="rtrow<%=i%>">
				        
				        <td>
				        <% if (!strRuleId.equals("CNV") && !strRuleId.equals("RFS") && !strRuleId.equals("BBA")) { //No need to show image for BBA rules %>
				        <a href=javascript:fnCallRuleSummary('<%=strRuleId %>')> <img src=<%=strImagePath%>/ordsum.gif     alt='Rule Summary'></a> 
				        <% } else if (strRuleId.equals("CNV") && strShowLink.equals("true")) { %>
				        <a href=javascript:fnCallRuleException('<%=strRuleId %>','<%=strPNUM%>')> <img src=<%=strImagePath%>/editcheck.gif     alt='Rule Summary'></a>
				        <% } else if (strRuleId.equals("BBA") && strShowLink.equals("true") && strExpType.equals("EXP1M")) { %>
				        <a href="javascript:fnCallExpDateRuleException('<%=strRuleId %>','<%=strPNUM%>');"> <img src=<%=strImagePath%>/editcheck.gif     alt='Expiry DateRule Summary'></a>
			             <% } %>
				        </td>
				        <td><%=strRuleName %></td>
				        <td><%=strPNUM %></td>
				        <td><%=strMessage %></td>
				        <td align="center"><%if (strPicture.equals("Y")) {  %>  <a href=javascript:fnOpenPicture('<%=strRuleId %>')> <img src=<%=strImagePath%>/product_icon.jpg ></a><% } %> </td>
				        <td align="center"><%=strHoldTransaction %></td>				      
				        <td><%=strUpdatedBy %></td>
				        <td align="center"><%=strUpdatedDate %></td>
				      </tr>
				      <%	      
				    }
				  %>
				 
				</tbody>	
			</tr>
			
			</Table>
			<%if (strOverrideAcccess.equalsIgnoreCase("Y")) {  %>
			<Table border="0" class="its" cellspacing="0" cellpadding="0">
			 	<tr> <td class="Line" colspan="2"></td> </tr>
			    <tr> <td class="RightText" bgcolor="#eeeeee"  height="25" colspan="2"> <input type="checkbox" name="OVERRIDEFL">  <b><fmtRulesAjax:message key="LBL_OVERRIDE"/></b> </td> </tr>
			    <tr> <td class="Line" colspan="2"></td> </tr>
			    <tr> <td height="10" colspan="2"></td> </tr>
			    <tr> <td valign="top" > <b><fmtRulesAjax:message key="LBL_OVERRIDE_REASON"/>: </b> </td><td> <textarea rows="3" cols="95" name="OVERRIDECOMMENTS"  onfocus="changeBgColor(this,'#AACCE8');" 
										onblur="changeBgColor(this,'#ffffff');" class="InputArea"></textarea>  </td></tr>
			 </Table>
			 <% } %>
			</div>
				
		</td>
	</tr>
</table>
<script type="text/javascript">

var rowsize = '<%=rows%>';
var obj = document.getElementById("ruleDiv");
if(obj != null)
{
	if(rowsize < 1)
	{	
		obj.style.visibility = 'hidden';
		obj.style.display = 'none';
	}
	else
	{
		obj.style.visibility = 'visible';
		obj.style.display = 'inline';
	}
}
</script>
</form>
