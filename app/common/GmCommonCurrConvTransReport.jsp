 <%
/*******************************************************************************
 * File		 		: GmCommonCurrConvTransReport.jsp
 * Desc		 		: This screen displys currency conversion transaction details
 * Version	 		: 1.0
 * author			: Dinesh Rajavel
*******************************************************************************/
%>




<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtCommonCurrConvTransRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmCommonCurrConvTransReport.js -->
<fmtCommonCurrConvTransRpt:setLocale value="<%=strLocale%>"/>
<fmtCommonCurrConvTransRpt:setBundle basename="properties.labels.common.GmCommonCurrConvReport"/> 
<bean:define id="currfromnm" name="frmCommonCurrConvForm" property="currfromnm" type="java.lang.String"> </bean:define>
<bean:define id="currtonm" name="frmCommonCurrConvForm" property="currtonm" type="java.lang.String"> </bean:define>
<bean:define id="currValue" name="frmCommonCurrConvForm" property="currValue" type="java.lang.String"> </bean:define>
<bean:define id="transcnt" name="frmCommonCurrConvForm" property="transcnt" type="java.lang.String"> </bean:define>
<bean:define id="alTransList" name="frmCommonCurrConvForm" property="alTransList" type="java.util.ArrayList"> </bean:define>
<bean:define id="createdby" name="frmCommonCurrConvForm" property="createdby" type="java.lang.String"> </bean:define>
<bean:define id="updatedby" name="frmCommonCurrConvForm" property="updatedby" type="java.lang.String"> </bean:define>
<%
String strWikiTitle = GmCommonClass.getWikiTitle("CURRENCY_CONVERSION_TRANS_RPT");
String strTransId = "";
String strTRStyle = "evenshade";
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10">
 
<html:form action="/gmCommonCurrConv.do"  >
	<table border="0" class="DtTable500" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="3"><fmtCommonCurrConvTransRpt:message key="LBL_CURRENCY_CONV_TRANS_RPT"/></td>
			<td align="right" class="RightDashBoardHeader" colspan="1"> 	
			<fmtCommonCurrConvTransRpt:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>			
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="evenshade">
		  <td class="RightTableCaption" align="right" colspan="2" height="25"><fmtCommonCurrConvTransRpt:message key="LBL_CURRENCY_FROM"/>&nbsp;:&nbsp;</td>
		  <td class="RightTableCaption" align="left" colspan="1" height="25">
		  <bean:write name="frmCommonCurrConvForm" property="currfromnm"></bean:write>
		  </td>
		</tr>
		<tr><td colspan="5" class="LLine" height="1"></td></tr> 
		<tr class="oddshade">
		  <td class="RightTableCaption" align="right" colspan="2"><fmtCommonCurrConvTransRpt:message key="LBL_CURRENCY_TO"/>&nbsp;:&nbsp;</td>
		  <td class="RightTableCaption" align="left" colspan="2" height="25">
		  <bean:write name="frmCommonCurrConvForm" property="currtonm"></bean:write>
		  </td>
		</tr>
		<tr><td colspan="5" class="LLine" height="1"></td></tr> 
		<tr class="evenshade">
            <td class="RightTableCaption" align="right" colspan="2" height="25"><fmtCommonCurrConvTransRpt:message key="LBL_CURRENCY_VALUE"/>&nbsp;:&nbsp;</td>
            <td class="RightTableCaption" align="left" colspan="2" height="25">
            <bean:write name="frmCommonCurrConvForm" property="currValue"></bean:write>
		    </td>
        </tr>		
		<tr><td colspan="5" class="LLine" height="1"></td></tr> 
		<tr class="oddshade">  
			<td class="RightTableCaption" align="right" colspan="1" height="25">
			<fmtCommonCurrConvTransRpt:message key="LBL_FROM_DATE"/>&nbsp;:&nbsp;</td>
			<td class="RightTableCaption" align="left" colspan="1" height="25">
			<fmtCommonCurrConvTransRpt:formatDate value="${frmCommonCurrConvForm.dtCurrFromDT}" pattern="<%=strGCompDateFmt%>"/>
			</td>
			<td class="RightTableCaption" align="right" colspan="1">
			<fmtCommonCurrConvTransRpt:message key="LBL_TO_DATE"/>&nbsp;:&nbsp;</td>
			<td class="RightTableCaption" align="left" colspan="1"><fmtCommonCurrConvTransRpt:formatDate value="${frmCommonCurrConvForm.dtCurrToDT}" pattern="<%=strGCompDateFmt%>"/>
			</td>		
		</tr>
		<tr><td colspan="5" class="LLine" height="1"></td></tr> 
		<tr class="evenshade">  
			<td class="RightTableCaption" align="right" colspan="1" height="25">
			<fmtCommonCurrConvTransRpt:message key="LBL_CREATED_BY"/>&nbsp;:&nbsp;</td>
			<td class="RightTableCaption" align="left" colspan="1">
			<bean:write name="frmCommonCurrConvForm" property="createdby"></bean:write>
			</td>
			<td class="RightTableCaption" align="right" colspan="1">
			<fmtCommonCurrConvTransRpt:message key="LBL_CREATED_DT"/>&nbsp;:&nbsp;</td>
			<td class="RightTableCaption" align="left" colspan="1">
			<fmtCommonCurrConvTransRpt:formatDate value="${frmCommonCurrConvForm.dtcreateddate}" pattern="<%=strGCompDateFmt%>"/>
			</td>		
		</tr>
		<tr><td colspan="5" class="LLine" height="1"></td></tr> 
		<tr class="oddshade">  
			<td class="RightTableCaption" align="right" colspan="1" height="25">
			<fmtCommonCurrConvTransRpt:message key="LBL_UPDATED_BY"/>&nbsp;:&nbsp;</td>
			<td class="RightTableCaption" align="left" colspan="1">
			<bean:write name="frmCommonCurrConvForm" property="updatedby"></bean:write>
			</td>
			<td class="RightTableCaption" align="right" colspan="1">
			<fmtCommonCurrConvTransRpt:message key="LBL_UPDATED_DT"/>&nbsp;:&nbsp;</td>
			<td class="RightTableCaption" align="left" colspan="1">
			<fmtCommonCurrConvTransRpt:formatDate value="${frmCommonCurrConvForm.dtupdateddt}" pattern="<%=strGCompDateFmt%>"/>
			</td>		
		</tr>
		<tr><td colspan="5" class="LLine" height="1"></td></tr> 
		<tr class="evenshade">
            <td class="RightTableCaption" align="right" colspan="2" height="25"><fmtCommonCurrConvTransRpt:message key="LBL_TRANS_COUNT"/>&nbsp;:&nbsp;</td>
            <td class="RightTableCaption" align="left" colspan="2" height="25">
            <bean:write name="frmCommonCurrConvForm" property="transcnt"></bean:write>
		    </td>
        </tr>	
		<tr><td colspan="5" class="Line" height="1"></td></tr>
		<tr class="oddshade">
            <td class="RightTableCaption" align="center" colspan="4" height="25"><fmtCommonCurrConvTransRpt:message key="LBL_TRANSACTION_IDS"/></td></tr>
        <tr><td colspan="5" class="Line" height="1"></td></tr>
        <tr><td colspan="5" height="1"></td></tr>
            		<% 
            			int intSize = alTransList.size();
						if (intSize > 0)
						{
							HashMap hmLoop = new HashMap();
											
							for (int i=0;i<intSize;i++)
							{
								hmLoop = (HashMap)alTransList.get(i);
								strTransId =  GmCommonClass.parseNull((String)hmLoop.get("TRANSID")); 
								int j = i%2;
								 if (j == 0){
								   strTRStyle = "evenshade";
								 }else{
								   strTRStyle = "oddshade";
								 }
					%>
		 <tr class="<%=strTRStyle%>">
        	<td class="RightTableCaption" align="center" colspan="4" height="25">	
					<span><%=strTransId%></span><br>		
		    </td>
        </tr>
        <tr><td colspan="5" class="Line" height="1"></td></tr>
								
					<%			
							}
							
						}
					%>
		<tr><td colspan="5" class="LLine" height="1"></td></tr>
        <tr>
            <td colspan="5" align="center" height="30">
                <fmtCommonCurrConvTransRpt:message key="BTN_CLOSE" var="varClose"/><gmjsp:button value="${varClose}" name="Btn_Close" gmClass="button" buttonType="Load" onClick="window.close();" />
            </td>
        </tr>   		
    </table>		 
</html:form>
   <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
