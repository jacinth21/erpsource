 <%
/**********************************************************************************
 * File		 		: GmStatusLogDetail.jsp
 * Desc		 		: This screen displyes status log detail
 * Version	 		: 1.0
 * author			: Angela Xiang
************************************************************************************/
%>



<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>

 <% %>
 <%@ taglib prefix="fmtStatuLogDetails" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- common\GmStatusLogDetail.jsp -->
<fmtStatuLogDetails:setLocale value="<%=strLocale%>"/>
<fmtStatuLogDetails:setBundle basename="properties.labels.common.GmStatusLogDetails"/>  
<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);
String strWikiTitle = GmCommonClass.getWikiTitle("STATUS_LOG_DETAILS");
String strApplDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
try {
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Status Log Detail</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	
<script language="JavaScript" src="<%=strJsPath%>/GmStatusLog.js"></script>
<script>
var format = '<%=strApplDateFmt%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
 
<html:form action="/gmstatuslog.do"  >
<html:hidden property="strOpt" />
<html:hidden property="haction" value=""/> 
<html:hidden property="source" /> 

	<table border="0" class="DtTable850" width="500" cellspacing="0" cellpadding="0">
	<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader">
				<fmtStatuLogDetails:message key="LBL_STATUS_LOG_DETAILS"/>
			</td>
			<td align="right" class=RightDashBoardHeader > 
			<fmtStatuLogDetails:message key="LBL_HELP" var="varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td> 
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<tr><td height="25" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">*</font><fmtStatuLogDetails:message key="LBL_REF_ID"/>:&nbsp;&nbsp;&nbsp;&nbsp;
	<html:text property="refID"  size="30" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>          		
		</td>
		<td align="center" colspan="3" height="30">
			<fmtStatuLogDetails:message key="BTN_LOAD" var="varLoad"/>
			<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Go" gmClass="button" buttonType="Load" onClick="fnGo();" />&nbsp;
		</td>
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
	
	<logic:notEqual name="frmStatusLog" property="haction" value="noList"> 
	<!-- Custom tag lib code modified for JBOSS migration changes -->
 <tr>
		<td height="30"  class="RightTableCaption">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <fmtStatuLogDetails:message key="LBL_FROM"/>:&nbsp;&nbsp;&nbsp;&nbsp;
			<gmjsp:dropdown controlName="fromLocationID" SFFormName="frmStatusLog" SFSeletedValue="fromLocationID" 
							SFValue="alLocations" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/>
			</td>	
		<td height="30"  class="RightTableCaption">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtStatuLogDetails:message key="LBL_TO"/>:&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><gmjsp:dropdown controlName="toLocationID" SFFormName="frmStatusLog" SFSeletedValue="toLocationID" 
							SFValue="alLocations" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]"/>
			</td>	
		</tr>
		<tr><td colspan="4" class="Line" height="1"></td></tr>
		<tr>
		<td align="center" colspan="6" height="30">
			<fmtStatuLogDetails:message key="BTN_SUBMIT" var="varSubmit"/>
			<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_save" buttonType="Save" gmClass="button" onClick="fnSave();" />
		</td>
		</tr>
 			
	      	<tr>
		<td colspan="6" width="100%">
		<display:table name="requestScope.frmStatusLog.aldetailList" export="false" class="its" requestURI="/gmstatuslog.do" id="currentRowObject" decorator="com.globus.common.displaytag.beans.DTStatusLogWrapper"  >
 		<fmtStatuLogDetails:message key="LBL_REF_ID" var="varRefId"/><display:column property="REFID" title="${varRefId}" class="alignleft" sortable="true"/>
 		<fmtStatuLogDetails:message key="LBL_TYPE" var="varType"/><display:column property="REFTYPE" title="${varType}" class="alignleft" />
		<fmtStatuLogDetails:message key="LBL_SOURCE" var="varSource"/><display:column property="SOURCE" title="${varSource}" class="alignleft" />
		<fmtStatuLogDetails:message key="LBL_USER" var="varUser"/><display:column property="UPDUSER" title="${varUser}" class="alignleft" />
		<fmtStatuLogDetails:message key="LBL_DATE" var="varDate"/><display:column property="UPDDATE" title="${varDate}" class="alignleft" />
			
		</display:table>
		</td>
		</tr> 
 </logic:notEqual>
		</table>
</html:form>
   <%@ include file="/common/GmFooter.inc" %>
   <%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
</BODY>
</HTML>
