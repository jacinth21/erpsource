<%
	/**********************************************************************************
	 * File		 		: GmCancelReport.jsp
	 * Desc		 		: This screen is used to report the Cancel transactions (Void, Delete, Rollback transaction)
	 * Version	 		: 1.0
	 * author			: Joe P Kumar
	 ************************************************************************************/
%>
<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Date"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.beans.GmLogger"%>
<%@ page import="com.globus.common.servlets.GmServlet"%> 
<!-- common\GmCancelReport.jsp -->
<!-- Imports for Logger -->
<%@ page import="com.globus.common.beans.GmLogger"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%>
<%@ page import="org.apache.log4j.Logger"%>

<%@include file="/common/GmHeader.inc"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ page buffer="16kb" autoFlush="true"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtCancelRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtCancelRpt:setLocale value="<%=strLocale%>"/>
<fmtCancelRpt:setBundle basename="properties.labels.common.GmCancelReport"/>


<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response, session);
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.common.GmCancelReport", strSessCompanyLocale);
	HashMap hmParam = new HashMap();
	ArrayList alCancelReason;
	hmParam = (HashMap) request.getAttribute("HMPARAM");

	String strAction = (String) request.getAttribute("hAction") == null
			? ""
			: (String) request.getAttribute("hAction");
	String strFromDate = GmCommonClass.parseNull((String) request
			.getAttribute("FROMDATE"));
	String strToDate = GmCommonClass.parseNull((String) request
			.getAttribute("TODATE"));
	String strCancelTypeFromLeftMenu = GmCommonClass
			.parseNull((String) request
					.getAttribute("hCancelTypeFromMenu"));
	String strCancelTypeName = GmCommonClass.parseNull((String) request
			.getAttribute("hCancelTypeName"));
	ArrayList alReasons = GmCommonClass
			.parseNullArrayList((ArrayList) request
					.getAttribute("hReasons"));
	ArrayList alTypes = GmCommonClass.parseNullArrayList((ArrayList) request
			.getAttribute("hTypes"));
	ArrayList alUsers = GmCommonClass
			.parseNullArrayList((ArrayList) request
					.getAttribute("alUsers"));
	String strTransactioName = GmCommonClass.parseNull((String) request
			.getAttribute("TRANSACTIONNAME"));

	String strCancelReason = "";
	String strComments = "";
	String strTxnId = "";
	String strCancelUser = "";
	String strCancelComment = "";
	String strCancelType = "";
	log.debug(" values inside hmParam is " + hmParam);
	if (hmParam != null) {
		strTxnId = GmCommonClass.parseNull((String) hmParam
				.get("TXNID"));
		strCancelReason = GmCommonClass.parseNull((String) hmParam
				.get("CANCELREASON"));
		strCancelUser = GmCommonClass.parseNull((String) hmParam
				.get("CANCELUSER"));
		strComments = GmCommonClass.parseNull((String) hmParam
				.get("COMMENTS"));
		strFromDate = GmCommonClass.parseNull((String) hmParam
				.get("FROMDATE"));
		strToDate = GmCommonClass.parseNull((String) hmParam
				.get("TODATE"));
		strCancelTypeFromLeftMenu = GmCommonClass
				.parseNull((String) hmParam.get("CANCELTYPEFROMMENU"));
		strCancelTypeName = GmCommonClass.parseNull((String) hmParam
				.get("CANCELTYPENAME"));
		strCancelComment = GmCommonClass.parseNull((String) hmParam
				.get("CANCELCOMMENT"));
	}
	
	String strApplDateFmt = strGCompDateFmt;
	Date dtfromdate=GmCommonClass.getStringToDate(strFromDate,strApplDateFmt);
	Date dttodate=GmCommonClass.getStringToDate(strToDate,strApplDateFmt);
	String strDateFmt = "{0,date,"+strApplDateFmt+"}";
	
	strCancelType = strCancelType.equals("")?GmCommonClass.parseNull((String)request.getAttribute("CNCLTYPE")):strCancelType;
	String v_id = "";
	if (strCancelTypeFromLeftMenu.equals("VQUERY")) {
		v_id = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_QUERY_ID"));
	} else {
		v_id = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_TRANS_ID"));
	}

	if (!strTransactioName.equals("")) {
		v_id = strTransactioName;
	}
	String strDeptId = (String) session.getAttribute("strSessDeptId");
	String strTableCSS = "";
	String strRowCSS = "";
	String strHeaderCSS = "";

	if (strDeptId.equalsIgnoreCase("R")) {
		strTableCSS = "DtTable765";
		strRowCSS = "evenshade";
		strHeaderCSS = "Header";
	} else {
		strTableCSS = "DtTable1100";
		strRowCSS = "";
		strHeaderCSS = "RightDashBoardHeader";
	}

	log.debug("strComments is " + strComments + " ------- "
			+ strTransactioName);
%>




<HTML>
<HEAD>
<TITLE>Globus Medical: Cancel Report</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">

 <!--  (PMT-21944) Cancel Reports screen loads with wrong space alignment -->
<%--  <link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css"> --%>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">

<SCRIPT type="text/javascript">

var dateFmt = '<%=strApplDateFmt%>';
var lblType = '<fmtCancelRpt:message key="LBL_TYPE"/>';

function fnGo()
{
		var objFromDt=document.frmCancelReport.Txt_FromDate;
		var objToDt=document.frmCancelReport.Txt_ToDate; 
		CommonDateValidation(objFromDt,dateFmt,message[10527]+ message[611]);
		CommonDateValidation(objToDt,dateFmt,message[10528]+ message[611]);
		fnValidateDropDn('Cbo_Type',lblType)
		if (ErrorCount > 0)
		{
			Error_Show();
			Error_Clear();
			return false;
		}
		fnStartProgress('Y');
		document.frmCancelReport.hAction.value = "reportForCancel";
		document.frmCancelReport.submit();	
}
function fnLoadReason(){
	document.frmCancelReport.hAction.value = "reasonLoad";
	document.frmCancelReport.hCancelTypeFromMenu.value = document.frmCancelReport.Cbo_Type.value;
	document.frmCancelReport.submit();
}
</SCRIPT>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
</head>


<body leftmargin="20" topmargin="10">
<FORM name="frmCancelReport" method="POST"
	action="<%=strServletPath%>/GmCommonCancelServlet"><input
	type="hidden" name="hAction" value=""> <input type="hidden"
	name="hTxnId" value="<%=strTxnId%>"> <input type="hidden"
	name="hCancelTypeName" value="<%=strCancelTypeName%>"> <input
	type="hidden" name="hCancelTypeFromMenu"
	value="<%=strCancelTypeFromLeftMenu%>">
<input
	type="hidden" name="hOpt" value="">
<table border="0" class="<%=strTableCSS%>" cellspacing="0"
	cellpadding="0">
	<tr>
		<td height="25" class="<%=strHeaderCSS%>" colspan="7"><fmtCancelRpt:message key="TD_CANCEL_RPT_HEADER"/><%//=strCancelTypeName%>
		</td>
	</tr>
	<tr>
		<td colspan="7" height="1" class="Line"></td>
	</tr>
	<tr class="<%=strRowCSS%>">
		<td height="30" align="right" colspan="2"><b><fmtCancelRpt:message key="LBL_FROM_DATE"/>: </b></td>
		<td colspan="1">&nbsp; 			
			<gmjsp:calendar textControlName="Txt_FromDate" textValue="<%=dtfromdate==null?null:new java.sql.Date(dtfromdate.getTime())%>"    gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>				 
			</td>

		<td align="right" colspan="1">&nbsp;&nbsp;<b><fmtCancelRpt:message key="LBL_TO_DATE"/>: </b></td>
		<td colspan="1">&nbsp;
		
			<gmjsp:calendar textControlName="Txt_ToDate" textValue="<%=dttodate==null?null:new java.sql.Date(dttodate.getTime())%>"   gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>		
 		 &nbsp;&nbsp;</td>

		<td align="right" colspan="1"><b><%=v_id%>: </b></td>
		<td colspan="1">&nbsp;<input type="text" size="20" value="<%=strTxnId%>"
			name="Txt_TxnId" class="InputArea"
			onFocus="changeBgColor(this,'#AACCE8');"
			onBlur="changeBgColor(this,'#ffffff');" tabindex=1></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->	
		<tr class="shade">
		<td height="30" align="right" colspan="2"><b><span style="color: red;">*</span> <fmtCancelRpt:message key="LBL_TYPE"/>: </b></td>
			<td colspan="1" style="width:25px;">&nbsp;<gmjsp:dropdown controlName="Cbo_Type"
				seletedValue="<%=strCancelTypeFromLeftMenu%>" value="<%=alTypes%>"
				codeId="CODENMALT" codeName="CODENM" defaultValue="[Choose One]" onChange="fnLoadReason();" /></td>
		
			<td height="30" align="right" colspan="1"><b><fmtCancelRpt:message key="LBL_REASON"/>: </b></td>
			<td colspan="3">&nbsp;<gmjsp:dropdown controlName="Cbo_Reason"
				seletedValue="<%=strCancelReason %>" value="<%=alReasons%>"
				codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" /></td>
		</tr>
		<tr>
			<td align="right" colspan="2"><b><fmtCancelRpt:message key="LBL_CREATED_BY"/>: </b></td>
			<td colspan="1">&nbsp;<gmjsp:dropdown 
				controlName="Cbo_UserFilter" value="<%=alUsers%>" codeId="ID"
				codeName="NAME" defaultValue="[Choose One]"
				seletedValue="<%=strCancelUser%>" /></td>
			<td align="right" colspan="1"><b><fmtCancelRpt:message key="LBL_COMMENTS"/>: </b></td>
			<td colspan="1">&nbsp;<input type="text" name="Txt_Comment"
				class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" height="18"  
				onBlur="changeBgColor(this,'#ffffff');" value="<%=strCancelComment%>"></td>
			<fmtCancelRpt:message key="BTN_LOAD" var="varLoad"/>
			<td colspan="2"><gmjsp:button buttonType="Load"
				value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button"
				onClick="fnGo();" tabindex="25" /></td>		
		</tr>
	<tr>
		<td colspan="7">
		<%
			if (strAction.equals("reportForCancel")) {
		%> <display:table name="ALRETURN" export="true" class="its"
			id="currentRowObject" freezeHeader="true">
            <display:setProperty name="export.excel.filename" value="gmCommonCancelServlet.xls" /> 
			<display:column property="TXNID" title="<%=v_id%>" sortable="true" />
			<fmtCancelRpt:message key="LBL_COMMENTS" var="varComments"/>
			<display:column property="COMMENTS" title="${varComments}"
				class="alignleft" />
				<fmtCancelRpt:message key="DT_CANCEL_REASON" var="varCReason"/>
			<display:column property="CANCELREASON" title="${varCReason}"
				class="alignleft" sortable="true" />
				<fmtCancelRpt:message key="LBL_CREATED_BY" var="varCreatedBy"/>
			<display:column property="CREATEDBY" title="${varCreatedBy}"
				class="alignleft" />
			<fmtCancelRpt:message key="DT_CANCEL_DATE" var="varCDate"/>
			<display:column property="CANCELDATE" title="${varCDate}"
				class="aligncenter" format="<%=strDateFmt%>"/>
		</display:table> <%
 	}
 %>
		</td>
	</tr>
</table>
</FORM>
<%@include file="/common/GmFooter.inc"%>
</body>
</HTML>
