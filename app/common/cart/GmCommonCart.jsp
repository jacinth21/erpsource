 <%
/**********************************************************************************
 * File		 		: GmCommonCart.jsp
 * Version	 		: 1.0
 * author			: Dhinakaran James
 * path             : \globusMedApp\common\cart\GmCommonCart.jsp (Added this comment to reflect ordercart.js changes)
************************************************************************************/
%>

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<!-- Imports for Logger -->
 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtCart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtCart:setLocale value="<%=strLocale%>"/>
<fmtCart:setBundle basename="properties.labels.common.cart.GmCommonCart"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	//Logger log = GmLogger.getInstance(GmCommonConstants.ACCOUNTING);

	int intTRCount = 10;
	int intSize = 0;
	int intAlSize = 0;
	HashMap hcboVal = new HashMap();
	String strCodeID = "";
	String strSelected = "";
	String strPartOrdType = "";
	String strConCode = GmCommonClass.parseZero((String)request.getAttribute("hConstructId"));
	String strGpoId = GmCommonClass.parseZero((String)request.getAttribute("hGpoId"));
	String strConsValue = GmCommonClass.parseZero((String)request.getAttribute("hConsValue"));
	String strCntrlTabIndexFl = GmCommonClass.parseNull((String)request.getAttribute("CTRLTABINDEX"));
	String strTotal = "";
	String strTotalBeforeAdj ="";
	String strOpt = GmCommonClass.parseNull((String)session.getAttribute("strSessOpt"));
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strShowDiscount = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_DISCOUNT"));
	String strCmpnyRule = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SLNUM_VALIDATION"));
	
	String strTagColumn = GmCommonClass.parseNull((String)request.getAttribute("TagColumn"));
	String strCapFlColumn = GmCommonClass.parseNull((String)request.getAttribute("CapFlColumn"));
	String strHiddenTagColumn = "hidden";
	String strHiddenCapColumn = "hidden";
	if(strTagColumn.equals("true")){
		strHiddenTagColumn = "";
	}
	if(strCapFlColumn.equals("true")){
		strHiddenCapColumn = "";
	}
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.common.cart.GmCommonCart", strSessCompanyLocale);
	
	String strQtyLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PHONE_QTY"));
	String strDescSpan = "1";
	String strRuleID = "";
	String strRuleVal = "";
	
	if (strOpt.equals("QUOTE"))
	{
		strQtyLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUOTE_QTY"));
		strDescSpan = "4";
	}
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alOrderPartType = new ArrayList();
	ArrayList alConstructs = new ArrayList();
	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");
	ArrayList alLoop = new ArrayList();
	ArrayList alColumnRule = new ArrayList();
	ArrayList alAdjCOdes = new ArrayList();
	//System.out.println("alReturn>>>>>>>>>>>>>........" + alReturn);
	if (hmReturn != null)
	{
		alOrderPartType = (ArrayList)hmReturn.get("ORDPARTTP");
		alConstructs = (ArrayList)hmReturn.get("CONSTRUCTS");
		alColumnRule = (ArrayList)hmReturn.get("COLRULE");
		//System.out.println("alColumnRule>>>>>>>>>>>>>........" + alColumnRule);
		alAdjCOdes   = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ADJCODES"));	
		HashMap hmMap = new HashMap();
		hmMap.put("CODEID","0");
		hmMap.put("CODENM","N/A");
		alAdjCOdes.add(0,hmMap);
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Common Cart</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/<%=strOpt.equals("PROFORMA")?"GmOrderCart.js":"OrderCart.js"%>"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmOrderCartCalc.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery-latest.js"></script>

<script>
var req;
var trcnt = 0;
var cnt = <%=intTRCount%>;
var showDiscount='<%=strShowDiscount%>';
var vcountryCode = '<%=strCountryCode%>';
var tagColumn = '<%=strTagColumn %>';
var capFlColumn = '<%=strCapFlColumn %>';
var cmpnyRule = '<%=strCmpnyRule%>';
var cntrlTabIndexFl = '<%=strCntrlTabIndexFl%>';
var resetBtnNm = '<fmtCart:message key="BTN_DISC_RESET"/>';
var applyBtnNm = '<fmtCart:message key="BTN_CALC"/>';

</script>

<script>
  
$(function () {
    var $chk = $("input:checkbox"); 
    var $tbl = $("#PartnPricing");
    var $tblhead = $("#PartnPricing th");

 //   $chk.prop('checked', true); 
    $chk.click(function () {
        var colToHide = $tblhead.filter("." + $(this).attr("name"));
        var index = $(colToHide).index();
        var colnm = $(this).attr('name');  
            if (document.getElementById(colnm).value =='0')
            	{
            		document.getElementById(colnm).value = '1';
            	}
            else
            	{
            		document.getElementById(colnm).value = '0';
            	}
        $tbl.find('tr :nth-child(' + (index + 1) + ')').toggle();
    });
});

</script>
</HEAD>
<FORM name="frmCart" method="POST" action="<%=strServletPath%>/GmCommonCartServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hRowCnt" value="<%=intTRCount%>">
<input type="hidden" name="hTotal" value="">
<input type="hidden" name="hTotalBeforeAdj" value="">
<input type="hidden" name="hGpoId" value="<%=strGpoId%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hAdjInputStr" value="">
<input type="hidden" name="hPartNumStr" value="">
<input type="hidden" name="hConsValue" value="<%=strConsValue%>">
<input type="hidden" name="hAdjStr" value="">

<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>

	      <td colspan="3" class="RightText" height="30">
	       <table border="0" cellspacing="0" cellpadding="0" width="100%"> <tr> 
	       <%
		if (strOpt.equals("PHONE"))
		{
%>
			<td >&nbsp;<fmtCart:message key="LBL_CONS_PRICE_CODE"/>:
				<select name="Cbo_Construct" onchange="javascript:fnCheckConstructAccount(this)" class="RightText" tabindex="11" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
					<option value="0" >[Choose One]
<%
			  		intSize = alConstructs.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alConstructs.get(i);
			  			strCodeID = (String)hcboVal.get("CID");
						strSelected = strConCode.equals(strCodeID)?"selected":"";
%>
					<option id="<%=(String)hcboVal.get("CVALUE")%>" <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CNAME")%> - <%=hcboVal.get("CDESC")%> - $<%=GmCommonClass.getStringWithCommas((String)hcboVal.get("CVALUE"))%></option>
<%
			  		}
%>

				</select>
			</td>
			<td align="left"><fmtCart:message key="LBL_COPY_CONSIGNMENT"/>: 
				<input type="text"  name="Txn_consignmentId" property="Txn_consignmentId" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" >
				<fmtCart:message key="BTN_LOAD" var="varLoad"/>
				<gmjsp:button gmClass="Button" name="Btn_Load" onClick="fnLoadParts()" buttonType="Load" value="${varLoad}"/>
			</td>
<%
			}else{
%>
			  <td width = "99%"></td> <!-- For alignment issue -->
<%			}
	       if (!strOpt.equals("QUOTE")){
%>			
			<fmtCart:message key="COL_TAG_ETCH" var="varTagEtch"/>
			<fmtCart:message key="COL_CAP_FLAG" var="varCapFlag"/>
			<td width = "1%">
			<jsp:include page="/common/cart/GmCartColumns.jsp" >
							<jsp:param name="cols" value="17" /> 								
								<jsp:param name="col16" value="${varTagEtch}" />  
								<jsp:param name="col17" value="${varCapFlag}" />
								<jsp:param name="tagColumn" value='<%=strTagColumn %>' />
								<jsp:param name="capFlColumn" value='<%=strCapFlColumn %>' />																 
								<jsp:param name="strOpt" value='<%=strOpt %>' />
							</jsp:include> 
			</td>
			</tr>
			</table>
			</td>
<%
			}else if (strOpt.equals("QUOTE")){
%>
				<td colspan="2"><input type="hidden" name="Cbo_Construct" value=0></td>
<%
			}
%>			
		
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<TR>
			<td colspan="3">
			<div style="overflow:auto; height:252px;">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" bordercolor="gainsboro" id="PartnPricing">
			  <thead>
				<TR bgcolor="#EEEEEE" class="RightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
					<TH class="col1 RightTableCaption" width="10" align="center">#</TH>
					<TH class="col2 RightTableCaption" width="10" align="center"><a href="javascript:fnClearCart();" tabindex="-1"><img border="0" Alt='Clear Cart' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></TH>
					<TH class="col3 RightTableCaption" width="70" align="center"><fmtCart:message key="LBL_PART"/><BR><fmtCart:message key="LBL_NUMBER"/></TH>
					<TH class="col4 RightTableCaption" width="50" align="center"><%=strQtyLabel %><BR><fmtCart:message key="LBL_QTY"/></TH>
					<TH class="col5 RightTableCaption" width="50" align="center"><fmtCart:message key="LBL_SHELF"/><BR><fmtCart:message key="LBL_QTY"/></TH>
<%
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA"))
			{
%>					
					<TH class="col6 RightTableCaption" width="40" align="center"><fmtCart:message key="LBL_BO"/><BR><fmtCart:message key="LBL_FLAG"/></TH>
<%
			}
%>					
					<TH class="col7 RightTableCaption" width="325" colspan="<%=strDescSpan %>" >&nbsp;<fmtCart:message key="LBL_PART_DESC"/></TH>
<%
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA"))
			{
%>					
					<TH class="col8 RightTableCaption" width="40" align="center"><fmtCart:message key="LBL_TYPE"/></TH> 
<%
			}
			if (!strCountryCode.equals("en")){
%>
					<TH class="col9 RightTableCaption" width="60" align="center"><fmtCart:message key="LBL_BASE"/> <BR><fmtCart:message key="LBL_PRICE_EA"/></TH>
<%
			}
%>
					<TH class="col10 RightTableCaption" width="60" align="center"><fmtCart:message key="LBL_UNIT"/><BR><fmtCart:message key="LBL_PRICE"/></TH>
					<TH class="col11 RightTableCaption" width="85" align="center"><fmtCart:message key="LBL_UNIT"/><BR><fmtCart:message key="LBL_PRICE"/><BR><fmtCart:message key="LBL_ADJ"/></TH>
					<TH class="col12 RightTableCaption" width="45" align="center"><fmtCart:message key="LBL_ADJ"/><BR><fmtCart:message key="LBL_CODE"/></TH>
					<TH class="col13 RightTableCaption" width="80" align="center"><fmtCart:message key="LBL_NET_UNIT"/><BR><fmtCart:message key="LBL_PRICE"/></TH>
					<TH class="col14 RightTableCaption" width="60" align="center"><fmtCart:message key="LBL_TOTAL"/><BR><fmtCart:message key="LBL_PRICE"/></TH> 
<%					
			if (strOpt.equals("PHONE")){ 
%>					 
					<TH class ="col15 RightTableCaption" width="280" align="center" > <fmtCart:message key="LBL_CONTROL"/><BR><fmtCart:message key="LBL_NUMBER"/></TH>
<%
			}
%>

<%
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA"))
			{
%>					<TH class="col16 RightTableCaption" width="70" <%=strHiddenTagColumn %> id="col16" align="center"><fmtCart:message key="LBL_TAG_ID"/>/<BR><fmtCart:message key="LBL_ETCH_ID"/></TH>					
					<TH class="col17 RightTableCaption" width="40" <%=strHiddenCapColumn %> id="col17" align="center"><fmtCart:message key="LBL_CAP"/><BR><fmtCart:message key="LBL_FLAG"/></TH>
<%
			}
			 
%>


				</TR>	
				<tr><th class="Line" height="1" colspan="16"></th></tr>
			  </thead>
			  <TBODY>
<%
				String strPartNum = "";
				String strQty = "";
				String strPartDesc = "";
				String strBOFlag = "";
				String strParttype = "";
				String strCapFl = "";
				String strPrice = "";
				String strExtPrice = "";
				String strStock = "";
				String strGroupId = "";
				String strCapSelected = "";
				String strLoanCogs = "";
				String strUnitPrice = "";
				String strUnitAdj = "";
				
				double dbPrice = 0.0;
				double dbUnitPrice = 0.0;
				double dbExtPrice = 0.0;
				double dbExtendedUnitPrice = 0.0;
				double dbTotal = 0.0;
				double dbTotalBeforeAdj = 0.0;
				int intQty = 0;
				String strLoanCogsChk = "";
				String strRefId = "";
				if (alReturn != null)
				{
					intAlSize = alReturn.size();
				}
				
				for (int i=0; i < intTRCount;i++)
				{
					if (i<intAlSize)
					{
						alLoop = (ArrayList)alReturn.get(i);
						strPartNum = (String)alLoop.get(0);
						strQty = (String)alLoop.get(1);
						strPrice = (String)alLoop.get(5);
						strUnitPrice = (String)alLoop.get(10);
						strGroupId = (String)alLoop.get(6);
						strCapSelected = strGroupId.equals("")?"":"checked";
						strStock = (String)alLoop.get(8) + "&nbsp;&nbsp;";
						strPartDesc = "&nbsp;".concat(GmCommonClass.getStringWithTM((String)alLoop.get(7)));
						
						intQty = Integer.parseInt(strQty);
						dbPrice = Double.parseDouble(strPrice);
						dbUnitPrice = Double.parseDouble(strUnitPrice);
						dbExtPrice = dbPrice * intQty;
						dbExtendedUnitPrice = dbUnitPrice * intQty;
						dbTotal = dbTotal + dbExtPrice;
						dbTotalBeforeAdj = dbTotalBeforeAdj + dbExtendedUnitPrice;
						
						strPrice = GmCommonClass.getStringWithCommas(strPrice);
						strUnitPrice = GmCommonClass.getStringWithCommas(strUnitPrice);
						
						strExtPrice = GmCommonClass.getStringWithCommas(""+dbExtPrice).concat("&nbsp;");
						strLoanCogs = (String)alLoop.get(9);												
						strUnitAdj = (String)alLoop.get(11);
						strUnitAdj = GmCommonClass.getStringWithCommas(strUnitAdj);
						
						
							if (!strPartNum.equals("")&& !strQty.equals("") && strLoanCogs.equals(""))
							{
								strLoanCogsChk = "disabled";
							}
							else
							{
								strLoanCogsChk = "";
							}
					}
					else
					{
						strPartNum = "";
						strPartDesc = "";
						strPrice = "";
						strUnitPrice = "";
						strQty = "";
						strExtPrice = "";
						strStock = "";
						strGroupId = "";
						strCapSelected = "";
						strLoanCogs = "";
					}
					alLoop = null;

%>
				<tr>
					<td class="RightText"><%=i+1%></td>
					<td class="RightText"><a href="javascript:fnRemoveItem('<%=i%>');" tabindex="-1"><img border="0" Alt='Remove from cart' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9">&nbsp;</a></td>
					<td class="col3"><input type="text" size="10" class=InputArea value="<%=strPartNum%>" name="Txt_PNum<%=i%>" onBlur=fnSetPartSearch('<%=i%>',this); onFocus="changeBgColor(this,'#AACCE8');"></td>
					<td class="col4" align="center"><input type="text" size="2" value="<%=strQty%>" class=InputArea name="Txt_Qty<%=i%>" onkeypress="return fnIsNumeric(event);" onblur="validate(<%=i%>,this);" onFocus="changeBgColor(this,'#AACCE8');"></td>
					<td class="col5" id="Lbl_Stock<%=i%>" class="RightText" align="right"><%=strStock%></td>
<%
			if (strOpt.equals("PHONE") ||  strOpt.equals("PROFORMA"))
				{
					String strDisable = "";
					if(strOpt.equals("PROFORMA")){
						strDisable = "disabled";
					}
%>					
					
					<td class="col6" align="center"><input type=checkbox name="Chk_BO<%=i%>" <%=strDisable%> tabindex="-1"><input type="hidden" name="hLoanCogs<%=i%>" value="<%=strLoanCogs%>"></td>
<%
			}
%>					
					<td class="col7"  id="Lbl_Desc<%=i%>" class="RightText" ><%=strPartDesc%></td>
<%
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA"))
			{
%>					               
					<td class="col8" align="center"><select <%=strLoanCogsChk%> tabindex="-1" name="Cbo_OrdPartType<%=i%>" class="RightText" onChange="fnChangeStatus(this.value,'<%=i%>');" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
<%
			  		intSize = alOrderPartType.size();
			  		hcboVal = new HashMap();
			  		for (int j=0;j<intSize;j++)
			  		{
			  			hcboVal = (HashMap)alOrderPartType.get(j);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strPartOrdType.equals(strCodeID)?"selected":"";
%>
								<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENMALT")%></option>
<%
			  		}
			  		strPartOrdType = "";
			  		strLoanCogs = "";
			  		strLoanCogsChk = "";
%>
						</select>
					</td>
		<!-- 		<td class="col9" ><input type="text" size="7" class="InputArea" value="<%=strRefId%>" name="Txt_RefId<%=i%>" tabindex="-1" onblur="fnChkValidFieldVal(this,<%=i%>);changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');"></td>
					<td class="col10" align="center"><input type=checkbox <%=strCapSelected%> name="Chk_Cap<%=i%>" tabindex="-1" value=""></td> -->	
<%
			}else if (strOpt.equals("QUOTE")){
%>
				<td colspan="3">	
					<input type=hidden name="Chk_BO<%=i%>" tabindex="-1">
					<input type="hidden" name="hLoanCogs<%=i%>" value="<%=strLoanCogs%>">
					<input type="hidden" name="Cbo_OrdPartType<%=i%>" value="50300">
					<input type="hidden" name="Txt_RefId<%=i%>" value="">
					<input type="hidden" name="Chk_Cap<%=i%>" value="">
				</td>
				<%-- <td id="Lbl_BasePrice<%=i%>" class="RightText" align="right">
					<input type="text" size="8" value="<%=strPrice%>" tabindex="-1" class=InputArea name="Txt_BasePrice<%=i%>" onBlur="fnCalExtPrice(this,'<%=i%>','Y');changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');"></td> --%>
<%
			}
			if (!strCountryCode.equals("en")){
%>						
					<td class="col9" id="Lbl_BasePrice<%=i%>" class="RightText" align="right">
					<input type="text" size="8" value="<%=strPrice%>" tabindex="-1" class=InputArea name="Txt_BasePrice<%=i%>" onBlur="fnCalExtPrice(this,'<%=i%>','Y');changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');"></td>
<%
			}
%>				

					<td class="col10" id="Lbl_unit_price<%=i%>" class="RightText" align="right"><input type="text" tabIndex = -1 size= 7 value="<%=strUnitPrice%>" readonly id="Txt_unit_price<%=i%>" name="Txt_unit_price<%=i%>" style="border: none; text-align: right; "> 
					<input type="hidden"  name="hUnitPartPrice<%=i%>" value="<%=strUnitPrice%>" /></td>										
					<td class="col11" id="Lbl_unit_adj<%=i%>" class="RightText" align="right"><input type="text" tabIndex = -1 size= 7 value="<%=strUnitAdj%>" readonly id="Txt_unit_adj<%=i%>" name="Txt_unit_adj<%=i%>" style="border: none;text-align: right;">
					<input type="hidden" name="hWaste<%=i%>" value="">
					<input type="hidden" name="hRevision<%=i%>" value="">
					</td>
					<td class="col12" id="Lbl_adj_code<%=i%>" class="RightText" align="right"> 
					<select tabindex="-1" name="Cbo_adj_code<%=i%>" class="RightText" onChange="fnGetUnitPrice(this,'<%=i%>');changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
					<%
			  		intSize = alAdjCOdes.size();
			  		hcboVal = new HashMap();
			  		for (int j=0;j<intSize;j++)
			  		{
			  			hcboVal = (HashMap)alAdjCOdes.get(j);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strPartOrdType.equals(strCodeID)?"selected":"";
%>
								<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENM")%></option>
<%
			  		}
			  		strPartOrdType = "";
			  		strLoanCogs = "";
			  		strLoanCogsChk = "";
%>
					</select> 
									
									</td>

					<td class="col13" id="Lbl_Price<%=i%>" class="RightText" align="right">
					<input type="text" size="8" value="<%=strPrice%>" tabindex="-1" class=InputArea name="Txt_Price<%=i%>" style="text-align: right;" onBlur="fnCalExtPrice(this,'<%=i%>','Y');" onFocus="changeBgColor(this,'#AACCE8');" tabindex="-1"><input type="hidden" name="hPartPrice<%=i%>" value="<%=strPrice%>"/><input type="hidden"  name="Lbl_PriceEA<%=i%>" value="<%=strPrice%>" /><input type="hidden"  name="hNetPartPrice<%=i%>" value="<%=strPrice%>" />
					<input type="hidden"  name="hBasePrice<%=i%>" value="<%=strPrice%>" /></td>
					<td class="col14" id="Lbl_Amount<%=i%>" class="RightText" align="right"><%=strExtPrice%></td>
					<% if (strOpt.equals("PHONE")){ %>
						<% if (strCntrlTabIndexFl.equals("Y")){ %>
						 <td class="col15" ><input type="text" size="28" class=InputArea value="" name="Txt_Cnum<%=i%>" id="Txt_Cnum<%=i%>" style="text-transform: uppercase;width: 80%;" onBlur="fnValidateCtrlNum(<%=i%>,this);changeBgColor(this,'#ffffff');" onFocus="fnRemoveImage(<%=i%>,this);changeBgColor(this,'#AACCE8');" maxlength="40">
					 	<%}else{ %>
					 	 <td class="col15" ><input type="text" size="28" class=InputArea value="" name="Txt_Cnum<%=i%>" id="Txt_Cnum<%=i%>" tabindex="-1" style="text-transform: uppercase;width: 80%;" onBlur="fnValidateCtrlNum(<%=i%>,this);changeBgColor(this,'#ffffff');" onFocus="fnRemoveImage(<%=i%>,this);changeBgColor(this,'#AACCE8');" maxlength="40">
					 	<%} %>
						 <input type="hidden" name="hCntrlNumberNeededFl<%=i%>" value="">
						 <span id="DivShowCnumAvl<%=i%>" style="vertical-align:middle; display: none;"> <img height="16" width="19" src="<%=strImagePath%>/success.gif"></img></span>
						 <span id="DivShowCnumNotExists<%=i%>" style="vertical-align:middle; display: none;"> <img height="12" width="12" src="<%=strImagePath%>/delete.gif"></img>
						 <%if(strCmpnyRule.equals("YES")){ %>
						 &nbsp;<a href="javascript:fnViewLotReport('<%=i%>')"><img height="15" border="0" title="Lot Code Report" src="<%=strImagePath%>/location.png"></img></a>
						 <%} %>
						 </span>
						 <input type="hidden" name="hPnumValid<%=i%>" id="hPnumValid<%=i%>" value="">
						 <input type="hidden" name="hPnumProjValid<%=i%>" id="hPnumProjValid<%=i%>" value="">
						 <input type="hidden" name="hproductMat<%=i%>" id="hproductMat<%=i%>" value="">
						 </td>
					 <%}%>
					
<%
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA"))
			{ 
					 
%> 					 	 
					<td class="col16" <%=strHiddenTagColumn %> ><input type="text" size="7"  class="InputArea" value="<%=strRefId%>" name="Txt_RefId<%=i%>" tabindex="-1" onblur="fnChkValidFieldVal(this,<%=i%>);changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');"></td>					 
					<td class="col17" <%=strHiddenCapColumn %> align="center"><input type=checkbox <%=strCapSelected%> name="Chk_Cap<%=i%>" tabindex="-1" value=""></td>					
<%
			} 
%>

				</tr> 
				
				<tr><td colspan="16" class="LLine" height="1"></td></tr>
				<!--<tr><td bgcolor="#eeeeee" height="1" colspan="11"></td></tr>-->
<%
				}
				strTotal = ""+dbTotal;
				strTotalBeforeAdj = ""+dbTotalBeforeAdj;
%>
			  </TBODY>
			</table>
			</div>
			</td>
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="Shade" height="23">
		 
			<td class="RightText" width="750" align="right" ><B><fmtCart:message key="LBL_TOTAL_BF_ADJ"/>: </B>&nbsp;</td>
			<td class="RightCaption" align="right" width="120" id="Lbl_Total_Before_Adj"><gmjsp:currency type="CurrTextSign"  textValue="<%=strTotalBeforeAdj%>" td="false"/>
<%			
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA")){ 
%>	
			  <td class="RightCaption" width="120">&nbsp;</td>
<%
			}
%> 		
		</tr>
		<tr class="Shade" height="23">
		 
			<td class="RightText" width="750" align="right" ><B><fmtCart:message key="LBL_TOTAL_AF_ADJ"/>: </B>&nbsp;</td>
			<td class="RightCaption" align="right" width="120" id="Lbl_Total"><gmjsp:currency type="CurrTextSign"  textValue="<%=strTotal%>" td="false"/>
<%			
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA")){ 
%>	
			  <td class="RightCaption" width="120">&nbsp;</td>
<%
			}
%> 		
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<tr>
			<td colspan="3" align="center" height="25">
			<script>
				document.frmCart.hTotal.value = '<%=strTotal%>';
				document.frmCart.hTotalBeforeAdj.value = '<%=strTotalBeforeAdj%>';
			</script>
				<input type="button" class="Button"  name ="Btn_PartLookUp" accesskey="P" tabindex="-1" onClick="fnOpenPart();" value ="&#818;<fmtCart:message key="BTN_PART_LKP"/>" />&nbsp;&nbsp;
				<fmtCart:message key="BTN_ADD_ROW" var="varAddRow"/>
				<gmjsp:button value="${varAddRow}" gmClass="Button" buttonType="Save" onClick="fnAddRow('PartnPricing');" tabindex="16" />&nbsp;&nbsp;
				<%if(strShowDiscount.equalsIgnoreCase("YES")){%>
			<fmtCart:message key="BTN_CALC" var="varCalc"/>
			<gmjsp:button gmClass="Button" name="Btn_Calculate" buttonType="Save" accesskey="A" tabindex="-1" disabled="true" onClick="javascript:fnCalculateDiscount()" value="&#818;${varCalc}"/>&nbsp;
			<%}%>
<%
			if (strOpt.equals("PHONE"))
			{
%>				<fmtCart:message key="BTN_UPD_PRICE" var="varUpdPrice"/>
				<gmjsp:button gmClass="Button" name="Btn_UpdatePrice" buttonType="Save" style="overflow:visible" accesskey="U" tabindex="17" onClick="fnUpdatePrice();" value="&#818;${varUpdPrice}" />&nbsp;
<%
			}
%>				
			</td>
		</tr>		
    </table>
    <script>
    var ordtype = parent.document.all.Cbo_OrdType.value;    
    fnLoad(ordtype);
    </script>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
