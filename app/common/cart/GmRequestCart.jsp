 <%@page import="com.globus.common.beans.GmCommonClass"%>
<%
/**********************************************************************************
 * File		 		: GmRequestCart.jsp
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@page import="java.sql.Date"%>
<%@ taglib prefix="fmtRequestCart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRequestCart.jsp -->
<fmtRequestCart:setLocale value="<%=strLocale%>"/>
<fmtRequestCart:setBundle basename="properties.labels.custservice.ProcessRequest.GmItemInitiate"/>
<%
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	
	int intTRCount = 3;
	int intSize = 0;
	int intAlSize = 0;
	
	HashMap hcboVal = new HashMap();
	String strCodeID = "";
	String strSelected = "";
	String strPartOrdType = "";
	String strTotal = "";
	
	HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
	ArrayList alSets = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alSets"));
	ArrayList alReturn = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alReturn"));
	
	ArrayList alWareHouse = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alWareHouse"));
	

	ArrayList alLoop = new ArrayList();
	String strApplJSDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessJSDateFmt"));
	log.debug(" INside cart -- " + alSets);
	if (hmReturn != null)
	{
			
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Common Cart</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/requestCart.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/jquery-1.7.2.min.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

</HEAD>
<FORM name="frmCart" method="POST" action="<%=strServletPath%>/GmCommonCartServlet" >
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hRowCnt" value="<%=intTRCount%>">
<input type="hidden" name="hTotal" value="">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hPartNumStr" value="">
<input type="hidden" name="hformatJS" value="<%=strApplJSDateFmt%>">

<input type="hidden" name="RE_FORWARD" value="gmRequestInitiate">
<input type="hidden" name="txnStatus" value="PROCESS">
<input type="hidden" name="RE_TXN" value="REQUESTINITWRAPPER">
<input type="hidden" name="hTissueParts" id="hTissueParts">
<input type="hidden" name="hpartfamily" id="hpartfamily" value="">
<input type="hidden" name="hTissueSet" id="hTissueSet">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" >
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	
	<tr class="shade">
			<td class="RightTableCaption" height="30" width="102" align="right">&nbsp;<fmtRequestCart:message key="LBL_SETBUNDLE_LIST"/>:&nbsp;</td>
			<td colspan="2">
			<jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_SetbundleId" />
					<jsp:param name="METHOD_LOAD" value="loadSetBundleNameList" />
					<jsp:param name="WIDTH" value="400" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="1"/>
					<jsp:param name="CONTROL_NM_VALUE" value="" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="" />  					
					<jsp:param name="SHOW_DATA" value="200" />
					<jsp:param name="AUTO_RELOAD" value="" />
							</jsp:include></td>		</tr>
		<tr><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr class="">
			<td class="RightTableCaption" height="30" width="102" align="right">&nbsp;<fmtRequestCart:message key="LBL_SETLIST"/>:</td>
			<td colspan="2">
			<%-- &nbsp;<gmjsp:dropdown controlName="Cbo_Set"  seletedValue="" width = "600"	
							value="<%=alSets%>" codeId="ID"  codeName="IDNAME"  defaultValue= "[Choose One]" tabIndex="1" disabled="disabled"/>  --%>
			 <jsp:include page="/common/GmAutoCompleteInclude.jsp" >
					<jsp:param name="CONTROL_NAME" value="Cbo_Set" />
					<jsp:param name="METHOD_LOAD" value="loadLoanerSetList" />
					<jsp:param name="WIDTH" value="600" />
					<jsp:param name="CSS_CLASS" value="search" />
					<jsp:param name="TAB_INDEX" value="1"/>
					<jsp:param name="CONTROL_NM_VALUE" value="" /> 
					<jsp:param name="CONTROL_ID_VALUE" value="" />  					
					<jsp:param name="SHOW_DATA" value="200" />
					<jsp:param name="AUTO_RELOAD" value="" />
							</jsp:include>
			</td>
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr class="shade">
			<td height="30" class="RightTableCaption" width="102" align="right">&nbsp;<fmtRequestCart:message key="LBL_PART_NUMBER"/>:</td>
			<td> &nbsp;<input type="text" size="40" value="" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" onkeyup="fnChangeButTag();" tabindex="2">
			</td>
			<fmtRequestCart:message key="BTN_ADDTOCART" var="varAddToCart"/>
			<td><gmjsp:button value="&nbsp;${varAddToCart}&nbsp;" name="Btn_GoCart" gmClass="button" buttonType="Load" onClick="fnAddToCart();" tabindex="3" /></td>
		</tr>
		<% if (!strCountryCode.equals("en")) {%>
		<tr id="divAddInfoTR"><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr>
		<tr id="divAddInfo" class="Shade">
			<td height="30"   class="RightTableCaption" width="102" align="right">&nbsp;<fmtRequestCart:message key="LBL_ADDITIONALINFO"/>:</td>
			<td colspan="2"> &nbsp;<input type="text" size="40" maxLength="200" value="" name="Txt_AddInfo" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="4">
			</td>
		</tr>
		<% } else {%>
		<input type="hidden" size="40" maxLength="200" value="" name="Txt_AddInfo" ></input>
		<% }  %>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<TR>
			<td colspan="3">
			<div style="overflow:auto; height:252px;" >
			<table cellpadding="0" cellspacing="0" width="100%" border="0" bordercolor="gainsboro" id="PartnPricing"  >
			  <thead>
				<TR bgcolor="#EEEEEE" class="RightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
					<TH class="RightText" width="20" align="center">#</TH>
					<TH class="RightText" width="10" align="center"><a href="javascript:fnClearCart();" tabindex=-1><img border="0" Alt='Clear Cart' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></TH>
					<TH class="RightText" width="70" align="center"><fmtRequestCart:message key="LBL_PART"/>/<BR><fmtRequestCart:message key="LBL_SETID"/></TH>
					<TH class="RightText" width="300" >&nbsp;<fmtRequestCart:message key="LBL_DESCRIPTION"/></TH>
					<TH class="RightText" width="50" align="center"><div id="shelfqty"> <fmtRequestCart:message key="LBL_FG"/><BR><fmtRequestCart:message key="LBL_QTY"/></div></TH>
					<TH id="rwqty" class="RightText" width="50" align="center" style="display: none;"><fmtRequestCart:message key="LBL_RW"/><BR><fmtRequestCart:message key="LBL_QTY"/></TH>
					<TH id="whType" class="RightText" width="100" align="center" style="display: none;"><fmtRequestCart:message key="LBL_WAREHOUSE"/></TH>
					<TH id="tdReqQty" class="RightText" width="50" ><fmtRequestCart:message key="LBL_REQ"/> <BR><fmtRequestCart:message key="LBL_QTY"/></TH>
					<TH id="tdDateReq"  class="RightText" width="90" align="center"><div id ="date2"><fmtRequestCart:message key="LBL_DATEREQ"/></div></TH>						
				</TR>
				<tr><th class="Line" height="1" colspan="7"></th></tr>
			  </thead>
			  <TBODY>
<%
				String strPartNum = "";
				String strQty = "";
				String strPartDesc = "";
				String strBOFlag = "";
				String strParttype = "";
				String strCapFl = "";
				String strPrice = "";
				String strExtPrice = "";
				String strStock = "";
				String strRwStock = "";
				String strGroupId = "";
				String strCapSelected = "";
				String strLblWareHouse = "";
				int intQty = 0;
				int tbCnt =4;
				String strDate = "";
				Date  dtRequestFrom = null;
				String strShipDtCtrl = "";
				if (alReturn != null)
				{
					intAlSize = alReturn.size();
				}
						log.debug(" values in loop " + alReturn);				
				for (int i=0; i < intTRCount;i++)
				{
					dtRequestFrom = null;
					strShipDtCtrl = "Txt_ReqDate"+i;
					if (i<intAlSize)
					{
						alLoop = (ArrayList)alReturn.get(i);
						strPartNum = (String)alLoop.get(0);
						strQty = (String)alLoop.get(1);
						strPrice = (String)alLoop.get(5);
						strGroupId = (String)alLoop.get(6);
						strCapSelected = strGroupId.equals("")?"":"checked";
						strStock = (String)alLoop.get(8) + "&nbsp;&nbsp;";
						strRwStock = "";// (String)alLoop.get(9) + "&nbsp;&nbsp;";
						strPartDesc = "&nbsp;".concat(GmCommonClass.getStringWithTM((String)alLoop.get(7)));
						intQty = Integer.parseInt(strQty);
					}
					else
					{
						strPartNum = "";
						strPartDesc = "";
						strPrice = "";
						strQty = "";
						strExtPrice = "";
						strStock = "&nbsp;";
						strRwStock = "&nbsp;";
						strGroupId = "";
						strCapSelected = "";
					}
					alLoop = null;

					strLblWareHouse = "warehouseType" + i;
%>
				<tr>
					<td class="RightText"><%=i+1%></td>
					<td class="RightText"><a href="javascript:fnRemoveItem('<%=i%>');" tabindex="-1"><img border="0" Alt='Remove from cart' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></td>
					<td id="Lbl_Part<%=i%>" class="RightText">&nbsp;<%=strPartNum%></td>
					<td id="Lbl_Desc<%=i%>" class="RightText">&nbsp;<%=strPartDesc%></td>
					<td id="Lbl_Stock<%=i%>" class="RightText" align="right"><%=strStock%></td>
					<td id="Lbl_RwStock<%=i%>" class="RightText" align="right"  style="display: none;"><%=strRwStock%></td>
					<td id="Lbl_WareHouse<%=i%>" class="RightText" align="right" style="display: none;">
						<gmjsp:dropdown controlId="<%=strLblWareHouse%>" controlName="<%=strLblWareHouse%>" value="<%=alWareHouse%>" codeId = "ID"  codeName = "WAREHOUSE_SH_NM" defaultValue= "[Choose One]" />
					</td>
					<td align="center">&nbsp;<input type="text" size="3" value="<%=strQty%>" class=InputArea name="Txt_Qty<%=i%>" onBlur="changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');" tabindex="<%=tbCnt+1%>"><input type="hidden" name="hStock<%=i%>" value=""></td>
					<td id="Lbl_Cal<%=i%>" width="160px">&nbsp;<gmjsp:calendar textControlName="<%=strShipDtCtrl%>" textValue="<%=dtRequestFrom%>" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
					 <% if (i ==0 ){  %><input type="checkbox" name="datechk" onclick="copyDate(this)"/> <% }%></td>
					
				</tr>
				<tr><td colspan="9" class="LLine" height="1"></td></tr>
<%
		tbCnt=tbCnt+2;
				}
%>
			 
			  </TBODY>
			</table>
			</div>
			</td>
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<tr>
			<td colspan="3" align="center" height="35">
				<fmtRequestCart:message key="BTN_PARTLOOKUP" var ="varPartLookup"/>
				<fmtRequestCart:message key="BTN_ADDROW" var="varAddRow"/>
				<gmjsp:button gmClass="button" name="Btn_PartLookUp" buttonType="Load" buttonTag="False" accesskey="P" tabindex="-1" onClick="fnOpenPart();" value="${varPartLookup}" />&nbsp;
				<gmjsp:button value="${varAddRow}" gmClass="button" buttonType="Save" onClick="fnAddRow('PartnPricing');" />&nbsp;&nbsp;&nbsp;
			</td>
		</tr><tr><td colspan="3" class="Line" height="1"></td></tr>
    </table>
</FORM>
<script>
cnt = document.frmCart.hRowCnt.value;

</script>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
