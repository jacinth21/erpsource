 
<%
/**********************************************************************************
 * File		 		: GmStockRequestCart.jsp
 * Version	 		: 1.0
 * author			: ppandiyan
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@page import="java.sql.Date"%>
<%@ taglib prefix="fmtRequestCart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmStockRequestCart.jsp -->
<fmtRequestCart:setLocale value="<%=strLocale%>"/>
<fmtRequestCart:setBundle basename="properties.labels.custservice.ProcessRequest.GmStockInitiate"/>
<%

	
	int intTRCount = 8;
	int intAlSize = 0;

	ArrayList alReturn = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alReturn"));

	ArrayList alLoop = new ArrayList();
	String strApplJSDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessJSDateFmt"));


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Common Cart</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmStockRequestCart.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/jquery-1.7.2.min.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
</HEAD>
<FORM name="frmCart" method="POST" action="<%=strServletPath%>/GmCommonCartServlet" >
<input type="hidden" name="hAction" value="ReqCart">
<input type="hidden" name="hRowCnt" value="<%=intTRCount%>">
<input type="hidden" name="hTotal" value="">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hPartNumStr" value="">
<input type="hidden" name="hformatJS" value="<%=strApplJSDateFmt%>">
<input type="hidden" name="hScreen" value="gmStockInitiate">

 
	<table border="0" cellspacing="0" cellpadding="0" width="100%" >

		<tr><td colspan="3" height="1" bgcolor="#CCCCCC"></td></tr>
		<tr>
			<td height="30" class="RightTableCaption"align="right">&nbsp;<fmtRequestCart:message key="LBL_PART_NUMBER"/>:</td>
			<td colspan="1"> &nbsp;<input type="text" size="70" value="" name="Txt_PartNum" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabindex="2">
			
			<fmtRequestCart:message key="BTN_ADDTOCART" var="varAddToCart"/>
			&nbsp;&nbsp;&nbsp;&nbsp;<gmjsp:button value="&nbsp;${varAddToCart}&nbsp;" name="Btn_GoCart" gmClass="button" buttonType="Load" onClick="fnAddToCart();" tabindex="3" /></td>
		</tr>

		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<TR>
			<td colspan="3">
			<div style="overflow:auto; height:252px;" >
			<table cellpadding="0" cellspacing="0" width="100%" border="0" bordercolor="gainsboro" id="PartnPricing"  >
			  <thead>
				<TR bgcolor="#EEEEEE" class="RightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
					<TH class="RightText" width="20" align="center">#</TH>
					<TH class="RightText" width="10" align="center"><a href="javascript:fnClearCart();" tabindex=-1><img border="0" Alt='Clear Cart' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></TH>
					<TH class="RightText" width="70" align="center"><fmtRequestCart:message key="LBL_PART"/></TH>
					<TH class="RightText" width="400" >&nbsp;<fmtRequestCart:message key="LBL_DESCRIPTION"/></TH>
					
					<TH id="tdReqQty" class="RightText" width="50" ><fmtRequestCart:message key="LBL_REQ"/> <BR><fmtRequestCart:message key="LBL_QTY"/></TH>
						
				</TR>
				<tr><th class="Line" height="1" colspan="7"></th></tr>
			  </thead>
			  <TBODY>
<%
				String strPartNum = "";
				String strQty = "";
				String strPartDesc = "";
			
				int intQty = 0;
				int tbCnt =8;
			
				if (alReturn != null)
				{
					intAlSize = alReturn.size();
				}
									
				for (int i=0; i < intTRCount;i++)
				{

					if (i<intAlSize)
					{
						alLoop = (ArrayList)alReturn.get(i);
						strPartNum = (String)alLoop.get(0);
						strQty = (String)alLoop.get(1);
						strPartDesc = "&nbsp;".concat(GmCommonClass.getStringWithTM((String)alLoop.get(7)));
						intQty = Integer.parseInt(strQty);
					}
					else
					{
						strPartNum = "";
						strPartDesc = "";
						strQty = "";

					}
					alLoop = null;

					
%>
				<tr>
					<td class="RightText"><%=i+1%></td>
					<td class="RightText"><a href="javascript:fnRemoveItem('<%=i%>');" tabindex="-1"><img border="0" Alt='Remove from cart' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></td>
					<td id="Lbl_Part<%=i%>" class="RightText">&nbsp;<%=strPartNum%></td>
					<td id="Lbl_Desc<%=i%>" class="RightText">&nbsp;<%=strPartDesc%></td>
					<td align="center">&nbsp;<input type="text" size="3" value="<%=strQty%>" class=InputArea name="Txt_Qty<%=i%>" onBlur="changeBgColor(this,'#ffffff');" 
					onFocus="changeBgColor(this,'#AACCE8');" onkeypress="return isNumberKey(event);" tabindex="<%=tbCnt+1%>">
					<input type="hidden" name="hStock<%=i%>" value=""></td>

					
				</tr>
				<tr><td colspan="9" class="LLine" height="1"></td></tr>
<%

		tbCnt=tbCnt+1;
				}
%>
			 
			  </TBODY>
			</table>
			</div>
			</td>
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<tr>
			<td colspan="3" align="center" height="35">
				<fmtRequestCart:message key="BTN_PARTLOOKUP" var ="varPartLookup"/>
				<fmtRequestCart:message key="BTN_ADDROW" var="varAddRow"/>
				<gmjsp:button gmClass="button" name="Btn_PartLookUp" buttonType="Load" accesskey="P" onClick="fnOpenPart();" value="${varPartLookup}" />&nbsp;
				<gmjsp:button value="${varAddRow}" gmClass="button" buttonType="Save" onClick="fnAddRow('PartnPricing');" />&nbsp;&nbsp;&nbsp;
			</td>
		</tr><tr><td colspan="3" class="Line" height="1"></td></tr>
    </table>
</FORM>
<script>
cnt = document.frmCart.hRowCnt.value;

</script>

</BODY>

</HTML>
