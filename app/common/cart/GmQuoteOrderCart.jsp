 <%
/**********************************************************************************
 * File		 		: GmCommonCart.jsp
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>


<!--common\GmQuoteOrderCart.jsp -->
<%@ include file="/common/GmHeader.inc"%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>

<!-- Imports for Logger -->

 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
 <%@ taglib prefix="fmtQuoteCart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtQuoteCart:setLocale value="<%=strLocale%>"/>
<fmtQuoteCart:setBundle basename="properties.labels.common.cart.GmCommonCart"/>
<%
try {
	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	//Logger log = GmLogger.getInstance(GmCommonConstants.ACCOUNTING);

	int intTRCount = 10;
	int intSize = 0;
	int intAlSize = 0;
	HashMap hcboVal = new HashMap();
	String strCodeID = "";
	String strSelected = "";
	String strPartOrdType = "";
	String strConCode = GmCommonClass.parseZero((String)request.getAttribute("hConstructId"));
	String strGpoId = GmCommonClass.parseZero((String)request.getAttribute("hGpoId"));
	String strConsValue = GmCommonClass.parseZero((String)request.getAttribute("hConsValue"));
	String strTotal = "";
	String strOpt = GmCommonClass.parseNull((String)session.getAttribute("strSessOpt"));
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	String strShowDiscount = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.SHOW_DISCOUNT"));
	
	GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.common.cart.GmCommonCart", strSessCompanyLocale);
	
	String strQtyLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PHONE_QTY"));
	String strDescSpan = "1";
	
	if (strOpt.equals("QUOTE"))
	{
		strQtyLabel = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_QUOTE_QTY"));
		strDescSpan = "4";
	}
	
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	ArrayList alOrderPartType = new ArrayList();
	ArrayList alConstructs = new ArrayList();
	ArrayList alReturn = (ArrayList)request.getAttribute("alReturn");
	ArrayList alLoop = new ArrayList();
	
	if (hmReturn != null)
	{
		alOrderPartType = (ArrayList)hmReturn.get("ORDPARTTP");
		alConstructs = (ArrayList)hmReturn.get("CONSTRUCTS");
	}
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Common Cart</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmQuoteOrderCart.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script>
var req;
var trcnt = 0;
var cnt = <%=intTRCount%>;
var showDiscount='<%=strShowDiscount%>';
var vcountryCode = '<%=strCountryCode%>';
</script>
</HEAD>
<FORM name="frmCart" method="POST" action="<%=strServletPath%>/GmCommonCartServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hRowCnt" value="<%=intTRCount%>">
<input type="hidden" name="hTotal" value="">
<input type="hidden" name="hGpoId" value="<%=strGpoId%>">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="hPartNumStr" value="">
<input type="hidden" name="hConsValue" value="<%=strConsValue%>">

<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
<%
			if (strOpt.equals("PHONE"))
			{
%>
	
			<td colspan="3" class="RightText" height="30">&nbsp;<fmtQuoteCart:message key="LBL_CONS_PRICE_CODE"/>:
				<select name="Cbo_Construct" onchange="javascript:fnCheckConstructAccount(this)" class="RightText" tabindex="11" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
					<option value="0" ><fmtQuoteCart:message key="LBL_CHOOSE_ONE"/>
<%
			  		intSize = alConstructs.size();
					hcboVal = new HashMap();
			  		for (int i=0;i<intSize;i++)
			  		{
			  			hcboVal = (HashMap)alConstructs.get(i);
			  			strCodeID = (String)hcboVal.get("CID");
						strSelected = strConCode.equals(strCodeID)?"selected":"";
%>
					<option id="<%=(String)hcboVal.get("CVALUE")%>" <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CNAME")%> - <%=hcboVal.get("CDESC")%> - $<%=GmCommonClass.getStringWithCommas((String)hcboVal.get("CVALUE"))%></option>
<%
			  		}
%>

				</select>
			</td>
<%
			}else if (strOpt.equals("QUOTE")){
%>
				<td colspan="2"><input type="hidden" name="Cbo_Construct" value=0></td>
<%
			}
%>			
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<TR>
			<td colspan="3">
			<div style="overflow:auto; height:252px;">
			<table cellpadding="0" cellspacing="0" width="100%" border="0" bordercolor="gainsboro" id="PartnPricing">
			  <thead>
				<TR bgcolor="#EEEEEE" class="RightTableCaption" style="position:relative; top:expression(this.offsetParent.scrollTop);">
					<TH class="RightText" width="10" align="center">#</TH><fmtQuoteCart:message key="IMG_ALT_CLEAR" var="varClear"/>
					<TH class="RightText" width="10" align="center"><a href="javascript:fnClearCart();" tabindex="-1"><img border="0" Alt='${varClear}' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></TH>
					<TH class="RightText" width="70" align="center"><fmtQuoteCart:message key="LBL_PART"/><BR><fmtQuoteCart:message key="LBL_NUMBER"/></TH>
					<TH class="RightText" width="50" align="center"><%=strQtyLabel %><BR><fmtQuoteCart:message key="LBL_QTY"/></TH>
					<TH class="RightText" width="50" align="center"><fmtQuoteCart:message key="LBL_SHELF"/><BR><fmtQuoteCart:message key="LBL_QTY"/></TH>
<%
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA"))
			{
%>					
					<TH class="RightText" width="40" align="center"><fmtQuoteCart:message key="LBL_BO"/><BR><fmtQuoteCart:message key="LBL_FLAG"/></TH>
<%
			}
%>
					
					<TH class="RightText" width="325" colspan="<%=strDescSpan %>" >&nbsp;<fmtQuoteCart:message key="LBL_PART_DESC"/></TH>
<%
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA"))
			{
%>					
					<TH class="RightText" width="40" align="center"><fmtQuoteCart:message key="LBL_TYPE"/></TH>
					<TH class="RightText" width="70" align="center"><fmtQuoteCart:message key="LBL_TAG_ID"/>/<BR><fmtQuoteCart:message key="LBL_ETCH_ID"/></TH>
					<TH class="RightText" width="40" align="center"><fmtQuoteCart:message key="LBL_CAP"/><BR><fmtQuoteCart:message key="LBL_FLAG"/></TH>
<%
			}
			if (strOpt.equals("QUOTE")){
%>
					<TH class="RightText" width="60" align="center"><fmtQuoteCart:message key="LBL_BASE"/> <BR><fmtQuoteCart:message key="LBL_PRICE_EA"/></TH>
<%
			}
%>
					<TH class="RightText" width="60" align="center"><fmtQuoteCart:message key="LBL_PRICE_BR_EA"/></TH>
					<TH class="RightText" width="80" align="center"><fmtQuoteCart:message key="LBL_EXT_PRICE"/></TH>
<%					
			if (strOpt.equals("PHONE")){ 
%>					 
					<TH class="RightText" width="135" align="center"><fmtQuoteCart:message key="LBL_CONTROL"/><BR><fmtQuoteCart:message key="LBL_NUMBER"/></TH>
<%
			}
%>
				</TR>	
				<tr><th class="Line" height="1" colspan="14"></th></tr>
			  </thead>
			  <TBODY>
<%
				String strPartNum = "";
				String strQty = "";
				String strPartDesc = "";
				String strBOFlag = "";
				String strParttype = "";
				String strCapFl = "";
				String strPrice = "";
				String strExtPrice = "";
				String strStock = "";
				String strGroupId = "";
				String strCapSelected = "";
				String strLoanCogs = "";
				double dbPrice = 0.0;
				double dbExtPrice = 0.0;
				double dbTotal = 0.0;
				int intQty = 0;
				String strLoanCogsChk = "";
				String strRefId = "";
				if (alReturn != null)
				{
					intAlSize = alReturn.size();
				}
				
				for (int i=0; i < intTRCount;i++)
				{
					if (i<intAlSize)
					{
						alLoop = (ArrayList)alReturn.get(i);
						strPartNum = (String)alLoop.get(0);
						strQty = (String)alLoop.get(1);
						strPrice = (String)alLoop.get(5);
						strGroupId = (String)alLoop.get(6);
						strCapSelected = strGroupId.equals("")?"":"checked";
						strStock = (String)alLoop.get(8) + "&nbsp;&nbsp;";
						strPartDesc = "&nbsp;".concat(GmCommonClass.getStringWithTM((String)alLoop.get(7)));
						intQty = Integer.parseInt(strQty);
						dbPrice = Double.parseDouble(strPrice);
						dbExtPrice = dbPrice * intQty;
						dbTotal = dbTotal + dbExtPrice;
						strPrice = GmCommonClass.getStringWithCommas(strPrice);
						strExtPrice = GmCommonClass.getStringWithCommas(""+dbExtPrice).concat("&nbsp;");
						strLoanCogs = (String)alLoop.get(9);
						if (!strPartNum.equals("")&& !strQty.equals("") && strLoanCogs.equals(""))
						{
							strLoanCogsChk = "disabled";
						}
						else
						{
							strLoanCogsChk = "";
						}
					}
					else
					{
						strPartNum = "";
						strPartDesc = "";
						strPrice = "";
						strQty = "";
						strExtPrice = "";
						strStock = "";
						strGroupId = "";
						strCapSelected = "";
						strLoanCogs = "";
					}
					alLoop = null;

%>
				<tr>
					<td class="RightText"><%=i+1%></td><fmtQuoteCart:message key="IMG_ALT_RMV_CART" var="varRmvCart"/>
					<td class="RightText"><a href="javascript:fnRemoveItem('<%=i%>');" tabindex="-1"><img border="0" Alt='${varRmvCart}' valign="left" src="<%=strImagePath%>/btn_remove.gif" height="10" width="9"></a></td>
					<td><input type="text" size="10" class=InputArea value="<%=strPartNum%>" name="Txt_PNum<%=i%>" onBlur=fnSetPartSearch('<%=i%>',this); onFocus="changeBgColor(this,'#AACCE8');"></td>
					<td align="center">&nbsp;<input type="text" size="3" value="<%=strQty%>" class=InputArea name="Txt_Qty<%=i%>" onblur="validate(<%=i%>,this);" onFocus="changeBgColor(this,'#AACCE8');"></td>
					<td id="Lbl_Stock<%=i%>" class="RightText" align="right"><%=strStock%></td>
<%
			if (strOpt.equals("PHONE") ||  strOpt.equals("PROFORMA"))
				{
					String strDisable = "";
					if(strOpt.equals("PROFORMA")){
						strDisable = "disabled";
					}
%>					
					<td align="center"><input type=checkbox name="Chk_BO<%=i%>" <%=strDisable%> tabindex="-1"><input type="hidden" name="hLoanCogs<%=i%>" value="<%=strLoanCogs%>"></td>
<%
			}
%>					
					<td  id="Lbl_Desc<%=i%>" class="RightText" ><%=strPartDesc%></td>
<%
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA"))
			{
%>					
					<td align="center"><select <%=strLoanCogsChk %> tabindex="-1" name="Cbo_OrdPartType<%=i%>" class="RightText" onchange="fnChangeStatus(this.value,'<%=i%>');" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');">
<%
			  		intSize = alOrderPartType.size();
			  		hcboVal = new HashMap();
			  		for (int j=0;j<intSize;j++)
			  		{
			  			hcboVal = (HashMap)alOrderPartType.get(j);
			  			strCodeID = (String)hcboVal.get("CODEID");
						strSelected = strPartOrdType.equals(strCodeID)?"selected":"";
%>
								<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("CODENMALT")%></option>
<%
			  		}
			  		strPartOrdType = "";
			  		strLoanCogs = "";
			  		strLoanCogsChk = "";
%>
						</select>
					</td>
					<td><input type="text" size="7" class="InputArea" value="<%=strRefId%>" name="Txt_RefId<%=i%>" tabindex="-1" onblur="fnChkValidFieldVal(this,<%=i%>);changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');"></td>
					<td align="center"><input type=checkbox <%=strCapSelected%> name="Chk_Cap<%=i%>" tabindex="-1" value=""></td>
<%
			}else if (strOpt.equals("QUOTE")){
%>
				<td colspan="3">	
					<input type=hidden name="Chk_BO<%=i%>" tabindex="-1">
					<input type="hidden" name="hLoanCogs<%=i%>" value="<%=strLoanCogs%>">
					<input type="hidden" name="Cbo_OrdPartType<%=i%>" value="50300">
					<input type="hidden" name="Txt_RefId<%=i%>" value="">
					<input type="hidden" name="Chk_Cap<%=i%>" value="">
				</td>
				<%-- <td id="Lbl_BasePrice<%=i%>" class="RightText" align="right">
					<input type="text" size="8" value="<%=strPrice%>" tabindex="-1" class=InputArea name="Txt_BasePrice<%=i%>" onBlur="fnCalExtPrice(this,'<%=i%>','Y');changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');"></td> --%>
<%
			}
			if (strOpt.equals("QUOTE")){
%>						
					<td id="Lbl_BasePrice<%=i%>" class="RightText" align="right">
					<input type="text" size="8" value="<%=strPrice%>" tabindex="-1" class=InputArea name="Txt_BasePrice<%=i%>" onBlur="fnCalExtPrice(this,'<%=i%>','Y');changeBgColor(this,'#ffffff');" onFocus="changeBgColor(this,'#AACCE8');"></td>
<%
			}
%>				
					<td id="Lbl_Price<%=i%>" class="RightText" align="right">
					<input type="text" size="8" value="<%=strPrice%>" tabindex="-1" class=InputArea name="Txt_Price<%=i%>" onBlur="fnCalExtPrice(this,'<%=i%>','Y');" onFocus="changeBgColor(this,'#AACCE8');" tabindex="-1"><input type="hidden" name="hPartPrice<%=i%>" value="<%=strPrice%>"/><input type="hidden"  name="Lbl_PriceEA<%=i%>" value="<%=strPrice%>" />
					<input type="hidden"  name="hBasePrice<%=i%>" value="<%=strPrice%>" /></td>
					<td id="Lbl_Amount<%=i%>" class="RightText" align="right"><%=strExtPrice%></td>
					<% if (strOpt.equals("PHONE")){ %>
						 <td>&nbsp;<input type="text" size="7" class=InputArea value="" name="Txt_Cnum<%=i%>" id="Txt_Cnum<%=i%>" tabindex="-1" style="text-transform: uppercase;" onBlur="fnValidateCtrlNum(<%=i%>,this);changeBgColor(this,'#ffffff');" onFocus="fnRemoveImage(<%=i%>,this);changeBgColor(this,'#AACCE8');" maxlength="20">
						 <input type="hidden" name="hCntrlNumberNeededFl<%=i%>" value="">
						 <fmtQuoteCart:message key="IMG_ALT_CTRL_AVAIL" var="varCtrl"/>
						 <fmtQuoteCart:message key="IMG_ALT_CTRL_NOT_AVAIL" var="varNoCtrl"/>
						 <span id="DivShowCnumAvl<%=i%>" style="vertical-align:middle; display: none;"> <img height="16" width="19" title="${varCtrl}" src="<%=strImagePath%>/success.gif"></img></span>
						 <span id="DivShowCnumNotExists<%=i%>" style="vertical-align:middle; display: none;"> <img height="12" width="12" title="${varNoCtrl}" src="<%=strImagePath%>/delete.gif"></img></span>
						 </td>
					 <%}%>
				</tr>
				<tr><td colspan="14" class="LLine" height="1"></td></tr>
				<!--<tr><td bgcolor="#eeeeee" height="1" colspan="11"></td></tr>-->
<%
				}
				strTotal = ""+dbTotal;
%>
			  </TBODY>
			</table>
			</div>
			</td>
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="Shade" height="23">
		 
			<td class="RightText" width="750" align="right" ><B> <fmtQuoteCart:message key="LBL_TOTAL_PRICE"/>: </B>&nbsp;</td>
			<td class="RightCaption" align="right" width="120" id="Lbl_Total"><gmjsp:currency type="CurrTextSign"  textValue="<%=strTotal%>" td="false"/>
<%			
			if (strOpt.equals("PHONE") || strOpt.equals("PROFORMA")){ 
%>	
			  <td class="RightCaption" width="120">&nbsp;</td>
<% 
			}
%> 		
		</tr>
		<tr><td colspan="3" class="Line" height="1"></td></tr>
		<tr>
			<td colspan="3" align="center" height="25">
			<script>
				document.frmCart.hTotal.value = '<%=strTotal%>';
			</script>
				<input type="button" class="Button"  name ="Btn_PartLookUp" accesskey="P" tabindex="-1" onClick="fnOpenPart();" value ="&#818;<fmtQuoteCart:message key="BTN_PART_LKP"/>" />&nbsp;&nbsp;
				<fmtQuoteCart:message key="BTN_ADD_ROW" var="varAddRow"/>
				<gmjsp:button value="${varAddRow}" gmClass="Button" buttonType="Save" onClick="fnAddRow('PartnPricing');" tabindex="16" />&nbsp;&nbsp;
				<%if(strShowDiscount.equalsIgnoreCase("YES")){%>
			<fmtQuoteCart:message key="BTN_CALC" var="varCalc"/>
			<gmjsp:button gmClass="Button" name="Btn_Calculate" buttonType="Save" accesskey="A" tabindex="-1" disabled="true" onClick="javascript:fnCalculateDiscount()" value="&#818;${varCalc}"/>&nbsp;
			<%}%>
<%
			if (strOpt.equals("PHONE"))
			{
%>				<fmtQuoteCart:message key="BTN_UPD_PRICE" var="varUpdPrice"/>
				<gmjsp:button gmClass="Button" name="Btn_UpdatePrice" buttonType="Save" accesskey="U" tabindex="17" onClick="fnUpdatePrice();" value="&#818;${varUpdPrice}" />&nbsp;
<%
			}
%>				
			</td>
		</tr>		
    </table>
    <script>
    var ordtype = parent.document.all.Cbo_OrdType.value;
    fnLoad(ordtype);
    </script>
</FORM>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>
