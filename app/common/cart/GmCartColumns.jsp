

<!--common\GmCartColumns.jsp -->


<!DOCTYPE html>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtCart" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>



<html lang="en">
<%	
String strCommonJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_COMMON");

	String strTagColumn = GmCommonClass.parseNull((String)request.getParameter("tagColumn"));
	String strCapFlColumn = GmCommonClass.parseNull((String)request.getParameter("capFlColumn"));
	
	if(!strSessCompanyLocale.equals("")){
		locale = new Locale("en", strSessCompanyLocale);
		strLocale = GmCommonClass.parseNull((String)locale.toString());
	}
	String strCheck = "";
	String strColumnCheck = "";
	String strTagCheck = "";
	String strCapFlChaeck = "";
	String strShowColumns = "";
	String strShowTagColumns = "0";
	String strShowCapColumns = "0";
	if(strTagColumn.equals("true")){
		strTagCheck = "checked";
		strShowTagColumns = "1";
	}
	if(strCapFlColumn.equals("true")){
		strCapFlChaeck = "checked";
		strShowCapColumns = "1";	
	}
%>
<fmtCart:requestEncoding value="UTF-8" />
<fmtCart:setLocale value="<%=strLocale%>"/>
<fmtCart:setBundle basename="properties.labels.common.cart.GmCommonCart"/>
<head>
<meta charset="UTF-8">
<title>Add More Columns </title>
<link rel="stylesheet" href="<%=strCssPath%>/bootstrap.min.css">
<link rel="stylesheet" href="<%=strCssPath%>/bootstrap-theme.min.css">
<script src="<%=strCommonJsPath%>/jquery.min.js"></script>
<script src="<%=strCommonJsPath%>/bootstrap.min.js"></script>
</head> 
 
<body>
<fmtCart:message key="BTN_CART_TITLE" var="varTitle"/>
<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal" type="button" title="${varTitle}" >
					 >>
</button>
				  
<div id="myModal" class="modal fade" data-color="blue">
    <div class="modal-dialog" style="width:245px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><fmtCart:message key="TITLE_MORE_COLS"/></h4>
            </div>
            <div class="modal-body" >
                  
				  <%
			  		int ColumnSize = Integer.valueOf((String)request.getParameter("cols")); 
					 
			  		for (int i=1;i<=ColumnSize ;i++)
			  		{
						String columnName = (String)request.getParameter("col"+i);
						if(i == 16){
							strCheck = strTagCheck;
							strColumnCheck = strShowTagColumns;
						}else if(i == 17){
							strCheck = strCapFlChaeck;
							strColumnCheck = strShowCapColumns;
						}						
						if (columnName != null && !columnName.equals(""))
						{
					%>
						  <div class="form-group">
		                    <div class="col-sm-offset-1 col-sm-10">
		                      <div class="checkbox">
		                        <label>
		                            <input name="col<%=i%>" id="cols<%=i%>" type="checkbox" <%=strCheck %> /><%=columnName%> 
		                            <input type="hidden" id="col<%=i%>" value='<%=strColumnCheck%>'>  
		                        </label>
		                      </div>
		                    </div>
		                  </div>
					  <% 
						}
						
					} %>
				<!-- 
					  <div class="form-group">
	                    <div class="col-sm-offset-1 col-sm-10">
	                      <div class="checkbox">
	                        <label>
	                            <input name="col9" type="checkbox" checked /> Tag ID/Etch ID
	                        </label>
	                      </div>
	                    </div>
	                  </div>
					  
					  <div class="form-group">
	                    <div class="col-sm-offset-1 col-sm-10">
	                      <div class="checkbox">
	                        <label>
	                            <input name="col10" type="checkbox" checked /> Cap Flag
	                        </label>
	                      </div>
	                    </div>
	                  </div>
                  -->
                
            </div>
            
            <div class="modal-header" >
                                
            </div>
            
        </div>
    </div>
</div>
</body>
</html>                                		