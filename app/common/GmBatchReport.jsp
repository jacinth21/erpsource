<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<!-- common\GmBatchReport.jsp  -->

<%@ taglib prefix="fmtBatchRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtBatchRpt:setLocale value="<%=strLocale%>"/>
<fmtBatchRpt:setBundle basename="properties.labels.common.GmBatchReport"/>

<bean:define id="gridData" name="frmBatchReport" property="gridData" type="java.lang.String"></bean:define>
<%
	String strWikiTitle = GmCommonClass.parseNull(GmCommonClass.getWikiTitle("BATCH_RPT"));
	String strApplDateFmt = strGCompDateFmt;// GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
	String strAccountsBatchJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_ACCOUNTS_BATCH");
%>



<HTML>
<HEAD>
<TITLE>Globus Medical: Batch Report</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmErrorCommon.js"></script>
<script language="javascript" src="<%=strJsPath%>/GmGrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Accounts<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strAccountsBatchJsPath%>/GmBatchReport.js"></script>
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script>
var objGridData;
objGridData = '<%=gridData%>';
var gridObj = '';
var dtFormat = '<%=strApplDateFmt %>';
var lblBatchInitFrmDt = '<fmtBatchRpt:message key="LBL_BATCH_INIT_FROM_DT"/>';
var lblBatchInitToDt = '<fmtBatchRpt:message key="LBL_BATCH_INIT_TO_DT"/>';
</script>

</HEAD>


<BODY leftmargin="20" topmargin="10" onkeyup="fnEnter();" onload="fnOnPageLoad();">

	<html:form action="/gmBatchReport.do?method=loadBatchRpt">
		<html:hidden property="strOpt" />
		<html:hidden property="batchScreenType" />

		<table border="0" class="DtTable1000" width="700" cellspacing="0"
			cellpadding="0">
			<tr>
				<td colspan="5" height="25" class="RightDashBoardHeader"><bean:write
						property="jspHeader" name="frmBatchReport" />
				</td>
				<fmtBatchRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
				<td height="25" class="RightDashBoardHeader"><img align="right"
					id='imgEdit' style='cursor: hand' src='<%=strImagePath%>/help.gif'
					title='${varHelp}' width='16' height='16'
					onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
				</td>
			</tr>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr>
				<fmtBatchRpt:message key="LBL_BATCH_ID" var = "varBatchId"/>
				<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
						type="RegularText" SFLblControlName="${varBatchId}:" td="false" /></td>
				<td align="left">&nbsp;<html:text property="batchID"
						onfocus="changeBgColor(this,'#AACCE8');"
						onblur="changeBgColor(this,'#ffffff');" onkeypress="return fnNumbersOnly(event);"/></td>
				<fmtBatchRpt:message key="LBL_BATCH_STATUS" var = "varBatchSts"/>
				<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
						type="RegularText" SFLblControlName="${varBatchSts}:" td="false" />
				</td>
				<td align="left">&nbsp;<gmjsp:dropdown
						controlName="batchStatus" SFFormName="frmBatchReport"
						SFSeletedValue="batchStatus" SFValue="alBatchStatus"
						codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" />
				</td>
				<fmtBatchRpt:message key="LBL_BATCH_TYPE" var = "varBatchType"/>
				<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
						type="RegularText" SFLblControlName="${varBatchType}:" td="false" /></td>
				<td align="left">&nbsp;<gmjsp:dropdown controlName="batchType"
						SFFormName="frmBatchReport" SFSeletedValue="batchType"
						SFValue="alBatchType" codeId="CODEID" codeName="CODENM"
						defaultValue="[Choose One]" /></td>
			</tr>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<tr class="Shade">
				<fmtBatchRpt:message key="LBL_BATCH_INIT" var = "varBatchInit"/>
				<td height="30" align="Right" class="RightTableCaption"><gmjsp:label
						type="RegularText" SFLblControlName="${varBatchInit}:"
						td="false" />&nbsp;&nbsp;<fmtBatchRpt:message key="LBL_FRM_DT"/>:</td>
				<td align="left">&nbsp;<gmjsp:calendar
						SFFormName="frmBatchReport" controlName="batchFromDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" /></td>
				<fmtBatchRpt:message key="LBL_TO_DATE" var = "varToDt"/>
				<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
						type="RegularText" SFLblControlName="${varToDt}:" td="false" />
				</td>
				<td align="left">&nbsp;<gmjsp:calendar
						SFFormName="frmBatchReport" controlName="batchToDt"
						gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');"
						onBlur="changeBgColor(this,'#ffffff');" /></td>
				<fmtBatchRpt:message key="BTN_LOAD" var = "varLoad"/>
				<td height="25" align="center" class="RightTableCaption" colspan="2">
					<gmjsp:button value="${varLoad}" buttonType="Load" gmClass="button" name="btn_Load"
					style="width: 6em" onClick="fnLoad();" />
				</td>
			</tr>
			<%
				if (gridData.indexOf("cell") != -1) {
			%>
			<tr>
				<td colspan="6">
					<div id="batchReport" style="" height="400px" width="1000px"></div>
				</td>
			</tr>
			<tr>
				<td height="1" colspan="6" class="LLine"></td>
			</tr>
			
			<tr>
				<td colspan="6" align="center">
					<div class='exportlinks'>
						<fmtBatchRpt:message key="DIV_EXPORT_OPT"/> : <img src='img/ico_file_excel.png' />&nbsp;<a
							href="#" onclick="fnDownloadXLS();"><fmtBatchRpt:message key="DIV_EXCEL"/></a>
					</div></td>
			</tr>
			<%}else{ %>
			<tr>
				<td colspan="6" class="LLine" height="1"></td>
			</tr>
			<tr height="30">
				<td colspan="6" class="RightText" align="center"><fmtBatchRpt:message key="MSG_NO_DATA"/></td>
			</tr>
			<%} %>
		</table>



	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>