
<!-- common\GmExchangeRateReport.jsp -->
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ taglib prefix="fmtExchangeRateReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtExchangeRateReport:setLocale value="<%=strLocale%>"/>
<fmtExchangeRateReport:setBundle basename="properties.labels.common.GmExchangeRateReport"/>


<bean:define id="gridData" name="frmExchangeRate" property="gridData" type="java.lang.String"/> 

<%

String strWikiTitle = GmCommonClass.getWikiTitle("REAL_TIME_EXCHANGE_RATE_REPORT");

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Exchnage Rate Report </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmRealTimeExchangeRate.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
	var objGridData = '<%=gridData%>';
	 var date_format = '<%=strGCompDateFmt%>';
</script> 

</HEAD>
<BODY leftmargin="20" topmargin="10"  onload = "fnOnPageLoadRpt()">

 <html:form action="/gmExchangeRate.do?method=loadExchangeRateList">
<html:hidden property="strOpt" value="frmExchangeRate"/>

<table border="0"  height="60" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr  height="25"><td class="RightDashBoardHeader" colspan="4">&nbsp;<fmtExchangeRateReport:message key="LBL_EXCHANGE_RATE_REPORT"/></td>
		 
		     <td class="RightDashBoardHeader" align="right">
					<fmtExchangeRateReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
					height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>&nbsp;
			    </td> 
		</tr>

		<tr class="oddshade" height="28">
			 <td class="RightTableCaption"  align="right"><fmtExchangeRateReport:message key="FROM_CURRENCY"/>:&nbsp;</td>
			 <td class="RightTableCaption" colspan = "4" > <gmjsp:dropdown controlName="fromCurrency" SFFormName="frmExchangeRate" SFSeletedValue="fromCurrency" 
			     codeId="CODEID" codeName="CODENMALT" tabIndex="1" SFValue="alCurrency" defaultValue="[Choose One]" /></td>
			
		</tr>
		<tr>
			<td  height="30"class="RightTableCaption"  align="right"><fmtExchangeRateReport:message key="DATE_RANGE"/>:&nbsp;<fmtExchangeRateReport:message key="FROM_DATE"/>:&nbsp;</td>
			<td  height="30"> 
				<gmjsp:calendar  SFFormName='frmExchangeRate'   SFDtTextControlName="fromDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/></td>		
			<td class="RightTableCaption" align="right" height="30"><fmtExchangeRateReport:message key="TO_DATE"/>:&nbsp;</td>
			<td  height="30">
				<gmjsp:calendar  SFFormName='frmExchangeRate'   SFDtTextControlName="toDate" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/> &nbsp;&nbsp; 
			</td>
			<td><fmtExchangeRateReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;Load&nbsp;"gmClass="button" buttonType="Load" onClick="fnReload()" /></td>	
		</tr>
	<%
		if (gridData.indexOf("cell") != -1) {
	%> 
		<tr>
			<td colspan="5"><div  id="dataGridDiv" height="650px"></div></td></tr>
		<tr>						
			<td align="center" colspan="7"><br>
				<div class='exportlinks'><fmtExchangeRateReport:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"><fmtExchangeRateReport:message key="DIV_EXCEL"/>  </a></div>                         
      		</td>
     	</tr> 
    <%} else if (!gridData.equals("")) {%>
		<tr>
			<td colspan="10" height="1" class="LLine"></td>
		</tr>
		<tr>
			<tr>
				<td colspan="9" align="center" class="RightText"><fmtExchangeRateReport:message key="LBL_NOTHING_FOUND_TO_DISPLAY"/></td>
			</tr>
		</tr>
	<%}%> 
 
</table>
</html:form> 
</BODY>
<%@ include file="/common/GmFooter.inc" %>

</HTML>