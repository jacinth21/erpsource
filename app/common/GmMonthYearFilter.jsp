



<%@page import="java.util.Locale"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtMonthYearFilter" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmMonthYearFilter.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtMonthYearFilter:setLocale value="<%=strLocale%>"/>
<fmtMonthYearFilter:setBundle basename="properties.labels.common.GmMonthYearFilter"/>  
<%
HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
ArrayList alMonthDropDown = new ArrayList();
ArrayList alYearDropDown = new ArrayList();
 
alMonthDropDown = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALMONTHDROPDOWN"));
alYearDropDown = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALYEARDROPDOWN"));

String strToMonth = "";
String strToYear ="";
String strFrmMonth = "";
String strFrmYear = "";

strFrmMonth = GmCommonClass.parseNull((String)request.getAttribute("Cbo_FromMonth"));
strFrmYear = GmCommonClass.parseNull((String)request.getAttribute("Cbo_FromYear"));
strToMonth = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToMonth"));
strToYear = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToYear"));

String strHaction = GmCommonClass.parseNull(request.getParameter("HACTION"));

%>
<table cellSpacing=0 cellPadding=0  border=0>
<!-- Custom tag lib code modified for JBOSS migration changes -->
<tr height=30>
	<TD class="RightTableCaption" width=100 align="right"><fmtMonthYearFilter:message key="LBL_FROM_DATE"/>:</TD>
	<TD>&nbsp;<gmjsp:dropdown controlName="Cbo_FromMonth" seletedValue="<%=strFrmMonth %>" value="<%=alMonthDropDown%>" 
			codeId = "CODENMALT"  codeName = "CODENM"  tabIndex="1" />
	</TD>
	<TD>&nbsp;<gmjsp:dropdown controlName="Cbo_FromYear"  seletedValue="<%=strFrmYear%>" value="<%=alYearDropDown%>" 
			codeId = "CODENM"  codeName = "CODENM"  tabIndex="2" />
	</TD>
	<TD class="RightTableCaption">&nbsp;&nbsp;&nbsp;<fmtMonthYearFilter:message key="LBL_TO_DATE"/>:</TD>
	<TD>&nbsp;<gmjsp:dropdown controlName="Cbo_ToMonth"  seletedValue="<%=strToMonth %>" value="<%=alMonthDropDown%>" 
			codeId = "CODENMALT"  codeName = "CODENM"  tabIndex="3" /></TD>

	<TD>&nbsp;<gmjsp:dropdown controlName="Cbo_ToYear"  seletedValue="<%=strToYear%>" value="<%=alYearDropDown%>" 
			codeId = "CODENM"  codeName = "CODENM"  tabIndex="4" />
	</TD>
	<%
	String strReloadSales = "fnReloadSales('"+strHaction+"')";
	%>
	<fmtMonthYearFilter:message key="BTN_GO" var="varGo"/>
	<TD align="center" width="100"><gmjsp:button value="&nbsp;&nbsp;${varGo}&nbsp;&nbsp;" gmClass="button" buttonType="Load" tabindex="5" onClick="<%=strReloadSales%>"/>
	</TD>
</TR>
</TABLE>
