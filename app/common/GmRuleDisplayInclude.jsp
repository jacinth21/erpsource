<%
/**********************************************************************************
 * File		 		: GmRuleDisplayInclude.jsp
 * Desc		 		: To display the Rule Messages in display tag 
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>



<!--common\GmRuleDisplayInclude.jsp -->

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Iterator,com.globus.common.beans.GmCommonClass" %>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Rule.css">
<%@ taglib prefix="fmtRuleDisplay" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRuleDisplayInclude.jsp -->
<fmtRuleDisplay:setLocale value="<%=strLocale%>"/>
<fmtRuleDisplay:setBundle basename="properties.labels.custservice.ModifyOrder.GmEditShipDetails"/>
<% 

String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
int intSize = 0;

ArrayList alReturn = new ArrayList();
alReturn = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSEQUENCES"));
String strStatus = GmCommonClass.parseNull((String)request.getParameter("txnStatus"));
String strOverrideAcccess = GmCommonClass.parseNull((String)request.getAttribute("OVERRIDE_ACCESS"));
String strApplDateFmt = strGCompDateFmt;
String strDateFmt = "{0,date,"+strApplDateFmt+"}";
intSize = alReturn.size();
String strCenterClass = "aligncenter";
String strAlignLeftClass = "alignleft";

String strType = GmCommonClass.parseNull(request.getParameter("Show"));
String strSize = GmCommonClass.parseNull(request.getParameter("Fonts"));
if(strSize.equals("true"))
{
	strCenterClass = "fontsizealigncenter";
	strAlignLeftClass= "fontsizealignleft";
}
%>
<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
<body>
	 
	 <%	if (intSize>0 ){%> 
	 <%	if ( !strType.equals("true")){%> 
	<table><tr><td class="RightText" bgcolor="#eeeeee"  height="25">
	<b><fmtRuleDisplay:message key="LBL_RULE_MESSAGE"/></b>
	</td></tr>
	
	</table>
	 <%} %>	
	 	
		 <display:table name="requestScope.CONSEQUENCES"  class="its" id="currentRowObject" decorator="com.globus.common.displaytag.beans.DTRuleDisplayWrapper"> 
				<display:column property="ID" title=""  class="<%=strCenterClass %>" />
				<fmtRuleDisplay:message key="LBL_RULENAME" var="varRuleName"/>
				<display:column property="NAME" title="${varRuleName}"  class="<%=strAlignLeftClass %>"/>
				<fmtRuleDisplay:message key="LBL_PART" var="varPart"/>
				<display:column property="PNUM" title="${varPart}"  class="<%=strAlignLeftClass %>"/>
				<fmtRuleDisplay:message key="LBL_MESSAGE" var="varMessage"/>
				<display:column property="MESSAGE" title="${varMessage}" style="width:280px"  class="<%=strAlignLeftClass %>"/>
				<fmtRuleDisplay:message key="LBL_PICTURE" var="varPicture"/>
				<display:column property="PICTURE" title="${varPicture}"  class="<%=strCenterClass %>" />
				<fmtRuleDisplay:message key="LBL_HOLDTXN" var="varHoldTxn"/>
				<display:column property="HOLDTXN" title="${varHoldTxn}"  class="<%=strCenterClass %>"/>
				<fmtRuleDisplay:message key="LBL_UPDATEDBY" var="varUpdatedBy"/>
				<display:column property="UPDBY" title="${varUpdatedBy}" class="<%=strAlignLeftClass %>" />
				<fmtRuleDisplay:message key="LBL_UPDATEDDATE" var="varUpdatedDate"/>
 		 		<display:column property="UPDDATE" title="${varUpdatedDate}" class="<%=strCenterClass %>" format="<%=strDateFmt%>"/>
		</display:table>
		
		   <%	if ( !strType.equals("true") && strOverrideAcccess.equalsIgnoreCase("Y")) { %> 
			 <Table border="0" class="its" cellspacing="0" cellpadding="0">
			 	<tr> <td class="Line" colspan="2"></td> </tr>
			    <tr> <td class="RightText" bgcolor="#eeeeee"  height="25" colspan="2"> <input type="checkbox" name="OVERRIDEFL">  <b><fmtRuleDisplay:message key="LBL_OVERRIDE" /></b> </td> </tr>
			    <tr> <td class="Line" colspan="2"></td> </tr>
			    <tr> <td height="10" colspan="2"></td> </tr>
			    <tr> <td valign="top" > <b><fmtRuleDisplay:message key="LBL_OVERRIDEREASON" />: </b> </td><td> <textarea rows="3" cols="95" name="OVERRIDECOMMENTS"  onfocus="changeBgColor(this,'#AACCE8');" 
										onblur="changeBgColor(this,'#ffffff');" class="InputArea"></textarea>  </td></tr>
			 </Table>
		<%} %>	
		
	<%}  %>
</body>		