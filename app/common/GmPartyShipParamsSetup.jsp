
<%
	/**********************************************************************************
	 * File		 		: GmPartyShipParamsSetup.jsp
	 * Desc		 		: 
	 * Version	 		: 1.0
	 * author			: HReddi 
	 ************************************************************************************/
%>



<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtPartyShipParamsSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPartyShipParamsSetup.jsp -->
<fmtPartyShipParamsSetup:setLocale value="<%=strLocale%>"/>
<fmtPartyShipParamsSetup:setBundle basename="properties.labels.common.GmPartyShipParamsSetup"/>  
<bean:define id="alShipmode" name="frmPartyShipParamsSetup" property="alShipmode" type="java.util.ArrayList"> </bean:define>
<bean:define id="alAccountList" name="frmPartyShipParamsSetup" property="alAccountList" type="java.util.ArrayList"> </bean:define>
<bean:define id="strAccLbl" name="frmPartyShipParamsSetup" property="strAccLbl" type="java.lang.String"> </bean:define>
<bean:define id="screenType" name="frmPartyShipParamsSetup" property="screenType" type="java.lang.String"> </bean:define>
<bean:define id="gpofl" name="frmPartyShipParamsSetup" property="gpofl" type="java.lang.String"> </bean:define>
<bean:define id="shipMode" name="frmPartyShipParamsSetup" property="shipMode" type="java.lang.String"> </bean:define>
<bean:define id="shipCarrier" name="frmPartyShipParamsSetup" property="shipCarrier" type="java.lang.String"> </bean:define>
<%

 String strCommonJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_COMMON");
	String strAccID = GmCommonClass.parseNull((String)request.getAttribute("hAccountID"));
	String strPartyID =  GmCommonClass.parseNull((String)request.getAttribute("hPartyID"));
	String strSubmitAccess = GmCommonClass.parseNull((String)request.getAttribute("hSubmitAccess"));
	String strBtnDisable = "false";
	if(!strSubmitAccess.equals("Y")){
		strBtnDisable = "true";
	}
	
	HashMap hcboVal = null;
	int intSize = 0;
	String strSelected = "";
	String strCodeID = "";
	String strCodeNm = "";
	
	String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
	GmResourceBundleBean gmResourceBundleBean1 = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);	
	String strCompanyId = GmCommonClass.parseNull( gmResourceBundleBean1.getProperty("ENABLE_SHIPPMENT_EMAIL"));
	
	String strTRClass = "";
	String strSSTRClass= "shade"; 
		
	if(strCompanyId.equals("YES")){
	    strTRClass = "shade";
	    strSSTRClass = ""; 
	}		

%> 
<HTML>
<HEAD>
<TITLE>Globus Medical: Account Parameter Setup</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strCommonJsPath%>/GmPartyShipParamsSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var accid = '<%=strAccID%>';
var partyId = '<%=strPartyID%>';
var shipmode = '<%=shipMode%>';
var shipCarrier = '<%=shipCarrier%>';
var shipModeLength = <%=alShipmode.size()%>;
<% hcboVal = new HashMap();
for (int i=0;i<alShipmode.size();i++){
	hcboVal = (HashMap)alShipmode.get(i); %>
var AllModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>
</script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");</style>
 
<style>
table.DtTable750{	
	margin: 0 0 0 0;
	width: 775px;
	border: 1px solid  #676767;
}
</style>
 
</HEAD>
<BODY onload="fnPageLoad1();">
<FORM name="frmPartyShipParamsSetup" id="formName" method="post" action="/gmPartyShipParamsSetup.do?method=setPartyShipParam" >
<html:hidden property="strOpt" name ="frmPartyShipParamsSetup"/>
<html:hidden property="screenType" name ="frmPartyShipParamsSetup"/>
<html:hidden property="accountId" styleId="accountId" name ="frmPartyShipParamsSetup"/>
<html:hidden property="partyID" styleId="partyID" name ="frmPartyShipParamsSetup"/>
<html:hidden property="gpofl" name ="frmPartyShipParamsSetup"/>

<table border="0" class="DtTable750" cellspacing="0" cellpadding="0">	
	<tr class="Shade">
		<td colspan="6" bgcolor="#CCCCCC" height="1"></td>
	</tr>	
	 <tr class="Shade">
		<td   class="RightTableCaption" align="right" HEIGHT="24"  width="200"> &nbsp;<%=strAccLbl%>:</td>
		<td colspan="3"> &nbsp;<html:text property="thirdParty" name="frmPartyShipParamsSetup" size="20" /></td>
	</tr>
	<tr><td colspan="4" height="1" class="LLine"></td></tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr >
		<td class="RightTableCaption" align="right" HEIGHT="24"  width="200"> &nbsp;<fmtPartyShipParamsSetup:message key="LBL_DEFAULT_CARRIER"/>:</td>
		<td colspan="3" align="left">&nbsp;<gmjsp:dropdown width="200"	controlId="shipCarrier" controlName="shipCarrier" SFFormName="frmPartyShipParamsSetup" SFSeletedValue="shipCarrier" SFValue="alShipCarrier" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" onChange="javascript:fnGetShipModes(this,this.form);" /></td>
	</tr>
	<tr><td colspan="4" height="1" class="LLine"></td></tr>
	<tr class="Shade">
		<td class="RightTableCaption" align="right" HEIGHT="24"  width="200"> &nbsp;<fmtPartyShipParamsSetup:message key="LBL_DEFAULT_SHIPPING_MODE"/>:</td>
		<td colspan="3" align="left">&nbsp;<gmjsp:dropdown width="200"	controlId="shipMode" controlName="shipMode" SFFormName="frmPartyShipParamsSetup" SFSeletedValue="shipMode" SFValue="alShipmode" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"  /></td>
	</tr>
	<% if(strCompanyId.equals("YES")){ %>
	<tr>
		<td colspan="4" height="1" class="LLine"></td>
	</tr>
	<tr>
		<td class="RightTableCaption" align="right" HEIGHT="24"  width="200"> &nbsp;<fmtPartyShipParamsSetup:message key="LBL_SHIPMENT_EMAIL"/>:</td>
		<td colspan="3"> &nbsp;<html:text property="shipmentEmail" name="frmPartyShipParamsSetup" size="45" /></td>
	</tr>
	<%} %>
	<tr>
		<td colspan="4" height="1" class="LLine"></td>
	</tr>	
	<tr class="<%=strTRClass %>">
		<td class="RightTableCaption" align="right" HEIGHT="24"  width="200">&nbsp;<fmtPartyShipParamsSetup:message key="LBL_SHIPMENT_TYPE"/>:</td>
		<td class="RightTableCaption" align="left" HEIGHT="24">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <fmtPartyShipParamsSetup:message key="LBL_PAYEE"/> </td>
		<td class="RightTableCaption" align="left" HEIGHT="24">&nbsp;<fmtPartyShipParamsSetup:message key="LBL_CHARGE_TYPE"/></td>
		<td class="RightTableCaption" align="left" HEIGHT="24">&nbsp;<fmtPartyShipParamsSetup:message key="LBL_VALUE"/></td>		 
	</tr>
	<tr><td colspan="4" height="1" class="LLine"></td></tr>	
	<tr class="<%=strSSTRClass %>">	
		<td class="RightTableCaption" align="right" HEIGHT="24"  width="200">&nbsp;<fmtPartyShipParamsSetup:message key="LBL_STANDARD_SHIPMENT"/>:</td>
		<td align="left">&nbsp;<gmjsp:dropdown  controlName="stdShipPayee" SFFormName="frmPartyShipParamsSetup" SFSeletedValue="stdShipPayee" SFValue="alStdShipPayee" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" onChange="javascript:fnDisableChrgVal(this.form);"  /></td>
		<td align="left">&nbsp;<gmjsp:dropdown  controlName="stdShipChargeType" SFFormName="frmPartyShipParamsSetup" SFSeletedValue="stdShipChargeType" SFValue="alStdShipChargeType" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"  onChange="javascript:fnDisableChrgVal(this.form);" /></td>
		<td align="left"> &nbsp;<html:text property="stdShipValue" name="frmPartyShipParamsSetup" size="10" /></td>			
	</tr>	
	<tr>
		<td colspan="4" height="1" class="LLine"></td>
	</tr>	
	<tr class="<%=strTRClass %>">
		<td class="RightTableCaption" align="right" HEIGHT="24"  width="200"> &nbsp;<fmtPartyShipParamsSetup:message key="LBL_RUSH_SHIPMENT"/>:</td>
		<td align="left">&nbsp;<gmjsp:dropdown  controlName="rushShipPayee" SFFormName="frmPartyShipParamsSetup" SFSeletedValue="rushShipPayee" SFValue="alRuchShipPayee" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]" onChange="javascript:fnDisableChrgVal(this.form);"  /></td>
		<td align="left">&nbsp;<gmjsp:dropdown  controlName="rushShipChargeType" SFFormName="frmPartyShipParamsSetup" SFSeletedValue="rushShipChargeType" SFValue="alRuchShipChargeType" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"  onChange="javascript:fnDisableChrgVal(this.form);" /></td>
		<td align="left" > &nbsp;<html:text property="rushShipValue" name="frmPartyShipParamsSetup" size="10" /></td>			
	</tr>	
	<tr>
		<td colspan="4" height="1" class="LLine"></td>
	</tr>
	<tr>
		<td colspan="4"> 
			<jsp:include page="/common/GmIncludeLog.jsp" >
			<jsp:param name="FORMNAME" value="frmPartyShipParamsSetup" />
			<jsp:param name="ALNAME" value="alLogReasons" />
			<jsp:param name="LogMode" value="Edit" />
			<jsp:param name="Mandatory" value="yes" />
			</jsp:include>
		</td>
	</tr>	
	<tr>
		<td colspan="4" align="center" height="30">
		<fmtPartyShipParamsSetup:message key="BTN_SUBMIT" var="varSubmit"/>
		<%if  (screenType.equals("group")) { %>
		<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit"  gmClass="button" disabled="<%=strBtnDisable %>" onClick="fnShipSubmit(this.form,'group');" buttonType="Save" />
		<%} else {%>
		<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" name="Btn_Submit"  gmClass="button" disabled="<%=strBtnDisable %>" onClick="fnShipSubmit(this.form,'');" buttonType="Save" />
			
		<%} %>
		<%if(!screenType.equals("group")){ %>
		<fmtPartyShipParamsSetup:message key="BTN_CLOSE" var="varClose"/>
			<gmjsp:button value="&nbsp;${varClose}&nbsp;" name="Btn_Reset" gmClass="button" onClick = "parent.window.close();" buttonType="Save" />
		<%} %>
		</td>
	</tr>
</table>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
