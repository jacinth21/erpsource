<%
/**********************************************************************************
 * File		 		: GmUploadedFileInclude.jsp
 * Desc		 		: Screen will give uploaded files details
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>




<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtUploadFileInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmUploadedFileInclude.jsp -->
<fmtUploadFileInclude:setLocale value="<%=strLocale%>"/>
<fmtUploadFileInclude:setBundle basename="properties.labels.common.GmUpload"/>
<%    
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strFormName = GmCommonClass.parseNull((String)request.getParameter("FORMNAME"));
String strRefId = GmCommonClass.parseNull(request.getParameter("refID"));
%>
<bean:define id="intResultSize" name="frmPDFileUpload" property="intResultSize" type="java.lang.Integer"> </bean:define>
 
 
<HTML>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Globus Medical: Files Uploaded</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css"  href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_pgn_bricks.css">
<%-- <link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.css"> --%>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<%-- <script language="JavaScript" src="<%=strJsPath%>/common/productcatalog/GmUploadedFileInclude.js"></script> --%>

</HEAD>
<BODY>
<table  border="0" width="100%" cellspacing="0" cellpadding="0" >
	<%
	if(intResultSize >= 1) {%>
	<tr>
		<td>
			<div id="uploadedFilesGrid" class="grid"  height="200px"></div>
		</td>
	</tr>
	
	<tr>
		<td colspan="3" height="30" align="center">
			<logic:equal name="frmPDFileUpload" property="fileUploadSubmitAcc" value="Y">
			<fmtUploadFileInclude:message key="BTN_SUBMIT" var="varSubmit"/>
				<gmjsp:button value="${varSubmit}" style="width: 6em; height: 23"  name="Btn_Submit" buttonType="Save" gmClass="Button" onClick="fnSubmit(this.form);"  />
			</logic:equal>
			<logic:equal name="frmPDFileUpload" property="accessFileVoid" value="Y">
			<fmtUploadFileInclude:message key="BTN_VOID" var="varVoid"/>
			&nbsp;<gmjsp:button value="${varVoid}" style="width: 6em; height: 23" buttonType="Save" name="Btn_void" gmClass="Button" onClick="fnVoid(this.form);" />
			</logic:equal>
		</td>
	</tr>
	
	<%}else{%>
	<tr>
		<td height="25" align="center"><font color="blue"><fmtUploadFileInclude:message key="LBL_NO_FILES_UPLOADED"/></font></td>
	</tr>
	<%} %>  

</table>			
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>