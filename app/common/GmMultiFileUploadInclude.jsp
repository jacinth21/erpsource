<%/**********************************************************************************
 * File		 		: GmMultiFileUploadInclude.jsp
 * Desc		 		: This screen is used to Upload multiple files for Produact catalog doc
 * Version	 		: 1.0
 * author			: Jignesh Shah
************************************************************************************/
%>


<!--common\GmMultiFileUploadInclude.jsp -->

<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Iterator" %>
<%@ taglib prefix="fmtMultiFileUploadInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmMultiFileUploadInclude.jsp -->
<fmtMultiFileUploadInclude:setLocale value="<%=strLocale%>"/>
<fmtMultiFileUploadInclude:setBundle basename="properties.labels.common.GmMultiFileUploadInclude"/>  
<%
	String strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
%>
<bean:define id="alFileType" name="frmPDFileUpload" property="alFileType" type="java.util.ArrayList"></bean:define>
<bean:define id="alSubType" name="frmPDFileUpload" property="alSubType" type="java.util.ArrayList"></bean:define>
<bean:define id="hmFileFmt" name="frmPDFileUpload" property="hmFileFmt" type="java.util.HashMap"></bean:define>
<bean:define id="strPartValid" name="frmPDFileUpload" property="strPartValid" type="java.lang.String"> </bean:define>
<%

String strCommonProductJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_COMMON_PRODUCTCATALOG");

strFormName = strFormName == "" ? "frmPDFileUpload" : strFormName;
int fileTypelen = alFileType.size();

String strCodeId = "";
String strCodeName = "";

String strOptionString = "";
String strChooseOne = "<option value=\"0\" >[Choose One]</option>";

if(fileTypelen!=0)
{
	for (int i=0; i<fileTypelen; i++)
	{
		HashMap hmFileType = (HashMap)alFileType.get(i);
		
		strCodeId = GmCommonClass.parseNull((String)hmFileType.get("CODEID"));
		strCodeName = GmCommonClass.parseNull((String)hmFileType.get("CODENM"));
		
		strOptionString += " <option value=\""+strCodeId+"\">"+strCodeName+"</option> ";
	}
	strOptionString = strChooseOne + strOptionString;
	
}
 
%>

<html>
<head>
<TITLE> Globus Medical:Multi File Upload</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message_Operations.js"></script>
<script language="JavaScript" src="<%=strCommonProductJsPath%>/GmMultiFileUploadInclude.js"></script>
<script>

var lblArtifactType = '<fmtMultiFileUploadInclude:message key="LBL_ARTIFACE_TYPE"/>';
var lblSubType = '<fmtMultiFileUploadInclude:message key="LBL_SUB_TYPE"/>';
var lblTitle = '<fmtMultiFileUploadInclude:message key="LBL_TITLE"/>';
var lblPartNum = '<fmtMultiFileUploadInclude:message key="LBL_PART_NUM"/>';

var ImagesFormat   ="png,gif,jpg,jpeg";
var LiteratureFormat ="pdf";
var BrochureFormat  ="pdf";

var fileCount = 1;
var optionString = '<%=strOptionString%>';
var subTypeArrLen = <%=alSubType.size()%>;
var partMad='<%=strPartValid%>';
<%
HashMap hmDistVal = new HashMap();
for (int i=0;i<alSubType.size();i++)
{
	hmDistVal = (HashMap)alSubType.get(i);
%>
var	 subTypeArr<%=i%> =  "<%=hmDistVal.get("CODEID")%>,<%=hmDistVal.get("CODENM")%>,<%=hmDistVal.get("CODENMALT")%>" ;
<%
}
Iterator itrKeys = hmFileFmt.keySet().iterator();
while(itrKeys.hasNext())
{
	String strKey = (String)itrKeys.next();
	String strValue = String.valueOf(hmFileFmt.get(strKey));
%> 
var	 fileFmt<%=strKey%> = "<%=strValue%>";
var imagePath = "<%=strImagePath%>";

<%}%>
</script>
</head>
<BODY leftmargin="20" topmargin="10">

<html:hidden property="fileTypeId"/>
<html:hidden property="fileTypeList"/>
<input type="hidden" name="partNumberValid" id="partNumberValid" value=""/>

<table border="0" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">					
			<tr><td class="LLine" colspan="5"></td></tr>
				
			<tr>
				<td colspan="5" align="center" height="25">&nbsp;<font style="color: Green;"><b><bean:write name="frmPDFileUpload" property="message"/></b></font></td>
			</tr>	
			<tr class="shade">
				<td>
					<table border="0" cellspacing="0" cellpadding="0" width="100%" id="UploadFiles">
					 <TBODY>
						<tr>
							<input type="hidden" name="hfilename" />
							<input type="hidden" name="hfileId" />
							<td align="right" height="30" width="80" class="RightTableCaption"><span id="typespan" style="vertical-align: middle"><font color="red">*</font>&nbsp;<fmtMultiFileUploadInclude:message key="LBL_ARTIFACE_TYPE"/>:&nbsp;</span></td>	
							<td align="left"  height="30" width="80"><span id="fileTypeSpan"  width="70"><gmjsp:dropdown controlName="fileTypeId0" controlId="fileTypeId0" onChange="fnArtifactChange(0);" 
								SFFormName="frmPDFileUpload" SFSeletedValue="fileTypeId" SFValue="alFileType"  
								codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/></span>
							</td>
							<td align="right" height="30" width="90" class="RightTableCaption"><span id="typespan" style="vertical-align: middle"><font color="red">*</font>&nbsp;<fmtMultiFileUploadInclude:message key="LBL_SUB_TYPE"/>:&nbsp;</span></td>	
							<td align="left"  height="30" width="80"><span id="fileTypeSpan"  width="70"><gmjsp:dropdown controlName="subType0" controlId="subType0" onChange="fnSubTypeChange(0);" width="140" 
								SFFormName="frmPDFileUpload"  
								codeId="CODEID"  codeName="CODENM"  defaultValue="[Choose One]"/></span>
							</td>							
							<td align="right" height="30" width="80" class="RightTableCaption"><span id="titlespan"  style="vertical-align: middle"><font color="red">*</font>&nbsp;<fmtMultiFileUploadInclude:message key="LBL_TITLE"/>:&nbsp;</span></td>
							<td align="left"  height="30"><span id="fileTitleSpan0">
							    <input type="text" name="fileTitle0" id="fileTitle0" style="width:200px" onfocus="changeBgColor(this,'#AACCE8');" class="InputArea" onblur="changeBgColor(this,'#ffffff');" onchange="javascript:fnGetPartNumVal(this,0);" value=""/>
							    <input type="hidden" name="titleType0" id="titleType0" value=""/>
							  </span></td>
							  
							  <td align="right" height="30" width="85" class="RightTableCaption"><span id="partspan0" style="vertical-align: middle">&nbsp;<fmtMultiFileUploadInclude:message key="LBL_PART_NUM"/>:&nbsp;&nbsp;</span></td>
							  <td id="partNumId0" align="left"  height="30" width="160px"><span id="parttxtval0" ></span>
							    <input type="hidden" name="partNum0" id="partNum0"/>
							  </td>
							<td align="left"  height="30" ><span id="fileSectionSpan" style=" vertical-align: middle"><input type="file" name="theFile[0]" id="theFile[0]" value=""></span></td>
						</tr>
						</TBODY>
					</table>
				</td>
			</tr>
			<tr><td class="LLine" colspan="5"></td></tr>
			<tr>
				<td>
					<table cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td colspan="5" height="25"><span id="attachMoreFile"><a href="javascript:fnAddElementMoreFiles('UploadFiles');" id="UploadBtn"><font color="#6699FF"><fmtMultiFileUploadInclude:message key="LBL_ATTACH_MORE_FILES"/></font></a></span></td>
						</tr>
						<tr><td class="LLine" colspan="5"></td></tr>
						<tr>
							<td colspan="5" height="30" align="center">
							<logic:equal name="frmPDFileUpload" property="fileUploadSubmitAcc" value="Y">
							<fmtMultiFileUploadInclude:message key="BTN_UPLOAD_FILES" var="varUploadFiles"/>
								<gmjsp:button controlId="UploadBtn" buttonType="Save" name="UploadBtn" value="${varUploadFiles}" gmClass="Button" style="height: 22" onClick="fnFileUpload(this.form);" />
							</logic:equal>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			

	</table>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>