 <%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmForgotPass.jsp
 * Desc		 		: This screen is used for the reseting the password
 * Version	 		: 1.0 
 * author			: Dhinakaran James
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ taglib prefix="fmtForgotPass" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmForgotPass.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtForgotPass:setLocale value="<%=strLocale%>"/>
<fmtForgotPass:setBundle basename="properties.labels.common.GmForgotPass"/>  
<%
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Globus Medical: Forgot Password</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
function fnSubmit()
{
	fnValidateTxtFld('Txt_Username',message[5128]);
	fnValidateTxtFld('Txt_email',message[5129]);
	 
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	else
	{
		document.fLogon.submit();
	}
}
</script>
<STYLE type="text/css">
.portalstyle
{position: absolute; visibility: hidden; left: 750; top: 18;}
.bgimage 
{ background-image: url(/images/SpineIT-header_greybar.png); background-repeat: repeat}
</STYLE>
</head>

<body leftmargin="0" topmargin="0" onLoad=document.fLogon.Txt_Username.focus();>
<form name="fLogon" method="post" action="<%=strServletPath%>/GmLogonServlet">
<input type="hidden" name="hAction" value="SendPassword">
<table border="0" width="770" cellspacing="1" cellpadding="0" bgcolor="#CCCCCC">
  <tr>
    <td class="bgimage" width="100%" height="60" valign="top">
	   <img src="<%=strImagePath%>/GlobusOne-Portal-Logo.jpg" border="0">
    </td>
    <td class="bgimage" valign="top">
	   <img src="<%=strImagePath%>/SpineIT-header_dotsandmap.png" border="0">
	</td>
  </tr>
  <tr>
    <td bgcolor="white" height="200" valign="top"></td>
    <td bgcolor="white" height="200" align="right" valign="top"><BR>
		<table border="0" width="300" cellspacing="0" cellpadding="0">
			<tr>
				<td rowspan="7" bgcolor="#666666" width="1"></td>
				<td colspan="2" bgcolor="#666666" height="1"></td>
				<td rowspan="7" bgcolor="#666666" width="1"></td>
			</tr>
			<tr>
				<td colspan="4" class="RightDashBoardHeader" height="20" align="center"><fmtForgotPass:message key="LBL_FORGOT_PASSWORD"/></td>
			</tr>
			<tr height="25">
				<td class="TableText" align="right"><fmtForgotPass:message key="LBL_USER_NAME"/> : </td>
				<td>&nbsp;<input type="text" size="15" value="" name="Txt_Username" class="InputArea">
			</tr>
			<tr height="25">
				<td class="TableText" align="right"><fmtForgotPass:message key="LBL_EMAIL_ID"/> :</td>
				<td>&nbsp;<input type="text" size="25" value="" name="Txt_email" class="InputArea">
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td  height="30">
					<fmtForgotPass:message key="BTN_SUBMIT" var="varSubmit"/>
					<fmtForgotPass:message key="BTN_RESET" var="varReset"/>
					<input type="button" value="${varSubmit}" class="button" onClick="javascript:fnSubmit();">&nbsp;
					<input type="reset" value="${varReset}" class="button">
				</td>
			</tr>
			 
			<tr>
				<td colspan="4" height="1" colspan="4" bgcolor="#666666"></td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td bgcolor="white" align="center" height="30" class="FooterText" colspan="2">
		<fmtForgotPass:message key="LBL_ALL_RIGHTS_RESERVED"/>
	</td>
  </tr>
</table>
</form>
</body>


 
</HTML>
