<%@page import="java.util.Locale"%>
<%
/**********************************************************************************
 * File		 		: GmCommonContactSetup.jsp
 * Desc		 		: Setting up Contacts
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>



<%@ include file="/common/GmHeader.inc" %> 

<%@ page import="org.apache.commons.beanutils.BasicDynaBean"%>
<%@ page import="org.apache.commons.beanutils.RowSetDynaClass"%>
<%@ page import="java.util.Iterator"%>

 
<%@ taglib prefix="fmtCommonContactSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmCommonContactSetup.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtCommonContactSetup:setLocale value="<%=strLocale%>"/>
<fmtCommonContactSetup:setBundle basename="properties.labels.common.GmCommonContactSetup"/>  
<% 
String strWikiPath = GmCommonClass.getString("GWIKI");
String strWikiTitle = GmCommonClass.getWikiTitle("CONTACT_SETUP");
Logger log = GmLogger.getInstance(GmCommonConstants.COMMON);  // Instantiating the Logger 
try { %>



<HTML>
<HEAD>
<TITLE> Globus Medical: Contact Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
function fnFetch()
{	
	fnReset();
	document.frmCommonContact.submit();
}

function fnEditId(val)
{	
	document.frmCommonContact.hcontactID.value = val;
	document.frmCommonContact.strOpt.value = "edit";
	document.frmCommonContact.submit();
}

function fnSubmit(val)
{
fnValidateDropDn('partyId',message[5143]);
fnValidateDropDn('contactType',message[5144]);
fnValidateDropDn('preference',message[5145]);
fnValidateDropDn('contactMode',message[5146]);	
fnValidateDropDn('contactValue',message[5147]);
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmCommonContact.strOpt.value = 'save';
	document.frmCommonContact.submit();
}

function fnReset()
{ 
 document.frmCommonContact.hcontactID.value = "";
 document.frmCommonContact.contactType.value = "0";
 document.frmCommonContact.preference.value = "0";
 document.frmCommonContact.contactMode.value = "0";
 document.frmCommonContact.contactValue.value = "";
 document.frmCommonContact.inActiveFlag.checked = false;  
}
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" >
<html:form action="/gmContactInfo.do">
<html:hidden property="strOpt" value=""/>
<html:hidden property="hcontactID" name="frmCommonContact"/>
<html:hidden property="partyType" name="frmCommonContact"/>
 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtCommonContactSetup:message key="LBL_CONTACT_SETUP"/></td>
			<td align="right" class=RightDashBoardHeader > 	
			<fmtCommonContactSetup:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
				onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>			
		</tr>
		<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<logic:notEqual name="frmCommonContact" property="hideList" value="true">
					<tr>
						<td class="RightTableCaption" align="right" HEIGHT="30" width="30%"></font><font color="red">*</font>&nbsp;<fmtCommonContactSetup:message key="LBL_NAME"/>:</td>
						<td width="70%">&nbsp;
							<gmjsp:dropdown controlName="partyId" onChange="fnFetch();" SFFormName="frmCommonContact" SFSeletedValue="partyId"
							   SFValue="alNames" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" />													
						</td>
					</tr>
					<tr>
                        <td class="RightTextBlue" colspan="2" align="center"><fmtCommonContactSetup:message key="LBL_CHOOSEONE_FROM_THE_LIST"/></td> 
                    </tr>
                    <tr><td colspan="2" class="Line"></td></tr>
                    </logic:notEqual>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font><font color="red">*</font>&nbsp;<fmtCommonContactSetup:message key="LBL_CONTACT_TYPE"/>:</td> 
                        <td>&nbsp;
							<gmjsp:dropdown controlName="contactType" SFFormName="frmCommonContact" SFSeletedValue="contactType"
								SFValue="alContactTypes" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
                        </td>
                    </tr>                    
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font><font color="red">*</font>&nbsp;<fmtCommonContactSetup:message key="LBL_PREFERENCE"/>:</td>
                        <td>&nbsp;
							<gmjsp:dropdown controlName="preference" SFFormName="frmCommonContact" SFSeletedValue="preference"
								SFValue="alPreferences" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						</td>
                    </tr>                    
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr> 
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><font color="red">*</font>&nbsp;<fmtCommonContactSetup:message key="LBL_CONTACT_MODE"/>:</td>
                        <td>&nbsp;
							<gmjsp:dropdown controlName="contactMode" SFFormName="frmCommonContact" SFSeletedValue="contactMode"
								SFValue="alContactModes" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />
						</td>
                    </tr>
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtCommonContactSetup:message key="LBL_CONTACE_VALUE"/>:</td> 
                        <td>&nbsp; <html:text property="contactValue"   size="12" name="frmCommonContact" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/>
                        </td> 
                    </tr>
                    <tr><td colspan="2" bgcolor="#cccccc"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtCommonContactSetup:message key="LBL_INACTIVE_FLAG"/>:</td> 
                        <td>&nbsp;<html:checkbox property="inActiveFlag" />
                         </td>
                    </tr>
                    <tr>
                    <td colspan="2">
                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmCommonContact" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
						</jsp:include>
					</td>
                    </tr>                    
                    <tr>                        
                        <td colspan="2" align="center" height="30">&nbsp;
                        <fmtCommonContactSetup:message key="BTN_SUBMIT" var="varSubmit"/>
                        <fmtCommonContactSetup:message key="BTN_ADD_NEW" var="varAddNew"/>
                        <gmjsp:button value="${varAddNew}" gmClass="button" buttonType="Save" onClick="fnReset();" />   
                        <gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();" />                     
                        </td>
                    </tr> 
           <tr> 
            <td colspan="2">
            <display:table name="requestScope.frmCommonContact.ldtResult" class="its" id="currentRowObject"  export = "false" decorator="com.globus.displaytag.beans.GmCommonSetupWrapper" > 
			<display:column property="ID" title="" class="alignleft"/>
			<fmtCommonContactSetup:message key="LBL_TYPE" var="varType"/><display:column property="CTYPE" title="${varType}" class="alignleft"/> 
			<fmtCommonContactSetup:message key="LBL_MODE" var="varMode"/><display:column property="CMODE" title="${varMode}" sortable="true"  />
			<fmtCommonContactSetup:message key="LBL_VALUE" var="varValue"/><display:column property="CVALUE" title="${varValue}" sortable="true" class="alignleft" />
			<fmtCommonContactSetup:message key="LBL_PREFERENCE" var="varPreference"/><display:column property="PREF" title="${varPreference}" sortable="true"  class="alignleft" />
		<fmtCommonContactSetup:message key="LBL_STATUS" var="varStatus"/>	<display:column property="INACTIVEFL" title="${varStatus}" sortable="true"  class="aligncenter" />						
			</display:table> 
		</td>
    </tr> 
      				</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<% 
} 
catch (Exception e) {
e.printStackTrace();
} %>
</BODY>

</HTML>

