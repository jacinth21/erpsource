


<!--common\GmAutoCompleteInclude.jsp -->


<%@ include file="/common/GmHeader.inc" %>

<%
			//Mandatory field
  			String strControlName = GmCommonClass.parseNull(request.getParameter("CONTROL_NAME"));
       		String strMethodLoad = GmCommonClass.parseNull(request.getParameter("METHOD_LOAD"));
			String strValue = GmCommonClass.parseNull(request.getParameter("CONTROL_NM_VALUE"));
			String strId = GmCommonClass.parseNull(request.getParameter("CONTROL_ID_VALUE"));
			//Optional field
			String strOnBlur = GmCommonClass.parseNull(request.getParameter("ON_BLUR"));
			String strMinLength = GmCommonClass.parseNull(request.getParameter("MIN_LENGTH"));
			String strWidth = GmCommonClass.parseNull(request.getParameter("WIDTH"));
			String strClass = GmCommonClass.parseNull(request.getParameter("CSS_CLASS"));
			String strTabIndex = GmCommonClass.parseNull(request.getParameter("TAB_INDEX"));
			String strShowData = GmCommonClass.parseNull((String) request.getParameter("SHOW_DATA"));
			String strAutoLoad = GmCommonClass.parseNull((String) request.getParameter("AUTO_RELOAD"));
			String strParameter = GmCommonClass.parseNull((String) request.getParameter("PARAMETER"));
			String strDynamicObj = GmCommonClass.parseNull((String) request.getParameter("DYNAMIC_OBJ"));// #Cbo_InvSource
			// to set the default values
			strWidth = strWidth.equals("") ? "300" : strWidth;
			strShowData = strShowData.equals("") ? "10" : strShowData;
			strMinLength = strMinLength.equals("") ? "3" : strMinLength;
			strClass = strClass.equals("") ? "search" : strClass;
			//
			String strAutoControlId = "search"+strControlName;
			String strAutoControlTmp = "'#"+strAutoControlId+"'";
%>
<head>
<link rel="stylesheet" href="<%=strCssPath%>/jquery-ui.css">
<style>
.ui-autocomplete-loading {
    background: url(<%=strImagePath%>/process.gif) no-repeat top right;
}
</style>

<script>
var Id = "No Data";
var localkey = '';
var localJSON = {};
var parameter = JSON.stringify(<%=strParameter%>);
var dyn_obj = '<%=strDynamicObj%>';
var dyn_obj_dtls = '';
var dyn_obj_str = '';

$(document).ready(function() {
	// to set the text box style
	//$(autoControlId).addClass("ui-widget ui-widget-content ui-corner-all");
	$(<%=strAutoControlTmp%>).attr("placeholder", "Type 3 characters to search");
	$(<%=strAutoControlTmp%>).css({
		'width' : '<%=strWidth%>'
	});
	$(function() {
		var localObjLen = 0;
		$(<%=strAutoControlTmp%>).autocomplete({
			minLength : '<%=strMinLength%>',
			source : function(request, response) {
				
				if (localObjLen <= 25) {
					// to set the dynamic values to parameter
					var dynamic_val = '';
					if(dyn_obj != ''){
						parameter = '';
						// to spilt the value
						dyn_obj_dtls = dyn_obj.split("^");
						for (i=0;i< (dyn_obj_dtls.length -1) ; i ++){
							dynamic_val = $(dyn_obj_dtls[i]).val();
							parameter = parameter + dynamic_val+'|';
						}
					}
					$.ajax({
						url : "/gmAutoComplete.do?method=<%=strMethodLoad%>&strMaxCount=<%=strShowData %>&"+ fnAppendCompanyInfo()+'&strParameter='+encodeURIComponent(parameter),
						type : "POST",
						data : {
							term : request.term
						},

						dataType : "json",
						success : function(data) {
							// to validate the data and show the message
							if (!data.length) {
								var result = [ {
									label : 'No data found',
									value : response.term
								} ];
								response(result);
							} else {
								localkey = request.term;
								localJSON = data;
								response($.map(data, function(el) {
									var val = el.NAME;
									Id = el.ID;
									return {
										label : val,
										value : val,
										id : Id
									};
								}));
							}
						}
					});
				} else {
					response($.map(localJSON, function(el) {
						var val = el.NAME;
						Id = el.ID;
						return {
							label : val,
							value : val,
							id : Id
						};
					}));
				}// end if
			}
		});

	});
	$(<%=strAutoControlTmp%>).on('autocompleteselect', function(e, ui) {
		$(<%="'#"+strControlName+"'"%>).val(ui.item.id);
		$(<%=strAutoControlTmp%>).val(ui.item.value);
		<%=strAutoLoad%>
	});
	// when remove the values then reset the auto control values
	$(<%=strAutoControlTmp%>).on('autocompletechange', function(e, ui) {
		if(ui.item == undefined){
			$(<%="'#"+strControlName+"'"%>).val('');
		}
		// When remove the values call the on change function (same as choose one selection)
		if(TRIM(document.getElementById("<%=strAutoControlId%>").value) ==''){
			<%=strAutoLoad%>
		}
	});
	// to fix the auto dropdown list width
	$.extend($.ui.autocomplete.prototype.options, {
		open: function(event, ui) {
			$(this).autocomplete("widget").css({
	            "width": ($(this).width() + "px")
	        });
	    }
	});
});

</script>

</head>

<html:hidden property="strSearchKey" value="" />
<% if(!strControlName.equals("") && !strMethodLoad.equals("")){  %>
<div class="search-container">
	<div class="ui-widget">
		<input type="text" id="<%=strAutoControlId%>" name="<%=strAutoControlId%>" value="<%=strValue %>" onblur="<%=strOnBlur%>" tabindex="<%=strTabIndex%>"/><input type="hidden" id="<%=strControlName%>" name="<%=strControlName%>" value="<%=strId%>" />
	</div>
</div>
<%} %>