



<%@ page import ="java.util.HashMap"%>
<%@ page import ="java.util.ArrayList"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtUpdationHistory" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmUpdationHistory.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtUpdationHistory:setLocale value="<%=strLocale%>"/>
<fmtUpdationHistory:setBundle basename="properties.labels.common.GmUpdationHistory"/> 

<%	/*********************************
	 * Reason for Modification
	 *********************************/ 
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	HashMap hmUpdHistory = new HashMap();
	hmUpdHistory =	GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("UPDHISTORY"));
	String strApplDateFmt = (String)session.getAttribute("strSessApplDateFmt");
	String strLogCaption = "";
		// To get the user Log information 
	String strCDate = "";
	String strCUser = "";
	String strLDate = "";
	String strLUser = "";
	
	if (!hmUpdHistory.isEmpty()	)
	{
		strCUser = GmCommonClass.parseNull((String)hmUpdHistory.get("CREATEDBY"));
		strCDate = GmCommonClass.getStringFromDate((java.sql.Date)hmUpdHistory.get("CREATEDDATE"),strApplDateFmt);
		strLUser = GmCommonClass.parseNull((String)hmUpdHistory.get("LASTUPDATEDBY"));
		strLDate = GmCommonClass.getStringFromDate((java.sql.Date)hmUpdHistory.get("LASTUPDATEDDATE"),strApplDateFmt);
	}
	
%>	

<HTML>
<HEAD>
<TITLE> Globus Medical: Updation History</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">	

<table border=0 Width="100%" cellspacing=0 cellpadding=0>
	<tr class="ShadeRightTableCaption">
		<td Height="24" colspan="4">&nbsp;<fmtUpdationHistory:message key="LBL_UPDATE_HISTORY"/></td>
	</tr>
	<tr><td height="1" colspan="4" bgcolor="#666666"></td></tr>		
<%	
	if (!hmUpdHistory.isEmpty()	)
	{
%>	
	<tr bgcolor="#EEEEEE" class="RightTableCaption">
		<td width="150" height="25"><fmtUpdationHistory:message key="LBL_CREATED_DATE"/></td>
		<td width="150"><fmtUpdationHistory:message key="LBL_CREATED_BY"/> </td>
		<td width="150"><fmtUpdationHistory:message key="LBL_UPDATEDDATE"/></td>
		<td width="150"><fmtUpdationHistory:message key="LBL_UPDATEDBY"/></td>
	</tr>
	<TR><TD colspan=4 height=1 class=Line></TD></TR>
	<tr >
		<td width="150" class="RightText">&nbsp;<%=strCDate%></td>
		<td width="150" class="RightText" height="20">&nbsp;<%=strCUser%></td>
		<td width="150" class="RightText" height="20">&nbsp;<%=strLDate%></td>
		<td width="150" class="RightText"><%=strLUser%></td>
	</tr>
	<tr><td colspan="4" height="25" align="center" class="RightTextBlue">
<%	
	}else{
%>
	<tr >
		<td height="40" align="center" colspan="4" class="RightText">&nbsp;<fmtUpdationHistory:message key="LBL_NODATAAVAILABLE"/></td>
	</tr>
<%	
	}
%>
	<TR><TD colspan=4 height=1 class=Line></TD></TR>
</table>

</HTML>