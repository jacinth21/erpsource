<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmAuditTrailDetail.jsp
 * Desc		 		: display required date history info
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>




<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
 <%@ taglib prefix="fmtAuditTrailDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmAuditTrailDetail.jsp -->
<fmtAuditTrailDetail:setLocale value="<%=strLocale%>"/>
<fmtAuditTrailDetail:setBundle basename="properties.labels.common.GmAuditTrailDetail"/>
<bean:define id="ldtResult" name="frmAuditTrail" property="ldtResult" type="java.util.List"></bean:define>
<bean:define id="auditId" name="frmAuditTrail" property="auditId" type="java.lang.String"></bean:define>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<%
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.common.GmAuditTrailDetail", strSessCompanyLocale);
String value=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VALUE"));
String transId=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TRANSACTION_ID"));
String strScreenHeader = "";
String strAlign ="alignleft";


boolean exportValue=false;
if(auditId.equals("1035")){
	value=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_LOCK"));
	transId=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_USER_NAME"));
	exportValue=true;
	strScreenHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_USER_LOCK_HISTORY"));
	strAlign ="aligncenter";
}
if(auditId.equals("1036")){
	value=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_CHANGE_PASSWORD"));
	transId=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_USER_NAME"));
	exportValue=true;
	strScreenHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_PASSWORD_CHANGE_HISTORY"));
	strAlign ="aligncenter";
}
%>
 

</HEAD>

<BODY leftmargin="20" topmargin="10">
 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	
	  <% if(!strScreenHeader.equals("")) { %>
		<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader"><%=strScreenHeader%> </td>
		</tr>
	 <% } %>	
	 
		 <tr>
	                     <td colspan="3">
	                     	<display:table export="<%=exportValue%>" name="requestScope.frmAuditTrail.ldtResult" class="its" id="currentRowObject" requestURI="/gmAuditTrail.do"  decorator="com.globus.common.displaytag.beans.DTAuditDisplayWrapper"  > 
							<display:column property="REQID" title="<%=transId%>"    class="alignleft"/>
						    <display:column property="RVALUE" title="<%=value%>"    class="<%=strAlign %>"/>
							<fmtAuditTrailDetail:message key="LBL_UPDATED_DATE" var="VarUpdatedDate"/><display:column property="UPDATE_DATE" title="${VarUpdatedDate}" class="alignleft"/>							 
							<fmtAuditTrailDetail:message key="LBL_UPDATED_BY" var="varUpdatedBy"/><display:column property="UPDATEDBY" title="${varUpdatedBy}"   class="alignleft"/> 
							</display:table>

    		             </td>
                    </tr> 
        
		 
    </table>		     	
 
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

