

<!--common\GmQuestions.jsp -->



<%@page import="java.util.Locale"%>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %> 
<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>
<%@ taglib prefix="fmtQuestion" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmQuestions.jsp -->
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtQuestion:setLocale value="<%=strLocale%>"/>
<fmtQuestion:setBundle basename="properties.labels.common.GmQuestions"/>  

<%	
 
 
	
	HashMap hmQuestion = new HashMap();
	HashMap hmAnswer = new HashMap();
	HashMap hmAnsGroup = new HashMap();
	HashMap hmQuestionAnswer = new HashMap();
	
	ArrayList alAnswer = new ArrayList();
	
	ArrayList alQuestion = new ArrayList();
	ArrayList alAnswerGroup = new ArrayList();
	ArrayList alQuestionAnswer = new ArrayList();
	
	int intQuestion = 0;
	int intAnserGroup = 0; 
	
	String strAnswerGrpId = "";
	String strBRValue = "";
	String strAnsQuestionID = "";
	String strAnsGrpNm = "";
	String strControl = "";
	String strAnswerGrpTemp = "";
	String strOptionNm = "";
	String strOptionId = ""; 
	String strAnswerGrpIds ="";
	
	String strAnsDs ="";
	String strListID ="";
	
	String strListPt1 = "<select type=select class=RightText id='' name=";
	//String strListPt2 = "><option value=0>Choose One</option>";
	String strListPt2 = ">";
	String strListPt3 = "</select>";
	String strTextPt1 = "<input type=text class=RightText size=80 maxlength=200 name=";
	String strTextPt2 = "<input type=text readonly='true' class=RightText name=";
	
	String strControlType = "";
	int intQuesSize = 0;
	int intAnsSize =0;
	int intAnsGrpSize =0;
	 
	 
	String strFormName = ""; 
	 
	
	String strQuestionNM = "";
	String strQuestionID = ""; 
	String strArrayListQuesName = "";
	String strArrayListAnsName = "";
	String strArrayListAnsGrpName = "";
	String strArrayListQuesAnsName ="";
		
	// Check if the page is in struts
	strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
	
	if(!strFormName.equals(""))
	{
	
		strArrayListQuesName = GmCommonClass.parseNull(request.getParameter("ALQUESTION"));
		strArrayListAnsName = GmCommonClass.parseNull(request.getParameter("ALANSWER"));
		strArrayListAnsGrpName = GmCommonClass.parseNull(request.getParameter("ALANSWERGROUP"));
		strArrayListQuesAnsName = GmCommonClass.parseNull(request.getParameter("ALQUESTIONANSWER"));
		Object objTemp = pageContext.getRequest().getAttribute(strFormName);
		alQuestion  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListQuesName));
		alAnswer  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListAnsName));
		alAnswerGroup  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListAnsGrpName));
		alQuestionAnswer  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListQuesAnsName));
		System.out.println("alQuestion.size() = " + alQuestion.size());
		System.out.println("alAnswer.size() = " + alAnswer.size());
		System.out.println("alAnswerGroup.size() = " + alAnswerGroup.size());
		System.out.println("alQuestionAnswer.size() = " + alQuestionAnswer.size());
	}
	System.out.println(" FormName " + strFormName);  
	String strMode = GmCommonClass.parseNull(request.getParameter("Mode"));
	 
	  
	
%>	

 
<table border=0 Width="100%" cellspacing=0 cellpadding=0>
	<tr class="ShadeRightTableCaption">
		<td Height="24" colspan="3">&nbsp;<fmtQuestion:message key="LBL_QUESTIONS"/></td>
	</tr>
<%
	if 	(!strMode.equals(""))
	{	
%>	
	<tr><td height="1" colspan="3" bgcolor="#666666"></td></tr>		
	<tr>
		<td colspan="3" height="50" >&nbsp; </td>
	</tr>
<%
	}
%>	
	<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
	 
<%	

 	if (alQuestion != null)
	{
		intQuesSize = alQuestion.size();
		
	}
	
	if (intQuesSize > 0)
	{   
		intAnsSize = alAnswer.size();
		intAnsGrpSize = alAnswerGroup.size(); 
		
		for (int i=0;i<intQuesSize;i++)
		{
			hmQuestion = (HashMap)alQuestion.get(i);
			strQuestionNM = (String)hmQuestion.get("QUESTIONNAME");
			strQuestionID = (String)hmQuestion.get("QUESTIONID");

		 	
			strAnswerGrpId = ""; 
			String strShade = "Shade";
			if(i%2 == 0)
			{
				strShade = "";
			}

			
%>			<tr class="<%=strShade %>">
				<td  colspan="3"  class="RightText" height="22"   valign="top">&nbsp;<b><%=strQuestionID%>)</b>&nbsp; 
				 <b><%=strQuestionNM%></b> </td>
			</tr>	
			<tr class="<%=strShade %>"><td colspan="3" height="24" class="RightText">
				<span class="RightTextBlue">&nbsp;&nbsp;<b>A.</b></span>
<%	
			//Below code loops thru for each answers
			
			strBRValue = "";
			for (int j=0;j<intAnsSize;j++)
			{
				hmAnswer = (HashMap)alAnswer.get(j);
				strAnsQuestionID = (String)hmAnswer.get("QUESTIONID");
				strAnsGrpNm = (String)hmAnswer.get("GROUPNAME");
				strAnswerGrpId = (String)hmAnswer.get("GROUPID");
				strControlType = (String)hmAnswer.get("CONTROLTYPE");
				strAnswerGrpIds = "hAnsGrpId".concat(strAnswerGrpId); 
				
				if (alQuestionAnswer.size()> 0)
				{
					hmQuestionAnswer = (HashMap)alQuestionAnswer.get(j);
					strListID = (String)hmQuestionAnswer.get("LISTID");
					strAnsDs = (String)hmQuestionAnswer.get("ANSWERDESC");
				}
				if (strAnsQuestionID.equals(strQuestionID))
				{
 
				// If the question is not associated with with combo box then will display below val
					if ( strAnsGrpNm.equals(""))
					{	 
						  if (strControlType.equals("6101"))
						{
							strControl = strTextPt1.concat(strAnswerGrpIds).concat(" id=").concat(""+strAnsQuestionID).concat(" value='"+strAnsDs).concat("'>");
							out.println( strBRValue +  strControl + "&nbsp;&nbsp;" ); 
							out.println( "<input type=hidden  name=hQuestionID" + strAnswerGrpId + " value = \"" + strAnsQuestionID + "\" >" );
						} 
					}
					else{
						strControl = "";
						intAnsGrpSize = alAnswerGroup.size();
						hmAnsGroup = new HashMap(); 
						for (int k=0;k<intAnsGrpSize;k++)
						{
							hmAnsGroup = (HashMap)alAnswerGroup.get(k);
							strAnswerGrpTemp = (String)hmAnsGroup.get("GROUPNAME");
							
							if (strAnswerGrpTemp.equals(strAnsGrpNm))
							{
								strOptionNm = (String)hmAnsGroup.get("LISTNAME");
								strOptionId = (String)hmAnsGroup.get("LISTID");
						//		strAnswerValue = (String)hcboVal.get("C605_ANSWER_VALUE");
								
								 
						 		strOptionId = strOptionId.equals(strListID)?strOptionId.concat(" selected"):strOptionId;
								
								strOptionId = "<option value=".concat(strOptionId).concat(">");
								strControl = strControl.concat(strOptionId).concat(strOptionNm).concat("</option>");
							}
						}	
						
					 	
						strControl = strListPt1.concat(strAnswerGrpIds).concat(strListPt2).concat(strControl).concat(strListPt3); 

						out.println( strBRValue + strControl + "&nbsp;&nbsp;") ; 
						  
						out.println( "<input type=hidden name=hQuestionID" + strAnswerGrpId + " value = \"" + strAnsQuestionID + "\" >" );
						 
					}

				}
%>				  
				
<%			//	strBRValue = "</td></tr><tr class='"+strShade+"'><td height=24 class=RightText >&nbsp;&nbsp;&nbsp;&nbsp;";
			} // end of Inner FOR LOOP Answer Loop 
%>							
		 
			<BR></td></tr>
			<tr><td colspan="3" height="1" class="Line"></td></tr>
<%		} // END of Outer FOR Loop
%>		 

<%  		}
	else {
%>		<tr><td colspan="3" height="25" align="center" class="RightTextBlue">
		<BR><fmtQuestion:message key="LBL_NO_DATA_AVAILABLE"/></td></tr>
<%		}

 %>
<TR><TD colspan=3 height=1 class=Line></TD></TR>
</table>
