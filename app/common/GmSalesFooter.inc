// PMT-53328 - Historical Sales Reports - Freeze header
<script language="JavaScript"  src="<%=strJsPath%>/common/jquery.freezeheader.js"></script>
<script>
$(document).ready(function(){
  $("#myHighlightTable").freezeHeader();
});
</script>  