 <%
/**********************************************************************************
 * File		 		: GmConsequenceBuilder.jsp
 * Desc		 		: for preparing the consequences dynamically.
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>




<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Iterator" %>
<%@ taglib prefix="fmtConsequenceBuilder" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmConsequenceBuilde.jsp -->
<fmtConsequenceBuilder:setLocale value="<%=strLocale%>"/>
<fmtConsequenceBuilder:setBundle basename="properties.labels.common.GmConsequenceBuilde"/>  
<%
Object bean = pageContext.getAttribute("frmRuleCondition", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<bean:define id="ruleConsequenceId" name="frmRuleCondition"  property="ruleConsequenceId" type="java.lang.String"></bean:define>
<bean:define id="strOpt" name="frmRuleCondition"  property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="ruleId" name="frmRuleCondition"  property="ruleId" type="java.lang.String"></bean:define>
<bean:define id="consequenceId" name="frmRuleCondition"  property="consequenceId" type="java.lang.String"></bean:define>
<bean:define id="consequenceGrp" name="frmRuleCondition"  property="consequenceGrp" type="java.lang.String"></bean:define>
<bean:define id="alConsequenceList" name="frmRuleCondition"  property="alConsequenceList" type="java.util.ArrayList"></bean:define>
<bean:define id="alDbConsequenceList" name="frmRuleCondition"  property="alDbConsequenceList" type="java.util.ArrayList"></bean:define>
<bean:define id="emailId" name="frmRuleCondition"  property="emailId" type="java.lang.String"></bean:define>

<BODY>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<html:hidden property="consequenceId" value="<%=consequenceId%>"/>
<html:hidden property="consequenceGrp" value="<%=consequenceGrp %>"/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VCNSQ"/>
 
<table style="width:100%;" cellspacing="0" cellpadding="0">
	
	<tr><td colspan="2" class="Line" height="1"></td></tr>
	<%
	//HashMap hmCodeList = GmCommonClass.parseNullHashMap((HashMap)(request.getAttribute("hmCodeList")));
		
	 
	HashMap hmField = null;
	HashMap hmFieldValue = null;
	String strRuleSeqId = "";
	String strParamId = "";
	String strConsValue = "";
	String styleClass = "";
	String strActvChk = "";
	String strDbActvChk = "";
	String strDisabled = "disabled";
	String strHoldTxnHistoryfl = "";
	int dbConsListSize = alDbConsequenceList.size();
	int i=0;
	for (Iterator iter = alConsequenceList.iterator(); iter.hasNext();)
	{
		hmField = (HashMap)iter.next();
		String fieldId = (String)hmField.get("CODEID");
		String fieldLabel = (String)hmField.get("CODENM");
		String fieldType = (String)hmField.get("CONTROLTYP");
		String fieldAttr = (String)hmField.get("CODENMALT");
		if(dbConsListSize > 0)
		{
			for(int j=0; j<dbConsListSize;j++)
			{
				hmFieldValue = (HashMap)alDbConsequenceList.get(j);
				strRuleSeqId = (String)hmFieldValue.get("RULESEQID");
				strParamId = (String)hmFieldValue.get("PARAMID"); 
				strHoldTxnHistoryfl = (String)hmFieldValue.get("HLD_TXN_HISTORY_FL");
				if(fieldId.equals(strParamId))
				{
					strConsValue = (String)hmFieldValue.get("CONSVALUE");
					strDisabled = "";
					break;
				}
				
			}
			
		}
		
		String strName = "label" + i;
		String strHName = "hlabel" + i;
		if(strConsValue.equals("Y"))
		{
			strActvChk = "checked";
		}		
	%>
	<tr height="24" class="<%=styleClass%>">
		<td class="RightTableCaption" align="right" width="30%"><%=fieldLabel%>&nbsp;:&nbsp;</td>
		<td align="left" width="70%">
		<input type="hidden" id="<%=strHName%>" name="<%=strHName%>" value="<%=fieldId%>"/>		
	<% 
		if(fieldType.equals("TXTARA")) 
		{
			
	%>
			<textarea rows="5" cols="60" name="<%=strName%>"  onfocus="changeBgColor(this,'#AACCE8');" 
			onblur="changeBgColor(this,'#ffffff');" onchange="fnEnableCheckbox();" class="InputArea"><%=strConsValue%></textarea>
	<% 
		}
		else if(fieldType.equals("CHKBOX")) 
		{
			
	%>
			<input type="checkbox" name="<%=strName%>" <%=strActvChk %>  />&nbsp; 
			<%  if ( strHoldTxnHistoryfl.equals("Y") ) { %>
			<fmtConsequenceBuilder:message key="LBL_CLICK_TO_OPEN_REQUIRED" var="varClickToOpenReq"/>
									<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1016','<%=ruleId %>');" title="${varClickToOpenReq}"  
									src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=18 width=18 />&nbsp;	 
								<% } %>	
	<% 
		}
		else if(fieldType.equals("TXTBOX")) 
		{
			if(fieldId.equals("91365") && strConsValue.equals("")) //To email
			{
	%>
				<input type="text" name="<%=strName%>" value="<%=emailId %>" onchange="fnValidateEmail();" size="60"/>
	<%			
			}
			else
			{
	%>
			<input type="text" name="<%=strName%>" value="<%=strConsValue %>" onchange="fnValidateEmail();" size="60"/>
	<% 
			}
		}	
	%>
		</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<% 
		i++;
	} // End of for loop
	%>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr>
		<td colspan="2" align="center" HEIGHT="25">&nbsp;
		<fmtConsequenceBuilder:message key="BTN_SUBMIT" var="varSubmit"/>
		<fmtConsequenceBuilder:message key="BTN_VOID" var="varVoid"/>
			 <logic:equal name="frmRuleCondition" property="enableCreateRule" value="true">	  
			<gmjsp:button value="&nbsp;&nbsp;${varSubmit}&nbsp;&nbsp;&nbsp;" gmClass="button" buttonType="Save" onClick="fnSubmitConsequence(this.form);" />
			</logic:equal>			
			<logic:equal name="frmRuleCondition" property="enableCreateRule" value="false">	
			<gmjsp:button value="&nbsp;&nbsp;${varSubmit}&nbsp;&nbsp;&nbsp;"  disabled="true" buttonType="Save" gmClass="button" onClick="fnSubmitConsequence(this.form);" />
			</logic:equal>		
			 <logic:equal name="frmRuleCondition" property="enableVoidRule" value="true">				
			<gmjsp:button value="&nbsp;&nbsp;${varVoid}&nbsp;&nbsp;&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoidConsequence(this.form);"/>
			</logic:equal>	
			 <logic:equal name="frmRuleCondition" property="enableVoidRule" value="false">				
			<gmjsp:button value="&nbsp;&nbsp;${varVoid}&nbsp;&nbsp;&nbsp;" disabled="true" buttonType="Save" gmClass="button"  onClick="fnVoidConsequence(this.form);"/>
			</logic:equal>
		</td>
	</tr>
	<% 
		if	(!strConsValue.equals("")&&!strOpt.equals("load")&&!strOpt.equals("edit")){
	%>
	<tr><td colspan="2"  ></td></tr>
	<tr>
		<td colspan="2" align="center" HEIGHT="25">&nbsp;
			<fmtConsequenceBuilder:message key="LBL_MESSAGE_SAVED_SUCESSFULLY"/>									
			 
		</td>
	</tr>
	<%  
	}  
	%>
</table>
<html:hidden property="ruleConsequenceId" value="<%=strRuleSeqId %>"/>
</body>