
<%
	/**********************************************************************************
	 * File		 		: GmConsequenceFileUpload.jsp
	 * Desc		 		: Upload - Upload page
	 * Version	 		: 1.0
	 * author			: Ritesh shah
	 ************************************************************************************/
%>



<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ taglib prefix="fmtConsequenceFileUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmConsequenceFileUpload.jsp -->
<fmtConsequenceFileUpload:setLocale value="<%=strLocale%>"/>
<fmtConsequenceFileUpload:setBundle basename="properties.labels.common.GmConsequenceFileUpload"/> 
<bean:define id="strOpt" name="frmRuleCondition"  property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="ruleConsequenceId" name="frmRuleCondition"  property="ruleConsequenceId" type="java.lang.String"></bean:define>
<bean:define id="strAction" name="frmRuleCondition"  property="strAction" type="java.lang.String"></bean:define>
<bean:define id="ruleId" name="frmRuleCondition"  property="ruleId" type="java.lang.String"></bean:define>
<bean:define id="consequenceId" name="frmRuleCondition"  property="consequenceId" type="java.lang.String"></bean:define>
<bean:define id="consequenceGrp" name="frmRuleCondition"  property="consequenceGrp" type="java.lang.String"></bean:define>
<bean:define id="alConsequenceList" name="frmRuleCondition"  property="alConsequenceList" type="java.util.ArrayList"></bean:define>
<bean:define id="alDbConsequenceList" name="frmRuleCondition"  property="alDbConsequenceList" type="java.util.ArrayList"></bean:define>
<bean:define id="alSelectedConsList" name="frmRuleCondition"  property="alSelectedConsList" type="java.util.ArrayList"></bean:define>
<bean:define id="editFlag" name="frmRuleCondition"  property="editFlag" type="java.lang.String"></bean:define>


<html>
<%
String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
    //The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
	
  try
	{
    int intSize =0;
	intSize = alDbConsequenceList.size();
 
%>
<head>
<title>Upload File</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
</head>
<BODY>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusSales.css">
<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
var companyInfoObj = '<%=strCompanyInfo%>';
function upload()
  {
  	var inputStr = '';
  	inputStr = getConsequenceString();
  	var strOpt = document.upform.strOpt.value;
  	var ruleId = document.upform.ruleId.value;
  	var ruleConsequenceId = document.upform.ruleConsequenceId.value;
  	var consequenceStr = inputStr;
  	var consGrpId  = document.upform.consequenceId.value;
  	var consequenceGrp = document.upform.consequenceGrp.value;
  	document.upform.fileNm.value = document.upform.uploadfile.value;
  	document.upform.inputStr.value = inputStr;
  	
  	var editFl =  document.upform.editFlag.value;
  	 
    document.upform.hAction.value='upload';
    document.upform.strOpt.value = 'save';
    if (inputStr == '')
    {
    	alert(message[5130]);      
    	return false;
    }
  	if (strOpt != 'edit')
  	{
	    if(document.upform.uploadfile.value == "")
	    { 	
		      alert(message[5131]);      
		      return false;
	    } 
    }   
    else if (editFl=='1')
    {
     document.upform.action = "/gmRuleConsequence.do?companyInfo="+companyInfoObj+"&ruleId="+ruleId+"&ruleConsequenceId="+ruleConsequenceId+"&consequenceStr="+consequenceStr+"&strOpt=save&consequenceId="+consGrpId+"&consequenceGrp="+consequenceGrp;
    }
    
    document.upform.submit();
   
    toggledisplay();
  }
  
function getConsequenceString()
{
	var inputString = '';
	var nm = '';
	var objId = '';
	var objVal = '';
	var cnt = 0;
	var form = document.upform;
	for(var i=0; i<form.elements.length; i++)
	{
		if((form.elements[i].name.indexOf('label') != -1 &&  form.elements[i].name.indexOf('hlabel') == -1) || form.elements[i].name == 'uploadfile') 
		{
		 	 
			if (form.elements[i].name == 'uploadfile')
			{	 
				nm = 'h'+form.elements[i].name;
				objId = document.getElementById(nm).value;
				objVal = form.elements[i].value;
		 	 
				if(objVal == '')
				{
					cnt = cnt + 1;
				}				
			}
			else
			{	 
				nm = 'h' + form.elements[i].name;
				objId = document.getElementById(nm).value;
				objVal = escape(form.elements[i].value);
	 			
				if(objVal == '')
				{
					cnt = cnt + 1;
				}				
			}
			inputString += objId + ';' + objVal + '|';			
		}
	}
 
	if (cnt > 0)
	{
		inputString = '';
	}
	return inputString;
}

function fnEdtSeq(seq, ruleId)
{
//	document.upform.hAction.value = '';
//	document.upform.ruleConsequenceId.value = seq;
//	document.upform.strAction.value = 'reload';
//	document.upform.strOpt.value = 'edit';
//	document.upform.editFlag.value = '1';
	var consGrpId  = document.upform.consequenceId.value;
  	var consequenceGrp = document.upform.consequenceGrp.value;
   
	document.upform.action = "/gmRuleConsequence.do?companyInfo="+companyInfoObj+"&ruleId="+ruleId+"&ruleConsequenceId="+seq+"&strOpt=edit&strAction=reload&editFlag=1&consequenceId="+consGrpId+"&consequenceGrp="+consequenceGrp;
  	
	document.upform.submit();
	
}
function fnVoidPicConsequence(form)
{
	var ruleConsId = form.ruleConsequenceId.value;
	if(ruleConsId == '')
	{
		alert(message[5132]);
		return;
	}
	document.upform.action="/GmCommonCancelServlet?hTxnId="+ruleConsId+"&hCancelType=VCNSQ&hTxnName= Picture Consequence";
	document.upform.submit();
	
}
</script>
<form method="post" name="upform" enctype="multipart/form-data" action="/GmConsUploadServlet?companyInfo=<%= strCompanyInfo%>">

<input type="hidden" name="hAction">
<input type="hidden" name="strAction" value = "<%=strAction %>">
<input type="hidden" name="strOpt" value = "<%=strOpt %>">
<input type="hidden" name="ruleId" value = "<%=ruleId %>">
<input type="hidden" name="consequenceId" value = "<%=consequenceId %>">
<input type="hidden" name="consequenceGrp" value = "<%=consequenceGrp %>">
<input type="hidden" name="fileNm">
<input type="hidden" name="inputStr">
<input type="hidden" name="ruleConsequenceId" value="<%=ruleConsequenceId %>">
<input type="hidden" name="editFlag" value = "<%=editFlag %>">

<table border="0" class="DtTable698" cellspacing="0" cellpadding="0">
	<tr height="24"></tr>
	<%
	//HashMap hmCodeList = GmCommonClass.parseNullHashMap((HashMap)(request.getAttribute("hmCodeList")));
		
	HashMap hmField = null;
	HashMap hmFieldValue = null;
	String strRuleSeqId = "";
	String strParamId = "";
	String strConsValue = "";
	String strName = "";
	String strHName = "";
	String strDisabled = "";
	String strFileName = "";
	int selectedConsListSize = alSelectedConsList.size();
	int i=0;
	for (Iterator iter = alConsequenceList.iterator(); iter.hasNext();)
	{
		hmField = (HashMap)iter.next();
		String fieldId = (String)hmField.get("CODEID");
		String fieldLabel = (String)hmField.get("CODENM");
		String fieldType = (String)hmField.get("CONTROLTYP");
		String fieldAttr = (String)hmField.get("CODENMALT");
		if(fieldType.equals("FILE"))
		{			
			strHName = "huploadfile";
		}else
		{
			strName = "label" + i;
			strHName = "hlabel" + i;
		}
		if(selectedConsListSize > 0)
		{
			for(int j=0; j<selectedConsListSize;j++)
			{
				hmFieldValue = (HashMap)alSelectedConsList.get(j);
				strRuleSeqId = (String)hmFieldValue.get("RULESEQID");
				strParamId = (String)hmFieldValue.get("PARAMID");
				strFileName = (String)hmFieldValue.get("FILENM");
				if(fieldId.equals(strParamId))
				{
					strConsValue = (String)hmFieldValue.get("CONSVALUE");
					strDisabled = "disabled";
					break;
				}
				
			}			
		}
	%>
	<tr height="24">
		<td class="RightTableCaption" align="right" width="30%"><%=fieldLabel%>&nbsp;:&nbsp;</td>
		<td align="left" width="70%">
		<input type="hidden" id="<%=strHName%>" name="<%=strHName%>" value="<%=fieldId%>"/>
	<% 
		if(fieldType.equals("TXTARA")) 
		{			
	%>
			<textarea rows="5" cols="60" name="<%=strName%>"  onfocus="changeBgColor(this,'#AACCE8');" 
			onblur="changeBgColor(this,'#ffffff');" class="InputArea"><%=strConsValue%></textarea>
	<% 
		}
		else if(fieldType.equals("FILE")) 
		{	
			if(strOpt.equals("edit")&& intSize > 0&&!editFlag.equals(""))
			{	
	%>
			<%=strFileName%> 
			 <input type="hidden" name="uploadfile" value="<%=strFileName%>"   />
	<%			
			}
		 	if(strAction.equals("upload")&&editFlag.equals("")||!strOpt.equals("edit"))
		//	else 
			{
	%>
				<input type="file" name="uploadfile" value="<%=strFileName%>" size="50"   />
	<% 		
			}
		}	
	%>
		</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<% 
		i++;
	} // End of for loop
	%>
	<tr id="pgbar" style="display: none">
		<td colspan=2 align="center"><img
			src="<%=strImagePath%>/progress_bar.gif"></td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<tr>
		<td colspan="2" align="center" HEIGHT="25">&nbsp;	
		<fmtConsequenceFileUpload:message key="BTN_SUBMIT" var="varSubmit"/>
		<fmtConsequenceFileUpload:message key="BTN_VOID" var="varVoid"/>	 
			 <logic:equal name="frmRuleCondition" property="enableCreateRule" value="true">	 
			<gmjsp:button value="&nbsp;&nbsp;${varSubmit}&nbsp;&nbsp;&nbsp;" gmClass="button" buttonType="Save" onClick="upload();" />	
			</logic:equal>			
			<logic:equal name="frmRuleCondition" property="enableCreateRule" value="false">	
			<gmjsp:button value="&nbsp;&nbsp;${varSubmit}Submit&nbsp;&nbsp;&nbsp;" disabled="true" buttonType="Save" gmClass="button"  onClick="upload();" />	
			</logic:equal>		
			 <logic:equal name="frmRuleCondition" property="enableVoidRule" value="true">				
			<gmjsp:button value="&nbsp;&nbsp;${varVoid}&nbsp;&nbsp;&nbsp;" gmClass="button"  buttonType="Save" onClick="fnVoidPicConsequence(this.form);"/>
			</logic:equal>	
			 <logic:equal name="frmRuleCondition" property="enableVoidRule" value="false">				
			<gmjsp:button value="&nbsp;&nbsp;${varVoid}Void&nbsp;&nbsp;&nbsp;" disabled="true" buttonType="Save" gmClass="button"  onClick="fnVoidPicConsequence(this.form);"/>
			</logic:equal>
		</td>
	</tr>	
	<tr><td colspan="2" class="LLine" height="1"></td></tr>	
	<tr>
			<td height="25" class="RightDashBoardHeader" colspan="2"><fmtConsequenceFileUpload:message key="LBL_UPLOADPICTURE"/></td>
			 
		</tr>	
	<tr>
		<td colspan="2">
			<display:table name="requestScope.frmRuleCondition.alDbConsequenceList" class="its" id="currentRowObject" style="height:10" decorator="com.globus.common.displaytag.beans.DTRuleSetupWrapper">
				<display:column property="RULECONSQID" title="" style="width:30px" />
				<fmtConsequenceFileUpload:message key="LBL_PICTURE" var="varPicture"/><display:column property="PICTURE" title="${varPicture}" style="width:60px" />
				<fmtConsequenceFileUpload:message key="LBL_COMMENTS" var="varComments"/><display:column property="MESSAGE" title="${varComments}" style="width:200px" />							
				<fmtConsequenceFileUpload:message key="LBL_CREATEDBY" var="varCretedBy"/><display:column property="CREATEDBY" title="${varCretedBy}" style="width:60"/>
				<fmtConsequenceFileUpload:message key="LBL_CREATED_DATE" var="varCreatedDate"/><display:column property="CREATEDDT" title="${varCreatedDate}" style="width:60"/>																											
			</display:table>   
		</td>
	</tr>
</table>
</form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>

<%
	}
catch(Exception ex)
{
	ex.printStackTrace();
}
%>