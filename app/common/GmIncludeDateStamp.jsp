<%
	/**********************************************************************************
	 * File		 		: GmIncludeDateStamp.jsp
	 * Desc		 		: UserId and Date include page
	 * Version	 		: 1.0
	 * Author			: Ritesh Shah
	 ************************************************************************************/
%>




<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java"%>
<%@page import="java.util.Date,com.globus.common.beans.GmCalenderOperations"%>
<%@ taglib prefix="fmtIncludeDateStamp" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmIncludeDateStamp.jsp -->
<fmtIncludeDateStamp:setLocale value="<%=strLocale%>"/>
<fmtIncludeDateStamp:setBundle basename="properties.labels.common.GmIncludeDateStamp"/>  
<%
String strPrintedBy = (String)session.getAttribute("strSessShName");
GmCalenderOperations.setTimeZone(strGCompTimeZone); 
%>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tr>
       <td width="15%"></td>
       <td width="20%" class="RightTableCaption" align="right">&nbsp;&nbsp;<fmtIncludeDateStamp:message key="LBL_PRINTED_BY"/></td>
       <td width="8%">&nbsp;<%=strPrintedBy%>&nbsp;</td>                                                      
       <td width="15%" class="RightTableCaption" align="right">&nbsp;&nbsp;<fmtIncludeDateStamp:message key="LBL_PRINTED_DATE"/></td>
       <td width="80%">&nbsp;<%=GmCalenderOperations.getCurrentDate(strGCompDateFmt)%>&nbsp;</td>
       
</tr> 
</table>