
<!-- common\GmExchangeRateAvgRpt.jsp -->
<%@ page language="java" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %> 
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import ="com.globus.common.beans.GmGridFormat"%>
<%@ page import ="com.globus.common.beans.GmCrossTabFormat"%>
<%@ taglib prefix="fmtExchangeRateReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtExchangeRateReport:setLocale value="<%=strLocale%>"/>
<fmtExchangeRateReport:setBundle basename="properties.labels.common.GmExchangeRateReport"/>

<bean:define id="hmCrossTabData" name="frmCommonCurrConvForm" property="hmCrossTabData" type="java.util.HashMap"></bean:define>

<%

String strWikiTitle = GmCommonClass.getWikiTitle("EXCHANGE_RATE_AVG_REPORT");
String strGridData = "";
GmGridFormat gmGridFormat = new GmGridFormat();
gmGridFormat.setDecorator("com.globus.crosstab.beans.GmExchangeRateDecorator");
gmGridFormat.setColumnWidth("Name",300);
gmGridFormat.addColumnAlign("Name","left");
gmGridFormat.reNameColumn("Name","Currency");
gmGridFormat.displayZeros(true);
gmGridFormat.setSortRequired(true);

gmGridFormat.setHideColumn("Total");
gmGridFormat.addSortType("Name","String");
strGridData = gmGridFormat.generateGridXML(hmCrossTabData);

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Exchnage Rate Report </TITLE>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">
<link rel="stylesheet" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonCurrConv.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>
<script>
	 var date_format = '<%=strGCompDateFmt%>';
	 var gridData= '<%=strGridData%>';
	 var objGridData = '<%=strGridData%>';

</script> 

</HEAD>
<BODY leftmargin="20" topmargin="10"  onload = "fnOnPageLoadRpt()">

 <html:form action="/gmCommonCurrConv.do">
<html:hidden property="strOpt" value="frmCommonCurrConvForm"/>
<html:hidden property="selectedCurrency" value="frmCommonCurrConvForm"/>
<table border="0"  height="60" class="DtTable800" cellspacing="0" cellpadding="0">
		<tr  height="25"><td class="RightDashBoardHeader" colspan="5">&nbsp;<fmtExchangeRateReport:message key="LBL_EXCHANGE_RATE_AVG_REPORT"/></td>
		 
		     <td class="RightDashBoardHeader" align="right">
					<fmtExchangeRateReport:message key="IMG_ALT_HELP" var = "varHelp"/><img id='imgEdit' align="right" style='cursor: hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
					height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');"/>&nbsp;
			    </td> 
		</tr>

		<tr class="oddshade" height="28">
			 <td class="RightTableCaption" colspan="1"  align="right"><fmtExchangeRateReport:message key="FROM_DATE"/>:&nbsp;</td>
			 <td class="RightTableCaption" colspan="1"> <gmjsp:dropdown controlName="fromMonth" SFFormName="frmCommonCurrConvForm" SFSeletedValue="fromMonth" 
			     codeId="CODENMALT" codeName="CODENM" tabIndex="1" SFValue="alMonthList"  /></td>
			 <td class="RightTableCaption" colspan="1"> <gmjsp:dropdown controlName="fromYear" SFFormName="frmCommonCurrConvForm" SFSeletedValue="fromYear" 
			     codeId="CODENM" codeName="CODENM" tabIndex="2" SFValue="alYearList"  /></td>
			 <td class="RightTableCaption" colspan="1"  align="right"><fmtExchangeRateReport:message key="TO_DATE"/>:&nbsp;</td>
			 <td class="RightTableCaption" colspan="1"> <gmjsp:dropdown controlName="toMonth" SFFormName="frmCommonCurrConvForm" SFSeletedValue="toMonth" 
			     codeId="CODENMALT" codeName="CODENM" tabIndex="3" SFValue="alMonthList" /></td>
			 <td class="RightTableCaption" colspan="1"> <gmjsp:dropdown controlName="toYear" SFFormName="frmCommonCurrConvForm" SFSeletedValue="toYear" 
			     codeId="CODENM" codeName="CODENM" tabIndex="4" SFValue="alYearList"  /></td>
			
		</tr>
		<tr >
			 <td class="RightTableCaption" align="right" HEIGHT="30" colspan="1"><fmtExchangeRateReport:message key="LBL_CURRENCY"/>:&nbsp;</td>            
    
			    <td colspan="3"> <DIV style="display: visible; height: 200px; width:100px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 1px; margin-right: 2px;">
						<table >
							<tr>
								<td><html:checkbox property="selectAll"
									onclick="fnSelectAll('toggle');" tabindex="2"/><B><fmtExchangeRateReport:message key="LBL_SELECT_ALL"/></B></td>
							</tr>
						</table>
			
						<table>
							  <logic:iterate id="selectedCurrencylist"
								name="frmCommonCurrConvForm" property="alCurrency">
								<tr>
									<td><htmlel:multibox property="checkSelectedCurrency" value="${selectedCurrencylist.CODEID}" tabindex="3"/>
										 <bean:write name="selectedCurrencylist" property="CODENMALT" /></td>
								</tr>
							</logic:iterate>
						</table>
						</DIV>  
						
			    </td>
		
			<td colspan="2" > <fmtExchangeRateReport:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;Load&nbsp;"gmClass="button" buttonType="Load" onClick="fnLoadAvgReport()" /></td>	
		</tr>
	
		

<%
		if (strGridData.indexOf("cell") != -1) {
	%> 
		<tr>
			<td colspan="7"><div width="100%" id="dataGridDiv" height="300px"></div></td></tr>
		<tr>						
			<td align="center" colspan="7"><br>
				<div class='exportlinks'><fmtExchangeRateReport:message key="DIV_EXPORT_OPT"/>: <img src='img/ico_file_excel.png' />&nbsp; <a href="#" onclick="fnExport();"><fmtExchangeRateReport:message key="DIV_EXCEL"/>  </a></div>                         
      		</td>
     	</tr> 
	<%}%> 
 
</table>
</html:form> 
</BODY>
<%@ include file="/common/GmFooter.inc" %>

</HTML>