<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %> 
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.Locale"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%@ page import="com.globus.valueobject.common.GmDataStoreVO"%> 
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%>

<% 
	Locale locale = null;
	String strLocale = "";
	String strJSLocale = "";
	String strPiwikUrl = "";
	String strPiwikSiteId = "";
	Logger log = GmLogger.getInstance(GmCommonConstants.JSP);  // Instantiating the Logger  - write it to temp
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH"); 
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	String strWikiPath = GmCommonClass.getString("GWIKI");
	String strBrowserType = GmCommonClass.parseNull((String)session.getAttribute("strSessBrowserType"));
	String strClientSysType = GmCommonClass.parseNull((String)session.getAttribute("strSessClientSysType"));
	String strCountryCode = GmCommonClass.countryCode;	
	String strSwitchUserFl = GmCommonClass.parseNull((String)session.getAttribute("strSwitchUserFl"));
	String strExtWebPath = GmCommonClass.getString("GMEXTWEB");
	String strXmlPath =  GmCommonClass.getString("GMXML");
	//Getting Company Locale from session
	String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
	if(!strSessCompanyLocale.equals("")){
		locale = new Locale("en", strSessCompanyLocale);
	 	strLocale = GmCommonClass.parseNull((String)locale.toString());
	 	strJSLocale = "_"+strLocale;
	}
	//Getting company info 
	GmDataStoreVO gmDataStoreVO = (GmDataStoreVO)request.getAttribute("gmDataStoreVO");
	//If VO is null then get the default company info
	gmDataStoreVO = ((gmDataStoreVO == null)||((gmDataStoreVO.getCmpdfmt()).equals("")))? GmCommonClass.getDefaultGmDataStoreVO():gmDataStoreVO;
	//Assign to local variables. It will used in all JSPs.
	String strGCompTimeZone =gmDataStoreVO.getCmptzone();
	String strGCompDateFmt = gmDataStoreVO.getCmpdfmt();
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	
	// Get property from JBOSS Server environment
	try{
	    strPiwikUrl    = System.getProperty("PIWIK_URL");
		strPiwikSiteId = System.getProperty("PIWIK_SITE_ID");
	}catch(Exception e){
		//catch and just suppress error
	}
%>
<html>
<link href="/assets/node_modules/switchery/dist/switchery.min.css" rel="stylesheet" />
<link href="/assets/node_modules/select2/dist/css/select2.min.css" rel="stylesheet" />
 <link href="/dist/css/style.min.css" rel="stylesheet">

	<script src="/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
	    <script src="/assets/node_modules/popper/popper.min.js"></script>
        <script src="/dist/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="/dist/js/waves.js"></script>
 
        <script src="/dist/js/custom.min.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/json2.js"></script>
<script language="javascript" src="/assets/node_modules/switchery/dist/switchery.min.js"></script>
<script language="javascript" src="/assets/node_modules/select2/dist/js/select2.full.min.js"></script>
<script src="/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
</html>
<script type="text/javascript">
	var GLOBAL_BrowserType = '<%=strBrowserType%>';
	var GLOBAL_ClientSysType = '<%=strClientSysType%>';
	var vCountryCode = '<%=strCountryCode%>';
	var SwitchUserFl = '<%=strSwitchUserFl%>';
	var piwikURL	= '<%=strPiwikUrl%>';
	var piwikSiteID = '<%=strPiwikSiteId%>';

	// To check whether an object is undefined or not
	function validateObject(object)
	{
	    var obj;
		obj = eval(object);
		if(obj!=undefined && obj!=null){
			return true;
		}else{
			return false;
		}
	}
	
	 // Below code is used for PIWIK Service
	 // It tracks online visits to spineIT and displays reports on these visits for analysis
 	 // Author: KarthikS
 	try{
	  	if(piwikURL != 'null' && piwikSiteID != 'null'){
		   var _paq = window._paq || [];
		   /*tracker methods like "setCustomDimension" should be called before "trackPageView" */
		  _paq.push(['trackPageView']);
		  _paq.push(['enableLinkTracking']);
		  (function() {
		    var u=piwikURL;
		    _paq.push(['setTrackerUrl', u+'matomo.php']);
		    _paq.push(['setSiteId', piwikSiteID]);
		    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
		  })();
		}
	} catch(e){
		//catch and just suppress error
	}

</script>

   <script>
        $(function () {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function () {
                new Switchery($(this)[0], $(this).data());
            });
            // For select 2
            $(".select2").select2();
            $('.selectpicker').selectpicker();
            //Bootstrap-TouchSpin
            $(".vertical-spin").TouchSpin({
                verticalbuttons: true
            });
            var vspinTrue = $(".vertical-spin").TouchSpin({
                verticalbuttons: true
            });
            if (vspinTrue) {
                $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
            }
            $("input[name='tch1']").TouchSpin({
                min: 0,
                max: 100,
                step: 0.1,
                decimals: 2,
                boostat: 5,
                maxboostedstep: 10,
                postfix: '%'
            });
            $("input[name='tch2']").TouchSpin({
                min: -1000000000,
                max: 1000000000,
                stepinterval: 50,
                maxboostedstep: 10000000,
                prefix: '$'
            });
            $("input[name='tch3']").TouchSpin();
            $("input[name='tch3_22']").TouchSpin({
                initval: 40
            });
            $("input[name='tch5']").TouchSpin({
                prefix: "pre",
                postfix: "post"
            });
            // For multiselect
            $('#pre-selected-options').multiSelect();
            $('#optgroup').multiSelect({
                selectableOptgroup: true
            });
            $('#public-methods').multiSelect();
            $('#select-all').click(function () {
                $('#public-methods').multiSelect('select_all');
                return false;
            });
            $('#deselect-all').click(function () {
                $('#public-methods').multiSelect('deselect_all');
                return false;
            });
            $('#refresh').on('click', function () {
                $('#public-methods').multiSelect('refresh');
                return false;
            });
            $('#add-option').on('click', function () {
                $('#public-methods').multiSelect('addOption', {
                    value: 42,
                    text: 'test 42',
                    index: 0
                });
                return false;
            });
            $(".ajax").select2({
                ajax: {
                    url: "https://api.github.com/search/repositories",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;
                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                minimumInputLength: 1,
                //templateResult: formatRepo, // omitted for brevity, see the source of this page
                //templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
            });
        });
    </script>