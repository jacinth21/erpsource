<%
/**********************************************************************************
 * File		 		: GmAuditPriceLogDetail.jsp
 * Desc		 		: To display Price Log Information
 * Version	 		: 1.0
 * author			: Elango
************************************************************************************/
%>







<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<!-- common\GmAuditPriceLogDetail.jsp -->
<%@ taglib prefix="fmtAuditPriceLog" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtAuditPriceLog:setLocale value="<%=strLocale%>"/>
<fmtAuditPriceLog:setBundle basename="properties.labels.common.GmAuditPriceLogDetail"/>

<bean:define id="ldtResult" name="frmAuditPriceLog" property="ldtResult" type="java.util.List"></bean:define>
<bean:define id="auditId" name="frmAuditPriceLog" property="auditId" type="java.lang.String"></bean:define>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Price Log Information </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<%
    String strApplDateFmt = strGCompDateFmt;
    String strDateFmt = "{0,date,"+strApplDateFmt+"}";
%>
<script>
function fnClose(){
	 CloseDhtmlxWindow();
	 return false;
}

 
 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10"> 
    <div id="pricelogdetails">
		<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader"><fmtAuditPriceLog:message key="LBL_PRICE_UPD_HIST"/></td>
			</tr>
		    <tr>
	             <td>
	            	<display:table  name="requestScope.frmAuditPriceLog.ldtResult" class="its" id="currentRowObject" requestURI="/gmAuditPriceLog.do"  >
	            	<fmtAuditPriceLog:message key="DT_NAME" var="varName"/> 
					<display:column property="NAME" 	title="${varName}"  group="1" class="alignleft"   style="width: 250px"  />
					<fmtAuditPriceLog:message key="DT_TYPE" var="varType"/> 
					<display:column property="TYPE" 	title="${varType}"  group="2" class="alignleft"   style="width:  80px"/>
					<fmtAuditPriceLog:message key="DT_VALUE" var="varValue"/>
					<display:column property="VALUE" 	title="${varValue}"    		class="alignright"   style="width:  50px" sortable="true"/>
					<fmtAuditPriceLog:message key="DT_COMMENTS" var="varComments"/>
					<display:column property="COMMENTS" title="${varComments}"    	class="alignleft"   style="width: 240px"/>
					<fmtAuditPriceLog:message key="DT_UPD_DT" var="varUpddt"/>
					<display:column property="UPDATED_DATE" title="${varUpddt}" class="aligncenter" style="width: 180px" sortable="true" format="<%=strDateFmt%>"/>							 
					<fmtAuditPriceLog:message key="UPDATED_BY" var="varUpdBy" /> 
					<display:column property="UPDATED_BY" 	title="${varUpdBy}" 	class="alignleft"   style="width: 100px" sortable="true"/> 
					</display:table>
	            </td>
	        </tr>
	        <tr><td class="LLine" height="1" ></td></tr>
	        <tr><td height="1" ></td>&nbsp;&nbsp;</tr>
			<tr>
			    <td align="center" >&nbsp;
			    		<fmtAuditPriceLog:message key="BTN_CLOSE" var="varClose" /> 
	          			<gmjsp:button value="&nbsp;${varClose}&nbsp;" gmClass="button" buttonType="Load" onClick="fnClose();" />&nbsp;
			    </td>			                				                    
		    </tr>
		    <tr><td height="1" ></td>&nbsp;&nbsp;</tr>
	    </table>
    </div>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

