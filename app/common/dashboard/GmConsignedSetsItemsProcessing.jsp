<%
/**********************************************************************************
 * File		 		: GmConsignedSetsItemsProcessing.jsp
 * Desc		 		: Demand Sheet Part Drill Down
 * Version	 		: 1.0
 * author			: Tarika Chandure
************************************************************************************/
%>

<%@include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtConsignedSetsItemsProcessing" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmConsignedSetsItemsProcessing.jsp -->
<fmtConsignedSetsItemsProcessing:setLocale value="<%=strLocale%>"/>
<fmtConsignedSetsItemsProcessing:setBundle basename="properties.labels.custservice.GmDashBoardHome"/>
<HTML>
<HEAD>
<% 
Object bean = pageContext.getAttribute("frmCustDashBoardDispatch", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<TITLE>Globus Medical: Dashboard - Consigned Sets/Items for
Processing</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_selection.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_nxml.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script> 
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<bean:define id="gridData" name="frmCustDashBoardDispatch"
	property="strXmlData" type="java.lang.String"></bean:define>
<script type="text/javascript">
var gridObjData ='<%=gridData%>';
var mygrid;   
function fnOnPageLoad(){
	if(gridObjData!="")
	{
		mygrid = initGridWithDistributedParsing('dataGridDiv',gridObjData);
		mygrid.attachHeader('#rspan,#rspan,#text_filter,#text_filter,#select_filter,#text_filter,#rspan,#select_filter');
		mygrid.groupBy(1);
		mygrid.enableTooltips("false,false,false,false,false,false,false,false");
		mygrid.setColumnHidden(1,true);
		mygrid.setColumnHidden(7,true);
		mygrid.enableHeaderMenu();
		mygrid.enableBlockSelection(true); 
		mygrid.attachEvent("onKeyPress", keyPressed);
		dhtmlxEvent(mygrid.hdr.rows[2],'contextmenu',function(e){   //added for PMT-37219 - Windows Right Click Function in Dashboards
			e.cancelBubble=true;
		});

	}
}
// Checking for Key Events to Copy
function keyPressed(code, ctrl, shift) {
    if (code == 67 && ctrl) {
        /*if (!mygrid._selectionArea)
            return alert("You need to select a block area in grid first");
            */
        mygrid.setCSVDelimiter("\t");
        mygrid.copyBlockToClipboard();
    }
    
    if (code == 86 && ctrl) {
    	if(mygrid._selectionArea!=null){
			var colIndex = mygrid._selectionArea.LeftTopCol;
			var cb_columnName = '';
			
			if(colIndex!=undefined && colIndex==0){
				alert(' Please Select a valid value from Part Number Drop Down');
			}else{
				mygrid.pasteBlockFromClipboard();
			}
			mygrid._HideSelection();
		}else{
			alert(' Please BlockSelect Cell(s) to Paste Data.');
		}
    }
	// This code is to block delete the content from the grid.
	if(code==46&&!ctrl&&!shift){
		
		if(mygrid._selectionArea!=null){
			var area=mygrid._selectionArea
			var leftTopCol=area.LeftTopCol;
			var leftTopRow=area.LeftTopRow;
			var rightBottomCol=area.RightBottomCol;
			var rightBottomRow=area.RightBottomRow;
			
			for (var i=leftTopRow; i<=rightBottomRow; i++){
				
				setRowAsModified(mygrid.getRowId(i),true);

				for (var j=leftTopCol; j<=rightBottomCol; j++){
					mygrid.cellByIndex(i,j).setValue("");
				}
			}
			mygrid._HideSelection();
		}
	}
    return true;
}
function fnExport(str){
	mygrid.detachHeader(1);
	mygrid.unGroup();
	mygrid.setColumnHidden(1,false);
	mygrid.setColumnHidden(0,true);
	if(str=="excel"){
		mygrid.toExcel('/phpapp/excel/generate.php');
	}else{
		mygrid.toExcel('/phpapp/pdf/generate.php');
	}
	mygrid.attachHeader('#rspan,#rspan,#text_filter,#text_filter,#select_filter,#text_filter,#rspan');
	mygrid.groupBy(1);
	mygrid.setColumnHidden(1,true);
	mygrid.setColumnHidden(0,false);
}
</script>
</HEAD>
<BODY leftmargin="0" topmargin="0" onload="fnOnPageLoad();">
<html:form action="/gmOperDashBoardDispatch.do"  >
<html:hidden property="hAction" name="frmOperDashBoardDispatch" />
<html:hidden property="hId" name="frmOperDashBoardDispatch"/>
<html:hidden property="hMode" name="frmOperDashBoardDispatch"/>
<html:hidden property="hFrom" name="frmOperDashBoardDispatch"/>
<html:hidden property="hNCMRId" name="frmOperDashBoardDispatch"/>
<html:hidden property="hConsignId" name="frmOperDashBoardDispatch"/>
<html:hidden property="hType" name="frmOperDashBoardDispatch"/>
<html:hidden property="hOpt" name="frmOperDashBoardDispatch"/>
</html:form>


<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<div id="dataGridDiv" width="990px" height="580px" style="top:0;left: 0;"></div>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
			<td align="center">
		<div class='exportlinks'><fmtConsignedSetsItemsProcessing:message key="LBL_EXPORT_OPTIONS"/> : <img
			src='img/ico_file_excel.png' />&nbsp;<a href="#"
			onclick="fnExport('excel');"><fmtConsignedSetsItemsProcessing:message key="LBL_EXCEL"/>  </a></div>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
</table>
</BODY>
</HTML>
