
<%
	/**********************************************************************************
	 * File		 		: GmPendingVerification.jsp
	 * Desc		 		: Dashboard - Pending Verification
	 * Version	 		: 1.0
	 * author			: Tarika Chandure
	 ************************************************************************************/
%>

<!--common\GmPendingVerification.jsp -->


<!-- WEB-INF path corrected for JBOSS migration changes -->


<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ page import="org.apache.struts.action.DynaActionForm"%>
<%@ page import="com.globus.clinical.beans.GmStudyBean"%>
<%@ page import="com.globus.common.beans.GmCommonBean"%>
<%
	Object bean = pageContext.getAttribute("frmClinicalDashBoardDispatch", PageContext.REQUEST_SCOPE);
	pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<%
	try {
		GmServlet gm = new GmServlet();
		gm.checkSession(response, session);

		String strDeptId = (String) session.getAttribute("strSessDeptId") == null ? "" : (String) session.getAttribute("strSessDeptId");
		String strAccessLvl = (String) session.getAttribute("strSessAccLvl") == null ? "" : (String) session.getAttribute("strSessAccLvl");

		int intSize = 0;

		HashMap hmReturn = (HashMap) request.getAttribute("hmReturn");
		HashMap hmValue = new HashMap();
		ArrayList alPending = new ArrayList();
		String strTemp = "";

		//Common Variable used for looping
		String strAccountID = "";
		String strAccountNm = "";
		String strPreAccountID = ""; // Assigned with some dummy initial value 
		String strPeriodID = "";
		String strPrePeriodID = "";
		String strPeriodDS = "";
		String strPrePatientIDE = ""; // Assigned with some dummy initial value 				
		StringBuffer strDivTab = new StringBuffer();
		String strStudyID = "";
		String strFromID = "";
		String strPatientPKey = "";
		String strStudyPKey = "";
		String strPatientIDE = "";
		String strPatientPeriod = "";
		int patientCount = 0;
		int formCount = 0;
		String strTempAccountId = "";
		DynaActionForm gmClinicalDashBoardDispatchForm = (DynaActionForm) bean;
		ArrayList alCras = (ArrayList) gmClinicalDashBoardDispatchForm.get("CRAS");
		ArrayList alStudyList = (ArrayList) gmClinicalDashBoardDispatchForm.get("CRSTUDYLIST");
		String strSelectedCraID = (String) gmClinicalDashBoardDispatchForm.get("strCraId");
%>

<HTML>
<HEAD>
<TITLE>Globus Medical: Accounts Dashboard</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css"
	href="<%=strCssPath%>/GlobusSales.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script>
function fnCallInv(po,id)
{
	document.frmClinicalDashBoardDispatch.hId.value = id;
	document.frmClinicalDashBoardDispatch.hPO.value = po;
	document.frmClinicalDashBoardDispatch.hMode.value = "INV";
	document.frmClinicalDashBoardDispatch.hPgToLoad.value = "GmInvoiceServlet";
	document.frmClinicalDashBoardDispatch.submit();
}
function Toggle(val)
{

	var obj = eval("document.all.div"+val);
	var trobj = eval("document.all.tr"+val);
	var tabobj = eval("document.all.tab"+val);

	if (obj.style.display == 'none')
	{
		obj.style.display = '';
		//trobj.className="ShadeRightTableCaptionBlue";
		//tabobj.style.background="#c6e6fe";
		//tabobj.style.background="#d7ecfd";
		//tabobj.style.background="#ecf6fe";
	}
	else
	{
		obj.style.display = 'none';
		//trobj.className="";
		//tabobj.style.background="#ffffff";
	}
}
function fnFormDetails(valFormID, valStudy, valStudyPeriodKey, valCboPeriod, valPatientID)
{
	document.frmClinicalDashBoardDispatch.action = "/GmFormDataEntryServlet";
	document.frmClinicalDashBoardDispatch.hAction.value="LoadQues";
	document.frmClinicalDashBoardDispatch.Cbo_Form.value = valFormID;
	document.frmClinicalDashBoardDispatch.Cbo_Study.value = valStudy;
	document.frmClinicalDashBoardDispatch.hStPerId.value = valStudyPeriodKey;
	document.frmClinicalDashBoardDispatch.Cbo_Period.value = valCboPeriod;
	document.frmClinicalDashBoardDispatch.Cbo_Patient.value = valPatientID;
	document.frmClinicalDashBoardDispatch.submit();
}

</script>
</HEAD>


<BODY leftmargin="20" topmargin="10">
<FORM name="frmClinicalDashBoardDispatch" method="POST"><input
	type="hidden" name="hAction"> <input type="hidden"
	name="Cbo_Form"> <input type="hidden" name="Cbo_Period">
<input type="hidden" name="hStPerId"> <input type="hidden"
	name="Cbo_Study"> <input type="hidden" name="Cbo_Patient">



<table border="0" width="900" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="3" bgcolor="#666666"></td>
	</tr>
	<tr>
		<td bgcolor="#666666" width="1"></td>
		<td width="898" valign="top">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td height="25" class="RightDashBoardHeader">
				<table>
					<tr>
						<td width="75%" class="RightDashBoardHeader">Pending
						Verification</td>
						<td align="right" class="RightDashBoardHeader">Study List :</td>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<td width="10%" align="left"><gmjsp:dropdown
							controlName="studyListId"
							SFFormName="frmClinicalDashBoardDispatch"
							SFSeletedValue="studyListId" SFValue="CRSTUDYLIST" codeId="ID"
							codeName="NAME" defaultValue="ALL" onChange="fnSubmit3();" /></td>
						<td align="right" class="RightDashBoardHeader">CRA Name :</td>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<td width="10%" align="left"><gmjsp:dropdown
							controlName="craId" SFFormName="frmClinicalDashBoardDispatch"
							SFSeletedValue="craId" onChange="fnSubmit3();" SFValue="CRAS"
							codeId="ID" codeName="NAME" defaultValue="ALL" /></td>
					</tr>
				</table>
				</td>

			</tr>


			<!--/************************ Below Code is to List Pending Verification *************** -->


			<tr>
				<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<thead>
						<tr class="aaTopHeader" height="18"
							style="position: relative; top: expression(this . offsetParent . scrollTop);">
							<th align="center" width="180">Account / Patient IDE</th>
							<th align="center" width=320>Form Name</th>
							<!--<th  align="center" width="50"> Expected Date</th>	-->
							<th align="center" width="70">Exam Date</th>
							<th align="center" width="70">Entered By</th>
							<th align="center" width="40">Edit</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="Line" colspan="8"></td>
						</tr>
						<%
							alPending = (ArrayList) gmClinicalDashBoardDispatchForm.get("ldtVerifyListResult");
								intSize = alPending.size();
								strPreAccountID = "INT#"; // Assigned with some dummy initial value 
								strPrePatientIDE = "INT#"; // Assigned with some dummy initial value 				

								for (int i = 0; i < intSize; i++) {
									hmValue = (HashMap) alPending.get(i);
									strAccountID = GmCommonClass.parseNull((String) hmValue.get("A_ID"));
									strAccountNm = GmCommonClass.parseNull((String) hmValue.get("A_NAME"));
									strAccountNm = strAccountNm.equals("") ? "No Name" : strAccountNm;
									strPeriodID = GmCommonClass.parseNull((String) hmValue.get("S_P_ID"));
									strPeriodDS = GmCommonClass.parseNull((String) hmValue.get("S_DS"));

									strStudyID = GmCommonClass.parseNull((String) hmValue.get("S_ID"));
									strFromID = GmCommonClass.parseNull((String) hmValue.get("F_ID"));
									strPatientPKey = GmCommonClass.parseNull((String) hmValue.get("P_KEY"));
									strStudyPKey = GmCommonClass.parseNull((String) hmValue.get("S_P_KEY"));
									strPatientIDE = GmCommonClass.parseNull((String) hmValue.get("P_IDE_NO"));
									strPatientPeriod = strPatientPKey + "-" + strPeriodDS;

									//	System.out.println(strPeriodID + strPeriodDS);

									//****************************************************
									// Below section is used to create Phase/Per Group 
									//****************************************************
									if (!strAccountID.equals(strPreAccountID)) {

										strDivTab.setLength(0);
										// To reset the old value 
										if (!strPreAccountID.equals("INT#")) {
											strDivTab.append("</table></td></tr></table></td></tr>");
											strPrePatientIDE = "INT#";
										}

										strDivTab.append("<tr id=tr" + i + "P class=ShadeDarkGrayTD ><td colspan=8 align=left height=20 > &nbsp;");
										strDivTab.append("<B><A class=RightText title='Click to Expand the Phase ' ");
										strDivTab.append("href=\"javascript:Toggle('");
										strDivTab.append(i);
										strDivTab.append("P')\">");
										strDivTab.append(strAccountNm);
										strDivTab.append("</a> </B></td></tr>");
										strDivTab.append("<tr><td colspan=8 class=borderDark ></td></tr>");
										strDivTab.append("<tr><td colspan=8><div style=display:none ");
										strDivTab.append(" id=div" + i + "P> ");
										strDivTab.append(" 	<table width=100% cellpadding=0 cellspacing=0 border=0 id=tab" + i + "P > ");

										strPreAccountID = strAccountID;

										out.println(strDivTab.toString());

									}

									//****************************************************
									// Below section is used to create the Account Group 
									//****************************************************
									if (!strPatientPeriod.equals(strPrePatientIDE)) {
										strDivTab.setLength(0);
										// To reset the old value 
										if (!strPrePatientIDE.equals("INT#")) {
											strDivTab.append("</table></td></tr>");
										}

										strDivTab.append("<tr id=tr" + i + "A class=ShadeLightGrayTD ><td colspan=8 align=left height=20 > &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
										strDivTab.append("<A class=RightText title='Click to Expand the Form' ");
										strDivTab.append("href=\"javascript:Toggle('");
										strDivTab.append(i);
										strDivTab.append("A')\">");
										strDivTab.append(strPatientIDE + " - " + strPeriodDS);
										strDivTab.append("</a></td></tr>");
										strDivTab.append("<tr><td colspan=8 class=borderDark ></td></tr>");
										strDivTab.append("<tr><td colspan=8><div style=display:none ");
										strDivTab.append(" id=div" + i + "A> ");
										strDivTab.append(" 	<table width=100% cellpadding=0 cellspacing=0 border=0 id=tab" + i + "A > ");

										strPrePatientIDE = strPatientPeriod;

										out.println(strDivTab.toString());

									}
						%>
						<tr class="RightTextSmall">
							<td align="left" width="180" height="20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td align="left" width="320"><%=GmCommonClass.parseNull((String) hmValue.get("F_NAME"))%></td>
							<!--<td align="center" width="50"> <%=GmCommonClass.parseNull((String)hmValue.get("EXP_DATE"))%></td> -->
							<td align="center" width="70"><%=GmCommonClass.parseNull((String) hmValue.get("E_DATE"))%></td>
							<td align="center" width="70"><%=GmCommonClass.parseNull((String) hmValue.get("ENTERED_BY"))%></td>
							<td align="center" width="40"><img id="imgEdit"
								style="cursor: hand" src="<%=strImagePath%>/edit_icon.gif"
								title="Click to Edit Patient Question Information" width="14"
								height="14"
								onClick="javascript:fnFormDetails('<%=strFromID%>', '<%=strStudyID%>', '<%=strStudyPKey%>' , '<%=strPeriodID%>' , '<%=strPatientPKey%>' )" />
							</td>
						</tr>
						<tr>
							<td class="borderDark" colspan="8"></td>
						</tr>
						<%
							}

								if (intSize <= 0) {
									out.println(" <tr class=RightTableCaption align=center > <td colspan=8 height=30>");
									out.println(" No Pending Verification </td> <tr> ");
									out.println(" <tr><td class=borderDark colspan=8></td></tr>");
								} else {
									out.println("</table></td></tr>"); // For the Account
									out.println("</table></td></tr>"); //  For the Phase  <!--Final Inner Table -->
								}
						%>
					
				</table>
				</td>
			</tr>
			<!-- Pending Verification Table -->


		</table>
		</td>
		<td bgcolor="#666666" width="1"></td>
	</tr>
	<tr>
		<td colspan="3" height="1" bgcolor="#666666"></td>
	</tr>
</table>
</FORM>
<%
	} catch (Exception e) {
		e.printStackTrace();
	}
%>
</BODY>

</HTML>