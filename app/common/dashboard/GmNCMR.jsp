<%
/**********************************************************************************
 * File		 		: GmNCMR.jsp
 * Desc		 		: Dashboard - NCMR
 * Version	 		: 1.0
 * author			: Tarika Chandure
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<!-- common\dashboard\GmNCMR.jsp -->
<%@ include file="/common/GmHeader.inc" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}

%>


<%@ taglib prefix="fmtNCMR" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtNCMR:setLocale value="<%=strLocale%>"/>
<fmtNCMR:setBundle basename="properties.labels.common.dashboard.GmDHRNCMR"/>
<HTML>
<HEAD>
<TITLE> Globus Medical: Dashboard - NCMR </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>


</HEAD>

<BODY leftmargin="20" topmargin="10">


	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<td colspan="2">
					<display:table name="requestScope.frmOperDashBoardDispatch.ldtNCMRResult" class="its"
						id="currentRowObject" requestURI="/gmOperDashBoardDispatch.do?method=dashboardNCMR" style="height:35" 
						decorator="com.globus.displaytag.beans.dashboard.DTNCMRWrapper" freezeHeader="true">
							<fmtNCMR:message key="DT_VENDOR_NM" var="varVendorNm"/>
							<display:column property="VNAME" title="${varVendorNm}" group="1" style="width:220"/>
							<fmtNCMR:message key="DT_NCMR_ID" var="varNCMR"/>
							<display:column property="NID" title="${varNCMR}"  style="width:220"/>
							<fmtNCMR:message key="DT_PART_NUM" var="varPNum"/>								
							<display:column property="PNUM" title="${varPNum}" style="width:120" />
							<fmtNCMR:message key="DT_PART_DESC" var="varPDesc"/>
							<display:column property="PDESC" title="${varPDesc}"  style="width:420;align:right"/>
							<fmtNCMR:message key="DT_TICKETS" var="varTicket"/>
							<display:column property="TICKETID" title="${varTicket}" style="width:110"/>
							<fmtNCMR:message key="DT_PEND_EVAL" var="varPendEval"/>
							<display:column property="EID" title="${varPendEval}" style="width:120" class="aligncenter"/>
							<fmtNCMR:message key="DT_PEND_ACTION" var="varPendAction"/>
							<display:column property="SFL" title="${varPendAction}" style="width:120" class="aligncenter"/>
							<fmtNCMR:message key="DT_PEND_CLOSURE" var="varPendClos"/>
							<display:column property="VID" title="${varPendClos}" style="width:45" class="aligncenter"/>							
													
						</display:table>  </td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
    </table>		     	


</BODY>

</HTML>

