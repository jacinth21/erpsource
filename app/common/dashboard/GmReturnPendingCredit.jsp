<%
/**********************************************************************************
 * File		 		: GmReturnPendingCredit.jsp
 * Desc		 		: Demand Sheet Part Drill Down
 * Version	 		: 1.0
 * author			: Tarika Chandure
************************************************************************************/
%>

<!--common\GmReturnPendingCredit.jsp -->

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc"%>
<HTML>
<HEAD>
<% 
Object bean = pageContext.getAttribute("frmCustDashBoardDispatch", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<TITLE> Globus Medical: WIP Orders </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmReturnPendingCredit.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>

<%
	String gridData=(String)request.getAttribute("XMLGRIDDATA"); // for DHTLMX report.
%>

<script>
	var objGridData;
	objGridData = '<%=gridData%>';

	function enterPressed() {		
		if (window.event && window.event.keyCode == 13) {		
		  return false;		
		}
	}
		document.onkeypress = enterPressed;
		
</script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/displaytag.css");
</style>
</HEAD>

<BODY onLoad="javascript:fnOnPageLoad();">
<form name="frmCustDashBoardDispatch">
	<input type="hidden" name="hRAId" value=""/>
	<input type="hidden" name="hAction" value=""/>
	<input type="hidden" name="hId" value=""/>
	<table border="0" class="DtTable1000" cellspacing="0" cellpadding="0">
     <tr>
        	<td colspan="4" height="50" >
				<div id="ReturnPendingCredit" style="grid" height="656px" width="100%"></div>
		    </td>	
		</tr>	
	</table>
</form>
</BODY>

</HTML>

