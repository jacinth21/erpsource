
<%
/**********************************************************************************
 * File                      : GmConsignedItems.jsp
 * Desc                      : Dashboard - Consigned Items
 * Version             : 1.0
 * author               : Tarika Chandure
************************************************************************************/
%><%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtConsignedItems" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- common/dashboard/GmConsignedItems.jsp -->
<fmtConsignedItems:setLocale value="<%=strLocale%>"/>
<fmtConsignedItems:setBundle basename="properties.labels.custservice.GmDashBoardHome"/>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>

<bean:define id="gridData" name="frmOperDashBoardDispatch" property="strGridXmlData" type="java.lang.String"></bean:define>

<%
	String strOpJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
   //The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyInfo ="{\"cmpid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getCmpid())+"\",\"partyid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPartyid())+"\",\"plantid\":\""+GmCommonClass.parseNull((String)gmDataStoreVO.getPlantid())+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>

<HTML>

<HEAD>
<TITLE>Globus Medical: Dashboard - Consigned Items</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_filter.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.css">
<script language="JavaScript"  src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_hmenu.js"></script>
<script language="JavaScript" src="<%=strOpJsPath%>/GmConsignedItems.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>

<script>
var companyInfoObj = '<%=strCompanyInfo%>';
var objGridData;
objGridData ='<%=gridData%>';
var lblTotalTrans = '<fmtConsignedItems:message key="LBL_TOTAL_TRANS"/>';

</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onload="javascript:fnOnPageLoad();">
<form name="frmOperDashBoardDispatch">
<input type="hidden" name="hId" value="" /> 
<input type="hidden" name="hFrom" value="" /> 
<input type="hidden" name="hConsignId" value="" /> 
<input type="hidden" name="hAction" value="" /> 
<input type="hidden" name="hType" value="" />
<input type="hidden" name="hOpt" value="" /> 
<input type="hidden" name="hMode" value="" />


<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="6" width="800px">
		<div id="dataGridDiv" style="" height="570px" width="850px"></div>
		</td>
	</tr>

	<tr>
		<td height="50" colspan="6">
			<div align="center" height="40"><fmtConsignedItems:message key="LBL_EXPORT_OPTIONS"/> : 
				<img src='img/ico_file_excel.png' />&nbsp;<a href="#" onclick="gridObj.toExcel('/phpapp/excel/generate.php');"><fmtConsignedItems:message key="LBL_EXCEL"/> 
			</div>
		</td>
	</tr>
</table>
</form>
</BODY>
</HTML>

