<%
/**********************************************************************************
 * File		 		: Loaner Reconciliation.jsp
 * Desc		 		: Loaner Reconciliation
 * Version	 		: 1.0
 * author			: Tarika Chandure
************************************************************************************/
%>




<%@ include file="/common/GmHeader.inc"%>
<%@ taglib prefix="fmtLoanerReconciliation" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<!-- GmLoanerReconciliation.jsp -->
<fmtLoanerReconciliation:setLocale value="<%=strLocale%>"/>
<fmtLoanerReconciliation:setBundle basename="properties.labels.common.dashboard.GmLoanerReconciliation"/>

<%
String strOpJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_OPERATIONS");
request.setCharacterEncoding("UTF-8");
response.setContentType("text/html; charset=UTF-8");
response.setCharacterEncoding("UTF-8");
String strWikiTitle = GmCommonClass.getWikiTitle("LOANER_RECONCILIATION"); 
String strTodaysDate = (String)session.getAttribute("strSessTodaysDate")==null?"":(String)session.getAttribute("strSessTodaysDate");
String strApplnDateFmt = GmCommonClass.parseNull((String)session.getAttribute("strSessApplDateFmt"));
%>

<bean:define id="gridData" name="frmLoanerReconciliation" property="gridData" type="java.lang.String"> </bean:define>
<bean:define id="deptid" name="frmLoanerReconciliation" property="deptId" type="java.lang.String"> </bean:define>
<bean:define id="haction" name="frmLoanerReconciliation" property="haction" type="java.lang.String"> </bean:define>
<HTML>
<HEAD>

<TITLE> Globus Medical: Loaner Reconciliation </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmLoanerReconciliation.js"></script>
<script language="JavaScript" src="<%=strOpJsPath%>/GmCommonShippingReport.js"></script>

<!-- MNTTASK-3518 Stack Overflow commented bolow line -->

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<link rel="STYLESHEET" type="text/css"	href="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/skins/dhtmlxgrid_dhx_skyblue.css">

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgridcell.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_group.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/ext/dhtmlxgrid_splt.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/dhtmlxgrid_form.js "></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxGrid/excells/dhtmlxgrid_excell_link.js"></script>

<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/displaytag.css">
<script type="text/javascript">
var objGridData;
objGridData='<%=gridData%>';
var deptid='<%=deptid%>';
var haction='<%=haction%>'
var todaysDate = '<%=strTodaysDate%>';
var format = '<%=strApplnDateFmt%>';
</script>
</HEAD>


<BODY leftmargin="20" topmargin="10" onLoad="fnOnPageLoad(objGridData);">

<html:form  action="/gmLoanerReconciliation.do">
<html:hidden property="strOpt" name="frmLoanerReconciliation"/>
<html:hidden property="hreqids" name="frmLoanerReconciliation"/>
<html:hidden property="requestId" name="frmLoanerReconciliation"/>

<input type="hidden" name ="hId" />
<input type="hidden" name ="hAction" />

<table border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" ><fmtLoanerReconciliation:message key="LBL_LOANER_RECONCILIATION"/></td>
			
			<td class="RightDashBoardHeader"></td>
			<td  height="25" class="RightDashBoardHeader" align="right" colspan="5">
			<fmtLoanerReconciliation:message key="IMG_HELP" var="varHelp"/>
			    <img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
		       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
		    </td>
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
			<tr class="Shade">
			<td width="10%" height="25" class="RightTableCaption" align="Right"><fmtLoanerReconciliation:message key="LBL_FIELD_SALES"/>:</td>
			<td width="25%">&nbsp;<gmjsp:dropdown controlName="dist"  SFFormName='frmLoanerReconciliation' SFSeletedValue="dist"  defaultValue= "[Choose One]"	
				SFValue="alDist" codeId="ID" codeName="NAME" /></td>		
			<td width="10%" height="25" class="RightTableCaption" align="Right"><fmtLoanerReconciliation:message key="LBL_SALES_REP"/>:</td>	
			<td width="20%" >&nbsp;<gmjsp:dropdown controlName="rep"  SFFormName='frmLoanerReconciliation' SFSeletedValue="rep"  defaultValue= "[Choose One]"	
				SFValue="alRepList" codeId="ID" codeName="NM"/></td>
				<td  width="10%" class="RightTableCaption" align="Right"><fmtLoanerReconciliation:message key="LBL_SET_NAME"/>:</td>
			<td width="20%"> &nbsp;<html:text property="setName" name="frmLoanerReconciliation" size="15" /></td>	
		</tr>
		
		<tr><td class="LLine" height="1" colspan="6"></td></tr>
		<!-- Struts tag lib code modified for JBOSS migration changes -->
		<tr>
			<td HEIGHT="50" class="RightTableCaption" align="Right"><fmtLoanerReconciliation:message key="LBL_DATE_RANGE"/>:</td>
			<td class="RightText">&nbsp;Type:&nbsp;<html:select property="dtType">
									 <html:option value='0' ><fmtLoanerReconciliation:message key="OPT_CHOOSE_ONE"/></html:option>
									 <html:option value='LN'><fmtLoanerReconciliation:message key="OPT_LOANED_DATE"/></html:option>
									 <html:option value='RT'><fmtLoanerReconciliation:message key="OPT_RETURNED_DATE"/></html:option>
									 <html:option value='EX'><fmtLoanerReconciliation:message key="OPT_EXPECTED_RETURN_DATE"/></html:option>
			</html:select>&nbsp;<BR>&nbsp;<fmtLoanerReconciliation:message key="LBL_FROM"/>:&nbsp;<gmjsp:calendar textControlName="fromDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			&nbsp;<fmtLoanerReconciliation:message key="LBL_TO"/>:&nbsp;<gmjsp:calendar textControlName="toDt"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>
			</td>
			<td height="25" class="RightTableCaption" align="Right"><fmtLoanerReconciliation:message key="LBL_STATUS"/>:</td>
			
			<td >&nbsp; <gmjsp:dropdown controlName="status"  SFFormName='frmLoanerReconciliation' SFSeletedValue="status"  defaultValue= "[Choose One]"	
				SFValue="alStatus" codeId="CODEID" codeName="CODENM" />  
			</td>	
			<td height="25" width="10%" class="RightTableCaption" align="Right"><fmtLoanerReconciliation:message key="LBL_LOANER_TYPE"/>:</td>			
			<td width="20%">&nbsp; <gmjsp:dropdown controlName="type"  SFFormName='frmLoanerReconciliation' SFSeletedValue="type"  defaultValue= "[Choose One]"	
				SFValue="alType" codeId="CODENMALT" codeName="CODENM" />  
			</td>
			
		</tr>
		<tr class="Shade">
		<td width="10%" height="25" class="RightTableCaption" align="Right"><fmtLoanerReconciliation:message key="LBL_TXN_TYPE"/>:</td>
			<td width="25%">&nbsp;<gmjsp:dropdown controlName="qtType"  SFFormName='frmLoanerReconciliation' SFSeletedValue="qtType"  defaultValue= "[Choose One]"	
				SFValue="alQtyType" codeId="CODEID" codeName="CODENM" /></td>	
		<td colspan="2" align="center" height="24"><fmtLoanerReconciliation:message key="BTN_LOAD" var="varLoad"/><gmjsp:button value="&nbsp;${varLoad}&nbsp;"  name="Btn_Submit" gmClass="button" buttonType="Load" onClick=" onReload();" /></td>
		<td  class="RightTableCaption" colspan="2">&nbsp;&nbsp;</td>
		</tr>
		
	
</table>	

<table  border="0" class="DtTable1100" cellspacing="0" cellpadding="0">
			<tr>	<td><div id="grpData"  class="grid" height="400px"></div></td> </tr>  
			<tr>
			                <td colspan="6" align="center">
			                <div class='exportlinks'><fmtLoanerReconciliation:message key="LBL_EXPORT_OPTIONS"/> : <img src='img/ico_file_excel.png' />&nbsp;<a href="#"
			                                onclick="gridObj.toExcel('/phpapp/excel/generate.php');"><fmtLoanerReconciliation:message key="LBL_EXCEL"/>  </a>
			                                &nbsp;
			                                </div>
			                </td>

					</tr>
</table>	

</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>

</HTML>

