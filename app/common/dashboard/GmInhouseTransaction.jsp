 <%
/*********************************************************************************************************
 * File		 		: GmAllocateUser.jsp
 * Desc		 		: This screen is used to display Inventory User's and Can change the Status (Manually)
 * Version	 		: 1.0
 * author			: Vamsi Kiran Nagavarapu
**********************************************************************************************************/
%>




<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ include file="/common/GmHeader.inc"%>
<!-- common\dashboard\GmInhouseTransaction.jsp -->
<%@ taglib prefix="fmtInHouseTxn" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtInHouseTxn:setLocale value="<%=strLocale%>"/>
<fmtInHouseTxn:setBundle basename="properties.labels.custservice.GmDashBoardHome"/>
<HTML>
<HEAD>
<TITLE>Globus Medical: Inhouse Transaction</TITLE>

<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmCommon.css">
</HEAD>

<BODY leftmargin="20" topmargin="10">
<%
%>
	<table border="0" class="DtTable765"   cellspacing="0"cellpadding="0">
		<tr>
			<td colspan="5" height="25" class="Header"><fmtInHouseTxn:message key="LBL_INHOUSE_TXN"/></td>
			
		</tr>
		<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
		<tr>
			<td>
				 <jsp:include page="/common/dashboard/GmConsignedItems.jsp" />
			</td>			
		</tr>			
	</table>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>