<%
/**********************************************************************************
 * File		 		: GmDummyConsignment.jsp
 * Desc		 		: Dashboard - Dummy Consignment
 * Version	 		: 1.0
 * author			: Tarika Chandure
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>
 <%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<%@ taglib prefix="fmtDummyConsignment" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmDummyConsignment.jsp -->
<fmtDummyConsignment:setLocale value="<%=strLocale%>"/>
<fmtDummyConsignment:setBundle basename="properties.labels.common.dashboard.GmDummyConsignment"/>
<HTML>
<HEAD>
<% 
Object bean = pageContext.getAttribute("frmCustDashBoardDispatch", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<TITLE> Globus Medical: Dashboard - Dummy Consignment </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>

<BODY leftmargin="20" topmargin="10" >
	<table border="0" class="DtTable850" cellspacing="0" cellpadding="0">

		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<td colspan="2">
					<display:table name="requestScope.frmCustDashBoardDispatch.ldtDummyResult" class="its"
						id="currentRowObject" requestURI="/gmCustDashBoardDispatch.do?method=dummyConsignment" style="height:35" 
						decorator="com.globus.displaytag.beans.dashboard.DTDummyConsignmentWrapper" freezeHeader="true">
							<fmtDummyConsignment:message key="DT_ID" var="varID"/><display:column property="CONDID" title="${varID}" group="1" style="width:320"/>
							<fmtDummyConsignment:message key="DT_FROM" var="varFrom"/><display:column property="FROMNAME" title="${varFrom}" style="width:320"/>							
							<fmtDummyConsignment:message key="DT_DATE" var="varDate"/><display:column property="CONDDATE" title="${varDate}"  style="align:right;width:70;" />
							<fmtDummyConsignment:message key="DT_INITIATED_BY" var="varInitiatedBy"/><display:column property="INITIATORNAME" title="${varInitiatedBy}" style="align:right;width:250;" />
							<fmtDummyConsignment:message key="DT_COMMENTS" var="varComments"/><display:column property="COMMENTS" title="${varComments}"  style="align:right;width:70;" />
						</display:table>  </td>
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</BODY>
</HTML>

