<%
/**********************************************************************************
 * File		 		: GmDHR.jsp
 * Desc		 		: Dashboard - DHR
 * Version	 		: 1.0
 * author			: Tarika Chandure
************************************************************************************/
%>

<!--common\GmDHR.jsp -->

<%@ include file="/common/GmHeader.inc" %>

<!-- WEB-INF path corrected for JBOSS migration changes -->

<%@ page import ="org.apache.struts.action.DynaActionForm"%>

<!-- common\dashboard\GmDHR.jsp -->
<%@ taglib prefix="fmtDHR" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import="java.util.HashMap"%>
<fmtDHR:setLocale value="<%=strLocale%>"/>
<fmtDHR:setBundle basename="properties.labels.common.dashboard.GmDHRNCMR"/>
<% 
Object bean = pageContext.getAttribute("frmOperDashBoardDispatch", PageContext.REQUEST_SCOPE);
Object dtWrapper = pageContext.getAttribute("DTDHRDashboardWrapper", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);

DynaActionForm gmOperDashBoardDispatchForm = (DynaActionForm) bean;

String ajaxFlag = (String)gmOperDashBoardDispatchForm.get("ajaxFlag");

HashMap hmDHRStatusCnt = (HashMap) gmOperDashBoardDispatchForm.get("hmDHRStatCnt");
String strPICnt = GmCommonClass.parseNull((String) hmDHRStatusCnt.get("PICNT"));
String strPPCnt = GmCommonClass.parseNull((String) hmDHRStatusCnt.get("PPCNT"));
String strPVCnt = GmCommonClass.parseNull((String) hmDHRStatusCnt.get("PVCNT"));
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Dashboard - DHR </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/DemandSheet.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>
 
	
<html:form action="/gmOperDashBoardDispatch.do"  >
<html:hidden property="hAction" />
<html:hidden property="hId" />
<html:hidden property="hMode" />
<html:hidden property="hFrom" />
<html:hidden property="hNCMRId" />
<html:hidden property="hConsignId" />
<html:hidden property="hType" />
<html:hidden property="hOpt" />
	</html:form>

<BODY>

	<table border="0" width="100%" cellspacing="0" cellpadding="0">
<%if (!ajaxFlag.equals("false")){
%>		<tr>
			<td width="100%" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr class="shade"><td width="150" class="RightText" align="right" HEIGHT="24"><fmtDHR:message key="LBL_PEND_INSPEC"/>:&nbsp;</td><td><%=strPICnt%></td></tr>
  		            <tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
					<tr ><td width="150" class="RightText" align="right" HEIGHT="24"><fmtDHR:message key="LBL_PEND_PACK"/>:&nbsp;</td><td><%=strPPCnt%></td></tr>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
					<tr class="shade"><td width="150" class="RightText" align="right" HEIGHT="24"><fmtDHR:message key="LBL_PEND_VERIFY"/>:&nbsp;</td><td><%=strPVCnt%></td></tr>
					<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
				</table>
			</td>	
		</tr>
<%}else{
%>
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtDHR:message key="LBL_DHR_DTLS"/></td>
		</tr>
<%}
%>						
		<tr>
			<td width="100%" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
					<td colspan="2">
					<display:table name="requestScope.frmOperDashBoardDispatch.ldtResult" class="its"
						id="currentRowObject" requestURI="/gmOperDashBoardDispatch.do?method=dashboardDHR" style="height:35" 
						decorator="com.globus.displaytag.beans.dashboard.DTDHRDashboardWrapper" freezeHeader="true" paneSize="400">
							<fmtDHR:message key="DT_VENDOR_NM" var="varVendorNm"/>
							<display:column property="VNAME" title="${varVendorNm}" group="1" style="width:220" />
							<fmtDHR:message key="DT_DHR_ID" var="varDHRId"/>
							<display:column property="ID" title="${varDHRId}"  style="width:270" />
							<fmtDHR:message key="DT_PART_NUM" var="varPartNum"/>							
							<display:column property="PARTNUM" title="${varPartNum}"  style="width:120"   />
							<fmtDHR:message key="DT_PART_DESC" var="varPDesc"/>
							<display:column property="PDESC" title="${varPDesc}"  maxWords="5"  style="width:420;align:right"/>
							<fmtDHR:message key="DT_CREATE_DT" var="varCreateDt"/>
							<display:column property="CDATEDIS" title="${varCreateDt}" style="width:120" sortable="true"  sortProperty="CDATE"/>
							<fmtDHR:message key="DT_QTY" var="varQty"/>
							<display:column property="TOT_QTY" title="${varQty}" class="alignright"/>
							<fmtDHR:message key="DT_PC" var="varPC"/>
							<display:column property="FL" title="${varPC}"  style="width:45" class="aligncenter"/>
							<fmtDHR:message key="DT_PI" var="varPI"/>							
							<display:column property="VID" title="${varPI}"  style="width:45;align:right" class="aligncenter"/>
							<fmtDHR:message key="DT_PP" var="varPP"/>
							<display:column property="CNUM" title="${varPP}"  style="width:45;align:right" class="aligncenter"/>
							<fmtDHR:message key="DT_PV" var="varPV"/>
							<display:column property="LDATE" title="${varPV}"  style="width:45;align:right" class="aligncenter"/>
							<fmtDHR:message key="DT_DHR" var="varDHRPriority"/>
							<display:column property="DHR_PRIORITY" title="${varDHRPriority}" sortable="true" class="alignright"/>
							</display:table>  </td>
							
					</tr>
			   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
<%

%>	


</BODY>

</HTML>

