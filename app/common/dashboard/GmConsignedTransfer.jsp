<%
/**********************************************************************************
 * File		 		: GmConsignedTransfer.jsp
 * Desc		 		: Dashboard - Consigned Transfer
 * Version	 		: 1.0
 * author			: Tarika Chandure
************************************************************************************/
%>
<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<!-- common\dashboard\GmConsignedTransfer.jsp -->
<%@ taglib prefix="fmtConsTrans" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtConsTrans:setLocale value="<%=strLocale%>"/>
<fmtConsTrans:setBundle basename="properties.labels.custservice.GmDashBoardHome"/>
<HTML>
<HEAD>
<% 
Object bean = pageContext.getAttribute("frmCustDashBoardDispatch", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>


<TITLE> Globus Medical: Dashboard - Consigned Transfer </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<bean:define id="gridData" name="frmCustDashBoardDispatch" property="ldtTransferResult" type="java.lang.String"> </bean:define>

<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>

<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>

<script>
 	var objGridData;
	objGridData = '<%=gridData%>';
	
	function fnOnPageLoad()
	{
		if (objGridData != '')
		{
			gridObj = initGrid('consigneddata',objGridData,'',[]);
			gridObj.enableTooltips("false,true,true,true,true");
		}

	}
	function fnOpenTransferLog(tID){
		windowOpener("/GmCommonLogServlet?hType=1209&hID="+tID,"TransLog","resizable=yes,scrollbars=yes,top=300,left=300,width=670,height=200");
		
	}
		
</script>
</HEAD>

<BODY leftmargin="0" topmargin="0" onLoad="fnOnPageLoad();">
<html:form action="/gmCustDashBoardDispatch.do">
	<html:hidden property="hAction" />
	<html:hidden property="hOrdId" />
	<html:hidden property="hConsignId" />
	<html:hidden property="hId" />
	<html:hidden property="hRAId" />
	<html:hidden property="hTransferId" />
	<html:hidden property="hMode" />
	<html:hidden property="hTransferType" />
	<table border="0" width="100%" cellspacing="0" cellpadding="0">

		<tr>
			<td width="100%" height="100" valign="top">
				<table border="0"  width="100%" cellspacing="0" cellpadding="0">
				
				<%if(!gridData.equals("")){%>
					<tr>
						<td colspan="6"> 
							<div  id ="consigneddata" style="height:650px;width:990;margin-left: 0;margin-top: 0;"></div>
						</td>
					</tr>
					<%}else { %>
					<tr>
						<td colspan="6"> 
							<fmtConsTrans:message key="MSG_NO_DATA_FOUND"/>
						</td>
					</tr>
					<%} %>
					</table>
  			   </td>
  		  </tr>	
    </table>		     	
</BODY>
</html:form>
</HTML>

