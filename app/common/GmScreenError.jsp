 <%@page import="java.util.ResourceBundle"%>
<%
/**********************************************************************************
 * File		 		: GmScreenError.jsp
 * Desc		 		: This screen is used to Display Database Error Message
 * Version	 		: 1.0 
 * author			: Dhinakaran James
************************************************************************************/
%>

<!--common\GmScreenError.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ include file="/common/GmHeader.inc" %>
<%

	String strMsgText 		= "";
	
    /** The resource bundle for the Application related error messages */
    ResourceBundle rbApp = ResourceBundle.getBundle("AppError");
	
try
{
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
	response.setDateHeader ("Expires", 0);

	String errString = (String)request.getAttribute("hErrorNumber") ;
	
	if(errString != null)
	{
		// If the Value not fount display the Actual Error Message
		try
		{
			strMsgText = rbApp.getString(errString);
		}
		catch(Exception e)
		{
			strMsgText = errString; 
		}
		
%>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" >
      	<tr>
        <td align="left" width="100%" class="RightTableCaption" height="25"><font
        	color="red">* <%=strMsgText%> </strong></font></td>
      	</tr>
<%		
	}
}
catch(Exception e)
{
 	e.printStackTrace();
}
%>

