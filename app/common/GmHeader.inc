<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %> 
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.Locale"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%@ page import="com.globus.valueobject.common.GmDataStoreVO"%> 
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%>

<% 
	Locale locale = null;
	String strLocale = "";
	String strJSLocale = "";
	
	Logger log = GmLogger.getInstance(GmCommonConstants.JSP);  // Instantiating the Logger  - write it to temp
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH"); 
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	String strWikiPath = GmCommonClass.getString("GWIKI");
	String strBrowserType = GmCommonClass.parseNull((String)session.getAttribute("strSessBrowserType"));
	String strClientSysType = GmCommonClass.parseNull((String)session.getAttribute("strSessClientSysType"));
	String strCountryCode = GmCommonClass.countryCode;	
	String strSwitchUserFl = GmCommonClass.parseNull((String)session.getAttribute("strSwitchUserFl"));
	String strExtWebPath = GmCommonClass.getString("GMEXTWEB");
	String strXmlPath =  GmCommonClass.getString("GMXML");
	//Getting Company Locale from session
	String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
	if(!strSessCompanyLocale.equals("")){
		locale = new Locale("en", strSessCompanyLocale);
	 	strLocale = GmCommonClass.parseNull((String)locale.toString());
	 	strJSLocale = "_"+strLocale;
	}
	//Getting company info 
	GmDataStoreVO gmDataStoreVO = (GmDataStoreVO)request.getAttribute("gmDataStoreVO");
	//If VO is null then get the default company info
	gmDataStoreVO = ((gmDataStoreVO == null)||((gmDataStoreVO.getCmpdfmt()).equals("")))? GmCommonClass.getDefaultGmDataStoreVO():gmDataStoreVO;
	//Assign to local variables. It will used in all JSPs.
	String strGCompTimeZone =gmDataStoreVO.getCmptzone();
	String strGCompDateFmt = gmDataStoreVO.getCmpdfmt();
	request.setCharacterEncoding("UTF-8");
	response.setContentType("text/html; charset=UTF-8");
	response.setCharacterEncoding("UTF-8");
	

%>
<html>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
	<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">
	<script language="JavaScript" src="<%=strJsPath%>/GmProgress.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/GmProgressNew.js"></script>
	<script language="JavaScript" src="<%=strJsPath%>/jquery.js?1"></script>
	<script type="text/javascript" src="<%=strJsPath%>/common/jquery-ui.js"></script> 
	<script language="JavaScript" src="<%=strJsPath%>/json2.js"></script>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
</html>
<script type="text/javascript">
	var GLOBAL_BrowserType = '<%=strBrowserType%>';
	var GLOBAL_ClientSysType = '<%=strClientSysType%>';
	var vCountryCode = '<%=strCountryCode%>';
	var SwitchUserFl = '<%=strSwitchUserFl%>';
	
	// To check whether an object is undefined or not
	function validateObject(object)
	{
	    var obj;
		obj = eval(object);
		if(obj!=undefined && obj!=null){
			return true;
		}else{
			return false;
		}
	}
	
	

</script>