 <%
/*******************************************************************************
 * File		 		: GmCommonCurrConv.jsp
 * Desc		 		: This screen displys currency conversion
 * Version	 		: 1.0
 * author			: Himanshu Patel
*******************************************************************************/
%>




<%@ page language="java" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtCommonCurrConv" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmCommonCurrConv.jsp -->
<fmtCommonCurrConv:setLocale value="<%=strLocale%>"/>
<fmtCommonCurrConv:setBundle basename="properties.labels.common.GmCommonCurrConv"/>  
 <bean:define id="strCurrValue" name="frmCommonCurrConvForm" property="currValue" type="java.lang.String"></bean:define>
 <bean:define id="strRateFlag" name="frmCommonCurrConvForm" property="monthRateFl" type="java.lang.String"></bean:define>
 <bean:define id="strEditSubmitBtnAccess" name="frmCommonCurrConvForm" property="strEditSubmitBtnAccess" type="java.lang.String"></bean:define>
<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);
String strWikiTitle = GmCommonClass.getWikiTitle("COMMON_CURRENCY_CONVERSION_SETUP");
String strApplDateFmt = strGCompDateFmt;
String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

String strCurrConvTransFl = "";
strCurrConvTransFl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CURR_CONV_BY_TRANS"));

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Enterprise Portal</TITLE>
<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonCurrConv.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script>
var format = '<%=strApplDateFmt%>';
var currConvTransFl = '<%=strCurrConvTransFl%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onload="fnOnLoad();">
 
<html:form action="/gmCommonCurrConv.do"  >
<html:hidden property="strOpt" />
<html:hidden property="currId" />
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VCURR"/>
<html:hidden property="hRateFlag" value="<%=strRateFlag%>"/>
<input type="hidden" name="hRedirectURL" >
<input type="hidden" name="hDisplayNm" >


<html:hidden property="hAction" value=""/> 
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtCommonCurrConv:message key="LBL_CURRENCY_CONVERSION"/></td>
			<td align="right" class="RightDashBoardHeader" colspan="7"> 	
			<fmtCommonCurrConv:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
			</td>			
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr class="evenshade">
		  <td class="RightTableCaption" align="right" colspan="2" height="25">
		  <fmtCommonCurrConv:message key="LBL_CURRENCY_FROM" var="varCurrencyFrom"/>
		      &nbsp;<gmjsp:currency type="MandatoryText"  label="${varCurrencyFrom}:" td="false"/>
		  </td>
		  <td colspan="2">
		      &nbsp;<gmjsp:dropdown controlName="currFrom" SFFormName="frmCommonCurrConvForm" SFSeletedValue="currFrom" SFValue="alCurrFrom" codeId = "CODEID"  codeName = "CODENMALT"  defaultValue= "[Choose One]" onChange="javascript:fnConvTypeChange();" tabIndex="1"/>
		  </td>
		  <td class="RightTableCaption" align="right" colspan="2">
		  <fmtCommonCurrConv:message key="LBL_CURRENCY_TO" var="varCurrencyTo"/>
		      &nbsp;<gmjsp:currency type="MandatoryText"  label="${varCurrencyTo}:" td="false"/>&nbsp;
		  </td>
		  <td colspan="2">
		      <gmjsp:dropdown controlName="currTo" SFFormName="frmCommonCurrConvForm" SFSeletedValue="currTo" SFValue="alCurrTo" codeId = "CODEID"  codeName = "CODENMALT"  defaultValue= "[Choose One]" onChange="javascript:fnConvTypeChange();" tabIndex="2"/>
		  </td>
		</tr>
		<tr><td colspan="8" class="LLine" height="1"></td></tr> 
		<tr class="oddshade">
            <td class="RightTableCaption" align="right" colspan="2">
            <fmtCommonCurrConv:message key="LBL_CONVERSION_TYPE" var="varConversionType"/>
                &nbsp;<gmjsp:currency type="MandatoryText"  label="${varConversionType}:" td="false"/>
            </td>
			<td colspan="2">
			    &nbsp;<gmjsp:dropdown controlName="convType" SFFormName="frmCommonCurrConvForm" SFSeletedValue="convType" SFValue="alConvType" codeId = "CODEID"  codeName = "CODENM"  defaultValue="[Choose One]" onChange="javascript:fnConvTypeChange();" tabIndex="3"/>
            </td>
            <td class="RightTableCaption" align="right" colspan="2" height="25">
            <fmtCommonCurrConv:message key="LBL_CURRENCY_VALUE" var="varCurrencyValue"/>
		        &nbsp;<gmjsp:currency type="MandatoryText"  label="${varCurrencyValue}:" td="false"/>&nbsp;
		    </td>
		    <td colspan="2">
		        <gmjsp:currency SFFormName="frmCommonCurrConvForm" controlName="currValue"  type="TextBox"  size="10"  onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" td="false" tabIndex="4"/>
	        </td>
        </tr>		
		<tr><td colspan="8" class="LLine" height="1"></td></tr> 
		<tr class="evenshade">  
			<td class="RightTableCaption" align="right" colspan="2" height="25">
			<fmtCommonCurrConv:message key="LBL_FROM_DATE" var="varFromDate"/>
			    &nbsp;<gmjsp:currency type="MandatoryText"  label="${varFromDate}:" td="false"/>
			</td>
			<td colspan="2">
			    &nbsp;<gmjsp:calendar SFFormName="frmCommonCurrConvForm" SFDtTextControlName="dtCurrFromDT" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/>&nbsp;
			</td>
			<td class="RightTableCaption" align="right" colspan="2">
			<fmtCommonCurrConv:message key="LBL_TO_DATE" var="varToDate"/>
			    &nbsp;<gmjsp:currency type="MandatoryText"  label="${varToDate}:" td="false"/>&nbsp;
			</td>		
			<td colspan="2">
			    <gmjsp:calendar SFFormName="frmCommonCurrConvForm" SFDtTextControlName="dtCurrToDT" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="6"/>&nbsp;                 		                                        		
           	</td>
		</tr>
		<!--  enabled only for the company having conversion by transaction -->
		<% if(strCurrConvTransFl.equals("YES")){ %>
		<tr class="oddshade">
			<td colspan="2" class="RightTableCaption" align="right"><font color="red">*</font>&nbsp;<fmtCommonCurrConv:message key="LBL_TRANSACTION_IDS"/>:</td>
			<td colspan="6">&nbsp;<html:textarea  property="transIds"  cols="50" style="height:70px" onkeyup="javascript:this.value=this.value.toUpperCase()"
			styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" /></td>
		</tr>
		<%} %>
        <tr><td colspan="8" class="LLine" height="1"></td></tr>
        <tr>
		    <td colspan="8">
	        	<jsp:include page="/common/GmIncludeLog.jsp" >
				<jsp:param name="FORMNAME" value="frmCommonCurrConvForm" />
				<jsp:param name="ALNAME" value="alLogReasons" />
				<jsp:param name="LogMode" value="Edit" />
				</jsp:include>
			</td>
        </tr>
        <tr>
            <td colspan="8" align="center" height="30">
                <fmtCommonCurrConv:message key="BTN_SUBMIT" var="varSubmit"/><gmjsp:button value="${varSubmit}" name="Btn_Submit" gmClass="button" buttonType="Save" onClick="fnSubmit();" disabled="<%=strEditSubmitBtnAccess %>"/> 
                <fmtCommonCurrConv:message key="BTN_VOID" var="varVoid"/><gmjsp:button value="${varVoid}" name="Btn_Void" gmClass="button" buttonType="Save" onClick="fnVoid();" disabled="<%=strEditSubmitBtnAccess %>"/>
                <fmtCommonCurrConv:message key="BTN_RESET" var="varReset"/><gmjsp:button value="${varReset}" name="Btn_Reset" gmClass="button" buttonType="Save" onClick="fnReset();" />
            </td>
        </tr>   		
    </table>		 
</html:form>
   <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>
