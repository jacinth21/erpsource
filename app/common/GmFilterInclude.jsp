


<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.Iterator,java.lang.*" %>
<%@ taglib prefix="fmtFilterInclude" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page isELIgnored="false" %>
<!-- GmFilterInclude.jsp -->
<fmtFilterInclude:setLocale value="<%=strLocale%>"/>
<fmtFilterInclude:setBundle basename="properties.labels.common.GmFilterInclude"/>  
<%

String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");

try {
	ArrayList alConditions = new ArrayList();
	ArrayList alOperators  = new ArrayList();
	ArrayList alCountries = new ArrayList();
	ArrayList alVendors = new ArrayList();
	ArrayList alSetList = new ArrayList();
	
	//ArrayList alRuleJoins = new ArrayList();
	ArrayList alRuleConditions = new ArrayList();
	String strRuleJoin = "";
	String strRuleJoinVal = "";
	String strSelCondition = "";
	String strRuleCond = "";
	String strOperator = "";
	String strValues = "";
	String strTxtValues = "";
	String strOperatorVal = "";
	String strDisabled = "";
	String strSelectedValues = "";
	int intTRCount = 3;
	ArrayList alConditionList = new ArrayList();
	HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
	HashMap hmConditionOpr = (HashMap)request.getAttribute("hmConditionOpr");
	Iterator itrConditionOpr = hmConditionOpr.keySet().iterator();	
	HashMap hmConditionValues = (HashMap)request.getAttribute("hmConditionValues");
	Iterator itrConditionValues = hmConditionValues.keySet().iterator();
	String strSelectedConditions = "";
	String strSelectedOperators ="";
	String strSelVal = "";
	String strAReport = GmCommonClass.parseNull(request.getParameter("aReport"));
	//System.out.println("strOpt " + strOpt);
	String strConHistoryfl= GmCommonClass.parseNull((String)request.getParameter("conhistoryfl"));
	//System.out.println("strConHistoryfl>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>..."+strConHistoryfl);
	String strRuleID= GmCommonClass.parseNull((String)request.getParameter("ruleId"));
	alConditions = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONDITIONS"));

	if (hmReturn != null)
	{
	  alRuleConditions = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALRULECONLIST"));
	}	
	if (alRuleConditions.size() > 0)
	{
		intTRCount = alRuleConditions.size();
		for(int i = 0 ; i < intTRCount; i++)
		{
			HashMap hmConditions = (HashMap)alRuleConditions.get(i);
			strSelCondition = (String)hmConditions.get("CONDITIONID");
			strSelectedConditions += strSelCondition + ",";
			strOperatorVal = (String)hmConditions.get("OPERATORID");
			strSelectedOperators += strOperatorVal + ",";
			strSelVal = (String)hmConditions.get("CONDITIONVAL");
			strSelectedValues += strSelVal + ";";
		}
	}
	//For loading the part# condition which has been drilled down from the process returns screen
	
	String loadRule = GmCommonClass.parseNull((String)request.getParameter("loadRule"));
	String strMainInputString = GmCommonClass.parseNull((String)request.getParameter("inputString"));
	strMainInputString = strMainInputString.indexOf("|") == -1 ? strMainInputString + "|" : strMainInputString;
	//System.out.println("0..."+strMainInputString);
	String strInputString[] = strMainInputString.split("\\|");
	
	//System.out.println("Array:"+strInputString[0]);
	
	int index = 0;
	String strTemp = "";
	if(loadRule.equals("true"))
	{
		for(int count = 0; count < strInputString.length; count++)
		{	
			index = strInputString[count].indexOf("^");
			strSelectedConditions += strInputString[count].substring(0,index) + ",";
			strTemp = strInputString[count].substring(index+1);
			
			index = strTemp.indexOf("^");
			strSelectedOperators += strTemp.substring(0,index) + ",";
			strTemp = strTemp.substring(index+1);
			
			index = strTemp.indexOf("^");
			strSelectedValues += strTemp.substring(0,index) + ";";
			
		}
		intTRCount = strInputString.length;
	}
%>

<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmFilter.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="javascript" src="<%=strJsPath%>/ajax.js"></script>
<script language="javascript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script type="text/javascript">
 var cnt = <%=intTRCount%>;
 var initialCnt = <%=intTRCount%>;
 var selectedConditions = '<%=strSelectedConditions%>';
 var selConArray = selectedConditions.split(",");
 var selectedOperators = '<%=strSelectedOperators%>';
 var selOprArray = selectedOperators.split(",");
 var selectedValues = '<%=strSelectedValues%>';
 var selValArray = selectedValues.split(";");
 
 function fnLoad()
 {
 cnt = 0;
	  for(rowCount =0; rowCount< initialCnt; rowCount++)
	    {
	        fnAddRow('RuleTable');
	        if(selConArray != '')
	        {
		    	var obj = eval("document.frmFilter.Cbo_Conditions"+rowCount);
				obj.value =selConArray[rowCount];
				fnloadOprValue(obj,rowCount);
				var objOpr = eval("document.frmFilter.Cbo_Operators"+rowCount);
				objOpr.value =selOprArray[rowCount];
				var objVal = eval("document.frmFilter.Cbo_Values"+rowCount);
				objVal.value =selValArray[rowCount];
				var objTxtVal = eval("document.frmFilter.Txt_Values"+rowCount);
				objTxtVal.value =selValArray[rowCount];
			}			
	    }
}
</script>

<BODY onLoad="fnLoad();">

<FORM name="frmFilter" method="POST" action="<%=strServletPath%>/GmFilterAjaxServlet">

<input type="hidden" name="hAction" value="">
<input type="hidden" name="hRowCnt" value="<%=intTRCount%>">
<Table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td>
			<div style="overflow:auto; height:207px;">
			<Table border="1" bordercolor="gainsboro" cellspacing="0" cellpadding="0" width="100%" id="RuleTable">
				<thead>
					<tr bgcolor="#EEEEEE" style="position:relative; top:expression(this.offsetParent.scrollTop);">
						<th nowrap class="RightText" align="left" height="25" width="35">&nbsp;
						<%  if ( strConHistoryfl.equals("Y") ) { %>
						<fmtFilterInclude:message key="LBL_CLICK_TO_OPEN_REQUIRED" var="varClickToOpenRequired"/>
												<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1014','<%=strRuleID %>');" title="${varClickToOpenRequired}"  
												src="<%=strImagePath%>/icon_History.gif" align="absmiddle"  height=15 width=18 />&nbsp;	 
											<% } %>	
						
						</th>
						<th nowrap class="RightText" align="left" height="25" width="148">&nbsp;<fmtFilterInclude:message key="LBL_CONDTION"/></th>
						<th nowrap class="RightText" align="left" width="95" height="25">&nbsp;<fmtFilterInclude:message key="LBL_OPERATOR"/></th>
						<th nowrap class="RightText" align="left" width="250" height="25">&nbsp;<fmtFilterInclude:message key="LBL_VALUE"/></th>			
					</TR>
				</thead>
				<tbody>
				
				</tbody>
			</table>
			</div>
		</td>			
	</tr>
	<tr><td class="Line" height="1"></td></tr>
	<tr>
		<td align="center" height="35">
			<fmtFilterInclude:message key="BTN_ADDROW" var="varAddRow"/>
			<fmtFilterInclude:message key="BTN_ANALYSISREPORT" var="varAnalysisReport"/>
			<fmtFilterInclude:message key="BTN_CLEARFILTER" var="varClearFilter"/>
			<gmjsp:button value="${varAddRow}" gmClass="button" buttonType="Save" onClick="fnAddRow('RuleTable');" />&nbsp;&nbsp;
			<gmjsp:button value="${varClearFilter}" gmClass="button" buttonType="Save" onClick="fnClearFilter();" />&nbsp;&nbsp;
			<% if(strAReport.equals("true")) { %>
			<gmjsp:button value="${varAnalysisReport}" gmClass="button" buttonType="Save" onClick="fnCallAnalysisReport();" />&nbsp;&nbsp;
		 	<% } %>
		</td>
	</tr>
</table>
<!-- Custom tag lib code modified for JBOSS migration changes -->
<div style="visibility:hidden;"  id="cboCon">
<gmjsp:dropdown onChange="fnloadOprValue" width="150" controlName="Cbo_Conditions" value="<%=alConditions%>" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
</div>
<div style="visibility:hidden;" id="cboOpr">
<gmjsp:dropdown controlName="Cbo_Operators" width="60" value="<%=alOperators%>" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
</div>

<div style="visibility:hidden;" id="cboVal">
<gmjsp:dropdown controlName="Cbo_Values" width="210" value="<%=alCountries%>" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
</div>

<%
 while(itrConditionOpr.hasNext())
 {
	 String strKey = (String)itrConditionOpr.next();
	 ArrayList alConOpr = (ArrayList) hmConditionOpr.get(strKey);
	 String strControlName = "Cbo_Con_Opr" + strKey;
	 String strID = "cboConOpr" + strKey;
%>
 <div style="visibility:hidden" id="<%=strID%>">
<gmjsp:dropdown controlName="<%=strControlName %>" width="60" value="<%=alConOpr%>" codeId="CODEID" codeName="CODENM"/>
</div>
<%
 }
%>
<%
 while(itrConditionValues.hasNext())
 {
	 String strValueKey = (String)itrConditionValues.next();
	 ArrayList alConValues = (ArrayList) hmConditionValues.get(strValueKey);
	 String strValueControlName = "Cbo_Con_Values" + strValueKey;
	 String strValueID = "cboConValues" + strValueKey;
%>
 <div style="visibility:hidden" id="<%=strValueID%>">
<gmjsp:dropdown controlName="<%=strValueControlName %>" width="60" value="<%=alConValues%>" codeId="CODEID" codeName="CODENM"/>
</div>
<%
 }
%>
<%
}catch(Exception e)
{
	e.printStackTrace();
}
%>

</FORM>
<%@ include file="/common/GmFooter.inc"%>
</BODY>