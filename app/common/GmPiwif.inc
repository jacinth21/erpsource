<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %> 
<%@ taglib uri="/WEB-INF/gmjsp-taglib.tld" prefix="gmjsp"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.Locale"%>
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 
<%@ page import="com.globus.common.beans.GmCommonClass"%> 
<%@ page import="com.globus.valueobject.common.GmDataStoreVO"%> 
<%@ page import="com.globus.common.beans.GmLogger"%> 
<%@ page import="org.apache.log4j.Logger"%>

<% 
	String strPiwikUrl = "";
	String strPiwikSiteId = "";
	Logger log_piwik = GmLogger.getInstance(GmCommonConstants.OPERATIONS);
	
	// Get property from JBOSS Server environment
	try{
	    strPiwikUrl    = System.getProperty("PIWIK_URL");
		strPiwikSiteId = System.getProperty("PIWIK_SITE_ID");   
		
		
	}catch(Exception e){
		//catch and just suppress error
	}
%>

<script type="text/javascript">

	var piwikURL	= '<%=strPiwikUrl%>';
	var piwikSiteID = '<%=strPiwikSiteId%>';

	
	 // Below code is used for PIWIK Service
	 // It tracks online visits to spineIT and displays reports on these visits for analysis
 	 // Author: KarthikS
 	 try{
 		console.log(piwikURL);
 		console.log(piwikSiteID);
	  	if(piwikURL != 'null' && piwikSiteID != 'null'){
		   var _paq = window._paq || []; 
		   /*tracker methods like "setCustomDimension" should be called before "trackPageView" */
		  _paq.push(['trackPageView']);
		  _paq.push(['enableLinkTracking']);
		  (function() {
		    var u=piwikURL;
		    _paq.push(['setTrackerUrl', u+'matomo.php']);
		    _paq.push(['setSiteId', piwikSiteID]);
		    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		   // g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
		      g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
		  })();
		}
	} catch(e){ 
		//catch and just suppress error
	 } 

</script>