



<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %> 
<%@ taglib prefix="fmtCommonCurrConvReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmCommonCurrConvReport.jsp -->
<fmtCommonCurrConvReport:setLocale value="<%=strLocale%>"/>
<fmtCommonCurrConvReport:setBundle basename="properties.labels.common.GmCommonCurrConvReport"/>  

<bean:define id="alSource" name="frmCommonCurrConvForm" property="alSource" type="java.util.ArrayList"> </bean:define>

<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);
String strWikiTitle = GmCommonClass.getWikiTitle("COMMON_CURRENCY_CONVERSION_REPORT");
String strApplDateFmt = strGCompDateFmt;
String strRptFmt = "{0,date,"+strApplDateFmt+"}";
String strCurrSign = GmCommonClass.parseNull((String)session.getAttribute("strSessCurrSymbol"));
String strCurrFmt = "{0,number,#,###,##0.000000}";

String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

String strCurrConvTransFl = "";
strCurrConvTransFl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CURR_CONV_BY_TRANS"));

%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Currency Conversion Report</TITLE>
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmCommonCurrConv.js"></script>
<script>
var format = '<%=strApplDateFmt%>';
var currConvTransFl = '<%=strCurrConvTransFl%>';
</script>
</HEAD>

<BODY onkeyup="fnEnter();" leftmargin="20" topmargin="10" >
 
<html:form action="/gmCommonCurrConv.do"  >
<html:hidden property="currId" />
<html:hidden property="strOpt" />
<html:hidden property="currValue" />
<html:hidden property="sourceInputStr" name="frmCommonCurrConvForm"/>

	<table border="0" class="DtTable900" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="11"><fmtCommonCurrConvReport:message key="LBL_CURRENCY_CONVERSION_REPORT"/></td>
			<td align="right" class="RightDashBoardHeader"> 	
			<fmtCommonCurrConvReport:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
				
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr>  
					<td class="RightTableCaption" align="right" height="25" colspan="2">&nbsp;<fmtCommonCurrConvReport:message key="LBL_CURRENCY_FROM"/>:</td>																
					
			<td HEIGHT="25" colspan="2">&nbsp;
							<gmjsp:dropdown controlName="currFrom" SFFormName="frmCommonCurrConvForm" SFSeletedValue="currFrom"
									SFValue="alCurrFrom" codeId = "CODEID"  codeName = "CODENMALT"  defaultValue= "[Choose One]" />
						</td>								
						<td class="RightTableCaption" align="right" width="0%" colspan="2">&nbsp;<fmtCommonCurrConvReport:message key="LBL_CURRENCY_TO"/>:</td>
						<td colspan="2">&nbsp;
							<gmjsp:dropdown controlName="currTo" SFFormName="frmCommonCurrConvForm" SFSeletedValue="currTo"
									SFValue="alCurrTo" codeId = "CODEID"  codeName = "CODENMALT"  defaultValue= "[Choose One]" />                    		                                        		
                      	</td>
                      	<td class="RightTableCaption" align="right" width="0%" colspan="2">&nbsp;<fmtCommonCurrConvReport:message key="LBL_CONVERSION_TYPE"/>:</td>
						<td colspan="2">&nbsp;
							<gmjsp:dropdown controlName="convType" SFFormName="frmCommonCurrConvForm" SFSeletedValue="convType"
									SFValue="alConvType" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />                    		                                        		
                      	</td>
					</tr>
				
		<tr><td colspan="12" class="LLine" height="1"></td></tr> 
		<tr class="Shade">  
					<td class="RightTableCaption" align="right" height="25" colspan="2">&nbsp;<fmtCommonCurrConvReport:message key="LBL_FROM_DATE"/>:</td>																
					
					<td colspan="2">&nbsp;
							<gmjsp:calendar SFFormName="frmCommonCurrConvForm" SFDtTextControlName="dtCurrFromDT" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
							<%-- 
							<html:text property="currFromDT"  size="10" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
						                    <img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmCommonCurrConvForm.currFromDT');" title="Click to open Calendar"  
								src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
							--%>
						</td>								
						<td class="RightTableCaption" align="right" colspan="2">&nbsp;<fmtCommonCurrConvReport:message key="LBL_TO_DATE"/>:</td>
						<td colspan="2">&nbsp;
							<gmjsp:calendar SFFormName="frmCommonCurrConvForm" SFDtTextControlName="dtCurrToDT" gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" />&nbsp;
							<%-- 
							<html:text property="currToDT"  size="10" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />
					                    <img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmCommonCurrConvForm.currToDT');" title="Click to open Calendar"  
							src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />
							--%>                    		                                        		
                      	</td>
                      	<td class="RightTableCaption" align="right" height="25" colspan="1">&nbsp;<fmtCommonCurrConvReport:message key="LBL_SOURCE"/>:</td>	
                      	<td colspan="1">&nbsp;
							<DIV style="display: visible; height: 65px; width:150px; overflow: auto; border-width: 1px; border-style: solid; margin-left: 1px; margin-right: 2px;">
									<table>
										  <logic:iterate id="selectedSource"
											name="frmCommonCurrConvForm" property="alSource">
											<tr>
												<td><htmlel:multibox property="checkSelectedSource" value="${selectedSource.CODEID}" />
													 <bean:write name="selectedSource" property="CODENM" /></td>
											</tr>
										</logic:iterate>
									</table>
								</DIV>
                      	</td>
                      	<td align="center" height="30" colspan="2">
                      	<fmtCommonCurrConvReport:message key="BTN_LOAD" var="varLoad"/>
								<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Submit" gmClass="button" buttonType="Load" onClick="fnRptReload();" />						
						</td>
					</tr>
				
		 <tr><td class="LLine" height="1" colspan="12"></td></tr>
		
		<tr><td colspan="12"></td></tr>
	      	<tr>
		<td colspan="12" width="100%">
		<display:table name="requestScope.frmCommonCurrConvForm.alCurrConv" freezeHeader="true"  export="true" class="its" requestURI="/gmCommonCurrConv.do" id="currentRowObject" decorator="com.globus.common.displaytag.beans.DTCommonCurrConvWrapper" >
         <display:setProperty name="export.excel.filename" value="gmCommonCurrConv.xls" />    
 		<display:column property="CURRID" title = "" style="width=50" class="alignleft"/>
 		<fmtCommonCurrConvReport:message key="LBL_CURR_FROM" var="varCurrFrom"/><display:column property="CURRFROM" title="${varCurrFrom}" class="alignleft" sortable="true"/>
 		<fmtCommonCurrConvReport:message key="LBL_CURR_TO" var="varCurrTo"/><display:column property="CURRTO" title="${varCurrTo}" class="alignleft" sortable="true"/>
		<fmtCommonCurrConvReport:message key="LBL_FROM_DATE" var="varFromDate"/><display:column property="DTCURRFROMDT" title="${varFromDate}" class="alignleft" sortable="true" format= "<%=strRptFmt%>"/>
		<fmtCommonCurrConvReport:message key="LBL_TO_DATE" var="varToDate"/><display:column property="DTCURRTODT" title="${varToDate}" class="alignleft"  sortable="true"  format="<%=strRptFmt%>"/>
		<fmtCommonCurrConvReport:message key="LBL_CONVERSION_TYPE" var="varConversionType"/><display:column property="CONVTP" title = "${varConversionType}" class="alignleft" sortable="true"/>
		<fmtCommonCurrConvReport:message key="LBL_SOURCE" var="varSource"/><display:column property="SOURCE" title = "${varSource}" class="alignleft" sortable="true"/>
		<fmtCommonCurrConvReport:message key="LBL_AMOUNT" var="varAmount"/>	<display:column property="CURRVALUE" title="${varAmount}" class="alignright" format="<%=strCurrFmt%>" sortable="true" />
		<!--  enabled only for the company having conversion by transaction -->
		<%if(strCurrConvTransFl.equals("YES")){ %>
		<fmtCommonCurrConvReport:message key="LBL_TRANS_COUNT" var="varTransCnt"/><display:column property="TRANSCNT" title = "${varTransCnt}" class="aligncenter"/>
		<%} %>
		</display:table>
		</td>
		</tr> 
 </table>
  			   
</html:form>
   <%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>