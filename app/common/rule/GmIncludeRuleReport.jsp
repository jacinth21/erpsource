<%
	/**********************************************************************************
	 * File		 		: GmIncludeAddressInfo.jsp
	 * Desc		 		: Address - include page
	 * Version	 		: 1.0
	 * Author			: Brinal G
	 * Last Modified By	: Satyajit Thadeshwar
	 ************************************************************************************/
%>



<%@ include file="/common/GmHeader.inc" %>
<!-- common\rule\GmIncludeRuleReport.jsp -->
<%@ taglib prefix="fmtIncRuleRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncRuleRpt:setLocale value="<%=strLocale%>"/>
<fmtIncRuleRpt:setBundle basename="properties.labels.common.rule.GmRuleReport"/>
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<%@ page language="java"%>
<display:table name="requestScope.frmRuleReport.returnList" requestURI="/gmRuleReport.do"   class="its" id="currentRowObject" decorator="com.globus.common.displaytag.beans.DTRuleRptWrapper"  >
				<fmtIncRuleRpt:message key="DT_RULE_NAME" var="varRuleNm"/>
				<display:column property="NAME" title="${varRuleNm}"  style="width:235px" />
				<fmtIncRuleRpt:message key="DT_INIT_BY" var="varInitBy"/>
 		 		<display:column property="INITBY" title="${varInitBy}"  />
 		 		<fmtIncRuleRpt:message key="DT_INIT_DT" var="varInitDt"/>
				<display:column property="INITDATE" title="${varInitDt}"  class="aligncenter" />
				<fmtIncRuleRpt:message key="DT_FLTR_COND" var="varFilCon"/>
				<display:column property="CONDITION" title="${varFilCon}" style="width: 300px;" class="alignleft" />
				<fmtIncRuleRpt:message key="DT_EXEC_CNT" var="varExeCnt"/>
				<display:column property="LOG_CNT" title="${varExeCnt}" class="alignright" />				  
</display:table> 
    
    
