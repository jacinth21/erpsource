<%
/**********************************************************************************
 * File		 		: GmRuleReport.jsp 
 * Desc		 		: Rule Report Screen
 * Version	 		: 1.0
 * author			: Xun Qu
************************************************************************************/
%>




<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.text.DecimalFormat" %>
<!-- common\rule\GmRuleReport.jsp -->
<%@ taglib prefix="fmtRuleRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtRuleRpt:setLocale value="<%=strLocale%>"/>
<fmtRuleRpt:setBundle basename="properties.labels.common.rule.GmRuleReport"/>

<bean:define id="returnList" name="frmRuleReport" property="returnList" type="java.util.List"></bean:define>
<bean:define id="hmParam" name="frmRuleReport" property="hmParam" type="java.util.HashMap"></bean:define> 
<bean:define id="loadRule" name="frmRuleReport" property="loadRule" type="java.lang.String"></bean:define> 
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("RULE_REPORT");
    //System.out.println("load RUle = "+ loadRule+" hmParam = "+hmParam.size());
    String strInputString  = GmCommonClass.parseNull((String)hmParam.get("INPUTSTR"));
    
    //The following code added for passing the company info to the child js fnFilterLoad function
  	String strCompanyId = gmDataStoreVO.getCmpid();
  	String strPlantId = gmDataStoreVO.getPlantid();
  	String strPartyId = gmDataStoreVO.getPartyid();
  	
  	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
  	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
  	String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Rule Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"
	media="print" />

<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmFilter.js"></script> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
<script>
var companyInfoObj = '<%=strCompanyInfo%>';
function enterPressed(evn) {
if (window.event && window.event.keyCode == 13) {
  fnReload();
} else if (evn && evn.keyCode == 13) {
  fnReload();
}
}
document.onkeypress = enterPressed;

function fnReload()
{     
	var str = '';
	var txnId,initiatedBy,consequenceId,status;
	//fnValidateDropDn('txnId',' Transaction '); 
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	str	= document.getElementById('frmFilter').contentWindow.fnCreateCondtionString('report');
	txnId = document.frmRuleReport.txnId.value;
	initiatedBy = document.frmRuleReport.initiatedBy.value;
	consequenceId = document.frmRuleReport.consequenceId.value;
	status =  document.frmRuleReport.status.value; 
	loadajaxpage('/gmRuleReport.do?companyInfo='+companyInfoObj+'&inputStr='+str+'&strOpt=reload&txnId='+txnId+'&initiatedBy='+initiatedBy+'&consequenceId='+consequenceId+'&status='+status,'ajaxreportarea');	 
}	

function fnCallRuleEdit(strRuleId)
 { 
       
   //   windowOpener("gmRuleCondition.do?strOpt=edit&ruleId="+strRuleId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=820,height=700");
   document.frmRuleReport.action = "gmRuleCondition.do?strOpt=edit&ruleId="+strRuleId;
   document.frmRuleReport.submit();
  }
    
 function fnCallRuleSummary(strRuleId)
 { 
       
    windowOpener("gmRuleReport.do?strOpt=summary&txnId="+strRuleId,"PrntInv","resizable=yes,scrollbars=yes,top=250,left=300,width=820,height=400");
    
    }    
/*
function setEvent() {
document.getElementById('frmFilter').contentWindow.document.onkeypress = new Function ('enterPressed(event)');
}
 */ 
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmRuleReport.do">
	<html:hidden property="strOpt" value="" /> 
	<html:hidden property="inputStr" value="" /> 
	<html:hidden property="ruleId" value=""/>  
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtRuleRpt:message key="LBL_RULE_RPT"/></td>
			<fmtRuleRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
			<td  height="25" class="RightDashBoardHeader" align="right">
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr><td colspan="6" class="LLine" height="1"></td></tr> 
		<tr>
			<td width="698" valign="top" colspan="6">
			<!-- Custom tag lib code modified for JBOSS migration changes -->
				<table border="0" width="100%" cellspacing="0" cellpadding="0">  
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtRuleRpt:message key="LBL_TRANSCTION"/> :</td> 
                        <td>&nbsp;
	                        <gmjsp:dropdown controlName="txnId"	SFFormName="frmRuleReport" SFSeletedValue="txnId"
								SFValue="alTransaction" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENM" />
	                    </td> 
                         <td class="RightTableCaption" align="right" HEIGHT="24"> &nbsp;<fmtRuleRpt:message key="LBL_INIT_BY"/> :</td> 
				                     <td>&nbsp;<gmjsp:dropdown controlName="initiatedBy"	SFFormName="frmRuleReport" SFSeletedValue="initiatedBy"
								SFValue="alInitiatedBy" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENM" />
								</td>
                    </tr>       
                    <tr><td colspan="6" class="LLine" height="1"></td></tr> 
                    <tr class="Shade">
                        <td class="RightTableCaption" align="right" HEIGHT="24"></font>&nbsp;<fmtRuleRpt:message key="LBL_CONSEQUENCE"/> :</td> 
	                    <td>&nbsp;&nbsp;<gmjsp:dropdown controlName="consequenceId"	SFFormName="frmRuleReport" SFSeletedValue="consequenceId"
							SFValue="alConsequence" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENM" />
					 	</td>
    		            <td class="RightTableCaption" align="right" HEIGHT="24"> &nbsp;<fmtRuleRpt:message key="LBL_STATUS"/> :</td> 
	                    <td>&nbsp;<gmjsp:dropdown controlName="status"	SFFormName="frmRuleReport" SFSeletedValue="status"
							SFValue="alStatus" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENM" />
							<fmtRuleRpt:message key="BTN_LOAD" var="varLoad"/>
							&nbsp;&nbsp;<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnReload();" />
						</td>						
					</tr>
				</table>
             </td>
        </tr>        
        <tr><td colspan="6" class="LLine" height="1"></td></tr>
        <tr><td colspan="6" height="3"></td></tr>      
		<tr>
			<td colspan="6" align="center">				
				<fmtRuleRpt:message key="BTN_NEW_RULE" var="varNewRule"/>
				<gmjsp:button value="${varNewRule}" gmClass="button" buttonType="Save" onClick="fnNewRule(this.form);" />
			</td>
		</tr>
		 <tr><td colspan="6" height="3"></td></tr>
	 	<tr>
			<td colspan="6">
				<iframe src="/GmFilterAjaxServlet?aReport=false&loadRule=<%=loadRule%>&inputString=<%=strInputString %>" scrolling="no" id="frmFilter" marginheight="0" width="100%" height="250"></iframe><BR>
			</td>
		</tr>		
		<tr>
			<td class="Line" height="1" colspan="6"></td>
		</tr>
		<tr><td colspan ="6" align=center> 
				 <div id="ajaxreportarea" class="contentstyle" style="display:visible;height: 130px; ">
				<table>
				<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
					<tr> <td>
							<jsp:include page= "/common/rule/GmIncludeRuleReport.jsp" />
						</td>
					</tr>
				</table>
			 </div>
		</td></tr>		 
	</table>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>