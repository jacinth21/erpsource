<%
/**********************************************************************************
 * File		 		: GmRuleAnalysisReport.jsp 
 * Desc		 		: Analysis Rule Report Screen
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>




<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="java.text.DecimalFormat" %>
<!-- common\rule\GmRuleAnalysisReport.jsp -->
<%@ taglib prefix="fmtRuleAnalysisRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtRuleAnalysisRpt:setLocale value="<%=strLocale%>"/>
<fmtRuleAnalysisRpt:setBundle basename="properties.labels.common.rule.GmRuleReport"/>

<bean:define id="returnList" name="frmRuleReport" property="returnList" type="java.util.List"></bean:define>
<bean:define id="hmParam" name="frmRuleReport" property="hmParam" type="java.util.HashMap"></bean:define> 
<bean:define id="loadRule" name="frmRuleReport" property="loadRule" type="java.lang.String"></bean:define> 
<% 
	String strWikiTitle = GmCommonClass.getWikiTitle("RULE_REPORT");
    //System.out.println("load RUle = "+ loadRule+" hmParam = "+hmParam.size());
   // String strInputString  = GmCommonClass.parseNull((String)hmParam.get("INPUTSTR"));
    String strInputString = GmCommonClass.parseNull((String)request.getParameter("inputString"));
    String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Rule Report</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css"
	media="print" />

<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmFilter.js"></script> 
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
<script>
function enterPressed(evn) {
if (window.event && window.event.keyCode == 13) {
  fnReload();
} else if (evn && evn.keyCode == 13) {
  fnReload();
}
}
document.onkeypress = enterPressed;

function fnReload()
{     
	var str = '';

	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	str	= document.frames.frmFilter.fnCreateCondtionString('report');
	repType = document.frmRuleReport.reportType.value;
	loadajaxpage('/gmRuleAnalysisReport.do?reportType='+repType+'&inputString='+str+'&strOpt=reloadAnalysisReport','ajaxreportarea');	 
}	
</script>
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmRuleAnalysisReport.do">
	<html:hidden property="strOpt" value="" /> 
	<html:hidden property="inputStr" value="" /> 
	<html:hidden property="ruleId" value=""/>  
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader" colspan="4"><fmtRuleAnalysisRpt:message key="LBL_RULE_ANALYSIS_RPT"/></td>
			<fmtRuleAnalysisRpt:message key="IMG_ALT_HELP" var = "varHelp"/>
			<td  height="25" class="RightDashBoardHeader" align="right">
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		<tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
                        <td class="RightTableCaption" align="left" HEIGHT="24" >&nbsp;<fmtRuleAnalysisRpt:message key="LBL_TRANSCTION"/> :
	                        <gmjsp:dropdown controlName="reportType"	SFFormName="frmRuleReport" SFSeletedValue="reportType"
								SFValue="alreportType" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENM" />
								<fmtRuleAnalysisRpt:message key="BTN_LOAD" var="varLoad"/>
								&nbsp;<gmjsp:button value="&nbsp;&nbsp;${varLoad}&nbsp;&nbsp;" gmClass="button" buttonType="Load" onClick="fnReload();" />
	                    </td> 
                      
                    </tr>   
		<tr><td colspan="6" class="LLine" height="1"></td></tr> 		
	 	<tr>
			<td colspan="6">
				<iframe src="/GmFilterAjaxServlet?aReport=false&loadRule=<%=loadRule%>&inputString=<%=strInputString %>" scrolling="no" id="frmFilter" marginheight="0" width="100%" height="250"></iframe><BR>
			</td>
		</tr>		
		<tr>
			<td class="Line" height="1" colspan="6"></td>
		</tr>
		<tr><td colspan ="6" align=center> 
				 <div id="ajaxreportarea" class="contentstyle" style="display:visible;height: 130px; ">
				<table>
					<tr> <td>
					<!-- Include Tag closed for JBOSS migration changes as there is no parameter -->
							<jsp:include page= "/common/rule/GmIncludeRuleAnalysisReport.jsp" />
						</td>
					</tr>
				</table>
			 </div>
		</td></tr>	
		 
	</table>
	 
</html:form>
 <%@ include file="/common/GmFooter.inc" %>
</BODY>

</HTML>