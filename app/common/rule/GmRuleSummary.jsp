 <%@page import="java.sql.Date"%>
<%
/**********************************************************************************
 * File		 		: GmRuleSummary.jsp
 * Desc		 		: This screen is used for displaying rule summary 
 * Version	 		: 1.0
 * author			: Xun
************************************************************************************/
%>




<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%> 
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<!-- common\rule\GmRuleSummary.jsp -->
<%@ taglib prefix="fmtRuleSummary" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<fmtRuleSummary:setLocale value="<%=strLocale%>"/>
<fmtRuleSummary:setBundle basename="properties.labels.common.rule.GmRuleReport"/>

<bean:define id="hmRuleDtls" name="frmRuleReport"  property="hmRuleDetails" type="java.util.HashMap"></bean:define> 
<bean:define id="hmConsequence" name="frmRuleReport"  property="hmConsequences" type="java.util.HashMap"></bean:define> 
<bean:define id="returnConditionList" name="frmRuleReport" property="returnConditionList" type="java.util.List"></bean:define>
<bean:define id="returnTransactionList" name="frmRuleReport" property="returnTransactionList" type="java.util.List"></bean:define>
<%   
String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
 String strWikiTitle = GmCommonClass.getWikiTitle("RULE_SUMMARY");
 String strRuleName = (String) hmRuleDtls.get("RULE_NAME");
 String strSessDtFormat = strGCompDateFmt;
 String strExpireDate = (String) GmCommonClass.getStringFromDate((Date)hmRuleDtls.get("EXPIRY_DATE"),strSessDtFormat);
 //String strExpireDate =  (String) hmRuleDtls.get("EXPIRY_DATE");
 String strInitiatedBy = (String) hmRuleDtls.get("INIT_BY");
 String strInitiatedDate = (String) hmRuleDtls.get("INIT_DATE"); 
 String strEmailFL = (String) hmRuleDtls.get("EMAIL_FL");
 String strComment = (String) hmRuleDtls.get("RULE_COMMENT");
 String strUpdatedBy = (String) hmRuleDtls.get("UPDATED_BY");
 String strUpdatedDate = (String) hmRuleDtls.get("UPDATED_DATE");
 String strActiveFl = GmCommonClass.parseNull((String) hmRuleDtls.get("ACTIVEFL"));
 strActiveFl = strActiveFl.equals("Y")?"Active":"Inactive";
 String strExpHistoryfl = GmCommonClass.parseNull((String)hmRuleDtls.get("EXP_HISFL"));
 String	strActiveHistoryfl = GmCommonClass.parseNull((String)hmRuleDtls.get("ACT_HISFL"));
 String	strTxnHistoryfl = GmCommonClass.parseNull((String)hmRuleDtls.get("TXN_HISFL"));
 String	strConHistoryfl = GmCommonClass.parseNull((String)hmRuleDtls.get("CON_HISFL"));
 String	strHoldTxnHistoryfl = GmCommonClass.parseNull((String)hmRuleDtls.get("HOLDTXN_HISFL"));
 
 String strMessage = (String) hmConsequence.get("MESSAGE");
 String strPicture = (String) hmConsequence.get("PICTURE");
 String strHoldTxn = (String) hmConsequence.get("HOLD_TXN");
 String strEmailTo = (String) hmConsequence.get("EMAIL_TO");
 String strEmailSubject = (String) hmConsequence.get("EMAIL_SUB");
 String strEmailBody = (String) hmConsequence.get("EMAIL_BODY");
  
 String strRuleID = (String) request.getParameter("txnId");	
 
 GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.common.rule.GmRuleReport", strSessCompanyLocale);
 
String strCondition ="";
String strTransaction ="";
String strLblCondition = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_CONDITIONS"));
String strRuleHistImgAlt = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("IMG_ALT_RULE_HIST"));
String strLblTransaction = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("LBL_TRANSACTION"));
if ( strConHistoryfl.equals("Y") ) 
{ 
	strCondition = ""+strLblCondition+" <img  id='imgEdit' style='cursor:hand' onclick=javascript:fnLoadHistory(1014,"+strRuleID+"); title='"+strRuleHistImgAlt+"'src='"+strImagePath+"/icon_History.gif' align='bottom'  height=15 width=18 />";	
		//strCondition = "Conditions";	
}
else
{
		strCondition = strLblCondition;
}	
if ( strTxnHistoryfl.equals("Y") ) 
{ 
	strTransaction = ""+strLblTransaction+" <img  id='imgEdit' style='cursor:hand' onclick=javascript:fnLoadHistory(1015,"+strRuleID+"); title='"+strRuleHistImgAlt+"' src='"+strImagePath+"/icon_History.gif' align='bottom'  height=15 width=18 />";	
		//strCondition = "Conditions";	
}
else
{
	strTransaction = strLblTransaction;
} 
 
%>
 
<HTML>
<HEAD>
<TITLE> Globus Medical: Rule Summary</TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
 <script>
function fnOpenPicture(ruleId)
{
  windowOpener('/gmRuleEngine.do?RE_FORWARD=gmRulePictureDisplay&strOpt=RE_SHOWPICTURE&RULEID='+ruleId,"PICTURES","resizable=yes,scrollbars=yes,top=150,left=150,width=760,height=610");
} 
</script> 
</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmRuleReport.do">
	 
	<table border="0" width="650" cellspacing="0" cellpadding="0">
		<tr>
			<td rowspan="30" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="30" width="1" class="Line"></td>
		</tr>
		<tr>
			<td  height="25" class="RightDashBoardHeader">
				<fmtRuleSummary:message key="LBL_RULE_SUMMARY"/> </td>	
			<fmtRuleSummary:message key="IMG_ALT_HELP" var = "varHelp"/>
			<td  height="25" class="RightDashBoardHeader" align="right"><img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       			height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>
		<tr><td class="Line" height="1" colspan="2"></td></tr>
		 
		<tr>
			<td colspan="2">
				<table border="0" width="100%" bgcolor="#CCCCCC" cellspacing="0" cellpadding="0">
					<tr height="24" bgcolor="#eeeeee" class="ShadeRightTableCaption">
						<td align="left" colspan="4" ><fmtRuleSummary:message key="LBL_RULE_DTLS"/>
						
						</td>
						 
					</tr>
					<tr height="16" bgcolor="white" >
						<td align="right" class="RightText" width="150"  ><b><fmtRuleSummary:message key="LBL_RULE_NM"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strRuleName%>&nbsp;</td>
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_EXP_DT"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strExpireDate%>&nbsp;
						<%  if ( strExpHistoryfl.equals("Y") ) { %>
									<fmtRuleSummary:message key="IMG_ALT_REQ_DT_HIST" var="varReqHist"/>
									<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1011','<%=strRuleID %>');" title="${varReqHist}"  
									src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />&nbsp;	 
								<% } %>
						</td>
					</tr>
					<tr height="16" bgcolor="white" class=ShadeBlueBk>
						<td align="right" class="RightText" width="150" ><b><fmtRuleSummary:message key="LBL_INIT_BY"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strInitiatedBy%>&nbsp;</td>
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_INIT_DT"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strInitiatedDate%>&nbsp;</td>
					</tr>
					<tr height="16" bgcolor="white">
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_EMAIL_FL"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strEmailFL%>&nbsp;</td>
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_RULE_DESC"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strComment%>&nbsp;</td>
					</tr>
					<tr height="16" bgcolor="white" class=ShadeBlueBk>
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_UPD_BY"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strUpdatedBy%>&nbsp;</td>
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_UPD_DT"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strUpdatedDate%>&nbsp;</td>
					</tr>
					<tr height="16" bgcolor="white">
						<td  align="right" class="RightText"><b><fmtRuleSummary:message key="LBL_STATUS"/>:</b>&nbsp;&nbsp;</td>
						<td colspan="3" class="RightText">&nbsp;<%=strActiveFl%>&nbsp;
						<%  if ( strActiveHistoryfl.equals("Y") ) { %>
							<fmtRuleSummary:message key="IMG_ALT_RULE_STATUS_HIST" var="varRuleStatus"/>
							<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1012','<%=strRuleID %>');" title="${varRuleStatus}"  
							src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />&nbsp;	
							
							<% } %>
						</td>						
					</tr>
					 						
				</table>
				
				<display:table name="requestScope.frmRuleReport.returnConditionList" requestURI="/gmRuleReport.do"   class="its" id="currentRowObject" decorator="com.globus.common.displaytag.beans.DTRuleSummaryWrapper">
				<display:column property="CONDITIONS" title="<%=strCondition%>"/>			  
				</display:table>
				
				<display:table name="requestScope.frmRuleReport.returnTransactionList" requestURI="/gmRuleReport.do"   class="its" id="currentRowObject" >
				<display:column property="TRANSACTIONS" title="<%=strTransaction%>"    />			  
				</display:table>
				
				<table border="0" width="100%" bgcolor="#CCCCCC" cellspacing="1" cellpadding="0">
					<tr height="24" bgcolor="#eeeeee" class="ShadeRightTableCaption">
						<td align="left" colspan="4"><fmtRuleSummary:message key="LBL_CONSEQUENCES"/></td> 
		 	 
					</tr>
					<tr height="16" bgcolor="white">
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_MESSAGE"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strMessage%>&nbsp;</td> 
					</tr>
					
					<tr height="16" bgcolor="white" class=ShadeBlueBk> 
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_PICTURE"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%if (strPicture.equals("Y")) {  %>  <a href=javascript:fnOpenPicture('<%=strRuleID %>')> <img src=<%=strImagePath%>/product_icon.jpg ></a><% } %>&nbsp;</td>
					</tr>
					
					<tr height="16" bgcolor="white"> 
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_HOLD_TXN"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strHoldTxn%>&nbsp;
						<%  if ( strHoldTxnHistoryfl.equals("Y") ) { %>
									<fmtRuleSummary:message key="IMG_ALT_REQ_TXN_HIST" var="varReqTxn"/>
									<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1016','<%=strRuleID %>');" title="${varReqTxn}"  
									src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />&nbsp;	
									
									<% } %>
						</td>
					</tr>
					
					<tr height="16" bgcolor="white" class=ShadeBlueBk> 
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_EMAIL_TO"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strEmailTo%>&nbsp;</td>
					</tr>
					
					<tr height="16" bgcolor="white"> 
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_EMAIL_SUB"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strEmailSubject%>&nbsp;</td>
					</tr>
					
					<tr height="16" bgcolor="white" class=ShadeBlueBk> 
						<td align="right" class="RightText" width="150"><b><fmtRuleSummary:message key="LBL_EMAIL_BDY"/>:</b>&nbsp;&nbsp;</td>
						<td class="RightText">&nbsp;<%=strEmailBody%>&nbsp;</td>
					</tr>
					 			
				</table>
			</td>
			
		</tr> 
		<tr>
			<td rowspan="30" width="1" class="Line"></td>
			<td colspan="2" height="1" bgcolor="#666666"></td>
			<td rowspan="30" width="1" class="Line"></td>
		</tr>
		 
	</table>
		
</html:form>
 
</BODY>

</HTML>
