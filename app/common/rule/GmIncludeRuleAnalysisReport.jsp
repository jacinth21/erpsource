<%
	/**********************************************************************************
	 * File		 		: GmIncludeAddressInfo.jsp
	 * Desc		 		: Address - include page
	 * Version	 		: 1.0
	 * Author			: Brinal G
	 * Last Modified By	: Satyajit Thadeshwar
	 ************************************************************************************/
%>



<%@ include file="/common/GmHeader.inc" %>
<!-- common\rule\GmIncludeRuleAnalysisReport.jsp -->
<%@ taglib prefix="fmtIncAnalysisRuleRpt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtIncAnalysisRuleRpt:setLocale value="<%=strLocale%>"/>
<fmtIncAnalysisRuleRpt:setBundle basename="properties.labels.common.rule.GmRuleReport"/>

<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<%@ page language="java"%>
<display:table name="requestScope.frmRuleReport.returnList" requestURI="/gmRuleAnalysisReport.do"   class="its" id="currentRowObject" decorator="com.globus.common.displaytag.beans.DTRuleRptWrapper"  >
				<fmtIncAnalysisRuleRpt:message key="DT_CONS_ID" var="varConsId"/>
				<display:column property="ID" title="${varConsId}"  style="width:235px" />
				<fmtIncAnalysisRuleRpt:message key="DT_PART_NUM" var="varPNum"/>
 		 		<display:column property="PNUM" title="${varPNum}"  />
 		 		<fmtIncAnalysisRuleRpt:message key="DT_CTRL_NUM" var="varCNum"/>
				<display:column property="CNUM" title="${varCNum}"  class="alignleft" />
				<fmtIncAnalysisRuleRpt:message key="DT_QTY" var="varQty"/>
				<display:column property="QTY" title="${varQty}" class="alignleft" />
				<fmtIncAnalysisRuleRpt:message key="DT_SET_ID" var="varSetId"/>
				<display:column property="SET_ID" title="${varSetId}" class="alignright" />				  
</display:table> 
    
    
