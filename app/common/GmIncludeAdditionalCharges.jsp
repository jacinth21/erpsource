 <%
/**********************************************************************************
 * File		 		: GmIncludeAdditionalCharges.jsp
 * Desc		 		: 
 *					  for party common search page.
 * Version	 		: 1.0
 * author			: Satyajit Thadeshwar
************************************************************************************/
%>




<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Iterator,com.globus.common.forms.GmAdditionalChargesForm" %>
<%@ taglib prefix="fmtIncludeAdditionalCharges" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmIncludeAdditionalCharges.jsp -->
<fmtIncludeAdditionalCharges:setLocale value="<%=strLocale%>"/>
<fmtIncludeAdditionalCharges:setBundle basename="properties.labels.common.GmIncludeDateStamp"/>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Filter</title>

<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GlobusParty.css">
<script language="JavaScript" src="<%=strJsPath%>/GmLoanerReconciliation.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
</head>
<body>
<table  width="100%" cellspacing="0" cellpadding="0">
	<tr height="24" class="ShadeRightTableCaption">
		<td align="left" class="RightTableCaption" colspan="2">&nbsp;<fmtIncludeAdditionalCharges:message key="LBL_CHARGES"/></td>
	</tr>
	<tr><td colspan="2" class="Line" height="1"></td></tr>
	<%
	//GmAdditionalChargesForm form = (GmAdditionalChargesForm)pageContext.getAttribute("frmAdditionalCharges", PageContext.REQUEST_SCOPE);
	ArrayList alFields = GmCommonClass.parseNullArrayList((ArrayList)(request.getAttribute("alReturn")));
	HashMap hmCodeList = GmCommonClass.parseNullHashMap((HashMap)(request.getAttribute("hmCodeList")));
		
	
	HashMap hmField = null;
	int i=0;
	for (Iterator iter = alFields.iterator(); iter.hasNext();)
	{
		hmField = (HashMap)iter.next();
		String fieldId = (String)hmField.get("CODEID");
		String fieldLabel = (String)hmField.get("CODENM");
		String fieldType = (String)hmField.get("CONTROLTYP");
		String fieldAttr = (String)hmField.get("CODENMALT");
		String styleClass = "";
		String strName = "charge" + i;
		String strHName = "hcharge" + i;
		if(i%2 == 0)
			styleClass = "None";
		else
			styleClass = "Shade";
	%>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr height="24" class="<%=styleClass%>">
		<td class="RightTableCaption" align="right" width="30%"><%=fieldLabel%>&nbsp;:&nbsp;</td>
		<td align="left" width="70%">
		<input type="hidden" id="<%=strHName%>" name="<%=strHName%>" value="<%=fieldId%>"/>
	<% 
		if(fieldType.equals("TEXTAR")) 
		{
	%>
			<textarea rows="3" cols="40" name="<%=strName%>" onfocus="changeBgColor(this,'#AACCE8');" 
			onblur="changeBgColor(this,'#ffffff');" class="InputArea"></textarea>
	<% 
		}
		else if(fieldType.equals("SELECT")) 
		{
			ArrayList alValues = (ArrayList)hmCodeList.get(fieldAttr);
			log.debug("alValues = " + alValues);
			
	%>
			<gmjsp:dropdown controlName="<%=strName%>" seletedValue="<%=strName%>" 	
			value="<%=alValues%>" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
	<%	} 
	%>
		</td>
	</tr>
	<tr><td colspan="2" class="LLine" height="1"></td></tr>
	<% 
		i++;
	} // End of for loop
	%>
	<tr><td colspan="2" class="Line" height="1"></td></tr>
</table>
</body>
</html>