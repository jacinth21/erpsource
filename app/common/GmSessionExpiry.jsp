
<%
	/**********************************************************************************
	 * File		 		: Replacing GmSSOLogin.jsp into GmSessionExpiry for keycloak implementation
	 * Desc		 		: This screen is used for the Keycloak Login Process
	 * Version	 		: 1.0 
	 * author			: Gomathi Palani
	 ************************************************************************************/
%>

<%

request.getRequestDispatcher("/GmLogonServlet").forward(request,response);
 
%>