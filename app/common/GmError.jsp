<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%@ page isErrorPage="true" import="java.io.*" %>
 <%
/**********************************************************************************
 * File		 		: GmOperHeader.jsp
 * Desc		 		: This screen is used for the Operations Header
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>



<!--common\GmError.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %> 
<%@ page import="com.Ostermiller.util.StringTokenizer" %> 

<!-- Imports for Logger -->
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="com.globus.common.beans.GmAppErrorBean"%> 
<%@ taglib prefix="fmtError" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmError.jsp -->
<fmtError:setLocale value="<%=strLocale%>"/>
<fmtError:setBundle basename="properties.labels.common.GmError"/>  
<% 
	String strRedirectURL = GmCommonClass.parseNull((String)request.getAttribute("hRedirectURL"));
	String strAddnlParam = GmCommonClass.parseNull((String)request.getAttribute("hAddnlParam")); 
    String strDisplayNm = GmCommonClass.parseNull((String)request.getAttribute("hDisplayNm")); 
 	// Back to Process Transaction screen to Process CN
    String strHScreenFrom = GmCommonClass.parseNull((String)request.getAttribute("hScreenFrom")); 
	// MNNTASK-2629 - The strDisplayNm is displaying two times, So commented the condition.
    /*if(strDisplayNm.length() >0){
		strDisplayNm+= "Back to "+strDisplayNm;
	}
	*/
	GmAppErrorBean appErrorBean = new GmAppErrorBean();

try
{
	 
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
	response.setDateHeader ("Expires", 0);

	String errString = "";
	String expString = "";
	
	String strMessage = "";
	String strReason = "";
	String strAction = "";
	String strMsgCode = "";
	String strMsgVal = "";
	
	String strErrmsg = "";
	String strMsg = "";
	String strActionval = "";
	String strReasonVal = "";
	String strReasonVal1 = "";
	String strErrId = "";
	
	int count = 0;
	HashMap hmDetails = new HashMap();
	
	GmResourceBundleBean gmResourceBundleBeanlbl = 
	    GmCommonClass.getResourceBundleBean("properties.labels.common.GmError", strSessCompanyLocale);
	errString = (String)session.getAttribute("hAppErrorMessage") ;
	log.debug(" Error from session " + errString);
	if (errString == null)
	errString = (String)request.getAttribute("hAppErrorMessage") ;
	
	if (exception != null && exception.toString() != null)
	{
		expString = exception.toString();
		exception.printStackTrace();
	}
	 
	if(errString != null && errString.indexOf("^") != -1)
	{
	String[] Msg =new String[2];
	Msg = errString.split("\\^");
	strMsgCode =  GmCommonClass.parseNull(Msg[0]);
	strMsgVal  =  GmCommonClass.parseNull(Msg[1]);
	//Based on the company locale calling the Apperror.xml , e.g for Japan will call Apperror_en_JP.xml (In Japanese)
	//for other companies will be calling the Apperror.xml(In English)
	
	//This if condition is used to display the dynamic Message and Reason values
	if(errString.contains("#$#")){               //20954^ DHBL-1805858*1119.9854#$#
		strErrmsg = errString; 
		if(strErrmsg.contains("#$#")){
			strErrmsg=strErrmsg.replace("#$#","^");
			StringTokenizer  stToken=new StringTokenizer(strErrmsg,"^");
			count=stToken.countTokens();
	        while(stToken.hasMoreTokens()){
	        	strErrId  = stToken.nextToken().trim();
	        	if(count==4){
	        		strMsg  = GmCommonClass.parseNull((String) stToken.nextToken());
		        	strReasonVal = GmCommonClass.parseNull((String) stToken.nextToken());
		        	strActionval = GmCommonClass.parseNull((String) stToken.nextToken());
	        	}else if(count==3){
	        		strMsg  = GmCommonClass.parseNull((String) stToken.nextToken());
		        	strReasonVal = GmCommonClass.parseNull((String) stToken.nextToken());
	        	}else if(count==2){
	        		strMsg  = GmCommonClass.parseNull((String) stToken.nextToken());
	        	}
	        	HashMap hmMap = appErrorBean.getApperrorMsg(GmCommonClass.getString("GMAPPERROR").replace("Apperror.xml","Apperror"+strJSLocale+".xml"), "ERR"+strErrId);
	        	strMessage = GmCommonClass.parseNull((String)hmMap.get("MESSAGE")); 
	        	strReason = GmCommonClass.parseNull((String)hmMap.get("REASON")); 
	        	strAction = GmCommonClass.parseNull((String)hmMap.get("ACTION"));
	        	if(strMsg.contains("*")){
	        		stToken=new StringTokenizer(strMsg,"*");
	        		count=stToken.countTokens();
	      			     for (int i = 1; i <= count; i++) {
	      				 	strMessage = strMessage.replace("[M:"+i+"]",stToken.nextToken());
	        		     }
	        	}else{
	        		strMessage = strMessage.replace("[M:1]",strMsg);
	        	}
	        	
	        	if(strReasonVal.contains("*")){
	        		stToken=new StringTokenizer(strReasonVal,"*");
	        		count=stToken.countTokens();
	      			     for (int i = 1; i <= count; i++) {
	      			    	strReason = strReason.replace("[R:"+i+"]",stToken.nextToken());
	        		     }
	        	}else{
	        		strReason = strReason.replace("[R:1]",strReasonVal);
	        	}
	        	
	        	if(strActionval.contains("*")){
	        		stToken=new StringTokenizer(strActionval,"*");
	        		count=stToken.countTokens();
	      			     for (int i = 1; i <= count; i++) {
	      			    	strAction = strAction.replace("[A:"+i+"]",stToken.nextToken());
	        		     }
	        	}else{
	        		strAction = strAction.replace("[A:1]",strActionval);
	        	}
	   	   }
		}
	} else {
 		HashMap hmMap = appErrorBean.getApperrorMsg(GmCommonClass.getString("GMAPPERROR").replace("Apperror.xml","Apperror"+strJSLocale+".xml"), "ERR"+strMsgCode);
 		strMessage = GmCommonClass.parseNull((String)hmMap.get("MESSAGE")).replace("[M:1]", strMsgVal); 
 		strReason = GmCommonClass.parseNull((String)hmMap.get("REASON")).replace("[R:1]", strMsgVal);
 		strAction = GmCommonClass.parseNull((String)hmMap.get("ACTION")).replace("[A:1]", strMsgVal);
	} 
 	
		if (strMessage.equals("") ) {
	 		strMessage = strMsgVal;
		} 
	}
	
	String errType  = (String)request.getAttribute("hType");
	log.debug(" hType is ..................... " + errType);
	if (errType == null)
		errType = (String)session.getAttribute("hType") ;
	String strChange  = GmCommonClass.parseNull((String)session.getAttribute("hChange"));
	log.debug(" strChange   is ..................... " + strChange);
	String strMsgBgColor ="#CCCCCC";
	String strMsgFont = "#663399";
	String strBorderColor = "red";
	String strBgColor = "red";
	String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_GLOBUS_MEDICAL_ERROR_MESSAGE"));
	String strPath = strImagePath + "/error.gif";
	String strScriptMsg = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_BACK_TO_PREVIOUS_VERSION"));
	
	if(strClientSysType.equals("IPAD")){
		strScriptMsg = "<font style='font-size: 12pt;'>Back to Previous Screen</font>";
	}
	log.error(" errtype is " + errType + " err string " + expString);
	 
	if(errType == null )
	{
		errType = "E";
	}
	
	if(errString == null || errString.indexOf("Exception") != -1)
	{
		errString = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_ERROR_OCCURED"));
		if (expString != null && !expString.equals(""))
		{
			errString += "<br>"+expString; 
		}
	}
	
	if (errType.equals( "S") )
	{
		strMsgFont = "black";
		strBgColor = "Green";
		strBorderColor = "Green";
		strHeader  = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_GLOBUS_MEDICAL_SUCESS_MESSAGE"));
		strPath	   = strImagePath + "/success.gif";
	}

	if(strDisplayNm.length()>0){
		strScriptMsg = strDisplayNm;
	}	
%>
<html>
<head>
<title>Globus Medical: Error</title>
<meta http-equiv="Content-Type"content="text/html; charset=iso-8859-1">
<script language="JavaScript" src="<%=strJsPath%>/GmDashboard.js"></script>
<FORM name="frmError"  >
<input type="hidden" name="hErrType" value="<%=errType%>">
<script type="text/javascript">  
var redirectURL = '<%=strRedirectURL %>';
var hScreenFrom = '<%=strHScreenFrom%>';
 function Redirect(sURL)
		{  window.location.replace(sURL,true);
		}
 // this fucntion used to redirect the user defind URL 
  function fnRedirectUrl() {
	 // to append the company information.
	 var URLAdd = fnAjaxURLAppendCmpInfo (redirectURL);
	// Back to Process Transaction screen to Process CN
	 if(hScreenFrom == 'PROCESS_TXN_SCN'){
		 parent.window.location.replace(URLAdd, true);
	 }else{
		 window.location.replace(URLAdd, true);
	 }
	 
 }
//This function used to press the enter key then re direct our URL page.
 function fnEnter() {
 	if (event.keyCode == 13) {
 		if(history.length==0){
 	 		window.close();
 	 	}else{
 	 	 	if(redirectURL !=''){
 	 	 		Redirect(redirectURL);
 	 	 	 }else{
 	 	 		history.go(-1);
 	 	 	 }
 	 	}
 	}
 }

/* function fnPicSlip(val)
 {
 	windowOpener("/GmConsignInHouseServlet?hAction=PicSlip&hConsignId="+val,"SetPic","resizable=yes,scrollbars=yes,top=150,left=150,width=750,height=500");
 }*/
 </script>
</head>

<body topmargin="3" leftmargin="3" onkeypress="fnEnter();">

<div align="center"><center>
<p>
<br>
</p>
<p>
<br>
</p>
    <table border="0" cellpadding="0" cellspacing="0" width="60%" bgcolor="#FFFFFF">
      <tr>
        <td>&nbsp;

        </TD>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>

</center></div><div align="center"><center>
  <%
if ( !strMsgCode.equals("") ) { %>
    <table border="1" cellpadding="0" cellspacing="0" width="50%"  >
    
    <tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td height="25"  colspan="2" class="RightDashBoardHeader">&nbsp;<img src="<%=strImagePath%>/alert.jpg" width="20" height="19">&nbsp;<fmtError:message key="LBL_ALERT"/></td>
					 
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
		  
					<tr bgcolor="#CCCCCC" class="RightTableCaption">
						<td  colspan="2" height="25">&nbsp;<fmtError:message key="LBL_MESSAGE"/></td> 
					</tr>			
						
					<tr><td class="Line" height="1" colspan="2"> </td></tr>
					<tr >
						 
						<td colspan="2"  height="25">&nbsp;<%=strMessage%></td>
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
		  
					<tr bgcolor="#CCCCCC" class="RightTableCaption">
						<td  colspan="2" height="25">&nbsp;<fmtError:message key="LBL_REASON"/></td> 
					</tr>			
						
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					<tr >
						 
						<td colspan="2"  height="25">&nbsp;<%=strReason%></td>
					</tr>
					<tr><td class="Line" height="1" colspan="2"></td></tr>
		  
					<tr bgcolor="#CCCCCC" class="RightTableCaption">
						<td  colspan="2" height="25">&nbsp;<fmtError:message key="LBL_ACTION"/></td> 
					</tr>			
				 
					<tr><td class="Line" height="1" colspan="2"></td></tr>
					<tr >
						 
						<td colspan="2"  height="25">&nbsp;<%=strAction%></td>
					</tr>
									    <%  if ( !strChange.equals("change") ) { %>
					      <tr>
					        <td align="center" width="100%" ><br><a class="RightText" href="javascript:if(history.length==0){window.close();}else{history.go(-1);}"><u><h5>
					          <b>
					      <%
					      if ( !errType.equals("E") ) { //code is kept because the following script tag is not getting called when Error.jsp is getting called under div tag.
					
					      if (!strRedirectURL.equals("")){
					   		%>
					   		<a class='RightText' href="javascript:fnRedirectUrl();"><u><h5><b><fmtError:message key="LBL_BACK_TO"/> <%=strDisplayNm%></b><h5></u></a>
					   		<%}%>    
					   <script>
					   if(history.length==0)
					   {
					   	document.write("Close this window");
					   }
					   else if (document.frmError.hErrType.value == 'E')
					   {
					   	document.write('<%=strScriptMsg%>');
					   }
					   </script>
					   <%}else {
					
					    if (!strRedirectURL.equals("")){
					   		%>
					   		<a class='RightText' href="javascript:fnRedirectUrl();"><u><h5>
					   		<%}%>
						   <%=strScriptMsg%>
						   <%} %>
					          </b><h5></u></a> </td>
					      </tr>
					      <% } else { %>
					      
					       <tr>
					        <td align="center" width="100%" bgcolor="#CCCCCC"><br> <u><h5>
					          <b>
					   			<fmtError:message key="LBL_PLEASE_WAIT_WILL_BE_BACK"/>
					          </b><h5></u> </td>
					      </tr>
					      <script type="text/javascript"> 
							setTimeout("Redirect('/GmLogin.jsp')",3000);
					 
						</script>
					      
					      <% session.setAttribute("hChange",""); 
					     
					      } %>
					      
				</table>
			</td>
		</tr>
		</table> 
	
 <%}else {%>	
		
      <table border="1" cellpadding="0" cellspacing="0" width="50%" bordercolor="<%=strBorderColor%>">
    
      <tr bgcolor="<%=strBgColor%>">
        <td align="center" width="100%" class="RightTableCaption" height="25"><font
        color="#FFFFFF"><%=strHeader%></strong></font></td>
      </tr>
      <tr>
        <td bgcolor="#CCCCCC">
          <center>
            <img src="<%=strPath%>" width="32" height="32">
          </center>
        </td>
      </tr>
      <tr>
        <td align="center" width="100%" bgcolor=<%=strMsgBgColor%> class="RightText"> <font color=<%=strMsgFont%> size="2">
        <b>
         <br>
          <br>
          <%=errString%> <br>
          <br>
          </b></font></td>
      </tr>
      <%  if ( !strChange.equals("change") ) { %>
      <tr>
        <td align="center" width="100%" bgcolor="#CCCCCC"><br><a class="RightText" href="javascript:if(history.length==0){window.close();}else{history.go(-1);}"><u><h5>
          <b>
      <%
      if ( !errType.equals("E") ) { //code is kept because the following script tag is not getting called when Error.jsp is getting called under div tag.

      if (!strRedirectURL.equals("")){
   		%>
   		<a class='RightText' href="javascript:fnRedirectUrl();"><u><h5><b><fmtError:message key="LBL_BACK_TO"/> <%=strDisplayNm.replace("Back to ","")%></b><h5></u></a>
   		<%}%>    
   <script>
   if(history.length==0)
   {
   	document.write("Close this window");
   }
   else if (document.frmError.hErrType.value == 'E')
   {
   	document.write('<%=strScriptMsg%>');
   }
   </script>
   <%}else {

    if (!strRedirectURL.equals("")){
   		%>
   		<a class='RightText' href="javascript:fnRedirectUrl();"><u><h5>
   		<%}%>
	   <%=strScriptMsg%>
	   <%} %>
          </b><h5></u></a> </td>
      </tr>
      <% } else { %>
      
       <tr>
        <td align="center" width="100%" bgcolor="#CCCCCC"><br> <u><h5>
          <b>
   			<fmtError:message key="LBL_PLEASE_WAIT_WILL_BE_BACK"/>
          </b><h5></u> </td>
      </tr>
      <script type="text/javascript"> 
		setTimeout("Redirect('/GmLogin.jsp')",3000);
 
	</script>
      
      <% session.setAttribute("hChange",""); 
     
      } %>
      
      
    </table> <%} %>	
</center></div>
</body>
<%@ include file="/common/GmFooter.inc" %>
</html>
<%
}
catch(Exception e)
{
e.printStackTrace();
}
%>
