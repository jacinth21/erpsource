<%
/**********************************************************************************
 * File		 		: GmRuleParamsSetup.jsp
 * Desc		 		: This screen is used to Add ICT Parameter
 * author			: Velu
************************************************************************************/
%>


<!--common\GmRuleParamsSetup.jsp -->


<%@ page language="java"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.lang.*"%>
<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@ page import="com.globus.common.beans.GmLogger"%>
<%@ page import="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="org.apache.commons.beanutils.DynaBean"%>
<%@ page import="com.globus.common.forms.GmRuleParamsSetupForm"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap,java.util.List" %>

<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtRuleParamsSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRuleParamsSetup.jsp -->
<fmtRuleParamsSetup:setLocale value="<%=strLocale%>"/>
<fmtRuleParamsSetup:setBundle basename="properties.labels.common.GmRuleParamsSetup"/>  
<bean:define id="lResult" name="frmRuleParamsSetup" property="lResult" type="java.util.List"></bean:define>
<bean:define id="ruleGrpId" name="frmRuleParamsSetup" property="ruleGrpId" type="java.lang.String"></bean:define>
<bean:define id="codeGrpId" name="frmRuleParamsSetup" property="codeGrpId" type="java.lang.String"></bean:define>
<bean:define id="header" name="frmRuleParamsSetup" property="header" type="java.lang.String"></bean:define>
 
<%
	String strWikiTitle = GmCommonClass.getWikiTitle("ADD_ICT_PARAMETERS");
	
	//Retrieve the Rule Code ID 
	StringBuffer sbInput = new StringBuffer();	
	int rowsize=0;
	DynaBean db;
	List alRuleParamsList=null;
	String strGrpLabelId = "";
			
	if(lResult!=null){
		alRuleParamsList = lResult;
		rowsize = alRuleParamsList.size();
	} 
%>

<HTML>
<HEAD>
<TITLE> Globus Medical: Add ICT Parameter </TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmRuleParamsSetup.js"></script>

<script>
	var varRows = '<%=rowsize%>';  
	var varInput = '<%=sbInput.toString()%>';
</script>

<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
</head>

<BODY leftmargin="20" topmargin="10">
<html:form action="/gmRuleParamSetup.do">
<html:hidden property="strOpt" name="frmRuleParamsSetup" value=""/>
<html:hidden property="strInputString" name="frmRuleParamsSetup" value=""/>
<html:hidden property="ruleGrpId" value="<%=ruleGrpId%>"/>
<html:hidden property="codeGrpId" value="<%=codeGrpId%>"/>
<html:hidden property="strInvSource"/>
<input type="hidden" name="header" value="<%=header%>" >

<html:hidden property="ruleGrpId" name="frmRuleParamsSetup" value="<%=ruleGrpId%>"/>

<table border="0" class="DtTable680" cellspacing="0" cellpadding="5">
		<tr>
			<td height="25" class="RightDashBoardHeader" ><%=header%></td>
			<fmtRuleParamsSetup:message key="LBL_HELP" var="varHelp"/>
			<td  height="25" class="RightDashBoardHeader" align="right">
			 <img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' 
	       		height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
	       </td>
		</tr>
		
		<tr>
		 <td colspan="2" >
			<display:table name="requestScope.frmRuleParamsSetup.lResult" requestURI="/gmRuleParamSetup.do"  class="its" id="currentRowObject"  decorator="com.globus.common.displaytag.beans.DTRuleTableParamWrapper"> 
 				<fmtRuleParamsSetup:message key="LBL_NAME" var="varName"/><display:column class="alignright" property="LABELNM" title="${varName}" />
 				<display:column property="LABELID" title=""/>
 				<fmtRuleParamsSetup:message key="LBL_VALUE" var="varValue"/><display:column property="RULVALUE" title="${varValue}"/>
 			</display:table>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">&nbsp;&nbsp;    <p>&nbsp;</p>
			<fmtRuleParamsSetup:message key="BTN_SUBMIT" var="varSubmit"/>
			<fmtRuleParamsSetup:message key="BTN_CLOSE" var="varClose"/>
                 <gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save"  onClick="fnSubmit();" /> &nbsp;&nbsp;
                 <gmjsp:button value="${varClose}" gmClass="button" buttonType="Load" onClick="fnClose();"/>  <p>&nbsp;</p>  
         </td>
		</tr>   
	</table>
</html:form>
<%@ include file="/common/GmFooter.inc"%>
</BODY>
</HTML>
