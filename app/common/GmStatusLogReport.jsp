



<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%@ page language="java" %>

<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc" %>

<%@page import="java.util.Date"%>
<%@ taglib prefix="fmtStatusLogReport" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmStatusLogReport.jsp -->
<fmtStatusLogReport:setLocale value="<%=strLocale%>"/>
<fmtStatusLogReport:setBundle basename="properties.labels.common.GmStatusLogReport"/>  
<bean:define id ="alTypes" name="frmStatusLog" property="alTypes" type="java.util.ArrayList"/>
<bean:define id ="source" name="frmStatusLog" property="source" type="java.lang.String"/>
<bean:define id ="hmTypeVal" name="frmStatusLog" property="hmTypeVal" type="java.util.HashMap"/>
<bean:define id ="strDateDiff" name="frmStatusLog" property="strDateDiff" type="java.lang.String"/>
<%
GmServlet gm = new GmServlet();
gm.checkSession(response,session);
String strWikiTitle = GmCommonClass.getWikiTitle("STATUS_LOG_REPORT");
String strApplDateFmt = strGCompDateFmt;
String strTodaysDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);

String strFrmDate = GmCommonClass.parseNull((String)request.getAttribute("FROMDATE"));
String strToDate =GmCommonClass.parseNull((String)request.getAttribute("TODATE"));
Date dtFrmDate = GmCommonClass.getStringToDate(strFrmDate, strApplDateFmt);
Date dtToDate = GmCommonClass.getStringToDate(strToDate, strApplDateFmt);
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Status Log Detail</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmStatusLog.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script>

</script>
<%
	ArrayList alSource = new ArrayList();
	HashMap hmSource = new HashMap();
	HashMap hmType = new HashMap();
	for (int i=0;i<alTypes.size();i++)
	{ 
		hmSource = (HashMap)alTypes.get(i);
		alSource =(ArrayList)hmTypeVal.get(hmSource.get("CODENMALT"));
		int arrLen = alSource.size();
		String strCodeid = (String)hmSource.get("CODEID");
		%>
		<script>put('<%=strCodeid%>','<%=arrLen%>');</script>	
		<%
		for(int j=0; j<alSource.size();j++){
			hmType = (HashMap)alSource.get(j);
			String strCode = (String)hmType.get("CODEID");
			String strCodenm = (String)hmType.get("CODENM");
		%>
		<script>put('<%=strCodeid%><%=j%>','<%=strCode%>,<%=strCodenm%>');</script>
		<%
		}
	}
%> 
<script>
var selectID = '<%=source%>';
var ruleVal = '<%=strDateDiff%>';
var format = '<%=strApplDateFmt%>';
var currentDate = '<%=strTodaysDate%>';
</script>

</HEAD>

<BODY leftmargin="20" topmargin="10" onkeydown="fnEnter();" onload="fnPageLoad();">
 
<html:form action="/gmstatuslog.do"  >
<html:hidden property="strOpt" />
<html:hidden property="haction" value="G"/>  

	<table border="0" class="DtTable850" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="6" height="25" class="RightDashBoardHeader"><fmtStatusLogReport:message key="LBL_STATUSLOGREPORT"/></td>
			<td align="right" class=RightDashBoardHeader > 
			<fmtStatusLogReport:message key="LBL_HELP" var="varHelp"/>
			<img id='imgEdit' align="right" style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td> 
		</tr>
		<!-- Custom tag lib code modified for JBOSS migration changes -->
		<tr height="25">
			<td  class="RightTableCaption" align="right">&nbsp;<fmtStatusLogReport:message key="LBL_TRANSACTION_ID"/>:</td>	
			<td>&nbsp;<html:text property="txnID"  size="20" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');" tabindex="1"/>
			</td>
			
		    <td  class="RightTableCaption" align="right">&nbsp;<fmtStatusLogReport:message key="LBL_SOURCE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="typeNum" SFFormName="frmStatusLog" SFSeletedValue="typeNum" 
					SFValue="alTypes" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" onChange="javascript:fnTypes(this.value);" tabIndex="2" />
			</td>
			<td  class="RightTableCaption" align="right">&nbsp;<fmtStatusLogReport:message key="LBL_TYPE"/>:</td>
			<td>&nbsp;<gmjsp:dropdown controlName="source" SFFormName="frmStatusLog" SFSeletedValue="source" 
					SFValue="alSource" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" tabIndex="3" />
			</td>
			
		</tr>
		<tr height="30" class="shade">
	       <td align="right" class="RightTableCaption">
	           <b><font color="red">*</font> <fmtStatusLogReport:message key="LBL_STARTDATE"/>:</b></td>
	           
		       <td>&nbsp;<gmjsp:calendar textControlName="startDate" textValue="<%=dtFrmDate==null?null:new java.sql.Date(dtFrmDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="4"/>
		       </td>
		       
		       <td align="right" class="RightTableCaption">
	           <b><font color="red">*</font> <fmtStatusLogReport:message key="LBL_ENDDATE"/>:</b></td>
		       <td>&nbsp;<gmjsp:calendar textControlName="endDate" textValue="<%=dtToDate==null?null:new java.sql.Date(dtToDate.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');" tabIndex="5"/>&nbsp;
							
		   </td>
           <td class="RightTableCaption" align="right"><fmtStatusLogReport:message key="LBL_USERS"/>:&nbsp;</td>
           <td>
               <gmjsp:dropdown controlName="userNum" SFFormName="frmStatusLog" SFSeletedValue="userNum" 
					SFValue="alUsers" codeId = "ID"  codeName = "NAME"  defaultValue= "[Choose One]" tabIndex="6" width="200"/>
           </td>
           <td align="center">
           <fmtStatusLogReport:message key="BTN_LOAD" var="varLoad"/>
			    &nbsp;<gmjsp:button value="&nbsp;${varLoad}&nbsp;" name="Btn_Submit" gmClass="button" buttonType="Load" onClick="fnRptReload();" tabindex="7" />						
		   </td>
	    </tr>
		<logic:notEqual name="frmStatusLog" property="haction" value="initiating"> 
		<tr><td colspan="7"></td></tr>
	      	<tr>
		<td colspan="7" width="100%">
		<display:table name="requestScope.frmStatusLog.alReportList" export="true" class="its" requestURI="/gmstatuslog.do" id="currentRowObject" decorator="com.globus.common.displaytag.beans.DTStatusLogWrapper" >
		<display:setProperty name="export.excel.filename" value="StatusLogReport.xls" /> 
 		<fmtStatusLogReport:message key="LBL_TRANSACTIONID" var="varTransactionId"/><display:column property="REFID" title="${varTransactionId}" class="alignleft" sortable="true"/>
 		<fmtStatusLogReport:message key="LBL_SOURCE" var="varSource"/><display:column property="SOURCE" title="${varSource}" class="alignleft" />
 		<fmtStatusLogReport:message key="LBL_TYPE" var="varType"/><display:column property="REFTYPE" title="${varType}" class="alignleft" />
		<fmtStatusLogReport:message key="LBL_USER" var="varUser"/><display:column property="UPDUSER" title="${varUser}" class="alignleft" />
		<fmtStatusLogReport:message key="LBL_DATE" var="varDate"/><display:column property="UPDDATE" title="${varDate}" class="alignleft" />
		</display:table>
		</td>
		</tr> 
 </logic:notEqual>
		</table>
</html:form>
   <%@ include file="/common/GmFooter.inc" %>
   <%
//}catch(Exception e)
//{
	//e.printStackTrace();
//}
%>
</BODY>
</HTML>