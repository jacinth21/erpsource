


<!--common\GmIncludeLog.jsp -->

<%@ include file="/common/GmHeader.inc"%>

<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="org.apache.commons.beanutils.PropertyUtils"%>
<%@ page import="com.globus.common.beans.GmResourceBundleBean" %>
<%@ taglib prefix="fmtIncLog" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmtIncLog:requestEncoding value="UTF-8" />
<fmtIncLog:setLocale value="<%=strLocale%>"/>
<fmtIncLog:setBundle basename="properties.labels.common.GmIncludeLog"/>
<%	

/*********************************
	 * Reason for Modification
	 *********************************/ 
	HashMap hmLog = new HashMap();
	ArrayList alLog = new ArrayList();
	int intLog = 0;	 
	String strLogCaption = "";
	String strLogTableHeader = "";
	String strFormName = "";
	String strArrayListName = "";
	String strLogReasonLabel = "txt_LogReason";
	String strComments = "";
	String tabIndex ="";
	boolean blHideComments = false;
	
	String dt = "";
	String uname = "";
	String comments = "";
	String strExpandCollase = "";

	GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.labels.common.GmIncludeLog", strSessCompanyLocale);
	// Check if the page is in struts
	strFormName = GmCommonClass.parseNull(request.getParameter("FORMNAME"));
	if(!strFormName.equals(""))
	{
	
		strArrayListName = GmCommonClass.parseNull(request.getParameter("ALNAME"));
		Object objTemp = pageContext.getRequest().getAttribute(strFormName);
		alLog  = GmCommonClass.parseNullArrayList((ArrayList)PropertyUtils.getProperty(objTemp,strArrayListName));
	}
	else {
	// To get the user Log information 
	alLog = (ArrayList)request.getAttribute("hmLog");
	strLogReasonLabel = "Txt_LogReason";
	}
	
	String strLogType = GmCommonClass.parseNull(request.getParameter("LogType"));
	String strLogWidth = GmCommonClass.parseNull(request.getParameter("LogWidth"));
	String strLogMode = GmCommonClass.parseNull(request.getParameter("LogMode"));
	String strTabIndex = GmCommonClass.parseNull(request.getParameter("TabIndex"));
	if(strTabIndex != ""){
		tabIndex = "tabIndex='"+strTabIndex+"'";
	}
	String strMandatory = GmCommonClass.parseNull(request.getParameter("Mandatory"));
	String strShowCam = GmCommonClass.parseNull(request.getParameter("ShowCam"));
	strExpandCollase = GmCommonClass.parseNull(request.getParameter("EXPAND_COLLAPSE"));
	if(strLogWidth.trim().equals("")){
		strLogWidth = "100%";
	}
	if (strShowCam.equals("true")) {
		strComments = GmCommonClass.parseNull((String)request.getAttribute("strComments")) + "\n";
	} else {
		strComments = ""; 
	}
	

	
	// Code to decide on the caption 
	// strLogType -- "CHANGE" for any modification
	//			  -- "PLOG" for any phone log
	
	if 	(strLogType.equals("CHANGE"))
	{
		strLogCaption = "Reason for changes";
		strLogTableHeader = "Reason for Modification";
	}
	else if (strLogType.equals("PLOG"))
	{
		strLogCaption = "Call Log";
		strLogTableHeader = "Call Log List";
	}
	else
	{
		strLogCaption = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("IMG_LOG_CAP"));
		strLogTableHeader = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TD_LOG_HEADER"));
	}
	
	
	if 	(strMandatory.equals("yes"))
	{
		strLogCaption="<span style=\"color: red;\">*</span>&nbsp;"+strLogCaption;
	
	}	
	if(strExpandCollase.equals("Y")){
	
	  %>
<tr>
		<td height="24" class="ShadeRightTableCaption"><a
			href="javascript:fnShowFilters('tabComments');"><IMG
				id="tabCommentsimg" border=0 src="<%=strImagePath%>/minus.gif"></a>&nbsp;<%=strLogCaption%></td>
	</tr>
	<tr>
		<td>
			<table id="tabComments" border=0 Width="<%=strLogWidth%>" cellspacing=0 cellpadding=0 style="word-wrap: break-word;">
	<%}else{ %>
<table border=0 Width="<%=strLogWidth%>" cellspacing=0 cellpadding=0 style="word-wrap: break-word;">
<tr class="ShadeRightTableCaption">
	<td Height="24" colspan="3" class="RightText">&nbsp;<%=strLogCaption%></td>
</tr>

	
<% }
	if 	(!strLogMode.equals("View"))
	{	
%>	
	<tr><td height="1" colspan="3" bgcolor="#666666"></td></tr>		
	<tr>
	    <td colspan="3" height="50"  ><input type="hidden" name="hShowCam" value="<%=strShowCam%>">&nbsp;<textarea id="comments" name="<%=strLogReasonLabel%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
		onBlur="changeBgColor(this,'#ffffff');"  rows=3 cols=100 <%=tabIndex%>><%=strComments%></textarea></td>
	</tr>
<%
	}
%>	
	<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
	<tr bgcolor="#EEEEEE" class="RightTableCaption">
		<td width="60" height="25">&nbsp;<fmtIncLog:message key="LBL_DATE"/></td>
		<td width="140"><fmtIncLog:message key="LBL_UNAME"/> </td>
		<td width="500"><%=strLogTableHeader%></td>
	</tr>
	<TR><TD colspan=3 height=1 class=Line></TD></TR>
	
	<%
	if (alLog != null){
		intLog = alLog.size();
	}
	if(intLog>3){
		blHideComments  = true;
	}
	if (intLog > 0)
	{
		
		for (int i=0;i<intLog;i++)
		{	hmLog = (HashMap)alLog.get(i);
%>
			<tr>
				<td class="RightText" height="20">&nbsp;<%=(String)hmLog.get("DT")%></td>
				<td class="RightText">&nbsp;<%=(String)hmLog.get("UNAME")%></td>
				<td style="width: 500px;" width="500"><%=(String)hmLog.get("COMMENTS")%></td></tr>
			<%if(blHideComments && i==2){%>
					<fmtIncLog:message key="IMG_ALT_VIEW_ALL" var="varViewAll"/>
					<tr><td  colspan=3><div id="divShow">
					&nbsp;<a href="javascript:fnShowDivTag()"><img alt="${varViewAll}Click to view all comments" border="0" src="<%=strImagePath%>/plus.gif"/>&nbsp;<fmtIncLog:message key="IMG_SHOW_MORE"/></a></div></td></tr>
				 	<tr><td colspan=3><table id="tblHideComments" border="0" style="display:none" width="100%" cellspacing=0 cellpadding=0>
			 		<tr>
						<td width="60"></td>
						<td width="140"></td>
						<td width="500"></td>
					</tr>
			<%} // End of Div flag 
			if(blHideComments && i==(intLog-1)){
			%>
				</table></td></tr> 
			<%} 
		}//End of FOR Loop
	}else {
%>		<tr><td colspan="3" height="25" align="center" class="RightTextBlue">
		<BR><fmtIncLog:message key="TD_NO_DATA"/></td></tr>
<%

}
	%>
<TR><TD colspan=3 height=1 class=Line></TD></TR>
</table>
<%@ include file="/common/GmFooter.inc"%>
