<%@page import="com.globus.common.beans.GmCalenderOperations"%>
<%
/**********************************************************************************
 * File		 		: GmRuleConditionTab.jsp
 * Desc		 		: Screen fro Rule Engine
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>


<!--common\GmRuleConditionTab.jsp -->


<%@ include file="/common/GmHeader.inc" %>
<%@ taglib uri="/WEB-INF/struts-html-el.tld" prefix="htmlel" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.Date"%>
<%@ taglib prefix="fmtRuleCondtionTab" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRuleConditionTab -->
<fmtRuleCondtionTab:setLocale value="<%=strLocale%>"/>
<fmtRuleCondtionTab:setBundle basename="properties.labels.common.GmRuleConditionTab"/>  
<%
Object bean = pageContext.getAttribute("frmRuleCondition", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
					
<bean:define id="hmValue" name="frmRuleCondition"  property="hmRules" type="java.util.HashMap"></bean:define>
<bean:define id="strOpt" name="frmRuleCondition"  property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="ruleId" name="frmRuleCondition" property="ruleId" type="java.lang.String"> </bean:define>

<%
	ArrayList alRule = new ArrayList();
	alRule = GmCommonClass.parseNullArrayList((ArrayList)hmValue.get("ALRULE"));
	String strSelectedTrans = GmCommonClass.parseNull((String)hmValue.get("TRANSACTION"));  
	int intSize = alRule.size();
	String strRuleId = ruleId;
	String strRuleName = "";
	String strExpiryDate = "";
	String strInitiatedBy = (String)session.getAttribute("strSessShName");
	String strApplDateFmt = strGCompDateFmt;
	String strInitiatedDate = GmCalenderOperations.getCurrentDate(strApplDateFmt) ;
	String strActiveFl = "Y";
	String strRuleEmlNotify = "";
	String strRuleComment = "";
	String strActvChk = "checked";
	String strRuleEmlNotifyChk = "";
	String strDisabled = "disabled";
	
	String strExpHistoryfl = "";
	String strActiveHistoryfl = "";
	String strEmailNotifyHistoryfl = "";
	String strTxnHistoryfl = "";
	String strConHistoryfl = "";
	
	Date dtExpiryDt = null;
	
	
	if(strOpt.equals("edit"))
	{
		strDisabled = "";
	}
	HashMap hmRuleHead = new HashMap();
		for (int j=0;j<intSize;j++)
		{
			hmRuleHead = (HashMap)alRule.get(j);
			strRuleId = (String)hmRuleHead.get("RULEID");
			strRuleName = (String)hmRuleHead.get("RULENAME");
			strExpiryDate = (String)hmRuleHead.get("EXPIRYDATE");
			strInitiatedBy = (String)hmRuleHead.get("INITIATEDBY");
			strInitiatedDate = (String)hmRuleHead.get("INITIATEDDATE");
			strActiveFl = (String)hmRuleHead.get("ACTIVEFL");
			strRuleEmlNotify = (String)hmRuleHead.get("RULEEMAILNOTIFY");
			strRuleComment = (String)hmRuleHead.get("RULECOMMENT");
			strRuleEmlNotifyChk = strRuleEmlNotify.equals("Y")?"checked":"";
			strActvChk = strActiveFl.equals("Y")?"checked":"";
			strExpHistoryfl = (String)hmRuleHead.get("EXP_HISFL");
			strActiveHistoryfl = (String)hmRuleHead.get("ACT_HISFL");
			strEmailNotifyHistoryfl = (String)hmRuleHead.get("EMAIL_HISFL");
			strTxnHistoryfl = (String)hmRuleHead.get("TXN_HISFL");
			strConHistoryfl = (String)hmRuleHead.get("CON_HISFL");				
		}
		dtExpiryDt = (Date)GmCommonClass.getStringToDate(strExpiryDate,strApplDateFmt);
%>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VRULE"/>
<html:hidden property="hruleId" value="<%=strRuleId%>"/>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	<tr>
		 <td colspan="2" width="100%" height="200" valign="top">
			<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="Shade">  
					<td HEIGHT="25">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
							<td class="RightTableCaption" align="right">&nbsp;<fmtRuleCondtionTab:message key="LBL_RULE_NAME"/>:</td>																
							</tr>
						</table>
					</td>
					<td HEIGHT="25">
						<table cellpadding="0" cellspacing="0" border="0">
						<!-- Struts tag lib code modified for JBOSS migration changes -->
							<tr>
								<td>&nbsp; <html:text property="ruleName" size="35" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
									onblur="changeBgColor(this,'#ffffff');" value="<%=strRuleName%>"/>
								</td>								
								<td class="RightTableCaption" align="right">&nbsp;&nbsp;<fmtRuleCondtionTab:message key="LBL_EXPIRY_DATE"/>:
								
								</td>
								<td>&nbsp;<gmjsp:calendar textControlName="expiryDate" textValue="<%=dtExpiryDt==null?null:new java.sql.Date(dtExpiryDt.getTime())%>"  gmClass="InputArea" onFocus="changeBgColor(this,'#AACCE8');" onBlur="changeBgColor(this,'#ffffff');"/>&nbsp;
								<fmtRuleCondtionTab:message key="LBL_CLICK_TOOPEN_REQUIRED_HISTORY" var="varClickToOpenREqHistory"/>
								<%  if ( strExpHistoryfl.equals("Y") ) { %>
								
									<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1011','<%=strRuleId %>');" title="${varClickToOpenREqHistory}"  
									src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />&nbsp;	 
								<% } %>								                                        		
                        		</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr>  
					<td HEIGHT="25">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
							<td class="RightTableCaption" align="right">&nbsp;<fmtRuleCondtionTab:message key="LBL_INITIATED_BY"/>:</td>																
							</tr>
						</table>
					</td>
					<td HEIGHT="25">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td width="50%">&nbsp; 
								<%-- <input type="text" size="20" class="InputArea" value="<%=strInitiatedBy%>" name="initiatedBy" <%=strDisabled %> onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea"
									onblur="changeBgColor(this,'#ffffff');">--%>
								<%=strInitiatedBy%>	&nbsp; 
								</td>								
								<td class="RightTableCaption" align="right">&nbsp;&nbsp;<fmtRuleCondtionTab:message key="LBL_INITIATED_DATE"/>:</td>
								<td width="27%">&nbsp;<%=strInitiatedDate%>
										<%-- <input type="text" size="10" styleClass="InputArea" value="<%=strInitiatedDate%>" name="initiatedDate" <%=strDisabled %> onfocus="changeBgColor(this,'#AACCE8');"
										onblur="changeBgColor(this,'#ffffff');"> 
										<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('all.initiatedDate');" title="Click to open Calendar"  
										src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />--%>	&nbsp;&nbsp;									                                        		
                        		</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr class="Shade">  
					<td HEIGHT="25">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
							<td class="RightTableCaption" align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtRuleCondtionTab:message key="LBL_ACTIVE"/>:&nbsp;
							
							</td>																
							</tr>
						</table>
					</td>
					<td HEIGHT="25">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
							
								<td width="40%" align="left">&nbsp; 
								<%    if(strOpt.equals("load")&&!strRuleId.equals(""))   strDisabled ="";    %>
									<input type="checkbox" <%=strDisabled %> name="activeFl" <%=strActvChk %> >
								<%  if ( strActiveHistoryfl.equals("Y") ) { %>
								
							<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1012','<%=strRuleId %>');" title="${varClickToOpenREqHistory}"  
							src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />&nbsp;	
							
							<% } %>
								</td>								
								<td class="RightTableCaption" align="right" width="33%"><fmtRuleCondtionTab:message key="LBL_EXIPYEMAIL_NOTIF"/>:</td>
								<td width="62%" align="left">
										<input type="checkbox" name="ruleEmailNotify" <%=strRuleEmlNotifyChk %>	 >&nbsp;&nbsp;
										
									<%  if ( strEmailNotifyHistoryfl.equals("Y") ) { %>
									
									<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1013','<%=strRuleId %>');" title="${varClickToOpenREqHistory}"  
									src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />&nbsp;	
									
									<% } %>									                                        		
                        		</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>  
					<td HEIGHT="25">
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
							<td class="RightTableCaption" align="right" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmtRuleCondtionTab:message key="LBL_DESCRIPTION"/>:&nbsp;							
							</td>																
							</tr>
						</table>
					</td>
					<td HEIGHT="25" >
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td colspan="3" width="5%" align="left">&nbsp; 
									<textarea rows="3" cols="95" name="ruleComment"  onfocus="changeBgColor(this,'#AACCE8');" 
										onblur="changeBgColor(this,'#ffffff');" class="InputArea"><%=strRuleComment%></textarea>
								</td>
							</tr>
						</table>
					</td>
				</tr>				
				<%-- <tr  height="24">
					<td colspan="2">&nbsp;Rule Description :</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr>
					<td colspan="2">
						<textarea rows="3" cols="120" name="ruleComment"  onfocus="changeBgColor(this,'#AACCE8');" 
						onblur="changeBgColor(this,'#ffffff');" class="InputArea"><%=strRuleComment%></textarea>
					</td>
					
				</tr>--%>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr  height="24" class="ShadeRightTableCaption">
					<td colspan="2">&nbsp;<fmtRuleCondtionTab:message key="LBL_RULE_CONDITION"/> :</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr>
					<td colspan="2">
						<iframe src="/GmFilterAjaxServlet?aReport=true&strOpt=edit&ruleId=<%=strRuleId%>&conhistoryfl=<%=strConHistoryfl%>" scrolling="no" id="frmFilter" marginheight="0" width="100%" height="250"></iframe><BR>
					</td>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>					
				<tr height="24" class="ShadeRightTableCaption"><td >
				
				<%  if ( strTxnHistoryfl.equals("Y") ) { %>
				
									<img  id="imgEdit" style="cursor:hand" onclick="javascript:fnLoadHistory('1015','<%=strRuleId %>');" title="${varClickToOpenREqHistory}"  
									src="<%=strImagePath%>/icon_History.gif" align="bottom"  height=15 width=18 />&nbsp;	
									
									<% } %>	
					&nbsp; <fmtRuleCondtionTab:message key="LBL_TRANSACTION"/> : 				
				</td>
				<td HEIGHT="25" align="left">
						<table cellpadding="0" cellspacing="0" border="0">
						<!-- Custom tag lib code modified for JBOSS migration changes -->
							<tr>
								<td><gmjsp:dropdown controlName="ruleTxnType"	SFFormName="frmRuleCondition" SFSeletedValue="ruleTxnType" 
														SFValue="alRuleTxnType" defaultValue="[Choose One]" codeId="CODEID" codeName="CODENM" onChange="fnLoadRuleTrans(this);"/>
								</td>
							</tr>
						</table>
					</td>
				<td align="right" class=RightDashBoardHeader ></td></tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr> 
				<tr bgcolor="gainsboro"> 							
		            <td width="20%" colspan="2">&nbsp;&nbsp;
				    	<html:checkbox property="selectAll" onclick="fnSelectAll('toggle');" /><B><fmtRuleCondtionTab:message key="LBL_SELECT_ALL"/></B> 
 					</td>											
	    	    </tr>
	    	    <tr>
					<table border="0">
						<tr>
							<td width="2%">							
								<div style="display:visible; height: 170px; overflow: hidden; overflow-y:scroll; border-width: 0px; border-style: solid; margin-left: 8px;">
									<table>
										<logic:iterate id="alTransactionList" name="frmRuleCondition" property="alTransactionList">
											<tr>
												<td>
													<htmlel:multibox property="selectedTrans" value="${alTransactionList.CODEID}" onclick="fnUpdtSelAll(this);"/>
													<bean:write name="alTransactionList" property="CODENM" />
												</td>
											</tr>
										</logic:iterate>
									</table>
								</div>
								<br/>
							</td>						       	
				       	</tr>
			       	</table>
				</tr>
				<tr><td colspan="2" class="LLine" height="1"></td></tr>
				<tr> 
					
					<td colspan="2" align="center" height="24">&nbsp;
						 <fmtRuleCondtionTab:message key="BTN_SUBMIT" var="varSubmit"/>
						 <fmtRuleCondtionTab:message key="BTN_NEW_RULE" var="varNewRule"/>
						 <logic:equal name="frmRuleCondition" property="enableCreateRule" value="true">	 
                    	<gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnConditionSubmit(this.form);" />&nbsp;&nbsp;
                    	 <% if(strOpt.equals("edit")||!strRuleId.equals("")) { %> 
                    	<gmjsp:button value="${varNewRule}" buttonType="Save" gmClass="button" onClick="fnNewRule(this.form);"/>&nbsp;&nbsp;     
                    	               	
                   		 <% } %>
                   		</logic:equal> 
                   		 
                   		 <logic:equal name="frmRuleCondition" property="enableCreateRule" value="false">	
                   		  
                    	<gmjsp:button value="${varSubmit}" gmClass="button" disabled="true" buttonType="Save" onClick="fnConditionSubmit(this.form);"/>&nbsp;&nbsp;
                    	  <% if(strOpt.equals("edit")||!strRuleId.equals("")) { %>
                    	<gmjsp:button value="${varNewRule}" gmClass="button" disabled="true" buttonType="Save" onClick="fnNewRule(this.form);"/>&nbsp;&nbsp;                    	
                   		 <% } %>
                   		 </logic:equal>
                   		 
                   		  <logic:equal name="frmRuleCondition" property="enableVoidRule" value="true">	
                   		  <fmtRuleCondtionTab:message key="BTN_VOID" var="varVoid"/>
						<gmjsp:button value="${varVoid}" gmClass="button" buttonType="Save" onClick="fnVoidRule(this.form);"/>&nbsp;&nbsp;
						 </logic:equal>
						 <logic:equal name="frmRuleCondition" property="enableVoidRule" value="false">	
						<gmjsp:button value="${varVoid}" gmClass="button" disabled="true" buttonType="Save"  onClick="fnVoidRule(this.form);"/>&nbsp;&nbsp;
						 </logic:equal>
                    </td>
                   
                  
				</tr>
				<tr><td colspan="2" height="15"></td></tr>
			</table>
		</td>
	</tr>
</table>
<%@ include file="/common/GmFooter.inc" %>

