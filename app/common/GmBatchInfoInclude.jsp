
<%
	/**********************************************************************************
	 * File		 		: GmBatchInfoInclude.jsp
	 * Desc		 		: Common Include for Batch
	 * Version	 		: 
	 * author			: 
	 ************************************************************************************/
%>






<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.HashMap"%>
<!-- common\GmBatchInfoInclude.jsp -->
<%@ taglib prefix="fmtBatchInfoInc" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmtBatchInfoInc:setLocale value="<%=strLocale%>"/>
<fmtBatchInfoInc:setBundle basename="properties.labels.common.GmBatchInfo"/>

<bean:define id="batchID" name="frmBatchInfo" property="batchID" type="java.lang.String"></bean:define>
<bean:define id="batchType" name="frmBatchInfo" property="batchType" type="java.lang.String"></bean:define>
<bean:define id="batchStatus" name="frmBatchInfo" property="batchStatus" type="java.lang.String"></bean:define>
<%
	String strFormName = GmCommonClass.parseNull((String) request.getParameter("FORMNAME"));
	strFormName = strFormName.equals("") ? "frmBatchInfo" : strFormName;
%>
<HTML>
<HEAD>
<TITLE>Globus Medical: Batch Include</TITLE>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/screen.css">


</HEAD>
<body>
<input type="hidden" name="batchID" value="<%=batchID%>"/>
<input type="hidden" name="batchType" value="<%=batchType%>"/>
<input type="hidden" name="batchStatus" value="<%=batchStatus%>"/>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td height="25" class="ShadeRightTableCaption" colspan="6"><fmtBatchInfoInc:message key="LBL_BATCH_INFO"/></td>
	</tr>
	<!-- Custom tag lib code modified for JBOSS migration changes -->
	<tr>
		<fmtBatchInfoInc:message key="LBL_BATCH_ID" var="varBatchId"/>
		<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
				type="RegularText" SFLblControlName="${varBatchId}:" td="false" />
		</td>
		<td align="left">&nbsp;<bean:write name="<%=strFormName%>" property="batchID" />
		</td>
		<fmtBatchInfoInc:message key="LBL_BATCH_INIT_BY" var="varBatchInit"/>
		<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
				type="RegularText" SFLblControlName="${varBatchInit}:" td="false" />
		</td>
		<td align="left">&nbsp;<bean:write name="<%=strFormName%>" property="batchInitiatedByNm"/>
		</td>
		<fmtBatchInfoInc:message key="LBL_RELEASE_BY" var="varReleaseBy"/>
		<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
				type="RegularText" SFLblControlName="${varReleaseBy}:" td="false" />
		</td>
		<td width="20%" align="left" width="25">&nbsp;<bean:write name="<%=strFormName%>" property="batchReleasedByNm" />
		</td>
	</tr>
	<tr>
		<td colspan="6" class="LLine" height="1"></td>
	</tr>
	<tr class="Shade">
		<fmtBatchInfoInc:message key="LBL_BATCH_STATUS" var="varBthStatus"/>
		<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
				type="RegularText" SFLblControlName="${varBthStatus}:" td="false" />
		</td>
		<td align="left">&nbsp;<bean:write name="<%=strFormName%>" property="batchStatusNm"/>
		</td>
		<fmtBatchInfoInc:message key="LBL_BATCH_INIT_DT" var="varBthInitDt"/>
		<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
				type="RegularText" SFLblControlName="${varBthInitDt}:" td="false" />
		</td>

		
		<td align="left">&nbsp;<fmtBatchInfoInc:formatDate value="${frmBatchInfo.batchInitiatedDt}" pattern="<%=strGCompDateFmt%>"/>
		</td>
		<fmtBatchInfoInc:message key="LBL_RELEASE_DT" var="varReleaseDT"/>
		<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
				type="RegularText" SFLblControlName="${varReleaseDT}:" td="false" />
		</td>
		<td width="20%" align="left" width="25">&nbsp;<fmtBatchInfoInc:formatDate value="${frmBatchInfo.batchReleasedDt}" pattern="<%=strGCompDateFmt%>"/>
		</td>
	</tr>
	<tr>
		<td colspan="6" class="LLine" height="1"></td>
	</tr>
	<tr>
		<fmtBatchInfoInc:message key="LBL_BATCH_TYPE" var="varBthType"/>
		<td height="25" align="Right" class="RightTableCaption"><gmjsp:label
				type="RegularText" SFLblControlName="${varBthType}:" td="false" />
		</td>
		<td align="left" colspan="5">&nbsp;<bean:write name="<%=strFormName%>" property="batchTypeNm"/>
		</td>		
	</tr>
</table>
</body>
<%@ include file="/common/GmFooter.inc" %>