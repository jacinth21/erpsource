<%
/**********************************************************************************
 * File		 		: GmRuleContainer.jsp
 * Desc		 		: Screen fro Rule Engine
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>

<!--common\GmRuleContainer.jsp -->



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.Date"%>
<%@page import="java.net.URLEncoder"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.Iterator,java.util.HashMap" %>
<%@ taglib prefix="fmtRuleContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRuleContainer.jsp -->
<fmtRuleContainer:setLocale value="<%=strLocale%>"/>
<fmtRuleContainer:setBundle basename="properties.labels.common.GmRuleContainer"/>  
<bean:define id="strOpt" name="frmRuleCondition" property="strOpt" type="java.lang.String"> </bean:define>
<bean:define id="ruleId" name="frmRuleCondition" property="ruleId" type="java.lang.String"> </bean:define>
<bean:define id="hmTrans" name="frmRuleCondition"  property="hmTrans" type="java.util.HashMap"></bean:define>
<%

String strProdmgntJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_PRODMGMNT");
String strCondSeclected = "";
String strConsSeclected = "selected";
if(strOpt.equals(""))
{	
	strOpt = "load";
	strCondSeclected = "selected";
	strConsSeclected = "";
	
}
else if(strOpt.equals("edit") && (!ruleId.equals("") || !ruleId.equals("0")))
{
	strOpt = "edit";
	strCondSeclected = "selected";
	strConsSeclected = "";
}
else if(strOpt.equals("save"))
{
	strOpt = "load";
	strCondSeclected = "";
	strConsSeclected = "selected";
}
Iterator itrRuleTrans = hmTrans.keySet().iterator();	
int m = 0;
String strRuleTrans = "";
String strRuleTransKey = "";

while(itrRuleTrans.hasNext())
 {
	String strKey = (String)itrRuleTrans.next();
	strRuleTransKey = strRuleTransKey +"^"+strKey;
	strRuleTrans = strRuleTrans+"^"+(String)hmTrans.get(strKey);			
	m++;
 } 

//Get the company information from gmDataStoreVO
		String strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
		strCompanyInfo = URLEncoder.encode(strCompanyInfo);
//System.out.println("TESTKEY = "+strRuleTransKey);
//System.out.println("TEST = "+strRuleTrans);
//System.out.println("Test...Rule id in......Conditiontab.."+ruleId +"...strOpt...."+ strOpt);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Rule Setup</title>

<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<style type="text/css" media="all">
	@import url("<%=strCssPath%>/screen.css");
</style>
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.css">
<script language="JavaScript" src="<%=strJsPath%>/GmCommonValidation.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmFilter.js"></script>
<script language="JavaScript" src="<%=strProdmgntJsPath%>/GmRule.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxCalendar/dhtmlxcalendar.js"></script>

<script type="text/javascript">
var ruleTransKey = '<%=strRuleTransKey%>';
var ruleTrans  = '<%=strRuleTrans%>';
var ruleTransKeyArray = new Array();
ruleTransKeyArray = ruleTransKey.split("^");
var ruleTransArray = new Array();
ruleTransArray = ruleTrans.split("^");
</script>
</head>
<body leftmargin="20" topmargin="10" onload="fnAjaxTabLoad();">
<html:form action="/gmRuleCondition.do" >
<html:hidden property="strOpt"/>
<html:hidden property="hcurrentTab" value="Rule_Setup"/>
<html:hidden property="conditionsInputStr"/>
<html:hidden property="transStr"/>
<html:hidden property="ruleId"/>
<input type="hidden" name="ruleIdtemp" value="<%=ruleId%>"/>
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader">&nbsp;<fmtRuleContainer:message key="LBL_RULE_SETUP"/></td>
			<td align="right" class="RightDashBoardHeader" > 	
				<fmtRuleContainer:message key="LBL_HELP" var="varHelp"/>
				<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' 
				onClick="javascript:fnRuleHelp('<%=strWikiPath%>');" /> 
			</td>
		</tr>
		<tr>
			<td colspan="2" height="1" bgcolor="#666666"></td>
		</tr>		
		<tr>
			<td colspan="2">
				
				<ul id="maintab" class="shadetabs">		
					<fmtRuleContainer:message key="LBL_CONDITION" var="varCondition"/>
					<fmtRuleContainer:message key="LBL_CONSEQUENCE" var="varConsequence"/>		
					<li ><a href="/gmRuleCondition.do?companyInfo=<%=strCompanyInfo %>&strOpt=<%=strOpt%>&strAction=Reload&ruleId=<%=ruleId %>" title="${varCondition}" rel="ajaxcontentarea" class=<%=strCondSeclected %> name="Condition"><fmtRuleContainer:message key="LBL_CONDITION"/></a></li>					
					<li ><a href="/gmRuleConsequence.do?companyInfo=<%=strCompanyInfo %>&strOpt=<%=strOpt%>&strAction=Reload&ruleId=<%=ruleId %>" title="${varConsequence}" rel="ajaxcontentarea" class=<%=strConsSeclected %> name="Consequence"><fmtRuleContainer:message key="LBL_CONSEQUENCE"/></a></li>
				</ul>
				<div id="ajaxdivcontentarea" class="contentstyle" style="display:visible;height:650;overflow:auto;" >
				</div>			
			</td>
		</tr>				
</table>
<%@ include file="/common/GmFooter.inc" %>
</html:form>
</body>
</html>