<%
/**********************************************************************************
 * File		 		: GmRulePictureDisplay.jsp
 * Desc		 		: This screen is used to display the picture messages for the rules
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>

<!--common\GmRulePictureDisplay.jsp -->


<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%> 
<%@ page import ="com.globus.common.beans.GmCommonControls"%>
<%@ taglib prefix="fmtRulePictureDisplay" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRulePictureDisplay.jsp -->
<fmtRulePictureDisplay:setLocale value="<%=strLocale%>"/>
<fmtRulePictureDisplay:setBundle basename="properties.labels.common.GmRulePictureDisplay"/>  
<%
String strWikiTitle = GmCommonClass.getWikiTitle("RULE_PICTURES");
%>
<html>
<title>Rule Picture Messages</title>

<style type="text/css" media="all">
     @import url("styles/screen.css");
</style>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javascript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>	

<%
ArrayList alList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("RESULT"));
String strReturnValue = "";
HashMap hmMap = new HashMap();
String strComments = "";
String strFileName = "";
String strFileID = "";

double intRowSize = 2; 
double imageSize = alList.size();
double i = Math.abs(imageSize / intRowSize);
int totalRows = (int) Math.ceil(i) ;
int totalTds = (int)(totalRows * intRowSize);
int lastRowColspan = (int)(totalTds - imageSize) + 1 ;
String strPath = "/rule/images";

%>
<BODY leftmargin="20" topmargin="10" >

 <table border="0" class="DtTable700" cellspacing="0" cellpadding="1"> 
		<tr>
			<td colspan="2" height="20" class="RightDashBoardHeader"><fmtRulePictureDisplay:message key="LBL_PICTURE_MESSAGE"/> </td>	 
			<td  height="20" class="RightDashBoardHeader" align="right">
			<fmtRulePictureDisplay:message key="LBL_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />
			</td>
		</tr>	
<% 
try
{
	 if (imageSize ==0)
	 {
		 strReturnValue = "<tr> <td height=50 align=center valign =center> <b> No Search Result Found </b></td></tr>"; 
	 }
	  
	for (int count =0 ; count < imageSize  ; count++) 	
		{		   
		    if ( count % intRowSize == 0) 
		    {
		    	strReturnValue += "<tr>";
		    }

		    if (count == (imageSize - 1) )
		    {		    	
		    	strReturnValue += "<td colspan="+lastRowColspan+">";	
		    }
		    else
		    {
		    	strReturnValue += "<td>";  
		    }
			hmMap =(HashMap)alList.get(count);
					 
			strComments = (String)hmMap.get("COMMENTS");
		//	strFileName = (String)hmMap.get("FILE_NAME");
			strFileID = (String)hmMap.get("FILEID");
			strReturnValue += "<table><tr><td>";			
		//	strReturnValue += "<a href="+strPath +"/"+ strFileName+">";
		//	strReturnValue += "<img src="+strPath +"/"+strFileName+" height=80 width=80 /> ";
			strReturnValue += "<a href="+"/GmCommonFileOpenServlet?uploadString=CONSUPLOADHOME&sId="+ strFileID+">";
			strReturnValue += "<img src="+"/GmCommonFileOpenServlet?uploadString=CONSUPLOADHOME&sId="+ strFileID + " height=540 width=330 /> ";
			strReturnValue += "</a>";
			strReturnValue += "</td></tr>";
			strReturnValue += "<tr><td>";
			strReturnValue += strComments;
			strReturnValue += "</td></tr></table>";
			strReturnValue += "</td> ";
			
			 if ( (count + 1) % intRowSize == 0) 
			    {
			    	strReturnValue += "</tr> <tr><td colspan= "+(intRowSize + 1)+" class='Line'></td></tr><tr><td >&nbsp;</td></tr>";
			    	
			    }						
		}
out.println(strReturnValue);
}
catch(Exception ex)
{
	
}
%>	
</table>
</BODY>
</html>