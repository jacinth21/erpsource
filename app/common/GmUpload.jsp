
<%
	/**********************************************************************************
	 * File		 		: GmUpload.jsp
	 * Desc		 		: Upload - Upload page
	 * Version	 		: 1.0
	 * author			: Tarika Chandure
	 ************************************************************************************/
%>


<!--common\GmUpload.jsp -->



<%@ page import="com.globus.common.beans.GmCommonClass"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ taglib prefix="fmtUpload" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmUpload.jsp -->
<fmtUpload:setLocale value="<%=strLocale%>"/>
<fmtUpload:setBundle basename="properties.labels.common.GmUpload"/> 
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<html>
<head>
<title></title>

<style TYPE="text/css">
table.dotborder {
	border-width: 1px;
	border-style: dotted;
	border-color: #cccccc;
}

td.TableCaption {
	font-family: Verdana, Helvetica, sans-serif;
	background: #EEEEEE;
	font-size: 11px;
	line-height: 20px;
	font-weight: bold;
}
</style>

<% 
	String strOverwrite = request.getParameter("Confirm_Radiographs");
	String strButtonDisabled = "";
	strButtonDisabled = GmCommonClass.parseNull((String)request.getAttribute("strButtonDisabled"));	
%>


<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FFFFFF" text="#000000">
<ul class="style1">
</ul>
<script>
var overwrite = '<%=strOverwrite%>';


function toggledisplay()
{
  var objDivStyle = eval('document.all.pgupload.style');
  objDivStyle.display = 'none';
  
  var objDivStyle1 = eval('document.all.pgtitle.style');
  objDivStyle1.display = 'block';
  
  var objDivStyle2 = eval('document.all.pgbar.style');
  objDivStyle2.display = 'block';
}

 
</script>

<input type="hidden" name="todo" value="upload">
<input type="hidden" name="addDirName" value="PostOP">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td width="100%" height="90" valign="top">
		<div>
		<table border="0" class="dotborder" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2"><br></td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#cccccc"></td>
			</tr>
			<tr id="pgupload" style="display: block">
				<td class="RightText" width="40%" align="right"><b><fmtUpload:message key="LBL_SELECT_FILE_TO_UPLOAD"/> :</b></td>
				<td align="center">
					<input type="file" name="uploadfile" size="50" accept="*.csv">
				
				</td>
			</tr>
<%
		String strDisableBtnVal = strButtonDisabled;
	if(strDisableBtnVal.equals("disabled"))
	{
		strDisableBtnVal = "true";
	}
%>
			<tr>
				<fmtUpload:message key="BTN_UPLOAD" var="varUpload"/>
				<td colspan="4" align="center"><br><gmjsp:button name="Submit" buttonType="Save" value="${varUpload}" gmClass="button"
					onClick="upload();" disabled="<%=strDisableBtnVal%>"  /></td>
			</tr>
			<tr id="pgtitle" style="display: none">
				<td colspan=2 align="center"><fmtUpload:message key="LBL_UPLOAD_IN_PROGRESS"/></td>
			</tr>
			<tr id="pgbar" style="display: none">
				<td colspan=2 align="center"><img
					src="<%=strImagePath%>/progress_bar.gif"></td>
			</tr>
		</table>
		</div>
		</td>
</table>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>
