


<!--common\GmIncludeLogAjax.jsp -->


<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ taglib prefix="fmtIncludeLogAjax" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmIncludeLogAjax.jsp -->
<fmtIncludeLogAjax:setLocale value="<%=strLocale%>"/>
<fmtIncludeLogAjax:setBundle basename="properties.labels.common.GmIncludeLogAjax"/>  

<%
String strCommonJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_COMMON");
String strLogCaption = "Comments";
String strLogTableHeader = "";
String strFormName = "";
String strArrayListName = "";
String strLogReasonLabel = "txt_LogReason";
String strComments = "";
String tabIndex ="";
String strShowCam= "";
String strLogWidth = "100%";
String strMandatory = GmCommonClass.parseNull(request.getParameter("Mandatory"));
String strAuditType = GmCommonClass.parseNull(request.getParameter("AuditType"));

if 	(strMandatory.equals("yes"))
{
	strLogCaption="<span style=\"color: red;\">*</span>&nbsp;"+strLogCaption;

}	
%>
<script language="javascript" src="<%=strCommonJsPath%>/GmIncludeLogAjax.js"></script>
<table border=0 Width="<%=strLogWidth%>" cellspacing=0 cellpadding=0 style="word-wrap: break-word;">
<tr class="ShadeRightTableCaption">
	<td Height="24" colspan="3">&nbsp;<%=strLogCaption%></td>
</tr>

	<tr><td height="1" colspan="3" bgcolor="#666666"></td></tr>		
	<tr>
	    <td colspan="3" height="50"  >
	    <input type="hidden" name="hShowCam" value="<%=strShowCam%>">
	    &nbsp;<textarea id="comments" name="<%=strLogReasonLabel%>" class="InputArea" onFocus="changeBgColor(this,'#AACCE8');" 
		onBlur="changeBgColor(this,'#ffffff');"  rows=3 cols=100 <%=tabIndex%>><%=strComments%></textarea>
		</td>
	</tr>
	<tr><td colspan="3" height="1" bgcolor="#666666"></td></tr>
	<TR><TD colspan=3 ><div id="divShowHide"></div></TD></TR>
	<tr bgcolor="#EEEEEE" class="RightTableCaption">
		<td width="140" height="25">&nbsp;<fmtIncludeLogAjax:message key="LBL_DATE"/></td>
		<td width="140"><fmtIncludeLogAjax:message key="LBL_USERNAME"/> </td>
		<td width="500"><fmtIncludeLogAjax:message key="LBL_DETAILS"/></td>
	</tr>
	<TR><TD colspan=3 height=1 class=Line></TD></TR>
	<TR><TD colspan=3 ><div id="divCommentTable" style="overflow:auto;height:80"></div></TD></TR>

<TR><TD colspan=3 height=1 class=Line></TD></TR>
</table>
