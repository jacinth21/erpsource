 <%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmRuleDisplay.jsp
 * Desc		 		: To display the Rule Messages in display tag 
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>


<!--common\GmRuleDisplay.jsp -->


<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,java.util.Iterator,com.globus.common.beans.GmCommonClass" %>
<%@ taglib prefix="fmtRuleDisplay" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmRuleDisplay.jsp -->
<fmtRuleDisplay:setLocale value="<%=strLocale%>"/>
<fmtRuleDisplay:setBundle basename="properties.labels.common.GmRuleDisplay"/> 
<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<script>
</script>
<body leftmargin="20" topmargin="10">
<%	
GmResourceBundleBean gmResourceBundleBeanlbl = 
GmCommonClass.getResourceBundleBean("properties.labels.common.GmRuleDisplay", strSessCompanyLocale);
	String strPath = strImagePath + "/hold.png";
	String strMsgBgColor ="#CCCCFF";
	String strMsgFont = "#663399";
	String strBorderColor = "black";
	String strBgColor = "6666FF";
	String strHeader = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_RULE_MESSAGE"));
	String strErrorString = GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TRANSACTION_HOLD"));
%>
<center>
<table border="0" cellpadding="0" cellspacing="0" bordercolor="<%=strBorderColor%>" class="DtTable800">
      <tr bgcolor="<%=strBgColor%>">
        <td  colspan="2" align="center" class="RightTableCaption" height="25"><font
        color="#FFFFFF"><%=strHeader%></strong></font></td>
      </tr>
      <tr><td colspan="2"  class="Line"></td></tr> 
      <tr align="center" valign="center">
        <td bgcolor=<%=strMsgBgColor%> height="40" align="right" width='30%' >       
            <img src="<%=strPath%>" width="45" height="45">
            </td>
            <td bgcolor=<%=strMsgBgColor%> align="left">
            <font color=<%=strMsgFont%> size="2"> <b>        
          <%=strErrorString%> 
          </b>   </font>   
        </td>
      </tr>
      
      <tr><td  colspan="2"  class="Line"></td></tr>      
<tr>
<td  colspan="2" >
		<jsp:include page="/common/GmRuleDisplayInclude.jsp"> 
		<jsp:param name="Show" value="true" /></jsp:include>
</td>
</tr>
</table>
</center>
<%@ include file="/common/GmFooter.inc" %>
</body>