<%@page import="com.globus.common.beans.GmResourceBundleBean"%>
<%
/**********************************************************************************
 * File		 		: GmPriceHistoryLogDetail.jsp
 * Desc		 		: display required date history info and request id
 * Version	 		: 1.0
 * author			: JReddy
************************************************************************************/
%>

<!--common\GmPriceHistoryLogDetail.jsp -->


<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc" %> 
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
 <%@ taglib prefix="fmtAuditTrailDetail" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmAuditTrailDetail.jsp -->
<fmtAuditTrailDetail:setLocale value="<%=strLocale%>"/>
<fmtAuditTrailDetail:setBundle basename="properties.labels.common.GmAuditTrailDetail"/>
<bean:define id="ldtResult" name="frmAuditTrail" property="ldtResult" type="java.util.List"></bean:define>
<bean:define id="auditId" name="frmAuditTrail" property="auditId" type="java.lang.String"></bean:define>
<bean:define id="strButtonAccessFl" name="frmAuditTrail" property="strButtonAccessFl" type="java.lang.String"></bean:define>
 
<HTML>
<HEAD>
<TITLE> GlobusOne Enterprise Portal: Set Mapping </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
 
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/common/GmPriceHistoryLogDetail.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
	var strModeName = '<%=auditId%>';
</script>
<%
GmResourceBundleBean gmResourceBundleBeanlbl = GmCommonClass.getResourceBundleBean("properties.labels.common.GmAuditTrailDetail", strSessCompanyLocale);
String value=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_VALUE"));
String transId=GmCommonClass.parseNull((String)gmResourceBundleBeanlbl.getProperty("LBL_TRANSACTION_ID"));
String strScreenHeader = "";
String strAlign ="alignleft";
String strModeCheck = auditId;
boolean exportValue=false;
//PC-5504 - to disabled/Enable the select price button based on the strButtonAccessFl value
String strBtnDisabled ="true";
if(strButtonAccessFl.equals("Y")){
	strBtnDisabled ="false";
}

%>
 

</HEAD>

<BODY leftmargin="20" topmargin="10">
<!-- PC-5504 -  below lines added for do select price functionality-->
<html:form action="/gmPriceHistoryLog.do">
<input type="hidden" name="hInputStr" value="">
<input type="hidden" name="auditId" value="">
<input type="hidden" name="txnId" value="">
	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
	
	  <% if(!strScreenHeader.equals("")) { %>
		<tr>
			<td colspan="3" height="25" class="RightDashBoardHeader"><%=strScreenHeader%> </td>
		</tr>
	 <% } %>	
	 
		 <tr>
	                     <td colspan="3">
	                     	<display:table export="<%=exportValue%>" name="requestScope.frmAuditTrail.ldtResult" class="its" id="currentRowObject" requestURI="/gmPriceHistoryLog.do"  decorator="com.globus.common.displaytag.beans.DTAuditDisplayWrapper"  > 
							<display:column property="REQID" title="<%=transId%>"    class="alignleft"/>
						    <display:column property="RVALUE" title="<%=value%>"    class="<%=strAlign %>"/>
						    <fmtAuditTrailDetail:message key="LBL_PRICE_REQID" var="varPriceReqId"/><display:column property="PRICE_REQ_ID" title="${varPriceReqId}" class="alignleft"/>
							<fmtAuditTrailDetail:message key="LBL_UPDATED_DATE" var="VarUpdatedDate"/><display:column property="UPDATE_DATE" title="${VarUpdatedDate}" class="alignleft"/>							 
							<fmtAuditTrailDetail:message key="LBL_UPDATED_BY" var="varUpdatedBy"/><display:column property="UPDATEDBY" title="${varUpdatedBy}"   class="alignleft"/> 
							</display:table>

    		             </td>
                    </tr> 
       <!-- PC-5504 - Select price button-->
        <tr id="btn_row"> <td align="center" colspan="3">
		<fmtAuditTrailDetail:message key="BTN_SELECT_PRICE" var="varPrice"/>
		<gmjsp:button value="&nbsp;${varPrice}&nbsp;"  name="BTN_Select_Price" gmClass="button" onClick="fnSave();" buttonType="Save" disabled="<%=strBtnDisabled%>"/>
		
		</td>
		</tr>
		 
    </table>		     	
 </html:form>
 <%@ include file="/common/GmFooter.inc" %>

</BODY>
</HTML>

