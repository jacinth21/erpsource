<%@page import="java.util.Locale"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ include file="/common/GmHeader.inc" %>

<%@ taglib prefix="fmtCrossTabFilter" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>


<!-- GmCrossTabFilterInclude.jsp -->


<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>
<fmtCrossTabFilter:setLocale value="<%=strLocale%>"/>
<fmtCrossTabFilter:setBundle basename="properties.labels.common.GmCrossTabFilterInclude"/>  
<%

String strAction = GmCommonClass.parseNull(request.getParameter("HACTION"));
HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
ArrayList alMonthDropDown = new ArrayList();
ArrayList alYearDropDown = new ArrayList();

alMonthDropDown = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALMONTHDROPDOWN"));
alYearDropDown = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ALYEARDROPDOWN"));

String strToMonth = "";
String strToYear ="";
String strFrmMonth = "";
String strFrmYear = "";

strFrmMonth = GmCommonClass.parseNull((String)request.getAttribute("Cbo_FromMonth"));
strFrmYear = GmCommonClass.parseNull((String)request.getAttribute("Cbo_FromYear"));
strToMonth = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToMonth"));
strToYear = GmCommonClass.parseNull((String)request.getAttribute("Cbo_ToYear"));

%>

<table cellSpacing=0 cellPadding=0  border=0>
<!-- Custom tag lib code modified for JBOSS migration changes -->
<tr height=30>
	<TD class=RightTableCaption align="right"><fmtCrossTabFilter:message key="LBL_FROM_DATE"/></TD>
	<TD>&nbsp;
		<gmjsp:dropdown controlName="Cbo_FromMonth" seletedValue="<%=strFrmMonth %>" value="<%=alMonthDropDown%>" 
			codeId = "CODENMALT"  codeName = "CODENM" tabIndex="1"/>
	</TD>
	<TD>&nbsp;
		<gmjsp:dropdown controlName="Cbo_FromYear"  seletedValue="<%=strFrmYear%>" value="<%=alYearDropDown%>" 
			codeId = "CODENM"  codeName = "CODENM"  tabIndex="2" />
	</TD>
	<TD class=RightTableCaption align="right"><fmtCrossTabFilter:message key="LBL_TO_DATE"/>:</TD>
	<TD>&nbsp;
		<gmjsp:dropdown controlName="Cbo_ToMonth"  seletedValue="<%=strToMonth %>" value="<%=alMonthDropDown%>" 
			codeId = "CODENMALT"  codeName = "CODENM"  tabIndex="3" />
	</TD>
	<TD>&nbsp;
		<gmjsp:dropdown controlName="Cbo_ToYear"  seletedValue="<%=strToYear%>" value="<%=alYearDropDown%>" 
			codeId = "CODENM"  codeName = "CODENM"  tabIndex="4" />
	</TD>
	<td  colspan="2"></td>
	<fmtCrossTabFilter:message key="BTN_LOAD" var="varLoad"/>
	<TD rowspan="2" class=RightTableCaption align="center" width="150"> <gmjsp:button value="${varLoad}" style="width: 5em;" buttonType="Load" gmClass="button" onClick="javascript:fnGo();" tabindex="5"/></TD>
</TR>
<TR height=30>
	<TD class=RightTableCaption align="right">&nbsp;<fmtCrossTabFilter:message key="LBL_SHOW_GROWTH_PERCENT"/>:</TD>
	<TD colspan="2">&nbsp;<input type="checkbox" name="Chk_ShowPercentFl" value=1 tabindex=6></TD>
	<TD class=RightTableCaption align="right" width="130">&nbsp;<fmtCrossTabFilter:message key="LBL_SHOW_GROWTH_ARROW"/>:</TD>
	<TD colspan="2">&nbsp;<input type="checkbox" size="30"  name="Chk_ShowArrowFl" value=1 tabindex=7></TD>
	<TD class=RightTableCaption align="right"><fmtCrossTabFilter:message key="LBL_HIDE_VALUE"/>:</TD>
	<TD>&nbsp;<input type="checkbox" size="30"  name="Chk_HideAmountFl" value=1 tabindex=7></TD>
	
	 
</TR>
</TABLE>
