<%
/**********************************************************************************
 * File		 		: GmRuleConsequenceTab.jsp
 * Desc		 		: Screen fro Rule Engine
 * Version	 		: 1.0
 * author			: Ritesh Shah
************************************************************************************/
%>


<!--common\GmConsequenceTab.jsp -->


<%@ include file="/common/GmHeader.inc" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import="java.util.ArrayList,java.util.Vector, java.net.URLEncoder" %>

<%
Object bean = pageContext.getAttribute("frmRuleCondition", PageContext.REQUEST_SCOPE);
pageContext.setAttribute("org.apache.struts.taglib.html.BEAN", bean, PageContext.REQUEST_SCOPE);
%>
<bean:define id="alConsTabList" name="frmRuleCondition"  property="alConsTabList" type="java.util.ArrayList"></bean:define>
<bean:define id="strOpt" name="frmRuleCondition"  property="strOpt" type="java.lang.String"></bean:define>
<bean:define id="ruleId" name="frmRuleCondition"  property="ruleId" type="java.lang.String"></bean:define>
<bean:define id="ruleErrorMsg" name="frmRuleCondition" property="ruleErrorMsg" type="java.lang.String"> </bean:define>
<%
    //The following code added for passing the company info to the child js fnFilterLoad function
	String strCompanyId = gmDataStoreVO.getCmpid();
	String strPlantId = gmDataStoreVO.getPlantid();
	String strPartyId = gmDataStoreVO.getPartyid();
	
	String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\"}";
	strCompanyInfo = URLEncoder.encode(strCompanyInfo);
%>
<%
try
{
	int alConsTabListSize = 0;
	String strAction = "";
	if(strOpt.equals("load") || strOpt.equals("edit"))
	{
		strAction = "reload";
	}
	if (ruleErrorMsg.equals(""))
	{
		
%>
<script>
var companyInfoObj = '<%=strCompanyInfo%>';
</script>
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td width="848" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr><td class="LLine"></td></tr> 
                    <tr>
						<td align=center>
						<ul id="consqtab" class="shadetabs" >
						<%
							HashMap hmapValue = new HashMap();
							String strName = "";
							String strAltName = "";
							String strID = "";
							String strConGrp = "";
							String strClass = "selected";
							int Headersize = alConsTabList.size();
							int cnt = 0;
							for (int i = 0; i < Headersize; i++)
							{ 
								hmapValue = (HashMap) alConsTabList.get(i);
								strName = (String)hmapValue.get("CODENM");
								strID = (String)hmapValue.get("CODEID");
								strConGrp = (String)hmapValue.get("CODENMALT");								
							  	if (i > 0)
							  	{
							  		strClass = "";
							  	}
							  	if (strConGrp.equals("CQPIC"))
							  	{
						%>
								<li><a href="/GmConsUploadServlet?ruleId=<%=ruleId%>&strOpt=<%=strOpt %>&strAction=upload&consequenceId=<%=strID%>&consequenceGrp=<%=strConGrp%>&companyInfo=<%=strCompanyInfo%>" rel="#iframe" name=<%=strConGrp%> >Picture</a></li>
						<%	  		
							  	}
							  	else
							  	{
						%>									 
								<li><a href="/gmRuleConsequence.do?ruleId=<%=ruleId%>&strOpt=<%=strOpt %>&strAction=<%=strAction %>&consequenceId=<%=strID%>&consequenceGrp=<%=strConGrp%>&companyInfo=<%=strCompanyInfo%>" rel="ajaxcontentarea" title="<%=strName%>" class=<%=strClass%> name=<%=strConGrp%>><%=strName%></a></li>
						<% 		}
							}
						%>	
						
						</ul> 						  
						<div id="ajaxdivtabarea" class="contentstyle" style="display:visible;height: 565px; overflow: hidden; " >						
							
						</div>
						</td>						
					</tr>
					
					<script type="text/javascript">	
						var consqtab=new ddajaxtabs("consqtab", "ajaxdivtabarea");				
					    consqtab.setpersist(false);								
					    consqtab.init();						   				  											    
					</script>			
								
			   	</table>
  			   </td>
  		  </tr>
</table>
<% 
	}
	else{		
%>
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">		
		<tr>
			<td colspan="2" height="25" align="center"><font color="red">&nbsp;<%= ruleErrorMsg%>&nbsp;</font></td>
		</tr>
</table>
<%		
	}
} catch(Exception exp) {
exp.printStackTrace();
}
%>

