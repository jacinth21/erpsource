<%
/**********************************************************************************
 * File		 		: GmJobSetup.jsp
 * Desc		 		: Setting up Job
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>
<%@ taglib prefix="fmtJobSetup" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ include file="/common/GmHeader.inc" %>
<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>


<fmtJobSetup:setLocale value="<%=strLocale%>"/>
<fmtJobSetup:setBundle basename="properties.labels.common.util.GmJobSetup"/>
<HTML>
<HEAD>
<TITLE> Globus Medical: Job Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/date-picker.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>
<script>
var lblJobSchedule = '<fmtJobSetup:message key="LBL_JOB_SCHEDULE"/>';
var lblFirstOccre = '<fmtJobSetup:message key="LBL_FIRST_OCCURE"/>';
var lblComments = '<fmtJobSetup:message key="LBL_COMMENTS"/>';
</script>
<script>

function fnFetch()
{	
	document.frmJobSetup.strOpt.value = "edit";
	document.frmJobSetup.submit();
}

function fnSubmit()
{
	fnValidateDropDn('jobScheduleId',lblJobSchedule);
	fnValidateTxtFld('firstOccurence',lblFirstOccre);
	
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmJobSetup.strOpt.value = 'save';
	fnStartProgress();
	document.frmJobSetup.submit();
}

function fnVoid()
{
		document.frmJobSetup.action ="<%=strServletPath%>/GmCommonCancelServlet";
		document.frmJobSetup.hTxnId.value = document.frmJobSetup.jobId.value;
		document.frmJobSetup.hTxnName.value = document.frmJobSetup.jobName.value;
		document.frmJobSetup.submit();
}

function fnReRun()
{
	fnValidateTxtFld('txt_LogReason',lblComments);
	if (ErrorCount > 0)
		 {
			Error_Show();
			Error_Clear();
			return false;
		}
	document.frmJobSetup.strOpt.value = 'rerun';
	document.frmJobSetup.submit();
}

function fnExecLoad(){
	document.frmJobSetup.strOpt.value = 'executeload';
	document.frmJobSetup.submit();
}

</script>

</HEAD>

<BODY leftmargin="20" topmargin="10">
<html:form action="/jobSetup.do"  >
<html:hidden property="strOpt" value=""/>
<html:hidden property="refId" />
<html:hidden property="jobId" />
<html:hidden property="refType" />
<html:hidden property="refTypeText" />
<html:hidden property="refStatus" />
<html:hidden property="jobName"/>
<html:hidden property="hTxnId" value=""/>
<html:hidden property="hTxnName" value=""/>
<html:hidden property="hCancelType" value="VDJBS"/>

	<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="25" class="RightDashBoardHeader"><fmtJobSetup:message key="LBL_JOB_SETUP"/> </td>
		</tr>
		<tr><td colspan="3" height="1" class="line"></td></tr>
		<tr>
			<td width="698" height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2">
							<html:errors />
						</td>
					</tr>
                    <tr><td colspan="2" class="ELine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="30"></font>&nbsp;<fmtJobSetup:message key="LBL_TYPE"/> :</td> 
	                     <td>&nbsp;
		                     <bean:write name="frmJobSetup" property="refTypeText" />
    		             </td>
                    </tr>  
                    <tr><td colspan="2" class="ELine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtJobSetup:message key="LBL_JOB_NAME"/>:</td> 
                        <td>&nbsp;
    		               <bean:write name="frmJobSetup" property="jobName" />
    		             </td>
                    </tr>   
                    <tr><td colspan="2" class="ELine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtJobSetup:message key="LBL_JOB_SCHEDULE"/>:</td> 
                        <td>&nbsp;
                        <!-- Custom tag lib code modified for JBOSS migration changes -->
    		               <gmjsp:dropdown controlName="jobScheduleId"  SFFormName="frmJobSetup" SFSeletedValue="jobScheduleId" 
							  SFValue="alJobSchedule" codeId = "CODEID"  codeName = "CODENM"  defaultValue= "[Choose One]" />                                                
    		             </td>
                    </tr>   
                    <tr><td colspan="2" class="ELine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24">&nbsp;<fmtJobSetup:message key="LBL_FIRST_OCCURE"/>:</td> 
                        <td>&nbsp;
                        <!-- Struts tag lib code modified for JBOSS migration changes -->
							<html:text property="firstOccurence"  size="10" styleClass="InputArea" onfocus="changeBgColor(this,'#AACCE8');" onblur="changeBgColor(this,'#ffffff');" />&nbsp;
							<img id="Img_Date" style="cursor:hand" onclick="javascript:show_calendar('frmJobSetup.firstOccurence');" title="Click to open Calendar"  
							src="<%=strImagePath%>/nav_calendar.gif" border=0 align="absmiddle" height=18 width=19 />	&nbsp;&nbsp; 
    		             </td>
                    </tr>   
                    <tr><td colspan="2" class="ELine"></td></tr>
                    <tr>
                        <td class="RightTableCaption" align="right" HEIGHT="24"><fmtJobSetup:message key="LBL_INACTIVE_FL"/>:</td> 
                        <td>&nbsp;<html:checkbox property="activeflag" />
                         </td>
                    </tr>
                    <tr><td colspan="2" class="ELine"></td></tr> 
                    	<tr>
						<td colspan="2" align="center" height="30">&nbsp;
	                    	<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmJobSetup" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>	
						</td>
					</tr>
					 <tr><td colspan="2" class="ELine"></td></tr> 
                    <tr>
                    	<td colspan="2" align="center">
                    		<fmtJobSetup:message key="BTN_SUBMIT" var="varSubmit"/>
		                    <gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" onClick="fnSubmit();" /> 
		                    <fmtJobSetup:message key="BTN_VOID" var="varVoid"/>
		                    <gmjsp:button value="&nbsp;&nbsp;${varVoid}&nbsp;&nbsp;&nbsp;" gmClass="button" buttonType="Save" onClick="fnVoid();" /> 
		                <logic:equal name="frmJobSetup" property="refStatus" value="Y">
							<fmtJobSetup:message key="BTN_RERUN" var="varReRun"/>
							<gmjsp:button value="&nbsp;&nbsp;${varReRun}&nbsp;&nbsp;&nbsp;" gmClass="button" buttonType="Save" onClick="fnReRun();" />
						</logic:equal>
						<logic:equal name="frmJobSetup" property="deptId" value="2006">
							<fmtJobSetup:message key="BTN_EXEC_LOAD" var="varExecLoad"/>
							<gmjsp:button value="${varExecLoad}" gmClass="button" buttonType="Save" onClick="fnExecLoad();" />
						</logic:equal>  
		                </td>
                    </tr>
   	</table>
  			   </td>
  		  </tr>	
    </table>		     	
</html:form>
<% } catch(Exception exp) {
exp.printStackTrace();
}
%>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>

