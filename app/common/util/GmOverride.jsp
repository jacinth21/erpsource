<%
/**********************************************************************************
 * File		 		: GmOverride.jsp
 * Desc		 		: Setting up Override
 * Version	 		: 1.0
 * author			: VPrasath
************************************************************************************/
%>
<!-- WEB-INF path corrected for JBOSS migration changes -->
<%@ include file="/common/GmHeader.inc" %>

<%@include file="/common/GmCommonInclude.jsp" %>
<!-- common\util\GmOverride.jsp -->

<%@ page import="com.globus.common.beans.GmCalenderOperations"%>

<%@ page import="java.util.Iterator"%> 

<%@ page import="org.apache.commons.beanutils.BasicDynaBean"%>

<%@ taglib prefix="fmtOverride" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<%
if(!strSessCompanyLocale.equals("")){
	locale = new Locale("en", strSessCompanyLocale);
	strLocale = GmCommonClass.parseNull((String)locale.toString());
	strJSLocale = "_"+strLocale;
}
%>



<fmtOverride:setLocale value="<%=strLocale%>"/>
<fmtOverride:setBundle basename="properties.labels.common.util.GmOverride"/>

<bean:define id="alTrial" name="frmOverride" property="alTrail" type="java.util.ArrayList"></bean:define>
<bean:define id="requestCount" name="frmOverride" property="requestCount" type="java.lang.String"></bean:define>

<%
   String strSubmitFlag = GmCommonClass.parseNull(request.getParameter("submitFlag"));
   String strWikiPath = GmCommonClass.getString("GWIKI");
   String strWikiTitle = GmCommonClass.getWikiTitle("OVERRIDE_GROWTH");
   String strRequestCount = GmCommonClass.parseZero(requestCount);
%>

<%@page import="java.util.Iterator"%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Override Setup </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/DemandSheet.js"></script>

<style type="text/css" media="all">
     @import url("<%=strCssPath%>/screen.css");
</style>

<SCRIPT>
<bean:define id="strOverrideStatus" name="frmOverride" property="pdisablevalue" type="java.lang.String"></bean:define>

function fnSaveComments()
{
    if(document.frmOverride.txt_LogReason.value == '')
    {
    	Error_Details(message[206]);
    }
    if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();					
	}	  
	else
	{
	    document.frmOverride.submitFlag.value = 'true';
		document.frmOverride.strOpt.value = 'savecomments';
		document.frmOverride.submit();
	}
}

function fnSubmit()
{  
  var value = document.frmOverride.newValue.value;
  var reqCount = parseInt("<%=strRequestCount%>",10);
  var type = document.frmOverride.type.value;
  var flag = true; 
    if(isNaN(value))
     {
       Error_Details(message[208]);
     }
     
	if(type == '0' || value == '' || document.frmOverride.txt_LogReason.value == '')
 	{
  		Error_Details(message[206]);
  	}
  
    if(value < reqCount)
  	 {
  	    flag = false;
  	    if(!confirm(message[10571]))
  	     {
  	        return false; 
  	     }  	    
  	 }
    if(type == '20411' && flag)
    {
      if(!confirm(message[10572]))
  	     {
  	        return false; 
  	     }
    }
    
    if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();					
	}	  
	else
	{
	    document.frmOverride.submitFlag.value = 'true';
		document.frmOverride.strOpt.value = 'save';
		document.frmOverride.submit();
	}
}

function fnLoad()
 {
   if ("true" == "<%=strSubmitFlag%>")
    {
      fnClose();
    }
 }

function fnClose()
{
 var url = window.opener.document.URL;
  if(url.indexOf("gmGrowthSetup") != -1)
   {
     window.opener.fnReloadReferences();
   }
  else  
   {
     window.opener.fnFetchGrowthInfo();
   }
  window.close();
}

</SCRIPT>
</HEAD>

<BODY leftmargin="20" topmargin="10" onLoad="javascript:fnLoad()">
<html:form action="/gmOverride.do"  >
<html:hidden property="strOpt" />
<html:hidden property="overrideTypeId" />
<html:hidden property="refId" />
<html:hidden property="references" />
<html:hidden property="refValue" />
<html:hidden property="currentValue" />
<html:hidden property="overrideType" />
<html:hidden property="refIdForLogReason" />
<html:hidden property="partType" />
<html:hidden property="submitFlag" />
<html:hidden property="demandSheetMonthId" />
<html:hidden property="pdisablevalue" />

<table border="0" class="DTTable700" width="700" cellspacing="0" cellpadding="0">
		<tr>
			<td height="100" valign="top">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="4" height="25" class="RightDashBoardHeader">&nbsp;<fmtOverride:message key="LBL_OVERRIDE_PROCESS"/> - <bean:write name="frmOverride" property="overrideType"/></td>
						<fmtOverride:message key="IMG_ALT_HELP" var = "varHelp"/>
						<td align="right" class=RightDashBoardHeader > 	
				<img id='imgEdit' style='cursor:hand' src='/images/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" /> 
				</td>
					</tr>
					<tr><td colspan="5" class="Line" height="1"></td></tr>
					<logic:notEqual name="frmOverride" property="msg" value="">
					<tr><td colspan="5" height="20" align="center" class="RightBlueText"><bean:write name="frmOverride" property="msg"/></td>
					<tr><td class="Line" colspan="5"></td></tr>
					</tr>
					</logic:notEqual>					
					<tr>
						<td class="RightTableCaption" nowrap align="right" HEIGHT="24"><fmtOverride:message key="LBL_REFERENCE"/>:</td>
						<td>&nbsp;<bean:write name="frmOverride" property="references"/></td>
						<td class="RightTableCaption" nowrap align="right" HEIGHT="24"><fmtOverride:message key="LBL_ID"/>:</td>
						<td width="300">&nbsp;<bean:write name="frmOverride" property="refValue"/></td>
					</tr>
					<tr><td colspan="5" class="ELine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" nowrap align="right" HEIGHT="24"><fmtOverride:message key="LBL_CURRNENT_VALUE"/>:</td>
						<td>&nbsp;<bean:write name="frmOverride" property="currentValue"/></td>
						<td class="RightTableCaption" nowrap align="right" HEIGHT="24"><fmtOverride:message key="LBL_NEW_VALUE"/>:</td>
						<td>&nbsp;<html:text property="newValue" size="10" onfocus="changeBgColor(this,'#AACCE8');" styleClass="InputArea" onblur="changeBgColor(this,'#ffffff');"/> 						
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<gmjsp:dropdown controlName="refMonth" SFFormName="frmOverride" SFSeletedValue="refMonth"
									SFValue="alRefMonths" codeId="CODEID" codeName="CODENM"/> &nbsp;
						</td>
					</tr>
					<tr><td colspan="5" class="ELine" height="1"></td></tr>
					<tr>
						<td class="RightTableCaption" nowrap align="right" HEIGHT="24"><fmtOverride:message key="LBL_ACTION"/>:</td>
						<!-- Custom tag lib code modified for JBOSS migration changes -->
						<td colspan="4">&nbsp;<gmjsp:dropdown controlName="type" SFFormName="frmOverride" SFSeletedValue="type"
							SFValue="alTypes" codeId="CODEID" codeName="CODENM" defaultValue="[Choose One]"/>
						</td>
					</tr>
					<tr><td colspan="5" class="Line" height="1"></td></tr>
					<tr class="ShadeRightTableCaption"><td colspan="5" height="25"><fmtOverride:message key="LBL_OVERRIDE_HIST"/></td></tr>
					<tr><td colspan="5" class="Line" height="1"></td></tr>
		            <tr>
		            	<td colspan="5">
				            <display:table name="<%=alTrial%>" class="its" id="currentRowObject" export="false"> 
							<display:column property="LOGID" title="#" class="aligncenter"/>
							<fmtOverride:message key="DT_VALUE" var="varValue"/>
							<display:column property="VALUE" title="${varValue}" class="aligncenter"/>
							<fmtOverride:message key="DT_DATE" var="varDate"/> 
							<display:column property="UDATE" title="${varDate}" />
							<fmtOverride:message key="DT_UPD_BY" var="varUpdBy"/> 
							<display:column property="UPDBY" title="${varUpdBy}" class="alignleft" />
							</display:table>
						</td>
			        </tr>
			        <tr><td colspan="5" class="Line" height="1"></td></tr>
					<tr>
						<td colspan="5"> 
							<jsp:include page="/common/GmIncludeLog.jsp" >
							<jsp:param name="FORMNAME" value="frmOverride" />
							<jsp:param name="ALNAME" value="alLogReasons" />
							<jsp:param name="LogMode" value="Edit" />
							</jsp:include>	
						</td>
					</tr>					
					<tr class="ShadeRightTableCaption"><td colspan="5" height="25"><fmtOverride:message key="LBL_OPEN_MR"/></td></tr>
					<tr><td colspan="5" class="Line" height="1"></td></tr>
					<tr>
					<td colspan="5"> 
					<jsp:include page="/operations/forecast/GmDemandSheetRequestDetail.jsp" >
					<jsp:param name="FORMNAME" value="frmOverride" />
					</jsp:include>		
					</td>			
					</tr>
					<tr><td colspan="5" class="Line" height="1"></td></tr>
				   <tr>
						<td colspan="5" align="center" height="40">&nbsp;
						<fmtOverride:message key="BTN_SUBMIT" var="varSubmit"/>
						<gmjsp:button value="${varSubmit}" gmClass="button" buttonType="Save" disabled="<%=strOverrideStatus %>" onClick="javascript:fnSubmit();" />
						<fmtOverride:message key="BTN_SAVE_COMMENTS" var="varSComments"/>
						<gmjsp:button value="${varSComments}" gmClass="button" buttonType="Save" onClick="javascript:fnSaveComments();" />												
						</td>
					</tr>		

				</table>
			</td>
			
		</tr>
    </table>
</html:form>

</BODY>

</HTML>
