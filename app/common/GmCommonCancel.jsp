<%
/**********************************************************************************
 * File		 		: GmSetConsignRollback.jsp
 * Desc		 		: This screen is used to Rollback Set Consignments
 * Version	 		: 1.0
 * author			: Joe P Kumar
************************************************************************************/
%>

<!--common\GmCommonCancel.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.servlets.GmServlet"%>
<%@ include file="/common/GmHeader.inc"%>
<!-- Imports for Logger -->
<%@ page import="com.globus.common.beans.GmCommonConstants"%> 


<%@ page buffer="16kb" autoFlush="true" %>
<%@ taglib prefix="fmtCommonCancel" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmCommonCancel.jsp -->
<fmtCommonCancel:setLocale value="<%=strLocale%>"/>
<fmtCommonCancel:setBundle basename="properties.labels.custservice.ModifyOrder.GmCommonCancel"/>

<%

	GmServlet gm = new GmServlet();
	gm.checkSession(response,session);
	HashMap hmParam = new HashMap();
	ArrayList alCancelReason ;
	hmParam = (HashMap)request.getAttribute("HMPARAM");
	
	String strAction = (String)request.getAttribute("hAction")==null?"":(String)request.getAttribute("hAction");
	String strCancelType = GmCommonClass.parseNull((String)request.getAttribute("STRCANCELTYPE"));
	String strTxnId = GmCommonClass.parseNull((String)request.getAttribute("STRTXNID"));
	String strTxnName = GmCommonClass.parseNull((String)request.getAttribute("STRTXNNAME"));
	String strCancelTypeNM = GmCommonClass.parseNull((String)request.getAttribute("STRHEADERFROMCODEID"));
	String strSrcDir = GmCommonClass.parseNull((String)request.getAttribute("STRSRC"));
	String strDestDir = GmCommonClass.parseNull((String)request.getAttribute("STRDEST"));
	String strFileName = GmCommonClass.parseNull((String)request.getAttribute("STRFILENAME"));
	String strCancelReturnVal = GmCommonClass.parseNull((String)request.getAttribute("CANCELRETURNVAL"));
	String strEnableEdit = GmCommonClass.parseNull((String)request.getAttribute("ENABLEEDIT"));
	
	String strRedirectURL = GmCommonClass.parseNull((String)request.getAttribute("REDIRECTURL"));
	String strAddnlParam = GmCommonClass.parseNull((String)request.getAttribute("ADDITIONALPARAM"));
	String strDisplayNm = GmCommonClass.parseNull((String)request.getAttribute("DISPLAYNAME"));
	String hSkipCmnCnl = GmCommonClass.parseNull((String)request.getAttribute("HSKIPCMNCNL"));
	String strHComments = GmCommonClass.parseNull((String)request.getAttribute("HLOGREASON"));
	String strJNDIConnection = GmCommonClass.parseNull((String)request.getAttribute("JNDICONNTCTION")); 
	
	// Back to Process Transaction screen to Process CN
	String strHScreenFrom = GmCommonClass.parseNull((String)request.getAttribute("hScreenFrom")); 
	
	if(!strHComments.trim().equals("")){
		strHComments += "\n";
	}
	String strCancelReason = "";
	String strComments = "";
	String strMessage = "";
	String strDisabled = "";
		
	String strTxnValue = strTxnId;
   alCancelReason  = (ArrayList)request.getAttribute("ALCANCELREASON");
   
   log.debug(" -- " + strHComments + " -- values inside hmParam is " + hmParam);
	
	if (hmParam != null)
	{
	     strTxnId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
	     strTxnName = GmCommonClass.parseNull((String)hmParam.get("TXNNAME"));
         strCancelReason = GmCommonClass.parseNull((String)hmParam.get("CANCELREASON"));
         strCancelType = GmCommonClass.parseNull((String)hmParam.get("CANCELTYPE"));
         strComments = GmCommonClass.parseNull((String)hmParam.get("COMMENTS"));
		       
	}
	
	if(strAction.equals("SUCCESSCANCEL"))
	{
		strDisabled = "true";
		strMessage = " Consignment " + strTxnId +"  Successfully Rolled back";
	}
	
	if (!strTxnName.equals("")){
	strTxnValue = strTxnName;
	}
	
	log.debug("strTxnValue is " +strTxnValue);
	
	
	String strReasonLabel = (String) request.getParameter("REASONLABEL");
	if(strReasonLabel != null && !strReasonLabel.equals(""))
	{
		
	}
	
	// Store Current URL - BACKURL
	log.debug("REQUEST   " + request.getHeader("Referer") );//request.getRequestURL().toString());//(request.getAttribute("org.apache.struts.globals.ORIGINAL_URI_KEY")).toString());
	
	 String action = request.getHeader("Referer");//(String)request.getAttribute("org.apache.struts.action.MESSAGE");//org.apache.struts.action.MODULE");//.getRequestURI();//request.getParameter("ORIGINAL_URI_KEY");
	
	 log.debug("URL ------> "+ action);
	
	session.setAttribute("BACKURL",action);
	
	
%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Set Consign Rollback </TITLE>
<link rel="stylesheet" href="<%=strCssPath%>/print.css" type="text/css" media="print" />
<script language="javascript" src="<%=strJsPath%>/AutoList.js"></script>
<script language="javascript" src="<%=strJsPath%>/AutoListCommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<SCRIPT>

function fnSubmit()
{
		var varreason = document.frmCommonCancel.Cbo_Reason.value;
		var varcomments = document.frmCommonCancel.Txt_Comments.value;

		if (varreason == "0")
		{
				Error_Details(message[910]);
		}
		
		if (varcomments == '')
		{
				Error_Details(message[911]);
		} 
		if (varcomments != '' && TRIM(varcomments).length >= '3900')
		{
		         Error_Details(message[10573]);
		}
		if (ErrorCount > 0)
			{
					Error_Show();
					Error_Clear();
					return false;
			}
			
		else {
		document.frmCommonCancel.hAction.value = "cancel";
		fnStartProgress();
		document.frmCommonCancel.submit();	
		}
}

function fnEdit(){
	var codeGroup = document.frmCommonCancel.hCancelType.value; 
	windowOpener('/gmCodeLookupSetup.do?codeGrp='+codeGroup+'&strOpt=void',"EditReasons","top=500,left=300,width=440,height=325");
}
function fnRefresh(){
	document.frmCommonCancel.hAction.value = "Load";
	document.frmCommonCancel.submit();
}
</SCRIPT>
</head>

<BODY leftmargin="20" topmargin="10">
<FORM name="frmCommonCancel" method="POST" action="<%=strServletPath%>/GmCommonCancelServlet">
<input type="hidden" name="hAction" value="">
<input type="hidden" name="hTxnId" value="<%=strTxnId%>">
<input type="hidden" name="hTxnName" value="<%=strTxnName%>">
<input id="hCancelType" type="hidden" name="hCancelType" value="<%=strCancelType%>">
<input type="hidden" name="hSrcDir" value="<%=strSrcDir%>">
<input type="hidden" name="hDestDir" value="<%=strDestDir%>">
<input type="hidden" name="hFileName" value="<%=strFileName%>">
<input type="hidden" name="hCancelReturnVal" value="<%=strCancelReturnVal%>">
<!--Back Button changes  -->
<input type="hidden" name="hRedirectURL" value="<%=strRedirectURL%>">
<input type="hidden" name="hAddnlParam" value="<%=strAddnlParam%>">
<input type="hidden" name="hDisplayNm" value="<%=strDisplayNm%>">
<input type="hidden" name="hScreenFrom" value="<%=strHScreenFrom%>">

<input type="hidden" name="hSkipCmnCnl" value="<%=hSkipCmnCnl%>">
<input type="hidden" name="hJNDIConnection" value="<%=strJNDIConnection%>">


	<table border="0" cellspacing="0" cellpadding="0" width="700">
		<tr>
			<td width="1" class="Line" rowspan="10"></td>
			<td height="1" colspan="2" class="Line"></td>
			<td width="1" class="Line" rowspan="10"></td>
		</tr>
		<tr>
		<% // strCancelTypeNM is the Code Id for Code Name Alt which is set in hCancelType. Eg: RBSCN - Rollback Set Consignment %>
			<td height="25" class="RightDashBoardHeader" colspan="2">&nbsp; <%=strCancelTypeNM%></td>
		</tr>
		<tr><td colspan="2" height="1" class="Line"></td></tr>	
		<tr><td colspan="2" height="30"><%=strMessage%></td></tr>
		<tr>
			<td height="30" width="150" align="right" class="RightTableCaption"><fmtCommonCancel:message key="LBL_TRANSACTIONID" />&nbsp;:&nbsp;</td>
			<td><%=strTxnValue%></td>
		</tr>
		<tr>
			<td height="30" width="150" align="right" class="RightTableCaption">
			<%
				if(strReasonLabel != null && !strReasonLabel.equals(""))
				{
			%>
					<%=strReasonLabel%>&nbsp;:&nbsp;
			<%
				} else {
			%>
					<fmtCommonCancel:message key="LBL_REASON"/>&nbsp;:&nbsp; 
			<%
				}
			%>
			</td>
			<!-- Custom tag lib code modified for JBOSS migration changes -->
			<fmtCommonCancel:message key="LBL_CHOOSEONE" var="varChooseOne" />
			<td><gmjsp:dropdown controlName="Cbo_Reason"  seletedValue="<%= strCancelReason %>" 	
						tabIndex="1"  value="<%= alCancelReason%>" codeId = "CODEID" codeName = "CODENM" defaultValue= "${varChooseOne}"  />&nbsp;&nbsp;
						<fmtCommonCancel:message key="BTN_EDIT" var="varEdit"/>
						<fmtCommonCancel:message key="BTN_REFRESH" var="varRefresh" />
						<gmjsp:button value="&nbsp;${varEdit}&nbsp;" gmClass="button" buttonType="Save" onClick="fnEdit();" />&nbsp;
						<gmjsp:button value="&nbsp;${varRefresh}&nbsp;" gmClass="button" buttonType="Save" onClick="javascript:fnRefresh();" />
			 </td>
		</tr>
		<tr>
			<td class="RightTableCaption" align="right" HEIGHT="24" width="150">&nbsp;&nbsp;<fmtCommonCancel:message key="LBL_COMMENTS" />&nbsp;:&nbsp;</td>
			<td><textarea name="Txt_Comments"  class="InputArea"  onFocus="changeBgColor(this,'#AACCE8');" 
						onBlur="changeBgColor(this,'#ffffff');  checkMaxLength(this.value,'3900','Comments');" rows=5 cols=45 value="" tabindex="9"><%=strHComments%><%=strComments%></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" height="30">
				<fmtCommonCancel:message key="BTN_SUBMIT" var="varSubmit"/>
				<fmtCommonCancel:message key="BTN_CANCEL" var = "varCancel" />
				<gmjsp:button value="&nbsp;${varSubmit}&nbsp;" gmClass="button" disabled="<%=strDisabled%>" buttonType="Save" onClick="fnSubmit();" tabindex="2" />
				<gmjsp:button value="${varCancel}" gmClass="button" buttonType="Load" onClick="history.back()" />
			</td>
		</tr>		
		<tr><td colspan="2" height="1" class="Line"></td></tr>	
	</table>
</FORM>
</BODY>
<%@ include file="/common/GmFooter.inc" %>
</HTML>
