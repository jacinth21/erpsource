 <%
/**********************************************************************************
 * File		 		: GmChangePass.jsp
 * Desc		 		: This screen is used for the changing the password
 * Version	 		: 1.0 
 * author			: Dhinakaran James
************************************************************************************/
%>





<%@ page language="java" %>
<%@ include file="/common/GmHeader.inc"%>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ taglib prefix="fmtChangePass" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmChangePass.jsp -->
<fmtChangePass:setLocale value="<%=strLocale%>"/>
<fmtChangePass:setBundle basename="properties.labels.common.GmChangePass"/>  
<%
	String strExp = GmCommonClass.parseNull((String)session.getAttribute("strExpired"));
	 
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Globus Medical: Change Password</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>
<script>
function fnSubmitLogon()
{
	 
	fnValidateTxtFld('Txt_Username',message[5149]);
	fnValidateTxtFld('Txt_OldPass',message[5150]);
	fnValidateTxtFld('Txt_NewPass',message[5151]);
	fnValidateTxtFld('Txt_NewPass1',message[5152]); 
	   
	var inputs = new Array();
	 
	  inputs[0] = "abcdefghijklmnopqrstuvwxyz";	
	  inputs[1] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	  inputs[2] = "0123456789";
	  inputs[3] = "~!@#$%^&*";
	var counter = 0;
	
	var newpassword = document.fLogon.Txt_NewPass.value;
 	 
	// find out if the password different cases 
	 
	
	var new_length =  document.fLogon.Txt_NewPass.value.length;
	  
	 for (j = 0; j < 4; j++) {
	 
	 if( contains(newpassword, inputs[j])) counter++;
	  
	 }
	  
	 
 
	
	if(counter < 3) 
	Error_Details(message[3001]);
	 
	 if ( new_length < 8)
	Error_Details(message[3003]);
	
	if(!(document.fLogon.Txt_NewPass.value == document.fLogon.Txt_NewPass1.value))
	Error_Details(message[3004]);
	
	if (ErrorCount > 0)
	{
			Error_Show();
			Error_Clear();
			return false;
	}
	else
	{
		document.fLogon.submit();
	}
}

// sees if a password contains one of a set of characters
	function contains(password, validChars) {
	
	    for (i = 0; i < password.length; i++) {
	        var char = password.charAt(i);
	        if (validChars.indexOf(char) > -1) {
	            return true;
	        }
	    }
	
	    return false;
	
	} 
</script>
<STYLE type="text/css">
.portalstyle
{position: absolute; visibility: hidden; left: 750; top: 18;}
.bgimage 
{ background-image: url(<%=strImagePath%>/SpineIT-header_greybar.png); background-repeat: repeat}
</STYLE>
</head>
<body leftmargin="0" topmargin="0" onLoad=document.fLogon.Txt_Username.focus();>
<form name="fLogon" method="post" action= "<%=strServletPath%>/GmLogonServlet">
<input type="hidden" name="hAction" value="ChangePass">
<table border="0" width="770" cellspacing="1" cellpadding="0" bgcolor="#CCCCCC">
  <tr>
    <td class="bgimage" width="100%" height="60" valign="top">
	   <img src="<%=strImagePath%>/GlobusOne-Portal-Logo.jpg" border="0">
    </td>
    <td class="bgimage" valign="top">
	   <img src="<%=strImagePath%>/SpineIT-header_dotsandmap.png" border="0">
	</td>
  </tr>
  <tr>
    <td bgcolor="white" height="200" valign="top"></td>
    <td bgcolor="white" height="200" align="right" valign="top"><br>
		<table border="0" width="370" cellspacing="0" cellpadding="0">
			<tr>
				<td rowspan="16" bgcolor="#666666" width="1"></td>
				<td colspan="2" bgcolor="#666666" height="1"></td>
				<td rowspan="16" bgcolor="#666666" width="1"></td>
			</tr>
			<%  if ( strExp.equals("90890") ) { %>
				<tr>
				<td colspan="4"  height="20" align="center" class = "RightTextRed"><b><fmtChangePass:message key="LBL_PASSWORD_EXPIRED"/>.</b></td>
			</tr>
			<% } %>
			
			<tr>
				<td colspan="4" class="RightDashBoardHeader" height="20" align="center"><fmtChangePass:message key="LBL_CHANGEPASSWORD"/></td>
			</tr>
			<tr height="25">
				<td class="TableText" align="right"><fmtChangePass:message key="LBL_USERNAME"/> : </td>
				<td>&nbsp;<input type="text" size="15" value="" name="Txt_Username" class="InputArea">
			</tr>
			<tr height="25">
				<td class="TableText" align="right"><fmtChangePass:message key="LBL_OLDPASSWORD"/> :</td>
				<td>&nbsp;<input type="password" size="15"  name="Txt_OldPass" class="InputArea">
			</tr>
			<tr height="25">
				<td class="TableText" align="right"><fmtChangePass:message key="LBL_NEWPASSWORD"/> :</td>
				<td>&nbsp;<input type="password" size="15"  name="Txt_NewPass" class="InputArea">
			</tr>
			<tr height="25">
				<td class="TableText" align="right"><fmtChangePass:message key="LBL_CONFIRMPASSWORD"/> :</td>
				<td>&nbsp;<input type="password" size="15"  name="Txt_NewPass1" class="InputArea">
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td  height="30">
				<fmtChangePass:message key="BTN_SUBMIT" var="varSubmit"/>
				<fmtChangePass:message key="BTN_RESET" var="varReset"/>
					<input type="button" value="${varSubmit}" class="button" onClick="javascript:fnSubmitLogon();">&nbsp;
				<!-- 	<input type="button" value="Close" class="button" onClick="javascript:window.close();">	-->
					<input type="reset" value="${varReset}" class="button">
				</td>
			</tr>
			<tr>
				<td colspan="4" height="1" colspan="4" bgcolor="#666666"></td>
			</tr>
			<tr>
				<td colspan="4"  height="20" align="left"  ><b><fmtChangePass:message key="LBL_PASSWORDCRITERIA"/>:</b></td>
			</tr>
			<tr>
				<td colspan="4"  height="20" align="left"  >  <fmtChangePass:message key="LBL_EIGHTCHAR_INLENGTH"/>. </td>
			</tr>
			<tr>
				<td colspan="4"  height="20" align="left"  > <fmtChangePass:message key="LBL_MUST_CHARACTERS"/>: </td>
			</tr>
			<tr>
				<td colspan="4"  height="20" align="left"  > &nbsp;&nbsp;&nbsp;<fmtChangePass:message key="LBL_EIGHTUPPER"/>. </td>
			</tr>
			<tr>
				<td colspan="4"  height="20" align="left"  > &nbsp;&nbsp;&nbsp;<fmtChangePass:message key="LBL_EIGHTLOWER"/>. </td>
			</tr>
			<tr>
				<td colspan="4"  height="20" align="left"  > &nbsp;&nbsp;&nbsp;<fmtChangePass:message key="LBL_BASETENDIGITS"/>. </td>
			</tr>
			<tr>
				<td colspan="4"  height="20" align="left"  > &nbsp;&nbsp;&nbsp;<fmtChangePass:message key="LBL_NONALPHABETICCHARACTERS"/>. </td>
			</tr>
			
			<tr>
				<td colspan="4" height="1" colspan="4" bgcolor="#666666"></td>
			</tr>
		</table>
	</td>
  </tr>
  <tr>
    <td bgcolor="white" align="center" height="30" class="FooterText" colspan="2">
		<fmtChangePass:message key="LBL_ALLRIGHTSRESERVED"/>
	</td>
  </tr>
</table>
		 
	</form>
</BODY>
</HTML>
