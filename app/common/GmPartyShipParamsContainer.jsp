<%
/**********************************************************************************
	* File		 		: GmPartyShipParamsContainer.jsp
	* Desc		 		: 
	* Version	 		: 1.0
	* author			: HReddi
 ************************************************************************************/
%>




<%@ page language="java"%>
<%@ include file="/common/GmHeader.inc"%> 
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap"%>
<%@ taglib prefix="fmtPartyShipParamsContainer" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<!-- GmPartyShipParamsContainer -->
<fmtPartyShipParamsContainer:setLocale value="<%=strLocale%>"/>
<fmtPartyShipParamsContainer:setBundle basename="properties.labels.common.GmPartyShipParamsSetup"/>  
<bean:define id="alShipmode" name="frmPartyShipParamsSetup" property="alShipmode" type="java.util.ArrayList"> </bean:define>
<bean:define id="alAccountList" name="frmPartyShipParamsSetup" property="alAccountList" type="java.util.ArrayList"> </bean:define>
<bean:define id="shipMode" name="frmPartyShipParamsSetup" property="shipMode" type="java.lang.String"> </bean:define>
<bean:define id="shipCarrier" name="frmPartyShipParamsSetup" property="shipCarrier" type="java.lang.String"> </bean:define>
<bean:define id="strOpt" name="frmPartyShipParamsSetup" property="strOpt" type="java.lang.String"> </bean:define>
<%

String strCustServiceJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_CUSTOMERSERVICE");
String strCommonJsPath = GmFilePathConfigurationBean.getFilePathConfig("JS_COMMON");
	String strWikiTitle = GmCommonClass.getWikiTitle("ACCOUNT_PARAMETER_SETUP");
	String strAccID = GmCommonClass.parseNull((String)request.getAttribute("hAccountID"));
	String strPartyID =  GmCommonClass.parseNull((String)request.getAttribute("hPartyID"));
    ArrayList alShipModes = new ArrayList();
    alShipModes = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("alShipmode"));
	HashMap hcboVal = null;
	int intSize = 0;
	String strSelected = "";
	String strCodeID = "";
	String strCodeNm = "";
	String strSubmitAccess = GmCommonClass.parseNull((String)request.getAttribute("hSubmitAccess"));
	String strBtnDisable = "false";
	if(strSubmitAccess.equals("N")){
		strBtnDisable = "true";
	}
%> 
<HTML>
<HEAD>
<TITLE>Globus Medical: Party Ship Parameter Setup</TITLE>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/crosstab.css">
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tablehighlight.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.css" />
<script language="JavaScript" src="<%=strExtWebPath%>/ajaxtabs/ajaxtabs.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/ajaxcommon.js"></script>
<script language="JavaScript" src="<%=strCustServiceJsPath%>/GmAccountParmsSetup.js"></script>
<script language="JavaScript" src="<%=strCommonJsPath%>/GmPartyShipParamsSetup.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message<%=strJSLocale%>.js"></script>

<script>
var accid = '<%=strAccID%>';
var partyId = '<%=strPartyID%>';
var shipModeLength = <%=alShipModes.size()%>;
var shipmode = '<%=shipMode%>';
var shipCarrier = '<%=shipCarrier%>';
var option = '<%=strOpt%>';

<% hcboVal = new HashMap();
for (int i=0;i<alShipModes.size();i++){
	hcboVal = (HashMap)alShipModes.get(i); %>
var AllModeArr<%=i%> ="<%=hcboVal.get("CODEID")%>,<%=hcboVal.get("CODENM")%>";
<% } %>
</script>
<style type="text/css" media="all">
@import url("<%=strCssPath%>/screen.css");
</style>
</HEAD>
<BODY leftmargin="20" topmargin="10">
<FORM name="frmPartyShipParamsSetup" method="post" action="/gmPartyShipParamsSetup.do?method=setPartyShipParam" >
<html:hidden property="screenType" name ="frmPartyShipParamsSetup"/>
<html:hidden property="accountId" name ="frmPartyShipParamsSetup"/>
<html:hidden property="partyID" name ="frmPartyShipParamsSetup"/>
<input type="hidden" name="hcurrentTab">
<table border="0" class="DtTable700" cellspacing="0" cellpadding="0">
<tr>
	<td>
		<table cellspacing="0" cellpadding="0" width="100%" border="0">
			<tr>
	<td height="25" class="RightDashBoardHeader">&nbsp;<fmtPartyShipParamsContainer:message key="LBL_ACCOUNT_PARAMETER_SETUP"/></td>
	<td align="right" class=RightDashBoardHeader> 
			<fmtPartyShipParamsContainer:message key="LBL_HELP" var="varHelp"/>
			<img id='imgEdit' style='cursor:hand' src='<%=strImagePath%>/help.gif' title='${varHelp}' width='16' height='16' onClick="javascript:fnHelp('<%=strWikiPath%>','<%=strWikiTitle%>');" />&nbsp;
	</td>
	</tr>
	</table>
	</td>
</tr>
<tr class="Shade">
	<td colspan="4" class="LLine" height="1"></td>
</tr>
<%if(strPartyID.equals("")){ %>
<tr>
	<td>
	<table cellspacing="0" cellpadding="0" width="100%" border="0">
		<tr>
			<td class="RightTableCaption" HEIGHT="24" width="17%" align="Right"> &nbsp;<fmtPartyShipParamsContainer:message key="LBL_ACCOUNT_LIST"/>:</td>
			<td colspan="5">&nbsp;<select name="Cbo_AccId" id="Region" class="RightText" onFocus="changeBgColor(this,'#EEEEEE');" onChange="javascript:fnLoadAccount(this,this.form);" onBlur="changeBgColor(this,'#ffffff');" tabindex=1><option value="0" >[Choose One]
<%
					boolean bolAccess = true;			  				
			  		intSize = alAccountList.size();
					hcboVal = new HashMap();		
			  		for (int i=0;i<intSize;i++){			  			
			  			hcboVal = (HashMap)alAccountList.get(i);
			  			strCodeID = (String)hcboVal.get("ID");
						strSelected = strAccID.equals(strCodeID)?"selected":"";
%>
			   
				<option <%=strSelected%> value="<%=strCodeID%>"><%=hcboVal.get("NAME")%></option>				
<%
			  		}
%>
				</select>&nbsp;<input type="text" size="3" id="txtAccount" value="<%=strAccID%>" name="Txt_AccId" onFocus="changeBgColor(this,'#EEEEEE');" onBlur="changeBgColor(this,'#ffffff');" onChange="javascript:fnLoadAccount(this,this.form);" tabindex=2>
	
	
	</td>	
	</tr>	
	</table>
	</td>
	</tr>	
	<%} %>
</FORM>
<tr>
	<td colspan="6">
		<ul id="maintab" class="shadetabs">
			<li onclick = "fnUpdateTabIndex(this);"><a href="/gmPartyShipParamsSetup.do?method=setPartyShipParam&strOpt=fetchParams&screenType=tab&accountId=<%=strAccID%>&partyID=<%=strPartyID%>" rel="ajaxdivcontentarea" id="shipdetails" title="shipdetails" > <fmtPartyShipParamsContainer:message key="LBL_SHIP_PARAMETER"/></a></li>
			<li onclick = "fnUpdateTabIndex(this);"><a href="/gmRuleParamSetup.do?codeGrpId=SHPPM&ruleGrpId=<%=strAccID%>&header=Map Shipping Parameters"
									rel="#iframe" id="InvoiceParam" title="InvoiceParam"><fmtPartyShipParamsContainer:message key="LBL_INVOICE_PARAMETER"/></a>
							</li>														
		</ul>
	</td>
</tr>
<tr>
	<td> 
		<div id="ajaxdivcontentarea" class="contentstyle" style="display:visible;overflow:auto;height:100%" ></div> 
	</td>	
</tr> 

</table>
<script type="text/javascript">				    
	var maintab=new ddajaxtabs("maintab", "ajaxdivcontentarea");				
    maintab.setpersist(true);		
    maintab.init();		
    maintab.onajaxpageload=function(pageurl){    	
    	if(pageurl.indexOf("gmPartyShipParamsSetup.do?method=setPartyShipParam")!=-1){
        	fnPageLoad1();
    	}
    	if (pageurl.indexOf("gmRuleParamSetup.do")!=-1){
			if(document.getElementById("ajaxdivcontentarea") != undefined){
	  			document.getElementById("ajaxdivcontentarea").style.height = "auto";
			}	
		}
    }		
</script>
</FORM>
<%@ include file="/common/GmFooter.inc" %>
</BODY>
</HTML>