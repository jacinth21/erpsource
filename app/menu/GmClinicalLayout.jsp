 <%
/**********************************************************************************
 * File		 		: GmContainer.jsp
 * Desc		 		: This screen is used for the all Portals  - Frameset
 * Version	 		: 1.0
 * author			: Ritesh Shah	
************************************************************************************/
%>

<!--menu\GmClinicalLayout.jsp -->


<%@ page language="java" %>
<%@ page import="com.globus.common.servlets.GmServlet" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ include file="/common/GmHeader.inc" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<%
String strClinicalJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
	GmServlet gmServlet = new GmServlet();
	gmServlet.checkSession(response,session);
	String strStudyId = "";
	String strSiteId = "";
	String strPatientId = "";
    String strPgToLoad = "";
    String strReqContext = "";
    String strSessContext = "";
    String strDhtmlx = "";
    String strMethod = "";
    String strLocation = "";
    String strPeriodId = "";
    String strOpt = "";
    String strTxnId = "";
    String strStudySiteId = "";
    String strPatientPkey = "";
    String strLastClicked = "";
    String strNodeOpt = "";
    String strNodeId = "";
    String strAccrd = "";
    strReqContext = GmCommonClass.parseNull((String)request.getParameter("strContext"));
    strSessContext = GmCommonClass.parseNull((String)session.getAttribute("strSessContext"));
    strOpt = GmCommonClass.parseNull((String)request.getParameter("strOpt"));
    strPgToLoad = strPgToLoad.equals("")?GmCommonClass.parseNull((String)request.getParameter("strPgToLoad")):strPgToLoad;
    strPgToLoad = strPgToLoad.equals("")?(String)session.getAttribute("strSessPgToLoad"):strPgToLoad;
    strStudyId = GmCommonClass.parseNull((String)request.getParameter("strStudyId"));
    strSiteId = GmCommonClass.parseNull((String)request.getParameter("strSiteId"));
    strPatientId = GmCommonClass.parseNull((String)request.getParameter("strPatientId"));
    
    strDhtmlx = GmCommonClass.parseNull((String)request.getParameter("strDhtmlx"));
    strMethod = GmCommonClass.parseNull((String)request.getParameter("strMethod"));
    strLocation = GmCommonClass.parseNull((String)request.getParameter("strLocation"));
    strPeriodId = GmCommonClass.parseNull((String)request.getParameter("strPeriodId"));
    
    strTxnId = GmCommonClass.parseNull((String)request.getParameter("strTxnId"));
    strStudySiteId = GmCommonClass.parseNull((String)request.getParameter("strStudySiteId"));
    strPatientPkey = GmCommonClass.parseNull((String)request.getParameter("strPatientPkey"));
        
    session.setAttribute("strSessStudyId",strStudyId);
    session.setAttribute("strSessSiteId",strSiteId);
    session.setAttribute("strSessPatientId",strPatientId);
    session.setAttribute("strSessPgToLoad",strPgToLoad);
    session.setAttribute("strSessMethod",strMethod);
    session.setAttribute("strSessDhtmlx",strDhtmlx);
    session.setAttribute("strSessPeriodId",strPeriodId);
    session.setAttribute("strSessStrOpt",strOpt);
    session.setAttribute("strSessStrTxnId",strTxnId);
    session.setAttribute("strSessStudySiteId",strStudySiteId);
    session.setAttribute("strSessPatientPkey",strPatientPkey);
    session.setAttribute("strSessReqContext",strReqContext);
%>



<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmClinical.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/skins/dhtmlxlayout_dhx_skyblue.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxToolbar/skins/dhtmlxtoolbar_dhx_skyblue.css"></link>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmClinicalLayout.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxlayout.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/dhtmlxcontainer.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxLayout/patterns/dhtmlxlayout_pattern4j.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxToolbar/dhtmlxtoolbar.js"></script>



<html>
<head>
<title>Globus Medical: Enterprise Portal</title>
  <script language="javascript">
    var strLocation = '<%=strLocation%>';
    if (self != top && strLocation != "top")
    {
    	top.location.replace(top.location.href);    	    	
    }    
  
  var actionURL = '<%=strPgToLoad%>'; 
  var strStudy = '<%=strStudyId%>'; 
  var dhtmlx = '<%=strDhtmlx%>';
  var strMethod = '<%=strMethod%>'; 
  var periodId = '<%=strPeriodId%>';
  var strOpt = '<%=strOpt%>';
  var strTxnId = '<%=strTxnId%>';
  var dhxToolbar = '';
 
</script>


</head>
<BODY onload="doOnLoad();">
<div id="parentId" style="position: relative; height: 100%; width:100%; aborder: #B5CDE4 1px solid;"></div>

<script>
</script>
</BODY>
</HTML>
