



<!--menu\GmProdLeftMenu.jsp -->



<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");

	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strMenuPath = GmCommonClass.getString("GMMENU");
	String strOperPath = GmCommonClass.getString("GMOPERATIONS");
	
	
	String strPgToLoad = (String)session.getAttribute("strSessPgToLoad");
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");

%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="AUTHOR" content="Dhinakaran James">
<title>Globus Medical: LeftMenu</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javaScript" src="<%=strJsPath%>/Submenu.js"></script>
<script>
// To Hide the frame
function hidetoc()
{
	top.fstMain.cols = "0,*";
	top.TopFrame.document.all("showtoc").style.display = "block";
}

function mouseover(item)
{
  switch (item)
    {
    case "moveprevious" :
      window.status = "Click to Close the Tree node";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious2.gif";
      break;

    case "movenext" :
      window.status = "Click to Expand the Tree node";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext2.gif";
      break;

    case "hidetoc" :
      window.status = "Hide Menu";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc2.gif"
      //hidetoc();
      break;
    }
  }

function mouseout(item)
  {
  switch (item)
   {
    case "moveprevious" :
     window.status = "";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious1.gif";
      break;

    case "movenext" :
      window.status = "";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext1.gif";
      break;

    case "hidetoc" :
      window.status = "";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc1.gif"
      break;
    }
  }

function fnCallLink(page,obj)
{
	parent.RightFrame.location.href = "<%=strServletPath%>/GmPageControllerServlet?strPgToLoad="+page+"&strOpt="+obj;
}
</script>
</head>

<BODY bgcolor="#eeeeee">
<TABLE cellSpacing=0 cellPadding=0 border=0 width="100%"  height = "100%" valign="top">
  <TR height = "10">
	<TD  colspan = "2"  height = "10"   class="LeftTableHeader">
	<DIV align = "right">
 	 <img id="imgHideToc" style="cursor:hand" onclick="hidetoc();"
	 onmouseover="mouseover('hidetoc');" onmouseout="mouseout('hidetoc');"
	 src="<%=strImagePath%>/hidetoc1.gif" title="Hide Menu" />
	 </DIV>
	</TD>
  </TR>
<%	
	if ( strTopMenu.equals("Trans")){
%>
	<TD valign="top">

			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartSearchServlet','');">
			Part Number Search</A>
			</TD></TR></TABLE>
	
			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="
<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignItemServlet','InHouse');">
			Request Items for In-House Use</A>
			</TD></TR></TABLE>	

	</TD>
  <%
	 }else if ( strTopMenu.equals("Setup")){
  %>
  <TR height = "100%">
	<TD valign="top">

		<!--
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmIdeaServlet');">
			New Idea</A>
			</TD></TR></TABLE>
		-->

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmProjectServlet',this);">
			Project Setup</A>
			</TD></TR></TABLE>
			
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartNumServlet',this);">
			Part Number Setup</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartNumEditServlet',this);">
			Part Number Assembly Setup</A>
			</TD></TR></TABLE>

		<TABLE BORDER=0><TR><TD width="7"></TD>
		<TD valign="top">
		 <A Class=SubMenuMouseOver name = Set onClick="Toggle(this)" href=# >
		 <IMG border=0 src="<%=strImagePath%>/plus.gif"> Set Maintenance</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetMasterServlet','SET');">
			Setup</A>
			</TD></TR></TABLE>
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetMapServlet','SETMAP');">
			Mapping</A>
			</TD></TR></TABLE>
			</DIV></TD></TR></TABLE>
	<script>
		CollapseAll(Set);
	</script>
	<TABLE BORDER=0><TR><TD WIDTH=7></TD>
	<TD><IMG src="<%=strImagePath%>/leaf.gif">
	<A class =SubMenuMouseOver tabindex=-1 href=#
	onClick="fnCallLink('GmSetBaselineValueServlet','20175');">
	Baseline Case Setup</A>
	</TD></TR></TABLE>	
<TABLE BORDER=0><TR><TD width="7"></TD>
		<TD valign="top">
		 <A Class=SubMenuMouseOver name = "Bom" onClick="Toggle(this)" href=# >
		 <IMG border=0 src="<%=strImagePath%>/plus.gif"> BOM Maintenance</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetMasterServlet','BOMSETUP');">
			Setup</A>
			</TD></TR></TABLE>
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetMapServlet','BOMMAP');">
			Mapping</A>
			</TD></TR></TABLE>
			</DIV></TD></TR></TABLE>
			
	<script>
		CollapseAll(Bom);
	</script>
	</TD>
</TR>
<%	
	 }else if ( strTopMenu.equals("Report")){
  %>
  <TR height = "100%">
	<TD valign="top">
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmProdReportServlet',this);">
			Projects Report</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartNumServlet','SUBASMSCH');">
			Sub Assembly Part Search</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetReportServlet','SETRPT');">
			Set List Report</A>
			</TD></TR></TABLE>
						
	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = PO onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Vendor PO Reports</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmPOReportServlet','Vendor');">
			By Vendor</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmPOReportServlet','Part');">
			By Part Number</A>
			</TD></TR></TABLE>
		</DIV></TD></TR></TABLE>
		
		
		<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = CSG onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Consignment Details</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmSalesConsignDetailsServlet','LoadPart');">
			Sales By Part</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmSalesConsignDetailsServlet','LoadDistributor');">
			Sales By Field Sales</A>
			</TD></TR></TABLE>

			
		</DIV></TD></TR></TABLE>
		
		
		<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartSalesYTDReportServlet','LoadPart');">
			Part YTD Report</A>
		</TD></TR></TABLE>		
		<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartPriceReportServlet','FROMPD');">
			Part Number Pricing</A>
		</TD></TR></TABLE>
		<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesConsignSearchServlet','BYSET');">
			Transaction Search</A>
		</TD></TR></TABLE>

		<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesVirtualActualServlet','BYDIST');">
			Logical vs Actual (By Field Sales) </A>
		</TD></TR></TABLE>

		<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesVirtualActualServlet','BYSET');">
			Logical vs Actual (By Set) </A>
		</TD></TR></TABLE>
		
		<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesVirtualActualServlet','BYPART');">
			Logical vs Actual (By Part) </A>
		</TD></TR></TABLE>

		<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmTrackImplantSummaryServlet','');">
			Tracking Implants Summary</A>
			</TD></TR></TABLE>
			
		<!--<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesVirtualActualServlet','BYSET');">
			One Part Search (By Field Sales) </A>
		</TD></TR></TABLE> -->		
	<script>
		CollapseAll(PO);
		CollapseAll(CSG);
	</script>					
	</TD>
</TR>
<%	
	}
%>
</table>
</BODY>
</HTML>
