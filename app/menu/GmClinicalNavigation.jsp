
<!--menu\GmClinicalNavigation.jsp -->

<%@ include file="/common/GmHeader.inc" %>
<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.HashMap,com.globus.common.beans.GmCommonClass" %>
<%@ page import ="com.globus.common.beans.GmFilePathConfigurationBean"%>
<bean:define id="treeSnapshotData" name="frmStudyFilterForm" property="treeSnapshotXmlData" type="java.lang.String"> </bean:define>
<bean:define id="treeReportData" name="frmStudyFilterForm" property="treeReportXmlData" type="java.lang.String"> </bean:define>
<bean:define id="treeTransData" name="frmStudyFilterForm" property="treeTransXmlData" type="java.lang.String"> </bean:define>
<% 
String strClinicalJsPath=GmFilePathConfigurationBean.getFilePathConfig("JS_CLINICAL");
boolean showGrid=true;
String strSessAccLvl = (String)session.getAttribute("strSessAccLvl");
String strSessStudyId = (String)session.getAttribute("strSessStudyId");
String strSessPatientPkey = (String)session.getAttribute("strSessPatientPkey");
String strNodeId = GmCommonClass.parseNull((String)session.getAttribute("strSessNodeId"));
String strAccrd = GmCommonClass.parseNull((String)session.getAttribute("strSessAccrd"));

HashMap hmParam = new HashMap();
hmParam.put("strSessAccLvl",strSessAccLvl);
hmParam.put("strSessStudyId",strSessStudyId);
hmParam.put("strSessPatientPkey",strSessPatientPkey);
//String strXML = GmCommonClass.getClinicalXML(hmParam);


%>
<HTML>
<HEAD>
<TITLE> Globus Medical: Product Information </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<link rel="stylesheet" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxAccordion/skins/dhtmlxaccordion_dhx_skyblue.css">
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxtree.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxAccordion/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxAccordion/dhtmlxaccordion.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxAccordion/dhtmlxcontainer.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxcommon.js"></script>		 
<script  src="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxtree.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script language="JavaScript" src="<%=strClinicalJsPath%>/GmClinicalNavigation.js"></script>	


<script type="text/javascript">
var objSnapshotData = '<%=treeSnapshotData%>';

var objReportData = '<%=treeReportData%>';
var objTransData = '<%=treeTransData%>';
var studyId = '<%=strSessStudyId%>';
var accessLvl = '<%=strSessAccLvl%>';
var patientId = '<%=strSessPatientPkey%>';

//For last clicked event
var leftNodeId = '<%=strNodeId%>';
var accrd = '<%=strAccrd%>';
//alert(actionToLoad+'..........'+actionOpt);
</script>
</HEAD>
<BODY leftmargin="0" topmargin="0"  onload="init();">
	<div id="accordObj" style="position: relative; width: 228px; height: 100%;"></div>		
</BODY>

</HTML>




