 <%
/**********************************************************************************
 * File		 		: GmManfLeftMenu.jsp
 * Desc		 		: This screen is used for the Menu structure of Manufacturing Portal
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!--menu\GmManfLeftMenu.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>

<%
	
       String strCssPath = GmCommonClass.getString("GMSTYLES"); 
       String strImagePath = GmCommonClass.getString("GMIMAGES");
       String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
       String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
       String strMenuPath = GmCommonClass.getString("GMMENU");
	
	String strPgToLoad = (String)session.getAttribute("strSessPgToLoad");
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");

%>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="AUTHOR" content="Dhinakaran James">
<title>Globus Medical: LeftMenu</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javaScript" src="<%=strJsPath%>/Submenu.js"></script>
<script>
// To Hide the frame
function hidetoc()
{
	top.fstMain.cols = "0,*";
	top.TopFrame.document.all("showtoc").style.display = "block";
}

function mouseover(item)
{
  switch (item)
    {
    case "moveprevious" :
      window.status = "Click to Close the Tree node";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious2.gif";
      break;

    case "movenext" :
      window.status = "Click to Expand the Tree node";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext2.gif";
      break;

    case "hidetoc" :
      window.status = "Hide Menu";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc2.gif"
      //hidetoc();
      break;
    }
  }

function mouseout(item)
  {
  switch (item)
   {
    case "moveprevious" :
     window.status = "";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious1.gif";
      break;

    case "movenext" :
      window.status = "";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext1.gif";
      break;

    case "hidetoc" :
      window.status = "";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc1.gif"
      break;
    }
  }

function fnCallLink(page,obj)
{
	parent.RightFrame.location.href = "<%=strServletPath%>/GmPageControllerServlet?strPgToLoad="+page+"&strOpt="+obj;
}
</script>
</head>

<BODY bgcolor="#eeeeee">
<TABLE cellSpacing=0 cellPadding=0 border=0 width="100%"  height = "100%" valign="top">
  <TR height = "10">
	<TD  colspan = "2"  height = "10"   class="LeftTableHeader">
	<DIV align = "right">
 	 <img id="imgHideToc" style="cursor:hand" onclick="hidetoc();"
	 onmouseover="mouseover('hidetoc');" onmouseout="mouseout('hidetoc');"
	 src="<%=strImagePath%>/hidetoc1.gif" title="Hide Menu" />
	 </DIV>
	</TD>
  </TR>
<%	
	if ( strTopMenu.equals("Trans")){
%>
  <TR height = "100%">
	<TD valign="top">
	<!--
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 style="cursor:hand" 
			onClick="fnCallLink('GmFormDataEntryServlet','');">
			Link 1</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetMasterServlet','MANF');">
			New BOM Setup</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetMapServlet','MANF');">
			BOM Mapping</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmDocumetSetUpServlet','');">
			Document SetUp</A>
			</TD></TR></TABLE>
-->			
	<TABLE  cellspacing="0" BORDER=0><TR><TD width=2></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = WorkOrder onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Manufacturing Runs</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmMFGInitiateWOServlet','WOINI');">
			Initiate new Run</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmMFGInitiateWOServlet','WOQRY');">
			Query Existing Runs</A>
			</TD></TR></TABLE>
			
		</DIV></TD></TR></TABLE>

	<TABLE  cellspacing="0" BORDER=0><TR><TD width=2></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = MatReq onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Material Requests</A><DIV>
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmManfDashBoardServlet','MATREQ');">
			Pending</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmMFGInitiateWOServlet','REQNEW');">
			New Request</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmMFGInitiateWOServlet','REQREL');">
			Query/Release</A>
			</TD></TR></TABLE>
						
		</DIV></TD></TR></TABLE>
		
	<script>
		CollapseAll(WorkOrder);
		CollapseAll(MatReq);
	</script>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmMFGReconWOServlet','');">
			Mat. Reconciliation</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmMFGMatTransServlet','');">
			Globus Material Transfers</A>
			</TD></TR></TABLE>			

	</TD>
</TR>
  <%
	 }else if ( strTopMenu.equals("Setup")){
  %>
  <TR height = "100%">
	<TD valign="top">
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmDistributorServlet','');">
			New Supply Setup</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetMasterServlet','MANF');">
			New BOM Setup</A>
			</TD></TR></TABLE>
			
	</TD>
</TR>
<%	
	 }else if ( strTopMenu.equals("Report")){
  %>
  <TR height = "100%">
	<TD valign="top">

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartInvReportServlet','Main');">
			Main Inventory</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetReportServlet','BOMRPT');">
			BOM Report</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmPOReportServlet','Part');">
			PO Reports - By Part Number</A>
			</TD></TR></TABLE>
				
	</TD>
</TR>
<%	
	}
%>
</table>
</BODY>
</HTML>
