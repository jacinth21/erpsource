 <%
/**********************************************************************************
 * File		 		: GmUserNavigation.jsp
 * Desc		 		: This screen is used for the Menu structure of the common Left Menu
 * Version	 		: 1.0
 * author			: Vprasath
************************************************************************************/
%>
<%@ page language="java" %>
<%@ page import ="java.util.ArrayList"%>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import = "javax.servlet.http.*"%>
<%@ include file="/common/GmHeader.inc" %>

<bean:define id="strTreeXML" name="frmUserNavigationSetup" property="treeXmlData" type="java.lang.String"></bean:define>
<bean:define id="alUserGroup" name="frmUserNavigationSetup" property="alUserGroup" type="java.util.ArrayList"></bean:define>
<bean:define id="grpType" name="frmUserNavigationSetup" property="grpType" type="java.lang.String"></bean:define>
<bean:define id="myFavorites" name="frmUserNavigationSetup" property="myFavorites" type="java.lang.String"></bean:define>

<%		
	String strPgToLoad = GmCommonClass.parseNull(request.getParameter("hSessPg"));
	if (!strPgToLoad.equals(""))
	{
		session.setAttribute("strSessPgToLoad",strPgToLoad);
	}	
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
%>
<html>
<head>
	<title>Globus Medical: Portal Navigator</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style>
	a, A:link, a:visited, a:active, A:hover
		{color: #000000; text-decoration: none; font-family: Tahoma, Verdana; font-size: 11px}
</style>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxtree.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxAccordion/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxAccordion/dhtmlxaccordion.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxAccordion/dhtmlxcontainer.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxcommon.js"></script>		 
<script  src="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxtree.js"></script>
<script  src="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxtree_ed.js"></script>
<script  src="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/ext/dhtmlxtree_li.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script  src="<%=strJsPath%>/GmUserNav.js"></script>
<script>
	setLeftMenuTree('<%=strTreeXML%>');
	var strImagePath = '<%=strImagePath%>';
</script>

</head>
<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="white" onload="javascript:fnLoad();">

<html:form  action="/gmUserNavigation.do"  >
<input type="hidden" name="hSessPg" value="<%=strPgToLoad%>">

<TABLE cellSpacing=0 cellPadding=0 border=0 width="99%"  height = "100%" valign="top">
 <thead>
  <tr style="position:relative; top:expression(this.offsetParent.scrollTop);" height = "10">
	<TD class="LeftTableHeader">
 	 <img id="imgHideToc" style="cursor:hand" onclick="hidetoc();"
	 onmouseover="mouseover('hidetoc');" onmouseout="mouseout('hidetoc');"
	 src="<%=strImagePath%>/hidetoc1.gif" title="Hide Portal Navigator" align="absmiddle"/>
	</TD>
	<Td width="90%" height = "10" class="LeftTableHeader">Portal Navigator</td>
  </TR>
<% if (alUserGroup.size() > 1)  {  %>
  <tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td colspan = "2" class="Line" height="1"></td></tr>
	<tr style="position:relative; top:expression(this.offsetParent.scrollTop);">
  	<td class="LeftTableHeader">&nbsp;</td>  	
  	<!-- Custom tag lib code modified for JBOSS migration changes -->
  	<td class="LeftTableHeader" width="90%" height="10" class="RightText"><%=grpType%>
			<gmjsp:dropdown width="125" controlName="userGroup" SFFormName="frmUserNavigationSetup" SFSeletedValue="userGroup" onChange="fnReload(this)"
			 SFValue="alUserGroup" codeId = "GROUP_ID"  codeName = "GROUP_NM"  />  	
  	</td>
  </tr>  
  <% }  %>
  <tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td colspan = "2" class="Line" height="1"></td></tr>
</thead>
  
 <tr>
    <td colspan="4" valign="top">
    <!-- Height is added for JBOSS migration changes -->			            
        <div id="treeboxbox_tree" style="width:203px; height:100%; border :1px solid Silver;overflow:hidden" ></div>
       </td>
</tr>	
<% if(grpType.equals("Modules: ")) { %>


<tr height="25">
    <td colspan="4" valign="center">		
    <% if(myFavorites.equals("true")){ %>	                
        	<gmjsp:button value="Remove from Favorites" name="Btn_Go" gmClass="button" buttonType="Save" onClick="javascript:fnRemFav();" />
      <% } else {%>
      		&nbsp;<gmjsp:button value="Add to My Favorites" name="Btn_Go" gmClass="button" buttonType="Save" onClick="javascript:fnFavPop();" />
       <%} %>  	
       </td>
</tr>
<% }%>
</TABLE>

</html:form>
<%@ include file="/common/GmFooter.inc"%>
</body>
</html>