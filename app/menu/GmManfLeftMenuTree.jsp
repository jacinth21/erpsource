 <%
/**********************************************************************************
 * File		 		: GmManfLeftMenu.jsp
 * Desc		 		: This screen is used for the Menu structure of Manufacturing Portal
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!--menu\GmManfLeftMenuTree.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>

<%
      String strCssPath = GmCommonClass.getString("GMSTYLES");
      String strImagePath = GmCommonClass.getString("GMIMAGES");
      String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
      String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
      String strMenuPath = GmCommonClass.getString("GMMENU");
	
	String strPgToLoad = (String)session.getAttribute("strSessPgToLoad");
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");

%>

<html>
<head>
	<title>Globus Medical: LeftMenu</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style>
	a, A:link, a:visited, a:active, A:hover
		{color: #000000; text-decoration: none; font-family: Tahoma, Verdana; font-size: 11px}
</style>
<script>
function hidetoc()
{
	top.fstMain.cols = "0,*";
	top.TopFrame.document.all("showtoc").style.display = "block";
}

function mouseover(item)
{
  switch (item)
    {
    case "moveprevious" :
      window.status = "Click to Close the Tree node";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious2.gif";
      break;

    case "movenext" :
      window.status = "Click to Expand the Tree node";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext2.gif";
      break;

    case "hidetoc" :
      window.status = "Hide Portal Navigator";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc2.gif"
      //hidetoc();
      break;
    }
  }

function mouseout(item)
  {
  switch (item)
   {
    case "moveprevious" :
     window.status = "";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious1.gif";
      break;

    case "movenext" :
      window.status = "";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext1.gif";
      break;

    case "hidetoc" :
      window.status = "";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc1.gif"
      break;
    }
  }
  
function fnCallLink(page,obj)
{
	parent.RightFrame.location.href = "<%=strServletPath%>/GmPageControllerServlet?strPgToLoad="+page+"&strOpt="+obj;
}
</script>
</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="white">
<script language="JavaScript" src="<%=strJsPath%>/tree.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tree_items.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tree_tpl.js"></script>

<TABLE cellSpacing=0 cellPadding=0 border=0 width="100%"  height = "100%" valign="top">
  <TR height = "10">
	<TD  colspan = "2"  height = "10"   class="LeftTableHeader">
 	 <img id="imgHideToc" style="cursor:hand" onclick="hidetoc();"
	 onmouseover="mouseover('hidetoc');" onmouseout="mouseout('hidetoc');"
	 src="<%=strImagePath%>/hidetoc1.gif" title="Hide Menu" align="absmiddle"/>
Portal Navigator
	</TD>
  </TR>
  <tr><td class="Line" height="1"></td></tr>
  <TR height = "100%">
	<TD valign="top">
		<table cellpadding="5" cellspacing="0" cellpadding="10" border="0" width="100%">
		<tr>
			<td>
			<script language="JavaScript">
			<!--//
				new tree (TREE_ITEMS, TREE_TPL);
			//-->
			</script>
			</td>
			
		</tr>
		</table>
	</TD>
  </TR>
</TABLE>
</body>
</html>