 <%@page import="com.globus.common.beans.GmCommonBean"%>
<%
/**********************************************************************************
 * File		 		: GmPortalHeader.jsp
 * Desc		 		: This screen is used for the all Portal Header
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!--menu\GmPortalHeader.jsp -->



<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ include file="/common/GmHeader.inc" %>
<%

	String strShowAlert = GmCommonClass.parseNull((String)GmCommonClass.getString("SHOW_ALERT"));
			
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");
	String strUserShName = (String)session.getAttribute("strSessShName");
	String strLoginTS = (String)session.getAttribute("strSessLoginTS");
	String strSessAlertsCnt = GmCommonClass.parseNull((String)session.getAttribute("strSessAlertsCnt"));
	String strShowCompany = GmCommonClass.getString("SHOW_COMPANY_DRPDWN");
	String strShowPlant = GmCommonClass.getString("SHOW_PLANT_DRPDWN");
	String strParamCmp = GmCommonClass.parseNull((String)request.getParameter("company")); 
	strShowCompany = (strParamCmp.equals("hide")) ? "NO" : "YES";
	strShowPlant = (strParamCmp.equals("hide")) ? "NO" : "YES";
	String strCntFlg = (strParamCmp.equals("hide")) ? "NO" : "YES";
   //userAgent - PC-5231-add message in login to migrate message
	String userAgent = request.getHeader("User-Agent");
    String edgeBrowserMsg = GmCommonClass.parseNull((String)GmCommonClass.getString("EDGE_BROWSER_MIGRATE_MESSAGE"));
%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>GlobusOne Enterprise Portal: Manufacturing TopHeader</title>
<link rel="icon" type="image/png" href="<%=strImagePath%>/GlobusOneLogo.png"/>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">
<script language="JavaScript" src="<%=strJsPath%>/GmProgress.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/jquery.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/json2.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmPortalHeader.js"></script>
<script>

function showtoc() // Called to maximise the hidden Left Navigation Menu
{
 top.fstMain.cols = "200,*";
 top.TopFrame.document.all("showtoc").style.display = "None";
}//End of showtoc()

function callLogOff()
{
	<%
		String strKeyCloak = (String) session.getAttribute("strKeyCloak");
		String strOpt = (String) session.getAttribute("strOpt");
		//keycloak variables   
		String strurlindex = GmCommonClass.parseNull((String)GmCommonClass.getString("GMURLINDEX"));
		String strkeycloakurl = GmCommonClass.parseNull((String)GmCommonClass.getString("GMKEYCLOAKURL"));
		String strkeycloakrealm = GmCommonClass.parseNull((String)GmCommonClass.getString("GMKEYCLOAKREALM"));
		String strkeycloakclientid = GmCommonClass.parseNull((String)GmCommonClass.getString("GMKEYCLOAKCLIENTID"));
	%>
	var varKeycloak="<%=strKeyCloak%>"; 
	var varOpt="<%=strOpt%>"; 
	if(varKeycloak != 'N'){
		location.href = "<%=strServletPath%>/GmLogOffServlet?companyInfo="+encodeURIComponent(JSON.stringify(top.gCompanyInfo));
		// Keycloak Logout functionality (Remove keycloak sessions)
		var urlIndex = "<%=strurlindex%>";
	    var keycloakConfig = {url: "<%=strkeycloakurl%>", realm: "<%=strkeycloakrealm%>", clientId: "<%=strkeycloakclientid%>"};
	    var keycloakService = {};
	    var config = keycloakConfig;
	    window.parent.location.href = keycloakConfig.url+'/realms/'+keycloakConfig.realm+'/protocol/openid-connect/logout?redirect_uri='+urlIndex;  
    }else if(varOpt == 'SwitchUser'){ 
    	top.close();
    }else{
    	location.href = "<%=strServletPath%>/GmLogOffServlet?companyInfo="+encodeURIComponent(JSON.stringify(top.gCompanyInfo));
    }
}

function fnCallUnreadAlert(){
	parent.RightFrame.location.href = "/gmAlertsReportAction.do?strOpt=Reload&alersActionable=102544&haction=Header"; // 102544: YES [Actionable is YES]
}

function fnNewLocation(){
	
	window.location.href="/GmGlobusOnePortal.jsp";
}

</script>

<STYLE type="text/css">
.portalstyle
{position: absolute; visibility: hidden; left: 750; top: 18;}
.bgimage 
{ background-image: url(<%=strImagePath%>/SpineIT-header_greybar.png); background-repeat: repeat}
.bgimagemap 
{ background-image: url(<%=strImagePath%>/SpineIT-header_dotsandmap.png);}
</STYLE>
</head>

<BODY link="YELLOW" alink="YELLOW" vlink="YELLOW" onload="fnPageLoad();">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="bgimage" height="60" valign="top">
	   <img src="<%=strImagePath%>/SpineIT-header_logo.png" border="0">
    </td>
    <td class="bgimage" width="100%" align="center" valign="center" style="position:relative">
    	<p id="header_message" align="center" style="font-weight : bold;color:#C00000;font-size:300%;font-family:Verdana, Arial, Helvetica, sans-serif;margin: 0;" ></p>
    	
<%if(!userAgent.contains("Edg")!= false){%>
    	<marquee behavior="alternate" style=" margin: 0; position: absolute;bottom: 0;width: 100%;"><span style="color:blue;font-weight:bold;left:0;"><%=edgeBrowserMsg%></span> </marquee>
  <%} %>
    </td>
    <td class="bgimage" width="100%" align="center" valign="center">
         <label class="switch">
         <input type="checkbox" id="togBtn" onchange="fnNewLocation()" >
         <div><span class="toggleoff">New look off</span>
         <img src="/assets/images/off.png" data-size="small" width="40px" height="20px"/>
         </div>
         </label>
    </td>
    <td class="bgimage" width="100%" height="60" align="right" padding=5 valign="center" style="display:<%=strCntFlg.equals("YES")?"inline-block":"none"%>">
    	&nbsp;&nbsp;<img id="country_flag" align="center" width="50" height="30" src="" hspace="5" vspace="3"  border="0">
    </td>
    <td class="bgimagemap" valign="top" >
<%-- 	   <img src="<%=strImagePath%>/SpineIT-header_dotsandmap.png" border="0"> --%>
 <div style="width:300;">
 	   <table cellSpacing="0" cellPadding="0">
 	   			<tr>
 	   			<td colspan="4">&nbsp;</td>
 	   			</tr>
				<tr>
				  <td height="18" align="right" id="Company"  style="font-weight: bold;display:<%=strShowCompany.equals("YES")?"block":"none"%>"> Company :&nbsp;
				  <select id="Cbo_Company"  name="Cbo_Company" style="width:220px;" class="RightText" onchange="fnChangeCompany();"></select>
				 </td>
				</tr>
				<tr>
				  <td height="18" align="right" id="Plant" style="font-weight: bold;display:<%=strShowPlant.equals("YES")?"block":"none"%>"> Plant :&nbsp;
				  <select id="Cbo_Plant"  name="Cbo_Plant"  style="width:220px;" class="RightText" onchange="fnChangePlant();"></select>
				 </td>
				</tr>

	   </table>
 </div>
	</td>
  </tr>
  <tr>
	<td colspan="5">
	   <TABLE id="Table1" style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-LEFT: 1px; BORDER-BOTTOM: #000000 1px solid" height="20" cellSpacing="0" cellPadding="0" width="100%" bgColor="#2E73AC" border="0">
	   <tr>
		<td width="18" nowrap>
			  <div id="showtoc" name="showtoc" style="display:none;">
		   <table onMouseOver="showtoc();" height="20" cellSpacing="0" cellPadding="0" border="0" style="cursor:hand">
				<tr>
				  <td height="18"><img title="Show Portal Navigator" border="0" src="<%=strImagePath%>/showtoc.gif"></td>
				</tr>
			   </table>
			  </div>
		</td>
		<td width="10" height="18" id="logo-td" style="visibility:hidden"  nowrap><img height="15" id="logo-td" border="0" align="left" src="<%=strImagePath%>/tab_icon_plus.jpg">
	<%-- Welcome back changed when we login as Switch User--%>
		<%if(!strSwitchUserFl.equals("Y")){%> 
		<td  align="right" colspan="4" width="100%">&nbsp;<font color="#EEEEEE">Welcome back, <b><%=strUserShName%>!</b> You last logged in on <%=strLoginTS%></font>&nbsp;&nbsp;
		<%}else {%>
		<td  align="right" colspan="4" width="100%">&nbsp;<font color="#EEEEEE">Logged in as <b><%=strUserShName%></b> with Read Only Access! You last logged in on <%=strLoginTS%></font>&nbsp;&nbsp;
		<%} %>
		<%if(!strSessAlertsCnt.equals("") && !strSessAlertsCnt.equals("0") && strShowAlert.equals("YES")){ %>
			<img title="Unread Notification" border="0" height="15" align="middle" width="20" src="<%=strImagePath%>/UnRead.gif"><div style="vertical-align: middle;color:yellow;display:inline;font-weight:bold;">&nbsp; <%=strSessAlertsCnt%> <a href="#" onClick="fnCallUnreadAlert()" >Unread</a></div> &nbsp;&nbsp;
		<%} %>
		</td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=callLogOff(); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Logout</A></td>
	   </tr>
	 </table>
	</td>
  </tr>
</table>
</BODY>
</HTML>