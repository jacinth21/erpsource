 <%
/**********************************************************************************
 * File		 		: GmAcctLeftMenu.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>


<!--menu\GmAcctLeftMenu.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>

<%
	
       String strCssPath = GmCommonClass.getString("GMSTYLES");
       String strImagePath = GmCommonClass.getString("GMIMAGES");
       String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
       
	String strPgToLoad = (String)session.getAttribute("strSessPgToLoad");
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");

%>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="AUTHOR" content="Dhinakaran James">
<title>Globus Medical: LeftMenu</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javaScript" src="<%=strJsPath%>/Submenu.js"></script>
<script>
// To Hide the frame
function hidetoc()
{
	top.fstMain.cols = "0,*";
	top.TopFrame.document.all("showtoc").style.display = "block";
}

function mouseover(item)
{
  switch (item)
    {
    case "moveprevious" :
      window.status = "Click to Close the Tree node";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious2.gif";
      break;

    case "movenext" :
      window.status = "Click to Expand the Tree node";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext2.gif";
      break;

    case "hidetoc" :
      window.status = "Hide Menu";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc2.gif"
      //hidetoc();
      break;
    }
  }

function mouseout(item)
  {
  switch (item)
   {
    case "moveprevious" :
     window.status = "";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious1.gif";
      break;

    case "movenext" :
      window.status = "";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext1.gif";
      break;

    case "hidetoc" :
      window.status = "";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc1.gif"
      break;
    }
  }

function fnCallLink(page,obj)
{
	parent.RightFrame.location.href = "<%=strServletPath%>/GmPageControllerServlet?strPgToLoad="+page+"&strOpt="+obj;
}
</script>
</head>

<BODY bgcolor="#eeeeee">
<TABLE cellSpacing=0 cellPadding=0 border=0 width="100%"  height = "100%" valign="top">
  <TR height = "10">
	<TD  colspan = "2"  height = "10"   class="LeftTableHeader">
	<DIV align = "right">
 	 <img id="imgHideToc" style="cursor:hand" onclick="hidetoc();"
	 onmouseover="mouseover('hidetoc');" onmouseout="mouseout('hidetoc');"
	 src="<%=strImagePath%>/hidetoc1.gif" title="Hide Menu" />
	 </DIV>
	</TD>
  </TR>
<%	
	if ( strTopMenu.equals("Trans")){
%>
  <TR height = "100%">
	<TD valign="top">

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmOrderVoidServlet','');">
			Modify Order</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmProcessCreditsServlet','');">
			Modify Invoice</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmCreditMemoServlet','IssueCredit');">
			Issue Credit</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvoiceServlet','POSTPAY');">
			Post Payments</A>
			</TD></TR></TABLE>			

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GMDHRFlagInvoiceServlet','POSTDHRTXN');">
			Post DHR Payment</A>
			</TD></TR></TABLE>	

			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPendingOrdersPOServlet','EMAIL');">
			Pending PO Email Reminders</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPOReceiveServlet','INFORMALRECV');">
			Receive Shipment</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetBaselineValueServlet','20178');">
			COGS Baseline Setup</A>
			</TD></TR></TABLE>
			
	</TD>
</TR>
  <%
	 }else if ( strTopMenu.equals("Setup")){
  %>
  <TR height = "100%">
	<TD valign="top">
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmDistributorServlet','');">
			Distributorship Setup</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmTerritoryServlet','');">
			Territory Setup</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesRepServlet','');">
			Sales Rep Setup</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmAccountServlet','');">
			Account Setup</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetBaselineValueServlet','20178');">
			COGS Baseline Setup</A>
			</TD></TR></TABLE>

	</TD>
</TR>
<%	
	 }else if ( strTopMenu.equals("Report")){
  %>
  <TR height = "100%">
	<TD valign="top">


			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmDailySalesServlet','');">
			Daily Sales Report</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPendingOrdersPOServlet','RPT');">
			Pending PO Report</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPendingInvoiceServlet','');">
			Invoices Report</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmARSummaryServlet','');">
			A/R Summary Report</A>
			</TD></TR></TABLE>

	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = PO onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Vendor PO Reports</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmPOReportServlet','Vendor');">
			By Vendor</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmPOReportServlet','Part');">
			By Part Number</A>
			</TD></TR></TABLE>
		</DIV></TD></TR></TABLE>


	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = DHR onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;DHR Reports</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmDHRReportServlet','Vendor');">
			By Vendor</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmDHRReportServlet','Part');">
			By Part Number</A>
			</TD></TR></TABLE>
			
	<script>
		CollapseAll(DHR);
		CollapseAll(PO);
	</script>
				
	</TD>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvPostServlet','COST');">
			Costing Report</A>
			</TD></TR></TABLE>
				
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvPostServlet','INV');">
			Inventory Posting</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="/images/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmAccountRollforwardServlet','');">
			Account Rollforward</A>
			</TD></TR></TABLE>

            <TABLE BORDER=0><TR><TD WIDTH=7></TD>
            <TD><IMG src="/images/leaf.gif">
            <A class =SubMenuMouseOver tabindex=-1 href=#
            onClick="fnCallLink('GmInvoiceOrderListServlet','LoadInvoiceList');">
            Invoices List</A>
            </TD></TR></TABLE>

            <TABLE BORDER=0><TR><TD WIDTH=7></TD>
            <TD><IMG src="/images/leaf.gif">
            <A class =SubMenuMouseOver tabindex=-1 href=#
            onClick="fnCallLink('GmInvoiceOrderListServlet','LoadOrderList');">
            Orders List</A>
            </TD></TR></TABLE>

            <TABLE BORDER=0><TR><TD WIDTH=7></TD>
            <TD><IMG src="/images/leaf.gif">
            <A class =SubMenuMouseOver tabindex=-1 href=#
            onClick="fnCallLink('GmPaymentListServlet','Load');">
            Payments List</A>
            </TD></TR></TABLE>
            
            <TABLE BORDER=0><TR><TD WIDTH=7></TD>
            <TD><IMG src="/images/leaf.gif">
            <A class =SubMenuMouseOver tabindex=-1 href=#
            onClick="fnCallLink('GmPaymentListServlet','PendingPO');">
            Pending Order Summary</A>
            </TD></TR></TABLE>

            <TABLE BORDER=0><TR><TD WIDTH=7></TD>
            <TD><IMG src="/images/leaf.gif">
            <A class =SubMenuMouseOver tabindex=-1 href=#
            onClick="fnCallLink('GmCommonLogRetrieveServlet','INVOICE');">
            Invoice Call Log</A>
            </TD></TR></TABLE>
            
         	<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmDHRFlagPaymentServlet','DHRREPORT');">
			DHR Flag Payment</A>
			</TD></TR></TABLE>
			
         	<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmDHRFlagPaymentServlet','PAYREPORT');">
			DHR Payments List</A>
			</TD></TR></TABLE>			


</TR>
<%	
	}
%>
</table>
</BODY>
</HTML>
