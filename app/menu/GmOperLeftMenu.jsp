 <%
/**********************************************************************************
 * File		 		: GmOperLeftMenu.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>


<!--menu\GmOperLeftMenu.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>

<%
	
      String strCssPath = GmCommonClass.getString("GMSTYLES");
      String strImagePath = GmCommonClass.getString("GMIMAGES");
      String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
      String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
      String strMenuPath = GmCommonClass.getString("GMMENU");
      String strOperPath = GmCommonClass.getString("GMOPERATIONS");
	
	String strPgToLoad = (String)session.getAttribute("strSessPgToLoad");
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");

	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");
	int intAccessLvl = Integer.parseInt(strAccessLvl);
	boolean bolAccess = false;
	if (strDeptId.equals("Z") || (strDeptId.equals("O") && intAccessLvl > 3) || intAccessLvl > 3 || (strDeptId.equals("A") && intAccessLvl > 1) || strDeptId.equals("C"))
	{
		bolAccess = true;
	}

%>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="AUTHOR" content="Dhinakaran James">
<title>Globus Medical: LeftMenu</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javaScript" src="<%=strJsPath%>/Submenu.js"></script>
<script>
// To Hide the frame
function hidetoc()
{
	top.fstMain.cols = "0,*";
	top.TopFrame.document.all("showtoc").style.display = "block";
}

function mouseover(item)
{
  switch (item)
    {
    case "moveprevious" :
      window.status = "Click to Close the Tree node";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious2.gif";
      break;

    case "movenext" :
      window.status = "Click to Expand the Tree node";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext2.gif";
      break;

    case "hidetoc" :
      window.status = "Hide Menu";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc2.gif"
      //hidetoc();
      break;
    }
  }

function mouseout(item)
  {
  switch (item)
   {
    case "moveprevious" :
     window.status = "";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious1.gif";
      break;

    case "movenext" :
      window.status = "";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext1.gif";
      break;

    case "hidetoc" :
      window.status = "";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc1.gif"
      break;
    }
  }

function fnCallLink(page,obj)
{
	parent.RightFrame.location.href = "<%=strServletPath%>/GmPageControllerServlet?strPgToLoad="+page+"&strOpt="+obj;
	//parent.RightFrame.location.href = "<%=strServletPath%>/GmPageControllerServlet?strPgToLoad="+page;
}
</script>
</head>

<BODY bgcolor="#eeeeee">
<TABLE cellSpacing=0 cellPadding=0 border=0 width="100%"  height = "100%" valign="top">
  <TR height = "10">
	<TD  colspan = "2"  height = "10"   class="LeftTableHeader">
	<DIV align = "right">
 	 <img id="imgHideToc" style="cursor:hand" onclick="hidetoc();"
	 onmouseover="mouseover('hidetoc');" onmouseout="mouseout('hidetoc');"
	 src="<%=strImagePath%>/hidetoc1.gif" title="Hide Menu" />
	 </DIV>
	</TD>
  </TR>
<%	
	if ( strTopMenu.equals("Trans")){
%>
  <TR height = "100%">	
	<TD valign="top">
	
	<TABLE BORDER=0 cellspacing="0"><TR><TD width=1></TD>
	<TD valign="top">
	
			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartSearchServlet','');">
			Part Number Search</A>
			</TD></TR></TABLE>
			
			
			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPOReceiveServlet');">
			Receive Shipment</A>
			</TD></TR></TABLE>

			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmDHRModifyServlet');">
			Modify DHR</A>
			</TD></TR></TABLE>

			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmNCMRUpdateServlet');">
			Modify NCMR</A>
			</TD></TR></TABLE>

			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmReturnSetBuildServlet','');">
			Receive Returns</A>
			</TD></TR></TABLE>
			

			<TABLE cellspacing="0" BORDER=0><TR><TD height=25 width=2></TD>
			<TD valign="top">
				 <A Class=SubMenuMouseOver name = PO onClick="Toggle(this)" href=#>
				 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Reprocess Material</A><DIV>
		
					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=# 
					onClick="fnCallLink('GmConsignItemServlet','Repack');">
					Repackaging</A>
					</TD></TR></TABLE>

			<TABLE cellspacing="0" BORDER=0><TR><TD height=25 width=12></TD>
			<TD valign="top">
				 <A Class=SubMenuMouseOver name = Quar onClick="Toggle(this)" href=#>
				 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Quarantine Transactions</A><DIV>
		
					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=# 
					onClick="fnCallLink('GmConsignItemServlet','QuaranIn');">
					Shelf --> Quarantine</A>
					</TD></TR></TABLE>
					
					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=# 
					onClick="fnCallLink('GmConsignItemServlet','QuaranOut');">
					Quarantine --> Scrap</A>
					</TD></TR></TABLE>

					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=# 
					onClick="fnCallLink('GmConsignItemServlet','InvenIn');">
					Quarantine --> Shelf</A>
					</TD></TR></TABLE>
													
				</DIV></TD></TR></TABLE>

			<TABLE cellspacing="0" BORDER=0><TR><TD height=25 width=12></TD>
			<TD valign="top">
				 <A Class=SubMenuMouseOver name = Bulk onClick="Toggle(this)" href=#>
				 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Bulk Transfers</A><DIV>
		
					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=# 
					onClick="fnCallLink('GmConsignInHouseServlet','BulkOut');">
					Shelf --> Bulk</A>
					</TD></TR></TABLE>
					
					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=# 
					onClick="fnCallLink('GmConsignInHouseServlet','BulkIn');">
					Bulk --> Shelf</A>
					</TD></TR></TABLE>					
					
				</DIV></TD></TR></TABLE>
				
			<script>
				CollapseAll(Quar);
				CollapseAll(Bulk);
			</script>
				</DIV></TD></TR></TABLE>
			
			<script>
				CollapseAll(PO);
			</script>


			<TABLE cellspacing="0" BORDER=0><TR><TD height=25 width=2></TD>
			<TD valign="top">
				 <A Class=SubMenuMouseOver name = InHouse onClick="Toggle(this)" href=#>
				 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;In-House Consignments</A><DIV>
		
					<TABLE cellspacing="0" BORDER=0><TR><TD height=25 WIDTH=2></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=#
					onClick="fnCallLink('GmConsignItemServlet','InHouse');">
					Consign Items</A>
					</TD></TR></TABLE>	

					<TABLE cellspacing="0" BORDER=0><TR><TD height=10 WIDTH=2></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=#
					onClick="fnCallLink('GmInHouseSetServlet','4119');">
					Manage Sets</A>
					</TD></TR></TABLE>	

				
			<script>
				CollapseAll(InHouse);
			</script>
				</DIV></TD></TR></TABLE>

			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInHouseSetServlet','4127');">
			Manage Loaners</A>
			</TD></TR></TABLE>				
												
	</TD>
  <%
	 }else if ( strTopMenu.equals("Setup")){
  %>
  <TR height = "100%">
	<TD valign="top">
<%
	if (bolAccess)
	{
%>
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
				onClick="fnCallLink('GmSetBuildServlet','');">
			Set Building</A>
			</TD></TR></TABLE>
<%
		if (!strDeptId.equals("C"))
		{
%>
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmVendorServlet','');">
			Vendor Setup</A>
			</TD></TR></TABLE>
	
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmVendorPricingServlet','');">
			Vendor Pricing Setup</A>
			</TD></TR></TABLE>
<%
			if (intAccessLvl > 3)
			{
%>

			<TABLE BORDER=0><TR><TD width=7></TD>
			<TD valign="top">
				 <A Class=SubMenuMouseOver name = PO onClick="Toggle(this)" href=#>
				 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Raise PO</A><DIV>
		
					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=# 
					onClick="fnCallLink('GmOrderPlanServlet','');">
					Regular PO</A>
					</TD></TR></TABLE>
		
					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=# 
					onClick="fnCallLink('GmReworkServlet','Rework');">
					Rework PO</A>
					</TD></TR></TABLE>
					
					<!--<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=# 
					onClick="fnCallLink('GmReworkServlet','Sterlize');">
					Sterilization PO</A>
					</TD></TR></TABLE>
					-->
								
				</DIV></TD></TR></TABLE>
			<script>
				CollapseAll(PO);
			</script>		
<%
			}
%>
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartInvReportServlet','Manual');">
			Manual Adjustment</A>
			</TD></TR></TABLE>
<%			
		}	
	}
%>
	</TD>
</TR>
<%	
	 }else if ( strTopMenu.equals("Report")){
  %>
  <TR height = "100%">
	<TD valign="top">

<%
	if (bolAccess)
	{
%>
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmOperReportServlet','');">
			Vendor Report</A>
			</TD></TR></TABLE>

	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = PO onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;PO Reports</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmPOReportServlet','Vendor');">
			By Vendor</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmPOReportServlet','Part');">
			By Part Number</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmPOReportServlet','PendVend');">
			Pending - By Vendor</A>
			</TD></TR></TABLE>
			
		</DIV></TD></TR></TABLE>
	<script>
		CollapseAll(PO);
	</script>
	
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartSalesReportServlet','');">
			Part Number Sales Report</A>
			</TD></TR></TABLE>


			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmVendorYTDReportServlet','LoadVendor');">
			Vendor YTD Report</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartYTDReportServlet','LoadPart');">
			Part YTD Report</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmNCMRReportServlet','LoadNCMR');">
			NCMR Report</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmTranLogReportServlet','LoadNCMR');">
			Transactions Log</A>
			</TD></TR></TABLE>

            <TABLE BORDER=0><TR><TD WIDTH=7></TD>
            <TD><IMG src="<%=strImagePath%>/leaf.gif">
            <A class =SubMenuMouseOver tabindex=-1 href=#
            onClick="fnCallLink('GmCommonLogRetrieveServlet','PURCHASEPO');">
            PO Call Log</A>
            </TD></TR></TABLE>
			
	
<%
	}
%>
	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = DHR onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;DHR Reports</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmDHRReportServlet','Vendor');">
			By Vendor</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmDHRReportServlet','Part');">
			By Part Number</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmDHRReportServlet','Track');">
			By Date Range/Tracking</A>
			</TD></TR></TABLE>			

		</DIV></TD></TR></TABLE>
		
	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = INVEN onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Inventory Reports</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartInvReportServlet','Main');">
			Main Inventory</A>
			</TD></TR></TABLE>
			
			<!--<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartInvReportServlet','Bulk');">
			Bulk Inventory</A>
			</TD></TR></TABLE>-->
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','BuiltSet');">
			Built Set - By Sets</A>
			</TD></TR></TABLE>			

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','BuiltSetParts');">
			Built Set - By Parts</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','Quarantine');">
			Quarantine Inventory</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','ReturnsHold');">
			Returns Hold Inventory</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','Returns');">
			Returns Hold - By Type</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','ReturnsPart');">
			Returns Hold -By Part</A>
			</TD></TR></TABLE>
						
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','Rework');">
			Rework Inventory</A>
			</TD></TR></TABLE>
								
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','Loaner');">
			Loaners - By Sets</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','LoanerPart');">
			Loaners - By Parts</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','InHouse');">
			InHouse - By Sets</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInvListReportServlet','InHousePart');">
			InHouse - By Parts</A>
			</TD></TR></TABLE>
						
		</DIV></TD></TR></TABLE>
				
		
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetTransReportServlet','');">
			Sets Transactions Report</A>
			</TD></TR></TABLE>			

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmReprocessMaterialServlet','Report');">
			Reprocess Material Report</A>
			</TD></TR></TABLE>	
						
	<script>
		CollapseAll(DHR);
		CollapseAll(INVEN);
	</script>
	
         	<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetSummaryReportServlet','');">
			Inhouse Summary List</A>
			</TD></TR></TABLE>		

			
			<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = CAN onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Cancel Report</A><DIV>
			<% /*Set the hOpt value as the action which needs to happen ie cancelreport + the cancel type.
				For eg  if the Rollback Set Consignment report has to be opened, then hopt value will be as cancelreport@VDSCN */  %>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmCommonCancelServlet','cancelreport@VDSCN');">Void Set Consignment Report
			</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmCommonCancelServlet','cancelreport@VDRTN');">Void Return Report
			</A>
			</TD></TR></TABLE>

	</TD>
</TR>
	<script>
		CollapseAll(CAN);
	</script>
<%	
	}
%>
</table>
</BODY>
</HTML>
