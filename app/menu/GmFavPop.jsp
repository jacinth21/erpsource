   
   <!--menu\GmFavPop.jsp -->
   
    <%@ include file="/common/GmHeader.inc" %> 
    <bean:define id="grpId" name="frmUserNavigationSetup" property="grpId" type="java.lang.String"></bean:define>
    <bean:define id="funId" name="frmUserNavigationSetup" property="funId" type="java.lang.String"></bean:define>
    <bean:define id="funName" name="frmUserNavigationSetup" property="funName" type="java.lang.String"></bean:define>
    <bean:define id="strOpt" name="frmUserNavigationSetup" property="strOpt" type="java.lang.String"></bean:define>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<title>Rename Your Favorite Link</title>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script  src="<%=strJsPath%>/GmUserNav.js"></script>
<script type="text/javascript">
	var grpid = '<%=grpId%>';
	var funid = '<%=funId%>';
	var strOpt = '<%=strOpt%>';
	function fnClose(){
		if(strOpt=='Saved'){
			window.close();
		}
	}
</script>
<style type="text/css" media="all">
@import url("styles/screen.css");
</style>
</head>
<body onload="fnClose();">
<html:form action ="/gmUserNavigation.do">
<table border="0" class="DtTable350"  cellspacing="0" cellpadding="0"  >	
		<tr>
			<td height="25" colspan="2" class="RightDashBoardHeader" >Rename Your Favorite Link</td>
		</tr>	
		<tr><td colspan="2" height="1" bgcolor="#cccccc"></td></tr>
		<tr class="Shade">
			<td class="RightTableCaption" HEIGHT="23" width="400" align="right"><font style="color: red;font-weight: bolder;">* </font>Link Name:</td>
			<td>&nbsp;
		        <html:text property="funName" name="frmUserNavigationSetup" maxlength="99" size="30" onfocus="changeBgColor(this,'#AACCE8');" 
		        styleClass="InputArea" onblur="changeBgColor(this,'#ffffff'); fnSkipSpec(this);"/>&nbsp;
		    </td> 
		</tr>		
		<tr>
		<td colspan="2" align="center">
		        <gmjsp:button value="Add" gmClass="button" buttonType="Save" onClick="fnAddFav();"/>
		        <br>
		        <div id="msg" style="color: red;font-weight: bold;"></div>
		</tr>
		<tr><td class="line" colspan="2"></td></tr>
	</table>
	</html:form>
	<%@ include file="/common/GmFooter.inc"%>
</body>
</html>