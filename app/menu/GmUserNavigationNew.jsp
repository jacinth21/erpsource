 <%
/**********************************************************************************
 * File		 		: GmUserNavigationNew.jsp
 * Desc		 		: This screen is used for the Menu structure of the common Left Menu
 * Version	 		: 1.0
 * author			: Vprasath
************************************************************************************/
%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ page language="java" %>
<%@ page import ="java.util.ArrayList,java.util.HashMap, java.net.URLEncoder"%>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ page import = "javax.servlet.http.*"%>


<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>


<%-- <bean:define id="strTreeXML" name="frmUserNavigationSetup" property="treeXmlData" type="java.lang.String"></bean:define> --%>
<bean:define id="alMenuList" name="frmUserNavigationSetup" property="alMenuList" type="java.util.ArrayList"></bean:define>
<bean:define id="alUserGroup" name="frmUserNavigationSetup" property="alUserGroup" type="java.util.ArrayList"></bean:define>
<bean:define id="grpType" name="frmUserNavigationSetup" property="grpType" type="java.lang.String"></bean:define>
<bean:define id="myFavorites" name="frmUserNavigationSetup" property="myFavorites" type="java.lang.String"></bean:define>
<bean:define id="hmAccessMap" name="frmUserNavigationSetup" property="hmAccessMap" type="java.util.HashMap"></bean:define>
<bean:define id="strSelectedGroupId" name="frmUserNavigationSetup" property="strSelectedGroupId" type="java.lang.String"></bean:define>
<bean:define id="strSelectedGroupName" name="frmUserNavigationSetup" property="strSelectedGroupName" type="java.lang.String"></bean:define>
<bean:define id="strReadOnlyAccess"  name="frmUserNavigationSetup" property="strReadOnlyAccess" type="java.lang.String"></bean:define>
<% String strJsPath = GmCommonClass.getString("GMJAVASCRIPT"); %>
<script language="JavaScript" src="<%=strJsPath%>/GmUserNavNew.js"></script>
<script src="/dist/js/sidebarmenu.js"></script>


<%		
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH"); 
	String strPgToLoad = GmCommonClass.parseNull(request.getParameter("hSessPg"));
	if (!strPgToLoad.equals(""))
	{
		session.setAttribute("strSessPgToLoad",strPgToLoad);
	}	
	String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
	String strShUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessShName"));
	String strUserName = strUserId + " - " + strShUserName;
%>
<style>
::-webkit-scrollbar {
    width: 0px;
    background: transparent; /* make scrollbar transparent */
}
@media (min-width: 768px) {
.mini-sidebar .scroll-sidebar {
   
    height: calc(100vh - 110px) !important;
}
}
</style>

 <html:form  action="/gmUserNavigationNew.do"  >
<input type="hidden" name="hSessPg" value="<%=strPgToLoad%>">
<input type="hidden" id="userGroupName" name="userGroupName" value="<%=strSelectedGroupName%>">
<input type="hidden" id="userGroupId" name="userGroupId" value="<%=strSelectedGroupId%>">
<input type="hidden" id="selectedFunctionId" name="selectedFunctionId" />
<input type="hidden" id="selectedFunctionName" name="selectedFunctionName" />
<input type="hidden" id="isFolder" name="isFolder" />
<input type="hidden" id="access" name="access" />
<input type="hidden" id="strOpt" name="strOpt" />
        <aside class="left-sidebar">
             
            <div class="scroll-sidebar" style="position: relative;overflow-y:scroll;" >
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li > <a  class="has-arrow waves-effect waves-dark "   aria-expanded="false" ><i class="ti-gallery"></i><span class="hide-menu" style="font-size:14px;font-weight: 600;"><%= strSelectedGroupName.replaceAll("'","") %></span></a>
                            <ul aria-expanded="false" class="collapse" style="cursor: pointer;" >
                            	<%
								for (int j = 0; j < alUserGroup.size(); j++) {
										java.util.LinkedHashMap groupObj = (java.util.LinkedHashMap)alUserGroup.get(j);
										String strGroupNm = (String) groupObj.get("GROUP_NM");
								%>
                                <li><a   onclick="fnReloadNew('<%=strGroupNm %>','<%=groupObj.get("GROUP_ID") %>')"><%=groupObj.get("GROUP_NM") %></a></li>
								<%	
								}
								%>
                            </ul>
                        </li>
 						 <%
							String strPrevGroupId = "";
 							String strPrevLevel = "";
 						 	String strCompanyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
 							String strJsDateFmt = GmCommonClass.parseNull((String) session.getAttribute("strSessJSDateFmt"));
 							String strPartyId =  GmCommonClass.parseNull((String) session.getAttribute("strSessPartyId"));
 							String strPlantId = GmCommonClass.parseNull((String) session.getAttribute("strSessPlantId"));
 							//The below Language id is used to show the Japan Master data in english on sales sales reports and in dropdowns for Non Globus Medical Japan as primary company 
 							String strLanguageId = "103097";
 							strLanguageId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLangId"));
 							
 							String strCompanyInfo ="{\"cmpid\":\""+strCompanyId+"\",\"partyid\":\""+strPartyId+"\",\"plantid\":\""+strPlantId+"\",\"cmplangid\":\""+strLanguageId+"\"}";
 							strCompanyInfo = URLEncoder.encode(strCompanyInfo);
 							
 							int intPrevLevel = 0;
 							int intPrevFolderLevelCount = 0;
 							int intPrevFolderLevel = 0;
 							boolean blIsSubFolder = false;
							for (int i = 0; i < alMenuList.size(); i++)
							{
								String strActive = ( i == 0) ? "active" : "";
								String strIn  = ( i == 0) ? "in" : "";
								java.util.LinkedHashMap jsonobj = (java.util.LinkedHashMap)alMenuList.get(i);
								String strGroupId = (String) jsonobj.get("GROUPID");
								String strLevel = (String) jsonobj.get("LEVEL");
								int intLevel =  Integer.parseInt(strLevel);
								String strType = (String) jsonobj.get("TYPE");
								String strFunctionID = (String) jsonobj.get("FUNCTION_ID");
								String strOPT = (String) jsonobj.get("STROPT");
								String strSqid = (String) jsonobj.get("SQID");
								boolean  blHasAccess= String.valueOf(hmAccessMap.get(strFunctionID+"_RD_ACC")).equalsIgnoreCase("Y") ? true : false;
								boolean blIsFolder = strType.equals("Folder") ? true : false;
								
								 if (hmAccessMap == null || ( hmAccessMap != null && hmAccessMap.size() == 0 ) || strReadOnlyAccess.equalsIgnoreCase("Y")){
									 blHasAccess = true;  
					        	  }
								 
								if(i >0 && intPrevLevel > intLevel)
								{
									if(blIsFolder && intLevel ==1)
									{
										for(int j=0; j<intPrevFolderLevelCount; j++)
										{
								%>
									
										</ul></li>
										
								<%
										}
										intPrevFolderLevelCount = 0;
									}
									else if(blIsFolder && intLevel > 1)
									{
										if(intPrevFolderLevel == intLevel) 
										{
										%>
											</ul></li> 
										<%
										intPrevFolderLevelCount = intPrevFolderLevelCount - 1;
										}
										else
										{
											for(int j=0; j<intPrevFolderLevelCount; j++)
											{
									%>		
											 </ul></li> 
									<% 
												intPrevFolderLevelCount = intPrevFolderLevelCount - 1;
											}
										}
										
										
									}
									else
									{
										%>		
										 </ul></li> 
									<% 
										intPrevFolderLevelCount = intPrevFolderLevelCount - 1;
									}
								}
								%>
								
								<%		
								if(intLevel ==1 && strType.equals("File"))
								{
			
									if(blHasAccess)
									{	
									    if(strGroupId.contains("FAV")){ %>
										<li> <a class="waves-effect waves-dark"  onclick="setSelectedId('<%=jsonobj.get("FUNCTION_ID") %>', '<%=jsonobj.get("FUNCTION_NM") %>','<%=blHasAccess %>','<%=blIsFolder %>','<%=strOPT %>','<%=jsonobj.get("URL") %>','<%=strSqid %>')"  aria-expanded="false" title="<%=jsonobj.get("PATH") %>"><i class="ti-file" style="width: 18px;font-size:13px !important"></i><span class="hide-menu"><%=jsonobj.get("FUNCTION_NM") %></span></a></li>
									  <%}else{  %>
								       	<li> <a class="waves-effect waves-dark"  onclick="setSelectedId('<%=jsonobj.get("FUNCTION_ID") %>', '<%=jsonobj.get("FUNCTION_NM") %>','<%=blHasAccess %>','<%=blIsFolder %>','<%=strOPT %>','<%=jsonobj.get("URL") %>','<%=strSqid %>')"  aria-expanded="false" ><i class="ti-file" style="width: 18px;font-size:13px !important"></i><span class="hide-menu"><%=jsonobj.get("FUNCTION_NM") %></span></a></li>
	                             	  <% } %>

							<%	
									}
									else
									{
							%>
										<li><a  onclick="setSelectedId('<%=jsonobj.get("FUNCTION_ID") %>', '<%=jsonobj.get("FUNCTION_NM") %>','<%=blHasAccess %>','<%=blIsFolder %>','<%=strOPT %>','<%=jsonobj.get("URL") %>','<%=strSqid %>')" style="text-decoration: none;cursor: default;color: #b9b3b3 !important;"><span class="hide-menu"><%=jsonobj.get("FUNCTION_NM") %></span></a></li>
							<%
									}
								}
								else if(intLevel == 1 && strType.equals("Folder"))
								{
									intPrevFolderLevelCount = intPrevFolderLevelCount + 1;
									intPrevFolderLevel = intLevel;
							%>
									<li> <a class="has-arrow waves-effect waves-dark <%= strActive %>"  aria-expanded="false" ><i><img border="0" align="absmiddle" src="/extweb/dhtmlx/dhtmlxTree/imgs/csh_dhx_skyblue/folderOpen.gif" style="padding: 0px; margin: 0px; width: 18px; height: 18px;"></i><span class="hide-menu"><%=jsonobj.get("FUNCTION_NM") %></span></a>
									<ul aria-expanded="false" class="collapse <%= strIn %>" style="cursor: pointer;">
								<%
								}
								
								if(intLevel > 1 && strType.equals("Folder"))
								{
									
									intPrevFolderLevelCount = intPrevFolderLevelCount + 1;
									intPrevFolderLevel = intLevel;
								%>
									
									<li> <a class="has-arrow waves-effect waves-dark" aria-expanded="false" ><i><img border="0" align="absmiddle" src="/extweb/dhtmlx/dhtmlxTree/imgs/csh_dhx_skyblue/folderOpen.gif" style="padding: 0px; margin: 0px; width: 18px; height: 18px;"></i><span class="marginfix"><%=jsonobj.get("FUNCTION_NM") %></span></a>
									<ul aria-expanded="false" class="collapse" style="cursor: pointer;">
								<%
								}
								else if(intLevel > 1 && strType.equals("File"))
								{
									if(blHasAccess)
									{
								%>
									
							                <li><a onclick="setSelectedId('<%=jsonobj.get("FUNCTION_ID") %>', '<%=jsonobj.get("FUNCTION_NM") %>','<%=blHasAccess %>','<%=blIsFolder %>','<%=strOPT %>','<%=jsonobj.get("URL") %>','<%=strSqid %>')"   ><span class="marginfix"><%=jsonobj.get("FUNCTION_NM") %></span></a></li>
							        
								<%
									}
									else
									{
								%>	
											<li><a onclick="setSelectedId('<%=jsonobj.get("FUNCTION_ID") %>', '<%=jsonobj.get("FUNCTION_NM") %>','<%=blHasAccess %>','<%=blIsFolder %>','<%=strOPT %>','<%=jsonobj.get("URL") %>','<%=strSqid %>')"   style="text-decoration: none;cursor: default;color:#b9b3b3 !important;" ><span class="marginfix"><%=jsonobj.get("FUNCTION_NM") %></span></a></li>
								<%
									}
								}
									
								intPrevLevel = intLevel;
							}
	
							%>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <nav class="sidebar-nav bg-change" >
                  <ul id="sidebarnav">
                     <% if(myFavorites.contains("true")){ %>
					  <li> <a class="waves-effect waves-dark"  onClick="javascript:fnRemFavNew();" style="background: #3172AA !important; color:white !important; padding-left:10px !important;" aria-expanded="false"><i class="fas fa-trash text-danger"></i><span class="hide-menu">Remove from Favorites</span></a></li>
                      <% } else {%>
					 <li> <a class="waves-effect waves-dark"  onClick="javascript:fnFavPopNew();" style="background: #3172AA !important; color:white !important; padding-left:10px !important;" aria-expanded="false"><i class="fas fa-plus text-success"></i><span class="hide-menu">Add to My Favorites</span></a></li>
                       <%} %>
                   </ul>
                </nav>
        </aside>
</html:form>


<%@ include file="/common/GmFooter.inc" %> 
