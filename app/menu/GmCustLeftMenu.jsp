 <%
/**********************************************************************************
 * File		 		: GmCustLeftMenu.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!--menu\GmCustLeftMenu.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>

<%
	
        String strCssPath = GmCommonClass.getString("GMSTYLES");
        String strImagePath = GmCommonClass.getString("GMIMAGES");
        String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
        String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
        String strMenuPath = GmCommonClass.getString("GMMENU");
	
	String strPgToLoad = (String)session.getAttribute("strSessPgToLoad");
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");
%>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="AUTHOR" content="Dhinakaran James">
<title>Globus Medical: LeftMenu</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javaScript" src="<%=strJsPath%>/Submenu.js"></script>
<script>
// To Hide the frame
function hidetoc()
{
	top.fstMain.cols = "0,*";
	top.TopFrame.document.all("showtoc").style.display = "block";
}

function mouseover(item)
{
  switch (item)
    {
    case "moveprevious" :
      window.status = "Click to Close the Tree node";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious2.gif";
      break;

    case "movenext" :
      window.status = "Click to Expand the Tree node";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext2.gif";
      break;

    case "hidetoc" :
      window.status = "Hide Menu";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc2.gif"
      //hidetoc();
      break;
    }
  }

function mouseout(item)
  {
  switch (item)
   {
    case "moveprevious" :
     window.status = "";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious1.gif";
      break;

    case "movenext" :
      window.status = "";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext1.gif";
      break;

    case "hidetoc" :
      window.status = "";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc1.gif"
      break;
    }
  }

function fnCallLink(page,obj)
{
	parent.RightFrame.location.href = "<%=strServletPath%>/GmPageControllerServlet?strPgToLoad="+page+"&strOpt="+obj;
}
</script>
</head>

<BODY bgcolor="#eeeeee">
<TABLE cellSpacing=0 cellPadding=0 border=0 width="100%"  height = "100%" valign="top">
  <TR height = "10">
	<TD  colspan = "2"  height = "10"   class="LeftTableHeader">
	<DIV align = "right">
 	 <img id="imgHideToc" style="cursor:hand" onclick="hidetoc();"
	 onmouseover="mouseover('hidetoc');" onmouseout="mouseout('hidetoc');"
	 src="<%=strImagePath%>/hidetoc1.gif" title="Hide Menu" />
	 </DIV>
	</TD>
  </TR>
<%	
	if ( strTopMenu.equals("Trans")){
%>
  <TR height = "100%">
	<TD valign="top">
	
			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartSearchServlet','');">
			Part Number Search</A>
			</TD></TR></TABLE>
			
			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmOrderItemServlet','PHONE');">
			Phone Order </A>
			</TD></TR></TABLE>
	
		<TABLE  cellspacing="0" BORDER=0><TR><TD height=20 width=2></TD>
		<TD valign="top">
		 <A Class=SubMenuMouseOver name = TSF onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Transfer </A><DIV>
			

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInitiateTransferServlet','ViewFullTransfer');">Full Transfer
			</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInitiateTransferServlet','ViewSetTransfer');">Set Transfer
			</A>
			</TD></TR></TABLE>
			
				<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmInitiateTransferServlet','ViewPartTransfer');">Part Transfer
			</A>
			</TD></TR></TABLE>
			
		</TABLE>

	<TABLE  cellspacing="0" BORDER=0><TR><TD height=20 width=2></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = ORD onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Process Orders</A><DIV>
		 
			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmOrderItemServlet','ProcBO');">
			Process Back Order</A>
			</TD></TR></TABLE>			
			
			<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmOrderVoidServlet','');">
			Modify Order</A>
			</TD></TR></TABLE>
		</DIV></TD></TR></TABLE>
		
	
									
	<TABLE  cellspacing="0" BORDER=0><TR><TD height=20 width=2></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = PO onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Process Consignments</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignSetServlet','');">
			Sets</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignItemServlet','');">
			Items</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignLoanerServlet','Grafts');">
			Allosource Loaners</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignModifyServlet','Modify');">
			Modify Consignment</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignModifyServlet','Reprocess');">
			Reprocess Consignment</A>
			</TD></TR></TABLE>
								
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignItemServlet','DummyTransfer');">
			Dummy Consignments</A>
			</TD></TR></TABLE>  
			
		</DIV></TD></TR></TABLE>


	<TABLE  cellspacing="0" BORDER=0><TR><TD width=2></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = Returns onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Process Returns</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmProcessReturnsServlet','');">
			Consignments</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmReturnSummaryServlet','');">
			Returns Summary</A>
			</TD></TR></TABLE>

		</DIV></TD></TR></TABLE>


	<script>
		CollapseAll(ORD);
		CollapseAll(PO);
		CollapseAll(Returns);
		CollapseAll(TSF);
	</script>

	<TABLE BORDER=0><TR><TD WIDTH=7></TD>
	<TD><IMG src="<%=strImagePath%>/leaf.gif">
	<A class =SubMenuMouseOver tabindex=-1 href=#
	onClick="fnCallLink('GmShippingReportServlet','TRANS');">
	Shipping</A>
	</TD></TR></TABLE>

	<TABLE cellspacing="0" BORDER=0><TR><TD WIDTH=2></TD>
	<TD><IMG src="<%=strImagePath%>/leaf.gif">
	<A class =SubMenuMouseOver tabindex=-1 href=#
	onClick="fnCallLink('GmInHouseSetServlet','4127');">
	Manage Loaners</A>
	</TD></TR></TABLE>	
						
	</TD>
</TR>
  <%
	 }else if ( strTopMenu.equals("Setup")){
  %>
  <TR height = "100%">
	<TD valign="top">

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmAccountServlet','');">
			Account Setup</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesRepServlet','');">
			Sales Rep Setup</A>
			</TD></TR></TABLE>

			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmAccountPriceReportServlet','Project');">
			Account/Part Number Pricing</A>
			</TD></TR></TABLE>
						
	</TD>
</TR>
<%	
	 }else if ( strTopMenu.equals("Report")){
  %>
  <TR height = "100%">
	<TD valign="top">

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmAccountReportServlet','ALL');">
			Account Report</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmTransferReportServlet','');">
			Transfer Report</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = CAN onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Cancel Report</A><DIV>
			<% /*Set the hOpt value as the action which needs to happen ie cancelreport + the cancel type.
				For eg  if the Rollback Set Consignment report has to be opened, then hopt value will be as cancelreport@RBSCN */  %>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmCommonCancelServlet','cancelreport@RBSCN');">Rollback Set Consignment Report
			</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmCommonCancelServlet','cancelreport@VODCN');">
			Void Item Consignment Report</A>
			</TD></TR></TABLE>
		</DIV></TD></TR></TABLE>
			

	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = CON onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Consignments Report</A><DIV>
		 
			 <TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmSalesConsignDetailsServlet','LoadDistributor');">
			Part Search (By Distributor)</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmSalesConsignDetailsServlet','LoadPart');">
			Part Search (By Part)</A>
			</TD></TR></TABLE>
					 
			 <TABLE BORDER=0><TR><TD WIDTH=7></TD>
				<TD><IMG src="<%=strImagePath%>/leaf.gif">
				<A class =SubMenuMouseOver tabindex=-1 href=#
				onClick="fnCallLink('GmSalesVirtualActualServlet','BYCDIST');">
				Consignment Net Report (By Distributor) </A>
			</TD></TR></TABLE>

			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesVirtualActualServlet','BYCSET');">
			Consignment Net Report (By Set) </A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
				<TD><IMG src="<%=strImagePath%>/leaf.gif">
				<A class =SubMenuMouseOver tabindex=-1 href=#
				onClick="fnCallLink('GmConsignSearchServlet','4110');">
				Transaction Search - Regular</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
				<TD><IMG src="<%=strImagePath%>/leaf.gif">
				<A class =SubMenuMouseOver tabindex=-1 href=#
				onClick="fnCallLink('GmConsignSearchServlet','4129');">
				Transaction Search - Dummy</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignmentReportServlet','Sets');">Sets
			</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignmentReportServlet','Items');">
			Items</A>
			</TD></TR></TABLE>
			
		</DIV></TD></TR></TABLE>


			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartNumSearchServlet','');">
			Part Number Transaction </A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPendingOrdersPOServlet','RPT');">
			Pending Orders (PO) Report</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmGrowthReportServlet','');">
			Growth Report</A>
			</TD></TR></TABLE>

	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = Credits onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Returns Report</A><DIV>

		
				<TABLE BORDER=0><TR><TD width=7></TD>
				<TD valign="top">
					 <A Class=SubMenuMouseOver name = RR onClick="Toggle(this)" href=#>
					 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Consignments</A><DIV>
			
						<TABLE BORDER=0><TR><TD WIDTH=7></TD>
						<TD><IMG src="<%=strImagePath%>/leaf.gif">
						<A class =SubMenuMouseOver tabindex=-1 href=#
						onClick="fnCallLink('GmReturnsReportServlet','Sets');">
						Sets</A>
						</TD></TR></TABLE>
			
						<TABLE BORDER=0><TR><TD WIDTH=7></TD>
						<TD><IMG src="<%=strImagePath%>/leaf.gif">
						<A class =SubMenuMouseOver tabindex=-1 href=#
						onClick="fnCallLink('GmReportCreditsServlet','Items');">
						Items</A>
						</TD></TR></TABLE>
					</DIV></TD></TR></TABLE>
						
		</DIV></TD></TR></TABLE>

		<TABLE BORDER=0><TR><TD WIDTH=7></TD>
		<TD><IMG src="<%=strImagePath%>/leaf.gif">
		<A class =SubMenuMouseOver tabindex=-1 href=#
		onClick="fnCallLink('GmLoanerPartRepServlet','');">
		Loaner Reconciliation </A>
		</TD></TR></TABLE>
			
	<script>
		CollapseAll(Credits);
		CollapseAll(CON);
		CollapseAll(RR);
		CollapseAll(CAN);		
	</script>

	</TD>
</TR>
<%	
	}
%>
</table>
</BODY>
</HTML>
