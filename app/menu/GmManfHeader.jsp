 <%
/**********************************************************************************
 * File		 		: GmManfHeader.jsp
 * Desc		 		: This screen is used for the Manufacturing Header
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>


<!--menu\GmManfHeader.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>

<%
      String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
      String strCssPath = GmCommonClass.getString("GMSTYLES");
      String strImagePath = GmCommonClass.getString("GMIMAGES");
      String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
      String strCommonPath = GmCommonClass.getString("GMCOMMON");

	String strTopMenu = (String)session.getAttribute("strSessSubMenu");
	String strUserShName = (String)session.getAttribute("strSessShName");
	String strLoginTS = (String)session.getAttribute("strSessLoginTS");
%>


<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Globus Medical: Manufacturing TopHeader</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function showtoc() // Called to maximise the hidden Left Navigation Menu
{
 top.fstMain.cols = "200,*";
 top.TopFrame.document.all("showtoc").style.display = "None";
}//End of showtoc()

function callLogOff()
{
	location.href = "<%=strServletPath%>/GmLogOffServlet";
}

function setTopMenu(opt)
{
	parent.location.href = "<%=strServletPath%>/GmManfFrameServlet?hSubMenu="+opt;
}

function fnCallPortal(val)
{
	if (val == 'OP')
	{
		parent.location.href = "<%=strServletPath%>/GmOperFrameServlet";
	}
	else if (val =='PM')
	{
		parent.location.href = "<%=strServletPath%>/GmProdFrameServlet";
	}
	else if (val =='CS')
	{
		parent.location.href = "<%=strServletPath%>/GmCustFrameServlet";
	}
	else if (val =='SL')
	{
		parent.location.href = "<%=strServletPath%>/GmSaleFrameServlet";
	}
}
</script>

<STYLE type="text/css">
.portalstyle
{position: absolute; visibility: hidden; left: 750; top: 18;}
</STYLE>

</head>

<BODY>
<table width="900" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td colspan="7">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	  <tr>
	    <td rowspan="3"><img src="<%=strImagePath%>/ss_logo.gif" border="0" valign="top"></td>
		<td background="<%=strImagePath%>/hdr_topbk.gif" height="22" width="100%" align="right" class="RightText">
		
		</td>
		<td valign="top"><img src="<%=strImagePath%>/hdr_toprt.gif"></td>
	  </tr>
	  <tr>
		<td colspan="2"><img src="<%=strImagePath%>/spacer.gif" height="1" width="80"><img src="<%=strImagePath%>/manf_header.gif" border="0" valign="middle" height="40"></td>
	  </tr>
	  <tr>
		<td background="<%=strImagePath%>/hdr_botbk.gif" height="22" align="right" class="RightText"> Welcome back, <b><%=strUserShName%>!</b> You last logged in on <%=strLoginTS%></td>
		<td><img src="<%=strImagePath%>/hdr_botrt.gif"></td>
	  </tr>
	</table>
   </td>
  </tr>
  <tr>
	<td>
	   <TABLE id="Table1" style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-LEFT: 1px; BORDER-BOTTOM: #000000 1px solid" height="20" cellSpacing="0" cellPadding="0" width="100%" bgColor="#2E73AC" border="0">
	   <tr>
		<td width="18" nowrap>
			  <div id="showtoc" name="showtoc" style="display:none;">
		   <table onClick="showtoc();" height="20" cellSpacing="0" cellPadding="0" border="0" style="cursor:hand">
				<tr>
				  <td height="18"><img title="Show Menu" border="0" src="<%=strImagePath%>/showtoc.gif"></td>
				</tr>
			   </table>
			  </div>
		</td>
		<td class="MenuItem" width="120" nowrap>&nbsp;</td>
		<td class="MenuItem" width="120"></td>
		<td class="MenuItem" width="120"></td>
		<td width="100%"></td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=callLogOff(); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Logout</A></td>
	   </tr>
	 </table>
	</td>
  </tr>
</table>
</BODY>
</HTML>
