 <%
/**********************************************************************************
 * File		 		: GmProdHeader.jsp
 * Desc		 		: This screen is used for the Operations Header
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!--menu\GmProdHeader.jsp -->




<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strCommonPath = GmCommonClass.getString("GMCOMMON");
	
	String strTopMenu = (String)session.getAttribute("strSessSubMenu")==null?"":(String)session.getAttribute("strSessSubMenu");
	String strUserShName = (String)session.getAttribute("strSessShName")==null?"":(String)session.getAttribute("strSessShName");
	String strLoginTS = (String)session.getAttribute("strSessLoginTS");
	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	boolean bolAccess = false;
	if (strDeptId.equals("Z") || strDeptId.equals("P"))
	{
		bolAccess = true;
	}
%>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Globus Medical: Operations TopHeader</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script>

function showtoc() // Called to maximise the hidden Left Navigation Menu
{
 top.fstMain.cols = "200,*";
 top.TopFrame.document.all("showtoc").style.display = "None";
}//End of showtoc()

function callLogOff()
{
	location.href = "<%=strServletPath%>/GmLogOffServlet";
}

function setTopMenu(opt)
{
	parent.location.href = "<%=strServletPath%>/GmProdFrameServlet?hSubMenu="+opt;
}

function fnCallPortal(val)
{
	if (val == 'OP')
	{
		parent.location.href = "<%=strServletPath%>/GmOperFrameServlet";
	}
	else if (val =='CS')
	{
		parent.location.href = "<%=strServletPath%>/GmCustFrameServlet";
	}
	else if (val =='AC')
	{
		parent.location.href = "<%=strServletPath%>/GmAcctFrameServlet";
	}
	else if (val =='SL')
	{
		parent.location.href = "<%=strServletPath%>/GmSaleFrameServlet";
	}
}
</script>

<STYLE type="text/css">
.portalstyle
{position: absolute; visibility: hidden; left: 750; top: 18;}
</STYLE>

</head>

<BODY>
<table width="900" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <td colspan="7">
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	  <tr>
	    <td rowspan="3"><img src="<%=strImagePath%>/ss_logo.gif" border="0" valign="top"></td>
		<td background="<%=strImagePath%>/hdr_topbk.gif" height="22" width="100%" align="right" class="RightText"><a href="#" onMouseOver="show('PortalLayer')" onMouseOut="closeOnTimeout('PortalLayer')" onClick="hide('PortalLayer')"><img src="<%=strImagePath%>/hdr_myportal.gif" border="0"></a>
			<DIV id="PortalLayer" class="portalstyle" onMouseOver="show('PortalLayer')" onMouseOut="hide('PortalLayer')">
			  <TABLE width="150" border="0" cellspacing="0" cellpadding="0" bgcolor="#333333">
			  <TR bgcolor="#666666"> 
				  <TD bgcolor="#666666" width="1" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" height="1" width="10"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" width="1" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
			  </TR>
			  <TR bgcolor="#666666"> 
				  <TD bgcolor="#666666" width="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#E1E1E1" width="15" valign="baseline"><IMG src="<%=strImagePath%>/sh_blt_333333.gif" width="11" height="11"></TD>
				  <TD bgcolor="#E1E1E1" valign="baseline"><A href="#" onClick="javascript:fnCallPortal('AC');" class="PortalText">Accounts</A></TD>
				  <TD bgcolor="#666666" width="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
			  </TR>	
			  <TR bgcolor="#666666"> 
				  <TD width="1" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD height="1" width="10"><IMG src="<%=strImagePath%>/spacer.gif" width="10" height="1"></TD>
				  <TD height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD width="1" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
			  </TR>
				  <TR bgcolor="#666666"> 
				  <TD bgcolor="#666666" width="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#E1E1E1" width="15" valign="baseline"><IMG src="<%=strImagePath%>/sh_blt_333333.gif" width="11" height="11"></TD>
				  <TD bgcolor="#E1E1E1" valign="baseline"><A href="#" onClick="javascript:fnCallPortal('CS');" class="PortalText">Customer Service</A></TD>
				  <TD bgcolor="#666666" width="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
			  </TR>
			  <TR bgcolor="#666666"> 
				  <TD bgcolor="#666666" width="1" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" height="1" width="10"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" width="1" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
			  </TR>
			  <TR bgcolor="#666666"> 
				  <TD bgcolor="#666666" width="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#E1E1E1" width="15" valign="baseline"><IMG src="<%=strImagePath%>/sh_blt_333333.gif" width="11" height="11"></TD>
				  <TD bgcolor="#E1E1E1" valign="baseline"><A href="#" onClick="javascript:fnCallPortal('OP');" class="PortalText">Operations</A></TD>
				  <TD bgcolor="#666666" width="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
			  </TR>
			  <TR bgcolor="#666666"> 
				  <TD bgcolor="#666666" width="1" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" height="1" width="10"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" width="1" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
			  </TR>
			  <TR bgcolor="#666666"> 
				  <TD bgcolor="#666666" width="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#E1E1E1" width="15" valign="baseline"><IMG src="<%=strImagePath%>/sh_blt_333333.gif" width="11" height="11"></TD>
				  <TD bgcolor="#E1E1E1" valign="baseline"><A href="#" onClick="javascript:fnCallPortal('SL');" class="PortalText">Sales</A></TD>
				  <TD bgcolor="#666666" width="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
			  </TR>
			  <TR bgcolor="#666666"> 
				  <TD bgcolor="#666666" width="1" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" height="1" width="10"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
				  <TD bgcolor="#666666" width="1" height="1"><IMG src="<%=strImagePath%>/spacer.gif" width="1" height="1"></TD>
			  </TR>
			  </TABLE>
			</DIV>		
		
		</td>
		<td valign="top"><img src="<%=strImagePath%>/hdr_toprt.gif"></td>
	  </tr>
	  <tr>
		<td colspan="2"><img src="<%=strImagePath%>/spacer.gif" height="1" width="80"><img src="<%=strImagePath%>/prod_header.gif" border="0" valign="middle" height="40"></td>
	  </tr>
	  <tr>
		<td background="<%=strImagePath%>/hdr_botbk.gif" height="22" align="right" class="RightText"> Welcome back, <b><%=strUserShName%>!</b> You last logged in on <%=strLoginTS%></td>
		<td><img src="<%=strImagePath%>/hdr_botrt.gif"></td>
	  </tr>
	</table>
   </td>
  </tr>
  <tr>
<%
	if ( strTopMenu.equals("Home")){
%>
	<td>
	   <TABLE id="Table1" style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-LEFT: 1px; BORDER-BOTTOM: #000000 1px solid" height="20" cellSpacing="0" cellPadding="0" width="100%" bgColor="#2E73AC" border="0">
	   <tr>
		<td width="18" nowrap>
			  <div id="showtoc" name="showtoc" style="display:none;">
		   <table onClick="showtoc();" height="20" cellSpacing="0" cellPadding="0" border="0" style="cursor:hand">
				<tr>
				  <td height="18"><img title="Show Menu" border="0" src="<%=strImagePath%>/showtoc.gif"></td>
				</tr>
			   </table>
			  </div>
		</td>
		<td class="MenuItemActive" width="120" nowrap ><A class="MenuItemText" style="COLOR: #ffffff" >Home</A></td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Trans'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Transactions</A></td>
<%
	if (bolAccess)
	{
%>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Setup'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Setup</A></td>
<%
	}
%>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Report'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Reports</A></td>
		<td width="100%"></td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=callLogOff(); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Logout</A></td>
	   </tr>
	 </table>
	</td>
<%
	}else if ( strTopMenu.equals("Trans")){
%>
	<td>
	   <TABLE id="Table1" style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-LEFT: 1px; BORDER-BOTTOM: #000000 1px solid" height="20" cellSpacing="0" cellPadding="0" width="100%" bgColor="#2E73AC" border="0">
	   <tr>
		<td width="18" nowrap>
			  <div id="showtoc" name="showtoc" style="display:none;">
		   <table onClick="showtoc();" height="20" cellSpacing="0" cellPadding="0" border="0" style="cursor:hand">
				<tr>
				  <td height="18"><img title="Show Menu" border="0" src="<%=strImagePath%>/showtoc.gif"></td>
				</tr>
			   </table>
			  </div>
		</td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Home'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Home</A></td>
		<td class="MenuItemActive" width="120" nowrap><A class="MenuItemText" style="COLOR: #ffffff">Transactions</A></td>
<%
	if (bolAccess)
	{
%>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Setup'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Setup</A></td>
<%
	}
%>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Report'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Reports</A></td>
		<td width="100%"></td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=callLogOff(); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Logout</A></td>
	   </tr>
	 </table>
	</td>
<%
	}else if( strTopMenu.equals("Setup")){
%>
	<td>
	   <TABLE id="Table1" style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-LEFT: 1px; BORDER-BOTTOM: #000000 1px solid" height="20" cellSpacing="0" cellPadding="0" width="100%" bgColor="#2E73AC" border="0">
	   <tr>
		<td width="18" nowrap>
			  <div id="showtoc" name="showtoc" style="display:none;">
		   <table onClick="showtoc();" height="20" cellSpacing="0" cellPadding="0" border="0" style="cursor:hand">
				<tr>
				  <td height="18"><img title="Show Menu" border="0" src="<%=strImagePath%>/showtoc.gif"></td>
				</tr>
			   </table>
			  </div>
		</td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Home'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff" >Home</A></td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Trans'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Transactions</A></td>
		<td class="MenuItemActive" width="120" nowrap><A class="MenuItemText" style="COLOR: #ffffff">Setup</A></td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Report'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Reports</A></td>
		<td width="100%"></td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=callLogOff(); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Logout</A></td>
	   </tr>
	 </table>
	</td>
<%
	}else if( strTopMenu.equals("Report")){
%>
	<td>
	   <TABLE id="Table1" style="BORDER-RIGHT: #000000 1px solid; BORDER-TOP: #000000 1px solid; BORDER-LEFT: 1px; BORDER-BOTTOM: #000000 1px solid" height="20" cellSpacing="0" cellPadding="0" width="100%" bgColor="#2E73AC" border="0">
	   <tr>
		<td width="18" nowrap>
			  <div id="showtoc" name="showtoc" style="display:none;">
		   <table onClick="showtoc();" height="20" cellSpacing="0" cellPadding="0" border="0" style="cursor:hand">
				<tr>
				  <td height="18"><img title="Show Menu" border="0" src="<%=strImagePath%>/showtoc.gif"></td>
				</tr>
			   </table>
			  </div>
		</td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Home'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Home</A></td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Trans'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Transactions</A></td>
<%
	if (bolAccess)
	{
%>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=setTopMenu('Setup'); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Setup</A></td>
<%
	}
%>
		<td class="MenuItemActive" width="120" nowrap><A class="MenuItemText" style="COLOR: #ffffff">Reports</A></td>
		<td width="100%"></td>
		<td class="MenuItem" width="120" nowrap onmouseover="changeBgColor(this,'#4791C5');" onClick=callLogOff(); onmouseout="changeBgColor(this,'#2E73AC');"><A class="MenuItemText" style="COLOR: #ffffff">Logout</A></td>
	   </tr>
	 </table>
	</td>
<%
	}
%>
  </tr>
</table>
</BODY>
</HTML>
