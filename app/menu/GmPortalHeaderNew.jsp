<%@page import="com.globus.common.beans.GmCommonBean"%>
<%
/**********************************************************************************
 * File		 		: GmPortalHeaderNew.jsp
 * Desc		 		: This screen is used for the all Portal Header
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!--menu\GmPortalHeaderNew.jsp -->

<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector,java.util.HashMap" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>
<%@ include file="/common/GmHeaderNew.inc" %>
<%
	String strShowAlert = GmCommonClass.parseNull((String)GmCommonClass.getString("SHOW_ALERT"));		
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");
	String strUserFName = (String)session.getAttribute("strSessFName");
	String strUserLName = (String)session.getAttribute("strSessLName");
	String strUserName = strUserFName+ ' ' + strUserLName;
	String strLoginTS = (String)session.getAttribute("strSessLoginTS");
	String strSessAlertsCnt = GmCommonClass.parseNull((String)session.getAttribute("strSessAlertsCnt"));
	String strShowCompany = GmCommonClass.getString("SHOW_COMPANY_DRPDWN");
	String strShowPlant = GmCommonClass.getString("SHOW_PLANT_DRPDWN");
	String strParamCmp = GmCommonClass.parseNull((String)request.getParameter("company")); 
	strShowCompany = (strParamCmp.equals("hide")) ? "NO" : "YES";
	strShowPlant = (strParamCmp.equals("hide")) ? "NO" : "YES";
	String strCntFlg = (strParamCmp.equals("hide")) ? "NO" : "YES";	
	
	String  strBaseURL = GmCommonClass.parseNull((String)GmCommonClass.getString("MICRO_APP_BASE_URL"));
	//String  strIndexURL = GmCommonClass.parseNull((String)GmCommonClass.getString("MICRO_APP_INDEX_URL"));  // ngix issue resolved, index.html suffix in the url not need  
	ArrayList alRuleValue = GmCommonClass.parseNullArrayList((ArrayList)session.getAttribute("hmMicroCredetials"));	
	int intSize = alRuleValue.size();
 	HashMap hmResult = new HashMap();
 	String strMicroURL; 
	String strURL;
	//userAgent - PC-5231-add message in login to migrate message
 	String userAgent = request.getHeader("User-Agent");
    String edgeBrowserMsg = GmCommonClass.parseNull((String)GmCommonClass.getString("EDGE_BROWSER_MIGRATE_MESSAGE"));
%>
<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Globus Medical: Manufacturing TopHeader</title>
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/GmProgress.css">
<script language="JavaScript" src="<%=strJsPath%>/GmProgressNew.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmProgress.js"></script>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxcommon.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/GmPortalHeaderNew.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script>
function callLogOffNew()
{
	<%-- //location.href = "<%=strServletPath%>/GmLogOffServlet?companyInfo="+encodeURIComponent(JSON.stringify(top.gCompanyInfo)); --%>
	
	<%
	 		String strKeyCloak = (String) session.getAttribute("strKeyCloak");
	 		String strOpt = (String) session.getAttribute("strOpt");
	 		//keycloak variables   
	 		String strurlindex = GmCommonClass.parseNull((String)GmCommonClass.getString("GMURLINDEX"));
		    String strkeycloakurl = GmCommonClass.parseNull((String)GmCommonClass.getString("GMKEYCLOAKURL"));
			String strkeycloakrealm = GmCommonClass.parseNull((String)GmCommonClass.getString("GMKEYCLOAKREALM"));
			String strkeycloakclientid = GmCommonClass.parseNull((String)GmCommonClass.getString("GMKEYCLOAKCLIENTID"));
	%>
		var varKeycloak="<%=strKeyCloak%>"; 
		var varOpt="<%=strOpt%>"; 
		if(varKeycloak != 'N'){
			location.href = "<%=strServletPath%>/GmLogOffServlet?companyInfo="+encodeURIComponent(JSON.stringify(top.gCompanyInfo));
			// Keycloak Logout functionality (Remove keycloak sessions)
			var urlIndex = "<%=strurlindex%>";
		    var keycloakConfig = {url: "<%=strkeycloakurl%>", realm: "<%=strkeycloakrealm%>", clientId: "<%=strkeycloakclientid%>"};
		    var keycloakService = {};
		    var config = keycloakConfig;
		    window.parent.location.href = keycloakConfig.url+'/realms/'+keycloakConfig.realm+'/protocol/openid-connect/logout?redirect_uri='+urlIndex;  
	   }else if(varOpt == 'SwitchUser'){ 
	   	 top.close();
       }else{
	   	location.href = "<%=strServletPath%>/GmLogOffServlet?companyInfo="+encodeURIComponent(JSON.stringify(top.gCompanyInfo));
       }
}

function fnCallUnreadAlert(){
	parent.RightFrame.location.href = "/gmAlertsReportAction.do?strOpt=Reload&alersActionable=102544&haction=Header"; // 102544: YES [Actionable is YES]
}

function fnClassicLocation(){
	
	window.location.href="/GmEnterprisePortal.jsp";
}

</script>
</head>

<body class="skin-blue fixed-layout" onload="fnPageLoadNew()">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
   <div class="preloader">
   		<div class="loader">
   			<div class="loader__figure"></div>
   			<p class="loader_label">GlobusOne</p>
   		</div>
   </div> 
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" style="height:60px" >
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand "  style="margin-left: -9px" >
                        <!-- Logo icon --><b style="padding:5px;">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="assets/images/globusone-icon.png" alt="homepage" class="dark-logo" style="width: 50px;height: 58px;padding-bottom: 6px;" />
                            <!-- Light Logo icon -->
                            <img src="assets/images/globusone-icon.png" alt="homepage" class="light-logo" style="width: 50px;height: 58px;padding-bottom: 6px;" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="assets/images/globusone-logo.png" alt="homepage" class="dark-logo" style="width: 215px;height: 48px;" />
                         <!-- Light Logo text -->    
                         <img src="assets/images/globusone-logo.png" class="light-logo" alt="homepage" style="width: 215px;height: 48px;"  /></span> </a>
                </div>
                
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav">
                        <!-- This is  -->
     
                         <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark"  ><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark"  ><i class="icon-menu" style="color:white"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        
                        
                      
                    </ul>
                  
                   <ul class="navbar-nav mr-auto">
                   <li id="logo-td" style="visibility:hidden"  nowrap><img height="20" width="20" style="margin-top:7px" id="logo-td" border="0" align="left" src="<%=strImagePath%>/tab_icon_plus.jpg"></li>
                   <li class="nav-item desktop-header" ><h4 style="color:#fff;font-weight:bold;margin-top:2px"  id="header_message"></h4>
                    <%if(!userAgent.contains("Edg")!= false){%>
                    <marquee behavior="alternate" style=" margin: 0px; position: absolute;bottom: 10;width: 48%;"><span style="color:#fff;font-weight:bold"><%=edgeBrowserMsg%></span> </marquee>
                    <%}%>
                   </li>
                   </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                     <ul class="navbar-nav">
                     <li class="nav-item"  style="width:150px;margin-right: -74px;margin-top:2px">
                         	<div><input type="checkbox" class="js-switch" data-size="small" checked data-color="#ffbc34" onchange="fnClassicLocation()"><label style="color:#fff;font-size:11px;font-weight: bold;">&nbsp;&nbsp;New Look on</label></div> 
                         </li>
                     </ul>
                    <ul class="navbar-nav my-lg-0 head-width" >
                      <!--    <li class="nav-item" style="margin-top:12px">
                    	 	 <label class="switch waves-effect waves-dark"><input type="checkbox" id="togBtn" onchange="fnClassicLocation()" ><div class="slider rounds" >ADDED HTML<span class="off">Classic View</span>END</div></label>
                         </li>  -->
                         
                         <!--  <li class="nav-item flag-left" style="display:<%=strCntFlg.equals("YES")?"inline-block":"none"%>;margin-left: -37px;">
    	              		 <img id="country_flag"  width="20" height="15" src="" hspace="5" vspace="8"  border="0"  >
                    	 </li>-->
		                <li class="nav-item" style="width:374px;margin-right: -30px">
	                                  <div id="Company" style="display:<%=strShowCompany.equals("YES")?"block":"none"%>;">
	                                      <label class="control-label col-sm-4 col-md-4  text-right" style="color:#fff;font-size:11px; font-weight: bold;">Company:</label>
	                                      <select class="form-control-sm custom-select-sm col-md-7 col-sm-7" id="Cbo_Company"  name="Cbo_Company" style="background-color: #fff;font-size:11px;" onchange="fnChangeCompanyNew();"></select>  
	                                </div>
	                      </li>
	                         
	                      <li class="nav-item"> 
	                              <div id="Plant" style="display:<%=strShowPlant.equals("YES")?"block":"none"%>">
	                                  <label class="control-label col-md-4 col-sm-4 text-right" style="color:#fff;font-size:11px;font-weight: bold;">Plant:</label>
	                                  <select class="form-control-sm custom-select-sm col-md-7 col-sm-7" id="Cbo_Plant"  name="Cbo_Plant"  style="background-color: #fff;font-size:11px;" onchange="fnChangePlantNew();"></select>  
	                             </div>
	                      
		             </li>
                        <!-- ============================================================== -->
                        <!-- End User Profile -->
                        <!-- ============================================================== -->
                    </ul>
                    <ul class="navbar-nav my-auto">
						<li class="nav-item dropdown u-pro" style="margin-top: -3px;"><a
							class="nav-link dropdown-toggle waves-effect waves-dark profile-pic"
							href="" data-toggle="dropdown" aria-haspopup="true"
							aria-expanded="false"><img src="/assets/images/users/Globus_User.png" width="30" height="25"
								alt="user" class=""> <span class="hidden-md-down" style="color:#fff;font-size:11px;font-weight: bold;"><%=strUserName%>
									&nbsp;<i class="fa fa-angle-down"></i>
							</span> </a>
							<div class="dropdown-menu dropdown-menu-right animated flipInY">
								<!-- text-->
								<a   class="dropdown-item" style="font-size:11px !important"><i
									class="ti-user"></i> You last logged in on <%=strLoginTS%></a>
								<!-- text-->
								<div class="dropdown-divider"></div>
								<!-- text-->
								<a  class="dropdown-item" onClick=callLogOffNew(); style="font-size:11px !important"><i
									class="fa fa-power-off"></i> Logout</a>
								<!-- text-->
							</div></li>
						 <li class="nav-item right-side-toggle" style="margin-top:15px"><a
							 class="nav-link  waves-effect waves-light"
							><i class="fas fa-th" style="color:white"></i></a></li>
					</ul>
                </div>	
            </nav>
        </header>
        
				<div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title" > <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20" >
                                <li><b></b></li>
<%								
							for (int i=0;i<intSize;i++)
							{
									hmResult = (HashMap)alRuleValue.get(i);
									String strRuleVal=	hmResult.get("RULEVALUE").toString();
									
									strMicroURL = strRuleVal.split("\\.")[0];
									strURL = strBaseURL+strMicroURL;
									int curPosition = i+1;
									int value = curPosition%3;
								%>  
								<%if(hmResult.get("ACCESSLEVEL").equals("Y")) {%>&emsp;
							   <li class="frame"><a href="<%=strURL%>" target="_blank"><img src="/assets/images/<%=hmResult.get("RULEVALUE")%>" width="60px" height="60px" 
							   style="width: 50px;height: 50px;" title="<%=hmResult.get("RULEDESC")%>"/>
							   <span> <small style="color: black;"><label class="info-label"></label><b style='padding-left:0px' class="info-bold" ><center><%=hmResult.get("RULEDESC")%></center></b>
							   </small></span></a></li>
							   <%if(value == 0){%> <br><br><%}%>
							   <%} else {%>&emsp;
     		 				   <li class="frame"><a  style="cursor: not-allowed;"><img src="/assets/images/<%=hmResult.get("RULEVALUE")%>" width="60px" height="60px"
							   style="width: 50px;height: 50px;opacity: 0.5;" title="<%=hmResult.get("RULEDESC")%>"/>
							   <span> <small style="color: black;"><label class="info-label"></label><b style='padding-left:0px' class="info-bold"><center><%=hmResult.get("RULEDESC")%></center></b>
							   </small></span></a></li>
							   <%if(value == 0){%> <br><br><%}%>
							   
							   
							   <%}%>							   
							<%
							}
							%>                                 
                            </ul>
                        </div>
                    </div>
                </div>  
    



