 <%
/**********************************************************************************
 * File		 		: GmSaleLeftMenu.jsp
 * Desc		 		: This screen is used for the 
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>


<!--menu\GmSaleLeftMenu.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import ="com.globus.common.beans.GmCommonClass"%>

<%
	String strCssPath = GmCommonClass.getString("GMSTYLES");
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");

	String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
	String strMenuPath = GmCommonClass.getString("GMMENU");
	
	String strPgToLoad = (String)session.getAttribute("strSessPgToLoad");
	String strTopMenu = (String)session.getAttribute("strSessSubMenu");

	String strDeptId = (String)session.getAttribute("strSessDeptId")==null?"":(String)session.getAttribute("strSessDeptId");
	String strAccessLvl = (String)session.getAttribute("strSessAccLvl")==null?"":(String)session.getAttribute("strSessAccLvl");
	int intAccessLvl = Integer.parseInt(strAccessLvl);
	boolean bolAccess = true;
	boolean bolAdmin = false;
	
	if (strDeptId.equals("S") && intAccessLvl < 3)
	{
		bolAccess = false;
	}
	
	if ((strDeptId.equals("S") && intAccessLvl == 5) || strDeptId.equals("Z"))
	{
		bolAdmin = true;
	}	
	
%>

<HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="AUTHOR" content="Dhinakaran James">
<title>Globus Medical: LeftMenu</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<script language="JavaScript" src="<%=strJsPath%>/GlobusCommonScript.js"></script>
<script language="javaScript" src="<%=strJsPath%>/Submenu.js"></script>
<script>
// To Hide the frame
function hidetoc()
{
	top.fstMain.cols = "0,*";
	top.TopFrame.document.all("showtoc").style.display = "block";
}

function mouseover(item)
{
  switch (item)
    {
    case "moveprevious" :
      window.status = "Click to Close the Tree node";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious2.gif";
      break;

    case "movenext" :
      window.status = "Click to Expand the Tree node";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext2.gif";
      break;

    case "hidetoc" :
      window.status = "Hide Menu";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc2.gif"
      //hidetoc();
      break;
    }
  }

function mouseout(item)
  {
  switch (item)
   {
    case "moveprevious" :
     window.status = "";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious1.gif";
      break;

    case "movenext" :
      window.status = "";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext1.gif";
      break;

    case "hidetoc" :
      window.status = "";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc1.gif"
      break;
    }
  }

function fnCallLink(page,obj)
{
	parent.RightFrame.location.href = "<%=strServletPath%>/GmPageControllerServlet?strPgToLoad="+page+"&strOpt="+obj;
}
</script>
</head>

<BODY bgcolor="#eeeeee">
<TABLE cellSpacing=0 cellPadding=0 border=0 width="100%"  height = "100%" valign="top">
  <TR height = "10">
	<TD  colspan = "2"  height = "10"   class="LeftTableHeader">
	<DIV align = "right">
 	 <img id="imgHideToc" style="cursor:hand" onclick="hidetoc();"
	 onmouseover="mouseover('hidetoc');" onmouseout="mouseout('hidetoc');"
	 src="<%=strImagePath%>/hidetoc1.gif" title="Hide Menu" />
	 </DIV>
	</TD>
  </TR>
<%	
	if (strTopMenu.equals("Report")){
%>
  <TR height = "100%">
	<TD valign="top">

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmAccountReportServlet','');">
			Accounts Report</A>
			</TD></TR></TABLE>

			<!--<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesAcctPriceReportServlet','STP');">
			Set-up of Account Pricing</A>
			</TD></TR></TABLE> -->

			<!--<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesAcctPriceReportServlet','DTL');">
			Account Pricing</A>
			</TD></TR></TABLE>  -->

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPendingOrdersPOServlet','RPT');">
			Orders Pending PO's</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmDetailedSalesServlet','');">
			Detailed Monthly Sales</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmMonthlyDashboardServlet','LoadStateSales');">
			Monthly Dashboard</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmDetailStateSalesServlet','LoadStateSales');">
			Sales By State</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesByProjectServlet','LoadProjectSales');">
			Sales By Project</A>
			</TD></TR></TABLE>

	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = YTDA onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Historical Sales </A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesYTDReportServlet','LoadDistributorA');">
			By Field Sales</A>
			</TD></TR></TABLE>
		
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesYTDReportServlet','LoadRepA');">
			By Spine Specialist</A>
			</TD></TR></TABLE>


			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesYTDReportServlet','LoadAccountA');">
			By Account</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetYTDReportServlet','LoadGroupA');">
			By Group</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesYTDReportServlet','LoadPartA');">
			By Part</A>
			</TD></TR></TABLE>		

		</DIV></TD></TR></TABLE>
		
	<script>
		CollapseAll(YTDA);
	</script>


	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = YTD onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Territory Sales</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesYTDReportServlet','LoadSummary');">
			Summary</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesYTDReportServlet','LoadTerritory');">
			By Territory</A>
			</TD></TR></TABLE>


			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesYTDReportServlet','LoadAccount');">
			By Account</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetYTDReportServlet','LoadGroup');">
			By Group</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesYTDReportServlet','LoadPart');">
			By Part</A>
			</TD></TR></TABLE>		

<%	// Quota Information only have access to 
	// Sales, Accounts, Managment and Z user
	if (strDeptId.equals("S") || strDeptId.equals("A") || strDeptId.equals("M") ||strDeptId.equals("Z"))
	{ %>
		

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmTerritoryMappingServlet','LoadQuota');">
			By Quota Breakup</A>
			</TD></TR></TABLE>		
<%	} %>
		</DIV></TD></TR></TABLE>
		
	<script>
		CollapseAll(YTD);
	</script>



	
	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = SGR onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Territory Performance</A><DIV>

<%	// Quota Information only have access to 
	// Sales, Accounts, Managment and Z user
	if (strDeptId.equals("S") || strDeptId.equals("A") || strDeptId.equals("M") ||strDeptId.equals("Z"))
	{ %>
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmQuotaTrackingServlet','QuotaTrack');">
			Summary </A>
			</TD></TR></TABLE>
<%	} %>
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesGrowthServlet','TERR');">
			By Territory</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesGrowthServlet','ACCT');">
			By Account</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesGrowthServlet','PGRP');">
			By Group</A>
			</TD></TR></TABLE>
			
	</DIV></TD></TR></TABLE>

	<script>
		CollapseAll(SGR);
	</script>

<%	// Below code to display consignment details 
	// Quota Information only have access to 
	// Sales, Accounts, Managment and Z user
	if (!strDeptId.equals("S") || (strDeptId.equals("S") && intAccessLvl > 2  ))
	{ 
%>	
	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = CONMANSDET onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Consignment Details</A><DIV>

		<!--	<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesConsignmentServlet','LoadSummary');">
			Summary</A>
			</TD></TR></TABLE> -->
			
<%			if(intAccessLvl != 3)
	  		{   
%>			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesConsignmentServlet','LoadAD');">
			By AD</A>
			</TD></TR></TABLE>									
<%
		    }
%>
			
		 	<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesConsignmentServlet','LoadGCONS');">
			By Group</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesConsignmentServlet','LoadDCONS');">
			By Field Sales</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmSalesConsignDetailsServlet','LoadPart');">
			Details By Part</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=# 
			onClick="fnCallLink('GmSalesConsignDetailsServlet','LoadDistributor');">
			Details By Field Sales</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesVirtualActualServlet','BYCDISTSALES');">
			Net Report(Fields Sales) </A>
			</TD></TR></TABLE>

			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesVirtualActualServlet','BYCSETSALES');">
			Net Report(Set) </A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetReportServlet','SETRPT');">
			Set List Report</A>
			</TD></TR></TABLE>
									
			<!-- <TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesConsignSearchServlet','BYPART');">
			Part Search</A>
			</TD></TR></TABLE> -->
						
		</DIV></TD></TR></TABLE>
		
	<script>
		CollapseAll(CONMANSDET);
	</script>
<% }%>		
<!--
	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = CONR onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Consignment Reports</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignmentReportServlet','SETNET');">
			Sets - Net</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignmentReportServlet','SETTRANS');">
			Sets - Transaction</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignmentReportServlet','PARTNET');">
			Part Number - Net</A>
			</TD></TR></TABLE>		
						
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmConsignmentReportServlet','PARTTRANS');">
			Part Number - Transaction</A>
			</TD></TR></TABLE>
			
	</DIV></TD></TR></TABLE>

	<script>
		CollapseAll(CONR);
	</script>
	
-->
	</TD>
</TR>
  <%
	 }else if ( strTopMenu.equals("Setup") && bolAccess){
  %>
  <TR height = "100%">
	<TD valign="top">
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmDistributorServlet','');">
			Distributorship Setup</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD width=7></TD>
			<TD valign="top">
				 <A Class=SubMenuMouseOver name = TERR onClick="Toggle(this)" href=#>
				 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Territory Maintenance</A><DIV>
		
					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=#
					onClick="fnCallLink('GmTerritoryServlet','');">
					Setup</A>
					</TD></TR></TABLE>
		
					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=#
					onClick="fnCallLink('GmTerritoryServlet','Report');">
					Mapping Report</A>
					</TD></TR></TABLE>

					<TABLE BORDER=0><TR><TD WIDTH=7></TD>
					<TD><IMG src="<%=strImagePath%>/leaf.gif">
					<A class =SubMenuMouseOver tabindex=-1 href=#
					onClick="fnCallLink('GmTerritoryServlet','MapReport');">
					All Mapping Report</A>
					</TD></TR></TABLE>
										
				</DIV></TD></TR></TABLE>
					
			<script>
				CollapseAll(TERR);
			</script>
	
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSalesRepServlet','');">
			Sales Rep Setup</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmAccountServlet','');">
			Account Setup</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmAccountReportServlet','ALL');">
			All Accounts Report</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmGroupSetMaintServlet','LINK');">
			Group-Set Mapping</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetBaselineValueServlet','20176');">
			Organization List Turns</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmSetBaselineValueServlet','20177');">
			Organization Baseline Case</A>
			</TD></TR></TABLE>
			
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmVanGuardSetupServlet','Load');">
			VanGuard Group</A>
			</TD></TR></TABLE> 

<%
	if (bolAdmin){
%>
			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmPartPriceReportServlet','');">
			Part Number Pricing</A>
			</TD></TR></TABLE>

						

	<TABLE BORDER=0><TR><TD width=7></TD>
	<TD valign="top">
		 <A Class=SubMenuMouseOver name = ACCP onClick="Toggle(this)" href=#>
		 <IMG border=0 src="<%=strImagePath%>/plus.gif">&nbsp;Account Pricing</A><DIV>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmAccountPriceReportServlet','Project');">
			By Project</A>
			</TD></TR></TABLE>

			<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmAccountPriceReportServlet','Batch');">
			Batch Update</A>
			</TD></TR></TABLE>
			
		</DIV></TD></TR></TABLE>
		
		<TABLE BORDER=0><TR><TD WIDTH=7></TD>
			<TD><IMG src="<%=strImagePath%>/leaf.gif">
			<A class =SubMenuMouseOver tabindex=-1 href=#
			onClick="fnCallLink('GmTrackImplantSummaryServlet','');">
			Tracking Implants Summary</A>
			</TD></TR></TABLE>	
			
	<script>
		CollapseAll(ACCP);
	</script>
<%
	}
%>
			
	</TD>
</TR>
<%	
	}
%>
</table>
</BODY>
</HTML>
