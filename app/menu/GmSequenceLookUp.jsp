 <%
/**********************************************************************************
 * File		 		: GmSequenceLookUp.jsp
 * Desc		 		: This screen is used for the Sequence Look Up
 * Version	 		: 1.0
 * author			: dreddy
************************************************************************************/
%>

<!--menu\GmSequenceLookUp.jsp -->


<%@ page language="java" %>
<%@ page import ="java.util.ArrayList"%>
<%@ page import = "javax.servlet.http.*"%>
<%@ include file="/common/GmHeader.inc" %>

<bean:define id="strTreeXML" name="frmUserNavigationSetup" property="treeXmlData" type="java.lang.String"></bean:define>

<html>
<head>
	<title>Globus Medical: Sequence Look up</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style>
	a, A:link, a:visited, a:active, A:hover
		{color: #000000; text-decoration: none; font-family: Tahoma, Verdana; font-size: 11px}
</style>
<script language="JavaScript" src="<%=strExtWebPath%>/dhtmlx/dhtmlxgrid_export.js"></script>
<link rel="STYLESHEET" type="text/css" href="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxtree.css">
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxAccordion/dhtmlxcommon.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxAccordion/dhtmlxaccordion.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxAccordion/dhtmlxcontainer.js"></script>
<script src="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxcommon.js"></script>		 
<script  src="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxtree.js"></script>
<script  src="<%=strExtWebPath%>/dhtmlx/dhtmlxTree/dhtmlxtree_ed.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Message.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/Error.js"></script>
<script  src="<%=strJsPath%>/GmUserNav.js"></script>
<script>
	setLeftMenuTree('<%=strTreeXML%>');
</script>
</head>
<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="white" onload="javascript:fnLoad();">
 
<html:form  action="/gmUserNavigation.do"  >

<TABLE cellSpacing=0 cellPadding=0 border=0 width="99%"  height = "100%" valign="top">
   <tr>
	 <td height="25" class="RightDashBoardHeader">Sequence Look Up For <bean:write name="frmUserNavigationSetup" property="userGroupName" /></td>
  </tr>
 <tr>
    <td colspan="4" valign="top">			            
        <div id="treeboxbox_tree" style=" border :1px solid Silver;overflow:hidden" ></div> <br>
       </td>
</tr>	
<tr height="25">
    <td colspan="4" valign="middle" align="center">
    <div id="msg" style="color: red;font-weight: bold;"></div>
    </td>
</tr>
</TABLE>

</html:form>
</body>
</html>