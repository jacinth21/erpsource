 <%
/**********************************************************************************
 * File		 		: GmPortalNav.jsp
 * Desc		 		: This screen is used for the Menu structure of the common Left Menu
 * Version	 		: 1.0
 * author			: Dhinakaran James
************************************************************************************/
%>

<!--menu\GmPortalNav.jsp -->


<%@ page language="java" %>
<%@ page import="java.util.ArrayList,java.util.Vector" %>
<%@ page import = "javax.servlet.http.*"%>
<%
	session = request.getSession(false);
    
    String strCssPath = GmCommonClass.getString("GMSTYLES");
    String strImagePath = GmCommonClass.getString("GMIMAGES");
    String strJsPath = GmCommonClass.getString("GMJAVASCRIPT");
    String strServletPath = GmCommonClass.getString("GMSERVLETPATH");	

	String strSessDeptId = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptId"));
	String strSessDeptSeq = GmCommonClass.parseNull((String)session.getAttribute("strSessDeptSeq"));
	String strDeptId = GmCommonClass.parseNull(request.getParameter("hDeptId"));
	String strMenu = "";
	
	strDeptId = strDeptId.equals("")?strSessDeptSeq:strDeptId;

	String strAccessLvl = GmCommonClass.parseNull((String)session.getAttribute("strSessAccLvl"));
	
	int intAccessLvl = Integer.parseInt(GmCommonClass.parseZero(strAccessLvl));
	
	if (strSessDeptSeq.equals("2005") && intAccessLvl < 3 )
	{
		strMenu = strDeptId.concat("_Out");
	}
	/*
	else if (strSessDeptSeq.equals("2005") && (intAccessLvl > 2 && intAccessLvl < 5 ))
	{
		strMenu = strDeptId.concat("_In");	
	}
	*/
	else if (strSessDeptSeq.equals("2005") && (intAccessLvl == 3) )
	{
		strMenu = strDeptId.concat("_In");	
	}
	// Adding this for releasing EPIC report to VPs 
	else if (strSessDeptSeq.equals("2005") && (intAccessLvl > 3  && intAccessLvl < 5))
	{
		strMenu = strDeptId.concat("_VP");	
	}	
	
	else if (strSessDeptSeq.equals("2005") && intAccessLvl == 5 )
	{
		strMenu = strDeptId;
	}
	else if (strSessDeptSeq.equals("2007") && intAccessLvl > 2 && strDeptId.equals("2007"))
	{
		strMenu = "2005-R";
	}
	else
	{
		strMenu = strDeptId;
	}
	boolean bolAccess = true;
	boolean bolAdmin = false;
	
	String strPgToLoad = GmCommonClass.parseNull(request.getParameter("hSessPg"));
	if (!strPgToLoad.equals(""))
	{
		session.setAttribute("strSessPgToLoad",strPgToLoad);
	}
	
%>

<html>
<head>
	<title>Globus Medical: Portal Navigator</title>
<link rel="stylesheet" type="text/css" href="<%=strCssPath%>/Globus.css">
<style>
	a, A:link, a:visited, a:active, A:hover
		{color: #000000; text-decoration: none; font-family: Tahoma, Verdana; font-size: 11px}
</style>
<script>
function hidetoc()
{
	top.fstMain.cols = "0,*";
	top.TopFrame.document.all("showtoc").style.display = "block";
}

function mouseover(item)
{
  switch (item)
    {
    case "moveprevious" :
      window.status = "Click to Close the Tree node";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious2.gif";
      break;

    case "movenext" :
      window.status = "Click to Expand the Tree node";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext2.gif";
      break;

    case "hidetoc" :
      window.status = "Hide Portal Navigator";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc2.gif"
      //hidetoc();
      break;
    }
  }

function mouseout(item)
  {
  switch (item)
   {
    case "moveprevious" :
     window.status = "";
      document.all.imgMovePrevious.src = "<%=strImagePath%>/moveprevious1.gif";
      break;

    case "movenext" :
      window.status = "";
      document.all.imgMoveNext.src = "<%=strImagePath%>/movenext1.gif";
      break;

    case "hidetoc" :
      window.status = "";
      document.all.imgHideToc.src = "<%=strImagePath%>/hidetoc1.gif"
      break;
    }
  }
  
function fnCallLink(page,obj)
{
	parent.RightFrame.location.href = "<%=strServletPath%>/GmPageControllerServlet?strPgToLoad="+page+"&strOpt="+obj;
}

function fnReload(obj)
{
	val = obj.value;
	sess = obj[obj.selectedIndex].id;
	if(val != 2007)
	{
		self.location.href = "/menu/GmPortalNav.jsp?hDeptId="+val+"&hSessPg="+sess;
	}
	else
	{
		top.location.href = "/GmClinicalPortal.jsp?";
	}
}

var aclevel = '<%=strAccessLvl%>';
function fnLoad()
{
	var val = document.FrmMenu.hSessPg.value;
	if (val != '')
	{
		parent.RightFrame.location.href = "<%=strServletPath%>/GmPageControllerServlet?strPgToLoad="+val;
	}
	//Condition added to avoid js error for lower access level users.
	if(aclevel>3){
		document.FrmMenu.Cbo_Opt.value = '<%=strDeptId%>';
	}
}

</script>

</head>

<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" bgcolor="white" onload="javascript:fnLoad();">
<script language="JavaScript" src="<%=strJsPath%>/tree.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/<%=strMenu%>.js"></script>
<script language="JavaScript" src="<%=strJsPath%>/tree_tpl.js"></script>
<form name="FrmMenu">
<input type="hidden" name="hDeptId="" value="<%=strDeptId%>">
<input type="hidden" name="hSessPg" value="<%=strPgToLoad%>">

<TABLE cellSpacing=0 cellPadding=0 border=0 width="100%"  height = "100%" valign="top">
 <thead>
  <tr style="position:relative; top:expression(this.offsetParent.scrollTop);" height = "10">
	<TD class="LeftTableHeader">
 	 <img id="imgHideToc" style="cursor:hand" onclick="hidetoc();"
	 onmouseover="mouseover('hidetoc');" onmouseout="mouseout('hidetoc');"
	 src="<%=strImagePath%>/hidetoc1.gif" title="Hide Portal Navigator" align="absmiddle"/>
	</TD>
	<Td width="90%" height = "10" class="LeftTableHeader">Portal Navigator</td>
  </TR>
  <tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td colspan = "2" class="Line" height="1"></td></tr>
<%
	
	if (intAccessLvl > 3 )
	{
%>
	<tr style="position:relative; top:expression(this.offsetParent.scrollTop);">
  	<td class="LeftTableHeader">&nbsp;</td>
  	<td class="LeftTableHeader" width="90%" height="10" class="RightText">Reports:&nbsp;<select class="RightText" name="Cbo_Opt" onChange="javascript:fnReload(this)">
  		<option value="<%=strSessDeptSeq%>">Default  
<%
		if (intAccessLvl > 4 && strSessDeptId.equals("O"))
		{
%>
			<option value="2001">Cust Service
			<option value="2014">Logistics
			<option value="2005-R">Sales
			<option value="2004-R">Prod Dev
<%
		}
		else if (intAccessLvl < 5 && strSessDeptId.equals("S"))
		{
			// Creating empty loop for VPs....might be used later....James 6/26/2009
%>
		
<%			
		}
		else if (intAccessLvl > 3 && !strSessDeptId.equals("Z"))
  		//else if ((intAccessLvl > 3 && !strSessDeptId.equals("Z")) || (intAccessLvl > 4 && !strSessDeptId.equals("O")))
		{
%>  
   			<option value="2001-R">Cust Service
			<option value="2014-R">Logistics
			<option value="2003-R">Operations
			<option value="2004-R">Prod Dev
			<option value="2005-R">Sales
			<option value="2012-R">Shipping
<%
		}
		else if (strSessDeptId.equals("Z"))
		{
%>  
			<option id="GmAcctDashBoardServlet" value="2000">Accounting
			<option id="/common/Gm_Blank.jsp" value="2020">A/R
			<option id="/common/Gm_Blank.jsp" value="2021">A/P
			<option id="/common/Gm_Blank.jsp" value="2023">A/c Reporting
			<option id="/common/Gm_Blank.jsp" value="2019">BioQC
			<option id="/gmClinicalStudyAction.do?" value="2007">Clinical Affairs
			<option id="/common/Gm_Blank.jsp" value="2018">Corp Strategy
			<option id="/common/Gm_Blank.jsp" value="2022">Costing
			<option id="GmCustDashBoardServlet" value="2001">Cust Service
			<option id="/common/Gm_Blank.jsp" value="2015">Executive
			<option id="gmETUserDashboard.do" value="2017">Human Resources
			<option id="/common/Gm_Blank.jsp" value="2024">Inventory
			<option id="/common/Gm_Blank.jsp" value="2016">IT
			<option id="/common/Gm_Blank.jsp" value="2011">Legal
			<option id="/common/Gm_Blank.jsp" value="2014">Logistics
			<option id="/common/Gm_Blank.jsp" value="2002">OUS Field Sales
			<option id="/common/Gm_Blank.jsp" value="2003">Operations
			<option id="/common/Gm_Blank.jsp" value="2008">Manufacturing
			<option id="/common/Gm_Blank.jsp" value="2004">Prod Dev
			<option id="/common/Gm_Blank.jsp" value="2010">Purchasing
			<option id="/common/Gm_Blank.jsp" value="2009">Quality
			<option id="/common/Gm_Blank.jsp" value="2013">Receiving
			<option id="GmSaleDashBoardServlet" value="2005">Sales
			<option id="GmShippingReportServlet" value="2012">Shipping
			<option id="GmSaleDashBoardServlet" value="2026">Pricing
<%
		}
		else if ((intAccessLvl == 2 && strSessDeptId.equals("O")) || (intAccessLvl == 3 && strSessDeptId.equals("O")))
		{
%>
			<option id="/common/Gm_Blank.jsp" value="2014">Logistics
<%
		}

		if (intAccessLvl > 3 && strSessDeptId.equals("X"))
		{
%>			
			<option value="2008">Manufacturing
<%
		}
	}
%>

  		</select>
  	</td>
  </tr>  
  <tr style="position:relative; top:expression(this.offsetParent.scrollTop);"><td colspan = "2" class="Line" height="1"></td></tr>
  </thead>

  <TR height = "100%">
	<TD valign="top" colspan = "2">
		<table cellpadding="5" cellspacing="0" cellpadding="10" border="0" width="100%">
		<tr>
			<td>
			<script language="JavaScript">
			<!--//
				new tree (TREE_ITEMS, TREE_TPL);
			//-->
			</script>
			</td>
			
		</tr>
		</table>
	</TD>
  </TR>
</TABLE>
</form>
</body>
</html>