package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCancelForm;

public class GmDownloadInvoiceForm extends GmCancelForm{

	private String vendorId = "";
	private String poNumber ="";
	private String invoiceNumber ="";
	private String invoiceDateFrom ="";
	private String invoiceDateTo="";
	private String hinputStr="";
	private String hinvoiceAmtTotal="";
	
	private String hlocation="";
	
	private ArrayList alVendorList = new ArrayList();
	private List returnList = new ArrayList(); 

	private HashMap hmInvoiceDownload = new HashMap();
	
	public String getVendorId() {
		return vendorId;
	}


	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}


	public ArrayList getAlVendorList() {
		return alVendorList;
	}


	public void setAlVendorList(ArrayList alVendorList) {
		this.alVendorList = alVendorList;
	}


	public List getReturnList() {
		return returnList;
	}


	public void setReturnList(List returnList) {
		this.returnList = returnList;
	}


	public String getPoNumber() {
		return poNumber;
	}


	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}


	public String getInvoiceDateFrom() {
		return invoiceDateFrom;
	}


	public void setInvoiceDateFrom(String invoiceDateFrom) {
		this.invoiceDateFrom = invoiceDateFrom;
	}


	public String getInvoiceDateTo() {
		return invoiceDateTo;
	}


	public void setInvoiceDateTo(String invoiceDateTo) {
		this.invoiceDateTo = invoiceDateTo;
	}


	public String getInvoiceNumber() {
		return invoiceNumber;
	}


	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}


	public String getHinputStr() {
		return hinputStr;
	}


	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}


	public String getHinvoiceAmtTotal() {
		return hinvoiceAmtTotal;
	}


	public void setHinvoiceAmtTotal(String hinvoiceAmtTotal) {
		this.hinvoiceAmtTotal = hinvoiceAmtTotal;
	}


	public HashMap getHmInvoiceDownload() {
		return hmInvoiceDownload;
	}


	public void setHmInvoiceDownload(HashMap hmInvoiceDownload) {
		this.hmInvoiceDownload = hmInvoiceDownload;
	}


	public String getHlocation() {
		return hlocation;
	}


	public void setHlocation(String hlocation) {
		this.hlocation = hlocation;
	} 
}
