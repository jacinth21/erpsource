/**
 * Description : This form class is used for populating AP Invoice Details form
 * Copyright : Globus Medical Inc
 */
package com.globus.accounts.forms;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCancelForm;
import com.globus.accounts.forms.GmDHRPaymentContainerForm;
/**
 * @author bvidyasankar
 *
 */
public class GmAPSaveInvoiceDtlsForm extends GmDHRPaymentContainerForm{
	private String invoiceId = "";
	private String invoiceDt = "";
	private String paymentAmt = "";
	private String invoiceComments = "";
	private String invoiceIntComments = "";
	private String ppvInstruments = "";
	private String ppvImplants = "";
	
	private String hvendorId = "";
	private String hdhrId = "";
	private String haddlChrgId = "";
	private String addlChrgType = "";
	
	private String showOpenDHRFlag = "";
	
	private ArrayList alAddlChrgType = new ArrayList();
	
	private HashMap invoiceDtls = new HashMap();
	private List ldtDHR = new ArrayList();
	private List ldtAddlChrg =  new ArrayList();
	
	private String hpaymentStatus = "";
	
	private String strDHRListFlag = "";
	
	private String strEnableSaveInvoice = "false";
	private String strEnableVoidInvoice = "false";
	
	private int hrowCnt = 0;
	private int htotal = 0;
	/**
	 * @return the hrowCnt
	 */
	public int getHrowCnt() {
		return hrowCnt;
	}
	/**
	 * @param hrowCnt the hrowCnt to set
	 */
	public void setHrowCnt(int hrowCnt) {
		this.hrowCnt = hrowCnt;
	}
	/**
	 * @return the htotal
	 */
	public int getHtotal() {
		return htotal;
	}
	/**
	 * @param htotal the htotal to set
	 */
	public void setHtotal(int htotal) {
		this.htotal = htotal;
	}
	/**
	 * @return the invoiceId
	 */
	public String getInvoiceId() {
		return invoiceId;
	}
	/**
	 * @param invoiceId the invoiceId to set
	 */
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	/**
	 * @return the invoiceDt
	 */
	public String getInvoiceDt() {
		return invoiceDt;
	}
	/**
	 * @param invoiceDt the invoiceDt to set
	 */
	public void setInvoiceDt(String invoiceDt) {
		this.invoiceDt = invoiceDt;
	}
	/**
	 * @return the paymentAmt
	 */
	public String getPaymentAmt() {
		return paymentAmt;
	}
	/**
	 * @param paymentAmt the paymentAmt to set
	 */
	public void setPaymentAmt(String paymentAmt) {
		this.paymentAmt = paymentAmt;
	}
	/**
	 * @return the invoiceComments
	 */
	public String getInvoiceComments() {
		return invoiceComments;
	}
	/**
	 * @param invoiceComments the invoiceComments to set
	 */
	public void setInvoiceComments(String invoiceComments) {
		this.invoiceComments = invoiceComments;
	}
	
	/**
	 * @return the invoiceIntComments
	 */
	public String getInvoiceIntComments() {
		return invoiceIntComments;
	}
	/**
	 * @param invoiceIntComments the invoiceIntComments to set
	 */
	public void setInvoiceIntComments(String invoiceIntComments) {
		this.invoiceIntComments = invoiceIntComments;
	}
	/**
	 * @return the hvendorId
	 */
	public String getHvendorId() {
		return hvendorId;
	}
	/**
	 * @param hvendorId the hvendorId to set
	 */
	public void setHvendorId(String hvendorId) {
		this.hvendorId = hvendorId;
	}
	/**
	 * @return the hdhrId
	 */
	public String getHdhrId() {
		return hdhrId;
	}
	/**
	 * @param hdhrId the hdhrId to set
	 */
	public void setHdhrId(String hdhrId) {
		this.hdhrId = hdhrId;
	}
	/**
	 * @return the haddlChrgId
	 */
	public String getHaddlChrgId() {
		return haddlChrgId;
	}
	/**
	 * @param haddlChrgId the haddlChrgId to set
	 */
	public void setHaddlChrgId(String haddlChrgId) {
		this.haddlChrgId = haddlChrgId;
	}
	/**
	 * @return the showOpenDHRFlag
	 */
	public String getShowOpenDHRFlag() {
		return showOpenDHRFlag;
	}
	/**
	 * @param showOpenDHRFlag the showOpenDHRFlag to set
	 */
	public void setShowOpenDHRFlag(String showOpenDHRFlag) {
		this.showOpenDHRFlag = showOpenDHRFlag;
	}
	/**
	 * @return the alAddlChrgType
	 */
	public ArrayList getAlAddlChrgType() {
		return alAddlChrgType;
	}
	/**
	 * @param alAddlChrgType the alAddlChrgType to set
	 */
	public void setAlAddlChrgType(ArrayList alAddlChrgType) {
		this.alAddlChrgType = alAddlChrgType;
	}
	/**
	 * @return the invoiceDtls
	 */
	public HashMap getInvoiceDtls() {
		return invoiceDtls;
	}
	/**
	 * @param invoiceDtls the invoiceDtls to set
	 */
	public void setInvoiceDtls(HashMap invoiceDtls) {
		this.invoiceDtls = invoiceDtls;
	}
	/**
	 * @return the ldtDHR
	 */
	public List getLdtDHR() {
		return ldtDHR;
	}
	/**
	 * @param ldtDHR the ldtDHR to set
	 */
	public void setLdtDHR(List ldtDHR) {
		this.ldtDHR = ldtDHR;
	}
	/**
	 * @return the ldtAddlChrg
	 */
	public List getLdtAddlChrg() {
		return ldtAddlChrg;
	}
	/**
	 * @param ldtAddlChrg the ldtAddlChrg to set
	 */
	public void setLdtAddlChrg(List ldtAddlChrg) {
		this.ldtAddlChrg = ldtAddlChrg;
	}
	/**
	 * @return the addlChrgType
	 */
	public String getAddlChrgType() {
		return addlChrgType;
	}
	/**
	 * @param addlChrgType the addlChrgType to set
	 */
	public void setAddlChrgType(String addlChrgType) {
		this.addlChrgType = addlChrgType;
	}

	/**
	 * @return the hpaymentStatus
	 */
	public String getHpaymentStatus() {
		return hpaymentStatus;
	}
	/**
	 * @param hpaymentStatus the hpaymentStatus to set
	 */
	public void setHpaymentStatus(String hpaymentStatus) {
		this.hpaymentStatus = hpaymentStatus;
	}
	/**
	 * @return the strEnableSaveInvoice
	 */
	public String getStrEnableSaveInvoice() {
		return strEnableSaveInvoice;
	}
	/**
	 * @param strEnableSaveInvoice the strEnableSaveInvoice to set
	 */
	public void setStrEnableSaveInvoice(String strEnableSaveInvoice) {
		this.strEnableSaveInvoice = strEnableSaveInvoice;
	}
	/**
	 * @return the strEnableVoidInvoice
	 */
	public String getStrEnableVoidInvoice() {
		return strEnableVoidInvoice;
	}
	/**
	 * @param strEnableVoidInvoice the strEnableVoidInvoice to set
	 */
	public void setStrEnableVoidInvoice(String strEnableVoidInvoice) {
		this.strEnableVoidInvoice = strEnableVoidInvoice;
	}
	/**
	 * @return the strDHRListFlag
	 */
	public String getStrDHRListFlag() {
		return strDHRListFlag;
	}
	/**
	 * @param strDHRListFlag the strDHRListFlag to set
	 */
	public void setStrDHRListFlag(String strDHRListFlag) {
		this.strDHRListFlag = strDHRListFlag;
	}
	/**
	 * @return the ppvInstruments
	 */
	public String getPpvInstruments() {
		return ppvInstruments;
	}
	/**
	 * @param ppvInstruments the ppvInstruments to set
	 */
	public void setPpvInstruments(String ppvInstruments) {
		this.ppvInstruments = ppvInstruments;
	}
	/**
	 * @return the ppvImplants
	 */
	public String getPpvImplants() {
		return ppvImplants;
	}
	/**
	 * @param ppvImplants the ppvImplants to set
	 */
	public void setPpvImplants(String ppvImplants) {
		this.ppvImplants = ppvImplants;
	}
	
	
}
