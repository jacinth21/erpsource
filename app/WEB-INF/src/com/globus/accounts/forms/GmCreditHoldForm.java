package com.globus.accounts.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmCreditHoldForm extends GmLogForm {
  private String creditLimit = "";
  private String accountId = "";
  private String accountName = "";
  private String subType = "";
  private String txnId = "";
  private String creditHoldType = "";
  private String emailType = "";
  private String arSummary = "";
  private String creditAttrString = "";
  private String creditType = "";
  private String inputCreditTypeStr = "";
  private String accessCreditType = "";
  private String submitAccFl = "";
  private ArrayList alCreditHoldList = new ArrayList();
  private ArrayList alAccountsList = new ArrayList();
  private ArrayList alARSummaryList = new ArrayList();
  private ArrayList alSubTypeCtrl = new ArrayList();
  private ArrayList alSubTypeValue = new ArrayList();


  /**
   * @return the emailType
   */
  public String getEmailType() {
    return emailType;
  }

  /**
   * @param emailType the emailType to set
   */
  public void setEmailType(String emailType) {
    this.emailType = emailType;
  }

  /**
   * @return the submitAccFl
   */
  public String getSubmitAccFl() {
    return submitAccFl;
  }

  /**
   * @param submitAccFl the submitAccFl to set
   */
  public void setSubmitAccFl(String submitAccFl) {
    this.submitAccFl = submitAccFl;
  }

  /**
   * @return the creditLimit
   */
  public String getCreditLimit() {
    return creditLimit;
  }

  /**
   * @param creditLimit the creditLimit to set
   */
  public void setCreditLimit(String creditLimit) {
    this.creditLimit = creditLimit;
  }

  /**
   * @return the accountId
   */
  public String getAccountId() {
    return accountId;
  }

  /**
   * @param accountId the accountId to set
   */
  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  /**
   * @return the accountName
   */
  public String getAccountName() {
    return accountName;
  }

  /**
   * @param accountName the accountName to set
   */
  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  /**
   * @return the subType
   */
  public String getSubType() {
    return subType;
  }

  /**
   * @param subType the subType to set
   */
  public void setSubType(String subType) {
    this.subType = subType;
  }

  /**
   * @return the txnId
   */
  public String getTxnId() {
    return txnId;
  }

  /**
   * @param txnId the txnId to set
   */
  public void setTxnId(String txnId) {
    this.txnId = txnId;
  }



  /**
   * @return the creditHoldType
   */
  public String getCreditHoldType() {
    return creditHoldType;
  }

  /**
   * @param creditHoldType the creditHoldType to set
   */
  public void setCreditHoldType(String creditHoldType) {
    this.creditHoldType = creditHoldType;
  }

  /**
   * @return the arSummary
   */
  public String getArSummary() {
    return arSummary;
  }

  /**
   * @param arSummary the arSummary to set
   */
  public void setArSummary(String arSummary) {
    this.arSummary = arSummary;
  }

  /**
   * @return the creditAttrString
   */
  public String getCreditAttrString() {
    return creditAttrString;
  }

  /**
   * @param creditAttrString the creditAttrString to set
   */
  public void setCreditAttrString(String creditAttrString) {
    this.creditAttrString = creditAttrString;
  }


  /**
   * @return the creditType
   */
  public String getCreditType() {
    return creditType;
  }

  /**
   * @param creditType the creditType to set
   */
  public void setCreditType(String creditType) {
    this.creditType = creditType;
  }

  /**
   * @return the inputCreditTypeStr
   */
  public String getInputCreditTypeStr() {
    return inputCreditTypeStr;
  }

  /**
   * @param inputCreditTypeStr the inputCreditTypeStr to set
   */
  public void setInputCreditTypeStr(String inputCreditTypeStr) {
    this.inputCreditTypeStr = inputCreditTypeStr;
  }

  /**
   * @return the accessCreditType
   */
  public String getAccessCreditType() {
    return accessCreditType;
  }

  /**
   * @param accessCreditType the accessCreditType to set
   */
  public void setAccessCreditType(String accessCreditType) {
    this.accessCreditType = accessCreditType;
  }

  /**
   * @return the alCreditHoldList
   */
  public ArrayList getAlCreditHoldList() {
    return alCreditHoldList;
  }

  /**
   * @param alCreditHoldList the alCreditHoldList to set
   */
  public void setAlCreditHoldList(ArrayList alCreditHoldList) {
    this.alCreditHoldList = alCreditHoldList;
  }

  /**
   * @return the alAccountsList
   */
  public ArrayList getAlAccountsList() {
    return alAccountsList;
  }

  /**
   * @param alAccountsList the alAccountsList to set
   */
  public void setAlAccountsList(ArrayList alAccountsList) {
    this.alAccountsList = alAccountsList;
  }

  /**
   * @return the alARSummaryList
   */
  public ArrayList getAlARSummaryList() {
    return alARSummaryList;
  }

  /**
   * @param alARSummaryList the alARSummaryList to set
   */
  public void setAlARSummaryList(ArrayList alARSummaryList) {
    this.alARSummaryList = alARSummaryList;
  }

  /**
   * @return the alSubTypeCtrl
   */
  public ArrayList getAlSubTypeCtrl() {
    return alSubTypeCtrl;
  }

  /**
   * @param alSubTypeCtrl the alSubTypeCtrl to set
   */
  public void setAlSubTypeCtrl(ArrayList alSubTypeCtrl) {
    this.alSubTypeCtrl = alSubTypeCtrl;
  }

  /**
   * @return the alSubTypeValue
   */
  public ArrayList getAlSubTypeValue() {
    return alSubTypeValue;
  }

  /**
   * @param alSubTypeValue the alSubTypeValue to set
   */
  public void setAlSubTypeValue(ArrayList alSubTypeValue) {
    this.alSubTypeValue = alSubTypeValue;
  }

}
