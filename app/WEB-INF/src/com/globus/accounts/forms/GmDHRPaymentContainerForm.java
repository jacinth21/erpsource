/**
 * Description : This form class is used for populating DHR Payment information
 * Copyright : Globus Medical Inc
 */
package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

/**
 * @author bvidyasankar
 *
 */
public class GmDHRPaymentContainerForm extends GmLogForm{
	private String containerPONumber = "";
	private HashMap hmpurchaseOrdDtls = new HashMap();//Stores PO Details like vendor name, currency etc
	private String hpaymentId = "";
	private String hpoNumber = "";
	private List ldtResult = new ArrayList(); //Stores report output table of invoice list for a PO.
	private String strEnableNewInvoice = "false";
	/**
	 * @return the strEnableNewInvoice
	 */
	public String getStrEnableNewInvoice() {
		return strEnableNewInvoice;
	}
	/**
	 * @param strEnableNewInvoice the strEnableNewInvoice to set
	 */
	public void setStrEnableNewInvoice(String strEnableNewInvoice) {
		this.strEnableNewInvoice = strEnableNewInvoice;
	}
	/**
	 * @return the hpaymentId
	 */
	public String getHpaymentId() {
		return hpaymentId;
	}
	/**
	 * @param hpaymentId the hpaymentId to set
	 */
	public void setHpaymentId(String hpaymentId) {
		this.hpaymentId = hpaymentId;
	}
	/**
	 * @return the hpoNumber
	 */
	public String getHpoNumber() {
		return hpoNumber;
	}
	/**
	 * @param hpoNumber the hpoNumber to set
	 */
	public void setHpoNumber(String hpoNumber) {
		this.hpoNumber = hpoNumber;
	}
	/**
	 * @return the ldtResult
	 */
	public List getLdtResult() {
		return ldtResult;
	}
	/**
	 * @param ldtResult the ldtResult to set
	 */
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}
	/**
	 * @return the containerPONumber
	 */
	public String getContainerPONumber() {
		return containerPONumber;
	}
	/**
	 * @param containerPONumber the containerPONumber to set
	 */
	public void setContainerPONumber(String containerPONumber) {
		this.containerPONumber = containerPONumber;
	}
	/**
	 * @return the hmpurchaseOrdDtls
	 */
	public HashMap getHmpurchaseOrdDtls() {
		return hmpurchaseOrdDtls;
	}
	/**
	 * @param hmpurchaseOrdDtls the hmpurchaseOrdDtls to set
	 */
	public void setHmpurchaseOrdDtls(HashMap hmpurchaseOrdDtls) {
		this.hmpurchaseOrdDtls = hmpurchaseOrdDtls;
	}
}
