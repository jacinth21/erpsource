package com.globus.accounts.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmCashApplicationForm extends GmCommonForm {

  private String inputString = "";
  private String paymentMode = "";
  private String gridData = "";
  private ArrayList alPaymentMode = new ArrayList();
  private String paymentDt = "";
  private String paymentAmt = "";
  private String strInvoiceID = "";
  private String paidInvoiceIDs = "";
  private String strCompCurrency = "";
  private String strCompCurrName = "";
  private String strARRptByDealerFlag = "";

  private ArrayList alCompCurrency = new ArrayList();
  
  /**
   * @return the strARRptByDealerFlag
   */
  public String getStrARRptByDealerFlag() {
    return strARRptByDealerFlag;
  }

  /**
   * @param strARRptByDealerFlag the strARRptByDealerFlag to set
   */
  public void setStrARRptByDealerFlag(String strARRptByDealerFlag) {
    this.strARRptByDealerFlag = strARRptByDealerFlag;
  }


  /**
   * @return the strInvoiceID
   */
  public String getStrInvoiceID() {
    return strInvoiceID;
  }

  /**
   * @param strInvoiceID the strInvoiceID to set
   */

  public void setStrInvoiceID(String strInvoiceID) {
    this.strInvoiceID = strInvoiceID;
  }

  public String getInputString() {
    return inputString;
  }

  public void setInputString(String inputString) {
    this.inputString = inputString;
  }

  public String getPaymentMode() {
    return paymentMode;
  }

  public void setPaymentMode(String paymentMode) {
    this.paymentMode = paymentMode;
  }

  public String getGridData() {
    return gridData;
  }

  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  public ArrayList getAlPaymentMode() {
    return alPaymentMode;
  }

  public void setAlPaymentMode(ArrayList alPaymentMode) {
    this.alPaymentMode = alPaymentMode;
  }

  public String getPaymentDt() {
    return paymentDt;
  }

  public void setPaymentDt(String paymentDt) {
    this.paymentDt = paymentDt;
  }

  public String getPaymentAmt() {
    return paymentAmt;
  }

  public void setPaymentAmt(String paymentAmt) {
    this.paymentAmt = paymentAmt;
  }

  /**
   * @return the paidInvoiceIDs
   */
  public String getPaidInvoiceIDs() {
    return paidInvoiceIDs;
  }

  /**
   * @param paidInvoiceIDs the paidInvoiceIDs to set
   */
  public void setPaidInvoiceIDs(String paidInvoiceIDs) {
    this.paidInvoiceIDs = paidInvoiceIDs;
  }

  /**
   * @return the strCompCurrency
   */
  public String getStrCompCurrency() {
    return strCompCurrency;
  }

  /**
   * @param strCompCurrency the strCompCurrency to set
   */
  public void setStrCompCurrency(String strCompCurrency) {
    this.strCompCurrency = strCompCurrency;
  }

  /**
   * @return the alCompCurrency
   */
  public ArrayList getAlCompCurrency() {
    return alCompCurrency;
  }

  /**
   * @param alCompCurrency the alCompCurrency to set
   */
  public void setAlCompCurrency(ArrayList alCompCurrency) {
    this.alCompCurrency = alCompCurrency;
  }

  /**
   * @return the strCompCurrName
   */
  public String getStrCompCurrName() {
    return strCompCurrName;
  }

  /**
   * @param strCompCurrName the strCompCurrName to set
   */
  public void setStrCompCurrName(String strCompCurrName) {
    this.strCompCurrName = strCompCurrName;
  }



}
