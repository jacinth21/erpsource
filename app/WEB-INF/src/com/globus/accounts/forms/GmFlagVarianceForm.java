package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCancelForm;

public class GmFlagVarianceForm extends GmCancelForm {

	
	private String auditId="";
	private String distId="";
	private String setId="";
	private String strVarianceType="";
	private String strInput="";
	private String strOpt = "";
	private String deviationQty="";
	private String unResolvedQty="";
	
	private List alOverall = new ArrayList();
	private List alAuditReport= new ArrayList();
	private List alSystemReport= new ArrayList();
	private List alPAssociatedReport= new ArrayList();
	private List alNAssociatedReport= new ArrayList();
	private List alReturnsReport= new ArrayList();
	
	private ArrayList alPCodeID=new ArrayList();
	private ArrayList alNCodeID=new ArrayList();
	private ArrayList alDistList= new ArrayList();
	private ArrayList alchange=new ArrayList();
	private ArrayList alPostingOptions=new ArrayList();
	
	HashMap hmHeaderInfo= new HashMap();
	
	
	public String getAuditId() {
		return auditId;
	}
	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}
	public String getDistId() {
		return distId;
	}
	public void setDistId(String distId) {
		this.distId = distId;
	}
	public String getSetId() {
		return setId;
	}
	public void setSetId(String setId) {
		this.setId = setId;
	}
	public List getAlOverall() {
		return alOverall;
	}
	public void setAlOverall(List alOverall) {
		this.alOverall = alOverall;
	}
	public List getAlAuditReport() {
		return alAuditReport;
	}
	public void setAlAuditReport(List alAuditReport) {
		this.alAuditReport = alAuditReport;
	}
	public List getAlSystemReport() {
		return alSystemReport;
	}
	public void setAlSystemReport(List alSystemReport) {
		this.alSystemReport = alSystemReport;
	}
	public String getStrVarianceType() {
		return strVarianceType;
	}
	public void setStrVarianceType(String strVarianceType) {
		this.strVarianceType = strVarianceType;
	}
	public HashMap getHmHeaderInfo() {
		return hmHeaderInfo;
	}
	public void setHmHeaderInfo(HashMap hmHeaderInfo) {
		this.hmHeaderInfo = hmHeaderInfo;
	}
	public ArrayList getAlPCodeID() {
		return alPCodeID;
	}
	public void setAlPCodeID(ArrayList alPCodeID) {
		this.alPCodeID = alPCodeID;
	}
	public ArrayList getAlNCodeID() {
		return alNCodeID;
	}
	public void setAlNCodeID(ArrayList alNCodeID) {
		this.alNCodeID = alNCodeID;
	}
	public ArrayList getAlDistList() {
		return alDistList;
	}
	public void setAlDistList(ArrayList alDistList) {
		this.alDistList = alDistList;
	}
	public ArrayList getAlchange() {
		return alchange;
	}
	public void setAlchange(ArrayList alchange) {
		this.alchange = alchange;
	}
	public String getStrInput() {
		return strInput;
	}
	public void setStrInput(String strInput) {
		this.strInput = strInput;
	}
	public String getStrOpt() {
		return strOpt;
	}
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}
	public List getAlPAssociatedReport() {
		return alPAssociatedReport;
	}
	public void setAlPAssociatedReport(List alPAssociatedReport) {
		this.alPAssociatedReport = alPAssociatedReport;
	}
	public List getAlNAssociatedReport() {
		return alNAssociatedReport;
	}
	public void setAlNAssociatedReport(List alNAssociatedReport) {
		this.alNAssociatedReport = alNAssociatedReport;
	}
	public List getAlReturnsReport() {
		return alReturnsReport;
	}
	public void setAlReturnsReport(List alReturnsReport) {
		this.alReturnsReport = alReturnsReport;
	}
	/**
	 * @return the deviationQty
	 */
	public String getDeviationQty() {
		return deviationQty;
	}
	/**
	 * @param deviationQty the deviationQty to set
	 */
	public void setDeviationQty(String deviationQty) {
		this.deviationQty = deviationQty;
	}
	/**
	 * @return the unResolvedQty
	 */
	public String getUnResolvedQty() {
		return unResolvedQty;
	}
	/**
	 * @param unResolvedQty the unResolvedQty to set
	 */
	public void setUnResolvedQty(String unResolvedQty) {
		this.unResolvedQty = unResolvedQty;
	}
	/**
	 * @return the alPostingOptions
	 */
	public ArrayList getAlPostingOptions() {
		return alPostingOptions;
	}
	/**
	 * @param alPostingOptions the alPostingOptions to set
	 */
	public void setAlPostingOptions(ArrayList alPostingOptions) {
		this.alPostingOptions = alPostingOptions;
	}


}
