package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmAuditVerifyForm extends GmLogForm {

	private HashMap hmAuditEntryDetails = new HashMap();
	
	private String tagID ="";
	private String partNumber ="";
	private String controlNumber="";
	private String comments ="";
	private String countedDate = "";
	private String auditEntryID = "";
	private String borrowedFrom="";
	private String distID = "";
	private String setID = "";
	private String statusID ="";
	private String borrFrom ="";
	private String locationID="";
	private String locationDet ="";
	private String names = "";
	private String loctyp ="";
	private ArrayList alSet = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alBorrfrm = new ArrayList();
	private ArrayList alLocation = new ArrayList();
	private ArrayList alLocDetails = new ArrayList();
	
	
	/**
	 * @return the alSet
	 */
	public ArrayList getAlSet() {
		return alSet;
	}

	/**
	 * @param alSet the alSet to set
	 */
	public void setAlSet(ArrayList alSet) {
		this.alSet = alSet;
	}

	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}

	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}

	/**
	 * @return the alBorrfrm
	 */
	public ArrayList getAlBorrfrm() {
		return alBorrfrm;
	}

	/**
	 * @param alBorrfrm the alBorrfrm to set
	 */
	public void setAlBorrfrm(ArrayList alBorrfrm) {
		this.alBorrfrm = alBorrfrm;
	}

	/**
	 * @return the alLocation
	 */
	public ArrayList getAlLocation() {
		return alLocation;
	}

	/**
	 * @param alLocation the alLocation to set
	 */
	public void setAlLocation(ArrayList alLocation) {
		this.alLocation = alLocation;
	}

	/**
	 * @return the alLocDetails
	 */
	public ArrayList getAlLocDetails() {
		return alLocDetails;
	}

	/**
	 * @param alLocDetails the alLocDetails to set
	 */
	public void setAlLocDetails(ArrayList alLocDetails) {
		this.alLocDetails = alLocDetails;
	}

	/**
	 * @return the setID
	 */
	public String getSetID() {
		return setID;
	}

	/**
	 * @param setID the setID to set
	 */
	public void setSetID(String hsetID) {
		this.setID = hsetID;
	}

	/**
	 * @return the statusID
	 */
	public String getStatusID() {
		return statusID;
	}

	/**
	 * @param statusID the statusID to set
	 */
	public void setStatusID(String hstatusID) {
		this.statusID = hstatusID;
	}

	/**
	 * @return the borrFrom
	 */
	public String getBorrFrom() {
		return borrFrom;
	}

	/**
	 * @param borrFrom the borrFrom to set
	 */
	public void setBorrFrom(String hborrFrom) {
		this.borrFrom = hborrFrom;
	}

	/**
	 * @return the locationID
	 */
	public String getLocationID() {
		return locationID;
	}

	/**
	 * @param locationID the locationID to set
	 */
	public void setLocationID(String hlocationID) {
		this.locationID = hlocationID;
	}

	/**
	 * @return the names
	 */
	public String getNames() {
		return names;
	}

	/**
	 * @param locationDet the locationDet to set
	 */
	public void setNames(String names) {
		this.names = names;
	}

	/**
	 * @return the auditEntryID
	 */
	public String getAuditEntryID() {
		return auditEntryID;
	}

	/**
	 * @param auditEntryID the auditEntryID to set
	 */
	public void setAuditEntryID(String auditEntryID) {
		this.auditEntryID = auditEntryID;
	}

	/**
	 * @return the hmAuditEntryDetails
	 */
	public HashMap getHmAuditEntryDetails() {
		return hmAuditEntryDetails;
	}

	/**
	 * @param hmAuditEntryDetails the hmAuditEntryDetails to set
	 */
	public void setHmAuditEntryDetails(HashMap hmAuditEntryDetails) {
		this.hmAuditEntryDetails = hmAuditEntryDetails;
	}

	/**
	 * @return the tagID
	 */
	public String getTagID() {
		return tagID;
	}

	/**
	 * @param tagID the tagID to set
	 */
	public void setTagID(String tagID) {
		this.tagID = tagID;
	}

	/**
	 * @return the partNumber
	 */
	public String getPartNumber() {
		return partNumber;
	}

	/**
	 * @param partNumber the partNumber to set
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	/**
	 * @return the controlNumber
	 */
	public String getControlNumber() {
		return controlNumber;
	}

	/**
	 * @param controlNumber the controlNumber to set
	 */
	public void setControlNumber(String controlNumber) {
		this.controlNumber = controlNumber;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the countedDate
	 */
	public String getCountedDate() {
		return countedDate;
	}

	/**
	 * @param countedDate the countedDate to set
	 */
	public void setCountedDate(String countedDate) {
		this.countedDate = countedDate;
	}

	/**
	 * @return the borrowedFrom
	 */
	public String getBorrowedFrom() {
		return borrowedFrom;
	}

	/**
	 * @param borrowedFrom the borrowedFrom to set
	 */
	public void setBorrowedFrom(String borrowedFrom) {
		this.borrowedFrom = borrowedFrom;
	}

	/**
	 * @return the loctyp
	 */
	public String getLoctyp() {
		return loctyp;
	}

	/**
	 * @param loctyp the loctyp to set
	 */
	public void setLoctyp(String loctyp) {
		this.loctyp = loctyp;
	}

	/**
	 * @return the locationDet
	 */
	public String getLocationDet() {
		return locationDet;
	}

	/**
	 * @param locationDet the locationDet to set
	 */
	public void setLocationDet(String locationDet) {
		this.locationDet = locationDet;
	}

	/**
	 * @return the distID
	 */
	public String getDistID() {
		return distID;
	}

	/**
	 * @param distID the distID to set
	 */
	public void setDistID(String distID) {
		this.distID = distID;
	}




}
