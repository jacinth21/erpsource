
package com.globus.accounts.forms;
import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

/**
 * @author pvigneshwaran
 * GmStandardReport Form:form value for GmVendorStandardRpt,
 * GmVendorStandardUpdate,GmVendorStandardHistory
 */
public class GmStandardReportForm   extends GmCommonForm  {

	private String strEditAccess = "";
	private String vendorId = "";
	private String name = "";
	private String cost = "";
	private String strsuccess="";
	private String  pnum="";
	private String gridHistoryData="";
	private String gridXmlData = ""; 
	private String projectId="";
	private String vendorName="";
	private String partDesc="";
	private String historyFl="";
	private List costResult = null;
	
	private String inputString="";
	private String strMaxUploadData = "";
	
	private ArrayList alVendorList = new ArrayList();
    /**
	 * @return the alVendorList
	 */
	public ArrayList getAlVendorList() {
		return alVendorList;
	}

	/**
	 * @param alVendorList the alVendorList to set
	 */
	public void setAlVendorList(ArrayList alVendorList) {
		this.alVendorList = alVendorList;
	}

	private ArrayList projectList = new ArrayList(); 
    private String strPartNumbers = "";
    private String strVendorId = "";
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}

	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}




	/**
	 * @return the costResult
	 */
	public List getCostResult() {
		return costResult;
	}

	/**
	 * @param costResult the costResult to set
	 */
	public void setCostResult(List costResult) {
		this.costResult = costResult;
	}



	

	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}

	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	/**
	 * @return the gridHistoryData
	 */
	public String getGridHistoryData() {
		return gridHistoryData;
	}




	/**
	 * @return the cost
	 */
	public String getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(String cost) {
		this.cost = cost;
	}

	

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the vendorId
	 */
	public String getVendorId() {
		return vendorId;
	}



	/**
	 * @return the strEditAccess
	 */
	public String getStrEditAccess() {
		return strEditAccess;
	}

	/**
	 * @param strEditAccess the strEditAccess to set
	 */
	public void setStrEditAccess(String strEditAccess) {
		this.strEditAccess = strEditAccess;
	}


	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @param gridHistoryData the gridHistoryData to set
	 */
	public void setGridHistoryData(String gridHistoryData) {
		this.gridHistoryData = gridHistoryData;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectId() {
		return projectId;
	}




	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the projectList
	 */
	public ArrayList getProjectList() {
		return projectList;
	}

	/**
	 * @param projectList the projectList to set
	 */
	public void setProjectList(ArrayList projectList) {
		this.projectList = projectList;
	}

	/**
	 * @return the vendorName
	 */
	public String getVendorName() {
		return vendorName;
	}

	/**
	 * @param vendorName the vendorName to set
	 */
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	/**
	 * @return the historyFl
	 */
	public String getHistoryFl() {
		return historyFl;
	}

	/**
	 * @param historyFl the historyFl to set
	 */
	public void setHistoryFl(String historyFl) {
		this.historyFl = historyFl;
	}

	/**
	 * @return the partDesc
	 */
	public String getPartDesc() {
		return partDesc;
	}

	/**
	 * @param partDesc the partDesc to set
	 */
	public void setPartDesc(String partDesc) {
		this.partDesc = partDesc;
	}

	/**
	 * @return the strsuccess
	 */
	public String getStrsuccess() {
		return strsuccess;
	}

	/**
	 * @return the strPartNumbers
	 */
	public String getStrPartNumbers() {
		return strPartNumbers;
	}

	/**
	 * @param strPartNumbers the strPartNumbers to set
	 */
	public void setStrPartNumbers(String strPartNumbers) {
		this.strPartNumbers = strPartNumbers;
	}

	/**
	 * @return the strVendorId
	 */
	public String getStrVendorId() {
		return strVendorId;
	}

	/**
	 * @param strVendorId the strVendorId to set
	 */
	public void setStrVendorId(String strVendorId) {
		this.strVendorId = strVendorId;
	}

	/**
	 * @param strsuccess the strsuccess to set
	 */
	public void setStrsuccess(String strsuccess) {
		this.strsuccess = strsuccess;
	}

	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}

	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	/**
	 * @return the strMaxUploadData
	 */
	public String getStrMaxUploadData() {
		return strMaxUploadData;
	}

	/**
	 * @param strMaxUploadData the strMaxUploadData to set
	 */
	public void setStrMaxUploadData(String strMaxUploadData) {
		this.strMaxUploadData = strMaxUploadData;
	}

	


	
}
