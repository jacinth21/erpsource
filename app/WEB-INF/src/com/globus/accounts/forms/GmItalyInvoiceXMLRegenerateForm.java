package com.globus.accounts.forms;

import com.globus.common.forms.GmCommonForm;

public class GmItalyInvoiceXMLRegenerateForm extends GmCommonForm{

	private String invoiceIds="";
	private String userid="";
	
	/**
	 * @return the invoiceIds
	 */
	public String getInvoiceIds() {
		return invoiceIds;
	}
	/**
	 * @param invoiceIds the invoiceIds to set
	 */
	public void setInvoiceIds(String invoiceIds) {
		this.invoiceIds = invoiceIds;
	}
	/**
	 * @return the userid
	 */
	public String getUserid() {
		return userid;
	}
	/**
	 * @param userid the userid to set
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	
}
