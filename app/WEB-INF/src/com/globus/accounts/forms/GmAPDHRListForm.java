/**
 * Description : This form class is used for populating AP DHR List report information
 * Copyright : Globus Medical Inc
 */
package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmLogForm;
/**
 * @author bvidyasankar
 *
 */
public class GmAPDHRListForm extends GmLogForm{
	private String hpoNumber = "";
	private List ldtResult = new ArrayList(); //Stores report output table of DHR list for a PO.
	/**
	 * @return the hpoNumber
	 */
	public String getHpoNumber() {
		return hpoNumber;
	}
	/**
	 * @param hpoNumber the hpoNumber to set
	 */
	public void setHpoNumber(String hpoNumber) {
		this.hpoNumber = hpoNumber;
	}
	/**
	 * @return the ldtResult
	 */
	public List getLdtResult() {
		return ldtResult;
	}
	/**
	 * @param ldtResult the ldtResult to set
	 */
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}

}
