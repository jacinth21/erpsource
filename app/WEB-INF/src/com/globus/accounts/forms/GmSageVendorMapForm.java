package com.globus.accounts.forms;
 
import com.globus.common.forms.GmCommonForm;
import java.util.ArrayList;
import java.util.List;

public class GmSageVendorMapForm extends GmCommonForm 
{
   private String vendorName = "";    
   private List returnList = new ArrayList();  
    
   private String hinputStr = "";
   private String missingSageVenIds = "on"; 
   private String sageVendorId = "";
   private String hmissingNotcheck ="";
   
    
 
public String getHmissingNotcheck() {
	return hmissingNotcheck;
}

public void setHmissingNotcheck(String hmissingNotcheck) {
	this.hmissingNotcheck = hmissingNotcheck;
}

public String getSageVendorId() {
	return sageVendorId;
}

public void setSageVendorId(String sageVendorId) {
	this.sageVendorId = sageVendorId;
}

public String getHinputStr() {
	return hinputStr;
}

public void setHinputStr(String hinputStr) {
	this.hinputStr = hinputStr;
}
  

public List getReturnList() {
	return returnList;
}

public void setReturnList(List returnList) {
	this.returnList = returnList;
}

public String getVendorName() {
	return vendorName;
}

public void setVendorName(String vendorName) {
	this.vendorName = vendorName;
}

public String getMissingSageVenIds() {
	return missingSageVenIds;
}

public void setMissingSageVenIds(String missingSageVenIds) {
	this.missingSageVenIds = missingSageVenIds;
}
 
 
 
}
