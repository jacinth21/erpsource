package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCancelForm;

public class GmFieldSalesAuditForm extends GmCancelForm {

	

	private String auditId="";
	private String distId="";
	private String auditorId="";
	private String inputString="";
	private String distName="";
	private String fileId="";
	
	private ArrayList alAuditName = new ArrayList();
	private ArrayList aldist = new ArrayList();
	private ArrayList alAuditor = new ArrayList();
	private List ldtAuditLockReport = new ArrayList();




	public ArrayList getAlAuditName() {
		return alAuditName;
	}

	public void setAlAuditName(ArrayList alAuditName) {
		this.alAuditName = alAuditName;
	}

	public String getDistId() {
		return distId;
	}

	public void setDistId(String distId) {
		this.distId = distId;
	}

	public ArrayList getAldist() {
		return aldist;
	}

	public void setAldist(ArrayList aldist) {
		this.aldist = aldist;
	}

	public String getAuditorId() {
		return auditorId;
	}

	public void setAuditorId(String auditorId) {
		this.auditorId = auditorId;
	}

	public ArrayList getAlAuditor() {
		return alAuditor;
	}

	public void setAlAuditor(ArrayList alAuditor) {
		this.alAuditor = alAuditor;
	}

	public String getInputString() {
		return inputString;
	}

	public void setInputString(String inputString) {
		this.inputString = inputString;
	}

	public String getDistName() {
		return distName;
	}

	public void setDistName(String distName) {
		this.distName = distName;
	}

	public String getAuditId() {
		return auditId;
	}

	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}

	/**
	 * @return the ldtAuditLockReport
	 */
	public List getLdtAuditLockReport() {
		return ldtAuditLockReport;
	}

	/**
	 * @param ldtAuditLockReport the ldtAuditLockReport to set
	 */
	public void setLdtAuditLockReport(List ldtAuditLockReport) {
		this.ldtAuditLockReport = ldtAuditLockReport;
	}

	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}

	/**
	 * @param fileId the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}


}
