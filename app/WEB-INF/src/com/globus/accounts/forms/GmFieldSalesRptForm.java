package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCancelForm;

public class GmFieldSalesRptForm extends GmCancelForm {

	

	private String auditId="";
	private String distId="";
	private String setId="";
	private String territoryId="";
	private String auditorId="";
	private int devQty=0;
	private String devOpr="";
	private String reportType="";
	private boolean dispOnlyDeviation ;
	private boolean dispNonflagged;
	private boolean dispflaggedDet;
	private boolean dispUnresolvedDet;
	private boolean dispNonApproved;
	private boolean dispNonReconciliation;
	
	private List ldtResult = new ArrayList();
	private ArrayList alAuditName = new ArrayList();
	private ArrayList aldist = new ArrayList();
	private ArrayList alAuditor = new ArrayList();
	private ArrayList alSet = new ArrayList();
	private ArrayList alTerritory = new ArrayList();
	private ArrayList alDevOpr = new ArrayList();
	

	public String getAuditorId() {
		return auditorId;
	}

	public void setAuditorId(String auditorId) {
		this.auditorId = auditorId;
	}

	
	public String getAuditId() {
		return auditId;
	}

	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}

	/**
	 * @return the distId
	 */
	public String getDistId() {
		return distId;
	}

	/**
	 * @param distId the distId to set
	 */
	public void setDistId(String distId) {
		this.distId = distId;
	}

	/**
	 * @return the setId
	 */
	public String getSetId() {
		return setId;
	}

	/**
	 * @param setId the setId to set
	 */
	public void setSetId(String setId) {
		this.setId = setId;
	}

	/**
	 * @return the territoryId
	 */
	public String getTerritoryId() {
		return territoryId;
	}

	/**
	 * @param territoryId the territoryId to set
	 */
	public void setTerritoryId(String territoryId) {
		this.territoryId = territoryId;
	}

	/**
	 * @return the devQty
	 */
	public int getDevQty() {
		return devQty;
	}

	/**
	 * @param devQty the devQty to set
	 */
	public void setDevQty(int devQty) {
		this.devQty = devQty;
	}

	/**
	 * @return the devOpr
	 */
	public String getDevOpr() {
		return devOpr;
	}

	/**
	 * @param devOpr the devOpr to set
	 */
	public void setDevOpr(String devOpr) {
		this.devOpr = devOpr;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	/**
	 * @return the dispOnlyDeviation
	 */
	public boolean isDispOnlyDeviation() {
		return dispOnlyDeviation;
	}

	/**
	 * @param dispOnlyDeviation the dispOnlyDeviation to set
	 */
	public void setDispOnlyDeviation(boolean dispOnlyDeviation) {
		this.dispOnlyDeviation = dispOnlyDeviation;
	}

	/**
	 * @return the dispNonflagged
	 */
	public boolean isDispNonflagged() {
		return dispNonflagged;
	}

	/**
	 * @param dispNonflagged the dispNonflagged to set
	 */
	public void setDispNonflagged(boolean dispNonflagged) {
		this.dispNonflagged = dispNonflagged;
	}

	/**
	 * @return the dispflaggedDet
	 */
	public boolean isDispflaggedDet() {
		return dispflaggedDet;
	}

	/**
	 * @param dispflaggedDet the dispflaggedDet to set
	 */
	public void setDispflaggedDet(boolean dispflaggedDet) {
		this.dispflaggedDet = dispflaggedDet;
	}

	/**
	 * @return the ldtResult
	 */
	public List getLdtResult() {
		return ldtResult;
	}

	/**
	 * @param ldtResult the ldtResult to set
	 */
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}

	public ArrayList getAlAuditName() {
		return alAuditName;
	}

	public void setAlAuditName(ArrayList alAuditName) {
		this.alAuditName = alAuditName;
	}

	public ArrayList getAldist() {
		return aldist;
	}

	public void setAldist(ArrayList aldist) {
		this.aldist = aldist;
	}

	public ArrayList getAlAuditor() {
		return alAuditor;
	}

	public void setAlAuditor(ArrayList alAuditor) {
		this.alAuditor = alAuditor;
	}

	public ArrayList getAlSet() {
		return alSet;
	}

	public void setAlSet(ArrayList alSet) {
		this.alSet = alSet;
	}

	public ArrayList getAlTerritory() {
		return alTerritory;
	}

	public void setAlTerritory(ArrayList alTerritory) {
		this.alTerritory = alTerritory;
	}

	public ArrayList getAlDevOpr() {
		return alDevOpr;
	}

	public void setAlDevOpr(ArrayList alDevOpr) {
		this.alDevOpr = alDevOpr;
	}

	/**
	 * @return the dispUnresolvedDet
	 */
	public boolean isDispUnresolvedDet() {
		return dispUnresolvedDet;
	}

	/**
	 * @param dispUnresolvedDet the dispUnresolvedDet to set
	 */
	public void setDispUnresolvedDet(boolean dispUnresolvedDet) {
		this.dispUnresolvedDet = dispUnresolvedDet;
	}

	/**
	 * @return the dispNonApproved
	 */
	public boolean isDispNonApproved() {
		return dispNonApproved;
	}

	/**
	 * @param dispNonApproved the dispNonApproved to set
	 */
	public void setDispNonApproved(boolean dispNonApproved) {
		this.dispNonApproved = dispNonApproved;
	}

	/**
	 * @return the dispNonReconciliation
	 */
	public boolean isDispNonReconciliation() {
		return dispNonReconciliation;
	}

	/**
	 * @param dispNonReconciliation the dispNonReconciliation to set
	 */
	public void setDispNonReconciliation(boolean dispNonReconciliation) {
		this.dispNonReconciliation = dispNonReconciliation;
	}


	

}
