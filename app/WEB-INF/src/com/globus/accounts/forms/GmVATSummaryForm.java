package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmCommonForm;

/**
 * @author Arockia Prasath
 * 
 */
public class GmVATSummaryForm extends GmCommonForm {
  private String gridData = "";
  private Date fromDate;
  private Date toDate;
  private String strCompCurrency = "";
  private String strARRptByDealerFlag = "";

 
  private ArrayList alCompCurrency = new ArrayList();

  /**
   * @return the strARRptByDealerFlag
   */
  public String getStrARRptByDealerFlag() {
    return strARRptByDealerFlag;
  }

  /**
   * @param strARRptByDealerFlag the strARRptByDealerFlag to set
   */
  public void setStrARRptByDealerFlag(String strARRptByDealerFlag) {
    this.strARRptByDealerFlag = strARRptByDealerFlag;
  }

  /**
   * @return the gridData
   */
  public String getGridData() {
    return gridData;
  }

  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  public Date getFromDate() {
    return fromDate;
  }

  public void setFromDate(Date fromDate) {
    this.fromDate = fromDate;
  }

  public Date getToDate() {
    return toDate;
  }

  public void setToDate(Date toDate) {
    this.toDate = toDate;
  }

  /**
   * @return the strCompCurrency
   */
  public String getStrCompCurrency() {
    return strCompCurrency;
  }

  /**
   * @param strCompCurrency the strCompCurrency to set
   */
  public void setStrCompCurrency(String strCompCurrency) {
    this.strCompCurrency = strCompCurrency;
  }

  /**
   * @return the alCompCurrency
   */
  public ArrayList getAlCompCurrency() {
    return alCompCurrency;
  }

  /**
   * @param alCompCurrency the alCompCurrency to set
   */
  public void setAlCompCurrency(ArrayList alCompCurrency) {
    this.alCompCurrency = alCompCurrency;
  }


}
