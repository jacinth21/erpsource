package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCancelForm;

public class GmReleaseInvoicePaymentForm extends GmCancelForm {

	private String vendorId = "";
	private String poNumber = "";//"GM-PO-";
	private String poType = "";
	private String matchingType  = "50030";

	private String hinputStr = "";
	private String invcInitiatedBy = "";
	private String invcInitiatedDateFrom = "";
	private String invcInitiatedDateTo = "";
	private String invcStatus = "10";
	
	private String matchingPO = "on"; 
	
	private String hmatchingNotcheck = "";
	private String release_fl = "";
	
	private String strEnableReleasePymt = "false";
	private String strEnableReleaseDownload = "false";
	private String strEnableRollbackRelease = "false";

	private ArrayList alVendorList = new ArrayList();
	private ArrayList alPOTypeList = new ArrayList();
	private ArrayList alInvcInitiatedBy = new ArrayList();
	private ArrayList almatchingTypeList = new ArrayList();
	
	private ArrayList alInvcStatus = new ArrayList();
	private List returnPOList = new ArrayList();  
	private List returnInvcList = new ArrayList();  

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getHinputStr() {
		return hinputStr;
	}

	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}

	public String getMatchingPO() {
		return matchingPO;
	}

	public void setMatchingPO(String matchingPO) {
		this.matchingPO = matchingPO;
	}

	public String getHmatchingNotcheck() {
		return hmatchingNotcheck;
	}

	public void setHmatchingNotcheck(String hmatchingNotcheck) {
		this.hmatchingNotcheck = hmatchingNotcheck;
	}

	public ArrayList getAlVendorList() {
		return alVendorList;
	}

	public void setAlVendorList(ArrayList alVendorList) {
		this.alVendorList = alVendorList;
	}

	public List getReturnPOList() {
		return returnPOList;
	}

	public void setReturnPOList(List returnPOList) {
		this.returnPOList = returnPOList;
	}

	public List getReturnInvcList() {
		return returnInvcList;
	}

	public void setReturnInvcList(List returnInvcList) {
		this.returnInvcList = returnInvcList;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	public String getMatchingType() {
		return matchingType;
	}

	public void setMatchingType(String matchingType) {
		this.matchingType = matchingType;
	}

	public ArrayList getAlPOTypeList() {
		return alPOTypeList;
	}

	public void setAlPOTypeList(ArrayList alPOTypeList) {
		this.alPOTypeList = alPOTypeList;
	}

	public ArrayList getAlmatchingTypeList() {
		return almatchingTypeList;
	}

	public void setAlmatchingTypeList(ArrayList almatchingTypeList) {
		this.almatchingTypeList = almatchingTypeList;
	}

	public String getInvcInitiatedBy() {
		return invcInitiatedBy;
	}

	public void setInvcInitiatedBy(String invcInitiatedBy) {
		this.invcInitiatedBy = invcInitiatedBy;
	} 

	public ArrayList getAlInvcInitiatedBy() {
		return alInvcInitiatedBy;
	}

	public void setAlInvcInitiatedBy(ArrayList alInvcInitiatedBy) {
		this.alInvcInitiatedBy = alInvcInitiatedBy;
	}

	/**
	 * @return the strEnableRelasePymt
	 */
	public String getStrEnableReleasePymt() {
		return strEnableReleasePymt;
	}

	/**
	 * @param strEnableRelasePymt the strEnableRelasePymt to set
	 */
	public void setStrEnableReleasePymt(String strEnableRelasePymt) {
		this.strEnableReleasePymt = strEnableRelasePymt;
	}

	public String getInvcInitiatedDateFrom() {
		return invcInitiatedDateFrom;
	}

	public void setInvcInitiatedDateFrom(String invcInitiatedDateFrom) {
		this.invcInitiatedDateFrom = invcInitiatedDateFrom;
	}

	public String getInvcInitiatedDateTo() {
		return invcInitiatedDateTo;
	}

	public void setInvcInitiatedDateTo(String invcInitiatedDateTo) {
		this.invcInitiatedDateTo = invcInitiatedDateTo;
	}

	public String getInvcStatus() {
		return invcStatus;
	}

	public void setInvcStatus(String invcStatus) {
		this.invcStatus = invcStatus;
	}

	public ArrayList getAlInvcStatus() {
		return alInvcStatus;
	}

	public void setAlInvcStatus(ArrayList alInvcStatus) {
		this.alInvcStatus = alInvcStatus;
	}

	public String getRelease_fl() {
		return release_fl;
	}

	public void setRelease_fl(String release_fl) {
		this.release_fl = release_fl;
	}

	/**
	 * @return the strEnableReleaseDownload
	 */
	public String getStrEnableReleaseDownload() {
		return strEnableReleaseDownload;
	}

	/**
	 * @param strEnableReleaseDownload the strEnableReleaseDownload to set
	 */
	public void setStrEnableReleaseDownload(String strEnableReleaseDownload) {
		this.strEnableReleaseDownload = strEnableReleaseDownload;
	}

	/**
	 * @return the strEnableRollbackRelease
	 */
	public String getStrEnableRollbackRelease() {
		return strEnableRollbackRelease;
	}

	/**
	 * @param strEnableRollbackRelease the strEnableRollbackRelease to set
	 */
	public void setStrEnableRollbackRelease(String strEnableRollbackRelease) {
		this.strEnableRollbackRelease = strEnableRollbackRelease;
	}
}
