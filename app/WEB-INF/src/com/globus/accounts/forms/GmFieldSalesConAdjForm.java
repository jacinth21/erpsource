package com.globus.accounts.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmFieldSalesConAdjForm extends GmCommonForm{
	
	private String adjustmentType = "";
	private String consignTo = "";
	private String tagID = "";
	private String pnumStr = "";
	private String setID = "";
	private String comments = "";
	
	private ArrayList alAjustmentType = new ArrayList();
	private ArrayList alConsignTo = new ArrayList();
	private ArrayList alSetList = new ArrayList();
	
	
	/**
	 * @return the adjustmentType
	 */
	public String getAdjustmentType() {
		return adjustmentType;
	}
	/**
	 * @param adjustmentType the adjustmentType to set
	 */
	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}
	/**
	 * @return the consignTo
	 */
	public String getConsignTo() {
		return consignTo;
	}
	/**
	 * @param consignTo the consignTo to set
	 */
	public void setConsignTo(String consignTo) {
		this.consignTo = consignTo;
	}
	/**
	 * @return the tagID
	 */
	public String getTagID() {
		return tagID;
	}
	/**
	 * @param tagID the tagID to set
	 */
	public void setTagID(String tagID) {
		this.tagID = tagID;
	}
	
	/**
	 * @return the pnumStr
	 */
	public String getPnumStr() {
		return pnumStr;
	}
	/**
	 * @param pnumStr the pnumStr to set
	 */
	public void setPnumStr(String pnumStr) {
		this.pnumStr = pnumStr;
	}
	/**
	 * @return the setID
	 */
	public String getSetID() {
		return setID;
	}
	/**
	 * @param setID the setID to set
	 */
	public void setSetID(String setID) {
		this.setID = setID;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the alAjustmentType
	 */
	public ArrayList getAlAjustmentType() {
		return alAjustmentType;
	}
	/**
	 * @param alAjustmentType the alAjustmentType to set
	 */
	public void setAlAjustmentType(ArrayList alAjustmentType) {
		this.alAjustmentType = alAjustmentType;
	}
	/**
	 * @return the alConsignTo
	 */
	public ArrayList getAlConsignTo() {
		return alConsignTo;
	}
	/**
	 * @param alConsignTo the alConsignTo to set
	 */
	public void setAlConsignTo(ArrayList alConsignTo) {
		this.alConsignTo = alConsignTo;
	}
	/**
	 * @return the alSetList
	 */
	public ArrayList getAlSetList() {
		return alSetList;
	}
	/**
	 * @param alSetList the alSetList to set
	 */
	public void setAlSetList(ArrayList alSetList) {
		this.alSetList = alSetList;
	}

	
	
	
}

