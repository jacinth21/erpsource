package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCancelForm;

public class GmAuditReconcilationForm extends GmCancelForm{

	private String auditId="";
	private String distId="";
	private String strOpt="";
	private String cbo_Action="";
	
	private ArrayList alDistList = new ArrayList();
	private ArrayList alAuditList = new ArrayList();
	private ArrayList alAuditAction= new ArrayList();
	private HashMap hmResult =new HashMap();

	
	
	
	/**
	 * @return the cbo_Action
	 */
	public String getCbo_Action() {
		return cbo_Action;
	}
	/**
	 * @param cbo_Action the cbo_Action to set
	 */
	public void setCbo_Action(String cbo_Action) {
		this.cbo_Action = cbo_Action;
	}
	/**
	 * @return the alAuditAction
	 */
	public ArrayList getAlAuditAction() {
		return alAuditAction;
	}
	/**
	 * @param alAuditAction the alAuditAction to set
	 */
	public void setAlAuditAction(ArrayList alAuditAction) {
		this.alAuditAction = alAuditAction;
	}
	public HashMap getHmResult() {
		return hmResult;
	}
	public void setHmResult(HashMap hmResult) {
		this.hmResult = hmResult;
	}
	public String getStrOpt() {
		return strOpt;
	}
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}
	public ArrayList getAlDistList() {
		return alDistList;
	}
	public void setAlDistList(ArrayList alDistList) {
		this.alDistList = alDistList;
	}
	public String getAuditId() {
		return auditId;
	}
	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}
	public String getDistId() {
		return distId;
	}
	public void setDistId(String distId) {
		this.distId = distId;
	}
	public ArrayList getAlAuditList() {
		return alAuditList;
	}
	public void setAlAuditList(ArrayList alAuditList) {
		this.alAuditList = alAuditList;
	}
}
