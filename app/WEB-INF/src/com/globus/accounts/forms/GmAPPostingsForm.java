package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmAPPostingsForm extends GmCommonForm{

	private String batchNumber ="";
	private String batchDateFrom ="";
	private String batchDateTo ="";
	private String haccountID ="";
	private String hruleID ="";
	
	private List returnList = new ArrayList(); 
	
	public List getReturnList() {
		return returnList;
	}
	public void setReturnList(List returnList) {
		this.returnList = returnList;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	public String getBatchDateFrom() {
		return batchDateFrom;
	}
	public void setBatchDateFrom(String batchDateFrom) {
		this.batchDateFrom = batchDateFrom;
	}
	public String getBatchDateTo() {
		return batchDateTo;
	}
	public void setBatchDateTo(String batchDateTo) {
		this.batchDateTo = batchDateTo;
	}
	public String getHaccountID() {
		return haccountID;
	}
	public void setHaccountID(String haccountID) {
		this.haccountID = haccountID;
	}
	public String getHruleID() {
		return hruleID;
	}
	public void setHruleID(String hruleID) {
		this.hruleID = hruleID;
	}
}
