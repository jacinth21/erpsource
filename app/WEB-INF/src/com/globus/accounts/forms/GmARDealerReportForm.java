package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import com.globus.common.forms.GmCommonForm;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author Jeeva Balaraman
 * 
 */
public class GmARDealerReportForm extends GmCommonForm {

	private String strAccId = "";
	private String strInvSource = "";
	private String strAction = "";
	private String strReportType = "";
	private String strFilterCondition = "";
	private String strAccountCurrency = "";
	private String strDivisionId = "";
	private String strIdAccount = "";
	private String strRegion = "";
	private String strDist = "";
	private String strDistUnChk = "";
	private String strSearchAccName = "";
	
	/**
	 * @return the strAccId
	 */
	public String getStrAccId() {
		return strAccId;
	}
	/**
	 * @param strAccId the strAccId to set
	 */
	public void setStrAccId(String strAccId) {
		this.strAccId = strAccId;
	}
	/**
	 * @return the strInvSource
	 */
	public String getStrInvSource() {
		return strInvSource;
	}
	/**
	 * @param strInvSource the strInvSource to set
	 */
	public void setStrInvSource(String strInvSource) {
		this.strInvSource = strInvSource;
	}
	/**
	 * @return the strAction
	 */
	public String getStrAction() {
		return strAction;
	}
	/**
	 * @param strAction the strAction to set
	 */
	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}
	/**
	 * @return the strReportType
	 */
	public String getStrReportType() {
		return strReportType;
	}
	/**
	 * @param strReportType the strReportType to set
	 */
	public void setStrReportType(String strReportType) {
		this.strReportType = strReportType;
	}
	/**
	 * @return the strFilterCondition
	 */
	public String getStrFilterCondition() {
		return strFilterCondition;
	}
	/**
	 * @param strFilterCondition the strFilterCondition to set
	 */
	public void setStrFilterCondition(String strFilterCondition) {
		this.strFilterCondition = strFilterCondition;
	}
	/**
	 * @return the strAccountCurrency
	 */
	public String getStrAccountCurrency() {
		return strAccountCurrency;
	}
	/**
	 * @param strAccountCurrency the strAccountCurrency to set
	 */
	public void setStrAccountCurrency(String strAccountCurrency) {
		this.strAccountCurrency = strAccountCurrency;
	}
	/**
	 * @return the strDivisionId
	 */
	public String getStrDivisionId() {
		return strDivisionId;
	}
	/**
	 * @param strDivisionId the strDivisionId to set
	 */
	public void setStrDivisionId(String strDivisionId) {
		this.strDivisionId = strDivisionId;
	}
	/**
	 * @return the strIdAccount
	 */
	public String getStrIdAccount() {
		return strIdAccount;
	}
	/**
	 * @param strIdAccount the strIdAccount to set
	 */
	public void setStrIdAccount(String strIdAccount) {
		this.strIdAccount = strIdAccount;
	}
	/**
	 * @return the strRegion
	 */
	public String getStrRegion() {
		return strRegion;
	}
	/**
	 * @param strRegion the strRegion to set
	 */
	public void setStrRegion(String strRegion) {
		this.strRegion = strRegion;
	}
	/**
	 * @return the strDist
	 */
	public String getStrDist() {
		return strDist;
	}
	/**
	 * @param strDist the strDist to set
	 */
	public void setStrDist(String strDist) {
		this.strDist = strDist;
	}
	/**
	 * @return the strDistUnChk
	 */
	public String getStrDistUnChk() {
		return strDistUnChk;
	}
	/**
	 * @param strDistUnChk the strDistUnChk to set
	 */
	public void setStrDistUnChk(String strDistUnChk) {
		this.strDistUnChk = strDistUnChk;
	}
	/**
	 * @return the strSearchAccName
	 */
	public String getStrSearchAccName() {
		return strSearchAccName;
	}
	/**
	 * @param strSearchAccName the strSearchAccName to set
	 */
	public void setStrSearchAccName(String strSearchAccName) {
		this.strSearchAccName = strSearchAccName;
	}
	
}
