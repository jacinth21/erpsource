/**
 * Description : This form class is used for populating AP DHR List report information Copyright :
 * Globus Medical Inc
 */
package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.globus.common.forms.GmLogForm;

/**
 * @author Xun
 * 
 */
/**
 * @author ckumar
 * 
 */
public class GmRevenueSamplingForm extends GmLogForm {

  private List alAccountList = new ArrayList();
  private List alRepList = new ArrayList();
  private List alRegion = new ArrayList();
  private List alStatus = new ArrayList();


  private String accountID = "";
  private String repID = "";
  private String regionID = "";
  private String status = "";
  private Date dtOrderFromDT = null;
  private Date dtOrderToDT = null;
  private Date dtStatusFromDT = null;
  private Date dtStatusToDT = null;

  private String strXmlString = "";
  private String searchaccountID = "";

  private int hsize = 0;
  private String hinputStr = "";
  private String chk_ParentFl = "";
  private String txt_AccId = "";
  private String strCompCurrency = "";
  private String strARRptByDealerFlag = "";
  private String strOrdDateType = "";

  private ArrayList alCompCurrency = new ArrayList();
  private ArrayList alOrdDateType = new ArrayList();


  /**
   * @return the strARRptByDealerFlag
   */
  public String getStrARRptByDealerFlag() {
    return strARRptByDealerFlag;
  }

  /**
   * @param strARRptByDealerFlag the strARRptByDealerFlag to set
   */
  public void setStrARRptByDealerFlag(String strARRptByDealerFlag) {
    this.strARRptByDealerFlag = strARRptByDealerFlag;
  }

  /**
   * @return the hinputStr
   */
  public String getHinputStr() {
    return hinputStr;
  }

  /**
   * @param hinputStr the hinputStr to set
   */
  public void setHinputStr(String hinputStr) {
    this.hinputStr = hinputStr;
  }

  /**
   * @return the hsize
   */
  public int getHsize() {
    return hsize;
  }

  /**
   * @param hsize the hsize to set
   */
  public void setHsize(int hsize) {
    this.hsize = hsize;
  }

  /**
   * @return the strXmlString
   */
  public String getStrXmlString() {
    return strXmlString;
  }

  /**
   * @param strXmlString the strXmlString to set
   */
  public void setStrXmlString(String strXmlString) {
    this.strXmlString = strXmlString;
  }

  /**
   * @return the alAccountList
   */
  public List getAlAccountList() {
    return alAccountList;
  }

  /**
   * @param alAccountList the alAccountList to set
   */
  public void setAlAccountList(List alAccountList) {
    this.alAccountList = alAccountList;
  }

  /**
   * @return the alRepList
   */
  public List getAlRepList() {
    return alRepList;
  }

  /**
   * @param alRepList the alRepList to set
   */
  public void setAlRepList(List alRepList) {
    this.alRepList = alRepList;
  }

  /**
   * @return the alRegion
   */
  public List getAlRegion() {
    return alRegion;
  }

  /**
   * @param alRegion the alRegion to set
   */
  public void setAlRegion(List alRegion) {
    this.alRegion = alRegion;
  }

  /**
   * @return the alStatus
   */
  public List getAlStatus() {
    return alStatus;
  }

  /**
   * @param alStatus the alStatus to set
   */
  public void setAlStatus(List alStatus) {
    this.alStatus = alStatus;
  }

  /**
   * @return the accountID
   */
  public String getAccountID() {
    return accountID;
  }

  /**
   * @param accountID the accountID to set
   */
  public void setAccountID(String accountID) {
    this.accountID = accountID;
  }

  /**
   * @return the repID
   */
  public String getRepID() {
    return repID;
  }

  /**
   * @param repID the repID to set
   */
  public void setRepID(String repID) {
    this.repID = repID;
  }

  /**
   * @return the regionID
   */
  public String getRegionID() {
    return regionID;
  }

  /**
   * @param regionID the regionID to set
   */
  public void setRegionID(String regionID) {
    this.regionID = regionID;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the dtOrderFromDT
   */
  public Date getDtOrderFromDT() {
    return dtOrderFromDT;
  }

  /**
   * @param dtOrderFromDT the dtOrderFromDT to set
   */
  public void setDtOrderFromDT(Date dtOrderFromDT) {
    this.dtOrderFromDT = dtOrderFromDT;
  }

  /**
   * @return the dtOrderToDT
   */
  public Date getDtOrderToDT() {
    return dtOrderToDT;
  }

  /**
   * @param dtOrderToDT the dtOrderToDT to set
   */
  public void setDtOrderToDT(Date dtOrderToDT) {
    this.dtOrderToDT = dtOrderToDT;
  }

  /**
   * @return the dtStatusFromDT
   */
  public Date getDtStatusFromDT() {
    return dtStatusFromDT;
  }

  /**
   * @param dtStatusFromDT the dtStatusFromDT to set
   */
  public void setDtStatusFromDT(Date dtStatusFromDT) {
    this.dtStatusFromDT = dtStatusFromDT;
  }

  /**
   * @return the dtStatusToDT
   */
  public Date getDtStatusToDT() {
    return dtStatusToDT;
  }

  /**
   * @param dtStatusToDT the dtStatusToDT to set
   */
  public void setDtStatusToDT(Date dtStatusToDT) {
    this.dtStatusToDT = dtStatusToDT;
  }

  /**
   * @return the txt_AccId
   */
  public String getTxt_AccId() {
    return txt_AccId;
  }

  /**
   * @param txt_AccId the txt_AccId to set
   */
  public void setTxt_AccId(String txt_AccId) {
    this.txt_AccId = txt_AccId;
  }

  /**
   * @return the chk_ParentFl
   */
  public String getChk_ParentFl() {
    return chk_ParentFl;
  }

  /**
   * @param chk_ParentFl the chk_ParentFl to set
   */
  public void setChk_ParentFl(String chk_ParentFl) {
    this.chk_ParentFl = chk_ParentFl;
  }

  /**
   * @return the strCompCurrency
   */
  public String getStrCompCurrency() {
    return strCompCurrency;
  }

  /**
   * @param strCompCurrency the strCompCurrency to set
   */
  public void setStrCompCurrency(String strCompCurrency) {
    this.strCompCurrency = strCompCurrency;
  }

  /**
   * @return the alCompCurrency
   */
  public ArrayList getAlCompCurrency() {
    return alCompCurrency;
  }

  /**
   * @param alCompCurrency the alCompCurrency to set
   */
  public void setAlCompCurrency(ArrayList alCompCurrency) {
    this.alCompCurrency = alCompCurrency;
  }

  /**
   * @return the strOrdDateType
   */
  public String getStrOrdDateType() {
    return strOrdDateType;
  }

  /**
   * @param strOrdDateType the strOrdDateType to set
   */
  public void setStrOrdDateType(String strOrdDateType) {
    this.strOrdDateType = strOrdDateType;
  }

  /**
   * @return the alOrdDateType
   */
  public ArrayList getAlOrdDateType() {
    return alOrdDateType;
  }

  /**
   * @param alOrdDateType the alOrdDateType to set
   */
  public void setAlOrdDateType(ArrayList alOrdDateType) {
    this.alOrdDateType = alOrdDateType;
  }

  /**
   * @return the searchaccountID
   */
  public String getSearchaccountID() {
    return searchaccountID;
  }

  /**
   * @param searchaccountID the searchaccountID to set
   */
  public void setSearchaccountID(String searchaccountID) {
    this.searchaccountID = searchaccountID;
  }



}
