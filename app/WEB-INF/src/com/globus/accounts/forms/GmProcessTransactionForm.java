package com.globus.accounts.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author Velu
 * 
 */
public class GmProcessTransactionForm extends GmCommonForm {
  private String strRefID = "";
  private String strSource = "1006652"; // Invoice Type Source
  private String strURL = "";
  private String strCompCurrency = "";

  private ArrayList alCompCurrency = new ArrayList();
  private ArrayList alSource = new ArrayList();


  /**
   * @return the strRefID
   */
  public String getStrRefID() {
    return strRefID;
  }

  /**
   * @param strRefID the strRefID to set
   */
  public void setStrRefID(String strRefID) {
    this.strRefID = strRefID;
  }

  /**
   * @return the strSource
   */
  public String getStrSource() {
    return strSource;
  }

  /**
   * @param strSource the strSource to set
   */
  public void setStrSource(String strSource) {
    this.strSource = strSource;
  }

  /**
   * @return the strURL
   */
  public String getStrURL() {
    return strURL;
  }

  /**
   * @param strURL the strURL to set
   */
  public void setStrURL(String strURL) {
    this.strURL = strURL;
  }

  /**
   * @return the alSource
   */
  public ArrayList getAlSource() {
    return alSource;
  }

  /**
   * @param alSource the alSource to set
   */
  public void setAlSource(ArrayList alSource) {
    this.alSource = alSource;
  }

  /**
   * @return the strCompCurrency
   */
  public String getStrCompCurrency() {
    return strCompCurrency;
  }

  /**
   * @param strCompCurrency the strCompCurrency to set
   */
  public void setStrCompCurrency(String strCompCurrency) {
    this.strCompCurrency = strCompCurrency;
  }

  /**
   * @return the alCompCurrency
   */
  public ArrayList getAlCompCurrency() {
    return alCompCurrency;
  }

  /**
   * @param alCompCurrency the alCompCurrency to set
   */
  public void setAlCompCurrency(ArrayList alCompCurrency) {
    this.alCompCurrency = alCompCurrency;
  }
}
