package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

public class GmAccountActivityForm extends GmCommonForm {

  private String fromDt = "";
  private String toDt = "";
  private String status = "";
  private String invSource = "";
  private String gridData = "";
  private String accSelected = "";
  private String typeSelected = "";
  private String strAccountName = "";
  private String strJasRptName = "";
  private String strCompCurrency = "";

  private ArrayList alStatus = new ArrayList();
  private ArrayList alInvSource = new ArrayList();
  private ArrayList alAccount = new ArrayList();
  private ArrayList alInvType = new ArrayList();
  private ArrayList alReturn = new ArrayList();
  private ArrayList alCompCurrency = new ArrayList();

  /**
   * @return the alReturn
   */
  public ArrayList getAlReturn() {
    return alReturn;
  }

  /**
   * @param alReturn the alReturn to set
   */
  public void setAlReturn(ArrayList alReturn) {
    this.alReturn = alReturn;
  }

  private HashMap hmJasperDetails = new HashMap();

  /**
   * @return the hmJasperDetails
   */
  public HashMap getHmJasperDetails() {
    return hmJasperDetails;
  }

  /**
   * @param hmJasperDetails the hmJasperDetails to set
   */
  public void setHmJasperDetails(HashMap hmJasperDetails) {
    this.hmJasperDetails = hmJasperDetails;
  }

  /**
   * @param fromDt the fromDt to set
   */
  /**
   * @return the fromDt
   */
  public String getFromDt() {
    return fromDt;
  }

  public void setFromDt(String fromDt) {
    this.fromDt = fromDt;
  }

  /**
   * @return the toDt
   */
  public String getToDt() {
    return toDt;
  }

  /**
   * @param toDt the toDt to set
   */
  public void setToDt(String toDt) {
    this.toDt = toDt;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the invSource
   */
  public String getInvSource() {
    return invSource;
  }

  /**
   * @param invSource the invSource to set
   */
  public void setInvSource(String invSource) {
    this.invSource = invSource;
  }

  /**
   * @return the alStatus
   */
  public ArrayList getAlStatus() {
    return alStatus;
  }

  /**
   * @param alStatus the alStatus to set
   */
  public void setAlStatus(ArrayList alStatus) {
    this.alStatus = alStatus;
  }

  /**
   * @return the alInvSource
   */
  public ArrayList getAlInvSource() {
    return alInvSource;
  }

  /**
   * @param alInvSource the alInvSource to set
   */
  public void setAlInvSource(ArrayList alInvSource) {
    this.alInvSource = alInvSource;
  }

  /**
   * @return the alAccount
   */
  public ArrayList getAlAccount() {
    return alAccount;
  }

  /**
   * @param alAccount the alAccount to set
   */
  public void setAlAccount(ArrayList alAccount) {
    this.alAccount = alAccount;
  }

  /**
   * @return the alInvType
   */
  public ArrayList getAlInvType() {
    return alInvType;
  }

  /**
   * @param alInvType the alInvType to set
   */
  public void setAlInvType(ArrayList alInvType) {
    this.alInvType = alInvType;
  }

  /**
   * @return the gridData
   */
  public String getGridData() {
    return gridData;
  }

  /**
   * @param gridData the gridData to set
   */
  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  /**
   * @return the accSelected
   */
  public String getAccSelected() {
    return accSelected;
  }

  /**
   * @param accSelected the accSelected to set
   */
  public void setAccSelected(String accSelected) {
    this.accSelected = accSelected;
  }

  /**
   * @return the typeSelected
   */
  public String getTypeSelected() {
    return typeSelected;
  }

  /**
   * @param typeSelected the typeSelected to set
   */
  public void setTypeSelected(String typeSelected) {
    this.typeSelected = typeSelected;
  }

  /**
   * @return the strAccountName
   */
  public String getStrAccountName() {
    return strAccountName;
  }

  /**
   * @param strAccountName the strAccountName to set
   */
  public void setStrAccountName(String strAccountName) {
    this.strAccountName = strAccountName;
  }

  /**
   * @return the strJasRptName
   */
  public String getStrJasRptName() {
    return strJasRptName;
  }

  /**
   * @param strJasRptName the strJasRptName to set
   */
  public void setStrJasRptName(String strJasRptName) {
    this.strJasRptName = strJasRptName;
  }

  /**
   * @return the alCompCurrency
   */
  public ArrayList getAlCompCurrency() {
    return alCompCurrency;
  }

  /**
   * @param alCompCurrency the alCompCurrency to set
   */
  public void setAlCompCurrency(ArrayList alCompCurrency) {
    this.alCompCurrency = alCompCurrency;
  }

  /**
   * @return the strCompCurrency
   */
  public String getStrCompCurrency() {
    return strCompCurrency;
  }

  /**
   * @param strCompCurrency the strCompCurrency to set
   */
  public void setStrCompCurrency(String strCompCurrency) {
    this.strCompCurrency = strCompCurrency;
  }



}
