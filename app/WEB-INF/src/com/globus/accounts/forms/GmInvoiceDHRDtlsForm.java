package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmInvoiceDHRDtlsForm extends GmCommonForm{

	private String vendorId = "";
	private String dhrNumber ="";
	private String invoiceNumber ="";
	private String invoiceDateFrom ="";
	private String invoiceDateTo="";
	private String downloadDateFrom ="";
	private String downloadDateTo="";
	
	private String invoiceType="";
	private String invoiceOper="";
	private String operatorSign="";
	private String poID="";
	private String hCloseFlag= "";
	
	private ArrayList alVendorList = new ArrayList();
	private List returnList = new ArrayList();
	
	private ArrayList alInvoiceType = new ArrayList();
	private ArrayList alInvoiceOper = new ArrayList();	
	
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getDhrNumber() {
		return dhrNumber;
	}
	public void setDhrNumber(String dhrNumber) {
		this.dhrNumber = dhrNumber;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public String getInvoiceDateFrom() {
		return invoiceDateFrom;
	}
	public void setInvoiceDateFrom(String invoiceDateFrom) {
		this.invoiceDateFrom = invoiceDateFrom;
	}
	public String getInvoiceDateTo() {
		return invoiceDateTo;
	}
	public void setInvoiceDateTo(String invoiceDateTo) {
		this.invoiceDateTo = invoiceDateTo;
	}
	public ArrayList getAlVendorList() {
		return alVendorList;
	}
	public void setAlVendorList(ArrayList alVendorList) {
		this.alVendorList = alVendorList;
	}
	public List getReturnList() {
		return returnList;
	}
	public void setReturnList(List returnList) {
		this.returnList = returnList;
	}
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	public String getInvoiceOper() {
		return invoiceOper;
	}
	public void setInvoiceOper(String invoiceOper) {
		this.invoiceOper = invoiceOper;
	}
	public ArrayList getAlInvoiceType() {
		return alInvoiceType;
	}
	public void setAlInvoiceType(ArrayList alInvoiceType) {
		this.alInvoiceType = alInvoiceType;
	}
	public ArrayList getAlInvoiceOper() {
		return alInvoiceOper;
	}
	public void setAlInvoiceOper(ArrayList alInvoiceOper) {
		this.alInvoiceOper = alInvoiceOper;
	}
	public String getOperatorSign() {
		return operatorSign;
	}
	public void setOperatorSign(String operatorSign) {
		this.operatorSign = operatorSign;
	}
	/**
	 * @return the downloadDateFrom
	 */
	public String getDownloadDateFrom() {
		return downloadDateFrom;
	}
	/**
	 * @param downloadDateFrom the downloadDateFrom to set
	 */
	public void setDownloadDateFrom(String downloadDateFrom) {
		this.downloadDateFrom = downloadDateFrom;
	}
	/**
	 * @return the downloadDateTo
	 */
	public String getDownloadDateTo() {
		return downloadDateTo;
	}
	/**
	 * @param downloadDateTo the downloadDateTo to set
	 */
	public void setDownloadDateTo(String downloadDateTo) {
		this.downloadDateTo = downloadDateTo;
	}
	/**
	 * @return the poID
	 */
	public String getPoID() {
		return poID;
	}
	/**
	 * @param poID the poID to set
	 */
	public void setPoID(String poID) {
		this.poID = poID;
	}
	/**
	 * @return the hCloseFlag
	 */
	public String gethCloseFlag() {
		return hCloseFlag;
	}
	/**
	 * @param hCloseFlag the hCloseFlag to set
	 */
	public void sethCloseFlag(String hCloseFlag) {
		this.hCloseFlag = hCloseFlag;
	} 
	
}
