package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmICTSummaryForm extends GmCommonForm{
	 private List lsummary = new ArrayList();

	/**
	 * @return the lsummary
	 */
	public List getLsummary() {
		return lsummary;
	}

	/**
	 * @param lsummary the lsummary to set
	 */
	public void setLsummary(List lsummary) {
		this.lsummary = lsummary;
	}

	
}
