package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.struts.upload.FormFile;

import com.globus.common.forms.GmCommonForm;

public class GmInvoiceUploadForm extends GmCommonForm{
	private FormFile file;
	private HashMap hmOrderDetails = new HashMap();
	private ArrayList alUploadinfo =  new ArrayList();
	private String invoiceId ="";

	
	
	/**
	 * @return the alUploadinfo
	 */
	public ArrayList getAlUploadinfo() {
		return alUploadinfo;
	}

	/**
	 * @param alUploadinfo the alUploadinfo to set
	 */
	public void setAlUploadinfo(ArrayList alUploadinfo) {
		this.alUploadinfo = alUploadinfo;
	}

	/**
	 * @return the invoiceId
	 */
	public String getInvoiceId() {
		return invoiceId;
	}

	/**
	 * @param invoiceId the invoiceId to set
	 */
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	/**
	 * @return the file
	 */
	public FormFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(FormFile file) {
		this.file = file;
	}

	/**
	 * @return the hmOrderDetails
	 */
	public HashMap getHmOrderDetails() {
		return hmOrderDetails;
	}

	/**
	 * @param hmOrderDetails the hmOrderDetails to set
	 */
	public void setHmOrderDetails(HashMap hmOrderDetails) {
		this.hmOrderDetails = hmOrderDetails;
	}
	
}
