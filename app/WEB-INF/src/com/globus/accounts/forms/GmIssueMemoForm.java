package com.globus.accounts.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmIssueMemoForm extends GmCommonForm {

  private String inputString = "";
  private String memoType = "";
  private String gridData = "";
  private String invoiceID = "";
  private String partNum = "";
  private ArrayList alMemoType = new ArrayList();
  private String strCompCurrency = "";
  private String strARRptByDealerFlag = "";


  private ArrayList alCompCurrency = new ArrayList();
 
  /**
   * @return the strARRptByDealerFlag
   */
  public String getStrARRptByDealerFlag() {
    return strARRptByDealerFlag;
  }

  /**
   * @param strARRptByDealerFlag the strARRptByDealerFlag to set
   */
  public void setStrARRptByDealerFlag(String strARRptByDealerFlag) {
    this.strARRptByDealerFlag = strARRptByDealerFlag;
  }

  /**
   * @return the inputString
   */
  public String getInputString() {
    return inputString;
  }

  /**
   * @param inputString the inputString to set
   */
  public void setInputString(String inputString) {
    this.inputString = inputString;
  }

  /**
   * @return the memoType
   */
  public String getMemoType() {
    return memoType;
  }

  /**
   * @param memoType the memoType to set
   */
  public void setMemoType(String memoType) {
    this.memoType = memoType;
  }

  /**
   * @return the gridData
   */
  public String getGridData() {
    return gridData;
  }

  /**
   * @param gridData the gridData to set
   */
  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  /**
   * @return the invoiceID
   */
  public String getInvoiceID() {
    return invoiceID;
  }

  /**
   * @param invoiceID the invoiceID to set
   */
  public void setInvoiceID(String invoiceID) {
    this.invoiceID = invoiceID;
  }

  /**
   * @return the partNum
   */
  public String getPartNum() {
    return partNum;
  }

  /**
   * @param partNum the partNum to set
   */
  public void setPartNum(String partNum) {
    this.partNum = partNum;
  }

  /**
   * @return the alMemoType
   */
  public ArrayList getAlMemoType() {
    return alMemoType;
  }

  /**
   * @param alMemoType the alMemoType to set
   */
  public void setAlMemoType(ArrayList alMemoType) {
    this.alMemoType = alMemoType;
  }

  /**
   * @return the strCompCurrency
   */
  public String getStrCompCurrency() {
    return strCompCurrency;
  }

  /**
   * @param strCompCurrency the strCompCurrency to set
   */
  public void setStrCompCurrency(String strCompCurrency) {
    this.strCompCurrency = strCompCurrency;
  }

  /**
   * @return the alCompCurrency
   */
  public ArrayList getAlCompCurrency() {
    return alCompCurrency;
  }

  /**
   * @param alCompCurrency the alCompCurrency to set
   */
  public void setAlCompCurrency(ArrayList alCompCurrency) {
    this.alCompCurrency = alCompCurrency;
  }

}
