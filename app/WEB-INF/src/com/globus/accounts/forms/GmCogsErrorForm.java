package com.globus.accounts.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmCogsErrorForm extends GmCommonForm {
  private String txnId = "";
  private String pnum = "";
  private String fromDate = "";
  private String toDate = "";
  private String invType = "";
  private String status = "";
  private String inputStr = "";
  private String companyId = "";
  private String countryId = "0";
  private String cogsErrorType = "";
  private String costingPlantId = "";
  private String ownerCompanyId = "";

  private ArrayList alInvType = new ArrayList();
  private ArrayList alStatus = new ArrayList();
  private ArrayList alCompanyId = new ArrayList();
  private ArrayList alCountryId = new ArrayList();
  private ArrayList alCogsErrorType = new ArrayList();
  private ArrayList alCogsPlant = new ArrayList();
  private ArrayList alCogsOwnerCompany = new ArrayList();


  private String gridXmlData = "";


  /**
   * @return the txnId
   */
  public String getTxnId() {
    return txnId;
  }

  /**
   * @param txnId the txnId to set
   */
  public void setTxnId(String txnId) {
    this.txnId = txnId;
  }

  /**
   * @return the pnum
   */
  public String getPnum() {
    return pnum;
  }

  /**
   * @param pnum the pnum to set
   */
  public void setPnum(String pnum) {
    this.pnum = pnum;
  }

  /**
   * @return the fromDate
   */
  public String getFromDate() {
    return fromDate;
  }

  /**
   * @param fromDate the fromDate to set
   */
  public void setFromDate(String fromDate) {
    this.fromDate = fromDate;
  }

  /**
   * @return the toDate
   */
  public String getToDate() {
    return toDate;
  }

  /**
   * @param toDate the toDate to set
   */
  public void setToDate(String toDate) {
    this.toDate = toDate;
  }

  /**
   * @return the invType
   */
  public String getInvType() {
    return invType;
  }

  /**
   * @param invType the invType to set
   */
  public void setInvType(String invType) {
    this.invType = invType;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the inputStr
   */
  public String getInputStr() {
    return inputStr;
  }

  /**
   * @param inputStr the inputStr to set
   */
  public void setInputStr(String inputStr) {
    this.inputStr = inputStr;
  }

  /**
   * @return the alInvType
   */
  public ArrayList getAlInvType() {
    return alInvType;
  }

  /**
   * @param alInvType the alInvType to set
   */
  public void setAlInvType(ArrayList alInvType) {
    this.alInvType = alInvType;
  }

  /**
   * @return the alStatus
   */
  public ArrayList getAlStatus() {
    return alStatus;
  }

  /**
   * @param alStatus the alStatus to set
   */
  public void setAlStatus(ArrayList alStatus) {
    this.alStatus = alStatus;
  }



  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the companyId
   */
  public String getCompanyId() {
    return companyId;
  }

  /**
   * @param companyId the companyId to set
   */
  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  /**
   * @return the countryId
   */
  public String getCountryId() {
    return countryId;
  }

  /**
   * @param countryId the countryId to set
   */
  public void setCountryId(String countryId) {
    this.countryId = countryId;
  }

  /**
   * @return the cogsErrorType
   */
  public String getCogsErrorType() {
    return cogsErrorType;
  }

  /**
   * @param cogsErrorType the cogsErrorType to set
   */
  public void setCogsErrorType(String cogsErrorType) {
    this.cogsErrorType = cogsErrorType;
  }

  /**
   * @return the alCompanyId
   */
  public ArrayList getAlCompanyId() {
    return alCompanyId;
  }

  /**
   * @param alCompanyId the alCompanyId to set
   */
  public void setAlCompanyId(ArrayList alCompanyId) {
    this.alCompanyId = alCompanyId;
  }

  /**
   * @return the alCountryId
   */
  public ArrayList getAlCountryId() {
    return alCountryId;
  }

  /**
   * @param alCountryId the alCountryId to set
   */
  public void setAlCountryId(ArrayList alCountryId) {
    this.alCountryId = alCountryId;
  }

  /**
   * @return the alCogsErrorType
   */
  public ArrayList getAlCogsErrorType() {
    return alCogsErrorType;
  }

  /**
   * @param alCogsErrorType the alCogsErrorType to set
   */
  public void setAlCogsErrorType(ArrayList alCogsErrorType) {
    this.alCogsErrorType = alCogsErrorType;
  }

  /**
   * @return the costingPlantId
   */
  public String getCostingPlantId() {
    return costingPlantId;
  }

  /**
   * @param costingPlantId the costingPlantId to set
   */
  public void setCostingPlantId(String costingPlantId) {
    this.costingPlantId = costingPlantId;
  }

  /**
   * @return the ownerCompanyId
   */
  public String getOwnerCompanyId() {
    return ownerCompanyId;
  }

  /**
   * @param ownerCompanyId the ownerCompanyId to set
   */
  public void setOwnerCompanyId(String ownerCompanyId) {
    this.ownerCompanyId = ownerCompanyId;
  }

  /**
   * @return the alCogsPlant
   */
  public ArrayList getAlCogsPlant() {
    return alCogsPlant;
  }

  /**
   * @param alCogsPlant the alCogsPlant to set
   */
  public void setAlCogsPlant(ArrayList alCogsPlant) {
    this.alCogsPlant = alCogsPlant;
  }

  /**
   * @return the alCogsOwnerCompany
   */
  public ArrayList getAlCogsOwnerCompany() {
    return alCogsOwnerCompany;
  }

  /**
   * @param alCogsOwnerCompany the alCogsOwnerCompany to set
   */
  public void setAlCogsOwnerCompany(ArrayList alCogsOwnerCompany) {
    this.alCogsOwnerCompany = alCogsOwnerCompany;
  }



}
