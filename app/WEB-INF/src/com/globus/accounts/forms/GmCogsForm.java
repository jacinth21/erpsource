package com.globus.accounts.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmCogsForm extends GmCommonForm {

	private ArrayList projectList = new ArrayList();
	private ArrayList inventoryList = new ArrayList();
	private List rptSummary = new ArrayList();
	private String inventoryListID = "";
	private String projectListID = "";
	private String pnum = "";
	private String pnumSuffix = "";

	/**
	 * @return the projectList
	 */
	public ArrayList getProjectList() {
		return projectList;
	}

	/**
	 * @param projectList
	 *            the projectList to set
	 */
	public void setProjectList(ArrayList projectList) {
		this.projectList = projectList;
	}

	/**
	 * @return the inventoryList
	 */
	public ArrayList getInventoryList() {
		return inventoryList;
	}

	/**
	 * @param inventoryList
	 *            the inventoryList to set
	 */
	public void setInventoryList(ArrayList inventoryList) {
		this.inventoryList = inventoryList;
	}

	/**
	 * @return the rptSummary
	 */
	public List getRptSummary() {
		return rptSummary;
	}

	/**
	 * @param rptSummary
	 *            the rptSummary to set
	 */
	public void setRptSummary(List rptSummary) {
		this.rptSummary = rptSummary;
	}

	/**
	 * @return the inventoryListID
	 */
	public String getInventoryListID() {
		return inventoryListID;
	}

	/**
	 * @param inventoryListID
	 *            the inventoryListID to set
	 */
	public void setInventoryListID(String inventoryListID) {
		this.inventoryListID = inventoryListID;
	}

	/**
	 * @return the projectListID
	 */
	public String getProjectListID() {
		return projectListID;
	}

	/**
	 * @param projectListID
	 *            the projectListID to set
	 */
	public void setProjectListID(String projectListID) {
		this.projectListID = projectListID;
	}

	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}

	/**
	 * @param pnum
	 *            the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	/**
	 * @return the pnumSuffix
	 */
	public String getPnumSuffix() {
		return pnumSuffix;
	}

	/**
	 * @param pnumSuffix
	 *            the pnumSuffix to set
	 */
	public void setPnumSuffix(String pnumSuffix) {
		this.pnumSuffix = pnumSuffix;
	}

}
