package com.globus.accounts.tax.beans;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hornetq.utils.json.JSONArray;
import org.hornetq.utils.json.JSONException;
import org.hornetq.utils.json.JSONObject;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.beans.GmARBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmJsonNameStrategyUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.accounts.GmCancelTaxResponseVo;
import com.globus.valueobject.accounts.GmCancelTaxResultVo;
import com.globus.valueobject.accounts.GmGetTaxLinesVo;
import com.globus.valueobject.accounts.GmGetTaxResponseVo;
import com.globus.valueobject.accounts.GmMessagesVO;
import com.globus.valueobject.accounts.GmValidateAddressVO;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * <h1>GmSalesTaxTransBean has all the new methods to calculate tax for the US Countries by calling
 * the Avalara AvaTax URL. URL:https://development.avalara.net</h1>
 * 
 * @author Manikandan Muthusamy
 * @version 1.0
 * @since 2015-12-16
 */
public class GmSalesTaxTransBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmSalesTaxTransBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesTaxTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * This method used to validate the Account shiiping address by calilng the Avalara AvaTax URL.
   * URL:https://development.avalara.net/1.0/address/validate?.
   * 
   * @param hmParam - Having the values of Account Shipping Address parameters.
   * @return GmValidateAddressVO - Which contains return Shipping address of account from the AvaTax
   *         webservices.
   * @throws AppError
   * @throws FileNotFoundException
   * @throws IOException
   * @throws JSONException
   * @throws JsonGenerationException
   * @throws JsonMappingException
   * @throws JsonParseException
   */
  public GmValidateAddressVO validateAvaTaxService(HashMap hmParam) throws AppError,
      FileNotFoundException, IOException, JSONException, JsonGenerationException,
      JsonMappingException, JsonParseException {
    GmValidateAddressVO gmValidateAddress = new GmValidateAddressVO();
    GmWSUtil gmWSUtil = new GmWSUtil();

    String strParam = GmCommonClass.parseNull((String) hmParam.get("PARAM"));

    String strServiceURL = GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_URL"));
    String strAccountNumber =
        GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_ACCOUNT"));
    String strLicenseKey =
        GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_LICENSE"));
    String strValidateAddUrl =
        GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_ADDRESS_VALID_URL"));
    URL url;
    HttpURLConnection conn;
    // Connect to specified URL with authorization header
    url = new URL(strServiceURL + strValidateAddUrl + strParam);
    conn = (HttpURLConnection) url.openConnection();
    // log.debug("URL details ::"+strServiceURL + strValidateAddUrl + strParam);
    String strEncoded =
        "Basic "
            + new String(Base64.encodeBase64((strAccountNumber + ":" + strLicenseKey).getBytes()));
    conn.setRequestProperty("Authorization", strEncoded); // Add authorization header
    ObjectMapper mapper = new ObjectMapper();
    mapper.setPropertyNamingStrategy(new GmJsonNameStrategyUtil());
    int intResponseCode = conn.getResponseCode();

    if (conn.getErrorStream() != null) {
      BufferedReader br1 = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
      String strExe = "";
      StringBuffer sbQuery1 = new StringBuffer();
      String output1;
      while ((output1 = br1.readLine()) != null) {
        sbQuery1.append(output1);
      }
      strExe = sbQuery1.toString();
      gmValidateAddress = mapper.readValue(strExe, GmValidateAddressVO.class);
      return gmValidateAddress;
    }
    BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
    String output;
    StringBuffer sbQuery = new StringBuffer();
    while ((output = br.readLine()) != null) {
      sbQuery.append(output);
    }
    // log.debug(" Address Response ::"+sbQuery.toString());
    gmValidateAddress = mapper.readValue(sbQuery.toString(), GmValidateAddressVO.class);
    conn.disconnect();
    return gmValidateAddress;
  }

  /**
   * This method is used to get the Sales Tax value by passing Invoice Id into the GmSalesTaxRptBean
   * bean fetchTaxInvoiceDtls method to get the TaxDate Flag,Country Flag abd Tax Flag and based on
   * these values calling the GmSalesTaxTransBean bean getTaxFromWebService method to get the Sales
   * Tax details.
   * 
   * @param strInvId - Invoice ID which is generated when the Invoice is raised passed into the
   *        GmSalesTaxRptBean bean fetchTaxInvoiceDtls method.
   * @param strUserId - User ID passed into the HashMap hmInvoiceDtls which is passed into the
   *        GmSalesTaxTransBean bean getTaxFromWebService method.
   * @return String
   * @throws AppError
   * @throws ParseException
   */
  public String updateSalesTax(String strInvId, String strUserId) throws AppError {
    GmGetTaxResponseVo gmGetTaxResponseVo = new GmGetTaxResponseVo();
    GmSalesTaxRptBean gmSalesTaxRptBean = new GmSalesTaxRptBean(getGmDataStoreVO());
    String strErrorMessage = "";
    String strResultCode = "";
    HashMap hmInvoiceDtls = new HashMap();
    
    // PC-1000: To capture AvaTax information (to declare local variable)
    
    HashMap hmAvaTaxDtls = new HashMap ();
	String strAvaTaxRequestJsonVal = "";
	String strAvaTaxResponseJsonVal = "";
	String strAvaTaxErrorVal = "";
    
    hmInvoiceDtls = gmSalesTaxRptBean.fetchTaxInvoiceDtls(strInvId);
    hmInvoiceDtls.put("USERID", strUserId);
    String strTaxDateFl = GmCommonClass.parseNull((String) hmInvoiceDtls.get("TAX_DATE_FL"));
    String strTaxCountryFl =
        GmCommonClass.parseNull((String) hmInvoiceDtls.get("TAXABLE_COUNTRY_FL"));
    String strCustPo = GmCommonClass.parseNull((String) hmInvoiceDtls.get("CUSTOMER_PO"));
    String strTaxFl = GmCommonClass.parseNull((String) hmInvoiceDtls.get("TAX_FL"));
    String strParentOrderTaxCost =
        GmCommonClass.parseNull((String) hmInvoiceDtls.get("PARENT_ORD_TAX_COST"));
    String strCompanyAvaTaxFl =
        GmCommonClass.parseNull((String) hmInvoiceDtls.get("COMPANY_AVATAX_FL"));
    log.debug("strCompanyAvaTaxFl ::" + strCompanyAvaTaxFl);
    if (strCompanyAvaTaxFl.equals("Y")) {
      log.debug(" strTaxDateFl:" + strTaxDateFl + " strTaxCountryFl:" + strTaxCountryFl
          + " strTaxFl :" + strTaxFl);
      // to removed the strParentOrderTaxCost condition because if parent order no tax then not
      // called the child orders - invoice
      if (strTaxCountryFl.equals("Y") && strTaxDateFl.equals("Y") && strTaxFl.equals("")) {
        try {
          log.error("Calling AvaTax web service: Invoice Id --> " + strInvId);
          
          // PC-1000: To capture the AvaTax information (request and response) and update to DB.
          hmAvaTaxDtls = getTaxFromWebService(hmInvoiceDtls);
          
          // to get the values
          gmGetTaxResponseVo = (GmGetTaxResponseVo) hmAvaTaxDtls.get("GMGETTAXRESPONSEVO");
          strAvaTaxRequestJsonVal = GmCommonClass.parseNull((String)hmAvaTaxDtls.get("AVATAX_REQUEST"));
          strAvaTaxResponseJsonVal = GmCommonClass.parseNull((String)hmAvaTaxDtls.get("AVATAX_RESPONSE"));
          strAvaTaxErrorVal = GmCommonClass.parseNull((String)hmAvaTaxDtls.get("AVATAX_ERROR"));
                    
          strResultCode = GmCommonClass.parseNull(gmGetTaxResponseVo.getResultCode());
          log.error(" ** Response Code from Avatax ** " + strResultCode);
          if (strResultCode.equalsIgnoreCase("Error")) {
            GmMessagesVO[] mess = gmGetTaxResponseVo.getMessages();
            GmMessagesVO gmMessagesVO = mess[0];
            // to get the error message check
            String strErrorMsg = gmMessagesVO.getSummary();
            String strRuleErrorMsg =
                GmCommonClass.getRuleValueByCompany("AVATAX_ERR_CODE", "AVATAX", getCompId());
            if (!strErrorMsg.equalsIgnoreCase(strRuleErrorMsg)) {
              strErrorMessage = strCustPo;
            }
            log.error(" Error Invoice ID ::" + strInvId);
          }
        } catch (Exception ex) {
        	// PC-1000: To capture the invoice error
        	strAvaTaxErrorVal = ex.toString();
          	voidInvoice(strInvId, strAvaTaxErrorVal, strUserId);
        }
        
        // PC-1000: To capture AvaTax information to our local DB
        updateAvaTaxJSONDetails(strInvId, strAvaTaxRequestJsonVal, strAvaTaxResponseJsonVal, strAvaTaxErrorVal , strUserId);
        
      } // end if
    } // end if company AvaTax flag check
    else { // Avatax flag is null then its call
      // to update tax amount for order line items updateTaxRate method
      updateTaxRate(strInvId, strCustPo, strUserId);
    }
    return strErrorMessage;
  }

  /**
   * This method used to get the Sales Tax by calling the Avalara AvaTax webservice for the Invoice.
   * URL:https://development.avalara.net/1.0/tax/get.
   * 
   * @param hmInvoiceDtls - Having the Invoice Id, Order Id and Account address related details.
   * @return GmGetTaxResponseVo - Having the values of Sales Tax related details like success and
   *         error details.
   * @throws AppError
   * @throws JSONException
   * @throws FileNotFoundException
   * @throws IOException
   */
  public HashMap getTaxFromWebService(HashMap hmInvoiceDtls) throws AppError,
      JSONException, IOException {
	// PC-1000: To capture AvaTax information (declare local variable)
	  HashMap hmReturn = new HashMap ();
	  String strAvaTaxRequestJson = "";
	  String strAvaTaxResponseJson = "";
	  String strAvaTaxError = "";
	  
    GmGetTaxResponseVo gmGetTaxResponseVo = new GmGetTaxResponseVo();
    String strInvoiceId = GmCommonClass.parseNull((String) hmInvoiceDtls.get("INVID"));
    String strUserId = GmCommonClass.parseNull((String) hmInvoiceDtls.get("USERID"));
    // Tax Details
    String strDocType = GmCommonClass.parseNull((String) hmInvoiceDtls.get("DOCTYPE"));
    String strOriginCode = GmCommonClass.parseNull((String) hmInvoiceDtls.get("TAX_COMPANY")); // Globus
                                                                                               // Medical
                                                                                               // North
                                                                                               // America
    //
    String strDocDate = "";
    String strAccountId = "";
    String strDocCode = "";
    String strAccountShipName = "";
    String strCustPO = "";
    // JSON object
    JSONObject taxRequestObj = new JSONObject();
    // JSON Array object
    JSONArray linesArrObj = new JSONArray();
    JSONArray addressArrObj = new JSONArray();
    GmSalesTaxRptBean gmSalesTaxRptBean = new GmSalesTaxRptBean(getGmDataStoreVO());
    //
    strDocDate = GmCommonClass.parseNull((String) hmInvoiceDtls.get("DOCDATE"));
    strAccountId = GmCommonClass.parseNull((String) hmInvoiceDtls.get("ACCID"));
    strDocCode = strInvoiceId;
    strAccountShipName = GmCommonClass.parseNull((String) hmInvoiceDtls.get("ACC_SHIP_NAME"));
    strCustPO = GmCommonClass.parseNull((String) hmInvoiceDtls.get("CUSTOMER_PO"));
    
    // PC-1000 to handle the AvaTax exception and update to local variable..
    try{   
    
	    // Max 50 char
	    if (strAccountShipName.length() > 50) {
	      strAccountShipName = strAccountShipName.substring(0, 48);
	    }
	    
	    // to get the line items
	    linesArrObj = getLineItemsDetails(strInvoiceId, strOriginCode, strAccountShipName);
	    // to get the address details
	    addressArrObj = getAddressDetails(hmInvoiceDtls);
	    // To set the Tax request
	    taxRequestObj.put("CustomerCode", strAccountId);
	    taxRequestObj.put("DocDate", strDocDate);
	    taxRequestObj.put("DocCode", strDocCode);
	    taxRequestObj.put("DocType", strDocType);
	    taxRequestObj.put("Commit", true);
	    taxRequestObj.put("Addresses", addressArrObj);
	    taxRequestObj.put("Lines", linesArrObj);
	    // to get the URL connection details
	    HttpURLConnection connection = getURLConnectionDetails("AVATAX_GET_TAX_URL");
	    //
	    OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
	    log.debug("JSON - request String:" + taxRequestObj.toString());
	    // PC-1000: to capture the avaTax information to local variable
	    strAvaTaxRequestJson = taxRequestObj.toString();
	    wr.write(strAvaTaxRequestJson);
	    wr.flush();
	    wr.close();
	    // to getting the response
	    ObjectMapper mapper = new ObjectMapper();
	    mapper.setPropertyNamingStrategy(new GmJsonNameStrategyUtil());
	    int intResponseCode = connection.getResponseCode();
	    log.debug(" intResponseCode " + intResponseCode);
	    if (connection.getErrorStream() != null) {
	      BufferedReader bufferError =
	          new BufferedReader(new InputStreamReader((connection.getErrorStream())));
	      String strError = "";
	      StringBuffer sbQuery1 = new StringBuffer();
	      String strErrorDtls;
	      while ((strErrorDtls = bufferError.readLine()) != null) {
	        sbQuery1.append(strErrorDtls);
	      }
	      strError = sbQuery1.toString();
	
	      gmGetTaxResponseVo = mapper.readValue(strError, GmGetTaxResponseVo.class);
	      GmMessagesVO[] mess = gmGetTaxResponseVo.getMessages();
	      GmMessagesVO gmMessagesVO = mess[0];
	      String strRuleErrorMsg =
	          GmCommonClass.getRuleValueByCompany("AVATAX_ERR_CODE", "AVATAX", getCompId());
	      log.debug(" strRuleErrorMsg " + strRuleErrorMsg);
	      if (!gmMessagesVO.getSummary().equalsIgnoreCase(strRuleErrorMsg)) {
	        // to call the error method to update the error details
	        getTaxError(gmGetTaxResponseVo, strInvoiceId, strUserId);
	      }
	      // PC-1000: to capture Avatax Error details
	      strAvaTaxError = gmMessagesVO.getSummary();
	      log.debug("AvaTax Error Summary:" + gmMessagesVO.getSummary());
	      
	   // to put all the values to HashMap
	      hmReturn.put("GMGETTAXRESPONSEVO", gmGetTaxResponseVo);
	      hmReturn.put("AVATAX_RESPONSE", strAvaTaxResponseJson);
	      hmReturn.put("AVATAX_REQUEST", strAvaTaxRequestJson);
	      hmReturn.put("AVATAX_ERROR", strAvaTaxError);
	      
	      return hmReturn;
	    }
	    
	    BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
	    String output;
	    StringBuffer sbQuery = new StringBuffer();
	    while ((output = br.readLine()) != null) {
	      sbQuery.append(output);
	    }
	    
	    strAvaTaxResponseJson = sbQuery.toString();
	    log.debug("JSON - Response String: " + strAvaTaxResponseJson);
	    gmGetTaxResponseVo = mapper.readValue(strAvaTaxResponseJson, GmGetTaxResponseVo.class);
	    connection.disconnect();
	    // to call the success method to update sales tax.
	    getTaxSuccess(gmGetTaxResponseVo, strInvoiceId, strUserId);
    
    }catch (Exception ex){
    	// PC-1000: To capture the invoice error
    	strAvaTaxError = ex.toString();
      	voidInvoice(strInvoiceId, strAvaTaxError, strUserId);
    }
    // to put all the values to HashMap
    hmReturn.put("GMGETTAXRESPONSEVO", gmGetTaxResponseVo);
    hmReturn.put("AVATAX_RESPONSE", strAvaTaxResponseJson);
    hmReturn.put("AVATAX_REQUEST", strAvaTaxRequestJson);
    hmReturn.put("AVATAX_ERROR", strAvaTaxError);
    
    return hmReturn;
  }

  /**
   * This method used to update the Sales Tax value in the Order Item table t502_item_order by
   * calling the package gm_pkg_ac_sales_tax procedure gm_upd_order_item_vat_rate.
   * 
   * @param strInvoiceId - Invoice ID which is generated when the Invoice rasied passed as a input
   *        parameter for the package gm_pkg_ac_sales_tax procedure gm_upd_order_item_vat_rate.
   * @param strInputStr - String contains the Order Item and its Vat rate passed as a input
   *        parameter for the package gm_pkg_ac_sales_tax procedure gm_upd_order_item_vat_rate.
   * @throws AppError
   */
  public void updateVatRate(String strInvoiceId, String strInputStr, String strUserId)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ac_sales_tax.gm_upd_order_item_vat_rate", 3);
    gmDBManager.setString(1, strInvoiceId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }


  /**
   * This method used to void the Invoice in the Avalara AvaTax site using the AvaTax webservies by
   * calling the URL. URL:https://development.avalara.net/1.0/tax/cancel.
   * 
   * @param hmAvaTaxDtls - Having the parameters to cancel Invoice.
   * @return GmCancelTaxResponseVo - Having webservices return values.
   * @throws AppError
   * @throws JSONException
   * @throws FileNotFoundException
   * @throws IOException
   */
  public GmCancelTaxResponseVo cancelSalesTax(HashMap hmAvaTaxDtls) throws AppError, JSONException,
      FileNotFoundException, IOException {
    GmCancelTaxResponseVo gmCancelTaxResponseVo = new GmCancelTaxResponseVo();
    String strCancelcode = (String) hmAvaTaxDtls.get("CANCELCODE");
    String strCompanycode = (String) hmAvaTaxDtls.get("TAX_COMPANY");;
    String strDoccode = (String) hmAvaTaxDtls.get("DOCCODE");;
    String strDocid = (String) hmAvaTaxDtls.get("DOCID");;
    String strDoctype = (String) hmAvaTaxDtls.get("DOCTYPE");;

    // to getting the properties information
    String strServiceURL = GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_URL"));
    String strAccountNumber =
        GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_ACCOUNT"));
    String strLicenseKey =
        GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_LICENSE"));
    String strCancelTaxUrl =
        GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_CANCEL_TAX_URL"));
    URL url;
    HttpURLConnection connection = null;
    ObjectOutputStream out;
    url = new URL(strServiceURL + strCancelTaxUrl); // Creating the URL.
    connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setRequestProperty("Content-Type", "application/json");
    connection.setRequestProperty("Accept", "application/json");
    String strEncoded =
        "Basic "
            + new String(Base64.encodeBase64((strAccountNumber + ":" + strLicenseKey).getBytes())); // Create
                                                                                                    // auth
                                                                                                    // content
    connection.setRequestProperty("Authorization", strEncoded); // Add authorization header
    connection.setUseCaches(false);
    connection.setDoInput(true);
    connection.setDoOutput(true);
    // Send request

    JSONObject cancel = new JSONObject();
    cancel.put("CancelCode", strCancelcode);
    cancel.put("CompanyCode", strCompanycode);
    cancel.put("DocCode", strDoccode);
    cancel.put("DocType", strDoctype);

    OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
    wr.write(cancel.toString());
    wr.flush();
    wr.close();
    ObjectMapper mapper = new ObjectMapper();
    mapper.setPropertyNamingStrategy(new GmJsonNameStrategyUtil());
    int intResponseCode = connection.getResponseCode();

    if (connection.getErrorStream() != null) {
      BufferedReader br1 = new BufferedReader(new InputStreamReader((connection.getErrorStream())));
      String strExe = "";
      StringBuffer sbQuery1 = new StringBuffer();
      String output1;
      while ((output1 = br1.readLine()) != null) {
        sbQuery1.append(output1);
      }
      strExe = sbQuery1.toString();
      gmCancelTaxResponseVo = mapper.readValue(strExe, GmCancelTaxResponseVo.class);
      GmCancelTaxResultVo cancelTax = gmCancelTaxResponseVo.getCancelTaxResult();
      GmMessagesVO[] mess = cancelTax.getMessages();
      GmMessagesVO gmMessagesVO = mess[0];
      log.debug(" Error details are " + gmMessagesVO.getSummary());
      return gmCancelTaxResponseVo;
    }
    BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
    String output;
    StringBuffer sbQuery = new StringBuffer();
    while ((output = br.readLine()) != null) {
      sbQuery.append(output);
    }
    gmCancelTaxResponseVo = mapper.readValue(sbQuery.toString(), GmCancelTaxResponseVo.class);
    connection.disconnect();
    return gmCancelTaxResponseVo;
  }

  /**
   * updateBatchSalesTax - This method used to update the sales tax details for batch process for
   * each PO's in the batch.
   * 
   * @author mmuthusamy
   * @param strBatchId - Batch ID which is generated while creating the batch passed as a parameter
   *        into the GmARBatchBean bean fetchBatchInvoiceDtls method to fech the batch details.
   * @param strUserId - User ID to update the Invoice details done by whom.
   * @return String
   * @throws AppError
   * 
   */
  public String updateBatchSalesTax(String strBatchId, String strUserId) throws AppError {
    ArrayList alResult = new ArrayList();
    GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
    alResult = GmCommonClass.parseNullArrayList(gmARBatchBean.fetchBatchInvoiceDtls(strBatchId));
    int intArLength = alResult.size();
    HashMap hmLoop;
    String strInvID;
    String strTempPO = "";
    String strErrorCustPo = "";
    if (intArLength > 0) {
      for (int i = 0; i < intArLength; i++) {
        hmLoop = (HashMap) alResult.get(i);
        strInvID = GmCommonClass.parseNull((String) hmLoop.get("INVID"));
        strTempPO = updateSalesTax(strInvID, strUserId);
        if (!strTempPO.equals("")) {
          strErrorCustPo = strErrorCustPo + strTempPO + ", ";
        }
      }
    }
    return strErrorCustPo;

  }

  /**
   * This method used to update the Sales Tax details for the Invoice by forming the string which
   * contains the Order Item Id and Vat rate
   * 
   * @param gmGetTaxResponseVo - Having the Sales Tax code and rate details.
   * @param strInvoiceId - Invoice ID which is generated when the Invoice raised passed as input
   *        parameter in GmSalesTaxTransBean bean updateVatRate method to update the Sales Tax
   *        details for the Invoice
   * @param strUserId - User ID to update the Invoice details done by whom.
   * @throws AppError
   */
  private void getTaxSuccess(GmGetTaxResponseVo gmGetTaxResponseVo, String strInvoiceId,
      String strUserId) throws AppError {
    GmGetTaxLinesVo gmGetTaxLinesVo;
    GmGetTaxLinesVo[] taxLines = gmGetTaxResponseVo.getTaxLines();
    String strItemOrdId = "";
    String strTaxRate = "";
    String strTax = "";
    String strTaxCalculated = "";
    Double dbVatRate;
    String strOrderItemInputStr = "";
    for (int i = 0; i < (taxLines.length); i++) {
      gmGetTaxLinesVo = new GmGetTaxLinesVo();
      dbVatRate = 0.0;
      gmGetTaxLinesVo = taxLines[i];
      strItemOrdId = GmCommonClass.parseNull(gmGetTaxLinesVo.getLineNo());
      strTaxRate = GmCommonClass.parseZero(gmGetTaxLinesVo.getRate());
      strTax = GmCommonClass.parseNull(gmGetTaxLinesVo.getTax());
      strTaxCalculated = GmCommonClass.parseZero(gmGetTaxLinesVo.getTax());
      if (strTax.equals("0")) {
        strTaxRate = "0";
      }
      dbVatRate = Double.parseDouble(strTaxRate) * 100;
      log.debug(" Tax Code ::" + gmGetTaxLinesVo.getTaxCode() + " ::Total tax rate amount ::"
          + dbVatRate + " strTaxCalculated ::" + strTaxCalculated);
      if (!strItemOrdId.equals(""))
        strOrderItemInputStr =
            strOrderItemInputStr + strItemOrdId + "^" + dbVatRate + "^" + strTaxCalculated + "|";
    }
    if (strOrderItemInputStr != "") {
      updateVatRate(strInvoiceId, strOrderItemInputStr, strUserId);
    }

  }

  /**
   * This method used to get the Error message from the Avalara AvaTax webservices to void the
   * Invoice.
   * 
   * @param gmGetTaxResponseVo - Having the Webservice error details.
   * @param strInvoiceId - Invoice ID which is generated when the Invoice raised passed as input
   *        parameter in GmSalesTaxTransBean bean voidTaxErrorInvoice method to void the Invoice.
   * @param strUserId - User ID to update the Invoice details voided by whom.
   * @throws AppError
   * @throws IOException
   * @throws JSONException
   */
  private void getTaxError(GmGetTaxResponseVo gmGetTaxResponseVo, String strInvoiceId,
      String strUserId) throws AppError, JSONException, IOException {
    GmMessagesVO[] mess = gmGetTaxResponseVo.getMessages();
    String strErrorMessage = "";
    GmMessagesVO gmMessagesVO = mess[0];
    strErrorMessage = gmMessagesVO.getSummary();
    // to void the Invoice to AvaTax
    GmSalesTaxRptBean gmSalesTaxRptBean = new GmSalesTaxRptBean(getGmDataStoreVO());
    HashMap hmAvaTaxParam = new HashMap();
    hmAvaTaxParam = gmSalesTaxRptBean.fetchTaxInvoiceDtls(strInvoiceId);
    String strInvTaxFl = GmCommonClass.parseNull((String) hmAvaTaxParam.get("TAX_FL"));
    // Already present in AvaTax then void the data
    if (strInvTaxFl.equals("Y")) {
      hmAvaTaxParam.put("CANCELCODE", "DocVoided");
      hmAvaTaxParam.put("DOCCODE", strInvoiceId); // Invoice number
      cancelSalesTax(hmAvaTaxParam);
    }

    voidInvoice(strInvoiceId, strErrorMessage, strUserId);
    log.debug(" Error Summary " + strErrorMessage);
  }

  /**
   * This method is used to void the Invoice which as the Error.
   * 
   * @param strInvoiceId - Invoice ID which is generated when the Invoice raised passed as input
   *        parameter in package gm_pkg_ac_sales_tax procedure gm_void_tax_error_invoice to void the
   *        Invoice.
   * @param strErrorComments - Reason for the voiding the Invoice.
   * @param strUserId - User ID
   * @throws AppError
   */
  public void voidInvoice(String strInvoiceId, String strErrorComments, String strUserId)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ac_sales_tax.gm_void_tax_error_invoice", 3);
    gmDBManager.setString(1, strInvoiceId);
    gmDBManager.setString(2, strErrorComments);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * rollBackReturns - This method used to roll back the pending return details.
   * 
   * @param strRAId - Return id - based on this update the invoice to be null
   * @param strUserId - User id - updated by use
   * @throws AppError
   */
  public void rollBackReturns(String strRAId, String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ac_sales_tax.gm_rollback_return", 2);
    gmDBManager.setString(1, strRAId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * @param strInputStr
   * @param strUserId
   * @throws AppError
   */
  public void updateTempShipAddress(String strInputStr, String strUserId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ac_sales_tax.gm_upd_temp_ship_info", 2);
    gmDBManager.setString(1, strInputStr);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * getLineItemsDetails - This method used loop each line items and set to Object.
   * 
   * @param strInvoiceId - Invoice ID
   * @param strOriginCode - Company Code
   * @param strAccountName - Account name
   * @throws AppError
   * @throws JSONException
   */
  private JSONArray getLineItemsDetails(String strInvoiceId, String strOriginCode,
      String strAccountName) throws AppError, JSONException {
    JSONArray linesArrObj = new JSONArray();
    HashMap hmCartDtls = new HashMap();
    ArrayList alOrderNums = new ArrayList();
    GmARBean gmARBean = new GmARBean(getGmDataStoreVO());
    GmSalesTaxRptBean gmSalesTaxRptBean = new GmSalesTaxRptBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    // to get the all the order list
    alOrderNums = gmDBManager.queryMultipleRecords(gmARBean.loadInvoiceSQL(strInvoiceId, ""));
    String strOrdId = "";
    HashMap hmTemp;
    ArrayList alLoop = new ArrayList();
    HashMap hmResult = new HashMap();
    // Below Code will fetch the Order Item details for the selected
    // customer invoice
    for (int i = 0; i < alOrderNums.size(); i++) {
      hmTemp = new HashMap();
      hmTemp = (HashMap) alOrderNums.get(i);
      strOrdId = (String) hmTemp.get("ID");
      alLoop = gmSalesTaxRptBean.loadOrderItems(strOrdId);
      hmCartDtls.put(strOrdId, alLoop);
    }
    int intSize = alOrderNums.size();
    HashMap hmLoop = new HashMap();

    String strItemOrdId = "";
    String strPartNum = "";
    String strDesc = "";
    String strPrice = "";
    String strPartQty = "";
    String strTaxCode = "";
    String strItemCode = "";

    JSONObject lineObj;

    for (int i = 0; i < intSize; i++) {
      hmTemp = (HashMap) alOrderNums.get(i);
      strOrdId = (String) hmTemp.get("ID");
      alLoop = (ArrayList) hmCartDtls.get(strOrdId);
      int intLoop = alLoop.size();
      for (int j = 0; j < intLoop; j++) {
        hmLoop = (HashMap) alLoop.get(j);
        strItemOrdId = (String) hmLoop.get("ITEM_ORDER_ID");
        strPartNum = (String) hmLoop.get("PNUM");
        strDesc = (String) hmLoop.get("PDESC");
        strPrice = (String) hmLoop.get("PRICE");
        strPartQty = (String) hmLoop.get("QTY");
        strTaxCode = (String) hmLoop.get("TAXCODE");
        strItemCode = (String) hmLoop.get("ITEMCODE");
        double dbTotalAmt = Double.parseDouble(strPrice) * Double.parseDouble(strPartQty);
        lineObj = new JSONObject();
        lineObj.put("LineNo", strItemOrdId);
        lineObj.put("ItemCode", strItemCode);
        lineObj.put("TaxCode", strTaxCode);
        lineObj.put("Qty", Math.abs(Integer.parseInt(strPartQty)));
        lineObj.put("Amount", dbTotalAmt);
        lineObj.put("OriginCode", strOriginCode);
        lineObj.put("DestinationCode", strAccountName);
        // lineObj.put("Description", strDesc);
        // Added to Line Array
        linesArrObj.put(lineObj);
      }
    }
    return linesArrObj;
  }

  /**
   * getAddressDetails - This method used set the Company and account address.
   * 
   * @param hmInvoiceDtls - Invoice details - hash map
   * @throws AppError
   * @throws JSONException
   */
  private JSONArray getAddressDetails(HashMap hmInvoiceDtls) throws AppError, JSONException {
    JSONArray addressArrObj = new JSONArray();
    JSONObject addressObj = new JSONObject();
    // to get the values from hash map
    String strLine1 = GmCommonClass.parseNull((String) hmInvoiceDtls.get("ACC_SHIP_ADD1"));
    String strLine2 = GmCommonClass.parseNull((String) hmInvoiceDtls.get("ACC_SHIP_ADD2"));
    String strLine3 = "";
    String strCity = GmCommonClass.parseNull((String) hmInvoiceDtls.get("ACC_SHIP_CITY"));
    String strRegion = GmCommonClass.parseNull((String) hmInvoiceDtls.get("ACC_SHIP_STATE_NM"));
    String strPostalCode = GmCommonClass.parseNull((String) hmInvoiceDtls.get("ACC_SHIP_ZIP_CODE"));
    //
    String strAccountShipName =
        GmCommonClass.parseNull((String) hmInvoiceDtls.get("ACC_SHIP_NAME"));
    // Max 50 char
    if (strAccountShipName.length() > 50) {
      strAccountShipName = strAccountShipName.substring(0, 48);
    }

    // Globus Medical Address
    addressObj.put("AddressCode", "GMNA");
    addressObj.put("Line1", "2560 General Armistead Avenue");
    addressObj.put("PostalCode", "19403");
    // Added to address Array (Origin Code)
    addressArrObj.put(addressObj);
    // Destination code set
    addressObj = new JSONObject();
    addressObj.put("AddressCode", strAccountShipName);
    addressObj.put("Line1", strLine1);
    addressObj.put("Line2", strLine2);
    addressObj.put("Line3", strLine3);
    addressObj.put("City", strCity);
    addressObj.put("Region", strRegion);
    if (!strPostalCode.equals("") && !strPostalCode.equals("-")) {
      addressObj.put("PostalCode", strPostalCode);
    }
    // Added to address Array (Destination code)
    addressArrObj.put(addressObj);
    return addressArrObj;
  }

  /**
   * getURLConnectionDetails - This method used setting the URL connection details.
   * 
   * @param strURL - Target URL passing as the string
   * @throws AppError
   * @throws IOException
   */
  private HttpURLConnection getURLConnectionDetails(String strURL) throws AppError, IOException {
    HttpURLConnection connection = null;
    // to getting the properties information
    String strServiceURL = GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_URL"));
    String strAccountNumber =
        GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_ACCOUNT"));
    String strLicenseKey =
        GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_LICENSE"));
    String strGetTaxUrl = GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig(strURL));
    // to call the Web service
    URL url;
    ObjectOutputStream out;
    url = new URL(strServiceURL + strGetTaxUrl); // Creating the URL.
    connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setRequestProperty("Content-Type", "application/json");
    connection.setRequestProperty("Accept", "application/json");
    String strEncoded =
        "Basic "
            + new String(Base64.encodeBase64((strAccountNumber + ":" + strLicenseKey).getBytes())); // Create
                                                                                                    // auth
                                                                                                    // content
    connection.setRequestProperty("Authorization", strEncoded); // Add authorization header
    connection.setUseCaches(false);
    connection.setDoInput(true);
    connection.setDoOutput(true);
    return connection;
  }

  /**
   * updateTaxRate - This method used to update tax rate to order line items
   * 
   * @param strInvoiceId,strCustPo,strUserId
   * @throws AppError
   * @throws AppError
   */
  public void updateTaxRate(String strInvoiceId, String strCustPo, String strUserId)
      throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_invoice_tax.gm_sav_taxrate_by_invoice", 3);
    gmDBManager.setString(1, strInvoiceId);
    gmDBManager.setString(2, strCustPo);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }
  
	/**
	 * updateAvaTaxJSONDetails - this method used to store the AvaTax details
	 * (Request and response details) to our DB.
	 * 
	 * AvaTax is the third party web
	 * service call. Some times its failed. Currently we don't know why its
	 * failed. So we need to track all the invoices (Request and response
	 * details).
	 * 
	 * @param strInvoiceId
	 * @param strAvaTaxReqJson
	 * @param strAvaTaxRespJson
	 * @param strAvaTaxError
	 * @param strUserId
	 * @throws AppError
	 */
	private void updateAvaTaxJSONDetails(String strInvoiceId,
			String strAvaTaxRequestJson, String strAvaTaxResponseJson,
			String strAvaTaxError, String strUserId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString(
				"gm_pkg_ac_invoice_tax.gm_sav_inv_avatax_json_dtl", 5);
		gmDBManager.setString(1, strInvoiceId);
		gmDBManager.setString(2, strAvaTaxRequestJson);
		gmDBManager.setString(3, strAvaTaxResponseJson);
		gmDBManager.setString(4, strAvaTaxError);
		gmDBManager.setString(5, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();

	}

}
