package com.globus.accounts.tax.beans;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * <h1>GmSalesTaxRptBean has all the new methods to calculate tax for the US Countries by calling
 * the Avalara AvaTax URL. URL:https://development.avalara.net</h1>
 * 
 * @author Manikandan Muthusamy
 * @version 1.0
 * @since 2015-12-16
 */


public class GmSalesTaxRptBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  public static ResourceBundle rbAvataxConfig;

  public GmSalesTaxRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSalesTaxRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * This method is used to load Item Order details by passing the Order ID into the package
   * gm_pkg_ac_sales_tax procedure gm_fch_order_item_dtls
   * 
   * @param strOrderId - Item Order Id which is generated when the order is raised as passed as a
   *        input parameter into the package gm_pkg_ac_sales_tax procedure gm_fch_order_item_dtls to
   *        get the order details.
   * @return ArrayList
   * @throws AppError
   * @see AppError
   */
  public ArrayList loadOrderItems(String strOrderId) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_sales_tax.gm_fch_order_item_dtls", 2);
    gmDBManager.setString(1, strOrderId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug(" alResult size " + alResult.size());
    return alResult;
  }


  /**
   * This method is used to fetch the Invoice tax flag value based on Invoice ID which is passed
   * into the package gm_pkg_ac_sales_tax function get_invoice_tax_fl.
   * 
   * @param strInvoiceId - Invoice ID which is generated when the Invoice is raised as passed as a
   *        input parameter for the package gm_pkg_ac_sales_tax function get_invoice_tax_fl.
   * @return String
   * @throws AppError
   */
  public String fetchInvoiceTaxFlag(String strInvoiceId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strInvTaxFl = "";
    gmDBManager.setFunctionString("gm_pkg_ac_sales_tax.get_invoice_tax_fl", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strInvoiceId);
    gmDBManager.execute();
    strInvTaxFl = GmCommonClass.parseNull(gmDBManager.getString(1));
    log.debug("Invoice Tax flag   " + strInvTaxFl);
    gmDBManager.close();
    return strInvTaxFl;
  }

  /**
   * This method returns a string value from the avatax Properties file based on the input as a
   * keyword
   * 
   * @param strKey - Keyword to fetch the property value from the Avatax property.
   * @return String
   */

  public static String getAvataxConfig(String strKey) {
    if (rbAvataxConfig == null) {
      rbAvataxConfig = ResourceBundle.getBundle(System.getProperty("ENV_AVAT_CONF"));
    }
    return rbAvataxConfig.getString(strKey);
  }

  /**
   * This method is used to fetch invoice tax details by passing invoice id into the package
   * gm_pkg_ac_sales_tax procedure gm_fch_tax_invoice_dtls
   * 
   * @param strInvoiceId - Invoice ID which is generated when the Invoice is raised as passed as a
   *        input parameter for the package gm_pkg_ac_sales_tax procedure gm_fch_tax_invoice_dtls.
   * @return HashMap
   * @throws AppError
   */

  public HashMap fetchTaxInvoiceDtls(String strInvoiceId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_sales_tax.gm_fch_tax_invoice_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strInvoiceId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * This method is used to get code id by passing code name alt and code group into the package
   * gm_pkg_ac_sales_tax function get_code_id_from_code_alt..
   * 
   * @param strCodeGrp - Group name in t901_code_lookup table for which we need code ID as passed as
   *        input parameter in package gm_pkg_ac_sales_tax function get_code_id_from_code_alt.
   * @param strCodeAlt - Alternate name in t901_code_lookup table for which we need code ID as
   *        passed as input parameter in package gm_pkg_ac_sales_tax function
   *        get_code_id_from_code_alt.
   * @return String
   * @throws AppError
   */
  public String fetchCodeIdFromCodeGrpAndCodeAlt(String strCodeGrp, String strCodeAlt)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCodeId = "";
    gmDBManager.setFunctionString("gm_pkg_ac_sales_tax.get_code_id_from_code_alt", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strCodeGrp);
    gmDBManager.setString(3, strCodeAlt);
    gmDBManager.execute();
    strCodeId = GmCommonClass.parseNull(gmDBManager.getString(1));
    // log.debug("Code Id is " + strCodeId);
    gmDBManager.close();
    return strCodeId;
  }

  /**
   * This method is used to check the URL is active, URL:https://development.avalara.net
   * 
   * @throws AppError, IOException
   * @see IOException
   */
  public void checkTaxWebsiteAvailability() throws AppError, IOException {
    String strServiceURL = GmCommonClass.parseNull(GmSalesTaxRptBean.getAvataxConfig("AVATAX_URL"));
    URL url = new URL(strServiceURL);
    HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
    httpConn.setInstanceFollowRedirects(false);
    httpConn.setRequestMethod("HEAD");
    try {
      httpConn.connect();
    } catch (Exception ex) {
      log.debug("Website Down Error code::" + httpConn.getResponseCode());
      httpConn.disconnect();
      throw new AppError("", "20667", 'E');
    }
    httpConn.disconnect();
  }

  /**
   * @return
   * @throws AppError
   */
  public ArrayList fetchUsAcccountList() throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_sales_tax.gm_fch_account_dtls", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alReturn;
  }

}
