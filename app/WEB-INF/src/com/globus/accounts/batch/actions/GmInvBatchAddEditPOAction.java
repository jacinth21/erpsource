package com.globus.accounts.batch.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.batch.forms.GmInvBatchAddEditPOForm;
import com.globus.accounts.beans.GmAcctDashboardBean;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.corporate.beans.GmCompanyBean;

public class GmInvBatchAddEditPOAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code
  String strAccountCurrId = "";
  String strCompanyCurrId = "";
  String strAccCurrName = "";
  HashMap hmCurrency = new HashMap();
  ArrayList alCompanyCurrency = new ArrayList();

  /**
   * invBatchAddPOs
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward invBatchAddPOs(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmInvBatchAddEditPOForm gmInvBatchAddEditPOForm = (GmInvBatchAddEditPOForm) actionForm;
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
    GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
    GmBatchBean gmBatchBean = new GmBatchBean(getGmDataStoreVO());
    GmAcctDashboardBean gmAcctDashboardBean = new GmAcctDashboardBean(getGmDataStoreVO());


    HashMap hmParam = new HashMap();
    ArrayList alResult = new ArrayList();
    String strForward = "GmInvBatchAddEditPO";
    String strOpt = "";
    String strInvBatchId = "";
    String strAccountIds = "";
    String strBatchType = "";

    gmInvBatchAddEditPOForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmInvBatchAddEditPOForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strInvBatchId = GmCommonClass.parseNull((String) hmParam.get("INVBATCHID"));
    strBatchType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    strAccountIds = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));

    alCompanyCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());
    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    strCompanyCurrId = GmCommonClass.parseNull((String) hmCurrency.get("TXNCURRENCYID"));
    strAccountCurrId = GmCommonClass.parseZero((String) hmParam.get("STRCOMPCURRENCY"));
    strAccountCurrId = strAccountCurrId.equals("0") ? strCompanyCurrId : strAccountCurrId;
    strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrId);
    // overriding the currency symbol - used for VM file
    hmParam.put("APPLNCURRSIGN", strAccCurrName);
    if (strOpt.equals("ADDPOS")) { // To create a new invoice batch
      hmParam.put("BATCHTYPE", strBatchType); // Invoice Batch
      hmParam.put("BATCHTID", strInvBatchId); // Invoice Batch ID
      hmParam.put("BATCHSTATUS", "18741"); // Batch Status - In Progress
      if (strInvBatchId.equals("")) {
        strInvBatchId = gmBatchBean.saveBatch(hmParam);
      }
      gmInvBatchAddEditPOForm.setInvBatchID(strInvBatchId);
      gmInvBatchAddEditPOForm.setInvBatchStatus("18741");
      gmInvBatchAddEditPOForm.setStrAccountIDs(strAccountIds);
    } else if (strOpt.equals("SaveBatch")) {
      // to set the redirectURL
      String strRedirectURL = GmCommonClass.parseNull(request.getParameter("hRedirectURL"));;
      request.setAttribute("hRedirectURL", strRedirectURL);
      gmARBatchBean.saveInvoiceBatch(hmParam);
      gmInvBatchAddEditPOForm.setScreenType("SavePOs");
      // In Vm files not show the check box.
      hmParam.put("SCREENTYPE", "SavePOs");
      gmInvBatchAddEditPOForm.setInvBatchStatus("18742"); // Batch Status - Pending Processing
    }

    if (strOpt.equals("ADDPOS")) {
      alResult =
          GmCommonClass.parseNullArrayList((ArrayList) gmARBatchBean.fetchNonBatchPOs(hmParam));
    } else {
      alResult = GmCommonClass.parseNullArrayList(gmARBatchBean.fetchInvBatchDet(hmParam));
    }

    // to set the template name and Path
    hmParam.put("TEMPNAME", "GmInvBatchAddEditPO.vm");
    hmParam.put("TEMPPATH", "accounts/batch/templates");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    String strXmlData = gmARBatchBean.getXmlGridData(alResult, hmParam);
    gmInvBatchAddEditPOForm.setGridData(strXmlData);
    gmInvBatchAddEditPOForm.setAlCompCurrency(alCompanyCurrency);
    gmInvBatchAddEditPOForm.setStrCompCurrency(strAccountCurrId);

    return actionMapping.findForward(strForward);
  }

  /**
   * invBatchEditPOs
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward invBatchEditPOs(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmInvBatchAddEditPOForm gmInvBatchAddEditPOForm = (GmInvBatchAddEditPOForm) actionForm;
    // IF the value returned by JMS_CLIENT_TO_USE is JBOSS then its a remote connection else if
    // WEBLOGIC is returned same JVM connetion
    String strBatchProcessService =
        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_PROCESS_SERVICE"));
    String strBatchMDBClass =
        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_CONSUMER_MDB_CLASS"));
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
    GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
    GmBatchBean gmBatchBean = new GmBatchBean(getGmDataStoreVO());
    GmAcctDashboardBean gmAcctDashboardBean = new GmAcctDashboardBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    ArrayList alResult = new ArrayList();
    String strForward = "GmInvBatchAddEditPO";
    String strOpt = "";
    String strInvBatchId = "";
    String strInvBatchType = "";
    String strInvStatus = "";
    int intInvStatus;

    gmInvBatchAddEditPOForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmInvBatchAddEditPOForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strInvBatchId = GmCommonClass.parseNull((String) hmParam.get("INVBATCHID"));
    strInvBatchType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
    strInvStatus = GmCommonClass.parseZero((String) hmParam.get("INVBATCHSTATUS"));
    String strErrorCustPOs = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmResourceBundleBean gmResourceBundleBeanlbl =
        GmCommonClass.getResourceBundleBean(
            "properties.labels.accounts.batch.GmInvBatchAddAccount", strSessCompanyLocale);
    gmInvBatchAddEditPOForm.setStrSuccessMsg("");
    gmInvBatchAddEditPOForm.setStrErrorCustPos("");

    alCompanyCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());
    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    strCompanyCurrId = GmCommonClass.parseNull((String) hmCurrency.get("TXNCURRENCYID"));
    strAccountCurrId = GmCommonClass.parseZero((String) hmParam.get("STRCOMPCURRENCY"));
    strAccountCurrId = strAccountCurrId.equals("0") ? strCompanyCurrId : strAccountCurrId;
    strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrId);
    // overriding the currency symbol - used for VM file
    hmParam.put("APPLNCURRSIGN", strAccCurrName);
    log.error("hmParam ------> " + hmParam);
    if (strOpt.equals("ProcessBatch")) {
      hmParam.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
      strErrorCustPOs =
          gmARBatchBean.saveProcessInvoice(hmParam, strBatchProcessService, strBatchMDBClass);
      log.error("strErrorCustPOs ProcessBatch ------> " + strErrorCustPOs);
      if (!strErrorCustPOs.equals("")) {
        log.debug(" Error POs::" + strErrorCustPOs);
        strErrorCustPOs = strErrorCustPOs.substring(0, strErrorCustPOs.length() - 2);
      }
      gmInvBatchAddEditPOForm.setScreenType("ProcessedPOs");
      hmParam.put("SCREENTYPE", "ProcessedPOs");
      if (strInvStatus.equals("18742")) { // Pending Progress
        String lblBatchProccInProgress =
            GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_BATCH_IN_PROGRESS"));
        gmInvBatchAddEditPOForm.setStrSuccessMsg(lblBatchProccInProgress);
      }
    }

    HashMap hmBatchDtl = new HashMap();
    hmParam.put("BATCHID", strInvBatchId);
    hmBatchDtl = gmBatchBean.fetchBatchHeader(hmParam);
    strInvStatus = GmCommonClass.parseZero((String) hmBatchDtl.get("BATCHSTATUS"));
    gmInvBatchAddEditPOForm.setInvBatchStatus(strInvStatus);

    if (strInvBatchType.equals("18751") || strInvBatchType.equals("18756")) { // Invoice Batch

      if (strInvBatchType.equals("18756")) {
        alResult = GmCommonClass.parseNullArrayList(gmARBatchBean.fetchDealerInvBatchDtls(hmParam));
      } else {
        alResult = GmCommonClass.parseNullArrayList(gmARBatchBean.fetchInvBatchDet(hmParam));
      }
      // to set the screen type based on the status.
      intInvStatus = Integer.parseInt(strInvStatus);
      if (intInvStatus == 18741) { // 18741 - In Progress
        gmInvBatchAddEditPOForm.setScreenType("AddPOs");
      } else if (intInvStatus >= 18743) { // 18743 - Processing In Progress)
        gmInvBatchAddEditPOForm.setScreenType("ProcessedPOs");
      }
      // to set the template name and Path
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmParam.put("TEMPNAME", "GmInvBatchAddEditPO.vm");
      hmParam.put("TEMPPATH", "accounts/batch/templates");
    } else if (strInvBatchType.equals("18752")) { // DO Summay Batch
      hmParam.put("BATCHTYPE", strInvBatchType);
      alResult = GmCommonClass.parseNullArrayList(gmARBatchBean.fetchDOSummaryBatchDtls(hmParam));
      // to set the template name and Path
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmParam.put("TEMPNAME", "GmDOPrintBatchAdd.vm");
      hmParam.put("TEMPPATH", "accounts/batch/templates");
    }

    // to get the customer currency symbol
    if (alResult.size() > 0) {
      HashMap hmTemp = new HashMap();
      hmTemp = (HashMap) alResult.get(0);
      hmParam.put("APPLNCURRSIGN", GmCommonClass.parseNull((String) hmTemp.get("ACC_CURR_NM")));
    }
    String strXmlData = gmARBatchBean.getXmlGridData(alResult, hmParam);
    gmInvBatchAddEditPOForm.setGridData(strXmlData);
    gmInvBatchAddEditPOForm.setStrErrorCustPos(strErrorCustPOs);
    gmInvBatchAddEditPOForm.setAlCompCurrency(alCompanyCurrency);
    gmInvBatchAddEditPOForm.setStrCompCurrency(strAccountCurrId);
    return actionMapping.findForward(strForward);
  }

}
