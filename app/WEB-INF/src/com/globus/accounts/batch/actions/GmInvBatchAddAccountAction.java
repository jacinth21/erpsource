package com.globus.accounts.batch.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.batch.forms.GmInvBatchAddAccountForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmInvBatchAddAccountAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Logger code

  /**
   * loadInvBatchAddAccount
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward loadInvBatchAddAccount(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmInvBatchAddAccountForm gmInvBatchAddAccountForm = (GmInvBatchAddAccountForm) actionForm;
    GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    ArrayList alResult = new ArrayList();
    String strForward = "GmInvBatchAddAccount";
    String strOpt = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    gmInvBatchAddAccountForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmInvBatchAddAccountForm);

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    // fetch the Pending POs
    alResult = GmCommonClass.parseNullArrayList(gmARBatchBean.fetchAccountPendingPOs());
    // to set the template name and Path
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

    hmParam.put("TEMPNAME", "GmInvBatchAddAccount.vm");
    hmParam.put("TEMPPATH", "accounts/batch/templates");

    String strXmlData = gmARBatchBean.getXmlGridData(alResult, hmParam);
    gmInvBatchAddAccountForm.setGridData(strXmlData);
    return actionMapping.findForward(strForward);
  }

}
