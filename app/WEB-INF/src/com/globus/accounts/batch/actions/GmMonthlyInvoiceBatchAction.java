package com.globus.accounts.batch.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.batch.beans.GmARBatchSplitBean;
import com.globus.accounts.batch.beans.GmMonthlyInvoiceBatchRptBean;
import com.globus.accounts.batch.beans.GmMonthlyInvoiceBatchTransBean;
import com.globus.accounts.batch.forms.GmMonthlyInvoiceBatchForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;



public class GmMonthlyInvoiceBatchAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadMonthlyInvoiceBatch--This method is used to load the invoice monthly batch records
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward loadMonthlyInvoiceBatch(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmMonthlyInvoiceBatchForm gmMonthlyInvoiceBatchForm = (GmMonthlyInvoiceBatchForm) form;
    gmMonthlyInvoiceBatchForm.loadSessionParameters(request);
    GmMonthlyInvoiceBatchRptBean gmMonthlyInvoiceBatchRptBean =
        new GmMonthlyInvoiceBatchRptBean(getGmDataStoreVO());
    GmMonthlyInvoiceBatchTransBean gmMonthlyInvoiceBatchTransBean =
        new GmMonthlyInvoiceBatchTransBean(getGmDataStoreVO());
    GmBatchBean gmBatchBean = new GmBatchBean(getGmDataStoreVO());
    GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmARBatchSplitBean gmARBatchSplitBean = new GmARBatchSplitBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alResult = new ArrayList();
    String strPOInputStr = "";
    String strAccInputStr = "";
    String strDealerInputStr = "";
    String strUserID = "";
    String strDealerInvBatchId = "";
    String strAccInvBatchId = "";
    String strBatchId = "";
    String strAccessFlag = "";
    String strUserId = "";
    String strCompanyId = "";
    String strSplitBatchFl = "";
    String strCustomerId = "";
    String strOpt = GmCommonClass.parseNull(gmMonthlyInvoiceBatchForm.getStrOpt());
    String strPartyType = GmCommonClass.parseNull(gmMonthlyInvoiceBatchForm.getPartyType());
    

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    hmParam = GmCommonClass.getHashMapFromForm(gmMonthlyInvoiceBatchForm);

    gmMonthlyInvoiceBatchForm.setAlBillingCustomer(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("INVCST", getGmDataStoreVO())));
    gmMonthlyInvoiceBatchForm.setAlInvoiceClosingDate(GmCommonClass
        .parseNullArrayList(GmCommonClass.getCodeList("INCLDT", getGmDataStoreVO())));
    gmMonthlyInvoiceBatchForm.setPartyType(strPartyType);

    gmMonthlyInvoiceBatchForm.setAlNameList(gmMonthlyInvoiceBatchRptBean.loadNameList(hmParam));
    gmMonthlyInvoiceBatchForm.setAlTransactionType(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("MINVTY", getGmDataStoreVO())));
    
    // Get the access to edit the invoice closing date range for Monthly Invoice Batch Screen 
    strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "MONTHINV_DT_EDIT_ACS"));
    strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmMonthlyInvoiceBatchForm.setStrAccessFlag(strAccessFlag);
    if (strOpt.equals("Reload")) {

      alResult = gmMonthlyInvoiceBatchRptBean.loadMonthlyInvoiceBatch(hmParam);

      hmParam.put("ALRESULT", alResult);
      hmParam.put("VMFILE", "GmMonthlyInvoiceBatch.vm");
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      String strGridData = generateOutPut(hmParam);
      gmMonthlyInvoiceBatchForm.setGridData(strGridData);
    }
    hmParam = GmCommonClass.getHashMapFromForm(gmMonthlyInvoiceBatchForm);
    if (strOpt.equals("save")) {



      strPOInputStr = GmCommonClass.parseNull((String) hmParam.get("POINPUTSTR"));
      strAccInputStr = GmCommonClass.parseNull((String) hmParam.get("ACCINPUTSTR"));
      strDealerInputStr = GmCommonClass.parseNull((String) hmParam.get("DEALERINPUTSTR"));
      strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));

      if (!strPOInputStr.equals("")) {
        gmMonthlyInvoiceBatchTransBean.saveMonthlyInvPO(strPOInputStr, strUserID);
      }

      // Creating batch for dealer
      if (!strDealerInputStr.equals("")) {

        hmParam.put("BATCHTYPE", "18756"); // Invoice Batch - Dealer
        hmParam.put("BATCHTID", strDealerInvBatchId); // Dealer Invoice Batch ID
        hmParam.put("BATCHSTATUS", "18741"); // Batch Status - In Progress

        if (strDealerInvBatchId.equals("")) {
          strDealerInvBatchId = gmBatchBean.saveBatch(hmParam);
          strBatchId = strDealerInvBatchId;

        }
        hmParam.put("INVBATCHID", strDealerInvBatchId);
        hmParam.put("INPUTSTRING", strDealerInputStr);
        gmARBatchBean.saveInvoiceBatch(hmParam);
      }

      //Split the batch id for bulk dealers, for japan
      
      if(!strDealerInputStr.equals("")){
    	  strBatchId = gmARBatchSplitBean.saveMultiSplitBatch(strDealerInvBatchId,strUserId); 
      }
      
      // Creating batch for Account
      if (!strAccInputStr.equals("")) {

        hmParam.put("BATCHTYPE", "18751"); // Invoice Batch - Account
        hmParam.put("BATCHTID", strAccInvBatchId); // Dealer Invoice Batch ID
        hmParam.put("BATCHSTATUS", "18741"); // Batch Status - In Progress

        if (strAccInvBatchId.equals("")) {
          strAccInvBatchId = gmBatchBean.saveBatch(hmParam);
	          if(!strBatchId.equals("")){
	        	  strBatchId = strBatchId + " , " + strAccInvBatchId;
	          }else{
	        	  strBatchId = strAccInvBatchId;
	          }

        }
        hmParam.put("INVBATCHID", strAccInvBatchId);
        hmParam.put("INPUTSTRING", strAccInputStr);
        gmARBatchBean.saveInvoiceBatch(hmParam);
      }
      
      gmMonthlyInvoiceBatchForm.setStrBatchId(strBatchId);



      hmParam.put("ALRESULT", alResult);
      hmParam.put("VMFILE", "GmMonthlyInvoiceBatch.vm");
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      String strGridData = generateOutPut(hmParam);
      gmMonthlyInvoiceBatchForm.setGridData(strGridData);


    }

    return mapping.findForward("GmMonthlyInvoiceBatch");
  }

  /**
   * loadNameList--This method is used to load Dealer Name and Accounts
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward loadNameList(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strJSON = "";
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    instantiate(request, response);
    GmMonthlyInvoiceBatchForm gmMonthlyInvoiceBatchForm = (GmMonthlyInvoiceBatchForm) form;
    gmMonthlyInvoiceBatchForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmMonthlyInvoiceBatchForm);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    GmMonthlyInvoiceBatchRptBean gmMonthlyInvoiceBatchRptBean =
        new GmMonthlyInvoiceBatchRptBean(getGmDataStoreVO());

    strJSON = gmWSUtil.parseArrayListToJson(gmMonthlyInvoiceBatchRptBean.loadNameList(hmParam));

    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return mapping.findForward("GmMonthlyInvoiceBatch");
  }

  /**
   * fetchInvCloseDate--This method is used to get the invoice closing for the accounts and dealer
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward fetchInvCloseDate(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strJSON = "";
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    instantiate(request, response);
    GmMonthlyInvoiceBatchForm gmMonthlyInvoiceBatchForm = (GmMonthlyInvoiceBatchForm) form;
    gmMonthlyInvoiceBatchForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmMonthlyInvoiceBatchForm);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    GmMonthlyInvoiceBatchRptBean gmMonthlyInvoiceBatchRptBean =
        new GmMonthlyInvoiceBatchRptBean(getGmDataStoreVO());

    strJSON = gmWSUtil.parseHashMapToJson(gmMonthlyInvoiceBatchRptBean.fetchInvCloseDate(hmParam));

    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return mapping.findForward("GmMonthlyInvoiceBatch");
  }

  /**
   * fetchBillCustType--This method is used to get the invoice closing for the accounts and dealer
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward fetchBillCustType(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strJSON = "";
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    instantiate(request, response);
    GmMonthlyInvoiceBatchForm gmMonthlyInvoiceBatchForm = (GmMonthlyInvoiceBatchForm) form;
    gmMonthlyInvoiceBatchForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmMonthlyInvoiceBatchForm);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    GmMonthlyInvoiceBatchRptBean gmMonthlyInvoiceBatchRptBean =
        new GmMonthlyInvoiceBatchRptBean(getGmDataStoreVO());

    strJSON = gmWSUtil.parseHashMapToJson(gmMonthlyInvoiceBatchRptBean.fetchBillCustType(hmParam));

    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return mapping.findForward("GmMonthlyInvoiceBatch");
  }


  /**
   * generateOutPut This method is used to generate data which need to show in grid.
   * 
   * @param hmParam HashMap
   * @exception AppError
   */

  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String vmFile = GmCommonClass.parseNull((String) hmParam.get("VMFILE"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.batch.GmMonthlyInvoiceBatch", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("accounts/batch/templates");
    templateUtil.setTemplateName(vmFile);
    return templateUtil.generateOutput();
  }

  /**
   * generateInvoiceStatement--This method is used to generate invoice statement
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   */
  public ActionForward generateInvoiceStatement(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    String strSucessMsg = "";
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    instantiate(request, response);
    GmMonthlyInvoiceBatchForm gmMonthlyInvoiceBatchForm = (GmMonthlyInvoiceBatchForm) form;
    gmMonthlyInvoiceBatchForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmMonthlyInvoiceBatchForm);

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    GmMonthlyInvoiceBatchRptBean gmMonthlyInvoiceBatchRptBean =
        new GmMonthlyInvoiceBatchRptBean(getGmDataStoreVO());

    strSucessMsg = gmMonthlyInvoiceBatchRptBean.generateInvoiceStatement(hmParam);


    if (!strSucessMsg.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strSucessMsg);
      pw.flush();
      return null;
    }
    return null;
  }

}
