package com.globus.accounts.batch.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.batch.forms.GmDOPrintBatchAddForm;
import com.globus.accounts.beans.GmAcctDashboardBean;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmDOPrintBatchAddAction extends GmSalesDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Logger code

  /**
   * saveDOSummaryFiles
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   * @throws ServletException
   */
  public ActionForward saveDOSummaryFiles(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException {
    instantiate(request, response);
    GmDOPrintBatchAddForm gmDOPrintBatchAddForm = (GmDOPrintBatchAddForm) actionForm;
    GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
    GmBatchBean gmBatchBean = new GmBatchBean(getGmDataStoreVO());
    GmAcctDashboardBean gmAcct = new GmAcctDashboardBean(getGmDataStoreVO());
    GmAction gmAction = new GmAction();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmResourceBundleBean gmResourceBundleBeanlbl =
        GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmSalesOrderReport",
            strSessCompanyLocale);

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmSalesFilters = new HashMap();
    HashMap hmCurrency = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alCompanyCurrency = new ArrayList();

    String strForward = "GmSalesOrderReport";
    String strOpt = "";
    String strSalesFilterCond = "";
    String strAccessCondition = "";

    // To get the Access Level information
    strSalesFilterCond = GmCommonClass.parseNull(getAccessFilter(request, response, false));
    hmSalesFilters =
        GmCommonClass.parseNullHashMap(gmCommonBean.getSalesFilterLists(strSalesFilterCond));
    strSalesFilterCond = GmCommonClass.parseNull(getAccessFilter(request, response, true));
    strAccessCondition =
        GmCommonClass.parseNull(gmAction.getSalesAccessCondition(request, response, true));

    gmDOPrintBatchAddForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmDOPrintBatchAddForm);
    // Getting currenct id when saving batch and again sending to jsp.
    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
    String strAccountCurrency = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
    strAccountCurrency = strAccountCurrency.equals("0") ? strCompanyCurrId : strAccountCurrency;
    String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrency);
    alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();
    log.debug("hmParam"+hmParam);
    
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    
    //As the Daily A/R DO Batch is implemented in servlet, we are passing values from JSP to Action via request (get)
    //When the user is selecting multiple orders, this get URL becomes so big and its not passing the entire order ,hence partially the batch is created.
    // Now we are passing it from the request paremeter ,so getting it from request and not from the form.
    String strInputStr = GmCommonClass.parseNull((String)request.getParameter("hInputString"));
    hmParam.put("INPUTSTRING",strInputStr);
    
    String strBatchID = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    hmParam.put("ACESSCONDITION", strAccessCondition);
    hmParam.put("ARCURRSYMBOL", strAccountCurrency);
    if (strOpt.equals("Save")) {
      String lblBatch = GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_BATCH"));
      String lblCreatedSuccessfully =
          GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_CREATED_SUCESSFULLY"));
      // save the Do Summary
      hmParam.put("companyInfo", GmCommonClass.parseNull(request.getParameter("companyInfo")));
      strBatchID = gmARBatchBean.saveProcessDOPrint(hmParam);
      hmParam.put("BATCHID", strBatchID);
      request.setAttribute("SuccessMsg", lblBatch + strBatchID + lblCreatedSuccessfully);
    }

    hmReturn = gmAcct.acctSalesReport(hmParam);
    hmReturn.put("ALCOMPANYCURRENCY", alCompanyCurrency);
    request.setAttribute("hmReturn", hmReturn);
    request.setAttribute("hFrom", strFromDate);
    request.setAttribute("hTo", strToDate);
    request.setAttribute("hType", "ACCT");
    request.setAttribute("ACC_CURR_ID", strAccountCurrency);
    request.setAttribute("ACC_CURR_SYMB", strAccCurrName);
    // to set the sales filter values
    request.setAttribute("hmSalesFilters", hmSalesFilters);
    return actionMapping.findForward(strForward);
  }

}
