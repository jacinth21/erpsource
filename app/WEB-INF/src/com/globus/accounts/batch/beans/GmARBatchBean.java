/*
 * Module: A/R Name: GmARBatchBean
 */

package com.globus.accounts.batch.beans;

import java.io.File;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.print.attribute.standard.MediaName;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmInvoiceBean;
import com.globus.accounts.tax.beans.GmSalesTaxTransBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.util.GmFileWriter;
import com.globus.common.util.GmPrintService;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmURLConnection;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmARBatchBean extends GmBean {
  GmCommonClass gmCommonClass = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
  public static final String TEMPLATE_NAME = "GmInvoicePDFEmail";

  // this will be removed all place changed with Data Store VO constructor
  public GmARBatchBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmARBatchBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchAccountPendingPOs to fetch the pending POs
   * 
   * @author
   * @param
   * @return ArrayList
   */
  public ArrayList fetchAccountPendingPOs() throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_acct_pending_pos", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    log.debug("alResult.size() -->" + alResult.size());

    return alResult;
  }

  /**
   * getXmlGridData xml content generation for Batch grid
   * 
   * @author
   * @param array list and hash map
   * @return String
   */
  public String getXmlGridData(ArrayList alResult, HashMap hmParam) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strTempletName = "";
    String strTempletPath = "";
    String strSessCompanyLocale =
        gmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));

    strTempletName = gmCommonClass.parseNull((String) hmParam.get("TEMPNAME"));
    strTempletPath = gmCommonClass.parseNull((String) hmParam.get("TEMPPATH"));
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.batch.templates.GmARBatchBean", strSessCompanyLocale));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir(strTempletPath);
    templateUtil.setTemplateName(strTempletName);

    return templateUtil.generateOutput();
  }

  /**
   * fetchInvDet to fetch the
   * 
   * @author
   * @param
   * @return ArrayList
   */
  public ArrayList fetchInvDet(HashMap hmParm) throws AppError {
    ArrayList alResult = new ArrayList();
    String strInputStr = gmCommonClass.parseNull((String) hmParm.get("INPUTSTRING"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_inv_det", 2);
    gmDBManager.setString(1, strInputStr);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("alResult.size() -->" + alResult.size());

    return alResult;
  }

  /**
   * fetchInvBatchDet to fetch the batch details
   * 
   * @author
   * @param HashMap
   * @return ArrayList
   */
  public ArrayList fetchInvBatchDet(HashMap hmParm) throws AppError {
    ArrayList alResult = new ArrayList();
    String strBatchId = gmCommonClass.parseNull((String) hmParm.get("INVBATCHID"));
    String strBatchType = gmCommonClass.parseNull((String) hmParm.get("REFTYPE"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_inv_batch_det", 3);
    gmDBManager.setString(1, strBatchId);
    gmDBManager.setString(2, strBatchType);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    log.debug("alResult.size() -->" + alResult.size());

    return alResult;
  }

  /**
   * fetchInvBatchDet to fetch the batch details
   * 
   * @author
   * @param HashMap
   * @return ArrayList
   */
  public ArrayList fetchDealerInvBatchDtls(HashMap hmParm) throws AppError {
    ArrayList alResult = new ArrayList();
    String strBatchId = gmCommonClass.parseNull((String) hmParm.get("INVBATCHID"));
    String strBatchType = gmCommonClass.parseNull((String) hmParm.get("REFTYPE"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_dealer_inv_batch_dtls", 3);
    gmDBManager.setString(1, strBatchId);
    gmDBManager.setString(2, strBatchType);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    log.debug("alResult.size() -->" + alResult.size());

    return alResult;
  }


  /**
   * saveInvoiceBatch to create the batch and add the batch details
   * 
   * @author
   * @param HashMap
   * @return String
   */
  public void saveInvoiceBatch(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strUserId = gmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputStr = gmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strInvBatchID = gmCommonClass.parseNull((String) hmParam.get("INVBATCHID"));

    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_trans.gm_sav_inv_batch", 3);
    gmDBManager.setString(1, strInputStr);
    gmDBManager.setString(2, strInvBatchID);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * saveProcessInvoice to create the batch and add the batch details This method also contains JMS
   * Code which put the batch on a queue and trigger a background JOB on seperate thread, The call
   * is asynchronous. Based on the constant BATCH_PROCESS_SERVICE in prop file is JMS need to be
   * activated then "Jms" or QuartzJob
   * 
   * @author
   * @param HashMap
   * @return String
   */
  public String saveProcessInvoice(HashMap hmParam, String strBatchProcessService,
      String strBatchMDBClass) throws AppError {
    String strUserId = gmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInvBatchID = gmCommonClass.parseNull((String) hmParam.get("INVBATCHID"));
    String strBatchStatus = gmCommonClass.parseNull((String) hmParam.get("INVBATCHSTATUS"));
    String strInputStr = gmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strCompanyInfo = gmCommonClass.parseNull((String) hmParam.get("companyInfo"));

    if (strBatchStatus.equals("18742")) { // 18742 - Pending Processing
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_trans.gm_sav_process_inv_batch", 3);
      gmDBManager.setString(1, strInvBatchID);
      gmDBManager.setString(2, strInputStr);
      gmDBManager.setString(3, strUserId);
      gmDBManager.execute();
      gmDBManager.commit();
    }

    // to update sales tax
    GmSalesTaxTransBean gmSalesTaxTransBean = new GmSalesTaxTransBean(getGmDataStoreVO());
    GmBatchBean gmBatchBean = new GmBatchBean();
    String strErrorCustPos = gmSalesTaxTransBean.updateBatchSalesTax(strInvBatchID, strUserId);
    log.debug("strErrorCustPos ::: " + strErrorCustPos);
    log.error("strErrorCustPos ----> " + strErrorCustPos);
    gmBatchBean.updatePOStatus(strInvBatchID,strErrorCustPos,strUserId);
    
    // This is JMS Code which put the batch on a queue and trigger a background JOB on seperate
    // thread, The call is asynchronous.
    if (strBatchProcessService.equals("JMS")) {
      HashMap hmMessageObj = new HashMap();
      GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
      // to get the queue name
      String strARBatchQueue = gmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_AR_QUEUE"));
      hmMessageObj.put("BATCHID", strInvBatchID);
      hmMessageObj.put("COMPANY_ID", getCompId());
      hmMessageObj.put("companyInfo", strCompanyInfo);
      // to set the JMS class name and queue name
      hmMessageObj.put("CONSUMERCLASS", strBatchMDBClass);
      hmMessageObj.put("QUEUE_NAME", strARBatchQueue);
      log.debug("Message Sent Batch hmMessageObj---------------> " + hmMessageObj);
      log.error("hmMessageObj --- JMS Call----> " + hmMessageObj);
      gmConsumerUtil.sendMessage(hmMessageObj);

      log.error("Message Sent Batch #:" + strInvBatchID);
    }
    return strErrorCustPos;

  }

  /**
   * fetchBatchInvoiceDtls to fetch the batch invoice details
   * 
   * @author
   * @param HashMap
   * @return ArrayList
   */
  public ArrayList fetchBatchInvoiceDtls(String strBatchID) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_batch_inv_dtls", 2);
    gmDBManager.setString(1, strBatchID);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("alResult.size() -->" + alResult.size());

    return alResult;
  }

  /**
   * saveInvoiceAsPDF to save invoice to PDF
   * 
   * @author
   * @param HashMap
   * @return
   * @throws Exception
   */
  public boolean saveInvoiceAsPDF(HashMap hmParam) throws Exception {

    String strInvId = gmCommonClass.parseNull((String) hmParam.get("INVID"));
    String strUser = gmCommonClass.parseNull((String) hmParam.get("USERID"));

    String resourcename = gmCommonClass.parseNull((String) hmParam.get("RESOURCE"));
    String strPara = gmCommonClass.parseNull((String) hmParam.get("PARAMETER"));
    String year = gmCommonClass.parseNull((String) hmParam.get("YEAR"));
    String strPDFUpload = gmCommonClass.parseNull((String) hmParam.get("PDFUPLOAD"));
    String strCompanyInfo = gmCommonClass.parseNull((String) hmParam.get("companyInfo"));

    GmURLConnection gmURLConnection = new GmURLConnection(getGmDataStoreVO());
    gmURLConnection.urlconnection(resourcename, strPara + "&year=" + year + "&companyInfo="
        + URLEncoder.encode(strCompanyInfo));
    // Get the company code, since the invoices are saving in the respective company folders
    GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(getGmDataStoreVO());
    HashMap hmCompanyInvoiceDtls = gmInvoiceBean.fetchCompanyInvoiceDtls(strInvId);
    String strCompanyCode = GmCommonClass.parseNull((String) hmCompanyInvoiceDtls.get("COMP_CODE"));

    boolean isPDFCreated = strPDFUpload.equals("YES") ? true : false;
    if (!isPDFCreated) {
      GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
      gmCommonUploadBean.saveUploadInfo(strInvId, "90906", strCompanyCode + "\\" + year + "\\"
          + strInvId + ".pdf", strUser);
      gmInvoiceBean.saveInvoiceFileRef(strInvId, "90906", strUser);
      isPDFCreated = true;
    }

    return isPDFCreated;
  }


  /**
   * saveInvoiceAsCSV to save invoice to PDF
   * 
   * @author
   * @param HashMap
   * @return
   * @throws Exception
   */
  public boolean saveInvoiceAsCSV(HashMap hmParam) throws Exception {


    String strInvId = gmCommonClass.parseNull((String) hmParam.get("INVID"));
    String strInvoicePDF = GmCommonClass.getString("BATCH_INVOICE");
    String strUser = gmCommonClass.parseNull((String) hmParam.get("USERID"));
    String year = gmCommonClass.parseNull((String) hmParam.get("YEAR"));
    String strCSVUpload = gmCommonClass.parseNull((String) hmParam.get("CSVUPLOAD"));
    String strCompanyCode = gmCommonClass.parseNull((String) hmParam.get("COMPANY_CODE"));

    GmFileWriter gmFileWriter = new GmFileWriter();
    GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(getGmDataStoreVO());
    HashMap hmReturnCSV = fetchInvoiceCSVDtl(strInvId);

    ArrayList alDetailList = (ArrayList) hmReturnCSV.get("DTLLIST");
    ArrayList alHeaderList = (ArrayList) hmReturnCSV.get("HEADERLIST");
    String strDetail = extractCSVDetail(alHeaderList, alDetailList);

    String pdffileName = strInvoicePDF + strCompanyCode + "\\" + year + "\\" + strInvId + ".csv";
    File strFileChk = new File(pdffileName);
    if (!strFileChk.exists()) {
      gmFileWriter.writeFile(strInvoicePDF + strCompanyCode + "\\" + year + "\\", strInvId, "csv",
          strDetail);
    }
    boolean isCSVCreated = strCSVUpload.equals("YES") ? true : false;
    if (!isCSVCreated) {
      GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
      gmCommonUploadBean.saveUploadInfo(strInvId, "90907", strCompanyCode + "\\" + year + "\\"
          + strInvId + ".csv", strUser);
      gmInvoiceBean.saveInvoiceFileRef(strInvId, "90907", strUser);
      isCSVCreated = true;
    }
    return isCSVCreated;
  }

  /**
   * fetchInvoiceCSVDtl to fetch csv file header and detail
   * 
   * @author
   * @param HashMap
   * @return
   * @throws Exception
   */
  public HashMap fetchInvoiceCSVDtl(String strInvID) throws AppError {

    HashMap hmInvDetail = new HashMap();
    String strLog = GmCommonClass.parseNull(strInvID);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_csv_invoice_dtls", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strInvID);
    gmDBManager.execute();

    ArrayList alHeaderList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    ArrayList alDtlList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

    hmInvDetail.put("HEADERLIST", alHeaderList);
    hmInvDetail.put("DTLLIST", alDtlList);

    gmDBManager.close();
    log.debug("hmInvDetail" + hmInvDetail);
    log.debug("alHeaderList" + alHeaderList);
    log.debug("DTLLIST" + alDtlList);

    return hmInvDetail;

  }

  /**
   * fetchInvoiceCSVDtl to map csv file header and detail
   * 
   * @author
   * @param String
   * @return
   */

  public String extractCSVDetail(ArrayList alHeaderList, ArrayList alDetailList) {
    StringBuffer sb = new StringBuffer();
    ArrayList alOrderedHeaderList = new ArrayList();

    for (int i = 0; i < alHeaderList.size(); i++) {
      HashMap hmRecord = (HashMap) alHeaderList.get(i);
      String strHeader = (String) hmRecord.get("CSVHEADER");
      alOrderedHeaderList.add(strHeader);
      strHeader = strHeader.replaceAll("_", " ");
      sb.append(strHeader).append(",");
    }
    sb.replace(sb.length() - 1, sb.length(), "\r");

    for (int i = 0; i < alDetailList.size(); i++) {
      HashMap hmRecord = (HashMap) alDetailList.get(i);

      for (int j = 0; j < alOrderedHeaderList.size(); j++) {
        String strKey = ((String) (alOrderedHeaderList.get(j))).toUpperCase();

        if (hmRecord.get(strKey) instanceof String) {
          sb.append(((String) hmRecord.get(strKey)).replaceAll(",", " ") + ",");
        } else {
          String strTempDt =
              GmCommonClass.getStringFromDate((java.util.Date) hmRecord.get(strKey),
                  getGmDataStoreVO().getCmpdfmt());
          sb.append((strTempDt).replaceAll(",", " ") + ",");
        }


      }
      sb.replace(sb.length() - 1, sb.length(), "\r");
    }
    return sb.toString();
  }

  /**
   * saveProcessInvoice to create the batch and add the batch details
   * 
   * @author
   * @param HashMap
   * @return String
   * @throws Exception
   */
  public void sendEmail(HashMap hmParam) throws Exception {
    String strInvId = gmCommonClass.parseNull((String) hmParam.get("INVID"));
    String strOUSDistFl = gmCommonClass.parseNull((String) hmParam.get("OUSDISTFL"));
    String strPDFFileName = gmCommonClass.parseNull((String) hmParam.get("PDFFILENAME"));
    String strCSVFileName = gmCommonClass.parseNull((String) hmParam.get("CSVFILENAME"));
    String strEversion = GmCommonClass.parseNull((String) hmParam.get("EVERSION"));
    String strAccountMailIDs = GmCommonClass.parseNull((String) hmParam.get("ACCMAILID"));
    String strCompanyID = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
    String strCompnayName = GmCommonClass.parseNull((String) hmParam.get("COMPANYNAME"));
    String strMPR = GmCommonClass.parseNull((String) hmParam.get("MPR"));
    String strInvType = GmCommonClass.parseNull((String) hmParam.get("INVTYPE"));
    String strDivisionId = GmCommonClass.parseNull((String) hmParam.get("DIVISION_ID"));
    String strTaxDateFl = GmCommonClass.parseNull((String) hmParam.get("TAX_DATE_FL"));
    String strTaxableCountry = GmCommonClass.parseNull((String) hmParam.get("TAXABLE_COUNTRY_FL"));
    String strFileName = strPDFFileName;
    String strcmpId = getGmDataStoreVO().getCmpid();
    String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    String strLogo = "";
    GmResourceBundleBean gmCompanyResourceBundleBean =
        gmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    strDivisionId = strDivisionId.equals("") ? "2000" : strDivisionId;
    strLogo =
        GmCommonClass.parseNull(gmCompanyResourceBundleBean.getProperty(strDivisionId + ".LOGO"));
    // Using the HTML mail - Full image size will be display - so, here hard coded the spine - logo
    // image
    if (strDivisionId.equals("2000")) {
      strLogo = "s_logo";
    }
    if (strTaxableCountry.equals("Y") && strTaxDateFl.equals("Y") && strDivisionId.equals("2000")) {
      // Globus medical North America
      strDivisionId = "100802";
    }
    String strCompanyName =
        GmCommonClass.parseNull(gmCompanyResourceBundleBean
            .getProperty(strDivisionId + ".COMPNAME"));
    String strCompanyAddress =
        GmCommonClass.parseNull(gmCompanyResourceBundleBean.getProperty(strDivisionId
            + ".COMPADDRESS"));
    String strCompanyPhone =
        GmCommonClass.parseNull(gmCompanyResourceBundleBean.getProperty(strDivisionId + ".PH"));

    if (strEversion.equals("91995"))// 91995 CSV, 91996 PDF and CSV
    {
      strFileName = strCSVFileName;
    }

    if (strEversion.equals("91996"))// 91996 PDF and CSV
    {
      strFileName = strPDFFileName + "," + strCSVFileName;
    }

    if (!strInvType.equals("50201")) { // 50201 -- OBO Invoice
      GmEmailProperties emailProps = new GmEmailProperties();
      GmJasperMail jasperMail = new GmJasperMail();
      String strFrom = "";
      String strTo = "";
      String strCC = "";
      String strMimeType = "";
      String strSubject = "";
      String strEmailId = "";
      String strPhoneExt = "";
      String strPhone = "";
      String strMessageBody = "";
      HashMap hmReturnDetails = new HashMap();
      HashMap hmTemp = new HashMap();

      GmResourceBundleBean gmResourceBundleBean =
    		  GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

      strFrom = GmCommonClass.getRuleValueByCompany("INVOICE_EMAIL", "EMAIL", strcmpId);
      strTo = strAccountMailIDs;
      
      /**
       For CC Update In International Invoices Going to US Invoices Inbox 
       */
      
      if(strOUSDistFl.equals("Y")) { 
    	  strCC =
    	          GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("INVOICE_EMAIL_OUS_CC", "EMAIL",
    	              strcmpId));
      }else
      {
    	  strCC =
    	          GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("INVOICE_EMAIL_CC", "EMAIL",
    	              strcmpId));
      }
      log.debug("strCC --- Email --"+strCC);

      strPhoneExt =
          GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("PHONE_EXT", "EMAIL",
              strcmpId));

      strPhone =
          GmCommonClass
              .parseNull(GmCommonClass.getRuleValueByCompany("PHONE", "COMPANY", strcmpId));
      strMessageBody =
          GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("INV_MESSAGE_BODY",
              "AR_EMAIL_BODY", strcmpId));

      strMimeType =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
              + GmEmailProperties.MIME_TYPE));
      strSubject =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "."
              + GmEmailProperties.SUBJECT));

      String strJasperName = "/GmAcctEmailInvoice.jasper";

      strSubject = GmCommonClass.replaceAll(strSubject, "#<COMP_NM>", strCompnayName);
      strSubject = GmCommonClass.replaceAll(strSubject, "#<INV_ID>", strInvId);

      hmTemp.put("EMAILID", strFrom);
      hmTemp.put("COMPANYID", strCompanyID);
      hmTemp.put("MPR", strMPR);
      hmTemp.put("PHONEEXT", strPhoneExt);
      hmTemp.put("COMPANYPH", strPhone);
      hmTemp.put("LOGOIMAGE", strLogo + ".gif");
      hmTemp.put("COMPANYNAME", strCompanyName);
      hmTemp.put("COMPANYADDRESS", strCompanyAddress + " / " + strCompanyPhone);
      hmTemp.put("MESSAGEBODY", strMessageBody);

      emailProps.setMimeType(strMimeType);
      emailProps.setSender(strFrom);
      emailProps.setRecipients(strTo);
      emailProps.setCc(strCC);
      log.debug("Ous/Non Ous CC"+strCC);
      emailProps.setSubject(strSubject);
      emailProps.setAttachment(strFileName);

      jasperMail.setJasperReportName(strJasperName);
      jasperMail.setAdditionalParams(hmTemp);
      jasperMail.setReportData(null);
      jasperMail.setEmailProperties(emailProps);
      hmReturnDetails = jasperMail.sendMail();
    }
  }

  /**
   * checkFileCreated to check if the file is created
   * 
   * @author
   * @param boolean
   * @return
   */
  public ArrayList fetchUploadDtls(HashMap hmParam) throws AppError {
    String strBatchId = gmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_file_upload", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strBatchId);
    gmDBManager.execute();
    ArrayList alDtlList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

    gmDBManager.close();
    return alDtlList;
  }

  /**
   * print invoice pdf file
   * 
   * @author
   * @param HashMap
   * @return String
   * @throws Exception
   */
  public void print(HashMap hmParam) throws Exception {

    GmDataStoreVO tmpGmDataStoreVO = null;

    String pdffileName = gmCommonClass.parseNull((String) hmParam.get("PDFFILENAME"));
    String strInvId = gmCommonClass.parseNull((String) hmParam.get("INVID"));
    String strRuleId = "";
    String strCompId = getGmDataStoreVO().getCmpid();
    GmPrintService gmPrintService = new GmPrintService();
    gmPrintService.setNumofCopies(1);

    String strCompanyLocale = GmCommonClass.getCompanyLocale(strCompId);
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    String strA4PaperSizeFl =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.PAPER_SIZE_A4"));
    strRuleId = "AR_" + strCompId;
    gmPrintService.setPrinterName(GmCommonClass.getRuleValueByCompany(strRuleId, "PRINTER",
        strCompId));
    gmPrintService.setFileName(pdffileName);
    gmPrintService.setPaperSize(MediaName.ISO_A4_WHITE);
    gmPrintService.setFontName("Verdana");
    gmPrintService.setFontSize(10);
    gmPrintService.setImgArea_x_offset(-9);
    gmPrintService.setImgArea_y_offset(-3);
    // PMT-19992: OUS print - bottom space issue fix.
    // A4 paper size to reset the Y offset values.
    if (strA4PaperSizeFl.equals("Y")) {
      gmPrintService.setImgArea_y_offset(10);
      gmPrintService.setImgArea_width_offset(-30);
      gmPrintService.setImgArea_height_offset(-32);
      // to set the A4 paper size
      // Width 595 pixels x height 842 pixels
      gmPrintService.setPaperSizeHeight(842);
      gmPrintService.setPaperSizeWidth(595);
    } else {
      gmPrintService.setImgArea_width_offset(-10);
      gmPrintService.setImgArea_height_offset(-12);
    }
    gmPrintService.setGraphics2D_x(20);
    gmPrintService.setGraphics2D_y(10);
    gmPrintService.print();
  }

  /**
   * This method will fetch the email for the corresponding invoice ID
   * 
   * @author
   * @param String
   */
  public String getAccountEmail(HashMap hmData) throws AppError {

    String strInvId = GmCommonClass.parseNull((String) hmData.get("INVID"));
    String strEmail = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_account_email", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strInvId);
    gmDBManager.execute();
    strEmail = gmDBManager.getString(1);
    gmDBManager.close();

    return strEmail;
  }

  /**
   * fetchLogDtls to fetch log details
   * 
   * @author
   * @param HashMap
   * @return ArrayList
   * @throws Exception
   */
  public ArrayList fetchLogDtls(HashMap hmParam) throws AppError {
    String strBatchId = gmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strRefType = gmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_log_detail", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strBatchId);
    gmDBManager.setString(2, strRefType);
    gmDBManager.execute();
    ArrayList alDtlList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

    gmDBManager.close();
    return alDtlList;

  }

  /**
   * This method will fetch the invoice year for the corresponding invoice ID
   * 
   * @author
   * @param String
   */
  public String getInvoiceYear(String strInvoicID) throws AppError {

    String strInvoiceYear = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_invoice_year", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strInvoicID);
    gmDBManager.execute();
    strInvoiceYear = gmDBManager.getString(1);
    gmDBManager.close();

    return strInvoiceYear;
  }


  /**
   * fetchNonBatchPOs to fetch the pending pos
   * 
   * @author
   * @param HashMap
   * @return Object
   */
  public Object fetchNonBatchPOs(HashMap hmParm) throws AppError {
    ArrayList alResult = new ArrayList();
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strAccountIDs = gmCommonClass.parseNull((String) hmParm.get("INPUTSTRING"));
    String strScreenType = gmCommonClass.parseNull((String) hmParm.get("SCREENTYPE"));
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_acc_non_batch_pos", 2);
    gmDBManager.setString(1, strAccountIDs);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    if (strScreenType.equals("INV_BATCH")) { // its call to AR home
      rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));// returnHashMap((ResultSet)
                                                                                    // gmDBManager.getObject(1));
      gmDBManager.close();
      return rdResult;
    } else {
      alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
      gmDBManager.close();
      return alResult;
    }
  }

  /**
   * saveInvoiceBatch to create the batch and add the batch details
   * 
   * @author
   * @param HashMap
   * @return String
   */
  public void saveUnlinkBatchPO(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strOrderID = gmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strCustomerPO = gmCommonClass.parseNull((String) hmParam.get("CUST_PO"));
    String strAccontID = gmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strType = gmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strUserId = gmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_trans.gm_sav_batch_hold_po", 5);
    gmDBManager.setString(1, strOrderID);
    gmDBManager.setString(2, strCustomerPO);
    gmDBManager.setString(3, strAccontID);
    gmDBManager.setString(4, strType);
    gmDBManager.setString(5, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * fetchOrderBarcodeDtls to fetch order - PO and account details
   * 
   * @author
   * @param HashMap
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchOrderBarcodeDtls(HashMap hmParam) throws AppError {
    String strRefID = GmCommonClass.parseNull((String) hmParam.get("STRREFID"));
    String strSource = GmCommonClass.parseNull((String) hmParam.get("STRSOURCE"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAccCurrId = GmCommonClass.parseNull((String) hmParam.get("STRCOMPCURRENCY"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_ord_batch_barcode_dtls", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRefID);
    // gmDBManager.setString(2,strSource);
    gmDBManager.setString(2, strAccCurrId);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();
    HashMap hmDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    log.debug(" hmDetails --> " + hmDetails);
    return hmDetails;

  }

  /**
   * saveOrderBarcodeDtls to add the batch details - based on the PO and account
   * 
   * @author
   * @param HashMap
   * @return void
   */
  public void saveOrderBarcodeDtls(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBatchID = gmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strCustomerPO = gmCommonClass.parseNull((String) hmParam.get("CUST_PO"));
    String strAccontID = gmCommonClass.parseNull((String) hmParam.get("ACCID"));
    String strCboActoin = gmCommonClass.parseNull((String) hmParam.get("ACTION"));
    String strEmailFl = gmCommonClass.parseNull((String) hmParam.get("EMAILFL"));
    Date dtInvDate = (Date) hmParam.get("INVDATE");
    String strUserId = gmCommonClass.parseNull((String) hmParam.get("USERID"));
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_trans.gm_sav_ord_batch_barcode_dtls", 7);
    gmDBManager.setString(1, strBatchID);
    gmDBManager.setString(2, strAccontID);
    gmDBManager.setString(3, strCustomerPO);
    gmDBManager.setString(4, strCboActoin);
    gmDBManager.setString(5, strEmailFl);
    gmDBManager.setDate(6, dtInvDate);
    gmDBManager.setString(7, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * This method will fetch the In Progress batch id based on the user id
   * 
   * @author
   * @param String
   */
  public String getInProgressBatchID(String strUserID) throws AppError {

    String strBatchId = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_inprogress_batch_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    strBatchId = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    log.debug(" strBatchId --> " + strBatchId);
    return strBatchId;
  }

  /**
   * fetchDOSummaryBatchDtls to fetch the batch details
   * 
   * @author
   * @param HashMap
   * @return ArrayList
   */
  public ArrayList fetchDOSummaryBatchDtls(HashMap hmParm) throws AppError {
    ArrayList alResult = new ArrayList();
    String strBatchId = gmCommonClass.parseNull((String) hmParm.get("BATCHID"));
    String strBatchType = gmCommonClass.parseNull((String) hmParm.get("BATCHTYPE"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_do_summary_print", 3);
    gmDBManager.setString(1, strBatchId);
    gmDBManager.setString(2, strBatchType);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    log.debug("alResult.size() -->" + alResult.size());

    return alResult;
  }

  /**
   * saveProcessDOPrint to create the batch and add the batch details This method also contains JMS
   * Code which put the batch on a queue and trigger a background JOB on separate thread, The call
   * is asynchronous. Based on the constant BATCH_PROCESS_SERVICE in prop file is JMS need to be
   * activated then "Jms" or QuartzJob
   * 
   * @author
   * @param HashMap
   * @return String
   */
  public String saveProcessDOPrint(HashMap hmParam) throws AppError {
    String strUserId = gmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputString = gmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strCompanyInfo = gmCommonClass.parseNull((String) hmParam.get("companyInfo"));

    String strBatchProcessService =
        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_PROCESS_SERVICE"));
    String strBatchMDBClass =
        GmCommonClass.parseNull(GmCommonClass.getString("BATCH_CONSUMER_MDB_DO_PRINT_CLASS"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_trans.gm_sav_process_do_summary", 3);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strBatchID = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.commit();
    log.debug(" strBatchID --> " + strBatchID);

    // This is JMS Code which put the batch on a queue and trigger a
    // background JOB on separate thread, The call is asynchronous.
    if (strBatchProcessService.equals("JMS")) {
      GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
      HashMap hmMessageObj = new HashMap();
      // to get the queue name
      String strARBatchQueue = GmCommonClass.getJmsConfig("JMS_AR_QUEUE");
      hmMessageObj.put("BATCHID", strBatchID);
      hmMessageObj.put("COMPANY_ID", getCompId());
      hmMessageObj.put("companyInfo", strCompanyInfo);
      // to set the JMS class name and queue name
      hmMessageObj.put("CONSUMERCLASS", strBatchMDBClass);
      hmMessageObj.put("QUEUE_NAME", strARBatchQueue);
      gmConsumerUtil.sendMessage(hmMessageObj);

      log.debug("Message Sent Batch #:" + strBatchID);
    }
    return strBatchID;
  }

  /**
   * saveDOSummaryasPDF to save Do Summary to PDF
   * 
   * @author
   * @param HashMap
   * @return
   * @throws Exception
   */
  public boolean saveDOSummaryasPDF(HashMap hmParam) throws Exception {
    String strRefID = gmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strUser = gmCommonClass.parseNull((String) hmParam.get("USERID"));
    String resourcename = gmCommonClass.parseNull((String) hmParam.get("RESOURCE"));
    String strPara = gmCommonClass.parseNull((String) hmParam.get("PARAMETER"));
    String strCompanyInfo = gmCommonClass.parseNull((String) hmParam.get("companyInfo"));

    strPara = strPara + "&companyInfo=" + URLEncoder.encode(strCompanyInfo);
    GmURLConnection gmURLConnection = new GmURLConnection(getGmDataStoreVO());
    gmURLConnection.urlconnection(resourcename, strPara);
    return true;
  }

  /**
   * fetchAccountParameterDtls to fetch the acccount paramter details
   * 
   * @author
   * @param String
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchAccountParameterDtls(String strInvoicId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_account_param_dtls", 2);
    gmDBManager.setString(1, strInvoicId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * saveInvoiceAsXML to create the new XML file for the invoice id
   * 
   * @author Manikandan
   * @param HashMap
   * @return boolean
   * @throws AppError
   */
  public boolean saveInvoiceAsXML(HashMap hmParam) throws AppError {
    String strInvId = gmCommonClass.parseNull((String) hmParam.get("INVID"));
    String strInvoicePDF = GmCommonClass.getString("BATCH_INVOICE");
    String strUser = gmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strXMLUpload = gmCommonClass.parseNull((String) hmParam.get("XMLUPLOAD"));
    String strXMLPath = gmCommonClass.parseNull((String) hmParam.get("XMLPATH"));
    String xmlFileName = strInvoicePDF + strXMLPath;
    GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(getGmDataStoreVO());
    boolean isXMLCreated = strXMLUpload.equals("YES") ? true : false;
    if (isXMLCreated) {
      GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
      gmCommonUploadBean.saveUploadInfo(strInvId, "90908", strXMLPath, strUser);
      gmInvoiceBean.saveInvoiceFileRef(strInvId, "90908", strUser);
      isXMLCreated = true;
    }
    return isXMLCreated;
  }
}// end of class GmARBean
