package com.globus.accounts.batch.beans;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmMonthlyInvoiceBatchRptBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmMonthlyInvoiceBatchRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * loadMonthlyInvoiceBatch - This method is used to load the invoice monthly batch records
   * 
   * @author
   * @param HashMap hmParam
   * @return ArrayList alReturn
   */
  public ArrayList loadMonthlyInvoiceBatch(HashMap hmParam) throws AppError {


    ArrayList alReturn = new ArrayList();
    String strCompanyID = getCompId();
    Date dtFromDate = (Date) hmParam.get("FROMDATE");
    Date dtToDate = (Date) hmParam.get("TODATE");
    String strSalesOrders = "";
    String strPayments = "";
    String strBalance = "";

    String strfromdate =
        GmCommonClass.parseNull(GmCommonClass.getStringFromDate(dtFromDate, getCompDateFmt()));
    String strToDate =
        GmCommonClass.parseNull(GmCommonClass.getStringFromDate(dtToDate, getCompDateFmt()));

    String strInvCloseDt = GmCommonClass.parseNull((String) hmParam.get("INVOICECLOSINGDATE"));
    String strBillCust = GmCommonClass.parseNull((String) hmParam.get("BILLINGCUSTOMER"));
    String strCustomerId = GmCommonClass.parseNull((String) hmParam.get("SELECTEDNAMELIST"));
    String strTransTypes = GmCommonClass.parseNull((String) hmParam.get("TRANSACTIONTYPE"));

    if (strTransTypes.contains(",")) {
      String[] strTransTypeArr;
      strTransTypeArr = strTransTypes.split(",");
      if (GmCommonClass.parseNull(strTransTypeArr[0]).equals("106180")) {
        strSalesOrders = GmCommonClass.parseNull(strTransTypeArr[0]);
        strPayments = GmCommonClass.parseNull(strTransTypeArr[1]);
      } else if (GmCommonClass.parseNull(strTransTypeArr[0]).equals("106181")) {
        strPayments = GmCommonClass.parseNull(strTransTypeArr[0]);
        strBalance = GmCommonClass.parseNull(strTransTypeArr[1]);
      }

    } else if (strTransTypes.equals("106180")) {
      strSalesOrders = strTransTypes;
      strPayments = "";
      strBalance = "";
    } else if (strTransTypes.equals("106181")) {
      strSalesOrders = "";
      strPayments = strTransTypes;
      strBalance = "";
    } else if (strTransTypes.equals("106182")) {
      strSalesOrders = "";
      strPayments = "";
      strBalance = strTransTypes;
    }

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();


    if ((!strSalesOrders.equals("") && !strPayments.equals(""))
        || (!strSalesOrders.equals("") && strPayments.equals(""))) {

      sbQuery
          .append(" SELECT get_code_name(get_account_attrb_value(t501.c704_account_id,'106200')) bill_cust , get_account_attrb_value(t501.c704_account_id,'106200') bill_cust_id ");

      sbQuery
          .append(" , t501.c101_dealer_id dealerid,DECODE ("
              + getComplangid()
              + ",'103097',NVL(C101_PARTY_NM_EN,c101_party_nm),c101_party_nm) dealernm,t501.c704_account_id accid ");

      sbQuery.append(" , DECODE(" + getComplangid()
          + ",'103097',NVL(C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM),C704_ACCOUNT_NM) accname  ");

      sbQuery
          .append(" , t501.c501_order_id transid,  NVL(t501.c501_parent_order_id,t501.c501_order_id) parent_transid  ,t501.c501_order_date ord_dt,t501.c501_surgery_date trans_date");

      sbQuery
          .append(" , SUM (t501.c501_total_cost + NVL (t501.c501_ship_cost, 0)) trans_amt , t501.c501_hold_fl holdfl");

      sbQuery
          .append(" , T901c.c901_code_nm currency, decode(t501.c901_order_type,null,2521,t501.c901_order_type) ordtypeid");

      sbQuery
          .append(" , decode(T501.C901_ORDER_TYPE,null,'Bill & Ship',t901ot.c901_code_nm) ordtype");
      sbQuery
          .append(" ,  GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91983) EMAILREQ,t501.c501_customer_po custpo ");

      sbQuery
          .append(" FROM t501_order t501, t704_account t704, T101_party t101, T901_code_lookup t901c,T901_code_lookup t901ot  ");

      sbQuery
          .append(" WHERE t501.c704_account_id = t704.c704_account_id AND t101.c101_party_id = NVL(t501.C101_DEALER_ID,T704.c101_party_id) ");

      sbQuery
          .append(" AND t704.c901_currency = t901c.c901_code_id AND NVL(T501.C901_ORDER_TYPE,2521) = t901ot.c901_code_id ");

      if (!strInvCloseDt.equals("0") || !strInvCloseDt.equals("")) {
        sbQuery
            .append(" AND t501.c704_account_id IN (SELECT c704_account_id FROM t704a_account_attribute ");
        sbQuery.append(" WHERE c901_attribute_type = '106202' AND c704a_attribute_value = '"
            + strInvCloseDt + "' AND c704a_void_fl IS NULL)");
      }
      // to exclude the orders which is already added to the batch
      sbQuery.append(" AND t501.c501_order_id NOT IN(SELECT c9601_ref_id  ");

      sbQuery.append(" from t9601_batch_details t9601,t9600_batch t9600");

      sbQuery.append(" WHERE t9600.c9600_batch_id  = t9601.c9600_batch_id");

      sbQuery.append(" AND c9601_void_fl IS NULL  AND t9600.C1900_COMPANY_ID = " + getCompId()
          + " and t9600.C901_BATCH_STATUS <> '18744' ) AND t501.c503_invoice_id IS NULL");
      // To exclude Open sales return orders
      sbQuery.append(" AND t501.c501_order_id NOT IN (SELECT c501_order_id FROM t506_returns ");
      sbQuery
          .append(" WHERE c506_ac_credit_dt IS NULL AND c501_order_id IS NOT NULL AND c506_void_fl IS NULL  AND c1900_company_id = "
              + getCompId() + " AND C506_TYPE = '3300'  ");
      sbQuery.append(" AND c506_created_date >= TO_DATE('" + strfromdate + "','" + getCompDateFmt()
          + "')");
      sbQuery.append(" AND c506_created_date <= TO_DATE('" + strToDate + "','" + getCompDateFmt()
          + "'))");

      // To include ext country id for GH
      sbQuery
          .append(" AND (t501.c901_ext_country_id   IS NULL  OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))");

      sbQuery.append(" AND t501.c501_delete_fl  IS NULL");

      sbQuery.append(" AND t501.c501_void_fl  IS NULL");


      if (strBillCust.equals("106154")) { // 106154 --Dealer
        sbQuery.append(" AND t704.c901_account_type = '26230710'"); // 26230710 --Hospital-Dealer
        if ((!strCustomerId.equals("0")) && (!strCustomerId.equals(""))) {
          sbQuery.append(" AND t501.c101_dealer_id = '" + strCustomerId + "'");
        }
      } else if (strBillCust.equals("106155")) { // 106155 --Direct Account
        if ((!strCustomerId.equals("0")) && (!strCustomerId.equals(""))) {
          sbQuery.append("  AND t501.c704_account_id = '" + strCustomerId + "'");
        }
        sbQuery.append(" AND t704.c901_account_type = '70110'"); // 70110 --Hospital-Company
      }


      sbQuery
          .append(" AND t501.C1900_COMPANY_ID = "
              + getCompId()
              + " AND NVL (c901_order_type, -9999) NOT IN (SELECT t906.c906_rule_value FROM t906_rules t906  WHERE t906.c906_rule_grp_id = 'JPN_ORDTYPE' AND c906_rule_id = 'JPN_ORD_EXC')");
      // Removing for the PMT-24369 to include the missed orders in the next month invoice
      // sbQuery.append(" and c501_surgery_date >= TO_DATE('" + strfromdate + "','" +
      // getCompDateFmt()
      // + "')");
      sbQuery.append(" and c501_surgery_date <= TO_DATE('" + strToDate + "','" + getCompDateFmt()
          + "')");

      sbQuery
          .append(" GROUP BY  t501.c501_order_id,t501.c501_order_date,t501.c501_surgery_date,t501.c704_account_id,t501.c101_dealer_id,t501.c501_customer_po, t704.c901_currency,t501.c901_order_type,t501.c501_parent_order_id,t501.c501_hold_fl");

      sbQuery
          .append(" , C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM,C101_PARTY_NM_EN,c101_party_nm, T901c.c901_code_nm,t901ot.c901_code_nm ");

      // -- fetching price adjustment orders to displays in monthly batch
      sbQuery.append(" UNION ALL ");
      sbQuery
          .append(" SELECT get_code_name(get_account_attrb_value(t501.c704_account_id,'106200')) bill_cust , get_account_attrb_value(t501.c704_account_id,'106200') bill_cust_id ");

      sbQuery
          .append(" , t501.c101_dealer_id dealerid,DECODE ("
              + getComplangid()
              + ",'103097',NVL(C101_PARTY_NM_EN,c101_party_nm),c101_party_nm) dealernm,t501.c704_account_id accid ");

      sbQuery.append(" , DECODE(" + getComplangid()
          + ",'103097',NVL(C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM),C704_ACCOUNT_NM) accname ");

      sbQuery
          .append(" , t501.c501_order_id transid,  NVL(t501.c501_parent_order_id,t501.c501_order_id) parent_transid  ,t501.c501_order_date ord_dt,t501.c501_surgery_date trans_date");

      sbQuery
          .append(" , SUM (t501.c501_total_cost + NVL (t501.c501_ship_cost, 0)) trans_amt , t501.c501_hold_fl holdfl");

      sbQuery.append(" , T901c.c901_code_nm currency, t501.c901_order_type ordtypeid");

      sbQuery
          .append(" ,DECODE(T501.C901_ORDER_TYPE,null,'Bill & Ship',t901ot.c901_code_nm) ordtype");

      sbQuery
          .append(" ,  GET_ACCOUNT_ATTRB_VALUE (t501.c704_account_id, 91983) EMAILREQ,'' custpo ");

      sbQuery
          .append(" FROM t501_order t501, t704_account t704, t101_party t101 , T901_code_lookup T901c,T901_code_lookup t901ot  ");

      sbQuery
          .append(" WHERE t501.c704_account_id = t704.c704_account_id AND t704.c901_currency = T901c.c901_code_id  ");

      sbQuery
          .append(" AND t101.c101_party_id = NVL(t501.C101_DEALER_ID,T704.c101_party_id) AND T501.C901_ORDER_TYPE = t901ot.c901_code_id  ");

      if (!strInvCloseDt.equals("0") || !strInvCloseDt.equals("")) {
        sbQuery
            .append(" AND t501.c704_account_id IN (SELECT c704_account_id FROM t704a_account_attribute ");
        sbQuery.append(" WHERE c901_attribute_type = '106202' AND c704a_attribute_value = '"
            + strInvCloseDt + "' AND c704a_void_fl IS NULL)");
      }
      // to exclude the orders which is already added to the batch
      sbQuery.append(" AND t501.c501_order_id NOT IN(SELECT c9601_ref_id  ");

      sbQuery.append(" from t9601_batch_details t9601,t9600_batch t9600");

      sbQuery.append(" WHERE t9600.c9600_batch_id  = t9601.c9600_batch_id");

      sbQuery.append(" AND c9601_void_fl IS NULL  AND t9600.C1900_COMPANY_ID = " + getCompId()
          + "  AND  ");

      sbQuery.append("   ( C9600_DATE_OF_ISSUE_FROM BETWEEN TO_DATE('" + strfromdate + "','"
          + getCompDateFmt() + "') AND TO_DATE('" + strToDate + "','" + getCompDateFmt() + "'))) ");

      sbQuery.append(" AND t501.c901_order_type = '2524' "); // 2524 - Price adjustment order
      sbQuery.append(" AND c501_order_date >= TO_DATE('" + strfromdate + "','" + getCompDateFmt()
          + "')");
      sbQuery.append(" AND c501_order_date <= TO_DATE('" + strToDate + "','" + getCompDateFmt()
          + "')");

      // To include ext country id for GH
      sbQuery
          .append(" AND (t501.c901_ext_country_id   IS NULL  OR t501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))");

      sbQuery.append(" AND t501.c501_delete_fl  IS NULL");

      sbQuery.append(" AND t501.c501_void_fl  IS NULL");


      if (strBillCust.equals("106154")) { // 106154 --Dealer
        sbQuery.append(" AND t704.c901_account_type = '26230710'"); // 26230710 --Hospital-Dealer
        if ((!strCustomerId.equals("0")) && (!strCustomerId.equals(""))) {
          sbQuery.append(" AND t501.c101_dealer_id = '" + strCustomerId + "'");
        }
      } else if (strBillCust.equals("106155")) { // 106155 --Direct Account
        if ((!strCustomerId.equals("0")) && (!strCustomerId.equals(""))) {
          sbQuery.append("  AND t501.c704_account_id = '" + strCustomerId + "'");
        }
        sbQuery.append(" AND t704.c901_account_type = '70110'"); // 70110 --Hospital-Company
      }

      sbQuery
          .append(" GROUP BY  t501.c501_order_id,t501.c501_order_date,t501.c501_surgery_date,t501.c704_account_id,t501.c101_dealer_id,t501.c501_customer_po, t704.c901_currency,t501.c901_order_type,t501.c501_parent_order_id,t501.c501_hold_fl");

      sbQuery
          .append(" , C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM,C101_PARTY_NM_EN,c101_party_nm,T901c.c901_code_nm,t901ot.c901_code_nm ");

    }

    if ((!strSalesOrders.equals("") && !strPayments.equals(""))) {

      sbQuery.append(" union all ");

    }


    if ((!strSalesOrders.equals("") && !strPayments.equals(""))
        || (strSalesOrders.equals("") && !strPayments.equals(""))) {

      sbQuery
          .append(" SELECT get_code_name(DECODE(t503.c704_account_id,NULL,106154,106155)) BILL_CUST, to_char(DECODE(t503.c704_account_id,NULL,106154,106155)) BILL_CUST_ID, ");

      sbQuery.append(" t503.c101_dealer_id dealerid,DECODE (" + getComplangid()
          + ",'103097',NVL(C101_PARTY_NM_EN,c101_party_nm),c101_party_nm) dealernm,");

      sbQuery
          .append(" t503.c704_account_id accid, DECODE("
              + getComplangid()
              + ",'103097',NVL(C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM),C704_ACCOUNT_NM) accname,t503.c503_invoice_id transid,t503.c503_invoice_id parent_transid,  ");

      sbQuery
          .append(" t508.C508_RECEIVED_DATE ord_dt , t508.C508_RECEIVED_DATE trans_date, NVL(T508.C508_PAYMENT_AMT,0) trans_amt , ' ' holdfl,");

      sbQuery.append(" T901c.c901_code_nm CURRENCY ,");
      sbQuery
          .append(" t508.c901_type ordtypeid , DECODE(T508.C901_TYPE,null,'Bill & Ship',t901ot.c901_code_nm) ordtype, ' ' EMAILREQ, ' ' custpo ");

      sbQuery
          .append(" FROM T508_RECEIVABLES T508,T503_INVOICE T503, t1900_company t1900, t704_account t704, T101_party t101, T901_code_lookup T901c,T901_code_lookup t901ot  ");

      sbQuery
          .append(" WHERE T503.C704_ACCOUNT_ID = t704.C704_ACCOUNT_ID(+) AND T1900.C901_TXN_CURRENCY = T901c.c901_code_id   ");

      sbQuery
          .append(" AND T508.C901_TYPE = t901ot.c901_code_id AND t101.c101_party_id = NVL(T503.C101_DEALER_ID,T704.C704_ACCOUNT_ID)");

      sbQuery.append(" AND T508.C508_RECEIVED_DATE >= TO_DATE('" + strfromdate + "','"
          + getCompDateFmt() + "')");

      sbQuery.append(" AND T508.C508_RECEIVED_DATE   <= TO_DATE('" + strToDate + "','"
          + getCompDateFmt() + "')");

      if (strBillCust.equals("106154")) { // 106154 --Dealer

        sbQuery.append(" AND T503.C901_INVOICE_SOURCE='26240213' "); // 26240213 --Company Dealer

        if ((!strCustomerId.equals("0")) && (!strCustomerId.equals(""))) {

          sbQuery.append(" AND t503.c101_dealer_id = '" + strCustomerId + "'");

        }
      } else if (strBillCust.equals("106155")) { // 106155 --Direct Account

        if ((!strCustomerId.equals("0")) && (!strCustomerId.equals(""))) {

          sbQuery.append("  AND t503.c704_account_id = '" + strCustomerId + "'");
        }
        sbQuery.append(" AND T503.C901_INVOICE_SOURCE='50255' "); // 50255 --Company Customer

      }



      sbQuery.append(" AND T503.C1900_COMPANY_ID      = " + getCompId()
          + " AND T508.C503_INVOICE_ID  = T503.C503_INVOICE_ID AND T503.C503_VOID_FL IS NULL");
      sbQuery.append(" AND T503.C1900_COMPANY_ID  = t1900.C1900_COMPANY_ID");

    }

    if (!strSalesOrders.equals("") || !strPayments.equals("")) {
      sbQuery.append(" ORDER BY dealernm,accname ");

      log.debug(" sbQuery.toString() " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    }



    if (!strBalance.equals("")) {

      ArrayList alOpeningBal = loadPendingBalance(hmParam);

      if (alOpeningBal.size() != 0) {
        alReturn.addAll(alOpeningBal);
      }
    }


    return alReturn;
  }

  /**
   * loadNameList - This method is used to load the active dealer/account list
   * 
   * @author
   * @param HashMap hmParam
   * @return ArrayList alReturn
   */

  public ArrayList loadNameList(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    String strBillingCustomer = GmCommonClass.parseNull((String) hmParam.get("BILLINGCUSTOMER"));

    StringBuffer sbQuery = new StringBuffer();
    if (strBillingCustomer.equals("") || strBillingCustomer.equals("0")
        || strBillingCustomer.equals("106154")) {
      sbQuery.append(" SELECT   TO_CHAR(t101.c101_party_id) ID,TRIM (DECODE(" + getComplangid()
          + ",'103097',NVL(t101.C101_party_nm_en,t101.C101_PARTY_NM),t101.C101_PARTY_NM)) NAME");

      sbQuery.append(" FROM t101_party t101,t101_party_invoice_details t101a  ");

      sbQuery
          .append(" WHERE c901_party_type = '26230725' AND t101.c101_party_id = t101a.c101_party_id and t101a.c101_consolidated_bill_flag = 'Y' ");

      sbQuery.append(" AND t101.c101_void_fl IS NULL AND t101a.c101_void_fl IS NULL");
      sbQuery.append(" AND t101a.c901_invoice_cust_type = '106154' "); // 106154 -Dealer
      sbQuery.append(" AND t101.C1900_COMPANY_ID = '" + getCompId()
          + "' AND NVL(t101.c101_active_fl,'Y') = 'Y' ");
    }

    if (strBillingCustomer.equals("") || strBillingCustomer.equals("0")) {
      sbQuery.append(" union all ");
    }
    if (strBillingCustomer.equals("") || strBillingCustomer.equals("0")
        || strBillingCustomer.equals("106155")) {
      sbQuery
          .append(" SELECT   t704.c704_account_id ID, DECODE("
              + getComplangid()
              + ",'103097',NVL(t704.c704_account_nm_en,t704.c704_account_nm),t704.c704_account_nm) NAME ");

      sbQuery.append("  FROM t704_account t704,(SELECT t704A.C704_ACCOUNT_ID , ");
      sbQuery
          .append("  MAX (DECODE (C901_ATTRIBUTE_TYPE, '106201',C704A_ATTRIBUTE_VALUE)) CONSOLID_BILL, ");
      sbQuery
          .append("  MAX (DECODE (C901_ATTRIBUTE_TYPE, '106200',C704A_ATTRIBUTE_VALUE)) INV_CUST_TYPE ");
      sbQuery.append(" FROM t704a_account_attribute t704a ");

      sbQuery.append(" WHERE c704a_void_fl     IS NULL ");

      sbQuery.append(" GROUP BY C704_ACCOUNT_ID)t704a  WHERE c704_void_fl IS NULL");

      sbQuery.append("  AND t704.c704_account_id = t704a.c704_account_id AND c1900_company_id = "
          + getCompId() + "");

      sbQuery.append(" AND NVL(c704_active_fl ,'Y') = 'Y' ");

      sbQuery.append(" AND t704a.CONSOLID_BILL = 'Y' AND  t704a.INV_CUST_TYPE = '106155' "); // 106155
                                                                                             // -Direct
                                                                                             // Account

    }
    sbQuery.append("ORDER BY NAME ");

    log.debug(" sbQuery.toString() " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }

  /**
   * fetchInvCloseDate - This method is used to get the Invoice Close Date for the selected
   * dealer/account
   * 
   * @author
   * @param HashMap hmParam
   * @return ArrayList alReturn
   */
  public HashMap fetchInvCloseDate(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();

    String strCustomerId = GmCommonClass.parseNull((String) hmParam.get("SELECTEDNAMELIST"));

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT c901_invoice_closing_date invclosingdate FROM t101_party_invoice_details WHERE c101_party_id= '"
            + strCustomerId + "' AND c101_void_fl IS NULL");
    sbQuery.append(" UNION ");
    sbQuery
        .append(" SELECT to_number(c704a_attribute_value) invclosingdate FROM t704a_account_attribute WHERE c704_account_id= '"
            + strCustomerId + "' AND c901_attribute_type = '106202' AND c704a_void_fl IS NULL");

    log.debug(" Inv Close Date sbQuery.toString() " + sbQuery.toString());
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());


    return hmReturn;
  }

  /**
   * fetchBillCustType - This method is used to get the Invoice Close Date for the selected
   * dealer/account
   * 
   * @author
   * @param HashMap hmParam
   * @return ArrayList alReturn
   */
  public HashMap fetchBillCustType(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();

    String strCustomerId = GmCommonClass.parseNull((String) hmParam.get("SELECTEDNAMELIST"));

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT c901_invoice_cust_type inv_cust_type FROM t101_party_invoice_details WHERE c101_party_id= '"
            + strCustomerId + "' AND c101_void_fl IS NULL");
    sbQuery.append(" UNION ");
    sbQuery
        .append(" SELECT to_number(c704a_attribute_value) inv_cust_type FROM t704a_account_attribute WHERE c704_account_id= '"
            + strCustomerId + "' AND c901_attribute_type = '106200' AND c704a_void_fl IS NULL");

    log.debug(" Bill Cust Type sbQuery.toString() " + sbQuery.toString());
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());


    return hmReturn;
  }


  /**
   * loadPendingBalance - This method is used to fetch opening balance details for dealer/account
   * 
   * @author Mahavishnu
   * @param HashMap hmParam
   * @return ArrayList alReturn
   */
  public ArrayList loadPendingBalance(HashMap hmParam) throws AppError {


    ArrayList alReturn = new ArrayList();
    String strCompanyID = getCompId();
    Date dtFromDate = (Date) hmParam.get("FROMDATE");
    Date dtToDate = (Date) hmParam.get("TODATE");
    String strSalesOrders = "";
    String strPayments = "";

    String strfromdate =
        GmCommonClass.parseNull(GmCommonClass.getStringFromDate(dtFromDate, getCompDateFmt()));
    String strToDate =
        GmCommonClass.parseNull(GmCommonClass.getStringFromDate(dtToDate, getCompDateFmt()));

    String strInvCloseDt = GmCommonClass.parseNull((String) hmParam.get("INVOICECLOSINGDATE"));
    String strBillCust = GmCommonClass.parseNull((String) hmParam.get("BILLINGCUSTOMER"));
    String strCustomerId = GmCommonClass.parseNull((String) hmParam.get("SELECTEDNAMELIST"));


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT BILL_CUST,BILL_CUST_ID,dealerid,dealernm,accid,  ");

    sbQuery.append(" accname,transid,parent_transid,ord_dt,trans_date,trans_amt,sales_amount, ");

    sbQuery.append(" holdfl,CURRENCY,ordtypeid,ordtype,EMAILREQ, custpo FROM ( ");

    if (strBillCust.equals("106154") || strBillCust.equals("0")) {// 106154 - dealer

      sbQuery
          .append(" SELECT get_code_name(106154) BILL_CUST, to_char(106154) BILL_CUST_ID,  t101.c101_party_id dealerid,  get_party_name(t101.c101_party_id) dealernm,'' accid,  '' accname,  get_code_name(106182) transid,  '' parent_transid,  ");

      sbQuery
          .append("  '' ord_dt ,  TO_DATE(sysdate) trans_date,  GET_DEALER_ACCT_INV_UNPAID_AMT(t101.c101_party_id,'',TO_DATE('"
              + strfromdate + "','" + getCompDateFmt() + "')) trans_amt , ");

      sbQuery.append("GET_DEALER_ACCT_SALE_IN_MONTH('',t101.c101_party_id,'',TO_DATE('"
          + strfromdate + "','" + getCompDateFmt() + "'),TO_DATE('" + strToDate + "','"
          + getCompDateFmt() + "'),NULL) sales_amount, ");

      sbQuery
          .append(" '' holdfl,  GET_CODE_NAME(T1900.C901_TXN_CURRENCY) CURRENCY ,  '106182' ordtypeid ,  get_code_name(106182) ordtype,  '' EMAILREQ, ");

      sbQuery
          .append(" '' custpo  from t101_party t101,t1900_company t1900,t101_party_invoice_details t101p ");

      sbQuery.append(" WHERE t101.c101_party_id= t101p.c101_party_id ");

      sbQuery.append(" AND  t101p.c901_invoice_closing_date='" + strInvCloseDt + "' ");

      if ((!strCustomerId.equals("0")) && (!strCustomerId.equals(""))) {
        sbQuery.append(" AND  t101.c101_party_id='" + strCustomerId + "' ");
      }

      sbQuery.append(" AND t101.c901_party_type = '26230725' ");

      sbQuery.append(" AND t101.c101_void_fl is null ");

      sbQuery.append(" AND t101.C1900_COMPANY_ID      = " + getCompId() + "  ");

      sbQuery.append(" AND t101.c1900_company_id = t1900.c1900_company_id ");

      sbQuery.append("  AND t1900.c1900_void_fl is null ");

      sbQuery.append("  AND t101p.c101_void_fl is null ");
    }

    if (strBillCust.equals("0")) {
      sbQuery.append("  UNION ALL ");
    }

    if (strBillCust.equals("106155") || strBillCust.equals("0")) { // 106155- direct account

      sbQuery
          .append(" SELECT get_code_name(106155) BILL_CUST, to_char(106155) BILL_CUST_ID,  to_number('') dealerid,  '' dealernm,  t704.c704_account_id accid,  get_account_name (t704.c704_account_id) accname,  get_code_name(106182) transid, ");

      sbQuery
          .append(" '' parent_transid,  '' ord_dt ,  TO_DATE(sysdate) trans_date,  GET_DEALER_ACCT_INV_UNPAID_AMT('',t704.c704_account_id,TO_DATE('"
              + strfromdate + "','" + getCompDateFmt() + "')) trans_amt , ");

      sbQuery.append("GET_DEALER_ACCT_SALE_IN_MONTH('','',t704.c704_account_id,TO_DATE('"
          + strfromdate + "','" + getCompDateFmt() + "'),TO_DATE('" + strToDate + "','"
          + getCompDateFmt() + "'),NULL) sales_amount, "); // PMT-40781 excluded orders are listing

      sbQuery
          .append(" '' holdfl,  GET_CODE_NAME(T1900.C901_TXN_CURRENCY) CURRENCY ,  '106182' ordtypeid ,  get_code_name(106182) ordtype,  '' EMAILREQ,   ");

      sbQuery
          .append(" '' custpo  from t704_account t704,t1900_company t1900,t704a_account_attribute t704a ");

      sbQuery.append(" where t704.c704_account_id=t704a.c704_account_id ");

      if ((!strCustomerId.equals("0")) && (!strCustomerId.equals(""))) {
        sbQuery.append(" and t704.c704_account_id='" + strCustomerId + "'  ");
      }

      sbQuery.append(" and t704a.c704a_attribute_value = '" + strInvCloseDt + "' ");

      sbQuery.append(" and t704a.c901_attribute_type = '106202' ");

      sbQuery.append(" and t704.c704_void_fl is null ");

      sbQuery.append(" AND t704.C1900_COMPANY_ID      = " + getCompId() + "  ");

      sbQuery.append(" and t704.c1900_company_id = t1900.c1900_company_id ");

      sbQuery.append("  and t1900.c1900_void_fl is null ");
    }

    sbQuery.append(" ) WHERE trans_amt != 0  AND sales_amount = 0");

    log.debug(" sbQuery.toString() " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }

  /**
   * generateInvoiceStatement - This method is used to generate invoice statement
   * 
   * @author Mahavishnu
   * @param HashMap hmParam
   * @return String strinvoiceId
   */
  public String generateInvoiceStatement(HashMap hmParam) throws AppError, ParseException {
    String strinvoiceId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    Date dtFromDate = (Date) hmParam.get("FROMDATE");
    Date dtToDate = (Date) hmParam.get("TODATE");


    String strInvCloseDt = GmCommonClass.parseNull((String) hmParam.get("INVOICECLOSINGDATE"));
    String strBillCust = GmCommonClass.parseNull((String) hmParam.get("BILLINGCUSTOMER"));
    String strDealerString = GmCommonClass.parseNull((String) hmParam.get("DEALERINPUTSTR"));
    String strAccountString = GmCommonClass.parseNull((String) hmParam.get("ACCINPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));


    gmDBManager.setPrepareString("gm_pkg_ar_invoice_statement.gm_sav_invoice_statement", 8);

    gmDBManager.setString(1, strInvCloseDt);
    gmDBManager.setDate(2, new java.sql.Date(dtFromDate.getTime()));
    gmDBManager.setDate(3, new java.sql.Date(dtToDate.getTime()));
    gmDBManager.setString(4, strDealerString);
    gmDBManager.setString(5, strAccountString);
    gmDBManager.setString(6, strBillCust);
    gmDBManager.setString(7, strUserId);
    gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strinvoiceId = GmCommonClass.parseNull(gmDBManager.getString(8));
    gmDBManager.commit();

    return strinvoiceId;
  }

}
