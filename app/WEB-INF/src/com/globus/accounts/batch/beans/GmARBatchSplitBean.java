package com.globus.accounts.batch.beans;

import java.util.HashMap;

/**
 * GmARBatchSplitBean--This method is used to split the batch id
 */

import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.OracleTypes;

public class GmARBatchSplitBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmARBatchSplitBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * saveARSplitBatch - will save the splitted batch for the Orders to raise AR report
   * 
   * @author
   * @param String strBatchId , strUserId
   * @return String
   */
  public String saveARSplitBatch(String strBatchId,String strUserId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strARBatchId = "";
    gmDBManager.setPrepareString("gm_pkg_ac_ar_split_batch_trans.gm_ar_split_multi_batch", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strBatchId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    strARBatchId = GmCommonClass.parseNull(gmDBManager.getString(3));
    log.debug("strARBatchId========= "+strARBatchId);
    log.error("strARBatchId --> " + strARBatchId);
    
    gmDBManager.commit();
    return strARBatchId;
  }
  
  /**
   * saveMultiSplitBatch - will save the splitted batch for the Orders to raise monthly invoice
   * 
   * @author
   * @param String strBatchId , strUserId
   * @return String
   */
  public String saveMultiSplitBatch(String strBatchId,String strUserId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBatchDealer = "";

    gmDBManager.setPrepareString("gm_pkg_ac_ar_split_batch_trans.gm_split_multi_batch", 3);
    gmDBManager.setString(1, strBatchId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strBatchDealer = GmCommonClass.parseNull(gmDBManager.getString(3));
    log.debug("strBatchDealer========= "+strBatchDealer);
    gmDBManager.commit();
    return strBatchDealer;
  }
  
  /**
   * processARSplitBatch -this method is used to save the status of batch and process the batch whatever we splitted in AR Project 
   * 
   * @author
   * @param String strBatchProcessService , strBatchMDBClass , hmParam
   * @return String
   */
  public String processARSplitBatch(HashMap hmParam,String strBatchProcessService,String strBatchMDBClass) throws AppError {
	  GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
	 String strErrorCustPos = gmARBatchBean.saveProcessInvoice(hmParam, strBatchProcessService,strBatchMDBClass);
	 log.error("strErrorCustPos --> " + strErrorCustPos);
	 return strErrorCustPos;
  }
  
  /**
   * fetchBatchEmailFl - fetch the email flag for batch
   * 
   * @author
   * @param String strBatchId 
   * @return String
   */
  public String fetchBatchEmailFl(String strBatchId) throws AppError {
	  GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strInputString = "";

	    
	    gmDBManager.setPrepareString("gm_pkg_ac_ar_split_batch_trans.gm_fet_batch_email_fl", 2);
	    gmDBManager.setString(1, strBatchId);
	    gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
	    gmDBManager.execute();
	    strInputString = GmCommonClass.parseNull(gmDBManager.getString(2));
	    gmDBManager.close();
	    return strInputString;
  }
 
}
