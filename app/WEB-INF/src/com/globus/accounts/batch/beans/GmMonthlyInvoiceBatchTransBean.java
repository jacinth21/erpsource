package com.globus.accounts.batch.beans;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmMonthlyInvoiceBatchTransBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmMonthlyInvoiceBatchTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * saveMonthlyInvPO - will update the PO for the Orders to raise monthly invoice
   * 
   * @author
   * @param String strPOInputStr - contains the Orderid ^ PO,String strUserID
   * @return String
   */
  public void saveMonthlyInvPO(String strPOInputStr, String strUserID) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_trans.gm_update_monthly_inv_po", 2);
    gmDBManager.setString(1, strPOInputStr);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }



}
