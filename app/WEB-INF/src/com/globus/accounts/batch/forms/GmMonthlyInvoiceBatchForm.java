package com.globus.accounts.batch.forms;

import java.util.ArrayList;
import java.util.Date;

import com.globus.common.forms.GmCommonForm;


public class GmMonthlyInvoiceBatchForm extends GmCommonForm {
	private Date fromDate=null;
	private Date toDate=null;
	private String gridData="";
	private String billingCustomer="";
	private String[] checkSelectedTransType = new String [10];
	private String selectedTransType="";
	private String invoiceClosingDate = "";
	private String selectedNameList = "";
	private String strBatchId = "";
	private String partyType="";
	private String selectAllAccount="";
	private String selectedNameId="";
	private String transactionType="";
    private String dealerInputStr = "";
    private String accInputStr = "";
    private String poInputStr = "";
    private String strAccessFlag = "";
	
	
    ArrayList alBillingCustomer = new ArrayList();
	ArrayList alTransactionType = new ArrayList();
	ArrayList alInvoiceClosingDate = new ArrayList();
	ArrayList alNameList = new ArrayList();

	/**
	 * @return the checkSelectedTransType
	 */
	public String[] getCheckSelectedTransType() {
		return checkSelectedTransType;
	}
	/**
	 * @param checkSelectedTransType the checkSelectedTransType to set
	 */
	public void setCheckSelectedTransType(String[] checkSelectedTransType) {
		this.checkSelectedTransType = checkSelectedTransType;
	}
	/**
	 * @return the selectedNameId
	 */
	public String getSelectedNameId() {
		return selectedNameId;
	}
	/**
	 * @param selectedNameId the selectedNameId to set
	 */
	public void setSelectedNameId(String selectedNameId) {
		this.selectedNameId = selectedNameId;
	}
	/**
	 * @return the selectedNameList
	 */
	public String getSelectedNameList() {
		return selectedNameList;
	}
	/**
	 * @param selectedNameList the selectedNameList to set
	 */
	public void setSelectedNameList(String selectedNameList) {
		this.selectedNameList = selectedNameList;
	}
	/**
	 * @return the alNameList
	 */
	public ArrayList getAlNameList() {
		return alNameList;
	}
	/**
	 * @param alNameList the alNameList to set
	 */
	public void setAlNameList(ArrayList alNameList) {
		this.alNameList = alNameList;
	}
	/**
	 * @return the transactionType
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * @param transactionType the transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * @return the selectAllAccount
	 */
	public String getSelectAllAccount() {
		return selectAllAccount;
	}
	/**
	 * @param selectAllAccount the selectAllAccount to set
	 */
	public void setSelectAllAccount(String selectAllAccount) {
		this.selectAllAccount = selectAllAccount;
	}
	
	 
	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}
	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	
	/**
	 * @return the partyType
	 */
	public String getPartyType() {
		return partyType;
	}
	/**
	 * @param partyType the partyType to set
	 */
	public void setPartyType(String partyType) {
		this.partyType = partyType;
	}
	
	/**
	 * @return the invoiceClosingDate
	 */
	public String getInvoiceClosingDate() {
		return invoiceClosingDate;
	}
	/**
	 * @param invoiceClosingDate the invoiceClosingDate to set
	 */
	public void setInvoiceClosingDate(String invoiceClosingDate) {
		this.invoiceClosingDate = invoiceClosingDate;
	}
	
	
	
	/**
	 * @return the alInvoiceClosingDate
	 */
	public ArrayList getAlInvoiceClosingDate() {
		return alInvoiceClosingDate;
	}
	/**
	 * @param alInvoiceClosingDate the alInvoiceClosingDate to set
	 */
	public void setAlInvoiceClosingDate(ArrayList alInvoiceClosingDate) {
		this.alInvoiceClosingDate = alInvoiceClosingDate;
	}


	/**
	 * @return the alTransactionType
	 */
	public ArrayList getAlTransactionType() {
		return alTransactionType;
	}
	/**
	 * @param alTransactionType the alTransactionType to set
	 */
	public void setAlTransactionType(ArrayList alTransactionType) {
		this.alTransactionType = alTransactionType;
	}
	/**
	 * @return the alBillingCustomer
	 */
	public ArrayList getAlBillingCustomer() {
		return alBillingCustomer;
	}
	/**
	 * @param alBillingCustomer the alBillingCustomer to set
	 */
	public void setAlBillingCustomer(ArrayList alBillingCustomer) {
		this.alBillingCustomer = alBillingCustomer;
	}
	
	/**
	 * @return the billingCustomer
	 */
	public String getBillingCustomer() {
		return billingCustomer;
	}
	/**
	 * @param billingCustomer the billingCustomer to set
	 */
	public void setBillingCustomer(String billingCustomer) {
		this.billingCustomer = billingCustomer;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the fromDate
	 */
	/**
	 * @return the selectedTransType
	 */
	public String getSelectedTransType() {
		return selectedTransType;
	}
	/**
	 * @param selectedTransType the selectedTransType to set
	 */
	public void setSelectedTransType(String selectedTransType) {
		this.selectedTransType = selectedTransType;
	}
	 public String getStrBatchId() {
		    return strBatchId;
		  }

	public void setStrBatchId(String strBatchId) {
		    this.strBatchId = strBatchId;
		  }
			
	public String getDealerInputStr() {
		    return dealerInputStr;
		  }

    public void setDealerInputStr(String dealerInputStr) {
		    this.dealerInputStr = dealerInputStr;
		  }

	public String getAccInputStr() {
		    return accInputStr;
		  }

	public void setAccInputStr(String accInputStr) {
		    this.accInputStr = accInputStr;
		  }

	public String getPoInputStr() {
		    return poInputStr;
		  }

	public void setPoInputStr(String poInputStr) {
		    this.poInputStr = poInputStr;
		  }
	/**
     * @return the strAccessFlag
     */
    public String getStrAccessFlag() {
      return strAccessFlag;
    }
    /**
     * @param strAccessFlag the strAccessFlag to set
     */
    public void setStrAccessFlag(String strAccessFlag) {
      this.strAccessFlag = strAccessFlag;
    }

	
	
}
