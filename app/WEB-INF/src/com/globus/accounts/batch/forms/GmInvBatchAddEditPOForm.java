package com.globus.accounts.batch.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCancelForm;

public class GmInvBatchAddEditPOForm extends GmCancelForm {

  private String inputString = "";
  private String gridData = "";
  private String screenType = "";
  private String invBatchID = "";
  private String refType = "";
  private String invBatchStatus = "";
  private String strSuccessMsg = "";
  private String strAccountIDs = "";
  private String strErrorCustPos = "";

  private String strCompCurrency = "";

  private ArrayList alCompCurrency = new ArrayList();


  /**
   * @return the inputString
   */
  public String getInputString() {
    return inputString;
  }

  /**
   * @param inputString the inputString to set
   */
  public void setInputString(String inputString) {
    this.inputString = inputString;
  }

  /**
   * @return the gridData
   */
  public String getGridData() {
    return gridData;
  }

  /**
   * @param gridData the gridData to set
   */
  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  /**
   * @return the screenType
   */
  public String getScreenType() {
    return screenType;
  }

  /**
   * @param screenType the screenType to set
   */
  public void setScreenType(String screenType) {
    this.screenType = screenType;
  }

  /**
   * @return the refType
   */
  public String getRefType() {
    return refType;
  }

  /**
   * @param refType the refType to set
   */
  public void setRefType(String refType) {
    this.refType = refType;
  }

  /**
   * @return the batchID
   */
  public String getInvBatchID() {
    return invBatchID;
  }

  /**
   * @param batchID the batchID to set
   */
  public void setInvBatchID(String batchID) {
    this.invBatchID = batchID;
  }

  /**
   * @return the invBatchStatus
   */
  public String getInvBatchStatus() {
    return invBatchStatus;
  }

  /**
   * @param invBatchStatus the invBatchStatus to set
   */
  public void setInvBatchStatus(String invBatchStatus) {
    this.invBatchStatus = invBatchStatus;
  }

  /**
   * @return the strSuccessMsg
   */
  public String getStrSuccessMsg() {
    return strSuccessMsg;
  }

  /**
   * @param strSuccessMsg the strSuccessMsg to set
   */
  public void setStrSuccessMsg(String strSuccessMsg) {
    this.strSuccessMsg = strSuccessMsg;
  }

  /**
   * @return the strAccountIDs
   */
  public String getStrAccountIDs() {
    return strAccountIDs;
  }

  /**
   * @param strAccountIDs the strAccountIDs to set
   */
  public void setStrAccountIDs(String strAccountIDs) {
    this.strAccountIDs = strAccountIDs;
  }

  /**
   * @return the strErrorCustPos
   */
  public String getStrErrorCustPos() {
    return strErrorCustPos;
  }

  /**
   * @param strErrorCustPos the strErrorCustPos to set
   */
  public void setStrErrorCustPos(String strErrorCustPos) {
    this.strErrorCustPos = strErrorCustPos;
  }

  /**
   * @return the strCompCurrency
   */
  public String getStrCompCurrency() {
    return strCompCurrency;
  }

  /**
   * @param strCompCurrency the strCompCurrency to set
   */
  public void setStrCompCurrency(String strCompCurrency) {
    this.strCompCurrency = strCompCurrency;
  }

  /**
   * @return the alCompCurrency
   */
  public ArrayList getAlCompCurrency() {
    return alCompCurrency;
  }

  /**
   * @param alCompCurrency the alCompCurrency to set
   */
  public void setAlCompCurrency(ArrayList alCompCurrency) {
    this.alCompCurrency = alCompCurrency;
  }


}
