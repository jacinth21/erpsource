package com.globus.accounts.batch.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmInvBatchAddAccountForm extends GmCommonForm {

  private String inputString = "";
  private String gridData = "";
  private String inputStr = "";
  private String accountID = "";
  private String batchID = "";
  private String strCompCurrency = "";

  private ArrayList alCompCurrency = new ArrayList();


  /**
   * @return the inputString
   */
  public String getInputString() {
    return inputString;
  }

  /**
   * @param inputString the inputString to set
   */
  public void setInputString(String inputString) {
    this.inputString = inputString;
  }

  /**
   * @return the gridData
   */
  public String getGridData() {
    return gridData;
  }

  /**
   * @param gridData the gridData to set
   */
  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  /**
   * @return the inputStr
   */
  public String getInputStr() {
    return inputStr;
  }

  /**
   * @param inputStr the inputStr to set
   */
  public void setInputStr(String inputStr) {
    this.inputStr = inputStr;
  }

  /**
   * @return the accountID
   */
  public String getAccountID() {
    return accountID;
  }

  /**
   * @param accountID the accountID to set
   */
  public void setAccountID(String accountID) {
    this.accountID = accountID;
  }

  /**
   * @return the batchID
   */
  public String getBatchID() {
    return batchID;
  }

  /**
   * @param batchID the batchID to set
   */
  public void setBatchID(String batchID) {
    this.batchID = batchID;
  }

  /**
   * @return the strCompCurrency
   */
  public String getStrCompCurrency() {
    return strCompCurrency;
  }

  /**
   * @param strCompCurrency the strCompCurrency to set
   */
  public void setStrCompCurrency(String strCompCurrency) {
    this.strCompCurrency = strCompCurrency;
  }

  /**
   * @return the alCompCurrency
   */
  public ArrayList getAlCompCurrency() {
    return alCompCurrency;
  }

  /**
   * @param alCompCurrency the alCompCurrency to set
   */
  public void setAlCompCurrency(ArrayList alCompCurrency) {
    this.alCompCurrency = alCompCurrency;
  }
}
