package com.globus.accounts.fieldaudit.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmFAVarianceRptBean {
	GmCommonClass gmCommon = new GmCommonClass();
	GmLogger log = GmLogger.getInstance();
	/**
	 * fetchVarianceByDistRpt  	This method will fetch the Variance Report By Distributor and Global Variance Report Set.
	 * @param 					HashMap
	 * @return 					RowSetDynaClass
	 * @exception 				AppError
	 */
	public RowSetDynaClass fetchVarianceByDistRpt(HashMap hmParam) throws AppError {

		String strAuditId     = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
		String strDistId      = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
		String strLockedFrmDt = GmCommonClass.parseNull((String) hmParam.get("LOCKEDFRMDT"));
		String strLockedToDt  = GmCommonClass.parseNull((String) hmParam.get("LOCKEDTODT"));		
		RowSetDynaClass rdVarianceRptByDist = null;		
		GmDBManager gmDBManager = new GmDBManager();		
		gmDBManager.setPrepareString("gm_pkg_ac_fa_variance_rpt.gm_fetch_variance_by_dist", 5);
		gmDBManager.setString(1, strAuditId);
		gmDBManager.setString(2, strDistId);
		gmDBManager.setString(3, strLockedFrmDt);
		gmDBManager.setString(4, strLockedToDt);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.execute();
		rdVarianceRptByDist = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));
		gmDBManager.close();
		return rdVarianceRptByDist;
	}
	
	/**
	 * fetchVarianceBySetRpt  	This method will fetch the Variance Report By Set and Global Variance Report By Distributor.
	 * @param 					HashMap
	 * @return 					RowSetDynaClass
	 * @exception 				AppError
	 */	
	public RowSetDynaClass fetchVarianceBySetRpt(HashMap hmParam) throws AppError {

		String strAuditId     = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
		String strSetId       = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		String strRegionId = GmCommonClass.parseNull((String) hmParam.get("REGIONID"));
		String strLockedFrmDt = GmCommonClass.parseNull((String) hmParam.get("LOCKEDFRMDT"));
		String strLockedToDt  = GmCommonClass.parseNull((String) hmParam.get("LOCKEDTODT"));
		strRegionId = strRegionId.equals("") ? "0" : strRegionId;
		RowSetDynaClass rdVarianceRptByDist = null;		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_fa_variance_rpt.gm_fetch_variance_by_set",6);
		gmDBManager.setString(1, strAuditId);
		gmDBManager.setString(2, strSetId);
		gmDBManager.setString(3, strRegionId);
		gmDBManager.setString(4, strLockedFrmDt);
		gmDBManager.setString(5, strLockedToDt);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.execute();
		rdVarianceRptByDist = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(6));
		gmDBManager.close();
		return rdVarianceRptByDist;

	}	
}
