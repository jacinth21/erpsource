package com.globus.accounts.fieldaudit.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import oracle.jdbc.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmFAFlagDeviationBean extends GmBean{	
	GmCommonClass gmCommon = new GmCommonClass();
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code
	

	public GmFAFlagDeviationBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	public GmFAFlagDeviationBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}
	
	/*   This Method will fetch all Details for the verify Upload report 
	 * 
	 * @param String .
	 * 
	 * @return rdVerifyUpload  
	 * @throws com.globus.common.beans.AppError	 * 
	 */
	
	public RowSetDynaClass fetchVerifyUploadData(HashMap hmParam) throws AppError {
		String strStatus = "";
		String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
		String strAuditorId = GmCommonClass.parseNull((String) hmParam.get("AUDITORID"));
		String strEntryTyp = GmCommonClass.parseZero((String) hmParam.get("ENTRYTYP"));
		String strOpt = GmCommonClass.parseZero((String) hmParam.get("STROPT"));
		RowSetDynaClass rdVerifyUpload = null;
		if(strOpt.equals("loadRpt")){
			strStatus = "SUMMARY";
		}		
		// set all required string variables to procedure & get the resulting output.
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_fa_flag_deviation.gm_fch_uploaded_data", 6);
		gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
		gmDBManager.setString(2, strDistId);
		gmDBManager.setString(3, GmCommonClass.parseNull(strAuditorId));
		gmDBManager.setInt(4, Integer.parseInt(strEntryTyp));
		gmDBManager.setString(5, GmCommonClass.parseNull(strStatus));
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.execute();
		rdVerifyUpload = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(6));
		gmDBManager.close();
		return rdVerifyUpload;
	}
	
	
	/*   This Method will get the Audit Entry Details for the Passed in Parameters
	 * 
	 * @param String .
	 * 
	 * @return hmReturn Containing the Audit Entry For the passed in AuditEntryID 
	 * @throws com.globus.common.beans.AppError	 * 
	 */
			
			
	public HashMap fetchEditUploadData(HashMap hmParam)throws AppError{
		
		HashMap hmReturn = new HashMap();
		
		String strAuditEntryID = GmCommonClass.parseNull((String) hmParam.get("AUDITENTRYID"));
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_fa_flag_deviation.gm_fch_edit_uploaded_data", 2);
		gmDBManager.setString(1, strAuditEntryID);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return hmReturn;
	}
	
	
	/*   This Method will fetch location type Details for the Passed in Parameters
	 * 
	 * 
	 * @return alLocation  
	 * @throws com.globus.common.beans.AppError	 * 
	 */
			
			
	public ArrayList fetchLocation(String locType)throws AppError{
		
		GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
		GmSalesBean gmSalesBean = new GmSalesBean();
		ArrayList alLocation = new ArrayList();
		
		if (locType.equals("6020"))//distributor
			alLocation = gmCustomerBean.getDistributorList("") ;
		else if (locType.equals("6022"))//sales rep
			alLocation = gmCustomerBean.getRepList("");
		else if (locType.equals("6021"))//hospital
			 alLocation = gmSalesBean.reportAccount();
				
		return alLocation;
	}
	
	
	/*   This Method will fetch all the location Details for the Passed in Parameters
	 * 
	 * 
	 * @return alReturn 
	 * @throws com.globus.common.beans.AppError	 * 
	 */
			
			
	public ArrayList fetchLocationInfo(String locType,String locationID)throws AppError{
		
		ArrayList alReturn = new ArrayList();
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_fa_flag_deviation.gm_fch_location_info", 3);
		gmDBManager.setString(1, locationID);
		gmDBManager.setString(2, locType);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return alReturn;
	}
	
	/*   This Method will save all the the edited data.
	 * 
	 * @param hmParam .
	 * 
	 * @return alReturn 
	 * @throws com.globus.common.beans.AppError	 * 
	 */
	
	public HashMap saveEditUploadData(HashMap hmParam)throws AppError{
		
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		String strLockedSet = GmCommonClass.parseNull((String) hmParam.get("LOCKEDSET"));
		String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		
		if (strOpt.equals("savelockedset") || strLockedSet.equalsIgnoreCase("on"))
		{
			strLockedSet ="Y";
		}
		
		gmDBManager.setPrepareString("gm_pkg_ac_fa_flag_deviation.gm_sav_edit_uploaded_data", 18);
		gmDBManager.setString(1,(String)hmParam.get("TAGID"));
		gmDBManager.setString(2,(String)hmParam.get("PARTNUMBER"));
		gmDBManager.setString(3,(String)hmParam.get("CONTROLNUMBER"));
		gmDBManager.setString(4,(String)hmParam.get("SETID"));
		gmDBManager.setString(5,(String)hmParam.get("COMMENTS"));
		gmDBManager.setString(6,(String)hmParam.get("STATUSID"));
		gmDBManager.setString(7,(String)hmParam.get("DISTID"));
		gmDBManager.setString(8,(String)hmParam.get("LOCTYP"));
		gmDBManager.setString(9,(String)hmParam.get("LOCATIONID"));
		gmDBManager.setString(10,(String)hmParam.get("LOCATIONDET"));
		gmDBManager.setString(11,(String)hmParam.get("FLAGDETID"));
		gmDBManager.setString(12,(String)hmParam.get("FLAGTYPE"));
		gmDBManager.setString(13,(String)hmParam.get("LOCATIONCOMMENTS"));
		gmDBManager.setString(14,(String)hmParam.get("COUNTEDDATE"));
		gmDBManager.setString(15,(String)hmParam.get("USERID"));
		gmDBManager.setString(16,(String)hmParam.get("AUDITENTRYID"));
		gmDBManager.setString(17, strLockedSet);
		gmDBManager.setString(18, strOpt);
		
		gmDBManager.execute();
		gmDBManager.commit();
		gmDBManager.close();
		return hmReturn;
	}
	

	/*
	 * This Method will Fetch the Flag variance reports (Matching, Positive and negative reports).
	 * 
	 * @param hmParam .
	 * 
	 * @return HashMap
	 * 
	 * @throws AppError *
	 */
	
public HashMap fetchFlagVarianceRpt (HashMap hmParam) throws AppError{
	    
	    String strAuditId=GmCommonClass.parseNull((String)hmParam.get("AUDITID"));
	    String strSetId=GmCommonClass.parseNull((String)hmParam.get("SETID"));
	    String strDistId=GmCommonClass.parseNull((String)hmParam.get("DISTID"));
	    String strPartNum = GmCommonClass.parseNull((String)hmParam.get("PNUM"));
	    if(strSetId.equals("null")){
	    	strSetId = "";
	    }

	    GmDBManager gmDBManager = new GmDBManager();
	    RowSetDynaClass rdMatchingRpt= null;
	    RowSetDynaClass rdNegativeRpt= null;
	    RowSetDynaClass rdPositiveRpt= null;
	    RowSetDynaClass rdPAssociatedReport= null;
	    RowSetDynaClass rdNAssociatedReport= null;
	    HashMap hmResult = new HashMap(); 
	    HashMap hmHeaderInfo = new HashMap();
	    HashMap hmAuditRpt = new HashMap();
	    
	    gmDBManager.setPrepareString("gm_pkg_ac_fa_flag_deviation.gm_fch_flag_variance", 11);
	    gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
	    gmDBManager.setString(2,GmCommonClass.parseNull(strDistId));
	    gmDBManager.setString(3, GmCommonClass.parseNull(strSetId));
	    gmDBManager.setString(4, GmCommonClass.parseNull(strPartNum));
	    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
	    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
	    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
	    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
	    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
	    gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
	    gmDBManager.registerOutParameter(11, OracleTypes.CURSOR);
	    gmDBManager.execute();

	    hmHeaderInfo=gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(5));
	    hmAuditRpt = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(6));
	    rdMatchingRpt = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(7));
	    rdPositiveRpt = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(8));
	    rdNegativeRpt = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(9));
	    rdPAssociatedReport = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(10));
	    rdNAssociatedReport = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(11));
	    gmDBManager.close();
	    
	    hmResult.put("HEADERINFO", hmHeaderInfo);
	    hmResult.put("AUDITREPORT", hmAuditRpt);
	    hmResult.put("MATCHINGREPORT", rdMatchingRpt);
	    hmResult.put("POSITIVEREPORT", rdPositiveRpt);
	    hmResult.put("NEGATIVEREPORT", rdNegativeRpt);
	    hmResult.put("PASSOCIATEDREPORT", rdPAssociatedReport);
	    hmResult.put("NASSOCIATEDREPORT", rdNAssociatedReport);

	    return hmResult;
	    
	}

	/*
	 * This Method to save the Flag variance reports (Positive and negative flagged data).
	 * 
	 * @param hmParam .
	 * 
	 * @return 
	 * 
	 * @throws AppError *
	 */
public void saveFlaggedVariance  (HashMap hmParam) throws AppError{
	String strAuditId=GmCommonClass.parseNull((String)hmParam.get("AUDITID"));
    String strSetId=GmCommonClass.parseNull((String)hmParam.get("SETID"));
    String strDistId=GmCommonClass.parseNull((String)hmParam.get("DISTID"));
    String strInput=GmCommonClass.parseNull((String)hmParam.get("STRINPUT"));
    String strUserId=GmCommonClass.parseNull((String)hmParam.get("USERID"));

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ac_fa_flag_deviation.gm_sav_flag_variance",5);
    gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
    gmDBManager.setString(2,GmCommonClass.parseNull(strDistId));
    gmDBManager.setString(3, GmCommonClass.parseNull(strSetId));
    gmDBManager.setString(4, GmCommonClass.parseNull(strInput));
    gmDBManager.setString(5, GmCommonClass.parseNull(strUserId));
    gmDBManager.execute();
    gmDBManager.commit();
}

	/*
	 * This Method to fetch the audit entry history reports.
	 * 
	 * @param Audit entry id .
	 * 
	 * @return RowsetDyna
	 * 
	 * @throws AppError *
	 */
public RowSetDynaClass fetchAuditEntryHistory(String strAuditEntryId) throws AppError {
	RowSetDynaClass rdHistory = null;
	GmDBManager gmDBManager = new GmDBManager();
	gmDBManager.setPrepareString("gm_pkg_ac_tag_info.gm_fch_fa_entry_history", 2);
	log.debug("strAuditEntryID --> "+strAuditEntryId);
	gmDBManager.setString(1, GmCommonClass.parseNull(strAuditEntryId));
	gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	gmDBManager.execute();
	rdHistory = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
	gmDBManager.close();
	return rdHistory;
}

}

