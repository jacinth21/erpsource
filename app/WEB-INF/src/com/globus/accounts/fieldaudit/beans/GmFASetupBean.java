package com.globus.accounts.fieldaudit.beans;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;

public class GmFASetupBean extends GmBean{	
	GmCommonClass gmCommon = new GmCommonClass();
	GmLogger log = GmLogger.getInstance();
	
	  public GmFASetupBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }
	
	  public GmFASetupBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	/**
	 * saveSetCost  This method will update the Cost to a particular Set Id's
	 * @param 		String strType
	 * @return 		String
	 * @exception 	AppError
	 */
	public String saveSetCost(HashMap hmParam) throws AppError {		
		String strMessage = "";
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));   	
        String strSetInputStr = GmCommonClass.parseNull((String) hmParam.get("HSETINPUTSTR"));
    	String strInputStr = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
        String strAttriType = GmCommonClass.parseNull((String) hmParam.get("ATTTYPE")); 
        GmDBManager gmDBManager = new GmDBManager ();		
		gmDBManager.setPrepareString("gm_pkg_ac_fa_setup.gm_ac_sav_set_costing", 5);
		gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
		gmDBManager.setString(1, strSetInputStr);
		gmDBManager.setString(2, strInputStr);
		gmDBManager.setString(3, strAttriType);
		gmDBManager.setString(4, strUserId);
		gmDBManager.execute(); 
		strMessage = GmCommonClass.parseNull(gmDBManager.getString(5));
		gmDBManager.commit();		
		return strMessage;		
	} //End of saveSetCost	
	/**
	 * saveSetCost  This method will fetch the Cost updated records for set id's
	 * @param 		String strType
	 * @return 		String
	 * @exception 	AppError
	 */
	public ArrayList fetchSetCostRpt(HashMap hmParam) throws AppError {
		ArrayList alReport = new ArrayList();
		String strAttriType = "11210";	
		GmDBManager gmDBManager = new GmDBManager ();		
		gmDBManager.setPrepareString("gm_pkg_ac_fa_setup.gm_ac_fch_set_cost", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strAttriType);
		gmDBManager.execute(); 
		alReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alReport;
	} //End of fetchSetCostRpt

	/**
	 * fetchAuditList  This method will fetch the Audit list
	 * @param 		none
	 * @return 		Array List
	 * @exception 	AppError
	 */
	public ArrayList fetchAuditList() throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_fa_setup.gm_fetch_audit_details", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));//   returnHashMap((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alReturn;
	}

	/**
	 * fetchAllAuditorName - This method will fetch all Auditor Information.
	 * @param - None 
	 * @return - ArrayList           
	 * @exception - AppError
	 */
	public ArrayList fetchAuditorName() throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_fa_setup.gm_fch_auditor_name", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alReturn;
	}
	/**
	 * fetchAuditRegionSetupDetails - This method will fetch all Distributor Information.
	 * @param - String
	 * @return - ArrayList           
	 * @exception - AppError
	 */
	public ArrayList fetchAuditRegionSetupDetails(String strRegion,String strAuditorId) throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_fa_setup.gm_fch_audit_region_setup_dtls", 3);
		gmDBManager.setString(1, strRegion);
		gmDBManager.setString(2, strAuditorId);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return alReturn;
	}
	/**
	 * fetchAuditSetupDetails - This method will fetch all Audit setup details.
	 * @param - Hash Map 
	 * @return - Hash map           
	 * @exception - AppError
	 */
	public HashMap fetchAuditSetupDetails(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		HashMap hmReturn = new HashMap();
		HashMap hmAuditDtls = new HashMap();
		ArrayList alLockedFieldSales = new ArrayList();
		String strSetIds = "";
		String strParts = "";
		String strAuditId = GmCommonClass.parseNull((String)hmParam.get("AUDITID"));
		
		gmDBManager.setPrepareString("gm_pkg_ac_fa_setup.gm_fch_audit_setup_details", 5);
		gmDBManager.setString(1, strAuditId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
		gmDBManager.execute();
		hmAuditDtls = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		alLockedFieldSales = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		strSetIds = gmDBManager.getString(4);
		strParts = gmDBManager.getString(5);
		gmDBManager.close();
		
		hmReturn.put("AUDITSETUPINFO", hmAuditDtls);
		hmReturn.put("LOCKEDDIST", alLockedFieldSales);
		hmReturn.put("SETIDS", strSetIds);
		hmReturn.put("PNUMS", strParts);
		
		return hmReturn;
	}
	/**
	 * saveAuditDetails - This method will save - Audit setup details.
	 * @param - Hash Map 
	 * @return - Hash Map           
	 * @exception - AppError and Exception
	 */
	public HashMap saveAuditDetails(HashMap hmParam) throws AppError,Exception {
		GmDBManager gmDBManager = new GmDBManager();
		HashMap hmReturn = new HashMap();
		HashMap hmAuditSetupDtls = new HashMap();
		ArrayList alLockedFieldSales = new ArrayList();
		
		log.debug("saveAuditDetails called "+hmParam);
		String strParts = "";
		String strOutSetIds = "";
		String strInvalidSet = "";
		String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
		String strAuditName = GmCommonClass.parseNull((String) hmParam.get("AUDITNAME"));
		String strPrimaryAuditor = GmCommonClass.parseNull((String) hmParam.get("PRIMARYAUDITORID"));
		String strAuditStartDt = GmCommonClass.parseNull((String) hmParam.get("AUDITSTARTDT"));
		//String strAuditStatus = GmCommonClass.parseNull((String) hmParam.get("AUDITSTATUS"));
		String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		String strSetIds = GmCommonClass.parseNull((String) hmParam.get("SETIDS"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strAppDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
		Date dtAuditStartDt = (Date) GmCommonClass.getStringToDate(strAuditStartDt, strAppDateFmt);
		
		gmDBManager.setPrepareString("gm_pkg_ac_fa_setup.gm_ac_sav_audit_details",13);
		gmDBManager.setInt(1, Integer.parseInt(strAuditId));
		gmDBManager.setString(2, strAuditName);
		gmDBManager.setString(3, strPrimaryAuditor);
		gmDBManager.setDate(4, dtAuditStartDt);
		gmDBManager.setString(5, strInputString);
		gmDBManager.setString(6, strSetIds);
		gmDBManager.setString(7, strOpt);
		gmDBManager.setString(8, strUserId);
		gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(11, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(12, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(13, OracleTypes.VARCHAR);
		gmDBManager.execute();
		hmAuditSetupDtls = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(9));
		alLockedFieldSales = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(10));
		strOutSetIds = GmCommonClass.parseNull((String)gmDBManager.getString(11));
		strParts = GmCommonClass.parseNull((String)gmDBManager.getString(12));
		strInvalidSet = GmCommonClass.parseNull((String)gmDBManager.getString(13));
		gmDBManager.commit();				
		log.debug("saveAuditDetails executed successfully");
		
		if(!strInvalidSet.equals("")){
			strInvalidSet = strInvalidSet.substring(0, strInvalidSet.length()-1);
		}
		
		hmReturn.put("AUDITSETUPINFO", hmAuditSetupDtls);
		hmReturn.put("LOCKEDDIST", alLockedFieldSales);
		hmReturn.put("SETIDS", strOutSetIds);
		hmReturn.put("PNUMS", strParts);
		hmReturn.put("INVALIDSET", strInvalidSet);
		
		return hmReturn;
	}

	/**
	 * fetchTagQty - This method will return the tag quantity details based on the distributor.
	 * @param - String 
	 * @return - String           
	 * @exception - AppError
	 */
	public String fetchTagQty(String strDistId) throws AppError{
		String strTagQty = "";
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setFunctionString("get_field_sales_tag_qty", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strDistId);
		gmDBManager.execute();
		strTagQty = gmDBManager.getString(1);
		gmDBManager.close();
		
		return strTagQty;
	}
	
	/**
	 * validateAuditName - This method will check the audit name.
	 * @param - String 
	 * @return - String           
	 * @exception - AppError
	 */
	public String validateAuditName (String strAuditName) throws AppError{
		String strName = "";
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_fa_setup.gm_validate_audit_name",2);
		gmDBManager.setString(1, strAuditName);
		gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDBManager.execute();
		strName = gmDBManager.getString(2);
		gmDBManager.close();
		
		return strName;
	}

}
