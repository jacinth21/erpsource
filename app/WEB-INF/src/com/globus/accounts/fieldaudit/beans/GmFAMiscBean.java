package com.globus.accounts.fieldaudit.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmFAMiscBean {
	GmCommonClass gmCommon = new GmCommonClass();
	GmLogger log = GmLogger.getInstance();
	/**
	 * fetchReturnsCreditRpt  This method will fetch the all the valid RA #'s based on the Set Id and Dist ID
	 * @param 		hmParam
	 * @return 		RowSetDynaClass
	 * @exception 	AppError
	 */
	public RowSetDynaClass fetchReturnsCreditRpt(HashMap hmParam) throws AppError{		
		String strDistId 		  = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
		String strSetId 		  = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		GmDBManager gmDBManager   = new GmDBManager();
		RowSetDynaClass rdReturns = null;
		gmDBManager.setPrepareString("gm_pkg_ac_fa_variance_rpt.gm_fch_returns_credit_dtl", 3);
		gmDBManager.setString(1, strDistId);
		gmDBManager.setString(2, strSetId);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		rdReturns = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		return rdReturns;
		
	}

}
