/*
 * Description: This file is used for Reconciliation, Summary Report, Posting Error
 */
package com.globus.accounts.fieldaudit.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmFAReconciliationBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmFAReconciliationBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmFAReconciliationBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * fetchSummaryReport This method will fetch the Summary Report
   * 
   * @param HashMap hmParam
   * @return ArrayList alReport
   * @exception AppError Author Jignesh Shah
   */
  public HashMap fetchSummaryReport(HashMap hmParam) throws AppError, Exception {

    ArrayList alAuditInfo = new ArrayList();
    ArrayList alFieldSalesInfo = new ArrayList();
    HashMap hmReturn = new HashMap();

    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("SESSDATEFMT"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FASUMFROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("FASUMTODT"));

    Date dtlockedFromDT = GmCommonClass.getStringToDate(strFromDt, strDateFmt);
    Date dtlockedToDT = GmCommonClass.getStringToDate(strToDt, strDateFmt);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_fa_reconciliation.gm_fch_summary_report", 4);
    gmDBManager.setDate(1, dtlockedFromDT);
    gmDBManager.setDate(2, dtlockedToDT);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.execute();
    alAuditInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    alFieldSalesInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();

    hmReturn.put("ALAUDITINFO", alAuditInfo);
    hmReturn.put("ALFIELDSALESINFO", alFieldSalesInfo);

    return hmReturn;
  }

  /**
   * fetchApprReconDeviation This method will fetch the flagged data for Approving as weel as
   * Approving data for Reconciling.
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchApprReconDeviation(String strDeviationType) throws AppError {
    if (strDeviationType.equals("Approval")) {
      strDeviationType = "51036";
    } else if (strDeviationType.equals("Reconcile")) {
      strDeviationType = "51037";
    }
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_fa_reconciliation.gm_fch_appr_recon_deviation", 2);
    gmDBManager.setString(1, strDeviationType);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * saveApprReconDeviation This method will save the flagged data for Approving as weel as
   * Approving data for Reconciling.
   * 
   * @param HashMap
   * @return RowSetDynaClass
   * @exception AppError
   */
  public void saveApproveDeviation(HashMap hmParam) throws AppError {
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_fa_reconciliation.gm_sav_appr_deviation", 2);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * saveReconcileDeviation This method will save the flagged data for Reconciling.
   * 
   * @param HashMap
   * @exception AppError
   */
  public String saveReconcileDeviation(HashMap hmParam) throws AppError {

    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strTxnIds = "";
    savePostingError(hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_fa_reconciliation.gm_sav_reconcile_deviation", 3);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.execute();
    strTxnIds = gmDBManager.getString(3);
    gmDBManager.commit();
    // to send the Customer service email
    sendReconcileEmail();
    return strTxnIds;
  }

  public HashMap fetchConfirmationReport(HashMap hmParam) throws AppError {
    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    HashMap hmHeaderInfo = new HashMap();
    ArrayList alOverAllReport = new ArrayList();
    ArrayList alMissingReport = new ArrayList();
    ArrayList alExtraReport = new ArrayList();
    ArrayList alMatchingReport = new ArrayList();
    ArrayList alBorrowLentTransRpt = new ArrayList();
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_fa_reconciliation.gm_fetch_confirmation_report", 8);
    gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
    gmDBManager.setString(2, GmCommonClass.parseNull(strDistId));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmHeaderInfo = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    alOverAllReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    alMissingReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    alExtraReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
    alBorrowLentTransRpt = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    alMatchingReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(8));
    gmDBManager.close();
    hmReturn.put("HEADERINFO", hmHeaderInfo);
    hmReturn.put("OVERALLREPORT", alOverAllReport);
    hmReturn.put("MISSINGREPORT", alMissingReport);
    hmReturn.put("EXTRAREPORT", alExtraReport);
    hmReturn.put("MATCHINGREPORT", alMatchingReport);
    hmReturn.put("BORROWEDREPORT", alBorrowLentTransRpt);

    return hmReturn;
  }

  public HashMap fetchInvPendChargeReport(HashMap hmParam) throws AppError {
    ArrayList alReport = new ArrayList();
    HashMap hmResult = new HashMap();
    HashMap hmDetails = new HashMap();
    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strPdfOption = GmCommonClass.parseNull((String) hmParam.get("PDFACTION"));
    String strRefId = "";
    if (strPdfOption.equals("18582")) { // Pending Charges
      strRefId = "PENDING_CHARGE";
    } else {
      strRefId = "INVOICED_CHARGE"; // Charge Set,Charge Part
    }
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_fa_reconciliation.gm_fch_inv_pend_charge_rpt", 5);
    gmDBManager.setString(1, strAuditId);
    gmDBManager.setString(2, strDistId);
    gmDBManager.setString(3, strRefId);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    hmDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(5));
    gmDBManager.close();

    hmResult.put("PEND_INV_LIST", alReport);
    hmResult.put("HMDETAILS", hmDetails);

    return hmResult;
  }

  /**
   * saveApprReconDeviation This method is used to fetch data for Posting Error report.
   * 
   * @param HashMap
   * @return RowSetDynaClass
   * @exception AppError
   */
  public RowSetDynaClass fetchPostingErrorRpt(HashMap hmParam) throws AppError {

    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strstatus = GmCommonClass.parseNull((String) hmParam.get("STATUSID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass rdPostingError = null;

    gmDBManager.setPrepareString("gm_pkg_ac_fa_post_err.gm_fch_posting_error", 4);
    gmDBManager.setString(1, strAuditId);
    gmDBManager.setString(2, strDistId);
    gmDBManager.setString(3, strstatus);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdPostingError = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
    log.debug("rdPostingError in bean-->" + rdPostingError);
    gmDBManager.close();
    return rdPostingError;
  }

  /**
   * sendReconcileEmail This method will fetch the required parameter details for sending the Jasper
   * Email to Customer Service Team when Tag is gets Reconciled.
   * 
   * @param HashMap
   * @return ''
   * @exception AppError
   */
  public void sendReconcileEmail() throws AppError {

    String strTEMPLATE_NAME = "GMFieldAuditReconcileEmailTemplate";
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

    GmEmailProperties emailProps = new GmEmailProperties();
    GmJasperMail jasperMail = new GmJasperMail();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_fa_reconciliation.gm_fch_reconcile_email", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    ArrayList alDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    String strInputString = "";

    if (alDetails != null && alDetails.size() > 0) {
      for (Iterator iter = alDetails.iterator(); iter.hasNext();) {
        HashMap hmEmailParams = (HashMap) iter.next();
        String strAuditEntryID =
            GmCommonClass.parseNull((String) hmEmailParams.get("AUDITENTRYID"));
        String strRegionName = GmCommonClass.parseNull((String) hmEmailParams.get("REGIONNM"));
        String strTagID = GmCommonClass.parseNull((String) hmEmailParams.get("TAGID"));
        String strSetID = GmCommonClass.parseNull((String) hmEmailParams.get("SETID"));
        String strEtchID = GmCommonClass.parseNull((String) hmEmailParams.get("ETCHID"));
        String strPossession = GmCommonClass.parseNull((String) hmEmailParams.get("DISTNAME"));
        String strTransID = GmCommonClass.parseNull((String) hmEmailParams.get("TRANSID"));
        String strCurrLocation = GmCommonClass.parseNull((String) hmEmailParams.get("CURRLOC"));
        String strStatus = GmCommonClass.parseNull((String) hmEmailParams.get("TAGSTATUS"));
        String strTemplateType =
            GmCommonClass.parseNull((String) hmEmailParams.get("TEMPLATETYPE"));
        String strEmailSubject = GmCommonClass.parseNull((String) hmEmailParams.get("SUBJECT"));
        String strEmailHeader = GmCommonClass.parseNull((String) hmEmailParams.get("EMAILHEADER"));
        String strEmailToAddr = GmCommonClass.parseNull((String) hmEmailParams.get("EMAILTO"));

        String strTo = "";
        String strCc = "";
        String[] strGetCc = null;

        HashMap hmJasperDetails = new HashMap();
        hmJasperDetails.put("TEMPLATETYPE", strTemplateType);
        hmJasperDetails.put("REGIONNM", strRegionName);
        hmJasperDetails.put("TAGID", strTagID);
        hmJasperDetails.put("SETID", strSetID);
        hmJasperDetails.put("ETCHID", strEtchID);
        hmJasperDetails.put("DISTNAME", strPossession);
        hmJasperDetails.put("TRANSID", strTransID);
        hmJasperDetails.put("CURRLOC", strCurrLocation);
        hmJasperDetails.put("TAGSTATUS", strStatus);
        hmJasperDetails.put("EMAILHEADER", strEmailHeader);

        String strJasperName = "/GmFAReconcileStatusEmail.jasper";
        strTo = strEmailToAddr;
        strCc = gmResourceBundleBean.getProperty(strTEMPLATE_NAME + "." + GmEmailProperties.CC);

        emailProps.setMimeType(gmResourceBundleBean.getProperty(strTEMPLATE_NAME + "."
            + GmEmailProperties.MIME_TYPE));
        emailProps.setSender(gmResourceBundleBean.getProperty(strTEMPLATE_NAME + "."
            + GmEmailProperties.FROM));
        strGetCc = emailProps.getCc();

        if (strGetCc == null) { // only one time to set the CC address
          emailProps.setCc(strCc);
        }
        emailProps.setRecipients(strTo);
        emailProps.setSubject(strEmailSubject);

        jasperMail.setJasperReportName(strJasperName);
        jasperMail.setAdditionalParams(hmJasperDetails);
        jasperMail.setReportData(null);
        jasperMail.setEmailProperties(emailProps);
        HashMap hmReturnDetails = jasperMail.sendMail();

        log.debug(" Jasper Email Send to Customer Service Team .... ");
        strInputString += strAuditEntryID + ",";
      }
      // to update the email type and flag
      saveReconcileEmailFl(strInputString);

    }
  }

  /**
   * saveReconcileEmailFl After Reconcile email sended this method will update the c2656_email_fl as
   * 'Y' so that will come to know whether email is sended or not.
   * 
   * @param HashMap
   * @return ''
   * @exception AppError
   */

  public void saveReconcileEmailFl(String strInputString) throws AppError {
    String strEmailType = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_fa_reconciliation.gm_sav_email_fl", 2);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strEmailType);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * savePostingError For Saving the Posting Rules in t2662_posting_error table whenever we
   * Reconcile the Tag
   * 
   * @param HashMap
   * @return ''
   * @exception AppError
   */

  public void savePostingError(HashMap hmParam) throws AppError {
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_fa_post_err.gm_post_validation", 2);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * saveConsignAdjust to create the new transaction ()
   * 
   * @param HashMap
   * @return String
   * @exception AppError
   */

  public String saveConsignAdjust(HashMap hmParam) throws AppError {
    String strAdjustType = GmCommonClass.parseZero((String) hmParam.get("ADJUSTMENTTYPE"));
    String strConsignTo = GmCommonClass.parseZero((String) hmParam.get("CONSIGNTO"));
    String strPartNumStr = GmCommonClass.parseNull((String) hmParam.get("PNUMSTR"));
    String strSetID = GmCommonClass.parseZero((String) hmParam.get("SETID"));
    String strTagID = GmCommonClass.parseNull((String) hmParam.get("TAGID"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strTxnIds = "";
    log.debug("hmParam Values --> " + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_fa_reconciliation.gm_ac_sav_cons_adj", 9);
    gmDBManager.setString(1, strAdjustType);
    gmDBManager.setString(2, strConsignTo);
    gmDBManager.setString(3, strPartNumStr);
    gmDBManager.setString(4, strSetID);
    gmDBManager.setString(5, strTagID);
    gmDBManager.setString(6, strComments);
    gmDBManager.setString(7, "18641"); // Consignment Adjustment (Screen)
    gmDBManager.setString(8, strUserID);
    gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strTxnIds = gmDBManager.getString(9);
    gmDBManager.commit();

    log.debug("strTxnIds --> " + strTxnIds);
    return strTxnIds;
  }
}
