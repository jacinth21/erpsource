package com.globus.accounts.fieldaudit.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCancelForm;

public class GmFieldAuditSetupForm extends GmCancelForm {

	private String auditId="";
	private String auditName = "";
	private String primaryAuditorId = "";
	private String auditStartDt = "";
	private String auditEndDt = "";
	private String auditStatus = "";
	private String lockedDt = "";
	private String regionId = "";
	private String gridData = "";
	private String setIds = "";
	private String pnums = "";
	private String inputString ="";
	private String invalidSetIds = "";
	private String auditStatusId = "";
	
	private ArrayList alAuditName = new ArrayList();
	private ArrayList alDistributor = new ArrayList();
	private ArrayList alAuditor = new ArrayList();
	private ArrayList alFALockReport = new ArrayList();
	private ArrayList alRegionName = new ArrayList();
	private ArrayList alInactiveDist = new ArrayList();
	
	/**
	 * @return the auditId
	 */
	public String getAuditId() {
		return auditId;
	}
	/**
	 * @param auditId the auditId to set
	 */
	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}
	/**
	 * @return the auditName
	 */
	public String getAuditName() {
		return auditName;
	}
	/**
	 * @param auditName the auditName to set
	 */
	public void setAuditName(String auditName) {
		this.auditName = auditName;
	}
	/**
	 * @return the primaryAuditorId
	 */
	public String getPrimaryAuditorId() {
		return primaryAuditorId;
	}
	/**
	 * @param primaryAuditorId the primaryAuditorId to set
	 */
	public void setPrimaryAuditorId(String primaryAuditorId) {
		this.primaryAuditorId = primaryAuditorId;
	}
	/**
	 * @return the auditStartDt
	 */
	public String getAuditStartDt() {
		return auditStartDt;
	}
	/**
	 * @param auditStartDt the auditStartDt to set
	 */
	public void setAuditStartDt(String auditStartDt) {
		this.auditStartDt = auditStartDt;
	}
	/**
	 * @return the auditEndDt
	 */
	public String getAuditEndDt() {
		return auditEndDt;
	}
	/**
	 * @param auditEndDt the auditEndDt to set
	 */
	public void setAuditEndDt(String auditEndDt) {
		this.auditEndDt = auditEndDt;
	}
	/**
	 * @return the auditStatus
	 */
	public String getAuditStatus() {
		return auditStatus;
	}
	/**
	 * @param auditStatus the auditStatus to set
	 */
	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}
	/**
	 * @return the lockedDt
	 */
	public String getLockedDt() {
		return lockedDt;
	}
	/**
	 * @param lockedDt the lockedDt to set
	 */
	public void setLockedDt(String lockedDt) {
		this.lockedDt = lockedDt;
	}
	/**
	 * @return the regionId
	 */
	public String getRegionId() {
		return regionId;
	}
	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the setIds
	 */
	public String getSetIds() {
		return setIds;
	}
	/**
	 * @param setIds the setIds to set
	 */
	public void setSetIds(String setIds) {
		this.setIds = setIds;
	}
	/**
	 * @return the pnums
	 */
	public String getPnums() {
		return pnums;
	}
	/**
	 * @param pnums the pnums to set
	 */
	public void setPnums(String pnums) {
		this.pnums = pnums;
	}
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	
	
	/**
	 * @return the setIdErrorMsg
	 */
	public String getInvalidSetIds() {
		return invalidSetIds;
	}
	/**
	 * @param setIdErrorMsg the setIdErrorMsg to set
	 */
	public void setInvalidSetIds(String setIdErrorMsg) {
		this.invalidSetIds = setIdErrorMsg;
	}
	
	/**
	 * @return the auditStatusId
	 */
	public String getAuditStatusId() {
		return auditStatusId;
	}
	/**
	 * @param auditStatusId the auditStatusId to set
	 */
	public void setAuditStatusId(String auditStatusId) {
		this.auditStatusId = auditStatusId;
	}
	/**
	 * @return the alAuditName
	 */
	public ArrayList getAlAuditName() {
		return alAuditName;
	}
	/**
	 * @param alAuditName the alAuditName to set
	 */
	public void setAlAuditName(ArrayList alAuditName) {
		this.alAuditName = alAuditName;
	}
	/**
	 * @return the alDistributor
	 */
	public ArrayList getAlDistributor() {
		return alDistributor;
	}
	/**
	 * @param alDistributor the alDistributor to set
	 */
	public void setAlDistributor(ArrayList alDistributor) {
		this.alDistributor = alDistributor;
	}
	/**
	 * @return the alAuditor
	 */
	public ArrayList getAlAuditor() {
		return alAuditor;
	}
	/**
	 * @param alAuditor the alAuditor to set
	 */
	public void setAlAuditor(ArrayList alAuditor) {
		this.alAuditor = alAuditor;
	}
	/**
	 * @return the alFALockReport
	 */
	public ArrayList getAlFALockReport() {
		return alFALockReport;
	}
	/**
	 * @param alFALockReport the alFALockReport to set
	 */
	public void setAlFALockReport(ArrayList alFALockReport) {
		this.alFALockReport = alFALockReport;
	}
	/**
	 * @return the alRegionName
	 */
	public ArrayList getAlRegionName() {
		return alRegionName;
	}
	/**
	 * @param alRegionName the alRegionName to set
	 */
	public void setAlRegionName(ArrayList alRegionName) {
		this.alRegionName = alRegionName;
	}
	/**
	 * @return the alInactiveDist
	 */
	public ArrayList getAlInactiveDist() {
		return alInactiveDist;
	}
	/**
	 * @param alInactiveDist the alInactiveDist to set
	 */
	public void setAlInactiveDist(ArrayList alInactiveDist) {
		this.alInactiveDist = alInactiveDist;
	}

	
}
