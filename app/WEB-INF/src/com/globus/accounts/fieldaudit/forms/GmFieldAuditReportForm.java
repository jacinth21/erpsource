package com.globus.accounts.fieldaudit.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCancelForm;
import com.globus.common.forms.GmCommonForm;

public class GmFieldAuditReportForm extends GmCancelForm{
	
	private String auditId		= "";
	private String distId		= "";
	private String statusId		= "";
	private String setId		= "";
	private String regionId		= "";
	private String auditorId	= "";	
	private String devOpr		= "";
	private String reportType	= "";
	private String lockedFrmDt 	= "" ;
	private String lockedToDt   = "";
	
	private int    devQty 		= 0;
	
	private boolean dispOnlyDeviation;
	private boolean dispNonflagged;
	private boolean dispflaggedDet;
	private boolean dispUnresolvedDet;	
	private boolean dispNonApproved;
	private boolean dispNonReconciliation;
	
	private List ldtResult = new ArrayList();
	private ArrayList alAuditName = new ArrayList();
	private ArrayList aldist = new ArrayList();
	private ArrayList alstatus = new ArrayList();
	private ArrayList alAuditor = new ArrayList();
	private ArrayList alSet = new ArrayList();
	private ArrayList alResions = new ArrayList();
	private ArrayList alDevOpr = new ArrayList();

	
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public ArrayList getAlstatus() {
		return alstatus;
	}
	public void setAlstatus(ArrayList alstatus) {
		this.alstatus = alstatus;
	}

	/**
	 * @return the lockedFrmDt
	 */
	public String getLockedFrmDt() {
		return lockedFrmDt;
	}
	/**
	 * @param lockedFrmDt the lockedFrmDt to set
	 */
	public void setLockedFrmDt(String lockedFrmDt) {
		this.lockedFrmDt = lockedFrmDt;
	}
	/**
	 * @return the lockedToDt
	 */
	public String getLockedToDt() {
		return lockedToDt;
	}
	/**
	 * @param lockedToDt the lockedToDt to set
	 */
	public void setLockedToDt(String lockedToDt) {
		this.lockedToDt = lockedToDt;
	}
	/**
	 * @return the auditId
	 */
	public String getAuditId() {
		return auditId;
	}	
	/**
	 * @param auditId the auditId to set
	 */
	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}
	/**
	 * @return the distId
	 */
	public String getDistId() {
		return distId;
	}
	/**
	 * @param distId the distId to set
	 */
	public void setDistId(String distId) {
		this.distId = distId;
	}
	/**
	 * @return the setId
	 */
	public String getSetId() {
		return setId;
	}
	/**
	 * @param setId the setId to set
	 */
	public void setSetId(String setId) {
		this.setId = setId;
	}
	
	/**
	 * @return the regionId
	 */
	public String getRegionId() {
		return regionId;
	}
	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	/**
	 * @return the auditorId
	 */
	public String getAuditorId() {
		return auditorId;
	}
	/**
	 * @param auditorId the auditorId to set
	 */
	public void setAuditorId(String auditorId) {
		this.auditorId = auditorId;
	}
	/**
	 * @return the devQty
	 */
	public int getDevQty() {
		return devQty;
	}
	/**
	 * @param devQty the devQty to set
	 */
	public void setDevQty(int devQty) {
		this.devQty = devQty;
	}
	/**
	 * @return the devOpr
	 */
	public String getDevOpr() {
		return devOpr;
	}
	/**
	 * @param devOpr the devOpr to set
	 */
	public void setDevOpr(String devOpr) {
		this.devOpr = devOpr;
	}
	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}
	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	/**
	 * @return the dispOnlyDeviation
	 */
	public boolean isDispOnlyDeviation() {
		return dispOnlyDeviation;
	}
	/**
	 * @param dispOnlyDeviation the dispOnlyDeviation to set
	 */
	public void setDispOnlyDeviation(boolean dispOnlyDeviation) {
		this.dispOnlyDeviation = dispOnlyDeviation;
	}
	/**
	 * @return the dispNonflagged
	 */
	public boolean isDispNonflagged() {
		return dispNonflagged;
	}
	/**
	 * @param dispNonflagged the dispNonflagged to set
	 */
	public void setDispNonflagged(boolean dispNonflagged) {
		this.dispNonflagged = dispNonflagged;
	}
	/**
	 * @return the dispflaggedDet
	 */
	public boolean isDispflaggedDet() {
		return dispflaggedDet;
	}
	/**
	 * @param dispflaggedDet the dispflaggedDet to set
	 */
	public void setDispflaggedDet(boolean dispflaggedDet) {
		this.dispflaggedDet = dispflaggedDet;
	}
	/**
	 * @return the dispUnresolvedDet
	 */
	public boolean isDispUnresolvedDet() {
		return dispUnresolvedDet;
	}
	/**
	 * @param dispUnresolvedDet the dispUnresolvedDet to set
	 */
	public void setDispUnresolvedDet(boolean dispUnresolvedDet) {
		this.dispUnresolvedDet = dispUnresolvedDet;
	}
	/**
	 * @return the dispNonApproved
	 */
	public boolean isDispNonApproved() {
		return dispNonApproved;
	}
	/**
	 * @param dispNonApproved the dispNonApproved to set
	 */
	public void setDispNonApproved(boolean dispNonApproved) {
		this.dispNonApproved = dispNonApproved;
	}
	/**
	 * @return the dispNonReconciliation
	 */
	public boolean isDispNonReconciliation() {
		return dispNonReconciliation;
	}
	/**
	 * @param dispNonReconciliation the dispNonReconciliation to set
	 */
	public void setDispNonReconciliation(boolean dispNonReconciliation) {
		this.dispNonReconciliation = dispNonReconciliation;
	}
	/**
	 * @return the ldtResult
	 */
	public List getLdtResult() {
		return ldtResult;
	}
	/**
	 * @param ldtResult the ldtResult to set
	 */
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}
	/**
	 * @return the alAuditName
	 */
	public ArrayList getAlAuditName() {
		return alAuditName;
	}
	/**
	 * @param alAuditName the alAuditName to set
	 */
	public void setAlAuditName(ArrayList alAuditName) {
		this.alAuditName = alAuditName;
	}
	/**
	 * @return the aldist
	 */
	public ArrayList getAldist() {
		return aldist;
	}
	/**
	 * @param aldist the aldist to set
	 */
	public void setAldist(ArrayList aldist) {
		this.aldist = aldist;
	}
	/**
	 * @return the alAuditor
	 */
	public ArrayList getAlAuditor() {
		return alAuditor;
	}
	/**
	 * @param alAuditor the alAuditor to set
	 */
	public void setAlAuditor(ArrayList alAuditor) {
		this.alAuditor = alAuditor;
	}
	/**
	 * @return the alSet
	 */
	public ArrayList getAlSet() {
		return alSet;
	}
	/**
	 * @param alSet the alSet to set
	 */
	public void setAlSet(ArrayList alSet) {
		this.alSet = alSet;
	}
	
	/**
	 * @return the alResions
	 */
	public ArrayList getAlResions() {
		return alResions;
	}
	/**
	 * @param alResions the alResions to set
	 */
	public void setAlResions(ArrayList alResions) {
		this.alResions = alResions;
	}
	/**
	 * @return the alDevOpr
	 */
	public ArrayList getAlDevOpr() {
		return alDevOpr;
	}
	/**
	 * @param alDevOpr the alDevOpr to set
	 */
	public void setAlDevOpr(ArrayList alDevOpr) {
		this.alDevOpr = alDevOpr;
	}
	
}
