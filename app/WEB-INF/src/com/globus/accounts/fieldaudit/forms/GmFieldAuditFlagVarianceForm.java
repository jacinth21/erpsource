/**
 * Description : This form class is used for populating Flag variance information
 * Copyright : Globus Medical Inc
 */

package com.globus.accounts.fieldaudit.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmFieldAuditFlagVarianceForm  extends GmCommonForm {
	
	private String auditId="";
	private String distId="";
	private String setId="";
	private String pnum="";
	private String strInput="";
	private String deviationQty="";
	private String unResolvedQty="";
	private String auditEntryId = "";
	private String auditQty = "";
	private String tagQty = "";
	private String opsQty ="";
	private String sysQty = "";
	private String absQty ="";
	private String flaggedQty = "";
	private String unResolveQty ="";
	private String distName = "";
	private String setType = "";
	
	
	private List alMatchingReport= new ArrayList();
	private List alPositiveReport= new ArrayList();
	private List alNegativeReport= new ArrayList();
	private List alPAssociatedReport= new ArrayList();
	private List alNAssociatedReport= new ArrayList();
	private List alHistory = new ArrayList();
	
	private ArrayList alPCodeID=new ArrayList();
	private ArrayList alNCodeID=new ArrayList();
	private ArrayList alDistList= new ArrayList();
	private ArrayList alchange=new ArrayList();
	private ArrayList alPostingOptions=new ArrayList();
	private ArrayList alExtra =new ArrayList();
	private ArrayList alNotDevPositive =new ArrayList();
	private ArrayList alUnVoidTag =new ArrayList();
	private ArrayList alVerifiedPost =new ArrayList();
	private ArrayList alNotDevNegative =new ArrayList();
	
	HashMap hmHeaderInfo= new HashMap();
	

	/**
	 * @return the auditId
	 */
	public String getAuditId() {
		return auditId;
	}

	/**
	 * @param auditId the auditId to set
	 */
	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}

	/**
	 * @return the distId
	 */
	public String getDistId() {
		return distId;
	}

	/**
	 * @param distId the distId to set
	 */
	public void setDistId(String distId) {
		this.distId = distId;
	}

	/**
	 * @return the setId
	 */
	public String getSetId() {
		return setId;
	}

	/**
	 * @param setId the setId to set
	 */
	public void setSetId(String setId) {
		this.setId = setId;
	}

	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}

	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	/**
	 * @return the strInput
	 */
	public String getStrInput() {
		return strInput;
	}

	/**
	 * @param strInput the strInput to set
	 */
	public void setStrInput(String strInput) {
		this.strInput = strInput;
	}

	/**
	 * @return the deviationQty
	 */
	public String getDeviationQty() {
		return deviationQty;
	}

	/**
	 * @param deviationQty the deviationQty to set
	 */
	public void setDeviationQty(String deviationQty) {
		this.deviationQty = deviationQty;
	}

	/**
	 * @return the unResolvedQty
	 */
	public String getUnResolvedQty() {
		return unResolvedQty;
	}

	/**
	 * @param unResolvedQty the unResolvedQty to set
	 */
	public void setUnResolvedQty(String unResolvedQty) {
		this.unResolvedQty = unResolvedQty;
	}

	/**
	 * @return the auditEntryId
	 */
	public String getAuditEntryId() {
		return auditEntryId;
	}

	/**
	 * @param auditEntryId the auditEntryId to set
	 */
	public void setAuditEntryId(String auditEntryId) {
		this.auditEntryId = auditEntryId;
	}

	/**
	 * @return the auditQty
	 */
	public String getAuditQty() {
		return auditQty;
	}

	/**
	 * @param auditQty the auditQty to set
	 */
	public void setAuditQty(String auditQty) {
		this.auditQty = auditQty;
	}

	/**
	 * @return the tagQty
	 */
	public String getTagQty() {
		return tagQty;
	}

	/**
	 * @param tagQty the tagQty to set
	 */
	public void setTagQty(String tagQty) {
		this.tagQty = tagQty;
	}

	/**
	 * @return the opsQty
	 */
	public String getOpsQty() {
		return opsQty;
	}

	/**
	 * @param opsQty the opsQty to set
	 */
	public void setOpsQty(String opsQty) {
		this.opsQty = opsQty;
	}

	/**
	 * @return the sysQty
	 */
	public String getSysQty() {
		return sysQty;
	}

	/**
	 * @param sysQty the sysQty to set
	 */
	public void setSysQty(String sysQty) {
		this.sysQty = sysQty;
	}

	/**
	 * @return the absQty
	 */
	public String getAbsQty() {
		return absQty;
	}

	/**
	 * @param absQty the absQty to set
	 */
	public void setAbsQty(String absQty) {
		this.absQty = absQty;
	}

	/**
	 * @return the flaggedQty
	 */
	public String getFlaggedQty() {
		return flaggedQty;
	}

	/**
	 * @param flaggedQty the flaggedQty to set
	 */
	public void setFlaggedQty(String flaggedQty) {
		this.flaggedQty = flaggedQty;
	}

	/**
	 * @return the unResolveQty
	 */
	public String getUnResolveQty() {
		return unResolveQty;
	}

	/**
	 * @param unResolveQty the unResolveQty to set
	 */
	public void setUnResolveQty(String unResolveQty) {
		this.unResolveQty = unResolveQty;
	}

	/**
	 * @return the distName
	 */
	public String getDistName() {
		return distName;
	}

	/**
	 * @param distName the distName to set
	 */
	public void setDistName(String distName) {
		this.distName = distName;
	}

	
	/**
	 * @return the setType
	 */
	public String getSetType() {
		return setType;
	}

	/**
	 * @param setType the setType to set
	 */
	public void setSetType(String setType) {
		this.setType = setType;
	}

	/**
	 * @return the alMatchingReport
	 */
	public List getAlMatchingReport() {
		return alMatchingReport;
	}

	/**
	 * @param alMatchingReport the alMatchingReport to set
	 */
	public void setAlMatchingReport(List alMatchingReport) {
		this.alMatchingReport = alMatchingReport;
	}

	/**
	 * @return the alPositiveReport
	 */
	public List getAlPositiveReport() {
		return alPositiveReport;
	}

	/**
	 * @param alPositiveReport the alPositiveReport to set
	 */
	public void setAlPositiveReport(List alPositiveReport) {
		this.alPositiveReport = alPositiveReport;
	}

	/**
	 * @return the alNegativeReport
	 */
	public List getAlNegativeReport() {
		return alNegativeReport;
	}

	/**
	 * @param alNegativeReport the alNegativeReport to set
	 */
	public void setAlNegativeReport(List alNegativeReport) {
		this.alNegativeReport = alNegativeReport;
	}

	/**
	 * @return the alPAssociatedReport
	 */
	public List getAlPAssociatedReport() {
		return alPAssociatedReport;
	}

	/**
	 * @param alPAssociatedReport the alPAssociatedReport to set
	 */
	public void setAlPAssociatedReport(List alPAssociatedReport) {
		this.alPAssociatedReport = alPAssociatedReport;
	}

	/**
	 * @return the alNAssociatedReport
	 */
	public List getAlNAssociatedReport() {
		return alNAssociatedReport;
	}

	/**
	 * @param alNAssociatedReport the alNAssociatedReport to set
	 */
	public void setAlNAssociatedReport(List alNAssociatedReport) {
		this.alNAssociatedReport = alNAssociatedReport;
	}

	/**
	 * @return the alHistory
	 */
	public List getAlHistory() {
		return alHistory;
	}

	/**
	 * @param alHistory the alHistory to set
	 */
	public void setAlHistory(List alHistory) {
		this.alHistory = alHistory;
	}

	/**
	 * @return the alPCodeID
	 */
	public ArrayList getAlPCodeID() {
		return alPCodeID;
	}

	/**
	 * @param alPCodeID the alPCodeID to set
	 */
	public void setAlPCodeID(ArrayList alPCodeID) {
		this.alPCodeID = alPCodeID;
	}

	/**
	 * @return the alNCodeID
	 */
	public ArrayList getAlNCodeID() {
		return alNCodeID;
	}

	/**
	 * @param alNCodeID the alNCodeID to set
	 */
	public void setAlNCodeID(ArrayList alNCodeID) {
		this.alNCodeID = alNCodeID;
	}

	/**
	 * @return the alDistList
	 */
	public ArrayList getAlDistList() {
		return alDistList;
	}

	/**
	 * @param alDistList the alDistList to set
	 */
	public void setAlDistList(ArrayList alDistList) {
		this.alDistList = alDistList;
	}

	/**
	 * @return the alchange
	 */
	public ArrayList getAlchange() {
		return alchange;
	}

	/**
	 * @param alchange the alchange to set
	 */
	public void setAlchange(ArrayList alchange) {
		this.alchange = alchange;
	}

	/**
	 * @return the alPostingOptions
	 */
	public ArrayList getAlPostingOptions() {
		return alPostingOptions;
	}

	/**
	 * @param alPostingOptions the alPostingOptions to set
	 */
	public void setAlPostingOptions(ArrayList alPostingOptions) {
		this.alPostingOptions = alPostingOptions;
	}

	/**
	 * @return the alExtra
	 */
	public ArrayList getAlExtra() {
		return alExtra;
	}

	/**
	 * @param alExtra the alExtra to set
	 */
	public void setAlExtra(ArrayList alExtra) {
		this.alExtra = alExtra;
	}

	/**
	 * @return the alNotDevPositive
	 */
	public ArrayList getAlNotDevPositive() {
		return alNotDevPositive;
	}

	/**
	 * @param alNotDevPositive the alNotDevPositive to set
	 */
	public void setAlNotDevPositive(ArrayList alNotDevPositive) {
		this.alNotDevPositive = alNotDevPositive;
	}

	/**
	 * @return the alUnVoidTag
	 */
	public ArrayList getAlUnVoidTag() {
		return alUnVoidTag;
	}

	/**
	 * @param alUnVoidTag the alUnVoidTag to set
	 */
	public void setAlUnVoidTag(ArrayList alUnVoidTag) {
		this.alUnVoidTag = alUnVoidTag;
	}

	/**
	 * @return the alVerifiedPost
	 */
	public ArrayList getAlVerifiedPost() {
		return alVerifiedPost;
	}

	/**
	 * @param alVerifiedPost the alVerifiedPost to set
	 */
	public void setAlVerifiedPost(ArrayList alVerifiedPost) {
		this.alVerifiedPost = alVerifiedPost;
	}

	/**
	 * @return the alNotDevNegative
	 */
	public ArrayList getAlNotDevNegative() {
		return alNotDevNegative;
	}

	/**
	 * @param alNotDevNegative the alNotDevNegative to set
	 */
	public void setAlNotDevNegative(ArrayList alNotDevNegative) {
		this.alNotDevNegative = alNotDevNegative;
	}

	/**
	 * @return the hmHeaderInfo
	 */
	public HashMap getHmHeaderInfo() {
		return hmHeaderInfo;
	}

	/**
	 * @param hmHeaderInfo the hmHeaderInfo to set
	 */
	public void setHmHeaderInfo(HashMap hmHeaderInfo) {
		this.hmHeaderInfo = hmHeaderInfo;
	}
	
	

}
