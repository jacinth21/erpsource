package com.globus.accounts.fieldaudit.forms;

import com.globus.common.forms.GmCommonForm;

public class GmFieldAuditSummaryForm extends GmCommonForm{

	private String faSumFromDt = "";
	private String faSumToDt = "";
	private String xmlGridData  = "";
	
	public String getFaSumFromDt() {
		return faSumFromDt;
	}
	public void setFaSumFromDt(String faSumFromDt) {
		this.faSumFromDt = faSumFromDt;
	}
	public String getFaSumToDt() {
		return faSumToDt;
	}
	public void setFaSumToDt(String faSumToDt) {
		this.faSumToDt = faSumToDt;
	}

	public String getXmlGridData() {
		return xmlGridData;
	}
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
}
