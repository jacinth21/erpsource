/**
 * Description : This form class is used for populating field audit set cost updation information
 * Copyright : Globus Medical Inc
 */
package com.globus.accounts.fieldaudit.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCancelForm;
import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;
/**
 * @author 
 *
 */
public class GmFieldAuditSetCostForm extends GmCommonForm{
	
	 private String batchData = "";
	 private String hSetInputStr = "";
	 private String message  = "";
	 private String hInputStr = "";
	 private String attType = "";
	 private String xmlGridData  = "";
	
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the batchData
	 */
	public String getBatchData() {
		return batchData;
	}
	/**
	 * @param batchData the batchData to set
	 */
	public void setBatchData(String batchData) {
		this.batchData = batchData;
	}
	/**
	 * @return the hSetInputStr
	 */
	public String gethSetInputStr() {
		return hSetInputStr;
	}
	/**
	 * @param hSetInputStr the hSetInputStr to set
	 */
	public void sethSetInputStr(String hSetInputStr) {
		this.hSetInputStr = hSetInputStr;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the hInputStr
	 */
	public String gethInputStr() {
		return hInputStr;
	}
	/**
	 * @param hInputStr the hInputStr to set
	 */
	public void sethInputStr(String hInputStr) {
		this.hInputStr = hInputStr;
	}
	/**
	 * @return the attType
	 */
	public String getAttType() {
		return attType;
	}
	/**
	 * @param attType the attType to set
	 */
	public void setAttType(String attType) {
		this.attType = attType;
	}
}