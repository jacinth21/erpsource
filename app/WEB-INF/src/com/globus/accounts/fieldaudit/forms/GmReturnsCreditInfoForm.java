package com.globus.accounts.fieldaudit.forms;

import com.globus.common.forms.GmCommonForm;

public class GmReturnsCreditInfoForm extends GmCommonForm{
	
	public String xmlGridData = "";
	public String SetId = "";
	public String DistId = "";
	public String refId = "";
	public String hRAID = "";
	
	/**
	 * @return the hRAID
	 */
	public String gethRAID() {
		return hRAID;
	}
	/**
	 * @param hRAID the hRAID to set
	 */
	public void sethRAID(String hRAID) {
		this.hRAID = hRAID;
	}
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/** @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	
	/**
	 * @return the setId
	 */
	public String getSetId() {
		return SetId;
	}
	/**
	 * @param setId the setId to set
	 */
	public void setSetId(String setId) {
		SetId = setId;
	}
	
	/**
	 * @return the distId
	 */
	public String getDistId() {
		return DistId;
	}
	/**
	 * @param distId the distId to set
	 */
	public void setDistId(String distId) {
		DistId = distId;
	}
	/**
	 * @return the refId
	 */
	public String getRefId() {
		return refId;
	}
	/**
	 * @param refId the refId to set
	 */
	public void setRefId(String refId) {
		this.refId = refId;
	}
	

}
