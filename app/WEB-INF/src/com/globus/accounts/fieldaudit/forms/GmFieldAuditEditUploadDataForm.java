package com.globus.accounts.fieldaudit.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCancelForm;


public class GmFieldAuditEditUploadDataForm extends GmCancelForm {
	
	private String strOpt = "";
	private String auditId="";
	private String distId="";
	private String auditorId="";
	private String auditName = "";
	private String tagID ="";
	private String partNumber ="";
	private String controlNumber="";
	private String comments ="";
	private String countedDate = "";
	private String auditEntryID = "";
	private String setID = "";
	private String statusID ="";
	private String borrFrom ="";
	private String locationID="";
	private String locationDet ="";
	private String locationInfo = "";
	private String names = "";
	private String loctyp ="";
	private String setCountFL = "";
	private String partCountFL = "";
	private String countedBy = "";
	private String reTag = "";
	private String addressID = "";
	private String flagType = "";
	private String addressType = "";
	private String locationComments = "";
	private String flagDetID = "";
	private String distName = "";
	private String lockedSet = "";
	private String hauditEntryID = "";
	private String hauditId="";
	private String hdistId="";
	private String hauditorId="";
	private String reportType = "";
	private String tagStatus = "";
	private String flagDetails = "";
	private String reconciled ="";
	private String showRA = "";
	private String showDist = "";
	private String approvedFl = "";
	
	private ArrayList alAuditName = new ArrayList();
	private ArrayList alDistributor = new ArrayList();
	private ArrayList alAuditor = new ArrayList();
	private ArrayList alSet = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alBorrfrm = new ArrayList();
	private ArrayList alLocation = new ArrayList();
	private ArrayList alLocDetails = new ArrayList();
	private ArrayList alFlagType = new ArrayList();
	private ArrayList alLocationType = new ArrayList();
	private ArrayList alAddressType = new ArrayList();
	private ArrayList alRefId = new ArrayList();
	
	private List ldtResult = new ArrayList();
	
	private HashMap hmAuditEntryDetails = new HashMap();
	
	
	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}
	/**
	 * @param reportType the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public String getHauditEntryID() {
		return hauditEntryID;
	}
	public void setHauditEntryID(String hauditEntryID) {
		this.hauditEntryID = hauditEntryID;
	}
	public String getHauditId() {
		return hauditId;
	}
	public void setHauditId(String hauditId) {
		this.hauditId = hauditId;
	}
	public String getHdistId() {
		return hdistId;
	}
	public void setHdistId(String hdistId) {
		this.hdistId = hdistId;
	}
	public String getHauditorId() {
		return hauditorId;
	}
	public void setHauditorId(String hauditorId) {
		this.hauditorId = hauditorId;
	}
	public String getLocationInfo() {
		return locationInfo;
	}
	public void setLocationInfo(String locationInfo) {
		this.locationInfo = locationInfo;
	}
	public String getStrOpt() {
		return strOpt;
	}
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}
	public String getDistName() {
		return distName;
	}
	public void setDistName(String distName) {
		this.distName = distName;
	}
	public String getAuditId() {
		return auditId;
	}
	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}
	public String getDistId() {
		return distId;
	}
	public void setDistId(String distId) {
		this.distId = distId;
	}
	public String getAuditorId() {
		return auditorId;
	}
	public void setAuditorId(String auditorId) {
		this.auditorId = auditorId;
	}
	public String getAuditName() {
		return auditName;
	}
	public void setAuditName(String auditName) {
		this.auditName = auditName;
	}
	public ArrayList getAlAuditName() {
		return alAuditName;
	}
	public void setAlAuditName(ArrayList alAuditName) {
		this.alAuditName = alAuditName;
	}
	public ArrayList getAlDistributor() {
		return alDistributor;
	}
	public void setAlDistributor(ArrayList alDistributor) {
		this.alDistributor = alDistributor;
	}
	public ArrayList getAlAuditor() {
		return alAuditor;
	}
	public void setAlAuditor(ArrayList alAuditor) {
		this.alAuditor = alAuditor;
	}
	public List getLdtResult() {
		return ldtResult;
	}
	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}
	public HashMap getHmAuditEntryDetails() {
		return hmAuditEntryDetails;
	}
	public void setHmAuditEntryDetails(HashMap hmAuditEntryDetails) {
		this.hmAuditEntryDetails = hmAuditEntryDetails;
	}
	public String getTagID() {
		return tagID;
	}
	public void setTagID(String tagID) {
		this.tagID = tagID;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getControlNumber() {
		return controlNumber;
	}
	public void setControlNumber(String controlNumber) {
		this.controlNumber = controlNumber;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getCountedDate() {
		return countedDate;
	}
	public void setCountedDate(String countedDate) {
		this.countedDate = countedDate;
	}
	public String getAuditEntryID() {
		return auditEntryID;
	}
	public void setAuditEntryID(String auditEntryID) {
		this.auditEntryID = auditEntryID;
	}
	public String getSetID() {
		return setID;
	}
	public void setSetID(String setID) {
		this.setID = setID;
	}
	public String getStatusID() {
		return statusID;
	}
	public void setStatusID(String statusID) {
		this.statusID = statusID;
	}
	public String getBorrFrom() {
		return borrFrom;
	}
	public void setBorrFrom(String borrFrom) {
		this.borrFrom = borrFrom;
	}
	public String getLocationID() {
		return locationID;
	}
	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}
	public String getLocationDet() {
		return locationDet;
	}
	public void setLocationDet(String locationDet) {
		this.locationDet = locationDet;
	}
	public String getNames() {
		return names;
	}
	public void setNames(String names) {
		this.names = names;
	}
	public String getLoctyp() {
		return loctyp;
	}
	public void setLoctyp(String loctyp) {
		this.loctyp = loctyp;
	}
	public ArrayList getAlSet() {
		return alSet;
	}
	public void setAlSet(ArrayList alSet) {
		this.alSet = alSet;
	}
	public ArrayList getAlStatus() {
		return alStatus;
	}
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	public ArrayList getAlBorrfrm() {
		return alBorrfrm;
	}
	public void setAlBorrfrm(ArrayList alBorrfrm) {
		this.alBorrfrm = alBorrfrm;
	}
	public ArrayList getAlLocation() {
		return alLocation;
	}
	public void setAlLocation(ArrayList alLocation) {
		this.alLocation = alLocation;
	}
	public ArrayList getAlLocDetails() {
		return alLocDetails;
	}
	public void setAlLocDetails(ArrayList alLocDetails) {
		this.alLocDetails = alLocDetails;
	}
	public String getSetCountFL() {
		return setCountFL;
	}
	public void setSetCountFL(String setCountFL) {
		this.setCountFL = setCountFL;
	}
	public String getPartCountFL() {
		return partCountFL;
	}
	public void setPartCountFL(String partCountFL) {
		this.partCountFL = partCountFL;
	}
	public String getCountedBy() {
		return countedBy;
	}
	public void setCountedBy(String countedBy) {
		this.countedBy = countedBy;
	}
	public String getReTag() {
		return reTag;
	}
	public void setReTag(String reTag) {
		this.reTag = reTag;
	}
	public String getAddressID() {
		return addressID;
	}
	public void setAddressID(String addressID) {
		this.addressID = addressID;
	}
	public String getFlagType() {
		return flagType;
	}
	public void setFlagType(String flagType) {
		this.flagType = flagType;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public String getLocationComments() {
		return locationComments;
	}
	public void setLocationComments(String locationComments) {
		this.locationComments = locationComments;
	}
	public String getFlagDetID() {
		return flagDetID;
	}
	public void setFlagDetID(String flagDetID) {
		this.flagDetID = flagDetID;
	}
	public String getLockedSet() {
		return lockedSet;
	}
	public void setLockedSet(String lockedSet) {
		this.lockedSet = lockedSet;
	}
	public ArrayList getAlFlagType() {
		return alFlagType;
	}
	public void setAlFlagType(ArrayList alFlagType) {
		this.alFlagType = alFlagType;
	}
	public ArrayList getAlAddressType() {
		return alAddressType;
	}
	public void setAlAddressType(ArrayList alAddressType) {
		this.alAddressType = alAddressType;
	}
	public ArrayList getAlLocationType() {
		return alLocationType;
	}
	public void setAlLocationType(ArrayList alLocationType) {
		this.alLocationType = alLocationType;
	}
	/**
	 * @return the tagStatus
	 */
	public String getTagStatus() {
		return tagStatus;
	}
	/**
	 * @param tagStatus the tagStatus to set
	 */
	public void setTagStatus(String tagStatus) {
		this.tagStatus = tagStatus;
	}
	/**
	 * @return the flagDetails
	 */
	public String getFlagDetails() {
		return flagDetails;
	}
	/**
	 * @param flagDetails the flagDetails to set
	 */
	public void setFlagDetails(String flagDetails) {
		this.flagDetails = flagDetails;
	}
	/**
	 * @return the reconciled
	 */
	public String getReconciled() {
		return reconciled;
	}
	/**
	 * @param reconciled the reconciled to set
	 */
	public void setReconciled(String reconciled) {
		this.reconciled = reconciled;
	}
	/**
	 * @return the showRA
	 */
	public String getShowRA() {
		return showRA;
	}
	/**
	 * @param showRA the showRA to set
	 */
	public void setShowRA(String showRA) {
		this.showRA = showRA;
	}
	/**
	 * @return the showDist
	 */
	public String getShowDist() {
		return showDist;
	}
	/**
	 * @param showDist the showDist to set
	 */
	public void setShowDist(String showDist) {
		this.showDist = showDist;
	}
	
	/**
	 * @return the approvedFl
	 */
	public String getApprovedFl() {
		return approvedFl;
	}
	/**
	 * @param approvedFl the approvedFl to set
	 */
	public void setApprovedFl(String approvedFl) {
		this.approvedFl = approvedFl;
	}
	/**
	 * @return the alRefId
	 */
	public ArrayList getAlRefId() {
		return alRefId;
	}
	/**
	 * @param alRefId the alRefId to set
	 */
	public void setAlRefId(ArrayList alRefId) {
		this.alRefId = alRefId;
	}
	
	
}