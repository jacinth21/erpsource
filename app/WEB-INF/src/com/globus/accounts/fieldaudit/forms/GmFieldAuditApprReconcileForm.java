package com.globus.accounts.fieldaudit.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmFieldAuditApprReconcileForm extends GmCommonForm{
	
	private String xmlGridData = "";
	private String inputString = "";
	private String deviationType = "";
	private String message = "";
	private String AuditEntryId = "";
	private String strDisableButton = "";
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	private ArrayList alFetchResult = new ArrayList();	
	
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	/**
	 * @return the deviationType
	 */
	public String getDeviationType() {
		return deviationType;
	}
	/**
	 * @param deviationType the deviationType to set
	 */
	public void setDeviationType(String deviationType) {
		this.deviationType = deviationType;
	}
	/**
	 * @return the alFetchResult
	 */
	public ArrayList getAlFetchResult() {
		return alFetchResult;
	}
	/**
	 * @param alFetchResult the alFetchResult to set
	 */
	public void setAlFetchResult(ArrayList alFetchResult) {
		this.alFetchResult = alFetchResult;
	}
	/**
	 * @return the auditEntryId
	 */
	public String getAuditEntryId() {
		return AuditEntryId;
	}
	/**
	 * @param auditEntryId the auditEntryId to set
	 */
	public void setAuditEntryId(String auditEntryId) {
		AuditEntryId = auditEntryId;
	}
	/**
	 * @return the strDisableButton
	 */
	public String getStrDisableButton() {
		return strDisableButton;
	}
	/**
	 * @param strDisableButton the strDisableButton to set
	 */
	public void setStrDisableButton(String strDisableButton) {
		this.strDisableButton = strDisableButton;
	}
	
}
