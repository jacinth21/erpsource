package com.globus.accounts.fieldaudit.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

public class GmFieldAuditConfirmationForm extends GmCommonForm {

	private String auditId = "";
	private String distId = "";
	private String pdfAction = "";
	private String strMessage = "";
	
	private ArrayList alAuditList = new ArrayList();
	private ArrayList alDistList = new ArrayList();
	private ArrayList alPdfList = new ArrayList();
	private HashMap hmResult =new HashMap();
	
	/**
	 * @return the auditId
	 */
	public String getAuditId() {
		return auditId;
	}
	/**
	 * @param auditId the auditId to set
	 */
	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}
	/**
	 * @return the distId
	 */
	public String getDistId() {
		return distId;
	}
	/**
	 * @param distId the distId to set
	 */
	public void setDistId(String distId) {
		this.distId = distId;
	}
	
	/**
	 * @return the pdfAction
	 */
	public String getPdfAction() {
		return pdfAction;
	}
	/**
	 * @param pdfAction the pdfAction to set
	 */
	public void setPdfAction(String pdfAction) {
		this.pdfAction = pdfAction;
	}
	
	/**
	 * @return the strMessage
	 */
	public String getStrMessage() {
		return strMessage;
	}
	/**
	 * @param strMessage the strMessage to set
	 */
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}
	/**
	 * @return the alAuditList
	 */
	public ArrayList getAlAuditList() {
		return alAuditList;
	}
	/**
	 * @param alAuditList the alAuditList to set
	 */
	public void setAlAuditList(ArrayList alAuditList) {
		this.alAuditList = alAuditList;
	}
	/**
	 * @return the alDistList
	 */
	public ArrayList getAlDistList() {
		return alDistList;
	}
	/**
	 * @param alDistList the alDistList to set
	 */
	public void setAlDistList(ArrayList alDistList) {
		this.alDistList = alDistList;
	}
	
	/**
	 * @return the hmResult
	 */
	public HashMap getHmResult() {
		return hmResult;
	}
	/**
	 * @param hmResult the hmResult to set
	 */
	public void setHmResult(HashMap hmResult) {
		this.hmResult = hmResult;
	}
	/**
	 * @return the alPdfList
	 */
	public ArrayList getAlPdfList() {
		return alPdfList;
	}
	/**
	 * @param alPdfList the alPdfList to set
	 */
	public void setAlPdfList(ArrayList alPdfList) {
		this.alPdfList = alPdfList;
	}
	
	
}
