package com.globus.accounts.fieldaudit.displaytag.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.sql.Date;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.accounts.fieldaudit.forms.GmFieldAuditFlagVarianceForm;
import com.globus.common.beans.GmCommonClass;



public class DTFieldAuditFlagVarianceWrapper extends TableDecorator  {

	private DynaBean db;
	GmFieldAuditFlagVarianceForm gmFieldAuditFlagVarianceForm;

	ArrayList alNCodeID= new ArrayList();
	ArrayList alPCodeID= new ArrayList();
	ArrayList alDistList= new ArrayList();
	ArrayList alchange=new ArrayList();
	ArrayList alPostingOptions=new ArrayList();
	ArrayList alExtra =new ArrayList();
	ArrayList alNotDevPositive =new ArrayList();
	ArrayList alUnVoidTag =new ArrayList();
	ArrayList alVerifiedPost =new ArrayList();
	ArrayList alNotDevNegative =new ArrayList();
	
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	
	Iterator alListIter;
	String strOptKey = "";
	String strOptValue = "";
	String 	strFlag = "";
	String strId="";
	String strReason="";
	String strRefDetail="";
	String strApprove="";
	String strApproveBy="";
	String strLDate="";
	String strApproveDate="";
	String strSetId="";
	String strDistId="";
	String strCurDate="";
	String strPostOption="";
	String strPostComment="";
	String strTagId = "";
	String  strApplJSDateFmt ="";
	String  strApplDateFmt = "";
	String strSetType = "";
	private int intCount = 0;
	HashMap hmUserList = new HashMap();
	public DTFieldAuditFlagVarianceWrapper() 
	{
		super();
	}
	public void init(PageContext context, Object decorated, TableModel tableModel) {
		// TODO Auto-generated method stub
		super.init(context, decorated, tableModel);
		gmFieldAuditFlagVarianceForm = (GmFieldAuditFlagVarianceForm) getPageContext().getAttribute("frmFieldAuditFlagVariance", PageContext.REQUEST_SCOPE);
		alNCodeID=gmFieldAuditFlagVarianceForm.getAlNCodeID();
		alPCodeID=gmFieldAuditFlagVarianceForm.getAlPCodeID();
		alDistList=gmFieldAuditFlagVarianceForm.getAlDistList();
		alchange=gmFieldAuditFlagVarianceForm.getAlchange();
		alExtra = gmFieldAuditFlagVarianceForm.getAlExtra();
		alNotDevPositive = gmFieldAuditFlagVarianceForm.getAlNotDevPositive();
		alUnVoidTag = gmFieldAuditFlagVarianceForm.getAlUnVoidTag();
		alVerifiedPost = gmFieldAuditFlagVarianceForm.getAlVerifiedPost();
		alNotDevNegative = gmFieldAuditFlagVarianceForm.getAlNotDevNegative();
		strSetId=gmFieldAuditFlagVarianceForm.getSetId();
		strDistId=gmFieldAuditFlagVarianceForm.getDistId();
		strSetType = gmFieldAuditFlagVarianceForm.getSetType();
		alPostingOptions=gmFieldAuditFlagVarianceForm.getAlPostingOptions();
		strApplJSDateFmt = GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
		strApplDateFmt=GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessApplDateFmt"));
		
	}
	public String getFLAG() 
	{
		db = (DynaBean) this.getCurrentRowObject();
		strFlag = GmCommonClass.parseNull(String.valueOf(db.get("FLAG")));		
		String strTagStatus = GmCommonClass.parseNull(String.valueOf(db.get("TAG_STATUS"))) ;
		 StringBuffer strValue = new StringBuffer();
		 
		 if(strTagStatus.equals("18543")&& intCount ==0){
			 intCount = 600;
		 }
		 	if(strTagStatus.equals("18543")){
				 if(this.isLastRow())
				 {
					 strValue.append("<input  TYPE='hidden' VALUE='");
					 strValue.append(String.valueOf(intCount));
					 strValue.append("' NAME='countNeg'");
					 strValue.append(" SIZE='10' >");
				 }
				 
			}else{
				
				if(this.isLastRow())
				 {
					 strValue.append("<input  TYPE='hidden' VALUE='");
					 strValue.append(String.valueOf(intCount));
					 strValue.append("' NAME='countPos'");
					 strValue.append(" SIZE='10'>");
				 }
			}
		 
			 if(strTagStatus.equals("18542"))
			 {
				 alListIter = alPCodeID.iterator();
			 }
			 else{
				 alListIter = alNCodeID.iterator();
			 }
			strValue.append(" <select name='cbo_Flag");
			strValue.append(String.valueOf(intCount));
			strValue.append("' onchange ='fnChangeReason(this,");
			strValue.append(String.valueOf(intCount));
			strValue.append(")'>");
			strValue.append(" <option value='0'>[Choose One] </option>");	
			while(alListIter.hasNext())
			{
				hmUserList = (HashMap) alListIter.next();
				strOptKey = (String)hmUserList.get("CODEID");
				strOptValue = (String)hmUserList.get("CODENM");
				if (strFlag.equals(strOptKey))
				{
					strOptKey += " selected"; 
				}
				strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
			}		
			strValue.append(" </select>");
			
			strId = GmCommonClass.parseNull(String.valueOf(db.get("ID")));	

			 strValue.append("<input  TYPE='hidden' VALUE='");
			 strValue.append(strId);
			 strValue.append("' NAME='txt_id");
			 strValue.append(String.valueOf(intCount));
			 strValue.append("' SIZE='10' MAXLENGTH='25'>");
			
			
			return strValue.toString();
			
	}
	
	public String getREASONS() 
	{ 
		db = (DynaBean) this.getCurrentRowObject();
		strReason =GmCommonClass.parseNull(String.valueOf(db.get("REASON"))) ;		
		String strTagStatus = GmCommonClass.parseNull(String.valueOf(db.get("TAG_STATUS"))) ;
		strApprove =GmCommonClass.parseNull(String.valueOf(db.get("APPROVED"))) ;
		String strTagType = GmCommonClass.parseNull(String.valueOf(db.get("TAG_TYPE"))) ;
		String strDisabled = "";

		if(strApprove.equals("null")){
			strApprove = "";
		}
		
		 StringBuffer strValue = new StringBuffer();
		 String strUrl = "";
		 alListIter = alDistList.iterator();
		 if(strFlag.equals("51025")||strFlag.equals("51026")||strFlag.equals("51023")||strFlag.equals("51027"))
		 {
			 
			 strValue.append("<Div  style='display:block;display:inline' id='div_dist");
		 }
		 else{
			 strValue.append("<Div  style='display:none;' id='div_dist");
		 }
		strValue.append(String.valueOf(intCount));
		strValue.append("'> <select style='width:150px' name='cbo_dist");
		strValue.append(String.valueOf(intCount));
		strValue.append("'> <option value='0'>[Choose One] </option>");	
			while(alListIter.hasNext())
			{
				hmUserList = (HashMap) alListIter.next();
				strOptKey = (String)hmUserList.get("ID");
				strOptValue = (String)hmUserList.get("NAME");
				if (strReason.equals(strOptKey))
				{
					strOptKey += " selected"; 
				}
				strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
			}		
			 strValue.append(" </select>");
			 strValue.append("</div>");
			 if(strFlag.equals("51029"))
			 {
				 strValue.append("<Div style='display:block;display:inline'  id='dev_tnx");
			 }
			 else
			 {
				 strValue.append("<Div style='display:none;' id='dev_tnx");
			 }
			 strValue.append(String.valueOf(intCount));
			 strValue.append("'> <input style='text-align:left' TYPE='text' VALUE='");
			 if(!strReason.equals("null"))
			 {
				 strValue.append(strReason);
				
			 }
			 strValue.append("' NAME='txt_tnx");
			 strValue.append(String.valueOf(intCount));
			 strValue.append("' onBlur ='fnVlaidateRA(");
			 strValue.append(String.valueOf(intCount));
			 strValue.append(");' MAXLENGTH='20' ");
			 if(strApprove.equals("Y") || strTagType.equals("4127"))
			 {
				 strValue.append(" disabled />");
			 }
			 else
			 {
			 strValue.append("/>");
			 }
			 strValue.append(" <input style='text-align:left' TYPE='hidden' VALUE='"+strTagType+"' name='tagType"+String.valueOf(intCount)+"' />");
			 strValue.append("</div>");
			
			 if(strFlag.equals("51029"))
			 {
				 strValue.append("<Div style='display:block;display:inline'  id='dev_but");
				 
			 }
			 else
			 {
				 strValue.append("<Div style='display:none;' id='dev_but");
			 }
		
			 strValue.append(String.valueOf(intCount));
			 strValue.append("'>");
			 if(!strApprove.equals("Y") && !strTagType.equals("4127")){
				 strValue.append("<a href=\"#\"");
				 strUrl +=  "/gmReturnsCreditInfo.do?method=loadReturnsCreditedInfo&setId=";
				 strUrl +=  strSetId;
				 strUrl +=  "&distId=";
				 strUrl +=  strDistId;
				 strUrl +=  "&refId=";
				 strUrl +=  intCount ;
			
				 strValue.append(" onclick=\"javascript:fnOpenPopup(\'" + strUrl + "\');\"");
				 strValue.append("> ");
			 }
			 strValue.append("  <img border=0 Alt='Click to Look up RA No'  align=absmiddle src=/images/L.jpg height=10 width=9 />");
			 if(!strApprove.equals("Y")&& !strTagType.equals("4127")){
				 strValue.append("</a>");
			 }
			 strValue.append("</div>");
			 strRefDetail =GmCommonClass.parseNull(String.valueOf(db.get("REF_DETAILS"))) ;			
			 if(strRefDetail.equals("null"))
					 strRefDetail = "";
			 alListIter = alchange.iterator();
			 if(strFlag.equals("51028"))
			 {
				 strValue.append("<Div style='display:block;display:inline' id='div_change");
			 }else{
				 strValue.append("<Div  style='display:none;' id='div_change");
			 }
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <select name='cbo_change");
			strValue.append(String.valueOf(intCount));
			strValue.append("' onchange ='fnCharge(this,");
			strValue.append(String.valueOf(intCount));
			strValue.append(")'> <option value='0'>[On Hold] </option>");	
			while(alListIter.hasNext())
				{
					hmUserList = (HashMap) alListIter.next();
					strOptKey = (String)hmUserList.get("CODEID");
					strOptValue = (String)hmUserList.get("CODENM");
					
					if (strReason.equals(strOptKey))
					{
						strOptKey += " selected"; 
					}
					strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
				}		
			strValue.append(" </select>");
			strValue.append("</div>");
			if(strFlag.equals("51028") && (strReason.equals("51040") ||strReason.equals("18566")))
			 {
				strValue.append("<Div style='display:block;display:inline'  id='div_detail");
			 }
			else
			{
				strValue.append("<Div style='display:none;' id='div_detail");
			}
			strValue.append(String.valueOf(intCount));
			strValue.append("'> &nbsp;<input TYPE='text' VALUE='");
			strValue.append(strRefDetail);
			 strValue.append("' NAME='txt_detail");
			 strValue.append(String.valueOf(intCount));
			 strValue.append("' SIZE='10' MAXLENGTH='25' onkeypress='return isNumberKey(event);'");
			 if(strApprove.equals("Y"))
			 {
				 strValue.append(" disabled />");
			 }
			 else
			 {
			 strValue.append("/>");
			 }
			 // Missing Cost (Part and Set) to show the updated values
			 strValue.append("  <input  TYPE='hidden' VALUE='");
	        	strValue.append(strRefDetail);
			 strValue.append("' NAME='missingCost");
			 strValue.append(String.valueOf(intCount));
			 strValue.append("'/> "); 
			 strValue.append(" <input  TYPE='hidden' VALUE='");
	        	strValue.append(strReason);
			 strValue.append("' NAME='missingCostType");
			 strValue.append(String.valueOf(intCount));
			 strValue.append("'/> "); 
		
			 strValue.append("</div>");
				
			 alListIter = alExtra.iterator();
			 if(strFlag.equals("51024"))
			 {
				 strValue.append("<Div style='display:block;display:inline' id='div_extra");
			 }else{
				 strValue.append("<Div  style='display:none;' id='div_extra");
			 }
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <select name='cbo_extra");
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <option value='0'>[Choose One] </option>");	
			while(alListIter.hasNext())
				{
					hmUserList = (HashMap) alListIter.next();
					strOptKey = (String)hmUserList.get("CODEID");
					strOptValue = (String)hmUserList.get("CODENM");
					
					if (strReason.equals(strOptKey))
					{
						strOptKey += " selected"; 
					}
					strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
				}		
			strValue.append(" </select>");
			strValue.append("</div>");
			// Not a deviation
			 alListIter = alNotDevPositive.iterator();
			 if(strFlag.equals("18551"))
			 {
				 strValue.append("<Div style='display:block;display:inline' id='div_notDevPos");
			 }else{
				 strValue.append("<Div  style='display:none;' id='div_notDevPos");
			 }
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <select name='cbo_notDevPos");
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <option value='0'>[Choose One] </option>");	
			while(alListIter.hasNext())
				{
					hmUserList = (HashMap) alListIter.next();
					strOptKey = (String)hmUserList.get("CODEID");
					strOptValue = (String)hmUserList.get("CODENM");
					
					if (strReason.equals(strOptKey))
					{
						strOptKey += " selected"; 
					}
					strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
				}		
			strValue.append(" </select>");
			strValue.append("</div>");
			// un void tag only
			 alListIter = alUnVoidTag.iterator();
			 if(strFlag.equals("18552"))
			 {
				 strValue.append("<Div style='display:block;display:inline' id='div_unVoidTag");
			 }else{
				 strValue.append("<Div  style='display:none;' id='div_unVoidTag");
			 }
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <select name='cbo_unVoidTag");
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <option value='0'>[Choose One] </option>");	
			while(alListIter.hasNext())
				{
					hmUserList = (HashMap) alListIter.next();
					strOptKey = (String)hmUserList.get("CODEID");
					strOptValue = (String)hmUserList.get("CODENM");
					
					if (strReason.equals(strOptKey))
					{
						strOptKey += " selected"; 
					}
					strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
				}		
			strValue.append(" </select>");
			strValue.append("</div>");
			
			// negative qty for 18556 verified post count
			
			 alListIter = alVerifiedPost.iterator();
			 if(strFlag.equals("18556"))
			 {
				 strValue.append("<Div style='display:block;display:inline' id='div_VerPost");
			 }else{
				 strValue.append("<Div  style='display:none;' id='div_VerPost");
			 }
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <select name='cbo_VerPost");
			strValue.append(String.valueOf(intCount));
			strValue.append("' style='width:200px'> <option value='0'>[Choose One] </option>");	
			while(alListIter.hasNext())
				{
					hmUserList = (HashMap) alListIter.next();
					strOptKey = (String)hmUserList.get("CODEID");
					strOptValue = (String)hmUserList.get("CODENM");
					
					if (strReason.equals(strOptKey))
					{
						strOptKey += " selected"; 
					}
					strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
				}		
			strValue.append(" </select>");
			strValue.append("</div>");
			
			// un void tag - negative
			
			 alListIter = alNotDevNegative.iterator();
			 if(strFlag.equals("18557"))
			 {
				 strValue.append("<Div style='display:block;display:inline' id='div_notDevNeg");
			 }else{
				 strValue.append("<Div  style='display:none;' id='div_notDevNeg");
			 }
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <select name='cbo_notDevNeg");
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <option value='0'>[Choose One] </option>");	
			while(alListIter.hasNext())
				{
					hmUserList = (HashMap) alListIter.next();
					strOptKey = (String)hmUserList.get("CODEID");
					strOptValue = (String)hmUserList.get("CODENM");
					
					if (strReason.equals(strOptKey))
					{
						strOptKey += " selected"; 
					}
					strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
				}		
			strValue.append(" </select>");
			strValue.append("</div>");
			 
			return strValue.toString();
		
	}
	public String getAPPROVED() 
	{ 
		
		db = (DynaBean) this.getCurrentRowObject();
		strApprove =GmCommonClass.parseNull(String.valueOf(db.get("APPROVED"))) ;
		if(strApprove.equals("null")){
			strApprove = "";
		}
		StringBuffer strValue = new StringBuffer();
	        
        strValue.append("<input  TYPE='hidden' VALUE='");
      
        	strValue.append(strApprove);
	
		 strValue.append("' NAME='approve");
		 strValue.append(String.valueOf(intCount));
		 strValue.append("'/> "); 
		 strValue.append(strApprove);
        return strValue.toString();
	}	
	public String getPOST() 
	{ 
		
		db = (DynaBean) this.getCurrentRowObject();
		strPostOption =GmCommonClass.parseNull(String.valueOf(db.get("POSTING_OPTION"))) ;
		//strPostOption="";
		StringBuffer strValue = new StringBuffer();
		strValue.append("<Div id='div_skip");
		strValue.append("'> <select style='width:100px' name='cbo_post");
		strValue.append(String.valueOf(intCount));
		strValue.append("'> <option value='0'>[Choose One] </option>");	
		 alListIter = alPostingOptions.iterator();
			while(alListIter.hasNext())
			{
				hmUserList = (HashMap) alListIter.next();
				strOptKey = (String)hmUserList.get("CODEID");
				strOptValue = (String)hmUserList.get("CODENM");
				if (strPostOption.equals(strOptKey))
				{
					strOptKey += " selected"; 
				}
				strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
			}		
			 strValue.append(" </select>");
		strPostComment =GmCommonClass.parseNull(String.valueOf(db.get("POSTING_COMMENT"))) ;
		strValue.append("&nbsp;<textarea rows='1' cols='20' id='txt_comment");
		strValue.append(String.valueOf(intCount));
		strValue.append("' >");
		
		if(!strPostComment.equals("null"))
		 {
			 strValue.append(strPostComment);
		 }
		strValue.append("</textarea>");
		
		 strValue.append("</div>");
		  intCount++;
        return strValue.toString();
	}
	public String getTAG_ID(){
		db = (DynaBean) this.getCurrentRowObject();
		strTagId = GmCommonClass.parseNull(String.valueOf(db.get("TAG_ID"))) ;
		strId = GmCommonClass.parseNull(String.valueOf(db.get("ID")));
		String strLogFl = GmCommonClass.parseNull(String.valueOf(db.get("LOG_FL")));
		String strTagStatus = GmCommonClass.parseNull(String.valueOf(db.get("TAG_STATUS"))) ;
		String strValidTag = "";

		// If it is valid tag then it come 'Y' otherwise it 'N'
		strValidTag = GmCommonClass.parseNull(String.valueOf(db.get("VALID_TAG"))); 
		
		StringBuffer strValue = new StringBuffer();
		
		//if(!strTagId.contains("No Tag")){
			 strValue.append(" <a class=RightText href=javascript:fnOpenLog('" + strId + "',1285);> ");
				if (strLogFl.equals("Y")) {
					strValue.append(" <img src=" + strImagePath + "/phone-icon_ans.gif border=0 width=14 height=13 title='Click to add/view Comments'></a>&nbsp;");
				} else {
					strValue.append(" <img src=" + strImagePath + "/phone_icon.jpg border=0 width=14 height=13 title='Click to add/view Comments'></a>&nbsp;");
				}
				strValue.append("<a href= javascript:fnAuditEntryHistory('");
				strValue.append(strId);
				strValue.append("');> <img src=" + strImagePath + "/icon_History.gif border=0 width='14' height='13' title='Click here to see the history' style='cursor:hand' /> </a>");
				//to show the tag history
				if(strValidTag.equals("Y")){
					strValue.append("<a href= javascript:fnTagHistory('");
					strValue.append(strTagId);
					strValue.append("');> <img src=" + strImagePath + "/Tag_History.gif border=0 width='14' height='13' title='Click here to see the Tag history' style='cursor:hand'/> </a>");
				}
				
		//}
				if(strSetId.equals("null"))
					strSetId = "";
				if(strTagStatus.equals("18543") && (strSetId.equals("") || strValidTag.equals("N"))){
					strValue.append("<a href= javascript:fnLoadEditUpload('");
					strValue.append(strId +"');>");
					strValue.append(strTagId);
					strValue.append("</a>");
				}else{
					strValue.append(strTagId);
				}
		 
		return strValue.toString();
	}
	public String getRETAG(){
		StringBuffer strValue = new StringBuffer();
		db = (DynaBean) this.getCurrentRowObject();
		String strRetag = GmCommonClass.parseNull(String.valueOf(db.get("RETAG"))) ;
		String strRetagFlag = GmCommonClass.parseNull(String.valueOf(db.get("RETAG_FL"))) ;
		strFlag = GmCommonClass.parseNull(String.valueOf(db.get("FLAG")));	
		if(strFlag.equals("null"))
			strFlag ="";
		if(strRetag.equals("null"))
			strRetag ="";

		if(strRetagFlag.equals("null"))
			strRetagFlag = "";
		
		if(!strFlag.equals("18558") && !strFlag.equals("51029") &&!strFlag.equals("51028")){

			if(strRetagFlag.equals("Y")){
				strValue.append("<input class=RightText type='hidden' name='retag");
				strValue.append(String.valueOf(intCount));
				strValue.append("' value=''  /> ");
				strValue.append(strRetag);
			}else{
				strValue.append("<input class=RightText type='text' style='display:block;display:inline' name='retag");
				strValue.append(String.valueOf(intCount));
				strValue.append("' value='");
				strValue.append(strRetag);
				strValue.append("' size='7' /> ");
			}
		}else{
			strValue.append("<input class=RightText type='text' style='display:none;' name='retag");
			strValue.append(String.valueOf(intCount));
			strValue.append("' value='' size='7' /> ");
		}
		strValue.append(" <input class=RightText type='hidden' name='retagFl");
		strValue.append(String.valueOf(intCount));
		strValue.append("' value='"+strRetagFlag+"'  /> ");
		
		
		return strValue.toString();
	}
	public String getFL_DATE(){
		StringBuffer strValue = new StringBuffer();
		db = (DynaBean) this.getCurrentRowObject();
		
		Date dtFlagDate = null;
		String strDisabled = "";
		
		if(db.get("FL_DATE")!=null){
			dtFlagDate = new Date(((java.util.Date)db.get("FL_DATE")).getTime());
		} 
		strApprove =GmCommonClass.parseNull(String.valueOf(db.get("APPROVED"))) ;
		if(strApprove.equals("null")){
			strApprove = "";
		}
		if(strApprove.equals("Y")){
			strDisabled = "disabled";
		}
		String strFlagDate = GmCommonClass.getStringFromDate(dtFlagDate,strApplDateFmt);
		if(strFlagDate.equals("null")){
			strFlagDate = "";
		}
		strValue.append("<input type=\"text\" maxlength=\"10\" id=\"flagDt" + intCount+"\" name=\"flagDt" + intCount+"\" value=\""+strFlagDate+"\"");		
		strValue.append(" size='9' styleClass=\"InputArea\" onBlur=\"changeBgColor(this,'#ffffff');\"");
		strValue.append("/>&nbsp;<img id='Img_Date'  style='cursor:hand'");	
		strValue.append("onclick=\"javascript:showSingleCalendar('divFlagDt"+intCount+"','flagDt"+intCount+"','"+strApplJSDateFmt+"');\" title='Click to open Calendar'");
		strValue.append("src='/images/nav_calendar.gif' border=0 align='absmiddle' height=15 width=18 "+strDisabled+" />&nbsp;");
		strValue.append("<div id=\"divFlagDt"+intCount+"\" style=\"position: absolute; z-index: 10;\">");
		
		return strValue.toString();
	}
}
