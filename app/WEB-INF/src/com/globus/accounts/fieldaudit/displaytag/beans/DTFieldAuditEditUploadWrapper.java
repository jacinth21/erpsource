package com.globus.accounts.fieldaudit.displaytag.beans;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditEditUploadDataForm;

public class DTFieldAuditEditUploadWrapper extends TableDecorator {
	private DynaBean db;
	GmFieldAuditEditUploadDataForm gmFieldAuditEditUploadDataForm;
	String strReportType = "";

	public void init(PageContext context, Object decorated, TableModel tableModel) {
		// TODO Auto-generated method stub
		super.init(context, decorated, tableModel);
		gmFieldAuditEditUploadDataForm = (GmFieldAuditEditUploadDataForm) getPageContext().getAttribute("frmFieldAuditEditUploadData", PageContext.REQUEST_SCOPE);
		strReportType = gmFieldAuditEditUploadDataForm.getReportType();
	}

	public String getTAGID() {
		String strTagId = "";
		int intAuditEntryId;
		StringBuffer strValue = new StringBuffer();

		db = (DynaBean) this.getCurrentRowObject();
		strTagId = String.valueOf(db.get("TAGID"));
		intAuditEntryId = Integer.parseInt(String.valueOf(db.get("AUDITENTRYID")));
		if (!strReportType.equals("SummaryRpt")) {
		strValue.append("<input class=RightText type='radio' name='radAuditEntryId' value='");
		strValue.append(intAuditEntryId);
		strValue.append("' onClick=javascript:fnSetAuditEntryID('");
		strValue.append(intAuditEntryId);
		strValue.append("') > ");
		}
		strValue.append(strTagId);

		return strValue.toString();
	}

}
