package com.globus.accounts.fieldaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmDispatchAction;
import com.globus.accounts.beans.GmFieldSalesAuditBean;
import com.globus.accounts.fieldaudit.beans.GmFAReconciliationBean;
import com.globus.accounts.fieldaudit.beans.GmFASetupBean;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditReportForm;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmFieldAuditPostingErrorAction extends GmDispatchAction {

	public ActionForward fetchPostingErrorRpt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code

		GmFASetupBean gmFASetupBean = new GmFASetupBean();
		GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean();
		GmFAReconciliationBean gmFAReconciliationBean = new GmFAReconciliationBean();
		GmFieldAuditReportForm gmFieldAuditReportForm = (GmFieldAuditReportForm) form;
		gmFieldAuditReportForm.loadSessionParameters(request);
		
		HashMap hmParam = new HashMap();
		ArrayList alAuditList = new ArrayList();
		ArrayList alDistList = new ArrayList();
		ArrayList alstatus = new ArrayList();
		List ldtResult = new ArrayList();
		RowSetDynaClass rdPostingError = null;
		String strAuditId = "";
		String strStatusId = "";
		
		hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditReportForm);
		String strOpt= GmCommonClass.parseNull((String)hmParam.get("STROPT"));
		
		alAuditList = GmCommonClass.parseNullArrayList((ArrayList)gmFASetupBean.fetchAuditList()); // To get Audit list
		gmFieldAuditReportForm.setAlAuditName(alAuditList);
		
		strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
		alDistList = GmCommonClass.parseNullArrayList((ArrayList) gmFieldSalesAuditBean.fetchLockedDistributorDetails(strAuditId));
		gmFieldAuditReportForm.setAldist(alDistList);
		
		alstatus = GmCommonClass.getCodeList("FAPES"); // To get status
		gmFieldAuditReportForm.setAlstatus(alstatus);
		
		strStatusId = GmCommonClass.parseNull((String) gmFieldAuditReportForm.getStatusId());
		if(strStatusId.equals("")){
			gmFieldAuditReportForm.setStatusId("53101"); // to set default - Open
		 }
		
		if(strOpt.equals("report"))
		{
			rdPostingError=gmFAReconciliationBean.fetchPostingErrorRpt(hmParam);
			ldtResult=rdPostingError.getRows();
			log.debug("In Action for strOpt is report ldtResult="+ldtResult);
			gmFieldAuditReportForm.setLdtResult(ldtResult);
		}
		return mapping.findForward("gmFieldAuditPostingErrorRpt");
	}
}