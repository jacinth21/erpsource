package com.globus.accounts.fieldaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmFieldSalesAuditBean;
import com.globus.accounts.fieldaudit.beans.GmFASetupBean;
import com.globus.accounts.fieldaudit.beans.GmFAVarianceRptBean;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditReportForm;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmTerritoryBean;

public class GmFieldAuditReportAction extends GmDispatchAction{
//	This method will fetch the Variance Report By Distributor and Global Variance Report Set
	public ActionForward reportVarianceByDist(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)	throws Exception {
		Logger log = GmLogger.getInstance(this.getClass().getName());
		GmFieldAuditReportForm gmFieldAuditReportForm = (GmFieldAuditReportForm) form;		
		GmFAVarianceRptBean gmFAVarianceRptBean = new GmFAVarianceRptBean();
		GmFASetupBean gmFASetupBean = new GmFASetupBean();
		GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean();
		HashMap hmParam 			= new HashMap();
		RowSetDynaClass rdresult 	= null;
		List alResult 				= new ArrayList();
		hmParam 					= GmCommonClass.getHashMapFromForm(gmFieldAuditReportForm);
		ArrayList alList 			= new ArrayList();		
		String strOpt = gmFieldAuditReportForm.getStrOpt();		
		String strAuditId = gmFieldAuditReportForm.getAuditId();		
		alList = gmFASetupBean.fetchAuditList();//gmFieldSalesAuditBean.fetchAuditDetails();
		gmFieldAuditReportForm.setAlAuditName(alList);		
		alList = GmCommonClass.getCodeList("CPRSN");
		gmFieldAuditReportForm.setAlDevOpr(alList);		
		if (!strAuditId.equals("")) {
			alList = gmFieldSalesAuditBean.fetchLockedDistributorDetails(strAuditId);
			gmFieldAuditReportForm.setAldist(alList);
		}
		if (strOpt.equals("")) {
			gmFieldAuditReportForm.setDispOnlyDeviation(true);
		}		
		String strLockedFrmDt 	= GmCommonClass.parseNull((String)hmParam.get("LOCKEDFRMDT"));
		String strLockedToDt	= GmCommonClass.parseNull((String)hmParam.get("LOCKEDTODT"));
		if(gmFieldAuditReportForm.getLockedFrmDt().equals("")){ gmFieldAuditReportForm.setLockedFrmDt(strLockedFrmDt); 	}
		if(gmFieldAuditReportForm.getLockedToDt().equals("")){  gmFieldAuditReportForm.setLockedToDt(strLockedToDt);	}		
		
		if (strOpt.equals("load")) {
			hmParam.put("AUDITID", strAuditId);
			hmParam.put("DISTID", gmFieldAuditReportForm.getDistId());			
			rdresult = gmFAVarianceRptBean.fetchVarianceByDistRpt(hmParam);		
			log.debug(" Count .." + rdresult.getRows().size());
			alResult = rdresult.getRows();			
			alResult = applySearchCriteria(alResult, gmFieldAuditReportForm);			
			gmFieldAuditReportForm.setLdtResult(alResult);
		}	
		return mapping.findForward("gmFieldAuditVarianceByDistRpt");
	}
// This method will fetch the Variance Report By Set and Global Variance Report By Distributor.
	public ActionForward reportVarianceBySet(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {
		Logger log = GmLogger.getInstance(this.getClass().getName());
		GmFieldAuditReportForm gmFieldAuditReportForm = (GmFieldAuditReportForm) form;
		GmFAVarianceRptBean gmFAVarianceRptBean = new GmFAVarianceRptBean();
		GmFASetupBean gmFASetupBean = new GmFASetupBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmProjectBean gmProj = new GmProjectBean();
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditReportForm);		
		RowSetDynaClass rdresult = null;
		ArrayList alList = new ArrayList();
		ArrayList alSets = new ArrayList();
		ArrayList alRegions = new ArrayList();
		List alResult = new ArrayList();
		
		String strOpt = gmFieldAuditReportForm.getStrOpt();
		String strReportType = gmFieldAuditReportForm.getReportType();
		
		alList = gmFASetupBean.fetchAuditList();
		gmFieldAuditReportForm.setAlAuditName(alList);
		
		alList = GmCommonClass.getCodeList("CPRSN");
		gmFieldAuditReportForm.setAlDevOpr(alList);
		
		alSets = gmProj.loadSetMap("1601");
		alRegions = GmCommonClass.parseNullArrayList((ArrayList) gmCommonBean.getRegions(""));
		gmFieldAuditReportForm.setAlSet(alSets);
		gmFieldAuditReportForm.setAlResions(alRegions);
		
		gmFieldAuditReportForm.setStrOpt(strOpt);
		gmFieldAuditReportForm.setReportType(strReportType);
		
		String strLockedFrmDt 	= GmCommonClass.parseNull((String)hmParam.get("LOCKEDFRMDT"));
		String strLockedToDt	= GmCommonClass.parseNull((String)hmParam.get("LOCKEDTODT"));
		if(gmFieldAuditReportForm.getLockedFrmDt().equals("")){ 
			gmFieldAuditReportForm.setLockedFrmDt(strLockedFrmDt); 
		}
		if(gmFieldAuditReportForm.getLockedToDt().equals("")){   
			gmFieldAuditReportForm.setLockedToDt(strLockedToDt);
		}			
		if (strReportType.equals("globalDist")) {
			hmParam.put("REGIONID", "0");
			hmParam.put("SETID", "0");
		}
		if (strOpt.equals("")) {
			gmFieldAuditReportForm.setDispOnlyDeviation(true);
		}
		if (strOpt.equals("load")) {
			rdresult = gmFAVarianceRptBean.fetchVarianceBySetRpt(hmParam);
			log.debug(" Count .." + rdresult.getRows().size());
			alResult = rdresult.getRows();
			alResult = applySearchCriteria(alResult, gmFieldAuditReportForm);
			gmFieldAuditReportForm.setLdtResult(alResult);
		}
		return mapping.findForward("gmFieldAuditVarianceBySetRpt");
	}
	
// This Method used for Appyling the Search Criteria.
	public List applySearchCriteria(List lresult, GmFieldAuditReportForm gmFieldAuditReportForm) {
		GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
		if (gmFieldAuditReportForm.isDispOnlyDeviation() == true) {
			gmSearchCriteria.addSearchCriteria("DEVIATION_QTY", "0", GmSearchCriteria.NOTEQUALS);
		}
		if (gmFieldAuditReportForm.isDispNonApproved() == true) 
		{
			gmSearchCriteria.addSearchCriteria("DISP_NONAPPROVED", "0", GmSearchCriteria.NOTEQUALS);
		}
		if(gmFieldAuditReportForm.isDispUnresolvedDet())
		{
			gmSearchCriteria.addSearchCriteria("UNRESOLVED_QTY", "0", GmSearchCriteria.NOTEQUALS);
		}
		if(gmFieldAuditReportForm.isDispNonReconciliation())
		{
			gmSearchCriteria.addSearchCriteria("DISP_NONRECONCILED", "0", GmSearchCriteria.NOTEQUALS);
		}
		if (!gmFieldAuditReportForm.getDevOpr().equals("0")) {
			gmSearchCriteria.addSearchCriteria("DEVIATION_QTY", "" + gmFieldAuditReportForm.getDevQty(), gmFieldAuditReportForm.getDevOpr());
		
		}
		if (gmFieldAuditReportForm.isDispNonflagged() == true) {
			gmSearchCriteria.addSearchCriteria("DISP_NONFLAGGED", "1", GmSearchCriteria.EQUALS);
		}
		gmSearchCriteria.query(lresult);		
		return lresult;
	}

}
