package com.globus.accounts.fieldaudit.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.fieldaudit.beans.GmFASetupBean;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditSetupForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmFieldAuditSetupAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadFieldAuditDetails() - load the Field Audit Master details, Field Sales details
   * 
   * @author mmuthusamy
   * @param
   */
  public ActionForward loadFieldAuditDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmFieldAuditSetupForm gmFieldAuditSetupForm = (GmFieldAuditSetupForm) form;
    gmFieldAuditSetupForm.loadSessionParameters(request);

    GmFASetupBean gmFASetupBean = new GmFASetupBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    ArrayList alAuditName = new ArrayList();
    ArrayList alFieldSales = new ArrayList();
    ArrayList alAuditor = new ArrayList();
    ArrayList alRegion = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alDistStatus = new ArrayList();
    ArrayList alInactiveDist = new ArrayList();

    String strOpt = "";
    String strAuditId = "";
    String strSetIds = "";
    String strParts = "";
    String strRegionId = "";
    String strAuditorId = "";

    HashMap hmParam = new HashMap();
    HashMap hmDetails = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAuditInfo = new HashMap();

    hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditSetupForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    strRegionId = GmCommonClass.parseNull((String) hmParam.get("REGIONID"));
    strAuditorId = GmCommonClass.parseZero((String) hmParam.get("PRIMARYAUDITORID"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    log.debug("strOpt" + strOpt);
    alInactiveDist =
        GmCommonClass.parseNullArrayList(gmCommonBean.getSalesFilterDist("v700.d_active_fl ='N'"));
    gmFieldAuditSetupForm.setAlInactiveDist(alInactiveDist);
    alRegion = GmCommonClass.parseNullArrayList(gmCommonBean.getRegions(""));
    gmFieldAuditSetupForm.setAlRegionName(alRegion);

    alFieldSales = GmCommonClass.parseNullArrayList(gmCommonBean.getSalesFilterDist(""));
    gmFieldAuditSetupForm.setAlDistributor(alFieldSales);

    HashMap hmAccess = new HashMap();
    hmAccess.put("GROUPNAME", "100000071"); // Field Audit - Auditor
    alAuditor =
        GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmAccess));
    gmFieldAuditSetupForm.setAlAuditor(alAuditor);

    alAuditName = gmFASetupBean.fetchAuditList();
    gmFieldAuditSetupForm.setAlAuditName(alAuditName);

    alDistStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FSSTAT"));
    gmFieldAuditSetupForm.setInvalidSetIds("");

    // if (strOpt.equals("reload")) { //We show the default Part #
    hmReturn = gmFASetupBean.fetchAuditSetupDetails(hmParam);
    hmAuditInfo = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("AUDITSETUPINFO"));
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("LOCKEDDIST"));
    strSetIds = GmCommonClass.parseNull((String) hmReturn.get("SETIDS"));
    strParts = GmCommonClass.parseNull((String) hmReturn.get("PNUMS"));
    GmCommonClass.getFormFromHashMap(gmFieldAuditSetupForm, hmAuditInfo);
    gmFieldAuditSetupForm.setSetIds(strSetIds);
    gmFieldAuditSetupForm.setPnums(strParts);
    // }
    hmDetails.put("ALDISTLIST", alFieldSales);
    hmDetails.put("ALAUDITOR", alAuditor);
    hmDetails.put("ALDISTSTATUS", alDistStatus);
    hmDetails.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    gmFieldAuditSetupForm.setGridData(getXmlGridData(alResult, hmDetails));

    return mapping.findForward("gmFieldAuditSetup");
  }

  /**
   * saveFieldAuditDetails() - save the Field Audit Master details and Field Sales details
   * 
   * @author mmuthusamy
   * @param
   */

  public ActionForward saveFieldAuditDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmFieldAuditSetupForm gmFieldAuditSetupForm = (GmFieldAuditSetupForm) form;
    gmFieldAuditSetupForm.loadSessionParameters(request);

    GmFASetupBean gmFASetupBean = new GmFASetupBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmDetails = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAuditInfo = new HashMap();

    ArrayList alAuditName = new ArrayList();
    ArrayList alFieldSales = new ArrayList();
    ArrayList alAuditor = new ArrayList();
    ArrayList alRegion = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alDistStatus = new ArrayList();
    ArrayList alInactiveDist = new ArrayList();

    String strInvalidSet = "";
    String strOpt = "";
    String strAuditId = "";
    String strSetIds = "";
    String strParts = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditSetupForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    alFieldSales = GmCommonClass.parseNullArrayList(gmCommonBean.getSalesFilterDist(""));
    gmFieldAuditSetupForm.setAlDistributor(alFieldSales);

    alRegion = GmCommonClass.parseNullArrayList(gmCommonBean.getRegions(""));
    gmFieldAuditSetupForm.setAlRegionName(alRegion);

    HashMap hmAccess = new HashMap();
    hmAccess.put("GROUPNAME", "100000071"); // Field Audit - Auditor
    alAuditor =
        GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmAccess));
    gmFieldAuditSetupForm.setAlAuditor(alAuditor);

    alInactiveDist = gmCommonBean.getSalesFilterDist("v700.d_active_fl ='N'");
    gmFieldAuditSetupForm.setAlInactiveDist(alInactiveDist);

    alDistStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FSSTAT"));
    gmFieldAuditSetupForm.setInvalidSetIds("");
    if (strOpt.equals("SAVE") || strOpt.equals("LOCK")) {
      hmReturn = gmFASetupBean.saveAuditDetails(hmParam);
      hmAuditInfo = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("AUDITSETUPINFO"));
      alResult = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("LOCKEDDIST"));
      strSetIds = GmCommonClass.parseNull((String) hmReturn.get("SETIDS"));
      strParts = GmCommonClass.parseNull((String) hmReturn.get("PNUMS"));
      strInvalidSet = GmCommonClass.parseNull((String) hmReturn.get("INVALIDSET"));
      gmFieldAuditSetupForm.setSetIds(strSetIds);
      gmFieldAuditSetupForm.setPnums(strParts);
      gmFieldAuditSetupForm.setInvalidSetIds(strInvalidSet);
      GmCommonClass.getFormFromHashMap(gmFieldAuditSetupForm, hmAuditInfo);
      log.debug(" hmAuditInfo -->  " + hmAuditInfo);
    }

    alAuditName = gmFASetupBean.fetchAuditList();
    gmFieldAuditSetupForm.setAlAuditName(alAuditName);
    hmDetails.put("ALDISTLIST", alFieldSales);
    hmDetails.put("ALAUDITOR", alAuditor);
    hmDetails.put("ALDISTSTATUS", alDistStatus);
    hmDetails.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    gmFieldAuditSetupForm.setGridData(getXmlGridData(alResult, hmDetails));

    return mapping.findForward("gmFieldAuditSetup");

  }

  /**
   * @getXmlGridData xml content generation for Field Sales lock grid
   * @author mmuthusamy
   * @param array list and hash map
   * @return String
   */

  private String getXmlGridData(ArrayList alResult, HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alDropDownDistName = new ArrayList();
    ArrayList alDropDownAuditor = new ArrayList();
    ArrayList alDropDownDistStatus = new ArrayList();

    alDropDownDistName = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALDISTLIST"));
    alDropDownAuditor = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALAUDITOR"));
    alDropDownDistStatus =
        GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALDISTSTATUS"));

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));

    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDropDownMaster("alDistributor", alDropDownDistName);
    templateUtil.setDropDownMaster("alAuditor", alDropDownAuditor);
    templateUtil.setDropDownMaster("alDistStatus", alDropDownDistStatus);
    templateUtil.setResourceBundle(GmCommonClass
        .getResourceBundleBean(
            "properties.labels.accounts.fieldaudit.GmFieldSalesLockInc",
            strSessCompanyLocale));
    templateUtil.setTemplateSubDir("accounts/fieldaudit/templates");
    templateUtil.setTemplateName("GmFieldSalesLockInc.vm");

    return templateUtil.generateOutput();
  }

  /**
   * fetchTagQty - This Method is used to Fetch the Tag Qty based on Distributor(Ajax request -
   * Distributor id)
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchTagQty(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmFieldAuditSetupForm gmFieldAuditSetupForm = (GmFieldAuditSetupForm) form;
    gmFieldAuditSetupForm.loadSessionParameters(request);

    GmFASetupBean gmFASetupBean = new GmFASetupBean(getGmDataStoreVO());
    String strDistId = GmCommonClass.parseNull(request.getParameter("distID"));

    String strTagQty = GmCommonClass.parseNull(gmFASetupBean.fetchTagQty(strDistId));
    // log.debug("Distributor --> "+strDistId + " Tag Qty -->"+strTagQty);
    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    if (!strTagQty.equals(""))
      pw.write(strTagQty);
    pw.flush();
    pw.close();
    return null;
  }

  /**
   * validateAuditName - This Method is used to validate AuditName Distributor(Ajax request - Audit
   * Name)
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward validateAuditName(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmFieldAuditSetupForm gmFieldAuditSetupForm = (GmFieldAuditSetupForm) form;
    gmFieldAuditSetupForm.loadSessionParameters(request);
    GmFASetupBean gmFASetupBean = new GmFASetupBean(getGmDataStoreVO());
    String strAuditName = GmCommonClass.parseNull(request.getParameter("auditName"));

    String strName = GmCommonClass.parseNull(gmFASetupBean.validateAuditName(strAuditName));

    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    if (!strName.equals(""))
      pw.write(strName);
    pw.flush();
    pw.close();
    return null;
  }

  /**
   * getFieldSalesRegionDtls Ajax call to get the details of region detailsvalidateAuditName
   * 
   */
  public ActionForward getFieldSalesRegionDtls(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmFieldAuditSetupForm gmFieldAuditSetupForm = (GmFieldAuditSetupForm) form;
    gmFieldAuditSetupForm.loadSessionParameters(request);
    GmFASetupBean gmFASetupBean = new GmFASetupBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    ArrayList alResult = new ArrayList();
    ArrayList alFieldSales = new ArrayList();
    ArrayList alAuditor = new ArrayList();
    ArrayList alDistStatus = new ArrayList();
    HashMap hmDetails = new HashMap();

    String strRegionId = "";
    String strAuditorId = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    strRegionId = GmCommonClass.parseNull(request.getParameter("regionId"));
    strAuditorId = GmCommonClass.parseZero(request.getParameter("primaryAuditorId"));
    log.debug("regionId and primaryAuditorId " + strAuditorId);
    alFieldSales = GmCommonClass.parseNullArrayList(gmCommonBean.getSalesFilterDist(""));
    HashMap hmAccess = new HashMap();
    hmAccess.put("GROUPNAME", "100000071"); // Field Audit - Auditor
    alAuditor =
        GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmAccess));
    alDistStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FSSTAT"));
    alResult =
        GmCommonClass.parseNullArrayList(gmFASetupBean.fetchAuditRegionSetupDetails(strRegionId,
            strAuditorId));
    hmDetails.put("ALDISTLIST", alFieldSales);
    hmDetails.put("ALAUDITOR", alAuditor);
    hmDetails.put("ALDISTSTATUS", alDistStatus);
    hmDetails.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(getXmlGridData(alResult, hmDetails));
    pw.flush();
    return null;
  }
}
