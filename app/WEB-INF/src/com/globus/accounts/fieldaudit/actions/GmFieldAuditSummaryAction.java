package com.globus.accounts.fieldaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.fieldaudit.beans.GmFAReconciliationBean;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditSummaryForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmFieldAuditSummaryAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
    GmFieldAuditSummaryForm gmFieldAuditSummaryForm = (GmFieldAuditSummaryForm) form;
    gmFieldAuditSummaryForm.loadSessionParameters(request);
    GmFAReconciliationBean gmFAReconciliationBean = new GmFAReconciliationBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();

    String strApplnDateFmt = "";
    String strFromDate = "";
    String strToDate = "";
    String strOpt = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditSummaryForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strOpt = strOpt.equals("") ? gmFieldAuditSummaryForm.getStrOpt() : strOpt;
    log.debug("strOpt: " + strOpt);

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strSessTodaysDate =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessTodaysDate"));

    // Get From/To Dates
    strFromDate = GmCommonClass.parseNull((String) hmParam.get("FASUMFROMDT"));
    strToDate = GmCommonClass.parseNull((String) hmParam.get("FASUMTODT"));
    if (strFromDate.equals("") && strToDate.equals("")) {
      strFromDate =
          strFromDate.equals("") ? GmCalenderOperations.addMonths(-1, strApplnDateFmt)
              : strFromDate;
      strToDate =
          strToDate.equals("") ? GmCalenderOperations.getCurrentDate(strApplnDateFmt) : strToDate;
      gmFieldAuditSummaryForm.setFaSumFromDt(strFromDate);
      gmFieldAuditSummaryForm.setFaSumToDt(strToDate);
    }

    hmParam.put("SESSDATEFMT", strApplnDateFmt);
    hmParam.put("FASUMFROMDT", strFromDate);
    hmParam.put("FASUMTODT", strToDate);
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

    hmResult = gmFAReconciliationBean.fetchSummaryReport(hmParam);
    hmParam.putAll(hmResult);

    request.setAttribute("hmParam", hmParam);
    gmFieldAuditSummaryForm.setXmlGridData(getXmlGridData(hmParam));

    return mapping.findForward("success");
  }

  private String getXmlGridData(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alSubRpt = new ArrayList();
    ArrayList alReport = new ArrayList();

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));

    alReport = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALAUDITINFO"));
    alSubRpt = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALFIELDSALESINFO"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alAuditList", alReport);
    templateUtil.setDataList("alFieldSales", alSubRpt);
    templateUtil.setResourceBundle(GmCommonClass
        .getResourceBundleBean(
            "properties.labels.accounts.fieldaudit.GmFieldAuditSummary",
            strSessCompanyLocale));
    templateUtil.setTemplateSubDir("accounts/fieldaudit/templates");
    templateUtil.setTemplateName("GmFieldAuditSummary.vm");

    return GmCommonClass.replaceForXML(GmCommonClass.parseNull(templateUtil.generateOutput()));
  }

}
