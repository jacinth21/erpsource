package com.globus.accounts.fieldaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.fieldaudit.beans.GmFAReconciliationBean;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditApprReconcileForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmFieldAuditApprReconcileAction extends GmDispatchAction {

  /**
   * loadFlaggedForApproval Method Used for Fetching the Flagged For Approval Report.
   */
  public ActionForward loadFlaggedForApproval(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmFieldAuditApprReconcileForm gmFieldAuditApprReconcileForm =
        (GmFieldAuditApprReconcileForm) form;
    gmFieldAuditApprReconcileForm.loadSessionParameters(request);
    GmFAReconciliationBean gmFAReconciliationBean = new GmFAReconciliationBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();
    String strOpt = "";
    String strhAction = "";
    String strDeviationType = "";
    String strApplDateFmt = "";
    String strUserId = "";
    String strAccessFlag = "";
    String strVoidFlag = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditApprReconcileForm);

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strhAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "FA_APPROVAL"));

    strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    strVoidFlag = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));

    if (!strAccessFlag.equals("Y")) {
      gmFieldAuditApprReconcileForm.setStrDisableButton("disabled");
    }

    if (strhAction.equals("save")) {
      gmFAReconciliationBean.saveApproveDeviation(hmParam);
      strOpt = "Approval";
    }
    if (strOpt.equals("Approval")) {
      strDeviationType = "Approval";
      ArrayList alResult = gmFAReconciliationBean.fetchApprReconDeviation(strDeviationType);

      hmReturn.put("APPRRECONDATA", alResult);
      hmReturn.put("SESSDATEFMT", strApplDateFmt);
      hmReturn.put("DEVIATIONTYPE", strDeviationType);
      hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      String strxmlGridData = generateOutPut(hmReturn, "GmFieldAuditApprReconcile.vm");
      gmFieldAuditApprReconcileForm.setXmlGridData(strxmlGridData);
      gmFieldAuditApprReconcileForm.setDeviationType("Approval");
    }
    return mapping.findForward("gmFieldAuditApprReconcile");
  }

  /**
   * loadApprovedForReconcile Method Used for Fetching the Approval For Reconcile Report.
   */
  public ActionForward loadApprovedForReconcile(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmFieldAuditApprReconcileForm gmFieldAuditApprReconcileForm =
        (GmFieldAuditApprReconcileForm) form;
    gmFieldAuditApprReconcileForm.loadSessionParameters(request);
    GmFAReconciliationBean gmFAReconciliationBean = new GmFAReconciliationBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();
    String strOpt = "";
    String strDeviationType = "";
    String strhAction = "";
    String strApplDateFmt = "";
    String strUserId = "";
    String strAccessFlag = "";
    String strVoidFlag = "";
    String strTxnIds = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditApprReconcileForm);

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strhAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "FA_RECONCILE"));

    strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    strVoidFlag = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));

    if (!strAccessFlag.equals("Y")) {
      gmFieldAuditApprReconcileForm.setStrDisableButton("disabled");
    }

    if (strhAction.equals("save")) {
      strTxnIds = GmCommonClass.parseNull(gmFAReconciliationBean.saveReconcileDeviation(hmParam));
      if (!strTxnIds.equals("")) {
        strTxnIds = strTxnIds.substring(0, strTxnIds.length() - 1);
        gmFieldAuditApprReconcileForm.setMessage(strTxnIds);
      }
      strOpt = "Reconcile";
    }

    if (strOpt.equals("Reconcile")) {
      strDeviationType = "Reconcile";
      ArrayList alResult = gmFAReconciliationBean.fetchApprReconDeviation(strDeviationType);

      hmReturn.put("APPRRECONDATA", alResult);
      hmReturn.put("SESSDATEFMT", strApplDateFmt);
      hmReturn.put("DEVIATIONTYPE", strDeviationType);
      hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      String strxmlGridData = generateOutPut(hmReturn, "GmFieldAuditApprReconcile.vm");
      gmFieldAuditApprReconcileForm.setXmlGridData(strxmlGridData);
      gmFieldAuditApprReconcileForm.setDeviationType("Reconcile");
    }
    return mapping.findForward("gmFieldAuditApprReconcile");
  }

  private String generateOutPut(HashMap hmParam, String vmFile) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.fieldaudit.GmFieldAuditApprReconcile", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("accounts/fieldaudit/templates");
    templateUtil.setTemplateName(vmFile);
    return templateUtil.generateOutput();
  }
}
