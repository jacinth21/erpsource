package com.globus.accounts.fieldaudit.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmFieldSalesAuditBean;
import com.globus.accounts.fieldaudit.beans.GmFAReconciliationBean;
import com.globus.accounts.fieldaudit.beans.GmFASetupBean;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditConfirmationForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;

public class GmFieldAuditConfirmationAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());
  public static final String TEMPLATE_NAME = "GmFieldAuditConfirmation";

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmFieldAuditConfirmationForm gmFieldAuditConfirmationForm = (GmFieldAuditConfirmationForm) form;
    gmFieldAuditConfirmationForm.loadSessionParameters(request);

    GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean(getGmDataStoreVO());
    GmFASetupBean gmFASetupBean = new GmFASetupBean(getGmDataStoreVO());
    GmFAReconciliationBean gmFAReconciliationBean = new GmFAReconciliationBean(getGmDataStoreVO());
    GmJasperReport gmJasperReport = new GmJasperReport();

    ArrayList alAuditList = new ArrayList();
    ArrayList alDistList = new ArrayList();
    ArrayList alPdfList = new ArrayList();
    ArrayList alReport = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmDetails = new HashMap();
    HashMap hmPendInv = new HashMap();
    String strOpt = "";
    String strPdfAction = "";
    String strAuditId = "";
    String strJasperName = "";
    String strLettterJasper = "";
    String strAuditorName = "";
    String strDistName = "";
    String strDistEmail = "";
    String fileName = "";
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");

    hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditConfirmationForm);

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    strPdfAction = GmCommonClass.parseNull((String) hmParam.get("PDFACTION"));
    gmFieldAuditConfirmationForm.setStrMessage("");

    alAuditList = GmCommonClass.parseNullArrayList(gmFASetupBean.fetchAuditList());
    alPdfList = GmCommonClass.parseNullArrayList(GmCommonClass.getAllCodeList("FACPDF"));

    alDistList =
        GmCommonClass.parseNullArrayList(gmFieldSalesAuditBean
            .fetchLockedDistributorDetails(strAuditId));

    if (strOpt.equals("Load") || strOpt.equals("PDF") || strOpt.equals("SendMail")) { // Load the
                                                                                      // reports
      hmResult =
          GmCommonClass.parseNullHashMap(gmFAReconciliationBean.fetchConfirmationReport(hmParam));
      HashMap hmHeaderInfo = (HashMap) hmResult.get("HEADERINFO");

      hmResult.putAll(hmHeaderInfo);

      if (strOpt.equals("Load")) { // To set the default letter head - Preliminary Results
        hmResult.put("LETTERHEAD", "/GmFieldAuditConfirmPreliminaryPDF.jasper");
      }
    }
    hmResult.put(
        "SUBREPORT_DIR",
        request.getSession().getServletContext()
            .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
            + "\\");


    if (strOpt.equals("PDF") || strOpt.equals("SendMail")) {
      strJasperName = "/GmFieldAuditConfirmationPDF.jasper";
      if (strPdfAction.equals("") || strPdfAction.equals("18581")) { // Preliminary Results
        strLettterJasper = "/GmFieldAuditConfirmPreliminaryPDF.jasper";
        hmResult.put("LETTERHEAD", strLettterJasper);
      } else if (strPdfAction.equals("18582")) { // Pending Charges
        strLettterJasper = "/GmFieldAuditConfirmPendingPDF.jasper";
        hmResult.put("LETTERHEAD", strLettterJasper);
        hmDetails =
            GmCommonClass
                .parseNullHashMap(gmFAReconciliationBean.fetchInvPendChargeReport(hmParam));
        alReport = GmCommonClass.parseNullArrayList((ArrayList) hmDetails.get("PEND_INV_LIST"));
        hmPendInv = GmCommonClass.parseNullHashMap((HashMap) hmDetails.get("HMDETAILS"));
        hmResult.put("REGNM", GmCommonClass.parseNull((String) hmPendInv.get("REGNM")));
        hmResult.put("TOTALCHARG", GmCommonClass.parseZero((String) hmPendInv.get("TOTALCHARG")));
        hmResult.put("PEND_INV_LIST", alReport);

        if (alReport.size() == 0) {
          throw new AppError("", "20691"); // Cannot generate the PDF for
                                           // invoiced/pending charges, missing tag
                                           // is empty.
        }
      } else if (strPdfAction.equals("18583")) { // Invoiced Charges
        strLettterJasper = "/GmFieldAuditConfirmInvoicedPDF.jasper";
        hmResult.put("LETTERHEAD", strLettterJasper);
        hmDetails =
            GmCommonClass
                .parseNullHashMap(gmFAReconciliationBean.fetchInvPendChargeReport(hmParam));
        alReport = GmCommonClass.parseNullArrayList((ArrayList) hmDetails.get("PEND_INV_LIST"));
        hmPendInv = GmCommonClass.parseNullHashMap((HashMap) hmDetails.get("HMDETAILS"));
        hmResult.put("SETCOST", GmCommonClass.parseZero((String) hmPendInv.get("SETCOST")));
        hmResult.put("PEND_INV_LIST", alReport);

        if (alReport.size() == 0) {
          throw new AppError("", "20691"); // Cannot generate the PDF for
                                           // invoiced/pending charges, missing tag
                                           // is empty.
        }
      } else if (strPdfAction.equals("18584")) { // Updated Results
        strLettterJasper = "/GmFieldAuditConfirmUpdatedPDF.jasper";
        hmResult.put("LETTERHEAD", strLettterJasper);
      } else if (strPdfAction.equals("18585")) { // Overall Reports
        strJasperName = "/GmFieldAuditConfirmationOverAllReports.jasper";
      }

      if (strLettterJasper.equals("")) {
        strLettterJasper = "/GmFieldAuditConfirmPreliminaryPDF.jasper";
      }

      gmJasperReport.setRequest(request);
      gmJasperReport.setResponse(response);
      if (strOpt.equals("PDF")) {
        gmJasperReport.setJasperReportName(strJasperName);
        gmJasperReport.setHmReportParameters(hmResult);
        gmJasperReport.setReportDataList(null);

        gmJasperReport.createJasperPdfReport();
        return null;
      } else { // SendMail
        strAuditorName = GmCommonClass.parseNull((String) hmResult.get("AUDIT_USER"));
        strDistName = GmCommonClass.parseNull((String) hmResult.get("DIST_NAME"));
        strDistEmail = GmCommonClass.parseNull((String) hmResult.get("DIST_EMAIL"));
        fileName = "Overall_Report_" + strDistName;

        String strFileName =
            createPDFFile(fileName, gmJasperReport, new HashMap(hmResult),
                "/GmFieldAuditConfirmationOverAllReports.jasper");
        GmEmailProperties gmEmailProperties = getEmailProperties(TEMPLATE_NAME);
        String strTo =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("FA_CONFIRMATION", "EMAIL"));
        gmEmailProperties.setAttachment(strFileName);
        gmEmailProperties.setRecipients(strTo);

        sendConfirmationMail(alReport, hmResult, gmEmailProperties, strLettterJasper);
        removeFile(strFileName);
        gmFieldAuditConfirmationForm.setStrMessage("Mail Send");

      }

    }

    gmFieldAuditConfirmationForm.setHmResult(hmResult);
    gmFieldAuditConfirmationForm.setAlAuditList(alAuditList);
    gmFieldAuditConfirmationForm.setAlPdfList(alPdfList);
    gmFieldAuditConfirmationForm.setAlDistList(alDistList);

    return mapping.findForward("GmFieldAuditConfirmation");
  }

  /*
   * getEmailProperties - used to get the email properties values and set it
   */

  public GmEmailProperties getEmailProperties(String strTemplate) {
    GmEmailProperties emailProps = new GmEmailProperties();

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

    emailProps.setSender(gmResourceBundleBean.getProperty(strTemplate + "."
        + GmEmailProperties.FROM));
    emailProps.setSubject(gmResourceBundleBean.getProperty(strTemplate + "."
        + GmEmailProperties.SUBJECT));
    emailProps.setMimeType(gmResourceBundleBean.getProperty(strTemplate + "."
        + GmEmailProperties.MIME_TYPE));
    emailProps.setCc(gmResourceBundleBean.getProperty(strTemplate + "." + GmEmailProperties.CC));
    return emailProps;
  }

  /*
   * createPDFFile - used to Create a new PDF file (Overall Reports)
   */

  public String createPDFFile(String strFile, GmJasperReport gmJasperReport, HashMap hmParam,
      String strJasperName) throws Exception {
    String fileName = "";
    String strFilePath = GmCommonClass.getString("UPLOADHOME");
    fileName = strFilePath + strFile + ".pdf";
    File newFile = new File(strFilePath);
    if (!newFile.exists()) {
      newFile.mkdir();
    }
    gmJasperReport.setJasperReportName(strJasperName);
    gmJasperReport.setHmReportParameters(hmParam);
    gmJasperReport.setReportDataList(null);
    gmJasperReport.exportJasperReportToPdf(fileName);

    return fileName;
  }

  /*
   * removeFile - used to Remove the PDF file (Overall Reports)
   */

  public void removeFile(String strFileName) {
    File newPDFFile = new File(strFileName);
    newPDFFile.delete();
  }

  /*
   * sendConfirmationMail - used to Send the Confirmation email
   */
  public void sendConfirmationMail(ArrayList alList, HashMap hmParam,
      GmEmailProperties gmEmailProperties, String strJasperName) {
    GmJasperMail jasperMail = new GmJasperMail();
    jasperMail.setJasperReportName(strJasperName);
    jasperMail.setAdditionalParams(hmParam);
    jasperMail.setReportData(alList);
    jasperMail.setEmailProperties(gmEmailProperties);
    HashMap hmjasperReturn = jasperMail.sendMail();

  }
}
