package com.globus.accounts.fieldaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.fieldaudit.beans.GmFAFlagDeviationBean;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditFlagVarianceForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;

public class GmFieldAuditFlagVarianceAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    GmFieldAuditFlagVarianceForm gmFieldAuditFlagVarianceForm = (GmFieldAuditFlagVarianceForm) form;
    gmFieldAuditFlagVarianceForm.loadSessionParameters(request);
    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditFlagVarianceForm);
    GmFAFlagDeviationBean gmFAFlagDeviationBean = new GmFAFlagDeviationBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());

    RowSetDynaClass rdMatchingRpt = null;
    RowSetDynaClass rdPositiveRpt = null;
    RowSetDynaClass rdPAssociatedReport = null;
    RowSetDynaClass rdNAssociatedReport = null;
    RowSetDynaClass rdNegativeRpt = null;

    List alMatchingRpt = new ArrayList();
    List alPostiveRpt = new ArrayList();
    List alPAssociatedReport = new ArrayList();
    List alNAssociatedReport = new ArrayList();
    List alNegativeRpt = new ArrayList();
    ArrayList alPCodeID = new ArrayList();
    ArrayList alNCodeID = new ArrayList();
    ArrayList alDistList = new ArrayList();
    ArrayList alPostingOptions = new ArrayList();
    ArrayList alchange = new ArrayList();

    HashMap hmHeaderInfo = new HashMap();
    HashMap hmAuditRpt = new HashMap();

    String strVarianceType = "";
    String strAuditId = "";
    String strSetType = "";

    strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    if (strOpt.equals("save")) {
      gmFAFlagDeviationBean.saveFlaggedVariance(hmParam);
    }

    HashMap hmResult = gmFAFlagDeviationBean.fetchFlagVarianceRpt(hmParam);

    hmHeaderInfo = (HashMap) hmResult.get("HEADERINFO");
    hmAuditRpt = (HashMap) hmResult.get("AUDITREPORT");
    rdMatchingRpt = (RowSetDynaClass) hmResult.get("MATCHINGREPORT");
    rdPositiveRpt = (RowSetDynaClass) hmResult.get("POSITIVEREPORT");
    rdNegativeRpt = (RowSetDynaClass) hmResult.get("NEGATIVEREPORT");
    rdPAssociatedReport = (RowSetDynaClass) hmResult.get("PASSOCIATEDREPORT");
    rdNAssociatedReport = (RowSetDynaClass) hmResult.get("NASSOCIATEDREPORT");
    strSetType = GmCommonClass.parseNull((String) hmHeaderInfo.get("SETTYPE"));
    gmFieldAuditFlagVarianceForm.setSetType(strSetType);
    hmHeaderInfo.putAll(hmAuditRpt);

    alMatchingRpt = rdMatchingRpt.getRows();
    alPostiveRpt = rdPositiveRpt.getRows();
    alNegativeRpt = rdNegativeRpt.getRows();
    alPAssociatedReport = rdPAssociatedReport.getRows();
    alNAssociatedReport = rdNAssociatedReport.getRows();

    gmFieldAuditFlagVarianceForm.setHmHeaderInfo(hmHeaderInfo);
    gmFieldAuditFlagVarianceForm.setAlMatchingReport(alMatchingRpt);
    gmFieldAuditFlagVarianceForm.setAlPositiveReport(alPostiveRpt);
    gmFieldAuditFlagVarianceForm.setAlNegativeReport(alNegativeRpt);
    gmFieldAuditFlagVarianceForm.setAlPAssociatedReport(alPAssociatedReport);
    gmFieldAuditFlagVarianceForm.setAlNAssociatedReport(alNAssociatedReport);

    alPCodeID = GmCommonClass.getCodeList("FLFPQ");// Positive Flag options
    gmFieldAuditFlagVarianceForm.setAlPCodeID(alPCodeID);
    alNCodeID = GmCommonClass.getCodeList("FLFNQ");// Negative Flag options
    gmFieldAuditFlagVarianceForm.setAlNCodeID(alNCodeID);
    alchange = GmCommonClass.getCodeList("AUDCH");// Missing Charges
    gmFieldAuditFlagVarianceForm.setAlchange(alchange);

    alPostingOptions = GmCommonClass.getCodeList("FAPOS"); // Posting Options (Skip & Proceed)
    gmFieldAuditFlagVarianceForm.setAlPostingOptions(alPostingOptions);
    alDistList = GmCommonClass.parseNullArrayList(gmCustomerBean.getDistributorList(""));
    gmFieldAuditFlagVarianceForm.setAlDistList(alDistList);
    gmFieldAuditFlagVarianceForm.setAlExtra(GmCommonClass.getCodeList("FAFLEX"));// Extra (Set,
                                                                                 // Part)
    gmFieldAuditFlagVarianceForm.setAlNotDevPositive(GmCommonClass.getCodeList("FAPNAD"));// Not A
                                                                                          // Deviation
    gmFieldAuditFlagVarianceForm.setAlUnVoidTag(GmCommonClass.getCodeList("FAPUTO"));// Unvoid tag
                                                                                     // Only
    gmFieldAuditFlagVarianceForm.setAlVerifiedPost(GmCommonClass.getCodeList("FAFLVP"));// Verified
                                                                                        // Post
                                                                                        // Count
    gmFieldAuditFlagVarianceForm.setAlNotDevNegative(GmCommonClass.getCodeList("FANNAD"));// Not A
                                                                                          // Deviation

    return mapping.findForward("GmFieldAuditFlagVariance");
  }

}
