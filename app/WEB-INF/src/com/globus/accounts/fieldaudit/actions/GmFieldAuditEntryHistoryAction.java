package com.globus.accounts.fieldaudit.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.fieldaudit.beans.GmFAFlagDeviationBean;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditFlagVarianceForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmFieldAuditEntryHistoryAction extends GmAction {

	/**
	 * @return org.apache.struts.action.ActionForward
	 * @roseuid
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		Logger log = GmLogger.getInstance(this.getClass().getName());
		GmFieldAuditFlagVarianceForm gmFieldAuditFlagVarianceForm = (GmFieldAuditFlagVarianceForm) form;
		GmFAFlagDeviationBean gmFAFlagDeviationBean = new GmFAFlagDeviationBean();
		RowSetDynaClass rdHistory = null;
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditFlagVarianceForm);
		String strAuditEntryId = GmCommonClass.parseNull((String) hmParam.get("AUDITENTRYID"));

		rdHistory = gmFAFlagDeviationBean.fetchAuditEntryHistory(strAuditEntryId);
		gmFieldAuditFlagVarianceForm.setAlHistory(rdHistory.getRows());

		return mapping.findForward("GmFieldAuditEntryHistoryReport");
	}
}
