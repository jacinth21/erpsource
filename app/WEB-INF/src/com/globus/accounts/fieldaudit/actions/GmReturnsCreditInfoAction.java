package com.globus.accounts.fieldaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.fieldaudit.beans.GmFAMiscBean;
import com.globus.accounts.fieldaudit.forms.GmReturnsCreditInfoForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmReturnsCreditInfoAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward loadReturnsCreditedInfo(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmReturnsCreditInfoForm gmReturnsCreditInfoForm = (GmReturnsCreditInfoForm) form;
    gmReturnsCreditInfoForm.loadSessionParameters(request);
    ArrayList alList = new ArrayList();
    HashMap hmReturn = new HashMap();
    RowSetDynaClass rdresult = null;
    HashMap hmParam = new HashMap();
    GmFAMiscBean gmFAMiscBean = new GmFAMiscBean();
    hmParam = GmCommonClass.getHashMapFromForm(gmReturnsCreditInfoForm);
    rdresult = gmFAMiscBean.fetchReturnsCreditRpt(hmParam);
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    alList = (ArrayList) rdresult.getRows();
    hmReturn.put("RTDATA", alList);
    hmReturn.put("SESSDATEFMT", strApplDateFmt);
    hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    String strxml = generateOutPut(hmReturn);
    gmReturnsCreditInfoForm.setXmlGridData(strxml);
    return mapping.findForward("success");
  }

  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("accounts/fieldaudit/templates");

    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.fieldaudit.templates.GmCreditedInfo", strSessCompanyLocale));
    templateUtil.setTemplateName("GmCreditedInfo.vm");
    return templateUtil.generateOutput();
  }

}
