package com.globus.accounts.fieldaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAuditVerifyBean;
import com.globus.accounts.beans.GmFieldSalesAuditBean;
import com.globus.accounts.fieldaudit.beans.GmFAFlagDeviationBean;
import com.globus.accounts.fieldaudit.beans.GmFASetupBean;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditEditUploadDataForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmFieldAuditEditUploadDataAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward verifyUploadData(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmFieldAuditEditUploadDataForm gmFieldAuditEditUploadDataForm =
        (GmFieldAuditEditUploadDataForm) form;
    gmFieldAuditEditUploadDataForm.loadSessionParameters(request);
    GmFASetupBean gmFASetupBean = new GmFASetupBean(getGmDataStoreVO());
    GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmFAFlagDeviationBean gmFAFlagDeviationBean = new GmFAFlagDeviationBean(getGmDataStoreVO());

    ArrayList alAuditName = new ArrayList();
    ArrayList alDistributor = new ArrayList();
    ArrayList alAuditor = new ArrayList();
    RowSetDynaClass rdResult = null;

    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();

    String strOpt = "";
    String strAuditId = "";
    String strDistId = "";
    String strAuditorId = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditEditUploadDataForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    log.debug("strOpt" + strOpt);

    strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));

    alAuditName = GmCommonClass.parseNullArrayList(gmFASetupBean.fetchAuditList());
    gmFieldAuditEditUploadDataForm.setAlAuditName(alAuditName); // set the AuditName ArrayList into
                                                                // form property

    if (!strAuditId.equals("") && !strAuditId.equals("0")) {

      alDistributor =
          GmCommonClass.parseNullArrayList(gmFieldSalesAuditBean
              .fetchLockedDistributorDetails(strAuditId));
      gmFieldAuditEditUploadDataForm.setAlDistributor(alDistributor);

      hmAccess.put("GROUPNAME", "100000071"); // Field Audit - Auditor
      alAuditor =
          GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchGroupMappingDetails(hmAccess));
      gmFieldAuditEditUploadDataForm.setAlAuditor(alAuditor);
    }
    if (strOpt.equals("report")) {
      hmParam.put("ENTRYTYP", "51030"); // audited
      rdResult = gmFAFlagDeviationBean.fetchVerifyUploadData(hmParam);
      gmFieldAuditEditUploadDataForm.setLdtResult(rdResult.getRows());
    } else if (strOpt.equals("loadRpt")) {
      String strAuditID = GmCommonClass.parseNull(gmFieldAuditEditUploadDataForm.getAuditId());
      String strDsitID = GmCommonClass.parseNull(gmFieldAuditEditUploadDataForm.getDistId());
      String strAuditorID = GmCommonClass.parseNull(gmFieldAuditEditUploadDataForm.getAuditorId());

      hmParam.put("ENTRYTYP", "51030"); // audited
      hmParam.put("AUDITID", strAuditID);
      hmParam.put("DISTID", strDsitID);
      hmParam.put("AUDITORID", strAuditorID);

      rdResult = gmFAFlagDeviationBean.fetchVerifyUploadData(hmParam);
      gmFieldAuditEditUploadDataForm.setLdtResult(rdResult.getRows());
      gmFieldAuditEditUploadDataForm.setReportType("SummaryRpt");
    }
    return mapping.findForward("gmFieldAuditVerifyUploadData");
  }

  public ActionForward editUploadData(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);

    Logger log = GmLogger.getInstance(this.getClass().getName());
    GmFieldAuditEditUploadDataForm gmFieldAuditEditUploadDataForm =
        (GmFieldAuditEditUploadDataForm) form;
    gmFieldAuditEditUploadDataForm.loadSessionParameters(request);

    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmAuditVerifyBean gmAuditVerifyBean = new GmAuditVerifyBean(getGmDataStoreVO());
    GmFAFlagDeviationBean gmFAFlagDeviationBean = new GmFAFlagDeviationBean(getGmDataStoreVO());

    HashMap hmAuditEntryDetails = new HashMap();
    HashMap hmFormValues = new HashMap();
    String strOpt = gmFieldAuditEditUploadDataForm.getStrOpt();
    String strDistId = gmFieldAuditEditUploadDataForm.getDistId();

    log.debug("strOpt" + strOpt);
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());

    // To Get the Set List
    ArrayList alSet = gmProjectBean.loadSetMap("1601");
    gmFieldAuditEditUploadDataForm.setAlSet(alSet);

    // To get the Status List
    ArrayList alStatus = GmCommonClass.getCodeList("SETST");
    gmFieldAuditEditUploadDataForm.setAlStatus(alStatus);

    // To get Location Type
    ArrayList alLocationType = GmCommonClass.getCodeList("FALTYP");
    gmFieldAuditEditUploadDataForm.setAlLocationType(alLocationType);

    ArrayList alDistributor =
        GmCommonClass.parseNullArrayList(gmCustomerBean.getDistributorList(""));

    hmFormValues = GmCommonClass.getHashMapFromForm(gmFieldAuditEditUploadDataForm);
    hmFormValues.put("ENTRYTYP", "51030");

    if (strOpt.equals("EDITUPLOAD") || strOpt.equals("savelockedset")) {

      gmFAFlagDeviationBean.saveEditUploadData(hmFormValues);
      if (strOpt.equals("savelockedset")) {
        strOpt = "lockedset";
      } else {
        strOpt = "onload_edit_audit_tag";
      }
      request.setAttribute("RESPONSE", "Data Saved Successfully");
    }
    if (strOpt.equals("onload_edit_audit_tag") || strOpt.equals("lockedset")) {

      hmAuditEntryDetails = gmFAFlagDeviationBean.fetchEditUploadData(hmFormValues);
      gmFieldAuditEditUploadDataForm.setHmAuditEntryDetails(hmAuditEntryDetails);
      gmFieldAuditEditUploadDataForm =
          (GmFieldAuditEditUploadDataForm) GmCommonClass.getFormFromHashMap(
              gmFieldAuditEditUploadDataForm, hmAuditEntryDetails);

      String strLocType = gmFieldAuditEditUploadDataForm.getLoctyp();
      String strLocationID = gmFieldAuditEditUploadDataForm.getLocationID();


      // To get Location
      ArrayList alLocation =
          GmCommonClass.parseNullArrayList(gmFAFlagDeviationBean.fetchLocation(strLocType));
      gmFieldAuditEditUploadDataForm.setAlLocation(alLocation);

      // To get address Type
      ArrayList alAddressType = GmCommonClass.getCodeList("ADDTP");
      gmFieldAuditEditUploadDataForm.setAlAddressType(alAddressType);

      if (!strLocType.equals("") && !strLocationID.equals("")) {
        // to get location details
        ArrayList alLocDetails =
            GmCommonClass.parseNullArrayList(gmFAFlagDeviationBean.fetchLocationInfo(strLocType,
                strLocationID));
        gmFieldAuditEditUploadDataForm.setAlLocDetails(alLocDetails);
      }


    }
    if (strOpt.equals("location") || strOpt.equals("locationDetails")) {

      String strLocType = gmFieldAuditEditUploadDataForm.getLoctyp();
      String strLocationID = gmFieldAuditEditUploadDataForm.getLocationID();

      // To get Location
      ArrayList alLocation =
          GmCommonClass.parseNullArrayList(gmFAFlagDeviationBean.fetchLocation(strLocType));
      gmFieldAuditEditUploadDataForm.setAlLocation(alLocation);

      if (strOpt.equals("locationDetails")) {
        if (!strLocType.equals("") && !strLocationID.equals("")) {
          // to get location details
          ArrayList alLocDetails =
              GmCommonClass.parseNullArrayList(gmFAFlagDeviationBean.fetchLocationInfo(strLocType,
                  strLocationID));
          gmFieldAuditEditUploadDataForm.setAlLocDetails(alLocDetails);

          if (strLocType.equals("6022")) {
            // To get address Type
            ArrayList alAddressType = GmCommonClass.getCodeList("ADDTP");
            gmFieldAuditEditUploadDataForm.setAlAddressType(alAddressType);
          }
        }
      }
    }
    String strTagStatus = GmCommonClass.parseNull(gmFieldAuditEditUploadDataForm.getTagStatus());
    String strFlagType = GmCommonClass.parseNull(gmFieldAuditEditUploadDataForm.getFlagType());
    String strFlagId = GmCommonClass.parseNull(gmFieldAuditEditUploadDataForm.getFlagDetID());
    String strFlagDetails =
        GmCommonClass.parseNull(gmFieldAuditEditUploadDataForm.getFlagDetails());
    ArrayList alFlagType = new ArrayList();
    ArrayList alFlagRefId = new ArrayList();

    gmFieldAuditEditUploadDataForm.setShowDist("No");
    if (strTagStatus.equals("18542")) { // Positive

      alFlagType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FLFPQ")); // Positive
                                                                                         // Flag
                                                                                         // options
      gmFieldAuditEditUploadDataForm.setAlFlagType(alFlagType);

      if (strFlagType.equals("51023") || strFlagType.equals("51025")) { // Borrowed/Transfer from
        gmFieldAuditEditUploadDataForm.setShowDist("Yes");
        gmFieldAuditEditUploadDataForm.setAlRefId(alDistributor);
      } else if (strFlagType.equals("51024")) { // Extra
        alFlagRefId = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FAFLEX")); // //Extra
                                                                                             // (Set,
                                                                                             // Part)
        gmFieldAuditEditUploadDataForm.setAlRefId(alFlagRefId);
      } else if (strFlagType.equals("18551")) { // Not a deviation
        alFlagRefId = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FAPNAD")); // //Not
                                                                                             // A
                                                                                             // Deviation
        gmFieldAuditEditUploadDataForm.setAlRefId(alFlagRefId);
      } else if (strFlagType.equals("18552")) { // Unvoid tag only
        alFlagRefId = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FAPUTO"));
        gmFieldAuditEditUploadDataForm.setAlRefId(alFlagRefId);
      } else { // flag type is null
        alFlagType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FLFPQ", "51015"));
        gmFieldAuditEditUploadDataForm.setAlFlagType(alFlagType);
        gmFieldAuditEditUploadDataForm.setAlRefId(alDistributor);
        gmFieldAuditEditUploadDataForm.setShowDist("Yes");
      }
    } else if (strTagStatus.equals("18543")) { // Negative
      alFlagType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FLFNQ"));
      gmFieldAuditEditUploadDataForm.setAlFlagType(alFlagType);

      if (strFlagType.equals("51026") || strFlagType.equals("51027")) { // Lent/Transfer to
        gmFieldAuditEditUploadDataForm.setShowDist("Yes");
        gmFieldAuditEditUploadDataForm.setAlRefId(alDistributor);
      } else if (strFlagType.equals("51028")) { // Missing
        alFlagRefId = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("AUDCH"));
        gmFieldAuditEditUploadDataForm.setAlRefId(alFlagRefId);
      } else if (strFlagType.equals("51029")) { // Return
        gmFieldAuditEditUploadDataForm.setShowRA("Yes");
        gmFieldAuditEditUploadDataForm.setShowDist("");
      } else if (strFlagType.equals("18556")) { // Verified post count
        alFlagRefId = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FAFLVP"));
        gmFieldAuditEditUploadDataForm.setAlRefId(alFlagRefId);
      } else if (strFlagType.equals("18557")) { // Not a deviation
        alFlagRefId = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FANNAD"));
        gmFieldAuditEditUploadDataForm.setAlRefId(alFlagRefId);
      }

    } else { // Matching
      alFlagType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("FLFPQ", "51015"));
      gmFieldAuditEditUploadDataForm.setAlFlagType(alFlagType);
      gmFieldAuditEditUploadDataForm.setAlRefId(alDistributor);
      gmFieldAuditEditUploadDataForm.setShowDist("Yes");
    }

    if (gmFieldAuditEditUploadDataForm.getAlRefId().size() == 0) {
      gmFieldAuditEditUploadDataForm.setAlRefId(alDistributor);
    }

    return mapping.findForward("gmFieldAuditEditUploadData");
  }
}
