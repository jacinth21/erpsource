package com.globus.accounts.fieldaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.fieldaudit.beans.GmFASetupBean;
import com.globus.accounts.fieldaudit.forms.GmFieldAuditSetCostForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmFieldAuditSetCostAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());


  public ActionForward editBatchSetCost(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmFieldAuditSetCostForm gmFieldAuditSetCostForm = (GmFieldAuditSetCostForm) form;
    gmFieldAuditSetCostForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    String strMessage = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditSetCostForm);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    if (strOpt.equals("save")) {
      GmFASetupBean gmFASetupBean = new GmFASetupBean();
      strMessage = GmCommonClass.parseNull(gmFASetupBean.saveSetCost(hmParam));
      gmFieldAuditSetCostForm.setMessage(strMessage.replaceAll(",", ", "));
    }
    return mapping.findForward("gmFieldAuditSetCostEdit");
  }

  public ActionForward fieldAuditSetCostRpt(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmFieldAuditSetCostForm gmFieldAuditSetCostForm = (GmFieldAuditSetCostForm) form;
    gmFieldAuditSetCostForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alResult = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmFieldAuditSetCostForm);
    GmFASetupBean gmFASetupBean = new GmFASetupBean();
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    alResult = gmFASetupBean.fetchSetCostRpt(hmParam);
    hmReturn.put("SETCOSTRPTDATA", alResult);
    hmReturn.put("APPLNDATEFMT", strApplDateFmt);
    hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    String strxmlGridData = generateOutPut(hmReturn);
    gmFieldAuditSetCostForm.setXmlGridData(strxmlGridData);
    return mapping.findForward("gmFieldAuditSetCostRpt");
  }

  /**
   * generateOutPut xml content generation for Set Cost Report grid
   * 
   * @param ArrayList
   * @return String
   */
  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.fieldaudit.GmFieldAuditSetCostRpt", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("accounts/fieldaudit/templates");
    templateUtil.setTemplateName("GmFieldAuditSetCostRpt.vm");
    return templateUtil.generateOutput();
  }

}
