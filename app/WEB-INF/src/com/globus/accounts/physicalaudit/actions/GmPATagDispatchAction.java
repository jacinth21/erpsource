// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmPATagDispatchAction.java

package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmPATagInfoBean;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmPATagDispatchAction extends GmAction {

  /**
   * @roseuid 48D03A8F035A
   */
  // public GmPATagDispatchAction()
  // {
  //
  // }
  /**
   * @param request
   * @param actionMapping
   * @param response
   * @param actionForm
   * @return org.apache.struts.action.ActionForward
   * @roseuid 48D03A8F035A
   */
  @Override
  public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());
      GmLockTagForm gmLockTagForm = (GmLockTagForm) actionForm;
      gmLockTagForm.loadSessionParameters(request);
      GmPATagInfoBean gmPATagInfoBean = new GmPATagInfoBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

      String strOpt = GmCommonClass.parseNull(gmLockTagForm.getStrOpt());
      String strPhysicalAuditID = GmCommonClass.parseNull(gmLockTagForm.getPhysicalAuditID());
      String strIncludeTags = GmCommonClass.parseNull(gmLockTagForm.getIncludeTags());
      String strExcludeTags = GmCommonClass.parseNull(gmLockTagForm.getExcludeTags());
      String strHaction = GmCommonClass.parseNull(gmLockTagForm.getHaction());
      String strUserId = GmCommonClass.parseNull(gmLockTagForm.getUserId());
      String strAccessFlag = "";
      String strVoidFlag = "";

      ArrayList alLocationList = gmPATagInfoBean.fetchLocations(strPhysicalAuditID);
      gmLockTagForm.setLocationList(alLocationList);
      gmLockTagForm.setAuditUserList(gmPATagInfoBean.fetchCountedBy(strPhysicalAuditID));
      gmLockTagForm.setAlAction(GmCommonClass.getCodeList("SVOID"));
      gmLockTagForm.setAlType(GmCommonClass.getCodeList("PRTAG"));
      List alResult = new ArrayList();
      HashMap hmParam = new HashMap();
      HashMap hmAccess = new HashMap();

      ArrayList alLocation = new ArrayList();
      ArrayList alCountedBy = new ArrayList();
      ArrayList alAuditList = new ArrayList();

      if (strOpt.equals("MissingTag")) {
        gmLockTagForm.setHaction(strOpt);
        // gmLockTagForm.setReloadMiss("ReloadMiss");
      }


      if (strOpt.equals("reload")) {
        hmParam = GmCommonClass.getHashMapFromForm(gmLockTagForm);
        log.debug(hmParam);

        if (strHaction.equals("MissingTag")) {
          hmParam.put("MISSINGFLAG", "Y");
          gmLockTagForm.setStrOpt("MissingTag");
          gmLockTagForm.setReloadMiss("ReloadMiss");
        } else {
          hmParam.put("MISSINGFLAG", "N");
        }

        alResult = gmPATagInfoBean.fetchTags(hmParam).getRows();
        log.debug("alResult.size(): " + alResult.size());
        // gmLockTagForm.setIncludeTags(String.valueOf(alResult.size()));
        // request.setAttribute("TOTALROWS",
        // String.valueOf(alResult.size()));
        gmLockTagForm.setReturnList(alResult);



      }
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "PI-TAGADMIN"));
      strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      strVoidFlag = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
      log.debug("Permissions for change status = " + hmAccess);
      if (strAccessFlag.equals("Y"))
        gmLockTagForm.setStrEnableSubmit("true");
      if (strVoidFlag.equals("Y"))
        gmLockTagForm.setStrEnableVoid("true");

      if (strOpt.equals("HTML") || strOpt.equals("PDF")) {
        log.debug("go to jasper report.");
        GmJasperReport gmJasperReport = new GmJasperReport();
        hmParam = GmCommonClass.getHashMapFromForm(gmLockTagForm);
        if (strHaction.equals("MissingTag")) {
          hmParam.put("MISSINGFLAG", "Y");
        } else {
          hmParam.put("MISSINGFLAG", "N");
        }

        // if using "tag filter" will depend on the selected the number of
        // rows
        String strInputString = strIncludeTags;
        strInputString += strExcludeTags;
        log.debug("strInputString " + strInputString);
        hmParam.put("TAGFILTER", strInputString);
        alResult = gmPATagInfoBean.fetchTags(hmParam).getRows();

        gmJasperReport.setRequest(request);
        gmJasperReport.setResponse(response);
        gmJasperReport.setJasperReportName("/GmPATagPrint.jasper");
        gmJasperReport.setHmReportParameters(hmParam);
        gmJasperReport.setReportDataList(alResult);
        // gmJasperReport.setPageHeight(1025);
        if (strOpt.equals("PDF")) {
          gmJasperReport.createJasperPdfReport();

        } else {
          gmJasperReport.createJasperHtmlReport();
        }
        return null;
      }
      alAuditList = GmCommonClass.parseNullArrayList(gmPATagInfoBean.fetchOpenPhysicalAudits());
      if (strPhysicalAuditID.equals("") && alAuditList.size() > 0) {
        HashMap hmPhysicalAudit = (HashMap) alAuditList.get(0);
        strPhysicalAuditID = (String) hmPhysicalAudit.get("CODEID");
        log.debug(" PhysicalAuditID " + strPhysicalAuditID);
      }
      GmProjectBean gmProj = new GmProjectBean();
      ArrayList alProject = new ArrayList();
      HashMap hmProj = new HashMap();
      hmProj.put("COLUMN", "ID");
      hmProj.put("STATUSID", "20301"); // Filter all approved projects
      alProject = gmProj.reportProject(hmProj);
      alLocation =
          GmCommonClass.parseNullArrayList(gmPATagInfoBean.fetchLocations(strPhysicalAuditID));

      alCountedBy =
          GmCommonClass.parseNullArrayList(gmPATagInfoBean.fetchCountedBy(strPhysicalAuditID));

      gmLockTagForm.setProjectList(alProject);
      gmLockTagForm.setLocationList(alLocation);
      gmLockTagForm.setPhysicalAuditList(alAuditList);
      gmLockTagForm.setAuditUserList(alCountedBy);
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);

    }

    return actionMapping.findForward("displaylist");

  }
}
