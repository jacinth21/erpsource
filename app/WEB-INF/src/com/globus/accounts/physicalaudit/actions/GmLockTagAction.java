// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmLockTagAction.java

package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmPATagInfoBean;
import com.globus.accounts.physicalaudit.beans.GmPATagTransactionBean;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmLockTagAction extends GmAction {
  // private String strOpt = LOAD / RELOAD / SAVE;

  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid 48D1292D0247
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {

      GmLockTagForm gmLockTagForm = (GmLockTagForm) form;
      gmLockTagForm.loadSessionParameters(request);
      GmPATagInfoBean gmPATagInfoBean = new GmPATagInfoBean(getGmDataStoreVO());
      GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
      GmPATagTransactionBean gmPATagTransactionBean =
          new GmPATagTransactionBean(getGmDataStoreVO());
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

      Logger log = GmLogger.getInstance(this.getClass().getName());

      String strOpt = gmLockTagForm.getStrOpt();
      String strPhysicalAuditID = gmLockTagForm.getPhysicalAuditID();
      String strLocationID = gmLockTagForm.getLocationID();
      String strInput = gmLockTagForm.getHinputReconAction();
      String strUserId = gmLockTagForm.getUserId();
      String strAccessFlag = "";

      if (strOpt.equals("load")) {
        ArrayList alLocationList = gmPATagInfoBean.fetchLocations(strPhysicalAuditID);
        gmLockTagForm.setLocationList(alLocationList);
        gmLockTagForm.setAuditUserList(gmPATagInfoBean.fetchCountedBy(strPhysicalAuditID));
      }


      RowSetDynaClass rdResult = null;
      HashMap hmParam = new HashMap();
      HashMap hmReturn = new HashMap();
      List alResult = new ArrayList();
      ArrayList alLocation = new ArrayList();
      ArrayList alAudit = new ArrayList();
      ArrayList alProject = new ArrayList();
      HashMap hmAccess = new HashMap();

      log.debug(GmCommonClass.getHashMapFromForm(gmLockTagForm));
      log.debug(" Testing. ..... ");
      hmParam = GmCommonClass.getHashMapFromForm(gmLockTagForm);
      String hstatuscheck = gmLockTagForm.getHstatusNotcheck();

      if (hstatuscheck.equals("true"))
        gmLockTagForm.setDisplayOnDeviation("");

      if (strOpt.equals("save")) {
        gmPATagTransactionBean.saveTagCorrection(hmParam);
        strOpt = "reload";
        gmLockTagForm.setStrOpt("reload");

      }
      if (strOpt.equals("reload")) {

        hmReturn = gmPATagInfoBean.fetchLockTagInfo(hmParam);
        rdResult = (RowSetDynaClass) hmReturn.get("LOCKTAGINFO");
        gmLockTagForm.setMaStatus(GmCommonClass.parseNull((String) hmReturn.get("MASTATUS")));
        log.debug(" alReport " + rdResult);
        alResult = rdResult.getRows();
        // log.debug("getValueNotReconciled is .............." +
        // gmLockTagForm.getValueNotReconciled());
        if (gmLockTagForm.getDisplayOnDeviation().equals("on")) {
          gmSearchCriteria.addSearchCriteria("DEVCOUNT", "0", GmSearchCriteria.NOTEQUALS);
        }
        if (gmLockTagForm.getValueNotReconciled().equals("on")) {

          gmSearchCriteria.addSearchCriteria("RECONCILEID", "50670", GmSearchCriteria.NOTEQUALS);
        }
        if (!gmLockTagForm.getDeviationOprValue().equals("")
            && !gmLockTagForm.getDeviationOprID().equals("0")) {
          gmSearchCriteria.addSearchCriteria("DEVDOLLAR", gmLockTagForm.getDeviationOprValue(),
              gmLockTagForm.getDeviationOprID());
        }
        if (!gmLockTagForm.getDeviationOprQtyValue().equals("")
            && !gmLockTagForm.getDeviationOprQtyID().equals("0")) {
          gmSearchCriteria.addSearchCriteria("DEVCOUNT", gmLockTagForm.getDeviationOprQtyValue(),
              gmLockTagForm.getDeviationOprQtyID());
        }
        gmSearchCriteria.query(alResult);

      }
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "PI-ADMIN"));
      strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      log.debug("Permissions for change status = " + hmAccess);
      if (strAccessFlag.equals("Y")) {
        gmLockTagForm.setStrEnableSubmit("true");
      }

      gmLockTagForm.setReturnList(alResult);
      HashMap hmProj = new HashMap();
      hmProj.put("COLUMN", "ID");
      hmProj.put("STATUSID", "20301"); // Filter all approved projects
      alProject = gmProj.reportProject(hmProj);

      //
      alAudit = gmPATagInfoBean.fetchOpenPhysicalAudits();
      if (strPhysicalAuditID.equals("") && alAudit.size() > 0) {
        strPhysicalAuditID = (String) ((HashMap) alAudit.get(0)).get("CODEID");
      }

      alLocation =
          GmCommonClass.parseNullArrayList(gmPATagInfoBean.fetchLocations(strPhysicalAuditID));

      gmLockTagForm.setProjectList(alProject);
      gmLockTagForm.setLocationList(alLocation);
      gmLockTagForm.setPhysicalAuditList(alAudit);
      gmLockTagForm.setDeviationOperatorsList(GmCommonClass.getCodeList("OPERS"));
      gmLockTagForm.setReconcileList(GmCommonClass.getCodeList("RETYP"));

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

}
