// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmPAManualAdjustmentAction.java

package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmPATagInfoBean;
import com.globus.accounts.physicalaudit.beans.GmPATagTransactionBean;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * New Action
 * 
 * Initial Load - To load Audit List, load Master location and project list
 * 
 * Go -- If load, Call GmFlagPartMABean.loadFlagPart to load all missing flagged Part information
 * 
 * Create MA If create MA, Call GmFlagPartMABean.createMA to generate MA for all missing flagged
 * part listed.
 */
public class GmPAManualAdjustmentAction extends GmAction {
  // private int strOpt = LOAD / RELOAD / CREATEMA;

  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid 48CED39B0023
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      GmLockTagForm gmLockTagForm = (GmLockTagForm) form;
      gmLockTagForm.loadSessionParameters(request);
      GmPATagInfoBean gmPATagInfoBean = new GmPATagInfoBean(getGmDataStoreVO());

      GmPATagTransactionBean gmPATagTransactionBean =
          new GmPATagTransactionBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

      Logger log = GmLogger.getInstance(this.getClass().getName());// Code
      // to
      // Initialize
      // the
      // Logger
      // Class.

      String strOpt = gmLockTagForm.getStrOpt();
      String strPhysicalAuditID = gmLockTagForm.getPhysicalAuditID();
      String strLocationID = gmLockTagForm.getLocationID();

      String strUserId = gmLockTagForm.getUserId();
      String strAccessFlag = "";

      RowSetDynaClass rdResult = null;
      RowSetDynaClass rdMAIDResult = null;
      HashMap hmParam = new HashMap();
      List alResult = new ArrayList();
      ArrayList alLocation = new ArrayList();
      ArrayList alAudit = new ArrayList();
      HashMap hmReturn = new HashMap();
      HashMap hmAccess = new HashMap();

      hmParam = GmCommonClass.getHashMapFromForm(gmLockTagForm);

      if (strOpt.equals("save")) {
        gmPATagTransactionBean.saveMA(hmParam);
        strOpt = "reload";
      }

      if (strOpt.equals("reload")) {

        hmReturn = gmPATagInfoBean.fetchFlagPart(hmParam);
        rdResult = (RowSetDynaClass) hmReturn.get("FLAGPARTS");
        rdMAIDResult = (RowSetDynaClass) hmReturn.get("MAIDS");

        alResult = rdResult.getRows();
        gmLockTagForm.setReturnList(alResult);
        gmLockTagForm.setMaIDList(rdMAIDResult.getRows());
        gmLockTagForm.setMaStatus(GmCommonClass.parseNull((String) hmReturn.get("MASTATUS")));
      }

      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "PI-ADMIN"));
      strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      log.debug("Permissions for change status = " + hmAccess);
      if (strAccessFlag.equals("Y")) {
        gmLockTagForm.setStrEnableSubmit("true");
      }

      gmLockTagForm.setSupervisiorList(GmCommonClass.parseNullArrayList(gmPATagInfoBean
          .fetchSupervisiorList()));
      alAudit = gmPATagInfoBean.fetchOpenPhysicalAudits();
      if (strPhysicalAuditID.equals("") && alAudit.size() > 0) {
        strPhysicalAuditID = (String) ((HashMap) alAudit.get(0)).get("CODEID");
      }

      alLocation =
          GmCommonClass.parseNullArrayList(gmPATagInfoBean.fetchLocations(strPhysicalAuditID));

      gmLockTagForm.setLocationList(alLocation);
      gmLockTagForm.setPhysicalAuditList(alAudit);

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

}
