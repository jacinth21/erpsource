// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmPAControlTagAction.java

package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmPATagInfoBean;
import com.globus.accounts.physicalaudit.beans.GmPATagTransactionBean;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmPAControlTagAction extends GmAction {

  /**
   * @roseuid 48D03A9000AA
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    {
      Logger log = GmLogger.getInstance(this.getClass().getName());
      try {
        GmLockTagForm gmLockTagForm = (GmLockTagForm) form;
        gmLockTagForm.loadSessionParameters(request);
        GmPATagInfoBean gmPATagInfoBean = new GmPATagInfoBean(getGmDataStoreVO());
        GmPATagTransactionBean gmTransBean = new GmPATagTransactionBean(getGmDataStoreVO());
        GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
        ArrayList alAudit = new ArrayList();
        ArrayList alCountedBy = new ArrayList();
        HashMap hmAccess = new HashMap();

        // String strAction = request.getParameter("hAction");
        // strAction = (strAction == null) ? "" : strAction;
        // log.debug(" action from req " + strAction);
        String strOpt = gmLockTagForm.getStrOpt();
        String strUserId = GmCommonClass.parseNull(gmLockTagForm.getUserId());
        String strAccessFlag = "";
        log.debug(" stropt " + strOpt);

        alAudit = GmCommonClass.parseNullArrayList(gmPATagInfoBean.fetchOpenPhysicalAudits());
        gmLockTagForm.setPhysicalAuditList(alAudit);

        String strPhysicalAuditID = gmLockTagForm.getPhysicalAuditID();
        String strPhysicalAuditNM = gmLockTagForm.getPhysicalAuditNM();

        if (strPhysicalAuditID.equals("") && alAudit.size() > 0) {
          HashMap hmPhysicalAudit = (HashMap) alAudit.get(0);
          strPhysicalAuditID = (String) hmPhysicalAudit.get("CODEID");
          strPhysicalAuditNM = (String) hmPhysicalAudit.get("CODENM");
        }
        gmLockTagForm.setPhysicalAuditNM(strPhysicalAuditNM);
        gmLockTagForm.setPhysicalAuditID(strPhysicalAuditID);
        alCountedBy =
            GmCommonClass.parseNullArrayList(gmPATagInfoBean.fetchCountedBy(strPhysicalAuditID));
        gmLockTagForm.setAuditUserList(alCountedBy);

        if (strOpt.equals("Load")) {
          strPhysicalAuditID = gmLockTagForm.getPhysicalAuditID();
          alCountedBy =
              GmCommonClass.parseNullArrayList(gmPATagInfoBean.fetchCountedBy(strPhysicalAuditID));
          gmLockTagForm.setAuditUserList(alCountedBy);
        }
        log.debug("Load PhysicalAuditID " + strPhysicalAuditID);
        if (strOpt.equals("Save")) {
          HashMap hmParams = GmCommonClass.getHashMapFromForm(gmLockTagForm);
          hmParams.put("USERID", gmLockTagForm.getUserId());
          gmTransBean.saveTagControl(hmParams);
          gmLockTagForm.setAuditUserID("0");
          gmLockTagForm.setTagRangeFrom("");
          gmLockTagForm.setTagRangeTo("");
        }
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "PI-TAGCONTROL"));
        strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        log.debug("Permissions for change status = " + hmAccess);
        if (strAccessFlag.equals("Y")) {
          gmLockTagForm.setStrEnableSubmit("true");
        }
      } catch (Exception exp) {
        exp.printStackTrace();
        throw new AppError(exp);
      }
      return mapping.findForward("success");
    }

    /**
     * @return org.apache.struts.action.ActionForward
     * @roseuid 48D01785005A
     */

  }
}
