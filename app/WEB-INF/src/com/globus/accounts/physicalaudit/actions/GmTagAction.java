//Source file: C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmTagAction.java

package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
 
import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.accounts.physicalaudit.forms.GmTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

 
public class GmTagAction extends GmAction {
	 
	/**
	 * @return org.apache.struts.action.ActionForward
	 * @roseuid 48CED39B0023
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
			instantiate(request, response);
			GmTagForm gmTagForm = (GmTagForm) form;
			gmTagForm.loadSessionParameters(request);
			GmTagBean gmTagBean = new GmTagBean(getGmDataStoreVO());
			GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
 
			Logger log = GmLogger.getInstance(this.getClass().getName());// Code 

			String strOpt = gmTagForm.getStrOpt();
			 
			//String strUserId = gmTagForm.getUserId();

			RowSetDynaClass rdTaggedResult = null;
			RowSetDynaClass rdUnTaggedResult = null;
			String accessFlag="";
			String strUserId="";
			 
			HashMap hmParam = new HashMap();
			HashMap hmReturn = new HashMap();
			HashMap hmAccess = new HashMap();
			
			List alTaggedResult = new ArrayList();
			List alUnTaggedResult = new ArrayList();
		 
			 
			
			hmParam = GmCommonClass.getHashMapFromForm(gmTagForm);
			strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
			hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "1016")); //Missing Tag Flag
			accessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
			if(accessFlag.equals("Y"))
			{
			gmTagForm.setMissingAccess(true);
			}
			else
			{
				gmTagForm.setMissingAccess(false);
			}
			hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "1017")) ; //unlinking Tag 
			accessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
			if(accessFlag.equals("Y"))
			{
			gmTagForm.setUnlinkAccess(true);
			}
			else
			{
				gmTagForm.setUnlinkAccess(false);
			}
			accessFlag = GmCommonClass.parseNull((String)hmAccess.get("VOIDFL"));
			log.debug("accessFlag"+accessFlag);
			if(accessFlag.equals("Y"))
			{
			gmTagForm.setVoidAccess(true);
			}
			else
			{
				gmTagForm.setVoidAccess(false);
			}
			if (strOpt.equals("save")) {
				
				log.debug(" values from hmParam >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + hmParam);
				gmTagBean.saveTagDetail(hmParam); 
				strOpt = "reload"; 
			}

			if (strOpt.equals("reload")) {
			 
				hmReturn  = (HashMap)gmTagBean.fetchTagDetail(hmParam);
				log.debug(" values in hmReturn>>>>>>>>>>>>>>>>>>>> " + hmReturn);
				rdTaggedResult = (RowSetDynaClass)hmReturn.get("TAGGEDRESULT");
				rdUnTaggedResult = (RowSetDynaClass)hmReturn.get("UNTAGGEDRESULT");
				String strIntransitFl = (String) hmReturn.get("INTRANSITFLAG");
				alTaggedResult = rdTaggedResult.getRows();
				alUnTaggedResult = rdUnTaggedResult.getRows();
				gmTagForm.setTaggedReturnList(alTaggedResult); 
				gmTagForm.setUnTaggedReturnList(alUnTaggedResult); 
				gmTagForm.setStrIntransitFlag(strIntransitFl);
			 
			} 
			  
		return mapping.findForward("GmTag");
	}

}
