// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmTagAction.java

package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.accounts.physicalaudit.forms.GmTagTransferForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmTagTransferAction extends GmAction {

  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid 48CED39B0023
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmTagTransferForm gmTagTransferForm = (GmTagTransferForm) form;
    gmTagTransferForm.loadSessionParameters(request);
    GmTagBean gmTagBean = new GmTagBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    String strException = "";
    String strIdname = "";
    String strSuccessMsg = "";
    String strAccessFlag = "";

    HashMap hmParam = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmNames = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alSetIdResult = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alTransferTo = new ArrayList();
    ArrayList alDistList = new ArrayList();
    ArrayList alInhouseList = new ArrayList();
    ArrayList alLocationType = new ArrayList();
    ArrayList alInventoryType = new ArrayList();
    String strOpt = GmCommonClass.parseNull(gmTagTransferForm.getStrOpt());
    String strTagId = GmCommonClass.parseNull(gmTagTransferForm.getTagId());
    String strPartyId = GmCommonClass.parseNull(gmTagTransferForm.getSessPartyId());
    strTagId =
        strTagId.equals("") ? GmCommonClass.parseNull(gmTagTransferForm.gethTagId()) : strTagId;
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    // To populate values of the LoanerSetID drop down
    hmValues.put("TYPE", "1601");
    alSetIdResult = gmProjectBean.loadSetMap(hmValues);

    alStatus = GmCommonClass.getCodeList("TAGST");
    alDistList = gmCustomerBean.getDistributorList("");
    alInhouseList = GmCommonClass.getCodeList("TLOCD");
    alLocationType = GmCommonClass.getCodeList("TLOCT");
    alInventoryType = GmCommonClass.getCodeList("TINVTY");
    hmNames.put("INHOUSELIST", alInhouseList);
    hmNames.put("DISTLIST", alDistList);
    gmTagTransferForm.setAlLocationType(alLocationType);
    gmTagTransferForm.setHmNames(hmNames);
    gmTagTransferForm.setAlInventoryType(alInventoryType);
    gmTagTransferForm.setAlSetIdResult(alSetIdResult);
    gmTagTransferForm.setAlStatus(alStatus);
    gmTagTransferForm.setStatus(gmTagTransferForm.getStatus());
    log.debug("strOpt========" + strOpt);

    /* Edit Tag Status:Get AccesFlag and set to Form */
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "TAG_STATUS_UPDATE"));
    strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    log.debug(" Permissions Flag = " + hmAccess);
    if (strAccessFlag.equals("Y")) {
      gmTagTransferForm.setStrTagStatusUpdateFl("Y");
    }

    if (strOpt.equals("edit")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmTagTransferForm);
      strSuccessMsg = gmTagBean.saveEditTag(hmParam);
      gmTagTransferForm.setSuccessMessage(strSuccessMsg);
    }
    hmValues = gmTagBean.fetchEditTagDetail(strTagId);
    gmTagTransferForm =
        (GmTagTransferForm) GmCommonClass.getFormFromHashMap(gmTagTransferForm, hmValues);

    return mapping.findForward("GmTagTransfer");
  }

}
