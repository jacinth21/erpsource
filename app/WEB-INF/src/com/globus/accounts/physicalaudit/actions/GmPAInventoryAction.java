// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmPAInventoryAction.java

package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmPATagInfoBean;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmPAInventoryAction extends GmAction {
  // private int strOpt = Load;

  /**
   * @roseuid 48D03A8F02DD
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());
      System.out.println("Enter");
      GmLockTagForm gmLockTagForm = (GmLockTagForm) form;
      GmPATagInfoBean gmPATagInfoBean = new GmPATagInfoBean(getGmDataStoreVO());
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      RowSetDynaClass rdResult = null;

      ArrayList alAudit = new ArrayList();
      ArrayList alLocation = new ArrayList();
      ArrayList alProject = new ArrayList();
      String strPhysicalAuditID = gmLockTagForm.getPhysicalAuditID();
      String strOpt = gmLockTagForm.getStrOpt();
      System.out.println(" stropt " + strOpt);

      HashMap hmProj = new HashMap();
      hmProj.put("COLUMN", "ID");
      hmProj.put("STATUSID", "20301"); // Filter all approved projects
      alProject = gmProj.reportProject(hmProj);
      gmLockTagForm.setProjectList(alProject);

      alAudit = gmPATagInfoBean.fetchOpenPhysicalAudits();
      gmLockTagForm.setPhysicalAuditList(alAudit);

      if (strPhysicalAuditID.equals("") && alAudit.size() > 0) {
        HashMap hmPhysicalAudit = (HashMap) alAudit.get(0);
        strPhysicalAuditID = (String) hmPhysicalAudit.get("CODEID");
      }
      if (strOpt.equals("Load")) {
        strPhysicalAuditID = gmLockTagForm.getPhysicalAuditID();
      }
      alLocation = gmPATagInfoBean.fetchLocations(strPhysicalAuditID);
      gmLockTagForm.setLocationList(alLocation);

      if (strOpt.equals("Fetch")) {

        HashMap hmParams = GmCommonClass.getHashMapFromForm(gmLockTagForm);
        System.out.println(" Fetch " + hmParams);
        rdResult = gmPATagInfoBean.fetchInventoryRpt(hmParams);
        gmLockTagForm.setReturnList(rdResult.getRows());
      }

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("success");
  }
}
