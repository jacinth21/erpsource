package com.globus.accounts.physicalaudit.actions;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.accounts.physicalaudit.forms.GmTagReportForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.operations.beans.GmDonorInfoBean;

/**
 * @author ppandiyan
 * This action is used to fetch the available count of the tag id
 */
public class GmTagValidateAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmTagBean gmTagBean = new GmTagBean(getGmDataStoreVO());
    GmTagReportForm gmTagForm = (GmTagReportForm) form;
    String strTagId = GmCommonClass.parseNull(gmTagForm.getTagId());
    int intTagCnt = gmTagBean.validateTagId(strTagId);
    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();
    pw.write(Integer.toString(intTagCnt));
    pw.flush();
    return null;

  }

}
