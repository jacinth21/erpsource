// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmPAInvSummaryAction.java

package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmPATagInfoBean;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;


public class GmPAInvSummaryAction extends GmAction {
  // private int strOpt = Load;

  /**
   * @roseuid 48D03A8F02DD
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);

    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());
      GmLockTagForm gmLockTagForm = (GmLockTagForm) form;
      GmPATagInfoBean gmPATagInfoBean = new GmPATagInfoBean(getGmDataStoreVO());

      ArrayList alAudit = new ArrayList();
      ArrayList alResult = new ArrayList();
      HashMap hmApplnParam = new HashMap();
      hmApplnParam.put("SESSCURRSYMBOL", request.getSession().getAttribute("strSessCurrSymbol"));
      String strPhysicalAuditID = gmLockTagForm.getPhysicalAuditID();
      String strOpt = gmLockTagForm.getStrOpt();
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) request.getSession()
              .getAttribute("strSessCompanyLocale"));

      log.debug(" stropt " + strOpt + "strPhysicalAuditID-" + strPhysicalAuditID);

      alAudit = gmPATagInfoBean.fetchOpenPhysicalAudits();
      gmLockTagForm.setPhysicalAuditList(alAudit);

      if (strOpt.equals("summary")) {

        alResult = gmPATagInfoBean.fetchInvSummaryRpt(strPhysicalAuditID, "N");
        GmTemplateUtil templateUtil = new GmTemplateUtil();
        templateUtil.setDataList("alResult", alResult);
        templateUtil.setDataMap("hmApplnParam", hmApplnParam);
        templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.accounts.physicalaudit.GmPISummaryReport", strSessCompanyLocale));
        templateUtil.setTemplateName("GmPISummaryReport.vm");
        templateUtil.setTemplateSubDir("accounts/physicalaudit/templates");
        gmLockTagForm.setGridXmlData(GmCommonClass.replaceForXML(templateUtil.generateOutput()));


      } else if (strOpt.equals("detail")) {

        alResult = gmPATagInfoBean.fetchInvSummaryRpt(strPhysicalAuditID, "Y");
        gmLockTagForm.setAlSummaryList(alResult);

      }

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("success");
  }
}
