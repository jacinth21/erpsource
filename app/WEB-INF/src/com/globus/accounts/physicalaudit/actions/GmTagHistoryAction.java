
package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.accounts.physicalaudit.forms.GmTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


public class GmTagHistoryAction extends GmAction {
	
	
	/**
	    * @return org.apache.struts.action.ActionForward
	    * @roseuid 
	    */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

				
			Logger log = GmLogger.getInstance(this.getClass().getName());
			GmTagForm gmTagForm = (GmTagForm) form;
			GmTagBean gmTagBean = new GmTagBean();
			RowSetDynaClass rdHistory= null;
						
			//alReportList = GmCommonClass.parseNullArrayList(gmTagBean.fetchTagHistory(gmTagForm.getTagID()));
			rdHistory = gmTagBean.fetchTagHistory(gmTagForm.getTagID());
			log.debug("rdHistory:" + rdHistory);
			gmTagForm.setRdHistory(rdHistory);
			
		
		return mapping.findForward("GmTagHistoryReport");
	}
}
