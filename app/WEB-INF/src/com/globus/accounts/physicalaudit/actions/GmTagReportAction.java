package com.globus.accounts.physicalaudit.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.accounts.physicalaudit.forms.GmTagReportForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.custservice.beans.GmCustomerBean;

public class GmTagReportAction extends GmAction {

  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid 48CED39B3454353450023
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    HttpSession session = request.getSession();

    GmTagReportForm gmTagReportForm = (GmTagReportForm) form;
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    gmTagReportForm.loadSessionParameters(request);
    String strUserId = GmCommonClass.parseNull(gmTagReportForm.getUserId());
    GmTagBean gmTagBean = new GmTagBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    ArrayList alStatus = new ArrayList();
    ArrayList alLocationType = new ArrayList();
    ArrayList alInhouseList = new ArrayList();
    ArrayList alDistList = new ArrayList();
    ArrayList alInventoryType = new ArrayList();
    ArrayList alCompanyList = new ArrayList();
    ArrayList alPlantList = new ArrayList();
    String strDefalultCompany = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    String strDefalultPlant = GmCommonClass.parseNull(getGmDataStoreVO().getPlantid());

    HashMap hmParam = new HashMap();
    HashMap hmNames = new HashMap();
    HashMap hmAccess = new HashMap();
    boolean blTransAccess = false;
    boolean blVoidAccess = false;
    String strAccessFlag = "";
    RowSetDynaClass rdReport = null;
    RowSetDynaClass rdAccess = null;
    ArrayList alresult = new ArrayList();

    alStatus = GmCommonClass.getCodeList("TAGST", getGmDataStoreVO());
    gmTagReportForm.setAlStatus(alStatus);

    // Inventory type added drop down for filter condition -
    // Consignment,Loaner,In-House
    // Consignment/Loaner
    alInventoryType = GmCommonClass.getCodeList("TINVTY");

    alLocationType = GmCommonClass.getCodeList("TLOCT");
    alDistList = gmCustomerBean.getDistributorList("");
    alInhouseList = GmCommonClass.getCodeList("TLOCD");
    hmNames.put("INHOUSELIST", alInhouseList);
    hmNames.put("DISTLIST", alDistList);

    alCompanyList = GmCommonClass.parseNullArrayList(gmCommonBean.getCompanyList());
    alPlantList = GmCommonClass.parseNullArrayList(gmCommonBean.getPlantList());
    gmTagReportForm.setAlLocationType(alLocationType);
    gmTagReportForm.setAlInventoryType(alInventoryType);
    gmTagReportForm.setAlCompanyList(alCompanyList);
    gmTagReportForm.setAlPlantList(alPlantList);

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "3250005"));
    log.debug("hmAccess  " + hmAccess);

    strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

    blTransAccess = strAccessFlag.equals("Y") ? true : false;
    gmTagReportForm.setTransferAccess(blTransAccess);

    strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));

    blVoidAccess = strAccessFlag.equals("Y") ? true : false;
    gmTagReportForm.setVoidAccess(blVoidAccess);

    hmParam = GmCommonClass.getHashMapFromForm(gmTagReportForm);
    String strName = GmCommonClass.parseNull((String) hmParam.get("NAME"));
    gmTagReportForm.setHmNames(hmNames);

    String strOpt = GmCommonClass.parseNull(gmTagReportForm.getStrOpt());
    log.debug("strOpt is:" + strOpt);

    String strHAcrion = GmCommonClass.parseNull(gmTagReportForm.getHaction());
    String strCompanyID = GmCommonClass.parseNull(gmTagReportForm.getCompanyNm());
    String strPlantID = GmCommonClass.parseNull(gmTagReportForm.getPalntNm());
    String strXMLString = "";
    ArrayList alCompmanyPlantList = new ArrayList();
    HashMap hmResult = new HashMap();
    String strDefaultPlant = "";
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
    strCompanyID = strCompanyID.equals("") ? strDefalultCompany : strCompanyID;
    strPlantID = strPlantID.equals("") ? strDefalultPlant : strPlantID;
    hmResult = gmCommonBean.fetchCompanyPlantList(strCompanyID);
    alCompmanyPlantList = (ArrayList) hmResult.get("PLANTLIST");
    strDefaultPlant = (String) hmResult.get("DEFAULTPLANT");
    if (strOpt.equals("fetchCompanyPlants")) {
      strDefaultPlant = (String) hmResult.get("DEFAULTPLANT");
      strXMLString =
          GmCommonClass.parseNull(gmXMLParserUtility.createXMLString(alCompmanyPlantList));
      strXMLString =
          "<response>" + strXMLString + "<defplant>" + strDefaultPlant + "</defplant></response>";
      log.debug("The strXMLString **** " + strXMLString);
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      if (alCompmanyPlantList.size() > 0) {
        pw.write(strXMLString);
      }
      pw.flush();
      return null;
    }
    gmTagReportForm.setAlPlantList(alCompmanyPlantList);
    HashMap hmReturn = new HashMap();
    if (!strOpt.equals("Load") && !strOpt.equals("") && !strOpt.equals("fetchCompanyPlants")) {
    	String strStatus = GmCommonClass.parseNull(request.getParameter("status"));
    	gmTagReportForm.setTypeStatus(strStatus);
    	hmParam = GmCommonClass.getHashMapFromForm(gmTagReportForm);
      // For this report, we do not need to include the Override Access for Internal employees.
      // For Sales rep the existing sales condition will be attached to the filter.
      request.setAttribute("IGNORE_OVERRIDE_ACS", "Y");
      rdReport =
          gmTagBean.loadTagDetails(hmParam,
              GmCommonClass.parseNull(getSalesAccessCondition(request, response)));
    }

    if (!strCompanyID.equals("") && !strPlantID.equals("")) {
      gmTagReportForm.setCompanyNm(strCompanyID);
      gmTagReportForm.setPalntNm(strPlantID);
    } else {
      gmTagReportForm.setPalntNm(strDefalultPlant);
      gmTagReportForm.setCompanyNm(strDefalultCompany);
    }

    gmTagReportForm.setRdReport(rdReport);
    gmTagReportForm.setLocationId(strName);
    return mapping.findForward("GmTagReport");
  }
}
