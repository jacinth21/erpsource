package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.accounts.physicalaudit.forms.GmTagStatusForm;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.shipping.beans.GmShippingInfoBean;

public class GmTagStatusAction extends GmAction {

	/**
	 * @return org.apache.struts.action.ActionForward
	 * 
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
		
		GmTagStatusForm gmTagStatusForm = (GmTagStatusForm) form;
		gmTagStatusForm.loadSessionParameters(request);
		GmTagBean gmTagBean = new GmTagBean();
		ArrayList alStatus = new ArrayList();
		HashMap hmParam = new HashMap();
		
		
		RowSetDynaClass rdReport = null;
		alStatus = GmCommonClass.getCodeList("TAGST");
		gmTagStatusForm.setAlStatus(alStatus);
		
		String strOpt = GmCommonClass.parseNull((String) gmTagStatusForm.getStrOpt());
		log.debug("strOpt is:" + strOpt);
		hmParam = GmCommonClass.getHashMapFromForm(gmTagStatusForm);
		if (strOpt.equals("save")){
			gmTagBean.saveTagStatus(hmParam);
		}
		return mapping.findForward("GmTagStatus");
	}

}