// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmPAGenerateTagAction.java

package com.globus.accounts.physicalaudit.actions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javazoom.upload.UploadException;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.LazyDynaBean;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.globus.accounts.physicalaudit.beans.GmPATagInfoBean;
import com.globus.accounts.physicalaudit.beans.GmPATagTransactionBean;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.excel.GmExcelManipulation;
import com.globus.prodmgmnt.beans.GmProjectBean;


public class GmPAGenerateTagAction extends GmAction {
  // private int strOpt = LOAD / GENERATETAG;



  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    try {
      // ArrayList


      log.debug("Enter");
      GmLockTagForm gmLockTagForm = (GmLockTagForm) form;
      GmPATagInfoBean gmPATagInfoBean = new GmPATagInfoBean(getGmDataStoreVO());
      GmPATagTransactionBean gmPATagTransactionBean =
          new GmPATagTransactionBean(getGmDataStoreVO());
      GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      gmLockTagForm.loadSessionParameters(request);
      HashMap hmParam = GmCommonClass.getHashMapFromForm(gmLockTagForm);
      String strPhysicalAuditID = gmLockTagForm.getPhysicalAuditID();
      String strUserId = GmCommonClass.parseNull(gmLockTagForm.getUserId());
      ArrayList alReturn = new ArrayList();
      ArrayList alMissingPart = new ArrayList();
      ArrayList alResult = new ArrayList();
      ArrayList alExcelData = new ArrayList();

      String strAccessFlag = "";
      String strFileName = "";
      String strUploadHome = GmCommonClass.getString("GMPHYSICALAUDITUPLOAD");
      String strMissingpart = "";

      ArrayList alPhysicalAuditList = gmPATagInfoBean.fetchOpenPhysicalAudits();
      gmLockTagForm.setPhysicalAuditList(alPhysicalAuditList);

      if (strPhysicalAuditID.equals("") && alPhysicalAuditList.size() > 0) {
        HashMap hmPhysicalAudit = (HashMap) alPhysicalAuditList.get(0);
        strPhysicalAuditID = (String) hmPhysicalAudit.get("CODEID");
      }
      ArrayList alLocationList = gmPATagInfoBean.fetchLocations(strPhysicalAuditID);
      gmLockTagForm.setLocationList(alLocationList);
      gmLockTagForm.setAuditUserList(gmPATagInfoBean.fetchCountedBy(strPhysicalAuditID));
      gmLockTagForm.setAlType(GmCommonClass.getCodeList("PRTAG"));
      // Filter all approved projects fetch Project List
      HashMap hmTemp = new HashMap();
      HashMap hmAccess = new HashMap();
      HashMap hmExcle = new HashMap();
      hmTemp.put("COLUMN", "ID");
      hmTemp.put("STATUSID", "20301");
      gmLockTagForm.setProjectList(gmProjectBean.reportProject(hmTemp));
      GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();
      String physicalAuditID = "";
      RowSetDynaClass rowSetDynaClass = null;
      GmPATagInfoBean GmPATagInfoBean = null;
      if (gmLockTagForm.getStrOpt().equals("RELOAD")) {
        FormFile fileUpload = gmLockTagForm.getUploadExcel();

        int intFileSize = fileUpload.getFileSize();
        log.debug("intFileSize:" + intFileSize);
        // if(fileUpload!= null){

        strFileName = GmCommonClass.parseNull(fileUpload.getFileName());
        if (!strFileName.equals("")) {
          // if(strFileName.equalsIgnoreCase(strFileNameUpload)){
          try {
            log.debug("Excel :" + fileUpload.getFileName());
            uploadFile(fileUpload, strUploadHome);
            gmExcelManipulation.setExcelFileName(strUploadHome + strFileName);
            gmExcelManipulation.setSheetNumber(0);
            gmExcelManipulation.setRowNumber(1);
            gmExcelManipulation.setColumnNumber(0);
            gmExcelManipulation.setMaxColumnNumberToRead(4);
            alExcelData = (ArrayList) gmExcelManipulation.readFromExcel();
          } catch (Exception e) {
            throw new AppError("", "20675");
          }
          physicalAuditID = GmCommonClass.parseNull((String) hmParam.get("PHYSICALAUDITID"));
          hmExcle =
              GmCommonClass.parseNullHashMap(gmPATagInfoBean.fetchExcelLockedTransactions(
                  alExcelData, physicalAuditID));
          alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmExcle.get("ALRESULT"));
          alMissingPart =
              GmCommonClass.parseNullArrayList((ArrayList) hmExcle.get("ALMISSINGPART"));

          gmLockTagForm.setStrCheckExcel("CHECK");
          // }//else{
          // throw new AppError("Please upload a valid file.", "", 'E');
          // }
        } else {
          if (!gmLockTagForm.getLocationID().equals("0")
              && !gmLockTagForm.getBlankTag().equalsIgnoreCase("true")) {
            alReturn = (ArrayList) gmPATagInfoBean.fetchLockedTransactions(hmParam).getRows();
            // gmLockTagForm.setLocationListWithType(gmPATagInfoBean.fetchLocationsForType(strPhysicalAuditID,
            // gmLockTagForm.getLocationID()));
            alLocationList =
                gmPATagInfoBean.fetchLocationsForType(strPhysicalAuditID,
                    gmLockTagForm.getLocationID());
          } else if (!gmLockTagForm.getBlankTag().equalsIgnoreCase("true")) {
            alReturn = (ArrayList) gmPATagInfoBean.fetchLockedTransactions(hmParam).getRows();
          }

          if (gmLockTagForm.getBlankTag().equalsIgnoreCase("true")) {
            addBlankTag(alReturn);
          }
        }
        gmLockTagForm.setLocationListWithType(alLocationList);
        gmLockTagForm.setReturnList(alReturn);
        gmLockTagForm.setMissingPart(alMissingPart);
        // }
      } else if (gmLockTagForm.getStrOpt().equals("HTML")
          || gmLockTagForm.getStrOpt().equals("PDF")) {
        log.debug("Inside Generate Tag");
        GmJasperReport gmJasperReport = new GmJasperReport();
        alReturn = gmPATagTransactionBean.saveTags(hmParam);
        gmJasperReport.setRequest(request);
        gmJasperReport.setResponse(response);
        gmJasperReport.setJasperReportName("/GmPATagPrint.jasper");
        gmJasperReport.setHmReportParameters(hmParam);
        gmJasperReport.setReportDataList(alReturn);
        if (gmLockTagForm.getStrOpt().equals("PDF")) {
          gmJasperReport.createJasperPdfReport();
        } else {
          gmJasperReport.createJasperHtmlReport();
        }
        return null;
      }
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "PI-TAGADMIN"));
      strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      log.debug("Permissions for change status = " + hmAccess);
      if (strAccessFlag.equals("Y")) {
        gmLockTagForm.setStrEnableSubmit("true");
      }

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    log.debug("Exit");
    return mapping.findForward("success");
  }

  private void addBlankTag(ArrayList alList) {
    DynaBean dynaBean = new LazyDynaBean();
    dynaBean.set("PNUM", "-");
    dynaBean.set("TXNDESC", "-");
    dynaBean.set("LOCATIONNM", "-");
    dynaBean.set("COUNTED_BY", "-");
    dynaBean.set("TAGCOUNT", "-");
    dynaBean.set("REFID", "-");
    alList.add(dynaBean);
  }


  public void uploadFile(FormFile formFile, String strUploadHome) throws UploadException,
      IOException {
    // Upload the file on server
    FileOutputStream outputStream = null;
    File file = new File(strUploadHome);
    if (!file.exists()) {
      file.mkdir();
    }
    String strUploadPath = strUploadHome + formFile.getFileName();
    try {
      outputStream = new FileOutputStream(new File(strUploadPath));
      outputStream.write(formFile.getFileData());
    } finally {
      if (outputStream != null) {
        outputStream.close();
      }
    }


  }
}
