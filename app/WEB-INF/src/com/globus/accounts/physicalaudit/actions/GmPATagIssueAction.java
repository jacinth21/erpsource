// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmPATagIssueAction.java

package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmPATagInfoBean;
import com.globus.accounts.physicalaudit.beans.GmPATagTransactionBean;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmPATagIssueAction extends GmAction {

  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid 48D01BC50002
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    {

      Logger log = GmLogger.getInstance(this.getClass().getName());
      try {
        log.debug("Enter");
        GmLockTagForm gmLockTagForm = (GmLockTagForm) form;
        GmPATagInfoBean gmPATagInfoBean = new GmPATagInfoBean(getGmDataStoreVO());
        GmPATagTransactionBean gmPATagTransactionBean =
            new GmPATagTransactionBean(getGmDataStoreVO());
        GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
        gmLockTagForm.loadSessionParameters(request);
        HashMap hmParam = GmCommonClass.getHashMapFromForm(gmLockTagForm);
        String strPhysicalAuditID = gmLockTagForm.getPhysicalAuditID();
        HashMap hmReturn = new HashMap();
        HashMap hmAccess = new HashMap();
        String strTagID = gmLockTagForm.getTagID();
        String strPartNumber = gmLockTagForm.getPnum();
        String strTransactionID = gmLockTagForm.getTransactionID();
        String strUserId = GmCommonClass.parseNull(gmLockTagForm.getUserId());
        String strAccessFlag = "";
        String strVoidFlag = "";
        String strPartNum = "";
        String strhPnum = "";
        String strhTxnID = "";
        String strInvType = "";
        strPartNum = strPartNumber.equals("") ? strTransactionID : strPartNumber;
        strInvType = strPartNumber.equals("") ? "transaction" : "part";

        ArrayList alPhysicalAuditList = gmPATagInfoBean.fetchOpenPhysicalAudits();
        gmLockTagForm.setPhysicalAuditList(alPhysicalAuditList);

        if (strPhysicalAuditID.equals("") && alPhysicalAuditList.size() > 0) {
          HashMap hmPhysicalAudit = (HashMap) alPhysicalAuditList.get(0);
          strPhysicalAuditID = (String) hmPhysicalAudit.get("CODEID");
        }
        ArrayList alLocationList = gmPATagInfoBean.fetchLocations(strPhysicalAuditID);
        gmLockTagForm.setLocationList(alLocationList);
        gmLockTagForm.setAuditUserList(gmPATagInfoBean.fetchCountedBy(strPhysicalAuditID));

        if (gmLockTagForm.getStrOpt().equals("SAVE")) {
          log.debug("hmParam: " + hmParam);
          if (gmLockTagForm.getAuditUserID().equals("0")) {
            throw new AppError("", "20691");
          }

          if (gmLockTagForm.getLocationID().equals("0")) {
            throw new AppError("", "20692");
          }

          gmPATagInfoBean.fetchPartDetail(strPartNumber, strTransactionID, strInvType);
          gmPATagTransactionBean.saveTagDetail(hmParam);
          gmLockTagForm.reset();
        }

        if (gmLockTagForm.getStrOpt().equals("RELOAD")) {

          if (!strTagID.equals("")) {
            hmReturn = gmPATagInfoBean.fetchTagDetail(strTagID);
            GmCommonClass.getFormFromHashMap(gmLockTagForm, hmReturn);
          }
          strhPnum = GmCommonClass.parseNull(gmLockTagForm.getPnum());
          strhTxnID = GmCommonClass.parseNull(gmLockTagForm.getTransactionID());
          gmLockTagForm.sethPart(strhPnum);
          gmLockTagForm.sethTransactionID(strhTxnID);

          if (!strPartNum.equals("")) {
            hmReturn = gmPATagInfoBean.fetchPartDetail(strPartNumber, strTransactionID, strInvType);
            gmLockTagForm.setTransactionID(strTransactionID);
            gmLockTagForm.setPnum(strPartNumber);
            gmLockTagForm.setPartDescription((String) hmReturn.get("PARTDESC"));
            gmLockTagForm.setUom((String) hmReturn.get("UOM"));
          }

        }
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "PI-TAGRECORDING"));
        strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        log.debug("Permissions for change status = " + hmAccess);
        if (strAccessFlag.equals("Y")) {
          gmLockTagForm.setStrEnableSubmit("true");
        }
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "PI-DATAENTRYADMIN"));
        strVoidFlag = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
        log.debug("Permissions for change status = " + hmAccess);
        if (strVoidFlag.equals("Y")) {
          gmLockTagForm.setStrEnableVoid("true");
        }

      } catch (Exception exp) {
        log.debug("exception in action : ");
        exp.printStackTrace();
        throw new AppError(exp);
      }
      log.debug("Exit");
      return mapping.findForward("success");
    }
  }
}
