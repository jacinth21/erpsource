package com.globus.accounts.physicalaudit.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmTagBean;
import com.globus.accounts.physicalaudit.forms.GmTagReportForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmMoveTagLocationAction extends GmAction {

  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid 48CED39B0023
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    GmTagReportForm gmTagReportForm = (GmTagReportForm) form;
    gmTagReportForm.loadSessionParameters(request);
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    ArrayList alDistList = null;
    ArrayList alAccList = null;
    ArrayList alRepList = null;

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    GmTagBean gmTagBean = new GmTagBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();

    // log.debug("inside GmShippingAction.GmShipDetailsForm action class");

    HashMap hmNames = new HashMap();

    alDistList = gmCommonBean.getDistributorList();
    alAccList = gmCommonBean.getAccountList("");
    alRepList = gmCustomerBean.getRepList();

    ArrayList alSet = gmProjectBean.loadSetMap("1601"); // 1601 is for current set list
    hmNames.put("REPLIST", alRepList);
    hmNames.put("ACCLIST", alAccList);
    hmNames.put("DISTLIST", alDistList);
    hmNames.put("SETLIST", alSet);

    gmTagReportForm.setAlDistributor(alDistList);
    gmTagReportForm.setAlAccList(alAccList);
    gmTagReportForm.setAlRepList(alRepList);
    gmTagReportForm.setAlSet(alSet);

    String strOpt = gmTagReportForm.getStrOpt();
    String strInputString = request.getParameter("hinputString");
    log.debug("strOpt is:" + strOpt);
    log.debug("strOpt is:" + gmTagReportForm.getDistributorId());

    if (strOpt.equals("Report") | strOpt.equals("save")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmTagReportForm);
      hmParam.put("INPUTSTR", strInputString);

      if (!strInputString.equals("")) {
        log.debug("strInputString is:" + strInputString);
        gmTagBean.saveChangedLocations(hmParam);
      }
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      log.debug("strOpt is:" + gmTagReportForm.getDistributorId());
      /*
       * rdReport = gmTagBean.loadTagCurrentLocationDetails(hmParam);
       * gmTagReportForm.setRdReport(rdReport);
       */
      ArrayList alResult = gmTagBean.loadTagCurrentLocationDetails(hmParam);
      gmTagReportForm.setGridXmlData(gmTagBean.getXmlGridData(alResult, hmNames));
      // log.debug("DATA :: " + gmTagReportForm.getGridXmlData());
    }

    return mapping.findForward("success");
  }


}
