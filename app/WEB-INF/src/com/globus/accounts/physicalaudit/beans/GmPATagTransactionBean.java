// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\beans\\GmPATagTransactionBean.java

package com.globus.accounts.physicalaudit.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPATagTransactionBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @roseuid 48D03B9403A0
   */

  // this will be removed all place changed with Data Store VO constructor
  public GmPATagTransactionBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }


  /**
   * Constructor will populate company info.
   * 
   * 
   * @param gmDataStore
   */
  public GmPATagTransactionBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * Will call the procedure gm_sav_tags()
   * 
   * @param hmParam
   * @return java.lang.Void
   * @roseuid 48D00EA5039D
   */
  public ArrayList saveTags(HashMap hmParam) throws AppError {
    RowSetDynaClass rd = null;

    log.debug("hmParam: " + hmParam);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPhysicalAuditID = GmCommonClass.parseNull((String) hmParam.get("PHYSICALAUDITID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strLocationID = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_ac_patag_trans.gm_sav_tags", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPhysicalAuditID);
    gmDBManager.setString(2, strLocationID);
    gmDBManager.setString(3, strInputString);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    rd = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));
    gmDBManager.commit();
    log.debug("rd.getRows(): " + rd.getRows());
    log.debug("Exit");
    return (ArrayList) rd.getRows();
  }

  /**
   * Will call the procedure Gm_sav_tag_control
   * 
   * @param hmParam
   * @return java.lang.Void
   * @roseuid 48D00EA5039F
   */
  public void saveTagControl(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strFrom = GmCommonClass.parseNull((String) hmParam.get("TAGRANGEFROM"));
    String strTo = GmCommonClass.parseNull((String) hmParam.get("TAGRANGETO"));
    String strCountdBy = GmCommonClass.parseNull((String) hmParam.get("AUDITUSERID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strPAId = GmCommonClass.parseNull((String) hmParam.get("PHYSICALAUDITID"));

    // log.debug(" strFrom: " + strFrom + " strTo: " + strTo + "
    // strCountdBy: " + strCountdBy + " strUserId: " + strUserId);
    gmDBManager.setPrepareString("gm_pkg_ac_patag_trans.gm_sav_tag_control", 5);
    gmDBManager.setInt(1, Integer.parseInt(strCountdBy));
    gmDBManager.setString(2, strFrom);
    gmDBManager.setString(3, strTo);
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, strPAId);

    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * Will call the procedure Gm_sav_tag_detail
   * 
   * @return java.lang.Void
   * @roseuid 48D00EA503A1
   */
  public void saveTagDetail(HashMap hmParam) throws AppError {

    String strTagID = GmCommonClass.parseNull((String) hmParam.get("TAGID"));
    String strLocationID = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));
    String strCountedBy = GmCommonClass.parseNull((String) hmParam.get("AUDITUSERID"));
    String strTransactionID = GmCommonClass.parseNull((String) hmParam.get("TRANSACTIONID"));
    String strTagQty = GmCommonClass.parseNull((String) hmParam.get("TAGQTY"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    // String strPartNum = !(GmCommonClass.parseNull((String)
    // hmParam.get("PARTDESCRIPTION")).equals("")) ? strTransactionID : "";
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strTxnID = "";


    // String strTxnID = (!strPartNum.equals("")) ? "" : strTransactionID;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("GmCommonClass.parseNull((String) hmParam.get(PARTDESCRIPTION)"
        + GmCommonClass.parseNull((String) hmParam.get("PARTDESCRIPTION")));
    gmDBManager.setPrepareString("gm_pkg_ac_patag_trans.gm_sav_update_tag", 7);
    gmDBManager.setString(1, strTagID);
    gmDBManager.setString(2, strLocationID);
    gmDBManager.setString(3, strCountedBy);
    gmDBManager.setString(4, strPartNum);
    gmDBManager.setString(5, strTransactionID);
    gmDBManager.setString(6, strTagQty);
    gmDBManager.setString(7, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * New Method Calls following stored procedure to save the value
   * 
   * Calls gm_pkg_ac_locktag.GM_SAV_TAG_CORRECTION to save info inputstring (compose of partNm_id,
   * actiontype ) as in parameter
   * 
   * @roseuid 48D0204E0307
   */
  public void saveTagCorrection(HashMap hmParam) throws AppError {
    String strPhysicalAuditID = GmCommonClass.parseNull((String) hmParam.get("PHYSICALAUDITID"));
    String inputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTRECONACTION"));
    String strLocationID = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));
    String strVerifyLocation = GmCommonClass.parseNull((String) hmParam.get("VERIFYLOCATION"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS, getGmDataStoreVO());
    // log.debug("inputString......................."+inputString);

    strVerifyLocation = strVerifyLocation.equals("on") ? "Y" : "N";
    gmDBManager.setPrepareString("gm_pkg_ac_patag_trans.gm_sav_tag_correction", 5);
    gmDBManager.setString(1, strPhysicalAuditID);
    gmDBManager.setString(2, strLocationID);
    gmDBManager.setString(3, inputString);
    gmDBManager.setString(4, strVerifyLocation);
    gmDBManager.setString(5, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * @roseuid 48D027490318
   */
  public void saveMA(HashMap hmParam) throws AppError {
    String strPhysicalAuditID = GmCommonClass.parseNull((String) hmParam.get("PHYSICALAUDITID"));

    String strLocationID = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));

    String strUserId = GmCommonClass.parseNull((String) hmParam.get("SUPERVISIORID"));
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS, getGmDataStoreVO());
    // log.debug("inputString......................."+inputString);
    gmDBManager.setPrepareString("gm_pkg_ac_physical_audit_info.gm_save_ma", 3);
    gmDBManager.setString(1, strPhysicalAuditID);
    gmDBManager.setString(2, strLocationID);
    gmDBManager.setString(3, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();

  }
}
