// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\beans\\GmPATagInfoBean.java

package com.globus.accounts.physicalaudit.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.LazyDynaBean;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPATagInfoBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.

  // this will be removed all place changed with Data Store VO constructor

  public GmPATagInfoBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPATagInfoBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /*
   * * Will call the procedure Gm_fch_tag_detail
   * 
   * @param strTagID
   * 
   * @return java.util.List
   * 
   * @roseuid 48CEBAB30149
   */
  public HashMap fetchTagDetail(String strTagID) throws AppError {
    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_patag_info.gm_fch_tag_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strTagID);
    gmDBManager.execute();
    HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("hmReturn: " + hmReturn);

    log.debug("Exit");
    return hmReturn;
  }

  public HashMap fetchPartDetail(String strPartNum, String strTxnID, String strType)
      throws AppError {
    log.debug("Enter" + strPartNum);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_patag_info.gm_fch_desc", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strTxnID);
    gmDBManager.setString(3, strType);
    gmDBManager.execute();
    HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    log.debug("hmReturn: " + hmReturn);
    log.debug("Exit");
    return hmReturn;
  }

  /**
   * @return ArrayList
   * @roseuid 48D018960360
   */
  public ArrayList fetchCountedBy(String strPhysicalAuditID) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_AC_PATAG_MASTER_INFO.GM_FCH_COUNTED_BY", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull(strPhysicalAuditID));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alResult;
  }


  /**
   * @return ArrayList
   * @roseuid 48D01C48019B
   */
  public ArrayList fetchLocations(String strPhysicalAuditID) throws Exception {
    log.debug("Enter");
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_AC_PATAG_MASTER_INFO.GM_FCH_AUDIT_LOCATIONS", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull(strPhysicalAuditID));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("Exit");
    return alResult;
  }

  /**
   * @return ArrayList
   * @roseuid 48D01C48019B
   */
  public ArrayList fetchLocationsForType(String strPhysicalAuditID, String strLocationID)
      throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_AC_PATAG_MASTER_INFO.GM_FCH_AUDIT_LOCATIONS_TYPE", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull(strPhysicalAuditID));
    gmDBManager.setString(2, GmCommonClass.parseNull(strLocationID));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return alResult;
  }

  /**
   * @return ArrayList
   * @roseuid 48D01EC800EC
   */
  public ArrayList fetchOpenPhysicalAudits() throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_AC_PATAG_MASTER_INFO.GM_FCH_OPEN_PHYSICAL_AUDITS", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alResult;
  }

  /**
   * New Method -- To Load Missing flagged parts Call --
   * Gm_pkg_ac_physical_audit_info.gm_fch_flagged_part
   * 
   * @return ArrayList
   * @roseuid 48D027340001
   */
  public HashMap fetchFlagPart(HashMap hmParam) throws AppError {

    RowSetDynaClass rdResult = null;
    RowSetDynaClass rdMAIDResult = null;
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strPhysicalAuditID = GmCommonClass.parseNull((String) hmParam.get("PHYSICALAUDITID"));
    String strLocationID = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));

    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_ac_physical_audit_info.gm_fch_flagged_part", 5);
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);

    gmDBManager.setString(1, strPhysicalAuditID);
    gmDBManager.setString(2, strLocationID);
    gmDBManager.execute();

    String strMAstatus = gmDBManager.getString(3);
    rdMAIDResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));

    gmDBManager.close();
    hmReturn.put("MAIDS", rdMAIDResult);
    hmReturn.put("FLAGPARTS", rdResult);
    hmReturn.put("MASTATUS", strMAstatus);
    return hmReturn;

  }

  /**
   * @return ArrayList
   * @roseuid 48D02B460134
   */
  public RowSetDynaClass fetchInventoryRpt(HashMap hmParam) throws AppError {
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("hmParam: " + hmParam);
    String strPhysicalAuditID = GmCommonClass.parseNull((String) hmParam.get("PHYSICALAUDITID"));
    String strLocationID = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));
    String strProjectID = GmCommonClass.parseNull((String) hmParam.get("PROJECTLISTID"));
    gmDBManager.setPrepareString("gm_pkg_ac_physical_audit_info.gm_fch_inventory_rpt", 4);
    gmDBManager.setString(1, GmCommonClass.parseNull(strPhysicalAuditID));
    gmDBManager.setString(2, GmCommonClass.parseNull(strLocationID));
    gmDBManager.setString(3, GmCommonClass.parseNull(strProjectID));
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
    log.debug("rdResult: " + rdResult.getRows());
    gmDBManager.close();
    return rdResult;
  }


  /**
   * @return ArrayList
   * @roseuid 48D02B460134
   */
  public ArrayList fetchInvSummaryRpt(String strPhysicalAuditID, String strDetailRpt)
      throws AppError {
    ArrayList alResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_physical_audit_info.gm_fch_inv_summary_rpt", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull(strPhysicalAuditID));
    gmDBManager.setString(2, GmCommonClass.parseNull(strDetailRpt));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    log.debug("rdResult: " + alResult);
    gmDBManager.close();
    return alResult;
  }

  /**
   * @return ArrayList
   * @roseuid 48D02B460134
   */
  public ArrayList fetchInvSummaryRpt(String strPhysicalAuditID) throws AppError {
    ArrayList alResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_physical_audit_info.gm_fch_inv_summary_rpt", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull(strPhysicalAuditID));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    log.debug("rdResult: " + alResult);
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchPartDrillDown - This method will drill down on the specific part
   * 
   * @return RowSetDynaClass
   * @exception AppError
   */
  public HashMap fetchLockTagInfo(HashMap hmParam) throws AppError {
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS, getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    String strPhysicalAuditID = GmCommonClass.parseNull((String) hmParam.get("PHYSICALAUDITID"));
    String strTxnID = GmCommonClass.parseNull((String) hmParam.get("TRANSACTIONID"));
    String strLocationID = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));
    String strProjectID = GmCommonClass.parseNull((String) hmParam.get("PROJECTLISTID"));
    String strByPart = GmCommonClass.parseNull((String) hmParam.get("BYPART"));

    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_ac_patag_info.gm_fch_lock_tag_info", 7);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);

    gmDBManager.setString(1, strPhysicalAuditID);
    gmDBManager.setString(2, strTxnID);
    gmDBManager.setString(3, strLocationID);
    gmDBManager.setString(4, strProjectID);
    gmDBManager.setString(5, GmCommonClass.getCheckBoxValue(strByPart));

    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(6));
    String strMAstatus = gmDBManager.getString(7);
    log.debug(" values from DB " + rdResult.getRows().size());
    gmDBManager.close();

    hmReturn.put("LOCKTAGINFO", rdResult);
    hmReturn.put("MASTATUS", strMAstatus);

    return hmReturn;
  }

  /**
   * fetchTags - This method will return an HashMap
   * 
   * @param HashMap hmParam
   * @return HashMap hmReturn
   * @exception AppError
   * 
   * @roseuid 48CAE5C102AD
   */

  public RowSetDynaClass fetchTags(HashMap hmParam) throws AppError {
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("Enter");
    log.debug("hmParam : " + hmParam);
    try {
      String strPhysicalAuditId = GmCommonClass.parseNull((String) hmParam.get("PHYSICALAUDITID"));
      String strLocationId = GmCommonClass.parseZero((String) hmParam.get("LOCATIONID"));
      String strPartNum = GmCommonClass.parseNull((String) hmParam.get("TRANSACTIONID"));
      // String strProjectId = GmCommonClass.parseNull((String) hmParam.get("PROJECTLISTID"));
      String strCountedBy = GmCommonClass.parseZero((String) hmParam.get("AUDITUSERID"));
      String strMissingFlag = GmCommonClass.parseNull((String) hmParam.get("MISSINGFLAG"));
      String strTagFilter = GmCommonClass.parseNull((String) hmParam.get("TAGFILTER")); // ,
      String strFrom = GmCommonClass.parseZero((String) hmParam.get("TAGRANGEFROM"));
      String strTo = GmCommonClass.parseZero((String) hmParam.get("TAGRANGETO"));
      String strFlgShowVoidRec = GmCommonClass.parseNull((String) hmParam.get("STRFLGSHOWVOIDREC"));
      strFlgShowVoidRec = (strFlgShowVoidRec.equals("on")) ? "Y" : "N";
      // log.debug("--strLocationId :" + strLocationId + "-- strCountedBy
      // : " + strCountedBy + " strProjectId: " + strProjectId);
      log.debug(" strTagFilter  " + strTagFilter);
      log.debug(" strFrom  " + strFrom);
      log.debug(" strTo  " + strTo);
      gmDBManager.setPrepareString("gm_pkg_ac_patag_info.gm_fch_tags", 10);

      gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
      gmDBManager.setString(1, strPhysicalAuditId);
      gmDBManager.setInt(2, Integer.parseInt(strLocationId));
      gmDBManager.setString(3, strPartNum);
      // gmDBManager.setString(4, strProjectId);
      gmDBManager.setInt(4, Integer.parseInt(strCountedBy));
      gmDBManager.setString(5, strMissingFlag);
      gmDBManager.setString(6, strTagFilter);
      gmDBManager.setInt(7, Integer.parseInt(strFrom));
      gmDBManager.setInt(8, Integer.parseInt(strTo));
      gmDBManager.setString(9, strFlgShowVoidRec);

      gmDBManager.execute();

      rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(10));
      log.debug(" rdResult.getRows() : " + rdResult.getRows().size());
      log.debug("results---------------------------------" + rdResult);
      gmDBManager.close();
    } catch (Exception exp) {
      exp.printStackTrace();
    }
    log.debug("Exit");
    return rdResult;

  } // End of fetchTags

  public RowSetDynaClass fetchLockedTransactions(HashMap hmParam) throws AppError {
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strPhysicalAuditID = GmCommonClass.parseNull((String) hmParam.get("PHYSICALAUDITID"));
    String strTxnID = GmCommonClass.parseNull((String) hmParam.get("TRANSACTIONID"));
    String strLocationID = GmCommonClass.parseZero((String) hmParam.get("LOCATIONID"));
    String strProjectID = GmCommonClass.parseNull((String) hmParam.get("PROJECTLISTID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRINGPNUM"));
    String strUntaggedTxn = GmCommonClass.parseNull((String) hmParam.get("UNTAGGEDTXN"));
    strUntaggedTxn = strUntaggedTxn.equals("on") ? "Y" : "";
    log.debug(" values in hmParam " + hmParam);
    log.debug(" strUntaggedTxn " + strUntaggedTxn);
    gmDBManager.setPrepareString("gm_pkg_ac_patag_info.gm_fch_locked_txn", 7);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);

    gmDBManager.setString(1, strPhysicalAuditID);
    gmDBManager.setInt(2, Integer.parseInt(strLocationID));
    gmDBManager.setString(3, strTxnID);
    gmDBManager.setString(4, strProjectID);
    gmDBManager.setString(5, strInputString);
    gmDBManager.setString(6, strUntaggedTxn);

    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(7));
    log.debug(" values from DB " + rdResult.getRows().size());
    gmDBManager.close();

    return rdResult;
  }

  /**
   * fetchExcelLockedTransactions - This method will return an ArrayList
   * 
   * @param ArrayListp alExcelData
   * @return ArrayList alResult
   * @exception AppError
   * 
   */
  public HashMap fetchExcelLockedTransactions(ArrayList alExcelData, String physicalAuditID)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hm1 = new HashMap();
    String strPartNum = "";
    String strPartLoc = "";
    String strConsingee = "";
    String strPartDesc = "";
    String strTagCount = "";
    String strInvLocation = "";
    String strType = "";
    String strPartLocNm = "";
    ArrayList alMissingPart = new ArrayList();
    DynaBean dynaBean = null;
    int intConsigneeIndex = 0;
    int intPartLocIndex = 0;
    if (alExcelData != null) {
      // alExcelData.size()-1 -- Excel Component is giving an extra "Empty" HashMap as last item. -1
      // is to exclude it
      for (int i = 0; i < alExcelData.size() - 1; i++) {

        dynaBean = new LazyDynaBean();
        hmReturn = (HashMap) alExcelData.get(i);

        strPartNum = (String) hmReturn.get("COLUMN0");
        strPartLoc = (String) hmReturn.get("COLUMN1");
        strConsingee = (String) hmReturn.get("COLUMN2");
        strTagCount = (String) hmReturn.get("COLUMN3");
        strInvLocation = (String) hmReturn.get("COLUMN4");

        intConsigneeIndex = strConsingee.lastIndexOf("-");
        intPartLocIndex = strPartLoc.lastIndexOf("-");
        if (intConsigneeIndex != -1)
          strConsingee = strConsingee.substring(intConsigneeIndex + 1, strConsingee.length());
        else
          strConsingee = "0";

        if (intPartLocIndex != -1)
          strPartLoc = strPartLoc.substring(intPartLocIndex + 1, strPartLoc.length());
        else
          strPartLoc = "0";

        gmDBManager.setFunctionString("gm_pkg_ac_patag_info.get_validate_excel_data", 5);

        gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
        gmDBManager.setString(2, physicalAuditID);
        gmDBManager.setString(3, strPartNum);
        gmDBManager.setString(4, strPartLoc);
        gmDBManager.setString(5, strConsingee);
        gmDBManager.setString(6, strInvLocation);
        gmDBManager.execute();
        strPartDesc = GmCommonClass.parseNull(gmDBManager.getString(1));
        gmDBManager.close();

        strType = fetchLocationType(physicalAuditID, strPartLoc);
        if (strType.equals("50660")) {
          strInvLocation = "";
        }
        if (!strPartDesc.equals("")) {
          dynaBean.set("PNUM", strPartNum);
          dynaBean.set("TXNDESC", strPartDesc);
          dynaBean.set("LOCATIONNM", strPartLoc);
          dynaBean.set("COUNTED_BY", strConsingee);
          dynaBean.set("TAGCOUNT", strTagCount);
          dynaBean.set("REFID", strInvLocation);
          alResult.add(dynaBean);
        } else {
          strPartLocNm = GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strPartLoc));
          hm1 = new HashMap();
          hm1.put("PART", strPartNum);
          hm1.put("WAREHOUSE", strPartLocNm);
          alMissingPart.add(hm1);
        }
      }
    }
    hmParam.put("ALRESULT", alResult);
    hmParam.put("ALMISSINGPART", alMissingPart);
    return hmParam;
  }

  public String fetchLocationType(String strPhysicalAuditID, String strLocationID) throws AppError {
    HashMap hmReturn = new HashMap();
    StringBuffer sbQuery = new StringBuffer();
    String strLocationType = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    sbQuery
        .append(" SELECT c901_type loctype FROM t2603_audit_location   WHERE c901_location_id ='");
    sbQuery.append(strLocationID + "' AND c2600_physical_audit_id ='");
    sbQuery.append(strPhysicalAuditID + "'");
    // log.debug(" Query to view Location type details " + sbQuery.toString());
    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
    strLocationType = GmCommonClass.parseNull((String) hmReturn.get("LOCTYPE"));
    return strLocationType;
  }

  public ArrayList fetchSupervisiorList() throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_AC_PATAG_MASTER_INFO.gm_fch_ma_supervisor", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alResult;
  }
}
