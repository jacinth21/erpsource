package com.globus.accounts.physicalaudit.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.GmTagDetailVO;
import com.globus.valueobject.common.GmTagInfoVO;
import com.globus.valueobject.prodmgmnt.GmPartNumberVO;
import com.globus.valueobject.prodmgmnt.GmSetAttributeVO;

public class GmTagBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize the Logger Class.

  // this will be removed all place changed with Data Store VO constructor

  public GmTagBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmTagBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // Initialize the Logger Class.

  /**
   * @return ArrayList
   * @roseuid
   */
  public RowSetDynaClass fetchTagHistory(String strTagID) throws AppError {
    RowSetDynaClass rdHistory = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_tag_info.gm_fch_tag_history", 2);
    log.debug("strTagID:" + strTagID);
    gmDBManager.setString(1, GmCommonClass.parseNull(strTagID));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdHistory = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return rdHistory;
  }

  /**
   * fetchTags - This method will return an RowSetDynaClass
   * 
   * @param HashMap * hmParam
   * @return HashMap hmReturn
   * @exception AppError
   * 
   * @roseuid 48CAE5C102AD
   */

  public HashMap fetchTagDetail(HashMap hmParam) throws AppError {
	  
	String strIntrasitFl = "";  
    RowSetDynaClass rdTaggedResult = null;
    RowSetDynaClass rdUnTaggedResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();

    String strPnum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strRefID = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strRefType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));

    log.debug(" values in hmParam '" + hmParam);
    log.debug(" values in strPnum '" + strPnum + "'");
    log.debug(" values in strConID '" + strRefID + "'");
    gmDBManager.setPrepareString("gm_pkg_ac_tag_info.gm_fc_tag_detail", 6);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
    
    gmDBManager.setString(1, strRefID);
    gmDBManager.setString(2, strPnum);
    gmDBManager.setString(3, strRefType);

    gmDBManager.execute();

    rdTaggedResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
    rdUnTaggedResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));
    strIntrasitFl = GmCommonClass.parseNull((String) gmDBManager.getString(6));
    
    log.debug(" values from DB " + rdUnTaggedResult.getRows().size());
    gmDBManager.close();
    hmReturn.put("TAGGEDRESULT", rdTaggedResult);
    hmReturn.put("UNTAGGEDRESULT", rdUnTaggedResult);
    hmReturn.put("INTRANSITFLAG", strIntrasitFl);
    return hmReturn;
  }

  public void saveTagDetail(HashMap hmParam) throws AppError {

    String inputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String strError = "";
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_ac_tag_info.gm_sav_tag_detail", 3);
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    log.debug(" values from inputString " + inputString);
    gmDBManager.setString(1, inputString);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    strError = GmCommonClass.parseNull(gmDBManager.getString(3));
    log.debug("strError = " + strError);
    if (!strError.equals("")) {
      gmDBManager.close();
      throw new AppError(strError, "", 'E');
    }
    gmDBManager.commit();
  }

  public void saveChangedLocations(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_tag_info.gm_sav_changed_locations", 2);
    String userId = (String) hmParam.get("USERID");
    String inputString = (String) hmParam.get("INPUTSTR");

    gmDBManager.setString(1, userId);
    gmDBManager.setString(2, inputString);
    log.debug(" values from inputString " + inputString);
    log.debug(" values from userId " + userId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * fetchTags - This method will return an RowSetDynaClass
   * 
   * @param HashMap * hmParam
   * @return HashMap hmReturn
   * @exception AppError
   * 
   * 
   */
  public RowSetDynaClass loadTagDetails(HashMap hmParam, String strFilterCondition) throws AppError {
    RowSetDynaClass rsResultSet = null;
    ArrayList alResult = null;
    HashMap hmNames = new HashMap();
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strSetID = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("TYPESTATUS"));
    String strLocationType = GmCommonClass.parseNull((String) hmParam.get("LOCATIONTYPE"));
    String strLocationName = GmCommonClass.parseNull((String) hmParam.get("NAME"));
    String strTagIDFrom = GmCommonClass.parseNull((String) hmParam.get("TAGIDFROM"));
    String strTagIDTo = GmCommonClass.parseNull((String) hmParam.get("TAGIDTO"));
    String strInventoryType = GmCommonClass.parseNull((String) hmParam.get("INVENTORYTYPE"));
    String strCompanyID = GmCommonClass.parseNull((String) hmParam.get("COMPANYNM"));
    String strPlantID = GmCommonClass.parseNull((String) hmParam.get("PALNTNM"));
    String strVoidTagsFl =
        GmCommonClass.parseNull((String) hmParam.get("VOIDEDTAGS")).equals("on") ? "Y" : "N";

    boolean blnFlag = false;
    log.debug("hmParam: " + hmParam);
    log.debug("strFilterCondition: " + strFilterCondition);

    hmNames = (HashMap) hmParam.get("HMNAMES");
    log.debug("strLocationName:" + strLocationName);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append("SELECT T5010.C5010_TAG_ID TAGNUM ");
    sbQuery.append(",T5010.C205_PART_NUMBER_ID PARTNUM");
    sbQuery.append(",t205.C205_PART_NUM_DESC PARTDESC");
    sbQuery.append(",T5010.C5010_CONTROL_NUMBER CONTROLNUM");
    sbQuery.append(",T5010.C5010_LAST_UPDATED_TRANS_ID REFID");
    sbQuery.append(", get_code_name (t5010.c901_inventory_type)INVENTORYTYPE ");
    sbQuery.append(",T5010.C207_SET_ID SETID");
    sbQuery
        .append(",DECODE (T5010.C901_LOCATION_TYPE , 4120, GET_DISTRIBUTOR_NAME (T5010.C5010_LOCATION_ID) ,4123, t101.C101_USER_F_NAME || ' ' || t101.C101_USER_L_NAME, get_code_name(T5010.C5010_LOCATION_ID)) LOCATION");
    sbQuery.append(", T5010.C901_LOCATION_TYPE  LOCTYPEID");
    sbQuery.append(" ,get_code_name(t5010.C901_STATUS) STATUS");
    sbQuery.append(" ,t5010.C901_STATUS STATUSID");
    sbQuery
        .append(",nvl(t101.C101_USER_F_NAME || ' ' || t101.C101_USER_L_NAME,trim(t101.C101_USER_F_NAME || ' ' || t101.C101_USER_L_NAME)) TAGGEDBY");
    sbQuery.append(",nvl(C5010_LAST_UPDATED_DATE,T5010.C5010_CREATED_DATE) CDATE");
    sbQuery
        .append(",T5010.C5010_VOID_FL VOIDFL, get_code_name (t5010.c901_sub_location_type) sublocation_type");
    sbQuery
        .append(", DECODE (T5010.C901_SUB_LOCATION_TYPE , 50221,  GET_DISTRIBUTOR_NAME (T5010.c5010_sub_location_id) , 50222, t703.C703_SALES_REP_NAME) sublocation");
    sbQuery
        .append(", t704.c704_account_nm  account , t1900.C1900_COMPANY_CD COMPANYID, t5040.C5040_PLANT_NAME PLANTID, t5010.C5010_MISSING_SINCE MISSINGSINCE");
    sbQuery
        .append(" FROM T5010_TAG T5010 , t1900_company t1900 , t5040_plant_master t5040 , t703_sales_rep t703 , t704_account t704 , T101_USER t101  , t205_part_number T205 ");

    // sbQuery.append(" AND T5010.C5010_TAG_ID = '");
    // sbQuery.append(" WHERE T5010.C5010_TAG_ID = '");
    // sbQuery.append("0001038");
    // sbQuery.append("'");
    if (strOpt.equals("reload")) {
      sbQuery.append(" WHERE ");
      if (!strSetID.equals("")) {
        blnFlag = true;
        sbQuery.append(" T5010.C207_SET_ID ='");
        sbQuery.append(strSetID);
        sbQuery.append("' ");

      }
      if (!strPartNum.equals("")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        
      sbQuery.append(" REGEXP_LIKE(T5010.C205_PART_NUMBER_ID,REGEXP_REPLACE('");
      sbQuery.append(strPartNum);
      sbQuery.append("','[+]','\\+'))");


      }
      if (!strStatus.equals("")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        strStatus = strStatus.replaceAll(",", "','");
        sbQuery.append(" T5010.C901_STATUS IN ('");
        sbQuery.append(strStatus);
        sbQuery.append("')");
      }
      if (!strLocationType.equals("0")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.C901_LOCATION_TYPE ='");
        sbQuery.append(strLocationType);
        sbQuery.append("' ");
      }
      if (!strTagIDFrom.equals("") && !strTagIDTo.equals("")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        // Currently Tags are available with minimum length as 7 digits , we added 0 as prefix  
        //(eg: tag id is 1 , In db as 0000001). If we increase lpad value only on right side, it will not fetch 7 digit tags.
        // To handle this scenario , added lpad condition on left side.  
        sbQuery.append(" DECODE(LENGTH(Trim(TRANSLATE(T5010.C5010_Tag_Id , '.0123456789',' '))),'',Lpad(T5010.C5010_Tag_Id ,40,'0'),T5010.C5010_Tag_Id )  BETWEEN ");
        sbQuery.append(" DECODE(LENGTH(TRIM(TRANSLATE('" + strTagIDFrom
            + "', '.0123456789',' '))),'',LPAD('" + strTagIDFrom + "',40,'0'),'" + strTagIDFrom
            + "')  ");
        // sbQuery.append("' ");
        sbQuery.append(" AND ");
        sbQuery.append(" DECODE(LENGTH(TRIM(TRANSLATE('" + strTagIDTo
            + "', '.0123456789',' '))),'',LPAD('" + strTagIDTo + "',40,'0'),'" + strTagIDTo + "') ");
        // sbQuery.append("AND ");
      } else if (!strTagIDFrom.equals("")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" DECODE(LENGTH(Trim(TRANSLATE(T5010.C5010_Tag_Id , '.0123456789',' '))),'',Lpad(T5010.C5010_Tag_Id ,40,'0'),T5010.C5010_Tag_Id )  = ");
        sbQuery.append(" DECODE(LENGTH(TRIM(TRANSLATE('" + strTagIDFrom
            + "', '.0123456789',' '))),'',LPAD('" + strTagIDFrom + "',40,'0'),'" + strTagIDFrom
            + "')  ");

      }
      if (!strLocationName.equals("0")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.C5010_LOCATION_ID ='");
        sbQuery.append(strLocationName);
        sbQuery.append("' ");

      }
    }
    if (!strFilterCondition.equals("")) {
      if (blnFlag) {
        sbQuery.append(" AND ");
      } else {
        blnFlag = true;
      }
      sbQuery.append(" T5010.C5010_TAG_ID IN (");
      sbQuery.append(" SELECT T5010.C5010_TAG_ID ");
      sbQuery.append(" FROM T5010_TAG T5010, T703_SALES_REP T501 ");
      sbQuery.append(" WHERE T5010.C901_LOCATION_TYPE = 4120 ");
      sbQuery.append(" AND t501.C701_DISTRIBUTOR_ID = t5010.C5010_LOCATION_ID ");
      sbQuery.append(" AND t5010.C901_TRANS_TYPE = 51000 AND ");
      sbQuery.append(strFilterCondition);
      sbQuery.append(" ) ");
    }
    // Voided tags
    if (strVoidTagsFl.equals("Y")) {
      if (blnFlag) {
        sbQuery.append(" AND ");
      } else {
        blnFlag = true;
      }
      sbQuery.append("  T5010.C5010_VOID_FL IS NOT NULL ");
      sbQuery.append("  AND t5010.c1900_company_id = t1900.c1900_company_id(+)");
      sbQuery.append("  AND t5010.c5040_plant_id = t5040.c5040_plant_id(+) ");
      sbQuery.append("  AND t5010.c5010_sub_location_id = t703.c703_sales_rep_id(+) ");
      sbQuery.append("  AND t5010.c704_account_id = t704.c704_account_id(+) ");
      sbQuery.append("  AND t5010.c205_part_number_id = t205.c205_part_number_id(+) ");
      sbQuery
          .append("  AND NVL(t5010.c5010_last_updated_by , t5010.c5010_created_by) = t101.c101_user_id(+) ");
      sbQuery.append("  AND t1900.c1900_void_fl(+) IS NULL AND t5040.c5040_void_fl(+) IS NULL ");
      sbQuery.append("  AND t703.c703_void_fl(+) IS NULL  AND t704.c704_void_fl(+) IS NULL  ");

    } else {
      if (blnFlag) {
        sbQuery.append(" AND ");
      } else {
        blnFlag = true;
      }
      sbQuery.append("  T5010.C5010_VOID_FL IS NULL ");
      sbQuery.append("  AND t5010.c1900_company_id = t1900.c1900_company_id(+) ");
      sbQuery.append("  AND t5010.c5040_plant_id = t5040.c5040_plant_id(+) ");
      sbQuery.append("  AND t5010.c5010_sub_location_id = t703.c703_sales_rep_id(+) ");
      sbQuery.append("  AND t5010.c704_account_id = t704.c704_account_id(+) ");
      sbQuery.append("  AND t5010.c205_part_number_id = t205.c205_part_number_id(+) ");
      sbQuery
          .append("  AND NVL(t5010.c5010_last_updated_by , t5010.c5010_created_by) = t101.c101_user_id(+) ");
      sbQuery.append("  AND t1900.c1900_void_fl(+) IS NULL AND t5040.c5040_void_fl(+) IS NULL ");
      sbQuery.append("  AND t703.c703_void_fl(+) IS NULL  AND t704.c704_void_fl(+) IS NULL  ");
    }
    // This filter condition inventory type selected only.
    if (!strInventoryType.equals("0")) {
      if (blnFlag) {
        sbQuery.append(" AND ");
      } else {
        blnFlag = true;
      }
      sbQuery.append(" t5010.c901_inventory_type =  ");
      sbQuery.append(strInventoryType);

    }

    if (!strCompanyID.equals("0") && !strPlantID.equals("0")) {
      sbQuery.append(" AND t5010.c1900_company_id(+) = '");
      sbQuery.append(strCompanyID);
      sbQuery.append("' AND t5010.c5040_plant_id(+) =  '");
      sbQuery.append(strPlantID);
      sbQuery.append("'");
    } else if (!strCompanyID.equals("0")) {
      sbQuery.append(" AND t5010.c1900_company_id(+) = '");
      sbQuery.append(strCompanyID);
      sbQuery.append("'");
    } else if (!strPlantID.equals("0")) {
      sbQuery.append(" AND t5010.c5040_plant_id(+) = '");
      sbQuery.append(strPlantID);
      sbQuery.append("'");
    }
    sbQuery.append(" ORDER BY T5010.C5010_TAG_ID ");

    log.debug(" TAG REPORT query: " + sbQuery.toString());
    rsResultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    return rsResultSet;
  } // End of loadTagDetails

  public ArrayList loadTagCurrentLocationDetails(HashMap hmParam) throws AppError {
    RowSetDynaClass rsResultSet = null;

    ArrayList alResult = null;

    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strSetID = GmCommonClass.parseZero((String) hmParam.get("SETID"));
    String strAccID = GmCommonClass.parseZero((String) hmParam.get("ACCID"));
    String strDistributorId = GmCommonClass.parseZero((String) hmParam.get("DISTRIBUTORID"));
    String strTagIDFrom = GmCommonClass.parseNull((String) hmParam.get("TAGIDFROM"));
    String strTagIDTo = GmCommonClass.parseNull((String) hmParam.get("TAGIDTO"));

    String strAccountID = GmCommonClass.parseZero((String) hmParam.get("ACCOUNTID"));
    String strSalesRepId = GmCommonClass.parseZero((String) hmParam.get("SALESREPID"));
    String strCurrentLoc = GmCommonClass.parseZero((String) hmParam.get("CURRENTLOC"));
    String strLockInv = GmCommonClass.parseZero((String) hmParam.get("LOCKINV"));
    String strDecommission = GmCommonClass.parseZero((String) hmParam.get("DECOMMISSION"));
    String strZone = GmCommonClass.parseZero((String) hmParam.get("ZONE"));
    String strRegion = GmCommonClass.parseZero((String) hmParam.get("REGION"));
    String strSystem = GmCommonClass.parseZero((String) hmParam.get("SYSTEM"));
    String strFilterCondition = GmCommonClass.parseNull((String) hmParam.get("ACCESSFILTER"));
    String strDivisonID = GmCommonClass.parseZero((String) hmParam.get("DIVISON"));

    boolean blnFlag = true;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" select tag.* from ");
    sbQuery
        .append(" (SELECT t5010.C5010_TAG_ID TAG_ID,nvl(t5010.c207_set_id,t5010.C205_PART_NUMBER_ID) SET_ID, nvl(t207.c207_set_nm, GET_PARTNUM_DESC(t5010.C205_PART_NUMBER_ID)) SET_DESC,t701.C701_DISTRIBUTOR_NAME OWNER ");
    sbQuery.append(" ,NVL(t701.c701_distributor_id,'-9999')  distributor_id ");
    sbQuery
        .append(" , t5010.C901_SUB_LOCATION_TYPE SUB_LOCATION_TYPE, DECODE (t5010.c901_sub_location_type, 50221, 'D'|| ");
    sbQuery
        .append(" t5010.C5010_SUB_LOCATION_ID, 50222, 'S'||t5010.C5010_SUB_LOCATION_ID, 11301, 'A'||t5010.C5010_SUB_LOCATION_ID) ");
    sbQuery.append(" SUB_LOCATION_ID,  ");
    sbQuery
        .append(" DECODE (t5010.c901_sub_location_type, 50221, get_distributor_name(t5010.C5010_SUB_LOCATION_ID), 50222, get_user_name(t5010.C5010_SUB_LOCATION_ID)) sub_location_nm ");
    sbQuery
        .append(" , get_image_count (t5010.C5010_TAG_ID) IMGCNT, GET_CODE_NAME (t5010.C901_SUB_LOCATION_TYPE) ");
    sbQuery
        .append(" CUR_LOCATION, NVL (v5010a.c5010_lockset, 'N') LOCK_INV_FL, NVL (v5010a.c5010_decommissioned, 'N') DECOMSIN_FL ");
    sbQuery
        .append(" , t5010.c703_sales_rep_id REP_ID, get_rep_name (t5010.c703_sales_rep_id) REP_NAME, v5010.name ADDR_NM ");
    sbQuery.append(" , v5010.address1 ADDRESS1, v5010.address2 ADDRESS2, v5010.city CITY ");
    sbQuery.append(" , v5010.state STATE, v5010.zipcode ZIPCODE, v5010.phonenumber PHONENUMBER ");
    sbQuery
        .append(" , v5010.emailid EMAILID, t5010.C704_ACCOUNT_ID ACCOUNT_ID, get_account_name (t5010.c704_account_id) account_nm ");
    sbQuery.append(" , t5010.C5010_SUB_LOCATION_COMMENTS COMMENTS ");
    sbQuery.append(" FROM t5010_tag t5010, v5010a_tag_attribute v5010a, ( ");
    sbQuery.append(" SELECT tagid, name, address1 ");
    sbQuery.append(" , address2, city, ");
    sbQuery.append(" t901.c901_code_nm state, zipcode, phonenumber, emailid ");
    sbQuery.append(" FROM v5010_curr_address v5010, t901_code_lookup t901 ");
    sbQuery.append(" where v5010.state = t901.c901_code_id ");
    sbQuery.append(" ) ");
    sbQuery.append(" v5010,T207_SET_MASTER t207,T701_DISTRIBUTOR t701 ");
    sbQuery.append(" WHERE t5010.C901_TRANS_TYPE    = 51000 ");
    sbQuery.append(" AND t5010.c5010_tag_id       = v5010.tagid (+) ");
    sbQuery.append(" AND t5010.c5010_tag_id       = v5010a.c5010_tag_id (+) ");
    sbQuery.append(" AND t5010.C901_LOCATION_TYPE = 4120     ");
    sbQuery.append(" AND t5010.C5010_VOID_FL     IS NULL ");
    sbQuery.append(" AND t5010.C207_SET_ID = t207.C207_SET_ID(+)   ");
    sbQuery.append(" AND t5010.C5010_LOCATION_ID = t701.C701_DISTRIBUTOR_ID(+)  ");

    if (strOpt.equals("Report") || strOpt.equals("save") || strOpt.equals("Reload")) {
      if (!strSetID.equals("0")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.C207_SET_ID ='");
        sbQuery.append(strSetID);
        sbQuery.append("' ");

      }

      if (!strDistributorId.equals("0")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.C5010_LOCATION_ID ='");
        sbQuery.append(strDistributorId);
        sbQuery.append("' ");

      }
      //
      if (!strSalesRepId.equals("0")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.c703_sales_rep_id ='");
        sbQuery.append(strSalesRepId);
        sbQuery.append("' ");

      }

      if (!strFilterCondition.equals("")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.c703_sales_rep_id IN (");
        sbQuery.append(" SELECT	DISTINCT REP_ID FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
        sbQuery.append(" WHERE  REP_ID IS NOT NULL  AND ");
        sbQuery.append(strFilterCondition);
        sbQuery.append(")");

      }

      if (!strCurrentLoc.equals("0")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" t5010.c901_sub_location_type ='");
        sbQuery.append(strCurrentLoc);
        sbQuery.append("' ");

      }
      if (!strTagIDFrom.equals("") && !strTagIDTo.equals("")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.C5010_TAG_ID BETWEEN ");
        sbQuery.append(" DECODE(LENGTH(TRIM(TRANSLATE('" + strTagIDFrom
            + "', '.0123456789',' '))),'',LPAD('" + strTagIDFrom + "',7,'0'),'" + strTagIDFrom
            + "')  ");
        sbQuery.append(" AND ");
        sbQuery.append(" DECODE(LENGTH(TRIM(TRANSLATE('" + strTagIDTo
            + "', '.0123456789',' '))),'',LPAD('" + strTagIDTo + "',7,'0'),'" + strTagIDTo + "') ");

      }
      if (!strAccountID.equals("0")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.C5010_SUB_LOCATION_ID ='");
        sbQuery.append(strAccountID);
        sbQuery.append("' ");

      }

      if (!strAccID.equals("0")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.C704_ACCOUNT_ID ='");
        sbQuery.append(strAccID);
        sbQuery.append("' ");

      }

      if (!strLockInv.equals("0")) // Locked inventory
      {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        if (strLockInv.equals("11311")) {
          sbQuery.append(" v5010a.c5010_lockset = 'Y' ");
        } else {
          sbQuery.append(" NVL(v5010a.c5010_lockset,'N') = 'N' ");
        }

      }

      if (!strDecommission.equals("0")) // Decommission set
      {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        if (strDecommission.equals("11320")) {
          sbQuery.append(" v5010a.c5010_decommissioned = 'Y' ");
        } else {
          sbQuery.append(" NVL(v5010a.c5010_decommissioned,'N') = 'N' ");
        }
      }
      if (!strZone.equals("0")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.C5010_LOCATION_ID  IN ( ");
        sbQuery.append(" SELECT c701_distributor_id FROM t701_distributor WHERE c701_region IN ");
        sbQuery
            .append(" (SELECT c901_region_id FROM t708_region_asd_mapping WHERE C708_DELETE_FL IS NULL AND c101_user_id = ");
        sbQuery.append(strZone);
        sbQuery.append(" )) ");
      }

      if (!strRegion.equals("0")) {
        if (blnFlag) {
          sbQuery.append(" AND ");
        } else {
          blnFlag = true;
        }
        sbQuery.append(" T5010.C5010_LOCATION_ID  IN ( ");
        sbQuery.append(" SELECT c701_distributor_id FROM t701_distributor WHERE c701_region = ");
        sbQuery.append(strRegion);
        sbQuery.append(" ) ");
      }
    }
    sbQuery.append(" )tag, ");
    sbQuery.append(" (SELECT DISTINCT v700.d_id d_id FROM  v700_territory_mapping_detail v700 ");
    if (!strDivisonID.equals("0")) {
      sbQuery.append(" WHERE v700.divid= ");
      sbQuery.append(strDivisonID);
    }
    sbQuery.append(" UNION      SELECT '-9999' FROM DUAL ");
    sbQuery.append(" ) v700 ");
    sbQuery.append(" where tag.distributor_id = v700.d_id ");

    if (strOpt.equals("Reload")) {
      sbQuery.append(" ORDER BY SET_DESC ");
    } else {
      sbQuery.append(" ORDER BY tag.TAG_ID ");
    }
    log.debug(" TAG REPORT query: " + sbQuery.toString());

    gmDBManager.setPrepareString(sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecord();
    gmDBManager.close();

    return alResult;
  } // End of loadTagDetails

  public String getXmlGridData(ArrayList alParam, HashMap hmNames) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmNames.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.physicalaudit.GmMoveTagLocation", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("accounts/physicalaudit/templates");
    templateUtil.setTemplateName("GmMoveTagLocation.vm");
    if (hmNames != null) {
      templateUtil.setDropDownMaster("alDistributor", (ArrayList) hmNames.get("DISTLIST"));
      templateUtil.setDropDownMaster("alAccList", (ArrayList) hmNames.get("ACCLIST"));
      templateUtil.setDropDownMaster("alSet", (ArrayList) hmNames.get("SETLIST"));
      templateUtil.setDropDownMaster("alRepList", (ArrayList) hmNames.get("REPLIST"));
      templateUtil.setDropDownMaster("alType", GmCommonClass.getCodeList("MVTGL"));
    }
    return templateUtil.generateOutput();
  }

  /*
   * Method(New):- ArrayList fetchTagTransferDetail(String ) throws AppError (Pseudo Code) Use
   * connection class gmDBManager to fetch or save the values. Calls GM_PKG_AC_TAG_INFO.
   * gm_fch_tf_tag)
   */
  public HashMap fetchEditTagDetail(String strTagId) throws AppError {

    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_tag_info.gm_fch_edit_tag", 2);
    log.debug("strTagID:" + strTagId);
    gmDBManager.setString(1, GmCommonClass.parseNull(strTagId));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;

  }

  public String saveEditTag(HashMap hmParam) throws AppError,Exception {

    String strTagID = GmCommonClass.parseNull((String) hmParam.get("HTAGID"));
    String strPartnum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strConsignedTo = GmCommonClass.parseZero((String) hmParam.get("CONSIGNEDTO"));
    String strSetID = GmCommonClass.parseZero((String) hmParam.get("SETID"));
    String strControlNum = GmCommonClass.parseNull((String) hmParam.get("CONTROLNUM"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strRefID = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strLocType = GmCommonClass.parseZero((String) hmParam.get("LOCATIONTYPE"));
    String strInvType = GmCommonClass.parseZero((String) hmParam.get("INVENTORYTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strComments=GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strMissingDate = GmCommonClass.parseNull((String) hmParam.get("MISSINGDATE"));
    String strOldLocationId = GmCommonClass.parseNull((String) hmParam.get("OLDLOCATIONID"));
    String strDateFormat = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());
    Date strMissDate = GmCommonClass.getStringToDate(strMissingDate,strDateFormat);
    log.debug("hashmap values   " + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_tag_info.gm_sav_edit_tag", 13);
    // gmDBManager.registerOutParameter(9, java.sql.Types.CHAR);
    gmDBManager.setString(1, strTagID);
    gmDBManager.setString(2, strPartnum);
    gmDBManager.setString(3, strConsignedTo);
    gmDBManager.setString(4, strSetID);
    gmDBManager.setString(5, strControlNum);
    gmDBManager.setString(6, strStatus);
    gmDBManager.setString(7, strRefID);
    gmDBManager.setString(8, strLocType);
    gmDBManager.setString(9, strInvType);
    gmDBManager.setString(10, strUserId);
    gmDBManager.setString(11, strComments);
    gmDBManager.setDate(12, strMissDate);
    gmDBManager.registerOutParameter(13, java.sql.Types.CHAR);
    gmDBManager.execute();
    String strSuccessMsg = GmCommonClass.parseNull(gmDBManager.getString(13));
    gmDBManager.commit();
    if(!strSuccessMsg.equals("") && strStatus.equals("26240614")){      //Status as 26240614-Missing
        // //To Update redis key to show add set field (PMT-35098)
       GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
       gmKitMappingBean.removeSetKey(strOldLocationId);
    }
    return strSuccessMsg;
    // strException = GmCommonClass.parseNull(gmDBManager.getString(9));

  }

  /**
   * saveTagStatus - This method will save all Tags based on Range & status.
   * 
   * @param HashMap * hmParam
   * @exception AppError
   * 
   **/
  /*
   * Method(New):- ArrayList fetchTagTransferDetail(String ) throws AppError (Pseudo Code) Use
   * connection class gmDBManager to fetch or save the values. Calls GM_PKG_AC_TAG_INFO.
   * gm_fch_tf_tag)
   */
  public void saveTagStatus(HashMap hmParam) throws AppError {
    log.debug("========hmParams=========" + hmParam);
    String strTagIDFrom = GmCommonClass.parseNull((String) hmParam.get("TAGIDFROM"));
    String strTagIDTo = GmCommonClass.parseNull((String) hmParam.get("TAGIDTO"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("TYPESTATUS"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_tag_info.gm_sav_tag_status", 4);
    gmDBManager.setString(1, strTagIDFrom);
    gmDBManager.setString(2, strTagIDTo);
    gmDBManager.setString(3, strStatus);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * fetchSetInvReport will be used to load the Sets Inventory Report based on Sales Hierarchy.
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchSetInvReport(HashMap hmParam) throws AppError {
    log.debug("========hmParams=========" + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();

    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strTypeId = GmCommonClass.parseNull((String) hmParam.get("TYPEID"));
    String strSystem = GmCommonClass.parseNull((String) hmParam.get("SYSTEM"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTRIBUTORID"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT SETINV.* ,  T207.C207_SET_NM SETNM ,  T701.C701_DISTRIBUTOR_NAME DISTNM ,");
    sbQuery
        .append(" T703.C703_SALES_REP_NAME REPNM ,t703.c703_phone_number REPPHONE,  t704.C704_ACCOUNT_nm ACCTNM");
    sbQuery.append(" FROM  (SELECT T5010.C207_SET_ID SETID, T5010.C5010_LOCATION_ID DISTID,");
    sbQuery.append(" T5010.C703_SALES_REP_ID REPID ,T5010.C704_ACCOUNT_ID ACCTID, 4070 TYPEID,");
    sbQuery
        .append(" get_code_name('4070') TYPENM, T5010.C5010_TAG_ID TAGID,'' ETCH_ID,'' STATUS ,");
    sbQuery.append(" ''LOANED_ON, '' EXRETNDT  FROM T5010_TAG T5010");
    sbQuery.append(" WHERE t5010.C901_TRANS_TYPE  = 51000");
    sbQuery.append(" AND T5010.C901_LOCATION_TYPE = 4120");
    sbQuery.append(" AND T5010.C901_INVENTORY_TYPE NOT IN (10004)"); // Product
    // Loaner
    sbQuery.append(" AND T5010.C5010_VOID_FL     IS NULL");
    sbQuery.append(" UNION ALL ");
    sbQuery.append(" SELECT T504.C207_SET_ID SETID,T504.C701_DISTRIBUTOR_ID DISTID,");
    sbQuery.append(" T504B.C703_SALES_REP_ID REPID ,T504B.C704_ACCOUNT_ID ACCTID,4074 TYPEID,");
    sbQuery
        .append(" get_code_name(4074) TYPENM,T5010.C5010_TAG_ID TAGID,T504A.C504A_ETCH_ID ETCH_ID,");
    sbQuery
        .append(" GM_PKG_OP_LOANER.GET_LOANER_STATUS (T504A.C504A_STATUS_FL) STATUS , TO_CHAR(T504A.C504A_LOANER_DT,'MM/DD/YYYY') LOANED_ON,");
    sbQuery.append(" TO_CHAR(T504A.C504A_EXPECTED_RETURN_DT,'MM/DD/YYYY') EXRETNDT");
    sbQuery
        .append(" FROM T504_CONSIGNMENT T504,T504A_CONSIGNMENT_LOANER T504A,T504A_LOANER_TRANSACTION T504B ,");
    sbQuery.append(" T526_PRODUCT_REQUEST_DETAIL T526,T907_SHIPPING_INFO T907,  T5010_TAG t5010 ");
    sbQuery.append(" WHERE T504.C504_VOID_FL                IS NULL");
    sbQuery.append(" AND t504.c504_status_fl                 = '4'");
    sbQuery.append(" AND t504.c504_verify_fl                 = '1'");
    sbQuery.append(" AND t504.c504_type                      = 4127");
    sbQuery.append(" AND t504.c504_consignment_id            = t504a.c504_consignment_id");
    sbQuery.append(" AND t504.c504_consignment_id            = t504b.c504_consignment_id");
    sbQuery
        .append(" AND t526.C526_PRODUCT_REQUEST_DETAIL_ID = t504b.C526_PRODUCT_REQUEST_DETAIL_ID");
    sbQuery.append(" AND t526.C526_VOID_FL                  IS NULL");
    sbQuery.append(" AND t504b.C504A_RETURN_DT              IS NULL");
    sbQuery.append(" AND t504b.c504a_void_fl                IS NULL");
    sbQuery.append(" AND t504a.c504a_void_fl                IS NULL");
    sbQuery.append(" AND T907.C907_REF_ID                    = t504.c504_consignment_id");
    sbQuery.append(" AND T526.C525_Product_Request_Id        = T907.C525_Product_Request_Id");
    sbQuery.append(" AND T907.C907_STATUS_FL                 = '40'");
    sbQuery.append(" AND T907.C907_VOID_FL                  IS NULL");
    sbQuery.append(" AND T907.C907_Shipped_Dt               IS NOT NULL");
    sbQuery.append(" AND TRUNC (T907.C907_SHIPPED_DT)       <= TRUNC (SYSDATE)");
    sbQuery.append(" AND T504A.C504A_STATUS_FL              IN ('20', '5', '10')");
    sbQuery.append(" AND t504.c504_consignment_id = t5010.c5010_last_updated_trans_id(+)");
    sbQuery.append(" AND T504.C207_SET_ID = T5010.C207_SET_ID (+) ");
    sbQuery.append(" AND T5010.C5010_VOID_FL(+)     IS NULL");
    sbQuery
        .append(" ) SETINV ,  T207_SET_MASTER T207,  T701_DISTRIBUTOR T701,  T703_SALES_REP T703,  T704_ACCOUNT T704");

    if (!strCondition.equals("")) {
      sbQuery.append(",( SELECT DISTINCT AD_ID, AD_NAME, REGION_ID, REGION_NAME, D_ID, D_NAME ");
      sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700 WHERE ");
      sbQuery.append(strCondition);
      sbQuery.append(" )V700");
    }

    sbQuery.append(" WHERE SETINV.SETID    = T207.C207_SET_ID(+)");
    if (!strCondition.equals("")) {
      sbQuery.append(" AND SETINV.DISTID = V700.D_ID ");
    }
    sbQuery.append(" AND SETINV.DISTID   = T701.C701_DISTRIBUTOR_ID(+)");
    sbQuery.append(" AND SETINV.ACCTID   = T704.C704_ACCOUNT_ID(+)");
    sbQuery.append(" AND SETINV.REPID    = T703.C703_SALES_REP_ID(+)");

    if (!strSetId.equals("") && !strSetId.equals("0")) {
      sbQuery.append(" AND SETINV.SETID = '" + strSetId + "'");
    }

    sbQuery.append(" AND SETINV.SETID  IN ( SELECT C207_SET_ID FROM T207_SET_MASTER WHERE");
    sbQuery.append(" C901_SET_GRP_TYPE           = 1601");
    if (!strSystem.equals("") && !strSystem.equals("0")) {
      sbQuery.append(" AND C207_SET_SALES_SYSTEM_ID IN (");
      sbQuery.append(strSystem);
      sbQuery.append(")");
    }
    sbQuery.append(")");

    if (!strDistId.equals("") && !strDistId.equals("0")) {
      sbQuery.append(" AND SETINV.DISTID ='" + strDistId + "'");
    }
    if (!strTypeId.equals("") && !strTypeId.equals("0")) {
      sbQuery.append(" AND SETINV.TYPEID= '" + strTypeId + "'");
    }
    sbQuery.append(" ORDER BY SETNM, typenm, distnm,repnm ");

    log.debug("fetchSetInvReport Query ::" + sbQuery.toString());
    gmDBManager.setPrepareString(sbQuery.toString());
    alResult = gmDBManager.queryMultipleRecord();
    gmDBManager.close();

    return alResult;
  } // End of fetchSetInvReport

  /**
   * validateTagId is used to validate the tag id 
   * 
   * @param String
   * @return integer
   * @exception AppError
   */
  public int validateTagId(String strTagId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_ac_tag_info.gm_validate_tag_id", 2);
    gmDBManager.setString(1, strTagId);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.execute();

    int intTagCount = gmDBManager.getInt(2);
    gmDBManager.close();
    return intTagCount;
  }
  
  /**
   * fetchTagDetailsSync is used to fetch the tag details for device tag sync
   * 
   * @param String
   * @return String
   * @exception AppError
   */
  public String fetchTagDetailsSync(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    List<GmTagDetailVO> listGmTagInfoVO;
    GmTagDetailVO gmTagInfoVO = (GmTagDetailVO) hmParams.get("VO");
    String strReturn = "";
    log.debug("fetchTagDetailsSync hmParams" + hmParams);
    gmDBManager.setPrepareString("gm_pkg_device_tag_sync.gm_fch_tag_details", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
    gmDBManager.setString(1,(String) hmParams.get("TOKEN"));
    gmDBManager.setString(2,(String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3,(String) hmParams.get("PAGENO"));
    gmDBManager.setString(4,(String) hmParams.get("UUID"));
    gmDBManager.execute();
    strReturn = GmCommonClass.parseNull(gmDBManager.getString(5));
    log.debug("fetchTagDetailsSync strReturn" + strReturn);
    gmDBManager.close();
    
    return strReturn;
  }

}
