
package com.globus.accounts.physicalaudit.forms;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCancelForm;
 
public class GmTagReportForm extends GmCancelForm {
	
	private String locationNames = "";
	private String typeStatus = "";
	private String tagIdFrom = "";
	private String tagIdTo = "";
	private String locationType = "";
	private String partNum = "";
	private String setID = "";
	private String strOpt = "";
	private String name = "";
	private String locationId="";
	private String distributorId = "";
	private String accId = "";
	private String tagId = ""; 
	private boolean transferAccess ;
	private boolean voidAccess ;
	private String companyNm = "";
	private String palntNm = "";
	private String xmlGridData  = "";
	
	
	private RowSetDynaClass rdReport= null; 
	private ArrayList alStatus = new ArrayList();
	private ArrayList alLocationType = new ArrayList();
	private ArrayList alLocationNames = new ArrayList();
	private HashMap hmNames = new HashMap ();
	
	private ArrayList alDistributor = new ArrayList();
	private ArrayList alAccList = new ArrayList();
	private ArrayList alSet = new ArrayList();
	private ArrayList alRepList = new ArrayList();
	private ArrayList alInventoryType = new ArrayList();
	private ArrayList alCompanyList = new ArrayList();
	private ArrayList alPlantList = new ArrayList();
	
	private String EMPTY_STRING = "";
	private String gridXmlData = EMPTY_STRING;
	private String locTypeId = "";
	private String statusId = "";	
	private String inventoryType = "";
	private String voidedTags = "";
	

	/**
	 * @return the voidedTags
	 */
	public String getVoidedTags() {
		return voidedTags;
	}

	/**
	 * @param voidedTags the voidedTags to set
	 */
	public void setVoidedTags(String voidedTags) {
		this.voidedTags = voidedTags;
	}

	/**
	 * @return the alInventoryType
	 */
	public ArrayList getAlInventoryType() {
		return alInventoryType;
	}

	/**
	 * @param alInventoryType the alInventoryType to set
	 */
	public void setAlInventoryType(ArrayList alInventoryType) {
		this.alInventoryType = alInventoryType;
	}

	/**
	 * @return the inventoryType
	 */
	public String getInventoryType() {
		return inventoryType;
	}

	/**
	 * @param inventoryType the inventoryType to set
	 */
	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}

	/**
	 * @return the statusId
	 */
	public String getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getGridXmlData() {
		return gridXmlData;
	}

	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	/**
	 * @return the locTypeId
	 */
	public String getLocTypeId() {
		return locTypeId;
	}

	/**
	 * @param locTypeId the locTypeId to set
	 */
	public void setLocTypeId(String locTypeId) {
		this.locTypeId = locTypeId;
	}

	/**
	 * @return the tagId
	 */
	public String getTagId() {
		return tagId;
	}

	/**
	 * @param tagId the tagId to set
	 */
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	/**
	 * @return the alDistributor
	 */
	public ArrayList getAlDistributor() {
		return alDistributor;
	}

	/**
	 * @param alDistributor the alDistributor to set
	 */
	public void setAlDistributor(ArrayList alDistributor) {
		this.alDistributor = alDistributor;
	}

	/**
	 * @return the alAccList
	 */
	public ArrayList getAlAccList() {
		return alAccList;
	}

	/**
	 * @param alAccList the alAccList to set
	 */
	public void setAlAccList(ArrayList alAccList) {
		this.alAccList = alAccList;
	}

	/**
	 * @return the alSet
	 */
	public ArrayList getAlSet() {
		return alSet;
	}

	/**
	 * @param alSet the alSet to set
	 */
	public void setAlSet(ArrayList alSet) {
		this.alSet = alSet;
	}

	/**
	 * @return the hmNames
	 */
	public HashMap getHmNames() {
		return hmNames;
	}

	/**
	 * @param hmNames the hmNames to set
	 */
	public void setHmNames(HashMap hmNames) {
		this.hmNames = hmNames;
	}

	/**
	 * @return the ArrayList for Tag  report Status
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	
	/**
	 * @param the ArrayList for status
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	
	
	/**
	 * @return the rdReport
	 */
	public RowSetDynaClass getRdReport() {
		return rdReport;
	}

	/**
	 * @param rdReport the rdReport to set
	 */
	public void setRdReport(RowSetDynaClass rdReport) {
		this.rdReport = rdReport;
	}

	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}

	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	
	/**
	 * @return the alLocationType
	 */
	public ArrayList getAlLocationType() {
		return alLocationType;
	}

	/**
	 * @param alLocationType the alLocationType to set
	 */
	public void setAlLocationType(ArrayList alLocationType) {
		this.alLocationType = alLocationType;
	}

	/**
	 * @return the setID
	 */
	public String getSetID() {
		return setID;
	}

	/**
	 * @param setID the setID to set
	 */
	public void setSetID(String setID) {
		this.setID = setID;
	}

	/**
	 * @return the locationType
	 */
	public String getLocationType() {
		return locationType;
	}

	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	/**
	 * @return the typeStatus
	 */
	public String getTypeStatus() {
		return typeStatus;
	}

	/**
	 * @param typeStatus the typeStatus to set
	 */
	public void setTypeStatus(String typeStatus) {
		this.typeStatus = typeStatus;
	}

	/**
	 * @return the tagIdFrom
	 */
	public String getTagIdFrom() {
		return tagIdFrom;
	}

	/**
	 * @param tagIdFrom the tagIdFrom to set
	 */
	public void setTagIdFrom(String tagIdFrom) {
		this.tagIdFrom = tagIdFrom;
	}

	/**
	 * @return the tagIdTo
	 */
	public String getTagIdTo() {
		return tagIdTo;
	}

	/**
	 * @param tagIdTo the tagIdTo to set
	 */
	public void setTagIdTo(String tagIdTo) {
		this.tagIdTo = tagIdTo;
	}

	/**
	 * @return the strOpt
	 */
	public String getStrOpt() {
		return strOpt;
	}

	/**
	 * @param strOpt the strOpt to set
	 */
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}

	/**
	 * @return the locationNames
	 */
	public String getLocationNames() {
		return locationNames;
	}

	/**
	 * @param locationNames the locationNames to set
	 */
	public void setLocationNames(String locationNames) {
		this.locationNames = locationNames;
	}

	/**
	 * @return the alLocationNames
	 */
	public ArrayList getAlLocationNames() {
		return alLocationNames;
	}

	/**
	 * @param alLocationNames the alLocationNames to set
	 */
	public void setAlLocationNames(ArrayList alLocationNames) {
		this.alLocationNames = alLocationNames;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the distributorId
	 */
	public String getDistributorId() {
		return distributorId;
	}

	/**
	 * @param distributorId the distributorId to set
	 */
	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}

	/**
	 * @return the accId
	 */
	public String getAccId() {
		return accId;
	}

	/**
	 * @param accId the accId to set
	 */
	public void setAccId(String accId) {
		this.accId = accId;
	}

	

	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}

	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}

	/**
	 * @return the transferAccess
	 */
	public boolean isTransferAccess() {
		return transferAccess;
	}

	/**
	 * @param transferAccess the transferAccess to set
	 */
	public void setTransferAccess(boolean transferAccess) {
		this.transferAccess = transferAccess;
	}

	/**
	 * @return the voidAccess
	 */
	public boolean isVoidAccess() {
		return voidAccess;
	}

	/**
	 * @param voidAccess the voidAccess to set
	 */
	public void setVoidAccess(boolean voidAccess) {
		this.voidAccess = voidAccess;
	}

	public String getCompanyNm() {
		return companyNm;
	}

	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}

	public String getPalntNm() {
		return palntNm;
	}

	public void setPalntNm(String palntNm) {
		this.palntNm = palntNm;
	}

	public ArrayList getAlCompanyList() {
		return alCompanyList;
	}

	public void setAlCompanyList(ArrayList alCompanyList) {
		this.alCompanyList = alCompanyList;
	}

	public ArrayList getAlPlantList() {
		return alPlantList;
	}

	public void setAlPlantList(ArrayList alPlantList) {
		this.alPlantList = alPlantList;
	}	
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
}
