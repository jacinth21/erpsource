package com.globus.accounts.physicalaudit.forms;

import java.util.ArrayList;
import org.apache.commons.beanutils.RowSetDynaClass;
import com.globus.common.forms.GmCommonForm;

public class GmTagStatusForm extends GmCommonForm {
	
	private String tagIdFrom = "";
	private String tagIdTo = "";
	private String typeStatus = "";
	
	private RowSetDynaClass rdReport= null;
	private ArrayList alStatus = new ArrayList();
	
	public String getTagIdFrom() {
		return tagIdFrom;
	}

	/**
	 * @param tagIdFrom the tagIdFrom to set
	 */
	public void setTagIdFrom(String tagIdFrom) {
		this.tagIdFrom = tagIdFrom;
	}

	/**
	 * @return the tagIdTo
	 */
	public String getTagIdTo() {
		return tagIdTo;
	}

	/**
	 * @param tagIdTo the tagIdTo to set
	 */
	public void setTagIdTo(String tagIdTo) {
		this.tagIdTo = tagIdTo;
	}
	/**
	 * @return the typeStatus
	 */
	public String getTypeStatus() {
		return typeStatus;
	}

	/**
	 * @param typeStatus the typeStatus to set
	 */
	public void setTypeStatus(String typeStatus) {
		this.typeStatus = typeStatus;
	}

	/**
	 * @return the ArrayList for Tag  report Status
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	
	/**
	 * @param the ArrayList for status
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	
	/**
	 * @return the rdReport
	 */
	public RowSetDynaClass getRdReport() {
		return rdReport;
	}

	/**
	 * @param rdReport the rdReport to set
	 */
	public void setRdReport(RowSetDynaClass rdReport) {
		this.rdReport = rdReport;
	}
}