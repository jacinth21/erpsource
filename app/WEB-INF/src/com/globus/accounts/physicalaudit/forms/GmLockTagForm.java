
package com.globus.accounts.physicalaudit.forms;

import com.globus.common.forms.GmCommonForm;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts.upload.FormFile;

public class GmLockTagForm extends GmCommonForm 
{
   private String pnum = "";
   private ArrayList projectList = new ArrayList();
   private ArrayList inventoryList = new ArrayList();
   private ArrayList physicalAuditList = new ArrayList();
   private ArrayList locationList = new ArrayList();
   private ArrayList auditUserList = new ArrayList();   
   private ArrayList reconcileList = new ArrayList();
   private ArrayList alAction = new ArrayList();
   private ArrayList alType = new ArrayList();
   private ArrayList alSummaryList = new ArrayList();
   private String inventoryListID = "";
   private String pnumSuffix = "";
   private String projectListID = "";
   private String verifyLocation = "";
   private String physicalAuditID = "";
   private String locationID = "";
   private String auditUserID = "";
   private String blankTag = "";
   private String tagCount = "1";
   private String hinputString = "";
   private String tagRangeFrom = "";
   private String tagRangeTo = "";
   private String partDescription = "";
   private String uom = "";
   private String tagQty = "";
   private ArrayList uomList = new ArrayList();
   private String transactionID = "";
   private String displayOnDeviation = "on";
   private String valueNotReconciled = "";
   private ArrayList deviationOperatorsList = new ArrayList();
   private String deviationOprID = "";
   private String deviationOprValue = "";
   private String deviationOprQtyID = "";
   private String deviationOprQtyValue = "";
   private List returnList = new ArrayList();   
   private String hinputReconAction = "";
   private String includeTags;
   private String excludeTags;
   private String reconcileListID = "";
   private String hstatusNotcheck = "";
   private ArrayList locationListWithType = new ArrayList();
   private String tagID = "";
   private String maStatus = "";
   private List maIDList = new ArrayList();
   private String historyfl = "";
   private String splitSize = "";
   private String inputStringPnum = "";
   private String strFlgShowVoidRec="";
   private String actionType="PDF";
   private String Chk_selectAll="";
   private String reloadMiss="";
   private String untaggedTxn = "";
   private String hRedirectURL = "";
   private String hAddnlParam = "";
   private String hDisplayNm = "";
   private FormFile uploadExcel = null;
   private String strCheckExcel = "";
   private String strEnableSubmit = "false";
   private String strEnableVoid = "false";
   private String byPart="";
   private ArrayList missingPart = new ArrayList();
   private String piType = "";
   private String supervisiorID = "";
   private ArrayList supervisiorList = new ArrayList(); 
   private String gridXmlData="";
   private String detail="";
   private String hPart = "";
   private String hTransactionID = "";
   
   private String physicalAuditNM = "";

/**
 * @return the physicalAuditNM
 */
public String getPhysicalAuditNM() {
	return physicalAuditNM;
}

/**
 * @param physicalAuditNM the physicalAuditNM to set
 */
public void setPhysicalAuditNM(String physicalAuditNM) {
	this.physicalAuditNM = physicalAuditNM;
}

public String gethPart() {
	return hPart;
}

public void sethPart(String hPart) {
	this.hPart = hPart;
}



	public String gethTransactionID() {
	return hTransactionID;
}

public void sethTransactionID(String hTransactionID) {
	this.hTransactionID = hTransactionID;
}

	public String getDetail() {
	return detail;
}

public void setDetail(String detail) {
	this.detail = detail;
}

	public ArrayList getAlSummaryList() {
	return alSummaryList;
}

public void setAlSummaryList(ArrayList alSummaryList) {
	this.alSummaryList = alSummaryList;
}

public String getGridXmlData() {
	return gridXmlData;
}

public void setGridXmlData(String gridXmlData) {
	this.gridXmlData = gridXmlData;
}

	public String getSupervisiorID() {
	return supervisiorID;
}

public void setSupervisiorID(String supervisiorID) {
	this.supervisiorID = supervisiorID;
}

public ArrayList getSupervisiorList() {
	return supervisiorList;
}

public void setSupervisiorList(ArrayList supervisiorList) {
	this.supervisiorList = supervisiorList;
}

	public String getPiType() {
	return piType;
}

public void setPiType(String piType) {
	this.piType = piType;
}

	public ArrayList getMissingPart() {
	return missingPart;
}

public void setMissingPart(ArrayList missingPart) {
	this.missingPart = missingPart;
}

	public String getByPart() {
    	return byPart;
    }

    public void setByPart(String byPart) {
    	this.byPart = byPart;
    }
    
   
   /**
 * @return the strEnableSubmit
 */
public String getStrEnableSubmit() {
	return strEnableSubmit;
}
/**
 * @param strEnableSubmit the strEnableSubmit to set
 */
public void setStrEnableSubmit(String strEnableSubmit) {
	this.strEnableSubmit = strEnableSubmit;
}
/**
 * @return the strEnableVoid
 */
public String getStrEnableVoid() {
	return strEnableVoid;
}
/**
 * @param strEnableVoid the strEnableVoid to set
 */
public void setStrEnableVoid(String strEnableVoid) {
	this.strEnableVoid = strEnableVoid;
}
/**
    * @return the verifyLocation
    */
   public String getVerifyLocation() {
	return verifyLocation;
}
   /**
    * @param verifyLocation the verifyLocation to set
    */
   public void setVerifyLocation(String verifyLocation) {
	 this.verifyLocation = verifyLocation;
}

/**
 * @return the alType
 */
public ArrayList getAlType() {
	return alType;
}

/**
 * @param alType the alType to set
 */
public void setAlType(ArrayList alType) {
	this.alType = alType;
}
   
   
   /**
 * @return the strCheckExcel
 */
public String getStrCheckExcel() {
	return strCheckExcel;
}

/**
 * @param strCheckExcel the strCheckExcel to set
 */
public void setStrCheckExcel(String strCheckExcel) {
	this.strCheckExcel = strCheckExcel;
}

/**
 * @return the hAddnlParam
 */
public String gethAddnlParam() {
	return hAddnlParam;
}

/**
 * @param hAddnlParam the hAddnlParam to set
 */
public void sethAddnlParam(String hAddnlParam) {
	this.hAddnlParam = hAddnlParam;
}

/**
 * @return the hDisplayNm
 */
public String gethDisplayNm() {
	return hDisplayNm;
}

/**
 * @param hDisplayNm the hDisplayNm to set
 */
public void sethDisplayNm(String hDisplayNm) {
	this.hDisplayNm = hDisplayNm;
}

/**
 * @return the hRedirectURL
 */
public String gethRedirectURL() {
	return hRedirectURL;
}

/**
 * @param hRedirectURL the hRedirectURL to set
 */
public void sethRedirectURL(String hRedirectURL) {
	this.hRedirectURL = hRedirectURL;
}


   /**
 * @return the untaggedTxn
 */
public String getUntaggedTxn() {
	return untaggedTxn;
}

/**
 * @param untaggedTxn the untaggedTxn to set
 */
public void setUntaggedTxn(String untaggedTxn) {
	this.untaggedTxn = untaggedTxn;
}

/**
 * @return the reloadMiss
 */
public String getReloadMiss() {
	return reloadMiss;
}

/**
 * @param reloadMiss the reloadMiss to set
 */
public void setReloadMiss(String reloadMiss) {
	this.reloadMiss = reloadMiss;
}




/**
 * @return the uploadExcel
 */
public FormFile getUploadExcel() {
	return uploadExcel;
}

/**
 * @param uploadExcel the uploadExcel to set
 */
public void setUploadExcel(FormFile uploadExcel) {
	this.uploadExcel = uploadExcel;
}


/**
 * @return the chk_selectAll
 */
public String getChk_selectAll() {
	return Chk_selectAll;
}

/**
 * @param chkSelectAll the chk_selectAll to set
 */
public void setChk_selectAll(String chkSelectAll) {
	Chk_selectAll = chkSelectAll;
}

/**
 * @return the actionType
 */
public String getActionType() {
	return actionType;
}

/**
 * @param actionType the actionType to set
 */
public void setActionType(String actionType) {
	this.actionType = actionType;
}

/**
 * @return the strFlgShowVoidRec
 */
public String getStrFlgShowVoidRec() {
	return strFlgShowVoidRec;
}

/**
 * @param strFlgShowVoidRec the strFlgShowVoidRec to set
 */
public void setStrFlgShowVoidRec(String strFlgShowVoidRec) {
	
	if(strFlgShowVoidRec.equals("Y") || strFlgShowVoidRec.equals("on")){
		this.strFlgShowVoidRec = "on";
	}else{
		this.strFlgShowVoidRec = strFlgShowVoidRec;
	}
	
}

public String getSplitSize() {
	return splitSize;
}

public void setSplitSize(String splitSize) {
	this.splitSize = splitSize;
}

public String getMaStatus() {
return maStatus;
}

public void setMaStatus(String maStatus) {
	this.maStatus = maStatus;
}
public void reset()
{
	tagID = "";
	auditUserID = "0";
	locationID = "0";
	partDescription = "";
	uom ="";
	transactionID = "";
	pnum = "";
	tagQty = "";
	piType = "";
}
   
public ArrayList getLocationListWithType() {
	return locationListWithType;
}

public void setLocationListWithType(ArrayList locationListWithType) {
	this.locationListWithType = locationListWithType;
}

public String getPnum() {
	return pnum;
}

public void setPnum(String pnum) {
	this.pnum = pnum;
}

public ArrayList getProjectList() {
	return projectList;
}

public void setProjectList(ArrayList projectList) {
	this.projectList = projectList;
}

public ArrayList getInventoryList() {
	return inventoryList;
}

public void setInventoryList(ArrayList inventoryList) {
	this.inventoryList = inventoryList;
}

public ArrayList getPhysicalAuditList() {
	return physicalAuditList;
}

public void setPhysicalAuditList(ArrayList physicalAuditList) {
	this.physicalAuditList = physicalAuditList;
}

public ArrayList getLocationList() {
	return locationList;
}

public void setLocationList(ArrayList locationList) {
	this.locationList = locationList;
}

public ArrayList getAuditUserList() {
	return auditUserList;
}

public void setAuditUserList(ArrayList auditUserList) {
	this.auditUserList = auditUserList;
}

public String getInventoryListID() {
	return inventoryListID;
}

public void setInventoryListID(String inventoryListID) {
	this.inventoryListID = inventoryListID;
}

public String getPnumSuffix() {
	return pnumSuffix;
}

public void setPnumSuffix(String pnumSuffix) {
	this.pnumSuffix = pnumSuffix;
}

public String getProjectListID() {
	return projectListID;
}

public void setProjectListID(String projectListID) {
	this.projectListID = projectListID;
}

public String getPhysicalAuditID() {
	return physicalAuditID;
}

public void setPhysicalAuditID(String physicalAuditID) {
	this.physicalAuditID = physicalAuditID;
}

public String getLocationID() {
	return locationID;
}

public void setLocationID(String locationID) {
	this.locationID = locationID;
}

public String getAuditUserID() {
	return auditUserID;
}

public void setAuditUserID(String auditUserID) {
	this.auditUserID = auditUserID;
}

public String getBlankTag() {
	return blankTag;
}

public void setBlankTag(String blankTag) {
	this.blankTag = blankTag;
}

public String getTagCount() {
	return tagCount;
}

public void setTagCount(String tagCount) {
	this.tagCount = tagCount;
}

public String getHinputString() {
	return hinputString;
}

public void setHinputString(String hinputString) {
	this.hinputString = hinputString;
}

public String getTagRangeFrom() {
	return tagRangeFrom;
}

public void setTagRangeFrom(String tagRangeFrom) {
	this.tagRangeFrom = tagRangeFrom;
}

public String getTagRangeTo() {
	return tagRangeTo;
}

public void setTagRangeTo(String tagRangeTo) {
	this.tagRangeTo = tagRangeTo;
}

public String getPartDescription() {
	return partDescription;
}

public void setPartDescription(String partDescription) {
	this.partDescription = partDescription;
}

public String getUom() {
	return uom;
}

public void setUom(String uom) {
	this.uom = uom;
}

public String getTagQty() {
	return tagQty;
}

public void setTagQty(String tagQty) {
	this.tagQty = tagQty;
}

public ArrayList getUomList() {
	return uomList;
}

public void setUomList(ArrayList uomList) {
	this.uomList = uomList;
}

public String getTransactionID() {
	return transactionID;
}

public void setTransactionID(String transactionID) {
	this.transactionID = transactionID;
}

public String getDisplayOnDeviation() {
	return displayOnDeviation;
}

public void setDisplayOnDeviation(String displayOnDeviation) {
	this.displayOnDeviation = displayOnDeviation;
}

public String getValueNotReconciled() {
	return valueNotReconciled;
}

public void setValueNotReconciled(String valueNotReconciled) {
	this.valueNotReconciled = valueNotReconciled;
}

public ArrayList getDeviationOperatorsList() {
	return deviationOperatorsList;
}

public void setDeviationOperatorsList(ArrayList deviationOperatorsList) {
	this.deviationOperatorsList = deviationOperatorsList;
}

public String getDeviationOprID() {
	return deviationOprID;
}

public void setDeviationOprID(String deviationOprID) {
	this.deviationOprID = deviationOprID;
}

public String getDeviationOprValue() {
	return deviationOprValue;
}

public void setDeviationOprValue(String deviationOprValue) {
	this.deviationOprValue = deviationOprValue;
}

public String getDeviationOprQtyID() {
	return deviationOprQtyID;
}

public void setDeviationOprQtyID(String deviationOprQtyID) {
	this.deviationOprQtyID = deviationOprQtyID;
}

public String getDeviationOprQtyValue() {
	return deviationOprQtyValue;
}

public void setDeviationOprQtyValue(String deviationOprQtyValue) {
	this.deviationOprQtyValue = deviationOprQtyValue;
}

public List getReturnList() {
	return returnList;
}

public void setReturnList(List returnList) {
	this.returnList = returnList;
}

public String getHinputReconAction() {
	return hinputReconAction;
}

public void setHinputReconAction(String hinputReconAction) {
	this.hinputReconAction = hinputReconAction;
}

public String getIncludeTags() {
	return includeTags;
}
public void setIncludeTags(String includeTags) {
	this.includeTags = includeTags;
}
public String getExcludeTags() {
	return excludeTags;
}
public void setExcludeTags(String excludeTags) {
	this.excludeTags = excludeTags;
}

public ArrayList getReconcileList() {
	return reconcileList;
}

public void setReconcileList(ArrayList reconcileList) {
	this.reconcileList = reconcileList;
}

public String getReconcileListID() {
	return reconcileListID;
}

public void setReconcileListID(String reconcileListID) {
	this.reconcileListID = reconcileListID;
}

public String getHstatusNotcheck() {
	return hstatusNotcheck;
}

public void setHstatusNotcheck(String hstatusNotcheck) {
	this.hstatusNotcheck = hstatusNotcheck;
}

public String getTagID() {
	return tagID;
}

public void setTagID(String tagID) {
	this.tagID = tagID;
}

public List getMaIDList() {
	return maIDList;
}

public void setMaIDList(List maIDList) {
	this.maIDList = maIDList;
}

public String getHistoryfl() {
	return historyfl;
}

public void setHistoryfl(String historyfl) {
	this.historyfl = historyfl;
}

/**
 * @return the inputStringPnum
 */
public String getInputStringPnum() {
	return inputStringPnum;
}

/**
 * @param inputStringPnum the inputStringPnum to set
 */
public void setInputStringPnum(String inputStringPnum) {
	this.inputStringPnum = inputStringPnum;
}



/**
 * @return the alAction
 */
public ArrayList getAlAction() {
	return alAction;
}

/**
 * @param alAction the alAction to set
 */
public void setAlAction(ArrayList alAction) {
	this.alAction = alAction;
}
 
 
}

