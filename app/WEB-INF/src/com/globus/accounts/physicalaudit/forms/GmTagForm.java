
package com.globus.accounts.physicalaudit.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCancelForm;
 
public class GmTagForm extends GmCancelForm {
	
	private String pnum = "";
	private String tagID = "";

	private String controlNum = "";
	private String refID = "";
	private String refType = "";
	private String setID = "";
	private String locationType = "";
	private String location = "";
	private String hinputStr = "";
	private String strIntransitFlag = "";
	private boolean missingFlag ;
	private boolean missingAccess ;
	private boolean unlinkAccess ;
	private boolean voidAccess ;
	
	private List taggedReturnList = new ArrayList();
	private List unTaggedReturnList = new ArrayList();
	private List alReasonList = new ArrayList();
	private RowSetDynaClass rdHistory = null;

	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}

	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}

	/**
	 * @return the tagID
	 */
	public String getTagID() {
		return tagID;
	}

	/**
	 * @param tagID the tagID to set
	 */
	public void setTagID(String tagID) {
		this.tagID = tagID;
	}

	/**
	 * @return the controlNum
	 */
	public String getControlNum() {
		return controlNum;
	}

	/**
	 * @param controlNum the controlNum to set
	 */
	public void setControlNum(String controlNum) {
		this.controlNum = controlNum;
	}

	/**
	 * @return the refID
	 */
	public String getRefID() {
		return refID;
	}

	/**
	 * @param refID the refID to set
	 */
	public void setRefID(String refID) {
		this.refID = refID;
	}

	/**
	 * @return the setID
	 */
	public String getSetID() {
		return setID;
	}

	/**
	 * @param setID the setID to set
	 */
	public void setSetID(String setID) {
		this.setID = setID;
	}

	/**
	 * @return the locationType
	 */
	public String getLocationType() {
		return locationType;
	}

	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	 
	/**
	 * @return the hinputStr
	 */
	public String getHinputStr() {
		return hinputStr;
	}

	/**
	 * @param hinputStr the hinputStr to set
	 */
	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}

	/**
	 * @return the refType
	 */
	public String getRefType() {
		return refType;
	}

	/**
	 * @param refType the refType to set
	 */
	public void setRefType(String refType) {
		this.refType = refType;
	}
	
	/**
	 * @return the strIntransitFlag
	 */
	public String getStrIntransitFlag() {
		return strIntransitFlag;
	}
	
	/**
	 * @param strIntransitFlag the strIntransitFlag to set
	 */
	public void setStrIntransitFlag(String strIntransitFlag) {
		this.strIntransitFlag = strIntransitFlag;
	}

	/**
	 * @return the taggedReturnList
	 */
	public List getTaggedReturnList() {
		return taggedReturnList;
	}

	/**
	 * @param taggedReturnList the taggedReturnList to set
	 */
	public void setTaggedReturnList(List taggedReturnList) {
		this.taggedReturnList = taggedReturnList;
	}

	/**
	 * @return the unTaggedReturnList
	 */
	public List getUnTaggedReturnList() {
		return unTaggedReturnList;
	}

	/**
	 * @param unTaggedReturnList the unTaggedReturnList to set
	 */
	public void setUnTaggedReturnList(List unTaggedReturnList) {
		this.unTaggedReturnList = unTaggedReturnList;
	}

	/**
	 * @return the rdHistory
	 */
	public RowSetDynaClass getRdHistory() {
		return rdHistory;
	}

	/**
	 * @param rdHistory the rdHistory to set
	 */
	public void setRdHistory(RowSetDynaClass rdHistory) {
		this.rdHistory = rdHistory;
	}


	/**
	 * @return the missingFlag
	 */
	public boolean isMissingFlag() {
		return missingFlag;
	}

	/**
	 * @param missingFlag the missingFlag to set
	 */
	public void setMissingFlag(boolean missingFlag) {
		this.missingFlag = missingFlag;
	}

	/**
	 * @return the alReasonList
	 */
	public List getAlReasonList() {
		return alReasonList;
	}

	/**
	 * @param alReasonList the alReasonList to set
	 */
	public void setAlReasonList(List alReasonList) {
		this.alReasonList = alReasonList;
	}

	/**
	 * @return the missingAccess
	 */
	public boolean isMissingAccess() {
		return missingAccess;
	}

	/**
	 * @param missingAccess the missingAccess to set
	 */
	public void setMissingAccess(boolean missingAccess) {
		this.missingAccess = missingAccess;
	}

	/**
	 * @return the unlinkAccess
	 */
	public boolean isUnlinkAccess() {
		return unlinkAccess;
	}

	/**
	 * @param unlinkAccess the unlinkAccess to set
	 */
	public void setUnlinkAccess(boolean unlinkAccess) {
		this.unlinkAccess = unlinkAccess;
	}

	/**
	 * @return the voidAccess
	 */
	public boolean isVoidAccess() {
		return voidAccess;
	}

	/**
	 * @param voidAccess the voidAccess to set
	 */
	public void setVoidAccess(boolean voidAccess) {
		this.voidAccess = voidAccess;
	}



	
}
