
package com.globus.accounts.physicalaudit.forms;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCommonForm;
 
public class GmTagTransferForm extends GmCommonForm {
	
	private String tagId  = ""; 
	private String hTagId = "";
	private String pnum = "";
	private String transferTo = "";
	private String controlNum = "";
	private String setId = "";
	private String refId = "";
	private String status = "";
	private String currlocation = "";
	private String currrefid = "";
	private boolean demandSheetAccess = false;
	private String currLocationId = "";
	private String locationType = "";
	private String strDisabled = "";
	private String consignedTo = "";
	private String inventoryType = "";
	private String setType = "";
	private String successMessage = "";
	private String comments = ""; 
	private String missingDate = "";
	private String missingdistid = "";
	private String oldlocationid = "";

	
	/*PMT-23439*/
	private String 	strTagStatusUpdateFl = "";
	

	private HashMap hmNames = new HashMap ();
	
	/**
	 * @return the currLocationId
	 */
	public String getCurrLocationId() {
		return currLocationId;
	}
	/**
	 * @param currLocationId the currLocationId to set
	 */
	public void setCurrLocationId(String currLocationId) {
		this.currLocationId = currLocationId;
	}

	private ArrayList alTransferTo = new ArrayList();
	private ArrayList alSetId = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alSetIdResult = new ArrayList();
	private ArrayList alLocationType = new ArrayList();
	private ArrayList alInventoryType = new ArrayList();
	
	/**
	 * @return the currlocation
	 */
	public String getCurrlocation() {
		return currlocation;
	}
	/**
	 * @param currlocation the currlocation to set
	 */
	public void setCurrlocation(String currlocation) {
		this.currlocation = currlocation;
	}
	/**
	 * @return the currrefid
	 */
	public String getCurrrefid() {
		return currrefid;
	}
	/**
	 * @param currrefid the currrefid to set
	 */
	public void setCurrrefid(String currrefid) {
		this.currrefid = currrefid;
	}
	/**
	 * @return the hTagId
	 */
	public String gethTagId() {
		return hTagId;
	}
	/**
	 * @param hTagId the hTagId to set
	 */
	public void sethTagId(String hTagId) {
		this.hTagId = hTagId;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	private String gridXmlData = "";

	/**
	 * @return the demandSheetAccess
	 */
	public boolean isDemandSheetAccess() {
		return demandSheetAccess;
	}
	/**
	 * @param demandSheetAccess the demandSheetAccess to set
	 */
	public void setDemandSheetAccess(boolean demandSheetAccess) {
		this.demandSheetAccess = demandSheetAccess;
	}
	/*the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	/**
	 * @return the alSetIdResult
	 */
	public ArrayList getAlSetIdResult() {
		return alSetIdResult;
	}
	/**
	 * @param alSetIdResult the alSetIdResult to set
	 */
	public void setAlSetIdResult(ArrayList alSetIdResult) {
		this.alSetIdResult = alSetIdResult;
	}
	/**
	 * @return the tagId
	 */
	public String getTagId() {
		return tagId;
	}
	/**
	 * @param tagId the tagId to set
	 */
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	/**
	 * @return the pNum
	 */
	public String getPnum() {
		return pnum;
	}
	/**
	 * @param pNum the pNum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	/**
	 * @return the transferTo
	 */
	public String getTransferTo() {
		return transferTo;
	}
	/**
	 * @return the strTagStatusUpdateFl
	 */
	public String getStrTagStatusUpdateFl() {
		return strTagStatusUpdateFl;
	}
	/**
	 * @param strTagStatusUpdateFl the strTagStatusUpdateFl to set
	 */
	public void setStrTagStatusUpdateFl(String strTagStatusUpdateFl) {
		this.strTagStatusUpdateFl = strTagStatusUpdateFl;
	}
	/**
	 * @param transferTo the transferTo to set
	 */
	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}
	/**
	 * @return the controlNum
	 */
	public String getControlNum() {
		return controlNum;
	}
	/**
	 * @param controlNum the controlNum to set
	 */
	public void setControlNum(String controlNum) {
		this.controlNum = controlNum;
	}
	/**
	 * @return the setId
	 */
	public String getSetId() {
		return setId;
	}
	/**
	 * @param setId the setId to set
	 */
	public void setSetId(String setId) {
		this.setId = setId;
	}
	/**
	 * @return the refId
	 */
	public String getRefId() {
		return refId;
	}
	/**
	 * @param refId the refId to set
	 */
	public void setRefId(String refId) {
		this.refId = refId;
	}
	/**
	 * @return the locationType
	 */
	public String getLocationType() {
		return locationType;
	}
	/**
	 * @return the setType
	 */
	public String getSetType() {
		return setType;
	}
	/**
	 * @param setType the setType to set
	 */
	public void setSetType(String setType) {
		this.setType = setType;
	}
	/**
	 * @return the strDisabled
	 */
	public String getStrDisabled() {
		return strDisabled;
	}
	/**
	 * @param strDisabled the strDisabled to set
	 */
	public void setStrDisabled(String strDisabled) {
		this.strDisabled = strDisabled;
	}
	/**
	 * @return the consignedTo
	 */
	public String getConsignedTo() {
		return consignedTo;
	}
	/**
	 * @param consignedTo the consignedTo to set
	 */
	public void setConsignedTo(String consignedTo) {
		this.consignedTo = consignedTo;
	}
	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}
	/**
	 * @return the successMessage
	 */
	public String getSuccessMessage() {
		return successMessage;
	}
	/**
	 * @param successMessage the successMessage to set
	 */
	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}
	/**
	 * @return the hmNames
	 */
	public HashMap getHmNames() {
		return hmNames;
	}
	/**
	 * @param hmNames the hmNames to set
	 */
	public void setHmNames(HashMap hmNames) {
		this.hmNames = hmNames;
	}
	/**
	 * @return the inventoryType
	 */
	public String getInventoryType() {
		return inventoryType;
	}
	/**
	 * @param inventoryType the inventoryType to set
	 */
	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}
	
	public ArrayList getAlTransferTo() {
		return alTransferTo;
	}
	/**
	 * @param alTransferTo the alTransferTo to set
	 */
	public void setAlTransferTo(ArrayList alTransferTo) {
		this.alTransferTo = alTransferTo;
	}
	/**
	 * @return the alSetId
	 */
	public ArrayList getAlSetId() {
		return alSetId;
	}
	/**
	 * @param alSetId the alSetId to set
	 */
	public void setAlSetId(ArrayList alSetId) {
		this.alSetId = alSetId;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the alLocationType
	 */
	public ArrayList getAlLocationType() {
		return alLocationType;
	}
	/**
	 * @param alLocationType the alLocationType to set
	 */
	public void setAlLocationType(ArrayList alLocationType) {
		this.alLocationType = alLocationType;
	}
	/**
	 * @return the alInventoryType
	 */
	public ArrayList getAlInventoryType() {
		return alInventoryType;
	}
	/**
	 * @param alInventoryType the alInventoryType to set
	 */
	public void setAlInventoryType(ArrayList alInventoryType) {
		this.alInventoryType = alInventoryType;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param Comments the Comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the missingDate
	 */
	public String getMissingDate() {
		return missingDate;
	}
	/**
	 * @param missingDate the missingDate to set
	 */
	public void setMissingDate(String missingDate) {
		this.missingDate = missingDate;
	}
	/**
	 * @return the missingdistid
	 */
	public String getMissingdistid() {
		return missingdistid;
	}
	/**
	 * @param missingdistid the missingdistid to set
	 */
	public void setMissingdistid(String missingdistid) {
		this.missingdistid = missingdistid;
	}
	/**
	 * @return the oldlocationid
	 */
	public String getOldlocationid() {
		return oldlocationid;
	}
	/**
	 * @param oldlocationid the oldlocationid to set
	 */
	public void setOldlocationid(String oldlocationid) {
		this.oldlocationid = oldlocationid;
	}

	
}
