package com.globus.accounts.beans;

import java.sql.ResultSet; 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.printwork.invoice.beans.GmInvoicePrintInterface;
import com.globus.common.printwork.invoice.beans.GmInvoicePrintLayoutInterface;
import com.globus.custservice.beans.GmDOBean;
import com.globus.sales.pricing.beans.GmPartPriceAdjRptBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmInvoicePrintBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmInvoicePrintBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  public HashMap fetchInvoiceDetails(String strPO, String strAccId, String strAction)
      throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmOrderDetails = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alLoop = new ArrayList();
    ArrayList alPayDetails = new ArrayList();

    String strOrdId = "";
    HashMap hmResult = new HashMap();
    HashMap hmConstruct = new HashMap();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    // Below code to fetch the Order List for the selected PO and
    // Account
    alReturn = gmDBManager.queryMultipleRecords(loadOrderSQL(strAccId, strPO, strAction));
    hmReturn.put("ORDERNUMS", alReturn);
    log.debug(" Values inside hmreturn s " + hmReturn);
    log.debug(" INout Values are " + strPO + " Ac Id " + strAccId + " Action us " + strAction);

    int intLength = 0;
    intLength = alReturn.size();
    if (intLength > 0) {
      hmTemp = (HashMap) alReturn.get(0);
      strOrdId = (String) hmTemp.get("ID");
      log.debug(" Order ID is " + strOrdId);
    }

    // Invoice Header Details
    sbQuery.setLength(0);
    sbQuery
        .append(" SELECT A.C704_ACCOUNT_ID ACCID, NVL(C.C503_INVOICE_ID,'TO-BE') INVFL , A.C503_INVOICE_ID INV");
    sbQuery.append(" , GET_DIST_REP_NAME(A.C703_SALES_REP_ID) REPDISTNM, ");

    sbQuery.append(" A.C501_CUSTOMER_PO PO, GET_BILL_ADD(A.C704_ACCOUNT_ID) BILLADD, ");
    sbQuery.append(" decode(C.C503_INVOICE_DATE,'',A.C501_ORDER_DATE,C.C503_INVOICE_DATE) INVDT, ");
    sbQuery
        .append(" decode(C.C503_INVOICE_DATE,'',(A.C501_ORDER_DATE+30),(C.C503_INVOICE_DATE+30)) DUEDT ,");
    sbQuery.append(" B.C704_PAYMENT_TERMS PAY, GET_CODE_"
        + "NAME(B.C704_PAYMENT_TERMS) PAYNM, A.C501_STATUS_FL SFL ");
    sbQuery.append(" ,A.C501_COMMENTS CMENT , C.C901_INVOICE_TYPE INVTYPE ");
    sbQuery.append(" ,GET_LOG_FLAG(C.C503_INVOICE_ID,1201) CALL_FLAG ");
    sbQuery
        .append(" ,GET_ACCOUNT_ATTRB_VALUE (B.c704_account_id, 91983) EMAILREQ,  GET_ACCOUNT_ATTRB_VALUE (B.c704_account_id, 91984)  EVERSION ");
    sbQuery
        .append(", GET_ACCOUNT_ATTRB_VALUE (b.c704_account_id, 101191) valid_add_fl, get_rule_value_by_company(b.c704_ship_country,'TAX_COUNTRY', b.c1900_company_id) tax_country_fl ");
    sbQuery
        .append(" , (CASE WHEN TRUNC (to_date (get_rule_value ('TAX_DATE', 'TAX_INVOICE'), 'mm/DD/YYYY')) <= TRUNC (A.c501_order_date) THEN 'Y' ELSE 'N' END) tax_start_date ");
    sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE (b.c704_account_id, 10304532) invoicelayout");
    sbQuery.append(" , get_code_name(c901_currency) CURRSIGN ");
    sbQuery.append(" FROM T501_ORDER A, T704_ACCOUNT B ,T503_INVOICE C ");
    sbQuery.append(" WHERE A.C501_ORDER_ID = '");
    sbQuery.append(strOrdId);
    sbQuery
        .append("' AND A.C704_ACCOUNT_ID = B.C704_ACCOUNT_ID AND A.C503_INVOICE_ID = C.C503_INVOICE_ID(+) ");
    sbQuery.append(" AND C.C503_VOID_FL IS NULL ");
    sbQuery.append(" AND A.C501_VOID_FL IS NULL ");
    log.debug(" Query to fetch Order details is " + sbQuery.toString());
    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    hmReturn.put("ORDERDETAILS", hmResult);
    log.debug(" Query to fetch Order details is " + sbQuery.toString());
    log.debug(" Values are " + hmResult);

    hmResult = new HashMap();

    // Below Code will fetch the Order Item details for the selected
    // customer invoice
    GmInvoicePrintInterface gmInvoicePrintInterface =
        (GmInvoicePrintInterface) GmInvoiceBean.getInvoicePrintClass(getGmDataStoreVO().getCmpid());
    for (int i = 0; i < intLength; i++) {
      sbQuery.setLength(0);
      hmTemp = new HashMap();
      hmTemp = (HashMap) alReturn.get(i);
      strOrdId = (String) hmTemp.get("ID");
      log.debug(" Calling LoadOrderItem from fetchInvoiceDetails .....");
      alLoop =
          gmDBManager.queryMultipleRecords(gmInvoicePrintInterface.loadOrderItemSQL(strOrdId,
              strAction, getGmDataStoreVO()));
      hmResult.put(strOrdId, alLoop);
      alLoop = gmDBManager.queryMultipleRecords(loadOrderConstructSQL(strOrdId));
      hmConstruct.put(strOrdId, alLoop);
    }
    hmReturn.put("CARTDETAILS", hmResult);
    hmReturn.put("CONSTRUCTDETAILS", hmConstruct);

    return hmReturn;
  } // End of loadInvoiceDetails

  /**
   * loadInvForReturns - This method will be used to send the required SQL based on the action
   * Requested
   * 
   * @param String strAction
   * @param String strPO
   * @param String strAction
   * @return HashMap
   * @exception AppError
   */
  public String loadOrderSQL(String strAccId, String strPO, String strAction) {
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT C501_ORDER_ID ID ");
    sbQuery.append(" ,C501_COMMENTS CMENT");
    sbQuery.append(" FROM T501_ORDER ");
    sbQuery.append(" WHERE C704_ACCOUNT_ID = '");
    sbQuery.append(strAccId);
    sbQuery.append("' AND C501_CUSTOMER_PO = '");
    sbQuery.append(strPO);
    sbQuery.append("'  AND C503_INVOICE_ID IS NULL AND C501_STATUS_FL IS NOT NULL ");
    sbQuery.append(" AND C501_VOID_FL is null ");
    sbQuery.append(" AND nvl(C901_order_type,-9999) NOT IN ('2535','101260') ");
    sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
    sbQuery.append(" SELECT t906.c906_rule_value ");
    sbQuery.append(" FROM t906_rules t906 ");
    sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
    sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
    // Invoice Print Should not include the return
    // that are raised before invoice
    // If condition is added for the same
    if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
      // To skip all the back order if not shipped
      sbQuery.append(" AND NVL(C901_ORDER_TYPE,-9999) <> 2529 ");
      sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
    }
    sbQuery.append(" ORDER BY ID ");
    log.debug(" loadOrder SQL is " + sbQuery.toString());

    return (sbQuery.toString());
  }

  /**
   * loadInvForReturns - This method will be used to send the required SQL based on the action
   * Requested
   * 
   * @param String strOrdId
   * @param String strAction
   * @return String
   * @exception AppError
   */
  public String loadOrderItemSQL(String strOrdId, String strAction) {

    StringBuffer sbQuery = new StringBuffer();
    String strApplDateFmt = getCompDateFmt();
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    GmResourceBundleBean gmResourceBundleBean1 =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    String strctrlSplitUp =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.CTRLSPLITUP"));
    String strShowPartUomFl =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.SHOW_PART_UOM"));
    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strUsedLotFl =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.USED_LOT_FL"));
    // Below Query is added in Print so the zero invoice order
    // is not listed in the screen
    if (strCompanyLocale.equals("FR")) {

      // This section is used to get the exact qty for the parts without adding the return qty into
      // the invoice print.
      if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
        sbQuery.append("  SELECT * FROM ");
        sbQuery.append(" ( ");
        sbQuery
            .append(" SELECT DISTINCT(A.C205_PART_NUMBER_ID) ID , get_partdesc_by_company (A.C205_PART_NUMBER_ID) PDESC ");
        sbQuery
            .append(" ,get_order_part_qty_invoice(A.C501_ORDER_ID,A.C205_PART_NUMBER_ID,A.C502_ITEM_PRICE,A.c901_type,DECODE(A.C502_CONSTRUCT_FL,NULL,'N',A.C502_CONSTRUCT_FL),A.C502_Control_Number) QTY ");
        sbQuery.append(" , A.C502_CONTROL_NUMBER CNUM, t205.c205_product_family prodfamily ");
      } else {
        sbQuery
            .append(" SELECT A.C205_PART_NUMBER_ID ID , get_partdesc_by_company (A.C205_PART_NUMBER_ID) PDESC  , A.C502_CONTROL_NUMBER CNUM  ");
        sbQuery.append(" , A.C502_ITEM_QTY QTY, A.C502_ITEM_ORDER_ID ITEM_ORD_ID ");
      }
      sbQuery
          .append(" , A.C502_ITEM_PRICE PRICE , NVL (A.C502_BASE_PRICE, 0) ACTUALPRICE , A.C502_EXT_ITEM_PRICE EXTERNAL_ITEM_PRICE , B.C501_EXT_TOTAL_COST EXTERNAL_TOTAL_COST ");

      if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
        sbQuery
            .append(" ,ROUND (NVL ( (get_order_part_qty_invoice(A.C501_ORDER_ID,A.C205_PART_NUMBER_ID,A.C502_ITEM_PRICE,A.c901_type,");
        sbQuery
            .append(" DECODE(A.C502_CONSTRUCT_FL,NULL,'N',A.C502_CONSTRUCT_FL),A.C502_Control_Number) * NVL (A.C502_ITEM_PRICE, 0) * NVL (A.c502_vat_rate, 0) / 100), 0), 2) tax_cost ");

      } else {
        sbQuery
            .append(" , NVL(A.c502_tax_amt, ROUND (NVL ( (A.C502_ITEM_QTY * NVL (A.C502_ITEM_PRICE, 0) * NVL (A.c502_vat_rate, 0) / 100), 0), 2)) tax_cost ");
      }

      sbQuery.append("  , A.c901_type TYPE , A.C502_CONSTRUCT_FL CONFL , B.C501_SHIP_COST SCOST ");
      sbQuery
          .append("  , B.C501_ORDER_DATE ODT , B.C501_SURGERY_DATE SURG_DT , NVL (a.c502_vat_rate, 0) VAT  , get_part_attr_value_by_comp (A.C205_PART_NUMBER_ID,'200001', B.C1900_COMPANY_ID) NTFTCODE ,");
      sbQuery.append(" NVL (B.C501_Shipping_Date, '') shipdate , NVL (c502_unit_price, 0) unitprice , NVL ( (c502_unit_price_adj_value * - 1 ), 0) adjval ");
      sbQuery
          .append(" , DECODE (C901_UNIT_PRICE_ADJ_CODE, NULL, 'N/A', GET_CODE_NAME (c901_unit_price_adj_code)) adjcode , DECODE (NVL (C502_UNIT_PRICE_ADJ_VALUE, 0), '0', '', 'Y') ADJFLAG ,");
      sbQuery.append(" GET_CODE_NAME_alt (c901_unit_price_adj_code)  ADJCODEALT ");
      sbQuery.append(" , NVL (A.C502_ITEM_PRICE, 0) - NVL (C502_UNIT_PRICE, 0) ADJEA ");
      sbQuery.append("  FROM T502_ITEM_ORDER A, T501_ORDER B, t205_part_number t205 ");
      sbQuery.append(" WHERE A.C501_ORDER_ID    = '" + strOrdId + "' ");
      sbQuery.append(" AND a.c205_part_number_id =  t205.c205_part_number_id ");
      // The following subquery will exclude the parent parts which available in sub component
      // mapping
      sbQuery
          .append(" AND A.C502_VOID_FL    IS NULL  AND A.C502_DELETE_FL  IS NULL AND A.C501_ORDER_ID    = B.C501_ORDER_ID  AND a.c205_part_number_id NOT IN (SELECT t2020a.c2020a_sub_component_num ID ");
      sbQuery
          .append(" FROM t2020a_sub_cmap_trans_details t2020a, t502_item_order t502, t501_order t501 ");
      sbQuery.append(" WHERE t2020a.c2020a_void_fl   IS NULL ");
      sbQuery
          .append(" AND t502.c502_void_fl       IS NULL AND t501.c501_void_fl       IS NULL AND t502.c205_part_number_id = t2020a.c205_part_number_id AND t2020a.c501_order_id     = t502.c501_order_id ");
      sbQuery
          .append(" AND t502.c501_order_id       = t501.c501_order_id AND t2020a.c501_order_id     = '"
              + strOrdId + "')  AND B.C1900_COMPANY_ID = '1007' ");

      sbQuery.append(" UNION ");
      sbQuery
          .append(" SELECT t2020a.c2020a_sub_component_num ID , t2020a.c2020a_sub_component_desc PDESC  ");

      if (strAction.equals("Print") || strAction.equals("PrintVoid")) {

        sbQuery
            .append(" , get_order_part_qty_invoice(t502.C501_ORDER_ID,t502.C205_PART_NUMBER_ID,t502.C502_ITEM_PRICE,t502.c901_type,DECODE(t502.C502_CONSTRUCT_FL,NULL,'N',t502.C502_CONSTRUCT_FL),t502.C502_Control_Number) QTY");
        sbQuery.append(" , t502.C502_CONTROL_NUMBER CNUM, t205.c205_product_family prodfamily ");
      } else {

        sbQuery
            .append(" , t502.C502_CONTROL_NUMBER CNUM , t502.C502_ITEM_QTY QTY, t2020a.c2020a_sub_component_id ITEM_ORD_ID ");

      }
      sbQuery.append("  , t2020a.c2020a_sub_component_price PRICE");
      sbQuery
          .append("  , NVL (t502.C502_BASE_PRICE, 0) ACTUALPRICE ,t502.C502_EXT_ITEM_PRICE EXTERNAL_ITEM_PRICE, t501.C501_EXT_TOTAL_COST EXTERNAL_TOTAL_COST ");
      if (strAction.equals("Print") || strAction.equals("PrintVoid")) {

        sbQuery
            .append(" , ROUND (NVL ( (get_order_part_qty_invoice(t502.C501_ORDER_ID,t502.C205_PART_NUMBER_ID,t502.C502_ITEM_PRICE,t502.c901_type,");
      sbQuery.append(" DECODE(t502.C502_CONSTRUCT_FL,NULL,'N',t502.C502_CONSTRUCT_FL),t502.C502_Control_Number) * NVL (t2020a.c2020a_sub_component_price, 0) * NVL (t2020a.c2020a_sub_component_tva, 0) / 100), 0), 2) tax_cost  ");
      } else {
        sbQuery
            .append("  ,  ROUND (NVL ( (t502.C502_ITEM_QTY * NVL (t2020a.c2020a_sub_component_price, 0) * NVL (t2020a.c2020a_sub_component_tva, 0) / 100), 0), 2) tax_cost   ");
      }
      sbQuery
          .append(" , t502.c901_type TYPE , null CONFL  , t501.C501_SHIP_COST SCOST , t501.C501_ORDER_DATE ODT,t501.C501_SURGERY_DATE SURG_DT , t2020a.c2020a_sub_component_tva VAT ");
      sbQuery
          .append(" , t2020a.c2020a_sub_component_lppr NTFTCODE  , NVL (t501.C501_Shipping_Date, '') shipdate , NVL (c2020a_sub_component_price, 0) unitprice , ");
      sbQuery.append(" NVL ( (c502_unit_price_adj_value * - 1), 0) adjval  , DECODE (C901_UNIT_PRICE_ADJ_CODE, NULL, 'N/A', GET_CODE_NAME (c901_unit_price_adj_code)) adjcode , ");
      sbQuery.append(" DECODE (NVL (C502_UNIT_PRICE_ADJ_VALUE, 0), '0', '', 'Y') ADJFLAG  , GET_CODE_NAME_alt (c901_unit_price_adj_code)  ADJCODEALT , NVL (t502.C502_ITEM_PRICE, 0) - NVL (C502_UNIT_PRICE, 0) ADJEA ");
      sbQuery
          .append(" FROM t2020a_sub_cmap_trans_details t2020a , t502_item_order t502 , t501_order t501, t205_part_number t205");
      sbQuery
          .append(" WHERE  t2020a.c2020a_void_fl IS NULL AND t502.c205_part_number_id      = t205.c205_part_number_id AND t502.c502_void_fl IS NULL AND t501.c501_void_fl IS NULL AND t502.c205_part_number_id = t2020a.c205_part_number_id");
      sbQuery.append(" AND t2020a.c501_order_id = t502.c501_order_id  AND t502.c501_order_id = t501.c501_order_id AND t2020a.c501_order_id = '"
              + strOrdId + "' ORDER BY ID ");
      if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
        sbQuery.append(" )");
        sbQuery.append(" WHERE  QTY != 0 AND  ( prodfamily != 26240096 or price != 0)  ORDER BY ID ");
      }
    } else {
      if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
        sbQuery.append("  SELECT * FROM ");
        sbQuery.append(" ( ");
        sbQuery
            .append(" SELECT distinct(A.C205_PART_NUMBER_ID) ID, get_partdesc_by_company(A.C205_PART_NUMBER_ID) PDESC ,T205.C205_PRODUCT_FAMILY ,B.C501_ORDER_DATE DATEOFSALES,");
        sbQuery.append(" B.C501_SURGERY_DATE SURG_DT,GET_ACCOUNT_NAME(B.c704_account_id) ACCTNAME,A.C501_ORDER_ID ORDERID ");

        if (strctrlSplitUp.equals("YES")) { // For BBA the value will be YES, need to get the qty
                                             // based on control number
       	  if (strUsedLotFl.equals("YES")) {
      		sbQuery
            .append(" ,DECODE(B.C901_ORDER_TYPE,'2532',A.C502_ITEM_QTY,get_order_part_qty_invoice(A.C501_ORDER_ID,A.C205_PART_NUMBER_ID,A.C502_ITEM_PRICE ,A.c901_type,DECODE(A.C502_CONSTRUCT_FL,NULL,'N',A.C502_CONSTRUCT_FL),A.C502_Control_Number)) QTY ");
      	  }else{
          sbQuery
              .append(" ,get_order_part_qty_invoice(A.C501_ORDER_ID,A.C205_PART_NUMBER_ID,A.C502_ITEM_PRICE");
          sbQuery
              .append(" ,A.c901_type,DECODE(A.C502_CONSTRUCT_FL,NULL,'N',A.C502_CONSTRUCT_FL),A.C502_Control_Number) QTY");
      	  }
          sbQuery
              .append(" ,ROUND (NVL ( (get_order_part_qty_invoice (A.C501_ORDER_ID, A.C205_PART_NUMBER_ID, A.C502_ITEM_PRICE,");
          sbQuery.append(" A.c901_type, DECODE (A.C502_CONSTRUCT_FL, NULL, 'N', A.C502_CONSTRUCT_FL), A.C502_Control_Number) * NVL (A.C502_ITEM_PRICE, 0) * NVL (a.c502_vat_rate, 0) / 100), 0), 2) tax_cost ");
        } else {
          sbQuery.append(" ,get_invoice_part_qty(A.C501_ORDER_ID, A.C502_ITEM_ORDER_ID) QTY");
          sbQuery
              .append(", get_invoice_part_tax (A.C501_ORDER_ID, A.C502_ITEM_ORDER_ID) tax_cost ");
        }
        sbQuery
            .append(" ,NVL(GET_CUSTOM_PARTNUM(B.C704_ACCOUNT_ID,A.C205_PART_NUMBER_ID),'-9999') CUST_PNUM");
        sbQuery
            .append(" ,NVL(GET_CUSTOM_PARTDESC(B.C704_ACCOUNT_ID,A.C205_PART_NUMBER_ID),'-9999') CUST_PDESC,A.C502_ITEM_PRICE PRICE,");
        sbQuery
            .append(" NVL(A.C502_BASE_PRICE,0) ACTUALPRICE,A.C502_EXT_ITEM_PRICE EXTERNAL_ITEM_PRICE, B.C501_EXT_TOTAL_COST EXTERNAL_TOTAL_COST, ");
      } else {

        sbQuery
            .append(" SELECT A.C205_PART_NUMBER_ID ID, get_partdesc_by_company(A.C205_PART_NUMBER_ID) PDESC, ");

        sbQuery
            .append(" A.C502_ITEM_QTY QTY, A.C502_ITEM_ORDER_ID ITEM_ORD_ID,");
        if (strUsedLotFl.equals("YES")) {
        	 sbQuery
                .append(" decode(B.C901_ORDER_TYPE,'2532',NVL(t502b.c502b_usage_lot_num,''),NVL(A.C502_Control_Number,'')) CNUM,");
          } else {
            sbQuery.append("NVL(A.C502_Control_Number,'') CNUM,");
          }
        sbQuery
            .append("  A.C502_ITEM_PRICE PRICE,NVL(A.C502_BASE_PRICE,0) ACTUALPRICE,A.C502_EXT_ITEM_PRICE EXTERNAL_ITEM_PRICE, B.C501_EXT_TOTAL_COST EXTERNAL_TOTAL_COST,");
        sbQuery
            .append(" NVL(A.c502_tax_amt, ROUND (NVL ( (A.C502_ITEM_QTY * NVL (A.C502_ITEM_PRICE, 0) * NVL (A.c502_vat_rate, 0) / 100), 0), 2)) tax_cost,A.C501_ORDER_ID ORDERID,");
        sbQuery.append(" B.C501_ORDER_DATE DATEOFSALES,B.C501_SURGERY_DATE SURG_DT,GET_ACCOUNT_NAME(B.c704_account_id) ACCTNAME,");
      }
      sbQuery
          .append(" A.c901_type TYPE, A.C502_CONSTRUCT_FL CONFL, get_part_attr_value_by_comp (A.C205_PART_NUMBER_ID,'200001', B.C1900_COMPANY_ID) NTFTCODE, ");
      sbQuery.append(" B.C501_SHIP_COST SCOST, B.C501_ORDER_DATE ODT,NVL(a.c502_vat_rate,0) VAT , NVL(a.c502_igst_rate,0) IGST, NVL(a.c502_sgst_rate,0) SGST, NVL(a.c502_cgst_rate,0) CGST , a.c502_hsn_code HSNCODE ,  NVL(a.C502_TCS_RATE,0) ACCOUNTTCS ");//to get the gst, TCS and HSN code values for India
      if (strctrlSplitUp.equals("YES")) { // For BBA the value will be YES, need to get the qty
        // based on control number
    	  if (strUsedLotFl.equals("YES")) {
          	sbQuery
                  .append(", decode(B.C901_ORDER_TYPE,'2532',NVL(t502b.c502b_usage_lot_num,''),NVL(A.C502_Control_Number,'')) ControlNum");
            sbQuery.append(",NVL(A.C502_Control_Number,'') CNUM");
            } else {
        sbQuery.append(",NVL(A.C502_Control_Number,'') ControlNum");
        sbQuery.append(",NVL(A.C502_Control_Number,'') CNUM");
            }
        sbQuery
            .append(",  gm_pkg_pd_partnumber.get_part_size(A.C205_PART_NUMBER_ID,A.C502_CONTROL_NUMBER) partsize ");
      }
      sbQuery.append(",  NVL(B.C501_Shipping_Date,'') shipdate ");
      sbQuery
          .append(",  NVL(c502_unit_price,0) unitprice, NVL((c502_unit_price_adj_value * -1),0) adjval, DECODE(C901_UNIT_PRICE_ADJ_CODE, NULL, 'N/A', GET_CODE_NAME(c901_unit_price_adj_code)) adjcode ");
      sbQuery.append(",  DECODE(NVL(C502_UNIT_PRICE_ADJ_VALUE,0),'0','','Y') ADJFLAG ");
      sbQuery.append(",  GET_CODE_NAME_alt(c901_unit_price_adj_code) ADJCODEALT ");
      sbQuery.append(",  NVL(A.C502_ITEM_PRICE,0)-NVL(C502_UNIT_PRICE,0) ADJEA ");
      // to set the Unit of Measure values
      if (strShowPartUomFl.equalsIgnoreCase("YES")) {
        sbQuery
            .append(" , get_rule_value_by_company(get_partnum_uom(A.C205_PART_NUMBER_ID), 'INVOICE_PART_UOM', B.C1900_COMPANY_ID) uom ");
      }
      sbQuery.append(" FROM T502_ITEM_ORDER A, T501_ORDER B , T205_PART_NUMBER T205 ");
      if (strUsedLotFl.equals("YES")) {
          sbQuery.append(" ,t502b_item_order_usage t502b, T500_Order_Info t500 ");
        }
      sbQuery.append(" WHERE A.C501_ORDER_ID = '");
      sbQuery.append(strOrdId);
      sbQuery.append("' AND A.C502_VOID_FL IS NULL AND A.C502_DELETE_FL IS NULL ");
      sbQuery.append(" AND A.C501_ORDER_ID = B.C501_ORDER_ID ");
      sbQuery.append(" AND A.C205_PART_NUMBER_ID =  T205.C205_PART_NUMBER_ID ");
      sbQuery.append(" AND B.C1900_COMPANY_ID = '" + strCompanyId + "' ");
      if (strUsedLotFl.equals("YES")) {
    	  sbQuery.append(" AND A.C501_Order_Id         = t500.c501_order_id(+) ");
    	  sbQuery.append(" AND t500.C501_Order_Id         = t502b.c501_order_id(+) ");
    	  sbQuery.append(" AND A.c500_order_info_id    = t500.c500_order_info_id(+) ");
          sbQuery.append(" AND t500.c500_order_info_id = t502b.c500_order_info_id(+) ");
    	  sbQuery.append(" AND t502b.C502B_VOID_FL (+)   IS NULL ");
    	  sbQuery.append(" AND B.C501_VOID_FL      IS NULL ");
    	  sbQuery.append(" AND A.C502_VOID_FL      IS NULL ");
    	  sbQuery.append(" AND t500.C500_VOID_FL (+)     IS NULL ");
        }

      sbQuery.append(" ORDER BY A.C205_PART_NUMBER_ID ");

      if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
        sbQuery.append(" )");
        sbQuery.append(" WHERE  QTY != 0 AND  ( c205_product_family != 26240096 or price != 0)  ORDER BY  CUST_PNUM,CUST_PDESC,ID ");
        if (strctrlSplitUp.equals("YES")) {
          sbQuery.append(" , ControlNum ");
          sbQuery.append(" , CNUM ");
        }

      }

    }

    log.debug(" Query formed for loadOrderItemSQL is " + sbQuery.toString());
    return (sbQuery.toString());
  }

  /**
   * @param strOrdId
   * @return String
   */
  public String loadOrderConstructSQL(String strOrdId) {
    StringBuffer sbQuery = new StringBuffer();
    // To get the Construct Pricing Info for the Order
    sbQuery
        .append(" SELECT T741.C741_CONSTRUCT_NM CONID, T741.C741_CONSTRUCT_DESC CONNM ,C512_CONSTRUCT_VALUE CVALUE");
    sbQuery.append(" FROM T512_ORDER_CONSTRUCT T512, T741_CONSTRUCT T741 ");
    sbQuery.append(" WHERE C501_ORDER_ID = '");
    sbQuery.append(strOrdId);
    sbQuery.append("' AND C512_VOID_FL IS NULL ");
    sbQuery.append(" AND T512.C741_CONSTRUCT_ID = T741.C741_CONSTRUCT_ID ");
    return (sbQuery.toString());
  }

  /**
   * filterInvoiceData - This method will do the calculations for invoice print.
   * 
   * @param HashMap hmInData
   * @return HashMap
   * @exception AppError
   */
  public HashMap filterInvoiceData(HashMap hmInData) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmOutData = new HashMap();
    ArrayList alOrderNums = new ArrayList();
    HashMap hmCartDetails = new HashMap();
    HashMap hmConstructs = new HashMap();
    ArrayList alConsLoop = null;
    HashMap hmConsLoop = null;
    hmReturn = GmCommonClass.parseNullHashMap((HashMap) hmInData.get("hmReturn"));
    HashMap hmOrderDetails = new HashMap();
    ArrayList alPaidMonth = new ArrayList();
    String strPayNm = "";
    String strOrdId = "";
    String strInvNum = "";
    String strParentInvId = "";
    String strCreditInvId = "";
    String strPartyFl = "";
    String strInvoiceNote = "";
    String strPayLbl = "";
    String strDiscLbl = "";
    String strCompId = "";
    String strInvComments = "";
    String strTaxStartDateFl = "";
    String strVAFSSFlag= "";
    String strInvoiceAdj = "";
    String strAdjStartDateFl = "";
    String strInvoiceShippingCost = "";
    
    double dbInvoiceAdjTotal = 0.0;
    double dbOverAllAdjTotal = 0.0;
    String strAccId = "";
    String strAccType = "";
    String strVatExcFl = "";
    String strBillCountry = "";
    
    java.sql.Date dtOrderDate = null;
    //getting the datastoreVO from hashmap because it is calling through interface 
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmInData.get("gmDataStoreVO");
    String strCompanyLocale = (String) hmInData.get("CompanyLocale");
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    String strInvoiceCompanyId = GmCommonClass.parseNull((String) hmInData.get("CompanyId"));
    GmInvoicePrintInterface gmInvoicePrintInterface =
        (GmInvoicePrintInterface) GmInvoiceBean.getInvoicePrintClass(strInvoiceCompanyId);
    GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(gmDataStoreVO);
    HashMap hmPrintInvDtls = gmInvoicePrintInterface.fetchInvoiceDtls(hmInData);
    hmReturn.putAll(hmPrintInvDtls);
    alPaidMonth = GmCommonClass.parseNullArrayList(((ArrayList) hmInData.get("ALPAIDMONTH")));
    if (hmReturn != null) {
      hmOrderDetails = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("ORDERDETAILS"));
      
      hmOrderDetails.putAll(hmPrintInvDtls);
      // Account Currency based date format
      String strAccountCurrencyDateFmt =
          GmCommonClass.parseNull((String) hmOrderDetails.get("ACC_CURR_DATEFMT"));
      strAccountCurrencyDateFmt =
          strAccountCurrencyDateFmt.equals("") ? gmDataStoreVO.getCmpdfmt()
              : strAccountCurrencyDateFmt;
      hmOrderDetails.put("INVDT", GmCommonClass.getStringFromDate(
          (java.sql.Date) hmOrderDetails.get("INVDT"), strAccountCurrencyDateFmt));

      strAccId = GmCommonClass.parseNull((String) hmOrderDetails.get("ACCID"));
      strBillCountry = GmCommonClass.parseNull((String) hmOrderDetails.get("BILLCNTRY"));
      
      strAccType =
          GmCommonClass.parseNull(GmCommonClass.getAccountAttributeValue(strAccId, "5504"));
    // exclude vat rate for company by account type 
      strVatExcFl =
          GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strAccType,
              "EXC_VAT_RATE", strInvoiceCompanyId));
     
      hmOrderDetails.put("PARTYFLAG", GmCommonClass.parseNull((String) hmReturn.get("PARTYFLAG")));
      hmOrderDetails.put("PARTYNAME", GmCommonClass.parseNull((String) hmReturn.get("PARTYNAME")));
      hmOrderDetails.put("HEADERADDRESS",
          GmCommonClass.parseNull((String) hmReturn.get("HEADERADDRESS")));
      hmOrderDetails.put("REMITADDRESS",
          GmCommonClass.parseNull((String) hmReturn.get("REMITADDRESS")));
      hmOrderDetails.put("REMITMAILADDRESS",
          GmCommonClass.parseNull((String) hmReturn.get("REMITMAILADDRESS")));
      hmOrderDetails.put("INVOICETITLE",
          GmCommonClass.parseNull((String) hmInData.get("INVOICETITLE")));
      hmOrderDetails.put("LOGOIMAGE", GmCommonClass.parseNull((String) hmReturn.get("LOGOIMAGE")));
      hmOrderDetails.put("SEALIMAGE", GmCommonClass.parseNull((String) hmReturn.get("SEALIMAGE")));

      alOrderNums = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ORDERNUMS"));
      hmCartDetails = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("CARTDETAILS"));
      strPartyFl = GmCommonClass.parseNull((String) hmReturn.get("PARTYFLAG"));
      strInvNum = GmCommonClass.parseNull((String) hmOrderDetails.get("INV"));
      strParentInvId = GmCommonClass.parseNull((String) hmOrderDetails.get("PARENT_INV_ID"));
      strInvoiceNote = GmCommonClass.parseNull((String) hmOrderDetails.get("INVOICENOTE"));
      strCompId = GmCommonClass.parseNull((String) hmOrderDetails.get("COMPID"));
      strPayNm = GmCommonClass.parseNull((String) hmOrderDetails.get("PAYNM"));
      strPayNm = strPayNm.equals("") ? "Net 30 Days" : strPayNm;
      strInvComments = GmCommonClass.parseNull((String) hmOrderDetails.get("COMMENTS"));
      strTaxStartDateFl = GmCommonClass.parseNull((String) hmOrderDetails.get("TAX_START_DATE_FL"));
      strAdjStartDateFl = GmCommonClass.parseNull((String) hmOrderDetails.get("ADJ_START_DATE_FL"));
      strVAFSSFlag=GmCommonClass.parseNull((String) hmOrderDetails.get("VAFSSACCOUNT"));//PMT-22387-VA/FSS Account
     
      // strInvoiceAdj = GmCommonClass.parseNull((String) hmOrderDetails.get("TOTALINVADJ"));
      // //Total invoice adjustment
      // dbInvoiceAdjTotal = Double.parseDouble(strInvoiceAdj);
      // /log.debug("strInvoiceAdj>>>" + strInvoiceAdj);
      hmOrderDetails.put("PAYNM", strPayNm);
      hmOrderDetails.put("GLOBUSVATNUM",
          GmCommonClass.parseNull((String) hmOrderDetails.get("GLOBUSVATNUM")));
      hmOrderDetails.put("COUNTRYVATNUM", GmCommonClass.parseNull((String) hmInData.get("NIP")));
      // PC-3929: To get shipping cost from Invoice table
      strInvoiceShippingCost = GmCommonClass.parseZero((String) hmOrderDetails.get("INVOICE_SHIP_COST"));      
      log.debug("Out values strInvoiceShippingCost ==> "+ strInvoiceShippingCost);
    }

    int intSize = alOrderNums.size();
    int intLoop = 0;
    ArrayList alLoop = new ArrayList();
    // ArrayList allList = new ArrayList();
    ArrayList alHashMap = new ArrayList();
    HashMap hmTemp = new HashMap();
    HashMap hmLoop = new HashMap();
    String strVATPER = "";
    String strPartVat = "";
    String strConFlag = "";
    String strConstructId = "";
    String strConstructNm = "";
    String strConstructValue = "";
    double vatPer = 0.0;
    double partVatPer = 0.0;
    String strItems = "";
    double intQty = 0.0;
    double dbItemTotal = 0.0;
    double dbItemUnitPriceTotal = 0.0;
    double dbItemListPriceTotal = 0.0;
    double dbTotalItemVAT = 0.0;
    double dbTotalVATByPart = 0.0;
    double dbGrossTotal = 0.0;
    double dbTotal = 0.0;
    double dblShip = 0.0;
    double dbOverAllTotal = 0.0;
    double dbOverAllShip = 0.0;
    double dbOverAllSubTotal = 0.0;
    double dbOverAllListPriceTotal = 0.0;
    double dbOverAllSubUPriceTotal = 0.0;
    double dbItemTotaltemp = 0.0;
    double dbItemVATTotaltemp = 0.0;
	double dbVatTotaltemp = 0.0;

    String strItemTotal = "";
    String strUnitPriceTotal = "";
    String strTotal = "";
    String strPrice = "";
    String strUnitPrice = "";
    String strListPrice = "";
    String strQty = "";
    String strNumInWords = "";
    String strOrderComments = "";
    String strPartDesc = "";
    int intConLoop = 0;
    String strSpace = "";
    String strOrderType = "";
    String strRAId = "";
    String strCurrency = "";
    String strTermsTitle = "";
    String strTitle = "";
    String strTaxCost = "";
    String strExclCmpny =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strInvoiceCompanyId, "EXCLUDECMPNY"));
    // Get rule value to show/hide shipping charges from invoice details section
    String strShipFieldFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SHIPCHRG", "ENABLECOST",
            strInvoiceCompanyId));
    
    if (intSize == 0) {
      /*
       * hmLoop.put( "ORDERID", "-"); hmLoop.put( "ORDERDATE", "-"); hmLoop.put( "ITEMTOTAL", "-");
       * hmLoop.put( "VATPER", "-"); hmLoop.put( "ID", "-"); hmLoop.put( "PDESC", "-"); hmLoop.put(
       * "QTY", "0"); hmLoop.put( "PRICE", "0");
       */
      strCurrency = "$";
      hmOrderDetails.put("CURRENCY", strCurrency);
      hmOrderDetails.put("GROSSTOTAL", dbGrossTotal);
      hmOrderDetails.put("TOTALVAT", dbTotalItemVAT);
      hmOrderDetails.put("NETTOTAL", dbTotal);
      // alHashMap.add(hmLoop);
    }
    // PMT-51405--- Based on the item order line item, putting the Sales tax value and tax amount in the HashMap
    // In case if the order is having only 10% ,then it won't put the  8% keys (TOTALSALESTAX8) in HashMap 
    // due to this, not able to check the equals or null condition in Jasper, That's why Setting the initial values 
        hmOrderDetails.put("TOTALSALESTAX10", 0.0);
        hmOrderDetails.put("TOTALSALESTAX8", 0.0);
        hmOrderDetails.put("TOTALPRICE10", 0.0);
        hmOrderDetails.put("TOTALPRICE8", 0.0);
    //  PC-1519:Not able to check the equals or null condition in Jasper, That's why Setting the initial values 
        hmOrderDetails.put("TOTALPRICE7", 0.0);
        hmOrderDetails.put("TOTALPRICE19", 0.0);

    for (int i = 0; i < intSize; i++) {
      hmTemp = (HashMap) alOrderNums.get(i);
      strOrdId = (String) hmTemp.get("ID");
      dtOrderDate = (java.sql.Date) hmTemp.get("ORDERDATE");
      strOrderComments = GmCommonClass.parseNull((String) hmTemp.get("CMENT"));
      if (strOrdId.endsWith("S") != true) // remove return order informations
      {
        strOrderComments = GmCommonClass.parseNull((String) hmTemp.get("CMENT"));
        strOrderType = GmCommonClass.parseNull((String) hmTemp.get("ORDERTYPE"));
        strRAId = GmCommonClass.parseNull((String) hmTemp.get("RMAID"));
        // L to C swap (back order)
        if (strOrderType.equals("2525") && !strRAId.equals("")) {
          continue;
        }
        alLoop = GmCommonClass.parseNullArrayList((ArrayList) hmCartDetails.get(strOrdId));
        intLoop = alLoop.size();
        ArrayList alLoop1 = alLoop;
        // Below code is for construct pricing.
        alConsLoop = GmCommonClass.parseNullArrayList((ArrayList) hmConstructs.get(strOrdId));
        intConLoop = alConsLoop.size();
        if (intConLoop > 0) {
          for (int j = 0; j < intConLoop; j++) {
            hmConsLoop = (HashMap) alConsLoop.get(j);
            strConstructId = (String) hmConsLoop.get("CONID");
            strConstructNm = (String) hmConsLoop.get("CONNM");
            strConstructValue = (String) hmConsLoop.get("CVALUE");
            dbItemTotal = Double.parseDouble(strConstructValue);
            // dbTotal = dbTotal + dbItemTotal;

            strOrdId = "";
          }
        }
        
        for (int j = 0; j < intLoop; j++) {
          hmLoop = (HashMap) alLoop.get(j);
          strPartDesc =
              GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String) hmLoop.get("PDESC")));
          strVATPER = GmCommonClass.parseNull((String) hmLoop.get("VAT"));
          strTaxCost = GmCommonClass.parseZero((String) hmLoop.get("TAX_COST"));
          if (!strVATPER.equals("")) {
            vatPer = Double.parseDouble(strVATPER) / 100;
          }
          strPrice = GmCommonClass.parseNull((String) hmLoop.get("PRICE"));
          strUnitPrice = GmCommonClass.parseZero((String) hmLoop.get("UNITPRICE"));
          strQty = GmCommonClass.parseNull((String) hmLoop.get("QTY"));
          strListPrice = GmCommonClass.parseZero((String) hmLoop.get("LISTPRICE"));
          intQty = Double.parseDouble(strQty);
          dbItemTotal = Double.parseDouble(strPrice);
          dbItemUnitPriceTotal = Double.parseDouble(strUnitPrice);
          dbItemListPriceTotal = Double.parseDouble(strListPrice);
          dbItemTotal = GmCommonClass.roundDigit((intQty * dbItemTotal), 2); // Multiply by Qty
          dbItemUnitPriceTotal = GmCommonClass.roundDigit((intQty * dbItemUnitPriceTotal), 2); // Multiply
                                                                                               // by
                                                                                               // Qty
          dbItemListPriceTotal = GmCommonClass.roundDigit((intQty * dbItemListPriceTotal), 2);
          if (!strVATPER.equals("0")) {
            dbTotalItemVAT = dbTotalItemVAT + Double.parseDouble(strTaxCost);
			
				// start > PMT-35627 Japan Invoice Paperwork to Include VAT Rate
						dbVatTotaltemp = Double.parseDouble(strTaxCost)
								+ GmCommonClass
										.parseDouble(((Double) hmOrderDetails
												.get("TOTALSALESTAX"
														+ strVATPER)));

						hmOrderDetails.put("TOTALSALESTAX" + strVATPER,
								dbVatTotaltemp);

						// < cod end;
						
            dbItemTotaltemp =
                dbItemTotal
                    + GmCommonClass.parseDouble(((Double) hmOrderDetails.get("TOTALPRICE"
                        + strVATPER)));
            hmOrderDetails.put("TOTALPRICE" + strVATPER, dbItemTotaltemp);

            dbItemVATTotaltemp =
                GmCommonClass.roundDigit((dbItemTotal * vatPer), 2)
                    + GmCommonClass.parseDouble(((Double) hmOrderDetails.get("TOTALVATPRICE"
                        + strVATPER)));
            hmOrderDetails.put("TOTALVATPRICE" + strVATPER, dbItemVATTotaltemp);
          }

        // vat percenatge is zero and exclude vat rate flag is Y , calculate vat rate to display in invoice
          if (strVATPER.equals("0") && strVatExcFl.equals("Y")) {
           strPartVat = GmCommonClass.parseZero((String) hmLoop.get("PART_VAT_RATE"));
            
            if (!strPartVat.equals("0")) {
              partVatPer = Double.parseDouble(strPartVat) / 100;
            }
            
            dbTotalVATByPart =
                dbTotalVATByPart + GmCommonClass.roundDigit((dbItemTotal * partVatPer), 2);
            
            dbItemTotaltemp =
                dbItemTotal
                    + GmCommonClass.parseDouble(((Double) hmOrderDetails.get("TOTALPRICE"
                        + strPartVat)));
            hmOrderDetails.put("TOTALPRICE" + strPartVat, dbItemTotaltemp);

            dbItemVATTotaltemp =
                GmCommonClass.roundDigit((dbItemTotal * partVatPer), 2)
                    + GmCommonClass.parseDouble(((Double) hmOrderDetails.get("TOTALVATPRICE"
                        + strPartVat)));
            hmOrderDetails.put("TOTALVATPRICE" + strPartVat, dbItemVATTotaltemp);

          }

          strItemTotal = "" + dbItemTotal;
          strUnitPriceTotal = "" + dbItemUnitPriceTotal;
          dbTotal = dbTotal + dbItemTotal;

          strTotal = "" + dbTotal;
          hmLoop.put("ORDERID", strOrdId);
          hmLoop.put("ORDERDATE", new java.util.Date(dtOrderDate.getTime()));
          hmLoop.put("ITEMTOTAL", strItemTotal);
          hmLoop.put("ITEMUNITPRICETOTAL", strUnitPriceTotal);
          
          if (strVATPER.equals("0") && strVatExcFl.equals("Y")) {
            hmLoop.put("VATPER", strPartVat.equals("0") ? "-" : strPartVat);
          } else {
            hmLoop.put("VATPER", strVATPER.equals("0") ? "-" : strVATPER);
          }

 
          hmLoop.put("PDESC", strPartDesc);
          hmLoop.put("ACTUALPRICE", strPrice);

          alHashMap.add(hmLoop);
          // Shipping information by orderwise.
          if (j == intLoop - 1) {
            HashMap hmLoopShipInfo = new HashMap();
            String strShipCost = GmCommonClass.parseZero((String) hmLoop.get("SCOST"));
            dblShip = Double.parseDouble(strShipCost);
            // PC-3929: Shipping cost get from Invoice table.
            if ((strTaxStartDateFl.equalsIgnoreCase("Y") || strAdjStartDateFl.equalsIgnoreCase("Y"))
                && (!strShipFieldFl.equals("Y"))) {
//            	when order create that selected without quantity(back order) part we need to add shipping cost only one time  
            	dbOverAllShip = Double.parseDouble(strInvoiceShippingCost);
            } else {
//              when we order create that time we select with quantity part and without quantity(back order) part  we need to add shipping cost only one time
//            	PC-5871 - below code added for BBA invoice total is 0.
            	dbTotal = dbTotal + Double.parseDouble(strInvoiceShippingCost);
            }

            strTotal = "" + dbTotal;
            hmLoopShipInfo.put("ORDERID", strOrdId);
            hmLoopShipInfo.put("ID", "");
            hmLoopShipInfo.put("PDESC", "Shipping Charges ");
            hmLoopShipInfo.put("QTY", "1");
            hmLoopShipInfo.put("PRICE", strShipCost);

            hmLoopShipInfo.put("CUST_PNUM", "-9999");
            hmLoopShipInfo.put("CUST_PDESC", "-9999");

            if ((strShipFieldFl.equals("Y"))
                || (!(strTaxStartDateFl.equalsIgnoreCase("Y") || strAdjStartDateFl
                    .equalsIgnoreCase("Y")))) {
              alHashMap.add(hmLoopShipInfo);
            }
          }
          // Tax
          dbOverAllSubTotal = dbOverAllSubTotal + dbItemTotal;
          dbOverAllListPriceTotal = dbOverAllListPriceTotal + dbItemListPriceTotal;
          dbOverAllSubUPriceTotal = dbOverAllSubUPriceTotal + dbItemUnitPriceTotal;
        }

        dbGrossTotal = dbTotalItemVAT + dbTotal;
        strNumInWords =
            GmCommonClass.parseNull(GmCommonClass.convetToPolishWords(dbGrossTotal, getCompId()));
        hmOrderDetails.put("GROSSTOTAL", dbGrossTotal);

        if (strVATPER.equals("0") && strVatExcFl.equals("Y")) {
          hmOrderDetails.put("TOTALVAT", dbTotalVATByPart);
        } else {
          hmOrderDetails.put("TOTALVAT", dbTotalItemVAT);
        }

        strCurrency = GmCommonClass.parseNull((String) hmOrderDetails.get("CURRENCY"));
        if (strCurrency.equals("USD") || strCurrency.equals("")) {
          strCurrency = "$";
        }
        hmOrderDetails.put("CURRENCY", strCurrency);
        hmOrderDetails.put("NETTOTAL", dbTotal);
        hmOrderDetails.put("TOTALINWORDS", strNumInWords);
      }
    } // end of for loop
    
    // Total Adjustments of the invoice [Total After adjustment - Total Before Adjustment]
    dbInvoiceAdjTotal = dbOverAllSubTotal - dbOverAllSubUPriceTotal;
    // Sales Tax Total Section
    dbOverAllTotal = dbOverAllSubTotal + dbOverAllShip + dbTotalItemVAT;
    // Total after reducing the adjustments and adding the shipping charge and vat amount
    dbOverAllAdjTotal =
        dbOverAllSubUPriceTotal + dbOverAllShip + dbTotalItemVAT + dbInvoiceAdjTotal;
    // /dbOverAllAdjTotal = dbOverAllTotal + dbInvoiceAdjTotal;
    hmOrderDetails.put("SUBTOTAL", dbOverAllSubTotal); //
    hmOrderDetails.put("LIST_PRICE_SUB_TOTAL", dbOverAllListPriceTotal); 
    hmOrderDetails.put("SUBUNITPRICETOTAL", dbOverAllSubUPriceTotal);
    hmOrderDetails.put("SHIPCHARGE", dbOverAllShip);
    hmOrderDetails.put("SALESTAX", dbTotalItemVAT);
    hmOrderDetails.put("OVERALLTOTAL", dbOverAllTotal);
    hmOrderDetails.put("OVERALLADJTOTAL", dbOverAllAdjTotal);
    hmOrderDetails.put("TOTALINVADJ", dbInvoiceAdjTotal);
    log.debug(" dbInvoiceAdjTotal --> " + dbInvoiceAdjTotal);
    if (!strCompId.equals("1000") && !strCompId.equals("1001")) {
      // OUS code - to setting the invoice amount in word
     if(!strCompId.equals("1009")&& !strCompId.equals("1011")) {
      HashMap hmInvAmount = gmInvoiceBean.fetchInvAmountInWord(hmOrderDetails);
      hmOrderDetails.putAll(hmInvAmount);
     }
    }

    String strInvType = GmCommonClass.parseNull((String) hmOrderDetails.get("INVTYPE"));
    if ((strInvType.equals("50202") || strInvType.equals("50203"))) {
      strCreditInvId = strInvNum;
      strInvNum = strParentInvId;
    }
    String strVoidFlag = GmCommonClass.parseNull((String) hmInData.get("hVoidFlag"));
    String strWaterMark = "";

    if (strVoidFlag.equals("VoidInvoice")) {
      strWaterMark = "voidinvoice";
    }
    if (strInvType.equals("50201")) {
      strWaterMark = "obo";
    }
    hmOrderDetails.put("WATERMARK", strWaterMark);
    String strTermsField = "";
    // String strTermsTitle = "";//(strInvType.equals("50202")) ? "Credit Memo # ":"Terms";
    // Added Debit memo.
    String strCountryRuleValue = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strBillCountry,"VAT_RATE_BY_CNTRY_FL"));
    //PC-4047: Invoice Luxembourg customers in GM Belgium - to get termstitle for Luxembourg
    if (strInvType.equals("50202")) {
    	if(strCountryRuleValue.equals("Y")) {
    		strTermsTitle =
    		          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.CREDIT_MEMO_LUX_LBL"));
    	}else {
    	      strTermsTitle =
    	          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.CREDIT_MEMO_LBL"));
    	}
      strTermsField = strCreditInvId;
    } else if (strInvType.equals("50203")) {
    	if(strCountryRuleValue.equals("Y")) {
    		strTermsTitle =
    		          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.DEBIT_MEMO_LUX_LBL"));
    	}else {
    		strTermsTitle =
    				GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.DEBIT_MEMO_LBL"));
    	}
      strTermsField = strCreditInvId;
      strTitle = "Debit Memo";
    } else {
      strTermsTitle = "Terms";
      strTermsField = strPayNm;
    }
    // String strTermsField = (strInvType.equals("50202")) ? strCreditInvId : strPayNm;

    hmOrderDetails.put("TERMSTITLE", strTermsTitle);
    hmOrderDetails.put("TERMSFIELD", strTermsField);
    hmOrderDetails.put("INV", strInvNum);

    String strCompNm = "Globus Medical";


    if (strTaxStartDateFl.equalsIgnoreCase("Y") || strAdjStartDateFl.equalsIgnoreCase("Y")) {
   	 if (strVAFSSFlag.equals("Y")) {//PMT-22387-VA/FSS Account
   	        strCompNm = "Globus Medical, Inc.";
      }else{
        strCompNm = "Globus Medical North America, Inc.";
   	      }
   }
    String strPayLblCompNm =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.PAYLABEL"));

    strCompNm = strPayLblCompNm.equals("") ? strCompNm : strPayLblCompNm;

    if (!strPartyFl.equals("Y")) {
      strPayLbl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.PAYLBLTXT"));
      strPayLbl = strPayLbl.replace("<<COMP_NAME>>", strCompNm);
    }
    if (strInvoiceNote.equals("Y")) {
      strDiscLbl =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.DISCOUNTLBLTXT"));
    }
    hmOrderDetails.put("PAYLBL", strPayLbl + strDiscLbl);
    if (strInvComments.equalsIgnoreCase("")) {
      hmOrderDetails.put("COMMENTS", strOrderComments);
    } else {
      hmOrderDetails.put("COMMENTS", strInvComments);
    }
    if (alPaidMonth.size() != 0) {
      for (int i = 0; i < alPaidMonth.size(); i++) {
        HashMap hmPaidMonth = GmCommonClass.parseNullHashMap((HashMap) alPaidMonth.get(i));
        alHashMap.add(hmPaidMonth);
      }
    }
    //PMT-51405 Converting Total Tax Values and Price to String (to convert into bigdecimal in jasper report) 
    hmOrderDetails.put("TOTALSALESTAX10",String.valueOf(hmOrderDetails.get("TOTALSALESTAX10")));
    hmOrderDetails.put("TOTALSALESTAX8",String.valueOf(hmOrderDetails.get("TOTALSALESTAX8")));
    hmOrderDetails.put("TOTALPRICE10",String.valueOf(hmOrderDetails.get("TOTALPRICE10")));
    hmOrderDetails.put("TOTALPRICE8",String.valueOf(hmOrderDetails.get("TOTALPRICE8")));
    
    
    hmOrderDetails.put("SUBREPORT_DIR",
        GmCommonClass.parseNull((String) hmReturn.get("SUBREPORT_DIR")));
    hmOrderDetails.put("REPORTSIZE", alHashMap.size());
    hmOrderDetails.put("RECORDCOUNT", alHashMap.size());
    hmOutData.put("HMORDERDETAILS", hmOrderDetails);
    hmOutData.put("ALORDERLIST", alHashMap);
    hmOutData.put("DISPATCHTO", GmCommonClass.parseNull((String) hmPrintInvDtls.get("DISPATCHTO")));
	log.debug("hmOrderDetails >>>>>>>>>>" + hmOrderDetails);
    return hmOutData;
  }

  /**
   * loadInvoiceDetails - This method gets Invoice information for the selected PO and for the
   * selected Account
   * 
   * @param String strPO
   * @param String strAccId
   * @param String strAction
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadInvoiceDetails(String strInvoiceId, String strAction) throws AppError {


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    HashMap hmInvSrc = new HashMap();
    String strInvSrc = "";
    StringBuffer sbQuery1 = new StringBuffer();
    sbQuery1.setLength(0);
    // Query to get the Invoice Source, If the invoice source is Dealer get the Invoice Detail based
    // on the Dealer else by Account.
    sbQuery1
        .append(" SELECT c901_invoice_source INV_SRC FROM t503_invoice WHERE c503_invoice_id = '");
    sbQuery1.append(strInvoiceId);
    sbQuery1.append("' AND c503_void_fl IS NULL ");

    hmInvSrc = gmDBManager.querySingleRecord(sbQuery1.toString());
    strInvSrc = GmCommonClass.parseNull((String) hmInvSrc.get("INV_SRC"));
    log.debug("strInvSrc==" + strInvSrc);

    GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
    GmPartPriceAdjRptBean gmPartPriceAdjRptBean = new GmPartPriceAdjRptBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmOrderDetails = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alLoop = new ArrayList();
    ArrayList alPayDetails = new ArrayList();
    ArrayList alDOControlNum = new ArrayList();

    String strOrdId = "";
    String strType = "";
    String strAttentionto = "";
    String strTaxDate = "";
    String strAdjustmentDate = "";

    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();

    HashMap hmResult = new HashMap();
    HashMap hmConstruct = new HashMap();
    HashMap hmAdjDtls = new HashMap();
    HashMap hmParty = new HashMap();
    HashMap hmPayment = new HashMap();
    HashMap hmLoop = new HashMap();

    strTaxDate = GmCommonClass.getRuleValue("TAX_DATE", "TAX_INVOICE");
    // To get the date to check whether the adjustments can be done or not
    strAdjustmentDate =
        GmCommonClass
            .parseNull(GmCommonClass.getRuleValue("ADJUSTMENT_DATE", "ADJUSTMENT_INVOICE"));
    log.debug(" INvoice ID is " + strInvoiceId + " Action is " + strAction);
    StringBuffer sbQuery = new StringBuffer();
    // Below code to fetch the Order List for the selected PO and
    // Account

    alReturn = gmDBManager.queryMultipleRecords(loadInvoiceSQL(strInvoiceId, strAction));
    hmReturn.put("ORDERNUMS", alReturn);
    log.debug(" Values inside hmreturn s " + hmReturn);
    log.debug(" INout Values are  " + strInvoiceId + " Action is " + strAction);

    int intLength = 0;
    intLength = alReturn.size();
    log.debug(" size of alReturn is " + intLength);
    if (intLength > 0) {
      hmTemp = (HashMap) alReturn.get(0);
      strOrdId = (String) hmTemp.get("ID");
      strType = GmCommonClass.parseNull((String) hmTemp.get("TYPE"));
      log.debug(" Order ID is " + strOrdId);
      hmReturn.put("ID", strOrdId);
      hmReturn.put("ORDERDATE", hmTemp.get("ORDERDATE"));
      hmReturn.put("ORDERMODE", GmCommonClass.parseNull((String) hmTemp.get("ORDERMODE")));
      hmReturn.put("CREATEDBY", GmCommonClass.parseNull((String) hmTemp.get("CREATEDBY")));
      hmReturn.put("SHIPMODE", GmCommonClass.parseNull((String) hmTemp.get("SHIPMODE")));
      hmReturn.put("ORDER_COMMENT", GmCommonClass.parseNull((String) hmTemp.get("CMENT")));
    }
    hmReturn.put("ORDER_ATTRIB", loadOrderAttribute(strOrdId, strType));
    // Usage LOT code section value get here
    alDOControlNum = gmDOBean.fetchDOControlNumDetails(strOrdId);
    hmReturn.put("DOCONTROLNUMS", alDOControlNum);
    hmReturn.put("DOCONTROLNUMSSIZE", alDOControlNum.size());
    // The Below Attention to variable is added for PMT-6402, In BBA value will be Y and in US value
    // will be empty.
    strAttentionto =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("SWAP_ATTN_TO", "SHIP_ADDRESS"));

    // Invoice Header Details
    sbQuery.setLength(0);
    // If invoice sources is Dealer (26240213) , then fetching the invoice details based on dealer
    // else fetching based on Account
    if (strInvSrc.equals("26240213")) {

      sbQuery.append(" SELECT t101.c101_party_id ACCID");
      sbQuery
          .append(" , DECODE('"
              + getComplangid()
              + "',103097,NVL(t101.c101_party_nm_en,t101.c101_party_nm),t101.c101_party_nm) REPDISTNM ");
      sbQuery.append(", GET_DEALER_BILL_ADD (t101.c101_party_id) BILLADD ");
      sbQuery
          .append(" ,t101p.c901_payment_terms PAY, GET_CODE_NAME(t101p.c901_payment_terms) PAYNM ");
      sbQuery
          .append(" , get_acct_curr_symb_by_dealer(t101.c101_party_id) CURRENCY, get_acct_curr_id_by_dealer (t101.c101_party_id) CURRENCYID,");
      sbQuery.append(" get_acct_curr_symb_by_dealer (t101.c101_party_id)  CURRSIGN ");
      sbQuery.append(" , t101p.c901_invoice_layout invoicelayout");
      sbQuery.append(" , t101p.c101_vendor_id VENDORID");
      sbQuery
          .append(" ,get_party_name(t101.c101_party_id)  ACCOUNTNAME,GET_DEALER_BILL_ADD(t101.c101_party_id)  ACCADDRESS");


    } else {

      sbQuery.append(" SELECT t704.C704_ACCOUNT_ID ACCID, t704.C704_BILL_COUNTRY BILLCNTRY ,t704.C704_CUSTOMER_REF_ID CUSTREFID ");
      sbQuery.append(" , GET_DIST_REP_NAME(GET_REP_ID(t704.C704_ACCOUNT_ID)) REPDISTNM ");
      sbQuery.append(" , GET_BILL_ADD(t704.C704_ACCOUNT_ID) BILLADD ");
      sbQuery.append(" , t704.C704_PAYMENT_TERMS PAY, GET_CODE_NAME(t704.C704_PAYMENT_TERMS) PAYNM ");
      sbQuery
          .append(" ,DECODE(get_account_attrb_value (t704.c704_account_id,91987),'Y','2587','') THIRDPARTYID ");
      sbQuery.append(" ,GET_RULE_VALUE(t704.C704_ACCOUNT_ID, 'THIRD-PARTY-DATE') THIRDPARTYDT");
      sbQuery.append(" ,GET_RULE_VALUE(t704.C704_ACCOUNT_ID, 'INVOICE-NOTE') INVOICENOTE ");
      sbQuery.append(" ,t704.C901_COMPANY_ID COMPID ");
      sbQuery
          .append(" ,DECODE (get_distributor_type (get_account_dist_id (t704.c704_account_id)), 70106, NVL (get_rule_value (50218, get_distributor_inter_party_id (get_account_dist_id (t704.c704_account_id))), get_code_name (t704.C901_CURRENCY)), get_code_name (t704.C901_CURRENCY)) CURRENCY, t704.C901_CURRENCY CURRENCYID");
      sbQuery
          .append(" ,DECODE(get_code_name(GET_ACCOUNT_ATTRB_VALUE(t704.c704_account_id,5507)),' ',get_code_name(t704.C901_CURRENCY), get_code_name(GET_ACCOUNT_ATTRB_VALUE(t704.c704_account_id,5507))) CURRSIGN ");
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE (t704.c704_account_id, 10304532) invoicelayout");
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE (t704.c704_account_id, 106642) VENDORID");
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE(t704.C704_ACCOUNT_ID, 101192) EANLOCATION");
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE(t704.C704_ACCOUNT_ID, 106861) VAFSSACCOUNT");
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE(t704.C704_ACCOUNT_ID, 101188) ACCVATTAXVAL");
      //     // 101185 - VAT # For Account   get Account CF number  for Italy
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE(t704.C704_ACCOUNT_ID, 101185) ACCOUNTVATNUM");
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE(t704.C704_ACCOUNT_ID, 107345) ACCOUNTCFNUM");
      sbQuery
          .append(" ,get_rule_value_by_company(t704.C901_CURRENCY, 'ACC_CURR_DATEFMT', t503.C1900_COMPANY_ID) acc_curr_datefmt ");
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE (t704.c704_account_id, 10306194) CIG ,  GET_CODE_NAME(t704.c704_ship_state) SHIPSTATE"); // to get shipped state value
      sbQuery
          .append(" , GET_ACCOUNT_ATTRB_VALUE (t704.c704_account_id, 5504) CUST_TYPE ,GET_ACCOUNT_NAME(t704.c704_account_id) ACCOUNTNAME,get_account_address(t704.c704_account_id) ACCADDRESS ");
      sbQuery.append(" , get_rule_value_by_company('DE_INV_AD_NM','DE_INV_INFO','" + strCompanyId
          + "') GMNAME ");
    }

    sbQuery
        .append(" , NVL(t503.C503_INVOICE_ID,'TO-BE') INVFL , t503.C503_INVOICE_ID INV , '' PDFINVOICE");
    sbQuery
        .append(" ,get_payment_terms(t503.C503_invoice_date,t503.c503_terms) PAYMENTDUEDATE , t503.C503_CUSTOMER_PO PO ");
    sbQuery.append(" , NVL(gm_pkg_cm_shipping_info.get_ship_add( t501.C501_ORDER_ID ,50180,'"
        + strAttentionto
        + "'),gm_pkg_cm_shipping_info.get_ship_add( t501.C501_PARENT_ORDER_ID ,50180,'"
        + strAttentionto + "')) SHIPADD ,");
    sbQuery.append(" decode(t503.C503_INVOICE_DATE,'',CURRENT_DATE,t503.C503_INVOICE_DATE) INVDT, ");
    sbQuery
        .append(" decode(t503.C503_INVOICE_DATE,'',(CURRENT_DATE+30),(t503.C503_INVOICE_DATE+30)) DUEDT , t503.C503_STATUS_FL SFL ");
    // sbQuery.append(" ,A.C501_COMMENTS CMENT " ) ;
    sbQuery
        .append(" , t503.C901_INVOICE_TYPE INVTYPE, GET_CODE_NAME_ALT(t503.C901_INVOICE_TYPE) INVTITLE ");
    sbQuery.append(" , DECODE(t503.C901_INVOICE_TYPE,50202,GET_PARENT_INVOICE_ID ('");
    sbQuery.append(strInvoiceId);
    sbQuery.append("'),50203,GET_PARENT_INVOICE_ID ('");
    sbQuery.append(strInvoiceId);
    sbQuery.append("'),'') PARENT_INV_ID ");
    sbQuery.append(" ,GET_LOG_FLAG(t503.C503_INVOICE_ID,1201) CALL_FLAG ");
    sbQuery.append(" ,C503_COMMENTS COMMENTS ");
    sbQuery.append(" ,get_distributor_type (C701_DISTRIBUTOR_ID) disttype ");
    sbQuery
        .append(" ,GET_DISTRIBUTOR_INTER_PARTY_ID(C701_DISTRIBUTOR_ID) PARTYID ,NVL (t501.C501_Shipping_Date, '') shipdate");
    sbQuery.append(" , t501.C506_RMA_ID RMA_ID");
    sbQuery
        .append(" ,CASE WHEN TRUNC (TO_DATE ('"
            + strTaxDate
            + "', 'mm/DD/YYYY')) <= TRUNC (gm_pkg_ac_sales_tax.get_min_orders_date (t503.c503_invoice_id)) THEN 'Y' ELSE 'N' END  TAX_START_DATE_FL");
    sbQuery.append(" ,CASE WHEN TRUNC (TO_DATE ('");

    if (!strAdjustmentDate.equals("")) {
      sbQuery.append(strAdjustmentDate);
    }

    sbQuery.append("', 'mm/DD/YYYY");
    sbQuery
        .append("')) <= TRUNC (gm_pkg_ac_sales_tax.get_min_orders_date (t503.c503_invoice_id)) THEN 'Y' ELSE 'N' END  ADJ_START_DATE_FL");

    sbQuery
        .append(" , t503.C1900_COMPANY_ID company_id, get_company_code (t503.C1900_COMPANY_ID) comp_code");
    sbQuery.append(" ,t503.c1910_division_id division_id ");

    sbQuery
        .append(" , t503.C503_INV_AMT inv_amt ,t503.c101_dealer_id dealerid");
    sbQuery
        .append(" , get_rule_value_by_company (t503.c1900_company_id, 'VAT_RATE', t503.c1900_company_id) COMP_VAT_RATE ");
    sbQuery.append(" , t503.c1910_division_id DIVISION_ID , TO_CHAR(t501.c501_surgery_date, '"+getCompDateFmt()+"') SURGERY_DATE ");
    sbQuery.append(" , t503.c503_inv_amt_wo_tax inv_amt_wo_tax ");
    // PC-3929: To get the shipping cost from invoice table
    sbQuery.append(" , t503.c503_inv_ship_cost invoice_ship_cost ");
    if (strInvSrc.equals("26240213")) {

      sbQuery.append(" FROM t101_party t101,t101_party_invoice_details t101p ");

    } else {
      sbQuery.append(" FROM T704_ACCOUNT t704 ");
    }

    sbQuery.append(" ,T503_INVOICE t503 , T501_ORDER t501 ,t1900_company T1900");
    sbQuery.append(" WHERE t503.C503_INVOICE_ID = '");
    sbQuery.append(strInvoiceId);

    if (strInvSrc.equals("26240213")) {

      sbQuery
          .append("' AND t101.c101_party_id = t101p.c101_party_id AND t101.c101_party_id   = t503.c101_dealer_id ");
      sbQuery.append(" AND t101.c101_void_fl IS NULL AND t101p.c101_void_fl IS NULL");

    } else {
      sbQuery.append("' AND t704.C704_ACCOUNT_ID = t503.C704_ACCOUNT_ID  ");
    }

    sbQuery.append(" AND t503.c503_invoice_id = t501.c503_invoice_id ");
    sbQuery.append(" AND t503.C1900_COMPANY_ID = '" + strCompanyId + "'");
    sbQuery.append(" AND t503.C1900_COMPANY_ID = T1900.C1900_COMPANY_ID");

    if (!strAction.equals("PrintVoid"))
      sbQuery.append(" AND t503.C503_VOID_FL IS NULL AND t501.c501_void_fl IS NULL ");

    log.debug(" Query to load Order details is " + sbQuery.toString());
    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
   
    String strInvLayout = GmCommonClass.parseNull((String) hmResult.get("INVOICELAYOUT"));
    log.debug("INVOICELAYOUT >>>>>>>>" + strInvLayout);
    strInvLayout = strInvLayout.equals("") ? "10304529" : strInvLayout; //10304529--Default
    
    // For invoice statement type , there has no order details, so using below method to fetch invoice details
    String strMonInvCompId =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "MONTHLY_INV_COMP"));
   
    if (hmResult.size() == 0 && strCompanyId.equals(strMonInvCompId)) {
      hmResult =
          loadInvoiceStatementDetails(gmDBManager, strInvoiceId, strInvSrc, strCompanyId);

    }
    java.util.Date dtpaymentDate = null;
    dtpaymentDate = (java.util.Date) hmResult.get("PAYMENTDUEDATE");
    String strpaymentDueDate = GmCommonClass.getStringFromDate(dtpaymentDate, "yyyy/MM/dd");
    hmResult.put("PAYMENTDUEDATE", strpaymentDueDate);
    hmReturn.put("ORDERDETAILS", hmResult);
    String strAccId = GmCommonClass.parseNull((String) hmResult.get("ACCID"));
    hmReturn.put("CUSTOM_PART", GmCommonClass.parseNull(checkCustomPartNumByAccout(strAccId)));
    hmResult.put("INVSRC",strInvSrc);
    hmParty = loadInvPartyInfo(hmResult);
    hmReturn.putAll(hmParty);

    hmResult = new HashMap();
    GmInvoicePrintLayoutInterface gmInvoiceLayoutPrintInterface = (GmInvoicePrintLayoutInterface) GmInvoiceBean.getInvoiceLayoutPrintClass(strInvLayout);
    // Below Code will fetch the Order Item details for the selected                                            
    // customer invoice
    for (int i = 0; i < intLength; i++) {
      sbQuery.setLength(0);
      hmTemp = new HashMap();
      hmTemp = (HashMap) alReturn.get(i);
      strOrdId = (String) hmTemp.get("ID");
      alLoop =  gmDBManager.queryMultipleRecords(gmInvoiceLayoutPrintInterface.loadOrderItemLayoutSQL(strOrdId, strAction, getGmDataStoreVO()));
      hmResult.put(strOrdId, alLoop);                                          

      alLoop = gmDBManager.queryMultipleRecords(loadOrderConstructSQL(strOrdId));
      hmConstruct.put(strOrdId, alLoop);
    }
    hmReturn.put("CARTDETAILS", hmResult);
    hmReturn.put("CONSTRUCTDETAILS", hmConstruct);
    hmReturn.put("ADJDETAILS", hmAdjDtls);

    if (strAction.equals("PAY") || strAction.equals("UPD")) {
      ArrayList alPayMode = GmCommonClass.getCodeList("PAYME");
      hmReturn.put("PAYMODEDETAILS", alPayMode);
      hmPayment.put("INVID", strInvoiceId);
      hmPayment.put("ORDERID", "");
      // The Above Query is moved to separate method.These changes are done for PMT-5022.
      alPayDetails = GmCommonClass.parseNullArrayList(fetchPaymentDetails(hmPayment));
      hmReturn.put("PAYDETAILS", alPayDetails);
    }
    hmReturn.put("INVOICE_ATTRIB", loadInvoiceAttribute("INVOICE_ATTRIB"));
    hmReturn.put("INV_SOURCE", strInvSrc);
    log.debug("hmReturn>>>>"+hmReturn);
    return hmReturn;
  } // End of loadInvoiceDetails

  /**
   * @param strInvoiceId
   * @param strAction
   * @return String
   */
  public String loadInvoiceSQL(String strInvoiceId, String strAction) {
    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strMonInvCompId =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "MONTHLY_INV_COMP"));
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT C501_ORDER_ID ID ");
    sbQuery.append(" ,C501_COMMENTS CMENT ");
    sbQuery.append(" ,C501_ORDER_DATE ORDERDATE ");
    sbQuery.append(" ,GET_USER_NAME(C501_CREATED_BY) CREATEDBY ");
    sbQuery.append(" ,GET_CODE_NAME(C501_DELIVERY_MODE) SHIPMODE ");
    sbQuery.append(" ,GET_CODE_NAME(C501_RECEIVE_MODE) ORDERMODE ");
    sbQuery.append(" ,GET_DIST_REP_NAME(C703_SALES_REP_ID) REPDISTNM ");
    sbQuery.append(" ,C901_ORDER_TYPE ORDERTYPE,c506_rma_id RMAID ");
    sbQuery.append(" , c501_customer_po_date cust_po_date ");
    sbQuery.append(" FROM T501_ORDER ");
    sbQuery.append(" WHERE C503_INVOICE_ID = '");
    sbQuery.append(strInvoiceId);
    sbQuery.append("' AND C501_STATUS_FL IS NOT NULL ");
    sbQuery.append(" AND C501_VOID_FL IS NULL ");
    sbQuery.append(" AND nvl(C901_order_type,-9999) NOT IN ('2535','101260') ");
    sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
    sbQuery.append(" SELECT t906.c906_rule_value ");
    sbQuery.append(" FROM t906_rules t906 ");
    sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
    sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
    // Invoice Print Should not include the return
    // that are raised before invoice
    // If condition is added for the same
    if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
      // Including Sales Adjustment for Japan
//      if (strCompanyId.equals(strMonInvCompId)) {
//        sbQuery.append(" AND NVL(C901_ORDER_TYPE,0) NOT IN ('101260') ");
//      } else {
        sbQuery.append(" AND NVL(C901_ORDER_TYPE,0) NOT IN ('2529','101260') ");
//      }
      sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
      sbQuery.append(" AND C1900_COMPANY_ID = '" + strCompanyId + "'");
      // AND C501_STATUS_FL > 2 sbQuery.append(" AND T501B.C901_ORDER_TYPE
      // <> 2525 "); //To skip all the back order if not shipped
    }
    if (strCompanyId.equals(strMonInvCompId)) {// 1022 = japan company
      sbQuery.append(" ORDER BY ORDERDATE ,ID");
    } else {
      sbQuery.append(" ORDER BY ID ");
    }
    log.debug(" loadOrder SQL is " + sbQuery.toString());

    return (sbQuery.toString());

  }

  /**
   * loadOrderAttribute - This method is used to load the attribute values of an order
   * 
   * @param String strOrdId
   * @param String strOrdType
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadOrderAttribute(String strOrdId, String strOrdType) throws AppError {

    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alReturn = null;
    HashMap hmTemp = null;
    HashMap hmReturn = new HashMap();
    HashMap hmParentOrderId = new HashMap();
    String strParentOrderId = "";
    /*
     * If the order type is back order, then we are getting the parent order id to get the attribute
     * type and value corresponding to the order id 2525: Back Order
     */
    if (strOrdType.equals("2525")) {
      sbQuery.append(" select c501_parent_order_id parentorderid from T501_ORDER ");
      sbQuery.append(" where c501_order_id = '");
      sbQuery.append(strOrdId);
      sbQuery.append("' AND c501_void_fl IS NULL ");
      hmParentOrderId = gmDBManager.querySingleRecord((sbQuery.toString()));
      strParentOrderId = GmCommonClass.parseNull((String) hmParentOrderId.get("PARENTORDERID"));
      sbQuery.setLength(0);
      // For the complete back order(insufficient shelf qty), parent order Id will be null.
      strOrdId = strParentOrderId.equals("") ? strOrdId : strParentOrderId;
    }

    sbQuery
        .append(" select get_code_name_alt(c901_attribute_type) NAME, c501A_attribute_value VALUE from T501A_ORDER_ATTRIBUTE ");
    sbQuery.append(" where c501_order_id = '");
    sbQuery.append(strOrdId);
    sbQuery.append("' AND c501a_void_fl IS NULL ");

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));

    // Iterating each records in the result and getting the values of keys, "NAME" and "VALUE" to
    // hmTemp
    if (alReturn != null) {
      for (Iterator it = alReturn.iterator(); it.hasNext();) {
        hmTemp = (HashMap) it.next();
        hmReturn.put(hmTemp.get("NAME"), hmTemp.get("VALUE"));
      }
    }
    return hmReturn;
  }

  /**
   * loadInvPartyInfo - This method gets Party information for the selected the selected Account to
   * see if it's third party
   * 
   * @param HashMap hmParty
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadInvPartyInfo(HashMap hmParty) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmHeadAddr = new HashMap();
    String strPartyAddress = "";
    String strPartyPhone = "";
    String strPartyFax = "";

    String strHeadAddress = "";
    String strPartyFlag = "";
    String strRemitAddress = "";
    String strPartyNm = "";
    String strIBAN = "";
    String strSwiftCode = "";
    String strRemitName = "";
    String strCompanyRemitAddress = "";
    String strRemitMailName = "";
    String strCompanyRemitMailAddress = "";
    String strRemitMailAddress = "";
    String strDivisionId = "";
    String strCompanyName = "";
    String strCompanyAddress = "";
    String strImportShipperTax = "";
    String strHidePhoneNo = "";
    String strCompanyPhone = "";
    String strShipTaxId = "";
    String strConsignName = "";
    String strConsignPhone = "";
    String strConsignRegn = "";
    String strShipPurpose = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());
    GmAccountingBean gmAccountingBean = new GmAccountingBean(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();

    String strThirdPartyId = GmCommonClass.parseNull((String) hmParty.get("THIRDPARTYID"));
    String strCompanyId = GmCommonClass.parseNull((String) hmParty.get("COMPID"));
    String strPartyId = GmCommonClass.parseNull((String) hmParty.get("PARTYID"));
    String strDistType = GmCommonClass.parseNull((String) hmParty.get("DISTTYPE"));
    String strInvSource = GmCommonClass.parseNull((String) hmParty.get("INVSRC"));
    String strInvType = GmCommonClass.parseNull((String) hmParty.get("INVTYPE"));
    String strTaxStartDateFl = GmCommonClass.parseNull((String) hmParty.get("TAX_START_DATE_FL"));
    String strAdjStartDateFl = GmCommonClass.parseNull((String) hmParty.get("ADJ_START_DATE_FL"));
    if (!strThirdPartyId.equals("")) {// && .isGreaterThan(strInvoiceDt,
                                      // strThirdPartyDt, "")

      sbQuery.setLength(0);
      sbQuery.append(" SELECT get_party_name('");
      sbQuery.append(strThirdPartyId);
      sbQuery.append("') PARTYNAME,");
      sbQuery.append(" gm_pkg_cm_party_info.get_party_address('");
      sbQuery.append(strThirdPartyId);
      sbQuery.append("') PARTYADDR,");
      sbQuery.append(" gm_pkg_cm_party_info.get_party_contact_info('");
      sbQuery.append(strThirdPartyId);
      sbQuery.append("', 90450) PARTYPHONE,"); // phone
      sbQuery.append(" gm_pkg_cm_party_info.get_party_contact_info('");
      sbQuery.append(strThirdPartyId);
      sbQuery.append("', 90454) PARTYFAX"); // fax
      sbQuery.append(" FROM DUAL ");

      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());

      log.debug(" Query to fetch Invoice Party details is " + sbQuery.toString());
      strPartyNm = GmCommonClass.parseNull((String) hmResult.get("PARTYNAME"));
      strPartyAddress = GmCommonClass.parseNull((String) hmResult.get("PARTYADDR"));
      strPartyPhone = GmCommonClass.parseNull((String) hmResult.get("PARTYPHONE"));
      strPartyFax = GmCommonClass.parseNull((String) hmResult.get("PARTYFAX"));
      strPartyFlag = "Y";
      strHeadAddress =
          "<b>" + strPartyNm + "</b><BR>" + strPartyAddress + "</b><BR>Phone: " + strPartyPhone
              + "</b><BR>Fax: " + strPartyFax;

    } else {
      /*
       * Below if condition is added for the header and remit to change for ICS credit and debit
       * note.
       */
      if (strDistType.equals("70105") || strInvSource.equals("50253")) {//50253-Inter company sale
                String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
                String strPortalCompanyId = GmCommonClass.parseNull((String) getGmDataStoreVO().getCmpid());
               
               GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
               strHidePhoneNo = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("CONSIGN.HIDE_PHONE_NO"));
               strDivisionId = GmCommonClass.parseNull((String) hmResult.get("DIVISION_ID"));
               strDivisionId = strDivisionId.equals("") ? "2000" : strDivisionId;
               HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(strPortalCompanyId, strDivisionId);
               strCompanyName = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("ICS.COMPNAME"));
               strCompanyAddress = GmCommonClass.parseNull((String) gmResourceBundleBean.getProperty("ICS.COMPADDRESS"));
        if (!strHidePhoneNo.equals("YES")) {
          strCompanyPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("PH"));
        }
        hmHeadAddr = (HashMap) gmPartyBean.fetchPartyRuleParam(strPartyId, "ICPTY", "PRINT");
        strHeadAddress = GmCommonClass.parseNull((String) hmHeadAddr.get("HEADERADDRESS"));
        strShipTaxId = GmCommonClass.parseNull((String) hmHeadAddr.get("SHIPTAXID")); 
        strConsignName = GmCommonClass.parseNull((String) hmHeadAddr.get("CONSIGNEEPERSON")); 
        strConsignPhone = GmCommonClass.parseNull((String) hmHeadAddr.get("CONSIGNEEPHONE")); 
        strConsignRegn = GmCommonClass.parseNull((String) hmHeadAddr.get("CONSIGNEEREGION")); 
        strShipPurpose = GmCommonClass.parseNull((String) hmHeadAddr.get("SHIPPURPOSE")); 
        strHeadAddress = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("ICS.GMCOMPADD"));
        strHeadAddress = "<br>" + strHeadAddress.replaceAll(",", "<br>");
        strImportShipperTax = GmCommonClass.parseNull((String) hmHeadAddr.get("CONSIGNEETAXID"));
        
        HashMap hmICSBankDtls = gmAccountingBean.fetchICSBankDetails("569", strPartyId);
        // get the details from hash map
        strIBAN = GmCommonClass.parseNull((String) hmICSBankDtls.get("IBAN"));
        strSwiftCode = GmCommonClass.parseNull((String) hmICSBankDtls.get("SWIFTCODE"));
        strRemitAddress = GmCommonClass.parseNull((String) hmICSBankDtls.get("BANK_ADD"));
        // to get the shipper id
        String strImportShipperTaxId = GmCommonClass.parseNull((String) hmICSBankDtls.get("IMPORT_SHIPPER_TAX"));
        if (!strImportShipperTaxId.equals("")) {
        	strImportShipperTax = strImportShipperTaxId;
        } // end if shipper tax
      
       // strRemitAddress = strRemitAddress + "<br>IBAN: " + strIBAN + "<br>Swift Code: " + strSwiftCode;   //PMT-30557: Credit and debit memo paperwork changes
        strPartyNm = "<u><b>Please remit payment to</b></u>";
        strPartyFlag = "ICS_INV";
        hmReturn.put("IMPORTSHIPPERTAX", strImportShipperTax);
        hmReturn.put("SHIPTAXID", strShipTaxId);
        hmReturn.put("GMADDRESS", strCompanyName + "/" + strCompanyAddress);
      } else {
        strPartyNm = GmCommonClass.parseNull(GmCommonClass.getString("GMCOMPANYNAME"));
        strPartyNm = "-Please make checks payable to <b>" + strPartyNm + "</b>";
        strPartyFlag = "";
      }
    }
    hmReturn.put("PARTYFLAG", strPartyFlag);
    hmReturn.put("PARTYNAME", strPartyNm);
    hmReturn.put("HEADERADDRESS", strHeadAddress);
    hmReturn.put("REMITADDRESS", strRemitAddress);
    hmReturn.put("SWIFTCODE", strSwiftCode);
    hmReturn.put("IBAN", strIBAN);
    hmReturn.put("CONSIGN_NAME", strConsignName);
    hmReturn.put("CONSIGN_PHONE", strConsignPhone);
    hmReturn.put("CONSIGN_REGN", strConsignRegn);
    hmReturn.put("SHIPPURPOSE", strShipPurpose);
    hmReturn.put("DISTTYPE", strDistType);
    
    return hmReturn;
  }

  /**
   * checkCustomPartNumByAccout - This method will check whether the account is having any customer
   * ref part # in t205e table.
   * 
   * @param String strAccId
   * @return String
   * @exception AppError
   */
  public String checkCustomPartNumByAccout(String strAccId) throws AppError {
    String strCustomPartNumCheckFl = "N";
    String strGpoId = "";
    String strCustomCount = "";
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbQuery.setLength(0);
    sbQuery.append(" SELECT NVL(GET_ACCOUNT_GPO_ID('");
    sbQuery.append(strAccId);
    sbQuery.append("'),0) GPO_ID FROM DUAL ");
    log.debug(" Query to Fetch Gpo id  " + sbQuery.toString());
    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    strGpoId = GmCommonClass.parseNull((String) hmResult.get("GPO_ID"));

    sbQuery.setLength(0);
    sbQuery.append(" SELECT COUNT(1) CNT FROM T205E_CUSTOM_PART_NUMBER ");
    sbQuery.append(" WHERE C205E_VOID_FL IS NULL ");
    sbQuery.append(" AND ( C205E_REF_VALUE ='");
    sbQuery.append(strAccId);
    sbQuery.append("'  OR C205E_REF_VALUE ='");
    sbQuery.append(strGpoId);
    sbQuery.append("') ");
    log.debug(" Query to Fetch custom part num count  " + sbQuery.toString());
    HashMap hmResultCount = gmDBManager.querySingleRecord(sbQuery.toString());
    strCustomCount = GmCommonClass.parseZero((String) hmResultCount.get("CNT"));

    if (!strCustomCount.equals("0")) {
      strCustomPartNumCheckFl = "Y";
    }
    return strCustomPartNumCheckFl;
  }

  /**
   * fetchPaymentDetails - This method will return the payment details when we are passing either
   * Invoice ID or Order ID. Order ID will be passed in Delivered Order Summary Screen. Invoice ID
   * will be passed in post payment Screen.
   * 
   * @exception AppError
   */
  public ArrayList fetchPaymentDetails(HashMap hmParam) throws AppError {

    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    String strInvoiceId = GmCommonClass.parseNull((String) hmParam.get("INVID"));
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbQuery.setLength(0);
    sbQuery.append(" SELECT  C508_RECEIVABLES_ID RID, T508.C503_INVOICE_ID INVID, ");
    sbQuery.append(" C508_RECEIVED_DATE RDATE, GET_CODE_NAME(C508_RECEIVED_MODE) RMODE,  ");
    sbQuery
        .append(" C508_PAYMENT_AMT PAYAMT, C508_DETAILS DETAILS  ,GET_USER_NAME(t508.c508_created_by) createdby ,t508.c508_created_date CREATEDDATE ");
    sbQuery.append(" FROM T508_RECEIVABLES T508 WHERE ");

    if (!strInvoiceId.equals("")) {// When the input is passed as invoice id this will be be
                                   // ececuted.This will be called from post payment link in modify
                                   // invoice screen
      sbQuery.append("  C503_INVOICE_ID='");
      sbQuery.append(strInvoiceId);
      sbQuery.append("'");
    }

    if (!strOrderId.equals("")) {// When the input is passed as order id this will be be
                                 // ececuted.This will be called from DO Summary SCreen.
      sbQuery.append("  C503_INVOICE_ID in ( ");
      sbQuery.append(" SELECT C503_Invoice_Id from t501_order WHERE  (c501_order_id  = '");
      sbQuery.append(strOrderId);
      sbQuery.append("'");
      sbQuery.append("or C501_PARENT_ORDER_ID = '"); // fetch the parent order invoice payment
                                                     // details
      sbQuery.append(strOrderId);
      sbQuery.append("'");
      sbQuery.append(" ) AND c501_void_fl is NULL )");
    }

    sbQuery.append(" ORDER BY C508_RECEIVED_DATE ");
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    return alReturn;
  }

  /**
   * loadInvoiceAttribute - This method will return the hashmap for the input rule grp id.
   * 
   * @exception AppError
   */
  public HashMap loadInvoiceAttribute(String strRuleGrpId) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = null;
    HashMap hmTemp = null;
    HashMap hmReturn = new HashMap();
    sbQuery
        .append(" SELECT C906_RULE_ID NAME,C906_RULE_VALUE VALUE,C906_RULE_GRP_ID GRPVALUE FROM t906_rules ");
    sbQuery.append(" where c906_rule_grp_id = '");
    sbQuery.append(strRuleGrpId);
    sbQuery.append("' ");
    sbQuery.append(" AND c1900_company_id = " + getCompId());
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
log.debug("alReturn>>>>>"+alReturn.size());

    if (alReturn != null) {
      for (Iterator it = alReturn.iterator(); it.hasNext();) {
        hmTemp = (HashMap) it.next();
        hmReturn.put(hmTemp.get("NAME"), hmTemp.get("VALUE"));
      }
    }
    return hmReturn;
  }// end of loadInvoiceAttribute

  /**
   * loadInvForReturns - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadInvForReturns(String strInvId, String strAccId) throws AppError {
    HashMap hmReturn = new HashMap();
    String strPO = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT   C503_CUSTOMER_PO POID ");
    sbQuery.append(" FROM T503_INVOICE ");
    sbQuery.append(" WHERE C503_INVOICE_ID = '");
    sbQuery.append(strInvId);
    sbQuery.append("'");
    sbQuery.append(" AND C503_VOID_FL IS NULL ");
    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    strPO = (String) hmResult.get("POID");
    hmReturn = fetchInvoiceDetails(strPO, strAccId, "");
    log.debug("hmReturn>>>>>"+hmReturn);
    return hmReturn;
  } // End of loadInvForReturns

  /**
   * This method used to fetch the Italy invoice XML header details
   * 
   * @param strInvoiceId
   * @return
   * @throws AppError
   */
  public HashMap fetchItalyInvHeaderDtls(String strInvoiceId) throws AppError {
    log.debug(" strInvoiceId " + strInvoiceId);
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_fch_italy_inv_header_dtls", 2);
    gmDBManager.setString(1, strInvoiceId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    return hmReturn;
  }

  /**
   * loadItalyOrderItemSQL - This method used to fetch the italy item details
   * 
   * @param strOrdId
   * @param strAction
   * @return String
   */
  public String loadItalyOrderItemSQL(String strOrdId, String strAction) {

    StringBuffer sbQuery = new StringBuffer();
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    GmResourceBundleBean gmResourceBundleBean1 =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    String strShowPartUomFl =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.SHOW_PART_UOM"));
    String strCompanyId = getGmDataStoreVO().getCmpid();
    // Below Query is added in Print so the zero invoice order
    // is not listed in the screen
    if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
      sbQuery.append("  SELECT * FROM ");
      sbQuery.append(" ( ");
      sbQuery
          .append(" SELECT  distinct (t502b.C205_PART_NUMBER_ID) ID, get_partdesc_by_company (t502b.C205_PART_NUMBER_ID) PDESC, ");
    //PC-4645-Add Admin Ref and CND # field
      sbQuery
      .append(" t2053.c2053_admin_ref ADMIN_REF, t2053.c2053_other_mgmt_data OTHER_MGMT_DATA, ");
      sbQuery
          .append(" gm_pkg_cs_usage_lot_number.get_italy_invoice_part_qty (t502b.c502b_item_order_usage_id) QTY, t205.c205_product_family, ");
      sbQuery
          .append(" ROUND (NVL (T500.c500_item_price, 0) * NVL (gm_pkg_cs_usage_lot_number.get_italy_invoice_part_qty (t502b.c502b_item_order_usage_id), 0) * NVL (T502.c502_vat_rate /100, 0),2) tax_cost,");
      sbQuery.append(" t500.C500_ITEM_PRICE PRICE,");
      //PC-4379 Italy Invoice XML is not created changes     
      sbQuery.append(" t502b.c502b_ref_id ddt, t502b.c502b_usage_lot_num usage_lot,  t2053.c901_dm_val dmval,t2053.c901_cnd_val cndval,NVL(t502b.c502b_last_updated_date,t502b.c502b_created_date) DDT_UPD_DT, ");
    } else {

      sbQuery
          .append(" SELECT T502.C205_PART_NUMBER_ID ID, get_partdesc_by_company(T502.C205_PART_NUMBER_ID) PDESC, ");

      sbQuery
          .append(" T502.C502_ITEM_QTY QTY,  T502.C502_CONTROL_NUMBER CNUM, T502.C502_ITEM_ORDER_ID ITEM_ORD_ID,");
      sbQuery
          .append("  T502.C502_ITEM_PRICE PRICE,NVL(T502.C502_BASE_PRICE,0) ACTUALPRICE,T502.C502_EXT_ITEM_PRICE EXTERNAL_ITEM_PRICE, T501.C501_EXT_TOTAL_COST EXTERNAL_TOTAL_COST,");
      sbQuery
          .append(" NVL(T502.c502_tax_amt, ROUND (NVL ( (T502.C502_ITEM_QTY * NVL (T502.C502_ITEM_PRICE, 0) * NVL (T502.c502_vat_rate, 0) / 100), 0), 2)) tax_cost, ");
      sbQuery.append("  NVL(T501.C501_Shipping_Date,'') shipdate ");
      sbQuery
          .append(",  NVL(c502_unit_price,0) unitprice, NVL((c502_unit_price_adj_value * -1),0) adjval, DECODE(C901_UNIT_PRICE_ADJ_CODE, NULL, 'N/A', GET_CODE_NAME(c901_unit_price_adj_code)) adjcode ");
      sbQuery.append(",  DECODE(NVL(T502.C502_UNIT_PRICE_ADJ_VALUE,0),'0','','Y') ADJFLAG ");
      sbQuery.append(",  GET_CODE_NAME_alt(T502.c901_unit_price_adj_code) ADJCODEALT ");
      sbQuery
          .append(",  NVL(T502.C502_ITEM_PRICE,0)-NVL(T502.C502_UNIT_PRICE,0) ADJEA,  T502.c901_type TYPE, T502.C502_CONSTRUCT_FL CONFL,");
      sbQuery.append(" T501.C501_ORDER_DATE ODT, ");
    }
    sbQuery
        .append(" T501.C501_SHIP_COST SCOST, get_part_attr_value_by_comp (T502.C205_PART_NUMBER_ID,'200001', T501.C1900_COMPANY_ID) NTFTCODE, NVL(T502.c502_vat_rate,0) VAT");
    sbQuery
        .append(" , get_rule_value_by_company(get_partnum_product(T502.C205_PART_NUMBER_ID),'VAT_RATE',T501.C1900_COMPANY_ID) PART_VAT_RATE ");
    // to set the Unit of Measure values
    if (strShowPartUomFl.equalsIgnoreCase("YES")) {
      sbQuery
          .append(" , get_rule_value_by_company(get_partnum_uom(T502.C205_PART_NUMBER_ID), 'INVOICE_PART_UOM', T501.C1900_COMPANY_ID) uom ");
    }
    sbQuery.append(" FROM T502_ITEM_ORDER T502, T501_ORDER T501 ");
    if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
      sbQuery.append(" , t502b_item_order_usage t502b, t500_order_info t500, t205_part_number t205,t2053_Part_elements     t2053 ");
    }
    sbQuery.append(" WHERE T502.C501_ORDER_ID = '");
    sbQuery.append(strOrdId);
    sbQuery.append("' AND T502.C502_VOID_FL IS NULL AND T502.C502_DELETE_FL IS NULL ");
    sbQuery.append(" AND T502.C501_ORDER_ID = T501.C501_ORDER_ID ");
    sbQuery.append(" AND T501.C1900_COMPANY_ID = '" + strCompanyId
        + "' AND T501.c501_void_fl IS NULL ");

    if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
      sbQuery.append(" AND T502.c205_part_number_id =  t205.c205_part_number_id ");
      sbQuery.append(" AND T501.C501_ORDER_ID = t502b.c501_order_id ");
      sbQuery.append(" AND T501.C501_ORDER_ID = T500.c501_order_id ");
      sbQuery.append(" AND T500.c500_order_info_id = t502.c500_order_info_id ");
      sbQuery.append(" AND T502.c500_order_info_id = t502b.c500_order_info_id ");
      sbQuery.append(" AND t502b.c502b_void_fl IS NULL "
      		+ "and t2053.c205_part_number_id(+) = t502.c205_part_number_id "
      		+ "and t2053.c2053_void_fl(+) is null ");
    }
    sbQuery.append(" ORDER BY T502.C205_PART_NUMBER_ID ");

    if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
      sbQuery.append(" )");
      sbQuery.append(" WHERE  QTY != 0 AND  ( c205_product_family != 26240096 or price != 0)  ORDER BY ID ");
    }
    log.debug(" Query formed for loadOrderItemSQL is " + sbQuery.toString());
    return (sbQuery.toString());
  }

  /**
   * loadJapanInvoiceDetails - This method used to fetch the japan invoice details
   * 
   * @param strInvoiceId
   * @param strAction
   * @return HashMap
   */
  public HashMap loadJapanInvoiceDetails(String strInvoiceId) throws AppError {

    HashMap hmResult = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strDateFormat = "yyyy/MM/dd";// For japanese paperwork date format should be this one.

    sbQuery.setLength(0);
    sbQuery
        .append(" SELECT T503.C503_INVOICE_ID INVOICEID,C503B_DATE_OF_ISSUE_FROM DATEFROM,C503B_DATE_OF_ISSUE_TO DATETO,");
    sbQuery
        .append(" NVL(C503B_UNPAID_INVOICE_AMT,0) UNPAIDINVOICEAMT,NVL(C503B_PAID_INVOICE_AMT,0) PAIDINVOICEAMT");
    sbQuery
        .append(" ,NVL(C503B_TRANSACTION_AMT,0) TRANSACTIONAMT,NVL(C503B_CONSUMPTION_TAX_AMOUNT,0) CONSUMPTIONTAX ");
    sbQuery
        .append(" ,NVL(C503B_TOTAL_SALES_AMT,0) TOTALAMOUNTMONTH,NVL(C503B_ENDING_AR_BAL,0) ENDINGBALANCEAR ");
    sbQuery.append(" FROM T503_INVOICE T503,T503B_INVOICE_ATTRIBUTE T503B ");
    sbQuery.append(" WHERE T503.C503_INVOICE_ID = T503B.C503_INVOICE_ID ");
    sbQuery.append(" AND T503B.C503_INVOICE_ID = '" + strInvoiceId + "'");
    sbQuery.append(" AND C503_VOID_FL IS NULL AND C503B_VOID_FL IS NULL");
    log.debug(" Subquery  " + sbQuery.toString());
    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    java.util.Date dtDateFrom = null;
    java.util.Date dtDateTo = null;
    dtDateFrom = (java.util.Date) hmResult.get("DATEFROM");
    dtDateTo = (java.util.Date) hmResult.get("DATETO");
    String strDateFrom = GmCommonClass.getStringFromDate(dtDateFrom, strDateFormat);
    String strDateTo = GmCommonClass.getStringFromDate(dtDateTo, strDateFormat);
    String strDateofIssue = strDateTo + "  (" + strDateFrom + " - " + strDateTo + ") ";
    hmResult.put("DATEOFISSUE", strDateofIssue);

    return hmResult;
  } // End of loadJapanInvoiceDetails

  /**
   * loadItalyOrderItemSQL - This method used to fetch the japan Invoice Paid in month amount.
   * 
   * @param strOrdId
   * @param strAction
   * @return String
   */
  public ArrayList loadJapanPaidInMonth(HashMap hmDealerInvoice) {

    ArrayList alReuslt = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    String strComDateFmt = "yyyy/MM/dd";// For japanese paperwork date format should be this one.
    String strDateFrom = "";
    String strDateTo = "";
    java.util.Date dtDateFrom = null;
    java.util.Date dtDateTo = null;
    dtDateFrom = (java.util.Date) hmDealerInvoice.get("DATEFROM");
    dtDateTo = (java.util.Date) hmDealerInvoice.get("DATETO");
    strDateFrom = GmCommonClass.getStringFromDate(dtDateFrom, strComDateFmt);
    strDateTo = GmCommonClass.getStringFromDate(dtDateTo, strComDateFmt);
    String strInvoiceID = GmCommonClass.parseNull((String) hmDealerInvoice.get("INVOICEID"));
    String strDealerId = GmCommonClass.parseNull((String) hmDealerInvoice.get("DEALERINVID"));
    String strAccountId = GmCommonClass.parseNull((String) hmDealerInvoice.get("ACCID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strCompanyId = getGmDataStoreVO().getCmpid();

    if (!strDealerId.equals("")) {


      sbQuery
          .append(" SELECT t503.c503_invoice_id ORDERID,'' PDESC,'0' QTY,'0' PRICE, '' SHIPDATE,'' EXTERNAL_ITEM_PRICE,'' CUST_PDESC,'' CONFL,'' VATPER,'' ADJFLAG,");
      sbQuery
          .append(" '' ACCTNAME,'' ADJVAL,'' ITEMTOTAL,'' ID,'' ACTUALPRICE,'' ORDERDATE,'' TYPE,'' EXTERNAL_TOTAL_COST,'' NTFTCODE,'' ITEMUNITPRICETOTAL,");
      sbQuery
          .append("  '' ADJEA,'' ADJCODE,'' TAX_COST,'' UNITPRICE,'' VAT,'' CUST_PNUM,'' SCOST,'' ODT,");
      sbQuery
          .append(" NVL(SUM(NVL(T508.C508_PAYMENT_AMT,0)),0) PAIDINMONTH ,T508.C508_RECEIVED_DATE SURG_DT ");
      sbQuery.append("FROM T508_RECEIVABLES T508, ");
      sbQuery.append("  T503_INVOICE T503 ");
      sbQuery.append(" WHERE T508.C508_RECEIVED_DATE >= to_Date('" + strDateFrom + "','"
          + strComDateFmt + "') ");
      sbQuery.append(" AND T508.C508_RECEIVED_DATE   <= to_Date('" + strDateTo + "','"
          + strComDateFmt + "') ");
      sbQuery.append(" AND T503.c503_invoice_id = T508.c503_invoice_id ");
      sbQuery.append(" AND T503.c101_dealer_id  = '" + strDealerId + "'");
      sbQuery.append(" AND T503.C503_VOID_FL   IS NULL ");
      sbQuery
          .append(" group by T508.C508_PAYMENT_AMT,T508.C508_RECEIVED_DATE,t503.c503_invoice_id ");
    } else {
      sbQuery
          .append(" SELECT t503.c503_invoice_id ORDERID,'' PDESC,'0' QTY,'0' PRICE, '' SHIPDATE,'' EXTERNAL_ITEM_PRICE,'' CUST_PDESC,'' CONFL,'' VATPER,'' ADJFLAG,");
      sbQuery
          .append(" '' ACCTNAME,'' ADJVAL,'' ITEMTOTAL,'' ID,'' ACTUALPRICE,'' ORDERDATE,'' TYPE,'' EXTERNAL_TOTAL_COST,'' NTFTCODE,'' ITEMUNITPRICETOTAL,");
      sbQuery
          .append("  '' ADJEA,'' ADJCODE,'' TAX_COST,'' UNITPRICE,'' VAT,'' CUST_PNUM,'' SCOST,'' ODT,");
      sbQuery
          .append("  NVL(SUM(NVL(T508.C508_PAYMENT_AMT,0)),0) PAIDINMONTH ,T508.C508_RECEIVED_DATE SURG_DT ");
      sbQuery.append(" FROM T508_RECEIVABLES T508, ");
      sbQuery.append(" T503_INVOICE T503, ");
      sbQuery.append(" T704_ACCOUNT T704 ");
      sbQuery.append(" WHERE T508.C508_RECEIVED_DATE >= to_Date('" + strDateFrom + "','"
          + strComDateFmt + "') ");
      sbQuery.append(" AND T508.C508_RECEIVED_DATE   <= to_Date('" + strDateTo + "','"
          + strComDateFmt + "') ");
      sbQuery.append(" AND T508.C503_INVOICE_ID = T503.C503_INVOICE_ID ");
      sbQuery.append(" AND T503.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID ");
      sbQuery.append("  AND T503.C704_ACCOUNT_ID  = '" + strAccountId + "' ");
      sbQuery.append(" AND T503.C503_VOID_FL   IS NULL ");
      sbQuery.append(" AND T704.C704_VOID_FL   IS NULL  ");
      sbQuery
          .append(" group by T508.C508_PAYMENT_AMT,T508.C508_RECEIVED_DATE,t503.c503_invoice_id ");
    }
    log.debug(" Query formed for loadJapanPaidInMonth is " + sbQuery.toString());
    alReuslt =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    return alReuslt;
  }


 /**
   * loadInvoiceStatementDetails - This method used to fetch invoice statement details.
   * 
   * @param GmDBManager
   * @param strInvoiceId
   * @param strInvSrc
   * @param strCompanyId
   * @return HashMap
   */
  public HashMap loadInvoiceStatementDetails(GmDBManager gmDBManager, String strInvoiceId,
      String strInvSrc, String strCompanyId)
      throws AppError {



    StringBuffer sbQuery = new StringBuffer();
    HashMap hmResult = new HashMap();

    // Invoice Header Details
    if (strInvSrc.equals("26240213")) {

      sbQuery.append(" SELECT t101.c101_party_id ACCID");
      
      sbQuery
          .append(" , DECODE('"
              + getComplangid()
              + "',103097,NVL(t101.c101_party_nm_en,t101.c101_party_nm),t101.c101_party_nm) REPDISTNM ");
      
      sbQuery.append(", GET_DEALER_BILL_ADD (t101.c101_party_id) BILLADD ");
     
      sbQuery.append(", GET_DEALER_BILL_ADD (t101.c101_party_id) BILLADD ");
      
      sbQuery
          .append(" ,t101p.c901_payment_terms PAY, GET_CODE_NAME(t101p.c901_payment_terms) PAYNM ");
     
      sbQuery
          .append(" , get_acct_curr_symb_by_dealer(t101.c101_party_id) CURRENCY, get_acct_curr_id_by_dealer (t101.c101_party_id) CURRENCYID,");
      
      sbQuery.append(" get_acct_curr_symb_by_dealer (t101.c101_party_id)  CURRSIGN ");
      
      sbQuery.append(" , t101p.c901_invoice_layout invoicelayout");
      
      sbQuery.append(" , t101p.c101_vendor_id VENDORID");
      
      sbQuery
          .append(" ,get_party_name(t101.c101_party_id)  ACCOUNTNAME,GET_DEALER_BILL_ADD(t101.c101_party_id)  ACCADDRESS");


    } else {

      sbQuery.append(" SELECT B.C704_ACCOUNT_ID ACCID, B.C704_CUSTOMER_REF_ID CUSTREFID ");
      
      sbQuery.append(" , GET_DIST_REP_NAME(GET_REP_ID(B.C704_ACCOUNT_ID)) REPDISTNM ");
      
      sbQuery.append(" , GET_BILL_ADD(B.C704_ACCOUNT_ID) BILLADD ");
      
      sbQuery.append(" , B.C704_PAYMENT_TERMS PAY, GET_CODE_NAME(B.C704_PAYMENT_TERMS) PAYNM ");
      
      sbQuery
          .append(" ,DECODE(get_account_attrb_value (B.c704_account_id,91987),'Y','2587','') THIRDPARTYID ");
      
      sbQuery.append(" ,GET_RULE_VALUE(B.C704_ACCOUNT_ID, 'THIRD-PARTY-DATE') THIRDPARTYDT");
      
      sbQuery.append(" ,GET_RULE_VALUE(B.C704_ACCOUNT_ID, 'INVOICE-NOTE') INVOICENOTE ");
      
      sbQuery.append(" ,B.C901_COMPANY_ID COMPID ");
      
      sbQuery
          .append(" ,DECODE (get_distributor_type (get_account_dist_id (b.c704_account_id)), 70106, NVL (get_rule_value (50218, get_distributor_inter_party_id (get_account_dist_id (b.c704_account_id))), get_code_name (B.C901_CURRENCY)), get_code_name (B.C901_CURRENCY)) CURRENCY, B.C901_CURRENCY CURRENCYID");
      
      sbQuery
          .append(" ,DECODE(get_code_name(GET_ACCOUNT_ATTRB_VALUE(B.c704_account_id,5507)),' ',get_code_name(B.C901_CURRENCY), get_code_name(GET_ACCOUNT_ATTRB_VALUE(B.c704_account_id,5507))) CURRSIGN ");
      
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE (b.c704_account_id, 10304532) invoicelayout");
      
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE (b.c704_account_id, 106642) VENDORID");
      
      sbQuery.append(" , GET_ACCOUNT_ATTRB_VALUE(B.C704_ACCOUNT_ID, 101192) EANLOCATION");
      
      
      sbQuery
          .append(" ,get_rule_value_by_company(B.C901_CURRENCY, 'ACC_CURR_DATEFMT', C.C1900_COMPANY_ID) acc_curr_datefmt ");
     
      sbQuery
          .append(" , GET_ACCOUNT_ATTRB_VALUE (b.c704_account_id, 10306194) CIG ,  GET_CODE_NAME(b.c704_ship_state) SHIPSTATE"); // to
                                                                                                                                 // get
                                                                                                                                 // shipped
                                                                                                                                 // state
                                                                                                                                 // value
      sbQuery
          .append(" , GET_ACCOUNT_ATTRB_VALUE (b.c704_account_id, 5504) CUST_TYPE ,GET_ACCOUNT_NAME(B.c704_account_id) ACCOUNTNAME,get_account_address(B.c704_account_id) ACCADDRESS ");
      
      sbQuery.append(" , get_rule_value_by_company('DE_INV_AD_NM','DE_INV_INFO','" + strCompanyId
          + "') GMNAME ");
   
    }

    sbQuery
        .append(" , NVL(C.C503_INVOICE_ID,'TO-BE') INVFL , C.C503_INVOICE_ID INV , '' PDFINVOICE");
   
    sbQuery
        .append(" ,get_payment_terms(C.C503_invoice_date,C.c503_terms) PAYMENTDUEDATE , C.C503_CUSTOMER_PO PO ");
  
    sbQuery.append(" , '' SHIPADD ,");
    
    sbQuery.append(" decode(C.C503_INVOICE_DATE,'',CURRENT_DATE,C.C503_INVOICE_DATE) INVDT, ");
   
    sbQuery
        .append(" decode(C.C503_INVOICE_DATE,'',(CURRENT_DATE+30),(C.C503_INVOICE_DATE+30)) DUEDT , C.C503_STATUS_FL SFL ");
  
    sbQuery
        .append(" , C.C901_INVOICE_TYPE INVTYPE, GET_CODE_NAME_ALT(C.C901_INVOICE_TYPE) INVTITLE ");
    
    sbQuery.append(" , DECODE(C.C901_INVOICE_TYPE,50202,GET_PARENT_INVOICE_ID ('");
    
    sbQuery.append(strInvoiceId);
    
    sbQuery.append("'),50203,GET_PARENT_INVOICE_ID ('");
    
    sbQuery.append(strInvoiceId);
    
    sbQuery.append("'),'') PARENT_INV_ID ");
   
    sbQuery.append(" ,GET_LOG_FLAG(C.C503_INVOICE_ID,1201) CALL_FLAG ");
    
    sbQuery.append(" ,C503_COMMENTS COMMENTS ");
   
    sbQuery.append(" ,get_distributor_type (C701_DISTRIBUTOR_ID) disttype ");
    
    sbQuery.append(" ,GET_DISTRIBUTOR_INTER_PARTY_ID(C701_DISTRIBUTOR_ID) PARTYID ,'' shipdate");
    
    sbQuery.append(" , '' RMA_ID");
   
    sbQuery
        .append(" , ''  TAX_START_DATE_FL");
    
    sbQuery.append(" , ''  ADJ_START_DATE_FL");

    sbQuery
        .append(" , c.C1900_COMPANY_ID company_id, get_company_code (c.C1900_COMPANY_ID) comp_code");

    
    sbQuery
        .append(" , C.C503_INV_AMT inv_amt ,C.c101_dealer_id dealerid");
    
    sbQuery
        .append(" , get_rule_value_by_company (C.c1900_company_id, 'VAT_RATE', C.c1900_company_id) COMP_VAT_RATE ");
    
    sbQuery.append(" , C.c1910_division_id DIVISION_ID , '' SURGERY_DATE ");
    if (strInvSrc.equals("26240213")) {

      sbQuery.append(" FROM t101_party t101,t101_party_invoice_details t101p ");

    } else {
      sbQuery.append(" FROM T704_ACCOUNT B ");
    }

    sbQuery.append(" ,T503_INVOICE C ,t1900_company T1900");
    
    sbQuery.append(" WHERE C.C503_INVOICE_ID = '");
    
    sbQuery.append(strInvoiceId);

    if (strInvSrc.equals("26240213")) {

      sbQuery
          .append("' AND t101.c101_party_id = t101p.c101_party_id AND t101.c101_party_id   = c.c101_dealer_id ");
      
      sbQuery.append(" AND t101.c101_void_fl IS NULL AND t101p.c101_void_fl IS NULL");

    } else {
     
      sbQuery.append("' AND B.C704_ACCOUNT_ID = C.C704_ACCOUNT_ID  ");
    
    }

    sbQuery.append(" AND C.C1900_COMPANY_ID = '" + strCompanyId + "'");
    
    sbQuery.append(" AND C.C1900_COMPANY_ID = T1900.C1900_COMPANY_ID");
    
    sbQuery.append(" AND C.C503_VOID_FL IS NULL ");


    log.debug(" Query to load invoice statement details " + sbQuery.toString());
    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    return hmResult;
  }

}

