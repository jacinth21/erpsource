package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmARSummarySnapShotBean extends GmBean implements GmARSummaryInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * 
   */

  public GmARSummarySnapShotBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmARSummarySnapShotBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }


  /**
   * reportARSummaryByDueDate - This method is used to fetch AR Summary Information based on due
   * date,
   * 
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  @Override
  public ArrayList reportARSummaryByDueDate(HashMap hmParam) throws AppError {
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
    if (gmDataStoreVO == null) {
      gmDataStoreVO = getGmDataStoreVO();
    }
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);

    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    String strAccId = "";
    strAccId = (String) hmParam.get("ACCID");
    String strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURCE"));
    String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("SALESFILTER"));
    String strPaySince = GmCommonClass.parseNull((String) hmParam.get("PAYSINCEDT"));
    String strCollectorId = GmCommonClass.parseZero((String) hmParam.get("COLLECTORID"));
    String strGroupId = GmCommonClass.parseZero((String) hmParam.get("GROUPID"));
    String strParentFl = GmCommonClass.parseNull((String) hmParam.get("PARENTCHKFL"));
    String strDateFormat = gmDataStoreVO.getCmpdfmt();
    String strARCurrId = GmCommonClass.parseNull((String) hmParam.get("COMP_CURR"));
    String strLanguageId = GmCommonClass.parseNull(gmDataStoreVO.getCmplangid());
    String strDivisionId = GmCommonClass.parseZero((String) hmParam.get("COMP_DIV_ID"));
    strDivisionId = strDivisionId.equals("0") ? "" : strDivisionId;
    String strArAging = GmCommonClass.parseZero((String) hmParam.get("AGING_LOCK_ID"));
    String strCompanyID = gmDataStoreVO.getCmpid();
    if (!strInvSource.equals("26240213")) {

      // ALL - Include the company customers and Company Dealers. (PMT-14375)
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" SELECT   DECODE('" + strParentFl
            + "','Y', C101_PAR_ACCOUNT_ID,C704_ACCOUNT_ID)  ID  ,   ");

      } else {
        sbQuery
            .append(" SELECT   DECODE( C901_INVOICE_SOURCE,'50256',C701_DISTRIBUTOR_ID ,DECODE('"
                + strParentFl + "','Y', C101_PAR_ACCOUNT_ID,C704_ACCOUNT_ID))  ID  ,   ");
      }
      sbQuery.append(" DECODE(C901_INVOICE_SOURCE,'50256',C701_DISTRIBUTOR_NAME,DECODE('"
          + strParentFl + "','Y', C101_PAR_ACCOUNT_NM,DECODE(" + strLanguageId
          + ",'103097',NVL(C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM),C704_ACCOUNT_NM)))  NAME  , ");

      sbQuery
          .append(" C5031_terms_name TERM ,GET_CODE_NAME(C901_INVOICE_SOURCE) INV_SRC ,C901_INVOICE_SOURCE INV_SRC_ID , ");
      sbQuery.append(" C901_ACC_CURRENCY_NM CURRENCY ,");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 1 ), -1,"
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0 )),0) CURRENTVALUE  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 0 ), 1, "
              + "DECODE(SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 31 ), -1, "
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0) ,0 )),0) ONE_THIRTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 30 ), 1,"
              + " DECODE(SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 61 ), -1, "
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0) ,0 )),0) THIRTYONE_SIXTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 60 ), 1,"
              + " DECODE(SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 91 ), -1, "
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0) ,0 )),0) SIXTYONE_NINETY , ");

      // generateAdditionalIntervalsByDueDate method is used to create new intervals based on input.
      sbQuery.append(generateAdditionalIntervalsByDueDateSnapShot(hmParam));
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 120),1,"
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0 )),0) GREATER_ONETWENTY , ");
      sbQuery.append(" NVL(SUM( (C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0))),0)  ROW_TOTAL ");

      sbQuery
          .append(", '' Region    , C704_ACCT_CREDIT_RATING Credit_Rating, C704_CREDIT_RATING_DATE  Last_Credit_Updated_Date");
      // When Show Parent fl is Y, we cannot include the last payment date,as the records will be
      // having multiple receive date for the rep account.
      // Las Payment Date is applicable only when the report is loaded for Rep Account only.
      sbQuery.append(" ,DECODE('" + strParentFl + "','Y','',to_char(C5031_LAST_PAYMENT_REC_DATE,'"
          + strDateFormat + "') ) last_payment_received_date ");
      sbQuery.append(" , GET_CODE_NAME(C901_COLLECTOR_ID ) COLLECTOR_NAME  ");
      sbQuery.append(" , GET_CODE_NAME(C901_CREDIT_TYPE ) CREDITTYPE_NAME ");
      sbQuery.append(" , C704A_CREDIT_LIMIT CREDITLIMIT");
      sbQuery.append(" ,C1910_DIVISION_NAME DIVISION_NAME ");
      sbQuery.append(" ,c1910_DIVISION_ID DIVISION_ID ");

      sbQuery.append(" FROM T5030_AR_AGING_LOCK t5030, T5031_AR_AGING_DETAIL t5031  ");
      sbQuery.append(" Where t5030.c5030_ar_aging_lock_id = t5031.c5030_ar_aging_lock_id ");
      sbQuery.append(" AND t5030.c5030_ar_aging_lock_id = " + strArAging);
      sbQuery.append("   AND t5030.C1900_COMPANY_ID =" + strCompanyID);


      // If Type is 50255 =" company customers" we are going filter based on currency .
      if (strInvSource.equals("50255")) {
        sbQuery.append(" AND C901_ACC_CURRENCY_ID ='" + strARCurrId + "'");
      }

      // if show parent account box is not checked only that particular account information will
      // going
      // to show.
      if (!strAccId.equals("") && !strAccId.equals("0") && strParentFl.equals("")) {

        if (strInvSource.equals("50255") || strInvSource.equals("50257")
            || strInvSource.equals("50253")) {
          /*
           * The Changes made for PMT-834.Since Account Dropdown is changed to list accounts for ICS
           * the data fetched should be based on Accounts.
           */
          sbQuery.append(" AND C704_ACCOUNT_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        } else {// if (strInvSource.equals("50256")) {
          sbQuery.append(" AND C701_DISTRIBUTOR_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        }

      }



      if (!strSalesFilter.equals("")) {
        sbQuery
            .append(" AND  C704_ACCOUNT_ID in (SELECT DISTINCT ac_id FROM v700_territory_mapping_detail v700 WHERE ");
        sbQuery.append(strSalesFilter);
        sbQuery.append(" )");
      }
      // 7008 : Group
      if (!strGroupId.equals("0")) {
        sbQuery
            .append(" AND  C704_ACCOUNT_ID in (SELECT  t704a.c704_account_id ID FROM T704A_ACCOUNT_ATTRIBUTE t704a WHERE "
                + " t704a.c704a_attribute_value ='");
        sbQuery.append(strGroupId);
        sbQuery.append("' and t704a.c901_attribute_type='7008' and t704a.c704a_VOID_FL is null)");
      }

      // ALL - Getting the Company Customers (50255) invoices using UNION ALL
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" AND C901_INVOICE_SOURCE = '50255'");
      } else if (!strInvSource.equals("") && !strInvSource.equals("ALL")) {
        sbQuery.append(" AND C901_INVOICE_SOURCE = ");
        sbQuery.append(strInvSource);
      }
      if (!strCollectorId.equals("0")) {
        sbQuery.append(" AND C901_COLLECTOR_ID = '");
        sbQuery.append(strCollectorId);
        sbQuery.append("' ");
      }
      if (!strPaySince.equals("")) {
        sbQuery.append(" and (C5031_LAST_PAYMENT_REC_DATE <= to_date('");
        sbQuery.append(strPaySince);
        sbQuery.append("','" + strDateFormat + "') OR C5031_LAST_PAYMENT_REC_DATE IS NULL) ");
      }

      if (!strDivisionId.equals("")) {
        sbQuery.append(" AND c1910_DIVISION_ID =  " + strDivisionId + " ");
      }


      // if show parent account is checked ,all rep accounts related information will going to show.

      if ((!strAccId.equals("")) && (!strAccId.equals("0")) && !strParentFl.equals("")) {
        sbQuery
            .append(" AND C101_PAR_ACCOUNT_ID = (select c101_party_id from t704_account where C704_ACCOUNT_ID = '"
                + strAccId + "') ");

      }
      sbQuery
          .append(" GROUP BY DECODE('"
              + strParentFl
              + "','Y', C101_PAR_ACCOUNT_ID,C704_ACCOUNT_ID) ,C901_ACC_CURRENCY_NM ,DECODE('"
              + strParentFl
              + "','Y', C101_PAR_ACCOUNT_NM,DECODE("
              + strLanguageId
              + ",'103097',NVL(C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM),C704_ACCOUNT_NM)),C701_DISTRIBUTOR_ID, C701_DISTRIBUTOR_NAME,"
              + "C901_INVOICE_SOURCE,C5031_terms_name  ");
      // When Show Parent fl is Y, we cannot include the last payment date,as the records will be
      // having multiple receive date for the rep account.
      // Las Payment Date is applicable only when the report is loaded for Rep Account only.
      sbQuery.append(" ,DECODE('" + strParentFl + "','Y','',to_char(C5031_LAST_PAYMENT_REC_DATE,'"
          + strDateFormat + "') ) ");

      sbQuery
          .append(" ,C704_ACCT_CREDIT_RATING,C704_CREDIT_RATING_DATE,C901_COLLECTOR_ID , C901_CREDIT_TYPE , C704A_CREDIT_LIMIT ,C1910_DIVISION_NAME, c1910_DIVISION_ID ");

      if (!strInvSource.equals("ALL")) {
        sbQuery.append(" ORDER BY NAME, C701_DISTRIBUTOR_NAME  ");
      }
      // if Invoice source is company dealer the below query will execute
    }

    // ALL - Include the company customers and Company Dealers. (PMT-14375)
    if (strInvSource.equals("ALL")) {
      sbQuery.append(" UNION ALL");
    }

    if (strInvSource.equals("26240213") || strInvSource.equals("ALL")) {

      sbQuery.append(" SELECT  C101_DEALER_ID ID  ,   ");
      sbQuery.append(" DECODE(" + strLanguageId
          + ",'103097',NVL(C101_DEALER_NM_EN,C101_DEALER_NM),C101_DEALER_NM) NAME  , ");

      sbQuery
          .append(" C5031_terms_name TERM , GET_CODE_NAME(C901_INVOICE_SOURCE) INV_SRC ,C901_INVOICE_SOURCE INV_SRC_ID, ");
      sbQuery.append(" C901_ACC_CURRENCY_NM CURRENCY ,");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 1 ), -1,"
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0 )),0) CURRENTVALUE  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 0 ), 1, "
              + "DECODE(SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 31 ), -1, "
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0) ,0 )),0) ONE_THIRTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 30 ), 1,"
              + " DECODE(SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 61 ), -1, "
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0) ,0 )),0) THIRTYONE_SIXTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 60 ), 1,"
              + " DECODE(SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 91 ), -1,"
              + " (C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0) ,0 )),0) SIXTYONE_NINETY , ");
      sbQuery.append(generateAdditionalIntervalsByDueDateSnapShot(hmParam));
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC (get_payment_terms (t5031.c503_invoice_dt, t5031.c901_terms_id)) - 120),1,"
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0 )),0) GREATER_ONETWENTY , ");
      sbQuery.append(" NVL(SUM( (C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0))),0)  ROW_TOTAL ");

      sbQuery
          .append(", '' Region    , C704_ACCT_CREDIT_RATING Credit_Rating, C704_CREDIT_RATING_DATE  Last_Credit_Updated_Date");
      sbQuery.append(" ,to_char(C5031_LAST_PAYMENT_REC_DATE,'" + strDateFormat
          + "') last_payment_received_date ");
      sbQuery.append(" , GET_CODE_NAME(C901_DEALER_COLLECTOR_ID) COLLECTOR_NAME  ");
      sbQuery.append(" , '' CREDITTYPE_NAME ");
      sbQuery.append(" , '' CREDITLIMIT ");
      sbQuery.append(" ,  C1910_DIVISION_NAME DIVISION_NAME ");
      sbQuery.append(" , c1910_DIVISION_ID DIVISION_ID ");

      sbQuery.append(" FROM T5030_AR_AGING_LOCK t5030, T5031_AR_AGING_DETAIL t5031  ");
      sbQuery.append(" Where t5030.c5030_ar_aging_lock_id = t5031.c5030_ar_aging_lock_id ");
      sbQuery.append(" AND t5030.c5030_ar_aging_lock_id = " + strArAging);
      sbQuery.append("   AND t5030.C1900_COMPANY_ID =" + strCompanyID);
      if (!strAccId.equals("") && !strAccId.equals("0")) {
        sbQuery.append(" AND C101_DEALER_ID ='" + strAccId + "'");
      }
      // To load the Dealer invoices based on more filter, We cannot have direct mapping for dealer
      // and region.So , checking against the accounts associated to that invoice.
      if (!strSalesFilter.equals("")) {
        sbQuery
            .append(" AND C503_INVOICE_ID IN (SELECT c503_invoice_id FROM T501_Order WHERE C704_Account_Id IN ");
        sbQuery.append(" (SELECT  V700.Ac_Id FROM v700_territory_mapping_detail v700 WHERE ");
        sbQuery.append(strSalesFilter);
        sbQuery.append(" ) AND C501_VOID_FL IS NULL) ");

      }

      // ALL - Getting the Company Dealer (26240213) invoices using UNION ALL
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" AND C901_INVOICE_SOURCE = '26240213'");
      } else if (!strInvSource.equals("") && !strInvSource.equals("ALL")) {

        sbQuery.append(" AND C901_INVOICE_SOURCE = ");
        sbQuery.append(strInvSource);
      }

      if (!strCollectorId.equals("0")) {
        sbQuery.append(" AND C901_DEALER_COLLECTOR_ID = '");
        sbQuery.append(strCollectorId);
        sbQuery.append("' ");
      }
      if (!strPaySince.equals("")) {
        sbQuery.append(" and (C5031_LAST_PAYMENT_REC_DATE <= to_date('");
        sbQuery.append(strPaySince);
        sbQuery.append("','" + strDateFormat + "') OR C5031_LAST_PAYMENT_REC_DATE IS NULL) ");
      }
      if (!strDivisionId.equals("")) {
        sbQuery.append(" AND c1910_DIVISION_ID =  " + strDivisionId + " ");
      }
      sbQuery
          .append(" GROUP BY C101_DEALER_ID , C901_ACC_CURRENCY_NM , C101_DEALER_NM,C101_DEALER_NM_EN,");
      sbQuery.append(" C5031_terms_name ,'',C704_ACCT_CREDIT_RATING, C704_CREDIT_RATING_DATE,");
      sbQuery
          .append(" C901_DEALER_COLLECTOR_ID, C901_INVOICE_SOURCE, C5031_LAST_PAYMENT_REC_DATE ,C1910_DIVISION_NAME,   c1910_DIVISION_ID ");
      if (!strInvSource.equals("ALL")) {
        sbQuery.append(" ORDER BY C101_DEALER_NM");
      }
    }

    if (strInvSource.equals("ALL")) {
      sbQuery.append(" ORDER BY NAME");
    }

    log.debug("Query for AR Summary: " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }


  /**
   * reportARSummaryByInvDate - This method is used to fetch AR Summary Information based on invoice
   * date,
   * 
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  @Override
  public ArrayList reportARSummaryByInvDate(HashMap hmParam) throws AppError {
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
    if (gmDataStoreVO == null) {
      gmDataStoreVO = getGmDataStoreVO();
    }
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);

    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    String strAccId = "";
    strAccId = (String) hmParam.get("ACCID");
    String strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURCE"));
    String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("SALESFILTER"));
    String strPaySince = GmCommonClass.parseNull((String) hmParam.get("PAYSINCEDT"));
    String strCollectorId = GmCommonClass.parseZero((String) hmParam.get("COLLECTORID"));
    String strGroupId = GmCommonClass.parseZero((String) hmParam.get("GROUPID"));
    String strDateFormat = gmDataStoreVO.getCmpdfmt();
    String strParentFl = GmCommonClass.parseNull((String) hmParam.get("PARENTCHKFL"));
    String strARCurrId = GmCommonClass.parseNull((String) hmParam.get("COMP_CURR"));
    String strLanguageId = GmCommonClass.parseNull(gmDataStoreVO.getCmplangid());
    String strDivisionId = GmCommonClass.parseZero((String) hmParam.get("COMP_DIV_ID"));
    String strArAging = GmCommonClass.parseZero((String) hmParam.get("AGING_LOCK_ID"));
    strDivisionId = strDivisionId.equals("0") ? "" : strDivisionId;
    String strCompanyID = gmDataStoreVO.getCmpid();
    if (!strInvSource.equals("26240213")) {
      // ALL - Include the company customers and Company Dealers. (PMT-14375)
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" SELECT   DECODE('" + strParentFl
            + "','Y', C101_PAR_ACCOUNT_ID,C704_ACCOUNT_ID)  ID  ,   ");

      } else {
        sbQuery
            .append(" SELECT   DECODE( C901_INVOICE_SOURCE,'50256',C701_DISTRIBUTOR_ID ,DECODE('"
                + strParentFl + "','Y', C101_PAR_ACCOUNT_ID,C704_ACCOUNT_ID))  ID  ,   ");
      }

      sbQuery.append(" DECODE(C901_INVOICE_SOURCE,'50256',C701_DISTRIBUTOR_NAME,DECODE('"
          + strParentFl + "','Y', C101_PAR_ACCOUNT_NM,DECODE(" + strLanguageId
          + ",'103097',NVL(C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM),C704_ACCOUNT_NM)))  NAME  , ");
      sbQuery
          .append(" C5031_terms_name TERM ,GET_CODE_NAME(C901_INVOICE_SOURCE) INV_SRC , C901_INVOICE_SOURCE INV_SRC_ID, ");
      sbQuery.append(" C901_ACC_CURRENCY_NM CURRENCY ,");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - 31),-1,"
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0 )),0) ZERO_THIRTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - 30 ),  1,"
              + " DECODE(SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - 61 ), -1, "
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0) ,0 )),0) THIRTYONE_SIXTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - 60 ),  1, "
              + "DECODE(SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - 91 ), -1, "
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0) ,0 )),0) SIXTYONE_NINETY  , ");
      // generateAdditionalIntervalsByInvDate method is used to create new intervals based on input.
      sbQuery.append(generateAdditionalIntervalsByInvDateSnapShot(hmParam));

      sbQuery.append(" NVL(SUM( (C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0))),0)  ROW_TOTAL ");

      sbQuery
          .append(", '' Region    , C704_ACCT_CREDIT_RATING Credit_Rating, C704_CREDIT_RATING_DATE  Last_Credit_Updated_Date");

      // When Show Parent fl is Y, we cannot include the last payment date,as the records will be
      // having multiple receive date for the rep account.
      // Las Payment Date is applicable only when the report is loaded for Rep Account only.
      sbQuery.append(" ,DECODE('" + strParentFl + "','Y','',to_char(C5031_LAST_PAYMENT_REC_DATE,'"
          + strDateFormat + "') ) last_payment_received_date ");
      sbQuery.append(" , GET_CODE_NAME(C901_COLLECTOR_ID ) COLLECTOR_NAME  ");
      sbQuery.append(" , GET_CODE_NAME(C901_CREDIT_TYPE ) CREDITTYPE_NAME ");
      sbQuery.append(" , C704A_CREDIT_LIMIT CREDITLIMIT ");
      sbQuery.append(" ,C1910_DIVISION_NAME DIVISION_NAME ");
      sbQuery.append(" ,c1910_DIVISION_ID DIVISION_ID ");

      sbQuery.append(" FROM T5030_AR_AGING_LOCK t5030, T5031_AR_AGING_DETAIL t5031 ");
      sbQuery.append(" Where t5030.c5030_ar_aging_lock_id = t5031.c5030_ar_aging_lock_id ");
      sbQuery.append(" AND t5030.c5030_ar_aging_lock_id = " + strArAging);
      sbQuery.append("   AND t5030.C1900_COMPANY_ID =" + strCompanyID);
      // If Type is 50255 =" company customers" we are going filter based on currency .
      if (strInvSource.equals("50255")) {
        sbQuery.append(" And C901_ACC_CURRENCY_ID ='" + strARCurrId + "'");
      }

      // if show parent account is checked ,all rep accounts related information will going to show.
      if (!strAccId.equals("") && !strAccId.equals("0") && strParentFl.equals("")) {

        if (strInvSource.equals("50255") || strInvSource.equals("50257")
            || strInvSource.equals("50253")) {
          /*
           * The Changes made for PMT-834.Since Account Dropdown is changed to list accounts for ICS
           * the data fetched should be based on Accounts.
           */
          sbQuery.append(" AND C704_ACCOUNT_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        } else {// if (strInvSource.equals("50256")) {
          sbQuery.append(" AND C701_DISTRIBUTOR_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        }

      }



      if (!strSalesFilter.equals("")) {
        sbQuery
            .append(" AND  C704_ACCOUNT_ID in (SELECT DISTINCT ac_id FROM v700_territory_mapping_detail v700 WHERE ");
        sbQuery.append(strSalesFilter);
        sbQuery.append(" )");
      }
      // 7008 : Group
      if (!strGroupId.equals("0")) {
        sbQuery
            .append(" AND  C704_ACCOUNT_ID in (SELECT  t704a.c704_account_id ID FROM T704A_ACCOUNT_ATTRIBUTE t704a "
                + "WHERE  t704a.c704a_attribute_value ='");
        sbQuery.append(strGroupId);
        sbQuery.append("' and t704a.c901_attribute_type='7008' and t704a.c704a_VOID_FL is null)");
      }
      // ALL - Getting the Company Customers (50255) invoices using UNION ALL
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" AND C901_INVOICE_SOURCE = '50255'");
      } else if (!strInvSource.equals("") && !strInvSource.equals("ALL")) {
        sbQuery.append(" AND C901_INVOICE_SOURCE = ");
        sbQuery.append(strInvSource);
      }
      if (!strCollectorId.equals("0")) {
        sbQuery.append(" AND C901_COLLECTOR_ID = '");
        sbQuery.append(strCollectorId);
        sbQuery.append("' ");
      }
      if (!strPaySince.equals("")) {
        sbQuery.append(" and (C5031_LAST_PAYMENT_REC_DATE <= to_date('");
        sbQuery.append(strPaySince);
        sbQuery.append("','" + strDateFormat + "') OR C5031_LAST_PAYMENT_REC_DATE IS NULL) ");
      }

      if (!strDivisionId.equals("")) {
        sbQuery.append(" AND c1910_DIVISION_ID = " + strDivisionId + " ");
      }

      // if show parent account box is not checked only that particular account information will
      // going
      // to show.

      if ((!strAccId.equals("")) && (!strAccId.equals("0")) && !strParentFl.equals("")) {
        sbQuery
            .append(" AND C101_PAR_ACCOUNT_ID = (select c101_party_id from t704_account where C704_ACCOUNT_ID = '"
                + strAccId + "') ");

      }

      sbQuery
          .append(" GROUP BY DECODE('"
              + strParentFl
              + "','Y', C101_PAR_ACCOUNT_ID,C704_ACCOUNT_ID) ,C901_ACC_CURRENCY_NM ,DECODE('"
              + strParentFl
              + "','Y', C101_PAR_ACCOUNT_NM,DECODE("
              + strLanguageId
              + ",'103097',NVL(C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM),C704_ACCOUNT_NM)),C701_DISTRIBUTOR_ID, "
              + "C701_DISTRIBUTOR_NAME,C901_INVOICE_SOURCE,C5031_terms_name   ");
      // When Show Parent fl is Y, we cannot include the last payment date,as the records will be
      // having multiple receive date for the rep account.
      // Las Payment Date is applicable only when the report is loaded for Rep Account only.
      sbQuery.append(" ,DECODE('" + strParentFl + "','Y','',to_char(C5031_LAST_PAYMENT_REC_DATE,'"
          + strDateFormat + "')) ");

      sbQuery
          .append(" ,'',C704_ACCT_CREDIT_RATING,C704_CREDIT_RATING_DATE,C901_COLLECTOR_ID , C901_CREDIT_TYPE , C704A_CREDIT_LIMIT , C1910_DIVISION_NAME,  c1910_DIVISION_ID");
      if (!strInvSource.equals("ALL")) {
        sbQuery.append(" ORDER BY NAME, C701_DISTRIBUTOR_NAME  ");
      }
    }
    // ALL - Include the company customers and Company Dealers. (PMT-14375)
    if (strInvSource.equals("ALL")) {
      sbQuery.append(" UNION ALL");
    }

    if (strInvSource.equals("26240213") || strInvSource.equals("ALL")) {

      sbQuery.append(" SELECT   C101_DEALER_ID  ID  ,   ");
      sbQuery.append(" DECODE(" + strLanguageId
          + ",'103097',NVL(C101_DEALER_NM_EN,C101_DEALER_NM),C101_DEALER_NM)  NAME  , ");
      sbQuery
          .append(" C5031_terms_name TERM , GET_CODE_NAME(C901_INVOICE_SOURCE) INV_SRC , C901_INVOICE_SOURCE INV_SRC_ID, ");
      sbQuery.append(" C901_ACC_CURRENCY_NM CURRENCY ,");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - 31), -1,"
              + "(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0 )),0) ZERO_THIRTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - 30 ),  1,"
              + "DECODE(SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - 61 ), -1,"
              + " (C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0) ,0 )),0) THIRTYONE_SIXTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - 60 ),  1,"
              + "DECODE(SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - 91 ), -1,"
              + " (C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0) ,0 )),0) SIXTYONE_NINETY  , ");
      sbQuery.append(generateAdditionalIntervalsByInvDateSnapShot(hmParam));

      sbQuery.append(" NVL(SUM( (C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0))),0)  ROW_TOTAL ");

      sbQuery
          .append(",'' Region    , C704_ACCT_CREDIT_RATING Credit_Rating, C704_CREDIT_RATING_DATE  Last_Credit_Updated_Date");

      sbQuery.append(" ,to_char(C5031_LAST_PAYMENT_REC_DATE,'" + strDateFormat
          + "') last_payment_received_date ");
      sbQuery.append(" , GET_CODE_NAME(C901_DEALER_COLLECTOR_ID) COLLECTOR_NAME  ");
      sbQuery.append(" , '' CREDITTYPE_NAME ");
      sbQuery.append(" , '' CREDITLIMIT ");
      sbQuery.append(", C1910_DIVISION_NAME DIVISION_NAME ");
      sbQuery.append(" ,c1910_DIVISION_ID DIVISION_ID ");

      sbQuery.append(" FROM T5030_AR_AGING_LOCK t5030, T5031_AR_AGING_DETAIL t5031 ");
      sbQuery.append(" Where t5030.c5030_ar_aging_lock_id = t5031.c5030_ar_aging_lock_id ");
      sbQuery.append(" AND t5030.c5030_ar_aging_lock_id = " + strArAging);
      sbQuery.append("   AND t5030.C1900_COMPANY_ID =" + strCompanyID);

      // To load the Dealer invoices based on more filter, We cannot have direct mapping for dealer
      // and region.So , checking against the accounts associated to that invoice.
      if (!strSalesFilter.equals("")) {
        sbQuery
            .append(" AND C503_INVOICE_ID IN (SELECT c503_invoice_id FROM T501_Order WHERE C704_Account_Id IN ");
        sbQuery.append(" (SELECT  V700.Ac_Id FROM v700_territory_mapping_detail v700 WHERE ");
        sbQuery.append(strSalesFilter);
        sbQuery.append(" ) AND C501_VOID_FL IS NULL) ");
      }
      // ALL - Getting the Company Dealer (26240213) invoices using UNION ALL
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" AND C901_INVOICE_SOURCE = '26240213'");
      } else if (!strInvSource.equals("") && !strInvSource.equals("ALL")) {

        sbQuery.append(" AND C901_INVOICE_SOURCE = ");
        sbQuery.append(strInvSource);
      }

      if (!strCollectorId.equals("0")) {
        sbQuery.append(" AND C901_DEALER_COLLECTOR_ID = '");
        sbQuery.append(strCollectorId);
        sbQuery.append("' ");
      }
      if (!strPaySince.equals("")) {
        sbQuery.append(" and (C5031_LAST_PAYMENT_REC_DATE <= to_date('");
        sbQuery.append(strPaySince);
        sbQuery.append("','" + strDateFormat + "') OR C5031_LAST_PAYMENT_REC_DATE IS NULL) ");
      }
      if (!strAccId.equals("") && !strAccId.equals("0")) {
        sbQuery.append(" AND C101_DEALER_ID ='" + strAccId + "'");
      }

      if (!strDivisionId.equals("")) {
        sbQuery.append("AND c1910_DIVISION_ID =" + strDivisionId + " ");
      }

      sbQuery
          .append(" GROUP BY C101_DEALER_ID , C901_ACC_CURRENCY_NM ,C101_DEALER_NM,C5031_terms_name ,C101_DEALER_NM_EN,");
      sbQuery
          .append(" '',C704_ACCT_CREDIT_RATING,C704_CREDIT_RATING_DATE, C901_DEALER_COLLECTOR_ID, C1910_DIVISION_NAME,   c1910_DIVISION_ID");
      sbQuery.append(", C901_INVOICE_SOURCE, C5031_LAST_PAYMENT_REC_DATE");
      if (!strInvSource.equals("ALL")) {
        sbQuery.append(" ORDER BY C101_DEALER_NM");
      }
    }

    if (strInvSource.equals("ALL")) {
      sbQuery.append(" ORDER BY NAME");
    }

    log.debug("Query for AR Summary: " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }



  /**
   * generateAdditionalIntervalsByDueDate - This method is used to create interval for AR Summary
   * Due date query,
   * 
   * @author Jignesh shah
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  public String generateAdditionalIntervalsByDueDateSnapShot(HashMap hmParam) throws AppError {
    /*
     * In this method we are creating intervals by adding invoice terms days
     */

    StringBuffer sbQuery = new StringBuffer();
    String strInterval = GmCommonClass.parseZero((String) hmParam.get("INTERVAL"));
    String strThrough = GmCommonClass.parseZero((String) hmParam.get("THROUGH"));
    String strDefaultThrough = GmCommonClass.parseZero((String) hmParam.get("DAFAULTTHROUGH"));
    String strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURCE"));
    String strPayCondition = "t5031.C901_TERMS_ID";

    int intInterval = Integer.parseInt(strInterval);
    int intThrough = Integer.parseInt(strThrough);
    int intTempInterval = 0;
    int intPrevInterval = Integer.parseInt(strDefaultThrough);
    intTempInterval = intPrevInterval + intInterval;
    while (intTempInterval <= intThrough) {

      sbQuery
          .append(" NVL(SUM(DECODE(SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC(get_payment_terms(C503_INVOICE_DT,"
              + strPayCondition
              + ")) - "
              + intPrevInterval
              + " ), 1, DECODE(SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(get_payment_terms(C503_INVOICE_DT,"
              + strPayCondition
              + ")) - "
              + (intTempInterval + 1)
              + " ), -1, (C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)),0) ,0 )),0) COL_"
              + (intPrevInterval + 1) + "_" + intTempInterval + " , ");

      log.debug("intPrevInterval=" + intPrevInterval + ":::intTempInterval=" + intTempInterval);
      intPrevInterval = intTempInterval;
      intTempInterval += intInterval;
    }
    sbQuery
        .append(" NVL(SUM(DECODE(  SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(get_payment_terms(C503_INVOICE_DT,"
            + strPayCondition
            + ")) - "
            + intPrevInterval
            + "),1,(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0 )),0) COL_GREATER_"
            + intPrevInterval + " , ");
    log.debug(" Query formed for generateAdditionalIntervals is " + sbQuery.toString());
    return (sbQuery.toString());
  }

  /**
   * generateAdditionalIntervalsByInvDate - This method is used to create interval for AR Summary
   * Invoice date query,
   * 
   * @author Jignesh shah
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  public String generateAdditionalIntervalsByInvDateSnapShot(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    String strInterval = GmCommonClass.parseZero((String) hmParam.get("INTERVAL"));
    String strThrough = GmCommonClass.parseZero((String) hmParam.get("THROUGH"));
    String strDefaultThrough = GmCommonClass.parseZero((String) hmParam.get("DAFAULTTHROUGH"));

    int intInterval = Integer.parseInt(strInterval);
    int intThrough = Integer.parseInt(strThrough);
    int intTempInterval = 0;
    int intPrevInterval = Integer.parseInt(strDefaultThrough);

    // code for query up to 61-91

    intTempInterval = intPrevInterval + intInterval;
    while (intTempInterval <= intThrough) {

      sbQuery
          .append(" NVL(SUM(DECODE(SIGN( TRUNC(t5030.c5030_lock_date) - TRUNC(C503_INVOICE_DT) - "
              + intPrevInterval
              + " ), 1, DECODE(SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - "
              + (intTempInterval + 1)
              + " ), -1, (C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)),0) ,0 )),0) COL_"
              + (intPrevInterval + 1) + "_" + intTempInterval + " , ");

      log.debug("intPrevInterval=" + intPrevInterval + ":::intTempInterval=" + intTempInterval);
      intPrevInterval = intTempInterval;
      intTempInterval += intInterval;
    }
    sbQuery
        .append(" NVL(SUM(DECODE(  SIGN( TRUNC(t5030.c5030_lock_date)  - TRUNC(C503_INVOICE_DT) - "
            + intPrevInterval
            + "),1,(C503_INVOICE_AMT - NVL(C503_INV_PAID_AMT,0)) ,0 )),0) COL_GREATER_"
            + intPrevInterval + " , ");
    log.debug(" Query formed for generateAdditionalIntervals is " + sbQuery.toString());
    return (sbQuery.toString());
  } // end of generateAdditionalIntervalsByInvDate()


  /**
   * reportARDetailByAccount - This method is used to fetch AR Summary Invoice details based on
   * account
   * 
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  @Override
  public RowSetDynaClass reportARDetailByAccount(HashMap hmParam) throws AppError {
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
    if (gmDataStoreVO == null) {
      gmDataStoreVO = getGmDataStoreVO();
    }
    String strRefId = GmCommonClass.parseNull((String) hmParam.get("hRefid"));
    String strReportType = GmCommonClass.parseNull((String) hmParam.get("REPORTTYPE"));
    String strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURSE"));
    String strFromDay = GmCommonClass.parseNull((String) hmParam.get("FROMDAY"));
    String strToDay = GmCommonClass.parseNull((String) hmParam.get("TODAY"));
    String strTxnTp = GmCommonClass.parseNull((String) hmParam.get("STRTXNTP"));
    String strDivId = GmCommonClass.parseNull((String) hmParam.get("DIV_COMP_ID"));
    String strArAging = GmCommonClass.parseZero((String) hmParam.get("AGING_LOCK_ID"));
    RowSetDynaClass resultSet = null;
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    gmDBManager.setPrepareString("gm_pkg_ac_ar_snap_shot.gm_fch_account_pedning_payment", 9);
    gmDBManager.setString(1, strArAging);
    gmDBManager.setString(2, strRefId);
    gmDBManager.setString(3, strReportType);
    gmDBManager.setString(4, strInvSource);
    gmDBManager.setString(5, strFromDay);
    gmDBManager.setString(6, strToDay);
    gmDBManager.setString(7, strTxnTp);
    gmDBManager.setString(8, strDivId);
    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
    gmDBManager.execute();
    resultSet = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(9));
    gmDBManager.close();
    return resultSet;

  }


}
