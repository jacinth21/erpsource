package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAPSageRptBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger

  public GmAPSageRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAPSageRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // Class.

  public RowSetDynaClass fetchSageVendorDtls(HashMap hmParam) throws AppError {

    RowSetDynaClass rdSageVendorResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strVendorName = GmCommonClass.parseNull((String) hmParam.get("VENDORNAME"));
    String strMissingVendorfl = GmCommonClass.parseNull((String) hmParam.get("MISSINGSAGEVENIDS"));

    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_ac_sage.gm_ac_fch_sage_vendor", 3);

    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.setString(1, strVendorName);
    gmDBManager.setString(2, strMissingVendorfl);

    gmDBManager.execute();
    rdSageVendorResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));

    gmDBManager.close();

    return rdSageVendorResult;

  }

  public RowSetDynaClass fetchDownloadInvoiceDtls(HashMap hmParam) throws AppError {

    RowSetDynaClass rdInvoiceDetails = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
    String strPONumber = GmCommonClass.parseNull((String) hmParam.get("PONUMBER"));
    String strInvoiceDateFrom = GmCommonClass.parseNull((String) hmParam.get("INVOICEDATEFROM"));
    String strInvoiceDateTo = GmCommonClass.parseNull((String) hmParam.get("INVOICEDATETO"));
    String strInvoiceNumber = GmCommonClass.parseNull((String) hmParam.get("INVOICENUMBER"));

    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_ac_sage.gm_ac_fch_download_invc_dtls", 6);
    // gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);

    gmDBManager.setString(1, strVendorId);
    gmDBManager.setString(2, strPONumber);
    gmDBManager.setString(3, strInvoiceDateFrom);
    gmDBManager.setString(4, strInvoiceDateTo);
    gmDBManager.setString(5, strInvoiceNumber);

    gmDBManager.execute();
    rdInvoiceDetails = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(6));

    gmDBManager.close();


    return rdInvoiceDetails;

  }
}
