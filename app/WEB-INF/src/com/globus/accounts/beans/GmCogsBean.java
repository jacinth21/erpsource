// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\beans\\GmCogsBean.java

package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * This class represent - to load/save the accounting COGS related data.
 * 
 * @author mmuthusamy
 *
 */
public class GmCogsBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @roseuid 48D03B9401CB
   */
  public GmCogsBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmCogsBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * @param hmParam
   * @return List
   * @roseuid 48CE90E00340
   */


  /**
   * Fetch the locked data lists from the DB for report.
   * 
   * @param String
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCogsLockReportLists(String strType) throws AppError {
    ArrayList alFormList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_lock_cogs_info.gm_fc_fch_cogslockids", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull(strType));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alFormList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    // log.debug("Exit");
    return alFormList;
  }

  /**
   * Fetch the locked data from the DB for report.
   * 
   * @param HashMap
   * @return RowSetDynaClass
   * @throws AppError
   */
  public RowSetDynaClass fetchlockedCogsDetail(HashMap Hmparam) throws AppError {
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_lock_cogs_info.gm_fch_locked_cogdetails", 4);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) Hmparam.get("PARTNUM")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) Hmparam.get("PROJECTID")));
    gmDBManager
        .setInt(3, Integer.parseInt(GmCommonClass.parseNull((String) Hmparam.get("LOCKID"))));
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    log.debug("b4 executing");
    gmDBManager.execute();
    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();

    return rdResult;
  }

  /**
   * fetchCogsErrorReport - Fetch the COGS error data based on the condition. To get the data from
   * T822 table and return the array list
   * <p>
   * PMT-19307: Added the filter condition - <b>COGS error type</b>. If error type is not equal to 0
   * then add the condition and fetch the data.
   * </p>
   * 
   * @param hmParam
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCogsErrorReport(HashMap hmParam) throws AppError {

    String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strPnum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strInvType = GmCommonClass.parseNull((String) hmParam.get("INVTYPE"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());
    String strCogsPlantId = GmCommonClass.parseZero((String) hmParam.get("COSTINGPLANTID"));
    String strCogsErrorType = GmCommonClass.parseZero((String) hmParam.get("COGSERRORTYPE"));

    log.debug("strFromDate   " + strFromDate + "  *");
    log.debug("strToDate   " + strToDate + "   *");
    log.debug("strStatus   " + strStatus + "   *");

    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strStatusDB = "";



    sbQuery.append("SELECT C822_COSTING_ERROR_LOG_ID LOGID, GET_CODE_NAME(c901_fix_type) STATUS,");
    sbQuery
        .append("C205_part_number_id PNUM, C822_TXN_ID TXNID, GET_CODE_NAME(C822_FROM_COSTING_TYPE) FID, ");
    sbQuery
        .append("GET_CODE_NAME(C822_TO_COSTING_TYPE) TID, C822_QTY QTY, C822_CREATED_DATE ERROR_DATE,");
    sbQuery
        .append("GET_USER_NAME(C822_CREATED_BY) ERROR_BY, c822_fixed_date FIXED_DATE,get_user_name(c822_fixed_by) FIXED_BY, ");
    sbQuery
        .append(" c5040_plant_id plantID, get_plant_name( c5040_plant_id) plantName, get_company_name (c1900_company_id) comp_name, c1900_company_id comp_id, ");
    sbQuery
        .append(" get_company_code (c1900_company_id) comp_cd, get_code_name (c901_error_type) COGSERRORTYPE ");
    sbQuery.append("FROM T822_COSTING_ERROR_LOG  ");

    if ((strStatus.equals("0") || strStatus.equals(""))
        && (strPnum.equals("") && strTxnId.equals("") && strInvType.equals(""))) {
      sbQuery.append("WHERE c901_fix_type IS NULL ");
    } else {
      sbQuery.append("WHERE C822_COSTING_ERROR_LOG_ID IS NOT NULL");
    }
    sbQuery.append(" AND c1900_company_id ='" + getCompId() + "'");
    if ((!strFromDate.equals("")) && (!strToDate.equals(""))) {
      sbQuery.append(" AND TRUNC(C822_CREATED_DATE) BETWEEN  to_date('");
      sbQuery.append(strFromDate);
      sbQuery.append("','" + strCompDateFmt + "') and to_date('");
      sbQuery.append(strToDate);
      sbQuery.append("','" + strCompDateFmt + "')");
    }

    if (!strPnum.equals("")) {
      sbQuery.append("  AND C205_part_number_id = '");
      sbQuery.append(strPnum);
      sbQuery.append("'");
    }

    if (!strTxnId.equals("")) {
      sbQuery.append(" AND C822_TXN_ID = '");
      sbQuery.append(strTxnId);
      sbQuery.append("'");
    }
    if ((!strInvType.equals("")) && (!strInvType.equals("0"))) {
      sbQuery.append(" AND C822_fROM_COSTING_TYPE = '");
      sbQuery.append(strInvType);
      sbQuery.append("'");
    }
    if (!strCogsPlantId.equals("0")) {
      sbQuery.append(" AND c5040_plant_id ='" + strCogsPlantId + "' ");
    }
    // to check Costing Error type
    if (!strCogsErrorType.equals("0")) {
      sbQuery.append(" AND c901_error_type ='" + strCogsErrorType + "' ");
    }

    if ((!strStatus.equals("")) && (!strStatus.equals("0"))) {
      sbQuery.append(" AND c901_fix_type = '");
      sbQuery.append(strStatus);
      sbQuery.append("'");
    } else {
      sbQuery.append(" AND c901_fix_type IS NULL ");
    }
    sbQuery.append(" ORDER BY PNUM ");


    // C822_CREATED_DATE BETWEEN to_date('07/07/2005','mm/dd/yyyy') and
    // to_date('07/29/2010','mm/dd/yyyy')
    log.debug("query====" + sbQuery);
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    gmDBManager.close();
    return alReturn;

  }

  /**
   * saveCorrectCOGSError - This method will save the COGS error details
   * 
   * @param String strInputString
   * @param String strUserId
   * @exception AppError
   */

  public void saveCorrectCOGSError(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_cogs_update.gm_sav_correct_cogs_err", 2);

    gmDBManager.setString(1, strInputStr);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

}
