package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAuditVerifyBean extends GmBean{

	Logger log = GmLogger.getInstance(this.getClass().getName());
	

	public GmAuditVerifyBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	public GmAuditVerifyBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}
	
	

	/*   This Method will get the Audit Entry Details for the Passed in Parameters
	 * 
	 * @param String .
	 * 
	 * @return hmReturn Containing the Audit Entry For the passed in AuditEntryID 
	 * @throws com.globus.common.beans.AppError	 * 
	 */
			
			
	public HashMap fetchAuditEntryByID(String strAuditEntryID)throws AppError{
		
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_fch_audit_entry_details", 2);
		gmDBManager.setString(1, strAuditEntryID);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		log.debug(" HashMap Returned from DB is "+hmReturn);
		gmDBManager.close();
		return hmReturn;
	}
	/*   This Method will Save the Audit Entry Details for the Passed in Parameters
	 * 
	 * @param HashMap Containing all the data to be saved in the Database..
	 *  
	 * @throws com.globus.common.beans.AppError	 * 
	 */
			
			
	public HashMap saveAuditEntry(HashMap hmAudEntValues)throws AppError{
		
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_sav_auditentrydetails", 12);
		
		gmDBManager.setString(1,(String)hmAudEntValues.get("TAGID"));
		gmDBManager.setString(2,(String)hmAudEntValues.get("PARTNUMBER"));
		gmDBManager.setString(3,(String)hmAudEntValues.get("CONTROLNUMBER"));
		gmDBManager.setString(4,(String)hmAudEntValues.get("SETID"));
		gmDBManager.setString(5,(String)hmAudEntValues.get("COMMENTS"));
		gmDBManager.setString(6,(String)hmAudEntValues.get("STATUSID"));
		gmDBManager.setString(7,(String)hmAudEntValues.get("BORROWEDFROM"));
		gmDBManager.setString(8,(String)hmAudEntValues.get("LOCATIONID"));
		gmDBManager.setString(9,(String)hmAudEntValues.get("LOCATIONDET"));
		gmDBManager.setString(10,(String)hmAudEntValues.get("COUNTEDDATE"));
		gmDBManager.setString(11,(String)hmAudEntValues.get("USERID"));
		gmDBManager.setString(12,(String)hmAudEntValues.get("AUDITENTRYID"));

		gmDBManager.execute();
		gmDBManager.commit();
		gmDBManager.close();
		return hmReturn;
	}	
	
/***
 * This method fetches the all the addresses of the sales rep for a given distributor
 * @param strDistID
 * @return
 * @throws AppError
 */	
public ArrayList fetchRepAddresses(String strDistID)throws AppError{
		
		ArrayList alAddress = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_fch_rep_addresses", 2);
		gmDBManager.setString(1, strDistID);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alAddress = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		log.debug(" ArrayList Returned from DB is "+alAddress);
		gmDBManager.close();
		return alAddress;
	}
}
