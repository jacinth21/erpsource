package com.globus.accounts.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.AppError;

/**
 * AR Summary and Lock report define the rule
 * 
 * @author mmuthusamy
 *
 */
public interface GmARSummaryInterface {

  /**
   * reportARSummaryByInvDate - This method is used to fetch AR Summary Information based on invoice
   * date,
   * 
   * @param hmParam
   * 
   * @return Array list
   * @exception AppError
   */

  public ArrayList reportARSummaryByInvDate(HashMap hmParam) throws AppError;

  /**
   * reportARSummaryByDueDate - This method is used to fetch AR Summary Information based on due
   * date,
   * 
   * @param hmParam
   * 
   * @return Array list
   * @exception AppError
   */
  public ArrayList reportARSummaryByDueDate(HashMap hmParam) throws AppError;

  /**
   * reportARDetailByAccount - This method is used to fetch AR Summary Invoice details based on
   * account
   * 
   * @param hmParam
   * 
   * @return RowsetDyna class
   * @exception AppError
   */
  public RowSetDynaClass reportARDetailByAccount(HashMap hmParam) throws AppError;

}
