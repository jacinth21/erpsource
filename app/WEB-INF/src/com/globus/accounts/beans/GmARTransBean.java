/**
 * 
 */
package com.globus.accounts.beans;

import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author mmuthusamy
 * 
 */
public class GmARTransBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  /**
   * @param gmDataStoreVO
   * @throws AppError
   */
  public GmARTransBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    // TODO Auto-generated constructor stub
    super(gmDataStoreVO);
  }

  /**
   * generateInvoice - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String generateInvoice(String strSessPO, String strSessAccId, String strPay,
      Date dtInvDate, String strUserId, String strAction, String strInvType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strInvoiceId = "";
    String strPrepareString = null;

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();
    strPay = strPay.equals("") ? "0" : strPay;

    gmDBManager.setPrepareString("GM_GENERATE_INVOICE", 9);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(9, java.sql.Types.CHAR);

    gmDBManager.setString(1, strSessPO);
    gmDBManager.setString(2, strSessAccId);
    gmDBManager.setInt(3, Integer.parseInt(strPay));
    gmDBManager.setDate(4, new java.sql.Date(dtInvDate.getTime()));
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strAction);
    gmDBManager.setString(7, strInvType);
    gmDBManager.setInt(8, Integer.parseInt("50255")); // US Customers

    gmDBManager.execute();
    strInvoiceId = gmDBManager.getString(9);
    gmDBManager.commit();
    log.debug(" New INvoice ID genereated is " + strInvoiceId);

    return strInvoiceId;
  } // end of generateInvoice

  /**
   * issueCredit - This method will Issue Credit through "Issue Credit" page
   * 
   * @param String strPO
   * @param String strAccId
   * @param String strAction
   * @return HashMap
   * @exception AppError
   */
  public String issueCredit(HashMap hmvatparam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
log.debug("hmvatparam>>>>>>>>>>>>"+hmvatparam);
    String strInvoiceId = "";
    String strOrderId = "";
    String strPrepareString = null;
    String strAccId = GmCommonClass.parseNull((String) hmvatparam.get("ACCID"));
    String strInputStr = GmCommonClass.parseNull((String) hmvatparam.get("INPUTSTRING"));
    String strTotal = GmCommonClass.parseNull((String) hmvatparam.get("TOTAL"));
    String strUserId = GmCommonClass.parseNull((String) hmvatparam.get("USERID"));
    String strComments = GmCommonClass.parseNull((String) hmvatparam.get("COMMENTS"));
    String strVatInputStr = GmCommonClass.parseNull((String) hmvatparam.get("VATINPUT"));
   
    
    gmDBManager.setPrepareString("GM_AC_UPD_ISSUECREDIT", 8);
   
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(8, java.sql.Types.CHAR);

    gmDBManager.setString(1, strAccId);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strTotal);
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, strComments);
    gmDBManager.setString(6, strVatInputStr);
    gmDBManager.execute();
    strInvoiceId = gmDBManager.getString(7);
    strOrderId = gmDBManager.getString(8);
    gmDBManager.commit();
    log.debug(" New Invoice ID is " + strInvoiceId + " new Order Id is " + strOrderId);


    return strInvoiceId;
  }

  /**
   * postMultiplePayments - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap postMultiplePayments(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    log.debug("Bean hmParam::" + hmParam);
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    Date dtPay = (Date) hmParam.get("PAYMENTDT");
    String strPayMode = GmCommonClass.parseZero((String) hmParam.get("STRPAYMODE"));
    String strDetails = GmCommonClass.parseNull((String) hmParam.get("STRDETAILS"));
    String strAmount = GmCommonClass.parseNull((String) hmParam.get("PAYMENTAMT"));
    String strSessAccId = GmCommonClass.parseNull((String) hmParam.get("STRSESSACCID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    log.error("Post payment total amount :" + strAmount);
    log.error("Post payment invoice string :" + strInputStr);


    gmDBManager.setPrepareString("GM_UPDATE_INVOICE_MULTIPLE", 7);
    gmDBManager.setString(1, strInputStr);
    gmDBManager.setDate(2, dtPay);
    gmDBManager.setInt(3, Integer.parseInt(strPayMode));
    gmDBManager.setString(4, strDetails);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strSessAccId);
    gmDBManager.setString(7, strAmount);

    gmDBManager.execute();
    gmDBManager.commit();
    // hmReturn.put("MSG", strMsg);
    hmReturn.put("MSG", "");
    return hmReturn;
  } // end of postMultiplePayments

  /**
   * saveCommentsOnInvoice - This method will update the comments on an Invoice
   * 
   * @param HashMap
   * @return
   * @exception AppError
   */
  public void saveCommentsOnInvoice(HashMap hmParams) throws AppError {
    String strInvoiceID = GmCommonClass.parseNull((String) hmParams.get("INVID"));
    String strComments = GmCommonClass.parseNull((String) hmParams.get("COMMENTS"));
    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    log.debug("hmParams in new method" + hmParams);
    // Database Connection
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    if (!strInvoiceID.equals("")) {
      gmDBManager.setPrepareString("GM_UPDATE_INVOICE_COMMENTS", 3);
      gmDBManager.setString(1, strInvoiceID);
      gmDBManager.setString(2, strComments);
      gmDBManager.setString(3, strUserID);

      gmDBManager.execute();
      gmDBManager.commit();
    }
  } // End of saveCommentsOnInvoice

  /**
   * saveIssueMemo - This method will save the issue memo
   * 
   * @param HashMap
   * 
   * @return HashMap
   * @exception AppError
   */

  public HashMap saveIssueMemo(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strMemoType = GmCommonClass.parseZero((String) hmParam.get("MEMOTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInvType = "CREDIT";
    String strInvoiceIDs = "";
    String strAdjOrderIDs = "";

    if (strMemoType.equals("50203")) { // 50203 - Debit
      strInvType = "DEBIT";
    }

    gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_sav_issue_memo_dtls", 5);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strInvType);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    strInvoiceIDs = GmCommonClass.parseNull(gmDBManager.getString(4));
    strAdjOrderIDs = GmCommonClass.parseNull(gmDBManager.getString(5));
    gmDBManager.commit();
    hmReturn.put("INVOICE", strInvoiceIDs);
    hmReturn.put("ORDER", strAdjOrderIDs);
    return hmReturn;
  } // end of saveIssueMemo

  /**
   * updateInvoice - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap updateInvoice(String strInv, String strSessAccId, String strSessPO,
      String strUserId, String strInputStr) throws AppError {

    String strBeanMsg = "";
    String strMsg = "";

    HashMap hmReturn = new HashMap();
    log.error("Post payment single invoice id :" + strInv);
    log.error("Post payment invoice string :" + strInputStr);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_AC_UPD_INV_SINGLE_PAY_MODE", 5);
    gmDBManager.setString(1, strInv);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserId);
    gmDBManager.setString(4, strSessAccId);
    gmDBManager.setString(5, strSessPO);

    gmDBManager.execute();
    gmDBManager.commit();
    hmReturn.put("MSG", "");
    log.debug(" Input String in Bean is " + strInputStr);

    return hmReturn;
  } // end of updateInvoice

  /**
   * updateNewPO - This method will be used to update the new PO
   * 
   * @param String strInv
   * @param String strAccId
   * @param String strOldPO
   * @param String strNewPO
   * @param String strUserId
   * @return HashMap
   * @exception AppError
   */
  public void updateNewPO(String strInv, String strAccId, String strOldPO, String strNewPO,
      String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_UPDATE_CUSTOMER_PO", 6);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);

    gmDBManager.setString(1, strAccId);
    gmDBManager.setString(2, strInv);
    gmDBManager.setString(3, strOldPO);
    gmDBManager.setString(4, strNewPO);
    gmDBManager.setString(5, strUserId);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(6);
    gmDBManager.commit();

    hmReturn.put("MSG", strMsg);


    // return hmReturn;
  } // end of updateNewPO

  /**
   * voidInvoice - This method will Void the Invoice
   * 
   * @param String strPO
   * @param String strAccId
   * @param String strAction
   * @return HashMap
   * @exception AppError
   */
  public void voidInvoice(String strInvoiceId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strMsg = "";
    String strPrepareString = null;

    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_AC_UPD_VOIDINVOICE", 1);
    /*
     * register out parameter and set input parameters
     */
    // csmt.registerOutParameter(7,java.sql.Types.CHAR);
    gmDBManager.setString(1, strInvoiceId);

    gmDBManager.execute();
    // strMsg = csmt.getString(7);
    gmDBManager.commit();

  } // End of voidInvoice
}
