/*
 * Module: GmAccountsBean.java Author: Dhinakaran James Project: Globus Medical
 * App Date-Written: 11 Mar 2004 Security: Unclassified Description: Accounts
 * Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What
 * you changed
 *  
 */

package com.globus.accounts.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;


import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmAccountsBean extends GmBean{
	GmCommonClass gmCommon = new GmCommonClass();
	
	  public GmAccountsBean() {
		    super(GmCommonClass.getDefaultGmDataStoreVO());
		  }
	
	public GmAccountsBean(GmDataStoreVO gmDataStoreVO) { //GmDataStoreVO gmDataStore
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	/**
	 * reportDashboard - This method will
	 * 
	 * @param String
	 *            strUsername
	 * @param String
	 *            strPassword
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap reportDashboard() throws AppError {
		GmCustomerBean gmCust = new GmCustomerBean();

		HashMap hmReturn = new HashMap();
		HashMap hmSales = new HashMap();
		ArrayList alReturnInvoice = new ArrayList();
		ArrayList alReturns = new ArrayList();

		hmSales = gmCust.todaysSales(); // Todays and Month to Date sales
										// figures
		hmReturn.put("SALES", hmSales);

		alReturnInvoice = reportDashboardInvoice(); // Invoices Dashboard
		hmReturn.put("INVOICE", alReturnInvoice);

		alReturns = reportDashboardReturns(); // Returns Dashboard
		hmReturn.put("RETURNS", alReturns);
		return hmReturn;

	} // ENd of reportDashboard

	/**
	 * reportDashboardInvoice - This method will
	 * 
	 * @param String
	 *            strUsername
	 * @param String
	 *            strPassword
	 * @return HashMap
	 * @exception AppError
	 */
	public ArrayList reportDashboardInvoice() throws AppError {
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		CallableStatement csmt = null;
		Connection conn = null;

		String strBeanMsg = "";
		String strMsg = "";
		String strPrepareString = null;

		ArrayList alReturn = new ArrayList();
		ResultSet rs = null;

		try {
			conn = dbCon.getConnection();
			strPrepareString = dbCon.getStrPrepareString(
					"GM_REPORT_INVOICE_DASHBOARD", 2);
			csmt = conn.prepareCall(strPrepareString);
			/*
			 * register out parameter and set input parameters
			 */
			csmt.registerOutParameter(1, java.sql.Types.CHAR);
			csmt.registerOutParameter(2, OracleTypes.CURSOR);

			csmt.execute();
			strBeanMsg = csmt.getString(1);
			rs = (ResultSet) csmt.getObject(2);
			alReturn = dbCon.returnArrayList(rs);

		} catch (Exception e) {
			GmLogError.log(
					"Exception in GmCustomerBean:reportDashboardInvoice",
					"Exception is:" + e);
			throw new AppError(e);
		} finally {
			try {
				if (csmt != null) {
					csmt.close();
				}//Enf of if (csmt != null)
				if (conn != null) {
					conn.close(); /* closing the Connections */
				}
			} catch (Exception e) {
				throw new AppError(e);
			}//End of catch
			finally {
				csmt = null;
				conn = null;
				dbCon = null;
			}
		}
		return alReturn;
	} // End of reportDashboardInvoice

	/**
	 * reportDashboardReturns - This method will
	 * 
	 * @param String
	 *            strUsername
	 * @param String
	 *            strPassword
	 * @return HashMap
	 * @exception AppError
	 */
	public ArrayList reportDashboardReturns() throws AppError {
		ArrayList alReturn = new ArrayList();
		try {

			DBConnectionWrapper dbCon = null;
			dbCon = new DBConnectionWrapper();

			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append(" SELECT	C506_RMA_ID RAID, C506_COMMENTS COMMENTS, ");
			sbQuery.append("to_char(C506_CREATED_DATE,'mm/dd/yyyy') CDATE, ");
			sbQuery.append(" GET_USER_NAME(C506_CREATED_BY) PER, ");
			sbQuery.append("GET_CODE_NAME(C506_TYPE) TYPE, C704_ACCOUNT_ID ACCID, ");
			sbQuery.append(" GET_CODE_NAME(C506_REASON) REASON, ");
			sbQuery.append("to_char(C506_EXPECTED_DATE,'mm/dd/yyyy') EDATE, ");
			sbQuery.append(" GET_ACCOUNT_NAME(C704_ACCOUNT_ID) ANAME, ");
			sbQuery.append(" C506_STATUS_FL STATUS_FL");
			sbQuery.append(" FROM T506_RETURNS ");
			sbQuery.append(" WHERE C506_STATUS_FL IN (0,1) AND C506_TYPE = '3300'");
			sbQuery.append(" ORDER BY C506_RMA_ID DESC");
			
			alReturn = dbCon.queryMultipleRecords(sbQuery.toString());

		} catch (Exception e) {
			GmLogError.log(
					"Exception in GmCustomerBean:reportDashboardReturns",
					"Exception is:" + e);
		}
		return alReturn;
	} //End of reportDashboardReturns

	
	/**
 	  * acctSalesReport - This method will 
 	  * @param String strUsername
	  * @param String strPassword
 	  * @return HashMap
 	  * @exception AppError
 	**/
	public HashMap acctSalesReport(String strFromDate, String strToDate) throws AppError
	{
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		CallableStatement csmt = null;
		Connection conn = null;

		String strBeanMsg = "";
		String strMsg = "";
		String strPrepareString = null;

		ArrayList alReturn = new ArrayList();
		HashMap hmReturn = new HashMap();
		ResultSet rs = null;

		try
		{
			conn = dbCon.getConnection();
			strPrepareString = dbCon.getStrPrepareString("GM_REPORT_ACCOUNT_ORDER",4);
			csmt = conn.prepareCall(strPrepareString);
			/*
			 *register out parameter and set input parameters
			 */
			csmt.registerOutParameter(3,java.sql.Types.CHAR);
			csmt.registerOutParameter(4,OracleTypes.CURSOR);

			csmt.setString(1,strFromDate);
			csmt.setString(2,strToDate);
			
			csmt.execute();
			
			strBeanMsg = csmt.getString(3);
			rs = (ResultSet)csmt.getObject(4);
			alReturn = dbCon.returnArrayList(rs);
			hmReturn.put("REPORT",alReturn);

		}catch(Exception e)
		{
			GmLogError.log("Exception in GmAccountBean:acctSalesReport","Exception is:"+e);
			throw new AppError(e);
		}
		finally
		{
			try
			{
				if (csmt != null)
				{
					csmt.close();
				}//Enf of if  (csmt != null)
				if(conn!=null)
				{
					conn.close();		/* closing the Connections */
				}
			}
			catch(Exception e)
			{
				throw new AppError(e);
			}//End of catch
			finally
			{
				 csmt = null;
				 conn = null;
				 dbCon = null;
			}
		}
		return hmReturn;
	} // End of acctSalesReport
	
	/**
	  * checkInvoiceXMLParams - Check for input in required fields when marking XML file in Invoice Parameter
	  * @return HashMap
	  * @exception AppError
	  * author : Shiny
	  * PMT    : 35167
	**/
	public void checkInvoiceXMLParams(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strAccId = GmCommonClass.parseNull((String) hmParam.get("RULEGRPID"));
	    String strInputString = GmCommonClass.parseNull((String) hmParam.get("STRINPUTSTRING"));
	    String strCompanyId = getGmDataStoreVO().getCmpid();
	    gmDBManager.setPrepareString("gm_pkg_sm_acct_trans.gm_chk_invoice_xml_param",3);
		gmDBManager.setString(1, strInputString); 
		gmDBManager.setString(2, strAccId); 
		gmDBManager.setString(3, strCompanyId); 
		gmDBManager.execute();
	    gmDBManager.close();
	}
	
}// end of class GmAccountsBean
