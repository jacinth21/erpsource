/*
 * Module: GmARBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 11 Mar
 * 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.accounts.beans;


import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmARReportBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public GmARReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmARReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }



  /**
   * loadInvoiceDetails - This method used to fetch invoice list for the selected filter condition
   * Account
   * 
   * @param HashMap hmFilterCondition Holds filter information
   * @return HashMap
   * @exception AppError
   **/
  public RowSetDynaClass reportInvoiceList(HashMap hmFilterCondition) throws AppError {

    ArrayList alInvoiceList = new ArrayList();
    HashMap hmResult = new HashMap();
    RowSetDynaClass resultSet = null;
    String strFrmDate = "";
    String strToDate = "";
    String strInvoiceType = "";
    String strAccId = "";
    String strInvoiceIDs = "";
    String strRepType = "";
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      strFrmDate = (String) hmFilterCondition.get("FromDate");
      strToDate = (String) hmFilterCondition.get("ToDate");
      strInvoiceType = (String) hmFilterCondition.get("INVTYPE");
      strAccId = (String) hmFilterCondition.get("ACCID");
      String strInvSource = GmCommonClass.parseNull((String) hmFilterCondition.get("INVSOURCE"));
      strInvoiceIDs = GmCommonClass.parseNull((String) hmFilterCondition.get("MEMO_INVOICE"));
      strRepType = GmCommonClass.parseNull((String) hmFilterCondition.get("INVREPTYPE"));
      String strCollectorId =
          GmCommonClass.parseZero((String) hmFilterCondition.get("COLLECTORID"));
      String strCreditType = GmCommonClass.parseZero((String) hmFilterCondition.get("CREDITTYPE"));
      String strParentFl = GmCommonClass.parseNull((String) hmFilterCondition.get("CHK_ACTIVEFL"));
      String strAccCurrId = GmCommonClass.parseZero((String) hmFilterCondition.get("ACC_CURR_ID"));
      String strDateType = GmCommonClass.parseZero((String) hmFilterCondition.get("DATETYPE"));
      String strDivisionId = GmCommonClass.parseZero((String) hmFilterCondition.get("COMP_DIV_ID"));
      String strCategory = GmCommonClass.parseNull((String) hmFilterCondition.get("CUSTCATEGORY"));
      String strCategoryId = GmCommonClass.parseZero((String) hmFilterCondition.get("CATEGORYID"));
      strDivisionId = strDivisionId.equals("0") ? "" : strDivisionId;
      String strDateFormat = getCompDateFmt();
      log.debug(" Value of Invoice Type is " + strInvoiceType + " AccID is " + strAccId);
      DynaBean Dbean = null;

      StringBuffer sbQuery = new StringBuffer();


      // Invoice Header Details
      /*
       * sbQuery.setLength(0); sbQuery.append(" SELECT C503_INVOICE_ID  INV_ID  " );
       * sbQuery.append(" ,C704_ACCOUNT_ID ACCT_ID  " );
       * sbQuery.append(" ,GET_ACCOUNT_NAME(C704_ACCOUNT_ID) ACCT_NAME  " );
       * sbQuery.append(" ,C503_INVOICE_DATE INV_DATE  " );
       * sbQuery.append(" ,C503_CUSTOMER_PO CUSTOMER_PO ");
       * sbQuery.append(" ,GET_CUSTOMER_PO_AMOUNT(C503_CUSTOMER_PO, C704_ACCOUNT_ID)INV_AMT  " );
       * sbQuery.append(" ,GET_INVOICE_PAID_AMOUNT(C503_INVOICE_ID) PAYMENT_AMT  " );
       * sbQuery.append(" ,C503_CREATED_DATE CREATED_DATE  " );
       * sbQuery.append(" ,GET_CODE_NAME(C901_INVOICE_TYPE) INV_TYP  " );
       * sbQuery.append(" FROM T503_INVOICE  " );
       */
      // if invoice source is Dealer (26240213) fetch by dealer else fetch by customers
      if (strInvSource.equals("26240213")) {

        sbQuery.append(" SELECT V503_DEALER_ID ACCT_ID ");
        sbQuery.append(" , DECODE('" + getComplangid()
            + "','103097',NVL(V503_DEALER_NM_EN,V503_DEALER_NM ),V503_DEALER_NM) NAME");
        sbQuery
            .append(" , GET_CODE_NAME(V503_DEALER_COLLECTOR_ID) COLLECTOR_NAME, ' ' CREDITTYPE_NAME ");

      } else {

        sbQuery.append(" SELECT NVL(V503_DISTRIBUTOR_ID, V503_ACCOUNT_ID) ACCT_ID ");
        sbQuery.append(" , v503_rep_name REPNAME ");
        sbQuery.append(" , NVL(V503_DISTRIBUTOR_NAME,DECODE('" + getComplangid()
            + "','103097',NVL(V503_ACCOUNT_NM_EN,V503_ACCOUNT_NM),V503_ACCOUNT_NM)) NAME"); // 103097:English
        sbQuery.append(" , GET_CODE_NAME(T704a.COLLECTORID ) COLLECTOR_NAME ");
        sbQuery
            .append(" ,  DECODE (T704a.CREDITTYPE,NULL,'',GET_CODE_NAME(T704a.CREDITTYPE )) CREDITTYPE_NAME  ");

      }
      if(strCategory.equals("Y")){
    	  sbQuery.append(" ,get_code_name(GET_ACCOUNT_ATTRB_VALUE (NVL(V503_DISTRIBUTOR_ID, V503_ACCOUNT_ID),'5504')) CUSTCATEGORY "); 
      }
      sbQuery.append(" ,V503_INVOICE_ID INV_ID, V503_CUSTOMER_PO CUSTOMER_PO ");
      sbQuery.append(" ,V503_INVOICE_DATE INV_DATE,GET_USER_NAME(V503_CREATED_BY) CREATED_BY "); // 103097:English
      sbQuery
          .append(" ,NVL(V503_INV_AMT,0) INV_AMT, NVL(V503_INV_PAID,0) PAYMENT_AMT, GET_CODE_NAME(V503_INVOICE_TYPE) INV_TYP,V503_CREATED_DATE CREATED_DATE, v503_acc_currency_nm CURRENCY  ");

      sbQuery
          .append(" , NVL(V503_ORDER_AMT,0) ORDER_AMT, NVL(V503_SHIP_COST,0) SHIP_AMT, NVL(V503_TAX_AMT,0) TAX_AMT ");
      sbQuery.append(", v503_division_name DIVISION_NAME ");
      sbQuery
          .append(" ,V503_TERMS payment_terms,v503_acc_currency_nm ARCURRENCYSYMB  FROM v503_invoice_reports v503 ");

      if (!strInvSource.equals("26240213")) {
        sbQuery.append(" , ( SELECT t704A.C704_ACCOUNT_ID ");
        sbQuery
            .append("  , MAX (DECODE (C901_ATTRIBUTE_TYPE, 103050,C704A_ATTRIBUTE_VALUE))  COLLECTORID ");
        sbQuery
            .append("  , MAX (DECODE (C901_ATTRIBUTE_TYPE, '101184', C704A_ATTRIBUTE_VALUE)) CREDITTYPE ");
        sbQuery.append("            FROM t704a_account_attribute t704a ");
        sbQuery.append(" WHERE c704a_void_fl  IS NULL "); // 103050 - Collector Attribute
        // The below rule entry condition is added as part of PMT-8430 to resolve time out issue
        sbQuery
            .append(" AND C901_ATTRIBUTE_TYPE IN (SELECT t906.c906_rule_value FROM t906_rules t906 ");
        sbQuery
            .append(" WHERE t906.c906_rule_grp_id = 'ACCNT_ATTR' AND c906_rule_id = 'INCLUDE_ATTR') ");
        sbQuery.append("   GROUP BY C704_ACCOUNT_ID  ");
        sbQuery.append(" )t704a ");
      }



      // sbQuery.append(" WHERE V503_INVOICE_ID IS NOT NULL ");

      // below condition is used to fetch the data based on the date type (invoice date or created date)
      if (!strFrmDate.equals("") && !strToDate.equals("")) {
          if(strDateType.equals("106460")){                         //106460--Created Date
              sbQuery.append(" WHERE TRUNC(V503_CREATED_DATE) >= TO_DATE('");
              sbQuery.append(strFrmDate);
              sbQuery.append("','" + strDateFormat + "' )");
              sbQuery.append(" AND TRUNC(V503_CREATED_DATE)<= TO_DATE('");
              sbQuery.append(strToDate);
              sbQuery.append("','" + strDateFormat + "' )");
          }else if(strDateType.equals("106461")){                    //106461-- Invoice Date
               sbQuery.append(" WHERE TRUNC(V503_INVOICE_DATE) >= TO_DATE('");
              sbQuery.append(strFrmDate);
              sbQuery.append("','" + strDateFormat + "' )");
              sbQuery.append(" AND TRUNC(V503_INVOICE_DATE)<= TO_DATE('");
              sbQuery.append(strToDate);
              sbQuery.append("','" + strDateFormat + "' )");
  
          }

      }

      /*
       * else //Based on the report type selection should fetch values { return null; }
       */
      if (strRepType.equals("1")) {
        sbQuery.append(" WHERE V503_STATUS_FL < = "); // OPEN
        sbQuery.append(strRepType);
      }

      if (strRepType.equals("2")) {
        sbQuery.append(" AND V503_STATUS_FL = "); // CLOSED
        sbQuery.append(strRepType);
      }

      if (!strInvoiceType.equals("0")) {
        sbQuery.append(" AND V503_INVOICE_TYPE = '");
        sbQuery.append(strInvoiceType);
        sbQuery.append("'");
      } else {
        // 26240439 - invoice statement , to avoid displaying invoice statement types
        sbQuery
            .append(" AND NVL (V503_INVOICE_TYPE, -9999) NOT IN (SELECT t906.c906_rule_value FROM t906_rules t906"
                + "  WHERE t906.c906_rule_grp_id = 'INVOICETYPE' AND C906_RULE_ID = 'EXCLUDE' AND C906_VOID_FL IS NULL) ");
      }

      // if show parent account is checked ,all rep accounts related information will going to show.
      if ((!strAccId.equals("")) && (!strAccId.equals("0")) && (!strParentFl.equals(""))
          && (!strInvSource.equals("26240213"))) {

        sbQuery
            .append(" AND v503.V503_PARACCT_ID  In (SELECT c101_party_id from t704_account where c704_account_id ="
                + strAccId + ") ");
      }

      sbQuery.append("AND v503.v503_company_id = " + getCompId() + "");

      if (!strDivisionId.equals("")) {
        sbQuery.append("AND v503_division_id =" + strDivisionId + " ");
      }

      // If Type is 50255 =" company customers" we are going filter based on currency .
      if (strInvSource.equals("50255"))
        sbQuery.append(" AND v503.v503_acc_currency_id = " + strAccCurrId);

      // if show parent account box is not checked only that particular account information will
      // going to show.
      if ((!strAccId.equals("")) && (!strAccId.equals("0")) && (strParentFl.equals(""))) {

        if (strInvSource.equals("50255") || strInvSource.equals("50257")
            || strInvSource.equals("50253")) {
          /*
           * The Changes made for PMT-834.Since Account Dropdown is changed to list accounts for ICS
           * the data fetched should be based on Accounts.
           */
          sbQuery.append(" AND V503_ACCOUNT_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");

        } else if (strInvSource.equals("50256")) {
          sbQuery.append(" AND V503_DISTRIBUTOR_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        } else if (strInvSource.equals("26240213")) {
          sbQuery.append(" AND V503_DEALER_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        }
      }
      if (!strInvSource.equals("0")) {
        sbQuery.append(" AND V503_INVOICE_SOURCE = ");
        sbQuery.append(strInvSource);
      }
      if(!strCategoryId.equals("0") && strCategory.equals("Y")){
    	  sbQuery.append(" AND GET_ACCOUNT_ATTRB_VALUE (NVL(V503_DISTRIBUTOR_ID, V503_ACCOUNT_ID),'5504') = '" +strCategoryId+"'" );	  
      }
     
      if (strInvSource.equals("26240213")) {

        if (!strCollectorId.equals("0")) {
          sbQuery.append(" AND V503_DEALER_COLLECTOR_ID ='" + strCollectorId + "'");
        }

      } else {

        if (!strCreditType.equals("") && !strCreditType.equals("0")) {
          sbQuery.append(" AND T704a.CREDITTYPE = " + strCreditType + "");
        }

        if (!strCollectorId.equals("0")) {
          sbQuery.append(" AND T704a.COLLECTORID ='" + strCollectorId + "'");
        }

        sbQuery.append(" And  V503_ACCOUNT_ID = T704a.C704_Account_Id (+) ");
      }

      if (!strInvoiceIDs.equals("")) {
        sbQuery.append(" AND V503_INVOICE_ID IN ('" + strInvoiceIDs + "')");
        sbQuery.append(" ORDER BY V503_INVOICE_ID ");
      } else {
        sbQuery.append(" ORDER BY V503_INVOICE_DATE ");
      }

      log.debug("Query for INvoice List " + sbQuery.toString());
      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
      // alInvoiceList = dbCon.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmARReportBean:reportInvoiceList", "Exception is:" + e);
    }

    return resultSet;
    // return alInvoiceList;
  } // End of reportInvoiceList


  /**
   * loadInvoiceDetails - This method used to fetch invoice list for the selected filter condition
   * Account
   * 
   * @param HashMap hmFilterCondition Holds filter information
   * @return HashMap
   * @exception AppError
   **/
  public RowSetDynaClass reportOrderList(HashMap hmFilterCondition) throws AppError {
    ArrayList alInvoiceList = new ArrayList();
    HashMap hmResult = new HashMap();
    RowSetDynaClass resultSet = null;
    String strFrmDate = "";
    String strToDate = "";
    String strOrderType = "";
    String strAccId = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    strFrmDate = (String) hmFilterCondition.get("FromDate");
    strToDate = (String) hmFilterCondition.get("ToDate");
    strOrderType = (String) hmFilterCondition.get("OrderType");
    strAccId = (String) hmFilterCondition.get("ACCID");
    String strSalesFilterCond =
        GmCommonClass.parseNull((String) hmFilterCondition.get("SALES_CON"));
    String strCollectorId = GmCommonClass.parseZero((String) hmFilterCondition.get("COLLECTORID"));
    DynaBean Dbean = null;
    String strCreditType = GmCommonClass.parseZero((String) hmFilterCondition.get("CREDITTYPE"));
    String strDateFormat = getCompDateFmt();
    StringBuffer sbQuery = new StringBuffer();
    String strParentFl = GmCommonClass.parseNull((String) hmFilterCondition.get("CHK_ACTIVEFL"));
    String strAccCurrId = GmCommonClass.parseZero((String) hmFilterCondition.get("ACC_CURR_ID"));
    String strRecevieMode = GmCommonClass.parseZero((String) hmFilterCondition.get("RECEVIE_MODE"));
    String strOrdDateType = GmCommonClass.parseZero((String) hmFilterCondition.get("ORDDATETYPE"));
    String strDivisionId = GmCommonClass.parseZero((String) hmFilterCondition.get("COMP_DIV_ID"));
    String strCategory = GmCommonClass.parseNull((String) hmFilterCondition.get("CUSTCATEGORY"));
    String strCategoryId = GmCommonClass.parseZero((String) hmFilterCondition.get("CATEGORYID"));
    
    String strIncludeAddnlClmnFl = GmCommonClass.parseNull((String) hmFilterCondition.get("CHK_SHOW_ADDNL_CLMN_FL"));

    
    strDivisionId = strDivisionId.equals("0") ? "" : strDivisionId;
    // Invoice Header Details
    sbQuery.setLength(0);
    sbQuery.append(" SELECT T501.C501_ORDER_DATE ORDER_DATE  ,T501.C501_DO_DOC_UPLOAD_FL DO_FLAG  , T501.C501_SURGERY_DATE SURG_DATE ");
    sbQuery.append(" , T501.C501_ORDER_ID ORDER_ID ");
    sbQuery.append(" , T501.C501_PO_AMOUNT PO_AMOUNT ");
    sbQuery.append(" ,T501.C704_ACCOUNT_ID ACCT_ID  ");
    sbQuery
        .append(" ,DECODE('"
            + getComplangid()
            + "','103097',NVL(T704.C704_ACCOUNT_NM_EN,T704.C704_ACCOUNT_NM),T704.C704_ACCOUNT_NM) ACCT_NAME "); // 103097:English
    sbQuery.append(" ,NVL(T501.C501_TOTAL_COST,0) ORDER_VALUE ");
    sbQuery.append(" ,NVL(T501.C501_SHIP_COST,0) SHIP_COST  ");
    sbQuery.append(" ,NVL(T501.C501_TOTAL_COST,0) + NVL(T501.C501_SHIP_COST,0) TOT_COST  ");
     
    if(strCategory.equals("Y")){
  	  sbQuery.append(" ,get_code_name(GET_ACCOUNT_ATTRB_VALUE (T501.C704_ACCOUNT_ID,'5504')) CUSTCATEGORY "); 
    }

    // if requested to fetch pending PO report
    if (strOrderType.equals("103184")) // 103184 - Pending PO
    {
      sbQuery.append(" ,( TRUNC(CURRENT_DATE) - T501.C501_ORDER_DATE) PENDING_DT  ");
    }

    if (strOrderType.equals("103186")) // 103186 - RA Not Returned but Issued Credit
    {
      sbQuery.append(" ,NVL(Get_Order_Part_COGS_Value(T501.C501_ORDER_ID),0) PART_COGS_VAL  ");
      sbQuery.append(" ,T501.C501_TRACKING_NUMBER TRACKINGNUM  ");
    }

    sbQuery.append(" ,T501.C503_INVOICE_ID INV_ID  ");
    sbQuery.append(" ,T501.C501_CUSTOMER_PO CUSTOMER_PO ");
    sbQuery.append(" ,T501.C501_CUSTOMER_PO_DATE CUST_PO_DATE ");
    sbQuery
        .append(" ,NVL(T503.C503_INVOICE_DATE, GET_CREDIT_MEMO_INVOICE_ID(C501_PARENT_ORDER_ID, 'D')) INV_DATE  ");
    sbQuery.append(" , GET_CODE_NAME(T704a.COLLECTORID ) COLLECTOR_NAME ");
    sbQuery
        .append(" , DECODE(T704a.CREDITTYPE,NULL,'',GET_CODE_NAME(T704a.CREDITTYPE )) CREDITTYPE_NAME  ");
    sbQuery
        .append(" ,get_code_name (t704.c704_payment_terms) PAYMENT_TERMS,get_code_name( t704.c901_currency) ARCURRENCYSYMB ");
    sbQuery.append(", get_code_name(t501.c501_receive_mode) MODE_OF_ORDER ");
    sbQuery.append(", T501.C501_COPAY COPAY,T501.C501_CAP_AMOUNT CAP_AMOUNT ");
    sbQuery.append(", t1910.C1910_DIVISION_NAME DIVISION_NAME ");
    sbQuery.append(", NVL(T503.C503_INV_TAX_AMT,0) TAX_AMT ");
    sbQuery.append(", NVL(T503.C503_INV_AMT,0) INV_AMT ");
    
    if(strIncludeAddnlClmnFl.equalsIgnoreCase("true")){
    	
    	sbQuery.append(", T501.C501_PARENT_ORDER_ID PARENT_ORDER_ID ");
    	sbQuery.append(", GET_USER_NAME(C501_AD_ID) AD_NAME ");
    	sbQuery.append(", GET_USER_NAME(C501_VP_ID) VP_NAME ");
    	// When the Contract column is having 0, the get code name function will return Place Holder, so we are avoiding 0.
		sbQuery.append(" ,DECODE(NVL(T704d.c901_contract,0),0,'',GET_CODE_NAME(T704d.c901_contract )) CONTRACT_TYPE_NAME  ");
		sbQuery.append(", DECODE('"+ getComplangid()+ "','103097',NVL(T703.C703_SALES_REP_NAME_EN,T703.C703_SALES_REP_NAME),T703.C703_SALES_REP_NAME) REP_NAME ");
		sbQuery.append(" , ROUND(( TRUNC(CURRENT_DATE) - T501.C501_ORDER_DATE),0) AGING ");
		sbQuery.append(" , get_code_name(t5001.C901_CUST_PO_STATUS) po_status ");
    }
    
    sbQuery.append(" FROM   T501_ORDER T501, t1910_division t1910  ");
//    PCA-5523 - Add PO Status Filter and column on A/R Orders List Report
    sbQuery.append(" ,T503_INVOICE T503,  T5001_ORDER_PO_DTLS t5001  ");
    if(strIncludeAddnlClmnFl.equalsIgnoreCase("true")){
    	sbQuery.append("  ,T703_SALES_REP T703 ,T704D_ACCOUNT_AFFLN t704d   ");
    }
    sbQuery.append(" , (  SELECT t704A.C704_ACCOUNT_ID ");
    sbQuery
        .append(" , MAX (DECODE (C901_ATTRIBUTE_TYPE, 103050,C704A_ATTRIBUTE_VALUE))  COLLECTORID ");
    sbQuery
        .append(" , MAX (DECODE (C901_ATTRIBUTE_TYPE, '101184', C704A_ATTRIBUTE_VALUE)) CREDITTYPE ");
    
    sbQuery.append(" FROM t704a_account_attribute t704a ");
    sbQuery.append(" WHERE c704a_void_fl  IS NULL "); // 103050 - Collector Attribute
    // The below rule entry condition is added as part of PMT-8430 to resolve time out issue
    sbQuery
        .append(" AND C901_ATTRIBUTE_TYPE IN (SELECT t906.c906_rule_value FROM t906_rules t906 ");
    sbQuery
        .append(" WHERE t906.c906_rule_grp_id = 'ACCNT_ATTR' AND c906_rule_id = 'INCLUDE_ATTR') ");
    sbQuery.append("GROUP BY C704_ACCOUNT_ID  ");
    sbQuery.append(" )t704a,  t704_account t704 ");
    sbQuery.append(" WHERE T501.C501_VOID_FL IS NULL  ");
    sbQuery.append("  and t501.c501_order_id = t5001.C501_ORDER_ID(+) AND t5001.C5001_VOID_FL(+) IS NULL ");
    
    if (!strRecevieMode.equals("0")) {
      sbQuery.append(" AND t501.c501_receive_mode=" + strRecevieMode);
    }
    if(strIncludeAddnlClmnFl.equalsIgnoreCase("true")){
    	sbQuery.append("  and T501.C703_SALES_REP_ID = T703.C703_SALES_REP_ID(+) ");
    	sbQuery.append("  and T501.C704_ACCOUNT_ID = t704d.C704_ACCOUNT_ID(+) ");
    	sbQuery.append("  and t704d.C704D_VOID_FL(+) is null ");
    }
    sbQuery.append("  AND T501.C704_ACCOUNT_ID = T704.C704_ACCOUNT_ID ");
    sbQuery.append("  AND t501.c1910_division_id     = t1910.c1910_division_id(+)  ");
    sbQuery.append("  AND t1910.c1910_void_fl (+) IS NULL  ");

    if (!strDivisionId.equals("")) {
      sbQuery.append("AND t501.c1910_division_id     = " + strDivisionId + " ");
    }


    // 5 Maps to pending po
    if (!strFrmDate.equals("") && !strToDate.equals("")) {
     if(strOrdDateType.equals("106480")){                        //based on Order date
      sbQuery.append(" AND T501.C501_ORDER_DATE >= TO_DATE('");
      sbQuery.append(strFrmDate);
      sbQuery.append("','" + strDateFormat + "' )");
      sbQuery.append(" AND T501.C501_ORDER_DATE <= TO_DATE('");
      sbQuery.append(strToDate);
      sbQuery.append("','" + strDateFormat + "' )");
      }else if(strOrdDateType.equals("106481")){                   //based on Surgery date
	  sbQuery.append(" AND T501.C501_SURGERY_DATE >= TO_DATE('");
      sbQuery.append(strFrmDate);
      sbQuery.append("','" + strDateFormat + "' )");
      sbQuery.append(" AND T501.C501_SURGERY_DATE <= TO_DATE('");
      sbQuery.append(strToDate);
      sbQuery.append("','" + strDateFormat + "' )");
      }
      else if(strOrdDateType.equals("26240690")){                   //based on PO Entered date
    	  sbQuery.append(" AND TRUNC(T501.C501_CUSTOMER_PO_DATE) >= TO_DATE('");
          sbQuery.append(strFrmDate);
          sbQuery.append("','" + strDateFormat + "' )");
          sbQuery.append(" AND TRUNC(T501.C501_CUSTOMER_PO_DATE) <= TO_DATE('");
          sbQuery.append(strToDate);
          sbQuery.append("','" + strDateFormat + "' )");
          }
      }
    // if show parent account box is not checked only that particular account information will going
    // to show.
    if ((!strAccId.equals("0")) && (!strAccId.equals("")) && strParentFl.equals("")) {
      sbQuery.append(" AND T501.C704_ACCOUNT_ID = '");
      sbQuery.append(strAccId);
      sbQuery.append("'");
    }
    if((!strCategoryId.equals("0")) && strCategory.equals("Y")) {
  	  sbQuery.append(" AND GET_ACCOUNT_ATTRB_VALUE (T501.C704_ACCOUNT_ID,'5504') = '" +strCategoryId+"'" );	  
    }
    // To add the order filter
    if (strOrderType.equals("103188")) // 103188 - Sales Order Only
    {
      sbQuery.append(" AND  (T501.C901_ORDER_TYPE <> 2528 OR T501.C901_ORDER_TYPE IS NULL) ");
      sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
    } else if (strOrderType.equals("103181")) // 103181 - Credit Memo
    {
      sbQuery.append(" AND  T501.C901_ORDER_TYPE = 2528 ");
      sbQuery.append(" AND  T501.C501_TOTAL_COST < 0 ");
    } else if (strOrderType.equals("103182")) // 103182 - Duplicate Order
    {
      sbQuery.append(" AND  T501.C901_ORDER_TYPE IS NOT NULL");
      sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
      sbQuery.append(" AND  T501.C501_TOTAL_COST > 0 ");
    } else if (strOrderType.equals("103184")) // 103184 - Pending PO
    {
      // sbQuery.append(" AND  T501.C901_ORDER_TYPE IS NULL");
      sbQuery.append(" AND T501.C503_INVOICE_ID IS NULL ");
      sbQuery.append(" AND  T501.C501_CUSTOMER_PO IS NULL");
    }

    else if (strOrderType.equals("103185")) // 103185 - Pending Invoices Generation
    {
      // sbQuery.append(" AND  T501.C901_ORDER_TYPE IS NULL ");
      sbQuery.append(" AND  T501.C501_CUSTOMER_PO IS NOT NULL ");
      sbQuery.append(" AND  T501.C503_INVOICE_ID IS NULL ");
      // sbQuery.append(" AND  T501.C501_STATUS_FL = '3'");
    }
    else if (strOrderType.equals("26241106")) // PCA-5523 -- 26241106 - Discrepant PO
    {
      sbQuery.append(" AND  t5001.C901_CUST_PO_STATUS =109544  ");
      sbQuery.append(" AND  T501.C503_INVOICE_ID IS NULL ");
    }
    else if (strOrderType.equals("103187")) // 103187 - Sales Adjustment
    {
      sbQuery.append(" AND  T501.C901_ORDER_TYPE <> 2528 ");
      sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
      sbQuery
          .append(" AND  (T501.C501_TOTAL_COST < 0 OR t503.c901_invoice_type IN (50202,50203)) "); // Issue
                                                                                                   // Credit:50202,Issue
                                                                                                   // Debit:50203
    }
    /*
     * else if (strOrderType.equals("8")) { //
     * sbQuery.append(" AND  T501.C901_ORDER_TYPE IS NULL ");
     * sbQuery.append(" AND  T501.C501_STATUS_FL < 3 ");
     * sbQuery.append(" AND T501.C501_SHIPPING_DATE IS NULL ");
     * sbQuery.append(" AND  T501.C503_INVOICE_ID IS NOT NULL "); }
     */
    else if (strOrderType.equals("103186")) // 103186 - RA Not Returned but Issued Credit
    {
      sbQuery.append(" AND  T501.C901_ORDER_TYPE <> 2528 ");
      sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
      sbQuery.append(" AND  T501.C506_RMA_ID IN ");
      sbQuery.append(" ( SELECT  T506.C506_RMA_ID FROM T506_RETURNS T506 ");
      sbQuery.append(" WHERE T506.C506_TYPE = 3300 ");
      sbQuery.append(" AND T506.C506_VOID_FL IS NULL ");
      sbQuery.append(" AND  T506.C506_STATUS_FL < 2 ) ");
    }

    else if (strOrderType.equals("103183")) // 103183 - Held Orders
    {
      sbQuery.append(" AND  T501.C501_HOLD_FL IS NOT NULL ");
    } else if (strOrderType.equals("103189")) // When select Debit Memo , it will fetch using
                                              // following filter. //103189 - Debit Memo
    {
      sbQuery.append(" AND  T501.C901_ORDER_TYPE <> 2528 ");
      sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");
      sbQuery.append(" AND t503.c901_invoice_type IN (50203) "); // Issue Debit:50203
    }
    /*
     * sbQuery.append(" AND T501.C501_CUSTOMER_PO   = T503.C503_CUSTOMER_PO (+)  ");
     * sbQuery.append(" AND T501.C704_ACCOUNT_ID    = T503.C704_ACCOUNT_ID (+) ");
     */
    sbQuery.append(" AND T501.C503_INVOICE_ID = T503.C503_INVOICE_ID (+) ");
    sbQuery.append(" AND T501.C501_DELETE_FL IS NULL  ");
    sbQuery.append(" AND T503.C503_VOID_FL IS NULL ");
    if (!strCreditType.equals("") && !strCreditType.equals("0")) {
      sbQuery.append(" AND T704a.CREDITTYPE = " + strCreditType + "");
    }
    sbQuery.append(" AND (T501.c901_ext_country_id is NULL ");
    sbQuery.append(" OR T501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes)) ");// To
                                                                                                    // include
                                                                                                    // ext
                                                                                                    // country
                                                                                                    // id
                                                                                                    // for
                                                                                                    // GH
    sbQuery
        .append(" AND (T503.c901_ext_country_id is NULL OR T503.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))  ");
    sbQuery.append(" AND nvl(T501.C901_order_type,-9999) <> 2535 ");
    sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
    sbQuery.append(" SELECT t906.c906_rule_value ");
    sbQuery.append(" FROM t906_rules t906 ");
    sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
    sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ");

    // To exclude orders from the Order List Report
    sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
    sbQuery.append(" SELECT t906.c906_rule_value ");
    sbQuery.append(" FROM t906_rules t906 ");
    sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDERTYPE' ");
    sbQuery.append(" AND c906_rule_id = 'EXCLUDE' )");

    // if show parent account is checked ,all rep accounts related information will going to show.
    if ((!strAccId.equals("")) && (!strAccId.equals("0")) && (!strParentFl.equals(""))) {

      sbQuery
          .append(" AND  T704. C101_party_id = (select c101_party_id from t704_account where C704_ACCOUNT_ID = "
              + strAccId + ") ");
    }

    sbQuery.append(" And T501.C704_Account_Id  = T704a.C704_Account_Id (+) ");
    sbQuery.append(" AND T501.c1900_company_id = '" + getCompId() + "' ");
    // Currency dropdown filter in AR
    if (!strAccCurrId.equals("")) {
      sbQuery.append(" AND t704.c901_currency = " + strAccCurrId);
    }
    if (!strCollectorId.equals("0")) {
      sbQuery.append(" AND T704a.COLLECTORID ='" + strCollectorId + "'");
    }
    if (!strSalesFilterCond.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strSalesFilterCond);
    }
    sbQuery.append(" ORDER BY T501.C501_ORDER_DATE, T501.C501_ORDER_ID  ");

    log.debug("Query for Order List " + sbQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    // alInvoiceList = dbCon.queryMultipleRecords(sbQuery.toString());

    return resultSet;
    // return alInvoiceList;
  } // End of reportInvoiceList

  /**
   * loadInvoiceDetails - This method used to fetch Payment list for the selected filter condition
   * 
   * @param HashMap hmFilterCondition Holds filter information
   * @return RowSetDynaClass
   * @exception AppError
   **/
  public RowSetDynaClass reportPaymentList(HashMap hmFilterCondition) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alInvoiceList = new ArrayList();
    HashMap hmResult = new HashMap();
    RowSetDynaClass resultSet = null;
    String strFrmDate = "";
    String strToDate = "";
    String strPayType = "";
    String strAccId = "";
    String strPayMode = "";

    strFrmDate = (String) hmFilterCondition.get("FromDate");
    strToDate = (String) hmFilterCondition.get("ToDate");
    strPayType = (String) hmFilterCondition.get("PayType");
    strPayMode = (String) hmFilterCondition.get("PayMode");
    strAccId = GmCommonClass.parseNull((String) hmFilterCondition.get("ACCID"));
    String strInvSource = GmCommonClass.parseNull((String) hmFilterCondition.get("INVSOURCE"));
    String strCollectorId = GmCommonClass.parseZero((String) hmFilterCondition.get("COLLECTORID"));
    String strCreditType = GmCommonClass.parseZero((String) hmFilterCondition.get("CREDITTYPE"));
    String strDivisionId = GmCommonClass.parseZero((String) hmFilterCondition.get("COMP_DIV_ID"));
    strDivisionId = strDivisionId.equals("0") ? "" : strDivisionId;
    String strDateFormat = getCompDateFmt();
    DynaBean Dbean = null;
    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strParentFL = GmCommonClass.parseNull((String) hmFilterCondition.get("PARENTFL"));
    String strAccCurrId = GmCommonClass.parseZero((String) hmFilterCondition.get("ACC_CURR_ID"));
    StringBuffer sbQuery = new StringBuffer();


    // Invoice Header Details
    sbQuery.setLength(0);
    sbQuery.append(" SELECT T508.C508_RECEIVED_DATE R_DATE");
    sbQuery.append(" ,GET_CODE_NAME(T508.C508_RECEIVED_MODE) R_MODE");
    sbQuery.append(" ,NVL(T508.C508_PAYMENT_AMT,0) PAYMENT_AMT ");
    sbQuery.append(" ,T503.C503_INVOICE_ID INV_ID");
    sbQuery.append(" ,C503_CUSTOMER_PO CUSTOMER_PO ");
    sbQuery.append(" ,T508.C508_DETAILS R_DTLS");
    sbQuery.append(" ,T503.C503_INVOICE_DATE  INVOICE_DATE ");

    // if invoice source is Dealer (26240213) fetch by dealer else fetch by customers
    if (strInvSource.equals("26240213")) {

      sbQuery
          .append(" , DECODE ("
              + getComplangid()
              + ",'103097',NVL(t101.C101_PARTY_NM_EN,DECODE(t101.c101_party_nm, NULL, t101.c101_first_nm || ' ' || t101.c101_last_nm || ' ' || t101.c101_middle_initial, t101.c101_party_nm))"
              + ",DECODE(t101.c101_party_nm, NULL, t101.c101_first_nm || ' ' || t101.c101_last_nm || ' ' || t101.c101_middle_initial, t101.c101_party_nm)) ACCT_NAME ");
      sbQuery.append(" ,t101.c101_party_id ACCT_ID ");
      sbQuery.append(" ,get_acct_curr_symb_by_dealer(t101.c101_party_id) CURRENCY ");
      sbQuery
          .append(" , GET_CODE_NAME(T101P.C901_COLLECTOR_ID) COLLECTOR_NAME, ' ' CREDITTYPE_NAME,t901.c901_code_nm payment_terms ");
      sbQuery.append(" , t1910.C1910_DIVISION_NAME DIVISION_NAME ");
      sbQuery
          .append(" FROM T508_RECEIVABLES T508, T503_INVOICE T503, t101_party t101,t101_party_invoice_details t101p,t901_code_lookup t901,t1910_division t1910 ");


    } else {
      sbQuery
          .append(" ,DECODE("
              + getComplangid()
              + ",'103097',NVL(T704.C704_ACCOUNT_NM_EN,T704.C704_ACCOUNT_NM),T704.C704_ACCOUNT_NM) ACCT_NAME ");
      sbQuery.append(" ,T503.C704_ACCOUNT_ID ACCT_ID ");
      sbQuery
          .append(",  DECODE (t503.C901_INVOICE_SOURCE, 50256, get_rule_value (50218, get_distributor_inter_party_id (t503.C701_DISTRIBUTOR_ID)), GET_CODE_NAME(t704.c901_currency)) CURRENCY ");
      sbQuery
          .append(" , GET_CODE_NAME(T704a.COLLECTORID ) COLLECTOR_NAME, DECODE(T704a.CREDITTYPE,NULL,'',GET_CODE_NAME(T704a.CREDITTYPE )) CREDITTYPE_NAME,get_code_name (t704.c704_payment_terms) payment_terms ");
      sbQuery.append(" , t1910.C1910_DIVISION_NAME DIVISION_NAME ");
      sbQuery.append(" FROM T508_RECEIVABLES T508, T503_INVOICE T503 ");
      sbQuery.append(" , (  SELECT t704A.C704_ACCOUNT_ID ");
      sbQuery
          .append("   , MAX (DECODE (C901_ATTRIBUTE_TYPE, 103050,C704A_ATTRIBUTE_VALUE))  COLLECTORID ");
      sbQuery
          .append("  , MAX (DECODE (C901_ATTRIBUTE_TYPE, '101184', C704A_ATTRIBUTE_VALUE)) CREDITTYPE ");
      sbQuery.append(" FROM t704a_account_attribute t704a ");
      sbQuery.append(" WHERE c704a_void_fl  IS NULL "); // 103050 - Collector Attribute
      // The below rule entry condition is added as part of PMT-8430 to resolve time out issue
      sbQuery
          .append(" AND C901_ATTRIBUTE_TYPE IN (SELECT t906.c906_rule_value FROM t906_rules t906 ");
      sbQuery
          .append(" WHERE t906.c906_rule_grp_id = 'ACCNT_ATTR' AND c906_rule_id = 'INCLUDE_ATTR') ");
      sbQuery.append("  GROUP BY C704_ACCOUNT_ID  ");
      sbQuery.append(" )t704a,  t704_account t704 ,t1910_division t1910 ");
    }

    sbQuery.append(" WHERE t503.c1910_division_id     = t1910.c1910_division_id(+) ");
    sbQuery.append(" AND t1910.c1910_void_fl (+) IS NULL ");

    if (!strDivisionId.equals("")) {
      sbQuery.append(" AND t503.c1910_division_id     =" + strDivisionId + " ");
    }


    if (!strFrmDate.equals("") && !strToDate.equals("")) {
      sbQuery.append(" AND T508.C508_RECEIVED_DATE  >= TO_DATE('");
      sbQuery.append(strFrmDate);
      sbQuery.append("','" + strDateFormat + "' ) ");
      sbQuery.append(" AND T508.C508_RECEIVED_DATE  <= TO_DATE('");
      sbQuery.append(strToDate);
      sbQuery.append("','" + strDateFormat + "' )");
    }

    else {
      return null;
    }

    // if (!strAccountId.equals("0") && !strAccountId.equals(""))
    // {
    // sbQuery.append(" AND C704_ACCOUNT_ID = '");
    // sbQuery.append(strAccountId);
    // sbQuery.append("'" );
    // }
    /*
     * If INVSOURCE = "50255" || "50257" sbQuery.append(" WHERE T503.C704_ACCOUNT_ID = '");
     * sbQuery.append(strAccId); sbQuery.append("' "); else
     * sbQuery.append(" WHERE T503.c701_distributor_id = '"); sbQuery.append(strAccId);
     * sbQuery.append("' ");
     * 
     * 2. If (!strSource.equals("0")){ AND T503.C901_INVOICE_SOURCE = strType }
     */

    // If show parent account check box is checked ,All rep accounts that belongs to parent account
    // should needs to show.
    if ((!strAccId.equals("")) && (!strAccId.equals("0")) && (!strParentFL.equals(""))
        && (!strInvSource.equals("26240213"))) {
      sbQuery
          .append(" AND  T704. C101_party_id = (select c101_party_id from t704_account where C704_ACCOUNT_ID = '"
              + strAccId + "')");
    }


    if (strInvSource.equals("26240213")) {

      sbQuery.append(" AND T101.C1900_COMPANY_ID = ");
      sbQuery.append("'" + strCompanyId + "'");

    } else {

      sbQuery.append(" AND T704.C1900_COMPANY_ID = ");
      sbQuery.append("'" + strCompanyId + "'");

    }

    // If Type is 50255 =" company customers" we are going filter based on currency .
    if (strInvSource.equals("50255"))
      sbQuery.append(" AND t704.c901_currency = " + strAccCurrId);
    // If show parent account check box is unchecked ,Particular rep account Details should needs to
    // show.
    if ((!strAccId.equals("")) && (!strAccId.equals("0")) && (strParentFL.equals(""))) {

      if (strInvSource.equals("50255") || strInvSource.equals("50257")
          || strInvSource.equals("50253")) {
        /*
         * The Changes made for PMT-834.Since Account Dropdown is changed to list accounts for ICS
         * the data fetched should be based on Accounts.
         */
        sbQuery.append(" AND T503.C704_ACCOUNT_ID = '");
        sbQuery.append(strAccId);
        sbQuery.append("'");
      } else if (strInvSource.equals("50256")) {
        sbQuery.append(" AND T503.c701_distributor_id = '");
        sbQuery.append(strAccId);
        sbQuery.append("' ");
      } else if (strInvSource.equals("26240213")) {
        sbQuery.append(" AND T503.C101_DEALER_ID = '");
        sbQuery.append(strAccId);
        sbQuery.append("'");
      }
    }

    if (!strInvSource.equals("0")) {
      sbQuery.append(" AND T503.C901_INVOICE_SOURCE = ");
      sbQuery.append(strInvSource);
      sbQuery.append(" ");
    }


    /*
     * The below code is changed for TSK-2878. Whenever the report is pulled with Payment Type as
     * Payment it needs to include the record which is having payment mode of wire transfer. When
     * report loaded with Payment Type as discount ,it need not include records of payment mode as
     * wire transfer.
     */
    if (!strPayType.equals("0")) {
      if (strPayType.equals("2700")) {// 2700 - Payment type as Payment
        sbQuery.append(" AND ( T508.C901_TYPE ='");
        sbQuery.append(strPayType);
        sbQuery.append(" ' OR T508.C508_RECEIVED_MODE in(5023) ) ");// 5023 - payment mode - wire
                                                                    // transfer.
      } else if (strPayType.equals("2701")) {// 2701 - Payment type as Discount
        sbQuery.append(" AND (T508.C901_TYPE ='");
        sbQuery.append(strPayType);
        sbQuery.append(" ' AND T508.C508_RECEIVED_MODE NOT in(5023) ) ");
      } else {
        sbQuery.append(" AND  T508.C901_TYPE ='");
        sbQuery.append(strPayType);
        sbQuery.append(" ' ");
      }
    }

    if (!strPayMode.equals("0")) {
      sbQuery.append(" AND   T508.C508_RECEIVED_MODE = '");
      sbQuery.append(strPayMode);
      sbQuery.append("' ");
    }

    sbQuery.append(" AND   T508.C503_INVOICE_ID = T503.C503_INVOICE_ID ");
    sbQuery.append(" AND T503.C503_VOID_FL IS NULL ");

    if (strInvSource.equals("26240213")) {

      if (!strCollectorId.equals("0")) {
        sbQuery.append(" AND T101P.C901_COLLECTOR_ID ='" + strCollectorId + "'");
      }

      sbQuery.append(" AND T101.C101_VOID_FL IS NULL ");
      sbQuery.append(" AND T101P.C101_VOID_FL IS NULL ");
      sbQuery.append(" AND T503.C101_DEALER_ID   = T101.C101_PARTY_ID ");
      sbQuery.append(" AND T101.C101_PARTY_ID  = T101P.C101_PARTY_ID ");
      sbQuery.append(" AND t101p.c901_payment_terms = t901.c901_code_id ");

    } else {

      if (!strCreditType.equals("") && !strCreditType.equals("0")) {
        sbQuery.append(" AND T704a.CREDITTYPE = " + strCreditType + "");
      }

      sbQuery.append(" And T503.C704_Account_Id  = T704a.C704_Account_Id (+) ");
      sbQuery.append(" AND T503.C704_ACCOUNT_ID  = T704.C704_ACCOUNT_ID ");
      if (!strCollectorId.equals("0")) {
        sbQuery.append(" AND T704a.COLLECTORID ='" + strCollectorId + "'");
      }
    }

    sbQuery.append(" ORDER BY  T508.C508_RECEIVED_DATE , T508.C503_INVOICE_ID ");

    log.debug(" Value of Query to get Payment List Report in ARReportBean is " + sbQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    // alInvoiceList = dbCon.queryMultipleRecords(sbQuery.toString());

    return resultSet;
    // return alInvoiceList;
  } // End of reportInvoiceList


  /**
   * loadInvoiceDetails - This method used to fetch Payment list for the selected filter condition
   * 
   * @param HashMap hmFilterCondition Holds filter information
   * @return HashMap
   * @exception AppError
   **/
  public RowSetDynaClass reportPendingPoSummary(String strAccountCurrency) throws AppError {
    ArrayList alInvoiceList = new ArrayList();
    HashMap hmResult = new HashMap();
    RowSetDynaClass resultSet = null;

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      DynaBean Dbean = null;

      StringBuffer sbQuery = new StringBuffer();


      // Invoice Header Details
      sbQuery.setLength(0);
      sbQuery.append(" SELECT GET_ACCOUNT_NAME(ACC_ID) ACCT_NAME ");
      sbQuery.append(" ,REPID REPID, get_rep_name(REPID) REPNM ");
      sbQuery.append(" ,SUM(DECODE( SIGN( DATE_DIFF - 15),-1,TOTAL_COST ,0 )) CURRENT_VALUE ");
      sbQuery.append(" ,SUM(DECODE( SIGN(DATE_DIFF - 14 ),  1  ");
      sbQuery.append(" ,DECODE(SIGN( DATE_DIFF- 31) , -1, TOTAL_COST ,0 ) ");
      sbQuery.append(" ,0) )FIFTEEN_THIRTY ");
      sbQuery.append(" ,SUM(DECODE( SIGN( DATE_DIFF - 30) , 1,TOTAL_COST ,0 )) GREATER_THIRTY ");
      sbQuery.append(" ,SUM(TOTAL_COST) TOT_AMOUNT, ARCURRENCYSYMB ");
      sbQuery.append(" FROM (   SELECT      T501.C704_ACCOUNT_ID ACC_ID ");
      sbQuery.append(" ,T501.C501_TOTAL_COST + NVL(T501.C501_SHIP_COST,0) TOTAL_COST ");
      sbQuery.append(" ,(TRUNC(CURRENT_DATE) - T501.C501_SHIPPING_DATE) DATE_DIFF ");
      sbQuery
          .append(" ,T501.C703_SALES_REP_ID REPID,get_code_name( t704.c901_currency) ARCURRENCYSYMB ");
      sbQuery.append(" FROM T501_ORDER T501, t704_account t704  ");
      sbQuery.append(" WHERE    T501.C901_ORDER_TYPE IS NULL ");
      sbQuery.append(" AND   T501.C501_CUSTOMER_PO IS NULL ");
      sbQuery.append(" AND t501.c704_account_id = t704.c704_account_id ");
      sbQuery.append(" AND t704.c901_currency = " + strAccountCurrency);
      // Commenting the below line due to Credit Memo changes. We would not need to check the Order
      // Status for "Pending PO".
      // An order has Pending PO as long as the C501_CUSTOMER_PO is NULL
      // sbQuery.append(" AND T501.C501_STATUS_FL = '2' )  ");
      sbQuery.append(" AND T501.C501_DELETE_FL IS NULL ");
      sbQuery.append(" AND T501.C501_VOID_FL IS NULL  ");
      sbQuery.append(" AND T501.C1900_COMPANY_ID = " + getCompId());
      sbQuery
          .append(" AND (T501.c901_ext_country_id is NULL OR T501.c901_ext_country_id IN (SELECT country_id FROM v901_country_codes))) ");// To
                                                                                                                                          // include
                                                                                                                                          // ext
                                                                                                                                          // country
                                                                                                                                          // id
                                                                                                                                          // for
                                                                                                                                          // GH
      sbQuery.append(" GROUP BY ACC_ID,REPID,ARCURRENCYSYMB ");
      sbQuery.append(" ORDER  BY ACCT_NAME ");

      log.debug(sbQuery.toString());
      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
      // alInvoiceList = dbCon.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      GmLogError.log("Exception in GmARReportBean:reportInvoiceList", "Exception is:" + e);
    }

    return resultSet;
    // return alInvoiceList;
  } // End of reportInvoiceList


  public ArrayList fetchVATSummaryDetails(HashMap hmParam) throws AppError {
    ArrayList alReturn = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    Date dtFrom = (Date) hmParam.get("FROMDATE");
    Date dtTo = (Date) hmParam.get("TODATE");
    String strFormat = (String) hmParam.get("FORMAT");
    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strARCurrSymbol = GmCommonClass.parseNull((String) hmParam.get("ARCURRSYMBOL"));
    sbQuery.setLength(0);
    sbQuery
        .append("select a.v503_invoice_date INVDT, get_code_name(a.v503_invoice_source) INV_SRC,");
    sbQuery
        .append(" DECODE(a.v503_invoice_source,'26240213',a.v503_dealer_id,a.v503_account_id) ACCID, ");
    sbQuery.append(" DECODE(a.v503_invoice_source,'26240213', ");
    sbQuery.append(" DECODE('" + getComplangid()
        + "','103097',NVL(a.v503_dealer_nm_en,a.v503_dealer_nm),a.v503_dealer_nm),");
    sbQuery
        .append(" DECODE('"
            + getComplangid()
            + "','103097',NVL(a.v503_account_nm_en,a.v503_account_nm),a.v503_account_nm)) ACCNAME, a.v503_invoice_id INVID, ");
    sbQuery.append(" a.v503_customer_po CUSPO,a.v503_inv_amt INVAMT,  A.v503_tax_amt ");
    sbQuery.append(" VAT from V503_INVOICE_REPORTS a WHERE a.v503_invoice_date between ");
    sbQuery.append("to_date('");
    sbQuery.append(GmCommonClass.getStringFromDate(dtFrom, strFormat));
    sbQuery.append("','");
    sbQuery.append(strFormat);
    sbQuery.append("') and  to_date('");
    sbQuery.append(GmCommonClass.getStringFromDate(dtTo, strFormat));
    sbQuery.append("','");
    sbQuery.append(strFormat);
    sbQuery.append(" ') AND a.V503_COMPANY_ID = ");
    sbQuery.append("'" + strCompanyId + "'");
    sbQuery.append(" AND a.V503_ACC_CURRENCY_ID = '" + strARCurrSymbol + "'");
    sbQuery.append(" order by a.v503_invoice_date  ");
    log.debug(" Query to fetch Order details is " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());


    return alReturn;
  } // End of fetchVATSummaryDetails

  /**
   * getInvoiceDtls - This method will get invoice details
   * 
   * @param String Invoice Id
   * @exception AppError
   */
  public ArrayList getInvoiceDtls(HashMap hmParam) throws AppError {

    String strInvId = GmCommonClass.parseNull((String) hmParam.get("STRINVOICEID"));
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_fch_invoice", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strInvId);
    gmDBManager.execute();

    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug(" Records is " + alResult.size());
    return alResult;
  }

  /**
   * fetchInvoiceId() : Get invoice id for Updated invoice in Process Transaction screen
   * 
   * @param RefID, Source
   * @return String
   */
  public String fetchInvoiceId(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strInvoiceId = "";
    String strRefID = "";
    String strSource = "";
    String strAccCurrId = "";

    strRefID = GmCommonClass.parseNull((String) hmParam.get("STRREFID"));
    strSource = GmCommonClass.parseNull((String) hmParam.get("STRSOURCE"));
    strAccCurrId = GmCommonClass.parseNull((String) hmParam.get("STRCOMPCURRENCY"));

    gmDBManager.setPrepareString("gm_pkg_ac_order.gm_fch_invoiceid", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strRefID);
    gmDBManager.setString(2, strSource);
    gmDBManager.setString(3, strAccCurrId);
    gmDBManager.execute();
    strInvoiceId = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.close();
    return strInvoiceId;
  }

  /**
   * validateInvoice - This method will get the invoice id and part number details for given part
   * and invoice
   * 
   * @param Hashmap
   * @exception AppError
   */
  public ArrayList validateInvoice(HashMap hmParam) throws AppError {

    ArrayList alResult = new ArrayList();
    String strInvoiceId = "";
    String strPartNum = "";

    strInvoiceId = GmCommonClass.parseNull((String) hmParam.get("INVOICEID"));
    strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_validate_inv", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strInvoiceId);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    log.debug(" Records is " + alResult.size());
    return alResult;
  } // end of validateInvoice

  /**
   * validateSubPartPrice - This method will validate sub part price with order item price - france
   * 
   * @param String PO
   * @param String account
   * @return void
   * @exception AppError
   */
  public void validateSubPartPrice(String strSessPO, String strSessAccId, String strUserId)
      throws AppError {
    log.debug(" validateSubPartPrice " + strSessPO);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_pd_sub_cmp_mpng.gm_validate_subpart_price", 3);
    gmDBManager.setString(1, strSessPO);
    gmDBManager.setString(2, strSessAccId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.close();
  }



  /**
   * reportPendingInvoice - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportPendingInvoice(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    ResultSet rs = null;
    StringBuffer sbQuery = new StringBuffer();

    // Filter Condition Temp variable
    String strAccID = "";
    String strType = "";
    String strOrderID = "";
    String strInvoiceID = "";
    String dateFrom = "";
    String dateTo = "";
    Date dtFrom = null;
    Date dtTo = null;
    String strPO = "";
    String strInvSource = "";
    String strAppDateFmt = "";
    String strParentFl = "";
    String strAccountCurrencyId = "";
    String strCategory = "";

    //PMT-41835 Division Filter in the Invoice Report
    String strDivision = "";
    strDivision = GmCommonClass.parseZero((String) hmParam.get("DIVISIONID"));
    //PMT-41835
    strAccID = GmCommonClass.parseNull((String) hmParam.get("ACCID"));
    strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    strOrderID = GmCommonClass.parseNull((String) hmParam.get("ORDERID"));
    strInvoiceID = GmCommonClass.parseNull((String) hmParam.get("INVOICEID"));
    dtFrom = (Date) hmParam.get("FROMDT");

    dtTo = (Date) hmParam.get("TODT");

    strAppDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLDATEFMT"));
    strPO = GmCommonClass.parseNull((String) hmParam.get("PO"));
    strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURCE"));
    strParentFl = GmCommonClass.parseNull((String) hmParam.get("PARENTFL"));
    strAccountCurrencyId = GmCommonClass.parseNull((String) hmParam.get("ACC_CUR_ID"));
    String strDateType = GmCommonClass.parseNull((String) hmParam.get("DATETYPE"));
    strCategory = GmCommonClass.parseNull((String) hmParam.get("CUSTCATEGORY"));
    
    // if invoice source is Dealer (26240213) fetch by dealer else fetch by customers
    if (strInvSource.equals("26240213")) {

      sbQuery.append(" SELECT V503_DEALER_ID ACCID ");
      sbQuery.append(" ,DECODE('" + getComplangid()
          + "','103097',NVL(V503_DEALER_NM_EN,V503_DEALER_NM),V503_DEALER_NM) NAME ");
    } else {

      sbQuery.append(" SELECT NVL(V503_DISTRIBUTOR_ID, V503_ACCOUNT_ID) ACCID ");
      sbQuery.append(" , NVL(V503_DISTRIBUTOR_NAME, DECODE(" + getComplangid()
          + ",'103097',NVL(V503_ACCOUNT_NM_EN,V503_ACCOUNT_NM),V503_ACCOUNT_NM)) NAME ");
      
      if(strCategory.equals("Y")){
    	  sbQuery.append(" ,get_code_name(GET_ACCOUNT_ATTRB_VALUE (NVL(V503_DISTRIBUTOR_ID, V503_ACCOUNT_ID),'5504')) CUSTCATEGORY  "); 
      }
      
      sbQuery.append(" ,EMAILREQ.ATR_VAL EMAILREQ, EVERSION.ATR_VAL EVERSION  ");
      sbQuery.append(", V503_PARACCT_NM PARENTACCTNM , V503_PARACCT_ID PARENTACCTID ");

    }

    sbQuery.append(" , V503_INVOICE_ID INVID, V503_CUSTOMER_PO PO ");
    sbQuery.append(" ,V503_INVOICE_DATE INVDT, V503_INVOICE_DATE INVDTREPORT ");
    sbQuery
        .append(" ,V503_INV_AMT COST, V503_INV_PAID AMTPAID, V503_STATUS_FL INVSFL, GET_LOG_FLAG(V503_INVOICE_ID,1201) CALL_FLAG , v503_acc_currency_nm CURRENCY ");
    sbQuery
        .append(" ,V503_PDF_ID PDFFILEID, V503_CSV_ID CSVFILEID");
    sbQuery
        .append(" ,V503_RTF_ID RTFFILEID , V503_XML_ID XMLFILEID");
    sbQuery.append(" ,v503_invoice_date INVYEAR, T901.c901_code_nm invtype  "); // Check
    if (strInvSource.equals("50253")) {
    	sbQuery.append(" , t501.icsflag ICSINCLUDE ");
    }
    // if
    // invoice
    // files
    // is
    // store
    // or
    // not,
    // if
    // store
    // then
    // return
    // id.
    // 90906
    // -
    // Invoice
    // PDF
    // File.
    sbQuery.append(" FROM v503_invoice_reports v503, T901_code_lookup t901 ");
    if (strInvSource.equals("50253")) { 
    	sbQuery.append(", ( SELECT c503_invoice_id , 'Y' icsflag ");
    	sbQuery.append(" FROM t501_order ");
    	sbQuery.append(" WHERE "); 
    	sbQuery.append(" c901_order_type = 102080 ");
    	sbQuery.append(" AND c501_ref_id IS NOT NULL ");
    	sbQuery.append(" AND c503_invoice_id is not null ");
    	sbQuery.append(" AND c501_delete_fl IS NULL ");
    	sbQuery.append(" AND c501_void_fl IS NULL) t501 ");
    	
    }
    if (!strInvSource.equals("26240213")) {
      // To get Email Request Flag value from attribute table, 91983 = Electronic Email
      sbQuery
          .append(" , (SELECT C704A_ATTRIBUTE_VALUE ATR_VAL, C704_ACCOUNT_ID FROM T704A_ACCOUNT_ATTRIBUTE ");
      sbQuery.append(" WHERE C704A_VOID_FL IS NULL ");
      sbQuery.append(" AND C901_ATTRIBUTE_TYPE = 91983)EMAILREQ ");

      // To get the Email Version from the attribute table, 91984 = Electronic File Format
      sbQuery
          .append(" , (SELECT C704A_ATTRIBUTE_VALUE ATR_VAL, C704_ACCOUNT_ID FROM T704A_ACCOUNT_ATTRIBUTE ");
      sbQuery.append(" WHERE C704A_VOID_FL IS NULL ");
      sbQuery.append(" AND C901_ATTRIBUTE_TYPE = 91984)EVERSION ");
    }
    sbQuery.append(" WHERE V503_INVOICE_ID IS NOT NULL ");
    if (strInvSource.equals("50253")) {
      sbQuery.append(" AND V503_INVOICE_ID = t501.c503_invoice_id (+) ");
    }
    if (!strInvSource.equals("26240213")) {
      sbQuery.append(" AND v503.V503_ACCOUNT_ID = EMAILREQ.C704_ACCOUNT_ID(+) ");
      sbQuery.append(" AND v503.V503_ACCOUNT_ID = EVERSION.C704_ACCOUNT_ID(+) ");
    }
    sbQuery.append(" AND t901.c901_code_grp ='IVCTP' ");
    sbQuery.append(" AND t901.c901_code_id = V503.v503_invoice_type ");


    if (strType.equals("1")) {
      // sbQuery.append(" AND INVSFL IN (4,5) ");
      sbQuery.append(" AND V503_STATUS_FL < = ");
      sbQuery.append(strType);
    } else if (strType.equals("2")) {
      sbQuery.append(" AND V503_STATUS_FL = ");
      sbQuery.append(strType);
    }

    // If show parent account box is unchecked ,Only particular account information details should
    // show.
    // Account Filter
    if ((!strAccID.equals("")) && (!strAccID.equals("0")) && (strParentFl.equals(""))) {

      if (strInvSource.equals("50255") || strInvSource.equals("50257")
          || strInvSource.equals("50253")) {
        /*
         * The Changes made for PMT-834.Since Account Dropdown is changed to list accounts for ICS
         * the data fetched should be based on Accounts.
         */
        sbQuery.append(" AND V503_ACCOUNT_ID = ");
        sbQuery.append(strAccID);

      } else if (strInvSource.equals("50256")) {
        sbQuery.append(" AND V503_DISTRIBUTOR_ID = ");
        sbQuery.append(strAccID);

      } else if (strInvSource.equals("26240213")) {
        sbQuery.append(" AND V503_DEALER_ID =");
        sbQuery.append(strAccID);
      }

    }
    // If Show parent account is checked, All rep accounts that belongs to parent account should
    // need to show.
    if ((!strAccID.equals("")) && (!strAccID.equals("0")) && (!strParentFl.equals(""))
        && (!strInvSource.equals("26240213"))) {
      sbQuery
          .append(" AND v503. V503_PARACCT_ID  In (SELECT c101_party_id from t704_account where c704_account_id ='"
              + strAccID + "')");
    }
    sbQuery.append(" AND v503.V503_COMPANY_ID = '" + getCompId() + "'");

    // 26240439 - invoice statement , to avoid displaying invoice statement types
    sbQuery
        .append(" AND NVL (v503.V503_INVOICE_TYPE, -9999) NOT IN (SELECT t906.c906_rule_value FROM t906_rules t906"
            + "  WHERE t906.c906_rule_grp_id = 'INVOICETYPE' AND C906_RULE_ID = 'EXCLUDE' AND C906_VOID_FL IS NULL) ");

    if (!strInvSource.equals("26240213")) {
      sbQuery.append(" AND v503.v503_acc_currency_id ='" + strAccountCurrencyId + "'");
    }

    if (!strInvSource.equals("0") || !strInvSource.equals(null) || !strInvSource.equals("")) {
      sbQuery.append(" AND V503_INVOICE_SOURCE = " + strInvSource + " ");
    }
    // Filter By Order
    if (!strOrderID.equals("")) {
      sbQuery.append(" AND (V503_INVOICE_ID) IN ");
      sbQuery.append(" ( SELECT C503_INVOICE_ID ");
      sbQuery.append(" FROM  T501_ORDER ");
      sbQuery.append(" WHERE C501_ORDER_ID  =  '");
      sbQuery.append(strOrderID);
      sbQuery.append("' AND nvl(C901_order_type,-9999) NOT IN ('2535','101260')  ");
      sbQuery.append(" AND NVL (c901_order_type, -9999) NOT IN ( ");
      sbQuery.append(" SELECT t906.c906_rule_value ");
      sbQuery.append(" FROM t906_rules t906 ");
      sbQuery.append(" WHERE t906.c906_rule_grp_id = 'ORDTYPE' ");
      sbQuery.append(" AND c906_rule_id = 'EXCLUDE') ) ");
    }



    // Filter By PO
    if (!strPO.equals("")) {
      sbQuery.append(" AND V503_CUSTOMER_PO  =  '");
      sbQuery.append(strPO);
      sbQuery.append("'");
    }
    // Filter By PO
    if (!strInvoiceID.equals("")) {
      sbQuery.append(" AND V503_INVOICE_ID   =  '");
      sbQuery.append(strInvoiceID);
      sbQuery.append("'");
    }

    // Filter for from and to date
    if (dtFrom != null) {
      if(strDateType.equals("106460")){
        sbQuery.append(" AND TRUNC(V503_CREATED_DATE) >= TO_DATE('");
       
      }else if(strDateType.equals("106461")){
        sbQuery.append(" AND TRUNC(V503_INVOICE_DATE) >= TO_DATE('");
      }
      sbQuery.append(GmCommonClass.getStringFromDate(dtFrom, strAppDateFmt));
      sbQuery.append("' , '");
      sbQuery.append(strAppDateFmt);
      sbQuery.append("' )");

    }
    if (dtTo != null) {
      if(strDateType.equals("106460")){
      sbQuery.append(" AND TRUNC(V503_CREATED_DATE) <= TO_DATE('");
      }else if(strDateType.equals("106461")){
        sbQuery.append(" AND TRUNC(V503_INVOICE_DATE) <= TO_DATE('");
      }
      sbQuery.append(GmCommonClass.getStringFromDate(dtTo, strAppDateFmt));
      sbQuery.append("' , '");
      sbQuery.append(strAppDateFmt);
      sbQuery.append("' )");

    }  
    //PMT-41835 Division Filter in the Invoice Report
    if(!strDivision.equals("0")){
    	sbQuery.append(" AND V503_DIVISION_ID = ");
    	sbQuery.append(strDivision);
    	sbQuery.append("");
    }//PMT-41835

    sbQuery.append(" ORDER BY UPPER(NAME) ASC ");
    sbQuery.append(" ,V503_INVOICE_DATE ASC ");
    sbQuery.append(" ,V503_INVOICE_ID ASC ");
    
    long starttime = System.currentTimeMillis();
    log.debug(" Qurty for Invoice Report -- " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    long endtime = System.currentTimeMillis();
    long tottime = endtime - starttime;
    log.debug(" Total time taken for Invoice reports is " + tottime);
    log.debug(" Total elements for Invoice reports is " + alReturn.size());

    hmReturn.put("REPORT", alReturn);
    return hmReturn;
  } // End of reportPendingInvoice

}// end of class GmARBean
