/**
 * 
 */
package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author mmuthusamy
 * 
 */
public class GmARSummaryBean extends GmBean implements GmARSummaryInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmARSummaryBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * @param gmDataStoreVO
   * @throws AppError
   */
  public GmARSummaryBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
  }

  /**
   * reportARSummaryByDueDate - This method is used to fetch AR Summary Information based on due
   * date,
   * 
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  @Override
  public ArrayList reportARSummaryByDueDate(HashMap hmParam) throws AppError {
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
    if (gmDataStoreVO == null) {
      gmDataStoreVO = getGmDataStoreVO();
    }
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);

    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    String strAccId = "";
    strAccId = (String) hmParam.get("ACCID");
    String strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURCE"));
    String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("SALESFILTER"));
    String strPaySince = GmCommonClass.parseNull((String) hmParam.get("PAYSINCEDT"));
    String strCollectorId = GmCommonClass.parseZero((String) hmParam.get("COLLECTORID"));
    String strGroupId = GmCommonClass.parseZero((String) hmParam.get("GROUPID"));
    String strParentFl = GmCommonClass.parseNull((String) hmParam.get("PARENTCHKFL"));
    String strDateFormat = gmDataStoreVO.getCmpdfmt();
    String strARCurrId = GmCommonClass.parseNull((String) hmParam.get("COMP_CURR"));
    String strLanguageId = GmCommonClass.parseNull(gmDataStoreVO.getCmplangid());
    String strDivisionId = GmCommonClass.parseZero((String) hmParam.get("COMP_DIV_ID"));
    String strCategory = GmCommonClass.parseNull((String) hmParam.get("CUSTCATEGORY"));
    String strCategoryId = GmCommonClass.parseZero((String) hmParam.get("CATEGORYID"));
    strDivisionId = strDivisionId.equals("0") ? "" : strDivisionId;

    if (!strInvSource.equals("26240213")) {
    	
    	if(strCategory.equals("Y")){
    		sbQuery.append(" select t.*,get_code_name(t.CATEGORYID) CUSTCATEGORY from ( ");
    	}
      // ALL - Include the company customers and Company Dealers. (PMT-14375)
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" SELECT   DECODE('" + strParentFl
            + "','Y', V503_PARACCT_ID,V503_ACCOUNT_ID)  ID  ,   ");

      } else {
        sbQuery
            .append(" SELECT   DECODE( V503_INVOICE_SOURCE,'50256',V503_DISTRIBUTOR_ID ,DECODE('"
                + strParentFl + "','Y', V503_PARACCT_ID,V503_ACCOUNT_ID))  ID  ,   ");
      }
      sbQuery.append(" DECODE(V503_INVOICE_SOURCE,'50256',V503_DISTRIBUTOR_NAME,DECODE('"
          + strParentFl + "','Y', V503_PARACCT_NM,DECODE(" + strLanguageId
          + ",'103097',NVL(V503_ACCOUNT_NM_EN,V503_ACCOUNT_NM),V503_ACCOUNT_NM)))  NAME  , ");

      if(strCategory.equals("Y")){
    	  sbQuery.append(" GET_ACCOUNT_ATTRB_VALUE (V503_ACCOUNT_ID,'5504') CATEGORYID, "); 
      }
      sbQuery
          .append(" V503_TERMS TERM ,GET_CODE_NAME(V503_INVOICE_SOURCE) INV_SRC ,V503_INVOICE_SOURCE INV_SRC_ID , ");
      sbQuery.append(" V503_ACC_CURRENCY_NM CURRENCY ,");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 1 ), -1,"
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0 )),0) CURRENTVALUE  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 0 ), 1, "
              + "DECODE(SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 31 ), -1, "
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0) ,0 )),0) ONE_THIRTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 30 ), 1,"
              + " DECODE(SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 61 ), -1, "
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0) ,0 )),0) THIRTYONE_SIXTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 60 ), 1,"
              + " DECODE(SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 91 ), -1, "
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0) ,0 )),0) SIXTYONE_NINETY , ");

      // generateAdditionalIntervalsByDueDate method is used to create new intervals based on input.
      sbQuery.append(generateAdditionalIntervalsByDueDate(hmParam));
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(CURRENT_DATE)  - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 120),1,"
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0 )),0) GREATER_ONETWENTY , ");
      sbQuery.append(" NVL(SUM( (V503_INV_AMT - NVL(V503_INV_PAID,0))),0)  ROW_TOTAL ");

      sbQuery
          .append(", get_code_name(v503_region) Region    , v503_credit_rating Credit_Rating, v503_credit_rating_date  Last_Credit_Updated_Date");
      // When Show Parent fl is Y, we cannot include the last payment date,as the records will be
      // having multiple receive date for the rep account.
      // Las Payment Date is applicable only when the report is loaded for Rep Account only.
      sbQuery.append(" ,DECODE('" + strParentFl + "','Y','',to_char(v503_acc_last_payment_rece_date,'"
          + strDateFormat + "') )last_payment_received_date ");
      sbQuery.append(" , GET_CODE_NAME(T704a.COLLECTORID ) COLLECTOR_NAME  ");
      sbQuery.append(" , GET_CODE_NAME(T704a.CREDITTYPE ) CREDITTYPE_NAME ");
      sbQuery.append(" , T704a.CREDITLIMIT ");
      sbQuery.append(" ,v503_division_name DIVISION_NAME ");
      sbQuery.append(" ,v503_division_id DIVISION_ID ");

      sbQuery.append(" FROM v503_invoice_reports ");
      // PC-3144 : AR Summary report timeout issue fix
      
      sbQuery.append(" , (  SELECT t704A.C704_ACCOUNT_ID ");
      sbQuery
          .append("  , MAX (DECODE (C901_ATTRIBUTE_TYPE, 103050,C704A_ATTRIBUTE_VALUE)) COLLECTORID ");
      sbQuery
          .append("  , MAX (DECODE (C901_ATTRIBUTE_TYPE, '101182', C704A_ATTRIBUTE_VALUE)) CREDITLIMIT ");
      sbQuery
          .append(" , MAX (DECODE (C901_ATTRIBUTE_TYPE, '101184', C704A_ATTRIBUTE_VALUE)) CREDITTYPE ");
      sbQuery.append("  FROM t704a_account_attribute t704a ");
      sbQuery.append("  WHERE c704a_void_fl  IS NULL ");
      sbQuery.append(" GROUP BY C704_ACCOUNT_ID  ");
      sbQuery.append(" )t704a ");

      sbQuery.append(" WHERE V503_STATUS_FL < 2  ");

      // If Type is 50255 =" company customers" we are going filter based on currency .
      if (strInvSource.equals("50255")) {
        sbQuery.append(" AND V503_ACC_CURRENCY_ID ='" + strARCurrId + "'");
      }

      // if show parent account box is not checked only that particular account information will
      // going
      // to show.
      if (!strAccId.equals("") && !strAccId.equals("0") && strParentFl.equals("")) {

        if (strInvSource.equals("50255") || strInvSource.equals("50257")
            || strInvSource.equals("50253")) {
          /*
           * The Changes made for PMT-834.Since Account Dropdown is changed to list accounts for ICS
           * the data fetched should be based on Accounts.
           */
          sbQuery.append(" AND V503_ACCOUNT_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        } else {// if (strInvSource.equals("50256")) {
          sbQuery.append(" AND V503_DISTRIBUTOR_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        }

      }

   /*   if(!strCategoryId.equals("0")) {
      	  sbQuery.append(" AND (GET_ACCOUNT_ATTRB_VALUE (V503_ACCOUNT_ID,'5504')) = '" +strCategoryId+"'" );	  
      }*/
      if (!strSalesFilter.equals("")) {
        sbQuery
            .append(" AND  V503_ACCOUNT_ID in (SELECT DISTINCT ac_id FROM v700_territory_mapping_detail v700 WHERE ");
        sbQuery.append(strSalesFilter);
        sbQuery.append(" )");
      }
      // 7008 : Group
      if (!strGroupId.equals("0")) {
        sbQuery
            .append(" AND  V503_ACCOUNT_ID in (SELECT  t704a.c704_account_id ID FROM T704A_ACCOUNT_ATTRIBUTE t704a WHERE "
                + " t704a.c704a_attribute_value ='");
        sbQuery.append(strGroupId);
        sbQuery.append("' and t704a.c901_attribute_type='7008' and t704a.c704a_VOID_FL is null)");
      }

      // ALL - Getting the Company Customers (50255) invoices using UNION ALL
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" AND V503_INVOICE_SOURCE = '50255'");
      } else if (!strInvSource.equals("") && !strInvSource.equals("ALL")) {
        sbQuery.append(" AND V503_INVOICE_SOURCE = ");
        sbQuery.append(strInvSource);
      }
      if (!strCollectorId.equals("0")) {
        sbQuery.append(" AND T704a.COLLECTORID = '");
        sbQuery.append(strCollectorId);
        sbQuery.append("' ");
      }
      sbQuery.append(" AND V503_COMPANY_ID = '" + gmDataStoreVO.getCmpid() + "'");

      // 26240439 - invoice statement , to avoid displaying invoice statement types
      sbQuery
          .append(" AND NVL (V503_INVOICE_TYPE, -9999) NOT IN (SELECT t906.c906_rule_value FROM t906_rules t906"
              + "  WHERE t906.c906_rule_grp_id = 'INVOICETYPE' AND C906_RULE_ID = 'EXCLUDE' AND C906_VOID_FL IS NULL) ");

      sbQuery.append(" And  V503_ACCOUNT_ID = T704a.C704_Account_Id (+) ");
      if (!strPaySince.equals("")) {
        sbQuery.append(" and (v503_acc_last_payment_rece_date <= to_date('");
        sbQuery.append(strPaySince);
        sbQuery.append("','" + strDateFormat + "') OR v503_acc_last_payment_rece_date IS NULL) ");
      }

      if (!strDivisionId.equals("")) {
        sbQuery.append(" AND v503_DIVISION_ID =  " + strDivisionId + " ");
      }


      // if show parent account is checked ,all rep accounts related information will going to show.

      if ((!strAccId.equals("")) && (!strAccId.equals("0")) && !strParentFl.equals("")) {
        sbQuery
            .append(" AND v503_paracct_id = (select c101_party_id from t704_account where C704_ACCOUNT_ID = '"
                + strAccId + "') ");

      }
      sbQuery
          .append(" GROUP BY DECODE('"
              + strParentFl
              + "','Y', V503_PARACCT_ID,V503_ACCOUNT_ID) ,V503_ACC_CURRENCY_NM ,DECODE('"
              + strParentFl
              + "','Y', V503_PARACCT_NM,DECODE("
              + strLanguageId
              + ",'103097',NVL(V503_ACCOUNT_NM_EN,V503_ACCOUNT_NM),V503_ACCOUNT_NM)),V503_DISTRIBUTOR_ID, V503_DISTRIBUTOR_NAME,"
              + "V503_INVOICE_SOURCE,V503_TERMS  ");
      // When Show Parent fl is Y, we cannot include the last payment date,as the records will be
      // having multiple receive date for the rep account.
      // Las Payment Date is applicable only when the report is loaded for Rep Account only.
      sbQuery.append(" ,DECODE('" + strParentFl + "','Y','',to_char(v503_acc_last_payment_rece_date,'"
          + strDateFormat + "') ) ");

      sbQuery
          .append(" ,v503_region,v503_credit_rating,v503_credit_rating_date,T704a.COLLECTORID , T704a.CREDITTYPE , T704a.CREDITLIMIT ,v503_division_name, v503_division_id ");
      if(strCategory.equals("Y")) {
    	  sbQuery.append(" , v503_account_id  ");
      }
      if (!strInvSource.equals("ALL")) {
        sbQuery.append(" ORDER BY NAME, V503_DISTRIBUTOR_NAME  ");
      }
      if(strCategory.equals("Y")) {
    	  sbQuery.append(" )t " );
    	  if(!strCategoryId.equals("0")){
    		  sbQuery.append(" where nvl(CATEGORYID,'-999') = '" +strCategoryId+"'" );
          }
      }
     
      // if Invoice source is company dealer the below query will execute
    }

    // ALL - Include the company customers and Company Dealers. (PMT-14375)
    if (strInvSource.equals("ALL")) {
      sbQuery.append(" UNION ALL");
    }

    if (strInvSource.equals("26240213") || strInvSource.equals("ALL")) {

      sbQuery.append(" SELECT  V503_DEALER_ID ID  ,   ");
      sbQuery.append(" DECODE(" + strLanguageId
          + ",'103097',NVL(V503_DEALER_NM_EN,V503_DEALER_NM),V503_DEALER_NM) NAME  , ");

      sbQuery
          .append(" V503_TERMS TERM , GET_CODE_NAME(V503_INVOICE_SOURCE) INV_SRC ,V503_INVOICE_SOURCE INV_SRC_ID, ");
      sbQuery.append(" V503_ACC_CURRENCY_NM CURRENCY ,");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 1 ), -1,"
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0 )),0) CURRENTVALUE  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 0 ), 1, "
              + "DECODE(SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 31 ), -1, "
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0) ,0 )),0) ONE_THIRTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 30 ), 1,"
              + " DECODE(SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 61 ), -1, "
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0) ,0 )),0) THIRTYONE_SIXTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE( SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 60 ), 1,"
              + " DECODE(SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 91 ), -1,"
              + " (V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0) ,0 )),0) SIXTYONE_NINETY , ");
      sbQuery.append(generateAdditionalIntervalsByDueDate(hmParam));
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(CURRENT_DATE)  - TRUNC(get_payment_terms(V503_INVOICE_DATE,v503_terms_id)) - 120),1,"
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0 )),0) GREATER_ONETWENTY , ");
      sbQuery.append(" NVL(SUM( (V503_INV_AMT - NVL(V503_INV_PAID,0))),0)  ROW_TOTAL ");

      sbQuery
          .append(", get_code_name(v503_region) Region    , v503_credit_rating Credit_Rating, v503_credit_rating_date  Last_Credit_Updated_Date");
      sbQuery.append(" ,to_char(v503_dealer_last_payment_rece_date,'" + strDateFormat
          + "') last_payment_received_date ");
      sbQuery.append(" , GET_CODE_NAME(v503_dealer_collector_id) COLLECTOR_NAME  ");
      sbQuery.append(" , '' CREDITTYPE_NAME ");
      sbQuery.append(" , '' CREDITLIMIT ");
      sbQuery.append(" ,  v503_division_name DIVISION_NAME ");
      sbQuery.append(" , v503_division_id DIVISION_ID ");

      sbQuery.append(" FROM v503_invoice_reports ");
      // PC-3144 : AR Summary report timeout issue fix
      sbQuery.append(" WHERE V503_STATUS_FL <2");
      if (!strAccId.equals("") && !strAccId.equals("0")) {
        sbQuery.append(" AND V503_DEALER_ID ='" + strAccId + "'");
      }
      // To load the Dealer invoices based on more filter, We cannot have direct mapping for dealer
      // and region.So , checking against the accounts associated to that invoice.
      if (!strSalesFilter.equals("")) {
        sbQuery
            .append(" AND V503_Invoice_Id IN (SELECT c503_invoice_id FROM T501_Order WHERE C704_Account_Id IN ");
        sbQuery.append(" (SELECT  V700.Ac_Id FROM v700_territory_mapping_detail v700 WHERE ");
        sbQuery.append(strSalesFilter);
        sbQuery.append(" ) AND C501_VOID_FL IS NULL) ");

      }

      // ALL - Getting the Company Dealer (26240213) invoices using UNION ALL
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" AND V503_INVOICE_SOURCE = '26240213'");
      } else if (!strInvSource.equals("") && !strInvSource.equals("ALL")) {

        sbQuery.append(" AND V503_INVOICE_SOURCE = ");
        sbQuery.append(strInvSource);
      }

      if (!strCollectorId.equals("0")) {
        sbQuery.append(" AND v503_dealer_collector_id = '");
        sbQuery.append(strCollectorId);
        sbQuery.append("' ");
      }
      if (!strPaySince.equals("")) {
        sbQuery.append(" and (v503_dealer_last_payment_rece_date <= to_date('");
        sbQuery.append(strPaySince);
        sbQuery.append("','" + strDateFormat + "') OR v503_dealer_last_payment_rece_date IS NULL) ");
      }
      if (!strDivisionId.equals("")) {
        sbQuery.append(" AND v503_DIVISION_ID =  " + strDivisionId + " ");
      }
      sbQuery.append(" AND V503_COMPANY_ID = '" + gmDataStoreVO.getCmpid() + "'");

      // 26240439 - invoice statement , to avoid displaying invoice statement types
      sbQuery
          .append(" AND NVL (V503_INVOICE_TYPE, -9999) NOT IN (SELECT t906.c906_rule_value FROM t906_rules t906"
              + "  WHERE t906.c906_rule_grp_id = 'INVOICETYPE' AND C906_RULE_ID = 'EXCLUDE' AND C906_VOID_FL IS NULL) ");

      sbQuery
          .append(" GROUP BY V503_DEALER_ID , V503_ACC_CURRENCY_NM , V503_DEALER_NM,V503_DEALER_NM_EN,");
      sbQuery.append(" V503_TERMS ,v503_region,v503_credit_rating, v503_credit_rating_date,");
      sbQuery
          .append(" v503_dealer_collector_id, V503_INVOICE_SOURCE, v503_dealer_last_payment_rece_date ,v503_division_name,   v503_division_id ");
      if (!strInvSource.equals("ALL")) {
        sbQuery.append(" ORDER BY V503_DEALER_NM");
      }
    }

    if (strInvSource.equals("ALL")) {
      sbQuery.append(" ORDER BY NAME");
    }

    log.debug("Query for AR Summary: " + sbQuery.toString());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }

  /**
   * reportARSummaryByInvDate - This method is used to fetch AR Summary Information based on invoice
   * date,
   * 
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  @Override
  public ArrayList reportARSummaryByInvDate(HashMap hmParam) throws AppError {
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
    if (gmDataStoreVO == null) {
      gmDataStoreVO = getGmDataStoreVO();
    }
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);

    StringBuffer sbQuery = new StringBuffer();
    ArrayList alReturn = new ArrayList();
    String strAccId = "";
    strAccId = (String) hmParam.get("ACCID");
    String strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURCE"));
    String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("SALESFILTER"));
    String strPaySince = GmCommonClass.parseNull((String) hmParam.get("PAYSINCEDT"));
    String strCollectorId = GmCommonClass.parseZero((String) hmParam.get("COLLECTORID"));
    String strGroupId = GmCommonClass.parseZero((String) hmParam.get("GROUPID"));
    String strDateFormat = gmDataStoreVO.getCmpdfmt();
    String strParentFl = GmCommonClass.parseNull((String) hmParam.get("PARENTCHKFL"));
    String strARCurrId = GmCommonClass.parseNull((String) hmParam.get("COMP_CURR"));
    String strLanguageId = GmCommonClass.parseNull(gmDataStoreVO.getCmplangid());
    String strDivisionId = GmCommonClass.parseZero((String) hmParam.get("COMP_DIV_ID"));
    String strCategory = GmCommonClass.parseNull((String) hmParam.get("CUSTCATEGORY"));
    String strCategoryId = GmCommonClass.parseZero((String) hmParam.get("CATEGORYID"));
    strDivisionId = strDivisionId.equals("0") ? "" : strDivisionId;

    if (!strInvSource.equals("26240213")) {
    	if(strCategory.equals("Y")){
    		sbQuery.append(" select t.*,get_code_name(t.CATEGORYID) CUSTCATEGORY from ( ");
    	}
      // ALL - Include the company customers and Company Dealers. (PMT-14375)
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" SELECT   DECODE('" + strParentFl
            + "','Y', V503_PARACCT_ID,V503_ACCOUNT_ID)  ID  ,   ");

      } else {
        sbQuery
            .append(" SELECT   DECODE( V503_INVOICE_SOURCE,'50256',V503_DISTRIBUTOR_ID ,DECODE('"
                + strParentFl + "','Y', V503_PARACCT_ID,V503_ACCOUNT_ID))  ID  ,   ");
      }

      sbQuery.append(" DECODE(V503_INVOICE_SOURCE,'50256',V503_DISTRIBUTOR_NAME,DECODE('"
          + strParentFl + "','Y', V503_PARACCT_NM,DECODE(" + strLanguageId
          + ",'103097',NVL(V503_ACCOUNT_NM_EN,V503_ACCOUNT_NM),V503_ACCOUNT_NM)))  NAME  , ");
      
      if(strCategory.equals("Y")){
    	  sbQuery.append(" GET_ACCOUNT_ATTRB_VALUE (V503_ACCOUNT_ID,'5504') CATEGORYID,  "); 
      }
      sbQuery
          .append(" V503_TERMS TERM ,GET_CODE_NAME(V503_INVOICE_SOURCE) INV_SRC , V503_INVOICE_SOURCE INV_SRC_ID, ");
      sbQuery.append(" V503_ACC_CURRENCY_NM CURRENCY ,");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - 31),-1,"
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0 )),0) ZERO_THIRTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - 30 ),  1,"
              + " DECODE(SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - 61 ), -1, "
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0) ,0 )),0) THIRTYONE_SIXTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - 60 ),  1, "
              + "DECODE(SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - 91 ), -1, "
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0) ,0 )),0) SIXTYONE_NINETY  , ");
      // generateAdditionalIntervalsByInvDate method is used to create new intervals based on input.
      sbQuery.append(generateAdditionalIntervalsByInvDate(hmParam));

      sbQuery.append(" NVL(SUM( (V503_INV_AMT - NVL(V503_INV_PAID,0))),0)  ROW_TOTAL ");

      sbQuery
          .append(", get_code_name(v503_region) Region    , v503_credit_rating Credit_Rating, v503_credit_rating_date  Last_Credit_Updated_Date");

      // When Show Parent fl is Y, we cannot include the last payment date,as the records will be
      // having multiple receive date for the rep account.
      // Las Payment Date is applicable only when the report is loaded for Rep Account only.
      sbQuery.append(" ,DECODE('" + strParentFl + "','Y','',to_char(v503_acc_last_payment_rece_date,'"
          + strDateFormat + "') )last_payment_received_date ");
      sbQuery.append(" , GET_CODE_NAME(T704a.COLLECTORID ) COLLECTOR_NAME  ");
      sbQuery.append(" , GET_CODE_NAME(T704a.CREDITTYPE ) CREDITTYPE_NAME ");
      sbQuery.append(" , T704a.CREDITLIMIT ");
      sbQuery.append(" ,v503_division_name DIVISION_NAME ");
      sbQuery.append(" ,v503_division_id DIVISION_ID ");

      sbQuery.append(" FROM v503_invoice_reports ");
      // PC-3144: AR Summary report - timeout issue fix
      sbQuery.append(" , (  SELECT t704A.C704_ACCOUNT_ID ");
      sbQuery
          .append("  , MAX (DECODE (C901_ATTRIBUTE_TYPE, 103050,C704A_ATTRIBUTE_VALUE))  COLLECTORID ");
      sbQuery
          .append("  , MAX (DECODE (C901_ATTRIBUTE_TYPE, '101182', C704A_ATTRIBUTE_VALUE)) CREDITLIMIT ");
      sbQuery
          .append(" , MAX (DECODE (C901_ATTRIBUTE_TYPE, '101184' , C704A_ATTRIBUTE_VALUE)) CREDITTYPE ");
      sbQuery.append("  FROM t704a_account_attribute t704a ");
      sbQuery.append("  WHERE c704a_void_fl  IS NULL ");
      sbQuery.append(" GROUP BY C704_ACCOUNT_ID  ");
      sbQuery.append(" )t704a ");

      sbQuery.append(" WHERE V503_STATUS_FL < 2  ");
      // If Type is 50255 =" company customers" we are going filter based on currency .
      if (strInvSource.equals("50255")) {
        sbQuery.append(" And V503_ACC_CURRENCY_ID ='" + strARCurrId + "'");
      }

      // if show parent account is checked ,all rep accounts related information will going to show.
      if (!strAccId.equals("") && !strAccId.equals("0") && strParentFl.equals("")) {

        if (strInvSource.equals("50255") || strInvSource.equals("50257")
            || strInvSource.equals("50253")) {
          /*
           * The Changes made for PMT-834.Since Account Dropdown is changed to list accounts for ICS
           * the data fetched should be based on Accounts.
           */
          sbQuery.append(" AND V503_ACCOUNT_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        } else {// if (strInvSource.equals("50256")) {
          sbQuery.append(" AND V503_DISTRIBUTOR_ID = '");
          sbQuery.append(strAccId);
          sbQuery.append("' ");
        }

      }

     /* if(!strCategoryId.equals("0")) {
      	  sbQuery.append(" AND (GET_ACCOUNT_ATTRB_VALUE (V503_ACCOUNT_ID,'5504')) = '" +strCategoryId+"'" );	  
        }*/
      if (!strSalesFilter.equals("")) {
        sbQuery
            .append(" AND  V503_ACCOUNT_ID in (SELECT DISTINCT ac_id FROM v700_territory_mapping_detail v700 WHERE ");
        sbQuery.append(strSalesFilter);
        sbQuery.append(" )");
      }
      // 7008 : Group
      if (!strGroupId.equals("0")) {
        sbQuery
            .append(" AND  V503_ACCOUNT_ID in (SELECT  t704a.c704_account_id ID FROM T704A_ACCOUNT_ATTRIBUTE t704a "
                + "WHERE  t704a.c704a_attribute_value ='");
        sbQuery.append(strGroupId);
        sbQuery.append("' and t704a.c901_attribute_type='7008' and t704a.c704a_VOID_FL is null)");
      }
      // ALL - Getting the Company Customers (50255) invoices using UNION ALL
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" AND V503_INVOICE_SOURCE = '50255'");
      } else if (!strInvSource.equals("") && !strInvSource.equals("ALL")) {
        sbQuery.append(" AND V503_INVOICE_SOURCE = ");
        sbQuery.append(strInvSource);
      }
      if (!strCollectorId.equals("0")) {
        sbQuery.append(" AND T704a.COLLECTORID = '");
        sbQuery.append(strCollectorId);
        sbQuery.append("' ");
      }
      sbQuery.append(" AND V503_COMPANY_ID = '" + gmDataStoreVO.getCmpid() + "'");

      // 26240439 - invoice statement , to avoid displaying invoice statement types
      sbQuery
          .append(" AND NVL (V503_INVOICE_TYPE, -9999) NOT IN (SELECT t906.c906_rule_value FROM t906_rules t906"
              + "  WHERE t906.c906_rule_grp_id = 'INVOICETYPE' AND C906_RULE_ID = 'EXCLUDE' AND C906_VOID_FL IS NULL) ");

      sbQuery.append(" And  V503_ACCOUNT_ID = T704a.C704_Account_Id (+) ");
      if (!strPaySince.equals("")) {
        sbQuery.append(" and (v503_acc_last_payment_rece_date <= to_date('");
        sbQuery.append(strPaySince);
        sbQuery.append("','" + strDateFormat + "') OR v503_acc_last_payment_rece_date IS NULL) ");
      }

      if (!strDivisionId.equals("")) {
        sbQuery.append(" AND v503_division_id = " + strDivisionId + " ");
      }

      // if show parent account box is not checked only that particular account information will
      // going
      // to show.

      if ((!strAccId.equals("")) && (!strAccId.equals("0")) && !strParentFl.equals("")) {
        sbQuery
            .append(" AND v503_paracct_id = (select c101_party_id from t704_account where C704_ACCOUNT_ID = '"
                + strAccId + "') ");

      }

      sbQuery
          .append(" GROUP BY DECODE('"
              + strParentFl
              + "','Y', V503_PARACCT_ID,V503_ACCOUNT_ID) ,V503_ACC_CURRENCY_NM ,DECODE('"
              + strParentFl
              + "','Y', V503_PARACCT_NM,DECODE("
              + strLanguageId
              + ",'103097',NVL(V503_ACCOUNT_NM_EN,V503_ACCOUNT_NM),V503_ACCOUNT_NM)),V503_DISTRIBUTOR_ID, "
              + "V503_DISTRIBUTOR_NAME,V503_INVOICE_SOURCE,V503_TERMS   ");
      // When Show Parent fl is Y, we cannot include the last payment date,as the records will be
      // having multiple receive date for the rep account.
      // Las Payment Date is applicable only when the report is loaded for Rep Account only.
      sbQuery.append(" ,DECODE('" + strParentFl + "','Y','',to_char(v503_acc_last_payment_rece_date,'"
          + strDateFormat + "')) ");

      sbQuery
          .append(" ,v503_region,v503_credit_rating,v503_credit_rating_date,T704a.COLLECTORID , T704a.CREDITTYPE , T704a.CREDITLIMIT , v503_division_name,  v503_division_id ");
      if(strCategory.equals("Y")) {
    	  sbQuery.append(" , v503_account_id  ");
      }
      if (!strInvSource.equals("ALL")) {
        sbQuery.append(" ORDER BY NAME, V503_DISTRIBUTOR_NAME  ");
      }
      if(strCategory.equals("Y")) {
    	  sbQuery.append(" )t " );
    	  if(!strCategoryId.equals("0")){
    		  sbQuery.append(" where nvl(CATEGORYID,'-999') = '" +strCategoryId+"'" );
          }
      }
    }
    // ALL - Include the company customers and Company Dealers. (PMT-14375)
    if (strInvSource.equals("ALL")) {
      sbQuery.append(" UNION ALL");
    }

    if (strInvSource.equals("26240213") || strInvSource.equals("ALL")) {

      sbQuery.append(" SELECT   V503_DEALER_ID  ID  ,   ");
      sbQuery.append(" DECODE(" + strLanguageId
          + ",'103097',NVL(V503_DEALER_NM_EN,V503_DEALER_NM),V503_DEALER_NM)  NAME  , ");
      sbQuery
          .append(" V503_TERMS TERM , GET_CODE_NAME(V503_INVOICE_SOURCE) INV_SRC , V503_INVOICE_SOURCE INV_SRC_ID, ");
      sbQuery.append(" V503_ACC_CURRENCY_NM CURRENCY ,");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - 31), -1,"
              + "(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0 )),0) ZERO_THIRTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - 30 ),  1,"
              + "DECODE(SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - 61 ), -1,"
              + " (V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0) ,0 )),0) THIRTYONE_SIXTY  , ");
      sbQuery
          .append(" NVL(SUM(DECODE(  SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - 60 ),  1,"
              + "DECODE(SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - 91 ), -1,"
              + " (V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0) ,0 )),0) SIXTYONE_NINETY  , ");
      sbQuery.append(generateAdditionalIntervalsByInvDate(hmParam));

      sbQuery.append(" NVL(SUM( (V503_INV_AMT - NVL(V503_INV_PAID,0))),0)  ROW_TOTAL ");

      sbQuery
          .append(", get_code_name(v503_region) Region    , v503_credit_rating Credit_Rating, v503_credit_rating_date  Last_Credit_Updated_Date");

      sbQuery.append(" ,to_char(v503_dealer_last_payment_rece_date,'" + strDateFormat
          + "')last_payment_received_date ");
      sbQuery.append(" , GET_CODE_NAME(v503_dealer_collector_id) COLLECTOR_NAME  ");
      sbQuery.append(" , '' CREDITTYPE_NAME ");
      sbQuery.append(" , '' CREDITLIMIT ");
      sbQuery.append(", v503_division_name DIVISION_NAME ");
      sbQuery.append(" ,v503_division_id DIVISION_ID ");

      sbQuery.append(" FROM v503_invoice_reports ");
      // PC-3144: AR Summary report - timeout issue fix
      sbQuery.append(" WHERE V503_STATUS_FL <2");

      // To load the Dealer invoices based on more filter, We cannot have direct mapping for dealer
      // and region.So , checking against the accounts associated to that invoice.
      if (!strSalesFilter.equals("")) {
        sbQuery
            .append(" AND V503_Invoice_Id IN (SELECT c503_invoice_id FROM T501_Order WHERE C704_Account_Id IN ");
        sbQuery.append(" (SELECT  V700.Ac_Id FROM v700_territory_mapping_detail v700 WHERE ");
        sbQuery.append(strSalesFilter);
        sbQuery.append(" ) AND C501_VOID_FL IS NULL) ");
      }
      // ALL - Getting the Company Dealer (26240213) invoices using UNION ALL
      if (strInvSource.equals("ALL")) {
        sbQuery.append(" AND V503_INVOICE_SOURCE = '26240213'");
      } else if (!strInvSource.equals("") && !strInvSource.equals("ALL")) {

        sbQuery.append(" AND V503_INVOICE_SOURCE = ");
        sbQuery.append(strInvSource);
      }

      if (!strCollectorId.equals("0")) {
        sbQuery.append(" AND v503_dealer_collector_id = '");
        sbQuery.append(strCollectorId);
        sbQuery.append("' ");
      }
      if (!strPaySince.equals("")) {
        sbQuery.append(" and (v503_dealer_last_payment_rece_date <= to_date('");
        sbQuery.append(strPaySince);
        sbQuery.append("','" + strDateFormat + "') OR v503_dealer_last_payment_rece_date IS NULL) ");
      }
      if (!strAccId.equals("") && !strAccId.equals("0")) {
        sbQuery.append(" AND V503_DEALER_ID ='" + strAccId + "'");
      }

      if (!strDivisionId.equals("")) {
        sbQuery.append("AND v503_division_id =" + strDivisionId + " ");
      }

      sbQuery.append(" AND V503_COMPANY_ID = '" + gmDataStoreVO.getCmpid() + "'");

      // 26240439 - invoice statement , to avoid displaying invoice statement types
      sbQuery
          .append(" AND NVL (V503_INVOICE_TYPE, -9999) NOT IN (SELECT t906.c906_rule_value FROM t906_rules t906"
              + "  WHERE t906.c906_rule_grp_id = 'INVOICETYPE' AND C906_RULE_ID = 'EXCLUDE' AND C906_VOID_FL IS NULL) ");

      sbQuery
          .append(" GROUP BY V503_DEALER_ID , V503_ACC_CURRENCY_NM ,V503_DEALER_NM,V503_TERMS ,V503_DEALER_NM_EN,");
      sbQuery
          .append(" v503_region,v503_credit_rating,v503_credit_rating_date, v503_dealer_collector_id, v503_division_name,   v503_division_id");
      sbQuery.append(", V503_INVOICE_SOURCE, v503_dealer_last_payment_rece_date");
      if (!strInvSource.equals("ALL")) {
        sbQuery.append(" ORDER BY V503_DEALER_NM");
      }
    }

    if (strInvSource.equals("ALL")) {
      sbQuery.append(" ORDER BY NAME");
    }

    log.debug("Query for AR Summary: " + sbQuery.toString());
    log.debug(" Company Id " + gmDataStoreVO.getCmpid());

    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  }

  /**
   * generateAdditionalIntervalsByDueDate - This method is used to create interval for AR Summary
   * Due date query,
   * 
   * @author Jignesh shah
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  public String generateAdditionalIntervalsByDueDate(HashMap hmParam) throws AppError {
    /*
     * In this method we are creating intervals by adding invoice terms days
     */

    StringBuffer sbQuery = new StringBuffer();
    String strInterval = GmCommonClass.parseZero((String) hmParam.get("INTERVAL"));
    String strThrough = GmCommonClass.parseZero((String) hmParam.get("THROUGH"));
    String strDefaultThrough = GmCommonClass.parseZero((String) hmParam.get("DAFAULTTHROUGH"));
    String strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURCE"));
    String strPayCondition = "v503_terms_id";

    int intInterval = Integer.parseInt(strInterval);
    int intThrough = Integer.parseInt(strThrough);
    int intTempInterval = 0;
    int intPrevInterval = Integer.parseInt(strDefaultThrough);
    intTempInterval = intPrevInterval + intInterval;
    while (intTempInterval <= intThrough) {

      sbQuery
          .append(" NVL(SUM(DECODE(SIGN( TRUNC(CURRENT_DATE) - TRUNC(get_payment_terms(V503_INVOICE_DATE,"
              + strPayCondition
              + ")) - "
              + intPrevInterval
              + " ), 1, DECODE(SIGN( TRUNC(CURRENT_DATE)  - TRUNC(get_payment_terms(V503_INVOICE_DATE,"
              + strPayCondition
              + ")) - "
              + (intTempInterval + 1)
              + " ), -1, (V503_INV_AMT - NVL(V503_INV_PAID,0)),0) ,0 )),0) COL_"
              + (intPrevInterval + 1) + "_" + intTempInterval + " , ");

      log.debug("intPrevInterval=" + intPrevInterval + ":::intTempInterval=" + intTempInterval);
      intPrevInterval = intTempInterval;
      intTempInterval += intInterval;
    }
    sbQuery
        .append(" NVL(SUM(DECODE(  SIGN( TRUNC(CURRENT_DATE)  - TRUNC(get_payment_terms(V503_INVOICE_DATE,"
            + strPayCondition
            + ")) - "
            + intPrevInterval
            + "),1,(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0 )),0) COL_GREATER_"
            + intPrevInterval
            + " , ");
    log.debug(" Query formed for generateAdditionalIntervals is " + sbQuery.toString());
    return (sbQuery.toString());
  }

  /**
   * generateAdditionalIntervalsByInvDate - This method is used to create interval for AR Summary
   * Invoice date query,
   * 
   * @author Jignesh shah
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  public String generateAdditionalIntervalsByInvDate(HashMap hmParam) throws AppError {
    StringBuffer sbQuery = new StringBuffer();
    String strInterval = GmCommonClass.parseZero((String) hmParam.get("INTERVAL"));
    String strThrough = GmCommonClass.parseZero((String) hmParam.get("THROUGH"));
    String strDefaultThrough = GmCommonClass.parseZero((String) hmParam.get("DAFAULTTHROUGH"));

    int intInterval = Integer.parseInt(strInterval);
    int intThrough = Integer.parseInt(strThrough);
    int intTempInterval = 0;
    int intPrevInterval = Integer.parseInt(strDefaultThrough);

    // code for query up to 61-91

    intTempInterval = intPrevInterval + intInterval;
    while (intTempInterval <= intThrough) {

      sbQuery.append(" NVL(SUM(DECODE(SIGN( TRUNC(CURRENT_DATE) - TRUNC(V503_INVOICE_DATE) - "
          + intPrevInterval
          + " ), 1, DECODE(SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - "
          + (intTempInterval + 1)
          + " ), -1, (V503_INV_AMT - NVL(V503_INV_PAID,0)),0) ,0 )),0) COL_"
          + (intPrevInterval + 1) + "_" + intTempInterval + " , ");

      log.debug("intPrevInterval=" + intPrevInterval + ":::intTempInterval=" + intTempInterval);
      intPrevInterval = intTempInterval;
      intTempInterval += intInterval;
    }
    sbQuery.append(" NVL(SUM(DECODE(  SIGN( TRUNC(CURRENT_DATE)  - TRUNC(V503_INVOICE_DATE) - "
        + intPrevInterval + "),1,(V503_INV_AMT - NVL(V503_INV_PAID,0)) ,0 )),0) COL_GREATER_"
        + intPrevInterval + " , ");
    log.debug(" Query formed for generateAdditionalIntervals is " + sbQuery.toString());
    return (sbQuery.toString());
  } // end of generateAdditionalIntervalsByInvDate()

  /**
   * populateIntervalDtl - This method is used to create interval for AR Summary,
   * 
   * @author Jignesh shah
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  public ArrayList populateIntervalDtl(HashMap hmParam) throws AppError {

    /*
     * This method is used to create intervals for vm file In this method DAFAULTTHROUGH=90 ,
     * INTERVAL - input from user else 0 , THROUGH - input from user else 0 , while loop will create
     * e.g. HEADING = 91-120, KEY = COL_91_120, FROMDAY=90, TODAY=120
     */

    HashMap hmColumn;
    ArrayList alColumn = new ArrayList();
    String strInterval = GmCommonClass.parseZero((String) hmParam.get("INTERVAL"));
    String strThrough = GmCommonClass.parseZero((String) hmParam.get("THROUGH"));
    String strDefaultThrough = GmCommonClass.parseZero((String) hmParam.get("DAFAULTTHROUGH"));

    int intInterval = Integer.parseInt(strInterval);
    int intThrough = Integer.parseInt(strThrough);
    int intPrevInterval = Integer.parseInt(strDefaultThrough);
    int intTempInterval = 0;

    intTempInterval = intPrevInterval + intInterval;

    while (intTempInterval <= intThrough) {
      hmColumn = new HashMap();
      // HEADING -Grid column heading
      hmColumn.put("HEADING", (intPrevInterval + 1) + "-" + intTempInterval);
      // KEY - Alias Name to get the corresponding value in vm file
      hmColumn.put("KEY", "COL_" + (intPrevInterval + 1) + "_" + intTempInterval);
      // FROMDAY & TODAY � values will pass fnViewDetails() in vm file
      hmColumn.put("FROMDAY", (intPrevInterval + 1));
      hmColumn.put("TODAY", intTempInterval);
      alColumn.add(hmColumn);
      intPrevInterval = intTempInterval;
      intTempInterval += intInterval;
    }

    // After loop end we have to show greater column if through is 120 then >120 will display
    hmColumn = new HashMap();
    hmColumn.put("HEADING", ">" + intPrevInterval);
    hmColumn.put("KEY", "COL_GREATER_" + intPrevInterval);
    hmColumn.put("FROMDAY", (intPrevInterval + 1));
    hmColumn.put("TODAY", "3000");
    alColumn.add(hmColumn);

    return alColumn;
  } // end of populateIntervalDtl()

  /**
   * reportARDetailByAccount - This method is used to fetch AR Summary Invoice details based on
   * account
   * 
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  @Override
  public RowSetDynaClass reportARDetailByAccount(HashMap hmParam) throws AppError {
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
    if (gmDataStoreVO == null) {
      gmDataStoreVO = getGmDataStoreVO();
    }
    String strRefId = GmCommonClass.parseNull((String) hmParam.get("hRefid"));
    String strReportType = GmCommonClass.parseNull((String) hmParam.get("REPORTTYPE"));
    String strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURSE"));
    String strFromDay = GmCommonClass.parseNull((String) hmParam.get("FROMDAY"));
    String strToDay = GmCommonClass.parseNull((String) hmParam.get("TODAY"));
    String strTxnTp = GmCommonClass.parseNull((String) hmParam.get("STRTXNTP"));
    String strDivId = GmCommonClass.parseNull((String) hmParam.get("DIV_COMP_ID"));
    RowSetDynaClass resultSet = null;
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    gmDBManager.setPrepareString("GM_REPORT_PEDNING_PAYMENT", 8);
    gmDBManager.setString(1, strRefId);
    gmDBManager.setString(2, strReportType);
    gmDBManager.setString(3, strInvSource);
    gmDBManager.setString(4, strFromDay);
    gmDBManager.setString(5, strToDay);
    gmDBManager.setString(6, strTxnTp);
    gmDBManager.setString(7, strDivId);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.execute();
    resultSet = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(8));
    gmDBManager.close();
    return resultSet;

  } // End of reportARDetailByAccount


  /**
   * method-fetchSnapShotDate -It fetchs snapshot date.
   * 
   * @param Hashmap
   * @exception AppError
   */
  public ArrayList fetchSnapShotDate() throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    String strPartNum = "";


    gmDBManager.setPrepareString("gm_pkg_ac_ar_snap_shot.gm_fch_snapshot_date", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    log.debug("Company ID " + getCompId() + " Records is " + alResult.size());
    return alResult;
  } // end of validateInvoice



}
