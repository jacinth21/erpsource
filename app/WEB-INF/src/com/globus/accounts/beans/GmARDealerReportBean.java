/*
 * Module: GmARDealerReportBean.java Author: Jeeva Balraman
 *  
 */

package com.globus.accounts.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmARDealerReportBean extends GmBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.

	// this will be removed all place changed with Data Store VO constructor
	public GmARDealerReportBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	/**
	 * Constructor will populate company info.
	 * 
	 * @param gmDataStore
	 */
	public GmARDealerReportBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	public ArrayList fetchSalesReport(HashMap hmParam) throws AppError {
		GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmParam.get("gmDataStoreVO");
		if (gmDataStoreVO == null) {
			gmDataStoreVO = getGmDataStoreVO();
		}
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		String strLanguageId = GmCommonClass.parseNull(gmDataStoreVO.getCmplangid());
		String strCompanyId = GmCommonClass.parseNull(gmDataStoreVO.getCmpid());
		String strAccId = GmCommonClass.parseNull((String) hmParam.get("STRACCID"));
		String strInvSource = GmCommonClass.parseNull((String) hmParam.get("STRINVSOURCE"));
		String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("STRFILTERCONDITION"));
		String strDivisionId = GmCommonClass.parseZero((String) hmParam.get("STRDIVISIONID"));
		strDivisionId = strDivisionId.equals("0") ? "" : strDivisionId;
		StringBuffer sbQuery = new StringBuffer();
		ArrayList alReturn = new ArrayList();
		sbQuery.append(" SELECT ");
		sbQuery.append(" v503_dealer_id ID, ");
		sbQuery.append(" DECODE(" + strLanguageId
				+ ", '103097', NVL(v503_dealer_nm_en, v503_dealer_nm), v503_dealer_nm) NAME , ");
		sbQuery.append(" get_dealer_address(v503_dealer_id) ADDRESS, ");
		sbQuery.append(" v503_acc_currency_nm   CURRENCY, ");
		sbQuery.append(
				" nvl(SUM(decode(sign(trunc(current_date) - trunc(get_payment_terms(v503_invoice_date, v503_terms_id)) - 0), 1, decode(sign(trunc ");
		sbQuery.append(
				" (current_date) - trunc(get_payment_terms(v503_invoice_date, v503_terms_id)) - 31), - 1,(v503_inv_amt - nvl(v503_inv_paid, 0)) ");
		sbQuery.append(" , 0), 0)), 0) AR_LAST_MONTH, ");
		sbQuery.append(
				" nvl(SUM(decode(sign(trunc(current_date) - trunc(get_payment_terms(v503_invoice_date, v503_terms_id)) - 1), - 1,(v503_inv_amt - ");
		sbQuery.append(" nvl(v503_inv_paid, 0)), 0)), 0) PAYMENT_THIS_MONTH, ");
		sbQuery.append(
				" nvl(SUM(decode(sign(trunc(current_date) - trunc(get_payment_terms(v503_invoice_date, v503_terms_id)) - 1), - 1,(v503_inv_amt) ");
		sbQuery.append(" , 0)), 0) THIS_MONTH_SALE, ");
		sbQuery.append(" nvl(SUM((v503_inv_amt - nvl(v503_inv_paid, 0))), 0) AR_AMOUNT_THIS_MONTH ");
		sbQuery.append(" FROM ");
		sbQuery.append(" v503_invoice_reports, ");
		sbQuery.append(" ( ");
		sbQuery.append(" SELECT ");
		sbQuery.append(" t503.c101_dealer_id, ");
		sbQuery.append(" MAX(t508.c508_received_date) last_payment_received_date ");
		sbQuery.append(" FROM ");
		sbQuery.append(" t508_receivables t508, ");
		sbQuery.append(" t503_invoice t503 ");
		sbQuery.append(" WHERE ");
		sbQuery.append(" t508.c508_received_mode NOT IN('5013','5014','5019') ");
		sbQuery.append(" AND t503.c1900_company_id = " + strCompanyId + " ");
		sbQuery.append(" AND t508.c503_invoice_id = t503.c503_invoice_id ");
		sbQuery.append(" GROUP BY ");
		sbQuery.append(" t503.c101_dealer_id) last_payment_info ");
		sbQuery.append(" WHERE ");
		sbQuery.append(" v503_status_fl < 2 ");
		if (!strAccId.equals("") && !strAccId.equals("0")) {
			sbQuery.append(" AND V503_DEALER_ID ='" + strAccId + "'");
		}
		if (!strSalesFilter.equals("")) {
			sbQuery.append(" AND v503_invoice_id IN ( ");
			sbQuery.append(" SELECT ");
			sbQuery.append(" c503_invoice_id ");
			sbQuery.append(" FROM ");
			sbQuery.append(" t501_order ");
			sbQuery.append(" WHERE ");
			sbQuery.append(" c704_account_id IN ");
			sbQuery.append(" (SELECT v700.ac_id FROM v700_territory_mapping_detail v700 WHERE ");
			sbQuery.append(strSalesFilter);
			sbQuery.append(" ) AND c501_void_fl IS NULL) ");
		}
		if (!strInvSource.equals("")) {
			sbQuery.append(" AND v503_invoice_source = " + strInvSource + " ");
		}
		sbQuery.append(" AND v503_company_id = " + strCompanyId + " ");
		if (!strDivisionId.equals("")) {
			sbQuery.append(" AND v503_DIVISION_ID =  " + strDivisionId + " ");
		}
		sbQuery.append(" AND nvl(v503_invoice_type, - 9999) NOT IN ( ");
		sbQuery.append(" SELECT t906.c906_rule_value FROM t906_rules t906 ");
		sbQuery.append(" WHERE t906.c906_rule_grp_id = 'INVOICETYPE' ");
		sbQuery.append(" AND c906_rule_id = 'EXCLUDE' AND c906_void_fl IS NULL) ");
		sbQuery.append(" AND v503_dealer_id = last_payment_info.c101_dealer_id (+) ");
		sbQuery.append(" GROUP BY ");
		sbQuery.append(" v503_dealer_id, v503_acc_currency_nm, ");
		sbQuery.append(" v503_dealer_nm, v503_dealer_nm_en, ");
		sbQuery.append(" v503_terms, v503_region, ");
		sbQuery.append(" v503_credit_rating, v503_credit_rating_date, ");
		sbQuery.append(" v503_dealer_collector_id, v503_invoice_source, ");
		sbQuery.append(" last_payment_received_date, v503_division_name, ");
		sbQuery.append(" v503_division_id ");
		sbQuery.append(" ORDER BY v503_dealer_nm");
		log.debug("Query for AR Dealer: " + sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alReturn;
	} // End of fetchSalesReport

}// end of class GmARDealerReportBean
