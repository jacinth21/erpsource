package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAuditReconciliationBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAuditReconciliationBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmAuditReconciliationBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public RowSetDynaClass fetchVarianceRptBySet(HashMap hmParam) throws AppError {

    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strTerritoryId = GmCommonClass.parseNull((String) hmParam.get("TERRITORYID"));
    strTerritoryId = strTerritoryId.equals("") ? "0" : strTerritoryId;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass rdVarianceRptBySet = null;
    gmDBManager.setPrepareString("gm_pkg_ac_audit_reconciliation.gm_fch_variance_by_set", 4);
    gmDBManager.setString(1, strAuditId);
    gmDBManager.setString(2, strSetId);
    gmDBManager.setString(3, strTerritoryId);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdVarianceRptBySet = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    return rdVarianceRptBySet;
  }

  public void saveApprove(HashMap hmParam) throws AppError {
    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    log.debug("hmParam" + hmParam);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_audit_reconciliation.gm_sav_lock_distributor", 3);
    gmDBManager.setString(1, strAuditId);
    gmDBManager.setString(2, strDistId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public String saveReconcile(HashMap hmParam) throws AppError {
    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strTxnIds = "";
    postingValidation(hmParam);
    log.debug("hmParam" + hmParam);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_audit_reconciliation.gm_sav_post_distributor", 4);
    gmDBManager.setString(1, strAuditId);
    gmDBManager.setString(2, strDistId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strTxnIds = gmDBManager.getString(4);
    gmDBManager.commit();

    log.debug("strTxnIds" + strTxnIds);

    return strTxnIds;
  }

  public RowSetDynaClass fetchPostingError(HashMap hmParam) throws AppError {

    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass rdPostingError = null;

    gmDBManager.setPrepareString("gm_pkg_ac_audit_reconciliation.gm_fech_posting_error", 3);
    gmDBManager.setString(1, strAuditId);
    gmDBManager.setString(2, strDistId);

    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdPostingError = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return rdPostingError;

  }


  public void postingValidation(HashMap hmParam) throws AppError {
    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    log.debug("	hmParam" + hmParam);
    String strMessage = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_audit_reconciliation.gm_post_validation", 3);
    gmDBManager.setString(1, strAuditId);
    gmDBManager.setString(2, strDistId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }


  public RowSetDynaClass fetchVarianceRptByDist(HashMap hmParam) throws AppError {

    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    RowSetDynaClass rdVarianceRptByDist = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_audit_reconciliation.gm_fch_variance_by_dist", 3);
    gmDBManager.setString(1, strAuditId);
    gmDBManager.setString(2, strDistId);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdVarianceRptByDist = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return rdVarianceRptByDist;

  }

  public RowSetDynaClass fetchUploadedData(HashMap hmParam) throws AppError {

    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strAuditorId = GmCommonClass.parseNull((String) hmParam.get("AUDITORID"));
    strAuditorId = strAuditorId.equals("") ? "0" : strAuditorId;
    String strReconSts = GmCommonClass.parseZero((String) hmParam.get("RECONSTS"));
    String strEntryTyp = GmCommonClass.parseZero((String) hmParam.get("ENTRYTYP"));
    String strCodeGrp = GmCommonClass.parseNull((String) hmParam.get("CODEGRP"));
    log.debug("strCodeGrp" + strCodeGrp);
    GmDBManager gmDBManager = new GmDBManager();
    RowSetDynaClass rdVerifyUpload = null;

    gmDBManager.setPrepareString("gm_pkg_ac_audit_reconciliation.gm_fch_uploaded_data", 7);
    gmDBManager.setString(1, strAuditId);
    gmDBManager.setString(2, strDistId);
    gmDBManager.setString(3, strAuditorId);
    gmDBManager.setInt(4, Integer.parseInt(strReconSts));
    gmDBManager.setInt(5, Integer.parseInt(strEntryTyp));
    gmDBManager.setString(6, strCodeGrp);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdVerifyUpload = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(7));
    gmDBManager.close();
    return rdVerifyUpload;

  }

  public HashMap fetchVariance(HashMap hmParam) throws AppError {

    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass rdOverall = null;
    RowSetDynaClass rdAuditReport = null;
    RowSetDynaClass rdSystemReport = null;
    RowSetDynaClass rdPAssociatedReport = null;
    RowSetDynaClass rdNAssociatedReport = null;
    RowSetDynaClass rdReturnReport = null;

    HashMap hmResult = new HashMap();
    HashMap hmHeaderInfo = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_ac_audit_reconciliation.gm_fch_variance", 10);
    gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
    gmDBManager.setString(2, GmCommonClass.parseNull(strDistId));
    gmDBManager.setString(3, GmCommonClass.parseNull(strSetId));
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(9, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
    gmDBManager.execute();
    // rdHeaderInfo = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
    hmHeaderInfo = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
    log.debug("hmHeaderInfo" + hmHeaderInfo);
    rdOverall = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));
    rdAuditReport = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(6));
    rdSystemReport = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(7));
    rdPAssociatedReport = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(8));
    rdNAssociatedReport = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(9));
    rdReturnReport = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(10));
    hmResult.put("HEADERINFO", hmHeaderInfo);
    hmResult.put("OVERALLREPORT", rdOverall);
    hmResult.put("AUDITREPORT", rdAuditReport);
    hmResult.put("SYSTEMREPORT", rdSystemReport);
    hmResult.put("PASSOCIATEDREPORT", rdPAssociatedReport);
    hmResult.put("NASSOCIATEDREPORT", rdNAssociatedReport);
    hmResult.put("RETURNREPORT", rdReturnReport);
    gmDBManager.close();
    return hmResult;

  }

  public void saveFlaggedVariance(HashMap hmParam) throws AppError {
    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strInput = GmCommonClass.parseNull((String) hmParam.get("STRINPUT"));
    String strVarianceType = GmCommonClass.parseNull((String) hmParam.get("STRVARIANCETYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    log.debug("strAuditId" + strAuditId);
    log.debug("strSetId" + strSetId);
    log.debug("strDistId" + strDistId);
    log.debug("strInput" + strInput);
    log.debug("strVarianceType" + strVarianceType);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_audit_reconciliation.gm_sav_flag_variance", 6);
    gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
    gmDBManager.setString(2, GmCommonClass.parseNull(strDistId));
    gmDBManager.setString(3, GmCommonClass.parseNull(strSetId));
    gmDBManager.setString(4, GmCommonClass.parseNull(strInput));
    gmDBManager.setString(5, GmCommonClass.parseNull(strVarianceType));
    gmDBManager.setString(6, GmCommonClass.parseNull(strUserId));
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public HashMap fetchConfirmationReport(HashMap hmParam) throws AppError {
    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    HashMap hmHeaderInfo = new HashMap();
    ArrayList alOverAllReport = new ArrayList();
    ArrayList alMissingReport = new ArrayList();
    ArrayList alExtraReport = new ArrayList();
    ArrayList alMatchingReport = new ArrayList();
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_audit_reconciliation.gm_fetch_confirmation_report", 7);
    gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
    gmDBManager.setString(2, GmCommonClass.parseNull(strDistId));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmHeaderInfo = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    alOverAllReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    alMissingReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
    alExtraReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
    alMatchingReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    gmDBManager.close();
    hmReturn.put("HEADERINFO", hmHeaderInfo);
    hmReturn.put("OVERALLREPORT", alOverAllReport);
    hmReturn.put("MISSINGREPORT", alMissingReport);
    hmReturn.put("EXTRAREPORT", alExtraReport);
    hmReturn.put("MATCHINGREPORT", alMatchingReport);
    log.debug(hmReturn);
    return hmReturn;
  }
}
