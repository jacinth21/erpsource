/*
 * Module: GmARBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 11 Mar
 * 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.accounts.beans;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAPBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public GmAPBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAPBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * getDHRList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return Object
   * @exception AppError Modified by Joe P Kumar to implement Dynaset for calculating Total in DHR
   *            Payments screen Depending on the strPage Parameter the return type will be either
   *            Dynaset (for DHR Post Payment transaction <strPage will be POSTDHRTXN>) or ArrayList
   *            (for DHR Post Payment Report)
   * 
   **/
  public Object getDHRList(HashMap hmParam) throws AppError {
    RowSetDynaClass resultSet = null;

    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPartID = GmCommonClass.parseNull((String) hmParam.get("PARTID"));
    String strSort = GmCommonClass.parseNull((String) hmParam.get("SORT"));
    String strSortType = GmCommonClass.parseNull((String) hmParam.get("SORTTYPE"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
    String strPOID = GmCommonClass.parseNull((String) hmParam.get("POID"));
    String strWOID = GmCommonClass.parseNull((String) hmParam.get("WOID"));
    String strDHRId = GmCommonClass.parseNull((String) hmParam.get("DHRID"));
    String strVenId = GmCommonClass.parseNull((String) hmParam.get("VENDID"));
    String strPSlip = GmCommonClass.parseNull((String) hmParam.get("PSLIP"));
    String strPoType = GmCommonClass.parseNull((String) hmParam.get("POTYPE"));
    String strPage = GmCommonClass.parseNull((String) hmParam.get("PAGE"));

    String strStatus = GmCommonClass.parseNull((String) hmParam.get("INVCTYPE"));
    String strStatusval = GmCommonClass.parseNull((String) hmParam.get("INVCTYPEVAL"));
    String strOperSign = GmCommonClass.parseNull((String) hmParam.get("HOPERATORSIGN"));
    String strDateType = GmCommonClass.parseNull((String) hmParam.get("DATETYPE"));
    log.debug("hmParam>>>>>>>>>>>>>>>>>>>>>>>>" + hmParam);
    /*************** Added for AP DHR List***************Start ********/
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strShowOpenDHRFlag = GmCommonClass.parseNull((String) hmParam.get("SHOWOPENDHRFLAG"));
    String strPaymentId = GmCommonClass.parseNull((String) hmParam.get("HPAYMENTID"));
    String strPONumber = GmCommonClass.parseNull((String) hmParam.get("HPONUMBER"));
    String strDateFmt = getGmDataStoreVO().getCmpdfmt();
    /*************** Added for AP DHR List***************End ********/
    if (strPaymentId.equals(""))
      strPaymentId = "0";

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery.append(" SELECT	T408.C408_DHR_ID ID, T408.C205_PART_NUMBER_ID PNUM , ");
      sbQuery.append(" T205.C205_PART_NUM_DESC PDESC, GET_CODE_NAME(T205.c205_product_family) PFAMYNM, T205.c205_product_family PFAMYID, ");
      sbQuery.append(" T408.C408_QTY_RECEIVED QTYREC, ");
      sbQuery.append(" T408.C408_CONTROL_NUMBER CNUM, ");
      sbQuery.append(" T408.C408_RECEIVED_DATE CDATE, T408.C408_VERIFIED_TS VDATE,");
      sbQuery
          .append(" T408.C408_PACKING_SLIP PSLIP, C408_UPDATE_INV_FL , T402.C401_PURCHASE_ORD_ID POID, ");
      sbQuery.append(" C301_VENDOR_SH_NAME VENDNM, ");
      sbQuery.append(" T408.C402_WORK_ORDER_ID WORKID, ");
      sbQuery.append(" T408.C301_VENDOR_ID VID, NVL(t408.c408_qty_on_shelf,0) SHELFQTY,");
      sbQuery
          .append(" get_currency_conversion (T301.C301_CURRENCY, get_comp_curr(T401.c1900_company_id) , NVL(T406.C406_DOWNLOAD_DT ,NVL(C406_ENTERED_DT ,T408.C408_RECEIVED_DATE )), ");
      sbQuery
          .append(" NVL(decode(t408.c901_type, 80191 , t402.c402_split_cost ,decode(t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost , t402.c402_cost_price)),0)/T405.C405_UOM_QTY)* 1 USDCOST, ");
      sbQuery
          .append(" get_currency_conversion (T301.C301_CURRENCY, get_comp_curr(T401.c1900_company_id) ,  NVL(T406.C406_DOWNLOAD_DT ,NVL(C406_ENTERED_DT ,T408.C408_RECEIVED_DATE )) ,   ");
      sbQuery
          .append(" NVL((decode(t408.c901_type, 80191 , t402.c402_split_cost ,decode(t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost , t402.c402_cost_price))/T405.C405_UOM_QTY * NVL(T408.C408_QTY_ON_SHELF,T408.C408_QTY_RECEIVED) ),0))*1   USDTCOST, ");
      sbQuery
          .append(" NVL(decode(t408.c901_type, 80191 , t402.c402_split_cost ,decode(t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost , t402.c402_cost_price)),0)/T405.C405_UOM_QTY COST, T405.C405_UOM_QTY UOM, ");
      sbQuery
          .append(" NVL((decode(t408.c901_type, 80191 , t402.c402_split_cost ,decode(t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost , t402.c402_cost_price))/T405.C405_UOM_QTY * NVL(T408.C408_QTY_ON_SHELF,T408.C408_QTY_RECEIVED) ),0)   TCOST, ");

      sbQuery.append(" T408.C408_STATUS_FL SFL, NVL(T408.C408_PAYMENT_FL,'N') PFL, ");
      /*************** Added for AP DHR List***************Start ********/
      sbQuery
          .append(" ROUND(NVL(((decode(t408.c901_type, 80191 , t402.c402_split_cost ,decode(t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost , t402.c402_cost_price))/T405.C405_UOM_QTY) * T408.C408_QTY_RECEIVED), 0),2) RECCOST, ");
      sbQuery
          .append(" ROUND(NVL(((decode(t408.c901_type, 80191 , t402.c402_split_cost ,decode(t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost , t402.c402_cost_price))/T405.C405_UOM_QTY) * T408.C408_QTY_ON_SHELF), 0),2) SHELFCOST, ");
      sbQuery.append(" CASE ");
      sbQuery.append("   WHEN t408.c408_status_fl = '1' THEN 'Received' ");
      sbQuery.append("   WHEN t408.c408_status_fl = '2' THEN 'Inspected' ");
      sbQuery.append("   WHEN t408.c408_status_fl = '3' THEN 'Packaged' ");
      sbQuery.append("   WHEN t408.c408_status_fl = '4' THEN 'Verified' ");
      sbQuery.append(" END CURSTS, ");
      /*************** Added for AP DHR List***************End ********/
      sbQuery
          .append(" T406.C406_INVOICE_ID INVID, T408.C406_PAYMENT_ID PID,T901.C901_CODE_NM POTYPE ");

      sbQuery
      .append(" ,NVL(T4083.C4083_INVOICE_AMOUNT,'0') INVAMT ");
      sbQuery
      .append(" ,NVL(T4083.C4083_EXT_INVOICE_AMOUNT,'0') EXTINVAMT ");
      
      sbQuery
      .append(" ,NVL(T4083.C4083_CREDIT_AMOUNT,'0') CREAMT ");
      sbQuery
      .append(" ,NVL(T4083.C4083_DIFFERENCE_AMOUNT,'0') DIFF ");
      
      sbQuery
      .append(" ,T4083.C4083_INVOICE_AMOUNT PAYMENTINVAMT ");
      sbQuery
      .append(" ,T4083.C4083_EXT_INVOICE_AMOUNT PAYMENTEXTINVAMT ");
      
      sbQuery
      .append(" ,T4083.C4083_CREDIT_AMOUNT PAYMENTCREAMT ");
      sbQuery
      .append(" ,T4083.C4083_DIFFERENCE_AMOUNT PAYMENTDIFF ");
      
      
      sbQuery
          .append(" FROM T408_DHR T408, T402_WORK_ORDER T402, T406_PAYMENT T406, T401_PURCHASE_ORDER T401 , T405_VENDOR_PRICING_DETAILS T405");
      sbQuery.append(" ,T205_PART_NUMBER T205,T301_VENDOR T301 , T901_CODE_LOOKUP T901, T4083_AP_DHR_PAYMENT T4083 ");
      sbQuery.append(" WHERE T401.C401_PURCHASE_ORD_ID = T402.C401_PURCHASE_ORD_ID ");
      sbQuery.append(" AND T408.C402_WORK_ORDER_ID = T402.C402_WORK_ORDER_ID ");
      sbQuery.append(" AND T408.C408_DHR_ID = T4083.C408_DHR_ID (+)  ");
      sbQuery.append(" AND T205.C205_PART_NUMBER_ID = T408.C205_PART_NUMBER_ID ");
      sbQuery.append(" AND T301.C301_VENDOR_ID = T402.C301_VENDOR_ID ");
      sbQuery.append(" AND T901.C901_CODE_GRP='POTYP' ");
      sbQuery.append(" AND T901.C901_CODE_ID= T401.C401_TYPE ");
      sbQuery.append(" AND T402.C402_VOID_FL IS NULL ");
      sbQuery.append(" AND T408.C408_VOID_FL IS NULL ");
      sbQuery.append(" AND T405.C405_VOID_FL IS NULL ");
      sbQuery.append(" AND T4083.C4083_VOID_FL (+) IS NULL ");
      sbQuery.append(" AND T405.C405_PRICING_ID = T402.C405_PRICING_ID ");
      sbQuery.append(" AND T408.C406_PAYMENT_ID = T406.C406_PAYMENT_ID (+) ");
      sbQuery.append(" AND T408.c1900_company_id = '" + getGmDataStoreVO().getCmpid() + "' ");

      if (!strVenId.equals("0")) // Vendor ID
      {
        sbQuery.append(" AND T408.C301_VENDOR_ID = ");
        sbQuery.append(strVenId);
      }

      if (!strDHRId.equals("")) // DHR ID
      {
        sbQuery.append(" AND T408.C408_DHR_ID LIKE '");
        sbQuery.append(strDHRId);
        sbQuery.append("%'");
      }

      if (!strPOID.equals("")) // Purchase Order ID
      {
        sbQuery.append(" AND T402.C401_PURCHASE_ORD_ID LIKE '");
        sbQuery.append(strPOID);
        sbQuery.append("%'");
      }

      if (!strWOID.equals("")) // Work Order ID
      {
        sbQuery.append(" AND T408.C402_WORK_ORDER_ID LIKE '");
        sbQuery.append(strWOID);
        sbQuery.append("%'");
      }

      if (!strFromDt.equals("")) {
        if (strDateType.equals("7030")) {
          sbQuery.append(" AND T408.C408_RECEIVED_DATE >= TO_DATE('");
          sbQuery.append(strFromDt);
          sbQuery.append("', '" + strDateFmt + "') ");
        }
        if (strDateType.equals("7031")) {
          sbQuery.append(" AND T406.C406_DOWNLOAD_DT >= TO_DATE('");
          sbQuery.append(strFromDt);
          sbQuery.append("', '" + strDateFmt + "') ");
        }
      }

      if (!strToDt.equals("")) {
        if (strDateType.equals("7030")) {
          sbQuery.append(" AND T408.C408_RECEIVED_DATE <= TO_DATE('");
          sbQuery.append(strToDt);
          sbQuery.append("', '" + strDateFmt + "') ");
        }
        if (strDateType.equals("7031")) {
          sbQuery.append(" AND T406.C406_DOWNLOAD_DT <= TO_DATE('");
          sbQuery.append(strToDt);
          sbQuery.append("', '" + strDateFmt + "') ");
        }
      }

      if (!strPartID.equals("")) {
        sbQuery.append(" AND T408.C205_PART_NUMBER_ID IN ('");
        sbQuery.append(strPartID);
        sbQuery.append("') ");
      }
      // To fetch all the open DHR (DHR verified, but not attached to Invoice)
      if (strType.equals("103301")) {
        sbQuery.append(" AND T408.C408_STATUS_FL = 4 ");
        sbQuery.append(" AND T408.C406_PAYMENT_ID IS NULL");
        sbQuery.append(" AND T408.C408_PAYMENT_FL IS NULL");
      }

      // All Not Verified DHR
      if (strType.equals("103300")) {
        sbQuery.append(" AND T408.C408_STATUS_FL < 4 ");
      }

      // Flag For Payment (DHR verified, and attached to invoice)
      if (strType.equals("103302")) {
        sbQuery.append(" AND T408.C408_STATUS_FL = 4 ");
        sbQuery.append(" AND T408.C406_PAYMENT_ID IS NOT NULL");
      }

      // All DHR verified
      if (strType.equals("103303")) {
        sbQuery.append(" AND T408.C408_STATUS_FL = 4 ");

      }

      if (!strStatus.equals("") && !strStatus.equals("0")) {

        if (strStatusval.equals("") || strStatusval.equals("0")) {
          strOperSign = "=";
        }

        // As we are doing outer join for T406.C406_PAYMENT_ID column, we also need to do outer join
        // for status.
        sbQuery.append(" AND t406.C406_STATUS_FL  " + strOperSign);
        sbQuery.append("GET_CODE_NAME_ALT('" + strStatus + "')");
      }



      // To Filter on Packing Slip Number
      if (!strPSlip.equals("")) // Work Order ID
      {
        sbQuery.append(" AND T408.C408_PACKING_SLIP LIKE '");
        sbQuery.append(strPSlip);
        sbQuery.append("%'");
      }
      log.debug(" PO Type is " + strPoType);
      // To Filter on PO Type
      if (!strPoType.equals("0") && !strPoType.equals("")) // PO Type
      {
        sbQuery.append(" AND T401.C401_TYPE = '");
        sbQuery.append(strPoType);
        sbQuery.append("'");
      }

      /*************** Added for AP DHR List***************Start ********/
      if (strOpt.equalsIgnoreCase("loadDHR")) // New Invoice
      {
        sbQuery.append("AND T402.C401_PURCHASE_ORD_ID = '");
        sbQuery.append(strPONumber);
        sbQuery.append("'");
      }

      if (strOpt.equalsIgnoreCase("newInvc")) // New Invoice
      {
        sbQuery.append("AND T402.C401_PURCHASE_ORD_ID = '");
        sbQuery.append(strPONumber);
        sbQuery.append("'");
        sbQuery.append(" AND T408.C406_PAYMENT_ID IS NULL");
      }

      if (strOpt.equalsIgnoreCase("invcGo")) // Invoice "Go" button
      {
        sbQuery.append("AND T402.C401_PURCHASE_ORD_ID = '");
        sbQuery.append(strPONumber);
        sbQuery.append("'");
        if (strShowOpenDHRFlag.equals("on")) {
          sbQuery.append("AND (T408.C406_PAYMENT_ID = ");
          sbQuery.append(strPaymentId);
          sbQuery.append(" OR T408.C406_PAYMENT_ID IS NULL)");
        } else {
          sbQuery.append("AND T408.C406_PAYMENT_ID = ");
          sbQuery.append(strPaymentId);
        }
      }

      if (strOpt.equalsIgnoreCase("viewInvc")) // save button
      {
        sbQuery.append("AND (T408.C406_PAYMENT_ID = ");
        sbQuery.append(strPaymentId);
        sbQuery.append(")");
      }

      if (strOpt.equalsIgnoreCase("save")) // save button
      {
        sbQuery.append("AND T402.C401_PURCHASE_ORD_ID = '");
        sbQuery.append(strPONumber);
        sbQuery.append("'");
        sbQuery.append(" AND (T408.C406_PAYMENT_ID = ");
        sbQuery.append(strPaymentId);
        sbQuery.append(")");
      }
      if (strOpt.equalsIgnoreCase("loadDHR") || strOpt.equalsIgnoreCase("save")
          || strOpt.equalsIgnoreCase("invcGo") || strOpt.equalsIgnoreCase("newInvc")
          || strOpt.equalsIgnoreCase("viewInvc")) {
        sbQuery.append(" ORDER BY PID, ID asc");
      }
      /*************** Added for AP DHR List***************End ********/

      /********************************************
       * Code to handle sort functionality
       *******************************************/
      if (!strSort.equals("")) {
        if (strSort.equals("CDATE") || strSort.equals("DDATE") || strSort.equals("CLOSEDATE")) {
          sbQuery.append(" ORDER BY TO_DATE(" + strSort + ", '" + strDateFmt + "')");
        } else {
          sbQuery.append(" ORDER BY " + strSort);
        }

        // Sort order information
        if (strSortType.equals("1")) {
          sbQuery.append(" asc ");
        } else {
          sbQuery.append(" desc ");
        }

      }

      log.debug("DHR List Query " + sbQuery.toString());

      if (strPage.equalsIgnoreCase("POSTDHRTXN")) {
        resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
        return resultSet;
      }

      else {
        resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

      }
      gmDBManager.close();
    } catch (Exception e) {
      GmLogError.log("Exception in GmAPBean:getDHRList", "Exception is:" + e);
      throw new AppError(e);
    }
    return resultSet;
  } // End of getDHRList

  /**
   * postMultiplePayments - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap postDHRPayments(String strDetails, String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    CallableStatement csmt = null;


    String strMsg = "";
    String strPrepareString = null;

    HashMap hmReturn = new HashMap();

    gmDBManager.setPrepareString("GM_UPDATE_DHR_PAYMENT", 3);
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);

    gmDBManager.setString(1, strDetails);
    gmDBManager.setString(2, strUserId);


    gmDBManager.execute();
    strMsg = gmDBManager.getString(3);
    gmDBManager.commit();
    hmReturn.put("MSG", strMsg);


    return hmReturn;
  } // end of postMultiplePayments

  //

  /**
   * saveVendorPayment - This method will save Invoice details along with additional charge details
   * 
   * @param HashMap
   * @param String UserId
   * @return String
   * @exception AppError
   **/
  public String saveVendorPayment(HashMap hmParam, String strUserId) throws AppError {
    String strReturn = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("HVENDORID"));
    String strPaymentId = GmCommonClass.parseNull((String) hmParam.get("HPAYMENTID"));
    String strInvoiceId = GmCommonClass.parseNull((String) hmParam.get("INVOICEID"));
    String strInvoiceDt = GmCommonClass.parseNull((String) hmParam.get("INVOICEDT"));
    String strPaymentAmt = GmCommonClass.parseNull((String) hmParam.get("PAYMENTAMT"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("INVOICECOMMENTS"));
    String strInterComments = GmCommonClass.parseNull((String) hmParam.get("INVOICEINTCOMMENTS"));
    String strPOID = GmCommonClass.parseNull((String) hmParam.get("HPONUMBER"));
    String strDHRStr = GmCommonClass.parseNull((String) hmParam.get("HDHRID"));
    String strAddlChrgId = GmCommonClass.parseNull((String) hmParam.get("HADDLCHRGID"));

    log.debug("GmAPBean VendorId:" + strVendorId);
    log.debug("GmAPBean strPaymentId:" + strPaymentId);
    log.debug("GmAPBean strInvoiceId:" + strInvoiceId);
    log.debug("GmAPBean strInvoiceDt:" + strInvoiceDt);
    log.debug("GmAPBean strPaymentAmt:" + strPaymentAmt);
    log.debug("GmAPBean strComments:" + strComments);
    log.debug("GmAPBean strPOID:" + strPOID);
    log.debug("GmAPBean DHRString:" + strDHRStr);
    log.debug("GmAPBean AddlChrgId:" + strAddlChrgId);

    if (strPaymentId.equals("")) {
      strPaymentId = "0";
    }
    log.debug("strPaymentId before set to proc: " + strPaymentId);
    log.debug("paymentid: " + Integer.parseInt(strPaymentId));

    gmDBManager.setPrepareString("gm_pkg_ac_payment.gm_update_dhr_payment_multiple", 12);

    gmDBManager.registerOutParameter(12, java.sql.Types.CHAR);

    gmDBManager.setInt(1, Integer.parseInt(strVendorId));
    gmDBManager.setInt(2, Integer.parseInt(strPaymentId));
    gmDBManager.setString(3, strInvoiceId);
    gmDBManager.setString(4, strInvoiceDt);
    gmDBManager.setDouble(5, Double.parseDouble(strPaymentAmt));
    gmDBManager.setString(6, strComments);
    gmDBManager.setString(7, strInterComments);
    gmDBManager.setString(8, strPOID);
    gmDBManager.setString(9, strDHRStr);
    gmDBManager.setString(10, strAddlChrgId);
    gmDBManager.setString(11, strUserId);

    gmDBManager.execute();

    strReturn = gmDBManager.getString(12);
    gmDBManager.commit();

    return strReturn;
  } // end of saveVendorPayment

  //

  /**
   * reportDHRPayments - This method will
   * 
   * @param String strFrmDt
   * @param String strToDt
   * @return HashMap
   * @exception AppError
   **/
  public RowSetDynaClass reportDHRPayments(HashMap hmParam) throws AppError {
    RowSetDynaClass resultSet = null;
    String strVenId = "";
    String strFrmDt = "";
    String strToDt = "";
    String strPOId = "";
    String strType = "";
    String strInvoiceId = "";
    String strPostBy = "";
    String strCompDFmt = getCompDateFmt();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      strVenId = GmCommonClass.parseZero((String) hmParam.get("VENDID"));
      strFrmDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
      strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
      strPOId = GmCommonClass.parseNull((String) hmParam.get("POID"));
      strType = GmCommonClass.parseZero((String) hmParam.get("POTYPE"));
      strInvoiceId = GmCommonClass.parseNull((String) hmParam.get("INVID"));
      strPostBy = GmCommonClass.parseZero((String) hmParam.get("POSTBY"));

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT	GET_VENDOR_SH_NAME(T406.C301_VENDOR_ID) VNAME, C406_INVOICE_ID INVID, ");
      sbQuery.append(" C406_ENTERED_DT ENTDT, C406_PAYMENT_DATE PAYDT, ");
      sbQuery.append(" C406_COMMENTS COMMENTS, GET_USER_SH_NAME(C406_CREATED_BY) UNAME, ");
      sbQuery.append(" T408.C408_DHR_ID ID, T408.C205_PART_NUMBER_ID PNUM ");
      sbQuery
          .append(" ,decode(t408.c901_type, 80191 , t402.c402_split_cost ,decode(t402.c402_rm_inv_fl, 'Y', t402.c402_cost_price - t402.c402_split_cost , t402.c402_cost_price))");
      sbQuery
          .append("  * T408.C408_QTY_ON_SHELF /(TO_NUMBER (NVL (SUBSTR (GET_WO_UOM_TYPE_QTY(T402.C402_WORK_ORDER_ID), INSTR (GET_WO_UOM_TYPE_QTY(T402.C402_WORK_ORDER_ID), '/') + 1), 1))) PAYAMT ");
      sbQuery.append(" ,get_code_name(T401.C401_TYPE) POTYPE ");
      sbQuery
          .append(" FROM T406_PAYMENT T406, T408_DHR T408, T402_WORK_ORDER T402, T401_PURCHASE_ORDER T401");
      sbQuery.append(" WHERE T406.C406_PAYMENT_ID = T408.C406_PAYMENT_ID (+)");
      sbQuery.append(" AND T408.C402_WORK_ORDER_ID = T402.C402_WORK_ORDER_ID ");
      sbQuery.append(" AND T401.C401_PURCHASE_ORD_ID = T402.C401_PURCHASE_ORD_ID ");
      if (!strFrmDt.equals("") && !strToDt.equals("")) {
        sbQuery.append(" AND C406_PAYMENT_DATE BETWEEN ");
        sbQuery.append(" to_date('");
        sbQuery.append(strFrmDt);
        sbQuery.append("','" + strCompDFmt + "')) AND to_date('");
        sbQuery.append(strToDt);
        sbQuery.append("','" + strCompDFmt + "'");
      }
      if (!strVenId.equals("0")) {
        sbQuery.append(" AND T406.C301_VENDOR_ID = ");
        sbQuery.append(strVenId);
      }
      if (!strPOId.equals("")) {
        sbQuery.append(" AND T402.C401_PURCHASE_ORD_ID = '");
        sbQuery.append(strPOId);
        sbQuery.append("'");
      }
      if (!strType.equals("0")) {
        sbQuery.append(" AND T401.C401_TYPE = ");
        sbQuery.append(strType);
      }
      if (!strInvoiceId.equals("")) {
        sbQuery.append(" AND C406_INVOICE_ID = '");
        sbQuery.append(strInvoiceId);
        sbQuery.append("'");
      }
      if (!strPostBy.equals("0")) {
        sbQuery.append(" AND C406_CREATED_BY = ");
        sbQuery.append(strPostBy);
      }

      sbQuery.append(" ORDER BY C406_INVOICE_ID ");

      log.debug("QUERY For reportDHRPayments:" + sbQuery.toString());

      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogonBean:getEmployeeList", "Exception is:" + e);
    }
    return resultSet;
  } // End of reportDHRPayments

  /**
   * getInvoiceList - This method will fetch the invoice details for given PO
   * 
   * @param HashMap hmParam
   * @return RowSetDynaClass
   * @exception AppError
   **/
  public RowSetDynaClass getInvoiceList(HashMap hmParam) throws AppError {
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strPONumber = GmCommonClass.parseNull((String) hmParam.get("HPONUMBER"));

    gmDBManager.setPrepareString("gm_pkg_ac_payment.gm_ac_fch_invoice_list", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.setString(1, strPONumber);

    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdResult;
  }

  /**
   * getInvoiceDtls - This method will fetch the invoice details for given PaymentId
   * 
   * @param String paymentId
   * @return HashMap
   * @exception AppError
   **/
  public HashMap getInvoiceDtls(String paymentId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_ac_payment.gm_ac_fch_invoice_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.setInt(1, Integer.parseInt(paymentId));
    gmDBManager.execute();

    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * getInvoiceAddlChrgs - This method will fetch the additional charges details for given PaymentId
   * 
   * @param String paymentId
   * @return RowSetDynaClass
   * @exception AppError
   **/
  public RowSetDynaClass getInvoiceAddlChrgs(String paymentId) throws AppError {
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_ac_payment.gm_ac_fch_addl_charge_dtls", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.setInt(1, Integer.parseInt(paymentId));

    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdResult;
  }


  public RowSetDynaClass fetchAPPostingReport(HashMap hmParam) throws AppError {

    RowSetDynaClass rdAPPostingRpt = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBatchNumber = GmCommonClass.parseNull((String) hmParam.get("BATCHNUMBER"));
    String strBatchDateFrom = GmCommonClass.parseNull((String) hmParam.get("BATCHDATEFROM"));
    String strBatchDateTo = GmCommonClass.parseNull((String) hmParam.get("BATCHDATETO"));

    log.debug(" values in hmParam " + hmParam);
    if (strBatchNumber.equals(""))
      strBatchNumber = "0";
    gmDBManager.setPrepareString("gm_pkg_ac_payment.gm_ac_fch_ap_posting", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

    gmDBManager.setInt(1, Integer.parseInt(strBatchNumber));
    gmDBManager.setString(2, strBatchDateFrom);
    gmDBManager.setString(3, strBatchDateTo);
    gmDBManager.execute();
    rdAPPostingRpt = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));

    gmDBManager.close();
    return rdAPPostingRpt;

  }


  public RowSetDynaClass fetchAPPostingDetail(HashMap hmParam) throws AppError {

    RowSetDynaClass rdAPPostingDtls = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBatchNumber = GmCommonClass.parseNull((String) hmParam.get("BATCHNUMBER"));
    String strBatchDateFrom = GmCommonClass.parseNull((String) hmParam.get("BATCHDATEFROM"));
    String strBatchDateTo = GmCommonClass.parseNull((String) hmParam.get("BATCHDATETO"));
    String strRuleID = GmCommonClass.parseNull((String) hmParam.get("HRULEID"));
    String strGLAccountID = GmCommonClass.parseNull((String) hmParam.get("HACCOUNTID"));
    log.debug(" strRuleID in hmParam " + strRuleID);
    log.debug(" strGLAccountID in hmParam " + strGLAccountID);
    log.debug(" values in hmParam " + hmParam);
    if (strBatchNumber.equals(""))
      strBatchNumber = "0";
    gmDBManager.setPrepareString("gm_pkg_ac_payment.gm_ac_fch_ap_dtls", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.setInt(1, Integer.parseInt(strBatchNumber));
    gmDBManager.setString(2, strBatchDateFrom);
    gmDBManager.setString(3, strBatchDateTo);
    gmDBManager.setString(4, strRuleID);
    gmDBManager.setString(5, strGLAccountID);

    gmDBManager.execute();
    rdAPPostingDtls = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(6));
    gmDBManager.close();
    return rdAPPostingDtls;

  }

  public HashMap fetchPaymentDtls(HashMap hmParam) throws AppError {

    ArrayList alPOList = new ArrayList();
    ArrayList alInvcList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();

    String strVendorID = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
    String strPONumber = GmCommonClass.parseNull((String) hmParam.get("PONUMBER"));
    String strInitiatedBy = GmCommonClass.parseNull((String) hmParam.get("INVCINITIATEDBY"));
    String strInitiatedDateFrom =
        GmCommonClass.parseNull((String) hmParam.get("INVCINITIATEDDATEFROM"));
    String strInitiatedDateTo =
        GmCommonClass.parseNull((String) hmParam.get("INVCINITIATEDDATETO"));
    String strInvcStatus = GmCommonClass.parseNull((String) hmParam.get("INVCSTATUS"));
    log.debug(" strInitiatedBy.........................." + strInitiatedBy);
    log.debug(" strInitiatedDate.........................." + strInitiatedDateFrom);
    gmDBManager.setPrepareString("gm_pkg_ac_payment.gm_ac_fch_payment_dtls", 8);

    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);

    gmDBManager.setInt(1, Integer.parseInt(strVendorID));
    gmDBManager.setString(2, strPONumber);
    gmDBManager.setString(3, strInitiatedBy);
    gmDBManager.setString(4, strInitiatedDateFrom);
    gmDBManager.setString(5, strInitiatedDateTo);
    gmDBManager.setInt(6, Integer.parseInt(strInvcStatus));

    gmDBManager.execute();

    // hmBatchDetails = GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet)
    // gmDBManager.getObject(2)));
    // alBatchList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)
    // gmDBManager.getObject(3)));
    alPOList =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(7)));
    alInvcList =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(8)));
    gmDBManager.close();

    // hmReturn.put("HMBATCHDETAIL", hmBatchDetails);
    hmReturn.put("RDPOLIST", alPOList);
    hmReturn.put("RDINVCLIST", alInvcList);
    return hmReturn;

  }

  public void saveReleasedPaymentDtls(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strInput = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strreleasefl = GmCommonClass.parseNull((String) hmParam.get("RELEASE_FL"));

    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_ac_payment.gm_ac_sav_release_payment", 3);

    gmDBManager.setString(1, strInput);
    gmDBManager.setString(2, strUserId);
    gmDBManager.setString(3, strreleasefl);

    gmDBManager.execute();

    gmDBManager.commit();

  }

  public RowSetDynaClass fetchInvoiceDHRDtls(HashMap hmParam) throws AppError {

    RowSetDynaClass rdInvoiceDetails = null;

    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("INVOICETYPE"));
    String strStatusval = GmCommonClass.parseNull((String) hmParam.get("INVOICEOPER"));
    String strOperSign = GmCommonClass.parseNull((String) hmParam.get("OPERATORSIGN"));
    String strInvoiceDateFrom = GmCommonClass.parseNull((String) hmParam.get("INVOICEDATEFROM"));
    String strInvoiceDateTo = GmCommonClass.parseNull((String) hmParam.get("INVOICEDATETO"));
    String strDownloadDateFrom = GmCommonClass.parseNull((String) hmParam.get("DOWNLOADDATEFROM"));
    String strDownloadDateTo = GmCommonClass.parseNull((String) hmParam.get("DOWNLOADDATETO"));
    String strPOId = GmCommonClass.parseNull((String) hmParam.get("POID"));

    String strDateFmt = getGmDataStoreVO().getCmpdfmt();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT  t406.c406_payment_id PAYMENTID, t301.c301_vendor_name vendnm ");
    sbQuery
        .append(" ,get_currency_conversion (t301.c301_currency, get_comp_curr(t301.c1900_company_id), NVL(t406.c406_download_dt, t406.c406_entered_dt), NVL(gm_pkg_ac_payment.get_dhr_amount (t406.c406_payment_id),0))*1 dhr_amt, t406.c406_invoice_id INVOICEID ");
    sbQuery.append(" ,t406.c406_invoice_dt invdt ");
    sbQuery
        .append(" ,get_currency_conversion (t301.c301_currency, get_comp_curr(T301.c1900_company_id), NVL(t406.c406_download_dt, t406.c406_entered_dt), NVL(gm_pkg_ac_payment.get_invc_addl_chrg (t406.c406_payment_id), 0))*1 addl_charge ");
    sbQuery.append(" ,gm_pkg_ac_payment.get_invoice_status (t406.c406_status_fl) invc_status ");
    sbQuery
        .append(" ,get_currency_conversion (t301.c301_currency, get_comp_curr(T301.c1900_company_id),  NVL(t406.c406_download_dt, t406.c406_entered_dt), NVL(t406.c406_payment_amount, 0))*1 invoice_amt, T406.C401_PURCHASE_ORD_ID POID ");
    sbQuery.append(" FROM    t406_payment t406, t301_vendor t301");
    sbQuery
        .append(" WHERE  t406.c301_vendor_id = t301.c301_vendor_id AND t406.c406_void_fl IS NULL ");
    sbQuery.append(" and t406.c406_status_fl is not null ");
    sbQuery.append(" AND  t406.C1900_COMPANY_ID = " + getCompId());
    sbQuery.append(" AND  t406.C1900_COMPANY_ID = t301.C1900_COMPANY_ID");
    if (!strInvoiceDateFrom.equals("") && !strInvoiceDateTo.equals("")) {
      sbQuery.append(" AND t406.c406_invoice_dt BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strInvoiceDateFrom);
      sbQuery.append("','" + strDateFmt + "') AND to_date('");
      sbQuery.append(strInvoiceDateTo);
      sbQuery.append("','" + strDateFmt + "')");
    }

    if (!strDownloadDateFrom.equals("") && !strDownloadDateTo.equals("")) {
      sbQuery.append(" AND t406.c406_download_dt BETWEEN ");
      sbQuery.append(" to_date('");
      sbQuery.append(strDownloadDateFrom);
      sbQuery.append("','" + strDateFmt + "') AND to_date('");
      sbQuery.append(strDownloadDateTo);
      sbQuery.append("','" + strDateFmt + "')");
    }

    if (!strVendorId.equals("0")) {
      sbQuery.append(" AND T406.C301_VENDOR_ID = ");
      sbQuery.append(strVendorId);
    }
    if (!strPOId.equals("0") && !strPOId.equals("")) {
      sbQuery.append(" AND T406.C401_PURCHASE_ORD_ID = ");
      sbQuery.append("'" + strPOId + "'");
    }

    if (!strStatus.equals("-1") && !strStatus.equals("")) {

      if (strStatusval.equals("") || strStatusval.equals("0")) {
        strOperSign = "=";
      }

      sbQuery.append(" AND t406.C406_STATUS_FL  " + strOperSign);
      sbQuery.append(strStatus);
    }



    sbQuery.append(" ORDER BY C406_INVOICE_ID ");

    log.debug("QUERY For fetchInvoiceDHRDtls:" + sbQuery.toString());

    // rdInvoiceDetails = dbCon.QueryDisplayTagRecordset(sbQuery.toString());
    rdInvoiceDetails = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return rdInvoiceDetails;
  }

}// end of class GmARBean
