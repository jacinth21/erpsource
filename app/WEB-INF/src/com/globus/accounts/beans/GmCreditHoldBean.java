package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.sales.beans.GmSalesMappingBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCreditHoldBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  // this will be removed all place changed with Data Store VO constructor

//  public GmCreditHoldBean() {
//    super(GmCommonClass.getDefaultGmDataStoreVO());
//  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmCreditHoldBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchAcctCreditHold - This method will fetch Account Credit Hold details
   * 
   * @param strAccId the Account Id
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchAcctCreditHold(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    String strCreditTypes = GmCommonClass.parseNull((String) hmParam.get("CRDITTYPESTR"));
    log.debug("the value inside strCreditTypes ** in fetchAcctCreditHold *** " + strCreditTypes);
    strCreditTypes =
        strCreditTypes.equals("") ? GmCommonClass.getRuleValue("ALL_TYPE", "CREDIT_HOLD_TYPE")
            : strCreditTypes;

    gmDBManager.setPrepareString("gm_pkg_ar_credit_hold.gm_fch_acct_credit_hold", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID")));
    gmDBManager.setString(2, strCreditTypes);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    log.debug("alResult **** " + alResult.size());
    return alResult;
  }

  /**
   * fetchCreditHoldDtl - This method will fetch Credit Hold detail and sub type mapping details.
   * The Functions that will be allowed for the Credit Hold Type.
   * 
   * @param hmParam
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCreditHoldDtl(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ar_credit_hold.gm_fch_credit_hold_dtl", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("CREDITHOLDTYPE")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("SUBTYPE")));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    log.debug("alResult **** " + alResult);
    return alResult;
  }

  /**
   * fetchAcctPaymentInfo - This method will fetch Account payment info
   * 
   * @param strAccId the Account Id
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchAcctLastPaymentInfo(String strAccId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ar_credit_hold.gm_fch_acct_last_payment_info", 2);
    gmDBManager.setString(1, strAccId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    log.debug("hmReturn hmReturn hmReturn in bean **** " + hmReturn);
    return hmReturn;
  }


  /**
   * This method is used to save credit hold details during account set up.
   * 
   * @see last modified PMT-8227
   * @param hmParams the hm params
   * @throws AppError the app error
   * @throws Exception the exception
   */
  public void saveAcctCreditHold(HashMap hmParams) throws AppError, Exception {
    GmCustSalesSetupBean gmCustSalesSetupBean = new GmCustSalesSetupBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager();
    log.debug("the parameters before saving the attributes **** " + hmParams);
    String strAcctId = GmCommonClass.parseNull((String) hmParams.get("ACCOUNTID"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strAccAttInputStr = GmCommonClass.parseNull((String) hmParams.get("CREDITATTRSTRING"));
    String strComments = GmCommonClass.parseNull((String) hmParams.get("TXT_LOGREASON"));
    gmCustSalesSetupBean.saveAccountAttribute(gmDBManager, strAcctId, strUserId, strAccAttInputStr,
        "");
    // to sync the credit hold parameter
    gmCustSalesSetupBean
        .saveSyncAccountAttr(gmDBManager, strAcctId, "CREDIT_HOLD_PARAM", strUserId);
    if (!strComments.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strAcctId, strComments, strUserId, "400915");
    }
    gmDBManager.commit();
  }

  /**
   * sendCreditHoldNotifyEmail -This method will send Add/Remove Account to Credit Hold and Place
   * Order for Account which is set as Credit Hold Type1.
   * 
   * @param hmParam
   * @return ArrayList
   * @throws AppError
   */
  public void sendCreditHoldNotifyEmail(HashMap hmParam) throws AppError {

    ArrayList alResult = new ArrayList();

    String strTemplate = "";
    String strEmailType = GmCommonClass.parseNull((String) hmParam.get("EMAILTYPE"));
    String strAccountId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));

    strTemplate =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strEmailType, "CREDIT_HOLD_EMAIL"));

    log.debug("strTemplate **** " + strTemplate);

    if (!strTemplate.equals("")) {
      // Getting Account & Credit Hold info - AccountId, Account Name & Created by
      alResult = fetchAcctCreditHold(hmParam);
      if (alResult.size() > 0) {
        hmParam.putAll((HashMap) alResult.get(0));
      }

      hmParam = populateCreditHoldEmailData(hmParam);
      try {
        GmCommonClass.sendMail(hmParam);
      } catch (Exception ex) {
        log.error("Exception in sending" + ex.getMessage());
        throw new AppError(ex);
      }

    }
  }

  /**
   * sendCreditHoldWeeklyEmail - This method will send weekly Credit Hold reminder. this will invoke
   * by job. It will fetch all accounts which are credit hold and separate records for each rep and
   * call email method.
   * 
   * @throws AppError
   */
  public void sendCreditHoldWeeklyEmail(HashMap hmParam) throws AppError {
    ArrayList alAcctCrdHld = new ArrayList();
    ArrayList alTemp = new ArrayList();
    log.debug("the value inside hmParam sendCreditHoldWeeklyEmail **** testing 1 ***" + hmParam);

    HashMap hmTempLoop = new HashMap();
    HashMap hmLoop = null;

    String strCurrRepId = "";
    String strNextRepId = "";
    String strDistname = "";
    String strCreditType = GmCommonClass.parseNull((String) hmParam.get("CRDITTYPESTR"));
    // fetch all accounts which are set as credit hold.

    alAcctCrdHld = fetchAcctCreditHold(hmParam);
    int intDataSize = alAcctCrdHld.size();

    for (int i = 0; i < intDataSize; i++) {
      hmLoop = new HashMap();
      hmLoop = (HashMap) alAcctCrdHld.get(i);
      strCurrRepId = GmCommonClass.parseNull((String) hmLoop.get("REPID"));
      strDistname = GmCommonClass.parseNull((String) hmLoop.get("DISTNAME"));
      // getting next record repid upto there is next record.
      if (i < intDataSize - 1) {
        hmTempLoop = (HashMap) alAcctCrdHld.get(i + 1);
        strNextRepId = GmCommonClass.parseNull((String) hmTempLoop.get("REPID"));
      }
      alTemp.add(hmLoop);

      // If next record repid is differ or loop reach last then call email method for current rep's
      // record list

      if (!strCurrRepId.equals(strNextRepId) || i == intDataSize - 1) {

        log.debug("Email data list 1" + alTemp);
        hmParam.put("REPID", strCurrRepId);
        hmParam.put("EMAILTYPE", "WEEKRIMD");
        hmParam.put("DISTNAME", strDistname);

        hmParam.put("CREDITTYPE", strCreditType);
        hmParam.put("DATALIST", alTemp);


        hmParam = populateCreditHoldEmailData(hmParam);

        try {
          GmCommonClass.sendMail(hmParam);
        } catch (Exception ex) {
          log.error("Exception in sending" + ex.getMessage());
          throw new AppError(ex);
        }

        alTemp = new ArrayList();
      }
    }
  }

  /**
   * puplateCreditHoldEmailData - This method will populate email data like CC email list,TO email
   * and addition values from rule table.
   * 
   * @param hmParam
   * @return HashMap
   * @throws AppError
   */
  public HashMap populateCreditHoldEmailData(HashMap hmParam) throws AppError {


	log.debug("hmParam 2 testing 123==================" + hmParam);
    HashMap hmEmailParam = new HashMap();
    HashMap hmSalesHierEmail = new HashMap();

    String strCCEMails = "";
    String strAccountId = "";
    String strCollectorEmail = "";
    String strCollectorRuleValue = "";
    String strCollectorName = "";
    String strContactMess =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("AR_INFO", "CREDIT_HOLD_MSG_ADD"));
    String strEmailType = GmCommonClass.parseNull((String) hmParam.get("EMAILTYPE"));
    String strCreditType = GmCommonClass.parseNull((String) hmParam.get("CREDITTYPE"));
    String strCurrency = GmCommonClass.parseNull((String) hmParam.get("ACCCURRENCY"));//PMT-53060
    GmSalesMappingBean gmSalesMappingBean = new GmSalesMappingBean(getGmDataStoreVO());
    hmSalesHierEmail = gmSalesMappingBean.fetchSalesHierarchyEmails(hmParam);
    strCollectorRuleValue =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("CONTACT_TEAM", "WEEK_REMD"));
    // Append CC emails
    strCCEMails +=
        "," + GmCommonClass.parseNull((String) hmSalesHierEmail.get("VPEMAIL")) + ","
            + GmCommonClass.parseNull((String) hmSalesHierEmail.get("ADEMAIL"));
    // Removing prefix ","
    strCCEMails = strCCEMails.substring(1);
    strCollectorEmail = GmCommonClass.parseNull((String) hmSalesHierEmail.get("COLLTREMAIL"));
    strCollectorName = GmCommonClass.parseNull((String) hmSalesHierEmail.get("COLLTRNAME"));

    if (!strCollectorEmail.equals("")) {
      strCCEMails = strCCEMails + ',' + strCollectorEmail;
    }
    if (strCollectorName.equals("")) {
      strCollectorName = strCollectorRuleValue;
    }

    strContactMess = strContactMess.replaceAll("CONTACTTEAM", strCollectorName);
    // Adding associate Emails in CC list. It is not applicable for weekly reminder email
    if (!strEmailType.equals("WEEKRIMD")) {
      strAccountId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
      // Getting Associate Sales Rep info with Email
      strCCEMails += fetchAssocRepEmails(strAccountId);

    }


    hmParam.put("WEEK_MESS",
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCreditType, "WEEK_REMD")));

    hmEmailParam
        .put("TOEMAILS", GmCommonClass.parseNull((String) hmSalesHierEmail.get("REPEMAIL")));
    hmEmailParam.put("CCEMAILS", strCCEMails);

    // Getting values from rule table
    hmEmailParam.put("TEMPLATE",
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strEmailType, "CREDIT_HOLD_EMAIL")));

    hmParam.put("ADDIT_MSG",
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCreditType, "CREDIT_HOLD_MSG_ADD")));
    hmParam.put("AR_MSG", strContactMess);

    hmParam.put("APPLNCURRFMT", GmCommonClass.getRuleValue("CURRFMT", "CURRFMT"));
    hmParam.put("APPLNCURRSIGN", GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));
    hmParam.put("ACCCURRENCY",strCurrency);//PMT-53060


    hmEmailParam.put("HMEMAILDATA", hmParam);
    return populateEmailDataFromProperty(hmEmailParam);

  }

  /**
   * fetchAssocRepEmails - This method will fetch associate rep info for the Account and append all
   * emails with comma and returns string
   * 
   * @param String Account Id
   * @return String - Associate EmailIds
   * @throws AppError
   */
  private String fetchAssocRepEmails(String strAccountId) throws AppError {
    String strAssocEmails = "";
    ArrayList alAssocRepEmail = new ArrayList();
    GmSalesMappingBean gmSalesMappingBean = new GmSalesMappingBean(getGmDataStoreVO());

    alAssocRepEmail = gmSalesMappingBean.getAssocRepList(strAccountId);
    // Append Associate rep emails in CC
    for (Object objAssocRepEmail : alAssocRepEmail) {
      HashMap hmAssocRepEmail = (HashMap) objAssocRepEmail;
      strAssocEmails +=
          "," + GmCommonClass.parseNull((String) hmAssocRepEmail.get("ASSOCREPEMAIL"));
    }
    return strAssocEmails;
  }

  /**
   * populateEmailDataFromProperty - This method will send email data from email template properties
   * 
   * @param hmParam
   * @return HashMap
   * @throws AppError
   */
  public HashMap populateEmailDataFromProperty(HashMap hmParam) throws AppError {
    HashMap hmEmailPro = new HashMap();
    HashMap hmEmailData = new HashMap();

    GmCommonClass gmCommonClass = new GmCommonClass();

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);

    String strTemplate = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE"));
    log.debug("hmParam 1=================" + hmParam);
    // Getting values from Email Property file
    String strSubject =
        gmResourceBundleBean.getProperty(strTemplate + "." + GmEmailProperties.SUBJECT);
    String strMimeType =
        gmResourceBundleBean.getProperty(strTemplate + "." + GmEmailProperties.MIME_TYPE);

    String strMessage =
        gmResourceBundleBean.getProperty(strTemplate + "." + GmEmailProperties.MESSAGE);
    String strFrom = gmResourceBundleBean.getProperty(strTemplate + "." + GmEmailProperties.FROM);
    String strMsgFooter =
        gmResourceBundleBean.getProperty(strTemplate + "." + GmEmailProperties.MESSAGE_FOOTER);
    String strCCEmailPro =
        gmResourceBundleBean.getProperty(strTemplate + "." + GmEmailProperties.CC);
    String strToEMails = GmCommonClass.parseNull((String) hmParam.get("TOEMAILS"));
    String strCCEMails = GmCommonClass.parseNull((String) hmParam.get("CCEMAILS"));

    hmEmailData = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("HMEMAILDATA"));
    hmEmailData.put("MESSAGE", strMessage);
    hmEmailData.put("FOOTER", strMsgFooter);
    hmEmailData.put("VMFILENAME", strTemplate + ".vm");

    log.debug("the value inside hmEmailData *** " + hmEmailData);
    // Replace dynamic values in subject
    strSubject =
        strSubject.replaceAll("#<ACCOUNTID>",
            GmCommonClass.parseNull((String) hmEmailData.get("ACCOUNTID")));
    strSubject =
        strSubject.replaceAll("#<ACCTNM>",
            GmCommonClass.parseNull((String) hmEmailData.get("ACCTNM")));

    strSubject =
        strSubject.replaceAll("#<DISTNAME>",
            GmCommonClass.parseNull((String) hmEmailData.get("DISTNAME")));


    strCCEMails += "," + strCCEmailPro;


    hmEmailPro.put("strFrom", strFrom);
    hmEmailPro.put("strTo", GmCommonClass.StringtoArray(strToEMails, ","));
    hmEmailPro.put("strCc", GmCommonClass.StringtoArray(strCCEMails, ","));
    hmEmailPro.put("strMimeType", strMimeType);
    hmEmailPro.put("strSubject", strSubject);

    strMessage = generateEmailBody(hmEmailData);
    log.debug(strMessage);
    hmEmailPro.put("strMessageDesc", strMessage);

    return hmEmailPro;
  }

  /**
   * generateEmailBody - This method will generate email body message by calling appropriate vmfile.
   * 
   * @param hmParam
   * @return String
   * @throws AppError
   */
  public String generateEmailBody(HashMap hmEmailData) throws AppError {
    String strFileName = GmCommonClass.parseNull((String) hmEmailData.get("VMFILENAME"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmEmailParam", hmEmailData);
    templateUtil.setTemplateSubDir("accounts/templates");
    templateUtil.setTemplateName(strFileName);
    return templateUtil.generateOutput();
  }

}
