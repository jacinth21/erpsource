package com.globus.accounts.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAPSageTransBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.


  public GmAPSageTransBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAPSageTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public void saveSageVendor(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strInput = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    log.debug(" values in hmParam " + hmParam);
    gmDBManager.setPrepareString("gm_pkg_ac_sage.gm_ac_sav_sage_vendor", 2);

    gmDBManager.setString(1, strInput);
    gmDBManager.setString(2, strUserId);

    gmDBManager.execute();

    gmDBManager.commit();

  }

}
