package com.globus.accounts.beans;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.valueobject.common.GmDataStoreVO;

import org.apache.log4j.Logger;

/**
 * GmGermanyInvoiceBean - This class is used to load Germany invoice(PMT-49549).
 */
public class GmGermanyInvoiceBean extends GmBean {
	
	public GmGermanyInvoiceBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 /**
	   * loadGermanyOrderItemSQL - This method is used to load Germany order details to show in invoice
	   * 
	   * @param String strOrdId
	   * @param String strAction
	   * @return String
	   */
	  public String loadGermanyOrderItemSQL(String strOrdId, String strAction) {

	    StringBuffer sbQuery = new StringBuffer();
	    String strApplDateFmt = getCompDateFmt();
	    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
	    GmResourceBundleBean gmResourceBundleBean =
	        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
	    GmResourceBundleBean gmResourceBundleBean1 =
	        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	    String strctrlSplitUp =
	        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DO.CTRLSPLITUP"));
	    String strShowPartUomFl =
	        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.SHOW_PART_UOM"));
	    String strCompanyId = getGmDataStoreVO().getCmpid();
	    String strUsedLotFl =
	            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.USED_LOT_FL"));
	    // Below Query is added in Print so the zero invoice order
	    // is not listed in the screen
	      if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
	        sbQuery.append("  SELECT * FROM ");
	        sbQuery.append(" ( ");
	        sbQuery
	            .append(" SELECT distinct(A.C205_PART_NUMBER_ID) ID, get_partdesc_by_company(A.C205_PART_NUMBER_ID) PDESC ,T205.C205_PRODUCT_FAMILY ,B.C501_ORDER_DATE DATEOFSALES,");
	        sbQuery.append(" B.C501_SURGERY_DATE SURG_DT,GET_ACCOUNT_NAME(B.c704_account_id) ACCTNAME,A.C501_ORDER_ID ORDERID ");

	        if (strctrlSplitUp.equals("YES")) { // For BBA the value will be YES, need to get the qty
	                                             // based on control number
	       	  if (strUsedLotFl.equals("YES")) {
	      		sbQuery
	            .append(" ,DECODE(B.C901_ORDER_TYPE,'2532',A.C502_ITEM_QTY,get_order_part_qty_invoice(A.C501_ORDER_ID,A.C205_PART_NUMBER_ID,A.C502_ITEM_PRICE ,A.c901_type,DECODE(A.C502_CONSTRUCT_FL,NULL,'N',A.C502_CONSTRUCT_FL),A.C502_Control_Number)) QTY ");
	      	  }else{
	          sbQuery
	              .append(" ,get_order_part_qty_invoice(A.C501_ORDER_ID,A.C205_PART_NUMBER_ID,A.C502_ITEM_PRICE");
	          sbQuery
	              .append(" ,A.c901_type,DECODE(A.C502_CONSTRUCT_FL,NULL,'N',A.C502_CONSTRUCT_FL),A.C502_Control_Number) QTY");
	      	  }
	          sbQuery
	              .append(" ,ROUND (NVL ( (get_order_part_qty_invoice (A.C501_ORDER_ID, A.C205_PART_NUMBER_ID, A.C502_ITEM_PRICE,");
	          sbQuery.append(" A.c901_type, DECODE (A.C502_CONSTRUCT_FL, NULL, 'N', A.C502_CONSTRUCT_FL), A.C502_Control_Number) * NVL (A.C502_ITEM_PRICE, 0) * NVL (a.c502_vat_rate, 0) / 100), 0), 2) tax_cost ");
	        } else {
	          sbQuery.append(" ,get_invoice_part_qty_ous(A.C501_ORDER_ID, A.C502_ITEM_ORDER_ID) QTY");
	          sbQuery
	              .append(", get_invoice_part_tax_ous (A.C501_ORDER_ID, A.C502_ITEM_ORDER_ID) tax_cost ");
	        }
	        sbQuery
	            .append(" ,NVL(GET_CUSTOM_PARTNUM(B.C704_ACCOUNT_ID,A.C205_PART_NUMBER_ID),'-9999') CUST_PNUM");
	        sbQuery
	            .append(" ,NVL(GET_CUSTOM_PARTDESC(B.C704_ACCOUNT_ID,A.C205_PART_NUMBER_ID),'-9999') CUST_PDESC,A.C502_ITEM_PRICE PRICE,");
	        sbQuery
	            .append(" NVL(A.C502_BASE_PRICE,0) ACTUALPRICE,A.C502_EXT_ITEM_PRICE EXTERNAL_ITEM_PRICE, B.C501_EXT_TOTAL_COST EXTERNAL_TOTAL_COST, ");
	      } else {

	        sbQuery
	            .append(" SELECT A.C205_PART_NUMBER_ID ID, get_partdesc_by_company(A.C205_PART_NUMBER_ID) PDESC, ");

	        sbQuery
	            .append(" A.C502_ITEM_QTY QTY, A.C502_ITEM_ORDER_ID ITEM_ORD_ID,");
	        if (strUsedLotFl.equals("YES")) {
	        	 sbQuery
	                .append(" decode(B.C901_ORDER_TYPE,'2532',NVL(t502b.c502b_usage_lot_num,''),NVL(A.C502_Control_Number,'')) CNUM,");
	          } else {
	            sbQuery.append("NVL(A.C502_Control_Number,'') CNUM,");
	          }
	        sbQuery
	            .append("  A.C502_ITEM_PRICE PRICE,NVL(A.C502_BASE_PRICE,0) ACTUALPRICE,A.C502_EXT_ITEM_PRICE EXTERNAL_ITEM_PRICE, B.C501_EXT_TOTAL_COST EXTERNAL_TOTAL_COST,");
	        sbQuery
	            .append(" NVL(A.c502_tax_amt, ROUND (NVL ( (A.C502_ITEM_QTY * NVL (A.C502_ITEM_PRICE, 0) * NVL (A.c502_vat_rate, 0) / 100), 0), 2)) tax_cost,A.C501_ORDER_ID ORDERID,");
	        sbQuery.append(" B.C501_ORDER_DATE DATEOFSALES,B.C501_SURGERY_DATE SURG_DT,GET_ACCOUNT_NAME(B.c704_account_id) ACCTNAME,");
	      }
	      sbQuery
	          .append(" A.c901_type TYPE, A.C502_CONSTRUCT_FL CONFL, get_part_attr_value_by_comp (A.C205_PART_NUMBER_ID,'200001', B.C1900_COMPANY_ID) NTFTCODE, ");
	      sbQuery.append(" B.C501_SHIP_COST SCOST, B.C501_ORDER_DATE ODT,NVL(a.c502_vat_rate,0) VAT , NVL(a.c502_igst_rate,0) IGST, NVL(a.c502_sgst_rate,0) SGST, NVL(a.c502_cgst_rate,0) CGST , a.c502_hsn_code HSNCODE");//to get the gst and HSN code values for India
	      if (strctrlSplitUp.equals("YES")) { // For BBA the value will be YES, need to get the qty
	        // based on control number
	    	  if (strUsedLotFl.equals("YES")) {
	          	sbQuery
	                  .append(", decode(B.C901_ORDER_TYPE,'2532',NVL(t502b.c502b_usage_lot_num,''),NVL(A.C502_Control_Number,'')) ControlNum");
	            sbQuery.append(",NVL(A.C502_Control_Number,'') CNUM");
	            } else {
	        sbQuery.append(",NVL(A.C502_Control_Number,'') ControlNum");
	        sbQuery.append(",NVL(A.C502_Control_Number,'') CNUM");
	            }
	        sbQuery
	            .append(",  gm_pkg_pd_partnumber.get_part_size(A.C205_PART_NUMBER_ID,A.C502_CONTROL_NUMBER) partsize ");
	      }
	      sbQuery.append(",  NVL(B.C501_Shipping_Date,'') shipdate ");
	      sbQuery
	          .append(",  NVL(c502_unit_price,0) unitprice, NVL((c502_unit_price_adj_value * -1),0) adjval, DECODE(C901_UNIT_PRICE_ADJ_CODE, NULL, 'N/A', GET_CODE_NAME(c901_unit_price_adj_code)) adjcode ");
	      sbQuery.append(",  DECODE(NVL(C502_UNIT_PRICE_ADJ_VALUE,0),'0','','Y') ADJFLAG ");
	      sbQuery.append(",  GET_CODE_NAME_alt(c901_unit_price_adj_code) ADJCODEALT ");
	      sbQuery.append(",  NVL(A.C502_ITEM_PRICE,0)-NVL(C502_UNIT_PRICE,0) ADJEA ");
	      // to set the Unit of Measure values
	      if (strShowPartUomFl.equalsIgnoreCase("YES")) {
	        sbQuery
	            .append(" , get_rule_value_by_company(get_partnum_uom(A.C205_PART_NUMBER_ID), 'INVOICE_PART_UOM', B.C1900_COMPANY_ID) uom ");
	      }
	      sbQuery.append(" FROM T502_ITEM_ORDER A, T501_ORDER B , T205_PART_NUMBER T205 ");
	      if (strUsedLotFl.equals("YES")) {
	          sbQuery.append(" ,t502b_item_order_usage t502b, T500_Order_Info t500 ");
	        }
	      sbQuery.append(" WHERE A.C501_ORDER_ID = '");
	      sbQuery.append(strOrdId);
	      sbQuery.append("' AND A.C502_VOID_FL IS NULL AND A.C502_DELETE_FL IS NULL ");
	      sbQuery.append(" AND A.C501_ORDER_ID = B.C501_ORDER_ID ");
	      sbQuery.append(" AND A.C205_PART_NUMBER_ID =  T205.C205_PART_NUMBER_ID ");
	      sbQuery.append(" AND B.C1900_COMPANY_ID = '" + strCompanyId + "' ");
	      if (strUsedLotFl.equals("YES")) {
	    	  sbQuery.append(" AND A.C501_Order_Id         = t500.c501_order_id(+) ");
	    	  sbQuery.append(" AND t500.C501_Order_Id         = t502b.c501_order_id(+) ");
	    	  sbQuery.append(" AND A.c500_order_info_id    = t500.c500_order_info_id(+) ");
	          sbQuery.append(" AND t500.c500_order_info_id = t502b.c500_order_info_id(+) ");
	    	  sbQuery.append(" AND t502b.C502B_VOID_FL (+)   IS NULL ");
	    	  sbQuery.append(" AND B.C501_VOID_FL      IS NULL ");
	    	  sbQuery.append(" AND A.C502_VOID_FL      IS NULL ");
	    	  sbQuery.append(" AND t500.C500_VOID_FL (+)     IS NULL ");
	        }

	      sbQuery.append(" ORDER BY A.C205_PART_NUMBER_ID ");

	      if (strAction.equals("Print") || strAction.equals("PrintVoid")) {
	        sbQuery.append(" )");
	        sbQuery.append(" WHERE  QTY != 0 AND  ( c205_product_family != 26240096 or price != 0)  ORDER BY  CUST_PNUM,CUST_PDESC,ID ");
	        if (strctrlSplitUp.equals("YES")) {
	          sbQuery.append(" , ControlNum ");
	          sbQuery.append(" , CNUM ");
	        }

	      }

	    log.debug(" Query formed for loadGermanyOrderItemSQL is " + sbQuery.toString());
	    return (sbQuery.toString());
	  }

}
