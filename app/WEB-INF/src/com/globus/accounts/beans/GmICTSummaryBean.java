package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmConsignmentPackSlipInterface;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.requests.beans.GmRequestReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmICTSummaryBean extends GmBean
	{
	    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	    
	public GmICTSummaryBean(GmDataStoreVO gmDataStore) {
		    super(gmDataStore);
		  }
	
	public GmICTSummaryBean() {
			super(GmCommonClass.getDefaultGmDataStoreVO());
		   }
	
	    /**
	      * reportDynamicData - This Method is used to report all the form data based on the filter conditions
	      * @param String strStudyListId, String strSiteId  
	      * @return HashMap
	      * @exception AppError
	    **/     
	      
	      public RowSetDynaClass fetchICTSummary(String StrConsgnId) throws AppError
	      {
	    	  RowSetDynaClass rdResult = null;
	           	  GmDBManager gmDBManager = new GmDBManager();
	              gmDBManager.setPrepareString("gm_pkg_op_ict.gm_op_fch_ict_summary", 2);
	              gmDBManager.setString(1, GmCommonClass.parseNull(StrConsgnId));
	              gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	              gmDBManager.execute();
	              rdResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(2));
	              gmDBManager.close();      
	              log.debug("Exit");                   
	              return rdResult;
	      } 
	      
	      /**
	       * This method fetches the COnsignment and Item consignment details that'll be used in print page (jasper bnased) 
	       * @author jkumar
	       * @date Dec 12, 2008	
	       * @param
	       */
	      public HashMap fetchConsignmentDetails(String strConsignId,String strLUserId ) throws AppError
	      {
	    	  HashMap hmRefDetails = new HashMap();
	    	  HashMap hmParam = new HashMap();	    	  
	    	  hmParam.put("CONSIGNID",strConsignId);
	    	  hmParam.put("USERID",strLUserId);
	    	  hmRefDetails=fetchConsignmentDetails(hmParam);	
	    	  return hmRefDetails;
	      }
	      /**
	       * This method fetches the COnsignment and Item consignment details that'll be used in print page (jasper bnased) 
	       * This method will call from shipment job and screen 
	       * @author aprasath
	       * @date Jan 24, 2018	
	       * @param
	       */
	      
	      public HashMap fetchConsignmentDetails(HashMap hmParam) throws AppError
	      {
	    	  String strConsignId = GmCommonClass.parseNull((String)hmParam.get("CONSIGNID"));
	    	  String strLUserId =GmCommonClass.parseNull((String)hmParam.get("USERID"));
	    	  
	    	  GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(getGmDataStoreVO());
	    	  GmOperationsBean gmOperationsBean = new GmOperationsBean(getGmDataStoreVO());
	    	  GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO()); 
	    	  
	    	  HashMap hmRefDetails = new HashMap();
	    	  HashMap hmSetMaster = new HashMap();
	    	  HashMap hmConsignDetails = new HashMap();
	    	  
	    	  String strRequestId = GmCommonClass.parseNull(gmRequestReportBean.fetchRequestId(strConsignId));
	    	  hmRefDetails.put("REQUESTID", strRequestId);
	    	
	    	 // hmConsignDetails = gmCustomerBean.loadConsignDetails(strConsignId,strLUserId);
	    	  
	    	  
	    	  GmConsignmentPackSlipInterface packSlipConsign =     	  
	    			  (GmConsignmentPackSlipInterface) GmCommonClass.getSpringBeanClass("xml/Consignment_Details_Beans.xml",
	    		              getGmDataStoreVO().getCmpid());    // packslip for Italy in PMT-42950
	    		hmConsignDetails = packSlipConsign.loadConsignDetails(strConsignId,strLUserId,getGmDataStoreVO());//PMT-57366  Loan detail sheet displaying Hospital name in English

	    	  
	    	  
	    	  
	    	  hmRefDetails.put("HMCONSIGNDETAILS", hmConsignDetails);
	    	  
	    	  String strType=GmCommonClass.parseNull((String) hmConsignDetails.get("TYPE"));
	    	  // 40057 - ICT ,106703: FG-ST ,106704: BL-ST
	    	  if(strType.equals("40057") || strType.equals("106703") || strType.equals("106704") ){
	    		 hmSetMaster = gmOperationsBean.loadSavedSetMasterDetails(strConsignId);
		    	 hmRefDetails.put("HMCONSIGNCHILDDETAILS", hmSetMaster); 
	    	  }else {
	    		 hmSetMaster = gmOperationsBean.loadSavedSetMaster(hmParam);
		    	 hmRefDetails.put("HMCONSIGNCHILDDETAILS", hmSetMaster);
	    	  }  
	    	  return hmRefDetails;
	    	    
	      }

	      
	      /* This method returns GOP OUS Consignment Txn details.
	       * @param HashMap hmParam  
	       * @return HashMap
	       * @exception AppError
		    **/
	      public HashMap fetchOUSConsignmentDetails(HashMap hmParam) throws AppError
	      {
	    	  GmRequestReportBean gmRequestReportBean = new GmRequestReportBean();
	    	  GmOperationsBean gmOperationsBean = new GmOperationsBean();
	    	  GmCustomerBean gmCustomerBean = new GmCustomerBean(); 
	    	  
	    	  HashMap hmRefDetails = new HashMap();
	    	  HashMap hmSetMaster = new HashMap();
	    	  HashMap hmConsignDetails = new HashMap();
	    	  
	    	  String strRequestId = GmCommonClass.parseNull(gmRequestReportBean.fetchOUSRequestId(hmParam));
	    	  hmRefDetails.put("REQUESTID", strRequestId);	    	  
	    	  hmConsignDetails = GmCommonClass.parseNullHashMap((HashMap)gmCustomerBean.loadOUSConsignDetails(hmParam));
	    	  hmRefDetails.put("HMCONSIGNDETAILS", hmConsignDetails);
	    	  hmSetMaster = GmCommonClass.parseNullHashMap((HashMap)gmOperationsBean.loadOUSSavedSetMaster(hmParam));
	    	  hmRefDetails.put("HMCONSIGNCHILDDETAILS", hmSetMaster);
	    	  
	    	  return hmRefDetails;   	  
	      }
	      
	      /**
	       * This method does the mapping from Returns details to the Alias specified in the GmEuropeConsignPrint.jrxml 
	       * @author jkumar
	       * @date Dec 12, 2008	
	       * @param
	       */
	      public HashMap mapAlias(HashMap hmParam){
	    	  HashMap hmReturn = new HashMap();
	    	  String strDefault = "-";
	    	  
	    	  hmReturn.put("TYPE", "ICT");
	    	  hmReturn.put("SHIPADD",strDefault);// hmParam.get(""));
	    	  hmReturn.put("BILLADD",hmParam.get("ACCADD"));// hmParam.get("ACCADD"));
	    	  hmReturn.put("SDATE",strDefault);// hmParam.get("RETDATE"));
	    	  hmReturn.put("PARTYID", hmParam.get("PARTYID"));
	    	  //hmReturn.put("CURRENCY", value)
	    	  hmReturn.put("TRACK", strDefault);
	    	  hmReturn.put("VARIABLECURRENCY", hmParam.get("NEWCURRENCY"));
	    	  hmReturn.put("CID", hmParam.get("RAID"));
	    	  hmReturn.put("SHIPCOST", "0");
	    	  hmReturn.put("CONSIGNSTATUS", hmParam.get("STATUS"));
	    	  hmReturn.put("NAME", strDefault);
	    	  hmReturn.put("ADDWATERMARK", "Credit Note");
	    	  hmReturn.put("SMODE", strDefault);
	    	  hmReturn.put("SCARR", strDefault);
	    	  hmReturn.put("UDATE", strDefault);
	    	  hmReturn.put("COMPANYID",hmParam.get("COMPANYID"));
	    	  return hmReturn;
	    	  
	      }
	      
	      /** Changes for ICT
		      * This method accepts Consign ID and returns 
		      * shipped date of that Consignment
		      * 
		      * @return String
		      * @exception AppError
		    **/     
		      
		  public String fetchConsignShipDt(String StrConsgnId) throws AppError
		   {
		    String  strShipDt = null;
		    GmDBManager gmDBManager = new GmDBManager();
		    gmDBManager.setFunctionString("gm_pkg_op_ict.get_ship_date", 1);
		    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		    gmDBManager.setString(2, GmCommonClass.parseNull(StrConsgnId));
		    gmDBManager.execute();
		    strShipDt = gmDBManager.getString(1);
		    gmDBManager.close();      
		    log.debug("Consignment Shipped Date:"+strShipDt);                   
		     return strShipDt;
		     
		   } 
		  
	      /**
	       * This method fetches the OUS Order details that'll be used in print page (jasper bnased) 
	       * @author ssharmila
	       * @param
	       */
	      public HashMap fetchOUSOrderDetails(String strOrdId,String strLUserId ) throws AppError
	      {
	    	  HashMap hmRefDetails = new HashMap();
	    	  HashMap hmParam = new HashMap();	    	  
	    	  hmParam.put("OUSORDID",strOrdId);
	    	  hmParam.put("USERID",strLUserId);
	    	  hmRefDetails=fetchOUSOrderDetails(hmParam);	
	    	  return hmRefDetails;
	      }
	      /**
	       * This method fetches the COnsignment and Item consignment details that'll be used in print page (jasper bnased) 
	       * This method will call from shipment job and screen 
	       * @author aprasath
	       * @date Jan 24, 2018	
	       * @param
	       */
	      
	      public HashMap fetchOUSOrderDetails(HashMap hmParam) throws AppError
	      {
	    	  String strOrdId = GmCommonClass.parseNull((String)hmParam.get("OUSORDID"));
	    	  String strLUserId =GmCommonClass.parseNull((String)hmParam.get("USERID"));
	    	  
	    	  GmRequestReportBean gmRequestReportBean = new GmRequestReportBean(getGmDataStoreVO());
	    	  GmOperationsBean gmOperationsBean = new GmOperationsBean(getGmDataStoreVO());
	    	  GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO()); 
	    	  
	    	  HashMap hmRefDetails = new HashMap();
	    	  HashMap hmSetMaster = new HashMap();
	    	  HashMap hmConsignDetails = new HashMap();
	    	  
	    	  String strRequestId = GmCommonClass.parseNull(gmRequestReportBean.fetchRequestId(strOrdId));
	    	  hmRefDetails.put("REQUESTID", strRequestId);
	    	  hmConsignDetails = gmCustomerBean.loadOUSOrdDetails(strOrdId,strLUserId);
	    	  hmRefDetails.put("HMCONSIGNDETAILS", hmConsignDetails);
	    	  
	    	  hmSetMaster = fetchOrderPartDtls(strOrdId);
	    	  hmRefDetails.put("HMCONSIGNCHILDDETAILS", hmSetMaster); 
	    
	    	  return hmRefDetails;
	    	    
	      }
	      
	      public HashMap fetchOrderPartDtls(String strOrdId) throws AppError
	      {
	    	  StringBuffer sbquery = new StringBuffer();
	    	  HashMap hmReturn = new HashMap();
	    	  ArrayList alReturn = new ArrayList();
	    	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());    	    

	    	    sbquery.append(" SELECT T502.C205_PART_NUMBER_ID PNUM ");
	    	    sbquery.append(",GET_PARTNUM_DESC(T502.C205_PART_NUMBER_ID) PDESC ");
	    	    sbquery.append(" ,T502.C502_ITEM_QTY IQTY ");
	    	    sbquery.append(",T502.C502_CONTROL_NUMBER CNUM ");
	    	    sbquery.append(" ,T502.C502_ITEM_PRICE PRICE ");
	    	    sbquery.append(" FROM T501_ORDER T501,T502_ITEM_ORDER T502 where T501.C501_ORDER_ID = '");
	    	    	  sbquery.append(strOrdId);
	    	    	      sbquery.append("'");
	    	    	      sbquery.append(" and T501.C501_ORDER_ID = T502.C501_ORDER_ID and T501.C501_VOID_FL IS NULL");	    	     
	    	      log.debug("Set Load Query fetchOrderPartDtls****** " + sbquery.toString());
	    	      alReturn = gmDBManager.queryMultipleRecords(sbquery.toString());
	    	      hmReturn.put("SETLOAD", alReturn);
	    	   
	    	    return hmReturn;
	    	    
	      }
	      
	      public RowSetDynaClass getOusReturnsDashboardDyna(String strStatus, String strAccCurrId)
	    	      throws AppError {
	    	    RowSetDynaClass resultSet = null;

	    	    try {
	    	      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    	      String strQueryReturnsDashBoard = getOusReturnsDashBoardQuery(strStatus, strAccCurrId);
	    	      resultSet = gmDBManager.QueryDisplayTagRecordset(strQueryReturnsDashBoard);
	    	      log.debug("RowSetDyna result Set is  " + resultSet.getRows().toArray());

	    	    } catch (Exception e) {
	    	      throw new AppError(e);
	    	    }
	    	    return resultSet;
	    	  } // End of getReturnsDashboard


	    	  private String getOusReturnsDashBoardQuery(String strStatus, String strAccCurrId) {
	    	    StringBuffer sbQuery = new StringBuffer();
	    	    sbQuery
	    	        .append(" SELECT    T506.C506_RMA_ID RAID, T506.C506_COMMENTS COMMENTS, T506.C506_CREATED_DATE CDATE, ");
	    	    sbQuery
	    	        .append(" GET_USER_NAME(T506.C506_CREATED_BY) PER, GET_CODE_NAME(T506.C506_TYPE) TYPE, T506.C506_TYPE TYPEID, ");
	    	    sbQuery.append(" GET_CODE_NAME(T506.C506_REASON) REASON, T506.C506_EXPECTED_DATE EDATE,");
	    	    sbQuery
	    	        .append(" T506.C701_DISTRIBUTOR_ID DID ,GET_DISTRIBUTOR_NAME(T506.C701_DISTRIBUTOR_ID) DNAME, ");
	    	    sbQuery.append(" T506.C704_ACCOUNT_ID ACCID, GET_ACCOUNT_NAME(T506.C704_ACCOUNT_ID) ANAME, ");
	    	    sbQuery.append(" T506.C501_ORDER_ID ORDID, T506.C501_REPROCESS_DATE RPDATE,");
	    	    sbQuery
	    	        .append(" DECODE(T506.C506_STATUS_FL,0,'-',1,'Y') STATUS_FL, NVL(GET_TOTAL_RETURN_AMT(T506.C506_RMA_ID),0) RAMT, ");
	    	    sbQuery.append(" C506_RETURN_DATE RDATE, GET_SET_NAME(T506.C207_SET_ID) SNAME ");
	    	    sbQuery.append(" ,T506.C501_ORDER_ID PARENTOID ");

	    	    if (strStatus.equals("ACCT")) {
	    	      sbQuery.append(" ,t101.C101_PARTY_NM paracctnm,t101.c101_party_id paracctid");
	    	      sbQuery.append(" FROM T506_RETURNS T506 ,t704_account t704, t101_party t101, t501_order t501");
	    	      sbQuery.append(" WHERE T506.C506_VOID_FL IS NULL ");
	    	      sbQuery.append(" AND T506.c704_account_id = t704.c704_account_id ");
	    	      sbQuery.append("  AND t704.c101_party_id = t101.c101_party_id ");
	    	      sbQuery.append("  AND t101.c101_void_fl IS NULL ");
	    	      sbQuery.append(" AND t506.c501_order_id = t501.c501_order_id(+) ");
	    	      sbQuery.append(" AND t704.c704_account_id =  t501.c704_account_id(+) ");
	    	      sbQuery.append(" AND NVL(t704.c901_account_type, -999) = 26240482 "); //PMT-31253 added this line to show OUS RAs in OUS Dashboard
	    	      sbQuery.append("  AND T506.c1900_company_id ='" + getGmDataStoreVO().getCmpid() + "' ");
	    	      sbQuery.append(" AND t704.c901_currency = " + strAccCurrId);
	    	    } else {
	    	      sbQuery.append(" FROM T506_RETURNS T506 ");
	    	      sbQuery.append(" WHERE T506.C506_VOID_FL IS NULL ");
	    	      sbQuery.append("   AND T506.C5040_PLANT_ID = '" + getCompPlantId() + "'");
	    	    }

	    	    if (strStatus.equals("ACCT")) {
	    	      sbQuery.append(" AND T506.C506_AC_CREDIT_DT IS NULL AND T506.C506_TYPE = '3300' ");
	    	      sbQuery.append(" AND T506.C506_REASON NOT IN (3316,3317) ");
	    	      sbQuery.append(" ORDER BY T506.C506_RMA_ID DESC ");
	    	    } else if (strStatus.equals("CUSTSER")) {
	    	      sbQuery.append(" AND T506.C506_STATUS_FL = 1 ");
	    	      sbQuery.append(" ORDER BY T506.C506_RMA_ID DESC ");
	    	    } else if (strStatus.equals("OPERALL")) {
	    	      sbQuery.append(" AND T506.C506_STATUS_FL = 0 ");
	    	      sbQuery.append(" ORDER BY T506.C506_TYPE, DNAME, ANAME, T506.C506_RMA_ID DESC ");
	    	    } else if (strStatus.equals("REPROCESS")) {
	    	      sbQuery.append(" AND T506.C506_STATUS_FL IN (1,2) AND T506.C501_REPROCESS_DATE IS NULL ");
	    	      sbQuery
	    	          .append(" AND 0 < (SELECT SUM(C507_ITEM_QTY) FROM T507_RETURNS_ITEM T507 WHERE T507.C506_RMA_ID = T506.C506_RMA_ID) ");
	    	      sbQuery.append(" ORDER BY T506.C506_TYPE,T506.C506_RMA_ID DESC ");
	    	    }

	    	    log.debug("getOusReturnsDashBoardQuery Query value is " + sbQuery.toString());
	    	    return sbQuery.toString();
	    	  }

}
