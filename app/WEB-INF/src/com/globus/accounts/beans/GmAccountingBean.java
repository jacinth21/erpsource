/*
 * Module: GmAccountingBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written:
 * 11 Mar 2004 Security: Unclassified Description: Accounts * Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What * you changed
 */

package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAccountingBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmAccountingBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmAccountingBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * listAccountElement- This method will
   * 
   * @param String strType
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList listAccountElement(String strType) throws AppError {
    ArrayList alReturn = new ArrayList();
    try {

      DBConnectionWrapper dbCon = null;
      dbCon = new DBConnectionWrapper();

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT	C801_ACCOUNT_ELEMENT_ID ACCELEMID, C801_NUMBER ACCNUM, C801_NAME ACCNM, ");
      sbQuery.append(" C801_DESC ACCDESC,C801_ACTIVE_FL AFL, C800_ACCOUNT_TYPE_ID ACCTYPEID ");
      sbQuery.append(" FROM T801_ACCOUNT_ELEMENT ");
      sbQuery.append(" WHERE C801_ACTIVE_FL = 'Y' ");
      sbQuery.append(" ORDER BY C801_NAME ");

      alReturn = dbCon.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmAccountingBean:listAccountElement", "Exception is:" + e);
    }
    return alReturn;
  } // End of listAccountElement

  /**
   * reportInvCosting - This method will
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */
  public RowSetDynaClass reportInvCosting(HashMap hmInvCost) throws AppError {
    String strPartNum = GmCommonClass.parseNull((String) hmInvCost.get("PNUM"));
    String strInvType = GmCommonClass.parseZero((String) hmInvCost.get("COST_INV_TYPE"));
    String strFromDt = GmCommonClass.parseNull((String) hmInvCost.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmInvCost.get("TODT"));
    String strCheckOpen = GmCommonClass.parseNull((String) hmInvCost.get("CHECK_OPEN"));
    String strArchiveFl = GmCommonClass.parseNull((String) hmInvCost.get("ARCHIVE_FL"));
    String strArchivedDate = GmCommonClass.parseNull((String) hmInvCost.get("ARCHIVE_DT"));
    String strOwnerCurrency = GmCommonClass.parseZero((String) hmInvCost.get("OWNER_CURRENCY"));
    String strInvPlant = GmCommonClass.parseZero((String) hmInvCost.get("COST_PLANT"));
    String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());
    String strOwnerCompanyId = GmCommonClass.parseZero((String) hmInvCost.get("OWNER_COMPANY_ID"));
    String strDivisionId = GmCommonClass.parseZero((String) hmInvCost.get("DIVISION"));
    String strCogsStatusId = GmCommonClass.parseZero((String) hmInvCost.get("COGS_STATUS_ID"));


    RowSetDynaClass resultSet = null;
    // boolean blFlag = false;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT	t820.C820_COSTING_ID COSTID, t820.C205_PART_NUMBER_ID	PNUM, T205.C205_PART_NUM_DESC PDESC, ");
    sbQuery.append(" to_char(t820.C820_PART_RECEIVED_DT,'" + strCompDateFmt
        + "') TXNDATE, t820.C820_QTY_RECEIVED QTYREC, ");
    sbQuery.append(" t820.C820_TXN_ID TXNID, COGSA.c901_code_nm SFL , COGTP.c901_code_nm CTYPE,");
    sbQuery
        .append(" t820.C820_PURCHASE_AMT PURCAMT, t820.C820_QTY_ON_HAND QOH, t820.C820_QTY_ADDED QTYADD, (t820.C820_PURCHASE_AMT * t820.C820_QTY_ON_HAND) EXTCOST  ");
    sbQuery
        .append(", t820.C1900_COMPANY_ID company_id, t5040.C5040_PLANT_NAME plant_name, c1900_owner_company_id owner_company_id ");
    sbQuery
        .append(", t1900_owner.C1900_COMPANY_CD owner_company_cd, t901_own_curr.C902_CODE_NM_ALT owner_currency, t820.C1910_DIVISION_NAME DIVISION ");
    sbQuery
        .append(", t901_loc_curr.C902_CODE_NM_ALT local_company_currency, NVL(t820.C820_LOCAL_COMPANY_COST, 0) local_company_cost , NVL (t820.C820_LOCAL_COMPANY_COST * t820.C820_QTY_ON_HAND, 0) ext_local_company_cost ");
    sbQuery.append(", decode(MAX(T901_own_curr.C902_CODE_NM_ALT) OVER (), MIN(T901_own_curr.C902_CODE_NM_ALT) OVER (),1,2) CUR_CNT");
    if (strArchiveFl.equals("")) {
      sbQuery.append(" FROM T820_COSTING t820 ");
    } else {
      sbQuery.append(" FROM T820_COSTING_archive t820 ");
    }

    sbQuery.append(" ,T901_CODE_LOOKUP COGSA ");
    sbQuery.append(" ,T901_CODE_LOOKUP COGTP ");
    sbQuery.append(" ,T205_PART_NUMBER T205 ");
    // International posting changes
    sbQuery.append(" ,T5040_PLANT_MASTER t5040 ");
    sbQuery.append(" ,T1900_COMPANY t1900_owner ");
    sbQuery.append(" ,T901_CODE_LOOKUP t901_loc_curr ");
    sbQuery.append(" ,T901_CODE_LOOKUP t901_own_curr ");
    sbQuery.append(" WHERE ");
    sbQuery.append(" t820.C901_STATUS = COGSA.c901_code_id ");
    sbQuery.append(" AND t820.C901_COSTING_TYPE  = COGTP.c901_code_id ");
    sbQuery.append(" AND t820.C205_PART_NUMBER_ID=T205.C205_PART_NUMBER_ID ");
    // International posting changes
    sbQuery.append(" AND t820.C5040_PLANT_ID = t5040.C5040_PLANT_ID (+) ");
    sbQuery.append(" AND t820.C1900_OWNER_COMPANY_ID = t1900_owner.C1900_COMPANY_ID (+) ");
    sbQuery.append(" AND t820.C901_LOCAL_COMPANY_CURRENCY =  t901_loc_curr.C901_CODE_ID  (+) ");
    sbQuery.append(" AND t820.C901_OWNER_CURRENCY =  t901_own_curr.C901_CODE_ID  (+) ");

    if (!strPartNum.equals("''")) {
      sbQuery.append(" AND t820.C205_PART_NUMBER_ID IN (");
      sbQuery.append(strPartNum);
      sbQuery.append(")");
      // blFlag = true;
    }

    if (!strInvType.equals("0")) {
      // if (blFlag) {
      // sbQuery.append(" AND ");
      // }
      sbQuery.append(" AND t820.C901_COSTING_TYPE = ");
      sbQuery.append(strInvType);
      // blFlag = true;
    }

    if (!strFromDt.equals("") && !strToDt.equals("")) {
      // if (blFlag) {
      // sbQuery.append(" AND ");
      // }
      sbQuery.append(" AND t820.C820_PART_RECEIVED_DT between ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strCompDateFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strCompDateFmt + "') ");
    }

    if (!strCheckOpen.equals("")) {
      sbQuery.append(" AND t820.C901_STATUS != 4802 ");

    }
    sbQuery.append(" AND t820.c1900_company_id = '" + getCompId() + "'");
    sbQuery.append(" AND c820_delete_fl = 'N' ");
    if (!strOwnerCurrency.equals("0")) {
      sbQuery.append(" AND t820.c901_owner_currency ='" + strOwnerCurrency + "'");
    }
    if (!strInvPlant.equals("0")) {
      sbQuery.append(" AND t820.c5040_plant_id ='" + strInvPlant + "'");
    }
    if (!strOwnerCompanyId.equals("0")) {
      sbQuery.append(" AND t820.c1900_owner_company_id ='" + strOwnerCompanyId + "'");
    }
    if (!strDivisionId.equals("0")) {
        sbQuery.append(" AND t820.C1910_DIVISION_ID =" + strDivisionId + "");
      }
    if (!strCogsStatusId.equals("0")) {
      sbQuery.append(" AND t820.c901_status ='" + strCogsStatusId + "'");
    }
    sbQuery.append(" ORDER BY t820.C205_PART_NUMBER_ID, t820.C820_COSTING_ID ");

    log.debug(" Query inside reportInvCosting is -- " + sbQuery.toString());

    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return resultSet;
  } // End of reportInvCosting

  /**
   * reportAcctInventoryTrans - This method will
   * 
   * @param String strType
   * @return ArrayList
   * @exception AppError
   */
  public RowSetDynaClass reportAcctInventoryTrans(HashMap hmParam) throws AppError {
    RowSetDynaClass resultSet = null;


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    boolean flag = false;
    String strType = GmCommonClass.parseNull((String) hmParam.get("ELEMID"));
    String strTransType = GmCommonClass.parseNull((String) hmParam.get("TRANSTYPE"));
    String strTransId = GmCommonClass.parseNull((String) hmParam.get("TRANSID"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPartNumFormat = GmCommonClass.parseNull((String) hmParam.get("PARTNUMFORMAT"));
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));

    String strCompanyId = GmCommonClass.parseZero((String) hmParam.get("COMPID"));
    String strCountryId = GmCommonClass.parseZero((String) hmParam.get("COUNTRYID"));
    String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());
    String strArchivefl = GmCommonClass.parseNull((String) hmParam.get("ARCHIVEFL"));
    String strArchivedDate = GmCommonClass.parseNull((String) hmParam.get("POST_ARCHIVEDDATE"));
    String strOwnerCurrency = GmCommonClass.parseNull((String) hmParam.get("OWNER_CURRENCY"));
    String strInvPlant = GmCommonClass.parseNull((String) hmParam.get("COST_PLANT"));
    String strOwnerCompanyId = GmCommonClass.parseNull((String) hmParam.get("OWNER_COMPANY_ID"));
    String strDivisionId = GmCommonClass.parseZero((String) hmParam.get("DIVISION"));
    

    StringBuffer sbQuery = new StringBuffer();
    // If archive check box is unchecked we are going to get data from t810_posting_txn table.

    sbQuery.append(" SELECT	t810.C810_ACCOUNT_TXN_ID ACTTXNID, to_char(t810.C810_TXN_DATE,'"
        + strCompDateFmt + "') TXNDATE,");
    sbQuery.append(" t801.C801_NAME ||'/'|| t901_acc.C901_CODE_NM TXNTYPE, ");
    sbQuery.append(" t810.C810_DESC TXNDESC, to_char(t810.C810_ACCT_DATE,'" + strCompDateFmt
        + "') ACTDATE, ");
    sbQuery
        .append(" DECODE(t810.C810_DR_AMT,0,0,nvl(t810.C810_DR_AMT*t810.C810_QTY,0)) DRAMT, DECODE(t810.C810_CR_AMT,0,0,nvl(t810.C810_CR_AMT*t810.C810_QTY,0)) CRAMT, ");
    sbQuery.append(" t810.C810_QTY QTY, t205.C205_PART_NUM_DESC  PDESC,");
    sbQuery.append(" t810.C803_PERIOD_ID PERID, t810.C205_PART_NUMBER_ID PNUM,  ");
    sbQuery
        .append(" t810.C810_PARTY_ID PARTYID, nvl(t810.C820_COSTING_ID,0) COSTID, to_char(t810.C810_CREATED_DATE,'hh24:mi AM') TXNTIME, ");
    sbQuery
        .append(" t810.C810_TXN_ID TXNID, t101.C101_USER_F_NAME || ' ' || t101.C101_USER_L_NAME CREATEDBY, t810.C901_ACCOUNT_TXN_TYPE TXNTYPEID, ");
    sbQuery.append(" t810.c901_company_id COMPID, t810.c901_country_id COUNTRYID, ");
    // sbQuery.append(" get_code_name(t810.c901_company_id) COMPNM, get_code_name(t810.c901_country_id) COUNTRYNM ");

    sbQuery.append("  t1900_owner.C1900_COMPANY_CD owner_company_cd, ");
    sbQuery.append("  t810.C1910_DIVISION_NAME division, ");
    sbQuery
        .append(" t1900_owner.C1900_COMPANY_NAME owner_company_nm, t901_own_curr.C902_CODE_NM_ALT owner_currency, ");
    sbQuery.append(" t5040.C5040_PLANT_NAME plant_nm, t1900.C1900_COMPANY_CD company_cd, ");
    sbQuery
        .append(" t901_loc_curr.C902_CODE_NM_ALT company_currency, NVL (C810_local_company_DR_AMT * t810.C810_QTY, 0) corp_dr_amt, NVL (C810_local_company_CR_AMT * t810.C810_QTY, 0)  corp_cr_amt ");
    // sbQuery.append(" ,get_company_code(t810.c1900_destination_company_id) destination_company_cd, get_company_name (t810.c1900_destination_company_id) destination_company_nm ");
    sbQuery
        .append(" , t1900_txn_comp.c1900_company_name txn_company_name, t1900_dest_comp.c1900_company_name dest_company_name ");
    sbQuery
        .append(" , t1900_txn_comp.c1900_company_cd txn_company_cd, t1900_dest_comp.c1900_company_cd dest_company_cd ");
    sbQuery.append(", decode(MAX(T901_own_curr.C902_CODE_NM_ALT) OVER (), MIN(T901_own_curr.C902_CODE_NM_ALT) OVER (),1,2) CUR_CNT");
    if (!strArchivefl.equals("true")) {
      sbQuery.append(" FROM t810_posting_txn t810, t1900_company t1900 ");
    } else {
      sbQuery.append(" FROM t810_posting_txn_archive t810, t1900_company t1900 ");
    }
    // International posting changes
    sbQuery
        .append(" , T5040_PLANT_MASTER t5040, T1900_COMPANY t1900_owner, T901_CODE_LOOKUP t901_loc_curr ");
    sbQuery.append(" , T901_CODE_LOOKUP t901_own_curr, T101_USER t101, T205_PART_NUMBER t205 ");
    sbQuery.append(" , T901_CODE_LOOKUP t901_acc, T801_ACCOUNT_ELEMENT t801 ");
    sbQuery.append(" , T1900_COMPANY t1900_txn_comp, T1900_COMPANY t1900_dest_comp  ");
    sbQuery.append("  WHERE C810_DELETE_FL <> 'Y' ");
    sbQuery.append("  AND t1900.C1900_COMPANY_ID = t810.C1900_COMPANY_ID ");
    // International posting changes
    sbQuery.append("  AND t810.C5040_PLANT_ID              = t5040.C5040_PLANT_ID (+) ");
    sbQuery.append("  AND t810.C1900_OWNER_COMPANY_ID      = t1900_owner.c1900_company_id (+) ");
    sbQuery.append("  AND t810.C901_LOCAL_COMPANY_CURRENCY = t901_loc_curr.C901_CODE_ID (+) ");
    sbQuery.append("  AND t810.C901_OWNER_CURRENCY         = t901_own_curr.C901_CODE_ID (+) ");
    sbQuery.append("  AND t810.c810_created_by             = t101.C101_USER_ID (+) ");
    sbQuery.append("  AND t810.C205_PART_NUMBER_ID         = t205.C205_PART_NUMBER_ID (+) ");
    sbQuery.append("  AND t810.C901_ACCOUNT_TXN_TYPE       = t901_acc.C901_CODE_ID ");
    sbQuery.append("  AND t810.C801_ACCOUNT_ELEMENT_ID     = t801.C801_ACCOUNT_ELEMENT_ID ");
    sbQuery.append(" AND t810.C1900_TXN_COMPANY_ID =  t1900_txn_comp.C1900_COMPANY_ID (+)  ");
    sbQuery
        .append(" AND t810.C1900_DESTINATION_COMPANY_ID = t1900_dest_comp.C1900_COMPANY_ID (+) ");
    
    if (!strType.equals("") && !strType.equals("0")) {
      sbQuery.append(" AND t810.C801_ACCOUNT_ELEMENT_ID ='");
      sbQuery.append(strType);
      sbQuery.append("'");
    }

    if (!strTransType.equals("") && !strTransType.equals("0")) {
      flag = true;
      sbQuery.append(" AND t810.C901_ACCOUNT_TXN_TYPE ='");
      sbQuery.append(strTransType);
      sbQuery.append("'");
    }
    if (!strTransId.equals("")) {
      flag = true;
      sbQuery.append(" AND t810.C810_TXN_ID ='");
      sbQuery.append(strTransId);
      sbQuery.append("'");
    }
    if (!strFromDt.equals("") && !strToDt.equals("")) {
      flag = true;
      sbQuery.append(" AND t810.C810_TXN_DATE between ");
      sbQuery.append(" to_date('");
      sbQuery.append(strFromDt);
      sbQuery.append("','" + strCompDateFmt + "') AND to_date('");
      sbQuery.append(strToDt);
      sbQuery.append("','" + strCompDateFmt + "') ");
    }
    if (!strPartNumFormat.equals("''")) {
      flag = true;
      sbQuery.append(" AND t810.C205_PART_NUMBER_ID IN (");
      sbQuery.append(strPartNumFormat);
      sbQuery.append(")");
    }
    if (!(strPartyId.equals("") || strPartyId.equals("0"))) {

      sbQuery.append(" and (t810.C810_PARTY_ID =");
      sbQuery.append(strPartyId);
      sbQuery.append("");
      sbQuery.append(" or t810.C810_PARTY_ID =get_account_dist_id('");
      sbQuery.append(strPartyId);
      sbQuery.append("')");
      sbQuery.append(")");
    }
    // Company filter added
    if (!strCompanyId.equals("0")) {
      sbQuery.append(" AND t810.c901_company_id =" + strCompanyId);
    }
    // Country filter added
    if (!strCountryId.equals("0")) {
      sbQuery.append(" AND t810.c901_country_id =" + strCountryId);
    }

    sbQuery.append(" AND t810.c1900_company_id = '" + getCompId() + "'");
    if (!strOwnerCurrency.equals("0")) {
      sbQuery.append(" AND t810.c901_owner_currency ='" + strOwnerCurrency + "'");
    }
    if (!strInvPlant.equals("0")) {
      sbQuery.append(" AND t810.c5040_plant_id ='" + strInvPlant + "'");
    }
    if (!strOwnerCompanyId.equals("0")) {
      sbQuery.append(" AND t810.c1900_owner_company_id ='" + strOwnerCompanyId + "'");
    }
    if (!strDivisionId.equals("0")) {
        sbQuery.append(" AND t810.C1910_DIVISION_ID =" + strDivisionId + "");
      }
    sbQuery.append(" ORDER BY t810.C810_ACCOUNT_TXN_ID ");

    log.debug("main sbQuery: " + sbQuery.toString());

    if (flag == true) {
      resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    }

    return resultSet;
  } // End of reportAcctInventoryTrans

  /**
   * fetchInvoiceSource - This method will return arrayList of the invoice source
   * 
   * @param String strInvoiceID
   * @exception AppError
   */
  public HashMap fetchInvoiceSource(String strInvoiceId) throws AppError {
    HashMap hmReturn = new HashMap();

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_op_fch_invoice_source", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strInvoiceId);

    gmDBManager.execute();

    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchConsignmentIdFromInvoice - This method will return arrayList of the Consignment Id
   * 
   * @param String strInvoiceId
   * @exception AppError
   */
  public String fetchConsignmentIdFromInvoice(String strInvoiceId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_op_fch_csg_from_invoice", 2);
    gmDBManager.registerOutParameter(2, java.sql.Types.CHAR);
    gmDBManager.setString(1, strInvoiceId);

    gmDBManager.execute();

    String strRefId = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strRefId;
  }

  /**
   * fetchInvoiceIdFromConsignmentId - This method will return strInvoiceId
   * 
   * @param String strConsignmentId
   * @exception AppError
   */
  public String fetchInvoiceIdFromConsignmentId(String strConsignmentId) throws AppError {

    String strInvoiceId = "";

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("gm_pkg_ac_invoice.get_invoiceidfromconsigid", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strConsignmentId);
    gmDBManager.execute();
    strInvoiceId = gmDBManager.getString(1);
    gmDBManager.close();

    return strInvoiceId;
  }

  /**
   * fetchCustomerPoNum - This method will return CustomerPoNum for the given InvoiceId
   * 
   * @param String invoiceid
   * @exception AppError
   */
  public String fetchCustomerPoNum(String strInvoiceid) throws AppError {

    String strReturn = "";

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("gm_pkg_ac_invoice.get_cust_po_num", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strInvoiceid);
    gmDBManager.execute();
    strReturn = gmDBManager.getString(1);
    gmDBManager.close();

    return strReturn;
  }

  /**
   * fetchPostingByCountryList - method used to fetch all the ICT distributor Country list.
   * 
   * @used Its used for Inventory Posting, Roll forward report and Cogs Error reports.
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchPostingByCountryList(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strRuleGroup = GmCommonClass.parseNull((String) hmParam.get("RULE_GRP"));
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_op_ict.gm_fch_posting_by_country", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRuleGroup);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }
  
  /**
   * fetchICSBankDetails - This method used to fetch ICS bank details from rules table
   * 
   * @param strDistId
   * @param strPartyId
   * @return Hash map
   * @throws AppError
   */
  public HashMap fetchICSBankDetails(String strDistId, String strPartyId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_ict.gm_fch_ics_bank_dtls", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDistId);
    gmDBManager.setString(2, strPartyId);
    gmDBManager.execute();
    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    log.debug(" hmReturn values " + hmReturn);
    return hmReturn;
  }



}// end of class GmAccountingBean
