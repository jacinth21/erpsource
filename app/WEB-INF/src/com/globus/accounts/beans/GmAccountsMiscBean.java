/*
 * Module: GmAccountsMiscBean Author: Dhinakaran James Project: Globus Medical App Date-Written: 7
 * Aug 2006 Security: Unclassified Description: Accounts * Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx
 */

package com.globus.accounts.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAccountsMiscBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  // this will be removed all place changed with Data Store VO constructor
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAccountsMiscBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * sendEmailReminderPO - This method will
   * 
   * @param - strInputStr - Contians the list of Orders for which emails have to be sent out.
   *        tokenised with the ^ symbol
   * @param - userid - Used to save in the Log Table.
   * @return - Doesnt have any value now
   * @exception AppError
   **/
  public HashMap sendEmailReminderPO(String strInputStr, String strUserId, String strEmailType,
      String strAccCurrName) throws AppError {
    log.debug("currency symbol is" + strAccCurrName);
    HashMap hmReturn = new HashMap();
    String strOrderId = "";
    String strAccName = "";
    Date dtOrderDt = null;
    String strOrderAmt = "";
    String strRepId = "";
    String strRepName = "";
    String strDistNm = "";
    ArrayList alReturn = new ArrayList();
    int intArLength = 0;
    String strDispNm = "";
    String strMailIds = "";
    String strCCMailIds = "";
    String strSenderEmail = "";
    String strCompanyId = getGmDataStoreVO().getCmpid();
    HashMap hmRuleData = new HashMap();
    HashMap hmReturnDetails = new HashMap();

    String strSubjectMsg = "";
    String strEmailId = "";
    String strMimeType = "";
    String strJasperName = "/GmRevenuePORemainder.jasper";
    String strDefaultEmail = "";
    String strActionMessage = "";
    String strEmailHeader = "";
    String strPoPolicyOne = "";
    String strPoPolicyTwo = "";
    String strPoQueryMessage = "";
    String strPolicyMessage = "";
    String strRevAction = "";
    String strRevActionOne = "";
    String strRevActionTwo = "";
    String strRevActionThree = "";
    String strRevActionFour = "";
    String strEmail = "";
    String strCurrSymbol = "";
    String strEmailTypeNm = strEmailType;
    String strSubject = "";
    String strREVActionEmail = "";
    String strCmpCurrSymbl = "";
    strCCMailIds =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("POREMIND", "EMAIL",
            strCompanyId));

    StringBuffer sbMailMsg = new StringBuffer();
    StringBuffer sbOrdDetails = new StringBuffer();
    StringBuffer sbMailMsgStart = new StringBuffer();
    StringBuffer sbMailMsgEnd = new StringBuffer();
    StringBuffer sbRuleQuery = new StringBuffer();

    HashMap hmJasperPerams = new HashMap();
    HashMap hmPODetails = new HashMap();
    HashMap hmCurrency = new HashMap();
    ArrayList alPORuleReturn = new ArrayList();
    ArrayList alRevenueRuleReturn = new ArrayList();

    GmEmailProperties emailProps = null;
    GmJasperMail jasperMail = new GmJasperMail();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    strCmpCurrSymbl = GmCommonClass.parseNull((String) hmCurrency.get("CMPCURRSMB"));

    if (strEmailType.equals("RevenueEmail")) {
      sbRuleQuery.append("SELECT GET_RULE_VALUE_BY_COMPANY('REVENUEEMAILFROM','REVENUEREMAINDER','"
          + strCompanyId
          + "') REVEMAIL,GET_RULE_VALUE_BY_COMPANY('REVENUEHEADER','REVENUEREMAINDER','"
          + strCompanyId
          + "') REVHEADER, GET_RULE_VALUE_BY_COMPANY('REVENUEACTION','REVENUEREMAINDER','"
          + strCompanyId + "') REVACTION");
      sbRuleQuery.append(",GET_RULE_VALUE_BY_COMPANY('REVENUEACTION1','REVENUEREMAINDER','"
          + strCompanyId
          + "') REVACTION1,GET_RULE_VALUE_BY_COMPANY('REVENUEACTION2','REVENUEREMAINDER','"
          + strCompanyId
          + "') REVACTION2,GET_RULE_VALUE_BY_COMPANY('REVENUEACTION3','REVENUEREMAINDER','"
          + strCompanyId + "') REVACTION3");
      sbRuleQuery.append(",GET_RULE_VALUE_BY_COMPANY('REVENUEACTION4','REVENUEREMAINDER','"
          + strCompanyId
          + "') REVACTIONEMAIL , GET_RULE_VALUE_BY_COMPANY('REVENUEQUERY','REVENUEREMAINDER','"
          + strCompanyId
          + "') REVQUERY, GET_RULE_VALUE_BY_COMPANY('REVENUESUBJECT','REVENUEREMAINDER','"
          + strCompanyId + "') REVSUBJECT");
      sbRuleQuery.append(",'" + strAccCurrName + "' CURRSYMBOL  FROM DUAL");
      alRevenueRuleReturn = gmDBManager.queryMultipleRecords(sbRuleQuery.toString());
    } else {
      sbRuleQuery.append("SELECT GET_RULE_VALUE_BY_COMPANY('POEMAILFROM','POREMAINDER','"
          + strCompanyId + "') POEMAIL,GET_RULE_VALUE_BY_COMPANY('POHEADER','POREMAINDER','"
          + strCompanyId + "') POHEADER, GET_RULE_VALUE_BY_COMPANY('POACTION','POREMAINDER','"
          + strCompanyId + "') POACTION");
      sbRuleQuery.append(",GET_RULE_VALUE_BY_COMPANY('POPOLICYONE','POREMAINDER','" + strCompanyId
          + "') POPOLICYONE,GET_RULE_VALUE_BY_COMPANY('POPOLICYTWO','POREMAINDER','" + strCompanyId
          + "') POPOLICYTWO,GET_RULE_VALUE_BY_COMPANY('POQUERYMESSAGE','POREMAINDER','"
          + strCompanyId + "') POQUERYMESSAGE");
      sbRuleQuery.append(",GET_RULE_VALUE_BY_COMPANY('POSUBJECT','POREMAINDER','" + strCompanyId
          + "') POSUBJECT");
      sbRuleQuery.append(",'" + strAccCurrName + "' CURRSYMBOL FROM DUAL");
      alPORuleReturn = gmDBManager.queryMultipleRecords(sbRuleQuery.toString());
    }
    log.debug("revenue email=======>" + sbRuleQuery.toString());
    gmDBManager.close();

    if (alRevenueRuleReturn.size() > 0 || alPORuleReturn.size() > 0) {
      if (alPORuleReturn.size() > 0) {
        for (Iterator iter = alPORuleReturn.iterator(); iter.hasNext();) {
          HashMap hmRuleDetails = new HashMap();
          hmRuleDetails = (HashMap) iter.next();
          strEmailId = GmCommonClass.parseNull((String) hmRuleDetails.get("POEMAIL"));
          strCurrSymbol = GmCommonClass.parseNull((String) hmRuleDetails.get("CURRSYMBOL"));
          strSubjectMsg = GmCommonClass.parseNull((String) hmRuleDetails.get("POSUBJECT"));
          strEmailHeader = GmCommonClass.parseNull((String) hmRuleDetails.get("POHEADER"));
          strPoPolicyOne = GmCommonClass.parseNull((String) hmRuleDetails.get("POPOLICYONE"));
          strPoPolicyTwo = GmCommonClass.parseNull((String) hmRuleDetails.get("POPOLICYTWO"));
          strActionMessage = GmCommonClass.parseNull((String) hmRuleDetails.get("POACTION"));
          strPoQueryMessage = GmCommonClass.parseNull((String) hmRuleDetails.get("POQUERYMESSAGE"));
        }
        strPolicyMessage = strPoPolicyOne + " " + strPoPolicyTwo;
      }
      if (alRevenueRuleReturn.size() > 0) {
        for (Iterator iter = alRevenueRuleReturn.iterator(); iter.hasNext();) {
          HashMap hmRuleDetails = new HashMap();
          hmRuleDetails = (HashMap) iter.next();
          strEmailId = GmCommonClass.parseNull((String) hmRuleDetails.get("REVEMAIL"));
          strRevAction = GmCommonClass.parseNull((String) hmRuleDetails.get("REVACTION"));
          strCurrSymbol = GmCommonClass.parseNull((String) hmRuleDetails.get("CURRSYMBOL"));
          strSubjectMsg = GmCommonClass.parseNull((String) hmRuleDetails.get("REVSUBJECT"));
          strEmailHeader = GmCommonClass.parseNull((String) hmRuleDetails.get("REVHEADER"));
          strRevActionOne = GmCommonClass.parseNull((String) hmRuleDetails.get("REVACTION1"));
          strRevActionTwo = GmCommonClass.parseNull((String) hmRuleDetails.get("REVACTION2"));
          strREVActionEmail = GmCommonClass.parseNull((String) hmRuleDetails.get("REVACTIONEMAIL"));
          strRevActionThree = GmCommonClass.parseNull((String) hmRuleDetails.get("REVACTION3"));
          strPoQueryMessage = GmCommonClass.parseNull((String) hmRuleDetails.get("REVQUERY"));
        }
      }
    }

    if (strEmailType.equals("RevenueEmail")) {
      strSubjectMsg = strSubjectMsg;
      sbMailMsgStart.append(strEmailHeader);
      sbMailMsgEnd.append("\n\n\n");
      sbMailMsgEnd.append(strRevAction);
      sbMailMsgEnd.append("\n\n");
      sbMailMsgEnd.append(strRevActionOne);
      sbMailMsgEnd.append("\n\n");
      sbMailMsgEnd.append(strRevActionTwo);
      sbMailMsgEnd.append("\n\n");
      sbMailMsgEnd.append(strRevActionThree);
      sbMailMsgEnd.append("\n\n");
      sbMailMsgEnd.append(strREVActionEmail);
      sbMailMsgEnd.append("\n\n\n");
      sbMailMsgEnd.append(strPoQueryMessage);
      strEmailId = strEmailId;
    } else {
      strSubjectMsg = strSubjectMsg;
      sbMailMsgStart.append("Important Reminder:");
      sbMailMsgStart.append("\n\n");
      sbMailMsgStart.append(strEmailHeader);
      sbMailMsgEnd.append(" \n\n\n");
      sbMailMsgEnd.append(strActionMessage);
      sbMailMsgEnd.append(" \n\n");
      sbMailMsgEnd.append(strPolicyMessage);
      sbMailMsgEnd.append(" \n\n");
      sbMailMsgEnd.append(strPoQueryMessage);
      strEmailId = strEmailId;
    }

    try {
      hmPODetails =
          GmCommonClass.parseNullHashMap(listPOReminderOrder(strInputStr, strUserId, "POReminder"));
      alReturn = GmCommonClass.parseNullArrayList((ArrayList) hmPODetails.get("alData"));
      intArLength = alReturn.size();

      if (intArLength > 0) {
        HashMap hmTempLoop = new HashMap();
        HashMap hmLoop = new HashMap();
        String strNextId = "";
        int intCount = 0;
        boolean blFlag = false;

        for (int i = 0; i < intArLength; i++) {
          hmLoop = (HashMap) alReturn.get(i);
          if (i < intArLength - 1) {
            hmTempLoop = (HashMap) alReturn.get(i + 1);
            strNextId = GmCommonClass.parseNull((String) hmTempLoop.get("RID"));
          }
          strRepId = GmCommonClass.parseNull((String) hmLoop.get("RID"));
          strOrderId = GmCommonClass.parseNull((String) hmLoop.get("ORDID"));
          strAccName = GmCommonClass.parseNull((String) hmLoop.get("ANAME"));
          dtOrderDt = (java.util.Date) hmLoop.get("ORDDT");
          strOrderAmt =
              GmCommonClass.getStringWithCommas(GmCommonClass.parseNull((String) hmLoop
                  .get("ORDAMT")));
          strRepName = GmCommonClass.parseNull((String) hmLoop.get("RNAME"));
          strDistNm = GmCommonClass.parseNull((String) hmLoop.get("DNAME"));
          strDispNm = strRepName.equals(strDistNm) ? "" : " : " + strRepName;
          strEmail = GmCommonClass.parseNull((String) hmLoop.get("EMAIL"));
          strDefaultEmail = GmCommonClass.parseNull((String) hmLoop.get("POREVENUEEMAIL"));
          log.debug("strEmail: " + i + " " + strEmail);
          
          strMailIds = strEmail.equals("") ? strDefaultEmail : strEmail;
          // strMailIds = strEmail.equals("")?"Linda Mohn<lmohn@globusmedical.com>":strEmail;

          // Saving an entry in the Log table
          if (strEmailType.equals("RevenueEmail")) {
            gmCom.saveLog(strOrderId, "Email Reminder sent for Revenue Recognition", strUserId,
                "53026");
          } else {
            gmCom.saveLog(strOrderId, "Email Reminder sent for PO", strUserId, "1200");

          }
          sbOrdDetails.append("\n");
          sbOrdDetails.append(strOrderId);
          sbOrdDetails.append("   ");
          sbOrdDetails.append(strAccName);
          sbOrdDetails.append("   ");
          sbOrdDetails.append(dtOrderDt);
          sbOrdDetails.append("   ");
          sbOrdDetails.append(strCurrSymbol);
          sbOrdDetails.append(" ");
          sbOrdDetails.append(strOrderAmt);
          sbOrdDetails.append("   ");

          if (strRepId.equals(strNextId)) {
            intCount++;
          } else {
            blFlag = true;
          }

          if (blFlag) {
            emailProps = new GmEmailProperties();
            strSubject = strSubjectMsg + strDistNm.concat(strDispNm);
            strMimeType = "text/html";
            emailProps.setMimeType(strMimeType);
            emailProps.setSender(strEmailId);
            emailProps.setRecipients(strMailIds);
            emailProps.setCc(strCCMailIds);
            emailProps.setSubject(strSubject);

            sbMailMsg.append(sbMailMsgStart.toString());
            sbMailMsg.append("\n\n");
            sbMailMsg.append(sbOrdDetails);
            sbMailMsg.append(sbMailMsgEnd.toString());
            hmJasperPerams.put("EMAILTYPE", strEmailType);
            hmJasperPerams.put("EMAILBODY", sbMailMsg.toString());
            jasperMail.setJasperReportName(strJasperName);
            jasperMail.setAdditionalParams(hmJasperPerams);
            jasperMail.setReportData(null);
            jasperMail.setEmailProperties(emailProps);
            hmReturnDetails = jasperMail.sendMail();
            strSubject = "";// Subject was showing multiple times, so need to remove previous value
            sbMailMsg.setLength(0);
            sbOrdDetails.setLength(0);
            blFlag = false;
          }
        }

        if (!blFlag && intCount > 0) {
          emailProps = new GmEmailProperties();
          strSubject = strSubjectMsg + strDistNm.concat(strDispNm);
          strMimeType = "text/html";
          emailProps.setMimeType(strMimeType);
          emailProps.setSender(strEmailId);
          emailProps.setRecipients(strMailIds);
          emailProps.setCc(strCCMailIds);
          emailProps.setSubject(strSubject);

          sbMailMsg.append(sbMailMsgStart.toString());
          sbMailMsg.append("\n\n");
          sbMailMsg.append(sbOrdDetails);
          sbMailMsg.append(sbMailMsgEnd.toString());
          hmJasperPerams.put("EMAILTYPE", strEmailType);
          hmJasperPerams.put("EMAILBODY", sbMailMsg.toString());
          jasperMail.setJasperReportName(strJasperName);
          jasperMail.setAdditionalParams(hmJasperPerams);
          jasperMail.setReportData(null);
          jasperMail.setEmailProperties(emailProps);
          hmReturnDetails = jasperMail.sendMail();
          strSubject = "";
          sbMailMsg.setLength(0);
          sbOrdDetails.setLength(0);
        }
      }
    } catch (Exception e) {
      GmLogError.log("Exception in GmAccountsMiscBean:sendEmailReminderPO", "Exception is:" + e);
      throw new AppError(e);
    }
    return hmReturn;
  } // end of sendEmailReminderPO

  /**
   * listPOReminderOrder - This method will get order details for the input order ID string -
   * strInputOrderIds.
   * 
   * @param strInputOrderIds
   * @param strUserId
   * @param strFromPage
   * @return HashMap
   * @throws AppError
   */
  public HashMap listPOReminderOrder(String strInputOrderIds, String strUserId, String strFromPage)
      throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alPOResult = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();
    String strOrderTotal = "";
    String strCompanyId = getGmDataStoreVO().getCmpid();

    HashMap hmCurrency = new HashMap();
    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());

    String strCurrency = (String) hmCurrency.get("CMPCURRSMB");
    String strDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strRevenueEmail =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("POREVENUEEMAIL",
            "POREVENUEDEFAULT", strCompanyId));

    sbQuery.append(" SELECT t501.*, '");
    sbQuery.append(strCurrency);
    sbQuery.append("', SUM (t501.ORDAMT) OVER () ORDER_TOTAL  FROM (");
    sbQuery.append(" SELECT A.C501_ORDER_ID ORDID, B.C703_SALES_REP_NAME RNAME, ");
    sbQuery
        .append(" GM_PKG_CM_CONTACT.GET_CONTACT_VALUE (B.C101_PARTY_ID, 90452) EMAIL, B.C703_SALES_REP_ID RID, GET_ACCOUNT_NAME(A.C704_ACCOUNT_ID) ANAME,");
    sbQuery.append(" A.C501_ORDER_DATE ORDDT, ");
    sbQuery.append("DECODE(T502_REBATE.REB_FL,'N', GET_TOTAL_DO_AMT(A.C501_ORDER_ID, ''),GET_TOTAL_EMAIL_DO_AMT (A.C501_ORDER_ID))ORDAMT ");
    sbQuery
        .append(" ,get_distributor_name(B.C701_DISTRIBUTOR_ID) DNAME ,NVL(A.c501_customer_po,'') CUSTPO ,");
    sbQuery.append("GET_USER_EMAILID(");
    sbQuery.append(strUserId);
    sbQuery.append(") SENDEREMAIL, '");
    sbQuery.append(strRevenueEmail);
    sbQuery.append("' POREVENUEEMAIL FROM T501_ORDER A, T703_SALES_REP B,");
    sbQuery.append("(SELECT DISTINCT T502.C501_ORDER_ID,");
    sbQuery.append(" DECODE(NVL(T502.C7200_REBATE_RATE,0),0,'N','Y') REB_FL FROM T502_ITEM_ORDER T502 ");
    sbQuery.append(" WHERE T502.C501_ORDER_ID IN (");
    sbQuery.append(strInputOrderIds);
    sbQuery.append(" ) AND T502.C502_VOID_FL IS NULL");
    sbQuery.append(")T502_REBATE");
    sbQuery.append(" WHERE A.C501_ORDER_ID IN (");
    sbQuery.append(strInputOrderIds);
    sbQuery.append(") AND A.C703_SALES_REP_ID = B.C703_SALES_REP_ID");
    sbQuery.append("  AND A.C501_ORDER_ID = T502_REBATE.C501_ORDER_ID (+)");
    sbQuery.append(" AND A.c1900_company_id = ");
    sbQuery.append("'" + strCompanyId + "'");
    sbQuery.append(" ORDER BY  B.C703_SALES_REP_ID, UPPER(ANAME) , A.C501_ORDER_DATE ASC ");
    sbQuery.append(" )T501 ");

    log.debug("EMAIL QUERY -> listPOReminderOrder --> " + sbQuery.toString());

    alPOResult =
        GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
    gmDBManager.close();

    int alSize = alPOResult.size();
    // This above query will contains the order total and it will be available in all the rows.
    // so taking order total value by default from the first row.
    if (alSize > 0) {
      hmTemp = GmCommonClass.parseNullHashMap((HashMap) alPOResult.get(0));
      strOrderTotal = GmCommonClass.parseNull((String) hmTemp.get("ORDER_TOTAL"));
    }
    hmReturn.put("alData", alPOResult);
    hmReturn.put("ORDER_TOTAL", strOrderTotal);
    hmReturn.put("CURRENCY", strCurrency);

    log.debug(" strOrderTotal is " + strOrderTotal);
    return hmReturn;
  } // end of listPOReminerOrder

  /**
   * fetchCommissionEmailInfo - This method will get all the required information that is needed to
   * be populated in the commission email composition page.
   * 
   * @param strInputOrderIds
   * @param strAction
   * @param strToDate
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchCommissionEmailInfo(HashMap hmCommissionParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    String strSubject = "";
    String strMessage = "";
    String strCcMailIds = "";
    String strToMailIds = "";
    String strRepNm = "";
    String strDueDt = "";
    String strFmtDueDt = "";
    String strFmtToDt = "";
    String strFmtToMonth = "";
    String strInputOrderIds = "";
    String strAction = "";
    String strToDate = "";
    String strReportType = "";
    String strDueDate = "";

    strInputOrderIds = GmCommonClass.parseNull((String) hmCommissionParam.get("INPUTSTR"));
    strAction = GmCommonClass.parseNull((String) hmCommissionParam.get("EMAILACTION"));
    strToDate = GmCommonClass.parseNull((String) hmCommissionParam.get("TODATE"));
    strReportType = GmCommonClass.parseNull((String) hmCommissionParam.get("REPORTTYPE"));
    strDueDate = GmCommonClass.parseNull((String) hmCommissionParam.get("DUEDATE"));

    log.debug("fetchCommissionEmailInfo=strInputOrderIds== " + strInputOrderIds + "=strAction="
        + strAction + "=strToDate=" + strToDate + "=strReportType=" + strReportType
        + " strDueDat= " + strDueDate);
    gmDBManager.setPrepareString("gm_pkg_ar_accounts_rpt.gm_fch_commsn_email", 14);
    gmDBManager.setString(1, strInputOrderIds);
    gmDBManager.setString(2, strAction);
    gmDBManager.setString(3, strToDate);
    gmDBManager.setString(4, strReportType);
    gmDBManager.setString(5, strDueDate);
    gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(10, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(11, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(12, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(13, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(14, OracleTypes.VARCHAR);
    gmDBManager.execute();

    strSubject = GmCommonClass.parseNull(gmDBManager.getString(6));
    strMessage = GmCommonClass.parseNull(gmDBManager.getString(7));
    strCcMailIds = GmCommonClass.parseNull(gmDBManager.getString(8));
    strToMailIds = GmCommonClass.parseNull(gmDBManager.getString(9));
    strRepNm = GmCommonClass.parseNull(gmDBManager.getString(10));
    strDueDt = GmCommonClass.parseNull(gmDBManager.getString(11));
    strFmtToDt = GmCommonClass.parseNull(gmDBManager.getString(12));
    strFmtDueDt = GmCommonClass.parseNull(gmDBManager.getString(13));
    strFmtToMonth = GmCommonClass.parseNull(gmDBManager.getString(14));

    gmDBManager.close();

    hmReturn.put("EMAILSUBJECT", strSubject);
    hmReturn.put("EMAILMESSAGE", strMessage);
    hmReturn.put("EMAILCC", strCcMailIds);
    hmReturn.put("EMAILTO", strToMailIds);
    hmReturn.put("REPNAME", strRepNm);
    hmReturn.put("DUEDATE", strDueDt);
    hmReturn.put("DUEDATEFMT", strFmtDueDt);
    hmReturn.put("TODATEFMT", strFmtToDt);
    hmReturn.put("TOMONTHFMT", strFmtToMonth);

    log.debug("fetchCommissionEmailInfo========" + hmReturn);
    return hmReturn;
  }// end of fetchCommissionEmailInfo

  /**
   * fetchCommissionEmailInfo - This method will get all the required CC List information that is
   * needed to be populated in the commission email composition page.
   * 
   * @param strInputOrderIds
   * @return
   * @throws AppError
   */
  public String fetchCollectorEmail(String strInputOrderIds) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCollectorMailId = "";
    gmDBManager.setPrepareString("gm_pkg_ar_accounts_rpt.gm_fch_collector_email", 2);
    gmDBManager.setString(1, strInputOrderIds);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strCollectorMailId = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();

    log.debug(" strCollectorMailId is -->>" + strCollectorMailId);
    return strCollectorMailId;
  }// end of fetchCommissionEmailInfo

}// end of class GmAccountsMiscBean
