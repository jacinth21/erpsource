/*
 * Bean: GmQuestionBean.java Author: Richard Project: Globus Medical App Date-Written: 31 Oct 2005
 * Security: Unclassified Description: This Bean will be used to store the Question Fetch
 * Information
 * 
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.accounts.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;



public class GmAccountingReportBean extends GmBean {
  /**
   * GmAccountingReportBean - This Bean gets all account releated reports
   * 
   **/
  public GmAccountingReportBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAccountingReportBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // Below 2 methos need to be removed
  /*
   * public static void main(String[] args) throws Exception { GmAccountingReportBean t = new
   * GmAccountingReportBean (); } public GmAccountingReportBean () throws Exception {
   * AccountRollForwardReport("1","1","1"); }
   */

  /**
   * AccountRollForwardReport - This method fecths all the Account information for the selected
   * Account Element List
   * 
   * @param String strAccountElementList
   * @param String strFormDate
   * @param String strToDate
   * @return HashMap
   * @exception AppError
   **/
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public HashMap AccountRollForwardReport(HashMap hmParam) throws AppError, Exception {
    HashMap hmResult = new HashMap();
    ArrayList alOpenBalance = new ArrayList();
    ArrayList alDetailList = new ArrayList();
    ArrayList alAddtnlOpenBalance = new ArrayList();
    ArrayList alApprxmateBalance = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strAccountElementList = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTLIST"));
    String strFormDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strPlantID = GmCommonClass.parseZero((String) hmParam.get("PLANTID"));
    String strCntxtCompanyId = getGmDataStoreVO().getCmpid();
    String strDateFormat = getCompDateFmt();
    String strArchiveFl = GmCommonClass.parseNull((String) hmParam.get("ARCHIVEFL"));

    String strApprxDate = "";
    long diffDays = 0;
    Date dtApprxDate = null;
    Date dtFromDate = null;



    log.debug("strPartyId " + strPartyId);
  //PC-4148 Remove Try Catch for Email Exception (TSK-23800)

      StringBuffer sbQuery = new StringBuffer();

      if (strAccountElementList.equals("") || strFormDate.equals("") || strToDate.equals("")) {
        hmResult.put("OPEN_BALANCE", alOpenBalance);
        hmResult.put("DETAIL_LIST", alDetailList);
        return hmResult;
      }

      // Query to fetch Approximate Date
      strApprxDate = fetchApprovedDate(hmParam);

      dtFromDate = GmCommonClass.getStringToDate(strFormDate, strDateFormat);
      dtApprxDate = GmCommonClass.getStringToDate(strApprxDate, strDateFormat);


      hmParam.put("apprmaxdate", strApprxDate);
      if (dtApprxDate != null) {
        long diff = Math.abs(dtFromDate.getTime() - dtApprxDate.getTime());
        diffDays = diff / (24 * 60 * 60 * 1000);
      }

      if (diffDays == 0) {
        // Query to Fetch the Account Opening Balance
        alOpenBalance = GmCommonClass.parseNullArrayList(fetchOpeningBalance(hmParam));
        // Query to Fect the detail Account transaction information
        // for the selected period
        alDetailList = GmCommonClass.parseNullArrayList(fetchRollforwardDtls(hmParam));



      } else if (diffDays > 0) {
        // Query to Fetch the Account Opening Balance
        alApprxmateBalance = GmCommonClass.parseNullArrayList(fetchOpeningBalance(hmParam));
        alAddtnlOpenBalance = GmCommonClass.parseNullArrayList(fetchOpeningBalanceAddtnl(hmParam));
        alOpenBalance =
            GmCommonClass.parseNullArrayList(fetchTotOpeningBal(alApprxmateBalance,
                alAddtnlOpenBalance));
        // Query to Fect the detail Account transaction information
        // for the selected period
        alDetailList = GmCommonClass.parseNullArrayList(fetchRollforwardDtls(hmParam));
      }
      hmResult.put("OPEN_BALANCE", alOpenBalance);
      hmResult.put("DETAIL_LIST", alDetailList);

    return hmResult;
  }

  /**
   * fetchReportDistributorList - This method will
   * 
   * @return RowSetDynaClass
   * @exception AppError
   **/

  public RowSetDynaClass AccountRevenueSamplingReport(HashMap hmParam) throws AppError {
    RowSetDynaClass rdReturn = null;
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strAccountID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strRegionID = GmCommonClass.parseNull((String) hmParam.get("REGIONID"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));

    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("DATEFORMAT"));
    String strSalesFilterCond = GmCommonClass.parseNull((String) hmParam.get("SALES_CON"));

    Date dtOrderFromDT = (Date) hmParam.get("DTORDERFROMDT");
    Date dtOrderToDT = (Date) hmParam.get("DTORDERTODT");
    Date dtStatusFromDT = (Date) hmParam.get("DTSTATUSFROMDT");
    Date dtStatusToDT = (Date) hmParam.get("DTSTATUSTODT");

    String strOrderFromDT = GmCommonClass.getStringFromDate(dtOrderFromDT, strDateFmt);
    String strOrderToDT = GmCommonClass.getStringFromDate(dtOrderToDT, strDateFmt);
    String strStatusFromDT = GmCommonClass.getStringFromDate(dtStatusFromDT, strDateFmt);
    String strStatusToDT = GmCommonClass.getStringFromDate(dtStatusToDT, strDateFmt);
    String strParentFl = GmCommonClass.parseNull((String) hmParam.get("CHK_ACTIVEFL"));
    String strARCurrSymbol = GmCommonClass.parseNull((String) hmParam.get("ARCURRSYMBOL"));
    String strOrdDateType = GmCommonClass.parseZero((String) hmParam.get("STRORDDATETYPE"));

    sbQuery
        .append(" SELECT t501.c704_account_id accid, DECODE('"
            + getComplangid()
            + "','103097',NVL(C704_ACCOUNT_NM_EN,C704_ACCOUNT_NM),C704_ACCOUNT_NM) acctname, t501.c501_order_id ordid,T501.C501_DO_DOC_UPLOAD_FL DO_FLAG ");
    sbQuery
        .append("  , DECODE('"
            + getComplangid()
            + "','103097',NVL(t703.c703_sales_rep_name_en,t703.c703_sales_rep_name),t703.c703_sales_rep_name) repname,  t501.c501_order_date orddt , t501.c501_surgery_date surgerydt ");
    sbQuery
        .append("  , get_party_name(t501.c101_dealer_id) dealername,    get_code_name(t704.c901_account_type) acct_type ");
    // Remove t501a_order_attribute and Add t5003_order_revenue_sample_dtls For PMT-53711
    sbQuery
        .append(" , DECODE('"
            + getComplangid()
            + "','103097',NVL(t701.c701_distributor_name_en,t701.c701_distributor_name),t701.c701_distributor_name) dname, v700.region_name regionnm, NVL(get_code_name(t5003.c901_po_dtls),'') hardcopy_po,NVL(get_code_name(t5003.c901_do_dtls),'') signed_do ");
    sbQuery.append("  ,NVL( NVL(t5003.c5003_po_last_updated_date,t5003.c5003_po_created_date),NVL(t5003.c5003_do_last_updated_date,t5003.c5003_do_created_date)) REVENUE_RECOG_DT, '' checkfl , rev_amt.total totdoamount ");
    
    sbQuery .append(" ,NVL( NVL(t5003.c5003_po_last_updated_date,t5003.c5003_po_created_date),NVL(t5003.c5003_do_last_updated_date,t5003.c5003_do_created_date)) UPDATED_DATE");
    /*
     * sbQuery .append(
     * "DECODE (decode(t704d.C901_ADMIN_FLAG,7003,GET_ACCOUNT_GPO_ID (t501.C704_ACCOUNT_ID),7011,nvl(t704d.c101_gpo,0),0),0, GET_TOTAL_DO_AMT(t501.C501_ORDER_ID, ''),"
     * ); sbQuery .append(
     * "DECODE(t704d.C704D_ADMIN_FEE,NULL,GET_TOTAL_DO_AMT (t501.C501_ORDER_ID, ''), GET_TOTAL_EMAIL_DO_AMT (t501.C501_ORDER_ID))) totdoamount, '' checkfl "
     * );
     */

    sbQuery.append(" ,t503.c503_invoice_id INVOICEID, t503.c503_invoice_date INVDATE");
    sbQuery
        .append(" ,NVL(t503.C503_INV_PYMNT_AMT,0) RECEIVEDAMT, DECODE(t503.c503_status_fl,2,'Closed','Pending') SFL ");
    sbQuery.append(", get_code_name(t501.c501_receive_mode) MODE_OF_ORDER ");
    sbQuery
        .append(", get_code_name(t501.c901_contract) CONTRACT , t501.c501_customer_po CUSTPO, t501.c501_po_amount POAMT");
    sbQuery.append(" FROM   t501_order t501 ,t704_account t704 ");
    sbQuery.append(" , t5003_order_revenue_sample_dtls t5003 ");
    sbQuery.append(" , t703_sales_rep t703 ");
    sbQuery.append(" , t701_distributor t701 ");
    sbQuery
        .append(" , (SELECT DISTINCT ad_id, ad_name, region_id, region_name, d_id, d_name, rep_id ");
    sbQuery.append(" FROM v700_territory_mapping_detail where rep_compid ='" + getCompId()
        + "' ) v700, t503_invoice t503 ");
    // to get the revenue sampling amount
    sbQuery
        .append(" , (SELECT CASE WHEN t501.c501_parent_order_id IS NULL THEN t501.c501_order_id  ELSE t501.c501_parent_order_id END order_id, SUM (t501.c501_total_cost) total ");
    sbQuery.append("           FROM t501_order t501");
    sbQuery
        .append("  WHERE NVL (t501.c901_order_type, -9999) NOT IN (SELECT C906_RULE_VALUE FROM v901_ORDER_TYPE_GRP) ");
    // to exclude the Sales Discount Orders (PMT-20705)
    sbQuery.append(" AND NVL(t501.c901_order_type,0) <> 2535 ");
    sbQuery.append(" AND T501.c1900_company_id = '" + getCompId() + "'");
    sbQuery.append("            AND t501.c501_void_fl IS NULL");
    sbQuery.append("            AND EXISTS");
    sbQuery.append("            (");
    sbQuery.append("                 SELECT NVL (t501e.c501_parent_order_id, t501e.c501_order_id)");
    sbQuery.append("                   FROM t501_order t501e");
    sbQuery.append("                  WHERE t501e.c501_void_fl IS NULL");
    sbQuery.append("  AND t501e.c501_delete_fl IS NULL ");
    sbQuery.append(" AND t501e.c1900_company_id = '" + getCompId() + "'");
    sbQuery
        .append("  AND NVL (t501e.c901_order_type, -9999) NOT IN (SELECT c906_rule_value FROM v901_order_type_grp) ");
    sbQuery.append(" AND t501.c501_order_id = t501e.c501_order_id ");
    if (!strOrderFromDT.equals("") && !strOrderToDT.equals("") && !strDateFmt.equals("")) {
      if (strOrdDateType.equals("106480")) { // based on the order date
        sbQuery.append(" AND  t501e.c501_order_date   ");
        sbQuery.append(" >= TO_DATE('");
        sbQuery.append(strOrderFromDT);
        sbQuery.append("' , '");
        sbQuery.append(strDateFmt);
        sbQuery.append("') ");
        sbQuery.append(" AND t501e.c501_order_date   ");
        sbQuery.append("  <= TO_DATE('");
        sbQuery.append(strOrderToDT);
        sbQuery.append("' , '");
        sbQuery.append(strDateFmt);
        sbQuery.append("') ");
      } else if (strOrdDateType.equals("106481")) { // based on the surgery date
        sbQuery.append(" AND  t501e.c501_surgery_date   ");
        sbQuery.append(" >= TO_DATE('");
        sbQuery.append(strOrderFromDT);
        sbQuery.append("' , '");
        sbQuery.append(strDateFmt);
        sbQuery.append("') ");
        sbQuery.append(" AND  t501e.c501_surgery_date   ");
        sbQuery.append("  <= TO_DATE('");
        sbQuery.append(strOrderToDT);
        sbQuery.append("' , '");
        sbQuery.append(strDateFmt);
        sbQuery.append("') ");
      }
    }
    sbQuery.append(" )");
    sbQuery
        .append(" GROUP BY CASE WHEN t501.c501_parent_order_id IS NULL THEN t501.c501_order_id  ELSE t501.c501_parent_order_id END ");
    sbQuery.append(" )");
    sbQuery.append(" rev_amt ");


    sbQuery.append("  WHERE t501.c703_sales_rep_id = t703.c703_sales_rep_id ");
    // if show parent account is checked ,all rep accounts related information will going to show.
    if ((!strAccountID.equals("")) && (!strAccountID.equals("0")) && !strParentFl.equals("")) {
      sbQuery
          .append(" AND T704.c101_party_id = (select c101_party_id from t704_account where C704_ACCOUNT_ID ="
              + strAccountID + ") ");
    }
    // Filtering report based on currency symbol.
    sbQuery.append("AND t704.C901_CURRENCY = '" + strARCurrSymbol + "'");

    sbQuery.append("AND T501.c1900_company_id = '" + getCompId() + "'");
    sbQuery.append(" AND T501.C704_ACCOUNT_ID = t704.C704_ACCOUNT_ID ");
    sbQuery.append(" AND t703.c701_distributor_id = t701.c701_distributor_id ");
    sbQuery.append(" AND t501.c703_sales_rep_id = v700.rep_id ");
    sbQuery.append(" AND t501.c501_order_id = t5003.c501_order_id (+)");
    sbQuery.append(" AND t501.c503_invoice_id = t503.c503_invoice_id(+) ");
    sbQuery.append(" AND t501.c501_order_id         = rev_amt.order_id ");
    sbQuery.append("  AND t501.c501_parent_order_id IS NULL ");
    sbQuery.append(" AND t501.c501_void_fl IS NULL ");
    sbQuery.append(" AND t701.c701_void_fl IS NULL ");
    sbQuery.append(" AND t703.c703_void_fl IS NULL ");
    sbQuery.append(" AND t501.c501_delete_fl IS NULL ");
    sbQuery.append(" AND t5003.c5003_void_fl(+) IS NULL");
    sbQuery.append(" AND (t701.c901_ext_country_id is NULL ");
    sbQuery.append(" OR t701.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append(" AND (t703.c901_ext_country_id is NULL ");
    sbQuery.append(" OR t703.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append(" AND (t501.c901_ext_country_id is NULL ");
    sbQuery.append(" OR t501.c901_ext_country_id  in (select country_id from v901_country_codes))");
    // to exclude the Sales Discount Orders
    sbQuery.append(" AND NVL(t501.c901_order_type,0) <> 2535 ");
    sbQuery
        .append(" AND NVL (t501.c901_order_type, -9999) NOT IN (SELECT C906_RULE_VALUE FROM v901_ORDER_TYPE_GRP) ");

    if (!strOrderFromDT.equals("") && !strOrderToDT.equals("") && !strDateFmt.equals("")) {
      // Our code ()
      sbQuery.append("    AND EXISTS ");
      sbQuery.append("( ");
      sbQuery.append(" SELECT NVL (t501e.c501_parent_order_id , t501e.c501_order_id) ");
      sbQuery.append(" FROM t501_order t501e ");
      sbQuery.append(" WHERE t501e.c501_void_fl    IS NULL ");
      sbQuery.append(" AND t501e.c501_delete_fl IS NULL ");
      sbQuery.append(" AND t501e.c1900_company_id = '" + getCompId() + "'");
      sbQuery
          .append(" AND NVL (t501e.c901_order_type, -9999) NOT IN (SELECT C906_RULE_VALUE FROM v901_ORDER_TYPE_GRP) ");
      sbQuery.append(" AND NVL (t501e.c901_order_type, 0)          <> 2535 ");
      sbQuery
          .append(" AND t501.c501_order_id     = NVL (t501e.c501_parent_order_id, t501e.c501_order_id) ");
      if (strOrdDateType.equals("106480")) { // based on the order date
        sbQuery.append(" AND  t501e.c501_order_date   ");
        sbQuery.append(" >= TO_DATE('");
        sbQuery.append(strOrderFromDT);
        sbQuery.append("' , '");
        sbQuery.append(strDateFmt);
        sbQuery.append("') ");
        sbQuery.append(" AND t501e.c501_order_date   ");
        sbQuery.append("  <= TO_DATE('");
        sbQuery.append(strOrderToDT);
        sbQuery.append("' , '");
        sbQuery.append(strDateFmt);
        sbQuery.append("') ");
      } else if (strOrdDateType.equals("106481")) { // based on the surgery date
        sbQuery.append(" AND  t501e.c501_surgery_date   ");
        sbQuery.append(" >= TO_DATE('");
        sbQuery.append(strOrderFromDT);
        sbQuery.append("' , '");
        sbQuery.append(strDateFmt);
        sbQuery.append("') ");
        sbQuery.append(" AND t501e.c501_surgery_date   ");
        sbQuery.append("  <= TO_DATE('");
        sbQuery.append(strOrderToDT);
        sbQuery.append("' , '");
        sbQuery.append(strDateFmt);
        sbQuery.append("') ");
      }
      sbQuery.append("  ) ");
    }


    if (!strStatusFromDT.equals("") && !strStatusToDT.equals("") && !strDateFmt.equals("")) {

      sbQuery.append(" AND trunc(t5003.c5003_updated_date)   ");
      sbQuery.append(" >= TO_DATE('");
      sbQuery.append(strStatusFromDT);
      sbQuery.append("' , '");
      sbQuery.append(strDateFmt);
      sbQuery.append("') ");
      sbQuery.append(" AND trunc(t5003.c5003_updated_date)   ");
      sbQuery.append("  <= TO_DATE('");
      sbQuery.append(strStatusToDT);
      sbQuery.append("' , '");
      sbQuery.append(strDateFmt);
      sbQuery.append("') ");

    }


    if ((!strAccountID.equals("")) && (!strAccountID.equals("0")) && (strParentFl.equals(""))) {
      sbQuery.append(" AND T501.C704_ACCOUNT_ID = '");
      sbQuery.append(strAccountID);
      sbQuery.append("'");
    }

    if (!strRegionID.equals("") && !strRegionID.equals("0")) {
      sbQuery.append(" AND v700.region_id = '");
      sbQuery.append(strRegionID);
      sbQuery.append("'");
    }

    if (!strRepID.equals("") && !strRepID.equals("0")) {
      sbQuery.append(" AND t501.C703_SALES_REP_ID = '");
      sbQuery.append(strRepID);
      sbQuery.append("'");
    }

    if (!strStatus.equals("") && !strStatus.equals("0")) {
      sbQuery.append(" AND (t5003.c901_po_dtls ='");
      sbQuery.append(strStatus);
      sbQuery.append("'");
      sbQuery.append(" OR t5003.c901_do_dtls ='");
      sbQuery.append(strStatus);
      sbQuery.append("')");
    }
    if (!strSalesFilterCond.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strSalesFilterCond);
    }
    log.debug("AccountRevenueSamplingReport-- sbQuery " + sbQuery.toString());
    rdReturn = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return rdReturn;
  }

  /**
   * fetchOpeningBalance - This method will return opening balance of before approximate date.
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList fetchOpeningBalance(HashMap hmParam) throws AppError {

    HashMap hmResult = new HashMap();
    ArrayList alOpenBalance = new ArrayList();
    String strAccountElementList = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTLIST"));
    strAccountElementList =
        strAccountElementList.substring(0, strAccountElementList.lastIndexOf(","));
    String strFormDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPlantId = GmCommonClass.parseZero((String) hmParam.get("PLANTID"));
    String strCntxtCompanyId = getGmDataStoreVO().getCmpid();
    String strDateFormat = getCompDateFmt();
    String strApprxDate = GmCommonClass.parseNull((String) hmParam.get("apprmaxdate"));
    String strOwnerCompanyId = GmCommonClass.parseZero((String) hmParam.get("OWNER_COMP_ID"));
    String strOwnerCurrency = GmCommonClass.parseZero((String) hmParam.get("OWNER_CURR_ID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT     T801.ID, T801.NAME, NVL(t810r.AMT,0) AMT, NVL(t810r.local_amt, 0) local_amt ");
    sbQuery.append(" FROM ");
    sbQuery.append(" (SELECT T801.C801_ACCOUNT_ELEMENT_ID ID, T801.C801_NAME NAME ");
    sbQuery.append(" FROM T801_ACCOUNT_ELEMENT T801 ");
    sbQuery.append(" WHERE T801.C801_ACCOUNT_ELEMENT_ID IN (");
    sbQuery.append(strAccountElementList);
    sbQuery.append(") ) T801, (SELECT     t810r.C801_ACCOUNT_ELEMENT_ID ID,");
    sbQuery.append(" NVL (SUM (t810r.c810_local_company_amt), 0 ) local_amt ");
    sbQuery
        .append(", NVL (SUM (t810r.C810_AMT), 0) AMT FROM t810_Rollforward t810r WHERE t810r.C801_ACCOUNT_ELEMENT_ID IN (");
    sbQuery.append(strAccountElementList);
    sbQuery.append(") AND t810r.c1900_company_id ='" + getCompId() + "'");
    if (!strPlantId.equals("0")) {
      sbQuery.append(" AND t810r.c5040_plant_id = '" + strPlantId + "' ");
    }
    if (!strOwnerCompanyId.equals("0")) {
      sbQuery.append(" AND t810r.c1900_owner_company_id = '" + strOwnerCompanyId + "' ");
    }
    if (!strOwnerCurrency.equals("0")) {
      sbQuery.append(" AND t810r.c901_owner_currency = '" + strOwnerCurrency + "' ");
    }
    sbQuery.append(" AND t810r.c810_txn_date <= to_Date('" + strApprxDate + "','" + strDateFormat
        + "')");
    sbQuery.append(" AND t810r.C810_VOID_FL IS NULL ");
    sbQuery.append(" GROUP BY t810r.C801_ACCOUNT_ELEMENT_ID) t810r");
    sbQuery.append(" WHERE T801.ID = t810r.ID(+) ");
    sbQuery.append(" ORDER BY NAME ");



    log.debug(" fetchOpeningBalance query is " + sbQuery.toString());
    alOpenBalance = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alOpenBalance;
  }

  /**
   * fetchRollforwardDtls - This method will return roll forward details report data
   * 
   * @return ArrayList
   * @exception AppError
   **/

  public ArrayList fetchRollforwardDtls(HashMap hmParam) throws AppError {

    HashMap hmResult = new HashMap();
    HashMap hmApprxDate = new HashMap();
    ArrayList alDetailList = new ArrayList();


    String strAccountElementList = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTLIST"));
    String strFormDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strPlantId = GmCommonClass.parseZero((String) hmParam.get("PLANTID"));
    String strCntxtCompanyId = getGmDataStoreVO().getCmpid();
    String strDateFormat = getCompDateFmt();
    String strArchiveFl = GmCommonClass.parseNull((String) hmParam.get("ARCHIVEFL"));
    String strOwnerCompanyId = GmCommonClass.parseZero((String) hmParam.get("OWNER_COMP_ID"));
    String strOwnerCurrency = GmCommonClass.parseZero((String) hmParam.get("OWNER_CURR_ID"));
    String strDivisionId = GmCommonClass.parseZero((String) hmParam.get("DIVISION"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT  t810.C801_ACCOUNT_ELEMENT_ID ");
    //sbQuery.append(" ,GET_ACCTELEMENT_DESC(t810.C801_ACCOUNT_ELEMENT_ID) A_NAME ");
    sbQuery.append(" ,T801.C801_NAME  A_NAME ");
    sbQuery.append(" ,t810.C901_ACCOUNT_TXN_TYPE TXN_ID");
    sbQuery.append(" ,GET_CODE_NAME(t810.C901_ACCOUNT_TXN_TYPE)  TXN_NAME   ");
    sbQuery.append(" ,SUM( NVL(t810.C810_QTY,0) * NVL(t810.C810_DR_AMT,0) ) - ");
    sbQuery.append("      SUM( NVL(t810.C810_QTY,0) * NVL(t810.C810_CR_AMT, 0)  ) AMT, ");
    // From Section starts...

    sbQuery
        .append(" NVL (SUM (c810_qty * C810_LOCAL_COMPANY_DR_AMT) - SUM (c810_qty   *  C810_LOCAL_COMPANY_CR_AMT), 0) LOCAL_AMT ");
    sbQuery
        .append(", T1900.C1900_COMPANY_NAME  owner_company, get_code_name_alt(c901_owner_currency) owner_currency ");
    sbQuery
        .append(" ,get_code_name_alt(c901_local_company_currency) local_currency , c1900_owner_company_id owner_company_id ");
    sbQuery
        .append(" , decode(MAX(c901_owner_currency) OVER (), MIN(c901_owner_currency) OVER (),1,2) CUR_CNT ,T806.C806_SAGE_ACC_ID SAGE_ACC_ID ");
    sbQuery.append(" ,T1900_txn_company.c1900_company_name txn_company_nm ");
    sbQuery.append(" ,t810.c1910_division_name  DIVISION_NAME,  t810.c1910_division_id DIVISION_ID ");
    // Query to Fetch the Answer Information
    if (strArchiveFl.equals("")) {
      sbQuery.append(" FROM T810_POSTING_TXN t810 ");
    } else {
      sbQuery.append(" FROM t810_posting_txn_archive t810 ");
    }
    //We added table T1900_COMPANY  T1900_txn_company for PMT-51247 to fetch transaction company name
    sbQuery
    .append(",  T801_ACCOUNT_ELEMENT T801, T1900_COMPANY T1900, T806_GL_ACCOUNT_SAGE_ACC_MAPPING T806, T1900_COMPANY  T1900_txn_company ");
    // *************************************
    sbQuery.append(" WHERE t810.C801_ACCOUNT_ELEMENT_ID IN ( ");
    strAccountElementList =
        strAccountElementList.substring(0, strAccountElementList.lastIndexOf(","));
    sbQuery.append(strAccountElementList);
    sbQuery.append(" ) ");
    sbQuery.append(" AND T801.C801_ACCOUNT_ELEMENT_ID  = t810.C801_ACCOUNT_ELEMENT_ID ");
    sbQuery.append(" AND  T810.c1900_owner_company_id = T1900.C1900_COMPANY_ID(+) ");
    sbQuery.append(" AND t810.C801_ACCOUNT_ELEMENT_ID  = T806.C801_ACCOUNT_ELEMENT_ID(+) ");
    sbQuery.append(" AND T1900_txn_company.c1900_company_id = t810.c1900_company_id ");
    sbQuery.append(" AND T806.C806_VOID_FL(+) IS NULL ");
    sbQuery.append(" AND T806.C1900_COMPANY_ID(+) = '" + getCompId() + "'");
    
	sbQuery.append(" AND t810.C810_TXN_DATE >=  ");
    sbQuery.append(" TO_DATE( '" + strFormDate + "' , '" + strDateFormat + "')  ");
    sbQuery.append(" AND t810.C810_TXN_DATE <=  ");
    sbQuery.append(" TO_DATE( '" + strToDate + "' , '" + strDateFormat + "') ");
    if (!(strPartyId.equals("") || strPartyId.equals("0"))) {


      sbQuery.append(" and (t810.C810_PARTY_ID =");
      sbQuery.append(strPartyId);
      sbQuery.append("");
      sbQuery.append(" or t810.C810_PARTY_ID =get_account_dist_id('");
      sbQuery.append(strPartyId);
      sbQuery.append("')");
      sbQuery.append(")");
    }
    // Plant filter added
    if (!strPlantId.equals("0")) {
      sbQuery.append(" AND t810.c5040_plant_id ='" + strPlantId + "' ");
    }
    if (!strOwnerCompanyId.equals("0")) {
      sbQuery.append(" AND t810.c1900_owner_company_id ='" + strOwnerCompanyId + "' ");
    }
    if (!strOwnerCurrency.equals("0")) {
      sbQuery.append(" AND t810.c901_owner_currency ='" + strOwnerCurrency + "' ");
    }
    
    if (!strDivisionId.equals("0")) {
        sbQuery.append(" AND t810.c1910_division_id = " + strDivisionId + "");
    }
    
    sbQuery.append(" AND t810.c1900_company_id = '" + getCompId() + "'");
    sbQuery.append(" AND t810.c810_delete_fl = 'N' ");

    sbQuery
        .append(" GROUP BY t810.C801_ACCOUNT_ELEMENT_ID, t810.C901_ACCOUNT_TXN_TYPE, c1900_owner_company_id,c901_owner_currency, c901_local_company_currency ");
    sbQuery
    .append(" , T801.C801_NAME, T1900.C1900_COMPANY_NAME,T806.C806_SAGE_ACC_ID , T1900_txn_company.C1900_COMPANY_NAME, t810.c1910_division_name, t810.c1910_division_id");
    sbQuery.append(" ORDER BY A_NAME, TXN_NAME ");

    log.debug(" fetchRollforwardDtls query is " + sbQuery.toString());
    alDetailList = gmDBManager.queryMultipleRecords(sbQuery.toString());



    return alDetailList;
  }

  /**
   * fetchOpeningBalanceAddtnl - This method will give amount of After approximate date to from
   * date.
   * 
   * @return ArrayList alAddtnlOpenBalance
   * @exception AppError
   **/

  public ArrayList fetchOpeningBalanceAddtnl(HashMap hmParam) throws AppError {

    HashMap hmResult = new HashMap();
    ArrayList alAddtnlOpenBalance = new ArrayList();
    String strAccountElementList = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTLIST"));
    String strFormDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    String strCntxtCompanyId = getGmDataStoreVO().getCmpid();
    String strDateFormat = getCompDateFmt();
    String strApprxDate = GmCommonClass.parseNull((String) hmParam.get("apprmaxdate"));
    String strArchiveFl = GmCommonClass.parseNull((String) hmParam.get("ARCHIVEFL"));
    String strPlantId = GmCommonClass.parseZero((String) hmParam.get("PLANTID"));
    String strOwnerCompanyId = GmCommonClass.parseZero((String) hmParam.get("OWNER_COMP_ID"));
    String strOwnerCurrency = GmCommonClass.parseZero((String) hmParam.get("OWNER_CURR_ID"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" select C801_ACCOUNT_ELEMENT_ID ID ,GET_ACCTELEMENT_DESC(C801_ACCOUNT_ELEMENT_ID) NAME ");
    sbQuery.append(" ,NVL (SUM (c810_qty * c810_dr_amt)  - SUM (c810_qty * c810_cr_amt), 0) AMT2 ");
    sbQuery
        .append(" , NVL (SUM (c810_qty * C810_LOCAL_COMPANY_DR_AMT) - SUM (c810_qty   *  C810_LOCAL_COMPANY_CR_AMT), 0) AMT3 ");
    if (strArchiveFl.equals("")) {
      sbQuery.append(" from t810_posting_txn  ");
    } else {
      sbQuery.append(" from t810_posting_txn_archive   ");
    }
    sbQuery
        .append(" where c810_txn_date > to_date('" + strApprxDate + "','" + strDateFormat + "')");
    sbQuery.append(" AND c810_txn_date < to_date('" + strFormDate + "','" + strDateFormat + "') ");
    if (!strPlantId.equals("0")) {
      sbQuery.append(" AND c5040_plant_id = '" + strPlantId + "' ");
    }
    if (!strOwnerCompanyId.equals("0")) {
      sbQuery.append(" AND c1900_owner_company_id = '" + strOwnerCompanyId + "' ");
    }
    if (!strOwnerCurrency.equals("0")) {
      sbQuery.append(" AND c901_owner_currency = '" + strOwnerCurrency + "' ");
    }
    sbQuery.append(" AND c1900_company_id=" + strCntxtCompanyId);
    sbQuery.append(" AND c810_delete_fl = 'N' ");
    sbQuery.append(" AND C801_ACCOUNT_ELEMENT_ID IN (");
    strAccountElementList =
        strAccountElementList.substring(0, strAccountElementList.lastIndexOf(","));

    sbQuery.append(strAccountElementList);
    sbQuery.append(") GROUP by c801_account_element_id ORDER BY  NAME");

    log.debug("fetchOpeningBalanceAddtnl date query=========>" + sbQuery.toString());
    alAddtnlOpenBalance = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alAddtnlOpenBalance;
  }

  /**
   * fetchTotOpeningBal - This method will return total opening balance that is coming before
   * approximate date plus after approximate date to from date.
   * 
   * @return ArrayList alAddtnlOpenBalance
   * @exception AppError
   **/
  public ArrayList fetchTotOpeningBal(ArrayList alApprxmateBalance, ArrayList alAddtnlOpenBalance)
      throws AppError {


    ArrayList alTotalOpenBalance = new ArrayList();
    int alApprxBalSize = alApprxmateBalance.size();
    int alDetListSize = alAddtnlOpenBalance.size();
    double dbTotal = 0.0;
    double dbLocalTotal = 0.0;
    if (alApprxBalSize <= 0) {
      alApprxBalSize = alDetListSize;
    }

    for (int i = 0; i < alApprxBalSize; i++) {
      HashMap hmResult = new HashMap();
      boolean flag = false;
      HashMap hmApprxBal = (HashMap) alApprxmateBalance.get(i);
      String strApprxBalId = GmCommonClass.parseNull((String) hmApprxBal.get("ID"));
      String strAcctName = GmCommonClass.parseNull((String) hmApprxBal.get("NAME"));
      String strApprxBalAmt1 = GmCommonClass.parseNull((String) hmApprxBal.get("AMT"));
      String strLocalAmt = GmCommonClass.parseNull((String) hmApprxBal.get("LOCAL_AMT"));
      String strOwnerCompany = GmCommonClass.parseNull((String) hmApprxBal.get("OWNER_COMPANY"));
      String strOwnerCurrency = GmCommonClass.parseNull((String) hmApprxBal.get("OWNER_CURRENCY"));
      String strLocalCurrency = GmCommonClass.parseNull((String) hmApprxBal.get("LOCAL_CURRENCY"));
      String strOwnerId = GmCommonClass.parseNull((String) hmApprxBal.get("OWNER_COMPANY"));
      for (int j = 0; j < alDetListSize; j++) {
        HashMap hmDetList = (HashMap) alAddtnlOpenBalance.get(j);
        String strDetailListId = GmCommonClass.parseNull((String) hmDetList.get("ID"));
        String strDetailLisAmt2 = GmCommonClass.parseNull((String) hmDetList.get("AMT2"));

        String strDetailOwnerCompany =
            GmCommonClass.parseNull((String) hmDetList.get("OWNER_COMPANY"));
        String strDetailLocalAmt = GmCommonClass.parseNull((String) hmDetList.get("AMT3"));

        if (strApprxBalId.equals(strDetailListId)) {
          flag = true;

          GmCommonClass.parseNullHashMap((HashMap) hmResult.put("ID", strDetailListId));
          dbTotal = Double.parseDouble(strDetailLisAmt2) + Double.parseDouble(strApprxBalAmt1);
          dbLocalTotal = Double.parseDouble(strDetailLocalAmt) + Double.parseDouble(strLocalAmt);
          String strTotal = GmCommonClass.parseNull(Double.toString(dbTotal));
          String strLocalTotal = GmCommonClass.parseNull(Double.toString(dbLocalTotal));
          GmCommonClass.parseNullHashMap((HashMap) hmResult.put("AMT", strTotal));
          GmCommonClass.parseNullHashMap((HashMap) hmResult.put("NAME", strAcctName));
          hmResult.put("LOCAL_AMT", strLocalTotal);
          hmResult.put("OWNER_COMPANY_ID", strOwnerId);
          alAddtnlOpenBalance.remove(alAddtnlOpenBalance.get(j));
          alDetListSize = alAddtnlOpenBalance.size();

          break;
        } else {
          flag = false;
        }

      }
      if (flag) {
        alTotalOpenBalance.add(hmResult);
      } else {
        alTotalOpenBalance.add(alApprxmateBalance.get(i));
      }

    }
    alTotalOpenBalance.addAll(alAddtnlOpenBalance);

    return alTotalOpenBalance;

  }

  /**
   * fetchApprovedDate - This method will get approximate date that is maximum date of transaction
   * date.
   * 
   * @return String
   * @exception AppError
   **/

  public String fetchApprovedDate(HashMap hmParam) throws AppError {
    HashMap hmApprxDate = new HashMap();

    String strDateFormat = getCompDateFmt();
    String strArchiveFl = GmCommonClass.parseNull((String) hmParam.get("ARCHIVEFL"));
    String strFormDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
    String strApprxDate = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT to_char(MAX(C810_TXN_DATE),'" + strDateFormat + "') apprmaxdate ");
    sbQuery.append(" FROM T810_rollforward ");
    sbQuery.append(" WHERE C901_STATUS  ='105901' ");// 105901-Approved.
    sbQuery.append(" AND C810_TXN_DATE <= to_date('" + strFormDate + "','" + strDateFormat + "') ");
    sbQuery.append(" AND c1900_company_id = '" + getCompId() + "'");

    log.debug("approximate date query=========>" + sbQuery.toString());
    hmApprxDate = gmDBManager.querySingleRecord(sbQuery.toString());
    strApprxDate = GmCommonClass.parseNull((String) hmApprxDate.get("APPRMAXDATE"));
    return strApprxDate;
  }

}
