package com.globus.accounts.beans;

import java.util.HashMap;

import com.globus.common.beans.AppError;
/* fetching the invoice details and amount/tax calulation interface method declaration*/
public interface GmInvoicePrintDtlsInterface {   
  public HashMap filterInvoiceData(HashMap hmInData) throws AppError;
}