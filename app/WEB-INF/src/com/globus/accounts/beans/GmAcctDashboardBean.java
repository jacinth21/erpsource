/*
 * Module: GmAccountsBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 11
 * Mar 2004 Security: Unclassified Description: Accounts * Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What * you changed
 */

package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAcctDashboardBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger

  // this will be removed all place changed with GmDataStoreVO constructor

  public GmAcctDashboardBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAcctDashboardBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * reportDashboard - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportDashboard(String strAccCurrId) throws AppError {
    GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
    GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    HashMap hmSales = new HashMap();
    RowSetDynaClass rsdReturnInvoice = null;
    RowSetDynaClass rsdReturns = null;
    ArrayList alReturns = new ArrayList();

    hmSales = gmCust.todaysSales(); // Todays and Month to Date sales
    // figures
    hmReturn.put("SALES", hmSales);

    rsdReturnInvoice = reportDashboardInvoice(strAccCurrId); // Invoices Dashboard
    hmReturn.put("INVOICE", rsdReturnInvoice);
    log.debug(" Count in Invoice " + rsdReturnInvoice.getRows().size());

    rsdReturns = gmOper.getReturnsDashboardDyna("ACCT", strAccCurrId); // Returns Dashboard
    // rsdReturns = rsdReturns.getRows(alReturns);
    hmReturn.put("RETURNS", rsdReturns);
    log.debug(" Count in Pending Credut " + rsdReturns.getRows().size());
    return hmReturn;

  } // ENd of reportDashboard

  /**
   * reportDashboardInvoice - This method will
   * 
   * 
   * @return RowSetDynaClass
   * @exception AppError
   */
  public RowSetDynaClass reportDashboardInvoice(String strAccCurrId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strPrepareString = null;

    RowSetDynaClass resultSet = null;
    ResultSet rs = null;


    gmDBManager.setPrepareString("GM_REPORT_INVOICE_DASHBOARD", 3);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.setString(1, strAccCurrId);
    gmDBManager.registerOutParameter(2, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.execute();
    strBeanMsg = gmDBManager.getString(2);
    rs = (ResultSet) gmDBManager.getObject(3);
    resultSet = gmDBManager.returnRowSetDyna(rs);
    gmDBManager.close();


    return resultSet;
  } // End of reportDashboardInvoice

  /**
   * acctSalesReport - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @param String strSalesFilter
   * @return HashMap
   * @exception AppError
   **/
  public HashMap acctSalesReport(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strSalesFilter = GmCommonClass.parseNull((String) hmParam.get("ACESSCONDITION"));
    String strARcurrSymbol = GmCommonClass.parseNull((String) hmParam.get("ARCURRSYMBOL"));
    String strBeanMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    ResultSet rs = null;


    gmDBManager.setPrepareString("GM_REPORT_ACCOUNT_ORDER", 6);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);

    gmDBManager.setString(1, strFromDate);
    gmDBManager.setString(2, strToDate);
    gmDBManager.setString(3, strSalesFilter);
    gmDBManager.setInt(4, Integer.parseInt(strARcurrSymbol));
    gmDBManager.execute();

    strBeanMsg = gmDBManager.getString(5);
    rs = (ResultSet) gmDBManager.getObject(6);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    hmReturn.put("REPORT", alReturn);
    hmReturn.put("MSG", strBeanMsg);

    return hmReturn;
  } // End of acctSalesReport


}// end of class GmAccountsBean
