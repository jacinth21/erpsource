package com.globus.accounts.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCreditHoldEngine extends GmBean{
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmCreditHoldEngine() {
	  super(GmCommonClass.getDefaultGmDataStoreVO());
  }
  
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmCreditHoldEngine(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }
  
  /**
   * validateCreditHold - This method is used to validate Credit Hold for given AccountId and return
   * Credit Hold detail for that Account.
   * 
   * @param hmParam - HashMap [ACCOUNID,CREDITTYPE]
   * @return HashMap
   * @throws AppError
   */
  public HashMap validateCreditHold(HashMap hmParam) throws AppError {
    String strCreditType = "";

    HashMap hmReturn = new HashMap();
    ArrayList alResult = new ArrayList();

    // Added for PMT-53329 passing the datastorevo to bean
    GmCreditHoldBean gmCreditHoldBean = new GmCreditHoldBean(getGmDataStoreVO());

    alResult = gmCreditHoldBean.fetchAcctCreditHold(hmParam);
    if (alResult.size() > 0) {
      hmReturn = (HashMap) alResult.get(0);
    }
    strCreditType = GmCommonClass.parseNull((String) hmReturn.get("CREDITTYPE"));

    log.debug("strCreditType" + strCreditType);

    if (!strCreditType.equals("") && !strCreditType.equals("4000844")) {// 4000844 - None
      hmParam.put("CREDITHOLDTYPE", strCreditType);
      // get Credit Hold Subtype value
      // Ex. If Credit Hold type is 'Type1' and subtype is 'Place Order'
      alResult = gmCreditHoldBean.fetchCreditHoldDtl(hmParam);
      if (alResult.size() > 0) {
        // Merge Account Credit Hold detail ie., Credit Limit & Credit Type and Credit Type's given
        // subtype allowed functionality.
        hmReturn.putAll((HashMap) alResult.get(0));
        hmReturn.put("CREDITHOLDMSG", GmCommonClass.getRuleValue("SHORT_MSG", "CREDIT_HOLD_MSG"));
      }
    }
    log.debug("hmReturn" + hmReturn);
    return hmReturn;
  }

  /**
   * createCreditMessage - This method is used to generated Credit Hold Message from rule table and
   * place dynamic values
   * 
   * @param hmParam - HashMap[Credit Hold details]
   * @return String - Credit Hold Message 'CreditHoldValue|HeaderMsg<BR>
   *         DetailMsg'
   * @throws AppError
   */
  public String createCreditMessage(HashMap hmParam) throws AppError {
    String strCreditMsg = "";
    String strHeaderMsg = "";
    String strDetailMsg = "";

    String strAcctId = GmCommonClass.parseNull((String) hmParam.get("ACCTID"));
    String strAcctNm = GmCommonClass.parseNull((String) hmParam.get("ACCTNM"));
    String strCreditType = GmCommonClass.parseNull((String) hmParam.get("CREDITTYPE"));
    String strCreditTypeNm = GmCommonClass.parseNull((String) hmParam.get("CREDITTYPENM"));
    String strCreatedDt = GmCommonClass.parseNull((String) hmParam.get("CREATEDDT"));
    String strCreatedBy = GmCommonClass.parseNull((String) hmParam.get("CREATEDBY"));
    String strCreditValue = GmCommonClass.parseNull((String) hmParam.get("CREDITVALUE"));
    String strMessage = GmCommonClass.parseNull((String) hmParam.get("MESSAGE"));
    log.debug("strCreditValue==" + strCreditValue + " strCreditType=" + strCreditType);
    if (!strCreditValue.equals("") && !strCreditType.equals("4000844")) {// 4000844 - None
      // Get the Message -
      // "This Account [<<ACCTID>>:<<ACCTNM>>] is on Credit Hold - <<CREDITTYPENM>>."
      strHeaderMsg = GmCommonClass.getRuleValue("HEADER", "CREDIT_HOLD_MSG");
      // Get the Message - " Credit Hold was set on <<CREATEDDT>> by <<CREATEDBY>>."
      strDetailMsg = GmCommonClass.getRuleValue("CREATED_DTL", "CREDIT_HOLD_MSG");
      // Replace appropriate values in that place holder.
      strHeaderMsg =
          strHeaderMsg.replace("<<ACCTID>>", strAcctId).replace("<<ACCTNM>>", strAcctNm)
              .replace("<<CREDITTYPENM>>", strCreditTypeNm);

      strDetailMsg =
          strDetailMsg.replace("<<CREATEDDT>>", strCreatedDt)
              .replace("<<CREATEDBY>>", strCreatedBy);

      // Append Message with format of CreditHoldValue|HeaderMsg<BR>DetailMsg
      strCreditMsg = strCreditValue + "|" + strHeaderMsg + "<BR>" + strMessage + strDetailMsg;
    }
    log.debug("strCreditMsg==" + strCreditMsg);
    return strCreditMsg;
  }

  /**
   * validateCreditHold - This method is used to validate Credit Hold for given AccountId and if
   * Account is on credit hold type1 then send email notification.
   * 
   * @param hmParam - HashMap [ACCOUNID,CREDITTYPE]
   * @return HashMap
   * @throws AppError
   */
  public void validateCreditHoldAndSendEmail(HashMap hmParam) throws AppError {
    String strCreditType = "";
    HashMap hmCreditDtl = new HashMap();
    // Added for PMT-53329 passing the datastorevo to bean
    GmCreditHoldBean gmCreditHoldBean = new GmCreditHoldBean(getGmDataStoreVO());

    hmCreditDtl = validateCreditHold(hmParam);
    strCreditType = GmCommonClass.parseNull((String) hmCreditDtl.get("CREDITTYPE"));
    if (strCreditType.equals("104960")) {// Type1
      gmCreditHoldBean.sendCreditHoldNotifyEmail(hmParam);
    }
  }
}
