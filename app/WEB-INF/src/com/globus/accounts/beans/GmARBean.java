/*
 * Module: GmARBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written: 11 Mar
 * 2004 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.accounts.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmARBean extends GmBean implements GmInvoicePrintDtlsInterface, GmARSummaryInterface {
  GmCommonClass gmCommon = new GmCommonClass();

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.

  // this will be removed all place changed with Data Store VO constructor

  public GmARBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmARBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }


  GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());

  public HashMap fetchInvoiceDetails(String strPO, String strAccId, String strAction)
      throws AppError {
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    hmReturn = gmInvoicePrintBean.fetchInvoiceDetails(strPO, strAccId, strAction);
    return hmReturn;
  } // End of loadInvoiceDetails

  /**
   * loadInvoiceDetails - This method gets Invoice information for the selected PO and for the
   * selected Account
   * 
   * @param String strPO
   * @param String strAccId
   * @param String strAction
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadInvoiceDetails(String strInvoiceId, String strAction) throws AppError {
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    hmReturn = gmInvoicePrintBean.loadInvoiceDetails(strInvoiceId, strAction);
    return hmReturn;
  } // End of loadInvoiceDetails

  /**
   * generateInvoice - This method will generate Invoice
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public String generateInvoice(String strSessPO, String strSessAccId, String strPay,
      Date dtInvDate, String strUserId, String strAction, String strInvType) throws AppError {
    GmARTransBean gmARTransBean = new GmARTransBean(getGmDataStoreVO());
    String strInvoiceId = "";
    strInvoiceId =
        gmARTransBean.generateInvoice(strSessPO, strSessAccId, strPay, dtInvDate, strUserId,
            strAction, strInvType);
    return strInvoiceId;
  } // end of generateInvoice

  /**
   * updateInvoice - This method will update Invoice
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap updateInvoice(String strInv, String strSessAccId, String strSessPO,
      String strUserId, String strInputStr) throws AppError {
    GmARTransBean gmARTransBean = new GmARTransBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    hmReturn = gmARTransBean.updateInvoice(strInv, strSessAccId, strSessPO, strUserId, strInputStr);
    return hmReturn;
  } // end of updateInvoice

  /**
   * updateNewPO - This method will be used to update the new PO
   * 
   * @param String strInv
   * @param String strAccId
   * @param String strOldPO
   * @param String strNewPO
   * @param String strUserId
   * @return HashMap
   * @exception AppError
   */
  public void updateNewPO(String strInv, String strAccId, String strOldPO, String strNewPO,
      String strUserId) throws AppError {
    GmARTransBean gmARTransBean = new GmARTransBean(getGmDataStoreVO());
    gmARTransBean.updateNewPO(strInv, strAccId, strOldPO, strNewPO, strUserId);
  } // end of updateNewPO

  /**
   * reportPendingInvoice - This method will report pending invoice.
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap reportPendingInvoice(HashMap hmParam) throws AppError {
    GmARReportBean gmARReportBean = new GmARReportBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    hmReturn = gmARReportBean.reportPendingInvoice(hmParam);
    return hmReturn;
  } // End of reportPendingInvoice


  /**
   * loadInvForReturns - This method will load invoice for returns
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadInvForReturns(String strInvId, String strAccId) throws AppError {
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    hmReturn = gmInvoicePrintBean.loadInvForReturns(strInvId, strAccId);
    return hmReturn;
  } // End of loadInvForReturns


  /**
   * filterInvoiceData - This method will do the calculations for invoice print.
   * 
   * @param HashMap hmInData
   * @return HashMap
   * @exception AppError
   */

  @Override
  public HashMap filterInvoiceData(HashMap hmInData) throws AppError {
    // to get the data store values
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmInData.get("gmDataStoreVO");
    // data store null then, assign the default values
    if (gmDataStoreVO == null) {
      gmDataStoreVO = getGmDataStoreVO();
    }

    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(gmDataStoreVO);
    HashMap hmOutData = new HashMap();
    hmOutData = gmInvoicePrintBean.filterInvoiceData(hmInData);
    return hmOutData;
  }


  /**
   * reportARSummaryByInvDate - This method is used to fetch AR Summary Information based on invoice
   * date,
   * 
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  @Override
  public ArrayList reportARSummaryByInvDate(HashMap hmParam) throws AppError {
    String strSummaryFetchType =
        GmCommonClass.parseNull((String) hmParam.get("SUMMARY_FETCH_TYPE"));
    log.debug(" strSummaryFetchType " + strSummaryFetchType);
    GmARSummaryInterface gmARSummaryInterface =
        (GmARSummaryInterface) GmCommonClass.getSpringBeanClass("xml/AR_Summary_Beans.xml",
            strSummaryFetchType);
    ArrayList alReturn = new ArrayList();

    alReturn = gmARSummaryInterface.reportARSummaryByInvDate(hmParam);

    return alReturn;
  }

  /**
   * populateIntervalDtl - This method is used to create interval for AR Summary,
   * 
   * @author Jignesh shah
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  public ArrayList populateIntervalDtl(HashMap hmParam) throws AppError {

    /*
     * This method is used to create intervals for vm file In this method DAFAULTTHROUGH=90 ,
     * INTERVAL - input from user else 0 , THROUGH - input from user else 0 , while loop will create
     * e.g. HEADING = 91-120, KEY = COL_91_120, FROMDAY=90, TODAY=120
     */
    GmARSummaryBean gmARSummaryBean = new GmARSummaryBean(getGmDataStoreVO());
    ArrayList alColumn = new ArrayList();
    alColumn = gmARSummaryBean.populateIntervalDtl(hmParam);
    return alColumn;
  } // end of populateIntervalDtl()

  /**
   * generateAdditionalIntervalsByInvDate - This method is used to create interval for AR Summary
   * Invoice date query,
   * 
   * @author Jignesh shah
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  public String generateAdditionalIntervalsByInvDate(HashMap hmParam) throws AppError {
    GmARSummaryBean gmARSummaryBean = new GmARSummaryBean(getGmDataStoreVO());
    String strQuery = gmARSummaryBean.generateAdditionalIntervalsByInvDate(hmParam);
    return strQuery;
  } // end of generateAdditionalIntervalsByInvDate()

  /**
   * reportARSummaryByDueDate - This method is used to fetch AR Summary Information based on due
   * date,
   * 
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  @Override
  public ArrayList reportARSummaryByDueDate(HashMap hmParam) throws AppError {
    String strSummaryFetchType =
        GmCommonClass.parseNull((String) hmParam.get("SUMMARY_FETCH_TYPE"));
    GmARSummaryInterface gmARSummaryInterface =
        (GmARSummaryInterface) GmCommonClass.getSpringBeanClass("xml/AR_Summary_Beans.xml",
            strSummaryFetchType);
    ArrayList alReturn = new ArrayList();

    alReturn = gmARSummaryInterface.reportARSummaryByDueDate(hmParam);


    return alReturn;
  }

  /**
   * generateAdditionalIntervalsByDueDate - This method is used to create interval for AR Summary
   * Due date query,
   * 
   * @author Jignesh shah
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  public String generateAdditionalIntervalsByDueDate(HashMap hmParam) throws AppError {
    /*
     * In this method we are creating intervals by adding invoice terms days
     */
    GmARSummaryBean gmARSummaryBean = new GmARSummaryBean(getGmDataStoreVO());
    String strQuery = gmARSummaryBean.generateAdditionalIntervalsByDueDate(hmParam);
    return strQuery;
  }

  /**
   * reportARDetailByAccount - This method is used to fetch AR Summary Invoice details based on
   * account
   * 
   * @param hmParam
   * 
   * @return Arraylist
   * @exception AppError
   */
  @Override
  public RowSetDynaClass reportARDetailByAccount(HashMap hmParam) throws AppError {
    String strSummaryFetchType =
        GmCommonClass.parseNull((String) hmParam.get("SUMMARY_FETCH_TYPE"));
    GmARSummaryInterface gmARSummaryInterface =
        (GmARSummaryInterface) GmCommonClass.getSpringBeanClass("xml/AR_Summary_Beans.xml",
            strSummaryFetchType);
    RowSetDynaClass resultSet = null;
    resultSet = gmARSummaryInterface.reportARDetailByAccount(hmParam);
    return resultSet;

  } // End of reportARDetailByAccount

  /**
   * postMultiplePayments - This method will post multiple payments
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public HashMap postMultiplePayments(HashMap hmParam) throws AppError {
    GmARTransBean gmARTransBean = new GmARTransBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    hmReturn = gmARTransBean.postMultiplePayments(hmParam);
    return hmReturn;
  } // end of postMultiplePayments

  public String loadInvoiceSQL(String strInvoiceId, String strAction) {
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(getGmDataStoreVO());
    String strInvocieSQLQuery = gmInvoicePrintBean.loadInvoiceSQL(strInvoiceId, strAction);
    return strInvocieSQLQuery;
  }

  /**
   * voidInvoice - This method will Void the Invoice
   * 
   * @param String strPO
   * @param String strAccId
   * @param String strAction
   * @return HashMap
   * @exception AppError
   */
  public void voidInvoice(String strInvoiceId) throws AppError {
    GmARTransBean gmARTransBean = new GmARTransBean(getGmDataStoreVO());
    gmARTransBean.voidInvoice(strInvoiceId);

  } // End of voidInvoice

  /**
   * issueCredit - This method will Issue Credit through "Issue Credit" page
   * 
   * @param String strPO
   * @param String strAccId
   * @param String strAction
   * @return HashMap
   * @exception AppError
   */
  public String issueCredit(HashMap hmvatparam) throws AppError {
    GmARTransBean gmARTransBean = new GmARTransBean(getGmDataStoreVO());
    String strInvoiceId = "";
    strInvoiceId =
        gmARTransBean.issueCredit(hmvatparam);
    log.debug("strInvoiceId>>>>"+strInvoiceId);
    return strInvoiceId;
  }
  /**
   * fetchInvoiceId() : Get invoice id for Updated invoice in Process Transaction screen
   * 
   * @param RefID, Source
   * @return String
   */
  public String fetchInvoiceId(HashMap hmParam) throws AppError {
    GmARReportBean gmARReportBean = new GmARReportBean(getGmDataStoreVO());
    String strInvoiceId = "";
    strInvoiceId = gmARReportBean.fetchInvoiceId(hmParam);
    return strInvoiceId;
  }

  /**
   * getInvoiceDtls - This method will get invoice details
   * 
   * @param String Invoice Id
   * @exception AppError
   */
  public ArrayList getInvoiceDtls(HashMap hmParam) throws AppError {
    GmARReportBean gmARReportBean = new GmARReportBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    alResult = gmARReportBean.getInvoiceDtls(hmParam);
    return alResult;
  }

  /**
   * validateInvoice - This method will get the invoice id and part number details for given part
   * and invoice
   * 
   * @param Hashmap
   * @exception AppError
   */
  public ArrayList validateInvoice(HashMap hmParam) throws AppError {
    GmARReportBean gmARReportBean = new GmARReportBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    alResult = gmARReportBean.validateInvoice(hmParam);
    return alResult;
  } // end of validateInvoice

  /**
   * saveIssueMemo - This method will save the issue memo
   * 
   * @param HashMap
   * 
   * @return HashMap
   * @exception AppError
   */

  public HashMap saveIssueMemo(HashMap hmParam) throws AppError {
    GmARTransBean gmARTransBean = new GmARTransBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    hmReturn = gmARTransBean.saveIssueMemo(hmParam);
    return hmReturn;
  } // end of saveIssueMemo

  /**
   * saveCommentsOnInvoice - This method will update the comments on an Invoice
   * 
   * @param HashMap
   * @return
   * @exception AppError
   */
  public void saveCommentsOnInvoice(HashMap hmParams) throws AppError {
    GmARTransBean gmARTransBean = new GmARTransBean(getGmDataStoreVO());
    gmARTransBean.saveCommentsOnInvoice(hmParams);
  } // End of saveCommentsOnInvoice

  /**
   * loadOrderAttribute - This method is used to load the attribute values of an order
   * 
   * @param String strOrdId
   * @param String strOrdType
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadOrderAttribute(String strOrdId, String strOrdType) throws AppError {
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    hmReturn = gmInvoicePrintBean.loadOrderAttribute(strOrdId, strOrdType);
    return hmReturn;
  }

  /**
   * loadInvoiceAttribute - This method will return the hashmap for the input rule grp id.
   * 
   * @exception AppError
   */
  public HashMap loadInvoiceAttribute(String strRuleGrpId) throws AppError {
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    hmReturn = gmInvoicePrintBean.loadInvoiceAttribute(strRuleGrpId);
    return hmReturn;
  }// end of loadInvoiceAttribute

  /**
   * fetchPaymentDetails - This method will return the payment details when we are passing either
   * Invoice ID or Order ID. Order ID will be passed in Delivered Order Summary Screen. Invoice ID
   * will be passed in post payment Screen.
   * 
   * @exception AppError
   */
  public ArrayList fetchPaymentDetails(HashMap hmParam) throws AppError {
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    alReturn = gmInvoicePrintBean.fetchPaymentDetails(hmParam);
    return alReturn;
  }

  /**
   * validateSubPartPrice - This method will validate sub part price with order item price - france
   * 
   * @param String PO
   * @param String account
   * @return void
   * @exception AppError
   */
  public void validateSubPartPrice(String strSessPO, String strSessAccId, String strUserId)
      throws AppError {
    GmARReportBean gmARReportBean = new GmARReportBean(getGmDataStoreVO());
    gmARReportBean.validateSubPartPrice(strSessPO, strSessAccId, strUserId);
  } // end of validateSubPartPrice

}// end of class GmARBean
