package com.globus.accounts.beans;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.printwork.invoice.beans.GmInvoicePrintInterface;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author pvigneshwaran
 *
 */

public class GmInvoiceBean extends GmBean implements GmInvoicePrintDtlsInterface{

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * Constructor will be called default company info.
   */
  public GmInvoiceBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStoreVO
   */
  public GmInvoiceBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  }

  ResourceBundleMessageSource sx;
  ReloadableResourceBundleMessageSource xt;

  /**
   * @param strCompanyId
   * @return object
   * @throws AppError
   */
  public static Object getInvoicePrintClass(String strCompanyId) throws AppError {  
    ApplicationContext context = new ClassPathXmlApplicationContext("xml/Invoice_Print_Beans.xml");
    return context.getBean("COM" + strCompanyId);
  }

  /**
   * PC-643 :: This method used for getting invoice layout with list price display. 
   * @param strInvLayout
   * @return object
   * @throws AppError
   */
  public static Object getInvoiceLayoutPrintClass(String strInvLayout) throws AppError {  
    ApplicationContext context = new ClassPathXmlApplicationContext("xml/Invoice_Layout_Print_Beans.xml");
    return context.getBean("INVLAYOUT" + strInvLayout);
  }

  
  /**
   * @param strInvoiceId
   * @return Hashmap
   * @throws AppError
   */
  public HashMap fetchCompanyInvoiceDtls(String strInvoiceId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_company_invoice_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strInvoiceId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * This method fetchInvAmountInWord will return invoice related information
   * 
   * @param HashMap
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchInvAmountInWord(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    String strCurrSign = GmCommonClass.parseNull((String) hmParam.get("CURRSIGN"));
    Double dbGrossTotal = GmCommonClass.parseDouble((Double) hmParam.get("GROSSTOTAL"));
	Double dbRoundedGrossTotal = GmCommonClass.parseDouble((Double) hmParam.get("ROUNDEDGROSSTOTAL"));
    //Double dbRoundedGrossTotal = 0.0;
    //dbRoundedGrossTotal = (double) Math.round(dbGrossTotal);
    hmReturn.put("ROUNDEDGROSSTOTAL", dbRoundedGrossTotal);
    hmReturn.put("ROUNDOFFAMOUNT", Math.round(dbGrossTotal) - dbGrossTotal);
    if (strCurrSign.equals("USD") || strCurrSign.equals("$") || strCurrSign.equals("&#36;")) {
      // This will show 25,782.30 as Twenty Five Thousand Seven Hundred Eighty Two and Thirty Cents
      // Only
      // convert double into string.
      double dbUSDGrossTotal = Math.abs(GmCommonClass.roundDigit(dbGrossTotal, 2));
      String strUSDGrossTot = "" + dbUSDGrossTotal;
      String strUSDGrossTotDecimal = strUSDGrossTot;
      // Get the Integer in words.
      strUSDGrossTot = strUSDGrossTot.substring(0, strUSDGrossTot.indexOf("."));
      strUSDGrossTot = GmCommonClass.convertToWords(Integer.valueOf(strUSDGrossTot));
      // Get the Decimal in words.
      strUSDGrossTotDecimal =
          strUSDGrossTotDecimal.substring(strUSDGrossTotDecimal.indexOf(".") + 1);
      // .10 we need to show it as ten. But in the decimal place we have 1 only. so appending zero.
      strUSDGrossTotDecimal =
          strUSDGrossTotDecimal.length() == 1 ? strUSDGrossTotDecimal + "0" : strUSDGrossTotDecimal;
      strUSDGrossTotDecimal = GmCommonClass.convertToWords(Integer.valueOf(strUSDGrossTotDecimal));
      String strUSDGrossTotalinWords = "";
      // if the decimal place values are less than zero, no need to append the following.
      if (!strUSDGrossTotDecimal.equals("")) {
        strUSDGrossTotalinWords =
            strUSDGrossTot.replace(" and", "") + " and" + strUSDGrossTotDecimal + " Cents";
      } else {
        strUSDGrossTotalinWords = strUSDGrossTot;
      }

      strUSDGrossTotalinWords =
          dbRoundedGrossTotal < 0 ? " Minus" + strUSDGrossTotalinWords : strUSDGrossTotalinWords;
      hmReturn.put("GRANDTOTALINWORDS", strUSDGrossTotalinWords);
      hmReturn.put("ROUNDEDGROSSTOTAL", dbGrossTotal);
    } else {
      // This is for To Get the amount in words.
      DecimalFormat df = new DecimalFormat("####");
      String strFormattedTotal = df.format(Math.abs(dbRoundedGrossTotal));
      String strGrandTotalInWords =
          GmCommonClass.convertToWords(Integer.valueOf(strFormattedTotal));
      strGrandTotalInWords =
          dbRoundedGrossTotal < 0 ? " Minus" + strGrandTotalInWords : strGrandTotalInWords;
      hmReturn.put("GRANDTOTALINWORDS", strGrandTotalInWords);
    }
    hmReturn.put("ROUNDOFFAMOUNT", Math.round(dbGrossTotal) - dbGrossTotal);
    String strDiscount = GmCommonClass.parseNull((String) hmParam.get("DISCOUNT"));
    strDiscount = strDiscount.equals("") ? "0.00" : strDiscount;
    hmReturn.put("DISCOUNT", strDiscount);
    hmReturn.put("FOOTER_TEXT", "");
    if (dbGrossTotal < 0) {
      hmReturn.put("HEADERLBL", "Gutschrift");
    } else {
      hmReturn.put("HEADERLBL", "Rechnung");
    }
    return hmReturn;
  }

  /**
   * This method used to add the usage details
   * 
   * @param hmParam
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList addUsageLotNumber(HashMap hmParam) throws AppError {
    ArrayList alAddUsageLot = new ArrayList();
    ArrayList alOrderDtls = new ArrayList();
    ArrayList alLotNums = new ArrayList();
    HashMap hmLotNumLoop = null;
    HashMap hmOrderLoop = null;
    String strDoPartNumber = "";
    String strDoPartQty = "";
    String strPartNumber = "";
    String strQty = "";
    // getting the array list values
    alLotNums = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("DOCONTROLNUMS"));
    alOrderDtls = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALTEMPORDERLIST"));

    for (int i = 0; i < alOrderDtls.size(); i++) {
      hmOrderLoop = (HashMap) alOrderDtls.get(i);
      // get the cart part # and Qty
      strPartNumber = GmCommonClass.parseNull((String) hmOrderLoop.get("ID"));
      strQty = GmCommonClass.parseNull((String) hmOrderLoop.get("QTY"));
      if (alLotNums.size() > 0) {
        for (int j = 0; j < alLotNums.size(); j++) {
          hmLotNumLoop = (HashMap) alLotNums.get(j);
          // get the usage lot part # and Qty
          strDoPartNumber = GmCommonClass.parseNull((String) hmLotNumLoop.get("PNUM"));
          strDoPartQty = GmCommonClass.parseNull((String) hmLotNumLoop.get("IQTY"));
          if (strPartNumber.equals(strDoPartNumber) && strQty.equals(strDoPartQty)) {
            hmOrderLoop.put("LOTNUM", GmCommonClass.parseNull((String) hmLotNumLoop.get("CNUM")));
            alLotNums.remove(j);
            break;
          }
        }
      }
      alAddUsageLot.add(hmOrderLoop);
    }
    return alAddUsageLot;
  }
  
  /**
   * filterInvoiceData - This method will do the calculations for invoice print for India.
   * overriding this method for Chennai/Delhi
   * @param HashMap hmInData
   * @return HashMap
   * @exception AppError
   */

  public HashMap filterInvoiceData(HashMap hmInData) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmOutData = new HashMap();
    ArrayList alOrderNums = new ArrayList();
    HashMap hmCartDetails = new HashMap();
    HashMap hmConstructs = new HashMap();
    ArrayList alConsLoop = null;
    HashMap hmConsLoop = null;
    hmReturn = GmCommonClass.parseNullHashMap((HashMap) hmInData.get("hmReturn"));
    log.debug("hmReturn>>>>"+hmReturn);
    
    HashMap hmOrderDetails = new HashMap();
    ArrayList alPaidMonth = new ArrayList();
    String strPayNm = "";
    String strOrdId = "";
    String strInvNum = "";
    String strParentInvId = "";
    String strCreditInvId = "";
    String strPartyFl = "";
    String strInvoiceNote = "";
    String strPayLbl = "";
    String strDiscLbl = "";
    String strCompId = "";
    String strInvComments = "";
    String strTaxStartDateFl = "";
    String strVAFSSFlag = "";
    String strInvoiceAdj = "";
    String strAdjStartDateFl = "";
    String strTCSPercent = "";
    double tcsPer = 0.0;
    double dbInvoiceAdjTotal = 0.0;
    double dbOverAllAdjTotal = 0.0;
    String strInvPrintlbl = "";
    String strPrintPurposelbl = "";
    java.sql.Date dtOrderDate = null;
    int intYear;
    int intMonth;
    String strNxtYear;
    String strFinYear = "";
    java.sql.Date dtInvDt = null;
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmInData.get("gmDataStoreVO");
    String strCompanyLocale = (String) hmInData.get("CompanyLocale");
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    GmResourceBundleBean gmCompResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    String strInvoiceCompanyId = GmCommonClass.parseNull((String) hmInData.get("CompanyId"));
    GmInvoicePrintInterface gmInvoicePrintInterface =
        (GmInvoicePrintInterface) GmInvoiceBean.getInvoicePrintClass(strInvoiceCompanyId);
    
  
    strInvPrintlbl =GmCommonClass.parseNull((String) hmInData .get("INVPRINTLBL"));

	   if(strInvPrintlbl.equals("ORG_RECIPIENT")){
	     strPrintPurposelbl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("ORG_RECIPIENT"));
	   }
	   else if(strInvPrintlbl.equals("DUP_TRANSPORTER")) {
	     strPrintPurposelbl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("DUP_TRANSPORTER"));
	   }
	   else if(strInvPrintlbl.equals("TRIP_SUPPLIER")) {
	     strPrintPurposelbl = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("TRIP_SUPPLIER"));
	   }
	   else{
	     strPrintPurposelbl = "";
	   }

   
    GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(gmDataStoreVO);
    GmDOBean gmDOBean = new GmDOBean(gmDataStoreVO);
    HashMap hmPrintInvDtls = gmInvoicePrintInterface.fetchInvoiceDtls(hmInData);
    hmReturn.putAll(hmPrintInvDtls);
    alPaidMonth = GmCommonClass.parseNullArrayList(((ArrayList) hmInData.get("ALPAIDMONTH")));

    if (hmReturn != null) {
      hmOrderDetails = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("ORDERDETAILS"));
     
      hmOrderDetails.putAll(hmPrintInvDtls);
      // Account Currency based date format
      String strAccountCurrencyDateFmt =
          GmCommonClass.parseNull((String) hmOrderDetails.get("ACC_CURR_DATEFMT"));
      strAccountCurrencyDateFmt =
          strAccountCurrencyDateFmt.equals("") ? gmDataStoreVO.getCmpdfmt()
              : strAccountCurrencyDateFmt;
      dtInvDt = (java.sql.Date)hmOrderDetails.get("INVDT");
      hmOrderDetails.put("INVDT", GmCommonClass.getStringFromDate(
          (java.sql.Date) hmOrderDetails.get("INVDT"), strAccountCurrencyDateFmt));
     

      hmOrderDetails.put("PARTYFLAG", GmCommonClass.parseNull((String) hmReturn.get("PARTYFLAG")));
      hmOrderDetails.put("PARTYNAME", GmCommonClass.parseNull((String) hmReturn.get("PARTYNAME")));
      hmOrderDetails.put("HEADERADDRESS",
          GmCommonClass.parseNull((String) hmReturn.get("HEADERADDRESS")));
      hmOrderDetails.put("REMITADDRESS",
          GmCommonClass.parseNull((String) hmReturn.get("REMITADDRESS")));
      hmOrderDetails.put("REMITMAILADDRESS",
          GmCommonClass.parseNull((String) hmReturn.get("REMITMAILADDRESS")));
      hmOrderDetails.put("INVOICETITLE",
          GmCommonClass.parseNull((String) hmInData.get("INVOICETITLE")));
      hmOrderDetails.put("LOGOIMAGE", GmCommonClass.parseNull((String) hmReturn.get("LOGOIMAGE")));
     
     

      alOrderNums = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ORDERNUMS"));
      hmCartDetails = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("CARTDETAILS"));
      strPartyFl = GmCommonClass.parseNull((String) hmReturn.get("PARTYFLAG"));
      strInvNum = GmCommonClass.parseNull((String) hmOrderDetails.get("INV"));
      strParentInvId = GmCommonClass.parseNull((String) hmOrderDetails.get("PARENT_INV_ID"));
      strInvoiceNote = GmCommonClass.parseNull((String) hmOrderDetails.get("INVOICENOTE"));
      strCompId = GmCommonClass.parseNull((String) hmOrderDetails.get("COMPID"));
      strPayNm = GmCommonClass.parseNull((String) hmOrderDetails.get("PAYNM"));
      strPayNm = strPayNm.equals("") ? "Net 30 Days" : strPayNm;
      strInvComments = GmCommonClass.parseNull((String) hmOrderDetails.get("COMMENTS"));
      strTaxStartDateFl = GmCommonClass.parseNull((String) hmOrderDetails.get("TAX_START_DATE_FL"));
      strAdjStartDateFl = GmCommonClass.parseNull((String) hmOrderDetails.get("ADJ_START_DATE_FL"));
      strVAFSSFlag=GmCommonClass.parseNull((String) hmOrderDetails.get("VAFSSACCOUNT"));//PMT-22387-VA/FSS Account
     
      // strInvoiceAdj = GmCommonClass.parseNull((String) hmOrderDetails.get("TOTALINVADJ"));
      // //Total invoice adjustment
      // dbInvoiceAdjTotal = Double.parseDouble(strInvoiceAdj);
      // /log.debug("strInvoiceAdj>>>" + strInvoiceAdj);
      hmOrderDetails.put("PAYNM", strPayNm);
      hmOrderDetails.put("GLOBUSVATNUM",
          GmCommonClass.parseNull((String) hmOrderDetails.get("GLOBUSVATNUM")));
      hmOrderDetails.put("COUNTRYVATNUM", GmCommonClass.parseNull((String) hmInData.get("NIP")));
      //to get the ship details
      hmOrderDetails.putAll(gmInvoiceBean.fetchShipDtls(strInvNum));
    //to get the GST start flag
      hmOrderDetails.put("GSTSTARTFL", gmDOBean.getGSTStartFlag(strInvNum,"1201"));
    }
  //to get the financial year for invoice date
    String strFinMnthVal  = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("FIN_MNTH","FIN_YEAR_CALC",gmDataStoreVO.getCmpid()));
    strFinMnthVal = strFinMnthVal.equals("")?"0":strFinMnthVal;
    int intMnthVal = Integer.parseInt(strFinMnthVal);
    intMonth = dtInvDt.getMonth();
    // add 1900 to get the exact 4 digit year
    intYear = dtInvDt.getYear()+1900;
    if(intMonth >= intMnthVal){  //PC-4554: Financial Year change in Sales invoice
        strNxtYear = Integer.toString(intYear +1);
    }else{
        strNxtYear = Integer.toString(intYear -1);
    }
    strFinYear = strFinYear + intYear +"-"+ strNxtYear.substring(2, strNxtYear.length());
    hmOrderDetails.put("FINYEAR", strFinYear);

    int intSize = alOrderNums.size();
    int intLoop = 0;
    ArrayList alLoop = new ArrayList();
    // ArrayList allList = new ArrayList();
    ArrayList alHashMap = new ArrayList();
    HashMap hmTemp = new HashMap();
    HashMap hmLoop = new HashMap();
    String strVATPER = "";
    String strIGSTPER = "";
    String strCGSTPER = "";
    String strSGSTPER = "";
    String strConstructId = "";
    String strConstructNm = "";
    String strConstructValue = "";
    double vatPer = 0.0;  
    double igstPer = 0.0;
    double cgstPer = 0.0;
    double sgstPer = 0.0;
    double intQty = 0.0;
    double dbItemTotal = 0.0;
    double dbItemUnitPriceTotal = 0.0;
    double dbItemListPriceTotal = 0.0;
    double dbTotalItemVAT = 0.0;
    double dbTotalItemIGST = 0.0;
    double dbTotalItemCGST = 0.0;
    double dbTotalItemSGST = 0.0;
    double dbGrossTotal = 0.0;
    double dbRoundedGrossTotal = 0.0;
    double dbTotal = 0.0;
    double dblShip = 0.0;
    double dbOverAllTotal = 0.0;
    double dbOverAllShip = 0.0;
    double dbOverAllSubTotal = 0.0;
    double dbOverAllSubUPriceTotal = 0.0;
    double dbOverAllListPriceTotal = 0.0;
    double dbItemTotaltemp = 0.0;
    double dbItemVATTotaltemp = 0.0;
    double dbTotalItemTCS = 0.0;

    String strItemTotal = "";
    String strUnitPriceTotal = "";
    String strTotal = "";
    String strPrice = "";
    String strUnitPrice = "";
    String strListPrice = "";
    String strQty = "";
    String strNumInWords = "";
    String strOrderComments = "";
    String strPartDesc = "";
    int intConLoop = 0;
    String strOrderType = "";
    String strRAId = "";
    String strCurrency = "";
    String strTermsTitle = "";
    String strTitle = "";
    String strTaxCost = "";
    String strExclCmpny =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strInvoiceCompanyId, "EXCLUDECMPNY"));
    // Get rule value to show/hide shipping charges from invoice details section
    String strShipFieldFl =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("SHIPCHRG", "ENABLECOST",
            strInvoiceCompanyId));
    if (intSize == 0) {
      /*
       * hmLoop.put( "ORDERID", "-"); hmLoop.put( "ORDERDATE", "-"); hmLoop.put( "ITEMTOTAL", "-");
       * hmLoop.put( "VATPER", "-"); hmLoop.put( "ID", "-"); hmLoop.put( "PDESC", "-"); hmLoop.put(
       * "QTY", "0"); hmLoop.put( "PRICE", "0");
       */
      strCurrency = "$";
      hmOrderDetails.put("CURRENCY", strCurrency);
      hmOrderDetails.put("GROSSTOTAL", dbGrossTotal);
      hmOrderDetails.put("TOTALVAT", dbTotalItemVAT);
      hmOrderDetails.put("TOTALIGST", dbTotalItemIGST);
      hmOrderDetails.put("TOTALCGST", dbTotalItemCGST);
      hmOrderDetails.put("TOTALSGST", dbTotalItemSGST);
      hmOrderDetails.put("TOTALTCS", dbTotalItemTCS);
      hmOrderDetails.put("NETTOTAL", dbTotal);
      // alHashMap.add(hmLoop);
    }
    for (int i = 0; i < intSize; i++) {
      hmTemp = (HashMap) alOrderNums.get(i);
      strOrdId = (String) hmTemp.get("ID");
      dtOrderDate = (java.sql.Date) hmTemp.get("ORDERDATE");
      strOrderComments = GmCommonClass.parseNull((String) hmTemp.get("CMENT"));
      if (strOrdId.endsWith("S") != true) // remove return order informations
      {
        strOrderComments = GmCommonClass.parseNull((String) hmTemp.get("CMENT"));
        strOrderType = GmCommonClass.parseNull((String) hmTemp.get("ORDERTYPE"));
        strRAId = GmCommonClass.parseNull((String) hmTemp.get("RMAID"));
        // L to C swap (back order)
        if (strOrderType.equals("2525") && !strRAId.equals("")) {
          continue;
        }
        alLoop = GmCommonClass.parseNullArrayList((ArrayList) hmCartDetails.get(strOrdId));
        intLoop = alLoop.size();
        ArrayList alLoop1 = alLoop;
        // Below code is for construct pricing.
        alConsLoop = GmCommonClass.parseNullArrayList((ArrayList) hmConstructs.get(strOrdId));
        intConLoop = alConsLoop.size();
        if (intConLoop > 0) {
          for (int j = 0; j < intConLoop; j++) {
            hmConsLoop = (HashMap) alConsLoop.get(j);
            strConstructId = (String) hmConsLoop.get("CONID");
            strConstructNm = (String) hmConsLoop.get("CONNM");
            strConstructValue = (String) hmConsLoop.get("CVALUE");
            dbItemTotal = Double.parseDouble(strConstructValue);
            // dbTotal = dbTotal + dbItemTotal;

            strOrdId = "";
          }
        }

        for (int j = 0; j < intLoop; j++) {
          hmLoop = (HashMap) alLoop.get(j);
          strPartDesc =
              GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String) hmLoop.get("PDESC")));
          strVATPER = GmCommonClass.parseNull((String) hmLoop.get("VAT"));
          strIGSTPER = GmCommonClass.parseNull((String) hmLoop.get("IGST"));
          strCGSTPER = GmCommonClass.parseNull((String) hmLoop.get("CGST"));
          strSGSTPER = GmCommonClass.parseNull((String) hmLoop.get("SGST"));
          strTaxCost = GmCommonClass.parseZero((String) hmLoop.get("TAX_COST")); 
          strTCSPercent= GmCommonClass.parseZero((String) hmLoop.get("ACCOUNTTCS"));
          if (!strVATPER.equals("")) {
            vatPer = Double.parseDouble(strVATPER) / 100;
          }
          if (!strIGSTPER.equals("")) {
            igstPer = Double.parseDouble(strIGSTPER) / 100;
          }
          if (!strCGSTPER.equals("")) {
            cgstPer = Double.parseDouble(strCGSTPER) / 100;
          }
          if (!strSGSTPER.equals("")) {
            sgstPer = Double.parseDouble(strSGSTPER) / 100;
          }
          strPrice = GmCommonClass.parseNull((String) hmLoop.get("PRICE"));
          strUnitPrice = GmCommonClass.parseZero((String) hmLoop.get("UNITPRICE"));
          strQty = GmCommonClass.parseNull((String) hmLoop.get("QTY"));
          strListPrice = GmCommonClass.parseZero((String) hmLoop.get("LISTPRICE"));
          intQty = Double.parseDouble(strQty);
          dbItemTotal = Double.parseDouble(strPrice);
          dbItemUnitPriceTotal = Double.parseDouble(strUnitPrice);
          dbItemListPriceTotal = Double.parseDouble(strListPrice);
          dbItemTotal = GmCommonClass.roundDigit((intQty * dbItemTotal), 2); // Multiply by Qty
          dbItemUnitPriceTotal = GmCommonClass.roundDigit((intQty * dbItemUnitPriceTotal), 2); // Multiply
                                                                                               // by
                                                                                               // Qty
          dbItemListPriceTotal = GmCommonClass.roundDigit((intQty * dbItemListPriceTotal), 2);
          
          if (!strVATPER.equals("0")) {
            dbTotalItemVAT = dbTotalItemVAT + Double.parseDouble(strTaxCost);
            dbItemTotaltemp =
                dbItemTotal
                    + GmCommonClass.parseDouble(((Double) hmOrderDetails.get("TOTALPRICE"
                        + strVATPER)));
            hmOrderDetails.put("TOTALPRICE" + strVATPER, dbItemTotaltemp);

            dbItemVATTotaltemp =
                GmCommonClass.roundDigit((dbItemTotal * vatPer), 2)
                    + GmCommonClass.parseDouble(((Double) hmOrderDetails.get("TOTALVATPRICE"
                        + strVATPER)));
            hmOrderDetails.put("TOTALVATPRICE" + strVATPER, dbItemVATTotaltemp);
          }
          dbTotalItemIGST = !strIGSTPER.equals("")? dbTotalItemIGST + (dbItemTotal * igstPer) : dbTotalItemIGST;
          dbTotalItemCGST = !strCGSTPER.equals("")? dbTotalItemCGST + (dbItemTotal * cgstPer) : dbTotalItemCGST;
          dbTotalItemSGST = !strSGSTPER.equals("")? dbTotalItemSGST + (dbItemTotal * sgstPer) : dbTotalItemSGST; 
          strItemTotal = "" + dbItemTotal;
          strUnitPriceTotal = "" + dbItemUnitPriceTotal;
          dbTotal = dbTotal + dbItemTotal;

          strTotal = "" + dbTotal;
          hmLoop.put("ORDERID", strOrdId);
          hmLoop.put("ORDERDATE", new java.util.Date(dtOrderDate.getTime()));
          hmLoop.put("ITEMTOTAL", strItemTotal);
          hmLoop.put("ITEMUNITPRICETOTAL", strUnitPriceTotal);
          hmLoop.put("VATPER", strVATPER.equals("0") ? "-" : strVATPER);
          hmLoop.put("PDESC", strPartDesc);
          hmLoop.put("ACTUALPRICE", strPrice);

          alHashMap.add(hmLoop);
          // Shiiping information by orderwise.
          if (j == intLoop - 1) {
            HashMap hmLoopShipInfo = new HashMap();
            String strShipCost = GmCommonClass.parseZero((String) hmLoop.get("SCOST"));
            dblShip = Double.parseDouble(strShipCost);

            if ((strTaxStartDateFl.equalsIgnoreCase("Y") || strAdjStartDateFl.equalsIgnoreCase("Y"))
                && (!strShipFieldFl.equals("Y"))) {
              dbOverAllShip = dbOverAllShip + dblShip;
            } else {
              dbTotal = dbTotal + dblShip;
            }

            strTotal = "" + dbTotal;
            hmLoopShipInfo.put("ORDERID", strOrdId);
            hmLoopShipInfo.put("ID", "");
            hmLoopShipInfo.put("PDESC", "Shipping Charges ");
            hmLoopShipInfo.put("QTY", "1");
            hmLoopShipInfo.put("PRICE", strShipCost);

            hmLoopShipInfo.put("CUST_PNUM", "-9999");
            hmLoopShipInfo.put("CUST_PDESC", "-9999");

            if ((strShipFieldFl.equals("Y"))
                || (!(strTaxStartDateFl.equalsIgnoreCase("Y") || strAdjStartDateFl
                    .equalsIgnoreCase("Y")))) {
              alHashMap.add(hmLoopShipInfo);
            }
          }
          // Tax
          dbOverAllSubTotal = dbOverAllSubTotal + dbItemTotal;
          dbOverAllListPriceTotal = dbOverAllListPriceTotal + dbItemListPriceTotal;
          dbOverAllSubUPriceTotal = dbOverAllSubUPriceTotal + dbItemUnitPriceTotal;
        }

        dbGrossTotal = dbTotalItemVAT+dbTotalItemIGST+dbTotalItemCGST+dbTotalItemSGST+dbTotal;
        strNumInWords =
            GmCommonClass.parseNull(GmCommonClass.convetToPolishWords(dbGrossTotal, getCompId()));
        hmOrderDetails.put("GROSSTOTAL", dbGrossTotal);
        hmOrderDetails.put("TOTALVAT", dbTotalItemVAT);
        hmOrderDetails.put("TOTALIGST", dbTotalItemIGST);
        hmOrderDetails.put("TOTALCGST", dbTotalItemCGST);
        hmOrderDetails.put("TOTALSGST", dbTotalItemSGST);
        hmOrderDetails.put("IGST", strIGSTPER.equals("0") ? "-" : strIGSTPER);
        hmOrderDetails.put("CGST", strCGSTPER.equals("0") ? "-" : strCGSTPER);
        hmOrderDetails.put("SGST", strSGSTPER.equals("0") ? "-" : strSGSTPER);
        hmOrderDetails.put("TCS", strTCSPercent.equals("0") ? "-" : strTCSPercent);
        strCurrency = GmCommonClass.parseNull((String) hmOrderDetails.get("CURRENCY"));
        if (strCurrency.equals("USD") || strCurrency.equals("")) {
          strCurrency = "$";
        }
        dbRoundedGrossTotal = (double)Math.round(dbGrossTotal);
        hmOrderDetails.put("ROUNDEDGROSSTOTAL",dbRoundedGrossTotal);
        hmOrderDetails.put("ROUNDOFFAMOUNT", (double)(Math.round(dbGrossTotal)-dbGrossTotal));  
        hmOrderDetails.put("CURRENCY", strCurrency);
        hmOrderDetails.put("NETTOTAL", dbTotal);
        hmOrderDetails.put("GRANDTOTALINWORDS", strNumInWords);
      }
    }
    // Total Adjustments of the invoice [Total After adjustment - Total Before Adjustment]
    dbInvoiceAdjTotal = dbOverAllSubTotal - dbOverAllSubUPriceTotal;
    // Sales Tax Total Section
    dbOverAllTotal = dbOverAllSubTotal + dbOverAllShip + dbTotalItemVAT;
    // Total after reducing the adjustments and adding the shipping charge and vat amount
    dbOverAllAdjTotal =
        dbOverAllSubUPriceTotal + dbOverAllShip + dbTotalItemVAT + dbInvoiceAdjTotal;
    // /dbOverAllAdjTotal = dbOverAllTotal + dbInvoiceAdjTotal;
    hmOrderDetails.put("SUBTOTAL", dbOverAllSubTotal); //
    hmOrderDetails.put("LISTPRICESUBTOTAL", dbOverAllListPriceTotal); 
    hmOrderDetails.put("SUBUNITPRICETOTAL", dbOverAllSubUPriceTotal);
    hmOrderDetails.put("SHIPCHARGE", dbOverAllShip);
    hmOrderDetails.put("SALESTAX", dbTotalItemVAT);
    hmOrderDetails.put("OVERALLTOTAL", dbOverAllTotal);
    hmOrderDetails.put("OVERALLADJTOTAL", dbOverAllAdjTotal);
    hmOrderDetails.put("TOTALINVADJ", dbInvoiceAdjTotal);
  
    

    String strInvType = GmCommonClass.parseNull((String) hmOrderDetails.get("INVTYPE"));
    if ((strInvType.equals("50202") || strInvType.equals("50203"))) {
      strCreditInvId = strInvNum;
      strInvNum = strParentInvId;
    }
    String strVoidFlag = GmCommonClass.parseNull((String) hmInData.get("hVoidFlag"));
    String strWaterMark = "";

    if (strVoidFlag.equals("VoidInvoice")) {
      strWaterMark = "voidinvoice";
    }
    if (strInvType.equals("50201")) {
      strWaterMark = "obo";
    }
    hmOrderDetails.put("WATERMARK", strWaterMark);
    String strTermsField = "";
    // String strTermsTitle = "";//(strInvType.equals("50202")) ? "Credit Memo # ":"Terms";
    // Added Debit memo.
    if (strInvType.equals("50202")) {
      strTermsTitle =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.CREDIT_MEMO_LBL"));
      strTermsField = strCreditInvId;
    } else if (strInvType.equals("50203")) {
      strTermsTitle =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.DEBIT_MEMO_LBL"));
      strTermsField = strCreditInvId;
      strTitle = "Debit Memo";
    } else {
      strTermsTitle = "Terms";
      strTermsField = strPayNm;
    }

    hmOrderDetails.put("TERMSTITLE", strTermsTitle);
    hmOrderDetails.put("TERMSFIELD", strTermsField);
    hmOrderDetails.put("INV", strInvNum);

    String strCompNm = "Globus Medical";


    if (strTaxStartDateFl.equalsIgnoreCase("Y") || strAdjStartDateFl.equalsIgnoreCase("Y")) {
    	 if (strVAFSSFlag.equals("Y")) {//PMT-22387-VA/FSS Account
    	        strCompNm = "Globus Medical, Inc.";
    }else{
      strCompNm = "Globus Medical North America, Inc.";
     }
    }
   
    String strPayLblCompNm =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.PAYLABEL"));

    strCompNm = strPayLblCompNm.equals("") ? strCompNm : strPayLblCompNm;

    if (!strPartyFl.equals("Y")) {
      strPayLbl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.PAYLBLTXT"));
      strPayLbl = strPayLbl.replace("<<COMP_NAME>>", strCompNm);
    }
    if (strInvoiceNote.equals("Y")) {
      strDiscLbl =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.DISCOUNTLBLTXT"));
    }
    hmOrderDetails.put("PAYLBL", strPayLbl + strDiscLbl);
    if (strInvComments.equalsIgnoreCase("")) {
      hmOrderDetails.put("COMMENTS", strOrderComments);
    } else {
      hmOrderDetails.put("COMMENTS", strInvComments);
    }
    if (alPaidMonth.size() != 0) {
      for (int i = 0; i < alPaidMonth.size(); i++) {
        HashMap hmPaidMonth = GmCommonClass.parseNullHashMap((HashMap) alPaidMonth.get(i));
        alHashMap.add(hmPaidMonth);
      }
    }
    if (!strTCSPercent.equals("")) {
        tcsPer = Double.parseDouble(strTCSPercent) / 100;
    }
	dbTotalItemTCS = !strTCSPercent.equals("")? dbTotalItemTCS + (dbGrossTotal * tcsPer) : dbTotalItemTCS;
    hmOrderDetails.put("TOTALTCS", dbTotalItemTCS);
    dbGrossTotal = dbGrossTotal + dbTotalItemTCS;
    dbRoundedGrossTotal = (double)Math.round(dbGrossTotal);
    hmOrderDetails.put("ROUNDEDGROSSTOTAL",dbRoundedGrossTotal);
    hmOrderDetails.put("ROUNDOFFAMOUNT", (double)(Math.round(dbGrossTotal)-dbGrossTotal));  
    if (!strCompId.equals("1000") && !strCompId.equals("1001")) {
      // OUS code - to setting the invoice amount in word
      HashMap hmInvAmount = gmInvoiceBean.fetchInvAmountInWord(hmOrderDetails);
      hmOrderDetails.putAll(hmInvAmount);
    }
    hmOrderDetails.put("RECIPIENTLABEL", strPrintPurposelbl);
    String strSealImage=GmCommonClass.parseNull(gmCompResourceBundleBean.getProperty("COMP_SEAL"));
    String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
    hmOrderDetails.put("SEALIMAGE",strImagePath+strSealImage+".gif");
    hmOrderDetails.put("SUBREPORT_DIR",
        GmCommonClass.parseNull((String) hmReturn.get("SUBREPORT_DIR")));
    hmOrderDetails.put("REPORTSIZE", alHashMap.size());
    hmOrderDetails.put("RECORDCOUNT", alHashMap.size());
    hmOutData.put("HMORDERDETAILS", hmOrderDetails);
    hmOutData.put("ALORDERLIST", alHashMap);
    hmOutData.put("DISPATCHTO", GmCommonClass.parseNull((String) hmPrintInvDtls.get("DISPATCHTO")));
    return hmOutData;
  }
  
  /**
   * fetchShipDtls - This method is to fetch the carrier and tracking number for invoice
   * @param String
   * @return HashMap
   * @exception AppError
   */
 public HashMap fetchShipDtls(String strInvoiceId) throws AppError{
     HashMap hmReturn = new HashMap();
     GmDBManager gmDBManager = new GmDBManager();
     gmDBManager.setPrepareString("gm_pkg_ac_invoice_info.gm_fch_ship_dtls", 3);
     gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
     gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
     gmDBManager.setString(1, strInvoiceId);
     gmDBManager.execute();
     hmReturn.put("SHIPVIA", gmDBManager.getString(2));
     hmReturn.put("TRACKNUM", gmDBManager.getString(3));
     gmDBManager.close();
     return hmReturn;
     
 }
 
 
 public HashMap reportOUSDashboard(String strAccCurrId,String strflg) throws AppError {
   GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
   GmICTSummaryBean gmIct = new GmICTSummaryBean(getGmDataStoreVO());

   HashMap hmReturn = new HashMap();
   HashMap hmSales = new HashMap();
   RowSetDynaClass rsdReturnInvoice = null;
   RowSetDynaClass rsdReturns = null;
   ArrayList alReturns = new ArrayList();


   rsdReturnInvoice = reportDashboardOUSInvoice(strAccCurrId,strflg); // Invoices Dashboard
   hmReturn.put("INVOICE", rsdReturnInvoice);
   log.debug(" Count in Invoice " + rsdReturnInvoice.getRows().size());

   rsdReturns = gmIct.getOusReturnsDashboardDyna("ACCT", strAccCurrId); // Returns Dashboard
   // rsdReturns = rsdReturns.getRows(alReturns);
   hmReturn.put("RETURNS", rsdReturns);
   log.debug(" Count in Pending Credut " + rsdReturns.getRows().size());
   return hmReturn;

 } 

 public RowSetDynaClass reportDashboardOUSInvoice(String strAccCurrId,String strflg) throws AppError {
   GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

   String strBeanMsg = "";
   String strPrepareString = null;

   RowSetDynaClass resultSet = null;
   ResultSet rs = null;


   gmDBManager.setPrepareString("GM_REPORT_OUSDIST_INV_DASH", 4);

   /*
    * register out parameter and set input parameters
    */
   gmDBManager.setString(1, strAccCurrId);
   gmDBManager.setString(2, strAccCurrId);
   gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
   gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

   gmDBManager.execute();
   strBeanMsg = gmDBManager.getString(3);
   rs = (ResultSet) gmDBManager.getObject(4);
   resultSet = gmDBManager.returnRowSetDyna(rs);
   gmDBManager.close();


   return resultSet;
 } 
 
 
 public String getPoOrderType(String strPo,String strAccid) throws AppError {
	 String strOrdtyp = "";
	 log.debug("strPo"+strPo);
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setFunctionString("get_po_order_type", 2);
	    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
	    gmDBManager.setString(2, strPo);
	    gmDBManager.setString(3, strAccid);
	    gmDBManager.execute();
	    strOrdtyp = gmDBManager.getString(1);
	    log.debug("getPoOrderType"+strOrdtyp);
	    gmDBManager.close();

	    return strOrdtyp;
	  }
 
 public String getInvoiceOrder(String strInvoiceId) throws AppError {
	 String strOrdid = "";
	 log.debug("strInvoiceId"+strInvoiceId);
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setFunctionString("get_order_invoice", 1);
	    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
	    gmDBManager.setString(2, strInvoiceId);
	    gmDBManager.execute();
	    strOrdid = gmDBManager.getString(1);
	    log.debug("strOrdid"+strOrdid);
	    gmDBManager.close();

	    return strOrdid;
    }
 /**
  * This method save invoice file id
  * 
  * @param String
  * @return Void
  * @throws AppError
  */
 public void saveInvoiceFileRef (String strInvoiceId,String strFileType,String strUserID) 
		 throws AppError {
	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
   	gmDBManager.setPrepareString("gm_pkg_ac_invoice_txn.gm_sav_invoice_file_ref", 3);	
   	gmDBManager.setString(1, strInvoiceId);
   	gmDBManager.setString(2, strFileType);
   	gmDBManager.setString(3, strUserID);
   	gmDBManager.execute();
   	gmDBManager.commit();
 }	
 
	/**
	 *
	 * fetchInvoicePaperworkDtls - This method used to fetch the customer
	 * invoice details based on the party This method called form all the
	 * invoice paperwork
	 * 
	 * @param strInvoiceId
	 * @return Hashmap
	 * @throws AppError
	 */
 
 public HashMap fetchInvoicePaperworkDtls(String strInvoiceId) throws AppError {
   HashMap hmReturn = new HashMap();
   GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
   gmDBManager.setPrepareString("gm_pkg_ac_invoice.gm_fch_cust_invoice_paperwork_dtls", 2);
   gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
   gmDBManager.setString(1, strInvoiceId);
   gmDBManager.execute();
   hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
   gmDBManager.close();
   return hmReturn;
 }
/**
 *
 * fetchParentInvDt - This method used to fetch parent invoice date
 * 
 * @param strInvId
 * @return strInvDt
 * @throws AppError
 */
 public String fetchParentInvDt(String strInvId) throws AppError{
	 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	 
	 gmDBManager.setPrepareString("gm_pkg_temp_vat_upd.gm_fch_parent_inv_date", 2);
	 gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
	 gmDBManager.setString(1, strInvId);
	 gmDBManager.execute();
	 String strInvDt = GmCommonClass.parseNull((String)gmDBManager.getString(2));
	 gmDBManager.close();
	 
	 return strInvDt;
 }
}
