package com.globus.accounts.beans;

import java.io.File;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.quartz.utils.DBConnectionManager;

import antlr.collections.List;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.excel.GmExcelManipulation;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmFieldSalesAuditBean extends GmBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	String strMasterFile = "";	
	String strBaseDirToUpload = "";
	
	public GmFieldSalesAuditBean()
	{
		super(GmCommonClass.getDefaultGmDataStoreVO());
		strMasterFile = GmCommonClass.getString("FIELDSALESMASTERFILE");
		strBaseDirToUpload = GmCommonClass.getString("FIELDSALESUPLOADDIR");
	}
	
	/**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmFieldSalesAuditBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	    strMasterFile = GmCommonClass.getString("FIELDSALESMASTERFILE");
		strBaseDirToUpload = GmCommonClass.getString("FIELDSALESUPLOADDIR");
	  }
	
	private void loadExcelData(HashMap hmParam) throws Exception
	{		
	 String strInputString =GmCommonClass.parseNull((String)hmParam.get("INPUTSTRING"));
	 String strDistName = GmCommonClass.parseNull((String)hmParam.get("DISTNAME"));
	 String strDistID = GmCommonClass.parseNull((String)hmParam.get("DISTID"));
	 
	ArrayList alHospitalInfo = GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("HOSPITALINFO"));
	ArrayList alRepInfo = GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("REPINFO"));
	ArrayList alDistInfo = GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("DISTINFO"));
	ArrayList alMasterPartSetInfo = GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("MASTERPARTSETINFO"));
	ArrayList alMasterCnumInfo = GmCommonClass.parseNullArrayList((ArrayList)hmParam.get("MASTERCONTROLNUM"));
	
	//log.debug("Master Control Number: "+alMasterCnumInfo);
	
	 String[] strAuditorArray = strInputString.split("\\|");
	 String strAuditorDetails = "";
	 String[] strArrAuditorDetails;
	 String strAuditorID="", strAuditorName="";
	 
	 String strAuditorNameTemp = "";
	 String strDistNameTemp = strDistName.replaceAll(" ", "");
	 String strExcelFileNameToPopulate = ""; 
	 String strExcelUploadDirName = "";

	 for(int index=0; index < strAuditorArray.length; index++)
	 {
		 strAuditorDetails = strAuditorArray[index];
		 strArrAuditorDetails = strAuditorDetails.split("\\^");
		 
		 strAuditorID = strArrAuditorDetails[0];
		 strAuditorName = strArrAuditorDetails[1];
		 strAuditorNameTemp = strAuditorName.replaceAll(" ", "");
		 
		// strExcelUploadDirName = createRequiredDirectory(strDistName); //strBaseDirToUpload + "\\" + GmCalenderOperations.getCurrentDate()+"\\" + strDistNameTemp;		 
		 strExcelFileNameToPopulate = strBaseDirToUpload+"\\" +"Master_Audit_Data_"+strDistNameTemp+"_"+strAuditorNameTemp + ".xls";
		 
		 
		 log.debug("strExcelFileNameToPopulate: "+strExcelFileNameToPopulate);
		 
		 try {
			 hmParam.put("AUDITORID", strAuditorID);		 
			 ArrayList alTagInfo = fetchTagIds(hmParam);
			// log.debug("alTagInfo: "+alTagInfo);
			 
			 if(!new File(strExcelFileNameToPopulate).exists())
			 {
				 log.debug("File Exists");
				 GmExcelManipulation.copyFile(strMasterFile, strExcelFileNameToPopulate);
				 loadSanityCheck(strAuditorID, strAuditorName, strDistID, strDistName, strExcelFileNameToPopulate);
				 loadHospitalInfo(strAuditorID, strAuditorName, strExcelFileNameToPopulate, alHospitalInfo);
			 	 loadRepInfo(strAuditorID, strAuditorName, strExcelFileNameToPopulate, alRepInfo);
				 loadDistInfo(strAuditorID, strAuditorName, strExcelFileNameToPopulate, alDistInfo);
				 loadMasterPartSet(strAuditorID, strAuditorName, strExcelFileNameToPopulate, alMasterPartSetInfo);
				 loadMasterControlNum(strAuditorID, strAuditorName, strExcelFileNameToPopulate, alMasterCnumInfo);
				 loadTagIds(strAuditorID, strAuditorName, strExcelFileNameToPopulate, alTagInfo);
			 }
			 else
			 {
				 log.debug("File Not Exists, Modifying the existing file");
				 loadTagIds(strAuditorID, strAuditorName, strExcelFileNameToPopulate, alTagInfo);
			 }
		 }
		 catch(Exception ex)
		 {
			 GmExcelManipulation.delete(strExcelFileNameToPopulate);
			 throw new Exception(ex);
		 }
	 }	 
	}
	
	private String createRequiredDirectory(String strDistName)
	{
		String strDirName =strBaseDirToUpload + "\\" + GmCalenderOperations.getCurrentDate("yyyyMMdd") + "\\"+ strDistName ;
		
		File dir = new File(strDirName);
		if(!dir.exists())
		{
			dir.mkdirs();
		}
	 return strDirName;
	}
	
	private void loadSanityCheck(String strAuditorId, String strAuditorName, String strDistID, String strDistName, String strFileName) throws Exception
	{		
			GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();			
			gmExcelManipulation.setExcelFileName(strFileName);
			
			gmExcelManipulation.setSheetNumber(0);
			gmExcelManipulation.setRowNumber(1);
			gmExcelManipulation.setColumnNumber(3);			
			gmExcelManipulation.writeToExcel(strDistName);
	
			gmExcelManipulation.setSheetNumber(0);
			gmExcelManipulation.setRowNumber(1);
			gmExcelManipulation.setColumnNumber(5);			
			gmExcelManipulation.writeToExcel(strAuditorName);
			
			gmExcelManipulation.setSheetNumber(0);
			gmExcelManipulation.setRowNumber(2);
			gmExcelManipulation.setColumnNumber(3);			
			gmExcelManipulation.writeToExcel(strDistID);
			
			gmExcelManipulation.setSheetNumber(0);
			gmExcelManipulation.setRowNumber(2);
			gmExcelManipulation.setColumnNumber(5);			
			gmExcelManipulation.writeToExcel(strAuditorId);	
		
	}
	
	private void loadHospitalInfo(String strAuditorId, String strAuditorName, String strFileName, ArrayList alDataList) throws Exception
	{		
			GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();			
			gmExcelManipulation.setExcelFileName(strFileName);
			gmExcelManipulation.setSheetNumber(2);
			gmExcelManipulation.setRowNumber(3);
			gmExcelManipulation.setColumnNumber(13);			
			gmExcelManipulation.writeToExcel(alDataList);		
	}
	
	private void loadRepInfo(String strAuditorId, String strAuditorName, String strFileName, ArrayList alDataList) throws Exception
	{		
			GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();			
			gmExcelManipulation.setExcelFileName(strFileName);
			gmExcelManipulation.setSheetNumber(2);
			gmExcelManipulation.setRowNumber(3);
			gmExcelManipulation.setColumnNumber(16);			
			gmExcelManipulation.writeToExcel(alDataList);			
	}
	
	private void loadDistInfo(String strAuditorId, String strAuditorName, String strFileName, ArrayList alDataList)  throws Exception
	{		
			GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();			
			gmExcelManipulation.setExcelFileName(strFileName);
			gmExcelManipulation.setSheetNumber(2);
			gmExcelManipulation.setRowNumber(3);
			gmExcelManipulation.setColumnNumber(19);			
			gmExcelManipulation.writeToExcel(alDataList);
	}
	
	private void loadMasterPartSet(String strAuditorId, String strAuditorName, String strFileName, ArrayList alDataList) throws Exception
	{
			GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();			
			gmExcelManipulation.setExcelFileName(strFileName);
			gmExcelManipulation.setSheetNumber(3);
			gmExcelManipulation.setRowNumber(1);
			gmExcelManipulation.setColumnNumber(1);			
			gmExcelManipulation.writeToExcel(alDataList);
	}
	
	private void loadMasterControlNum(String strAuditorId, String strAuditorName, String strFileName, ArrayList alDataList) throws Exception
	{
			GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();			
			gmExcelManipulation.setExcelFileName(strFileName);
			gmExcelManipulation.setSheetNumber(4);
			gmExcelManipulation.setRowNumber(1);
			gmExcelManipulation.setColumnNumber(1);			
			gmExcelManipulation.writeToExcel(alDataList);		
	}
	
	private void loadTagIds(String strAuditorId, String strAuditorName, String strFileName, ArrayList alDataList) throws Exception
	{		
			GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();			
			gmExcelManipulation.setExcelFileName(strFileName);
			gmExcelManipulation.setSheetNumber(1);
			gmExcelManipulation.setRowNumber(2);
			gmExcelManipulation.setColumnNumber(0);		
			gmExcelManipulation.setCursorToNextEditableRow();
			gmExcelManipulation.writeToExcel(alDataList);		
	}
	
	public ArrayList fetchAuditDetails() throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_fetch_audit_details", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));//   returnHashMap((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alReturn;
	}
	
	public ArrayList fetchDistributorDetails(String strAuditId) throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_fetch_distributor", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));//   returnHashMap((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alReturn;
	}
	
	public ArrayList fetchAuditorName(String strAuditId) throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_fetch_Auditor", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));//   returnHashMap((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alReturn;
	}
	
	/**
	 * loadAuditDetails - This method will load Audit details
	 * 
	 * @param  -hmParam 
	 *			Distributor Id, Audit Id, User Id, InputString
	 * @exception AppError
	 */
	public void loadAuditDetails(HashMap hmParam ) throws Exception {
		
		log.debug("hmParam: "+hmParam); 
				
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
		String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
		HashMap hmExcelData = new HashMap();
		
		GmDBManager gmDBManager = new GmDBManager();
		
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_ac_ld_lock",4);
		gmDBManager.setString(1, strDistId);
		gmDBManager.setInt(2, Integer.parseInt(strAuditId));
		gmDBManager.setString(3, strUserId);
		gmDBManager.setString(4, strInputString);
		gmDBManager.execute();
		
		hmParam.put("GMDBMANAGER", gmDBManager);
		
		hmExcelData = fetchExcelData(hmParam);
		
		log.debug("MASTERCONTROLNUM : "+ hmExcelData.get("MASTERCONTROLNUM"));
		
		hmParam.putAll(hmExcelData);	
		
		loadExcelData(hmParam);
		
		gmDBManager.commit();				
		log.debug("loadAuditDetails executed successfully");
	}
	
	/**
	 * fetchExcelData - This method will fetch the excel data
	 * 
	 * @param  -
	 *            Distributor ID
	 * @exception AppError
	 */
	private HashMap fetchExcelData(HashMap hmParam) throws AppError {
		
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
		GmDBManager gmDBManager = (GmDBManager)hmParam.get("GMDBMANAGER");
		
		HashMap hmReturn = new HashMap();

		log.debug("strDistId  is  " + strDistId);
		ArrayList alHospitalInfo = new ArrayList();
		ArrayList alRepInfo = new ArrayList();
		ArrayList alDistributorInfo = new ArrayList();
		ArrayList alMasterPartSet= new ArrayList();
		ArrayList alMasterCntlNo = new ArrayList();

		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_fch_excel_data", 6);

		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strDistId));
		gmDBManager.execute();

		alHospitalInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		alRepInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		alDistributorInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		alMasterPartSet = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
		alMasterCntlNo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
		gmDBManager.close();
		
		hmReturn.put("HOSPITALINFO", alHospitalInfo);
		hmReturn.put("REPINFO", alRepInfo);
		hmReturn.put("DISTINFO", alDistributorInfo);
		hmReturn.put("MASTERPARTSETINFO", alMasterPartSet);
		hmReturn.put("MASTERCONTROLNUM", alMasterCntlNo);
		
		//log.debug("MASTERCONTROLNUM : " +alMasterCntlNo);
		
		return hmReturn;
		
	}	
	/**
	 * fetchTagIds - This method will fetch all tags for auditor
	 * 
	 * @param -hmParam 
	 *            Distributor ID, Audit Id, Auditor Id
	 * @exception AppError
	 */
	private ArrayList fetchTagIds(HashMap hmParam) throws AppError 
	{
		//log.debug("hmParam: "+hmParam);
		GmDBManager gmDBManager = (GmDBManager)hmParam.get("GMDBMANAGER");
		String strDistId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
		String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
		String strAuditorId = GmCommonClass.parseNull((String) hmParam.get("AUDITORID"));
		
		//log.debug("strDistId: "+strDistId +"strAuditId: "+strAuditId+"strAuditorId: "+strAuditorId);
				
		ArrayList alReturn = new ArrayList();
		
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_fch_auditor_tags", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1, strDistId);
		gmDBManager.setInt(2, Integer.parseInt(strAuditId));
		gmDBManager.setString(3, strAuditorId);
		
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		gmDBManager.close();
		return alReturn;
	}
	/* This Method will fetch all audit Lock details for the passed in field sales audit and distributor 
	 * @param - 
	 */
	public RowSetDynaClass fetchAuditLockReport(String  strAuditId, String strDistId) throws AppError{
		
		GmDBManager gmDBManager = new GmDBManager();
		RowSetDynaClass rdAudituploadReport = null;
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_fetch_audit_upload_info", 5);
		gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strDistId));
		gmDBManager.setString(3, "0");
		gmDBManager.setString(4, "90903");
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.execute();
		rdAudituploadReport = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));		
		gmDBManager.close();
		return rdAudituploadReport;
		
	}
	
	public ArrayList fetchLockedDistributorDetails(String strAuditId) throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_fetch_locked_distributor", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));//   returnHashMap((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alReturn;
	}
	
	public RowSetDynaClass fetchAuditUploadInfo( String strAuditId, String strDistId,String  strAuditorId) throws AppError
	{
		GmDBManager gmDBManager = new GmDBManager();
		RowSetDynaClass rdAudituploadReport = null;
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_fetch_audit_upload_info", 5);
		gmDBManager.setString(1, GmCommonClass.parseNull(strAuditId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strDistId));
		gmDBManager.setString(3, GmCommonClass.parseNull(strAuditorId));
		gmDBManager.setString(4, "90904");
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.execute();
		rdAudituploadReport = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));		
		gmDBManager.close();
		return rdAudituploadReport;
	}
	
	public void uploadExcelData(HashMap hmParam) throws Exception
	{
		GmDBManager gmDBManager = new GmDBManager();		
		String strFileName=GmCommonClass.parseNull((String)hmParam.get("FILENAME"));
		log.debug("Uploaded File"+strFileName);
		GmExcelManipulation gmExcelManipulation = new GmExcelManipulation();                 
		gmExcelManipulation.setExcelFileName(strFileName);
        gmExcelManipulation.setSheetNumber(5);
        gmExcelManipulation.setRowNumber(2);
        gmExcelManipulation.setColumnNumber(3);         
        gmExcelManipulation.setMaxColumnNumberToRead(14);
		ArrayList alList= (ArrayList)gmExcelManipulation.readFromExcel();
		log.debug("List : "+alList.size());	
		batchInsert(alList, gmDBManager);
		uploadData(hmParam, gmDBManager);
		gmDBManager.commit();
	}
	
	public void batchInsert(ArrayList alInsertList, GmDBManager gmDBManager) throws Exception
	{
		Connection conn = null;
		Statement stmt 	= null;
		DBConnectionWrapper connectionWrapper = new DBConnectionWrapper();
		try {		
		conn = connectionWrapper.getConnection();
		String insertQuery="";
		String strKey ="";
		
		stmt =conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		stmt.addBatch("DELETE FROM TEMP_AUDIT_STATUS_TAG");
		
		for (int i=0;i<alInsertList.size();i++ )
		{
			insertQuery="";
			HashMap hmRow=(HashMap)alInsertList.get(i);

			Iterator rowIterator =  hmRow.keySet().iterator();
			
			while(rowIterator.hasNext())
			{
				strKey = (String)rowIterator.next();
				insertQuery+= GmCommonClass.parseNull((String)hmRow.get(strKey));
			}

			if(!insertQuery.equals(""))
			{	
				log.debug(insertQuery);
				stmt.addBatch(insertQuery);
			}
		}
		
		int[] updateCounts = stmt.executeBatch();
		log.debug("Updated Count : "+updateCounts[0]);	
		conn.commit();
	   }
		catch(BatchUpdateException b) {
			conn.rollback();
	        log.error("SQLException: " + b.getMessage(), b);	
	        throw b;
	    }
	    catch(SQLException ex) {
	    	conn.rollback();
	        log.error("SQLException: " + ex.getMessage(), ex);
	        throw ex;
	    }
	    catch(Exception e) {
	    	conn.rollback();
	    	log.error("Exception: " + e.getMessage(), e);
	    	throw e;
	    }
	    finally {	        
	        stmt.close();		
	        conn.close();
	    }
	}
	
	public void uploadData(HashMap hmParam, GmDBManager gmDBManager1) throws AppError
	{
		String strAuditId=GmCommonClass.parseNull((String)hmParam.get("AUDITID"));
		String strDistId=GmCommonClass.parseNull((String)hmParam.get("DISTID"));
		String strRefTypeId=GmCommonClass.parseNull((String)hmParam.get("REFTYPE"));
		String strFileName=GmCommonClass.parseNull((String)hmParam.get("UPLOADFILENAME"));
		String strUserId=GmCommonClass.parseNull((String)hmParam.get("STRSESSUSERID"));
		String strAuditorId=GmCommonClass.parseNull((String)hmParam.get("AUDITOR"));
		
		log.debug("Hm Param" + hmParam);
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_ac_field_sales.gm_ac_ld_upload_data", 9);
		gmDBManager.setString(1, GmCommonClass.parseNull(strDistId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strAuditId));
		gmDBManager.setString(3, GmCommonClass.parseNull(strRefTypeId));
		gmDBManager.setString(4, GmCommonClass.parseNull(strFileName));
		gmDBManager.setString(5, GmCommonClass.parseNull(strUserId));
		gmDBManager.setString(6, GmCommonClass.parseNull(strAuditorId));
		gmDBManager.registerOutParameter(7, OracleTypes.CHAR);
		gmDBManager.registerOutParameter(8, OracleTypes.CHAR );
		gmDBManager.registerOutParameter(9, OracleTypes.CHAR );
		gmDBManager.execute();
		
		String errTag= GmCommonClass.parseNull(gmDBManager.getString(7));
		String entryErrTag= GmCommonClass.parseNull(gmDBManager.getString(8));
		String strFileId= GmCommonClass.parseNull(gmDBManager.getString(9));
		gmDBManager.commit();

		
	if(!errTag.equals("")||!entryErrTag.equals(""))
		{	
			throw new AppError("Error Tags for the Tag Table :"+errTag+";<br>Error Tags for the Entry Table"+entryErrTag, "", 'S');
		}
		throw new AppError("Upload Sucessful for the File ID"+strFileId, "", 'S');
	}
}
