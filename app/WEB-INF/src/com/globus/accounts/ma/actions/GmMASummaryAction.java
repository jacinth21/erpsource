package com.globus.accounts.ma.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.ma.beans.GmManualAdjustBean;
import com.globus.accounts.ma.forms.GmMASummaryForm;
import com.globus.accounts.ma.forms.GmManualAdjTxnForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;
import com.globus.common.beans.GmLogger;



/**
 * ActionClass for MA Summary screen
 */

public class GmMASummaryAction extends GmDispatchAction {
  public ActionForward reportMASummary(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmMASummaryForm gmMASummaryForm = (GmMASummaryForm) form;
    GmManualAdjustBean gmManualAdjustBean = new GmManualAdjustBean(getGmDataStoreVO());
    ArrayList alTxnType;
    List alResult;
    Map hmParam;
    final String strOpt;
    final String strDeptId;

    try {
      gmMASummaryForm.loadSessionParameters(request);
      strDeptId = gmMASummaryForm.getDeptId();
      if (strDeptId.equals(GmCommonConstants.DEPT_ACCOUNTS)) {
        alTxnType = gmManualAdjustBean.fetchAccountingTxnType();
        gmMASummaryForm.setAlType(alTxnType);
      } else {
        gmMASummaryForm.setAlType((GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList(
            "TXNTP", strDeptId))));
      }

      strOpt = gmMASummaryForm.getStrOpt();

      if (strOpt.equals("Report")) {
        hmParam = GmCommonClass.getHashMapFromForm(gmMASummaryForm);
        alResult = gmManualAdjustBean.loadMASummary(hmParam).getRows();
        gmMASummaryForm.setLdtResult(alResult);
      }
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("success");
  }

  public ActionForward reportMADetail(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.
    GmManualAdjustBean gmManualAdjustBean = new GmManualAdjustBean(getGmDataStoreVO());
    Map hmMAData;
    RowSetDynaClass rdMADetail;
    Map hmMAHeader;

    GmManualAdjTxnForm gmMADetailsForm = (GmManualAdjTxnForm) form;
    gmMADetailsForm.loadSessionParameters(request);
    String strMaID = gmMADetailsForm.getMaID();
    String strOpt = gmMADetailsForm.getStrOpt();
    log.debug("MAID clicked is  : " + strMaID);

    try {
      if (strOpt != "FromMenu") {
        hmMAData = gmManualAdjustBean.fetchMADetail(strMaID);
        hmMAHeader = (HashMap) hmMAData.get("HMMAHEADER");
        GmCommonClass.getFormFromHashMap(gmMADetailsForm, (HashMap) hmMAHeader);

        rdMADetail = (RowSetDynaClass) hmMAData.get("RDMADETAIL");
        List alResult = rdMADetail.getRows();
        gmMADetailsForm.setLdtResult(alResult);
      }
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("success");
  }
}
