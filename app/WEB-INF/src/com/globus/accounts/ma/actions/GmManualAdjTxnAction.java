package com.globus.accounts.ma.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.accounts.ma.beans.GmManualAdjustBean;
import com.globus.accounts.ma.forms.GmManualAdjTxnForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;



public class GmManualAdjTxnAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception, AppError {
    instantiate(request, response);
    String strRows = "";
    String strDeptId = "";
    String strOpt = "";
    int intOwnerCmp = 0;
    int intOwnerCur = 0;
    String strOwnerCmp = "";
    String strOwnerCur = "";
    HashMap hmOwnerCur = new HashMap();
	HashMap hmOwnerCmp = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmRows = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmParam = new HashMap();
    ArrayList alTxnType = new ArrayList();

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmManualAdjustBean gmManualAdjustBean = new GmManualAdjustBean(getGmDataStoreVO());
    GmManualAdjTxnForm gmManualAdjTxnForm = (GmManualAdjTxnForm) form;
    gmManualAdjTxnForm.loadSessionParameters(request);
    // gmManualAdjTxnForm.setCreatedDate((String)
    // request.getSession().getAttribute("strSessTodaysDate"));
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    gmManualAdjTxnForm.setCreatedDate(GmCalenderOperations.getCurrentDate(getGmDataStoreVO()
        .getCmpdfmt()));

    strOpt = gmManualAdjTxnForm.getStrOpt();
    strDeptId = gmManualAdjTxnForm.getDeptId();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    // hmParam = GmCommonClass.getHashMapFromForm(gmManualAdjTxnForm);
    ArrayList alOwnerCompany = gmCommonBean.getParentCompanyList();
    ArrayList alOwnerCurrency = GmCommonClass.getCodeList("CURRN");
    gmManualAdjTxnForm.setAlOwnerCompany(alOwnerCompany);
    gmManualAdjTxnForm.setAlOwnerCurrency(alOwnerCurrency);
  //PC-4903-copy paste issue in modern browser
//    initializing the data for owner company
    intOwnerCmp = alOwnerCompany.size();
	hmOwnerCmp = new HashMap();
	if (intOwnerCmp > 0) {
		for (int i = 0; i < intOwnerCmp; i++) {
			hmOwnerCmp = (HashMap) alOwnerCompany.get(i);
			strOwnerCmp += "ownerCmpArr[" + i + "] = new Array(\""
					+ hmOwnerCmp.get("COMPANYID") + "\",\""
					+ hmOwnerCmp.get("COMPANYNM") + "\");";
		}
	}
//  initializing the data for owner currency
	intOwnerCur = alOwnerCurrency.size();
	hmOwnerCur = new HashMap();
	if (intOwnerCur > 0) {
		for (int i = 0; i < intOwnerCur; i++) {
			hmOwnerCur = (HashMap) alOwnerCurrency.get(i);
			strOwnerCur += "ownerCurArr[" + i + "] = new Array(\""
					+ hmOwnerCur.get("CODEID") + "\",\""
					+ hmOwnerCur.get("CODENMALT") + "\");";
		}
	}
	
	gmManualAdjTxnForm.setStrOwnerCmp(GmCommonClass.parseNull(strOwnerCmp));
	gmManualAdjTxnForm.setStrOwnerCur(GmCommonClass.parseNull(strOwnerCur));
    if (strOpt.equals("OnLoadMaType")) {
      fetchMATypes(gmManualAdjTxnForm);
    }
    strOpt = gmManualAdjTxnForm.getStrOpt();

    if (strOpt.equals("save")) {
      return saveMATypes(gmManualAdjTxnForm, request);
    }

    if (gmManualAdjTxnForm.getStrOpt().equals("loadGrid")) {

      strRows = GmCommonClass.parseNull(GmCommonClass.getRuleValue("DEFGRIDROWS", "DEFAULT"));
      gmManualAdjTxnForm.setRows(strRows);
      hmRows.put("ROWSIZE", strRows);
      hmRows.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmRows.put("ALOWNERCOMPANY", alOwnerCompany);
      hmRows.put("ALOWNERCURRENCY", alOwnerCurrency);
    }
    gmManualAdjTxnForm.setStrOpt(strOpt);
    // if (strDeptId.matches("200(0|6)"))
    if (strDeptId.equals("2022")) {
      alTxnType = gmManualAdjustBean.fetchAccountingTxnType();
      gmManualAdjTxnForm.setAlMAType(alTxnType);
    } else {
      alTxnType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TXNTP", strDeptId));
      gmManualAdjTxnForm.setAlMAType(alTxnType);
    }

    request.setAttribute("ALOWNERCOMPANY", alOwnerCompany);
    request.setAttribute("ALOWNERCURRENCY", alOwnerCurrency);
    return mapping.findForward("success");
  }// End of execute

  /**
   * fetchMATypes - used to select any MaType and loading all parts details
   * 
   * @param GmManualAdjTxnForm gmManualAdjTxnForm
   * @return void
   * @throws AppError
   */
  private void fetchMATypes(GmManualAdjTxnForm gmManualAdjTxnForm) throws AppError {

    String strMATypeId = "";
    String strFromCostType = "";
    String strHaction = "";
    GmManualAdjustBean gmManualAdjustBean = new GmManualAdjustBean(getGmDataStoreVO());
    strHaction = gmManualAdjTxnForm.getHaction();
    strMATypeId = gmManualAdjTxnForm.getMaTypeId();

    // Hardcoding for Scrap to RM (48056) since this has to use RM Cost. all other 'Scrap --
    // (anything)' fetches null and takes 4900 (FG) cost-
    // Joe and James. june 16,2008. Probably will look at changing this later
    strFromCostType =
        strMATypeId.equals("48056") ? "4909" : gmManualAdjustBean.fetchCostType(strMATypeId);
    gmManualAdjTxnForm.setHmaFromType(strFromCostType);
    // We need to update the MA process for accounting so that they do not have to affect operations
    // qty (MNTTASK-2504).
    // The changes are in jsp file
    gmManualAdjTxnForm.setUpdOprQtyDisableFlag(GmCommonClass.getCodeAltName(strMATypeId)
        .equals("Y") ? false : true);
    gmManualAdjTxnForm.setStrOpt(strHaction);
  }// End of fetchMATypes

  /**
   * saveMATypes - used to save all the parts cost details
   * 
   * @param GmManualAdjTxnForm gmManualAdjTxnForm
   * @return ActionRedirect
   * @throws AppError
   */
  private ActionRedirect saveMATypes(GmManualAdjTxnForm gmManualAdjTxnForm,
      HttpServletRequest request) throws AppError {

    String strDeptId = "";
    String strHaction = "";
    String strMaID = "";
    String strOpt = "";
    HashMap hmFrmParam = new HashMap();
    GmManualAdjustBean gmManualAdjustBean = new GmManualAdjustBean(getGmDataStoreVO());

    hmFrmParam = GmCommonClass.getHashMapFromForm(gmManualAdjTxnForm);
    strDeptId = GmCommonClass.parseNull(gmManualAdjTxnForm.getDeptId());
    strHaction = GmCommonClass.parseNull(gmManualAdjTxnForm.getHaction());
    strOpt = gmManualAdjTxnForm.getStrOpt();

    if (strDeptId.equals("2003") || strDeptId.equals("2014") || strDeptId.equals("2008")
        || strDeptId.equals("2024") || strDeptId.equals("2001") || strDeptId.equals("2010")) {
      hmFrmParam.put("UPDOPRQTY", "on");
    }
    // saveMADetails is used for saving all parts details
    strMaID = gmManualAdjustBean.saveMADetails(hmFrmParam);


    String strUrl =
        "/gmMADetail.do?method=reportMADetail&maID=" + strMaID + "&strOpt=" + strOpt + "&haction="
            + strHaction;
    return actionRedirect(strUrl, request);
  }// End of saveMATypes

  private String generateXMLString(HashMap hmRows) throws AppError { // Processing HashMap into XML
                                                                     // data
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmRows.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmRows", hmRows);
    templateUtil.setDropDownMaster("alOwnerCompany",
        GmCommonClass.parseNullArrayList((ArrayList) hmRows.get("ALOWNERCOMPANY")));
    templateUtil.setDropDownMaster("alOwnerCurrency",
        GmCommonClass.parseNullArrayList((ArrayList) hmRows.get("ALOWNERCURRENCY")));
    templateUtil.setTemplateSubDir("accounts/ma/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.ma.GmManualAdjTxnGrid", strSessCompanyLocale));
    templateUtil.setTemplateName("GmManualAdjTxnGrid.vm");
    return templateUtil.generateOutput();
  }// End of generateXMLString
}
