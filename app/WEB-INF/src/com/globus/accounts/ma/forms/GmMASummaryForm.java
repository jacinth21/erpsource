package com.globus.accounts.ma.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmMASummaryForm extends GmCommonForm{

	
	private String typeId = "";  //MAType selected
	private ArrayList alType = new ArrayList(); //MAType dropdown
	
	
	private String maID = ""; //MA#  	
	private String partNumber = "";  //Part Number
	
	
	private List ldtResult = new ArrayList(); //Stores report output table.
	
	
					
	private String fromDate = "";
	private String toDate = "";
	
	
	
	/** MAType dropdown
     * @return Returns the alType.
     */
    public ArrayList getAlType()
    {
        return alType;
    }
    
    /** MAType dropdown
     * @param alType The alType to set.
     */
    public void setAlType(final ArrayList alType)
    {
        this.alType = alType;
    }
   
    
    /**
     * @return Returns the typeId.
     */
    public String getTypeId()
    {
        return typeId;
    }
    /**
     * @param typeId The typeId to set.
     */
    public void setTypeId(String typeId)
    {
        this.typeId = typeId;
    }

    /**
     * MA ID
     */
    
	public String getMaID() {
		return maID;
	}

	public void setMaID(String maID) {
		this.maID = maID;
	}

	/**
	 * PartNumber
	 */
	
	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	
	 /**
     * @return Returns the ldtResult.
     */
    public List getLdtResult()
    {
        return ldtResult;
    }
    /**
     * @param ldtResult The ldtResult to set.
     */
    public void setLdtResult(List ldtResult)
    {
        this.ldtResult = ldtResult;
    }
	
	
}
