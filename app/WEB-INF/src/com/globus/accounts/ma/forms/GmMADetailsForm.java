package com.globus.accounts.ma.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmMADetailsForm extends GmCommonForm 
{
	
	private String MAID = ""; //MA# must match paramId from .jsp href paramId in display tag
	
	private String MADesc = ""; 
	
	private String MAType = "";
	
	private String operationQtyUpdate = ""; 
	
	private String createdDate = "";
	
	private String createdBy = "";

	
	private List ldtResult = new ArrayList(); //Stores report output table.
	
	public String getMAID() {
		return MAID;
	}

	public void setMAID(String maid) {
		MAID = maid;
	}

	public String getMADesc() {
		return MADesc;
	}

	public void setMADesc(String desc) {
		MADesc = desc;
	}

	public String getMAType() {
		return MAType;
	}

	public void setMAType(String type) {
		MAType = type;
	}

	public String getOperationQtyUpdate() {
		return operationQtyUpdate;
	}

	public void setOperationQtyUpdate(String operationQtyUpdate) {
		this.operationQtyUpdate = operationQtyUpdate;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	 
	/**
     * @return Returns the ldtResult.
     */
    public List getLdtResult()
    {
        return ldtResult;
    }
    /**
     * @param ldtResult The ldtResult to set.
     */
    public void setLdtResult(List ldtResult)
    {
        this.ldtResult = ldtResult;
    }

	
	

}
