package com.globus.accounts.ma.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmManualAdjTxnForm extends GmCommonForm {

  private String maTypeId = ""; // common MAType
  private String createdDate = ""; // common createdDate
  private String maID = ""; // common MAID. MA# must match paramId from .jsp href paramId in display
                            // tag
  private String maDesc = ""; // common MADesc
  private String updOprQty = ""; // common operationQtyUpdate
  private String hmaFromType = "";
  private String hinputStr = "";
  private String createdBy = "";
  private String xmlStringData = "";
  private String rows = "";
  private String strOwnerCmp = "";
  private String strOwnerCur = "";

  /**
 * @return the strOwnerCmp
 */
public String getStrOwnerCmp() {
	return strOwnerCmp;
}

/**
 * @param strOwnerCmp the strOwnerCmp to set
 */
public void setStrOwnerCmp(String strOwnerCmp) {
	this.strOwnerCmp = strOwnerCmp;
}

/**
 * @return the strOwnerCur
 */
public String getStrOwnerCur() {
	return strOwnerCur;
}

/**
 * @param strOwnerCur the strOwnerCur to set
 */
public void setStrOwnerCur(String strOwnerCur) {
	this.strOwnerCur = strOwnerCur;
}

  private List ldtResult = new ArrayList(); // Stores report output table.

  private boolean updOprQtyDisableFlag = true;

  private ArrayList alMAType = new ArrayList();
  private ArrayList alOwnerCompany = new ArrayList();
  private ArrayList alOwnerCurrency = new ArrayList();

  /**
   * @return the maTypeId
   */
  public String getMaTypeId() {
    return maTypeId;
  }

  /**
   * @param maTypeId the maTypeId to set
   */
  public void setMaTypeId(String maTypeId) {
    this.maTypeId = maTypeId;
  }

  /**
   * @return the createdDate
   */
  public String getCreatedDate() {
    return createdDate;
  }

  /**
   * @param createdDate the createdDate to set
   */
  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }

  /**
   * @return the maDesc
   */
  public String getMaDesc() {
    return maDesc;
  }

  /**
   * @param maDesc the maDesc to set
   */
  public void setMaDesc(String maDesc) {
    this.maDesc = maDesc;
  }

  /**
   * @return the updOprQty
   */
  public String getUpdOprQty() {
    return updOprQty;
  }

  /**
   * @param updOprQty the updOprQty to set
   */
  public void setUpdOprQty(String updOprQty) {
    this.updOprQty = updOprQty;
  }

  /**
   * @return the hmaFromType
   */
  public String getHmaFromType() {
    return hmaFromType;
  }

  /**
   * @param hmaFromType the hmaFromType to set
   */
  public void setHmaFromType(String hmaFromType) {
    this.hmaFromType = hmaFromType;
  }

  /**
   * @return the alMAType
   */
  public ArrayList getAlMAType() {
    return alMAType;
  }

  /**
   * @param alMAType the alMAType to set
   */
  public void setAlMAType(ArrayList alMAType) {
    this.alMAType = alMAType;
  }

  /**
   * @return the updOprQtyDisableFlag
   */
  public boolean isUpdOprQtyDisableFlag() {
    return updOprQtyDisableFlag;
  }

  /**
   * @param updOprQtyDisableFlag the updOprQtyDisableFlag to set
   */
  public void setUpdOprQtyDisableFlag(boolean updOprQtyDisableFlag) {
    this.updOprQtyDisableFlag = updOprQtyDisableFlag;
  }

  /**
   * @return the hinputStr
   */
  public String getHinputStr() {
    return hinputStr;
  }

  /**
   * @param hinputStr the hinputStr to set
   */
  public void setHinputStr(String hinputStr) {
    this.hinputStr = hinputStr;
  }

  /**
   * @return the maID
   */
  public String getMaID() {
    return maID;
  }

  /**
   * @param maID the maID to set
   */
  public void setMaID(String maID) {
    this.maID = maID;
  }

  /**
   * @return the createdBy
   */
  public String getCreatedBy() {
    return createdBy;
  }

  /**
   * @param createdBy the createdBy to set
   */
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  /**
   * @return the ldtResult
   */
  public List getLdtResult() {
    return ldtResult;
  }

  /**
   * @param ldtResult the ldtResult to set
   */
  public void setLdtResult(List ldtResult) {
    this.ldtResult = ldtResult;
  }

  public String getXmlStringData() {
    return xmlStringData;
  }

  public void setXmlStringData(String xmlStringData) {
    this.xmlStringData = xmlStringData;
  }

  public String getRows() {
    return rows;
  }

  public void setRows(String rows) {
    this.rows = rows;
  }

  /**
   * @return the alOwnerCompany
   */
  public ArrayList getAlOwnerCompany() {
    return alOwnerCompany;
  }

  /**
   * @param alOwnerCompany the alOwnerCompany to set
   */
  public void setAlOwnerCompany(ArrayList alOwnerCompany) {
    this.alOwnerCompany = alOwnerCompany;
  }

  /**
   * @return the alOwnerCurrency
   */
  public ArrayList getAlOwnerCurrency() {
    return alOwnerCurrency;
  }

  /**
   * @param alOwnerCurrency the alOwnerCurrency to set
   */
  public void setAlOwnerCurrency(ArrayList alOwnerCurrency) {
    this.alOwnerCurrency = alOwnerCurrency;
  }



}
