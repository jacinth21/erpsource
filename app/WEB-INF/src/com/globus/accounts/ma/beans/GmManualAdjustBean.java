package com.globus.accounts.ma.beans;


import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonSort;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmManualAdjustBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmManualAdjustBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmManualAdjustBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // RowSetDyna

  public RowSetDynaClass loadMASummary(Map hmConditions) throws AppError {
    final String MAId = (String) hmConditions.get("MAID");
    final String MAType = (String) hmConditions.get("TYPEID");
    final String partNumber = (String) hmConditions.get("PARTNUMBER");
    final String fromDate = (String) hmConditions.get("FROMDATE");
    final String toDate = (String) hmConditions.get("TODATE");
    String strCompDateFmt = GmCommonClass.parseNull(getCompDateFmt());
    log.debug(" MAId: " + MAId + " MAType: " + MAType + " partNumber: " + partNumber
        + "  fromDate: " + fromDate + " toDate: " + toDate);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass rdResult = null;

    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT T215.C215_PART_QTY_ADJ_ID MAID, T216.C205_PART_NUMBER_ID PNUM, T215.C215_COMMENTS MADESC, GET_CODE_NAME(T215.C901_ADJUSTMENT_SOURCE) MATYPE, T216.C216_QTY_TO_BE_ADJUSTED MAQTY, ");
    sbQuery
        .append(" TO_CHAR(T215.C215_CREATED_DATE,'"
            + getGmDataStoreVO().getCmpdfmt()
            + "') CREATEDDATE, get_user_name(t215.c215_created_by) createdby, NVL (t216.c216_item_price, 0) maprice, ");
    sbQuery
        .append(" Get_Acctelement_Desc(get_partnum_account_element (T804.C901_ACCOUNT_TYPE_CR, T216.C205_PART_NUMBER_ID)) CREDITACCT,");
    sbQuery
        .append(" Get_Acctelement_Desc(get_partnum_account_element (T804.C901_ACCOUNT_TYPE_DR, T216.C205_PART_NUMBER_ID)) DEBITACCT ");
    sbQuery
        .append(" , t216.c1900_owner_company_id owner_id, get_company_code(t216.c1900_owner_company_id) owner_cd, ");
    sbQuery
        .append(" get_company_name(t216.c1900_owner_company_id) owner_name, t216.c216_local_item_price local_cost, ");
    sbQuery.append(" get_code_name_alt (get_comp_curr(t216.c1900_owner_company_id)) owner_curr, ");
    sbQuery.append(" get_code_name_alt (get_comp_curr(t215.c1900_company_id)) local_curr ");
    sbQuery
        .append(" FROM T215_PART_QTY_ADJUSTMENT T215, T216_PART_QTY_ADJ_DETAILS T216, T804_POSTING_REF T804 ");
    sbQuery
        .append(" WHERE T215.C215_PART_QTY_ADJ_ID = T216.C215_PART_QTY_ADJ_ID  AND T215.C901_ADJUSTMENT_SOURCE = T804.C901_TRANSACTION_TYPE");

    sbQuery.append(" AND  T215.C1900_COMPANY_ID  =  '" + getCompId() + "'");
    if (!MAId.equals("")) {
      sbQuery.append(" AND T215.C215_PART_QTY_ADJ_ID  = '");
      sbQuery.append(MAId);
      sbQuery.append("'");
    }

    // Part number
    if (!partNumber.equals("")) {
      sbQuery.append("  AND  T216.C205_PART_NUMBER_ID  =  '");
      sbQuery.append(partNumber);
      sbQuery.append("'");
    }

    // MAType - codeID
    if (!MAType.equals("") && !MAType.equals("0")) // check for 0 ?
    {
      sbQuery.append("  AND  T215.C901_ADJUSTMENT_SOURCE =  ");
      sbQuery.append(Integer.parseInt(MAType));
    }

    // From Date
    if (!fromDate.equals("")) {
      sbQuery.append(" AND TRUNC(T215.C215_CREATED_DATE) >= TO_DATE('");
      sbQuery.append(fromDate);
      sbQuery.append("', '" + getGmDataStoreVO().getCmpdfmt() + "') ");
    }

    // To Date
    if (!toDate.equals("")) {
      sbQuery.append(" AND TRUNC(T215.C215_CREATED_DATE) <= TO_DATE('");
      sbQuery.append(toDate);
      sbQuery.append("', '" + getGmDataStoreVO().getCmpdfmt() + "')  ");
    }

    log.debug("Query for MASummaryReport : " + sbQuery.toString());
    // verify
    rdResult = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    gmDBManager.close();

    return rdResult;

  }

  // *fetchTTPGroupMap
  public HashMap fetchMADetail(final String strMAId) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmMAHeader;
    RowSetDynaClass rdMADetail = null; // hashmap for header and rowsetdyna detail.


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    // procedure call.
    gmDBManager.setPrepareString("gm_pkg_ac_ma.gm_ac_fch_madetails", 3);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); // header query 1
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR); // MA Detail query 2

    gmDBManager.setString(1, strMAId);

    gmDBManager.execute();

    hmMAHeader = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2)); // hashmap.
    rdMADetail = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.commit();

    hmReturn.put("RDMADETAIL", rdMADetail);
    hmReturn.put("HMMAHEADER", hmMAHeader); // key HMMAHEADER and alUnselected to new hm variable.
    return hmReturn;
  }

  /**
   * @param strMAType - Manual Adjustment Id
   * @exception AppError
   * @return String
   */
  public String fetchCostType(String strMAType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strFromCostType = "";
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT GET_FROMCOST_TYPE('");
    sbQuery.append(strMAType);
    sbQuery.append("') FROMCOSTTYPE FROM DUAL ");

    log.debug("Query for fetchCostType : " + sbQuery.toString());
    // verify
    HashMap hmValue = gmDBManager.querySingleRecord(sbQuery.toString());
    gmDBManager.close();

    strFromCostType = GmCommonClass.parseNull((String) hmValue.get("FROMCOSTTYPE"));
    return strFromCostType;
  }

  /**
   * @param strMAId - Manual Adjustment Id
   * @exception AppError
   * @return String
   */
  public String saveMADetails(HashMap hmParam) throws AppError {
    String strMAId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String strDescription = GmCommonClass.parseNull((String) hmParam.get("MADESC"));
    String strMAType = GmCommonClass.parseNull((String) hmParam.get("MATYPEID"));
    String strFromCostType = GmCommonClass.parseNull((String) hmParam.get("HMAFROMTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strUpdOprQty = GmCommonClass.getCheckBoxValue((String) hmParam.get("UPDOPRQTY"));

    gmDBManager.setPrepareString("gm_pkg_ac_ma.gm_ac_sav_madetails", 7);

    gmDBManager.registerOutParameter(7, java.sql.Types.CHAR);

    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strDescription);
    gmDBManager.setString(3, strMAType);
    gmDBManager.setString(4, strFromCostType);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strUpdOprQty);
    gmDBManager.execute();

    strMAId = gmDBManager.getString(7);
    log.debug("strMAId  " + strMAId);

    gmDBManager.commit();

    return strMAId;
  }


  /**
   * fetchAccountingTxnType - removes some of the txn types which are not needed by Accounting team
   * 
   * @author jkumar
   * @date Feb 20, 2008
   * 
   */
  public ArrayList fetchAccountingTxnType() throws AppError {
    /*
     * 48050 - WIP to raw material 48051 - WIP to DHR 48068 - COGS to loaner 4810 - Purchasing 48047
     * - Purchase to Bio Raw material
     */
    Iterator itrTest;
    HashMap hmValue = new HashMap();
    String strValue = "";
    String strList = "4810,48047,48050,48051,48068";
    String strPatternVar;
    Pattern pattern;
    Matcher matcher;

    ArrayList alTxnType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("TXNTP"));
    itrTest = alTxnType.iterator();
    while (itrTest.hasNext()) {
      hmValue = (HashMap) itrTest.next();
      strValue = (String) hmValue.get("CODEID");
      strPatternVar = "\\b".concat(strValue).concat("\\b");
      pattern = Pattern.compile(strPatternVar);
      matcher = pattern.matcher(strList);
      if (matcher.find()) {
        itrTest.remove();
      }
    }
    alTxnType = (ArrayList) GmCommonSort.sortArrayList(alTxnType, "CODENM", "String", 1);
    return alTxnType;
  }
}
