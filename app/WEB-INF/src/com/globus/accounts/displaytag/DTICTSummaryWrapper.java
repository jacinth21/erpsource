package com.globus.accounts.displaytag;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;
import com.globus.common.beans.GmLogger;
 

public class DTICTSummaryWrapper extends TableDecorator
{
          
    private DynaBean db ;
    
    public StringBuffer  strValue = new StringBuffer();
  
 Logger log = GmLogger.getInstance(GmCommonConstants.OPERATIONS); 
    /**
     * Returns the string for RequestId field icon.
     * @return String
     */
   
    
    public String getINVOICEID()
    {
        db =    (DynaBean) this.getCurrentRowObject();               
        String strInvId   = GmCommonClass.parseNull((String)db.get("INVOICEID"));
        //String strOrderId   = GmCommonClass.parseNull((String)db.get("ORDERID"));
        String strType   = ""+db.get("INVTYPEID");
       
        String strDeptId = (String)getPageContext().getSession().getAttribute("strSessDeptSeq");
        strValue.setLength(0); 
        //if (strOrderId.startsWith("GM-CN-")){
        //	strValue.append("<a href=javascript:fnPrintVer()> <img src=/images/printer.jpg height='14' width='14' alt='Print'>  </a>" );
        //}
        //else{
        if (!strDeptId.equals("2000") && !strType.equals("50258")){//cust service
        	strValue.append("<a href=javascript:fnPrintVer('");
        	strValue.append(strInvId);
        	strValue.append("')> <img src=/images/product_icon.gif height='14' width='14' alt='Proforma Invoice Print'>  </a>" );
        }
        if (!strDeptId.equals("2001")){//accounts
        	strValue.append("<a href=javascript:fnPrintInvoice('");
        	strValue.append(strInvId);
        	strValue.append("')> <img src=/images/consignment.gif height='14' width='14' alt='Commercial Invoice Print'>  </a>" );
        }
        //}
        strValue.append(strInvId);    	
    	return strValue.toString();
                
    }
 /*   
    public String getCONSIGNID()
    {
        db =    (DynaBean) this.getCurrentRowObject();               
        String strConsignId   = GmCommonClass.parseNull((String)db.get("CONSIGNID"));
        String strInvId   = GmCommonClass.parseNull((String)db.get("INVOICEID"));
        strValue.setLength(0); 
        strValue.append("<a href=javascript:fnPrintVer()> <img src=/images/printer.jpg height='14' width='14' alt='Commercial Invoice Print'>  </a>" );
        strValue.append(strConsignId);    	
    	return strValue.toString();
                
    }
   */ 
 /*   
    public String getORDERID()
    {
        db =    (DynaBean) this.getCurrentRowObject();               
        String strOrderId   = GmCommonClass.parseNull((String)db.get("ORDERID"));
        strValue.setLength(0); 
        if (strOrderId.startsWith("GM-CN-")){
        	strValue.append("<a href=javascript:fnPrintVer('"+strOrderId+"')> <img src=/images/printer.jpg height='14' width='14' alt='Print'>  </a>" );
        }
        else
        {
        	strValue.append("<a href=javascript:fnViewOrder('"+strOrderId+"')> <img src=/images/printer.jpg height='14' width='14' alt='Print'>  </a>" );
        }
       strValue.append(strOrderId);
       //strValue.append("<a href=javascript:fnopengmOperDashBoard('"+strPNUM+"')> <img src='/images/d2.gif' /> </a>" );   
    	
    	return strValue.toString();
                
    }
*/
    
}
