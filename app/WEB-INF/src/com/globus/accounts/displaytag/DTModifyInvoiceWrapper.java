package com.globus.accounts.displaytag;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTModifyInvoiceWrapper extends TableDecorator {

	private DynaBean db;
	GmLogger log = GmLogger.getInstance();

	public DTModifyInvoiceWrapper() 
	{
		super();
	}

	public String getINVID() 
	{
	     String 	strInvId = "";	
		 String 	strCustPO = "";
		 String 	strInvType = "";
		 String 	strStatusFl = "";
		 
		db = (DynaBean) this.getCurrentRowObject();
		 strInvId = String.valueOf(db.get("INVID"));		 
		 strInvType =String.valueOf(db.get("INVTP"));
		 strCustPO =String.valueOf(db.get("PO"));
	   //  strStatusFl =String.valueOf(db.get("SFL"));
		 StringBuffer strValue = new StringBuffer();	
		 strValue.append("<input type=\"radio\" value=\"");
		 strValue.append(strCustPO);
			strValue.append("\" id=\"");
			strValue.append(strInvId);
			strValue.append("\" name=\"rad\">&nbsp; ");
			strValue.append("<a class = \"" +strInvType+"\" href=\"javascript:fnPrintInvoice('"+strInvId+"');\" class=\"RightText\"> ");    
			strValue.append(strInvId);
			strValue.append("</a>");
			strValue.append("<input type=\"hidden\" name=\""+strCustPO+"\" value=\""+strStatusFl+"\">");
			
			return strValue.toString();
	}
}

