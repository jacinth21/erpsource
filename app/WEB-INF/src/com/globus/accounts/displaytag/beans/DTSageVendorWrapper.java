package com.globus.accounts.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTSageVendorWrapper extends TableDecorator{

		private String strSageVendorID; 
	     
	    private DynaBean db ;
	    private int intCount = 0;
	    public StringBuffer  strValue = new StringBuffer();
	    
	    /**
	     * Returns the string for text field.
	     * @return String
	     */
	    public String getSAGEVENDORID()
	    {
	    	db =    (DynaBean) this.getCurrentRowObject();
	        strSageVendorID = String.valueOf(db.get("SAGEVENDORID"));
	        if(strSageVendorID.equals("null"))	strSageVendorID ="";
	       // System.out.println("strSageVendorID : "+ strSageVendorID);        
	        strValue.setLength(0);
	        strValue.append("<input type = text name = sageVendorId");
	        strValue.append(String.valueOf(intCount) );
	        strValue.append(" size = 20 value='");
	        strValue.append(strSageVendorID);
	        strValue.append("'>");
	       
	        intCount++;
	        return strValue.toString(); 
	    }
	    
	    
}
