package com.globus.accounts.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

public class DTFieldAuditLockReportWrapper extends TableDecorator {

	private DynaBean db;
	
	public DTFieldAuditLockReportWrapper() 
	{
		super();
	}

	public String getFILE_ID() 
	{
	     String 	strFileID = "";
		 
		db = (DynaBean) this.getCurrentRowObject();
		strFileID = String.valueOf(db.get("FILE_ID"));	
		
		 StringBuffer strValue = new StringBuffer();
		 
		 strValue.append("<a href=javascript:fnOpenForm(");
		 strValue.append(strFileID);
			strValue.append(")><img border=\"0\" src=\"/images/csv_icon.gif\"></a>");
			return strValue.toString();
	}
}

