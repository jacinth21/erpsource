package com.globus.accounts.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

public class DTBatchRptWrapper extends TableDecorator{

    
    private DynaBean db ;    
    private int intBatchId = 0; 
    
    public String getBATCHNUMBER()
    {
        db =  	(DynaBean) this.getCurrentRowObject();     
		intBatchId = Integer.parseInt(String.valueOf(db.get("BATCHNUMBER"))); 
		StringBuffer strValue = new StringBuffer();
		
		strValue.append("&nbsp;&nbsp; <a href= javascript:fnCallAPPosting('");
		strValue.append(intBatchId);
		strValue.append("')>  <img src=/images/a.gif height='14' width='12' alt='A/P Posting'> </a>");
		
        strValue.append("&nbsp; <a href=javascript:fnBatchdetails('");
 		strValue.append(intBatchId);
 		strValue.append("')> "); 
 		strValue.append( intBatchId); 
 		strValue.append( "</a>"); 
     	
		return strValue.toString();
       
    }
   
    
}
