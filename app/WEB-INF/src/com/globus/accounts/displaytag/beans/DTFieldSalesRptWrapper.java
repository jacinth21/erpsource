package com.globus.accounts.displaytag.beans;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.accounts.forms.GmFieldSalesRptForm;
import com.globus.common.beans.GmCommonClass;

public class DTFieldSalesRptWrapper extends TableDecorator {
	private DynaBean db;
	GmFieldSalesRptForm gmFieldSalesRptForm;
	String strReportType = "";
	String strImagePath = GmCommonClass.getString("GMIMAGES");

	public void init(PageContext context, Object decorated, TableModel tableModel) {
		// TODO Auto-generated method stub
		super.init(context, decorated, tableModel);
		gmFieldSalesRptForm = (GmFieldSalesRptForm) getPageContext().getAttribute("frmFieldSalesRptForm", PageContext.REQUEST_SCOPE);
		strReportType = gmFieldSalesRptForm.getReportType();
	}

	public String getSET_ID() {
		String strSetId = "";
		String strDistID = "";
		db = (DynaBean) this.getCurrentRowObject();
		strSetId = String.valueOf(db.get("SET_ID"));
		strDistID = String.valueOf(db.get("DIST_ID"));
		String strLogFl = String.valueOf(db.get("LOG_FL"));
		String strCommentId = strDistID + strSetId;
		StringBuffer strValue = new StringBuffer();
		if (strReportType.equals("globalSet")) {
			strValue.append("<a href=javascript:fnOpenVarianceBySet(");
			strValue.append(strSetId);
			strValue.append(")>" + strSetId + "</a>");
		} else {
			strValue.append(" <a class=RightText href=javascript:fnOpenLog('" + strCommentId + "',1251);> ");
			if (strLogFl.equals("N")) {
				strValue.append(" <img src=" + strImagePath + "/phone_icon.jpg border=0 width=15 height=13 title='Click to add/view Comments'></a>&nbsp;");
			} else {
				strValue.append(" <img src=" + strImagePath + "/phone-icon_ans.gif border=0 width=15 height=13 title='Click to add/view Comments'></a>&nbsp;");
			}
			strValue.append(strSetId);
		}

		return strValue.toString();
	}

	public String getDIST_NAME() {
		String strDistID = "";
		String strDistName = "";
		String strSetId = "";
		db = (DynaBean) this.getCurrentRowObject();
		strDistID = String.valueOf(db.get("DIST_ID"));
		strDistName = String.valueOf(db.get("DIST_NAME"));
		strSetId = String.valueOf(db.get("SET_ID"));
		String strLogFl = String.valueOf(db.get("LOG_FL"));
		String strCommentId = strDistID + strSetId;
		StringBuffer strValue = new StringBuffer();
		if (strReportType.equals("globalDist")) {
			strValue.append("<a href=javascript:fnOpenVarianceByDist(");
			strValue.append(strDistID);
			strValue.append(")>" + strDistName + "</a>");
		} else {
			strValue.append(" <a class=RightText href=javascript:fnOpenLog('" + strCommentId + "',1251);> ");
			if (strLogFl.equals("N")) {
				strValue.append(" <img src=" + strImagePath + "/phone_icon.jpg border=0 width=15 height=13 title='Click to add/view Comments'></a>&nbsp;");
			} else {
				strValue.append(" <img src=" + strImagePath + "/phone-icon_ans.gif border=0 width=15 height=13 title='Click to add/view Comments'></a>&nbsp;");
			}
			strValue.append(strDistName);
		}

		return strValue.toString();
	}

	public String getTAGID() {
		String strTagId = "";
		int intAuditEntryId;
		StringBuffer strValue = new StringBuffer();

		db = (DynaBean) this.getCurrentRowObject();
		strTagId = String.valueOf(db.get("TAGID"));
		intAuditEntryId = Integer.parseInt(String.valueOf(db.get("AUDITENTRYID")));

		strValue.append("<input class=RightText type='radio' name='radAuditEntryId' value='");
		strValue.append(intAuditEntryId);
		strValue.append("' onClick=javascript:fnSelect('");
		strValue.append(intAuditEntryId);
		strValue.append("') > ");
		strValue.append(strTagId);

		return strValue.toString();
	}

	
	 public String getDEVIATION_QTY() { 
		 int intDeviationQty; String strSetId =""; 
		 String strDistId = ""; String strVarianceType = "";
		 int intVarianceQty=0, intAuditQty=0,intSystemQty=0,intDeviation_qty=0,intTransfer_to=0,intLend_To=0,intExtra=0,intBorrowed_from=0,intReturns=0,intMissing=0,intTransfer_from=0,intLend_to=0;
 
	  db = (DynaBean) this.getCurrentRowObject(); 
	  intDeviationQty =Integer.parseInt(String.valueOf((db.get("DEVIATION_QTY"))));
	  strDistId =String.valueOf(db.get("DIST_ID")); 
	  strSetId =String.valueOf(db.get("SET_ID")); 
	

	 String strAuditQty= String.valueOf(db.get("AUDIT_QTY"));
	 
	 if(!strAuditQty.equals(""))
	 {
	  intAuditQty =Integer.parseInt(strAuditQty); 
	 }
	 
	 String strSystemQty= String.valueOf(db.get("SYSTEM_QTY"));
	 if(!strSystemQty.equals(""))
	 {
	  intSystemQty =Integer.parseInt(strSystemQty); 
	 }
	 String strTransfer_to= String.valueOf(db.get("TRANSFER_TO"));
	 if(!strTransfer_to.equals(""))
	 {
	  intTransfer_to =Integer.parseInt(strTransfer_to);
	 }
	 String strLend_TO= String.valueOf(db.get("LEND_TO"));
	 if(!strLend_TO.equals(""))
	 {
	  intLend_To =Integer.parseInt(strLend_TO);
	 }
	 
	 String strMissing=String.valueOf(db.get("MISSING"));
	 
	 if(!strMissing.equals(""))
	 {
	  intMissing =Integer.parseInt(strMissing);
	 }
	 
	 String strReturns=String.valueOf(db.get("RETURNS"));
	 
	 if(!strReturns.equals(""))
	 {
	  intReturns =Integer.parseInt(strReturns);  
	 }

	  
	 String strExtra=String.valueOf(db.get("EXTRA"));
	 
	 if(!strExtra.equals(""))
	 {
	  intExtra =Integer.parseInt(strExtra);
	 }
	 
	 String strBorrowed_from =String.valueOf(db.get("BORROWED_FROM"));
	 
	 if(!strBorrowed_from.equals(""))
	 {
	  intBorrowed_from =Integer.parseInt(strBorrowed_from);
	 }
	 
	 String strTransfer_from=String.valueOf(db.get("TRANSFER_FROM"));
	 if(!strBorrowed_from.equals(""))
	 {
	  intTransfer_from =Integer.parseInt(strTransfer_from);
	 }
	  
	  intVarianceQty=(intAuditQty+intTransfer_to+intLend_To+intMissing+intReturns)-(intSystemQty+intExtra+intBorrowed_from+ intTransfer_from);
	  
	  if (strReportType.equals("globalSet")||strReportType.equals("globalDist")) {
	  return String.valueOf(intDeviationQty); 
	  } 
 
		 // return String.valueOf(intDeviationQty); 
	 
	  StringBuffer strValue = new
	  StringBuffer();
	  
	  strValue.append("<a href=javascript:fnOpenFlagVariance('");
	  strValue.append(strDistId); strValue.append("','");
	  strValue.append(strSetId);strValue.append("','");
	  strValue.append(intVarianceQty);
	  strValue.append("')>" + intDeviationQty + "</a>");
	  return strValue.toString();
	  }
	
}
