package com.globus.accounts.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

public class DTAPPostingWrapper  extends TableDecorator{

    
    private DynaBean db ;    
    private String  accountID = ""; 
    private String  ruleID = "";
    
    public String getACCOUNTID()
    {
        db =  	(DynaBean) this.getCurrentRowObject();     
        accountID =  String.valueOf(db.get("ACCOUNTID")); 
        ruleID =  String.valueOf(db.get("RULEID")); 
             return "&nbsp;<a href=javascript:fnPostingDetails('" + 
             accountID + "','" + ruleID + "')> "  + accountID + "</a>" ; 
    } 
    
}
