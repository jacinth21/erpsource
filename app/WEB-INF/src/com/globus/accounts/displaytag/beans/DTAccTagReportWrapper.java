/**
 * FileName    : DTAcctCurrConvWrapper.java 
 * Description :
 * Author      : Himanshu Patel
 * Date        : Jan 15,2010
 * Copyright   : Globus Medical Inc
 */
package com.globus.accounts.displaytag.beans;

import javax.servlet.jsp.PageContext;
import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.accounts.physicalaudit.forms.GmTagReportForm;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTAccTagReportWrapper extends TableDecorator {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	private String strTagNum = "";
	 private DynaBean db ;
	 GmTagReportForm gmTagReportForm ;
	 boolean blTransAccess;
	 boolean blVoidAccess;
	 private String strLocTypeId = "";
	 private String strStatusId = "";
	 private String strVoidFl = "";
	public DTAccTagReportWrapper() {
		super();
	}
	public void init(PageContext context, Object decorated, TableModel tableModel) {
		// TODO Auto-generated method stub
		super.init(context, decorated, tableModel);
		//GmTagReportForm gmTagReportForm = (GmTagReportForm) form;
		gmTagReportForm = (GmTagReportForm) getPageContext().getAttribute("frmTagReport", PageContext.REQUEST_SCOPE);
		blTransAccess = gmTagReportForm.isTransferAccess();
		blVoidAccess=gmTagReportForm.isVoidAccess();
		
	}
	public String getTAGNUM() {
		String strAccess =   GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessDeptId"));
		db =  	(DynaBean) this.getCurrentRowObject();     
		strTagNum =  String.valueOf(db.get("TAGNUM")); 
		strLocTypeId =  String.valueOf(db.get("LOCTYPEID"));
		strStatusId =  String.valueOf(db.get("STATUSID"));
		strVoidFl =  String.valueOf(db.get("VOIDFL"));
		StringBuffer sbHtml = new StringBuffer();
		
		if (strAccess.equals("S")){
			if (blTransAccess && (strStatusId.equals("51011") || strStatusId.equals("51012"))){
				sbHtml.append("<a href= javascript:fnTagEdit('"+strTagNum+",'"+strStatusId+"'");
				//sbHtml.append(strTagNum);
				sbHtml.append("');> <img width='14' height='15' title='Click here to edit' style='cursor:hand' src='/images/detail_icon.gif'/> </a>");				
			} if(blVoidAccess &&  !strVoidFl.equals("Y")){
				sbHtml.append("<a href= javascript:fnVoidTag('");
				sbHtml.append(strTagNum);
				sbHtml.append("');> <img  width='14' height='15' title='Click here to void' style='cursor:hand' src='/images/void.jpg'/> </a>");
			}if(blVoidAccess && !strVoidFl.equals("Y")){
				sbHtml.append("<a href= javascript:fnUnVoidTag('");
				sbHtml.append(strTagNum);
				sbHtml.append("');> <img  width='20' height='15' title='Click here to Unvoid' style='cursor:hand' src='/images/unvoid.jpg'/> </a>");
			}
			sbHtml.append(strTagNum);
		}
		else
		{
			sbHtml.append("<a href= javascript:fnTagHistory('");
			sbHtml.append(strTagNum);
			sbHtml.append("');> <img  width='14' height='15' title='Click here to see the history' style='cursor:hand' src='/images/icon_History.gif'/> </a>");
			if (blTransAccess && (strStatusId.equals("51011") || strStatusId.equals("51012"))){
				sbHtml.append("<a href= javascript:fnTagEdit('");
				sbHtml.append(strTagNum);
				sbHtml.append("');> <img width='14' height='15' title='Click here to edit' style='cursor:hand' src='/images/detail_icon.gif'/> </a>");				
			} if(blVoidAccess &&  !strVoidFl.equals("Y")){
				sbHtml.append("<a href= javascript:fnVoidTag('");
				sbHtml.append(strTagNum);
				sbHtml.append("');> <img  width='14' height='15' title='Click here to void' style='cursor:hand' src='/images/void.jpg'/> </a>");				
			}if(blVoidAccess && strVoidFl.equals("Y")){
				sbHtml.append("<a href= javascript:fnUnVoidTag('");
				sbHtml.append(strTagNum);
				sbHtml.append("');> <img  width='20' height='15' title='Click here to Unvoid' style='cursor:hand' src='/images/unvoid.jpg'/> </a>");
			}
			sbHtml.append(strTagNum);
		}	
		return sbHtml.toString();	
	}
}
