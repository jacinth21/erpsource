package com.globus.accounts.displaytag.beans;
import javax.servlet.jsp.PageContext;
import java.math.BigDecimal;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;
import org.apache.commons.beanutils.DynaBean;

import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTPrintTagWrapper extends TableDecorator {
	private DynaBean db;
	private String strTAGID = "";
	private String strTID = "";
	private String strLOCATIONID="";
	GmLogger log = GmLogger.getInstance();
	GmLockTagForm gmLockTagForm; 
	public StringBuffer strValue = new StringBuffer();
	private int i = 0;
	
	java.util.ArrayList alUserList;
	java.util.ArrayList alLocationList;
	
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) { 	 
        super.init(pageContext, decorated, tableModel); 
        gmLockTagForm = (GmLockTagForm)getPageContext().getAttribute("frmLockTag", PageContext.REQUEST_SCOPE);
        alUserList = gmLockTagForm.getAuditUserList();	    
        alLocationList = gmLockTagForm.getLocationListWithType();
    } 


	public DTPrintTagWrapper() {
		super();
	}

	public String getTAGID() {
		db = (DynaBean) this.getCurrentRowObject();
		strTAGID = GmCommonClass.parseNull((String) db.get("TAGID"));

		strValue.setLength(0);

		strValue.append("	  <input type=\"checkbox\" value=\"" + strTAGID + "\"  name=\"Chk_tagid" + i + "\" onClick=\"fnGetPrintedTagIDs(this);\">");
		strValue.append("  " + strTAGID);
		i++;
		return strValue.toString();
	}
	
	
	public String getTID() {
		db = (DynaBean) this.getCurrentRowObject();
		strTID = GmCommonClass.parseNull((String) db.get("TID"));
		if(!gmLockTagForm.getStrFlgShowVoidRec().equalsIgnoreCase("on"))
		{
		
		strLOCATIONID= GmCommonClass.parseNull((BigDecimal) db.get("LOCATIONID")+"");
		strValue.setLength(0);
		strValue.append("	  <input type=\"hidden\" value=\"" + strLOCATIONID + "\"  name=\"Hdn_locationid" + i + "\" >");
		strValue.append("	  <input type=\"checkbox\" value=\"" + strTID + "\"  onClick=\"fnGetCount("+i+");\" name=\"Chk_tagid" + i + "\" >");
		strValue.append("  " + strTID);
		i++;
		}else
		{
			return strTID;
		}
		return strValue.toString();
	}

}