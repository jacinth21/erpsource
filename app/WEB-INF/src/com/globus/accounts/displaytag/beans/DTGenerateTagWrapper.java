package com.globus.accounts.displaytag.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;

import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;
import com.globus.common.beans.GmCommonClass;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.beans.GmLogger;

public class DTGenerateTagWrapper extends TableDecorator {
	private DynaBean db;	
	GmLogger log = GmLogger.getInstance();
	GmLockTagForm gmLockTagForm; 
	ArrayList alUserList;
	ArrayList alLocationList;
	Iterator alListIter;
	String strOptKey = "";
	String strOptValue = "";
	String strCheck = "";
	String strCheckExcel = "";
	
	 public void init(PageContext pageContext, Object decorated, TableModel tableModel) { 	 
	        super.init(pageContext, decorated, tableModel); 
	        gmLockTagForm = (GmLockTagForm)getPageContext().getAttribute("frmLockTag", PageContext.REQUEST_SCOPE);
	        alUserList = gmLockTagForm.getAuditUserList();	    
	        alLocationList = gmLockTagForm.getLocationListWithType();
	        strCheckExcel = gmLockTagForm.getStrCheckExcel();
	       
	    } 

	 public String getPNUM() 
	{
		db = (DynaBean) this.getCurrentRowObject();
		String strPnum = GmCommonClass.parseNull((String) db.get("PNUM"));
		if(strCheckExcel.equals("CHECK")){
			strCheck = "checked";
		}		
		
		StringBuffer strValue = new StringBuffer();
		strValue.append(" <input type='checkbox' name='chk_transactionID' id ='");
		strValue.append(strPnum);
		strValue.append("' "+strCheck+"> &nbsp; ");		
		strValue.append(strPnum);
		return strValue.toString();
	}
	 public String getREFID() 
		{		
			db = (DynaBean) this.getCurrentRowObject();
			String strRefID = GmCommonClass.parseNull((String) db.get("REFID"));
			StringBuffer strValue = new StringBuffer();
			strValue.append(" <input type='hidden' name='txt_refid' value = '");
			strValue.append(strRefID);
			strValue.append("' />");
			strValue.append(strRefID);
			return strValue.toString();
		}

	public String getTAGCOUNT() 
	{		
		db = (DynaBean) this.getCurrentRowObject();
		String strTagCount = GmCommonClass.parseNull((String) db.get("TAGCOUNT"));
		if(strTagCount.equals("-"))
		strTagCount = "1";
		strTagCount = strTagCount == "" ?"1":strTagCount;
		StringBuffer strValue = new StringBuffer();
		strValue.append(" <input type='text' name='txt_tagCount' value = '");
		strValue.append(strTagCount);
		strValue.append("'/>");				
		return strValue.toString();
	}
	
	public String getCOUNTED_BY() 
	{	
		db = (DynaBean) this.getCurrentRowObject();
		String strCountedBy = String.valueOf(db.get("COUNTED_BY"));
		
		StringBuffer strValue = new StringBuffer();
		HashMap hmUserList = new HashMap();
		
		alListIter = alUserList.iterator();
		
		strValue.append(" <select name='cbo_countedBy'>");
		strValue.append(" <option value='0'>[Choose One] </option>");	
		
		while(alListIter.hasNext())
		{
			hmUserList = (HashMap) alListIter.next();
			strOptKey = (String)hmUserList.get("CODEID");
			strOptValue = (String)hmUserList.get("CODENM");
			if (strCountedBy.equals(strOptKey))
			{
				strOptKey += " selected"; 
			}
			strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
		}		
		strValue.append(" </select>");
		return strValue.toString();
	}
	
	public String getLOCATIONNM() 
	{	
		db = (DynaBean) this.getCurrentRowObject();
		String strLocationNm = String.valueOf(db.get("LOCATIONNM"));
		HashMap hmLocationList = new HashMap();
		StringBuffer strValue = new StringBuffer();
		alListIter = alLocationList.iterator();
		
		
			strValue.append(" <select name='cbo_Location'>");
			strValue.append(" <option value='0'>[Choose One] </option>");	
			
			while(alListIter.hasNext())
			{
				hmLocationList = (HashMap) alListIter.next();
				strOptKey = (String)hmLocationList.get("CODEID");
				strOptValue = (String)hmLocationList.get("CODENM");
				
				if(gmLockTagForm.getLocationID().equals(strOptKey))
				{
					strOptKey += " selected"; 
				}else if(strLocationNm.equals(strOptKey)){
					strOptKey += " selected"; 
				}
				
				strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + " </option>");			
			}		
			strValue.append(" </select>");		
		
		return strValue.toString();
	}
	
}
