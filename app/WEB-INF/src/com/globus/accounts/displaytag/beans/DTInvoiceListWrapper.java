/**
 * 
 */
package com.globus.accounts.displaytag.beans;

import java.text.DecimalFormat;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.accounts.forms.GmAPSaveInvoiceDtlsForm;

import org.displaytag.decorator.TotalTableDecorator;
import org.displaytag.model.TableModel;

/**
 * @author bvidyasankar
 *
 */
public class DTInvoiceListWrapper extends TotalTableDecorator{
	private DynaBean db;

    private int iPaymentID;
    private String strPOID;
    private StringBuffer  strValue;
    private DecimalFormat moneyFormat;
    
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	  
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
	
    public DTInvoiceListWrapper()
    {
    	super();
    	strValue	= new StringBuffer();
    	this.moneyFormat = new DecimalFormat("$#,###,###.00"); //$NON-NLS-1$
    }
	public String getPAYMENTID(){
		String strPaymentId = null;
		db = 	(DynaBean) this.getCurrentRowObject();
    	iPaymentID = Integer.parseInt((db.get("PAYMENTID")).toString());
    	strPOID = (db.get("POID")).toString();
    	strPaymentId = "<input type=\"radio\" name=\"paymentId\" onClick=\"fnLoadInvcDtls("+ iPaymentID +",'"+ strPOID +"',this.form);\" value=\"" + iPaymentID +"\"/>";
    	log.debug("decorator string " + strPaymentId);
		return strPaymentId;
	}
}
