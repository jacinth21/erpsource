package com.globus.accounts.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTDownloadInvoiceWrapper extends TableDecorator {
	
	private DynaBean db;
	private String strPayment = "";
	 
	public StringBuffer strValue = new StringBuffer();
	private int intCount = 0;
	private String strSageVendorID = "";
	private String strdisabled = "";
	
	public String getPAYMENTID() {
		int intPaymentID = 0;
		double dInvoiceAmt = 0;
	 
		db = (DynaBean) this.getCurrentRowObject(); 
		intPaymentID = Integer.parseInt(String.valueOf( db.get("PAYMENTID")));
		dInvoiceAmt = Double.parseDouble(String.valueOf( db.get("INVOICE_AMOUNT")));
		strSageVendorID =  String.valueOf( db.get("SAGE_VENDOR_ID"));
		 
		if(strSageVendorID.equals("null")) strdisabled="disabled";  else strdisabled="";
		
		strValue.setLength(0);

		strValue.append("<input class=RightText type='checkbox' "); 
		strValue.append(strdisabled);
		strValue.append(" name='Chk_Sage");
		strValue.append(String.valueOf(intCount));
		
		strValue.append("' onClick=javascript:fnCalcInvcAmt()");
	//	strValue.append("Chk_Sage" + intCount);
	//	strValue.append("',this.form)");
		strValue.append(" value=");
	//	strValue.append(strPayment);
		strValue.append(" > ");
		
		
		strValue.append("<input type=hidden name = hPaymentID");
		strValue.append(String.valueOf(intCount));
		strValue.append(" value=");
		strValue.append(intPaymentID);
		strValue.append("> ");
		 
		strValue.append("<input type=hidden name = hInvoiceAmt");
		strValue.append(String.valueOf(intCount));
		strValue.append(" value=");
		strValue.append(dInvoiceAmt);
		strValue.append("> ");

		intCount++;
		return strValue.toString();
	}
	 

}
