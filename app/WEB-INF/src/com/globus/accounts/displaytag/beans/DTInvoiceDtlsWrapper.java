package com.globus.accounts.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTInvoiceDtlsWrapper extends TableDecorator{

	
    private DynaBean db ;    
    private String  invoiceID = ""; 
    private String  paymentID = "";
    private String  poID = "";
    
    public String getINVOICEID()
    {
        db =  	(DynaBean) this.getCurrentRowObject();     
        invoiceID =  String.valueOf(db.get("INVOICEID")); 
        paymentID =  String.valueOf(db.get("PAYMENTID")); 
        if(!paymentID.equals("-"))
             return "&nbsp;<a href=javascript:fnInvcDetails('"  + paymentID + "')> "  + invoiceID + "</a>" ; 
        else 
        	 return "&nbsp;"  + invoiceID  ; 
    } 
    public String getPOID(){    	
    	db =  	(DynaBean) this.getCurrentRowObject();
		String  strPO = GmCommonClass.parseNull((String)db.get("POID"));
		return  "<a href= javascript:fnViewPO('" + strPO + "')>" + strPO + "</a>" ;
    }
    
}
