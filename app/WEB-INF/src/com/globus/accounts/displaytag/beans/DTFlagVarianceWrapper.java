package com.globus.accounts.displaytag.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.accounts.forms.GmFlagVarianceForm;
import com.globus.common.beans.GmCommonClass;



public class DTFlagVarianceWrapper extends TableDecorator  {

	private DynaBean db;
	GmFlagVarianceForm gmFlagVarianceForm;
	String strVarianceType="";
	ArrayList alNCodeID= new ArrayList();
	ArrayList alPCodeID= new ArrayList();
	ArrayList alDistList= new ArrayList();
	ArrayList alchange=new ArrayList();
	ArrayList alPostingOptions=new ArrayList();
	
	Iterator alListIter;
	String strOptKey = "";
	String strOptValue = "";
	String 	strFlag = "";
	String strId="";
	String strReason="";
	String strRefDetail="";
	String strApprove="";
	String strApproveBy="";
	String strLDate="";
	String strApproveDate="";
	String strSetId="";
	String strDistId="";
	String strCurDate="";
	String strPostOption="";
	String strPostComment="";
	private int intCount = 0;
	HashMap hmUserList = new HashMap();
	public DTFlagVarianceWrapper() 
	{
		super();
	}
	public void init(PageContext context, Object decorated, TableModel tableModel) {
		// TODO Auto-generated method stub
		super.init(context, decorated, tableModel);
		gmFlagVarianceForm = (GmFlagVarianceForm) getPageContext().getAttribute("frmFlagVarianceForm", PageContext.REQUEST_SCOPE);
		alNCodeID=gmFlagVarianceForm.getAlNCodeID();
		alPCodeID=gmFlagVarianceForm.getAlPCodeID();
		alDistList=gmFlagVarianceForm.getAlDistList();
		alchange=gmFlagVarianceForm.getAlchange();
		strVarianceType=gmFlagVarianceForm.getStrVarianceType();
		strSetId=gmFlagVarianceForm.getSetId();
		strDistId=gmFlagVarianceForm.getDistId();
		alPostingOptions=gmFlagVarianceForm.getAlPostingOptions();
		
	}
	public String getFLAG() 
	{
	     
		db = (DynaBean) this.getCurrentRowObject();
		strFlag = GmCommonClass.parseNull(String.valueOf(db.get("FLAG")));		
		 StringBuffer strValue = new StringBuffer();
		 
		 
		 if(this.isLastRow())
		 {
			 strValue.append("<input  TYPE='hidden' VALUE='");
			 strValue.append(String.valueOf(intCount));
			 strValue.append("' NAME='count'");
			 strValue.append("' SIZE='10' MAXLENGTH='25'>");
		 }
			
			 if(strVarianceType.equals("positive"))
			 {
				 alListIter = alPCodeID.iterator();
			 }
			 else{
				 alListIter = alNCodeID.iterator();
			 }
			strValue.append(" <select name='cbo_Flag");
			strValue.append(String.valueOf(intCount));
			strValue.append("' onchange ='fnChangeReason(this,");
			strValue.append(String.valueOf(intCount));
			strValue.append(")'>");
			strValue.append(" <option value='0'>[Choose One] </option>");	
			while(alListIter.hasNext())
			{
				hmUserList = (HashMap) alListIter.next();
				strOptKey = (String)hmUserList.get("CODEID");
				strOptValue = (String)hmUserList.get("CODENM");
				if (strFlag.equals(strOptKey))
				{
					strOptKey += " selected"; 
				}
				strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
			}		
			strValue.append(" </select>");
			
			strId = GmCommonClass.parseNull(String.valueOf(db.get("ID")));	

			 strValue.append("<input  TYPE='hidden' VALUE='");
			 strValue.append(strId);
			 strValue.append("' NAME='txt_id");
			 strValue.append(String.valueOf(intCount));
			 strValue.append("' SIZE='10' MAXLENGTH='25'>");
			
			
			return strValue.toString();
			
	}
	
	public String getREASONS() 
	{ 
		db = (DynaBean) this.getCurrentRowObject();
		strReason =GmCommonClass.parseNull(String.valueOf(db.get("REASON"))) ;		
		strApprove =GmCommonClass.parseNull(String.valueOf(db.get("APPROVE"))) ;
		 StringBuffer strValue = new StringBuffer();
		 String strUrl = "";
		 alListIter = alDistList.iterator();
		 if(strFlag.equals("51025")||strFlag.equals("51026")||strFlag.equals("51023")||strFlag.equals("51027"))
		 {
			 
			 strValue.append("<Div  style='display:block;display:inline' id='div_dist");
		 }
		 else{
			 strValue.append("<Div  style='display:none;' id='div_dist");
		 }
		strValue.append(String.valueOf(intCount));
		strValue.append("'> <select style='width:150px' name='cbo_dist");
		strValue.append(String.valueOf(intCount));
		strValue.append("'> <option value='0'>[Choose One] </option>");	
			while(alListIter.hasNext())
			{
				hmUserList = (HashMap) alListIter.next();
				strOptKey = (String)hmUserList.get("DISTID");
				strOptValue = (String)hmUserList.get("DISTNAME");
				if (strReason.equals(strOptKey))
				{
					strOptKey += " selected"; 
				}
				strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
			}		
			 strValue.append(" </select>");
			 strValue.append("</div>");
			 if(strFlag.equals("51029"))
			 {
				 strValue.append("<Div style='display:block;display:inline'  id='dev_tnx");
			 }
			 else
			 {
				 strValue.append("<Div style='display:none;' id='dev_tnx");
			 }
			 strValue.append(String.valueOf(intCount));
			 strValue.append("'> <input style='text-align:left' TYPE='text' VALUE='");
			 if(!strReason.equals("null"))
			 {
				 strValue.append(strReason);
				
			 }
			 strValue.append("' NAME='txt_tnx");
			 strValue.append(String.valueOf(intCount));
			 strValue.append("' onBlur ='fnVlaidateRA(");
			 strValue.append(String.valueOf(intCount));
			 strValue.append(");'");
			 if(strApprove.equals("51037") || strApprove.equals("51038"))
			 {
				 strValue.append(" disabled />");
			 }
			 else
			 {
			 strValue.append("/>");
			 }
			 
			 strValue.append("</div>");
			 if(strVarianceType.equals("negative"))
			 {
				 strLDate =GmCommonClass.parseNull(String.valueOf(db.get("LDATE"))) ;
				 strValue.append("<input  TYPE='hidden' VALUE='");
				 strValue.append(strLDate);
				 strValue.append("' NAME='ldate");
				 strValue.append(String.valueOf(intCount));
				 strValue.append("'>");
			 }
			 if(strFlag.equals("51029"))
			 {
				 strValue.append("<Div style='display:block;display:inline'  id='dev_but");
				 
			 }
			 else
			 {
				 strValue.append("<Div style='display:none;' id='dev_but");
			 }
		
			 
			 strValue.append(String.valueOf(intCount));
			 strValue.append("'>");
			 strCurDate =GmCommonClass.parseNull(String.valueOf(db.get("CURDATE"))) ;
			 strValue.append("<a href=\"#\"");
			 strUrl +=  "/gmReturnsCreditInfo.do?method=loadReturnsCreditedInfo&setId=";
			 strUrl +=  strSetId;
			 strUrl +=  "&distId=";
			 strUrl +=  strDistId;
			 strUrl +=  "&refId=";
			 strUrl +=  intCount ;
			 strValue.append(" onclick=\"javascript:fnOpenPopup(\'" + strUrl + "\');\"");
			 strValue.append("> ");
			 strValue.append("  <img border=0 Alt='Click to Look up RA No'  align=absmiddle src=/images/L.jpg height=10 width=9 />");
			 strValue.append("</a>");
			 strValue.append("</div>");
			 strRefDetail =GmCommonClass.parseNull(String.valueOf(db.get("REF_DETAILS"))) ;				 
			 alListIter = alchange.iterator();
			 if(strFlag.equals("51028"))
			 {
				 strValue.append("<Div style='display:block;display:inline' id='div_change");
			 }else{
				 strValue.append("<Div  style='display:none;' id='div_change");
			 }
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <select name='cbo_change");
			strValue.append(String.valueOf(intCount));
			strValue.append("' onchange ='fnCharge(this,");
			strValue.append(String.valueOf(intCount));
			strValue.append(")'> <option value='0'>[On Hold] </option>");	
			while(alListIter.hasNext())
				{
					hmUserList = (HashMap) alListIter.next();
					strOptKey = (String)hmUserList.get("CODEID");
					strOptValue = (String)hmUserList.get("CODENM");
					
					if (strReason.equals(strOptKey))
					{
						strOptKey += " selected"; 
					}
					strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
				}		
			strValue.append(" </select>");
			strValue.append("</div>");
			if(strFlag.equals("51026")||strFlag.equals("51027")||(strFlag.equals("51028")&&strReason.equals("51040")))
			 {
				strValue.append("<Div style='display:block;display:inline'  id='div_detail");
			 }
			else
			{
				strValue.append("<Div style='display:none;' id='div_detail");
			}
			strValue.append(String.valueOf(intCount));
			strValue.append("'> <input TYPE='text' VALUE='");
			 if(!strRefDetail.equals("null"))
			 {
				 strValue.append(strRefDetail);
			 }
			 else
			 {
				 strValue.append("");
			 }
				 
			 strValue.append("' NAME='txt_detail");
			 strValue.append(String.valueOf(intCount));
			 strValue.append("' SIZE='10' MAXLENGTH='25'");
			 if(strApprove.equals("51037") || strApprove.equals("51038"))
			 {
				 strValue.append(" disabled />");
			 }
			 else
			 {
			 strValue.append("/>");
			 }
			 strValue.append("</div>");
			
			 
			return strValue.toString();
		
	}
	public String getAPPROVED() 
	{ 
		
		db = (DynaBean) this.getCurrentRowObject();
		strApprove =GmCommonClass.parseNull(String.valueOf(db.get("APPROVE"))) ;
		StringBuffer strValue = new StringBuffer();
		strValue.append("<Div id='div_chk");
		strValue.append(String.valueOf(intCount));
		strValue.append("' > <input class=RightText type='checkbox' name='chk_box");
        strValue.append(String.valueOf(intCount) );
        
        if(strApprove.equals("51037") || strApprove.equals("51038"))
        {
        	strValue.append("' disabled checked ='true") ;
        }
     
        strValue.append("'  " );
        strValue.append("/> ") ; 
        
        strValue.append("<input  TYPE='hidden' VALUE='");
      
        	strValue.append(strApprove);
	
		 strValue.append("' NAME='approve");
		 strValue.append(String.valueOf(intCount));
		 strValue.append("'/></div>");
        return strValue.toString();
	}	
	public String getPOST() 
	{ 
		
		db = (DynaBean) this.getCurrentRowObject();
		strPostOption =GmCommonClass.parseNull(String.valueOf(db.get("POSTING_OPTION"))) ;
		//strPostOption="";
		StringBuffer strValue = new StringBuffer();
		strValue.append("<Div id='div_skip");
		strValue.append("'> <select style='width:100px' name='cbo_post");
		strValue.append(String.valueOf(intCount));
		strValue.append("'> <option value='0'>[Choose One] </option>");	
		 alListIter = alPostingOptions.iterator();
			while(alListIter.hasNext())
			{
				hmUserList = (HashMap) alListIter.next();
				strOptKey = (String)hmUserList.get("CODEID");
				strOptValue = (String)hmUserList.get("CODENM");
				if (strPostOption.equals(strOptKey))
				{
					strOptKey += " selected"; 
				}
				strValue.append(" <option value= " + strOptKey + " > " +  strOptValue + "</option>");			
			}		
			 strValue.append(" </select>");
		strPostComment =GmCommonClass.parseNull(String.valueOf(db.get("POSTING_COMMENT"))) ;
		strValue.append("&nbsp;<textarea rows='1' cols='20' id='txt_comment");
		strValue.append(String.valueOf(intCount));
		strValue.append("' >");
		
		if(!strPostComment.equals("null"))
		 {
			 strValue.append(strPostComment);
		 }
		strValue.append("</textarea>");
		
		 strValue.append("</div>");
		  intCount++;
        return strValue.toString();
	}
}
