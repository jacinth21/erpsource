/**
 * 
 */
package com.globus.accounts.displaytag.beans;

import java.text.DecimalFormat;

import javax.servlet.jsp.PageContext;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;

import com.globus.accounts.forms.GmAPSaveInvoiceDtlsForm;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmCommonClass;
import org.displaytag.decorator.TotalTableDecorator;
import org.displaytag.model.TableModel;


/**
 * @author bvidyasankar
 *
 */
public class DTDHRListWrapper extends TotalTableDecorator{
	private DynaBean db;

    private String strDHRID;
    private String strVID;
    private String strDHRShelfCost;
    private String strDHRRecCost;
    private String strDHRStatus;
    private String strDHRInfo;
    private String strValueFormatted;
    private String strPartNumber;
    private String strPartDesc;
    private String strPaymentId = "";
    private String strInvAmt = "";
    private String strExtInvAmt = "";
    private String strCreAmt= "";
    private String strExtInvAmtDiff= "";
    private String strDiffAmt= "";
    private String strQtyRec= "";
    private String strAcceptCost = "";
    private String strPFamily= "";
    private String strInvCost= "";
    private String strCREAmt= "";
    private DecimalFormat moneyFormat;
    private GmAPSaveInvoiceDtlsForm gmAPSaveInvoiceDtlsForm;
    private String strOpt;
    private int icheckCnt = 8;
    
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	
    private int intCount = 0;
    private int indexInvAmt = 0;
    private int indexExtInvAmt = 0;
    private int indexCreAmt = 0;
    private int indexDiff = 0;
    
    private double dubDiffAmt = 0.00;
    private double dubExtInvAmt = 0.00;
    private static DecimalFormat dubfmt = new DecimalFormat("0.00");
    
    Object objInvAmt = null;
    Object objCreAmt = null;
    
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		gmAPSaveInvoiceDtlsForm = (GmAPSaveInvoiceDtlsForm) getPageContext().getAttribute("frmAPInvoiceDtls", PageContext.REQUEST_SCOPE);
		if(gmAPSaveInvoiceDtlsForm != null){
			strOpt = gmAPSaveInvoiceDtlsForm.getStrOpt();
		}	
	}
	
	public String getCNUM(){
		StringBuffer strValue = new StringBuffer();
		try{
		db = 	(DynaBean) this.getCurrentRowObject();
		strDHRID = (db.get("ID")).toString();
		strValueFormatted =  ((String)db.get("ID")).replaceAll("-","");
		db = 	(DynaBean) this.getCurrentRowObject();
		strVID = (db.get("VID")).toString();
		strDHRShelfCost = db.get("SHELFCOST").toString();
		strDHRRecCost = db.get("RECCOST").toString();
		strDHRStatus = db.get("SFL").toString();
		
		strInvAmt = GmCommonClass.parseNull(String.valueOf(db.get("PAYMENTINVAMT")));
		strExtInvAmt = GmCommonClass.parseNull(String.valueOf(db.get("PAYMENTEXTINVAMT")));
		strCreAmt = GmCommonClass.parseNull(String.valueOf(db.get("PAYMENTCREAMT"))); //
		strDiffAmt = GmCommonClass.parseNull(String.valueOf(db.get("PAYMENTDIFF"))); //
		
        if(strInvAmt.equals("null")){
        	strInvAmt = "";
        }
        if(strExtInvAmt.equals("null")){
        	strExtInvAmt = "";
        }
        if(strCreAmt.equals("null")){
        	strCreAmt = "";
        }
        if(strDiffAmt.equals("null")){
        	strDiffAmt = "";
        }
		
		strDHRInfo = strDHRStatus + "^" + strDHRRecCost + "^" + strDHRShelfCost + "^" + strInvAmt + "^" + strExtInvAmt 
				+ "^" + strCreAmt + "^" + strDiffAmt;
		Object obj = db.get("PID");
		if(obj != null)
			strPaymentId = GmCommonClass.parseNull(obj.toString());
				
		
		strValue.append("<input class=RightText name='DHR' type='checkbox' "); 
		strValue.append(" id='Chk_DHR");
		strValue.append(intCount);
		strValue.append("' value='");
		strValue.append(strDHRInfo);
		strValue.append("' onClick=javascript:fnCalcDHRAmt('");
		strValue.append("Chk_DHR" + intCount);
		strValue.append("',this.form)");
		if(strOpt.equalsIgnoreCase("viewInvc"))
			strValue.append(" disabled=true");
		if(!strPaymentId.equals(""))
			strValue.append(" checked");
		strValue.append(" tabindex=\"" + icheckCnt++ +"\">");
		
		strPaymentId = "";
		
		//hidden value to fetch DHR Id
		strValue.append("<input type=hidden name ='hDHRID");
		strValue.append(intCount);
		strValue.append("' value='");
		strValue.append(strDHRID);
		strValue.append("'> ");
		//hidden value to fetch Vendor Id		
		strValue.append("<input type=hidden name ='hVID");
		strValue.append(intCount);
		strValue.append("' value='");
		strValue.append(strVID);
		strValue.append("'> ");
		
		strValue.append("<input type='hidden' value='");
		strValue.append(strDHRShelfCost);//This value is used to calculate Total in javascript
		strValue.append("'name='hDHRAmt");
		strValue.append(strValueFormatted);
		strValue.append("'>");
	
		intCount++;
    	log.debug("decorator string " + strValue);
		}catch(Exception e){
			e.printStackTrace();			
		}
		return strValue.toString();
	}
	
	public String getID(){
        db =  	(DynaBean) this.getCurrentRowObject();     
        strDHRID =  db.get("ID").toString();
        strVID = db.get("VID") .toString();
        return "&nbsp;<a tabindex='-1' href=javascript:fnViewDHR('"  + strDHRID + "')> "  + strDHRID + "</a>" ; 
	}
	
	public String getPNUM(){
		db =  	(DynaBean) this.getCurrentRowObject();     
		strPartNumber =  db.get("PNUM").toString();
		strPartDesc = db.get("PDESC").toString();
		return "<a title=\""+ strPartDesc +"\">"+ strPartNumber +"</a>";
	}
	
	public String getINVAMT(){
		StringBuffer strValue = new StringBuffer();
        db =  	(DynaBean) this.getCurrentRowObject();     
        strInvAmt =  GmCommonClass.parseNull(String.valueOf(db.get("PAYMENTINVAMT")));
        if(strInvAmt.equals("null")){
        	strInvAmt = GmCommonClass.parseNull(String.valueOf(db.get("USDCOST")));
        }

        strQtyRec =  GmCommonClass.parseNull(String.valueOf(db.get("QTYREC")));

        strValue.append("<input class=RightText name='TXT_INVAMT' type='text' "); 
        strValue.append(" id='TXT_INVAMT");
        strValue.append(indexInvAmt);
        strValue.append("' size = 10 value='");
        strValue.append(strInvAmt);
    
        strValue.append("' onchange=javascript:fnCreditAmount('");
		strValue.append(indexInvAmt);
		strValue.append("',this.form)");

		if(strOpt.equalsIgnoreCase("viewInvc"))
			strValue.append(" disabled=true");
		
		strValue.append(" attrqtyrec = '");
		strValue.append(strQtyRec);
        strValue.append("'>");

		
        indexInvAmt++;
        return strValue.toString();
	}
	
	public String getEXTINVAMT(){
		StringBuffer strValue = new StringBuffer();
        db =  	(DynaBean) this.getCurrentRowObject();     
        strExtInvAmt =  GmCommonClass.parseNull(String.valueOf(db.get("PAYMENTEXTINVAMT")));
        
        if(strExtInvAmt.equals("null")){
        	strExtInvAmt = GmCommonClass.parseNull(String.valueOf(db.get("RECCOST")));
        	dubExtInvAmt = Double.parseDouble(strExtInvAmt);
        	dubExtInvAmt = Double.parseDouble(dubfmt.format(dubExtInvAmt));
        	
        }
        
        strValue.append("<label class=RightText name='LBL_EXTINVAMT' "); 
        strValue.append(" id='LBL_EXTINVAMT");
		strValue.append(indexExtInvAmt);
        strValue.append("'>"+dubExtInvAmt+"</label>");
        
		//hidden value to fetch Quantity
		strValue.append("<input type=hidden id='hEXTINVAMT");
		strValue.append(indexExtInvAmt);
		strValue.append("' value='");
		strValue.append(dubExtInvAmt);
		strValue.append("'> ");
		
        indexExtInvAmt++;
        return strValue.toString();
	}
	public String getCREAMT(){
		StringBuffer strValue = new StringBuffer();
        db =  	(DynaBean) this.getCurrentRowObject();     
        strCreAmt =  GmCommonClass.parseNull(String.valueOf(db.get("PAYMENTCREAMT")));
        if(strCreAmt.equals("null")){
        	strCreAmt = "";
        }
        
        strValue.append("<input class=RightText name='TXT_CREAMT' type='text' "); 
        strValue.append(" id='TXT_CREAMT");
        strValue.append(indexCreAmt);
        strValue.append("' size = 10 value='");
        strValue.append(strCreAmt);
        strValue.append("' onchange=javascript:fnCreditAmount('");
  		strValue.append(indexCreAmt);
  		strValue.append("',this.form)");
  		
		if(strOpt.equalsIgnoreCase("viewInvc"))
			strValue.append(" disabled=true");
		
        strValue.append(">");
       
		
        indexCreAmt++;
        return strValue.toString();
	}
	public String getDIFF(){
		StringBuffer strValue = new StringBuffer();
        db =  	(DynaBean) this.getCurrentRowObject();  
        strCreAmt =  db.get("CREAMT").toString();
        strExtInvAmtDiff =  GmCommonClass.parseNull(String.valueOf(db.get("PAYMENTEXTINVAMT")));
        
        if(strExtInvAmtDiff.equals("null")){
        	strExtInvAmtDiff = GmCommonClass.parseNull(String.valueOf(db.get("SHELFCOST")));
        }
        
        strAcceptCost =  GmCommonClass.parseNull(String.valueOf(db.get("TCOST")));
        if(strAcceptCost.equals("null")){
        	strAcceptCost = "0";  // temp value
        }
        
        strPFamily =  GmCommonClass.parseNull(String.valueOf(db.get("PFAMYID")));
        if(strExtInvAmtDiff.equals("null")){
        	strPFamily = "0"; // temp value
        }
        
        dubDiffAmt = Double.parseDouble(strCreAmt) + Double.parseDouble(strExtInvAmtDiff) - Double.parseDouble(strAcceptCost);
        dubDiffAmt = Double.parseDouble(dubfmt.format(dubDiffAmt));
        strValue.append("<label class=RightText name='LBL_DIFF'"); 
        strValue.append(" id='LBL_DIFF");
        strValue.append(indexDiff);
        strValue.append("'>"+dubDiffAmt+"</label>");
        
        //hidden value to fetch ACCEPTCOST
        strValue.append("<input type=hidden id ='hACCEPTCOST");
        strValue.append(indexDiff);
        strValue.append("' value='");
        strValue.append(strAcceptCost);
        strValue.append("'> ");
        
		//hidden value to fetch Diff
		strValue.append("<input type=hidden id ='hDIFF");
		strValue.append(indexDiff);
		strValue.append("' value='");
		strValue.append(dubDiffAmt);
		strValue.append("'> ");
		
		//hidden value to fetch product family
		strValue.append("<input type=hidden id ='hPFamily");
		strValue.append(indexDiff);
		strValue.append("' value='");
		strValue.append(strPFamily);
		strValue.append("'> ");
		
		//hidden value to fetch PFamilyID -- 4050	Implants
		if (strPFamily.equals("4050")) {
			strValue.append("<input name='difference' type=hidden id ='hPFamyimplants");
			strValue.append(indexDiff);
			strValue.append("' value='");
			strValue.append(dubDiffAmt);
			strValue.append("'> "); 
		}
		else {
			strValue.append("<input name='difference' type=hidden id ='hPFamyOthers");
			strValue.append(indexDiff);
			strValue.append("' value='");
			strValue.append(dubDiffAmt);
			strValue.append("'> "); 	
		}
			
		
        indexDiff++;
        return strValue.toString();
	}
}
