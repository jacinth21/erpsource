package com.globus.accounts.displaytag.beans;

import java.text.DecimalFormat;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTMAIDsWrapper extends TableDecorator{

    
    private DynaBean db ;    
    private String strMAID =""; 
    
    public String getMAID()
    {
        db =  	(DynaBean) this.getCurrentRowObject();    
        strMAID     = (String)db.get("MAID");
        
        
             return "&nbsp;<a href=javascript:fnMAdetails('" + 
             strMAID + "')> "  + strMAID + "</a>" ;
       
    }
   
    
}


