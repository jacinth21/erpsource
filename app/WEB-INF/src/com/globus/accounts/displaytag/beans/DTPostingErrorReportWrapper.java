/**
 * @author pramaraj
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.globus.accounts.displaytag.beans;
import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.commons.lang.time.FastDateFormat;
import org.displaytag.decorator.TableDecorator;
import org.apache.commons.beanutils.DynaBean;

import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 
import com.globus.common.beans.GmCommonClass;


public class DTPostingErrorReportWrapper  extends TableDecorator{


     /**
     * DecimalFormat used to format money in getMoney().
     */
    private DynaBean db ;
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    
    
    /**
     * Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
     */
    public DTPostingErrorReportWrapper()
    {
        super();
    }

    public String getTSFID()
    {
        db =    (DynaBean) this.getCurrentRowObject();
        String strTransferId = GmCommonClass.parseNull((String) db.get("TRANSFERID")); 

      
     if(strTransferId.equals("null"))
     {
    	 strTransferId="";
    	 return strTransferId;
     }
     else
     {
    	 return "<a href=javascript:fnCallEditTransfer('" + strTransferId + "')> "  + strTransferId + "</a>" ;
     }
    }
    
}
