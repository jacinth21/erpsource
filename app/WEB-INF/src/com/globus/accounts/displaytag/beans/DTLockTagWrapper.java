package com.globus.accounts.displaytag.beans;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;
import org.apache.log4j.Logger;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;

public class DTLockTagWrapper extends TableDecorator {
	private int intDevCount;
	private double dblDevDollar = 0.0;
	private int intCountRecon = 0;
	private int intCountID = 0;
	private int intCountRefID = 0;
	private int intCountDev = 0;
	private int intSize = 0;
	public StringBuffer strValue = new StringBuffer();
	private ArrayList alList = new ArrayList();
	private HashMap hmVal = new HashMap();

	private String strCodeID;
	private String strSelected;
	private String strID;
	private String strDetail;
	private String strReconcilecombo;
	private String strMAstatus;
	HttpServletRequest httpRequest = null;

	GmLockTagForm gmLockTagForm;
	ArrayList alReconcile;
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		gmLockTagForm = (GmLockTagForm) getPageContext().getAttribute("frmLockTag", PageContext.REQUEST_SCOPE);

		alReconcile = gmLockTagForm.getReconcileList();
		intSize = alReconcile.size();

		strReconcilecombo = (String) getPageContext().getAttribute("reconcilecombo", PageContext.REQUEST_SCOPE);
		strMAstatus = (String) getPageContext().getAttribute("mastatus", PageContext.REQUEST_SCOPE);

		// log.debug("alUserList: "+ alUserList);
	}

	/**
	 * Returns the string for text field.
	 * 
	 * @return String
	 */
	public String getRECONCILEID() {
		DynaBean db = (DynaBean) this.getCurrentRowObject();
		StringBuffer strValue = new StringBuffer();
		String strSetActionType = String.valueOf(db.get("RECONCILEID"));

		// HttpServletRequest httpRequest = (HttpServletRequest)
		// getPageContext().getRequest();
		// ArrayList alReconcile = (ArrayList)
		// httpRequest.getAttribute("reconcile");

		// System.out.println("\t reconcile : " + alList);
		// ArrayList alReconcile = (ArrayList)
		// httpRequest.getAttribute("reconcile");
		// System.out.println("strReconcilecombo " + strReconcilecombo);
		// System.out.println("strSetActionType " + strSetActionType);

		strValue.setLength(0);

		// System.out.println("alReconcile is
		// *********************"+alReconcile);
		if (strSetActionType.equals("null")) {
			if (strMAstatus.equals("Y")) {
				strValue.append("");
			} else {
				strValue.append("<select name = setactiontype");
				strValue.append(String.valueOf(intCountRecon));
				strValue.append("> ");
				strValue.append(strReconcilecombo);
			}
		} else {
			strValue.append(String.valueOf(db.get("RECONCILENM")));
			// alReconcile
			/*
			 * strValue.append("<option value= >"); strValue.append("[Choose
			 * One]</option>");
			 * 
			 * for (int i = 0; i < intSize; i++) { hmVal = (HashMap)
			 * alReconcile.get(i); strCodeID = (String) hmVal.get("CODEID");
			 * strSelected = strSetActionType.equals(strCodeID) ? "selected" :
			 * ""; strValue.append("<option " + strSelected + " value= ");
			 * strValue.append(strCodeID + ">" + hmVal.get("CODENM") + "</option>"); }
			 * strValue.append("</select>");
			 */
		}

		// System.out.println(strValue);

		intCountRecon++;
		return strValue.toString();

		// return "<input type = text name = parvalue size = 10
		// value="+strPar+">";

	}

	public String getID() {
		DynaBean db = (DynaBean) this.getCurrentRowObject();
		StringBuffer strValue = new StringBuffer();
		strID = GmCommonClass.parseNull(String.valueOf(db.get("ID")));
		strID=(strID.equals("null")?"":strID);
		
		strValue.setLength(0);
		strValue.append(strID);
		
		strValue.append("<input type=hidden name = hPnum");
		strValue.append(String.valueOf(intCountID));
		strValue.append(" value=");
		strValue.append(strID);
		strValue.append("> ");

		intCountID++;
		return strValue.toString();

		// return "<input type = text name = parvalue size = 10
		// value="+strPar+">";

	}
	
	public String getREFID() {
		DynaBean db = (DynaBean) this.getCurrentRowObject();
		strID = GmCommonClass.parseNull(String.valueOf(db.get("REFID")));
		strID=(strID.equals("null")?"":strID);
		
		strValue.setLength(0);
		strValue.append(strID);
		
		strValue.append("<input type=hidden name = hRefID");
		strValue.append(String.valueOf(intCountRefID));
		strValue.append(" value=");
		strValue.append(strID);
		strValue.append("> ");

		intCountRefID++;
		
		return strValue.toString();
	}

	public String getDEVCOUNT() {
		double intDevCount = 0;
		DynaBean db = (DynaBean) this.getCurrentRowObject();
		StringBuffer strValue = new StringBuffer();
		//System.out.println(db.get("DEVCOUNT")
		intDevCount = Double.parseDouble(String.valueOf(db.get("DEVCOUNT")));
		strValue.setLength(0);
		// return strValue.toString();

		if (intDevCount < 0)
			strValue.append("<table><tr><td class=RightTextRed align=right> " + intDevCount + " </td></tr></table>");

		else
			strValue.append("<table><tr><td class=RightText align=right>  " + intDevCount + " </td></tr></table>");

		strValue.append("<input type=hidden name = hDevcount");
		strValue.append(String.valueOf(intCountDev));
		strValue.append(" value=");
		strValue.append(intDevCount);
		strValue.append("> ");

		intCountDev++;
		return strValue.toString();
	}

	public String getAFTERCOUNT() {
		double intAfterCount = 0;
		DynaBean db = (DynaBean) this.getCurrentRowObject();
		StringBuffer strValue = new StringBuffer();
		intAfterCount = Double.parseDouble(db.get("AFTERCOUNT").toString());
		strID = String.valueOf(db.get("ID"));
		// strDetail = String.valueOf(db.get("DETAIL"));
		// if (strID.indexOf('-') != -1)
		// strID = strDetail.substring(0, 7);
		strValue.setLength(0);
		// return strValue.toString();

		strValue.append("<table><tr><td class=RightTextRed align=right ><a href= javascript:fnViewDetails('" + strID + "')><font color = blue>" + intAfterCount
				+ "</font></a></td></tr></table>");

		return strValue.toString();
	}

	/*
	 * public String getDEVDOLLAR() { DynaBean db = (DynaBean)
	 * this.getCurrentRowObject(); double dblDevDollar = 0.0; dblDevDollar =
	 * Double.parseDouble(db.get("DEVDOLLAR").toString());
	 * strValue.setLength(0); //
	 * 
	 * if (dblDevDollar < 0) strValue.append("<table><tr><td class=RightTextRed align=right>" +
	 * dblDevDollar + "</td></tr></table>");
	 * 
	 * else strValue.append("<table><tr><td class=RightText align=right >" +
	 * dblDevDollar + "</td></tr></table>"); return strValue.toString(); }
	 */
}
