package com.globus.accounts.xml;

import com.globus.common.beans.AppError;

public interface GmInvoiceXmlInterface {
  public void generateInvocieXML(String strInvoiceId, String strUserId) throws AppError;

}
