package com.globus.accounts.xml;

import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.AllegatiType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.AnagraficaType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.CedentePrestatoreType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.CessionarioCommittenteType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.CodiceArticoloType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.CondizioniPagamentoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.ContattiTrasmittenteType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiAnagraficiCedenteType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiAnagraficiCessionarioType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiAnagraficiTerzoIntermediarioType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiBeniServiziType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiDDTType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiDocumentiCorrelatiType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiGeneraliDocumentoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiGeneraliType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiPagamentoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiRiepilogoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DatiTrasmissioneType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DettaglioLineeType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.DettaglioPagamentoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.EsigibilitaIVAType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.FatturaElettronicaBodyType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.FatturaElettronicaHeaderType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.FatturaElettronicaType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.FormatoTrasmissioneType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.IdFiscaleType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.IndirizzoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.IscrizioneREAType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.ModalitaPagamentoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.RegimeFiscaleType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.SocioUnicoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.SoggettoEmittenteType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.StatoLiquidazioneType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.TerzoIntermediarioSoggettoEmittenteType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.TipoDocumentoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.NaturaType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v1.AltriDatiGestionaliType;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.beans.GmInvoicePrintBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * GmFatturaXMLGeneration - This class used to generate the Italy XML file.
 *  This class called from - Generate invoice screen (after invoice generated this
 * class will call based on account parameter mapping - create xml flag.
 * 
 * If Create XML flag YES then this class call and generate the XML file.
 * 
 * @author mmuthusamy
 *
 */
public class GmFatturaXMLGeneration extends GmBean implements GmInvoiceXmlInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());

 /**
 * GmFatturaXMLGeneration - empty constructor method
 */
public GmFatturaXMLGeneration() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

 /**
 * 
 * GmFatturaXMLGeneration - parameter -constructor method
 * 
 * @param gmDataStoreVO
 * @throws AppError
 */
public GmFatturaXMLGeneration(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /* (non-Javadoc)
 * @see com.globus.accounts.xml.GmInvoiceXmlInterface#generateInvocieXML(java.lang.String, java.lang.String)
 * generateInvocieXML - This method used to create the XML file - using Fattura API
 * 
 */
@Override
  public void generateInvocieXML(String strInvoiceId, String strUserId) throws AppError {
    log.debug(" strInvoiceId " + strInvoiceId);
    getGmDataStoreVO().setCmpid("1020");
    getGmDataStoreVO().setCmpdfmt("dd/MM/YYYY");
    GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
    HashMap hmAccountParamData = new HashMap();
    hmAccountParamData = gmARBatchBean.fetchAccountParameterDtls(strInvoiceId);
    log.debug("hmAccountParamData"+hmAccountParamData);
    String strCompanyCode = GmCommonClass.parseNull((String) hmAccountParamData.get("COMP_CODE"));
    
    // based on Account id, to get the Exclude VAT flag (account attribute)
    String strExcludeVAT = GmCommonClass.parseNull((String) hmAccountParamData.get("EXCLUDE_ACCOUNT_VAT"));
    log.debug("strExcludeVAT"+strExcludeVAT);
    String strXmlPath = "";
    String strAccType = "";
    String strInvoicePath = GmCommonClass.parseNull(GmCommonClass.getString("BATCH_INVOICE"));
    String year = GmCalenderOperations.getCurrentDate("yyyy");
    String strInvoiceXML =
        strCompanyCode + "\\" + year + "\\xml\\IT02416870992_"
            + strInvoiceId.substring(3, strInvoiceId.length()) + ".xml";
    File file = new File(strInvoicePath + strCompanyCode + "\\" + year + "\\xml");
    if (!file.exists()) {
      file.mkdirs();
    }
    strInvoicePath = strInvoicePath + strInvoiceXML;

    FatturaElettronicaType fatturaElettronicaType = new FatturaElettronicaType();
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(getGmDataStoreVO());
    HashMap hmParam = gmInvoicePrintBean.fetchItalyInvHeaderDtls(strInvoiceId);
    log.debug(" Header details are " + hmParam);
    // Fetching account type public or private
    strAccType = GmCommonClass.parseNull((String) hmParam.get("ACC_TYPE"));
    // 1 - Header
    FatturaElettronicaHeaderType fatturaElettronicaHeaderType = new FatturaElettronicaHeaderType();
    fatturaElettronicaHeaderType = header(hmParam);
    // 2 - the block has a multiplicity of 1 in the case of a single invoice; in the case of an
    // invoice batch, the batch itself is repeated for every invoice of the batch
    FatturaElettronicaBodyType fatturaElettronicaBodyType = new FatturaElettronicaBodyType();
    hmParam.put("COMP_CODE", strCompanyCode);
    hmParam.put("EXCLUDE_ACCOUNT_VAT", strExcludeVAT);
    fatturaElettronicaBodyType = xmlBody(hmParam);
    // to set the version

    // Added for PMT-29950 , private account FPR_12 and public account FPA_12
    if (strAccType.equals("5502")) {
      fatturaElettronicaType.setVersione(FormatoTrasmissioneType.FPR_12);
    } else {
      fatturaElettronicaType.setVersione(FormatoTrasmissioneType.FPA_12);
    }
    fatturaElettronicaType.setFatturaElettronicaHeader(fatturaElettronicaHeaderType);
    fatturaElettronicaType.getFatturaElettronicaBody().add(fatturaElettronicaBodyType);

    try {
      createXML(strInvoicePath, fatturaElettronicaType);
      HashMap hmXmlParam = new HashMap();

      hmXmlParam.put("INVID", strInvoiceId);
      hmXmlParam.put("USERID", strUserId);
      hmXmlParam.put("XMLUPLOAD", "YES");
      hmXmlParam.put("XMLPATH", strInvoiceXML);

      gmARBatchBean.saveInvoiceAsXML(hmXmlParam);
    } catch (JAXBException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * createXML - This method used to create the XML file using Fattura API.
   * 			To form required data and set to FatturaElettronicaType class
   * 	
   * @param strXMLPath the str xml path
   * @param envelope the envelope
   * @throws JAXBException the JAXB exception
   */
  private void createXML(String strXMLPath, FatturaElettronicaType envelope) throws JAXBException {
    log.debug(" Inside the XML method ");
    // String strHttpXsdPath = "C:\\com\\udi\\italyxml\\fatturapa_v1.1.xsd";

    File file = new File(strXMLPath);
    JAXBContext jaxbContext = JAXBContext.newInstance(FatturaElettronicaType.class);
    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

    // output pretty printed
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    // jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, strHttpXsdPath);
    jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
    jaxbMarshaller.marshal(envelope, file);// marshal(envelope,file);
    log.debug(" XML file created " + strXMLPath);

  }

	/**
	 * header - this method used to set the Fattura object header details.
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
  
private FatturaElettronicaHeaderType header(HashMap hmParam) throws AppError {
    String strInvoiceId = (String) hmParam.get("INVID");
    String strAccType = GmCommonClass.parseNull((String) hmParam.get("ACC_TYPE"));
    log.debug(" invoice # " + strInvoiceId);
    FatturaElettronicaHeaderType fatturaElettronicaHeaderType = new FatturaElettronicaHeaderType();
    // 1.1 Document Transmitted
    DatiTrasmissioneType datiTrasmissioneType = new DatiTrasmissioneType();
    // 1.1.1 Unique Identification of the transmitter
    IdFiscaleType idFiscaleType = new IdFiscaleType();
    idFiscaleType.setIdPaese((String) hmParam.get("TRANS_COUNTRY_ID")); // Country ID
    idFiscaleType.setIdCodice((String) hmParam.get("TRANS_ID_CODE")); // ID code
    datiTrasmissioneType.setIdTrasmittente(idFiscaleType);
    // 1.1.2 unique progressive code attributed to the transmitter, relative to each single invoice
    // document
    datiTrasmissioneType.setProgressivoInvio(strInvoiceId);
    // 1.1.3 contains the identification code of the format/version in which the invoice document
    // has been transmitted
    // // Added for PMT-29950 , private account FPR_12 and public account FPA_12
    if (strAccType.equals("5502")) {
      datiTrasmissioneType.setFormatoTrasmissione(FormatoTrasmissioneType.FPR_12);
    } else {
      datiTrasmissioneType.setFormatoTrasmissione(FormatoTrasmissioneType.FPA_12);
    }
    // 1.1.4 code of the administrative office of the state of destination of the invoice, defined
    // by the administration to which it belongs as described in the "Indice PA" list.
    datiTrasmissioneType.setCodiceDestinatario((String) hmParam.get("TRANS_ADDRESS_CODE"));
    // 1.1.5 data relative to the transmitter's contacts
    ContattiTrasmittenteType contattiTrasmittenteType = new ContattiTrasmittenteType();
    // 1.1.5.1 fixed or mobile telephone contact
    contattiTrasmittenteType.setTelefono((String) hmParam.get("TRANS_TELE_PH"));
    // 1.1.5.2 email address
    contattiTrasmittenteType.setEmail((String) hmParam.get("TRANS_EMAIL"));
    datiTrasmissioneType.setContattiTrasmittente(contattiTrasmittenteType);
    // 1.1 setting
    fatturaElettronicaHeaderType.setDatiTrasmissione(datiTrasmissioneType);
    // 1.2 block always obligatory containing data relative to the seller/provider
    CedentePrestatoreType cedentePrestatoreType = new CedentePrestatoreType();
    // 1.2.1 block always obligatory containing the identity, professional and tax data of the
    // seller/provider
    DatiAnagraficiCedenteType datiAnagraficiCedenteType = new DatiAnagraficiCedenteType();
    // 1.2.1.1 tax identification number for VAT purposes; the first two characters represent the
    // country (IT, DE, ES, etc. ) and the remaining characters (up to a maximum of 28) are the
    // actual code which, for Italian residents, corresponds to their VAT number.
    idFiscaleType = new IdFiscaleType();
    // 1.2.1.1.1 country code according to the alpha-2 code of the ISO 3166-1 standard
    idFiscaleType.setIdPaese((String) hmParam.get("SELLER_COUNTRY_ID"));
    // 1.2.1.1.2 tax identification code
    idFiscaleType.setIdCodice((String) hmParam.get("SELLER_ID_CODE"));
    datiAnagraficiCedenteType.setIdFiscaleIVA(idFiscaleType);
    // 1.2.1.2 tax code
    // datiAnagraficiCedenteType.setCodiceFiscale("09876543210");
    // 1.2.1.3 the identity data of the seller/provider
    AnagraficaType anagraficaType = new AnagraficaType();
    // 1.2.1.3.1 firm, name or company name (firm, company, society, body) to be filled in instead
    // of fields 1.2.1.3.2 and 1.2.1.3.3
    anagraficaType.setDenominazione((String) hmParam.get("SELLER_COMPANY_NM"));
    datiAnagraficiCedenteType.setAnagrafica(anagraficaType);
    // 1.2.1.8 tax system
    datiAnagraficiCedenteType.setRegimeFiscale(RegimeFiscaleType.RF_01);
    cedentePrestatoreType.setDatiAnagrafici(datiAnagraficiCedenteType);
    // 1.2.2 block always obligatory containing the data of the head office of seller/provider
    IndirizzoType indirizzoType = new IndirizzoType();
    // 1.2.2.1 address of the seller's or provider's head office (name of street, square, etc.)
    indirizzoType.setIndirizzo((String) hmParam.get("SELLER_ADDRESS"));
    // 1.2.2.3 Post Code
    indirizzoType.setCAP((String) hmParam.get("SELLER_POST_CODE"));
    // 1.2.2.4 town/city where the seller's/provider's head office is located
    indirizzoType.setComune((String) hmParam.get("SELLER_CITY"));
    // 1.2.2.5 initials of the province of the town/city indicated in field 1.2.2.4
    indirizzoType.setProvincia((String) hmParam.get("SELLER_PROVINCIA"));
    // 1.2.2.6 country code according to the alpha-2 code of the ISO 3166-1 standard
    indirizzoType.setNazione((String) hmParam.get("SELLER_COUNTRY_ID"));
    cedentePrestatoreType.setSede(indirizzoType);
    // 1.2.3 block to be filled in if the seller/provider is not resident
    indirizzoType = new IndirizzoType();
    indirizzoType.setIndirizzo((String) hmParam.get("SELLER_ADDRESS"));
    indirizzoType.setCAP((String) hmParam.get("SELLER_POST_CODE"));
    indirizzoType.setComune((String) hmParam.get("SELLER_CITY"));
    indirizzoType.setProvincia((String) hmParam.get("SELLER_PROVINCIA"));
    indirizzoType.setNazione((String) hmParam.get("SELLER_COUNTRY_ID"));
    cedentePrestatoreType.setStabileOrganizzazione(indirizzoType);
    // 1.2.4 block to be filled in if the company is listed on the companies register pursuant to
    // Art. 2250 of the civil code
    IscrizioneREAType iscrizioneREAType = new IscrizioneREAType();
    // 1.2.4.1 initials of the province where the Companies' Registry Office at which the company is
    // registered is located
    iscrizioneREAType.setUfficio((String) hmParam.get("ADMIN_OFFICE"));
    // 1.2.4.2 companies register registration number
    iscrizioneREAType.setNumeroREA((String) hmParam.get("ADMIN_COM_REG_NO"));
    // 1.2.4.3 only in the case of a company (SpA, SApA, SRL), the field must be filled in with the
    // share
    // capital
    iscrizioneREAType.setCapitaleSociale(new BigDecimal(Double.parseDouble((String) hmParam
        .get("ADMIN_SHARE_CAPITAL"))).setScale(2, BigDecimal.ROUND_HALF_UP)); // Bigdecimal
    // 1.2.4.4 only in the case of a joint-stock company or limited liability company, the field
    // must be
    // filled in to indicate if there is a single shareholder or several shareholders
    iscrizioneREAType.setSocioUnico(SocioUnicoType.SU);
    // 1.2.4.5 indication of whether the Company is in liquidation or not.
    iscrizioneREAType.setStatoLiquidazione(StatoLiquidazioneType.LN);
    cedentePrestatoreType.setIscrizioneREA(iscrizioneREAType);
    fatturaElettronicaHeaderType.setCedentePrestatore(cedentePrestatoreType);
    // 1.4 block always obligatory containing data relative to the buyer/orderer
    CessionarioCommittenteType cessionarioCommittenteType = new CessionarioCommittenteType();
    // 1.4.1 block containing the tax and identity data of the buyer/orderer
    DatiAnagraficiCessionarioType datiAnagraficiCessionarioType =
        new DatiAnagraficiCessionarioType();
    // 1.4.1.1 tax identification number for VAT purposes; the first two characters represent the
    // country
    // (IT, DE, ES, etc. ) and the remaining characters (up to a maximum of 28) are the actual code
    // which, for Italian residents, corresponds to their VAT number.
    idFiscaleType = new IdFiscaleType();
    // 1.4.1.1.1 country code according to the alpha-2 code of the ISO 3166-1 standard
    idFiscaleType.setIdPaese((String) hmParam.get("BUYER_COUNTRY_ID"));
    // 1.4.1.1.2 tax identification code
    idFiscaleType.setIdCodice((String) hmParam.get("BUYER_ID_CODE"));
    datiAnagraficiCessionarioType.setIdFiscaleIVA(idFiscaleType);
    // 1.4.1.3 identity data of the buyer/orderer
    anagraficaType = new AnagraficaType();
    // 1.4.1.3.1 firm, name or company name (firm, company, society, body) to be entered instead of
    // fields 1.4.1.3.2 and 1.4.1.3.3
    anagraficaType.setDenominazione((String) hmParam.get("BUYER_COMPANY_NM"));
    datiAnagraficiCessionarioType.setAnagrafica(anagraficaType);
    cessionarioCommittenteType.setDatiAnagrafici(datiAnagraficiCessionarioType);
    // 1.4.2 block always obligatory containing the data of the buyer's/orderer's head office (in
    // the case of the supply of services such as electricity, gas, etc. the data may refer to the
    // location of the facility, pursuant to Min. Decree 370/2000)
    indirizzoType = new IndirizzoType();
    indirizzoType.setIndirizzo((String) hmParam.get("BUYER_ADDRESS"));
    indirizzoType.setCAP((String) hmParam.get("BUYER_POST_CODE"));
    indirizzoType.setComune((String) hmParam.get("BUYER_CITY"));
    // if shipping address - state present then adding the element.
    String strBuyerProvincia = GmCommonClass.parseNull((String) hmParam.get("BUYER_PROVINCIA"));
    if (!strBuyerProvincia.equals("")) {
      indirizzoType.setProvincia((String) hmParam.get("BUYER_PROVINCIA"));
    }
    indirizzoType.setNazione((String) hmParam.get("BUYER_COUNTRY_ID"));
    cessionarioCommittenteType.setSede(indirizzoType);
    fatturaElettronicaHeaderType.setCessionarioCommittente(cessionarioCommittenteType);
    String strCFNum = GmCommonClass.parseNull((String) hmParam.get("BUYER_CF_NUMBER"));
    // CF number is not present, displaying VAT NIP number
    if (!strCFNum.equals("")) {
      datiAnagraficiCessionarioType.setCodiceFiscale(strCFNum);
    } else {
      datiAnagraficiCessionarioType.setCodiceFiscale((String) hmParam.get("BUYER_ID_CODE"));
    }    // Account CF Number
    // 1.5 data relative to the third party which issues an invoice on behalf of the seller/provider
    TerzoIntermediarioSoggettoEmittenteType terzoIntermediarioSoggettoEmittenteType =
        new TerzoIntermediarioSoggettoEmittenteType();
    // 1.5.1 block containing the tax and identity data of the third party intermediary
    DatiAnagraficiTerzoIntermediarioType datiAnagraficiTerzoIntermediarioType =
        new DatiAnagraficiTerzoIntermediarioType();
    idFiscaleType = new IdFiscaleType();
    idFiscaleType.setIdPaese((String) hmParam.get("THIRD_PARTY_COUNTRY_ID"));
    idFiscaleType.setIdCodice((String) hmParam.get("THIRD_PARTY_ID_CODE"));
    datiAnagraficiTerzoIntermediarioType.setIdFiscaleIVA(idFiscaleType);

    anagraficaType = new AnagraficaType();
    // 1.5.1.3 identity data of the third party intermediary
    anagraficaType.setDenominazione((String) hmParam.get("THIRD_PARTY_COMPANY_NM"));
    datiAnagraficiTerzoIntermediarioType.setAnagrafica(anagraficaType);
    terzoIntermediarioSoggettoEmittenteType.setDatiAnagrafici(datiAnagraficiTerzoIntermediarioType);
    fatturaElettronicaHeaderType
        .setTerzoIntermediarioOSoggettoEmittente(terzoIntermediarioSoggettoEmittenteType);
    // 1.6 to be filled in in the case of documents issued by a subject other than the //
    // seller/provider;
    // indicating whether the invoice has been issued by the buyer/orderer or by a third party on
    // behalf of the buyer/orderer
    fatturaElettronicaHeaderType.setSoggettoEmittente(SoggettoEmittenteType.TZ);
    return fatturaElettronicaHeaderType;
  }



	/**
	 * xmlBody - this method used to set the details section to Fattura object.
	 * 			Next time modify the code - to refactor method (to split) by Mani 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
private FatturaElettronicaBodyType xmlBody(HashMap hmParam) throws AppError {
    String strInvoiceId = (String) hmParam.get("INVID");
    String strInvType = (String) hmParam.get("INV_TYPE");
    String strGmCompanyIBAN = GmCommonClass.parseNull((String) hmParam.get("COMPANY_IBAN"));
    GmARBean gmARBean = new GmARBean(getGmDataStoreVO());
    HashMap hmInvDtls = gmARBean.loadInvoiceDetails(strInvoiceId, "Print");
    HashMap hmInvoice = GmCommonClass.parseNullHashMap((HashMap) hmInvDtls.get("ORDERDETAILS"));
    HashMap hmCartDtls = GmCommonClass.parseNullHashMap((HashMap) hmInvDtls.get("CARTDETAILS"));
    HashMap hmOrderAtt = GmCommonClass.parseNullHashMap((HashMap) hmInvDtls.get("ORDER_ATTRIB"));
    log.debug(" hmOrderAtt " + hmOrderAtt);
    ArrayList alOrderNums =
        GmCommonClass.parseNullArrayList((ArrayList) hmInvDtls.get("ORDERNUMS"));
    ArrayList alLoop = null;
    HashMap hmTemp = null;
    Date dtInvDate = (Date) hmInvoice.get("INVDT");
    log.debug(" dtInvDate " + dtInvDate);
    String strCustPo = GmCommonClass.parseNull((String) hmInvoice.get("PO"));
    String strCIG = GmCommonClass.parseNull((String) hmOrderAtt.get("ORD_CIG"));
    String strEndPointId = GmCommonClass.parseNull((String) hmOrderAtt.get("ENDPOINT_ID"));
    String strAccountCIG = GmCommonClass.parseNull((String) hmInvoice.get("CIG"));
    strCIG = strCIG.equals("") ? strAccountCIG : strCIG;
    String strCustType = GmCommonClass.parseNull((String) hmParam.get("CUST_TYPE"));
    String strCompanyCode = GmCommonClass.parseNull((String) hmParam.get("COMP_CODE"));
    String strAccId = GmCommonClass.parseNull((String) hmInvoice.get("ACCID"));
    // based on Account id, to get the Exclude VAT flag (account attribute)
    String strExcludeVAT = GmCommonClass.parseNull((String) hmParam.get("EXCLUDE_ACCOUNT_VAT"));
    //PC-2192 View Vendita Document And Vendita Field in Xml File
    String strVenditaId = GmCommonClass.parseNull((String) hmParam.get("VENDITAID"));
    log.debug(" strVenditaId " + strVenditaId);
    String strAccType =
        GmCommonClass.parseNull(GmCommonClass.getAccountAttributeValue(strAccId, "5504"));
    

    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
   
    double dbInvAmt =
        GmCommonClass.roundDigit((Double.parseDouble((String) hmInvoice.get("INV_AMT"))), 2);
    log.debug(" dbInvAmt " + dbInvAmt);
    double dbInvTotalAmt = dbInvAmt;
    HashMap hmLoop = null;
    FatturaElettronicaBodyType fatturaElettronicaBodyType = new FatturaElettronicaBodyType();
    // 2.1 block always obligatory containing the general data of the main document and the data of
    // the connected documents
    DatiGeneraliType datiGeneraliType = new DatiGeneraliType();
    // 2.1.1 block always obligatory containing the general data of the main document
    DatiGeneraliDocumentoType datiGeneraliDocumentoType = new DatiGeneraliDocumentoType();
    if (strInvType.equals("50202")) { // Credit
      datiGeneraliDocumentoType.setTipoDocumento(TipoDocumentoType.TD_04);
    } else if (strInvType.equals("50203")) { // debit
      datiGeneraliDocumentoType.setTipoDocumento(TipoDocumentoType.TD_05);
    } else {
      datiGeneraliDocumentoType.setTipoDocumento(TipoDocumentoType.TD_01);
    }

    datiGeneraliDocumentoType.setDivisa("EUR");
    XMLGregorianCalendar invXmlCalendar = asXMLGregorianCalendar(dtInvDate);

    /*
     * Calendar cal = Calendar.getInstance(); cal.setTime(dtInvDate); int month =
     * cal.get(Calendar.MONTH) + 1; log.debug(" Invoice date and month details " +
     * cal.get(Calendar.DAY_OF_MONTH) + " " + month + " " + cal.get(Calendar.YEAR));
     * invXmlCalendar.setDay(cal.get(Calendar.DAY_OF_MONTH)); invXmlCalendar.setMonth(month);
     * invXmlCalendar.setYear(cal.get(Calendar.YEAR));
     */

    datiGeneraliDocumentoType.setData(invXmlCalendar);
    datiGeneraliDocumentoType.setNumero(strInvoiceId);
  //PC-2192 View Vendita Document And Vendita Field in Xml File
    if(!strVenditaId.equals("")){
        datiGeneraliDocumentoType.getCausale().add(strVenditaId);
    }

    datiGeneraliType.setDatiGeneraliDocumento(datiGeneraliDocumentoType);
    DatiDocumentiCorrelatiType datiDocumentiCorrelatiType = null;
    int x = 1;
    int iDDTNum = 1;

    DatiBeniServiziType datiBeniServiziType = new DatiBeniServiziType();
    DatiRiepilogoType datiRiepilogoType = null;
    DettaglioLineeType dettaglioLineeType = null;
    double dbItemTotalAll = 0.0;
    double dbItemOverAll = 0.0;
    double dbVat = 0.0;
    double dbTaxCost = 0.0;
    double dbTaxTotal = 0.0;
    double dbTempTotal = 0.0;
    double dbTempTaxCost = 0.0;
    //
    HashMap hmVatDtls = new HashMap();
    HashMap hmVatTemp = null;
    
    // loop the orders based on Invoice id
    for (int i = 0; i < alOrderNums.size(); i++) {
      hmTemp = (HashMap) alOrderNums.get(i);
      String strOrdId = GmCommonClass.parseNull((String) hmTemp.get("ID"));
      Date dtOrderDt = (Date) hmTemp.get("ORDERDATE");
      Date dtCustPoDt = (Date) hmTemp.get("CUST_PO_DATE");
      
      //if customer po date is null we need to get the current date for generating xml when issue credit (50202)
      //and issue debit(50203).
      if (dtCustPoDt == null && (strInvType.equals("50202") || strInvType.equals("50203"))) {
    	 dtCustPoDt = new Date();
    	 
    	    //when issue credit from generate issue screen at that time we need to pass customer po to generate xml to pdf conversion.
    	    // strInvType - 50202 (Credit) and company id - 1020 (Italy)
    	 String strDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    	    if(strCustPo.equals("")){    
    	    	strCustPo = strAccId+"-"+strDate; 
    	    	//GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strInvType, "CUST_PO_XML",getGmDataStoreVO().getCmpid()));
    	    }
      }

      alLoop = GmCommonClass.parseNullArrayList((ArrayList) hmCartDtls.get(strOrdId));
      log.debug(" inside the order count " + alOrderNums.size() + " dtCustPoDt " + dtCustPoDt);
    
      
      
      /*
       * Calendar calendarOrderDt = Calendar.getInstance(); calendarOrderDt.setTime(dtOrderDt); int
       * orderMonth = calendarOrderDt.get(Calendar.MONDAY) + 1;
       * orderXmlCalendar.setDay(calendarOrderDt.get(Calendar.DAY_OF_MONTH));
       * orderXmlCalendar.setMonth(orderMonth);
       * orderXmlCalendar.setYear(calendarOrderDt.get(Calendar.YEAR));
       */

      // Loop the line items based on order
      for (int j = 0; j < alLoop.size(); j++) {
        hmLoop = (HashMap) alLoop.get(j);
        String strPnum = GmCommonClass.parseNull((String) hmLoop.get("ID"));
        String strCnum = GmCommonClass.parseNull((String) hmLoop.get("USAGE_LOT"));
        String strDDT = GmCommonClass.parseNull((String) hmLoop.get("DDT"));
        String strDM = GmCommonClass.parseNull((String) hmLoop.get("DMVAL"));
        String strCND = GmCommonClass.parseNull((String) hmLoop.get("CNDVAL"));
        log.debug(" my lotto values is " + strCnum + " strDDT " + strDDT);
        // strCnum = strCnum.equals("") ? "NOC#" : strCnum;
        String strPartDesc =
            GmCommonClass.getStringWithTM(GmCommonClass.parseNull((String) hmLoop.get("PDESC")));
        log.debug("before xml " + strPartDesc);
        // escape XML
        strPartDesc = StringEscapeUtils.escapeXml(strPartDesc);
        String strAdminRef = GmCommonClass.parseNull((String) hmLoop.get("ADMIN_REF"));	//PC-4645-Add Ref  fields 
        String strOtherMgmtData = GmCommonClass.parseNull((String) hmLoop.get("OTHER_MGMT_DATA"));   //PC-4645-Add  CND # fields      
        String strVATPER = GmCommonClass.parseNull((String) hmLoop.get("VAT"));
        String strTaxCost = GmCommonClass.parseZero((String) hmLoop.get("TAX_COST"));
        String strPrice = GmCommonClass.parseNull((String) hmLoop.get("PRICE"));
        String strUnitPrice = GmCommonClass.parseZero((String) hmLoop.get("UNITPRICE"));
        String strQty = GmCommonClass.parseNull((String) hmLoop.get("QTY"));
        String strUom = GmCommonClass.parseNull((String) hmLoop.get("UOM"));
        String strPartVATPer = GmCommonClass.parseZero((String) hmLoop.get("PART_VAT_RATE"));
        strUom = strUom.equals("") ? "EA" : strUom;
        double dbQty = GmCommonClass.roundDigit(Double.parseDouble(strQty), 2);
        double dbItemTotal = Double.parseDouble(strPrice);

   
            
 // If item qty is in negative value , we are changing qty to positive value and unit price to negative value.
 // Fattura government site was not allowing negative qty for invoice
        if (dbQty < 0) {
          dbQty = dbQty * (-1);
          dbItemTotal = dbItemTotal * (-1);
        }

        dbItemTotalAll = GmCommonClass.roundDigit((dbQty * dbItemTotal), 2); // Multiply by

        // 5501- account type public,get part vat percentage.
        if (strAccType.equals("5501")) {
          strVATPER = strPartVATPer;
        }


        // Qty
        dbItemOverAll = dbItemOverAll + dbItemTotalAll;
        dbVat = GmCommonClass.roundDigit(Double.parseDouble(strVATPER), 2);
        dbTaxCost = GmCommonClass.roundDigit(Double.parseDouble(strTaxCost), 2);
        // 2.1.2 block containing the information relative to the purchase order

        datiDocumentiCorrelatiType = new DatiDocumentiCorrelatiType();
        datiDocumentiCorrelatiType.getRiferimentoNumeroLinea().add(x);
        datiDocumentiCorrelatiType.setIdDocumento(strCustPo);
        if (dtCustPoDt != null) {
        XMLGregorianCalendar custPOXmlCalendar = asXMLGregorianCalendar(dtCustPoDt);
        datiDocumentiCorrelatiType.setData(custPOXmlCalendar);
        }
        datiDocumentiCorrelatiType.setCodiceCommessaConvenzione(strEndPointId);
        if (!strCIG.equals("")) {
          datiDocumentiCorrelatiType.setCodiceCIG(strCIG);
        }


        datiGeneraliType.getDatiOrdineAcquisto().add(x - 1, datiDocumentiCorrelatiType);
        
        // Checking the DDT number - if values available then adding the attribute
        if (!strDDT.equals("")) {
          Date dtDDTUpdDt = (Date)hmLoop.get("DDT_UPD_DT");
          log.debug("dtDDTUpdDt Italy"+dtDDTUpdDt);
          XMLGregorianCalendar ddtUpdDtXmlCalendar = asXMLGregorianCalendar(dtDDTUpdDt);
          DatiDDTType datiDDTType = new DatiDDTType();

          datiDDTType.setNumeroDDT(strDDT);
          datiDDTType.setDataDDT(ddtUpdDtXmlCalendar);
          datiDDTType.getRiferimentoNumeroLinea().add(iDDTNum);
          log.debug(" log 1");
          datiGeneraliType.getDatiDDT().add(iDDTNum - 1, datiDDTType);
          iDDTNum++;
        }


        fatturaElettronicaBodyType.setDatiGenerali(datiGeneraliType);
        // 2.2 block always obligatory containing the nature, quality and quantity of the
        // goods/services
        // involved in the transaction
        // 2.2.1 block always obligatory containing the detail rows of the document (the fields of
        // the
        // block are repeated for every detail row)
        ArrayList<String> lineTypeRow = new ArrayList<String>();
        dettaglioLineeType = new DettaglioLineeType();
        dettaglioLineeType.setNumeroLinea(x);
        CodiceArticoloType codiceArticoloType = new CodiceArticoloType();
        codiceArticoloType.setCodiceTipo("Articolo");
        codiceArticoloType.setCodiceValore(strPnum);
        lineTypeRow.add(strPnum);
        log.debug(" log 2");
        dettaglioLineeType.getCodiceArticolo().add(0, codiceArticoloType);
        log.debug(" strCnum --> " + strCnum);
        if (!strCnum.equals("")) {
          codiceArticoloType = new CodiceArticoloType();
          codiceArticoloType.setCodiceTipo("Lotto");
          codiceArticoloType.setCodiceValore(strCnum);
          log.debug(" log 3");
          
          dettaglioLineeType.getCodiceArticolo().add(lineTypeRow.size(), codiceArticoloType);
          lineTypeRow.add(strCnum);
        }
        log.debug(" strDM --> " + strDM);
        log.debug(" strCND --> " + strCND);
        //To show DM and CND data in Sales Order and Invoice print(PC-141)
        if (!(strDM.equals("") && strCND.equals(""))) {
        	codiceArticoloType = new CodiceArticoloType();
            codiceArticoloType.setCodiceTipo("DM"+strDM);
            codiceArticoloType.setCodiceValore(strCND);
            dettaglioLineeType.getCodiceArticolo().add(lineTypeRow.size(), codiceArticoloType);
            lineTypeRow.add(strCND);
        }
        //pc-4934-Reposition RiferimentoAmministrazione and AltriDatiGestionali xml fields
        log.debug(" strAdminRef --> " + strAdminRef);
        if(!strAdminRef.equals("")) {
        dettaglioLineeType.setRiferimentoAmministrazione(strAdminRef);
        }
        log.debug(" strOtherMgmtData --> " + strOtherMgmtData);
        if(!strOtherMgmtData.equals("")) {
        dettaglioLineeType.getAltriDatiGestionali().add(getAltriDatiGestionaliType(strOtherMgmtData));
        }

        log.debug(" strPartDesc --> " + strPartDesc);
        dettaglioLineeType.setDescrizione(strPartDesc);
        dettaglioLineeType.setQuantita(new BigDecimal(dbQty).setScale(2, BigDecimal.ROUND_HALF_UP));
        dettaglioLineeType.setUnitaMisura(strUom);
        dettaglioLineeType.setPrezzoUnitario(new BigDecimal(dbItemTotal).setScale(2,
            BigDecimal.ROUND_HALF_UP));
        dettaglioLineeType.setPrezzoTotale(new BigDecimal(dbItemTotalAll).setScale(2,
            BigDecimal.ROUND_HALF_UP));
      //PMT-44586 Vatican city customer then, we are setting the VAT as N3
        if(strExcludeVAT.equals("")){
        	dettaglioLineeType.setAliquotaIVA(new BigDecimal(Double.parseDouble(strVATPER)).setScale(2,
                    BigDecimal.ROUND_HALF_UP));
        }else{
        	//PC-5799:Update VAT exemption code N3 to N3.1 for Vatican City invoice
        	dettaglioLineeType.setNatura (NaturaType.N_3_4);
        	dettaglioLineeType.setAliquotaIVA(new BigDecimal(0).setScale(2,
                    BigDecimal.ROUND_HALF_UP));
        	//dettaglioLineeType.setAliquotaIVA(NaturaType.fromValue("N_3"));
        }
        
        log.debug(" log 5");
        datiBeniServiziType.getDettaglioLinee().add(x - 1, dettaglioLineeType);
        log.debug(" log 5.123 " + strVATPER);
        hmVatTemp = GmCommonClass.parseNullHashMap((HashMap) hmVatDtls.get(strVATPER));
        dbTempTotal = GmCommonClass.parseDouble((Double) hmVatTemp.get("AMOUNT"));
        dbTaxTotal = GmCommonClass.parseDouble((Double) hmVatTemp.get("VAT_AMOUNT"));
        log.debug(" before VAT Temp values " + hmVatTemp);
        hmVatTemp = new HashMap();
        dbTempTotal = dbTempTotal + dbItemTotalAll;
        dbTaxTotal = dbTaxTotal + dbTaxCost;
        hmVatTemp.put("AMOUNT", dbTempTotal);
        hmVatTemp.put("VAT_AMOUNT", dbTaxTotal);
        hmVatDtls.put(strVATPER, hmVatTemp);
        //
        log.debug(" log X values " + hmVatDtls);
        x++;
      }
    }
    // loop the VAT details

    Iterator iter = hmVatDtls.keySet().iterator();
    while (iter.hasNext()) {
      String strTmpVat = GmCommonClass.parseNull((String) iter.next());
      hmVatTemp = GmCommonClass.parseNullHashMap((HashMap) hmVatDtls.get(strTmpVat));
      log.debug(" strTmpVat --> " + strTmpVat + " Vat Temp " + hmVatTemp);
      dbTempTotal = GmCommonClass.parseDouble((Double) hmVatTemp.get("AMOUNT"));
      dbTaxTotal = GmCommonClass.parseDouble((Double) hmVatTemp.get("VAT_AMOUNT"));
      dbVat = Double.parseDouble(strTmpVat);
      // 2.2.2 block always obligatory containing the summary data for every VAT rate or nature
      datiRiepilogoType = new DatiRiepilogoType();
      
    //PMT-44586 Vatican city customer then, we are setting the VAT as N3
      if(strExcludeVAT.equals("")){
    	  datiRiepilogoType.setAliquotaIVA(new BigDecimal(dbVat).setScale(2, BigDecimal.ROUND_HALF_UP));
      }else{
    	  //datiRiepilogoType.setAliquotaIVA(NaturaType.N_3);
    	  //PC-5799:Update VAT exemption code N3 to N3.1 for Vatican City invoice 
    	  datiRiepilogoType.setNatura(NaturaType.N_3_4);
    	  datiRiepilogoType.setAliquotaIVA(new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP));
    	  datiRiepilogoType.setRiferimentoNormativo (gmResourceBundleBean.getProperty("INVOICE.NORMATIVE_REFERENCE"));
      }
      datiRiepilogoType.setImponibileImporto(new BigDecimal(dbTempTotal).setScale(2,
          BigDecimal.ROUND_HALF_UP));
      log.debug(" dbTaxTotal --> " + dbTaxTotal);
      dbTaxTotal = (dbVat / 100) * dbTempTotal;
      log.debug(" calculation --> " + dbTaxTotal);
      dbTempTaxCost = dbTempTaxCost + GmCommonClass.roundDigit(dbTaxTotal, 2);
      // to remove the tax amount -if applied the exclude VAT
      dbTaxTotal = strExcludeVAT.equals("Y")? 0: dbTaxTotal;
      datiRiepilogoType
          .setImposta(new BigDecimal(dbTaxTotal).setScale(2, BigDecimal.ROUND_HALF_UP));
      
      //PMT-36118 :: Change VAT description in xml file based on Customer Category
      if (strAccType.equals("5501")) { // 5501	Public
    	  datiRiepilogoType.setEsigibilitaIVA(EsigibilitaIVAType.S); 
      }
      else if (strAccType.equals("5502")) { // 5502	Private
          datiRiepilogoType.setEsigibilitaIVA(EsigibilitaIVAType.I);
      }
      
      log.debug(" log 6");
      if (!strCustType.equals("")) {
        String strMsg = gmResourceBundleBean.getProperty("INVOICE." + strCustType);
        log.debug(" Msg " + strMsg + " cust type " + strCustType);
        datiRiepilogoType.setRiferimentoNormativo(strMsg);
      }

      datiBeniServiziType.getDatiRiepilogo().add(0, datiRiepilogoType);
    }
    fatturaElettronicaBodyType.setDatiBeniServizi(datiBeniServiziType);


 // Added for PMT-27351, to include vat amount in invoice total amount for Public account
    if (strAccType.equals("5501")) { //If Account Type is Public
    	// PMT-43914 : Changes in Invoice print and xml version
    	// To get only Invoice amount and without tax values and show the total
    	dbInvAmt = GmCommonClass.roundDigit((Double.parseDouble((String) hmInvoice.get("INV_AMT_WO_TAX"))), 2);
    	//dbInvAmt = GmCommonClass.roundDigit(dbInvAmt, 2);
    	dbInvTotalAmt = GmCommonClass.roundDigit(dbInvTotalAmt, 2) + GmCommonClass.roundDigit(dbTempTaxCost, 2);
    }
    
    datiGeneraliDocumentoType.setImportoTotaleDocumento(new BigDecimal(dbInvTotalAmt).setScale(2,
    	BigDecimal.ROUND_HALF_UP));
    // 2.4 data relating to the payment
    DatiPagamentoType datiPagamentoType = new DatiPagamentoType();
    datiPagamentoType.setCondizioniPagamento(CondizioniPagamentoType.TP_02);
    DettaglioPagamentoType dettaglioPagamentoType = new DettaglioPagamentoType();
    dettaglioPagamentoType.setModalitaPagamento(ModalitaPagamentoType.MP_05);
    Date dtPayTermsDt = (Date) hmParam.get("PAY_TERMS_DATE");

    /*
     * gregorianCalendar = new GregorianCalendar(); Calendar c1 = Calendar.getInstance();
     * c1.add(Calendar.DATE, payterm); gregorianCalendar.setTime(c1.getTime());
     * xmlCalendar.setDay(24); xmlCalendar.setMonth(11); xmlCalendar.setYear(2016);
     */
    XMLGregorianCalendar payXmlCalendar = asXMLGregorianCalendar(dtPayTermsDt);
    dettaglioPagamentoType.setDataScadenzaPagamento(payXmlCalendar);
    dettaglioPagamentoType.setImportoPagamento(new BigDecimal(dbInvAmt).setScale(2,
        BigDecimal.ROUND_HALF_UP));
    dettaglioPagamentoType.setIBAN(strGmCompanyIBAN);
    datiPagamentoType.getDettaglioPagamento().add(dettaglioPagamentoType);
    log.debug(" log 7");
    fatturaElettronicaBodyType.getDatiPagamento().add(datiPagamentoType);
    String strPdfFile = GmCommonClass.parseNull((String) hmParam.get("PDFFILE"));
    if (!strPdfFile.equals("")) {
      log.debug("Inside the attachment PDF ");
      // 2.5 Attachement
      AllegatiType allegatiType = new AllegatiType();
      allegatiType.setNomeAttachment("Fattura.PDF");
      allegatiType.setFormatoAttachment("PDF");
      allegatiType.setDescrizioneAttachment("Fattura " + strInvoiceId);
      try {
        allegatiType.setAttachment(getPDFBytes(strInvoiceId, strCompanyCode));
      } catch (FileNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
      fatturaElettronicaBodyType.getAllegati().add(allegatiType);
    }
    return fatturaElettronicaBodyType;
  }

  public byte[] getPDFBytes(String strInvoiceId, String strCompanyCode)
      throws FileNotFoundException {
    String strInvoicePath = GmCommonClass.parseNull(GmCommonClass.getString("BATCH_INVOICE"));
    String year = GmCalenderOperations.getCurrentDate("yyyy");
    String strPDFPath =
        strInvoicePath + strCompanyCode + "\\" + year + "\\" + strInvoiceId + ".pdf";
    log.debug(" PDF file to attached " + strPDFPath);
    File file = new File(strPDFPath);
    FileInputStream fis = new FileInputStream(file);
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    byte[] buf = new byte[1024];
    try {
      for (int readNum; (readNum = fis.read(buf)) != -1;) {
        bos.write(buf, 0, readNum); // no doubt here is 0
        // Writes len bytes from the specified byte array starting at offset off to this byte array
        // output stream.
        // System.out.println("read " + readNum + " bytes,");

      }
    } catch (IOException ex) {
      log.debug("Execption " + ex);
    }
    byte[] bytes = bos.toByteArray();



    byte[] encodedBytes = Base64.encodeBase64(bytes);
    // byte[] encodedBytes = Base64.encodeBase64("JavaTips.net".getBytes());
    // System.out.println("encodedBytes :::::::::::::" + new String(encodedBytes));
    return encodedBytes;
  }

  
  public XMLGregorianCalendar asXMLGregorianCalendar(Date dtDate) {
    XMLGregorianCalendar xmlCalendar = null;

    try {
      log.debug(" date is --> " + dtDate);
      String strDate = GmCommonClass.getStringFromDate(dtDate, "yyyy-MM-dd");
      log.debug(" return date " + strDate);
      DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
      xmlCalendar = datatypeFactory.newXMLGregorianCalendar(strDate);
    } catch (DatatypeConfigurationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return xmlCalendar;
  }
  
  //pc-4934-Reposition RiferimentoAmministrazione and AltriDatiGestionali xml fields
  /**
   * getAltriDatiGestionaliType - It is used to add tipo dato and rif testo details in AltriDatiGestionaliType
   * @param strOtherMgmtData
   * @return altriDatiGestionaliType
   */
  public AltriDatiGestionaliType getAltriDatiGestionaliType(String strOtherMgmtData) {
  AltriDatiGestionaliType altriDatiGestionaliType = new AltriDatiGestionaliType();
  altriDatiGestionaliType.setTipoDato("CND");
  altriDatiGestionaliType.setRiferimentoTesto(strOtherMgmtData);
  return altriDatiGestionaliType ;
  }
}