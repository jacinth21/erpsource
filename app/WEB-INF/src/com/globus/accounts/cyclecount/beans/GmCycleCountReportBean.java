package com.globus.accounts.cyclecount.beans;

import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmCycleCountReportBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @param gmDataStoreVO
   * @throws AppError
   */
  public GmCycleCountReportBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   * This method get accuracy Report Values
   * 
   * @param hmParam
   * @return
   * @throws AppError
   * @throws ParseException
   */
  public HashMap loadAccuracyReport(HashMap hmParam) throws AppError, ParseException {
    HashMap hmCountedQtyDtls = new HashMap();
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbDetailsQuery = new StringBuffer();
    GmCrossTabReport gmCrossTabReport = new GmCrossTabReport();

    String strLocation = GmCommonClass.parseZero((String) hmParam.get("WAREHOUSE"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strVarianceBy = GmCommonClass.parseZero((String) hmParam.get("VARIANCEBY"));
    String strAccuracyDetails = "";
    String strCurrentMonth = "";


    sbHeaderQuery.append("  SELECT refmonth  FROM   ( ");
    sbHeaderQuery.append(" SELECT TO_CHAR (t2705.c2705_lock_date, 'Mon YY') refmonth ");
    sbHeaderQuery.append(" FROM t2705_cycle_count_lock t2705, t2700_cycle_count t2700 ");
    sbHeaderQuery.append(" WHERE t2700.c2700_cycle_count_id      = t2705.c2700_cycle_count_id");
    sbHeaderQuery.append(" AND t2700.c5040_plant_id = " + getCompPlantId());
    sbHeaderQuery.append(" AND t2705.c901_lock_status = 106831 ");
    sbHeaderQuery.append(" AND t2705.c2705_void_fl IS NULL ");
    sbHeaderQuery.append(" AND t2700.C2700_VOID_FL IS NULL");
    if (!strLocation.equals("0")) {
      sbHeaderQuery.append(" AND t2705.C2701_CYC_CNT_LOCATION_ID = " + strLocation);
    }
    sbHeaderQuery.append(" AND t2705.c2705_lock_date BETWEEN TO_DATE('");
    sbHeaderQuery.append(strFromDate);
    sbHeaderQuery.append("','Mon YY') and LAST_DAY(TO_DATE('");
    sbHeaderQuery.append(strToDate);
    sbHeaderQuery.append("','Mon YY')) ");
    sbHeaderQuery.append(" group by TO_CHAR (t2705.c2705_lock_date, 'Mon YY') ");
    sbHeaderQuery.append(" ORDER BY TO_DATE(refmonth,'Mon YY') ");
    // used for Union all (so, that sequence order by month)
    sbHeaderQuery.append(" ) UNION ALL SELECT 'YTD' refmonth FROM dual ");


    log.debug(" sbHeaderQuery is " + sbHeaderQuery);

    // get the counted Qty Details
    strAccuracyDetails = getAccuracyDtlsQuery(hmParam);
    sbDetailsQuery = sbDetailsQuery.append(strAccuracyDetails);
    // to get the Year to Date column values
    Calendar calendar = GmCalenderOperations.getCalendarTZ();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM YY");
    strCurrentMonth = simpleDateFormat.format(calendar.getTime());

    hmParam.put("EXCLUDE_CURRENT_MONTH", strCurrentMonth);
    strAccuracyDetails = getAccuracyDtlsQuery(hmParam);
    sbDetailsQuery.append(" UNION  select ID, NAME, 'YTD', ROUND (AVG(refvalue), 1) from  ( ");
    sbDetailsQuery.append(strAccuracyDetails);
    sbDetailsQuery.append(" )  group by ID, NAME ");
    sbDetailsQuery.append(" Order by Name ");

    log.debug(" sbDetailsQuery is " + sbDetailsQuery);
    hmCountedQtyDtls =
        gmCrossTabReport
            .GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailsQuery.toString());


    return hmCountedQtyDtls;
  }

  /**
   * getAccuracyDtlsQuery - This method used to get get the cycle count accuracy reports - Details
   * sections
   * 
   * @param hmParam
   * @return String
   * @throws AppError
   */

  public String getAccuracyDtlsQuery(HashMap hmParam) throws AppError {

    StringBuffer sbDetailsQuery = new StringBuffer();

    String strLocation = GmCommonClass.parseZero((String) hmParam.get("WAREHOUSE"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strVarianceBy = GmCommonClass.parseZero((String) hmParam.get("VARIANCEBY"));
    String strExcludeMonth = GmCommonClass.parseNull((String) hmParam.get("EXCLUDE_CURRENT_MONTH"));

    sbDetailsQuery.append(" SELECT ID, NAME, refmonth ");

    // to change the Accuracy formula

    sbDetailsQuery.append(" , CASE ");
    sbDetailsQuery.append(" WHEN ABS (sys_data) >= ABS (variancy_data) ");
    sbDetailsQuery
        .append(" THEN ROUND ( ( (1          - (ABS (variancy_data) / ABS (DECODE(sys_data,0,1,sys_data)))) * 100), 1) "); // PMT#52901-Add DECODE for System count to make it 1 if it is 0
    sbDetailsQuery.append(" ELSE ROUND (ABS (sys_data) / ABS (variancy_data)*100, 1)  END refvalue ");

    sbDetailsQuery.append("   FROM (");

    sbDetailsQuery.append(" SELECT t901.c901_code_id ID, t901.c901_code_nm NAME ");
    sbDetailsQuery.append("  , TO_CHAR (t2705.c2705_lock_date, 'Mon YY') refmonth  ");

    if (strVarianceBy.equals("107080")) { // Variance by Currency (Corporate)
      sbDetailsQuery
          .append(" ,sum(NVL(t2706.c2706_cycle_count_cost, 0) * ABS(NVL (t2706.c2706_physical_qty, 0) -  ");
      sbDetailsQuery
          .append(" ABS(NVL (t2706.c2706_override_qty,t2706.c2706_system_qty)))) variancy_data ");
      sbDetailsQuery
          .append("  , sum(NVL(t2706.c2706_cycle_count_cost, 0) * NVL (t2706.c2706_physical_qty, 0)) sys_data ");
    } else if (strVarianceBy.equals("107081")) { // Variance by Currency (Local)
      sbDetailsQuery
          .append(" ,sum(NVL(t2706.c2706_cycle_count_local_cost, 0) * ABS(NVL (t2706.c2706_physical_qty, 0) -  ");
      sbDetailsQuery
          .append(" ABS(NVL (t2706.c2706_override_qty,t2706.C2706_SYSTEM_QTY)))) variancy_data ");
      sbDetailsQuery
          .append("  , sum(NVL(t2706.c2706_cycle_count_local_cost, 0) * NVL (t2706.c2706_physical_qty, 0)) sys_data ");

    } else { // Variance by Qty
      sbDetailsQuery
          .append(" ,SUM (ABS(NVL (t2706.c2706_physical_qty, 0) - ABS(NVL (t2706.c2706_override_qty, t2706.c2706_system_qty)))) variancy_data "
              + ", SUM (NVL (t2706.c2706_physical_qty, 0)) sys_data ");
    }

    sbDetailsQuery
        .append("  FROM t2706_cycle_count_lock_dtls t2706, t2705_cycle_count_lock t2705, t2701_cycle_count_location t2701 ");
    sbDetailsQuery.append(" , t901_code_lookup t901, t2700_cycle_count t2700 ");
    sbDetailsQuery.append("  WHERE t2705.c2705_cyc_cnt_lock_id     = t2706.c2705_cyc_cnt_lock_id ");
    sbDetailsQuery
        .append(" AND t2701.c2701_cyc_cnt_location_id = t2705.c2701_cyc_cnt_location_id  ");
    sbDetailsQuery.append(" AND t2700.c2700_cycle_count_id = t2701.c2700_cycle_count_id ");
    sbDetailsQuery.append(" and t901.c901_code_id = t2701.c901_warehouse_location ");
    // filter condition
    if (!strLocation.equals("0")) {
      sbDetailsQuery.append(" AND t2705.c2701_cyc_cnt_location_id = " + strLocation);
    }

    if (!strFromDate.equals("") && !strFromDate.equals("")) {
      sbDetailsQuery.append(" AND t2705.c2705_lock_date >= TO_DATE('" + strFromDate + "','"
          + "Mon/YY" + "')");
      sbDetailsQuery.append(" AND t2705.c2705_lock_date <= LAST_DAY(TO_DATE ('" + strToDate + "','"
          + "Mon/YY" + "'))");
    }
    sbDetailsQuery.append(" AND t2700.c5040_plant_id = '" + getCompPlantId() + "' ");

    sbDetailsQuery.append(" AND t2705.c901_lock_status = 106831  "); // 106831 - Approved
    sbDetailsQuery.append(" AND t2700.c2700_void_fl IS NULL ");
    sbDetailsQuery.append(" AND t2705.c2705_void_fl            IS NULL ");
    sbDetailsQuery.append(" AND t2706.c2706_void_fl            IS NULL ");
    sbDetailsQuery.append(" AND t2701.C2701_VOID_FL            IS NULL ");
    sbDetailsQuery.append(" AND t2706.c2706_physical_qty       IS NOT NULL ");
    sbDetailsQuery.append(" GROUP BY t901.c901_code_id,  t901.c901_code_nm ");
    sbDetailsQuery.append(" , TO_CHAR (t2705.c2705_lock_date, 'Mon YY') ");
     sbDetailsQuery.append(" ) ");
    if (!strExcludeMonth.equals("")) {
    	sbDetailsQuery.append(" where refmonth <> '" + strExcludeMonth + "' ");
    }

    return sbDetailsQuery.toString();

  }

  /**
   * fetchRankList - This method used to fetch Rank list.
   * 
   * @param hmParam
   * @throws AppError
   */

  public ArrayList fetchRankList() throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    String strCycleCountId = "1";
    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_rpt.gm_fch_cycle_count_rank", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCycleCountId);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();

    return alResult;
  }

  /**
   * loadCountedQtyReport - This method used to load counted Qty.
   * 
   * @param hmParam
   * @throws AppError
   */

  public HashMap loadCountedQtyReport(HashMap hmParam) throws AppError, ParseException {
    HashMap hmCountedQtyDtls = new HashMap();
    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbDetailsQuery = new StringBuffer();
    GmCrossTabReport gmCrossTabReport = new GmCrossTabReport();
    String strLocation = GmCommonClass.parseZero((String) hmParam.get("WAREHOUSE"));
    String strPartnumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strRank = GmCommonClass.parseZero((String) hmParam.get("RANK"));
    String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODATE"));

    gmCrossTabReport.setAcceptExtraColumn(true);

    gmCrossTabReport.setExtraColumnParams(4, "refvalue");
    gmCrossTabReport.setExtraColumnParams(5, "Description");
    gmCrossTabReport.setExtraColumnParams(6, "refyear");
    gmCrossTabReport.setExtraColumnParams(7, "refmonthtype");


    // to get the current year Jan to Dec month

    sbHeaderQuery.append(" SELECT  TO_CHAR(v9001.month_value,'Mon YY') refmonth ");
    sbHeaderQuery.append(" FROM v9001_month_list v9001 ");
    sbHeaderQuery.append(" WHERE v9001.month_value BETWEEN TO_DATE('");
    sbHeaderQuery.append(strFromDt);
    sbHeaderQuery.append("','Mon YY') and LAST_DAY(TO_DATE('");
    sbHeaderQuery.append(strToDt);
    sbHeaderQuery.append("','Mon YY')) ORDER BY TO_DATE(refmonth,'Mon YY') ");

    log.debug(" sbHeaderQuery is " + sbHeaderQuery);

    // get the counted Qty Details
    sbDetailsQuery.append(" SELECT t2706.c205_part_number_id ID, t2706.c205_part_number_id NAME ");
    sbDetailsQuery.append("  , TO_CHAR (t2705.C2705_LOCK_DATE , 'Mon YY') refmonth  ");
    // sbDetailsQuery.append(" , get_code_name (t2701.C901_WAREHOUSE_LOCATION) refvalue ");
    sbDetailsQuery.append(" ,  COUNT (t2706.c2706_physical_qty) refvalue ");
    sbDetailsQuery.append(", t205.c205_part_num_desc Description ");
    sbDetailsQuery.append(",  t901.c901_code_nm refyear, ");
    sbDetailsQuery.append(" t2703.C2703_RANK  refmonthtype ");
    sbDetailsQuery
        .append("  FROM t2706_cycle_count_lock_dtls t2706, t2705_cycle_count_lock t2705, t2701_cycle_count_location t2701 ");
    sbDetailsQuery
        .append(" , t205_part_number t205, t2703_cycle_count_rank t2703, t901_code_lookup t901, t2700_cycle_count t2700 ");
    sbDetailsQuery.append("  WHERE t2705.c2705_cyc_cnt_lock_id     = t2706.c2705_cyc_cnt_lock_id ");
    sbDetailsQuery.append(" AND t205.c205_part_number_id        = t2706.c205_part_number_id ");
    sbDetailsQuery.append(" AND t2700.c2700_cycle_count_id = t2701.c2700_cycle_count_id ");
    sbDetailsQuery
        .append(" AND t2701.c2701_cyc_cnt_location_id = t2705.c2701_cyc_cnt_location_id  ");
    sbDetailsQuery.append(" AND t205.c205_part_number_id        = t2703.c205_part_number_id (+) ");
    sbDetailsQuery
        .append(" AND t2701.c2701_cyc_cnt_location_id = t2703.c2701_cyc_cnt_location_id (+) ");
    sbDetailsQuery.append(" and t901.c901_code_id = t2701.c901_warehouse_location ");
    sbDetailsQuery.append(" and t2700.c5040_plant_id = '" + getCompPlantId() + "' ");
    // filter condition
    if (!strLocation.equals("0")) {
      sbDetailsQuery.append(" AND t2705.c2701_cyc_cnt_location_id = " + strLocation);
    }
    if (!strPartnumber.equals("")) {
    	sbDetailsQuery.append(" AND regexp_like (t2706.c205_part_number_id, REGEXP_REPLACE('");
    	sbDetailsQuery.append(strPartnumber);
    	sbDetailsQuery.append("','[+]','\\+'))");

    }

    if (!strFromDt.equals("") && !strToDt.equals("")) {

      sbDetailsQuery.append(" AND t2705.C2705_LOCK_DATE  >= TO_DATE('" + strFromDt + "','"
          + "Mon/YY" + "')");
      sbDetailsQuery.append(" AND t2705.C2705_LOCK_DATE  <= LAST_DAY(TO_DATE ('" + strToDt + "','"
          + "Mon/YY" + "'))");
    }
    if (!strRank.equals("0")) {
      sbDetailsQuery.append(" AND t2703.c2703_rank = '" + strRank + "'");
    }

    sbDetailsQuery.append(" AND t2700.C2700_VOID_FL IS NULL ");
    sbDetailsQuery.append(" AND t2701.C2701_VOID_FL IS NULL ");
    sbDetailsQuery.append(" AND t2705.c2705_void_fl            IS NULL ");
    sbDetailsQuery.append(" AND t2706.c2706_void_fl            IS NULL ");
    sbDetailsQuery.append(" AND t2706.c2706_physical_qty       IS NOT NULL ");
    sbDetailsQuery
        .append(" GROUP BY t2706.c205_part_number_id, t205.c205_part_num_desc,  t901.c901_code_nm ");
    sbDetailsQuery
        .append(" , t2705.c2701_cyc_cnt_location_id, t2703.c2703_rank, TO_CHAR(t2705.C2705_LOCK_DATE , 'Mon YY') ");
    sbDetailsQuery
        .append("ORDER BY  t901.c901_code_nm, t2703.c2703_rank, t2706.c205_part_number_id  ");


    log.debug(" sbDetailsQuery is " + sbDetailsQuery);
    hmCountedQtyDtls =
        gmCrossTabReport
            .GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailsQuery.toString());

    return hmCountedQtyDtls;
  }

  /**
   * fetchCycleCountYear - This method used to fetch Cycle Count Year list.
   * 
   * @return
   * @throws AppError
   */
  public ArrayList fetchCyclyCountYear() throws AppError {
    ArrayList alCountYear = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_rpt.gm_fch_cycle_count_year", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alCountYear =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(1)));
    gmDBManager.close();
    return alCountYear;

  }


  /**
   * loadRankReport - This method used to load Rank Report.
   * 
   * @param hmParam
   * @throws AppError
   */

  public ArrayList loadRankReport(HashMap hmParam) throws AppError, ParseException {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();

    String strCycleCount = GmCommonClass.parseZero((String) hmParam.get("CYCLECOUNTYEAR"));
    String strRank = GmCommonClass.parseZero((String) hmParam.get("RANK"));
    String strPartnumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));
    String strProjectId = GmCommonClass.parseZero((String) hmParam.get("PROJECTID"));
    String strProductFamily = GmCommonClass.parseZero((String) hmParam.get("PRODUCTFAMILY"));

    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_rpt.gm_fch_rank_rpt", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCycleCount);
    gmDBManager.setString(2, strRank);
    gmDBManager.setString(3, strPartnumber);
    gmDBManager.setString(4, strProjectId);
    gmDBManager.setString(5, strProductFamily);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(6)));
    gmDBManager.close();
    return alResult;

  }


  /**
   *
   * loadFullDataReport - Method used to Load the cycle count Data details
   * 
   * @param hmParam
   * @return ArrayList
   * @throws AppError
   * @throws ParseException
   */
  public ArrayList loadFullDataReport(HashMap hmParam) throws AppError, ParseException {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();


    String strWareHouse = GmCommonClass.parseZero((String) hmParam.get("WAREHOUSE"));
    String strPart = GmCommonClass.parseNull((String) hmParam.get("PART"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strCycleCountLockId = GmCommonClass.parseNull((String) hmParam.get("CYCLECOUNTID"));
    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_rpt.gm_fch_full_data_rpt", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.setString(1, strWareHouse);
    gmDBManager.setString(2, strPart);
    gmDBManager.setDate(3, GmCommonClass.getStringToDate(strFromDate, getCompDateFmt()));
    gmDBManager.setDate(4, GmCommonClass.getStringToDate(strToDate, getCompDateFmt()));
    gmDBManager.setString(5, strCycleCountLockId);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(6)));
    gmDBManager.close();
    log.debug(" Locked size ==> " + alResult.size());
    return alResult;
  }


  /**
   * 
   * generateOutput - Array list converted to XML string using (VM)
   * 
   * @param alGridData
   * @param hmDtls
   * @return String
   * @throws AppError
   */
  public String generateOutput(ArrayList alGridData, HashMap hmDtls) throws AppError {
    GmTemplateUtil gmTemplateUtil = new GmTemplateUtil();
    String strTemplateName = GmCommonClass.parseNull((String) hmDtls.get("TEMPLATE_NM"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmDtls.get("STRSESSCOMPANYLOCALE"));
    String strLabelProperties = GmCommonClass.parseNull((String) hmDtls.get("LABEL_PROPERTIES_NM"));
    gmTemplateUtil.setDataList("alGridData", alGridData);
    gmTemplateUtil.setDataMap("hmApplnParam", hmDtls);
    gmTemplateUtil.setTemplateName(strTemplateName);
    gmTemplateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strLabelProperties,
        strSessCompanyLocale));
    gmTemplateUtil.setTemplateSubDir("accounts/cyclecount/templates");
    return gmTemplateUtil.generateOutput();
  }

}
