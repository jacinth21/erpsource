package com.globus.accounts.cyclecount.beans;

import java.sql.ResultSet;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;



/**
 * 
 * GmCycleCountLockReportBean - cycle count Reports related method
 * 
 * @author mmuthusamy
 *
 */


public class GmCycleCountLockReportBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @param gmDataStoreVO
   * @throws AppError
   */

  public GmCycleCountLockReportBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   *
   * fetchDashBoardDtls - Method used to fetch the Dashboard details (Cycle count lock details)
   * 
   * @param hmParam
   * @return ArrayList
   * @throws AppError
   * @throws ParseException
   */
  public ArrayList fetchDashBoardDtls(HashMap hmParam) throws AppError, ParseException {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();

    String strCycleCountLockId = GmCommonClass.parseNull((String) hmParam.get("CYCLECOUNTID"));
    String strWareHouse = GmCommonClass.parseZero((String) hmParam.get("WAREHOUSE"));
    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
    String strStatus = GmCommonClass.parseZero((String) hmParam.get("STATUS"));
    // to convert the case
    strCycleCountLockId = strCycleCountLockId.toUpperCase();
    if (!strCycleCountLockId.equals("")
        && (strCycleCountLockId.indexOf("CC") == -1 || strCycleCountLockId.indexOf("-") == -1)) {
      strCycleCountLockId = "CC-" + strCycleCountLockId;
    }
    log.debug(" After convert strCycleCountLockId " + strCycleCountLockId);

    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_rpt.gm_fetch_warehouse_dtls", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCycleCountLockId);
    gmDBManager.setString(2, strWareHouse);
    gmDBManager.setDate(3, GmCommonClass.getStringToDate(strFromDate, getCompDateFmt()));
    gmDBManager.setDate(4, GmCommonClass.getStringToDate(strToDate, getCompDateFmt()));
    gmDBManager.setString(5, strStatus);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(6)));
    gmDBManager.close();
    log.debug(" Locked size ==> " + alResult.size());
    return alResult;
  }

  /**
   * fetchWarehoueType - to fetch the current year cycle count warehouse type (based on plant)
   * 
   * @return Array List
   * @throws AppError
   */
  public ArrayList fetchWarehoueType() throws AppError {
    ArrayList alWarehouse = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_rpt.gm_fetch_warehouse_type", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alWarehouse = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    log.debug("alWarehouse size ==> " + alWarehouse.size());
    return alWarehouse;
  }



  /**
   * 
   * generateOutput - Array list converted to XML string using (VM)
   * 
   * @param alGridData
   * @param hmDtls
   * @return String
   * @throws AppError
   */
  public String generateOutput(ArrayList alGridData, HashMap hmDtls) throws AppError {
    GmTemplateUtil gmTemplateUtil = new GmTemplateUtil();
    String strTemplateName = GmCommonClass.parseNull((String) hmDtls.get("TEMPLATE_NM"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmDtls.get("STRSESSCOMPANYLOCALE"));
    String strLabelProperties = GmCommonClass.parseNull((String) hmDtls.get("LABEL_PROPERTIES_NM"));

    gmTemplateUtil.setDataList("alGridData", alGridData);
    gmTemplateUtil.setDataMap("hmApplnParam", hmDtls);
    gmTemplateUtil.setTemplateName(strTemplateName);
    gmTemplateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strLabelProperties,
        strSessCompanyLocale));
    gmTemplateUtil.setTemplateSubDir("accounts/cyclecount/templates");

    return gmTemplateUtil.generateOutput();

  }

  /**
   * loadCycleCountInfo - this method used to fetch the cycle count lock Header and details section
   * 
   * @param hmParam
   * @return hashmap
   * @throws AppError
   */
  public HashMap loadCycleCountInfo(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    HashMap hmHeader = new HashMap();
    ArrayList alResult = new ArrayList();

    String strLockID = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
    String strAction = GmCommonClass.parseNull((String) hmParam.get("ACTION"));
    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_rpt.gm_fch_cycle_count_record_dtls", 4);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLockID);
    gmDBManager.setString(2, strAction);
    gmDBManager.execute();
    hmHeader =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(3)));
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(4)));
    gmDBManager.close();

    log.debug("alResult" + alResult.size());
    hmReturn.put("HEADER", hmHeader);
    hmReturn.put("LOCK_DTLS", alResult);
    return hmReturn;
  }

  /**
   * loadTxnDtls - this method used to fetch the in-house transactions details
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public ArrayList loadTxnDtls(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strLockID = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_rpt.gm_fch_txn_ref_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLockID);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();

    return alResult;
  }
  /**
   * fetchAddPartsDetails - this method used to fetch the part number details
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public ArrayList fetchAddPartsDetails(HashMap hmParam) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    ArrayList alResult = new ArrayList();
	    String strLockID = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
	    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_rpt.gm_fch_add_parts_dtls", 2);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.setString(1, strLockID);
	    gmDBManager.execute();
	    alResult =
	        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
	            .getObject(2)));
	    gmDBManager.close();
	    return alResult;
	  }

  /**
   * fetchCycleCountDtls - get cycle count details 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public String fetchCycleCountDtls(HashMap hmParam) throws AppError {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String strLockID = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
	  String strOutString = "";
	  gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_rpt.gm_fch_cycle_count_dtls_by_lot_qty", 2);
	  gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
	  gmDBManager.setString(1, strLockID);
	  gmDBManager.execute();
	  strOutString = GmCommonClass.parseNull(gmDBManager.getString(2));
	  gmDBManager.close();
	  return strOutString;
  }
}
