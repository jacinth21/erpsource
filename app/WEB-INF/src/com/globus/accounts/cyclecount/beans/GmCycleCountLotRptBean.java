package com.globus.accounts.cyclecount.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCycleCountLotRptBean extends GmBean {

	GmCommonClass gmCommon = new GmCommonClass();

	// Instantiating the Logger
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

	// Initialize
	// the
	// Logger
	// Class.
	public GmCycleCountLotRptBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	}

	/**
	 * Constructor will populate company info. R
	 * 
	 * @param gmDataStore
	 */
	public GmCycleCountLotRptBean(GmDataStoreVO gmDataStore) {
		super(gmDataStore);
	}

	  /**
	   * loadScannedLots - used to fetch the Cycle Count Scanned Lots Report Details
	   * @return
	   * @throws AppError
	   */
	  public String loadScannedLots(HashMap hmParam) throws AppError {
		  
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    
		    // to get the drilldown values
		    String strCycCntLockId = GmCommonClass.parseNull((String) hmParam.get("CYCLECOUNTID")).trim();
		    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PART"));
		    String strLocationId = GmCommonClass.parseNull((String) hmParam.get("LOCATIONID"));
		    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSIGNMENTID"));
		    
		     if (!strCycCntLockId.equals("")
		    	        && (strCycCntLockId.indexOf("CC") == -1 || strCycCntLockId.indexOf("LT") == -1 || strCycCntLockId.indexOf("-") == -1)) {
		    	 strCycCntLockId = "CC-LT-" + strCycCntLockId;
		    	    }
		    String strScanLotRptJSONString = "";
	           
            gmDBManager.setPrepareString("gm_pkg_ac_cc_lot_rpt.gm_fch_lot_details", 5);
            gmDBManager.registerOutParameter(5, OracleTypes.CLOB);
            gmDBManager.setString(1, strCycCntLockId);
            gmDBManager.setString(2, strPartNum);
            gmDBManager.setString(3, strConsignId);
            gmDBManager.setString(4, strLocationId);
            gmDBManager.execute();
           
            strScanLotRptJSONString = GmCommonClass.parseNull(gmDBManager.getString(5));
            gmDBManager.close();
            return strScanLotRptJSONString; 
	   }
}
