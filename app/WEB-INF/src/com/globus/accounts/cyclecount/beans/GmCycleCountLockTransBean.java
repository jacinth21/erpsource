package com.globus.accounts.cyclecount.beans;


import java.util.HashMap;



import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;

/**
 * 
 * GmCycleCountLockTransBean - Used to update/save the cycle count locked information
 * 
 * @author mmuthusamy
 *
 */
/**
 * @author pvigneshwaran
 *
 */
public class GmCycleCountLockTransBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @param gmDataStoreVO
   * @throws AppError
   */
  public GmCycleCountLockTransBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }



  /**
   * saveCycleCountDetail - This method used to save the cycle count locked information.
   * 
   * @param hmParam
   * @throws AppError
   */
  public void saveCycleCountDetail(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLockID = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strRecordCompleteFl = GmCommonClass.parseNull((String) hmParam.get("RECORDCOMPLETEFL"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_trans.gm_sav_record_infor", 4);
    gmDBManager.setString(1, strLockID);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strRecordCompleteFl);
    gmDBManager.setString(4, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * saveRecountdetails - This method used to save the recount details.
   * 
   * @param hmParam
   * @throws AppError
   */
  public void saveRecountdetails(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLockID = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_trans.gm_sav_recount_dtls", 3);
    gmDBManager.setString(1, strLockID);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * saveOverrideQty - This method used to update the Override Qty.
   * 
   * @param hmParam
   * @throws AppError
   */
  public void saveOverrideQty(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLockID = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_trans.gm_sav_override_qty_dtls", 3);
    gmDBManager.setString(1, strLockID);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * saveApprovalQty - This method used to save the Approval Qty and create the ajustment
   * transactions.
   * 
   * @param hmParam
   * @throws AppError
   */
  public void saveApprovalQty(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strLockID = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_trans.gm_sav_approval_qty_dtls", 3);
    gmDBManager.setString(1, strLockID);
    gmDBManager.setString(2, strInputStr);
    gmDBManager.setString(3, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * saveNewParts - This method used to save partnumberdetails
   * 
   * 
   * @param 
   * @throws AppError
   */
  public void saveNewParts(HashMap hmParam) throws AppError {

	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strLockID = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
	    String strPartInputStr = GmCommonClass.parseNull((String) hmParam.get("PARTINPUTSTRING"));
	    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
	    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    gmDBManager.setPrepareString("gm_pkg_ac_cycle_count_trans.gm_add_new_parts", 4);
	    gmDBManager.setString(1, strLockID);
	    gmDBManager.setString(2, strPartInputStr);
	    gmDBManager.setString(3, strInputStr);
	    gmDBManager.setString(4, strUserID);
	    gmDBManager.execute();
	    gmDBManager.commit();
	 	
	 
	  }
  /**
   * updateLotProcessJMS - This method used to update or insert qty based on inhouse and inventory qty
   * 
   * 
   * @param hmParam
   * @throws AppError
   */
  public void updateLotProcessJMS(HashMap hmParam) throws AppError{

	    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
	    String strCycleCountLoTQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_CYCLE_COUNT_LOT_PROCESS_QUEUE"));
	    String strstrCycleCountLotConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("CYCLE_COUNT_LOT_PROCESS_CONSUMER_CLASS"));
	    
	    hmParam.put("QUEUE_NAME",strCycleCountLoTQueueName);
	    hmParam.put("CONSUMERCLASS",strstrCycleCountLotConsumerClass);
	    log.debug("hmParam---> " + hmParam);
	    gmConsumerUtil.sendMessage(hmParam);
  }  
  /**
   * processLotUpdate - This method used to update qty
   * 
   * 
   * @param hmParam
   * @throws AppError
   */
  public void processLotUpdate(HashMap hmParam) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    String strLockId = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
	    log.debug("strLockId-->"+strLockId);
	    gmDBManager.setPrepareString("gm_pkg_ac_cc_lot_txn.gm_upd_lot_track_data", 1);
	    gmDBManager.setString(1, strLockId);
	    
	    gmDBManager.execute();
	    gmDBManager.commit();
  }
  
}
