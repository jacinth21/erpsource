package com.globus.accounts.cyclecount.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * Cycle count Dash board related variable declare and generated the getter and setter method
 * 
 * @author mmuthusamy
 *
 */
public class GmCycleCountDashboardForm extends GmCommonForm {

  private String xmlString = "";
  private String warehouse = "";
  private String cycleCountId = "";
  private String status = "";
  private String fromDate = "";
  private String toDate = "";
  private String statusId = "";
  private ArrayList alWarehoue = new ArrayList();
  private ArrayList alStatus = new ArrayList();


  /**
   * @return the xmlString
   */
  public String getXmlString() {
    return xmlString;
  }

  /**
   * @param xmlString the xmlString to set
   */
  public void setXmlString(String xmlString) {
    this.xmlString = xmlString;
  }

  /**
   * @return the warehouse
   */
  public String getWarehouse() {
    return warehouse;
  }

  /**
   * @param warehouse the warehouse to set
   */
  public void setWarehouse(String warehouse) {
    this.warehouse = warehouse;
  }

  /**
   * @return the cycleCountId
   */
  public String getCycleCountId() {
    return cycleCountId;
  }

  /**
   * @param cycleCountId the cycleCountId to set
   */
  public void setCycleCountId(String cycleCountId) {
    this.cycleCountId = cycleCountId;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the fromDate
   */
  public String getFromDate() {
    return fromDate;
  }

  /**
   * @param fromDate the fromDate to set
   */
  public void setFromDate(String fromDate) {
    this.fromDate = fromDate;
  }

  /**
   * @return the toDate
   */
  public String getToDate() {
    return toDate;
  }

  /**
   * @param toDate the toDate to set
   */
  public void setToDate(String toDate) {
    this.toDate = toDate;
  }

  /**
   * @return the statusId
   */
  public String getStatusId() {
    return statusId;
  }

  /**
   * @param statusId the statusId to set
   */
  public void setStatusId(String statusId) {
    this.statusId = statusId;
  }

  /**
   * @return the alWarehoue
   */
  public ArrayList getAlWarehoue() {
    return alWarehoue;
  }

  /**
   * @param alWarehoue the alWarehoue to set
   */
  public void setAlWarehoue(ArrayList alWarehoue) {
    this.alWarehoue = alWarehoue;
  }

  /**
   * @return the alStatus
   */
  public ArrayList getAlStatus() {
    return alStatus;
  }

  /**
   * @param alStatus the alStatus to set
   */
  public void setAlStatus(ArrayList alStatus) {
    this.alStatus = alStatus;
  }



}
