package com.globus.accounts.cyclecount.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmCycleCountApprovalForm extends GmCommonForm {
  private String xmlString = "";
  private String lockid = "";
  private String cycleCountId = "";
  private String warehouseName = "";
  private String lockDate = "";
  private String scheduleDate = "";
  private String status = "";
  ArrayList alChooseAction = new ArrayList();
  private String totalCount = "";
  private String countedQty = "";
  private String warehouseId = "";
  private String invMapId = "";
  private String inputString = "";
  private String warehouseType = "";
  private String statusId = "";
  private String recordCompleteFl = "";
  private String chooseAction = "";
  private String txnXmlString = "";
  private ArrayList alTxnRef = new ArrayList();
  private String countType = "";
  private String buttonAccessFl = "";
  private String transLogDate = "";
  private String approvedBy="";
  private String approvedDate="";

  /**
   * @return the xmlString
   */
  public String getXmlString() {
    return xmlString;
  }

  /**
   * @param xmlString the xmlString to set
   */
  public void setXmlString(String xmlString) {
    this.xmlString = xmlString;
  }

  /**
   * @return the lockid
   */
  public String getLockid() {
    return lockid;
  }

  /**
   * @param lockid the lockid to set
   */
  public void setLockid(String lockid) {
    this.lockid = lockid;
  }

  /**
   * @return the cycleCountId
   */
  public String getCycleCountId() {
    return cycleCountId;
  }

  /**
   * @param cycleCountId the cycleCountId to set
   */
  public void setCycleCountId(String cycleCountId) {
    this.cycleCountId = cycleCountId;
  }

  /**
   * @return the warehouseName
   */
  public String getWarehouseName() {
    return warehouseName;
  }

  /**
   * @param warehouseName the warehouseName to set
   */
  public void setWarehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
  }

  /**
   * @return the lockDate
   */
  public String getLockDate() {
    return lockDate;
  }

  /**
   * @param lockDate the lockDate to set
   */
  public void setLockDate(String lockDate) {
    this.lockDate = lockDate;
  }

  /**
   * @return the scheduleDate
   */
  public String getScheduleDate() {
    return scheduleDate;
  }

  /**
   * @param scheduleDate the scheduleDate to set
   */
  public void setScheduleDate(String scheduleDate) {
    this.scheduleDate = scheduleDate;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }


  /**
   * @return the totalCount
   */
  public String getTotalCount() {
    return totalCount;
  }

  /**
   * @param totalCount the totalCount to set
   */
  public void setTotalCount(String totalCount) {
    this.totalCount = totalCount;
  }

  /**
   * @return the countedQty
   */
  public String getCountedQty() {
    return countedQty;
  }

  /**
   * @param countedQty the countedQty to set
   */
  public void setCountedQty(String countedQty) {
    this.countedQty = countedQty;
  }

  /**
   * @return the warehouseId
   */
  public String getWarehouseId() {
    return warehouseId;
  }

  /**
   * @param warehouseId the warehouseId to set
   */
  public void setWarehouseId(String warehouseId) {
    this.warehouseId = warehouseId;
  }

  /**
   * @return the invMapId
   */
  public String getInvMapId() {
    return invMapId;
  }

  /**
   * @param invMapId the invMapId to set
   */
  public void setInvMapId(String invMapId) {
    this.invMapId = invMapId;
  }

  /**
   * @return the inputString
   */
  public String getInputString() {
    return inputString;
  }

  /**
   * @param inputString the inputString to set
   */
  public void setInputString(String inputString) {
    this.inputString = inputString;
  }

  /**
   * @return the warehouseType
   */
  public String getWarehouseType() {
    return warehouseType;
  }

  /**
   * @param warehouseType the warehouseType to set
   */
  public void setWarehouseType(String warehouseType) {
    this.warehouseType = warehouseType;
  }

  /**
   * @return the statusId
   */
  public String getStatusId() {
    return statusId;
  }

  /**
   * @param statusId the statusId to set
   */
  public void setStatusId(String statusId) {
    this.statusId = statusId;
  }

  /**
   * @return the recordCompleteFl
   */
  public String getRecordCompleteFl() {
    return recordCompleteFl;
  }

  /**
 * @return the approvedBy
 */
public String getApprovedBy() {
	return approvedBy;
}

/**
 * @param approvedBy the approvedBy to set
 */
public void setApprovedBy(String approvedBy) {
	this.approvedBy = approvedBy;
}

/**
 * @return the approvedDate
 */
public String getApprovedDate() {
	return approvedDate;
}

/**
 * @param approvedDate the approvedDate to set
 */
public void setApprovedDate(String approvedDate) {
	this.approvedDate = approvedDate;
}

/**
   * @param recordCompleteFl the recordCompleteFl to set
   */
  public void setRecordCompleteFl(String recordCompleteFl) {
    this.recordCompleteFl = recordCompleteFl;
  }

  /**
   * @return the alChooseAction
   */
  public ArrayList getAlChooseAction() {
    return alChooseAction;
  }

  /**
   * @param alChooseAction the alChooseAction to set
   */
  public void setAlChooseAction(ArrayList alChooseAction) {
    this.alChooseAction = alChooseAction;
  }

  /**
   * @return the chooseAction
   */
  public String getChooseAction() {
    return chooseAction;
  }

  /**
   * @param chooseAction the chooseAction to set
   */
  public void setChooseAction(String chooseAction) {
    this.chooseAction = chooseAction;
  }

  /**
   * @return the txnXmlString
   */
  public String getTxnXmlString() {
    return txnXmlString;
  }

  /**
   * @param txnXmlString the txnXmlString to set
   */
  public void setTxnXmlString(String txnXmlString) {
    this.txnXmlString = txnXmlString;
  }

  /**
   * @return the alTxnRef
   */
  public ArrayList getAlTxnRef() {
    return alTxnRef;
  }

  /**
   * @param alTxnRef the alTxnRef to set
   */
  public void setAlTxnRef(ArrayList alTxnRef) {
    this.alTxnRef = alTxnRef;
  }

  /**
   * @return the countType
   */
  public String getCountType() {
    return countType;
  }

  /**
   * @param countType the countType to set
   */
  public void setCountType(String countType) {
    this.countType = countType;
  }

  /**
   * @return the buttonAccessFl
   */
  public String getButtonAccessFl() {
    return buttonAccessFl;
  }

  /**
   * @param buttonAccessFl the buttonAccessFl to set
   */
  public void setButtonAccessFl(String buttonAccessFl) {
    this.buttonAccessFl = buttonAccessFl;
  }

  /**
   * @return the transLogDate
   */
  public String getTransLogDate() {
    return transLogDate;
  }

  /**
   * @param transLogDate the transLogDate to set
   */
  public void setTransLogDate(String transLogDate) {
    this.transLogDate = transLogDate;
  }



}
