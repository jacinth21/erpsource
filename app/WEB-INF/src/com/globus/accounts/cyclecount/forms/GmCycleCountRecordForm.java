package com.globus.accounts.cyclecount.forms;

import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

/**
 * This class used to set/get the record the cycle count details
 * 
 * @author pvigneshwaran
 *
 */
public class GmCycleCountRecordForm extends GmCommonForm {

  private String xmlString = "";
  private String lockid = "";
  private String cycleCountId = "";
  private String warehouseName = "";
  private String lockDate = "";
  private String scheduleDate = "";
  private String status = "";
  private String inputString = "";
  private String totalCount = "";
  private String countedQty = "";
  private String warehouseType = "";
  private String statusId = "";
  private String recordCompleteFl = "";
  private String countType = "";
  private String buttonAccessFl = "";
  //PMT-23643
  private String xmlPartData="";
  private String partInputString="";
  private String lockType="";
  //Add new Parts button only show  FG Bulk and RW bulk face.
  private String strRuleValue="";
  // PC-3495 Cycle count - Adding lots track Fl in print paperwork 
  private String strLotTrackFl="";
  private HashMap hmData = new HashMap();

  /**
   * @return the xmlString
   */
  public String getXmlString() {
    return xmlString;
  }

  /**
   * @param xmlString the xmlString to set
   */
  public void setXmlString(String xmlString) {
    this.xmlString = xmlString;
  }

  /**
   * @return the lockid
   */
  public String getLockid() {
    return lockid;
  }

  /**
   * @param lockid the lockid to set
   */
  public void setLockid(String lockid) {
    this.lockid = lockid;
  }

  /**
   * @return the cycleCountId
   */
  public String getCycleCountId() {
    return cycleCountId;
  }

  /**
   * @param cycleCountId the cycleCountId to set
   */
  public void setCycleCountId(String cycleCountId) {
    this.cycleCountId = cycleCountId;
  }

  /**
   * @return the warehouseName
   */
  public String getWarehouseName() {
    return warehouseName;
  }

  /**
   * @param warehouseName the warehouseName to set
   */
  public void setWarehouseName(String warehouseName) {
    this.warehouseName = warehouseName;
  }

  /**
   * @return the lockDate
   */
  public String getLockDate() {
    return lockDate;
  }

  /**
   * @param lockDate the lockDate to set
   */
  public void setLockDate(String lockDate) {
    this.lockDate = lockDate;
  }

  /**
   * @return the scheduleDate
   */
  public String getScheduleDate() {
    return scheduleDate;
  }

  /**
   * @param scheduleDate the scheduleDate to set
   */
  public void setScheduleDate(String scheduleDate) {
    this.scheduleDate = scheduleDate;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the inputString
   */
  public String getInputString() {
    return inputString;
  }

  /**
   * @param inputString the inputString to set
   */
  public void setInputString(String inputString) {
    this.inputString = inputString;
  }

  /**
   * @return the totalCount
   */
  public String getTotalCount() {
    return totalCount;
  }

  /**
   * @param totalCount the totalCount to set
   */
  public void setTotalCount(String totalCount) {
    this.totalCount = totalCount;
  }

  /**
   * @return the countedQty
   */
  public String getCountedQty() {
    return countedQty;
  }

  /**
   * @param countedQty the countedQty to set
   */
  public void setCountedQty(String countedQty) {
    this.countedQty = countedQty;
  }

  /**
   * @return the warehouseType
   */
  public String getWarehouseType() {
    return warehouseType;
  }

  /**
   * @param warehouseType the warehouseType to set
   */
  public void setWarehouseType(String warehouseType) {
    this.warehouseType = warehouseType;
  }

  /**
   * @return the statusId
   */
  public String getStatusId() {
    return statusId;
  }

  /**
   * @param statusId the statusId to set
   */
  public void setStatusId(String statusId) {
    this.statusId = statusId;
  }

  /**
   * @return the hmData
   */
  public HashMap getHmData() {
    return hmData;
  }

  /**
   * @param hmData the hmData to set
   */
  public void setHmData(HashMap hmData) {
    this.hmData = hmData;
  }

  /**
   * @return the recordCompleteFl
   */
  public String getRecordCompleteFl() {
    return recordCompleteFl;
  }

  /**
   * @param recordCompleteFl the recordCompleteFl to set
   */
  public void setRecordCompleteFl(String recordCompleteFl) {
    this.recordCompleteFl = recordCompleteFl;
  }
/**
 * @return the strRuleValue
 */
public String getStrRuleValue() {
	return strRuleValue;
}

/**
 * @param strRuleValue the strRuleValue to set
 */
public void setStrRuleValue(String strRuleValue) {
	this.strRuleValue = strRuleValue;
}

/**
 * @return the partInputString
 */
public String getPartInputString() {
	return partInputString;
}

/**
 * @param partInputString the partInputString to set
 */
public void setPartInputString(String partInputString) {
	this.partInputString = partInputString;
}

/**
 * @return the xmlPartData
 */
public String getXmlPartData() {
	return xmlPartData;
}

/**
 * @param xmlPartData the xmlPartData to set
 */
public void setXmlPartData(String xmlPartData) {
	this.xmlPartData = xmlPartData;
}

/**
   * @return the countType
   */
  public String getCountType() {
    return countType;
  }

  /**
   * @param countType the countType to set
   */
  public void setCountType(String countType) {
    this.countType = countType;
  }

  /**
   * @return the buttonAccessFl
   */
  public String getButtonAccessFl() {
    return buttonAccessFl;
  }

  /**
   * @param buttonAccessFl the buttonAccessFl to set
   */
  public void setButtonAccessFl(String buttonAccessFl) {
    this.buttonAccessFl = buttonAccessFl;
  }

/**
 * @return the lockType
 */
public String getLockType() {
	return lockType;
}

/**
 * @param lockType the lockType to set
 */
public void setLockType(String lockType) {
	this.lockType = lockType;
}

/**
 * @return the strLotTrackFl
 */
public String getStrLotTrackFl() {
	return strLotTrackFl;
}

/**
 * @param strLotTrackFl the strLotTrackFl to set
 */
public void setStrLotTrackFl(String strLotTrackFl) {
	this.strLotTrackFl = strLotTrackFl;
}


}
