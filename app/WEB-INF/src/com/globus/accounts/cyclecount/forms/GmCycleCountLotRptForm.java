package com.globus.accounts.cyclecount.forms;

import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

/**
 * Cycle count Dash board related variable declare and generated the getter and setter method
 * 
 * @author MKosalram
 *
 */

public class GmCycleCountLotRptForm extends GmCommonForm {
	

	private String cycleCountId = "";
	private String part = "";
	private String locationId ="";
	private String consignmentId ="";
	private String pnumSuffix = "";
	private String xmlString = "";
	private String strPopupFlag = "";
	
	private String hcycleCountId = "";
	private String hpart = "";
	private String hlocationId ="";
	private String hconsignmentId ="";
	private String hstrPopupFlag = "";

	private HashMap hmData = new HashMap();
	private HashMap hmRegStr = new HashMap();
	
	
	/**
	 * @return the hmData
	 */
	public HashMap getHmData() {
		return hmData;
	}

	/**
	 * @return the cycleCountId
	 */
	public String getCycleCountId() {
		return cycleCountId;
	}

	/**
	 * @param cycleCountId the cycleCountId to set
	 */
	public void setCycleCountId(String cycleCountId) {
		this.cycleCountId = cycleCountId;
	}

	/**
	 * @return the partNumber
	 */
	public String getPart() {
		return part;
	}

	/**
	 * @param partNumber the partNumber to set
	 */
	public void setPart(String part) {
		this.part = part;
	}

	/**
	 * @return the locationId
	 */
	public String getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId the locationId to set
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the consignmentId
	 */
	public String getConsignmentId() {
		return consignmentId;
	}

	/**
	 * @param consignmentId the consignmentId to set
	 */
	public void setConsignmentId(String consignmentId) {
		this.consignmentId = consignmentId;
	}
	

	/**
	 * @return the pnumSuffix
	 */
	public String getPnumSuffix() {
		return pnumSuffix;
	}

	/**
	 * @param pnumSuffix the pnumSuffix to set
	 */
	public void setPnumSuffix(String pnumSuffix) {
		this.pnumSuffix = pnumSuffix;
	}
	
	

	/**
	 * @return the strPopupFlag
	 */
	public String getStrPopupFlag() {
		return strPopupFlag;
	}

	/**
	 * @param strPopupFlag the strPopupFlag to set
	 */
	public void setStrPopupFlag(String strPopupFlag) {
		this.strPopupFlag = strPopupFlag;
	}

	/**
	 * @return the xmlString
	 */
	public String getXmlString() {
		return xmlString;
	}

	/**
	 * @param xmlString the xmlString to set
	 */
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}

	/**
	 * @param hmData the hmData to set
	 */
	public void setHmData(HashMap hmData) {
		this.hmData = hmData;
	}

	/**
	 * @return the hmRegStr
	 */
	public HashMap getHmRegStr() {
		return hmRegStr;
	}

	/**
	 * @param hmRegStr the hmRegStr to set
	 */
	public void setHmRegStr(HashMap hmRegStr) {
		this.hmRegStr = hmRegStr;
	}

	/**
	 * @return the hcycleCountId
	 */
	public String getHcycleCountId() {
		return hcycleCountId;
	}

	/**
	 * @param hcycleCountId the hcycleCountId to set
	 */
	public void setHcycleCountId(String hcycleCountId) {
		this.hcycleCountId = hcycleCountId;
	}

	/**
	 * @return the hpart
	 */
	public String getHpart() {
		return hpart;
	}

	/**
	 * @param hpart the hpart to set
	 */
	public void setHpart(String hpart) {
		this.hpart = hpart;
	}

	/**
	 * @return the hlocationId
	 */
	public String getHlocationId() {
		return hlocationId;
	}

	/**
	 * @param hlocationId the hlocationId to set
	 */
	public void setHlocationId(String hlocationId) {
		this.hlocationId = hlocationId;
	}

	/**
	 * @return the hconsignmentId
	 */
	public String getHconsignmentId() {
		return hconsignmentId;
	}

	/**
	 * @param hconsignmentId the hconsignmentId to set
	 */
	public void setHconsignmentId(String hconsignmentId) {
		this.hconsignmentId = hconsignmentId;
	}

	/**
	 * @return the hstrPopupFlag
	 */
	public String getHstrPopupFlag() {
		return hstrPopupFlag;
	}

	/**
	 * @param hstrPopupFlag the hstrPopupFlag to set
	 */
	public void setHstrPopupFlag(String hstrPopupFlag) {
		this.hstrPopupFlag = hstrPopupFlag;
	}
	
	
}
