package com.globus.accounts.cyclecount.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmCommonForm;

/**
 * @author pvigneshwaran
 * 
 */
public class GmCycleCountReportForm extends GmCommonForm {

  private String xmlString = "";
  private String warehouse = "";
  private String cycleCountId = "";
  private String part = "";
  private String fromDate = "";
  private String toDate = "";
  private String pnumSuffix = "";
  private String partNo = "";
  private String partDescr = "";
  private String rank = "";
  private String cycleCountYear = "";
  private String varianceBy = "";
  //
  private String projectId = "";
  private String projectName = "";
  private String productFamily = "";

  private HashMap hmCrossTabDtls = new HashMap();
  private ArrayList alWarehouse = new ArrayList();
  private ArrayList alVarianceBy = new ArrayList();
  private ArrayList alRank = new ArrayList();
  private ArrayList alYearDropDown = new ArrayList();
  private ArrayList alProductFamily = new ArrayList();

  /**
   * @return the varianceBy
   */
  public String getVarianceBy() {
    return varianceBy;
  }

  /**
   * @param varianceBy the varianceBy to set
   */
  public void setVarianceBy(String varianceBy) {
    this.varianceBy = varianceBy;
  }

  /**
   * @return the alVarianceBy
   */
  public ArrayList getAlVarianceBy() {
    return alVarianceBy;
  }

  /**
   * @param alVarianceBy the alVarianceBy to set
   */
  public void setAlVarianceBy(ArrayList alVarianceBy) {
    this.alVarianceBy = alVarianceBy;
  }

  /**
   * @return the xmlString
   */
  public String getXmlString() {
    return xmlString;
  }

  /**
   * @param xmlString the xmlString to set
   */
  public void setXmlString(String xmlString) {
    this.xmlString = xmlString;
  }

  /**
   * @return the warehouse
   */
  public String getWarehouse() {
    return warehouse;
  }

  /**
   * @param warehouse the warehouse to set
   */
  public void setWarehouse(String warehouse) {
    this.warehouse = warehouse;
  }

  /**
   * @return the alWarehouse
   */
  public ArrayList getAlWarehouse() {
    return alWarehouse;
  }

  /**
   * @param alWarehouse the alWarehouse to set
   */
  public void setAlWarehouse(ArrayList alWarehouse) {
    this.alWarehouse = alWarehouse;
  }

  /**
   * @return the cycleCountId
   */
  public String getCycleCountId() {
    return cycleCountId;
  }

  /**
   * @param cycleCountId the cycleCountId to set
   */
  public void setCycleCountId(String cycleCountId) {
    this.cycleCountId = cycleCountId;
  }

  /**
   * @return the part
   */
  public String getPart() {
    return part;
  }

  /**
   * @param part the part to set
   */
  public void setPart(String part) {
    this.part = part;
  }

  /**
   * @return the fromDate
   */
  public String getFromDate() {
    return fromDate;
  }

  /**
   * @param fromDate the fromDate to set
   */
  public void setFromDate(String fromDate) {
    this.fromDate = fromDate;
  }

  /**
   * @return the toDate
   */
  public String getToDate() {
    return toDate;
  }

  /**
   * @param toDate the toDate to set
   */
  public void setToDate(String toDate) {
    this.toDate = toDate;
  }

  /**
   * @return the pnumSuffix
   */
  public String getPnumSuffix() {
    return pnumSuffix;
  }

  /**
   * @param pnumSuffix the pnumSuffix to set
   */
  public void setPnumSuffix(String pnumSuffix) {
    this.pnumSuffix = pnumSuffix;
  }

  /**
   * @return the partNo
   */
  public String getPartNo() {
    return partNo;
  }

  /**
   * @param partNo the partNo to set
   */
  public void setPartNo(String partNo) {
    this.partNo = partNo;
  }

  /**
   * @return the partNo
   */
  public String getPartDescr() {
    return partDescr;
  }

  /**
   * @param partDescr the partDescr to set
   */
  public void setPartDescr(String partDescr) {
    this.partDescr = partDescr;
  }

  /**
   * @return the rank
   */
  public String getRank() {
    return rank;
  }

  /**
   * @param rank the rank to set
   */
  public void setRank(String rank) {
    this.rank = rank;
  }

  /**
   * @return the alRank
   */
  public ArrayList getAlRank() {
    return alRank;
  }

  /**
   * @param alRank the alRank to set
   */
  public void setAlRank(ArrayList alRank) {
    this.alRank = alRank;
  }

  /**
   * @return the hmCrossTabDtls
   */
  public HashMap getHmCrossTabDtls() {
    return hmCrossTabDtls;
  }

  /**
   * @param hmCrossTabDtls the hmCrossTabDtls to set
   */
  public void setHmCrossTabDtls(HashMap hmCrossTabDtls) {
    this.hmCrossTabDtls = hmCrossTabDtls;
  }

  /**
   * @return
   */
  public String getCycleCountYear() {
    return cycleCountYear;
  }

  /**
   * @param cycleCountYear
   */
  public void setCycleCountYear(String cycleCountYear) {
    this.cycleCountYear = cycleCountYear;
  }

  /**
   * @return the alYearDropDown
   */
  public ArrayList getAlYearDropDown() {
    return alYearDropDown;
  }


  /**
   * @param alYearDropDown the alYearDropDown to set
   */
  public void setAlYearDropDown(ArrayList alYearDropDown) {
    this.alYearDropDown = alYearDropDown;
  }

  /**
   * @return the projectId
   */
  public String getProjectId() {
    return projectId;
  }

  /**
   * @param projectId the projectId to set
   */
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  /**
   * @return the projectName
   */
  public String getProjectName() {
    return projectName;
  }

  /**
   * @param projectName the projectName to set
   */
  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  /**
   * @return the productFamily
   */
  public String getProductFamily() {
    return productFamily;
  }

  /**
   * @param productFamily the productFamily to set
   */
  public void setProductFamily(String productFamily) {
    this.productFamily = productFamily;
  }

  /**
   * @return the alProductFamily
   */
  public ArrayList getAlProductFamily() {
    return alProductFamily;
  }

  /**
   * @param alProductFamily the alProductFamily to set
   */
  public void setAlProductFamily(ArrayList alProductFamily) {
    this.alProductFamily = alProductFamily;
  }



}
