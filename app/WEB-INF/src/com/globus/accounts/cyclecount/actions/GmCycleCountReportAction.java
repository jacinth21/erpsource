package com.globus.accounts.cyclecount.actions;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.cyclecount.beans.GmCycleCountLockReportBean;
import com.globus.accounts.cyclecount.beans.GmCycleCountReportBean;
import com.globus.accounts.cyclecount.forms.GmCycleCountReportForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author pvigneshwaran This action used to loadfulldataReport for cyclecount
 */
public class GmCycleCountReportAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to

  // Initialize
  // the
  // Logger

  public ActionForward loadFullDataRpt(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException, ParseException {
    // call the instantiate method
    instantiate(request, response);
    GmCycleCountReportForm gmCycleCountReportForm = (GmCycleCountReportForm) form;
    gmCycleCountReportForm.loadSessionParameters(request);
    GmCycleCountLockReportBean gmCycleCountLockReportBean =
        new GmCycleCountLockReportBean(getGmDataStoreVO());
    GmCycleCountReportBean gmCycleCountReportBean = new GmCycleCountReportBean(getGmDataStoreVO());
    // GmCalenderOperations gmCalenderOperations = new
    // GmCalenderOperations();
    HashMap hmParam = new HashMap();
    ArrayList alWareHouse = new ArrayList();
    ArrayList alResult = null;
    HashMap hmApplnParam = null;

    String strApplDateFmt = "";
    String strTodayDt = "";
    String strOpt = "";
    String strSessCompanyLocale = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountReportForm);
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    alWareHouse = GmCommonClass.parseNullArrayList(gmCycleCountLockReportBean.fetchWarehoueType());
    // to set the warehouse drop down values
    gmCycleCountReportForm.setAlWarehouse(alWareHouse);
    // first time load we setting the date
    if (strOpt.equals("")) {

      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      String todayDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);
      int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      String firstDayofMonth = GmCalenderOperations.getAnyDateOfMonth(currMonth, 1, strApplDateFmt);
      // to set to form
      gmCycleCountReportForm.setFromDate(firstDayofMonth);
      gmCycleCountReportForm.setToDate(todayDate);

    }

    if (strOpt.equals("Load")) {
      /* PartNumber Filter */
      HashMap hmRegStr = new HashMap();
      hmRegStr.put("PARTNUM", gmCycleCountReportForm.getPart());
      hmRegStr.put("SEARCH", gmCycleCountReportForm.getPnumSuffix());
      String strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
      log.debug("strPartNumFormat " + strPartNumFormat);
      hmParam.put("PART", strPartNumFormat);
      alResult = new ArrayList();
      alResult = gmCycleCountReportBean.loadFullDataReport(hmParam);
      //
      strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      hmApplnParam = new HashMap();
      hmApplnParam.put("APPLNCURRFMT", GmCommonClass.getRuleValue("CURRFMT", "CURRFMT"));
      hmApplnParam.put("APPLNCURRSIGN", GmCommonClass.getRuleValue("CURRSYMBOL", "CURRSYMBOL"));
      hmApplnParam.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
      hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmApplnParam.put("TEMPLATE_NM", "GmCycleCountFullDataRpt.vm");
      hmApplnParam.put("LABEL_PROPERTIES_NM",
          "properties.labels.accounts.cyclecount.GmCycleCountFullDataRpt");
      // based on array list to get the Grid xml string
      String xmlData = gmCycleCountLockReportBean.generateOutput(alResult, hmApplnParam);
      gmCycleCountReportForm.setXmlString(xmlData);

    }

    return actionMapping.findForward("GmCycleCountFullDataRpt");
  }


  /**
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws ServletException
   * @throws IOException
   * @throws ParseException
   */
  public ActionForward loadCountedRpt(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException, ParseException {

    instantiate(request, response);

    GmCycleCountReportForm gmCycleCountReportForm = (GmCycleCountReportForm) form;
    gmCycleCountReportForm.loadSessionParameters(request);
    // create object
    GmCycleCountReportBean gmCycleCountReportBean = new GmCycleCountReportBean(getGmDataStoreVO());
    GmCycleCountLockReportBean gmCycleCountLockReportBean =
        new GmCycleCountLockReportBean(getGmDataStoreVO());
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();

    HashMap hmParam = new HashMap();
    ArrayList alWareHouse = new ArrayList();
    ArrayList alRank = new ArrayList();
    HashMap hmApplnParam = null;
    HashMap hmCrossTabDtls = null;
    ArrayList alMonthDropDown = new ArrayList();
    ArrayList alYearDropDown = new ArrayList();
    HashMap hmReturn = new HashMap();
    String strOpt = "";
    String strForwardPage = "";
    String strApplDateFmt = "";
    String strHiddenFromDate = "";
    String strHiddenToDate = "";
    String strFromDate = "";
    String strToDate = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountReportForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    alWareHouse = GmCommonClass.parseNullArrayList(gmCycleCountLockReportBean.fetchWarehoueType());
    alRank = GmCommonClass.parseNullArrayList(gmCycleCountReportBean.fetchRankList());
    gmCycleCountReportForm.setAlWarehouse(alWareHouse);
    gmCycleCountReportForm.setAlRank(alRank);
    // This is to set the From,toDate during onLoad Condition.
    Calendar cal = Calendar.getInstance();
    String strApplnDateFormat = getGmDataStoreVO().getCmpdfmt();
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM'/'yy");
    SimpleDateFormat formatter = new SimpleDateFormat(strApplnDateFormat);
    // get year of first month
    cal.set(Calendar.MONTH, Calendar.JANUARY);
    strFromDate = dateFormat.format(cal.getTime());

    if (gmCycleCountReportForm.getFromDate() == "") {
      gmCycleCountReportForm.setFromDate(strFromDate);
    }
    strHiddenFromDate = formatter.format(dateFormat.parse(gmCycleCountReportForm.getFromDate()));
    // get current month
    Calendar currMonth = Calendar.getInstance();
    strToDate = dateFormat.format(currMonth.getTime());

    if (gmCycleCountReportForm.getToDate() == "") {
      gmCycleCountReportForm.setToDate(strToDate);
    }
    strHiddenToDate = formatter.format(dateFormat.parse(gmCycleCountReportForm.getToDate()));
    // Date Format to be used in the calendar .
    request.setAttribute("hidden_FromDate", strHiddenFromDate);
    request.setAttribute("hidden_ToDate", strHiddenToDate);

    if (strOpt.equals("Load")) {
      HashMap hmRegStr = new HashMap();
      hmRegStr.put("PARTNUM", gmCycleCountReportForm.getPartNo());
      hmRegStr.put("SEARCH", gmCycleCountReportForm.getPnumSuffix());
      String strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
      hmParam.put("PARTNUM", strPartNumFormat);
      // Cycle Count - Grid Cross tab
      hmCrossTabDtls =
          GmCommonClass.parseNullHashMap(gmCycleCountReportBean.loadCountedQtyReport(hmParam));
      gmCycleCountReportForm.setHmCrossTabDtls(hmCrossTabDtls);

    }

    strForwardPage = "GmCylceCountCountedQtyRpt";

    return actionMapping.findForward(strForwardPage);

  }

  /**
   * This Method Used to Load Accuracy Report
   * 
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward loadAccuracyRpt(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    // call the instantiate method
    instantiate(request, response);
    GmCycleCountReportForm gmCycleCountReportForm = (GmCycleCountReportForm) form;
    gmCycleCountReportForm.loadSessionParameters(request);
    GmCycleCountLockReportBean gmCycleCountLockReportBean =
        new GmCycleCountLockReportBean(getGmDataStoreVO());
    GmCycleCountReportBean gmmCycleCountReportBean = new GmCycleCountReportBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    String strOpt = "";
    String strForwardPage = "";
    ArrayList alWareHouse = new ArrayList();
    ArrayList alResult = null;
    HashMap hmCrossTabDtls = null;
    String strApplDateFmt = "";
    String strTodayDt = "";
    String strSessCompanyLocale = "";

    hmParam =
        GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmCycleCountReportForm));
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    alWareHouse = GmCommonClass.parseNullArrayList(gmCycleCountLockReportBean.fetchWarehoueType());
    // to set the warehouse drop down values
    gmCycleCountReportForm.setAlWarehouse(alWareHouse);
    // to Set variancedropdown values
    gmCycleCountReportForm.setAlVarianceBy(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("CYCVAR")));

    // This is to set the From,toDate during onLoad Condition.
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM'/'yy");
    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    String hFromDate = "";
    String hToDate = "";
    String strToDate = "";

    // get year of first month
    cal.set(Calendar.MONTH, Calendar.JANUARY);
    String strFromDate = dateFormat.format(cal.getTime());

    if (gmCycleCountReportForm.getFromDate() == "") {
      gmCycleCountReportForm.setFromDate(strFromDate);
    }
    hFromDate = formatter.format(dateFormat.parse(gmCycleCountReportForm.getFromDate()));

    // get current month
    Calendar currMonth = Calendar.getInstance();
    strToDate = dateFormat.format(currMonth.getTime());

    if (gmCycleCountReportForm.getToDate() == "") {
      gmCycleCountReportForm.setToDate(strToDate);
    }
    hToDate = formatter.format(dateFormat.parse(gmCycleCountReportForm.getToDate()));
    log.debug(" hFromDate is : " + hFromDate + "\n" + " hToDate :" + hToDate);

    if (strOpt.equals("Load")) {
      hmCrossTabDtls =
          GmCommonClass.parseNullHashMap(gmmCycleCountReportBean.loadAccuracyReport(hmParam));
      log.debug("hmCrossTabDtls" + hmCrossTabDtls);
      gmCycleCountReportForm.setHmCrossTabDtls(hmCrossTabDtls);

    }
    return actionMapping.findForward("GmCycleCountAccuracyRpt");
  }



  /**
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws ServletException
   * @throws IOException
   * @throws ParseException
   */
  public ActionForward loadRankRpt(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException, ParseException {

    instantiate(request, response);

    GmCycleCountReportForm gmCycleCountReportForm = (GmCycleCountReportForm) form;
    gmCycleCountReportForm.loadSessionParameters(request);
    // create object
    GmCycleCountReportBean gmCycleCountReportBean = new GmCycleCountReportBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
    String strForwardPage = "";
    String strSessCompanyLocale = "";
    String strToYear = "";
    String strPartNumFormat = "";
    String strOpt = "";
    String strCycleCountID = "";
    int intCyclecountYear = 0;

    ArrayList alRank = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alMonthDropDown = new ArrayList();
    ArrayList alYearDropDown = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmApplnParam = null;
    ArrayList alCountYear = new ArrayList();
    ArrayList alProductFamily = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountReportForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    // to get current year
    strToYear = GmCommonClass.parseNull(String.valueOf(GmCalenderOperations.getCurrentYear()));
    alRank = GmCommonClass.parseNullArrayList(gmCycleCountReportBean.fetchRankList());
    alCountYear = GmCommonClass.parseNullArrayList(gmCycleCountReportBean.fetchCyclyCountYear());
    alProductFamily = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PFLY"));

    gmCycleCountReportForm.setAlRank(alRank);
    gmCycleCountReportForm.setAlProductFamily(alProductFamily);
    gmCycleCountReportForm.setAlYearDropDown(alCountYear);

    intCyclecountYear = alCountYear.size();
    strCycleCountID = GmCommonClass.parseZero((String) hmParam.get("CYCLECOUNTYEAR"));
    if (strCycleCountID.equals("0")) {
      // to set year - default values (current year)
      for (int i = 0; i < intCyclecountYear; i++) {
        HashMap hmTmp = new HashMap();
        hmTmp = (HashMap) alCountYear.get(i);
        if (hmTmp.get("YEAR").equals(strToYear)) {
          strCycleCountID = GmCommonClass.parseNull((String) hmTmp.get("ID"));
          gmCycleCountReportForm.setCycleCountYear(strCycleCountID);
        }
      }
    }

    // Load Function
    if (strOpt.equals("Load")) {
      HashMap hmRegStr = new HashMap();
      hmRegStr.put("PARTNUM", gmCycleCountReportForm.getPartNo());
      hmRegStr.put("SEARCH", gmCycleCountReportForm.getPnumSuffix());
      strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
      hmParam.put("PARTNUM", strPartNumFormat);
      alResult = gmCycleCountReportBean.loadRankReport(hmParam);
      strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      hmApplnParam = new HashMap();
      hmApplnParam.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
      hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmApplnParam.put("TEMPLATE_NM", "GmCycleCountRankRpt.vm");
      hmApplnParam.put("LABEL_PROPERTIES_NM",
          "properties.labels.accounts.cyclecount.GmCycleCountRankRpt");
      // based on array list to get the Grid xml string
      String xmlData = gmCycleCountReportBean.generateOutput(alResult, hmApplnParam);

      gmCycleCountReportForm.setXmlString(xmlData);
    }
    strForwardPage = "GmCycleCountRankRpt";

    return actionMapping.findForward(strForwardPage);

  }
}
