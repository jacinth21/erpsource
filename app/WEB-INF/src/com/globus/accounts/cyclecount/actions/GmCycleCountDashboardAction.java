package com.globus.accounts.cyclecount.actions;


import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.cyclecount.beans.GmCycleCountLockReportBean;
import com.globus.accounts.cyclecount.forms.GmCycleCountDashboardForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


/**
 * GmCycleCountDashboardAction Class used to show the Locked transactions on the screen
 * 
 * @author mmuthusamy
 *
 */
public class GmCycleCountDashboardAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger


  /**
   *
   * loadDashboard - Method used to load the Locked transactions details based on filter condition.
   * Default set the date filed as current date
   * 
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws IOException
   * @throws ServletException
   * @throws ParseException
   */

  public ActionForward loadDashboard(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException, ParseException {

    // to call the instantiate method
    instantiate(request, response);
    GmCycleCountDashboardForm gmCycleCountDashboardForm = (GmCycleCountDashboardForm) form;
    gmCycleCountDashboardForm.loadSessionParameters(request);
    // create object
    GmCycleCountLockReportBean gmCycleCountLockReportBean =
        new GmCycleCountLockReportBean(getGmDataStoreVO());
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
    // declare variable
    HashMap hmParam = new HashMap();
    ArrayList alWareHouse = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alResult = null;
    HashMap hmApplnParam = null;

    String strForwardPage = "";
    String strApplDateFmt = "";
    String strTodayDt = "";
    String strOpt = "";
    String strSessCompanyLocale = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountDashboardForm);

    strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    alWareHouse = GmCommonClass.parseNullArrayList(gmCycleCountLockReportBean.fetchWarehoueType());
    alStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CYCLST")); // status

    // first time load we setting the date
    if (strOpt.equals("")) {
      strTodayDt = GmCommonClass.parseNull(gmCalenderOperations.getCurrentDate(strApplDateFmt));
      // to set to form
      gmCycleCountDashboardForm.setFromDate(strTodayDt);
      gmCycleCountDashboardForm.setToDate(strTodayDt);
    }

    if (strOpt.equals("Load")) {
      alResult = new ArrayList();
      alResult = gmCycleCountLockReportBean.fetchDashBoardDtls(hmParam);
      //
      strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      hmApplnParam = new HashMap();
      hmApplnParam.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
      hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmApplnParam.put("TEMPLATE_NM", "GmCycleCountDashboard.vm");
      hmApplnParam.put("LABEL_PROPERTIES_NM",
          "properties.labels.accounts.cyclecount.GmCycleCountDashBoard");
      // based on array list to get the Grid xml string
      String xmlData = gmCycleCountLockReportBean.generateOutput(alResult, hmApplnParam);
      gmCycleCountDashboardForm.setXmlString(xmlData);

    }
    strForwardPage = "GmCycleCountDashBoard";
    // to set the warehouse and status drop down values
    gmCycleCountDashboardForm.setAlWarehoue(alWareHouse);
    gmCycleCountDashboardForm.setAlStatus(alStatus);
    return actionMapping.findForward(strForwardPage);
  }
}
