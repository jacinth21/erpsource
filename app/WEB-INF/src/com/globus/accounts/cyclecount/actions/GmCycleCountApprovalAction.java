package com.globus.accounts.cyclecount.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.cyclecount.beans.GmCycleCountLockReportBean;
import com.globus.accounts.cyclecount.beans.GmCycleCountLockTransBean;
import com.globus.accounts.cyclecount.forms.GmCycleCountApprovalForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * Cycle count review and approval
 * 
 * @author mmuthusamy
 *
 */
public class GmCycleCountApprovalAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger

  /**
   * loadReviewDtls - This method used to load the Counted sheet information to Manager review
   * 
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward loadReviewDtls(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCycleCountApprovalForm gmCycleCountApprovalForm = (GmCycleCountApprovalForm) form;
    gmCycleCountApprovalForm.loadSessionParameters(request);

    GmCycleCountLockReportBean gmCycleCountLockReportBean =
        new GmCycleCountLockReportBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmApplnParam = new HashMap();
    HashMap hmData = new HashMap();
    HashMap hmHeader = new HashMap();
    HashMap hmAccess = null;

    ArrayList alResult = new ArrayList();

    String strAccessFlag = "";
    String strLocationAccessGrp = "";

    String strForwardPage = "GmCycleCountApproval";
    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountApprovalForm);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String cycleCountId = GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    String strVMTemplateName = "GmCycleCountApprovalPartDtls.vm";
    hmApplnParam.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
    hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

    hmApplnParam.put("LABEL_PROPERTIES_NM",
        "properties.labels.accounts.cyclecount.GmCycleCountApproval");

    ArrayList alAction = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CYCACT"));
    gmCycleCountApprovalForm.setAlChooseAction(alAction);

    hmData = gmCycleCountLockReportBean.loadCycleCountInfo(hmParam);
    ArrayList alTxnDtls =
        GmCommonClass.parseNullArrayList(gmCycleCountLockReportBean.loadTxnDtls(hmParam));
    // to get the header information
    hmHeader = GmCommonClass.parseNullHashMap((HashMap) hmData.get("HEADER"));
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmData.get("LOCK_DTLS"));
    String strCountType = GmCommonClass.parseNull((String) hmHeader.get("COUNTTYPE"));
	// strPopupFlag ,strStatus used L icon show to PC-3314 Cycle count Review/Approval changes
    String strPopupFlag = GmCommonClass.parseNull((String) hmHeader.get("LOTFL"));  
    String strStatus = GmCommonClass.parseNull((String) hmHeader.get("STATUS"));
    log.debug(" My count type values " + strCountType);
    if (strCountType.equals("106816")) { // Set
      strVMTemplateName = "GmCycleCountApprovalSetDtls.vm";
    }
    hmApplnParam.put("TEMPLATE_NM", strVMTemplateName);
      // strPopupFlag ,strStatus used L icon show to PC-3314 Cycle count Review/Approval changes
    hmApplnParam.put("STRPOPUPFLAG", strPopupFlag); 
    hmApplnParam.put("STRSTATUS", strStatus);
    hmApplnParam.put("CYCLECOUNTID", cycleCountId);

    String xmlData = gmCycleCountLockReportBean.generateOutput(alResult, hmApplnParam);
    gmCycleCountApprovalForm.setXmlString(xmlData);
    // to set the form values
    GmCommonClass.getFormFromHashMap(gmCycleCountApprovalForm, hmHeader);
    // tXn ref string
    // hmApplnParam.put("TEMPLATE_NM", "GmCycleCountApprovalPartDtls.vm");
    // String strTxnXMLData = gmCycleCountLockReportBean.generateOutput(alResult, hmApplnParam);

    gmCycleCountApprovalForm.setAlTxnRef(alTxnDtls);

    // to get the button access flag
    strLocationAccessGrp = GmCommonClass.parseNull((String) hmHeader.get("LOCATION_CODE_ALT_NM"));
    strLocationAccessGrp = "CYC_CNT_APP_" + strLocationAccessGrp;
    log.debug(" Cycle Count Record - Access " + strLocationAccessGrp);

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(
            GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID")), strLocationAccessGrp));
    strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    log.debug(" Cycle Count Approval - Permissions Flag = " + hmAccess);
    if (strAccessFlag.equals("Y")) {
      gmCycleCountApprovalForm.setButtonAccessFl(strAccessFlag);
    }
    return actionMapping.findForward(strForwardPage);
  }


  /**
   * saveRecountDtls - Manager review the cycle count and decide the Recount then we update recount
   * flag
   * 
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward saveRecountDtls(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCycleCountApprovalForm gmCycleCountApprovalForm = (GmCycleCountApprovalForm) form;
    gmCycleCountApprovalForm.loadSessionParameters(request);
    GmCycleCountLockTransBean gmCycleCountLockTransBean =
        new GmCycleCountLockTransBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountApprovalForm);

    gmCycleCountLockTransBean.saveRecountdetails(hmParam);

    String strForwardPage =
        "/gmCycleCountApproval.do?method=loadReviewDtls&lockid="
            + GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
    return actionRedirect(strForwardPage, request);
  }


  /**
   * saveOverrideQtyDtls - Using this method to override the System Qty
   * 
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward saveOverrideQtyDtls(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCycleCountApprovalForm gmCycleCountApprovalForm = (GmCycleCountApprovalForm) form;
    gmCycleCountApprovalForm.loadSessionParameters(request);
    GmCycleCountLockTransBean gmCycleCountLockTransBean =
        new GmCycleCountLockTransBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountApprovalForm);

    gmCycleCountLockTransBean.saveOverrideQty(hmParam);

    String strForwardPage =
        "/gmCycleCountApproval.do?method=loadReviewDtls&lockid="
            + GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
    return actionRedirect(strForwardPage, request);
  }

  /**
   * saveApprovedDtls - using this method to approve the Cycle count - If any adjustment then create
   * the IAXX and XXIA transactions
   * 
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  public ActionForward saveApprovedDtls(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCycleCountApprovalForm gmCycleCountApprovalForm = (GmCycleCountApprovalForm) form;
    gmCycleCountApprovalForm.loadSessionParameters(request);
    GmCycleCountLockTransBean gmCycleCountLockTransBean =
        new GmCycleCountLockTransBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountApprovalForm);
    String strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
    hmParam.put("COMPANYINFO",strCompanyInfo);
    gmCycleCountLockTransBean.saveApprovalQty(hmParam);
    
    //JMS call to update or insert qty
    gmCycleCountLockTransBean.updateLotProcessJMS(hmParam);
    
    String strForwardPage =
        "/gmCycleCountApproval.do?method=loadReviewDtls&lockid="
            + GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
    return actionRedirect(strForwardPage, request);
  }



}
