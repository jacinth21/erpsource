package com.globus.accounts.cyclecount.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.cyclecount.beans.GmCycleCountLotRptBean;
import com.globus.accounts.cyclecount.forms.GmCycleCountLotRptForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;

/**
 * 
 * GmCycleCountRecordAction - To record the cycle count qty
 * 
 * @author MKosalram
 *
 */

public class GmCycleCountLotRptAction extends GmDispatchAction {
	  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger

		/**
		 * 
		 * printDtls - This method used to print the cycle count Scanned Lot Report
		 * 
		 * @param actionMapping
		 * @param form
		 * @param request
		 * @param response
		 * @return
		 * @throws AppError
		 * @throws ServletException
		 * @throws IOException
		 */
		public ActionForward loadScannedLots(ActionMapping actionMapping, ActionForm form, HttpServletRequest request,
				HttpServletResponse response) throws AppError, ServletException, IOException {
			// call the instantiate
			instantiate(request, response);
			GmCycleCountLotRptForm gmCycleCountLotRptForm = (GmCycleCountLotRptForm) form;
			gmCycleCountLotRptForm.loadSessionParameters(request);
			
			gmCycleCountLotRptForm.setHcycleCountId(gmCycleCountLotRptForm.getCycleCountId());
			gmCycleCountLotRptForm.setHpart(gmCycleCountLotRptForm.getPart());
			gmCycleCountLotRptForm.setHlocationId(gmCycleCountLotRptForm.getLocationId());
			gmCycleCountLotRptForm.setHconsignmentId(gmCycleCountLotRptForm.getConsignmentId());
			gmCycleCountLotRptForm.setHstrPopupFlag(gmCycleCountLotRptForm.getStrPopupFlag());
			
		    String strForwardPage = "GmCycleCountLotRpt";
	    return actionMapping.findForward(strForwardPage);
	  }

		/**
		 *fetchScannedLots - used to fetch the Scanned Lots Report Details
		 * 
		 * @param mapping, form, request, response
		 * @return
		 * @throws Exception
		 */
		public ActionForward fetchScannedLots(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response) throws Exception {
			
			instantiate(request, response);
			GmCycleCountLotRptForm gmCycleCountLotRptForm = (GmCycleCountLotRptForm) form;
			gmCycleCountLotRptForm.loadSessionParameters(request);
			GmCycleCountLotRptBean gmCycleCountLotRptBean = new GmCycleCountLotRptBean(getGmDataStoreVO());

			String strJSONGridData = "";
			HashMap hmParam = new HashMap();
			HashMap hmRegStr = new HashMap();
			
			hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountLotRptForm);
			hmRegStr.put("PARTNUM", gmCycleCountLotRptForm.getPart());
			hmRegStr.put("SEARCH", gmCycleCountLotRptForm.getPnumSuffix());
	        String strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
	        
	        hmParam.put("PART", strPartNumFormat);

			strJSONGridData = gmCycleCountLotRptBean.loadScannedLots(hmParam);
			response.setContentType("text/json");
			PrintWriter pw = response.getWriter();
			if (!strJSONGridData.equals("")) {
				pw.write(strJSONGridData);
			}
			pw.flush();

			return null;
		}
}
