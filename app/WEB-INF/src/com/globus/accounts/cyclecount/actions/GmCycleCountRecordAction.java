package com.globus.accounts.cyclecount.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.cyclecount.beans.GmCycleCountLockReportBean;
import com.globus.accounts.cyclecount.beans.GmCycleCountLockTransBean;
import com.globus.accounts.cyclecount.forms.GmCycleCountRecordForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
/**
 * 
 * GmCycleCountRecordAction - To record the cycle count qty
 * 
 * @author pvigneshwaran
 *
 */
/**
 * @author pvigneshwaran
 *
 */
public class GmCycleCountRecordAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger

  /**
   * loadRecordInfo - to load the cycle count lock id information.
   * 
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws IOException
   * @throws ServletException
   * @throws Exception
   */
  public ActionForward loadRecordInfo(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException {
    // call the instantiate method
    instantiate(request, response);
    GmCycleCountRecordForm gmCycleCountRecordForm = (GmCycleCountRecordForm) form;
    gmCycleCountRecordForm.loadSessionParameters(request);

    GmCycleCountLockReportBean gmCycleCountLockReportBean =
        new GmCycleCountLockReportBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmAccess = null;
    HashMap hmData = new HashMap();
    HashMap hmHeader = new HashMap();
    HashMap hmApplnParam = new HashMap();
    ArrayList alResult = new ArrayList();

    String strForwardPage = "";
    String strAccessFlag = "";
    String strLocationAccessGrp = "";
    String strLotTrackFlag = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountRecordForm);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    if (strOpt.equals("Load")) {

    
      String strVMTemplateName = "GmCycleCountRecordPartDtls.vm";
      // to get the Lock details
      hmData = gmCycleCountLockReportBean.loadCycleCountInfo(hmParam);
      // to get the header and details information
      hmHeader = GmCommonClass.parseNullHashMap((HashMap) hmData.get("HEADER"));
      alResult = GmCommonClass.parseNullArrayList((ArrayList) hmData.get("LOCK_DTLS"));
     //  strLotTrackFlag used PC-3498 Cycle count - Changes in cycle count recording
      strLotTrackFlag = GmCommonClass.parseNull((String) hmHeader.get("LOTFL"));
      gmCycleCountRecordForm.setStrLotTrackFl(strLotTrackFlag);
      String strCountType = GmCommonClass.parseNull((String) hmHeader.get("COUNTTYPE"));
      //Get warehouse ID For Button Show.Button show only FG Bulk and RW bulk face
      String strWarehouseId =  GmCommonClass.parseNull((String) hmHeader.get("WAREHOUSEID"));
      String strRuleValue = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strWarehouseId, "CYC_CNT_LOC"));
      gmCycleCountRecordForm.setStrRuleValue(strRuleValue);
      
      
      if (strCountType.equals("106816")) { // Set
        strVMTemplateName = "GmCycleCountRecordSetDtls.vm";
      }

      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
  //  strLotTrackFlag used PC-3498 Cycle count - Changes in cycle count recording
      hmApplnParam.put("STRLOTTRACKFL", strLotTrackFlag);    
      hmApplnParam.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
      hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmApplnParam.put("TEMPLATE_NM", strVMTemplateName);
      hmApplnParam.put("LABEL_PROPERTIES_NM",
          "properties.labels.accounts.cyclecount.GmCycleCountRecord");

      String xmlData = gmCycleCountLockReportBean.generateOutput(alResult, hmApplnParam);
      gmCycleCountRecordForm.setXmlString(xmlData);

      // to set the form values
      GmCommonClass.getFormFromHashMap(gmCycleCountRecordForm, hmHeader);


    }

    // to get the button access flag
    strLocationAccessGrp = GmCommonClass.parseNull((String) hmHeader.get("LOCATION_CODE_ALT_NM"));
    strLocationAccessGrp = "CYC_CNT_REC_" + strLocationAccessGrp;
    log.debug(" Cycle Count Record - Access " + strLocationAccessGrp);

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(
            GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID")), strLocationAccessGrp));
    strAccessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    log.debug(" Cycle Count Record - Permissions Flag = " + hmAccess);
    if (strAccessFlag.equals("Y")) {
      gmCycleCountRecordForm.setButtonAccessFl(strAccessFlag);
    }


    strForwardPage = "GmCycleCountRecord";
    return actionMapping.findForward(strForwardPage);
  }

  /**
   * saveRecordInfo - to update the cycle count qty details - based on Lock id
   * 
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws IOException
   * @throws ServletException
   * @throws Exception
   */
  public ActionForward saveRecordInfo(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException {
    // call the instantiate method
    instantiate(request, response);

    GmCycleCountRecordForm gmCycleCountRecordForm = (GmCycleCountRecordForm) form;
    gmCycleCountRecordForm.loadSessionParameters(request);

    GmCycleCountLockTransBean gmCycleCountRecordBean =
        new GmCycleCountLockTransBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    String strForwardPage = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountRecordForm);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    if (strOpt.equals("Save")) {

      // to call the save reocord information
      gmCycleCountRecordBean.saveCycleCountDetail(hmParam);
      // once update the values call the fetch method
      strForwardPage =
          "/gmCycleCountRecord.do?method=loadRecordInfo&strOpt=Load&lockid="
              + GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
      
      return actionRedirect(strForwardPage, request);
    }

    return actionMapping.findForward(strForwardPage);
  }

  /**
   * 
   * printDtls - This method used to print the cycle count details
   * 
   * @param actionMapping
   * @param form
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws ServletException
   * @throws IOException
   */
  public ActionForward printDtls(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException {
    // call the instantiate
    instantiate(request, response);
    GmCycleCountRecordForm gmCycleCountRecordForm = (GmCycleCountRecordForm) form;
    gmCycleCountRecordForm.loadSessionParameters(request);

    GmCycleCountLockReportBean gmCycleCountLockReportBean =
        new GmCycleCountLockReportBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    String strForwardPage = "GmCycleCountRecordPrint";
    String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");

    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountRecordForm);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    // PC-3495 Cycle count - Adding lots in print paperwork
    String strLotTrackFl = GmCommonClass.parseNull((String)hmParam.get("STRLOTTRACKFL"));
    hmParam.put("ACTION", "PRINT");
    HashMap hmData = gmCycleCountLockReportBean.loadCycleCountInfo(hmParam);
    HashMap hmHeader = GmCommonClass.parseNullHashMap((HashMap) hmData.get("HEADER"));
    // to get the company information
    HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), "");
    hmHeader.put("COMPNAME",
        GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYNAME")));
    hmHeader.put("COMPADDRESS",
        GmCommonClass.parseNull((String) hmCompanyAddress.get("GMCOMPANYADDRESS")));
    hmHeader
        .put(
            "COMP_LOGO",
            strImagePath
                + GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_LOGO") + ".gif"));

    hmData.put("HEADER", hmHeader);
    // PC-3495 Cycle count - Adding lots in print paperwork
    hmData.put("LOTTRACKFL", strLotTrackFl);
    gmCycleCountRecordForm.setHmData(hmData);
    return actionMapping.findForward(strForwardPage);
  }
  
  
  /**
   * add new parts:This method used to add new parts
 * @param actionMapping
 * @param form
 * @param request
 * @param response
 * @return
 * @throws AppError
 * @throws ServletException
 * @throws IOException
 */
public ActionForward addnewParts(ActionMapping actionMapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
	      IOException {
	    // call the instantiate
	    instantiate(request, response);
	    GmCycleCountRecordForm gmCycleCountRecordForm = (GmCycleCountRecordForm) form;
	    gmCycleCountRecordForm.loadSessionParameters(request);
	    GmCycleCountLockReportBean gmCycleCountLockReportBean =
	        new GmCycleCountLockReportBean(getGmDataStoreVO());
	    GmCycleCountLockTransBean gmCycleCountRecordBean =
	        new GmCycleCountLockTransBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    String strForwardPage = "";
	    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountRecordForm);
	    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		String strURL = "/gmCycleCountRecord.do?method=saveRecordInfo&strOpt=Save&lockid="
	              + GmCommonClass.parseNull((String) hmParam.get("LOCKID"));
		String strDisplayNm = "Cycle Count Record Screen";
	    if (strOpt.equals("Save")) {
	        // To call the new method to save the new parts details
	    	gmCycleCountRecordBean.saveNewParts(hmParam);
	        // once update the values call the fetch method
	    	//for throw Success Message
			request.setAttribute("hType", "S");
			request.setAttribute("hRedirectURL", strURL);
			request.setAttribute("hDisplayNm", strDisplayNm);
		    String msg = "Part Number Updated Successfully";
			throw new AppError(msg, "", 'S');   
	      }
	    return actionMapping.findForward(strForwardPage);
	
	    
  }
  /**
   * loadPartInfo:This Method used to load part information
 * @param actionMapping
 * @param form
 * @param request
 * @param response
 * @return
 * @throws AppError
 * @throws ServletException
 * @throws IOException
 */
public ActionForward loadPartInfo(ActionMapping actionMapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
	      IOException {
	    // call the instantiate method
	    instantiate(request, response);
	    GmCycleCountRecordForm gmCycleCountRecordForm = (GmCycleCountRecordForm) form;
	    gmCycleCountRecordForm.loadSessionParameters(request);
	    GmCycleCountLockReportBean gmCycleCountLockReportBean =new GmCycleCountLockReportBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    HashMap hmData = new HashMap();
	    HashMap hmApplnParam = new HashMap();
	    ArrayList alResult = new ArrayList();
	    String strForwardPage = "";
	    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountRecordForm);
	    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
	    if (strOpt.equals("Load")) {
	    	
	    	 String strVMTemplateName = "GmCycleCountAddParts.vm";
	         // to get the fetch add part details
	    	 alResult = gmCycleCountLockReportBean.fetchAddPartsDetails(hmParam);
	         String strSessCompanyLocale =
	             GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

	         hmApplnParam.put("SESSDATEFMT", getGmDataStoreVO().getCmpdfmt());
	         hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
	         hmApplnParam.put("TEMPLATE_NM", strVMTemplateName);
	         hmApplnParam.put("LABEL_PROPERTIES_NM",
	             "properties.labels.accounts.cyclecount.GmCycleCountRecord");

	         String xmlData = gmCycleCountLockReportBean.generateOutput(alResult, hmApplnParam);
	         gmCycleCountRecordForm.setXmlPartData(xmlData);

	         
	    }
	    
	    strForwardPage = "GmCycleCountAddParts";
	    return actionMapping.findForward(strForwardPage);
}



/**
 * fetchCycleCountDtls - get cycle count physical quantity
 * @param actionMapping, form, request, response
 * @return
 * @throws Exception
 */
public ActionForward fetchCycleCountDtls(ActionMapping actionMapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws Exception {
	    instantiate(request, response);
	    GmCycleCountRecordForm gmCycleCountRecordForm = (GmCycleCountRecordForm) form;
	    gmCycleCountRecordForm.loadSessionParameters(request);
	    GmCycleCountLockReportBean gmCycleCountLockReportBean =new GmCycleCountLockReportBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    hmParam = GmCommonClass.getHashMapFromForm(gmCycleCountRecordForm);
	    String strJsonString = GmCommonClass.parseNull(gmCycleCountLockReportBean.fetchCycleCountDtls(hmParam));
	    response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
		}
		pw.flush();
		return null;
 }

}
