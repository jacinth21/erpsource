/*****************************************************************************
 * File			 : GmInvoiceServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;
import com.globus.common.beans.*;
import com.globus.accounts.beans.*;
import com.globus.custservice.beans.*;
import com.globus.operations.beans.GmVendorBean;
import com.globus.sales.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;

import org.apache.commons.beanutils.RowSetDynaClass;

import java.util.*;

public class GMDHRFlagInvoiceServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
		String strDispatchTo = strAcctPath.concat("/GmDHRMultiplePayment.jsp");
		
		
		try
		{
				checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				GmCommonClass gmCommon = new GmCommonClass();
				GmVendorBean gmVendor = new GmVendorBean();
				GmAPBean gmAP = new GmAPBean();
				
				HashMap hmReturn = new HashMap();
				HashMap hmLoadReturn = new HashMap();
				HashMap hmParam = new HashMap();
                HashMap hmTemp = new HashMap();
                hmTemp.put("CHECKACTIVEFL","N"); // check for active flag status in vendor list
                
				ArrayList alReturn = new ArrayList();
				RowSetDynaClass resultSet = null ;

				String strUserId = 	(String)session.getAttribute("strSessUserId");
				String strPageInfo = (String)session.getAttribute("strSessOpt"); // Getting strPageInfo from LeftMenu JSP. The value should be POSTDHRTXN
				String strCurDate = (String)session.getAttribute("strSessTodaysDate");
				String strDetails = "";
				String strAction = request.getParameter("hAction");
				System.out.println(" Value of Mystery action is " + strAction);
							
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"load":strAction;
				
					String strType 		= GmCommonClass.parseNull(request.getParameter("Cbo_Type"));
					String strVenId 	= GmCommonClass.parseNull(request.getParameter("Cbo_VendorId"));
					String strPOID 		= GmCommonClass.parseNull(request.getParameter("Txt_PORDER"));
					String strPartID 	= GmCommonClass.parseNull(request.getParameter("Txt_PARTID"));
					String strPackingSlipNum 		= GmCommonClass.parseNull(request.getParameter("Txt_PACKINGSLIP"));
					String strPaymentDate = GmCommonClass.parseNull(request.getParameter("Txt_PDate"));
					String strInvoiceNum =  GmCommonClass.parseNull(request.getParameter("Txt_InvNum"));
					String strPayMode =  GmCommonClass.parseNull(request.getParameter("Cbo_PayMode"));
					strDetails =  GmCommonClass.parseNull(request.getParameter("Txt_Details"));
					String strCheckDHRID = GmCommonClass.parseNull(request.getParameter("hInputStr"));
					String strTotal = GmCommonClass.parseNull(request.getParameter("hTotal"));
					String strChequeNum = GmCommonClass.parseNull(request.getParameter("Txt_chqNum"));
				

				/* System.out.println("strType" +strType);	
				System.out.println("strVenId" +strVenId);
				System.out.println("Date of Txt_PDate" +strPaymentDate);
				System.out.println("strInvoiceNum " +strInvoiceNum);
				System.out.println("strPayMode e" +strPayMode);
				System.out.println("strDetails " +strDetails);
				System.out.println("strCheckDHRID " +strCheckDHRID);
				System.out.println("strTotal " +strTotal);
				System.out.println("Cheque Number " +strChequeNum); */
				
				// To defaul to 1 means DHR Ready for Payment
				if (strType.equals("")) {
				    strType = "1";
				}
				
				/* HashMap which contains the data which has to be re-loaded
				 *  The data in this HashMap contains the filters which needs to be sent to the Query to fetch the details of the DHR table
				 */
				
				hmLoadReturn.put("VENDID",strVenId);
				hmLoadReturn.put("PARTID",strPartID);
				hmLoadReturn.put("POID",strPOID);
				hmLoadReturn.put("TYPE",strType);
				hmLoadReturn.put("PSLIP",strPackingSlipNum);
				hmLoadReturn.put("PAGE",strPageInfo);
				/*
				 * HashMap which contains the data that needs to be updated when the payment is made
				 */
				hmParam.put("VENDID",strVenId);
				hmParam.put("INVOICEID",strInvoiceNum);
				hmParam.put("PAYDATE",strPaymentDate);
				hmParam.put("PAYMODE",strPayMode);
				hmParam.put("PAYAMT",strTotal);
				hmParam.put("CHEQUENUM",strChequeNum);
				hmParam.put("COMMENTS",strDetails);
				hmParam.put("DHRID",strCheckDHRID);
				hmParam.put("CURRDT",strCurDate);
				
				// If the user selected to view vendor information
				hmReturn = gmVendor.getVendorList(hmTemp);
				ArrayList alPayMode = gmCommon.getCodeList("PAYME");
				hmReturn.put("PAYMODEDETAILS",alPayMode);
				
				if (strAction.equals("UPDALL"))
				{
					String strResult = gmAP.saveVendorPayment(hmParam,strUserId);
					strAction = "Reload";
				}
				
				if (strAction.equals("Reload")  || (strAction.equals("UPDALL")))
				{
					resultSet = (RowSetDynaClass)gmAP.getDHRList(hmLoadReturn);
					request.setAttribute("resultSet",resultSet);
				}
			
				request.setAttribute("hmLoadReturn",hmLoadReturn);
				request.setAttribute("hmReturn",hmReturn);
				request.setAttribute("hmParam",hmParam);
				request.setAttribute("resultSet",resultSet);

				request.setAttribute("hAction",strAction);
								
				strDispatchTo = strAcctPath.concat("/GmDHRMultiplePayment.jsp");
				dispatch(strDispatchTo,request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmInvoiceServlet