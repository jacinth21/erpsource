/*****************************************************************************
 * File : GmInvoiceInfoServlet Desc : This Clss used to generate the invoice print/pdf/xml file
 * based on user request.
 * 
 * This class called from below location 1. All the invoice print (portal) 2. After generate the
 * invoice - to create the PDF file (JMS)
 * 
 * Author : Elango Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.beans.GmAccountingBean;
import com.globus.accounts.beans.GmInvoiceBean;
import com.globus.accounts.beans.GmInvoicePrintBean;
import com.globus.accounts.beans.GmInvoicePrintDtlsInterface;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.printwork.invoice.beans.GmInvoicePrintInterface;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.party.beans.GmDealerInvoiceParamsBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmInvoiceInfoServlet extends GmServlet {
  public static final String TEMPLATE_NAME = "GmInvoicePDFEmail";

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = strAcctPath.concat("/GmInvoicePrint.jsp");
    String strPrintPurposelbl = "";
    String strBillCountry = "";

    try {
      String type = GmCommonClass.parseNull(request.getParameter("type"));
      String strComapnyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
      GmDataStoreVO tmpGmDataStoreVO = null;
      log.debug("GmInvoiceInfoServlet//type " + type);
      if (!type.equals("savePDFfile")) {
        HttpSession session = request.getSession(false);
        checkSession(response, session); // Checks if the current session is valid, else redirecting
                                         // to SessionExpiry page
        instantiate(request, response);
      }
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      if (type.equals("savePDFfile")) {
        // When calling the JMS some times company info came as empty values.
        // at the time we are getting the default company information.
        if (strComapnyInfo.equals("")) {
          tmpGmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
        } else {
          tmpGmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strComapnyInfo);
        }
      } else {
        tmpGmDataStoreVO = getGmDataStoreVO();
      }

      GmCommonClass gmCommon = new GmCommonClass();
      GmARBean gmAR = new GmARBean(tmpGmDataStoreVO);
      GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(tmpGmDataStoreVO);
      GmDealerInvoiceParamsBean gmDealerSetupBean = new GmDealerInvoiceParamsBean(tmpGmDataStoreVO);
      GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmValues = new HashMap();
      HashMap hmDealerInvoice = new HashMap();
      HashMap hmDealerInfo = new HashMap();
      HashMap hmAcctAdd = new HashMap();

      ArrayList alPaidMonth = new ArrayList();
      ArrayList alDealerAccount = new ArrayList();

      String strInvSource =
          GmCommonClass.parseNull(request.getParameter("Cbo_InvSource")) == null ? "" : request
              .getParameter("Cbo_InvSource");
      String strFormat =
          GmCommonClass.parseNull(request.getParameter("Cbo_Action")) == null ? "" : request
              .getParameter("Cbo_Action");
      String strAccId =
          GmCommonClass.parseNull(request.getParameter("Cbo_AccId")) == null ? "" : request
              .getParameter("Cbo_AccId");
      String strUserId =
          !type.equals("savePDFfile") ? GmCommonClass.parseNull((String) session
              .getAttribute("strSessUserId")) : "";


      String strSessAccId = "";
      String strSessPO = "";
      String strInv = "";
      Date dtInvDate = null;
      String strInvType = "";

      String strVoidFlag = "";
      String strPrnPdf = "";
      String strCountryInvoice = "";
      String strGlobusVATNum = "";
      String strCountryVATNum = "";
      String strNipAcc = "";
      String strRuleID = "1";
      String strVoidFl = "";
      String strAccountVATNum = "";
      // getting the company id
      String strCompanyId = tmpGmDataStoreVO.getCmpid();
      String strCompanyLocale = "";
      String strDealerId = "";
      String strAcctId = "";

      // Getting date format based on country
      String strApplnDateFmt =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
      request.setAttribute("APPLNDATEFMT", strApplnDateFmt);

      ArrayList alType = new ArrayList();
      alType = GmCommonClass.getCodeList("INVSR");
      log.debug("GmInvoiceInfoServlet//alType " + alType);
      log.debug("GmInvoiceInfoServlet//strAccId " + strAccId);
      log.debug("GmInvoiceInfoServlet//strFormat " + strFormat);
      log.debug("GmInvoiceInfoServlet//strInvSource " + strInvSource);
      request.setAttribute("ALTYPE", alType);
      request.setAttribute("IDACCOUNT", strAccId);
      request.setAttribute("hMode", strFormat);
      request.setAttribute("INVSOURCE", strInvSource);

      String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
      strAction =
          !type.equals("savePDFfile") ? GmCommonClass.parseNull((String) session
              .getAttribute("hAction")) : strAction;
      strAction = strAction.equals("") ? "Print" : strAction;

      strPrnPdf = GmCommonClass.parseNull((String) request.getAttribute("printPDF"));

      String strOpt =
          !type.equals("savePDFfile") ? GmCommonClass.parseNull((String) session
              .getAttribute("strSessOpt")) : "";
      log.debug("strAction " + strAction + ":strPrnPdf:" + strPrnPdf + ":strOpt:" + strOpt);

      strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
      // Locale
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
      log.debug("GmInvoiceInfoServlet//strAction " + strAction);
      if (strAction.equals("Print")) {

        HashMap hmReportInData = new HashMap();
        GmAccountingBean gmAccountingBean = new GmAccountingBean(tmpGmDataStoreVO);
        strSessAccId = GmCommonClass.parseNull(request.getParameter("hAccId"));
        strSessPO = GmCommonClass.parseNull(request.getParameter("hPO"));
        strInv = GmCommonClass.parseNull(request.getParameter("hInv"));
        strInv =
            strInv.equals("") ? GmCommonClass.parseNull((String) session.getAttribute("hInv"))
                : strInv;
        strInvType = GmCommonClass.parseNull(request.getParameter("hInvType"));
        log.debug("GmInvoiceInfoServlet//strInvType " + strInvType);
        strVoidFlag = GmCommonClass.parseNull(request.getParameter("hVoidFlag"));
        // these codes will work for print invoice so comment them for today's demo

        hmValues = gmAccountingBean.fetchInvoiceSource(strInv);
        strInvSource = GmCommonClass.parseNull((String) hmValues.get("INVSRC"));
        strInvType = GmCommonClass.parseNull((String) hmValues.get("INVTYP"));
        log.debug("hmValues::" + hmValues + "::strInvType::" + strInvType + "::INvoice Source ::"
            + strInvSource);

        if ((strInvSource.equals("50256") || strInvSource.equals("50253"))
            && (!strInvType.equals("50202") && !strInvType.equals("50203"))) {
          // redirect to GmConsignSetServlet with action "PrintVersion" and pass the Invoice Id and
          // Dept ID
          String strDeptId =
              !type.equals("savePDFfile") ? GmCommonClass.parseNull((String) session
                  .getAttribute("strSessDeptSeq")) : "2020";
          request.setAttribute("hAction", "PrintVersion");
          request.setAttribute("hInvoiceId", strInv);
          request.setAttribute("hDeptId", strDeptId);
          // PC-3329 Invoice Word rtf Download - InterCompanySale
          if(strInvSource.equals("50253") && type.equals("saveRTFfile")){
        	  type = "saveRTFfile";
          }
          String strAdress =
              "/GmConsignSetServlet?hAction=PrintVersion&hInvoiceId=" + strInv + "&hInvoiceType="
                  + strInvType + "&type=" + type;
          // Passing user id and type
          /*
           * String strAdress =
           * "/GmConsignSetServlet?hAction=PrintVersion&hUserID="+strUserId+"&type="+ type
           * +"&hInvoiceId=" + strInv + "&hInvoiceType=" + strInvType;
           */
          if (strPrnPdf.equals("pdfprint")) {
            log.debug("strPrnPdf ====" + strPrnPdf);
            dispatch(strAdress, request, response);
            // strDispatchTo = "/prodmgmnt/GmProjectsDashboard.jsp";
          } else {
            gotoPage(strAdress, request, response);
          }
          log.debug("GmInvoiceInfoServlet//strAdress " + strAdress);
        } else {
          if (strVoidFlag.equals("VoidInvoice")) {
            strAction = "PrintVoid";
          }

          hmReturn = new HashMap();
          hmReturn = gmAR.loadInvoiceDetails(strInv, strAction);
          String strMonInvCompId =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "MONTHLY_INV_COMP"));
          // To get japan invoice details
          if (strCompanyId.equals(strMonInvCompId)) {
            strDealerId = GmCommonClass.parseNull((String) hmValues.get("DEALERNAME"));
            strAcctId = GmCommonClass.parseNull((String) hmValues.get("ACCID"));
            hmDealerInvoice = gmInvoicePrintBean.loadJapanInvoiceDetails(strInv);
            hmDealerInvoice.put("DEALERINVID", strDealerId);
            hmDealerInvoice.put("ACCID", strAcctId);
            alPaidMonth = gmInvoicePrintBean.loadJapanPaidInMonth(hmDealerInvoice);

            if (!strDealerId.equals("")) {
              alDealerAccount = gmDealerSetupBean.fetchDealerAccountReport(hmValues);
              if (alDealerAccount.size() != 0) {
                hmDealerInfo = GmCommonClass.parseNullHashMap((HashMap) alDealerAccount.get(0));
              }
            } else {
              hmAcctAdd = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("ORDERDETAILS"));
              String strAccountAdd = GmCommonClass.parseNull((String) hmAcctAdd.get("ACCADDRESS"));
              String strAccID = GmCommonClass.parseNull((String) hmAcctAdd.get("ACCID"));
              String strAccName = GmCommonClass.parseNull((String) hmAcctAdd.get("ACCOUNTNAME"));
              hmDealerInfo.put("DEALERADDRESS", strAccountAdd);
              hmDealerInfo.put("DEALERID", strAccID);
              hmDealerInfo.put("DEALERNAME", strAccName);
            }

            if (alPaidMonth.size() != 0) {
              hmReportInData.put("ALPAIDMONTH", alPaidMonth);
            }
          }
          String strSubReportPath = GmCommonClass.getString("GMJASPERTEMPLATEPATH");
          strSubReportPath = strSubReportPath + "invoice";

          hmReturn.put("DEALERDETAILS", hmDealerInvoice);
          hmReturn.put("SUBREPORT_DIR", strSubReportPath);
          hmReportInData.put("hmReturn", hmReturn);
          hmReportInData.put("hVoidFlag", strVoidFlag);

          request.setAttribute("hmReturn", hmReturn);
          request.setAttribute("hVoidFlag", strVoidFlag);

          // print 'Commercial Invoice' when strInvsource is 50257:Inter-Company and Invoice title
          // in database Invoice,
          HashMap hmOrderDetails = new HashMap();
          hmOrderDetails = (HashMap) hmReturn.get("ORDERDETAILS");
          String strInvoiceTitle = "";
          String strInvLable = GmCommonClass.parseNull((String) hmOrderDetails.get("INVTITLE"));

          log.debug("GmInvoiceInfoServlet//strInvSource1 " + strInvSource);


          if (strInvSource.equals("50257") && !strInvLable.equalsIgnoreCase("Freight")
              && !strInvType.equals("50202") && !strInvType.equals("50203")) {
            strInvoiceTitle = "Commercial Invoice";
            hmReportInData.put("COMMERCIALINV", "yes");
            request.setAttribute("COMMERCIALINV", "yes");
          } else {
            strInvoiceTitle = gmResourceBundleBean.getProperty("INVOICE." + strInvType);
            strInvoiceTitle = strInvoiceTitle.equals("") ? "Invoice" : strInvoiceTitle;
            hmReportInData.put("COMMERCIALINV", "no");
            request.setAttribute("COMMERCIALINV", "no");
          }
          request.setAttribute("INVOICETITLE", strInvoiceTitle);
          strCountryInvoice =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRuleID, "INVOICEFMT"));
          strGlobusVATNum =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRuleID, "GLOBUSVAT"));
          strCountryVATNum =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRuleID, "COUNTRYVATNUM"));
          String strAccID = GmCommonClass.parseNull((String) hmOrderDetails.get("ACCID"));
          strBillCountry = GmCommonClass.parseNull((String) hmOrderDetails.get("BILLCNTRY"));
          strNipAcc = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strAccID, "NIPACC"));
          // 101185 - VAT # For Account
          strAccountVATNum =
              GmCommonClass.parseNull(GmCommonClass.getAccountAttributeValue(strAccID, "101185"));

          request.setAttribute("NIP", strNipAcc);
          request.setAttribute("GLOBUSVATNUM", strGlobusVATNum);
          request.setAttribute("COUNTRYVATNUM", strCountryVATNum);
          request.setAttribute("ACCOUNTVATNUM", strAccountVATNum);
          // to get the purpose of the multiple invoice print
          strPrintPurposelbl = GmCommonClass.parseNull(request.getParameter("PrintPurpose"));

          hmReportInData.put("INVOICETITLE", strInvoiceTitle);
          hmReportInData.put("NIP", strNipAcc);
          hmReportInData.put("GLOBUSVATNUM", strGlobusVATNum);
          hmReportInData.put("COUNTRYVATNUM", strCountryVATNum);
          hmReportInData.put("strSessApplDateFmt", strApplnDateFmt);
          hmReportInData.put("CompanyId", hmOrderDetails.get("COMPANY_ID"));
          hmReportInData.put("INVPRINTLBL", strPrintPurposelbl);
          strCompanyLocale =
              GmCommonClass.getCompanyLocale((String) hmOrderDetails.get("COMPANY_ID"));
          hmReportInData.put("CompanyLocale", strCompanyLocale);
          // passing the datastoreVO for interface call
          hmReportInData.put("gmDataStoreVO", tmpGmDataStoreVO);
          HashMap hmReportOutData = new HashMap();
          log.debug("GmInvoiceInfoServlet//hmReportInData " + hmReportInData);
          // Implemented an interface call
          GmInvoicePrintDtlsInterface gmInvoicePrintDtlsInterface =
              (GmInvoicePrintDtlsInterface) GmCommonClass.getSpringBeanClass(
                  "xml/Invoice_PrintDtls_Beans.xml", tmpGmDataStoreVO.getCmpid());
          hmReportOutData = gmInvoicePrintDtlsInterface.filterInvoiceData(hmReportInData);
          // hmReportOutData = gmAR.filterInvoiceData(hmReportInData);


          request.setAttribute("CompanyId", hmOrderDetails.get("COMPANY_ID"));
          request.setAttribute("INVRPTDATA", hmReportOutData);
          request.setAttribute("DEALERDETAILS", hmDealerInvoice);
          request.setAttribute("DEALERINFO", hmDealerInfo);
          request.setAttribute("STRBILLCOUNTRY",strBillCountry);
          printInvoice(request, response); // calling this method to dispatch this page into
                                           // GmInvoiceJasperPrint.jsp / GmInvoicePrint.jsp.
        }
      }
    }// End of try
    catch (Exception e) {
      // e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  /**
   * printInvoice : This method is to print/save the invoice. If the method is getting called while
   * Generating the invoice, then the invoice will be saved as PDF or RTF format. If the method is
   * getting called while click on invoice link or button then it will generate the invoice and open
   * the page
   * 
   * @param request
   * @param response
   * @exception AppError
   * @exception Exception
   */
  private void printInvoice(HttpServletRequest request, HttpServletResponse response)
      throws AppError, Exception {

    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = strAcctPath.concat("/GmInvoicePrint.jsp");

    String type = GmCommonClass.parseNull(request.getParameter("type"));
    String strCountryCode = GmCommonClass.countryCode;
    String strInvoiceRuleId = "";
    String strInvoiceJasName = "";
    String strBankName = "";
    String strInvoiceCompanyId =
        GmCommonClass.parseNull((String) request.getAttribute("CompanyId")); // getGmDataStoreVO().getCmpid();

    GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(getGmDataStoreVO());

    ArrayList alOrderNums = new ArrayList();
    ArrayList alDOControlNum = new ArrayList();
    ArrayList alDOCNums = new ArrayList();
    HashMap hmCartDetails = new HashMap();
    HashMap hmConstructs = new HashMap();
    HashMap hmDoCnum = new HashMap();
    HashMap hmDealerInvoice = new HashMap();
    HashMap hmDealerInfo = new HashMap();
    HashMap hmCustPaperDtls = new HashMap();


    String strCustInvoicePortalPrint = "";
    String strCustInvoicePDFPrint = "";

    HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("hmReturn"));
    String strInv = GmCommonClass.parseNull(request.getParameter("hInv"));

    // to get the invoice customer paperwork dtls
    hmCustPaperDtls = gmInvoiceBean.fetchInvoicePaperworkDtls(strInv);
    log.debug(" hmCustPaperDtls ==> " + hmCustPaperDtls);
    strCustInvoicePortalPrint =
        GmCommonClass.parseNull((String) hmCustPaperDtls.get("PORTAL_TEMPLATE"));
    strCustInvoicePDFPrint = GmCommonClass.parseNull((String) hmCustPaperDtls.get("PDF_TEMPLATE"));

    request.setAttribute("CUST_JASPER_NAME", strCustInvoicePortalPrint);

    // To fetch the jasper file name
    fetchJasperFile(request, response);
    strInvoiceRuleId = GmCommonClass.parseNull((String) request.getAttribute("INVOICERULEID"));
    strInvoiceJasName = GmCommonClass.parseNull((String) request.getAttribute("INVJASNAME"));
    strBankName = GmCommonClass.parseNull((String) request.getAttribute("BANKNAME"));
    String strCompanyLocale = GmCommonClass.getCompanyLocale(strInvoiceCompanyId);
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(getGmDataStoreVO());

    String strDispatchJSP =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INVOICE.DISPATCHTO"));
    strDispatchTo = strAcctPath.concat(strDispatchJSP);
    String strSetRbObjectFl =
        GmCommonClass
            .parseNull(gmResourceBundleBean.getProperty("INVOICE.ADD_RESOURCE_BUNDLE_OBJ"));
    log.debug("printInvoice type-- " + type);
    if (type.equals("savePDFfile") || type.equals("saveRTFfile")) {
      // Commenting the condition, to save pdf for ous also
      // if(strCountryCode.equals("en")){
      String strhEmailReq = GmCommonClass.parseNull(request.getParameter("hEmailReq"));
      String strhEversion = GmCommonClass.parseNull(request.getParameter("hEversion"));
      String strhBatchID = GmCommonClass.parseNull(request.getParameter("hBatchID"));

      HashMap hmReturnData =
          GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("INVRPTDATA"));
      ArrayList alHashMap = new ArrayList();
      HashMap hmOrderDetails =
          GmCommonClass.parseNullHashMap((HashMap) hmReturnData.get("HMORDERDETAILS"));

      hmOrderDetails.put("PDFINVOICE", "PDF");
      String strInvoicePDF = GmCommonClass.getString("BATCH_INVOICE");
      String strImgPath = System.getProperty("ENV_PAPERWORKIMAGES");
      hmReturnData.put("COMPANYID", strInvoiceCompanyId);
      hmReturnData.put("hmReturn", hmReturn);
      hmReturnData.put("gmResourceBundleBean", gmResourceBundleBean);
      hmReturnData.put("ACCOUNTVATNUM",
          GmCommonClass.parseNull((String) request.getAttribute("ACCOUNTVATNUM")));
      // OUS code merge, Added code from GmOUSInvoicePrint.jsp
      GmInvoicePrintInterface gmInvoicePrintInterface =
          (GmInvoicePrintInterface) GmInvoiceBean.getInvoicePrintClass(strInvoiceCompanyId);


      alHashMap = gmInvoicePrintInterface.fetchInvoiceArray(hmReturnData);
      hmOrderDetails.put("BANK_DETAILS", strBankName);
      hmOrderDetails.put("IMAGEPATH", strImgPath);
      hmOrderDetails.put("SIZE", alHashMap.size());
      hmOrderDetails.put("ALORDERLIST", alHashMap);
      hmOrderDetails.put("ACCOUNTVATNUM",
          GmCommonClass.parseNull((String) request.getAttribute("ACCOUNTVATNUM")));
      // Passing company address and dealer info for japan invoice PDF
      hmDealerInfo = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("DEALERINFO"));
      hmDealerInvoice =
          GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("DEALERDETAILS"));
      hmOrderDetails.putAll(hmDealerInfo);
      hmOrderDetails.putAll(hmDealerInvoice);

      String year = GmCommonClass.parseNull(request.getParameter("year"));
      String strCompanyCode = GmCommonClass.parseNull((String) hmOrderDetails.get("COMP_CODE"));
      File newFile = new File(strInvoicePDF + "\\" + strCompanyCode + "\\" + year);

      if (!strCustInvoicePDFPrint.equals("")) {
        strInvoiceJasName = strCustInvoicePDFPrint;
      } else
      // US/BBA - different PDF template
      if (GmCommonClass.parseNull(
          GmCommonClass.getRuleValueByCompany(strInvoiceCompanyId, "INVOICE_PDF_FLAG",
              strInvoiceCompanyId)).equals("Y")) {
        strInvoiceJasName =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strInvoiceRuleId,
                "INVOICE_PDF_JASPER", strInvoiceCompanyId));
      }
    //PC-2107: Temporary VAT rate change - Ireland
      //PC-2107: Temporary VAT rate change - Ireland
      if(GmCommonClass.parseNull(
              GmCommonClass.getRuleValueByCompany(strInvoiceCompanyId, "TEMP_VAT",
                      strInvoiceCompanyId)).equals("Y")){
    	  String strInvDt = GmCommonClass.parseNull((String) hmOrderDetails.get("INVDT"));
    	  String strParentInvId = GmCommonClass.parseNull((String) hmOrderDetails.get("PARENT_INV_ID"));
    	  String strInvType = GmCommonClass.parseNull((String) hmOrderDetails.get("INVTYPE"));
      	if (strInvDt != null) {
      		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
      		Date dtInvDt = dateFormat.parse(strInvDt);
      		Date dtStartTempdt = dateFormat.parse(GmCommonClass.getRuleValueByCompany
      				(strInvoiceCompanyId, "TEMP_VAT_ST_DT",strInvoiceCompanyId)); 
      		Date dtEndTempdt = dateFormat.parse(GmCommonClass.getRuleValueByCompany
      				(strInvoiceCompanyId, "TEMP_VAT_ED_DT",strInvoiceCompanyId));
      		if(strInvType.equals("50202") && (!strParentInvId.equals(""))){
      			strInvDt = gmInvoiceBean.fetchParentInvDt(strParentInvId);
      			dtInvDt = dateFormat.parse(strInvDt);
      		} if ((dtInvDt.compareTo(dtStartTempdt) >= 0) && (dtInvDt.compareTo(dtEndTempdt) <= 0)){
      			hmOrderDetails.put("IRTEMPVATRATE", GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strInvoiceCompanyId, "TEMP_VAT_PER",strInvoiceCompanyId)));
      		} else {
      			hmOrderDetails.put("IRTEMPVATRATE", "VAT 23%");
      		}
          }
      }

      log.debug("=========strInvoiceJasName before PDF PRINT===========" + strInvoiceJasName);
      if (!newFile.exists()) {
        newFile.mkdirs();
      }

      String pdffileName =
          strInvoicePDF + "\\" + strCompanyCode + "\\" + year + "\\" + strInv + ".pdf";
      log.debug("pdffileName ++" + pdffileName);
      File strFileChk = new File(pdffileName);
      if (!strFileChk.exists()) {
        GmJasperReport gmJasperReport = new GmJasperReport();
        gmJasperReport.setRequest(request);
        gmJasperReport.setResponse(response);
        gmJasperReport.setJasperReportName("invoice" + strInvoiceJasName);
        // to set the dynamic resource bundle object
        if (strSetRbObjectFl.equalsIgnoreCase("YES")) {
          gmJasperReport.setRbPaperwork(gmResourceBundleBean.getBundle());
        }
        gmJasperReport.setHmReportParameters(hmOrderDetails);
        gmJasperReport.setReportDataList(alHashMap);
        if (type.equals("savePDFfile")) {
          gmJasperReport.exportJasperToPdf(pdffileName);
        } else {
          gmJasperReport.exportJasperReportToRTF();
        }
      }
      // }
      ObjectOutputStream sendStream = null;
      sendStream = new ObjectOutputStream(response.getOutputStream());
      sendStream.close();
    } else {
      dispatch(strDispatchTo, request, response);
    }
  }

  /**
   * fetchJasperFile : To fetch the jasper file name based on different types and layouts. The
   * corresponding name is fetching from the rules table
   * 
   * @param request
   * @param response
   * @exception AppError
   * @exception Exception
   */
  private void fetchJasperFile(HttpServletRequest request, HttpServletResponse response)
      throws AppError, Exception {
	  GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(getGmDataStoreVO());
    String strInvLayoutId = GmCommonClass.parseNull(request.getParameter("invLayout"));
    String strInvoiceCompanyId =
        GmCommonClass.parseNull((String) request.getAttribute("CompanyId"));
    String strInvoiceRuleType = "";
    String strInvLayout = "";
    String strInvType = "";
    String strTaxStartDateFl = "";
    String strAdjStartDateFl = "";
    String strICSInvoiceJasName = "/GmUSInvPrintWithTaxJasperForICS.jasper";
    String strXhtmlInvoiceFlag = "N";
    String strCustJasperName = "";
    String strInvoiceJasName = "";
    double dbInvoiceAdjTotal = 0.0;

    HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("hmReturn"));
    HashMap hmReturnData =
        GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("INVRPTDATA"));
    HashMap hmOrderDetails =
        GmCommonClass.parseNullHashMap((HashMap) hmReturnData.get("HMORDERDETAILS"));
    String strTypeAccount = GmCommonClass.parseNull((String) hmReturn.get("INV_SOURCE"));
    String strDistType = GmCommonClass.parseNull((String) hmReturn.get("DISTTYPE"));
    //PC-4381 - Updates in OUS Distributor invoice
    strDistType = strDistType.equals("")?strTypeAccount:strDistType;
    String strICSVATRuleValue =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strDistType, "ICS_SHIPPERTAX"));

    strCustJasperName = GmCommonClass.parseNull((String) request.getAttribute("CUST_JASPER_NAME"));
    log.debug(" Customer Jasper Template ==> " + strCustJasperName);

    hmOrderDetails.put("IBAN", GmCommonClass.parseNull((String) hmReturn.get("IBAN")));
    hmOrderDetails.put("CONSIGN_NAME",
        GmCommonClass.parseNull((String) hmReturn.get("CONSIGN_NAME")));
    hmOrderDetails.put("CONSIGN_PHONE",
        GmCommonClass.parseNull((String) hmReturn.get("CONSIGN_PHONE")));
    hmOrderDetails.put("SWIFTCODE", GmCommonClass.parseNull((String) hmReturn.get("SWIFTCODE")));
    hmOrderDetails.put("GMADDRESS", GmCommonClass.parseNull((String) hmReturn.get("GMADDRESS")));
    hmOrderDetails.put("CREATEDBY", GmCommonClass.parseNull((String) hmReturn.get("CREATEDBY")));
    hmOrderDetails.put("IMPORTSHIPPERTAX",
        GmCommonClass.parseNull((String) hmReturn.get("IMPORTSHIPPERTAX")));
    hmOrderDetails.put("SHIPTAXID", GmCommonClass.parseNull((String) hmReturn.get("SHIPTAXID")));
    hmOrderDetails.put("ICSSHIPTAXID", GmCommonClass.parseNull((String) strICSVATRuleValue));
    hmOrderDetails.put("CONSIGN_REGN",
        GmCommonClass.parseNull((String) hmReturn.get("CONSIGN_REGN")));
    hmOrderDetails
        .put("SHIPPURPOSE", GmCommonClass.parseNull((String) hmReturn.get("SHIPPURPOSE")));

    // Get the adjustment total, if the adjustment total is '0', call the default layout instead of
    // adjustment summary and adjustment detail
    dbInvoiceAdjTotal = GmCommonClass.parseDouble((Double) hmOrderDetails.get("TOTALINVADJ"));

    String strCustomPartRef = GmCommonClass.parseNull((String) hmReturn.get("CUSTOM_PART"));
    String strInvoiceRuleId = "INV_JSP"; // By default it will call invoice JSP.
    strInvoiceRuleId = (strCustomPartRef.equals("Y")) ? "GROUP" : "REGULAR";

    // To get the Invoice rule Id to fetch the jasper for Sales Tax based invoices
    HashMap hmTaxOrderDetails = (HashMap) hmReturn.get("ORDERDETAILS");
    String strDivisionId = GmCommonClass.parseNull((String) hmTaxOrderDetails.get("DIVISION_ID"));

    String strInvoiceType = GmCommonClass.parseNull((String) hmOrderDetails.get("INVTYPE"));
    String strInvTemplate =
        GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strInvoiceRuleId,
            "DEBIT_CREDIT_TEMP", strInvoiceCompanyId));


    /*
     * Code modified by Mani - PMT-44586 : Italy Vatican changes
     * 
     * if customer invoice configured to a separate template then, we are setting the customer
     * template otherwise to exclude the current code
     * 
     * For now customer template added only for Vatican city customer (Italy company)
     */

    if (strCustJasperName.equals("")) {

      if (strDivisionId.equals("")) {
        strDivisionId = "2000";
      }
      // Get the invoice Layout from the Invoice Layout screen, if it is not from the screen then
      // get
      // the layout selected for the account
      strInvLayout =
          strInvLayoutId.equals("") || strInvLayoutId.equals("0") ? GmCommonClass
              .parseNull((String) hmTaxOrderDetails.get("INVOICELAYOUT")) : strInvLayoutId;
      // If no layouts are specified, then it should show the default invoice layout
      strInvLayout = strInvLayout.equals("") ? "10304529" : strInvLayout;// 10304529: Default
                                                                         // invoice
                                                                         // layout
      strInvType = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strInvLayout, "INVRULEID"));
      // the below flag is to know whether the tax related data to be included or not
      strTaxStartDateFl =
          GmCommonClass.parseNull((String) hmTaxOrderDetails.get("TAX_START_DATE_FL"));
      // Get the Adjustment start date to know whether the adjustments are applicable for the
      // invoice
      // or not
      strAdjStartDateFl =
          GmCommonClass.parseNull((String) hmTaxOrderDetails.get("ADJ_START_DATE_FL"));
      // To get the invoice including the tax/ adjustments, either the tax start date should be 'Y'
      // or
      // the adjustment date should be 'Y'
      String strTaxCountryFlag =
          GmCommonClass.parseNull((String) hmTaxOrderDetails.get("TAX_COUNTRY_FL"));
      if (strInvoiceCompanyId.equals("1000")
          && (strTaxStartDateFl.equalsIgnoreCase("Y") || strAdjStartDateFl.equalsIgnoreCase("Y"))) {
        // If the invoice adjustment or tax is not needed to be applied, then should fetch the
        // default
        // invoice layout without any adjustments
        // if the adjustment total is '0', call the default layout instead of adjustment summary and
        // adjustment detail
        strInvType =
            (!strAdjStartDateFl.equalsIgnoreCase("Y") || (dbInvoiceAdjTotal == 0)) ? "TAX"
                : strInvType;
        strInvType = strInvLayout.equals("110040") ? "LISTPRICE" : strInvType; //110040 --Default- List Price
        strInvoiceRuleType = (strCustomPartRef.equals("Y")) ? "_GROUP" : "_REGULAR";
        strInvoiceRuleId = strInvType + strInvoiceRuleType;
      }

      if ((strInvoiceType.equals("50202") || strInvoiceType.equals("50203"))
    		  && (strInvTemplate.equals("Y"))) {
        strInvoiceRuleType = (strCustomPartRef.equals("Y")) ? "_GROUP" : "_REGULAR";;
        strInvoiceRuleId = strInvoiceType + strInvoiceRuleType; // 50203_REGULAR1026      
      }   

      // will fetch the different invoice JASPER name.
      strInvoiceJasName =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strInvoiceCompanyId + "_"
              + strDivisionId + "_" + strInvoiceRuleId, "INVOICE_JASPER"));
      
    //PC-2107: Temporary VAT rate change - Ireland
      if(GmCommonClass.parseNull(
              GmCommonClass.getRuleValueByCompany(strInvoiceCompanyId, "TEMP_VAT",
                      strInvoiceCompanyId)).equals("Y")){
    	  String strInvDt = GmCommonClass.parseNull((String) hmOrderDetails.get("INVDT"));
    	  String strParentInvId = GmCommonClass.parseNull((String) hmOrderDetails.get("PARENT_INV_ID"));
      	if (strInvDt != null) {
      		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
      		Date dtInvDt = dateFormat.parse(strInvDt);
      		Date dtStartTempdt = dateFormat.parse(GmCommonClass.getRuleValueByCompany
      				(strInvoiceCompanyId, "TEMP_VAT_ST_DT",strInvoiceCompanyId)); 
      		Date dtEndTempdt = dateFormat.parse(GmCommonClass.getRuleValueByCompany
      				(strInvoiceCompanyId, "TEMP_VAT_ED_DT",strInvoiceCompanyId));
      		String strInvceType = GmCommonClass.parseNull((String) hmOrderDetails.get("INVTYPE"));
      		if(strInvceType.equals("50202") && (!strParentInvId.equals(""))){
      			strInvDt = gmInvoiceBean.fetchParentInvDt(strParentInvId);
      			dtInvDt = dateFormat.parse(strInvDt);
      		} if ((dtInvDt.compareTo(dtStartTempdt) >= 0) && (dtInvDt.compareTo(dtEndTempdt) <= 0)){
                hmOrderDetails.put("IRTEMPVATRATE", GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strInvoiceCompanyId, "TEMP_VAT_PER",strInvoiceCompanyId)));
      		} else {
      			hmOrderDetails.put("IRTEMPVATRATE", "VAT 23%");
      		}
          }
      }

      strInvoiceJasName = strTypeAccount.equals("50253") ? strICSInvoiceJasName : strInvoiceJasName; // 50253
                                                                                                     // --
                                                                                                     // Inter_Company
                                                                                                     // Sale

    } else {
      strInvoiceJasName = strCustJasperName;
    }// end of cust print


    strXhtmlInvoiceFlag = strTypeAccount.equals("50253") ? "Y" : strXhtmlInvoiceFlag; // 50253 --
                                                                                      // Inter_Company
                                                                                      // Sale
    String strBankName =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strDivisionId, "BANK_DETAILS"));
    request.setAttribute("BANKNAME", strBankName);
    request.setAttribute("INVJASNAME", strInvoiceJasName);
    request.setAttribute("INVOICERULEID", strInvoiceRuleId);
    request.setAttribute("STRXHTMLINVOICEFLAG", strXhtmlInvoiceFlag);

  }
}// End of GmInvoiceServlet
