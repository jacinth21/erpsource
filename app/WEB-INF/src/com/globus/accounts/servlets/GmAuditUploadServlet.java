package com.globus.accounts.servlets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadException;
import javazoom.upload.UploadFile;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmFieldSalesAuditBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmUploadServlet;

public class GmAuditUploadServlet extends GmUploadServlet {

	private static String strUploadHome = GmCommonClass.getString("GMAUDITORUPLOAD");
	private static String strDefaultFileToDispatch = "/accounts/GmAuditUpload.jsp";
	
	private static String strFileName = "";
	private String strFileToDispatch = "";
	private String strUploadFilename="";
	Logger log = GmLogger.getInstance(this.getClass().getName());	
	
	private static String strComnPath = GmCommonClass.getString("GMCOMMON");
	private static String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
	
	public void loadFileProperties( String strFileNm)throws AppError {
		String fileExt = strFileNm.substring(strFileNm.lastIndexOf('.'));	
		strFileNm = strFileNm.substring(0,strFileNm.lastIndexOf('.'));
		File strFile = new File ( strUploadHome,strFileNm+fileExt );
		if(strFile.exists())
		{
			String strNewFile=strFileNm+fileExt;			
			File strFileDir = new File(strUploadHome);
			String strFileList[] = strFileDir.list();			
			for(int i=1; i<=strFileList.length+1; i++)
			{				
				String strTempFile = strNewFile.substring(0, strNewFile.substring(strNewFile.lastIndexOf('\\')+1).lastIndexOf('.'))+"_VER_"+i+fileExt;
				File strFileChk = new File(strUploadHome,strTempFile );
				if(!strFileChk.exists())
				{
					strFileNm = strNewFile.substring(0, strNewFile.substring(strNewFile.lastIndexOf('\\')+1).lastIndexOf('.'))+"_VER_"+i;					
					break;
				}
			}					
		}			
		strUploadFilename=strFileNm+fileExt;
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		String strAction = "";
		strUploadHome = GmCommonClass.getString("GMAUDITORUPLOAD");
		MultipartFormDataRequest mrequest = null;
		strFileToDispatch = strDefaultFileToDispatch;
		HashMap hmparam = new HashMap();
		
		checkSession(response, session); // Checks if the current session is
		// valid, else redirecting to
		// SessionExpiry page
		try {
			if (MultipartFormDataRequest.isMultipartFormData(request)) {
				// creating MultiPart request
				mrequest = new MultipartFormDataRequest(request);
				strAction = GmCommonClass.parseNull((String) mrequest.getParameter("hAction"));	
				Hashtable hmFiles = mrequest.getFiles();
				if ((hmFiles != null) && (!hmFiles.isEmpty())) {
					UploadFile file = (UploadFile) hmFiles.get("uploadfile");
					if (file != null)
					strFileName = file.getFileName();			
				}
			} else {
				strAction = GmCommonClass.parseNull((String) request.getParameter("hAction"));
			}
			
			GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean(); 
			RowSetDynaClass rdAudituploadReport=null;
			HashMap hmParam= new HashMap();
			request.setAttribute("hAction", strAction);			
			ArrayList alDistList= new ArrayList();
			ArrayList alAuditor= new ArrayList();
			String struploadFile="";
			String strRefID="90904";
			
			String strAuditId = GmCommonClass.parseNull((String) request.getParameter("strAuditList"));
			String strDistList = GmCommonClass.parseNull((String) request.getParameter("strDistList"));
			String strAuditorId = GmCommonClass.parseNull((String) request.getParameter("strAuditorId"));
			
			
			ArrayList alAuditList = gmFieldSalesAuditBean.fetchAuditDetails();
			if (strAction.equals("reload")||strAction.equals("load"))
			{				
				alDistList=gmFieldSalesAuditBean.fetchLockedDistributorDetails(strAuditId);
				alAuditor=gmFieldSalesAuditBean.fetchAuditorName(strAuditId);	
			}
			rdAudituploadReport=gmFieldSalesAuditBean.fetchAuditUploadInfo(strAuditId, strDistList, strAuditorId);
			
			hmParam.put("AUDITID", strAuditId);
			hmParam.put("DISTID", strDistList);
			hmParam.put("AUDITOR", strAuditorId);
			hmParam.put("ALAUDITLIST",alAuditList);
			hmParam.put("ALDISTLIST",alDistList);
			hmParam.put("ALAUDITORLIST",alAuditor);
			String strUser=(String) session.getAttribute("strSessUserId");
			hmParam.put("STRSESSUSERID", strUser);
			hmParam.put("REFTYPE",strRefID);			

			if (strAction.equals("upload")) {
				loadFileProperties(strFileName);
				uploadFile(mrequest, request);
				struploadFile=strUploadHome+strUploadFilename;
				hmParam.put("UPLOADFILENAME",strUploadFilename);
				hmParam.put("FILENAME", struploadFile);
				// Saving the file info & values in database
				gmFieldSalesAuditBean.uploadExcelData(hmParam);			
			} 
			
			request.setAttribute("results",rdAudituploadReport);
			request.setAttribute("HMPARAM", hmParam);
			
			dispatch(strFileToDispatch, request, response);

		} catch (AppError e) {
			File file=new File(strUploadHome+strFileName);			
			if( e.getType().equals("E") && file.exists())
			file.delete();
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			request.setAttribute("hType",e.getType());
			dispatch(strFileToDispatch, request, response);
		} catch (UploadException e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		} catch (Exception uex) {
			File file=new File(strUploadHome+strFileName);
			if(file.exists())
			file.delete();
			session.setAttribute("hAppErrorMessage", uex.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		}
	}

	public boolean uploadFile(MultipartFormDataRequest mrequest, HttpServletRequest request) throws UploadException, IOException {
		// Upload the file on server
		
		UploadBean uploadBean = new UploadBean();
		boolean flag = true;
		String strUploadPath = strUploadHome;
		uploadBean.setFolderstore(strUploadPath);
		uploadBean.setOverwrite(true);
		uploadBean.setWhitelist("*.xls");
		Hashtable hmFiles = mrequest.getFiles();
		if ((hmFiles != null) && (!hmFiles.isEmpty())) {
			UploadFile file = (UploadFile) hmFiles.get("uploadfile");
			file.setFileName(strUploadFilename);
			if (file != null)
				uploadBean.store(mrequest, "uploadfile");
			strFileName = file.getFileName();

		}
		return flag;
	}

}
