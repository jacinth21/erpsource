/*****************************************************************************
 * File : GmInvPostServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.accounts.beans.GmAccountingBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.servlets.GmServlet;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.custservice.beans.GmSalesBean;

public class GmInvPostServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = strAcctPath.concat("/GmInvPosting.jsp");

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      GmLogger log = GmLogger.getInstance();
      instantiate(request, response);
      GmAccountingBean gmAcct = new GmAccountingBean(getGmDataStoreVO());
      GmCommonClass gmCommon = new GmCommonClass();
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
      RowSetDynaClass resultSet = null;

      HashMap hmReturn = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alCompId = new ArrayList();
      ArrayList alCountryList = new ArrayList();

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = (String) session.getAttribute("strSessOpt");
      strOpt = (strOpt == null) ? "Load" : strOpt;

      // Locale
      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

      String strElemId = "";
      String strTransType = "";
      String strFromDt = "";
      String strToDt = "";
      String strCogsType = "";
      String strPartNum = "";
      String strTransId = "";
      String strPartNumFormat = "";
      String strCheckOpen = "";
      String strCompanyId = "";
      String strCountryId = "";
      String strArchiveFl = "";
      String strPostArchivedDate = "";
      String strCostArchivedDate = "";
      String strInvPlant = "";
      String strOwnerCurrency = "";
      String strOwnerCompanyId = "";
      String strCogsStatusId = "";
      String strDivisionId = "";

      strArchiveFl = GmCommonClass.parseNull(request.getParameter("Chk_archive_fl"));
      strArchiveFl = strArchiveFl.equals("on") ? "true" : "";
      strPostArchivedDate = GmCommonClass.getRuleValue("POST_ARCHIVE_DATE", "ARCHIVE");
      strCostArchivedDate = GmCommonClass.getRuleValue("COST_ARCHIVE_DATE", "ARCHIVE");
      request.setAttribute("POST_ARCHIVEDDATE", strPostArchivedDate);
      request.setAttribute("COST_ARCHIVE_DATE", strCostArchivedDate);
      request.setAttribute("Chk_archive_fl", strArchiveFl);

      HashMap hmRuleGrp = new HashMap();

      ArrayList alSalesReturn = new ArrayList();
      GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
      alSalesReturn = new ArrayList();
      ArrayList alType = new ArrayList();
      ArrayList alReturnInvType = new ArrayList();
      ArrayList alCompDivList = new ArrayList();
      log.debug(" Action value is " + strAction + " Opt Value is " + strOpt);

      alType = GmCommonClass.getCodeList("INVSR");
      request.setAttribute("ALTYPE", alType);
      alReturnInvType = GmCommonClass.getCodeList("IVCTP");
      hmReturn.put("CODELIST", alReturnInvType);
      alCompId = GmCommonClass.parseNullArrayList(gmCommon.getCodeList("COMP"));
      hmReturn.put("ALCOMPID", alCompId);

      hmRuleGrp.put("RULE_GRP", "ICT_CNTRY_DIST_MAP");
      alCountryList = GmCommonClass.parseNullArrayList(gmAcct.fetchPostingByCountryList(hmRuleGrp));
      hmReturn.put("ALCOUNTRYLIST", alCountryList);
      // to get the pland and currency information
      alReturn = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CURRN"));
      hmReturn.put("ALCURRN", alReturn);
      ArrayList alPlant = GmCommonClass.parseNullArrayList(gmCommonBean.loadPlantCompanyMapping());
      hmReturn.put("ALPLANT", alPlant);
      alCompDivList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmReturn));
      hmReturn.put("ALDIVISION", alCompDivList);
      alReturn = GmCommonClass.parseNullArrayList(gmCommonBean.getParentCompanyList());
      hmReturn.put("ALOWNERCOMPANY", alReturn);

      strInvPlant = GmCommonClass.parseZero(request.getParameter("cbo_inv_plant"));
      strOwnerCurrency = GmCommonClass.parseZero(request.getParameter("cbo_owner_currency"));
      strOwnerCompanyId = GmCommonClass.parseZero(request.getParameter("cbo_owner_company"));
      strCogsStatusId = GmCommonClass.parseZero(request.getParameter("cbo_cogs_status"));
      strDivisionId = GmCommonClass.parseZero(request.getParameter("cbo_division"));
    		  
      if (strAction.equals("Load") && strOpt.equals("INV")) {
        alReturn = gmAcct.listAccountElement("");
        hmReturn.put("ELEMENTLIST", alReturn);
        alReturn = gmCommon.getCodeList("TXNTP");
        hmReturn.put("TXNTYPELIST", alReturn);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strAcctPath.concat("/GmInvPosting.jsp");
      } else if (strAction.equals("Reload")) {
        strElemId =
            request.getParameter("Cbo_ElemId") == null ? "" : request.getParameter("Cbo_ElemId");
        strTransType =
            request.getParameter("Cbo_TransTp") == null ? "" : request.getParameter("Cbo_TransTp");
        strFromDt =
            request.getParameter("Txt_FromDate") == null ? "" : request
                .getParameter("Txt_FromDate");
        strToDt =
            request.getParameter("Txt_ToDate") == null ? "" : request.getParameter("Txt_ToDate");
        strTransId =
            request.getParameter("Txt_TransId") == null ? "" : request.getParameter("Txt_TransId");
        strPartNum =
            request.getParameter("Txt_PNum") == null ? "" : request.getParameter("Txt_PNum");

        String strIdAccount =
            request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");

        strCompanyId = GmCommonClass.parseZero(request.getParameter("Cbo_CompanyId"));
        strCountryId = GmCommonClass.parseZero(request.getParameter("Cbo_CountryId"));

        strPartNumFormat = strPartNum;
        strPartNumFormat = "'".concat(strPartNumFormat).concat(",");
        strPartNumFormat = strPartNumFormat.replaceAll(",", "','");
        strPartNumFormat = strPartNumFormat.substring(0, strPartNumFormat.length() - 2);

        HashMap hmparam = new HashMap();
        hmparam.put("ELEMID", strElemId);
        hmparam.put("TRANSTYPE", strTransType);
        hmparam.put("TRANSID", strTransId);
        hmparam.put("FROMDT", strFromDt);
        hmparam.put("TODT", strToDt);
        hmparam.put("PARTNUMFORMAT", strPartNumFormat);
        hmparam.put("PARTYID", strIdAccount); // partyid
        hmparam.put("COMPID", strCompanyId); // Company id
        hmparam.put("COUNTRYID", strCountryId); // Country id
        hmparam.put("ARCHIVEFL", strArchiveFl);
        hmparam.put("POST_ARCHIVEDDATE", strPostArchivedDate);
        // international posting changes
        hmparam.put("OWNER_CURRENCY", strOwnerCurrency);
        hmparam.put("DIVISION", strDivisionId);
        hmparam.put("COST_PLANT", strInvPlant);
        hmparam.put("OWNER_COMPANY_ID", strOwnerCompanyId);
        resultSet = gmAcct.reportAcctInventoryTrans(hmparam);
        //To get Currency Count
        String strCurrCnt="";  
        if(resultSet!=null)
        {
        	ArrayList alCurrCnt=GmCommonClass.parseNullArrayList((ArrayList)resultSet.getRows());        	  	
         	if (alCurrCnt.size()>0){
         		DynaBean  db = (DynaBean)alCurrCnt.get(0);
         		strCurrCnt=GmCommonClass.parseNull((String)db.get("CUR_CNT").toString());
         	}
        }
       
        request.setAttribute("REPORT", resultSet);
        request.setAttribute("CUR_CNT",strCurrCnt);
        // hmReturn.put("INVPOSTINGS",alReturn);
        alReturn = gmAcct.listAccountElement("");
        hmReturn.put("ELEMENTLIST", alReturn);
        alReturn = gmCommon.getCodeList("TXNTP");
        hmReturn.put("TXNTYPELIST", alReturn);
        hmReturn.put("hInvPlant", strInvPlant);
        hmReturn.put("hOwnerCurrency", strOwnerCurrency);
        hmReturn.put("hDivision", strDivisionId);
        hmReturn.put("hOwnerCompany", strOwnerCompanyId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hElemId", strElemId);
        request.setAttribute("hTxnType", strTransType);
        request.setAttribute("hTransId", strTransId);
        request.setAttribute("hFrmDt", strFromDt);
        request.setAttribute("hToDt", strToDt);
        request.setAttribute("hPartNum", strPartNum);
        request.setAttribute("hCompanyId", strCompanyId);
        request.setAttribute("hCountryId", strCountryId);

        request.setAttribute("IDACCOUNT", strIdAccount);
        request.setAttribute("hAccId", strIdAccount);

        strDispatchTo = strAcctPath.concat("/GmInvPosting.jsp");
      } else if (strAction.equals("Load") && strOpt.equals("COST")) {
        strCheckOpen = "on";
        alReturn = gmCommon.getCodeList("COGTP");
        hmReturn.put("COGSTYPE", alReturn);
        hmReturn.put("ALCOGSSTATUS", gmCommon.getCodeList("COGSA"));

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hCheck", strCheckOpen);
        strDispatchTo = strAcctPath.concat("/GmInvCosting.jsp");
      } else if (strAction.equals("ReloadCost")) {
        alReturn = gmCommon.getCodeList("COGTP");
        hmReturn.put("COGSTYPE", alReturn);
        hmReturn.put("ALCOGSSTATUS", gmCommon.getCodeList("COGSA"));
        strPartNum =
            request.getParameter("Txt_PNum") == null ? "" : request.getParameter("Txt_PNum");
        strCogsType =
            request.getParameter("Cbo_Cogs") == null ? "" : request.getParameter("Cbo_Cogs");
        strFromDt =
            request.getParameter("Txt_FromDate") == null ? "" : request
                .getParameter("Txt_FromDate");
        strToDt =
            request.getParameter("Txt_ToDate") == null ? "" : request.getParameter("Txt_ToDate");
        strCheckOpen =
            request.getParameter("Chk_Open") == null ? "" : request.getParameter("Chk_Open");
        log.debug("strCheckOpenis ............." + strCheckOpen);


        strPartNumFormat = strPartNum;
        strPartNumFormat = "'".concat(strPartNumFormat).concat(",");
        strPartNumFormat = strPartNumFormat.replaceAll(",", "','");
        strPartNumFormat = strPartNumFormat.substring(0, strPartNumFormat.length() - 2);

        HashMap hmInvCosting = new HashMap();
        hmInvCosting.put("PNUM", strPartNumFormat);
        hmInvCosting.put("COST_INV_TYPE", strCogsType);
        hmInvCosting.put("FROMDT", strFromDt);
        hmInvCosting.put("TODT", strToDt);
        hmInvCosting.put("CHECK_OPEN", strCheckOpen);
        hmInvCosting.put("ARCHIVE_FL", strArchiveFl);
        hmInvCosting.put("ARCHIVE_DT", strCostArchivedDate);
        hmInvCosting.put("OWNER_CURRENCY", strOwnerCurrency);
        hmInvCosting.put("DIVISION", strDivisionId);
        hmInvCosting.put("COST_PLANT", strInvPlant);
        hmInvCosting.put("OWNER_COMPANY_ID", strOwnerCompanyId);
        hmInvCosting.put("COGS_STATUS_ID", strCogsStatusId);

        resultSet = gmAcct.reportInvCosting(hmInvCosting);
        //To get Currency Count     
        String strCurrCnt="";  	
        if(resultSet!=null){
    	ArrayList alCurrCnt =  GmCommonClass.parseNullArrayList((ArrayList)resultSet.getRows()) ;	   	
    	if (alCurrCnt.size()>0){
    		DynaBean  db = (DynaBean)alCurrCnt.get(0);
    		strCurrCnt=GmCommonClass.parseNull((String)db.get("CUR_CNT").toString());
    	}
        }
        hmReturn.put("COSTING", alReturn);
        hmReturn.put("hInvPlant", strInvPlant);
        hmReturn.put("hOwnerCurrency", strOwnerCurrency);
        hmReturn.put("hOwnerCompany", strOwnerCompanyId);
        hmReturn.put("hCogsStatus", strCogsStatusId);
        hmReturn.put("hDivision", strDivisionId);
        request.setAttribute("results", resultSet);
        request.setAttribute("CUR_CNT",strCurrCnt);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hPartNum", strPartNum);
        request.setAttribute("hCogsType", strCogsType);
        request.setAttribute("hFrmDt", strFromDt);
        request.setAttribute("hToDt", strToDt);
        request.setAttribute("hCheck", strCheckOpen);
        request.setAttribute("hAction", strAction);

        strDispatchTo = strAcctPath.concat("/GmInvCosting.jsp");
      }

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmInvPostServlet
