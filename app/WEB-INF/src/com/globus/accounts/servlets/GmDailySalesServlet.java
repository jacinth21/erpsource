/*****************************************************************************
 * File : GmDailySalesServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.accounts.beans.GmAcctDashboardBean;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmDailySalesServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = strAcctPath.concat("/GmDailySalesReport.jsp");

    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);

      GmAcctDashboardBean gmAcct = new GmAcctDashboardBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      GmCalenderOperations gmCal = new GmCalenderOperations();
      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmSalesFilters = new HashMap();
      HashMap hmAccess = new HashMap();
      HashMap hmCurrency = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alCompanyCurrency = new ArrayList();
      String strSalesFilterCond = "";
      String strAccessCondition = "";
      String strType = "";
      String strFromDate = "";
      String strToDate = "";
      String strMode = "";
      String strARCurrSymbol = "";
      String strAccCurrName = "";


      hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      String strSessApplCurrFmt = (String) hmCurrency.get("CMPCURRFMT");
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");

      String strUpdFl = "";
      strType =
          request.getParameter("hType") == null ? "ACCT" : (String) request.getParameter("hType");
      strMode = request.getParameter("hMode") == null ? "" : (String) request.getParameter("hMode");
      String strAccountCurrency = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      strAccountCurrency = strAccountCurrency.equals("0") ? strCompanyCurrId : strAccountCurrency;
      String strPartyId = (String) session.getAttribute("strSessPartyId");
      String strApplnDateFmt = strApplDateFmt;
      String strTodaysDate = (String) session.getAttribute("strSessTodaysDate");
      String strMonthBeginDate = "";
      strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrency);
      int currMonth = 0;
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
              "CREATE_BATCH"));
      strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

      /*
       * Calendar cal = new GregorianCalendar(); int year = cal.get(Calendar.YEAR); int month =
       * cal.get(Calendar.MONTH); int day = cal.get(Calendar.DATE); month = month + 1; String
       * strMonth = ""+month; String strDay = ""+day; if (strMonth.length() == 1) { strMonth =
       * "0".concat(strMonth); } if (strDay.length() == 1) { strDay = "0".concat(strDay); } String
       * strYear = ""+year; String strDate = strMonth.concat("/01/"); strFromDate =
       * strDate.concat(strYear); strToDate =
       * strMonth.concat("/").concat(strDay).concat("/").concat(strYear);
       */

      currMonth = gmCal.getCurrentMonth() - 1;
      strMonthBeginDate =
          GmCommonClass.parseNull(gmCal.getAnyDateOfMonth(currMonth, 1, strApplnDateFmt));
      strTodaysDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
      // To get the Access Level information
      strSalesFilterCond =
          GmCommonClass.parseNull(gmSalesDispatchAction.getAccessFilter(request, response, false));
      hmSalesFilters =
          GmCommonClass.parseNullHashMap(gmCommonBean.getSalesFilterLists(strSalesFilterCond));
      strSalesFilterCond =
          GmCommonClass.parseNull(gmSalesDispatchAction.getAccessFilter(request, response, true));
      strAccessCondition = GmCommonClass.parseNull(getAccessCondition(request, response, true));

      if (strType.equals("ACCT")) {
        strFromDate =
            request.getParameter("Txt_FromDate") == null ? strMonthBeginDate : (String) request
                .getParameter("Txt_FromDate");
        strToDate =
            request.getParameter("Txt_ToDate") == null ? strTodaysDate : (String) request
                .getParameter("Txt_ToDate");
        hmParam.put("FROMDATE", strFromDate);
        hmParam.put("TODATE", strToDate);
        hmParam.put("ACESSCONDITION", strAccessCondition);
        hmParam.put("ARCURRSYMBOL", strAccountCurrency);
        hmReturn = gmAcct.acctSalesReport(hmParam);
        strDispatchTo = strAcctPath.concat("/GmSalesOrderReport.jsp");
        request.setAttribute("hFrom", strFromDate);
        request.setAttribute("hTo", strToDate);
        request.setAttribute("ACC_CURR_SYMB", strAccCurrName);

        if (strMode.equals("Excel")) {
          request.setAttribute("hmReturn", hmReturn);
          request.setAttribute("hMode", "Excel");
          response.setContentType("application/vnd.ms-excel");
          response.setHeader("Content-disposition", "attachment;filename=Report-Sales.xls");
          dispatch(strAcctPath.concat("/GmSalesOrderReportExcel.jsp"), request, response);
        }

      }
      alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();
      hmReturn.put("ALCOMPANYCURRENCY", alCompanyCurrency);

      request.setAttribute("hFrom", strFromDate);
      request.setAttribute("hTo", strToDate);

      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hType", strType);

      // ////// Code Added By Rajkumar Jayakumar///////
      ArrayList alRegion = GmCommonClass.getCodeList("ZONE");
      request.setAttribute("alRegion", alRegion);
      // //////// END CODE////////////
      request.setAttribute("UPDFL", strUpdFl);
      // more filter added
      request.setAttribute("hmSalesFilters", hmSalesFilters);
      request.setAttribute("ACC_CURR_ID", strAccountCurrency);

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmDailySalesServlet
