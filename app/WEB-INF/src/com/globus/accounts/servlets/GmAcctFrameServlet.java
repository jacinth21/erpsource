/*****************************************************************************
 * File			 : GmAcctFrameServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;
import com.globus.common.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 


public class GmAcctFrameServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

 
 	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);

		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strServletPath = GmCommonClass.getString("GMSERVLETPATH");
		String strRootPath = GmCommonClass.getString("GMROOT");
		String strDispatchTo = "";
		String strPageToLoad = "";
		String strSubMenu = "";
		String strFrom = "";
		String strId = "";
		String strPO = "";

		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
			
				strFrom = request.getParameter("hFrom");
				log.debug(" hFrom value is " + strFrom);
				
				if (strFrom != null && strFrom.equals("ProjReport"))
				{
					strId = (String)request.getParameter("hId");
					session.setAttribute("strSessProjectId",strId);
					session.setAttribute("hAction","EditLoad");
				}
				else if (strFrom != null && strFrom.equals("AccountReport"))
				{
					strId = (String)request.getParameter("hId");
					session.setAttribute("strSessAccountId",strId);
					session.setAttribute("hAction","EditLoad");
				}
				else if (strFrom != null && strFrom.equals("EditControl") || strFrom != null && strFrom.equals("EditShip"))
				{
					strId = (String)request.getParameter("hId");
					session.setAttribute("strSessOrderId",strId);
					session.setAttribute("hAction",strFrom);
				}
				else if (strFrom != null && strFrom.equals("Dashboard"))
				{
					strId = (String)request.getParameter("hId");
					String strMode = (String)request.getParameter("hMode");
					
					session.setAttribute("strSessOrderId",strId);
					session.setAttribute("hAction",strMode);
					
					
					if (strMode.equals("INV") || strMode.equals("PAY"))
					{
						String strOpt = (String)request.getParameter("hOpt");
						strPO = (String)request.getParameter("hPO");
						session.setAttribute("strSessPO",strPO);
					    session.setAttribute("strSessAccId",strId);
						session.setAttribute("hAction",strMode);
						session.setAttribute("hOpt",strOpt);
						log.debug(" Id and Mode " + strId + "  -- " + strMode + " Opt Val is " + strOpt);
					}
					session.setAttribute("hMode",strMode);
				}
				else if (strFrom != null && strFrom.equals("SetDashboard"))
				{
					strId = (String)request.getParameter("hId");
					session.setAttribute("strSessConsignId",strId);
					session.setAttribute("hAction","EditLoad");
				}
				else if (strFrom != null && strFrom.equals("ItemDashboard"))
				{
					strId = (String)request.getParameter("hId");
					session.setAttribute("strSessConsignId",strId);
					session.setAttribute("hAction","EditControl");
				}
				else if (strFrom != null && strFrom.equals("LoadItemShip"))
				{
					strId = (String)request.getParameter("hId");
					session.setAttribute("strSessConsignId",strId);
					session.setAttribute("hAction","LoadItemShip");
				}
				else if (strFrom != null && strFrom.equals("LoadSetShip"))
				{
					strId = (String)request.getParameter("hId");
					session.setAttribute("strSessConsignId",strId);
					session.setAttribute("hAction","LoadSetShip");
				}
				else if (strFrom != null && strFrom.equals("ReturnsDashboard"))
				{
					strId = (String)request.getParameter("hId");
					session.setAttribute("strSessReturnId",strId);
					session.setAttribute("hAction","CreditReturn");
				}

				strPageToLoad = request.getParameter("hPgToLoad");
				strSubMenu = request.getParameter("hSubMenu");

				strPageToLoad = (strPageToLoad == null)?"Blank":strPageToLoad;
				strSubMenu = (strSubMenu == null)?"Home":strSubMenu;
				
				log.debug(" Page to Load is " + strPageToLoad  + " SubMenu is " + strSubMenu); 
				
				session.setAttribute("strSessPgToLoad",strPageToLoad);
				session.setAttribute("strSessSubMenu",strSubMenu);
								
				gotoPage(strRootPath.concat("/GmAccounts.jsp"),request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of  GmAcctFrameServlet