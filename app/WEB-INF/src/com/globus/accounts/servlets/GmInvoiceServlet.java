/*****************************************************************************
 * File : GmInvoiceServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.beans.GmCreditHoldEngine;
import com.globus.accounts.beans.GmInvoiceBean;
import com.globus.accounts.tax.beans.GmSalesTaxTransBean;
import com.globus.accounts.xml.GmInvoiceXmlInterface;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmParentRepAccountBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmDOBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.custservice.beans.GmSalesCreditBean;
import com.globus.party.beans.GmAddressBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.sales.event.beans.GmEventSetupBean;



public class GmInvoiceServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strAcctPath.concat("/GmInvoiceEdit.jsp");



    try {
      // checkSession(response, session); // Checks if the current session is valid, else
      // redirecting
      // to SessionExpiry page
      instantiate(request, response);
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      String strTodaysDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strFromDate = "";
      String strToDate = "";
      String strCurrency = "";
      String strRepOpenType = "";
      String strDynaValue = "";
      String strAccountTpe = "";
      String strhScreenType = "";
      String strCompanyInfo = "";
      String strSubCompFlag = "";
      String strPaymentTerm = "";
      String strGSTstartFl = "";
      String strOUSDistFl = "";
      // getting the company id
      String strCompanyId = getGmDataStoreVO().getCmpid();
      String strCompanyLocale = "";

      strCompanyLocale = GmCommonClass.getCompanyLocale(strCompanyId);
      // Locale
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
      //getting the JSP name from property
      strDispatchTo = strAcctPath.concat(GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INV_GENERATE_JSP")));
      ArrayList alResults = new ArrayList();
      Calendar cal = new GregorianCalendar();// To set the date
      int month = cal.get(Calendar.MONTH);
      String strCompDateFmt = getGmDataStoreVO().getCmpdfmt();
      String strFromDateSess = GmCalenderOperations.getAnyDateOfMonth(month, 01, strCompDateFmt);// First
      // Date
      // of
      // Month
      // is
      // assigned
      // to
      // strFromDate
      HashMap hmCurrency = new HashMap();
      hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      strCurrency = (String) hmCurrency.get("CMPCURRSMB");
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
      GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
      GmCommonClass gmCommon = new GmCommonClass();
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmSalesCreditBean gmSalesCredit = new GmSalesCreditBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmARBean gmAR = new GmARBean(getGmDataStoreVO());
      GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
      GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
      GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
      GmAddressBean gmAddrBean = new GmAddressBean(getGmDataStoreVO());
      GmCreditHoldEngine gmCreditHoldEngine = new GmCreditHoldEngine(getGmDataStoreVO());
      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
      GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
      GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(getGmDataStoreVO());
      GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());

      GmParentRepAccountBean gmParentRepAccountBean =
          new GmParentRepAccountBean(getGmDataStoreVO());
      HashMap hmCreditHld = new HashMap();
      HashMap hmReturn = new HashMap();
      HashMap hmValues = new HashMap();
      HashMap hmLoadReturn = new HashMap();
      HashMap hmAccess = new HashMap();
      HashMap hmHeaderDtls = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alReturnInvType = new ArrayList();
      ArrayList alInvoiceBatch = new ArrayList();
      ArrayList alInvLayout = new ArrayList();
      ArrayList alRepAccId = new ArrayList();
      ArrayList alPayMode = new ArrayList();
      List alGrpAccAddr = new ArrayList();
      HashMap hmParam = new HashMap();
      HashMap hmDynaGrpAddr = new HashMap();

      String strInvSource = GmCommonClass.parseNull(request.getParameter("Cbo_InvSource"));
      String strFormat = GmCommonClass.parseNull(request.getParameter("Cbo_Action"));
      String strGridSortField = GmCommonClass.parseNull(request.getParameter("orderByField"));
      String strGridSortType = GmCommonClass.parseNull(request.getParameter("gridSortOrder"));
      String strAccGrpVal = GmCommonClass.parseNull(request.getParameter("strAccGrpVal"));
      strFromDate = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
      strToDate = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
      strRepOpenType = GmCommonClass.parseNull(request.getParameter("strRepOpenType"));
      String strScreen = GmCommonClass.parseNull(request.getParameter("hScreen"));
      String strParentFl = GmCommonClass.parseNull(request.getParameter("Chk_ParentFl"));
      strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
      String strAccCurrencyID = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      strAccCurrencyID = strAccCurrencyID.equals("0") ? strCompanyCurrId : strAccCurrencyID;
      String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccCurrencyID);      
      ArrayList alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();
      log.debug("GmInvoiceServlet strInvSource>>>" + strInvSource);
     
      if (request.getParameter("Cbo_InvSource") == null) {
        strInvSource =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE",
                "AR_DEFAULT_TYPE", getGmDataStoreVO().getCmpid()));
      }
      log.debug("GmInvoiceServlet strInvSource1>>>" + strInvSource);
      strParentFl = strParentFl.equals("false") ? "" : strParentFl;
      if (strFromDate.equals("") && strAction.equals("Load")) {// we need to set From date value
                                                               // from session only is Action is
                                                               // Load
        strFromDate = strFromDateSess;
      }
      if (strToDate.equals("") && strAction.equals("Load")) {// we need to set To date value from
                                                             // session only is Action is Load
        strToDate = strTodaysDate;
      }
      String strSessAccId = "";
      String strSessPO = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strPay = "";
      String strInv = "";
      Date dtInvDate = null;
      String strNewPO = "";
      String strReasonforChange = "";
      String strAmount = "";
      String strPayDate = "";
      String strPayMode = "";
      String strDetails = "";
      String strInputVal[] = new String[4];
      String strInput1 = "";
      String strInput2 = "";
      String strInputType = "";
      String strInvType = "";
      String strInvoiceId = "";
      String strInputStr = "";
      String strInvId = "";
      String strAccId = "";
      String strVoidFlag = "";
      String strCustPO = "";
      String strInvAmt = "";
      String strInvoType = "";
      String strSort = "";
      String strSortType = "";
      String strPrnPdf = "";
      String strCountryInvoice = "";
      String strGlobusVATNum = "";
      String strCountryVATNum = "";
      String strNipAcc = "";
      String strRuleID = "1";
      String strReadFl = "";
      String strUpdFl = "";
      String strVoidFl = "";
      String strProcessBatch = "";
      String strAccountCurrSign = "";
      String strDefultCurrSign = "";      
      String addr1 = "", addr2 = "", billadd = "", billcity = "", state = "", country = "", zipcode =
          "";
      String strGrpAccAddr = "";
      int alGrpAccAddrLen = 0;
      int colSize = 0;
      int billaddLen = 0;
      Date dtCurrentDate = null;
      dtCurrentDate = GmCommonClass.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
      dtCurrentDate = GmCommonClass.getLastWorkingDay(dtCurrentDate);

      HashMap hmMultiPay = new HashMap();
      // generate PDF file (Batch) variable declare
      HashMap hmUnlinkPO = new HashMap();
      HashMap hmGenPDFFile = new HashMap();
      HashMap hmSendMail = new HashMap();
      String strPDFParameter = "";
      String strEmailFl = "";
      String strEmailversion = "";
      String year = GmCalenderOperations.getCurrentDate("yyyy");
      String strInvYear = "";
      String strPDFFileName = "";
      String strCSVFileName = "";
      String accessFlag = "";
      String strInvoicePDF = GmCommonClass.parseNull(GmCommonClass.getString("BATCH_INVOICE"));
      Boolean bPDFFileCreated = false;
      Boolean bCSVFileCreated = false;
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      GmResourceBundleBean gmResourceBundleBeanlbl =
          GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmInvoiceEdit",
              strSessCompanyLocale);
      ArrayList alType = new ArrayList();
      ArrayList alDynaVal = new ArrayList();
      // To check the access for Print button of Statement
      hmAccess =
          GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
              "STMNT_PRINT_ACCESS"));
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      request.setAttribute("hAccess", accessFlag);

      alType = GmCommonClass.getCodeList("INVSR", getGmDataStoreVO());
      strAccId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
      if (strAccId.equals("") || strAccId.equals("0")) {
        strAccId = GmCommonClass.parseNull(request.getParameter("hAccId"));
      }
      log.debug("GmInvoiceServlet strAccId>>>" + strAccId);
      log.debug("GmInvoiceServlet alType>>>" + alType);
      // Code added for passing the Currency symbol to the jsp separately instead of appending this
      // symbol to the total outstanding amount.
      strAccountCurrSign =
          GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(GmCommonClass
              .parseNull(GmCommonClass.getAccountAttributeValue(strAccId, "5507"))));
      strDefultCurrSign = (String) hmCurrency.get("CMPCURRSMB");

      if (strAccId.equals("") || strAccId.equals("0") || strAccountCurrSign.equals("")) {
        strAccountCurrSign = strDefultCurrSign;
      }
      // getting the user defined date format (based on currency)
      String strAccountCurrencyDateFmt =
          GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strAccCurrencyID,
              "ACC_CURR_DATEFMT", getGmDataStoreVO().getCmpid()));
      strAccountCurrencyDateFmt =
          strAccountCurrencyDateFmt.equals("") ? strCompDateFmt : strAccountCurrencyDateFmt;
  	//PMT-23755 Default Current Date as Invoice Date in GM Germany
      String strShowCurDate = GmCommonClass.getRuleValue(strCompanyId,"INVOICE_CURRENT_DATE");
      request.setAttribute("INV_CURR_DT", strShowCurDate);
      
      request.setAttribute("ALTYPE", alType);
      request.setAttribute("IDACCOUNT", strAccId);
      request.setAttribute("hMode", strFormat);
      request.setAttribute("hFrom", strFromDate);
      request.setAttribute("hTo", strToDate);
      request.setAttribute("ACC_CURR_DATEFMT", strAccountCurrencyDateFmt);
    

      String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
      String strStatementType = GmCommonClass.parseNull(request.getParameter("hStatementType"));
      String strType = GmCommonClass.parseNull(request.getParameter("hType"));
      String strTotAmt = GmCommonClass.parseNull(request.getParameter("hTotalAmtVal"));
      String strTotOut = GmCommonClass.parseNull(request.getParameter("hTotalOverdue"));
      String strAddBatch = GmCommonClass.parseNull(request.getParameter("hAddBatch"));
      strPrnPdf = GmCommonClass.parseNull((String) request.getAttribute("printPDF"));
      String strSearchAccName = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction =
          strAction.equals("") ? GmCommonClass.parseNull((String) request.getAttribute("hAction"))
              : strAction;
      strAction = (strAction == null || strAction.equals("")) ? "Load" : strAction;

      String strOpt = (String) session.getAttribute("strSessOpt");
      strOpt = (strOpt == null) ? "Load" : strOpt;
      log.debug("GmInvoiceServlet strOpt>>>" + strOpt);
      log.debug("GmInvoiceServlet strAction>>>" + strAction);
      if (strOpt.equals("POSTPAY") || strAction.equals("PAYALL") || strAction.equals("UPDALL")) {
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "POST_PAYMENT_ACCESS"));
        strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        request.setAttribute("PPUPD", strUpdFl);
      }
      if (strAction.equals("PAY") || strAction.equals("UPD")) {
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "INV_UPD_ACCESS"));
        strReadFl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
        strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        strVoidFl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
        request.setAttribute("INVUPDFL", strUpdFl);
      }

      request.setAttribute("INVSOURCE", strInvSource);

      if (strAction.equals("Load")
          && (strOpt.equals("POSTPAY") || strType.equals("Account") || strType.equals("Group"))) {
        if (strType.equals("Group"))// For Statement By Group Report screen, to load group list
        {
          alReturn = gmPartyInfoBean.fetchPartyNameList("7008");
          hmReturn.put("GROUPLIST", alReturn);
          request.setAttribute("GROUPLIST", alReturn);
        } else {
          strAccountTpe =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue("ACCOUNT_TYPE", "PROCESS_CREDIT"));// From
                                                                                                    // Rule
                                                                                                    // We
                                                                                                    // are
                                                                                                    // getting
                                                                                                    // Account
                                                                                                    // Type
                                                                                                    // Value

        }
        alReturnInvType = GmCommonClass.getCodeList("IVCTP");
        hmReturn.put("CODELIST", alReturnInvType);

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        if (strType.equals("Account") || strType.equals("Group")) {
          strDispatchTo = strAcctPath.concat("/GmInvoiceStatementReport.jsp");
        } else {
          strDispatchTo = strAcctPath.concat("/GmInvoiceMultiplePayment.jsp");
        }
      } else if (strAction.equals("INV") || strAction.equals("PAY")) {
        strOpt = request.getParameter("hOpt");
        String strBatchID = "";
        String strCboAction = "";
        String strCboInvLayout = "";
        if (strOpt == null) {
          strOpt = GmCommonClass.parseNull((String) session.getAttribute("hOpt"));
        }

        if (strAction.equals("INV") && strOpt.equals("LOAD")) {
        	 strSessPO = request.getParameter("hPO");
             if (strSessPO == null) {
               strSessPO = (String) session.getAttribute("strSessPO");
             }
             strSessPO = (strSessPO == null) ? "" : strSessPO;
          hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                  "CUS_PO_ACCESS"));
          strReadFl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
          strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          strVoidFl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
          String strAccessFl = gmEventSetupBean.getEventAccess(getGmDataStoreVO().getPartyid(), "EVENT", "OUS_PO_ACCESS");
          request.setAttribute("AccessFlg", strAccessFl);
          strOUSDistFl = GmCommonClass.parseNull(request.getParameter("hOUSDistFl")); 
          log.debug("strOUSDistFl... "+strOUSDistFl);
          request.setAttribute("hOUSDistFl", strOUSDistFl);
          if (!strReadFl.equals("Y")) {
            throw new AppError("", "20685");
          }
          request.setAttribute("UPDFL", strUpdFl);
        
        }

        strSessAccId = request.getParameter("hAccId");

        if (strSessAccId == null) {
          strSessAccId = (String) session.getAttribute("strSessAccId");
        }
        strSessAccId = (strSessAccId == null) ? "" : strSessAccId;

        strSessPO = request.getParameter("hPO");
        if (strSessPO == null) {
          strSessPO = (String) session.getAttribute("strSessPO");
        }
        strSessPO = (strSessPO == null) ? "" : strSessPO;

        // strSessAccId = (String)session.getAttribute("strSessAccId");
        // strSessPO = (String)session.getAttribute("strSessPO");
        strBatchID = GmCommonClass.parseNull(request.getParameter("hBatchID"));
        strCboAction = GmCommonClass.parseZero(request.getParameter("hCboAction"));
        strCboInvLayout = GmCommonClass.parseZero(request.getParameter("hCboInvLayout"));
        log.debug("strBatchID ***** " + strBatchID + "***** " + strAddBatch);
        if (strAddBatch.equals("") && strBatchID.equals("")) {
          strBatchID = gmARBatchBean.getInProgressBatchID(strUserId);
        }
        alInvoiceBatch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("INVGEN"));
        if (strAction.equals("PAY")) {
          alPayMode = gmCommon.getCodeList("PAYME", getGmDataStoreVO());
          alInvLayout = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("INVL"));
          strInvoiceId = request.getParameter("hInv");
          if (strInvoiceId == null || strInvoiceId.equals("")) {
            strInvoiceId =
                request.getParameter("hInvId") == null ? "" : request.getParameter("hInvId");
          }
          String strAccessFl = gmEventSetupBean.getEventAccess(getGmDataStoreVO().getPartyid(), "EVENT", "OUS_PO_ACCESS");
          request.setAttribute("AccessFlg", strAccessFl);
          hmReturn = gmAR.loadInvoiceDetails(strInvoiceId, strAction);
          strInvSource = GmCommonClass.parseNull((String) hmReturn.get("INV_SOURCE"));
            //getting GST start date flag
          strGSTstartFl = gmDOBean.getGSTStartFlag(strInvoiceId,"1201");
          request.setAttribute("strGSTStartFl", strGSTstartFl);
          request.setAttribute("INVSOURCE", strInvSource);
          request.setAttribute("hScreen", strScreen);
          strDispatchTo =
              (strScreen.equals("Layout")) ? strAcctPath.concat("/GmInvoiceLayout.jsp")
                  : strDispatchTo;
        } else
          hmReturn = gmAR.fetchInvoiceDetails(strSessPO, strSessAccId, strAction);
        hmReturn.put("alInvoiceBatch", alInvoiceBatch);
        hmReturn.put("alInvLayout", alInvLayout);
        hmReturn.put("PAYMODEDETAILS", alPayMode);
        session.removeAttribute("hOpt");
        request.setAttribute("hOpt", strOpt);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hAddBatch", strAddBatch);
        request.setAttribute("hBatchID", strBatchID);
        request.setAttribute("hCboAction", strCboAction);
        request.setAttribute("hCboAction", strCboInvLayout);
        request.setAttribute("hmReturn", hmReturn);
        log.debug("GmInvoiceServlet strOpt1>>>" + strOpt);
        log.debug("GmInvoiceServlet hAction1>>>" + strAction);
      } else if (strAction.equals("GEN")) {
        strSessAccId = GmCommonClass.parseNull(request.getParameter("hAccId"));
        strSessPO = GmCommonClass.parseNull(request.getParameter("hPO"));
        strPay = GmCommonClass.parseNull(request.getParameter("hPay"));
        dtInvDate =
            GmCalenderOperations.getDate(
                request.getParameter("Txt_InvDate") == null ? "" : request
                    .getParameter("Txt_InvDate"), strCompDateFmt);
        strInvType = GmCommonClass.parseNull(request.getParameter("hInvType"));
        strEmailFl = GmCommonClass.parseNull(request.getParameter("hEmailFl"));
        strEmailversion = GmCommonClass.parseNull(request.getParameter("hEmailVersion"));
        // to validate sub part price with order item price - france
        strSubCompFlag =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strCompanyId,
                "SUB_PART_VALID", strCompanyId));
        if (strSubCompFlag.equalsIgnoreCase("Y")) {
          gmAR.validateSubPartPrice(strSessPO, strSessAccId, strUserId);
        }
        strInvoiceId =
            gmAR.generateInvoice(strSessPO, strSessAccId, strPay, dtInvDate, strUserId, "ADD",
                strInvType);
        // to call the web service and getting the Tax details.
        GmSalesTaxTransBean gmSalesTaxTransBean = new GmSalesTaxTransBean(getGmDataStoreVO());
        String strTempInvoiceId = "";
        boolean bMultipleInv = false;
        /*
         * For BBA we will be generating multiple Invoice based on the Ship Address for the Order.
         * This piece of code will handle multiple invoices generated for the order and it will
         * create the PDF and send email as well.
         */
        String arInvoice[] = GmCommonClass.StringtoArray(strInvoiceId, ",");
        int size = arInvoice.length;
        if (size > 1) {
          bMultipleInv = true;
          strTempInvoiceId = strInvoiceId;
          request.setAttribute("MULTI_INV", "YES");
          String lblForThisPo =
              GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_FOR_THIS_PO"));
          String lblMultipleInovicesGenerated =
              GmCommonClass.parseNull(gmResourceBundleBeanlbl
                  .getProperty("LBL_MULTIPLE_INVOICES_GENERATED"));
          String lblPrintInvoice =
              GmCommonClass.parseNull(gmResourceBundleBeanlbl
                  .getProperty("LBL_PRINT_INVOICE_MODIFY_ORDER"));
          request.setAttribute("MULTI_INV_MESSAGE", lblForThisPo + strSessPO
              + lblMultipleInovicesGenerated + strInvoiceId + lblPrintInvoice);
        }
        for (int i = 0; i < size; i++) {
          strInvoiceId = arInvoice[i];
          log.debug(" # # strInvoiceId" + strInvoiceId);
          // single invoice(to getting tax if any)
          String strErrorCustPo = gmSalesTaxTransBean.updateSalesTax(strInvoiceId, strUserId);
          if (!strErrorCustPo.equals("")) {
            throw new AppError("", "20667");
          }
          //Adding new condition to generate different invoice paperwork for OUS distributor order
          strOUSDistFl = GmCommonClass.parseNull(request.getParameter("hOUSDistFl")); 
          log.debug("strOUSDistFl... "+strOUSDistFl);
          if(strOUSDistFl.equals("Y")) {         
          hmGenPDFFile.put("INVID", strInvoiceId);
          hmGenPDFFile.put("USERID", strUserId);
          hmGenPDFFile.put("RESOURCE", "GmConsignSetServlet");
          strPDFParameter ="&hAction=PrintVersion&type=savePDFfile&hInvoiceId=" + strInvoiceId + "&hInvoiceType=" + strInvType+"&hUserID="+strUserId;
          }
          //Existing paperwork
          else{
          hmGenPDFFile.put("INVID", strInvoiceId);
          hmGenPDFFile.put("USERID", strUserId);
          hmGenPDFFile.put("RESOURCE", "GmInvoiceInfoServlet");       
          strPDFParameter = "&hAction=Print&type=savePDFfile&hInv=" + strInvoiceId;
          }
          log.debug("strPDFParameter....... "+strPDFParameter);
          hmGenPDFFile.put("PARAMETER", strPDFParameter);
          hmGenPDFFile.put("YEAR", year);
          hmGenPDFFile.put("companyInfo",
              GmCommonClass.parseNull(request.getParameter("companyInfo")));
          // call this existing method to create PDF file
          bPDFFileCreated = gmARBatchBean.saveInvoiceAsPDF(hmGenPDFFile);
          log.debug(" PDF files created --> " + bPDFFileCreated);
          String strGenerateXmlFl =
              GmCommonClass
                  .parseNull(GmCommonClass.getAccountAttributeValue(strSessAccId, "92000"));
          // if Company is creating the XML file
          // 50201 - OBO invoice, To avoid XML generation for OBO invoice type  
          if (strGenerateXmlFl.equals("Y") && !strInvType.equals("50201")) {
            GmInvoiceXmlInterface gmInvoiceXmlInterface =
                (GmInvoiceXmlInterface) GmCommonClass.getSpringBeanClass(
                    "xml/Invoice_Xml_Beans.xml", strCompanyId);

            gmInvoiceXmlInterface.generateInvocieXML(strInvoiceId, strUserId);
          }
          if (strEmailFl.equals("Y")) {
            // to assign the empty values
            strPDFFileName = "";
            strCSVFileName = "";
            HashMap hmAccountParamData = new HashMap();
            hmAccountParamData = gmARBatchBean.fetchAccountParameterDtls(strInvoiceId);
            String strCompanyCode =
                GmCommonClass.parseNull((String) hmAccountParamData.get("COMP_CODE"));
            hmGenPDFFile.put("COMPANY_CODE", strCompanyCode);
            if (strEmailversion.equals("91995") || strEmailversion.equals("91996"))// 91994 PDF,
                                                                                   // 91995 CSV,
                                                                                   // 91996 PDF and
                                                                                   // CSV
            {
              bCSVFileCreated = gmARBatchBean.saveInvoiceAsCSV(hmGenPDFFile);
              log.debug(" CSV files created --> " + bCSVFileCreated);
              strCSVFileName =
                  strInvoicePDF + strCompanyCode + "\\" + year + "\\" + strInvoiceId + ".csv";
            }
            // to send the Email.
            strPDFFileName =
                strInvoicePDF + strCompanyCode + "\\" + year + "\\" + strInvoiceId + ".pdf";
            // get the account parameter details
            hmSendMail.put("INVID", strInvoiceId); // Invoice ID
            hmSendMail.put("PDFFILENAME", strPDFFileName);
            hmSendMail.put("CSVFILENAME", strCSVFileName);
            hmSendMail.putAll(hmAccountParamData);
            gmARBatchBean.sendEmail(hmSendMail);
          }
          // }

          // to check the POs part of the batch - If batch then void the POs to batch
          hmUnlinkPO.put("ORDERID", "");
          hmUnlinkPO.put("CUST_PO", strSessPO);
          hmUnlinkPO.put("ACCID", strSessAccId);
          hmUnlinkPO.put("TYPE", "INVOICED");
          hmUnlinkPO.put("USERID", strUserId);
          // if PO is part of the batch, then unlink the PO.
          gmARBatchBean.saveUnlinkBatchPO(hmUnlinkPO);
        }
        //getting GST start date flag
        strGSTstartFl = gmDOBean.getGSTStartFlag(strInvoiceId,"1201");
        request.setAttribute("strGSTStartFl", strGSTstartFl);


        // If Multiple Invoice is generated, then we do not need this method call, as this will only
        // get the information
        // for the last Invoice .
        if (!bMultipleInv)
          // Changed to new LoadINVDetails
          hmReturn = gmAR.loadInvoiceDetails(strInvoiceId, strAction);
        request.setAttribute("hAction", "PRINT");
        request.setAttribute("hmReturn", hmReturn);
      } else if (strAction.equals("UPD") || strAction.equals("EC")) {
        strSessAccId = request.getParameter("hAccId");
        strSessPO = request.getParameter("hPO");
        strInv = request.getParameter("hInv");
        strInputStr = request.getParameter("hInputStr");

        strAmount = GmCommonClass.parseNull(request.getParameter("Txt_PAmt"));
        strPayDate = GmCommonClass.parseNull(request.getParameter("Txt_PDate"));
        strPayMode = GmCommonClass.parseNull(request.getParameter("Cbo_PayMode"));
        strDetails = GmCommonClass.parseNull(request.getParameter("Txt_Details"));

        hmParam.put("INVID", strInv);
        hmParam.put("COMMENTS", strDetails);
        hmParam.put("USERID", strUserId);

        // hmReturn =
        // gmAR.updateInvoice(strInv,strSessAccId,strSessPO,strAmount,strPayDate,strPayMode,strDetails,strUserId);
        if (strAction.equals("UPD")) {
          hmReturn = gmAR.updateInvoice(strInv, strSessAccId, strSessPO, strUserId, strInputStr);
          request.setAttribute("hAction", "PAY");
        } else if (strAction.equals("EC")) {
          gmAR.saveCommentsOnInvoice(hmParam);
          request.setAttribute("hAction", "EC");
        }

        HashMap hmReturn1 = new HashMap();
        // Changed to new LoadINVDetails
        hmReturn1 = gmAR.loadInvoiceDetails(strInv, strAction);
        //getting GST start date flag
        strGSTstartFl = gmDOBean.getGSTStartFlag(strInv,"1201");
        request.setAttribute("strGSTStartFl", strGSTstartFl);
        request.setAttribute("hmReturn", hmReturn1);
      }
      // *********************************
      // Functionality to update the PO
      // *********************************
      else if (strAction.equals("UPDPO")) {
        strSessAccId = request.getParameter("hAccId");
        strSessPO = request.getParameter("hPO");
        strInv = request.getParameter("hInv");
        strInvYear = gmARBatchBean.getInvoiceYear(strInv);
        strNewPO = request.getParameter("hCustomerPO");
        strReasonforChange = request.getParameter("hReasonForChange");

        // GmAccountingBean gmAccountingBean = new GmAccountingBean();
        // ArrayList alInvoiceSource = gmAccountingBean.fetchInvoiceSource(strInv);//is strInv
        // strInvId?
        log.debug("GmInvoiceServlet--strInv "+strInv);
        log.debug("GmInvoiceServlet--strSessPO "+strSessPO);
        gmAR.updateNewPO(strInv, strSessAccId, strSessPO, strNewPO, strUserId);
        gmCommonBean.saveLog(strInv, strReasonforChange, strUserId, "1201");

        
        HashMap hmCompanyInvoiceDtls = gmInvoiceBean.fetchCompanyInvoiceDtls(strInv);
        String strCompanyCode =
            GmCommonClass.parseNull((String) hmCompanyInvoiceDtls.get("COMP_CODE"));
        strCSVFileName =
            strInvoicePDF + strCompanyCode + "\\" + strInvYear + "\\" + strInv + ".csv";
        strPDFFileName =
            strInvoicePDF + strCompanyCode + "\\" + strInvYear + "\\" + strInv + ".pdf";
        File CSVFileName = new File(strCSVFileName);
        File PDFFileName = new File(strPDFFileName);
        // to generate the PDF/CSV files.
        strPDFParameter = "&hAction=Print&type=savePDFfile&hInv=" + strInv;
        hmGenPDFFile.put("INVID", strInv);
        hmGenPDFFile.put("USERID", strUserId);
        hmGenPDFFile.put("RESOURCE", "GmInvoiceInfoServlet");
        hmGenPDFFile.put("PARAMETER", strPDFParameter);
        hmGenPDFFile.put("YEAR", strInvYear);
        hmGenPDFFile.put("companyInfo",
            GmCommonClass.parseNull(request.getParameter("companyInfo")));
        hmGenPDFFile.put("COMPANY_CODE", strCompanyCode);
        
        log.debug("GmInvoiceServlet--hmGenPDFFile "+hmGenPDFFile);
        // call this existing method to replace existing PDF/CSV file.
        if (CSVFileName.exists()) {
          CSVFileName.delete();
          bCSVFileCreated = gmARBatchBean.saveInvoiceAsCSV(hmGenPDFFile);
        }
        if (PDFFileName.exists()) {
          PDFFileName.delete();
          bPDFFileCreated = gmARBatchBean.saveInvoiceAsPDF(hmGenPDFFile);
        }

        // Function to reload the screen
        HashMap hmReturn1 = new HashMap();
        // Changed to new LoadINVDetails
        hmReturn1 = gmAR.loadInvoiceDetails(strInv, "UPD");
        //getting GST start date flag
        strGSTstartFl = gmDOBean.getGSTStartFlag(strInv,"1201");
        request.setAttribute("strGSTStartFl", strGSTstartFl);
        // hmReturn1 = gmAR.loadInvoiceDetails(strNewPO,strSessAccId,"UPD");
        request.setAttribute("hAction", "PAY");
        request.setAttribute("hmReturn", hmReturn1);
      }
      /*
       * commenting the following code since all the following things moved to
       * GmInvoiceInfoServlet.Java
       * 
       * else if (strAction.equals("Print")) { GmAccountingBean gmAccountingBean = new
       * GmAccountingBean(); strSessAccId = request.getParameter("hAccId"); strSessPO =
       * request.getParameter("hPO"); strInv =
       * GmCommonClass.parseNull((String)request.getParameter("hInv")); strInv = strInv.equals("")?
       * GmCommonClass.parseNull((String)request.getAttribute("hInv")) : strInv; strInvType =
       * GmCommonClass.parseNull(request.getParameter("hInvType")); strVoidFlag =
       * GmCommonClass.parseNull(request.getParameter("hVoidFlag")); //these codes will work for
       * print invoice so comment them for today's demo
       * 
       * hmValues = gmAccountingBean.fetchInvoiceSource(strInv); log.debug(" hashmao " + hmValues);
       * strInvSource = GmCommonClass.parseNull((String)hmValues.get("INVSRC")); strInvType =
       * GmCommonClass.parseNull((String)hmValues.get("INVTYP")); log.debug(" strInvType " +
       * strInvType + " INvoice Source "+strInvSource);
       * 
       * if(strInvSource.equals("50256") || strInvSource.equals("50253")){ //redirect to
       * GmConsignSetServlet with action "PrintVersion" and pass the Invoice Id and Dept ID String
       * strDeptId =GmCommonClass.parseNull((String)session.getAttribute("strSessDeptSeq"));
       * request.setAttribute("hAction" ,"PrintVersion"); request.setAttribute("hInvoiceId"
       * ,strInv); request.setAttribute("hDeptId" ,strDeptId); String strAdress =
       * "/GmConsignSetServlet?hAction=PrintVersion&hInvoiceId="+strInv+"&hInvoiceType="+strInvType;
       * if (strPrnPdf.equals("pdfprint")) { log.debug("strPrnPdf ===="+strPrnPdf);
       * dispatch(strAdress,request, response); //strDispatchTo =
       * "/prodmgmnt/GmProjectsDashboard.jsp"; } else { gotoPage(strAdress, request, response); }
       * 
       * } else { if (strVoidFlag.equals("VoidInvoice")) { strAction = "PrintVoid"; }
       * 
       * hmReturn = new HashMap(); // Changed to new LoadINVDetails hmReturn =
       * gmAR.loadInvoiceDetails(strInv,strAction); request.setAttribute("hmReturn",hmReturn);
       * request.setAttribute("hVoidFlag",strVoidFlag);
       * 
       * //print 'Commercial Invoice' when strInvsource is 50257:Inter-Company and Invoice title in
       * database Invoice, HashMap hmOrderDetails = new HashMap(); hmOrderDetails =
       * (HashMap)hmReturn.get("ORDERDETAILS"); //log.debug("  hmOrderDetails "+hmOrderDetails);
       * String strInvoiceTitle = ""; String strInvLable
       * =GmCommonClass.parseNull((String)hmOrderDetails.get("INVTITLE"));
       * if(strInvSource.equals("50257") && !strInvLable.equalsIgnoreCase("Freight") &&
       * !strInvType.equals("50202")){ strInvoiceTitle = "Commercial Invoice";
       * request.setAttribute("COMMERCIALINV","yes"); }else {
       * strInvoiceTitle=((String)hmOrderDetails
       * .get("INVTITLE"))==null?"Invoice":((String)hmOrderDetails.get("INVTITLE"));
       * request.setAttribute("COMMERCIALINV","no"); }
       * 
       * request.setAttribute("INVOICETITLE",strInvoiceTitle); strCountryInvoice =
       * GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRuleID,"INVOICEFMT"));
       * strGlobusVATNum =
       * GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRuleID,"GLOBUSVAT"));
       * strCountryVATNum =
       * GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRuleID,"COUNTRYVATNUM")); String
       * strAccID =GmCommonClass.parseNull((String)hmOrderDetails.get("ACCID"));
       * log.debug("strAccID   ="+strAccID); strNipAcc =
       * GmCommonClass.parseNull(GmCommonClass.getRuleValue(strAccID,"NIPACC"));
       * request.setAttribute("NIP" ,strNipAcc); request.setAttribute("GLOBUSVATNUM"
       * ,strGlobusVATNum); request.setAttribute("COUNTRYVATNUM" ,strCountryVATNum);
       * 
       * if(!((GmCommonClass.parseNull(GmCommonClass.getString("COUNTRYCODE"))).equalsIgnoreCase("en"
       * ))){ strDispatchTo = strAcctPath.concat(strCountryInvoice); // will call the
       * GmPolandInvoicePrint.jsp }else{ strDispatchTo = strAcctPath.concat("/GmInvoicePrint.jsp");
       * } } }
       */
      else if (strAction.equals("PAYALL") || strAction.equals("UPDALL")
          || strAction.equals("STMNT") || strAction.equals("ACCDET")) {
        strSessAccId = request.getParameter("Cbo_AccId");
        strInvId = GmCommonClass.parseNull(request.getParameter("Txt_InvoiceID"));
        if (strAction.equals("UPDALL")) {
          strInputStr = request.getParameter("hInputStr");
          // date format use method getDateFromStr
          strPayDate = GmCommonClass.parseNull(request.getParameter("Txt_PDate"));
          strPayMode = GmCommonClass.parseNull(request.getParameter("Cbo_PayMode"));
          strDetails = GmCommonClass.parseNull(request.getParameter("Txt_Details"));
          strAmount = GmCommonClass.parseNull(request.getParameter("Txt_PAmt"));

          // We are using for CashApplication following post MultiplePayment method. So changed
          // parameter to HashMap.
          hmMultiPay.put("INPUTSTRING", strInputStr);
          hmMultiPay.put("PAYMENTDT", GmCalenderOperations.getDate(strPayDate, strCompDateFmt));
          hmMultiPay.put("STRPAYMODE", strPayMode);
          hmMultiPay.put("STRDETAILS", strDetails);
          hmMultiPay.put("PAYMENTAMT", strAmount);
          hmMultiPay.put("STRACCID", strSessAccId);
          hmMultiPay.put("USERID", strUserId);
          hmReturn = gmAR.postMultiplePayments(hmMultiPay);
        } else if (strAction.equals("PAYALL") || strAction.equals("STMNT")
            || strAction.equals("ACCDET")) {
          strSort = GmCommonClass.parseNull(request.getParameter("hSort"));
          strSortType = GmCommonClass.parseNull(request.getParameter("hSortAscDesc"));
          strCustPO = GmCommonClass.parseNull(request.getParameter("Txt_CustomerPO"));
          strInvAmt = GmCommonClass.parseNull(request.getParameter("Txt_Invoice_Amount"));
          String[] strInvoiceTypeArr = request.getParameterValues("Chk_GrpChk_Invoice_Type");
          strFormat =
              request.getParameter("Cbo_Action") == null ? "" : request.getParameter("Cbo_Action");
          request.setAttribute("STRPAYMENTDATE", dtCurrentDate);
          // strAccountTpe = GmCommonClass.parseNull(GmCommonClass.getRuleValue("ACCOUNT_TYPE",
          // "PROCESS_CREDIT"));//From Rule We are getting Account Type Value
          if (strFormat.equals("")) {
            strFormat = "OPEN";
          }
          log.debug("strFormat " + strFormat);
          if (strInvoiceTypeArr != null) {
            strInvoType =
                GmCommonClass.parseNull(GmCommonClass.createInputString(strInvoiceTypeArr));
          }

          if (strType.equals("Group") && strAction.equals("STMNT")) {
            RowSetDynaClass rowSetDynaClass = gmAddrBean.fetchAddressInfo(strSessAccId);
            // The below method return the Array list value from RowSetDynaClass.
            alDynaVal = GmCommonClass.convertDynaToArrayList(rowSetDynaClass);
            if (alDynaVal.size() > 0) {
              hmDynaGrpAddr = (HashMap) alDynaVal.get(0);
              addr1 = GmCommonClass.parseNull((String) hmDynaGrpAddr.get("ADDR1"));
              addr2 = GmCommonClass.parseNull((String) hmDynaGrpAddr.get("ADDR2"));
              billcity = GmCommonClass.parseNull((String) hmDynaGrpAddr.get("BILLCITY"));
              zipcode = GmCommonClass.parseNull((String) hmDynaGrpAddr.get("ZIPCODE"));
              strGrpAccAddr =
                  strAccGrpVal + "<BR>" + addr1 + "<BR>" + addr2 + "<BR>" + billcity + "-"
                      + zipcode;
            } else {
              strGrpAccAddr = strAccGrpVal;
            }
            hmParam.put("GROUPACCADD", strGrpAccAddr);
          }
        }

        hmParam.put("ACCID", strSessAccId);
        hmParam.put("INVID", strInvId);
        hmParam.put("CUSTPO", strCustPO);
        hmParam.put("INVAMT", strInvAmt);
        hmParam.put("INVOTYP", strInvoType);
        hmParam.put("FROM", "POSTPAYMENT");
        hmParam.put("SORT", strSort);
        hmParam.put("SORTTYPE", strSortType);
        hmParam.put("FORMAT", strFormat);
        hmParam.put("INVSOURCE", strInvSource);
        hmParam.put("STATUS", "0,1");
        hmHeaderDtls =
            GmCommonClass.parseNullHashMap(gmSalesCredit.fetchInvoiceHeaderDtls(hmParam));

        hmParam.put("hFrom", strFromDate);
        hmParam.put("hTo", strToDate);
        hmParam.put("strType", strType);
        hmParam.put("strCurrency", strCurrency);
        hmParam.put("strSessDtFmt", strAccountCurrencyDateFmt);
        hmParam.put("strGridSortField", strGridSortField);
        hmParam.put("strGridSortType", strGridSortType);
        hmParam.put("strRepOpenType", strRepOpenType);
        hmParam.put("PARENTFL", strParentFl);
        hmParam.put("ACCOUNT_CURR_ID", strAccCurrencyID);
        hmParam.put("DATETYPE", "106461");      // 106491 - Invoice date. It should be filter based on Date Criteria for Statement By Account Report. 
        // If Type of Customer is Dealer (26240213), fetching invoices for dealer
        // else fetching the invoices of Account.
        /*
         * if (strInvSource.equals("26240213")) { alReturn = (ArrayList)
         * gmSalesCredit.loadUnpaidDealerInvoice(hmParam); } else {
         */
        alReturn = (ArrayList) gmSalesCredit.loadUnpaidInvoice(hmParam);
        // }
        request.setAttribute("Chk_ParentFl", strParentFl);

        if (strAction.equals("ACCDET")) {
          GmTemplateUtil templateUtil = new GmTemplateUtil();
          templateUtil.setDataList("alResult", alReturn);
          templateUtil.setDataMap("hOptType", hmParam);

          templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
              "properties.labels.accounts.GmAccGroupReport", strSessCompanyLocale));
          templateUtil.setTemplateSubDir("accounts/templates");
          templateUtil.setTemplateName("GmAccGroupReport.vm");
          request.setAttribute("alResult", templateUtil.generateOutput());
        }

        String strInvoiceStmntJas =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("INV_STATEMENT.INVSTMNTJAS"));
        /*
         * Filter added to find equal Invoice Amount that entered by user
         */
        if (!strInvAmt.equals("")) {
          gmSearchCriteria.addSearchCriteria("INVAMT", strInvAmt, GmSearchCriteria.EQUALS);
          gmSearchCriteria.query(alReturn);
        }

        hmReturn.put("OPENINVOICES", alReturn);

        // alReturn = gmSales.reportAccount();
        strAccountTpe =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue("ACCOUNT_TYPE", "PROCESS_CREDIT"));// From
                                                                                                  // Rule
                                                                                                  // We
                                                                                                  // are
                                                                                                  // getting
                                                                                                  // Account
                                                                                                  // Type
                                                                                                  // Value

        alReturnInvType = GmCommonClass.getCodeList("IVCTP");
        hmReturn.put("CODELIST", alReturnInvType);

        alPayMode = gmCommon.getCodeList("PAYME", getGmDataStoreVO());
        hmReturn.put("PAYMODEDETAILS", alPayMode);

        if (strInvSource.equals("26240213")) {
          strPaymentTerm = gmSalesCredit.getPaymentTermForDelaer(strSessAccId);
        } else {
          strPaymentTerm = gmSalesCredit.getPaymentTerm(strSessAccId);
        }

        request.setAttribute("PAYMENTTERM", strPaymentTerm);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hmParam", hmParam);
        request.setAttribute("hAction", "Reload");
        request.setAttribute("hAccId", strSessAccId);
        request.setAttribute("HEADERDETAILS", hmHeaderDtls);
        request.setAttribute("strType", strType);
        request.setAttribute("strTotAmt", strTotAmt);
        // request.setAttribute("strTotOut",strTotOut);
        request.setAttribute("strAccountCurrSign", strAccCurrName);
        request.setAttribute("hAction", "Reload");
        request.setAttribute("strStatementType", strStatementType);
        request.setAttribute("hAccId", strSessAccId);
        request.setAttribute("INVSTMNTJAS", strInvoiceStmntJas);

        if (strAction.equals("UPDALL") || strAction.equals("PAYALL")) {

          // Making the Show parent account check box as active or inactive after click on load.When
          // we click on left link default it should be Active.
          strParentFl = strParentFl.equals("on") ? "true" : "false";
          request.setAttribute("DEFAULTPARENTFL", strParentFl);

          strDispatchTo = strAcctPath.concat("/GmInvoiceMultiplePayment.jsp");
        } else if (strAction.equals("STMNT")) {

          // Check Account is on Credit Hold. If yes set the msg in request and display in invoice
          // statement.
          hmParam.put("ACCOUNTID", strSessAccId);
          // if show parent account is checked ,all rep accounts related information will going to
          // show.
          if ((!strParentFl.equals("STMTBYGRP")) && (!strInvSource.equals("26240213"))) {
            alRepAccId = gmParentRepAccountBean.fetchParentRepAccountInfo(strSessAccId);
            String strRepAcctId = GmCommonClass.createStringFromListbyKey(alRepAccId, "REPACID");
            request.setAttribute("REPACTID", strRepAcctId);
            request.setAttribute("Chk_ParentFl", strParentFl);

          }



          hmCreditHld = gmCreditHoldEngine.validateCreditHold(hmParam);
          request.setAttribute("CREDITHOLDMSG", hmCreditHld.get("CREDITHOLDMSG"));


          strDispatchTo = strAcctPath.concat("/GmInvoiceStatement.jsp");
        } else if (strAction.equals("ACCDET")) {// To get the values of Group dropdown, 7008:
                                                // Account Group
          // Making the Show parent account check box as active or inactive after click on load.When
          // we click on left link default it should be Active.
          strParentFl = strParentFl.equals("on") ? "true" : "false";

          alReturn = gmPartyInfoBean.fetchPartyNameList("7008");

          strhScreenType = GmCommonClass.parseNull(request.getParameter("hScreenType"));
          hmReturn.put("GROUPLIST", alReturn);
          request.setAttribute("GROUPLIST", alReturn);
          request.setAttribute("HSCREENTYPE", strhScreenType);
          request.setAttribute("Chk_ParentFl", strParentFl);

          strDispatchTo = strAcctPath.concat("/GmInvoiceStatementReport.jsp");
        }
      } else if (strAction.equals("ADD_BATCH")) {
        strSessAccId = GmCommonClass.parseNull(request.getParameter("hAccId"));
        strSessPO = GmCommonClass.parseNull(request.getParameter("hPO"));
        strAddBatch = GmCommonClass.parseNull(request.getParameter("hAddBatch"));
        String strCboActoin = GmCommonClass.parseZero(request.getParameter("Cbo_userBatch"));
        String strBatchID = GmCommonClass.parseNull(request.getParameter("hBatchID"));
        strEmailFl = GmCommonClass.parseNull(request.getParameter("hEmailFl"));
        dtInvDate =
            GmCalenderOperations.getDate(
                GmCommonClass.parseNull(request.getParameter("Txt_InvDate")), strCompDateFmt);
        if (strAddBatch.equals("Y")) {
          String strReDirectUrl = GmCommonClass.parseNull(request.getParameter("hRedirectURL"));
          request.setAttribute("hRedirectURL", strReDirectUrl + "&companyInfo=" + strCompanyInfo);
        } else { // AR Dash board.
          request.setAttribute("hRedirectURL", "\\GmInvoiceServlet?hAction=INV&hOpt=LOAD&hId="
              + strSessAccId + "&hAccId=" + strSessAccId + "&hPO=" + strSessPO + "&hBatchID="
              + strBatchID + "&hCboAction=" + strCboActoin + "&companyInfo=" + strCompanyInfo);
        }
        hmAccess =
            GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                "CUS_PO_ACCESS"));
        strReadFl = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
        strUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        strVoidFl = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
        // based on the Update flag we are show the Generate and Batch button.
        request.setAttribute("UPDFL", strUpdFl);
        // to get the batch id.
        HashMap hmUserBatch = new HashMap();
        hmUserBatch.put("BATCHID", strBatchID);
        hmUserBatch.put("CUST_PO", strSessPO);
        hmUserBatch.put("ACCID", strSessAccId);
        hmUserBatch.put("ACTION", strCboActoin);
        hmUserBatch.put("USERID", strUserId);
        hmUserBatch.put("EMAILFL", strEmailFl);
        hmUserBatch.put("INVDATE", dtInvDate);
        gmARBatchBean.saveOrderBarcodeDtls(hmUserBatch);
        if (strAddBatch.equals("Y")) { // Process Transaction Batch
          strProcessBatch = "Y";
          gotoPage("/gmProcessTrans.do?haction=Batch", request, response);
        } else { // AR Dash board call (After add PO to batch)
          String strSuccessMsg = "";
          alInvoiceBatch = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("INVGEN"));
          hmReturn = gmAR.fetchInvoiceDetails(strSessPO, strSessAccId, strAction);
          hmReturn.put("alInvoiceBatch", alInvoiceBatch);

          // to set the success message.
          if (strCboActoin.equals("18853")) { // Create In Progress batch and Add PO
            strSuccessMsg =
                GmCommonClass.parseNull(gmResourceBundleBeanlbl
                    .getProperty("LBL_BATCH_CREATED_SUCCESSFULLY"));
          } else if (strCboActoin.equals("18854")) { // Add PO to In Progress Batch
            strSuccessMsg =
                GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_PO_ADDED_BATCH"));
          } else if (strCboActoin.equals("18855")) { // Release Batch to Pending Processing
            strSuccessMsg =
                GmCommonClass.parseNull(gmResourceBundleBeanlbl
                    .getProperty("LBL_BATCH_RELEASED_PENDING"));
          }

          session.removeAttribute("hOpt");
          request.setAttribute("hOpt", strOpt);
          request.setAttribute("hAction", "INV");
          request.setAttribute("hAddBatch", strAddBatch);
          request.setAttribute("hBatchID", strBatchID);
          request.setAttribute("hCboAction", strCboActoin);
          request.setAttribute("hSuccessMsg", strSuccessMsg);
          request.setAttribute("hEmailReqFl", strEmailFl);
          request.setAttribute("hmReturn", hmReturn);
        }
      }

      // Void the Invoice
      // Deleted code, Void invoice is handled using GmCommonCancelServlet
      if (!strPrnPdf.equals("pdfprint") && strProcessBatch.equals("")) {
        log.debug("strPrnPdf ====" + strPrnPdf);
        log.debug("strDispatchTo ====" + strDispatchTo);
        // to set the value to request.
        request.setAttribute("ACC_CURR_ID", strAccCurrencyID);
        request.setAttribute("ACC_CURR_SYMB", strAccCurrName);
        request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);
        request.setAttribute("SEARCH_ACC_NAME", strSearchAccName);
        dispatch(strDispatchTo, request, response);
      }
    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      dispatch(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmInvoiceServlet
