/*****************************************************************************
 * File : GmPendingInvoiceServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.beans.GmARBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.servlets.GmServlet;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.corporate.beans.GmDivisionBean;

public class GmPendingInvoiceServlet extends GmServlet {


  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    GmLogger log = GmLogger.getInstance();
    GmARBean gmAR = new GmARBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmSendMail = new HashMap();

    String strComnPath = GmCommonClass.parseNull(GmCommonClass.getString("GMCOMMON"));
    String strAcctPath = GmCommonClass.parseNull(GmCommonClass.getString("GMACCOUNTS"));
    String strInvoicePDF = GmCommonClass.parseNull(GmCommonClass.getString("BATCH_INVOICE"));
    String strEMailSuccessMsg = "";

    String strDispatchTo = "";
    String strAction = "";
    String strMode = "";
    String strTemp = "";
    String strAccountType = "";
    String strCategory = "";

    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
    ArrayList alType = new ArrayList();
    ArrayList alReturnInvType = new ArrayList();
    ArrayList alDateType = new ArrayList();
    ArrayList alCompDivList = new ArrayList(); //PMT-41835
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();// (String)
                                                            // session.getAttribute("strSessApplDateFmt");
    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page

      // GmARBean gmAR = new GmARBean(getGmDataStoreVO());
      alDateType =
          GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("INDTTY", getGmDataStoreVO()));
      request.setAttribute("DATETYPE", alDateType);
      
      //PMT-41835 Division Filter in the Invoice Report
      alCompDivList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmReturn));
      
     
      String strDivision = GmCommonClass.parseNull(request.getParameter("Cbo_Division"));
      hmParam.put("DIVISIONID", strDivision);
      //PMT-41835 Division Filter in the Invoice Report
      
      alType = GmCommonClass.getCodeList("INVSR", getGmDataStoreVO());
      request.setAttribute("ALTYPE", alType);
      // Locale
      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
      String strInvSource = GmCommonClass.parseNull(request.getParameter("Cbo_InvSource"));

      if (request.getParameter("Cbo_InvSource") == null) {
        strInvSource =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE",
                "AR_DEFAULT_TYPE", getGmDataStoreVO().getCmpid()));
        log.debug("getGmDataStoreVO().getCmpid()-------" + getGmDataStoreVO().getCmpid());
      }
      strInvSource = strInvSource.equals("") ? "50255" : strInvSource;

      alReturnInvType = GmCommonClass.getCodeList("IVCTP");
      hmReturn.put("CODELIST", alReturnInvType);

      String strIdAccount =
          request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
      String strSearchAccName = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));

      strAction =
          request.getParameter("hAction") == null ? "1" : (String) request.getParameter("hAction");
      strMode = request.getParameter("hMode") == null ? "" : (String) request.getParameter("hMode");
      log.debug("yyyyyyyyyyyy" + strInvSource);
      hmParam.put("ACCID", strIdAccount);
      hmParam.put("INVSOURCE", strInvSource);
      request.setAttribute("INVSOURCE", strInvSource);
      request.setAttribute("IDACCOUNT", strIdAccount);
      request.setAttribute("hAccId", strIdAccount);
      request.setAttribute("SEARCH_ACC_NAME", strSearchAccName);
      request.setAttribute("ALCOMPDIVLIST",alCompDivList);//PMT-41835
      strTemp = GmCommonClass.parseZero(request.getParameter("Cbo_AccId"));
      hmParam.put("ACCID", strTemp);
      // to set the default status type -"Pending" (1) changed to "All"(0)
      strTemp =
          request.getParameter("Cbo_Type") == null ? "0" : (String) request
              .getParameter("Cbo_Type");
      hmParam.put("TYPE", strTemp);
      request.setAttribute("Cbo_Type", strTemp);

      strTemp = GmCommonClass.parseNull(request.getParameter("Txt_PO"));
      hmParam.put("PO", strTemp);

      strTemp = GmCommonClass.parseNull(request.getParameter("Txt_InvoiceID"));
      hmParam.put("INVOICEID", strTemp);

      strTemp = GmCommonClass.parseNull(request.getParameter("Txt_OrderID"));
      hmParam.put("ORDERID", strTemp);

      strTemp = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
      hmParam.put("FROMDT", getDateFromStr(strTemp));

      strTemp = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));

      String strParentFl = GmCommonClass.parseNull(request.getParameter("Chk_ParentFl"));

      String strDateType = GmCommonClass.parseNull(request.getParameter("Cbo_DateType"));
      hmParam.put("DATETYPE", strDateType);
      request.setAttribute("hDateType", strDateType);

      HashMap hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
      ArrayList alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();

      String strAccountCurrency = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      strAccountCurrency = strAccountCurrency.equals("0") ? strCompanyCurrId : strAccountCurrency;
      String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrency);

      hmParam.put("PARENTFL", strParentFl);

      hmParam.put("TODT", getDateFromStr(strTemp));
      hmParam.put("APPLDATEFMT", strApplDateFmt);
      hmParam.put("ACC_CUR_ID", strAccountCurrency);
      strCategory = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(getGmDataStoreVO().getCmpid(),"SHOW_ACC_CATEGORY"));
      if (strAction.equals("GO")) {
        /*
         * if(strInvSource.equals("26240213")){ hmReturn = gmAR.reportPendingDealerInvoice(hmParam);
         * } else{
         */
    	hmParam.put("CUSTCATEGORY", strCategory);
        hmReturn = gmAR.reportPendingInvoice(hmParam);
        strParentFl = strParentFl.equals("on") ? "true" : "false";
        // }

      }

      request.setAttribute("PARFLAG", strParentFl);
      request.setAttribute("hmParam", hmParam);
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("hAction", strAction);
      request.setAttribute("hMode", strMode);
      request.setAttribute("strCategory", strCategory);
      if (strMode.equals("SendMail")) {
        String strInputStr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
        StringTokenizer stToken = new StringTokenizer(strInputStr, "|");
        String strInvID = "";
        String strPDFFileName = "";
        String strCSVFileName = "";
        String strInvoicedYear = "";
        String strAccountEmails = "";
        String strTokenString = "";
        String strEmailVersion = "";

        while (stToken.hasMoreElements()) {
          strTokenString = stToken.nextToken();
          log.debug("strTokenString::" + strTokenString);
          String[] al = strTokenString.split(",");
          log.debug("al::" + al);
          strInvID = al[0];
          log.debug("strInvID::" + strInvID);
          strEmailVersion = al[1];
          // get the invoiced year
          strInvoicedYear = gmARBatchBean.getInvoiceYear(strInvID);
          HashMap hmAccountParamData = new HashMap();
          hmAccountParamData = gmARBatchBean.fetchAccountParameterDtls(strInvID);
          String strCompanyCode =
              GmCommonClass.parseNull((String) hmAccountParamData.get("COMP_CODE"));
          strPDFFileName =
              strInvoicePDF + strCompanyCode + "\\" + strInvoicedYear + "\\" + strInvID + ".pdf";
          strCSVFileName =
              strInvoicePDF + strCompanyCode + "\\" + strInvoicedYear + "\\" + strInvID + ".csv";


          hmSendMail.put("INVID", strInvID); // Invoice ID
          hmSendMail.put("PDFFILENAME", strPDFFileName);
          hmSendMail.put("CSVFILENAME", strCSVFileName);
          hmSendMail.put("ACCMAILID", strAccountEmails);
          hmSendMail.put("EVERSION", strEmailVersion);
          hmSendMail.putAll(hmAccountParamData);
          gmARBatchBean.sendEmail(hmSendMail);
          strEMailSuccessMsg = "Mail sent successfully for selected Invoices.";
        }
      }
      // to set the value to request.
      request.setAttribute("ACC_CURR_ID", strAccountCurrency);
      request.setAttribute("ACC_CURR_SYMB", strAccCurrName);
      request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);

      if (strMode.equals("Excel")) {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment;filename=Report.xls");
        dispatch(strAcctPath.concat("/GmPendingInvoiceReportExcel.jsp"), request, response);
      } else {
        request.setAttribute("MailSent", strEMailSuccessMsg);
        dispatch(strAcctPath.concat("/GmPendingInvoiceReport.jsp"), request, response);
      }
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmPendingInvoiceServlet
