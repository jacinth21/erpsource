/*****************************************************************************
 * File : GmPaymentListServlet Desc : This Value is used to display Payment Removed and for Pending
 * PO Servlet used for multiple purpose
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmARReportBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.servlets.GmServlet;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.custservice.beans.GmSalesBean;


public class GmPaymentListServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = strAcctPath.concat("/GmInvoiceList.jsp");
    GmCommonClass gmCommon = new GmCommonClass();
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      GmARReportBean gmARReport = new GmARReportBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
      GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
      RowSetDynaClass resultSet = null;
      String strAccountTpe = "";
      String strCreditType = "";
      HashMap hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
      String strAccountCurrency = "";
      ArrayList alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();

      Date dtFrm = getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_FromDate")));
      Date dtTo = getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_ToDate")));
      String strPayType = GmCommonClass.parseNull(request.getParameter("Cbo_PayType"));
      String strAccountId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
      String strPayMode = GmCommonClass.parseNull(request.getParameter("Cbo_PayMode"));
      String strInvSource =
          request.getParameter("Cbo_InvSource") == null ? "" : request
              .getParameter("Cbo_InvSource");
      if (request.getParameter("Cbo_InvSource") == null) {
        strInvSource =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE",
                "AR_DEFAULT_TYPE", getGmDataStoreVO().getCmpid()));
        request.setAttribute("INVSOURCE", strInvSource);
      }
      String strIdAccount =
          request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
      String strFormat =
          request.getParameter("Cbo_Action") == null ? "" : request.getParameter("Cbo_Action");
      request.setAttribute("hAccId", strAccountId);
      ArrayList alType = GmCommonClass.getCodeList("INVSR", getGmDataStoreVO());
      request.setAttribute("ALTYPE", alType);
      HashMap hmFilterReturn = new HashMap();

      ArrayList alCompDivList = new ArrayList();
      String strParentFl = GmCommonClass.parseNull(request.getParameter("Chk_ParentFl"));
      String strDivisionId = GmCommonClass.parseZero(request.getParameter("Cbo_Division"));
      // fetching division dropdown based on company
      alCompDivList =
          GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmFilterReturn));
      request.setAttribute("ALCOMPDIV", alCompDivList);
      request.setAttribute("DIVISIONID", strDivisionId);

      String strUserId = (String) session.getAttribute("strSessUserId");
      String strApplDtFmt = getGmDataStoreVO().getCmpdfmt();
      String strAction = request.getParameter("hAction");
      String strSearchAccName = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));

      ArrayList alCreditHold = new ArrayList();
      alCreditHold = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CRDTYP"));
      log.debug("The ArrayList ***** " + alCreditHold);
      request.setAttribute("ALCREDITTYPE", alCreditHold);

      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      ArrayList alReturn = new ArrayList();

      // Taking Account type from Rules, similar to what we have in the Post Payment screen.
      ArrayList alCollector = new ArrayList();
      alCollector = GmCommonClass.getCodeList("COLLTR");
      request.setAttribute("ALCOLLECTOR", alCollector);
      String strCollectorId = GmCommonClass.parseNull(request.getParameter("Cbo_CollectorId"));
      strAction = (strAction == null) ? (String) session.getAttribute("strSessOpt") : strAction;
      log.debug("strAction  " + strAction);

      // Default to Payment List
      strDispatchTo = strAcctPath.concat("/GmPaymentList.jsp");
      // This is for Payment received list
      strAccountCurrency = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      strAccountCurrency = strAccountCurrency.equals("0") ? strCompanyCurrId : strAccountCurrency;
      String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrency);
      if (strAction.equals("Reload")) {
        strIdAccount =
            request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
        strFormat =
            request.getParameter("Cbo_Action") == null ? "" : request.getParameter("Cbo_Action");
        strCollectorId = GmCommonClass.parseNull(request.getParameter("Cbo_CollectorId"));
        strCreditType = GmCommonClass.parseNull(request.getParameter("Cbo_CreditTypeId"));

        hmFilterReturn.put("FromDate", GmCommonClass.getStringFromDate(dtFrm, strApplDtFmt));
        hmFilterReturn.put("ToDate", GmCommonClass.getStringFromDate(dtTo, strApplDtFmt));
        hmFilterReturn.put("PayType", strPayType);
        hmFilterReturn.put("PayMode", strPayMode);
        hmFilterReturn.put("ACCID", strAccountId);
        hmFilterReturn.put("INVSOURCE", strInvSource);
        hmFilterReturn.put("COLLECTORID", strCollectorId);
        hmFilterReturn.put("CREDITTYPE", strCreditType);
        hmFilterReturn.put("PARENTFL", strParentFl);
        hmFilterReturn.put("ACC_CURR_ID", strAccountCurrency);
        hmFilterReturn.put("COMP_DIV_ID", strDivisionId);
        // If Type of Customer is Dealer (26240013), fetching payment details for dealer
        // else fetching the payment details of Account.
        /*
         * if (strInvSource.equals("26240213")) { resultSet =
         * gmARReport.reportDealerPaymentList(hmFilterReturn); } else {
         */
        resultSet = gmARReport.reportPaymentList(hmFilterReturn);
        // }
        strParentFl = strParentFl.equals("on") ? "true" : "false";
        request.setAttribute("PARFLAG", strParentFl);
        request.setAttribute("hFrom", dtFrm);
        request.setAttribute("hTo", dtTo);
        request.setAttribute("Txt_PayType", strPayType);
        request.setAttribute("Txt_PayMode", strPayMode);
        request.setAttribute("AccId", strAccountId);
        request.setAttribute("INVSOURCE", strInvSource);
        request.setAttribute("Cbo_InvSource", strInvSource);
        request.setAttribute("IDACCOUNT", strIdAccount);
        request.setAttribute("hMode", strFormat);
        request.setAttribute("hAccId", strIdAccount);
        request.setAttribute("COLLECTORID", strCollectorId);
        request.setAttribute("CREDITTYPE", strCreditType);
      }
      // Below is for Pending PO (15, 30 and over)
      else if (strAction.equals("PendingPO")) {
        resultSet = gmARReport.reportPendingPoSummary(strAccountCurrency);
        strDispatchTo = strAcctPath.concat("/GmPendingPOSummary.jsp");
      }
      ArrayList alPayType = gmCommon.getCodeList("INVTP");
      ArrayList alPayMode = gmCommon.getCodeList("PAYME");
      request.setAttribute("PAYTYPE", alPayType);
      request.setAttribute("PAYMODE", alPayMode);
      // to set the value to request.
      request.setAttribute("ACC_CURR_ID", strAccountCurrency);
      request.setAttribute("ACC_CURR_SYMB", strAccCurrName);
      request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);
      request.setAttribute("SEARCH_ACC_NAME", strSearchAccName);
      request.setAttribute("results", resultSet);
      request.setAttribute("hAction", strAction);
      request.setAttribute("COLLECTORID", strCollectorId);
      request.setAttribute("CREDITTYPE", strCreditType);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmInvoiceServlet
