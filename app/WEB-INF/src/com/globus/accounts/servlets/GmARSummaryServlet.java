/*****************************************************************************
 * File : GmPendingInvoiceServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.beans.GmARSummaryBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.it.beans.GmCodeGroupBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.sales.actions.GmSalesDispatchAction;


public class GmARSummaryServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = "";
    String strAction = "";
    String strAccountID = "";
    String strReportType = "";
    String strInvSrc = "";
    String strFilterCondition = "";
    String strAccountTpe = "";
    String strhScreenType = "";
    String strTxnTp = "";
    String strApplDateFmt = "";
    String strInvSource = "";
    String strARReportByDealerFlag = "";
    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);

      strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
      strAction = strAction.equals("") ? "LoadAR" : strAction;
      strReportType = GmCommonClass.parseNull(request.getParameter("reportType"));

      String strCondition = null;
      RowSetDynaClass resultSet = null;
      HashMap hmParam = new HashMap();
      HashMap hmReturn = new HashMap();
      ArrayList alType = new ArrayList();
      ArrayList alReturn = new ArrayList();
      ArrayList alResult = new ArrayList();
      ArrayList alCollector = new ArrayList();
      ArrayList alColumnDtl = new ArrayList();
      ArrayList alCompanyCurrency = new ArrayList();
      ArrayList alCompDivList = new ArrayList();
      ArrayList alCategory = new ArrayList();
      HashMap hmApplnParam = new HashMap();
      HashMap hmCurrency = new HashMap();
      String strGridFile = "GmARSummaryReport.vm";
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

      GmARBean gmAR = new GmARBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
      GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());

      hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());

      strApplDateFmt = GmCommonClass.parseNull(getGmDataStoreVO().getCmpdfmt());

      String strCompanyLocale =
          GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));

      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
      strARReportByDealerFlag =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("AR_REPORT_BY_DEALER"));
      hmApplnParam.put("ARREPORTBYDEALER", strARReportByDealerFlag);
      // If the Dateformat from the VO is null, take it from the Session
      if (strApplDateFmt.equals(""))
        strApplDateFmt = (String) session.getAttribute("strSessApplDateFmt");

      String strSessApplCurrFmt = (String) hmCurrency.get("CMPCURRFMT");
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
      String strAccCurrName = "";

      hmApplnParam.put("SESSDATEFMT", strApplDateFmt);
      hmApplnParam.put("SESSCURRFMT", strSessApplCurrFmt);

      // getting the show parent account check box value and make it as Y ,If it is checked.
      String strParentFl = GmCommonClass.parseNull(request.getParameter("Chk_ParentFl"));
      if (strParentFl.equals("on")) {
        strParentFl = "Y";
        // Based on the parent check selection load the appropriate VM File.
        strGridFile = "GmARSummaryParReport.vm";
      }
      String strAccId = "";

      alType = GmCommonClass.getCodeList("INVSR", getGmDataStoreVO());
      request.setAttribute("ALTYPE", alType);

      alCollector = GmCommonClass.getCodeList("COLLTR");
      request.setAttribute("ALCOLLECTOR", alCollector);
      alCategory = GmCommonClass.parseNullArrayList(new GmCodeGroupBean().getCodeList("CATGRY", "",getGmDataStoreVO().getCmpid()));
      request.setAttribute("ALCATEGORY", alCategory);
      
      String strDivisionId = GmCommonClass.parseZero(request.getParameter("Cbo_Division"));
      // fetching division dropdown values based on company
      alCompDivList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmReturn));
      request.setAttribute("ALCOMPDIV", alCompDivList);
      request.setAttribute("DIVISIONID", strDivisionId);

      GmARSummaryBean gmARSummaryBean = new GmARSummaryBean(getGmDataStoreVO());
      ArrayList alSnapShotDate =
          GmCommonClass.parseNullArrayList(gmARSummaryBean.fetchSnapShotDate());
      request.setAttribute("AL_SNAPSHOT_DATE", alSnapShotDate);
      String strSnapShotDate = GmCommonClass.parseZero(request.getParameter("Cbo_SnapShot"));
      request.setAttribute("AGING_LOCK_ID", strSnapShotDate);

      // Below code is used to fetch GROUP LIST information, 7008:Group
      GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
      ArrayList alAccountGrp = gmPartyInfoBean.fetchPartyNameList("7008");
      hmReturn.put("ACCOUNT_GRP_LIST", alAccountGrp);
      alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();
      hmReturn.put("ALCOMPANYCURRENCY", alCompanyCurrency);
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      HashMap hmSalesFilters = new HashMap();
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
      hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
      request.setAttribute("hmSalesFilters", hmSalesFilters);


      GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
      if (request.getParameter("Cbo_InvSource") == null) {
        strInvSource =
            GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE", "AR_DEFAULT_TYPE",
                getGmDataStoreVO().getCmpid());
      }

      strInvSource =
          request.getParameter("Cbo_InvSource") == null ? "50255" : request
              .getParameter("Cbo_InvSource");
      log.debug("strInvSource " + strInvSource);
      String strIdAccount =
          request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
      String strRegion =
          request.getParameter("hRegnFilter") == null ? "" : request.getParameter("hRegnFilter");
      String strDist =
          request.getParameter("hDistFilter") == null ? "" : request.getParameter("hDistFilter");
      String strDistUnChk =
          request.getParameter("hDistFilterUnChk") == null ? "" : request
              .getParameter("hDistFilterUnChk");
      Date dtPaySince =
          getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_PaySinceDate")));
      log.debug("strRegion " + strRegion + "strDist " + strDist);
      String strCollectorId = GmCommonClass.parseNull(request.getParameter("Cbo_CollectorId"));
      String strCategoryID = GmCommonClass.parseZero(request.getParameter("Cbo_Category"));
      String strGroupId = GmCommonClass.parseNull(request.getParameter("Cbo_AccGrp"));
      String strAccountCurrency = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      strAccountCurrency = strAccountCurrency.equals("0") ? strCompanyCurrId : strAccountCurrency;
      strAccId = request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
      String strInterval = GmCommonClass.parseNull(request.getParameter("Txt_Interval"));
      String strThrough = GmCommonClass.parseNull(request.getParameter("Txt_Through"));
      String strDefaultThrough =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue("DEFAULT_THROUGH", "AR_THROUGHVAL"));
      if (request.getParameter("Cbo_InvSource") == null) {
        strInvSource =
            GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE", "AR_DEFAULT_TYPE",
                getGmDataStoreVO().getCmpid());
      }
      String strSearchAccName = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));

      String strXMLString = gmXMLParserUtility.createXMLString(alReturn);

      request.setAttribute("hmReturn", hmReturn);
      String strCategory = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(getGmDataStoreVO().getCmpid(),"SHOW_ACC_CATEGORY"));
      // code change based on two left links
      if (strAction.equals("LoadAR") || strAction.equals("ReLoadAR")) {
        if (strAction.equals("ReLoadAR")) {
          log.debug("inside LOADAR ");

          strhScreenType = GmCommonClass.parseNull(request.getParameter("hScreenType"));
          log.debug("ScreenType" + strhScreenType);
          hmParam.put("ACCID", strAccId);
          hmParam.put("INVSOURCE", strInvSource);
          hmParam.put("SALESFILTER", strFilterCondition);
          hmParam.put("PAYSINCEDT", GmCommonClass.getStringFromDate(dtPaySince, strApplDateFmt));
          hmParam.put("COLLECTORID", strCollectorId);
          hmParam.put("CATEGORYID", strCategoryID);
          hmParam.put("GROUPID", strGroupId);
          hmParam.put("INTERVAL", strInterval);
          hmParam.put("THROUGH", strThrough);
          hmParam.put("DAFAULTTHROUGH", strDefaultThrough);
          hmParam.put("PARENTCHKFL", strParentFl);
          hmParam.put("COMP_CURR", strAccountCurrency);
          hmParam.put("COMP_DIV_ID", strDivisionId);
          hmParam.put("AGING_LOCK_ID", strSnapShotDate);
          hmParam.put("SUMMARY_FETCH_TYPE", strSnapShotDate.equals("0") ? "CURRENTDATE"
              : "SNAPSHOT");
          hmParam.put("gmDataStoreVO", getGmDataStoreVO());
          // populateIntervalDtl method is used to generate the intervals column for vm file.
          alColumnDtl = GmCommonClass.parseNullArrayList(gmAR.populateIntervalDtl(hmParam));
          
          hmParam.put("CUSTCATEGORY", strCategory);
          if (strReportType.equals("ARByInvoiceDt")) {
            // reportARSummaryByInvDate method is used to generate AR summary detail report based on
            // invoice date
              alResult = gmAR.reportARSummaryByInvDate(hmParam);
          } else {
            // reportARSummaryByDueDate method is used to generate AR summary detail report based on
            // due date        	  
            alResult = gmAR.reportARSummaryByDueDate(hmParam);
          }

          hmApplnParam.put("REPORTTYPE", strReportType);
          hmApplnParam.put("ALCOLUMNDTL", alColumnDtl);
          hmApplnParam.put("PARENTCHKFL", strParentFl);
          hmApplnParam.put("INVSOURCE", strInvSource);
          hmApplnParam.put("CUSTCATEGORY", strCategory);
          
          GmTemplateUtil templateUtil = new GmTemplateUtil();
          templateUtil.setDataList("alResult", alResult);
          strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrency);
          hmApplnParam.put("SESSCURRFMT", strAccCurrName);
          templateUtil.setDataMap("hmApplnParam", hmApplnParam);
          templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
              "properties.labels.accounts.GmARSummaryReport", strSessCompanyLocale));
          templateUtil.setTemplateName(strGridFile);
          templateUtil.setTemplateSubDir("accounts/templates");
          request.setAttribute("alResult",
              GmCommonClass.replaceForXML(templateUtil.generateOutput()));
          strParentFl = strParentFl.equals("Y") ? "true" : "false";
        }
        strDispatchTo = "/GmARSummaryReport.jsp";

        request.setAttribute("PAYSINCEDT", dtPaySince);
        request.setAttribute("INVSOURCE", strInvSource);
        request.setAttribute("IDACCOUNT", strIdAccount);
        request.setAttribute("hAccId", strAccId);
        request.setAttribute("hRegnFilter", strRegion);
        request.setAttribute("hDistFilter", strDist);
        request.setAttribute("hDistFilterUnChk", strDistUnChk);
        request.setAttribute("COLLECTORID", strCollectorId);
        request.setAttribute("CATEGORYID", strCategoryID);
        request.setAttribute("GROUPID", strGroupId);
        request.setAttribute("REPORTTYPE", strReportType);
        request.setAttribute("INTERVALDAYS", strInterval);
        request.setAttribute("THROUGHDAYS", strThrough);
        request.setAttribute("DAFAULTTHROUGH", strDefaultThrough);
        request.setAttribute("COLUMNSIZE", alColumnDtl.size());
        request.setAttribute("HSCREENTYPE", strhScreenType);
        request.setAttribute("PARENTCHKFL", strParentFl);
        request.setAttribute("CUSTCATEGORY", strCategory);
      }
      // Loads Detail Order Information
      if (strAction.equals("LoadARDetail")) {
        HashMap hmARDetail = new HashMap();
        strAccountID = GmCommonClass.parseNull(request.getParameter("hRefid"));
        strReportType = GmCommonClass.parseNull(request.getParameter("hReportType"));
        strInvSource = GmCommonClass.parseNull(request.getParameter("hInvSource"));
        String strFromDay = GmCommonClass.parseNull(request.getParameter("hFromDay"));
        String strToDay = GmCommonClass.parseNull(request.getParameter("hToDay"));
        String strDivId = GmCommonClass.parseNull(request.getParameter("hDivId"));

        // if show parent check box is checked we are going to show all rep accounts that are
        // mapped for
        // parent account.oterwise only that rep account.(This change is for click on D icon in A/R
        // invoice by due date.)
        log.debug("ar summary servlet value for parent fl is " + strParentFl);
        if (strParentFl.equals("true")) {
          strTxnTp = "PARENTACCTID";
        } else {
          strTxnTp = "ACCTID";
        }

        hmARDetail.put("hRefid", strAccountID);
        hmARDetail.put("REPORTTYPE", strReportType);
        hmARDetail.put("INVSOURSE", strInvSource);
        hmARDetail.put("FROMDAY", strFromDay);
        hmARDetail.put("TODAY", strToDay);
        hmARDetail.put("PARENTCHKFL", strParentFl);
        hmARDetail.put("STRTXNTP", strTxnTp);
        hmARDetail.put("DIV_COMP_ID", strDivId);
        hmARDetail.put("SUMMARY_FETCH_TYPE", strSnapShotDate.equals("0") ? "CURRENTDATE"
            : "SNAPSHOT");
        hmARDetail.put("gmDataStoreVO", getGmDataStoreVO());
        hmARDetail.put("AGING_LOCK_ID", strSnapShotDate);
        resultSet = gmAR.reportARDetailByAccount(hmARDetail);

        strDispatchTo = "/GmARDetailByAccountReport.jsp";
      }
      request.setAttribute("ARREPORTBYDEALER", strARReportByDealerFlag);
      request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);
      request.setAttribute("ACC_CURR_ID", strAccountCurrency);
      request.setAttribute("ACC_CURR_SYMB", strAccCurrName);
      request.setAttribute("results", resultSet);
      request.setAttribute("SEARCH_ACC_NAME", strSearchAccName);
      dispatch(strAcctPath.concat(strDispatchTo), request, response);
    }// End of try

    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of Servlet
