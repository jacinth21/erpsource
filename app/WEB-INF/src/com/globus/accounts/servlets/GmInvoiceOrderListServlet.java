/*****************************************************************************
 * File : GmInvoiceOrderListServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmARReportBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.servlets.GmServlet;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.it.beans.GmCodeGroupBean;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmInvoiceOrderListServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = strAcctPath.concat("/GmInvoiceList.jsp");
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.


    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);
      GmARReportBean gmARReport = new GmARReportBean(getGmDataStoreVO());
      GmCommonClass gmCommon = new GmCommonClass();

      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      RowSetDynaClass resultSet = null;
      GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
      GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());

      Date dtFrm = getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_FromDate")));
      Date dtTo = getDateFromStr(GmCommonClass.parseNull(request.getParameter("Txt_ToDate")));
      String strOrderType = "";
      String strLoadScreen = request.getParameter("hLoadScreen");
      String strInvoiceType = "";
      String strAccountId = "";
      String strCallFrom = "";
      String strSuccessMsg = "";
      String strInvoiceIDs = "";
      String strInvRepType = "";
      String strDateType = GmCommonClass.parseNull(request.getParameter("Cbo_DateType"));
      String strOrdDateType = GmCommonClass.parseNull(request.getParameter("Cbo_OrdDateType"));
      String strParentFl = GmCommonClass.parseNull(request.getParameter("Chk_ParentFl"));
      String strReceiveMode = GmCommonClass.parseNull(request.getParameter("Cbo_OrderModeId"));
      String strDivisionId = GmCommonClass.parseZero(request.getParameter("Cbo_Division"));
      String strCategoryID = GmCommonClass.parseZero(request.getParameter("Cbo_Category"));
      String strShowAddlnColumnFl = GmCommonClass.parseNull(request.getParameter("Chk_addnl_clmn_Fl"));
      strShowAddlnColumnFl = strShowAddlnColumnFl.equals("on") ? "true" : "false";
      ArrayList alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();

      ArrayList alOrderModes = new ArrayList();
      ArrayList alOrdMode = new ArrayList();
      ArrayList alReturn = new ArrayList();
      HashMap hmFilterReturn = new HashMap();
      HashMap hmReturn = new HashMap();
      HashMap hmSalesFilters = new HashMap();
      ArrayList alDateType = new ArrayList();
      ArrayList alOrdDateType = new ArrayList();
      ArrayList alCompDivList = new ArrayList();
      ArrayList alCategory = new ArrayList();     
      ArrayList alInvoiceType = new ArrayList();
      ArrayList alInvoiceListType = new ArrayList();      
      String strUserId = (String) session.getAttribute("strSessUserId");
      ArrayList alType = new ArrayList();
      alType = GmCommonClass.getCodeList("INVSR", getGmDataStoreVO());
      request.setAttribute("ALTYPE", alType);
      ArrayList alCollector = new ArrayList();
      alCollector = GmCommonClass.getCodeList("COLLTR");
      request.setAttribute("ALCOLLECTOR", alCollector);
      alCategory =  GmCommonClass.parseNullArrayList(new GmCodeGroupBean().getCodeList("CATGRY", "",getGmDataStoreVO().getCmpid()));
      request.setAttribute("ALCATEGORY", alCategory);
      // Fetching division dropdown based on company
      alCompDivList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmReturn));
      request.setAttribute("ALCOMPDIV", alCompDivList);
      request.setAttribute("DIVISIONID", strDivisionId);
      request.setAttribute("CATEGORYID", strCategoryID);
      String strAction = request.getParameter("hAction");
      String strInvSource =
          request.getParameter("Cbo_InvSource") == null ? "" : request
              .getParameter("Cbo_InvSource");
      String strIdAccount =
          request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
      String strCollectorId = GmCommonClass.parseNull(request.getParameter("Cbo_CollectorId"));
      String strSearchAccName = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));

      String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();

      HashMap hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
      String strAccCurrencyId = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      strAccCurrencyId = strAccCurrencyId.equals("0") ? strCompanyCurrId : strAccCurrencyId;
      String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccCurrencyId);
      ArrayList alCreditHold = new ArrayList();
      alCreditHold = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CRDTYP"));
      log.debug("The ArrayList ***** " + alCreditHold);
      request.setAttribute("ALCREDITTYPE", alCreditHold);

      alOrderModes =
          GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ORDMO", getGmDataStoreVO()));
      alOrdMode =
          GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ORDMOD", getGmDataStoreVO()));
      alOrderModes.addAll(alOrdMode);
      request.setAttribute("ORDERMODES", alOrderModes);

      alDateType =
          GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("INDTTY", getGmDataStoreVO()));   
      alOrdDateType =
              GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ORDTTY","26240690",getGmDataStoreVO()));
      
      


      String strCreditType = GmCommonClass.parseNull(request.getParameter("Cbo_CreditTypeId"));

      String strSalesFilterCond = "";
      String strAccessCondition = "";
      String strModeOfOrder = "";
      ArrayList alOrderType = new ArrayList();
      alOrderType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("ORDACT"));
      request.setAttribute("ALORDERTYPE", alOrderType);

      // To get the Access Level information
      strSalesFilterCond =
          GmCommonClass.parseNull(gmSalesDispatchAction.getAccessFilter(request, response, false));
      hmSalesFilters =
          GmCommonClass.parseNullHashMap(gmCommonBean.getSalesFilterLists(strSalesFilterCond));
      strSalesFilterCond =
          GmCommonClass.parseNull(gmSalesDispatchAction.getAccessFilter(request, response, true));
      strAccessCondition = GmCommonClass.parseNull(getAccessCondition(request, response, true));

      String strCategory = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(getGmDataStoreVO().getCmpid(),"SHOW_ACC_CATEGORY"));
      request.setAttribute("CUSTCATEGORY", strCategory);
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;
      // If LoadScreen from the screen has no value then get the information from the Session
      strLoadScreen =
          (strLoadScreen == null) ? (String) session.getAttribute("strSessOpt") : strLoadScreen;
      log.debug(" Action is " + strAction + " Load Screen value is " + strLoadScreen);

      if (strLoadScreen.equals("LoadOrderList"))
        strDispatchTo = "/GmOrderList.jsp";
      else
        strDispatchTo = "/GmInvoiceOrderList.jsp";

      if (strAction.equals("Reload")) {
        hmFilterReturn.put("FromDate", GmCommonClass.getStringFromDate(dtFrm, strApplnDateFmt));
        hmFilterReturn.put("ToDate", GmCommonClass.getStringFromDate(dtTo, strApplnDateFmt));

        // If load Invoice List then load below screen
        if (strLoadScreen.equals("LoadInvoiceList")) {
          strInvRepType = GmCommonClass.parseNull(request.getParameter("Cbo_InvRepType"));
          strInvoiceType = GmCommonClass.parseNull(request.getParameter("Cbo_InvoiceType"));
          strAccountId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
          strInvSource =
              request.getParameter("Cbo_InvSource") == null ? "" : request
                  .getParameter("Cbo_InvSource");
          strIdAccount =
              request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
          strCallFrom = GmCommonClass.parseNull(request.getParameter("hCallFrom"));
          strSuccessMsg = GmCommonClass.parseNull(request.getParameter("hSuccessMsg"));
          strInvoiceIDs = GmCommonClass.parseNull(request.getParameter("hMemoInvoices"));
          strCollectorId = GmCommonClass.parseNull(request.getParameter("Cbo_CollectorId"));
          strCreditType = GmCommonClass.parseNull(request.getParameter("Cbo_CreditTypeId"));
         
          log.debug(" Account ID is " + strAccountId + " *****" + strParentFl+" strCategoryID====>"+strCategoryID);
          hmFilterReturn.put("INVTYPE", strInvoiceType);
          hmFilterReturn.put("ACCID", strAccountId);
          hmFilterReturn.put("INVSOURCE", strInvSource);
          hmFilterReturn.put("MEMO_INVOICE", strInvoiceIDs);
          hmFilterReturn.put("INVREPTYPE", strInvRepType);
          hmFilterReturn.put("COLLECTORID", strCollectorId);
          hmFilterReturn.put("CREDITTYPE", strCreditType);
          hmFilterReturn.put("CHK_ACTIVEFL", strParentFl);
          hmFilterReturn.put("ACC_CURR_ID", strAccCurrencyId);
          hmFilterReturn.put("DATETYPE", strDateType);
          hmFilterReturn.put("COMP_DIV_ID", strDivisionId);
          hmFilterReturn.put("CATEGORYID", strCategoryID);
          hmFilterReturn.put("CUSTCATEGORY", strCategory);
          hmFilterReturn.put("CHK_SHOW_ADDNL_CLMN_FL",strShowAddlnColumnFl);
          
          // If Type of Customer is Dealer (26240013), fetching invoices for dealer
          // else fetching the invoices of Account.
          /*
           * if (strInvSource.equals("26240213")) { resultSet =
           * gmARReport.reportDealerInvoiceList(hmFilterReturn); } else {
           */
          resultSet = gmARReport.reportInvoiceList(hmFilterReturn);
          // }
          strParentFl = strParentFl.equals("on") ? "true" : "false";
          request.setAttribute("hInvType", strInvoiceType);
          request.setAttribute("hAccId", strAccountId);
          request.setAttribute("hCallFrom", strCallFrom);
          request.setAttribute("hSuccessMsg", strSuccessMsg);
          request.setAttribute("INVREPTYPE", strInvRepType);
          request.setAttribute("COLLECTORID", strCollectorId);
          request.setAttribute("CREDITTYPE", strCreditType);
          request.setAttribute("Chk_ParentFl", strParentFl);

        } else if (strLoadScreen.equals("LoadOrderList")) {
          // To fetch the order type
          strOrderType = GmCommonClass.parseNull(request.getParameter("Cbo_OrderType"));
          strAccountId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
          strCollectorId = GmCommonClass.parseNull(request.getParameter("Cbo_CollectorId"));
          strCreditType = GmCommonClass.parseNull(request.getParameter("Cbo_CreditTypeId"));
          
          hmFilterReturn.put("OrderType", strOrderType);
          hmFilterReturn.put("ACCID", strAccountId);
          hmFilterReturn.put("INVSOURCE", strInvSource);
          hmFilterReturn.put("SALES_CON", strAccessCondition);
          hmFilterReturn.put("COLLECTORID", strCollectorId);
          hmFilterReturn.put("CREDITTYPE", strCreditType);
          hmFilterReturn.put("CHK_ACTIVEFL", strParentFl);
          hmFilterReturn.put("ACC_CURR_ID", strAccCurrencyId);
          hmFilterReturn.put("RECEVIE_MODE", strReceiveMode);
          hmFilterReturn.put("ORDDATETYPE", strOrdDateType);
          hmFilterReturn.put("COMP_DIV_ID", strDivisionId);
          hmFilterReturn.put("CATEGORYID", strCategoryID);
          hmFilterReturn.put("CUSTCATEGORY", strCategory);
          hmFilterReturn.put("CHK_SHOW_ADDNL_CLMN_FL",strShowAddlnColumnFl);

          resultSet = gmARReport.reportOrderList(hmFilterReturn);
          strParentFl = strParentFl.equals("on") ? "true" : "false";
          request.setAttribute("Cbo_OrderType", strOrderType);
          request.setAttribute("hAccId", strAccountId);
          request.setAttribute("COLLECTORID", strCollectorId);
          request.setAttribute("CREDITTYPE", strCreditType);
          request.setAttribute("Chk_ParentFl", strParentFl);
          request.setAttribute("MODEOFORDER", strReceiveMode);
        }
        request.setAttribute("results", resultSet);

      }
      request.setAttribute("Chk_addnl_clmn_Fl", strShowAddlnColumnFl);
      if (strOrderType.equals("null") || strOrderType.equals("")) {
        strOrderType = "103180";
      }
      if (request.getParameter("Cbo_InvSource") == null) {
        strInvSource =
            GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE", "AR_DEFAULT_TYPE",
                getGmDataStoreVO().getCmpid());
      }
      // For Populating Invoice Order List during the Load of Invoice List Report
      alInvoiceType = gmCommon.getCodeList("IVCTP");
      alInvoiceListType = gmCommon.getCodeList("IVLTTP");
      request.setAttribute("INVOICETYPE", alInvoiceType);
      request.setAttribute("INVOICELISTTYPE", alInvoiceListType);
      request.setAttribute("INVSOURCE", strInvSource);
      request.setAttribute("IDACCOUNT", strIdAccount);
      request.setAttribute("SEARCH_ACC_NAME", strSearchAccName);
      request.setAttribute("hAction", strAction);
      request.setAttribute("hLoadScreen", strLoadScreen);
      request.setAttribute("hFrom", dtFrm);
      request.setAttribute("hTo", dtTo);
      request.setAttribute("COLLECTORID", strCollectorId);
      request.setAttribute("Cbo_OrderType", strOrderType);
      request.setAttribute("CREDITTYPE", strCreditType);
      // to set the value to request.
      request.setAttribute("ACC_CURR_ID", strAccCurrencyId);
      request.setAttribute("ACC_CURR_SYMB", strAccCurrName);
      request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);
      request.setAttribute("DATETYPE", alDateType);
      request.setAttribute("hDateType", strDateType);
      request.setAttribute("ORDDATETYPE", alOrdDateType);
      request.setAttribute("hOrdDateType", strOrdDateType);
      // more filter added
      request.setAttribute("hmSalesFilters", hmSalesFilters);
      log.debug(" Load Screen value is " + strLoadScreen);
      strDispatchTo = strAcctPath.concat(strDispatchTo);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmInvoiceServlet
