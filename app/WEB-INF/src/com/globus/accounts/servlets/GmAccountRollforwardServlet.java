/*****************************************************************************
 * File : GmAccountRollforwardServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.accounts.beans.GmAccountingBean;
import com.globus.accounts.beans.GmAccountingReportBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.servlets.GmServlet;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.custservice.beans.GmSalesBean;

public class GmAccountRollforwardServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = "";
    String strAction = "";
    String strMode = "";
    String strType = "";

    String strAccountList = "";
    String strListPlus = "";
    String strFromDt = "";
    String strToDt = "";
    String strJSPName = "";
    String strExcel = "";

    String strInvSource = "";
    String strIdAccount = "";
    String strPlantId = "";
    String strAccountType = "";
    String strArchiveFl = "";
    String strArchivedDate = "";
    String strOwnerCompanyId = "";
    String strOwnerCurrency = "";
    String strExcelDownloadType = "";
    String strTxnCompanyID = "";
    String strDivisionId = "";
    
    try {

      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
      strAction =
          request.getParameter("hAction") == null ? "4" : (String) request.getParameter("hAction");
      strMode = request.getParameter("hMode") == null ? "" : (String) request.getParameter("hMode");
      
      strTxnCompanyID = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
      strArchiveFl = GmCommonClass.parseNull(request.getParameter("Chk_archive_fl"));
      strArchiveFl = strArchiveFl.equals("on") ? "true" : "";
      strArchivedDate = GmCommonClass.getRuleValue("POST_ARCHIVE_DATE", "ARCHIVE");
      strAccountList = GmCommonClass.parseNull(request.getParameter("hAccountList"));
      strListPlus = GmCommonClass.parseNull(request.getParameter("hAccountListPlus"));
      strFromDt = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
      strToDt = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
      strInvSource =
          request.getParameter("Cbo_InvSource") == null ? "" : request
              .getParameter("Cbo_InvSource");
      strIdAccount =
          request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
      strPlantId = GmCommonClass.parseZero(request.getParameter("Cbo_Plant_Id"));
      strOwnerCompanyId = GmCommonClass.parseZero(request.getParameter("Cbo_Owner_Company"));
      strOwnerCurrency = GmCommonClass.parseZero(request.getParameter("Cbo_Owner_Currency"));
  	  strDivisionId = GmCommonClass.parseZero(request.getParameter("cbo_division"));	

      ArrayList alSalesReturn = new ArrayList();
      GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
      alSalesReturn = new ArrayList();
      ArrayList alType = new ArrayList();
      ArrayList alReturnInvType = new ArrayList();
      ArrayList alPlantList = new ArrayList();
      ArrayList alCompDivList = new ArrayList();

      alType = GmCommonClass.getCodeList("INVSR");
      request.setAttribute("ALTYPE", alType);



      // if requested to download the report into Excel Sheet
      strExcel = request.getParameter("hExcel");
      strExcelDownloadType = GmCommonClass.parseNull(request.getParameter("exceltype"));
      
      GmAccountingReportBean gmAcctReport = new GmAccountingReportBean(getGmDataStoreVO());
      GmAccountingBean gmAccountBean = new GmAccountingBean(getGmDataStoreVO());


      HashMap hmReturn = new HashMap();
      HashMap hmRuleGrp = new HashMap();
      ArrayList alReturn = new ArrayList();
      HashMap hmParams = new HashMap();
      alPlantList = GmCommonClass.parseNullArrayList(gmCommonBean.loadPlantCompanyMapping());
      alCompDivList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmReturn));


      hmParams.put("ACCOUNTLIST", strAccountList);
      hmParams.put("FROMDT", strFromDt);
      hmParams.put("TODT", strToDt);

      hmParams.put("PARTYID", strIdAccount); // partyid
      hmParams.put("INVSOURCE", strInvSource); //
      hmParams.put("PLANTID", strPlantId); // Plant id
      hmParams.put("DIVISION", strDivisionId); 
      hmParams.put("ARCHIVEFL", strArchiveFl);
      hmParams.put("OWNER_COMP_ID", strOwnerCompanyId);
      hmParams.put("OWNER_CURR_ID", strOwnerCurrency);
      hmReturn = gmAcctReport.AccountRollForwardReport(hmParams);
      alReturn = gmAccountBean.listAccountElement(strType);
      hmReturn.put("ALPLANT", alPlantList);
      hmReturn.put("ALDIVISION", alCompDivList);
      
      request.setAttribute("hmReturn", hmReturn);
      request.setAttribute("alAccountList", alReturn);
      alReturn = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CURRN"));
      hmReturn.put("ALOWNERCURRENCY", alReturn);
      alReturn = GmCommonClass.parseNullArrayList(gmCommonBean.getParentCompanyList());
      hmReturn.put("ALOWNERCOMPANY", alReturn);

      request.setAttribute("hFrom", strFromDt);
      request.setAttribute("hTo", strToDt);
      request.setAttribute("hElementList", strListPlus);

      request.setAttribute("INVSOURCE", strInvSource);
      request.setAttribute("IDACCOUNT", strIdAccount);
      request.setAttribute("hAccId", strIdAccount);

      request.setAttribute("hPlantId", strPlantId);
      request.setAttribute("hOwnerCompanyId", strOwnerCompanyId);
      request.setAttribute("hOwnerCurrency", strOwnerCurrency);
      request.setAttribute("hDivision", strDivisionId);
      request.setAttribute("ArchivedDate", strArchivedDate);
      request.setAttribute("Chk_archive_fl", strArchiveFl);
      
      log.debug(":GL acc:"+GmCommonClass.parseNull(request.getParameter("Chk_glaccount_fl")));
      request.setAttribute("Chk_glaccount_fl", GmCommonClass.parseNull(request.getParameter("Chk_glaccount_fl")));
      
      alReturnInvType = GmCommonClass.getCodeList("IVCTP");
      hmParams.put("CODELIST", alReturnInvType);


      if (strExcel == null) {
        strJSPName = "/GmAccountRollForwardReport.jsp";
      }
      
      else {
    	  if(strExcelDownloadType.equalsIgnoreCase("list_view")){
    		  strJSPName = "/GmAccountRollForwardListViewExcel.jsp";
    		  response.setHeader("Content-disposition", "attachment;filename=" + "RollForwardListView"+strTxnCompanyID+".xls");
    	  }else{
    		  strJSPName = "/GmAccountRollForwardExcel.jsp";
    		  response.setHeader("Content-disposition", "attachment;filename=" + "RollForward"+strTxnCompanyID+".xls");
    		  //Added strTxnCompanyID in header users to download excel sheet for each company without the need to save the file and 
    		  //close it before downloading the next company data (PMT-51247).
    	  }
    		  
		        response.setContentType("application/vnd.ms-excel");
		        
		        
      }

      dispatch(strAcctPath.concat(strJSPName), request, response);


    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmPendingInvoiceServlet
