/*****************************************************************************
 * File : GmAcctDashBoardServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.beans.GmAcctDashboardBean;
import com.globus.accounts.beans.GmInvoiceBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmAcctDashBoardServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");

    String strDispatchTo = "";
    String strPageToLoad = "";
    String strSubMenu = "";
    String strFrom = "";
    String strId = "";
    String strPO = "";
    String strOUSPOAccess = "";

    RowSetDynaClass resultSet = null;
    RowSetDynaClass rsdSalesReturn = null;

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strAction = request.getParameter("hAction");
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmAcctDashboardBean gmAcct = new GmAcctDashboardBean(getGmDataStoreVO());
      GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
      GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
      GmInvoiceBean	invoiceBean = new GmInvoiceBean(getGmDataStoreVO());
      //To get PO filter access for OUS distributor orders PO
      String strAccessFl = gmEventSetupBean.getEventAccess(getGmDataStoreVO().getPartyid(), "EVENT", "OUS_PO_ACCESS");
      strOUSPOAccess = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("102080", "INV_SOURCE", getGmDataStoreVO().getCmpid()));
      strAccessFl = strAccessFl.equals("Y")?(strOUSPOAccess.equals("50253")?"Y":"N") : "N";
      request.setAttribute("AccessFlg", strAccessFl);
      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
      String strAccountId = "";
      String strScreenType = "";

      HashMap hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");

      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
      ArrayList alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();

      String strAccountCurrency = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      strAccountCurrency = strAccountCurrency.equals("0") ? strCompanyCurrId : strAccountCurrency;
      String strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrency);

      if (strAction.equals("Load")) {
        strAccountId = GmCommonClass.parseNull(request.getParameter("accountID"));
        strScreenType = GmCommonClass.parseNull(request.getParameter("screenType"));
        
        if (strAccessFl.equals("Y")) // to load the OUS pending PO based on the access flag 50253 - ICS
        {
        	hmReturn = invoiceBean.reportOUSDashboard(strAccountCurrency,strAccessFl);
            request.setAttribute("hmReturn", hmReturn);
        }
        else
        {
            hmReturn = gmAcct.reportDashboard(strAccountCurrency);
            request.setAttribute("hmReturn", hmReturn);
        }

        if (strScreenType.equals("")) {
          resultSet = (RowSetDynaClass) hmReturn.get("INVOICE");
        } else if (strScreenType.equals("INV_BATCH")) { // Invoice Batch screen
          hmParam.put("INPUTSTRING", strAccountId);
          hmParam.put("SCREENTYPE", strScreenType);
          resultSet = (RowSetDynaClass) gmARBatchBean.fetchNonBatchPOs(hmParam);
        }
        rsdSalesReturn = (RowSetDynaClass) hmReturn.get("RETURNS");

        request.setAttribute("invoice", resultSet);
        request.setAttribute("SalesReturn", rsdSalesReturn);
        request.setAttribute("screenType", strScreenType);
      } else if (strAction.equals("Add") || strAction.equals("Edit")) {
        String strUserId = (String) session.getAttribute("strSessUserId");

      }
      // to set the value to request.
      request.setAttribute("ACC_CURR_ID", strAccountCurrency);
      request.setAttribute("ACC_CUR_NM", strAccCurrName);
      request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);
      dispatch(strAcctPath.concat("/GmAccountsHome.jsp"), request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmAcctDashBoardServlet
