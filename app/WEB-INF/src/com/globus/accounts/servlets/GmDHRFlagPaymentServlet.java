/*****************************************************************************
 * File : GmDHRFlagPaymentServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmAPBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmVendorBean;

public class GmDHRFlagPaymentServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(true);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = strAcctPath.concat("/GmDHRFlagPayment.jsp");

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      GmCommonClass gmCommon = new GmCommonClass();
      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmAPBean gmAP = new GmAPBean(getGmDataStoreVO());
      RowSetDynaClass resultSet = null;
      GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
      HashMap hmTemp = new HashMap();
      hmTemp.put("CHECKACTIVEFL", "N"); // check for active flag status in vendor list

      ArrayList alCreatedBy = gmLogon.getEmployeeList("300", "A");
      ArrayList alReturn = new ArrayList();
      ArrayList alPoType = new ArrayList();
      ArrayList alInvcStatus = new ArrayList();
      ArrayList alInvcStatusVal = new ArrayList();
      ArrayList alDateType = new ArrayList();

      String strUserId = (String) session.getAttribute("strSessUserId");
      String strDetails = "";
      String strAction = request.getParameter("hAction");

      String strFromDt = "";
      String strToDt = "";
      String strSort = "";
      String strSortType = "";
      String strType = "";
      String strVenId = "";
      String strPOID = "";
      String strPartID = "";
      String strWOID = "";
      String strDHRId = "";
      String strPoType = "";
      String strInvId = "";
      String strPostBy = "";
      String strInvcStatus = "";
      String strInvcStatusVal = "";
      String strOperSign = "";
      String strDateType = "";

      // String strPageInfo = (String)session.getAttribute("strSessOpt"); // Getting PageInfo from
      // Leftmenu jsp. Value should be DHRREPORT

      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      log.debug(" strAction is " + strAction + " stropt is " + strOpt);

      alPoType = gmCommon.getCodeList("POTYP");

      alInvcStatus = gmCommon.getCodeList("INVST");
      alInvcStatusVal = gmCommon.getCodeList("CPRSN");
      alDateType = gmCommon.getCodeList("DTTYP");
      // Get the DHR Type Drop down.
      request.setAttribute("alDHRType", gmCommon.getCodeList("DHRFLT"));

      request.setAttribute("alInvcStatus", alInvcStatus);
      request.setAttribute("alInvcStatusVal", alInvcStatusVal);
      request.setAttribute("alDateType", alDateType);

      request.setAttribute("alPOTYPE", alPoType);
      request.setAttribute("alCreatedBy", alCreatedBy);

      if (strAction.equals("Load")) {
        hmReturn = gmVendor.getVendorList(hmTemp);
        request.setAttribute("hmReturn", hmReturn);
        if (strOpt.equals("DHRREPORT")) {
          strDispatchTo = strAcctPath.concat("/GmDHRFlagPayment.jsp");
        } else if (strOpt.equals("PAYREPORT")) {
          strDispatchTo = strAcctPath.concat("/GmDHRPaymentsList.jsp");
        }
      } else if (strAction.equals("UPDALL") || strAction.equals("Reload")) {
        strFromDt = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
        strToDt = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
        strSort = GmCommonClass.parseNull(request.getParameter("hSort"));
        strSortType = GmCommonClass.parseNull(request.getParameter("hSortAscDesc"));
        strType = GmCommonClass.parseNull(request.getParameter("Cbo_Type"));
        strVenId = GmCommonClass.parseNull(request.getParameter("Cbo_VendorId"));
        strPOID = GmCommonClass.parseNull(request.getParameter("Txt_PORDER"));
        strPartID = GmCommonClass.parseNull(request.getParameter("Txt_PARTID"));
        strWOID = GmCommonClass.parseNull(request.getParameter("Txt_WORDER"));
        strDHRId = GmCommonClass.parseNull(request.getParameter("Txt_DHR"));
        strPoType = GmCommonClass.parseNull(request.getParameter("Cbo_PoType"));

        strInvcStatus = GmCommonClass.parseNull(request.getParameter("Cbo_InvoiceType"));
        strInvcStatusVal = GmCommonClass.parseNull(request.getParameter("Cbo_InvoiceOper"));
        strOperSign = GmCommonClass.parseNull(request.getParameter("hOperatorSign"));

        strDateType = GmCommonClass.parseNull(request.getParameter("Cbo_DateType"));

        if (strSort.equals("")) {
          strSort = "PSLIP";
          strSortType = "2";
        }

        strPartID = strPartID.replaceAll(",", "','"); // replacing the , to ',' so that IN operator
                                                      // works good for multiple part # inputs

        hmParam.put("FROMDT", strFromDt);
        hmParam.put("TODT", strToDt);
        hmParam.put("SORT", strSort);
        hmParam.put("SORTTYPE", strSortType);
        hmParam.put("VENDID", strVenId);
        hmParam.put("PARTID", strPartID);
        hmParam.put("TYPE", strType);
        hmParam.put("POID", strPOID);
        hmParam.put("WOID", strWOID);
        hmParam.put("DHRID", strDHRId);
        hmParam.put("POTYPE", strPoType);
        hmParam.put("PAGE", strOpt);

        hmParam.put("INVCTYPE", strInvcStatus);
        hmParam.put("INVCTYPEVAL", strInvcStatusVal);
        hmParam.put("HOPERATORSIGN", strOperSign);
        hmParam.put("DATETYPE", strDateType);

        if (strAction.equals("UPDALL")) {
          strDetails =
              request.getParameter("hInputStr") == null ? "" : request.getParameter("hInputStr");
          gmAP.postDHRPayments(strDetails, strUserId);
          strAction = "Reload";
        }
        // If the user selected to view vendor information
        hmReturn = gmVendor.getVendorList(hmTemp);
        if (strAction.equals("Reload")) {
          resultSet = (RowSetDynaClass) gmAP.getDHRList(hmParam);
          // hmReturn.put("DHRLIST",resultSet);
          // log.debug("values in DHR List " + resultSet.getRows().iterator().next());
          log.debug("Size in DHR List " + resultSet.getRows().size());
          request.setAttribute("DHRLIST", resultSet);
        }

        strPartID = strPartID.replaceAll("','", ","); // reversing ',' to , so that the input is AS
                                                      // IS and doesnt show the ','

        request.setAttribute("hFromDate", strFromDt);
        request.setAttribute("hToDate", strToDt);
        request.setAttribute("hPartNum", strPartID);
        request.setAttribute("hVendID", strVenId);
        request.setAttribute("hSortAscDesc", strSortType);
        request.setAttribute("hSort", strSort);
        request.setAttribute("Cbo_Type", strType);
        request.setAttribute("hPOId", strPOID);
        request.setAttribute("hWOId", strWOID);
        request.setAttribute("hDHRId", strDHRId);
        request.setAttribute("hPoType", strPoType);
        request.setAttribute("hmReturn", hmReturn);

        request.setAttribute("hInvcType", strInvcStatus);
        request.setAttribute("hInvcTypeVal", strInvcStatusVal);
        request.setAttribute("hDateType", strDateType);
      } else if (strAction.equals("ReloadPayReport")) {
        strVenId = GmCommonClass.parseNull(request.getParameter("Cbo_VendorId"));
        strFromDt = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
        strToDt = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
        strPOID = GmCommonClass.parseNull(request.getParameter("Txt_POId"));
        strType = GmCommonClass.parseNull(request.getParameter("Cbo_Type"));
        strInvId = GmCommonClass.parseNull(request.getParameter("Txt_InvId"));
        strPostBy = GmCommonClass.parseNull(request.getParameter("Cbo_PostBy"));
        Date dtFrmDate = getDateFromStr(strFromDt);
        Date dtToDate = getDateFromStr(strToDt);
        hmParam.put("VENDID", strVenId);
        hmParam.put("FROMDT", strFromDt);
        hmParam.put("TODT", strToDt);
        hmParam.put("POID", strPOID);
        hmParam.put("POTYPE", strType);
        hmParam.put("INVID", strInvId);
        hmParam.put("POSTBY", strPostBy);

        resultSet = gmAP.reportDHRPayments(hmParam);

        hmReturn = gmVendor.getVendorList(hmTemp);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hFrom", dtFrmDate);
        request.setAttribute("hTo", dtToDate);
        request.setAttribute("hAction", strAction);
        request.setAttribute("results", resultSet);
        request.setAttribute("hmParam", hmParam);
        strDispatchTo = strAcctPath.concat("/GmDHRPaymentsList.jsp");
      }
      request.setAttribute("hAction", strAction);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmDHRFlagPaymentServlet
