/*****************************************************************************
 * File : GmCreditMemoServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.accounts.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.tax.beans.GmSalesTaxTransBean;
import com.globus.accounts.xml.GmInvoiceXmlInterface;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.servlets.GmServlet;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.operations.beans.GmOperationsBean;


public class GmCreditMemoServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = strCustPath.concat("/GmConsignReport.jsp");

    String test = "remember that's Joe who did destory here";

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      instantiate(request, response);

      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "Load" : strOpt;
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

      GmCommonClass gmCommon = new GmCommonClass();
      GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
      GmARBean gmARBean = new GmARBean(getGmDataStoreVO());
      GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
      GmResourceBundleBean gmResourceBundleBeanlbl =
          GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmIssueCredit",
              strSessCompanyLocale);
      ArrayList alType = new ArrayList();

      String strAccId = "";
      String strConsignId = "";
      String strInputStr = "";
      String strCreditMemoId = "";
      String strInvoiceId = "";
      String strUserId = "";
      String strComments = "";
      String strTotal = "";
      String strXMLString = "";
      String strAccCurrName = "";
   
      String strVatInputStr = "";
      HashMap hmReturn = new HashMap();
      HashMap hmResult = new HashMap();
      HashMap hmCurrency = new HashMap();
      ArrayList alReturn = new ArrayList();
      ArrayList alCompanyCurrency = new ArrayList();

      log.debug(" Action inside Credit Memo Servlet is " + strAction + " Opt Value is " + strOpt);

      String strStatFlag = "";
      String strVeriFlag = "";
      String strSetId = "";
      String strAccountTpe = "";

      strStatFlag = GmCommonClass.parseNull(request.getParameter("staInputs"));
      strVeriFlag = GmCommonClass.parseNull(request.getParameter("veriFlag"));
      strSetId = GmCommonClass.parseNull(request.getParameter("setID"));
      strConsignId = GmCommonClass.parseNull(request.getParameter("consignID"));
      hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
      String strAccountCurrency = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
      strAccountCurrency = strAccountCurrency.equals("0") ? strCompanyCurrId : strAccountCurrency;
      strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrency);
      alCompanyCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());
      String strSearchAccName = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));

      HashMap hmValues = new HashMap();
      hmValues.put("SFL", strStatFlag);
      hmValues.put("VFL", strVeriFlag);
      hmValues.put("SETID", strSetId);
      hmValues.put("CONID", strConsignId);

      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

      if (strAction.equals("Load") && strOpt.equals("Load")) {
        alReturn = gmOper.getSetBuildDashboard(hmValues);
        hmReturn.put("INISETS", alReturn);
        // hmReturn = gmOper.reportDashboard("1");
        request.setAttribute("hmReturn", hmReturn);
      } else if (strAction.equals("Add") || strAction.equals("Edit")) {

      } else if (strAction.equals("EditLoad")) {
        strConsignId =
            request.getParameter("hId") == null ? "" : (String) request.getParameter("hId");

        hmResult = gmOper.loadSavedSetMaster(strConsignId);
        hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
        hmResult = gmOper.viewBuiltSetDetails(strConsignId);
        hmReturn.put("CONDETAILS", hmResult);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hAction", strAction);
        strDispatchTo = strCustPath.concat("/GmSetPicSlip.jsp");
      }

      else if (strOpt.equals("IssueCredit")) {

        request.setAttribute("hAction", strAction);

        alType = GmCommonClass.getCodeList("INVSR");
        request.setAttribute("ALTYPE", alType);
        strDispatchTo = strAcctPath.concat("/GmIssueCredit.jsp");
      }

      else if (strAction.equals("UPDIssueCredit")) {
        strCreditMemoId = GmCommonClass.parseNull(request.getParameter("Txt_CreditMemo"));
        strAccId = GmCommonClass.parseNull(request.getParameter("Cbo_AccId"));
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
       
        strTotal = GmCommonClass.parseNull(request.getParameter("hTotal"));
        strVatInputStr = GmCommonClass.parseNull(request.getParameter("hVatInputStr"));
        log.debug("strVatInputStr>>>>"+strVatInputStr);
        strComments = GmCommonClass.parseNull(request.getParameter("Txt_Comments"));
        strUserId = (String) session.getAttribute("strSessUserId");

        log.debug(" Acc Id is " + strAccId + " User Id is " + strUserId + " Comments is "
            + strComments);
        log.debug(" Input String is " + strInputStr);
        log.debug(" Total Cost is " + strTotal);
        HashMap hmvatparam = new HashMap();
        hmvatparam.put("ACCID", strAccId);
        hmvatparam.put("INPUTSTRING", strInputStr);
        hmvatparam.put("TOTAL", strTotal);
        hmvatparam.put("USERID", strUserId);
        hmvatparam.put("COMMENTS", strComments);
        hmvatparam.put("VATINPUT", strVatInputStr);
        strInvoiceId =
            gmARBean.issueCredit(hmvatparam);
        // to update the sales Tax
        GmSalesTaxTransBean gmSalesTaxTransBean = new GmSalesTaxTransBean(getGmDataStoreVO());
        String strErrorCustPo = gmSalesTaxTransBean.updateSalesTax(strInvoiceId, strUserId);
        if (!strErrorCustPo.equals("")) {
          throw new AppError("", "20667");
        }
        String strGenerateXmlFl =
            GmCommonClass.parseNull(GmCommonClass.getAccountAttributeValue(strAccId, "92000"));
        // if Company is creating the XML file
        if (strGenerateXmlFl.equals("Y")) {
          GmInvoiceXmlInterface gmInvoiceXmlInterface =
              (GmInvoiceXmlInterface) GmCommonClass.getSpringBeanClass("xml/Invoice_Xml_Beans.xml",
                  getGmDataStoreVO().getCmpid());

          gmInvoiceXmlInterface.generateInvocieXML(strInvoiceId, strUserId);
        }

        alType = GmCommonClass.getCodeList("INVSR");
        request.setAttribute("ALTYPE", alType);

        // request.setAttribute("ACCOUNTLIST", alReturn);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hAccId", strAccId);
        StringBuffer sbQuery = new StringBuffer();
        String strLblCreditMemo =
            GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_NEW_CREDIT_MEMO"));
        String strLblSuccGenerated =
            GmCommonClass.parseNull(gmResourceBundleBeanlbl
                .getProperty("LBL_SUCCESSFULLY_GENERATED"));
        sbQuery.append(" <BR>" + strLblCreditMemo + "<B> ");
        sbQuery.append(" <a href= javascript:fnPrintInvoice('");
        sbQuery.append(strInvoiceId);
        sbQuery.append("'); class='RightText'> ");
        sbQuery.append(strInvoiceId);
        sbQuery.append(" </a> </B>" + strLblSuccGenerated + "<BR> <BR> ");
        request.setAttribute("hMessage", sbQuery.toString());
        strDispatchTo = strAcctPath.concat("/GmIssueCredit.jsp");
      }

      request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);
      request.setAttribute("ACC_CURR_ID", strAccountCurrency);
      request.setAttribute("ACC_CURR_SYMB", strAccCurrName);
      request.setAttribute("SEARCH_ACC_NAME", strSearchAccName);
      dispatch(strDispatchTo, request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmCreditMemoServlet
