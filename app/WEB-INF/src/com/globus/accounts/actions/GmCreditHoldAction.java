package com.globus.accounts.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmCreditHoldBean;
import com.globus.accounts.beans.GmCreditHoldEngine;
import com.globus.accounts.forms.GmCreditHoldForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmSalesBean;


public class GmCreditHoldAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * validateCreditHold - This method is used to validate Account Credit hold by calling AJAX. It
   * will return Credit Message when Account is on Credit hold.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   */
  public ActionForward validateCreditHold(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    String strCreditHoldMsg = "";
    String strAccountId = "";

    instantiate(request, response);


    HashMap hmParam = new HashMap();

    GmCreditHoldEngine gmCreditHoldEngine = new GmCreditHoldEngine(getGmDataStoreVO());


    GmCreditHoldForm gmCreditHoldForm = (GmCreditHoldForm) actionForm;
    gmCreditHoldForm.loadSessionParameters(request);

    hmParam = GmCommonClass.getHashMapFromForm(gmCreditHoldForm);
    strAccountId = gmCreditHoldForm.getAccountId();
    hmParam = gmCreditHoldEngine.validateCreditHold(hmParam);
    strCreditHoldMsg = gmCreditHoldEngine.createCreditMessage(hmParam);

    if (!strAccountId.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strCreditHoldMsg);
      pw.flush();
      return null;
    }

    return actionMapping.findForward("GmCreditHold");

  }

  /**
   * fetchAcctCreditHold - This method is used to fetch Account Credit hold by calling AJAX. It will
   * return Credit Message when Account is on Credit hold.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   */
  public ActionForward fetchAcctCreditHold(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCreditHoldBean gmCreditHoldBean = new GmCreditHoldBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();

    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alResult = new ArrayList();

    String strAccountID = "";
    String strJSONString = "";
    String strOpt = "";
    String strUserId = "";

    GmCreditHoldForm gmCreditHoldForm = (GmCreditHoldForm) form;
    gmCreditHoldForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmCreditHoldForm);

    strOpt = gmCreditHoldForm.getStrOpt();
    strAccountID = GmCommonClass.parseNull(gmCreditHoldForm.getAccountId());
    strUserId = GmCommonClass.parseNull(gmCreditHoldForm.getUserId());

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
            "CREDIT_HOLD_SETUP"));
    gmCreditHoldForm.setSubmitAccFl(GmCommonClass.parseNull((String) hmAccess.get("UPDFL")));

    if (strOpt.equals("reload")) {
      alResult = gmCreditHoldBean.fetchAcctCreditHold(hmParam);
      if (alResult.size() > 0) {
        strJSONString = gmWSUtil.parseHashMapToJson((HashMap) alResult.get(0));
      }
      log.debug(strJSONString);
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSONString);
      pw.flush();
      return null;

    }

    gmCreditHoldForm.setAlCreditHoldList(GmCommonClass.getCodeList("CRDTYP"));
    gmCreditHoldForm.setAlARSummaryList(GmCommonClass.getCodeList("ARSMRY"));
    gmCreditHoldForm.setAlAccountsList(gmSales.reportAccount());
    gmCreditHoldForm.setAlLogReasons(gmCommonBean.getLog(strAccountID, "400915"));

    return mapping.findForward("GmCreditHoldSetup");
  }

  /**
   * fetchAcctPaymentInfo - This method is used to fetch Account payment info calling AJAX.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   */

  public ActionForward fetchAcctPaymentInfo(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);

    GmCreditHoldBean gmCreditHoldBean = new GmCreditHoldBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmCreditHoldForm gmCreditHoldForm = (GmCreditHoldForm) form;

    HashMap hmReturn = new HashMap();

    String strAccountID = "";
    String strJSONString = "";

    gmCreditHoldForm.loadSessionParameters(request);
    strAccountID = GmCommonClass.parseNull(gmCreditHoldForm.getAccountId());

    hmReturn = gmCreditHoldBean.fetchAcctLastPaymentInfo(strAccountID);
    if (hmReturn.size() > 0) {

      strJSONString = gmWSUtil.parseHashMapToJson(hmReturn);
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSONString);
      pw.flush();
      return null;
    }
    return mapping.findForward("GmCreditHoldSetup");
  }

  /**
   * saveAcctCreditHold - This method is used to save the credit hold.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   */
  public ActionForward saveAcctCreditHold(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    String strAcctId = "";
    instantiate(request, response);
    HashMap hmParam = new HashMap();
    GmCreditHoldBean gmCreditHoldBean = new GmCreditHoldBean(getGmDataStoreVO());
    GmCreditHoldForm gmCreditHoldForm = (GmCreditHoldForm) form;
    gmCreditHoldForm.loadSessionParameters(request);
    strAcctId = GmCommonClass.parseNull(gmCreditHoldForm.getAccountId());

    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmCreditHoldForm));
    // saveFileAttributes is used to save uploaded files details
    gmCreditHoldBean.saveAcctCreditHold(hmParam);

    gmCreditHoldBean.sendCreditHoldNotifyEmail(hmParam);
    String strURL =
        "/gmCreditHold.do?method=fetchAcctCreditHold&strOpt=load&accountId=" + strAcctId;

    return actionRedirect(strURL, request);
  }

  /**
   * fetchCreditHoldType - This method is used to fetch the credit hold type by calling AJAX.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   */

  public ActionForward fetchCreditHoldTypeDetail(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    HashMap hmParam = new HashMap();
    ArrayList alSubTypeValue = new ArrayList();
    ArrayList alSubTypeCtrl = new ArrayList();
    String strCreditHoldType = "";
    instantiate(request, response);
    GmCreditHoldBean gmCreditHoldBean = new GmCreditHoldBean(getGmDataStoreVO());



    GmCreditHoldForm gmCreditHoldForm = (GmCreditHoldForm) form;
    gmCreditHoldForm.loadSessionParameters(request);

    hmParam = GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmCreditHoldForm));
    strCreditHoldType = GmCommonClass.parseNull((String) hmParam.get("CREDITHOLDTYPE"));

    alSubTypeCtrl =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CRSBTY", strCreditHoldType));
    alSubTypeValue = gmCreditHoldBean.fetchCreditHoldDtl(hmParam);

    gmCreditHoldForm.setAlCreditHoldList(GmCommonClass.getCodeList("CRDTYP"));
    gmCreditHoldForm.setAlSubTypeValue(alSubTypeValue);
    gmCreditHoldForm.setAlSubTypeCtrl(alSubTypeCtrl);

    return mapping.findForward("GmCreditHoldType");

  }

  /**
   * fetchCommentLog - This method is used to fetch comment log by calling AJAX.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   */
  public ActionForward fetchCommentLog(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    String strCommentLog = "";
    String strAccountID = "";
    instantiate(request, response);
    ArrayList alCommentLog = new ArrayList();
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();

    GmCreditHoldForm gmCreditHoldForm = (GmCreditHoldForm) form;
    gmCreditHoldForm.loadSessionParameters(request);

    strAccountID = GmCommonClass.parseNull(gmCreditHoldForm.getAccountId());

    log.debug("strAccountID=" + strAccountID);
    alCommentLog = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strAccountID, "400915"));
    strCommentLog = gmWSUtil.parseArrayListToJson(alCommentLog);

    if (!strAccountID.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strCommentLog);
      pw.flush();
      return null;
    }

    return actionMapping.findForward("GmCreditHold");

  }
}
