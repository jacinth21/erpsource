// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\accounts\\physicalaudit\\actions\\GmLockTagAction.java

package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.fieldaudit.beans.GmFAReconciliationBean;
import com.globus.accounts.forms.GmFieldSalesConAdjForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmFieldSalesConAdjAction extends GmAction {
  // private String strOpt = LOAD / RELOAD / SAVE;

  /**
   * @return org.apache.struts.action.ActionForward
   * @roseuid 48D1292D0247
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmFieldSalesConAdjForm gmFieldSalesConAdjForm = (GmFieldSalesConAdjForm) form;
    gmFieldSalesConAdjForm.loadSessionParameters(request);

    GmFAReconciliationBean gmFAReconciliationBean = new GmFAReconciliationBean(getGmDataStoreVO());
    GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    Logger log = GmLogger.getInstance(this.getClass().getName());
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmFieldSalesConAdjForm);
    String strOpt = gmFieldSalesConAdjForm.getStrOpt();
    ArrayList alDistList = new ArrayList();
    ArrayList alSetList = new ArrayList();
    ArrayList alAdjustmentType = new ArrayList();
    String strTxnIds = "";
    String strPartInputStr = "";
    String strMsg = "";
    String strURL = "/gmFieldSalesConAdj.do";
    String strDisplayNm = "Consignment Adjustment Screen";
    alDistList = gmCustomerBean.getDistributorList("");
    try {
      HashMap hmValues = new HashMap();
      // To populate values of the Consignment SetID drop down
      hmValues.put("SETTYPE", "4070");
      hmValues.put("TYPE", "1601");
      alSetList = gmProj.loadSetMap(hmValues);

      alAdjustmentType = GmCommonClass.getCodeList("CNADJM");

      if (strOpt.equals("save")) {

        request.setAttribute("hRedirectURL", strURL);
        request.setAttribute("hDisplayNm", strDisplayNm);
        strTxnIds = gmFAReconciliationBean.saveConsignAdjust(hmParam);
        strMsg = "The following Transaction ID created: <b> " + strTxnIds + "</b>";
        throw new AppError(strMsg, "", 'S');
      }

      gmFieldSalesConAdjForm.setAlAjustmentType(alAdjustmentType);
      gmFieldSalesConAdjForm.setAlConsignTo(alDistList);
      gmFieldSalesConAdjForm.setAlSetList(alSetList);

    } catch (AppError appError) {
      request.setAttribute("hType", appError.getType());
      throw appError;
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("GmFieldSalesConAdj");
  }

}
