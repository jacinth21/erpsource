package com.globus.accounts.actions;

import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.forms.GmCashApplicationForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;


public class GmCashApplicationAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code

  /**
   * loadCashAppDtls
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws ParseException
   */

  public ActionForward loadCashAppDtls(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ParseException,
      Exception {

    instantiate(request, response);
    GmCashApplicationForm gmCashApplicationForm = (GmCashApplicationForm) actionForm;
    gmCashApplicationForm.loadSessionParameters(request);
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    ArrayList alPaymentMode = new ArrayList();
    String strPaymentDt = "";
    String strApplDateFmt = "";
    Date dtCurrentDate = null;
    String strAccCurrName = "";
    String strARCurrSymbol = "";
    String strCompanyCurrId = "";
    String strARReportByDealerFlag = "";
    HashMap hmCurrency = new HashMap();
    ArrayList alCompanyCurrency = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmCashApplicationForm);
    alCompanyCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());
    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    strCompanyCurrId = GmCommonClass.parseNull((String) hmCurrency.get("TXNCURRENCYID"));
    strARCurrSymbol = GmCommonClass.parseZero((String) hmParam.get("STRCOMPCURRENCY"));
    strARCurrSymbol = strARCurrSymbol.equals("0") ? strCompanyCurrId : strARCurrSymbol;
    strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strARCurrSymbol);
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strCompanyLocale =
        GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));
    
    GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);   
    strARReportByDealerFlag = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("AR_REPORT_BY_DEALER"));

    alPaymentMode = GmCommonClass.parseNullArrayList((GmCommonClass.getCodeList("PAYME",getGmDataStoreVO())));
    gmCashApplicationForm.setAlPaymentMode(alPaymentMode);
    // set the payment date as last working days

    strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    String strCurrentDate = GmCalenderOperations.getCurrentDate(getGmDataStoreVO().getCmpdfmt());
    dtCurrentDate = GmCommonClass.getStringToDate(strCurrentDate, strApplDateFmt);
    dtCurrentDate = GmCommonClass.getLastWorkingDay(dtCurrentDate);

    strPaymentDt =
        GmCommonClass.parseNull(GmCommonClass.getStringFromDate(dtCurrentDate, strApplDateFmt));
    // to set the payment date to form
    gmCashApplicationForm.setPaymentDt(strPaymentDt);
    hmParam.put("ARREPORTBYDEALER", strARReportByDealerFlag);
    hmParam.put("ALPAYMENTMODE", alPaymentMode);
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    log.debug("alPaymentMode --> " + alPaymentMode.size());
    gmCashApplicationForm.setAlCompCurrency(alCompanyCurrency);
    gmCashApplicationForm.setStrCompCurrency(strARCurrSymbol);
    gmCashApplicationForm.setStrCompCurrName(strAccCurrName);
    gmCashApplicationForm.setStrARRptByDealerFlag(strARReportByDealerFlag);
    String strXmlData = getXmlGridData(alPaymentMode, hmParam);
    gmCashApplicationForm.setGridData(strXmlData);
    return actionMapping.findForward("GmCashApplication");
  }

  /**
   * @getXmlGridData xml content generation for Field Sales lock grid
   * @author
   * @param array list and hash map
   * @return String
   */

  private String getXmlGridData(ArrayList alResult, HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    ArrayList alPaymentMode = new ArrayList();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    alPaymentMode = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALPAYMENTMODE"));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDropDownMaster("alPaymentMode", alPaymentMode);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.GmCashApplication", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("accounts/templates");
    templateUtil.setTemplateName("GmCashApplication.vm");

    return templateUtil.generateOutput();
  }

  /**
   * fetchInvDtls
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchInvoiceDtls(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmCashApplicationForm gmCashApplicationForm = (GmCashApplicationForm) form;
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
    gmCashApplicationForm.loadSessionParameters(request);
    GmARBean gmARBean = new GmARBean(getGmDataStoreVO());
    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmCashApplicationForm);

    ArrayList alInvDetails = gmARBean.getInvoiceDtls(hmParam);
    String strXMLString = GmCommonClass.parseNull(gmXMLParserUtility.createXMLString(alInvDetails));
    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();

    if (alInvDetails.size() > 0) {
      pw.write(strXMLString);
    }
    pw.flush();
    return null;
  }

  /**
   * saveCashAppDtls
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward saveCashAppDtls(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmCashApplicationForm gmCashApplicationForm = (GmCashApplicationForm) actionForm;
    gmCashApplicationForm.loadSessionParameters(request);

    GmARBean gmARBean = new GmARBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();

    String strURL = "/gmCashApplication.do?method=loadCashAppDtls";
    String strDisplayNm = "Cash Application Screen";
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmResourceBundleBean gmResourceBundleBeanlbl =
        GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmCashApplication",
            strSessCompanyLocale);

    hmParam = GmCommonClass.getHashMapFromForm(gmCashApplicationForm);

    hmParam.put("PAYMENTDT",
        GmCommonClass.getStringToDate((String) hmParam.get("PAYMENTDT"), strApplDateFmt));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strPaidInvoiceIDs = GmCommonClass.parseNull((String) hmParam.get("PAIDINVOICEIDS"));
    if (strOpt.equals("Save")) {
      hmReturn = gmARBean.postMultiplePayments(hmParam);
      log.debug(" hmReturn ==> " + hmReturn);
    }

    String strSuccessMessage =
        GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_PAYMENT_POSTED"))
            + strPaidInvoiceIDs;

    if (!strSuccessMessage.equals("")) {
      request.setAttribute("hType", "S");
      request.setAttribute("hDisplayNm", strDisplayNm);
      request.setAttribute("hRedirectURL", strURL);
      throw new AppError(strSuccessMessage, "S", 'S');
    }
    return loadCashAppDtls(actionMapping, gmCashApplicationForm, request, response);

  }

}
