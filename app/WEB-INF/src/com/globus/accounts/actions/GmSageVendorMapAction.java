package com.globus.accounts.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAPSageRptBean;
import com.globus.accounts.beans.GmAPSageTransBean;
import com.globus.accounts.forms.GmSageVendorMapForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmSageVendorMapAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);

      GmSageVendorMapForm gmSageVendorMapForm = (GmSageVendorMapForm) form;
      gmSageVendorMapForm.loadSessionParameters(request);
      GmAPSageRptBean gmAPSageRptBean = new GmAPSageRptBean(getGmDataStoreVO());
      GmAPSageTransBean gmAPSageTransBean = new GmAPSageTransBean(getGmDataStoreVO());

      Logger log = GmLogger.getInstance(this.getClass().getName());

      String strOpt = gmSageVendorMapForm.getStrOpt();

      RowSetDynaClass rdSageVendorResult = null;

      HashMap hmParam = new HashMap();
      hmParam = GmCommonClass.getHashMapFromForm(gmSageVendorMapForm);

      if (strOpt.equals("save")) {
        gmAPSageTransBean.saveSageVendor(hmParam);

      }
      if (!strOpt.equals("")) {

        String hmissingcheck = gmSageVendorMapForm.getHmissingNotcheck();

        if (hmissingcheck.equals("true"))
          gmSageVendorMapForm.setMissingSageVenIds("");
        hmParam = GmCommonClass.getHashMapFromForm(gmSageVendorMapForm);
        rdSageVendorResult = gmAPSageRptBean.fetchSageVendorDtls(hmParam);

        log.debug(" rdSubResult.getRows(). ..... " + rdSageVendorResult.getRows().size());
        gmSageVendorMapForm.setReturnList(rdSageVendorResult.getRows());
      }

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

}
