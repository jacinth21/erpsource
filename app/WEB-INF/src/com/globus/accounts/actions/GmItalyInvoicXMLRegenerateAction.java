package com.globus.accounts.actions;

import com.globus.accounts.forms.GmItalyInvoiceXMLRegenerateForm;
import com.globus.accounts.xml.GmInvoiceXmlInterface;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GmItalyInvoicXMLRegenerateAction extends GmDispatchAction {
	  Logger log = GmLogger.getInstance(this.getClass().getName());
	  /*
	   * The below method is used to load - regenerate Invoice XML
	   */
	  @Override
	  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	      HttpServletResponse response) throws AppError, Exception {
	    instantiate(request, response);
	    GmItalyInvoiceXMLRegenerateForm gmItalyInvoiceXMLRegenerateForm = (GmItalyInvoiceXMLRegenerateForm) form;
	    gmItalyInvoiceXMLRegenerateForm.loadSessionParameters(request);
	    String strSuccessInvoices="";
	    String strFailedInvoices="";
	    String strInvIds = "";
	    String strUserId = "";
	    HashMap hmParam = new HashMap();
	    hmParam = GmCommonClass.getHashMapFromForm(gmItalyInvoiceXMLRegenerateForm);
	    strInvIds= GmCommonClass.parseNull((String)hmParam.get("INVOICEIDS"));
	    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    String[] StrArrInvIds = strInvIds.split(",");
	    
	    for(int i = 0; i<StrArrInvIds.length; i++)
		 {          
	        log.debug("hmParam list"+StrArrInvIds[i]);
	        try{
	        	
	        	GmInvoiceXmlInterface gmInvoiceXmlInterface =
		                (GmInvoiceXmlInterface) GmCommonClass.getSpringBeanClass(
		                    "xml/Invoice_Xml_Beans.xml", "1020");
		            gmInvoiceXmlInterface.generateInvocieXML(StrArrInvIds[i], strUserId);
		            strSuccessInvoices = strSuccessInvoices + ","+ StrArrInvIds[i];
		            log.debug("Italy Invoice generatwed successfully"+strSuccessInvoices);
	        	}catch(Exception e){

	        	strFailedInvoices = strFailedInvoices + ","+ StrArrInvIds[i] +"->"+e.getMessage();
	        	 log.debug("Italy Invoice generatwed failed"+strFailedInvoices);
	        	}
	        
	    }
	    PrintWriter out = response.getWriter();
	    if(!strSuccessInvoices.isEmpty()) {
	    out.println("Sucessfully Generated XML File for below Invoices <br/>");
        out.println( strSuccessInvoices.substring(1));
	    }
	    if(!strFailedInvoices.isEmpty()){
	    out.println("<br/> XML File are failed for below Invoices <br/>");
	    out.println( strFailedInvoices.substring(1) );
	    }
	    return null;
	  }
		
}	
