package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.DynaBean;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAuditReconciliationBean;
import com.globus.accounts.beans.GmFieldSalesAuditBean;
import com.globus.accounts.forms.GmFieldSalesAuditForm;
import com.globus.accounts.forms.GmFlagVarianceForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmFlagVarianceAction extends GmAction  {

	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
		GmFlagVarianceForm gmFlagVarianceForm = (GmFlagVarianceForm) form;
		gmFlagVarianceForm.loadSessionParameters(request);
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmFlagVarianceForm);
		RowSetDynaClass  rdHeaderInfo=null;
		RowSetDynaClass  rdOverall=null;
		RowSetDynaClass  rdAuditReport=null;
		RowSetDynaClass rdSystemReport=null;
		RowSetDynaClass rdPAssociatedReport=null;
		RowSetDynaClass rdNAssociatedReport=null;
		RowSetDynaClass rdReturnReport=null;
		List alHeaderInfo = new ArrayList();
		List alOverall = new ArrayList();
		List alAuditReport= new ArrayList();
		List alSystemReport= new ArrayList();
		List alPAssociatedReport= new ArrayList();
		List alNAssociatedReport= new ArrayList();
		List alReturnsReport= new ArrayList();
		ArrayList alPCodeID=new ArrayList();
		ArrayList alNCodeID=new ArrayList();
		ArrayList alDistList=new ArrayList();
		ArrayList alPostingOptions=new ArrayList();
		ArrayList alchange=new ArrayList();
		int intDeviationQty=0,intUnresolvedQty=0;
		
		
		 HashMap hmHeaderInfo = new HashMap();
		 
		GmAuditReconciliationBean gmAuditReconciliationBean = new GmAuditReconciliationBean();
		GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean();
		String strVarianceType="";
		String strOpt= gmFlagVarianceForm.getStrOpt();
		log.debug("strOpt"+strOpt);
		
	
		if(strOpt.equals("save"))
		{
			gmAuditReconciliationBean.saveFlaggedVariance(hmParam);
		}
		
		HashMap hmResult= gmAuditReconciliationBean.fetchVariance(hmParam);
	
		//rdHeaderInfo=(RowSetDynaClass)hmResult.get("HEADERINFO");
		hmHeaderInfo=(HashMap)hmResult.get("HEADERINFO");
		rdOverall=(RowSetDynaClass)hmResult.get("OVERALLREPORT");
		rdAuditReport=(RowSetDynaClass)hmResult.get("AUDITREPORT");
		rdSystemReport=(RowSetDynaClass)hmResult.get("SYSTEMREPORT");
		rdPAssociatedReport=(RowSetDynaClass)hmResult.get("PASSOCIATEDREPORT");
		rdNAssociatedReport=(RowSetDynaClass)hmResult.get("NASSOCIATEDREPORT");
		rdReturnReport=(RowSetDynaClass)hmResult.get("RETURNREPORT");
		alOverall=rdOverall.getRows();
		alAuditReport=rdAuditReport.getRows();
		alSystemReport=rdSystemReport.getRows();
		alPAssociatedReport=rdPAssociatedReport.getRows();
		alNAssociatedReport=rdNAssociatedReport.getRows();
		alReturnsReport=rdReturnReport.getRows();
		
		gmFlagVarianceForm.setHmHeaderInfo(hmHeaderInfo);
		gmFlagVarianceForm.setAlOverall(alOverall);
		gmFlagVarianceForm.setAlAuditReport(alAuditReport);
		gmFlagVarianceForm.setAlSystemReport(alSystemReport);
		gmFlagVarianceForm.setAlPAssociatedReport(alPAssociatedReport);
		gmFlagVarianceForm.setAlNAssociatedReport(alNAssociatedReport);
		gmFlagVarianceForm.setAlReturnsReport(alReturnsReport);
		BasicDynaBean basicDynaBean=(BasicDynaBean)alOverall.get(0);
		
		String strUnresolvedQty= GmCommonClass.parseNull((String)hmParam.get("UNRESOLVEDQTY"));
		log.debug(strUnresolvedQty);
		if(!strUnresolvedQty.equals(""))
		{
			intUnresolvedQty=Integer.parseInt(strUnresolvedQty);
		}
		 intDeviationQty=Integer.parseInt(String.valueOf(( basicDynaBean.get("DEVIATION_QTY"))));
		//gmFlagVarianceForm.setDeviationQty(strDeviation);
		//int intDeviationQty=Integer.parseInt(String.valueOf(( basicDynaBean.get("DEVIATION_QTY"))));
	      
		
		// int intDeviationQty = -1;
		 
		 if (intDeviationQty > 0) {
				strVarianceType = "positive";
			} 
		 else if (intDeviationQty <0) {
				strVarianceType = "negative";
			}else{
				if(intUnresolvedQty>0)
				{
					strVarianceType = "positive";
				}
				else{
					strVarianceType = "negative";
				}
			}
		 
		 log.debug(strVarianceType);
		 if(strVarianceType.equals("positive")){
		 alPCodeID=GmCommonClass.getCodeList("FLFPQ");
		 gmFlagVarianceForm.setAlPCodeID(alPCodeID);
		 }
		 else{
		 alNCodeID=GmCommonClass.getCodeList("FLFNQ");
		 gmFlagVarianceForm.setAlNCodeID(alNCodeID);
		 }
		 alchange=GmCommonClass.getCodeList("AUDCH");
		 gmFlagVarianceForm.setAlchange(alchange);
		 
		 alPostingOptions=GmCommonClass.getCodeList("FAPOS");
		 gmFlagVarianceForm.setAlPostingOptions(alPostingOptions);
		 String strAuditId=GmCommonClass.parseNull((String)hmParam.get("AUDITID"));
		 alDistList=gmFieldSalesAuditBean.fetchLockedDistributorDetails(strAuditId);
		 gmFlagVarianceForm.setAlDistList(alDistList);
		 gmFlagVarianceForm.setStrVarianceType(strVarianceType);
		 
		 gmFlagVarianceForm.setDeviationQty(Integer.toString(intDeviationQty));
		//gmFlagVarianceForm.setStrVarianceType(strVarianceType);
		
		return mapping.findForward("GmFlagVariance");
	}
	
	
}