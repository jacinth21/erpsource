package com.globus.accounts.actions;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAuditReconciliationBean;
import com.globus.accounts.beans.GmFieldSalesAuditBean;
import com.globus.accounts.forms.GmAuditReconcilationForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;

public class GmAuditReconcilationAction extends GmAction {


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    GmAuditReconcilationForm gmAuditReconcilationForm = (GmAuditReconcilationForm) form;
    gmAuditReconcilationForm.loadSessionParameters(request);
    GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean(getGmDataStoreVO());
    GmAuditReconciliationBean gmAuditReconciliationBean =
        new GmAuditReconciliationBean(getGmDataStoreVO());
    GmJasperReport gmJasperReport = new GmJasperReport();

    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmAuditReconcilationForm);
    ArrayList alDistList = new ArrayList();
    ArrayList alAuditList = new ArrayList();
    ArrayList alAuditAction = new ArrayList();
    HashMap hmResult = new HashMap();
    String strOpt = "";
    InputStream reportStream;
    JasperPrint jp = null;
    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
    alAuditAction = GmCommonClass.getAllCodeList("FLDAA");
    gmAuditReconcilationForm.setAlAuditAction(alAuditAction);
    alAuditList = gmFieldSalesAuditBean.fetchAuditDetails();
    gmAuditReconcilationForm.setAlAuditList(alAuditList);
    strOpt = gmAuditReconcilationForm.getStrOpt();


    alDistList = gmFieldSalesAuditBean.fetchLockedDistributorDetails(strAuditId);

    gmAuditReconcilationForm.setAlDistList(alDistList);

    if (strOpt.equals("Approve")) {
      gmAuditReconciliationBean.saveApprove(hmParam);
      strOpt = "Load";
    }
    if (strOpt.equals("Reconcile")) {
      String strTxnIds = gmAuditReconciliationBean.saveReconcile(hmParam);
      strTxnIds = GmCommonClass.parseNull(strTxnIds).equals("") ? "" : strTxnIds.substring(1);
      throw new AppError(
          "The following Transaction ID's are created as a part of this posting. <br> " + strTxnIds,
          "", 'S');
    }

    if (strOpt.equals("Load") || strOpt.equals("PDF")) {
      hmResult = gmAuditReconciliationBean.fetchConfirmationReport(hmParam);
      HashMap hmHeaderInfo = (HashMap) hmResult.get("HEADERINFO");
      hmResult.put("ADDRESS", hmHeaderInfo.get("ADDRESS"));
      hmResult.put("CUR_DATE", hmHeaderInfo.get("CUR_DATE"));
      hmResult.put("DIST_NAME", hmHeaderInfo.get("DIST_NAME"));
      hmResult.put("RETURN_DATE", hmHeaderInfo.get("RETURN_DATE"));
      hmResult.put("AUDIT_USER", hmHeaderInfo.get("AUDIT_USER"));
      hmResult.put("LOCKED_DATE", hmHeaderInfo.get("LOCKED_DATE"));
    }

    hmResult.put(
        "SUBREPORT_DIR",
        request.getSession().getServletContext()
            .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
            + "\\");

    log.debug("strJasperPath" + strJasperPath);
    if (strOpt.equals("PDF")) {
      ServletOutputStream servletOutputStream = response.getOutputStream();
      response.setHeader("Content-Disposition", "inline; filename=\"file2.pdf\"");
      response.setContentType("application/pdf");
      reportStream =
          request.getSession().getServletContext()
              .getResourceAsStream(strJasperPath + "/GmAuditConfirmationPDF.jasper");
      JRPdfExporter exporter = new JRPdfExporter();
      jp = JasperFillManager.fillReport(reportStream, hmResult, new JREmptyDataSource());
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
      exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
      exporter.exportReport();
      servletOutputStream.close();
      return mapping.findForward("");
    }

    gmAuditReconcilationForm.setHmResult(hmResult);
    return mapping.findForward("GmConfirmation");
  }
}
