package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmARReportBean;
import com.globus.accounts.forms.GmVATSummaryForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;


public class GmVATSummaryAction extends GmAction {



  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmVATSummaryForm gmVATSummaryForm = (GmVATSummaryForm) form;
    gmVATSummaryForm.loadSessionParameters(request);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    HttpSession session = request.getSession(false);

    String strARCurrSymbol = "";
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    strApplnDateFmt = strApplnDateFmt.equals("") ? "mm/dd/yyyy" : strApplnDateFmt;
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HashMap hmApplnParam = new HashMap();
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmCurrency = new HashMap();
    ArrayList alCompanyCurrency = new ArrayList();
    
    String strCompanyLocale =
        GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));
    GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);   
    String strARReportByDealerFlag = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("AR_REPORT_BY_DEALER"));
    hmApplnParam.put("ARREPORTBYDEALER", strARReportByDealerFlag);
    hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
    hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");


    GmARReportBean gmARReportBean = new GmARReportBean(getGmDataStoreVO());
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
    hmParam = GmCommonClass.getHashMapFromForm(gmVATSummaryForm);
    String load = gmVATSummaryForm.getStrOpt();
    alCompanyCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());
    strARCurrSymbol = GmCommonClass.parseZero(gmVATSummaryForm.getStrCompCurrency());
    strARCurrSymbol = strARCurrSymbol.equals("0") ? strCompanyCurrId : strARCurrSymbol;
    hmParam.put("ARCURRSYMBOL", strARCurrSymbol);

    if (!load.equals("VATSUMMARY")) {
      hmParam.put("FORMAT", strApplnDateFmt);
      alResult = gmARReportBean.fetchVATSummaryDetails(hmParam);
    }
    gmVATSummaryForm.setStrARRptByDealerFlag(strARReportByDealerFlag);
    gmVATSummaryForm.setAlCompCurrency(alCompanyCurrency);
    gmVATSummaryForm.setStrCompCurrency(strARCurrSymbol);
    gmVATSummaryForm.setGridData(generateOutPut(alResult, hmApplnParam));
    return mapping.findForward("gmVATSummary");
  }

  // @SuppressWarnings("unchecked")
  private String generateOutPut(ArrayList alResult, HashMap hmApplnParam) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmApplnParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmApplnParam", hmApplnParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.GmVATSummary", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("accounts/templates");
    templateUtil.setTemplateName("GmVATSummary.vm");
    return templateUtil.generateOutput();
  }
}
