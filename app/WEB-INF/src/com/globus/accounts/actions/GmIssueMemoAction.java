package com.globus.accounts.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.beans.GmInvoiceBean;
import com.globus.accounts.forms.GmIssueMemoForm;
import com.globus.accounts.tax.beans.GmSalesTaxTransBean;
import com.globus.accounts.xml.GmInvoiceXmlInterface;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;

public class GmIssueMemoAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code

  /**
   * loadIssueMemoDtls
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward loadIssueMemoDtls(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmIssueMemoForm gmIssueMemo = (GmIssueMemoForm) actionForm;
    gmIssueMemo.loadSessionParameters(request);
    GmARBean gmARBean = new GmARBean(getGmDataStoreVO());
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alMemoType = new ArrayList();
    String strMemoType = "";
    String strOpt = "";
    String strSuccessMessage = "";
    String strURL = "";
    String strDisplayNm = "";
    String strXmlData = "";
    String strOutInvoiceIDs = "";
    String strOutOrderIDs = "";
    String strMemoName = "";
    String strDateFmt = "";
    String strTodayDt = "";
    String strAccCurrName = "";
    String strARCurrSymbol = "";
    String strCompanyCurrId = "";
    String strARReportByDealerFlag = "";
    HashMap hmCurrency = new HashMap();
    ArrayList alCompanyCurrency = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmIssueMemo);
    alCompanyCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());
    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    strCompanyCurrId = GmCommonClass.parseNull((String) hmCurrency.get("TXNCURRENCYID"));
    strARCurrSymbol = GmCommonClass.parseZero((String) hmParam.get("STRCOMPCURRENCY"));
    strARCurrSymbol = strARCurrSymbol.equals("0") ? strCompanyCurrId : strARCurrSymbol;
    strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strARCurrSymbol);
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strMemoType = GmCommonClass.parseZero((String) hmParam.get("MEMOTYPE"));
    strDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
    GmResourceBundleBean gmResourceBundleBean1 = GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);   
    strARReportByDealerFlag = GmCommonClass.parseNull(gmResourceBundleBean1.getProperty("AR_REPORT_BY_DEALER"));
    if (strOpt.equals("Save")) {
      hmReturn = gmARBean.saveIssueMemo(hmParam);
      strOutInvoiceIDs = GmCommonClass.parseNull((String) hmReturn.get("INVOICE"));
      strOutOrderIDs = GmCommonClass.parseNull((String) hmReturn.get("ORDER"));
      strMemoName = GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strMemoType));
      strSuccessMessage =
          "The following are the " + strMemoName + " invoices created. <BR>" + strOutInvoiceIDs;
      /*
       * strURL = "/gmIssueMemo.do?method=loadIssueMemoDtls"; strDisplayNm =
       * "Issue Credit/Debit Screen";
       * 
       * request.setAttribute("hType", "S"); request.setAttribute("hDisplayNm", strDisplayNm);
       * request.setAttribute("hRedirectURL", strURL); throw new AppError(strSuccessMessage, "S",
       * 'S');
       */
      // redirect the Invoice List screen
      // to call the Web service
      String strInvID = "";
      GmSalesTaxTransBean gmSalesTaxTransBean = new GmSalesTaxTransBean(getGmDataStoreVO());
      if (!strOutInvoiceIDs.contains(",")) {
        strOutInvoiceIDs = strOutInvoiceIDs.concat(",");
      }
      String strTempCustPo = "";
      String strErrorCustPo = "";
      String[] al = strOutInvoiceIDs.split(",");
      for (int i = 0; i < al.length; i++) {
        strInvID = GmCommonClass.parseNull(al[i]);
        log.debug("Invoice ID:" + strInvID);
        strTempCustPo =
            GmCommonClass.parseNull(gmSalesTaxTransBean.updateSalesTax(strInvID,
                (String) hmParam.get("USERID")));
        if (!strTempCustPo.equals("")) {
          strErrorCustPo = strErrorCustPo + strTempCustPo + ", ";
        }
        GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(getGmDataStoreVO());
        HashMap hmCompanyInvoice = gmInvoiceBean.fetchCompanyInvoiceDtls(strInvID);
        String strGenerateXmlFl =
            GmCommonClass.parseNull((String) hmCompanyInvoice.get("GENERATE_XML"));
        // if Company is creating the XML file
        if (strGenerateXmlFl.equals("Y")) {
          GmInvoiceXmlInterface gmInvoiceXmlInterface =
              (GmInvoiceXmlInterface) GmCommonClass.getSpringBeanClass("xml/Invoice_Xml_Beans.xml",
                  getGmDataStoreVO().getCmpid());

          gmInvoiceXmlInterface.generateInvocieXML(strInvID, (String) hmParam.get("USERID"));
        }
      }
      if (!strErrorCustPo.equals("")) {
        throw new AppError("", "20667");
      }
      strOutInvoiceIDs = GmCommonClass.parseNull(GmCommonClass.removeSpaces(strOutInvoiceIDs)); // remove
                                                                                                // the
                                                                                                // space
      strOutInvoiceIDs = strOutInvoiceIDs.replaceAll(",", "','");
      // Getting the current date from GmCalenderOperation.
      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      strTodayDt = GmCommonClass.parseNull(GmCalenderOperations.getCurrentDate(strDateFmt));

      String strForward =
          "/GmInvoiceOrderListServlet?hLoadScreen=LoadInvoiceList&hAction=Reload&Cbo_InvSource=0&Cbo_InvoiceType="
              + strMemoType
              + "&Txt_FromDate="
              + strTodayDt
              + "&Txt_ToDate="
              + strTodayDt
              + "&hCallFrom=IssueMemo&hSuccessMsg="
              + strSuccessMessage
              + "&hMemoInvoices="
              + strOutInvoiceIDs;

      return actionRedirect(strForward, request);
    }

    alMemoType = GmCommonClass.parseNullArrayList((GmCommonClass.getCodeList("IVCTP", "40200")));// only
                                                                                                 // fetch
                                                                                                 // Credit
                                                                                                 // and
                                                                                                 // Debit
    // 40200 - Issue Memo type (new code id)
    // default selected as Credit memo
    if (strMemoType.equals("0")) {
      strMemoType = "50202"; // Credit
      hmParam.put("MEMOTYPE", strMemoType);
      gmIssueMemo.setMemoType(strMemoType);
    }
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("ARREPORTBYDEALER", strARReportByDealerFlag);
    gmIssueMemo.setStrARRptByDealerFlag(strARReportByDealerFlag);
    gmIssueMemo.setAlMemoType(alMemoType);

    strXmlData = getXmlGridData(alMemoType, hmParam);
    gmIssueMemo.setGridData(strXmlData);
    gmIssueMemo.setAlCompCurrency(alCompanyCurrency);
    gmIssueMemo.setStrCompCurrency(strARCurrSymbol);
    return actionMapping.findForward("GmIssueMemo");
  }

  /**
   * fetchInvDtls
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  public ActionForward fetchInvDtls(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmIssueMemoForm gmIssueMemo = (GmIssueMemoForm) form;
    GmARBean gmARBean = new GmARBean(getGmDataStoreVO());
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();

    HashMap hmParam = new HashMap();
    ArrayList alInvDetails = new ArrayList();

    gmIssueMemo.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmIssueMemo);
    // validate the invoice id and part number
    alInvDetails = GmCommonClass.parseNullArrayList(gmARBean.validateInvoice(hmParam));
    String strXMLString = GmCommonClass.parseNull(gmXMLParserUtility.createXMLString(alInvDetails));
    response.setContentType("text/xml");
    PrintWriter pw = response.getWriter();

    if (alInvDetails.size() > 0) {
      pw.write(strXMLString);
    }
    pw.flush();
    return null;
  }

  /**
   * @getXmlGridData xml content generation for Issue Memo grid
   * @author
   * @param array list and hash map
   * @return String
   */

  private String getXmlGridData(ArrayList alResult, HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    ArrayList alDropDownDistName = new ArrayList();
    ArrayList alDropDownAuditor = new ArrayList();
    ArrayList alDropDownDistStatus = new ArrayList();

    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.GmIssueMemo", strSessCompanyLocale));
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("accounts/templates");
    templateUtil.setTemplateName("GmIssueMemo.vm");

    return templateUtil.generateOutput();
  }

}
