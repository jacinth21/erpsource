package com.globus.accounts.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.accounts.forms.GmStandardReportForm;
import com.globus.operations.beans.GmVendorReportBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.operations.beans.GmVendorStandardCostBean;

/**
 * @author pvigneshwaran
 * Method:GmVendorStandardCostAction
 * return:GmStandardReport
 * This method load standard cost details when we click load button
 */

public class GmVendorStandardCostAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	HashMap hmParam = new HashMap();
	public ActionForward loadStandardCostRpt(ActionMapping mapping,ActionForm form, HttpServletRequest request,HttpServletResponse response) throws AppError,Exception{
	
		/*Initiate class to load details*/
		instantiate(request, response);
		ArrayList alReport = new ArrayList();
		GmStandardReportForm gmStandardReportForm = (GmStandardReportForm) form;
		gmStandardReportForm.loadSessionParameters(request);
		/*DeclareBeanObject*/
		GmVendorReportBean gmVendorReportBean = new GmVendorReportBean(getGmDataStoreVO());
		GmProjectBean gmProject = new GmProjectBean(getGmDataStoreVO());
	    GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());
	
		
	    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
	    String strCompId = getGmDataStoreVO().getCmpid();
	    /*get the vendor name and set to Form*/
	    HashMap hmName= new HashMap();
	    HashMap hmReturn = new HashMap();
	    hmName.put("COST_TYPE_FL","Y");
	    hmName.put("COST_TYPE","106561");//Standard Cost
	    hmReturn = gmVendorBean.getVendorList(hmName);
	    alReport = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("VENDORLIST"));
	    gmStandardReportForm.setAlVendorList(alReport);

		/*For Access*/
		ArrayList alProject = new ArrayList();
		HashMap hmProj = new HashMap();
		hmProj.put("COLUMN", "ID");
		hmProj.put("STATUSID", "20301"); // Filter all approved projects
		alProject = gmProject.reportProject(hmProj);
		gmStandardReportForm.setProjectList(alProject);
	

	    
		/*Get form values from HashMap*/
	    hmParam = GmCommonClass.getHashMapFromForm(gmStandardReportForm);
		HashMap hmResult = new HashMap();
		 
		ArrayList alReportDetails = new ArrayList();
		String strUserId="";
		String strOpt="";
		String strXmlGridData = "";

	
		/*Get Stropt Values and user id s*/
	    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
	   strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	   
		
		if (strOpt.equals("Load")) {
			 alReportDetails = gmVendorReportBean.fetchStandardCostDtls(hmParam);
			 hmResult.put("ALRESULT", alReportDetails);
		     hmResult.put("TEMPLATENAME", "GmVendorStandardCostRpt.vm");
			 strXmlGridData = generateOutPut(hmResult);
			 gmStandardReportForm.setGridXmlData(strXmlGridData);
			 
		}
		
		return mapping.findForward("GmVendorStandardCostRpt");
	}
	/**
	 * @author pvigneshwaran
	 * Method:saveStandardCost
	 * return:GmStandardEdit
	 * This method save standard cost details when we click edit button
	 */
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward saveStandardCost(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError,Exception
    { 
		
		/*Initiate class to load details*/
		instantiate(request, response);
		GmStandardReportForm gmStandardReportForm = (GmStandardReportForm) form;
		gmStandardReportForm.loadSessionParameters(request);
		GmVendorReportBean gmVendorReportBean = new GmVendorReportBean(getGmDataStoreVO());
		  GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());
		    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		HashMap hmReturn = new HashMap();
		HashMap hmAccess = new HashMap();
		String strOpt = "";
		String strOutMsg = "";
		String strPartyId = "";
		
		hmParam = GmCommonClass.getHashMapFromForm(gmStandardReportForm);
		
		strOpt = GmCommonClass.parseNull((String)hmParam.get("STROPT"));
		strPartyId = GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID"));
		String strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
	    String strVendorId = GmCommonClass.parseNull((String) hmParam.get("VENDORID"));
		String strPartNumbers = GmCommonClass.parseNull((String) hmParam.get("STRPARTNUMBERS"));
	    String strVendorIds = GmCommonClass.parseNull((String) hmParam.get("STRVENDORID"));
	    String strProjectIds = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strAccessFlag = "";
		hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId, "EDIT_STD_COST_ACCESS"));  	 // Allow Security event to Submit button used 'partyId' instead of 'userId' in PMT-32391
		strAccessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		if(strAccessFlag.equals("Y"))
		{
			gmStandardReportForm.setStrEditAccess("Y");
		}
		gmStandardReportForm.setStrsuccess("");
		if(strOpt.equals("Save")){
			strOutMsg = gmVendorBean.saveStandardCost(hmParam);
			String strRedirectPath = "/gmVendorStandardCost.do?method=saveStandardCost"
					+ "&projectId="
					+ strProjectIds
					+ ""
					+ "&strVendorId="
					+ strVendorIds
					+ ""
					+ "&strPartNumbers="
					+ strPartNumbers
					+ ""
					+ "&strsuccess="
					+ strOutMsg
					+ ""
					+ "&pnum="
					+ strPartNumber
					+ ""
					+ "&vendorId="
					+ strVendorId
					+ "&strOpt=Load";
				    return actionRedirect(strRedirectPath, request);
				
		
		}
		
		if (strOpt.equals("Load")) {
			 String success = GmCommonClass.parseNull((String)hmParam.get("STRSUCCESS"));
			 gmStandardReportForm.setStrsuccess(success);
			hmReturn = gmVendorReportBean.fetchEditCost(hmParam);
			gmStandardReportForm = (GmStandardReportForm) GmCommonClass.getFormFromHashMap(gmStandardReportForm, hmReturn);
		}
	
		
		return mapping.findForward("GmVendorStandardCostUpdate");
    }
	/**
	 * @author pvigneshwaran
	 * Method:loadStandardCostHistory
	 * return:GmHistory
	 * This method load standard cost history details when we click history icon
	 */
	public ActionForward loadStandardCostHistory(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError,Exception
    {   	
		instantiate(request, response);
		GmStandardReportForm gmStandardReportForm = (GmStandardReportForm) form;
		gmStandardReportForm.loadSessionParameters(request);
		GmVendorReportBean gmVendorReportBean = new GmVendorReportBean(getGmDataStoreVO());
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		String strOpt = "";
		String strUserId = "";
		
		hmParam = GmCommonClass.getHashMapFromForm(gmStandardReportForm);
	
			HashMap hmResult = new HashMap();
			ArrayList allResult = new ArrayList();
			allResult = gmVendorReportBean.fetchStandardCostHistory(hmParam);
			 hmResult.put("ALRESULT", allResult);
		     hmResult.put("TEMPLATENAME", "GmVendorStandardCostHistory.vm");
		 	 String strXmlHisoryData = generateOutPut(hmResult);
		 	gmStandardReportForm.setGridHistoryData(strXmlHisoryData);
			
		
    
		return mapping.findForward("GmVendorStandardCostHistory");
    }
	
	public String generateOutPut(HashMap hmParam) throws AppError {

		GmTemplateUtil templateUtil = new GmTemplateUtil();
		ArrayList alList = new ArrayList();
		alList = GmCommonClass.parseNullArrayList((ArrayList) hmParam
				.get("ALRESULT"));
	    String strTempName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATENAME"));
		templateUtil.setDataMap("hmParam", hmParam);
		templateUtil.setDataList("alList", alList);
		templateUtil.setTemplateSubDir("accounts/templates");	
		templateUtil.setTemplateName(strTempName);
		return templateUtil.generateOutput();
	}
	
	/**loadStandardCostBulkUpload:This method used to load bulk cost upload screen details vendordropdown,SecurityGroupAccess
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadStandardCostBulkUpload(ActionMapping mapping,ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		// call the instantiate method
		instantiate(request, response);
		GmStandardReportForm gmStandardReportForm = (GmStandardReportForm) form;
		gmStandardReportForm.loadSessionParameters(request);
		
        GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
        GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());
        /*get the vendor name and set to Form*/
        String strAccessFlag = "";
        String strPartyId ="";
        
	    HashMap hmName= new HashMap();
	    HashMap hmReturn = new HashMap();
	    HashMap hmParam = new HashMap();
	    HashMap hmAccess = new HashMap();
	    ArrayList alReport = new ArrayList();
	    
	    hmName.put("COST_TYPE_FL","Y");
	    hmName.put("COST_TYPE","106561");//Standard Cost
	    hmReturn = gmVendorBean.getVendorList(hmName);
	    alReport = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("VENDORLIST"));
	    gmStandardReportForm.setAlVendorList(alReport);
	    
	    /*Get form values from HashMap*/
	    hmParam = GmCommonClass.getHashMapFromForm(gmStandardReportForm);

	  //Get 1000 value from MAX DATA for Validation if Excel copy to grid data is 1000 above
	   String  strMaxUploadData = GmCommonClass.parseNull(GmCommonClass.getRuleValue("MAX_DATA", "PD_DATA_SETUP"));
	   gmStandardReportForm.setStrMaxUploadData(strMaxUploadData);
	    
	    //Security event to Submit button
	    strPartyId = GmCommonClass.parseNull((String) gmStandardReportForm.getSessPartyId());
		hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId, "VEN_COST_BULK_UPL")); // Allow Security event to Submit button used 'partyId' instead of 'userId' in PMT-32391
		strAccessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		log.debug(" Standard Cost Bulk  Upload - Permissions Flag = " + hmAccess);
		gmStandardReportForm.setStrEditAccess(strAccessFlag);
			    
		return mapping.findForward("GmVendorStandardCostBulkUpload");
	}
	
	/**saveStandardCostBulkUpload:This Method used to save Vendor Cost Bulk Upload Details
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward saveStandardCostBulkUpload(ActionMapping mapping, 
			ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError,Exception
    { 
		
		/*Initiate class to load details*/
		instantiate(request, response);
		GmStandardReportForm gmStandardReportForm = (GmStandardReportForm) form;
		gmStandardReportForm.loadSessionParameters(request);
		GmVendorStandardCostBean gmVendorStandardCostBean = new GmVendorStandardCostBean(getGmDataStoreVO());
		
	    HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmStandardReportForm);
	
		String strJsonString = GmCommonClass.parseNull(gmVendorStandardCostBean.saveStandardCostBulkUpload(hmParam));
		
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strJsonString.equals("")) {
			pw.write(strJsonString);
		}
		pw.flush();
		return null;
    }	
}
