package com.globus.accounts.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAPBean;
import com.globus.accounts.forms.GmAPPostingsForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


public class GmAPPostingsAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      GmAPPostingsForm gmAPPostingsForm = (GmAPPostingsForm) form;
      gmAPPostingsForm.loadSessionParameters(request);

      GmAPBean gmAPtBean = new GmAPBean(getGmDataStoreVO());

      Logger log = GmLogger.getInstance(this.getClass().getName());

      String strOpt = gmAPPostingsForm.getStrOpt();
      RowSetDynaClass rdAPPostingRpt = null;
      RowSetDynaClass rdAPPostingDtls = null;
      HashMap hmParam = new HashMap();

      log.debug(" Testing. ..... ");
      hmParam = GmCommonClass.getHashMapFromForm(gmAPPostingsForm);


      if (strOpt.equals("reload")) {

        rdAPPostingRpt = gmAPtBean.fetchAPPostingReport(hmParam);

        log.debug(" rdAPPostingRpt.getRows(). ..... " + rdAPPostingRpt.getRows().size());
        gmAPPostingsForm.setReturnList(rdAPPostingRpt.getRows());

      }

      if (strOpt.equals("DtlsFromRpt")) {

        rdAPPostingDtls = gmAPtBean.fetchAPPostingDetail(hmParam);

        log.debug(" rdAPPostingDtls.getRows(). ..... " + rdAPPostingDtls.getRows().size());
        gmAPPostingsForm.setReturnList(rdAPPostingDtls.getRows());
        return mapping.findForward("apprptdtls");
      }

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

}
