package com.globus.accounts.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmICTSummaryBean;
import com.globus.accounts.forms.GmICTSummaryForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.requests.forms.GmRequestForm;


public class GmICTSummaryAction extends GmAction {
Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
HashMap hmData = new HashMap() ;
		
		public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {			
	    	try {
	    		RowSetDynaClass rdResult = null;
	    		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	    		String StrConsgnId = GmCommonClass.parseNull(request.getParameter("REFID"));
	    		//String StrConsgnId = "GM-CN-71524";
	    		log.debug("StrConsgnId "+StrConsgnId);
	    		GmICTSummaryForm gmICTSummaryForm = (GmICTSummaryForm)form;
	    		GmICTSummaryBean gmICTSummaryBean = new GmICTSummaryBean();
	    		rdResult=gmICTSummaryBean.fetchICTSummary(StrConsgnId);	    		
	    		gmICTSummaryForm.setLsummary(rdResult.getRows());
	    		log.debug("fetched row size "+rdResult.getRows().size());
	    	}
	    	catch(Exception exp){
	            exp.printStackTrace();
	            throw new AppError(exp);
	        }
	    	 return mapping.findForward("success");
		}
		
	
		
}
