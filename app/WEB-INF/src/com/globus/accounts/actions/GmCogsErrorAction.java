package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAccountingBean;
import com.globus.accounts.beans.GmCogsBean;
import com.globus.accounts.forms.GmCogsErrorForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

/**
 * The class represents to load the <b>COGS error reports</b>.
 * <p>
 * Using this class to fetch the COGS error details and Repost or Cancel the items - at the part
 * level.
 * </p>
 * 
 * @author mmuthusamy
 *
 */

public class GmCogsErrorAction extends GmAction {


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());

    HttpSession session = request.getSession(false);
    GmCogsErrorForm gmCogsErrorForm = (GmCogsErrorForm) form;
    gmCogsErrorForm.loadSessionParameters(request);
    GmCogsBean gmCogsBean = new GmCogsBean(getGmDataStoreVO());
    GmAccountingBean gmAccountingBean = new GmAccountingBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strOpt = "";
    String strSessTodaysDate = "";
    String strCogsErrorType = "";
    String strDateFormat = getGmDataStoreVO().getCmpdfmt();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    strOpt = GmCommonClass.parseNull(gmCogsErrorForm.getStrOpt());
    // strSessTodaysDate =
    // (String)GmCommonClass.parseNull(session.getAttribute("strSessTodaysDate"));
    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    strSessTodaysDate = GmCalenderOperations.getCurrentDate(strDateFormat);
    HashMap hmParam = new HashMap();
    HashMap hmParams = new HashMap();
    HashMap hmRuleGrp = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alInvType = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alCompanyId = new ArrayList();
    ArrayList alCountryId = new ArrayList();
    ArrayList alCogsErrorType = new ArrayList();

    alInvType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("COGTP"));
    gmCogsErrorForm.setAlInvType(alInvType);

    alStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("COGST"));
    gmCogsErrorForm.setAlStatus(alStatus);

    alCompanyId = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("COMP"));
    gmCogsErrorForm.setAlCompanyId(alCompanyId);

    hmRuleGrp.put("RULE_GRP", "ICT_CNTRY_DIST_MAP");
    alCountryId =
        GmCommonClass.parseNullArrayList(gmAccountingBean.fetchPostingByCountryList(hmRuleGrp));
    gmCogsErrorForm.setAlCountryId(alCountryId);

    alCogsErrorType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("COETY"));
    gmCogsErrorForm.setAlCogsErrorType(alCogsErrorType);

    if (strOpt.equals("")) {
      gmCogsErrorForm.setFromDate(strSessTodaysDate);
      log.debug(" strSessTodaysDate " + strSessTodaysDate);
      gmCogsErrorForm.setToDate(strSessTodaysDate);
    }

    hmParam = GmCommonClass.getHashMapFromForm(gmCogsErrorForm);
    strCogsErrorType = GmCommonClass.parseNull((String) hmParam.get("COGSERRORTYPE"));
    if (strCogsErrorType.equals("")) { // default to set Costing Error type (102940).
      gmCogsErrorForm.setCogsErrorType("102940");
      hmParam.put("COGSERRORTYPE", "102940");
    }

    if (strOpt.equals("Save")) {
      gmCogsBean.saveCorrectCOGSError(hmParam);
    }

    if (!strOpt.equals("")) {
      alReturn = gmCogsBean.fetchCogsErrorReport(hmParam);
    }


    if (alReturn.size() > 0) {
      hmParams.put("DATEFORMAT", strDateFormat);
      hmParams.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      gmCogsErrorForm.setGridXmlData(getXmlGridData(alReturn, hmParams));
    }
    gmCogsErrorForm.setStrOpt("reload");
    // to get the company and plant information
    gmCogsErrorForm.setAlCogsPlant(GmCommonClass.parseNullArrayList(gmCommonBean
        .loadPlantCompanyMapping()));
    gmCogsErrorForm.setAlCogsOwnerCompany(GmCommonClass.parseNullArrayList(gmCommonBean
        .getParentCompanyList()));

    return mapping.findForward("GmCogsErrorReport");
  }

  /**
   * getXmlGridData - Using this method to get the XML string Based on this XML file load the COGS
   * error reports.
   * 
   * @param alParam
   * @param hmParams
   * @return
   * @throws AppError
   */
  public String getXmlGridData(ArrayList alParam, HashMap hmParams) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParams.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alReturn", alParam);
    templateUtil.setDataMap("hmParams", hmParams);
    templateUtil.setTemplateName("GmCogsErrorReport.vm");
    templateUtil.setTemplateSubDir("accounts/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.GmCogsErrorDetailReport", strSessCompanyLocale));
    templateUtil.setDropDownMaster("alStatList", GmCommonClass.getCodeList("COGST"));
    return templateUtil.generateOutput();
  }
}
