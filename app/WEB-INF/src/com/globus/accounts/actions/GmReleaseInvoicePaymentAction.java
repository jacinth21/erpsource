package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAPBean;
import com.globus.accounts.forms.GmReleaseInvoicePaymentForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmVendorBean;

public class GmReleaseInvoicePaymentAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {

      instantiate(request, response);
      GmReleaseInvoicePaymentForm gmReleaseInvoicePaymentForm = (GmReleaseInvoicePaymentForm) form;
      gmReleaseInvoicePaymentForm.loadSessionParameters(request);
      GmAPBean gmAPBean = new GmAPBean(getGmDataStoreVO());
      GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

      Logger log = GmLogger.getInstance(this.getClass().getName());

      String strOpt = gmReleaseInvoicePaymentForm.getStrOpt();
      String strPartyId = gmReleaseInvoicePaymentForm.getSessPartyId();
      String accessFlag = "";
      String strmatchingPO = gmReleaseInvoicePaymentForm.getMatchingPO();

      ArrayList alPOList = new ArrayList();
      ArrayList alInvcList = new ArrayList();

      HashMap hmParam = new HashMap();
      HashMap hmReturn = new HashMap();
      HashMap hmAccess = new HashMap();

      hmParam = GmCommonClass.getHashMapFromForm(gmReleaseInvoicePaymentForm);
      log.debug(" hmParam. <<<<<<<<<<<<<<<<<<<<<<<<<<..... " + hmParam);
      if (strOpt.equals("save")) {
        gmAPBean.saveReleasedPaymentDtls(hmParam);

      }
      if (!strOpt.equals("")) {
        // if(strmatchingPO.equals("true")) gmReleaseInvoicePaymentForm.setMatchingPO("on");
        // else if(strmatchingPO.equals("on")) gmReleaseInvoicePaymentForm.setMatchingPO("");

        log.debug(" test.....>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>. ..... ");
        hmReturn = gmAPBean.fetchPaymentDtls(hmParam);

        alPOList = (ArrayList) hmReturn.get("RDPOLIST");
        alInvcList = (ArrayList) hmReturn.get("RDINVCLIST");
        log.debug(" alPOList.getRows(). ..... " + alPOList.size());
        log.debug(" alInvcList.getRows(). ..... " + alInvcList.size());
        gmReleaseInvoicePaymentForm.setReturnPOList(alPOList);
        gmReleaseInvoicePaymentForm.setReturnInvcList(alInvcList);
      }
      HashMap hmTemp = gmVendorBean.getVendorList(new HashMap());
      gmReleaseInvoicePaymentForm.setAlVendorList((ArrayList) hmTemp.get("VENDORLIST"));

      // gmReleaseInvoicePaymentForm.setAlPOTypeList((ArrayList)GmCommonClass.getCodeList("POTPE"));
      // gmReleaseInvoicePaymentForm.setAlmatchingTypeList((ArrayList)GmCommonClass.getCodeList("MATPE"));
      gmReleaseInvoicePaymentForm.setAlInvcInitiatedBy(GmCommonClass.getCodeList("INVIN"));

      gmReleaseInvoicePaymentForm.setAlInvcStatus(GmCommonClass.getCodeList("INVST"));

      // code for access rights
      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "AP_RELEASE_PAYMENT"); // PMT-51299-->A/p Release Payment Access
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      log.debug("Permissions = " + hmAccess);
      if (accessFlag.equals("Y")) {
        gmReleaseInvoicePaymentForm.setStrEnableReleasePymt("true");
      }

      // code for access rights
      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "AP_RELEASE_DOWNLOAD"); // PMT-51299-->A/p Release Download Access
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      log.debug("Permissions = " + hmAccess);
      if (accessFlag.equals("Y")) {
        gmReleaseInvoicePaymentForm.setStrEnableReleaseDownload("true");
      }

      // code for access rights
      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "AP_ROLLBACK_RELEASE"); // PMT-51299->A/p Rollback Download Access
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      log.debug("Permissions = " + hmAccess);
      if (accessFlag.equals("Y")) {
        gmReleaseInvoicePaymentForm.setStrEnableRollbackRelease("true");
      }

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

}
