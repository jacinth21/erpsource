/**
 * Description : This action class is used operate on AP Invoice Details Copyright : Globus Medical
 * Inc
 * 
 */
package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAPBean;
import com.globus.accounts.forms.GmAPSaveInvoiceDtlsForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmPurchaseBean;

/**
 * @author bvidyasankar
 * 
 */
public class GmAPSaveInvoiceDtlsAction extends GmAction {
  /*
   * The below method is used to fetch and save AP Invoice details
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError,Exception {
    String strOpt = "";
    String strDHRListFlag = "";
    boolean bSaveAndNewFlag = false;
    

      instantiate(request, response);
      GmAPSaveInvoiceDtlsForm gmAPSaveInvoiceDtlsForm = (GmAPSaveInvoiceDtlsForm) form;
      gmAPSaveInvoiceDtlsForm.loadSessionParameters(request);
      GmAPBean gmAPBean = new GmAPBean(getGmDataStoreVO());
      GmPurchaseBean gmPurchaseBean = new GmPurchaseBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());

      List lDHRResult = new ArrayList();
      List lAddlChrgResult = new ArrayList();

      ArrayList alAddlChrg = new ArrayList();

      HashMap hmParam = new HashMap();
      HashMap hmInvoiceDtls = new HashMap();
      HashMap hmPOInfo = new HashMap();
      HashMap hmPODetails = new HashMap();
      HashMap hmAccess = new HashMap();

      Logger log = GmLogger.getInstance(this.getClass().getName());

      strOpt = GmCommonClass.parseNull(gmAPSaveInvoiceDtlsForm.getStrOpt());
      String strPONumber = GmCommonClass.parseNull(gmAPSaveInvoiceDtlsForm.getHpoNumber());
      String strPaymentId = GmCommonClass.parseNull(gmAPSaveInvoiceDtlsForm.getHpaymentId());
      String strUserId = GmCommonClass.parseNull(gmAPSaveInvoiceDtlsForm.getUserId());
      String strShowOpenFlag =
          GmCommonClass.parseNull(gmAPSaveInvoiceDtlsForm.getShowOpenDHRFlag());
      String strHdhrId = GmCommonClass.parseNull(gmAPSaveInvoiceDtlsForm.getHdhrId());
      String strHaddlChrgId = GmCommonClass.parseNull(gmAPSaveInvoiceDtlsForm.getHaddlChrgId());
      strDHRListFlag = GmCommonClass.parseNull(gmAPSaveInvoiceDtlsForm.getStrDHRListFlag());


      log.debug("1@.................strOpt..." + strOpt);
      String strPartyId = gmAPSaveInvoiceDtlsForm.getSessPartyId();
      String accessFlag = "";

      log.debug("strPONumber: " + strPONumber);
      log.debug("strPaymentId: " + strPaymentId);
      log.debug("strUserId: " + strUserId);
      log.debug("strHdhrId: " + strHdhrId);
      log.debug("strHaddlChrgId: " + strHaddlChrgId);

      String strPaymentAmt = "";
      String strInvoiceId = "";
      String strInvoiceDt = "";
      String strInvoiceComments = "";
      String strInvoiceINTComments = "";
      String strPaymentStatus = "";


      hmParam = GmCommonClass.getHashMapFromForm(gmAPSaveInvoiceDtlsForm);

      if (strOpt.equalsIgnoreCase("saveAndNew")) {
        bSaveAndNewFlag = true;
        strOpt = "save";
        hmParam.put("strOpt", strOpt);
      }
      log.debug(" values inside hmParam " + hmParam);

      if (strPaymentId.equals(""))
        strPaymentId = "0";

      hmParam.put("VENDID", "0");
      if (strOpt.equalsIgnoreCase("newInvc") || strDHRListFlag.equals("Y")) {
        lDHRResult = ((RowSetDynaClass) gmAPBean.getDHRList(hmParam)).getRows();
        gmAPSaveInvoiceDtlsForm.setLdtDHR(lDHRResult);
        gmAPSaveInvoiceDtlsForm.setHpoNumber(strPONumber);
        gmAPSaveInvoiceDtlsForm.setStrOpt(strOpt);
      } else if (strOpt.equals("save")) {
        strPaymentId = gmAPBean.saveVendorPayment(hmParam, strUserId);
        hmParam.put("HPAYMENTID", strPaymentId);
        strShowOpenFlag = "";
        /*
         * hmParam.put("strOpt", "newInvc"); lDHRResult =
         * ((RowSetDynaClass)gmAPBean.getDHRList(hmParam)).getRows();
         * gmAPSaveInvoiceDtlsForm.setLdtDHR(lDHRResult);
         * gmAPSaveInvoiceDtlsForm.setHpoNumber(strPONumber);
         * gmAPSaveInvoiceDtlsForm.setStrOpt(strOpt);
         */

      }
      /*
       * if bSaveAndNewFlag is true the forward is to container so need not set any values to the
       * form attributes.
       * 
       * When strDHRListFlag is Y the request is to load DHR List, so need not load other values.
       */
      if ((strOpt.equalsIgnoreCase("InvcGo") || strOpt.equalsIgnoreCase("viewInvc") || strOpt
          .equalsIgnoreCase("save")) && !bSaveAndNewFlag && !strDHRListFlag.equalsIgnoreCase("Y")) {
        hmInvoiceDtls = GmCommonClass.parseNullHashMap(gmAPBean.getInvoiceDtls(strPaymentId));
        log.debug("Invoice Details: " + hmInvoiceDtls);

        strInvoiceId = GmCommonClass.parseNull((String) hmInvoiceDtls.get("INVOICEID"));
        strInvoiceDt = GmCommonClass.parseNull((String) hmInvoiceDtls.get("INVOICEDT"));
        strPaymentAmt = GmCommonClass.parseNull((String) hmInvoiceDtls.get("PAMOUNT"));
        strInvoiceComments = GmCommonClass.parseNull((String) hmInvoiceDtls.get("COMMENTS"));
        strInvoiceINTComments = GmCommonClass.parseNull((String) hmInvoiceDtls.get("INVOICEINTCOMMENTS"));
        strPaymentStatus = GmCommonClass.parseNull((String) hmInvoiceDtls.get("PSTATUS"));
        strPONumber = GmCommonClass.parseNull((String) hmInvoiceDtls.get("POID"));
        log.debug("1@......payment status: " + strPaymentStatus);

        gmAPSaveInvoiceDtlsForm.setInvoiceId(strInvoiceId);
        gmAPSaveInvoiceDtlsForm.setInvoiceDt(strInvoiceDt);
        gmAPSaveInvoiceDtlsForm.setPaymentAmt(strPaymentAmt);
        gmAPSaveInvoiceDtlsForm.setInvoiceComments(strInvoiceComments);
        gmAPSaveInvoiceDtlsForm.setInvoiceIntComments(strInvoiceINTComments);
        gmAPSaveInvoiceDtlsForm.setHpaymentStatus(strPaymentStatus);
        gmAPSaveInvoiceDtlsForm.setHpaymentId(strPaymentId);

        lDHRResult = ((RowSetDynaClass) gmAPBean.getDHRList(hmParam)).getRows();
        lAddlChrgResult = gmAPBean.getInvoiceAddlChrgs(strPaymentId).getRows();

        gmAPSaveInvoiceDtlsForm.setLdtDHR(lDHRResult);
        if (lAddlChrgResult != null) {
          gmAPSaveInvoiceDtlsForm.setLdtAddlChrg(lAddlChrgResult);
        }

        gmAPSaveInvoiceDtlsForm.setShowOpenDHRFlag(strShowOpenFlag);
        gmAPSaveInvoiceDtlsForm.setStrOpt(strOpt);
      }

      if (!bSaveAndNewFlag && !strDHRListFlag.equals("Y")) {// if bSaveAndNewFlag is true the
                                                            // forward is to container so need not
                                                            // set any values to the form attributes
        if (!strPONumber.equals("")) {
          // PMT-44785: AP Invoice entry - PO not loading
          // We need only PO details so calling the new method viewPODetails and get the PO details.
          hmPOInfo = gmPurchaseBean.viewPODetails(strPONumber);
          hmPODetails = GmCommonClass.parseNullHashMap((HashMap) hmPOInfo.get("PODETAILS"));
          String strCurrency = GmCommonClass.parseNull((String) hmPODetails.get("CURR"));
          if (!strCurrency.equals(""))
            strCurrency = GmCommonClass.getCodeAltName((String) hmPODetails.get("CURR"));
          log.debug("Currency: " + strCurrency);
          hmPODetails.put("CURR", strCurrency);
          gmAPSaveInvoiceDtlsForm.setHmpurchaseOrdDtls(hmPODetails);
        }

        // code for access rights
        hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "AP_SAVE_INVOICE"); // PMT-51299-->A/P Save Invoice Access
        accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        log.debug("Permissions = " + hmAccess);
        if (accessFlag.equals("Y")) {
          gmAPSaveInvoiceDtlsForm.setStrEnableSaveInvoice("true");
        }

        hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "AP_VOID_INVOICE"); // PMT-51299-->A/P void Invoice Access
        accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        log.debug("Permissions = " + hmAccess);
        if (accessFlag.equals("Y")) {
          gmAPSaveInvoiceDtlsForm.setStrEnableVoidInvoice("true");
        }

        alAddlChrg = GmCommonClass.getCodeList("APADDL");
        gmAPSaveInvoiceDtlsForm.setAlAddlChrgType(alAddlChrg);
      }

      // code for access rights
      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "AP_NEW_INVOICE"); // PMT-51299-->A/P New Invoice Access
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      log.debug("Permissions = " + hmAccess);
      if (accessFlag.equals("Y")) {
        gmAPSaveInvoiceDtlsForm.setStrEnableNewInvoice("true");
      }

      /***
       * When User selects from Invoice List report for viewing invoice details. If the user has no
       * rights to create new invoice then we should display the screen in view mode.
       * */

      if (strOpt.equals("InvcGo") && !accessFlag.equals("Y")) {
        gmAPSaveInvoiceDtlsForm.setStrOpt("viewInvc");
        log.debug("strOPt set in gmAPSaveInvoiceDtlsForm.....viewInvc");
      }

      strOpt = "success";

      if (strDHRListFlag.equals("Y"))
        strOpt = "loadDHRList";

      if (bSaveAndNewFlag) {
        strOpt = "fetchContainer";
        gmAPSaveInvoiceDtlsForm.setStrOpt("");
        gmAPSaveInvoiceDtlsForm.setHpoNumber("");
      }

      log.debug("forward>" + strOpt);
    
    // if(!strOpt.equalsIgnoreCase("fetchContainer"))
    return mapping.findForward(strOpt);
    // else
    // return mapping.findForward("/accounts/GmDummy.jsp");
  }
}
