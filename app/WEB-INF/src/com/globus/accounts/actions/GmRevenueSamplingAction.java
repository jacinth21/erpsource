package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAccountingReportBean;
import com.globus.accounts.beans.GmAccountsMiscBean;
import com.globus.accounts.forms.GmRevenueSamplingForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.sales.actions.GmSalesDispatchAction;

public class GmRevenueSamplingAction extends GmSalesDispatchAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmRevenueSamplingForm gmRevenueSamplingForm = (GmRevenueSamplingForm) form;
    gmRevenueSamplingForm.loadSessionParameters(request);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    HttpSession session = request.getSession(false);
    String strCompanyCurrId = "";
    String strApplnDateFmt = "";
    String strAccCurrName = "";
    String strARCurrSymbol = "";
    String strARReportByDealerFlag = "";
    HashMap hmCurrency = new HashMap();
    ArrayList alCompanyCurrency = new ArrayList();

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmCommonClass gmCommonClass = new GmCommonClass();
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmAccountingReportBean gmAccountingReportBean = new GmAccountingReportBean(getGmDataStoreVO());
    GmAccountsMiscBean gmAcct = new GmAccountsMiscBean(getGmDataStoreVO());
    GmAction gmAction = new GmAction();
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());

    strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    // Getting company currency symbol from currency dropdown.
    alCompanyCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());
    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    strCompanyCurrId = GmCommonClass.parseNull((String) hmCurrency.get("TXNCURRENCYID"));
    strARCurrSymbol = GmCommonClass.parseZero(gmRevenueSamplingForm.getStrCompCurrency());
    strARCurrSymbol = strARCurrSymbol.equals("0") ? strCompanyCurrId : strARCurrSymbol;
    String strParentFl = GmCommonClass.parseNull(gmRevenueSamplingForm.getChk_ParentFl());
    strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strARCurrSymbol);
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    HashMap hmApplnParam = new HashMap();
    hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
    hmApplnParam.put("SESSCURRFMT", strAccCurrName);
    hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    String strCompanyLocale =
        GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    strARReportByDealerFlag =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("AR_REPORT_BY_DEALER"));

    Date dtCurrDate = GmCommonClass.getCurrentDate(strApplnDateFmt);

    GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    ArrayList alRegion = GmCommonClass.parseNullArrayList(gmCommonClass.getCodeList("REGN"));
    ArrayList alStatus = GmCommonClass.parseNullArrayList(gmCommonClass.getCodeList("REVST"));
    // ArrayList alAccountList = GmCommonClass.parseNullArrayList(gmSales.reportAccount());
    ArrayList alRepList = GmCommonClass.parseNullArrayList(gmCustomerBean.getRepList("Active"));
    ArrayList alOrdDateType =
            GmCommonClass.parseNullArrayList(gmCommonClass.getCodeList("ORDTTY", "106480", getGmDataStoreVO()));
    
    HashMap hmSalesFilters = new HashMap();
    String strSalesFilterCond = "";
    String strAccessCondition = "";

    // gmRevenueSamplingForm.setAlAccountList(alAccountList);
    gmRevenueSamplingForm.setAlRegion(alRegion);
    gmRevenueSamplingForm.setAlRepList(alRepList);
    gmRevenueSamplingForm.setAlStatus(alStatus);
    gmRevenueSamplingForm.setChk_ParentFl("on");
    gmRevenueSamplingForm.setStrARRptByDealerFlag(strARReportByDealerFlag);
    gmRevenueSamplingForm.setAlOrdDateType(alOrdDateType);
    // To get the Access Level information
    strSalesFilterCond = GmCommonClass.parseNull(getAccessFilter(request, response, false));
    hmSalesFilters =
        GmCommonClass.parseNullHashMap(gmCommonBean.getSalesFilterLists(strSalesFilterCond));
    strSalesFilterCond = GmCommonClass.parseNull(getAccessFilter(request, response, true));
    strAccessCondition =
        GmCommonClass.parseNull(gmAction.getSalesAccessCondition(request, response, true));

    String strOpt = GmCommonClass.parseNull(gmRevenueSamplingForm.getStrOpt());
    if (strOpt.equals("")) {

      // Getting the current date from GmCalenderOperation.
      GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
      String todayDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);

      int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      String firstDayofMonth =
          GmCalenderOperations.getAnyDateOfMonth(currMonth, 1, strApplnDateFmt);

      gmRevenueSamplingForm.setDtOrderFromDT(GmCalenderOperations.getDate(firstDayofMonth,
          strApplnDateFmt));
      gmRevenueSamplingForm
          .setDtOrderToDT(GmCalenderOperations.getDate(todayDate, strApplnDateFmt));
    }
    String strhAction = gmRevenueSamplingForm.getHaction();
    String strInputStr = "";

    if (strhAction.equals("SendEmail")) {
      strInputStr = gmRevenueSamplingForm.getHinputStr();
      strInputStr = strInputStr.replaceAll("\\^", "','");
      strInputStr = "'".concat(strInputStr).concat("'");
      String strUserId = GmCommonClass.parseNull(gmRevenueSamplingForm.getUserId());
      if (!strInputStr.equals("")) {
        String strAccCurrAltName = GmCommonClass.getCodeAltName(strARCurrSymbol);
        gmAcct.sendEmailReminderPO(strInputStr, strUserId, "RevenueEmail", strAccCurrAltName);
      }
      strOpt = "Reload";
      gmRevenueSamplingForm.setHaction("");
    }

    if (strOpt.equals("Reload")) {

      HashMap hmParam = new HashMap();
      hmParam = GmCommonClass.getHashMapFromForm(gmRevenueSamplingForm);
      hmParam.put("DATEFORMAT", strApplnDateFmt);
      hmParam.put("SALES_CON", strAccessCondition);
      hmParam.put("CHK_ACTIVEFL", strParentFl);
      hmParam.put("ARCURRSYMBOL", strARCurrSymbol);
      hmApplnParam.put("ARREPORTBYDEALER", strARReportByDealerFlag);

      RowSetDynaClass rdRevenueSampling =
          gmAccountingReportBean.AccountRevenueSamplingReport(hmParam);
      // To Make the show parent account as checked or unchecked .Default it should be checked.
      strParentFl = strParentFl.equals("on") ? "on" : "false";
      gmRevenueSamplingForm.setChk_ParentFl(strParentFl);
      String strXmlData = generateOutPut((ArrayList) rdRevenueSampling.getRows(), hmApplnParam);
      strXmlData = GmCommonClass.replaceForXML(strXmlData);
      gmRevenueSamplingForm.setStrXmlString(strXmlData);

      gmRevenueSamplingForm.setHsize(rdRevenueSampling.getRows().size());

      log.debug(" values for (ArrayList)rdRevenueSampling.getRows() "
          + ((ArrayList) rdRevenueSampling.getRows()).size());
    }
    request.setAttribute("hmSalesFilters", hmSalesFilters);
    gmRevenueSamplingForm.setAlCompCurrency(alCompanyCurrency);
    gmRevenueSamplingForm.setStrCompCurrency(strARCurrSymbol);
    return mapping.findForward("GmRevenueSampling");
  }

  private String generateOutPut(ArrayList alGridData, HashMap hmApplnParam) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmApplnParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setDataMap("hmApplnParam", hmApplnParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.GmRevenueSampling", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("accounts/templates");
    templateUtil.setTemplateName("GmRevenueSampling.vm");
    return templateUtil.generateOutput();
  }
}
