/**
 * Description : This action class is used for fetching AP DHR List report content Copyright :
 * Globus Medical Inc
 * 
 */
package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAPBean;
import com.globus.accounts.forms.GmAPDHRListForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author bvidyasankar
 * 
 */
public class GmAPDHRListAction extends GmAction {
  /*
   * The below method is used to load AP DHR List report content
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError {

    try {
      instantiate(request, response);
      GmAPDHRListForm gmAPDHRListForm = (GmAPDHRListForm) form;
      gmAPDHRListForm.loadSessionParameters(request);

      GmAPBean gmAPBean = new GmAPBean(getGmDataStoreVO());

      List lResult = new ArrayList();
      HashMap hmParam;

      Logger log = GmLogger.getInstance(this.getClass().getName());

      String strOpt = gmAPDHRListForm.getStrOpt();
      String strPONumber = gmAPDHRListForm.getHpoNumber();

      hmParam = GmCommonClass.getHashMapFromForm(gmAPDHRListForm);
      log.debug(" values inside hmParam " + hmParam);

      hmParam.put("VENDID", "0");
      if (strOpt.equalsIgnoreCase("LoadDHR")) {
        lResult = ((RowSetDynaClass) gmAPBean.getDHRList(hmParam)).getRows();
        log.debug("after calling DHR list...." + lResult);
        gmAPDHRListForm.setLdtResult(lResult);
        gmAPDHRListForm.setHpoNumber(strPONumber);
      }

    } catch (Exception e) {
      throw new AppError(e);
    }

    return mapping.findForward("success");
  }
}
