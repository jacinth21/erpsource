package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import com.globus.accounts.beans.GmAuditReconciliationBean;
import com.globus.accounts.beans.GmFieldSalesAuditBean;
import com.globus.accounts.forms.GmFieldSalesRptForm;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.sales.beans.GmTerritoryBean;

public class GmFieldSalesRptAction extends GmDispatchAction {

	public ActionForward reportVarianceByDist(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Logger log = GmLogger.getInstance(this.getClass().getName());
		GmFieldSalesRptForm gmFieldSalesRptForm = (GmFieldSalesRptForm) form;
		GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean();
		GmAuditReconciliationBean gmAuditReconciliationBean = new GmAuditReconciliationBean();
		HashMap hmParam = new HashMap();
		RowSetDynaClass rdresult = null;
		List alResult = new ArrayList();

		ArrayList alList = new ArrayList();

		String strOpt = gmFieldSalesRptForm.getStrOpt();
		log.debug("called reportVarianceByDist " + strOpt);

		String strAuditId = gmFieldSalesRptForm.getAuditId();

		alList = gmFieldSalesAuditBean.fetchAuditDetails();
		gmFieldSalesRptForm.setAlAuditName(alList);

		alList = GmCommonClass.getCodeList("CPRSN");
		gmFieldSalesRptForm.setAlDevOpr(alList);

		if (!strAuditId.equals("")) {
			alList = gmFieldSalesAuditBean.fetchLockedDistributorDetails(strAuditId);
			gmFieldSalesRptForm.setAldist(alList);
		}
		if (strOpt.equals("")) {
			gmFieldSalesRptForm.setDispOnlyDeviation(true);
		}
		if (strOpt.equals("load")) {
			hmParam.put("AUDITID", strAuditId);
			hmParam.put("DISTID", gmFieldSalesRptForm.getDistId());
			rdresult = gmAuditReconciliationBean.fetchVarianceRptByDist(hmParam);

			alResult = rdresult.getRows();
			alResult = applySearchCriteria(alResult, gmFieldSalesRptForm);
			gmFieldSalesRptForm.setLdtResult(alResult);
		}

		return mapping.findForward("gmVarianceRptByDist");

	}

	public ActionForward reportVarianceRptBySet(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Logger log = GmLogger.getInstance(this.getClass().getName());
		GmFieldSalesRptForm gmFieldSalesRptForm = (GmFieldSalesRptForm) form;
		GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean();
		GmAuditReconciliationBean gmAuditReconciliationBean = new GmAuditReconciliationBean();
		GmTerritoryBean gmTerritoryBean = new GmTerritoryBean();
		GmProjectBean gmProj = new GmProjectBean();
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmFieldSalesRptForm);

		RowSetDynaClass rdresult = null;
		ArrayList alList = new ArrayList();
		ArrayList alSets = new ArrayList();
		ArrayList alTerritories = new ArrayList();
		List alResult = new ArrayList();

		String strOpt = gmFieldSalesRptForm.getStrOpt();
		String strReportType = gmFieldSalesRptForm.getReportType();

		alList = gmFieldSalesAuditBean.fetchAuditDetails();
		gmFieldSalesRptForm.setAlAuditName(alList);

		alList = GmCommonClass.getCodeList("CPRSN");
		gmFieldSalesRptForm.setAlDevOpr(alList);

		alSets = gmProj.loadSetMap("1601");
		alTerritories = gmTerritoryBean.getTerritoryList("ALL");
		gmFieldSalesRptForm.setAlSet(alSets);
		gmFieldSalesRptForm.setAlTerritory(alTerritories);

		gmFieldSalesRptForm.setStrOpt(strOpt);
		gmFieldSalesRptForm.setReportType(strReportType);
		log.debug("hmParam" + hmParam);
		// log.debug("set List"+alReturn.get("IDNAME"));
		if (strReportType.equals("globalDist")) {
			hmParam.put("TERRITORYID", "0");
			hmParam.put("SETID", "0");
		}
		if (strOpt.equals("")) {
			gmFieldSalesRptForm.setDispOnlyDeviation(true);
		}
		if (strOpt.equals("load")) {
			rdresult = gmAuditReconciliationBean.fetchVarianceRptBySet(hmParam);
			log.debug(" Count .." + rdresult.getRows().size());
			alResult = rdresult.getRows();
			alResult = applySearchCriteria(alResult, gmFieldSalesRptForm);
			gmFieldSalesRptForm.setLdtResult(alResult);
		}
		return mapping.findForward("gmVarianceRptBySet");

	}

	public ActionForward reportUploadedData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code
		ArrayList alList = new ArrayList();
		RowSetDynaClass rdresult = null;
		HashMap hmParam = new HashMap();

		GmFieldSalesRptForm gmFieldSalesRptForm = (GmFieldSalesRptForm) form;
		gmFieldSalesRptForm.loadSessionParameters(request);

		GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean();
		GmAuditReconciliationBean gmAuditReconciliationBean = new GmAuditReconciliationBean();

		String strOpt = gmFieldSalesRptForm.getStrOpt();
		log.debug("strOpt " + strOpt);

		String strAuditId = gmFieldSalesRptForm.getAuditId();

		alList = gmFieldSalesAuditBean.fetchAuditDetails();
		gmFieldSalesRptForm.setAlAuditName(alList);

		if (!strAuditId.equals("")) {
			alList = gmFieldSalesAuditBean.fetchLockedDistributorDetails(strAuditId);
			gmFieldSalesRptForm.setAldist(alList);

			alList = gmFieldSalesAuditBean.fetchAuditorName(strAuditId);
			gmFieldSalesRptForm.setAlAuditor(alList);
		}
		hmParam = GmCommonClass.getHashMapFromForm(gmFieldSalesRptForm);
		if (strOpt.equals("load")) {
			hmParam.put("ENTRYTYP", "51030"); //audited
			rdresult = gmAuditReconciliationBean.fetchUploadedData(hmParam);
			log.debug("resultset size " + rdresult.getRows().size());
			gmFieldSalesRptForm.setLdtResult(rdresult.getRows());

		}
		return mapping.findForward("gmVerifyUpload");
	}

	public List applySearchCriteria(List lresult, GmFieldSalesRptForm gmFieldSalesRptForm) {
		GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
		if (gmFieldSalesRptForm.isDispOnlyDeviation() == true) {
			gmSearchCriteria.addSearchCriteria("DEVIATION_QTY", "0", GmSearchCriteria.NOTEQUALS);
		}
		if (gmFieldSalesRptForm.isDispNonApproved() == true) 
		{
			gmSearchCriteria.addSearchCriteria("APPROVED_QTY", "0", GmSearchCriteria.NOTEQUALS);
		}
		if(gmFieldSalesRptForm.isDispUnresolvedDet())
		{
			gmSearchCriteria.addSearchCriteria("UNRESOLVED_QTY", "0", GmSearchCriteria.NOTEQUALS);
		}
		if(gmFieldSalesRptForm.isDispNonReconciliation())
		{
			gmSearchCriteria.addSearchCriteria("RECONCILED_QTY", "0", GmSearchCriteria.EQUALS);
		}
		if (!gmFieldSalesRptForm.getDevOpr().equals("0")) {
			gmSearchCriteria.addSearchCriteria("DEVIATION_QTY", "" + gmFieldSalesRptForm.getDevQty(), gmFieldSalesRptForm.getDevOpr());

		}
		if (gmFieldSalesRptForm.isDispNonflagged() == true) {
			gmSearchCriteria.addSearchCriteria("DISP_NONFLAGGED", "1", GmSearchCriteria.EQUALS);
		}
		gmSearchCriteria.query(lresult);

		return lresult;
	}
}
