package com.globus.accounts.actions;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.forms.GmAccountActivityForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.custservice.beans.GmSalesCreditBean;
import com.globus.party.beans.GmPartyInfoBean;

public class GmAccountActivityAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /*
   * The below method is used to load - Account activity reports
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmAccountActivityForm gmAccountActivityForm = (GmAccountActivityForm) form;
    gmAccountActivityForm.loadSessionParameters(request);
    GmSalesBean gmSalesBean = new GmSalesBean(getGmDataStoreVO());
    GmSalesCreditBean gmSalesCredit = new GmSalesCreditBean(getGmDataStoreVO());
    GmCalenderOperations gmCal = new GmCalenderOperations();
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
    GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());

    HashMap hmParam = new HashMap();
    HashMap hmJasper = new HashMap();
    HashMap hmParams = new HashMap();
    ArrayList alAccount = new ArrayList();
    ArrayList alReturn = new ArrayList();
    ArrayList alCompCurrency = new ArrayList();

    String strOpt = "";
    String strInvoTypes = "";
    String strAccountIds = "";
    String strInvSource = "";
    String strApplDateFmt = "";
    String strTodayDt = "";
    int currMonth = 0;
    String[] strInvoiceTypeArr = null;
    String[] strAccountArr = null;
    String strUserId = "";
    String strRuleValue = "";
    String strAccountType = "";
    String strARCurrSymbol = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));

    hmParam = GmCommonClass.getHashMapFromForm(gmAccountActivityForm);
    HashMap hmCurrency = new HashMap();

    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    String strSessApplCurrFmt = (String) hmCurrency.get("CMPCURRFMT");
    String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    alCompCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());
    strARCurrSymbol = GmCommonClass.parseZero(gmAccountActivityForm.getStrCompCurrency());
    strARCurrSymbol = strARCurrSymbol.equals("0") ? strCompanyCurrId : strARCurrSymbol;
    hmParam.put("ARCURRENCY", strARCurrSymbol);


    // Locale
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);

    strRuleValue =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strUserId, "ACCESS_LEFT_LINK"));
    // to avoid the left link access for the particular user
    if (strRuleValue.equals("N")) {
      throw new AppError("", "20690");
    }

    gmAccountActivityForm.setAlInvSource(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("INVSR", getGmDataStoreVO())));
    gmAccountActivityForm.setAlInvType(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("IVCTP")));
    gmAccountActivityForm.setAlStatus(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("REQST")));
    gmAccountActivityForm.setAlCompCurrency(alCompCurrency);
    gmAccountActivityForm.setStrCompCurrency(strARCurrSymbol);

    strInvoiceTypeArr = request.getParameterValues("Chk_GrpChk_Invoice_Type");
    strAccountArr = request.getParameterValues("Chk_GrpChk_Account");

    if (strInvoiceTypeArr != null && !strInvoiceTypeArr.equals("")) {
      strInvoTypes = GmCommonClass.parseNull(GmCommonClass.createInputString(strInvoiceTypeArr));
    }
    if (strAccountArr != null && !strAccountArr.equals("")) {
      strAccountIds = GmCommonClass.parseNull(GmCommonClass.createInputString(strAccountArr));
    }
    // to set the company date format
    hmParam.put("APPLNDATEFMT", getGmDataStoreVO().getCmpdfmt());
    strInvSource = GmCommonClass.parseNull((String) hmParam.get("INVSOURCE"));
    if(strInvSource.equals("")){
    strInvSource =
  		  GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE", "AR_DEFAULT_TYPE",
            getGmDataStoreVO().getCmpid()));
    hmParam.put("INVSOURCE", strInvSource);
    
    }
    strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    strTodayDt = GmCommonClass.parseNull((String) hmParam.get("TODT"));
    currMonth = gmCal.getCurrentMonth() - 1;
    String firstDayofMonth = "";
    firstDayofMonth = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));

    if (firstDayofMonth.equals("")) {
      firstDayofMonth = gmCal.getAnyDateOfMonth(currMonth, 1, strApplDateFmt);
    }
    if (strTodayDt.equals("")) {
      strTodayDt = GmCommonClass.parseNull(gmCal.getCurrentDate(strApplDateFmt));;
    }

    strInvSource = strInvSource.equals("") ? "50255" : strInvSource;
    // 50256: Inter-Company Transfer; 50255: US customers - need to get account type from rules
    // table
    strAccountType =
        GmCommonClass.parseNull(gmResourceBundleBean
            .getProperty("ACCOUNT_ACTIVITY." + strInvSource));

    if (strOpt.equals("Load") || strOpt.equals("Reload")) {

      gmAccountActivityForm.setStrAccountName("");
      if (strInvSource.equals("50255") || strInvSource.equals("")) {// US
        // 70110 - Hospital-US : 70111 - Hospital-International
        alAccount = gmSalesBean.reportAccountByType(strAccountType);   
      }
      if (strInvSource.equals("50256")) {// ICT
        hmParam.put("DISTTYP", "70103"); // 70103 - Inter-Company Transfer (ICT)
        alAccount = gmCustomerBean.getDistributorList(hmParam);
      } else if (strInvSource.equals("50257"))// SWISS/INDIA
      {
        alAccount = gmSalesBean.reportAccountByType(strAccountType);
      } else if (strInvSource.equals("50253")) // Inter-Company Sale
      {
        // The Changes made for PMT-834.In Account Dropdown for Type ICS Accounts are listed in
        // dropdown instead of distributor list.
        alAccount = gmSalesBean.reportAccountByType(strAccountType);
      }
      else if (strInvSource.equals("26240213") ) {// JP
    	  alAccount = gmPartyInfoBean.fetchPartyNameList("26230725", "");
        }
      gmAccountActivityForm.setAlAccount(alAccount);
      // default to check the Regular type
      strInvoTypes = strInvoTypes.equals("") ? "50200" : strInvoTypes;

      if (strOpt.equals("Reload")) {
        hmParam.put("ACCID", strAccountIds);
        hmParam.put("INVOTYP", strInvoTypes);
        hmParam.put("FORMAT", "ALL");
        hmParam.put("INVSOURCE", strInvSource);

        hmJasper = gmSalesCredit.fetchAccountActivityRpt(hmParam);
        alReturn = (ArrayList) hmJasper.get("ALRESULT");
      } // end reload
    } else if (strOpt.equals("Print")) {
      String strBillAddress = "";
      String strPayTerms = "";
      String strCompanyID = "";
      String strCurrSymbol = "";
      String strTotOsdAmt = "";
      String strLogoImage = "";
      String strDivisionId = "";
      Date dtFromDt = null;
      Date dtToDt = null;
      String strTypeSelected = GmCommonClass.parseNull((String) hmParam.get("TYPESELECTED"));
      strAccountIds = GmCommonClass.parseNull((String) hmParam.get("ACCSELECTED"));

      hmParam.put("ACCID", strAccountIds);
      hmParam.put("INVOTYP", strTypeSelected);

      hmJasper = gmSalesCredit.fetchAccountActivityRpt(hmParam);
      alReturn = (ArrayList) hmJasper.get("ALRESULT");
      HashMap hmJasperHeaderParams = (HashMap) hmJasper.get("HEADER");
      String strAccActJasName =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("ACCOUNT_ACTIVITY.ACCACTRPT"));

      if (alReturn.size() > 0) {
        strBillAddress = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("BILLADD"));
        String strIcsAddress =
            GmCommonClass.parseNull((String) hmJasperHeaderParams.get("ICSCOMPADD"));
        strPayTerms = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("PAYTERM"));
        strCompanyID = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("COMPANYID"));
        strDivisionId = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("DIVISION_ID"));
        strTotOsdAmt = GmCommonClass.parseZero((String) hmJasperHeaderParams.get("TOTOSDAMT"));
        strCurrSymbol = GmCommonClass.parseNull((String) hmJasperHeaderParams.get("CURRSYMBOL"));
        dtFromDt = GmCommonClass.getStringToDate((String) hmParam.get("FROMDT"), strApplDateFmt);
        dtToDt = GmCommonClass.getStringToDate((String) hmParam.get("TODT"), strApplDateFmt);

        String strCompanyName = "";
        String strCompanyAddress = "";
        String strCompanyPhone = "";
        String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");

        HashMap hmCompanyAddress = new HashMap();
        strDivisionId = strDivisionId.equals("") ? "2000" : strDivisionId;
        if (!strDivisionId.equals("")) {
          hmCompanyAddress =
              GmCommonClass.fetchCompanyAddress(getGmDataStoreVO().getCmpid(), strDivisionId);
          strCompanyName = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPNAME"));
          strCompanyAddress = GmCommonClass.parseNull((String) hmCompanyAddress.get("COMPADDRESS"));
          strCompanyPhone = GmCommonClass.parseNull((String) hmCompanyAddress.get("PH"));
          strLogoImage = GmCommonClass.parseNull((String) hmCompanyAddress.get("LOGO"));
          hmJasperHeaderParams.put("LOGOIMAGE", strImagePath + strLogoImage + ".gif");
        }


        if (strInvSource.equals("50253")) {
          strCompanyAddress = strIcsAddress;
          strCompanyName = "";
        } else {
          strCompanyAddress = strCompanyAddress + "\n" + strCompanyPhone;
        }

        hmJasperHeaderParams.put("COMPNAME", strCompanyName);
        hmJasperHeaderParams.put("HEADERADDRESS", strCompanyAddress);
        hmJasperHeaderParams.put("COMPADDRESS", strCompanyAddress);
        hmJasperHeaderParams.put("TOTOSDAMT", Double.parseDouble(strTotOsdAmt));
        hmJasperHeaderParams.put("FRMDATE", dtFromDt);
        hmJasperHeaderParams.put("TODATE", dtToDt);
        hmJasperHeaderParams.put("SESSIONDTFMT", strApplDateFmt);
        hmJasperHeaderParams.put("ACCID", strAccountIds);
        hmJasperHeaderParams.put("INVSOURCE", strInvSource);
        gmAccountActivityForm.setHmJasperDetails(hmJasperHeaderParams);
        gmAccountActivityForm.setAlReturn(alReturn);
        gmAccountActivityForm.setStrJasRptName(strAccActJasName);
      }
      return mapping.findForward("GmAccountActivityReportPrint");
    }
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    log.debug("alReturn size --> " + alReturn.size());
    String strxml = generateOutPut(hmParam, alReturn);
    gmAccountActivityForm.setGridData(strxml);
    gmAccountActivityForm.setAccSelected(strAccountIds);
    gmAccountActivityForm.setTypeSelected(strInvoTypes);
    gmAccountActivityForm.setToDt(strTodayDt);
    gmAccountActivityForm.setFromDt(firstDayofMonth);
    gmAccountActivityForm.setInvSource(strInvSource);
    return mapping.findForward("GmAccountActivityRpt");
  }

  private String generateOutPut(HashMap hmParam, ArrayList alReturn) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alReturn", alReturn);
    templateUtil.setTemplateSubDir("accounts/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.accounts.GmAccountActivityRpt", strSessCompanyLocale));
    templateUtil.setTemplateName("GmAccountActivityRpt.vm");
    return templateUtil.generateOutput();
  }
}
