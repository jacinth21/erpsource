package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAPBean;
import com.globus.accounts.forms.GmInvoiceDHRDtlsForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.beans.GmVendorBean;

public class GmInvoiceDHRDtlsAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      GmInvoiceDHRDtlsForm gmInvoiceDHRDtlsForm = (GmInvoiceDHRDtlsForm) form;
      gmInvoiceDHRDtlsForm.loadSessionParameters(request);
      GmAPBean gmAPBean = new GmAPBean(getGmDataStoreVO());
      GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());
      GmPurchaseBean gmPurchaseBean = new GmPurchaseBean(getGmDataStoreVO());

      Logger log = GmLogger.getInstance(this.getClass().getName());

      String strOpt = gmInvoiceDHRDtlsForm.getStrOpt();
      RowSetDynaClass rdInvoiceDHRRpt = null;
      List alResult = new ArrayList();

      HashMap hmParam = new HashMap();

      log.debug(" Testing. ..... ");
      hmParam = GmCommonClass.getHashMapFromForm(gmInvoiceDHRDtlsForm);


      if (strOpt.equals("reload")) {
        gmPurchaseBean.validatePOID(gmInvoiceDHRDtlsForm.getPoID());
        rdInvoiceDHRRpt = gmAPBean.fetchInvoiceDHRDtls(hmParam);
        alResult = rdInvoiceDHRRpt.getRows();
        log.debug(" rdInvoiceDHRRpt.getRows(). ..... " + rdInvoiceDHRRpt.getRows().size());

        gmInvoiceDHRDtlsForm.setReturnList(alResult);

      }
      HashMap hmTemp = gmVendorBean.getVendorList(new HashMap());
      gmInvoiceDHRDtlsForm.setAlVendorList((ArrayList) hmTemp.get("VENDORLIST"));
      gmInvoiceDHRDtlsForm.setAlInvoiceType(GmCommonClass.getCodeList("INVST"));
      gmInvoiceDHRDtlsForm.setAlInvoiceOper(GmCommonClass.getCodeList("CPRSN"));
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

}
