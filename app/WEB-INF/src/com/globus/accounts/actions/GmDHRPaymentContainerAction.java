/**
 * Description : This action class is used for fetching AP Invoice List report content and PO
 * details Copyright : Globus Medical Inc
 * 
 */
package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAPBean;
import com.globus.accounts.forms.GmAPSaveInvoiceDtlsForm;
import com.globus.accounts.forms.GmDHRPaymentContainerForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.beans.GmPurchaseBean;

/**
 * @author bvidyasankar
 * 
 */
public class GmDHRPaymentContainerAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
    String strOpt = "";
    
      instantiate(request, response);
      GmDHRPaymentContainerForm gmAPSaveInvoiceDtlsForm = (GmAPSaveInvoiceDtlsForm) form;
      gmAPSaveInvoiceDtlsForm.loadSessionParameters(request);
      GmAPBean gmAPBean = new GmAPBean(getGmDataStoreVO());
      GmPurchaseBean gmPurchaseBean = new GmPurchaseBean(getGmDataStoreVO());// To get PO Details
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
      // Getting Party Id

      strOpt = gmAPSaveInvoiceDtlsForm.getStrOpt();
      String strPartyId = gmAPSaveInvoiceDtlsForm.getSessPartyId();
      String accessFlag = "";

      if (strOpt.equals("") || strOpt.equalsIgnoreCase("invcGo")) {
        strOpt = "default";
      }
      List lResult = new ArrayList();
      HashMap hmPOInfo = new HashMap();
      HashMap hmPODetails = new HashMap();
      HashMap hmAccess = new HashMap();

      HashMap hmParam;

      Logger log = GmLogger.getInstance(this.getClass().getName());
      // When "Go" is hit from container, PO number will be populated in containerPONumber
      String strPONumber = gmAPSaveInvoiceDtlsForm.getHpoNumber();
      String strContainerPONumber = gmAPSaveInvoiceDtlsForm.getContainerPONumber();
      String strPaymentId = gmAPSaveInvoiceDtlsForm.getHpaymentId();
      log.debug(" strOpt " + strOpt);
      log.debug(" PONumber " + strPONumber);
      log.debug(" strContainerPONumber " + strContainerPONumber);
      gmPurchaseBean.validatePOID(strContainerPONumber);
      hmParam = GmCommonClass.getHashMapFromForm(gmAPSaveInvoiceDtlsForm);
      log.debug(" values inside hmParam " + hmParam);

      if (strOpt.equalsIgnoreCase("loadInvc")) {
        lResult = gmAPBean.getInvoiceList(hmParam).getRows();
        log.debug("1@............lResult.." + lResult);
        gmAPSaveInvoiceDtlsForm.setLdtResult(lResult);
      }

      // DHR List is loaded in order to display Vendor details like name and currency
      // invcGo will be set when called from GmReleaseInvoicePayment on Invoice icon click
      if (strOpt.equalsIgnoreCase("default")) {

        if (!strContainerPONumber.equals("") && !strContainerPONumber.equals("GM-PO-")) {
        	// PMT-44785: AP Invoice entry - PO not loading
        	// We need only PO details so calling the new method viewPODetails and get the PO details.
          hmPOInfo = gmPurchaseBean.viewPODetails(strContainerPONumber);


          hmPODetails = GmCommonClass.parseNullHashMap((HashMap) hmPOInfo.get("PODETAILS"));

          if (hmPODetails.size() > 0) {
            String strCurrency = GmCommonClass.parseNull((String) hmPODetails.get("CURR"));
            if (!strCurrency.equals(""))
              strCurrency = GmCommonClass.getCodeAltName((String) hmPODetails.get("CURR"));
            log.debug("Currency: " + strCurrency);
            hmPODetails.put("CURR", strCurrency);
            gmAPSaveInvoiceDtlsForm.setHmpurchaseOrdDtls(hmPODetails);
            strContainerPONumber = GmCommonClass.parseNull((String) hmPODetails.get("POID"));
          }
        }
        // code for access rights
        hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "AP_NEW_INVOICE"); // PMT-51299-->A/P New Invoice Access
        accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
        log.debug("Permissions = " + hmAccess);
        if (accessFlag.equals("Y")) {
          gmAPSaveInvoiceDtlsForm.setStrEnableNewInvoice("true");
        }
        gmAPSaveInvoiceDtlsForm.setHpoNumber(strContainerPONumber);
      } else
        gmAPSaveInvoiceDtlsForm.setHpoNumber(strPONumber);

      gmAPSaveInvoiceDtlsForm.setContainerPONumber(strContainerPONumber);
      if (strOpt.equalsIgnoreCase("NewInvc"))
        gmAPSaveInvoiceDtlsForm.setHpaymentId("");
      else
        gmAPSaveInvoiceDtlsForm.setHpaymentId(strPaymentId);

      if (gmAPSaveInvoiceDtlsForm.getStrOpt().equalsIgnoreCase("invcGo"))
        gmAPSaveInvoiceDtlsForm.setStrOpt("invcGo");
      else
        gmAPSaveInvoiceDtlsForm.setStrOpt("newInvc");

      if (strOpt.equalsIgnoreCase("loadInvc"))
        strOpt = "invoice";

      log.debug("Forwarding to = >" + strOpt + "<");

    
    return mapping.findForward(strOpt);
  }

}
