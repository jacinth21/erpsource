package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAuditVerifyBean;
import com.globus.accounts.forms.GmAuditVerifyForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmAuditVerifyAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    GmAuditVerifyForm gmAuditVerifyForm = (GmAuditVerifyForm) form;

    gmAuditVerifyForm.loadSessionParameters(request);

    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmAuditVerifyBean gmAuditVerifyBean = new GmAuditVerifyBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    HashMap hmAuditEntryDetails = new HashMap();
    HashMap hmcboDetails = new HashMap();
    HashMap hmFormValues = new HashMap();
    ArrayList alLocDetails = new ArrayList();
    String strOpt = gmAuditVerifyForm.getStrOpt();
    String strDistID = gmAuditVerifyForm.getDistID();

    log.debug("strOpt" + strOpt);
    log.debug("strDistID " + strDistID);
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    // GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean();

    // To Get the Set List
    ArrayList alSet = gmProjectBean.loadSetMap("1601");
    gmAuditVerifyForm.setAlSet(alSet);

    // To get the Status List
    ArrayList alStatus = GmCommonClass.getCodeList("SETST");
    gmAuditVerifyForm.setAlStatus(alStatus);

    // To get Location Type
    ArrayList alLocationType = GmCommonClass.getCodeList("SHIP");
    gmAuditVerifyForm.setAlLocation(alLocationType);

    // To get Location details

    ArrayList alAccList = new ArrayList();
    alAccList = gmSales.reportAccount();

    ArrayList alDistList = new ArrayList();
    alDistList = gmCustomerBean.getDistributorList("Active");

    ArrayList alAddresses = new ArrayList();
    alAddresses = gmAuditVerifyBean.fetchRepAddresses(strDistID);

    hmcboDetails.put("ACCLIST", alAccList);
    hmcboDetails.put("DISTLIST", alDistList);
    hmcboDetails.put("REPLIST", alAddresses);
    // hmcboDetails = gmShippingInfoBean.loadShipping();
    // hmcboDetails = (HashMap) hmcboDetails.get("NAMES");
    request.setAttribute("NAMES", hmcboDetails);

    // To get the Distributor List
    ArrayList alDistributor = gmCustomerBean.getDistributorList();
    gmAuditVerifyForm.setAlBorrfrm(alDistributor);

    if (strOpt.equals("update_audit_tag")) {

      hmFormValues = GmCommonClass.getHashMapFromForm(gmAuditVerifyForm);
      gmAuditVerifyBean.saveAuditEntry(hmFormValues);

      strOpt = "onload_edit_audit_tag";
      request.setAttribute("RESPONSE", "Data Saved Successfully");
    }

    if (strOpt.equals("onload_edit_audit_tag")) {

      String strAuditEntryID = gmAuditVerifyForm.getAuditEntryID();

      hmAuditEntryDetails = gmAuditVerifyBean.fetchAuditEntryByID(strAuditEntryID);
      gmAuditVerifyForm.setHmAuditEntryDetails(hmAuditEntryDetails);
      gmAuditVerifyForm =
          (GmAuditVerifyForm) GmCommonClass.getFormFromHashMap(gmAuditVerifyForm,
              hmAuditEntryDetails);

      String locType = gmAuditVerifyForm.getLoctyp();

      if (locType.equalsIgnoreCase("Hospital")) {
        alLocDetails = (ArrayList) hmcboDetails.get("ACCLIST");
      } else if (locType.equalsIgnoreCase("Distributor")) {
        alLocDetails = (ArrayList) hmcboDetails.get("DISTLIST");
      } else if (locType.equalsIgnoreCase("Sales Rep")) {
        alLocDetails = (ArrayList) hmcboDetails.get("REPLIST");
      }
      gmAuditVerifyForm.setAlLocDetails(alLocDetails);
      log.debug(" Borrowed From :" + alLocDetails + gmAuditVerifyForm.getBorrowedFrom());
      log.debug(" Borrowed From :" + gmAuditVerifyForm.getLocationID());
      log.debug(" ArrayList Returned is " + alLocDetails);
    }


    return mapping.findForward("GmEditAuditTag");
  }
}
