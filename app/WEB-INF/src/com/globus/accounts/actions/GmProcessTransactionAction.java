package com.globus.accounts.actions;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.batch.beans.GmARBatchBean;
import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.forms.GmProcessTransactionForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.corporate.beans.GmCompanyBean;


public class GmProcessTransactionAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    instantiate(request, response);
    GmProcessTransactionForm gmProcessTransactionForm = (GmProcessTransactionForm) form;
    gmProcessTransactionForm.loadSessionParameters(request);
    Logger log = GmLogger.getInstance(this.getClass().getName());
    HttpSession session = request.getSession(false);

    ArrayList alSource = new ArrayList();
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmDetails = new HashMap();
    GmARBean gmARBean = new GmARBean(getGmDataStoreVO());
    GmARBatchBean gmARBatchBean = new GmARBatchBean(getGmDataStoreVO());
    GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
    String strInvoiceId = "";
    String strURL = "";
    String strAccountID = "";
    String strCustPO = "";
    String strBatchID = "";
    String strUserID = "";
    String strAction = "";
    String strCompanyInfo = "";
    String strAccCurrName = "";
    String strAccountCurrId = "";
    String strCompanyCurrId = "";
    HashMap hmCurrency = new HashMap();
    ArrayList alCompanyCurrency = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmProcessTransactionForm);
    // to redirect to process transaction home when app error occur
    strAction = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
    strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strRefID = GmCommonClass.parseNull((String) hmParam.get("STRREFID"));
    String strSource = GmCommonClass.parseNull((String) hmParam.get("STRSOURCE"));
    // Getting company currency symbol from currency dropdown.
    alCompanyCurrency = GmCommonClass.parseNullArrayList(gmCompanyBean.fetchCompCurrMap());
    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    strCompanyCurrId = GmCommonClass.parseNull((String) hmCurrency.get("TXNCURRENCYID"));
    log.debug(" strCompanyCurrId --> " + strCompanyCurrId);
    strAccountCurrId = GmCommonClass.parseZero((String) hmParam.get("STRCOMPCURRENCY"));
    strAccountCurrId = strAccountCurrId.equals("0") ? strCompanyCurrId : strAccountCurrId;
    log.debug(" strAccountCurrId --> " + strAccountCurrId);
    strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrId);
    strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
    strCompanyInfo = URLEncoder.encode(strCompanyInfo);
    String strDisUrl =
        "/gmProcessTrans.do?strSource=" + strSource + "&haction=" + strAction + "&companyInfo="
            + strCompanyInfo + "&strCompCurrency=" + strAccountCurrId;
    request.setAttribute("hRedirectURL", strDisUrl);

    // Get source for Inventory Transaction ArrayList
    alSource = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("INVTRN"));

    String strOpt = gmProcessTransactionForm.getStrOpt();
    if (strAction.equals("Batch")) {
      // to load only Order id in Source drop down.
      alSource = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("INVTRN", "1006653")); // 1006653
                                                                                                   // -
                                                                                                   // Order
                                                                                                   // ID
    }
    if (strAction.equals("Batch") && strOpt.equals("")) { // Via batch then default source set -
                                                          // Order id
      // 1006653 - Order ID -- 1006651 - Customer PO -- 1006652 - Invoice ID
      strSource = "1006653"; // Order ID
      gmProcessTransactionForm.setStrSource(strSource);
    }
    if (strOpt.equals("Reload")) {
      if (strAction.equals("Batch") && strSource.equals("1006653")) {
        hmDetails = gmARBatchBean.fetchOrderBarcodeDtls(hmParam);
        strAccountID = GmCommonClass.parseNull((String) hmDetails.get("ACCID"));
        strCustPO = GmCommonClass.parseNull((String) hmDetails.get("PO"));
        strInvoiceId = GmCommonClass.parseNull((String) hmDetails.get("INVID"));
        strBatchID = GmCommonClass.parseNull((String) hmDetails.get("BATCHID"));
        // call to Generate Invoice screen.
        strURL =
            "\\GmInvoiceServlet?hAction=INV&hAddBatch=Y&hOpt=LOAD&hId=" + strAccountID + "&hAccId="
                + strAccountID + "&hPO=" + strCustPO + "&hBatchID=" + strBatchID + "&companyInfo="
                + strCompanyInfo + "&Cbo_Comp_Curr=" + strAccountCurrId;
      } else { // existing flow
        strInvoiceId = GmCommonClass.parseNull(gmARBean.fetchInvoiceId(hmParam));
        strURL =
            "\\GmInvoiceServlet?hAction=PAY&hInv=" + strInvoiceId + "&companyInfo="
                + strCompanyInfo + "&Cbo_Comp_Curr=" + strAccountCurrId;
      }
      log.debug("strURL:::" + strURL + "::strInvoiceId::" + strInvoiceId);
      gmProcessTransactionForm.setStrURL(strURL);
    }
    // to set the Source drop down values
    gmProcessTransactionForm.setAlSource(alSource);
    gmProcessTransactionForm.setStrCompCurrency(strAccountCurrId);
    gmProcessTransactionForm.setAlCompCurrency(alCompanyCurrency);
    return mapping.findForward("GmProcessTrans");
  }
}
