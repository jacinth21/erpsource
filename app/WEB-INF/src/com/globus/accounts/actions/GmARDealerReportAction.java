package com.globus.accounts.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.util.GmTemplateUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.it.beans.GmCodeGroupBean;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.accounts.beans.GmARDealerReportBean;
import com.globus.accounts.forms.GmARDealerReportForm;
import javax.servlet.http.HttpSession;
import com.globus.sales.actions.GmSalesDispatchAction;

/**
 * @author JBalaraman
 * 
 */
public class GmARDealerReportAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**
	 * @author JBalaraman
	 * Method:loadFilters
	 */
	public ActionForward loadFilters(ActionMapping mapping, ActionForm form, 
			HttpServletRequest request, HttpServletResponse response) throws AppError,Exception
    {   	
	    HttpSession session = request.getSession(false);
	    String strComnPath = GmCommonClass.getString("GMCOMMON");
	    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
	    String strAction = "";
	    String strAccountID = "";
	    String strReportType = "";
	    String strFilterCondition = "";
	    String strAccountTpe = "";
	    String strInvSource = "";
	      checkSession(response, session); // Checks if the current session is valid, else redirecting
	      instantiate(request, response);
	      
	      GmARDealerReportForm gmARDealerReportForm = (GmARDealerReportForm) form;
	      strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
	      strAction = strAction.equals("") ? "LoadAR" : strAction;
	      strReportType = GmCommonClass.parseNull(request.getParameter("reportType"));
	      String strCondition = null;
	      //RowSetDynaClass resultSet = null;
	      HashMap hmParam = new HashMap();
	      HashMap hmReturn = new HashMap();
	      ArrayList alType = new ArrayList();
	      ArrayList alResult = new ArrayList();
	      ArrayList alCompanyCurrency = new ArrayList();
	      ArrayList alCompDivList = new ArrayList();
	      HashMap hmCurrency = new HashMap();
	      String strGridFile = "GmARDealerReport.vm";
	      String strSessCompanyLocale =
	          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

	      GmARDealerReportBean gmARDealer = new GmARDealerReportBean(getGmDataStoreVO());
	      GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
	      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
	      GmCompanyBean gmCompanyBean = new GmCompanyBean(getGmDataStoreVO());
	      GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());

	      hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());

	      String strCompanyLocale =
	          GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));

	      GmResourceBundleBean gmResourceBundleBean =
	          GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
	      // If the Dateformat from the VO is null, take it from the Session
	      String strSessApplCurrFmt = (String) hmCurrency.get("CMPCURRFMT");
	      String strCompanyCurrId = (String) hmCurrency.get("TXNCURRENCYID");
	      String strAccCurrName = "";

	      String strAccId = "";
	      alType = GmCommonClass.getCodeList("INVSR", getGmDataStoreVO());
	      request.setAttribute("ALTYPE", alType);
	      String strDivisionId = GmCommonClass.parseZero(request.getParameter("Cbo_Division"));
	      
	      // fetching division dropdown values based on company
	      alCompDivList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmReturn));
	      request.setAttribute("ALCOMPDIV", alCompDivList);
	      request.setAttribute("DIVISIONID", strDivisionId);
	      GmARDealerReportBean gmARDealerBean = new GmARDealerReportBean(getGmDataStoreVO());
	      alCompanyCurrency = gmCompanyBean.fetchCompCurrMap();
	      hmReturn.put("ALCOMPANYCURRENCY", alCompanyCurrency);
	      GmSalesDispatchAction gmSalesDispatchAction = new GmSalesDispatchAction();
	      HashMap hmSalesFilters = new HashMap();
	      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, false);
	      hmSalesFilters = gmCommonBean.getSalesFilterLists(strFilterCondition);
	      strFilterCondition = gmSalesDispatchAction.getAccessFilter(request, response, true);
	      request.setAttribute("hmSalesFilters", hmSalesFilters);
	      
	      if (request.getParameter("Cbo_InvSource") == null) {
	        strInvSource =
	            GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE", "AR_DEFAULT_TYPE",
	                getGmDataStoreVO().getCmpid());
	      }
	      strInvSource =
	          request.getParameter("Cbo_InvSource") == null ? "26240213" : request
	              .getParameter("Cbo_InvSource");
	      String strIdAccount = request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
	      String strRegion =
	          request.getParameter("hRegnFilter") == null ? "" : request.getParameter("hRegnFilter");
	      String strDist =
	          request.getParameter("hDistFilter") == null ? "" : request.getParameter("hDistFilter");
	      String strDistUnChk =
	          request.getParameter("hDistFilterUnChk") == null ? "" : request
	              .getParameter("hDistFilterUnChk");
	      String strAccountCurrency = GmCommonClass.parseZero(request.getParameter("Cbo_Comp_Curr"));
	      strAccountCurrency = strAccountCurrency.equals("0") ? strCompanyCurrId : strAccountCurrency;
	      strAccId = request.getParameter("Cbo_AccId") == null ? "" : request.getParameter("Cbo_AccId");
	      
	      if (request.getParameter("Cbo_InvSource") == null) {
	        strInvSource =
	            GmCommonClass.getRuleValueByCompany("CUSTOMER_DEF_TYPE", "AR_DEFAULT_TYPE",
	                getGmDataStoreVO().getCmpid());
	      }
	      String strSearchAccName = GmCommonClass.parseNull(request.getParameter("searchCbo_AccId"));
	      request.setAttribute("hmReturn", hmReturn);

	      if (strAction.equals("LoadAR") || strAction.equals("ReLoadAR")) {
	    	if (strAction.equals("ReLoadAR")) {
	          gmARDealerReportForm.setStrDivisionId(strDivisionId);
	          gmARDealerReportForm.setStrAction(strAction);
	          gmARDealerReportForm.setStrReportType(strReportType);
	          gmARDealerReportForm.setStrAccId(strAccId);
	          gmARDealerReportForm.setStrFilterCondition(strFilterCondition);
	  	      gmARDealerReportForm.setStrInvSource(strInvSource);
		      gmARDealerReportForm.setStrIdAccount(strIdAccount);
		      gmARDealerReportForm.setStrRegion(strRegion);
		      gmARDealerReportForm.setStrRegion(strRegion);
		      gmARDealerReportForm.setStrDistUnChk(strDistUnChk);
		      gmARDealerReportForm.setStrAccountCurrency(strAccountCurrency);
		      gmARDealerReportForm.setStrSearchAccName(strSearchAccName);
		      hmParam = GmCommonClass.getHashMapFromForm(gmARDealerReportForm);
	          alResult = gmARDealer.fetchSalesReport(hmParam);
	          
	          GmTemplateUtil templateUtil = new GmTemplateUtil();
	          templateUtil.setDataList("alResult", alResult);
	          strAccCurrName = GmCommonClass.getCodeNameFromCodeId(strAccountCurrency);
	          templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
	              "properties.labels.accounts.GmARDealerReport", strSessCompanyLocale));
	          templateUtil.setTemplateName(strGridFile);
	          templateUtil.setTemplateSubDir("accounts/templates");
	          request.setAttribute("alResult",
	              GmCommonClass.replaceForXML(templateUtil.generateOutput()));
	        }
	        request.setAttribute("INVSOURCE", strInvSource);
	        request.setAttribute("IDACCOUNT", strIdAccount);
	        request.setAttribute("hAccId", strAccId);
	        request.setAttribute("hRegnFilter", strRegion);
	        request.setAttribute("hDistFilter", strDist);
	        request.setAttribute("hDistFilterUnChk", strDistUnChk);
	        request.setAttribute("REPORTTYPE", strReportType);
	      }
	      request.setAttribute("ALCOMPANYCURRENCY", alCompanyCurrency);
	      request.setAttribute("ACC_CURR_ID", strAccountCurrency);
	      request.setAttribute("ACC_CURR_SYMB", strAccCurrName);
	      request.setAttribute("SEARCH_ACC_NAME", strSearchAccName);
	      return mapping.findForward("gmARDealerRpt");
    }
	
}
