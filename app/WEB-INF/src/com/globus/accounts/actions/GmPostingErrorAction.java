package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAuditReconciliationBean;
import com.globus.accounts.beans.GmFieldSalesAuditBean;
import com.globus.accounts.forms.GmFieldSalesRptForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmCustomerBean;

public class GmPostingErrorAction extends GmAction {


  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code

    GmFieldSalesRptForm gmFieldSalesRptForm = (GmFieldSalesRptForm) form;
    gmFieldSalesRptForm.loadSessionParameters(request);
    GmAuditReconciliationBean gmAuditReconciliationBean =
        new GmAuditReconciliationBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean(getGmDataStoreVO());

    HashMap hmParam = GmCommonClass.getHashMapFromForm(gmFieldSalesRptForm);
    ArrayList alDistList = new ArrayList();
    ArrayList alAuditList = new ArrayList();
    List ldtResult = new ArrayList();
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));

    alAuditList = gmFieldSalesAuditBean.fetchAuditDetails();
    log.debug(alAuditList);
    gmFieldSalesRptForm.setAlAuditName(alAuditList);

    alDistList = gmCustomerBean.getDistributorList("");
    log.debug(alDistList);
    gmFieldSalesRptForm.setAldist(alDistList);
    log.debug(gmFieldSalesRptForm.getAldist());

    if (strOpt.equals("load")) {
      log.debug("loadReport");
      RowSetDynaClass rdPostingError = gmAuditReconciliationBean.fetchPostingError(hmParam);
      ldtResult = rdPostingError.getRows();
      gmFieldSalesRptForm.setLdtResult(ldtResult);
    }



    return mapping.findForward("GmPostingError");
  }

}
