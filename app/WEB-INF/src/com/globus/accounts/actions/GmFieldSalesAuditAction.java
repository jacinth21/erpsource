package com.globus.accounts.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmFieldSalesAuditBean;
import com.globus.accounts.forms.GmFieldSalesAuditForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmFieldSalesAuditAction extends GmAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code
	GmFieldSalesAuditForm gmFieldSalesAuditForm = (GmFieldSalesAuditForm) form;
	gmFieldSalesAuditForm.loadSessionParameters(request);
	
	GmFieldSalesAuditBean gmFieldSalesAuditBean = new GmFieldSalesAuditBean(); 
	
	ArrayList alcboAuditName= new ArrayList();
	ArrayList alDistName= new ArrayList();
	ArrayList alAuditor= new ArrayList();
	String strMessage="";
	HashMap hmParam = new HashMap();
	String strOpt = gmFieldSalesAuditForm.getStrOpt();
	String forwardTo= "GmLockLoad";
	log.debug("strOpt"+strOpt);
	alcboAuditName = gmFieldSalesAuditBean.fetchAuditDetails();
	String strDistributorID = "";
	
	gmFieldSalesAuditForm.setAlAuditName(alcboAuditName);
	hmParam = GmCommonClass.getHashMapFromForm(gmFieldSalesAuditForm);
	String strAuditId=GmCommonClass.parseNull((String) hmParam.get("AUDITID"));
	
	if(strOpt.equals("reload")){
		alDistName=gmFieldSalesAuditBean.fetchDistributorDetails(strAuditId);
		gmFieldSalesAuditForm.setAldist(alDistName);
		
		alAuditor=gmFieldSalesAuditBean.fetchAuditorName(strAuditId);
		gmFieldSalesAuditForm.setAlAuditor(alAuditor);
	}
	if(strOpt.equals("load_lock_report")){		
		strDistributorID = (String) hmParam.get("DISTID");
		gmFieldSalesAuditForm.setLdtAuditLockReport(gmFieldSalesAuditBean.fetchAuditLockReport(strAuditId,strDistributorID).getRows());	
		strOpt = "loadDistributor";
	}
	if (strOpt.equals("loadDistributor")) {
		alDistName=gmFieldSalesAuditBean.fetchLockedDistributorDetails(strAuditId);
		gmFieldSalesAuditForm.setAldist(alDistName);
	}
	log.debug("hmParam"+hmParam);
	
	if(strOpt.equals("on_load_report")||strOpt.equals("load_lock_report")|| strOpt.equals("loadDistributor")){
		forwardTo="GmFieldSalesAuditReport";
	}
	
	if (strOpt.equals("save"))
	{
		gmFieldSalesAuditBean.loadAuditDetails(hmParam);
		//return mapping.findForward("GmTagLoked");
		strMessage="Field Sales Audit Lock is successful";
		throw new AppError(strMessage, "", 'S');
	}
	return mapping.findForward(forwardTo);
}
}

