package com.globus.accounts.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmCogsBean;
import com.globus.accounts.forms.GmCogsForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmCogsAction extends GmAction {
  // private String strOpt = "LOAD / RELOAD";

  /**
   * @roseuid 48D03A8F0195
   */

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);

    Logger log = GmLogger.getInstance(this.getClass().getName());
    try {

      GmCogsForm gmCogsForm = (GmCogsForm) form;
      GmCogsBean gmCogsBean = new GmCogsBean(getGmDataStoreVO());
      GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();
      RowSetDynaClass rdResult = null;
      String strOpt = gmCogsForm.getStrOpt();
      log.debug(" stropt " + strOpt);

      // Audit -- fetch Inv list
      gmCogsForm.setInventoryList(gmCogsBean.fetchCogsLockReportLists("20431")); // physical
      // Audit

      HashMap hmTemp = new HashMap();
      hmTemp.put("COLUMN", "ID");
      hmTemp.put("STATUSID", "20301"); // Filter all approved projects
      gmCogsForm.setProjectList(gmProj.reportProject(hmTemp)); // fetch
      // Project
      // List

      if (strOpt.equals("ReloadLock")) {
        String strInvListId = gmCogsForm.getInventoryListID();
        String strProjId = gmCogsForm.getProjectListID();

        HashMap hmRegStr = new HashMap();
        HashMap hmParams = new HashMap();
        hmRegStr.put("PARTNUM", gmCogsForm.getPnum());
        hmRegStr.put("SEARCH", gmCogsForm.getPnumSuffix());
        String strPartNumFormat = GmCommonClass.createRegExpString(hmRegStr);
        log.debug("strInvListId " + strInvListId);
        log.debug("strProjId " + strProjId);
        log.debug("strPartNumFormat " + strPartNumFormat);

        hmParams.put("PARTNUM", strPartNumFormat);
        hmParams.put("PROJECTID", strProjId);
        hmParams.put("LOCKID", strInvListId);

        rdResult = gmCogsBean.fetchlockedCogsDetail(hmParams);
        gmCogsForm.setRptSummary(rdResult.getRows());

        log.debug("rows size " + rdResult.getRows().size());

      }
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("success");
  }
}
