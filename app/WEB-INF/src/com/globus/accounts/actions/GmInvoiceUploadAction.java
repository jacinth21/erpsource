package com.globus.accounts.actions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javazoom.upload.UploadException;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.globus.accounts.beans.GmARBean;
import com.globus.accounts.beans.GmInvoiceBean;
import com.globus.accounts.forms.GmInvoiceUploadForm;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;

public class GmInvoiceUploadAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    String strDispatchTo = "GmInvoiceUpload";
    String strUploadHome = GmCommonClass.getString("UPLOAD_INVOICE");
    String strOpt = "";
    String strInvoiceId = "";
    String strUserId = "";
    String strRefType = "90905";
    String strRedirectURL = "";
    HashMap hmReturn = new HashMap();
    HashMap hmOrderDetails = new HashMap();
    ArrayList alUploadinfo = new ArrayList();
    GmARBean gmARBean = new GmARBean(getGmDataStoreVO());
    GmInvoiceBean gmInvoiceBean = new GmInvoiceBean(getGmDataStoreVO());
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());

    GmInvoiceUploadForm gmInvoiceUploadForm = (GmInvoiceUploadForm) form;
    gmInvoiceUploadForm.loadSessionParameters(request);

    strOpt = gmInvoiceUploadForm.getStrOpt();
    strInvoiceId = gmInvoiceUploadForm.getInvoiceId();
    strUserId = gmInvoiceUploadForm.getUserId();

    log.debug("strOpt==>" + strOpt);
    log.debug("strInvoiceId==>" + strInvoiceId);
    if (strOpt.equals("upload")) {
      FormFile strFile = gmInvoiceUploadForm.getFile();
      String strFileName = strFile.getFileName();
      String fileExt = strFileName.substring(strFileName.lastIndexOf("."));
      String strFileNameToPopulate =
          strUploadHome + "Consolidate_" + strInvoiceId + "_Ver_1" + fileExt;
      strFileName = createCreateFileVersions(strFileNameToPopulate, strUploadHome, fileExt);

      if (!strFileName.equals("")) {
        validateFile(strFile);
        try {
          uploadFile(strFile, strUploadHome, strFileName);
        } catch (Exception e) {
          throw new AppError("", "20675");
        }
        strFileName = strFileName.substring(strFileName.lastIndexOf("\\") + 1);
        gmCommonUploadBean.saveUploadUniqueInfo(strInvoiceId, strRefType, strFileName, strUserId);
        gmInvoiceBean.saveInvoiceFileRef(strInvoiceId,strRefType,strUserId);
      }
      /* Redirect to same page to avoid refresh issue after file uploading */
      strRedirectURL = "/gmInvoiceUpload.do?strOpt=load&invoiceId=" + strInvoiceId;
      return actionRedirect(strRedirectURL, request);
    } else if (strOpt.equals("load")) {

      hmReturn = gmARBean.loadInvoiceDetails(strInvoiceId, "");
      alUploadinfo =
          GmCommonClass.parseNullArrayList(gmCommonUploadBean.fetchUploadInfo(strRefType,
              strInvoiceId));
      gmInvoiceUploadForm.setAlUploadinfo(alUploadinfo);
      hmOrderDetails = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("ORDERDETAILS"));
      gmInvoiceUploadForm.setHmOrderDetails(hmOrderDetails);
    }
    return mapping.findForward(strDispatchTo);
  }// End of execute method

  private void uploadFile(FormFile formFile, String strUploadHome, String strFileNameToPopulate)
      throws UploadException, IOException {
    FileOutputStream outputStream = null;
    File file = new File(strUploadHome);
    if (!file.exists()) {
      file.mkdir();
    }
    try {
      outputStream = new FileOutputStream(new File(strFileNameToPopulate));
      outputStream.write(formFile.getFileData());
    } finally {
      if (outputStream != null) {
        outputStream.close();
      }
    }
  }// End of uploadFile method

  private String createCreateFileVersions(String strFileNameToPopulate, String strDirs,
      String fileExt) {
    if (new File(strFileNameToPopulate).exists()) {

      File chkFile = new File(strDirs);
      String strFileList[] = chkFile.list();
      for (int j = 1; j <= strFileList.length; j++) {

        if (strFileNameToPopulate.endsWith(fileExt)) {
          String strTempFileName =
              strFileNameToPopulate.substring(0, strFileNameToPopulate.lastIndexOf("_Ver"))
                  + "_Ver_" + (j + 1) + fileExt;
          if (!new File(strTempFileName).exists()) {
            strFileNameToPopulate = strTempFileName;
            log.debug("strFileNameToPopulate:" + strFileNameToPopulate);
            break;

          }

        }
      }
    }
    return strFileNameToPopulate;
  }// End of createCreateFileVersions method

  private void validateFile(FormFile formFile) throws AppError {
    String strWhiteList = GmCommonClass.getString("UPLOAD_INVOICE_WHITELIST");
    String strFileName = formFile.getFileName();
    String strfileExt = strFileName.substring(strFileName.lastIndexOf("."));
    String strUploadFileExt = "*" + strfileExt;
    String strExtList[] = strWhiteList.split(",");
    int intMaxFileSize = Integer.parseInt(GmCommonClass.getString("MAXFILESIZE"));
    int intFileSize = formFile.getFileSize();
    boolean validFlag = false;

    for (int i = 0; i < strExtList.length; i++) {
      String strAllowedExt = strExtList[i];
      if (strAllowedExt.equalsIgnoreCase(strUploadFileExt)) {
        validFlag = true;
        break;
      }
    }
    if (!validFlag) {
      throw new AppError(
          "Upload not successful, as the file type(extension) is not supported.<br>Allowed extension(s):"
              + strWhiteList, "", 'E');
    } else if (intFileSize == 0) {
      throw new AppError("", "20680");
    } else if (intFileSize > intMaxFileSize) {
      throw new AppError("", "20681");
    }
  }// End of validateFile method
}
