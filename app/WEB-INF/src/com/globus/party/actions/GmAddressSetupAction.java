package com.globus.party.actions;

import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.tax.beans.GmSalesTaxRptBean;
import com.globus.accounts.tax.beans.GmSalesTaxTransBean;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.party.beans.GmAddressBean;
import com.globus.party.forms.GmAddressSetupForm;
import com.globus.valueobject.accounts.GmMessagesVO;
import com.globus.valueobject.accounts.GmValidShipAddressVo;
import com.globus.valueobject.accounts.GmValidateAddressVO;

public class GmAddressSetupAction extends GmDispatchAction {

  // Code to Initialize the Logger Class
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * This method is used to Save and Edit the address details for the party.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return ActionForward
   * @throws AppError
   * @throws Exception
   */


  public ActionForward loadAddressSetup(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmAddressSetupForm gmAddressSetupForm = (GmAddressSetupForm) form;
    GmAddressBean gmAddressBean = new GmAddressBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

    HashMap hmParam = new HashMap();
    HashMap hmValues = new HashMap();
    ArrayList alLogReasons = new ArrayList();
    String strAccessFl = "";
    String strSalesRepEditAcsFl = "";
    HashMap hmAccess = new HashMap();
    String strAddressId = gmAddressSetupForm.getHaddressID();
    String strPartyId = gmAddressSetupForm.getPartyId();
    String strActiveFlg = gmAddressSetupForm.getActiveFlag();
    String strhActiveFlag = gmAddressSetupForm.gethActiveFlag();
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    String strDealerNameENFlag =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOW_DEALER_NAME_EN"));
    gmAddressSetupForm.setDealerFlag(strDealerNameENFlag);
    String strPartyType =
        gmAddressSetupForm.getPartyType() == "" ? gmAddressSetupForm.getStrOpt()
            : gmAddressSetupForm.getPartyType();
    gmAddressSetupForm.setPartyType(strPartyType);
    // for areaset
    gmAddressSetupForm.setRefID(gmAddressSetupForm.getRefID());
    gmAddressSetupForm.setRepID(gmAddressSetupForm.getRepID());
    gmAddressSetupForm.setOldAddID(gmAddressSetupForm.getOldAddID());
    gmAddressSetupForm.setFromStrOpt(gmAddressSetupForm.getFromStrOpt());
    gmAddressSetupForm.sethAddrId(gmAddressSetupForm.gethAddrId());
    gmAddressSetupForm.loadSessionParameters(request);
    String strUserID = gmAddressSetupForm.getUserId();
    String strPartyID = gmAddressSetupForm.getSessPartyId();
    String strSecEvent = "ADDRESS_PRIMARY_FLAG";
    String strOpt = gmAddressSetupForm.getStrOpt();
    // PMT-4359 -- Getting Country Code and Rule Preferred carried based on the Country Code
    String strPrefCarrier = "";
    String strCountryCode = GmCommonClass.parseNull(GmCommonClass.countryCode);
    String strRulePrefCarrier =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCountryCode, "PREFCARRIER"));
    log.debug(" stropt is " + strOpt);
    log.debug(" strPartyId is " + strPartyId);
    log.debug(" active flag value is " + strActiveFlg);
    
    // Sales rep address edit flag access
    hmAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyID,"ADDRESS_ADD_EDIT"));
    strSalesRepEditAcsFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmAddressSetupForm.setEditAccessFl(strSalesRepEditAcsFl);
    
    if (strOpt.equals("save")) {
      hmParam = GmCommonClass.getHashMapFromForm(gmAddressSetupForm);
      log.debug(" values inside hmParam " + hmParam);
      String strAddressIdRt = gmAddressBean.saveAddressInfo(hmParam);
      log.debug("strAddressIdRt.............................." + strAddressIdRt);
      // Reseting the form
      // PMT-288 if any address modified/added address dropdown will show choose one.
      gmAddressSetupForm.setAddIdReturned("0");
      gmAddressSetupForm.setHaddressID("");
      gmAddressSetupForm.setAddressType("0");
      gmAddressSetupForm.setBillAdd1("");
      gmAddressSetupForm.setBillAdd2("");
      gmAddressSetupForm.setBillCity("");
      gmAddressSetupForm.setState("0");
      gmAddressSetupForm.setZipCode("");
      gmAddressSetupForm.setCountry("0");
      gmAddressSetupForm.setBillname("");
      // PMT-4359 -- Setting the Preferred Carrier value with Rule Carrier after Editing the Rep
      // Address
      gmAddressSetupForm.setCarrier(strRulePrefCarrier);
      gmAddressSetupForm.setPreference("");
      gmAddressSetupForm.setActiveFlag("off");
      gmAddressSetupForm.setPrimaryFlag("off");

    } else if (strOpt.equals("edit")) {
      hmValues = gmAddressBean.fetchEditAddressInfo(strAddressId);
      log.debug(" values fetched for edit " + hmValues);
      gmAddressSetupForm =
          (GmAddressSetupForm) GmCommonClass.getFormFromHashMap(gmAddressSetupForm, hmValues);
      gmAddressSetupForm.setHaddressID(strAddressId);
      // Setting the no. of transactions associated with an address
      gmAddressSetupForm.setHcount(GmCommonClass.parseZero((String) hmValues.get("ADDRCOUNT")));
      gmAddressSetupForm.setPrimaryFlag((String) hmValues.get("PRIMARYFL"));
      gmAddressSetupForm.setActiveFlag((String) hmValues.get("INACTIVEFL"));
      // PMT-4359 -- Setting the Preferred Carrier value with Saved Preferred carrier value
      gmAddressSetupForm.setCarrier(GmCommonClass.parseNull(gmAddressSetupForm.getCarrier()));
      log.debug("form value for primaryflg is " + gmAddressSetupForm.getPrimaryFlag());
    }

    if (!strPartyId.equals("")) {
      alLogReasons = gmCommonBean.getLog(strPartyId, "1240");
      gmAddressSetupForm.setAlLogReasons(alLogReasons);
    }
    // ADDRESS_PRIMARY_FLAG - security event added to access primary flag in address setup screen
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyID,
            strSecEvent));
    strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmAddressSetupForm.setPrimaryStsFl(strAccessFl);
    gmAddressSetupForm.setAlAddressTypes(GmCommonClass.getCodeList("ADDTP"));
    gmAddressSetupForm.setAlPreferences(GmCommonClass.getCodeList("ADPRF"));
    gmAddressSetupForm.setAlStates(GmCommonClass.getCodeList("STATE", getGmDataStoreVO()));
    gmAddressSetupForm.setAlCountries(GmCommonClass.getCodeList("CNTY", getGmDataStoreVO()));
    // PMT-4359 -- Forming the alCarrier by getting code lookup values from DCAR code group
    gmAddressSetupForm.setAlCarriers(GmCommonClass.getCodeList("DCAR", getGmDataStoreVO()));
    gmAddressSetupForm.setStrOpt(strOpt);
    strPrefCarrier = GmCommonClass.parseNull(gmAddressSetupForm.getCarrier());
    // PMT-4359 -- IF Preferred Carrier from Form is null we are setting this value with Rule
    // Preferred carrier value else Form value
    if (strPrefCarrier.equals("")) {
      gmAddressSetupForm.setCarrier(strRulePrefCarrier);
    } else {
      gmAddressSetupForm.setCarrier(strPrefCarrier);
    }
    return mapping.findForward("setup");
  }

  /**
   * This method is used to load the address details of the party by passing the party ID.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return ActionForward
   * @throws AppError
   * @throws Exception
   */

  public ActionForward loadAddressReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      GmAddressSetupForm gmAddressSetupForm = (GmAddressSetupForm) form;
      GmAddressBean gmAddressBean = new GmAddressBean();
      List lResult = null;

      String strPartyId = gmAddressSetupForm.getPartyId();
      // for areaset
      gmAddressSetupForm.setRefID(gmAddressSetupForm.getRefID());
      gmAddressSetupForm.setRepID(gmAddressSetupForm.getRepID());
      gmAddressSetupForm.setOldAddID(gmAddressSetupForm.getOldAddID());
      gmAddressSetupForm.setFromStrOpt(gmAddressSetupForm.getFromStrOpt());
    //  gmAddressSetupForm.setEditAccessFl(gmAddressSetupForm.getEditAccessFl());

      if (!strPartyId.equals("")) {
        log.debug(" strPartyId ins " + strPartyId);
        lResult = gmAddressBean.fetchAddressInfo(strPartyId).getRows();
        gmAddressSetupForm.setLresult(lResult);
      }

      return mapping.findForward("report");
    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    }
  }



  /**
   * This method is used to validate the Account Shipping Address details by calling the URL and
   * passing the Ship address parameters. URL:https://development.avalara.net/1.0/address/validate?.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return NULL
   * @throws AppError
   * @throws Exception
   */
  public ActionForward validateAccountShipAdd(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);

    GmValidateAddressVO gmValidateAddress = new GmValidateAddressVO();
    GmSalesTaxTransBean gmSalesTaxTransBean = new GmSalesTaxTransBean(getGmDataStoreVO());
    GmSalesTaxRptBean gmSalesTaxRptBean = new GmSalesTaxRptBean(getGmDataStoreVO());
    String strXMLString = "";
    String shipAdd = GmCommonClass.parseNull(request.getParameter("Address1"));
    String shipAdd2 = GmCommonClass.parseNull(request.getParameter("Address2"));
    String shipCity = GmCommonClass.parseNull(request.getParameter("City"));
    String shipState = GmCommonClass.parseZero(request.getParameter("State"));
    String shipCountry = GmCommonClass.parseZero(request.getParameter("Country"));
    String shipzipCode = GmCommonClass.parseNull(request.getParameter("ZipCode"));


    shipState = GmCommonClass.getCodeAltName(shipState);
    shipCountry = GmCommonClass.getCodeAltName(shipCountry);

    HashMap hmParam = new HashMap();
    hmParam.put("METHOD", "VALIDATE_ADD");

    if (!shipzipCode.equals("") && !shipzipCode.equals("-")) {
      shipzipCode = "&PostalCode=" + URLEncoder.encode(shipzipCode);
    } else {
      shipzipCode = "";
    }
    hmParam.put("PARAM",
        "Line1=" + URLEncoder.encode(shipAdd) + "&Line2=" + URLEncoder.encode(shipAdd2) + "&Line3="
            + URLEncoder.encode("") + "&City=" + URLEncoder.encode(shipCity) + "&Region="
            + URLEncoder.encode(shipState) + "&Country=" + URLEncoder.encode(shipCountry)
            + shipzipCode);

    gmValidateAddress = gmSalesTaxTransBean.validateAvaTaxService(hmParam);
    GmValidShipAddressVo gmValidShipAddressVo = gmValidateAddress.getAddress();
    String strResultCode = gmValidateAddress.getResultCode();

    if (strResultCode.equalsIgnoreCase("Success")) {
      shipAdd = gmValidShipAddressVo.getLine1();
      shipAdd2 = gmValidShipAddressVo.getLine2();
      shipCity = gmValidShipAddressVo.getCity();
      shipState =
          gmSalesTaxRptBean.fetchCodeIdFromCodeGrpAndCodeAlt("STATE",
              gmValidShipAddressVo.getRegion());
      shipCountry =
          gmSalesTaxRptBean.fetchCodeIdFromCodeGrpAndCodeAlt("CNTY",
              gmValidShipAddressVo.getCountry());
      shipzipCode = gmValidShipAddressVo.getPostalCode();

      strXMLString =
          "Y" + "^^" + shipAdd + "^^" + shipAdd2 + "^^" + shipCity + "^^" + shipState + "^^"
              + shipCountry + "^^" + shipzipCode;

    } else {
      GmMessagesVO[] mess = gmValidateAddress.getMessages();
      GmMessagesVO gmMessagesVO = mess[0];
      strXMLString = gmMessagesVO.getSummary();
    }

    response.setContentType("text/xml;charset=utf-8");
    response.setCharacterEncoding("UTF-8");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter pw = response.getWriter();
    if (!strXMLString.equals("")) {
      pw.write(strXMLString);
    }
    pw.flush();
    pw.close();

    return null;
  }
}
