//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\GmInternalCommentsSetupAction.java

package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.db.GmDBManager;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.forms.GmPartySetupForm;

/**
 * Action class for contact info setup for the party
 * 
 * @author aXiang
 */
public class GmInternalCommentsSetupAction extends GmAction {

	/**
	 * @return ActionForward
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		Logger log = GmLogger.getInstance(this.getClass().getName());
		try {

			GmPartySetupForm gmInternalCommentsSetupForm = (GmPartySetupForm) form;
			GmCommonBean gmCommonBean = new GmCommonBean();
			GmDBManager gmDBManager = new GmDBManager();
			HashMap hmParam = new HashMap();
			gmInternalCommentsSetupForm.loadSessionParameters(request);

			String strPartyId = gmInternalCommentsSetupForm.getPartyId();

			String strHAction = GmCommonClass.parseNull((String) gmInternalCommentsSetupForm.getHaction());
			strPartyId = strPartyId.equals("") ? GmCommonClass.parseNull((String) request.getParameter("partyId")) : strPartyId;
			strHAction = strPartyId.equals("") ? GmCommonClass.parseNull((String) request.getParameter("haction")) : strHAction;
			ArrayList alLogReasons = new ArrayList();

			if (strHAction.equalsIgnoreCase("save")) {
				hmParam = GmCommonClass.getHashMapFromForm(gmInternalCommentsSetupForm);
				String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
				String strType = "91950"; 
				// c901_code_id: 91950 name is "Internal Comments"

				String strComments = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

				if (!strComments.equals("")) {
					gmCommonBean.saveLog(gmDBManager, strPartyId, strComments, strUserID, strType);
					gmDBManager.commit();
				}
			}
			alLogReasons = gmCommonBean.getLog(strPartyId, "91950");

			gmInternalCommentsSetupForm.setAlLogReasons(alLogReasons);
		} catch (Exception exp) {
			exp.printStackTrace();
			throw new AppError(exp);
		}

		return mapping.findForward("success");
	}
}
