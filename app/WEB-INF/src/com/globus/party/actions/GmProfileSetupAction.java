/**
 * FileName    : GmProfileSetupAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Nov 11, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.party.forms.GmPartySetupForm;

/**
 * @author sthadeshwar
 *
 */
public class GmProfileSetupAction extends GmAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			GmPartySetupForm gmPartySetupForm = (GmPartySetupForm)form;
			GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean();
			//gmPartySetupForm.loadSessionParameters(request);
			
			String strOpt = gmPartySetupForm.getStrOpt();
			String strPartyId = gmPartySetupForm.getPartyId();
			String strPartyType = gmPartySetupForm.getPartyType();
			
			log.debug("strOpt = " + strOpt);
			log.debug("strPartyId = " + strPartyId);
			log.debug("strPartyType = " + strPartyType);
			
//			ArrayList profileTabs = gmPartyInfoBean.fetchSetupPageDisplayTabs(strPartyType, "92104");
//			for(int i=0; i<profileTabs.size(); i++) {
//				log.debug("profileTab = " + profileTabs.get(i));
//			}
			gmPartySetupForm.setAlTabs(gmPartyInfoBean.fetchSetupPageDisplayTabs(strPartyType, "92104"));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
		return mapping.findForward("success");
	}
}
