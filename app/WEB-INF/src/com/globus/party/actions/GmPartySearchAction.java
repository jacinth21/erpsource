/**
 * FileName    : GmPartySearchAction.java 
 * Description :
 * Author      : vprasath
 * Date        : Nov 10, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmEducationBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.party.forms.GmPartySearchForm;

/**
 * @author vprasath
 *
 */
public class GmPartySearchAction extends GmAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {
		GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean();
		GmPartySearchForm gmPartySearchForm = (GmPartySearchForm)form;
		ArrayList alReturn = new ArrayList();
		
		try {			
			String strSearchString = "";
			String searchStrWithId = gmPartySearchForm.getSearchStringWithId();
			String strPartyType = gmPartySearchForm.getPartyType();
			log.debug("strPartyType in GmPartySearchAction = " + strPartyType);
			
			if(searchStrWithId.equals("")) {
				String strSearchVal = gmPartySearchForm.getSearchString().replaceAll("~", " ");			
				strSearchString = gmPartySearchForm.getLookupId()+";=^"+strSearchVal + "|";	// Added '=' and '^' since operator is introduced in the search party procedure
			} else {
				strSearchString = searchStrWithId;
			}
			log.debug("strSearchString = " + strSearchString);
			alReturn = gmPartyInfoBean.fetchParty(strSearchString, strPartyType);		
			gmPartySearchForm.setAlResult(alReturn);
			//request.setAttribute("RESULT", alReturn);
		} catch(Exception ex) {
			log.error(ex.getMessage(), ex);
		}	
		return mapping.findForward("success");
	}	
}