// Source file:
// C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\GmPartySetupAction.java

/**
 * FileName : GmPartySetupAction.java Description : Author : sthadeshwar Date : Oct 3, 2008
 * Copyright : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.party.beans.GmDealerInvoiceParamsBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.party.beans.GmPartyTransBean;
import com.globus.party.forms.GmPartySetupForm;

/**
 * Action class for basic info setup for the party
 * 
 * @author sthadeshwar
 */
public class GmPartySetupAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @return ActionForward
   * @roseuid 48F65C7B01BA
   */
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    String strOpt = "";
    try {
      instantiate(request, response);
      GmPartySetupForm gmPartySetupForm = (GmPartySetupForm) form;
      gmPartySetupForm.loadSessionParameters(request);
      GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
      GmPartyTransBean gmPartyTransBean = new GmPartyTransBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmDealerInvoiceParamsBean gmDealerInvoiceParamsBean =
          new GmDealerInvoiceParamsBean(getGmDataStoreVO());
      GmAutoCompleteTransBean gmAutoCompleteTransBean =
          new GmAutoCompleteTransBean(getGmDataStoreVO());

      HashMap hmValues = new HashMap();
      String title = "";
      String strMasterGrpValue = "Y"; // For getting the list without Active flag Condition from
                                      // table

      strOpt = gmPartySetupForm.getStrOpt();
      String strPartyId = gmPartySetupForm.getPartyId();
      String strPartyType = gmPartySetupForm.getPartyType();
      // String strReloadCnt = "";
      HashMap hmParam = GmCommonClass.getHashMapFromForm(gmPartySetupForm);
      String strAcion = GmCommonClass.parseNull((String) hmParam.get("HACTION"));
      log.debug("hmParam in GmPartySetupAction = " + hmParam);
      if (strOpt.equals("save")) {
        if (strPartyType.equals("7007")) {
          strPartyId = gmPartyTransBean.saveUserScInfo(hmParam);
        } else {
          String strInActiveFlag =
              GmCommonClass.getCheckBoxValue((String) hmParam.get("INACTIVEFLAG"));
          if (strInActiveFlag.equals("on")) {
            strInActiveFlag = "Y";
          }
          strPartyId = gmPartyTransBean.savePartyInfo(hmParam);

          GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
          // View refresh JMS called
          if (strPartyType.equals("26230725")) {// Dealer party type = 26230725
            HashMap hmViewRefresh = new HashMap();
            hmViewRefresh.put("M_VIEW", "v700_territory_mapping_detail");
            hmViewRefresh.put("companyInfo",
                GmCommonClass.parseNull(request.getParameter("companyInfo")));

            String strViewConsumerClass =
                GmCommonClass.parseNull(GmCommonClass.getString("VIEW_REFRESH_CONSUMER_CLASS"));
            hmViewRefresh.put("CONSUMERCLASS", strViewConsumerClass);
            gmConsumerUtil.sendMessage(hmViewRefresh);
          }
          // jms call - party type
          HashMap hmPartyAcc = new HashMap();
          hmPartyAcc.put("ID", strPartyId);
          hmPartyAcc.put("OLD_NM", "");
          hmPartyAcc.put("NEW_NM", "Glo");
          hmPartyAcc.put("METHOD", "PartyList");
          hmPartyAcc.put("PARTY_TYPE", strPartyType);
          hmPartyAcc.put("companyInfo",
              GmCommonClass.parseNull(request.getParameter("companyInfo")));
          gmAutoCompleteTransBean.saveDetailsToCache(hmPartyAcc);
        }
        gmPartySetupForm.setPartyId(strPartyId);
        gmPartySetupForm.setPartyType(strPartyType);
        /*
         * String strRedirectURL =
         * "/GmPageControllerServlet?strPgToLoad=/gmPartySetup.do?haction=FETCHCONTAINER&partyId" +
         * strPartyId + "&partyType=" + strPartyType; ActionRedirect actionRedirect = new
         * ActionRedirect(strRedirectURL); return actionRedirect;
         */
      }
      if (!strPartyId.equals("")) {
        hmValues = gmPartyInfoBean.fetchPartyInfo(strPartyId); // TODO implement this
        // these lines of codes are used to fetch the dealer accounts based on the dealer party type
        if (strPartyType.equals("26230725")) {// Dealer party type = 26230725
          ArrayList alDealerAcct = gmDealerInvoiceParamsBean.fetchDealerAccounts(strPartyId);
          String strDealerAcctFlag = alDealerAcct.size() != 0 ? "Y" : "N";
          gmPartySetupForm.setStrDealerAcctFlag(strDealerAcctFlag);

        }
        log.debug("\nFETCHING VALUES:\nValues = " + hmValues);
        gmPartySetupForm =
            (GmPartySetupForm) GmCommonClass.getFormFromHashMap(gmPartySetupForm, hmValues);

        gmPartySetupForm.setAlLogReasons(gmCommonBean.getLog(strPartyId, "1236"));
        gmPartySetupForm
            .setAlTabs(gmPartyInfoBean.fetchSetupPageDisplayTabs(strPartyType, "92103"));
        // strOpt="basicinfo";
      }

      if (strPartyType.equals("7002")) {
        gmPartySetupForm.setDisplayPartyName("true");
      }

      // For container page
      log.debug("master group value" + strMasterGrpValue);

      gmPartySetupForm.setAlParty(gmPartyInfoBean.fetchPartyNameList(strPartyType,
          strMasterGrpValue));

      title = GmCommonClass.getCodeNameFromCodeId(strPartyType);
      log.debug("title in GmPartySetupAction = " + title);
      gmPartySetupForm.setTitle(title);

      String strAccessFlag =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPartyType, "DISPLAY_NAME"));

      gmPartySetupForm.setAccessFlag(strAccessFlag);

      if (strOpt.equals("") && strAcion.equals("FETCHCONTAINER")) {
        strOpt = "groupaccount";
      } else if (((strOpt.equals("") || strOpt.equals("fetchcontainer")) && strAcion
          .equals("DIVCONTAINER")) || (strOpt.equals("save") && strAcion.equals("DIVCONTAINER"))) {
        strOpt = "basicinfo";
      } else if (strOpt.equals("")) {
        strOpt = "default";
      } else if (strOpt.equals("fetchcontainer") && strAcion.equals("FETCHCONTAINER")) {
        strOpt = "groupaccount";
      }

      log.debug("Forwarding to = >" + strOpt + "<");
      if (strPartyId.equals("")) {
        gmPartySetupForm.reset(mapping, request);
      }
    } catch (Exception e) {
      throw new AppError(e);
    }
    log.debug(" strOpt ***** " + strOpt);
    return mapping.findForward(strOpt);
  }
}
