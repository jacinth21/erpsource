/**
 * FileName    : GmEducationSetupAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Oct 10, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.party.beans.GmEducationBean;
import com.globus.party.forms.GmEducationSetupForm;

/**
 * @author sthadeshwar
 *
 */
public class GmEducationSetupAction extends GmDispatchAction {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward loadEducationSetup(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			
			GmEducationSetupForm gmEducationSetupForm = (GmEducationSetupForm) form;
			gmEducationSetupForm.loadSessionParameters(request);

			GmEducationBean gmPartyEducationSetupBean = new GmEducationBean();
			
			HashMap hmValues = new HashMap();
			HashMap hmParam = new HashMap();			
			String strEducationId = gmEducationSetupForm.getHeducationId();
			
			String strOpt = gmEducationSetupForm.getStrOpt();
			String strPartyId = gmEducationSetupForm.getPartyId();
			
			log.debug("strOpt = " + strOpt);
			log.debug("strPartyId = " + strPartyId);
			log.debug("strEducationId = " + strEducationId);
			
			if(strOpt.equals("load")) {
				String eduLevel = gmEducationSetupForm.getEducationLevel();
				if(eduLevel.equals("90996")) {	// Fellowship
					gmEducationSetupForm.setCourseLabel("Mentor");
				}
			}
			
			if(strOpt.equals("save")) { // save
				hmParam = GmCommonClass.getHashMapFromForm(gmEducationSetupForm);
                strEducationId = gmPartyEducationSetupBean.savePartyEducationInfo(hmParam);
                
                // Reseting the form
                gmEducationSetupForm.setHeducationId("");
                gmEducationSetupForm.setEducationLevel("0");
                gmEducationSetupForm.setInstituteName("");
                gmEducationSetupForm.setDesignation("0");
                gmEducationSetupForm.setClassYear("");
                gmEducationSetupForm.setCourse("");
			} 
			
			if (strOpt.equals("edit")) {
	            hmValues = gmPartyEducationSetupBean.fetchEditEducationInfo(strEducationId);
	            log.debug("VALUES FOR EDIT = \n" +hmValues);
	            gmEducationSetupForm = (GmEducationSetupForm)GmCommonClass.getFormFromHashMap(gmEducationSetupForm,hmValues);
	        }
		
			gmEducationSetupForm.setAlLevel(GmCommonClass.getCodeList("EDULVL"));		// level drop-down
			gmEducationSetupForm.setAlDesignation(GmCommonClass.getCodeList("DESGN")); // designation drop-down						 	
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
		return mapping.findForward("setup");
	}
/* The following method will be used in displaying the display tag results for party */
	
	public ActionForward loadEducationReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception 
	{
		
		GmEducationSetupForm gmEducationSetupForm = (GmEducationSetupForm) form;
		String strPartyId = gmEducationSetupForm.getPartyId();
		GmEducationBean gmEducationSetupBean = new GmEducationBean();
		List lResult = null;
		log.debug("Party ID: " + strPartyId);
		
		if(!strPartyId.equals("")) { // fetch
			lResult = gmEducationSetupBean.fetchEducationInfo(strPartyId).getRows();
			gmEducationSetupForm.setLresult(lResult);	
			
			/* below code to search for the particular group for party */			
			request.setAttribute("SEARCHCODEMAP", GmCommonClass.getSearchList("PAEDU")); // PARTY EDUCATION
		}
		
		return mapping.findForward("report");
	}		
}
