//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\GmProffessionalDetailsSetupAction.java

package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmProffessionalDetailsBean;
import com.globus.party.forms.GmProfDetailsSetupForm;

/**
 * Action class for about me setup for the party
 * 
 * @author sthadeshwar
 */
public class GmProffessionalDetailsSetupAction extends GmAction {

	/**
	 * @roseuid 49139141002E
	 */
	public GmProffessionalDetailsSetupAction() {

	}

	/**
	 * @return ActionForward
	 * @roseuid 48FCD3EB017D
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			ArrayList alFields = new ArrayList();
			Logger log = GmLogger.getInstance(this.getClass().getName());
			GmProfDetailsSetupForm gmProfDetailsSetupForm = (GmProfDetailsSetupForm) form;
			gmProfDetailsSetupForm.loadSessionParameters(request);
			String strOpt = GmCommonClass.parseNull(request.getParameter("hAction"));
			String strParty_Id = GmCommonClass.parseNull(request.getParameter("partyId"));
			GmProffessionalDetailsBean gmProffessionalDetailsBean = new GmProffessionalDetailsBean();
			String strOtherInfo[] = new String[2];
			strOtherInfo[0] = gmProfDetailsSetupForm.getSpeciality();
			log.debug("Speciality: " + strOtherInfo[0]);
			strOtherInfo[1] = gmProfDetailsSetupForm.getProfessionalInterest();
			log.debug("ProfessionalInteresty: " + strOtherInfo[1]);
			gmProfDetailsSetupForm.setPartyId(strParty_Id);

			log.debug("strParty_Id = " + strParty_Id);

			if (strOpt.equals("save")) {
				//alFields = GmCommonClass.getCodeList("TVALS");
				alFields = gmProffessionalDetailsBean.fetchProfDetails(strParty_Id);
				String keyList[] = new String[alFields.size()];
				for (int i = 0; i < alFields.size(); i++) {
					HashMap hmResult = (HashMap) alFields.get(i);
					keyList[i] = (String) hmResult.get("CODENM");
				}
				gmProffessionalDetailsBean.saveProfDetails(Integer.parseInt(strParty_Id), keyList, strOtherInfo, gmProfDetailsSetupForm.getUserId());

			}

			alFields = gmProffessionalDetailsBean.fetchProfDetails(strParty_Id);

			for (int i = 0; i < alFields.size(); i++) {
				HashMap hmLoop = (HashMap) alFields.get(i);
				String strName = (String) hmLoop.get("CODENMALT");
				if (strName.equals("speciality")) {
					gmProfDetailsSetupForm.setSpeciality((String) hmLoop.get("VALUE"));
				} else if (strName.equals("professionalInterest")) {
					gmProfDetailsSetupForm.setProfessionalInterest((String) hmLoop.get("VALUE"));
				}
			}
			request.setAttribute("fieldValue", alFields);
			gmProfDetailsSetupForm.setFieldValue(alFields);
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
		return mapping.findForward("success");
	}
}
