/**
 * 
 * FileName : GmAboutMeSummaryAction.java Description : Author : sthadeshwar Date : Nov 26, 2008
 * Copyright : Globus Medical Inc
 */
package com.globus.party.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteTransBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.party.beans.GmNPIProcessBean;
import com.globus.party.forms.GmNPIProcessForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Aprasath
 * 
 */
public class GmNPIProcessAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * saveNPITrasactionDetails - To Save the NPI Details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward saveNPITrasactionDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);

    GmNPIProcessForm gmNPIProcessForm = (GmNPIProcessForm) form;
    GmNPIProcessBean gmNPIProcessBean = new GmNPIProcessBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmNPIProcessForm);
    String strUserId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
    String strSaveAction = GmCommonClass.parseNull((String) request.getParameter("hSaveAction"));
    String strhTxnId = GmCommonClass.parseNull((String) request.getParameter("htxnId"));
    hmParam.put("USERID", strUserId);
    hmParam.put("SAVEACTION", strSaveAction);
    hmParam.put("HTXNID", strhTxnId);
    log.debug( "  HM Param values ============ "+hmParam);
    String strnpitranid = gmNPIProcessBean.saveNPITransactionDetails(hmParam);
    String strNpiNum = GmCommonClass.parseNull((String) hmParam.get("NPINUM"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strCompInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
    createNewNPI(strNpiNum, strOrderId, strCompInfo, strUserId); // to initiate the job

    // sending t6640 primary key for edit
    if (!strnpitranid.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strnpitranid);
      pw.flush();
      return null;
    }

    return null;
  }

  /**
   * createNewNPI - To create new NPI
   * 
   * @param String
   * @return
   * @throws AppError
   * @throws Exception
   */
  //Creating new NPI number - PMT-39853
  private void createNewNPI (String strNpiNum,String strOrderId, String strCompInfo, String strUserId) throws AppError{
    HashMap hmParam = new HashMap();
    HashMap hmMailParams = new HashMap();
    ArrayList alMailParams = new ArrayList();
    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
    
    try{
    String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_NPI_QUEUE"));
    hmParam.put("QUEUE_NAME", strQueueName);
    log.debug( "  strQueueName================ :: "+strQueueName);
    // JMS_NPI_QUEUE is mentioned in JMSConsumerConfig. properties file.This key contain the
    // path for : com.globus.jms.consumers.processors.GmNPIConsumerJob
    String strConsumerClass =
        GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("NPI_CONSUMER_CLASS"));
    hmParam.put("TRANSID",strOrderId);
    hmParam.put("NPINO",strNpiNum);
    hmParam.put("CONSUMERCLASS", strConsumerClass);
    hmParam.put("COMPANYINFO", strCompInfo);
    hmParam.put("USERID", strUserId);
    
    gmConsumerUtil.sendMessage(hmParam);
    }
    catch(Exception e){
          log.error("Mail not sent exception occured in GmNPIProcessAction: "  + e.getMessage());
    }
  }

  /**
   * fetchNPITrasactionDetails - To Fetch the NPI Details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward fetchNPITrasactionDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);

    GmNPIProcessForm gmNPIProcessForm = (GmNPIProcessForm) form;
    GmNPIProcessBean gmNPIProcessBean = new GmNPIProcessBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmNPIProcessForm);

    ArrayList alNPIDetails = gmNPIProcessBean.fetchNPITransactionDetails(hmParam);
    String strNPIDetails = gmWSUtil.parseArrayListToJson(alNPIDetails);

    response.getWriter().write(strNPIDetails);

    return null;
  }

  /**
   * cancelNPITrasactionDetails - To void the NPI details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward cancelNPITrasactionDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmNPIProcessForm gmNPIProcessForm = (GmNPIProcessForm) form;
    GmNPIProcessBean gmNPIProcessBean = new GmNPIProcessBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();

    hmParam = GmCommonClass.getHashMapFromForm(gmNPIProcessForm);
    String strUserId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
    hmParam.put("USERID", strUserId);
    gmNPIProcessBean.cancelNPITransactionDetails(hmParam);

    return null;
  }

  /**
   * fetchNPITempDetails - To fetch NPI details where the transaction flag is not updated to 'Y'
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward fetchNPITempDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);

    GmNPIProcessForm gmNPIProcessForm = (GmNPIProcessForm) form;
    GmNPIProcessBean gmNPIProcessBean = new GmNPIProcessBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmNPIProcessForm);

    ArrayList alNPIDetails = gmNPIProcessBean.fetchNPITempDetails(hmParam);
    String strNPIDetails = gmWSUtil.parseArrayListToJson(alNPIDetails);
    // sending t6640 primary key for edit
    response.getWriter().write(strNPIDetails);

    return null;

  }

  /**
   * fetchNPIHistoryDtls - To fetch NPI History Details for the sales rep and account
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward fetchNPIHistoryDtls(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);

    GmNPIProcessForm gmNPIProcessForm = (GmNPIProcessForm) form;
    GmNPIProcessBean gmNPIProcessBean = new GmNPIProcessBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmNPIProcessForm);

    log.debug("SAVING VALUES = " + hmParam);
    ArrayList alNPIDetails = gmNPIProcessBean.fetchNPIHistoryDtls(hmParam);
    String strNPIDetails = gmWSUtil.parseArrayListToJson(alNPIDetails);
    // String strReturn = Arrays.toString(alNPIDetails.toArray());
    log.debug(" strNPIDetails is ....." + strNPIDetails);
    // sending t6640 primary key for edit
    response.getWriter().write(strNPIDetails);

    return null;

  }

  /**
   * reportNPIError - To fetch Invalid NPI Details for the sales rep and account
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward reportNPIError(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);

    GmNPIProcessForm gmNPIProcessForm = (GmNPIProcessForm) form;
    GmNPIProcessBean gmNPIProcessBean = new GmNPIProcessBean(getGmDataStoreVO());
    String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strCompId = getGmDataStoreVO().getCmpid();
    String strRuleValue = "";
    strRuleValue = GmCommonClass.getRuleValue(strCompId, "SHOW_NPI");
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    ArrayList alNPIErrDtls = new ArrayList();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strVmPath = "properties.labels.party.GmNPIErrorReport";
    String strXmlGridData = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmNPIProcessForm);

    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    if (strOpt.equals("Reload")) {
      alNPIErrDtls = gmNPIProcessBean.loadNPIErrorDetails(hmParam);
      hmResult.put("ALRESULT", alNPIErrDtls);
      hmResult.put("TEMPLETENAME", "GmNPIErrorReport.vm");
      hmResult.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmResult.put("STRVMPATH", strVmPath);
      hmResult.put("SESSDATEFMT", strApplDateFmt);
      hmResult.put("STRRULEVALUE", strRuleValue);
      strXmlGridData = generateOutPut(hmResult);
      gmNPIProcessForm.setGridXmlData(strXmlGridData);
      gmNPIProcessForm.setIntGridSize(alNPIErrDtls.size());
    }
    request.setAttribute("hmParam", hmParam);
    return mapping.findForward("GmNPIErrorReport");
  }

  /**
   * saveValidNPI - To save the NPI and make the record valid by the user
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public String saveValidNPI(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);

    GmNPIProcessForm gmNPIProcessForm = (GmNPIProcessForm) form;
    GmNPIProcessBean gmNPIProcessBean = new GmNPIProcessBean(getGmDataStoreVO());
    GmAutoCompleteTransBean gmAutoCompleteTransBean =
        new GmAutoCompleteTransBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmParamNPIDetails = new HashMap();
    HashMap hmDetails = new HashMap();
    String strId = "";
    String strNewName = "";
    String strUserId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
    hmParam = GmCommonClass.getHashMapFromForm(gmNPIProcessForm);
    hmParam.put("USERID", strUserId);
    log.debug("SAVING VALUES = " + hmParam);
    gmNPIProcessBean.saveValidNPI(hmParam);
    // fetch the valid NPI
    hmDetails = gmNPIProcessBean.fetchValidNPIDtsl(hmParam);
    strId = GmCommonClass.parseNull((String) hmDetails.get("SURGEON_NPI_ID"));
    strNewName = GmCommonClass.parseNull((String) hmDetails.get("SURGEON_NAME"));
    hmParamNPIDetails.put("ID", strId);
    hmParamNPIDetails.put("OLD_NM", "");
    hmParamNPIDetails.put("NEW_NM", strNewName);
    hmParamNPIDetails.put("METHOD", "NPIValid");
    hmParamNPIDetails.put("companyInfo",
        GmCommonClass.parseNull(request.getParameter("companyInfo")));
    // the valid NPI save to cache
    gmAutoCompleteTransBean.saveDetailsToCache(hmParamNPIDetails);
    return null;
  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPath = GmCommonClass.parseNull((String) hmParam.get("STRVMPATH"));
    String strTempName = GmCommonClass.parseNull((String) hmParam.get("TEMPLETENAME"));
    String strName = GmCommonClass.parseNull((String) hmParam.get("SESSDATEFMT"));
    String Name = (String) hmParam.get("STRRULEVALUE");
    ArrayList alList = new ArrayList();
    alList = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("ALRESULT"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setDataList("alList", alList);
    templateUtil.setTemplateSubDir("party/templates");
    templateUtil.setTemplateName(strTempName);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPath,
        strSessCompanyLocale));
    return templateUtil.generateOutput();
  }
  /**
   * saveNPIWIDTrasactionDetails - To Save the NPI and Surgeon Details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward saveNPIWIDTrasactionDetails(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);

    GmNPIProcessForm gmNPIProcessForm = (GmNPIProcessForm) form;
    GmNPIProcessBean gmNPIProcessBean = new GmNPIProcessBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmNPIProcessForm);
    String strUserId =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessUserId"));
    String strSaveAction = GmCommonClass.parseNull((String) request.getParameter("hSaveAction"));
    String strhTxnId = GmCommonClass.parseNull((String) request.getParameter("htxnId"));
    hmParam.put("USERID", strUserId);
    hmParam.put("SAVEACTION", strSaveAction);
    hmParam.put("HTXNID", strhTxnId);
    
    String strnpitranid = gmNPIProcessBean.saveNPIWIDTransactionDetails(hmParam);
    String strNpiNum = GmCommonClass.parseNull((String) hmParam.get("NPINUM"));
    String strOrderId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strCompInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
    createNewNPI(strNpiNum, strOrderId, strCompInfo, strUserId); // to initiate the job

    // sending t6640 primary key for edit
    if (!strnpitranid.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strnpitranid);
      pw.flush();
      return null;
    }

    return null;
  }
}
