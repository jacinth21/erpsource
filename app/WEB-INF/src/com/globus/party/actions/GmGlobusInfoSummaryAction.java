/**
 * FileName    : GmGlobusInfoSummaryAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Dec 2, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmGlobusAssociationBean;
import com.globus.party.forms.GmAboutMeSummaryForm;
import com.globus.party.forms.GmGlobusInfoSummaryForm;

/**
 * @author sthadeshwar
 *
 */
public class GmGlobusInfoSummaryAction extends GmAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		GmGlobusInfoSummaryForm gmGlobusInfoSummaryForm = (GmGlobusInfoSummaryForm)form;
		gmGlobusInfoSummaryForm.loadSessionParameters(request);
		GmGlobusAssociationBean gmGlobusAssociationBean = new GmGlobusAssociationBean();
		HashMap hmValues = new HashMap();
		
		String strPartyId = gmGlobusInfoSummaryForm.getPartyId();
		log.debug("strPartyId = " + strPartyId);
		
		hmValues = gmGlobusAssociationBean.fetchGlobusAssociationSummaryInfo(strPartyId);
		log.debug("hmValues = " + hmValues);
		gmGlobusInfoSummaryForm = (GmGlobusInfoSummaryForm)GmCommonClass.getFormFromHashMap(gmGlobusInfoSummaryForm,hmValues);
		
		return mapping.findForward("success");
	}
}
