// Source file:
// C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\GmContactSetupAction.java

package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmContactBean;
import com.globus.party.forms.GmContactSetupForm;

/**
 * Action class for contact info setup for the party
 * 
 * @author sthadeshwar
 */
public class GmContactSetupAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward loadContactSetup(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      GmContactSetupForm gmContactSetupForm = (GmContactSetupForm) form;
      GmContactBean gmContactBean = new GmContactBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
      
      gmContactSetupForm.loadSessionParameters(request);

      HashMap hmParam = new HashMap();
      HashMap hmValues = new HashMap();
      ArrayList alLogReasons = new ArrayList();
      HashMap hmAccess = new HashMap();
      
      String strContactId = "";
      String strSalesRepEditAcsFl= "";
      String strAccessPartyId="";
      String strPartyId = gmContactSetupForm.getPartyId();
      String strOpt = gmContactSetupForm.getStrOpt();
      String strIncludedPage = gmContactSetupForm.getIncludedPage();
      hmParam = GmCommonClass.getHashMapFromForm(gmContactSetupForm);
      String strUserId =gmContactSetupForm.getUserId();

      log.debug(" stropt is " + strOpt);
      log.debug(" strIncludedPage is " + strIncludedPage);
      
      // Sales rep contact edit flag access
      strAccessPartyId=gmContactSetupForm.getSessPartyId();
      hmAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strAccessPartyId,"CONTACT_ADD_EDIT"));
      strSalesRepEditAcsFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      gmContactSetupForm.setEditAccessFl(strSalesRepEditAcsFl);
      
      if (strOpt.equals("save")) {
        log.debug(" values to save " + hmParam);
        strContactId = gmContactBean.saveContactInfo(hmParam);
        // Reseting the form
        gmContactSetupForm.reset();
      } else if (strOpt.equals("edit")) {
        strContactId = gmContactSetupForm.getHcontactId();
        hmValues = gmContactBean.fetchEditContactInfo(strContactId);
        log.debug(" values fetched for edit " + hmValues);
        gmContactSetupForm =
            (GmContactSetupForm) GmCommonClass.getFormFromHashMap(gmContactSetupForm, hmValues);
      }

      gmContactSetupForm.setAlContactMode(GmCommonClass.getCodeList("COTMD", getGmDataStoreVO()));

      // Displaying comments
      log.debug("strContactId is " + strContactId);
      alLogReasons = gmCommonBean.getLog(strPartyId, "1231");
      gmContactSetupForm.setAlLogReasons(alLogReasons);

    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    }
    return mapping.findForward("setup");
  }

  public ActionForward loadContactReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
	  
    try {
      instantiate(request, response);
      GmContactSetupForm gmContactSetupForm = (GmContactSetupForm) form;
      GmContactBean gmContactBean = new GmContactBean(getGmDataStoreVO());
      //edit Sales Rep details 
      gmContactSetupForm.setEditAccessFl(gmContactSetupForm.getEditAccessFl());
     
      String strPartyId = gmContactSetupForm.getPartyId();
      if (!strPartyId.equals("")) {
        log.debug("strPartyId = " + strPartyId);
        List lResult = gmContactBean.fetchContactInfo(strPartyId).getRows();
        log.debug("Size of lResult " + lResult.size());
        gmContactSetupForm.setLresult(lResult);

       // As there is not search hyperlinks for Contact page
       // request.setAttribute("SEARCHCODEMAP", GmCommonClass.getSearchList("NA"));
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    }
    return mapping.findForward("report");
  }
}
