//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\GmLicenseSetupAction.java

package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmLicenseBean;
import com.globus.party.forms.GmLicenseSetupForm;

/**
 * Action class for licenses info setup for the party
 * 
 * @author sthadeshwar
 */
public class GmLicenseSetupAction extends GmPartyAction {

	/**
	 * @return ActionForward
	 * @roseuid 48F7A3C90073
	 */

	Logger log = GmLogger.getInstance(this.getClass().getName());

	public ActionForward loadLicenseSetup(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			GmLicenseSetupForm gmLicenseSetupForm = (GmLicenseSetupForm) form;
			gmLicenseSetupForm.loadSessionParameters(request);
			GmLicenseBean gmLicenseBean = new GmLicenseBean();
			
			String strOpt = gmLicenseSetupForm.getStrOpt();

			HashMap hmParam = new HashMap();
			HashMap hmValues = new HashMap();

			if (strOpt.equals("edit")) {
				String strLicenseId = gmLicenseSetupForm.getLicenseId();
				hmValues = gmLicenseBean.fetchEditLicenseInfo(strLicenseId);
				log.debug("Values fetched for Edit = " + hmValues);
				gmLicenseSetupForm = (GmLicenseSetupForm) GmCommonClass.getFormFromHashMap(gmLicenseSetupForm, hmValues);
			}
			if (strOpt.equals("save")) {
				hmParam = GmCommonClass.getHashMapFromForm(gmLicenseSetupForm);
				log.debug("Saving values = " + hmParam);
				gmLicenseBean.saveLicenseInfor(hmParam);
				gmLicenseSetupForm.reset();
			}

			gmLicenseSetupForm.setAlCountries(GmCommonClass.getCodeList("CNTY"));
			gmLicenseSetupForm.setAlStates(GmCommonClass.getCodeList("STATE", getGmDataStoreVO()));
			gmLicenseSetupForm.setAlMonth(GmCommonClass.getCodeList("MONTH"));

		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
		return mapping.findForward("success");
	}

	public ActionForward loadLicenseReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		GmLicenseSetupForm gmLicenseForm = (GmLicenseSetupForm) form;

		GmLicenseBean gmLicenseBean = new GmLicenseBean();
		List lResult = null;
		String strPartyId = GmCommonClass.parseNull((String) gmLicenseForm.getPartyId());
		
		if (!strPartyId.equals("")) { // fetch
			lResult = gmLicenseBean.fetchLicenseInfo(strPartyId).getRows();
			
			String type = GmCommonClass.parseNull((String)request.getParameter("TYPE"));
			log.debug("type = " + type);
			int size = lResult.size();
			lResult = getRestrictedList(type, lResult, "LICENSES");
			if(size > lResult.size()) {
				request.setAttribute("MORE_LICENSES", "true");
			}
			gmLicenseForm.setLresult(lResult);

			/* below code to search for the particular group for party */
			request.setAttribute("SEARCHCODEMAP", GmCommonClass.getSearchList("PALIC"));

		}

		return mapping.findForward("report");
	}
}
