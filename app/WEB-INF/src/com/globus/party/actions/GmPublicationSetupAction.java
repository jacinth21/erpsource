package com.globus.party.actions;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmPublicationBean;
import com.globus.party.forms.GmPublicationSetupForm;

public class GmPublicationSetupAction extends GmPartyAction {

	public ActionForward loadPublicationSetup(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			GmPublicationSetupForm gmPublicationSetupForm = (GmPublicationSetupForm) form;
			gmPublicationSetupForm.loadSessionParameters(request);
			GmPublicationBean gmPublicationBean = new GmPublicationBean();
			 
			Logger log = GmLogger.getInstance(this.getClass().getName());// 

			String strOpt = gmPublicationSetupForm.getStrOpt();
			String strPartyId = gmPublicationSetupForm.getPartyId();
			
			String strhPublicationID = gmPublicationSetupForm.getHpublicationID();

			HashMap hmParam = new HashMap();
			HashMap hmValues = new HashMap();

			log.debug(GmCommonClass.getHashMapFromForm(gmPublicationSetupForm));
			log.debug(" Testing. ..... ");
			log.debug(" strPartyId is ....." + strPartyId);
			log.debug(" strOpt is ....." + strOpt);
			
			if (strOpt.equals("save")) {
				hmParam = GmCommonClass.getHashMapFromForm(gmPublicationSetupForm);
				log.debug("SAVING VALUES = " + hmParam);
				strhPublicationID = gmPublicationBean.savePublicationInfo(hmParam);
				
				// Resetting the form
				gmPublicationSetupForm.reset();
			} 
			if (strOpt.equals("edit")) {
				hmValues = gmPublicationBean.fetchEditPublicationInfo(strhPublicationID);
				log.debug("VALUES FETCHED FOR EDIT = " + hmParam);
				gmPublicationSetupForm =(GmPublicationSetupForm)GmCommonClass.getFormFromHashMap(gmPublicationSetupForm,hmValues);
			}
			
			gmPublicationSetupForm.setAlTypeList(GmCommonClass.getCodeList("PUBTP"));
			gmPublicationSetupForm.setAlMonthList(GmCommonClass.getCodeList("MONTH"));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}

		return mapping.findForward("setup");
	}
	
	public ActionForward loadPublicationReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			
			GmPublicationSetupForm gmPublicationSetupForm = (GmPublicationSetupForm) form;
			GmPublicationBean gmPublicationBean = new GmPublicationBean();
			
			String strPartyId = gmPublicationSetupForm.getPartyId();
			
			String type = GmCommonClass.parseNull((String)request.getParameter("TYPE"));
			
			List lResult = gmPublicationBean.fetchPublicationInfo(strPartyId).getRows();
			int size = lResult.size();
			lResult = getRestrictedList(type, lResult, "PUBLICATIONS");
			if(size > lResult.size()) {
				request.setAttribute("MORE_PUBLICATIONS", "true");
			}
			
			gmPublicationSetupForm.setLresult(lResult);
			
			/* below code to search for the particular group for party */			
			request.setAttribute("SEARCHCODEMAP", GmCommonClass.getSearchList("PAPUB")); // PARTY EDUCATION
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
		return mapping.findForward("report");
	}

}
