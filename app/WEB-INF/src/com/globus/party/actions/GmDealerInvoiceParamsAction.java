package com.globus.party.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.party.beans.GmDealerInvoiceParamsBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.party.forms.GmDealerInvoiceParamsForm;
import com.globus.prodmgmnt.beans.GmPartNumBean;
import com.globus.webservice.crm.bean.GmCRMSurgeonBean;


public class GmDealerInvoiceParamsAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());


  /**
   * This method is used to save dealer invoice parameter values
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return ActionForward
   * @throws AppError
   * @throws Exception
   */

  public ActionForward saveDealerSetup(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmDealerInvoiceParamsForm gmDealerInvoiceParamsForm = (GmDealerInvoiceParamsForm) form;
    instantiate(request, response);
    gmDealerInvoiceParamsForm.loadSessionParameters(request);
    GmDealerInvoiceParamsBean gmDealerSetupBean = new GmDealerInvoiceParamsBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    ArrayList alElecrtonicFileFmt = new ArrayList();
    ArrayList alCsvTempName = new ArrayList();
    ArrayList alInvoiceLayout = new ArrayList();
    ArrayList alPaymentTerms = new ArrayList();
    ArrayList alInvoiceClosingDate = new ArrayList();
    ArrayList alCollector = new ArrayList();
    HashMap hmParam = new HashMap();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alDealerName = new ArrayList();
    String strDealerId = "";
    String strForwardName = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmDealerInvoiceParamsForm);

    alElecrtonicFileFmt =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("EFF", getGmDataStoreVO()));
    alCsvTempName =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CSVTNM", getGmDataStoreVO()));
    alInvoiceLayout =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("INVL", getGmDataStoreVO()));
    alPaymentTerms =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PAYM", getGmDataStoreVO()));
    alCollector =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("COLLTR", getGmDataStoreVO()));
    alInvoiceClosingDate =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("INCLDT", getGmDataStoreVO()));
    strDealerId = GmCommonClass.parseNull(request.getParameter("partyId"));

    gmDealerInvoiceParamsForm.setAlInvoiceCustomer(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("INVCST", getGmDataStoreVO())));
    gmDealerInvoiceParamsForm.setAlElecrtonicFileFmt(alElecrtonicFileFmt);
    gmDealerInvoiceParamsForm.setAlCsvTempName(alCsvTempName);
    gmDealerInvoiceParamsForm.setAlInvoiceLayout(alInvoiceLayout);
    gmDealerInvoiceParamsForm.setAlPaymentTerms(alPaymentTerms);
    gmDealerInvoiceParamsForm.setAlInvoiceClosingDate(alInvoiceClosingDate);
    gmDealerInvoiceParamsForm.setAlCollector(alCollector);
    gmDealerInvoiceParamsForm.setPartyId(strDealerId);

    String strOpt = GmCommonClass.parseNull(gmDealerInvoiceParamsForm.getStrOpt());

    if (strOpt.equals("save")) {
      hmParam.put("PARTYID", strDealerId);
      gmDealerSetupBean.saveDealerInvoiceDetails(hmParam);
    }
    if (!strDealerId.equals("")) {
      alLogReasons = gmCommonBean.getLog(strDealerId, "26230747");// 26230747=Dealer
      gmDealerInvoiceParamsForm.setAlLogReasons(alLogReasons);
    }
    hmParam = gmDealerSetupBean.fetchDealerInvoiceDetails(strDealerId);
    String strEmailFl = GmCommonClass.parseNull((String) hmParam.get("ELECTRONICEMAIL"));
    String strConsolidatedFlag =
        GmCommonClass.parseNull((String) hmParam.get("CONSOLIDATEDBILLING"));
    strEmailFl = strEmailFl.equals("Y") ? "on" : "";
    strConsolidatedFlag = strConsolidatedFlag.equals("Y") ? "on" : "";
    gmDealerInvoiceParamsForm =
        (GmDealerInvoiceParamsForm) GmCommonClass.getFormFromHashMap(gmDealerInvoiceParamsForm,
            hmParam);
    gmDealerInvoiceParamsForm.setElectronicEmail(strEmailFl);
    gmDealerInvoiceParamsForm.setConsolidatedBilling(strConsolidatedFlag);
    strForwardName = "GmDealerInvoiceParamSetup";

    return mapping.findForward(strForwardName);

  }

  /**
   * This method is used fetch dealer account mapping report values
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return ActionForward
   * @throws AppError
   * @throws Exception
   */
  public ActionForward fetchDealerAccounMappingReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmDealerInvoiceParamsForm gmDealerInvoiceParamsForm = (GmDealerInvoiceParamsForm) form;
    instantiate(request, response);
    gmDealerInvoiceParamsForm.loadSessionParameters(request);
    GmDealerInvoiceParamsBean gmDealerInvoiceParamsBean =
        new GmDealerInvoiceParamsBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    ArrayList alResult = new ArrayList();
    String strPartyType = GmCommonClass.parseNull(gmDealerInvoiceParamsForm.getPartyType());
    log.debug("strPartyType=========" + strPartyType);
    String strMasterGrpValue = "Y";
    String strForwardName = "GmDealerAccountReport";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
    hmParam = GmCommonClass.getHashMapFromForm(gmDealerInvoiceParamsForm);
    String strOpt = GmCommonClass.parseNull(gmDealerInvoiceParamsForm.getStrOpt());
    GmPartNumBean gmPartNumBean = new GmPartNumBean(getGmDataStoreVO());
    gmDealerInvoiceParamsForm.setAlstatus(GmCommonClass.getCodeList("GRPSTS", getGmDataStoreVO()));
    gmDealerInvoiceParamsForm.setPartyType(strPartyType);
    gmDealerInvoiceParamsForm.setAlDealerNm(gmPartyInfoBean.fetchPartyNameList(strPartyType,
        strMasterGrpValue));
    alResult = gmDealerInvoiceParamsBean.fetchDealerAccountReport(hmParam);
    hmParam.put("ALRESULT", alResult);
    hmParam.put("VMFILE", "GmDealerAccountMapping.vm");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

    String strGridData = generateOutPut(hmParam);
    gmDealerInvoiceParamsForm.setGridData(strGridData);
    if (strOpt.equals("AccountMappingPopup")) {
      strForwardName = "GmDealerAccounts";
    }


    return mapping.findForward(strForwardName);

  }

  /**
   * This method is used to fetch Dealer associated accounts
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return ActionForward
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadDealerMappedAccounts(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strJSON = "";
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    instantiate(request, response);
    GmDealerInvoiceParamsForm gmDealerInvoiceParamsForm = (GmDealerInvoiceParamsForm) form;
    gmDealerInvoiceParamsForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmDealerInvoiceParamsForm);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmSalesBean gmSalesBean = new GmSalesBean(getGmDataStoreVO());
    GmDealerInvoiceParamsBean gmDealerInvoiceParamsBean =
        new GmDealerInvoiceParamsBean(getGmDataStoreVO());
    String strDealerId = GmCommonClass.parseNull(gmDealerInvoiceParamsForm.getDealerID());
    strJSON =
        gmWSUtil.parseArrayListToJson(gmDealerInvoiceParamsBean.fetchDealerAccounts(strDealerId));
    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return mapping.findForward("GmDealerAccounts");
  }

  /**
   * This method is used get Dealer Name List
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return ActionForward
   * @throws AppError
   * @throws Exception
   */

  public ActionForward fetchDealerList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strPartyType = "26230725";
    String strMasterGrpValue = "";
    String strJSON = "";
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    instantiate(request, response);
    GmDealerInvoiceParamsForm gmDealerInvoiceParamsForm = (GmDealerInvoiceParamsForm) actionForm;
    gmDealerInvoiceParamsForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());
    GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
    strJSON =
        gmWSUtil.parseArrayListToJson(gmPartyInfoBean.fetchPartyNameList(strPartyType,
            strMasterGrpValue));
    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return actionMapping.findForward("success");
  }

  /**
   * This method is used get get Dealer Name List
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return ActionForward
   * @throws AppError
   * @throws Exception
   */

  public ActionForward fetchDearlerInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strJSON = "";
    String strDealerAccountId = GmCommonClass.parseNull(request.getParameter("Cbo_Dealer_Id"));
    HashMap hmParam = new HashMap();
    HashMap hmReturns = new HashMap();
    HashMap hmContact = new HashMap();
    ArrayList alAddress = new ArrayList();
    ArrayList alContact = new ArrayList();
    GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
    GmDealerInvoiceParamsBean gmDealerInvoiceParamsBean =
        new GmDealerInvoiceParamsBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();
    instantiate(request, response);
    GmDealerInvoiceParamsForm gmDealerInvoiceParamsForm = (GmDealerInvoiceParamsForm) actionForm;
    gmDealerInvoiceParamsForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());
    GmDealerInvoiceParamsBean gmDealerSetupBean = new GmDealerInvoiceParamsBean(getGmDataStoreVO());
    GmCRMSurgeonBean gmCRMSurgeonBean = new GmCRMSurgeonBean();
    hmParam.put("PARTYID", strDealerAccountId);
    hmParam.put("DEALERNAME", strDealerAccountId);
    alAddress = gmCRMSurgeonBean.fetchSurgeonAddressReport(hmParam);
    alContact = gmDealerInvoiceParamsBean.fetchDealerAccountReport(hmParam);
    if (alAddress.size() > 0) {
      hmReturns = (HashMap) alAddress.get(0);
    }
    if (alContact.size() > 0) {
      hmContact = (HashMap) alContact.get(0);
      hmReturns.putAll(hmContact);
    }
    hmReturns.putAll(gmDealerSetupBean.fetchDealerInvoiceDetails(strDealerAccountId));
    strJSON = gmWSUtil.parseHashMapToJson(hmReturns);
    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return actionMapping.findForward("success");
  }

  /**
   * This method is used get grid in Dealer accounts mapping screen
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return ActionForward
   * @throws AppError
   * @throws Exception
   */
  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String vmFile = GmCommonClass.parseNull((String) hmParam.get("VMFILE"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.party.GmDealerAccountMapping", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("party/templates");
    templateUtil.setTemplateName(vmFile);
    return templateUtil.generateOutput();
  }
}
