//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\GmPersonalInfoSetupAction.java

package com.globus.party.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmPersonalInfoBean;
import com.globus.party.forms.GmPersonalInfoSetupForm;

/**
 * Action class for personal info setup for the party
 * @author sthadeshwar
 */
public class GmPersonalInfoSetupAction extends GmAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**
	 * @return ActionForward
	 * @roseuid 48FCD3FB0287
	 */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		GmPersonalInfoSetupForm gmPersonalInfoSetupForm = (GmPersonalInfoSetupForm) form;
		gmPersonalInfoSetupForm.loadSessionParameters(request);
		GmPersonalInfoBean gmPersonalInfoBean = new GmPersonalInfoBean();
		HashMap hmResult = null;
		
		String strOpt = gmPersonalInfoSetupForm.getStrOpt();
		String strPartyId = gmPersonalInfoSetupForm.getPartyId();
		
		log.debug("strOpt = " + strOpt);
		log.debug("strPartyId = " + strPartyId);
		log.debug("Children Details = " + gmPersonalInfoSetupForm.getChildrenDetails());
		
		gmPersonalInfoSetupForm.setSavedLabel("");
		gmPersonalInfoSetupForm.setAlGender(GmCommonClass.getCodeList("GENDER"));
		gmPersonalInfoSetupForm.setAlChildren(GmCommonClass.getCodeList("CHILDN"));
		gmPersonalInfoSetupForm.setAlRelationshipStatus(GmCommonClass.getCodeList("RELTNS"));

		if(strOpt.equals("save"))
		{
			hmResult = GmCommonClass.getHashMapFromForm(gmPersonalInfoSetupForm);
			log.debug("Saving values = " + hmResult);
			gmPersonalInfoBean.savePersonalInfo(hmResult);
			gmPersonalInfoSetupForm.setSavedLabel("Successfully saved data");
			
			
		}

		if(!strPartyId.equals("")) {
			hmResult = gmPersonalInfoBean.fetchPersonalInfo(strPartyId);
			log.debug("Fetching values = " + hmResult);
			gmPersonalInfoSetupForm = (GmPersonalInfoSetupForm) GmCommonClass.getFormFromHashMap(gmPersonalInfoSetupForm,hmResult);
		}
		String strRelStatus = gmPersonalInfoSetupForm.getRelationshipStatus();
		String strChildren = gmPersonalInfoSetupForm.getChildren();
		
		if(strRelStatus.equals("92009") || strRelStatus.equals("0") || strRelStatus.equals("")) {	// Single or [Choose One]
			gmPersonalInfoSetupForm.setDisableSpouseDet("true");
		} else {
			gmPersonalInfoSetupForm.setDisableSpouseDet("false");
		}
		
		if(strChildren.equals("92011") || strChildren.equals("0") || strChildren.equals("")) {	// None or [Choose One]
			gmPersonalInfoSetupForm.setDisableChildDet("true");
		} else {
			gmPersonalInfoSetupForm.setDisableChildDet("false");
		}
		
		log.debug("strRelStatus = " + strRelStatus);
		log.debug("strChildren = " + strChildren);
		log.debug("disableSpouseDet = " + gmPersonalInfoSetupForm.getDisableSpouseDet());
		log.debug("disableChildDet = " + gmPersonalInfoSetupForm.getDisableChildDet());
		
		return mapping.findForward("success");
	}
}
