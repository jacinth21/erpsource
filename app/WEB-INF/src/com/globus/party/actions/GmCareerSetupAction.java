// Source file:
// C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\GmCareerSetupAction.java

/**
 * FileName : GmCareerSetupAction.java Description : Author : sthadeshwar Date : Oct 10, 2008
 * Copyright : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmCareerBean;
import com.globus.party.forms.GmCareerSetupForm;

/**
 * Action class for career setup for the party
 * 
 * @author sthadeshwar
 */
public class GmCareerSetupAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @return ActionForward
   * @roseuid 48F7A3450026
   */
  public ActionForward loadCareerSetup(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);

    GmCareerSetupForm gmCareerSetupForm = (GmCareerSetupForm) form;
    gmCareerSetupForm.loadSessionParameters(request);
    GmCareerBean gmCareerBean = new GmCareerBean();

    String strOpt = gmCareerSetupForm.getStrOpt();
    String hcareerId = gmCareerSetupForm.getHcareerId();

    log.debug("strOpt = " + strOpt);

    if (strOpt.equals("edit")) {
      HashMap hmResult = gmCareerBean.fetchEditCareerInfo(hcareerId);
      log.debug("Values fetched for edit = " + hmResult);
      gmCareerSetupForm =
          (GmCareerSetupForm) GmCommonClass.getFormFromHashMap(gmCareerSetupForm, hmResult);
    }

    if (strOpt.equals("save")) {
      HashMap hmResult = GmCommonClass.getHashMapFromForm(gmCareerSetupForm);
      log.debug("Saving values = " + hmResult);
      gmCareerBean.saveCareerInfo(hmResult);
      gmCareerSetupForm.setTitle("");
      gmCareerSetupForm.setCompanyName("");
      gmCareerSetupForm.setCompanyDesc("");
      gmCareerSetupForm.setStartMonth("0");
      gmCareerSetupForm.setStartYear("");
      gmCareerSetupForm.setEndMonth("0");
      gmCareerSetupForm.setEndYear("");
      gmCareerSetupForm.setStillInPosition("off");
      gmCareerSetupForm.setGroupDivision("");
      gmCareerSetupForm.setCountry("0");
      gmCareerSetupForm.setState("0");
      gmCareerSetupForm.setCity("");
      gmCareerSetupForm.setIndustry("0");
    }

    gmCareerSetupForm.setAlMonths(GmCommonClass.getCodeList("MONTH"));
    gmCareerSetupForm.setAlState(GmCommonClass.getCodeList("STATE", getGmDataStoreVO()));
    gmCareerSetupForm.setAlCountry(GmCommonClass.getCodeList("CNTY"));
    gmCareerSetupForm.setAlIndustry(GmCommonClass.getCodeList("INDUS"));


    return mapping.findForward("setup");
  }

  /**
   * @return ActionForward
   * @roseuid 48F7A3450026
   */
  public ActionForward loadCareerReport(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);

    GmCareerSetupForm gmCareerSetupForm = (GmCareerSetupForm) form;
    GmCareerBean gmCareerBean = new GmCareerBean();
    String strPartyId = gmCareerSetupForm.getPartyId();

    log.debug("strPartyId = " + strPartyId);
    if (!strPartyId.equals("")) {
      List ldtResult = gmCareerBean.fetchCareerInfo(strPartyId).getRows();
      log.debug("Fetching values = " + ldtResult);
      gmCareerSetupForm.setLresult(ldtResult);

      /* below code to search for the particular group for party */
      request.setAttribute("SEARCHCODEMAP", GmCommonClass.getSearchList("PACAR")); // PARTY CAREER
    }


    return mapping.findForward("report");
  }
}
