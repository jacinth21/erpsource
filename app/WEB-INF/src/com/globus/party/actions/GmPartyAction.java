/**
 * FileName    : GmPartyAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Dec 5, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.List;
import java.util.MissingResourceException;

import org.apache.log4j.Logger;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author sthadeshwar
 *
 */
public class GmPartyAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public List getRestrictedList(String type, List lResult, String screen) {
		String key = "PARTYSUMMARYLISTSIZE";
		int limit = 0;
		String limitSize = "";
		if(type.equals("Report")) {
			try {
				limitSize = GmCommonClass.getString(key + "_" + screen);
			} catch (MissingResourceException e) {
				limitSize = GmCommonClass.getString(key);
			}
			log.debug("limitSize for " + screen + " = " + limitSize);
			
			if(!limitSize.equals("")) {
				limit = Integer.parseInt(limitSize);
			}
			if(lResult.size() > limit && limit != 0) {
				lResult = lResult.subList(0, limit);
			}
		}
		return lResult;
	}
}
