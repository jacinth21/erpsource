/**
 * FileName    : GmPartyCommonSearchAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Dec 12, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.party.forms.GmPartyCommonSearchForm;

/**
 * @author sthadeshwar
 *
 */
public class GmPartyCommonSearchAction extends GmAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		String forward = "mainpage";
		try {
			GmPartyCommonSearchForm gmPartyCommonSearchForm = (GmPartyCommonSearchForm)form;
			gmPartyCommonSearchForm.loadSessionParameters(request);
			GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean();
			
			ArrayList tabs = new ArrayList();
			ArrayList fields = new ArrayList();

			String searchGroup = gmPartyCommonSearchForm.getSearchGroup();

			if(!searchGroup.equals("")) {
				forward = "filterpage";
				fields = gmPartyInfoBean.fetchPartySearchScreenFilter(searchGroup);
				gmPartyCommonSearchForm.setAlFields(fields);
				
				for(int i=0; i<fields.size(); i++) {
					HashMap hmField = (HashMap)fields.get(i);
					String fieldType = (String)hmField.get("FIELDTYPE");
					String fieldAttr = (String)hmField.get("FIELDATTR");
				
					if(fieldType.equals("92017")) {
						ArrayList alList = GmCommonClass.getCodeList(fieldAttr);
						for(int k=0; k<alList.size(); k++) {
							// Adding lookup id in attribute list, as we don't need the code lookup (901) id here
							((HashMap)alList.get(k)).put("LOOKUPID", (String)hmField.get("LOOKUPID"));	
						}
						((HashMap)fields.get(i)).remove("FIELDATTR");
						((HashMap)fields.get(i)).put("FIELDATTR", alList);
					}
				}
				request.setAttribute("FIELDS", fields);
				
			} else {
				String strPartyType = gmPartyCommonSearchForm.getPartyType();
				String title = GmCommonClass.getCodeNameFromCodeId(strPartyType);
				log.debug("title in GmPartyCommonSearchAction = " + title);
				gmPartyCommonSearchForm.setTitle(title);
				
				forward = "mainpage";
				tabs = gmPartyInfoBean.fetchPartySearchScreenFilterTabs(strPartyType);
				gmPartyCommonSearchForm.setAlTabs(tabs);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return mapping.findForward(forward);
	}
}
