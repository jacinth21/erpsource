/**
 * FileName    : GmAboutMeSummaryAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Nov 26, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.party.forms.GmAboutMeSummaryForm;

/**
 * @author sthadeshwar
 *
 */
public class GmAboutMeSummaryAction extends GmAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)
	throws Exception {
		
		try {
			
			GmAboutMeSummaryForm gmAboutMeSummaryForm = (GmAboutMeSummaryForm)form;
			GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean();
			HashMap hmValues = new HashMap();
			GmCommonUploadBean uploadBean = new GmCommonUploadBean();
			
			String strPartyId = gmAboutMeSummaryForm.getPartyId();
			String strPartyType = gmAboutMeSummaryForm.getPartyType();
			hmValues = gmPartyInfoBean.fetchPartyAboutMeSummary(strPartyId);
			
			// Search links
			gmAboutMeSummaryForm = (GmAboutMeSummaryForm)GmCommonClass.getFormFromHashMap(gmAboutMeSummaryForm,hmValues);
			
			String website = gmAboutMeSummaryForm.getPersonalWebsite();
			if(!website.equals("") && website.indexOf("http://") == -1) {
				website = "http://" + website;
				gmAboutMeSummaryForm.setPersonalWebsite(website);
			}
			
			String hobbies = getSearchHyperlinks(gmAboutMeSummaryForm.getHobbies(), "OTHERINFO", "PAOTH", strPartyType);
			String specialty = getSearchHyperlinks(gmAboutMeSummaryForm.getSpeciality(), "OTHERINFO", "PAOTH", strPartyType);
			String interests = getSearchHyperlinks(gmAboutMeSummaryForm.getProfessionalInterests(), "OTHERINFO", "PAOTH", strPartyType);

			gmAboutMeSummaryForm.setHobbies(hobbies);
			gmAboutMeSummaryForm.setSpeciality(specialty);
			gmAboutMeSummaryForm.setProfessionalInterests(interests);
			
			ArrayList alUploadInfo = uploadBean.fetchUploadInfo("91181", strPartyId);
			//log.debug("alUploadInfo = " + alUploadInfo);
			String strFileId = "";
			if(alUploadInfo.size() > 0) {
				HashMap hmRow = (HashMap)alUploadInfo.get(0);
				Object obj = hmRow.get("ID");
				if(obj != null)
					strFileId = (String)obj;
			}
			
			gmAboutMeSummaryForm.setFileId(strFileId);
			
		} catch (Exception e) {
			throw new AppError(e);
		}
		
		return mapping.findForward("success");
	}
	
	/**
	 * Common method to create the search hyperlinks for hobbies, specialty and interests on summary page
	 * @param strSearchString - the search string
	 * @param strPropertyName - name of the property for which the search is to be provided
	 * @param strSearchGroup - search group from the t912 table 
	 * @param strPartyType - party type 
	 */
	public String getSearchHyperlinks(String strSearchString, String strPropertyName, String strSearchGroup, String strPartyType) {
		try {
			String returnString = "";
			String[] searchValues = strSearchString.split(";");
			
			HashMap hmSearch = GmCommonClass.getSearchList(strSearchGroup);
			log.debug("hmSearch = " + hmSearch);
			String strLookupId = GmCommonClass.parseNull((String)hmSearch.get(strPropertyName));
			log.debug("strLookupId = " + strLookupId);
			if(!strLookupId.equals("")) {
				for(int i=0; i<searchValues.length; i++) {
					returnString += "<a href =/gmSearchParty.do?partyType="+strPartyType+"&lookupId="+strLookupId+"&searchString="+searchValues[i].replaceAll(" ", "~")+">" + searchValues[i] + "</a>, ";
				}
				strSearchString = returnString.substring(0, returnString.length() - 2);	// To remove the last comma (,)
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return strSearchString;
	}
	
}
