//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\GmGlobusAssociationSetupAction.java

package com.globus.party.actions;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.party.beans.GmGlobusAssociationBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.party.forms.GmGlobusAssociationSetupForm;

/**
 * Action class for globus association setup for the party
 * @author sthadeshwar
 */
public class GmGlobusAssociationSetupAction extends GmDispatchAction 
{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * @return ActionForward
	 * @roseuid 48FCD4340056
	 */
	public ActionForward loadGlobusAssociationSetup(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {

			GmGlobusAssociationSetupForm gmGlobusAssociationSetupForm = (GmGlobusAssociationSetupForm) form;
			gmGlobusAssociationSetupForm.loadSessionParameters(request);

			GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean();
			GmGlobusAssociationBean gmGlobusAssociationBean = new GmGlobusAssociationBean();
			HashMap hmParam = new HashMap();
			HashMap hmValues = new HashMap();
			
			String strOpt = gmGlobusAssociationSetupForm.getStrOpt();
			String strPartyType = gmGlobusAssociationSetupForm.getPartyType();
			String strPersonnelType = gmGlobusAssociationSetupForm.getPersonnelType();
			String hGlobusAssociationId = gmGlobusAssociationSetupForm.getHglobusAssociationId();
			
			log.debug("strOpt = " + strOpt);
			log.debug("strPartyType = " + strPartyType);
			log.debug("hGlobusAssociationId = " + hGlobusAssociationId);
			
			gmGlobusAssociationSetupForm.setAlPersonnelType(GmCommonClass.getCodeList("PTTYP", strPartyType));
			
			if(strOpt.equals("save")) {
				hmParam = GmCommonClass.getHashMapFromForm(gmGlobusAssociationSetupForm);
                hGlobusAssociationId = gmGlobusAssociationBean.saveGlobusAssociationInfo(hmParam);
                gmGlobusAssociationSetupForm.setHglobusAssociationId(hGlobusAssociationId);
                gmGlobusAssociationSetupForm.setPersonnelType("0");
                gmGlobusAssociationSetupForm.setPrimaryFlag("off");
			}
			
			if(strOpt.equals("load")) {
				gmGlobusAssociationSetupForm.setAlPersonnelName(gmPartyInfoBean.fetchPartyNameList(strPersonnelType));
			}
			
			if(strOpt.equals("edit")) {
				hmValues = gmGlobusAssociationBean.fetchEditGlobusAssociationInfo(hGlobusAssociationId);
	            strPersonnelType = (String)hmValues.get("PERSONNELTYPE");
	            gmGlobusAssociationSetupForm.setAlPersonnelName(gmPartyInfoBean.fetchPartyNameList(strPersonnelType));
	            gmGlobusAssociationSetupForm = (GmGlobusAssociationSetupForm)GmCommonClass.getFormFromHashMap(gmGlobusAssociationSetupForm,hmValues);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
		return mapping.findForward("setup");
	}
	
	public ActionForward loadGlobusAssociationReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			GmGlobusAssociationSetupForm gmGlobusAssociationSetupForm = (GmGlobusAssociationSetupForm) form;
			GmGlobusAssociationBean gmGlobusAssociationBean = new GmGlobusAssociationBean();
			
			String strPartyId = gmGlobusAssociationSetupForm.getPartyId();
			log.debug("strPartyId = " + strPartyId);
			if(!strPartyId.equals("")) {
				List lResult = gmGlobusAssociationBean.fetchGlobusAssociationInfo(strPartyId).getRows();
				gmGlobusAssociationSetupForm.setLresult(lResult);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
		return mapping.findForward("report");
	}
}
