package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmAchievementBean;
import com.globus.party.forms.GmAcheivementSetupForm;

public class GmAchievementSetupAction extends GmPartyAction {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public ActionForward loadAchievementSetup(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		try {
			GmAcheivementSetupForm gmAcheivementSetupForm = (GmAcheivementSetupForm) form;
			gmAcheivementSetupForm.loadSessionParameters(request);
			
			GmAchievementBean gmAchievementBean = new GmAchievementBean();

			HashMap hmParam = new HashMap();
			
			String strOpt = gmAcheivementSetupForm.getStrOpt();

			String strPartyId = gmAcheivementSetupForm.getPartyId();
			String strAchievementId = gmAcheivementSetupForm.getHachievementID();
			
			log.debug("strOpt = " + strOpt);
			log.debug("strPartyId = " + strPartyId);
			
			if (strOpt.equalsIgnoreCase("edit")) {
				hmParam = gmAchievementBean.editAchievementInfo(strAchievementId);
				strPartyId = (String) hmParam.get("PARTYID");
				gmAcheivementSetupForm = (GmAcheivementSetupForm) GmCommonClass.getFormFromHashMap(gmAcheivementSetupForm, hmParam);

			} 
			
			if (strOpt.equalsIgnoreCase("save")) {
				hmParam = GmCommonClass.getHashMapFromForm(gmAcheivementSetupForm);
				hmParam.put("ACHIEVEMENTID", strAchievementId);
				hmParam.put("PARTYID", strPartyId);
				log.debug("Saving values = " + hmParam);
				gmAchievementBean.saveAchievementInfo(hmParam);
				gmAcheivementSetupForm.reset();
			}
			
			gmAcheivementSetupForm.setAlType(GmCommonClass.getCodeList("AHTYP"));
			gmAcheivementSetupForm.setAlMonth(GmCommonClass.getCodeList("MONTH"));

		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
		return mapping.findForward("success");
	}

	/*
	 * The following method will be used in displaying the display tag results
	 * for party achievement
	 */

	public ActionForward loadAchievementReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		GmAcheivementSetupForm gmAcheivementSetupForm = (GmAcheivementSetupForm) form;
		GmAchievementBean gmAchievementBean = new GmAchievementBean();
		String strPartyId = gmAcheivementSetupForm.getPartyId();
		List lResult = null;
		int limit = 0;
		
		if (!strPartyId.equals("")) {
			lResult = gmAchievementBean.fetchAchievementInfo(strPartyId).getRows();
			
			String type = GmCommonClass.parseNull((String)request.getParameter("TYPE"));
			int size = lResult.size();
			lResult = getRestrictedList(type, lResult, "ACHIEVEMENTS");
			if(size > lResult.size()) {
				request.setAttribute("MORE_ACHIEVEMENTS", "true");
			}
			
			gmAcheivementSetupForm.setLresult(lResult);
	
			/* below code to search for the particular group for party */
			request.setAttribute("SEARCHCODEMAP", GmCommonClass.getSearchList("ACHIE"));
			// "ACHIV" PARTY: Achievement
		}

		return mapping.findForward("report");
	}

}
