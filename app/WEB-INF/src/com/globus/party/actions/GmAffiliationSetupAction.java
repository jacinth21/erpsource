//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\GmAffiliationSetupAction.java

/**
 * FileName    : GmAffiliationDetailAction.java 
 * Description :
 * Author      : Angela 
 * Date        : Nov 4, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmAffiliationBean;
import com.globus.party.forms.GmAffiliationSetupForm;
import com.globus.party.forms.GmLicenseSetupForm;

/**
 * Action class for Affiliation setup for the party
 * 
 * @author Angela
 */
public class GmAffiliationSetupAction extends GmPartyAction {

	/**
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return org.apache.struts.action.ActionForward
	 */

	Logger log = GmLogger.getInstance(this.getClass().getName());

	public ActionForward loadAffiliationSetup(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		try {
			GmAffiliationSetupForm gmAffiliationSetupForm = (GmAffiliationSetupForm) form;
			gmAffiliationSetupForm.loadSessionParameters(request);
			
			String strOpt = gmAffiliationSetupForm.getStrOpt();
			String strAffId = gmAffiliationSetupForm.getAffId();

			GmAffiliationBean gmAffiliationBean = new GmAffiliationBean();
			HashMap hmParam = new HashMap();

			log.debug("strOpt = " + strOpt);
			if (strOpt.equals("edit")) {
				hmParam = gmAffiliationBean.fetchEditAffiliationDetails(strAffId);
				gmAffiliationSetupForm = (GmAffiliationSetupForm) GmCommonClass.getFormFromHashMap(gmAffiliationSetupForm, hmParam);
			}
			if (strOpt.equals("save")) {
				hmParam = GmCommonClass.getHashMapFromForm(gmAffiliationSetupForm);
				gmAffiliationBean.saveAffiliationDetails(hmParam);
				gmAffiliationSetupForm.reset();
			}

			gmAffiliationSetupForm.setAlTypes(GmCommonClass.getCodeList("AFFTP"));
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
		return mapping.findForward("success");
	}

	public ActionForward loadAffiliationReport(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		GmAffiliationSetupForm gmAffiliationSetupForm = (GmAffiliationSetupForm) form;
		String strPartyId = GmCommonClass.parseNull((String) gmAffiliationSetupForm.getPartyId());
		List lResult = null;
		GmAffiliationBean gmAffBean = new GmAffiliationBean();
		
		log.debug("strPartyId = " + strPartyId);
		if (!strPartyId.equals("")) { // fetch
			lResult = gmAffBean.fetchAffiliationDetails(strPartyId).getRows();// (strPartyId).getRows();
			
			String type = GmCommonClass.parseNull((String)request.getParameter("TYPE"));
			log.debug("type = " + type);
			int size = lResult.size();
			lResult = getRestrictedList(type, lResult, "AFFILIATIONS");
			if(size > lResult.size()) {
				request.setAttribute("MORE_AFFILIATIONS", "true");
			}
			gmAffiliationSetupForm.setLresult(lResult);
			/*
			 * below code to search for the particular group for party:
			 * PAAFFAffiliation
			 */
			request.setAttribute("SEARCHCODEMAP", GmCommonClass.getSearchList("PAAFF"));
		}
		return mapping.findForward("report");
	}

}