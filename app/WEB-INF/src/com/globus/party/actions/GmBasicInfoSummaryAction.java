/**
 * FileName    : GmBasicInfoSummaryAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Nov 24, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.party.forms.GmBasicInfoSummaryForm;

/**
 * @author sthadeshwar
 *
 */
public class GmBasicInfoSummaryAction extends GmAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {
		try {
			
			log.debug("Fetching Basic Info");
			
			GmBasicInfoSummaryForm gmBasicInfoSummaryForm = (GmBasicInfoSummaryForm)form;
			gmBasicInfoSummaryForm.loadSessionParameters(request);
			
			String strPartyId = gmBasicInfoSummaryForm.getPartyId();
			log.debug("strPartyId = " + strPartyId);
			
			GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean();
			HashMap hmValues = gmPartyInfoBean.fetchPartyBasicInfo(strPartyId);
			
			log.debug("hmValues = " + hmValues);
			String address = (String)hmValues.get("ADDRESS");
			address = address.replaceAll("~", "\n");
			gmBasicInfoSummaryForm.setAlContacts((ArrayList)hmValues.get("CONTACT"));
			gmBasicInfoSummaryForm.setAddress(address);
			gmBasicInfoSummaryForm.setName((String)hmValues.get("PARTYNAME"));
			
			log.debug("Address = " + gmBasicInfoSummaryForm.getAddress());
			
			log.debug("Fetching Image");
			
			GmCommonUploadBean uploadBean = new GmCommonUploadBean();
			ArrayList alUploadInfo = new ArrayList();
			
			alUploadInfo = uploadBean.fetchUploadInfo("91182", strPartyId);
			
			String strFileId = "999";
			if(alUploadInfo.size() > 0) {
				HashMap hmRow = (HashMap)alUploadInfo.get(0);
				Object obj = hmRow.get("ID");
				log.debug("obj = " + obj);
				if(obj != null)
					strFileId = (String)obj;
			}
			
			log.debug("strFileId = " + strFileId);
			request.setAttribute("FILEID", strFileId);
			
			log.debug("Fetching Image Exit");
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
		return mapping.findForward("basicinfo");
	}
	

}
