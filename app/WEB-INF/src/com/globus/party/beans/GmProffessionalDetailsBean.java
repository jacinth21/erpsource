//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\beans\\GmProffessionalDetailsBean.java

package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * Bean class for fetching and saving the 'about me' details of party
 * @author sthadeshwar
 */
public class GmProffessionalDetailsBean 
{
	 Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
   /**
    * @roseuid 49139141008C
    */
   public GmProffessionalDetailsBean() 
   {
    
   }
   
   /**
    * Fetches the 'About Me' details for party
    * @param strPartyId
    * @return java.util.HashMap
    * @throws com.globus.common.beans.AppError
    * @roseuid 48FCE457017F
    */
   public ArrayList fetchProfDetails(String strPartyId) throws AppError 
   {
	   ArrayList alProfInfo;
 	   GmDBManager gmDBManager = new GmDBManager();
       gmDBManager.setPrepareString("gm_pkg_cm_profdetails.gm_fch_profdetails", 2);
       gmDBManager.setString(1,strPartyId);
       gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
       gmDBManager.execute();
       alProfInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
       gmDBManager.close();      
       return alProfInfo;
   }
   
   /**
    * Saves the 'About Me' information for the party
    * @param hmParam
    * @return java.lang.String
    * @throws com.globus.common.beans.AppError
    * @roseuid 48FCE46A00A5
    */
   public void saveProfDetails(int partyid, String[] strArrFieldName, String[] strArrFieldValue, String strUserid) throws AppError 
   {	   
	   log.debug("Enter");
	   	 GmDBManager gmDBManager = new GmDBManager();		
		 gmDBManager.setPrepareString("gm_pkg_cm_party_trans.gm_sav_party_otherinfo", 4);
		 String strValue ="";
		 
		 for(int i=0;i<strArrFieldName.length;i++)
		 {			
			 
			log.debug("strArrFieldName = [" + i + "] = " + strArrFieldName[i]);
			log.debug("strArrFieldValue = [" + i + "] = " + strArrFieldValue[i]);
			strValue = GmCommonClass.parseNull(strArrFieldValue[i]);
			if(!strValue.equals(""))
				strValue = strValue.charAt(strValue.length()-1) != ';' ? strValue+=";" : strValue;
			log.debug(partyid+ ", "+ GmCommonClass.getCodeID(strArrFieldName[i], "TVALS") +", "+ strValue + ", " +  strUserid );
		    gmDBManager.setInt(1,partyid);
		    gmDBManager.setString(2,GmCommonClass.getCodeID(strArrFieldName[i], "TVALS"));
		    gmDBManager.setString(3,strValue);
		    gmDBManager.setString(4,strUserid);
		    gmDBManager.execute();
		 }
		    gmDBManager.commit();
		    gmDBManager.close();
		 log.debug("Exit");	 
   }
}
