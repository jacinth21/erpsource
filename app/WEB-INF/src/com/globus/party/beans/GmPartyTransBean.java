// Source file:
// C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\beans\\GmPartyTransBean.java

package com.globus.party.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * Bean class for saving the basic details of party
 * 
 * @author sthadeshwar
 */
public class GmPartyTransBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @roseuid 49138FCB0273
   */
  public GmPartyTransBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());

  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPartyTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * Saves the basic information for the party in db
   * 
   * @param hmParam
   * @return java.lang.String
   * @throws com.globus.common.beans.AppError
   * @roseuid 48F65CB30285
   */
  public String savePartyInfo(HashMap hmParam) throws AppError {
    String strPartyIdFromDb = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strPartyId = (String) hmParam.get("PARTYID");
    String strLastName = (String) hmParam.get("LASTNAME");
    String strFirstName = (String) hmParam.get("FIRSTNAME");
    String strPartyName = (String) hmParam.get("PARTYNAME");
    String strMiddleInitial = (String) hmParam.get("MIDDLEINITIAL");
    String strPartyType = (String) hmParam.get("PARTYTYPE");
    String strUserId = (String) hmParam.get("USERID");
    String strInActiveFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("INACTIVEFLAG"));
    String strPartyNameEn = (String) hmParam.get("PARTYNAMEEN");
    // If Inactive field is checked, need to pass 'N' to the DB for active flag column
    if (strInActiveFlag.equals("Y")) {
      strInActiveFlag = "N";
    }
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

    gmDBManager.setPrepareString("gm_pkg_cm_party_trans.gm_sav_partyinfo", 9);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);

    gmDBManager.setString(1, strPartyId);
    gmDBManager.setString(2, strFirstName);
    gmDBManager.setString(3, strLastName);

    gmDBManager.setString(4, strMiddleInitial);
    log.debug("strMiddleInitial = " + strMiddleInitial);
    gmDBManager.setString(5, strPartyName);
    gmDBManager.setString(6, strPartyType);
    gmDBManager.setString(7, strInActiveFlag);
    gmDBManager.setString(8, strUserId);
    gmDBManager.setString(9, strPartyNameEn);
    gmDBManager.execute();

    strPartyIdFromDb = gmDBManager.getString(1);
    log.debug("strPartyIdFromDb = " + strPartyIdFromDb);

    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strPartyIdFromDb, strLogReason, strUserId, "1236"); // TODO
                                                                                            // Create
                                                                                            // 1227
                                                                                            // - Log
                                                                                            // Code
                                                                                            // Number
                                                                                            // for
                                                                                            // Demand
                                                                                            // Sheet
    }
    gmDBManager.commit();

    return strPartyIdFromDb;
  }


  /**
   * Saves the basic information for the party in db
   * 
   * @param hmParam
   * @return java.lang.String
   * @throws com.globus.common.beans.AppError
   * 
   */
  public String saveUserScInfo(HashMap hmParam) throws AppError {
    String strPartyIdFromDb = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strPartyId = (String) hmParam.get("PARTYID");
    String strLastName = (String) hmParam.get("LASTNAME");
    String strFirstName = (String) hmParam.get("FIRSTNAME");
    String strMiddleInitial = (String) hmParam.get("MIDDLEINITIAL");
    String strUserId = (String) hmParam.get("USERID");
    // String strPartyType = (String)hmParam.get("PARTYTYPE");
    String strInActiveFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("INACTIVEFLAG"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

    gmDBManager.setPrepareString("gm_pkg_cm_party_trans.gm_sav_user_sc_info", 6);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.setString(2, strFirstName);
    gmDBManager.setString(3, strMiddleInitial);
    gmDBManager.setString(4, strLastName);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, strInActiveFlag);

    gmDBManager.execute();
    strPartyIdFromDb = gmDBManager.getString(1);
    log.debug("strPartyIdFromDb = " + strPartyIdFromDb);

    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strPartyIdFromDb, strLogReason, strUserId, "1236"); // TODO
                                                                                            // Create
                                                                                            // 1227
                                                                                            // - Log
                                                                                            // Code
                                                                                            // Number
                                                                                            // for
                                                                                            // Demand
                                                                                            // Sheet
    }
    gmDBManager.commit();

    return strPartyIdFromDb;
  }
}
