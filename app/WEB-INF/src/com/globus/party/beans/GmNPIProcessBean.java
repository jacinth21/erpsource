package com.globus.party.beans;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.hornetq.utils.json.JSONArray;
import org.hornetq.utils.json.JSONException;
import org.hornetq.utils.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;

import org.apache.log4j.Logger;
import org.omg.CORBA.portable.InputStream;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * Bean class for fetching the address details of party
 * 
 * @author arajan
 */
public class GmNPIProcessBean extends GmBean {
  // Code to Initialize the Logger Class.
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmNPIProcessBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmNPIProcessBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * saveNPITransactionDetails - To save NPI details
   * 
   * @throws AppError
   */
  public String saveNPITransactionDetails(HashMap hmParam) throws AppError {

    String strnpitranid = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strNpiTranId = GmCommonClass.parseNull((String) hmParam.get("NPITRANID"));
    String strTranId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strAccID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strNpiNum = GmCommonClass.parseNull((String) hmParam.get("NPINUM"));
    String strSurgeonName = GmCommonClass.parseNull((String) hmParam.get("SURGEONNAME"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strSubmitType = GmCommonClass.parseNull((String) hmParam.get("SUBMITTYPE"));
    String strSaveAction = GmCommonClass.parseNull((String) hmParam.get("SAVEACTION"));
    String strFlag = "";

    if (strSaveAction.equals("UpdateTxn")) {
      updateNPITransactionId(gmDBManager, hmParam);
    } else {
      gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_sav_npi_transaction", 8);
      gmDBManager.registerOutParameter(1, java.sql.Types.NVARCHAR);
      gmDBManager.setString(1, strNpiTranId);
      gmDBManager.setString(2, strTranId);
      gmDBManager.setString(3, strAccID);
      gmDBManager.setString(4, strNpiNum);
      gmDBManager.setString(5, strSurgeonName);
      gmDBManager.setString(6, strFlag);
      gmDBManager.setString(7, strUserId);
      gmDBManager.setString(8, strSubmitType);
      gmDBManager.execute();
      strnpitranid = GmCommonClass.parseNull(gmDBManager.getString(1));
    }
    gmDBManager.commit();
    return strnpitranid;
  }

  /**
   * fetchNPITransactionDetails - To fetch NPI details
   * 
   * @throws AppError
   */
  public ArrayList fetchNPITransactionDetails(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strTranId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    gmDBManager.setPrepareString("gm_pkg_cm_npi_process_rpt.gm_fch_npi_transaction", 2);
    gmDBManager.setString(1, strTranId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    ArrayList alNPIDetails =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    log.debug("alNPIDetails============= "+alNPIDetails);
    gmDBManager.close();
    return alNPIDetails;
  }

  /**
   * cancelNPITransactionDetails - To void NPI details
   * 
   * @throws AppError
   */
  public void cancelNPITransactionDetails(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strNpiTranId = GmCommonClass.parseNull((String) hmParam.get("NPITRANID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_cancel_npi_transaction", 2);
    gmDBManager.setString(1, strNpiTranId);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * save NPITransaction status
   * 
   * @throws AppError
   */
  public void saveNPITransactionStatus(GmDBManager gmDBManager, String strRefID, String strUserId)
      throws AppError {
    gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_save_npi_tran_status", 2);
    gmDBManager.setString(1, strRefID);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
  }

  /**
   * updateNPITransactionId - To update the old transaction id with new one
   * 
   * @throws AppError
   */
  public void updateNPITransactionId(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    String strOldTxnId = GmCommonClass.parseNull((String) hmParam.get("HTXNID"));
    String strNewTranId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_upd_npi_tran_id", 2);
    gmDBManager.setString(1, strOldTxnId);
    gmDBManager.setString(2, strNewTranId);
    gmDBManager.execute();
  }

  /**
   * fetchNPITempDetails - To fetch NPI details where the transaction flag is not updated to 'Y'
   * 
   * @throws AppError
   */
  public ArrayList fetchNPITempDetails(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strTranId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    gmDBManager.setPrepareString("gm_pkg_cm_npi_process_rpt.gm_fch_npi_temp_transaction", 2);
    gmDBManager.setString(1, strTranId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    ArrayList alNPIDetails =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alNPIDetails;
  }

  /**
   * fetchNPIHistoryDtls - To fetch NPI History Details
   * 
   * @throws AppError
   */
  public ArrayList fetchNPIHistoryDtls(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_npi_process_rpt.gm_fch_npi_hist_dtls", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("TXNID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("REPID")));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    ArrayList alNPIDetails =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    return alNPIDetails;
  }

  /**
   * loadNPIErrorDetails - To fetch Invalid NPI Details
   * 
   * @throws AppError
   */

  public ArrayList loadNPIErrorDetails(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alNPIErrDetails = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    String strNPINum = GmCommonClass.parseNull((String) hmParam.get("SEARCHNPINUM"));
    String strSurgeonNm = GmCommonClass.parseNull((String) hmParam.get("SEARCHSURGEONNAME"));
    String strAccountId = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strNPIFromDt = GmCommonClass.parseNull((String) hmParam.get("NPIFROMDT"));
    String strNPIToDt = GmCommonClass.parseNull((String) hmParam.get("NPITODT"));
    String strCompDFmt = getCompDateFmt();
    String strCompId = getCompId();
    sbQuery
        .append("SELECT c6640_ref_id refid, c6600_surgeon_npi_id npiid, c6640_surgeon_name surgeonname, ");
    sbQuery
        .append("c6640_reason reason, c704_account_id accid, get_account_name(c704_account_id) accname,c6640_created_date createdby, ");
    sbQuery
        .append("c703_sales_rep_id repid, get_rep_name(c703_sales_rep_id) repname, v700.ad_name adname ");
    sbQuery.append("FROM t6640_npi_transaction T6640, v700_territory_mapping_detail V700 ");
    sbQuery.append("WHERE t6640.c6640_transaction_fl = 'Y' ");
    sbQuery.append("AND t6640.c704_account_id = v700.ac_id(+) ");
    if (!strNPINum.equals("")) {
      sbQuery.append("AND t6640.c6600_surgeon_npi_id = '" + strNPINum + "'");
    }
    if (!strSurgeonNm.equals("")) {
      sbQuery.append("AND t6640.c6640_surgeon_name = '" + strSurgeonNm + "'");
    }
    if (!strAccountId.equals("")) {
      sbQuery.append("AND t6640.c704_account_id = '" + strAccountId + "'");
    }
    if (!strRepId.equals("")) {
      sbQuery.append("AND t6640.c703_sales_rep_id = '" + strRepId + "'");
    }
    if (!strTxnId.equals("")) {
      sbQuery.append("AND t6640.c6640_ref_id = '" + strTxnId + "'");
    }
    if (!strNPIFromDt.equals("") && !strNPIToDt.equals("")) {
      sbQuery.append("AND trunc(t6640.c6640_created_date) BETWEEN to_date('" + strNPIFromDt + "','"
          + strCompDFmt + "')");
      sbQuery.append("AND to_date('" + strNPIToDt + "','" + strCompDFmt + "')");
    }

    sbQuery.append("AND t6640.c1900_company_id = " + strCompId);
    sbQuery.append(" AND t6640.c6640_void_fl IS NULL ");
    sbQuery.append("AND T6640.C6640_VALID_FL = 'N' AND NVL(t6640.c6640_user_validated,'N') != 'Y'");
    sbQuery.append(" Order by c6640_ref_id");

    log.debug("sbQuery.toString()>>>>>>>>>>" + sbQuery.toString());
    alNPIErrDetails = gmDBManager.queryMultipleRecords(sbQuery.toString());
    return alNPIErrDetails;
  }

  /**
   * saveValidNPI - To make the Invalid NPI to Valid
   * 
   * @throws AppError
   */
  public void saveValidNPI(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_sav_valid_npi", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("NPITRANID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * fetch Valid NPI records- To make the valid NPI records
   */
  public HashMap fetchValidNPIDtsl(HashMap hmParam) throws AppError {
    HashMap hmDetails = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_fch_valid_NPI_Dtls", 2);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("NPITRANID")));
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmDetails;
  }

  /**
   * saveNewNPI - To make the Invalid NPI to Valid
   * 
   * @throws AppError
   */
  public String saveNewNPI(String strNpiNum, String strOrdId, String strUserId) throws AppError,
      IOException,JSONException {
    log.debug("saveNewNPI is "+strNpiNum);
    String strNPIURL = GmCommonClass.parseNull(GmCommonClass.getString("NPIURL"));
    log.debug("strNPIURL is "+strNPIURL);
    String strStatusfl = "N";
    String strURL = strNPIURL+strNpiNum;
    log.debug("sURL"+strURL);
    // Connect to the URL using java's native library
    StringBuffer sbQuery = new StringBuffer();
    JsonArray jsonArray = new JsonArray();
    JsonArray jAddArray = new JsonArray();
    JsonObject jAddbject = new JsonObject();
    URL url = new URL(strURL);
    BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
    JsonParser jp = new JsonParser();
    JsonElement root = jp.parse(br);
    JsonObject  jobject = root.getAsJsonObject();
    jsonArray = jobject.getAsJsonArray("results");

    String strFirstNm = "";
    String strMiddleNm = "";
    String strLastNm = "";
    String strCountry = "";
    String strAdd1 = "";
    String strAdd2 = "";
    String strCity = "";
    String strZipcode = "";
    String strState = "";
    String strMidInitial = "";
        if(jsonArray.size() > 0){
          strStatusfl = "Y";
        }
    for(int i = 0; i < jsonArray.size(); i++){
      jobject = jsonArray.get(i).getAsJsonObject();
      JsonObject jbasicobject = jobject.getAsJsonObject("basic");
      strFirstNm = jbasicobject.get("first_name").getAsString();
      //suppose middle name is not there in object
      if (jbasicobject.has("middle_name")) {
      strMiddleNm = jbasicobject.get("middle_name").getAsString();
      }else{
        strMiddleNm = "";
      }
      strLastNm = jbasicobject.get("last_name").getAsString();
      jAddArray = jobject.getAsJsonArray("addresses");
      jAddbject = jAddArray.get(i).getAsJsonObject();
      strCountry = jAddbject.get("country_code").getAsString();
      strAdd1 = jAddbject.get("address_1").getAsString();
      strAdd2 = jAddbject.get("address_2").getAsString();
      strCity = jAddbject.get("city").getAsString();
      strState = jAddbject.get("state").getAsString();
      strZipcode = jAddbject.get("postal_code").getAsString();
      
      if(strMiddleNm.length() >= 1){
        strMidInitial = strMiddleNm.substring(0, 1);
      }
      
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_sav_npi", 12);
      gmDBManager.setString(1, strNpiNum);
      gmDBManager.setString(2, strOrdId);
      gmDBManager.setString(3, strFirstNm);
      gmDBManager.setString(4, strMidInitial);
      gmDBManager.setString(5, strLastNm);
      gmDBManager.setString(6, strAdd1);
      gmDBManager.setString(7, strAdd2);
      gmDBManager.setString(8, strCity);
      gmDBManager.setString(9, strZipcode);
      gmDBManager.setString(10, strState);
      gmDBManager.setString(11, strCountry);
      gmDBManager.setString(12, strUserId);
      gmDBManager.execute();
      gmDBManager.commit();
    } 
    return strStatusfl;
  }

  /**
   * saveNewNPI - To make the Invalid NPI to Valid
   * 
   * @throws AppError
   */
  public void saveNewNPIMasterStatus(String strNpiNum, String strOrdId, String strStatusfl, String strUserId)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_sav_npi_master_update", 4);
    gmDBManager.setString(1, strNpiNum);
    gmDBManager.setString(2, strOrdId);
    gmDBManager.setString(3, strStatusfl);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }
  /**
   * mail - To make the Invalid NPI to Valid
   * 
   * @throws AppError
   */ 
  public void invalidNPISendMail(HashMap hmParam) throws AppError {
    HashMap hmEmailDetails = new HashMap();
    String strOrdId = GmCommonClass.parseNull((String) hmParam.get("TRANSID"));
    String strNpiNum = GmCommonClass.parseNull((String) hmParam.get("NPINO"));
    String strMessage = "Invalid NPI Number";
    String strMailBody = strNpiNum+" Passing NPI # is not available in  NPI database for the order "+ strOrdId;
    String strToMail = GmCommonClass.getRuleValue("NPIAPI", "EMAIL");

    hmEmailDetails.put("strFrom", "notification@globusmedical.com");
    hmEmailDetails.put("strTo", strToMail);
    hmEmailDetails.put("strSubject", strMessage);
    hmEmailDetails.put("strMessageDesc", strMailBody);
    log.debug(" Sending email for new Invalid NPI " + hmEmailDetails);
    try {
      GmCommonClass.sendMail(hmEmailDetails);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
}
  /**
   * saveNPIWIDTransactionDetails - To save NPI and Surgeon details
   * 
   * @throws AppError
   */
  public String saveNPIWIDTransactionDetails(HashMap hmParam) throws AppError {

    String strnpitranid = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strNpiTranId = GmCommonClass.parseNull((String) hmParam.get("NPITRANID"));
    String strTranId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strAccID = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTID"));
    String strNpiNum = GmCommonClass.parseNull((String) hmParam.get("NPINUM"));
    String strSurgeonName = GmCommonClass.parseNull((String) hmParam.get("SURGEONNAME"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strSubmitType = GmCommonClass.parseNull((String) hmParam.get("SUBMITTYPE"));
    String strSaveAction = GmCommonClass.parseNull((String) hmParam.get("SAVEACTION"));
    String strSurgeonId = GmCommonClass.parseNull((String)hmParam.get("SURGEONID"));
    String strFlag = "";

    if (strSaveAction.equals("UpdateTxn")) {
      updateNPITransactionId(gmDBManager, hmParam);
    } else {
      gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_sav_npi_partyid_transaction", 9);
      gmDBManager.registerOutParameter(1, java.sql.Types.NVARCHAR);
      gmDBManager.setString(1, strNpiTranId);
      gmDBManager.setString(2, strTranId);
      gmDBManager.setString(3, strAccID);
      gmDBManager.setString(4, strNpiNum);
      gmDBManager.setString(5, strSurgeonName);
      gmDBManager.setString(6, strFlag);
      gmDBManager.setString(7, strUserId);
      gmDBManager.setString(8, strSubmitType);
      gmDBManager.setString(9, strSurgeonId);
      gmDBManager.execute();
      strnpitranid = GmCommonClass.parseNull(gmDBManager.getString(1));
    }
    gmDBManager.commit();
    return strnpitranid;
  }

}
