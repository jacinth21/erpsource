// Source file:
// C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\beans\\GmAddressBean.java

package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * Bean class for fetching the address details of party
 * 
 * @author Brinal
 */
public class GmAddressBean extends GmBean {
  // Code to Initialize the Logger Class.
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmAddressBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmAddressBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * Fetches the address information for the party from db
   * 
   * @param strPartyId
   * @return RowSetDynaClass
   * @throws com.globus.common.beans.AppError
   * @roseuid 48F8A3D4009D
   */
  public RowSetDynaClass fetchAddressInfo(String strPartyId) throws AppError {
    RowSetDynaClass rsDynaResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_cm_address.gm_cm_fch_addressinfo", 2);
    // log.debug(" party id " +strPartyId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.execute();
    rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rsDynaResult;
  }

  /**
   * Fetches the address information for the party from db when user clicks on 'Edit'
   * 
   * @param strAddressId
   * @return java.lang.String
   * @throws com.globus.common.beans.AppError
   * @roseuid 48F8A43C01F8
   */
  public HashMap fetchEditAddressInfo(String strAddressId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_cm_address.gm_cm_fch_editaddressinfo", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strAddressId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return hmReturn;
  }

  /**
   * Method to save the address information for party
   * 
   * @param hmParam
   * @return java.lang.String
   * @throws com.globus.common.beans.AppError
   * @roseuid 4908D17D0013
   */
  public String saveAddressInfo(HashMap hmParam) throws AppError {
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmDealerInvoiceParamsBean gmDealerInvoiceParamsBean =
        new GmDealerInvoiceParamsBean(getGmDataStoreVO());

    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strAddressType = GmCommonClass.parseNull((String) hmParam.get("ADDRESSTYPE"));
    String strBillAdd1 = GmCommonClass.parseNull((String) hmParam.get("BILLADD1"));
    String strBillAdd2 = GmCommonClass.parseNull((String) hmParam.get("BILLADD2"));
    String strBillCity = GmCommonClass.parseNull((String) hmParam.get("BILLCITY"));
    String strZipCode = GmCommonClass.parseNull((String) hmParam.get("ZIPCODE"));
    String strState = GmCommonClass.parseNull((String) hmParam.get("STATE"));
    String strCountry = GmCommonClass.parseNull((String) hmParam.get("COUNTRY"));
    String strPreference = GmCommonClass.parseNull((String) hmParam.get("PREFERENCE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strActiveFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("ACTIVEFLAG"));
    String strPrimaryFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("PRIMARYFLAG"));
    String strAddressId = GmCommonClass.parseNull((String) hmParam.get("HADDRESSID"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strBillName = GmCommonClass.parseNull((String) hmParam.get("BILLNAME"));
    String strAddressIdReturned = "";
    // PMT-4359 -- Getting the Preferred value from jsp and need to save in t106_address,
    // c901_carrier as 14th Parameter
    String strPrefCarrier = GmCommonClass.parseNull((String) hmParam.get("CARRIER"));
    String strPartyType = GmCommonClass.parseNull((String) hmParam.get("PARTYTYPE"));
    gmDBManager.setPrepareString("gm_pkg_cm_address.gm_cm_sav_addressinfo", 15);
    gmDBManager.registerOutParameter(2, java.sql.Types.CHAR);

    log.debug("flags " + strActiveFlag + " " + strPrimaryFlag);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.setString(2, strAddressId);
    gmDBManager.setString(3, strAddressType);
    gmDBManager.setString(4, strBillAdd1);
    gmDBManager.setString(5, strBillAdd2);
    gmDBManager.setString(6, strBillCity);
    gmDBManager.setString(7, strState);
    gmDBManager.setString(8, strCountry);
    gmDBManager.setString(9, strZipCode);
    gmDBManager.setString(10, strPreference);
    gmDBManager.setString(11, strUserId);
    gmDBManager.setString(12, strActiveFlag);
    gmDBManager.setString(13, strPrimaryFlag);
    gmDBManager.setString(14, strPrefCarrier);
    gmDBManager.setString(15, strBillName);
    gmDBManager.execute();

    strAddressIdReturned = gmDBManager.getString(2);
    if (!strLog.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strPartyId, strLog, strUserId, "1240");
    }
    if (strPartyType.equals("26230725")) {// Deaeler party type =26230725 ,need to sync Address of
                                          // all accounts that are associated to Dealer
      gmDealerInvoiceParamsBean.syncDealerAccountDetails(strPartyId, "", strUserId, gmDBManager);
    }
    gmDBManager.commit();

    return strAddressIdReturned;
  }

  /* PMT-288 new parameter strInActiveFl added to filter inactive address of rep */

  public ArrayList fetchPartyAddress(String strRepId, String strInActiveFl) throws AppError {
    RowSetDynaClass rsDynaResult = null;
    ArrayList alist = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    strInActiveFl = GmCommonClass.parseNull(strInActiveFl);
    if (strInActiveFl.equals("")) {
      strInActiveFl = "Y";
    }
    gmDBManager.setPrepareString("gm_pkg_cm_address.gm_fch_repaddress", 3);
    // log.debug(" party id " +strPartyId);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRepId);
    gmDBManager.setString(2, strInActiveFl);
    gmDBManager.execute();
    alist = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alist;

  }

  public ArrayList fetchEmpPartyAddress(String strEmpID, String strInActiveFl) throws AppError {
    RowSetDynaClass rsDynaResult = null;
    ArrayList alist = new ArrayList();
    strInActiveFl = GmCommonClass.parseNull(strInActiveFl);
    if (strInActiveFl.equals("")) {
      strInActiveFl = "Y";
    }
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_address.gm_fch_empaddress", 3);
    // log.debug(" party id " +strPartyId);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strEmpID);
    gmDBManager.setString(2, strInActiveFl);
    gmDBManager.execute();
    alist = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alist;

  }

  /**
   * @param strAddrId
   * @return method to validate inActive rep Address
   */
  public String validateRepAddress(String strAddrId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strMessage = "";
    // The code has been changed due to connection leak.
    strAddrId = GmCommonClass.parseZero(strAddrId);
    int iAddId = Integer.parseInt(strAddrId);
    gmDBManager.setPrepareString("gm_pkg_cm_address.gm_validate_rep_addr", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setInt(1, iAddId);
    gmDBManager.execute();
    strMessage = GmCommonClass.parseNull((String) gmDBManager.getObject(2));
    gmDBManager.close();
    return strMessage;
  }

  /**
   * @param strShipTo , strRepAccID , strAddreID
   * @return String
   * @method to fetch the Default preferred Carrier values for the Sales Rep's And Accounts --
   *         PMT-4359
   */
  public String fetchPrefCarrier(String strShipTo, String strRepAccID, String strAddreID,
      String strAcctID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPrefCarrier = "";
    gmDBManager.setPrepareString("gm_pkg_cm_address.gm_fch_preferred_carrier", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strRepAccID);
    gmDBManager.setString(2, strShipTo);
    gmDBManager.setString(3, strAddreID);
    gmDBManager.setString(4, strAcctID);
    gmDBManager.execute();
    strPrefCarrier = GmCommonClass.parseNull((String) gmDBManager.getObject(5));
    gmDBManager.close();
    return strPrefCarrier;
  }

  /**
   * fetchAddressDetail
   * 
   * @return ArrayList
   * @method This method will returns Address values while calling web service.The Adress Values
   *         will be retrieved based on Active Address id.
   */
  public ArrayList fetchAddressDetail(HashMap hmParams) throws AppError {
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess end
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_cm_address.gm_fch_address_info", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  /**
   * sendAddressExceptionEmail
   * 
   * @throws AppError,Exception
   * @method If any Exception Thrown,Sales Rep Will be notified by an E-mail.The below method is
   *         called to send an Exception email
   */
  public void sendAddressExceptionEmail(HashMap hmParams) throws AppError, Exception {
    String strRepEmail = "";
    String strMessage = "";
    String strMessageHeader = "";
    String strMessageBody = "";
    String strMessageFooter = "";
    strRepEmail = GmCommonClass.parseNull((String) hmParams.get("REPEMAIL"));
    String TEMPLATE_NAME = "GmAddNewAddressExcp";

    GmEmailProperties emailProps = new GmEmailProperties();


    emailProps.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.FROM));
    emailProps.setRecipients(strRepEmail);
    emailProps.setSubject(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.SUBJECT));
    emailProps.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.MIME_TYPE));
    strMessage = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE);
    strMessageHeader =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_HEADER);
    strMessageBody =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_BODY);
    strMessageBody =
        GmCommonClass
            .replaceAll(strMessageBody, "#<ERRORMESS>", (String) hmParams.get("EXCEPTION"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<REPNAME>", (String) hmParams.get("USERNAME"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<ADDRESSTYPE>",
            (String) hmParams.get("ADDRESSTYPENAME"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<BILLADD1>", (String) hmParams.get("BILLADD1"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<BILLADD2>", (String) hmParams.get("BILLADD2"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<BILLCITY>", (String) hmParams.get("BILLCITY"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<STATE>", (String) hmParams.get("STATENAME"));
    strMessageBody =
        GmCommonClass
            .replaceAll(strMessageBody, "#<COUNTRY>", (String) hmParams.get("COUNTRYNAME"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<ZIPCODE>", (String) hmParams.get("ZIPCODE"));
    strMessageFooter =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_FOOTER);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageHeader>", strMessageHeader);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageBody>", strMessageBody);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageFooter>", strMessageFooter);


    emailProps.setMessage(strMessage);
    GmCommonClass.sendMail(emailProps);
  }

  /**
   * sendAddressExceptionEmail
   * 
   * @throws AppError,Exception
   * @method If the data's are successfully saved into server, Customer Service,Sales Admin and
   *         Sales Rep Will be notified by an E-mail.The below method is called to send an email.
   */
  public void sendNewAddressEmail(HashMap hmParams) throws AppError, Exception {
    log.debug("Values are saved into save address email");
    String strMessage = "";
    String strMessageHeader = "";
    String strMessageBody = "";
    String strMessageFooter = "";
    String strRepEmail = "";
    String strSubject = "";
    strRepEmail = GmCommonClass.parseNull((String) hmParams.get("REPEMAIL"));
    String TEMPLATE_NAME = "GmAddNewAddress";
    GmEmailProperties emailProps = new GmEmailProperties();

    emailProps.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.FROM));
    emailProps.setRecipients(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.TO));
    emailProps.setCc(strRepEmail);
    emailProps.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."
        + GmEmailProperties.MIME_TYPE));

    strSubject = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT);
    strSubject =
        GmCommonClass.replaceAll(strSubject, "#<rep_name>", (String) hmParams.get("USERNAME"));
    emailProps.setSubject(strSubject);

    strMessage = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE);
    strMessageHeader =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_HEADER);
    strMessageBody =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_BODY);
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<REPNAME>", (String) hmParams.get("USERNAME"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<ADDRESSTYPE>",
            (String) hmParams.get("ADDRESSTYPENAME"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<BILLADD1>", (String) hmParams.get("BILLADD1"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<BILLADD2>", (String) hmParams.get("BILLADD2"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<BILLCITY>", (String) hmParams.get("BILLCITY"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<STATE>", (String) hmParams.get("STATENAME"));
    strMessageBody =
        GmCommonClass
            .replaceAll(strMessageBody, "#<COUNTRY>", (String) hmParams.get("COUNTRYNAME"));
    strMessageBody =
        GmCommonClass.replaceAll(strMessageBody, "#<ZIPCODE>", (String) hmParams.get("ZIPCODE"));
    strMessageFooter =
        GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MESSAGE_FOOTER);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageHeader>", strMessageHeader);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageBody>", strMessageBody);
    strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageFooter>", strMessageFooter);
    emailProps.setMessage(strMessage);
    GmCommonClass.sendMail(emailProps);
  }

  /**
   * fetchAddressDetail
   * 
   * @return ArrayList
   * @method This method will returns Address values while calling web service.The Adress Values
   *         will be retrieved based on Active Address id.
   */
  public ArrayList fetchShipAddress(String strPartyId, String strInActiveFl) throws AppError {
    RowSetDynaClass rsDynaResult = null;
    ArrayList alist = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    strInActiveFl = GmCommonClass.parseNull(strInActiveFl);
    if (strInActiveFl.equals("")) {
      strInActiveFl = "Y";
    }
    gmDBManager.setPrepareString("gm_pkg_cm_address.gm_fch_ship_address", 3);
    // log.debug(" party id " +strPartyId);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.setString(2, strInActiveFl);
    gmDBManager.execute();
    alist = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alist;

  }

  /**
   * fetchThirdPatyAccount
   * 
   * @return HashMap
   * @param String strAccountId
   * @method This method will returns Third Party Account Name and its associated Freight Amount.
   */
  public HashMap fetchThirdPatyAccount(String strAccountId) throws AppError {
    String strThirdPartyName = "";
    String strFreightAmount = "";
    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT GET_ACCOUNT_ATTRB_VALUE('");
    sbQuery.append(strAccountId);
    sbQuery.append("','91980')");
    sbQuery.append("THIRDPARTY , GET_ACCOUNT_ATTRB_VALUE('");
    sbQuery.append(strAccountId);
    sbQuery.append("', '91981') FREIGHTAMT  FROM DUAL ");
    hmResult = gmDBManager.querySingleRecord((sbQuery.toString()));
    strThirdPartyName = GmCommonClass.parseNull((String) hmResult.get("THIRDPARTY"));
    strFreightAmount = GmCommonClass.parseNull((String) hmResult.get("FREIGHTAMT"));
    hmReturn.put("THIRDPARTY", strThirdPartyName);
    hmReturn.put("FREIGHTAMT", strFreightAmount);
    return hmReturn;
  }
}
