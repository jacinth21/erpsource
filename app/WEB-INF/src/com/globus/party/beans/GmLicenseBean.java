//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\beans\\GmLicenseBean.java

package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

/**
 * Bean class for fetching and saving the license details of party
 * 
 * @author sthadeshwar
 */
public class GmLicenseBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	
	/**
	 * Fetches the license details for the party from db
	 * 
	 * @param strPartyId
	 * @return RowSetDynaClass
	 * @throws com.globus.common.beans.AppError
	 * @roseuid 48F8F41E02BF
	 */
	public RowSetDynaClass fetchLicenseInfo(String strPartyId) throws AppError {
		RowSetDynaClass rsDynaResult = null;
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cm_license.gm_fch_licenseinfo", 2);

		gmDBManager.setInt(1, Integer.parseInt(strPartyId));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		
		 rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(2));
	
		gmDBManager.close();
		return rsDynaResult;
	}

	/**
	 * Fetches the license details for the party from db when user clicks on
	 * 'Edit'
	 * 
	 * @param strLicenseId
	 * @return java.util.HashMap
	 * @throws com.globus.common.beans.AppError
	 * @roseuid 48F8FA8B00B5
	 */
	public HashMap fetchEditLicenseInfo(String strLicenseId) throws AppError {
	
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cm_license.gm_fch_edit_licenseinfo", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strLicenseId);
		gmDBManager.execute();
		HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return hmReturn;
	}

	/**
	 * Save the license details for the party in db
	 * 
	 * @param hmParam
	 * @return java.lang.String
	 * @throws com.globus.common.beans.AppError
	 * @roseuid 48F8FA9B026B
	 */
	public String saveLicenseInfor(HashMap hmParams) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		String strLicenseIdFromDB = "";
	
		String strLicenseId = GmCommonClass.parseNull((String) hmParams.get("LICENSEID"));
		String strTitle = GmCommonClass.parseNull((String) hmParams.get("TITLE"));
		String strMonth = GmCommonClass.parseNull((String) hmParams.get("ISSUEMONTH"));
		String strYear = GmCommonClass.parseNull((String) hmParams.get("ISSUEYEAR"));
		String strDetail = GmCommonClass.parseNull((String) hmParams.get("DESCRIPTION"));
		String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
		String strUser = GmCommonClass.parseNull((String) hmParams.get("USERID"));	
		String strCountry = GmCommonClass.parseNull((String) hmParams.get("COUNTRY"));
		String strState = GmCommonClass.parseNull((String) hmParams.get("STATE"));
		gmDBManager.setPrepareString("gm_pkg_cm_license.gm_sav_license_info", 9);
		 gmDBManager.registerOutParameter(1,java.sql.Types.CHAR);

		gmDBManager.setString(1, strLicenseId);
		gmDBManager.setString(2, strTitle);
		gmDBManager.setString(3, strMonth);
		gmDBManager.setString(4, strYear);
		gmDBManager.setString(5, strDetail);
		gmDBManager.setString(6, strPartyId);
		gmDBManager.setString(7, strUser);	
		gmDBManager.setString(8, strCountry);
		gmDBManager.setString(9, strState);

		gmDBManager.execute();
		  
		strLicenseIdFromDB = gmDBManager.getString(1);
		gmDBManager.commit();
	
		return strLicenseIdFromDB;
	}
}
