//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\beans\\GmPartyInfoBean.java

package com.globus.party.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;

/**
 * Bean class for fetching the basic details of party
 * @author sthadeshwar
 */
public class GmPartyInfoBean extends GmBean {

   
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
   /**
    * @roseuid 49138FCB0273
    */
   public GmPartyInfoBean() 
   {
	   super(GmCommonClass.getDefaultGmDataStoreVO());
    
   }

     /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
    public GmPartyInfoBean(GmDataStoreVO gmDataStore) {
	 super(gmDataStore);
    }

   
   /**
    * Fetches the basic information for the party from db
    * @param strPartyId
    * @return java.util.HashMap
    * @throws com.globus.common.beans.AppError
    * @roseuid 48F65CCD02C3
    */
   public HashMap fetchPartyInfo(String strPartyId) throws AppError 
   {
	   HashMap  hmReturn = new HashMap();
       GmDBManager gmDBManager = new GmDBManager ();
      
       gmDBManager.setPrepareString("gm_pkg_cm_party_info.gm_fch_partyinfo", 2);
       log.debug("Party id in fetchPartyInfo() = " + strPartyId);
       gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
       gmDBManager.setString(1, strPartyId);
       gmDBManager.execute();
       hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
       gmDBManager.close();

       return hmReturn;
   }
   
   /**
    * Method to fetch the information for Basic Info section on summary page
    * @param strPartyId
    * @return java.util.HashMap
    * @throws com.globus.common.beans.AppError
    * @roseuid 49120A260191
    */
   public HashMap fetchPartyBasicInfo(String strPartyId) throws AppError 
   {
	   ArrayList  alReturn = new ArrayList();
	   HashMap hmReturn = new HashMap();
	   GmDBManager gmDBManager = new GmDBManager ();       
	   gmDBManager.setPrepareString("gm_pkg_cm_party_info.gm_fch_party_basicinfo", 3);        
	   gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	   gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	   gmDBManager.setString(1, strPartyId);
	   gmDBManager.execute();            
	   hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
	   alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
	   hmReturn.put("CONTACT", alReturn);
	   
	   gmDBManager.close();

	   return hmReturn;
   }
   
   /**
    * Method to fetch the information for About Me section on summary page
    * @param strPartyId
    * @return java.util.HashMap
    * @throws com.globus.common.beans.AppError
    * @roseuid 49120A4C022D
    */
   public HashMap fetchPartyAboutMeSummary(String strPartyId) throws AppError 
   {
	   HashMap hmReturn = new HashMap();
	   GmDBManager gmDBManager = new GmDBManager();       
	   
	   gmDBManager.setPrepareString("gm_pkg_cm_party_info.gm_fch_party_aboutmeinfo", 2);    
	   gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	   
	   gmDBManager.setString(1, strPartyId);
	   gmDBManager.execute();            
	   
	   hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
	   gmDBManager.close();

	   return hmReturn;
   }
   
   /**
    * Method to fetch the party list based on the search string for party attribute
    * @param strCodeId
    * @param strSearchString
    * @return RowSetDynaClass
    * @throws com.globus.common.beans.AppError
    * @roseuid 49122DA4000D
    */
   public RowSetDynaClass fetchPartiesForSearchKey(String strCodeId, String strSearchString) throws AppError 
   {
    return null;
   }
   
   /**
    * Method to fetch the tabs to be displayed on the setup screen for a specific party type
    * @param strPartyType
    * @return ArrayList
    * @throws com.globus.common.beans.AppError
    * @roseuid 49122DA4000D
    */
   public ArrayList fetchSetupPageDisplayTabs(String strPartyType, String strLevel) throws AppError 
   {
	   ArrayList  alReturn = new ArrayList();
       GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());
      
       gmDBManager.setPrepareString("gm_pkg_cm_party_info.gm_fch_setup_displaytabs", 3);
       gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
       gmDBManager.setString(1, strPartyType);
       gmDBManager.setString(2, strLevel);
       gmDBManager.execute();
       alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
       gmDBManager.close();

       return alReturn;
   }
   
   /**
    * Method used for fetching the parties for different search criteria
    * @param strSearchString
    * @return ArrayList
    * @throws com.globus.common.beans.AppError
    */
   public ArrayList fetchParty(String strSearchString, String strPartyType) throws AppError {
	   log.debug("strSearchString: "+ strSearchString);

	   ArrayList  alReturn = new ArrayList();
	   GmDBManager gmDBManager = new GmDBManager ();       
	   gmDBManager.setPrepareString("gm_pkg_cm_party_info.gm_search_party", 4);        
	   gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
	   gmDBManager.setString(1, strSearchString);
	   gmDBManager.setString(2, ""); 	// not used now, will contain the id and search type ( '=' or 'LIKE' or 'IN' etc.,)
	   gmDBManager.setString(3, strPartyType);
	   gmDBManager.execute();            
	   alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
	   gmDBManager.close();

	   log.debug("alReturn: " + alReturn);
	   return alReturn;
   }
   
   /**
    * Method to fetch the list of Active parties based on party type. Used for party drop-down
    * @param strType
    * @return ArrayList
    * @throws com.globus.common.beans.AppError
    */
   public ArrayList fetchPartyNameList(String strType) throws AppError {
	   
	   ArrayList  alReturn = new ArrayList();
	   GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());       
	   gmDBManager.setPrepareString("gm_pkg_cm_party_info.gm_fch_partylist", 3);        
	   gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
	   gmDBManager.setString(2, "");
	   gmDBManager.setString(1, strType);
	   gmDBManager.execute();            
	   alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
	   gmDBManager.close();

	   //log.debug("alReturn: " + alReturn);
	   return alReturn;
	}
   
   /**
    * Method is used for getting Group values without Active flag Condition from table
    * @param strType
    * @param strMasterGrpValue
    * @return ArrayList
    * @throws com.globus.common.beans.AppError
    */
   public ArrayList fetchPartyNameList(String strType,String strMasterGrpValue) throws AppError {

  	   ArrayList  alReturn = new ArrayList();
  	   GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());       
  	   gmDBManager.setPrepareString("gm_pkg_cm_party_info.gm_fch_partylist", 3);        
  	   gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
  	   gmDBManager.setString(2, strMasterGrpValue);
  	   gmDBManager.setString(1, strType);
  	   gmDBManager.execute();            
  	   alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
  	   gmDBManager.close();
  	   return alReturn;
  	}
   
   public ArrayList fetchPartySearchScreenFilter(String searchGroup) throws AppError {

	   ArrayList  alReturn = new ArrayList();
	   GmDBManager gmDBManager = new GmDBManager ();       
	   gmDBManager.setPrepareString("gm_pkg_cm_party_info.gm_fch_search_filter", 2);        
	   gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
	   gmDBManager.setString(1, searchGroup);
	   gmDBManager.execute();            
	   alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
	   gmDBManager.close();

	   return alReturn;
   }
   	

   	public ArrayList fetchPartySearchScreenFilterTabs(String strPartyType) throws AppError {

   		ArrayList  alReturn = new ArrayList();
   		GmDBManager gmDBManager = new GmDBManager ();       
   		
   		gmDBManager.setPrepareString("gm_pkg_cm_party_info.gm_fch_search_filter_tabs", 2);        
   		gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
   		log.debug("strPartyType: " + strPartyType);
   		gmDBManager.setString(1, strPartyType);
   		gmDBManager.execute();            
   		
   		alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
   		gmDBManager.close();
   		return alReturn;
   	}
}
