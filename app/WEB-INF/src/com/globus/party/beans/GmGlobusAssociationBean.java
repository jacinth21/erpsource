//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\beans\\GmGlobusAssociationBean.java

package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * Bean class for fetching and saving the globus association details of party
 * @author sthadeshwar
 */
public class GmGlobusAssociationBean 
{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
   /**
    * Fetches the globus association information for the party.
    * @param strPartyId
    * @return RowSetDynaClass
    * @roseuid 48FCE4EF0145
    */
   public RowSetDynaClass fetchGlobusAssociationInfo(String strPartyId) throws AppError
   {
	   RowSetDynaClass rsDynaResult = null;
       GmDBManager gmDBManager = new GmDBManager ();
       
       gmDBManager.setPrepareString("gm_pkg_cm_globusinfo.gm_fch_globusinfo", 2);
       gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
       gmDBManager.setString(1, strPartyId);
       gmDBManager.execute();
       rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(2));
       gmDBManager.close();

       return rsDynaResult;
   }
   
   /**
    * Fetches the globus association information for the party when user clicks on 
    * 'Edit'
    * @param strAssociationId
    * @return java.util.HashMap
    * @roseuid 48FCE502031A
    */
   public HashMap fetchEditGlobusAssociationInfo(String strAssociationId) throws AppError
   {
       HashMap  hmReturn = new HashMap();
       GmDBManager gmDBManager = new GmDBManager ();
      
       gmDBManager.setPrepareString("gm_pkg_cm_globusinfo.gm_fch_editglobusinfo", 2);
       
       gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
       gmDBManager.setString(1, strAssociationId);
       gmDBManager.execute();
       hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
       gmDBManager.close();

       return hmReturn;
   }
   
   /**
    * Saves the globus association information for the party.
    * @param hmParam
    * @return java.lang.String
    * @roseuid 48FCE51202EC
    */
   public String saveGlobusAssociationInfo(HashMap hmParam) throws AppError
   {
		String strAssociationIdFromDb = "";
		GmDBManager gmDBManager = new GmDBManager();
		
		String strAssociationId = GmCommonClass.parseNull((String)hmParam.get("HGLOBUSASSOCIATIONID"));
		String strPartyId = GmCommonClass.parseNull((String)hmParam.get("PARTYID"));
		String strToPartyId = GmCommonClass.parseNull((String)hmParam.get("PERSONNELNAME"));
		String strPrimaryFlag = GmCommonClass.getCheckBoxValue((String)hmParam.get("PRIMARYFLAG"));
		String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));

		gmDBManager.setPrepareString("gm_pkg_cm_globusinfo.gm_sav_globusinfo", 6);
		gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
		
		gmDBManager.setString(1, strAssociationId);
		gmDBManager.setString(2, strPartyId);
		gmDBManager.setString(3, strToPartyId);
		gmDBManager.setString(4, strPrimaryFlag);
		gmDBManager.setString(5, strUserId);
		gmDBManager.execute();
		
		strAssociationIdFromDb = gmDBManager.getString(6);
		log.debug("strAssociationIdFromDb = " + strAssociationIdFromDb);
	
        gmDBManager.commit();

        return strAssociationIdFromDb;
   }
   
   /**
    * Method to fetch the primary globus association information
    * @param strPartyId
    * @return java.util.HashMap
    * @throws com.globus.common.beans.AppError
    * @roseuid 49120A7C00A6
    */
   public HashMap fetchGlobusAssociationSummaryInfo(String strPartyId) throws AppError {
       HashMap  hmReturn = new HashMap();
       GmDBManager gmDBManager = new GmDBManager();
      
       gmDBManager.setPrepareString("gm_pkg_cm_globusinfo.gm_fch_globusinfosummary", 2);
       
       gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
       gmDBManager.setString(1, strPartyId);
       gmDBManager.execute();
       hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
       gmDBManager.close();

       return hmReturn;
   }
}
