/**
 * FileName    : GmEducationBean.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Oct 10, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * @author sthadeshwar
 *
 */
public class GmEducationBean {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * Saves the education information of party
	 * @param hmParam - the values to save
	 * @return String - Education id
	 */
	public String savePartyEducationInfo(HashMap hmParam) throws AppError {
		
		String strEduIdFromDb = "";
		GmDBManager gmDBManager = new GmDBManager();
		
		String strEduId = (String)hmParam.get("HEDUCATIONID");
		String strPartyId = (String)hmParam.get("PARTYID");
		String strLevel = (String)hmParam.get("EDUCATIONLEVEL");
		String strInstituteName = (String)hmParam.get("INSTITUTENAME");
		String strClassYear = (String)hmParam.get("CLASSYEAR");
		String strCourse = (String)hmParam.get("COURSE");
		String strDesgn = (String)hmParam.get("DESIGNATION");
		String strUserId = (String)hmParam.get("USERID");

		gmDBManager.setPrepareString("gm_pkg_cm_education.gm_sav_educationinfo", 8);
		gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
		
		gmDBManager.setString(1, strEduId);
		gmDBManager.setString(2, strPartyId);
		gmDBManager.setString(3, strLevel);
		gmDBManager.setString(4, strInstituteName);
		gmDBManager.setString(5, strClassYear);
		gmDBManager.setString(6, strCourse);
		gmDBManager.setString(7, strDesgn);
		gmDBManager.setString(8, strUserId);
		gmDBManager.execute();
		
		strEduIdFromDb = gmDBManager.getString(1);
        gmDBManager.commit();

        return strEduIdFromDb;
	}

	/**
	 * Fetches the education information of party for display tag
	 * @param strPartyId
	 * @return RowSetDynaClass
	 */
	public RowSetDynaClass fetchEducationInfo(String strPartyId) throws AppError {
        RowSetDynaClass rsDynaResult = null;
        GmDBManager gmDBManager = new GmDBManager ();
        
        gmDBManager.setPrepareString("gm_pkg_cm_education.gm_fch_educationinfo", 2);
        log.debug("Party id = " + strPartyId);
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
        gmDBManager.setString(1, strPartyId);
        gmDBManager.execute();
        rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(2));
        gmDBManager.close();

        return rsDynaResult;
    }
	
	/**
	 * Fetches the education information of party for editing
	 * @param strEducationId
	 * @return HashMap
	 */
    public HashMap fetchEditEducationInfo(String strEducationId) throws AppError {
        HashMap  hmReturn = new HashMap();
        GmDBManager gmDBManager = new GmDBManager ();
       
        gmDBManager.setPrepareString("gm_pkg_cm_education.gm_fch_editeducationinfo", 2);
        
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
        gmDBManager.setString(1, strEducationId);
        gmDBManager.execute();
        hmReturn = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
        log.debug("****************************");
        log.debug("hmReturn = " + hmReturn);
        log.debug("****************************");
        gmDBManager.close();
 
        return hmReturn;
    }
}
