package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmPublicationBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.

	 /**
     * saveDemandSheet - This method will be save / update the publication details 
     * @param hmParam - parameters to be saved / updated
     * @exception AppError
     */
    public String savePublicationInfo (HashMap hmParam)throws AppError {
    	String strPublicationIdFromDb = "";
    	GmDBManager gmDBManager = new GmDBManager ();
    	String strPublicationID = GmCommonClass.parseZero((String)hmParam.get("HPUBLICATIONID"));
    	String strPartyId = GmCommonClass.parseNull((String)hmParam.get("PARTYID"));
    	String strType = GmCommonClass.parseNull((String)hmParam.get("TYPEID"));
    	String strTitle = GmCommonClass.parseNull((String)hmParam.get("TITLE"));
    	String strReference = GmCommonClass.parseNull((String)hmParam.get("REFERENCE"));
    	String strAbst = GmCommonClass.parseNull((String)hmParam.get("PABSTRACT"));
    	String strMonth = GmCommonClass.parseZero((String)hmParam.get("MONTHID"));
    	String strYear = GmCommonClass.parseNull((String)hmParam.get("PYEAR"));
    	String strAuthor = GmCommonClass.parseNull((String)hmParam.get("AUTHOR"));
    	String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
    	String strRefURL = GmCommonClass.parseNull((String)hmParam.get("REFURL"));
    	
    	if(!strRefURL.equals("") && strRefURL.indexOf("http://") == -1) {
    		strRefURL = "http://" + strRefURL;
    	}

    	gmDBManager.setPrepareString("gm_pkg_cm_publication.gm_sav_publicationinfo",11);
    	gmDBManager.registerOutParameter(1,java.sql.Types.CHAR);

    	gmDBManager.setString(1,strPublicationID);
    	gmDBManager.setString(2,strPartyId);
    	gmDBManager.setString(3,strType);
    	gmDBManager.setString(4,strTitle);
    	gmDBManager.setString(5,strReference);
    	gmDBManager.setString(6,strAbst);
    	gmDBManager.setString(7,strMonth);
    	gmDBManager.setString(8,strYear);
    	gmDBManager.setString(9,strAuthor);
    	gmDBManager.setString(10,strUserId);
    	gmDBManager.setString(11,strRefURL);
    	gmDBManager.execute();

    	strPublicationIdFromDb = GmCommonClass.parseNull(gmDBManager.getString(1));

    	gmDBManager.commit();
    	return strPublicationIdFromDb;

    }
    
    /**
     * fetchDemandSheetInfo - This method will be fetch publication details 
     * @param strPartyId 
     * @return RowSetDynaClass
     * @exception AppError
     */
    public RowSetDynaClass fetchPublicationInfo(String strPartyId) throws AppError
    { 
    		RowSetDynaClass rdResult = null; 
            GmDBManager gmDBManager = new GmDBManager (); 
            gmDBManager.setPrepareString("gm_pkg_cm_publication.gm_fch_publicationinfo",2);
            
            gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
                       
            gmDBManager.setString(1,strPartyId);
                       
            gmDBManager.execute(); 
            rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
            gmDBManager.close(); 
                    
            return rdResult; 
        
    }
    
    /**
     * fetchDemandSheetInfo - This method will be fetch publication details for editing
     * @param strPublicationID 
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchEditPublicationInfo(String strPublicationID) throws AppError
    { 
    	    HashMap hmReturn = new HashMap();                     
            GmDBManager gmDBManager = new GmDBManager ();
            
            gmDBManager.setPrepareString("gm_pkg_cm_publication.gm_fch_editpublicationinfo",2);            
            gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);                       
            gmDBManager.setString(1,strPublicationID);
                       
            gmDBManager.execute(); 
            
            hmReturn = GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2))); 
            gmDBManager.close();
            return hmReturn;            
        
    }
    
}
