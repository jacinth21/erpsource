//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\com\\globus\\party\\beans\\GmCareerBean.java

package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;

/**
 * Bean class for fetching and saving the career details of party
 * @author sthadeshwar
 */
public class GmCareerBean 
{
   
   /**
    * @roseuid 49109C31013D
    */
   public GmCareerBean() 
   {
    
   }
   
   /**
    * Fetches the career information for the party from db
    * @param strPartyId
    * @return RowSetDynaClass
    * @throws com.globus.common.beans.AppError
    * @roseuid 48F8F12900D5
    */
   public RowSetDynaClass fetchCareerInfo(String strPartyId) throws AppError 
   {
	   RowSetDynaClass rsCareerInfo;
 	   GmDBManager gmDBManager = new GmDBManager();
       gmDBManager.setPrepareString("gm_pkg_cm_career.gm_fch_careerinfo", 2);
       gmDBManager.setString(1,strPartyId);
       gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
       gmDBManager.execute();
       rsCareerInfo = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
       gmDBManager.close();      
       return rsCareerInfo;

   }
   
   /**
    * Fetches the career information for the party from db when user clicks on 'Edit'
    * @param hEducationId
    * @return java.util.HashMap
    * @throws com.globus.common.beans.AppError
    * @roseuid 48F8F1460342
    */
   public HashMap fetchEditCareerInfo(String hCareerId) throws AppError 
   {
	   HashMap hmCareerInfo = new HashMap();
 	   GmDBManager gmDBManager = new GmDBManager();
       gmDBManager.setPrepareString("gm_pkg_cm_career.gm_fch_editcareerinfo", 2);
       gmDBManager.setString(1,GmCommonClass.parseNull(hCareerId));
       gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
       gmDBManager.execute();
       hmCareerInfo = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
       gmDBManager.close();      
       return hmCareerInfo;
   }
   
   /**
    * Saves the address information for the party in db
    * @param hmParam
    * @return java.lang.String
    * @throws com.globus.common.beans.AppError
    * @roseuid 48F8F1580294
    */

   public void saveCareerInfo(HashMap hmParam)throws AppError{
	   // TODO Auto-generated method stub
	   GmDBManager gmDBManager = new GmDBManager();
	   String strPartyId = GmCommonClass.parseNull((String)hmParam.get("PARTYID"));
	   String strTitle = GmCommonClass.parseNull((String)hmParam.get("TITLE"));
	   String strCompanyName = GmCommonClass.parseNull( (String)hmParam.get("COMPANYNAME"));
	   String strCompanyDesc = GmCommonClass.parseNull( (String)hmParam.get("COMPANYDESC"));
	   String strStartMonth = GmCommonClass.parseNull((String)hmParam.get("STARTMONTH"));
	   String strStartYear = GmCommonClass.parseNull((String)hmParam.get("STARTYEAR"));
	   String strEndMonth = GmCommonClass.parseNull((String)hmParam.get("ENDMONTH"));
	   String strEndYear = GmCommonClass.parseNull((String)hmParam.get("ENDYEAR"));
	   String strStillInPosition = GmCommonClass.getCheckBoxValue((String)hmParam.get("STILLINPOSITION"));
	   String strGroupDivision = GmCommonClass.parseNull((String)hmParam.get("GROUPDIVISION"));
	   String strCountry = GmCommonClass.parseNull((String)hmParam.get("COUNTRY"));
	   String strState = GmCommonClass.parseNull((String)hmParam.get("STATE"));
	   String strCity = GmCommonClass.parseNull((String)hmParam.get("CITY"));
	   String strIndustry = GmCommonClass.parseNull( (String)hmParam.get("INDUSTRY"));
	   String strCareerId = GmCommonClass.parseNull((String) hmParam.get("HCAREERID"));
	   String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

	   gmDBManager.setPrepareString("gm_pkg_cm_career.gm_sav_careerinfo", 16);
	   gmDBManager.setString(1,strPartyId);
	   gmDBManager.setString(2,strTitle);
	   gmDBManager.setString(3,strCompanyName);
	   gmDBManager.setString(4,strCompanyDesc);
	   gmDBManager.setString(5,strStartMonth);
	   gmDBManager.setString(6,strStartYear);
	   gmDBManager.setString(7,strEndMonth);
	   gmDBManager.setString(8,strEndYear);
	   gmDBManager.setString(9,strStillInPosition);
	   System.out.println("strStillInPosition = " + strStillInPosition);
	   gmDBManager.setString(10,strGroupDivision);
	   gmDBManager.setString(11,strCountry);
	   gmDBManager.setString(12,strState);
	   gmDBManager.setString(13,strCity);
	   gmDBManager.setString(14,strIndustry);
	   gmDBManager.setString(15,strCareerId);
	   gmDBManager.setString(16,strUserId);

	   gmDBManager.execute();
	   gmDBManager.commit();

}
}
