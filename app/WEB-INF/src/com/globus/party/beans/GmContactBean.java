// Source file:
// C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\beans\\GmContactBean.java

package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * Bean class for fetching the contact details of party
 * 
 * @author sthadeshwar
 */
public class GmContactBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @roseuid 49138FCC015A
   */

  // this will be removed all place changed with Data Store VO constructor
  public GmContactBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmContactBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * Fetches the contact information for the party from db
   * 
   * @param strPartyId
   * @return RowSetDynaClass
   * @throws com.globus.common.beans.AppError
   * @roseuid 48F8F334027F
   */
  public RowSetDynaClass fetchContactInfo(String strPartyId) throws AppError {
    RowSetDynaClass rsDynaResult = null;
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_cm_contact.gm_fch_contactInfo", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.execute();
    rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rsDynaResult;
  }

  /**
   * Fetches the contact information for the party from db when user clicks on 'Edit'
   * 
   * @param hContactId
   * @return java.util.HashMap
   * @throws com.globus.common.beans.AppError
   * @roseuid 48F8F38000BA
   */
  public HashMap fetchEditContactInfo(String hContactId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager();

    gmDBManager.setPrepareString("gm_pkg_cm_contact.gm_fch_editcontactinfo", 2);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, hContactId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return hmReturn;
  }

  /**
   * Method to save the contact information for the party
   * 
   * @param hmParam
   * @return java.lang.String
   * @throws com.globus.common.beans.AppError
   * @roseuid 4908D3E3005D
   */
  public String saveContactInfo(HashMap hmParam) throws AppError {
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmDBManager gmDBManager = new GmDBManager();
    GmDealerInvoiceParamsBean gmDealerInvoiceParamsBean = new GmDealerInvoiceParamsBean(getGmDataStoreVO());
    String strContacIdFromDB = "";

    // String strPartyId = form.getPartyId(); assign the partyId to variable strPartyId. Similar
    // operation for other
    // variables
    String strContactId = GmCommonClass.parseNull((String) hmParam.get("HCONTACTID"));
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strContactMode = GmCommonClass.parseNull((String) hmParam.get("CONTACTMODE"));
    String strContactType = GmCommonClass.parseNull((String) hmParam.get("CONTACTTYPE"));
    String strContactValue = GmCommonClass.parseNull((String) hmParam.get("CONTACTVALUE"));
    String strPreference = GmCommonClass.parseNull((String) hmParam.get("PREFERENCE"));
    String strPrimaryFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("PRIMARYFLAG"));
    String strActiveFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("INACTIVEFLAG"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strPartyType = GmCommonClass.parseNull((String) hmParam.get("PARTYTYPE"));
    gmDBManager.setPrepareString("gm_pkg_cm_contact.gm_sav_contactinfo", 9);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);

    /*
     * register out parameter and set input parameters
     */

    gmDBManager.setString(1, strContactId);
    gmDBManager.setString(2, strPartyId);
    gmDBManager.setString(3, strContactMode);
    gmDBManager.setString(4, strContactType);
    gmDBManager.setString(5, strContactValue);
    gmDBManager.setString(6, strPreference);
    gmDBManager.setString(7, strPrimaryFlag);
    gmDBManager.setString(8, strActiveFlag);
    gmDBManager.setString(9, strUserId);
    gmDBManager.execute();

    strContacIdFromDB = gmDBManager.getString(1);

    if (!strLog.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strPartyId, strLog, strUserId, "1231"); // 1231 is for
                                                                                // contact
    }
    // call this method based on the party type to sync Dealer invoice parameter,Contact,Address
    // related values to Dealer Associated accounts values
    if (strPartyType.equals("26230725")) {
      gmDealerInvoiceParamsBean.syncDealerAccountDetails(strPartyId, "", strUserId, gmDBManager);
    }
    gmDBManager.commit();
    return strContacIdFromDB;
  }
}
