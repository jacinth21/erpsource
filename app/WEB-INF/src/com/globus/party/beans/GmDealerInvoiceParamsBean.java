package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDealerInvoiceParamsBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmDealerInvoiceParamsBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * Save Dealer Invoice parameter values
   * 
   * @param hmParam
   * @return Void
   * @throws com.globus.common.beans.AppError
   */
  public void saveDealerInvoiceDetails(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strInovoiceCustomerType =
        GmCommonClass.parseNull((String) hmParam.get("INVOICECUSTOMERTYPE"));
    strInovoiceCustomerType = strInovoiceCustomerType.equals("0") ? "" : strInovoiceCustomerType;
    String strElectronicFileFmt =
        GmCommonClass.parseNull((String) hmParam.get("ELECRONICFILEFORMAT"));
    String strCSVTemplateNm = GmCommonClass.parseNull((String) hmParam.get("CSVTEMPLATTENAME")); // parentid
    String strInvoiceLayout = GmCommonClass.parseNull((String) hmParam.get("INVOICELAYOUT"));
    String strPaymentTerms = GmCommonClass.parseNull((String) hmParam.get("PAYMENTTERMS"));
    String strCollectorId = GmCommonClass.parseNull((String) hmParam.get("COLLETORID"));
    String strInvoiceClosingDate =
        GmCommonClass.parseNull((String) hmParam.get("INVOICECLOSINGDATE"));
    String strReceipentsEmail = GmCommonClass.parseNull((String) hmParam.get("RECEIPIENTSEMAIL"));
    String strVendor = GmCommonClass.parseNull((String) hmParam.get("VENDOR"));
    String strCreditRating = GmCommonClass.parseNull((String) hmParam.get("CREDITRATING"));
    String strEmailFl = GmCommonClass.parseNull((String) hmParam.get("ELECTRONICEMAIL"));
    String strDealerId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strConsolidatedFlag =
        GmCommonClass.parseNull((String) hmParam.get("CONSOLIDATEDBILLING"));
    strEmailFl = (strEmailFl.equals("on")) ? "Y" : "";
    strConsolidatedFlag = (strConsolidatedFlag.equals("on")) ? "Y" : "";
    gmDBManager.setPrepareString("gm_pkg_sm_admin_dealer_txn.gm_sav_dealer_inv_details", 14);
    gmDBManager.setString(1, strDealerId);
    gmDBManager.setString(2, strInovoiceCustomerType);
    gmDBManager.setString(3, strEmailFl);
    gmDBManager.setString(4, strElectronicFileFmt);
    gmDBManager.setString(5, strReceipentsEmail);
    gmDBManager.setString(6, strCSVTemplateNm);
    gmDBManager.setString(7, strInvoiceLayout);
    gmDBManager.setString(8, strVendor);
    gmDBManager.setString(9, strPaymentTerms);
    gmDBManager.setString(10, strCreditRating);
    gmDBManager.setString(11, strCollectorId);
    gmDBManager.setString(12, strInvoiceClosingDate);
    gmDBManager.setString(13, strUserId);
    gmDBManager.setString(14, strConsolidatedFlag);
    gmDBManager.execute();
    if (!strLog.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strDealerId, strLog, strUserId, "26230747");
    }
    // call this method to sync Dealer invoice parameter,Contact,Address
    // related values to Dealer Associated accounts values
    syncDealerAccountDetails(strDealerId, "", strUserId, gmDBManager);
    gmDBManager.commit();



  }

  /**
   * This method is used to fetch Dealer invoice parameter values
   * 
   * @param strDealerId
   * @return HashMap
   * @throws com.globus.common.beans.AppError
   */
  public HashMap fetchDealerInvoiceDetails(String strDealerId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_admin_dealer_rpt.gm_fch_dealer_inv_details", 2);
    gmDBManager.setString(1, strDealerId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();

    HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;

  }

  /**
   * This method is used to fetch the values for Dealer Account Mapping Report Screen
   * 
   * @param hmParam
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */
  public ArrayList fetchDealerAccountReport(HashMap hmParam) throws AppError {

    ArrayList alResult = new ArrayList();

    String strdealerName = GmCommonClass.parseNull((String) hmParam.get("DEALERNAME"));

    String strStatusFl = GmCommonClass.parseZero((String) hmParam.get("STATUSFL"));


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_admin_dealer_rpt.gm_fch_dealer_Acct_details", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strdealerName);
    gmDBManager.setString(2, strStatusFl);

    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();
    return alResult;


  }

  /**
   * This method is used to fetch Dealer Associated Accounts.
   * 
   * @param strDealerId
   * @return ArrayList
   * @throws com.globus.common.beans.AppError
   */

  public ArrayList fetchDealerAccounts(String strDealerId) throws AppError {

    ArrayList alResult = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_sm_admin_dealer_rpt.gm_fch_dealer_Accounts", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDealerId);


    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alResult;
  }

  /**
   * This method is used to sync Dealer invoice parameter,Contact,Address related values to Dealer
   * Associated accounts values
   * 
   * @param strDealerId,strAccountId,strUserId,gmDBManager
   * @return void
   * @throws com.globus.common.beans.AppError
   */
  public void syncDealerAccountDetails(String strDealerId, String strAccountId, String strUserId,
      GmDBManager gmDBManager) throws AppError {

    gmDBManager.setPrepareString("gm_pkg_sm_admin_dealer_txn.gm_sync_dealer_info_to_account", 3);
    gmDBManager.setString(1, strDealerId);
    gmDBManager.setString(2, strAccountId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
  }

}
