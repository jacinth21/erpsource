//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\actions\\com\\globus\\party\\beans\\GmAffiliationBean.java

package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * Bean class for fetching and saving the Affiliation details of party
 * 
 * @author Angela
 */
public class GmAffiliationBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * Fetches the Affiliation detail information for the party from db
	 * 
	 * @param strPartyId
	 * @return ArrayList
	 * @throws com.globus.common.beans.AppError
	 *             get Multiple rows from db
	 */
	public RowSetDynaClass fetchAffiliationDetails(String strPartyId) throws AppError {
		RowSetDynaClass rsDynaResult = null;
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cm_affiliation.gm_fch_affiliationinfo", 2);

		gmDBManager.setString(1, strPartyId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();

		rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));

		gmDBManager.close();

		return rsDynaResult;
	}

	/**
	 * Fetches the Affiliation information for the party from db when user
	 * clicks on 'Edit'
	 * 
	 * @param strPartyAffId
	 * @return java.util.HashMap
	 * @throws com.globus.common.beans.AppError
	 *             get a single row from db
	 */
	public HashMap fetchEditAffiliationDetails(String strPartyAffId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cm_affiliation.gm_fch_editaffiliationinfo", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strPartyAffId);
		gmDBManager.execute();
		HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return hmReturn;
	}

	/**
	 * Saves the AffiliationDetails information for the party in db
	 * 
	 * @param hmParam
	 * @return java.lang.String
	 * @throws com.globus.common.beans.AppError
	 */
	public String saveAffiliationDetails(HashMap hmParams) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		String strAffIdFromDB = "";

		String strAffId = GmCommonClass.parseNull((String) hmParams.get("AFFID"));
		String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
		String strOrganization = GmCommonClass.parseNull((String) hmParams.get("ORGANIZATION"));
		String strAffType = GmCommonClass.parseNull((String) hmParams.get("AFFTYPE"));
		String strCommitteeName = GmCommonClass.parseNull((String) hmParams.get("COMMITTEENAME"));
		String strRole = GmCommonClass.parseNull((String) hmParams.get("ROLE"));
		String strStartYear = (String) hmParams.get("STARTYEAR");
		String strEndYear = (String) hmParams.get("ENDYEAR");
		String strStillInPosition = GmCommonClass.getCheckBoxValue((String) hmParams.get("STILLINPOSITION"));
		String strUser = GmCommonClass.parseNull((String) hmParams.get("USERID"));


		gmDBManager.setPrepareString("gm_pkg_cm_affiliation.gm_sav_affiliationinfo", 10);
		gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
		
		gmDBManager.setString(1, strAffId);
		gmDBManager.setString(2, strPartyId);
		gmDBManager.setString(3, strOrganization);
		gmDBManager.setString(4, strAffType);
		gmDBManager.setString(5, strCommitteeName);
		gmDBManager.setString(6, strRole);
		gmDBManager.setString(7, strStartYear);
		gmDBManager.setString(8, strEndYear);
		gmDBManager.setString(9, strStillInPosition);
		gmDBManager.setString(10, strUser);

		gmDBManager.execute();
		strAffIdFromDB = gmDBManager.getString(1);
		gmDBManager.commit();
		return strAffIdFromDB;
	}
}
