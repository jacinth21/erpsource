package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmAchievementBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	public RowSetDynaClass fetchAchievementInfo(String strPartyId) throws AppError {
		RowSetDynaClass rsDynaResult = null;
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_cm_achievement.gm_fch_achievementinfo", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strPartyId);
		gmDBManager.execute();
		rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();

		return rsDynaResult;
	}

	public HashMap editAchievementInfo(String strAchievementId) throws AppError {
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_cm_achievement.gm_fch_editachievementinfo", 2);

		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strAchievementId);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return hmReturn;
	}

	public void saveAchievementInfo(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		
		String strAchievementid = GmCommonClass.parseNull((String) hmParam.get("ACHIEVEMENTID"));
		String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
		String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		String strTitle = GmCommonClass.parseNull((String) hmParam.get("TITLE"));
		String strDetail = GmCommonClass.parseNull((String) hmParam.get("DESCRIPTION"));
		String strYear = GmCommonClass.parseNull((String) hmParam.get("YEAR"));
		String strMonth = GmCommonClass.parseNull((String) hmParam.get("MONTH"));
		String strCoInvestigators = GmCommonClass.parseNull((String) hmParam.get("COINVESTIGATORS"));
		String strUserid = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_cm_achievement.gm_sav_achievementinfo", 9);

		gmDBManager.setString(1, strAchievementid);
		gmDBManager.setString(2, strPartyId);
		gmDBManager.setString(3, strType);
		gmDBManager.setString(4, strTitle);
		gmDBManager.setString(5, strDetail);
		gmDBManager.setString(6, strYear);
		gmDBManager.setString(7, strMonth);
		gmDBManager.setString(8, strCoInvestigators);
		gmDBManager.setString(9, strUserid);
		
		gmDBManager.execute();
		gmDBManager.commit();
		gmDBManager.close();
	}
}
