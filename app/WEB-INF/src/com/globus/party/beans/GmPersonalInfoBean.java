//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\beans\\GmPersonalInfoBean.java

package com.globus.party.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * Bean class for fetching and saving the personal information of party
 * @author sthadeshwar
 */
public class GmPersonalInfoBean 
{
	 Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
   /**
    * @roseuid 49137B57000E
    */
   public GmPersonalInfoBean() 
   {
    
   }
   
   /**
    * Fetches the personal information for party
    * @param strPartyId
    * @return java.util.HashMap
    * @throws com.globus.common.beans.AppError
    * @roseuid 48FCDDD80170
    */
   public HashMap fetchPersonalInfo(String strPartyId) throws AppError 
   {
	   HashMap hmPersonalInfo;
 	   GmDBManager gmDBManager = new GmDBManager();
       gmDBManager.setPrepareString("gm_pkg_cm_personalinfo.gm_fch_personalinfo", 2);
       gmDBManager.setString(1,strPartyId);
       gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
       gmDBManager.execute();
       hmPersonalInfo = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
       gmDBManager.close();      
       return hmPersonalInfo;

   }
   
   /**
    * Saves the personal information of party
    * @param hmParam
    * @return java.lang.String
    * @throws com.globus.common.beans.AppError
    * @roseuid 48FCDF2E0002
    */
   public void savePersonalInfo(HashMap hmParam) throws AppError 
   {

	   GmDBManager gmDBManager = new GmDBManager();
	   
	   String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
	   String gender = GmCommonClass.parseNull((String)hmParam.get("GENDER"));
	   String strDateOfBirth = GmCommonClass.parseNull((String)hmParam.get("DATEOFBIRTH"));
	   String strHomeTown = GmCommonClass.parseNull((String)hmParam.get("HOMETOWN"));
	   String strPersonalWebsite = GmCommonClass.parseNull((String)hmParam.get("PERSONALWEBSITE"));
	   String relationshipStatus = GmCommonClass.parseNull((String)hmParam.get("RELATIONSHIPSTATUS"));
	   String strSpouseDetails = GmCommonClass.parseNull((String)hmParam.get("SPOUSEDETAILS"));
	   String children = GmCommonClass.parseNull(GmCommonClass.parseZero((String)hmParam.get("CHILDREN")));
	   String strChildrenDetails = GmCommonClass.parseNull((String)hmParam.get("CHILDRENDETAILS"));
	   String strHobbies = GmCommonClass.parseNull( (String)hmParam.get("HOBBIES"));
	   String strUserId = GmCommonClass.parseNull( (String)hmParam.get("USER"));
	   
	   log.debug("child"+children);
	   log.debug("strDateOfBirth = "+strDateOfBirth);
	   gmDBManager.setPrepareString("gm_pkg_cm_personalinfo.gm_sav_personalinfo", 11);
	   gmDBManager.setString(1,strPartyId);
	   gmDBManager.setString(2,gender);
	   gmDBManager.setString(3,strDateOfBirth);
	   gmDBManager.setString(4,strHomeTown);
	   gmDBManager.setString(5,strPersonalWebsite);
	   gmDBManager.setString(6,relationshipStatus);
	   gmDBManager.setString(7,strSpouseDetails);
	   gmDBManager.setString(8,children);
	   gmDBManager.setString(9,strChildrenDetails);
	   gmDBManager.setString(10,strHobbies);
	   gmDBManager.setString(11,strUserId);

	   gmDBManager.execute();
	   gmDBManager.commit();

	   gmDBManager.close(); 

   }
}
