package com.globus.party.displaytag;

import org.apache.commons.beanutils.DynaBean;

public class DTLicenseWrapper extends GmCommonSearchDecorator {

	public String getLICENSEID() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("LICENSEID"));
		return super.getIDLink("fnLicenseEditId", strValue);
	}

	public String getTITLE() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("TITLE"));
		return super.getSearchAction("TITLE", strValue);
	}

	public String getDATEISSUE() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("DATEISSUE"));
		return super.getSearchAction("DATEISSUE", strValue);
	}

	public String getDESCRIPTION() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("DESCRIPTION"));
		return super.getSearchAction("DESCRIPTION", strValue);
	}

	public String getCOUNTRYNAME() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("COUNTRYNAME"));
		return super.getSearchAction("COUNTRYNAME", strValue);
	}

	public String getSTATENAME() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("STATENAME"));
		return super.getSearchAction("STATENAME", strValue);
	}

}