/**
 * FileName    : DTContactInfoWrapper.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Nov 13, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.displaytag;

import org.apache.commons.beanutils.DynaBean;

/**
 * @author sthadeshwar
 *
 */
public class DTContactInfoWrapper extends GmCommonSearchDecorator {

	public String getID()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("ID"));
		return super.getIDLink("fnContactEditId", strValue);
	}
	
	public String getCTYPE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("CTYPE"));		
		return super.getSearchAction("CTYPE", strValue);	
	}
	
	public String getCMODE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("CMODE"));		
		return super.getSearchAction("CMODE", strValue);	
	}
	
	public String getCVALUE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("CVALUE"));		
		return super.getSearchAction("CVALUE", strValue);	
	}
	
	public String getPRIMARYFL()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PRIMARYFL"));		
		return super.getSearchAction("PRIMARYFL", strValue);	
	}
	
	public String getINACTIVEFL()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("INACTIVEFL"));		
		return super.getSearchAction("INACTIVEFL", strValue);	
	}
}
