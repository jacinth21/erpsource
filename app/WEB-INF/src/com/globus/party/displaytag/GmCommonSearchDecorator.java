package com.globus.party.displaytag;

import java.util.HashMap;

import javax.servlet.jsp.PageContext;

import org.apache.log4j.Logger;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.displaytag.beans.GmCommonDecorator;
import com.globus.party.forms.GmPartySetupForm;

public class GmCommonSearchDecorator extends GmCommonDecorator {

	protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	protected HashMap hmSearchMap = new HashMap();
	protected String strPartyType;
	
	public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		hmSearchMap = (HashMap)pageContext.getAttribute("SEARCHCODEMAP", PageContext.REQUEST_SCOPE);
		strPartyType = pageContext.getRequest().getParameter("partyType");
		hmSearchMap = hmSearchMap == null ? new HashMap() : hmSearchMap;
		log.debug("hmSearchMap: " + hmSearchMap);
	}

	protected String getSearchAction(String strPropertyName, String strSearchValue)	{	
		String strLookupId = (String)hmSearchMap.get(strPropertyName);
		log.debug("strLookupId = " + strLookupId + " for strPropertyName = " + strPropertyName + " and strSearchValue = " + strSearchValue);
		if(strLookupId != null && !strLookupId.equals("") && strSearchValue != null && !strSearchValue.equals("null")) {
			return "<a href =/gmSearchParty.do?partyType="+strPartyType+"&lookupId="+strLookupId+"&searchString="+strSearchValue.replaceAll(" ", "~")+">" + strSearchValue + "</a>";
		} else {
			return strSearchValue;
		}
	}

	protected String getIndividualSearchLinks(String strPropertyName, String strValue) {
		if(strValue != null && !strValue.equals("") && !strValue.equals("null")) {
			String[] links = strValue.split(";");
			String searchLinks = "";
			for(int i=0; i<links.length; i++) {
				searchLinks += getSearchAction(strPropertyName, links[i]) + " ; ";
			}
			searchLinks = searchLinks.substring(0, searchLinks.length() - 3);
			return searchLinks;
		} else {
			return strValue;
		}
	}


}
	 
	 