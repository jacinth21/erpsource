package com.globus.party.displaytag;

import org.apache.commons.beanutils.DynaBean;

public class DTEducationInfoWrapper extends GmCommonSearchDecorator {

	
	public String getID()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("ID"));
		return super.getIDLink("fnEducationEditId", strValue);
	}
	
	public String getINSTITUTENAME()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("INSTITUTENAME"));		
		return super.getSearchAction("INSTITUTENAME", strValue);		
	}
	
	public String getEDUCATIONLEVEL()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("EDUCATIONLEVEL"));		
		return super.getSearchAction("EDUCATIONLEVEL", strValue);	
	}
	
	public String getCLASSYEAR()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("CLASSYEAR"));		
		return super.getSearchAction("CLASSYEAR", strValue);	
	}
	
	public String getCOURSE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("COURSE"));		
		return super.getSearchAction("COURSE", strValue);	
	}
	
	public String getDESIGNATION()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("DESIGNATION"));		
		return super.getSearchAction("DESIGNATION", strValue);	
	}
}
