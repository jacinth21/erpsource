package com.globus.party.displaytag;

import org.apache.commons.beanutils.DynaBean;

public class DTSiteMappingInfoWrapper extends GmCommonSearchDecorator {

	public String getID()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PARTY_ID"));
		String strValue1 = String.valueOf(db.get("PARTY_NM"));
		String rValue = "<a href=\"javascript:fnSiteMappingEditId('"+strValue+"','"+strValue1+"');\"><img width='39' height='19' title='Click here to edit details' style='cursor:hand' src='/images/edit.jpg'/></a>";
		return rValue;	
	}
	
}
