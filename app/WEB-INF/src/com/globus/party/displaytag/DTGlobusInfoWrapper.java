/**
 * FileName    : DTGlobusInfoWrapper.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Nov 13, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.displaytag;

import org.apache.commons.beanutils.DynaBean;

/**
 * @author sthadeshwar
 *
 */
public class DTGlobusInfoWrapper extends GmCommonSearchDecorator {

	public String getID()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("ID"));
		return super.getIDLink("fnGlobusEditId", strValue);
	}
	
	public String getPERSONNELNAME()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PERSONNELNAME"));	
		return super.getSearchAction("PERSONNELNAME", strValue);	
	}
	
	public String getPERSONNELTYPE1()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PERSONNELTYPE1"));		
		return super.getSearchAction("PERSONNELTYPE1", strValue);	
	}
	
	public String getPRIMARYFLAG()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PRIMARYFLAG"));		
		return super.getSearchAction("PRIMARYFLAG", strValue);	
	}
}
