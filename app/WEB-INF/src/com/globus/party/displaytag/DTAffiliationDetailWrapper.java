package com.globus.party.displaytag;

import org.apache.commons.beanutils.DynaBean;

public class DTAffiliationDetailWrapper extends GmCommonSearchDecorator {

	public String getAFFID() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("AFFID"));
		return super.getIDLink("fnAffiliationEditId", strValue);
	}

	public String getAFFNAME() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("AFFNAME"));
		return super.getSearchAction("AFFNAME", strValue);
	}

	public String getCODENAME() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("CODENAME"));
		return super.getSearchAction("CODENAME", strValue);
	}

	public String getSTARTYEAR() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("STARTYEAR"));
		return super.getSearchAction("STARTYEAR", strValue);
	}

	public String getENDYEAR() {
		DynaBean db = (DynaBean) getCurrentRowObject();
		String strValue = String.valueOf(db.get("ENDYEAR"));
		return super.getSearchAction("ENDYEAR", strValue);
	}
}