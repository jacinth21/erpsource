package com.globus.party.displaytag;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTAchievementWrapper extends GmCommonSearchDecorator {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public String getID() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("ID"));
		return super.getIDLink("fnAchieveEditId", strValue);
	}
	
	public String getTYPE() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("TYPE"));		
		return super.getSearchAction("TYPE", strValue);		
	}
	
	public String getTITLE() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("TITLE"));		
		return super.getSearchAction("TITLE", strValue);		
	}
	
	public String getDETAIL() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("DETAIL"));		
		return super.getSearchAction("DETAIL", strValue);		
	}
	
	public String getDT() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("DT"));		
		return super.getSearchAction("DT", strValue);		
	}
	
	public String getCOINVESTIGATORS() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = GmCommonClass.parseNull(String.valueOf(db.get("COINVESTIGATORS")));
		return super.getIndividualSearchLinks("COINVESTIGATORS", strValue);		
	}
}