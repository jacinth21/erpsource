/**
 * FileName    : DTAddressInfoWrapper.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Nov 13, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.displaytag;

import org.apache.commons.beanutils.DynaBean;

/**
 * @author sthadeshwar
 *
 */
public class DTAddressInfoWrapper extends GmCommonSearchDecorator {

	public String getID()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("ID"));
		return super.getIDLink("fnAddressEditId", strValue);
	}
	
	public String getADDRESSTYPE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("ADDRESSTYPE"));		
		return super.getSearchAction("ADDRESSTYPE", strValue);	
	}
	
	public String getBILLADD1()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("BILLADD1"));		
		return super.getSearchAction("BILLADD1", strValue);	
	}
	
	public String getBILLADD2()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("BILLADD2"));		
		return super.getSearchAction("BILLADD2", strValue);	
	}
	
	public String getBILLCITY()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("BILLCITY"));		
		return super.getSearchAction("BILLCITY", strValue);	
	}
	
	public String getSTATE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("STATE"));		
		return super.getSearchAction("STATE", strValue);	
	}
	
	public String getCOUNTRY()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("COUNTRY"));		
		return super.getSearchAction("COUNTRY", strValue);	
	}
	
	public String getZIPCODE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("ZIPCODE"));		
		return super.getSearchAction("ZIPCODE", strValue);	
	}
	
	public String getPREFERENCE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PREFERENCE"));		
		return super.getSearchAction("PREFERENCE", strValue);	
	}
	
	public String getINACTIVEFL()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("INACTIVEFL"));		
		return super.getSearchAction("INACTIVEFL", strValue);	
	}
	// PMT-4359 -- New Column Preferred Carrier is added in Address Report
	public String getCARRIER()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("CARRIER"));		
		return super.getSearchAction("CARRIER", strValue);	
	}
	
	public String getPRIMARYFL()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PRIMARYFL"));
		String strID = String.valueOf(db.get("ID"));
		if(strValue.equals("Y")){
			return "<input type=\"hidden\" value=\"" +strID+ "\"  id=\"primaryAddID\" name=\"primaryAddID\" >"+super.getSearchAction("PRIMARYFL", strValue);
		}
		return super.getSearchAction("PRIMARYFL", strValue);	
	}
}
