/**
 * FileName    : GmCareerSetupWrapper.java 
 * Description :
 * Author      : tchandure
 * Date        : Nov 6, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.displaytag;

import java.text.DecimalFormat;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmLogger;

/**
 * @author tchandure
 *
 */
public class DTCareerInfoWrapper extends GmCommonSearchDecorator {

	public String getCAREERID() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("CAREERID"));
		return super.getIDLink("fnCareerEditId", strValue);
	}

	public String getTITLE() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("TITLE"));		
		return super.getSearchAction("TITLE", strValue);	
	}
	
	public String getCOMPANYNAME() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("COMPANYNAME"));		
		return super.getSearchAction("COMPANYNAME", strValue);	
	}
	
	public String getSTARTDATE() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("STARTDATE"));		
		return super.getSearchAction("STARTDATE", strValue);	
	}
	
	public String getENDDATE() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("ENDDATE"));		
		return super.getSearchAction("ENDDATE", strValue);	
	}
	
	public String getGROUPDIVISION() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("GROUPDIVISION"));		
		return super.getSearchAction("GROUPDIVISION", strValue);	
	}
	
	public String getCOUNTRY() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("COUNTRY"));		
		return super.getSearchAction("COUNTRY", strValue);	
	}
	
	public String getSTATE() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("STATE"));		
		return super.getSearchAction("STATE", strValue);	
	}
	
	public String getCITY() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("CITY"));		
		return super.getSearchAction("CITY", strValue);	
	}
	
	public String getINDUSTRY() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("INDUSTRY"));		
		return super.getSearchAction("INDUSTRY", strValue);	
	}
	
	public String getCOMPANYDESC() {
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("COMPANYDESC"));		
		return super.getSearchAction("COMPANYDESC", strValue);	
	}


}
