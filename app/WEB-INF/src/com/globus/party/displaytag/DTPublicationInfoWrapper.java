/**
 * FileName    : DTPublicationInfoWrapper.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Nov 13, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.displaytag;

import org.apache.commons.beanutils.DynaBean;

/**
 * @author sthadeshwar
 *
 */
public class DTPublicationInfoWrapper extends GmCommonSearchDecorator {

	public String getPUBID()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PUBID"));
		return super.getIDLink("fnPublicationEditId", strValue);
	}
	
	public String getPUBTYPE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PUBTYPE"));		
		return super.getSearchAction("PUBTYPE", strValue);	
	}
	
	public String getTITLE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("TITLE"));		
		return super.getSearchAction("TITLE", strValue);	
	}
	
	public String getPUBDATE()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PUBDATE"));		
		return super.getSearchAction("PUBDATE", strValue);	
	}
	
	public String getPAUTHOR()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PAUTHOR"));		
		return super.getIndividualSearchLinks("PAUTHOR", strValue);
	}
	
	public String getPUBREF()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("PUBREF"));		
		return super.getSearchAction("PUBREF", strValue);	
	}
}
