/**
 * FileName    : GmCareerSetupWrapper.java 
 * Description :
 * Author      : tchandure
 * Date        : Nov 6, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.displaytag;

import java.text.DecimalFormat;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmLogger;

/**
 * @author tchandure
 *
 */
public class DTCareerSetupWrapper extends TableDecorator{

	/**
	 * 
	 */
	GmLogger log = GmLogger.getInstance();

	private DynaBean db;
    private String strCid;

	 public String getCAREERID()
	    {
	    	db = 	(DynaBean) this.getCurrentRowObject();
	    	strCid = db.get("CAREERID").toString();
	         return "<a href=javascript:fnEdit('" + 
	         strCid + "')>  <img src='/images/edit.jpg' />  </a>" ;
	      
	    }
	 
	 
}
