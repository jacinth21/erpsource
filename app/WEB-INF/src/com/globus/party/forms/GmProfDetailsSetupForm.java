//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\forms\\GmProfDetailsSetupForm.java

package com.globus.party.forms;

import java.util.ArrayList;


/**
 * Form bean class for the about me setup for party
 * @author sthadeshwar
 */
public class GmProfDetailsSetupForm extends GmPartySetupForm 
{
  
   private ArrayList fieldValue;
   
   private String speciality ="";
   private String professionalInterest = "";
   /**
    * @roseuid 4913914100DA
    */
   public GmProfDetailsSetupForm() 
   {
    
   }






public ArrayList getFieldValue() {
	return fieldValue;
}

public void setFieldValue(ArrayList fieldValue) {
	this.fieldValue = fieldValue;
}

public String getProfessionalInterest() {
	return professionalInterest;
}

public void setProfessionalInterest(String professionalInterest) {
	this.professionalInterest = professionalInterest;
}



public String getSpeciality() {
	return speciality;
}


public void setSpeciality(String speciality) {
	this.speciality = speciality;
}

}
