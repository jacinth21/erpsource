package com.globus.party.forms;

import java.util.ArrayList;
import java.util.List;

public class GmAcheivementSetupForm extends GmPartySetupForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String hachievementID = "";
	private String type = "";
	private String title = "";
	private String description = "";
	private String month = "";
	private String year = "";
	private String coInvestigators = "";
	
	private ArrayList alType = new ArrayList();
	private ArrayList alMonth = new ArrayList();

	public void reset() {
		type = "0";
		title = "";
		description = "";
		month = "0";
		year = "";
		coInvestigators = "";
	}

	/**
	 * @return the hachievementID
	 */
	public String getHachievementID() {
		return hachievementID;
	}

	/**
	 * @param hachievementID the hachievementID to set
	 */
	public void setHachievementID(String hachievementID) {
		this.hachievementID = hachievementID;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the alType
	 */
	public ArrayList getAlType() {
		return alType;
	}

	/**
	 * @param alType the alType to set
	 */
	public void setAlType(ArrayList alType) {
		this.alType = alType;
	}

	/**
	 * @return the alMonth
	 */
	public ArrayList getAlMonth() {
		return alMonth;
	}

	/**
	 * @param alMonth the alMonth to set
	 */
	public void setAlMonth(ArrayList alMonth) {
		this.alMonth = alMonth;
	}

	/**
	 * @return the coInvestigators
	 */
	public String getCoInvestigators() {
		return coInvestigators;
	}

	/**
	 * @param coInvestigators the coInvestigators to set
	 */
	public void setCoInvestigators(String coInvestigators) {
		this.coInvestigators = coInvestigators;
	}
	
	
	
}