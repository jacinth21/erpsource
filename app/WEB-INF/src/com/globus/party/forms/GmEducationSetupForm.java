/**
 * FileName    : GmEducationSetupForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Oct 10, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.forms;

import java.util.ArrayList;


/**
 * @author sthadeshwar
 *
 */
public class GmEducationSetupForm extends GmPartySetupForm {
	
	private String heducationId = "";
	private String educationLevel = "";
	private String instituteName = "";
	private String classYear = "";
	private String course = "";
	private String designation = "";
	
	private ArrayList alLevel = new ArrayList();
	private ArrayList alDesignation = new ArrayList();
	
	private String courseLabel = "Course";
	
	/**
	 * @return the heducationId
	 */
	public String getHeducationId() {
		return heducationId;
	}
	/**
	 * @param heducationId the heducationId to set
	 */
	public void setHeducationId(String heducationId) {
		this.heducationId = heducationId;
	}
	/**
	 * @return the educationLevel
	 */
	public String getEducationLevel() {
		return educationLevel;
	}
	/**
	 * @param educationLevel the educationLevel to set
	 */
	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}
	/**
	 * @return the instituteName
	 */
	public String getInstituteName() {
		return instituteName;
	}
	/**
	 * @param instituteName the instituteName to set
	 */
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	/**
	 * @return the classYear
	 */
	public String getClassYear() {
		return classYear;
	}
	/**
	 * @param classYear the classYear to set
	 */
	public void setClassYear(String classYear) {
		this.classYear = classYear;
	}
	/**
	 * @return the course
	 */
	public String getCourse() {
		return course;
	}
	/**
	 * @param course the course to set
	 */
	public void setCourse(String course) {
		this.course = course;
	}
	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}
	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	/**
	 * @return the alLevel
	 */
	public ArrayList getAlLevel() {
		return alLevel;
	}
	/**
	 * @param alLevel the alLevel to set
	 */
	public void setAlLevel(ArrayList alLevel) {
		this.alLevel = alLevel;
	}
	/**
	 * @return the alDesignation
	 */
	public ArrayList getAlDesignation() {
		return alDesignation;
	}
	/**
	 * @param alDesignation the alDesignation to set
	 */
	public void setAlDesignation(ArrayList alDesignation) {
		this.alDesignation = alDesignation;
	}
	/**
	 * @return the courseLabel
	 */
	public String getCourseLabel() {
		return courseLabel;
	}
	/**
	 * @param courseLabel the courseLabel to set
	 */
	public void setCourseLabel(String courseLabel) {
		this.courseLabel = courseLabel;
	}
	
}
