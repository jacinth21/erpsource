//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\forms\\GmCareerSetupForm.java

/**
 * FileName    : GmCareerSetupForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Oct 10, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.RowSetDynaClass;

/**
 * Form bean class for the career setup for party
 * @author sthadeshwar
 */
public class GmCareerSetupForm extends GmPartySetupForm 
{
	private String hcareerId = "";
	private String title = "";
	private String companyName = "";
	private String companyDesc = "";
	private String startMonth ="0";
	private String startYear="";
	private String endMonth="0";
	private String endYear="";
	private String stillInPosition = "";
	private String groupDivision = "";
	private String country="0";
	private String state = "";
	private String city = "";
	private String industry = "";

	private ArrayList alMonths = new ArrayList();
	private ArrayList alState = new ArrayList();
	private ArrayList alCountry = new ArrayList();
	private ArrayList alIndustry = new ArrayList();
	
	/**
	 * @return the hcareerId
	 */
	public String getHcareerId() {
		return hcareerId;
	}
	/**
	 * @param hcareerId the hcareerId to set
	 */
	public void setHcareerId(String hcareerId) {
		this.hcareerId = hcareerId;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}
	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	/**
	 * @return the companyDesc
	 */
	public String getCompanyDesc() {
		return companyDesc;
	}
	/**
	 * @param companyDesc the companyDesc to set
	 */
	public void setCompanyDesc(String companyDesc) {
		this.companyDesc = companyDesc;
	}
	/**
	 * @return the startMonth
	 */
	public String getStartMonth() {
		return startMonth;
	}
	/**
	 * @param startMonth the startMonth to set
	 */
	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}
	/**
	 * @return the startYear
	 */
	public String getStartYear() {
		return startYear;
	}
	/**
	 * @param startYear the startYear to set
	 */
	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}
	/**
	 * @return the endMonth
	 */
	public String getEndMonth() {
		return endMonth;
	}
	/**
	 * @param endMonth the endMonth to set
	 */
	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}
	/**
	 * @return the endYear
	 */
	public String getEndYear() {
		return endYear;
	}
	/**
	 * @param endYear the endYear to set
	 */
	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}
	/**
	 * @return the stillInPosition
	 */
	public String getStillInPosition() {
		return stillInPosition;
	}
	/**
	 * @param stillInPosition the stillInPosition to set
	 */
	public void setStillInPosition(String stillInPosition) {
		this.stillInPosition = stillInPosition;
	}
	/**
	 * @return the groupDivision
	 */
	public String getGroupDivision() {
		return groupDivision;
	}
	/**
	 * @param groupDivision the groupDivision to set
	 */
	public void setGroupDivision(String groupDivision) {
		this.groupDivision = groupDivision;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the industry
	 */
	public String getIndustry() {
		return industry;
	}
	/**
	 * @param industry the industry to set
	 */
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	/**
	 * @return the alMonths
	 */
	public ArrayList getAlMonths() {
		return alMonths;
	}
	/**
	 * @param alMonths the alMonths to set
	 */
	public void setAlMonths(ArrayList alMonths) {
		this.alMonths = alMonths;
	}
	/**
	 * @return the alState
	 */
	public ArrayList getAlState() {
		return alState;
	}
	/**
	 * @param alState the alState to set
	 */
	public void setAlState(ArrayList alState) {
		this.alState = alState;
	}
	/**
	 * @return the alCountry
	 */
	public ArrayList getAlCountry() {
		return alCountry;
	}
	/**
	 * @param alCountry the alCountry to set
	 */
	public void setAlCountry(ArrayList alCountry) {
		this.alCountry = alCountry;
	}
	/**
	 * @return the alIndustry
	 */
	public ArrayList getAlIndustry() {
		return alIndustry;
	}
	/**
	 * @param alIndustry the alIndustry to set
	 */
	public void setAlIndustry(ArrayList alIndustry) {
		this.alIndustry = alIndustry;
	}

	
}
