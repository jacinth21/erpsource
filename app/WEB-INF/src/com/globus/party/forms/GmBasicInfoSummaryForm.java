/**
 * FileName    : GmBasicInfoSummaryForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Nov 24, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.forms;

import java.util.ArrayList;

/**
 * @author sthadeshwar
 *
 */
public class GmBasicInfoSummaryForm extends GmPartySearchForm {

	private ArrayList alContacts = new ArrayList();
	private String name = "";
	private String address = "";
	/**
	 * @return the alContacts
	 */
	public ArrayList getAlContacts() {
		return alContacts;
	}
	/**
	 * @param alContacts the alContacts to set
	 */
	public void setAlContacts(ArrayList alContacts) {
		this.alContacts = alContacts;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	
}
