// Source file:
// C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\forms\\GmPartySetupForm.java

/**
 * FileName : GmPartySetupForm.java Description : Author : sthadeshwar Date : Oct 3, 2008 Copyright
 * : Globus Medical Inc
 */
package com.globus.party.forms;


import java.util.ArrayList;

/**
 * Form bean class for the basic info setup for NPI transaction
 * 
 * @author APrasath
 */
public class GmNPIProcessForm extends GmPartySetupForm {

  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  private String txnid = "";
  private String accountid = "";
  private String repid = "";
  private String npinum = "";
  private String surgeonname = "";
  private String searchnpinum = "";
  private String searchsurgeonname = "";
  private String npitranid = "";
  private String submitType = "";
  private String npiFromDt = "";
  private String npiToDt = "";
  private String gridXmlData = "";
  private String searchrepid = "";
  private String searchaccountid = "";
  private String surgeonId = "";
  private int intGridSize;
  private ArrayList alNPIDetails = new ArrayList();

  public String getTxnid() {
    return txnid;
  }

  public void setTxnid(String txnid) {
    this.txnid = txnid;
  }

  public String getAccountid() {
    return accountid;
  }

  public ArrayList getAlNPIDetails() {
    return alNPIDetails;
  }

  public void setAlNPIDetails(ArrayList alNPIDetails) {
    this.alNPIDetails = alNPIDetails;
  }

  public void setAccountid(String accountid) {
    this.accountid = accountid;
  }

  public String getNpinum() {
    return npinum;
  }

  public void setNpinum(String npinum) {
    this.npinum = npinum;
  }

  public String getSurgeonname() {
    return surgeonname;
  }

  public void setSurgeonname(String surgeonname) {
    this.surgeonname = surgeonname;
  }

  public String getNpitranid() {
    return npitranid;
  }

  public void setNpitranid(String npitranid) {
    this.npitranid = npitranid;
  }

  /**
   * @return the repid
   */
  public String getRepid() {
    return repid;
  }

  /**
   * @param repid the repid to set
   */
  public void setRepid(String repid) {
    this.repid = repid;
  }

  /**
   * @return the submitType
   */
  public String getSubmitType() {
    return submitType;
  }

  /**
   * @param submitType the submitType to set
   */
  public void setSubmitType(String submitType) {
    this.submitType = submitType;
  }

  /**
   * @return the npiFromDt
   */
  public String getNpiFromDt() {
    return npiFromDt;
  }

  /**
   * @param npiFromDt the npiFromDt to set
   */
  public void setNpiFromDt(String npiFromDt) {
    this.npiFromDt = npiFromDt;
  }

  /**
   * @return the npiToDt
   */
  public String getNpiToDt() {
    return npiToDt;
  }

  /**
   * @param npiToDt the npiToDt to set
   */
  public void setNpiToDt(String npiToDt) {
    this.npiToDt = npiToDt;
  }

  /**
   * @return the gridXmlData
   */
  public String getGridXmlData() {
    return gridXmlData;
  }

  /**
   * @param gridXmlData the gridXmlData to set
   */
  public void setGridXmlData(String gridXmlData) {
    this.gridXmlData = gridXmlData;
  }

  /**
   * @return the searchnpinum
   */
  public String getSearchnpinum() {
    return searchnpinum;
  }

  /**
   * @param searchnpinum the searchnpinum to set
   */
  public void setSearchnpinum(String searchnpinum) {
    this.searchnpinum = searchnpinum;
  }

  /**
   * @return the searchsurgeonname
   */
  public String getSearchsurgeonname() {
    return searchsurgeonname;
  }

  /**
   * @param searchsurgeonname the searchsurgeonname to set
   */
  public void setSearchsurgeonname(String searchsurgeonname) {
    this.searchsurgeonname = searchsurgeonname;
  }



  /**
   * @return the searchrepid
   */
  public String getSearchrepid() {
    return searchrepid;
  }

  /**
   * @param searchrepid the searchrepid to set
   */
  public void setSearchrepid(String searchrepid) {
    this.searchrepid = searchrepid;
  }

  /**
   * @return the searchaccountid
   */
  public String getSearchaccountid() {
    return searchaccountid;
  }

  /**
   * @param searchaccountid the searchaccountid to set
   */
  public void setSearchaccountid(String searchaccountid) {
    this.searchaccountid = searchaccountid;
  }

  /**
   * @return the intGridSize
   */
  public int getIntGridSize() {
    return intGridSize;
  }

  /**
   * @param intGridSize the intGridSize to set
   */
  public void setIntGridSize(int intGridSize) {
    this.intGridSize = intGridSize;
  }

/**
 * @return the surgeonid
 */
public String getSurgeonId() {
	return surgeonId;
}

/**
 * @param surgeonid the surgeonid to set
 */
public void setSurgeonId(String surgeonId) {
	this.surgeonId = surgeonId;
}



}
