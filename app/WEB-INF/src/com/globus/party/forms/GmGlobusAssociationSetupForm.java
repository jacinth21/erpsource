//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\forms\\GmGlobusAssociationSetupForm.java

package com.globus.party.forms;

import java.util.ArrayList;

/**
 * Form bean class for the globus association setup for party
 * @author sthadeshwar
 */
public class GmGlobusAssociationSetupForm extends GmPartySetupForm 
{
	private String hglobusAssociationId = "";
	private String personnelType = "";
	private ArrayList alPersonnelType = new ArrayList();
	private String personnelName = "";
	private ArrayList alPersonnelName = new ArrayList();
	private String primaryFlag = "";
	
	
	/**
	 * @return the hglobusAssociationId
	 */
	public String getHglobusAssociationId() {
		return hglobusAssociationId;
	}
	/**
	 * @param hglobusAssociationId the hglobusAssociationId to set
	 */
	public void setHglobusAssociationId(String hglobusAssociationId) {
		this.hglobusAssociationId = hglobusAssociationId;
	}
	/**
	 * @return the personnelType
	 */
	public String getPersonnelType() {
		return personnelType;
	}
	/**
	 * @param personnelType the personnelType to set
	 */
	public void setPersonnelType(String personnelType) {
		this.personnelType = personnelType;
	}
	/**
	 * @return the alPersonnelType
	 */
	public ArrayList getAlPersonnelType() {
		return alPersonnelType;
	}
	/**
	 * @param alPersonnelType the alPersonnelType to set
	 */
	public void setAlPersonnelType(ArrayList alPersonnelType) {
		this.alPersonnelType = alPersonnelType;
	}
	/**
	 * @return the personnelName
	 */
	public String getPersonnelName() {
		return personnelName;
	}
	/**
	 * @param personnelName the personnelName to set
	 */
	public void setPersonnelName(String personnelName) {
		this.personnelName = personnelName;
	}
	/**
	 * @return the alPersonnelName
	 */
	public ArrayList getAlPersonnelName() {
		return alPersonnelName;
	}
	/**
	 * @param alPersonnelName the alPersonnelName to set
	 */
	public void setAlPersonnelName(ArrayList alPersonnelName) {
		this.alPersonnelName = alPersonnelName;
	}
	/**
	 * @return the primaryFlag
	 */
	public String getPrimaryFlag() {
		return primaryFlag;
	}
	/**
	 * @param primaryFlag the primaryFlag to set
	 */
	public void setPrimaryFlag(String primaryFlag) {
		this.primaryFlag = primaryFlag;
	}

	
}
