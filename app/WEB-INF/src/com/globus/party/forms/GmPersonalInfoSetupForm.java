//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\forms\\GmPersonalInfoSetupForm.java

package com.globus.party.forms;

import java.util.ArrayList;

/**
 * Form bean class for the personal info setup for party
 * @author sthadeshwar
 */
public class GmPersonalInfoSetupForm extends GmPartySetupForm 
{
	private String gender ="";
	private ArrayList alGender;
	private String dateOfBirth = "";
	private String hometown = "";
	private String personalWebsite = "";
	private String relationshipStatus ="";
	private ArrayList alRelationshipStatus;
	private String spouseDetails = "";
	private String children = "";
	private ArrayList alChildren;
	private String childrenDetails = "";
	private String hobbies = "";

	private String savedLabel = "";
	private String disableSpouseDet = "false";
	private String disableChildDet = "false";

	/**
	 * @roseuid 49137B57007C
	 */
	public GmPersonalInfoSetupForm() 
	{

	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public ArrayList getAlGender() {
		return alGender;
	}

	public void setAlGender(ArrayList alGender) {
		this.alGender = alGender;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getHometown() {
		return hometown;
	}

	public void setHometown(String hometown) {
		this.hometown = hometown;
	}

	public String getPersonalWebsite() {
		return personalWebsite;
	}

	public void setPersonalWebsite(String personalWebsite) {
		this.personalWebsite = personalWebsite;
	}

	public String getRelationshipStatus() {
		return relationshipStatus;
	}

	public void setRelationshipStatus(String relationshipStatus) {
		this.relationshipStatus = relationshipStatus;
	}

	public ArrayList getAlRelationshipStatus() {
		return alRelationshipStatus;
	}

	public void setAlRelationshipStatus(ArrayList alRelationshipStatus) {
		this.alRelationshipStatus = alRelationshipStatus;
	}

	public String getSpouseDetails() {
		return spouseDetails;
	}

	public void setSpouseDetails(String spouseDetails) {
		this.spouseDetails = spouseDetails;
	}

	public String getChildren() {
		return children;
	}

	public void setChildren(String children) {
		this.children = children;
	}

	public ArrayList getAlChildren() {
		return alChildren;
	}

	public void setAlChildren(ArrayList alChildren) {
		this.alChildren = alChildren;
	}

	public String getChildrenDetails() {
		return childrenDetails;
	}

	public void setChildrenDetails(String childrenDetails) {
		this.childrenDetails = childrenDetails;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	/**
	 * @return the savedLabel
	 */
	public String getSavedLabel() {
		return savedLabel;
	}

	/**
	 * @param savedLabel the savedLabel to set
	 */
	public void setSavedLabel(String savedLabel) {
		this.savedLabel = savedLabel;
	}

	/**
	 * @return the disableSpouseDet
	 */
	public String getDisableSpouseDet() {
		return disableSpouseDet;
	}

	/**
	 * @param disableSpouseDet the disableSpouseDet to set
	 */
	public void setDisableSpouseDet(String disableSpouseDet) {
		this.disableSpouseDet = disableSpouseDet;
	}

	/**
	 * @return the disableChildDet
	 */
	public String getDisableChildDet() {
		return disableChildDet;
	}

	/**
	 * @param disableChildDet the disableChildDet to set
	 */
	public void setDisableChildDet(String disableChildDet) {
		this.disableChildDet = disableChildDet;
	}
	
	
}
