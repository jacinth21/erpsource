/**
 * FileName    : GmGlobusInfoSummaryForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Dec 2, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.forms;

/**
 * @author sthadeshwar
 *
 */
public class GmGlobusInfoSummaryForm extends GmPartySetupForm {

	private String reps = "";
	private String adName = "";
	private String region = "";
	/**
	 * @return the reps
	 */
	public String getReps() {
		return reps;
	}
	/**
	 * @param reps the reps to set
	 */
	public void setReps(String reps) {
		this.reps = reps;
	}
	/**
	 * @return the adName
	 */
	public String getAdName() {
		return adName;
	}
	/**
	 * @param adName the adName to set
	 */
	public void setAdName(String adName) {
		this.adName = adName;
	}
	/**
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}
	/**
	 * @param region the region to set
	 */
	public void setRegion(String region) {
		this.region = region;
	}
	
	
}
