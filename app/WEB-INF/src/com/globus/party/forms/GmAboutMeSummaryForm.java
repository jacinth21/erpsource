/**
 * FileName    : GmAboutMeSummaryForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Nov 26, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.forms;

/**
 * @author sthadeshwar
 *
 */
public class GmAboutMeSummaryForm extends GmPersonalInfoSetupForm {
	
	private String speciality = "";
	private String professionalInterests = "";
	private String fileId = "";
	
	/**
	 * @return the speciality
	 */
	public String getSpeciality() {
		return speciality;
	}
	/**
	 * @param speciality the speciality to set
	 */
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	/**
	 * @return the professionalInterests
	 */
	public String getProfessionalInterests() {
		return professionalInterests;
	}
	/**
	 * @param professionalInterests the professionalInterests to set
	 */
	public void setProfessionalInterests(String professionalInterests) {
		this.professionalInterests = professionalInterests;
	}
	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}
	/**
	 * @param fileId the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	
	
	
}
