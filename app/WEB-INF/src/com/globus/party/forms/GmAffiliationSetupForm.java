package com.globus.party.forms;

import java.util.ArrayList;

/**
 * @author Angela
 */

public class GmAffiliationSetupForm extends GmPartySetupForm {

	private ArrayList alTypes = new ArrayList();
	
	private String organization = "";
	private String affType = "";
	private String committeeName = "";
	private String role = "";
	private String startYear = "";
	private String endYear = "";
	private String stillInPosition = "";
	private String affId = "";

	public void reset() {
		affId = "";
		organization = "";
		affType = "";
		committeeName = "";
		role = "";
		startYear = "";
		endYear = "";
		stillInPosition = "off";
	}

	/**
	 * @return the alTypes
	 */
	public ArrayList getAlTypes() {
		return alTypes;
	}

	/**
	 * @param alTypes the alTypes to set
	 */
	public void setAlTypes(ArrayList alTypes) {
		this.alTypes = alTypes;
	}

	/**
	 * @return the organization
	 */
	public String getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	/**
	 * @return the affType
	 */
	public String getAffType() {
		return affType;
	}

	/**
	 * @param affType the affType to set
	 */
	public void setAffType(String affType) {
		this.affType = affType;
	}

	/**
	 * @return the committeeName
	 */
	public String getCommitteeName() {
		return committeeName;
	}

	/**
	 * @param committeeName the committeeName to set
	 */
	public void setCommitteeName(String committeeName) {
		this.committeeName = committeeName;
	}

	
	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the startYear
	 */
	public String getStartYear() {
		return startYear;
	}

	/**
	 * @param startYear the startYear to set
	 */
	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	/**
	 * @return the endYear
	 */
	public String getEndYear() {
		return endYear;
	}

	/**
	 * @param endYear the endYear to set
	 */
	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}

	/**
	 * @return the stillInPosition
	 */
	public String getStillInPosition() {
		return stillInPosition;
	}

	/**
	 * @param stillInPosition the stillInPosition to set
	 */
	public void setStillInPosition(String stillInPosition) {
		this.stillInPosition = stillInPosition;
	}

	/**
	 * @return the affId
	 */
	public String getAffId() {
		return affId;
	}

	/**
	 * @param affId the affId to set
	 */
	public void setAffId(String affId) {
		this.affId = affId;
	}
	
	
}