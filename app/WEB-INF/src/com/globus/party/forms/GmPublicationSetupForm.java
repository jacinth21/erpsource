package com.globus.party.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCancelForm;
import com.globus.common.forms.GmCommonForm;

public class GmPublicationSetupForm extends  GmPartySetupForm {

	/**
	 * 
	 * @author xqu
	 * @date Oct 15, 2008
	 * @param
	 */

	private ArrayList alMonthList = new ArrayList();
	private ArrayList alTypeList = new ArrayList();
	private ArrayList alPartyList = new ArrayList();

	private List returnList = new ArrayList();

	private String author = "";
	private String refURL = "";
	private String title = "";
	private String reference = "";
	private String pabstract = "";
	private String pyear = "";
	private String typeID = "";
	private String monthID = "";
	private String hpublicationID = "";
	
	public void reset() {
		author = "";
		refURL = "";
		title = "";
		reference = "";
		pabstract = "";
		pyear = "";
		typeID = "0";
		monthID = "0";
		hpublicationID = "";		
	}
	
	/**
	 * @return the alMonthList
	 */
	public ArrayList getAlMonthList() {
		return alMonthList;
	}
	/**
	 * @param alMonthList the alMonthList to set
	 */
	public void setAlMonthList(ArrayList alMonthList) {
		this.alMonthList = alMonthList;
	}
	/**
	 * @return the alTypeList
	 */
	public ArrayList getAlTypeList() {
		return alTypeList;
	}
	/**
	 * @param alTypeList the alTypeList to set
	 */
	public void setAlTypeList(ArrayList alTypeList) {
		this.alTypeList = alTypeList;
	}
	/**
	 * @return the alPartyList
	 */
	public ArrayList getAlPartyList() {
		return alPartyList;
	}
	/**
	 * @param alPartyList the alPartyList to set
	 */
	public void setAlPartyList(ArrayList alPartyList) {
		this.alPartyList = alPartyList;
	}
	/**
	 * @return the returnList
	 */
	public List getReturnList() {
		return returnList;
	}
	/**
	 * @param returnList the returnList to set
	 */
	public void setReturnList(List returnList) {
		this.returnList = returnList;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * @return the refURL
	 */
	public String getRefURL() {
		return refURL;
	}
	/**
	 * @param refURL the refURL to set
	 */
	public void setRefURL(String refURL) {
		this.refURL = refURL;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}
	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}
	/**
	 * @return the pabstract
	 */
	public String getPabstract() {
		return pabstract;
	}
	/**
	 * @param pabstract the pabstract to set
	 */
	public void setPabstract(String pabstract) {
		this.pabstract = pabstract;
	}
	/**
	 * @return the pyear
	 */
	public String getPyear() {
		return pyear;
	}
	/**
	 * @param pyear the pyear to set
	 */
	public void setPyear(String pyear) {
		this.pyear = pyear;
	}
	/**
	 * @return the typeID
	 */
	public String getTypeID() {
		return typeID;
	}
	/**
	 * @param typeID the typeID to set
	 */
	public void setTypeID(String typeID) {
		this.typeID = typeID;
	}
	/**
	 * @return the monthID
	 */
	public String getMonthID() {
		return monthID;
	}
	/**
	 * @param monthID the monthID to set
	 */
	public void setMonthID(String monthID) {
		this.monthID = monthID;
	}
	/**
	 * @return the hpublicationID
	 */
	public String getHpublicationID() {
		return hpublicationID;
	}
	/**
	 * @param hpublicationID the hpublicationID to set
	 */
	public void setHpublicationID(String hpublicationID) {
		this.hpublicationID = hpublicationID;
	}
	
	
}
