//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\forms\\GmContactSetupForm.java

package com.globus.party.forms;

import java.util.ArrayList;

/**
 * Form bean class for the contact info setup for party
 * 
 * @author sthadeshwar
 */
public class GmContactSetupForm extends GmPartySetupForm 
{
	private String hcontactId = "";
	private String contactType = "";
	private String contactMode = "";
	private ArrayList alContactMode = new ArrayList();
	private String preference = "";
	private String contactValue = "";
	private String primaryFlag = "";
	private String inactiveFlag = "";
	/**
	 * PMT-28307 Access Driven Sales Rep Setup 
	*/
	private String editAccessFl ="";
	
	public String getEditAccessFl() {
		return editAccessFl;
	}
	public void setEditAccessFl(String editAccessFl) {
		this.editAccessFl = editAccessFl;
	}
	/**
	 * Method to reset the form
	 */
	public void reset() {
		hcontactId = "";
		contactType = "";
		contactMode = "0";
		preference = "";
		contactValue = "";
		primaryFlag = "off";
		inactiveFlag = "off";
	}
	/**
	 * @return the hcontactId
	 */
	public String getHcontactId() {
		return hcontactId;
	}
	/**
	 * @param hcontactId the hcontactId to set
	 */
	public void setHcontactId(String hcontactId) {
		this.hcontactId = hcontactId;
	}
	/**
	 * @return the contactType
	 */
	public String getContactType() {
		return contactType;
	}
	/**
	 * @param contactType the contactType to set
	 */
	public void setContactType(String contactType) {
		this.contactType = contactType;
	}
	/**
	 * @return the contactMode
	 */
	public String getContactMode() {
		return contactMode;
	}
	/**
	 * @param contactMode the contactMode to set
	 */
	public void setContactMode(String contactMode) {
		this.contactMode = contactMode;
	}
	/**
	 * @return the alContactMode
	 */
	public ArrayList getAlContactMode() {
		return alContactMode;
	}
	/**
	 * @param alContactMode the alContactMode to set
	 */
	public void setAlContactMode(ArrayList alContactMode) {
		this.alContactMode = alContactMode;
	}
	/**
	 * @return the preference
	 */
	public String getPreference() {
		return preference;
	}
	/**
	 * @param preference the preference to set
	 */
	public void setPreference(String preference) {
		this.preference = preference;
	}
	/**
	 * @return the contactValue
	 */
	public String getContactValue() {
		return contactValue;
	}
	/**
	 * @param contactValue the contactValue to set
	 */
	public void setContactValue(String contactValue) {
		this.contactValue = contactValue;
	}
	/**
	 * @return the primaryFlag
	 */
	public String getPrimaryFlag() {
		return primaryFlag;
	}
	/**
	 * @param primaryFlag the primaryFlag to set
	 */
	public void setPrimaryFlag(String primaryFlag) {
		this.primaryFlag = primaryFlag;
	}
	/**
	 * @return the inactiveFlag
	 */
	public String getInactiveFlag() {
		return inactiveFlag;
	}
	/**
	 * @param inactiveFlag the inactiveFlag to set
	 */
	public void setInactiveFlag(String inactiveFlag) {
		this.inactiveFlag = inactiveFlag;
	}

}
