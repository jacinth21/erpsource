/**
 * FileName    : GmPartyCommonSearchForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Dec 12, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.party.forms;

import java.util.ArrayList;

/**
 * @author sthadeshwar
 *
 */
public class GmPartyCommonSearchForm extends GmPartySetupForm {

	private String searchGroup = "";
	private ArrayList alTabs = new ArrayList();
	private ArrayList alFields = new ArrayList();

	/**
	 * @return the searchGroup
	 */
	public String getSearchGroup() {
		return searchGroup;
	}
	/**
	 * @param searchGroup the searchGroup to set
	 */
	public void setSearchGroup(String searchGroup) {
		this.searchGroup = searchGroup;
	}
	/**
	 * @return the alTabs
	 */
	public ArrayList getAlTabs() {
		return alTabs;
	}
	/**
	 * @param alTabs the alTabs to set
	 */
	public void setAlTabs(ArrayList alTabs) {
		this.alTabs = alTabs;
	}
	/**
	 * @return the alFields
	 */
	public ArrayList getAlFields() {
		return alFields;
	}
	/**
	 * @param alFields the alFields to set
	 */
	public void setAlFields(ArrayList alFields) {
		this.alFields = alFields;
	}

}
