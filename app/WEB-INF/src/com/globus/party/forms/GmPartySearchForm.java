package com.globus.party.forms;

import java.util.ArrayList;

public class GmPartySearchForm extends GmPartySetupForm {

	private  String lookupId ="";
	private  String searchString ="";
	private String results = "";
	private String searchStringWithId = "";
	private ArrayList alResult = new ArrayList();
	
	/**
	 * @return the lookupId
	 */
	public String getLookupId() {
		return lookupId;
	}
	/**
	 * @param lookupId the lookupId to set
	 */
	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}
	/**
	 * @return the searchString
	 */
	public String getSearchString() {
		return searchString;
	}
	/**
	 * @param searchString the searchString to set
	 */
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}
	/**
	 * @return the results
	 */
	public String getResults() {
		return results;
	}
	/**
	 * @param results the results to set
	 */
	public void setResults(String results) {
		this.results = results;
	}
	/**
	 * @return the searchStringWithId
	 */
	public String getSearchStringWithId() {
		return searchStringWithId;
	}
	/**
	 * @param searchStringWithId the searchStringWithId to set
	 */
	public void setSearchStringWithId(String searchStringWithId) {
		this.searchStringWithId = searchStringWithId;
	}
	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}
	/**
	 * @param alResult the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	
}
