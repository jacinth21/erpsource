package com.globus.party.forms;

import java.util.ArrayList;

public class GmDealerInvoiceParamsForm extends GmPartySetupForm {
  private String dealerName = "";
  private String contactPerson = "";
  private String gridData = "";
  private String dealerID = "";
  private String invoicecustomertype = "";
  private String elecronicFileFormat = "";
  private String csvTemplatteName = "";
  private String invoiceLayout = "";
  private String paymentTerms = "";
  private String colletorId = "";
  private String invoiceClosingDate = "";
  private String electronicEmail = "";
  private String receipientsEmail = "";
  private String vendor = "";
  private String creditRating = "";
  private String partyId = "";
  private String statusFl = "";
  private String paymentMonth = "";
  private String paymentDays = "";
  private String paymentTermFlag = "";
  private String consolidatedBilling = "";


  ArrayList alDealerNm = new ArrayList();
  ArrayList alInvoiceCustomer = new ArrayList();
  ArrayList alElecrtonicFileFmt = new ArrayList();
  ArrayList alCsvTempName = new ArrayList();
  ArrayList alInvoiceLayout = new ArrayList();
  ArrayList alPaymentTerms = new ArrayList();
  ArrayList alInvoiceClosingDate = new ArrayList();
  ArrayList alCollector = new ArrayList();
  ArrayList alstatus = new ArrayList();
  ArrayList alDealer = new ArrayList();

  /**
   * @return the dealerName
   */
  public String getDealerName() {
    return dealerName;
  }

  /**
   * @param dealerName the dealerName to set
   */
  public void setDealerName(String dealerName) {
    this.dealerName = dealerName;
  }

  /**
   * @return the contactPerson
   */
  public String getContactPerson() {
    return contactPerson;
  }

  /**
   * @param contactPerson the contactPerson to set
   */
  public void setContactPerson(String contactPerson) {
    this.contactPerson = contactPerson;
  }

  /**
   * @return the gridData
   */
  public String getGridData() {
    return gridData;
  }

  /**
   * @param gridData the gridData to set
   */
  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  /**
   * @return the dealerID
   */
  public String getDealerID() {
    return dealerID;
  }

  /**
   * @param dealerID the dealerID to set
   */
  public void setDealerID(String dealerID) {
    this.dealerID = dealerID;
  }

  /**
   * @return the invoicecustomertype
   */
  public String getInvoicecustomertype() {
    return invoicecustomertype;
  }

  /**
   * @param invoicecustomertype the invoicecustomertype to set
   */
  public void setInvoicecustomertype(String invoicecustomertype) {
    this.invoicecustomertype = invoicecustomertype;
  }

  /**
   * @return the elecronicFileFormat
   */
  public String getElecronicFileFormat() {
    return elecronicFileFormat;
  }

  /**
   * @param elecronicFileFormat the elecronicFileFormat to set
   */
  public void setElecronicFileFormat(String elecronicFileFormat) {
    this.elecronicFileFormat = elecronicFileFormat;
  }

  /**
   * @return the csvTemplatteName
   */
  public String getCsvTemplatteName() {
    return csvTemplatteName;
  }

  /**
   * @param csvTemplatteName the csvTemplatteName to set
   */
  public void setCsvTemplatteName(String csvTemplatteName) {
    this.csvTemplatteName = csvTemplatteName;
  }

  /**
   * @return the invoiceLayout
   */
  public String getInvoiceLayout() {
    return invoiceLayout;
  }

  /**
   * @param invoiceLayout the invoiceLayout to set
   */
  public void setInvoiceLayout(String invoiceLayout) {
    this.invoiceLayout = invoiceLayout;
  }

  /**
   * @return the paymentTerms
   */
  public String getPaymentTerms() {
    return paymentTerms;
  }

  /**
   * @param paymentTerms the paymentTerms to set
   */
  public void setPaymentTerms(String paymentTerms) {
    this.paymentTerms = paymentTerms;
  }

  /**
   * @return the colletorId
   */
  public String getColletorId() {
    return colletorId;
  }

  /**
   * @param colletorId the colletorId to set
   */
  public void setColletorId(String colletorId) {
    this.colletorId = colletorId;
  }

  /**
   * @return the invoiceClosingDate
   */
  public String getInvoiceClosingDate() {
    return invoiceClosingDate;
  }

  /**
   * @param invoiceClosingDate the invoiceClosingDate to set
   */
  public void setInvoiceClosingDate(String invoiceClosingDate) {
    this.invoiceClosingDate = invoiceClosingDate;
  }

  /**
   * @return the electronicEmail
   */
  public String getElectronicEmail() {
    return electronicEmail;
  }

  /**
   * @param electronicEmail the electronicEmail to set
   */
  public void setElectronicEmail(String electronicEmail) {
    this.electronicEmail = electronicEmail;
  }

  /**
   * @return the receipientsEmail
   */
  public String getReceipientsEmail() {
    return receipientsEmail;
  }

  /**
   * @param receipientsEmail the receipientsEmail to set
   */
  public void setReceipientsEmail(String receipientsEmail) {
    this.receipientsEmail = receipientsEmail;
  }

  /**
   * @return the vendor
   */
  public String getVendor() {
    return vendor;
  }

  /**
   * @param vendor the vendor to set
   */
  public void setVendor(String vendor) {
    this.vendor = vendor;
  }

  /**
   * @return the creditRating
   */
  public String getCreditRating() {
    return creditRating;
  }

  /**
   * @param creditRating the creditRating to set
   */
  public void setCreditRating(String creditRating) {
    this.creditRating = creditRating;
  }

  /**
   * @return the partyId
   */
  @Override
  public String getPartyId() {
    return partyId;
  }

  /**
   * @param partyId the partyId to set
   */
  @Override
  public void setPartyId(String partyId) {
    this.partyId = partyId;
  }

  /**
   * @return the statusFl
   */
  public String getStatusFl() {
    return statusFl;
  }

  /**
   * @param statusFl the statusFl to set
   */
  public void setStatusFl(String statusFl) {
    this.statusFl = statusFl;
  }

  /**
   * @return the alDealerNm
   */
  public ArrayList getAlDealerNm() {
    return alDealerNm;
  }

  /**
   * @param alDealerNm the alDealerNm to set
   */
  public void setAlDealerNm(ArrayList alDealerNm) {
    this.alDealerNm = alDealerNm;
  }

  /**
   * @return the alInvoiceCustomer
   */
  public ArrayList getAlInvoiceCustomer() {
    return alInvoiceCustomer;
  }

  /**
   * @param alInvoiceCustomer the alInvoiceCustomer to set
   */
  public void setAlInvoiceCustomer(ArrayList alInvoiceCustomer) {
    this.alInvoiceCustomer = alInvoiceCustomer;
  }

  /**
   * @return the alElecrtonicFileFmt
   */
  public ArrayList getAlElecrtonicFileFmt() {
    return alElecrtonicFileFmt;
  }

  /**
   * @param alElecrtonicFileFmt the alElecrtonicFileFmt to set
   */
  public void setAlElecrtonicFileFmt(ArrayList alElecrtonicFileFmt) {
    this.alElecrtonicFileFmt = alElecrtonicFileFmt;
  }

  /**
   * @return the alCsvTempName
   */
  public ArrayList getAlCsvTempName() {
    return alCsvTempName;
  }

  /**
   * @param alCsvTempName the alCsvTempName to set
   */
  public void setAlCsvTempName(ArrayList alCsvTempName) {
    this.alCsvTempName = alCsvTempName;
  }

  /**
   * @return the alInvoiceLayout
   */
  public ArrayList getAlInvoiceLayout() {
    return alInvoiceLayout;
  }

  /**
   * @param alInvoiceLayout the alInvoiceLayout to set
   */
  public void setAlInvoiceLayout(ArrayList alInvoiceLayout) {
    this.alInvoiceLayout = alInvoiceLayout;
  }

  /**
   * @return the alPaymentTerms
   */
  public ArrayList getAlPaymentTerms() {
    return alPaymentTerms;
  }

  /**
   * @param alPaymentTerms the alPaymentTerms to set
   */
  public void setAlPaymentTerms(ArrayList alPaymentTerms) {
    this.alPaymentTerms = alPaymentTerms;
  }

  /**
   * @return the alInvoiceClosingDate
   */
  public ArrayList getAlInvoiceClosingDate() {
    return alInvoiceClosingDate;
  }

  /**
   * @param alInvoiceClosingDate the alInvoiceClosingDate to set
   */
  public void setAlInvoiceClosingDate(ArrayList alInvoiceClosingDate) {
    this.alInvoiceClosingDate = alInvoiceClosingDate;
  }

  /**
   * @return the alCollector
   */
  public ArrayList getAlCollector() {
    return alCollector;
  }

  /**
   * @param alCollector the alCollector to set
   */
  public void setAlCollector(ArrayList alCollector) {
    this.alCollector = alCollector;
  }

  /**
   * @return the alstatus
   */
  public ArrayList getAlstatus() {
    return alstatus;
  }

  /**
   * @param alstatus the alstatus to set
   */
  public void setAlstatus(ArrayList alstatus) {
    this.alstatus = alstatus;
  }

  /**
   * @return the alDealer
   */
  public ArrayList getAlDealer() {
    return alDealer;
  }

  /**
   * @param alDealer the alDealer to set
   */
  public void setAlDealer(ArrayList alDealer) {
    this.alDealer = alDealer;
  }

  /**
   * @return the paymentMonth
   */
  public String getPaymentMonth() {
    return paymentMonth;
  }

  /**
   * @param paymentMonth the paymentMonth to set
   */
  public void setPaymentMonth(String paymentMonth) {
    this.paymentMonth = paymentMonth;
  }

  /**
   * @return the paymentDays
   */
  public String getPaymentDays() {
    return paymentDays;
  }

  /**
   * @param paymentDays the paymentDays to set
   */
  public void setPaymentDays(String paymentDays) {
    this.paymentDays = paymentDays;
  }


  /**
   * @return the paymentTermFlag
   */
  public String getPaymentTermFlag() {
    return paymentTermFlag;
  }

  /**
   * @param paymentTermFlag the paymentTermFlag to set
   */
  public void setPaymentTermFlag(String paymentTermFlag) {
    this.paymentTermFlag = paymentTermFlag;
  }

  /**
   * @return the consolidatedBilling
   */
  public String getConsolidatedBilling() {
    return consolidatedBilling;
  }

  /**
   * @param consolidatedBilling the consolidatedBilling to set
   */
  public void setConsolidatedBilling(String consolidatedBilling) {
    this.consolidatedBilling = consolidatedBilling;
  }

}
