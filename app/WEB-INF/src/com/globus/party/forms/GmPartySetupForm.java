// Source file:
// C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\forms\\GmPartySetupForm.java

/**
 * FileName : GmPartySetupForm.java Description : Author : sthadeshwar Date : Oct 3, 2008 Copyright
 * : Globus Medical Inc
 */
package com.globus.party.forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import com.globus.common.forms.GmCancelForm;

/**
 * Form bean class for the basic info setup for party
 * 
 * @author sthadeshwar
 */
public class GmPartySetupForm extends GmCancelForm {

  private String title = "";
  private String firstName = "";
  private String lastName = "";
  private String middleInitial = "";
  private String partyId = "";
  private String displayPartyName = "";
  private String partyName = "";
  private List lresult = null;
  private String partyType = "";
  private String inactiveFlag = "";
  private ArrayList alParty = new ArrayList();
  private ArrayList alTabs = new ArrayList();
  private HashMap hmSearchCodeMap = new HashMap();
  private String accessFlag = "";
  private String grpPartyId = "";
  private String haction = "";
  private String strReloadCnt = "";
  private String partyNameEn = "";
  private String strDealerAcctFlag = "";

  /**
   * @return the accessFlag
   */
  public String getAccessFlag() {
    return accessFlag;
  }

  /**
   * @param accessFlag the accessFlag to set
   */
  public void setAccessFlag(String accessFlag) {
    this.accessFlag = accessFlag;
  }

  private String includedPage = "";

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the firstName
   */
  public String getFirstName() {
    return firstName;
  }

  /**
   * @param firstName the firstName to set
   */
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  /**
   * @return the lastName
   */
  public String getLastName() {
    return lastName;
  }

  /**
   * @param lastName the lastName to set
   */
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  /**
   * @return the middleInitial
   */
  public String getMiddleInitial() {
    return middleInitial;
  }

  /**
   * @param middleInitial the middleInitial to set
   */
  public void setMiddleInitial(String middleInitial) {
    this.middleInitial = middleInitial;
  }

  /**
   * @return the partyId
   */
  @Override
  public String getPartyId() {
    return partyId;
  }

  /**
   * @param partyId the partyId to set
   */
  @Override
  public void setPartyId(String partyId) {
    this.partyId = partyId;
  }

  /**
   * @return the displayPartyName
   */
  public String getDisplayPartyName() {
    return displayPartyName;
  }

  /**
   * @param displayPartyName the displayPartyName to set
   */
  public void setDisplayPartyName(String displayPartyName) {
    this.displayPartyName = displayPartyName;
  }

  /**
   * @return the partyName
   */
  public String getPartyName() {
    return partyName;
  }

  /**
   * @param partyName the partyName to set
   */
  public void setPartyName(String partyName) {
    this.partyName = partyName;
  }

  /**
   * @return the partyType
   */
  public String getPartyType() {
    return partyType;
  }

  /**
   * @param partyType the partyType to set
   */
  public void setPartyType(String partyType) {
    this.partyType = partyType;
  }

  /**
   * @return the lresult
   */
  public List getLresult() {
    return lresult;
  }

  /**
   * @param lresult the lresult to set
   */
  public void setLresult(List lresult) {
    this.lresult = lresult;
  }

  /**
   * @return the inactiveFlag
   */
  public String getInactiveFlag() {
    return inactiveFlag;
  }

  /**
   * @param inactiveFlag the inactiveFlag to set
   */
  public void setInactiveFlag(String inactiveFlag) {
    this.inactiveFlag = inactiveFlag;
  }

  /**
   * @return the alParty
   */
  public ArrayList getAlParty() {
    return alParty;
  }

  /**
   * @param alParty the alParty to set
   */
  public void setAlParty(ArrayList alParty) {
    this.alParty = alParty;
  }

  /**
   * @return the alTabs
   */
  public ArrayList getAlTabs() {
    return alTabs;
  }

  /**
   * @param alTabs the alTabs to set
   */
  public void setAlTabs(ArrayList alTabs) {
    this.alTabs = alTabs;
  }

  public HashMap getHmSearchCodeMap() {
    return hmSearchCodeMap;
  }

  public void setHmSearchCodeMap(HashMap hmSearchCodeMap) {
    this.hmSearchCodeMap = hmSearchCodeMap;
  }

  /**
   * @return the includedPage
   */
  public String getIncludedPage() {
    return includedPage;
  }

  /**
   * @param includedPage the includedPage to set
   */
  public void setIncludedPage(String includedPage) {
    this.includedPage = includedPage;
  }

  /**
   * @return the haction
   */
  @Override
  public String getHaction() {
    return haction;
  }

  /**
   * @param haction the haction to set
   */
  @Override
  public void setHaction(String haction) {
    this.haction = haction;
  }

  /**
   * @return the grpPartyId
   */
  public String getGrpPartyId() {
    return grpPartyId;
  }

  /**
   * @param grpPartyId the grpPartyId to set
   */
  public void setGrpPartyId(String grpPartyId) {
    this.grpPartyId = grpPartyId;
  }

  /**
   * @return the strReloadCnt
   */
  public String getStrReloadCnt() {
    return strReloadCnt;
  }

  /**
   * @param strReloadCnt the strReloadCnt to set
   */
  public void setStrReloadCnt(String strReloadCnt) {
    this.strReloadCnt = strReloadCnt;
  }

  @Override
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    this.partyName = "";
    this.inactiveFlag = "off";
    try {
      request.setCharacterEncoding("UTF-8");
    } catch (UnsupportedEncodingException ex) {
    }

  }

  /**
   * @return the partyNameEn
   */
  public String getPartyNameEn() {
    return partyNameEn;
  }

  /**
   * @param partyNameEn the partyNameEn to set
   */
  public void setPartyNameEn(String partyNameEn) {
    this.partyNameEn = partyNameEn;
  }

  /**
   * @return the strDealerAcctFlag
   */
  public String getStrDealerAcctFlag() {
    return strDealerAcctFlag;
  }

  /**
   * @param strDealerAcctFlag the strDealerAcctFlag to set
   */
  public void setStrDealerAcctFlag(String strDealerAcctFlag) {
    this.strDealerAcctFlag = strDealerAcctFlag;
  }

}
