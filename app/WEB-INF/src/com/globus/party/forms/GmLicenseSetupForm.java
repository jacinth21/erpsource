//Source file: C:\\Projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\party\\forms\\GmLicenseSetupForm.java

package com.globus.party.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCancelForm;

/**
 * Form bean class for the license setup for party
 * 
 * @author sthadeshwar
 */
public class GmLicenseSetupForm extends GmPartySetupForm {
	private String licenseId = "";
	private String issueMonth = "";
	private String issueYear = "";
	private String country = "";
	private String state = "";
	private String description = "";
	private String partyId = "";
	private String title = "";
	private List ldtResult = new ArrayList();

	private ArrayList alIssueMonth = new ArrayList();
	private ArrayList alCountries = new ArrayList();
	private ArrayList alStates = new ArrayList();
	private ArrayList alMonth = new ArrayList();

	public void reset() {
		issueMonth = "";
		issueYear = "";
		country = "";
		state = "";
		description = "";
		title = "";
	}

	public List getLdtResult() {
		return ldtResult;
	}

	public void setLdtResult(List ldtResult) {
		this.ldtResult = ldtResult;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(String licenseId) {
		this.licenseId = licenseId;
	}

	public ArrayList getAlCountries() {
		return alCountries;
	}

	public void setAlCountries(ArrayList alCountries) {
		this.alCountries = alCountries;
	}

	public ArrayList getAlStates() {
		return alStates;
	}

	public void setAlStates(ArrayList alStates) {
		this.alStates = alStates;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPartyId() {
		return partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getIssueMonth() {
		return issueMonth;
	}

	public void setIssueMonth(String issueMonth) {
		this.issueMonth = issueMonth;
	}

	public String getIssueYear() {
		return issueYear;
	}

	public void setIssueYear(String issueYear) {
		this.issueYear = issueYear;
	}

	public ArrayList getAlIssueMonth() {
		return alIssueMonth;
	}

	public void setAlIssueMonth(ArrayList alIssueMonth) {
		this.alIssueMonth = alIssueMonth;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public ArrayList getAlMonth() {
		return alMonth;
	}

	public void setAlMonth(ArrayList alMonth) {
		this.alMonth = alMonth;
	}

}
