/**
 * 
 */
package com.globus.party.forms;

import java.util.ArrayList;

public class GmAddressSetupForm extends GmPartySetupForm {
  private static final long serialVersionUID = 1L;


  private ArrayList alNames = new ArrayList();
  private ArrayList alAddressTypes = new ArrayList();
  private ArrayList alPreferences = new ArrayList();
  private ArrayList alStates = new ArrayList();
  private ArrayList alCountries = new ArrayList();
  private ArrayList alCarriers = new ArrayList();

  private String billAdd1 = "";
  private String billAdd2 = "";
  private String billCity = "";
  private String zipCode = "";
  private String activeFlag = "";
  private String primaryFlag = "";

  private String addressType = "";
  private String preference = "";
  private String state = "";
  private String country = "";
  private String carrier = "";
  private String haddressID = "";
  private String transactPage = "";
  private String addIdReturned = ""; // added this parameter to be returned back on the opening jsp.

  private String refID = "";
  private String repID = "";
  private String oldAddID = "";
  private String fromStrOpt = "";
  private String shipMode = "";
  private String caseDistID = "";
  private String caseRepID = "";
  private String caseAccID = "";

  private String hActiveFlag = "";
  private String hAddrId = "";
  private String primaryStsFl = "";

  private String hcount = "";
  private String billname = "";
  private String dealerFlag = "";
  /**
   * PMT-28307 Access Driven Sales Rep Setup 
   */
  private String editAccessFl ="";

  public String getEditAccessFl() {
	return editAccessFl;
}

public void setEditAccessFl(String editAccessFl) {
	this.editAccessFl = editAccessFl;
}

/**
   * @return the hAddrId
   */
  public String gethAddrId() {
    return hAddrId;
  }

  /**
   * @param hAddrId the hAddrId to set
   */
  public void sethAddrId(String hAddrId) {
    this.hAddrId = hAddrId;
  }

  public String gethActiveFlag() {
    return hActiveFlag;
  }

  public void sethActiveFlag(String hActiveFlag) {
    this.hActiveFlag = hActiveFlag;
  }

  public String getCaseDistID() {
    return caseDistID;
  }

  public void setCaseDistID(String caseDistID) {
    this.caseDistID = caseDistID;
  }

  public String getCaseRepID() {
    return caseRepID;
  }

  public void setCaseRepID(String caseRepID) {
    this.caseRepID = caseRepID;
  }

  public String getCaseAccID() {
    return caseAccID;
  }

  public void setCaseAccID(String caseAccID) {
    this.caseAccID = caseAccID;
  }

  public String getShipMode() {
    return shipMode;
  }

  public void setShipMode(String shipMode) {
    this.shipMode = shipMode;
  }

  public String getOldAddID() {
    return oldAddID;
  }

  public void setOldAddID(String oldAddID) {
    this.oldAddID = oldAddID;
  }

  public String getRefID() {
    return refID;
  }

  public void setRefID(String refID) {
    this.refID = refID;
  }

  public String getRepID() {
    return repID;
  }

  public void setRepID(String repID) {
    this.repID = repID;
  }

  public String getFromStrOpt() {
    return fromStrOpt;
  }

  public void setFromStrOpt(String fromStrOpt) {
    this.fromStrOpt = fromStrOpt;
  }



  private String hideList = ""; // Added for Party Setup page

  /**
   * Reset the form variables
   */
  @Override
  public void reset() {
    /*
     * billAdd1 = ""; billAdd2 = ""; billCity = ""; zipCode = ""; activeFlag = "";
     * 
     * addressType = ""; preference = ""; state = ""; country = ""; haddressID = "";
     */
  }

  /**
   * @return Returns the activeFlag.
   */
  public String getActiveFlag() {
    return activeFlag;
  }

  /**
   * @param activeFlag The activeFlag to set.
   */
  public void setActiveFlag(String activeFlag) {
    this.activeFlag = activeFlag;
  }

  /**
   * @return Returns the alAddressTypes.
   */
  public ArrayList getAlAddressTypes() {
    return alAddressTypes;
  }

  /**
   * @param alAddressTypes The alAddressTypes to set.
   */
  public void setAlAddressTypes(ArrayList alAddressTypes) {
    this.alAddressTypes = alAddressTypes;
  }

  /**
   * @return Returns the alCountries.
   */
  public ArrayList getAlCountries() {
    return alCountries;
  }

  /**
   * @param alCountries The alCountries to set.
   */
  public void setAlCountries(ArrayList alCountries) {
    this.alCountries = alCountries;
  }

  /**
   * @return Returns the alNames.
   */
  public ArrayList getAlNames() {
    return alNames;
  }

  /**
   * @param alNames The alNames to set.
   */
  public void setAlNames(ArrayList alNames) {
    this.alNames = alNames;
  }

  /**
   * @return Returns the alPreferences.
   */
  public ArrayList getAlPreferences() {
    return alPreferences;
  }

  /**
   * @param alPreferences The alPreferences to set.
   */
  public void setAlPreferences(ArrayList alPreferences) {
    this.alPreferences = alPreferences;
  }

  /**
   * @return Returns the alStates.
   */
  public ArrayList getAlStates() {
    return alStates;
  }

  public ArrayList getAlCarriers() {
    return alCarriers;
  }

  public void setAlCarriers(ArrayList alCarriers) {
    this.alCarriers = alCarriers;
  }

  /**
   * @param alStates The alStates to set.
   */
  public void setAlStates(ArrayList alStates) {
    this.alStates = alStates;
  }

  /**
   * @return Returns the billAdd1.
   */
  public String getBillAdd1() {
    return billAdd1;
  }

  /**
   * @param billAdd1 The billAdd1 to set.
   */
  public void setBillAdd1(String billAdd1) {
    this.billAdd1 = billAdd1;
  }

  /**
   * @return Returns the billAdd2.
   */
  public String getBillAdd2() {
    return billAdd2;
  }

  /**
   * @param billAdd2 The billAdd2 to set.
   */
  public void setBillAdd2(String billAdd2) {
    this.billAdd2 = billAdd2;
  }

  /**
   * @return Returns the billCity.
   */
  public String getBillCity() {
    return billCity;
  }

  /**
   * @param billCity The billCity to set.
   */
  public void setBillCity(String billCity) {
    this.billCity = billCity;
  }

  /**
   * @return Returns the haddressID.
   */
  public String getHaddressID() {
    return haddressID;
  }

  /**
   * @param haddressID The haddressID to set.
   */
  public void setHaddressID(String haddressID) {
    this.haddressID = haddressID;
  }

  /**
   * @return Returns the addressType.
   */
  public String getAddressType() {
    return addressType;
  }

  /**
   * @param addressType The addressType to set.
   */
  public void setAddressType(String addressType) {
    this.addressType = addressType;
  }

  /**
   * @return Returns the country.
   */
  public String getCountry() {
    return country;
  }

  /**
   * @param country The country to set.
   */
  public void setCountry(String country) {
    this.country = country;
  }

  /**
   * @return the carrier
   */
  public String getCarrier() {
    return carrier;
  }

  /**
   * @param carrier the carrier to set
   */
  public void setCarrier(String carrier) {
    this.carrier = carrier;
  }

  /**
   * @return Returns the preference.
   */
  public String getPreference() {
    return preference;
  }

  /**
   * @param preference The preference to set.
   */
  public void setPreference(String preference) {
    this.preference = preference;
  }

  /**
   * @return Returns the state.
   */
  public String getState() {
    return state;
  }

  /**
   * @param state The state to set.
   */
  public void setState(String state) {
    this.state = state;
  }

  /**
   * @return Returns the zipCode.
   */
  public String getZipCode() {
    return zipCode;
  }

  /**
   * @param zipCode The zipCode to set.
   */
  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  @Override
  public String toString() {
    return "[ Party ID :" + getPartyId() + ", Address Type :" + getAddressType() + ", Address1 :"
        + getBillAdd1() + ", Address2 : " + getBillAdd2() + ", City : " + getBillCity() + ", Zip :"
        + getZipCode() + ", Active Flag :" + getActiveFlag() + "]";
  }

  public String getHideList() {
    return hideList;
  }

  public void setHideList(String hideList) {
    this.hideList = hideList;
  }

  /**
   * @return the primaryFlag
   */
  public String getPrimaryFlag() {
    return primaryFlag;
  }

  /**
   * @param primaryFlag the primaryFlag to set
   */
  public void setPrimaryFlag(String primaryFlag) {
    this.primaryFlag = primaryFlag;
  }

  /**
   * @return the transactPage
   */
  public String getTransactPage() {
    return transactPage;
  }

  /**
   * @param transactPage the transactPage to set
   */
  public void setTransactPage(String transactPage) {
    this.transactPage = transactPage;
  }

  /**
   * @return the addIdReturned
   */
  public String getAddIdReturned() {
    return addIdReturned;
  }

  /**
   * @param addIdReturned the addIdReturned to set
   */
  public void setAddIdReturned(String addIdReturned) {
    this.addIdReturned = addIdReturned;
  }

  public String getPrimaryStsFl() {
    return primaryStsFl;
  }

  public void setPrimaryStsFl(String primaryStsFl) {
    this.primaryStsFl = primaryStsFl;
  }

  /**
   * @return the hcount
   */
  public String getHcount() {
    return hcount;
  }

  /**
   * @param hcount the hcount to set
   */
  public void setHcount(String hcount) {
    this.hcount = hcount;
  }

  /**
   * @return the billname
   */
  public String getBillname() {
    return billname;
  }

  /**
   * @param billname the billname to set
   */
  public void setBillname(String billname) {
    this.billname = billname;
  }

  /**
   * @return the dealerFlag
   */
  public String getDealerFlag() {
    return dealerFlag;
  }

  /**
   * @param dealerFlag the dealerFlag to set
   */
  public void setDealerFlag(String dealerFlag) {
    this.dealerFlag = dealerFlag;
  }


}
