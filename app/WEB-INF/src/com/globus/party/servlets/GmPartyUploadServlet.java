package com.globus.party.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadException;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmUploadServlet;

public class GmPartyUploadServlet extends GmUploadServlet {
	private static String strDefaultFileToDispatch = "";
	private static String strErrorFileToDispatch = "";
	private String strFileToDispatch = "";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	private static String strComnPath = GmCommonClass.getString("GMCOMMON");

	public void loadPartyProperties(String strPartyType, String strPartyId) {
		String strWhiteList = "";
		setStrUploadHome(GmCommonClass.getString("PARTYUPLOADHOME"));
		if (strPartyType.equals("91181")) {
			setStrFileName(strPartyId + "_CV");
			strWhiteList = GmCommonClass.getString("PARTYCVWHITELIST");
			setStrWhitelist(strWhiteList);
			//setStrWhitelist("*.doc,*.pdf");//TODO take from Constants.properties
			log.debug("Allowed formats for CV = " + strWhiteList);
		} else {
			setStrFileName(strPartyId + "_Photo");
			strWhiteList = GmCommonClass.getString("PARTYIMAGEWHITELIST");
			setStrWhitelist(strWhiteList);
			log.debug("Allowed formats for Image = " + strWhiteList);
		}

		strDefaultFileToDispatch = "/party/GmPartyUpload.jsp";
		strErrorFileToDispatch = strComnPath + "/GmError.jsp";
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		String strAction = "";
		MultipartFormDataRequest mrequest = null;
		checkSession(response, session); // Checks if the current session is
		// valid, else redirecting to
		// SessionExpiry page

		try {
			GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
			String strPartyType = "";
			String strPartyId = "";
			ArrayList alUploadinfo = new ArrayList();
			ArrayList alPartyList = new ArrayList();

			if (MultipartFormDataRequest.isMultipartFormData(request)) {
				// creating MultiPart request
				mrequest = new MultipartFormDataRequest(request);
				strAction = GmCommonClass.parseNull((String) mrequest.getParameter("hAction"));
				strPartyId = GmCommonClass.parseNull((String) mrequest.getParameter("partyId"));
				strPartyType = GmCommonClass.parseNull((String) mrequest.getParameter("strPartyList"));
				
				log.debug("strAction: "+ strAction+ " strPartyId: "+ strPartyId+ " strPartyType: "+ strPartyType);
			}

			if (strAction.equals("upload")) {
				// Upload the file on server
				loadPartyProperties(strPartyType, strPartyId);

				uploadFile(mrequest, request);
				// Saving the file info & values in database
				gmCommonUploadBean.saveUploadUniqueInfo(strPartyId, strPartyType, getStrFileName(), (String) session.getAttribute("strSessUserId"));
			}
			
			alUploadinfo = gmCommonUploadBean.fetchUploadInfo(strPartyType, strPartyId);
			alPartyList = GmCommonClass.getCodeList("PTYFLT");

			request.setAttribute("mrequest", mrequest);
			request.setAttribute("hAction", strAction);
			request.setAttribute("alPartyList", alPartyList);
			request.setAttribute("strPartyList", strPartyType);
			request.setAttribute("partyId", strPartyId);
			request.setAttribute("alUploadinfo", alUploadinfo);
			// upload page if load or reload
			dispatch("/party/GmPartyUpload.jsp", request, response);
		} catch (AppError e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			request.setAttribute("hType", e.getType());
			dispatch(strFileToDispatch, request, response);
		} catch (UploadException e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		} catch (Exception uex) {
			session.setAttribute("hAppErrorMessage", uex.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		}
	}
}
