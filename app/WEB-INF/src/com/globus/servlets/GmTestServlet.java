/*
 * Module:       GmTestServlet.java
 * Author:       Dhinakaran James
 * Project:      Globus Medical App
 * Date-Written: 11 Mar 2004
 * Security:     Unclassified
 * Description:  Test Servlet
 *
 * Revision Log (mm/dd/yy initials description)
 * ---------------------------------------------------------
 * mm/dd/yy xxx  What you changed
 *
 */
package com.globus.servlets;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.beans.GmTestBean;

public class GmTestServlet extends HttpServlet
{

	/**
	* service  - The HttpServlet's service method which is invoked
	*		   - whenever this servlet is requested.
	* @param HttpServletRequest  request  - The servlet's Request object
	* @param HttpServletResponse response - The servlet's Response object
	* @return void
	* @exception javax.servlet.ServletException, java.util.IOException
	*/
	public void service(HttpServletRequest request,	HttpServletResponse response)
				throws ServletException, IOException
	{
		HttpSession session	= request.getSession(false);
		try
		{
		    String strDispatchTo = "/Login.jsp";
			
			GmTestBean gmTestBean = new GmTestBean();
			String strMsg = gmTestBean.getFromBean();

			request.setAttribute("msg",strMsg);
			getServletContext().getRequestDispatcher(strDispatchTo).include(request,response);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}