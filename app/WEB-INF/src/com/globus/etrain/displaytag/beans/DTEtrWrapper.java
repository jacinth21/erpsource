package com.globus.etrain.displaytag.beans;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTEtrWrapper extends TableDecorator {
	private HashMap hmParam = new HashMap();
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	Logger log = GmLogger.getInstance(this.getClass().getName());
	String strRes = "";
	int i = 0;
	
	/**
	* Creates a new Wrapper decorator who's job is to reformat some of the data located in our TestObject's.
	*/
	public DTEtrWrapper()
	{
		super();
	}
	
	/*
	 * Used in GmEtrSesnMatMap.jsp
	 * To be made common for...
	 * -getCSDEPTSEQ
	 * -getUSERSEQ 
	 * edit_icon.gif
	 */
	public String getPENDING()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strId = hmParam.get("USERSEQ").toString();
		String strPending = hmParam.get("PENDING").toString();
		String strModUsrId = hmParam.get("MODUSRID").toString();
		String strModuleId = hmParam.get("MODID").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<a href=javascript:fnOpenUsrMatPage('"+strModUsrId+"','"+strModuleId+"')>"+strPending+"</a>");
		strRes = strBufVal.toString(); 
		return strRes;
	}
	public String getSEQ()  
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strSeq = hmParam.get("SEQ").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<img style='cursor:hand' src='");
		strBufVal.append(strImagePath+"/edit.jpg'");
   		strBufVal.append(" title='Click to Edit Mapping Details' width='39' height='19'" ); 
	   	strBufVal.append(" onClick =fnEdtSeq('"+strSeq+"'); </img> ");
   		strRes = strBufVal.toString(); 
		
		//return  "<a href= javascript:fnViewInv('" + strPO + "','" + strId + "')>" + "Y" + "</a>" ; ,'" +strSessNm+ "','" + 2 + "','" + 2 + "','" + 2 + "','" + 2 + "' ----  "','"+strSessNm+"','"+strSessStartDt+"','"+strSessEndDt+"','"+strSessOwner+"','"+strSessStatus+
 		return strRes;
	}
	
	/*
	 * Used in GmIncludeModules.jsp
	 */
	public String getEDTID()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strSessModSeq = hmParam.get("SESSMODSEQ").toString();
		String strName = hmParam.get("COMNAME").toString();
		String strModId = GmCommonClass.parseNull(hmParam.get("MAPMODULEID").toString());
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<img style='cursor:hand' src='");
		strBufVal.append(strImagePath+"/edit.jpg'");
   		strBufVal.append(" title='Click to Edit Mapping Details' width='39' height='19'" ); 
	   	strBufVal.append(" onClick =fnEdtSessMap('"+strSessModSeq+"'); </img> &nbsp;");
	   	strBufVal.append("<img style='cursor:hand' src='");
		strBufVal.append(strImagePath+"/edit_icon.gif'");
   		strBufVal.append(" title='Click to View Materials Details' width='12' height='17'" ); 
	   	strBufVal.append(" onClick =fnOpenMatForm('"+strSessModSeq+"','"+strModId+"'); </img> &nbsp;");
	   	strBufVal.append(strName);
	   	strBufVal.append("<input type=\"hidden\" name=\"modId1\" value=\""+strModId+"\"/>");
		strBufVal.append("<input type=\"hidden\" value=\""+strModId+"\" name=\"modId2\" id=");
		strBufVal.append(strSessModSeq);
		strRes = strBufVal.toString(); 
		
		//return  "<a href= javascript:fnViewInv('" + strPO + "','" + strId + "')>" + "Y" + "</a>" ; ,'" +strSessNm+ "','" + 2 + "','" + 2 + "','" + 2 + "','" + 2 + "' ----  "','"+strSessNm+"','"+strSessStartDt+"','"+strSessEndDt+"','"+strSessOwner+"','"+strSessStatus+
 		return strRes;
	}
	
	public String getCSDEPTSEQ()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strCourseMapSeq = hmParam.get("CSDEPTSEQ").toString();
		String strName = hmParam.get("COMNAME").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<img style='cursor:hand' src='");
		strBufVal.append(strImagePath+"/edit.jpg'");
   		strBufVal.append(" title='Click to Edit Mapping Details' width='39' height='19'" );
		strBufVal.append(" onClick =\"fnEditCourseMap('"+strCourseMapSeq+"');\" </img> &nbsp;");
		strBufVal.append(strName);
   		strRes = strBufVal.toString(); 
   		
 		return strRes;
	}
	
	/*
	 * Used in GmEtrUserModMap.jsp
	 */
	
	public String getUSERSEQ()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strUsrSeq = hmParam.get("USERSEQ").toString();
		String strName = hmParam.get("MODOWNER").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<img style='cursor:hand' src='");
		strBufVal.append(strImagePath+"/edit.jpg'");
   		strBufVal.append(" title='Click to Edit Mapping Details' width='39' height='19'" ); 
	   	strBufVal.append(" onClick =\"fnEdtUsrMapDtl('"+strUsrSeq+"');\" </img> &nbsp;");
	   	strBufVal.append(strName);
   		strRes = strBufVal.toString(); 
 		return strRes;
 	}
	/*
	 * Used in GmEtrSesnUserInvite.jsp
	 */
	public String getUSRMAPSEQ()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strUsrID = hmParam.get("USRID").toString();
		String strUsrNm = hmParam.get("USERNM").toString();
		String strSeq = hmParam.get("USRMAPSEQ").toString();
		String strUsrStatus = hmParam.get("STATUSID").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<img style='cursor:hand' src='");
		strBufVal.append(strImagePath+"/edit_icon.gif'");
   		strBufVal.append(" title='Click to Void' width='12' height='17'" ); 
	   	strBufVal.append(" onClick =\"fnVoid('"+strUsrID+"','"+strSeq+"');\" </img> ");
		strBufVal.append("<img style='cursor:hand' src='");
		strBufVal.append(strImagePath+"/edit.jpg'");
   		strBufVal.append(" title='Click to Edit Mapping Details' width='39' height='19'" ); 
	   	strBufVal.append(" onClick =\"fnEdtUsrDtl('"+strUsrID+"','"+strUsrNm+"');\" </img> ");
	   	strBufVal.append("<img style='cursor:hand' src='");
		strBufVal.append(strImagePath+"/edit_icon.gif'");
   		strBufVal.append(" title='Click to Change Status' width='12' height='17'" ); 
	   	strBufVal.append(" onClick =\"fnChngUsrStatus('"+strUsrStatus+"','"+strSeq+"','"+strUsrID+"');\" </img> ");
   		strRes = strBufVal.toString(); 
 		return strRes;
	}
	
	
	/*
	 * Used in GmEtrMoodLookup.jsp,  GmEtrUserDashboard.jsp
	 */
	
	
	
	public String getID()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strId = hmParam.get("ID").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<a href=javascript:fnSelect('"+strId+"')>"+strId+"</a>");
		//strBufVal.append("<input type='radio' onClick=fnSelect('"+strId+"'); name='modType'");
		//strBufVal.append("onFocus=changeBgColor(this,'#AACCE8');>&nbsp;");	
		//strBufVal.append(strId);
   		strRes = strBufVal.toString(); 
		return strRes;
	}
	
	/*
	 * Used in GmEtrSesnDtlRpt.jsp
	 */
	
	public String getSESNID()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strId = hmParam.get("SESNID").toString();
		String strOwner = hmParam.get("OWNERID").toString();
		String strSessionStartDate = hmParam.get("SESSIONSTARTDT").toString();
		String strStatus = hmParam.get("STATUSID").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<a href=javascript:fnViewModule('"+strId+"')> <img src='/images/m.gif' height='10' width='10' alt='vmodule'  title='Click to View Related Modules'></img>  </a>" );
		strBufVal.append("<a href=javascript:fnViewUser('"+strId+"')> <img src='/images/u.gif' height='10' width='10' alt='vuser' title='Click to View Related Users'></img>  </a>" );
		strBufVal.append("<a href=javascript:fnViewCourse('"+strId+"')> <img src='/images/p.gif' height='10' width='10' alt='vmodule' title='Click to View Related Programs'></img>  </a>" );
		strBufVal.append("<a href=javascript:fnPrintRecord('"+strId+"','"+strSessionStartDate+"')> <img src='/images/r.gif' height='10' width='10' alt='vmodule' title='Click to View Related Records'></img>  </a>" );
		strBufVal.append("<input type='radio' onClick=\"fnSelect('"+strId+"','"+strOwner+"','"+strStatus+"')\"; name='modType'");
		strBufVal.append("onFocus=changeBgColor(this,'#AACCE8');>&nbsp;");	
		strBufVal.append(strId);
   		strRes = strBufVal.toString(); 
		return strRes;
	}
	/*
	 * Used in GmEtrUserDtlRpt.jsp
	 */
	
	public String getRPTID()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strId = hmParam.get("RPTID").toString();
		String strSesnId = hmParam.get("SESNID").toString();
		String strUsrNm = hmParam.get("USERNM").toString();
		String strTrneeDeptNm = hmParam.get("TRNEEDEPTNAME").toString();
		String strTrneeStartDt = hmParam.get("TRNEESTARTDATE").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<a href=javascript:fnViewModule('"+strId+"')> <img src=/images/m.gif height='10' width='10' alt='vmodule' title='Click to View Related Modules'>  </a>" );
		strBufVal.append("<a href=javascript:fnViewSession('"+strId+"')> <img src=/images/s.gif height='10' width='10' alt='vsession' title='Click to View Related Sessions'>  </a>" );
		strBufVal.append("<a href=javascript:fnViewCourse('"+strId+"')> <img src=/images/p.gif height='10' width='10' alt='vmodule' title='Click to View Related Programs'>  </a>" );
		strBufVal.append("<a> <img src=/images/t.gif height='10' width='10' alt='vmodule' title='Click to View Related Records' onClick=\"fnPrintSummary('"+strId+ "','"+strUsrNm+"','"+strTrneeDeptNm+"','"+strTrneeStartDt+"');\"></img>  </a>" );
		//strBufVal.append("<input type='radio' onClick=fnSelect('"+strId+"','"+strSesnId+"'); name='modType'");
		//strBufVal.append("onFocus=changeBgColor(this,'#AACCE8');>&nbsp;");	
		strBufVal.append(strUsrNm);
   		strRes = strBufVal.toString(); 
		return strRes;
	}
	/*
	 * Used in GmEtrSesnCmpltn.jsp
	 */
	public String getDATE()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strNo = hmParam.get("NO").toString();
		String strSeq = hmParam.get("USERSEQ").toString();
		String strDate = hmParam.get("COMPLTNDT").toString();
		StringBuffer strBufVal = new StringBuffer();
		if ("".equals(strDate))
		{
		strBufVal.append("<input type=\"text\" name=\"compltnDt" + strNo+"\" value=\""+strDate+"\"");		
		strBufVal.append(" size='9' styleClass=\"InputArea\" onFocus=\"fnSelectDate(this.value);\" onBlur=\"changeBgColor(this,'#ffffff');\"");
		strBufVal.append("/>&nbsp;<img id='Img_Date' style='cursor:hand'");	
		strBufVal.append("onclick=\"javascript:show_calendar('frmEtrSesnCmpltn.compltnDt"+strNo+"');\" title='Click to open Calendar'");
		strBufVal.append("src='/images/nav_calendar.gif' border=0 align='absmiddle' height=15 width=18 />&nbsp;");
		strBufVal.append("<input type=\"hidden\" value=\""+strSeq+"\" name=\"ModEndDate\" id=");
		strBufVal.append(strNo);
		strBufVal.append("> &nbsp; ");
		strBufVal.append("<input type=\"hidden\" name=\"compltnFl" + strNo+"\" value=\"\" />");
		}
		else
		{
			strBufVal.append(strDate);
			strBufVal.append("<input type=\"hidden\" name=\"compltnDt" + strNo+"\" value=\""+strDate+"\"/>");
			strBufVal.append("<input type=\"hidden\" name=\"compltnFl" + strNo+"\" value=\"Y\" />");
			strBufVal.append("<input type=\"hidden\" value=\""+strSeq+"\" name=\"ModEndDate\" id=");
			strBufVal.append(strNo);
			strBufVal.append("> &nbsp; ");
		}	
		strRes = strBufVal.toString(); 
		return strRes;
	}
	
	
	public String getUSERID() {
		hmParam = (HashMap) this.getCurrentRowObject();
		String strUserID = hmParam.get("USERID").toString();
		StringBuffer strValue = new StringBuffer();
		strValue.append("<input type=\"checkbox\" value=\"\" name=\"Chk_tagid\" id=");
		strValue.append(strUserID);
		strValue.append("> &nbsp; ");		
		strValue.append(strUserID);
		//i++;
		strRes = strValue.toString();
		return strRes;
	}
	
	
	public String getUSRID() {
		hmParam = (HashMap) this.getCurrentRowObject();
		String strUserID = hmParam.get("USRID").toString();
		StringBuffer strValue = new StringBuffer();
		strValue.append("<input type=\"checkbox\" value=\"\" name=\"Chk_invid\" id=");
		strValue.append(strUserID);
		strValue.append("> &nbsp; ");		
		strValue.append(strUserID);
		//i++;
		strRes = strValue.toString();
		return strRes;
	}
	
	public String getPROGRAMID()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strId = hmParam.get("PROGRAMID").toString();
		String strOwner = hmParam.get("OWNERID").toString();		
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<a href=javascript:fnViewSession('"+strId+"')> <img src=/images/s.gif height='10' width='10' alt='vsession' title='Click to View Related Sessions'>  </a>" );
		strBufVal.append("<a href=javascript:fnViewUser('"+strId+"')> <img src=/images/u.gif height='10' width='10' alt='vuser' title='Click to View Related Users'>  </a>" );
		strBufVal.append("<a href=javascript:fnViewModule('"+strId+"')> <img src=/images/m.gif height='10' width='10' alt='vmodule' title='Click to View Related Modules'>  </a>" );
		strBufVal.append("<input type='radio' onClick=\"fnSelect('"+strId+"','"+strOwner+"')\"; name='modType'");
		strBufVal.append("onFocus=changeBgColor(this,'#AACCE8');>&nbsp;");	
		strBufVal.append(strId);
   		strRes = strBufVal.toString(); 
		return strRes;
	}
	
	public String getMODULEID()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strId = hmParam.get("MODULEID").toString();
		String strOwner = hmParam.get("MODULEOWNERID").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<a href=javascript:fnViewCourse('"+strId+"')> <img src=/images/p.gif height='10' width='10' alt='vmodule' title='Click to View Related Programs'>  </a>" );
		strBufVal.append("<a href=javascript:fnViewSession('"+strId+"')> <img src=/images/s.gif height='10' width='10' alt='vsession' title='Click to View Related Sessions'>  </a>" );
		strBufVal.append("<a href=javascript:fnViewUser('"+strId+"')> <img src=/images/u.gif height='10' width='10' alt='vuser' title='Click to View Related Users'>  </a>" );	
		strBufVal.append("<input type='radio' onClick=\"fnSelect('"+strId+"','"+strOwner+"')\"; name='modType'");		
		strBufVal.append("onFocus=changeBgColor(this,'#AACCE8');>&nbsp;");	
		strBufVal.append(strId);
   		strRes = strBufVal.toString(); 
		return strRes;
	}
	
	public String getHLINK()
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strId = hmParam.get("MATID").toString();
		String strFileNm = hmParam.get("FILENM").toString();
		String strSessUserMatId = hmParam.get("USERMATID").toString();
		String strCmpltnDt = GmCommonClass.parseNull(hmParam.get("MATCMPLTNDT").toString());		
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<a> <img src=/images/edit_icon.gif height='17' width='12' title='Click to View Material' onClick=\"fnOpenForm('"+strId+ "','"+strFileNm+"','"+strSessUserMatId+"','"+strCmpltnDt+"');\"></img></a>&nbsp;" );
		strBufVal.append(strFileNm);
		//strBufVal.append("<a href=javascript:fnOpenForm('"+strId+"','"+strFileNm+"');>"+strFileNm+"</a>" );
		strRes = strBufVal.toString(); 
		return strRes;
	}
	
	/*Used for GmEtrUserMatMap.jsp*/
	public String getMAT() {
		hmParam = (HashMap) this.getCurrentRowObject();
		String strID = hmParam.get("MATID").toString();
		String strNm = hmParam.get("MATNM").toString();
		String strType = hmParam.get("REFTYPEID").toString();
		String strMatStat = hmParam.get("MATSTAT").toString();
		StringBuffer strValue = new StringBuffer();
		strValue.append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" onClick=\"fnChkStat(this,'"+strMatStat+"')\"; value=\"\" name=\"Chk_matid\" id=");
		strValue.append(strID);
		strValue.append("> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ");
		strValue.append("<input type=\"checkbox\" value=\"\" name=\"Chk_matid1\" id=");
		strValue.append(strID);
		strValue.append(">");
		strValue.append(strNm);
		strValue.append("<input type=\"hidden\" name=\"reftype" + strID+"\" value=\""+strType+"\"/>");
		//i++;
		strRes = strValue.toString();
		return strRes;
	}
	
	
	
}
