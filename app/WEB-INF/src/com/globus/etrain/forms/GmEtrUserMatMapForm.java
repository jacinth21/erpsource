/**
 * FileName    : GmEtrUserMatMapForm.java 
 * Description :
 * Author      : rshah
 * Date        : Dec 17, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 * 
 */
public class GmEtrUserMatMapForm extends GmCommonForm {
	private String moduleId = "";
	private String sessionId = "";
	private String moduleName = "";
	private String ownername = "";
	private String sessionownernm = "";
	private String fileNm ="";
	private String fileId="";
	private String sesnUserMatId="";
	private String userMatTrackId="";
	private String trcmpltn="";
	private String matCmpltnDt = "";
	private String hinputString = "";
	private String hinputString1 = "";	
	private String flag = "";
	private String sesnOwner = "";
	private String moduleUserId = "";
	private ArrayList alUserMatMapList;
	private ArrayList alMatList;

	/**
	 * @return the moduleId
	 */
	public String getModuleId() {
		return moduleId;
	}

	/**
	 * @param moduleId
	 *            the moduleId to set
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId
	 *            the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the alUserMatMapList
	 */
	public ArrayList getAlUserMatMapList() {
		return alUserMatMapList;
	}

	/**
	 * @param alUserMatMapList
	 *            the alUserMatMapList to set
	 */
	public void setAlUserMatMapList(ArrayList alUserMatMapList) {
		this.alUserMatMapList = alUserMatMapList;
	}

	/**
	 * @return the moduleName
	 */
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * @param moduleName
	 *            the moduleName to set
	 */
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * @return the ownername
	 */
	public String getOwnername() {
		return ownername;
	}

	/**
	 * @param ownername
	 *            the ownername to set
	 */
	public void setOwnername(String ownername) {
		this.ownername = ownername;
	}

	/**
	 * @return the sessionownernm
	 */
	public String getSessionownernm() {
		return sessionownernm;
	}

	/**
	 * @param sessionownernm
	 *            the sessionownernm to set
	 */
	public void setSessionownernm(String sessionownernm) {
		this.sessionownernm = sessionownernm;
	}

	/**
	 * @return the fileNm
	 */
	public String getFileNm() {
		return fileNm;
	}

	/**
	 * @param fileNm the fileNm to set
	 */
	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}

	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}

	/**
	 * @param fileId the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	

	/**
	 * @return the sesnUserMatId
	 */
	public String getSesnUserMatId() {
		return sesnUserMatId;
	}

	/**
	 * @param sesnUserMatId the sesnUserMatId to set
	 */
	public void setSesnUserMatId(String sesnUserMatId) {
		this.sesnUserMatId = sesnUserMatId;
	}

	/**
	 * @return the userMatTrackId
	 */
	public String getUserMatTrackId() {
		return userMatTrackId;
	}

	/**
	 * @param userMatTrackId the userMatTrackId to set
	 */
	public void setUserMatTrackId(String userMatTrackId) {
		this.userMatTrackId = userMatTrackId;
	}

	/**
	 * @return the trcmpltn
	 */
	public String getTrcmpltn() {
		return trcmpltn;
	}

	/**
	 * @param trcmpltn the trcmpltn to set
	 */
	public void setTrcmpltn(String trcmpltn) {
		this.trcmpltn = trcmpltn;
	}

	/**
	 * @return the alMatList
	 */
	public ArrayList getAlMatList() {
		return alMatList;
	}

	/**
	 * @param alMatList the alMatList to set
	 */
	public void setAlMatList(ArrayList alMatList) {
		this.alMatList = alMatList;
	}

	/**
	 * @return the matCmpltnDt
	 */
	public String getMatCmpltnDt() {
		return matCmpltnDt;
	}

	/**
	 * @param matCmpltnDt the matCmpltnDt to set
	 */
	public void setMatCmpltnDt(String matCmpltnDt) {
		this.matCmpltnDt = matCmpltnDt;
	}

	/**
	 * @return the hinputString
	 */
	public String getHinputString() {
		return hinputString;
	}

	/**
	 * @param hinputString the hinputString to set
	 */
	public void setHinputString(String hinputString) {
		this.hinputString = hinputString;
	}

	/**
	 * @return the flag
	 */
	public String getFlag() {
		return flag;
	}

	/**
	 * @param flag the flag to set
	 */
	public void setFlag(String flag) {
		this.flag = flag;
	}

	/**
	 * @return the hinputString1
	 */
	public String getHinputString1() {
		return hinputString1;
	}

	/**
	 * @param hinputString1 the hinputString1 to set
	 */
	public void setHinputString1(String hinputString1) {
		this.hinputString1 = hinputString1;
	}

	/**
	 * @return the sesnOwner
	 */
	public String getSesnOwner() {
		return sesnOwner;
	}

	/**
	 * @param sesnOwner the sesnOwner to set
	 */
	public void setSesnOwner(String sesnOwner) {
		this.sesnOwner = sesnOwner;
	}

	/**
	 * @return the moduleUserId
	 */
	public String getModuleUserId() {
		return moduleUserId;
	}

	/**
	 * @param moduleUserId the moduleUserId to set
	 */
	public void setModuleUserId(String moduleUserId) {
		this.moduleUserId = moduleUserId;
	}
	
}
