/**
 * FileName    : GmEtrUserModMapForm.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 14, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 *
 */
public class GmEtrUserModMapForm extends GmCommonForm{
	private String userSeq="";
	private String sessionId="";
	private String sessionStartDt="";
	private String sessionEndDt="";
	private String sesnUserId="";
	private String sesnUserNm="";
	private String moduleNm="";
	private String reason="";
	private String modStatus = "";
	private String isRequired = "";
	
	private ArrayList alReasonList;
	private ArrayList alUsrDtlMapList;
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * @return the sessionStartDt
	 */
	public String getSessionStartDt() {
		return sessionStartDt;
	}
	/**
	 * @param sessionStartDt the sessionStartDt to set
	 */
	public void setSessionStartDt(String sessionStartDt) {
		this.sessionStartDt = sessionStartDt;
	}
	/**
	 * @return the sessionEndDt
	 */
	public String getSessionEndDt() {
		return sessionEndDt;
	}
	/**
	 * @param sessionEndDt the sessionEndDt to set
	 */
	public void setSessionEndDt(String sessionEndDt) {
		this.sessionEndDt = sessionEndDt;
	}
	/**
	 * @return the sesnUserId
	 */
	public String getSesnUserId() {
		return sesnUserId;
	}
	/**
	 * @param sesnUserId the sesnUserId to set
	 */
	public void setSesnUserId(String sesnUserId) {
		this.sesnUserId = sesnUserId;
	}
	/**
	 * @return the sesnUserNm
	 */
	public String getSesnUserNm() {
		return sesnUserNm;
	}
	/**
	 * @param sesnUserNm the sesnUserNm to set
	 */
	public void setSesnUserNm(String sesnUserNm) {
		this.sesnUserNm = sesnUserNm;
	}
	/**
	 * @return the moduleNm
	 */
	public String getModuleNm() {
		return moduleNm;
	}
	/**
	 * @param moduleNm the moduleNm to set
	 */
	public void setModuleNm(String moduleNm) {
		this.moduleNm = moduleNm;
	}
	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}
	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	/**
	 * @return the alReasonList
	 */
	public ArrayList getAlReasonList() {
		return alReasonList;
	}
	/**
	 * @param alReasonList the alReasonList to set
	 */
	public void setAlReasonList(ArrayList alReasonList) {
		this.alReasonList = alReasonList;
	}
	/**
	 * @return the alUsrDtlMapList
	 */
	public ArrayList getAlUsrDtlMapList() {
		return alUsrDtlMapList;
	}
	/**
	 * @param alUsrDtlMapList the alUsrDtlMapList to set
	 */
	public void setAlUsrDtlMapList(ArrayList alUsrDtlMapList) {
		this.alUsrDtlMapList = alUsrDtlMapList;
	}
	/**
	 * @return the isRequired
	 */
	public String getIsRequired() {
		return isRequired;
	}
	/**
	 * @param isRequired the isRequired to set
	 */
	public void setIsRequired(String isRequired) {
		this.isRequired = isRequired;
	}
	/**
	 * @return the modStatus
	 */
	public String getModStatus() {
		return modStatus;
	}
	/**
	 * @param modStatus the modStatus to set
	 */
	public void setModStatus(String modStatus) {
		this.modStatus = modStatus;
	}
	/**
	 * @return the userSeq
	 */
	public String getUserSeq() {
		return userSeq;
	}
	/**
	 * @param userSeq the userSeq to set
	 */
	public void setUserSeq(String userSeq) {
		this.userSeq = userSeq;
	}
	
	
}
