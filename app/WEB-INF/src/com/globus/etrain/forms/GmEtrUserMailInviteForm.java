/**
 * FileName    : GmEtrUserMailInviteForm.java 
 * Description :
 * Author      : rshah
 * Date        : Oct 23, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 *
 */
public class GmEtrUserMailInviteForm extends GmCommonForm{
	private String sessionId="";
	private String textReason = "";
	private String toMail="";
	private String ccMail="";
	private String subMail="";
	private String bodyMail="";
	private String strInviteList=""; 
	/**
	 * @return the strInviteList
	 */
	public String getStrInviteList() {
		return strInviteList;
	}
	/**
	 * @param strInviteList the strInviteList to set
	 */
	public void setStrInviteList(String strInviteList) {
		this.strInviteList = strInviteList;
	}
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * @return the textReason
	 */
	public String getTextReason() {
		return textReason;
	}
	/**
	 * @param textReason the textReason to set
	 */
	public void setTextReason(String textReason) {
		this.textReason = textReason;
	}
	/**
	 * @return the toMail
	 */
	public String getToMail() {
		return toMail;
	}
	/**
	 * @param toMail the toMail to set
	 */
	public void setToMail(String toMail) {
		this.toMail = toMail;
	}
	/**
	 * @return the ccMail
	 */
	public String getCcMail() {
		return ccMail;
	}
	/**
	 * @param ccMail the ccMail to set
	 */
	public void setCcMail(String ccMail) {
		this.ccMail = ccMail;
	}
	/**
	 * @return the subMail
	 */
	public String getSubMail() {
		return subMail;
	}
	/**
	 * @param subMail the subMail to set
	 */
	public void setSubMail(String subMail) {
		this.subMail = subMail;
	}
	/**
	 * @return the bodyMail
	 */
	public String getBodyMail() {
		return bodyMail;
	}
	/**
	 * @param bodyMail the bodyMail to set
	 */
	public void setBodyMail(String bodyMail) {
		this.bodyMail = bodyMail;
	}

	
}
