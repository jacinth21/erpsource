/**
 * FileName    : GmEtrModLookupForm.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 12, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 *
 */
public class GmEtrModLookupForm extends GmCommonForm{
	private String moduleId="";
	private String moduleNm="";
	private String moduleOwner="";
	private String courseId="";
	
	private ArrayList alModLookupList;
	private ArrayList alCourseList;
	private ArrayList alModuleOwnerList;
	/**
	 * @return the alModuleOwnerList
	 */
	public ArrayList getAlModuleOwnerList() {
		return alModuleOwnerList;
	}
	/**
	 * @param alModuleOwnerList the alModuleOwnerList to set
	 */
	public void setAlModuleOwnerList(ArrayList alModuleOwnerList) {
		this.alModuleOwnerList = alModuleOwnerList;
	}
	/**
	 * @return the alCourseList
	 */
	public ArrayList getAlCourseList() {
		return alCourseList;
	}
	/**
	 * @param alCourseList the alCourseList to set
	 */
	public void setAlCourseList(ArrayList alCourseList) {
		this.alCourseList = alCourseList;
	}
	/**
	 * @return the alModLookupList
	 */
	public ArrayList getAlModLookupList() {
		return alModLookupList;
	}
	/**
	 * @param alModLookupList the alModLookupList to set
	 */
	public void setAlModLookupList(ArrayList alModLookupList) {
		this.alModLookupList = alModLookupList;
	}
	/**
	 * @return the moduleId
	 */
	public String getModuleId() {
		return moduleId;
	}
	/**
	 * @param moduleId the moduleId to set
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	/**
	 * @return the moduleNm
	 */
	public String getModuleNm() {
		return moduleNm;
	}
	/**
	 * @param moduleNm the moduleNm to set
	 */
	public void setModuleNm(String moduleNm) {
		this.moduleNm = moduleNm;
	}
	/**
	 * @return the moduleOwner
	 */
	public String getModuleOwner() {
		return moduleOwner;
	}
	/**
	 * @param moduleOwner the moduleOwner to set
	 */
	public void setModuleOwner(String moduleOwner) {
		this.moduleOwner = moduleOwner;
	}
	/**
	 * @return the courseId
	 */
	public String getCourseId() {
		return courseId;
	}
	/**
	 * @param courseId the courseId to set
	 */
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
}
