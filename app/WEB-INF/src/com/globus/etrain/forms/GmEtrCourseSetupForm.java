/**
 * FileName    : GmEtrCourseSetupForm.java 
 * Description :
 * Author      : Angela
 * Date        : Nov 21, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;
import com.globus.common.forms.GmLogForm;

public class GmEtrCourseSetupForm extends GmLogForm {
	private String courseId = "";
	private String courseName = "";
	private String description = "";
	private String ownerName = "";
	private String courseOwner = "";
	private String deptId = "";
	private String courseStatus = "";
	private String statusName = "";

	private ArrayList alCourseList = new ArrayList();
	private ArrayList alCourseOwnerList = new ArrayList();
	private ArrayList alCourseDeptList = new ArrayList();
	private ArrayList alStatusList = new ArrayList();

	public String getCourseId() {
		return courseId;
	}

	public void reset() {
		courseId = "";
		courseName = "";
		description = "";
		courseOwner = "";
		courseStatus = "";

	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getCourseStatus() {
		return courseStatus;
	}

	public void setCourseStatus(String courseStatus) {
		this.courseStatus = courseStatus;
	}

	public ArrayList getAlCourseList() {
		return alCourseList;
	}

	public void setAlCourseList(ArrayList alCourseList) {
		this.alCourseList = alCourseList;
	}

	public ArrayList getAlCourseOwnerList() {
		return alCourseOwnerList;
	}

	public void setAlCourseOwnerList(ArrayList alCourseOwnerList) {
		this.alCourseOwnerList = alCourseOwnerList;
	}

	public ArrayList getAlCourseDeptList() {
		return alCourseDeptList;
	}

	public void setAlCourseDeptList(ArrayList alCourseDeptList) {
		this.alCourseDeptList = alCourseDeptList;
	}

	public ArrayList getAlStatusList() {
		return alStatusList;
	}

	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getCourseOwner() {
		return courseOwner;
	}

	public void setCourseOwner(String courseOwner) {
		this.courseOwner = courseOwner;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
}
