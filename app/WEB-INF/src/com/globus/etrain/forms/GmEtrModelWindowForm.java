/**
 * FileName    : GmEtrModelWindowForm.java 
 * Description :
 * Author      : rshah
 * Date        : Jan 27, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 *
 */
public class GmEtrModelWindowForm extends GmCommonForm{
	private String fileId = "";
	private String fileNm = "";
	private String sessionId = "";
	private String moduleId = "";
	private String matCmpltnDt = "";
	private String sesnUserMatId = "";
	private String userMatTrackId = "";
	private String actionFlag = "";
	private String trcmpltn ="";
	/**
	 * @return the trcmpltn
	 */
	public String getTrcmpltn() {
		return trcmpltn;
	}
	/**
	 * @param trcmpltn the trcmpltn to set
	 */
	public void setTrcmpltn(String trcmpltn) {
		this.trcmpltn = trcmpltn;
	}
	/**
	 * @return the actionFlag
	 */
	public String getActionFlag() {
		return actionFlag;
	}
	/**
	 * @param actionFlag the actionFlag to set
	 */
	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * @return the moduleId
	 */
	public String getModuleId() {
		return moduleId;
	}
	/**
	 * @param moduleId the moduleId to set
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	/**
	 * @return the fileId
	 */
	public String getFileId() {
		return fileId;
	}
	/**
	 * @param fileId the fileId to set
	 */
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	/**
	 * @return the fileNm
	 */
	public String getFileNm() {
		return fileNm;
	}
	/**
	 * @param fileNm the fileNm to set
	 */
	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}
	/**
	 * @return the matCmpltnDt
	 */
	public String getMatCmpltnDt() {
		return matCmpltnDt;
	}
	/**
	 * @param matCmpltnDt the matCmpltnDt to set
	 */
	public void setMatCmpltnDt(String matCmpltnDt) {
		this.matCmpltnDt = matCmpltnDt;
	}
	/**
	 * @return the sesnUserMatId
	 */
	public String getSesnUserMatId() {
		return sesnUserMatId;
	}
	/**
	 * @param sesnUserMatId the sesnUserMatId to set
	 */
	public void setSesnUserMatId(String sesnUserMatId) {
		this.sesnUserMatId = sesnUserMatId;
	}
	/**
	 * @return the userMatTrackId
	 */
	public String getUserMatTrackId() {
		return userMatTrackId;
	}
	/**
	 * @param userMatTrackId the userMatTrackId to set
	 */
	public void setUserMatTrackId(String userMatTrackId) {
		this.userMatTrackId = userMatTrackId;
	}
}
