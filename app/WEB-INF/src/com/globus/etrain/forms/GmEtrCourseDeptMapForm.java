/**
 * FileName    : GmEtrCourseDeptMapForm.java 
 * Description :
 * Author      : rshah
 * Date        : Dec 2, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import com.globus.common.forms.GmCommonForm;
import java.util.ArrayList;

/**
 * @author rshah
 * 
 */
public class GmEtrCourseDeptMapForm extends GmCommonForm {
	private String courseMapId = "";
	private String deptMapId = "";
	private String mandFlType = "";
	private String courseMapSeq = "";
	private String courseName = "";
	private ArrayList alTypeList = new ArrayList();
	private ArrayList alDeptList = new ArrayList();

	private ArrayList alCourseDeptMapList = new ArrayList();

	public void reset() {
		deptMapId = "";
		mandFlType = "";
	}

	public String getCourseMapId() {
		return courseMapId;
	}

	public void setCourseMapId(String courseMapId) {
		this.courseMapId = courseMapId;
	}

	public String getDeptMapId() {
		return deptMapId;
	}

	public void setDeptMapId(String deptMapId) {
		this.deptMapId = deptMapId;
	}

	public String getMandFlType() {
		return mandFlType;
	}

	public void setMandFlType(String mandFlType) {
		this.mandFlType = mandFlType;
	}

	public String getCourseMapSeq() {
		return courseMapSeq;
	}

	public void setCourseMapSeq(String courseMapSeq) {
		this.courseMapSeq = courseMapSeq;
	}

	public ArrayList getAlTypeList() {
		return alTypeList;
	}

	public void setAlTypeList(ArrayList alTypeList) {
		this.alTypeList = alTypeList;
	}

	public ArrayList getAlDeptList() {
		return alDeptList;
	}

	public void setAlDeptList(ArrayList alDeptList) {
		this.alDeptList = alDeptList;
	}

	public ArrayList getAlCourseDeptMapList() {
		return alCourseDeptMapList;
	}

	public void setAlCourseDeptMapList(ArrayList alCourseDeptMapList) {
		this.alCourseDeptMapList = alCourseDeptMapList;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

}
