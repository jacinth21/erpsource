/**
 * FileName    : GmEtrModuleSetupForm.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 24, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

/**
 * @author rshah
 * 
 */
public class GmEtrModuleSetupForm extends GmLogForm {

	private String moduleId = "";
	private String moduleName = "";

	private String courseId = "";
	private String courseName = "";
	private String description = "";
	private String ownerName = "";
	private String moduleOwner = "";
	private String moduleStatus = "";
	private String statusName = "";

	private ArrayList alModuleList = new ArrayList();
	private ArrayList alModuleOwnerList = new ArrayList();
	private ArrayList alCourseList = new ArrayList();
	private ArrayList alStatusList = new ArrayList();

	public void reset() {
		setModuleId("");
		setModuleName("");
		setCourseId("");
		setDescription("");
		setModuleOwner("");
		setModuleStatus("");		
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getModuleOwner() {
		return moduleOwner;
	}

	public void setModuleOwner(String moduleOwner) {
		this.moduleOwner = moduleOwner;
	}

	public String getModuleStatus() {
		return moduleStatus;
	}

	public void setModuleStatus(String moduleStatus) {
		this.moduleStatus = moduleStatus;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public ArrayList getAlModuleList() {
		return alModuleList;
	}

	public void setAlModuleList(ArrayList alModuleList) {
		this.alModuleList = alModuleList;
	}

	public ArrayList getAlModuleOwnerList() {
		return alModuleOwnerList;
	}

	public void setAlModuleOwnerList(ArrayList alModuleOwnerList) {
		this.alModuleOwnerList = alModuleOwnerList;
	}

	public ArrayList getAlCourseList() {
		return alCourseList;
	}

	public void setAlCourseList(ArrayList alCourseList) {
		this.alCourseList = alCourseList;
	}

	public ArrayList getAlStatusList() {
		return alStatusList;
	}

	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}

}
