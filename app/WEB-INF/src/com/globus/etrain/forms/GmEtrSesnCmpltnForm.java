/**
 * FileName    : GmEtrSesnCmpltnForm.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 24, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 * 
 */
public class GmEtrSesnCmpltnForm extends GmCommonForm {
	private String sessionId = "";
	private String sessionDesc = "";
	private String sessionStartDt = "";
	private String sessionEndDt = "";
	private String sessionLcnTypeNm = "";
	private String sessionLcn = "";
	private String sessionOwnerNm = "";
	private String hinputString="";
	private String strSesnOwner="";
	private String strTempDate="";
	
	private String sesnUsrMapStatus = "";
	private String feedback = "";
	private String cmpltnDt = "";
	
	
	private ArrayList alUsrDtlMapList;
	/**
	 * @return the alUsrDtlMapList
	 */
	public ArrayList getAlUsrDtlMapList() {
		return alUsrDtlMapList;
	}
	/**
	 * @param alUsrDtlMapList
	 *            the alUsrDtlMapList to set
	 */
	public void setAlUsrDtlMapList(ArrayList alUsrDtlMapList) {
		this.alUsrDtlMapList = alUsrDtlMapList;
	}
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param sessionId
	 *            the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * @return the sessionDesc
	 */
	public String getSessionDesc() {
		return sessionDesc;
	}
	/**
	 * @param sessionDesc
	 *            the sessionDesc to set
	 */
	public void setSessionDesc(String sessionDesc) {
		this.sessionDesc = sessionDesc;
	}
	/**
	 * @return the sessionStartDt
	 */
	public String getSessionStartDt() {
		return sessionStartDt;
	}
	/**
	 * @param sessionStartDt
	 *            the sessionStartDt to set
	 */
	public void setSessionStartDt(String sessionStartDt) {
		this.sessionStartDt = sessionStartDt;
	}
	/**
	 * @return the sessionEndDt
	 */
	public String getSessionEndDt() {
		return sessionEndDt;
	}
	/**
	 * @param sessionEndDt
	 *            the sessionEndDt to set
	 */
	public void setSessionEndDt(String sessionEndDt) {
		this.sessionEndDt = sessionEndDt;
	}

	/**
	 * @return the sessionLcn
	 */
	public String getSessionLcn() {
		return sessionLcn;
	}
	/**
	 * @param sessionLcn
	 *            the sessionLcn to set
	 */
	public void setSessionLcn(String sessionLcn) {
		this.sessionLcn = sessionLcn;
	}

	/**
	 * @return the sesnUsrMapStatus
	 */
	public String getSesnUsrMapStatus() {
		return sesnUsrMapStatus;
	}
	/**
	 * @param sesnUsrMapStatus
	 *            the sesnUsrMapStatus to set
	 */
	public void setSesnUsrMapStatus(String sesnUsrMapStatus) {
		this.sesnUsrMapStatus = sesnUsrMapStatus;
	}
	/**
	 * @return the feedback
	 */
	public String getFeedback() {
		return feedback;
	}
	/**
	 * @param feedback
	 *            the feedback to set
	 */
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	/**
	 * @return the cmpltnDt
	 */
	public String getCmpltnDt() {
		return cmpltnDt;
	}
	/**
	 * @param cmpltnDt
	 *            the cmpltnDt to set
	 */
	public void setCmpltnDt(String cmpltnDt) {
		this.cmpltnDt = cmpltnDt;
	}
	/**
	 * @return the sessionOwnerNm
	 */
	public String getSessionOwnerNm() {
		return sessionOwnerNm;
	}
	/**
	 * @param sessionOwnerNm
	 *            the sessionOwnerNm to set
	 */
	public void setSessionOwnerNm(String sessionOwnerNm) {
		this.sessionOwnerNm = sessionOwnerNm;
	}
	/**
	 * @return the sessionLcnTypeNm
	 */
	public String getSessionLcnTypeNm() {
		return sessionLcnTypeNm;
	}
	/**
	 * @param sessionLcnTypeNm
	 *            the sessionLcnTypeNm to set
	 */
	public void setSessionLcnTypeNm(String sessionLcnTypeNm) {
		this.sessionLcnTypeNm = sessionLcnTypeNm;
	}
	/**
	 * @return the hinputString
	 */
	public String getHinputString() {
		return hinputString;
	}
	/**
	 * @param hinputString the hinputString to set
	 */
	public void setHinputString(String hinputString) {
		this.hinputString = hinputString;
	}
	/**
	 * @return the strSesnOwner
	 */
	public String getStrSesnOwner() {
		return strSesnOwner;
	}
	/**
	 * @param strSesnOwner the strSesnOwner to set
	 */
	public void setStrSesnOwner(String strSesnOwner) {
		this.strSesnOwner = strSesnOwner;
	}
	/**
	 * @return the strTempDate
	 */
	public String getStrTempDate() {
		return strTempDate;
	}
	/**
	 * @param strTempDate the strTempDate to set
	 */
	public void setStrTempDate(String strTempDate) {
		this.strTempDate = strTempDate;
	}
	

}
