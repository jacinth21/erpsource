/**
 * FileName    : GmEtrSesnDtlRptForm.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 18, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 * 
 */
public class GmEtrSesnDtlRptForm extends GmCommonForm {

	private String sessionOwner = "";
	private String moduleId = "";
	private String courseId = "";
	private String sessionStartDt = "";
	private String sessionEndDt = "";
	private String sessionLcnType = "";
	private String status = "";
	private String reason = "";
	private String strSessionId = "";
	private String strSesnOwner = "";
	private String strUserId = "";
	private String strSessionStartDt = "";
	private String strStatus = "";

	private ArrayList alSessionOwnerList;
	private ArrayList alModuleList;
	private ArrayList alCourseList;
	private ArrayList alStatusList;
	private ArrayList alReasonList;
	private ArrayList alSessionLcnTypeList;
	private ArrayList alSesnDtlRptList;

	/**
	 * @return the alSesnDtlRptList
	 */
	public ArrayList getAlSesnDtlRptList() {
		return alSesnDtlRptList;
	}

	/**
	 * @param alSesnDtlRptList
	 *            the alSesnDtlRptList to set
	 */
	public void setAlSesnDtlRptList(ArrayList alSesnDtlRptList) {
		this.alSesnDtlRptList = alSesnDtlRptList;
	}

	/**
	 * @return the sessionOwner
	 */
	public String getSessionOwner() {
		return sessionOwner;
	}

	/**
	 * @param sessionOwner
	 *            the sessionOwner to set
	 */
	public void setSessionOwner(String sessionOwner) {
		this.sessionOwner = sessionOwner;
	}

	/**
	 * @return the moduleId
	 */
	public String getModuleId() {
		return moduleId;
	}

	/**
	 * @param moduleId
	 *            the moduleId to set
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @return the courseId
	 */
	public String getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId
	 *            the courseId to set
	 */
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	/**
	 * @return the sessionStartDt
	 */
	public String getSessionStartDt() {
		return sessionStartDt;
	}

	/**
	 * @param sessionStartDt
	 *            the sessionStartDt to set
	 */
	public void setSessionStartDt(String sessionStartDt) {
		this.sessionStartDt = sessionStartDt;
	}

	/**
	 * @return the sessionEndDt
	 */
	public String getSessionEndDt() {
		return sessionEndDt;
	}

	/**
	 * @param sessionEndDt
	 *            the sessionEndDt to set
	 */
	public void setSessionEndDt(String sessionEndDt) {
		this.sessionEndDt = sessionEndDt;
	}

	/**
	 * @return the sessionLcnType
	 */
	public String getSessionLcnType() {
		return sessionLcnType;
	}

	/**
	 * @param sessionLcnType
	 *            the sessionLcnType to set
	 */
	public void setSessionLcnType(String sessionLcnType) {
		this.sessionLcnType = sessionLcnType;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason
	 *            the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the alSessionOwnerList
	 */
	public ArrayList getAlSessionOwnerList() {
		return alSessionOwnerList;
	}

	/**
	 * @param alSessionOwnerList
	 *            the alSessionOwnerList to set
	 */
	public void setAlSessionOwnerList(ArrayList alSessionOwnerList) {
		this.alSessionOwnerList = alSessionOwnerList;
	}

	/**
	 * @return the alModuleList
	 */
	public ArrayList getAlModuleList() {
		return alModuleList;
	}

	/**
	 * @param alModuleList
	 *            the alModuleList to set
	 */
	public void setAlModuleList(ArrayList alModuleList) {
		this.alModuleList = alModuleList;
	}

	/**
	 * @return the alCourseList
	 */
	public ArrayList getAlCourseList() {
		return alCourseList;
	}

	/**
	 * @param alCourseList
	 *            the alCourseList to set
	 */
	public void setAlCourseList(ArrayList alCourseList) {
		this.alCourseList = alCourseList;
	}

	/**
	 * @return the alStatusList
	 */
	public ArrayList getAlStatusList() {
		return alStatusList;
	}

	/**
	 * @param alStatusList
	 *            the alStatusList to set
	 */
	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}

	/**
	 * @return the alReasonList
	 */
	public ArrayList getAlReasonList() {
		return alReasonList;
	}

	/**
	 * @param alReasonList
	 *            the alReasonList to set
	 */
	public void setAlReasonList(ArrayList alReasonList) {
		this.alReasonList = alReasonList;
	}

	/**
	 * @return the alSessionLcnTypeList
	 */
	public ArrayList getAlSessionLcnTypeList() {
		return alSessionLcnTypeList;
	}

	/**
	 * @param alSessionLcnTypeList
	 *            the alSessionLcnTypeList to set
	 */
	public void setAlSessionLcnTypeList(ArrayList alSessionLcnTypeList) {
		this.alSessionLcnTypeList = alSessionLcnTypeList;
	}

	/**
	 * @return the strSessionId
	 */
	public String getStrSessionId() {
		return strSessionId;
	}

	/**
	 * @param strSessionId
	 *            the strSessionId to set
	 */
	public void setStrSessionId(String strSessionId) {
		this.strSessionId = strSessionId;
	}

	/**
	 * @return the strSesnOwner
	 */
	public String getStrSesnOwner() {
		return strSesnOwner;
	}

	/**
	 * @param strSesnOwner
	 *            the strSesnOwner to set
	 */
	public void setStrSesnOwner(String strSesnOwner) {
		this.strSesnOwner = strSesnOwner;
	}

	/**
	 * @return the strUserId
	 */
	public String getStrUserId() {
		return strUserId;
	}

	/**
	 * @param strUserId
	 *            the strUserId to set
	 */
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}

	public String getStrSessionStartDt() {
		return strSessionStartDt;
	}

	public void setStrSessionStartDt(String strSessionStartDt) {
		this.strSessionStartDt = strSessionStartDt;
	}

	/**
	 * @return the strStatus
	 */
	public String getStrStatus() {
		return strStatus;
	}

	/**
	 * @param strStatus the strStatus to set
	 */
	public void setStrStatus(String strStatus) {
		this.strStatus = strStatus;
	}

}
