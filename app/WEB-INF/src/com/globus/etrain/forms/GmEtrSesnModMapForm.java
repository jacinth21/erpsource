/**
 * FileName    : GmEtrSesnModMapForm.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 11, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 *
 */
public class GmEtrSesnModMapForm extends GmCommonForm{
    private String sessModSeq="";
    private String sessionId="";
    private String moduleId="";
    private String durationHrs="";
    private String details="";
    private String trainerId="";
    private String reason="";
    private String statusMap="";
    private String alLength="";
    
    private ArrayList alTrainerList;
    private ArrayList alReasonList;
    private ArrayList alStatusList;
    private ArrayList alModuleList;
    
    private List sessModMapDT = new ArrayList();
	/**
	 * @return the alModuleList
	 */
	public ArrayList getAlModuleList() {
		return alModuleList;
	}
	/**
	 * @param alModuleList the alModuleList to set
	 */
	public void setAlModuleList(ArrayList alModuleList) {
		this.alModuleList = alModuleList;
	}
	/**
	 * @return the alTrainerList
	 */
	public ArrayList getAlTrainerList() {
		return alTrainerList;
	}
	/**
	 * @param alTrainerList the alTrainerList to set
	 */
	public void setAlTrainerList(ArrayList alTrainerList) {
		this.alTrainerList = alTrainerList;
	}
	/**
	 * @return the alReasonList
	 */
	public ArrayList getAlReasonList() {
		return alReasonList;
	}
	/**
	 * @param alReasonList the alReasonList to set
	 */
	public void setAlReasonList(ArrayList alReasonList) {
		this.alReasonList = alReasonList;
	}
	/**
	 * @return the alStatusList
	 */
	public ArrayList getAlStatusList() {
		return alStatusList;
	}
	/**
	 * @param alStatusList the alStatusList to set
	 */
	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}
	/**
	 * @return the sessModSeq
	 */
	public String getSessModSeq() {
		return sessModSeq;
	}
	/**
	 * @param sessModSeq the sessModSeq to set
	 */
	public void setSessModSeq(String sessModSeq) {
		this.sessModSeq = sessModSeq;
	}
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * @return the moduleId
	 */
	public String getModuleId() {
		return moduleId;
	}
	/**
	 * @param moduleId the moduleId to set
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	/**
	 * @return the durationHrs
	 */
	public String getDurationHrs() {
		return durationHrs;
	}
	/**
	 * @param durationHrs the durationHrs to set
	 */
	public void setDurationHrs(String durationHrs) {
		this.durationHrs = durationHrs;
	}
	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}
	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}
	
	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}
	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	/**
	 * @return the statusMap
	 */
	public String getStatusMap() {
		return statusMap;
	}
	/**
	 * @param statusMap the statusMap to set
	 */
	public void setStatusMap(String statusMap) {
		this.statusMap = statusMap;
	}
	/**
	 * @return the sessModMapDT
	 */
	public List getSessModMapDT() {
		return sessModMapDT;
	}
	/**
	 * @param sessModMapDT the sessModMapDT to set
	 */
	public void setSessModMapDT(List sessModMapDT) {
		this.sessModMapDT = sessModMapDT;
	}
	/**
	 * @return the trainerId
	 */
	public String getTrainerId() {
		return trainerId;
	}
	/**
	 * @param trainerId the trainerId to set
	 */
	public void setTrainerId(String trainerId) {
		this.trainerId = trainerId;
	}
	/**
	 * @return the alLength
	 */
	public String getAlLength() {
		return alLength;
	}
	/**
	 * @param alLength the alLength to set
	 */
	public void setAlLength(String alLength) {
		this.alLength = alLength;
	}
}
