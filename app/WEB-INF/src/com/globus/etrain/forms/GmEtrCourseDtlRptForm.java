/**
 * FileName    : GmEtrCourseDtlRptForm.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 25, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;
import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah/Angela
 * 
 */
public class GmEtrCourseDtlRptForm extends GmCommonForm {

	private String courseId = "";
	private String ownerId = "";
	private String courseDeptId = "";
	private String statusId = "";
	private String courseName = "";
	private String strUserId="";
	private String strSessionId = "";
	private String moduleId = "";
	private String strOwner="";

	private ArrayList alOwners = new ArrayList();
	private ArrayList alDept = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alCourseDetailRpt = new ArrayList();

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getCourseDeptId() {
		return courseDeptId;
	}

	public void setCourseDeptId(String courseDeptId) {
		this.courseDeptId = courseDeptId;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public ArrayList getAlOwners() {
		return alOwners;
	}

	public void setAlOwners(ArrayList alOwners) {
		this.alOwners = alOwners;
	}

	public ArrayList getAlDept() {
		return alDept;
	}

	public void setAlDept(ArrayList alDept) {
		this.alDept = alDept;
	}

	public ArrayList getAlStatus() {
		return alStatus;
	}

	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}

	public ArrayList getAlCourseDetailRpt() {
		return alCourseDetailRpt;
	}

	public void setAlCourseDetailRpt(ArrayList alCourseDetailRpt) {
		this.alCourseDetailRpt = alCourseDetailRpt;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	/**
	 * @return the strUserId
	 */
	public String getStrUserId() {
		return strUserId;
	}

	/**
	 * @param strUserId the strUserId to set
	 */
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}

	/**
	 * @return the strSessionId
	 */
	public String getStrSessionId() {
		return strSessionId;
	}

	/**
	 * @param strSessionId the strSessionId to set
	 */
	public void setStrSessionId(String strSessionId) {
		this.strSessionId = strSessionId;
	}

	/**
	 * @return the moduleId
	 */
	public String getModuleId() {
		return moduleId;
	}

	/**
	 * @param moduleId the moduleId to set
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @return the strOwner
	 */
	public String getStrOwner() {
		return strOwner;
	}

	/**
	 * @param strOwner the strOwner to set
	 */
	public void setStrOwner(String strOwner) {
		this.strOwner = strOwner;
	}

}
