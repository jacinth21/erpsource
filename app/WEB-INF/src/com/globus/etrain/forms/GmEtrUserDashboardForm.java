/**
 * FileName    : GmEtrUserDashboardForm.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 21, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 *
 */
public class GmEtrUserDashboardForm extends GmCommonForm{
	private String strSessionId="";
	private ArrayList alOpenTrainings;

	/**
	 * @return the alOpenTrainings
	 */
	public ArrayList getAlOpenTrainings() {
		return alOpenTrainings;
	}

	/**
	 * @param alOpenTrainings the alOpenTrainings to set
	 */
	public void setAlOpenTrainings(ArrayList alOpenTrainings) {
		this.alOpenTrainings = alOpenTrainings;
	}

	/**
	 * @return the strSessionId
	 */
	public String getStrSessionId() {
		return strSessionId;
	}

	/**
	 * @param strSessionId the strSessionId to set
	 */
	public void setStrSessionId(String strSessionId) {
		this.strSessionId = strSessionId;
	}
	
}
