/**
 * FileName    : GmEtrModuleDtlRptForm.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 26, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import com.globus.common.forms.GmCommonForm;
import java.util.ArrayList;

/**
 * @author rshah/Angela
 * 
 */
public class GmEtrModuleDtlRptForm extends GmCommonForm {
	private String courseId = "";
	private String moduleDeptId = "";
	private String moduleOwnerId = "";
	private String moduleStatusId = "";
	private String moduleName = "";
	private String moduleId = "";
	private String strSessionId = "";
	private String strUserId = "";
	private String strOwner = "";

	private ArrayList alCourseList = new ArrayList();
	private ArrayList alModuleDeptList = new ArrayList();
	private ArrayList alModuleOwnerList = new ArrayList();
	private ArrayList alModuleStatusList = new ArrayList();
	private ArrayList alModuleDtlRptList = new ArrayList();

	public ArrayList getAlModuleDtlRptList() {
		return alModuleDtlRptList;
	}

	public void setAlModuleDtlRptList(ArrayList alModuleDtlRptList) {
		this.alModuleDtlRptList = alModuleDtlRptList;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getModuleDeptId() {
		return moduleDeptId;
	}

	public void setModuleDeptId(String moduleDeptId) {
		this.moduleDeptId = moduleDeptId;
	}

	public String getModuleOwnerId() {
		return moduleOwnerId;
	}

	public void setModuleOwnerId(String moduleOwnerId) {
		this.moduleOwnerId = moduleOwnerId;
	}

	public String getModuleStatusId() {
		return moduleStatusId;
	}

	public void setModuleStatusId(String moduleStatusId) {
		this.moduleStatusId = moduleStatusId;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public ArrayList getAlCourseList() {
		return alCourseList;
	}

	public void setAlCourseList(ArrayList alCourseList) {
		this.alCourseList = alCourseList;
	}

	public ArrayList getAlModuleDeptList() {
		return alModuleDeptList;
	}

	public void setAlModuleDeptList(ArrayList alModuleDeptList) {
		this.alModuleDeptList = alModuleDeptList;
	}

	public ArrayList getAlModuleOwnerList() {
		return alModuleOwnerList;
	}

	public void setAlModuleOwnerList(ArrayList alModuleOwnerList) {
		this.alModuleOwnerList = alModuleOwnerList;
	}

	public ArrayList getAlModuleStatusList() {
		return alModuleStatusList;
	}

	public void setAlModuleStatusList(ArrayList alModuleStatusList) {
		this.alModuleStatusList = alModuleStatusList;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @return the strSessionId
	 */
	public String getStrSessionId() {
		return strSessionId;
	}

	/**
	 * @param strSessionId the strSessionId to set
	 */
	public void setStrSessionId(String strSessionId) {
		this.strSessionId = strSessionId;
	}

	/**
	 * @return the strUserId
	 */
	public String getStrUserId() {
		return strUserId;
	}

	/**
	 * @param strUserId the strUserId to set
	 */
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}

	/**
	 * @return the strOwner
	 */
	public String getStrOwner() {
		return strOwner;
	}

	/**
	 * @param strOwner the strOwner to set
	 */
	public void setStrOwner(String strOwner) {
		this.strOwner = strOwner;
	}
}
