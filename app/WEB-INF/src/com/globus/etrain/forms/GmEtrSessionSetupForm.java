/**
 * FileName    : GmPOCEtrModSessionInfoForm.java 
 * Description :
 * Author      : rshah
 * Date        : Oct 15, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

/**
 * @author rshah
 *
 */
public class GmEtrSessionSetupForm extends GmLogForm{
	private String sessionId="";
	private String sessionDesc="";
	private String sessionStartDt="";
	private String sessionEndDt="";
	private String sessionOwner = "";
	private String sessionLcnType = "";
	private String sessionLcn = "";
	private String duration = "";
	private String minUser = "";
	private String maxUser = "";
	private String status = "";
	private String lcnFlag = "";
	private String sesnlcnval="";
	
	private ArrayList alOwnerList = new ArrayList();
	private ArrayList alStatusList = new ArrayList();
	private ArrayList alSessionLcnList = new ArrayList();
	private ArrayList alSessionLcnVal = new ArrayList();
	
	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}
	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	/**
	 * @return the sessionDesc
	 */
	public String getSessionDesc() {
		return sessionDesc;
	}
	/**
	 * @param sessionDesc the sessionDesc to set
	 */
	public void setSessionDesc(String sessionDesc) {
		this.sessionDesc = sessionDesc;
	}
	/**
	 * @return the sessionStartDt
	 */
	public String getSessionStartDt() {
		return sessionStartDt;
	}
	/**
	 * @param sessionStartDt the sessionStartDt to set
	 */
	public void setSessionStartDt(String sessionStartDt) {
		this.sessionStartDt = sessionStartDt;
	}
	/**
	 * @return the sessionEndDt
	 */
	public String getSessionEndDt() {
		return sessionEndDt;
	}
	/**
	 * @param sessionEndDt the sessionEndDt to set
	 */
	public void setSessionEndDt(String sessionEndDt) {
		this.sessionEndDt = sessionEndDt;
	}
	/**
	 * @return the sessionOwner
	 */
	public String getSessionOwner() {
		return sessionOwner;
	}
	/**
	 * @param sessionOwner the sessionOwner to set
	 */
	public void setSessionOwner(String sessionOwner) {
		this.sessionOwner = sessionOwner;
	}
	/**
	 * @return the sessionLcnType
	 */
	public String getSessionLcnType() {
		return sessionLcnType;
	}
	/**
	 * @param sessionLcnType the sessionLcnType to set
	 */
	public void setSessionLcnType(String sessionLcnType) {
		this.sessionLcnType = sessionLcnType;
	}
	/**
	 * @return the sessionLcn
	 */
	public String getSessionLcn() {
		return sessionLcn;
	}
	/**
	 * @param sessionLcn the sessionLcn to set
	 */
	public void setSessionLcn(String sessionLcn) {
		this.sessionLcn = sessionLcn;
	}
	/**
	 * @return the duration
	 */
	public String getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(String duration) {
		this.duration = duration;
	}
	/**
	 * @return the minUser
	 */
	public String getMinUser() {
		return minUser;
	}
	/**
	 * @param minUser the minUser to set
	 */
	public void setMinUser(String minUser) {
		this.minUser = minUser;
	}
	/**
	 * @return the maxUser
	 */
	public String getMaxUser() {
		return maxUser;
	}
	/**
	 * @param maxUser the maxUser to set
	 */
	public void setMaxUser(String maxUser) {
		this.maxUser = maxUser;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the alOwnerList
	 */
	public ArrayList getAlOwnerList() {
		return alOwnerList;
	}
	/**
	 * @param alOwnerList the alOwnerList to set
	 */
	public void setAlOwnerList(ArrayList alOwnerList) {
		this.alOwnerList = alOwnerList;
	}
	/**
	 * @return the alStatusList
	 */
	public ArrayList getAlStatusList() {
		return alStatusList;
	}
	/**
	 * @param alStatusList the alStatusList to set
	 */
	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}
	/**
	 * @return the alSessionLcnList
	 */
	public ArrayList getAlSessionLcnList() {
		return alSessionLcnList;
	}
	/**
	 * @param alSessionLcnList the alSessionLcnList to set
	 */
	public void setAlSessionLcnList(ArrayList alSessionLcnList) {
		this.alSessionLcnList = alSessionLcnList;
	}
	/**
	 * @return the alSessionLcnVal
	 */
	public ArrayList getAlSessionLcnVal() {
		return alSessionLcnVal;
	}
	/**
	 * @param alSessionLcnVal the alSessionLcnVal to set
	 */
	public void setAlSessionLcnVal(ArrayList alSessionLcnVal) {
		this.alSessionLcnVal = alSessionLcnVal;
	}
	/**
	 * @return the lcnFlag
	 */
	public String getLcnFlag() {
		return lcnFlag;
	}
	/**
	 * @param lcnFlag the lcnFlag to set
	 */
	public void setLcnFlag(String lcnFlag) {
		this.lcnFlag = lcnFlag;
	}
	/**
	 * @return the sesnlcnval
	 */
	public String getSesnlcnval() {
		return sesnlcnval;
	}
	/**
	 * @param sesnlcnval the sesnlcnval to set
	 */
	public void setSesnlcnval(String sesnlcnval) {
		this.sesnlcnval = sesnlcnval;
	}
	
	
	
}
