package com.globus.etrain.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmEtrSesnUserInviteForm extends GmCommonForm {
	private String sessionId="";
	private String sessionDesc="";
	private String sessionStartDt="";
	private String sessionEndDt="";
	private String maxuser="";
	private String sessionLcnType="";
	private String sessionLcnTypeNm="";
	private String sessionOwner="";
	private String sessionOwnerNm="";
	private String fname="";
	private String lname="";
	private String deptList="";
	private String hinputString = "";
	private String hinviteList="";
	private String statusId = "";
	private String userSeq = "";
	private String strUserId = "";
	
	private ArrayList alDeptList;
	private ArrayList userDetailList;
	private ArrayList invitedUserList;

	/**
	 * @return the userDetailList
	 */
	public ArrayList getUserDetailList() {
		return userDetailList;
	}

	/**
	 * @param userDetailList the userDetailList to set
	 */
	public void setUserDetailList(ArrayList userDetailList) {
		this.userDetailList = userDetailList;
	}

	/**
	 * @return the invitedUserList
	 */
	public ArrayList getInvitedUserList() {
		return invitedUserList;
	}

	/**
	 * @param invitedUserList the invitedUserList to set
	 */
	public void setInvitedUserList(ArrayList invitedUserList) {
		this.invitedUserList = invitedUserList;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the sessionDesc
	 */
	public String getSessionDesc() {
		return sessionDesc;
	}

	/**
	 * @param sessionDesc the sessionDesc to set
	 */
	public void setSessionDesc(String sessionDesc) {
		this.sessionDesc = sessionDesc;
	}

	/**
	 * @return the sessionStartDt
	 */
	public String getSessionStartDt() {
		return sessionStartDt;
	}

	/**
	 * @param sessionStartDt the sessionStartDt to set
	 */
	public void setSessionStartDt(String sessionStartDt) {
		this.sessionStartDt = sessionStartDt;
	}

	/**
	 * @return the sessionEndDt
	 */
	public String getSessionEndDt() {
		return sessionEndDt;
	}

	/**
	 * @param sessionEndDt the sessionEndDt to set
	 */
	public void setSessionEndDt(String sessionEndDt) {
		this.sessionEndDt = sessionEndDt;
	}

	/**
	 * @return the maxuser
	 */
	public String getMaxuser() {
		return maxuser;
	}

	/**
	 * @param maxuser the maxuser to set
	 */
	public void setMaxuser(String maxuser) {
		this.maxuser = maxuser;
	}

	/**
	 * @return the sessionOwner
	 */
	public String getSessionOwner() {
		return sessionOwner;
	}

	/**
	 * @param sessionOwner the sessionOwner to set
	 */
	public void setSessionOwner(String sessionOwner) {
		this.sessionOwner = sessionOwner;
	}

	/**
	 * @return the fname
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * @param fname the fname to set
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * @return the lname
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * @param lname the lname to set
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * @return the deptList
	 */
	public String getDeptList() {
		return deptList;
	}

	/**
	 * @param deptList the deptList to set
	 */
	public void setDeptList(String deptList) {
		this.deptList = deptList;
	}

	/**
	 * @return the alDeptList
	 */
	public ArrayList getAlDeptList() {
		return alDeptList;
	}

	/**
	 * @param alDeptList the alDeptList to set
	 */
	public void setAlDeptList(ArrayList alDeptList) {
		this.alDeptList = alDeptList;
	}

	/**
	 * @return the sessionLcnType
	 */
	public String getSessionLcnType() {
		return sessionLcnType;
	}

	/**
	 * @param sessionLcnType the sessionLcnType to set
	 */
	public void setSessionLcnType(String sessionLcnType) {
		this.sessionLcnType = sessionLcnType;
	}

	/**
	 * @return the sessionOwnerNm
	 */
	public String getSessionOwnerNm() {
		return sessionOwnerNm;
	}

	/**
	 * @param sessionOwnerNm the sessionOwnerNm to set
	 */
	public void setSessionOwnerNm(String sessionOwnerNm) {
		this.sessionOwnerNm = sessionOwnerNm;
	}

	/**
	 * @return the sessionLcnTypeNm
	 */
	public String getSessionLcnTypeNm() {
		return sessionLcnTypeNm;
	}

	/**
	 * @param sessionLcnTypeNm the sessionLcnTypeNm to set
	 */
	public void setSessionLcnTypeNm(String sessionLcnTypeNm) {
		this.sessionLcnTypeNm = sessionLcnTypeNm;
	}

	/**
	 * @return the hinputString
	 */
	public String getHinputString() {
		return hinputString;
	}

	/**
	 * @param hinputString the hinputString to set
	 */
	public void setHinputString(String hinputString) {
		this.hinputString = hinputString;
	}

	/**
	 * @return the hinviteList
	 */
	public String getHinviteList() {
		return hinviteList;
	}

	/**
	 * @param hinviteList the hinviteList to set
	 */
	public void setHinviteList(String hinviteList) {
		this.hinviteList = hinviteList;
	}

	/**
	 * @return the statusId
	 */
	public String getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the userSeq
	 */
	public String getUserSeq() {
		return userSeq;
	}

	/**
	 * @param userSeq the userSeq to set
	 */
	public void setUserSeq(String userSeq) {
		this.userSeq = userSeq;
	}

	/**
	 * @return the strUserId
	 */
	public String getStrUserId() {
		return strUserId;
	}

	/**
	 * @param strUserId the strUserId to set
	 */
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}
}
