/**
 * FileName    : GmEtrUserDtlRptForm.java 
 * Description :
 * Author      : rshah
 * Date        : Dec 1, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author rshah
 * 
 */
public class GmEtrUserDtlRptForm extends GmCommonForm {
	private String traineeId = "";
	private String moduleId = "";
	private String courseId = "";
	private String startDt = "";
	private String endDt = "";
	private String status = "";
	private String userDeptId = "";
	private String reason = "";
	private String strUserId = "";
	private String strSessionId = "";
	private String strTraineeName = "";
	private String trneeDeptName = "";
	private String trneeStartDate = "";	

	private ArrayList alReasonList;
	private ArrayList alUserDeptIdList;
	private ArrayList alCourseList;
	private ArrayList alModuleList;
	private ArrayList alTraineeIdList;
	private ArrayList alStatusList;
	private ArrayList alUserDtlRptList;

	
	/**
	 * @return the moduleId
	 */
	public String getModuleId() {
		return moduleId;
	}

	/**
	 * @param moduleId
	 *            the moduleId to set
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	/**
	 * @return the courseId
	 */
	public String getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId
	 *            the courseId to set
	 */
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	/**
	 * @return the startDt
	 */
	public String getStartDt() {
		return startDt;
	}

	/**
	 * @param startDt
	 *            the startDt to set
	 */
	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}

	/**
	 * @return the endDt
	 */
	public String getEndDt() {
		return endDt;
	}

	/**
	 * @param endDt
	 *            the endDt to set
	 */
	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason
	 *            the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the alReasonList
	 */
	public ArrayList getAlReasonList() {
		return alReasonList;
	}

	/**
	 * @param alReasonList
	 *            the alReasonList to set
	 */
	public void setAlReasonList(ArrayList alReasonList) {
		this.alReasonList = alReasonList;
	}

	/**
	 * @return the userDeptId
	 */
	public String getUserDeptId() {
		return userDeptId;
	}

	/**
	 * @param userDeptId
	 *            the userDeptId to set
	 */
	public void setUserDeptId(String userDeptId) {
		this.userDeptId = userDeptId;
	}

	/**
	 * @return the alUserDeptIdList
	 */
	public ArrayList getAlUserDeptIdList() {
		return alUserDeptIdList;
	}

	/**
	 * @param alUserDeptIdList
	 *            the alUserDeptIdList to set
	 */
	public void setAlUserDeptIdList(ArrayList alUserDeptIdList) {
		this.alUserDeptIdList = alUserDeptIdList;
	}

	/**
	 * @return the alCourseIdList
	 */
	public ArrayList getAlCourseList() {
		return alCourseList;
	}

	/**
	 * @param alCourseIdList
	 *            the alCourseIdList to set
	 */
	public void setAlCourseList(ArrayList alCourseList) {
		this.alCourseList = alCourseList;
	}

	/**
	 * @return the alModuleIdList
	 */
	public ArrayList getAlModuleList() {
		return alModuleList;
	}

	/**
	 * @param alModuleIdList
	 *            the alModuleIdList to set
	 */
	public void setAlModuleList(ArrayList alModuleList) {
		this.alModuleList = alModuleList;
	}
	

	/**
	 * @return the alUserDtlRptList
	 */
	public ArrayList getAlUserDtlRptList() {
		return alUserDtlRptList;
	}

	/**
	 * @param alUserDtlRptList
	 *            the alUserDtlRptList to set
	 */
	public void setAlUserDtlRptList(ArrayList alUserDtlRptList) {
		this.alUserDtlRptList = alUserDtlRptList;
	}

	/**
	 * @return the alStatusList
	 */
	public ArrayList getAlStatusList() {
		return alStatusList;
	}

	/**
	 * @param alStatusList
	 *            the alStatusList to set
	 */
	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}

	/**
	 * @return the strUserId
	 */
	public String getStrUserId() {
		return strUserId;
	}

	/**
	 * @param strUserId
	 *            the strUserId to set
	 */
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}

	/**
	 * @return the strSessionId
	 */
	public String getStrSessionId() {
		return strSessionId;
	}

	/**
	 * @param strSessionId
	 *            the strSessionId to set
	 */
	public void setStrSessionId(String strSessionId) {
		this.strSessionId = strSessionId;
	}

	public String getStrTraineeName() {
		return strTraineeName;
	}

	public void setStrTraineeName(String strTraineeName) {
		this.strTraineeName = strTraineeName;
	}

	public String getTrneeDeptName() {
		return trneeDeptName;
	}

	public void setTrneeDeptName(String trneeDeptName) {
		this.trneeDeptName = trneeDeptName;
	}

	public String getTrneeStartDate() {
		return trneeStartDate;
	}

	public void setTrneeStartDate(String trneeStartDate) {
		this.trneeStartDate = trneeStartDate;
	}

	/**
	 * @return the traineeId
	 */
	public String getTraineeId() {
		return traineeId;
	}

	/**
	 * @param traineeId the traineeId to set
	 */
	public void setTraineeId(String traineeId) {
		this.traineeId = traineeId;
	}

	/**
	 * @return the alTraineeIdList
	 */
	public ArrayList getAlTraineeIdList() {
		return alTraineeIdList;
	}

	/**
	 * @param alTraineeIdList the alTraineeIdList to set
	 */
	public void setAlTraineeIdList(ArrayList alTraineeIdList) {
		this.alTraineeIdList = alTraineeIdList;
	}

}
