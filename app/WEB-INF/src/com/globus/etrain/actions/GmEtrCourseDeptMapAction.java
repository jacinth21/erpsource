/**
 * FileName    : GmEtrCourseDeptMapAction.java 
 * Description :
 * Author      : rshah
 * Date        : Dec 2, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.beans.GmEtrTransBean;
import com.globus.etrain.forms.GmEtrCourseDeptMapForm;

/**
 * @author rshah/Angela
 * 
 */
public class GmEtrCourseDeptMapAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrCourseDeptMapForm gmEtrCourseDeptMapForm = (GmEtrCourseDeptMapForm) form;
		gmEtrCourseDeptMapForm.loadSessionParameters(request);

		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		GmEtrTransBean gmEtrTransBean = new GmEtrTransBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrCourseDeptMapForm.getStrOpt();
		String strCourseMapId = GmCommonClass.parseNull(gmEtrCourseDeptMapForm.getCourseMapId());
		String strCourseName = GmCommonClass.parseNull(gmEtrCourseDeptMapForm.getCourseName());

		log.debug("strCourseMapId : " + strCourseMapId);

		String strCourseMapSeq = gmEtrCourseDeptMapForm.getCourseMapSeq();

		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();
		ArrayList alCourseDeptMapList = new ArrayList();	

		hmParam = GmCommonClass.getHashMapFromForm(gmEtrCourseDeptMapForm);

		if (strOpt.equals("save")) {
			String strCourseDeptMapSeq = gmEtrTransBean.saveCourseDeptMapDetail(hmParam);
			gmEtrCourseDeptMapForm.reset();
		} else if (strOpt.equals("loadCourseDtl")) {
			hmValues = gmEtrTransBean.fetchCourseDeptDetail(strCourseMapSeq);
			gmEtrCourseDeptMapForm = (GmEtrCourseDeptMapForm) GmCommonClass.getFormFromHashMap(gmEtrCourseDeptMapForm, hmValues);
		}
		hmValues = gmEtrSetupBean.fetchEtrCourseDetail(strCourseMapId);
		gmEtrCourseDeptMapForm = (GmEtrCourseDeptMapForm) GmCommonClass.getFormFromHashMap(gmEtrCourseDeptMapForm, hmValues);
		gmEtrCourseDeptMapForm.setCourseMapId(gmEtrCourseDeptMapForm.getCourseMapId());
		gmEtrCourseDeptMapForm.setCourseName(gmEtrCourseDeptMapForm.getCourseName());
		gmEtrCourseDeptMapForm.setAlDeptList(GmCommonClass.getCodeList("DEPID"));
		gmEtrCourseDeptMapForm.setAlTypeList(GmCommonClass.getCodeList("CMTYP"));
		alCourseDeptMapList = gmEtrTransBean.fetchCourseDeptMapDetail(gmEtrCourseDeptMapForm.getCourseMapId());
		gmEtrCourseDeptMapForm.setAlCourseDeptMapList(alCourseDeptMapList);
		return mapping.findForward("success");
	}
}
