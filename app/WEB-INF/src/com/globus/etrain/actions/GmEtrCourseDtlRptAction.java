/**
 * FileName    : GmEtrCourseDtlRptAction.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 25, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

/**
 * @author Angela
 * 
 */
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrRptBean;
import com.globus.etrain.forms.GmEtrCourseDtlRptForm;

public class GmEtrCourseDtlRptAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrCourseDtlRptForm gmEtrCourseDtlRptForm = (GmEtrCourseDtlRptForm) form;
		gmEtrCourseDtlRptForm.loadSessionParameters(request);

		GmEtrRptBean gmEtrRptBean = new GmEtrRptBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrCourseDtlRptForm.getStrOpt();

		HashMap hmParam = new HashMap();
		ArrayList alCourseDtlRptList = new ArrayList();
		gmEtrCourseDtlRptForm.setStrUserId((String) request.getParameter("strUserId"));
		gmEtrCourseDtlRptForm.setModuleId((String) request.getParameter("moduleId"));
		gmEtrCourseDtlRptForm.setStrSessionId((String) request.getParameter("strSessionId"));
		
		if (strOpt.equals("load")) {
			hmParam = GmCommonClass.getHashMapFromForm(gmEtrCourseDtlRptForm);
			alCourseDtlRptList = GmCommonClass.parseNullArrayList(gmEtrRptBean.fetchCourseRptDetail(hmParam));
			gmEtrCourseDtlRptForm.setAlCourseDetailRpt(alCourseDtlRptList);
			gmEtrCourseDtlRptForm = (GmEtrCourseDtlRptForm) GmCommonClass.getFormFromHashMap(gmEtrCourseDtlRptForm, hmParam);

		}
		gmCommonBean.loadEmployeeList();
		gmEtrCourseDtlRptForm.setAlOwners(gmCommonBean.getEmployeeList());
		gmEtrCourseDtlRptForm.setAlDept(GmCommonClass.getCodeList("DEPID"));
		gmEtrCourseDtlRptForm.setAlStatus(GmCommonClass.getCodeList("ECRSE"));

		return mapping.findForward("success");
	}
}
