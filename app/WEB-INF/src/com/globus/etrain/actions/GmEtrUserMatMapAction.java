package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.beans.GmEtrTransBean;
import com.globus.etrain.forms.GmEtrUserMatMapForm;

public class GmEtrUserMatMapAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrUserMatMapForm gmEtrUserMatMapForm = (GmEtrUserMatMapForm) form;
		gmEtrUserMatMapForm.loadSessionParameters(request);

		GmEtrTransBean gmEtrTransBean = new GmEtrTransBean();
		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		String strUserMatTrackId = "";

		Logger log = GmLogger.getInstance(this.getClass().getName());

		gmEtrUserMatMapForm.setSessionId(GmCommonClass.parseNull((String) request.getParameter("sessionId")));
		gmEtrUserMatMapForm.setModuleId(GmCommonClass.parseNull((String) request.getParameter("moduleId")));
		gmEtrUserMatMapForm.setFlag(GmCommonClass.parseNull((String)request.getParameter("flag")));
		gmEtrUserMatMapForm.setModuleUserId(GmCommonClass.parseNull((String) request.getParameter("modUserId")));

		HashMap hmValues = new HashMap();
		HashMap hmParam = new HashMap();
		ArrayList alUserMatMapList = new ArrayList();
		ArrayList alMatList = new ArrayList();
		
		hmValues = gmEtrSetupBean.fetchSesnDetail(gmEtrUserMatMapForm.getSessionId());
		gmEtrUserMatMapForm = (GmEtrUserMatMapForm) GmCommonClass.getFormFromHashMap(gmEtrUserMatMapForm, hmValues);
		hmValues = gmEtrSetupBean.fetchEtrModuleDetail(gmEtrUserMatMapForm.getModuleId());
		gmEtrUserMatMapForm = (GmEtrUserMatMapForm) GmCommonClass.getFormFromHashMap(gmEtrUserMatMapForm, hmValues);
		hmParam = GmCommonClass.getHashMapFromForm(gmEtrUserMatMapForm);
		if(!"".equals(GmCommonClass.parseNull(gmEtrUserMatMapForm.getHinputString())) || !"".equals(GmCommonClass.parseNull(gmEtrUserMatMapForm.getHinputString1())))
		{
			gmEtrTransBean.saveMaterialDetail(hmParam);
		}
		
		if(gmEtrUserMatMapForm.getFlag().equals("matList")) // code to display the arraylist of old and new materials.
		{
			alMatList = gmEtrTransBean.fetchMaterialDetail(gmEtrUserMatMapForm.getModuleId(),gmEtrUserMatMapForm.getSessionId());
			log.debug(alMatList);			
		}
		else
		{
			alUserMatMapList = GmCommonClass.parseNullArrayList(gmEtrTransBean.fetchUserMatMapDetail(gmEtrUserMatMapForm.getModuleId(), gmEtrUserMatMapForm
					.getModuleUserId(), gmEtrUserMatMapForm.getSessionId()));			
		}		
		gmEtrUserMatMapForm.setAlMatList(GmCommonClass.parseNullArrayList(alMatList));
		gmEtrUserMatMapForm.setAlUserMatMapList(GmCommonClass.parseNullArrayList(alUserMatMapList));
		return mapping.findForward("success");
	}
}
