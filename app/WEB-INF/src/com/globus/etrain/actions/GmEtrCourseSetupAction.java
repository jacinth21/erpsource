/**
 * FileName    : GmEtrCourseSetupAction.java 
 * Description :
 * Author      : Angela
 * Date        : Nov 21, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.forms.GmEtrCourseSetupForm;

public class GmEtrCourseSetupAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrCourseSetupForm gmEtrCourseSetupForm = (GmEtrCourseSetupForm) form;
		gmEtrCourseSetupForm.loadSessionParameters(request);

		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrCourseSetupForm.getStrOpt();
		String strCourseId = gmEtrCourseSetupForm.getCourseId();
		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();

		ArrayList alLogReasons = new ArrayList();
		hmParam = GmCommonClass.getHashMapFromForm(gmEtrCourseSetupForm);

		if (strOpt.equals("save")) {
			strCourseId = gmEtrSetupBean.saveCourseDetail(hmParam);
			gmEtrCourseSetupForm.setCourseId(strCourseId);
			alLogReasons = gmCommonBean.getLog(strCourseId, "90883");
			gmEtrCourseSetupForm.setAlLogReasons(alLogReasons);
		} else if (strOpt.equals("load")) {
			hmValues = gmEtrSetupBean.fetchEtrCourseDetail(strCourseId);
			alLogReasons = gmCommonBean.getLog(strCourseId, "90883");
			gmEtrCourseSetupForm.setAlLogReasons(alLogReasons);
			gmEtrCourseSetupForm = (GmEtrCourseSetupForm) GmCommonClass.getFormFromHashMap(gmEtrCourseSetupForm, hmValues);
		} else if (strOpt.equals("reset")) {
			gmEtrCourseSetupForm.reset();
		}
		gmCommonBean.loadEmployeeList();
		gmEtrCourseSetupForm.setAlCourseOwnerList(gmCommonBean.getEmployeeList());

		gmEtrCourseSetupForm.setAlCourseList(gmEtrSetupBean.fetchEtrCourseList());
		gmEtrCourseSetupForm.setAlStatusList(GmCommonClass.getCodeList("ECRSE"));
		gmEtrCourseSetupForm = (GmEtrCourseSetupForm) GmCommonClass.getFormFromHashMap(gmEtrCourseSetupForm, hmValues);

		return mapping.findForward("success");
	}
}
