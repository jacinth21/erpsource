/**
 * FileName    : GmEtrModuleSetupAction.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 24, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

/**
 * FileName : GmEtrCourseSetupAction.java Description : Author : Angela Date :
 * Nov 21, 2008 Copyright : Globus Medical Inc
 */

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.forms.GmEtrModuleSetupForm;

public class GmEtrModuleSetupAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		GmEtrModuleSetupForm gmEtrModuleSetupForm = (GmEtrModuleSetupForm) form;
		gmEtrModuleSetupForm.loadSessionParameters(request);

		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrModuleSetupForm.getStrOpt();
		String strModuleId = gmEtrModuleSetupForm.getModuleId();
		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();

		ArrayList alLogReasons = new ArrayList();
		hmParam = GmCommonClass.getHashMapFromForm(gmEtrModuleSetupForm);

		if (strOpt.equals("save")) {
			strModuleId = gmEtrSetupBean.saveModuleDetail(hmParam);
			gmEtrModuleSetupForm.setModuleId(strModuleId);
			alLogReasons = gmCommonBean.getLog(strModuleId, "90884");
			gmEtrModuleSetupForm.setAlLogReasons(alLogReasons);
		} else if (strOpt.equals("load")) {
			hmValues = gmEtrSetupBean.fetchEtrModuleDetail(strModuleId);
			alLogReasons = gmCommonBean.getLog(strModuleId, "90884");
			gmEtrModuleSetupForm.setAlLogReasons(alLogReasons);
			gmEtrModuleSetupForm = (GmEtrModuleSetupForm) GmCommonClass.getFormFromHashMap(gmEtrModuleSetupForm, hmValues);
		} else if (strOpt.equals("reset")) {			
			gmEtrModuleSetupForm.reset();			
		}

		gmCommonBean.loadEmployeeList();
		gmEtrModuleSetupForm.setAlModuleOwnerList(gmCommonBean.getEmployeeList());
		gmEtrModuleSetupForm.setAlCourseList(gmEtrSetupBean.fetchEtrCourseList());
		gmEtrModuleSetupForm.setAlModuleList(gmEtrSetupBean.fetchEtrModList());

		gmEtrModuleSetupForm.setAlStatusList(GmCommonClass.getCodeList("EMODL"));

		gmEtrModuleSetupForm = (GmEtrModuleSetupForm) GmCommonClass.getFormFromHashMap(gmEtrModuleSetupForm, hmValues);
		return mapping.findForward("success");
	}
}
