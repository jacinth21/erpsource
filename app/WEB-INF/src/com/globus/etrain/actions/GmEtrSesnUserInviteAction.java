package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.beans.GmEtrTransBean;
import com.globus.etrain.forms.GmEtrSesnUserInviteForm;

public class GmEtrSesnUserInviteAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrSesnUserInviteForm gmEtrSesnUserInviteForm = (GmEtrSesnUserInviteForm) form;
		gmEtrSesnUserInviteForm.loadSessionParameters(request);

		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		GmEtrTransBean gmEtrTransBean = new GmEtrTransBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrSesnUserInviteForm.getStrOpt();
		String strSessionId = (String) request.getParameter("sessionId");
		String[] strMailId = new String[1];
		String[] strCCMailId = new String[1];

		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();
		ArrayList alUserList = new ArrayList();
		ArrayList alInvitedUserList = new ArrayList();
		
		gmEtrSesnUserInviteForm.setUserDetailList(GmCommonClass.parseNullArrayList(alUserList));

		gmEtrSesnUserInviteForm.setSessionId(strSessionId);
		hmParam = GmCommonClass.getHashMapFromForm(gmEtrSesnUserInviteForm);

		if (strOpt.equals("save")) 
		{
			String strMapSeq = gmEtrTransBean.saveUserSesnMapDetail(hmParam);
		} 
		else if (strOpt.equals("loadUser")) 
		{
			alUserList = gmEtrTransBean.fetchUserDetail(gmEtrSesnUserInviteForm.getFname(), gmEtrSesnUserInviteForm.getLname(), gmEtrSesnUserInviteForm
					.getDeptList());
			gmEtrSesnUserInviteForm.setUserDetailList(GmCommonClass.parseNullArrayList(alUserList));
		}
		else if(strOpt.equals("chngStat"))
		{
			//To do... Prepare a proper subject line after discussing with James/Sam
			gmEtrTransBean.saveSesnStatus("",hmParam);			
			strMailId[0] = "rshah@globusmedical.com";//gmEtrTransBean.fetchUserMailIdDetail(gmEtrSesnUserInviteForm.getStrUserId()+"|");
			//log.debug("...."+gmEtrTransBean.fetchUserMailIdDetail(gmEtrSesnUserInviteForm.getSessionOwner()+"|")+"......"+gmEtrTransBean.fetchUserMailIdDetail(gmEtrSesnUserInviteForm.getStrUserId()+"|"));
			boolean test = GmCommonClass.sendMail(gmEtrTransBean.fetchUserMailIdDetail(gmEtrSesnUserInviteForm.getSessionOwner()+"|"), strMailId, strCCMailId, "Delete...Test Mail..For Active/DeActivate..", "");
		}
		hmValues = gmEtrSetupBean.fetchSesnDetail(strSessionId);
		gmEtrSesnUserInviteForm = (GmEtrSesnUserInviteForm) GmCommonClass.getFormFromHashMap(gmEtrSesnUserInviteForm, hmValues);
		gmEtrSesnUserInviteForm.setAlDeptList(GmCommonClass.getCodeList("DEPID"));

		alInvitedUserList = gmEtrTransBean.fetchInvitedUserDetail(gmEtrSesnUserInviteForm.getSessionId());
		gmEtrSesnUserInviteForm.setInvitedUserList(GmCommonClass.parseNullArrayList(alInvitedUserList));

		return mapping.findForward("success");
	}
}
