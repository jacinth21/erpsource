/**
 * FileName    : GmEtrUserSessInviteAction.java 
 * Description :
 * Author      : rshah
 * Date        : Oct 22, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.beans.GmEtrTransBean;
import com.globus.etrain.forms.GmEtrSesnModMapForm;
import com.globus.etrain.forms.GmEtrSessionSetupForm;

/**
 * @author rshah
 * 
 */
public class GmEtrSesnModMapAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrSesnModMapForm gmEtrSesnModMapForm = (GmEtrSesnModMapForm) form;
		gmEtrSesnModMapForm.loadSessionParameters(request);

		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		GmEtrTransBean gmEtrTransBean = new GmEtrTransBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrSesnModMapForm.getStrOpt();
		String strSessionId = (String) request.getParameter("sessionId");
		// String selModuleId = (String)request.getParameter("selModuleId");

		// log.debug("come from modlookup...."+selModuleId);

		String strSessModSeq = gmEtrSesnModMapForm.getSessModSeq();
		// log.debug("strSessionId..1@......"+strSessionId);
		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();
		ArrayList alList = new ArrayList();

		gmEtrSesnModMapForm.setSessionId(strSessionId);
		hmParam = GmCommonClass.getHashMapFromForm(gmEtrSesnModMapForm);
		log.debug("hmParam....1@....." + hmParam + "......strOpt......." + strOpt);
		if (strOpt.equals("save")) {

			strSessModSeq = gmEtrTransBean.savSesnModMapDetail(hmParam);
			// alList = gmEtrTransBean.fetchSesnModMapDetail(strSessionId);
			// request.setAttribute("RDSESNMODMAP",alList);
		} else if (strOpt.equals("load")) {
			hmValues = gmEtrTransBean.fetchSesnModDetail(strSessModSeq);
			gmEtrSesnModMapForm = (GmEtrSesnModMapForm) GmCommonClass.getFormFromHashMap(gmEtrSesnModMapForm, hmValues);
			// log.debug("hmValues...."+hmValues+"..form
			// values..."+gmEtrSesnModMapForm.getDurationHrs());
		}
		gmEtrSesnModMapForm.setAlModuleList(gmEtrSetupBean.fetchEtrModList());
		gmCommonBean.loadEmployeeList();
		gmEtrSesnModMapForm.setAlTrainerList(gmCommonBean.getEmployeeList());
		// gmEtrSesnModMapForm.setAlTrainerList(gmCommonBean.loadEmployeeList("A",
		// "2000"));
		gmEtrSesnModMapForm.setAlReasonList(GmCommonClass.getCodeList("SMRSN"));
		gmEtrSesnModMapForm.setAlStatusList(GmCommonClass.getCodeList("SMMAP"));

		// log.debug("1@....");
		alList = GmCommonClass.parseNullArrayList(gmEtrTransBean.fetchSesnModMapDetail(gmEtrSesnModMapForm.getSessionId()));
		log.debug("2@...." + alList);
		gmEtrSesnModMapForm.setSessModMapDT(alList);

		return mapping.findForward("success");
	}
}
