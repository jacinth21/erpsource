/**
 * FileName    : GmEtrModDtlRptAction.java 
 * Description :
 * Author      : rshah
 * Date        : Oct 15, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrRptBean;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.forms.GmEtrModLookupForm;

/**
 * @author rshah
 *
 */
public class GmEtrModLookupAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrModLookupForm gmEtrModLookupForm = (GmEtrModLookupForm) form;
		gmEtrModLookupForm.loadSessionParameters(request);

		GmCommonBean gmCommonBean = new GmCommonBean();
		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		GmEtrRptBean gmEtrRptBean = new GmEtrRptBean();

		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

		String strOpt = gmEtrModLookupForm.getStrOpt();
		String strCourseId = gmEtrModLookupForm.getCourseId();
		String strModuleId = gmEtrModLookupForm.getModuleId();
		String strModuleNm = gmEtrModLookupForm.getModuleNm();
		String strModuleOwner = gmEtrModLookupForm.getModuleOwner();

		ArrayList alList = new ArrayList();
		gmCommonBean.loadEmployeeList();
		gmEtrModLookupForm.setAlModuleOwnerList(gmCommonBean.getEmployeeList());
		//gmEtrModLookupForm.setAlModuleOwnerList(gmCommonBean.loadEmployeeList("A", "2000"));
		gmEtrModLookupForm.setAlCourseList(gmEtrSetupBean.fetchEtrCourseList());
		alList = GmCommonClass.parseNullArrayList(alList);
		gmEtrModLookupForm.setAlModLookupList(alList);

		if (strOpt.equals("load")) {
			alList = GmCommonClass.parseNullArrayList(gmEtrRptBean.fetchModLookupDetail(strModuleId, strModuleNm, strModuleOwner, strCourseId));
			gmEtrModLookupForm.setAlModLookupList(alList);
		}

		return mapping.findForward("success");
	}
}
