/**
 * FileName    : GmEtrUserDtlRptAction.java 
 * Description :
 * Author      : rshah
 * Date        : Dec 1, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrRptBean;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.forms.GmEtrUserDtlRptForm;
import com.globus.common.beans.GmJasperReport;
/**
 * @author rshah
 *
 */
public class GmEtrUserDtlRptAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrUserDtlRptForm gmEtrUserDtlRptForm = (GmEtrUserDtlRptForm) form;
		gmEtrUserDtlRptForm.loadSessionParameters(request);

		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		GmEtrRptBean gmEtrRptBean = new GmEtrRptBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrUserDtlRptForm.getStrOpt();
		log.debug("strOpt...In Action..." + strOpt);

		HashMap hmParam = new HashMap();
		ArrayList alUserDtlRptList = new ArrayList();
		ArrayList alTrainingSummary = new ArrayList();
		alUserDtlRptList = GmCommonClass.parseNullArrayList(alUserDtlRptList);
		gmEtrUserDtlRptForm.setAlUserDtlRptList(alUserDtlRptList);
		gmEtrUserDtlRptForm.setStrSessionId(GmCommonClass.parseNull((String) request.getParameter("strSessionId")));
		gmEtrUserDtlRptForm.setTraineeId(GmCommonClass.parseNull((String) request.getParameter("traineeId")));
		//gmEtrUserDtlRptForm.setCourseId(GmCommonClass.parseNull((String) request.getParameter("courseId")));

		hmParam = GmCommonClass.getHashMapFromForm(gmEtrUserDtlRptForm);

		if (strOpt.equals("load")) {
			alUserDtlRptList = GmCommonClass.parseNullArrayList(gmEtrRptBean.fetchUserRptDetail(hmParam));
			gmEtrUserDtlRptForm.setAlUserDtlRptList(alUserDtlRptList);
			log.debug("alUserDtlRptList...." + alUserDtlRptList);

		} else if (strOpt.equals("printsummary")) {
			GmJasperReport gmJasperReport = new GmJasperReport();
			String strTraineeId = gmEtrUserDtlRptForm.getTraineeId();
			String strTraineeName = gmEtrUserDtlRptForm.getStrTraineeName();
			String strTraineeDept = gmEtrUserDtlRptForm.getTrneeDeptName();
			String strTraineeStartDt = gmEtrUserDtlRptForm.getTrneeStartDate();
			alTrainingSummary = gmEtrRptBean.fetchTrainingSummary(strTraineeId);
			HashMap hmTemp = new HashMap();
			hmTemp.put("TRUSERNAME", strTraineeName);
			hmTemp.put("TRDEPTNAME", strTraineeDept);
			hmTemp.put("STARTDATE", strTraineeStartDt);

			if (alTrainingSummary.size() < 20) {
				int subRows = 20 - alTrainingSummary.size();
				for (int i = 0; i < subRows; i++) {
					HashMap hmEmpty = new HashMap();
					hmEmpty.put("PROJCOMDATE", "");
					hmEmpty.put("ACTCOMDATE", "");
					hmEmpty.put("LOCATION", "");
					hmEmpty.put("COURSETITLE", "");
					alTrainingSummary.add(hmEmpty);
				}
			}
			gmJasperReport.setRequest(request);
			gmJasperReport.setResponse(response);
			gmJasperReport.setJasperReportName("/GmEtrTrainingSummary.jasper");
			gmJasperReport.setHmReportParameters(hmTemp);
			gmJasperReport.setReportDataList(alTrainingSummary);
			gmJasperReport.createJasperReport();

			return null;
		}

		gmCommonBean.loadEmployeeList();
		gmEtrUserDtlRptForm.setAlTraineeIdList(gmCommonBean.getEmployeeList());
		gmEtrUserDtlRptForm.setAlReasonList(GmCommonClass.getCodeList("SMRSN"));
		gmEtrUserDtlRptForm.setAlStatusList(GmCommonClass.getCodeList("SUMAP"));
		gmEtrUserDtlRptForm.setAlModuleList(gmEtrSetupBean.fetchEtrModList());
		gmEtrUserDtlRptForm.setAlCourseList(gmEtrSetupBean.fetchEtrCourseList());
		gmEtrUserDtlRptForm.setAlUserDeptIdList(GmCommonClass.getCodeList("DEPID"));

		return mapping.findForward("success");
	}
}
