/**
 * FileName    : GmEtrSesnCmpltnAction.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 24, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.beans.GmEtrTransBean;
import com.globus.etrain.forms.GmEtrSesnCmpltnForm;

/**
 * @author rshah
 *
 */
public class GmEtrSesnCmpltnAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrSesnCmpltnForm gmEtrSesnCmpltnForm = (GmEtrSesnCmpltnForm) form;
		gmEtrSesnCmpltnForm.loadSessionParameters(request);

		GmEtrTransBean gmEtrTransBean = new GmEtrTransBean();
		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();

		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrSesnCmpltnForm.getStrOpt();
		String strSessionId = GmCommonClass.parseNull((String) request.getParameter("strSessionId"));
		String strSesnOwner = GmCommonClass.parseNull(gmEtrSesnCmpltnForm.getStrSesnOwner());
		String strUsrId = "";
		if ("".equals(strSesnOwner)) {
			strUsrId = gmEtrSesnCmpltnForm.getUserId();
		} else
			strUsrId = "0";
		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();
		ArrayList alUsrDtlMapList = new ArrayList();

		hmParam = GmCommonClass.getHashMapFromForm(gmEtrSesnCmpltnForm);

		if (strOpt.equals("save")) {
			log.debug("hmParam......" + hmParam + "....SessionId..." + gmEtrSesnCmpltnForm.getSessionId());
			gmEtrTransBean.saveSesnCmpltnStage(hmParam);
		}

		if (!"".equals(strSessionId)) {
			gmEtrSesnCmpltnForm.setSessionId(strSessionId);
		}
		log.debug("....SessionId..." + gmEtrSesnCmpltnForm.getSessionId());
		hmValues = gmEtrSetupBean.fetchSesnDetail(gmEtrSesnCmpltnForm.getSessionId());
		gmEtrSesnCmpltnForm = (GmEtrSesnCmpltnForm) GmCommonClass.getFormFromHashMap(gmEtrSesnCmpltnForm, hmValues);
		hmValues = gmEtrTransBean.fetchSesnCmpltnDetail(gmEtrSesnCmpltnForm.getSessionId(), strUsrId);
		log.debug("1@.." + hmValues);
		gmEtrSesnCmpltnForm = (GmEtrSesnCmpltnForm) GmCommonClass.getFormFromHashMap(gmEtrSesnCmpltnForm, hmValues);
		log.debug("1.1@..." + strUsrId + "...." + strSesnOwner);
		alUsrDtlMapList = GmCommonClass.parseNullArrayList(gmEtrTransBean.fetchUserModMapDetail(gmEtrSesnCmpltnForm.getSessionId(), strUsrId, strSesnOwner));
		log.debug("2@...." + alUsrDtlMapList);
		gmEtrSesnCmpltnForm.setAlUsrDtlMapList(alUsrDtlMapList);

		return mapping.findForward("success");
	}
}
