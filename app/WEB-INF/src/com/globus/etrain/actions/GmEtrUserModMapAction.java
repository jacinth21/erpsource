package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.beans.GmEtrTransBean;
import com.globus.etrain.forms.GmEtrSesnModMapForm;
import com.globus.etrain.forms.GmEtrUserModMapForm;

public class GmEtrUserModMapAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrUserModMapForm gmEtrUserModMapForm = (GmEtrUserModMapForm) form;
		gmEtrUserModMapForm.loadSessionParameters(request);

		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		GmEtrTransBean gmEtrTransBean = new GmEtrTransBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrUserModMapForm.getStrOpt();
		String strSessionId = (String) request.getParameter("sessionId");
		//String strSesnUserId = (String)request.getParameter("sesnUserId");
		//String strSesnUserNm = (String)request.getParameter("sesnUserNm");
		String strUserSeq = gmEtrUserModMapForm.getUserSeq();

		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();
		ArrayList alUsrDtlMapList = new ArrayList();

		gmEtrUserModMapForm.setSessionId(strSessionId);
		hmParam = GmCommonClass.getHashMapFromForm(gmEtrUserModMapForm);
		log.debug("strSessionId......"+strSessionId+"......"+strOpt);
		if (strOpt.equals("loadUsrDtl")) {
			hmValues = gmEtrTransBean.fetchUserModDetail(strUserSeq);
			gmEtrUserModMapForm = (GmEtrUserModMapForm) GmCommonClass.getFormFromHashMap(gmEtrUserModMapForm, hmValues);
		}
		if (strOpt.equals("save")) {
			String strUsrSeq = gmEtrTransBean.saveUserModMapDetail(hmParam);
		}
		hmValues = gmEtrSetupBean.fetchSesnDetail(strSessionId);
		gmEtrUserModMapForm = (GmEtrUserModMapForm) GmCommonClass.getFormFromHashMap(gmEtrUserModMapForm, hmValues);
		gmEtrUserModMapForm.setAlReasonList(GmCommonClass.getCodeList("SURSN"));

		alUsrDtlMapList = GmCommonClass.parseNullArrayList(gmEtrTransBean.fetchUserModMapDetail(gmEtrUserModMapForm.getSessionId(), gmEtrUserModMapForm
				.getSesnUserId(), ""));
		gmEtrUserModMapForm.setAlUsrDtlMapList(alUsrDtlMapList);

		return mapping.findForward("success");
	}
}
