package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrRptBean;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.beans.GmEtrTransBean;
import com.globus.etrain.forms.GmEtrSesnDtlRptForm;
import com.globus.common.beans.GmJasperReport;

public class GmEtrSesnDtlRptAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrSesnDtlRptForm gmEtrSesnDtlRptForm = (GmEtrSesnDtlRptForm) form;
		gmEtrSesnDtlRptForm.loadSessionParameters(request);

		//GmEtrModSetupBean gmEtrModSetupBean = new GmEtrModSetupBean();
		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		GmEtrRptBean gmEtrRptBean = new GmEtrRptBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmEtrTransBean gmEtrTransBean = new GmEtrTransBean();
		
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrSesnDtlRptForm.getStrOpt();
		log.debug("strOpt...In Action..." + strOpt);

		HashMap hmParam = new HashMap();
		ArrayList alSesnDtlRptList = new ArrayList();		
		gmEtrSesnDtlRptForm.setAlSesnDtlRptList(GmCommonClass.parseNullArrayList(alSesnDtlRptList));
		gmEtrSesnDtlRptForm.setStrUserId((GmCommonClass.parseNull((String) request.getParameter("strUserId"))));
		hmParam = GmCommonClass.getHashMapFromForm(gmEtrSesnDtlRptForm);

		if (strOpt.equals("load")) {
			alSesnDtlRptList = gmEtrRptBean.fetchSessionRptDetail(hmParam);
			gmEtrSesnDtlRptForm.setAlSesnDtlRptList(GmCommonClass.parseNullArrayList(alSesnDtlRptList));
			log.debug("alSesnDtlRptList...." + alSesnDtlRptList);

		} else if (strOpt.equals("printrecords")) {
			// goes to jasper record report
			GmJasperReport gmJasperReport = new GmJasperReport();
			String strSessionId = gmEtrSesnDtlRptForm.getStrSessionId();
			String strSessionStartDt = gmEtrSesnDtlRptForm.getStrSessionStartDt();
			ArrayList alTrainingRpt = new ArrayList();
			ArrayList alModuleList = new ArrayList();
			HashMap hmTemp = new HashMap();
			alTrainingRpt = gmEtrRptBean.fetchTrainingRecords(strSessionId);
			log.debug("alTrainingRpt........"+alTrainingRpt);
			alModuleList = GmCommonClass.parseNullArrayList(gmEtrTransBean.fetchSesnModMapDetail(strSessionId));
			log.debug("alModuleList...."+alModuleList);
			gmJasperReport.setResponse(response);
			gmJasperReport.setRequest(request);
			hmTemp.put("SUBMODULELIST", alModuleList);
			hmTemp.put("SUBREPORT_DIR", gmJasperReport.getJasperRealLocation());
			hmTemp.put("SESSIONDATE", strSessionStartDt);
			hmTemp.put("SESSIONNAME", strSessionId);

			if (alTrainingRpt.size() < 15) {
				int subRows = 15 - alTrainingRpt.size();
				for (int i = 0; i < subRows; i++) {
					HashMap hmEmpty = new HashMap();
					hmEmpty.put("TRUSERNAME", "");
					hmEmpty.put("TRDEPTNAME", "");
					alTrainingRpt.add(hmEmpty);
				}
			}
			
			gmJasperReport.setJasperReportName("/GmEtrTrainingRecord.jasper");
			gmJasperReport.setHmReportParameters(hmTemp);
			gmJasperReport.setReportDataList(alTrainingRpt);
			gmJasperReport.createJasperReport();
			return null;
		}
		gmCommonBean.loadEmployeeList();
		gmEtrSesnDtlRptForm.setAlSessionOwnerList(gmCommonBean.getEmployeeList());
		//gmEtrSesnDtlRptForm.setAlSessionOwnerList(gmCommonBean.loadEmployeeList("A", "2000"));
		gmEtrSesnDtlRptForm.setAlSessionLcnTypeList(GmCommonClass.getCodeList("SNLCN"));
		gmEtrSesnDtlRptForm.setAlReasonList(GmCommonClass.getCodeList("SMRSN"));
		gmEtrSesnDtlRptForm.setAlStatusList(GmCommonClass.getCodeList("ESESN"));
		gmEtrSesnDtlRptForm.setAlModuleList(gmEtrSetupBean.fetchEtrModList());
		gmEtrSesnDtlRptForm.setAlCourseList(gmEtrSetupBean.fetchEtrCourseList());

		return mapping.findForward("success");
	}
}
