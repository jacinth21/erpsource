/**
 * FileName    : GmEtrUserDashboardAction.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 21, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrRptBean;
import com.globus.etrain.forms.GmEtrUserDashboardForm;

/**
 * @author rshah
 * 
 */
public class GmEtrUserDashboardAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrUserDashboardForm gmEtrUserDashboardForm = (GmEtrUserDashboardForm) form;
		gmEtrUserDashboardForm.loadSessionParameters(request);

		GmEtrRptBean gmEtrRptBean = new GmEtrRptBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrUserDashboardForm.getStrOpt();
		String strUserId = gmEtrUserDashboardForm.getUserId();

		log.debug("strOpt...In Action..." + strOpt + "...strUserId..." + strUserId);

		ArrayList alOpenTrainings = new ArrayList();

		// if ("load".equals(strOpt)) {
		alOpenTrainings = GmCommonClass.parseNullArrayList(gmEtrRptBean.fetchDashboardDetail(strUserId));
		gmEtrUserDashboardForm.setAlOpenTrainings(alOpenTrainings);
		// }

		return mapping.findForward("success");
	}
}
