/**
 * FileName    : GmEtrModelWindowAction.java 
 * Description :
 * Author      : rshah
 * Date        : Jan 27, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrTransBean;
import com.globus.etrain.forms.GmEtrModelWindowForm;

/**
 * @author rshah
 *
 */
public class GmEtrModelWindowAction extends GmAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		GmEtrModelWindowForm gmEtrModelWindowForm = (GmEtrModelWindowForm) form;
		gmEtrModelWindowForm.loadSessionParameters(request);	
		
		Logger log = GmLogger.getInstance(this.getClass().getName());
		GmEtrTransBean gmEtrTransBean = new GmEtrTransBean();
		String strUserMatTrackId = "";
		String actionFlag = "";
		HashMap hmParam = new HashMap();
		HttpSession session = request.getSession(false);
		gmEtrModelWindowForm.setFileId(GmCommonClass.parseNull((String)request.getParameter("fileId")));
		gmEtrModelWindowForm.setSessionId(GmCommonClass.parseNull((String)request.getParameter("sessionId")));
		gmEtrModelWindowForm.setModuleId(GmCommonClass.parseNull((String)request.getParameter("moduleId")));
		gmEtrModelWindowForm.setMatCmpltnDt(GmCommonClass.parseNull((String)request.getParameter("matCmpltnDt")));
		gmEtrModelWindowForm.setSesnUserMatId(GmCommonClass.parseNull((String)request.getParameter("sesnUserMatId")));
		gmEtrModelWindowForm.setUserMatTrackId(GmCommonClass.parseNull((String)session.getAttribute("userMatTrackId"))); //from session
		//gmEtrModelWindowForm.setSesnOwner(GmCommonClass.parseNull((String)request.getParameter("sesnOwner")));
		hmParam = GmCommonClass.getHashMapFromForm(gmEtrModelWindowForm);
		
		log.debug("1@...................."+"...file..."+(String)request.getParameter("fileId")+".....222.."+GmCommonClass.parseNull((String)session.getAttribute("userMatTrackId")));
		
		actionFlag = GmCommonClass.parseNull((String)request.getParameter("actionFlag"));
		
		if (!"".equals(GmCommonClass.parseNull(gmEtrModelWindowForm.getFileId())) && "".equals(GmCommonClass.parseNull(gmEtrModelWindowForm.getMatCmpltnDt()))) 
		{
			
			strUserMatTrackId = gmEtrTransBean.saveUserMatTrackDetail(hmParam);
			session.setAttribute("userMatTrackId", strUserMatTrackId);			
			log.debug("2@...." + strUserMatTrackId);			
		}
		log.debug("1@............"+actionFlag);
		if(!actionFlag.equals("true"))
    	{
			request.setAttribute("sId",gmEtrModelWindowForm.getFileId());
			request.setAttribute("uploadString","ETRUPLOADHOME");
			return mapping.findForward("success");			
		}
		log.debug("...removing session..parameter....userMatTrackId");	
		session.removeAttribute("userMatTrackId");
		return null;
	}

}
