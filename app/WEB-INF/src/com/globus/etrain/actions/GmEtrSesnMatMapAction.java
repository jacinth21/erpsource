/**
 * FileName    : GmEtrSesnMatMapAction.java 
 * Description :
 * Author      : rshah
 * Date        : Dec 15, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

/**
 * @author rshah
 *
 */
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.beans.GmEtrTransBean;
import com.globus.etrain.forms.GmEtrSesnMatMapForm;
public class GmEtrSesnMatMapAction extends GmAction {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrSesnMatMapForm gmEtrSesnMatMapForm = (GmEtrSesnMatMapForm) form;
		gmEtrSesnMatMapForm.loadSessionParameters(request);

		GmEtrTransBean gmEtrTransBean = new GmEtrTransBean();
		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();

		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrSesnMatMapForm.getStrOpt();
		gmEtrSesnMatMapForm.setModuleId((String) request.getParameter("moduleId"));
		String strSeqNo = gmEtrSesnMatMapForm.getSeqNo();
		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();

		ArrayList alSesnMatList = new ArrayList();

		hmParam = GmCommonClass.getHashMapFromForm(gmEtrSesnMatMapForm);

		if (strOpt.equals("save")) {
			log.debug("hmParam......" + hmParam + "....SessionId..." + gmEtrSesnMatMapForm.getSessionId());
			gmEtrTransBean.saveSesnMatMapDetail(hmParam);
		}
		if (strOpt.equals("edit")) {
			hmValues = gmEtrTransBean.fetchSesnMatDetail(strSeqNo);
			log.debug(hmValues + "....." + gmEtrSesnMatMapForm.getMandatory());
			gmEtrSesnMatMapForm = (GmEtrSesnMatMapForm) GmCommonClass.getFormFromHashMap(gmEtrSesnMatMapForm, hmValues);

		}
		hmValues = gmEtrSetupBean.fetchEtrModuleDetail(gmEtrSesnMatMapForm.getModuleId());
		gmEtrSesnMatMapForm = (GmEtrSesnMatMapForm) GmCommonClass.getFormFromHashMap(gmEtrSesnMatMapForm, hmValues);

		alSesnMatList = GmCommonClass.parseNullArrayList(gmEtrTransBean.fetchSesnMatMapDetail(gmEtrSesnMatMapForm.getModuleId()));
		gmEtrSesnMatMapForm.setAlSesnMatList(alSesnMatList);

		return mapping.findForward("success");
	}

}
