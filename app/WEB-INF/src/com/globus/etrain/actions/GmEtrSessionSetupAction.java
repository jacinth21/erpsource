/**
 * FileName    : GmEtrModSessionInfoAction.java 
 * Description :
 * Author      : rshah
 * Date        : Oct 15, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.forms.GmEtrSessionSetupForm;

/**
 * @author rshah
 *
 */
public class GmEtrSessionSetupAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrSessionSetupForm gmEtrSessionSetupForm = (GmEtrSessionSetupForm) form;
		gmEtrSessionSetupForm.loadSessionParameters(request);

		//GmEtrModSetupBean gmEtrModSetupBean = new GmEtrModSetupBean();
		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrSessionSetupForm.getStrOpt();
		String strSessionId = gmEtrSessionSetupForm.getSessionId();
		log.debug("strSessionId......................"+strSessionId);
		HashMap hmParam = new HashMap();
		HashMap hmValues = new HashMap();

		ArrayList alLogReasons = new ArrayList();
		hmParam = GmCommonClass.getHashMapFromForm(gmEtrSessionSetupForm);
		gmEtrSessionSetupForm.setLcnFlag("false");
		if (strOpt.equals("save")) 
		{
			strSessionId = gmEtrSetupBean.savSesnDetail(hmParam);
			gmEtrSessionSetupForm.setSessionId(strSessionId);			
			strOpt = "60310".equals(gmEtrSessionSetupForm.getSessionLcnType()) ? "load" : "";	//60310 - for Location drop down.		
		} 
		if (strOpt.equals("load")) 
		{
			hmValues = gmEtrSetupBean.fetchSesnDetail(strSessionId);
			gmEtrSessionSetupForm = (GmEtrSessionSetupForm) GmCommonClass.getFormFromHashMap(gmEtrSessionSetupForm, hmValues);			
			strOpt = "60310".equals(gmEtrSessionSetupForm.getSessionLcnType()) ? "loadLcn" : "";			
		}
		if (strOpt.equals("loadLcn")) 
		{			
			gmEtrSessionSetupForm.setAlSessionLcnVal(GmCommonClass.getCodeList("LCNVL"));
			gmEtrSessionSetupForm.setLcnFlag("true");
		}
		alLogReasons = gmCommonBean.getLog(strSessionId, "1238"); //1238 - For Specific comments
		gmEtrSessionSetupForm.setAlLogReasons(alLogReasons);
		gmCommonBean.loadEmployeeList();
		gmEtrSessionSetupForm.setAlOwnerList(gmCommonBean.getEmployeeList());
		gmEtrSessionSetupForm.setAlSessionLcnList(GmCommonClass.getCodeList("SNLCN"));
		gmEtrSessionSetupForm.setAlStatusList(GmCommonClass.getCodeList("ESESN"));

		return mapping.findForward("success");
	}
}
