/**
 * FileName    : GmEtrModuleDtlRptAction.java 
 * Description :
 * Author      : rshah/angela
 * Date        : Nov 26, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrRptBean;
import com.globus.etrain.beans.GmEtrSetupBean;
import com.globus.etrain.forms.GmEtrModuleDtlRptForm;

public class GmEtrModuleDtlRptAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrModuleDtlRptForm gmEtrModuleDtlRptForm = (GmEtrModuleDtlRptForm) form;
		gmEtrModuleDtlRptForm.loadSessionParameters(request);

		GmEtrRptBean gmEtrRptBean = new GmEtrRptBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmEtrSetupBean gmEtrSetupBean = new GmEtrSetupBean();
		Logger log = GmLogger.getInstance(this.getClass().getName());

		String strOpt = gmEtrModuleDtlRptForm.getStrOpt();

		HashMap hmParam = new HashMap();
		ArrayList alModuleDtlRptList = new ArrayList();
		gmEtrModuleDtlRptForm.setStrUserId((String) request.getParameter("strUserId"));
		gmEtrModuleDtlRptForm.setStrSessionId((String) request.getParameter("strSessionId"));

		if (strOpt.equals("load")) {
			hmParam = GmCommonClass.getHashMapFromForm(gmEtrModuleDtlRptForm);
			alModuleDtlRptList = GmCommonClass.parseNullArrayList(gmEtrRptBean.fetchModuleRptDetail(hmParam));
			gmEtrModuleDtlRptForm.setAlModuleDtlRptList(alModuleDtlRptList);
			gmEtrModuleDtlRptForm = (GmEtrModuleDtlRptForm) GmCommonClass.getFormFromHashMap(gmEtrModuleDtlRptForm, hmParam);
		}
		gmCommonBean.loadEmployeeList();
		gmEtrModuleDtlRptForm.setAlModuleOwnerList(gmCommonBean.getEmployeeList());
		gmEtrModuleDtlRptForm.setAlModuleDeptList(GmCommonClass.getCodeList("DEPID"));
		gmEtrModuleDtlRptForm.setAlModuleStatusList(GmCommonClass.getCodeList("ECRSE"));
		gmEtrModuleDtlRptForm.setAlCourseList(gmEtrSetupBean.fetchEtrCourseList());

		return mapping.findForward("success");
	}
}
