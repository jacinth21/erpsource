/**
 * FileName    : GmEtrUserMailInviteAction.java 
 * Description :
 * Author      : rshah
 * Date        : Oct 23, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.etrain.beans.GmEtrTransBean;
import com.globus.etrain.forms.GmEtrUserMailInviteForm;

/**
 * @author rshah
 *
 */
public class GmEtrUserMailInviteAction extends Action {
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmEtrUserMailInviteForm gmEtrUserMailInviteForm = (GmEtrUserMailInviteForm) form;
		gmEtrUserMailInviteForm.loadSessionParameters(request);
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

		GmEtrTransBean gmEtrTransBean = new GmEtrTransBean();
		String strOpt = (String) request.getParameter("strOpt");
		//String strSessionId = (String)request.getParameter("sessionId");		
		
		String strToMail = "";
		String strCcMail = "";
		if (strOpt.equals("load")) {
			gmEtrUserMailInviteForm.setStrInviteList((String) request.getParameter("hinviteList"));
			log.debug("In action..0@..." + gmEtrUserMailInviteForm.getStrInviteList());
			strToMail = gmEtrTransBean.fetchUserMailIdDetail(gmEtrUserMailInviteForm.getStrInviteList());
			gmEtrUserMailInviteForm.setToMail(strToMail);
			/*
				strToMail = "rshah@globusmedical.com;";
			    String strTemp = "<html><head><title>" +
			    "This is a Test Format" +
			    "</title></head><body><h1>" +
			    "abcpqr" +
			    "</h1><p>This is a test of sending an HTML e-mail" +
			    " through Java.</body></html>";
			    gmEtrUserMailInviteForm.setTextReason(strTemp);

			 */
		} else if (strOpt.equals("save")) {
			strToMail = gmEtrUserMailInviteForm.getToMail();
			strCcMail = gmEtrUserMailInviteForm.getCcMail();
			String[] strRecipients = strToMail.split(";");
			String[] cc = strCcMail.split(";");
			log.debug("In action..1@..." + gmEtrUserMailInviteForm.getStrInviteList());
			boolean test = GmCommonClass.sendMail("rshah@globusmedical.com", strRecipients, cc, "Delete....Test Mail..For Invite....", gmEtrUserMailInviteForm.getTextReason());
			gmEtrTransBean.saveSesnStatus(gmEtrUserMailInviteForm.getStrInviteList(),null);
		}

		return mapping.findForward("success");
	}
}
