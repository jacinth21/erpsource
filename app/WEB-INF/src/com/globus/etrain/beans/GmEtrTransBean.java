/**
 * FileName    : GmEtrUserSessInviteBean.java 
 * Description :
 * Author      : rshah
 * Date        : Oct 22, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * @author rshah
 *
 */
public class GmEtrTransBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	GmCommonBean gmCommonBean = new GmCommonBean();
	
	
	/**
     * fetchSesnModMapDetail - This method will be used for modules associated with the session 
     * @param strSessionId
     * @return ArrayList - will be used by displaytag
     * @exception AppError
     */
    public ArrayList fetchSesnModMapDetail(String strSessionId) throws AppError
    {
        ArrayList alList = new ArrayList();
        
        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_sesnmodmap_detail",2);
	    gmDBManager.setString(1, strSessionId);
	    gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
	    gmDBManager.close();
                    
        return alList;
    }
    
    /**
     * fetchSesnModDetail - This method will be fetch session module detail
     * @param strSessModSeq 
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchSesnModDetail(String strSessModSeq) throws AppError
    {
        HashMap hmValues = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_sesnmod_detail", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(strSessModSeq));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmValues = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		
		return hmValues;	
    }
    /**
     * savSesnModMapDetail - This method saves the session module mapping details 
     * @param hmParam - parameters to be saved
     * @exception AppError
     * @return String
     */
	public String savSesnModMapDetail(HashMap hmParam) throws AppError
	{
		String strSessModFromDb = "";
        GmDBManager gmDBManager = new GmDBManager ();
        String strSessModSeq = GmCommonClass.parseNull((String)hmParam.get("SESSMODSEQ"));
        String strSessionId = GmCommonClass.parseNull((String)hmParam.get("SESSIONID"));
        String strModuleId = GmCommonClass.parseZero((String)hmParam.get("MODULEID"));
        String strDuration = GmCommonClass.parseNull((String)hmParam.get("DURATIONHRS"));
        String strReason = GmCommonClass.parseZero((String)hmParam.get("REASON"));
        String strDetails = GmCommonClass.parseNull((String)hmParam.get("DETAILS"));
        String strTrainer = GmCommonClass.parseZero((String)hmParam.get("TRAINERID"));
        String strStatus = GmCommonClass.parseZero((String)hmParam.get("STATUSMAP"));
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_sav_sesnmodmap_detail",9);        
        gmDBManager.registerOutParameter(9,java.sql.Types.CHAR);
        gmDBManager.setString(1,strSessModSeq);
        gmDBManager.setString(2,strSessionId);
        gmDBManager.setString(3,strModuleId);
        gmDBManager.setInt(4,Integer.parseInt(strDuration));
        gmDBManager.setInt(5,Integer.parseInt(strReason));
        gmDBManager.setString(6,strDetails);        
        gmDBManager.setInt(7,Integer.parseInt(strTrainer));
        gmDBManager.setInt(8,Integer.parseInt(strStatus));
        
        gmDBManager.execute();        
        strSessModFromDb = gmDBManager.getString(9);
        
        gmDBManager.commit();
		return strSessModFromDb;
	}
	
	/**
     * fetchInvitedUserDetail  - This method will be used to retrieve the invited user 
     * @param strSessionId
     * @return ArrayList - will be used by displaytag
     * @exception AppError
     */
    public ArrayList fetchInvitedUserDetail(String strSessionId) throws AppError
    {
        ArrayList alList = new ArrayList();
        
        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_inviteduser_detail",2);
	    gmDBManager.setString(1, strSessionId);
	    gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
	    gmDBManager.close();                    
        return alList;
    }
    
    /**
     * fetchInvitedUserDetail  - This method will be used to retrieve the invited user 
     * @param strSessionId
     * @return ArrayList - will be used by displaytag
     * @exception AppError
     */
    public ArrayList fetchUserDetail(String strFname, String strLname, String strDeptId) throws AppError
    {
        ArrayList alList = new ArrayList();
        log.debug(strFname+strLname+strDeptId);
        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_user_detail",4);
	    gmDBManager.setString(1, strFname);
	    gmDBManager.setString(2, strLname);
	    gmDBManager.setString(3, GmCommonClass.parseZero(strDeptId));
	    gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
	    log.debug(alList);
	    gmDBManager.close();                    
        return alList;
    }
    
    /**
     * saveUserSesnMapDetail  - This method saves the session user mapping details 
     * @param hmParam - parameters to be saved
     * @exception AppError
     * @return String
     */
	public String saveUserSesnMapDetail(HashMap hmParam) throws AppError
	{
		String strMapSeqFromDB = "";
        GmDBManager gmDBManager = new GmDBManager ();
        String strMapSeq = GmCommonClass.parseNull((String)hmParam.get("MAPSEQ"));
        String strSessionId = GmCommonClass.parseNull((String)hmParam.get("SESSIONID"));
        String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
        log.debug("input String...."+strInputString);
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_sav_usersesnmap_detail",4);        
        gmDBManager.registerOutParameter(4,java.sql.Types.CHAR);
        gmDBManager.setString(1,strMapSeq);
        gmDBManager.setString(2,strSessionId);
        gmDBManager.setString(3,strInputString);     
        gmDBManager.execute();        
        strMapSeqFromDB = gmDBManager.getString(4);
        
        gmDBManager.commit();
		return strMapSeqFromDB;
	}
	
	/**
     * fetchUserModMapDetail  - This method will be used to retrieve the module list for the session and user 
     * @param String strSesnId, String strUsrId
     * @return ArrayList - will be used by displaytag
     * @exception AppError
     */
    public ArrayList fetchUserModMapDetail(String strSesnId, String strUsrId, String strSesnOwner) throws AppError
    {
        ArrayList alList = new ArrayList();        
        GmDBManager gmDBManager = new GmDBManager ();
        log.debug(strSesnId+"....."+strUsrId+"....."+strSesnOwner);
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_usermodmap_detail",4);
	    gmDBManager.setString(1, strSesnId);
	    gmDBManager.setString(2, strUsrId);
	    gmDBManager.setString(3, strSesnOwner);
	    gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
	    log.debug(alList);
	    gmDBManager.close();                    
        return alList;
    }
    
    /**
     * fetchSesnModDetail - This method will be fetch session module detail
     * @param strSessModSeq 
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchUserModDetail(String strUserSeq) throws AppError
    {
        HashMap hmValues = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_usermod_detail", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(strUserSeq));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmValues = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		
		return hmValues;	
    }
    
    /**
     * saveUserModMapDetail  - This method saves the session user and module mapping details 
     * @param hmParam - parameters to be saved
     * @exception AppError
     * @return String
     */
	public String saveUserModMapDetail(HashMap hmParam) throws AppError
	{
		String strUserSeqFromDB = "";
        GmDBManager gmDBManager = new GmDBManager ();
        String strUserSeq = GmCommonClass.parseNull((String)hmParam.get("USERSEQ"));
        String strReason = GmCommonClass.parseZero((String)hmParam.get("REASON"));
        String strIsRequired = GmCommonClass.getCheckBoxValue((String)hmParam.get("ISREQUIRED"));
        log.debug("strIsRequired...."+strIsRequired);
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_sav_usermodmap_detail",4);        
        gmDBManager.registerOutParameter(4,java.sql.Types.CHAR);
        gmDBManager.setInt(1,Integer.parseInt(strUserSeq));
        gmDBManager.setInt(2,Integer.parseInt(strReason));
        gmDBManager.setString(3,strIsRequired);     
        gmDBManager.execute();        
        strUserSeqFromDB = gmDBManager.getString(4);
        
        gmDBManager.commit();
		return strUserSeqFromDB;
	}
	
	/**
     * fetchUserMailIdDetail  - This method will retrieve the mail ids of respecitve users for the session 
     * @param strSessionId - parameters to be used for retrieve
     * @exception AppError
     * @return String
     */
	public String fetchUserMailIdDetail(String strInviteList) throws AppError
	{
		String strUserMailIdList = "";
        GmDBManager gmDBManager = new GmDBManager ();
        log.debug("strInviteList...."+strInviteList);
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_usermailid_detail",2);        
        gmDBManager.registerOutParameter(2,java.sql.Types.CHAR);
        gmDBManager.setString(1,strInviteList);     
        gmDBManager.execute();        
        strUserMailIdList = gmDBManager.getString(2);
        log.debug("strUserMailIdList...."+strUserMailIdList);
        gmDBManager.close();
		return strUserMailIdList;
	}
	
	/**
     * saveSesnCmpltnStage  - This method saves the session completion details 
     * @param hmParam - parameters to be saved
     * @exception AppError
     * @return String
     */
	public void saveSesnCmpltnStage(HashMap hmParam) throws AppError
	{
		GmDBManager gmDBManager = new GmDBManager ();
        String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
        String strSessionId = GmCommonClass.parseNull((String) hmParam.get("SESSIONID"));
        String strSesnUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        String strFeedback = GmCommonClass.parseNull((String)hmParam.get("FEEDBACK"));
        log.debug("strInputString...."+strInputString+"..strSesnUserId...."+strSesnUserId);
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_sav_sesn_cmpltn_stage",4);        
        //gmDBManager.registerOutParameter(4,java.sql.Types.CHAR);
        gmDBManager.setString(1,strInputString); 
        gmDBManager.setInt(2,Integer.parseInt(strSesnUserId)); 
        gmDBManager.setString(3,strSessionId);
        gmDBManager.setString(4,strFeedback);
        gmDBManager.execute();        
        //strUserSeqFromDB = gmDBManager.getString(4);
        
        gmDBManager.commit();
		//return strUserSeqFromDB;
	}
	
	/**
     * fetchSesnCmpltnDetail - This method will be fetch session completion detail
     * @param strSessionId ,  strUserId
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchSesnCmpltnDetail(String strSessionId, String strUserId) throws AppError
    {
        HashMap hmValues = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_sesncmpltn_detail", 3);
		gmDBManager.setString(1, GmCommonClass.parseNull(strSessionId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strUserId));
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmValues = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		
		return hmValues;	
    }
	
    /**
     * fetchCourseDeptDetail - This method will be fetch a course-department Map completion detail
     * @param strSeqNo 
     * @return HashMap
     * @exception AppError
     */
	public HashMap fetchCourseDeptDetail(String strSeqNo) throws AppError
    {
        HashMap hmValues = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_coursedept_detail", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(strSeqNo));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmValues = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		
		return hmValues;	
    }
	
    
    /**
     * fetchCourseDeptMapDetail - This method will be used for course associated with the department
     * @param strCourseId
     * @return ArrayList - will be used by displaytag
     * @exception AppError
     */
    public ArrayList fetchCourseDeptMapDetail(String strCourseId) throws AppError
    {
        ArrayList alList = new ArrayList();
        
        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_coursedeptmap_detail",2);
	    gmDBManager.setString(1, strCourseId);
	    gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
	    gmDBManager.close();
                    
        return alList;
   } 
    
    /**
	 * saveCourseDeptMapDetail  - This method saves the course and department mapping details 
	 * @param hmParam - parameters to be saved
	 * @exception AppError
	 * @return String
	 */
	public String saveCourseDeptMapDetail(HashMap hmParam) throws AppError {
		String strCDMapSeqFromDB = "";
		GmDBManager gmDBManager = new GmDBManager();
		String strMapSeqNo = GmCommonClass.parseNull((String) hmParam.get("COURSEMAPSEQ"));
		String strCourseMapId = GmCommonClass.parseNull((String) hmParam.get("COURSEMAPID"));
		String strDeptMapId = GmCommonClass.parseNull((String) hmParam.get("DEPTMAPID"));
		String strMandFlType = GmCommonClass.getCheckBoxValue((String)hmParam.get("MANDFLTYPE"));

		gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_sav_coursedeptmap_detail", 5);
		gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
		log.debug("seq @123..."+strMapSeqNo+"........"+strCourseMapId+"........."+strDeptMapId);
		gmDBManager.setString(1, strMapSeqNo);
		gmDBManager.setString(2, strCourseMapId);
		gmDBManager.setString(3, strDeptMapId);
		gmDBManager.setString(4, strMandFlType);
		gmDBManager.execute();
		strCDMapSeqFromDB = gmDBManager.getString(5);

		gmDBManager.commit();
		return strCDMapSeqFromDB;
	}
	
	/**
     * fetchSesnMatMapDetail - This method will be used for retrieving the materials for that session
     * @param strModuleId
     * @return ArrayList - will be used by displaytag
     * @exception AppError
     */
    public ArrayList fetchSesnMatMapDetail(String strModuleId) throws AppError
    {
        ArrayList alList = new ArrayList();
        
        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_sesnmatmap_detail",2);
	    gmDBManager.setString(1, strModuleId);
	    gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
	    gmDBManager.close();
                    
        return alList;
   } 
   
    /**
     * fetchSesnMatDetail - This method will be fetch a material detail based on material id
     * @param strSeqNo 
     * @return HashMap
     * @exception AppError
     */
	public HashMap fetchSesnMatDetail(String strSeqNo) throws AppError
    {
        HashMap hmValues = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_sesnmat_detail", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(strSeqNo));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmValues = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		
		return hmValues;	
    }
    
	/**
     * saveSesnMatMapDetail  - This method saves updated material details 
     * @param hmParam - parameters to be saved
     * @exception AppError
     *
     */
	public void saveSesnMatMapDetail(HashMap hmParam) throws AppError
	{
		GmDBManager gmDBManager = new GmDBManager ();
		String strSeq = GmCommonClass.parseNull((String) hmParam.get("SEQNO"));
		String strMandatory = GmCommonClass.getCheckBoxValue((String)hmParam.get("MANDATORY"));
		gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_sav_sesnmatmap_detail",2);        
        gmDBManager.setInt(1,Integer.parseInt(strSeq)); 
        gmDBManager.setString(2,strMandatory);
        gmDBManager.execute();        
        gmDBManager.commit();		
	}
	
	/**
     * fetchUserMatMapDetail - This method will be used for retrieving the materials for that user
     * @param strModuleId, strUserId
     * @return ArrayList - will be used by displaytag
     * @exception AppError
     */
    public ArrayList fetchUserMatMapDetail(String strModuleId, String strUserId, String strSessionId) throws AppError
    {
        ArrayList alList = new ArrayList();
        log.debug("in bean...4@..."+strModuleId + "...."+strUserId+"......."+strSessionId);
        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_usermatmap_detail",4);
	    gmDBManager.setString(1, strModuleId);
	    gmDBManager.setString(2, strUserId);
	    gmDBManager.setString(3, strSessionId);
	    gmDBManager.registerOutParameter(4,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
	    gmDBManager.close();
                    
        return alList;
   } 
    
    /**
     * saveUserMatTrackDetail  - This method saves user material tracking details 
     * @param hmParam - parameters to be saved
     * @exception AppError
     *
     */
    public String saveUserMatTrackDetail(HashMap hmParam) throws AppError
	{
		String strUserMatTrackIdFromDB="";
		String strSesnUserMatId = (String) GmCommonClass.parseNullHashMap(hmParam).get("SESNUSERMATID");
		String strUserMatTrackId = (String) GmCommonClass.parseNullHashMap(hmParam).get("USERMATTRACKID");
		String strTrCmpltn = (String) GmCommonClass.parseNullHashMap(hmParam).get("TRCMPLTN"); 
		String strModuleId = (String) GmCommonClass.parseNullHashMap(hmParam).get("MODULEID");
		String strSessionId = (String) GmCommonClass.parseNullHashMap(hmParam).get("SESSIONID"); 
		String strUserId =  (String) GmCommonClass.parseNullHashMap(hmParam).get("USERID");
		GmDBManager gmDBManager = new GmDBManager ();
		gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_sav_usermattrack_detail",7);
		log.debug("strUserMatTrackId...."+strUserMatTrackId+"....strSesnUserMatId...."+strSesnUserMatId+"................"+strTrCmpltn);
		gmDBManager.setInt(1, Integer.parseInt(GmCommonClass.parseZero(strUserMatTrackId)));
		gmDBManager.setInt(2,Integer.parseInt(GmCommonClass.parseZero(strSesnUserMatId)));
		gmDBManager.setString(3,GmCommonClass.getCheckBoxValue(strTrCmpltn));
		gmDBManager.setString(4,GmCommonClass.parseNull(strModuleId));		
		gmDBManager.setString(5,GmCommonClass.parseNull(strUserId));
		gmDBManager.setString(6,GmCommonClass.parseNull(strSessionId));
        gmDBManager.registerOutParameter(7,java.sql.Types.CHAR);
        gmDBManager.execute();
        strUserMatTrackIdFromDB = gmDBManager.getString(7);
        gmDBManager.commit();
        return strUserMatTrackIdFromDB;
	}
	
	/**
	 * saveSesnStatus
	 * @param String
	 * @exception AppError
	 */
	public void saveSesnStatus(String userList, HashMap hmParam) throws AppError
	{
		GmDBManager gmDBManager = new GmDBManager ();
		String strStatusId = GmCommonClass.parseNull((String) GmCommonClass.parseNullHashMap(hmParam).get("STATUSID"));
		String strUserSeq = GmCommonClass.parseNull((String) GmCommonClass.parseNullHashMap(hmParam).get("USERSEQ"));
		gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_sav_sesnstatus",3);
		log.debug(userList+"......"+strStatusId+"........."+strUserSeq);
		gmDBManager.setString(1,GmCommonClass.parseNull(userList));
		gmDBManager.setString(2,strStatusId);
		gmDBManager.setString(3,strUserSeq);
		gmDBManager.execute();
		gmDBManager.commit();
	}
	
	/**
     * fetchMaterialDetail - This method will be used for fetching materials for the module 
     * @param strModuleId
     * @return ArrayList - will be used by displaytag
     * @exception AppError
     */
    public ArrayList fetchMaterialDetail(String strModuleId, String strSessionId) throws AppError
    {
        ArrayList alList = new ArrayList();
        
        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_fch_material_detail",3);
	    gmDBManager.setString(1, strModuleId);
	    gmDBManager.setString(2, strSessionId);
	    gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
	    gmDBManager.close();
                    
        return alList;
    }
    
    /**
	 * saveMaterialDetail
	 * @param HashMap
	 * @exception AppError
	 */
	public void saveMaterialDetail(HashMap hmParam) throws AppError
	{
		GmDBManager gmDBManager = new GmDBManager ();
		String strModuleId = GmCommonClass.parseNull((String) GmCommonClass.parseNullHashMap(hmParam).get("MODULEID"));
		String strSessionId = GmCommonClass.parseNull((String) GmCommonClass.parseNullHashMap(hmParam).get("SESSIONID"));
		String strInputStr = GmCommonClass.parseNull((String) GmCommonClass.parseNullHashMap(hmParam).get("HINPUTSTRING"));
		String strInputStr1 = GmCommonClass.parseNull((String) GmCommonClass.parseNullHashMap(hmParam).get("HINPUTSTRING1"));
		gmDBManager.setPrepareString("gm_pkg_etr_trans.gm_sav_material_detail",4);
		log.debug(strModuleId+"......"+strSessionId+"........."+strInputStr+"..........."+strInputStr1);
		gmDBManager.setString(1,strModuleId);
		gmDBManager.setString(2,strSessionId);
		gmDBManager.setString(3,strInputStr);
		gmDBManager.setString(4,strInputStr1);
		gmDBManager.execute();
		gmDBManager.commit();
	}
}
