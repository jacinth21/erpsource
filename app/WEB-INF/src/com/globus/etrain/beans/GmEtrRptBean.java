/**
 * FileName    : GmPOEtrModDtlRptBean.java 
 * Description :
 * Author      : rshah
 * Date        : Oct 15, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * @author rshah
 * 
 */
public class GmEtrRptBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**
	 * fetchModLookupDetail - This method will be used for retrieving all the
	 * module info based on search condition.
	 * 
	 * @param strDeptListId
	 * @return ArrayList - will be used by displaytag
	 * @exception AppError
	 */

	public ArrayList fetchModLookupDetail(String strModuleId, String strModuleNm, String strModuleOwner, String strCourseId) throws AppError {
		ArrayList alList = new ArrayList();

		GmDBManager gmDBManager = new GmDBManager();
		log.debug(strModuleId + "...." + strModuleNm + "...." + strModuleOwner + "...." + strCourseId);
		gmDBManager.setPrepareString("gm_pkg_etr_rpt.gm_fch_modlookup_detail", 5);
		gmDBManager.setString(1, GmCommonClass.parseNull(strModuleId));
		gmDBManager.setString(2, GmCommonClass.parseNull(strModuleNm));
		gmDBManager.setString(3, GmCommonClass.parseZero(strModuleOwner));
		gmDBManager.setString(4, GmCommonClass.parseZero(strCourseId));
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
		gmDBManager.close();

		return alList;
	}

	/**
	 * fetchSessionRptDetail - This method retrieves the session detail based on
	 * search conditions.
	 * 
	 * @param hmParam -
	 *            parameters to be saved
	 * @exception AppError
	 * @return String
	 */
	public ArrayList fetchSessionRptDetail(HashMap hmParam) throws AppError {
		ArrayList alList = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		DBConnectionWrapper db = new DBConnectionWrapper();

		String strOwnerList = GmCommonClass.parseZero((String) hmParam.get("SESSIONOWNER"));
		String strModuleId = GmCommonClass.parseZero((String) hmParam.get("MODULEID"));
		String strCourseId = GmCommonClass.parseZero((String) hmParam.get("COURSEID"));
		String strSessionStartDt = GmCommonClass.parseNull((String) hmParam.get("SESSIONSTARTDT"));
		String strSessionEndDt = GmCommonClass.parseNull((String) hmParam.get("SESSIONENDDT"));
		String strStatus = GmCommonClass.parseZero((String) hmParam.get("STATUS"));
		String strSessionLcnType = GmCommonClass.parseZero((String) hmParam.get("SESSIONLCNTYPE"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("STRUSERID"));
		log.debug("In Bean.....userid."+strUserId);

		sbQuery.append("SELECT t6303.c6303_session_id sesnid");
		sbQuery.append(" , t6303.c6303_session_desc sessiondesc, TO_CHAR (t6303.c6303_start_date, 'MM/DD/YYYY') sessionstartdt");
		sbQuery.append(" , TO_CHAR (t6303.c6303_end_date, 'MM/DD/YYYY') sessionenddt, get_code_name (t6303.c901_status) status, t6303.c901_status statusid ");
		sbQuery.append(" , get_code_name (t6303.c901_location_type) sessionlcntype ");
		sbQuery.append(" , get_user_name(t6303.c101_session_owner) sessionowner, t6303.c101_session_owner ownerid, t6303.c6303_max_user maxuser ");
		sbQuery.append(" FROM  t6303_session t6303 ");
		sbQuery.append(" WHERE  t6303.c101_session_owner = DECODE ('" + strOwnerList + "', '0', t6303.c101_session_owner,'" + strOwnerList + "')");
		sbQuery.append(" AND  TO_CHAR (t6303.c6303_start_date, 'MM/DD/YYYY') = NVL ('" + strSessionStartDt
				+ "', TO_CHAR (t6303.c6303_start_date, 'MM/DD/YYYY'))");
		sbQuery.append(" AND  TO_CHAR (t6303.c6303_end_date, 'MM/DD/YYYY') = NVL ('" + strSessionEndDt + "', TO_CHAR (t6303.c6303_end_date, 'MM/DD/YYYY'))");
		sbQuery.append(" AND t6303.c901_status = DECODE ('" + strStatus + "', '0', t6303.c901_status,'" + strStatus + "')");
		sbQuery.append(" AND t6303.c901_location_type = DECODE ('" + strSessionLcnType + "', '0', t6303.c901_location_type,'" + strSessionLcnType + "')");
		if (!"0".equals(strModuleId)) {
			sbQuery.append(" AND t6303.c6303_session_id IN (SELECT t6304.c6303_session_id");
			sbQuery.append(" FROM t6304_session_module_map t6304 WHERE t6304.c6302_module_id = '" + strModuleId + "')");
		}
		if (!"0".equals(strCourseId)) {
			sbQuery.append(" AND t6303.c6303_session_id IN (SELECT t6304.c6303_session_id");
			sbQuery.append(" FROM t6304_session_module_map t6304, t6302_module t6302 WHERE t6304.c6302_module_id = t6302.c6302_module_id ");
			sbQuery.append(" AND t6302.c6301_course_id = '" + strCourseId + "')");
		}
		if(!"".equals(strUserId))
		{
			sbQuery.append(" AND t6303.c6303_session_id IN (SELECT t6305.c6303_session_id");
			sbQuery.append(" FROM t6305_session_user_map t6305 WHERE t6305.c101_user_id = '" + strUserId + "')");
		}
		sbQuery.append(" AND t6303.c6303_void_fl is null");
		alList = db.queryMultipleRecords(sbQuery.toString());
		//log.debug("query...."+sbQuery.toString());
		return alList;
	}
	
	/**
	 * fetchDashboardDetail - This method will be used for retrieving all the open trainings for logged in user
	 *  
	 * @return ArrayList - will be used by displaytag
	 * @exception AppError
	 */

	public ArrayList fetchDashboardDetail(String strUserId) throws AppError {
		ArrayList alList = new ArrayList();

		GmDBManager gmDBManager = new GmDBManager();
		//String strUserId = (String)hmParam.get("USERID");
		gmDBManager.setPrepareString("gm_pkg_etr_rpt.gm_fch_dashboard_detail", 2);
		gmDBManager.setInt(1, Integer.parseInt(strUserId));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alList;
	}
	// to be deleted
	/**
	 * fetchModDtlInfo - This method will be used for retrieving all the module
	 * info based on the department.
	 * 
	 * @param strDeptListId
	 * @return RowSetDynaClass - will be used by displaytag
	 * @exception AppError
	 */

	public ArrayList fetchModDtlInfo(String strDeptListId) throws AppError {
		ArrayList alList = new ArrayList();

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_modsetup.gm_fch_moddtlinfo", 2);
		gmDBManager.setInt(1, Integer.parseInt(strDeptListId));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alList;
	}
	
	/**
	 * fetchCourseRptDetail - This method will be used for retrieving all the
	 * course info based on search condition.
	 * 
	 * @param hmParams
	 * @return ArrayList - will be used by displaytag
	 * @exception AppError
	 */

	public ArrayList fetchCourseRptDetail(HashMap hmParams) throws AppError {
		ArrayList alList = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		DBConnectionWrapper db = new DBConnectionWrapper();
		
		String strCourseDeptId = GmCommonClass.parseZero((String) hmParams.get("COURSEDEPTID"));
		String strOwnerId = GmCommonClass.parseZero((String) hmParams.get("OWNERID"));
		String strStatusId = GmCommonClass.parseZero((String) hmParams.get("STATUSID"));
		String strUserId = GmCommonClass.parseNull((String) hmParams.get("STRUSERID"));
		String strModuleId = GmCommonClass.parseZero((String) hmParams.get("MODULEID"));
		String strSessionId = GmCommonClass.parseNull((String) hmParams.get("STRSESSIONID"));
		
		log.debug("..userid.."+strUserId + "...111.."+strModuleId +"..222..."+ strSessionId);
		sbQuery.append("SELECT DISTINCT t6301.c6301_course_id courseid, t6301.c6301_course_id programid");
		sbQuery.append(" , t6301.c6301_course_nm coursename, t6301.c901_primary_dept_id coursedeptid");
		sbQuery.append(" , get_code_name_alt (t6301.c901_primary_dept_id) coursedeptname ");
		sbQuery.append(" , get_user_name (t6301.c101_course_owner) ownername, t6301.c101_course_owner ownerid ");
		sbQuery.append(" , get_code_name (t6301.c901_status) statusname, t6301.c901_status statusid ");
		sbQuery.append(" FROM  t6301_course t6301, t101_user t101 ");
		sbQuery.append(" WHERE  t101.c101_dept_id = DECODE ('" + strCourseDeptId + "', '0', t101.c101_dept_id,'" + strCourseDeptId + "')");		
		sbQuery.append(" AND t6301.c901_status = DECODE ('" + strStatusId + "', '0', t6301.c901_status,'" + strStatusId + "')");
		sbQuery.append(" AND t6301.c101_course_owner = DECODE ('" + strOwnerId + "', '0', t6301.c101_course_owner,'" + strOwnerId + "')");
		sbQuery.append(" AND t6301.c101_course_owner = t101.c101_user_id");
		sbQuery.append(" AND t6301.c6301_void_fl IS NULL");
		if (!"".equals(strUserId)) {
			sbQuery.append(" AND t6301.c6301_course_id IN (select t6302.c6301_course_id from t6302_module t6302, t6304_session_module_map t6304,");
			sbQuery.append(" t6305_session_user_map t6305 where t6302.c6302_module_id = t6304.c6302_module_id");
			sbQuery.append(" and t6304.c6303_session_id = t6305.c6303_session_id and t6305.c101_user_id = '" + strUserId + "')");
		}
		if (!"0".equals(strModuleId)) {
			sbQuery.append(" and t6301.c6301_course_id in (select t6302.c6301_course_id from t6302_module t6302 ");
			sbQuery.append(" where t6302.c6302_module_id = '" + strModuleId + "')");
		}
		if (!"".equals(strSessionId)) {
			sbQuery.append(" and t6301.c6301_course_id in (select t6302.c6301_course_id from t6302_module t6302, t6304_session_module_map t6304");
			sbQuery.append(" where t6302.c6302_module_id = t6304.c6302_module_id and t6304.c6303_session_id = '" + strSessionId + "')");
		}
		
		
		alList = db.queryMultipleRecords(sbQuery.toString());
		return alList;
		
		/* to be deleted
		gmDBManager.setPrepareString("gm_pkg_etr_rpt.gm_fch_courserpt_detail", 4);
		gmDBManager.setString(1, strCourseDeptId);
		gmDBManager.setString(2, strOwnerId);
		gmDBManager.setString(3, strStatusId);

		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		gmDBManager.close();

		return alList;
		*/
	}
	
	/**
	 * fetchModuleRptDetail - This method will be used for retrieving all the
	 * module info based on search condition.
	 * 
	 * @param hmParams
	 * @return ArrayList - will be used by displaytag
	 * @exception AppError
	 */

	public ArrayList fetchModuleRptDetail(HashMap hmParams) throws AppError {
		ArrayList alList = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		DBConnectionWrapper db = new DBConnectionWrapper();		

		String strCourseId =GmCommonClass.parseZero((String) hmParams.get("COURSEID"));
		String strModuleDeptId=GmCommonClass.parseZero((String) hmParams.get("MODULEDEPTID"));
		String strModuleOwnerId=GmCommonClass.parseZero((String) hmParams.get("MODULEOWNERID"));
		String strModuleStatusId=GmCommonClass.parseZero((String) hmParams.get("MODULESTATUSID"));
		String strUserId = GmCommonClass.parseNull((String) hmParams.get("STRUSERID"));
		String strSessionId = GmCommonClass.parseNull((String) hmParams.get("STRSESSIONID"));
		
		log.debug("..userid.."+strUserId + "...1@.."+strCourseId +"..2@..."+ strSessionId);
		
		sbQuery.append("SELECT t6302.c6302_module_id moduleid, t6302.c6302_module_nm modulename");
		sbQuery.append(" , get_user_name (t6302.c101_module_owner) ownername, t6302.c101_module_owner moduleownerid");
		sbQuery.append(" , t101.c101_dept_id moduledeptid, get_code_name_alt (t6301.c901_primary_dept_id) moduledeptname ");
		sbQuery.append(" , t6301.c6301_course_nm coursename, get_code_name (t6302.c901_status) modulestatusname");
		sbQuery.append(" , t6302.c901_status modulestatusid ");
		sbQuery.append(" FROM  t6302_module t6302, t101_user t101, t6301_course t6301 ");
		sbQuery.append(" WHERE  t101.c101_dept_id = DECODE ('" + strModuleDeptId + "', '0', t101.c101_dept_id,'" + strModuleDeptId + "')");	
		sbQuery.append(" AND t6301.c6301_course_id = t6302.c6301_course_id");
		sbQuery.append(" AND t6301.c6301_course_id = DECODE ('" + strCourseId + "', '0', t6301.c6301_course_id,'" + strCourseId + "')");
		sbQuery.append(" AND t6302.c101_module_owner = DECODE ('" + strModuleOwnerId + "', '0', t6302.c101_module_owner,'" + strModuleOwnerId + "')");
		sbQuery.append(" AND t6302.c901_status = DECODE ('" + strModuleStatusId + "', '0', t6302.c901_status,'" + strModuleStatusId + "')");
		sbQuery.append(" AND t6302.c101_module_owner = t101.c101_user_id");
		sbQuery.append(" AND t6302.c6302_void_fl IS NULL");
		
		if (!"".equals(strUserId)) {
			sbQuery.append(" and t6302.c6302_module_id in (select t6304.c6302_module_id from t6304_session_module_map t6304, t6305_session_user_map t6305 ");
			sbQuery.append(" where t6304.c6303_session_id = t6305.c6303_session_id and t6305.c101_user_id = '" + strUserId + "')");
		}
		if (!"".equals(strSessionId)) {
			sbQuery.append(" AND t6302.c6302_module_id in (select t6304.c6302_module_id from t6304_session_module_map t6304");
			sbQuery.append(" where t6304.c6303_session_id = '" + strSessionId + "')");
		}
		/* to be deleted
		gmDBManager.setPrepareString("gm_pkg_etr_rpt.gm_fch_modulerpt_detail", 5);
		gmDBManager.setString(1, strCourseId);
		gmDBManager.setString(2, strModuleDeptId);
		gmDBManager.setString(3, strModuleOwnerId);
		gmDBManager.setString(4, strModuleStatusId);

		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
		gmDBManager.close();

		return alList;
		*/
		alList = db.queryMultipleRecords(sbQuery.toString());
		return alList;
	}
	
	/**
	 * fetchUserRptDetail - This method retrieves the user detail based on
	 * search conditions.
	 * 
	 * @param hmParam -
	 *            parameters to be saved
	 * @exception AppError
	 * @return String
	 */
	public ArrayList fetchUserRptDetail(HashMap hmParam) throws AppError {
		ArrayList alList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		
		String strTraineeId = GmCommonClass.parseZero((String) hmParam.get("TRAINEEID"));
		String strModuleId = GmCommonClass.parseZero((String) hmParam.get("MODULEID"));
		String strCourseId = GmCommonClass.parseZero((String) hmParam.get("COURSEID"));
		String strStartDt = GmCommonClass.parseNull((String) hmParam.get("STARTDT"));
		String strEndDt = GmCommonClass.parseNull((String) hmParam.get("ENDDT"));
		String strStatus = GmCommonClass.parseZero((String) hmParam.get("STATUS"));
		String strDeptId = GmCommonClass.parseZero((String) hmParam.get("USERDEPTID"));
		String strReason = GmCommonClass.parseZero((String) hmParam.get("REASON"));
		String strSesnId = GmCommonClass.parseNull((String) hmParam.get("STRSESSIONID"));
		
		log.debug("...."+strTraineeId+"...."+strModuleId+"...."+strCourseId+"...."+strStartDt+"...."+strEndDt+"...."+strStatus+"......"+strDeptId+"...."+strReason);
		gmDBManager.setPrepareString("gm_pkg_etr_rpt.gm_fch_userrpt_detail", 10);
		gmDBManager.setInt(1, Integer.parseInt(strTraineeId));
		gmDBManager.setString(2, strModuleId);
		gmDBManager.setString(3, strCourseId);
		gmDBManager.setString(4, strStartDt);
		gmDBManager.setString(5, strEndDt);
		gmDBManager.setInt(6, Integer.parseInt(strStatus));
		gmDBManager.setInt(7, Integer.parseInt(strDeptId));
		gmDBManager.setInt(8, Integer.parseInt(strReason));	
		gmDBManager.setString(9, strSesnId);
		gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);
		log.debug("...."+strTraineeId+"...."+strModuleId+"...."+strCourseId+"...."+strStartDt+"...."+strEndDt+"...."+strStatus+"......"+strDeptId+"...."+strReason);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(10));
		gmDBManager.close();		
		log.debug("1@......."+alList);
		return alList;
	}
	
	/**
	 * fetchTrainingRecords - This method will be used for retrieving all the records the users are trained for that session
	 *  
	 * @return ArrayList - will be used by displaytag
	 * @exception AppError
	 */

	public ArrayList fetchTrainingRecords(String strSessionId) throws AppError {
		ArrayList alList = new ArrayList();
		log.debug("strSessionId in bean: "+strSessionId);
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_rpt.gm_fch_training_records", 2);	
		gmDBManager.setString(1, strSessionId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alList;
	}
	
	/**
	 * fetchTrainingSummary - This method will be used for retrieving all the records the users are trained for that session
	 *  
	 * @return ArrayList - will be used by displaytag
	 * @exception AppError
	 */

	public ArrayList fetchTrainingSummary(String strTraineeId) throws AppError {
		ArrayList alList = new ArrayList();
		log.debug("strSessionId in bean: "+strTraineeId);
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_rpt.gm_fch_training_summary", 2);	
		gmDBManager.setString(1, strTraineeId);		
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return alList;
	}
}
