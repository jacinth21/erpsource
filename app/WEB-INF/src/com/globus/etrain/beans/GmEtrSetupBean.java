/**
 * FileName    : GmPOCEtrModSessionInfoBean.java 
 * Description :
 * Author      : rshah
 * Date        : Oct 15, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * @author rshah
 * 
 */
public class GmEtrSetupBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.
	GmCommonBean gmCommonBean = new GmCommonBean();

	/**
	 * savSesnDetail - This method saves the session detail for a module
	 * 
	 * @param hmParam -
	 *            parameters to be saved
	 * @exception AppError
	 * @return String
	 */
	public String savSesnDetail(HashMap hmParam) throws AppError {
		String strSessionIdFromDb = "";
		GmDBManager gmDBManager = new GmDBManager();

		String strSessionId = GmCommonClass.parseNull((String) hmParam.get("SESSIONID"));
		String strSessionDesc = GmCommonClass.parseNull((String) hmParam.get("SESSIONDESC"));
		String strSessionStartDt = GmCommonClass.parseNull((String) hmParam.get("SESSIONSTARTDT"));
		String strSessionEndDt = GmCommonClass.parseNull((String) hmParam.get("SESSIONENDDT"));
		String strOwnerList = GmCommonClass.parseZero((String) hmParam.get("SESSIONOWNER"));
		String strSessionLcnType = GmCommonClass.parseZero((String) hmParam.get("SESSIONLCNTYPE"));
		String strSessionLcn = GmCommonClass.parseNull((String) hmParam.get("SESSIONLCN"));
		String strDuration = GmCommonClass.parseNull((String) hmParam.get("DURATION"));
		String strMinUser = GmCommonClass.parseNull((String) hmParam.get("MINUSER"));
		String strMaxUser = GmCommonClass.parseNull((String) hmParam.get("MAXUSER"));
		String strStatus = GmCommonClass.parseZero((String) hmParam.get("STATUS"));
		// log.debug("in Bean...."+strSessionId+"........"+strSessionDesc);
		String strUserId = (String) hmParam.get("USERID");
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

		gmDBManager.setPrepareString("gm_pkg_etr_setup.gm_sav_sesn_detail", 13);
		gmDBManager.registerOutParameter(13, java.sql.Types.CHAR);

		gmDBManager.setString(1, strSessionId);
		gmDBManager.setString(2, strSessionDesc);
		gmDBManager.setString(3, strSessionStartDt);
		gmDBManager.setString(4, strSessionEndDt);
		gmDBManager.setInt(5, Integer.parseInt(strOwnerList));
		gmDBManager.setInt(6, Integer.parseInt(strSessionLcnType));
		gmDBManager.setString(7, strSessionLcn);
		// log.debug("in Bean...."+strOwnerList+"........"+strSessionLcnType);
		gmDBManager.setInt(8, Integer.parseInt(strDuration));
		gmDBManager.setInt(9, Integer.parseInt(strMinUser));
		gmDBManager.setInt(10, Integer.parseInt(strMaxUser));
		gmDBManager.setInt(11, Integer.parseInt(strStatus));
		gmDBManager.setString(12, strUserId);

		gmDBManager.execute();
		strSessionIdFromDb = gmDBManager.getString(13);
		// log.debug("in
		// Bean...."+strSessionIdFromDb+"........"+strSessionDesc);
		if (!strLogReason.equals("")) {
			gmCommonBean.saveLog(gmDBManager, strSessionIdFromDb, strLogReason, strUserId, "1238");
		}
		gmDBManager.commit();
		return strSessionIdFromDb;
	}

	/**
	 * fetchSesnDetail - This method will be used to retrieve session records
	 * 
	 * @param strSessionId
	 * @return ArrayList
	 * @exception AppError
	 */
	public HashMap fetchSesnDetail(String strSessionId) throws AppError {
		HashMap hmReturn = new HashMap();

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_setup.gm_fch_sesn_detail", 2);
		gmDBManager.setString(1, strSessionId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmReturn = GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2)));
		gmDBManager.close();
		return hmReturn;
	}

	/**
	 * fetchEtrModList - This method will be used to load the module list
	 * 
	 * @param hmParam -
	 *            to be used later for any filters
	 * @return ArrayList - will be used by drop down tag
	 * @exception AppError
	 */
	public ArrayList fetchEtrModList() throws AppError {
		ArrayList alModList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_setup.gm_fch_modlist", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alModList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		// log.debug("In setup bean......"+alModList);
		return alModList;
	}

	/**
	 * fetchEtrCourseList - This method will be used to load the course list
	 * 
	 * @param hmParam -
	 *            to be used later for any filters
	 * @return ArrayList - will be used by drop down tag
	 * @exception AppError
	 */
	public ArrayList fetchEtrCourseList() throws AppError {
		ArrayList alModList = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_setup.gm_fch_courselist", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alModList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alModList;
	}

	/**
	 * fetchEtrCourseDetail - This method will be used to load the course Detail
	 * 
	 * @param String
	 *            strCourseId
	 * @return HashMap - will be used by fields on the screen
	 * @exception AppError
	 */
	public HashMap fetchEtrCourseDetail(String strCourseId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_setup.gm_fch_course_detail", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strCourseId);
		gmDBManager.execute();
		HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return hmReturn;

	}

	/**
	 * Save the course details in db
	 * 
	 * @param hmCourseDetail
	 * @return java.lang.String
	 * @throws com.globus.common.beans.AppError
	 * 
	 */
	public String saveCourseDetail(HashMap hmCourseDetail) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		String strCourseIdFromDB = "";

		String strCourseId = GmCommonClass.parseNull((String) hmCourseDetail.get("COURSEID"));
		String strCourseName = GmCommonClass.parseNull((String) hmCourseDetail.get("COURSENAME"));
		String strDescription = GmCommonClass.parseNull((String) hmCourseDetail.get("DESCRIPTION"));
		String strUserId = GmCommonClass.parseNull((String) hmCourseDetail.get("USERID"));
		String strDeptId = GmCommonClass.parseNull((String) hmCourseDetail.get("DEPTID"));
		String strOwner = GmCommonClass.parseNull((String) hmCourseDetail.get("COURSEOWNER"));
		String strCourseStatus = GmCommonClass.parseNull((String) hmCourseDetail.get("COURSESTATUS"));
		String strLogReason = GmCommonClass.parseNull((String) hmCourseDetail.get("TXT_LOGREASON"));
		gmDBManager.setPrepareString("gm_pkg_etr_setup.gm_sav_course_detail", 7);
		gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);

		gmDBManager.setString(1, strCourseId);
		gmDBManager.setString(2, strCourseName);
		gmDBManager.setString(3, strDescription);
		gmDBManager.setString(4, strUserId);
		gmDBManager.setString(5, strDeptId);
		gmDBManager.setString(6, strOwner);
		gmDBManager.setString(7, strCourseStatus);

		gmDBManager.execute();

		strCourseIdFromDB = gmDBManager.getString(1);
		if (!strLogReason.equals("")) {
			gmCommonBean.saveLog(gmDBManager, strCourseIdFromDB, strLogReason, strUserId, "90883"); 
		}
		
		gmDBManager.commit();

		return strCourseIdFromDB;
	}
	
	
	/**
	 * fetchEtrModuleDetail - This method will be used to load the Module Detail
	 * 
	 * @param String
	 *            strModuleId
	 * @return HashMap - will be used by fields on the screen
	 * @exception AppError
	 */
	public HashMap fetchEtrModuleDetail(String strModuleId) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_etr_setup.gm_fch_module_detail", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strModuleId);
		gmDBManager.execute();
		HashMap hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return hmReturn;
	}

	/**
	 * Save the Module details in db
	 * 
	 * @param hmModuleDetail
	 * @return java.lang.String
	 * @throws com.globus.common.beans.AppError
	 */

	public String saveModuleDetail(HashMap hmModuleDetail) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		String strModuleIdFromDB = "";

		String strModuleId = GmCommonClass.parseNull((String) hmModuleDetail.get("MODULEID"));
		String strModuleName = GmCommonClass.parseNull((String) hmModuleDetail.get("MODULENAME"));
		String strDescription = GmCommonClass.parseNull((String) hmModuleDetail.get("DESCRIPTION"));
		String strCourseId = GmCommonClass.parseNull((String) hmModuleDetail.get("COURSEID"));
		String strModuleStatus = GmCommonClass.parseNull((String) hmModuleDetail.get("MODULESTATUS"));
		String strUserId = GmCommonClass.parseNull((String) hmModuleDetail.get("USERID"));
		String strModuleOwner = GmCommonClass.parseNull((String) hmModuleDetail.get("MODULEOWNER"));		
		String strLogReason = GmCommonClass.parseNull((String) hmModuleDetail.get("TXT_LOGREASON"));
		gmDBManager.setPrepareString("gm_pkg_etr_setup.gm_sav_module_detail", 7);
		gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);

		gmDBManager.setString(1, strModuleId);
		gmDBManager.setString(2, strModuleName);
		gmDBManager.setString(3, strDescription);
		gmDBManager.setString(4, strCourseId);
		gmDBManager.setString(5, strModuleStatus);
		gmDBManager.setString(6, strUserId);
		gmDBManager.setString(7, strModuleOwner);
	
		gmDBManager.execute();

		strModuleIdFromDB = gmDBManager.getString(1);
		if (!strLogReason.equals("")) {
			gmCommonBean.saveLog(gmDBManager, strModuleIdFromDB, strLogReason, strUserId, "90884"); 
		}
		gmDBManager.commit();

		return strModuleIdFromDB;
	}

}
