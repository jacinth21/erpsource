/**
 * FileName    : GmEtrFileUploadServlet.java 
 * Description :
 * Author      : rshah
 * Date        : Dec 4, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.etrain.servlets;

/**
 * @author rshah
 *
 */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadException;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmUploadServlet;

public class GmEtrFileUploadServlet extends GmUploadServlet {
	private static String strDefaultFileToDispatch = "";
	private static String strErrorFileToDispatch = "";
	private String strFileToDispatch = "";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	private static String strComnPath = GmCommonClass.getString("GMCOMMON");

	public void loadFileProperties(String strFileTypeList, String strFileNm) {
		setStrUploadHome(GmCommonClass.getString("ETRUPLOADHOME"));
		String fileExt = strFileNm.substring(strFileNm.lastIndexOf('.'));
		String fileNm = strFileNm.substring(strFileNm.lastIndexOf('\\')+1).substring(0, strFileNm.substring(strFileNm.lastIndexOf('\\')+1).lastIndexOf('.'));
		log.debug("fileExt.....1"+fileExt+"....fileNm....."+fileNm);
		if (strFileTypeList.equals("60386")) { //60386 - For the type of materials
			//setStrFileName(strFileNm + "_REFTP");
			strFileNm = fileNm + "_REFTP";
		} else {
			//setStrFileName(strFileNm + "_CRSEMAT");
			strFileNm = fileNm + "_CRSEMAT";
		}
		File strFile = new File ( GmCommonClass.getString("ETRUPLOADHOME"),strFileNm+fileExt );
		if(strFile.exists())
		{
			String strNewFile=strFileNm+fileExt;
			
			File strFileDir = new File(GmCommonClass.getString("ETRUPLOADHOME"));
			String strFileList[] = strFileDir.list();
			log.debug(".............................."+strFileList.length);
			for(int i=1; i<=strFileList.length+1; i++)
			{				
				String strTempFile = strNewFile.substring(0, strNewFile.substring(strNewFile.lastIndexOf('\\')+1).lastIndexOf('.'))+"_VER_"+i+fileExt;
				File strFileChk = new File(GmCommonClass.getString("ETRUPLOADHOME"),strTempFile );
				if(!strFileChk.exists())
				{
					strFileNm = strNewFile.substring(0, strNewFile.substring(strNewFile.lastIndexOf('\\')+1).lastIndexOf('.'))+"_VER_"+i;
					log.debug("finally...."+strFileNm);
					break;
				}
			}
			//strFileNm = strFileNm.substring(0, strFileNm.substring(strFileNm.lastIndexOf('\\')+1).lastIndexOf('.'))+"_VER_"+1;			
		}
		//else
			//strFileNm = strFileNm.substring(0, strFileNm.substring(strFileNm.lastIndexOf('\\')+1).lastIndexOf('.'));		
		
		setStrFileName(strFileNm);
		setStrWhitelist(GmCommonClass.getString("ETRWHITELIST"));
		strDefaultFileToDispatch = "/etrain/GmEtrFileUpload.jsp";
		strErrorFileToDispatch = strComnPath + "/GmError.jsp";
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		String strAction = "";
		MultipartFormDataRequest mrequest = null;
		checkSession(response, session); // Checks if the current session is
		// valid, else redirecting to
		// SessionExpiry page

		try {
			GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
			String strFileTypeList = "";
			String strFileNm = "";
			String strId = "";
			ArrayList alUploadinfo = new ArrayList();
			ArrayList alFileTypeList = new ArrayList();
			// To load the defauld list for the uploaded material
			strId = GmCommonClass.parseNull((String) request.getParameter("strId"));			
			
			if (MultipartFormDataRequest.isMultipartFormData(request)) 
			{
				mrequest = new MultipartFormDataRequest(request);
				strAction = GmCommonClass.parseNull((String) mrequest.getParameter("hAction"));
				strId = GmCommonClass.parseNull((String) mrequest.getParameter("strId"));
				strFileTypeList = GmCommonClass.parseNull((String) mrequest.getParameter("strFileTypeList"));
				strFileNm = GmCommonClass.parseNull((String) mrequest.getParameter("fileNm"));

				log.debug("strAction: "+ strAction+ " strId: "+ strId+ " strFileTypeList: "+ strFileTypeList +"..strFileNm." +strFileNm);
			}

			if ("upload".equals(strAction)) 
			{
				strFileNm = strFileNm.substring(strFileNm.lastIndexOf('\\')+1);
				//strFileNm = strFileNm.substring(strFileNm.lastIndexOf('\\')+1).substring(0, strFileNm.substring(strFileNm.lastIndexOf('\\')+1).lastIndexOf('.'));
				
				loadFileProperties(strFileTypeList, strFileNm);
				uploadFile(mrequest, request);
				log.debug("strFileTypeList: "+ strFileTypeList+ " strId: "+ strId+ " getStrFileName: "+ getStrFileName() + "...strSessUserId..." +(String) session.getAttribute("strSessUserId"));
				gmCommonUploadBean.saveUploadInfo(strId, strFileTypeList, getStrFileName(), (String) session.getAttribute("strSessUserId"));
			}	
			alUploadinfo = gmCommonUploadBean.fetchUploadInfo(strFileTypeList, strId);
			alFileTypeList = GmCommonClass.getCodeList("MATTP");

			request.setAttribute("mrequest", mrequest);
			request.setAttribute("hAction", strAction);
			request.setAttribute("alFileTypeList", alFileTypeList);
			request.setAttribute("strFileTypeList", strFileTypeList);
			request.setAttribute("strId", strId);
			request.setAttribute("alUploadinfo", alUploadinfo);
			// upload page if load or reload
			dispatch("/etrain/GmEtrFileUpload.jsp", request, response);
		} catch (AppError e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			request.setAttribute("hType", e.getType());
			dispatch(strFileToDispatch, request, response);
		} catch (UploadException e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		} catch (Exception uex) {
			session.setAttribute("hAppErrorMessage", uex.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		}
	}
}

