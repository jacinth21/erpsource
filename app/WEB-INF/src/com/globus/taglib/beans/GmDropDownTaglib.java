/*****************************************************************************
 * File			 : GmDropDownTaglib
 * Desc		 	 : This taglib will be used to print Comobo value (Dropdown list value)
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.taglib.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.beanutils.PropertyUtils;

public class GmDropDownTaglib extends TagSupport
{

    String strControlName 	= "";
    String strSelectedValue = "";
    String strDefaultValue 	= "";
    String strClass			= "RightText";
    String strWidth 		= "";
    String strId			= "ID";
    String strName			= "NAME";
    String strDisabled      = "";
    String strOnChange      = "";
    String strFormName      = "";
    String strControlId     = "";
    String strOptionId      = ""; 
    String strDefaultId		= "";
    String strOnBlur      = "";
    
    Object objBean;
    
    int intTabIndex	= 0;
    
    boolean isBoldValues;
    
    
    ArrayList alList = new ArrayList();
    HashMap hcboVal = new HashMap();
    
    StringBuffer sbDetails = new StringBuffer();
    
    public void setControlName(String Value)
    {
        strControlName  = Value; 
    }

    // optional set the class type of the combobox
    public void setClass (String Value)
    {
        strClass = Value;
    }

    // To set the combo default value like [All Account] [All] 
    public void setDefaultValue (String Value)
    {
        strDefaultValue = Value;
    }
    
    // To set the combo default Id like [Choose One] [All] 
    public void setDefaultId (String Value)
    {
    	strDefaultId = Value;
    }

    // optional to specify the selected value 
    public void setSeletedValue (String Value)
    {
        strSelectedValue = Value;
    }

    // Optional to specify the width of the Tabindex
    public void setWidth (int value )
    {
        strWidth = " style=\"width:" + value + "px\"";
    }
    
    // get the combo list value
    public void setValue(ArrayList Value)
    {
        alList  = Value; 
    }
    
    // get the combo tabindex value 
    public void setTabIndex(int Value)
    {
        intTabIndex  = Value; 
    }
    
    public void setCodeId(String value)
    {
    	strId = value;
    }
    
    public void setCodeName(String value)
    {
    	strName = value;
    }
    
    public void setDisabled(String value)
    {
        strDisabled = value;
    }
   
    public void setOnChange (String value )
    {
        strOnChange =  value ;
    }
    
    // get the combo list value for Struts
    public void setSFValue(String Value)
    {
        try {
            this.alList  = (ArrayList)PropertyUtils.getProperty(objBean,Value);
        }
        catch(Exception exp){
            exp.printStackTrace();
        }
    }
    
    // optional to specify the selected value of Struts Framework
    public void setSFSeletedValue (String Value)
    {
        try {
            this.strSelectedValue  = (String)PropertyUtils.getProperty(objBean,Value);
        }
        catch(Exception exp){
            exp.printStackTrace();
        }
    }
    
    //  optional to set the Form Name
    public void setSFFormName(String Value)
    {
        this.strFormName = Value;
        Object objTemp = getFormInstance();
        setTempObject(objTemp);
    }
    
    public void setTempObject(Object objTemp)
    {
        this.objBean = objTemp;
    }
    
    public Object getFormInstance()
    {
        return pageContext.getRequest().getAttribute(this.strFormName);
    }
    
    public void setControlId(String strValue)
    {
        this.strControlId = strValue;
    }
    
    public void setOptionId(String strOptionId) {
		this.strOptionId = strOptionId;
	}
    
	public void setOnBlur(String value ) {
		this.strOnBlur = " onBlur=javascript:".concat(value);
	}
	public void setBoldValues(Boolean Value)
	{
		isBoldValues  = Value; 
	}
	
    public int doStartTag() {
		int	intSize = 0;
		String strCodeID;
		String strSelected;

        try {
            
           JspWriter out = pageContext.getOut();
           sbDetails.setLength(0);
           sbDetails.append("<select name=\"" );
           sbDetails.append(strControlName);
           sbDetails.append("\" id=\"" );
           if(strControlId.equals("")){
             sbDetails.append(strControlName);
           }else{
             sbDetails.append(strControlId); 
           }
           if(isBoldValues){
        	   strClass = "RightTableCaption";
           }
           sbDetails.append("\" class=\"" + strClass + "\" ");
           sbDetails.append("tabindex=" + intTabIndex + strWidth +  "  ");
           sbDetails.append(strDisabled);
           sbDetails.append(" onChange=\"" + strOnChange + "\"");
           sbDetails.append(strOnBlur);
           sbDetails.append(" > ");
           
           // To get the optional value 
           if ( !strDefaultValue.equals("") )
           {
               sbDetails.append("<option value=0 id=0 >" + strDefaultValue);
           }
           
           // To get the optional value 
           if ( !strDefaultId.equals("") )
           {
               sbDetails.append("<option value=-1 id=0 >" + strDefaultId);
           }

           String strTemp = "";
           intSize =  alList.size();
           for (int i=0;i<intSize;i++)
           {
				hcboVal = (HashMap)alList.get(i);
				strCodeID = (String)hcboVal.get(strId);
				strSelected = strSelectedValue.equals(strCodeID)?"selected":"";
				sbDetails.append("<option " + strSelected + " value= '" ) ;
				sbDetails.append( strCodeID + "'  id="+ hcboVal.get(strOptionId) + ">" + hcboVal.get(strName) + "</option>");
				
				strTemp = strTemp + "'" + hcboVal.get("NAME") + "' , ";
           }
           sbDetails.append( "</select>");
       
           // To print the final value
           out.print(sbDetails.toString());
                   
        } catch (IOException ioe)
        {
            System.out.println("Error in RepeatTag: " + ioe); 
        }
        return (SKIP_BODY);
    }

	

}// End of Servlet