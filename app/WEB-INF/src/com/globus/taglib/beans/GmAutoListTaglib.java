/*****************************************************************************
 * File : GmDropDownTaglib Desc : This taglib will be used to print Autolist value (Dropdown list
 * value) Works like a normal dropdown
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.taglib.beans;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;

public class GmAutoListTaglib extends TagSupport {

  String strControlName = "";
  String strSelectedValue = "";
  String strDefaultValue = "";
  String strClass = "RightText";
  String strWidth = "";

  String strDisabled = "";
  String strOnChange = "";
  String strFormName = "";

  String strCodeId = "ID";
  String strCodeName = "NAME";
  String strOnBlur = "";
  String strBlur = "";
  String strChange = "";
  String strComboType = "";
  int intWidth = 150;

  Object objBean;
  /**
   * @param strOnChange The strOnChange to set.
   */
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public void setOnBlur(String value) {
    this.strOnBlur = "onBlur='".concat(value) + "'";
    value = GmCommonClass.parseNull(value);
    if (value.length() == 0) {
      value = "";
    }
    strBlur = value;
  }

  /**
   * @param strOnChange The strOnChange to set.
   */
  public void setOnChange(String value) {
    this.strOnChange = "onChange=javascript:".concat(value);
    value = GmCommonClass.parseNull(value);
    if (value.length() == 0) {
      value = "";
    }
    strChange = value;
  }

  int intTabIndex = 0;


  ArrayList alList = new ArrayList();


  public void setControlName(String Value) {
    strControlName = Value;
  }

  // optional set the class type of the combobox
  public void setClass(String Value) {
    strClass = Value;
  }

  // optional to specify the selected value
  public void setSeletedValue(String Value) {
    strSelectedValue = Value;
  }

  // To set the combo default value like [All Account] [All]
  public void setDefaultValue(String Value) {
    strDefaultValue = Value;
  }

  // Optional to specify the width of the Tabindex
  public void setWidth(int value) {
    strWidth = " style=\"width:" + value + "px\"";
    intWidth = value;
  }

  // get the combo list value
  public void setValue(ArrayList Value) {
    alList = Value;
  }

  // get the combo tabindex value
  public void setTabIndex(int Value) {
    intTabIndex = Value;
  }

  // get the Combo Type
  public void setComboType(String Value) {
    strComboType = Value;
  }

  /**
   * @param strCodeId the strCodeId to set
   */
  public void setCodeId(String strCodeId) {
    this.strCodeId = strCodeId;
  }

  /**
   * @param strCodeName the strCodeName to set
   */
  public void setCodeName(String strCodeName) {
    this.strCodeName = strCodeName;
  }

  public String getStrDisabled() {
    return strDisabled;
  }

  public void setStrDisabled(String strDisabled) {
    this.strDisabled = strDisabled;
  }

  // optional to set the Form Name
  public void setSFFormName(String Value) {
    this.strFormName = Value;
    Object objTemp = getFormInstance();
    setTempObject(objTemp);
  }

  public void setTempObject(Object objTemp) {
    this.objBean = objTemp;
  }

  public Object getFormInstance() {
    return pageContext.getRequest().getAttribute(this.strFormName);
  }

  // get the combo list value for Struts
  public void setSFValue(String Value) {
    try {
      this.alList = (ArrayList) PropertyUtils.getProperty(objBean, Value);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
  }

  // optional to specify the selected value of Struts Framework
  public void setSFSeletedValue(String Value) {
    try {
      this.strSelectedValue = (String) PropertyUtils.getProperty(objBean, Value);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
  }


  /**
   * generateInvoice - This method is called to generate autolist for the selected value Accepts
   * following mandatory parameter as properties setValue -- Accepts Arraylist as input parameter
   * setControlName -- Accepts Control Name setTabIndex -- Accepts Control TabIndex
   * 
   * Other optional parameter ------------------------ setSeletedValue -- Holds selected value
   * setDefaultValue -- Holds default value setWidth -- Sets the width of the Text box setClass --
   * Sets the css class path (defaults to RightText)
   * 
   * @exception AppError
   **/

  @Override
  @SuppressWarnings("deprecation")
  public int doStartTag() {

    String strImagePath = GmCommonClass.getString("GMIMAGES");
    try {
      JspWriter out = pageContext.getOut();
      StringBuffer comboDetails = new StringBuffer();
      if (strComboType == "DhtmlXCombo") {
        comboDetails = generateDhtmlXCombo();
      } else {
        comboDetails = generateAutoList();
      }

      // To print the final value6
      out.print(comboDetails.toString());
    } catch (Exception e) {
    }

    return (SKIP_BODY);
  }

  private StringBuffer generateDhtmlXCombo() throws Exception {
    StringBuffer sbDetails = new StringBuffer();
    sbDetails.setLength(0);

    String strDivRef = "div" + strControlName;
    String strJsRef = strControlName;

    HashMap hmComboMap = new HashMap();
    hmComboMap.put("alResult", alList);

    hmComboMap.put("ID", strCodeId);
    hmComboMap.put("NAME", strCodeName);

    hmComboMap.put("DEFAULTVALUE", strDefaultValue);
    hmComboMap.put("selectedCodeId", strSelectedValue);
    String strXmlData = generateOutPut(alList, hmComboMap);
    sbDetails.append("<span id=\"" + strDivRef
        + "\" class=\"dhxspan\" style=\"width:150px;\"></span>");
    sbDetails.append(" <input type=\"hidden\" name=\"h" + strControlName + "\" id=\"h"
        + strControlName + "\" value=\'" + strXmlData + "\'>   ");
    sbDetails
        .append("<script language=\"JavaScript\"  src=\"/javascript/GmDhtmlxCombo.js\"></script>");
    sbDetails.append("<script>var " + strJsRef + " = loadDXCombo('" + strDivRef + "','"
        + strControlName + "',document.getElementById('h" + strControlName + "').value,'" + strBlur
        + "','" + strChange + "'," + intWidth + "," + intTabIndex + ")</script>");
    return sbDetails;
  }

  private StringBuffer generateAutoList() throws Exception {
    String strName = "";
    String strCodeID;
    String strSelected;
    String strSetName = "";
    String strSetValue = "0";
    int intSize = 0;
    StringBuffer strNameArray = new StringBuffer();
    StringBuffer strIDArray = new StringBuffer();
    StringBuffer sbDetails = new StringBuffer();
    HashMap hcboVal = new HashMap();
    if (!strDefaultValue.equals("")) {
      strSetName = strDefaultValue;
      strNameArray.append(" '" + strDefaultValue + "' ,");
      strIDArray.append(" '0' ,");
    }

    intSize = alList.size();

    for (int i = 0; i < intSize; i++) {
      hcboVal = (HashMap) alList.get(i);
      strCodeID = (String) hcboVal.get(strCodeId);
      strName = (String) hcboVal.get(strCodeName);

      if (strSelectedValue.equals(strCodeID)) {
        strSetName = strName;
      }

      strNameArray.append(" '" + GmCommonClass.formatSpecialCharFromDB(strName) + "' ,");
      strIDArray.append(" '" + strCodeID + "' ,");
    }

    sbDetails.setLength(0);
    // Final Script Framing
    sbDetails.append("<script>");
    sbDetails.append("var " + strControlName + "Nmarr = new Array(");
    sbDetails.append(strNameArray.substring(0, strNameArray.length() - 1));
    sbDetails.append("); \n");
    sbDetails.append("var " + strControlName + "IDarr = new Array(");
    sbDetails.append(strIDArray.substring(0, strIDArray.length() - 1));
    sbDetails.append("); ");

    sbDetails.append(" </script> ");

    // To set the control name
    sbDetails.append("<input type='text'  id='" + strControlName + "tb' ");
    sbDetails.append(strOnBlur);
    // sbDetails.append(" onBlur =" + strOnBlur);
    sbDetails.append(strWidth + " Class = '" + strClass);
    sbDetails.append("' value='" + strSetName + "'TabIndex = '" + intTabIndex + "' />");
    sbDetails.append("<input class= 'xx' type='hidden' name='" + strControlName + "' value='"
        + strSelectedValue + "' id='" + strControlName + "'>");
    sbDetails.append("<img id='ig" + strControlName + "' border='0' align='top' tabindex=-1 ");
    sbDetails.append("src='/images/combobtn.gif' height='20' width='13' /> ");
    // sbDetails.append("<input type='text'  id='td' value=''/> ");

    // Send the Array to the Script
    sbDetails.append("<script>");
    sbDetails.append("var obj = ");
    sbDetails.append(" actb(document.getElementById('" + strControlName + "tb') , "
        + strControlName + "Nmarr, ");
    sbDetails.append(" document.getElementById('" + strControlName + "'), " + strControlName
        + "IDarr, ");
    sbDetails.append(" document.getElementById('ig" + strControlName + "')  );");
    sbDetails.append("</script>");
    return sbDetails;
  }

  private String generateOutPut(ArrayList alComboData, HashMap hmCombo) throws Exception {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alComboData);
    templateUtil.setDataMap("hmCombo", hmCombo);
    templateUtil.setTemplateSubDir("custservice/templates");
    templateUtil.setTemplateName("GmCombAutoList.vm");
    return templateUtil.generateOutput();
  }
}// End of Servlet
