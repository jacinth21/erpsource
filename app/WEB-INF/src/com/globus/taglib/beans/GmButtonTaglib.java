package com.globus.taglib.beans;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;


import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.beanutils.PropertyUtils;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;

/**
 * @author gopinathan
 *
 */
public class GmButtonTaglib extends TagSupport{
	
	private String strOnClick="";
	private String strValue="";
	private String strControlName="";
	private String strStyle="";
	private String strAlign="";
	private String strTD="";
	private String strWidth = "";
	private String strColspan = "";
	private String strAccesskey = "";
	private String strDisabled= "";
	private String strBrowserType ="";
	private String strClientSysType ="";
	private String strSubmitBtnDisableFl = "";
	private String strTabIndex="";
	private String strButtonTag = "";
	private String strClass	= "";
	private String strControlId = "";
	private String strButtonType = "";
	private String strOnMouseDown="";
	private boolean blDisableFl = false;


	protected HttpSession session;  
	
	StringBuffer sbDetails = new StringBuffer();
	
	public void setDisabled(String strDisabled)
	{
	    this.strDisabled  = strDisabled; 
	}		
	public void setAlign(String strAlign)
    {
        this.strAlign  = strAlign; 
    }	

	public void setWidth(String strWidth) {
		this.strWidth = strWidth;
	}
	
	public void setTD(String strTD)
    {
        this.strTD  = strTD; 
    }
	
	public void setStyle(String strStyle)
    {
        this.strStyle  = strStyle; 
    }	
	
	public void setValue(String strValue)
    {
		this.strValue = strValue;
    }
	
    public void setName(String strControlName)
    {
    	this.strControlName  = strControlName;        
    }
    
    public void setOnClick(String strOnClick) {
		this.strOnClick = strOnClick;
	}

	public void setAccesskey(String strAccesskey)
	{
	    this.strAccesskey  = strAccesskey; 
	}    
    
    public void setColspan(String strColspan) {
		this.strColspan = strColspan;
	}	
    
    /**
	 * @param tabindex the tabindex to set
	 */
	public void setTabindex(String strTabIndex) {
		this.strTabIndex = strTabIndex;
	}	
	
  	/**
	 * @param ButtonTag the ButtonTag to set
	 */
	public void setButtonTag(String strButtonTag) {
		this.strButtonTag = strButtonTag;
	}	
	
    // optional set the class type of the combobox
    public void setGmClass (String strValue)
    {
        strClass = strValue;
    }
	/**
	 * @param strControlId the strControlId to set
	 */
	public void setControlId(String strControlId) {
		this.strControlId = strControlId;
	}
	/**
	 * @param buttonType the buttonType to set
	 */
	public void setButtonType(String strButtonType) {
		this.strButtonType = strButtonType;
	}

	/**
	 * @param OnMouseDown the OnMouseDown to set
	 */	
    public void setOnMouseDown(String strOnMouseDown) {
		this.strOnMouseDown = strOnMouseDown;
	}
	
	public Object getReqObj()
    {
        return pageContext.getRequest();
    }
	
	public int doStartTag() {
		try {
			String strUserAgent = GmCommonClass.parseNull((String)((HttpServletRequest)getReqObj()).getHeader("user-agent"));
			/*
			String strIE8 = "";
			//If IE version is less than 9 then Blue button will appear instead of red button
			
			if (strUserAgent.toUpperCase().indexOf("MSIE 8.") > -1){
				strIE8 = "YES";
			}
			*/
			session = ((HttpServletRequest)pageContext.getRequest()).getSession(false);
			strBrowserType = GmCommonClass.parseNull((String)session.getAttribute("strSessBrowserType"));		
			strClientSysType = GmCommonClass.parseNull((String)session.getAttribute("strSessClientSysType"));
			strSubmitBtnDisableFl = GmCommonClass.parseNull((String)session.getAttribute("strSwitchUserFl"));
			
			JspWriter out = pageContext.getOut();
			sbDetails.setLength(0);
			if(strTD.equalsIgnoreCase("true")){
	        	sbDetails.append("  <td");
	        	if(!strWidth.equalsIgnoreCase("")){
	        		sbDetails.append("  width=\"");
	        		sbDetails.append(strWidth);
	        		sbDetails.append("\"");
	        	}
	        	sbDetails.append("\" align=\"");
				if(!strAlign.equalsIgnoreCase("")){
					sbDetails.append(strAlign);
				}else{
					sbDetails.append("left");
				}
	        	sbDetails.append("\" colspan=\"");
				if(!strColspan.equalsIgnoreCase("")){
					sbDetails.append(strColspan);
				}else{
					sbDetails.append("1");
				}
				sbDetails.append("\">");
			}
			if(strClass.equals("")){
				strClass = "Button";
			}
			if(strButtonTag.equalsIgnoreCase("true")){
				sbDetails.append("<button name=\"");
			}else{
					sbDetails.append("<input type=\"button\" name=\"");
			}
					if(!strControlName.equals("")){
						sbDetails.append(strControlName);
					}
					sbDetails.append("\" id=\"");
					if(!strControlId.equals("")){
						sbDetails.append(strControlId);
					}
					sbDetails.append("\" style=\"");
					if(!strStyle.equals("")){
						sbDetails.append(strStyle);
					}
		        	sbDetails.append("\"  class=\"");
		        	if(strBrowserType.equalsIgnoreCase("IE") || strClientSysType.equalsIgnoreCase("PC")){
		        		sbDetails.append(strClass.equalsIgnoreCase("Button")?"Button":strClass);
					}else{
						sbDetails.append("button-red");
					}
					sbDetails.append("\" value=\"" );
					sbDetails.append(strValue);
					sbDetails.append("\" accesskey=\"" );
					if(!strAccesskey.equals("")){
						sbDetails.append(strAccesskey); 
					}
					sbDetails.append("\" " );
					if(strSubmitBtnDisableFl.equalsIgnoreCase("Y")){
						if (!strButtonType.equalsIgnoreCase("Load")) {
							blDisableFl = true;
							sbDetails.append(" disabled=\"disabled\" ");
						}
					}
		        	if(strDisabled.equalsIgnoreCase("true") && !blDisableFl){
		        		sbDetails.append(" disabled=\"disabled\" ");	
		        	}
					sbDetails.append(" onClick=\"" );
		        	if(!strOnClick.equals("")){
		        		sbDetails.append(strOnClick);
					}
		        	sbDetails.append("\" onmousedown=\"" );
		        	if(!strOnMouseDown.equals("")){
		        		sbDetails.append(strOnMouseDown);
					}
		        	sbDetails.append("\" tabindex=\"");
		        	if(!strTabIndex.equals("")){
		        		sbDetails.append(strTabIndex);
		        	}
		        	sbDetails.append("\"");
		        	
			if(strButtonTag.equalsIgnoreCase("true")){
				sbDetails.append(">"+ strValue +"</button>");
			}else{
				sbDetails.append("/>");
			}
			if(strTD.equalsIgnoreCase("true")){
				sbDetails.append("</td>");
			}
	        out.write(sbDetails.toString());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return (SKIP_BODY);
	}
	
}
