/**
 * 
 */
package com.globus.taglib.beans;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.beanutils.PropertyUtils;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonConstants;

/**
 * @author rshah
 *
 */
public class GmLabelTaglib extends TagSupport{
	
	private String strType="";
	private String strAlign="";
	private String strTD="";
	private String strRegularText="RightText";
	private String strBoldText="RightTableCaption";
	private String strMandFont="<font color=\"red\">*</font>&nbsp;";
	private String strLabel="";
	
	private String strValue="";
	private String strFormName = "";
	private String strControlName="";
	private String strClass="";
	private String strWidth = "";
	
	protected HttpSession session;  
	
	StringBuffer sbDetails = new StringBuffer();
	String strDivRef="";
	
	Date dtDate;
	Object objBean;
	
	private String dateFormat="";
	
	
	public void setClass(String Value)
	    {
	        strClass  = Value; 
	    }	
	
	public void setTempObject(Object objTemp)
    {
        this.objBean = objTemp;
    }
	
	//Code for calendar date change based on format for Struts
    public void setSFLblControlName(String Value)
    {
        strControlName  = Value;        
    }
    
    public void setType(String strType) {
		this.strType = strType;
	}

	public void setAlign(String strAlign) {
		this.strAlign = strAlign;
	}
	
	public void setTd(String strTD) {
		this.strTD = strTD;
	}
	
	public void setWidth(String strWidth) {
		this.strWidth = strWidth;
	}
	public int doStartTag() {
		try {
			JspWriter out = pageContext.getOut();
			sbDetails.setLength(0);
	        if(strType.equalsIgnoreCase("RegularText") || strType.equalsIgnoreCase("BoldText") || strType.equalsIgnoreCase("MandatoryText")){
	        	if(!strTD.equalsIgnoreCase("false")){
		        	sbDetails.append("  <td");
		        	if(!strWidth.equalsIgnoreCase("")){
		        		sbDetails.append("  width=\"");
		        		sbDetails.append(strWidth);
		        		sbDetails.append("\"");
		        	}
		        	sbDetails.append("  class=\"");
		        	if(!strClass.equalsIgnoreCase("")){
						sbDetails.append(strClass);
					}else{
			        	if(strType.equalsIgnoreCase("RegularText")){
			        		sbDetails.append(strRegularText);
			        	}else{
			        		sbDetails.append(strBoldText);
			        	}
					}
		        	sbDetails.append("\" align=\"");
					if(!strAlign.equalsIgnoreCase("")){
						sbDetails.append(strAlign);
					}else{
						sbDetails.append("Right");
					}
					sbDetails.append("\">");
	        	}
				if(strType.equalsIgnoreCase("MandatoryText")){
					sbDetails.append(strMandFont);
				}
				sbDetails.append(strControlName);
				if(!strTD.equalsIgnoreCase("false")){
					sbDetails.append("</td>");
				}
	        }
	        out.write(sbDetails.toString());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return (SKIP_BODY);
	}
	
}
