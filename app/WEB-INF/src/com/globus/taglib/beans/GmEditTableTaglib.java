/*****************************************************************************
 * File			 : GmDropDownTaglib
 * Desc		 	 : This taglib will be used to print Comobo value (Dropdown list value)
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.taglib.beans;

import java.io.IOException;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class GmEditTableTaglib extends BodyTagSupport 
{

    private String controlid 	= "";
    private String title 		= "";
    private String format 		= "";
    private String defaultvalue = "";
    private int maxLength;
    private boolean totaled;
    private ArrayList alColumnList = new ArrayList();

    public void addColumn(HashMap value)
    {
        alColumnList.add(value);
    }
    
    /**
     * Only basic variable setting will be made in StartTag
     */
    public int doStartTag() {
      
        return (SKIP_BODY);
    }
    
    /**
     * Draw the table. This is where everything happens, we figure out what values we are supposed to be showing, we
     * figure out how we are supposed to be showing them, then we draw them.
     * @return int
     * @throws JspException generic exception
     * @see javax.servlet.jsp.tagext.Tag#doEndTag()
     */
    public int doEndTag() throws JspException
    {
        // Get the data back in the representation that the user is after, do they want HTML/XML/CSV/EXCEL/etc...
        int returnValue = EVAL_PAGE;
        
        return returnValue;
    }

}// End of Servlet