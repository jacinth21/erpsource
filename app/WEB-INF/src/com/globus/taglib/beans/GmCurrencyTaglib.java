package com.globus.taglib.beans;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.beanutils.PropertyUtils;

import com.globus.common.beans.GmCommonClass;

public class GmCurrencyTaglib extends TagSupport {

  private String strProperty = "";
  private String size = "10";
  private String maxlength = "20";
  private String onfocus = "";
  private String styleClass = "";
  private String onblur = "";
  private String tabIndex = "";
  private String strValue = "";
  private String strToValue = "";
  private String strFormName = "";
  StringBuffer sbDetails = new StringBuffer();
  Object objBean;
  private String strControlName = "";
  private String strToControlName = "";
  private String strClass = "";
  private String strOnFocus = "";
  private String strOnBlur = "";
  private String view = "single";
  private String strJsFunction = "";
  private String strCurr = "";
  private String currSymbol = "";
  private String strType = "";
  private String strCurrSymbol = "";
  private String strWidth = "";
  private String strHeight = "";
  private String strAlign = "";
  private String strLabel = "";
  private String strTextBoxClass = "InputArea";
  private String strRegularText = "RightText";
  private String strBoldText = "RightTableCaption";
  private String strMandFont = "<font color=\"red\">*</font>&nbsp;";
  private String strDisabled = "disabled";
  private String strTDHeight = " Height=\"25\" ";
  protected HttpSession session;
  String strDivRef = "";
  java.sql.Date dtDate;
  private String strTD = "";
  private String strCurrPos = "";
  private String strGmClass = "";

  public void setCurrpos(String strCurrPos) {
    this.strCurrPos = strCurrPos;
  }

  public String getTd() {
    return strTD;
  }

  public void setTd(String strTD) {
    this.strTD = strTD;
  }

  private String currFormat = "";

  public void setLabel(String strLabel) {
    this.strLabel = strLabel;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public void setAlign(String strAlign) {
    this.strAlign = strAlign;
  }

  public void setWidth(String strWidth) {
    this.size = strWidth;
  }

  public void setHeight(String strHeight) {
    this.size = strHeight;
  }

  public void setType(String strType) {
    this.strType = strType;
  }

  public void setCurrSymbol(String strCurrSymbol) {
    this.strCurrSymbol = strCurrSymbol;
  }

  public void setClass(String strClass) {
    this.strClass = strClass;
  }

  public void setGmClass(String strGmClass) {
    this.strClass = strGmClass;
  }

  public void setControlName(String Value) {
    strControlName = Value;
    this.setSFValue(Value);

  }

  public void setToControlName(String Value) {
    strToControlName = Value;
    this.setSFToValue(Value);
  }

  /**
   * @param strProperty the strProperty to set
   */
  public void setProperty(String property) {
    this.strProperty = property;
  }

  /**
   * @param strProperty the strProperty to set
   */
  public void setStrProperty(String strProperty) {
    this.strProperty = strProperty;
  }

  /**
   * @param maxlength the maxlength to set
   */
  public void setMaxlength(String maxlength) {
    this.maxlength = maxlength;
  }


  /**
   * @param onblur the onblur to set
   */

  public void setSFValue(String Value) {
    try {
      this.strValue = (String) PropertyUtils.getProperty(objBean, Value);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
  }

  public void setSFToValue(String Value) {
    try {
      this.strToValue = (String) PropertyUtils.getProperty(objBean, Value);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
  }

  public void setSFFormName(String Value) {
    this.strFormName = Value;
    Object objTemp = getFormInstance();
    setTempObject(objTemp);
  }

  public void setTempObject(Object objTemp) {
    this.objBean = objTemp;
  }

  public Object getFormInstance() {
    return pageContext.getRequest().getAttribute(this.strFormName);
  }

  public void setOnBlur(String value) {
    this.strOnBlur = " onBlur=\"javascript:".concat(value);
  }

  public void setOnFocus(String value) {
    this.strOnFocus = " onFocus=\"javascript:".concat(value);
  }

  /**
   * @param tabindex the tabindex to set
   */
  public void setTabIndex(String tabIndex) {
    this.tabIndex = tabIndex;
  }

  public void setCurrFormat(String currFormat) {
    this.currFormat = currFormat;
  }

  public Object getReqObj() {
    return pageContext.getRequest();
  }

  // Used only for the JSPs which are not using forms
  public void setTextControlName(String Value) {
    strControlName = Value;
  }

  // Used only for the JSPs which are not using forms
  public void setTextValue(String strValue) {
    this.strValue = strValue;
  }

  public String setCurrSymbolColor(String strCurr, String strCurrSymbol) {
    if (strCurr.charAt(0) == '-') {
      return ("<span class=RightTextRed>" + strCurrSymbol + "</span>");
    } else {
      return (strCurrSymbol);
    }
  }

  public String setCurrValue(String strCurr) {
    session = ((HttpServletRequest) getReqObj()).getSession(false);
    String strVal = "";
    if (strCurr != null) {
      String strApplCurrFmt =
          GmCommonClass.parseNull((String) session.getAttribute("strSessApplCurrFmt"));
      strVal = GmCommonClass.getRedText(GmCommonClass.getStringWithCommas(strCurr, strApplCurrFmt));
    } else {
      this.strValue = "";
    }
    return strVal;
  }

  public int doStartTag() {
    try {
      JspWriter out = pageContext.getOut();
      session = ((HttpServletRequest) getReqObj()).getSession(false);
      // currSymbol = GmCommonClass.parseNull((String) session.getAttribute("strSessCurrSymbol"));
      // if strCurrSymbol set from screen then we take the new currency 
      // otherwise getting from session.
      if (strCurrSymbol.equals("")) {
        currSymbol =
            GmCommonClass.parseNull((String) ((HttpServletRequest) getReqObj())
                .getAttribute("hCurrSymb"));

        currSymbol = currSymbol.equals("") ? GmCommonClass.parseNull((String) session.getAttribute("strSessCurrSymbol")) : currSymbol;
      } else {
        currSymbol = strCurrSymbol;
      }
      if (strCurrPos.equals("")) {
        strCurrPos = GmCommonClass.getString("CURRPOS");
      }
      // System.out.println("strCurrPos++++++++++++++"+strCurrPos);
      sbDetails.setLength(0);

      if (strType.equalsIgnoreCase("TextBox") || strType.equalsIgnoreCase("TextBoxSign")
          || strType.equalsIgnoreCase("ReadOnlyTextBox")
          || strType.equalsIgnoreCase("ReadOnlyTextBoxSign")) {
        if (!strTD.equalsIgnoreCase("false")) {
          sbDetails.append(" <td");
          sbDetails.append(strTDHeight);
          sbDetails.append(">&nbsp;");
        }
        if ((strType.equalsIgnoreCase("TextBoxSign") || strType
            .equalsIgnoreCase("ReadOnlyTextBoxSign")) && !strCurrPos.equalsIgnoreCase("right")) {
          sbDetails.append(currSymbol);
        }
        sbDetails.append(" <input type=\"text\" name=\"");
        sbDetails.append(strControlName);
        sbDetails.append("\" size=\"");
        sbDetails.append(size);
        sbDetails.append("\" value=\"");
        sbDetails.append(strValue);
        sbDetails.append("\" class=\"");
        if (!strClass.equalsIgnoreCase("")) {
          sbDetails.append(strClass);
        } else {
          sbDetails.append(strTextBoxClass);
        }
        sbDetails.append("\" tabindex=\"");
        sbDetails.append(tabIndex + "\"");
        sbDetails.append(strOnBlur + "\" ");
        sbDetails.append(strOnFocus);
        sbDetails.append("\"  maxlength=\"");
        sbDetails.append(maxlength);
        sbDetails.append("\"");
        if (strType.equalsIgnoreCase("ReadOnlyTextBox")
            || strType.equalsIgnoreCase("ReadOnlyTextBoxSign")) {
          sbDetails.append(strDisabled);
        }
        sbDetails.append("> ");
        if (strCurrPos.equalsIgnoreCase("right")) {
          sbDetails.append(currSymbol);
        }
        if (!strTD.equalsIgnoreCase("false")) {
          sbDetails.append("</td>");
        }
      } else if (strType.equalsIgnoreCase("RegularText") || strType.equalsIgnoreCase("BoldText")
          || strType.equalsIgnoreCase("MandatoryText")) {
        if (!strTD.equalsIgnoreCase("false")) {
          sbDetails.append("  <td");
          sbDetails.append("  class=\"");
          if (!strClass.equalsIgnoreCase("")) {
            sbDetails.append(strClass);
          } else {
            if (strType.equalsIgnoreCase("RegularText")) {
              sbDetails.append(strRegularText);
            } else {
              sbDetails.append(strBoldText);
            }
          }
          /*
           * sbDetails.append("\" width=\""); sbDetails.append(strWidth);
           * sbDetails.append("\" height=\""); sbDetails.append(strHeight);
           */
          sbDetails.append("\" align=\"");
          if (!strAlign.equalsIgnoreCase("")) {
            sbDetails.append(strAlign);
          } else {
            sbDetails.append("right");
          }
          sbDetails.append("\">");
        }
        // sbDetails.append(strAlign);

        // sbDetails.append(">");
        if (strType.equalsIgnoreCase("MandatoryText")) {
          sbDetails.append(strMandFont);
        }
        sbDetails.append(strLabel);
        if (!strTD.equalsIgnoreCase("false")) {
          sbDetails.append("</td>");
        }
      } else if (strType.equalsIgnoreCase("CurrText") || strType.equalsIgnoreCase("BoldCurrText")
          || strType.equalsIgnoreCase("CurrTextSign")
          || strType.equalsIgnoreCase("BoldCurrTextSign")) {
        if (!strTD.equalsIgnoreCase("false")) {
          sbDetails.append("  <td ");
          sbDetails.append(strTDHeight);
          sbDetails.append("  class=\"");
          if (!strClass.equalsIgnoreCase("")) {
            sbDetails.append(strClass);
          } else {
            if (strType.equalsIgnoreCase("CurrText") || strType.equalsIgnoreCase("CurrTextSign")) {
              sbDetails.append(strRegularText);
            } else {
              sbDetails.append(strBoldText);
            }
          }
          /*
           * sbDetails.append("\" width=\""); sbDetails.append(strWidth);
           * sbDetails.append("\" height=\""); sbDetails.append(strHeight);
           */
          sbDetails.append("\" align=\"");
          if (!strAlign.equalsIgnoreCase("")) {
            sbDetails.append(strAlign);
          } else {
            sbDetails.append("right");
          }
          sbDetails.append("\">");


        }
        if ((strType.equalsIgnoreCase("CurrTextSign") || strType
            .equalsIgnoreCase("BoldCurrTextSign")) && !strCurrPos.equalsIgnoreCase("right")) {
          sbDetails.append(currSymbol);
          sbDetails.append("&nbsp;");
        }
        sbDetails.append(setCurrValue(strValue));
        if (strCurrPos.equalsIgnoreCase("right")) {
          sbDetails.append("&nbsp;");
          sbDetails.append(currSymbol);
        }
        if (!strTD.equalsIgnoreCase("false")) {
          sbDetails.append("</td>");
        }

      }
      out.write(sbDetails.toString());

    } catch (Exception e) {
      e.printStackTrace();
    }
    return (SKIP_BODY);
  }
}
