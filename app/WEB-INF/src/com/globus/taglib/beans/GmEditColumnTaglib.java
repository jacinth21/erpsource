/*****************************************************************************
 * File			 : GmDropDownTaglib
 * Desc		 	 : This taglib will be used to print Comobo value (Dropdown list value)
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.taglib.beans;

import java.io.IOException;

import javax.servlet.jsp.*;
import javax.servlet.jsp.tagext.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class GmEditColumnTaglib extends BodyTagSupport 
{

    private String controlid 	= "";
    private String title 		= "";
    private String format 		= "";
    private String defaultvalue = "";
    private int maxLength;
    private boolean totaled;
    
    
    /**
     * property holds ControlID information for the selected parts 
     */
    public void setControlID(String Value)
    {
        controlid  = Value; 
    }

    /**
     * the title displayed for this column. if this is omitted then the property name is used for the title of the
     * column (optional).
     */
    public void settitle(String Value)
    {
        title  = Value; 
    }
    
    /**
     * Looks up the parent table tag.
     * @return a table tag instance.
     */
    private GmEditTableTaglib getTableTag()
    {
        return (GmEditTableTaglib) findAncestorWithClass(this, GmEditTableTaglib.class);
    }
    
    /**
     * Currently nothing performed in start tag
     */
    public int doStartTag() throws JspException{
      
        return super.doStartTag();
    }
    
    public int doEndTag() throws JspException
    {
        // Below code to get super tag class information
        GmEditTableTaglib EditTag = getTableTag();
        
        addHeaderToTable(EditTag);
        
        return super.doEndTag();
    }
    
    /**
     * Adds the current header to the table model calling addColumn in the parent table tag. This method should be
     * called only at first iteration.
     * @param tableTag parent table tag
     * @throws DecoratorInstantiationException for error during column decorator instantiation
     * @throws ObjectLookupException for errors in looking up values
     */
    private void addHeaderToTable(GmEditTableTaglib EditTag) 
    {
        HashMap hmap = new HashMap();
        
        hmap.put("ControlID", this.controlid);
        hmap.put("Title", this.title);
        
        EditTag.addColumn(hmap);
    }

}// End of Servlet