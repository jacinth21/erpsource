package com.globus.taglib.beans;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.beanutils.PropertyUtils;

import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCalendarTaglib extends TagSupport {

  private String strProperty = "";
  private String size = "9";
  private String maxlength = "10";
  private String onfocus = "";
  private String styleClass = "";
  private String onblur = "";
  private String tabIndex = "";
  private String strValue = "";
  private String strToValue = "";
  private String strFormName = "";
  StringBuffer sbDetails = new StringBuffer();
  Object objBean;
  private String strControlName = "";
  private String strToControlName = "";
  private String strClass = "";
  private String strOnFocus = "";
  private String strOnBlur = "";
  private String view = "single";
  private String strJsFunction = "";
  private String strApplnDateFmt = "";
  private String strType = "";
  private String strGmClass = "";

  protected HttpSession session;

  String strDivRef = "";

  Date dtDate;

  private String dateFormat = "";

  public void setClass(String Value) {
    strClass = Value;
  }

  public void setGmClass(String strGmClass) {
    this.strClass = strGmClass;
  }

  public void setControlName(String Value) {
    strControlName = Value;
    this.setSFValue(Value);
  }

  public void setToControlName(String Value) {
    strToControlName = Value;
    this.setSFToValue(Value);
  }

  /**
   * @param strProperty the strProperty to set
   */
  public void setProperty(String property) {
    this.strProperty = property;
  }

  /**
   * @param strProperty the strProperty to set
   */
  public void setStrProperty(String strProperty) {
    this.strProperty = strProperty;
  }

  /**
   * @param maxlength the maxlength to set
   */
  public void setMaxlength(String maxlength) {
    this.maxlength = maxlength;
  }


  /**
   * @param onblur the onblur to set
   */

  public void setSFValue(String Value) {
    try {
      this.strValue = (String) PropertyUtils.getProperty(objBean, Value);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
  }

  public void setSFToValue(String Value) {
    try {
      this.strToValue = (String) PropertyUtils.getProperty(objBean, Value);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
  }

  public void setSFFormName(String Value) {
    this.strFormName = Value;
    Object objTemp = getFormInstance();
    setTempObject(objTemp);
    setApplnDateFmt();
  }

  public void setTempObject(Object objTemp) {
    this.objBean = objTemp;
  }

  public Object getFormInstance() {
    return pageContext.getRequest().getAttribute(this.strFormName);
  }

  public void setOnBlur(String value) {
    this.strOnBlur =
        " onBlur=\"javascript:fnDateFmtOnBlur(this,'" + strApplnDateFmt + "');".concat(value);
  }

  public void setOnFocus(String value) {
    this.strOnFocus = " onFocus=\"javascript:".concat(value);
  }

  /**
   * @param tabindex the tabindex to set
   */
  public void setTabIndex(String tabIndex) {
    this.tabIndex = tabIndex;
  }

  public void setDateFormat(String dateFormat) {
    this.dateFormat = dateFormat;
  }

  public Object getReqObj() {
    return pageContext.getRequest();
  }

  public void setApplnDateFmt() {
    session = ((HttpServletRequest) pageContext.getRequest()).getSession(false);
    GmDataStoreVO gmDataStoreVO = null;

    gmDataStoreVO = (GmDataStoreVO) pageContext.getRequest().getAttribute("gmDataStoreVO");

    if (gmDataStoreVO != null) {
      this.strApplnDateFmt = GmCommonClass.parseNull(gmDataStoreVO.getCmpdfmt());
    }
    // If company date format is empty then set session format.
    if (this.strApplnDateFmt.equals("")) {
      this.strApplnDateFmt =
          GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
    }
  }

  // Used only for the JSPs which are not using forms
  public void setTextControlName(String Value) {
    strControlName = Value;
  }

  // Used only for the JSPs which are not using forms
  public void setTextValue(Date dtDate) {

    if (dtDate != null) {
      setApplnDateFmt();
      this.strValue = GmCommonClass.getStringFromDate(dtDate, strApplnDateFmt);
      this.dtDate = dtDate;
    } else {
      this.strValue = "";
      this.dtDate = null;
    }

  }

  // end
  // Code for calendar date change based on format for Struts
  public void setSFDtTextControlName(String Value) {
    strControlName = Value;
    this.setSFDtTextValue(Value);

  }

  public void setSFDtTextValue(String Value) {
    java.util.Date dtValue;

    try {
      dtValue = (java.util.Date) PropertyUtils.getProperty(objBean, Value);
      if (dtValue != null) {
        setApplnDateFmt();
        this.strValue = GmCommonClass.getStringFromDate(dtValue, strApplnDateFmt);
        this.dtDate = new java.sql.Date(dtValue.getTime());
      } else {
        this.strValue = "";
        this.dtDate = null;
      }
    } catch (Exception exp) {
      exp.printStackTrace();
    }
  }

  // end
  public void setType(String strType) {
    this.strType = strType;
  }

  public int doStartTag() {
    try {
      JspWriter out = pageContext.getOut();
      session = ((HttpServletRequest) getReqObj()).getSession(false);
      dateFormat = GmCommonClass.parseNull((String) session.getAttribute("strSessJSDateFmt"));
      sbDetails.setLength(0);
      if (strType.equalsIgnoreCase("label")) {
        sbDetails.append(strValue);
      } else {
        sbDetails.append("<input type=\"text\" name=\"");
        sbDetails.append(strControlName);
        sbDetails.append("\" id=\"");
        sbDetails.append(strControlName);
        sbDetails.append("\" size=\"");
        sbDetails.append(size);
        sbDetails.append("\" value=\"");
        sbDetails.append(strValue);
        sbDetails.append("\" class=\"");
        sbDetails.append(strClass);
        sbDetails.append("\" tabindex=\"");
        sbDetails.append(tabIndex + "\"");
        sbDetails.append(strOnBlur + "\" ");
        sbDetails.append(strOnFocus);
        sbDetails.append("\" class=\"");
        sbDetails.append(strClass);
        sbDetails.append("\"  maxlength=\"");
        sbDetails.append(maxlength);
        sbDetails.append("\">");
        strDivRef = "div" + strControlName;
        strJsFunction =
            "javascript:showSingleCalendar('" + strDivRef
                + "','".concat(strControlName).concat("','").concat(dateFormat).concat("')\"");


        if (strToControlName != null && strToControlName.length() > 0) {
          sbDetails.append("<input type=\"text\" name=\"");
          sbDetails.append(strToControlName);
          // PMT-2885 - When using double calendar passing two id's in showDoubleCalendar() method
          // like startDate and endDate.
          // So need to created id for strToControlName also.
          sbDetails.append("\" id=\"");
          sbDetails.append(strToControlName);
          sbDetails.append("\" size=\"");
          sbDetails.append(size);
          sbDetails.append("\" value=\"");
          sbDetails.append(strToValue);
          sbDetails.append("\" class=\"");
          sbDetails.append(strClass);
          sbDetails.append("\" tabindex=\"");
          sbDetails.append(tabIndex + "\"");
          sbDetails.append(strOnBlur + "\" ");
          sbDetails.append(strOnFocus);
          sbDetails.append("\" class=\"");
          sbDetails.append(strClass);
          sbDetails.append("\"  maxlength=\"");
          sbDetails.append(maxlength);
          sbDetails.append("\">");
          strDivRef = "div" + strToControlName;
          strJsFunction =
              "javascript:showDoubleCalendar('"
                  + strDivRef
                  + "','".concat(strControlName).concat("','").concat(strToControlName)
                      .concat("','").concat(dateFormat).concat("')\"");


        }

        sbDetails.append("&nbsp;<img id=\"Img_Date\" style=\"cursor: hand\" onclick=\"");
        sbDetails.append(strJsFunction);

        sbDetails
            .append(" title=\"Click to open Calendar\" src=\"/images/nav_calendar.gif\" border=0 align=\"absmiddle\" height=18 width=19 />");
        sbDetails.append(" <div id=\"" + strDivRef
            + "\" style=\"position: absolute; z-index: 10;\"></div>");
      }
      out.write(sbDetails.toString());

    } catch (Exception e) {
      e.printStackTrace();
    }
    return (SKIP_BODY);
  }
}
