/**
 * FileName : GmSurgeonSearchReportAction.java Description : Author : sthadeshwar Date : Dec 10,
 * 2008 Copyright : Globus Medical Inc
 */
package com.globus.sim.actions;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.sim.beans.GmSurgeonSearchBean;
import com.globus.sim.forms.GmSurgeonSearchReportForm;

/**
 * @author sthadeshwar
 * 
 */
public class GmSurgeonSearchReportAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws AppError, ServletException, IOException {
    instantiate(request, response);
    GmSurgeonSearchReportForm gmSurgeonSearchReportForm = (GmSurgeonSearchReportForm) form;
    gmSurgeonSearchReportForm.loadSessionParameters(request);

    GmSurgeonSearchBean gmSurgeonSearchBean = new GmSurgeonSearchBean();

    HashMap hmParam = new HashMap();
    String strOpt = gmSurgeonSearchReportForm.getStrOpt();
    log.debug("strOpt = " + strOpt);
    String strFirstName = "";
    String strLastName = "";
    String strPhone = "";
    String strRegions = "";
    String strStates = "";
    List lResult = null;

    if (strOpt.equals("fetch")) {

      strFirstName = gmSurgeonSearchReportForm.getFirstName();
      strLastName = gmSurgeonSearchReportForm.getLastName();
      strPhone = gmSurgeonSearchReportForm.getPhone();

      strRegions =
          GmCommonClass.createInputStringWithType(gmSurgeonSearchReportForm.getSelectedRegions(),
              "REGION");
      strStates =
          GmCommonClass.createInputStringWithType(gmSurgeonSearchReportForm.getSelectedStates(),
              "STATE");

      hmParam.put("FIRSTNAME", strFirstName);
      hmParam.put("LASTNAME", strLastName);
      hmParam.put("PHONE", strPhone);
      hmParam.put("REGIONS", strRegions);
      hmParam.put("STATES", strStates);
      log.debug("hmParam = " + hmParam);

      lResult = gmSurgeonSearchBean.fetchSurgeonReport(hmParam).getRows();
      log.debug("lResult = " + lResult);

      gmSurgeonSearchReportForm.setLresult(lResult);
    }

    gmSurgeonSearchReportForm.setAlRegions(GmCommonClass.getCodeList("REGN"));
    gmSurgeonSearchReportForm.setAlStates(GmCommonClass.getCodeList("STATE", getGmDataStoreVO()));

    return mapping.findForward("success");
  }
}
