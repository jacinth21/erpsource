/**
 * FileName    : GmSurgeonSearchBean.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Dec 10, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.sim.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * @author sthadeshwar
 *
 */
public class GmSurgeonSearchBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * Searches the surgeon based on the provided filter criteria
	 * @param strPartyId
	 * @return RowSetDynaClass
	 */
	public RowSetDynaClass fetchSurgeonReport(HashMap hmParam) throws AppError {
        RowSetDynaClass rsDynaResult = null;
        GmDBManager gmDBManager = new GmDBManager();
        
        String strFirstName = GmCommonClass.parseNull((String)hmParam.get("FIRSTNAME"));
        String strLastName = GmCommonClass.parseNull((String)hmParam.get("LASTNAME"));
        String strPhone = GmCommonClass.parseNull((String)hmParam.get("PHONE"));
        String strRegions = GmCommonClass.parseNull((String)hmParam.get("REGIONS"));
        String strStates = GmCommonClass.parseNull((String)hmParam.get("STATES"));

        log.debug("strFirstName = " + strFirstName);
        log.debug("strLastName = " + strLastName);
        log.debug("strPhone = " + strPhone);
        log.debug("strRegions = " + strRegions);
        log.debug("strStates = " + strStates);
        
        gmDBManager.setPrepareString("gm_pkg_sim_surgeon.gm_fch_surgeon_report", 6);
        gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
        gmDBManager.setString(1, strFirstName);
        gmDBManager.setString(2, strLastName);
        gmDBManager.setString(3, strPhone);
        gmDBManager.setString(4, strRegions);
        gmDBManager.setString(5, strStates);
        gmDBManager.execute();       

        rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet)gmDBManager.getObject(6));
        gmDBManager.close();

        return rsDynaResult;
    }
}
