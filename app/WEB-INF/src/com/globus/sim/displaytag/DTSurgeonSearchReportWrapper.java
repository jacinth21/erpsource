/**
 * FileName    : DTSurgeonSearchReportWrapper.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Dec 11, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.sim.displaytag;

import org.apache.commons.beanutils.DynaBean;

import com.globus.party.displaytag.GmCommonSearchDecorator;

/**
 * @author sthadeshwar
 *
 */
public class DTSurgeonSearchReportWrapper extends GmCommonSearchDecorator {

	public String getNAME()
	{
		DynaBean db = (DynaBean)getCurrentRowObject();
		String strValue = String.valueOf(db.get("NAME"));
		String partyId = String.valueOf(db.get("ID"));
		String partyType = String.valueOf(db.get("PARTYTYPE"));
		return "<a href =/gmPartySummary.do?partyId="+partyId+"&partyType="+partyType+">"+strValue+"</a>";
	}
}
