/**
 * FileName    : GmSurgeonSearchReportForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Dec 10, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.sim.forms;

import java.util.List;
import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

/**
 * @author sthadeshwar
 *
 */
public class GmSurgeonSearchReportForm extends GmCommonForm {

	private ArrayList alStates = new ArrayList();
	private ArrayList alRegions = new ArrayList();

	private String[] selectedRegions = new String[15];
	private String[] selectedStates = new String[15];
	
	private String firstName = "";
	private String lastName = "";
	private String phone = "";
	
	private List lresult;
	/**
	 * @return the alStates
	 */
	public ArrayList getAlStates() {
		return alStates;
	}
	/**
	 * @param alStates the alStates to set
	 */
	public void setAlStates(ArrayList alStates) {
		this.alStates = alStates;
	}
	/**
	 * @return the alRegions
	 */
	public ArrayList getAlRegions() {
		return alRegions;
	}
	/**
	 * @param alRegions the alRegions to set
	 */
	public void setAlRegions(ArrayList alRegions) {
		this.alRegions = alRegions;
	}
	/**
	 * @return the selectedRegions
	 */
	public String[] getSelectedRegions() {
		return selectedRegions;
	}
	/**
	 * @param selectedRegions the selectedRegions to set
	 */
	public void setSelectedRegions(String[] selectedRegions) {
		this.selectedRegions = selectedRegions;
	}
	/**
	 * @return the selectedStates
	 */
	public String[] getSelectedStates() {
		return selectedStates;
	}
	/**
	 * @param selectedStates the selectedStates to set
	 */
	public void setSelectedStates(String[] selectedStates) {
		this.selectedStates = selectedStates;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the lresult
	 */
	public List getLresult() {
		return lresult;
	}
	/**
	 * @param lresult the lresult to set
	 */
	public void setLresult(List lresult) {
		this.lresult = lresult;
	}
	
}
