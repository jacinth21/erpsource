/*
 * Module:       GmSalesReportBean.java
 * Author:       RichardK
 * Project:      Globus Medical App
 * Date-Written: Mar 2005
 * Security:     Unclassified
 * Description:  This beans will be used to generate all kind of sames report 
 *
 * Revision Log (mm/dd/yy initials description)
 * ---------------------------------------------------------
 * mm/dd/yy xxx  What you changed
 *
 */

package com.globus.alerts.beans;


import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;


public class GmAlertsReportBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	
	/**	 
	 * fetchModuleGroups() : Used to fetch the All the records created 	
	 * @return ArrayList - user records
	 */		
	public ArrayList fetchAlertsReport(HashMap hmParam) throws AppError{	

		ArrayList alAlertsReport = new ArrayList();
		String strUserId = GmCommonClass.parseZero((String)hmParam.get("USERID"));
		String alertsType = GmCommonClass.parseZero((String)hmParam.get("ALERTSTYPE"));
		String alertsReason = GmCommonClass.parseZero((String)hmParam.get("ALERTSREASON"));
		String alersActionable = GmCommonClass.parseZero((String)hmParam.get("ALERSACTIONABLE"));
		String fromDate = GmCommonClass.parseZero((String)hmParam.get("FROMDATE"));
		String toDate = GmCommonClass.parseZero((String)hmParam.get("TODATE"));
		GmDBManager gmDBManager = new GmDBManager();		
		gmDBManager.setPrepareString("gm_pkg_alt_alerts.gm_fch_alerts_rpt",7); 
        gmDBManager.registerOutParameter(7,OracleTypes.CURSOR);
        gmDBManager.setString(1,alertsType);
        gmDBManager.setString(2,alertsReason);
        gmDBManager.setString(3,alersActionable);
        gmDBManager.setString(4,fromDate);
        gmDBManager.setString(5,toDate);
        gmDBManager.setString(6,strUserId);
        
        gmDBManager.execute();
        alAlertsReport = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(7));
        gmDBManager.close();
		return alAlertsReport;
	}	
	/**	 
	 * Method : fecthAlertDetails()  : To fetch the details corresponding to an alert
	 * Param -- user id through HashMap
	 * @return HashMap - user records
	 */		
	public HashMap fecthAlertDetails(HashMap hmParam) throws AppError{
		GmDBManager gmDBManager = new GmDBManager();
		HashMap hmReturn  = new HashMap();
		
		String strUserId = GmCommonClass.parseZero((String)hmParam.get("USERID"));
		String strAlertID = GmCommonClass.parseZero((String)hmParam.get("ALERTID"));
		
		gmDBManager.setPrepareString("gm_pkg_alt_alerts.gm_fch_alerts_detail", 3);
		gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
		gmDBManager.setString(1, strAlertID);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		log.debug("hmReturn::"+hmReturn);
		gmDBManager.close();
		return hmReturn;
	}
	/**	 
	 * Method : fecthAlertDetails()  : To fetch alert details with ID.
	 * Param -- user id through HashMap
	 * @return HashMap - user records
	 */		
	public void updateAlerts(HashMap hmParam) throws AppError{
		GmDBManager gmDBManager = new GmDBManager();
		String strUserId = GmCommonClass.parseZero((String)hmParam.get("USERID"));
		String strInputAlertIDs = GmCommonClass.parseZero((String)hmParam.get("STRINPUTALERTIDS"));
		String strReadFlag = GmCommonClass.parseZero((String)hmParam.get("READFLAG"));
		gmDBManager.setPrepareString("gm_pkg_alt_alerts.gm_upd_alerts", 3);
		
		gmDBManager.setString(1, strInputAlertIDs);
		gmDBManager.setString(2, strUserId);
		gmDBManager.setString(3, strReadFlag);
		gmDBManager.execute();
		gmDBManager.commit();
		gmDBManager.close();
	}
	/**	 
	 * Method : fecthAlertDetails()  : To get the unread alert count
	 * Param -- user id through HashMap
	 * @return HashMap - user records
	 */		
	public String getUnReadAlertCount(HashMap hmParam) throws AppError{
		GmDBManager gmDBManager = new GmDBManager();
		String strUserId = GmCommonClass.parseZero((String)hmParam.get("USERID"));
		gmDBManager.setFunctionString("GM_PKG_ALT_ALERTS.GET_UNREAD_ALERTS_CNT", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		String strAlertCount  = GmCommonClass.parseNull(gmDBManager.getString(1));
		gmDBManager.close();
		return strAlertCount;
	}
}