package com.globus.crosstab.export;

import javax.servlet.http.HttpServletResponse;

public interface BufferedResponseWrapper  extends HttpServletResponse {

    /**
     * Headers which cause problems during file download.
     */
    String[] FILTERED_HEADERS = new String[]{"cache-control", "expires", "pragma"};

    /**
     * Return <code>true</code> if ServletOutputStream has been requested from Table tag.
     * @return <code>true</code> if ServletOutputStream has been requested
     */
    boolean isOutRequested();

    /**
     * If the app server sets the content-type of the response, it is sticky and you will not be able to change it.
     * Therefore it is intercepted here.
     * @return the ContentType that was most recently set
     */
    String getContentType();

    /**
     * Returns the String representation of the content written to the response.
     * @return the content of the response
     */
    String getContentAsString();

}
