package com.globus.crosstab.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.globus.common.beans.GmCommonClass;



/**
 * @author tramasamy
 *
 */
public class GmCycleCountCountedRptGridDecorator extends GmRowHighlightDecorator {

  /* (non-Javadoc)
 * @see com.globus.common.beans.GmCrossTabDecorator#get(java.lang.String)
 */
@Override
  public String get(String strColumnName) {
    String strReturnValue = "";
    HashMap hmRow = getCurrentRow();


    if (!strColumnName.equalsIgnoreCase("Description") && !strColumnName.equalsIgnoreCase("Name")
        && !strColumnName.equalsIgnoreCase("ID") && !strColumnName.equalsIgnoreCase("refyear")
        && !strColumnName.equals("refmonthtype") && !strColumnName.equals("Total")) {
      strColumnName += "refvalue";
    }
    strReturnValue = GmCommonClass.parseZero((String) hmRow.get(strColumnName));


    if (strColumnName.equalsIgnoreCase("Description")) {
      Set keySet = hmRow.keySet();
      for (Iterator itr = keySet.iterator(); itr.hasNext();) {
        String strKey = (String) itr.next();
        if (strKey.indexOf("Description") != -1) {
          strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strKey));
          break;
        }
      }
      strColumnName += "Description";
    } else if (strColumnName.equalsIgnoreCase("refmonthtype")) {
      Set keySet = hmRow.keySet();
      for (Iterator itr = keySet.iterator(); itr.hasNext();) {
        String strKey = (String) itr.next();
        if (strKey.indexOf("refmonthtype") != -1) {
          strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strKey));
          break;
        }
      }
      strColumnName += "refmonthtype";
    } else if (strColumnName.equalsIgnoreCase("refyear")) {
      Set keySet = hmRow.keySet();
      for (Iterator itr = keySet.iterator(); itr.hasNext();) {
        String strKey = (String) itr.next();
        if (strKey.indexOf("refyear") != -1) {
          strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strKey));
          break;
        }
      }
      strColumnName += "refyear";
    }



    return strReturnValue;

  }

 
  /* (non-Javadoc)
 * @see com.globus.common.beans.GmCrossTabDecorator#getStyle(java.lang.String)
 */
public String getStyle(String strColumnName) {
    HashMap hmRow = getCurrentRow();
    String strStyle = "";
    String colType = this.getColumnType(strColumnName);
    return strStyle + colType;
  }
  
  /**
 * @param strColumnName
 * @return
 */
public String getColumnType(String strColumnName) {
    HashMap hmRow = getCurrentRow();
    String strPrefix = "\" type=\"";
    String strColumnType = "ro";
    boolean isPastDate = false;

    isPastDate = this.checkDate(strColumnName);
    if (isPastDate) {
      strColumnType = "ro";
    }
    strPrefix += strColumnType + "\"";
    return strPrefix;
  }
 

  /**
 * @param strcolumnDate
 * @return
 */
private boolean checkDate(String strcolumnDate) {
    boolean isPastDate = false;
    try {
      SimpleDateFormat dateFormat = new SimpleDateFormat("MMM' 'yy");
      Calendar cal = Calendar.getInstance();
      Date columnDate = dateFormat.parse(strcolumnDate);
      Date sysdate = dateFormat.parse((dateFormat.format(cal.getTime())));
      isPastDate = sysdate.after(columnDate);

    } catch (Exception e) {
      isPastDate = true;
    }
    return isPastDate;

  }
}
