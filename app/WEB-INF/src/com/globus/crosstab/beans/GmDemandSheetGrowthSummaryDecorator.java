package com.globus.crosstab.beans;

import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabDecorator;

public class GmDemandSheetGrowthSummaryDecorator extends GmRowHighlightDecorator {

	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		String strLinkType="";

		if (strColumnName.equals("Name")) {
			String strID = (String) hmRow.get("ID");
			String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));

			sbReturn.setLength(0);

			
			//GTYPE
			
			String strGType = GmCommonClass.parseNull((String) hmRow.get("GTYPE"));
			//!System.out.println("Gtype:"+strGType);
			
			
			// Add Icon 'C' - "Cons"
			sbReturn.append("<img id='imgEditP' style='cursor:hand' ");
			sbReturn.append("src='/images/consignment.gif' ");
			// strValue.append("title='View Consignment Detials' ");
			// strValue.append(" width='12' height='13' ");
			sbReturn.append("onClick=\"javascript:fnCallConsD('");
			sbReturn.append(strID);
			sbReturn.append("' , '" + strLinkType + "' )\"/> ");
			
			
			
			
			// D icon - for set, group, part
			
			//S - set, "AD"
			if( strGType.equals("S")) {				
				sbReturn.append("<img id='imgEdit' style='cursor:hand' ");
				sbReturn.append("src='/images/d2.gif' ");
				sbReturn.append("title='View Field Sales Consignment detail '");
				// strValue.append(" width='14' height='14' ");
				sbReturn.append("onClick=\"javascript:fnCallAD('");
				sbReturn.append(strID);
				sbReturn.append("' , '" + strLinkType + "' , '" + strValue + "' )\"/> ");				
			}
			
			//P - Part, "Disp" code
			if( strGType.equals("P")) {
				
				sbReturn.append("<img id='imgEditP' style='cursor:hand' ");
				sbReturn.append("src='/images/d2.gif' ");
				sbReturn.append("title='View Field Sales Part Detials' ");
				// strValue.append(" width='12' height='13' ");
				sbReturn.append("onClick=\"javascript:fnCallDisp('");
				sbReturn.append(strID);
				sbReturn.append("' , '" + strLinkType + "' )\"/> ");
			
			}
			
			
			//Add Icon for G  for Group 			
			
			if( strGType.equals("G")) {
				sbReturn.append("<img id='imgEditG' style='cursor:hand' ");
				sbReturn.append("src='/images/d2.gif' ");
				sbReturn.append("title='View Group Information' ");
				// strValue.append(" width='12' height='13' ");
				//sbReturn.append("onClick=\"javascript:fnCallSetNumber('");
				sbReturn.append("onClick=\"javascript:fnCallGroupMap('");
				sbReturn.append(strID);
				sbReturn.append("' , '" + strLinkType + "' )\"/> ");
				
			}
			
			
			
			
			
			
			
			sbReturn.append(strValue);
			strReturnValue = sbReturn.toString();

		} else {
			strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		}
		return strReturnValue;
	}

	/*
	 * To Override the TD Style of the column Name
	 */

	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		if (strColumnName.equals("Name")) {
			String strID = GmCommonClass.parseNull((String) hmRow.get("CROSSOVERF"));
			if (strID.equals("Y")) {
				strStyle = "OrangeShadeTD";
			}
		}
		if (strStyle.equals("")) {
			strStyle = super.getStyle(strColumnName);
		}
		return strStyle;
	}
}
