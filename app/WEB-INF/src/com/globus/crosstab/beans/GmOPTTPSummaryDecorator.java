package com.globus.crosstab.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


public class GmOPTTPSummaryDecorator extends GmRowHighlightDecorator{
	
	/*
	 * To Override the TD Style of the column Name
	 */
Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
int i = 0;
 public String get(String strColumnName) {
	 
	HashMap hmRow = getCurrentRow();
	String strColumnValue = "";
	StringBuffer sbReturn = new StringBuffer();
	
	if (strColumnName.equals("TPR") && getGroupingCount().equals("0")) 
	{
		//log.debug("HM ROW:"+hmRow);
		String strPnum = (String)hmRow.get("ID");
		String strData = strPnum+"^"+"4000106";
		strColumnValue ="<a href=javascript:fnCallTPRReport('"+strData+"')>"+(String)hmRow.get("TPR")+"</a>";
	}
	else if (strColumnName.equals("TPR US") && getGroupingCount().equals("0")) 
	{
		//log.debug("HM ROW:"+hmRow);
		String strPnum = (String)hmRow.get("ID");
		String strData = strPnum+"^"+"4000107";
		strColumnValue ="<a href=javascript:fnCallTPRReport('"+strData+"')>"+(String)hmRow.get("TPR US")+"</a>";
	}
	else if (strColumnName.equals("TPR OUS") && getGroupingCount().equals("0")) 
	{
		//log.debug("HM ROW:"+hmRow);
		String strPnum = (String)hmRow.get("ID");
		String strData = strPnum+"^"+"4000108";
		strColumnValue ="<a href=javascript:fnCallTPRReport('"+strData+"')>"+(String)hmRow.get("TPR OUS")+"</a>";
	}
	else if (strColumnName.equals("Parent Part Qty")) //&& getGroupingCount().equals("0")) 
	{
		//log.debug("HM ROW:"+hmRow);
		String strPnum = (String)hmRow.get("ID");
		String strParentPartQty = GmCommonClass.parseNull((String) hmRow.get("Parent Part Qty"));
			if (strParentPartQty.equals("")) {
				strColumnValue = "-";
			} else {
				strColumnValue = "<a href=javascript:fnCallPPQtyDetails('" + strPnum + "','" + strParentPartQty + "')>" + (String) hmRow.get("Parent Part Qty")
						+ "</a>";
			}
	}
	else if (strColumnName.equals("Name"))
	{
		String strPnum = (String)hmRow.get("ID");
		String strTTPType = (String) getAttribute("TTPTYPE");		
		String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		String strOrdQty = (String)hmRow.get("Order Qty");
		String strLevelID = (String) getAttribute("LEVELID");
		String strHierarchy = (String) getAttribute("HLVLHIERARCHY");
		String strAccessID = (String) getAttribute("ACCESSID");
		String strForecastMonth = (String) getAttribute("FORECASTMONTH");
		  if(getGroupingCount().equals("0")){
			String strData=strPnum+"^"+strLevelID+"^"+strHierarchy+"^"+strAccessID+"^"+strForecastMonth;
			sbReturn.append("<img id='imgEdit' style='cursor:hand' ");
			sbReturn.append("src='/images/a.gif' ");
			sbReturn.append("title='View Actual Values'");
			if(strTTPType.equals("50310"))
			{
				sbReturn.append("onClick=\"javascript:fnCallMultiSheetTTP('");
			}	
			else
			{
				sbReturn.append("onClick=\"javascript:fnCallActual('");
			}
			sbReturn.append(strData);
			sbReturn.append("' )\"/> ");		
			
			sbReturn.append("<img id='imgEditP' style='cursor:hand' ");
			sbReturn.append("src='/images/product_icon.jpg' ");
			sbReturn.append("title='View Part Details' ");
			// strValue.append(" width='12' height='13' ");
			sbReturn.append("onClick=\"javascript:fnCallPart('");
			sbReturn.append(strPnum);
			sbReturn.append("', '");
			sbReturn.append("0");
			sbReturn.append("' , '" + strPnum + "','"+strLevelID+"','"+strHierarchy+"' )\"/> ");
			sbReturn.append( strPnum );
			
			strColumnValue = sbReturn.toString();
		  }else{
			  strColumnValue = (String)hmRow.get(strColumnName);
		  }
		
		strColumnValue= strColumnValue + "<input type=\"hidden\" value=\""+strPnum+"\" name=\"partnumber\" id="+i+"/>";
		strColumnValue= strColumnValue + "<input type=\"hidden\" value=\""+strOrdQty+"\" name=\"ordqty\" id="+i+"/>";
		i++;

		
	}
	else 
	{
		strColumnValue = (String)hmRow.get(strColumnName);
	}
	
	return strColumnValue;
}

	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		if (strColumnName.equals("Name")) {
			String strID = GmCommonClass.parseNull((String) hmRow.get("CROSSOVERFL"));
			//log.debug(" Name is " + strColumnName + " Value is " + strID);
			if (strID.equals("Y")) {
				strStyle = "YellowShadeTD";
			}
		}
		if (strStyle.equals("")) {
			strStyle = super.getStyle(strColumnName);
		}
		return strStyle;
	}

}
