package com.globus.crosstab.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmPatientTrackDecorator extends GmRowHighlightDecorator{
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	Object dt = null;
	String strStudyPeriodId ="";
	public String get(String strColumnName) {
		String strReturnValue = "";
		String cboPeriodID="";
		String cboFormID="";
		String strStPerId ="";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		dt = (Object)hmRow.get("DUEDATE");
		strStudyPeriodId = (String)hmRow.get("STUDYPERIODID");
		HashMap hmAdditionalVal = getAdditionalValMap();
		    	if (hmRow.get(strColumnName) == null) {
				    sbReturn.setLength(0);
					sbReturn.append("&nbsp;xxx");
					strReturnValue = sbReturn.toString();
				  }	else if (!hmRow.get(strColumnName).equals("")) {
					  ArrayList alRule = GmCommonClass.parseNullArrayList((ArrayList) hmAdditionalVal.get(strColumnName));
					   if(alRule.size()> 0){
						   //log.debug("alRule==>"+alRule);
						   // Getting formid, perid from rules for multi-event forms and removed hard code values. 
						   	  cboPeriodID="6309";
							  cboFormID =(String)alRule.get(0);
							  strStPerId =(String)alRule.get(1); 
						  if(!strStPerId.equals("Y")){
							  sbReturn.append("<![CDATA[<a href=\"javascript:fnViewfrmAdverseEvent(\\'"+hmRow.get("Name")+"\\'\\,\\'"+cboFormID+"\\'\\,\\'"+cboPeriodID+"\\'\\,\\'"+strStPerId+"\\');\">"
						  			   +(String)hmRow.get(strColumnName)+"</a>]]>");
							  strReturnValue = sbReturn.toString();	
						  }else{
							  if(hmRow.get(strColumnName).equals("1"))
							  {
								  strReturnValue = "<![CDATA[<font color=\\'green\\'>*</font>]]>";
							  }
						  }
					   }else{
						  strReturnValue = (String) hmRow.get(strColumnName);  
				  	  }
				  }else if(strColumnName.equalsIgnoreCase("Comments")){
				  if(hmRow.get(strColumnName)!=null){
					  strReturnValue = "<![CDATA[<a href=\"javascript:fnCallCommentSummaryReport("+strStudyPeriodId+");\">"
	  					+(String)hmRow.get(strColumnName)+"</a>]]>";	
				  }else{
					  strReturnValue = " ";
				  }
			  }else{
				  strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
				  // If the value is not present in the HashMap, setting it to a space to avoid display of - in the cross tab.
				  if(strReturnValue.equals("")){
					  strReturnValue=" ";
				  }
			  }
		return strReturnValue;
	}	
	
	/*
	 * To Override the TD Style of the column Name
	 */

	public String getStyle(String strColumnName) {
		
		String strStyle = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		  dt = (Object)hmRow.get("DUEDATE");
			   if(strColumnName.endsWith("Films")){
				  if(hmRow.get(strColumnName) != null && hmRow.get(strColumnName).equals("*")){
					  strStyle = "greenShadeTD" + "\"";
				  }else{
					  strStyle = super.getStyle(strColumnName) + "\"";
					  //log.debug("strReturnValue " + strReturnValue);
				  }
			  }else if(hmRow.get(strColumnName) == null || hmRow.get(strColumnName).equals("")){
				  strStyle = super.getStyle(strColumnName) + "\"";
			  }else if(hmRow.get(strColumnName).equals("*")){
				  strStyle = "greenShadeTD" + "\"";
			  }else{
				  strStyle = super.getStyle(strColumnName) + "\"";
				  //log.debug("strReturnValue " + strReturnValue);
			  }
		return strStyle;
	}
}
