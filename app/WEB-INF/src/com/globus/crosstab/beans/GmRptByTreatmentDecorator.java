package com.globus.crosstab.beans;

import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;

public class GmRptByTreatmentDecorator extends GmRowHighlightDecorator{
	Object dt = null;
	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		dt = (Object)hmRow.get("DUEDATE");
		   
				  strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
				// If the value is not present in the HashMap, setting it to a space to avoid display of - in the cross tab.
				  if(strReturnValue.equals("")){
					  strReturnValue="-";
				  }
				
			 
	
		return strReturnValue;
	}	
	
public String getStyle(String strColumnName) {
		
		String strStyle = "";
		HashMap hmRow = getCurrentRow();
		
		String strName = (String)hmRow.get("Name");
		
		StringBuffer sbReturn = new StringBuffer();
		  dt = (Object)hmRow.get("DUEDATE");
		  
		  
		  if(strName.equalsIgnoreCase("ALL PATIENTS"))
	  		  strStyle = "ShadeLevel3" + "\"  style=\"text-align: left; font-size : 9px; \" ";
		  else if(strName.equalsIgnoreCase("Out-of-Window Visits"))
		  		  strStyle = "ShadeLevel4" + "\"  style=\"text-align: left;  font-size : 9px;\" ";
		  else if(strName.equalsIgnoreCase("Missing Follow-Up"))
			  strStyle = "ShadeLevel5" + "\"  style=\"text-align: left; font-size : 9px; \" ";
		  else if(strName.equalsIgnoreCase("Randomized ACDF") || strName.equalsIgnoreCase("ALL SECURE-C") 
				  || strName.equalsIgnoreCase("FLEXUS") || strName.equalsIgnoreCase("XSTOP")||
				  strName.equalsIgnoreCase("ALL SECURE-C"))
			  strStyle = "ShadeLevel2" + "\"  style=\"text-align: left; font-size : 9px;\" ";
		  else if(strName.equalsIgnoreCase("First 5 SECURE-C") || strName.equalsIgnoreCase("Randomized SECURE-C"))
				  strStyle = "ShadeLevel3NoBold" + "\"  style=\"text-align: left;font-weight :bold;font-size : 9px;\" ";
		  else
			  strStyle = " " + "\"  style=\"text-align: left; font-size : 9px;\" ";
		  
		//  if(strName.equalsIgnoreCase("Randomized ACDF") || strName.equalsIgnoreCase("Randomized ACDF") || strName.equalsIgnoreCase("ALL SECURE-C")) 
	  		  
			  	 
		return strStyle;
	}
}
