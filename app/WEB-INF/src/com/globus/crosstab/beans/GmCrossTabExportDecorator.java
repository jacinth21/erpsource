/**
 * FileName    : GmCrossTabExportDecorator.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : May 7, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.crosstab.beans;

import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabDecorator;

/**
 * @author sthadeshwar
 *
 */
public class GmCrossTabExportDecorator extends GmCrossTabDecorator {
	
	public String get(String strColumnName) 
	{
		HashMap hmRow = getCurrentRow();
		String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		//strValue = "<font style='FONT-FAMILY: Verdana;FONT-SIZE: 11px;'>" + strValue + "</font>";
		return strValue;
	}

}
