package com.globus.crosstab.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabFormat;
import com.globus.common.beans.GmLogger;

public class GmSetOverviewDecorator extends GmRowHighlightDecorator{

	
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	String strFirstDay = "";
	Calendar c=Calendar.getInstance();	
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy");
	
	
	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		String strID = (String) hmRow.get("ID");
		String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		  
			
		if (strColumnName.equals("Inhouse Loaner") ) {
			
			  
		 
			if (strValue.equals("")) {
				strReturnValue = "-";
			} else {
				strReturnValue = "<a href= javascript:fnHouseLoaner('" + strID + "')>" + strValue + "</a>";
			}
		} 
		
		 
		
		else if (strColumnName.equals("Product Loaner") ) {
		 	  
			 
			if (strValue.equals("")) {
				strReturnValue = "-";
			} else {
				strReturnValue = "<a href= javascript:fnProductLoaner('" + strID + "')>" + strValue + "</a>";
			}
		}	 
		else if (strColumnName.equals("Avg Net") ) {
			 
			strValue = GmCommonClass.getStringWithCommas(strValue);
			 
			if (strValue.equals("")||strValue.equals("0.00")) {
				strReturnValue = "-";
			} else {
				strReturnValue = "<a href= javascript:fnAvgNet('" + strID + "')>" + strValue + "</a>";
			}
		} 
		
		else if (strColumnName.equals("Initiated")|| strColumnName.equals("WIP")|| strColumnName.equals("Pending Verification")||strColumnName.equals("Ready To Consign")
				||strColumnName.equals("Pending Shipping")||strColumnName.equals("Shipped")||strColumnName.equals("Yet To Build") ) {
			
			String strConstatus ="";
			String strReqstatus ="0";
			
			if(strColumnName.equals("Initiated"))	   strConstatus = "Initiated";
			if(strColumnName.equals("WIP"))	   strConstatus = "WIP";
			if(strColumnName.equals("Pending Verification"))	   strConstatus = "PendVer";
			if(strColumnName.equals("Yet To Build"))	  strReqstatus = "50644";
			if(strColumnName.equals("Ready To Consign"))	   strReqstatus = "50646";
			if(strColumnName.equals("Pending Shipping"))	   strReqstatus = "50647";
			if(strColumnName.equals("Shipped"))	   strConstatus = "Shipped";
			
			if (strValue.equals("")) {
				strReturnValue = "-";
			} else {
				if (!strColumnName.equals("Shipped"))
				strReturnValue = "<a href= javascript:fnSetStatus('" + strID + "','"  + strReqstatus +  "','"  + strConstatus  + "')>" + strValue + "</a>";
				else 
					strReturnValue = "<a href= javascript:fnSetStatusShipped('" + strID + "','"  + strReqstatus +  "','"  + strConstatus  + "')>" + strValue + "</a>";
			}
			
		} 
		
		else if (strColumnName.equals("Name")){
			strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		}
		else
		{
			if (strValue.equals("")||strValue.equals("0")) {
				strReturnValue = "-";
			} else {
				strReturnValue = "<a>" + strValue + "</a>";				
				if(strColumnName.matches(".*\\b-\\b.*"))
				{
					try
					{
						c.set(c.get(c.YEAR),c.get(c.MONTH),1);
						strFirstDay = (formatter.format(c.getTime()));
						Date dateHeader = (Date) formatter.parse("01-"+strColumnName);
						Date dateFirstDay = (Date) formatter.parse(strFirstDay);
						//log.debug("dateHeader...."+dateHeader+".....dateFirstDay....."+dateFirstDay+"....."+dateHeader.compareTo(dateFirstDay));
						if(dateHeader.compareTo(dateFirstDay) >= 0)
						{					
							strReturnValue = "<a href= javascript:fnForecastMon('" + strID + "','"  + strColumnName   + "')>" + strValue + "</a>";						
						}	
						
					}catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		}
		return strReturnValue;
	}
}
