package com.globus.crosstab.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmTrendDecorator extends GmRowHighlightDecorator {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 


	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		String strLinkType="";
		String strID = (String) hmRow.get("ID");
		String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		
		
		  if (strColumnName.equals("Description")) {
			 
			sbReturn.setLength(0);
 	
			//Add Icon 'P' 			
			sbReturn.append("<img id='imgEditP' style='cursor:hand' ");
			sbReturn.append("src='/images/product_icon.jpg' ");
			sbReturn.append("title='View Part Inventory Detials' ");
			sbReturn.append("onClick=\"javascript:fnCallInv('");
			sbReturn.append(strID);
			sbReturn.append( "' )\"/> ");			
			sbReturn.append(strValue);
			strReturnValue = sbReturn.toString();

		} 
		  
		  else if (strColumnName.equals("On PO") ) {
			 	 
				if (sbReturn.equals("")) {
					strReturnValue = "-";
				} else {
					strReturnValue = "<a href= javascript:fnCallPO('" + strID + "')>" + strValue + "</a>";
				}
			} 
			
		  else {
			strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		}
		return strReturnValue;
	}
	/*
	 * To Override the TD Style of the column Name
	 */

	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		double dTrendQty = 0;
		
		if (strColumnName.indexOf("-") != -1) {
			String strValue = GmCommonClass.parseZero((String) hmRow.get(strColumnName));			
			dTrendQty = Double.parseDouble(strValue);
			if (dTrendQty < 0)
			{
				strStyle = "ShadeMedRedTD";
			}			
		}
		if (strStyle.equals("")) {
			strStyle = super.getStyle(strColumnName);
		}
		return strStyle;
	}
}
