/**
 * 
 */
package com.globus.crosstab.beans;

import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;

/**
 * @author rshah
 *
 */
public class GmCrtlEndPointRptDecorator extends GmRowHighlightDecorator {
	
	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		// If the value is not present in the HashMap, setting it to a space to avoid display of - in the cross tab.
		if(strReturnValue.equals(""))
		{
			  strReturnValue="-";
		}
		return strReturnValue;
	}	
	
/*public String getStyle(String strColumnName) 
	{
		String strStyle = " " + "\"  style=\"text-align: left; font-size : 9px;\" ";
		return strStyle;
	}*/
// grouping count hard coded to show .ShadeLevel2 style
public String getGroupingCount()
	{
		return "2";
	}
}
