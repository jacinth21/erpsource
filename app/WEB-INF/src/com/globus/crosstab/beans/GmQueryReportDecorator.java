package com.globus.crosstab.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.clinical.querysystem.forms.GmQueryReportForm;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCrossTabFormat;
import com.globus.common.beans.GmLogger;

public class GmQueryReportDecorator extends GmRowHighlightDecorator{

	
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	GmCrossTabFormat gmCrossTab = new GmCrossTabFormat();
	GmQueryReportForm gmQueryReportForm = new GmQueryReportForm();
	
	
	public String get(String strColumnName) {
		String strReturnValue = "";
		
		HashMap hmRow = getCurrentRow();
		String strID = (String) hmRow.get("ID");
		String strName = GmCommonClass.parseNull((String) hmRow.get("Name"));
		strName = strName.equals("") ? strName : strName.replaceAll(" ", "");
		String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		if (strColumnName.equals("Name") ) {
			if (strValue.equals("")) {
				strReturnValue = "-";
			} else {				
				strReturnValue = "<![CDATA[<a href=javascript:fnStudy(&#39"+"#"+ strID +"#"+"&#39,&#39"+"#"+ strName +"#"+"&#39)>"+strValue+"</a>]]>";
				//strReturnValue =  "<![CDATA[ Yanka Kupala^http://www.belarus-misc.org/writer/ykupala.htm^iframe_a]]>"; 

			}			
		}
		else{
			if (strValue.equals("")) {
				strReturnValue = "-";
			}
			else
				strReturnValue = strValue;
		}		
		return strReturnValue; 
	}
	
	/*
	 * To Override the TD Style of the column Name
	 */

	public String getStyle(String strColumnName) {
		String strSort ="";
		
		if ( strColumnName.equals("Name"))
				{
				strSort = " sort=\"int\"";
				}
		else
		{
			strSort = " sort=\"int\"";
		}
				
		
		String strStyle = "";
		String strPrefix = "\" type=\"";
		String strColumnType = "ro\"";			
		String colType = strPrefix+strColumnType;
		strStyle = strStyle+colType+strSort;
		
		return strStyle;
	}
}