package com.globus.crosstab.beans;



import java.util.HashMap;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmPartQtyDecorator extends GmRowHighlightDecorator {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	
	/*
	 * To Override the TD Style of the column Name
	 */
	public String getTRopen() {		
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		String strID = GmCommonClass.parseNull((String) hmRow.get("Name"));			
		String strNumID = strID.substring(0,2);
		if (!NumberUtils.isNumber(strNumID)) {
			  if(!strID.equals("Total"))
				  strStyle = "<tr  height =" + getRowHeight() + "  class=ShadeLevel1  onmouseover=fnstripedbyid('"+getTrId()+"','lightBrown'); >";
			  else
				  strStyle = "<tr  height =" + getRowHeight() + "  class=ShadeLevel3  onmouseover=fnstripedbyid('"+getTrId()+"','lightBrown'); >";
		}
		else 
		{
			strStyle = super.getTRopen();
		}
		return strStyle;
	}
	
	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		String strID = GmCommonClass.parseNull((String) hmRow.get("Name"));	
		strID = strID.substring(0,2);

		if (!NumberUtils.isNumber(strID)) {
			   if(!strColumnName.equals("Name"))
			   {
				   strStyle= "RightTextSmallCT";
			   }
			}		
		else {
			strStyle = super.getStyle(strColumnName);
		  }
		return strStyle;
	}
}	
