package com.globus.crosstab.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;


public class GmTTPSummaryDecorator extends GmRowHighlightDecorator{
	
	/*
	 * To Override the TD Style of the column Name
	 */
Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
int i = 0;
 public String get(String strColumnName) {
	 
	HashMap hmRow = getCurrentRow();
	String strColumnValue = "";
	StringBuffer sbReturn = new StringBuffer();
	if (strColumnName.equals("TPR") && getGroupingCount().equals("0")) 
	{
		//log.debug("HM ROW:"+hmRow);
		String strPnum = (String)hmRow.get("ID");
		strColumnValue ="<a href=javascript:fnCallTPRReport('"+strPnum+"')>"+(String)hmRow.get("TPR")+"</a>";
	}
	else if (strColumnName.equals("Parent Part Qty")) //&& getGroupingCount().equals("0")) 
	{
		//log.debug("HM ROW:"+hmRow);
		String strPnum = (String)hmRow.get("ID");
		String strParentPartQty = GmCommonClass.parseNull((String) hmRow.get("Parent Part Qty"));
			if (strParentPartQty.equals("")) {
				strColumnValue = "-";
			} else {
				strColumnValue = "<a href=javascript:fnCallPPQtyDetails('" + strPnum + "','" + strParentPartQty + "')>" + (String) hmRow.get("Parent Part Qty")
						+ "</a>";
			}
	}
	else if (strColumnName.equals("Name"))
	{
		String strPnum = (String)hmRow.get("ID");		
		String strTTPType = (String) getAttribute("TTPTYPE");		
		String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		String strOrdQty = (String)hmRow.get("Order Qty");
		if(strTTPType.equals("50310"))
		{
			sbReturn.append("<img id='imgEdit' style='cursor:hand' ");
			sbReturn.append("src='/images/a.gif' ");
			sbReturn.append("title='View Actual Values'");
		
			sbReturn.append("onClick=\"javascript:fnCallMultiSheetTTP('");
			sbReturn.append(strPnum);
			sbReturn.append("' )\"/> ");		
			
			sbReturn.append("<img id='imgEditP' style='cursor:hand' ");
			sbReturn.append("src='/images/product_icon.jpg' ");
			sbReturn.append("title='View Part Details' ");
			// strValue.append(" width='12' height='13' ");
			sbReturn.append("onClick=\"javascript:fnCallPart('");
			sbReturn.append(strPnum);
			sbReturn.append("', '");
			sbReturn.append("0");
			sbReturn.append("' , '" + strPnum + "' )\"/> ");
			sbReturn.append( strPnum );
			
			strColumnValue = sbReturn.toString();
		}	
		else
		{
			strColumnValue = strValue;
		}
		strColumnValue= strColumnValue + "<input type=\"hidden\" value=\""+strPnum+"\" name=\"partnumber\" id="+i+"/>";
		strColumnValue= strColumnValue + "<input type=\"hidden\" value=\""+strOrdQty+"\" name=\"ordqty\" id="+i+"/>";
		i++;

		
	}
	else 
	{
		strColumnValue = (String)hmRow.get(strColumnName);
	}
	
	return strColumnValue;
}

	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		if (strColumnName.equals("Name")) {
			String strID = GmCommonClass.parseNull((String) hmRow.get("CROSSOVERFL"));
			//log.debug(" Name is " + strColumnName + " Value is " + strID);
			if (strID.equals("Y")) {
				strStyle = "YellowShadeTD";
			}
		}
		else if (strColumnName.equals("Description")) {
			strStyle = "Left";
	    }
		if (strStyle.equals("")) {
			strStyle = super.getStyle(strColumnName);
		}
		return strStyle;
	}

}
