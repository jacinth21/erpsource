/**
 * FileName    : GmIncidentDecorator.java 
 * Description :
 * Author      : rshah
 * Date        : Apr 7, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.crosstab.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author rshah
 *
 */
public class GmIncidentDecorator extends GmRowHighlightDecorator {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 


	public String get(String strColumnName) {
		HashMap hmRow = getCurrentRow();

		String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		String strReturnValue = strValue;
		
		/*Dont Delete will be used for next requirement.
		 * 
		String strId = GmCommonClass.parseNull((String) hmRow.get("ID"));
		if (!strColumnName.equals("Name") && !strColumnName.equals("Total")&& !strColumnName.equals("ID")) {
			if(!strValue.equals(""))
			{
				strReturnValue = "<a href=\"#\">"+strValue+"</a>";  
			}
			
		}
		*/
		return strReturnValue;
	}
	
	/*
	 * To Override the TD Style of the column Name
	 */

	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		if (strColumnName.equals("Name")) 
		{
			strStyle = "ShadeMedGrayTD";
		}
		if (strStyle.equals("")) {
			strStyle = super.getStyle(strColumnName);
		}
		return strStyle;
	}
	

}
