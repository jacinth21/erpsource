package com.globus.crosstab.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmDemandSummaryDecorator extends GmRowHighlightDecorator {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 


	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		String strLinkType="";
		String strDemandMasterID = (String) getAttribute("DMID");
		String strDemandSheetID = (String) getAttribute("DSID");
		String strSheetstatus = (String) getAttribute("SHEETSTATUS");
		
		if (strColumnName.equals("PAR") && strSheetstatus.equals("50550")) {
			String strID = (String) hmRow.get("ID");			
			String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
			if (strValue.equals("")) {
				strReturnValue = "-";
			} else if(getGroupingCount().equals("0")){
				strReturnValue = "<a href= javascript:fnCallEditPAR('" + strID + "','"  + strDemandSheetID + "','"+ strDemandMasterID + "')>" + strValue + "</a>";
			}
			else {
				strReturnValue =   strValue ;
			}
			 
		} 
		else if(strColumnName.equals("PBO")|| strColumnName.equals("PBL")||strColumnName.equals("PCS")){
			
			String strStatus = "";
			String strPnum = (String)hmRow.get("ID");			
			if(strColumnName.equals("PBO")) strStatus = "10";
			if(strColumnName.equals("PBL")) strStatus = "15";
			if(strColumnName.equals("PCS")) strStatus = "20";
			String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
			if (strValue.equals("")||strValue.equals("0")) {
				strReturnValue = "-";
			} else if(getGroupingCount().equals("0")){
				strReturnValue = "<a href= javascript:fnCallPPP('" + strPnum + "','"  + strStatus + "','"+ strDemandSheetID + "','"+ strDemandMasterID +"')>" + strValue + "</a>";
			}
			else {
				strReturnValue =   strValue ;
			}
			 
		}
		else if (strColumnName.equals("Name")) {
			String strID = (String) hmRow.get("ID");			
			String strDemandType = (String) getAttribute("DEMANDTYPE");			
			String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
			//log.debug(" Values in row " + hmRow);
			String strRowOverRideValue = GmCommonClass.parseNull((String) hmRow.get("ROWOVERRIDEFL"));

			sbReturn.setLength(0);

			if (strRowOverRideValue.equals("Y")) {
				sbReturn.append("<img id='imgEdit' style='cursor:hand' ");
				sbReturn.append("src='/images/icon_History.gif' ");
				sbReturn.append(" width='12' height='13' ");
				sbReturn.append("title='View Comments'");
				sbReturn.append("onClick=\"javascript:fnViewComments('");
				sbReturn.append(strID);
				sbReturn.append("' , '" + strLinkType + "' , '" + strDemandMasterID + "', 'Edit')\"/> ");
			}
			else{
                sbReturn.append("<img id='imgEditP' style='cursor:hand' ");
                sbReturn.append("src='/images/consignment.gif' ");
                sbReturn.append("title='View Comments'");
                sbReturn.append("onClick=\"javascript:fnViewComments('");
                sbReturn.append(strID);
                sbReturn.append("' , '" + strLinkType + "' , '" + strDemandMasterID + "', 'Edit')\"/> ");
			}
			
			// Add Icon 'A' 			
			sbReturn.append("<img id='imgEdit' style='cursor:hand' ");
			sbReturn.append("src='/images/a.gif' ");
			sbReturn.append("title='View Actual Values'");
			if (!strDemandType.equals("40023"))
			{
			sbReturn.append("onClick=\"javascript:fnCallActuals('");
			sbReturn.append(strID);
			sbReturn.append("' , '" + strLinkType + "' )\"/> ");
			}
			else
			{
				sbReturn.append("onClick=\"javascript:fnCallMultiSheet('");
				sbReturn.append(strID);
				sbReturn.append("' , '" + strDemandSheetID + "' )\"/> ");	
			}

			// Add Icon 'P' 			
			sbReturn.append("<img id='imgEditP' style='cursor:hand' ");
			sbReturn.append("src='/images/product_icon.jpg'");
			sbReturn.append("title='View Part Detials' ");
			sbReturn.append("onClick=\"javascript:fnCallDisp('");
			sbReturn.append(strID);
			sbReturn.append("' , '" + strLinkType + "' )\"/> ");			
			
			
			String strPSFlag = (String) hmRow.get("PS_FLAG");
			if (strPSFlag != null)
			{
			sbReturn.append("<img id='imgEditP'  width='12' height='12'  style='cursor:hand' ");
			sbReturn.append("src='/images/ParentChild.png'");
			sbReturn.append("title='Sub Part Details' ");
			sbReturn.append("onClick=\"javascript:fnSubComponentDrillDown('");
			sbReturn.append(strID);
			sbReturn.append("' )\"/> ");						
			}
			
			sbReturn.append(strValue);
			strReturnValue = sbReturn.toString();
			// 

		} else {
			strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		}
		return strReturnValue;
	}

	/*
	 * To Override the TD Style of the column Name
	 */

	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		String strDemandType = (String) getAttribute("DEMANDTYPE");			
		if (strColumnName.equals("Name")) {
			String strID = GmCommonClass.parseNull((String) hmRow.get("CROSSOVERF"));
			if (strID.equals("Y")) {
				strStyle = "YellowShadeTD";
			}
		}
		if (strStyle.equals("")) {
			strStyle = super.getStyle(strColumnName);
		}
		return strStyle;
	}
}
