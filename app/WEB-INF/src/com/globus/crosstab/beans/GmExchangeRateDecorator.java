package com.globus.crosstab.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.globus.common.beans.GmCommonClass;

public class GmExchangeRateDecorator extends GmRowHighlightDecorator {

	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();		

		
		if(strColumnName.equalsIgnoreCase("Name")){

		  strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		}
		else{
		  strReturnValue = GmCommonClass.parseZero((String) hmRow.get(strColumnName));
		  strReturnValue = (strReturnValue.charAt(0) == '.')?"0"+strReturnValue:strReturnValue;
		}

		return strReturnValue;
	} 
    public String getStyle(String strColumnName) {
      HashMap hmRow = getCurrentRow();
      String strStyle = "";
      String colType = this.getColumnType(strColumnName);     
      return strStyle+colType;
  }

  public String getColumnType(String strColumnName) {
      HashMap hmRow = getCurrentRow();
      String strPrefix = "\" type=\"";
      String strColumnType = "ed";
  
      if(!strColumnName.trim().equalsIgnoreCase("Name")&&!strColumnName.trim().equalsIgnoreCase("ID")){
          
      if((String)hmRow.get(strColumnName+"controlvalue")!=null)
          strColumnType = (String)hmRow.get(strColumnName+"controlvalue");
          if(strColumnType!=null){
              if(strColumnType.equalsIgnoreCase("D"))
                  strColumnType = "ro";
              if(strColumnType.equalsIgnoreCase("E"))
                  strColumnType = "ed";
          }
      }else{
          strColumnType = "ro";
      }
    
      strPrefix+=strColumnType+"\"";
      return strPrefix;
  }
  


}
