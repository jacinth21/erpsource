package com.globus.crosstab.beans;

import com.globus.common.beans.GmCrossTabDecorator;

public class GmRowHighlightDecorator extends GmCrossTabDecorator {
	
	public String getTRopen() {
		String strValue;
		if (!getGroupingCount().equals("0")) {
			strValue = "<tr  height =" + getRowHeight() + "  " + getRowClass() + getGroupingCount() + " onmouseover=fnstripedbyid('"+getTrId()+"','lightBrown'); >";
		} else {
			strValue = "<tr  height =" + getRowHeight() + " onmouseover=fnstripedbyid('"+getTrId()+"','lightBrown'); >";
		}
		return strValue;
	}

}
