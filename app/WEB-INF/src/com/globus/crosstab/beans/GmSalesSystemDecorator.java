package com.globus.crosstab.beans;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmSalesSystemDecorator extends GmRowHighlightDecorator {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	public static final String strTurnsLabel = "#Cases<BR> per M/Set";
	public static final String strBreakUpLabel = "Case<BR>BreakUp";
	
	public String get(String strColumnName) {
		HashMap hmRow = getCurrentRow();

//		log.debug("hmRow = " + hmRow);
		String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		String strReturnValue = strValue;
		if (strColumnName.equals("AI Usage<BR>(Quarter)")) {
			String strADId = GmCommonClass.parseNull((String) hmRow.get("ID"));
			String strADName = GmCommonClass.encodeForJS((String) hmRow.get("Name"));
			
			double dblValue = Double.parseDouble(GmCommonClass.parseZero(strValue))/1000;
			strValue =  GmCommonClass.getStringWithCommas(String.valueOf(dblValue),2);
			strReturnValue = "<a href=javascript:fnViewAIQuotaDrillDown('"+strADId+"','"+strADName+"'"+");> " + strValue + "</a>";
		}
		
		if(strColumnName.equals(strTurnsLabel))
		{
			//log.debug("hmRow = "+hmRow);
			DecimalFormat dfTwoDForm = new DecimalFormat("#.#");
			String strCaseBrkup = GmCommonClass.parseNull((String)hmRow.get(strBreakUpLabel));
			String strCaseSet = GmCommonClass.parseNull((String)hmRow.get(strTurnsLabel));
			strCaseSet = strCaseSet.equals("")?"0":strCaseSet;
			String strTemp = dfTwoDForm.format((Double.parseDouble(strCaseSet)));
			//double dbCaseTotal = dfTwoDForm.format((Double.parseDouble(strCaseSet)));
			if(!strCaseBrkup.equals(""))
			{
				strReturnValue = "<b><font color=blue title='"+strCaseBrkup+"'>&nbsp;*&nbsp;</font></b> "+strTemp;				
			}
			
		}
		return strReturnValue;
	}
}