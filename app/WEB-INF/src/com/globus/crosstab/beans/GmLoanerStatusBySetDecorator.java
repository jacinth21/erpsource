package com.globus.crosstab.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmLoanerStatusBySetDecorator extends GmRowHighlightDecorator{

	/*
	 * To Override the TR Style
	 */
	public String getTRwithStyleOpen() {
		String strValue = "<tr style=\"position:relative; top:expression(this.offsetParent.scrollTop);\" height =" + 22 + ">";
		return strValue;
	}
	
	public String getStyle(String strColumnName) {
		String strSort ="";
		
		if ( strColumnName.equals("Name"))
				{
				strSort = " sort=\"str\"";
				}
		else
		{
			strSort = " sort=\"int\"";
		}
				
		
		String strStyle = "";
		String strPrefix = "\" type=\"";
		String strColumnType = "ro\"";			
		String colType = strPrefix+strColumnType;
		strStyle = strStyle+colType+strSort;
		
		return strStyle;
	}
	
	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();	
		strReturnValue=GmCommonClass.parseNull((String)hmRow.get(strColumnName));
		
		if(strColumnName.equalsIgnoreCase("Name")){			
				String strName = String.valueOf((String)hmRow.get("Name"));		
				String strId = String.valueOf((String)hmRow.get("ID"));		
				//log.debug("strId "+strId);
				strName = "<![CDATA[<a href=\"javascript:fnCallLoanerDistDetails(&#39#"+strId+"&#39);\" >" +
						"<img  id=\"imgEdit\" style=\"cursor:hand\" border=\"0\" src=\"/images/icon-letter.png\" width=\"14\" height=\"14\" align=\"left\"></a>"+strName+"  ]]> ";
				return strName;
			}
			
		
		if(!strColumnName.equalsIgnoreCase("Total")){			
			if (strReturnValue.equals("") || strReturnValue.equals("0")){
				strReturnValue="--";
			}
			return strReturnValue;
		}
		
		
		float ftotalvalue = Float.valueOf(strReturnValue).floatValue();
		
		if(strColumnName.equalsIgnoreCase("Total")){			
			String strOpenrequest = GmCommonClass.parseZero((String)hmRow.get("Opn Req"));
			String strPndApproval = GmCommonClass.parseZero((String)hmRow.get("Pnd Apr Req"));
			ftotalvalue = ftotalvalue - (Float.valueOf(strOpenrequest).floatValue()+Float.valueOf(strPndApproval).floatValue());
		}
		
		return String.valueOf(Math.round(ftotalvalue));
	}			
}
