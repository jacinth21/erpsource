package com.globus.crosstab.beans;

import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;

public class GmQueryByTimePointDecorator extends GmRowHighlightDecorator{
	Object dt = null;
	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		dt = (Object)hmRow.get("DUEDATE");
		    if (strColumnName.startsWith("SD") || strColumnName.startsWith("SC") || strColumnName.startsWith("SF")) {
				  sbReturn.setLength(0);
				  if(hmRow.get(strColumnName) == null){
					sbReturn.append("&nbsp;");
					strReturnValue = sbReturn.toString();
				  }	else if (!hmRow.get(strColumnName).equals("")) {					 
						  strReturnValue = (String) hmRow.get(strColumnName);  				  	 
				  }
			  }else{
				  strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
				// If the value is not present in the HashMap, setting it to a space to avoid display of - in the cross tab.
				  if(strReturnValue.equals("")){
					  strReturnValue=" ";
				  }
				  //log.debug("strReturnValue " + strReturnValue);
			  }
	
		return strReturnValue;
	}	
	
public String getStyle(String strColumnName) {
		
		String strStyle = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		  dt = (Object)hmRow.get("DUEDATE");
		  
		 	  if (strColumnName.startsWith("SD") || strColumnName.startsWith("SC") || strColumnName.startsWith("SF")) {
				  sbReturn.setLength(0);
				  if(hmRow.get(strColumnName) == null){
					  //strStyle = "GreyShadeTD" + "\"";
					  strStyle = "GreyShadeTD" + "\" style=\"background-color:#C0C0C0;border:1px solid white;\" ";
				  }	else if (!hmRow.get(strColumnName).equals("")  ) {
					  strStyle = super.getStyle(strColumnName) + "\"";
					  //log.debug("strReturnValue " + strReturnValue);
				  }
			  }else{
				  strStyle = super.getStyle(strColumnName) + "\"";
				  //log.debug("strReturnValue " + strReturnValue);
			  }
		return strStyle;
	}
}
