package com.globus.crosstab.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.crosstab.beans.GmRowHighlightDecorator;

public class GmProjectsReportDecorator extends GmRowHighlightDecorator {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 


	public String get(String strColumnName) {
		HashMap hmRow = getCurrentRow();

//		log.debug("hmRow = " + hmRow);
		String strValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		String strReturnValue = strValue;
		String projectId = GmCommonClass.parseNull((String) hmRow.get("ID"));
		if (strColumnName.equals("Name")) {
			projectId = GmCommonClass.parseNull((String) hmRow.get("ID"));
			strReturnValue = "<a href=javascript:fnViewProjectMembers('"+projectId+"','7000');>" +
							 "<img title='Click here to view surgeon members' style='cursor:hand' src='/images/ordsum.gif'/></a>&nbsp;" + 
						     "&nbsp;<a href=javascript:fnViewProjectMembers('"+projectId+"','7006');>" +
						   	 "<img title='Click here to view globus members' style='cursor:hand' src='/images/group._green.gif'/></a>" +
						     "&nbsp;&nbsp;" + strValue;
		}
		
		if (!strColumnName.equals("Name") && !strColumnName.equals("Status") && !strColumnName.equals("Segment") && !strColumnName.equals("Group")
			&& !strColumnName.equals("Project Manager") && !strColumnName.equals("Product Manager")) {
			
			
			String strDateFlag = GmCommonClass.parseNull((String) hmRow.get(strColumnName+"_FL"));
			if(strDateFlag.equals("T"))
				strReturnValue = "TBD";
		}

		return strReturnValue;
	}

	/*
	 * To Override the TD Style of the column Name
	 */

	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		if (!strColumnName.equals("Name") && !strColumnName.equals("Status") && !strColumnName.equals("Segment") && !strColumnName.equals("Group")
				&& !strColumnName.equals("Project Manager") && !strColumnName.equals("Product Manager")) 
		{
			String strDateFlag = GmCommonClass.parseNull((String) hmRow.get(strColumnName+"_FL"));
			if(strDateFlag.equals("B")) {
				strStyle = "BoldFontSmall";
			} else if(strDateFlag.equals("T")) {
				strStyle = "YellowShadeTD";
			} else if(strDateFlag.equals("R")) {
				strStyle = "BoldRedFontSmall";
			}
		}
		if (strStyle.equals("")) {
			strStyle = super.getStyle(strColumnName);
		}
		return strStyle;
	}
}
