package com.globus.crosstab.beans;

import java.util.HashMap;

import com.globus.common.beans.GmCommonClass;

public class GmRptByOutOfWindowDecorator extends GmRowHighlightDecorator{
	Object dt = null;
	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();
		StringBuffer sbReturn = new StringBuffer();
		dt = (Object)hmRow.get("DUEDATE");
		   
				  strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
				// If the value is not present in the HashMap, setting it to a space to avoid display of - in the cross tab.
				  if(strReturnValue.equals("")){
					  strReturnValue="-";
				  }
				  //log.debug("strReturnValue " + strReturnValue);
			 
	
		return strReturnValue;
	}	
	
public String getStyle(String strColumnName) {
		
		String strStyle = "";
		HashMap hmRow = getCurrentRow();
		log.debug("Row is "+ hmRow);
		String strName = (String)hmRow.get("Name");
		//  log.debug("Name " + strName);
		StringBuffer sbReturn = new StringBuffer();
		  dt = (Object)hmRow.get("DUEDATE");
		  //log.debug("Row Value " + hmRow);
		  
		  if(strColumnName.equalsIgnoreCase("Name") || strColumnName.equalsIgnoreCase("Surgeon Name") || strColumnName.equalsIgnoreCase("Treatment"))
			  strStyle = "ShadeLevel2NoBold" + "\"  style=\"text-align: left; font-size : 9px;\" ";
		  else
			  strStyle = "RightTextSmallCT" + "\"  style=\"text-align: right; font-size : 9px; background-color:white;\" ";
		  
		 
		//  if(strName.equalsIgnoreCase("Randomized ACDF") || strName.equalsIgnoreCase("Randomized ACDF") || strName.equalsIgnoreCase("ALL SECURE-C")) 
	  		  
			  	 
		return strStyle;
	}
}
