/**
 * FileName    : GmLoanerUsageDetailsDecorator.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Dec 30, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.crosstab.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;

/**
 * @author sthadeshwar
 *
 */
public class GmLoanerUsageDetailsDecorator extends GmRowHighlightDecorator {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public String get(String strColumnName) {
		
		HashMap hmRow = getCurrentRow();
		String retStr = (String)hmRow.get(strColumnName);
		String idStr = (String)hmRow.get("ID");
		if(!strColumnName.equals("ID") && !strColumnName.equals("Name") && !strColumnName.equals("Total") && hmRow.get(strColumnName) != null) {
			String strColValue = (String)hmRow.get(strColumnName);
			strColumnName = strColumnName.replaceAll(" ", "");
			idStr = "'"+idStr+"'";
			retStr = "<a href=javascript:fnShowLoanerUsageDetails("+idStr+",'"+strColumnName+"')>"+strColValue+"</a>";
		}
		return retStr;
	}
}
