package com.globus.crosstab.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.globus.common.beans.GmCommonClass;

public class GmGrowthGridDecorator extends GmRowHighlightDecorator {

	public String get(String strColumnName) {
		String strReturnValue = "";
		HashMap hmRow = getCurrentRow();		
		
		if(!strColumnName.equalsIgnoreCase("Description") && !strColumnName.equalsIgnoreCase("Name") && !strColumnName.equalsIgnoreCase("ID") ){
			strColumnName+="refvalue";
		}
		strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strColumnName));
		
		if(strColumnName.equalsIgnoreCase("Description")){
			Set keySet =  hmRow.keySet();
			for(Iterator itr=keySet.iterator(); itr.hasNext();){
				String strKey = (String)itr.next();
				if(strKey.indexOf("Description")!=-1){
					strReturnValue = GmCommonClass.parseNull((String) hmRow.get(strKey));
					break;
				}
			}
			strColumnName+="Description";
		}
		return strReturnValue;
	} 
	public String getStyle(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strStyle = "";
		String strComments = (String)hmRow.get(strColumnName+"comments");
		String strHistoryFl = (String)hmRow.get(strColumnName+"history_fl");
		boolean bolComments = false;
		boolean bolHistoryFl = false;
		
		if(!strColumnName.trim().equalsIgnoreCase("Name")&&!strColumnName.trim().equalsIgnoreCase("ID")){
			if(strComments!=null && strComments.length()>0){
				bolComments =true;
				strStyle = "ShadeMedRedGrid";
			}
			
			if(strHistoryFl!=null && strHistoryFl.length()>0){
				bolHistoryFl = true;
				strStyle = "ShadeMedYellowGrid";
			}
			if(bolHistoryFl && bolComments ){
				strStyle = "ShadeDarkBrownGrid";
			}
		}
		String colType = this.getColumnType(strColumnName)+this.getAdditionalAttribute(strColumnName);
		
		return strStyle+colType;
	}

	public String getColumnType(String strColumnName) {
		HashMap hmRow = getCurrentRow();
		String strPrefix = "\" type=\"";
		String strColumnType = "ed";
		boolean isPastDate = false;
		if(!strColumnName.trim().equalsIgnoreCase("Name")&&!strColumnName.trim().equalsIgnoreCase("ID")){
			
		if((String)hmRow.get(strColumnName+"controlvalue")!=null)
			strColumnType = (String)hmRow.get(strColumnName+"controlvalue");
			if(strColumnType!=null){
				if(strColumnType.equalsIgnoreCase("D"))
					strColumnType = "ro";
				if(strColumnType.equalsIgnoreCase("E"))
					strColumnType = "ed";
			}
		}else{
			strColumnType = "ro";
		}
		isPastDate = this.checkDate(strColumnName);
		if(isPastDate){
			strColumnType="ro";
		}		
		strPrefix+=strColumnType+"\"";
		return strPrefix;
	}
	
	public String getAdditionalAttribute(String strColumnName){
		HashMap hmRow = getCurrentRow();
		String strGrowthDetailsID = (String)hmRow.get(strColumnName+"grw_details_id");
		String strAddAttr=" growth_det_id=\""+strGrowthDetailsID+"\" ";
		return strAddAttr;
	}
	private boolean checkDate(String strcolumnDate){
		boolean isPastDate = false;
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMM' 'yy");
			Calendar cal = Calendar.getInstance();			
			Date columnDate = dateFormat.parse(strcolumnDate);
			Date sysdate = dateFormat.parse((dateFormat.format(cal.getTime())));
			isPastDate = sysdate.after(columnDate);
			
		}catch(Exception e){
			isPastDate=true;
		}
		return isPastDate;

	}
}
