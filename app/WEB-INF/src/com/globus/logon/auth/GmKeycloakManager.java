package com.globus.logon.auth;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;
/*
 * This class is to manage the Keycloak user (fetching user details from Keycloak)
 */

public class GmKeycloakManager {
	  Logger log = GmLogger.getInstance(this.getClass().getName());
	
	 //To fetch user details from Keycloak through Microapp
	public HashMap fetchKeycloakUserDetail(String strUserName)  {
		// new HashMap - hmReturn
		HashMap hmReturn = new HashMap();
		
		try{

			//getting the URL  from jboss system property using the key �ENV_KEYCLOAK�
             String strKeycloakURL = System.getProperty("ENV_KEYCLOAK");

             //URI for webservices call with microapp common
             String uri = strKeycloakURL+
                  "keycloak/fetchUserInfo/"+strUserName;
			URL url = new URL(uri);
			HttpURLConnection connection =
			    (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			InputStream xml = connection.getInputStream();
						
		    BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
		    String output;
		    StringBuffer sbQuery = new StringBuffer();
		    while ((output = br.readLine()) != null) {
		      sbQuery.append(output);
		    }
			String response= sbQuery.toString();
			if(!response.isEmpty() && response != null ) {
				//Spliting the String using �|� symbol 
				String[] result= response.split("\\|");
				if(result.length == 3 || result.length == 4 ) {
					//Adding user details in hashmap hmReturn 	
					hmReturn.put("FIRSTNAME", result[1]);
					hmReturn.put("LASTNAME", result[2]);
					hmReturn.put("PASSWORD", "password"); 
				}
				if(result.length == 4) {
					hmReturn.put("EMAILADDRESS", result[3]);
				}
				
			}
			
		}catch(Exception e){
			log.debug("exception: "+e.getMessage());
			e.printStackTrace();
		}
		//Returning HashMap hmReturn
             return hmReturn;
	}
}	   
