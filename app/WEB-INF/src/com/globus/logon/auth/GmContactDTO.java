package com.globus.logon.auth;

import java.util.HashMap;

public class GmContactDTO {
	private String commonName;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String description;
	private String uid;
	private String password;
	private String initials;
	private String sAMAccountName;
	private String OU;
	private String domain;
	private String distinguishedName;
	private String userAccountControl;

	public String getUserAccountControl() {
		return userAccountControl;
	}

	public void setUserAccountControl(String userAccountControl) {
		this.userAccountControl = userAccountControl;
	}

	/**
	 * @return the domain
	 */
	public String getDomain() {
		domain = "DC=gminc,DC=local";
		return domain;
	}

	/**
	 * @param domain
	 *            the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}

	/**
	 * @return the oU
	 */
	public String getOU() {
		return OU;
	}

	/**
	 * @param ou
	 *            the oU to set
	 */
	public void setOU(String ou) {
		OU = ou;
	}

	/**
	 * @return the sAMAccountName
	 */
	public String getSAMAccountName() {
		return sAMAccountName;
	}

	/**
	 * @param accountName
	 *            the sAMAccountName to set
	 */
	public void setSAMAccountName(String accountName) {
		sAMAccountName = accountName;
	}

	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}

	/**
	 * @return the initials
	 */
	public String getInitials() {
		return initials;
	}

	/**
	 * @param initials
	 *            the initials to set
	 */
	public void setInitials(String initials) {
		this.initials = initials;
	}

	public String getCommonName() {
		return commonName;
	}

	public void setCommonName(String commonName) {
		this.commonName = commonName;
		int firstNameIndex = commonName.indexOf(" ");

		if (firstNameIndex != -1) {
			this.firstName = commonName.substring(0, firstNameIndex);
			this.lastName = commonName.substring(firstNameIndex, commonName.length()).trim();
		} else {
			this.firstName = commonName;
		}

	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String toString() {
		StringBuffer contactDTOStr = new StringBuffer("Person=[");
		contactDTOStr.append(" Common Name = " + commonName);
		contactDTOStr.append(", Last Name = " + lastName);
		contactDTOStr.append(", Description = " + description);
		contactDTOStr.append(", EmailId = " + emailAddress);
		contactDTOStr.append(", initials = " + initials);
		contactDTOStr.append(", sAMAccountName = " + sAMAccountName);
		contactDTOStr.append(", distinguishedName = " + distinguishedName);
		contactDTOStr.append(", userAccountControl = " + userAccountControl);
		contactDTOStr.append(" ]");
		return contactDTOStr.toString();
	}

	public HashMap getUserMap() {
		HashMap hmUserDetails = new HashMap();

		hmUserDetails.put("COMMONNAME", commonName);
		hmUserDetails.put("FIRSTNAME", firstName);
		hmUserDetails.put("LASTNAME", lastName);
		hmUserDetails.put("EMAILADDRESS", emailAddress);
		hmUserDetails.put("DESCRIPTION", description);
		hmUserDetails.put("UID", uid);
		hmUserDetails.put("PASSWORD", password);
		hmUserDetails.put("INITIALS", initials);
		hmUserDetails.put("SAMACCOUNTNAME", sAMAccountName);
		hmUserDetails.put("OU", OU);
		hmUserDetails.put("DOMAIN", domain);
		hmUserDetails.put("DISTINGUISHEDNAME", distinguishedName);
		hmUserDetails.put("USERACCOUNTCONTROL", userAccountControl);

		return hmUserDetails;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the distinguishedName
	 */
	public String getDistinguishedName() {
		return distinguishedName;
	}

	/**
	 * @param distinguishedName
	 *            the distinguishedName to set
	 */
	public void setDistinguishedName(String distinguishedName) {
		this.distinguishedName = distinguishedName;
	}

}

