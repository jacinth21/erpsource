package com.globus.logon.auth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;

import org.apache.log4j.Logger;
import org.springframework.ldap.AuthenticationException;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CollectingAuthenticationErrorCallback;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.LikeFilter;

import com.globus.common.beans.GmLogger;

public class GmLdapDAO {
	private LdapTemplate ldapTemplate;
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.

	public void setLdapTemplate(LdapTemplate ldapTemplate) {
		this.ldapTemplate = ldapTemplate;
		this.ldapTemplate.setIgnorePartialResultException(true);
	}

	public boolean login(String username, String password) throws AuthenticationException {
		boolean blAuthenticate = false;
		AndFilter filter = new AndFilter();
		filter.and(new EqualsFilter("objectclass", "user")).and(new EqualsFilter("sAMAccountName", username));
		log.debug("LDAP Query for login " + filter.encode());

		CollectingAuthenticationErrorCallback errorCallback = new CollectingAuthenticationErrorCallback();
		blAuthenticate = ldapTemplate.authenticate(DistinguishedName.EMPTY_PATH, filter.toString(), password, errorCallback);

		if (!blAuthenticate) {
			Exception exception = errorCallback.getError();
			if (exception instanceof AuthenticationException) {
				AuthenticationException authExp = (AuthenticationException) exception;
				throw authExp;
			}
		}
		return blAuthenticate;
	}

	public HashMap getADUserInfo(String username) {
		List alContactDetails = new ArrayList();
		HashMap hmUserDetails = new HashMap();
		int size = 0;

		AndFilter andFilter = new AndFilter();
		andFilter.and(new EqualsFilter("objectclass", "user"));
		andFilter.and(new EqualsFilter("sAMAccountname", username));
		log.debug("LDAP Query " + andFilter.encode());

		alContactDetails = ldapTemplate.search("", andFilter.encode(), new GmContactAttributeMapper());
		size = alContactDetails == null ? 0 : alContactDetails.size();
		
		log.debug(" size is  " + size);

		for (int i = 0; i < alContactDetails.size(); i++) {
			log.debug(" Value at " + i + " is " + alContactDetails.get(i));
			GmContactDTO contactDTO = (GmContactDTO) alContactDetails.get(i);
			hmUserDetails = contactDTO.getUserMap();
		}
		return hmUserDetails;
	}

	public boolean doesUserExist(String username) {
		boolean blDoesUserExist = false;
		List alContactDetails = new ArrayList();
		int size = 0;

		AndFilter andFilter = new AndFilter();
		andFilter.and(new EqualsFilter("objectclass", "user"));
		andFilter.and(new EqualsFilter("sAMAccountname", username));

		alContactDetails = ldapTemplate.search("", andFilter.encode(), new GmContactAttributeMapper());
		if (alContactDetails != null)
			size = alContactDetails.size();

		if (size > 0)
			blDoesUserExist = true;

		return blDoesUserExist;
	}

	public void updateContact(GmContactDTO contactDTO) {
		Attributes personAttributes = new BasicAttributes();
		BasicAttribute personBasicAttribute = new BasicAttribute("objectclass");
		personBasicAttribute.add("person");
		personAttributes.put(personBasicAttribute);
		personAttributes.put("cn", contactDTO.getCommonName());
		personAttributes.put("sn", contactDTO.getLastName());
		personAttributes.put("description", contactDTO.getDescription());

		DistinguishedName newContactDN = new DistinguishedName("ou=users");
		// newContactDN.add("cn", contactDTO.getCommonName());
		newContactDN.add("sAMAccountname", contactDTO.getSAMAccountName());

		ldapTemplate.rebind(newContactDN, null, personAttributes);
	}
	
 public List getUsers(String strUserName) {
 		List alResult  = new ArrayList();	
 		Map hmUser = new HashMap();
        AndFilter filter = new AndFilter();  
        filter.and(new EqualsFilter("objectclass", "user")).and(new LikeFilter("sAMAccountName", strUserName)); 		          
        List users = ldapTemplate.search(DistinguishedName.EMPTY_PATH,  filter.encode(),   new GmContactAttributeMapper());  
          
          for (int i = 0; i < users.size(); i++) 
          {		  	
  			GmContactDTO contactDTO = (GmContactDTO) users.get(i);
  			hmUser = contactDTO.getUserMap();		  			
  			String userAccountControl =  (String)hmUser.get("USERACCOUNTCONTROL");		  			
  			if(userAccountControl != null && userAccountControl.equals("514"))
  				userAccountControl = "InActive";
  			else
  				userAccountControl = "Active";		  			
  			hmUser.put("USERACCOUNTCONTROL",userAccountControl);		  			
  			alResult.add(hmUser);
  		  }
          
          return alResult;  
      }  

 public Map getUser(String strUserName) {
			
		Map hmUser = new HashMap();
     AndFilter filter = new AndFilter();  
     filter.and(new EqualsFilter("objectclass", "user")).and(new EqualsFilter("sAMAccountName", strUserName)); 		          
     List users = ldapTemplate.search(DistinguishedName.EMPTY_PATH,  filter.encode(),   new GmContactAttributeMapper());  
       
       for (int i = 0; i < users.size(); i++) 
       {		  	
			GmContactDTO contactDTO = (GmContactDTO) users.get(i);
			hmUser = contactDTO.getUserMap();		  			
			String userAccountControl =  (String)hmUser.get("USERACCOUNTCONTROL");		  			
			if(userAccountControl != null && userAccountControl.equals("514"))
				userAccountControl = "InActive";
			else
				userAccountControl = "Active";		  			
			hmUser.put("USERACCOUNTCONTROL",userAccountControl);		  			
			
		  }
       
       return hmUser;  
   }  
 
 public void disableUser(HashMap hmUser) throws Exception {		
		ModificationItem[] mods = new ModificationItem[1];							
		mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
		    new BasicAttribute("userAccountControl", "514"));
		String strName = (String)hmUser.get("DISTINGUISHEDNAME");
		strName = strName.substring(0, strName.indexOf("DC") -1);
		DistinguishedName groupDn = new DistinguishedName(strName);
		DirContext dirContext = ldapTemplate.getContextSource().getReadWriteContext();
		dirContext.modifyAttributes(groupDn, mods)  ;      
	}


 public void enableUser(HashMap hmUser) throws Exception {		
		ModificationItem[] mods = new ModificationItem[1];							
		mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
		    new BasicAttribute("userAccountControl", "512"));
		String strName = (String)hmUser.get("DISTINGUISHEDNAME");
		strName = strName.substring(0, strName.indexOf("DC") -1);
		DistinguishedName groupDn = new DistinguishedName(strName);
		DirContext dirContext = ldapTemplate.getContextSource().getReadWriteContext();
		dirContext.modifyAttributes(groupDn, mods)  ;          
	}

}
