package com.globus.logon.auth;

import java.util.List;

public interface ContactDAO {

    public List getAllContactNames();
    
    public List getContactDetails(String commonName, String lastName);
    
    public void insertContact(GmContactDTO contactDTO);
    
    public void updateContact(GmContactDTO contactDTO);
    
    public void deleteContact(GmContactDTO contactDTO);
   
    
    public boolean login(String username, String password);
}

