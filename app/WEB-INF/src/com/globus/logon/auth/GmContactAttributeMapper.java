package com.globus.logon.auth;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.springframework.ldap.core.AttributesMapper;

public class GmContactAttributeMapper implements AttributesMapper{

	public Object mapFromAttributes(Attributes attributes) throws NamingException {
		GmContactDTO contactDTO = new GmContactDTO();
		
		String commonName = (String)attributes.get("cn").get();
		if(commonName != null)
			contactDTO.setCommonName(commonName);
		
		Attribute description = attributes.get("description");
		if(description != null)
			contactDTO.setDescription((String)description.get());
		
		Attribute attrUid = attributes.get("mail");
		if(attrUid != null)
			contactDTO.setEmailAddress((String)attrUid.get());
		
		Attribute distinguishedName = attributes.get("distinguishedName");
		if(distinguishedName != null)
			contactDTO.setDistinguishedName((String)distinguishedName.get());
		
		Attribute sAMAccountName = attributes.get("sAMAccountName");
		if(distinguishedName != null)
			contactDTO.setSAMAccountName((String)sAMAccountName.get());
		
		
		Attribute userAccountControl = attributes.get("userAccountControl");
		if(userAccountControl != null)
			contactDTO.setUserAccountControl((String)userAccountControl.get());
		
		return contactDTO;
	}

}
