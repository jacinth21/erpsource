package com.globus.logon.auth;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.ldap.AuthenticationException;
import org.springframework.ldap.core.support.LdapContextSource;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSecurityBean;
import com.globus.common.crypt.AESEncryptDecrypt;

/*
 * <property name="url" value="ldap://gmiroot:389" /> <property name="base"
 * value="DC=gminc,DC=local" /> <property name="userDn"
 * value="CN=Beta Admin,OU=IT users,OU=IT,DC=gminc,DC=local" /> <property name="password"
 * value="beta2560admin" />
 */

public class GmLDAPManager {

  Resource resource;
  BeanFactory factory;
  private LdapContextSource ldapContextSource;
  private GmLdapDAO ldapDAO;
  Logger log = GmLogger.getInstance(this.getClass().getName());
  private static String strDefaultLDAP = GmCommonClass.getRuleValue("DEFAULT_LDAP", "LDAP");
  private static String strDbLDAP = GmCommonClass.getRuleValue("DB_AUTH", "LDAP");

  public GmLDAPManager() {
    initLdapContext(strDefaultLDAP);
  }

  public GmLDAPManager(String strLdapId) {
    // 3 - DB Auth
    if (strLdapId.equals(strDbLDAP) || strLdapId.equals("")) {
      strLdapId = strDefaultLDAP;
    }
    initLdapContext(strLdapId);
  }

  public GmLDAPManager(String strUserName, String strPassWord) {
    initializeLDAP(strUserName, strPassWord);
  }

  /**
   * constructor - used to test LDAP connection
   * 
   * @param hmParam - LDAP connection values from UI
   * @throws Exception
   */
  public GmLDAPManager(HashMap hmParam) throws Exception {
    initializeLDAP(hmParam);
  }

  /**
   * initializeLDAP - used to test LDAP connection
   * 
   * @param hmParam - LDAP connection values from UI
   * @throws Exception
   */
  public void initializeLDAP(HashMap hmParam) throws Exception {
    GmSecurityBean gmSecurityBean = new GmSecurityBean();
    resource = new ClassPathResource("com/globus/properties/springldap.xml");
    factory = new XmlBeanFactory(resource);
    String strUserName = GmCommonClass.parseNull((String) hmParam.get("USERNAME"));
    String strPwd = GmCommonClass.parseNull((String) hmParam.get("PASSWORD"));
    String strHostName = GmCommonClass.parseNull((String) hmParam.get("HOSTNAME"));
    String strPort = GmCommonClass.parseNull((String) hmParam.get("PORT"));
    String strUserDN = GmCommonClass.parseNull((String) hmParam.get("USERDN"));
    String strBase = GmCommonClass.parseNull((String) hmParam.get("BASE"));

    ldapContextSource = (LdapContextSource) factory.getBean("contextSource");
    ldapContextSource.setUrl("ldap://" + strHostName + ":" + strPort);
    ldapContextSource.setUserDn("CN=" + strUserName + "," + strUserDN);
    ldapContextSource.setPassword(strPwd);
    ldapContextSource.setBase(strBase);
    ldapContextSource.afterPropertiesSet();

    ldapDAO = (GmLdapDAO) factory.getBean("ldapDAO");
    log.debug(" test connection LDAP context... ");
  }

  public void initializeLDAP(String strUserName, String strPassword) {
    initializeLDAP(strDefaultLDAP);
    AESEncryptDecrypt aesEncryptDecrypt = new AESEncryptDecrypt();
    strUserName = aesEncryptDecrypt.decryptText(strUserName);
    strPassword = aesEncryptDecrypt.decryptText(strPassword);
    HashMap hmMap = getADUserDetails(strUserName);
    String dnName = (String) hmMap.get("DISTINGUISHEDNAME");
    ldapContextSource.setUserDn(dnName);
    ldapContextSource.setPassword(strPassword);
    log.debug(" INitialized LDAP context for the Logged in User ..... ");
  }


  public void initializeLDAP(String strLdapId) {
    try {
      GmSecurityBean gmSecurityBean = new GmSecurityBean();
      resource = new ClassPathResource("com/globus/properties/springldap.xml");
      factory = new XmlBeanFactory(resource);
      HashMap hmLdapCrd = GmCommonClass.hmLdapCrd;
      log.debug(" LDAP ID  --> " + strLdapId);
      hmLdapCrd = (HashMap) hmLdapCrd.get(strLdapId);
      String strUserName = GmCommonClass.parseNull((String) hmLdapCrd.get("USERNAME"));
      String strPwd = GmCommonClass.parseNull((String) hmLdapCrd.get("PASSWORD"));
      String strHostName = GmCommonClass.parseNull((String) hmLdapCrd.get("HOSTNAME"));
      String strPort = GmCommonClass.parseNull((String) hmLdapCrd.get("PORT"));
      String strUserDN = GmCommonClass.parseNull((String) hmLdapCrd.get("USERDN"));
      String strBase = GmCommonClass.parseNull((String) hmLdapCrd.get("BASE"));
      strPwd = gmSecurityBean.decrypt(strPwd, strUserName);

      ldapContextSource = (LdapContextSource) factory.getBean("contextSource");
      ldapContextSource.setUrl("ldap://" + strHostName + ":" + strPort);
      ldapContextSource.setUserDn("CN=" + strUserName + "," + strUserDN);
      ldapContextSource.setPassword(strPwd);
      ldapContextSource.setBase(strBase);
      ldapContextSource.afterPropertiesSet();

      ldapDAO = (GmLdapDAO) factory.getBean("ldapDAO");
      log.debug(" INitialized LDAP context... ");
    } catch (Exception e) {
      e.printStackTrace();
      log.debug("Error occured while initializing " + e.getCause());
    }
  }

  public void initLdapContext(String strLdapId) {
    initializeLDAP(strLdapId);
  }

  public void initLdapContext(String strUserName, String strPassword) {

    initializeLDAP(strUserName, strPassword);

  }

  public boolean authenticateUser(String strUserName, String strPassword) {
    boolean blLogin = false;
    try {
      blLogin = ldapDAO.login(strUserName, strPassword);
      log.debug("login status " + blLogin);
    }

    catch (AuthenticationException authExp) {
      throw authExp;
    }
    return blLogin;
  }

  public HashMap getADUserDetails(String strUserName) {
    HashMap hmADUserInfo = ldapDAO.getADUserInfo(strUserName);
    return hmADUserInfo;
  }

  public boolean doesUserExist(String strUserName) {
    boolean blDoesUserExist = ldapDAO.doesUserExist(strUserName);
    return blDoesUserExist;
  }

  public List getUsers(String strUserName) {
    return ldapDAO.getUsers(strUserName);
  }

  public Map getUser(String strUserName) {
    return ldapDAO.getUser(strUserName);
  }


  public void disableUser(String strUserName) throws Exception {
    HashMap hmUser = getADUserDetails(strUserName);
    ldapDAO.disableUser(hmUser);
  }

  public void enableUser(String strUserName) throws Exception {
    HashMap hmUser = getADUserDetails(strUserName);
    ldapDAO.enableUser(hmUser);
  }
}
