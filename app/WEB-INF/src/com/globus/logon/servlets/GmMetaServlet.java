package com.globus.logon.servlets;


import com.globus.logon.beans.*;
import com.globus.common.beans.*;
import com.globus.common.servlets.*;
import com.globus.common.util.GmCookieManager;
//JAVA Classes
import java.util.*;
import java.lang.reflect.*;
import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
/**
 * @author mselvamani
 * 
 */

public class GmMetaServlet extends GmServlet
{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**
	   * loadFieldSalesList - This is the service method of the servlet to get metaInfo value in FileReader 
	   * 
	   * @param request
	   * @param response
	   * @throws ServletException
	   * @throws IOException
	   */
    public void service(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException
    {
		HttpSession session = request.getSession(false);
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		response.setHeader("control-cache","no-cache");
		String metaInfo = GmFileReader.findVersionInfo();
		out.println(metaInfo);
	    out.close();
    }
}