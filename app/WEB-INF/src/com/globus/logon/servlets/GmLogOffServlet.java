/**
* This class is the handler for the Logoff Process
* @author $Author:$
* @version $Revision:$  <br>
* $Date:$
*/

package com.globus.logon.servlets;

import com.globus.logon.beans.*;
import com.globus.common.beans.*;
import com.globus.common.servlets.*;
import com.globus.common.util.GmCookieManager;

//JAVA Classes
import java.util.*;
import java.lang.reflect.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.naming.Context;
import javax.naming.InitialContext;

public class GmLogOffServlet extends GmServlet
{

	   /* This is the service method of the servlet and it take two parameters
       i.e HttpServletRequest and HttpServletResponse.*/

    public void service(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException
    {
		HttpSession session = request.getSession(false);
		response.setContentType("text/html");
		response.setHeader("control-cache","no-cache");
		HashMap hmParam = new HashMap();
		String strSessionId = request.getSession().getId();
		hmParam.put("SESSID",strSessionId);
		GmSecurityBean gmSecurityBean = new GmSecurityBean();
		
		GmCookieManager gmCookieManager = new GmCookieManager();
		String strGen = gmCookieManager.getCookieValue(request, "gen");
		String strSwitchUserFl = "";
		
		try
		{
			String strUserName = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
			strSwitchUserFl = GmCommonClass.parseNull((String)session.getAttribute("strSwitchUserFl"));
			
			//Code added to delete the cookie on log out.
			gmCookieManager.deleteCookie(request, response);
			if(strGen != null)
			{
				gmSecurityBean.saveForgotUser(strUserName);								 
			}
			
			
			if (session == null) // Checks if the current session is the logged-in session, else redirecting to SessionExpiry.jsp
			{
			   gotoPage(GmCommonClass.getString("GMCOMMON").concat("/GmSessionExpiry.jsp"),request,response);
			   return;
		    }
		    
			if (session != null)
			{
				GmLogonBean gmLogonBean = new GmLogonBean();
				// if user not used the switch user option then only update the t103 table.
				if (!strSwitchUserFl.equalsIgnoreCase("Y")) {
					gmLogonBean.userTracking(hmParam, strUserName, "logoff");
				}
                session.invalidate();
                session = null; // Makes the current session null after invalidating
                gotoPage(GmCommonClass.getString("GMCOMMON").concat("/GmSessionExpiry.jsp"),request,response);
			}

		}
		catch (Exception e)
		{
			session.setAttribute("hAppErrorMessage",e.getMessage());
			gotoPage(GmCommonClass.getString("GMCOMMON").concat("/GmError.jsp"),request,response);
		}
    }
}