
/**********************************************************************************
 * File		 		: GmUserContactBean.java
 * Desc		 		: This screen is used to get users personal info
 * Version	 		: 1.0
 * author			: Arunkumar.A
************************************************************************************/



package com.globus.logon.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmUserContactBean {

Logger log = GmLogger.getInstance(this.getClass().getName());
  public  HashMap getUserContact(String  strUserid ) throws AppError {
	 
	     HashMap hmReturn = new HashMap();
	     GmDBManager gmDBManager = new GmDBManager();
         StringBuffer sbQuery = new StringBuffer();
	     sbQuery.append(" SELECT T101.C101_USER_ID USERID,GET_USER_NAME(T101.C101_USER_ID) USERNM, ");
	     sbQuery.append(" MAX(DECODE (CON.C901_MODE, 90452, CON.C107_CONTACT_VALUE)) EMAIL, ");
	     sbQuery.append(" MAX(DECODE (CON.C901_MODE, 90450, CON.C107_CONTACT_VALUE)) PHONE " );
	     sbQuery.append(" FROM T101_USER T101,(SELECT RTRIM (XMLAGG (XMLELEMENT (N, C107_CONTACT_VALUE ");
	     sbQuery.append(" ||' / ')) .EXTRACT ('//text()'), ' / ') C107_CONTACT_VALUE,C901_MODE FROM");
	     sbQuery.append(" (SELECT T107.C901_MODE,T107.C107_CONTACT_VALUE FROM T107_CONTACT T107,T101_USER T101 ");
	     sbQuery.append(" WHERE T107.C101_PARTY_ID = T101.C101_PARTY_ID AND T107.C107_VOID_FL   IS NULL ");
	     sbQuery.append("AND C107_INACTIVE_FL   IS NULL  AND C107_PRIMARY_FL ='Y' AND  C101_USER_ID =' " + strUserid + " ')");
	     sbQuery.append(" GROUP BY C901_MODE  ) CON WHERE T101.C101_USER_ID = '" + strUserid + "'  ");
	     sbQuery.append(" GROUP BY T101.C101_USER_ID ");
	     log.debug(" The contact Fetch is **** " + sbQuery.toString());
	     hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());
	     log.debug(hmReturn);
	 return hmReturn;     
  }}