package com.globus.logon.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;


public class GmUserTokenBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());


  /**
   * This method will be called to save the Token Information and the Device Information. When the
   * Device Type and the UUID is not passed in, this method will not do any operation.
   * 
   * @param HashMap
   */
  public void saveTokenDeviceInfo(HashMap hmParam) throws AppError {
    String strDeviceType = GmCommonClass.parseNull((String) hmParam.get("DEVICE_TYPE"));
    String strUUID = GmCommonClass.parseNull((String) hmParam.get("UUID"));

    // When the device Type is passed , persis the token and device info in the DB.
    if (!strDeviceType.equals("") && !strUUID.equals("")) {
      GmDBManager gmDBManager = new GmDBManager();
      saveUserToken(hmParam, gmDBManager);
      saveDeviceInfo(hmParam, gmDBManager);
      gmDBManager.commit();
    }
  }

  /**
   * This method will be called to save the Token Information .
   * 
   * @param HashMap
   */
  public void saveUserToken(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_it_user_token.gm_sav_user_token", 3);
    gmDBManager.setString(1, (String) hmParam.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParam.get("STATUS"));
    gmDBManager.setString(3, (String) hmParam.get("USERID"));
    gmDBManager.execute();

  }

  /**
   * This is an overloaded method to save the Token Information .
   * 
   * @param HashMap
   */
  public void saveUserToken(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    saveUserToken(hmParam, gmDBManager);
    gmDBManager.commit();
  }

  /**
   * This method will be called to save the Device Information . *
   * 
   * @param HashMap
   */
  public void saveDeviceInfo(HashMap hmParam, GmDBManager gmDBManager) throws AppError {
    gmDBManager.setPrepareString("gm_pkg_it_user_token.gm_sav_device_info", 4);
    gmDBManager.setString(1, (String) hmParam.get("UUID"));
    gmDBManager.setString(2, (String) hmParam.get("DEVICE_TYPE"));
    gmDBManager.setString(3, (String) hmParam.get("USERID"));
    gmDBManager.setString(4, (String) hmParam.get("DEVICETOKEN"));
    gmDBManager.execute();

  }

  /**
   * This is an overloaded method to save the Device Information .
   * 
   * @param HashMap
   */
  public void saveDeviceInfo(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    saveDeviceInfo(hmParam, gmDBManager);
    gmDBManager.commit();
  }

}
