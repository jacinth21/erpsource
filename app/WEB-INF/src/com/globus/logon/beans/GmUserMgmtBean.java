/*
 * Module: GmLogonBean.java Author: Dhinakaran James Project: Globus Medical App - SpineIT
 * Date-Written: 11 Mar 2004 Security: Unclassified Description: This bean contains methods that are
 * used for the logon process.
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.logon.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.logon.auth.GmLDAPManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmUserMgmtBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  GmCommonClass gmCommon = new GmCommonClass();


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmUserMgmtBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmUserMgmtBean() {
    // TODO Auto-generated constructor stub
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public HashMap loadUserMgmtLists() throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();

    ArrayList alType = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alDept = new ArrayList();
    ArrayList alUserType = new ArrayList();
    ArrayList alUserTitle = new ArrayList();
    ArrayList alSecGroup = new ArrayList();
    ArrayList alCompany = new ArrayList();


    alType = gmCommon.getCodeList("USRTP");
    alStatus = gmCommon.getCodeList("EMPST");
    alDept = gmCommon.getCodeList("DEPID");
    alUserType = gmCommon.getCodeList("USRTP");
    alUserTitle = gmCommon.getCodeList("DSGTP");
    alSecGroup = gmAccessControlBean.fetchACGroupDetails("92264");
    alCompany = gmCommonBean.getCompanyList();

    hmReturn.put("TYPE", alType);
    hmReturn.put("STATUS", alStatus);
    hmReturn.put("DEPT", alDept);
    hmReturn.put("USERTYPE", alUserType);
    hmReturn.put("USERTITLE", alUserTitle);
    hmReturn.put("SECGRP", alSecGroup);
    hmReturn.put("COMPANY", alCompany);

    return hmReturn;
  } // End of loadUserMgmtLists

  public boolean isUserMappedToSecurityGroup(String strUserId) throws AppError {
    boolean isUserExists = false;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_user_sg_cnt", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.INTEGER);
    gmDBManager.setString(1, strUserId);
    gmDBManager.execute();
    int iCount = (gmDBManager.getInt(2));
    gmDBManager.close();

    if (iCount > 0) {
      isUserExists = true;
    }

    return isUserExists;
  }

  /**
   * fetchModuleGroups() : Used to fetch the All the groups created for the left menu
   * 
   * @return ArrayList - user records
   */
  public ArrayList fetchActiveModules(HashMap hmParam) throws AppError {
    ArrayList alModuleGroupList = new ArrayList();
    String strLoginUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_modules", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLoginUserId);
    gmDBManager.execute();
    alModuleGroupList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alModuleGroupList;
  }

  public ArrayList getUserList(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strUsrType = GmCommonClass.parseNull((String) hmParam.get("USRTYPE"));
    String strUsrStatus = GmCommonClass.parseNull((String) hmParam.get("USRSTAT"));
    String strDept = GmCommonClass.parseNull((String) hmParam.get("DEPARTMENTID"));
    String strFName = GmCommonClass.parseNull((String) hmParam.get("FNAME"));
    String strLName = GmCommonClass.parseNull((String) hmParam.get("LNAME"));
    String strAccess = GmCommonClass.parseNull((String) hmParam.get("ACCESSLEVEL"));
    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
  
    String strLdapID = GmCommonClass.parseZero((String) hmParam.get("LDAPID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	t101.C101_USER_F_NAME FNAME, t101.C101_USER_L_NAME LNAME, t101.C101_USER_ID ID, t102.C102_LOGIN_USERNAME UNAME, ");
    sbQuery
        .append(" t101.C901_DEPT_ID DEPID, t101.C101_ACCESS_LEVEL_ID ACCESSID, get_code_name_alt(t101.C901_DEPT_ID) DEPTID, ");
    sbQuery
        .append(" get_code_name(t101.C901_USER_STATUS) STATUS, get_code_name(t101.C901_USER_TYPE) TYPE ");
    sbQuery
        .append(" , T102.C102_EXT_ACCESS EXT_ACC ,  GET_CODE_NAME(T102.C901_AUTH_TYPE) AUTHTYPE,T102.C102_LOGIN_USERNAME UNAME, GET_USER_LAST_LOGIN(t101.C101_USER_ID) LASTLOG");
    sbQuery
        .append(" ,T102.C102_LOGIN_LOCK_FL USRLOCKFL, nvl(T102.C102_PASSWORD_EXPIRED_FL,'N') PWDEXPFL, T102.C102_PASSWORD_UPDATED_DATE PWDUPDDT ");
    sbQuery.append(" ,gm_pkg_it_user.get_default_group_nm(t101.C101_USER_ID) DFLTGRP");
    sbQuery.append(" ,t906c.c906c_ldap_name ldap_Name, t906c.c906c_ldap_master_id ldap_id ");
    sbQuery.append(" , t101.C1900_DEF_COMPANY_ID,  get_company_name(T1019.C1900_COMPANY_ID) COMPANY_NAME");
    sbQuery.append(" FROM T101_USER T101, T102_USER_LOGIN T102, t906c_ldap_master t906c,T1019_PARTY_COMPANY_MAP T1019 ");
    sbQuery.append(" WHERE t101.C101_USER_ID IS NOT NULL");
    sbQuery.append(" AND T101.C101_USER_ID = T102.C101_USER_ID(+) ");
    sbQuery.append(" AND t102.c906c_ldap_master_id =  t906c.c906c_ldap_master_id(+) ");
    if (!strUsrType.equals("0")) {
      sbQuery.append(" AND t101.C901_USER_TYPE = ");
      sbQuery.append(strUsrType);
    }

    if (!strDept.equals("0")) {
      sbQuery.append(" AND t101.C901_DEPT_ID = ");
      sbQuery.append(strDept);
    }
    if (!strUsrStatus.equals("0")) {
      sbQuery.append(" AND t101.C901_USER_STATUS = ");
      sbQuery.append(strUsrStatus);
    }
    if (!strFName.equals("")) {
      sbQuery.append(" AND upper(t101.C101_USER_F_NAME) LIKE upper(('%");
      sbQuery.append(strFName);
      sbQuery.append("%'))");
    }
    if (!strLName.equals("")) {
      sbQuery.append(" AND upper(t101.C101_USER_L_NAME) LIKE upper(('%");
      sbQuery.append(strLName);
      sbQuery.append("%'))");
    }
    if (!strAccess.equals("")) {
      sbQuery.append(" AND t101.C101_ACCESS_LEVEL_ID = ");
      sbQuery.append(strAccess);
    }
    if (!strLdapID.equals("0")) {
      sbQuery.append(" AND t102.c906c_ldap_master_id = " + strLdapID);
    }
    sbQuery.append(" AND t906c.c906c_void_fl IS NULL ");
    sbQuery.append(" AND T101.C101_PARTY_ID = T1019.C101_PARTY_ID(+) ");
    sbQuery.append(" AND T1019.C901_PARTY_MAPPING_TYPE(+) = 105400 ");/*PMT-26730-system manager Module Changes*/
    if (!strCompanyId.equals("0")) {
    sbQuery.append(" AND  T1019.C1900_COMPANY_ID ="+strCompanyId);
    }
    sbQuery.append(" ORDER BY t101.C101_USER_F_NAME ");
    log.debug("Query for getting USer List " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  } // End of getUserList

  /**
   * fetchUserADStatusList This method will return all users records from LDAP with matching
   * criteria
   * 
   * @param hmParam HashMap
   * @return List
   */
  public List fetchUserADStatusList(HashMap hmParam) throws AppError {
    List alUsers = new ArrayList();
    String strUserName = (String) hmParam.get("ENCUSERNAME");
    String strPassword = (String) hmParam.get("ENCPASSWORD");
    String strSearchUser = (String) hmParam.get("SEARCHUSER");
    try {

      strSearchUser = strSearchUser + "*";
      log.debug("Encrypted Username is:" + strUserName);
      log.debug("Encrypted Password is:" + strPassword);

      GmLDAPManager gmLDAPManager = new GmLDAPManager(strUserName, strPassword);
      alUsers = gmLDAPManager.getUsers(strSearchUser);
    } catch (Exception e) {
      throw new AppError(e);
    }

    return alUsers;
  }

  /**
   * updateUserADStatusList This method will update user AD status and will send notification email.
   * 
   * @param hmParam HashMap
   * @return hmReturn HashMap
   */
  public HashMap updateUserADStatusList(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    String strUserName = GmCommonClass.parseNull((String) hmParam.get("ENCUSERNAME"));
    String strPassword = GmCommonClass.parseNull((String) hmParam.get("ENCPASSWORD"));
    String strInput = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strLoginUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));

    log.debug("Encrypted Username is:" + strUserName);
    log.debug("Encrypted Password is:" + strPassword);

    GmLDAPManager gmLDAPManager = new GmLDAPManager(strUserName, strPassword);

    String strUserList[] = strInput.split("\\|");
    String strEnableUsers = "";
    String strDisableUsers = "";

    try {
      for (int i = 0; i < strUserList.length; i++) {

        String strUserIdStatus[] = strUserList[i].split("\\^");
        log.debug("Before Enable/Disable User:" + strUserIdStatus[0]);

        if (strUserIdStatus[1].equals("91390")) {
          gmLDAPManager.enableUser(strUserIdStatus[0]);
          strEnableUsers = strEnableUsers + strUserIdStatus[0] + ",";
        } else if (strUserIdStatus[1].equals("91391")) {
          gmLDAPManager.disableUser(strUserIdStatus[0]);
          strDisableUsers = strDisableUsers + strUserIdStatus[0] + ",";
        }
        log.debug("After Enable/Disable User:" + strUserIdStatus[0]);
      }
      if (strEnableUsers.length() > 0) {
        strEnableUsers = strEnableUsers.substring(0, strEnableUsers.length() - 1);
      }
      if (strDisableUsers.length() > 0) {
        strDisableUsers = strDisableUsers.substring(0, strDisableUsers.length() - 1);
      }


      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      gmDBManager.setPrepareString("gm_pkg_it_user.gm_update_user_ad_log", 2);
      gmDBManager.setString(1, strInput);
      gmDBManager.setString(2, strLoginUserId);
      gmDBManager.execute();
      gmDBManager.commit();


      gmDBManager.setPrepareString("gm_pkg_it_user.gm_get_ad_emails_recipients", 3);
      gmDBManager.setString(1, strInput);
      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
      gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
      gmDBManager.execute();

      ArrayList alADGroupMails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
      ArrayList alBOMails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

      GmEmailProperties gmEmailProperties = getEmailProperties("GmADStatusUpdate");
      String to = "";

      for (int i = 0; i < alADGroupMails.size(); i++) {
        HashMap hmTemp = (HashMap) alADGroupMails.get(i);
        to = to + hmTemp.get("USEREMAIL") + ",";
      }
      for (int i = 0; i < alBOMails.size(); i++) {
        HashMap hmTemp = (HashMap) alBOMails.get(i);
        to = to + hmTemp.get("BOMAIL") + ",";
      }
      if (to.length() > 0)
        to = to.substring(0, to.length() - 1);

      log.debug("Email to:" + to);

      gmEmailProperties.setRecipients(to);

      String strSuccessMessage = "";

      if (strEnableUsers.length() > 0)
        strSuccessMessage =
            strSuccessMessage + "Following user are enabled successfully:" + strEnableUsers;
      if (strDisableUsers.length() > 0)
        strSuccessMessage =
            strSuccessMessage + "\nFollowing user are disabled successfully:" + strDisableUsers;

      gmEmailProperties.setMessage(strSuccessMessage);

      GmCommonClass.sendMail(gmEmailProperties);


      hmReturn.put("ENABLEDUSERS", strEnableUsers);
      hmReturn.put("DISABLEDUSERS", strDisableUsers);

    } catch (Exception e) {
      log.debug("Error:" + e.getMessage());
      e.printStackTrace();
      throw new AppError(e);
    }
    return hmReturn;
  }

  /**
   * fetchADAccessUserList() This method retrieves all users who have access permission for AD
   * status updaet
   * 
   * @return ArrayList - user records
   */
  public ArrayList fetchADAccessUserList() throws AppError {

    ArrayList alUserList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_ad_access_user_list", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alUserList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alUserList;
  }

  /**
   * fetchUserADStatusReportList() This method retrieves all users status for a given criteria
   * 
   * @return ArrayList - user records
   */
  public ArrayList fetchUserADStatusReportList(HashMap hmParam) throws AppError {

    ArrayList alUserList = new ArrayList();
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("SELECTEDUSER"));
    String strStatusId = GmCommonClass.parseNull((String) hmParam.get("SELECTEDSTATUS"));

    if (strUserId != null && (strUserId.equalsIgnoreCase("0") || strUserId.equalsIgnoreCase(""))) {
      strUserId = null;
    }
    if (strStatusId != null
        && (strStatusId.equalsIgnoreCase("0") || strStatusId.equalsIgnoreCase(""))) {
      strStatusId = null;
    }

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_ad_status_list", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.setString(1, strUserId);
    gmDBManager.setString(2, strStatusId);

    gmDBManager.execute();
    alUserList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alUserList;
  }


  /**
   * getEmailProperties() This method retrieves properties from EmailTemplate.Properties and return
   * GmEmailProperties
   * 
   * @return GmEmailProperties
   */
  public GmEmailProperties getEmailProperties(String strTemplate) {
    GmEmailProperties emailProps = new GmEmailProperties();
    emailProps
        .setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
    emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.SUBJECT));
    emailProps.setMessage(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.MESSAGE));
    emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.MIME_TYPE));
    emailProps.setCc(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.CC));
    return emailProps;
  }


  /**
   * updateNode() This method add or update node in navigation tree
   * 
   * @return ArrayList - user records
   */
  public void updateNode(HashMap hmParam) throws AppError {


    String strNodeId = GmCommonClass.parseNull((String) hmParam.get("NODEID"));
    String strNodeParentId = GmCommonClass.parseNull((String) hmParam.get("NODEPARENTID"));
    String strSelectedGroupId = GmCommonClass.parseNull((String) hmParam.get("SELECTEDGROUP"));
    String strLoginUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    log.debug("strNodeId : " + strNodeId);
    log.debug("strNodeParentId : " + strNodeParentId);

    if (strSelectedGroupId.equalsIgnoreCase("0") || strSelectedGroupId.equalsIgnoreCase("")) {
      strSelectedGroupId = null;
    }
    if (strNodeId.equalsIgnoreCase("-1") || strNodeId.equalsIgnoreCase("")) {
      strNodeId = null;
    }
    if (strNodeParentId.equalsIgnoreCase("-1") || strNodeParentId.equalsIgnoreCase("")) {
      log.debug("Inside If strNodeParentId : " + strNodeParentId);
      strNodeParentId = null;
    }

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_update_user_navigation_link", 4);

    gmDBManager.setString(1, strNodeId);
    gmDBManager.setString(2, strNodeParentId);
    gmDBManager.setString(3, strSelectedGroupId);
    gmDBManager.setString(4, strLoginUserId);

    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * fetchUserGroups() : This method is used to fetch the groups associated for the logged in user.
   * 
   * @return ArrayList - user records
   */
  public ArrayList fetchUserGroups(HashMap hmParam) throws AppError {

    ArrayList alUserGroupList = new ArrayList();
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    log.debug("hmParam : " + hmParam);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_user_groups", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strUserId);
    gmDBManager.execute();
    alUserGroupList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    log.debug("alUserGroupList : " + alUserGroupList);

    HashMap hmDefaultGroup = new HashMap();
    hmDefaultGroup.put("GROUP_ID", strDeptId);
    hmDefaultGroup.put("GROUP_NM", "Default");

    alUserGroupList.add(0, hmDefaultGroup);

    gmDBManager.close();
    return alUserGroupList;
  }

  /**
   * fetchUserNavigationLink() : This method is used to fetch the tree of links for the given user /
   * for the given dept.
   * 
   * @return ArrayList - user records
   */
  public ArrayList fetchUserNavigationLink(HashMap hmParam) throws AppError {

    ArrayList alUserNavigationList = new ArrayList();
    String strSelectedGroupId = GmCommonClass.parseNull((String) hmParam.get("USERGROUP"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    log.debug("strSelectedGroupId : " + strSelectedGroupId + " strUserId : " + strUserId);
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setPrepareString("gm_pkg_it_user.gm_fetch_user_navigation_link", 3);
    gmCacheManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmCacheManager.setString(1, strSelectedGroupId);
    gmCacheManager.setString(2, strUserId);
    gmCacheManager.setKey("fetchUserNavigationLink:" + strSelectedGroupId);
    gmCacheManager.execute();
    alUserNavigationList = gmCacheManager.returnArrayList((ResultSet) gmCacheManager.getObject(3));
    log.debug("alUserNavigationList : " + alUserNavigationList);
    gmCacheManager.close();
    return alUserNavigationList;
  }

  /**
   * fetchModuleGroups() : Used to fetch the All the groups created for the left menu
   * 
   * @return ArrayList - user records
   */
  public ArrayList fetchModuleGroups() throws AppError {

    ArrayList alModuleGroupList = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fetch_module_groups", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alModuleGroupList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alModuleGroupList;
  }

  public ArrayList fetchUserAccess(String strUserId) throws AppError {

    ArrayList alUserAccessList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_user_task_access", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strUserId);
    gmDBManager.execute();
    alUserAccessList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alUserAccessList;
  }



  /**
   * deleteNode() This method delete node in navigation tree
   * 
   * @return ArrayList - user records
   */
  public void deleteNode(HashMap hmParam) throws AppError {

    String strNodeId = GmCommonClass.parseNull((String) hmParam.get("NODEID"));
    String strSelectedGroupId = GmCommonClass.parseNull((String) hmParam.get("SELECTEDGROUP"));
    String strLoginUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    log.debug("strNodeId : " + strNodeId);

    if (strSelectedGroupId.equalsIgnoreCase("0") || strSelectedGroupId.equalsIgnoreCase("")) {
      strSelectedGroupId = null;
    }

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_delete_navigation_link", 3);

    gmDBManager.setString(1, strNodeId);
    gmDBManager.setString(2, strSelectedGroupId);
    gmDBManager.setString(3, strLoginUserId);

    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * fetchMenuList() : Fetch the Menu list
   * 
   * @return ArrayList - user records
   */
  public HashMap fetchMenuList() throws AppError {

    HashMap hmReturn = new HashMap();

    ArrayList alMenuFolderList = new ArrayList();
    ArrayList alMenuFileList = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_menu_names", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alMenuFolderList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    alMenuFileList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

    hmReturn.put("FOLDERLIST", alMenuFolderList);
    hmReturn.put("FILELIST", alMenuFileList);
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * Method : fetchUserDetails() : To fetch user details with ID. Param -- user id through HashMap
   * 
   * @return HashMap - user records
   */
  public HashMap fetchUserdetail(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    HashMap hmUserDetails = new HashMap();
    String strUserLoginNm = GmCommonClass.parseNull((String) hmParam.get("USERLOGINNAME"));
    String strUserId = GmCommonClass.parseZero((String) hmParam.get("HUSERID"));
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_user_detail", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strUserLoginNm.toLowerCase());
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * saveUser - This method will be used to save the edit user.
   * 
   * @param HashMap
   * @exception AppError
   */
  public HashMap saveUser(HashMap hmParam) throws AppError {
    log.debug("hmParam:::: " + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strUserIdFromDB = "";
    String strSalesRepID = "";
    HashMap hmReturn = new HashMap();
    //  new username text field is added, so get username value from USERLOGINNAME field
    String strUsrLoginName = GmCommonClass.parseNull((String) hmParam.get("USERLOGINNAME"));
    String strFirstName = GmCommonClass.parseNull((String) hmParam.get("FNAME"));
    String strLastName = GmCommonClass.parseNull((String) hmParam.get("LNAME"));
    String strPassword = GmCommonClass.parseNull((String) hmParam.get("HPASSWORD"));
    String strEmailId = GmCommonClass.parseNull((String) hmParam.get("EMAILID"));
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPARTMENTID"));
    String strShortName = GmCommonClass.parseNull((String) hmParam.get("SHORTNAME"));
    String strAccessLevel = GmCommonClass.parseNull((String) hmParam.get("ACCESSLEVEL"));
    String strWorkPhone = GmCommonClass.parseNull((String) hmParam.get("WORKPHONE"));
    String strTitle = GmCommonClass.parseNull((String) hmParam.get("TITLE"));
    String strLoginUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("HUSERID"));
    String strUserType = GmCommonClass.parseNull((String) hmParam.get("USERTYPE"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strApplnDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strExternalAccFl = GmCommonClass.getCheckBoxValue((String) hmParam.get("EXTERNALACCFL"));
    String strLdapId = GmCommonClass.parseNull((String) hmParam.get("LDAPID"));
    String strCompID= GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
    String strLoginPin = GmCommonClass.parseNull((String) hmParam.get("LOCKPIN"));//--PMT-29459 Enable User PIN upon new user Creation
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_save_user", 19);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(17, java.sql.Types.CHAR);
    gmDBManager.setString(1, strUserId);
    gmDBManager.setString(2, strUsrLoginName);
    gmDBManager.setString(3, strPassword);
    gmDBManager.setString(4, strFirstName);
    gmDBManager.setString(5, strLastName);
    gmDBManager.setString(6, strShortName);
    gmDBManager.setString(7, strAccessLevel);
    gmDBManager.setString(8, strEmailId);
    gmDBManager.setString(9, strDeptId);
    gmDBManager.setString(10, strWorkPhone);
    gmDBManager.setString(11, strTitle);
    gmDBManager.setString(12, strLoginUserId);
    gmDBManager.setString(13, strUserType);
    gmDBManager.setString(14, strApplnDateFmt);
    gmDBManager.setString(15, strExternalAccFl);
    gmDBManager.setString(16, strLdapId);
    gmDBManager.setString(18, strCompID);
    gmDBManager.setString(19, strLoginPin);
    gmDBManager.execute();
    strUserIdFromDB = GmCommonClass.parseNull(gmDBManager.getString(1));
    strSalesRepID = GmCommonClass.parseNull(gmDBManager.getString(17));
    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strUserIdFromDB, strLogReason, strLoginUserId, "1252");
    }
    gmDBManager.commit();
    hmReturn.put("USERID", strUserIdFromDB);
    hmReturn.put("SALESREPID", strSalesRepID);
    // PC-446: To comment the redis refresh code.
    // Now every time update (user) we are remove the keys. This is handle in action class.
    
    //GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    //gmCacheManager.removePatternKeys("REP_LIST");
    
    return hmReturn;
  }

  public void addFavorites(String strGrpId, String strFunId, String strUserId, String strFunName)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_create_favorites", 4);
    gmDBManager.setString(1, strGrpId);
    gmDBManager.setString(2, strFunId);
    gmDBManager.setString(3, strFunName);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public void removeFavorites(String strGrpId, String strFunId, String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_remove_favorites", 3);
    gmDBManager.setString(1, strGrpId);
    gmDBManager.setString(2, strFunId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public String fetchUserDet(String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPass = "";
    gmDBManager.setPrepareString("gm_pkg_it_grp_user_map.gm_fetch_user_det", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strUserId);
    gmDBManager.execute();
    strPass = gmDBManager.getString(2);
    gmDBManager.close();
    return strPass;
  }

  /**
   * Method : reactivateUser() : To reactivate user
   * @param : userid , login user id
   * @return String - updated username
   */
  public String reactivateUser(String strUserId, String strLoginUserId) {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_user_rollback", 3);
    gmDBManager.setString(1, strUserId);
    gmDBManager.setString(2, strLoginUserId);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strUserLoginName = gmDBManager.getString(3);
    gmDBManager.commit();
    return strUserLoginName;
  }

 /**
   * Method : validateUserName() : To reactivate user
   * @param : user login name
   * @return int - username count
   */
  public int validateUserName(String strLoginName) {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_validate_user_name", 2);
    gmDBManager.setString(1, strLoginName);
    gmDBManager.registerOutParameter(2, OracleTypes.INTEGER);
    gmDBManager.execute();
    int userCount = gmDBManager.getInt(2);
    gmDBManager.close();
    return userCount;
  }
  
  /**
   * Method : saveLinkPath: To save link path to favorites tolltip
   * @param : String strGrpId, String strFunId, String strUserId
   * @return 
   */
  public void saveLinkPath(String strGrpId, String strFunId, String strUserId) {
	  log.debug("strGrpId==>"+strGrpId+" strFunId==>"+strFunId+" strUserId==>"+strUserId);
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_sav_leftlink.gm_pkg_sav_leftlink_path", 3);
	    gmDBManager.setString(1, strGrpId);
	    gmDBManager.setString(2, strFunId);
	    gmDBManager.setString(3, strUserId);
	    gmDBManager.execute();
	    gmDBManager.commit();
  }
  
  /**copyAccessGrp:This Method  used to get security group details for existing user 
 				   and save table t1501 and also update c101_copy_access_user_id column
 * @param strUserName
 * @param strCopyUserNm
 * @param strUserId
 * @throws AppError
 */
public void copyAccessGrp(String strUserName, String strCopyUserNm, String strUserId) throws AppError {
		log.debug("strUserName==>" + strUserName + " strCopyUserNm==>"+ strCopyUserNm + " strUserId==>" + strUserId);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_it_user.gm_copy_access_grp", 3);
		gmDBManager.setString(1, strUserName);
		gmDBManager.setString(2, strCopyUserNm);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}
}// end of class GmUserMgmtBean
