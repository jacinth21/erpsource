package com.globus.logon.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmUserBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  GmCommonClass gmCommon = new GmCommonClass();


  public GmUserBean(GmDataStoreVO gmDataStore) {
    // TODO Auto-generated constructor stub
    super(gmDataStore);
  }

  public GmUserBean() {
    // TODO Auto-generated constructor stub
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public HashMap createUser(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = (GmDBManager) hmParam.get("DBMANAGEROBJECT");
    log.debug(" details for creating user " + hmParam);
    HashMap hmUserInfo = new HashMap();

    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strShName = GmCommonClass.parseNull((String) hmParam.get("SHNAME"));
    String strFName = GmCommonClass.parseNull((String) hmParam.get("FIRSTNAME"));
    String strLName = GmCommonClass.parseNull((String) hmParam.get("LASTNAME"));
    String strEmailId = GmCommonClass.parseNull((String) hmParam.get("EMAILADDRESS"));
    String strDeptId = GmCommonClass.parseNull((String) hmParam.get("DEPTID"));
    String strAccessLevelId = GmCommonClass.parseNull((String) hmParam.get("ACCESSLEVELID"));
    String strCreatedBy = GmCommonClass.parseNull((String) hmParam.get("CREATEDBY"));
    String strUserStatus = GmCommonClass.parseNull((String) hmParam.get("USERSTATUS"));
    String strUserType = GmCommonClass.parseNull((String) hmParam.get("USERTYPE"));
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strPartyType = GmCommonClass.parseNull((String) hmParam.get("PARTYTYPE"));
    String strPartyFlag = GmCommonClass.parseNull((String) hmParam.get("PARTYFLAG"));
    String strPartyMiddleInitial =
        GmCommonClass.parseNull((String) hmParam.get("PARTYMIDDLEINITIAL"));

    gmDBManager.setPrepareString("gm_pkg_it_login.gm_save_user", 16);
    gmDBManager.registerOutParameter(15, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(16, java.sql.Types.CHAR);

    gmDBManager.setString(1, strUserId);
    gmDBManager.setString(2, strShName);
    gmDBManager.setString(3, strFName);
    gmDBManager.setString(4, strLName);
    gmDBManager.setString(5, strEmailId);
    gmDBManager.setString(6, strDeptId);
    gmDBManager.setString(7, strAccessLevelId);
    gmDBManager.setString(8, strCreatedBy);
    gmDBManager.setString(9, strUserStatus);
    gmDBManager.setString(10, strUserType);
    gmDBManager.setString(11, strPartyId);
    gmDBManager.setString(12, strPartyType);
    gmDBManager.setString(13, strPartyFlag);
    gmDBManager.setString(14, strPartyMiddleInitial);

    gmDBManager.execute();
    strPartyId = gmDBManager.getString(15);
    strUserId = gmDBManager.getString(16);
    // gmDBManager.commit(); This will be closed in GmLogonBean
    log.debug(" created new user with id " + strUserId);
    hmUserInfo.put("USERID", strUserId);
    hmUserInfo.put("PARTYID", strPartyId);

    return hmUserInfo;
  }

  public String createSalesRep(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = (GmDBManager) hmParam.get("DBMANAGEROBJECT");
    log.debug(" details for creating SALES REP ---  " + hmParam);

    String strSalesRepId = "";

    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strFName = GmCommonClass.parseNull((String) hmParam.get("FIRSTNAME"));
    String strLName = GmCommonClass.parseNull((String) hmParam.get("LASTNAME"));
    // For PMT-12136 Master data changes for Japan , added new field Sales Rep Name (En).
    String strRepNameEn = strFName + " " + strLName;
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strDistributorId = GmCommonClass.parseNull((String) hmParam.get("DISTID"));
    String strSalesRepCategory = GmCommonClass.parseZero((String) hmParam.get("REPCATEGORY"));

    gmDBManager.setPrepareString("gm_save_salesrep_table", 16);
    gmDBManager.registerOutParameter(15, java.sql.Types.CHAR);

    gmDBManager.setString(1, strFName);
    gmDBManager.setString(2, strLName);
    gmDBManager.setString(3, strRepNameEn);
    gmDBManager.setString(4, strDistributorId);
    gmDBManager.setString(5, strSalesRepCategory);
    gmDBManager.setString(6, strSalesRepId);
    gmDBManager.setString(7, strUserId);
    gmDBManager.setString(8, "");
    gmDBManager.setString(9, "Y");
    gmDBManager.setString(10, strPartyId);
    gmDBManager.setString(11, "");
    gmDBManager.setString(12, "");
    gmDBManager.setString(13, "");
    gmDBManager.setString(14, "");
    gmDBManager.setString(15, "");

    gmDBManager.execute();
    strSalesRepId = gmDBManager.getString(16);
    // gmDBManager.commit(); This will be closed in GmLogonBean
    log.debug(" created new user with id " + strSalesRepId);

    hmParam.put("MAILBODY", " Please setup his / her Territory, Category and other Information ");
    hmParam.put("MAILRULEID", "NEWREPNOTIFICATION");
    notifyUserCreation(hmParam);
    return strSalesRepId;
  }

  public void createUserLogin(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = (GmDBManager) hmParam.get("DBMANAGEROBJECT");
    log.debug(" details for creating user LOGIN details " + hmParam);

    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strUserName = GmCommonClass.parseNull((String) hmParam.get("USERNAME"));
    String strPassword = GmCommonClass.parseNull((String) hmParam.get("PASSWORD"));
    String strAuthType = GmCommonClass.parseNull((String) hmParam.get("AUTHTYPE"));

    gmDBManager.setPrepareString("gm_pkg_it_login.gm_save_user_login", 4);

    gmDBManager.setString(1, strUserId);
    gmDBManager.setString(2, strUserName);
    gmDBManager.setString(3, strPassword);
    gmDBManager.setString(4, strAuthType);

    gmDBManager.execute();
    // gmDBManager.commit(); This will be closed in GmLogonBean

  }

  public void notifyUserCreation(HashMap hmParam) throws AppError {
    HashMap hmEmailDetails = new HashMap();
    String strUserName = GmCommonClass.parseNull((String) hmParam.get("USERNAME"));
    String strMessage = "New User " + strUserName + " created";
    String strMailBody = GmCommonClass.parseNull((String) hmParam.get("MAILBODY"));
    String MailRuleId = GmCommonClass.parseNull((String) hmParam.get("MAILRULEID"));
    String strToMail = GmCommonClass.getRuleValue(MailRuleId, "EMAIL");
    String[] strRecipients = strToMail.split(";");

    hmEmailDetails.put("strFrom", "notification@globusmedical.com");
    hmEmailDetails.put("strTo", strRecipients);
    hmEmailDetails.put("strSubject", strMessage);
    hmEmailDetails.put("strMessageDesc", strMailBody);
    log.debug(" Sending email for new user " + hmEmailDetails);
    try {
      GmCommonClass.sendMail(hmEmailDetails);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
  }

  /**
   * fetchUserList() This method retrieves all active users from database to configure their login
   * lock *
   * 
   * @return ArrayList - all active user records
   */
  public ArrayList fetchUserList() throws AppError {

    ArrayList alUserList = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_user_list", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alUserList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alUserList;
  }

  /**
   * updateUserLockList() This method update logi lock status for selected users in database
   * 
   * @param hmParam - HashMap
   */
  public void updateUserLockList(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strUserLocks = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_update_user_lock_list", 2);
    gmDBManager.setString(1, strUserLocks);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public void saveInActivateDBUser(String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_sav_inactive_user", 1);
    gmDBManager.setString(1, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public void saveConvertToDBUser(String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_sav_convert_user", 1);
    gmDBManager.setString(1, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  public List fetchActiveUsers(String strLdapID) throws AppError {
    ArrayList alUserList = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_active_users", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLdapID);
    gmDBManager.execute();
    alUserList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alUserList;
  }

}
