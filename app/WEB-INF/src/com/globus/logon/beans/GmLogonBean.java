/*
 * \ * Module: GmLogonBean.java Author: Dhinakaran James Project: Globus Medical App - SpineIT
 * Date-Written: 11 Mar 2004 Security: Unclassified Description: This bean contains methods that are
 * used for the logon process.
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.logon.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ResourceBundle;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.springframework.ldap.AuthenticationException;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.beans.GmSecurityBean;
import com.globus.common.db.GmDBManager;
import com.globus.logon.auth.GmLDAPManager;
import com.globus.sales.beans.GmSalesCompBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.logon.GmLogonAccessVO;

public class GmLogonBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  GmCommonClass gmCommon = new GmCommonClass();


  public static ResourceBundle rbAuthentication = ResourceBundle.getBundle("Authentication");
  // public static ResourceBundle rbBOInterfaceApp = ResourceBundle.getBundle("BOInterfaceApp");
  public static final String PROD_SERVER_LIST = "gmivmprod;gmiasrv03";
  // private boolean blDoesUserExistInAD = false;
  private String strAuthenticationMode = "secWinAD";

  public GmLogonBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
    strAuthenticationMode = rbAuthentication.getString("MODE");
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmLogonBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
    strAuthenticationMode = rbAuthentication.getString("MODE");
  }

  public boolean doesNeedAuthentication(String strServerName) {

    boolean blDoesNeedAuth = true;

    String strBlackList = rbAuthentication.getString("MODE");
    log.debug(" Black List " + strBlackList + " Server Name " + strServerName);
    String pattern = "^.*" + strServerName + ".*";

    if (PROD_SERVER_LIST.matches(pattern)) {
      return true;
    }

    if (strBlackList.equalsIgnoreCase("NOAUTH")) {
      blDoesNeedAuth = false;
    }

    return blDoesNeedAuth;
  }

  public HashMap fetchUserDetailsAfterAuthentication(HashMap hmParam) throws AppError {
    HashMap hmUserDetails = new HashMap();
    String strUserTrackingMessage = "";
    String strUserId = "";
    String strUserName = GmCommonClass.parseNull((String) hmParam.get("USERNM"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    log.debug("fetchUserDetailsAfterAuthentication hmParam --"+hmParam);
    try {
      hmUserDetails = getUserInfo(strUserName);
      strUserId = (String) hmUserDetails.get("USERID");
      if (!strOpt.equals("SwitchUser")) {
        strUserTrackingMessage = userTracking(hmParam, strUserId, "logon");
      }
      log.debug(" User tracking msg " + strUserTrackingMessage);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
    return hmUserDetails;
  }

  public boolean authenticate(HashMap hmParam) throws AppError {

    boolean blIsAuthenticated = false;
    String strUserName = GmCommonClass.parseNull((String) hmParam.get("USERNM"));

    if (doesUserNeedADAuthN(strUserName)) {
      blIsAuthenticated = authenticateUserInAD(hmParam);
      // authenticateUserInEnterpriseDatabase(hmParam);
    } else {
      blIsAuthenticated = authenticateUserInEnterpriseDatabase(hmParam);
      log.debug("blIsAuthenticated--"+blIsAuthenticated);
    }    return blIsAuthenticated;
  }

  public boolean doesUserNeedADAuthN(String strUserName) throws AppError {
    // GmLDAPManager gmLDAPManager = new GmLDAPManager();
    boolean blDoesUserNeedADAuthn = false;
    boolean blIsUserMarkedForADAuthentication = true; // default is AD Auth

    blIsUserMarkedForADAuthentication = isUserMarkedForADAuthentication(strUserName);
    // blDoesUserExistInAD = gmLDAPManager.doesUserExist(strUserName);
    log.debug(" Is User Marked for AD Auth " + blIsUserMarkedForADAuthentication);

    if (blIsUserMarkedForADAuthentication
        && !strAuthenticationMode.equalsIgnoreCase("secEnterprise")) {
      blDoesUserNeedADAuthn = true;
    }
    log.debug(" blDoesUserNeedADAuthn " + blDoesUserNeedADAuthn);
    return blDoesUserNeedADAuthn;
  }

  public boolean authenticateUserInAD(HashMap hmParam) throws AppError {
    String strLdapId = GmCommonClass.parseNull((String) hmParam.get("LDAPID"));
    GmLDAPManager gmLDAPManager = new GmLDAPManager(strLdapId);
    GmSecurityBean gmSecurityBean = new GmSecurityBean(getGmDataStoreVO());
    GmUserBean gmUserBean = new GmUserBean(getGmDataStoreVO());
    HashMap hmUserDetails = new HashMap();
    HashMap hmADUserInfo = new HashMap();

    boolean blAuthentication = false;
    boolean blDoesUserExistInPortal = false;
    boolean blIsNewUser = false;
    boolean blIsUserActiveInPortal = false;

    String strUserName = GmCommonClass.parseNull((String) hmParam.get("USERNM"));
    String strPassword = GmCommonClass.parseNull((String) hmParam.get("PASSWD"));

    try {
      blAuthentication = gmLDAPManager.authenticateUser(strUserName, strPassword);
    } catch (AuthenticationException authExp) {
      checkAndThrowError(authExp);
    } catch (Exception exp) {
      exp.printStackTrace();
    }

    if (!blAuthentication) {
      gmSecurityBean.saveForgotUser(strUserName);
      throw new AppError(AppError.APPLICATION, "1002");
    }

    try {
      blDoesUserExistInPortal = doesUserExistInPortal(strUserName);
      log.debug(" User exists in portal ? " + blDoesUserExistInPortal);
      hmUserDetails.put("USERNAME", strUserName);
      // As users will be created using "New User Link" , this code need not be there.

      /*
       * if (!blDoesUserExistInPortal) { hmADUserInfo = gmLDAPManager.getADUserDetails(strUserName);
       * hmADUserInfo.put("USERNAME", strUserName); log.debug(" hmADUserInfo " + hmADUserInfo);
       * createUser(hmADUserInfo); }
       * 
       * blIsNewUser = isNewUser(strUserName); if (blIsNewUser) { hmUserDetails.put("MAILBODY",
       * " Please setup his / her department and access level. "); hmUserDetails.put("MAILRULEID",
       * "NEWUSERNOTIFICATION"); gmUserBean.notifyUserCreation(hmUserDetails); }
       */
      blIsUserActiveInPortal = isUserActiveInPortal(strUserName);
      if (!blIsUserActiveInPortal) {
        throw new AppError(AppError.APPLICATION, "1013");
      }
    } catch (Exception exp) {
      throw new AppError(exp);
    }

    return blAuthentication;
  }

  public HashMap createUser(HashMap hmADUserInfo) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmNewUserInfo = new HashMap();
    log.debug(" AD User Info is " + hmADUserInfo);
    String strUserId = "";
    String strUserType = "300";
    String strDepartmentId = "";
    String strPartyId = "";
    String strSalesRepCategory = "4021";
    String PATTERN_EXTERNAL_USER = "^.*OU=External.*";
    String PATTERN_AREA_DIRECTOR = "^.*OU=Area_Director.*";

    // Default values
    String strPartyType = "7006";
    strDepartmentId = "2027"; // New User department
    // String strDepartmentChar = "NEW";
    String strAccessLevelId = "0";
    String strCreatedBy = "30301";
    String strPassword = "GOTOAD@745247@5";

    GmUserBean gmUserBean = new GmUserBean(getGmDataStoreVO());
    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());
    String strDistinguishedName =
        GmCommonClass.parseNull((String) hmADUserInfo.get("DISTINGUISHEDNAME"));

    if (strDistinguishedName.matches(PATTERN_EXTERNAL_USER)
        || strDistinguishedName.matches(PATTERN_AREA_DIRECTOR)) {
      strDepartmentId = "2005";
      strPartyType = "7005";
      if (strDistinguishedName.matches(PATTERN_AREA_DIRECTOR)) {
        strAccessLevelId = "3";
        strSalesRepCategory = "40028";
      } else {
        strAccessLevelId = "1";
        strUserType = "301";
      }
    }

    hmADUserInfo.put("USERSTATUS", "311");
    hmADUserInfo.put("USERTYPE", strUserType);
    hmADUserInfo.put("AUTHTYPE", "320");
    hmADUserInfo.put("DEPTID", strDepartmentId);
    // hmADUserInfo.put("DEPTCHARACTER", strDepartmentChar);
    hmADUserInfo.put("ACTIVEFLAG", "Y");
    hmADUserInfo.put("PARTYTYPE", strPartyType);
    hmADUserInfo.put("SHNAME", hmADUserInfo.get("FIRSTNAME"));
    hmADUserInfo.put("ACCESSLEVELID", strAccessLevelId);
    hmADUserInfo.put("CREATEDBY", strCreatedBy);
    hmADUserInfo.put("DBMANAGEROBJECT", gmDBManager);

    // strPartyId = gmPartyBean.saveParty(hmADUserInfo);

    hmNewUserInfo = gmUserBean.createUser(hmADUserInfo);
    log.debug(" New User Info " + hmNewUserInfo);
    hmADUserInfo.putAll(hmNewUserInfo);
    // hmADUserInfo.put("USERID", hmNewUserInfo.get("USERID"));
    hmADUserInfo.put("PASSWORD", strPassword);
    // hmADUserInfo.put("PARTYID", hmNewUserInfo.get("PARTYID")); // since Party Id should always be
    // User Id
    log.debug(" New User Info in ALL HashMap ---  " + hmADUserInfo);
    gmUserBean.createUserLogin(hmADUserInfo);

    if (strDepartmentId.equals("2005")) {
      hmADUserInfo.put("DISTID", "00");
      hmADUserInfo.put("REPCATEGORY", strSalesRepCategory);
      log.debug(" creating sales rep... " + hmADUserInfo);
      gmUserBean.createSalesRep(hmADUserInfo);
    }

    gmDBManager.commit(); // this makes all the user creation as one unit of transaction
    return hmADUserInfo;
  }

  /*
   * Checks if the User exists in portal database
   */
  public boolean isUserActiveInPortal(String strUserName) throws AppError {
    boolean blIsUserActiveInPortal = false;
    String strResult = "0";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_it_login.gm_is_useractive", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);
    gmDBManager.setString(2, strUserName);
    gmDBManager.execute();
    strResult = gmDBManager.getString(1);
    gmDBManager.close();

    blIsUserActiveInPortal = getBooleanStatus(strResult);
    return blIsUserActiveInPortal;
  }

  /*
   * Checks if the User department is setup in portal database
   */
  public boolean isNewUser(String strUserName) throws AppError {
    boolean blIsNewUser = false;
    String strResult = "0";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_it_login.gm_is_NewUser", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);
    gmDBManager.setString(2, strUserName);
    gmDBManager.execute();
    strResult = gmDBManager.getString(1);
    gmDBManager.close();

    blIsNewUser = getBooleanStatus(strResult);

    return blIsNewUser;
  }

  /*
   * Checks if the User exists in portal database
   */
  public boolean doesUserExistInPortal(String strUserName) throws AppError {
    boolean blDoesUserExistInPortal = false;
    String strResult = "0";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_it_login.gm_does_userExist", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);
    gmDBManager.setString(2, strUserName);
    gmDBManager.execute();
    strResult = gmDBManager.getString(1);
    gmDBManager.close();
    log.debug(" strResult " + strResult);

    blDoesUserExistInPortal = getBooleanStatus(strResult);
    return blDoesUserExistInPortal;
  }

  /*
   * Checks if the User exists in portal database
   */
  public boolean isUserMarkedForADAuthentication(String strUserName) throws AppError {
    boolean blIsUserMarkedForADAuthentication = false;
    String strResult = "0";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_it_login.gm_is_usr_forAD", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);
    gmDBManager.setString(2, strUserName);
    gmDBManager.execute();
    strResult = gmDBManager.getString(1);
    gmDBManager.close();
    log.debug(" strResult " + strResult);

    blIsUserMarkedForADAuthentication = getBooleanStatus(strResult);
    return blIsUserMarkedForADAuthentication;
  }

  private boolean getBooleanStatus(String strResult) {
    boolean blStatus = false;
    if (strResult == null)
      return blStatus;

    if (strResult.equals("1")) {
      blStatus = true;
    }

    return blStatus;
  }

  /*
   * Inspect the Exception from LDAP and throw corresponding error
   */
  private void checkAndThrowError(AuthenticationException authExp) throws AppError {
    String strErrorMessage = authExp.getExplanation();
    log.debug(" Error while logging in " + strErrorMessage);

    String INVALID_CREDENTIALS = "^.*data 52e.*";
    String ACCOUNT_DISABLED = "^.*data 533.*";
    String ACCOUNT_TEMPORARILY_LOCKED = "^.*data 775.*";
    String USER_NOT_FOUND = "^.*data 525.*";
    String USER_SHOULD_CHANGE_PASSWORD = "^.*data 773.*";

    if (strErrorMessage.matches(INVALID_CREDENTIALS)) {
      throw new AppError(AppError.APPLICATION, "1002");
    } else if (strErrorMessage.matches(ACCOUNT_DISABLED)) {
      throw new AppError(AppError.APPLICATION, "1005");
    } else if (strErrorMessage.matches(ACCOUNT_TEMPORARILY_LOCKED)) {
      throw new AppError(AppError.APPLICATION, "1015");
    } else if (strErrorMessage.matches(USER_NOT_FOUND)) {
      throw new AppError(AppError.APPLICATION, "1001");
    } else if (strErrorMessage.matches(USER_SHOULD_CHANGE_PASSWORD)) {
      throw new AppError(AppError.APPLICATION, "0006");
    }
  }

  public HashMap getUserInfo(String strUserName) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmSalesCompBean gmSalesCompBean = new GmSalesCompBean(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmReturn = new HashMap();
    ArrayList alRegionList = new ArrayList();

    sbQuery.setLength(0);
    sbQuery
        .append("SELECT userinfo.*, gm_pkg_op_plant_rpt.get_default_plant_for_comp(userinfo.COMPID) PLANTID, ");
    sbQuery
        .append(" NVL(get_rule_value(DEPTSEQ,'COMPGRP'),get_company_id(ACID,DEPTSEQ,USERID,USERINFO.COMPID )) companyid  ,get_rule_value_by_company(userinfo.COMPID,'MODULEACCESS',userinfo.COMPID) COMPMODULEACCESS,");
    sbQuery
        .append("get_company_timezone(userinfo.COMPID) CMPTIMEZONE,get_rule_value('DOUPLDCOUNT','FAILURECOUNT') pdffailurecnt ,get_company_locale(userinfo.COMPID) compcd , ");
    // PC-4014 Scorecard and Field Inventory Report full access (mschmitt) - get_rule_value
    sbQuery
    	.append(" get_company_dtfmt(userinfo.COMPID) CMPDATEFMT,get_comp_curr_symbol(userinfo.COMPID) defcurrncysymbol, get_rule_value(userinfo.USERID,'SALES_FILTER_FULL_AC') SALES_FILTER_ACCESS_FL FROM (");
    sbQuery
        .append(" SELECT T101.C101_USER_ID USERID , T101.C101_USER_SH_NAME SHNAME, T102.C102_LOGIN_USERNAME USERNM, ");
    sbQuery.append(" GET_CODE_NAME(C901_DEPT_ID) DEPTID, C101_ACCESS_LEVEL_ID ACID, ");
    sbQuery
        .append(" C901_DEPT_ID DEPTSEQ , GET_LAST_LOGIN_TS(T101.C101_USER_ID) LOGINTS , T101.C101_PARTY_ID PARTYID ");
    sbQuery
        .append(" , DECODE(T101.C101_DATE_FORMAT,NULL,GET_RULE_VALUE('DATEFMT','DATEFORMAT'),T101.C101_DATE_FORMAT) APPLDATEFMT");
    // JSDATEFMT DECODE is modified for BUG-3625. the existing DOCODE is wrong. so we are taking
    // T101.C101_DATE_FORMAT directly.
    // If T101.C101_DATE_FORMAT is NULL, then this JSDATEFMT is NULL then from LogOnServlet we have
    // %m/%d/%Y format.
    sbQuery.append(" , get_rule_value(T101.C101_DATE_FORMAT, 'DATEFORMAT') JSDATEFMT ");
    /*
     * Get Rule value is added to make the ALL option default to specific departments. Rest will be
     * Globus by default.
     */
    sbQuery.append(" , T102.C102_EXT_ACCESS EXTACCESS ");
    sbQuery
        .append(" , get_division_id(C101_ACCESS_LEVEL_ID,C901_DEPT_ID,T101.C101_USER_ID, gm_pkg_it_user.get_default_party_company(t101.c101_party_id)) divid ");
    sbQuery
        .append(" , get_rep_id_by_ass_id(T101.C101_USER_ID) REPID,C101_USER_F_NAME FNAME, C101_USER_L_NAME LNAME ");
    // sbQuery.append(" , GM_PKG_ALT_ALERTS.GET_UNREAD_ALERTS_CNT(T101.C101_USER_ID) ALERTCNT ");//To
    // get the count of Unread alerts

    // The COMPID is user default company. It will be replaced with user's company once all user
    // mapped with companys.
    sbQuery.append(" ,  gm_pkg_it_user.get_default_party_company(t101.c101_party_id) COMPID ");
    sbQuery.append(" ,  gm_pkg_sm_mapping.get_salesrep_division(t703.c703_sales_rep_id) repdiv ");
    // PC-4143: To get the user email id
    sbQuery.append(" , t101.c101_email_id emailid ");
    sbQuery.append(" FROM T101_USER T101, T102_USER_LOGIN T102, T703_SALES_REP T703 ");
    sbQuery.append(" WHERE T101.C101_USER_ID = T102.C101_USER_ID");
    sbQuery.append(" AND T101.C101_PARTY_ID = T703.C101_PARTY_ID(+) ");
    sbQuery.append(" AND T102.C102_LOGIN_USERNAME = '");
    sbQuery.append(strUserName);
    sbQuery.append("'");
    sbQuery.append(") userinfo");
    log.debug(" Query to get user details " + sbQuery.toString());

    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());

    // To set the default division in the more filter for the users other than the sales modules for
    // OUS countries.
    String strDeptId = GmCommonClass.parseNull((String) hmReturn.get("DEPTID"));
    String strPrimaryCmpyId = GmCommonClass.parseNull((String) hmReturn.get("COMPID"));

    // If the user primary company is not US and Dept is not in sales ,Providing the Override access
    // as AD or VP and also if they where mapped to multiple companies then setting the currency
    // Type as Base(105460). For e.g users like yrabie,aesser.
    if ((!strPrimaryCmpyId.equals("1000")) && (!strDeptId.equals("S"))) {

      fetchSalesOverrideInfo(hmReturn);

    }

    // If the user as the AD+ access i.e user were mapped to multiple Regions,Then setting the
    // currency Type as Base(105460). For e.g users like igriffiths.
    if ((!strPrimaryCmpyId.equals("1000")) && (strDeptId.equals("S"))) {

      alRegionList = gmSalesCompBean.getRegionByUserId(hmReturn);

      if (alRegionList.size() > 1) {
        hmReturn.put("MULTIREGIONMAP", "YES");
      }

    }


    log.debug(" User details final >>>>>>> " + hmReturn);
    gmDBManager.close();

    return hmReturn;
  }

  /**
   * fetchSalesOverrideInfo - This method will fetch the ovverride access level details for the
   * users other than sales module
   * 
   * @param HashMap
   * @return void
   */

  public void fetchSalesOverrideInfo(HashMap hmReturn) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    ArrayList alCompanyList = new ArrayList();

    String strPrimaryCmpyId = GmCommonClass.parseNull((String) hmReturn.get("COMPID"));
    String strOverrideAccessId = "";

    alCompanyList = gmPartyBean.fetchPartyCompany(hmReturn);


    if (alCompanyList.size() != 0) {

      Iterator itrHashMap = alCompanyList.iterator();

      try {
        while (itrHashMap.hasNext()) {
          HashMap hmMap = (HashMap) itrHashMap.next();
          Iterator itrKeys = hmMap.keySet().iterator();

          String strKey = (String) itrKeys.next();
          String strValue = String.valueOf(hmMap.get(strKey));

          if (strKey.equals("COMPANYID")) {

            strOverrideAccessId = strOverrideAccessId + strValue + ",";
          }

        }
      } catch (Exception ex) {
        ex.printStackTrace();
        throw new AppError(ex);
      }
    }

    if (strOverrideAccessId.length() != 0) {

      strOverrideAccessId = strOverrideAccessId.substring(0, strOverrideAccessId.length() - 1);
    }

    String strOverrideAccessTo = GmCommonClass.getRuleValue(strPrimaryCmpyId, "OVERRIDE_ACS_TO");
    String strOverrideAccessLvl = GmCommonClass.getRuleValue(strPrimaryCmpyId, "OVERRIDE_ACS_LVL");

    hmReturn.put("OVERRIDE_ACS_TO", strOverrideAccessTo);
    hmReturn.put("OVERRIDE_ACS_ID", strOverrideAccessId);
    hmReturn.put("OVERRIDE_ACS_LVL", strOverrideAccessLvl);

    if (alCompanyList.size() > 1) {
      hmReturn.put("MULTICOMPANYMAP", "YES");
    }
  }

  /**
   * validateLogon - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public boolean authenticateUserInEnterpriseDatabase(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    boolean blAuthenticate = false;
    String strId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strUsername = GmCommonClass.parseNull((String) hmParam.get("USERNM"));
    String strPassword = GmCommonClass.parseNull((String) hmParam.get("PASSWD"));
    String strPin = GmCommonClass.parseNull((String) hmParam.get("PIN"));
    String strUserPin = "";
    String strUserPass = "";
    String strUserStat = "";
    String strLoginLock = "";
    String strUserId = "";
    String strAuthType = "";
    StringBuffer sbQuery = new StringBuffer();;
    HashMap hmResult = new HashMap();

    sbQuery
        .append(" SELECT T102.C102_LOGIN_USERNAME USERNAME,  C102_USER_PASSWORD USERPASS , T102.C102_LOGIN_PIN USERPIN, T101.C901_USER_STATUS USERSTAT, T102.C102_LOGIN_LOCK_FL LOGINLOCKFL, T101.C101_USER_ID USERID, T102.C901_AUTH_TYPE AUTH_TYPE");
    sbQuery.append(" ,T102.c906c_ldap_master_id LDAPID ");
    sbQuery.append(" FROM T101_USER T101, T102_USER_LOGIN T102 ");
    sbQuery.append(" WHERE T101.C101_USER_ID = T102.C101_USER_ID ");
    if (!strUsername.equals("")) {
      sbQuery.append(" AND C102_LOGIN_USERNAME = '");
      sbQuery.append(strUsername);
      sbQuery.append("'");
    } else if (!strId.equals("")) {
      sbQuery.append(" AND T101.C101_USER_ID = '");
      sbQuery.append(strId);
      sbQuery.append("'");
    }

    log.debug("Query: " + sbQuery.toString());

    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());

    log.debug("hmResult: " + hmResult);

    if (hmResult.isEmpty()) {
      throw new AppError(AppError.APPLICATION, "1001");
    }
    strUserPass = (String) hmResult.get("USERPASS");
    strUserPin = (String) hmResult.get("USERPIN");
    strUserStat = (String) hmResult.get("USERSTAT");
    strLoginLock = (String) hmResult.get("LOGINLOCKFL");
    strUserId = (String) hmResult.get("USERID");
    strAuthType = GmCommonClass.parseNull((String) hmResult.get("AUTH_TYPE"));

    if (hmResult.size() == 0) {
      // Invalid username
      throw new AppError(AppError.APPLICATION, "1001");
    } else if (strAuthType.equals("")) {
      throw new AppError(AppError.APPLICATION, "1562");
    } else if (!strUserStat.equals("311")) {
      // User Account is not in active status
      throw new AppError(AppError.APPLICATION, "1005");
    } else if (strLoginLock != null && strLoginLock.equals("Y")) {
      // User Account is locked for login
      throw new AppError(AppError.APPLICATION, "1026");
    } else if (!strPassword.equals("") && !strUserPass.equals(strPassword)) {
      updateUserLoginAttempt(strUserId);
      // The password you supplied is incorrect
      throw new AppError(AppError.APPLICATION, "1002");
    } else if (!strPin.equals("") && !strPin.equals(strUserPin)) {
      updateUserLoginAttempt(strUserId);
      throw new AppError(AppError.APPLICATION, "1002.1");
    } else if (strPin.equals("") && strPassword.equals("")) {
      updateUserLoginAttempt(strUserId);
      throw new AppError(AppError.APPLICATION, "1002.1");
    } else {
      blAuthenticate = true;
      updateUserLock(strUserId, "N", strUserId);
    }
    hmParam.put("USERNM", GmCommonClass.parseNull((String) hmResult.get("USERNAME")));

    return blAuthenticate;
  } // End of validateLogon

  public String fetchPartyId(String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_login.gm_fch_partyid", 2);
    gmDBManager.registerOutParameter(2, java.sql.Types.CHAR);

    String strPartyId = null;
    gmDBManager.setString(1, strUserId);
    gmDBManager.execute();
    strPartyId = gmDBManager.getString(2);
    gmDBManager.close();
    return strPartyId;
  }

  /**
   * userTracking - This method
   * 
   * @param String strUserId
   * @return String
   * @exception AppError
   */
  public String userTracking(HashMap hmParam, String strUserId, String strAction) throws AppError {
    String strSessId = (String) hmParam.get("SESSID");
    String strClientIP = (String) hmParam.get("CLIENTIP");

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strMessage = null;

    gmDBManager.setPrepareString("GM_USER_TRACKING_PRC", 5);
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);

    gmDBManager.setInt(1, Integer.parseInt(strUserId));
    gmDBManager.setString(2, strAction);
    gmDBManager.setString(3, strSessId);
    gmDBManager.setString(4, strClientIP);

    gmDBManager.execute();
    strMessage = gmDBManager.getString(5);

    if (("1000").equals(strMessage)) {
      gmDBManager.close();
      throw new AppError(AppError.APPLICATION, "1014");
    }
    gmDBManager.commit();
    return strMessage;
  } // End of userTracking method

  /**
   * changePassword - This method
   * 
   * @param String strUserId
   * @return String
   * @exception AppError
   */
  public HashMap changePassword(String strUserId, String strOldPass, String strNewPass)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_UPDATE_PASSWORD", 4);
    gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);

    String strMessage = null;
    HashMap hmReturn = new HashMap();

    gmDBManager.setString(1, strUserId);
    gmDBManager.setString(2, strOldPass);
    gmDBManager.setString(3, strNewPass);

    gmDBManager.execute();
    strMessage = gmDBManager.getString(4);

    hmReturn.put("MSG", strMessage);
    gmDBManager.commit();
    return hmReturn;
  } // End of changePassword method

  public void sendPassword(String strUserId, String strEmail) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_login.gm_fch_password", 2);

    HashMap hmReturn = new HashMap();

    gmDBManager.setString(1, strUserId);
    gmDBManager.setString(2, strEmail);

    gmDBManager.execute();

    gmDBManager.commit();

  } // End of sendPassword method

  public String checkExpired(String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_login.gm_validate_passexp", 2);
    gmDBManager.registerOutParameter(2, java.sql.Types.CHAR);

    String strMessage = null;
    gmDBManager.setString(1, strUserId);
    gmDBManager.execute();
    strMessage = gmDBManager.getString(2);

    gmDBManager.close();
    return strMessage;
  } // End of changePassword method

  /**
   * getEmployeeList - This method will
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public ArrayList getEmployeeList() throws AppError {
    ArrayList alReturn = new ArrayList();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      // Added CompanyId for getting EmployeeList depending on company id drop down value.
      sbQuery
          .append(" SELECT	t101.C101_USER_F_NAME ||' '||t101.C101_USER_L_NAME NAME, t101.C101_USER_ID ID ,t101.C101_PARTY_ID  PARTYID ");
      sbQuery.append(" ,C101_USER_F_NAME ||' '||C101_USER_L_NAME CODENM, C101_USER_ID CODEID  ");
      sbQuery.append(" FROM T101_USER t101 ");
      sbQuery.append("  , T1019_PARTY_COMPANY_MAP t1019 ");
      sbQuery.append("WHERE t1019.C101_PARTY_ID = t101.C101_PARTY_ID");
      sbQuery.append(" AND  t1019.C1900_COMPANY_ID =" + getCompId());
      sbQuery.append(" ORDER BY C101_USER_F_NAME ");
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());


    } catch (Exception e) {
      GmLogError.log("Exception in GmLogonBean:getEmployeeList", "Exception is:" + e);
    }
    return alReturn;
  } // End of getEmployeeList

  /**
   * getEmployeeList - This method will
   * 
   * @param String User Code 300 Employee 301 Direct Sales Rep 302 Agent Sales Rep 303 Agent
   *        Principal 304 Temp Hire
   * @param String dept
   * @return HashMap
   * @exception AppError
   */
  public ArrayList getEmployeeList(HashMap hmMap) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strUsrCode = GmCommonClass.parseNull((String) hmMap.get("USERCODE"));
    String strDept = GmCommonClass.parseNull((String) hmMap.get("DEPT"));
    String strUserSts = GmCommonClass.parseNull((String) hmMap.get("STATUS"));
    // The following code added for Getting the Employee List based on the PLANT too
    String strEmpFilter = GmCommonClass.parseNull((String) hmMap.get("EMPFILTER"));
    String strEmployeeList = GmCommonClass.parseNull((String) hmMap.get("EMPLOYEELIST"));
    String strInactiveUser = GmCommonClass.parseNull((String) hmMap.get("INACTIVE_USER"));
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT	t101.C101_USER_F_NAME ||' '||t101.C101_USER_L_NAME NAME, t101.C101_USER_ID ID  ,t101.C101_PARTY_ID  PARTYID ");
      sbQuery.append(" FROM T101_USER t101 ");
      sbQuery.append("  , T1019_PARTY_COMPANY_MAP t1019 ");
      sbQuery.append("WHERE t1019.C101_PARTY_ID = t101.C101_PARTY_ID");
      sbQuery.append(" AND t101.C901_USER_TYPE IN (");
      sbQuery.append(strUsrCode);
      sbQuery.append(") ");
      // The following code added for Getting the Employee List based on the PLANT too if the
      // Selected Company is EDC
      if (strEmpFilter.equals("COMPANY-PLANT")) {
        sbQuery
            .append(" AND  t1019.C1900_COMPANY_ID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = "
                + getCompPlantId() + "  AND C5041_VOID_FL IS NULL )");
      } else if (strEmployeeList.equalsIgnoreCase("ALLEMPLOYEE")) {
        sbQuery
            .append(" AND  t1019.C1900_COMPANY_ID  IN (SELECT C1900_COMPANY_ID FROM T1900_COMPANY WHERE C1900_VOID_FL IS NULL");
        sbQuery.append(") ");
      } else {
        sbQuery.append(" AND  t1019.C1900_COMPANY_ID = " + getCompId());
	        if(!strInactiveUser.equals("Y")){
	        sbQuery.append(" AND  t1019.C1019_VOID_FL IS NULL");
	        }
      }
      // Code Commented on 11/20 to avoid using of C101_DEPT_ID
      // Instead c901_Dept_id should be used.
      // if (!strDept.equals(""))
      // {
      // sbQuery.append(" AND C101_DEPT_ID IN ('");
      // sbQuery.append(strDept);
      // sbQuery.append("')");
      // }

      if (!strDept.equals("")) {
        sbQuery.append(" AND t101.C901_DEPT_ID IN (");
        sbQuery.append("  SELECT C901_CODE_ID FROM T901_CODE_LOOKUP WHERE C901_CODE_NM IN ('");
        sbQuery.append(strDept);
        sbQuery.append("')AND C901_CODE_GRP='DEPID') ");
      }
      if (!strUserSts.equals("")) {
        sbQuery.append(" AND t101.C901_USER_STATUS IN ( ");
        sbQuery.append(strUserSts);
        sbQuery.append(") ");
      }
      if (strEmployeeList.equalsIgnoreCase("ALLEMPLOYEE")) {
        sbQuery
            .append(" GROUP BY t101.C101_USER_F_NAME, t101.C101_USER_L_NAME, t101.C101_USER_ID, t101.C101_PARTY_ID ");
      }
      sbQuery.append(" ORDER BY t101.C101_USER_F_NAME ");
      log.debug("Query for getting Employee List " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogonBean:getEmployeeList", "Exception is:" + e);
    }
    return alReturn;
  } // End of getEmployeeList


  /**
   * updateUserLoginAttempt - This method update login attempt count for a given userid
   * 
   * @param String strUserId
   * @exception AppError
   */
  public void updateUserLoginAttempt(String strUserId) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strUserMaxAttempt = GmCommonClass.getString("USER_MAX_LOGINATTEMPT");

    gmDBManager.setPrepareString("gm_pkg_it_user.gm_update_user_loginattempt", 2);

    gmDBManager.setString(1, strUserId);
    gmDBManager.setString(2, strUserMaxAttempt);

    gmDBManager.execute();

    gmDBManager.commit();

  } // End of updateUserLoginAttempt method

  /**
   * updateUserLock - This method is used to update login lock status for a given userid
   * 
   * @param String strUserId String strLoginLockFl
   * 
   * @exception AppError
   */
  public void updateUserLock(String strUserId, String strLoginLockFl, String strUpdatedByUserId)
      throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_it_user.gm_update_user_lock", 3);

    gmDBManager.setString(1, strUserId);
    gmDBManager.setString(2, strLoginLockFl);
    gmDBManager.setString(3, strUpdatedByUserId);
    gmDBManager.execute();

    gmDBManager.commit();

  } // End of updateUserLock method

  /**
   * getEmployeeList - Overloaded method of getEmployeeList(HashMap hmMap)
   * 
   */
  public ArrayList getEmployeeList(String strUsrCode, String strDept) throws AppError {
    HashMap hmMap = new HashMap();
    hmMap.put("USERCODE", strUsrCode);
    hmMap.put("DEPT", strDept);
    hmMap.put("STATUS", "311");

    return getEmployeeList(hmMap);
  } // End of getEmployeeList

  // End of updateUserLock method

  /**
   * fetchAllDBTokens - System should have functionality to store the last token and also when user
   * logs in, should validate against that token. This method is used for Web Service.In this method
   * we are passing token id as an i/p parameter and we are checking whether that token is available
   * in DB. return HashMap throws AppError
   * 
   */
  public HashMap fetchAllDBTokens(String strToken) throws AppError {
    HashMap hmTokenList = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_it_login.gm_fch_db_tokens", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strToken);
    gmDBManager.execute();
    hmTokenList =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return hmTokenList;
  } // End of fetchAllDBTokens


  /**
   * This method will get the User Info based on the User ID. This method will call the
   * get_user_name function by passing the User ID. With the User Name the method will call
   * getUserInfo to get the User Info.
   * 
   * @param strUserID return HashMap throws AppError
   */
  public HashMap getUserInfoByID(String strUserID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmReturn = new HashMap();

    gmDBManager.setFunctionString("get_login_user_name_by_id", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CHAR);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    String strUserName = gmDBManager.getString(1);
    gmDBManager.close();

    return GmCommonClass.parseNullHashMap(getUserInfo(strUserName));
  }
  
  /**
   * This method will get the User Access Information based on the User ID. 
   * @param strUserID return ArrayList throws AppError
   */
  public ArrayList fetchAppModuleAccess(String strUserId) throws AppError{
		ArrayList alAccessResult = null;
		HashMap hmReturn = new HashMap();
		GmLogonAccessVO gmLogonAccessVO =new GmLogonAccessVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_multi_event_access", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strUserId);
		gmDBManager.execute();
		alAccessResult = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
		log.debug("fetchAppModuleAccess alAccessResult "+alAccessResult);
		gmDBManager.close();
		hmReturn.put("ALACCESSRESULT", alAccessResult);
		
	  return alAccessResult;
  }

  /**
   * This method will get the User Access Information based on the User ID. 
   * @param strUserID return ArrayList throws AppError
   */
  public HashMap fetchAppViewAccess(String strUserId,String strAcclvl) throws AppError{
		ArrayList alAccessResult = null;
		ArrayList alViewResult = null;
		HashMap hmReturn = new HashMap();
		GmLogonAccessVO gmLogonAccessVO =new GmLogonAccessVO();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_app_view_access", 4);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1, strUserId);
		gmDBManager.setString(2, strAcclvl);
		gmDBManager.execute();
		alViewResult = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
		alAccessResult = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
		log.debug("fetchAppViewAccess alAccessResult "+alAccessResult);
		gmDBManager.close();
		hmReturn.put("ALVIEWRESULT", alViewResult);
		hmReturn.put("ALACCESSRESULT", alAccessResult);
		
	  return hmReturn;
  }

}// end of class GmLogonBean
