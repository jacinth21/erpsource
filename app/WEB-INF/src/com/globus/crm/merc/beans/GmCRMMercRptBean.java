package com.globus.crm.merc.beans;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.beans.GmBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.crm.bean.GmCRMActivityBean;

import oracle.jdbc.OracleTypes;


/**
 * @author TMuthusamy
 * GmCRMMercRptBean  --- MERC Save and Fetch Details
 */
public class GmCRMMercRptBean extends GmBean {

	  Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	  public GmCRMMercRptBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		   super(gmDataStoreVO);
		   // TODO Auto-generated constructor stub
	   }

/**
 * This method used to fetch Merc details to display in CRM Dashboard
 * @return
 * @throws AppError
 */
public String fetchMercDashboard (String strInput) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_crm_merc.gm_fch_merc", 2);
    gmDBManager.setString(1, strInput);
    gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
    gmDBManager.execute();
    String strMercDetails = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strMercDetails;
     
  }

/**
 * This method used to fetch Merc Attendee Report
 * @return
 * @throws AppError
 */
public String fetchMercAttendeeReport (String strInput) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_crm_merc.gm_fch_merc_attendee_list", 2);
    gmDBManager.setString(1, strInput);
    gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
    gmDBManager.execute();
    String strMercAttendee = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    return strMercAttendee;
  }

  
  
  /**
   * This method used to fetch Merc Report
   * @return
   * @throws AppError
   */
public String loadMERCDetailReport(HashMap hmParams) throws AppError {

	 String hmResult = "";
	    log.debug("alReturn>>>>" + hmParams);

	    String strRegionId = GmCommonClass.parseNull((String) hmParams.get("REGNID"));
	    String strFromDate = GmCommonClass.parseNull((String) hmParams.get("FROMDT"));
	    String strToDate = GmCommonClass.parseNull((String) hmParams.get("TODT"));
	    String strTitle = GmCommonClass.parseNull((String) hmParams.get("TITLE"));
	    String strStateId = GmCommonClass.parseNull((String) hmParams.get("STATE"));
	    String strCountryId = GmCommonClass.parseNull((String) hmParams.get("COUNTRY"));
	    String strfirstnm = GmCommonClass.parseNull((String) hmParams.get("FIRSTNM"));
	    String strlastnm = GmCommonClass.parseNull((String) hmParams.get("LASTNM"));
	    String strTypeId = GmCommonClass.parseNull((String) hmParams.get("TYPE"));
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();

	    sbQuery.append(" SELECT JSON_ARRAYAGG(JSON_OBJECT('id' VALUE T6670.C6670_MERC_ID, ");
	    sbQuery.append("'name' VALUE T6670.C6670_MERC_NM,");
	    sbQuery.append(" 'mtype' VALUE get_code_name(T6670.C901_TYPE),"); 
		sbQuery.append(" 'mtypeid' VALUE T6670.C901_TYPE,");
	    sbQuery.append(" 'mstatus' VALUE get_code_name(T6670.C901_STATUS),");
	    sbQuery.append(" 'mstatusid' VALUE T6670.C901_STATUS,");
	    sbQuery.append(" 'fromdt' VALUE TO_CHAR(T6670.C6670_FROM_DT, 'MM/DD/YYYY'), ");
	    sbQuery.append(" 'todt' VALUE TO_CHAR(T6670.C6670_TO_DT, 'MM/DD/YYYY'), ");
	    sbQuery.append(" 'city' VALUE T6670.C6670_CITY, ");
	    sbQuery.append(" 'state' VALUE T6670.C901_STATE, ");
	    sbQuery.append(" 'lang' VALUE get_code_name(T6670.C901_LANGUAGE), ");
	    sbQuery.append(" 'opento' VALUE T6670.C6670_OPENTO, ");
        sbQuery.append(" 'attendeefl' VALUE T6670.C6670_ATTENDEE_FL, ");
	    sbQuery.append(" 'statenm' VALUE NVL2(T6670.C901_STATE,get_code_name_alt(T6670.C901_STATE),get_code_name(T6670.C901_COUNTRY)), ");
	    sbQuery.append(" 'countrynm' VALUE get_code_name(T6670.C901_COUNTRY), ");
	    sbQuery.append(" 'regnid' VALUE T6670.C901_REGION, ");
	    sbQuery.append(" 'link' VALUE T6670.C6670_LINK");
	    sbQuery.append(" ) ORDER BY  T6670.C6670_FROM_DT ASC RETURNING CLOB)");
	    sbQuery.append("  FROM T6670_MERC T6670 ");
	    sbQuery.append("  WHERE T6670.C6670_VOID_FL          IS NULL ");
	    if (!strRegionId.equals(""))
	      sbQuery.append(" AND T6670.C6670_FROM_DT >= TRUNC(CURRENT_DATE) AND T6670.C901_REGION = '" + strRegionId + "'");
	    if (!strTypeId.equals(""))
	    	sbQuery.append(" AND T6670.C901_TYPE IN ('"+strTypeId.replaceAll(",", "','")+"')");
	    if (!strFromDate.equals("") && !strToDate.equals(""))
	      sbQuery.append( " AND T6670.C6670_FROM_DT BETWEEN TO_DATE('"+ strFromDate+"', 'MM/DD/YYYY') AND TO_DATE('"+ strToDate+"', 'MM/DD/YYYY') " 
	    		  		  + " AND  T6670.C6670_TO_DT BETWEEN TO_DATE('"+ strFromDate+"', 'MM/DD/YYYY') AND TO_DATE('"+ strToDate+"', 'MM/DD/YYYY') ");
	    if (!strTitle.equals(""))
		      sbQuery.append(" AND UPPER(c6670_MERC_NM) like '%"+ strTitle.toUpperCase() + "%' ");
	    if (!strStateId.equals(""))
	    	sbQuery.append(" AND T6670.C901_STATE IN ('"+strStateId.replaceAll(",", "','")+"')");
	    if (!strCountryId.equals(""))
	    	sbQuery.append(" AND T6670.C901_COUNTRY IN ('"+strCountryId.replaceAll(",", "','")+"')");
	    if ((!strfirstnm.equals("")) || (!strlastnm.equals(""))){
	    	sbQuery.append(" AND T6670.C6670_MERC_ID IN (SELECT T6671.C6670_MERC_ID FROM T6671_MERC_ATTENDEE_LIST T6671, T101_PARTY T101 ");
	    	sbQuery.append(" WHERE  T6671.C101_SURGEON_PARTY_ID = T101.C101_PARTY_ID ");
	    	sbQuery.append(" AND T101.C101_VOID_FL IS NULL ");
	    	sbQuery.append(" AND T6671.C6671_VOID_FL IS NULL "); 
	    	if (!strfirstnm.equals(""))
		        sbQuery.append(" AND UPPER(T101.C101_FIRST_NM) LIKE '%" + strfirstnm.replace("'", "''").toUpperCase() + "%' ");
	    	if (!strlastnm.equals(""))
		        sbQuery.append(" AND UPPER(T101.C101_LAST_NM) LIKE '%" + strlastnm.replace("'", "''").toUpperCase() + "%' ");
	    	sbQuery.append(" ) ");
	    }

	    log.debug("loadMERCDetailReport  Query  " + sbQuery.toString());
	    hmResult = gmDBManager.queryJSONRecord(sbQuery.toString());
	    gmDBManager.close();
	    return hmResult;

  }

	/**
	 * This method used to Create Merc Event in MERC module. Save the data from updated
	 * model object in front-end as a JSONDATA.
	 * 
	 * @param hmParams
	 * @return String strMercId
	 * @throws AppError
	 */
	public String saveMercDetail(HashMap hmParams) throws AppError {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String strJSONInput = GmCommonClass.parseNull((String) hmParams.get("JSONDATA"));
	  String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
	  String strMercId = "";
	  gmDBManager.setPrepareString("gm_pkg_crm_merc_txn.gm_sav_merc_dtl", 3);
	  gmDBManager.setString(1, strJSONInput);
	  gmDBManager.setString(2, strUserID);
	  gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
	  gmDBManager.execute();
	  strMercId = GmCommonClass.parseNull(gmDBManager.getString(3));
	  gmDBManager.commit();
	
	  return strMercId;
	}
	
	/**
	 * This method used to fetch MERC Event module. Save the data from updated
	 * model object in front-end as a JSONDATA.
	 * 
	 * @param hmParams
	 * @return String strMercId
	 * @throws AppError
	 */
	public String fetchMercDetail(HashMap hmParams) throws AppError {
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String strInput = GmCommonClass.parseNull((String) hmParams.get("MERCID"));
	  String hmResult = "";
	  gmDBManager.setPrepareString("gm_pkg_crm_merc_txn.gm_fch_merc_dtl", 2);
	  gmDBManager.setString(1, strInput);
	  gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
	  gmDBManager.execute();
	  hmResult = GmCommonClass.parseNull(gmDBManager.getString(2));
	  gmDBManager.close();
	
	  return hmResult;
	}


}

