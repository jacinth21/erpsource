package com.globus.crm.quickref.beans;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.beans.GmBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.crm.bean.GmCRMActivityBean;

import oracle.jdbc.OracleTypes;


/**
 * @author TMuthusamy
 * GmCRMQuickRefRptBean --- fetch Quick Reference report details
 */
public class GmCRMQuickRefRptBean extends GmBean {

	  Logger log = GmLogger.getInstance(this.getClass().getName());
	  
	  public GmCRMQuickRefRptBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		   super(gmDataStoreVO);
		   // TODO Auto-generated constructor stub
	   }

/**
 * This method used to fetch Quick Reference List as JSON String to display in CRM Dashboard
 * @return
 * @throws AppError
 */
public String fetchQuickRefListJSON() throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_crm_quickref.gm_fch_quick_ref_dash_json", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CLOB);
    gmDBManager.execute();
    String strQrefDetails = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strQrefDetails;
     
  }



}

