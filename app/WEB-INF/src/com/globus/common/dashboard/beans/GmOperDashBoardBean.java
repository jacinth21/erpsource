/**
 * \ * FileName : GmOperDashBoardBean.java Description : Author : tchandure Date : Jun 17, 2008
 * Copyright : Globus Medical Inc ----------------------------------------- 12/23/2008 bvidyasankar
 * Added columns in queries of getDHRDashboard and reportDashboardItems as part of Issue 1476 and
 * 1477.
 */
package com.globus.common.dashboard.beans;

import java.sql.ResultSet; 
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author tchandure
 * 
 */
public class GmOperDashBoardBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger

  // Class.
  // this will be removed all place changed with Data Store VO constructor
  public GmOperDashBoardBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmOperDashBoardBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public ArrayList getDHRDashboard(String strPOType, String StrPartNum) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // To improve DHR dashboard performance java query code changed to procedure.
    gmDBManager.setPrepareString("gm_pkg_op_dhr_rpt.gm_fch_dhr_dashboard", 3);
    gmDBManager.setString(1, strPOType);
    gmDBManager.setString(2, StrPartNum);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    log.debug("alReturn==>" + alReturn.size());

    return alReturn;
  } // End of getDHRDashboard

  public ArrayList getNCMRDashboard() throws AppError {
    ArrayList alReturn = new ArrayList();
    alReturn = getNCMRDashForShipment("");
    return alReturn;
  } // End of getNCMRDashboard

  public RowSetDynaClass reportDashboardItems(HashMap hmParamV) throws AppError {
    RowSetDynaClass rdReturn = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strTxnType = GmCommonClass.parseNull((String) hmParamV.get("TXNTYPE"));
    String strAgingVal = GmCommonClass.parseNull((String) hmParamV.get("AGINGVAL"));
    String strProc = GmCommonClass.parseNull((String) hmParamV.get("PRCTYPE"));
    String strRefId = GmCommonClass.parseNull((String) hmParamV.get("REFID"));

    StringBuffer sbInhouseQuery = new StringBuffer();
    StringBuffer sbConsQuery = new StringBuffer();
    
    String strFilter = 
    		GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "IN-HOUSE_TRANS_DASH"));/*IN-HOUSE_TRANS_DASH- To fetch the records for EDC entity countries based on plant*/
    strFilter = strFilter.equals("") ? getCompId() : getCompPlantId();

    log.debug("hmParam====" + hmParamV);


    if (strAgingVal.equals("CURR_VAL")) {
      sbInhouseQuery
          .append(" AND trunc(C412_CREATED_DATE) >= (trunc(CURRENT_DATE) - 7) + 1 and trunc(C412_CREATED_DATE) <= trunc(CURRENT_DATE) ");
      sbConsQuery
          .append(" AND trunc(C504_CREATED_DATE) >= (trunc(CURRENT_DATE) - 7) + 1 and trunc(C504_CREATED_DATE) <= trunc(CURRENT_DATE) ");
    } else if (strAgingVal.equals("SEVEN_15")) {
      sbInhouseQuery
          .append(" AND trunc(C412_CREATED_DATE) >= trunc(CURRENT_DATE) - 15 and trunc(c412_CREATED_DATE) <= trunc(CURRENT_DATE)-7 ");
      sbConsQuery
          .append(" AND trunc(C504_CREATED_DATE) >= trunc(CURRENT_DATE) - 15 and trunc(C504_CREATED_DATE) <= trunc(CURRENT_DATE)-7 ");
    } else if (strAgingVal.equals("SIXTEEN_21")) {
      sbInhouseQuery
          .append(" AND trunc(C412_CREATED_DATE) > trunc(CURRENT_DATE)-21 and trunc(C412_CREATED_DATE) <= trunc(CURRENT_DATE)-16 ");
      sbConsQuery
          .append(" AND trunc(C504_CREATED_DATE) > trunc(CURRENT_DATE)-21 and trunc(C504_CREATED_DATE) <= trunc(CURRENT_DATE)-16 ");
    } else if (strAgingVal.equals("GREATER_21")) {
      sbInhouseQuery.append(" AND trunc(C412_CREATED_DATE) < TRUNC(CURRENT_DATE)-20 ");
      sbConsQuery.append(" AND trunc(C504_CREATED_DATE) < TRUNC(CURRENT_DATE) - 20 ");
    }

    StringBuffer sbQuery = new StringBuffer();
    sbQuery.append(" SELECT * FROM (");
    sbQuery
        .append(" SELECT C504_CONSIGNMENT_ID CID, NVL(GET_RULE_VALUE('CON_TYPE',C504_TYPE),'C') CON_TYPE, ");
    sbQuery
        .append(" TO_CHAR(decode(C504_STATUS_FL,'2','Y','-')) CONTROLFL, C504_TYPE CTYPE, GET_CODE_NAME(C504_TYPE) TYPNAME, "); //PC-4748-Tuning-T504_CONSIGNMENT
    sbQuery.append(" TO_CHAR(C504_SHIP_REQ_FL) SHIPREQFL, C504_COMMENTS COMMENTS, ");
    sbQuery
        .append(" DECODE(c504_ship_to, '0', GET_USER_NAME(C504_SHIP_TO_ID),NULL,GET_USER_NAME(C504_SHIP_TO_ID),NVL (trim (get_cs_ship_name (c504_ship_to,c504_ship_to_id)), ' ')) UNAME, ");
    // PMT-20074 : 106703 :FG-ST and 106704 : BL-ST request id will be ref id
    sbQuery
        .append(" C504_CREATED_DATE CDATE , DECODE(C504_TYPE,106703,t504.c520_request_id,106704,t504.c520_request_id,9110,nvl(c504_ref_id,'-'),nvl(C504_REPROCESS_ID,'-')) REFID, to_number('1') CNT , C504_REPROCESS_ID TXNID , ROUND(CURRENT_DATE-C504_CREATED_DATE) AGING, C5057_BUILDING_SHORT_NAME BUILDSNAME ");
    sbQuery.append(" FROM T504_CONSIGNMENT T504, T5057_BUILDING T5057");  //T5057_BUILDING added for PMT-33514
    /*
     * IHLN(9110) - Once controlled moved to shipping.So,after controlled it should be disappear in
     * the InHouse Dashboard
     */
    sbQuery //PC-4748-Tuning-T504_CONSIGNMENT
        .append(" WHERE C504_STATUS_FL > '1' AND C504_STATUS_FL < DECODE(C504_TYPE,9110,'3',400085,'3',400086,'3',106703,'3',106704,'3','4') AND C207_SET_ID IS NULL "); // 400085-QNRT,400086-FGRT,106703:FG-ST,106704:BL-ST
    sbQuery.append(" AND C504_VOID_FL IS NULL ");
    sbQuery.append(" AND T504.C5057_BUILDING_ID = T5057.C5057_BUILDING_ID(+) ");   // added for PMT-33514
    sbQuery.append(" AND T5057.C5057_ACTIVE_FL(+) = 'Y' AND  T5057.C5057_VOID_FL IS NULL ");  // added for PMT-33514
    sbQuery.append(" AND ( C704_ACCOUNT_ID = '01' OR C504_TYPE in (400085,400086,106703,106704) )"); //PMT-20074 : 106703:FG-ST,106704:BL-ST
    sbQuery.append(" AND ( T504.C1900_company_id = " + strFilter);
    sbQuery.append(" OR T504.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");
    sbQuery
        .append(" AND C504_TYPE NOT IN (40051,40052,40053,40054,40055,40056,100069,100075,100076,103932)");
    if (strProc.equals("PROCTRANS")) {
      sbQuery.append(" AND C504_CONSIGNMENT_ID = '" + strRefId + "' ");
    }
    if (!strTxnType.equals("")) {
      sbQuery.append(" AND C504_TYPE = " + strTxnType);
    }
    if (!strAgingVal.equals("")) {
      sbQuery.append(sbConsQuery);
    }
    sbQuery.append(" UNION ALL ");
    sbQuery
        .append(" select t412.C412_INHOUSE_TRANS_ID CID , NVL(GET_RULE_VALUE('CON_TYPE',C412_TYPE),'I') CON_TYPE, TO_CHAR(decode(C412_STATUS_FL,'2','Y','-')) CONTROLFL , t412.C412_TYPE CTYPE,"); //PC-4748-Tuning-T504_CONSIGNMENT
    sbQuery
        .append(" GET_CODE_NAME(C412_TYPE) TYPNAME, TO_CHAR(decode(C412_VERIFY_FL,NULL,'1','-')) SHIPREQFL, t412.C412_COMMENTS COMMENTS,");
    sbQuery.append(" GET_USER_NAME(C412_CREATED_BY) UNAME, t412.C412_CREATED_DATE CDATE, ");
    sbQuery
        .append(" nvl(DECODE ( C412_TYPE, '50155', get_cs_fch_loaner_etchid(C412_REF_ID) , C412_REF_ID),'-') REFID, ");
    sbQuery
        .append(" to_number('1') CNT, t412.C412_REF_ID TXNID, ROUND(CURRENT_DATE-C412_CREATED_DATE) AGING, C5057_BUILDING_SHORT_NAME BUILDSNAME ");  //C5057_BUILDING_SHORT_NAME added for PMT-33514
    sbQuery.append(" FROM T412_INHOUSE_TRANSACTIONS t412, T5057_BUILDING T5057");   //T5057_BUILDING added for PMT-33514
    sbQuery //PC-4748-Tuning-T504_CONSIGNMENT
        .append(" WHERE t412.C412_STATUS_FL > '1' AND t412.C412_STATUS_FL < DECODE(C412_TYPE,50154,'3','4') "); // 50154:FGLE,400085:QNRT,400086:FGRT
    // No
    // need
    // to
    // fetch
    // FGLE
    // transaction
    // to
    // the
    // dash
    // board,
    // if
    // it
    // is
    // controlled
    sbQuery
        .append(" AND t412.C412_TYPE NOT IN ( 50159,1006571,1006572,40051,40052,40053,40054,40055,40056,100069,100075,100076,1006483,1006572,56045,56025,103932 ) ");
    sbQuery.append(" AND t412.C412_VOID_FL IS NULL");
    sbQuery.append(" AND T412.C5057_BUILDING_ID = T5057.C5057_BUILDING_ID(+) ");    // added for PMT-33514
    sbQuery.append(" AND T5057.C5057_ACTIVE_FL(+) = 'Y' AND  T5057.C5057_VOID_FL IS NULL ");   // added for PMT-33514
    sbQuery.append(" AND ( t412.C1900_company_id = " + strFilter);
    sbQuery.append(" OR t412.C5040_plant_id = " + strFilter);
    sbQuery.append(") ");

    if (strProc.equals("PROCTRANS")) {
      sbQuery.append(" AND t412.C412_REF_ID = '" + strRefId + "' ");
    }
    if (!strTxnType.equals("")) {
      sbQuery.append(" AND t412.C412_TYPE= " + strTxnType);
    }
    if (!strAgingVal.equals("")) {
      sbQuery.append(sbInhouseQuery);
    }
    sbQuery.append(") ORDER BY TYPNAME, CID, CDATE DESC");

    rdReturn = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());
    log.debug("::reportDashboardItems::SBQRY::::" + sbQuery.toString());
    return rdReturn;
  }// End of reportDashboardItems

  public ArrayList fetchInhouseAgingSummary() throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_PKG_OP_INVENTORY.GM_FCH_AGING_SUMMARY", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alReturn;
  }

  public String getXmlGridData(HashMap hmParamV) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParamV.get("STRSESSCOMPANYLOCALE"));
    String strVmPath = GmCommonClass.parseNull((String) hmParamV.get("STRVMPATH"));
    ArrayList alParam = (ArrayList) hmParamV.get("RLIST");
    String strGridXmlData = "";
    String strTemplate = (String) hmParamV.get("TEMPLATE");
    String strTemplatePath = (String) hmParamV.get("TEMPLATEPATH");
    templateUtil.setDataList("alResult", alParam);
    templateUtil.setDataMap("hmApplnParam", hmParamV);
    templateUtil.setTemplateName(strTemplate);
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPath,
        strSessCompanyLocale));
    strGridXmlData = templateUtil.generateOutput();
    return strGridXmlData;
  }

  /*
   * Description : This method is used to fetch the details of the NCMR based on the shipment.
   * Author: VPrasath
   */
  public ArrayList getNCMRDashForShipment(String strShipmentId) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCompnayID = GmCommonClass.parseNull(getCompId());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	t409.C409_NCMR_ID NID, t409.C408_DHR_ID DHRID, t409.C205_PART_NUMBER_ID PNUM, GET_PARTNUM_DESC(t409.C205_PART_NUMBER_ID) PDESC,");
    sbQuery
        .append(" t409.C402_WORK_ORDER_ID WORKID, t409.C301_VENDOR_ID VID, t409.C409_CREATED_DATE CDATE, ");
    sbQuery.append(" GET_VENDOR_NAME(t409.C301_VENDOR_ID) VNAME,t409.C409_STATUS_FL SFL, t409.C409_JIRA_TICKET_ID TICKETID ");
    sbQuery.append(" FROM T409_NCMR t409 ");

    if (!strShipmentId.equals("")) {
      sbQuery.append(", T408_DHR T408, T4082_BULK_DHR_MAPPING T4082 ");
    }

    sbQuery.append(" WHERE t409.C409_STATUS_FL < 3 ");

    if (!strShipmentId.equals("")) {
      sbQuery.append(" AND T4082.C4081_DHR_BULK_ID = '");
      sbQuery.append(strShipmentId);
      sbQuery.append("' AND T4082.C408_DHR_ID = T408.C408_DHR_ID ");
      sbQuery.append("  AND T408.C408_DHR_ID = t409.C408_DHR_ID ");
      sbQuery.append("  AND T4082.C4082_VOID_FL IS NULL ");
      sbQuery.append("  AND T408.C408_VOID_FL IS NULL ");
    }

    sbQuery.append(" AND t409.C409_VOID_FL IS NULL ");
    sbQuery.append(" AND  t409.C1900_COMPANY_ID = " + getCompId());
    if (!getCompPlantId().equals("")) {
      sbQuery.append(" AND t409.c5040_plant_id = " + getCompPlantId());
    }
    sbQuery.append(" ORDER BY UPPER(VNAME), t409.C409_CREATED_DATE DESC ");
    log.debug("The NCMR tab Query ***** " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  } // End of getNCMRDashForShipment

  /*
   * Description : This method is used to fetch the details of the DHR based on the shipment.
   * Author: Anilkumar
   */
  public ArrayList getDHRDashForShipment(String strShipmentId) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strCompanyID = GmCommonClass.parseNull(getCompId());

    sbQuery.append(" SELECT t408.c408_dhr_id DHRID,t408.c301_vendor_id VENDID,");
    sbQuery.append(" t408.c205_part_number_id PNUM,");
    sbQuery.append(" gm_pkg_op_bulk_dhr_rpt.get_dhr_status_name(t408.c408_dhr_id) status");
    sbQuery.append(" FROM t408_dhr t408,");
    sbQuery.append(" t4081_dhr_bulk_receive t4081,");
    sbQuery.append(" t4082_bulk_dhr_mapping t4082");
    sbQuery.append(" WHERE t4081.c4081_dhr_bulk_id = '");
    if (!strShipmentId.equals("")) {
      sbQuery.append(strShipmentId);
    }
    sbQuery.append("' AND t4081.c4081_dhr_bulk_id   = t4082.c4081_dhr_bulk_id");
    sbQuery.append(" AND t4082.c408_dhr_id         = t408.c408_dhr_id");
    sbQuery.append(" AND t4081.c1900_company_id = t408.c1900_company_id ");
    sbQuery.append(" AND t4081.c1900_company_id = " + strCompanyID + " ");
    sbQuery.append(" AND t408.c408_void_fl IS NULL ");
    sbQuery.append(" AND t4081.c4081_void_fl IS NULL ");
    sbQuery.append(" AND t4082.c4082_void_fl IS NULL ");
    log.debug("The DHR tab Query ***** " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alReturn;
  } // End of getDHRDashForShipment


}
