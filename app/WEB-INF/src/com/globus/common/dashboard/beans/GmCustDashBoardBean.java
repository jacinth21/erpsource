/*
 * 
 */
package com.globus.common.dashboard.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.transfer.beans.GmTransferBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCustDashBoardBean extends GmSalesFilterConditionBean {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing

	public GmCustDashBoardBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }

	public GmCustDashBoardBean() {
		super(GmCommonClass.getDefaultGmDataStoreVO());
	   }
	
  public HashMap reportDashboard(String strOpt) throws AppError {

    HashMap hmReturn = new HashMap();
    HashMap hmSales = new HashMap();
    RowSetDynaClass alReturnControl;
    GmCustomerBean gmCustBean = new GmCustomerBean(getGmDataStoreVO());
    GmTransferBean gmTransferBean = new GmTransferBean(getGmDataStoreVO());
    ArrayList alPendingTransfers = new ArrayList();

    RowSetDynaClass alDummyConsignment;

    ArrayList alReturnSets = new ArrayList();

    RowSetDynaClass alLoaners;

    alReturnControl = reportOrdDashboardCtrl(); // Orders Dashboard
    hmReturn.put("CNTRL", alReturnControl);

    hmSales = gmCustBean.todaysSales(); // Todays and Month to Date sales
    // figures
    hmReturn.put("SALES", hmSales);

    alReturnSets = reportDashboardSets(); // Sets Dashboard
    hmReturn.put("SETS", alReturnSets);

    if (strOpt.equals("CUST")) {
      alPendingTransfers = gmTransferBean.loadPendingTransfer(); // Return
      // the
      // pending
      // transfers
      hmReturn.put("TRANSFERS", alPendingTransfers);

      // alReturns = gmOper.getReturnsDashboard("CUSTSER"); // Returns
      // Dashboard
      // hmReturn.put("RETURNS",alReturns);

      alDummyConsignment = loadPendingDummyConsignmnet(); // Return the
      // pending dummy
      // consignments
      hmReturn.put("DUMMYCONSG", alDummyConsignment);

      // alLoaners = loadLoanerReconDashboard(); // Returns the
      // unreconciled
      // Loaner Transactions
      // (missing transactions
      // 50159)
      // hmReturn.put("LOANERS", alLoaners);
    }
    return hmReturn;
  }

  public RowSetDynaClass loadPendingDummyConsignmnet() throws AppError {
    RowSetDynaClass rdResult = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("GM_CS_FCH_PENDING_DUMMYCONSG", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    // gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();

    return rdResult;
  }

  public ArrayList loadLoanerReconDashboard(HashMap hmParam) throws AppError {

	    ArrayList alReturn = null;
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    StringBuffer sbQuery = new StringBuffer();
	    initializeParameters(hmParam);
	    log.debug("hmParam:::" + hmParam);
	    String strstatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
	    String strSetName = GmCommonClass.parseNull((String) hmParam.get("SETNAME"));
	    String strFromDate = GmCommonClass.parseNull((String) hmParam.get("FROMDT"));
	    String strToDate = GmCommonClass.parseNull((String) hmParam.get("TODT"));
	    String strDatetype = GmCommonClass.parseNull((String) hmParam.get("DTTYPE"));
	    String strDist = GmCommonClass.parseNull((String) hmParam.get("DIST"));
	    String strRep = GmCommonClass.parseNull((String) hmParam.get("REP"));
	    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
	    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("REQUESTID"));
	    String strQtType = GmCommonClass.parseNull((String) hmParam.get("QTTYPE"));



	    // strType= (strType.equals("0"))?"":strType;

	    sbQuery.append("SELECT t504a.c504a_consigned_to_id did, ");
	    sbQuery
	        .append(" nvl(t526.C526_PRODUCT_REQUEST_DETAIL_ID,0) reqdetid, nvl(t526.C525_PRODUCT_REQUEST_ID,0) reqid, ");
	    sbQuery.append(" NVL (t525.c525_email_count, 0) emailcnt, ");
	    sbQuery
	        .append(" gm_pkg_op_loaner.get_reconciled_flg (NVL (t526.c525_product_request_id, 0)) pendingrecon, ");
	    sbQuery.append(" get_distributor_name (t504a.c504a_consigned_to_id) dname, ");
	    sbQuery.append("  GET_REP_NAME(t504a.C703_SALES_REP_ID) repname,");
	    sbQuery.append(" t504b.c504a_etch_id etchid,");
	    sbQuery.append(" get_set_name (t504.c207_set_id) sname, ");
	    sbQuery.append(" t504a.c504a_loaner_dt ldate,");
	    sbQuery.append(" t504a.C504A_EXPECTED_RETURN_DT ExpRetdate,");
	    sbQuery.append(" t504a.c504a_return_dt rdate,");
	    sbQuery.append(" NVL(t504a.C504A_EXPECTED_RETURN_DT - t504a.c504a_return_dt,0) DaysLate,");
	    sbQuery.append(" NVL(t412.c412_inhouse_trans_id,'-') inhid,");
	    sbQuery.append(" get_user_name (t412.c412_created_by) cuser,");
	    sbQuery.append(" t412.c412_created_date cdate,");
	    sbQuery.append(" t504a.c504a_loaner_transaction_id lntransid, ");
	    sbQuery.append(" GET_LOG_FLAG(t504a.c504a_loaner_transaction_id,1245) acceptlog,");
	    sbQuery.append(" GET_LOG_FLAG(NVL (t412.c412_inhouse_trans_id, ''),1246) processlog, ");
	    sbQuery.append(" get_log_flag (NVL (t526.C525_PRODUCT_REQUEST_ID, ''), 1247) requestlog,");
	    sbQuery
	        .append(" t412.c412_status_fl sfl, DECODE(t412.c412_inhouse_trans_id, null, DECODE(t412.c412_status_fl, '4','R',''),DECODE(t412.c412_status_fl, '3','Y','4','R','')) consfl ,DECODE(t412.c412_type, 50159, gm_pkg_cm_loaner_jobs.get_jira_ticket_id(t526.c526_product_request_detail_id, t526.c525_product_request_id), '') ticketid, t5010.c5010_tag_id tagid");
	    sbQuery
	        .append(" FROM t504a_loaner_transaction t504a,t412_inhouse_transactions t412,t504_consignment t504,t504a_consignment_loaner t504b,t526_product_Request_detail t526,t525_product_request t525,  t5010_tag t5010");
	    sbQuery.append(getAccessFilterClauseWithRepID());
	    sbQuery
	        .append(" WHERE t504a.c504a_loaner_transaction_id = t412.c504a_loaner_transaction_id(+) ");
	    sbQuery.append(" AND t525.C1900_company_id = " + getCompId());
	    sbQuery.append(" AND t525.C1900_company_id = t526.C1900_company_id");
	    sbQuery.append(" AND t525.C1900_company_id = t412.C1900_company_id");
	    sbQuery.append(" AND t504a.c504_consignment_id = t504.c504_consignment_id ");
	    sbQuery.append(" AND t504.c504_consignment_id = t504b.c504_consignment_id ");
	    sbQuery
	        .append(" AND t504a.C526_PRODUCT_REQUEST_DETAIL_ID = t526.C526_PRODUCT_REQUEST_DETAIL_ID(+) ");
	    sbQuery.append(" AND t526.c525_product_request_id = t525.c525_product_request_id(+)");
	    sbQuery.append(" AND t412.c412_ref_id = t5010.c5010_last_updated_trans_id(+)");
	    sbQuery.append(" AND t412.c412_void_fl IS NULL");	    
	    // sbQuery.append(" AND t504b.c504a_status_fl !=60 ");//inactive
	    sbQuery.append(" AND t504a.c703_sales_rep_id   = V700.REP_ID ");	    

	    // distributor
	    if (!strDist.equals("") && !strDist.equals("0")) {
	      sbQuery.append(" and t504a.c504a_consigned_to_id = " + strDist);
	    }
	    // sales rep
	    if (!strRep.equals("") && !strRep.equals("0")) {
	      sbQuery.append(" and t504a.c703_sales_rep_id = " + strRep);
	    }
	    // set
	    if (!strSetName.equals("")) {
	      sbQuery.append(" and upper(get_set_name (t504.c207_set_id)) like upper('%" + strSetName
	          + "%')");
	    }

	    // date
	    if (!strDatetype.equals("0") && !strFromDate.equals("") && !strToDate.equals("")) {
	      String strdt = "";
	      if (strDatetype.equals("LN")) {
	        strdt = " t504a.C504A_LOANER_DT ";
	      } else if (strDatetype.equals("RT")) {
	        strdt = " t504a.C504A_RETURN_DT ";
	      } else if (strDatetype.equals("EX")) {
	        strdt = " t504a.C504A_EXPECTED_RETURN_DT ";
	      }
	      sbQuery.append(" and");
	      sbQuery.append(strdt);
	      sbQuery.append("  >= TO_DATE('");
	      sbQuery.append(strFromDate);
	      sbQuery.append("','" + getGmDataStoreVO().getCmpdfmt() + "')");
	      sbQuery.append(" and");
	      sbQuery.append(strdt);
	      sbQuery.append("  <= TO_DATE('");
	      sbQuery.append(strToDate);
	      sbQuery.append("','" + getGmDataStoreVO().getCmpdfmt() + "')");
	    }
	    if (!strRequestId.equals("") && !strRequestId.equals("0")) {
	      sbQuery.append(" and t526.c525_product_request_id =" + strRequestId);
	    }

	    if (!strQtType.equals("10060")) {
	      sbQuery.append(" and t412.c412_type = 50159");
	    }

	    // set status
	    if (strstatus.equals("50824")) { // usage
	      sbQuery.append(" AND t412.c412_type = 50159");
	      //
	    } else if (strstatus.equals("50822")) { // reconciled
	      sbQuery.append(" AND t412.c412_type = 50159");
	      sbQuery.append(" AND t412.c412_status_fl = 4");
	    } else if (strstatus.equals("50823")) { // non - reconciled
	      sbQuery.append(" AND t412.c412_type = 50159");
	      sbQuery.append(" AND t412.c412_status_fl < 4");
	    } else if (strstatus.equals("50825")) { // Late
	      sbQuery.append(" and t504a.c504a_return_dt > t504a.C504A_EXPECTED_RETURN_DT ");
	    }
	    if (!strType.equals("") && !strType.equals("0")) {
	      sbQuery.append(" and t504.C504_TYPE= " + strType);
	    }
	    sbQuery.append(" order by  t526.C525_PRODUCT_REQUEST_ID ");
	    log.debug("Loaner Recon Query " + sbQuery.toString());

	    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

	    return alReturn;
	  }

  public ArrayList reportDashboardSets() {
    ArrayList alReturn = new ArrayList();
    String strCompPlantFilter = "";
    strCompPlantFilter = GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "CONSIGN_ITEM_DASH")); //CONSIGN_ITEM_DASH-Rule Value:= plant.
    strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT T504.C504_CONSIGNMENT_ID CID, get_log_flag (T504.C504_CONSIGNMENT_ID, 1236) clog,T504.C504_STATUS_FL,");
      sbQuery
          .append("DECODE(get_consign_request_for(T504.C504_CONSIGNMENT_ID),'40025','Account Consignment','Item Consignment') SNAME,");
      sbQuery.append(" GET_USER_NAME(T504.C504_CREATED_BY) PER, GET_USER_NAME(T504.C504_SHIP_TO_ID) UNAME, ");
      sbQuery.append(" decode(T504.C701_DISTRIBUTOR_ID,'',T504.C704_ACCOUNT_ID,T504.C701_DISTRIBUTOR_ID) DID, ");
      sbQuery
          .append(" decode(T504.C701_DISTRIBUTOR_ID,'',GET_ACCOUNT_NAME(T504.C704_ACCOUNT_ID),GET_DISTRIBUTOR_NAME(T504.C701_DISTRIBUTOR_ID)) DNAME, ");
      sbQuery.append(" T504.C504_CREATED_DATE IDATE,T504.C504_CREATED_DATE CDATE, T504.C504_COMMENTS COMMENTS , ");
      sbQuery.append(" decode(T504.C504_STATUS_FL,2,'Y','-') CONSFL, T504.C504_SHIP_REQ_FL SHIPREQFL ");
      sbQuery.append(" ,NVL(GET_RULE_VALUE('CON_TYPE',T504.C504_TYPE),'T') CON_TYPE ");
      sbQuery.append(" ,T504.C504_TYPE CTYPE ");
      sbQuery.append(" ,T520.C520_PLANNED_SHIP_DATE PLANNED_SHIP_DATE, T5057.C5057_BUILDING_SHORT_NAME BUILDSNAME ");  //C5057_BUILDING_SHORT_NAME added for PMT-33514

      sbQuery.append(" FROM T504_CONSIGNMENT T504, T520_REQUEST T520, T5057_BUILDING T5057");   //t5057_building added for PMT-33514
      sbQuery.append(" WHERE T504.C504_STATUS_FL >= 2 AND T504.C504_STATUS_FL < 3 "); // changed
      // from
      // 3 to
      // 4 as
      // need
      // not
      // show
      // pending
      // shipping
      sbQuery.append(" AND  ((T504.C701_DISTRIBUTOR_ID IS NOT NULL OR T504.C704_ACCOUNT_ID = '01')");
      // to include account item consignment; 40025:Account Consignment
      sbQuery
          .append(" OR (get_consign_request_for (T504.C504_CONSIGNMENT_ID)='40025' AND T504.C704_ACCOUNT_ID IS NOT NULL))");
      sbQuery.append(" AND T504.C504_TYPE IN (4110,4111,4112) ");
      sbQuery.append(" AND T504.C504_VOID_FL IS NULL ");
      sbQuery.append(" AND T504.C207_SET_ID IS NULL ");
      sbQuery.append(" AND ( T504.C1900_COMPANY_ID = " + strCompPlantFilter);
      sbQuery.append(" OR T504.C5040_PLANT_ID = " + strCompPlantFilter);
      sbQuery.append(") ");
      sbQuery.append(" AND T520.C520_VOID_FL IS NULL ");
      sbQuery.append(" AND T504.C520_REQUEST_ID=T520.C520_REQUEST_ID");
      sbQuery.append(" AND T504.C5057_BUILDING_ID = T5057.C5057_BUILDING_ID(+) ");  //added for PMT-33514
      sbQuery.append(" AND T5057.C5057_ACTIVE_FL(+) = 'Y' AND  T5057.C5057_VOID_FL IS NULL ");   //added for PMT-33514
      sbQuery.append(" ORDER BY DNAME, C504_CONSIGNMENT_ID DESC ");
      log.debug("Query for reportDashboardSets : " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:reportDashboardSets", "Exception is:" + e);
    }
    return alReturn;
  }

  public RowSetDynaClass reportOrdDashboardCtrl() throws AppError {
    RowSetDynaClass rdResult = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("GM_REPORT_ORDER_CONTROL", 2);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    // gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return rdResult;

  }
/**
	 * reportIntrasittrans - This method used to fetch the Intransit transaction details
	 * @param No params
	 * @return ArrayList 
*/  
  public ArrayList reportIntrasittrans() {
	 
	 ArrayList alReturn = new ArrayList();
	 GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	 //StringBuffer sbQuery = new StringBuffer();
	 gmDBManager.setPrepareString("gm_pkg_in_transit_trans.gm_fetch_intransit_trans", 1);
	 gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
	 gmDBManager.execute();
	 alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
	 gmDBManager.close();
	 return alReturn;
  }
}
