/**
 * FileName    : GmClinicalDashBoardBean.java 
 * Description :
 * Author      : tchandure
 * Date        : Jul 10, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.dashboard.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

/**
 * @author tchandure
 * 
 */
public class GmClinicalDashBoardBean {

    Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ArrayList rPedingForm(HashMap hmParam, String strCondition) throws AppError {
		
		ArrayList alPedingForm = new ArrayList();
	    DBConnectionWrapper dbCon = new DBConnectionWrapper();

	    String strStudyID = GmCommonClass.parseNull((String) hmParam.get("strStudyID"));
		String strPatientId  = GmCommonClass.parseNull((String) hmParam.get("strPatientId"));
        String strSiteNames = GmCommonClass.parseNull((String) hmParam.get("strSiteNames"));
        
        log.debug("strStudyID **** ##########" + strStudyID+"###");
        log.debug("strPatientId **** ##########" + strPatientId+"###");
        log.debug("strSiteNames **** ##########" + strSiteNames+"###");
	    
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT v621.c611_study_id s_id, v621.c613_study_period_ds s_ds, v621.c901_study_period s_p_id");
		sbQuery.append(" , v621.c613_study_period_id s_p_key, c704_account_id a_id");
		sbQuery.append(" , DECODE (v621.c614_site_id, '', '', v621.c614_site_id || '-')");
		sbQuery.append(" || get_account_name (v621.c704_account_id)");
		sbQuery.append(" || DECODE (v621.c101_user_id, '', '', '-' || get_user_name (v621.c101_user_id)) a_name");
		sbQuery.append(" , v621.c621_patient_ide_no p_ide_no, v621.c621_patient_id p_key, v621.c601_form_id f_id");
		sbQuery.append(" , v621.c612_study_form_ds || ' - ' || get_form_name (v621.c601_form_id) f_name");
		sbQuery.append(" , TO_CHAR (c621_exp_date_start, 'MM/DD/YYYY') exp_start_date");
		sbQuery.append(" , TO_CHAR (v621.c621_exp_date, 'MM/DD/YYYY') exp_date");
		sbQuery.append(" , TO_CHAR (v621.c621_exp_date_final, 'MM/DD/YYYY') final_due_date");
		sbQuery.append(" , 1, 'Y'");
		sbQuery.append(" , 'N'");
		sbQuery.append(" , c612_seq seq");
		sbQuery.append(" FROM v621_patient_study_list v621");
		sbQuery.append(" WHERE (v621.c621_patient_id, v621.c613_study_period_id) NOT IN (");
		sbQuery.append(" SELECT t622.c621_patient_id");
		sbQuery.append(" , t622.c613_study_period_id");
		sbQuery.append(" FROM t622_patient_list t622");
		sbQuery.append(" WHERE t622.c621_patient_id = v621.c621_patient_id AND NVL(t622.C622_DELETE_FL,'N')='N' )");
		sbQuery.append(" AND v621.c621_exp_date_start <= SYSDATE	");
		sbQuery.append(" AND v621.c621_exp_date_final >= SYSDATE");
		sbQuery.append(" AND v621.c611_study_id = DECODE ('"+strStudyID+"', '', v621.c611_study_id, '"+strStudyID+"')");
		sbQuery.append(" AND v621.c614_site_id = DECODE ('"+strSiteNames+"', '', v621.c614_site_id, '"+strSiteNames+"')");
		sbQuery.append(" AND v621.c621_patient_id NOT IN (SELECT c621_patient_id");
		sbQuery.append(" FROM t622_patient_list");
		sbQuery.append(" WHERE c601_form_id = 15 AND c613_study_period_id = 91 AND NVL(C622_DELETE_FL,'N')='N')");
		sbQuery.append(" AND v621.c621_patient_ide_no = DECODE ('"+strPatientId+"', '', v621.c621_patient_ide_no, '"+strPatientId+"')");
		
		if (!strCondition.equals(""))
		{
			 sbQuery.append(" and "+strCondition);
		}
		sbQuery.append(" ORDER BY s_id, a_name, p_ide_no, s_p_id, seq");
		 
		log.debug("query==="+sbQuery.toString());
		alPedingForm = dbCon.queryMultipleRecords(sbQuery.toString());
	    log.debug("alPedingForm: "+alPedingForm);	
		
		return alPedingForm;
	}

	public ArrayList rOutOfWindow(HashMap hmParam, String strCondition) throws AppError {

		ArrayList alOutOfWindow = new ArrayList();

	    DBConnectionWrapper dbCon = new DBConnectionWrapper();

	    String strStudyID = GmCommonClass.parseNull((String) hmParam.get("strStudyID"));
		String strPatientId  = GmCommonClass.parseNull((String) hmParam.get("strPatientId"));
        String strSiteNames = GmCommonClass.parseNull((String) hmParam.get("strSiteNames"));
        
        log.debug("strStudyID **** ##########" + strStudyID+"###");
        log.debug("strPatientId **** ##########" + strPatientId+"###");
        log.debug("strSiteNames **** ##########" + strSiteNames+"###");
	    
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT   v621.c611_study_id s_id, v621.c613_study_period_ds s_ds, v621.c901_study_period s_p_id");
		sbQuery.append(" , v621.c613_study_period_id s_p_key, c704_account_id a_id");
		sbQuery.append(" ,	 DECODE (v621.c614_site_id, '', '', v621.c614_site_id || '-')");
		sbQuery.append(" || get_account_name (v621.c704_account_id)");
		sbQuery.append(" || DECODE (v621.c101_user_id, '', '', '-' || get_user_name (v621.c101_user_id)) a_name");
		sbQuery.append(" , v621.c621_patient_ide_no p_ide_no, v621.c621_patient_id p_key, v621.c601_form_id f_id");
		sbQuery.append(" , v621.c612_study_form_ds || ' - ' || get_form_name (v621.c601_form_id) f_name");
		sbQuery.append(" , TO_CHAR (c621_exp_date_start, 'MM/DD/YYYY') exp_start_date");
		sbQuery.append(" , TO_CHAR (v621.c621_exp_date, 'MM/DD/YYYY') exp_date");
		sbQuery.append(" , TO_CHAR (v621.c621_exp_date_final, 'MM/DD/YYYY') final_due_date");
		sbQuery.append(" , DECODE (SIGN (SYSDATE - DECODE (c621_exp_date_final, '', SYSDATE, c621_exp_date_final))");
		sbQuery.append(" , 1, 'Y'");
		sbQuery.append(" , 'N'");
		sbQuery.append(" ) over_due");
		sbQuery.append(" , c612_seq seq");
		sbQuery.append(" FROM v621_patient_study_list v621");
		sbQuery.append(" WHERE (v621.c621_patient_id, v621.c613_study_period_id) NOT IN (");
		sbQuery.append(" SELECT t622.c621_patient_id");
		sbQuery.append(" , t622.c613_study_period_id");
		sbQuery.append(" FROM t622_patient_list t622");
		sbQuery.append(" WHERE t622.c621_patient_id = v621.c621_patient_id AND NVL(t622.C622_DELETE_FL,'N')='N' )");
		sbQuery.append(" AND v621.c621_exp_date_final < SYSDATE");
		sbQuery.append(" AND v621.c611_study_id = DECODE ('"+strStudyID+"', '', v621.c611_study_id, '"+strStudyID+"')");
		sbQuery.append(" AND v621.c621_patient_id NOT IN (SELECT c621_patient_id");
		sbQuery.append(" FROM t622_patient_list");
		sbQuery.append(" WHERE c601_form_id = 15 AND c613_study_period_id = 91 AND NVL(C622_DELETE_FL,'N')='N')");
		sbQuery.append(" AND v621.c621_patient_ide_no='"+strPatientId+"'");
		if (!strCondition.equals(""))
		{
			 sbQuery.append(" and "+strCondition);
		}
		sbQuery.append(" ORDER BY s_id, a_name, p_ide_no, s_p_id, seq");
		 
		log.debug("query==="+sbQuery.toString());
		alOutOfWindow = dbCon.queryMultipleRecords(sbQuery.toString());
	    log.debug("alOutOfWindow: "+alOutOfWindow);	
		
		return alOutOfWindow;
	}

	public ArrayList rVerifyList(HashMap hmParam, String strCondition) throws AppError {

		ArrayList alVerifyList = new ArrayList();
	    DBConnectionWrapper dbCon = new DBConnectionWrapper();

	    String strStudyID = GmCommonClass.parseNull((String) hmParam.get("strStudyID"));
		String strPatientId  = GmCommonClass.parseNull((String) hmParam.get("strPatientId"));
        String strSiteNames = GmCommonClass.parseNull((String) hmParam.get("strSiteNames"));
        
        log.debug("strStudyID **** ##########" + strStudyID+"###");
        log.debug("strPatientId **** ##########" + strPatientId+"###");
        log.debug("strSiteNames **** ##########" + strSiteNames+"###");
	    
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT   t621.c611_study_id s_id, t613.c613_study_period_ds s_ds, t613.c901_study_period s_p_id");
		sbQuery.append(" , t613.c613_study_period_id s_p_key, get_code_seq_no (c901_study_period) seq_no");
		sbQuery.append(" , t621.c704_account_id a_id");
		sbQuery.append(" , DECODE (t614.c614_site_id, '', '', t614.c614_site_id || '-')");
		sbQuery.append(" || get_account_name (t614.c704_account_id)");
		sbQuery.append(" || DECODE (t614.c101_user_id, '', '', '-' || get_user_name (t614.c101_user_id)) a_name");
		sbQuery.append(" , t621.c621_patient_ide_no p_ide_no, t621.c621_patient_id p_key, t612.c601_form_id f_id");
		sbQuery.append(" , t612.c612_study_form_ds || ' - ' || get_form_name (t612.c601_form_id) f_name");
		sbQuery.append(" , TO_CHAR (NVL (t622.c622_date, ''), 'MM/DD/YYYY') e_date");
		sbQuery.append(" , get_user_name (NVL (t622.c622_last_updated_by, t622.c622_created_by)) entered_by");
		sbQuery.append(" FROM t621_patient_master t621");
		sbQuery.append(" , t612_study_form_list t612");
		sbQuery.append(" , t613_study_period t613");
		sbQuery.append(" , t614_study_site t614");
		sbQuery.append(" , t622_patient_list t622");
		sbQuery.append(" WHERE t621.c621_delete_fl = 'N'");
		sbQuery.append(" AND t612.c611_study_id = t621.c611_study_id");
		sbQuery.append(" AND t621.c611_study_id = t614.c611_study_id");
		sbQuery.append(" AND t612.c612_study_list_id = t613.c612_study_list_id");
		sbQuery.append(" AND t621.c621_patient_id = t622.c621_patient_id");
		sbQuery.append(" AND t612.c611_study_id = DECODE ('"+strStudyID+"', '', t612.c611_study_id, '"+strStudyID+"')");
		sbQuery.append(" AND t613.c613_study_period_id = t622.c613_study_period_id");
		sbQuery.append(" AND t621.c704_account_id = t614.c704_account_id");
		sbQuery.append(" AND NVL (t622.c622_verify_fl, 'N') = 'N'");
		sbQuery.append(" AND t621.C621_PATIENT_IDE_NO =DECODE ('"+strPatientId+"', '', t621.C621_PATIENT_IDE_NO, '"+strPatientId+"')");
		sbQuery.append(" AND NVL(t622.C622_DELETE_FL,'N')='N' ");
		if (!strCondition.equals(""))
		{
			 sbQuery.append(" and "+strCondition);
		}
		sbQuery.append(" ORDER BY s_id, a_name, p_ide_no, c612_seq");
		 
		log.debug("query==="+sbQuery.toString());
		alVerifyList = dbCon.queryMultipleRecords(sbQuery.toString());
	    log.debug("alPedingForm: "+alVerifyList);	
		
		return alVerifyList;
	}

	public ArrayList rVerifyList(GmDBManager gmDBManager, String strStudyID, String strCraId, String flag) throws AppError {

		ArrayList alVerifyList = new ArrayList();
		gmDBManager.setPrepareString("gm_pkg_cr_common.gm_pending_verification_list", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

		gmDBManager.setString(1, GmCommonClass.parseNull(strStudyID));
		gmDBManager.setString(2, GmCommonClass.parseNull(strCraId));
		gmDBManager.setString(3, GmCommonClass.parseNull(flag));
		gmDBManager.execute();
		alVerifyList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));

		return alVerifyList;
	}

	public String getXmlGridData(HashMap hmParamV) throws AppError {
		
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		ArrayList alParam = (ArrayList)hmParamV.get("RLIST");
		String strTemplate = (String)hmParamV.get("TEMPLATE");
		String strTemplatePath = (String)hmParamV.get("TEMPLATEPATH");
		
		templateUtil.setDataList("alResult", alParam);
		templateUtil.setTemplateName(strTemplate);
		templateUtil.setTemplateSubDir(strTemplatePath);
		return templateUtil.generateOutput();
	}
	
	
}
