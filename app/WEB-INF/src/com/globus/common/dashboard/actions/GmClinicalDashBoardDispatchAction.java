/**
 * FileName    : GmClinicalDashBoardDispatchAction.java 
 * Description :
 * Author      : tchandure
 * Date        : Jul 10, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.dashboard.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import com.globus.clinical.beans.GmStudyBean;
import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.dashboard.beans.GmClinicalDashBoardBean;

/**
 * @author tchandure
 * 
 */
public class GmClinicalDashBoardDispatchAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.

	public ActionForward pendingForms(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		GmClinicalDashBoardBean gmClinicalDashBoardBean = new GmClinicalDashBoardBean();	
		DynaActionForm gmClinicalDashBoardDispatchForm = (DynaActionForm) form;
		String strStudyID = GmCommonClass.parseNull((String) gmClinicalDashBoardDispatchForm.get("studyListId"));
		String strPatientId  = GmCommonClass.parseNull((String) gmClinicalDashBoardDispatchForm.get("Cbo_Patient"));
        String strSiteNames = GmCommonClass.createInputString(request.getParameterValues("checkSiteName"));
        log.debug("strStudyID **** ########## " + strStudyID);
        log.debug("strPatientId **** ########## " + strPatientId);
        log.debug("strSiteNames **** ########## " + strSiteNames);
        
        String strCondition = getAccessFilter(request, response);
        
        HashMap hmParam = new HashMap();
        hmParam.put("strStudyID",strStudyID);
        hmParam.put("strPatientId",strPatientId);
        hmParam.put("strSiteNames",strSiteNames);
                
        ArrayList alResult = gmClinicalDashBoardBean.rPedingForm(hmParam, strCondition);
        HashMap hmParamV = new HashMap();
        hmParamV.put("TEMPLATE","GmClinicalPatiendFormData.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alResult);

        gmClinicalDashBoardDispatchForm.set("gridXmlData", gmClinicalDashBoardBean.getXmlGridData(hmParamV));
		
		return mapping.findForward("successPendingForm");
	}

	public ActionForward outOfWindow(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		log.debug("pendingVerification ");
		GmClinicalDashBoardBean gmClinicalDashBoardBean = new GmClinicalDashBoardBean();	
		DynaActionForm gmClinicalDashBoardDispatchForm = (DynaActionForm) form;
		String strStudyID = GmCommonClass.parseNull((String) gmClinicalDashBoardDispatchForm.get("studyListId"));
		String strPatientId  = GmCommonClass.parseNull((String) gmClinicalDashBoardDispatchForm.get("Cbo_Patient"));
        String strSiteNames = GmCommonClass.createInputString(request.getParameterValues("checkSiteName"));
        log.debug("strStudyID **** ########## " + strStudyID);
        log.debug("strPatientId **** ########## " + strPatientId);
        log.debug("strSiteNames **** ########## " + strSiteNames);
        
        String strCondition = getAccessFilter(request, response);
        
        HashMap hmParam = new HashMap();
        hmParam.put("strStudyID",strStudyID);
        hmParam.put("strPatientId",strPatientId);
        hmParam.put("strSiteNames",strSiteNames);

        ArrayList alResult = gmClinicalDashBoardBean.rOutOfWindow(hmParam, strCondition);
        HashMap hmParamV = new HashMap();
        hmParamV.put("TEMPLATE","GmClinicalOutOfWindow.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alResult);

        gmClinicalDashBoardDispatchForm.set("gridXmlData", gmClinicalDashBoardBean.getXmlGridData(hmParamV));
		
		return mapping.findForward("successOutOfWindow");

	}

	public ActionForward pendingVerification(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("pendingVerification ");
		GmClinicalDashBoardBean gmClinicalDashBoardBean = new GmClinicalDashBoardBean();	
		DynaActionForm gmClinicalDashBoardDispatchForm = (DynaActionForm) form;
		String strStudyID = GmCommonClass.parseNull((String) gmClinicalDashBoardDispatchForm.get("studyListId"));
		String strPatientId  = GmCommonClass.parseNull((String) gmClinicalDashBoardDispatchForm.get("Cbo_Patient"));
        String strSiteNames = GmCommonClass.createInputString(request.getParameterValues("checkSiteName"));
        log.debug("strStudyID **** ########## " + strStudyID);
        log.debug("strPatientId **** ########## " + strPatientId);
        log.debug("strSiteNames **** ########## " + strSiteNames);
        
        String strCondition = getAccessFilter(request, response);
        
        HashMap hmParam = new HashMap();
        hmParam.put("strStudyID",strStudyID);
        hmParam.put("strPatientId",strPatientId);
        hmParam.put("strSiteNames",strSiteNames);

        ArrayList alResult = gmClinicalDashBoardBean.rVerifyList(hmParam, strCondition);
        HashMap hmParamV = new HashMap();
        hmParamV.put("TEMPLATE","GmClinicalPendingVerification.vm");
        hmParamV.put("TEMPLATEPATH","clinical/templates");
        hmParamV.put("RLIST",alResult);

        gmClinicalDashBoardDispatchForm.set("gridXmlData", gmClinicalDashBoardBean.getXmlGridData(hmParamV));
		
		return mapping.findForward("successVerifyList");
	}

	public ActionForward custDashBoardHome(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;
		gmCustDashBoardDispatchForm.set("hAction", "CLINICALDASHBOARD");
		return mapping.findForward("successHome");
	}
}
