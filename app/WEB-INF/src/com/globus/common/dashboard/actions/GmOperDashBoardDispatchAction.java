/**
 * FileName : GmOperDashBoardDispatchAction.java Description : Author : tchandure Date : Jun 17,
 * 2008 Copyright : Globus Medical Inc ----------------------------------------- 12/23/2008
 * bvidyasankar Get DHR count and set to attribute in form
 */
package com.globus.common.dashboard.actions;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmWidgetBean;
import com.globus.common.dashboard.beans.GmOperDashBoardBean;
import com.globus.common.util.GmTemplateUtil;

/**
 * @author tchandure
 * 
 */
public class GmOperDashBoardDispatchAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                // Class.

  public ActionForward dashboardDHR(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;
    instantiate(request, response);
    HashMap hmDHRStatusCnt = new HashMap();
    HashMap hmWidget = new HashMap();
    GmWidgetBean gmWidgetBean = new GmWidgetBean(getGmDataStoreVO());
    try {
      // String strOpt = GmCommonClass.parseNull((String)gmCustDashBoardDispatchForm.get("strOpt"));
      GmOperDashBoardBean gmOperDashBoardBean = new GmOperDashBoardBean(getGmDataStoreVO());

      List rdResult;
      rdResult = gmOperDashBoardBean.getDHRDashboard("3100,3101", request.getParameter("PartNum")); // FOR
                                                                                                    // DHR
                                                                                                    // Dashboard

      hmWidget.put("WIDGETID", "1");
      hmDHRStatusCnt = gmWidgetBean.createWidget(hmWidget);

      gmCustDashBoardDispatchForm.set("ldtResult", rdResult);
      // gmCustDashBoardDispatchForm.set("strOpt", strOpt);
      log.debug(hmDHRStatusCnt);
      gmCustDashBoardDispatchForm.set("hmDHRStatCnt", hmDHRStatusCnt);
    } catch (Exception e) {
      e.printStackTrace();
      // session.setAttribute("hAppErrorMessage",e.getMessage());
    }

    return mapping.findForward("success");
  }

  public ActionForward dashboardNCMR(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;
    try {
      instantiate(request, response);
      GmOperDashBoardBean gmOperDashBoardBean = new GmOperDashBoardBean(getGmDataStoreVO());
      List rdResult;
      rdResult = gmOperDashBoardBean.getNCMRDashboard(); // FOR DHR Dashboard

      gmCustDashBoardDispatchForm.set("ldtNCMRResult", rdResult);

    } catch (Exception e) {
      e.printStackTrace();
      // session.setAttribute("hAppErrorMessage",e.getMessage());
    }

    return mapping.findForward("successNCMR");
  }

  public ActionForward consignedItems(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    HashMap hmParam = new HashMap();
    instantiate(request, response);
    String strGridXmlData = "";
    String strForward = "successItems";
    String strTxnType = GmCommonClass.parseNull(request.getParameter("txnType"));
    String strAgingVal = GmCommonClass.parseNull(request.getParameter("agingVal"));
    HttpSession session = request.getSession(false);
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strVmPath = "properties.labels.custservice.GmDashBoardHome";
    DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;
    RowSetDynaClass rdResult = null;
    GmOperDashBoardBean gmOperDashBoardBean = new GmOperDashBoardBean(getGmDataStoreVO());

    hmParam.put("TXNTYPE", strTxnType);
    hmParam.put("AGINGVAL", strAgingVal);

    rdResult = gmOperDashBoardBean.reportDashboardItems(hmParam); // FOR DHR Dashboard
    ArrayList alResult = new ArrayList();
    ArrayList alReslt = new ArrayList();
    alResult = (ArrayList) rdResult.getRows();
    HashMap hmDetails = null;
    for (int itr = 0; itr < alResult.size(); itr++) {
      hmDetails = new HashMap();
      BasicDynaBean bdb = (BasicDynaBean) alResult.get(itr);
      String strName = GmCommonClass.parseNull((String) bdb.get("COMMENTS"));
      hmDetails.put("CID", bdb.get("CID"));
      hmDetails.put("CON_TYPE", bdb.get("CON_TYPE"));
      hmDetails.put("CONTROLFL", bdb.get("CONTROLFL"));
      hmDetails.put("CTYPE", bdb.get("CTYPE"));
      hmDetails.put("TYPNAME", bdb.get("TYPNAME"));
      hmDetails.put("SHIPREQFL", bdb.get("SHIPREQFL"));
      StringBuilder sbResult = new StringBuilder("");
      if (!strName.trim().equals("")) {
        for (int i = 0; i < strName.length(); i++) {
          char tmpChar = strName.charAt(i);
          if (Character.isLetterOrDigit(tmpChar) || tmpChar == '_' || tmpChar == '-'
              || tmpChar == ',' || tmpChar == '.' || tmpChar == ' ' || tmpChar == '('
              || tmpChar == ')') {
            sbResult.append(tmpChar);
          }
        }
      }
      hmDetails.put("COMMENTS", sbResult.toString());
      hmDetails.put("UNAME", bdb.get("UNAME"));
      hmDetails.put("CDATE", bdb.get("CDATE"));
      hmDetails.put("REFID", bdb.get("REFID"));
      hmDetails.put("CNT", bdb.get("CNT"));
      hmDetails.put("TXNID", bdb.get("TXNID"));
      hmDetails.put("AGING", bdb.get("AGING"));
      hmDetails.put("BUILDSNAME", bdb.get("BUILDSNAME"));

      alReslt.add(hmDetails);
    }

    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("STRVMPATH", strVmPath);
    hmParam.put("RLIST", alReslt);
    hmParam.put("SESSDATEFMT", strApplnDateFmt);
    hmParam.put("TEMPLATE", "GmConsignedItems.vm");
    hmParam.put("TEMPLATEPATH", "common/templates");
    strGridXmlData = gmOperDashBoardBean.getXmlGridData(hmParam);
    gmCustDashBoardDispatchForm.set("strGridXmlData", strGridXmlData);
    if (strTxnType != "" && strAgingVal != "") {
      strForward = "gmInhouseTransaction";
    }

    return mapping.findForward(strForward);
  }

  public ActionForward custDashBoardHome(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;
    gmCustDashBoardDispatchForm.set("hAction", "OPERDASHBOARD");
    return mapping.findForward("successHome");
  }

  public ActionForward openDashboardDHR(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    return mapping.findForward("success");
  }

  /**
   * NCMR dash for shipment.
   * 
   * @param ActionMapping,ActionForm,HttpServletRequest,HttpServletResponse
   * @return the ActionMapping
   * @throws Exception
   */
  public ActionForward NCMRDashForShipment(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmOperDashBoardBean gmOperDashBoardBean = new GmOperDashBoardBean(getGmDataStoreVO());
    DynaActionForm gmNCMRDashForShipmentForm = (DynaActionForm) form;

    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    String strxmlGridData = "";
    String strShipmentId = "";
    String strAction = "";
    String strVmPath = "properties.labels.operations.receiving.GmNCMRInfoInclude";

    strAction = GmCommonClass.parseNull(gmNCMRDashForShipmentForm.getString("hAction"));
    strShipmentId = GmCommonClass.parseNull(gmNCMRDashForShipmentForm.getString("hId"));

    if (strAction.equals("NCMR")) {
      alResult =
          GmCommonClass.parseNullArrayList(gmOperDashBoardBean
              .getNCMRDashForShipment(strShipmentId));
      hmParam.put("alResult", alResult);
      hmParam.put("STRVMPATH", strVmPath);
      strxmlGridData = generateOutPut(hmParam, "GmNCMRInfoInclude.vm");
      gmNCMRDashForShipmentForm.set("strGridXmlData", strxmlGridData);
    }
    return mapping.findForward("gmNCMRInfo");
  }

  /**
   * DHR dash for shipment.
   * 
   * @param ActionMapping,ActionForm,HttpServletRequest,HttpServletResponse
   * @return the ActionMapping
   * @throws Exception
   */
  public ActionForward DHRDashForShipment(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmOperDashBoardBean gmOperDashBoardBean = new GmOperDashBoardBean(getGmDataStoreVO());
    DynaActionForm gmDHRDashForShipmentForm = (DynaActionForm) form;

    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    String strxmlGridData = "";
    String strShipmentId = "";
    String strAction = "";
    String strVmPath = "properties.labels.operations.receiving.GmDHRInfoInclude";

    strAction = GmCommonClass.parseNull(gmDHRDashForShipmentForm.getString("hAction"));
    strShipmentId = GmCommonClass.parseNull(gmDHRDashForShipmentForm.getString("hId"));

    if (strAction.equals("DHR")) {
      alResult =
          GmCommonClass
              .parseNullArrayList(gmOperDashBoardBean.getDHRDashForShipment(strShipmentId));
      hmParam.put("alResult", alResult);
      hmParam.put("STRVMPATH", strVmPath);
      strxmlGridData = generateOutPut(hmParam, "GmDHRInfoInclude.vm");
      gmDHRDashForShipmentForm.set("strGridXmlData", strxmlGridData);
    }
    return mapping.findForward("gmDHRInfo");
  }

  /**
   * Generate out put.
   * 
   * @param hmReturn,vmFile
   * @return the string
   * @throws AppError the app error
   */
  private String generateOutPut(HashMap hmReturn, String vmFile) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmReturn.get("STRSESSCOMPANYLOCALE"));
    String strVmPath = GmCommonClass.parseNull((String) hmReturn.get("STRVMPATH"));
    templateUtil.setDataMap("hmReturn", hmReturn);
    templateUtil.setTemplateSubDir("operations/receiving/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPath,
        strSessCompanyLocale));
    templateUtil.setTemplateName(vmFile);

    return templateUtil.generateOutput();
  }
}
