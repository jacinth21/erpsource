/**
 * FileName : GmCustDashBoardDispatchAction.java Description : Author : tchandure Date : Jun 4, 2008
 * Copyright : Globus Medical Inc
 */
package com.globus.common.dashboard.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.dashboard.beans.GmCustDashBoardBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.transfer.beans.GmTransferBean;
import com.globus.operations.beans.GmOperationsBean;

/**
 * @author tchandure
 * 
 */
public class GmCustDashBoardDispatchAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  String strXmlData = "";

  public ActionForward wipOrderDashBoard(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;

    instantiate(request, response);
    HttpSession session = request.getSession(false);
    try {
      instantiate(request, response);
      GmCustDashBoardBean gmCustDashBoardBean = new GmCustDashBoardBean(getGmDataStoreVO());
     
      ArrayList alResult = new ArrayList();
      alResult = (ArrayList) gmCustDashBoardBean.reportOrdDashboardCtrl().getRows();
      String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
      String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
      String strVmPath="properties.labels.custservice.GmDashBoardHome";
      log.debug("date format in the customer action is" + strApplnDateFmt);
      HashMap hmApplnParam = new HashMap();
      hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
      hmApplnParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmApplnParam.put("STRVMPATH", strVmPath);
      log.debug("Data ==> " + alResult);
      strXmlData = generateOutPut(alResult, hmApplnParam, "GmWIPOrders.vm");
      log.debug("XMl Data ==> " + strXmlData);
      gmCustDashBoardDispatchForm.set("strXmlData", strXmlData);
    } catch (Exception e) {
      e.printStackTrace();
      // session.setAttribute("hAppErrorMessage",e.getMessage());
    }	

    return mapping.findForward("success");
  }

  public ActionForward consignedSetsItemsProcessing(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;
    instantiate(request, response);
    
    GmCustDashBoardBean gmCustDashBoardBean = new GmCustDashBoardBean(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    HashMap hmShipDateFl= new HashMap();
    String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
    String strVmPath="properties.labels.custservice.GmDashBoardHome";
    String strCompany = GmCommonClass.parseNull(getGmDataStoreVO().getCmpid());
    String strPlant = GmCommonClass.parseNull(getGmDataStoreVO().getPlantid());
    
    String strPlannedShipDateFl=
            GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPlant, "PLANNED_SHIP_DATE"));
    
    hmShipDateFl.put("PlannedShipDateFl",strPlannedShipDateFl);
      alResult = gmCustDashBoardBean.reportDashboardSets(); // Todays and
    // Month to Date
    // sales figures
    strXmlData = generateOutPut(alResult,hmShipDateFl, "GmConsignedSetsItem.vm",strSessCompanyLocale,strVmPath);
    log.debug("XMl Data GmConsignedSetsItem==> " + strXmlData);

    gmCustDashBoardDispatchForm.set("strXmlData", strXmlData);
    return mapping.findForward("successSale");

  }

  /**
   * fetch the data and display cust service dashboard screen
   * 
   * @return ArrayList
   * @method This method will get the values based on consignment id and void flag.
   */  
  public ActionForward inTransitProcessing(ActionMapping mapping, ActionForm form,
	      HttpServletRequest request, HttpServletResponse response) throws Exception {
	    DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;
	    instantiate(request, response);
	    
	    GmCustDashBoardBean gmCustDashBoardBean = new GmCustDashBoardBean(getGmDataStoreVO());
	    ArrayList alResult = new ArrayList();
	    String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
	    String strVmPath="properties.labels.custservice.GmDashBoardHome";
	    alResult = gmCustDashBoardBean.reportIntrasittrans();
	 	strXmlData = generateOutPut(alResult, "GmInTransit.vm",strSessCompanyLocale,strVmPath);
	    gmCustDashBoardDispatchForm.set("strXmlData", strXmlData);
	    HashMap hmRet=new HashMap();
	    hmRet.put("HMCOMDETAILS",alResult);
	    return mapping.findForward("successInTransit");

	  }
  
  
  public ActionForward consignedTransfer(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;
    log.debug("inside GmCustDashboradAction");
    instantiate(request, response);
  
    GmTransferBean gmTransferBean = new GmTransferBean(getGmDataStoreVO());
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    ArrayList alResult;
    String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
    HashMap hmApplnParam = new HashMap();
    hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
    templateUtil.setDataMap("hmApplnParam", hmApplnParam);
    alResult = gmTransferBean.loadPendingTransfer();
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setTemplateName("GmConsignedTransfer.vm");
    templateUtil.setTemplateSubDir("/common/templates/");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.custservice.GmDashBoardHome",
            strSessCompanyLocale));
    log.debug("Grid Data  size: " + alResult.size());
    gmCustDashBoardDispatchForm.set("ldtTransferResult", templateUtil.generateOutput());
    return mapping.findForward("successTransfer");

  }

  public ActionForward dummyConsignment(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;
    try {
      instantiate(request, response);
      GmCustDashBoardBean gmCustDashBoardBean = new GmCustDashBoardBean(getGmDataStoreVO());

      RowSetDynaClass rdResult;
      rdResult = gmCustDashBoardBean.loadPendingDummyConsignmnet();
      gmCustDashBoardDispatchForm.set("ldtDummyResult", rdResult.getRows());

    } catch (Exception e) {
      e.printStackTrace();
      // session.setAttribute("hAppErrorMessage",e.getMessage());
    }

    return mapping.findForward("successDummy");

  }

  public ActionForward returnsPendingCredits(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
   
    GmOperationsBean gmOperationsBean = new GmOperationsBean(getGmDataStoreVO());
    List alReturnsReturn = new ArrayList();
    HttpSession session = request.getSession(false);
    alReturnsReturn = gmOperationsBean.getReturnsDashboard("REPROCESS", ""); // FOR
    // Returns
    // Dashboard

    // Velu Aug 30 created for DHTMLH Report
   
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    String strSessCompanyLocale = GmCommonClass.parseNull((String)session.getAttribute("strSessCompanyLocale"));
    HashMap hmApplnParam = new HashMap();
    hmApplnParam.put("SESSDATEFMT", strApplnDateFmt);
    templateUtil.setDataMap("hmApplnParam", hmApplnParam);
    templateUtil.setDataList("alResult", alReturnsReturn);
    templateUtil.setTemplateName("GmReturnPendingCredit.vm");
    templateUtil.setTemplateSubDir("/operations/returns/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.custservice.GmDashBoardHome",
            strSessCompanyLocale));
    String XmlGridData = templateUtil.generateOutput();
    request.setAttribute("XMLGRIDDATA", XmlGridData);
    // end code
    return mapping.findForward("successReturnPendingCredit");

  }

  public ActionForward custDashBoardHome(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    DynaActionForm gmCustDashBoardDispatchForm = (DynaActionForm) form;
    gmCustDashBoardDispatchForm.set("hAction", "CUSTDASHBOARD");
    return mapping.findForward("successHome");
  }

  private String generateOutPut(ArrayList alGridData,  String vmFile, String strSessCompanyLocale, String strVmPath) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    
    HashMap hmParam = new HashMap();
    hmParam.put("DATEFORMAT", getGmDataStoreVO().getCmpdfmt());
    templateUtil.setDataMap("hmApplnParam", hmParam);
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setTemplateSubDir("common/dashboard/templates");
    templateUtil.setTemplateName(vmFile);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
    		strVmPath,strSessCompanyLocale));
    return templateUtil.generateOutput();
  }
  
  
  private String generateOutPut(ArrayList alGridData, HashMap hmShipDateFl, String vmFile, String strSessCompanyLocale, String strVmPath) throws AppError {
	    GmTemplateUtil templateUtil = new GmTemplateUtil();
	    
	    HashMap hmParam = new HashMap();
	    hmParam.put("DATEFORMAT", getGmDataStoreVO().getCmpdfmt());
	    templateUtil.setDataMap("hmApplnParam", hmParam);
	    templateUtil.setDataList("alGridData", alGridData);
	    templateUtil.setDataMap("hmShipDateFl", hmShipDateFl);
	    templateUtil.setTemplateSubDir("common/dashboard/templates");
	    templateUtil.setTemplateName(vmFile);
	    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
	    		strVmPath,strSessCompanyLocale));
	    return templateUtil.generateOutput();
	  }

  private String generateOutPut(ArrayList alGridData, HashMap hmParam, String vmFile)
      throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =(String)hmParam.get("STRSESSCOMPANYLOCALE");
    String strVmPath =(String)hmParam.get("STRVMPATH");
    templateUtil.setDataMap("hmApplnParam", hmParam);
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setTemplateSubDir("common/dashboard/templates");
    templateUtil.setTemplateName(vmFile);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
    		strVmPath,
            strSessCompanyLocale));
    return templateUtil.generateOutput();
  }
}
