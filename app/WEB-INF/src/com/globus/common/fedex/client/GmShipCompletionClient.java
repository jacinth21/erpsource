package com.globus.common.fedex.client;

/**
 * @author Bala
 *
 */ 
 
public abstract  class GmShipCompletionClient implements GmShipCompletionInterface {
	  
	/**
	 * This method returns the object dynamically. 
	 * @param strClassName
	 * @return
	 * @throws Exception
	 */
	
	public static GmShipCompletionInterface getInstance(String strClassName) throws Exception
	{	 
	  Class shipCmpClient  = Class.forName(strClassName); 
	  return (GmShipCompletionInterface)shipCmpClient.newInstance();
	}

	
}