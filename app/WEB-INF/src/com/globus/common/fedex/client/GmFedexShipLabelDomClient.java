package com.globus.common.fedex.client;

import java.util.Calendar;
import java.util.HashMap;

import org.apache.axis.types.NonNegativeInteger;
import org.apache.log4j.Logger;

import com.fedex.ship.stub.DropoffType;
import com.fedex.ship.stub.Notification;
import com.fedex.ship.stub.PackagingType;
import com.fedex.ship.stub.ProcessShipmentReply;
import com.fedex.ship.stub.ProcessShipmentRequest;
import com.fedex.ship.stub.RateRequestType;
import com.fedex.ship.stub.RequestedPackageLineItem;
import com.fedex.ship.stub.RequestedShipment;
import com.fedex.ship.stub.ShipPortType;
import com.fedex.ship.stub.ShipServiceLocator;
import com.fedex.ship.stub.TransactionDetail;
import com.fedex.ship.stub.VersionId;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * Sample code to call the FedEx Ship Service
 * <p>
 * com.fedex.ship.stub is generated via WSDL2Java, like this:<br>
 * 
 * <pre>
 * java org.apache.axis.wsdl.WSDL2Java -w -p com.fedex.ship.stub http://www.fedex.com/...../ShipService?wsdl
 * </pre>
 * 
 * This sample code has been tested with JDK 5 and Apache Axis 1.4
 */
//
// Sample code to call the FedEx Ship Service - Domestic MPS Express
//
public class GmFedexShipLabelDomClient extends GmFedexShipLabelMainClient {
  protected Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * This is the main method for generate FedEx Label
   * 
   * @param hmParam
   * @throws AppError
   */
  public HashMap printFedExLabel(HashMap hmShipInfo) {
    HashMap hmReturn = new HashMap();

    strPrintFeddxLabel = GmCommonClass.parseNull((String) hmShipInfo.get("PRINTSHIPLABEL"));
    strPrintRtnLabel = GmCommonClass.parseNull((String) hmShipInfo.get("PRINTRETURNLABEL"));

    setPropertiesFromHashMap(hmShipInfo);

    ProcessShipmentRequest masterRequest = buildRequest(); // Build a masterRequest object
    log.debug("Domestic Request");
    //
    try {
      // Initialize the service
      ShipServiceLocator service;
      ShipPortType port;
      //
      service = new ShipServiceLocator();
      updateEndPoint(service);
      port = service.getShipServicePort();// "https://wsbeta.fedex.com:443/web-services/ship";
      //
      log.debug("processShipment...");
      ProcessShipmentReply masterReply = port.processShipment(masterRequest); // This is the call to
                                                                              // the ship web
                                                                              // service passing in
                                                                              // a request object
                                                                              // and returning a
                                                                              // reply object
      //
      if (isResponseOk(masterReply.getHighestSeverity())) // check if the call was successful
      {
        log.debug("process master request isResponseOk");
        writeServiceOutput(masterReply);
        log.debug("Ship Track : " + strTrackingNo);
        hmReturn.put("TRACKINGNO", strTrackingNo);
        hmReturn.put("PRINTSHIPLABEL", strPrintFeddxLabel);
        // hmReturn.put("PRINTRETURNLABEL", strPrintRtnLabel);
      } else {
        Notification notification[] = masterReply.getNotifications();
        String strNotification = "";
        for (int i = 0; i < notification.length; i++) {
          log.debug("Notification : = " + notification[i].getMessage());
          strNotification = strNotification + notification[i].getMessage() + " ";

        }
        strNotification =
            strNotification.equals("") ? "Insufficient/Wrong parameters in call to FedEx service. Please refer http://us.spineit.net"
                : strNotification;
        hmReturn.put("ERRMSG", strNotification);
        throw new AppError(strNotification, "", 'E');
      }

    } catch (Exception e) {
      hmReturn.put("TRACKINGNO", "");
      hmReturn.put("PRINTSHIPLABEL", "N");
      // hmReturn.put("PRINTRETURNLABEL", "N");
      log.error("Error : ");
      e.printStackTrace();
      // throw new AppError(e);
    }
    return hmReturn;
  }

  /**
   * This method is used to build the FedEx shipment request
   * 
   * @return
   */
  private ProcessShipmentRequest buildRequest() {


    ProcessShipmentRequest request = new ProcessShipmentRequest(); // Build a request object

    request.setClientDetail(createClientDetail());
    request.setWebAuthenticationDetail(createWebAuthenticationDetail());
    //
    TransactionDetail transactionDetail = new TransactionDetail();
    transactionDetail.setCustomerTransactionId("Ship Request - "); // The client will get the same
                                                                   // value back in the response
    request.setTransactionDetail(transactionDetail);
    //
    VersionId versionId = new VersionId("ship", 13, 0, 0);
    request.setVersion(versionId);
    //
    RequestedShipment requestedShipment = new RequestedShipment();
    // Below code added due to fedEx changes the date on labels after 8:00 pm print.
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.HOUR, -2);
    requestedShipment.setShipTimestamp(cal); // Ship date and time
    requestedShipment.setDropoffType(DropoffType.REGULAR_PICKUP); // Dropoff Types are
                                                                  // BUSINESS_SERVICE_CENTER,
                                                                  // DROP_BOX, REGULAR_PICKUP,
                                                                  // REQUEST_COURIER, STATION
    requestedShipment.setServiceType(addServiceType()); // Service types are STANDARD_OVERNIGHT,
                                                        // PRIORITY_OVERNIGHT, FEDEX_GROUND ...

    // for totes set the packaging type as payor packaging otherwise set the type as fedex box
    String strPackageType =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("PKGTYP", strSource));
    requestedShipment.setPackagingType(PackagingType.fromString(strPackageType)); // Packaging type
                                                                                  // FEDEX_BOX,
                                                                                  // FEDEX_PAK,
                                                                                  // FEDEX_TUBE,
                                                                                  // YOUR_PACKAGING,
                                                                                  // ...

    requestedShipment.setShipper(addShipper());
    requestedShipment.setRecipient(addRecipient());
    requestedShipment.setShippingChargesPayment(addShippingChargesPayment());

    // requestedShipment
    // Example Shipment special service (Express COD).
    // requestedShipment.set
    log.debug("strServiceType === " + strServiceType);
    if (strServiceType.equals("5043") || strServiceType.equals("5042")
        || strServiceType.equals("5006")) {
      requestedShipment.setSpecialServicesRequested(addShipmentSpecialServicesRequested()); // Not
                                                                                            // allowed
                                                                                            // for
                                                                                            // 5003
    }

    requestedShipment
        .setRequestedPackageLineItems(new RequestedPackageLineItem[] {addRequestedPackageLineItem()});
    requestedShipment.setLabelSpecification(addLabelSpecification());
    //
    RateRequestType rateRequestType[] = new RateRequestType[1];
    rateRequestType[0] = RateRequestType.ACCOUNT; // Rate types requested LIST, MULTIWEIGHT, ...
    requestedShipment.setRateRequestTypes(rateRequestType);
    requestedShipment.setPackageCount(new NonNegativeInteger("1"));
    //
    request.setRequestedShipment(requestedShipment);
    //
    return request;
  }
}
