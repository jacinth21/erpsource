package com.globus.common.fedex.client;
import java.util.HashMap;

import com.globus.common.beans.AppError;

/**
 * @author Bala
 * Interface created for write method to complete the ship status for FEDEX and UPS.
 *
 */
 
public interface GmShipCompletionInterface 
	 
{
	public boolean complete (HashMap hmDetails) throws AppError, Exception;	

}
