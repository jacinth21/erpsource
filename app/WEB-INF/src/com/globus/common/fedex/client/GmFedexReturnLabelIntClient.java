package com.globus.common.fedex.client;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;

import org.apache.axis.types.NonNegativeInteger;
import org.apache.log4j.Logger;

import com.fedex.ship.stub.Address;
import com.fedex.ship.stub.Commodity;
import com.fedex.ship.stub.Contact;
import com.fedex.ship.stub.CustomsClearanceDetail;
import com.fedex.ship.stub.CustomsOptionDetail;
import com.fedex.ship.stub.CustomsOptionType;
import com.fedex.ship.stub.DropoffType;
import com.fedex.ship.stub.EdtRequestType;
import com.fedex.ship.stub.InternationalDocumentContentType;
import com.fedex.ship.stub.Money;
import com.fedex.ship.stub.Notification;
import com.fedex.ship.stub.PackagingType;
import com.fedex.ship.stub.Party;
import com.fedex.ship.stub.Payment;
import com.fedex.ship.stub.PaymentType;
import com.fedex.ship.stub.Payor;
import com.fedex.ship.stub.ProcessShipmentReply;
import com.fedex.ship.stub.ProcessShipmentRequest;
import com.fedex.ship.stub.RateRequestType;
import com.fedex.ship.stub.RequestedPackageLineItem;
import com.fedex.ship.stub.RequestedShipment;
import com.fedex.ship.stub.ReturnShipmentDetail;
import com.fedex.ship.stub.ReturnType;
import com.fedex.ship.stub.ShipPortType;
import com.fedex.ship.stub.ShipServiceLocator;
import com.fedex.ship.stub.ShipmentSpecialServiceType;
import com.fedex.ship.stub.ShipmentSpecialServicesRequested;
import com.fedex.ship.stub.TransactionDetail;
import com.fedex.ship.stub.VersionId;
import com.fedex.ship.stub.Weight;
import com.fedex.ship.stub.WeightUnits;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * Sample code to call the FedEx Ship Service
 * <p>
 * com.fedex.ship.stub is generated via WSDL2Java, like this:<br>
 * 
 * <pre>
 * java org.apache.axis.wsdl.WSDL2Java -w -p com.fedex.ship.stub http://www.fedex.com/...../ShipService?wsdl
 * </pre>
 * 
 * This sample code has been tested with JDK 5 and Apache Axis 1.4
 */
//
// Sample code to call the FedEx Ship Service - Domestic MPS Express
//
public class GmFedexReturnLabelIntClient extends GmFedexShipLabelMainClient {
  protected Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * This is the main method for generate FedEx Label
   * 
   * @param hmParam
   * @throws AppError
   */
  public HashMap printReturnLabel(HashMap hmShipInfo) {
    HashMap hmReturn = new HashMap();

    strPrintFeddxLabel = GmCommonClass.parseNull((String) hmShipInfo.get("PRINTSHIPLABEL"));
    strPrintRtnLabel = GmCommonClass.parseNull((String) hmShipInfo.get("PRINTRETURNLABEL"));

    setPropertiesFromHashMap(hmShipInfo);

    ProcessShipmentRequest masterRequest = buildRequest(); // Build a masterRequest object
    log.debug("International Request");
    //
    try {
      // Initialize the service
      ShipServiceLocator service;
      ShipPortType port;
      //
      service = new ShipServiceLocator();
      updateEndPoint(service);
      port = service.getShipServicePort();// "https://wsbeta.fedex.com:443/web-services/ship";
      //
      log.debug("processShipment...");
      ProcessShipmentReply masterReply = port.processShipment(masterRequest); // This is the call to
                                                                              // the ship web
                                                                              // service passing in
                                                                              // a request object
                                                                              // and returning a
                                                                              // reply object
      //
      if (isResponseOk(masterReply.getHighestSeverity())) // check if the call was successful
      {
        log.debug("process master request isResponseOk");
        writeServiceOutput(masterReply);
        // hmReturn.put("TRACKINGNO", strTrackingNo);
        // hmReturn.put("PRINTSHIPLABEL", strPrintFeddxLabel);
        hmReturn.put("PRINTRETURNLABEL", strPrintRtnLabel);
        hmReturn.put("RETURNTRACKINGNO", strReturnTrackingNumber);
      } else {
        Notification notification[] = masterReply.getNotifications();
        String strNotification = "";
        for (int i = 0; i < notification.length; i++) {
          log.debug("Notification : = " + notification[i].getMessage());
          strNotification = strNotification + notification[i].getMessage() + " ";

        }
        strNotification =
            strNotification.equals("") ? "Insufficient/Wrong parameters in call to FedEx service. Please refer http://us.spineit.net"
                : strNotification;
        hmReturn.put("ERRMSG", strNotification);
        throw new AppError(strNotification, "", 'E');
      }

    } catch (Exception e) {
      // hmReturn.put("TRACKINGNO", "");
      // hmReturn.put("PRINTSHIPLABEL", "N");
      hmReturn.put("PRINTRETURNLABEL", "N");
      log.error("Error :" + e);
      // throw new AppError(e);
    }
    return hmReturn;
  }

  /**
   * This method is used to build the FedEx shipment request
   * 
   * @return
   */
  private ProcessShipmentRequest buildRequest() {


    ProcessShipmentRequest request = new ProcessShipmentRequest(); // Build a request object

    request.setClientDetail(createClientDetail());
    request.setWebAuthenticationDetail(createWebAuthenticationDetail());
    //
    TransactionDetail transactionDetail = new TransactionDetail();
    transactionDetail.setCustomerTransactionId("Ship Request - "); // The client will get the same
                                                                   // value back in the response
    request.setTransactionDetail(transactionDetail);
    //
    VersionId versionId = new VersionId("ship", 13, 0, 0);
    request.setVersion(versionId);
    //
    RequestedShipment requestedShipment = new RequestedShipment();
    requestedShipment.setShipTimestamp(Calendar.getInstance()); // Ship date and time
    requestedShipment.setDropoffType(DropoffType.REGULAR_PICKUP); // Dropoff Types are
                                                                  // BUSINESS_SERVICE_CENTER,
                                                                  // DROP_BOX, REGULAR_PICKUP,
                                                                  // REQUEST_COURIER, STATION
    requestedShipment.setServiceType(addServiceType()); // Service types are STANDARD_OVERNIGHT,
                                                        // PRIORITY_OVERNIGHT, FEDEX_GROUND ...

    // for totes set the packaging type as payor packaging otherwise set the type as fedex box
    String strPackageType =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("PKGTYP", strSource));
    requestedShipment.setPackagingType(PackagingType.fromString(strPackageType)); // Packaging type
                                                                                  // FEDEX_BOX,
                                                                                  // FEDEX_PAK,
                                                                                  // FEDEX_TUBE,
                                                                                  // YOUR_PACKAGING,
                                                                                  // ...

    requestedShipment.setShipper(addRecipient());
    requestedShipment.setRecipient(addShipper());
    requestedShipment.setShippingChargesPayment(addShippingChargesPayment());
    requestedShipment.setCustomsClearanceDetail(addCustomsClearanceDetail());
    requestedShipment.setLabelSpecification(addLabelSpecification());
    //
    RateRequestType[] rrt = new RateRequestType[] {RateRequestType.ACCOUNT}; // Rate types requested
                                                                             // LIST, MULTIWEIGHT,
                                                                             // ...
    requestedShipment.setRateRequestTypes(rrt);
    requestedShipment.setEdtRequestType(EdtRequestType.ALL);
    requestedShipment.setPackageCount(new NonNegativeInteger("1"));

    ShipmentSpecialServicesRequested shipmentSpecialServicesRequested =
        new ShipmentSpecialServicesRequested();
    shipmentSpecialServicesRequested
        .setSpecialServiceTypes(new ShipmentSpecialServiceType[] {ShipmentSpecialServiceType.RETURN_SHIPMENT});
    ReturnShipmentDetail returnShipmentDetail = new ReturnShipmentDetail();
    returnShipmentDetail.setReturnType(ReturnType.PRINT_RETURN_LABEL);
    shipmentSpecialServicesRequested.setReturnShipmentDetail(returnShipmentDetail);
    requestedShipment.setSpecialServicesRequested(shipmentSpecialServicesRequested);

    //
    RequestedPackageLineItem[] rp = new RequestedPackageLineItem[] {addRequestedPackageLineItem()};
    requestedShipment.setRequestedPackageLineItems(rp);
    request.setRequestedShipment(requestedShipment);

    //
    return request;
  }

  @Override
  protected CustomsClearanceDetail addCustomsClearanceDetail() {
    CustomsClearanceDetail customs = new CustomsClearanceDetail(); // International details
    customs.setDutiesPayment(addDutiesPayment());
    customs.setCustomsValue(addMoney("USD", 100.00));
    customs.setDocumentContent(InternationalDocumentContentType.NON_DOCUMENTS);
    customs.setCustomsOptions(new CustomsOptionDetail(CustomsOptionType.FAULTY_ITEM,
        "CustmsOptionDescription"));
    customs.setCommodities(new Commodity[] {addCommodity()});// Commodity details
    return customs;
  }

  @Override
  protected Payment addDutiesPayment() {
    Payment payment = new Payment(); // Payment information
    payment.setPaymentType(PaymentType.SENDER);
    Payor payor = new Payor();
    Party responsibleParty = new Party();
    responsibleParty.setAccountNumber(getPayorAccountNumber());
    Address responsiblePartyAddress = new Address();
    responsiblePartyAddress.setCountryCode("US");
    responsibleParty.setAddress(responsiblePartyAddress);
    responsibleParty.setContact(new Contact());
    payor.setResponsibleParty(responsibleParty);
    payment.setPayor(payor);
    return payment;
  }

  @Override
  protected Money addMoney(String currency, Double value) {
    Money money = new Money();
    money.setCurrency(currency);
    money.setAmount(new BigDecimal(value));
    return money;
  }

  @Override
  protected Commodity addCommodity() {
    Commodity commodity = new Commodity();
    commodity.setNumberOfPieces(new NonNegativeInteger("1"));
    commodity.setDescription("Books");
    commodity.setCountryOfManufacture("US");
    commodity.setWeight(new Weight());
    commodity.getWeight().setValue(new BigDecimal(1.0));
    commodity.getWeight().setUnits(WeightUnits.LB);
    commodity.setQuantity(new NonNegativeInteger("1"));
    commodity.setQuantityUnits("EA");
    commodity.setUnitPrice(new Money());
    commodity.getUnitPrice().setAmount(new java.math.BigDecimal(400.000000));
    commodity.getUnitPrice().setCurrency("USD");
    commodity.setCustomsValue(new Money());
    commodity.getCustomsValue().setAmount(new java.math.BigDecimal(100.000000));
    commodity.getCustomsValue().setCurrency("USD");
    commodity.setCountryOfManufacture("US");
    commodity.setHarmonizedCode("490199009100");
    return commodity;
  }
}
