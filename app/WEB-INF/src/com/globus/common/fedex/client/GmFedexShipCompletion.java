package com.globus.common.fedex.client;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.operations.shipping.beans.GmShipTrackingInfoBean;

/* @author bgnanamani
 * This class used to 
 */


public class GmFedexShipCompletion implements GmShipCompletionInterface{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public boolean complete (HashMap hmDetails) throws AppError, Exception {
		GmShipTrackingInfoBean gmShipTrackingInfoBean = new GmShipTrackingInfoBean();
		boolean btrackFl = true;		
		btrackFl = gmShipTrackingInfoBean.getTrackStatus(hmDetails);
		return btrackFl;		
	}
}