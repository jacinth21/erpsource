package com.globus.common.fedex.client;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.axis.types.NonNegativeInteger;
import org.apache.axis.types.PositiveInteger;
import org.apache.log4j.Logger;

import com.fedex.ship.stub.Address;
import com.fedex.ship.stub.AssociatedShipmentDetail;
import com.fedex.ship.stub.ClientDetail;
import com.fedex.ship.stub.Commodity;
import com.fedex.ship.stub.CompletedPackageDetail;
import com.fedex.ship.stub.CompletedShipmentDetail;
import com.fedex.ship.stub.Contact;
import com.fedex.ship.stub.CustomerReference;
import com.fedex.ship.stub.CustomerReferenceType;
import com.fedex.ship.stub.CustomerSpecifiedLabelDetail;
import com.fedex.ship.stub.CustomsClearanceDetail;
import com.fedex.ship.stub.CustomsOptionDetail;
import com.fedex.ship.stub.CustomsOptionType;
import com.fedex.ship.stub.Dimensions;
import com.fedex.ship.stub.DocTabContent;
import com.fedex.ship.stub.DocTabContentType;
import com.fedex.ship.stub.DocTabContentZone001;
import com.fedex.ship.stub.DocTabZoneJustificationType;
import com.fedex.ship.stub.DocTabZoneSpecification;
import com.fedex.ship.stub.InternationalDocumentContentType;
import com.fedex.ship.stub.LabelFormatType;
import com.fedex.ship.stub.LabelPrintingOrientationType;
import com.fedex.ship.stub.LabelSpecification;
import com.fedex.ship.stub.LabelStockType;
import com.fedex.ship.stub.LinearUnits;
import com.fedex.ship.stub.Money;
import com.fedex.ship.stub.NotificationSeverityType;
import com.fedex.ship.stub.Party;
import com.fedex.ship.stub.Payment;
import com.fedex.ship.stub.PaymentType;
import com.fedex.ship.stub.Payor;
import com.fedex.ship.stub.ProcessShipmentReply;
import com.fedex.ship.stub.RequestedPackageLineItem;
import com.fedex.ship.stub.ServiceType;
import com.fedex.ship.stub.ShipServiceLocator;
import com.fedex.ship.stub.ShipmentRateDetail;
import com.fedex.ship.stub.ShipmentRating;
import com.fedex.ship.stub.ShipmentSpecialServiceType;
import com.fedex.ship.stub.ShipmentSpecialServicesRequested;
import com.fedex.ship.stub.ShippingDocument;
import com.fedex.ship.stub.ShippingDocumentImageType;
import com.fedex.ship.stub.ShippingDocumentPart;
import com.fedex.ship.stub.WebAuthenticationCredential;
import com.fedex.ship.stub.WebAuthenticationDetail;
import com.fedex.ship.stub.Weight;
import com.fedex.ship.stub.WeightUnits;
import com.fedex.ship.stub.TrackingId;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmPrintService;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;

/**
 * Sample code to call the FedEx Ship Service
 * <p>
 * com.fedex.ship.stub is generated via WSDL2Java, like this:<br>
 * 
 * <pre>
 * java org.apache.axis.wsdl.WSDL2Java -w -p com.fedex.ship.stub http://www.fedex.com/...../ShipService?wsdl
 * </pre>
 * 
 * This sample code has been tested with JDK 5 and Apache Axis 1.4
 */
//
// Sample code to call the FedEx Ship Service - Domestic MPS Express
//
public class GmFedexShipLabelMainClient {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  private String strShipperName = "";

  // Recipient Address properties
  private String strToTitle = "";
  private String strToPersonName = "";
  private String strToCompanyName = "";
  private String strToPhoneNumber = "";
  private String strToAdd1 = "";
  private String strToAdd2 = "";
  private String strToCity = "";
  private String strToState = "";
  private String strToPostalCode = "";
  private String strToCountryName = "";
  protected String strToCountryCode = "";

  //

  protected String strSource = ""; // portal source code - Ex. 50182 -> Loaner
  private String strSourceNm = "";
  private String strCustRef = ""; // Ex. Consignment/Loaner
  private String strPONum = "";
  private String strRefId = ""; // Ex. GM-CN-293745 -> Consignment

  // Package detail properties
  protected String strPackageWeight = "";
  private String strPackageLength = "";
  private String strPackageHeight = "";
  private String strPackageWidth = "";
  private String strReturnTrackingNo = "";

  private String strToteWeight = "";
  private String strUserId = "";
  private String strAcctPayorNo = "";
  private String strShipTo = "";

  protected String strPrintFeddxLabel = "";
  protected String strPrintRtnLabel = "";
  protected String strTrackingNo = "";
  protected String strServiceType = ""; // portal Ship mode code - Ex. 5004 -> Priority Overnight
  protected String strCarrierType = ""; // portal Ship carrier code - Ex. 5001 -> FedEx
  protected String strReturnTrackingNumber = "";
  static String strDivisionID="";

  /**
   * This method is used to set value for properties
   * 
   * @param hmParam
   */
  protected void setPropertiesFromHashMap(HashMap hmShipInfo) {

    GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
    gmTxnShipInfoVO = (GmTxnShipInfoVO) hmShipInfo.get("SHIPINFO");
    log.debug("==hmShipInfo==="+hmShipInfo);
    strToteWeight = GmCommonClass.parseNull((String) hmShipInfo.get("TOTEWEIGHT"));
    strPrintFeddxLabel = GmCommonClass.parseNull((String) hmShipInfo.get("PRINTSHIPLABEL"));
    strPrintRtnLabel = GmCommonClass.parseNull((String) hmShipInfo.get("PRINTRETURNLABEL"));
    strDivisionID = GmCommonClass.parseNull((String) hmShipInfo.get("DIVISION_ID"));
    log.debug("==strDivisionID==="+strDivisionID);
    strShipperName = "SHIPPING";
    strToTitle = "";

    strToPersonName = GmCommonClass.parseNull(gmTxnShipInfoVO.getAttnto());
    strToPersonName =
        strToPersonName.equals("") ? "" : "ATTN: "
            + GmCommonClass.parseNull(gmTxnShipInfoVO.getAttnto());
    strToCompanyName = GmCommonClass.parseNull(gmTxnShipInfoVO.getShip_to_nm());
    strToPhoneNumber = GmCommonClass.parseNull(gmTxnShipInfoVO.getPh_no());
    strToPhoneNumber =
        strToPhoneNumber.equals("") ? GmCommonClass.parseNull(GmCommonClass
            .getShipCarrierString("FEDEX_PHONE")) : strToPhoneNumber;
    strToAdd1 = GmCommonClass.parseNull(gmTxnShipInfoVO.getAddr1());
    strToAdd2 = GmCommonClass.parseNull(gmTxnShipInfoVO.getAddr2());
    strToCity = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipcity());
    strToState = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipstate());
    strToPostalCode = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipzip());
    strToCountryName = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipcountry());
    strToCountryCode = GmCommonClass.parseNull(gmTxnShipInfoVO.getCountrycode());

    strServiceType = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipmode());
    strCarrierType = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipcarrier());
    strSource = GmCommonClass.parseNull(gmTxnShipInfoVO.getSource());
    strShipTo = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipto());
    strSourceNm = GmCommonClass.parseNull(gmTxnShipInfoVO.getSourcenm());
    strCustRef = GmCommonClass.parseNull(gmTxnShipInfoVO.getTxnid());
    strRefId = GmCommonClass.parseNull(gmTxnShipInfoVO.getTxnid());
    strPONum = GmCommonClass.parseNull(gmTxnShipInfoVO.getCustpo());
    strUserId = GmCommonClass.parseNull(gmTxnShipInfoVO.getUserid());
    strAcctPayorNo = GmCommonClass.parseNull(gmTxnShipInfoVO.getAcctpayorno());
    /*
     * log.debug("strToPersonName==="+strToPersonName);
     * log.debug("strToCompanyName==="+strToCompanyName);
     * log.debug("strToPhoneNumber==="+strToPhoneNumber); log.debug("strToAdd1==="+strToAdd1);
     * log.debug("strToAdd2==="+strToAdd2); log.debug("strToCity==="+strToCity);
     * log.debug("strToState==="+strToState); log.debug("strToPostalCode==="+strToPostalCode);
     * log.debug("strToCountryName==="+strToCountryName);
     * log.debug("strServiceType==="+strServiceType); log.debug("strSource==="+strSource);
     * log.debug("strShipTo==="+strShipTo); log.debug("strSourceNm==="+strSourceNm);
     * log.debug("strPONum==="+strPONum); log.debug("strToCountryCode==="+strToCountryCode);
     */

    strPackageWeight = strToteWeight.equals("0") ? "5" : strToteWeight;

    // 50182: Loaner, 50181: Consignment
    strPackageLength = GmCommonClass.parseNull(GmCommonClass.getRuleValue("LENGTH", strSource));
    strPackageWidth = GmCommonClass.parseNull(GmCommonClass.getRuleValue("WIDTH", strSource));
    strPackageHeight = GmCommonClass.parseNull(GmCommonClass.getRuleValue("HEIGHT", strSource));
  }


  /**
   * This is the method to map portal ship mode to FedEx ship mode
   * 
   * @return
   */
  protected ServiceType addServiceType() {
    /*
     * 5042 Saturday-Prio Over 5043 Saturday-First Over 5008 Ground --No 5009 Freight --No 5004
     * Priority Overnight 5003 Early AM --No 5032 Standard Overnight 5033 International Priority
     * 5034 International Economy
     */

    try {
      // PMT-3542 -- Switch code is modified to Rule values. in future if we add some more Ship
      // Modes then we need to add the
      // corresponding CASE to the switch condition.
      // code becomes lengthy. for avoiding this, if we add those new Ship modes to Rule table, its
      // easy to display here.
      String strRuleServiceType = GmCommonClass.getRuleValue(strServiceType, strCarrierType);
      if (!strRuleServiceType.equals("")) {
        return ServiceType.fromString(strRuleServiceType);
      } else {
        throw new AppError("Please enter a valid shipmode.", "", 'E');
      }
    } catch (Exception ex) {
      log.error("error :" + ex.getMessage());
      throw new AppError("Please enter a valid shipmode.", "", 'E');
    }
  }

  /**
   * This method is used to get the label documents from the shipment reply
   * 
   * @param reply
   * @throws Exception
   */
  protected void writeServiceOutput(ProcessShipmentReply reply) throws Exception {
    log.debug(reply.getTransactionDetail().getCustomerTransactionId());
    CompletedShipmentDetail csd = reply.getCompletedShipmentDetail();
    CompletedPackageDetail cpd[] = csd.getCompletedPackageDetails();
    String masterTrackingNumber = printMasterTrackingNumber(csd);
    strTrackingNo = masterTrackingNumber;
    //For Return Tracking Number(PMT-33800)
    if (strPrintRtnLabel.equals("Y"))
        strReturnTrackingNumber = getReturnTrackingNumber(cpd);
   
    printShipmentRating(csd.getShipmentRating());
   
    log.debug("strPrintFeddxLabel==" + strPrintFeddxLabel);
    if (strPrintFeddxLabel.equals("Y") || strPrintRtnLabel.equals("Y")) {
      printPackageDetails(cpd);
    }
    log.debug("strPrintRtnLabel==" + strPrintRtnLabel);
    // If Express COD shipment is requested, the COD return label is returned as an Associated
    // Shipment.
    /*
     * if(strPrintRtnLabel.equals("Y")){ getAssociatedShipmentLabels(csd.getAssociatedShipments());
     * }
     */
  }

  /**
   * This method is used to check whether the FedEx web service is executed successfully or not
   * 
   * @param notificationSeverityType
   * @return
   */
  protected boolean isResponseOk(NotificationSeverityType notificationSeverityType) {
    if (notificationSeverityType == null) {
      return false;
    }
    if (notificationSeverityType.equals(NotificationSeverityType.WARNING)
        || notificationSeverityType.equals(NotificationSeverityType.NOTE)
        || notificationSeverityType.equals(NotificationSeverityType.SUCCESS)) {
      return true;
    }
    return false;
  }

  /**
   * this method is used to get tracking number
   * 
   * @param csd
   * @return
   */
  protected String printMasterTrackingNumber(CompletedShipmentDetail csd) {
    String trackingNumber = "";
    if (null != csd.getMasterTrackingId()) {
      trackingNumber = csd.getMasterTrackingId().getTrackingNumber();
    }
    return trackingNumber;
  }

  /**
   * This method is used to set package weight
   * 
   * @param packageWeight
   * @param weightUnits
   * @return
   */
  protected Weight addPackageWeight(Double packageWeight, WeightUnits weightUnits) {
    Weight weight = new Weight();
    weight.setUnits(weightUnits);
    weight.setValue(new BigDecimal(packageWeight));
    return weight;
  }

  /**
   * This method is used to set package dimensions
   * 
   * @param length
   * @param height
   * @param width
   * @param linearUnits
   * @return
   */
  protected Dimensions addPackageDimensions(Integer length, Integer height, Integer width,
      LinearUnits linearUnits) {
    Dimensions dimensions = new Dimensions();
    dimensions.setLength(new NonNegativeInteger(length.toString()));
    dimensions.setHeight(new NonNegativeInteger(height.toString()));
    dimensions.setWidth(new NonNegativeInteger(width.toString()));
    dimensions.setUnits(linearUnits);
    return dimensions;
  }


  /**
   * This method is used to get shipment weight and shipping cost. It will used in DocTab
   * 
   * @param shipmentRating
   */
  protected void printShipmentRating(ShipmentRating shipmentRating) {
    if (shipmentRating != null) {
      log.debug("Shipment Rate Details");
      ShipmentRateDetail[] srd = shipmentRating.getShipmentRateDetails();
      for (int j = 0; j < srd.length; j++) {
        log.debug("  Rate Type: " + srd[j].getRateType().getValue());
        log.debug("Shipment Billing Weight  =  " + srd[j].getTotalBillingWeight());
        log.debug("Shipment Base Charge =   " + srd[j].getTotalBaseCharge().getAmount());
        log.debug("Shipment Net Charge =   " + srd[j].getTotalNetCharge().getAmount());
        log.debug("Shipment Total Surcharge =   " + srd[j].getTotalSurcharges().getAmount());
      }
    }
  }


  /**
   * This method is used to set pay or account number detail
   * 
   * @return
   */
  protected String getPayorAccountNumber() {
    // See if payor account number is set as system property,
    // if not default it to "XXX"
    String payorAccountNumber = strAcctPayorNo;
    if (payorAccountNumber == null || payorAccountNumber.equals("")) {
      payorAccountNumber = GmCommonClass.getShipCarrierString("FEDEX_PAYOR_ACCOUNTNUMBER"); // Replace
                                                                                            // "XXX"
                                                                                            // with
                                                                                            // the
                                                                                            // payor
                                                                                            // account
                                                                                            // number
    }
    return payorAccountNumber;
  }
  
  /**
   * This method is used to set pay or account number detail
   * 
   * @return
   */
  protected String getPayorAccountNumber(String strDivisionId) {
    // See if payor account number is set as system property,
    // if not default it to "XXX"
	  log.debug("getPayorAccountNumber strDivisionId=="+strDivisionId);
    String payorAccountNumber = strAcctPayorNo;
    if (payorAccountNumber == null || payorAccountNumber.equals("")) {
      payorAccountNumber = GmCommonClass.getShipCarrierString("FEDEX_PAYOR_ACCOUNTNUMBER_"+strDivisionId); // Replace
                                                                                            // "XXX"
                                                                                            // with
                                                                                            // the
                                                                                            // payor
                                                                                            // account
                                                                                            // number
      log.debug("getPayorAccountNumber payorAccountNumber=="+payorAccountNumber);
    }
    return payorAccountNumber;
  }


  /**
   * This method is used to set Globus Address detail from constant.properties
   * 
   * @return
   */
  protected Party addShipper() {
    Party shipperParty = new Party(); // Sender information
    Contact shipperContact = new Contact();
    Address shipperAddress = new Address();

    String strCompanyName =
        GmCommonClass.parseNull(GmCommonClass.getShipCarrierString("FEDEX_COMPANY_NAME"));
    String strCompanyAddr1 =
        GmCommonClass.parseNull(GmCommonClass.getShipCarrierString("FEDEX_ADDR1"));
    String strCompanyAddr2 =
        GmCommonClass.parseNull(GmCommonClass.getShipCarrierString("FEDEX_ADDR2"));
    String strCompanyPhone =
        GmCommonClass.parseNull(GmCommonClass.getShipCarrierString("FEDEX_PHONE"));
    String strCompanyCity =
        GmCommonClass.parseNull(GmCommonClass.getShipCarrierString("FEDEX_CITY"));
    String strCompanyState =
        GmCommonClass.parseNull(GmCommonClass.getShipCarrierString("FEDEX_STATE"));
    String strCompanyPcode =
        GmCommonClass.parseNull(GmCommonClass.getShipCarrierString("FEDEX_PCODE"));
    String strCountryCode =
        GmCommonClass.parseNull(GmCommonClass.getShipCarrierString("FEDEX_STATEORPRV_CODE"));

    shipperContact.setPersonName(strShipperName);
    shipperContact.setCompanyName(strCompanyName);
    shipperContact.setPhoneNumber(strCompanyPhone);

    String strGlobusAddrStreetLines[] = new String[2];
    strGlobusAddrStreetLines[0] = strCompanyAddr1;
    strGlobusAddrStreetLines[1] = strCompanyAddr2;

    shipperAddress.setStreetLines(strGlobusAddrStreetLines);
    shipperAddress.setCity(strCompanyCity);
    shipperAddress.setStateOrProvinceCode(strCompanyState);
    shipperAddress.setPostalCode(strCompanyPcode);
    shipperAddress.setCountryCode(strCountryCode);
    shipperParty.setContact(shipperContact);
    shipperParty.setAddress(shipperAddress);

    return shipperParty;
  }

  /**
   * This method is used to set Recipient address detail
   * 
   * @return
   */
  protected Party addRecipient() {
    Party recipientParty = new Party(); // Recipient information
    Contact recipientContact = new Contact();

    if (!strToTitle.equals("")) {
      recipientContact.setTitle(strToTitle);
    }
    if (!strToPersonName.equals("")) {
      recipientContact.setPersonName(strToPersonName);
    }
    if (!strToCompanyName.equals("")) {
      recipientContact.setCompanyName(strToCompanyName);
    }
    if (!strToPhoneNumber.equals("")) {
      recipientContact.setPhoneNumber(strToPhoneNumber);
    }
    Address recipientAddress = new Address();
    if (!strToAdd1.equals("") || !strToAdd2.equals("")) {
      String[] strStreetLines = new String[2];
      strStreetLines[0] = strToAdd1;
      strStreetLines[1] = strToAdd2;
      recipientAddress.setStreetLines(strStreetLines);
    }
    if (!strToCity.equals("")) {
      recipientAddress.setCity(strToCity);
    }
    if (!strToState.equals("")) {
      recipientAddress.setStateOrProvinceCode(strToState);
    }
    if (!strToPostalCode.equals("")) {
      recipientAddress.setPostalCode(strToPostalCode);
    }
    if (!strToCountryName.equals("")) {
      recipientAddress.setCountryName(strToCountryName);
    }
    if (!strToCountryCode.equals("")) {
      recipientAddress.setCountryCode(strToCountryCode);
    }
    recipientAddress.setResidential(Boolean.valueOf(false));
    recipientParty.setContact(recipientContact);
    recipientParty.setAddress(recipientAddress);
    return recipientParty;
  }

  /**
   * This method is used to set the Payment type detail
   * 
   * @return
   */
  protected Payment addShippingChargesPayment() {
    Payment payment = new Payment(); // Payment information
    Payor payor = new Payor();
    Party responsibleParty = new Party();
    String strAcctNo = "";
    log.debug("==strDivisionID==="+strDivisionID);
    String strDefaultAcctNo = GmCommonClass.getShipCarrierString("FEDEX_PAYOR_ACCOUNTNUMBER"+"_"+strDivisionID);
    log.debug("==strDefaultAcctNo==="+strDefaultAcctNo);
    if (strSource.equals("50180")) {// order source
      strAcctNo = getPayorAccountNumber(strDivisionID);
      if (strAcctNo.equals(strDefaultAcctNo)) {
        payment.setPaymentType(PaymentType.SENDER);
      } else {
        payment.setPaymentType(PaymentType.THIRD_PARTY);
      }
    } else {
      payment.setPaymentType(PaymentType.SENDER);
    }
    strAcctNo = strAcctNo.equals("") ? strDefaultAcctNo : strAcctNo;
    log.debug("strAcctNo=====" + strAcctNo);
    responsibleParty.setAccountNumber(strAcctNo);
    Address responsiblePartyAddress = new Address();
    responsiblePartyAddress.setCountryCode(strToCountryCode);
    responsibleParty.setAddress(responsiblePartyAddress);
    responsibleParty.setContact(new Contact());
    payor.setResponsibleParty(responsibleParty);
    payment.setPayor(payor);

    return payment;
  }

  /**
   * This method is used to set Return label option
   * 
   * @return
   */
  protected ShipmentSpecialServicesRequested addShipmentSpecialServicesRequested() {
    log.debug("test====its in spectial service request");
    ShipmentSpecialServicesRequested shipmentSpecialServicesRequested =
        new ShipmentSpecialServicesRequested();
    ShipmentSpecialServiceType shipmentSpecialServiceType[] = new ShipmentSpecialServiceType[1];
    shipmentSpecialServiceType[0] = ShipmentSpecialServiceType.SATURDAY_DELIVERY;
    shipmentSpecialServicesRequested.setSpecialServiceTypes(shipmentSpecialServiceType);
    return shipmentSpecialServicesRequested;
  }

  /**
   * This method is used to set package weight,Dimensions and cust reference detail
   * 
   * @return
   */
  protected RequestedPackageLineItem addRequestedPackageLineItem() {

    int intPackageLength = Integer.parseInt(strPackageLength);
    int intPackageHeight = Integer.parseInt(strPackageHeight);
    int intPackageWidth = Integer.parseInt(strPackageWidth);
    double dbPackageWeight = Double.parseDouble(strPackageWeight);
    RequestedPackageLineItem requestedPackageLineItem = new RequestedPackageLineItem();
    requestedPackageLineItem.setSequenceNumber(new PositiveInteger("1"));
    requestedPackageLineItem.setGroupPackageCount(new PositiveInteger("1"));
    requestedPackageLineItem.setWeight(addPackageWeight(dbPackageWeight, WeightUnits.LB));
    requestedPackageLineItem.setDimensions(addPackageDimensions(intPackageLength, intPackageHeight,
        intPackageWidth, LinearUnits.IN));
    requestedPackageLineItem.setCustomerReferences(new CustomerReference[] {
        addCustomerReference(CustomerReferenceType.CUSTOMER_REFERENCE.getValue(), strSourceNm),
        // addCustomerReference(CustomerReferenceType.INVOICE_NUMBER.getValue(), "IV1234"),
        addCustomerReference(CustomerReferenceType.P_O_NUMBER.getValue(), strPONum),
        addCustomerReference(CustomerReferenceType.DEPARTMENT_NUMBER.getValue(), "Shipping"),

    });
    return requestedPackageLineItem;
  }

  /**
   * This method is used to set customer reference
   * 
   * @param customerReferenceType
   * @param customerReferenceValue
   * @return
   */
  protected CustomerReference addCustomerReference(String customerReferenceType,
      String customerReferenceValue) {
    CustomerReference customerReference = new CustomerReference();
    customerReference.setCustomerReferenceType(CustomerReferenceType
        .fromString(customerReferenceType));
    customerReference.setValue(customerReferenceValue);
    return customerReference;
  }

  /**
   * This method is used to set Label type, format and image type
   * 
   * @return
   */
  protected LabelSpecification addLabelSpecification() {
    LabelSpecification labelSpecification = new LabelSpecification(); // Label specification
    labelSpecification.setImageType(ShippingDocumentImageType.ZPLII);// Image types PDF, PNG, DPL,
                                                                     // ...
    labelSpecification.setLabelFormatType(LabelFormatType.COMMON2D); // LABEL_DATA_ONLY, COMMON2D
    labelSpecification.setLabelStockType(LabelStockType.value9); // STOCK_4X6.75_LEADING_DOC_TAB
    labelSpecification
        .setLabelPrintingOrientation(LabelPrintingOrientationType.TOP_EDGE_OF_TEXT_FIRST);
    labelSpecification.setCustomerSpecifiedDetail(addCustomerLabelTab());
    return labelSpecification;
  }

  /**
   * This method is used to set Doc Tab details
   * 
   * @return
   */
  protected CustomerSpecifiedLabelDetail addCustomerLabelTab() {
    DocTabContent docTabContent = new DocTabContent();
    docTabContent.setDocTabContentType(DocTabContentType.ZONE001);

    Date date = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    String strTodaysDt = sdf.format(date);

    DocTabZoneSpecification[] docTabZoneSpecification = new DocTabZoneSpecification[12];
    docTabZoneSpecification[0] =
        new DocTabZoneSpecification(new PositiveInteger("1"), "Ref", "", strCustRef,
            DocTabZoneJustificationType.LEFT);
    docTabZoneSpecification[1] =
        new DocTabZoneSpecification(new PositiveInteger("2"), "Dep", "", "Shipping",
            DocTabZoneJustificationType.LEFT);
    docTabZoneSpecification[2] =
        new DocTabZoneSpecification(new PositiveInteger("3"), "", "", "",
            DocTabZoneJustificationType.LEFT);
    docTabZoneSpecification[3] =
        new DocTabZoneSpecification(new PositiveInteger("4"), "", "", "",
            DocTabZoneJustificationType.LEFT);
    docTabZoneSpecification[4] =
        new DocTabZoneSpecification(new PositiveInteger("5"), "Date", "", strTodaysDt,
            DocTabZoneJustificationType.LEFT);
    docTabZoneSpecification[5] =
        new DocTabZoneSpecification(new PositiveInteger("6"), "Wgt", "", strPackageWeight + " LB",
            DocTabZoneJustificationType.LEFT);
    docTabZoneSpecification[6] =
        new DocTabZoneSpecification(new PositiveInteger("7"), "", "", "",
            DocTabZoneJustificationType.LEFT);
    docTabZoneSpecification[7] =
        new DocTabZoneSpecification(new PositiveInteger("8"), "DV", "", "",
            DocTabZoneJustificationType.LEFT);
    docTabZoneSpecification[8] =
        new DocTabZoneSpecification(new PositiveInteger("9"), "Shipping",
            "REPLY/PACKAGE/RATES/ACTUAL/BaseCharge/Amount", "", DocTabZoneJustificationType.RIGHT);
    docTabZoneSpecification[9] =
        new DocTabZoneSpecification(new PositiveInteger("10"), "Special",
            "REPLY/PACKAGE/RATES/ACTUAL/TotalFreightDiscounts/Amount", "",
            DocTabZoneJustificationType.RIGHT);
    docTabZoneSpecification[10] =
        new DocTabZoneSpecification(new PositiveInteger("11"), "Handling",
            "REPLY/PACKAGE/RATES/ACTUAL/TotalSurcharges/Amount", "",
            DocTabZoneJustificationType.RIGHT);
    docTabZoneSpecification[11] =
        new DocTabZoneSpecification(new PositiveInteger("12"), "Total",
            "REPLY/PACKAGE/RATES/ACTUAL/NetCharge/Amount", "", DocTabZoneJustificationType.RIGHT);

    docTabContent.setZone001(new DocTabContentZone001(docTabZoneSpecification));

    CustomerSpecifiedLabelDetail CSLD = new CustomerSpecifiedLabelDetail();
    CSLD.setDocTabContent(docTabContent);
    return CSLD;
  }

  /**
   * This method is used to get label detail
   * 
   * @param cpd
   * @throws Exception
   */
  protected void printPackageDetails(CompletedPackageDetail[] cpd) throws Exception {

    if (cpd != null) {
      log.debug("Package Details");
      for (int i = 0; i < cpd.length; i++) { // Package details / Rating information for each
                                             // package
        String trackingNumber = cpd[i].getTrackingIds()[0].getTrackingNumber();
        strTrackingNo = trackingNumber;
        // Write label buffer to file
        ShippingDocument sd = cpd[i].getLabel();
        log.debug("Package Details 111");
        saveShipmentLabelToFile(sd, trackingNumber, "");
      }
    }

  }

  /**
   * This method is used to get return label detail
   * 
   * @param associatedShipmentDetail
   * @throws Exception
   */
  protected void getAssociatedShipmentLabels(AssociatedShipmentDetail[] associatedShipmentDetail)
      throws Exception {
    log.debug("associatedShipmentDetail = " + associatedShipmentDetail);
    if (associatedShipmentDetail != null) {
      for (int j = 0; j < associatedShipmentDetail.length; j++) {
        if (associatedShipmentDetail[j].getLabel() != null
            && associatedShipmentDetail[j].getType() != null) {
          String trackingNumber = associatedShipmentDetail[j].getTrackingId().getTrackingNumber();
          strReturnTrackingNo = trackingNumber;
          String associatedShipmentType = associatedShipmentDetail[j].getType().getValue();
          ShippingDocument associatedShipmentLabel = associatedShipmentDetail[j].getLabel();
          log.debug("associatedShipmentType==" + associatedShipmentType);
          saveShipmentLabelToFile(associatedShipmentLabel, trackingNumber, associatedShipmentType);
        }
      }
    }

  }

  /**
   * This method is used to print label/ generate PDF
   * 
   * @param shippingDocument
   * @param trackingNumber
   * @param labelName
   * @throws Exception
   */
  protected void saveShipmentLabelToFile(ShippingDocument shippingDocument, String trackingNumber,
      String labelName) throws Exception {
    ShippingDocumentPart[] sdparts = shippingDocument.getParts();
    for (int a = 0; a < sdparts.length; a++) {

      ShippingDocumentPart sdpart = sdparts[a];

      GmPrintService gmPrintService = new GmPrintService();
      gmPrintService.setPrinterName(GmCommonClass.getRuleValue(strUserId, "ZEBRA_PRINTER"));
      log.debug("Zebra Printer === " + GmCommonClass.getRuleValue(strUserId, "ZEBRA_PRINTER"));
      //Changes added for the PMT-33800, the zebra printer is not available in all environment
      //so below code is to handle the null pointer exception
      try{
	      gmPrintService.printByteStream(sdpart.getImage());
	    }
	  catch (Exception ex)
	    {
	        log.error ( "ZEBRA printer not configured for this environment" + ex.getMessage());
	    }
    }
  }

  /**
   * This method is used to set client detail from constant properties
   * 
   * @return
   */
  protected ClientDetail createClientDetail() {
    ClientDetail clientDetail = new ClientDetail();
    log.debug("==createClientDetail strDivisionID==="+strDivisionID);
    String accountNumber = GmCommonClass.getShipCarrierString("FEDEX_ACCOUNTNUMBER"+"_"+strDivisionID);
    String meterNumber = GmCommonClass.getShipCarrierString("FEDEX_METERNUMBER"+"_"+strDivisionID);
    log.debug("==accountNumber==="+accountNumber);
    log.debug("==meterNumber==="+meterNumber);
    //
    // See if the accountNumber and meterNumber properties are set,
    // if set use those values, otherwise default them to "XXX"
    //
    if (accountNumber == null) {
      accountNumber = "XXX"; // Replace "XXX" with clients account number
    }
    if (meterNumber == null) {
      meterNumber = "XXX"; // Replace "XXX" with clients meter number
    }
    clientDetail.setAccountNumber(accountNumber);
    clientDetail.setMeterNumber(meterNumber);
    return clientDetail;
  }

  /**
   * This method is used to set FedEx authentication detail from constant properties
   * 
   * @return
   */
  protected WebAuthenticationDetail createWebAuthenticationDetail() {
    WebAuthenticationCredential wac = new WebAuthenticationCredential();
    log.debug("==createWebAuthenticationDetail strDivisionID==="+strDivisionID);
    String key = GmCommonClass.getShipCarrierString("FEDEX_KEY"+"_"+strDivisionID);
    String password = GmCommonClass.getShipCarrierString("FEDEX_PASSWORD"+"_"+strDivisionID);
    log.debug("== key==="+key);
    log.debug("== password==="+password);
    //
    // See if the key and password properties are set,
    // if set use those values, otherwise default them to "XXX"
    //
    if (key == null) {
      key = "XXX"; // Replace "XXX" with clients key
    }
    if (password == null) {
      password = "XXX"; // Replace "XXX" with clients password
    }
    wac.setKey(key);
    wac.setPassword(password);
    return new WebAuthenticationDetail(wac);
  }

  /**
   * This method is used to set FedEx web service URL from constant properties
   * 
   * @param serviceLocator
   */
  protected void updateEndPoint(ShipServiceLocator serviceLocator) {
    String endPoint = GmCommonClass.getShipCarrierString("FEDEX_SHIP_ENDPOINT");
    if (endPoint != null) {
      serviceLocator.setShipServicePortEndpointAddress(endPoint);
    }
  }

  protected CustomsClearanceDetail addCustomsClearanceDetail() {
    CustomsClearanceDetail customs = new CustomsClearanceDetail(); // International details
    customs.setDutiesPayment(addDutiesPayment());
    customs.setCustomsValue(addMoney("USD", 100.00));
    customs.setDocumentContent(InternationalDocumentContentType.NON_DOCUMENTS);
    customs.setCustomsOptions(new CustomsOptionDetail(CustomsOptionType.COURTESY_RETURN_LABEL,
        "CustmsOptionDescription"));
    // Set export detail - To be used for Shipments that fall under AES Compliance
    // ExportDetail exportDetail = new ExportDetail();
    // exportDetail.setExportComplianceStatement("AESX20091127123456");
    // intd.setExportDetail(exportDetail);
    customs.setCommodities(new Commodity[] {addCommodity()});// Commodity details
    return customs;
  }

  protected Payment addDutiesPayment() {
    Payment payment = new Payment(); // Payment information
    payment.setPaymentType(PaymentType.SENDER);
    Payor payor = new Payor();
    Party responsibleParty = new Party();
    responsibleParty.setAccountNumber(getPayorAccountNumber());
    Address responsiblePartyAddress = new Address();
    responsiblePartyAddress.setCountryCode("US");
    responsibleParty.setAddress(responsiblePartyAddress);
    responsibleParty.setContact(new Contact());
    payor.setResponsibleParty(responsibleParty);
    payment.setPayor(payor);
    return payment;
  }

  protected Money addMoney(String currency, Double value) {
    Money money = new Money();
    money.setCurrency(currency);
    money.setAmount(new BigDecimal(value));
    return money;
  }

  protected Commodity addCommodity() {
    Commodity commodity = new Commodity();
    commodity.setNumberOfPieces(new NonNegativeInteger("1"));
    commodity.setDescription("Books");
    commodity.setCountryOfManufacture("US");
    commodity.setWeight(new Weight());
    commodity.getWeight().setValue(new BigDecimal(1.0));
    commodity.getWeight().setUnits(WeightUnits.LB);
    commodity.setQuantity(new NonNegativeInteger("1"));
    commodity.setQuantityUnits("EA");
    commodity.setUnitPrice(new Money());
    commodity.getUnitPrice().setAmount(new java.math.BigDecimal(400.000000));
    commodity.getUnitPrice().setCurrency("USD");
    commodity.setCustomsValue(new Money());
    commodity.getCustomsValue().setAmount(new java.math.BigDecimal(100.000000));
    commodity.getCustomsValue().setCurrency("USD");
    commodity.setCountryOfManufacture("US");
    commodity.setHarmonizedCode("490199009100");
    return commodity;
  }
  /**
   * This method is used to get the return tracking number  
   * 
   * @param CompletedPackageDetail
   */
  protected String getReturnTrackingNumber(CompletedPackageDetail  cpd[])
  {
 
      for(int i=0 ; i<cpd.length; i++)
      {
        CompletedPackageDetail cpdDetail = cpd[i];
        TrackingId trackingId[] = cpdDetail.getTrackingIds();
        if(trackingId.length > 0){
        strReturnTrackingNo =  trackingId[0].getTrackingNumber();
        }
      }
  return strReturnTrackingNo;
  }

}
