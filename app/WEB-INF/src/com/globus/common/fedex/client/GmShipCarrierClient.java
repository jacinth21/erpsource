package com.globus.common.fedex.client;

import com.globus.common.beans.AppError;

/**
 * @author Xun
 *
 */ 
 
public abstract  class GmShipCarrierClient implements GmShipCarrierClientInterface {
	  
	/**
	 * This method returns the object dynamically. 
	 * @param strClassName
	 * @return
	 * @throws Exception
	 */
	
	public static GmShipCarrierClientInterface getInstance(String strClassName) throws AppError	
	{	 
		GmShipCarrierClientInterface clientInterface = null;
		try {
			Class shipClient = Class.forName(strClassName);
			clientInterface = (GmShipCarrierClientInterface) shipClient
					.newInstance();
		} catch (Exception ex) {
			throw new AppError(ex);
		}
		return clientInterface;	  
	}
	
}