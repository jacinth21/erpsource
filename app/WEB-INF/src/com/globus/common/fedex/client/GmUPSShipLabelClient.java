package com.globus.common.fedex.client;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmPrintService;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.ups.Base64Dec;
import com.ups.SOAPLogging;
import com.ups.Ship.AccountAddressType;
import com.ups.Ship.BillShipperType;
import com.ups.Ship.BillThirdPartyChargeType;
import com.ups.Ship.CodeType;
import com.ups.Ship.CurrencyMonetaryType;
import com.ups.Ship.DimensionsType;
import com.ups.Ship.ErrorDetailType;
import com.ups.Ship.Errors;
import com.ups.Ship.LabelImageFormatType;
import com.ups.Ship.LabelSpecificationType;
import com.ups.Ship.LabelStockSizeType;
import com.ups.Ship.PackageResultsType;
import com.ups.Ship.PackageType;
import com.ups.Ship.PackageWeightType;
import com.ups.Ship.PackagingType;
import com.ups.Ship.PaymentInfoType;
import com.ups.Ship.RateInfoType;
import com.ups.Ship.ReferenceNumberType;
import com.ups.Ship.RequestType;
import com.ups.Ship.ReturnServiceType;
import com.ups.Ship.ServiceType;
import com.ups.Ship.ShipAddressType;
import com.ups.Ship.ShipFromType;
import com.ups.Ship.ShipPhoneType;
import com.ups.Ship.ShipPortType;
import com.ups.Ship.ShipService;
import com.ups.Ship.ShipToAddressType;
import com.ups.Ship.ShipToType;
import com.ups.Ship.ShipUnitOfMeasurementType;
import com.ups.Ship.ShipmentChargeType;
import com.ups.Ship.ShipmentErrorMessage;
import com.ups.Ship.ShipmentRequest;
import com.ups.Ship.ShipmentResponse;
import com.ups.Ship.ShipmentResultsType;
import com.ups.Ship.ShipmentType;
import com.ups.Ship.ShipmentType.ShipmentServiceOptions;
import com.ups.Ship.ShipperType;
import com.ups.Ship.TaxIDCodeDescType;
import com.ups.Ship.TransactionReferenceType;
import com.ups.Ship.UPSSecurity;

/**
 * @author Xun
 * 
 */

// Sample code to call the UPS Ship Service

public class GmUPSShipLabelClient implements GmShipCarrierClientInterface {
	protected Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * This is the main method for generate UPS Label
	 * 
	 * @param HashMap
	 *            hmShipInfo
	 * @return HashMap object
	 */
	public HashMap printLabel(HashMap hmShipInfo) throws AppError {
		HashMap hmReturn = new HashMap();

		GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
		gmTxnShipInfoVO = (GmTxnShipInfoVO) hmShipInfo.get("SHIPINFO");

		String strToShipState = GmCommonClass.parseNull(gmTxnShipInfoVO
				.getShipstate());
		String strToCountryCode = GmCommonClass.parseNull(gmTxnShipInfoVO
				.getCountrycode());
		String strPrintLabel = GmCommonClass.parseNull((String) hmShipInfo
				.get("PRINTSHIPLABEL"));
		String strPrintRtnLabel = GmCommonClass.parseNull((String) hmShipInfo
				.get("PRINTRETURNLABEL"));

		hmReturn.putAll(decideAndCallPrintLabel(strPrintLabel,
				strPrintRtnLabel, hmShipInfo, strToCountryCode, strToShipState));

		return hmReturn;
	}

	/**
	 * This method returns an HashMap containing Error message, tracking no and
	 * print return label flag
	 * 
	 * @param String
	 *            strPrintLabel, String strPrintRtnLabel, HashMap hmShipInfo,
	 *            String strToCountryCode, String strToShipState
	 * @exception
	 * @return HashMap object
	 */

	private HashMap decideAndCallPrintLabel(String strPrintLabel,
			String strPrintRtnLabel, HashMap hmShipInfo,
			String strToCountryCode, String strToShipState) throws AppError {
		String strShipErrMsg = "";
		String strRtnErrMsg = "";
		String strPrintRtnLbl = "";
		HashMap hmReturn = new HashMap();
		HashMap hmReturnLbl = new HashMap();
		if (strPrintLabel.equals("Y")) {
			log.error("decideAndCallPrintLabel ---hmShipInfo--"+hmShipInfo);
			log.error("decideAndCallPrintLabel ---strToCountryCode--"+strToCountryCode);
			log.error("decideAndCallPrintLabel ---strToShipState--"+strToShipState);
			hmReturn = printShipLabel(hmShipInfo, strToCountryCode,
					strToShipState); // To print UPS ship label
			strShipErrMsg = GmCommonClass.parseNull((String) hmReturn
					.get("ERRMSG"));
		}
		// Ship label print has any issue then no need to call Return label  
		if (strPrintRtnLabel.equals("Y") && strShipErrMsg.equals("")) {
			hmReturnLbl = printReturnLabel(hmShipInfo, strToCountryCode,
					strToShipState);// To print UPS Return label
			strRtnErrMsg = GmCommonClass.parseNull((String) hmReturnLbl
					.get("ERRMSG"));
			strPrintRtnLbl = GmCommonClass.parseNull((String) hmReturnLbl
					.get("PRINTRETURNLABEL"));
		}
		hmReturn.put("PRINTRETURNLABEL", strPrintRtnLbl);
		hmReturn.put("ERRMSG", strShipErrMsg + strRtnErrMsg);
		return hmReturn;
	}

	/**
	 * This method returns an HashMap which containing Error message and
	 * tracking no fro ship label
	 * 
	 * @param HashMap
	 *            hmShipInfo, String strToCountryCode, String strToShipState
	 * @exception
	 * @return HashMap object
	 */
	public HashMap printShipLabel(HashMap hmShipInfo, String strToCountryCode,
			String strToShipState) throws AppError {
		log.error("printShipLabel enter.........." + hmShipInfo);
		GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
		gmTxnShipInfoVO = (GmTxnShipInfoVO) hmShipInfo.get("SHIPINFO");
		String strSoucre = "";
		String strTransId = "";
		String strUserId = "";
		String strCarrier = "";
		String strDivisionID="";
		 log.debug("== hmShipInfo UPS printShipLabel==="+hmShipInfo);
		strUserId = GmCommonClass.parseNull((String) hmShipInfo.get("USERID"));
		strTransId = gmTxnShipInfoVO.getTxnid();
		strSoucre = gmTxnShipInfoVO.getSourcenm();
		strCarrier = GmCommonClass.parseNull((String) hmShipInfo.get("SHIPCARRIER"));
		strDivisionID = GmCommonClass.parseNull((String) hmShipInfo.get("DIVISION_ID"));
		  log.debug("== strDivisionID UPS printShipLabel ==="+strDivisionID);
		String LabelFormat = GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_LABEL_FORMAT"));// "ZPL";// Quick way
															// to set the label
															// type, specify ZPL
															// or GIF.
		log.error("printShipLabel .......... UserId" + strUserId + " TransId" + strTransId+"Soucre " +strSoucre+"Carrier "+strCarrier);		

		/********* set test or prod environment base on LabelFormat in properties file **************/
		ShipPortType spt = setEnvironment();

		// Set SOAP message show on console
		setSOAPMsgToConsole(spt);

		/*************** Set security envelope***************** */
		UPSSecurity upsSecurity = accessCheck(strDivisionID);

		ShipmentRequest shipRequest = citystatezipValidate();

		/*************** set shipper, shipTo , shipFrom Information***************** */
		ShipmentType shpmnt = setShippingInfo(strTransId, strSoucre,
				gmTxnShipInfoVO);

		setShipmentVal(hmShipInfo, gmTxnShipInfoVO, shpmnt, strTransId,
				strSoucre);

		/********** Service********************** */
		setServiceType(gmTxnShipInfoVO, strCarrier, shpmnt);
		
		/************ Label Specification ******************** */
		setLabelSpec(LabelFormat, shipRequest, shpmnt);

		/************* Print ZPL ship label********************* */
		printShipLabelZPL(strUserId, LabelFormat, hmShipInfo, spt, shipRequest,
				upsSecurity);

		return hmShipInfo;
	}

	/**
	 * This method returns an HashMap which containing Error message and
	 * tracking no for return label
	 * 
	 * @param HashMap
	 *            hmShipInfo, String strToCountryCode, String strToShipState
	 * @exception
	 * @return HashMap object
	 */
	public HashMap printReturnLabel(HashMap hmShipInfo,
			String strToCountryCode, String strToShipState) throws AppError {
		log.debug("enter....  printReturnLabel ....." + hmShipInfo);
		GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
		gmTxnShipInfoVO = (GmTxnShipInfoVO) hmShipInfo.get("SHIPINFO");
		String strSoucre = "";
		String strTransId = "";
		String strUserId = "";
		String strDivisionID = "";
		log.debug("== printReturnLabel UPS  hmShipInfo==="+hmShipInfo);
		strTransId = GmCommonClass.parseNull(gmTxnShipInfoVO.getTxnid());
		strSoucre = GmCommonClass.parseNull(gmTxnShipInfoVO.getSourcenm());
		strUserId = GmCommonClass.parseNull(gmTxnShipInfoVO.getUserid());
		strDivisionID = GmCommonClass.parseNull((String) hmShipInfo.get("DIVISION_ID"));
		log.debug("== printReturnLabel UPS  strDivisionID==="+strDivisionID);
		String LabelFormat = GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_LABEL_FORMAT"));// "ZPL";// Quick way
															// to set the label
															// type, specify ZPL
															// or GIF.

		ShipPortType spt = setEnvironment();
		// Set SOAP message show on console
		setSOAPMsgToConsole(spt);

		// Set security envelope
		UPSSecurity upsSecurity = accessCheck(strDivisionID);

		ShipmentRequest shipRequest = citystatezipValidate();

		// set shipper, shipTo , shipFrom Information
		ShipmentType shpmnt = setRtnShippingInfo(strTransId, strSoucre,
				gmTxnShipInfoVO);

		setShipmentVal(hmShipInfo, gmTxnShipInfoVO, shpmnt, strTransId,
				strSoucre);

		/********** set return service ***********************/
		setRtnService(shpmnt);

		/************ Label Specification *********************/
		setLabelSpec(LabelFormat, shipRequest, shpmnt);

		/************* Print ZPL ship label **********************/
		printShipLabelZPL(strUserId, LabelFormat, hmShipInfo, spt, shipRequest,
				upsSecurity);
		return hmShipInfo;
	}

	/**
	 * This method returns ShipPortType which containing test or prod UPS URL
	 * 
	 * @param
	 * @exception
	 * @return ShipPortType object
	 */
	private ShipPortType setEnvironment() throws AppError {
		ShipService Ship_Service = new ShipService();
		ShipPortType spt = Ship_Service.getShipPort();
		log.error(" setEnvironment spt :" + spt);
		log.error(" setEnvironment spt URL:" + GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_SHIP_ENDPOINT")));
		try {		
		BindingProvider bp = (BindingProvider) spt;
		log.error("setEnvironment bp :" + bp);
		bp.getRequestContext().put(
				BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
				GmCommonClass.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_SHIP_ENDPOINT"))); // test
																		// "https://wwwcie.ups.com/webservices/Ship"
																		// prod
																		// https://onlinetools.ups.com/webservices/Ship
		log.error("setEnvironment bp bind :" + bp);
		}
        catch (Exception ex) {
        	ex.printStackTrace();
			String description = ex.getMessage();
			String statusCode = ex.toString();
			log.error("\nError: Code=" + statusCode + " Description="
					+ description);			
			throw new AppError("Unable to reach server", "", 'E');
		}		
		return spt;
	}

	/**
	 * This method set BindingProvider to show SOAP messages in the console.
	 * 
	 * @param ShipPortType
	 *            spt
	 * @exception
	 * @return
	 */
	private void setSOAPMsgToConsole(ShipPortType spt) throws AppError {
		// Ability to show SOAP messages in the console.
		BindingProvider bp = (BindingProvider) spt;
		try{
		Boolean LogSOAPMessageToConsole = true;
		if (LogSOAPMessageToConsole) {
			@SuppressWarnings("rawtypes")
			List<Handler> bb_handler = bp.getBinding().getHandlerChain();
			bb_handler.add(new SOAPLogging());
			bp.getBinding().setHandlerChain(bb_handler);
		}
	    }
        catch (Exception ex) {
		   throw new AppError(ex);
	    }				
	}

	/**
	 * This method check the auth access privilege.
	 * 
	 * @param
	 * @exception
	 * @return UPSSecurity object
	 */
	private UPSSecurity accessCheck(String strDivisionID) throws AppError {
		UPSSecurity upsSecurity = new UPSSecurity();
		try{
			 log.debug("== accessCheck UPS  strDivisionID==="+strDivisionID);
		UPSSecurity.UsernameToken usernameToken = new com.ups.Ship.UPSSecurity.UsernameToken();
		usernameToken.setUsername(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_USERNAME"+"_"+strDivisionID))); // "gmedups"
		usernameToken.setPassword(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_PASSWORD"+"_"+strDivisionID))); // "Globus!234"
		log.debug("UPS_USERNAME.........." 	+ GmCommonClass.getShipCarrierString("UPS_USERNAME"));
		 log.debug("== accessCheck UPS  UPS_USERNAME==="+GmCommonClass.getShipCarrierString("UPS_USERNAME"+"_"+strDivisionID));
		 log.debug("== accessCheck UPS  UPS_PASSWORD==="+GmCommonClass.getShipCarrierString("UPS_PASSWORD"+"_"+strDivisionID));
		upsSecurity.setUsernameToken(usernameToken);
		com.ups.Ship.UPSSecurity.ServiceAccessToken accessToken = new com.ups.Ship.UPSSecurity.ServiceAccessToken();
		accessToken.setAccessLicenseNumber(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_LICENCENUMBER"+"_"+strDivisionID))); // "BCD39B76BDA6FB16"
		upsSecurity.setServiceAccessToken(accessToken);
	    }
        catch (Exception ex) {
		   throw new AppError(ex);
	    }		
		return upsSecurity;
	}

	/**
	 * This method validate city, state, and zip code.
	 * 
	 * @param
	 * @exception
	 * @return ShipmentRequest object
	 */
	private ShipmentRequest citystatezipValidate() throws AppError {
		ShipmentRequest shipRequest = new ShipmentRequest();
		RequestType requestType = new RequestType();
		try{
		TransactionReferenceType transactionReference = new TransactionReferenceType();
		transactionReference.setCustomerContext("GUID");
		requestType.setTransactionReference(transactionReference);
		List<String> requestOption = requestType.getRequestOption();
		requestOption.add("nonvalidate"); // This controls if the ShipAPI
											// performs City State Zip
											// validation before generating the
											// label. Values can be validate or
											// nonvalidate
		shipRequest.setRequest(requestType);
	    }
        catch (Exception ex) {
		   throw new AppError(ex);
	    }		
		return shipRequest;
	}

	/**
	 * This method set Negotiated Rates.
	 * 
	 * @param ShipmentType
	 *            shpmnt
	 * @exception
	 * @return
	 */
	private void setNegotiatedRates(ShipmentType shpmnt) throws AppError {
		RateInfoType RIT = new RateInfoType();
		try{
		RIT.setNegotiatedRatesIndicator("True"); // This will bring back negotiated rates in the Ship API response.
		shpmnt.setShipmentRatingOptions(RIT);
	    }
        catch (Exception ex) {
		   throw new AppError(ex);
	    }		
	}

	/**
	 * This method set shipping information, it includes 3 methods 1.
	 * setShipperInfo(shipper) 2. setShipToInfo(shipTo, gmTxnShipInfoVO); 3.
	 * setShipFromInfo(shipFrom);
	 * 
	 * @param String
	 *            strTransId,String strSoucre, GmTxnShipInfoVO gmTxnShipInfoVO
	 * @exception
	 * @return ShipmentType object
	 */
	private ShipmentType setShippingInfo(String strTransId, String strSoucre,
			GmTxnShipInfoVO gmTxnShipInfoVO) throws AppError {
		ShipmentType shpmnt = new ShipmentType();
		try{
		shpmnt.setDescription(strTransId + " * " + strSoucre);

		/** ********** International Shipment ******************** */
		// If the Shipment US to PR (Puerto Rico) then need to set the below
		// value.
		if (gmTxnShipInfoVO.getShipstate().equals(
				GmCommonClass.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_STATE_INTERNATIONAL"))))// PR
		{
			CurrencyMonetaryType cm = new CurrencyMonetaryType();
			cm.setMonetaryValue("1");
			cm.setCurrencyCode("USD");
			shpmnt.setInvoiceLineTotal(cm);
		}

		/** *******Shipper******************** */
		// The Shipper information is what will appear on the label as the Ship
		// From address
		ShipperType shipper = new ShipperType();

		setShipperInfo(shipper);

		shpmnt.setShipper(shipper);

		/** ************ShipTo****************** */
		ShipToType shipTo = new ShipToType();

		setShipToInfo(shipTo, gmTxnShipInfoVO);

		shpmnt.setShipTo(shipTo);

		/** ************ShipFrom****************** */

		ShipFromType shipFrom = new ShipFromType();

		setShipFromInfo(shipFrom);

		shpmnt.setShipFrom(shipFrom);
		}
        catch (Exception ex) {
			throw new AppError(ex);
		}
		return shpmnt;

	}

	/**
	 * This method set Return shipping information, it includes 3 methods 1.
	 * setRtnShipperInfo(shipper, gmTxnShipInfoVO) 2. setRtnShipToInfo(shipTo)
	 * 3. setRtnShipFromInfo(shipFrom, gmTxnShipInfoVO)
	 * 
	 * @param String
	 *            strTransId,String strSoucre, GmTxnShipInfoVO gmTxnShipInfoVO
	 * @exception
	 * @return ShipmentType object
	 */

	private ShipmentType setRtnShippingInfo(String strTransId,
			String strSoucre, GmTxnShipInfoVO gmTxnShipInfoVO) throws AppError {
		ShipmentType shpmnt = new ShipmentType();
		try{
		shpmnt.setDescription(strTransId + " * " + strSoucre);
		/** *******Shipper******************** */
		// The Shipper information is what will appear on the label as the Ship
		// From address
		ShipperType shipper = new ShipperType();

		setRtnShipperInfo(shipper, gmTxnShipInfoVO);

		shpmnt.setShipper(shipper);

		/** ************ShipTo****************** */
		ShipToType shipTo = new ShipToType();

		setRtnShipToInfo(shipTo);

		shpmnt.setShipTo(shipTo);

		/** ************ShipFrom****************** */

		ShipFromType shipFrom = new ShipFromType();

		setRtnShipFromInfo(shipFrom, gmTxnShipInfoVO);

		shpmnt.setShipFrom(shipFrom);
		}
        catch (Exception ex) {
			throw new AppError(ex);
		}
		return shpmnt;

	}

	/**
	 * This method check the printer name and call
	 * gmPrintService.printByteStream to print ZPL label
	 * 
	 * @param ShipmentResultsType
	 *            srt, String strUserId, HashMap hmShipInfo
	 * @exception Exception
	 * @return
	 */
	private void findPrinterandPrintZPL(ShipmentResultsType srt,
			String strUserId, HashMap hmShipInfo) throws Exception {
		List<PackageResultsType> packages = srt.getPackageResults();
		GmPrintService gmPrintService = new GmPrintService();
		gmPrintService.setPrinterName(GmCommonClass.getRuleValue(strUserId,"ZEBRA_PRINTER_UPS"));
		log.debug("Zebra Printer === "+ GmCommonClass.getRuleValue(strUserId, "ZEBRA_PRINTER_UPS"));
		String strTrackNum = GmCommonClass.parseNull((String) hmShipInfo.get("TRACKINGNO"));		
		for (int x = 0; x < packages.size(); x++) {
			String PackageTrackNum = packages.get(x).getTrackingNumber();
			if (strTrackNum.equals("")) {
				hmShipInfo.put("TRACKINGNO", PackageTrackNum);
			}
			byte[] decodedbase64byte = Base64Dec.decode(packages.get(x)
					.getShippingLabel().getGraphicImage());
			gmPrintService.printByteStream(decodedbase64byte);

		}
	}

	/**
	 * This method call method findPrinterandPrintZPL to print ZPL label
	 * 
	 * @param String
	 *            strUserId, HashMap hmShipInfo, ShipPortType spt,
	 *            ShipmentRequest shipRequest,UPSSecurity upsSecurity
	 * @exception Exception
	 * @return
	 */
	private void printShipLabelZPL(String strUserId, String LabelFormat,
			HashMap hmShipInfo, ShipPortType spt, ShipmentRequest shipRequest,
			UPSSecurity upsSecurity) throws AppError {
		try {
			ShipmentResponse shipResponse = spt.processShipment(shipRequest,
					upsSecurity);
			log.debug("PrintshiplabelResponse:" + shipResponse);
			String statusCode = shipResponse.getResponse().getResponseStatus().getCode();
			log.debug("statusCode:" + statusCode);

			ShipmentResultsType srt = shipResponse.getShipmentResults();

			if (LabelFormat.equalsIgnoreCase("ZPL")) {
				findPrinterandPrintZPL(srt, strUserId, hmShipInfo);
			} else {
				savePrintGIFFile(srt);
			}

		} catch (ShipmentErrorMessage SEM) {
			Errors errs = SEM.getFaultInfo();
			List<ErrorDetailType> errDetailList = errs.getErrorDetail();
			ErrorDetailType aError = errDetailList.get(0);

			CodeType primaryError = aError.getPrimaryErrorCode();

			String description = primaryError.getDescription();
			String statusCode = primaryError.getCode();
			String severity = aError.getSeverity(); // Severity will show if
													// this is transient error.
			log.debug("\nError Response: Severity=" + severity + " Code="
					+ statusCode + " Description=" + description);
			hmShipInfo.put("ERRMSG", description);

		} catch (Exception e) {
			e.printStackTrace();
			String description = e.getMessage();
			String statusCode = e.toString();
			log.debug("\nError: Code=" + statusCode + " Description="
					+ description);
			hmShipInfo.put("ERRMSG", description);
		}
	}

	/**
	 * This method set all shipper information
	 * 
	 * @param ShipperType
	 *            shipper
	 * @exception
	 * @return
	 */
	private void setShipperInfo(ShipperType shipper) throws AppError {
		/** *******Shipper******************** */
		// The Shipper information is what will appear on the label as the Ship
		// From address
		try{
		shipper.setName(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_COMPANY_NAME"))); // "Valley Forge Business Center"
		
		shipper.setAttentionName(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_ATTN_NAME"))); // "Globus Medical
															// Inc.
		ShipPhoneType shipperPhone = new ShipPhoneType();
		shipperPhone.setNumber(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_GLOBUS_PHONE")));// "6109301800"

		shipper.setPhone(shipperPhone);
		shipper.setShipperNumber(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_SHIP_NUMBER")));// "309R3V"

		ShipAddressType shipperAddress = new ShipAddressType();
		List<String> addressLineList = shipperAddress.getAddressLine();
		addressLineList.add(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_ADDR2"))); // "2560 General Armistead Avenue"
														// // Address line 1

		shipperAddress.setCity(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_CITY"))); // "Audubon"
		
		shipperAddress.setPostalCode(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_ZIP"))); // "19403"
		
		shipperAddress.setStateProvinceCode(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_STATE")));// "PA"
		
		shipperAddress.setCountryCode(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_COUNTRY")));// "US"
		
		shipper.setTaxIdentificationNumber(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_GLOBUS_TAXID")));// "04-3744954"
		shipper.setAddress(shipperAddress);
		}
        catch (Exception ex) {
			throw new AppError(ex);
		}		

	}

	/**
	 * This method set all ShipTo information
	 * 
	 * @param ShipToType
	 *            shipTo, GmTxnShipInfoVO gmTxnShipInfoVO
	 * @exception
	 * @return
	 */
	private void setShipToInfo(ShipToType shipTo,
			GmTxnShipInfoVO gmTxnShipInfoVO) throws AppError {
		try{
		ShipToAddressType shipToAddress = new ShipToAddressType();
		
		// This method set the ship to address values
		setShipToAddrVal(shipTo,shipToAddress,gmTxnShipInfoVO);
		
		shipToAddress.setCity(gmTxnShipInfoVO.getShipcity());//
		shipToAddress.setStateProvinceCode(gmTxnShipInfoVO.getShipstate());
		shipToAddress.setPostalCode(gmTxnShipInfoVO.getShipzip());
		if (gmTxnShipInfoVO.getShipstate().equals(
				GmCommonClass.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_STATE_INTERNATIONAL"))))// PR
		{
			shipToAddress.setCountryCode(GmCommonClass
					.parseNull((String) GmCommonClass
							.getShipCarrierString("UPS_STATEPR_CNTY_CODE")));// "PR"
		} else {
			shipToAddress.setCountryCode(gmTxnShipInfoVO.getShipcountry());
		}
		shipTo.setAddress(shipToAddress);
		shipTo.setTaxIdentificationNumber(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_TO_TAXID")));// "20559265527"
		}
        catch (Exception ex) {
			throw new AppError(ex);
		}		
	}

	/**
	 * This method set all ShipFrom information
	 * 
	 * @param ShipFromType
	 *            shipFrom
	 * @exception
	 * @return
	 */
	private void setShipFromInfo(ShipFromType shipFrom) throws AppError {
		try{
		shipFrom.setName(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_COMPANY_NAME"))); // ("Valley Forge Business Center");
		
		shipFrom.setAttentionName(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_ATTN_NAME"))); // ("Shipping");
		
		ShipAddressType shipFromAddress = new ShipAddressType();
		List<String> addressLineList_shipFrom = shipFromAddress
				.getAddressLine();
		addressLineList_shipFrom.add(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_ADDR2"))); // ("2560 General Armistead Avenue");
		
		shipFromAddress.setCity(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_CITY"))); // ("Audubon");
		
		shipFromAddress.setPostalCode(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_ZIP")));// ("19403");
		
		shipFromAddress.setStateProvinceCode(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_STATE")));// ("PA");
		
		shipFromAddress.setCountryCode(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_COUNTRY")));// ("US");
		
		shipFrom.setAddress(shipFromAddress);
		shipFrom.setTaxIdentificationNumber(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_GLOBUS_TAXID")));// ("04-3744954");
		
		TaxIDCodeDescType TIDtype = new TaxIDCodeDescType(); // Needed for EEI
		TIDtype.setCode("EIN"); // Neede for EEI
		shipFrom.setTaxIDType(TIDtype); // Needed for EEI
		}
        catch (Exception ex) {
			throw new AppError(ex);
		}		
	}

	/**
	 * This method set all Payment information
	 * 
	 * @param ShipFromType
	 *            shipFrom
	 * @exception
	 * @return
	 */
	private void setPaymentInfo(ShipmentType shpmnt, String strThirdPartyVal)
			throws AppError {
		try{
		PaymentInfoType payInfo = new PaymentInfoType();		
		ShipmentChargeType shpmntCharge = new ShipmentChargeType();
		shpmntCharge.setType("01"); // 01 indicates transportation, 02 indicates
									// Duties and Taxes.
		if (strThirdPartyVal.equals("")) {
			// Below is used for billing transportation costs to Shipper
			BillShipperType billShipper = new BillShipperType();
			billShipper.setAccountNumber(GmCommonClass
					.parseNull((String) GmCommonClass
							.getShipCarrierString("UPS_SHIP_NUMBER")));// ("309R3V");
																		// //
																		// Setup
																		// so it
																		// bills
																		// shipper
			// number for transportation
			// costs.
			shpmntCharge.setBillShipper(billShipper);
			// End bill transportation to Shipper
		} else {
			// Below is used for billing transportation costs to 3rd party
			// Shipper
			BillThirdPartyChargeType bill3rdParty = new BillThirdPartyChargeType();
			bill3rdParty.setAccountNumber(strThirdPartyVal);
			AccountAddressType thirdpartyaddress = new AccountAddressType();
			thirdpartyaddress.setPostalCode(GmCommonClass
					.parseNull((String) GmCommonClass
							.getShipCarrierString("UPS_ZIP"))); // ("19403")
			
			thirdpartyaddress.setCountryCode(GmCommonClass
					.parseNull((String) GmCommonClass
							.getShipCarrierString("UPS_COUNTRY")));// ("US");
			
			bill3rdParty.setAddress(thirdpartyaddress);
			shpmntCharge.setBillThirdParty(bill3rdParty);
			// End Bill transportation to 3rd Party Shipper.
		}
		ShipmentChargeType[] shpmntChargeArray = { shpmntCharge };
		payInfo.getShipmentCharge().add(shpmntChargeArray[0]);

		shpmnt.setPaymentInformation(payInfo);
		}
        catch (Exception ex) {
			throw new AppError(ex);
		}		
	}

	/**
	 * This method set all Package information
	 * 
	 * @param ShipmentType
	 *            shpmnt, String strToteWeight, String strTransId, String
	 *            strSoucre
	 * @exception
	 * @return
	 */
	private void setPackage(ShipmentType shpmnt,
			GmTxnShipInfoVO gmTxnShipInfoVO, String strToteWeight,
			String strTransId, String strSoucre) throws AppError {
		try{
		List<PackageType> pkgtype_list = shpmnt.getPackage();
		PackageType pkg = new PackageType();
		PackagingType pkgingType = new PackagingType();
		pkgingType.setCode("02"); // Packing Type of 02 means customer
									// supplied box
		pkgingType.setDescription("Misc Materials");
		pkg.setPackaging(pkgingType);

		DimensionsType dimensionsType = new DimensionsType();
		ShipUnitOfMeasurementType unitOfMeas = new ShipUnitOfMeasurementType();
		unitOfMeas.setCode("IN"); // US Domestic uses inches for dimensions
		unitOfMeas.setDescription("Inches");
		dimensionsType.setUnitOfMeasurement(unitOfMeas);
		dimensionsType.setLength("3");
		dimensionsType.setWidth("3");
		dimensionsType.setHeight("3");
		pkg.setDimensions(dimensionsType);

		PackageWeightType weight = new PackageWeightType();
		weight.setWeight(strToteWeight.equals("0") ? "5" : strToteWeight);
		ShipUnitOfMeasurementType shpUnitOfMeas = new ShipUnitOfMeasurementType();
		shpUnitOfMeas.setCode("LBS");
		shpUnitOfMeas.setDescription("Pounds");
		weight.setUnitOfMeasurement(shpUnitOfMeas);
		pkg.setPackageWeight(weight);
		pkg.setDescription(strTransId + " * " + strSoucre);

		if (!gmTxnShipInfoVO.getShipstate().equals(
				GmCommonClass.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_STATE_INTERNATIONAL"))))// PR
		{
			ReferenceNumberType ref1 = new ReferenceNumberType();
			ref1.setValue(strTransId + " * " + strSoucre);
			pkg.getReferenceNumber().add(ref1);
		}

		pkgtype_list.add(pkg);
		}
        catch (Exception ex) {
			throw new AppError(ex);
		}		
	}

	/**
	 * This method set Label specific information
	 * 
	 * @param String
	 *            LabelFormat,ShipmentRequest shipRequest,ShipmentType shpmnt
	 * @exception
	 * @return
	 */
	private void setLabelSpec(String LabelFormat, ShipmentRequest shipRequest,
			ShipmentType shpmnt) throws AppError {
		try{
		LabelSpecificationType labelSpecType = new LabelSpecificationType();
		LabelImageFormatType labelImageFormat = new LabelImageFormatType();
		labelImageFormat.setCode(LabelFormat);
		labelSpecType.setLabelImageFormat(labelImageFormat);
		if (labelImageFormat.getCode().equals("ZPL")) {
			LabelStockSizeType LSST = new LabelStockSizeType();
			LSST.setHeight("6");
			LSST.setWidth("4");
			labelSpecType.setLabelStockSize(LSST);
		}
		labelSpecType.setHTTPUserAgent("Mozilla/4.5");

		shipRequest.setLabelSpecification(labelSpecType);

		shipRequest.setShipment(shpmnt);
		}
		catch (Exception ex) {
			throw new AppError(ex);
		}

	}

	/**
	 * This method set all Return Shipper information
	 * 
	 * @param ShipperType
	 *            shipper, GmTxnShipInfoVO gmTxnShipInfoVO
	 * @exception
	 * @return
	 */
	private void setRtnShipperInfo(ShipperType shipper,
			GmTxnShipInfoVO gmTxnShipInfoVO) throws AppError {
		try{
		shipper.setName(GmCommonClass.parseNull(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_COMPANY_NAME"))));
		
		shipper.setAttentionName(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_ATTN_NAME")));
		
		shipper.setShipperNumber(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_SHIP_NUMBER")));// ("309R3V");

		String strPhoneNum = (GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_GLOBUS_PHONE")));
		
		if (!strPhoneNum.equals("")) {
			ShipPhoneType shipperPhone = new ShipPhoneType();
			shipperPhone.setNumber(strPhoneNum);
			shipper.setPhone(shipperPhone);
		}
		
		ShipAddressType shipperAddress = new ShipAddressType();

		/*************** Return Service Shipper Address***************** */
		setRtnShipperAddress(gmTxnShipInfoVO, shipperAddress);

		shipper.setTaxIdentificationNumber(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_TO_TAXID")));// "20559265527"
		shipper.setAddress(shipperAddress);
		}
		catch (Exception ex) {
			throw new AppError(ex);
		}
	}

	/**
	 * This method set Return Shipper Address information
	 * 
	 * @param gmTxnShipInfoVO
	 *            ,shipperAddress
	 */
	private void setRtnShipperAddress(GmTxnShipInfoVO gmTxnShipInfoVO,
			ShipAddressType shipperAddress) throws AppError {
		try{
		List<String> addressLineList = shipperAddress.getAddressLine();

			addressLineList.add(GmCommonClass.parseNull((String) GmCommonClass
					.getShipCarrierString("UPS_ADDR2")));// ("2560 General Armistead Avenue");
															// // Address
			shipperAddress.setCity(GmCommonClass
					.parseNull((String) GmCommonClass
							.getShipCarrierString("UPS_CITY")));// Audubon
			
			shipperAddress.setStateProvinceCode(GmCommonClass
					.parseNull((String) GmCommonClass
							.getShipCarrierString("UPS_STATE")));// PA
			
			shipperAddress.setPostalCode(GmCommonClass
					.parseNull((String) GmCommonClass
							.getShipCarrierString("UPS_ZIP")));// 19403
			
			shipperAddress.setCountryCode(GmCommonClass
					.parseNull((String) GmCommonClass
							.getShipCarrierString("UPS_COUNTRY")));// US
		}
		catch (Exception ex) {
			throw new AppError(ex);
		}
		
	}

	/**
	 * This method set all Return ShipTo information
	 * 
	 * @param ShipToType
	 *            shipTo
	 * @exception
	 * @return
	 */
	private void setRtnShipToInfo(ShipToType shipTo) throws AppError {
		try{
		shipTo.setName(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_COMPANY_NAME")));
		
		shipTo.setAttentionName(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_ATTN_NAME")));

		ShipPhoneType shipToPhone = new ShipPhoneType();
		shipToPhone.setNumber(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_GLOBUS_PHONE")));// ("6109301800");
		shipTo.setPhone(shipToPhone);

		ShipToAddressType shipToAddress = new ShipToAddressType();
		List<String> ShipToAddressLineList = shipToAddress.getAddressLine();

		ShipToAddressLineList.add(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_ADDR2")));// ("2560 General Armistead Avenue");
																// // Address
		// Line 2
		shipToAddress.setCity(GmCommonClass.parseNull((String) GmCommonClass
				.getShipCarrierString("UPS_CITY")));// ("Audubon")
		
		shipToAddress.setStateProvinceCode(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_STATE"))); // ("PA")
		
		shipToAddress.setPostalCode(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_ZIP"))); // ("19403")
		
		shipToAddress.setCountryCode(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_COUNTRY"))); // ("US")
		
		shipTo.setAddress(shipToAddress);
		shipTo.setTaxIdentificationNumber(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_GLOBUS_TAXID")));// ("20559265527");
		}
		catch (Exception ex) {
			throw new AppError(ex);
		}
	}

	/**
	 * This method set all Return ShipFrom information
	 * 
	 * @param ShipperType
	 *            shipper, GmTxnShipInfoVO gmTxnShipInfoVO
	 * @exception
	 * @return
	 */
	private void setRtnShipFromInfo(ShipFromType shipFrom,
			GmTxnShipInfoVO gmTxnShipInfoVO) throws AppError {
		try{
		
		ShipAddressType shipFromAddress = new ShipAddressType();
		// This method set the from address value for return service. 
		setRtnShipFromAddrVal(shipFrom,gmTxnShipInfoVO,shipFromAddress);
		
		shipFromAddress.setCity(gmTxnShipInfoVO.getShipcity());
		shipFromAddress.setPostalCode(gmTxnShipInfoVO.getShipzip());
		shipFromAddress.setStateProvinceCode(gmTxnShipInfoVO.getShipstate());
		if (gmTxnShipInfoVO.getShipstate().equals(
				GmCommonClass.parseNull((String) GmCommonClass.getShipCarrierString("UPS_STATE_INTERNATIONAL"))))// PR
		{
			shipFromAddress.setCountryCode(GmCommonClass
					.parseNull((String) GmCommonClass
							.getShipCarrierString("UPS_STATEPR_CNTY_CODE")));// PR
		} else {
			shipFromAddress.setCountryCode(gmTxnShipInfoVO.getShipcountry());
		}
		shipFrom.setAddress(shipFromAddress);
		shipFrom.setTaxIdentificationNumber(GmCommonClass
				.parseNull((String) GmCommonClass
						.getShipCarrierString("UPS_TO_TAXID")));// ("04-3744954");
		TaxIDCodeDescType TIDtype = new TaxIDCodeDescType(); // Needed for EEI
		TIDtype.setCode("EIN"); // Neede for EEI
		shipFrom.setTaxIDType(TIDtype); // Needed for EEI
		}
		catch (Exception ex) {
			throw new AppError(ex);
		}

	}

	/**
	 * This method set Return service, this need to show RS symbol on the return
	 * label
	 * 
	 * @param ShipmentType
	 *            shpmnt
	 * @exception
	 * @return
	 */
	private void setRtnService(ShipmentType shpmnt) throws AppError {
		try {
			ReturnServiceType RServ = new ReturnServiceType();
			RServ.setCode(GmCommonClass.parseNull((String) GmCommonClass
					.getShipCarrierString("UPS_RETURN_SERV_TYPE")));// 9
			shpmnt.setReturnService(RServ);

			// Return service Mode
			ServiceType service = new ServiceType();
			service.setCode(GmCommonClass.parseNull((String) GmCommonClass
					.getShipCarrierString("UPS_RETURN_SERV_MODE")));// 01
			shpmnt.setService(service);
		} catch (Exception ex) {
			throw new AppError(ex);
		}
	}

	/**
	 * This method set UPS Service with globus ship mode which from rule table
	 * UPS 2nd Day Air 2 Day Air 5007 02 UPS 2nd Day Air Freight 5009 02 UPS
	 * Next Day Air� Early A.M. Early AM 5003 14 UPS Next Day Air Priority
	 * Overnight 5004 01 UPS Next Day Air Saturday-Prio Over 5042 01 UPS Next
	 * Day Air Saturday 5006 01 UPS Next Day Air Saver Standard Overnight 5032
	 * 13 UPS Ground Ground 5008 03 UPS Worldwide Expedited International
	 * Economy 5034 08 UPS Worldwide Express International Priority 5033 07
	 * 
	 * @param GmTxnShipInfoVO
	 *            gmTxnShipInfoVO, String strCarrier, ShipmentType shpmnt
	 * @exception
	 * @return
	 */
	private void setServiceType(GmTxnShipInfoVO gmTxnShipInfoVO,
			String strCarrier, ShipmentType shpmnt) throws AppError

	{
		ServiceType service = new ServiceType();
		String strServiceType = gmTxnShipInfoVO.getShipmode();
		try {
			String strRuleServiceType = GmCommonClass.getRuleValue(
					strServiceType, strCarrier);
			String saturdayDelivery = GmCommonClass
					.parseNull((String) GmCommonClass
							.getShipCarrierString("UPS_SATURDAY_DELIVERY"));          
	
			log.debug("strCarrier :=" + strCarrier);
			log.debug("strServiceType :=" + strServiceType);
			log.debug("strRuleServiceType :=" + strRuleServiceType);			
			if (!strRuleServiceType.equals("")) {
				service.setCode(strRuleServiceType);
				//Saturday Delivery Indicator
				StringTokenizer sToken = new StringTokenizer(saturdayDelivery,",");	            
	            while(sToken.hasMoreElements()){
	            	saturdayDelivery = sToken.nextToken();
	            	if (strServiceType.equals(saturdayDelivery)) {
						ShipmentServiceOptions shipService = new ShipmentServiceOptions();
						log.debug("Entered Saturday Delivery");
						shipService.setSaturdayDeliveryIndicator("");					
						shpmnt.setShipmentServiceOptions(shipService);
					}
	            }				
			} else {
				throw new AppError("Please enter a valid shipmode.", "", 'E');
			}
		} catch (Exception ex) {
			log.error("error :" + ex.getMessage());
			throw new AppError("Please enter a valid shipmode.", "", 'E');
		}
		shpmnt.setService(service);
	}

	/**
	 * This method set Shipment values
	 * 
	 * @param HashMap
	 *            hmShipInfo,GmTxnShipInfoVO gmTxnShipInfoVO,ShipmentType
	 *            shpmnt,String strTransId,String strSoucre
	 * @exception
	 * @return
	 */
	private void setShipmentVal(HashMap hmShipInfo,
			GmTxnShipInfoVO gmTxnShipInfoVO, ShipmentType shpmnt,
			String strTransId, String strSoucre) throws AppError {
		String strToteWeight = "";		
		String strThirdPartyAcc = "";
		String strThirdPartyVal = "";
		strToteWeight = GmCommonClass.parseNull((String) hmShipInfo.get("TOTEWEIGHT"));		
		strThirdPartyAcc = GmCommonClass.parseNull(gmTxnShipInfoVO.getAcctpayorno());
		if (strThirdPartyAcc.length() == 6 && strSoucre.equals("Orders")) {
			strThirdPartyVal = strThirdPartyAcc;
		}
		try{

		/*************** Payment Information***************** */
		setPaymentInfo(shpmnt, strThirdPartyVal);

		/*** Show negotiated Rates ***/
		setNegotiatedRates(shpmnt);

		/******************** Package***************** */
		setPackage(shpmnt, gmTxnShipInfoVO, strToteWeight, strTransId,strSoucre);
		}
		catch (Exception ex) {
			throw new AppError(ex);
		}

	}

	/**
	 * This method save the UPS Print lable as Image
	 * 
	 * @param ShipmentType
	 *            shpmnt
	 * @throws IOException
	 * @exception
	 * @return
	 */
	private void savePrintGIFFile(ShipmentResultsType srt) throws AppError,
			IOException {
		List<PackageResultsType> packages = srt.getPackageResults();
		String labelLocation = "C://UPS_LABEL_PRINT/";
		String shipUPSLabelFileName = "";
		String PackageTrackNum = "";
		for (int x = 0; x < packages.size(); x++) {
			PackageTrackNum = packages.get(x).getTrackingNumber();
			byte[] decodedbase64byte = Base64Dec.decode(packages.get(x)
					.getShippingLabel().getGraphicImage());
			log.debug("UPS labelLocation " + labelLocation);
			
			shipUPSLabelFileName = labelLocation + PackageTrackNum + ".gif";
			File shipUPSFile = new File(shipUPSLabelFileName);
			java.io.OutputStream FW = new FileOutputStream(shipUPSFile);
			FW.write(decodedbase64byte);
			FW.close();
			log.debug("\nAssociated shipment label file name "+ shipUPSFile.getAbsolutePath());
			Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+ shipUPSFile.getAbsolutePath());
		}
	}
	
	/**
	 * This method set the ship to address values
	 * @param shipTo , shipToAddress , gmTxnShipInfoVO
	 */
	private void  setShipToAddrVal(ShipToType shipTo,ShipToAddressType shipToAddress,GmTxnShipInfoVO gmTxnShipInfoVO) throws AppError
	{		
		String strShipAttnName = GmCommonClass.parseNull(gmTxnShipInfoVO.getAttnto());		
		String strDefaultAttnName = GmCommonClass.parseNull(GmCommonClass
				.getShipCarrierString("UPS_SHIPTO_ATTN_NAME"));
		String strPhoneNum = GmCommonClass.parseNull(gmTxnShipInfoVO.getPh_no());
		strPhoneNum = strPhoneNum.trim(); // If the value came space, we handled it. 
        String strShiptoName = GmCommonClass.parseNull(gmTxnShipInfoVO.getShip_to_nm());
        String strAddress1 = GmCommonClass.parseNull(gmTxnShipInfoVO.getAddr1());
        String strAddress2 = GmCommonClass.parseNull(gmTxnShipInfoVO.getAddr2());
        String strAddrLength = GmCommonClass.parseNull(GmCommonClass.getShipCarrierString("UPS_SHIPTO_ADDR_LENGTH"));
        String strShipState = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipstate());
        String strAddress3 = "";
        String stTempValueHolder = "";          
        int addrLengthSize = 35;  
        
        if (!strAddrLength.equals("")) {
			addrLengthSize = Integer.parseInt(strAddrLength);
		}
        try{
        //Set Ship To Attention value 	
        stTempValueHolder = trimString(strDefaultAttnName.concat(strShipAttnName),addrLengthSize);	
        shipTo.setAttentionName(stTempValueHolder);
     
		if (!strPhoneNum.equals("")) {
			ShipPhoneType shipFromPhone = new ShipPhoneType();
			shipFromPhone.setNumber(strPhoneNum);
			shipTo.setPhone(shipFromPhone);
		}
		
		List<String> ShipToAddressLineList = shipToAddress.getAddressLine();
        stTempValueHolder = trimString(strShiptoName,addrLengthSize);        
		shipTo.setName(GmCommonClass.parseNull(stTempValueHolder));
		
		strAddress1 =   getExtraChar(strShiptoName,addrLengthSize) + " "+  strAddress1 ;
		stTempValueHolder = trimString(strAddress1,addrLengthSize);		
		if (!strAddress1.equals("")) {
			ShipToAddressLineList.add(stTempValueHolder);
		}	
		
		strAddress2 = getExtraChar(strAddress1,addrLengthSize) + " "+  strAddress2 ;
		stTempValueHolder = trimString(strAddress2,addrLengthSize);		
		if (!strAddress2.equals("")) {
			ShipToAddressLineList.add(stTempValueHolder);
		}		
		
		strAddress3 = getExtraChar(strAddress2,addrLengthSize);
		stTempValueHolder = trimString(strAddress3,addrLengthSize);		
		if (!strAddress3.equals("")) {
			ShipToAddressLineList.add(stTempValueHolder);
		}
		
		stTempValueHolder = getExtraChar(strAddress3,addrLengthSize); 
		if (!stTempValueHolder.equals("")) {
		throw new AppError("This address is not for UPS.Please use FEDEX", "", 'E');
		}
        }
        catch (Exception ex) {
			throw new AppError(ex);
		}		
	}
	
	/**
	 * This method set the From Address values for Return service 
	 * @param shipFrom , gmTxnShipInfoVO ,shipFromAddress
	 */
	
	private void  setRtnShipFromAddrVal(ShipFromType shipFrom, GmTxnShipInfoVO gmTxnShipInfoVO,ShipAddressType shipFromAddress) throws AppError
	{		
		String strShipAttnName = GmCommonClass.parseNull(GmCommonClass.parseNull(gmTxnShipInfoVO.getAttnto()));
		String strPhoneNum = GmCommonClass.parseNull(gmTxnShipInfoVO.getPh_no());
		strPhoneNum = strPhoneNum.trim(); // If the value came space, we handled it.
        String strShipfromName  = GmCommonClass.parseNull(gmTxnShipInfoVO.getShip_to_nm());
        String strAddress1 = GmCommonClass.parseNull(gmTxnShipInfoVO.getAddr1());
        String strAddress2 = GmCommonClass.parseNull(gmTxnShipInfoVO.getAddr2());
        String strAddrLength = GmCommonClass.parseNull(GmCommonClass.getShipCarrierString("UPS_RET_SHIPFROM_ADDR_LENGTH"));       
        int addrLengthSize = 30; 
        
		if (!strAddrLength.equals("")) {
			addrLengthSize = Integer.parseInt(strAddrLength);
		}
		
		String strAddress3 = "";
	    String stTempValueHolder = "";    
        try{
		if (!strShipAttnName.equals("")) {
			stTempValueHolder = trimString(strShipAttnName, addrLengthSize);
			shipFrom.setAttentionName(stTempValueHolder);
		}		
				
		if (!strPhoneNum.equals("")) {
			ShipPhoneType shipFromPhone = new ShipPhoneType();
			shipFromPhone.setNumber(strPhoneNum);
			shipFrom.setPhone(shipFromPhone);
		}
		
		List<String> ShipToAddressLineList = shipFromAddress.getAddressLine();
        stTempValueHolder = trimString(strShipfromName,addrLengthSize);        
        shipFrom.setName(GmCommonClass.parseNull(stTempValueHolder));
		
        strAddress1 =   getExtraChar(strShipfromName,addrLengthSize) + " "+  strAddress1 ;
		stTempValueHolder = trimString(strAddress1,35);		
		if (!strAddress1.equals("")) {
			ShipToAddressLineList.add(stTempValueHolder);
		}
		
		strAddress2 = getExtraChar(strAddress1,addrLengthSize) + " "+  strAddress2 ;
		stTempValueHolder = trimString(strAddress2,addrLengthSize);	
		if (!strAddress2.equals("")) {
			ShipToAddressLineList.add(stTempValueHolder);
		}
		
		strAddress3 = getExtraChar(strAddress2,35);
		stTempValueHolder = trimString(strAddress3,addrLengthSize);		
		if (!strAddress3.equals("")) {
			ShipToAddressLineList.add(stTempValueHolder);
		}
		
		//Validation need then use the below code.
		/*stTempValueHolder = getExtraChar(strAddress3,addrLengthSize); 
		if (!stTempValueHolder.equals("")) {
		    throw new AppError("This address is not for UPS.Please use FEDEX", "", 'E');
		}*/			
        }
        catch (Exception ex) {
			throw new AppError(ex);
		}
	}	
	
	/**
	 * This method returns the String value from 1 to  passed length value
	 * @param str, size	 
	 * @return
	 */
	private String trimString(String str,int size) throws AppError
	{
		if (!str.equals("") && str.length() > size) {
			str = str.substring(0, size);
		}		
		return str;
	}
	
	/**
	 * This method returns the String value - exceed passed length
	 * @param str, size
	 * @return
	 */
	private String getExtraChar(String str,int size) throws AppError
	{
		String ret = "";
		if (!str.equals("") && str.length() > size) {
			ret = str.substring(size, str.length());
		}		
		return ret;
	}	
	
}

