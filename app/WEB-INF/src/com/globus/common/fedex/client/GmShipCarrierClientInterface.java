package com.globus.common.fedex.client;
import java.util.HashMap;

/**
 * @author Xun
 * Interface created for write method to print the ship label for FEDEX and UPS.
 *
 */
 
public interface GmShipCarrierClientInterface 
	 
{
	public HashMap printLabel (HashMap hmShipInfo);

	public HashMap printShipLabel(HashMap hmShipInfo, String strToCountryCode, String strToShipState);

	public HashMap printReturnLabel(HashMap hmShipInfo, String strToCountryCode, String strToShipState);

}
