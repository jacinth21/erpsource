/* 
 ** 
 ** Filename: JAXBQVClient.java 
** Authors: United Parcel Service of America
 ** 
 ** The use, disclosure, reproduction, modification, transfer, or transmittal 
 ** of this work for any purpose in any form or by any means without the 
 ** written permission of United Parcel Service is strictly prohibited. 
 ** 
 ** Confidential, Unpublished Property of United Parcel Service. 
 ** Use and Distribution Limited Solely to Authorized Personnel. 
 ** 
 ** Copyright 2009 United Parcel Service of America, Inc.  All Rights Reserved. 
 ** 
 */
package com.globus.common.fedex.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.ups.Tracking.QuantumViewRequest;
import com.ups.Tracking.Request;
import com.ups.Tracking.Access.*;

public class GmUPSTrackingClient {
	  Logger log = GmLogger.getInstance(this.getClass().getName());
    
    public void generateUPSTrackingFile() {    
    		
		StringWriter strWriter = null;
		String strResults = "";
        try {	    
        	
        	//Create JAXBContext and marshaller for AccessRequest object        			
        	JAXBContext accessRequestJAXBC = JAXBContext.newInstance(com.ups.Tracking.Access.AccessRequest.class);
        	Marshaller accessRequestMarshaller = accessRequestJAXBC.createMarshaller();
			ObjectFactory accessRequestObjectFactory = new ObjectFactory();
			AccessRequest  accessRequest = accessRequestObjectFactory.createAccessRequest();
			populateAccessRequest(accessRequest);
			 
			//Create JAXBContext and marshaller for QuantumViewRequest object
			JAXBContext qvRequestJAXBC = JAXBContext.newInstance(com.ups.Tracking.QuantumViewRequest.class);	            
			Marshaller qvRequestMarshaller = qvRequestJAXBC.createMarshaller();
			com.ups.Tracking.ObjectFactory requestObjectFactory = new com.ups.Tracking.ObjectFactory();
			QuantumViewRequest qvRequest = requestObjectFactory.createQuantumViewRequest();
			
			populateQVRequest(qvRequest);	
			
			//Get String out of access request and qv request objects.
			strWriter = new StringWriter();       		       
			accessRequestMarshaller.marshal(accessRequest, strWriter);
			qvRequestMarshaller.marshal(qvRequest, strWriter);
			strWriter.flush();
			strWriter.close();
			
			strResults =contactService(strWriter.getBuffer().toString());
			
			//The xml data getting from strResults will be updated in the xml file
			updateResultsToFile(strResults);		   
        } catch (Exception e) {
			updateResultsToFile(e.toString());
			e.printStackTrace();
		} finally{
			try{
				if(strWriter != null){
					strWriter.close();
					strWriter = null;
				}
			}catch (Exception e) {
				updateResultsToFile(e.toString());
				e.printStackTrace();
			}
		}
    }    
    
	private  String contactService(String strXmlInputString) throws Exception{		
		String outputStr = null;
		OutputStream outputStream = null;
		try {
			URL url = new URL(GmCommonClass.parseNull((String) GmCommonClass.getShipCarrierString("UPS_TRACK_ENDPOINT")));
			
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			log.debug("Client established connection with " + url.toString());
			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			
			outputStream = connection.getOutputStream();		
			outputStream.write(strXmlInputString.getBytes());
			outputStream.flush();
			outputStream.close();
			log.debug("Http status = " + connection.getResponseCode() + " " + connection.getResponseMessage());
			
			outputStr = readURLConnection(connection);			
		} catch (Exception e) {
			log.debug("Error sending data to server");
			throw e;
		} finally {						
			if(outputStream != null){
				outputStream.close();
				outputStream = null;
			}
		}		
		return outputStr;
	}
	
	/**
	 * This method read all of the data from a URL connection to a String
	 * @param uc
	 */

	public String readURLConnection(URLConnection uc) throws Exception {
		StringBuffer buffer = new StringBuffer();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			int letter = 0;			
		
			while ((letter = reader.read()) != -1){
				buffer.append((char) letter);
			}
			reader.close();
		} catch (Exception e) {
			log.debug("Could not read from URL: " + e.toString());
			throw e;
		} finally {
			if(reader != null){
				reader.close();
				reader = null;
			}
		}
		return buffer.toString();
	}

    /**
     * Populates the access request object.
     * @param accessRequest
     */
    private void populateAccessRequest(com.ups.Tracking.Access.AccessRequest accessRequest){
    	log.debug("populateAccessRequest: ");
    	//Get the UPS username, password and license number from the ShippingCarrier.properties file
    	String strPassword = GmCommonClass.parseNull((String) GmCommonClass.getShipCarrierString("UPS_PASSWORD"));
    	String strUserNm = GmCommonClass.parseNull((String) GmCommonClass.getShipCarrierString("UPS_USERNAME"));
    	String strLicenseNum = GmCommonClass.parseNull((String) GmCommonClass.getShipCarrierString("UPS_LICENCENUMBER"));
    	
    	accessRequest.setAccessLicenseNumber(strLicenseNum);
    	accessRequest.setUserId(strUserNm);
    	accessRequest.setPassword(strPassword);
    }
   
    /**
     * Populate QuantumViewRequest object
     * @param qvRequest
     */
    private void populateQVRequest(com.ups.Tracking.QuantumViewRequest qvRequest){   	

     	Request request = new Request();
     	request.setRequestAction("QVEvents");
     	qvRequest.setRequest(request);
             	
    }
    
    /**
     * This method updates the XOLTResult.xml file with the received status and description
     * @param strResponse
     */
    private void updateResultsToFile(String strResponse) throws AppError{
    	BufferedWriter bw = null;
    	try{
    		//Getting the name and of the file to be updated
    		File outFile = new File(GmCommonClass.parseNull((String) GmCommonClass.getShipCarrierString("UPS_TRACK_FILEPATH")));
    		//log.debug("Output file deletion status: " + outFile.delete());
    		outFile.createNewFile();    		
    		bw = new BufferedWriter(new FileWriter(outFile));
    		StringBuffer strBuf = new StringBuffer();
    		strBuf.append(strResponse);
    		bw.write(strBuf.toString());
    		bw.close();    		   
    		log.debug("Output file created:" + outFile.getAbsolutePath());
    	}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
				if (bw != null){
					bw.close();
					bw = null;
				}
			}catch (Exception e) {
				e.printStackTrace();
				throw new AppError (e);
			}			
		}		
    }
}