package com.globus.common.fedex.client;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fedex.track.stub.ClientDetail;
import com.fedex.track.stub.CompletedTrackDetail;
import com.fedex.track.stub.Notification;
import com.fedex.track.stub.NotificationSeverityType;
import com.fedex.track.stub.TrackDetail;
import com.fedex.track.stub.TrackIdentifierType;
import com.fedex.track.stub.TrackPackageIdentifier;
import com.fedex.track.stub.TrackPortType;
import com.fedex.track.stub.TrackReply;
import com.fedex.track.stub.TrackRequest;
import com.fedex.track.stub.TrackRequestProcessingOptionType;
import com.fedex.track.stub.TrackSelectionDetail;
import com.fedex.track.stub.TrackServiceLocator;
import com.fedex.track.stub.TrackStatusDetail;
import com.fedex.track.stub.TransactionDetail;
import com.fedex.track.stub.VersionId;
import com.fedex.track.stub.WebAuthenticationCredential;
import com.fedex.track.stub.WebAuthenticationDetail;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/** 
 * Demo of using the Track service with Axis 
 * to track a shipment.
 * <p>
 * com.fedex.track.stub is generated via WSDL2Java, like this:<br>
 * <pre>
 * java org.apache.axis.wsdl.WSDL2Java -w -p com.fedex.track.stub http://www.fedex.com/...../TrackService?wsdl
 * </pre>
 * 
 * This sample code has been tested with JDK 5 and Apache Axis 1.4
 */
/**
 * @author gopinathan
 *
 */
public class GmShipTrackingClient {
	protected Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/**This is the main method to check the shipment /tracking no is actually picked by FedEx carrier.
	 * @param strTrackingNumber
	 * @throws AppError
	 */
	public boolean trackFedExTrackingNumber(String strTrackingNumber) throws AppError {   
		boolean blShipped = false;
		//
	    TrackRequest request = new TrackRequest();

        request.setClientDetail(createClientDetail());
        request.setWebAuthenticationDetail(createWebAuthenticationDetail());
        //
        TransactionDetail transactionDetail = new TransactionDetail();
        transactionDetail.setCustomerTransactionId("java sample - Tracking Request"); //This is a reference field for the customer.  Any value can be used and will be provided in the response.
        request.setTransactionDetail(transactionDetail);
 
        //
        VersionId versionId = new VersionId("trck", 8, 0, 0);
        request.setVersion(versionId);
        //
        TrackSelectionDetail selectionDetail=new TrackSelectionDetail();
        TrackPackageIdentifier packageIdentifier=new TrackPackageIdentifier();
        packageIdentifier.setType(TrackIdentifierType.TRACKING_NUMBER_OR_DOORTAG);
        packageIdentifier.setValue(strTrackingNumber); // tracking number
        selectionDetail.setPackageIdentifier(packageIdentifier);
        request.setSelectionDetails(new TrackSelectionDetail[] {selectionDetail});
        TrackRequestProcessingOptionType processingOption=TrackRequestProcessingOptionType.INCLUDE_DETAILED_SCANS;
        request.setProcessingOptions(new TrackRequestProcessingOptionType[]{processingOption});

	    //
		try {
			// Initializing the service
			TrackServiceLocator service;
			TrackPortType port;
			//
			service = new TrackServiceLocator();
			updateEndPoint(service);
			port = service.getTrackServicePort();
		    //
			log.debug("--Calling Fedex Web service--");
			TrackReply reply = port.track(request); // This is the call to the web service passing in a request object and returning a reply object
			//
			if (isResponseOk(reply.getHighestSeverity())) // check if the call was successful
			{
				if(validateNotifications(reply.getNotifications())){
					log.debug("--calling printCompletedTrackDetail method --");
					blShipped = printCompletedTrackDetail(reply.getCompletedTrackDetails());
				}
			}else{
				Notification notification[] = reply.getNotifications();
				String strNotification = "";
				for(int i=0; i<notification.length; i++){
					log.debug("Notification : = "+ notification[i].getMessage());
					strNotification = strNotification + notification[i].getMessage() + " ";					
				}				
				strNotification = strNotification.equals("")?"Insufficient/Wrong parameters in call to FedEx service. Please refer http://us.spineit.net":strNotification;
				throw new AppError(strNotification, "", 'E');
			}
			log.debug("--FInal track status from main method == "+blShipped );
			//return blShipped;
		} catch (Exception e) {
		    e.printStackTrace();
		    throw new AppError(e);
		    
		} 
		return blShipped;
	}
	
	/**this method is used to check the completed Tracking detail
	 * @param ctd
	 * @return
	 */
	private boolean printCompletedTrackDetail(CompletedTrackDetail[] ctd){
		boolean blStatus = false;
		for (int i=0; i< ctd.length; i++) { // package detail information
			boolean cont=true;
			if(ctd[i].getNotifications()!=null){
				cont= validateNotifications(ctd[i].getNotifications());
			}
			if(cont){
				blStatus =checkTrackDetail(ctd[i].getTrackDetails());				
			}
		}
		log.debug("--the blStatus from the printCompletedTrackDetail method is --"+blStatus);
		return blStatus;
	}

	/**This method is used to check track detail
	 * @param td
	 * @return
	 */
	private boolean checkTrackDetail(TrackDetail[] td){
		for (int i=0; i< td.length; i++) {
			boolean cont=false;
			if(td[i].getNotification()!=null){
				cont = validateNotifications(td[i].getNotification());
			}
			if(cont){
				if(td[i].getStatusDetail()!=null){
					printStatusDetail(td[i].getStatusDetail());
					return true;
				}
				
			}
			}
		return false;
	}
	
	/**This method is used to print status detail. We need to confirm the shipment is picked using the status code.
	 * @param tsd
	 */
	private void printStatusDetail(TrackStatusDetail tsd){
		if(tsd!=null){
			log.debug(tsd.getCreationTime());
			log.debug("Code "+ tsd.getCode());
		}
	}
	
	/**This method is used to check whether the FedEx web service is executed successfully or not
	 * @param notificationSeverityType
	 * @return
	 */
	private boolean isResponseOk(NotificationSeverityType notificationSeverityType) {
		if (notificationSeverityType == null) {
			return false;
		}
		if (notificationSeverityType.equals(NotificationSeverityType.WARNING) ||
			notificationSeverityType.equals(NotificationSeverityType.NOTE)    ||
			notificationSeverityType.equals(NotificationSeverityType.SUCCESS)) {
			return true;
		}
 		return false;
	}


	/**This method is used to check whether the notification is valid or not
	 * @param n
	 * @return
	 */
	private boolean validateNotifications(Object n) {
		boolean cont=true;
		if(n!=null){
			Notification[] notifications=null;
			Notification notification=null;
			if(n instanceof Notification[]){
				notifications=(Notification[])n;
				if (notifications == null || notifications.length == 0) {
					log.debug("  No notifications returned");
				}
				for (int i=0; i < notifications.length; i++){
					//printNotification(notifications[i]);
					if(!success(notifications[i])){cont=false;}
				}
			}else if(n instanceof Notification){
				notification=(Notification)n;
				//printNotification(notification);
				if(!success(notification)){cont=false;}
			}

		}
		return cont;
	}

	
	/**This method is used to check whether the notification severity is valid or not
	 * @param notification
	 * @return
	 */
	private boolean success(Notification notification){
		Boolean cont = true;
		if(notification!=null){
			if(notification.getSeverity()==NotificationSeverityType.FAILURE || 
					notification.getSeverity()==NotificationSeverityType.ERROR){
				cont=false;
			}
		}
		
		return cont;
	}
	
	/**This method is used to set client detail from constant properties
	 * @return
	 */
	private ClientDetail createClientDetail() {
        ClientDetail clientDetail = new ClientDetail();
        String accountNumber = GmCommonClass.getShipCarrierString("FEDEX_ACCOUNTNUMBER");
        String meterNumber = GmCommonClass.getShipCarrierString("FEDEX_METERNUMBER");
        
        //
        // See if the accountNumber and meterNumber properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (accountNumber == null) {
        	accountNumber = "XXX"; // Replace "XXX" with clients account number
        }
        if (meterNumber == null) {
        	meterNumber = "XXX"; // Replace "XXX" with clients meter number
        }
        clientDetail.setAccountNumber(accountNumber);
        clientDetail.setMeterNumber(meterNumber);
        return clientDetail;
	}
	
	/**This method is used to set FedEx authentication detail from constant properties
	 * @return
	 */
	private WebAuthenticationDetail createWebAuthenticationDetail() {
        WebAuthenticationCredential wac = new WebAuthenticationCredential();
        String key =  GmCommonClass.getShipCarrierString("FEDEX_KEY");
        String password =  GmCommonClass.getShipCarrierString("FEDEX_PASSWORD");
        
        //
        // See if the key and password properties are set,
        // if set use those values, otherwise default them to "XXX"
        //
        if (key == null) {
        	key = "XXX"; // Replace "XXX" with clients key
        }
        if (password == null) {
        	password = "XXX"; // Replace "XXX" with clients password
        }
        wac.setKey(key);
        wac.setPassword(password);
		return new WebAuthenticationDetail(wac);
	}	
	/**This method is used to set FedEx web service URL from constant properties
	 * @param serviceLocator
	 */
	private void updateEndPoint(TrackServiceLocator serviceLocator) {
		String endPoint = GmCommonClass.getShipCarrierString("FEDEX_TRACK_ENDPOINT");
		if (endPoint != null) {
			serviceLocator.setTrackServicePortEndpointAddress(endPoint);
		}
	}

	/** UPS
	 * trackUPSTrackingNumber - This method is to check whether the tracking number is available in the xml file generated from UPS
	 * @param strTrackingNumber
	 * @return blShipped
	 * @throws AppError
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public boolean trackUPSTrackingNumber(String strTrackingNumber) throws AppError, SAXException, IOException, ParserConfigurationException { 
		
		boolean blShipped = false;
		String strXmlFileName = GmCommonClass.parseNull((String) GmCommonClass.getShipCarrierString("UPS_TRACK_FILEPATH"));//XML file name
		//Root element in the xml file--Origin --QuantumViewResponse
		String strXmlRoot = GmCommonClass.parseNull((String) GmCommonClass.getShipCarrierString("UPS_XML_ROOT"));//XML Root name
		
		try {
			File file = new File(strXmlFileName);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = (Document) db.parse(file);
			doc.getDocumentElement().normalize();
			
			NodeList nodeRoot = doc.getElementsByTagName(strXmlRoot);// Getting the root element from xml file
			
			blShipped = validateTrackingNumber(nodeRoot, strTrackingNumber);// For checking whether the tracking number is available in Origin event or not
			
		}catch (Exception e) {
		    e.printStackTrace();
		    throw new AppError(e);
		    
		} 	
		return blShipped;//If tracking number is there in Origin event, will return true
	}
	

	/** UPS
	 * validateTrackingNumber - This method is to check whether the tracking number is available in the xml file generated from UPS
	 * @param nodeRoot, strTrackingNumber
	 * @return blShipped
	 * @throws AppError
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public boolean validateTrackingNumber(NodeList nodeRoot, String strTrackingNumber) throws AppError, SAXException, IOException, ParserConfigurationException { 
	
		boolean blShipped = false;
		String strXmlTrackNum = "";
		for (int n = 0; n < nodeRoot.getLength(); n++) {
			Node nodeRootNode = nodeRoot.item(n);
			
			if (nodeRootNode.getNodeType() == Node.ELEMENT_NODE) {
				Element nodeRootNodeElem = (Element) nodeRootNode;
				//Tag element in the xml file, from where we need to check the availability tracking number
				NodeList nodeLst = nodeRootNodeElem.getElementsByTagName(GmCommonClass.parseNull((String) GmCommonClass.getShipCarrierString("UPS_TRACK_EVENT")));
				for (int s = 0; s < nodeLst.getLength(); s++) {
					Node fstNode = nodeLst.item(s);
					
					if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
						Element fstElmnt = (Element) fstNode;
						//Get the tracking number element
						NodeList fstNmElmntLst = fstElmnt.getElementsByTagName(GmCommonClass.parseNull((String) GmCommonClass.getShipCarrierString("UPS_TRACK_ELEMENT")));
						Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
						NodeList fstNm = fstNmElmnt.getChildNodes();
						strXmlTrackNum = ((Node) fstNm.item(0)).getNodeValue();//Set the tracking number from the xml file to a local variable
					}
					
					if(strXmlTrackNum.equals(strTrackingNumber)){
						blShipped = true;//If both tracking number are same, return true										
						break;//No need to compare further if tracking number is found in the xml file
					}					
				}
			}
		}
		return blShipped;
	}
	
}
