package com.globus.common.fedex.client;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.shipping.beans.GmShipTrackingInfoBean;

/* @author bgnanamani
 *
 */

public class GmUPSShipCompletion implements GmShipCompletionInterface{

	Logger log = GmLogger.getInstance(this.getClass().getName());
	public boolean complete(HashMap hmDetails) throws AppError, SAXException, IOException, ParserConfigurationException {
		GmUPSTrackingClient gmUPSTrackingClient = new GmUPSTrackingClient();
		GmShipTrackingClient gmShipTrackingClient = new GmShipTrackingClient();
		boolean btrackFl = true;
		String strTrackNum = GmCommonClass.parseNull((String)hmDetails.get("TRACKNUMBER"));
		String strShipMode = GmCommonClass.parseNull((String)hmDetails.get("SHIPMODE"));;
		if(strShipMode.equals("5000")){		
		btrackFl = gmShipTrackingClient.trackUPSTrackingNumber(strTrackNum);
		}
		return btrackFl;
	}

}