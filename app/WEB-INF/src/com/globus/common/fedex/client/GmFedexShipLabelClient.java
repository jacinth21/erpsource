package com.globus.common.fedex.client;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;

/** 
 * Sample code to call the FedEx Ship Service
 * <p>
 * com.fedex.ship.stub is generated via WSDL2Java, like this:<br>
 * <pre>
 * java org.apache.axis.wsdl.WSDL2Java -w -p com.fedex.ship.stub http://www.fedex.com/...../ShipService?wsdl
 * </pre>
 * 
 * This sample code has been tested with JDK 5 and Apache Axis 1.4
 */
//
//Sample code to call the FedEx Ship Service - Domestic MPS Express
//
public class GmFedexShipLabelClient implements GmShipCarrierClientInterface
{
Logger log = GmLogger.getInstance(this.getClass().getName());
	
	//private String strToCountryCode = "";
	
	/**This is the main method for generate FedEx Label
	 * @param hmParam
	 * @throws AppError
	 */
	public HashMap printLabel(HashMap hmShipInfo) 
	{
		GmFedexShipLabelIntClient gmFedexShipLabelIntClient = new GmFedexShipLabelIntClient();
		GmFedexShipLabelDomClient gmFedexShipLabelDomClient = new GmFedexShipLabelDomClient();
		GmFedexReturnLabelIntClient gmFedexReturnLabelIntClient = new GmFedexReturnLabelIntClient();
		GmFedexReturnLabelDomClient gmFedexReturnLabelDomClient = new GmFedexReturnLabelDomClient();
		HashMap hmReturn = new HashMap();
		HashMap hmReturnLbl = new HashMap();
		
		String strShipErrMsg = "";
		String strRtnErrMsg = "";
		String strPrintRtnLbl = "";
		String strRtnTrackNo = "";
		
		GmTxnShipInfoVO gmTxnShipInfoVO = new GmTxnShipInfoVO();
		gmTxnShipInfoVO = (GmTxnShipInfoVO)hmShipInfo.get("SHIPINFO");
		
		String strToShipState = GmCommonClass.parseNull(gmTxnShipInfoVO.getShipstate());
		String strToCountryCode =  GmCommonClass.parseNull(gmTxnShipInfoVO.getCountrycode());
		String strPrintFeddxLabel = GmCommonClass.parseNull((String)hmShipInfo.get("PRINTSHIPLABEL"));
		String strPrintRtnLabel = GmCommonClass.parseNull((String)hmShipInfo.get("PRINTRETURNLABEL"));
		
		log.debug("strToShipState==="+strToShipState);
		log.debug("strToCountryCode==="+strToCountryCode);
		
		if(strPrintFeddxLabel.equals("Y")){
			hmReturn = printShipLabel(hmShipInfo, strToCountryCode, strToShipState); // To print Fedex ship label
			strShipErrMsg = GmCommonClass.parseNull((String)hmReturn.get("ERRMSG"));
		}
		if(strPrintRtnLabel.equals("Y")){
			hmReturnLbl = printReturnLabel(hmShipInfo, strToCountryCode, strToShipState); // To print Fedex Return label
			strRtnErrMsg = GmCommonClass.parseNull((String)hmReturnLbl.get("ERRMSG"));
			strPrintRtnLbl = GmCommonClass.parseNull((String)hmReturnLbl.get("PRINTRETURNLABEL"));
			strRtnTrackNo= GmCommonClass.parseNull((String)hmReturnLbl.get("RETURNTRACKINGNO"));
		}
		hmReturn.put("PRINTRETURNLABEL", strPrintRtnLbl);
		hmReturn.put("RETURNTRACKINGNO", strRtnTrackNo);
		hmReturn.put("ERRMSG", strShipErrMsg + strRtnErrMsg);
		return hmReturn;
	}
	
	/**This is method is used to generate FedEx Ship Label
	 */
	public HashMap printShipLabel(HashMap hmShipInfo, String strToCountryCode, String strToShipState) 
	{
		GmFedexShipLabelIntClient gmFedexShipLabelIntClient = new GmFedexShipLabelIntClient();
		GmFedexShipLabelDomClient gmFedexShipLabelDomClient = new GmFedexShipLabelDomClient();
		HashMap hmReturn = new HashMap();
		
		/* PR - Puerto Rico, TX - Antonio*/  
		if((strToShipState.equals("PR")) && strToCountryCode.equals("US")) {
				hmReturn = gmFedexShipLabelIntClient.printFedExLabel(hmShipInfo);
		}else if (!strToCountryCode.equals("US") && !strToCountryCode.equals("")){
				hmReturn = gmFedexShipLabelIntClient.printFedExLabel(hmShipInfo);
		}else{
				hmReturn = gmFedexShipLabelDomClient.printFedExLabel(hmShipInfo);
		}
		return hmReturn;
	}
	
	/**This is method is used to generate FedEx Return Label
	 */
	public HashMap printReturnLabel(HashMap hmShipInfo, String strToCountryCode, String strToShipState) 
	{
		GmFedexReturnLabelIntClient gmFedexReturnLabelIntClient = new GmFedexReturnLabelIntClient();
		GmFedexReturnLabelDomClient gmFedexReturnLabelDomClient = new GmFedexReturnLabelDomClient();
		HashMap hmReturn = new HashMap();
		
		/* PR - Puerto Rico, TX - Antonio*/  
		if((strToShipState.equals("PR")) && strToCountryCode.equals("US")) {
				hmReturn = gmFedexReturnLabelIntClient.printReturnLabel(hmShipInfo);
		}else if (!strToCountryCode.equals("US") && !strToCountryCode.equals("")){
				hmReturn = gmFedexReturnLabelIntClient.printReturnLabel(hmShipInfo);
		}else{
				hmReturn = gmFedexReturnLabelDomClient.printReturnLabel(hmShipInfo);
		}
		return hmReturn;
	}
	
	
}