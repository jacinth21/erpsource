/**
 * FileName : GmRuleAction.java Description : Author : vprasath Date : Nov 21, 2013 Copyright :
 * Globus Medical Inc
 */
package com.globus.common.rule.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;

/**
 * @author vprasath
 * 
 */
public class GmRulePrintAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    GmBatchBean gmBatchBean = new GmBatchBean();
    GmJasperReport gmJasperReport = new GmJasperReport();
    HashMap hmJasperDetails = new HashMap();
    hmJasperDetails.put(
        "SUBREPORT_DIR",
        request.getSession().getServletContext()
            .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"))
            + "\\");
    ArrayList alConsequences = (ArrayList) request.getAttribute("CONSEQUENCES");
    String strUserId = GmCommonClass.parseNull((String) request.getAttribute("USERID"));

    HashMap hmValue = new HashMap();

    GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
    gmSearchCriteria.addSearchCriteria("HOLDTXN", "Y", GmSearchCriteria.EQUALS);
    gmSearchCriteria.query(alConsequences);

    if (alConsequences.size() > 0) {
      String strRuleUploadLocation = GmCommonClass.getString("RULEUPLOAD");
      String strPDFFileName = strRuleUploadLocation + "\\" + "GmRuleDisplay.pdf";
      log.debug("strPDFFileName::" + strPDFFileName);
      gmJasperReport.setRequest(request);
      gmJasperReport.setResponse(response);
      gmJasperReport.setJasperReportName("/GmRuleDisplay.jasper");
      gmJasperReport.setHmReportParameters(hmJasperDetails);

      gmJasperReport.setReportDataList(alConsequences);
      File newPDFFile = new File(strPDFFileName);
      if (newPDFFile.exists()) {
        newPDFFile.deleteOnExit();
      }
      gmJasperReport.exportJasperReportToPdf(strPDFFileName);
      hmValue.put("PDFFILENAME", strPDFFileName);
      hmValue.put("RULEID", strUserId);
      gmBatchBean.batchPrint(hmValue);
    } else {
      throw new AppError("", "20683");
    }
    return null;
  }


}
