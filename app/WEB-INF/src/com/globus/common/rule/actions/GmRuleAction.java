/**
 * FileName : GmRuleAction.java Description : Author : vprasath Date : Jun 9, 2009 Copyright :
 * Globus Medical Inc
 */
package com.globus.common.rule.actions;

import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.rule.beans.GmRuleEngineWrapper;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
public class GmRuleAction extends GmAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    log.error("inside the Rule Action");
    instantiate(request, response);
    String strTxn = geValuefromRequest(request, "RE_TXN");
    GmDataStoreVO gmTempDataStoreVO = null;
    // to assign the Data Store VO object to temp VO object.
    gmTempDataStoreVO = getGmDataStoreVO();
    // checking the company id is present - If not then to set the default company ID (GMAN)
    if (gmTempDataStoreVO.getCmpid().equals("")) {
      gmTempDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
    }
    // to update the Data store VO to temp Data store VO - getGmDataStoreVO --> gmTempDataStoreVO
    String strAjaxWrapperName = GmCommonClass.parseNull(request.getParameter("RE_AJAX_WRAPPER"));
    String strWrapperId = strAjaxWrapperName.equals("") ? strTxn : strAjaxWrapperName;
    String strAction = GmCommonClass.parseNull(request.getParameter("strOpt"));
    String strRequestor = GmCommonClass.parseNull(request.getParameter("requestor"));
    String strCompanyLocale = GmCommonClass.getCompanyLocale(gmTempDataStoreVO.getCmpid());
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    String strRuleEngineOn =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("RULE_ENGINE_ON"));
    log.debug("strRuleEngineOn: " + strRuleEngineOn);
    log.error("GmRuleAction:before rule engine if condition>>" + strRuleEngineOn);
    if (strRuleEngineOn.equals("YES")) {
      if (strAction.equals("RE_SHOWPICTURE")) {
        ArrayList alPictures = new ArrayList();
        GmRuleBean gmRuleBean = new GmRuleBean(gmTempDataStoreVO);
        String strRuleID = GmCommonClass.parseNull(request.getParameter("RULEID"));
        String strFileID = GmCommonClass.parseNull(request.getParameter("FILEID"));
        alPictures = gmRuleBean.fetchPictureMessages(strRuleID, strFileID);
        request.setAttribute("RESULT", alPictures);
      } else {
        log.error("GmRuleAction:inside else condition condition>>strWrapperId:" + strWrapperId);
        if (!strWrapperId.equals("")) {
          GmRuleEngineWrapper engineWrapper = GmRuleEngineWrapper.getInstance(strWrapperId);
          List al = engineWrapper.formatData(request);
          engineWrapper.executeRules(al, request);
          log.error("GmRuleAction:before rule engine if condition>> strRequestor:" + strRequestor);
          if (strRequestor.equals("Resource")) {
            log.error("GmRuleAction:inside rule engine if condition>> strRequestor:");
            ArrayList alConsequences = (ArrayList) request.getAttribute("CONSEQUENCES");
            response.setContentType("application/octet-stream");
            ObjectOutputStream objectOutputStream =
                new ObjectOutputStream(response.getOutputStream());
            objectOutputStream.writeObject(alConsequences);
            objectOutputStream.flush();
            objectOutputStream.close();

            return null;
          }
        }
      }
    }
    log.error("GmRuleAction: before findforward>>");
    return mapping.findForward(geValuefromRequest(request, "RE_FORWARD"));
  }

  private String geValuefromRequest(HttpServletRequest request, String strKey) {

    String strForwardName = GmCommonClass.parseNull((String) request.getAttribute(strKey));
    strForwardName =
        strForwardName.equals("") ? GmCommonClass.parseNull(request.getParameter(strKey))
            : strForwardName;
    log.debug("Forward name = " + strForwardName);
    log.error("Forward name = " + strForwardName);
    return strForwardName;
  }
}
