/**
 * FileName    : GmRuleAnalysisReportAction.java 
 * Description :
 * Author      : vprasath
 * Date        : Aug 20, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.rule.forms.GmRuleReportForm;

/**
 * @author vprasath
 *
 */
public class GmRuleAnalysisReportAction extends GmAction {
	
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		Logger log = GmLogger.getInstance(this.getClass().getName());
		GmRuleReportForm gmRuleReportForm = (GmRuleReportForm) form;
		gmRuleReportForm.loadSessionParameters(request);
		GmRuleBean gmRuleBean = new GmRuleBean();
		String strOpt = gmRuleReportForm.getStrOpt();		
		gmRuleReportForm.setAlreportType(GmCommonClass.getCodeList("ANART"));
		log.debug("Report:" + gmRuleReportForm.getAlreportType() );
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmRuleReportForm);
		log.debug("hmParam: " + hmParam);
		String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING")); 
		if(!strInputString.equals(""))
		{
			RowSetDynaClass rowSetDynaClass =gmRuleBean.fetchAnalysisRuleReport(hmParam);
			log.debug("rowSetDynaClass: "+ rowSetDynaClass.getRows());
			gmRuleReportForm.setReturnList(rowSetDynaClass.getRows());			
		}
		return mapping.findForward(strOpt);
	}
}
