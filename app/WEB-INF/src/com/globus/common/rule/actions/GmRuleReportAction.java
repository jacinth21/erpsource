package com.globus.common.rule.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.rule.forms.GmRuleReportForm;
import com.globus.logon.beans.GmLogonBean;

public class GmRuleReportAction extends GmAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		Logger log = GmLogger.getInstance(this.getClass().getName());
		GmRuleReportForm gmRuleReportForm = (GmRuleReportForm) form;
		gmRuleReportForm.loadSessionParameters(request);
		instantiate(request,response);
		GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
		GmRuleBean gmRuleBean = new GmRuleBean(getGmDataStoreVO());

		String strOpt = gmRuleReportForm.getStrOpt();
		String strUserId = gmRuleReportForm.getUserId();

		RowSetDynaClass rdResult = null;
		//RowSetDynaClass rdAnalysisResult = null;

		RowSetDynaClass rdConditionResult = null;
		RowSetDynaClass rdTransactionResult = null;

		HashMap hmParam = new HashMap();

		ArrayList alTxnType = new ArrayList();
		ArrayList alConsequence = new ArrayList();
		ArrayList alInitiatedBy = new ArrayList();
		ArrayList alStatus = new ArrayList();
		HashMap hmReturn = new HashMap();
		HashMap hmConsequences = new HashMap();
		HashMap hmRuleDetails = new HashMap();
		
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
		hmParam = GmCommonClass.getHashMapFromForm(gmRuleReportForm);
		log.debug(" hmParam. ..... " + hmParam);
		String strLoadRule = GmCommonClass.parseNull((String)hmParam.get("LOADRULE"));
		gmRuleReportForm.setLoadRule(strLoadRule);
		gmRuleReportForm.setHmParam(hmParam);
		if (strOpt.equals("reload")) {

			rdResult = gmRuleBean.fetchRulesReport(hmParam);
			log.debug(" rdPartResult.getRows().size() ..... " + rdResult.getRows().size());
			gmRuleReportForm.setReturnList(rdResult.getRows());
			log.debug("load rule = "+strLoadRule);
			if(strLoadRule.equals("true"))
			{
				strOpt = "load";
			}
		}

		if (strOpt.equals("summary")) {

			hmReturn = gmRuleBean.fetchRuleSummary(hmParam);

			hmRuleDetails = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("RULEDETAIL"));
			hmConsequences = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("CONSEQUENCES"));
			rdConditionResult = (RowSetDynaClass) hmReturn.get("CONDITION");
			rdTransactionResult = (RowSetDynaClass) hmReturn.get("TRANSACTION");

			gmRuleReportForm.setReturnConditionList(rdConditionResult.getRows());
			gmRuleReportForm.setReturnTransactionList(rdTransactionResult.getRows());
			gmRuleReportForm.setHmRuleDetails(hmRuleDetails);
			gmRuleReportForm.setHmConsequences(hmConsequences);

		}
		
		alTxnType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RLTXN"));

		gmRuleReportForm.setAlTransaction(alTxnType);
		alConsequence = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONSQ"));
		gmRuleReportForm.setAlConsequence(alConsequence);
		alStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RULST"));
		gmRuleReportForm.setAlStatus(alStatus);
		alInitiatedBy = GmCommonClass.parseNullArrayList(gmAccessControlBean.getPartyList("9",null));
		gmRuleReportForm.setAlInitiatedBy(alInitiatedBy);

		if (strOpt.equals("")) {
			strOpt = "load";
		}
		log.debug("strOpt  = "+strOpt);
		return mapping.findForward(strOpt);
	}

}
