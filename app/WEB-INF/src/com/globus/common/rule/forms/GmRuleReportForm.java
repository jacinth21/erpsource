/**
 * FileName    : GmRuleConditionForm.java 
 * Description :
 * Author      : rshah
 * Date        : Jun 3, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
 
public class GmRuleReportForm extends GmCommonForm{
	private String txnId = "";
	 
	private String  status = "91371";
	private String  initiatedBy = "";
	private String consequenceId = "";
	private String inputStr = ""; 
	private String loadRule = "";
	private ArrayList alTransaction  = new ArrayList(); 
	private ArrayList alConsequence  = new ArrayList();
	private ArrayList alStatus  = new ArrayList();
	private ArrayList alInitiatedBy = new ArrayList();
	private List returnList = new ArrayList();
	private String inputString = ""; // for analysis report
	private HashMap hmRuleDetails = new HashMap();
	private HashMap hmConsequences = new HashMap();
	private HashMap hmParam = new HashMap();
	private List returnConditionList = new ArrayList();
	private List returnTransactionList = new ArrayList();
	private List alreportType = new ArrayList();
	private String reportType = "92330";
	
	public List getAlreportType() {
		return alreportType;
	}
	public void setAlreportType(List alreportType) {
		this.alreportType = alreportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	/**
	 * @return the returnList
	 */
	public List getReturnList() {
		return returnList;
	}
	/**
	 * @param returnList the returnList to set
	 */
	public void setReturnList(List returnList) {
		this.returnList = returnList;
	}
 
	 
	/**
	 * @return the txnId
	 */
	public String getTxnId() {
		return txnId;
	}
	/**
	 * @param txnId the txnId to set
	 */
	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the initiatedBy
	 */
	public String getInitiatedBy() {
		return initiatedBy;
	}
	/**
	 * @param initiatedBy the initiatedBy to set
	 */
	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}
	/**
	 * @return the consequenceId
	 */
	public String getConsequenceId() {
		return consequenceId;
	}
	/**
	 * @param consequenceId the consequenceId to set
	 */
	public void setConsequenceId(String consequenceId) {
		this.consequenceId = consequenceId;
	}
	/**
	 * @return the alTransaction
	 */
	public ArrayList getAlTransaction() {
		return alTransaction;
	}
	/**
	 * @param alTransaction the alTransaction to set
	 */
	public void setAlTransaction(ArrayList alTransaction) {
		this.alTransaction = alTransaction;
	}
	/**
	 * @return the alConsequence
	 */
	public ArrayList getAlConsequence() {
		return alConsequence;
	}
	/**
	 * @param alConsequence the alConsequence to set
	 */
	public void setAlConsequence(ArrayList alConsequence) {
		this.alConsequence = alConsequence;
	}
	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}
	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	/**
	 * @return the alInitiatedBy
	 */
	public ArrayList getAlInitiatedBy() {
		return alInitiatedBy;
	}
	/**
	 * @param alInitiatedBy the alInitiatedBy to set
	 */
	public void setAlInitiatedBy(ArrayList alInitiatedBy) {
		this.alInitiatedBy = alInitiatedBy;
	}
	/**
	 * @return the hmRuleDetails
	 */
	public HashMap getHmRuleDetails() {
		return hmRuleDetails;
	}
	/**
	 * @param hmRuleDetails the hmRuleDetails to set
	 */
	public void setHmRuleDetails(HashMap hmRuleDetails) {
		this.hmRuleDetails = hmRuleDetails;
	}
	/**
	 * @return the hmConsequences
	 */
	public HashMap getHmConsequences() {
		return hmConsequences;
	}
	/**
	 * @param hmConsequences the hmConsequences to set
	 */
	public void setHmConsequences(HashMap hmConsequences) {
		this.hmConsequences = hmConsequences;
	}
	/**
	 * @return the returnConditionList
	 */
	public List getReturnConditionList() {
		return returnConditionList;
	}
	/**
	 * @param returnConditionList the returnConditionList to set
	 */
	public void setReturnConditionList(List returnConditionList) {
		this.returnConditionList = returnConditionList;
	}
	/**
	 * @return the returnTransactionList
	 */
	public List getReturnTransactionList() {
		return returnTransactionList;
	}
	/**
	 * @param returnTransactionList the returnTransactionList to set
	 */
	public void setReturnTransactionList(List returnTransactionList) {
		this.returnTransactionList = returnTransactionList;
	}
	/**
	 * @return the inputStr
	 */
	public String getInputStr() {
		return inputStr;
	}
	/**
	 * @param inputStr the inputStr to set
	 */
	public void setInputStr(String inputStr) {
		this.inputStr = inputStr;
	}
	/**
	 * @return the hmParam
	 */
	public HashMap getHmParam() {
		return hmParam;
	}
	/**
	 * @param hmParam the hmParam to set
	 */
	public void setHmParam(HashMap hmParam) {
		this.hmParam = hmParam;
	}
	/**
	 * @return the loadRule
	 */
	public String getLoadRule() {
		return loadRule;
	}
	/**
	 * @param loadRule the loadRule to set
	 */
	public void setLoadRule(String loadRule) {
		this.loadRule = loadRule;
	}
	public String getReportType() {
		return reportType;
	}
	/**
	 * @return the inputString
	 */
	public String getInputString() {
		return inputString;
	}
	/**
	 * @param inputString the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	 

}
