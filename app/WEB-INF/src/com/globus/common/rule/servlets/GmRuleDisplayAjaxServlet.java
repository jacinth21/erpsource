/**
 * FileName    : GmRuleDisplayAjaxServlet.java 
 * Description :
 * Author      : vprasath
 * Date        : Jun 18, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.servlets.GmServlet;

/**
 * @author vprasath
 *
 */
public class GmRuleDisplayAjaxServlet extends GmServlet{

	
	public String createAjaxString (ArrayList alList, String strShowLink){		
		StringBuffer sbVal = new StringBuffer();
		HashMap hmTemp = new HashMap();		
		List alTemplate;
		String strHeader = "";
		String strValue = "";
		alTemplate= getTableHeaderTemplate();
		int tdSize = alTemplate.size();
		int trSize = alList.size();
		
    	for (int i = 0; i < trSize; i++){
			hmTemp = (HashMap)alList.get(i);
			sbVal.append(createTR(i));	
			for (int count = 0; count <tdSize; count++)
			{
				 strHeader = String.valueOf(alTemplate.get(count));
				 strValue = GmCommonClass.parseNull((String) hmTemp.get(strHeader));
				 strValue = decorate(strHeader, strValue, hmTemp,strShowLink);
				 sbVal.append(createTD(count, strValue, strHeader));			
				 sbVal.append(addTDtoRow(i, count));
			}
			sbVal.append(addRowToTable(i));
		}
    	return sbVal.toString();
	}
	
	private String decorate(String strHeader, String strValue, HashMap hmCurrentRowObject, String strShowLink)
	{	
		String strReturnValue = "";
		String strRuleID = "";
		String strPnum="";
		String strExpType = "";
		log.debug("hmCurrentRowObject........."+hmCurrentRowObject);
		if(strHeader.equals("PICTURE"))
		{
			strRuleID = String.valueOf(hmCurrentRowObject.get("ID"));
			if(strValue.equals("Y"))
			{
				strReturnValue = "<a href=javascript:fnOpenPicture("+strRuleID+")>" + " <img src=/images/product_icon.jpg ></a>";
			} 
		} 
		else if(strHeader.equals("ID"))
		{
			strRuleID = String.valueOf(hmCurrentRowObject.get("ID")); 
			strPnum = String.valueOf(hmCurrentRowObject.get("PNUM"));
			strExpType = GmCommonClass.parseNull((String)hmCurrentRowObject.get("EXPTYPE"));
			log.debug("strRuleID : " + strRuleID);
			//No need to show image for BBA rules
			if (!strRuleID.equals("CNV") && !strRuleID.equals("RFS") && !strRuleID.equals("BBA"))
			{
				strReturnValue = "<a href=javascript:fnCallRuleSummary("+strRuleID+")>"+ " <img src=/images/ordsum.gif ></a>";
			}  
			else if (strRuleID.equals("CNV") && (strShowLink.equals("true"))) { 
				strReturnValue = "<a href=javascript:fnCallRuleException('"+strRuleID+"',&#39#"+strPnum+"&#39);> <img src=/images/editcheck.gif ></a>";
	        } 
			else if (strRuleID.equals("BBA") && (strShowLink.equals("true") &&  strExpType.equals("EXP1M"))) { 
				String[] strPartAndCntrlNum=strPnum.split("\\|");
				String strPartNum=strPartAndCntrlNum[0].trim()+"|"+strPartAndCntrlNum[1].trim();
				strReturnValue = "<a href=javascript:fnCallExpDateRuleException('"+strRuleID+"',&#39#"+strPartNum+"&#39);> <img src=/images/editcheck.gif ></a>";	            
	        } 
			else
			{
				strReturnValue = ""; 
			}
		}		
		else  
		{
			strReturnValue = strValue;
		}
		return strReturnValue;
	}
	private String addTDtoRow(int rowCount, int columnCount)
	{
		String strTd = "";
		strTd += "row"+rowCount+".appendChild(td"+columnCount+");";
		return strTd;
	}
	private String addRowToTable(int i)
	{
		String strTable = "";
		strTable += "tbody.appendChild(row"+i+");";	
		return strTable;
	}
	
	private String createTR(int i)
	{
		String strRow = "";
			strRow = "var row"+i+" = document.createElement('TR');";
			strRow += "row"+i+".style.backgroundColor = '#FFFFFF';";
			return strRow;
	}
	
	private String createTD(int tdCount, String strValue, String strHeader)
	{
		String strTD = "";
		strValue = strValue.replaceAll("'", "&#39");
		strTD += "td"+tdCount+"= document.createElement('TD');";
		if(strHeader.equals("HOLDTXN"))
		{
			strValue = strValue.equals("Y")?"YES":"NO";
			strTD += "td"+tdCount+".innerHTML = '"+strValue+"';";
		}
		else
		{
			strTD += "td"+tdCount+".innerHTML = '"+strValue+"';";
		}
		if(strHeader.equals("HOLDTXN")||strHeader.equals("UPDDATE") || strHeader.equals("PICTURE"))
		{
			strTD += "td"+tdCount+".style.textAlign='center';";
		}
		//log.debug("TD..."+strTD);
		return strTD;
	}
	
	private List getTableHeaderTemplate()
	{
		 String strTableHeaderNames = GmCommonClass.getString("RULETABLETEMPLATE");
		 List alList = Arrays.asList(strTableHeaderNames.split(","));		
		 return alList;		 
	}

	public void doPost(HttpServletRequest request, HttpServletResponse  response) throws IOException, ServletException
	{
		log.debug("Inside Servlet");
		HttpSession session = request.getSession(false);
		GmAccessControlBean	gmAccessControlBean = new GmAccessControlBean();
		String strPartyId = (String) session.getAttribute("strSessPartyId");
		ArrayList alPartyList = new ArrayList(); 
		String strShowLink="false";
		
		String strXMLString = "";		
		try
		{
				alPartyList = gmAccessControlBean.getPartyList("CNOVERRIDE",strPartyId);
				if (alPartyList.size()!=0) {
					strShowLink =  "true";
			 	}
				ArrayList alList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("CONSEQUENCES"));		
				strXMLString =createAjaxString(alList,strShowLink);
				
				log.debug("strXMLStringVal..."+strXMLString);
				
				PrintWriter writer = response.getWriter(); 
	            writer.println(strXMLString);
	            writer.close();
		}// End of try
		catch (Exception e)
		{
				e.printStackTrace();
		}// End of catch	

	}
	public void doGet(HttpServletRequest request, HttpServletResponse  response) throws IOException, ServletException
	{
		doPost(request,response);
			}
}

