/**
 * FileName    : GmSetConsignRTCRuleWrapper.java 
 * Description :
 * Author      : vprasath
 * Date        : Jun 30, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmRuleBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 *
 */
public class GmSetConsignRTCRuleWrapper extends GmRuleEngineWrapper {

	/* (non-Javadoc)
	 * @see com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest)
	 */
    private static String AVAILABLE_TXN = "50920,50921,50922";
    
	public List formatData(HttpServletRequest request) throws AppError {
		
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		gmDataStoreVO = GmFramework.getCompanyParams(request);
		
		HashMap hmReturn =  (HashMap) request.getAttribute("hmReturn");
		ArrayList alReturn  = new ArrayList();
		ArrayList alDetails =  hmReturn == null ? new ArrayList() : GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETLOAD"));
		HashMap hmRow = new HashMap();
		String strTxnStatus = GmCommonClass.parseNull((String) request.getAttribute("txnStatus"));
		String strTxn = GmCommonClass.parseNull((String) request.getAttribute("RE_TXN"));
		GmRuleBean gmRuleBean = new GmRuleBean(gmDataStoreVO);
		String strCompanyID = gmDataStoreVO.getCmpid();
		
		strTxnStatus = strTxnStatus.equals("") ? GmCommonClass.parseNull((String) request.getParameter("txnStatus")) : strTxnStatus;
		strTxn = strTxn.equals("") ? GmCommonClass.parseNull((String) request.getParameter("RE_TXN")) :strTxn;
		log.debug("alDetails: "+alDetails);
		String strConsignId = GmCommonClass.parseNull((String) request.getParameter("hConsignId"));
		
		String strNameId = GmCommonClass.parseNull((String)request.getAttribute("names"));
		String strShipToId = GmCommonClass.parseNull((String)request.getAttribute("shipTo"));
		String strAddressId =  GmCommonClass.parseNull((String)request.getAttribute("addressid"));
		String strCountryID = "";
		 
		strCountryID = GmCommonClass.parseNull(gmRuleBean.fetchShipOutCountryID(strShipToId, strNameId, strAddressId));
		
		//PC-5648: Enable returns of expired parts from entities to owner company for Built set to Returns
		String strSelectedAction = GmCommonClass.parseNull((String) request.getAttribute("hSelectedAction"));
		String strSelectedAct = GmCommonClass.parseNull((String) request.getParameter("hSelectedAction"));		
		strSelectedAction = strSelectedAction == "" ? strSelectedAct : strSelectedAction;
		
		if(strTxnStatus.equals("VERIFY"))
		{	  
			GmOperationsBean gmOper = new GmOperationsBean(gmDataStoreVO);
			HashMap hmResult = new HashMap();						
			AVAILABLE_TXN = strTxn;			
			hmResult = gmOper.loadSavedSetMaster(strConsignId);
			alDetails =  hmResult == null ? new ArrayList() : GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("SETLOAD"));					
			log.debug("alDetails in Verify : "+alDetails);
		}
		
		if(alDetails.size() > 0 )
		{
			Iterator itrDetails =  alDetails.iterator();

			while(itrDetails.hasNext())
			{
				hmRow = (HashMap)itrDetails.next();
				String strPnum = GmCommonClass.parseNull((String)hmRow.get("PNUM"));
				String strCnum = GmCommonClass.parseNull((String)hmRow.get("CNUM"));
				String strQty = GmCommonClass.parseNull((String)hmRow.get("QTY"));
	
				String strTxnArray[] = AVAILABLE_TXN.split(",");
				for(int i = 0; i< strTxnArray.length; i++ )
				{
				GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
				gmRuleParamVO.setPnum(strPnum);
				gmRuleParamVO.setControlNumber(strCnum);
				gmRuleParamVO.setQty(strQty);
				gmRuleParamVO.setTxn(strTxnArray[i]);
				gmRuleParamVO.setTxnStatus(strTxnStatus);
				gmRuleParamVO.setTxnID(strConsignId);
				gmRuleParamVO.setCountryID(strCountryID);
				gmRuleParamVO.setCmpid(strCompanyID);
				gmRuleParamVO.sethSelectedAction(strSelectedAction);
				alReturn.add(gmRuleParamVO);
				//log.debug("gmRuleParamVO"+gmRuleParamVO.toHashMap());
				}
			}
		}
		log.debug("alReturn: "+alReturn);
		return alReturn;
	}

}
