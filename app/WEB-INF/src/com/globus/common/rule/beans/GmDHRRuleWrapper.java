/**
 * FileName : GmDHRRuleWrapper.java Description : Author : vprasath Date : Jun 9, 2009 Copyright :
 * Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.DynaBean;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
public class GmDHRRuleWrapper extends GmRuleEngineWrapper {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest
   * )
   */
  List alList;

  @Override
  public List formatData(HttpServletRequest request) {
    // TODO Auto-generated method stub
    alList = new ArrayList();
    
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    gmDataStoreVO = GmFramework.getCompanyParams(request);
	
    HashMap hmDHRList = new HashMap();
    String strAction = "";
    String strPNUM = "";
    String strCNUM = "";
    String strQty = "";
    String strTotal = "";
    String strPrint = "";
    String strAllTxn = "50900,50901,50902,50903,50931,50932,50933,50934,50935,50936,50937";
    
    String strTxnArray[] = new String[5];
    String strInpStr = "";
    int iTxnLength = 1;
    DynaBean Dbean = null;

    String strTxn = GmCommonClass.parseNull((String) request.getAttribute("RE_TXN"));
    strTxn = strTxn.equals("") ? GmCommonClass.parseNull(request.getParameter("RE_TXN")) : strTxn;
    strInpStr = GmCommonClass.parseNull((String) request.getAttribute("hInpStr"));
    strInpStr =
        strInpStr.equals("") ? GmCommonClass.parseNull(request.getParameter("hInpStr")) : strInpStr;
    String strTxnStatus = GmCommonClass.parseNull((String) request.getAttribute("txnStatus"));
    strTxnStatus =
        strTxnStatus.equals("") ? GmCommonClass.parseNull(request.getParameter("txnStatus"))
            : strTxnStatus;
    String strMode = GmCommonClass.parseNull((String) request.getAttribute("transMode"));
    strPrint = GmCommonClass.parseNull((String) request.getAttribute("PRINTDHR"));
    HashMap hmValues = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("hmReturn"));
    hmDHRList = GmCommonClass.parseNullHashMap((HashMap) hmValues.get("DHRDETAILS"));
    String strCompanyID = gmDataStoreVO.getCmpid();
    List ldhrPartResult;
    // log.debug("RstrTxn = "+strTxn);
    // log.debug("RstrInpStr = "+strInpStr);
    // log.debug("RhmValues in Return Rule Wrapper..."+hmDHRList+" hmDHRList..."+hmDHRList.size());
    strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
    // log.debug("RMode....."+strMode);
    // log.debug("RstrAction = "+strAction);

    if (!strTxnStatus.equals(""))// If the strTxnStatus value is not null then GmRuleParamVO value
                                 // not added in the alList.
    {
      if (strAction.equals("I") || strMode.equals("I")) {
        strQty = GmCommonClass.parseNull(request.getParameter("Txt_QtyInsp"));
        strQty = strQty.equals("") ? "0" : strQty;
        strTotal = GmCommonClass.parseNull(request.getParameter("totalQty"));
        strTotal = strTotal.equals("") ? "0" : strTotal;
        double dQtyPer =
            (Double.parseDouble(strQty) / Double.parseDouble(strTotal)) * Double.parseDouble("100");
        int iQty = (int) Math.ceil(dQtyPer);
        strQty = iQty + "";
        // log.debug("Rin I...."+strQty);
      }
      if (hmDHRList.size() > 0) {
        strTxnArray[0] = strTxn;
        // strTxnStatus = "VERIFY";
        // if(strPrint.equals("printDHR"))
        // {
        strTxnArray = strAllTxn.split("\\,");
        iTxnLength = strTxnArray.length;
        // }
        // log.debug("Rlength = "+iTxnLength);
        for (int i = 0; i < iTxnLength; i++) {
          GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
          // log.debug("PUM = "+(String)hmDHRList.get("PNUM"));
          gmRuleParamVO.setPnum(GmCommonClass.parseNull((String) hmDHRList.get("PNUM")));
          // log.debug("RCUM = "+(String)hmDHRList.get("CNUM"));
          gmRuleParamVO.setControlNumber(GmCommonClass.parseNull((String) hmDHRList.get("CNUM")));
          // log.debug("strTxn = "+strTxnArray[i]);
          gmRuleParamVO.setTxn(strTxnArray[i]);
          // log.debug("strTxnStatus = "+strTxnStatus);
          gmRuleParamVO.setTxnStatus(strTxnStatus);
          gmRuleParamVO.setCountry("US");
          // log.debug("strQty = "+strQty);
          gmRuleParamVO.setQty(GmCommonClass.parseNull(strQty));
          alList.add(gmRuleParamVO);
        }
      } else {
        if (strAction.equals("V") || strAction.equals("P"))
          strQty = GmCommonClass.parseNull(request.getParameter("Txt_Qty"));
        // log.debug("RstrQty = "+strQty);
        strPNUM = GmCommonClass.parseNull(request.getParameter("hPartNum"));
        strCNUM = GmCommonClass.parseNull(request.getParameter("hControlNum"));
        GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
        gmRuleParamVO.setPnum(strPNUM);
        gmRuleParamVO.setControlNumber(strCNUM);
        gmRuleParamVO.setTxn(strTxn);
        gmRuleParamVO.setCountry("US");
        gmRuleParamVO.setTxnStatus(strTxnStatus);
        gmRuleParamVO.setQty(GmCommonClass.parseNull(strQty));
        gmRuleParamVO.setCmpid(strCompanyID);
        alList.add(gmRuleParamVO);

      }
    }
    return alList;
  }

}
