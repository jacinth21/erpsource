/**
 * FileName    : GmSetConsignRuleWrapper.java 
 * Description :
 * Author      : vprasath
 * Date        : Jun 9, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmRuleBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.valueobject.common.GmDataStoreVO;

 
public class GmRequestInitiateRuleWrapper extends GmRuleEngineWrapper {
 
	List alList;
	public List formatData(HttpServletRequest request){
		alList = new ArrayList();
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		gmDataStoreVO = GmFramework.getCompanyParams(request);
		GmRuleBean gmRuleBean = new GmRuleBean(gmDataStoreVO);
		GmProjectBean gmProjectBean  = new GmProjectBean(gmDataStoreVO);
		 
		HashMap hmValues = new HashMap();
		ArrayList alData = new ArrayList();
		String strTxn = "";
		String strCompanyID = gmDataStoreVO.getCmpid();
		String strNameId = "";
		String strShipToId = "";  
		String strSetId = "";
		String strInput = "";
		String strRefIDs  = "";
		String strRefName  = "";
		String strPNUM  = "";
		String strAddressId = "";
		
		String strTxnStatus = GmCommonClass.parseNull((String) request.getParameter("txnStatus")); 
        strNameId = GmCommonClass.parseNull((String)request.getParameter("names"));
         
        strInput = GmCommonClass.parseNull((String)request.getParameter("hinputString")); 
        strShipToId = GmCommonClass.parseNull((String)request.getParameter("shipTo")); 
        
        strAddressId =  GmCommonClass.parseNull((String)request.getParameter("addressid"));
        
        strRefIDs = GmCommonClass.parseNull((String)request.getParameter("RefIDs"));
        strRefName = GmCommonClass.parseNull((String)request.getParameter("RefName"));

      
		try
		{  			 
			String strCountry = GmCommonClass.parseNull(gmRuleBean.fetchShipOutCountryID(strShipToId, strNameId, strAddressId)); 
			  
				strRefIDs = GmCommonClass.parseNull((String) request.getParameter("RefIDs")); 
				String[] strRefID = strRefIDs.split(",");
				 
					if(strRefName.equals("Loaner")){ 
						
						 for(int j=0; j < strRefID.length; j++)
							{
								alData = gmProjectBean.fetchSetMaping(strRefID[j],"SET"); 
								for (int i =0; i< alData.size(); i++)
								{
									hmValues = (HashMap)alData.get(i);
									 
									strPNUM = GmCommonClass.parseNull((String)hmValues.get("PNUM")); 
									 
									GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();						 
									gmRuleParamVO.setPnum(strPNUM);					 
									gmRuleParamVO.setCountryID(strCountry);   
									gmRuleParamVO.setTxnStatus(strTxnStatus);  
									gmRuleParamVO.setCmpid(strCompanyID); 
									alList.add(gmRuleParamVO);
									 
								}	
							}
					}
				
					else{
						  
						 for(int i=0; i < strRefID.length; i++)
							{
							 GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();						 
							 gmRuleParamVO.setPnum(strRefID[i]);					 
							 gmRuleParamVO.setCountryID(strCountry);
							 gmRuleParamVO.setTxnStatus(strTxnStatus);
							 gmRuleParamVO.setCmpid(strCompanyID);
							 alList.add(gmRuleParamVO);
							}
						}
					
				if(alList.size()==0){
					 alList.add(new GmRuleParamVO());
				} 
					 		
		}catch(Exception exp)
		{
			exp.printStackTrace();
		}
		return alList;
	}
}
