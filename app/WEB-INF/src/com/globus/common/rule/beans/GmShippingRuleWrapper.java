/**
 * FileName    : GmShippingRuleWrapper.java 
 * Description :
 * Author      : rshah
 * Date        : Jun 24, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmRuleBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author rshah
 *
 */
public class GmShippingRuleWrapper extends GmRuleEngineWrapper {
	List alList;
	public List formatData(HttpServletRequest request) {
		// TODO Auto-generated method stub
		alList = new ArrayList();
		
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		gmDataStoreVO = GmFramework.getCompanyParams(request);
		
		GmRuleBean gmRuleBean = new GmRuleBean(gmDataStoreVO);
		HashMap hmShipout = new HashMap();
		HashMap hmValues = new HashMap();
		ArrayList alData = new ArrayList();
		String strTxn = "";
		String strAddressId = "";
		String strNameId = "";
		String strShipToId = "";
		String[] strTxnIds = new String[15];
		String strTxnStatus = GmCommonClass.parseNull((String)request.getAttribute("txnStatus"));
        strTxnStatus = strTxnStatus.equals("")?GmCommonClass.parseNull((String)request.getParameter("txnStatus")):strTxnStatus;
		String strRefId = GmCommonClass.parseNull((String)request.getAttribute("REFID")); 
		strRefId = strRefId.equals("")?GmCommonClass.parseNull((String)request.getParameter("refId")):strRefId;
		String strSource = GmCommonClass.parseNull((String)request.getAttribute("SOURCE"));
		strSource = strSource.equals("")?GmCommonClass.parseNull((String)request.getParameter("source")):strSource;
		strAddressId = GmCommonClass.parseNull((String)request.getAttribute("ADDRESSID"));
		strAddressId = strAddressId.equals("")?GmCommonClass.parseNull((String)request.getParameter("addressid")):strAddressId;
		String strCompanyID = gmDataStoreVO.getCmpid();
		log.debug("strAddressId: " + strAddressId);
		
        strNameId = GmCommonClass.parseNull((String)request.getAttribute("NAMEID"));
        strNameId = strNameId.equals("")?GmCommonClass.parseNull((String)request.getParameter("names")):strNameId;

        strShipToId = GmCommonClass.parseNull((String)request.getAttribute("SHIPTOID"));
        strShipToId = strShipToId.equals("")?GmCommonClass.parseNull((String)request.getParameter("shipTo")):strShipToId;
		
		log.debug("strTxnStatus = "+strTxnStatus);
		try
		{ 
			String strCountryID = GmCommonClass.parseNull(gmRuleBean.fetchShipOutCountryID(strShipToId, strNameId,strAddressId));
			String strCountry = GmCommonClass.parseNull(gmRuleBean.fetchShipOutCountry(strAddressId));			
			strCountry = strCountry.equals("") ? GmCommonClass.parseNull(GmCommonClass.getCodeAltName(strCountryID)) : strCountry;
			
			hmShipout = GmCommonClass.parseNullHashMap(gmRuleBean.fetchRuleShipoutDetails(strRefId, strSource));
			alData = GmCommonClass.parseNullArrayList((ArrayList)hmShipout.get("ALDATA"));
			strTxn = GmCommonClass.parseNull((String)hmShipout.get("RE_TXN"));
			 
			strTxnIds = strTxn.split("\\,");
			log.debug("strTxnIds length = "+strTxnIds.length);
			for(int j=0;j<strTxnIds.length;j++)
			{
				if(alData.size() > 0)
				{
					strTxn = strTxnIds[j];
					log.debug("strTxn = "+strTxn);
					for (int i =0; i< alData.size(); i++)
					{
						hmValues = (HashMap)alData.get(i);
						String strSETID = GmCommonClass.parseNull((String)hmValues.get("SETID"));
						String strPNUM = GmCommonClass.parseNull((String)hmValues.get("PNUM"));
						String strCNUM = GmCommonClass.parseNull((String)hmValues.get("CNUM"));
						String strQTY = GmCommonClass.parseNull((String)hmValues.get("QTY"));
						String strType = GmCommonClass.parseNull((String)hmValues.get("CTYPE"));
						strQTY = strQTY.equals("")?"0":strQTY;
						GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
						gmRuleParamVO.setPnum(strPNUM);
						gmRuleParamVO.setControlNumber(strCNUM);
						gmRuleParamVO.setTxn(strTxn); 
						gmRuleParamVO.setTxnStatus(strTxnStatus);
						gmRuleParamVO.setQty(strQTY);
						gmRuleParamVO.setCountry(strCountry);
						gmRuleParamVO.setTxnID(strRefId);
						gmRuleParamVO.setSetID(strSETID);
						gmRuleParamVO.setCountryID(strCountryID);
						gmRuleParamVO.setRuleType("Shipping");
						//Action - Verify is used for accept NOC# control number for all parts only in Verification (MNTTASK-2771)
						gmRuleParamVO.setAction("Verify");
						gmRuleParamVO.setCmpid(strCompanyID);
						
						//PC-5648: Enable returns of expired parts from entities to owner company for Built set to Returns
						if(strType.equals("26240144")) {   //Return to Owner Company (26240144)
							gmRuleParamVO.sethSelectedAction("BSTR");
						}
						
						//Distributor shipToID  
						 if(strShipToId.equals("4120")){
                            gmRuleParamVO.setShipTo(strNameId);
                         }

						//following code id is for Inhouse Loaner and Product Loaner.
						if(strTxn.equals("50916") || strTxn.equals("50917") || strTxn.equals("50923") || strTxn.equals("50924") )
						{
							if(Integer.parseInt(strQTY)>0)
							{
								log.debug("strQTY = "+strQTY+" strPNUM = "+strPNUM+" strTxn= "+strTxn);
								alList.add(gmRuleParamVO);
							}
						}
						else
						{ 
							alList.add(gmRuleParamVO);
						}
					}
				}
			}
		}catch(Exception exp)
		{
			exp.printStackTrace();
		}
		return alList;
	}
}
