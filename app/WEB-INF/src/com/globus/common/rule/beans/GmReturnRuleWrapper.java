/**
 * FileName    : GmReturnRuleWrapper.java 
 * Description :
 * Author      : vprasath
 * Date        : Jun 9, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 *
 */
public class GmReturnRuleWrapper extends GmRuleEngineWrapper {

	/* (non-Javadoc)
	 * @see com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest)
	 */
	List alList;
	public List formatData(HttpServletRequest request) {
		// TODO Auto-generated method stub
	    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
	    gmDataStoreVO = GmFramework.getCompanyParams(request);
		String strRuleTransId = "50904,50905,50907";//For CN/PN/QN
        String[] strTransIds = new String[3]; 
        String strTxn = "";
		ArrayList alReturnDetails = new ArrayList();
		alList = new ArrayList();
		HashMap hmReturnDetail = new HashMap();
		
		String strSetID = GmCommonClass.parseNull((String)request.getParameter("Cbo_SetID"));
        String strCNStr = GmCommonClass.parseNull((String)request.getParameter("hCNStr"));
        String strPNStr = GmCommonClass.parseNull((String)request.getParameter("hPNStr"));
        String strQNStr = GmCommonClass.parseNull((String)request.getParameter("hQNStr"));
        String strTxnStatus = GmCommonClass.parseNull((String)request.getAttribute("txnStatus"));
        String strTxnTypeId ="";
        String strCNRtnType ="";
        String strPNRtnType =""; 
        String strCompanyID = gmDataStoreVO.getCmpid();
		HashMap hmValues = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
		log.debug("hmValues in Return Rule Wrapper..."+hmValues);
		alReturnDetails = GmCommonClass.parseNullArrayList((ArrayList)hmValues.get("RAITEMDETAILS"));
		HashMap hmHeader = GmCommonClass.parseNullHashMap((HashMap)hmValues.get("RADETAILS"));
		String strRAID = GmCommonClass.parseNull((String)hmHeader.get("RAID"));
		strSetID = strSetID.equals("")?GmCommonClass.parseNull((String)hmHeader.get("SETID")):strSetID;		
		strTxnStatus = strTxnStatus.equals("")? GmCommonClass.parseNull((String)request.getParameter("txnStatus")):strTxnStatus;
		strTransIds = strRuleTransId.split("\\,");
		strTxnTypeId = GmCommonClass.parseNull((String)request.getParameter("TXN_TYPE_ID"));
		strCNRtnType = GmCommonClass.parseNull((String)request.getParameter("CNRTNTYPE"));
		strPNRtnType = GmCommonClass.parseNull((String)request.getParameter("PNRTNTYPE")); 
		
		log.debug("set id = "+ strSetID+"..strTxn.."+strTxn +"..strTxnStatus.."+strTxnStatus);
		log.debug("strCNStr..."+strCNStr);
		log.debug("strPNStr..."+strPNStr);
		log.debug("strQNStr..."+strQNStr);
		log.debug("strTxnTypeId..."+strTxnTypeId);
		log.debug("size.."+alReturnDetails.size()+"..."+alReturnDetails);
		
		if(alReturnDetails.size() > 0)
		{
			for(int j=0; j<strTransIds.length;j++)
			{
				log.debug("strTransIds[j]....."+strTransIds[j]);
				for (int i=0; i< alReturnDetails.size(); i++)
				{
					hmReturnDetail = (HashMap)alReturnDetails.get(i);
					GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
					gmRuleParamVO.setPnum((String)hmReturnDetail.get("PNUM"));
					gmRuleParamVO.setControlNumber((String)hmReturnDetail.get("CNUM"));
					gmRuleParamVO.setTxn(strTransIds[j]); 
					gmRuleParamVO.setTxnStatus(strTxnStatus);
					gmRuleParamVO.setSetID(strSetID);
					gmRuleParamVO.setTxnID(strRAID);
					//set txntypeid here, if it's 'RETURN', will skip expiry validation	
					gmRuleParamVO.setTxnTypeID(strTxnTypeId);
					gmRuleParamVO.setCmpid(strCompanyID);
					alList.add(gmRuleParamVO);			
				}
			}	
		}
		else
		{			
			if(!strCNStr.equals(""))
			{
				strTxn = "50933";
				String[] strCNArray = strCNStr.split("\\|");
				log.debug("strCNArray..."+strCNArray.length);
				for (int j=0; j<strCNArray.length; j++)
				{
					String[] strCNValues = strCNArray[j].split("\\^");
					GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
					gmRuleParamVO.setPnum(strCNValues[0]);
					gmRuleParamVO.setControlNumber(strCNValues[1]);
					gmRuleParamVO.setQty(strCNValues[2]);
					gmRuleParamVO.setTxn(strTxn); 
					gmRuleParamVO.setTxnStatus(strTxnStatus);						
					gmRuleParamVO.setSetID(strSetID);
					gmRuleParamVO.setTxnID(strRAID);
					gmRuleParamVO.setTxnTypeID(strCNRtnType);
					gmRuleParamVO.setCmpid(strCompanyID);
					alList.add(gmRuleParamVO);										
				}
			}			
			if(!strPNStr.equals(""))
			{
				String[] strPNArray = strPNStr.split("\\|");
				log.debug("strPNArray..."+strPNArray.length);
				for (int j=0; j<strPNArray.length; j++)
				{
					String[] strPNValues = strPNArray[j].split("\\^");
					GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
					gmRuleParamVO.setPnum(strPNValues[0]);
					gmRuleParamVO.setControlNumber(strPNValues[1]);
					gmRuleParamVO.setQty(strPNValues[2]);
					gmRuleParamVO.setTxn(strTransIds[1]); 
					gmRuleParamVO.setTxnStatus(strTxnStatus);						
					gmRuleParamVO.setSetID(strSetID);
					gmRuleParamVO.setTxnID(strRAID);
					gmRuleParamVO.setTxnTypeID(strPNRtnType);
					gmRuleParamVO.setCmpid(strCompanyID);
					alList.add(gmRuleParamVO);						
				}
			}
			if(!strQNStr.equals(""))
			{
				String[] strQNArray = strQNStr.split("\\|");
				log.debug("strQNArray..."+strQNArray.length);
				for (int j=0; j<strQNArray.length; j++)
				{
					String[] strQNValues = strQNArray[j].split("\\^");
					GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
					gmRuleParamVO.setPnum(strQNValues[0]);
					gmRuleParamVO.setControlNumber(strQNValues[1]);
					gmRuleParamVO.setQty(strQNValues[2]);
					gmRuleParamVO.setTxn(strTransIds[2]); 
					gmRuleParamVO.setTxnStatus(strTxnStatus);						
					gmRuleParamVO.setSetID(strSetID);
					gmRuleParamVO.setTxnID(strRAID);
					gmRuleParamVO.setTxnTypeID(strTxnTypeId);
					gmRuleParamVO.setCmpid(strCompanyID);
					alList.add(gmRuleParamVO);						
				}
			}
			
		}
		return alList;
	}

}
