/**
 * FileName : GmReconfigSetConsgWrapper.java Description : Author : vprasath Date : Jul 3, 2009
 * Copyright : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
public class GmReconfigSetConsgWrapper extends GmRuleEngineWrapper {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest
   * )
   */
  @Override
  public List formatData(HttpServletRequest request) throws AppError {
    ArrayList alList = new ArrayList();
    ArrayList alResult = new ArrayList();
    HashMap hmResult = new HashMap();
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    gmDataStoreVO = GmFramework.getCompanyParams(request);
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(gmDataStoreVO);
    String strCompanyID = gmDataStoreVO.getCmpid();

    String strTxnStatus = GmCommonClass.parseNull(request.getParameter("txnStatus"));
    strTxnStatus =
        strTxnStatus.equals("") ? GmCommonClass.parseNull((String) request
            .getAttribute("txnStatus")) : strTxnStatus;
    String strRuleVerify = GmCommonClass.parseNull(request.getParameter("ruleVerify"));
    String strTransIdDash =
        request.getParameter("hTxnId") == null ? "" : request.getParameter("hTxnId");
    String strTxnTypeID = GmCommonClass.parseNull((String) request.getAttribute("TxnTypeID"));
    strTxnTypeID =
        strTxnTypeID.equals("") ? GmCommonClass.parseNull(request.getParameter("TXN_TYPE_ID"))
            : strTxnTypeID;
    log.debug("strRuleVerify = " + strRuleVerify);
    log.debug("strTransIdDash = " + strTransIdDash);
    log.debug("GmReconfigSetConsgWrapper strTxnTypeID = " + strTxnTypeID);

    if (strTxnStatus.equalsIgnoreCase("PROCESS")) {
      String strTxn = "50918,50919";
      String strTXnArr[] = strTxn.split(",");

      HashMap hmReturn = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("hmReturn"));

      ArrayList alSetList = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("SETLOAD"));
      Iterator iterator = alSetList.iterator();
      HashMap hmRow = new HashMap();
      String strPnum = "";
      String strCnum = "";

      while (iterator.hasNext()) {
        hmRow = (HashMap) iterator.next();
        strPnum = (String) hmRow.get("PNUM");
        strCnum = (String) hmRow.get("CNUM");
        // log.debug("strCnum: " + strCnum);
        for (int i = 0; i < strTXnArr.length; i++) {
          GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
          gmRuleParamVO.setPnum(strPnum);
          gmRuleParamVO.setControlNumber(strCnum);
          gmRuleParamVO.setTxn(strTXnArr[i]);
          gmRuleParamVO.setCountry("US");
          gmRuleParamVO.setTxnStatus(strTxnStatus);
          gmRuleParamVO.setTxnID(strTransIdDash);
          gmRuleParamVO.setTxnTypeID(strTxnTypeID);
          gmRuleParamVO.setCmpid(strCompanyID);
          alList.add(gmRuleParamVO);
          // log.debug("VO :"+gmRuleParamVO.toHashMap());
        }
      }

      // log.debug("Set Load: " + alSetList);
    } else if (strTxnStatus.equalsIgnoreCase("VERIFY")) {
      String strCount =
          request.getParameter("hCnt") == null ? "" : (String) request.getParameter("hCnt");
      int intCount = Integer.parseInt(strCount);
      String strInputString = "";
      String strPartNum = "";
      String strQty = "";
      String strControl = "";
      String strReconfigAction = "";
      String strReleaseTo = "";
      String strTxn = "";
      String strTxnValue = "";

      if (strRuleVerify.equals("true")) {
        // hmResult =
        // gmInHouseRptBean.viewInHouseTransDetails(strTransIdDash);
        // //CONDETAILS
        alResult = gmInHouseRptBean.loadSavedSetMaster(strTransIdDash);// SETLOAD
        // log.debug("alResult = " + alResult);
        strTxn = GmCommonClass.parseNull(request.getParameter("RE_TXN"));
        log.debug("strTxn = " + strTxn);

        strTxnValue = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strTxnTypeID, "RULETXN"));
        strTxn = strTxnValue.equals("") ? strTxn : strTxnValue;

        log.debug("strTxn ::: " + strTxn);

        Iterator iterator = alResult.iterator();
        HashMap hmRow = new HashMap();
        String strPnum = "";
        String strCnum = "";

        while (iterator.hasNext()) {
          hmRow = (HashMap) iterator.next();
          strPnum = GmCommonClass.parseNull((String) hmRow.get("PNUM"));
          strCnum = GmCommonClass.parseNull((String) hmRow.get("CNUM"));
          // log.debug("strCnum: " + strCnum);
          GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
          gmRuleParamVO.setPnum(strPnum);
          gmRuleParamVO.setControlNumber(strCnum);
          gmRuleParamVO.setTxn(strTxn);
          gmRuleParamVO.setCountry("US");
          gmRuleParamVO.setTxnStatus(strTxnStatus);
          gmRuleParamVO.setTxnID(strTransIdDash);
          gmRuleParamVO.setTxnTypeID(strTxnTypeID);
          gmRuleParamVO.setCmpid(strCompanyID);
          alList.add(gmRuleParamVO);
          // log.debug("VO :"+gmRuleParamVO.toHashMap());
        }
      } else {
        for (int i = 0; i <= intCount; i++) {
          strPartNum =
              request.getParameter("hPartNum" + i) == null ? "" : (String) request
                  .getParameter("hPartNum" + i);
          // log.debug("strPartNum = "+strPartNum);
          if (!strPartNum.equals("")) {
            strQty = GmCommonClass.parseNull(request.getParameter("Txt_ReconfigQty" + i));
            strQty = strQty.equals("") ? "0" : strQty;

            strControl = GmCommonClass.parseNull(request.getParameter("hControlNum" + i));
            strControl = strControl.equals("") ? " " : strControl;
            /*
             * If Control Number is TBE for Reconfigure Consignment transaction,Then transaction
             * should allow Control Number TBE in this case to next level. So if control number is
             * TBE in this wrapper, then am making it as empty string for RuleEngine validations.
             */
            strControl = strControl.equalsIgnoreCase("TBE") ? " " : strControl;
            // log.debug("strControl = "+strControl);
            strReconfigAction = GmCommonClass.parseNull(request.getParameter("Cbo_Action" + i));
            strReconfigAction = strReconfigAction.equals("") ? "0" : strReconfigAction;
            strReleaseTo = GmCommonClass.parseNull(request.getParameter("Cbo_ReleaseTo" + i));
            strReleaseTo = strReleaseTo.equals("") ? "0" : strReleaseTo;
            log.debug("strReleaseTo = " + strReleaseTo);

            if (!strReconfigAction.equals("0") && !strReleaseTo.equals("0")) // Validate both
            // actions
            // selected.
            {
              if (strReleaseTo.equals("90805"))// release to shelf
                strTxn = "50919";
              else if (strReleaseTo.equals("90811")) // release to
                // quarantine
                strTxn = "50918";
              // log.debug("strTxn = "+strTxn);
              GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
              gmRuleParamVO.setPnum(strPartNum);
              gmRuleParamVO.setControlNumber(strControl);
              gmRuleParamVO.setTxn(strTxn);
              gmRuleParamVO.setTxnID(strTransIdDash);
              gmRuleParamVO.setTxnStatus("VERIFY");
              gmRuleParamVO.setTxnTypeID(strTxnTypeID);
              gmRuleParamVO.setCmpid(strCompanyID);
              alList.add(gmRuleParamVO);
            }
          }
        }
        log.debug(strInputString);
      }
    }
    return alList;
  }
}
