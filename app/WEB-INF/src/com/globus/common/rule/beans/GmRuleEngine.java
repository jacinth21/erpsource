/**
 * FileName : GmRuleEngine.java Description : Author : vprasath Date : Jun 9, 2009 Copyright :
 * Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.prodmgmnt.beans.GmPartNumBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
public final class GmRuleEngine extends GmBean {


  public GmRuleEngine() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmRuleEngine(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  private static final String RULEID = "RULEID";
  private static final String RULELEVEL = "RULELEVEL";
  private static final String RULEHOLDTXN = "HOLDTXN";
  private static final String REGEX = "\\d{1,2}/\\d{1,2}/\\d{2,4}";
  public static final String TEMPLATE_NAME = "GmRuleEngine";

  private ArrayList alAvailableRules = new ArrayList();
  private String strTxn = "";
  private String strTxnStatus = "";
  private String strAvailableRules = "";
  private String strReqParameterInputString = "";
  private final ArrayList alValidRules = new ArrayList();
  private List alConsequences = new ArrayList();
  private List alEmailConsequences = new ArrayList();
  private String strReturnType = "";
  private int maxParamLevel;
  private HashMap hmConditionLevelDetails;
  private String strValidRuleIds = "";
  private final Logger log = GmLogger.getInstance(this.getClass().getName());
  private final HashMap hmPnumMap = new HashMap();
  private final List alRFS = new ArrayList();
  private final List alInternational = new ArrayList();
  private final List alPartExpiry = new ArrayList();
  private final List alWareHouse = new ArrayList();
  private HashMap hmTemp = new HashMap();
  private final HashMap hmShippingCnum = new HashMap();
  private final boolean bSetStatus = false;
  String strSkipPicSlipPartExpFl = ""; // PC-5132 To skip the Part Expiry Validation message in Pick Slip for FGRT & QNRT.
  String strTxnReturnType = "";

  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
  
  public List getConsequences() {
    return alConsequences;
  }

  public List getEmailConsequences() {
    return alEmailConsequences;
  }

  public List getValidRules() {
    return alValidRules;
  }

  public boolean execute(List alData) throws AppError {
    try {
      if (alData.size() == 0) {
        // throw new AppError(AppError.APPLICATION, "No Data Found to Process");
        return false;
      }
      Iterator itrValidateData = alData.iterator();
      while (itrValidateData.hasNext()) {
        GmRuleParamVO gmRuleParamVO = (GmRuleParamVO) itrValidateData.next();
        String strCompanyID = "";
        String strPnum = gmRuleParamVO.getPnum();
        String strCnum = gmRuleParamVO.getControlNumber();
        String strTxnSts = gmRuleParamVO.getTxnStatus();
        String strTxn = gmRuleParamVO.getTxn();
        String strCountry = gmRuleParamVO.getCountry();
        String strCountryId = gmRuleParamVO.getCountryID();
        String strRuleType = gmRuleParamVO.getRuleType();
        strTxnStatus = gmRuleParamVO.getTxnStatus();
        String strShipTo = gmRuleParamVO.getShipTo();
        String strAction = gmRuleParamVO.getAction();
        String strTxnTypeID = gmRuleParamVO.getTxnTypeID();
        String strTxnId = gmRuleParamVO.getTxnID();
        String strSelectedAction = GmCommonClass.parseNull((String)gmRuleParamVO.gethSelectedAction());
        log.debug("strTxnId in rule engine" +strTxnId);
        
        if(strTxnId.indexOf('-')!= -1){
        	strTxnReturnType = strTxnId.substring(0, strTxnId.indexOf('-'));           
        	strSkipPicSlipPartExpFl = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strTxnReturnType, "SKIP_EXPDT_PICSLIP"));
        	strSkipPicSlipPartExpFl = strSkipPicSlipPartExpFl.equals("") ? "N" : strSkipPicSlipPartExpFl;
        }
        
        // strCompanyID = GmCommonClass.parseNull((String) getTransactionCompanyId(strTxn));
        // //getTransactionCompanyId := Method to fetch the companyid based on Transaction.
        strCompanyID = strCompanyID.equals("") ? GmCommonClass.parseNull(gmRuleParamVO.getCmpid()) : strCompanyID;
        log.debug(" Outside strCompanyID : " + strCompanyID);
        // PC-3257 RFS Issue - Loaners shipped out without RFS
        strCompanyID = strCompanyID.equals("") ? GmCommonClass.parseNull((String) getTransactionCompanyId(strTxnId)) : strCompanyID;
        log.debug(" Outside strCompanyID1 : " + strCompanyID);
        String strSkipInternationalFl =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("PART",
                "INTERNATIONAL_USE_FL", strCompanyID));
        strSkipInternationalFl = strSkipInternationalFl.equals("") ? "Y" : strSkipInternationalFl;
        String strSkipPartExplFl =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("PART", "PART_EXPIRY_FLAG",
                strCompanyID));
        strSkipPartExplFl = strSkipPartExplFl.equals("") ? "Y" : strSkipPartExplFl;
        String strSkipRule =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("RLTXN", "SKIP_RULE",
                strCompanyID)); // need to skip rule validation for DHR related trasactions
        Boolean blSkipRuleFl = true;

        blSkipRuleFl = strSkipRule.indexOf("|" + strTxn + "|") != -1 ? false : true;
        
        String strSkipPartExpValid = skipPartExpireDateValidation(strTxnTypeID);// This method
                                                                                // checks the Part
                                                                                // Expire date
                                                                                // validation for
                                                                                // Trans ID

        // It is getting the Rule Value from rule table for Skip Release for validation Globus Cares
        // distributor.
        String strSkipRFSFl =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("RFS", "SKIP_RFS",
                strCompanyID));// Return N
        if (strSkipRFSFl.equals("N")) {
          strSkipRFSFl =
              GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strShipTo, "SKIPRFS",
                  strCompanyID));
          strSkipRFSFl = strSkipRFSFl.equals("") ? "N" : strSkipRFSFl;
        }
        strSkipRFSFl = strSkipRFSFl.equals("") ? "Y" : strSkipRFSFl;
        log.debug(" Outside strCnum : " + strCnum + " strPnum : " + strPnum + " strTxnStatus : "
            + strTxnStatus + "strTxnID: " + strTxn + " 	strTxnTypeID:" + strTxnTypeID);
        log.debug(" Outside continued strCountryId :" + strCountryId + "strRuleType :"
            + strRuleType + "strCompanyID :" + strCompanyID + " strSkipInternationalFl : "
            + strSkipInternationalFl + "strSkipPartExplFl :" + strSkipPartExplFl + "strSkipRule :"
            + strSkipRule + " strSkipRFSFl:" + strSkipRFSFl);
        if (blSkipRuleFl
            && strCnum != null
            && !strCnum.equals("")
            && (strCnum.toUpperCase().indexOf("NOC#") == -1 || !(strAction
                .equalsIgnoreCase("VERIFY") || strAction.equalsIgnoreCase("VERIFICATION")))
            && !strTxn.equals("50900")
            && !strTxn.equals("50918")
            && strCnum.toUpperCase().indexOf("TBE") == -1
            && (strPnum != null && !strPnum.equals(""))
            || (strCnum.toUpperCase().indexOf("TBE") != -1 && gmRuleParamVO.getTxnStatus()
                .equalsIgnoreCase("VERIFY"))) {
          String strDuplicateCnum = GmCommonClass.parseNull((String) hmShippingCnum.get(strPnum));
          if (!strDuplicateCnum.equals(strCnum)) {
            HashMap hmMap = validateControlNumber(strPnum, strCnum, strTxnSts, strTxnTypeID, strCompanyID);
            if (hmMap != null && hmMap.size() > 0) {
              alRFS.add(hmMap);
            }
          }
          hmShippingCnum.put(strPnum, strCnum);
        }
        if (strPnum != null && !strPnum.equals("") && strCountryId != null
            && !strCountryId.equals("0") && !strSkipRFSFl.equals("Y")) {
          String strCountryTemp = GmCommonClass.parseNull((String) hmTemp.get(strPnum));

          if (!strCountryTemp.equals(strCountryId)) {
            HashMap hmMap = fetchRFSMessage(strPnum, strCountryId, strRuleType, strTxnSts);
            if (hmMap != null && hmMap.size() > 0) {
              alRFS.add(hmMap);
            }
          }
          hmTemp.put(strPnum, strCountryId);
        }


        /*
         * Below condition is used to throw/skip rule validation for international use
         */

        if (blSkipRuleFl
            && strPnum != null
            && !strPnum.equals("")
            && strCountryId != null
            && !strCountryId.equals("")
            && !strCountryId.equals("0")
            && !strSkipInternationalFl.equalsIgnoreCase("Y")
            && !strCnum.equals("")
            || (!(strAction.equalsIgnoreCase("VERIFY") || strAction
                .equalsIgnoreCase("VERIFICATION"))) && !strTxn.equals("50900")
            && !strTxn.equals("50918")) {

          HashMap hmMap = fetchInternationalFlMessage(strPnum, strCnum, strCountryId);
          if (hmMap != null && hmMap.size() > 0) {
            alInternational.add(hmMap);
          }
        }

        /*
         * Below condition is used to throw/skip rule validation for part expiry
         */
      //PC-5648: Enable returns of expired parts from entities to owner company for Built set to Returns
       if(strSelectedAction.equals("BSTR")) {
    	   log.debug("strSelectedAction==>"+strSelectedAction); //To skip rule value for part expiry (BSTR-Build set to returns)
       }else {
        if ((blSkipRuleFl && !strSkipPartExplFl.equalsIgnoreCase("Y"))
            && (strPnum != null && !strPnum.equals("") && !strCnum.equals("") || (!(strAction
                .equalsIgnoreCase("VERIFY") || strAction.equalsIgnoreCase("VERIFICATION")))
                && !strTxn.equals("50900") && !strTxn.equals("50918"))) {
          if (!strSkipPartExpValid.equalsIgnoreCase("Y")) { 
        	  if (!strSkipPicSlipPartExpFl.equals("Y")) {
            HashMap hmMap = fetchPartExpiryMessage(strPnum, strCnum, strTxnStatus, strTxnTypeID,strTxnId);
            if (hmMap != null && hmMap.size() > 0) {
              alPartExpiry.add(hmMap);
             }
            }
          }
        }
       }

        /* Below condition is used to throw/skip rule validation for Inventory Mismatch */
        String strSkipWareHouseFl = "";
        try {
          strSkipWareHouseFl =
              GmCommonClass.parseNull(GmCommonClass.getString("VALIDATE_WAREHOUSE"));
        } catch (Exception ex) {
          strSkipWareHouseFl = "NO";
        }
        /*
         * strSkipWareHouseFl checks is Validation is for BBA or US blSkipRuleFl checks for
         * RULE_ENGINE is ON or OFF strPnum checks for Part number should not be null strCnum checks
         * for Control Number should not be null strAction checks for should fire the validation
         * during Transaction Control time. strTxn checks for Rule Message should not fire for
         * Transaction types 50900 -DHR-Pending Receiving and 50918 -SB-Built Sets to Quarantine
         */

        if (strSkipWareHouseFl.equalsIgnoreCase("YES")
            && blSkipRuleFl
            && strPnum != null
            && !strPnum.equals("")
            && !strCnum.equals("")
            || !(strAction.equalsIgnoreCase("VERIFY") || strAction.equalsIgnoreCase("VERIFICATION"))
            && !strTxn.equals("50900") && !strTxn.equals("50918")) { // 50900 -DHR-Pending Receiving
                                                                     // 50918 -SB-Built Sets to
                                                                     // Quarantine
          HashMap hmMap = fethchWareHouseMessage(strTxn, strPnum, strCnum, strTxnStatus);
          if (hmMap != null && hmMap.size() > 0) {
            alWareHouse.add(hmMap);
          }
        }
      }

      setConditionLevelDetails();
      Iterator itrAlData = alData.iterator();
      while (itrAlData.hasNext()) {
        GmRuleParamVO gmRuleParamVO = (GmRuleParamVO) itrAlData.next();
        HashMap hmRuleParamVO = gmRuleParamVO.toHashMap();
        strAvailableRules = "";
        alAvailableRules = new ArrayList();

        maxParamLevel = getMaxParamLevel(gmRuleParamVO.getTxn());
        strTxn = gmRuleParamVO.getTxn();
        strTxnStatus = gmRuleParamVO.getTxnStatus();

        for (int iLevel = 1; iLevel <= maxParamLevel; iLevel++) {
          boolean btree = createConditionString(iLevel, hmRuleParamVO);
          if (btree) {
            alAvailableRules.addAll(getAvaialableRules(iLevel));
            // log.debug("alAvailableRules"+alAvailableRules);
            setValidRules(iLevel, hmRuleParamVO);
          }
        }
      }

      HashMap hmAllConsequences = getConsequencesForValidRules();
      alEmailConsequences = (ArrayList) hmAllConsequences.get("EMAILCONS");
      alConsequences = (ArrayList) hmAllConsequences.get("CONSEQUENCE");

      if (alRFS.size() > 0) {
        alConsequences.addAll(alRFS);
      }
      if (alInternational.size() > 0) {
        alConsequences.addAll(alInternational);
      }
      if (alPartExpiry.size() > 0) {
        alConsequences.addAll(alPartExpiry);
      }
      if (alWareHouse.size() > 0) {
        alConsequences.addAll(alWareHouse);
      }
    } catch (Exception ex) {
      throw new AppError(ex);
    }

    return true;
  }

  private HashMap getConsequencesForValidRules() throws AppError {
    HashMap hmConsequence = new HashMap();
    HashMap hmEmailCons = new HashMap();
    GmDBManager manager = new GmDBManager();
    String strPnum = "";
    String strRuleID = "";
    HashMap hmRuleCons = new HashMap();
    String strRuleIDInCons = "";
    String strTempPnum = "";
    String strTempRuleId = "";

    String strTempCnum = "";
    String strRuleKey = "";
    String strRuleValue = "";
    String strEmailRuleIdCons = "";
    String strRuleString = "";

    Iterator itrValidRules = alValidRules.iterator();

    // log.debug("alValidRules: "+alValidRules);

    while (itrValidRules.hasNext()) {
      HashMap hmTemp = (HashMap) itrValidRules.next();
      strTempPnum = GmCommonClass.parseNull((String) hmTemp.get("PARTNUMBER"));
      strTempRuleId = GmCommonClass.parseNull((String) hmTemp.get("RULEID"));
      strTempCnum = GmCommonClass.parseNull((String) hmTemp.get("CONTROLNO"));
      if (strTempPnum.length() > 0 || strTempCnum.length() > 0) {
        hmPnumMap.put(strTempPnum + "-" + strTempCnum + ";" + strTempRuleId, strTempRuleId);
        // hmPnumMap.put(strTempPnum+"-"+strTempRuleId,strTempRuleId);
      }
    }


    manager.setPrepareString("gm_pkg_cm_rule_engine.gm_fch_consequences", 4);
    manager.registerOutParameter(3, OracleTypes.CURSOR);
    manager.registerOutParameter(4, OracleTypes.CURSOR);
    manager.setString(1, strValidRuleIds);
    manager.setString(2, strTxnStatus);
    manager.execute();
    ResultSet rs1 = (ResultSet) manager.getObject(3);
    ResultSet rs2 = (ResultSet) manager.getObject(4);

    ArrayList alConsequence = manager.returnArrayList(rs1);
    ArrayList alTempConsequence = new ArrayList();
    ArrayList alEmail = manager.returnArrayList(rs2);


    Iterator alConsequenceItr = alConsequence.iterator();
    Iterator alEmailItr = alEmail.iterator();
    // log.debug("hmPnumMap.keySet(): "+ hmPnumMap);
    // log.debug("alConsequence: "+alConsequence);
    // log.debug("hmPnumMap: "+hmPnumMap);
    while (alConsequenceItr.hasNext()) {
      hmRuleCons = (HashMap) alConsequenceItr.next();
      strRuleIDInCons = (String) hmRuleCons.get("ID");
      // strRuleCons = (String)hmRuleCons.get("CONDITION");
      Iterator ruleIdItr = hmPnumMap.keySet().iterator();

      while (ruleIdItr.hasNext())// && !strRuleCons.equals("7"))
      {
        strPnum = (String) ruleIdItr.next();
        strRuleID = (String) hmPnumMap.get(strPnum);

        if (strRuleIDInCons.equals(strRuleID)) {
          String strVPnum = GmCommonClass.parseNull((String) hmRuleCons.get("PNUM"));
          strPnum = strPnum.substring(0, strPnum.indexOf("-"));
          // log.debug("strPnum: "+strPnum);

          if (strVPnum.equals("")) {

            hmRuleCons.put("PNUM", strPnum);
          } else {
            HashMap hmNewCons = new HashMap(hmRuleCons);
            hmNewCons.put("PNUM", strPnum);
            alTempConsequence.add(hmNewCons);
          }
          // break;
        }
      }
    }

    while (alEmailItr.hasNext()) {
      hmEmailCons = (HashMap) alEmailItr.next();
      strEmailRuleIdCons = (String) hmEmailCons.get("ID");
      Iterator emailRuleIdItr = hmPnumMap.keySet().iterator();

      while (emailRuleIdItr.hasNext()) {
        strRuleKey = (String) emailRuleIdItr.next();
        strRuleValue = (String) hmPnumMap.get(strRuleKey);
        if (strEmailRuleIdCons.equals(strRuleValue)) {
          strRuleKey = strRuleKey.substring(0, strRuleKey.indexOf(";"));
          strRuleString = strRuleString + strRuleKey + ",";
        }
      }
      hmEmailCons.put("STRRULESTRING", strRuleString);
      strRuleString = "";
    }
    // log.debug("alEmail: " + alEmail);
    alConsequence.addAll(alTempConsequence);
    // log.debug("alConsequence: " + alConsequence);
    hmConsequence.put("CONSEQUENCE", alConsequence);
    hmConsequence.put("EMAILCONS", alEmail);

    // log.debug("hmConsequence: "+ hmConsequence);
    // log.debug("hmPnumMap: "+ hmPnumMap);

    manager.close();
    return hmConsequence;
  }


  private int getMaxParamLevel(String strTxn) throws Exception {
    int paramLevel = 0;
    // log.debug("strTxn: "+strTxn);
    GmDBManager manager = new GmDBManager();
    manager.setPrepareString("gm_pkg_cm_rule_engine.gm_fch_max_param_level", 2);
    manager.registerOutParameter(2, OracleTypes.VARCHAR);
    manager.setString(1, strTxn);
    manager.execute();
    paramLevel = Integer.parseInt(GmCommonClass.parseZero(manager.getString(2)));
    manager.close();
    // log.debug("paramLevel: "+paramLevel);
    return paramLevel;
  }

  private void setConditionLevelDetails() throws Exception {
    HashMap hmLevel = new HashMap();
    ArrayList alConditions = new ArrayList();
    GmDBManager manager = new GmDBManager();
    manager.setPrepareString("gm_pkg_cm_rule_engine.gm_fch_conditions", 1);
    manager.registerOutParameter(1, OracleTypes.CURSOR);
    manager.execute();
    ResultSet rs = (ResultSet) manager.getObject(1);
    alConditions = manager.returnArrayList(rs);
    String strLevel = "";
    String strValue = "";
    List alConditionNames;
    hmConditionLevelDetails = new HashMap();

    // //////log.debug("alConditions: "+alConditions);

    Iterator sConditionsKeyItr = alConditions.iterator();

    while (sConditionsKeyItr.hasNext()) {

      hmLevel = (HashMap) sConditionsKeyItr.next();
      strLevel = (String) hmLevel.get("CON_LEVEL");
      strValue = (String) hmLevel.get("CONDITIONS");
      alConditionNames = Arrays.asList(strValue.split(","));
      hmConditionLevelDetails.put(strLevel, alConditionNames);
    }
    manager.close();
    // //////log.debug("hmConditionLevelDetails: "+hmConditionLevelDetails);
  }

  private boolean createConditionString(int iLevel, HashMap hmUserConditions) {
    // ////log.debug("hmUserConditions: "+ hmUserConditions);

    strReqParameterInputString = "";
    List alConditionsWithLevel = (List) hmConditionLevelDetails.get("" + iLevel);
    Iterator alConditionLevelItr = alConditionsWithLevel.iterator();
    String strKey = "";
    String strValue = "";

    while (alConditionLevelItr.hasNext()) {
      strKey = (String) alConditionLevelItr.next();
      strValue = GmCommonClass.parseNull((String) hmUserConditions.get(strKey));
      if (!strValue.equals("")) {
        strReqParameterInputString += strKey + "," + strValue + "|";
      }
    }
    // ////log.debug("strReqParameterInputString.length() : " +
    // strReqParameterInputString.length());
    return strReqParameterInputString.equals("") ? false : true;
  }

  private ArrayList getAvaialableRules(int iLevel) throws Exception {
    // //log.debug("strReqParameterInputString: "+strReqParameterInputString);
    // //log.debug("strAvailableRules: "+strAvailableRules);
    // //log.debug("strTxn: "+strTxn);
    // //log.debug("strTxnStatus: "+strTxnStatus);
    // //log.debug("iLevel: "+iLevel);

    ArrayList alList = new ArrayList();
    String strRuleId = "";
    GmDBManager manager = new GmDBManager(getGmDataStoreVO());

    manager.setPrepareString("gm_pkg_cm_rule_engine.gm_fch_available_rules", 5);
    manager.registerOutParameter(5, OracleTypes.CURSOR);
    manager.setString(1, strReqParameterInputString);
    // log.debug("strAvailableRules 1 = "+strAvailableRules);
    manager.setString(2, strAvailableRules);
    manager.setString(3, strTxn);
    manager.setInt(4, iLevel);
    manager.execute();
    ResultSet rs = (ResultSet) manager.getObject(5);
    alList = manager.returnArrayList(rs);
    // log.debug("alList: "+alList);
    manager.close();
    return alList;
  }


  private ArrayList getConditionsForRule(String strRuleCondition) {
    ArrayList alConditions = new ArrayList();

    StringTokenizer strRuleToken = new StringTokenizer(strRuleCondition, "|;");
    StringTokenizer strConditionToken;

    String strCondition = "";
    // log.debug("strRuleToken: "+strRuleToken);
    while (strRuleToken.hasMoreTokens()) {
      strCondition = strRuleToken.nextToken();
      strConditionToken = new StringTokenizer(strCondition, "^");
      String strConditionName = strConditionToken.nextToken();
      String strOperator = strConditionToken.nextToken();
      String strValue = strConditionToken.nextToken();

      HashMap hmConditionMap = new HashMap();
      hmConditionMap.put("CONDITIONNAME", strConditionName);
      hmConditionMap.put("OPERATOR", strOperator);
      hmConditionMap.put("VALUE", strValue);
      // log.debug("hmConditionMap: "+hmConditionMap);
      alConditions.add(hmConditionMap);
    }

    return alConditions;
  }

  private void setValidRules(int iLevel, HashMap hmRuleValuesFromScreen) {
    ArrayList tmpList = new ArrayList(alAvailableRules);
    Iterator itrTmpList = tmpList.iterator();
    String strRuleLevel = "";
    String strRuleId = "";
    String strRuleCondition = "";
    ArrayList alConditions = new ArrayList();
    Iterator conditionItr;
    HashMap hmRuleCondition;
    String strConditionName;
    String strOperator;
    String strValue;
    String strValueToCheck;
    String strScreenConditions = "";

    String strControlNumber =
        GmCommonClass.parseNull((String) hmRuleValuesFromScreen.get("CONTROLNO"));
    strControlNumber = strControlNumber.replaceAll(" ", "");
    strControlNumber = strControlNumber.toUpperCase();

    boolean bRuleRemoved;

    HashMap hmSetId = null;
    while (itrTmpList.hasNext()) {
      bRuleRemoved = false;
      HashMap hmAvailableRule = (HashMap) itrTmpList.next();
      strRuleLevel = (String) hmAvailableRule.get("RULELEVEL");
      strRuleId = (String) hmAvailableRule.get("RULEID");
      strRuleCondition = (String) hmAvailableRule.get("CONDITIONS");
      strScreenConditions = "";

      strAvailableRules += strRuleId + ",";
      // log.debug("strRuleCondition = "+strRuleCondition);
      if (strRuleLevel.equals("" + iLevel)) // Allow the current Level if the control number is not
                                            // NOC#
      {

        // Convert the condition String to Arraylist of HashMaps
        alConditions = getConditionsForRule(strRuleCondition);
        // log.debug("alConditions = "+alConditions);
        conditionItr = alConditions.iterator();

        while (conditionItr.hasNext()) {
          hmRuleCondition = (HashMap) conditionItr.next();
          strConditionName = GmCommonClass.parseNull((String) hmRuleCondition.get("CONDITIONNAME"));
          strOperator = (String) hmRuleCondition.get("OPERATOR");
          strValueToCheck = (String) hmRuleCondition.get("VALUE");
          strValue = GmCommonClass.parseNull((String) hmRuleValuesFromScreen.get(strConditionName));

          // log.debug("strConditionName = "+strConditionName+" strValue = "+strValue+" strOperator = "+strOperator+" strValueToCheck = "+strValueToCheck+"checkConditionValues"+checkConditionValues(strValue,strOperator,
          // strValueToCheck));

          if (!checkConditionValues(strValue, strOperator, strValueToCheck)) { // Firstly check if
                                                                               // condition name is
                                                                               // 'PARTNUMBER' or
                                                                               // 'COUNTRY', then
                                                                               // remove rules


            if (strConditionName.equals("PARTNUMBER")) {
              itrTmpList.remove();
              bRuleRemoved = true;
              break;
            }

            /*
             * HELPDESK ID : 5678, If NOC# is entered as the control number in the transaction
             * screen then all the rules should be displayed for that corresponding part number.
             */

            /* Commented for MNTTASK-2771 control number changes */
            if (strControlNumber.indexOf("NOC") != -1) {
              // do nothing
            } else {
              itrTmpList.remove();
              bRuleRemoved = true;
              break;
            }

          } else {

            strScreenConditions = strScreenConditions + strConditionName + "=" + strValue + " AND ";
            hmAvailableRule.putAll(hmRuleValuesFromScreen);

          }
        }

        if (!bRuleRemoved) {
          strValidRuleIds += strRuleId + ",";
          // log.debug("TESTTEST 3 = "+strValidRuleIds);
          if (strScreenConditions.length() > 0) {
            // log.debug(" strScreenConditions.lastIndexOf(AND) "+strScreenConditions.lastIndexOf("AND")
            // + " - " + strScreenConditions.substring(0, strScreenConditions.lastIndexOf("AND")
            // -1));
            strScreenConditions =
                strScreenConditions.substring(0, strScreenConditions.lastIndexOf("AND") - 1);
            hmAvailableRule.put("SCRCONS", strScreenConditions);
            hmAvailableRule.put("TXN", hmRuleValuesFromScreen.get("TXN"));
          }
        }
      } else {
        itrTmpList.remove();
      }
    }


    alValidRules.addAll(tmpList);
    // log.debug("-alValidRules---------------"+alValidRules);
  }

  private boolean isNumericComparision(String valueToCheck, String actualValue) {
    boolean b = false;
    int i = 0;
    try {
      i = Integer.parseInt(valueToCheck) + Integer.parseInt(actualValue);
      if (i > 0) {
        b = true;
      }
    } catch (Exception ex) {
      b = false;
    }
    return b;
  }

  private boolean isDateComparision(String valueToCheck, String actualValue) {
    boolean b = false;
    Pattern p = Pattern.compile(REGEX);
    Matcher m1 = p.matcher(valueToCheck);
    Matcher m2 = p.matcher(actualValue);
    if (m1.matches() == true && m2.matches() == true) {
      b = true;
    }
    return b;
  }

  private boolean checkDateValues(String valueToCheck, String operator, String actualValue) {
    DateFormat formatter;
    Date valueToCheckDate, actualValueDate;
    try {
      formatter = new SimpleDateFormat("MM/dd/yyyy");
      valueToCheckDate = formatter.parse(valueToCheck);
      actualValueDate = formatter.parse(actualValue);
      int iDate = valueToCheckDate.compareTo(actualValueDate);
      if (iDate == 1 && (operator.equals(">=") || operator.equals(">")))
        return true;
      else if (iDate == 0 && operator.equals("="))
        return true;
      else if (iDate == -1 && (operator.equals("<=") || operator.equals("<")))
        return true;

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return false;
  }

  private boolean checkNumericValues(int iValuetoCheck, String operator, int iActualValue) {
    boolean breturn = false;

    if (operator.equals("=")) {
      breturn = (iValuetoCheck == iActualValue);
    } else if (operator.equals(">=")) {
      breturn = (iValuetoCheck >= iActualValue);
    } else if (operator.equals("<=")) {
      breturn = (iValuetoCheck <= iActualValue);
    } else if (operator.equals(">")) {
      breturn = (iValuetoCheck > iActualValue);
    } else if (operator.equals("<")) {
      breturn = (iValuetoCheck < iActualValue);
    } else if (operator.equals("!=")) {
      breturn = (iValuetoCheck != iActualValue);
    }
    return breturn;
  }

  private boolean checkConditionValues(String valueToCheck, String operator, String actualValue) {
    boolean breturn = false;
    if (isDateComparision(valueToCheck, actualValue)) {
      return checkDateValues(valueToCheck, operator, actualValue);
    }
    if (isNumericComparision(valueToCheck, actualValue)) {
      return checkNumericValues(Integer.parseInt(valueToCheck), operator,
          Integer.parseInt(actualValue));
    } else if (valueToCheck.equals("") && !actualValue.equals("")) {
      breturn = false;
    } else if (operator.equals("=")) {
      breturn = valueToCheck.equals(actualValue);
    } else if (operator.equals(">=")) {
      int result = valueToCheck.compareTo(actualValue);
      if (result == 0 || result > 0)
        breturn = true;
    } else if (operator.equals("<=")) {
      int result = valueToCheck.compareTo(actualValue);
      if (result == 0 || result < 0)
        breturn = true;
    } else if (operator.equals(">")) {
      int result = valueToCheck.compareTo(actualValue);
      if (result > 0)
        breturn = true;
    } else if (operator.equals("<")) {
      int result = valueToCheck.compareTo(actualValue);
      if (result < 0)
        breturn = true;
    } else if (operator.equals("!=")) {
      breturn = !valueToCheck.equals(actualValue);
    } else if (operator.equals("LIKE")) {
      if (actualValue.indexOf(",") == -1) {
        breturn = valueToCheck.indexOf(actualValue) != -1 ? true : false;
      } else {
        String[] strActualValues = actualValue.split(",");
        for (int count = 0; count < strActualValues.length; count++) {
          breturn =
              valueToCheck.indexOf(strActualValues[count].replaceAll(" ", "")) != -1 ? true : false;
          if (breturn) {
            break;
          }
        }
      }
    } else if (operator.equals("IN")) {
      // log.debug("valueToCheck: " + valueToCheck + " : " + "actualValue: " + actualValue);
      breturn = actualValue.indexOf(valueToCheck) != -1 ? true : false;
    }
    // log.debug("COmparision Result:" + breturn );
    return breturn;
  }

  public String getReturnType() {
    if (strTxnStatus.equalsIgnoreCase("VERIFY")) {
      strReturnType = "ERROR";
    }

    return strReturnType;
  }


  public HashMap fetchRFSMessage(String strPartNum, String strCountryId, String strRuleType,
      String strTxnSts) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    HashMap hmReturn = null;

    gmDBManager.setPrepareString("gm_pkg_common.gm_chk_rfs_country", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strCountryId);

    gmDBManager.execute();
    String strErrorMessage = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();

    if (!strErrorMessage.equals("")) {
      hmReturn = new HashMap();
      hmReturn.put("ID", "RFS");
      hmReturn.put("NAME", "Release For Sale Validation");
      hmReturn.put("PNUM", strPartNum);
      if (strRuleType.equals("Shipping") || strRuleType.equals("Bill_Ship")
          || strTxnSts.equalsIgnoreCase("VERIFY")) {
        hmReturn.put("HOLDTXN", "Y");
      } else {
        hmReturn.put("HOLDTXN", "");
      }
      hmReturn.put("PICTURE", "");
      hmReturn.put("UPDBY", "");
      hmReturn.put("UPDDATE", "");
      hmReturn.put("MESSAGE", strErrorMessage);
    }

    return hmReturn;
  }

  public HashMap fetchInternationalFlMessage(String strPartNum, String strControlNum,
      String strCountryCode) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    HashMap hmReturn = null;
    String strProdType = "";
    String strTxnSts = "";
    strProdType = GmPartNumBean.fetchPartMaterialType(strPartNum);
    strTxnSts =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strProdType, "INTERNTL_USE_TXN"));

    gmDBManager.setPrepareString("gm_pkg_common.gm_chk_international_flag", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strControlNum);
    gmDBManager.setString(3, strCountryCode);
    gmDBManager.execute();
    String strErrorMessage = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.close();

    if (!strErrorMessage.equals("")) {
      hmReturn = new HashMap();
      hmReturn.put("ID", "BBA");
      hmReturn.put("NAME", "International use validation");
      hmReturn.put("PNUM", strPartNum);
      if (strTxnSts.equalsIgnoreCase("Y")) {
        hmReturn.put("HOLDTXN", "Y");
      } else {
        hmReturn.put("HOLDTXN", "");
      }
      hmReturn.put("PICTURE", "");
      hmReturn.put("UPDBY", "");
      hmReturn.put("UPDDATE", "");
      hmReturn.put("MESSAGE", strErrorMessage);
    }

    return hmReturn;
  }


  public HashMap fetchPartExpiryMessage(String strPartNum, String strControlNum,
      String strTxnStatus, String strTxnTypeID,String strTxnId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    HashMap hmReturn = null;
    gmDBManager.setPrepareString("gm_pkg_common.gm_chk_part_expiry", 8);
    gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strControlNum);
    gmDBManager.setString(3, strTxnStatus);
    gmDBManager.setString(4, strTxnTypeID);
    gmDBManager.setString(5, strTxnId);
    gmDBManager.execute();
    String strErrorMessage = GmCommonClass.parseNull(gmDBManager.getString(6));
    String strTxnSts = GmCommonClass.parseNull(gmDBManager.getString(7));
    String strExpType = GmCommonClass.parseNull(gmDBManager.getString(8));
    gmDBManager.close();

    if (!strErrorMessage.equals("")) {
      hmReturn = new HashMap();
      hmReturn.put("ID", "BBA");
      hmReturn.put("NAME", "Part expiry validation");
      hmReturn.put("PNUM", strPartNum + " | " + strControlNum);
      if (strTxnSts.equalsIgnoreCase("Y")) {
        hmReturn.put("HOLDTXN", "Y");
      } else {
        hmReturn.put("HOLDTXN", "");
      }
      hmReturn.put("PICTURE", "");
      hmReturn.put("UPDBY", "");
      hmReturn.put("UPDDATE", "");
      hmReturn.put("MESSAGE", strErrorMessage);
      hmReturn.put("EXPTYPE", strExpType);
    }
    return hmReturn;
  }

  public HashMap validateControlNumber(String strPartNum, String strCntrlNum, String strTxnSts,
      String strTxnType, String strCompanyID) throws AppError {
    HashMap hmReturn = null;
    log.debug("ValidateControlNumber...." + strPartNum + "::" + strCntrlNum + "::" + strTxnSts
        + "::" + strTxnType);
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_common.gm_chk_cntrl_num", 5);
    gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strPartNum);
    gmDBManager.setString(2, strCntrlNum);
    gmDBManager.setString(3, strTxnType);
    gmDBManager.setString(4, strCompanyID);
    gmDBManager.execute();
    String strErrorMessage = GmCommonClass.parseNull(gmDBManager.getString(5));
    gmDBManager.close();

    if (!strErrorMessage.equals("")) {
      log.debug("strErrorMessage : " + strErrorMessage);

      hmReturn = new HashMap();
      hmReturn.put("ID", "CNV");
      hmReturn.put("NAME", "Control Number Validation");
      hmReturn.put("PNUM", strPartNum);
      if (strTxnSts.equalsIgnoreCase("VERIFY")) {
        hmReturn.put("HOLDTXN", "Y");
      } else {
        hmReturn.put("HOLDTXN", "");
      }
      hmReturn.put("PICTURE", "");
      hmReturn.put("UPDBY", "");
      hmReturn.put("UPDDATE", "");
      hmReturn.put("MESSAGE", strErrorMessage);
    }

    return hmReturn;
  }

  public void sendRFSEmail(String TnxID, ArrayList alReturn, String strType) throws AppError {

    if (alReturn.size() > 0) {
      GmEmailProperties gmEmailProperties = getEmailProperties(TEMPLATE_NAME);

      StringBuffer sbDetails = new StringBuffer();
      String strMessage = gmEmailProperties.getMessage();
      String strSubject = gmEmailProperties.getSubject();

      for (int i = 0; i < alReturn.size(); i++) {
        hmTemp = (HashMap) alReturn.get(i);

        String strID = (String) hmTemp.get("ID");
        if (strID.equals("RFS")) {
          String strM = (String) hmTemp.get("MESSAGE");
          sbDetails.append(strM);
          sbDetails.append("<BR/><BR/>");
        }

      }
      strSubject = GmCommonClass.replaceAll(strSubject, "#<TXN_TYPE>", strType);
      strMessage = GmCommonClass.replaceAll(strMessage, "#<TXN_ID>", TnxID);
      strMessage = GmCommonClass.replaceAll(strMessage, "#<MESSAGES>", sbDetails.toString());
      gmEmailProperties.setSubject(strSubject);
      gmEmailProperties.setMessage(strMessage);
      // gmEmailProperties.setRecipients("xqu@globusmedical.com");
      gmEmailProperties.setRecipients(GmCommonClass.getEmailProperty("GmRuleEngine."
          + GmEmailProperties.TO));
      gmEmailProperties.setCc(GmCommonClass
          .getEmailProperty("GmRuleEngine." + GmEmailProperties.CC));

      try {
        GmCommonClass.sendMail(gmEmailProperties);
      } catch (Exception e) {
        GmLogError.log("Exception in GmRuleEngine:sendRFSEmail", "Exception is:" + e);
      }

    }

  }

  public GmEmailProperties getEmailProperties(String strTemplate) {
    GmEmailProperties emailProps = new GmEmailProperties();
    emailProps
        .setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
    emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.SUBJECT));
    emailProps.setMessage(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.MESSAGE));
    emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "."
        + GmEmailProperties.MIME_TYPE));
    // emailProps.setMessageFooter(GmCommonClass.getEmailProperty(strTemplate + "." +
    // GmEmailProperties.MESSAGE_FOOTER));
    return emailProps;
  }

  /**
   * chkPartExpireDateValidation - This method check the transaction need Part Expire Date
   * Validation or not
   * 
   * @param String
   * @return String
   */
  public String skipPartExpireDateValidation(String strTxnTypeID) {
    String retVal = "";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_common.gm_skip_part_expdate_valid", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strTxnTypeID);
    gmDBManager.execute();
    retVal = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();

    return retVal;
  }

  /**
   * fethchWareHouseMessage - This method check the transaction need Warehouse Miss Match Validation
   * or not
   * 
   * @param String strTransactionType,String strPartNum, String strControlNum, String strTxnStatus
   * @return HashMap
   * @author Harinadh Reddi
   * @throws AppError
   */
  public HashMap fethchWareHouseMessage(String strTransactionType, String strPartNum,
      String strControlNum, String strTxnStatus) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strCompanyId = getGmDataStoreVO().getCmpid();
    String strSkipWHValidationFL =
            GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("VALIDATION",
                "WAREHOUSE_VLD_FL", strCompanyId));
    String strLotTrackFL =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "LOT_TRACK"));

    HashMap hmReturn = null;
    // Getting the Rule Warehouse for the input Transaction Type.
    String strRuleWareHouse =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strTransactionType, "RLTXNWAREHOUSE"));
    String strCurrWareHouse = "";
    String strRuleMessage = "";
    String strRuleInvName = "";
    String strCurrInvName = "";
    String ruleName = "";
    // Getting the Current Ware house for the input Part Number and COntrol Number Combination
    if (!strRuleWareHouse.equals("")) {
      gmDBManager.setPrepareString("gm_pkg_op_inv_warehouse.gm_fetch_curr_warehouse", 3);
      gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
      gmDBManager.setString(1, strPartNum);
      gmDBManager.setString(2, strControlNum);
      gmDBManager.execute();
      strCurrWareHouse = GmCommonClass.parseNull(gmDBManager.getString(3));
      gmDBManager.close();
    }
    /* if the control number is not TBE  and 
     * if the part is available in T2550 for the particular plant and lot
     * and if the lot track is enabled for the company then throw the "Plant Mismatch Validation" validation */
    
    strControlNum = strControlNum.trim().equals("") ? "TBE" : strControlNum;
   if(!strControlNum.equals("TBE") && strCurrWareHouse.equals("99999") && strLotTrackFL.equalsIgnoreCase("Y")) {	  
	  strRuleMessage =
	          strRuleMessage + "Allograft ID " + strControlNum + " is unavailable in " + gmCommonBean.getPlantName(getCompPlantId()) + " Plant ";
	  ruleName = "Plant Mismatch Validation";
   }
   
   /*check if the warehouse validation is applicable for the company [strSkipWHValidationFL] and 
    * current warehouse should not be null for the lot  and
    * current warehouse should not match with rule warehouse then throw the  "Warehouse Mismatch Validation" */
   
   else if (!strSkipWHValidationFL.equalsIgnoreCase("YES") && (!strCurrWareHouse.equals("") && !strCurrWareHouse.equals("99999") && !strCurrWareHouse.equals(strRuleWareHouse))) {
	  // Getting the Inventory Names for the Inventory Types.
	    strRuleInvName = GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strRuleWareHouse));
	    strCurrInvName = GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strCurrWareHouse));
	    
      strRuleMessage =
          strRuleMessage + "The Control Number " + strControlNum + " entered for the part "
              + strPartNum + " is currently not available in " + strRuleInvName
              + " and it is available in " + strCurrInvName;
      ruleName = "Warehouse Mismatch Validation";
   }
   if (!strRuleMessage.equals("")){
      hmReturn = new HashMap();
      hmReturn.put("ID", "BBA");
      hmReturn.put("NAME", ruleName);
      hmReturn.put("PNUM", strPartNum + " | " + strControlNum);
      hmReturn.put("HOLDTXN", "Y");
      hmReturn.put("PICTURE", "");
      hmReturn.put("UPDBY", "");
      hmReturn.put("UPDDATE", "");
      hmReturn.put("MESSAGE", strRuleMessage);
   }
    return hmReturn;
  }

  /**
   * getTransactionCompanyId - This method is used to fetch the CompanyID based on transaction
   * 
   * @param String strTxn
   * @return String
   * @author kSomanathan
   * @throws AppError
   */

  public String getTransactionCompanyId(String strTxn) throws AppError {
    // TODO Auto-generated method stub
    String strTransCompID = "";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_common.gm_fch_trans_comp_id", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strTxn);
    gmDBManager.execute();
    strTransCompID = GmCommonClass.parseNull(gmDBManager.getString(2));
    gmDBManager.close();
    log.debug("getTransactionCompanyId>>>>> : " + strTransCompID);
    return strTransCompID;
  }


}
