/**
 * FileName    : GmItemConsignRuleWrapper.java 
 * Description :
 * Author      : vprasath
 * Date        : Jun 9, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmRuleBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 *
 */
public class GmItemConsignRuleWrapper extends GmRuleEngineWrapper {

	/* (non-Javadoc)
	 * @see com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest)
	 */
	List alList;
	String strInputString = "";
	String strTxnValue = "";
	public List formatData(HttpServletRequest request) {		
		alList = new ArrayList();
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		gmDataStoreVO = GmFramework.getCompanyParams(request);
		GmRuleBean gmRuleBean = new GmRuleBean(gmDataStoreVO);
		GmOperationsBean gmOper = new GmOperationsBean(gmDataStoreVO);
		GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(gmDataStoreVO);
		ArrayList alSetLoad = new ArrayList();
		HashMap hmReturn = new HashMap();
		HashMap hmConDetail = new HashMap();
		HashMap hmValues = new HashMap();
		HashMap hmResult = new HashMap();
		String strCompanyID = gmDataStoreVO.getCmpid();
		String strTxn = GmCommonClass.parseNull((String)request.getAttribute("RE_TXN"));
		strTxn = strTxn.equals("")?GmCommonClass.parseNull((String)request.getParameter("RE_TXN")):strTxn;
        String strTxnStatus = GmCommonClass.parseNull((String)request.getAttribute("txnStatus"));
        String strTxnTypeID = GmCommonClass.parseNull((String)request.getAttribute("TxnTypeID"));
        strTxnTypeID = strTxnTypeID.equals("")?GmCommonClass.parseNull((String)request.getParameter("TXN_TYPE_ID")):strTxnTypeID;
        strTxnStatus = strTxnStatus.equals("")?GmCommonClass.parseNull((String)request.getParameter("txnStatus")):strTxnStatus;
        String strAction = GmCommonClass.parseNull((String)request.getParameter("hAction"));
        String strSetId = GmCommonClass.parseNull((String)request.getParameter("consignSetId"));
        hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));
        hmConDetail = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("CONDETAILS"));
        alSetLoad = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("SETLOAD"));
        
        String strConsignId = GmCommonClass.parseNull((String)request.getAttribute("hConsignId"));
        strConsignId = strConsignId.equals("")?GmCommonClass.parseNull((String)request.getParameter("hConsignId")):strConsignId;
        
//        log.debug("strSetId...."+hmConDetail.get("SETID"));
//        log.debug("hConsignId...."+strConsignId);
//		log.debug("hmConDetail "+hmConDetail);
//		log.debug("alSetLoad "+alSetLoad);
//		log.debug("strTxn = "+strTxn);
//		log.debug("strAction = "+strAction);
//		log.debug("strTxnTypeID = "+strTxnTypeID);
		if(strTxn.equals("50908")||strTxn.equals("50926")||strTxn.equals("50929"))
		{
			String strTxnId =GmCommonClass.parseNull((String)request.getParameter("hTxnId"));
			strConsignId = strTxnId.equals("")?strConsignId:strTxnId; // This line of code is added for RMFG
		}
		try
		{
			String strCountry = GmCommonClass.parseNull(gmRuleBean.fetchCNCountry(strConsignId));
			

			strCountry = strCountry.equals("")?"US":strCountry; 
			strTxnValue = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strTxnTypeID, "RULETXN"));
			strTxn = strTxnValue.equals("")?strTxn:strTxnValue;
            
	        if((alSetLoad.size() > 0 && hmConDetail.size() > 0) || strAction.equals("Verify"))
	        {
	        	if(strAction.equals("Verify"))
	        	{
	        		if(strTxn.equals("50908")||strTxn.equals("50926"))
	        		{
	        			alSetLoad = gmInHouseRptBean.loadSavedSetMaster(strConsignId);		        		
	        		}
	        		else
	        		{
		        		hmResult = gmOper.loadSavedSetMaster(strConsignId);
		        		alSetLoad = GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("SETLOAD"));
						hmConDetail = gmOper.viewBuiltSetDetails(strConsignId);
	        		}
					log.debug("alSetLoad ="+alSetLoad);
					
	        	}
	        	for (int i=0; i< alSetLoad.size(); i++)
				{
	        		hmValues = (HashMap)alSetLoad.get(i);
					GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
					gmRuleParamVO.setPnum((String)hmValues.get("PNUM"));
					//log.debug("Cnum ..... "+ (String)hmValues.get("CNUM"));
					gmRuleParamVO.setControlNumber((String)hmValues.get("CNUM"));
					gmRuleParamVO.setTxn(strTxn);
					gmRuleParamVO.setTxnStatus(strTxnStatus);
					gmRuleParamVO.setQty((String)hmValues.get("IQTY"));
					gmRuleParamVO.setSetID((String)hmConDetail.get("SETID"));
					gmRuleParamVO.setCountry(strCountry);
					gmRuleParamVO.setTxnID(strConsignId);
					gmRuleParamVO.setAction(strAction);
					gmRuleParamVO.setTxnTypeID(strTxnTypeID);
					gmRuleParamVO.setCmpid(strCompanyID);
					alList.add(gmRuleParamVO);
				}
	        }
	        
	        else
	        {
	        	if (strAction.equals("SaveControl")||strAction.equals("ReleaseControl"))
                {
	        		log.debug("strAction....."+strAction);
	        		strInputString = GmCommonClass.parseNull(formateString(request));
                    log.debug("string....."+strInputString);
                    
                    
                    if(!strInputString.equals(""))
		        	{
		        		String[] strArray = strInputString.split("\\|");
						log.debug("strArray..."+strArray.length);
						for (int j=0; j<strArray.length; j++)
						{
							String[] strValues = strArray[j].split("\\,");
							GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
							gmRuleParamVO.setPnum(strValues[0]);
							gmRuleParamVO.setQty(strValues[1]);
							gmRuleParamVO.setControlNumber(strValues[2]);					
							gmRuleParamVO.setTxn(strTxn); 
							gmRuleParamVO.setTxnStatus(strTxnStatus);						
							gmRuleParamVO.setSetID(strSetId);
							gmRuleParamVO.setCountry(strCountry);
							gmRuleParamVO.setTxnID(strConsignId);
							gmRuleParamVO.setAction(strAction);
							gmRuleParamVO.setTxnTypeID(strTxnTypeID);
							gmRuleParamVO.setCmpid(strCompanyID);
							alList.add(gmRuleParamVO);									
						}
		        	}
		        	
                }	        	
	        }
		}
	catch(Exception exp)
		{
			exp.printStackTrace();
		}
        log.debug("alList "+alList);
		return alList;
	}
	
	private String formateString(HttpServletRequest request)
	{
		String strCount = "";
		String strPartNum = "";
		String strQty = "";
        String strControl = "";       
        
        int intCount = 0;
        strCount = request.getParameter("hCnt")==null?"0":request.getParameter("hCnt");
        intCount = Integer.parseInt(strCount);
        log.debug("strCount....."+strCount);
        for (int i=0;i<=intCount;i++)
        {
            strPartNum = request.getParameter("hPartNum"+i)==null?"":request.getParameter("hPartNum"+i);
            if (!strPartNum.equals(""))
            {
                strQty = GmCommonClass.parseNull(request.getParameter("Txt_Qty"+i));
                log.debug("strQty = "+strQty);
                strQty = strQty.equals("")?"0":strQty;
                strControl = GmCommonClass.parseNull(request.getParameter("Txt_CNum"+i));
                log.debug("strControl = "+strControl);
                strControl = strControl.equals("")?" ":strControl;
                strInputString = strInputString.concat(strPartNum).concat(",").concat(strQty).concat(",").concat(strControl).concat("|");
            }
        }
        return strInputString;
	}
}


