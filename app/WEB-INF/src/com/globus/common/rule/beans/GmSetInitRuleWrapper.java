/**
 * FileName    : GmSetConsignRuleWrapper.java 
 * Description :
 * Author      : vprasath
 * Date        : Jun 9, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmRuleBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.valueobject.common.GmDataStoreVO;

 
public class GmSetInitRuleWrapper extends GmRuleEngineWrapper {
 
	List alList;
	public List formatData(HttpServletRequest request){
		alList = new ArrayList();
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		gmDataStoreVO = GmFramework.getCompanyParams(request);
		GmRuleBean gmRuleBean = new GmRuleBean(gmDataStoreVO);
		GmProjectBean gmProjectBean  = new GmProjectBean(gmDataStoreVO);
		 
		HashMap hmValues = new HashMap();
		ArrayList alData = new ArrayList();
		String strTxn = "";
		 
		String strNameId = "";
		String strShipToId = "";  
		String strSetId = "";
		String strAddressId = "";
		String strCompanyID = gmDataStoreVO.getCmpid();
		String strTxnStatus = GmCommonClass.parseNull((String) request.getAttribute("txnStatus"));
		strTxnStatus = strTxnStatus.equals("") ? GmCommonClass.parseNull((String) request.getParameter("txnStatus")) : strTxnStatus;
		 
        strNameId = GmCommonClass.parseNull((String)request.getParameter("names"));
        strNameId = strNameId.equals("")?GmCommonClass.parseNull((String)request.getParameter("distributorId")):strNameId;
        
        strSetId = GmCommonClass.parseNull((String)request.getAttribute("SETID"));
        strSetId = strSetId.equals("")?GmCommonClass.parseNull((String)request.getParameter("setId")):strSetId;
        
        strShipToId = GmCommonClass.parseNull((String)request.getParameter("shipTo"));
        strShipToId = strShipToId.equals("")?"4120":strShipToId; //4120 distributor
        
        strAddressId =  GmCommonClass.parseNull((String)request.getParameter("addressid"));

        alData = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALDATA")); 
    	 
		try
		{  
			String strCountry = GmCommonClass.parseNull(gmRuleBean.fetchShipOutCountryID(strShipToId, strNameId, strAddressId)); 
			if(alData.size() == 0) { 
				alData = gmProjectBean.fetchSetMaping(strSetId,"SET");
				
			}
			if(!strCountry.equals("0"))
			{	 
			 	if(alData.size() > 0)
				{ 
					for (int i =0; i< alData.size(); i++)
					{
						hmValues = (HashMap)alData.get(i);
						 
						String strPNUM = GmCommonClass.parseNull((String)hmValues.get("PNUM")); 
						 
						GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();						 
						gmRuleParamVO.setPnum(strPNUM);					 
						gmRuleParamVO.setCountryID(strCountry);   
						gmRuleParamVO.setTxnStatus(strTxnStatus); 							 
						gmRuleParamVO.setCmpid(strCompanyID);
						alList.add(gmRuleParamVO);
						 
					}			 
				}
			}
		}catch(Exception exp)
		{
			exp.printStackTrace();
		}
		return alList;
	}
}
