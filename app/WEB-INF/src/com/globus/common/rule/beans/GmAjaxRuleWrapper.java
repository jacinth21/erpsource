/**
 * FileName : GmAjaxRuleWrapper.java Description : Author : vprasath Date : Jun 19, 2009 Copyright :
 * Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
public class GmAjaxRuleWrapper extends GmRuleEngineWrapper {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest
   * )
   */
  @Override
  public List formatData(HttpServletRequest request) {
    GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
    ArrayList alReturn = new ArrayList();
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    gmDataStoreVO = GmFramework.getCompanyParams(request);
    try {
      log.debug("Entering GmAjaxRuleWrapper");
      String strTxn = GmCommonClass.parseNull(request.getParameter("RE_TXN"));
      String strTxnStatus = GmCommonClass.parseNull(request.getParameter("txnStatus"));
      String strPnum = GmCommonClass.parseNull(request.getParameter("pnum"));;
      String strControlNumber = GmCommonClass.parseNull(request.getParameter("cntrlnum"));;
      String strSetID = GmCommonClass.parseNull(request.getParameter("setId"));;
      String strCountry = GmCommonClass.parseNull(request.getParameter("country"));;
      String strQty = GmCommonClass.parseNull(request.getParameter("qty"));;
      String strTxnID = GmCommonClass.parseNull(request.getParameter("txnId"));;
      String strTxnTypeID = GmCommonClass.parseNull(request.getParameter("TXN_TYPE_ID"));
      // Get the value of TXN_TYPE_ID from attribute if it is not in parameter
      strTxnTypeID =
          strTxnTypeID.equals("") ? GmCommonClass.parseNull((String) request
              .getAttribute("TXN_TYPE_ID")) : strTxnTypeID;
      log.debug("GmAjaxRuleWrapper strTxnTypeID::" + strTxnTypeID + ":::" + strPnum + "::::"
          + strControlNumber);

      String strTxnValue =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strTxnTypeID, "RULETXN"));
      strTxn = strTxnValue.equals("") ? strTxn : strTxnValue;
      String strCompanyID = gmDataStoreVO.getCmpid();

      gmRuleParamVO.setPnum(strPnum);
      gmRuleParamVO.setControlNumber(strControlNumber);
      gmRuleParamVO.setTxn(strTxn);
      gmRuleParamVO.setTxnStatus(strTxnStatus);
      gmRuleParamVO.setSetID(strSetID);
      gmRuleParamVO.setCountry("US");
      gmRuleParamVO.setQty("");
      gmRuleParamVO.setTxnID("");
      gmRuleParamVO.setTxnTypeID(strTxnTypeID);
      gmRuleParamVO.setCmpid(strCompanyID);
      log.debug("gmRuleParamVO: " + gmRuleParamVO.toHashMap());

      alReturn.add(gmRuleParamVO);
    } catch (Exception exp) {
      exp.printStackTrace();
    }
    return alReturn;
  }

}
