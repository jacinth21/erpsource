/**
 * FileName : GmOrderRuleWrapper.java Description : Author : vprasath Date : Jun 9, 2009 Copyright :
 * Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmRuleBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
public class GmOrderRuleWrapper extends GmRuleEngineWrapper {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest
   * )
   */
  List alList;
  String strInputString = "";

  @Override
  public List formatData(HttpServletRequest request) throws AppError {
    alList = new ArrayList();
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    gmDataStoreVO = GmFramework.getCompanyParams(request);
    GmRuleBean gmRuleBean = new GmRuleBean(gmDataStoreVO);
    ArrayList alCartList = new ArrayList();
    HashMap hmOrderValues = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmOrders = new HashMap();
    String strCompanyID = gmDataStoreVO.getCmpid();
    String strTxn = GmCommonClass.parseNull((String) request.getAttribute("RE_TXN"));
    strTxn = strTxn.equals("") ? GmCommonClass.parseNull(request.getParameter("RE_TXN")) : strTxn;
    String strTxnStatus = GmCommonClass.parseNull((String) request.getAttribute("txnStatus"));
    strTxnStatus =
        strTxnStatus.equals("") ? GmCommonClass.parseNull(request.getParameter("txnStatus"))
            : strTxnStatus;
    String strAction = GmCommonClass.parseNull((String) request.getAttribute("hAction"));
    strAction =
        strAction.equals("") ? GmCommonClass.parseNull(request.getParameter("hAction")) : strAction;
    String strTxnTypeID = GmCommonClass.parseNull((String) request.getAttribute("TXN_TYPE_ID"));
    hmOrderValues = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("hmReturn"));
    hmOrders = GmCommonClass.parseNullHashMap((HashMap) hmOrderValues.get("ORDERDETAILS"));
    alCartList = GmCommonClass.parseNullArrayList((ArrayList) hmOrderValues.get("CARTDETAILS"));
    

    String hOrderId = GmCommonClass.parseNull((String) hmOrders.get("ID"));
    hOrderId =
        hOrderId.equals("") ? GmCommonClass.parseNull(request.getParameter("hOrderId")) : hOrderId;                                                                                     
        hOrderId =
                hOrderId.equals("") ? GmCommonClass.parseNull(request.getParameter("txnID")) : hOrderId; 
                                                                                                            
    log.debug("strAction..." + strAction);
    log.debug("hmOrders...." + hmOrders);
    log.debug("alCartList...." + alCartList);
    log.debug("strTxn = " + strTxn);
    log.debug("hOrderId = " + hOrderId);

    /*
     * log.debug("hTxnId...."+hTxnId); log.debug("hmHeaderValues "+hmHeaderValues);
     * log.debug("alConsignList "+alConsignList);
     * 
     * log.debug("strTxnStatus = "+strTxnStatus);
     */
    String strNameId = "";
    String strShipToId = "";
    String strAddressId = "";
    String strOrderType = "";

    strNameId = GmCommonClass.parseNull(request.getParameter("names"));
    strShipToId = GmCommonClass.parseNull(request.getParameter("shipTo"));
    strAddressId = GmCommonClass.parseNull(request.getParameter("addressid"));
    strOrderType = GmCommonClass.parseNull(request.getParameter("orderType"));

    String strPartNums = GmCommonClass.parseNull(request.getParameter("PartNums"));
    String strRefName = GmCommonClass.parseNull(request.getParameter("RefName"));
    try {
      String strCountry =
          GmCommonClass.parseNull(gmRuleBean.fetchShipOutCountryID(strShipToId, strNameId,
              strAddressId));
      String strItemType = "";

      if (!strRefName.equals("RFS") && !strAction.equals("PlaceOrder")) {
        if (alCartList.size() > 0) {
          for (int i = 0; i < alCartList.size(); i++) {
            hmValues = (HashMap) alCartList.get(i);
            strItemType = GmCommonClass.parseNull((String) hmValues.get("ITEMTYPE"));

            /*
             * if the part type is loaner, there is no need to check the rules for the part, so no
             * need to add the part to the gmRuleParamVO
             */
            if (!strItemType.equals("50301")) {
              GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
              gmRuleParamVO.setPnum((String) hmValues.get("ID"));
              gmRuleParamVO.setControlNumber((String) hmValues.get("CNUM"));
              gmRuleParamVO.setTxn(strTxn);
              gmRuleParamVO.setTxnStatus(strTxnStatus);
              gmRuleParamVO.setQty((String) hmValues.get("QTY"));
              gmRuleParamVO.setTxnID(hOrderId);
              gmRuleParamVO.setCmpid(strCompanyID);
              gmRuleParamVO.setTxnTypeID(strTxnTypeID);
              alList.add(gmRuleParamVO);
            }
          }
        }

        else if (!strAction.equals("PlaceOrder")) {
          if (strAction.equals("SaveControl")) {
            strInputString = GmCommonClass.parseNull(formateString(request));
            log.debug("string....." + strInputString);
            if (!strInputString.equals("")) {
              String[] strArray = strInputString.split("\\|");
              log.debug("strArray..." + strArray.length);
              for (int j = 0; j < strArray.length; j++) {
                String[] strValues = strArray[j].split("\\,");

                GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
                gmRuleParamVO.setPnum(strValues[0]);
                gmRuleParamVO.setQty(strValues[1]);
                gmRuleParamVO.setControlNumber(strValues[2]);
                gmRuleParamVO.setTxn(strTxn);
                gmRuleParamVO.setTxnStatus(strTxnStatus);
                gmRuleParamVO.setTxnID(hOrderId);
                gmRuleParamVO.setCmpid(strCompanyID);
                gmRuleParamVO.setTxnTypeID(strTxnTypeID);
                alList.add(gmRuleParamVO);
              }
            }

          }
        }
      } else if (strAction.equals("PlaceOrder")) {
        strInputString = strPartNums;
        if (!strInputString.equals("")) {
          String[] strArray = strInputString.split("\\|");
          for (int j = 0; j < strArray.length; j++) {
            String[] strValues = strArray[j].split("\\,");
            if (!strValues[0].equals("")) {// && !strValues[1].equals("")){
              GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
              gmRuleParamVO.setPnum(strValues[0]);
              if (strValues.length > 1)
                gmRuleParamVO.setControlNumber(strValues[1]);
              gmRuleParamVO.setTxn(strTxn);
              gmRuleParamVO.setTxnStatus(strTxnStatus);
              gmRuleParamVO.setCountryID(strCountry);
              gmRuleParamVO.setTxnID(hOrderId);
              gmRuleParamVO.setRuleType(strOrderType);
              gmRuleParamVO.setCmpid(strCompanyID);
              gmRuleParamVO.setTxnTypeID(strTxnTypeID);
              alList.add(gmRuleParamVO);
            }
          }
        }
      } else {
        if (!strCountry.equals("0")) {
          String[] strPartID = strPartNums.split(",");
          for (int i = 0; i < strPartID.length; i++) {
            GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
            gmRuleParamVO.setPnum(strPartID[i]);
            // gmRuleParamVO.setCountry(strCountry);
            gmRuleParamVO.setCountryID(strCountry);
            gmRuleParamVO.setRuleType(strOrderType);
            gmRuleParamVO.setTxn(strTxn);
            gmRuleParamVO.setTxnStatus(strTxnStatus);
            gmRuleParamVO.setCmpid(strCompanyID);
            gmRuleParamVO.setTxnTypeID(strTxnTypeID);
            gmRuleParamVO.setTxnID(hOrderId);
            alList.add(gmRuleParamVO);
          }
        }
      }
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return alList;
  }

  private String formateString(HttpServletRequest request) {
    String strCount = "";
    String strPartNum = "";
    String strQty = "";
    String strControl = "";

    int intCount = 0;
    strCount = request.getParameter("hCnt") == null ? "" : request.getParameter("hCnt");
    intCount = Integer.parseInt(strCount);
    for (int i = 0; i <= intCount; i++) {
      strPartNum =
          request.getParameter("hPartNum" + i) == null ? "" : request.getParameter("hPartNum" + i);
      if (!strPartNum.equals("")) {
        strQty = GmCommonClass.parseNull(request.getParameter("Txt_Qty" + i));
        strQty = strQty.equals("") ? "0" : strQty;
        strControl = GmCommonClass.parseNull(request.getParameter("Txt_CNum" + i));
        strControl = strControl.equals("") ? " " : strControl;
        strInputString =
            strInputString.concat(strPartNum).concat(",").concat(strQty).concat(",")
                .concat(strControl).concat("|");
      }
    }
    return strInputString;
  }

}
