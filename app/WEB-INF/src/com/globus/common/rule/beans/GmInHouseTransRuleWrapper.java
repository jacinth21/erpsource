/**
 * FileName : GmInHouseTransRuleWrapper Description : Author : Brinal Date : Nov 10, 2010 Copyright
 * : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.math.NumberUtils;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmRuleBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author Brinal
 * 
 */
public class GmInHouseTransRuleWrapper extends GmRuleEngineWrapper {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest
   * )
   */
  List alList;
  String strInputString = "";

  @Override
  public List formatData(HttpServletRequest request) {
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    gmDataStoreVO = GmFramework.getCompanyParams(request);
    alList = new ArrayList();
    GmRuleBean gmRuleBean = new GmRuleBean();
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean();
    ArrayList alLoanerList = new ArrayList();
    HashMap hmLoanerValues = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmConsign = new HashMap();
    HashMap hmResult = new HashMap();
    String strConsignId = "";
    String hConsignId = "";
    String strCountry = "";
    String strQtyInsp = "";
    String strInsetFl = "";
    String strPNUM = "";
    String strTxnStatus = "";
    String strTxn = "";
    String strAction = "";
    String strType = "";
    String strTxnValue = "";
    String strCompanyID = gmDataStoreVO.getCmpid();

    strTxn = GmCommonClass.parseNull((String) request.getAttribute("RE_TXN"));
    strTxn = strTxn.equals("") ? GmCommonClass.parseNull(request.getParameter("RE_TXN")) : strTxn;
    strTxnStatus = GmCommonClass.parseNull((String) request.getAttribute("txnStatus"));
    log.debug("strTxnStatus====1=" + strTxnStatus);
    strTxnStatus =
        strTxnStatus.equals("") ? GmCommonClass.parseNull(request.getParameter("txnStatus"))
            : strTxnStatus;
    log.debug("strTxnStatus====1=" + strTxnStatus);
    strAction = GmCommonClass.parseNull((String) request.getAttribute("hAction"));
    strAction =
        strAction.equals("") ? GmCommonClass.parseNull(request.getParameter("hAction")) : strAction;
    log.debug("GmInHouseTransRuleWrapper strAction..." + strAction);
    hmLoanerValues = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("hmReturn"));
    hmConsign = GmCommonClass.parseNullHashMap((HashMap) hmLoanerValues.get("CONDETAILS"));
    alLoanerList = GmCommonClass.parseNullArrayList((ArrayList) hmLoanerValues.get("SETLOAD"));
    log.debug("GmInHouseTransRuleWrapper hmConsign " + hmConsign);
    log.debug("GmInHouseTransRuleWrapper alLoanerList " + alLoanerList);
    strType = GmCommonClass.parseNull((String) request.getAttribute("hType"));
    // log.debug("GmInHouseTransRuleWrapper strType....."+strType);
    strConsignId = GmCommonClass.parseNull((String) request.getAttribute("hConsignId"));
    log.debug("GmInHouseTransRuleWrapper strConsignId.1..." + strConsignId);
    strConsignId =
        strConsignId.equals("") ? GmCommonClass.parseNull(request.getParameter("hConsignId"))
            : strConsignId;
    log.debug("GmInHouseTransRuleWrapper strConsignId.2..." + strConsignId);
    strConsignId =
        strConsignId.equals("") ? GmCommonClass.parseNull((String) hmConsign.get("CONSIGNID"))
            : strConsignId;
    log.debug("GmInHouseTransRuleWrapper strConsignId.3..." + strConsignId);
    strConsignId =
        strConsignId.equals("") ? GmCommonClass.parseNull(request.getParameter("hTxnId"))
            : strConsignId;
    hConsignId = GmCommonClass.parseNull(request.getParameter("txnID"));
    log.debug("GmInHouseTransRuleWrapper strConsignId.4..."
        + GmCommonClass.parseNull(request.getParameter("hTxnId")) +"  hConsignId"+hConsignId);
    String strTxnTypeID = GmCommonClass.parseNull((String) request.getAttribute("TxnTypeID"));
    strTxnTypeID =
        strTxnTypeID.equals("") ? GmCommonClass.parseNull(request.getParameter("TXN_TYPE_ID"))
            : strTxnTypeID;

    // log.debug("GmInHouseTransRuleWrapper strTxnTypeID = "+strTxnTypeID);

    try {
      strTxnValue = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strTxnTypeID, "RULETXN"));
      strTxn = strTxnValue.equals("") ? strTxn : strTxnValue;

      log.debug("strTxn ::: " + strTxn);
      log.debug("alLoanerList.size() ::: " + alLoanerList.size());
      if (alLoanerList.size() > 0 || strAction.equals("Verify") || strAction.equals("Verification")) {
        if (strTxn.equals("50984") || strTxn.equals("50951") || strTxn.equals("50948") || strTxn.equals("50958") ||  strTxn.equals("26241142"))// RHIH-
                                                                                                                  // IHLE -
                                                                                                                  // IHIS
        	                                                                                                      // LRPN //LRQN -29081998
        {
          strConsignId = GmCommonClass.parseNull(request.getParameter("hTxnId"));
        }
        if (alLoanerList.size() == 0) {
          alLoanerList =
              GmCommonClass.parseNullArrayList(gmInHouseRptBean.loadSavedSetMaster(strConsignId));
        }

        for (int i = 0; i < alLoanerList.size(); i++) {
          hmValues = (HashMap) alLoanerList.get(i);

          strPNUM = GmCommonClass.parseNull((String) hmValues.get("PNUM"));
          GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
          gmRuleParamVO.setPnum(strPNUM);
          gmRuleParamVO.setControlNumber((String) hmValues.get("CNUM"));
          if (NumberUtils.isNumber(strTxn)) {
            gmRuleParamVO.setTxn(strTxn); // this is comment line removed for testing
          }
          gmRuleParamVO.setTxnStatus(strTxnStatus);
          gmRuleParamVO.setAction(strAction);
          gmRuleParamVO.setCountry("US");
          gmRuleParamVO.setTxnID(hConsignId);
          gmRuleParamVO.setTxnTypeID(strTxnTypeID);
          gmRuleParamVO.setCmpid(strCompanyID);
          alList.add(gmRuleParamVO);
        }
      } else {
        if (strAction.equals("SaveControl") || strAction.equals("ReleaseControl")) {
          strInputString = GmCommonClass.parseNull(formateString(request));
          log.debug("GmInHouseTransRuleWrapper string....." + strInputString);
          if (!strInputString.equals("")) {
            String[] strArray = strInputString.split("\\|");
            log.debug("strArray..." + strArray.length);
            for (int j = 0; j < strArray.length; j++) {
              String[] strValues = strArray[j].split("\\,");
              strQtyInsp = GmCommonClass.parseNull(strValues[1]);
              strQtyInsp = strQtyInsp.equals("") ? "0" : strQtyInsp;
              GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
              gmRuleParamVO.setPnum(strValues[0]);
              gmRuleParamVO.setControlNumber(strValues[2]);
              if (NumberUtils.isNumber(strTxn)) {
                gmRuleParamVO.setTxn(strTxn);
              }
              gmRuleParamVO.setCountry("US");
              gmRuleParamVO.setTxnStatus(strTxnStatus);
              gmRuleParamVO.setTxnID(hConsignId);
              gmRuleParamVO.setTxnTypeID(strTxnTypeID);
              alList.add(gmRuleParamVO);
            }
          }
        }
      }
    } catch (Exception exp) {
      exp.printStackTrace();
    }
    log.debug("GmInHouseTransRuleWrapper alList.................. " + alList.size());
    log.debug("GmInHouseTransRuleWrapper alList.................. " + alList);
    return alList;
  }

  private String formateString(HttpServletRequest request) {
    String strCount = "";
    String strPartNum = "";
    String strQty = "";
    String strControl = "";

    int intCount = 0;
    strCount = request.getParameter("hCnt") == null ? "" : request.getParameter("hCnt");
    intCount = Integer.parseInt(strCount);
    for (int i = 0; i <= intCount; i++) {
      strPartNum =
          request.getParameter("hPartNum" + i) == null ? "" : request.getParameter("hPartNum" + i);
      if (!strPartNum.equals("")) {
        strQty = GmCommonClass.parseNull(request.getParameter("Txt_Qty" + i));
        strQty = strQty.equals("") ? "0" : strQty;
        strControl = GmCommonClass.parseNull(request.getParameter("Txt_CNum" + i));
        strControl = strControl.equals("") ? " " : strControl;
        strInputString =
            strInputString.concat(strPartNum).concat(",").concat(strQty).concat(",")
                .concat(strControl).concat("|");
      }
    }
    log.debug("strInputString " + strInputString);
    return strInputString;
  }

}
