/**
 * FileName    : GmRuleParamVO.java 
 * Description :
 * Author      : vprasath
 * Date        : Jun 9, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmRuleBean;

/**
 * @author vprasath
 *
 */

public class GmRuleParamVO {


	private  String txn = "";
	private  String txnID="";
	private  String txnStatus = "";
	private  String pnum ="";
	private  String vendor ="";
	private  String manfactureDate="";
	private  String lot="";
	private  String revision="";
	private  String country="";
	private  String setID = "";
	private  String qty = "";
	private  String controlNumber = "";
	private  String countryID="";
	private  String ruleType="";
	private  String shipTo = "";
	private  String action = "";
	private  String txnTypeID = "";
	private  String cmpid = "";
	private  String hSelectedAction = "";
	
	
	/**
	 * @return the txnTypeID
	 */
	public String getTxnTypeID() {
		return txnTypeID;
	}

	/**
	 * @param txnTypeID the txnTypeID to set
	 */
	public void setTxnTypeID(String txnTypeID) {
		this.txnTypeID = txnTypeID;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	private  HashMap hmNewMap = new HashMap();
	/**
	 * @return the shipTo
	 */
	public String getShipTo() {
		return shipTo;
	}

	/**
	 * @param shipTo the shipTo to set
	 */
	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}

	private  HashMap hmRuleData = new HashMap();
	private  GmCommonBean gmCommonBean = new GmCommonBean();
	
private void splitControlNumber()
{
	int days;
	controlNumber = controlNumber.replaceAll(" ", "");
	controlNumber = controlNumber.toUpperCase();
	lot = ""+controlNumber;
	
	if (controlNumber != null && controlNumber.length() == 8 )
	{
		vendor = controlNumber.substring(0, 2);
		try 
		{
			days =	Integer.parseInt(GmCommonClass.parseNull(controlNumber.substring(3, 6)));	
		}
		catch(Exception ex)
		{
			days = -1;
		}
		if (days != -1)
		{
			manfactureDate = getDate(controlNumber.charAt(2), days);			
		}			
		revision = ""+ controlNumber.charAt(7);
	}
}
	
public String getCountryID() {
	return countryID;
}

public void setCountryID(String countryID) {
	this.countryID = countryID;
}

private String getDate(char year, int days)
{
	int intJulianRule =0;
	try {
		intJulianRule = Integer.parseInt(GmCommonClass.parseNull(GmCommonClass.getRuleValue("JULIAN_DATE", "LOTVALIDATION")));
	} catch (NumberFormatException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (AppError e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	if(days < 0 || (days > 366 && days < intJulianRule+1) || days > (intJulianRule+366)) 
	{
		return "";
	}
	String strDate = "";
	int iYear = 0; 
	try
	{
		Calendar c = Calendar.getInstance();
		hmRuleData.put("RULEID", Character.toString(year));
		hmRuleData.put("RULEGROUP", "MFGYEAR");
		iYear = Integer.parseInt(gmCommonBean.getRuleValue(hmRuleData));
		c.set(Calendar.DAY_OF_YEAR, days);
		c.set(Calendar.YEAR, iYear);
	
		Date dDate = c.getTime();       
		SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
		strDate = fmt.format(dDate);
	}catch(Exception ex)
	{
		ex.printStackTrace();
	}
	
		return strDate;
}
	
public String getTxn() {
	return txn;
}
public void setTxn(String txn) {
	this.txn = txn;
}
public String getTxnStatus() {
	return txnStatus;
}
public void setTxnStatus(String txnStatus) {
	this.txnStatus = txnStatus;
}
	
public HashMap toHashMap()
{
	hmNewMap.put("PARTNUMBER", pnum);
	hmNewMap.put("REVISION", revision);
	hmNewMap.put("COUNTRY", country);
	hmNewMap.put("VENDOR", vendor);
	hmNewMap.put("LOT", lot);
	hmNewMap.put("MANUFACTUREDDATE", manfactureDate);
	hmNewMap.put("SETID", setID);
	hmNewMap.put("QTYINSPECTED(%)", qty);
	hmNewMap.put("TXN", txn);
	hmNewMap.put("TXNSTATUS", txnStatus);
	hmNewMap.put("TXNID", txnID);
	hmNewMap.put("CONTROLNO",controlNumber);
	hmNewMap.put("CMPID",cmpid);	
	return hmNewMap;
}
public String getTxnID() {
	return txnID;
}
public void setTxnID(String txnID) {
	this.txnID = txnID;
}
public String getPnum() {
	return pnum;
}
public void setPnum(String pnum) {
	this.pnum = pnum;
}
public String getVendor() {
	return vendor;
}

public String getManfactureDate() {
	return manfactureDate;
}

public String getLot() {
	return lot;
}

public String getRevision() {
	return revision;
}

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

public String getSetID() {
	return setID;
}

public void setSetID(String setID) {
	this.setID = setID;
}

public String getQty() {
	return qty;
}

public void setQty(String qty) {
	this.qty = qty;
}

public void setControlNumber(String controlNumber) {
	this.controlNumber = controlNumber;
	splitControlNumber();
	String strRevNumber = fetchRevisionNumber();
	if(!strRevNumber.equals("")){
		revision = strRevNumber;
		
	}
}

public String fetchRevisionNumber(){
	String strRevisionNumber = "";
	if ( pnum != null && !pnum.equals("") && controlNumber != null && !controlNumber.equals("") ){
		GmRuleBean gmRuleBean = new GmRuleBean();
		strRevisionNumber = gmRuleBean.fetchRevisionNumber( pnum,controlNumber );
       }
	return strRevisionNumber;
}

public String getControlNumber() {
	return controlNumber;
}

public String getRuleType() {
	return ruleType;
}

public void setRuleType(String ruleType) {
	this.ruleType = ruleType;
}

/**
 * @return the cmpid
 */
public String getCmpid() {
    return cmpid;
}

/**
 * @param cmpid the cmpid to set
 */
public void setCmpid(String cmpid) {
    this.cmpid = cmpid;
}

/**
 * @return the hSelectedAction
 */
public String gethSelectedAction() {
	return hSelectedAction;
}

/**
 * @param hSelectedAction the hSelectedAction to set
 */
public void sethSelectedAction(String hSelectedAction) {
	this.hSelectedAction = hSelectedAction;
}

}
