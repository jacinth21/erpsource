/**
 * FileName : GmLoanerRuleWrapper.java Description : Author : vprasath Date : Jun 9, 2009 Copyright
 * : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmRuleBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
public class GmLoanerRuleWrapper extends GmRuleEngineWrapper {

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest
   * )
   */
  List alList;
  String strInputString = "";

  @Override
  public List formatData(HttpServletRequest request) {
    alList = new ArrayList();
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    gmDataStoreVO = GmFramework.getCompanyParams(request);
    GmRuleBean gmRuleBean = new GmRuleBean(gmDataStoreVO);
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(gmDataStoreVO);
    ArrayList alLoanerList = new ArrayList();
    HashMap hmLoanerValues = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmConsign = new HashMap();
    HashMap hmResult = new HashMap();
    String hConsignId = "";
    String strConsignId = "";
    String strCountry = "";
    String strQtyInsp = "";
    String strInsetFl = "";
    String strPNUM = "";
    String strCNID = "";
    String strCompanyID = gmDataStoreVO.getCmpid();
    String strTxn = GmCommonClass.parseNull((String) request.getAttribute("RE_TXN"));
    strTxn = strTxn.equals("") ? GmCommonClass.parseNull(request.getParameter("RE_TXN")) : strTxn;
    String strTxnStatus = GmCommonClass.parseNull((String) request.getAttribute("txnStatus"));
    log.debug("strTxnStatus====1=" + strTxnStatus);
    strTxnStatus =
        strTxnStatus.equals("") ? GmCommonClass.parseNull(request.getParameter("txnStatus"))
            : strTxnStatus;
    log.debug("strTxnStatus====2=" + strTxnStatus);
    String strTxnTypeID = GmCommonClass.parseNull((String) request.getAttribute("TxnTypeID"));
    strTxnTypeID =
        strTxnTypeID.equals("") ? GmCommonClass.parseNull(request.getParameter("TXN_TYPE_ID"))
            : strTxnTypeID;
    String strAction = GmCommonClass.parseNull((String) request.getAttribute("hAction"));
    strAction =
        strAction.equals("") ? GmCommonClass.parseNull(request.getParameter("hAction")) : strAction;
    hmLoanerValues = GmCommonClass.parseNullHashMap((HashMap) request.getAttribute("hmReturn"));
    hmConsign = GmCommonClass.parseNullHashMap((HashMap) hmLoanerValues.get("CONDETAILS"));
    alLoanerList = GmCommonClass.parseNullArrayList((ArrayList) hmLoanerValues.get("SETLOAD"));
    String strType = GmCommonClass.parseNull((String) request.getAttribute("hType"));
    hConsignId = GmCommonClass.parseNull((String) request.getAttribute("hConsignId"));
    hConsignId =
        hConsignId.equals("") ? GmCommonClass.parseNull((String) hmConsign.get("CONSIGNID"))
            : hConsignId;
    strCNID = hConsignId;
    log.debug("strAction..." + strAction);
    try {
      if (strAction.equals("EditLoad")) {
        strCountry = GmCommonClass.parseNull(gmRuleBean.fetchCNCountry(hConsignId));
      }

      if (strAction.equals("SaveControl") || strAction.equals("ReleaseControl")) {
        hConsignId = GmCommonClass.parseNull(request.getParameter("hConsignId"));
        strCountry = GmCommonClass.parseNull(gmRuleBean.fetchCNCountry(hConsignId));
      }
      if (strAction.equals("Verification") || strAction.equals("Verify")) {
        if (strAction.equals("Verify")) {
          String hTxnId = GmCommonClass.parseNull(request.getParameter("hTxnId"));
          if (!hTxnId.equals("")) {
            hmConsign = gmInHouseRptBean.viewInHouseTransDetails(hTxnId);
            alLoanerList =
                GmCommonClass.parseNullArrayList(gmInHouseRptBean.loadSavedSetMaster(hTxnId));
          }
        }
        hConsignId = GmCommonClass.parseNull((String) hmConsign.get("CONSIGNID"));
        strCNID = hConsignId;
        strCountry = GmCommonClass.parseNull(gmRuleBean.fetchCNCountry(hConsignId));
        hConsignId = GmCommonClass.parseNull((String) request.getAttribute("hConsignId"));
        hConsignId =
            hConsignId.equals("") ? GmCommonClass.parseNull(request.getParameter("hConsignId"))
                : hConsignId;
      }
      strCNID = hConsignId;
      /*
       * log.debug("hTxnId...."+hTxnId); log.debug("hmHeaderValues "+hmHeaderValues);
       * log.debug("alConsignList "+alConsignList); log.debug("strTxn = "+strTxn);
       * log.debug("strTxnStatus = "+strTxnStatus);
       */
      strCountry = strCountry.equals("") ? "US" : strCountry;
      if (strType.equals("50158") || strType.equals("50916")) {
        strCNID = GmCommonClass.parseNull((String) hmConsign.get("CONSIGNID"));
      }

      if (alLoanerList.size() > 0 || strAction.equals("Verify") || strAction.equals("Verification")) {
        for (int i = 0; i < alLoanerList.size(); i++) {
          hmValues = (HashMap) alLoanerList.get(i);
          strQtyInsp = GmCommonClass.parseNull((String) hmValues.get("QTY_IN_TRANS"));
          strQtyInsp = strQtyInsp.equals("") ? "0" : strQtyInsp;
          strPNUM = GmCommonClass.parseNull((String) hmValues.get("PNUM"));
          GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
          gmRuleParamVO.setPnum(strPNUM);
          gmRuleParamVO.setControlNumber((String) hmValues.get("CNUM"));
          gmRuleParamVO.setTxn(strTxn);
          log.debug("strTxnStatus====3=" + strTxnStatus);
          gmRuleParamVO.setTxnStatus(strTxnStatus);
          gmRuleParamVO.setQty(strQtyInsp);
          gmRuleParamVO.setCountry(strCountry);
          gmRuleParamVO.setTxnID(strCNID);
          gmRuleParamVO.setTxnTypeID(strTxnTypeID);
          gmRuleParamVO.setAction(strAction);
          gmRuleParamVO.setCmpid(strCompanyID);

          strInsetFl = GmCommonClass.parseNull(fetchInsetFlag(strCNID, strPNUM));
          if (strTxn.equals("50910")) // FG-Shelf to built set
          {
            alList.add(gmRuleParamVO);
          } else if (strInsetFl.equals("Y") && !strTxn.equals("50910")) {
            // log.debug("Part# = "+(String)hmValues.get("CNUM"));
            alList.add(gmRuleParamVO);
          } else if (strTxn.equals("50916")) // Code added for new rule changes as part of RW
                                             // project
          {
            alList.add(gmRuleParamVO);
          }
        }
      } else {
        if (strAction.equals("SaveControl") || strAction.equals("ReleaseControl")) {
          strInputString = GmCommonClass.parseNull(formateString(request));
          log.debug("string....." + strInputString);
          if (!strInputString.equals("")) {
            String[] strArray = strInputString.split("\\|");
            log.debug("strArray..." + strArray.length);
            for (int j = 0; j < strArray.length; j++) {
              String[] strValues = strArray[j].split("\\,");
              strQtyInsp = GmCommonClass.parseNull(strValues[1]);
              strQtyInsp = strQtyInsp.equals("") ? "0" : strQtyInsp;
              GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
              gmRuleParamVO.setPnum(strValues[0]);
              gmRuleParamVO.setQty(strQtyInsp);
              gmRuleParamVO.setControlNumber(strValues[2]);
              gmRuleParamVO.setTxn(strTxn);
              log.debug("strTxnStatus====4=" + strTxnStatus);
              gmRuleParamVO.setTxnStatus(strTxnStatus);
              gmRuleParamVO.setCountry(strCountry);
              gmRuleParamVO.setTxnTypeID(strTxnTypeID);
              gmRuleParamVO.setTxnID(strCNID);
              gmRuleParamVO.setCmpid(strCompanyID);
              strInsetFl = GmCommonClass.parseNull(fetchInsetFlag(strCNID, strValues[0]));
              if (strTxn.equals("50910")) // FG-Shelf to built set
              {
                alList.add(gmRuleParamVO);
              } else if (strInsetFl.equals("Y") && !strTxn.equals("50910")) {
                // log.debug("Part# = "+(String)hmValues.get("CNUM"));
                alList.add(gmRuleParamVO);
              } else if (strTxn.equals("50916")) // Code added for new rule changes as part of RW
                                                 // project
              {
                alList.add(gmRuleParamVO);
              }
            }
          }

        }
      }
    } catch (Exception exp) {
      exp.printStackTrace();
    }
    log.debug("alList " + alList);
    return alList;
  }

  private String formateString(HttpServletRequest request) {
    String strCount = "";
    String strPartNum = "";
    String strQty = "";
    String strControl = "";

    int intCount = 0;
    strCount = request.getParameter("hCnt") == null ? "" : request.getParameter("hCnt");
    intCount = Integer.parseInt(strCount);
    for (int i = 0; i <= intCount; i++) {
      strPartNum =
          request.getParameter("hPartNum" + i) == null ? "" : request.getParameter("hPartNum" + i);
      if (!strPartNum.equals("")) {
        strQty = GmCommonClass.parseNull(request.getParameter("Txt_Qty" + i));
        strQty = strQty.equals("") ? "0" : strQty;
        strControl = GmCommonClass.parseNull(request.getParameter("Txt_CNum" + i));
        strControl = strControl.equals("") ? " " : strControl;
        strInputString =
            strInputString.concat(strPartNum).concat(",").concat(strQty).concat(",")
                .concat(strControl).concat("|");
      }
    }
    return strInputString;
  }

}
