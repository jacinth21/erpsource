/**
 * FileName    : GmRuleEngineWrapper.java 
 * Description :
 * Author      : vprasath
 * Date        : Jun 9, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 *
 */
public abstract class GmRuleEngineWrapper {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	public abstract List formatData(HttpServletRequest request) throws AppError;
	
	public static GmRuleEngineWrapper getInstance(String strTxn) throws Exception
	{	 	
	  String strClassName = GmCommonClass.getString(strTxn);
	  Class ruleWrapperClass = Class.forName(strClassName);
	  return (GmRuleEngineWrapper)ruleWrapperClass.newInstance();
	}
	
 private void doEmail(ArrayList alEmailConsequences)
	{
		Iterator itrEmail = alEmailConsequences.iterator();
		
		while(itrEmail.hasNext())
		{
			HashMap hmEmail = (HashMap)itrEmail.next();			
			sendMail(hmEmail);
		}
	}

 
 private void sendMail(HashMap hmEmail)
 {
	 try
	 {
	 String strRuleID = (String) hmEmail.get("ID");
	 String strRuleName = (String) hmEmail.get("NAME");
	 String strEmailTo = (String) hmEmail.get("EMAIL_TO");
	 String strEmailSubject = (String) hmEmail.get("EMAIL_SUB");
	 String strEmailMessage = (String) hmEmail.get("EMAIL_BODY");
	 String strInitatedBy = (String) hmEmail.get("INITBY");
	 String strInitiatedDate = (String) hmEmail.get("INITDATE");
	 String strUpdatedBy = (String) hmEmail.get("UPDBY");
	 String strUpdatedDate = (String) hmEmail.get("UPDDATE");
	 String strRuleString = (String)hmEmail.get("STRRULESTRING");
	 
	 GmEmailProperties  gmEmailProperties = new GmEmailProperties();
	 gmEmailProperties.setSender(GmCommonClass.getEmailProperty("GmJobStatusMail" + "." + GmEmailProperties.FROM));
	 gmEmailProperties.setRecipients(strEmailTo);
	 gmEmailProperties.setCc(GmCommonClass.getEmailProperty("GmJobStatusMail" + "." + GmEmailProperties.CC));
	 gmEmailProperties.setSubject(strEmailSubject);
	 
	 strRuleString = strRuleString.substring(0,strRuleString.length()-1);
	 strRuleString = GmCommonClass.replaceAll(strRuleString, ",", "<br>");

	 String strFooter = "<br><br>The following are the rule parameters: <br>";
     strFooter += "Rule ID: "+ strRuleID+"<br>";
     strFooter += "Rule Name: "+ strRuleName+"<br>";
     strFooter += "Initated By: "+ strInitatedBy+"<br>";
     strFooter += "Initiated Date: "+ strInitiatedDate+"<br>";
     strFooter += "Updated By: "+ strUpdatedBy+"<br>";
     strFooter += "Updated Date: "+ strUpdatedDate+"<br>";
     strFooter += "Part Numbers / Control Numbers: "+"<br>"+strRuleString+"<br>";
	 strEmailMessage +=  strFooter;
	  
	 gmEmailProperties.setMessage(strEmailMessage);
	 gmEmailProperties.setMimeType("text/html");
	 GmCommonClass.sendMail(gmEmailProperties);
	}
	 catch(Exception ex)
	 {
		 ex.printStackTrace();
	 }
	//  log.debug("Sending Mail: "+ hmEmail);
 }
 
	public void executeRules(List alFormattedData, HttpServletRequest request) throws AppError
	{
	
	    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
	    gmDataStoreVO = GmFramework.getCompanyParams(request);
	    
			HttpSession session = request.getSession(false);
			GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
			String strPartyId = GmCommonClass.parseNull((String) session.getAttribute("strSessPartyId"));
		GmRuleEngine gmRuleEngine = new GmRuleEngine(gmDataStoreVO);
		ArrayList alPartyList = new ArrayList(); 
		if(gmRuleEngine.execute(alFormattedData))
		{
		ArrayList alConsequenceList = (ArrayList)gmRuleEngine.getConsequences();
		HashMap hmLogDetails = new HashMap(); 
		String strType = gmRuleEngine.getReturnType();
		
		request.setAttribute("CONSEQUENCES", alConsequenceList);
		session.setAttribute("CONSEQUENCES", alConsequenceList);
		ArrayList alEmailConsequenceList = (ArrayList)gmRuleEngine.getEmailConsequences();
		doEmail(alEmailConsequenceList);
		
		
		ArrayList alValidRules = (ArrayList)gmRuleEngine.getValidRules();
		hmLogDetails = getDetailsToLog(alFormattedData, request, alValidRules, strType);
		
		log.debug("hmLogDetails" + hmLogDetails);
		
		String strOverRideFl = GmCommonClass.parseNull((String)request.getParameter("OVERRIDEFL"));
		strOverRideFl = strOverRideFl.equals("on") ? "Y" : "";
		
		log.debug("OVERRIDEFL:1 "+ strOverRideFl);

		GmRuleBean gmRuleBean = new GmRuleBean();
		gmRuleBean.saveRulesLog(hmLogDetails);
		alPartyList = gmAccessControlBean.getPartyList("CNOVERRIDE",strPartyId);
		if (alPartyList.size()!=0) {	
			request.setAttribute("SHOWLINK","true");
	 	}
		else
			request.setAttribute("SHOWLINK","false");
		
		HashMap hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PD_OVERRIDE_RULE"); // PMT-51299-Product Development Override Rule access
		String strOverrideAccessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		request.setAttribute("OVERRIDE_ACCESS",strOverrideAccessFlag);
		String strRuleIds = GmCommonClass.getRuleValue("RULES","MUSTHOLDRULEIDS");
		String strRequestor = GmCommonClass.parseNull((String) request.getParameter("requestor"));	
		
		//Rule Access Override to hold for at least one control number validation was not satisfied
			if(strOverRideFl.equals("Y") && strRequestor.equals(""))
			{
				for(int i=0;i<alConsequenceList.size();i++)
				{
					HashMap hmConSeq=GmCommonClass.parseNullHashMap((HashMap)alConsequenceList.get(i));
					String strConSeq = GmCommonClass.parseNull((String)hmConSeq.get("ID"));

					if (strRuleIds.indexOf(strConSeq) != -1)
						   {
							   strType = "ERROR";
							   strOverRideFl="";
							   break;
						   }						  			
				}
			}
		//-- end of the hold code
		
			if( alConsequenceList.size() > 0 && strType.equals("ERROR") && strOverRideFl.equals(""))
			{
				 request.setAttribute("RE_FORWARD", "gmRuleDisplay");
			}		
		}
	}
	private HashMap getDetailsToLog(List alFormattedData,  HttpServletRequest request, List alValidRules, String strType)
	{
		HashMap hmResult = new HashMap();
		HashMap hmValidRule = new HashMap();
		HashMap hmFilterMap = new HashMap();
		String strRuleID = "";
		String strConditions = "";
		String strInputString = "";
		String strOverRideFl = "";
		String strOverRideComments ="";
		String strTxnID = "";
		String strTxnType = "";
		String strUserID = "";
		
		Iterator itrValidRules = alValidRules.iterator();
		
		// Get the rule ID, Transaction type and the conditions
		
		while(itrValidRules.hasNext())
		{
			hmValidRule = (HashMap)itrValidRules.next();
			strRuleID = GmCommonClass.parseNull((String)hmValidRule.get("RULEID"));
			strConditions = GmCommonClass.parseNull((String)hmValidRule.get("SCRCONS"));
			strTxnType = GmCommonClass.parseNull((String)hmValidRule.get("TXN"));
		    hmFilterMap.put(strRuleID+"^"+strConditions,strTxnType);				
		}
		
		 Iterator itrFilter = hmFilterMap.keySet().iterator();
		 
		 while(itrFilter.hasNext())
		 {
			 strConditions = (String)itrFilter.next();
			 strTxnType = (String)hmFilterMap.get(strConditions);
			 strInputString = strInputString + strConditions + "^" + strTxnType + "^|";
		 }
		
		hmResult.put("INPUTSTRING", strInputString);
		
		// Get the override values
		if(strType.equals("ERROR"))
		{
			strOverRideFl = GmCommonClass.parseNull(request.getParameter("OVERRIDEFL"));
			strOverRideFl = strOverRideFl.equals("on") ? "Y" : "";
			strOverRideComments = GmCommonClass.parseNull(request.getParameter("OVERRIDECOMMENTS"));
		}
		
		hmResult.put("OVERRIDEFL", strOverRideFl);
		hmResult.put("OVERRIDECOMMENTS", strOverRideComments);
		
		// Get the Transaction ID
		
		GmRuleParamVO gmRuleParamVO = (GmRuleParamVO)alFormattedData.get(0);
		strTxnID = gmRuleParamVO.getTxnID();
		
		hmResult.put("TXNID", strTxnID);
		
		// Get the user id
		
		HttpSession httpSession =  request.getSession(false);
		strUserID  = (String)httpSession.getAttribute("strSessUserId");
		
		// Need to set User id for Rule Print when it is null.
		if(strUserID == null || strUserID.equals(""))
			strUserID = GmCommonClass.parseNull(request.getParameter("USERID"));

		request.setAttribute("USERID", strUserID);
	return hmResult;			
	}
	
	
	
	/**
     * fetchInsetFlag  - This method will be used to fetch the inset flag for the set qty for that CN Id. 
     * @param strUserId,
     * @return String
     * @exception AppError
     */
	public String fetchInsetFlag(String strCNID, String strPNUM) throws AppError
	{
		
    	String strInsetFl="";
    	GmDBManager gmDBManager = new GmDBManager ();
    	log.debug("strCNID..."+strCNID);
    	log.debug("strPNUM..."+strPNUM);
        gmDBManager.setFunctionString("gm_pkg_cm_rules.get_inset_flag",2);
        gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
        gmDBManager.setString(2,GmCommonClass.parseNull(strCNID));
        gmDBManager.setString(3,GmCommonClass.parseNull(strPNUM));
        gmDBManager.execute();
        strInsetFl = gmDBManager.getString(1);
	    log.debug("strInsetFl..."+strInsetFl);
	    gmDBManager.close();
    	return strInsetFl;
	    
	}

}
