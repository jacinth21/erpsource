package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmReceivePORuleWrapper extends GmRuleEngineWrapper {

	public List formatData(HttpServletRequest request) {
		
		HashMap hmReturn =  (HashMap) request.getAttribute("hmReturn");
		ArrayList alReturn  = new ArrayList();
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		gmDataStoreVO = GmFramework.getCompanyParams(request);
		ArrayList alDetails =  hmReturn == null ? new ArrayList() : GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("ITEMDETAILS"));
		HashMap hmRow = new HashMap();
		String strTxnStatus = GmCommonClass.parseNull((String) request.getAttribute("txnStatus"));
		String strTxn = GmCommonClass.parseNull((String) request.getAttribute("RE_TXN"));
		String strCompanyID = gmDataStoreVO.getCmpid();
		strTxnStatus = strTxnStatus.equals("") ? GmCommonClass.parseNull((String) request.getParameter("txnStatus")) : strTxnStatus;
		strTxn = strTxn.equals("") ? GmCommonClass.parseNull((String) request.getParameter("RE_TXN")) :strTxn;
		
		if(alDetails.size() > 0  && strTxnStatus.equals("PROCESS"))
		{
			Iterator itrDetails =  alDetails.iterator();

			while(itrDetails.hasNext())
			{
				GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
				hmRow = (HashMap)itrDetails.next();
				String strPnum = (String)hmRow.get("PNUM");
				String strQty = (String)hmRow.get("QTY");
				
				gmRuleParamVO.setPnum(strPnum);
				gmRuleParamVO.setQty(strQty);
				gmRuleParamVO.setTxn(strTxn);
				gmRuleParamVO.setTxnStatus(strTxnStatus);
				gmRuleParamVO.setCmpid(strCompanyID);
				alReturn.add(gmRuleParamVO);
				log.debug("hmRow: "+hmRow);
			}
			log.debug("alReturn: "+alReturn);
		}
		else if(strTxnStatus.equals("VERIFY"))
		{
			String strInputString = GmCommonClass.parseNull((String) request.getParameter("hInputString"));
			log.debug("strInputString: " + strInputString);
			List lRows = Arrays.asList(strInputString.split("\\|"));
			log.debug("lRows"+lRows);
			Iterator itrAlRows  = lRows.iterator();
			while(itrAlRows.hasNext())
			{
			GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
			String strCurrentRow = (String)itrAlRows.next();
			log.debug("strCurrentRow"+strCurrentRow);
			String strRow[] = strCurrentRow.split("\\^");
			String strPnum = strRow[0];
			String strCNum = strRow[3];
			String strQty = strRow[2];
			log.debug("strPnum"+strPnum);
			log.debug("strCNum"+strCNum);
			log.debug("strQty"+strQty);
			
			gmRuleParamVO.setPnum(strPnum);
			gmRuleParamVO.setControlNumber(strCNum);
			gmRuleParamVO.setQty(strQty);
			gmRuleParamVO.setTxn(strTxn);
			gmRuleParamVO.setCmpid(strCompanyID);
			gmRuleParamVO.setTxnStatus(strTxnStatus);
			
			alReturn.add(gmRuleParamVO);

			
			}
		}
		
		//System.out.println("hmReturn: "+ hmReturn);
		return alReturn;
	}

}
