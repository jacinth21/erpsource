/**
 * FileName    : GmSetConsignRuleWrapper.java 
 * Description :
 * Author      : vprasath
 * Date        : Jun 9, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmRuleBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 *
 */
public class GmSetConsignRuleWrapper extends GmRuleEngineWrapper {

	/* (non-Javadoc)
	 * @see com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest)
	 */
	List alList;
	public List formatData(HttpServletRequest request){
		alList = new ArrayList();
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		gmDataStoreVO = GmFramework.getCompanyParams(request);
		GmRuleBean gmRuleBean = new GmRuleBean(gmDataStoreVO);
		ArrayList alConsignList = new ArrayList();
		HashMap hmHeaderValues = new HashMap();
		HashMap hmValues = new HashMap();
		
		String strTxn = "50997"; //GmCommonClass.parseNull((String)request.getAttribute("RE_TXN"));
		//strTxn = strTxn.equals("")?GmCommonClass.parseNull((String)request.getParameter("RE_TXN")):strTxn;
        String strTxnStatus = GmCommonClass.parseNull((String)request.getAttribute("txnStatus"));
        strTxnStatus = strTxnStatus.equals("")?GmCommonClass.parseNull((String)request.getParameter("txnStatus")):strTxnStatus;
        hmHeaderValues = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("HEADERDATA"));
        alConsignList = GmCommonClass.parseNullArrayList((ArrayList)request.getAttribute("ALDATA"));
        String hinputstring = GmCommonClass.parseNull((String)request.getParameter("hinputstring"));
        String strSetId = GmCommonClass.parseNull((String)request.getParameter("setID"));
        strSetId = strSetId.equals("")?GmCommonClass.parseNull((String)hmHeaderValues.get("SETID")):strSetId;
        String hTxnId = GmCommonClass.parseNull((String)request.getParameter("hTxnId"));
        hTxnId = hTxnId.equals("")?GmCommonClass.parseNull((String)hmHeaderValues.get("CONSIGNID")):hTxnId;
        String strCompanyID = gmDataStoreVO.getCmpid();
        /*
        log.debug("inputstr..."+hinputstring);
        log.debug("strSetId...."+strSetId);
        log.debug("hTxnId...."+hTxnId);
		log.debug("hmHeaderValues "+hmHeaderValues);
		log.debug("alConsignList "+alConsignList);
		
		log.debug("strTxnStatus = "+strTxnStatus);
		
        log.debug("strTxn = "+strTxn);*/
        log.debug("strSetId...."+strSetId);
		try
		{
			request.setAttribute("SETID", strSetId);
			String strCountry = GmCommonClass.parseNull(gmRuleBean.fetchCNCountry(hTxnId));
			strCountry = strCountry.equals("")?"US":strCountry;
			if(alConsignList.size() > 0 && hmHeaderValues.size() > 0)
	        {
	        	for (int i=0; i< alConsignList.size(); i++)
				{
	        		hmValues = (HashMap)alConsignList.get(i);
					GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
					gmRuleParamVO.setPnum((String)hmValues.get("PNUM"));
					gmRuleParamVO.setControlNumber((String)hmValues.get("CONTROLNUM"));
					gmRuleParamVO.setTxn(strTxn);
					gmRuleParamVO.setTxnStatus(strTxnStatus);
					gmRuleParamVO.setQty((String)hmValues.get("QTY"));
					gmRuleParamVO.setSetID(strSetId);
					gmRuleParamVO.setCountry(strCountry);
					gmRuleParamVO.setTxnID(hTxnId);
					gmRuleParamVO.setCmpid(strCompanyID);
					alList.add(gmRuleParamVO);
					
				}
	        }
	        else
	        {
	        	if(!hinputstring.equals(""))
	        	{
	        		String[] strArray = hinputstring.split("\\|");
					log.debug("strArray..."+strArray.length);
					for (int j=0; j<strArray.length; j++)
					{
						String[] strValues = strArray[j].split("\\^");
						GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
						gmRuleParamVO.setPnum(strValues[0]);
						gmRuleParamVO.setQty(strValues[1]);
						gmRuleParamVO.setControlNumber(strValues[2]);					
						gmRuleParamVO.setTxn(strTxn); 
						gmRuleParamVO.setTxnStatus(strTxnStatus);						
						gmRuleParamVO.setSetID(strSetId);
						gmRuleParamVO.setCountry(strCountry);
						gmRuleParamVO.setTxnID(hTxnId);
						gmRuleParamVO.setCmpid(strCompanyID);
						alList.add(gmRuleParamVO);									
					}
	        	}
	        }
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
		GmRuleParamVO gmRuleParamVO1 = new GmRuleParamVO();
		for(int j= 0; j< alList.size(); j++)
		{
			gmRuleParamVO1 = (GmRuleParamVO)alList.get(j);
			log.debug("gmRuleParamVO1 = "+gmRuleParamVO1.toHashMap());
			
		}
        log.debug("alList "+alList);
		return alList;
	}
}
