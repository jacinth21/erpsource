package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmBuiltSetVerifyWrapper extends GmRuleEngineWrapper {

	public List formatData(HttpServletRequest request) throws AppError {
		ArrayList alList = new ArrayList();
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		gmDataStoreVO = GmFramework.getCompanyParams(request);
		String strTxnStatus = GmCommonClass.parseNull((String) request.getParameter("txnStatus"));
		strTxnStatus = strTxnStatus.equals("") ? GmCommonClass.parseNull((String) request.getAttribute("txnStatus")) : strTxnStatus;
		String strCompanyID = gmDataStoreVO.getCmpid();
		log.debug("strTxnStatus: "+strTxnStatus);
		ArrayList alReturn = new ArrayList();
		
		if(strTxnStatus.equals("PROCESS"))
		{
			HashMap hmReturn = (HashMap)request.getAttribute("hmReturn");
			ArrayList alIniSets =  (ArrayList)hmReturn.get("INISETS");
			Iterator itr =  alIniSets.iterator();
			String strConsignIds = "";
			String strConsignId = "";
			
			while(itr.hasNext())
			{
			  HashMap hmRow = 	(HashMap)itr.next();
			  strConsignId = (String)hmRow.get("CID");
			  strConsignIds += strConsignId + ","; 
			}
			if(!strConsignIds.equals("")) 
			{
			strConsignIds = strConsignIds.substring(0, strConsignIds.lastIndexOf(','));
			log.debug(strConsignIds);
			 alReturn = fetchSetDetails(strConsignIds);
			}
			log.debug("alReturn: "+alReturn);
		}
		else if(strTxnStatus.equals("VERIFY"))
		{
		String strInput = GmCommonClass.parseNull((String) request.getParameter("hinputStr"));
		log.debug("strInput: "+strInput);
		String strArr[] = strInput.split("\\|");
		String strConsignId = "";
		String strConsignIds = "";
		for(int i=0; i < strArr.length;i++ )
		{
			strConsignId = strArr[i].subSequence(0, strArr[i].indexOf('^')).toString();
			log.debug("strConsignId: "+strConsignId);
			//gmOperationsBean.loadSavedSetMaster(strConsignId);
			strConsignIds += strConsignId + ",";
		}
		if(!strConsignIds.equals("")) 
		{
		strConsignIds = strConsignIds.substring(0, strConsignIds.lastIndexOf(','));
		log.debug(strConsignIds);
		alReturn = fetchSetDetails(strConsignIds);
		}
		}	
		Iterator itrAlReturn = alReturn.iterator();
		while(itrAlReturn.hasNext())
		{
		  HashMap hmRow = 	(HashMap)itrAlReturn.next();
		  String strPnum = (String)hmRow.get("PNUM");
		  String strCnum = (String)hmRow.get("CNUM");
		  GmRuleParamVO gmRuleParamVO = new  GmRuleParamVO();
		  gmRuleParamVO.setPnum(strPnum);
		  gmRuleParamVO.setControlNumber(strCnum);
		  gmRuleParamVO.setTxn("50910");
		  gmRuleParamVO.setTxnStatus(strTxnStatus);
		  gmRuleParamVO.setCmpid(strCompanyID);
		  alList.add(gmRuleParamVO);
		}
		log.debug("alReturn: "+alReturn);
		
		return alList;
	}
	
	public ArrayList fetchSetDetails(String strConsignId) throws AppError
	{
		ArrayList alReturn = new ArrayList();
		String strConsignIds = GmCommonClass.getStringWithQuotes(strConsignId);		
		GmDBManager gmDBManager  = new GmDBManager();
		try
		    {
				StringBuffer sbQuery = new StringBuffer();
			/*	sbQuery.append("SELECT  T505.C205_PART_NUMBER_ID PNUM ");
				sbQuery.append(",T505.C505_CONTROL_NUMBER CNUM");
				sbQuery.append(" FROM    T504_CONSIGNMENT T504 ");
				sbQuery.append(", T505_ITEM_CONSIGNMENT T505");
				sbQuery.append(", (SELECT T208.C205_PART_NUMBER_ID, T208.C208_CRITICAL_FL CRITFL");
				sbQuery.append(",DECODE(T208.C208_CRITICAL_QTY,NULL,T208.C208_SET_QTY,T208.C208_CRITICAL_QTY) CRITQTY");
				sbQuery.append(",T208.C208_INSET_FL SETFL ");
				sbQuery.append(",T208.C208_CRITICAL_TAG CTRITAG");
				sbQuery.append(",GET_CODE_NAME(T208.C901_CRITICAL_TYPE) CRITTAG");
				sbQuery.append(",T208.C208_SET_QTY QTY");
				sbQuery.append(" FROM T208_SET_DETAILS T208, T504_CONSIGNMENT T504");
				sbQuery.append(" WHERE T504.C504_CONSIGNMENT_ID IN ('");
				sbQuery.append(strConsignIds);
				sbQuery.append("') AND T208.C207_SET_ID = T504.C207_SET_ID) SETDT");
				sbQuery.append(" WHERE  T504.C504_CONSIGNMENT_ID IN ('");
				sbQuery.append(strConsignIds);
				sbQuery.append("') AND T504.C504_CONSIGNMENT_ID = T505.C504_CONSIGNMENT_ID");
				sbQuery.append(" AND T505.C205_PART_NUMBER_ID = SETDT.C205_PART_NUMBER_ID (+)");
				sbQuery.append(" ORDER BY T505.C205_PART_NUMBER_ID ");*/
				
				// Added the new script to load part# and control# without duplicates
				sbQuery.append("SELECT T505.C205_PART_NUMBER_ID  PNUM   , T505.C505_CONTROL_NUMBER CNUM ");
				sbQuery.append("FROM T505_ITEM_CONSIGNMENT T505 WHERE ");
				sbQuery.append("T505.C504_CONSIGNMENT_ID IN ('");
				sbQuery.append(strConsignIds);
				sbQuery.append("') AND T505.C505_VOID_FL IS NULL ORDER BY T505.C205_PART_NUMBER_ID ");
				log.debug(" SETLOAD query " + sbQuery.toString());
				alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
				//hmReturn.put("SETLOAD",alReturn);
				gmDBManager.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			gmDBManager.close();
		}
		return alReturn;
	}
}
