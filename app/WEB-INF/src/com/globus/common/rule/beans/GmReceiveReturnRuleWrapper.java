/**
 * FileName    : GmInHouseTransRuleWrapper 
 * Description :
 * Author      : Brinal
 * Date        : Nov 10, 2010 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.rule.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmRuleBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author Brinal
 *
 */
public class GmReceiveReturnRuleWrapper extends GmRuleEngineWrapper {

	/* (non-Javadoc)
	 * @see com.globus.common.rule.beans.GmRuleEngineWrapper#formatData(javax.servlet.http.HttpServletRequest)
	 */
	List alList;
	String strInputString = "";
	public List formatData(HttpServletRequest request){
		alList = new ArrayList();
		GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
		gmDataStoreVO = GmFramework.getCompanyParams(request);
		GmRuleBean gmRuleBean = new GmRuleBean(gmDataStoreVO);
		ArrayList alRaDetails = new ArrayList();
		HashMap hmReturn = new HashMap();
		HashMap hmValues = new HashMap();
		HashMap hmConsign = new HashMap();
		HashMap hmResult = new HashMap();
		String hConsignId ="";
		String strConsignId = "";
		String strCountry = "";
		String strQtyInsp = "";
		String strInsetFl = "";
		String strPNUM = "";
		String strTxnStatus ="";
		String strTxn  ="";
		String strAction ="";
		String strType="";
		String strTxnTypeId ="";
		String strCompanyID = gmDataStoreVO.getCmpid();
		strTxn = GmCommonClass.parseNull((String)request.getAttribute("RE_TXN"));
		strTxn = strTxn.equals("")?GmCommonClass.parseNull((String)request.getParameter("RE_TXN")):strTxn;
        strTxnStatus = GmCommonClass.parseNull((String)request.getAttribute("txnStatus"));
        strTxnStatus = strTxnStatus.equals("")?GmCommonClass.parseNull((String)request.getParameter("txnStatus")):strTxnStatus;
        strAction = GmCommonClass.parseNull((String)request.getAttribute("hAction"));
        strAction = strAction.equals("")?GmCommonClass.parseNull((String)request.getParameter("hAction")):strAction;
        hmReturn = GmCommonClass.parseNullHashMap((HashMap)request.getAttribute("hmReturn"));   
       // hmReturnDetails = GmCommonClass.parseNullHashMap((HashMap)hmReturn.get("RAITEMDETAILS"));   
        alRaDetails = GmCommonClass.parseNullArrayList((ArrayList)hmReturn.get("RAITEMDETAILS"));
        log.debug("GmReceiveReturnRuleWrapper hmConsign "+hmReturn);
        log.debug("GmReceiveReturnRuleWrapper alRaDetails "+alRaDetails);
        hConsignId = GmCommonClass.parseNull((String)request.getAttribute("hConsignId"));
        hConsignId = hConsignId.equals("")?GmCommonClass.parseNull((String)hmConsign.get("CONSIGNID")):hConsignId;
        strTxnTypeId = GmCommonClass.parseNull((String)request.getParameter("TXN_TYPE_ID"));
         
        try
		{	      
			if(alRaDetails.size() > 0 )
	        {
				for (int i=0; i< alRaDetails.size(); i++)
				{
	        		hmValues = (HashMap)alRaDetails.get(i);
	        	
	        		strPNUM = GmCommonClass.parseNull((String)hmValues.get("PNUM"));
	        		GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
					gmRuleParamVO.setControlNumber((String)hmValues.get("CNUM"));
					gmRuleParamVO.setPnum(strPNUM);
					gmRuleParamVO.setTxnStatus(strTxnStatus);
					//set txntypeid here, if it's 'RETURN', will skip expiry validation	
					gmRuleParamVO.setTxnTypeID(strTxnTypeId);
					gmRuleParamVO.setCmpid(strCompanyID);
					alList.add(gmRuleParamVO);
				}
	        }
	        else
	        {
	        	if (strAction.equals("Save"))
                {
                    strInputString = GmCommonClass.parseNull(formateString(request));
                    log.debug("GmReceiveReturnRuleWrapper string....."+strInputString);
                    if(!strInputString.equals(""))
		        	{
		        		String[] strArray = strInputString.split("\\|");
						log.debug("strArray..."+strArray.length);
						for (int j=0; j<strArray.length; j++)
						{
							String[] strValues = strArray[j].split("\\,");
							strQtyInsp = GmCommonClass.parseNull(strValues[1]);
			        		strQtyInsp = strQtyInsp.equals("")?"0":strQtyInsp;
							GmRuleParamVO gmRuleParamVO = new GmRuleParamVO();
							gmRuleParamVO.setPnum(strValues[0]);
							gmRuleParamVO.setControlNumber(strValues[2]);					
							gmRuleParamVO.setTxnStatus(strTxnStatus);
							//set txntypeid here, if it's 'RETURN', will skip expiry validation	
							gmRuleParamVO.setTxnTypeID(strTxnTypeId);
							gmRuleParamVO.setCmpid(strCompanyID);
							alList.add(gmRuleParamVO);
						}
		        	}
                }
	        }
		}
		catch(Exception exp)
		{
			exp.printStackTrace();
		}
        log.debug("GmReceiveReturnRuleWrapper alList.................. "+alList.size());
		return alList;
	}
	private String formateString(HttpServletRequest request)
	{
		String strCount = "";
		String strPartNum = "";
		String strQty = "";
        String strControl = "";       
        
        int intCount = 0;
        strCount = request.getParameter("hCnt")==null?"":request.getParameter("hCnt");
        intCount = Integer.parseInt(strCount);
        for (int i=0;i<=intCount;i++)
        {
            strPartNum = request.getParameter("hPartNum"+i)==null?"":request.getParameter("hPartNum"+i);
            if (!strPartNum.equals(""))
            {
                strQty = GmCommonClass.parseNull(request.getParameter("hPartNumTotalQty"+i));
                strQty = strQty.equals("")?"0":strQty;
                strControl = GmCommonClass.parseNull(request.getParameter("Txt_CNum"+i));
                strControl = strControl.equals("")?" ":strControl;
                strInputString = strInputString.concat(strPartNum).concat(",").concat(strQty).concat(",").concat(strControl).concat("|");
            }
        }
        return strInputString;
	}

}
