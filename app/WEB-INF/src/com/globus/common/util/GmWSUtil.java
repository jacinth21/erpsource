package com.globus.common.util;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.WrapDynaBean;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.GmDataVO;
import com.globus.valueobject.prodmgmnt.GmShareEngineVO;
import com.globus.valueobject.sales.GmCompanyVO;

public class GmWSUtil {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public List getVOListFromDynaBeanList(List alDynaBeans, GmDataStoreVO gmDataStoreVO)
      throws AppError {
    List<GmDataStoreVO> alReturn = new ArrayList<GmDataStoreVO>();
    WrapDynaBean dataStore = null;
    Iterator itrDynaBean = alDynaBeans.iterator();

    try {
      while (itrDynaBean.hasNext()) {
        BasicDynaBean db = (BasicDynaBean) itrDynaBean.next();
        Map hmMap = db.getMap();
        Iterator itrKeys = hmMap.keySet().iterator();
        dataStore = new WrapDynaBean(gmDataStoreVO.getClass().newInstance());

        while (itrKeys.hasNext()) {
          String strKey = (String) itrKeys.next();
          String strValue = String.valueOf(db.get(strKey));
          dataStore.set(strKey.toLowerCase(), strValue);
        }
        alReturn.add((GmDataStoreVO) dataStore.getInstance());
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }

    return alReturn;
  }

  public List getGmVOListFromDynaBeanList(List alDynaBeans, GmDataStoreVO gmDataStoreVO)
      throws AppError {
    List<GmDataStoreVO> alReturn = new ArrayList<GmDataStoreVO>();
    WrapDynaBean dataStore = null;
    Iterator itrDynaBean = alDynaBeans.iterator();

    try {
      while (itrDynaBean.hasNext()) {
        BasicDynaBean db = (BasicDynaBean) itrDynaBean.next();
        Map hmMap = db.getMap();
        Iterator itrKeys = hmMap.keySet().iterator();
        dataStore = new WrapDynaBean(gmDataStoreVO.getClass().newInstance());

        while (itrKeys.hasNext()) {
          String strKey = (String) itrKeys.next();
          String strValue = String.valueOf(db.get(strKey));
          dataStore.set(strKey.toLowerCase(), strValue);
        }
        alReturn.add((GmDataStoreVO) dataStore.getInstance());
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }

    return alReturn;
  }

  public List getVOListFromHashMapList(List alMap, GmDataVO gmDataVO) throws AppError {
    List<GmDataVO> alReturn = new ArrayList<GmDataVO>();
    WrapDynaBean dataStore = null;
    Iterator itrHashMap = alMap.iterator();

    try {

      while (itrHashMap.hasNext()) {
        HashMap hmMap = (HashMap) itrHashMap.next();
        Iterator itrKeys = hmMap.keySet().iterator();
        dataStore = new WrapDynaBean(gmDataVO.getClass().newInstance());

        while (itrKeys.hasNext()) {
          String strKey = (String) itrKeys.next();
          String strValue = String.valueOf(hmMap.get(strKey));
          dataStore.set(strKey.toLowerCase(), strValue);
        }
        alReturn.add((GmDataVO) dataStore.getInstance());
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }
    return alReturn;
  }

  public List getVOListFromHashMapList(List alMap, GmDataVO gmDataVO, Properties properties)
      throws AppError {
    List<GmDataVO> alReturn = new ArrayList<GmDataVO>();
    WrapDynaBean dataStore = null;
    if (alMap.size() == 0) {
      return alReturn;
    }

    Iterator itrHashMap = alMap.iterator();

    try {
      while (itrHashMap.hasNext()) {
        HashMap hmMap = (HashMap) itrHashMap.next();
        Iterator itrKeys = hmMap.keySet().iterator();
        dataStore = new WrapDynaBean(gmDataVO.getClass().newInstance());

        while (itrKeys.hasNext()) {
          String strKey = (String) itrKeys.next();
          String strValue = String.valueOf(hmMap.get(strKey));
          if (properties.containsKey(strKey))
            dataStore.set(((String) properties.get(strKey)).toLowerCase(), strValue);
        }
        alReturn.add((GmDataVO) dataStore.getInstance());
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }
    return alReturn;
  }

  public List getVOListFromHashMapListWithCaseProperty(List alMap, GmDataStoreVO gmDataStoreVO,
      Properties properties) throws AppError {
    List<GmDataStoreVO> alReturn = new ArrayList<GmDataStoreVO>();
    WrapDynaBean dataStore = null;
    if (alMap.size() == 0) {
      return alReturn;
    }

    Iterator itrHashMap = alMap.iterator();

    try {
      while (itrHashMap.hasNext()) {
        HashMap hmMap = (HashMap) itrHashMap.next();
        Iterator itrKeys = hmMap.keySet().iterator();
        dataStore = new WrapDynaBean(gmDataStoreVO.getClass().newInstance());

        while (itrKeys.hasNext()) {
          String strKey = (String) itrKeys.next();
          String strValue = String.valueOf(hmMap.get(strKey));
          if (properties.containsKey(strKey))
            dataStore.set(((String) properties.get(strKey)), strValue);
        }
        alReturn.add((GmDataStoreVO) dataStore.getInstance());
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }
    return alReturn;
  }


  public GmDataStoreVO getVOFromHashMap(HashMap hmMap, GmDataStoreVO gmDataStoreVO) throws AppError {
    Iterator itrKeys = hmMap.keySet().iterator();
    WrapDynaBean dataStore = null;

    try {
      dataStore = new WrapDynaBean(gmDataStoreVO);
      while (itrKeys.hasNext()) {
        String strKey = (String) itrKeys.next();
        String strValue = String.valueOf(hmMap.get(strKey));
        dataStore.set(strKey.toLowerCase(), strValue);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }

    return (GmDataStoreVO) dataStore.getInstance();
  }

  public GmDataStoreVO getVOFromHashMap(MultivaluedMap<String, String> hmMap,
      GmDataStoreVO gmDataStoreVO) throws AppError {
    Iterator itrKeys = hmMap.keySet().iterator();
    WrapDynaBean dataStore = null;

    try {
      dataStore = new WrapDynaBean(gmDataStoreVO);
      while (itrKeys.hasNext()) {
        String strKey = (String) itrKeys.next();
        String strValue = String.valueOf(hmMap.get(strKey).get(0));
        dataStore.set(strKey.toLowerCase(), strValue);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }

    return (GmDataStoreVO) dataStore.getInstance();
  }

  public GmDataStoreVO getVOFromHashMap(HashMap hmMap, GmDataStoreVO gmDataStoreVO,
      Properties properties) throws AppError {
    Iterator itrKeys = hmMap.keySet().iterator();
    WrapDynaBean dataStore = new WrapDynaBean(gmDataStoreVO);

    try {
      while (itrKeys.hasNext()) {
        String strKey = (String) itrKeys.next();
        String strValue = String.valueOf(hmMap.get(strKey));
        if (properties.containsKey(strKey))
          dataStore.set(((String) properties.get(strKey)).toLowerCase(), strValue);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }

    return (GmDataStoreVO) dataStore.getInstance();
  }

  /**
   * getHashMapFromVO - This method will populate the HashMap with the values present in the Value
   * Object Passed
   * 
   * @param ActionForm actForm
   * @return HashMap hmParam
   * @exception AppError
   */
  public static HashMap getHashMapFromVO(GmDataStoreVO gmDataStoreVO) throws AppError {
    String strMethodName = "";
    String strMethodReturnType = "";
    String strFieldNameFromMethod = "";
    Method[] methods = gmDataStoreVO.getClass().getMethods();
    Class[] types = new Class[] {};

    int msize = methods.length;
    HashMap hmParam = new HashMap();
    try {
      for (int i = 0; i < msize; i++) {
        Method methodInForm = methods[i];
        strMethodName = methodInForm.getName();
        Class clType = methodInForm.getReturnType();
        strMethodReturnType = clType.getName();
        if (strMethodReturnType.equals("java.lang.String")
            || strMethodReturnType.equals("java.util.Date")) {
          if (strMethodName.startsWith("get")) {
            strFieldNameFromMethod = strMethodName.substring(3, strMethodName.length());
            Method method = gmDataStoreVO.getClass().getMethod(strMethodName, types);
            hmParam.put(strFieldNameFromMethod.toUpperCase(),
                method.invoke(gmDataStoreVO, new Object[0]));
          }
        }
      }
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return hmParam;
  }

  /**
   * populateCompanyInfo - This method will populate the Company Info
   * 
   * @param String strJson
   * @return GmDataStoreVO gmDataStoreVO
   * @exception AppError
   */
  public GmDataStoreVO populateCompanyInfo(String strJson) throws AppError {
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    GmCompanyVO gmCompanyVO = new GmCompanyVO();
    GmCommonBean gmCommonBean = new GmCommonBean();
    log.debug("strJson" + strJson);
    parseJsonToVO(strJson, gmDataStoreVO);
    log.debug("CmpId = " + gmDataStoreVO.getCmpid());
    gmCompanyVO = gmCommonBean.getCompanyVO(gmDataStoreVO.getCmpid());

    gmDataStoreVO.setCmptzone(gmCompanyVO.getCmptzone());
    gmDataStoreVO.setCmpdfmt(gmCompanyVO.getCmpdfmt());
    return gmDataStoreVO;
  }

  public String getParamFromJsonString(String strJson, String strField) throws AppError {
    String strValue = "";
    if (strJson == null || strJson.equals("")) {
      return "";
    }

    try {
      JSONObject json = (JSONObject) new JSONParser().parse(strJson);
      strValue = String.valueOf(json.get(strField));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }
    return strValue;
  }

  public GmDataStoreVO parseJsonToVO(String strJson, GmDataStoreVO gmDataStoreVO) throws AppError {
    WrapDynaBean dataStore = null;

    try {
      JSONObject json = (JSONObject) new JSONParser().parse(strJson);
      Iterator itrKeys = json.keySet().iterator();
      dataStore = new WrapDynaBean(gmDataStoreVO);
      while (itrKeys.hasNext()) {
        String strKey = (String) itrKeys.next();
        String strValue = String.valueOf(json.get(strKey));
		log.debug(strKey.toLowerCase() + ": " + strValue);
        dataStore.set(strKey.toLowerCase(), strValue);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }
    return (GmDataStoreVO) dataStore.getInstance();
  }

  public Map parseJsonToMap(String strJson) throws AppError {
    Map hmMap = new HashMap();
    try {
      JSONObject json = (JSONObject) new JSONParser().parse(strJson);
      Iterator itrKeys = json.keySet().iterator();

      while (itrKeys.hasNext()) {
        String strKey = (String) itrKeys.next();
        String strValue = String.valueOf(json.get(strKey));
        hmMap.put(strKey, strValue);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }
    return hmMap;
  }


  /**
   * getDbInputStringFromVO this method is used to convert Object array to DB input sting based on
   * the field and row seperator This method is used when a JSON need to be saved to Database tables
   * 
   * @param arrObjArray
   * @param strFieldSeperator
   * @param strRowSeperator
   * @param hmLinkedDbInputStr
   * @param strPropertyName
   * @return
   * @throws AppError
   */
  public String getDbInputStringFromVO(Object[] arrObjArray, String strFieldSeperator,
      String strRowSeperator, Map hmLinkedDbInputStr, String strPropertyName) throws AppError {
    String srtFieldValue = "";
    String strValue = "";
    StringBuffer strBuffDbInputStr = new StringBuffer();


    if (arrObjArray.getClass().isArray()) {
      int length = Array.getLength(arrObjArray);
      try {
        for (int i = 0; i < length; i++) {
          Object arrayElement = Array.get(arrObjArray, i);
          // log.debug("arrayElement: " + arrayElement);

          Class clTtype = arrayElement.getClass();

          // Get Named Method
          Method propMethod = clTtype.getMethod(strPropertyName, null);
          // log.debug("propMethod.getName(): " + propMethod.getName());

          LinkedHashMap propNames = (LinkedHashMap) propMethod.invoke(arrayElement, new Object[0]);

          Iterator itrKeys = propNames.keySet().iterator();
          StringBuffer strRowBuff = new StringBuffer();
          while (itrKeys.hasNext()) {
            String strKey = (String) itrKeys.next();
            if (hmLinkedDbInputStr.containsKey(strKey)) {
              strValue = String.valueOf(propNames.get(strKey));

              Method mappedPropMethod = clTtype.getMethod(strValue, null);
              srtFieldValue = (String) mappedPropMethod.invoke(arrayElement, new Object[0]);

            }
            strRowBuff.append(srtFieldValue + strFieldSeperator);
          }
          String strDbInputStr = strRowBuff.substring(0, strRowBuff.length() - 1);
          strRowBuff = null;
          strBuffDbInputStr.append(strDbInputStr).append(strRowSeperator);

        }
      } catch (Exception exp) {
        exp.printStackTrace();
        throw new AppError(exp);
      }
    }
    log.debug("strBuffInputStr:=" + strBuffDbInputStr.toString());
    return strBuffDbInputStr.toString();
  }

  /**
   * convertVOToCommaString to convert object array to commas separated in put string This method is
   * not used but can be used if required in future
   * 
   * @param objVO
   * @return
   * @throws AppError
   * 
   *         public static String convertVOToCommaString(GmShareEngineVO shareVO)throws AppError{
   * 
   *         String [] arrSharedFiles = null; String strSharedFileInputStr ="";
   * 
   *         arrSharedFiles = shareVO.getGmSharedFileVO(); strSharedFileInputStr
   *         =GmCommonClass.createInputString(arrSharedFiles); return strSharedFileInputStr;
   * 
   *         }
   */

  public Object parseJsonToVO(String strJson, Object objVO) throws AppError {

    Object objParsedVO;
    ObjectMapper mapper = new ObjectMapper();
    try {

      objParsedVO = mapper.readValue(strJson, GmShareEngineVO.class);
      log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(
          mapper.readValue(strJson, Object.class)));


    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return objParsedVO;
  }

  /**
   * parseArrayListToJson - here the ArrayList object is converted into JSON string.
   * 
   * @param alList
   * @throws AppError
   * @return string
   */

  public String parseArrayListToJson(ArrayList alList) throws AppError {
    String strOutput = "";
    ObjectMapper mapper = new ObjectMapper();
    try {
      strOutput = mapper.writeValueAsString(alList);
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return strOutput;
  }

  /**
   * parseHashMapToJson - here the HashMap is converted into JSON string.
   * 
   * @param hmParam
   * @throws AppError
   * @return string
   */


  public String parseHashMapToJson(HashMap hmParam) throws AppError {
    String strOutput = "";
    ObjectMapper mapper = new ObjectMapper();
    try {
      strOutput = mapper.writeValueAsString(hmParam);
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return strOutput;
  }

}
