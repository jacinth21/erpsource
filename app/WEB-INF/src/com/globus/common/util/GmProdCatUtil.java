package com.globus.common.util;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.prodmgmnt.beans.GmShareEngineTransBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;

public class GmProdCatUtil {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**validateInput - This method is used to validation property in VO which are given in the HashMap
	 * Example if hashmap having langid-20607, uuid-20608 then it validate langid and uuid in VO
	 * If these values are not passed then it will throw error msg for the error code[hashmap value]
	 * @param gmDataStoreVO
	 * @param hmMap
	 */
	public void validateInput( GmDataStoreVO gmDataStoreVO, HashMap hmMap){
		log.debug("======hmMap========="+hmMap);
		Iterator itrKeys = hmMap.keySet().iterator();
		Class[] types = new Class[] {};
		try
		{
			while(itrKeys.hasNext())
			{
				String strKey = (String)itrKeys.next();
				String strMethodName =  "get"+Character.toUpperCase(strKey.charAt(0)) + strKey.substring(1);
				Method method = gmDataStoreVO.getClass().getMethod(strMethodName, types);
				String inputValue = (String) method.invoke(gmDataStoreVO, new Object[0]);			 	
				if(inputValue.equals("")){
					String strErrCode = GmCommonClass.parseNull((String)hmMap.get(strKey));
					throw new AppError("",strErrCode,'E'); 
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw new AppError(ex);
		}
	}
	
	/*
	 * deleteProdCatFiles method is used to delete the files from system folder
	 * in this method we are passing comma seprated inputSting (like = FileListId,FileListId,FileListId,)
	 * After that call gmCommonUploadBean.fetchUploadFilesInfo(inputSting) which will return files info
	 * get the details from the arraylist, append the file name to the path and check file is exists
	 * if file is available then delete it from folder.
	 */
	public void deleteProdCatFiles(String inputSting, String strUserID)throws AppError
	{
		GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
		GmShareEngineTransBean gmShareEngineTransBean = new GmShareEngineTransBean();
		
		String strFileGrp = "";
		String strFileRefId = "";
		String strFileName = "";                     
		String strDltFileName = "";
		String strArtifactType = "";
		String strSubType = "";
		String strDltEgnyteFileName = "";
		
		HashMap hmMessageObj = new HashMap();
		HashMap hmParamJob = new HashMap();
		HashMap hmFileProp = new HashMap();
		
		ArrayList alFilesInfo = new ArrayList();
		ArrayList alFilePath = null;
		alFilesInfo = gmCommonUploadBean.fetchUploadFilesInfo(inputSting);
		
		for(int i=0;i<alFilesInfo.size();i++)
        {
			HashMap hmInputParams = GmCommonClass.parseNullHashMap((HashMap)alFilesInfo.get(i));
			strFileGrp = GmCommonClass.parseNull((String)hmInputParams.get("FILEREFGRP"));
    		strFileRefId = GmCommonClass.parseNull((String)hmInputParams.get("FILEREFID"));
			strFileName = GmCommonClass.parseNull((String)hmInputParams.get("FILENAME"));
			strArtifactType = GmCommonClass.parseNull((String)hmInputParams.get("REFTYPE"));
			strSubType = GmCommonClass.parseNull((String)hmInputParams.get("SUBTYPE"));
		 	if(!strFileName.equals("")){
				alFilePath = new ArrayList();
				alFilePath.add(strFileGrp);
				alFilePath.add(strFileRefId);
				alFilePath.add(strArtifactType);
				alFilePath.add(strSubType);
				
				hmFileProp.put("PATHPROPERTY", "PRODCATALOGUPLOADDIR");
				hmFileProp.put("SEPARATOR", "\\");
				hmFileProp.put("APPENDPATH", alFilePath);
	   
	    		strDltFileName = getFilePath(hmFileProp);
	    		gmCommonUploadBean.deleteFile(strDltFileName+strFileName);
	    	}
        }
		String strConsumFileSync = GmCommonClass.parseNull((String)GmCommonClass.getString("BATCH_CONSUMER_MARKETING_COLL_FILE_SYNC"));
		
        if(strConsumFileSync.equals("YES")){
        	hmMessageObj.put("FILEIDS",inputSting);
        	hmMessageObj.put("ACTION","VOID");
        	
        	// PC-4295: spineIT upload details added to cloud
            hmMessageObj.put("USERID", strUserID);
            
        	hmParamJob.put("CONSUMERCLASS",GmCommonClass.getString("BATCH_CONSUMER_MARKETING_COLL_FILE_SYNC_CLASS"));
        	hmParamJob.put("MESSAGEOBJECT",hmMessageObj);
        	gmShareEngineTransBean.saveConsumerJms (hmParamJob);  
        }
	} // End of deleteProdCatFiles
	/*
	 * getFilePath method is used to create upload file path 
	 * in this method we are passing HashMap contains folder name, separator and arraylist
	 * By looping arraylist we are getting folder names, and then appending with separator
	 * finally this method returns String 
	 */
	public String getFilePath(HashMap hmParam)throws AppError{
		
		ArrayList alFilePath = new ArrayList();
		HashMap hmValues = new HashMap();
		String strUploadHomeDir = "";
		String strSeparator = "";
		String strAdditionalDir = "";
		String strProperty = GmCommonClass.parseNull((String)hmParam.get("PATHPROPERTY"));
		if(!strProperty.equals("")){
			strUploadHomeDir = GmCommonClass.getString(strProperty);
		}
		strSeparator = (String)hmParam.get("SEPARATOR");
		alFilePath = (ArrayList)hmParam.get("APPENDPATH");
		
		for(int i=0;i<alFilePath.size();i++){
			strAdditionalDir +=alFilePath.get(i).toString()+strSeparator; 
		}
		return strUploadHomeDir+strAdditionalDir; 
	}// End of getFilePath
	
	/* This is the method to populate the HashMap Key for Mobile Sales Dashboard App.
	 * The returned HashMap contains all the key info's to get the sales filter condition.
	 */
	public HashMap populateSalesParam(GmUserVO gmUserVO , GmSalesDashReqVO gmSalesDashReqVO) throws AppError{
		HashMap hmParam = new HashMap();

		String strUserID  	= GmCommonClass.parseNull((String)gmUserVO.getUserid());
		String strAccessLvlID 	= GmCommonClass.parseNull((String)gmUserVO.getAcid());
		String strDeptID 		= GmCommonClass.parseNull((String)gmUserVO.getDeptid());
		String strCompanyID 	= GmCommonClass.parseNull((String)gmUserVO.getCompanyid());
		String strDivisionID 	= GmCommonClass.parseNull((String)gmUserVO.getDivid());
		String strRepID 	    = GmCommonClass.parseNull((String)gmUserVO.getRepid());
		
		//When company id not passed as input, the default company id is taken here.
		String strReqCompanyID = GmCommonClass.parseNull((String)gmSalesDashReqVO.getCompanyid());
		String strReqDivisionID = GmCommonClass.parseNull((String)gmSalesDashReqVO.getDivids());
		strCompanyID  = (!strReqCompanyID.equals("")) ?strReqCompanyID:strCompanyID;
		strDivisionID = (!strReqDivisionID.equals(""))?strReqDivisionID:strDivisionID;

		hmParam.put("USER_ID",strUserID);
		hmParam.put("ACCES_LVL",strAccessLvlID);
		hmParam.put("DEPT_ID",strDeptID);
		hmParam.put("COMP_FILTER",strCompanyID);
		hmParam.put("DIV_FILTER",strDivisionID);
		hmParam.put("ZONE_FILTER",GmCommonClass.parseNull((String)gmSalesDashReqVO.getZoneids()));
		hmParam.put("REGN_FILTER",GmCommonClass.parseNull((String)gmSalesDashReqVO.getRegionids()));
		hmParam.put("DIST_FILTER",GmCommonClass.parseNull((String)gmSalesDashReqVO.getFsids()));
		hmParam.put("RPT_REPID",strRepID);

		hmParam.put("CURRTYPE", GmCommonClass.parseNull((String)gmSalesDashReqVO.getSalesdbcurrtype()));
		hmParam.put("CURR_SYMBOL", GmCommonClass.parseNull((String)gmSalesDashReqVO.getSalesdbcurrsymbol()));
		return hmParam;
		
	}
  public HashMap populateCRMSalesParam(GmUserVO gmUserVO, GmSalesDashReqVO gmSalesDashReqVO)
      throws AppError {
    HashMap hmParam = new HashMap();

    hmParam = populateSalesParam(gmUserVO, gmSalesDashReqVO);
    /*
     * When Associate Rep login. His user id passed as reporting repid. Otherwise, Associate Rep VIP
     * visit, Sales call will not load in Calendar / upcoming Activity reports
     */
    hmParam.put("RPT_REPID", gmUserVO.getUserid());
    hmParam.put("OVERRIDE_ASS_LVL", hmParam.get("ACCES_LVL"));
    hmParam.put("ACCS_LVL", hmParam.get("ACCES_LVL"));
    hmParam.put("COMP_FILTER","100803");//PC-3309 Trauma surgeon's surgical activity is not showing for Non-Sales Users
    return hmParam;

  }
}
