package com.globus.common.util.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmOverrideForm extends GmLogForm {
	private String overrideTypeId = "";
	private String overrideType = "";
	private String references = "";
	private String refValue = "";
	private String currentValue = "";
	private String newValue = "";
	private String type = "";
	private String refId = "";
	private ArrayList alTypes = new ArrayList();
	private String msg = "";
	private String refIdForLogReason = "";
	private String demandSheetMonthId = "";
	private ArrayList alTrail = new ArrayList();
	private ArrayList alRefMonths = new ArrayList();
	private String refMonth = "";
	private String strRefMonthDisabled = "";
	private String refType = "";
	private String partType = "";
	private String submitFlag = "";
    private ArrayList alRequest = new ArrayList();
    private String requestCount = ""; 
    private String demandMasterId = "";    
    private String    pdisablevalue     = "";

	public String getDemandMasterId() {
		return demandMasterId;
	}

	public void setDemandMasterId(String demandMasterId) {
		this.demandMasterId = demandMasterId;
	}

	public String getRequestCount() {
		return requestCount;
	}

	public void setRequestCount(String requestCount) {
		this.requestCount = requestCount;
	}

	public String getPartType() {
		return partType;
	}

	public void setPartType(String partType) {
		this.partType = partType;
	}

	public String getRefType() {
		return refType;
	}

	public void setRefType(String refType) {
		this.refType = refType;
	}

	public String getStrRefMonthDisabled() {
		return strRefMonthDisabled;
	}

	public void setStrRefMonthDisabled(String strRefMonthDisabled) {
		this.strRefMonthDisabled = strRefMonthDisabled;
	}

	/**
	 * @return Returns the alRefMonths.
	 */
	public ArrayList getAlRefMonths() {
		return alRefMonths;
	}

	/**
	 * @param alRefMonths
	 *            The alRefMonths to set.
	 */
	public void setAlRefMonths(ArrayList alRefMonths) {
		this.alRefMonths = alRefMonths;
	}

	/**
	 * @return Returns the refIdForLogReason.
	 */
	public String getRefIdForLogReason() {
		return refIdForLogReason;
	}

	/**
	 * @param refIdForLogReason
	 *            The refIdForLogReason to set.
	 */
	public void setRefIdForLogReason(String refIdForLogReason) {

		this.refIdForLogReason = refIdForLogReason;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "[overrideTypeId: " + overrideTypeId + ", overrideType: " + overrideType + " ,references:" + references + " ,refValue:" + refValue
				+ " ,currentValue:" + currentValue + " ,newValue:" + newValue + " ,type:" + type + " ,alTypes:" + alTypes + " ]";
	}

	/**
	 * @return Returns the msg.
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg
	 *            The msg to set.
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return Returns the alTypes.
	 */
	public ArrayList getAlTypes() {
		return alTypes;
	}

	/**
	 * @param alTypes
	 *            The alTypes to set.
	 */
	public void setAlTypes(ArrayList alTypes) {
		this.alTypes = alTypes;
	}

	/**
	 * @return Returns the currentValue.
	 */
	public String getCurrentValue() {
		return currentValue;
	}

	/**
	 * @param currentValue
	 *            The currentValue to set.
	 */
	public void setCurrentValue(String currentValue) {
		this.currentValue = currentValue;
	}

	/**
	 * @return Returns the newValue.
	 */
	public String getNewValue() {
		return newValue;
	}

	/**
	 * @param newValue
	 *            The newValue to set.
	 */
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	/**
	 * @return Returns the overrideType.
	 */
	public String getOverrideType() {
		return overrideType;
	}

	/**
	 * @param overrideType
	 *            The overrideType to set.
	 */
	public void setOverrideType(String overrideType) {
		this.overrideType = overrideType;
	}

	/**
	 * @return Returns the partNumber.
	 */
	public String getRefValue() {
		return refValue;
	}

	/**
	 * @param partNumber
	 *            The partNumber to set.
	 */
	public void setRefValue(String refValue) {
		this.refValue = refValue;
	}

	/**
	 * @return Returns the references.
	 */
	public String getReferences() {
		return references;
	}

	/**
	 * @param references
	 *            The references to set.
	 */
	public void setReferences(String references) {
		System.out.println(" Inside Oerride form -=----- " + references);
		this.references = references;
	}

	/**
	 * @return Returns the type.
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            The type to set.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return Returns the overrideTypeId.
	 */
	public String getOverrideTypeId() {
		return overrideTypeId;
	}

	/**
	 * @param overrideTypeId
	 *            The overrideTypeId to set.
	 */
	public void setOverrideTypeId(String overrideTypeId) {
		this.overrideTypeId = overrideTypeId;
	}

	/**
	 * @return Returns the refId.
	 */
	public String getRefId() {
		return refId;
	}

	/**
	 * @param refId
	 *            The refId to set.
	 */
	public void setRefId(String refId) {
		this.refId = refId;
	}

	/**
	 * @return Returns the alTrail.
	 */
	public ArrayList getAlTrail() {
		return alTrail;
	}

	/**
	 * @param alTrail
	 *            The alTrail to set.
	 */
	public void setAlTrail(ArrayList alTrail) {
		this.alTrail = alTrail;
	}

	/**
	 * @return Returns the demandSheetMonthId.
	 */
	public String getDemandSheetMonthId() {
		return demandSheetMonthId;
	}

	/**
	 * @param demandSheetMonthId
	 *            The demandSheetMonthId to set.
	 */
	public void setDemandSheetMonthId(String demandSheetMonthId) {
		this.demandSheetMonthId = demandSheetMonthId;
	}

	public String getRefMonth() {
		return refMonth;
	}

	public void setRefMonth(String refMonth) {
		this.refMonth = refMonth;
	}

	public String getSubmitFlag() {
		return submitFlag;
	}

	public void setSubmitFlag(String submitFlag) {
		this.submitFlag = submitFlag;
	}

	public ArrayList getAlRequest() {
		return alRequest;
	}

	public void setAlRequest(ArrayList alRequest) {
		this.alRequest = alRequest;
	}

	public String getPdisablevalue() {
		return pdisablevalue;
	}

	public void setPdisablevalue(String pdisablevalue) {
		this.pdisablevalue = pdisablevalue;
	}
}
