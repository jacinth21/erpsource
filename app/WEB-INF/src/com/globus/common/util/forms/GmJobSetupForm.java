package com.globus.common.util.forms;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.routines.DateValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCancelForm;
import com.globus.common.forms.GmLogForm;

public class GmJobSetupForm extends GmLogForm
{
    
    private String refId = "";
    private String refType = "";
    private String refTypeText = "";
    private String jobId = "";
    private String jobName = "";
    private String jobScheduleId = "";
    private String firstOccurence = "";
    private String activeflag = "";
    private String refStatus = "";

    private ArrayList alJobList = new ArrayList();
    private ArrayList alJobSchedule = new ArrayList();
    Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class. 
    
    /**
     * @return Returns the alJobList.
     */
    public ArrayList getAlJobList()
    {
        return alJobList;
    }
    /**
     * @param alJobList The alJobList to set.
     */
    public void setAlJobList(ArrayList alJobList)
    {
        this.alJobList = alJobList;
    }
    /**
     * @return Returns the alJobSchedule.
     */
    public ArrayList getAlJobSchedule()
    {
        return alJobSchedule;
    }
    /**
     * @param alJobSchedule The alJobSchedule to set.
     */
    public void setAlJobSchedule(ArrayList alJobSchedule)
    {
        this.alJobSchedule = alJobSchedule;
    }
    /**
     * @return Returns the firstOccurence.
     */
    public String getFirstOccurence()
    {
        return firstOccurence;
    }
       
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request ) {
            ActionErrors errors = new ActionErrors();
            String strFirstOccurence = getFirstOccurence();
            log.debug(" firstOccurence is " + strFirstOccurence);
            if (!strFirstOccurence.equals(""))
            {
                DateValidator dateVal = DateValidator.getInstance();
                
                DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                Date date = new Date();
                String strToday = dateFormat.format(date);
                log.debug(" today is " + strToday);
                Date today = dateVal.validate(strToday);
                
                    log.debug(" after null checkl firstOccurence is " + strFirstOccurence);
                Date dtFirstOccurence = dateVal.validate(strFirstOccurence);
                int dateCompare = dateVal.compareDates(dtFirstOccurence,today,null);
                log.debug("dateCompare is " +dateCompare);
                
                if( dateCompare <= 0 ) {
                  errors.add("MessageResources",new ActionMessage("error.job.firstOccurence"));
                  try{
                	  this.alJobSchedule = GmCommonClass.getCodeList("JOBSH");
                  }
                  catch (Exception exp){
                	  exp.printStackTrace();
                  }
                }
            }

            return errors;
        }

    /**
     * @return Returns the jobId.
     */
    public String getJobId()
    {
        return jobId;
    }
    /**
     * @param jobId The jobId to set.
     */
    public void setJobId(String jobId)
    {
        this.jobId = jobId;
    }
    /**
     * @return Returns the jobName.
     */
    public String getJobName()
    {
        return jobName;
    }
    /**
     * @param jobName The jobName to set.
     */
    public void setJobName(String jobName)
    {
        this.jobName = jobName;
    }
    /**
     * @return Returns the jobScheduleId.
     */
    public String getJobScheduleId()
    {
        return jobScheduleId;
    }
    /**
     * @param jobScheduleId The jobScheduleId to set.
     */
    public void setJobScheduleId(String jobScheduleId)
    {
        this.jobScheduleId = jobScheduleId;
    }
    /**
     * @return Returns the refId.
     */
    public String getRefId()
    {
        return refId;
    }
    /**
     * @param refId The refId to set.
     */
    public void setRefId(String refId)
    {
        this.refId = refId;
    }
    /**
     * @return Returns the refType.
     */
    public String getRefType()
    {
        return refType;
    }
    /**
     * @param refType The refType to set.
     */
    public void setRefType(String refType)
    {
        this.refType = refType;
    }
    /**
     * @return Returns the refTypeText.
     */
    public String getRefTypeText()
    {
        return refTypeText;
    }
    /**
     * @param refTypeText The refTypeText to set.
     */
    public void setRefTypeText(String refTypeText)
    {
        this.refTypeText = refTypeText;
    }
    /**
     * @return Returns the hCancelType.
     */
   
    /**
     * @return Returns the activeflag.
     */
    public String getActiveflag()
    {
        return activeflag;
    }
    /**
     * @param activeflag The activeflag to set.
     */
    public void setActiveflag(String activeflag)
    {
        this.activeflag = activeflag;
    }
    /**
     * @return Returns the refStatus.
     */
    public String getRefStatus()
    {
        return refStatus;
    }
    /**
     * @param refStatus The refStatus to set.
     */
    public void setRefStatus(String refStatus)
    {
        this.refStatus = refStatus;
    }
    public void setFirstOccurence(String firstOccurence)
    {
        this.firstOccurence = firstOccurence;
    }

}
