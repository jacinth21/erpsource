package com.globus.common.util.beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmOverrideBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to
																	// Initialize
																	// the
																	// Logger
																	// Class.

	public HashMap saveOverrideDetails(HashMap hmParam) throws AppError {
		HashMap hmReturn = new HashMap();
        log.debug("Values in hmparam in Bean :"+hmParam);  
		String strOverrideID = GmCommonClass.parseNull((String) hmParam.get("REFIDFORLOGREASON"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strOverrideTypeID = GmCommonClass.parseNull((String) hmParam.get("OVERRIDETYPEID"));
		GmCommonBean gmCommonBean = new GmCommonBean();
		String strMessage = "";

		try {
			GmDBManager gmDBManager = new GmDBManager();
			gmDBManager.setPrepareString("GM_PKG_CM_OVERRIDE.GM_CM_SAVE_OVERRIDE_DETAILS", 9);
			gmDBManager.setString(1, strOverrideTypeID);
			gmDBManager.setString(2, strOverrideID);
			gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("NEWVALUE")));
			gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("TYPE")));
			gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParam.get("REFVALUE")));
			gmDBManager.setString(6, strUserId);
			gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParam.get("REFERENCES")));
			gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParam.get("REFMONTH")));
			gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParam.get("PARTTYPE")));
			gmDBManager.execute();

			if (!strLogReason.equals("")) {
				if (strOverrideTypeID.equals("20398") || strOverrideTypeID.equals("20399")) {
					HashMap tempRHashMap = new HashMap(hmParam);
					tempRHashMap.put("OVERRIDETYPEID", "20395");
					strOverrideID = fetchRefID(tempRHashMap);
				}
				gmCommonBean.saveLog(gmDBManager, strOverrideID, strLogReason, strUserId, "1224");
			}
			gmDBManager.commit();
			gmDBManager.close();
			strMessage = "Overridden value saved successfully";
		} catch (Exception e) {
			strMessage = "Exception happened, data not saved Contact System Administrator";
		}
		log.debug("GROWTHDETAILSID : " + strOverrideID);
		hmReturn.put("GROWTHDETAILSID", strOverrideID);
		hmReturn.put("MSG", strMessage);

		return hmReturn;
	}

	public String fetchRefID(HashMap hmParam) throws AppError {
		String strRefID = "";
		log.debug("fetchRefID INSIDE:" + hmParam);
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_CM_OVERRIDE.GM_CM_FCH_OVERRIDE_REFID", 8);
		gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR);
		gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("REFID")));
		gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("REFERENCES")));
		gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("REFVALUE")));
		gmDBManager.setInt(4, Integer.parseInt((String) hmParam.get("OVERRIDETYPEID")));
		gmDBManager.setInt(5, Integer.parseInt(GmCommonClass.parseZero((String) hmParam.get("DEMANDSHEETMONTHID"))));
		gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParam.get("PARTTYPE")));
		gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParam.get("USERID")));
		gmDBManager.execute();
		strRefID = gmDBManager.getString(8);
		gmDBManager.commit();
		gmDBManager.close();
		return strRefID;
	}

	public ArrayList fetchOverrideHistory(String strRefId, String strTrailId) throws AppError {
		ArrayList alReturn = new ArrayList();
		ResultSet rs = null;
		log.debug("strRefId: "+strRefId+" strTrailId: "+strTrailId);
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("GM_PKG_CM_OVERRIDE.GM_CM_FCH_OVERRIDE_HISTORY", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, GmCommonClass.parseNull(strRefId));
		gmDBManager.setInt(2, Integer.parseInt(strTrailId));
		gmDBManager.execute();
		rs = (ResultSet) gmDBManager.getObject(3);
		alReturn = gmDBManager.returnArrayList(rs);
		gmDBManager.commit();
		gmDBManager.close();
		log.debug("Exit");
		return alReturn;
	}

	public void saveComments(HashMap hmParam) throws SQLException, AppError {
		log.debug("HmParam:"+hmParam);
		String strLogID = GmCommonClass.parseNull((String) hmParam.get("STRLOGID"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmDBManager gmDBManager = new GmDBManager();
		Connection conn = gmDBManager.getConnection();		
		gmCommonBean.saveLog(conn, strLogID, strLogReason, strUserId, "1224");
		conn.commit();
		gmDBManager.close();
	}
	
	public HashMap fetchGrowthRequestdetail(HashMap hmParam) throws AppError {
		log.debug("Hmparam: "+ hmParam);
		ArrayList alReturn = new ArrayList();
		String requestCount = "";
		int dsid = Integer.parseInt(GmCommonClass.parseZero((String) hmParam.get("DEMANDMASTERID")));
		String refid =  GmCommonClass.parseNull((String) hmParam.get("REFID"));
		if(refid.indexOf(".") == -1)
		{			 
			 refid =  GmCommonClass.parseNull((String) hmParam.get("REFVALUE"));
		}
		HashMap hmReturn = new HashMap();	
		  log.debug("refid: "+refid + "dsid :  "+dsid   );
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_op_sheet_summary.gm_fc_fch_growth_requestdetail", 6);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(6, OracleTypes.INTEGER);
		gmDBManager.setInt(1, dsid);
		gmDBManager.setString(2, refid);
		gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("REFTYPE")));
		gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("REFERENCES")));
		gmDBManager.execute();		
		alReturn = (ArrayList)((RowSetDynaClass)gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5))).getRows();
		requestCount = String.valueOf(gmDBManager.getObject(6));
		log.debug("alReturn :"+alReturn);
		hmReturn.put("RDREQUEST", alReturn);
		hmReturn.put("REQCOUNT", requestCount);
		gmDBManager.commit();
		gmDBManager.close();
		log.debug("Exit");
		return hmReturn;
	}

}
