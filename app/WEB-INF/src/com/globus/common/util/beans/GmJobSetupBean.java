package com.globus.common.util.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmJobSetupBean
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    GmCommonBean gmCommonBean = new GmCommonBean();
    
    /**
     * loadJobList - This method will be used to fetch the demand sheet
     * @param hmParam - to be used later for any filters
     * @return RowSetDynaClass - will be used by display tag
     * @exception AppError
     */
    public ArrayList loadJobList(HashMap hmParam) throws AppError
    {
        ArrayList alDemandSheet = new ArrayList();
        try{
                
                DBConnectionWrapper dbCon = null;
                dbCon = new DBConnectionWrapper();

                StringBuffer sbQuery = new StringBuffer();
                sbQuery.append(" SELECT C9300_JOB_ID CODEID, C9300_JOB_NM CODENM ");
                sbQuery.append(" FROM T9300_JOB ");
                sbQuery.append(" WHERE C9300_VOID_FL IS NULL ");
                
                log.debug(" Query to fetchJobList is " + sbQuery.toString());
                
                alDemandSheet = dbCon.queryMultipleRecords(sbQuery.toString());
        }
        
        catch (Exception e)
        {
                GmLogError.log("Exception in GmJobSetupBean:fetchJobList","Exception is:"+e);
                throw new AppError(e);
        }
        return alDemandSheet;
    }
    
    /**
     * saveJob - This method will be save / update the Job data 
     * @param hmParam - parameters to be saved / updated
     * @exception AppError
     */
    public String saveJob (HashMap hmParam)throws AppError
    {
        String strJobIdFromDb = "";
        try {
        GmDBManager gmDBManager = new GmDBManager ();
        
        String strJobId = GmCommonClass.parseNull((String)hmParam.get("JOBID"));
        String strJobName = GmCommonClass.parseNull((String)hmParam.get("JOBNAME"));
        String strRefId = GmCommonClass.parseNull((String)hmParam.get("REFID"));
        String strRefType = GmCommonClass.parseNull((String)hmParam.get("REFTYPE"));
        String strJobScheduleId = GmCommonClass.parseNull((String)hmParam.get("JOBSCHEDULEID"));
        String strFirstOccurence = GmCommonClass.parseNull((String)hmParam.get("FIRSTOCCURENCE"));
        String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        String strProcedureName = "Gm_pkg_op_ttp.gm_fc_sav_demsheet("+strJobId+")"; // GmCommonClass.parseNull((String)hmParam.get("PROCEDURENAME"));
        String strInActiveFlag = GmCommonClass.getCheckBoxValue((String)hmParam.get("ACTIVEFLAG"));
        String strLogReason = GmCommonClass.parseNull((String)hmParam.get("TXT_LOGREASON"));

        gmDBManager.setPrepareString("Gm_pkg_cm_job.gm_cm_sav_job",10);
        
        gmDBManager.registerOutParameter(10,java.sql.Types.CHAR);
        
        gmDBManager.setString(1,strJobId);
        gmDBManager.setString(2,strJobName);
        gmDBManager.setString(3,strRefId);
        gmDBManager.setString(4,strRefType);
        gmDBManager.setString(5,strJobScheduleId);
        gmDBManager.setString(6,strFirstOccurence);
        gmDBManager.setString(7,strUserId);
        gmDBManager.setString(8,strProcedureName);
        gmDBManager.setString(9,strInActiveFlag);
        gmDBManager.execute();
        
        strJobIdFromDb = gmDBManager.getString(10);
        log.debug("strJobIdFromDb  "+strJobIdFromDb);
        if (!strLogReason.equals(""))
        {
            gmCommonBean.saveLog(gmDBManager,strJobIdFromDb , strLogReason,strUserId, "1223"); // 1223 - Log Code Number for Job
        }
        gmDBManager.commit();
        }
        catch(Exception exp){
            exp.printStackTrace();
        }
        
        return strJobIdFromDb;
    }
    
    
    /**
     * fetchJobInfo - This method will be fetch job data for editing 
     * @param strJobId 
     * @return HashMap
     * @exception AppError
     */
    public HashMap fetchJobInfo(String strRefId, String strRefType) throws AppError
    {
        HashMap hmReturn = new HashMap();
        GmDBManager gmDBManager = new GmDBManager ();
        
        gmDBManager.setPrepareString("gm_pkg_cm_job.gm_cm_fch_job",3);
        
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
        gmDBManager.setString(1,strRefId);
        gmDBManager.setString(2,strRefType);
        gmDBManager.execute();
        hmReturn = GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(3)));
        gmDBManager.close();
        return hmReturn;
    }

    /**
     * fetchJobInfo - This method will be fetch job data for editing 
     * @param strJobId 
     * @return HashMap
     * @exception AppError
     */
    public void saveReRun(HashMap hmParam) throws AppError
    {
        GmDBManager gmDBManager = new GmDBManager ();
        
        String strJobId = GmCommonClass.parseNull((String)hmParam.get("JOBID"));
        String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        String strLogReason = GmCommonClass.parseNull((String)hmParam.get("TXT_LOGREASON"));

        gmDBManager.setPrepareString("gm_pkg_cm_job.gm_cm_sav_job_trigger",4);
        gmDBManager.setString(1,strJobId);
        gmDBManager.setString(2,""); // blank so that Oracle can take SYSDATE
        gmDBManager.setString(3,"90775"); // State is waiting
        gmDBManager.setString(4,"90786"); // Type is manual
        gmDBManager.execute();
        
        if (!strLogReason.equals(""))
        {
            gmCommonBean.saveLog(gmDBManager,strJobId , strLogReason,strUserId, "1223"); // 1223 - Log Code Number for Job 
        }
        
        gmDBManager.commit();
    }
}
