package com.globus.common.util.beans;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author mmuthusamy
 *
 */
public class GmRebateCalculationBean extends GmBean {


  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to

  // Initialize
  // the
  // Logger
  // Class.

  /**
   * @param gmDataStoreVO
   * @throws AppError
   */
  public GmRebateCalculationBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   * processDiscountOrder - method used to call the DS order create procedure.
   * 
   * @throws AppError
   */

  public void processDiscountOrder() throws AppError {
    log.debug(" Company id " + getCompId() + " Plant id " + getCompPlantId());

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_order_rebate_txn.gm_sav_rebate", 2);
    gmDBManager.setString(1, getCompId());
    gmDBManager.setString(2, getCompPlantId());
    gmDBManager.execute();
    gmDBManager.commit();

    log.debug(" Execute completed ");
  }
}
