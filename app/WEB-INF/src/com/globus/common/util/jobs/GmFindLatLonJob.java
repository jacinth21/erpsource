package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmFindLatLonJob extends GmActionJob {
	ArrayList alReturn = new ArrayList();
	public void execute(JobDataMap jobDataMap)throws Exception{
		Logger log = GmLogger.getInstance(this.getClass().getName());
		
		ArrayList alResult = new ArrayList();
		HashMap hmReturn = new HashMap();
		log.debug("GmFindLatLonJob is started: ");
		fetchSalesRepAddress();
		fetchDistAddress();
		fetchAccAddress();
		
		if(alReturn.size()>0)
		{
		Exception ex =null;
		String strError=" Below exceptions are found in GmFindLatLonJob,<br><br>";
		for(int j=0;j<alReturn.size();j++)
		{
			hmReturn=(HashMap)alReturn.get(j);
			log.debug("inside for loop hmreturn= "+hmReturn);
			String Tbl = GmCommonClass.parseNull((String)hmReturn.get("TBL"));
			String Id = GmCommonClass.parseNull((String)hmReturn.get("ID"));
			
			ex = (Exception)hmReturn.get("EXCEPTION");
			
			if(ex != null)
			{
				strError+=	"Status - <b>"+GmJob.JOB_STATUS_FAIL+"</b> for Table: <b>"+Tbl+"</b> for Address ID: <b>"+Id+"</b> Exception is : <b>"+ex+"</b><br>";
			}
		}
		log.debug("strError message = "+strError);
		log.debug("GmFindLatLonJob is Completed.");
		sendEmail("GmFindLatLonJob",strError);
		}
	}
	
	private void fetchSalesRepAddress()throws Exception{
		ArrayList alResult= new ArrayList();
		ArrayList alReqDetails = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_gmap.gm_fch_salesrep_addressinfo", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReqDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		alResult = GmCommonClass.parseNullArrayList(fetchAddressList(alReqDetails,""));
		updateSalesRepLatLon(alResult,"");
		
	}
	
	private void fetchDistAddress()throws Exception{
		ArrayList alResult= new ArrayList();
		ArrayList alReqDetails = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_gmap.gm_fch_dist_addressinfo", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReqDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		alResult = GmCommonClass.parseNullArrayList(fetchAddressList(alReqDetails,""));
		updateDistLatLon(alResult,"");
		alResult = GmCommonClass.parseNullArrayList(fetchAddressList(alReqDetails,"BILL"));
		updateDistLatLon(alResult,"BILL");
	}
	
	private void fetchAccAddress()throws Exception{
		ArrayList alResult= new ArrayList();
		ArrayList alReqDetails = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_gmap.gm_fch_acc_addressinfo", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReqDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		alResult = GmCommonClass.parseNullArrayList(fetchAddressList(alReqDetails,""));
		updateAccLatLon(alResult,"");
		alResult = GmCommonClass.parseNullArrayList(fetchAddressList(alReqDetails,"BILL"));
		updateAccLatLon(alResult,"BILL");
	}
	
	private ArrayList fetchAddressList(ArrayList alReqDetails,String strbill)throws Exception{
		ArrayList alResult= new ArrayList();
		for (int i = 0; i < alReqDetails.size(); i++) {
			HashMap hmReqDetails = new HashMap();
			String strAddR1 = "";
			String strAddR2 = "";
			String strCity = "";
			String strState = "";
			String strCountry = "";
			String strZipcode = "";
			
			hmReqDetails = GmCommonClass.parseNullHashMap((HashMap) alReqDetails.get(i));
			String strTbl = GmCommonClass.parseNull((String) hmReqDetails.get("TBL"));
			String strId = GmCommonClass.parseNull((String) hmReqDetails.get("ID"));
			if (strbill.equals("BILL")){
				strAddR1 = GmCommonClass.parseNull((String) hmReqDetails.get("BADDR1"));
				strAddR2 = GmCommonClass.parseNull((String) hmReqDetails.get("BADDR2"));
				strCity = GmCommonClass.parseNull((String) hmReqDetails.get("BCITY"));
				strState = GmCommonClass.parseNull((String) hmReqDetails.get("BSTATE"));
				strCountry = GmCommonClass.parseNull((String) hmReqDetails.get("BCOUNTRY"));
				strZipcode = GmCommonClass.parseZero((String) hmReqDetails.get("BZIPCODE"));	
			}
			else{
				strAddR1 = GmCommonClass.parseNull((String) hmReqDetails.get("ADDR1"));
				strAddR2 = GmCommonClass.parseNull((String) hmReqDetails.get("ADDR2"));
				strCity = GmCommonClass.parseNull((String) hmReqDetails.get("CITY"));
				strState = GmCommonClass.parseNull((String) hmReqDetails.get("STATE"));
				strCountry = GmCommonClass.parseNull((String) hmReqDetails.get("COUNTRY"));
				strZipcode = GmCommonClass.parseZero((String) hmReqDetails.get("ZIPCODE"));
			}
			strAddR1 = strAddR1.replaceAll("\\s", "+");
			strAddR2 = strAddR2.replaceAll("\\s", "+");
			strCity = strCity.replaceAll("\\s", "+");
			strState = strState.replaceAll("\\s", "+");
			strCountry = strCountry.replaceAll("\\s", "+");
			String strAddress="line1="+strAddR1+","+strAddR2+"&line2="+strCity+","+strState+","+strCountry;
			
			strAddress=strAddress.replaceAll("'", "");
			strAddress=strAddress.replaceAll(":", "");
			strAddress=strAddress.replaceAll("#", "");
			strAddress=strAddress.replaceAll("N/A", "");
			
			if(strAddR1.length()>0 || !strAddR1.equals("")) 
			{	HashMap hmResult = new HashMap();
				hmResult = GmCommonClass.parseNullHashMap(fetchLatLon(strTbl, strId ,strAddress));
				alResult.add(hmResult);
			}
		}
		return alResult;
	}
	
	private HashMap fetchLatLon(String strTbl ,String strId ,String strAddress) throws AppError,ParserConfigurationException, SAXException
	{
		String latitude = "";
		String longitude = "";
		HashMap hmreturn = new HashMap();
		HashMap hmReturn = new HashMap();  
		HashMap hmResult = new HashMap();
			 try{
			 DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
             DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
             Document doc =  docBuilder.parse("http://where.yahooapis.com/geocode?"+ strAddress);
             doc.getDocumentElement().normalize ();
                        
             NodeList result = doc.getElementsByTagName("latitude");
             latitude =result.item(0).getFirstChild().getNodeValue();  
             result = doc.getElementsByTagName("longitude");
             longitude = result.item(0).getFirstChild().getNodeValue(); 
             
             hmResult.put("LATITUDE", latitude);
             hmResult.put("LONGITUDE", longitude);
             hmResult.put("TBL",strTbl);
             hmResult.put("ID",strId);
			 } catch (Exception e) {   
				 hmReturn = new HashMap();
				 hmReturn.put("EXCEPTION", e);
				 hmReturn.put("ID",strId);
				 hmReturn.put("TBL",strTbl);
				 alReturn.add(hmReturn);
            }              	  
          return hmResult;
	}
	
	private void updateSalesRepLatLon(ArrayList alResult,String strbill)throws AppError{
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_gmap.gm_sav_salesrep_latlon_info", 5);
		long starttime = System.currentTimeMillis();
		for(int i=0;i<alResult.size();i++)
		{
			HashMap hmaddress = GmCommonClass.parseNullHashMap((HashMap)alResult.get(i));
			String strTbl = GmCommonClass.parseNull((String)hmaddress.get("TBL"));
			String strId = GmCommonClass.parseNull((String)hmaddress.get("ID"));
			String strLatitude = GmCommonClass.parseNull((String)hmaddress.get("LATITUDE")); 
			String strLongitude = GmCommonClass.parseNull((String)hmaddress.get("LONGITUDE"));
			
			gmDBManager.setString(1, strTbl);
			gmDBManager.setString(2, strId); 
			gmDBManager.setString(3, strLatitude);
			gmDBManager.setString(4, strLongitude);
			gmDBManager.setInt(5, 30301);
			
			gmDBManager.execute();
		}
    	long endtime = System.currentTimeMillis();
		long tottime = endtime - starttime;
		gmDBManager.commit();
	}
	
	private void updateDistLatLon(ArrayList alResult,String strbill)throws AppError{
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_gmap.gm_sav_dist_latlon_info", 7);
		long starttime = System.currentTimeMillis();
		for(int i=0;i<alResult.size();i++)
		{
			HashMap hmaddress = GmCommonClass.parseNullHashMap((HashMap)alResult.get(i));
			String strTbl = GmCommonClass.parseNull((String)hmaddress.get("TBL"));
			String strId = GmCommonClass.parseNull((String)hmaddress.get("ID"));
			String strLatitude = GmCommonClass.parseNull((String)hmaddress.get("LATITUDE")); 
			String strLongitude = GmCommonClass.parseNull((String)hmaddress.get("LONGITUDE"));
			
			gmDBManager.setString(1, strTbl);
			gmDBManager.setString(2, strId);
			if (strbill.equals("BILL"))
			{
				gmDBManager.setString(3, "");
				gmDBManager.setString(4, "");	
				gmDBManager.setString(5, strLatitude);
				gmDBManager.setString(6, strLongitude);
			}
			else
			{
				gmDBManager.setString(3, strLatitude);
				gmDBManager.setString(4, strLongitude);
				gmDBManager.setString(5, "");
				gmDBManager.setString(6, "");
			}
			gmDBManager.setInt(7, 30301);
			
			gmDBManager.execute();
		}
    	long endtime = System.currentTimeMillis();
		long tottime = endtime - starttime;
		gmDBManager.commit();
	}

	private void updateAccLatLon(ArrayList alResult,String strbill)throws AppError{
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_sm_gmap.gm_sav_acc_latlon_info", 7);
		long starttime = System.currentTimeMillis();
		for(int i=0;i<alResult.size();i++)
		{
			HashMap hmaddress = GmCommonClass.parseNullHashMap((HashMap)alResult.get(i));
			String strTbl = GmCommonClass.parseNull((String)hmaddress.get("TBL"));
			String strId = GmCommonClass.parseNull((String)hmaddress.get("ID"));
			String strLatitude = GmCommonClass.parseNull((String)hmaddress.get("LATITUDE")); 
			String strLongitude = GmCommonClass.parseNull((String)hmaddress.get("LONGITUDE"));
	
			gmDBManager.setString(1, strTbl);
			gmDBManager.setString(2, strId); 
			
			if (strbill.equals("BILL"))
			{
				gmDBManager.setString(3, "");
				gmDBManager.setString(4, "");	
				gmDBManager.setString(5, strLatitude);
				gmDBManager.setString(6, strLongitude);
			}
			else
			{
				gmDBManager.setString(3, strLatitude);
				gmDBManager.setString(4, strLongitude);
				gmDBManager.setString(5, "");
				gmDBManager.setString(6, "");
			}
			gmDBManager.setInt(7, 30301);
			
			gmDBManager.execute();
			
			}
	    	long endtime = System.currentTimeMillis();
			long tottime = endtime - starttime;
			gmDBManager.commit();
	}
	
	public static void sendEmail(String templateId, String strMessage) throws Exception
	{
		String strTo = GmCommonClass.getEmailProperty(templateId + "." + GmEmailProperties.TO);
		String strCc = GmCommonClass.getEmailProperty(templateId + "." + GmEmailProperties.CC);
		String strBcc = GmCommonClass.getEmailProperty(templateId + "." + GmEmailProperties.BCC);
		String strFrom = GmCommonClass.getEmailProperty(templateId + "." + GmEmailProperties.FROM);
		String strSubject = GmCommonClass.getEmailProperty(templateId + "." + GmEmailProperties.SUBJECT);
		String strMimeType = GmCommonClass.getEmailProperty(templateId + "." + GmEmailProperties.MIME_TYPE);
		String strMessageFooter = GmCommonClass.getEmailProperty(templateId + "." + GmEmailProperties.MESSAGE_FOOTER);
		
		
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(strFrom);
		emailProps.setRecipients(strTo);
		emailProps.setCc(strCc);
		emailProps.setBcc(strBcc);
		emailProps.setSubject(strSubject);
		emailProps.setMimeType(strMimeType);
		emailProps.setMessage(strMessage);
		emailProps.setMessageFooter(strMessageFooter);

		GmCommonClass.sendMail(emailProps);

	}
}
