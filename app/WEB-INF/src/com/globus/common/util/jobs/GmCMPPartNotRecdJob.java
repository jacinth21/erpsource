/**
 * FileName    : GmCMPPartNotRecdJob.java 
 * Description : Auto Notification, when a part has not been received by Globus 
 * Author      : arajan
 * Copyright   : Globus Medical Inc
 */
 
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

/**
 * @author arajan
 *
 */
public class GmCMPPartNotRecdJob extends GmActionJob {

	
	public void execute(JobDataMap jobDataMap) throws Exception {
		GmDBManager gmDBManager = new GmDBManager();
		GmEmailProperties emailProps = new GmEmailProperties();
        GmJasperMail jasperMail = new GmJasperMail();
        GmCommonBean gmCommonBean = new GmCommonBean();
        
        String strCompNm 	= "";
        String strCompRecDt = "";
        String strPartNum 	= "";
        String strLotNum 	= "";
        String strEventDesc = "";
        String strCompEmail = "";
        String strLogType 	= "";
        String strRefId		= "";
		ArrayList alResult = new ArrayList();
		ArrayList alList = new ArrayList();
		HashMap hmResult = new HashMap();
		HashMap hmAdditionalParams = new HashMap();
		HashMap hmReturnDetails = new HashMap();
		
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_process.gm_fch_partnotreced_info", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();

		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		emailProps.setSender(GmCommonClass.getEmailProperty("GmCMPPartNotRecd.From"));
        emailProps.setCc(GmCommonClass.getEmailProperty("GmCMPPartNotRecd.Cc"));
        emailProps.setMimeType(GmCommonClass.getEmailProperty("GmCMPPartNotRecd.MimeType"));
        emailProps.setReplyTo(GmCommonClass.getEmailProperty("GmCMPPartNotRecd.ReplyTo"));
        String strSubject = GmCommonClass.getEmailProperty("GmCMPPartNotRecd.Subject");
        String strLog = GmCommonClass.getEmailProperty("GmCMPPartNotRecd.log");
        
        
		for (Iterator iter = alResult.iterator(); iter.hasNext();) {

			try{
				hmResult = (HashMap) iter.next();
				String strEmailSubject = strSubject;
				strEmailSubject 		= GmCommonClass.replaceAll(strEmailSubject, "#<RAID>", ((hmResult.get("RAID")).equals("")?"N/A":(String)hmResult.get("RAID")));
				strCompNm  		= GmCommonClass.parseNull((String)hmResult.get("COMPNAME"));
				strCompRecDt  	= GmCommonClass.parseNull((String)hmResult.get("COMPRECDT"));
				strPartNum  	= GmCommonClass.parseNull((String)hmResult.get("PARTNUM"));
				strLotNum  		= GmCommonClass.parseNull((String)hmResult.get("LOTNUM"));
				strEventDesc  	= GmCommonClass.parseNull((String)hmResult.get("EVENTDESC"));
				strCompEmail  	= GmCommonClass.parseNull((String)hmResult.get("COMPEMAILID"));
				strLogType  	= GmCommonClass.parseNull((String)hmResult.get("REFTYPE"));
				strRefId  		= GmCommonClass.parseNull((String)hmResult.get("INCIDENTID"));
				if(strEventDesc.equals("")){
					strEventDesc = "N/A";
				}
				String strJasperName = "/GmCMPPartNotRecd.jasper"; 
				
				hmAdditionalParams.put("COMPNAME", strCompNm);
		        hmAdditionalParams.put("COMPRECDT", strCompRecDt);
		        hmAdditionalParams.put("PARTNUM", strPartNum);
		        hmAdditionalParams.put("LOTNUM", strLotNum);
		        hmAdditionalParams.put("EVENTDESC", strEventDesc);
				
		        emailProps.setRecipients(strCompEmail);		        
		        emailProps.setSubject(strEmailSubject);
		        jasperMail.setJasperReportName(strJasperName);
		        jasperMail.setAdditionalParams(hmAdditionalParams);
		        jasperMail.setReportData(null);
		        jasperMail.setEmailProperties(emailProps);
		        hmReturnDetails = jasperMail.sendMail();  
		        
		        gmCommonBean.saveLog(strRefId, strLog, "30301", strLogType); // To save log
			}catch (Exception e) {

				log.error("Exception in job body: " + e.getMessage());
				hmReturnDetails.put(GmJob.EXCEPTION, e);
			}
			Exception ex = (Exception)hmReturnDetails.get(GmJob.EXCEPTION);
			if(ex != null)
			{
				log.error("Exception in sendJasperMail");
				setJobStatus(GmJob.JOB_STATUS_FAIL);
				setJobException(GmCommonClass.getExceptionStackTrace(ex,"<br>"));
				setAdditionalParam((String)hmReturnDetails.get("ORIGINALMAIL"));
				sendJobExceptionEmail();
				throw ex;
			}
			
		}
		

	}

}
