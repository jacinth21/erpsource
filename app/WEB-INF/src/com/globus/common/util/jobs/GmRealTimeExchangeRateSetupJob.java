package com.globus.common.util.jobs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonCurrConvBean;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.GmGetOANDAQuotesVo;
import com.globus.valueobject.common.GmGetOANDAResponseVo;

public class GmRealTimeExchangeRateSetupJob extends GmActionJob {
  // Instantiating the Logger
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.


  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);

    GmCommonCurrConvBean gmCommonCurrConvBean = new GmCommonCurrConvBean(gmDataStoreVO);

    HashMap hmParam = new HashMap();
    HashMap hmCurrenyDtls = new HashMap();

    hmParam = GmCommonClass.parseNullHashMap((HashMap) jobDataMap.get("HMPARAM"));

    Date dtCurrentDate = null;
    Date dtMonthLastDt = null;
    int MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
    // Get the date difference between 1st of month to the current date
    GmCalenderOperations.setTimeZone(gmDataStoreVO.getCmptzone());
    dtCurrentDate = GmCommonClass.getCurrentDate(gmDataStoreVO.getCmpdfmt());
    int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
    String strMonthLastDay =
        GmCalenderOperations.getLastDayOfMonth(currMonth, gmDataStoreVO.getCmpdfmt());
    dtMonthLastDt = GmCommonClass.getStringToDate(strMonthLastDay, gmDataStoreVO.getCmpdfmt());
    long timeDiff = dtMonthLastDt.getTime() - dtCurrentDate.getTime();
    long daysDiff = timeDiff / (MILLIS_IN_DAY);

    hmCurrenyDtls =
        GmCommonClass.parseNullHashMap(GmCommonClass.getCurrSymbolFormatfrmComp(gmDataStoreVO
            .getCmpid()));

    String strBaseCurrID = GmCommonClass.parseNull((String) hmCurrenyDtls.get("TXNCURRENCYID"));
    String strBaseCurrSymb = GmCommonClass.parseNull(GmCommonClass.getCodeAltName(strBaseCurrID));
    String strQuoteCurrID = GmCommonClass.parseNull((String) hmParam.get("QUOTE_CURR"));
    String strQuoteCurrSymb = GmCommonClass.parseNull(GmCommonClass.getCodeAltName(strQuoteCurrID));

    String strServiceLink =
        GmCommonClass.parseNull(gmCommonCurrConvBean.getOandaApiConfig("OANDA_API_URL"));
    String strAPIKey =
        GmCommonClass.parseNull(gmCommonCurrConvBean.getOandaApiConfig("OANDA_API_KEY"));
    // Taking yesterday date to get the close bid
    String strPrevDate = gmCommonCurrConvBean.getCurrentDateDiff(1,"yyyy-MM-dd");
    
    hmParam.put("PREV_DATE", strPrevDate);
    hmParam.put("BASE_CURR_SYM", strBaseCurrSymb);
    hmParam.put("QUOTE_CURR_SYM", strQuoteCurrSymb);
    hmParam.put("BASE_CURR", strBaseCurrID);
    hmParam.put("USERID", "30301");
    hmParam.put("SERVICE_URL", strServiceLink);
    hmParam.put("API_KEY", strAPIKey);
    hmParam.put("gmDataStoreVO", gmDataStoreVO);


    String strRuleValue =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("ER_DT_DIFF", "ER_SALES_AVG"));
    int ruleVale = Integer.parseInt(strRuleValue);

    /*
     * If the days difference is less than or equal to 2 (ruleValue) i.e last 3 days of the current
     * month ,then we need to calculate the exchange rate as average from sales (sum(Sales in
     * USD)/sum(Sales in Local)), else call the OANDA webservise API to get the exchange rate
     */
    if (daysDiff <= ruleVale) {

      hmParam.put("SOURCE", "106910"); // 106910- SpineIT Average

    } else {

      hmParam = GmCommonClass.parseNullHashMap(getExchangeRate(hmParam));
    }
    // To save the real time exchange rate value for the currency pair
    gmCommonCurrConvBean.saveRealTimeCurrConvDtls(hmParam);

  }

  /**
   * getExchangeRate - This method used get the exchange rate by calling OANDA API
   * 
   * @param
   * @return HashMap
   * @throws AppError
   * @throws IOException
   */
  private HashMap getExchangeRate(HashMap hmERDtls) throws AppError, IOException {
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmERDtls.get("gmDataStoreVO");

    GmGetOANDAResponseVo gmGetOANDAResponseVo = new GmGetOANDAResponseVo();
    GmCommonCurrConvBean gmCommonCurrConvBean = new GmCommonCurrConvBean(gmDataStoreVO);
    HttpURLConnection connection = getURLConnectionDetails(hmERDtls);

    int intResponseCode = connection.getResponseCode();
    log.debug(" intResponseCode " + intResponseCode);

    // IF the connection to OANDA API is failed then set the failure flag to get the previous day
    // exchange rate , else get the close bid rate from the OANDA API response
    if (connection.getErrorStream() != null) {
      BufferedReader bufferError =
          new BufferedReader(new InputStreamReader((connection.getErrorStream())));
      String strError = "";
      StringBuffer sbQuery1 = new StringBuffer();
      String strErrorDtls;
      while ((strErrorDtls = bufferError.readLine()) != null) {
        sbQuery1.append(strErrorDtls);
      }
      connection.disconnect();
      strError = sbQuery1.toString();
      log.debug(" strError " + strError);

      hmERDtls.put("SOURCE", "106911"); // 106911 - OANDA
      hmERDtls.put("FAILURE_FL", "Y");
      hmERDtls.put("FAILURE_MSG", strError);
      
      gmCommonCurrConvBean.sendRealTimeERFailureNotifyMail(hmERDtls);

    } else {
      BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
      String output;
      StringBuffer sbQuery = new StringBuffer();
      while ((output = br.readLine()) != null) {
        sbQuery.append(output);

      }
      connection.disconnect();
      log.error("JSON - Response String: " + sbQuery.toString());
      String jsonString = sbQuery.toString();

      ObjectMapper mapper = new ObjectMapper();

      try {

        gmGetOANDAResponseVo = mapper.readValue(jsonString, GmGetOANDAResponseVo.class);
        GmGetOANDAQuotesVo[] quotes = gmGetOANDAResponseVo.getQuotes();
        GmGetOANDAQuotesVo gmGetOANDAQuotesVo = quotes[0];

        log.debug(":Base Currency:" + gmGetOANDAQuotesVo.getBase_currency() + ": Quote Currency:"
            + gmGetOANDAQuotesVo.getQuote_currency() + " :: close bid=="
            + gmGetOANDAQuotesVo.getAverage_bid());

        hmERDtls.put("AVERAGE_BID_RATE", gmGetOANDAQuotesVo.getAverage_bid());
        hmERDtls.put("SOURCE", "106911"); // 106911 - OANDA

      } catch (Exception e) {
        log.debug("exception:" + e);
        hmERDtls.put("FAILURE_MSG", e.toString());
        gmCommonCurrConvBean.sendRealTimeERFailureNotifyMail(hmERDtls);
      }
    }



    return hmERDtls;
  }

  /**
   * getURLConnectionDetails - This method used setting the URL connection details.
   * 
   * @param strURL - Target URL passing as the string
   * @throws AppError
   * @throws IOException
   */
  private HttpURLConnection getURLConnectionDetails(HashMap hmParam) throws AppError, IOException {

    String strBaseCurr = GmCommonClass.parseNull((String) hmParam.get("BASE_CURR_SYM"));
    String strQuoteCurr = GmCommonClass.parseNull((String) hmParam.get("QUOTE_CURR_SYM"));
    String strServiceLink = GmCommonClass.parseNull((String) hmParam.get("SERVICE_URL"));
    String strAPIKey = GmCommonClass.parseNull((String) hmParam.get("API_KEY"));
    String strDateTime = GmCommonClass.parseNull((String) hmParam.get("DATE_TIME"));
    String strPrevDate = GmCommonClass.parseNull((String) hmParam.get("PREV_DATE"));

    HttpURLConnection connection = null;
    // to getting the properties information
    String strServiceURL =
        strServiceLink + "api_key=" + strAPIKey + "&date_time=" + strPrevDate + strDateTime
            + "&base=" + strBaseCurr + "&quote=" + strQuoteCurr + "&fields=averages";

    log.debug("strServiceURL==" + strServiceURL);

    URL url;
    ObjectOutputStream out;
    url = new URL(strServiceURL); // Creating the URL.
    connection = (HttpURLConnection) url.openConnection();
    connection.setRequestProperty("Accept", "application/json");
    connection.setUseCaches(false);
    return connection;
  }

}
