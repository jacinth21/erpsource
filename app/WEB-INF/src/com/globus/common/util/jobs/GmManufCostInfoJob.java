package com.globus.common.util.jobs;

import java.util.ArrayList;
import java.util.HashMap;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;

import com.globus.manufact.beans.GmManfBean;

public class GmManufCostInfoJob extends GmActionJob {
	public static final String TEMPLATE_NAME = "GmManufCostInfoJob";
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	public void execute(JobDataMap jobDataMap) throws Exception {
		// TODO Auto-generated method stub
		log.debug("Start the GmManufCostInfoJob");
		GmManfBean gmManfBean = new GmManfBean ();
		GmEmailProperties emailProps = new GmEmailProperties();
		GmJasperMail jasperMail = new GmJasperMail();
		
		ArrayList alResult = new ArrayList();
		HashMap hmParam = new HashMap();
		HashMap hmReturnDetails = new HashMap();
		
		hmParam.put("STRSHOWZEROCOST", "on");
		alResult =  (ArrayList) gmManfBean.fetchCostingRpt(hmParam).getRows();
		log.debug(" Array list size --> "+alResult.size());
		if(alResult.size() > 0)
		{
			String strJasperName = "/GmManufCostInfo.jasper";
			String strSubject = "";
			String strTo = "";
			String strCc = "";
			
			strSubject = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.SUBJECT);
			strTo = GmCommonClass.getRuleValue("MANUF_COST", "EMAIL");
			strCc = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.CC);
			
			emailProps.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
			emailProps.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM));
			emailProps.setCc(strCc);
			emailProps.setRecipients(strTo);
			emailProps.setSubject(strSubject);
			
			jasperMail.setJasperReportName(strJasperName);
			jasperMail.setAdditionalParams(null);
			jasperMail.setReportData((ArrayList) alResult);
			jasperMail.setEmailProperties(emailProps);
			hmReturnDetails = jasperMail.sendMail();
		}
		log.debug(" Job End .... ");
	}
}
