/**
 * FileName : GmReceiveRequestsEmailJob.java Description : Author : sthadeshwar Date : Mar 24, 2009
 * Copyright : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author sthadeshwar
 * 
 */
public class GmCancelRequestEmailJob extends GmLoanerRequestsEmailJob {

  public static final String TEMPLATE_NAME = "GmCancelRequestEmailJob";
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    log.debug("jobDataMap = " + jobDataMap);
    ArrayList alRepId = new ArrayList();
    ArrayList alRequestDetails = new ArrayList();

    HashMap hmRepDetails = new HashMap();
    HashMap hmParams = new HashMap();
    HashMap hmReturn = new HashMap();

    String strAssrepID = "";
    String strAssRepName = "";
    String strAssRepEmail = "";
    String strRepName = "";
    String strRepEmail = "";
    // hmParams = getShippingDetails();

    /* Getting all the rep id's who have submitted requests today */
    gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_cancel_loaner_rep_ids", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alRepId = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    for (Iterator iter = alRepId.iterator(); iter.hasNext();) {
      try {
        hmParams.put("GMCSPHONE", GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSPhone"));
        hmParams.put("GMCSEMAIL", GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSEmail"));

        hmRepDetails = (HashMap) iter.next();
        String strRepId = (String) hmRepDetails.get("REP_ID");
        strRepName = (String) hmRepDetails.get("REP_NAME");
        strRepEmail = (String) hmRepDetails.get("EMAIL");
        strAssRepName = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPNM"));
        strAssrepID = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPID"));
        strAssRepEmail = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPEMAIL"));

        /*
         * The associate rep code has been added for PMT-4031. The functionality is to send an email
         * to Associate rep instead of Rep. And Sales rep should be in CC. The same code changes has
         * been added to below files as well. GmLoanerSetDueEmailJob.java (Due job),
         * GmLoanerBean.java (Transfer), GmTicketBean (missing charge)
         */

        if (!strAssrepID.equals("")) {
          strRepName = strAssRepName;
          strRepEmail = strAssRepEmail;
        }

        log.debug("Fetching requests for rep " + strRepId);
        /* Getting request details for each rep */
        gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_cancel_requests", 3);
        gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
        gmDBManager.setString(1, strRepId);
        gmDBManager.setString(2, strAssrepID);
        gmDBManager.execute();

        alRequestDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
        gmDBManager.commit();

        hmParams.putAll(hmRepDetails);

        GmEmailProperties emailProps = getEmailProperties(TEMPLATE_NAME, strRepId);
        String strSubject = emailProps.getSubject();
        strSubject = GmCommonClass.replaceAll(strSubject, "#<REP_NAME>", strRepName);
        strSubject = GmCommonClass.replaceAll(strSubject, "#<PSDATE>", df.format(new Date()));

        emailProps.setRecipients(strRepEmail);
        emailProps.setSubject(strSubject);


        GmJasperMail jasperMail = new GmJasperMail();
        jasperMail.setJasperReportName("/GmEmailReqVoid.jasper");
        jasperMail.setAdditionalParams(hmParams);
        jasperMail.setReportData(alRequestDetails);
        jasperMail.setEmailProperties(emailProps);

        hmReturn = jasperMail.sendMail();

        // HashMap hmReportData = new HashMap();
        // hmReportData.put("FILENAME", "/GmEmailReqVoid.jasper");
        // hmReportData.put("PARAMS", hmParams);
        // hmReportData.put("DETAILS", alRequestDetails);
        // hmReportData.put("EMAILPROPS", emailProps);
        //
        // hmReturn = (HashMap)GmCommonClass.sendRequest("/GmJasperMailServlet", hmReportData,
        // true);
        // log.error("Request for sending jasper mail sent ...");

        // GmJasperReport gmJasperReport = new GmJasperReport();
        // hmReturn = gmJasperReport.sendJasperMail("/GmEmailReqVoid.jasper", hmParams,
        // alRequestDetails, emailProps, null);
      } catch (Exception e) {
        log.error("Exception in job body: " + e.getMessage());
        hmReturn.put(GmJob.EXCEPTION, e);
      }
      Exception ex = (Exception) hmReturn.get(GmJob.EXCEPTION);
      if (ex != null) {
        log.error("Exception in sendJasperMail");
        setJobStatus(GmJob.JOB_STATUS_FAIL);
        setJobException(GmCommonClass.getExceptionStackTrace(ex, "<br>"));
        setAdditionalParam((String) hmReturn.get("ORIGINALMAIL"));
        sendJobExceptionEmail();
        throw ex;
      }
    }
  }



}
