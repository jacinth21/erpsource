/**
 * FileName    : GmUserChangePasswordJob.java 
 * Description :
 * Author      : HParikh
 * Date        : Jul  2010 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

 
public class GmUserChangePasswordJob extends GmActionJob {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
		log.debug("Start");
		GmDBManager gmDBManager = new GmDBManager();		
		gmDBManager.setPrepareString("gm_pkg_it_user.gm_update_password_exp_fl", 0);		
		gmDBManager.execute(); 
		gmDBManager.commit();
		log.debug("Finish");
	}
}
