/**
 * FileName    : GmRuleInactiveNotificationJob.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Mar 24, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.text.SimpleDateFormat;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author Ritesh
 *
 */
public class GmRuleInactiveNotificationJob extends GmActionJob {

	public static final String TEMPLATE_NAME = "GmRuleInactiveNotificationJob";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	SimpleDateFormat df = null;

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
	    
	        GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
	        String strCmpDtFmt = GmCommonClass.parseNull((String)gmDataStoreVO.getCmpdfmt());
	        df = new SimpleDateFormat(strCmpDtFmt);
	        
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		String strStatus = "";
		String strMsg="successfully done";
		log.debug("jobDataMap = " + jobDataMap);
		
		HashMap hmReturn = new HashMap();
 
		 
		gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_sav_rules_inactive_status",1);
		gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);
		gmDBManager.execute();
		strStatus = gmDBManager.getString(1);
		gmDBManager.commit();
		if(!strStatus.equals("1"))
		{
			strMsg = "not done";
		}
		try
		{ 
			GmEmailProperties gmEmailProperties = getEmailProperties(TEMPLATE_NAME);
			String strDate = df.format(new java.util.Date()).toString();
			String strMessage = gmEmailProperties.getMessage();
			strMessage = GmCommonClass.replaceAll(strMessage, "#<STATUS>", strMsg);
			strMessage = GmCommonClass.replaceAll(strMessage, "#<EXPDATE>", strDate);
			
			gmEmailProperties.setMessage(strMessage);
			GmCommonClass.sendMail(gmEmailProperties);
		}
		catch(Exception e)
		{
			log.error("Exception in job body: " + e.getMessage());
			hmReturn.put(GmJob.EXCEPTION, e);
		}
		Exception ex = (Exception)hmReturn.get(GmJob.EXCEPTION);
		if(ex != null)
		{
			log.error("Exception in sendInactiveRuleNotificationMail");
			setJobStatus(GmJob.JOB_STATUS_FAIL);
			setJobException(GmCommonClass.getExceptionStackTrace(ex,"<br>"));
			setAdditionalParam((String)hmReturn.get("ORIGINALMAIL"));
			sendJobExceptionEmail();
			throw ex;
		}
		
	}
	
	
	public GmEmailProperties getEmailProperties(String strTemplate)
	{
		log.debug("template = "+strTemplate);
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
		emailProps.setRecipients(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.TO));
		emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT));
		emailProps.setMessage(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MESSAGE));
		emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MIME_TYPE));
		log.debug("email Prop = "+emailProps);
		return emailProps;
	}


}
