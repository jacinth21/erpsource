package com.globus.common.util.jobs;

import org.quartz.JobDataMap;

import com.globus.common.db.GmDBManager;

public class GmAutoReconcilationJob extends GmActionJob {
	@Override
	public void execute(JobDataMap jobDataMap) throws Exception {

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_op_process_recon.gm_main_flow", 0);
		gmDBManager.execute();
		gmDBManager.commit();
	}
}
