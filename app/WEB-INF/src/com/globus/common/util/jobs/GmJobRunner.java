package com.globus.common.util.jobs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmJobRunner extends GmJob {

  Logger log = GmLogger.getInstance(this.getClass().getName());
  SimpleDateFormat dbDateFormatter = new SimpleDateFormat("MM/dd/yyyy");

  @Override
  public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

    HashMap hmParams = new HashMap();
    ArrayList alParams = new ArrayList();

    String strJobName = "";
    String strJobClassName = "";
    String jobStartTime = "";
    String jobEndTime = "";
    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    try {
      // Getting the job details from XML
      JobDetail jobDetail = jobExecutionContext.getJobDetail();
      strJobName = jobDetail.getName();
      JobDataMap jobDataMap = jobDetail.getJobDataMap();
      strJobClassName = (String) jobDataMap.get(strJobName);

      setJobName(strJobClassName);
      jobDataMap.put("JOBNAME", strJobClassName);

      Class strClass = Class.forName(strJobClassName);

      // Instantiating the actionjob class
      GmActionJob gmActionJob = (GmActionJob) strClass.newInstance();
      gmDataStoreVO = gmActionJob.getGmDataStoreVO(jobDataMap);
      dateFormatter = new SimpleDateFormat(gmDataStoreVO.getCmpdfmt() + " hh:mm:ss");
      gmActionJob.setJobName(strJobClassName);
      jobStartTime = dateFormatter.format(new Date());
      setJobStartTime(jobStartTime);
      jobDataMap.put("STARTTIME", jobStartTime);
      gmActionJob.setJobStartTime(jobStartTime);

      hmParams.put(JOBNAME, strJobName);
      hmParams.put(STARTTIME, dateFormatter.format(new Date()));

      log.debug("Executing job " + strJobClassName);

      gmActionJob.execute(jobDataMap); // *** Running the job

      log.debug("Completed job " + strJobName);

      jobEndTime = dateFormatter.format(new Date());
      setJobEndTime(jobEndTime);
      jobDataMap.put("ENDTIME", getJobEndTime());
      gmActionJob.setJobEndTime(jobEndTime);

      hmParams.put(ENDTIME, dateFormatter.format(new Date()));
      hmParams.put(STATUS, JOB_STATUS_SUCCESS);
      hmParams.put("STATUSDETAILS", JOB_STATUS_SUCCESS);
      hmParams.put("LOGNAME", strJobClassName);
      hmParams.put("STATUSFL", "S"); // For saveLoadStatus procedure
      hmParams.put("COMPANY_ID", gmDataStoreVO.getCmpid());
      alParams.add(hmParams);
    } catch (Exception e) {
      e.printStackTrace();
      hmParams.put("STATUSFL", "F");
      hmParams.put(STATUS, JOB_STATUS_FAIL);
      hmParams.put("STATUSDETAILS", GmCommonClass.getExceptionStackTrace(e, "<br>"));
      hmParams.put("COMPANY_ID", gmDataStoreVO.getCmpid());
      alParams = new ArrayList();
      alParams.add(hmParams);

    }
    try {
      sendJobStatusEmail(alParams, hmParams);
      GmCommonClass.saveLoadStatus(hmParams, gmDataStoreVO);
    } catch (Exception ex) {
      log.error(GmCommonClass.getExceptionStackTrace(ex, "<br>"));
    }

  }

}
