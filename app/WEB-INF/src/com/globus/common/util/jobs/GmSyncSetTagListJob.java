package com.globus.common.util.jobs;

import java.util.ArrayList;
import java.util.HashMap;

import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.common.bean.GmMasterRedisReportBean;
import com.globus.webservice.common.bean.GmMasterRedisTransBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingReportBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingWrapperBean;

public class GmSyncSetTagListJob extends GmActionJob {
	 /*
	   * (non-Javadoc)
	   * 
	   * @see com.globus.common.util.jobs.GmActionJob#execute(org.quartz.JobDataMap)
	   */
	  @Override

/**
 * This method is used to update the set and tag details to cache based on distributor (PMT-24650)
 * @exception AppError
 */ 	  
	  public void execute(JobDataMap jobDataMap) throws AppError {
		  log.debug("GmSyncSetTagListJob=>");
	    // to get the data store information from jobDataMap object
	    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
		GmMasterRedisTransBean gmMasterRedisTransBean = new GmMasterRedisTransBean(gmDataStoreVO);
		GmKitMappingWrapperBean gmKitMappingWrapperBean = new GmKitMappingWrapperBean(gmDataStoreVO);
		GmMasterRedisReportBean gmMasterRedisReportBean = new GmMasterRedisReportBean(gmDataStoreVO);
		GmKitMappingReportBean gmKitMappingReportBean = new GmKitMappingReportBean(gmDataStoreVO);
		ArrayList alListSet = new ArrayList();
		ArrayList alListTag = new ArrayList();
		String strCompanyInfo = "";
		String strSetId = "";
		String strOldNm = "";
		String strNewNm = "";
		String strPartyId = "";
		String strDistId = "";
		String strTagId = "";
		alListSet = gmMasterRedisTransBean.fetchSyncSetDetails(gmDataStoreVO.getCmpid());
		strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
		log.debug("GmSyncSetTagListJob strCompanyInfo----->>"+strCompanyInfo);
		log.debug("GmSyncSetTagListJobalList===>>>"+alListSet.size());
			if(alListSet.size()>0){
				for(int i=0;i<alListSet.size()-1;i++){
					HashMap hmLoop = (HashMap)alListSet.get(i);	
					strDistId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
					strSetId = GmCommonClass.parseNull((String)hmLoop.get("ID"));
					strNewNm = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
					strTagId = GmCommonClass.parseNull((String)hmLoop.get("TID"));
					if(!(strSetId.equals("") || strSetId.equals("null"))){
					gmKitMappingWrapperBean.syncSetTagList(strSetId, strSetId, strNewNm, "syncSetTagList", strCompanyInfo,strDistId);
					}
					
				}
			}
			alListTag = gmMasterRedisTransBean.fetchSyncTagDetails(strDistId);
			log.debug("GmSyncSetTagListJobalList===>>>"+alListTag.size());
				if(alListTag.size()>0){
					for(int i=0;i<alListTag.size()-1;i++){
						HashMap hmLoop = (HashMap)alListTag.get(i);	
						strDistId = GmCommonClass.parseNull((String)hmLoop.get("DID"));
						strTagId = GmCommonClass.parseNull((String)hmLoop.get("TID"));
						strSetId = GmCommonClass.parseNull((String)hmLoop.get("ID"));
						strNewNm = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
						if(!(strTagId.equals("") || strTagId.equals("null"))){
						gmKitMappingWrapperBean.syncTagList(strSetId, strSetId, strNewNm, "syncTagList", strCompanyInfo,gmDataStoreVO.getCmpid());
						}
					}
				}
	  }

}
