/**
 * FileName    : GmMoveAllocReqToShippingJob.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Mar 25, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import oracle.jdbc.driver.OracleTypes;

import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;

/**
 * @author sthadeshwar
 *
 */
public class GmMoveAllocReqToShippingJob extends GmActionJob {

	public void execute(JobDataMap jobDataMap) throws AppError {

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_move_allocated_requests", 0);
		gmDBManager.execute();
		gmDBManager.commit();
	}

}
