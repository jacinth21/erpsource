package com.globus.common.util.jobs;

import org.quartz.JobDataMap;

import com.globus.common.db.GmDBManager;

public class GmEditCheckJob extends GmActionJob {

	public void execute(JobDataMap jobDataMap) throws Exception {		
		GmDBManager gmDBManager = new GmDBManager();		
		gmDBManager.setPrepareString("gm_pkg_cr_edit_check.gm_sav_edit_chk_job", 0);
		gmDBManager.execute();
		gmDBManager.commit();
	}
}
