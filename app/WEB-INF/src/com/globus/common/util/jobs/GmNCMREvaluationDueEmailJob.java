/**
 * FileName    : GmNCMREvaluationDueEmailJob.java 
 * Description :
 * Author      : Himanshu
 * Date        : Mar 5, 2010 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
 
import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties; 
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * @author Himanshu
 *
 */
public class GmNCMREvaluationDueEmailJob extends GmActionJob {

	public static final String TEMPLATE_NAME = "GmNCMREvaluationDueEmailJob";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
		GmDBManager gmDBManager = new GmDBManager();
		log.debug("jobDataMap = " + jobDataMap);
		ArrayList  alEvalDtls = new ArrayList(); 
		
		HashMap hmEvalDetails = new HashMap();
		HashMap hmEvalDetailsNew = new HashMap();
		
		HashMap hmReturn = new HashMap();
		String strEmail="";
		String strEmailnxt="";
 
		 
		gmDBManager.setPrepareString("GM_PKG_OP_NCMR.gm_fch_eval_rem_email_det", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();  
		alEvalDtls = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1));
		gmDBManager.close();
		ArrayList alTemp = new ArrayList(alEvalDtls);
		
		for(int j=0;j<alEvalDtls.size();j++){
			ArrayList  alEvalPrint = new ArrayList();
			hmEvalDetails=(HashMap)alEvalDtls.get(j);
			strEmail = GmCommonClass.parseNull((String)hmEvalDetails.get("TO_EMAIL"));
			GmEmailProperties gmEmailProperties = getEmailProperties(TEMPLATE_NAME);
			gmEmailProperties.setRecipients(strEmail);
			GmJasperMail jasperMail = new GmJasperMail();
			jasperMail.setJasperReportName("/GmInHouseNCMREvalRemJob.jasper");
			
			for(int k=j;k<alTemp.size();k++){
				hmEvalDetailsNew=(HashMap)alTemp.get(k);
				HashMap hmParams = new HashMap();
				strEmailnxt = GmCommonClass.parseNull((String)hmEvalDetailsNew.get("TO_EMAIL"));
				if(strEmail.equals(strEmailnxt)){
					hmParams.put("EVALID",GmCommonClass.parseNull((String)hmEvalDetailsNew.get("EVALID")));
					hmParams.put("PNUM",GmCommonClass.parseNull((String)hmEvalDetailsNew.get("PNUM")));
					hmParams.put("PDESC",GmCommonClass.parseNull((String)hmEvalDetailsNew.get("PDESC")));
					hmParams.put("VNAME",GmCommonClass.parseNull((String)hmEvalDetailsNew.get("VNAME")));
					hmParams.put("DHRID",GmCommonClass.parseNull((String)hmEvalDetailsNew.get("DHRID")));
					hmParams.put("DQTY",GmCommonClass.parseNull((String)hmEvalDetailsNew.get("DQTY")));
					hmParams.put("NQTY",GmCommonClass.parseNull((String)hmEvalDetailsNew.get("NQTY")));
					hmParams.put("NRES",GmCommonClass.parseNull((String)hmEvalDetailsNew.get("NRES")));
					alEvalPrint.add(GmCommonClass.parseNullHashMap(hmParams));
					j=k;
					
				}
			}
			jasperMail.setReportData(GmCommonClass.parseNullArrayList(alEvalPrint));
			
			jasperMail.setEmailProperties(gmEmailProperties);
			
			hmReturn = jasperMail.sendMail();
		}
		
	}
	
	
	public GmEmailProperties getEmailProperties(String strTemplate)
	{
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
		emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT));
		emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MIME_TYPE));
		return emailProps;
	}


}
