/**
 * FileName    : GmLotExpiryReminderInternal.java 
 * Description :
 * Author      : sindhu
 * Date        : MAY 6, 2020 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * GmLotExpiryEmailJobInternal- expiry lot details email nodification
 *
 */
public class GmLotExpiryReminderInternal extends GmActionJob {

	public static final String TEMPLATE_NAME = "GmLotExpiryReminderInternal";
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
	    	GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		log.debug("jobDataMap = " + jobDataMap);

		HashMap hmRuleDetails = new HashMap();
		HashMap hmParams = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmEmailProperty = new HashMap();
		
		
		HashMap hmParam = GmCommonClass.parseNullHashMap((HashMap) jobDataMap.get("HMPARAM"));
		String strExpiryLot = GmCommonClass.parseNull((String) hmParam.get("LOTEXPIRY")); 
	
	    ArrayList alExpiredLot = new ArrayList();
	    gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_expired_lot_mail_internal",2);
	    gmDBManager.setString(1, strExpiryLot);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alExpiredLot = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		int intsize = alExpiredLot.size();
		
		if (intsize > 0){
			hmParam.put("ALRETURN", alExpiredLot);
			hmParam.put("CMPID", GmCommonClass.parseNull(gmDataStoreVO.getCmpid()));
			hmEmailProperty = populateEmailData(hmParam);
			try {
				GmCommonClass.sendMail(hmEmailProperty);
			} catch (Exception ex) {
				log.error("Exception in sending" + ex.getMessage());
			} 
		}

	}
	
	public HashMap populateEmailData(HashMap hmParam) throws AppError {
		
		GmWSUtil gmWSUtil = new GmWSUtil();
		String strSubject = "";
		String strMessage = "";
		String strTemplatePath = System.getProperty("ENV_TYPE");
		
		String strCompanyLocale = GmCommonClass.getCompanyLocale(GmCommonClass.parseNull((String) hmParam.get("CMPID")));
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Email."+strTemplatePath,
						strCompanyLocale);
		
		String strExpiryLot = GmCommonClass.parseNull((String) hmParam.get("LOTEXPIRY"));      //get expiry type here
		/*String strFrom = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM);
		String strMimeType = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE);
		strSubject = GmCommonClass.getEmailProperty(TEMPLATE_NAME + strExpiryLot + "." + GmEmailProperties.SUBJECT);
		strMessage = GmCommonClass.getEmailProperty(TEMPLATE_NAME + strExpiryLot + "." + GmEmailProperties.MESSAGE);
		*/
		String strMimeType = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
		String strFrom = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM));
		strSubject = gmResourceBundleBean.getProperty(TEMPLATE_NAME + strExpiryLot + "." + GmEmailProperties.SUBJECT);
		strMessage = gmResourceBundleBean.getProperty(TEMPLATE_NAME + strExpiryLot + "." +  GmEmailProperties.MESSAGE);
		String strToCc = GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOT_EXPIRY_CC", "LOT_EXPIRY"));
		String strTo   = GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOT_EXPIRY_TO", "LOT_EXPIRY"));
			
		HashMap hmEmailProperty;
		hmParam.put("STRMESSAGE", strMessage);
		
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setMimeType(strMimeType);
		emailProps.setSender(strFrom);
		emailProps.setRecipients(strTo);
		emailProps.setCc(strToCc);
		emailProps.setSubject(strSubject);
		emailProps.setMessage(strMessage);
		hmEmailProperty = generateOutput(emailProps, hmParam);
		return hmEmailProperty;

	}
	
	/**
	 * generateOutPut This method is used to generate data which need to show
	 * email content.
	 * 
	 * @param hmParam
	 *            HashMap,GmEmailProperties emailProps
	 * @exception AppError
	 */
	public HashMap generateOutput(GmEmailProperties emailProps, HashMap hmParam)
			throws AppError {
		String strTemplateName = GmCommonClass.parseNull(GmCommonClass.getRuleValue("TEMPLATE", "LOT_EXPIRY"));
		log.debug("generateOutput-->>> "+strTemplateName );
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setTemplateSubDir("operations/templates");
		templateUtil.setTemplateName(strTemplateName);
		templateUtil.setDataMap("hmEmailParam", hmParam);
		String strEmailContent = templateUtil.generateOutput();

		HashMap hmEmailProperty = new HashMap();
		hmEmailProperty.put("strFrom", emailProps.getSender());
		hmEmailProperty.put("strTo", emailProps.getRecipients());
		hmEmailProperty.put("strCc", emailProps.getCc());
		hmEmailProperty.put("strSubject", emailProps.getSubject());
		hmEmailProperty.put("strMessage", emailProps.getMessage());
		hmEmailProperty.put("strMessageDesc", strEmailContent);
		hmEmailProperty.put("strMimeType", emailProps.getMimeType());

		return hmEmailProperty;
	}
}
