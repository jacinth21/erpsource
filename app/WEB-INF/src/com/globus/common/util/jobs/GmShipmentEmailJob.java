package com.globus.common.util.jobs;

import java.io.File;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRepEmailProperties;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmShipmentEmailJob extends GmActionJob {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
  /**
   * execute - This method will be used to send mail based on Job
   * 
   * @param JobExecutionContext
   * @exception JobExecutionException
   */
  String strFormedEmail = "";
  int shipmentRepCnt = Integer.parseInt(GmCommonClass.getString("GMSHIPMENTREPCNT"));
  SimpleDateFormat strFormatter1 = null;
  SimpleDateFormat strFormatter2 = null;
  SimpleDateFormat strTransDtFmt = null;

  @Override
  public void execute(JobDataMap jobDataMap) throws AppError {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmShipmentEmailJob gmShipmentEmailJob = new GmShipmentEmailJob();
    strFormatter1 = new SimpleDateFormat(gmDataStoreVO.getCmpdfmt() + " hh:mm:ss");
    strFormatter2 = new SimpleDateFormat(gmDataStoreVO.getCmpdfmt());

    String strTrackVisitedFl = "";
    String strJobEndTime = "";
    String strShipToNm = "";
    String strJobStatusFl = "SUCCESS";
    String strShipCCEmail = "";
    String strJob = GmJob.getClassName(this.getClass().getName());
    StringBuffer strHtmlString = new StringBuffer();

    HashMap hmShippedData = new HashMap();
    HashMap hmShippedRowData = new HashMap();
    HashMap hmSourceMap = new HashMap();
    HashMap hmStaticVal = new HashMap();

    ArrayList alShippedData = new ArrayList();
    ArrayList alExceptionList = new ArrayList();
    ArrayList alRuleList = new ArrayList();

    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean(gmDataStoreVO);
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(gmDataStoreVO);
    String strCompanyLocale =
        GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid()));
    GmResourceBundleBean rsCompanyBundle =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    String strRepCCEmail = "";
    String strShipToEmail = "";
    String strSource = "";
    String strShipToId = "";
    String strShipToType = "";
    String strTransdateFmt = "";
    
   

    String strVisitedFl = "";
    String strShiptoVisitedFl = "";
    String strExcpTime = "";
    String strFedxFl = "";
    ArrayList alSourceList = new ArrayList();
    ArrayList alShipValue = new ArrayList();
    HashMap hmShipMap = new HashMap();
    HashMap hmReturn = new HashMap();
    hmShippedData = gmShippingInfoBean.fetchShippedData();//report query
    alRuleList = (ArrayList) hmShippedData.get("alRuleList");
    alShippedData = (ArrayList) hmShippedData.get("alShippingList");
    ArrayList alTempList = new ArrayList(alShippedData);
    hmStaticVal = getShipmentStaticValues(alRuleList);
    StringBuffer strRule = (StringBuffer) hmStaticVal.get("strRule");
    String strFromMailValue = (String) hmStaticVal.get("gmFromMailValue");
    String strSubjectValue = (String) hmStaticVal.get("gmSubjectValue");
    String strCCMail = (String) hmStaticVal.get("gmCSMail");

    String strGmShipDesc = (String) hmStaticVal.get("SHIPDESC");
    String strGmShipValue = (String) hmStaticVal.get("SHIPMAIL");
    String strGmPhone = (String) hmStaticVal.get("SHIPPONE");
    String strFedexDesc = (String) hmStaticVal.get("FEDEXDESC");
    String strFedexValue = (String) hmStaticVal.get("FEDEXPHONE");
    String strGmMailFooter = (String) hmStaticVal.get("GMFOOTER");
    String strGmMailHeader = (String) hmStaticVal.get("GMHEADER");
    hmStaticVal.put("strClass", strJob);
    Iterator itr = alShippedData.iterator();
    /*
     * Logic Explanation :
     * 
     * - Here the fetchShippedData method will get the two arraylist. One having all the details of
     * shipped data and other having all the static values from rule table for sending mails. - Loop
     * has been traversed based on the records of alShippedData. From which we are getting the
     * values for the first record. - Then shiptoid and the alShippedData has been passed in
     * getShipValues() to get the arraylist(alShipValue) of only shipped records for that
     * rep/distributor. - After that we loop through alShipValue and pass the source and alShipValue
     * in getSourceValue() to get the arraylist(alSourceList) for specific source. - After that we
     * loop through alSourceList and pass the track and alSourceList in getTrackValue() to get the
     * arraylist(alTrackList) for specific source. - alTrackList will be helpful for preparing the
     * HTML String for email template. - if any exception occure in between the mail will be sent to
     * the respective department with the details otherwise shipment mails will be sent to
     * respective rep/distributor. - Job status mails will be sent to IT at the end of this method.
     */
    while (itr.hasNext()) {
      hmShippedRowData = (HashMap) itr.next();
      strFormedEmail = "";
      strShipToEmail = (String) hmShippedRowData.get("SHIPEMAIL");
      strShipCCEmail = (String) hmShippedRowData.get("CCLIST");
      strShipToNm = (String) hmShippedRowData.get("SHIPTONM");
      strShipToId = (String) hmShippedRowData.get("SHIPTOID");
      strShipToType = (String) hmShippedRowData.get("SHIPTOTYPE");
      strRepCCEmail = (String) hmShippedRowData.get("CCREPEMAIL");
      String strCompanyName = GmCommonClass.parseNull(rsCompanyBundle.getProperty("GMCOMPANYNAME"));
      String strComanyFooter = (strCompanyName.equals("")) ? "Globus Medical" : strCompanyName;
      strShiptoVisitedFl = GmCommonClass.parseNull((String) hmShippedRowData.get("SHIPVISITED"));
      log.debug("Shipto id....." + strShipToId + ".....refid....."
          + (String) hmShippedRowData.get("REFIDACTUAL") + ".....visited...." + strShiptoVisitedFl);
      if (!strShiptoVisitedFl.equals("Y")) {
        strHtmlString = new StringBuffer();

        hmReturn = getShipValues(strShipToEmail, alShippedData);
        alShipValue = (ArrayList) hmReturn.get("ALSHIP");
        strFedxFl = (String) hmReturn.get("FEDXFL");
        log.debug("alShipValue....." + alShipValue);
      }
      strTransdateFmt = GmCommonClass.parseNull((String) hmShippedRowData.get("TRANSDTFMT"));

      if (!strShiptoVisitedFl.equals("Y")) {
        try {
          HashMap hmSendMailData = new HashMap();
          hmSendMailData.put("strShipToId", strShipToId);
          hmSendMailData.put("strShipToNm", strShipToNm);
          hmSendMailData.put("EmailData", alShipValue);
          hmSendMailData.put("strShipToEmail", strShipToEmail);
          hmSendMailData.put("strRuleData", strRule.toString());
          hmSendMailData.put("strFromMailValue", strFromMailValue);
          hmSendMailData.put("strSubjectValue", strSubjectValue);
          hmSendMailData.put("strCCMail", strCCMail);
          hmSendMailData.put("strShipCCEmail", strShipCCEmail);
          hmSendMailData.put("strShipToType", strShipToType);
          hmSendMailData.put("SHIPDESC", strGmShipDesc);
          hmSendMailData.put("SHIPMAIL", strGmShipValue);
          hmSendMailData.put("SHIPPONE", strGmPhone);
          hmSendMailData.put("FEDEXDESC", strFedexDesc);
          hmSendMailData.put("FEDEXPHONE", strFedexValue);
          hmSendMailData.put("GMFOOTER", strGmMailFooter);
          hmSendMailData.put("FEDXFL", strFedxFl);
          hmSendMailData.put("strrepCCmail", strRepCCEmail);
          hmSendMailData.put("FOOTER", strComanyFooter);
          hmSendMailData.put("TRANSDATEFMT", strTransdateFmt);
          hmSendMailData.put("GMHEADER", strGmMailHeader);

          log.debug("calling send mail......................................................"
              + strCCMail);
          generateHTMLReport(hmSendMailData);// htmlString.append("")
          gmShippingTransBean.saveShippedData(updateShipmentEmail(alShipValue));

        } catch (Exception ex) {
          HashMap hmExcpVal = new HashMap();
          strJobStatusFl = "FAILED";
          strExcpTime = (strFormatter1.format(new java.util.Date()));
          hmExcpVal.put("EXCEPTION", GmCommonClass.getExceptionStackTrace(ex, "<br>"));
          hmExcpVal.put("strExcpTime", strExcpTime);
          hmExcpVal.put("strShipToNm", strShipToNm);
          hmExcpVal.put("strEmailData", strFormedEmail);
          alExceptionList.addAll(alShipValue);
          try {
            generateExcpReport(hmStaticVal, hmExcpVal);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }

    if (alExceptionList.size() > 0) {
      handleException(alTempList, alExceptionList);
    }
    // log.debug("alTempList......."+alTempList);



  }

  /**
   * updateShipmentEmail - This method will be used to update the flag for shippped email
   * 
   * @param ArrayList
   * @return HashMap
   */

  private HashMap updateShipmentEmail(ArrayList alList) {
    String strDbRefId = "";
    String strDbSource = "";
    String strDbValue = "";
    Iterator itrShipments = alList.iterator();
    HashMap hmReturn = new HashMap();
    HashMap hmEmailShipment = new HashMap();
    while (itrShipments.hasNext()) {
      hmEmailShipment = (HashMap) itrShipments.next();
      strDbRefId = (String) hmEmailShipment.get("REFIDACTUAL") + ",";
      strDbSource = (String) hmEmailShipment.get("SOURCEID") + "|";
      strDbValue = strDbValue + strDbRefId + strDbSource;

    }
    hmReturn.put("REFID", strDbValue);
    return hmReturn;
  }

  /**
   * handleException - This method will be used to handle the generated exceptions and remove those
   * record from the shipment list so the flag for those records wont updated.
   * 
   * @param ArrayList, ArrayList
   * 
   */
  private void handleException(ArrayList alShipment, ArrayList alExceptions) {
    Iterator itrShipments = alShipment.iterator();
    while (itrShipments.hasNext()) {
      HashMap hmMap = (HashMap) itrShipments.next();
      Iterator itrExceptions = alExceptions.iterator();
      while (itrExceptions.hasNext()) {
        HashMap hmTempMap = (HashMap) itrExceptions.next();

        if (hmMap == hmTempMap) {
          itrShipments.remove();
          // break;
        }
      }
    }

  }

  /**
   * generateExcpReport - This method will be used to generate exception template and send mails for
   * the records having exception
   * 
   * @param HashMap, HashMap
   * @exception Exception
   */
  private void generateExcpReport(HashMap hmExcpMailData, HashMap hmExcp) throws Exception {
    HashMap hmMailValues = new HashMap();
    StringBuffer strFormedExcpEmail = new StringBuffer();
    StringBuffer strExcpEmailData = new StringBuffer();
    String[] cc = {(String) hmExcpMailData.get("gmITMail")};
    String[] strRecipients = {(String) hmExcpMailData.get("gmCSMail")};

    strExcpEmailData.append("<tr> <td height=15 ></td> </tr>");
    strExcpEmailData.append("<tr> <td height=15 ><B>Shipment Status<B></td> </tr>");
    strExcpEmailData.append("<tr> <td height=15 >Exception Occure For :"
        + (String) hmExcp.get("strShipToNm") + " </td></tr>");
    strExcpEmailData.append("<tr> <td height=15 >Exception Time :"
        + (String) hmExcp.get("strExcpTime") + "</td></tr>");
    strExcpEmailData.append("<tr> <td height=15 >MESSAGE FAILED</td></tr>");
    strExcpEmailData.append("<tr> <td height=15 >Exception Message :"
        + (String) hmExcp.get("EXCEPTION") + "</td></tr>");
    strFormedExcpEmail.append("<table>".concat(strExcpEmailData.toString()) + "</table>");
    strFormedExcpEmail.append("<tr> <td height=15 ></td> </tr>");

    strFormedExcpEmail.append((String) hmExcp.get("strEmailData"));
    // log.debug("Complete.....string......"+strFormedEmail);

    hmMailValues.put("strFrom", hmExcpMailData.get("gmFromMailValue"));
    hmMailValues.put("strTo", strRecipients);
    hmMailValues.put("strCc", cc);
    hmMailValues.put("strSubject", (String) hmExcpMailData.get("strClass") + " - FAILED");
    hmMailValues.put("strMessageDesc", strFormedExcpEmail.toString());
    hmMailValues.put("strMimeType", "text/html");
    GmCommonClass.sendMail(hmMailValues);
  }

  /**
   * setHyperLinkForPackSlip - This method will be used to get the hyperlink of the packslip
   * 
   * @param ArrayList, String
   * @exception Exception
   */
  private ArrayList setHyperLinkForPackSlip(ArrayList alInput, String strLocalServerNm) {
    HashMap hmShippedRowData = null;
    String strSourceId = "";
    String strRefId = "";
    String strType = "";
    String strHyper = "";
    String strGUId = "";
    ArrayList alEmailData = new ArrayList();

    for (int i = 0; i < alInput.size(); i++) {

      hmShippedRowData = (HashMap) alInput.get(i);

      strSourceId = GmCommonClass.parseNull((String) hmShippedRowData.get("SOURCEID"));
      strRefId = GmCommonClass.parseNull((String) hmShippedRowData.get("REFIDACTUAL"));
      strGUId = GmCommonClass.parseNull((String) hmShippedRowData.get("GUID"));
      strType = GmCommonClass.parseNull((String) hmShippedRowData.get("TYPE"));

      strHyper =
           strLocalServerNm + GmCommonClass.getRuleValue(strSourceId, "EMAILTXNLINK")
              + strGUId;

      if (strSourceId.equals("50182")) { // Loaner
        strHyper = strHyper.replaceAll("TRANSTYPE", strType);
      }

      hmShippedRowData.put("HYPERLINK", strHyper);
      alEmailData.add(hmShippedRowData);

    }
    return alEmailData;
  }

  /**
   * generateHTMLReport - This method will be used to generate email template and send mails for
   * those records
   * 
   * @param HashMap
   * @exception Exception
   */

  private void generateHTMLReport(HashMap hmSendMailData) throws Exception {
    GmEmailProperties gmEmailProperties = null;

    HashMap hmMailValues = new HashMap();
    
   
    String strTransDatetFmt = (String) hmSendMailData.get("TRANSDATEFMT");    
    strTransDtFmt = new SimpleDateFormat(strTransDatetFmt);    
    String strDateFormat = (strTransDtFmt.format(new java.util.Date()));
    //String strDateFormat = (strFormatter2.format(new java.util.Date()));
    
    // String strEmailData = (String)hmSendMailData.get("strEmailData");
    String strRuleData = (String) hmSendMailData.get("strRuleData");
    String strShipToNm = (String) hmSendMailData.get("strShipToNm");
    String strShipToId = (String) hmSendMailData.get("strShipToId");
    String strShipToType = (String) hmSendMailData.get("strShipToType");
    String strShipCC = (String) hmSendMailData.get("strShipCCEmail");
    String strCCMail = (String) hmSendMailData.get("strCCMail");
    String strRepCC = (String) hmSendMailData.get("strrepCCmail");
    String strShipCCEmail = GmCommonClass.parseNull((String) hmSendMailData.get("strShipCCEmail"));
    
    
    /*String strLocalServerNm = InetAddress.getLocalHost().getHostName().toLowerCase();*/
    
    // Get property from JBOSS Server environment
    String strLocalServerNm = System.getProperty("HOST_NAME");
    log.debug("HostName "+strLocalServerNm);
    
    
    
    
    ArrayList alEmailData =
        GmCommonClass.parseNullArrayList((ArrayList) hmSendMailData.get("EmailData"));

    strLocalServerNm =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strLocalServerNm, "SERVERNAME"));
    log.debug("strLocalServerNm....." + strLocalServerNm);
    alEmailData = setHyperLinkForPackSlip(alEmailData, strLocalServerNm);

    hmSendMailData.put("EmailData", alEmailData);

    if (!strShipCCEmail.equals("")) {
      strCCMail = strCCMail.concat(";" + strShipCC);
    }
    if (!strRepCC.equals("")) {
      strCCMail = strCCMail.concat(";" + strRepCC);

    }


    String[] cc = GmCommonClass.StringtoArray(strCCMail, ";");
    // String[] cc = {strCCMail};
    String[] strRecipients = {(String) hmSendMailData.get("strShipToEmail")};
    String strShipToMail =
        "<tr> <td height=15 colspan=2><B>Following Shipments are Made to " + strShipToNm
            + "</B></td> </tr>";
    String strSubject =
        (String) hmSendMailData.get("strSubjectValue") + " to " + strShipToNm + " on "
            + strDateFormat;
    String strHeader =  (String) hmSendMailData.get("GMHEADER");
    // strFormedEmail =
    // "<table style=font-size:10px;>".concat(strShipToMail).concat(strEmailData).concat(strRuleData)
    // + "</table>";

    if (strShipToType.equals("4122")) {
      gmEmailProperties = new GmRepEmailProperties(strShipToId, "GmShipmentEmailJob");
    } else {
      gmEmailProperties = new GmEmailProperties();
    }
    // the following code is written for sending the attachment during sending the Shipment JOB
    // email.
    String strFilePath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("FILE", "MAILATTACHMENT"));
    File folder = new File(strFilePath);
    if (!folder.exists()) {
      folder.mkdir();
    }
    File[] listOfFiles = folder.listFiles();
    String fileNameInputStr = "";
    for (int i = 0; i < listOfFiles.length; i++) {
      if (listOfFiles[i].isFile()) {
        fileNameInputStr = fileNameInputStr + strFilePath + listOfFiles[i].getName() + ",";
      }
    }

    gmEmailProperties.setSender((String) hmSendMailData.get("strFromMailValue"));
    gmEmailProperties.setRecipients(StringUtils.join(strRecipients, ","));
    gmEmailProperties.setCc(StringUtils.join(cc, ","));
    gmEmailProperties.setSubject(strSubject);
    gmEmailProperties.setMessage(strFormedEmail);
    gmEmailProperties.setEmailHeaderName(strHeader);
    if (!fileNameInputStr.equals("")) {
      fileNameInputStr = fileNameInputStr.substring(0, fileNameInputStr.length() - 1);
      gmEmailProperties.setAttachment(fileNameInputStr);
    }
    gmEmailProperties.setMimeType("text/html");

    GmJasperMail jasperMail = new GmJasperMail();
    jasperMail.setJasperReportName("/GmShipmentEmail.jasper");
    jasperMail.setAdditionalParams(hmSendMailData);
    jasperMail.setReportData((ArrayList) hmSendMailData.get("EmailData"));
    jasperMail.setEmailProperties(gmEmailProperties);
    HashMap hmReturn = jasperMail.sendMail();

    // GmCommonClass.sendMail(gmEmailProperties);
  }

  /**
   * getShipmentStaticValues - This method will be used to get the static records from the rule
   * table
   * 
   * @param ArrayList
   * @return HashMap
   */

  private HashMap getShipmentStaticValues(ArrayList alRuleList) {
    Iterator itrRule = alRuleList.iterator();

    String strGmShipDesc = "";
    String strGmShipValue = "";
    String strFedexDesc = "";
    String strFedexValue = "";
    String strGmPhone = "";
    String strGmFromMailDesc = "";
    String strGmFromMailValue = "";
    String strGmSubjectDesc = "";
    String strGmSubjectValue = "";
    String strGmMailFooter = "";
    String strCSMail = "";
    String strITMail = "";
    String strGmMailHeader = "";
    HashMap hmRule = new HashMap();
    StringBuffer strRule = new StringBuffer();

    while (itrRule.hasNext()) {
      hmRule = (HashMap) itrRule.next();
      String ruleId = (String) hmRule.get("RULEID");
      if (ruleId.equals("GMSHIPINFO")) {
        strGmShipDesc = (String) hmRule.get("DESCRIPTION");
        strGmShipValue = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMPHONE")) {
        strGmPhone = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("FEDEXPHONE")) {
        strFedexDesc = (String) hmRule.get("DESCRIPTION");
        strFedexValue = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMFROMMAIL")) {
        strGmFromMailDesc = (String) hmRule.get("DESCRIPTION");
        strGmFromMailValue = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("EMAILSUB")) {
        strGmSubjectDesc = (String) hmRule.get("DESCRIPTION");
        strGmSubjectValue = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMMAILFOOTER")) {
        strGmMailFooter = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMCSMAILID")) {
        strCSMail = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMITMAILID")) {
        strITMail = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMMAILHEADER")) {
    	  strGmMailHeader = (String) hmRule.get("RULEVALUE");
        }
      /*
       * if(ruleId.equals("GMCCMAILID")) { strGmCCMail = (String)hmRule.get("RULEVALUE"); }
       * 
       * if(ruleId.equals("EXCPCCMAILID")) { strExcpCCMail = (String)hmRule.get("RULEVALUE"); }
       * if(ruleId.equals("EXCPTOMAILID")) { strExcpToMail = (String)hmRule.get("RULEVALUE"); }
       */

    }
    /*
     * strRule.append("<tr> <td height=15 colspan=2></td> </tr>");
     * strRule.append("<tr><td colspan=2 > <B>"+strGmShipDesc+" :</B> </td></tr>");
     * strRule.append("<tr> <td colspan=2> </td> </tr>");
     * strRule.append("<tr> <td align=right nowrap>  <B>Email :</B> </td> <td >"
     * +strGmShipValue+"</td> </tr>");
     * strRule.append("<tr> <td align=right nowrap>  <B>Phone :</B> </td> <td >"
     * +strGmPhone+"</td>  </tr>"); strRule.append("<tr> <td height=15 colspan=2> </td> </tr>");
     * strRule.append("<tr> <td colspan=2><B>"+strFedexDesc+" :</B></td></tr>");
     * strRule.append("<tr> <td align=right nowrap>  <B>Phone :</B> </td>  <td>"
     * +strFedexValue+"</td> </tr>"); strRule.append("<tr> <td height=15 colspan=2> </td> </tr>");
     * strRule.append("<tr> <td  colspan=2> <B>Globus Medical</B> </td> </tr>");
     * strRule.append("<tr> <td height=15 colspan=2> </td> </tr>");
     * strRule.append("<tr> <td colspan=2><B>"+strGmMailFooter+" </B></td></tr>");
     */

    hmRule.put("strRule", strRule);
    hmRule.put("gmFromMailDesc", strGmFromMailDesc);
    hmRule.put("gmFromMailValue", strGmFromMailValue);
    hmRule.put("gmSubjectValue", strGmSubjectValue);
    hmRule.put("gmCSMail", strCSMail);
    hmRule.put("gmITMail", strITMail);

    hmRule.put("SHIPDESC", strGmShipDesc);
    hmRule.put("SHIPMAIL", strGmShipValue);
    hmRule.put("SHIPPONE", strGmPhone);
    hmRule.put("FEDEXDESC", strFedexDesc);
    hmRule.put("FEDEXPHONE", strFedexValue);
    hmRule.put("GMFOOTER", strGmMailFooter);
    hmRule.put("GMHEADER", strGmMailHeader);

    // hmRule.put("strExcpCCMail", strExcpCCMail);

    return hmRule;
  }

  /**
   * getShipValues - This method will be used to group the arraylist based on the rep id
   * 
   * @param String shipToId, ArrayList alList
   * @return ArrayList
   */
  private HashMap getShipValues(String shipToId, ArrayList alList) {
    int shipmentCnt = 0; // Check with Prasath, need to set from properties file.
    int fedexCnt = 0;
    Iterator itrValues = alList.iterator();
    HashMap hmValues = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alShipList = new ArrayList();
    while (itrValues.hasNext()) {

      hmValues = (HashMap) itrValues.next();
      String strShiptoVisitedFl = GmCommonClass.parseNull((String) hmValues.get("SHIPVISITED"));
      String strDeliverCrr = GmCommonClass.parseNull((String) hmValues.get("DCRR"));
      if (shipToId.equals(hmValues.get("SHIPEMAIL")) && !strShiptoVisitedFl.equals("Y")) {
        alShipList.add(hmValues);
        hmValues.put("SHIPVISITED", "Y");
        shipmentCnt++;
        if (strDeliverCrr.equals("5001")) {
          fedexCnt++;
        }
      }
      if (shipmentCnt == shipmentRepCnt)
        break;
    }
    log.debug("alShipList...size....." + alShipList.size());
    if (fedexCnt > 0)
      hmReturn.put("FEDXFL", "Y");

    hmReturn.put("ALSHIP", alShipList);
    return hmReturn;
  }


  /**
   * getSourceValues - This method will be used to group the arraylist based on the source
   * 
   * @param String strSource, ArrayList alSourceList
   * @return ArrayList
   */
  /*
   * private ArrayList getSourceValues( String strSource, ArrayList alSourceList) { Iterator
   * itrValues = alSourceList.iterator(); HashMap hmValues = new HashMap(); ArrayList alList = new
   * ArrayList(); int index = 0; while(itrValues.hasNext()) { hmValues = (HashMap)itrValues.next();
   * 
   * if(strSource.equals((String)hmValues.get("SOURCE"))) { hmValues.put("VISITED", "Y");
   * alList.add(hmValues);
   * 
   * if(index != 0) { hmValues.put("SOURCE", ""); } index++; }
   * 
   * 
   * } return alList; }
   */
  /**
   * getTrackValues - This method will be used to group the arraylist based on the Tracking No.
   * 
   * @param String trackNo, ArrayList alList
   * @return ArrayList
   */

  /*
   * private ArrayList getTrackValues(String trackNo, ArrayList alList) { Iterator itrValues =
   * alList.iterator(); HashMap hmValues = new HashMap(); ArrayList alTrackList = new ArrayList();
   * 
   * while(itrValues.hasNext()) { hmValues = (HashMap)itrValues.next();
   * if(trackNo.equals((String)hmValues.get("TRACK"))) { alTrackList.add(hmValues);
   * hmValues.put("TRACKVISITED", "Y"); } } return alTrackList; }
   */

  /**
   * getHTMLString - This method will be used to prepare the HTML String for the template.
   * 
   * @param ArrayList alList
   * @return String
   */

  /*
   * private String getHTMLString(ArrayList alList) { StringBuffer strEmailData = new
   * StringBuffer(); Iterator itrValues = alList.iterator(); HashMap hmValues = new HashMap(); int
   * cnt = 0; int alListSize = alList.size(); int trackCnt = 1; while(itrValues.hasNext()) {
   * hmValues = (HashMap)itrValues.next(); String strSource =
   * GmCommonClass.parseNull((String)hmValues.get("SOURCE")); strSource = strSource.equals("") ? ""
   * : strSource+" : "; if (cnt == 0) {
   * strEmailData.append("<tr> <td height=15 colspan=2> </td> </tr>");
   * strEmailData.append("<tr> <td colspan=2> <b>"+ strSource + "</b> </td> </tr>");
   * strEmailData.append("<tr> <td height=15 colspan=2> </td> </tr>");
   * strEmailData.append("<tr> <td align=right nowrap> <b> Shipping Carrier : </b> </td><td>"
   * +(String)hmValues.get("SCARR")+"</td> </tr>");
   * strEmailData.append("<tr> <td align=right nowrap> <b> Shipping Method : </b> </td> <td>"
   * +(String)hmValues.get("SMODE")+"</td> </tr>"); cnt=1; }
   * strEmailData.append("<tr> <td align=right nowrap> <b> ID : </b> </td> <td>"
   * +(String)hmValues.get("REFID")+"</td> </tr>"); String strSetId =
   * GmCommonClass.parseNull((String)hmValues.get("SETID")); if(!strSetId.equals("")) {
   * strEmailData.
   * append("<tr> <td align=right nowrap> <b> Set ID : </b> </td> <td>"+(String)hmValues
   * .get("SETID")+"</td> </tr>");
   * strEmailData.append("<tr> <td align=right nowrap> <b> Set Name : </b> </td> <td>"
   * +(String)hmValues.get("SETNAME")+"</td> </tr>"); } if(trackCnt == alListSize) {
   * strEmailData.append(
   * "<tr> <td align=right nowrap > <b> Tracking Number : </b> </td> <td><a href=http://fedex.com/Tracking?ascend_header=1&clienttype=dotcom&cntry_code=us&language=english&tracknumbers="
   * +(String)hmValues.get("TRACK")+">"+(String)hmValues.get("TRACK")+"</a></td> </tr>"); }
   * trackCnt++; }
   * 
   * return strEmailData.toString(); }
   */
}
