/**
 * FileName    : GmCMPOverdueEvalJob.java 
 * Description : Auto Notification for Overdue Evaluation
 * Author      : arajan
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

/**
 * @author arajan
 *
 */
public class GmCMPOverdueEvalJob extends GmActionJob {

	
	public void execute(JobDataMap jobDataMap) throws Exception {

		GmDBManager gmDBManager = new GmDBManager();
		GmEmailProperties emailProps = new GmEmailProperties();
        GmJasperMail jasperMail = new GmJasperMail();
        GmCommonBean gmCommonBean = new GmCommonBean();
        
        String strGemMailId = "";
        String strRefId		= "";
        String strLogType	= "";
		ArrayList alResult = new ArrayList();
		ArrayList alList = new ArrayList();
		HashMap hmResult = new HashMap();
		HashMap hmList = new HashMap();
		HashMap hmAdditionalParams = new HashMap();
		HashMap hmReturnDetails = new HashMap();
		
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_process.gm_fch_overdue_eval_info", 2); // To get the mail Id
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, null);
		gmDBManager.execute();

		alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)); 
		gmDBManager.close();
		
		if(alResult.size() > 0){
			emailProps.setSender(GmCommonClass.getEmailProperty("GmCMPOverdueEval.From"));
	        emailProps.setCc(GmCommonClass.getEmailProperty("GmCMPOverdueEval.Cc"));
	        emailProps.setMimeType(GmCommonClass.getEmailProperty("GmCMPOverdueEval.MimeType"));
	        emailProps.setReplyTo(GmCommonClass.getEmailProperty("GmCMPOverdueEval.ReplyTo"));
	        emailProps.setSubject(GmCommonClass.getEmailProperty("GmCMPOverdueEval.Subject"));
	        String strLog = GmCommonClass.getEmailProperty("GmCMPOverdueEval.log");
        
       
			for (Iterator iter = alResult.iterator(); iter.hasNext();) {
	
				try{
					
					hmResult = (HashMap) iter.next();
					
					strGemMailId  	= GmCommonClass.parseNull((String)hmResult.get("GEMMAILID"));
					
					gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_process.gm_fch_overdue_eval_info", 2); // To get the details based on mail Id
					gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
					gmDBManager.setString(1, strGemMailId);
					gmDBManager.execute();
					
					alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
					gmDBManager.close();
					String strJasperName = "/GmCMPOverdueEval.jasper"; 
						
					hmAdditionalParams.put("ALLIST", alList);
					
						
				    emailProps.setRecipients(strGemMailId);
				    jasperMail.setJasperReportName(strJasperName);
				    jasperMail.setAdditionalParams(hmAdditionalParams);
				    jasperMail.setReportData(alList);
				    jasperMail.setEmailProperties(emailProps);
				    hmReturnDetails = jasperMail.sendMail();  
				       
				    for(int i=0; i<alList.size(); i++){ // To save log corresponding to each COM/RSR Id
				       	hmList = (HashMap)alList.get(i);
				       	strRefId  		= GmCommonClass.parseNull((String)hmList.get("INCIDENTID"));
						strLogType  	= GmCommonClass.parseNull((String)hmList.get("REFTYPE"));
						gmCommonBean.saveLog(strRefId, strLog, "30301", strLogType); 
				    }
					
				}
				catch (Exception e) {
	
					log.error("Exception in job body: " + e.getMessage());
					hmReturnDetails.put(GmJob.EXCEPTION, e);
				}
				Exception ex = (Exception)hmReturnDetails.get(GmJob.EXCEPTION);
				if(ex != null)
				{
					log.error("Exception in sendJasperMail");
					setJobStatus(GmJob.JOB_STATUS_FAIL);
					setJobException(GmCommonClass.getExceptionStackTrace(ex,"<br>"));
					setAdditionalParam((String)hmReturnDetails.get("ORIGINALMAIL"));
					sendJobExceptionEmail();
					throw ex;
				}
				
			}
			
        }

	}

}
