/**
 * FileName    : GmDHRPriorityRefresh.java 
 * Description : This class is called by the Quartz job to refresh the DHR Priority for the Received DHR ID which are not yet processed.
 * Author      : gpalani (Modified)
 * Date        : Jul 17, 2018
 * Copyright   : Globus Medical Inc
 */

package com.globus.common.util.jobs;
import org.quartz.JobDataMap;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.ArrayList;

import oracle.jdbc.driver.OracleTypes;

public class GmDHRPriorityRefresh extends GmActionJob  {
     @Override
	public void execute(JobDataMap jobDataMap) throws Exception  {
         GmDBManager gmDBManagerTTPWeb = new GmDBManager(GmDNSNamesEnum.EXTTIME_WEBGLOBUS, getGmDataStoreVO(jobDataMap));

		// Calling the procedure to refresh the DHR Priority for the received parts
         gmDBManagerTTPWeb.setPrepareString("gm_pkg_dhr_priority.gm_dhr_priority_refresh",1);
         gmDBManagerTTPWeb.setInt(1, 30301);
         gmDBManagerTTPWeb.execute();
	     gmDBManagerTTPWeb.commit();
				

	}
	
}

