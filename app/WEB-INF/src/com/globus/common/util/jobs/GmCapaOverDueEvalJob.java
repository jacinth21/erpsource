/**
 * FileName    : GmCapaOverDueEvalJob.java
 * Description :
 * Author      : Jignesh Shah
 * Date        : Oct 24, 2013 
 * Copyright   : Globus Medical Inc
 */

package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.common.util.jobs.GmJob;
import com.globus.quality.capa.beans.GmCapaTransBean;


public class GmCapaOverDueEvalJob extends GmActionJob {

	public static final String TEMPLATE_NAME = "GmCapaOverDueEvalJob";
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
		String strToEmail = "";
		String strCc = "";
		String strCapaId = "";
		String strMessageBody = "";
		String strMimeType = "";
		String strCapaTemp = "";
		String strFrom = "";
		String strCapaResDate = "";
		String strLog = "";
		
		ArrayList  alResult = new ArrayList();
		
		GmCapaTransBean gmCapaTransBean = new GmCapaTransBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		
		GmDBManager gmDBManager = new GmDBManager();
		log.debug("jobDataMap = " + jobDataMap);
		gmDBManager.setPrepareString("gm_pkg_qa_capa_rpt.gm_fch_capa_eval_info", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1)));
		gmDBManager.close();
		HashMap hmCapaOverDueDtls = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmParamValues =  new HashMap();
		HashMap hmRetEmailId =  new HashMap();
		
		for(Iterator iter = alResult.iterator(); iter.hasNext();)
		{
			try
			{
				hmCapaOverDueDtls = (HashMap)iter.next();
				
				String strDay = (String)hmCapaOverDueDtls.get("DAY");
				
				GmEmailProperties emailProps = new GmEmailProperties();
				
				strCapaId = GmCommonClass.parseNull((String)hmCapaOverDueDtls.get("CAPAID"));
				strCapaResDate = GmCommonClass.parseNull((String)hmCapaOverDueDtls.get("CAPARESDATE"));
				
				
				if(!strCapaTemp.equals(strCapaId)){					
					strCapaTemp = strCapaId;
					log.debug("strCapaTemp = "+strCapaTemp);
					hmParamValues.put("REFID", strCapaTemp);
					hmParamValues.put("STROPT", "closureEmail");
					
					hmRetEmailId = gmCapaTransBean.fetchCapa(hmParamValues);  
	                strToEmail = GmCommonClass.parseNull((String)hmRetEmailId.get("TOEMAIL"));
	               /* Use Commented code to avoid duplicate email ids
	                * String[] arryEmailIDs = strToEmail.split(",");
	                HashSet setEmails = new HashSet();
	                Collections.addAll(setEmails, arryEmailIDs); 
	                String strSetEmails = setEmails.toString();
	                strToEmail = strSetEmails.substring(1, strSetEmails.length()-1);
	                log.debug("strToEmail = "+strToEmail);*/
	                
					strCc = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."+ GmEmailProperties.CC));
					strFrom = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."+ GmEmailProperties.FROM));
					strMimeType = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
					strLog = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + "LogReason20"));
					strMessageBody = "This is just a notification that the above referenced CAPA response is still pending. The response is due back to QA on " +strCapaResDate+ ".  Please let us know if you have any questions or concerns.";
					if (strDay.equals("30")){
						strMessageBody = "The response to the above referenced CAPA is overdue and due back to QA as soon as possible. The response was due back on " +strCapaResDate+ ".  Please let us know if you have any questions or concerns.";
						strLog = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + "LogReason30"));
					}
					log.debug("strLog:::"+strLog);
					hmCapaOverDueDtls.put("CAPAMESSAGE", strMessageBody);
					
					emailProps.setMimeType(strMimeType);
					emailProps.setRecipients(strToEmail);
					emailProps.setCc(strCc);
					emailProps.setSender(strFrom);
					emailProps.setSubject(strCapaTemp);
					
					GmJasperMail jasperMail = new GmJasperMail();
					jasperMail.setJasperReportName("/GmCapaOverDueEvalJob.jasper");
					jasperMail.setAdditionalParams(hmCapaOverDueDtls);
					jasperMail.setReportData(null);
					jasperMail.setEmailProperties(emailProps);
					hmReturn = jasperMail.sendMail();
					
					if (!strLog.equals("")) {
						GmDBManager gmLogDBManager = new GmDBManager();
                        log.debug("The log value is  ****** "+strLog+ " ***** "+strCapaTemp);
                        gmCommonBean.saveLog(gmLogDBManager, strCapaTemp, strLog, "30301", "4000317");//4000317-->Code id for Scar Log
                        gmLogDBManager.commit();
                  }
				}
			}
			catch(Exception e)
			{
				log.error("Exception in job body: " + e.getMessage());
				hmReturn.put(GmJob.EXCEPTION, e);
			}
			Exception ex = (Exception)hmReturn.get(GmJob.EXCEPTION);
			if(ex != null)
			{
				log.error("Exception in sendJasperMail");
				setJobStatus(GmJob.JOB_STATUS_FAIL);
				setJobException(GmCommonClass.getExceptionStackTrace(ex,"<br>"));
				setAdditionalParam((String)hmReturn.get("ORIGINALMAIL"));
				sendJobExceptionEmail();
				throw ex;
			}
		}
		
	}
}
