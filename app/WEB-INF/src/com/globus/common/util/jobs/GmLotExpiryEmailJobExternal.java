/**
 * FileName    : GmLotExpiryEmailJobExternal.java 
 * Description :
 * Author      : T.S Ramachandiran
 * Date        : MAY 6, 2020 
 * Copyright   : Globus Medical Inc
 */

package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * GmLotExpiryEmailJobExternal- This class is used to send notification email to Lot Expiry details for External
 *
 */

public class GmLotExpiryEmailJobExternal extends GmActionJob{
	
	public static final String TEMPLATE_NAME = "GmLotExpiryEmailJobExternal";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public void execute(JobDataMap jobDataMap) throws Exception 
	{
		GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		log.debug("jobDataMap = " + jobDataMap);

		HashMap hmTempLoop = new HashMap();
		HashMap hmParams = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmEmailProperty = new HashMap();
		HashMap hmLoop = null;
		
		String strCurrDistId = "";
		String strNextDistId = "";
		String strTo = "";
		
		HashMap hmParam = GmCommonClass.parseNullHashMap((HashMap) jobDataMap.get("HMPARAM"));
		String strExpiryLot = GmCommonClass.parseNull((String) hmParam.get("LOTEXPIRY")); 
	
	    ArrayList alTempExpiredLot = new ArrayList();
	    gmDBManager.setPrepareString("gm_pkg_op_lot_expiry_report.gm_fch_expired_lot_mail_external",2);
	    gmDBManager.setString(1, strExpiryLot);
	    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    ArrayList alExpiredLot = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
	    log.debug("alExpiredLot--->" + alExpiredLot.size());
	    int intsize = alExpiredLot.size();
	    for(int i=0; i<intsize; i++)
	    {
	    	hmLoop = new HashMap();
			hmLoop = (HashMap) alExpiredLot.get(i);
			strCurrDistId = GmCommonClass.parseNull((String) hmLoop.get("DISTID"));
			strTo = GmCommonClass.parseNull((String) hmLoop.get("TOMAIL"));
			
			if (i < intsize - 1) {
				hmTempLoop = (HashMap) alExpiredLot.get(i + 1);
				strNextDistId = GmCommonClass.parseNull((String) hmTempLoop.get("DISTID"));
			}
			
			alTempExpiredLot.add(hmLoop);
			if(!strCurrDistId.equals(strNextDistId) || i == intsize - 1)
			{
				hmParam.put("ALRETURN", alTempExpiredLot);
				hmParam.put("STRTO", strTo);
				
				hmEmailProperty = populateEmailData(hmParam);
				log.debug(":hmEmailProperty=======:" + hmEmailProperty);
				try {
					GmCommonClass.sendMail(hmEmailProperty);
				} catch (Exception ex) {
					log.error("Exception in sending" + ex.getMessage());
				}
			alTempExpiredLot = new ArrayList();
			}
	    }
	}
	
	/**
	 * populateEmailData - This Methods is used to populate the mail content
	 * 
	 * @param hmParam
	 *  * @return
	 * @throws AppError
	 */
	public HashMap populateEmailData(HashMap hmParam) throws AppError {
		
		String strSubject = "";
		String strMessage = "";
		
		String strExpiryLot = GmCommonClass.parseNull((String) hmParam.get("LOTEXPIRY"));      //to get expiry date range for lot
		String strFrom = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM);
		String strMimeType = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE);
		strSubject = GmCommonClass.getEmailProperty(TEMPLATE_NAME + strExpiryLot + "." + GmEmailProperties.SUBJECT);
		strMessage = GmCommonClass.getEmailProperty(TEMPLATE_NAME + strExpiryLot + "." + GmEmailProperties.MESSAGE);
		String strToCc = GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOT_EXPIRY_EXTERNEL_CC", "LOT_EXPIRY"));
		String strTo   = GmCommonClass.parseNull((String) hmParam.get("STRTO"));
			
		HashMap hmEmailProperty;
		hmParam.put("STRMESSAGE", strMessage);
		
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setMimeType(strMimeType);
		emailProps.setSender(strFrom);
		emailProps.setRecipients(strTo);
		emailProps.setCc(strToCc);
		emailProps.setSubject(strSubject);
		emailProps.setMessage(strMessage);
		hmEmailProperty = generateOutput(emailProps, hmParam);
		return hmEmailProperty;

	}
	
	/**
	 * generateOutPut This method is used to generate data which need to show
	 * email content.
	 * 
	 * @param hmParam HashMap,GmEmailProperties emailProps
	 *            
	 * @exception AppError
	 */
	public HashMap generateOutput(GmEmailProperties emailProps, HashMap hmParam)
			throws AppError {
		String strTemplateName = GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOTEXTERNALTEMPLATE", "LOT_EXPIRY"));
		log.debug("generateOutput-->>> "+strTemplateName );
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setTemplateSubDir("operations/templates");
		templateUtil.setTemplateName(strTemplateName);
		templateUtil.setDataMap("hmEmailParam", hmParam);
		String strEmailContent = templateUtil.generateOutput();

		HashMap hmEmailProperty = new HashMap();
		hmEmailProperty.put("strFrom", emailProps.getSender());
		hmEmailProperty.put("strTo", emailProps.getRecipients());
		hmEmailProperty.put("strCc", emailProps.getCc());
		hmEmailProperty.put("strSubject", emailProps.getSubject());
		hmEmailProperty.put("strMessage", emailProps.getMessage());
		hmEmailProperty.put("strMessageDesc", strEmailContent);
		hmEmailProperty.put("strMimeType", emailProps.getMimeType());

		return hmEmailProperty;
	}

}
