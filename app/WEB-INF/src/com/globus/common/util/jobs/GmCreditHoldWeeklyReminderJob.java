package com.globus.common.util.jobs;

import java.util.HashMap;

import org.quartz.JobDataMap;

import com.globus.accounts.beans.GmCreditHoldBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * The Class GmCreditHoldWeeklyReminderJob- This job will execute every week Friday midnight to
 * notify Account Credit Hold information to their respective sales reps.
 */
public class GmCreditHoldWeeklyReminderJob extends GmActionJob {


  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    // TODO Auto-generated method stub
	// Added for PMT-53329 passing the datastorevo to bean
	GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmCreditHoldBean gmCreditHoldBean = new GmCreditHoldBean(gmDataStoreVO);
    // The Below method is called two times,First Time Credit type will be soft and Second time
    // Credit type will be hard.While sending Job mail there needs to be different message need to
    // be displayed for soft and hard type.
    HashMap hmCreditTypeString = new HashMap();
    hmCreditTypeString.put("CRDITTYPESTR", GmCommonClass.getRuleValue("SOFT", "CREDIT_HOLD_TYPE"));
    gmCreditHoldBean.sendCreditHoldWeeklyEmail(hmCreditTypeString);
    hmCreditTypeString.put("CRDITTYPESTR", GmCommonClass.getRuleValue("HARD", "CREDIT_HOLD_TYPE"));
    gmCreditHoldBean.sendCreditHoldWeeklyEmail(hmCreditTypeString);
  }

}
