package com.globus.common.util.jobs;

import java.util.ArrayList;
import java.util.HashMap;
import org.quartz.JobDataMap;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.common.bean.GmMasterRedisTransBean;
import com.globus.webservice.sales.kit.bean.GmKitMappingWrapperBean;

public class GmSyncPartListJob extends GmActionJob{
	 /*
	   * (non-Javadoc)
	   * 
	   * @see com.globus.common.util.jobs.GmActionJob#execute(org.quartz.JobDataMap)
	   */
	  @Override
	  public void execute(JobDataMap jobDataMap) throws AppError {

	    // to get the data store information from jobDataMap object
	    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
		GmMasterRedisTransBean gmMasterRedisTransBean = new GmMasterRedisTransBean(gmDataStoreVO);
		GmKitMappingWrapperBean gmKitMappingWrapperBean = new GmKitMappingWrapperBean(gmDataStoreVO);
		ArrayList alList = new ArrayList();
		String strCompanyInfo = "";
		String strOldPartNm = "";
		String strPartNum = "";
		strCompanyInfo ="{\"cmpid\":\""+gmDataStoreVO.getCmpid()+"\",\"partyid\":\""+gmDataStoreVO.getPartyid()+"\",\"plantid\":\""+gmDataStoreVO.getPlantid()+"\"}";
		log.debug("GmSyncPartListJob strCompanyInfo----->>"+strCompanyInfo);
		String strPartName = "";
		alList = gmMasterRedisTransBean.fetchSyncPartDetails();
			if(alList.size()>0){
				for(int i=0;i<alList.size()-1;i++){
					HashMap hmLoop = (HashMap)alList.get(i);			
						strPartNum = GmCommonClass.parseNull((String)hmLoop.get("ID"));
						strOldPartNm = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
						strPartName = GmCommonClass.parseNull((String)hmLoop.get("NAME"));
						hmLoop.put("ID",strPartNum);
						hmLoop.put("OLD_NM",strOldPartNm);
						hmLoop.put("NEW_NM",strPartName);
						gmKitMappingWrapperBean.syncPartNumList(strPartNum, strPartNum, strPartName, "syncPartNumList", strCompanyInfo);	
				}
			}
	  }
}

