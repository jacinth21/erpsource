/**
 * FileName : GmEventShipmentJob.java Description : Author : Jignesh Shah Copyright : Globus Medical
 * Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperJobMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author Jignesh shah
 * 
 */
public class GmEventShipmentJob extends GmLoanerRequestsEmailJob {

  public static final String TEMPLATE_NAME = "GmEventShipmentJob";
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    log.debug("jobDataMap = " + jobDataMap);
    ArrayList alCaseInfoId = new ArrayList();
    ArrayList alEventInfoDetails = new ArrayList();
    ArrayList alShippedSetsInfo = new ArrayList();
    ArrayList alShippedItemsInfo = new ArrayList();

    HashMap hmRepDetails = new HashMap();
    HashMap hmParams = new HashMap();
    HashMap hmEventInfoDetails = new HashMap();
    HashMap hmReturn = new HashMap();
    String strPath = GmCommonClass.getString("GMSUBREPORTLOCATION");
    Boolean FedexFl = false;
    log.debug("strPath::" + strPath);
    String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
    // getting the email properties object
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    GmResourceBundleBean rsCompanyProperties =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    // hmParams.put("SUBREPORT_DIR", strPath);

    // hmParams = getShippingDetails();

    /* Getting all the rep id's who have submitted requests today */
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_shipment_info", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alCaseInfoId = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    for (Iterator iter = alCaseInfoId.iterator(); iter.hasNext();) {
      try {
        hmParams.put("GMCSPHONE", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSPhone"));
        hmParams.put("GMCSEMAIL", gmResourceBundleBean.getProperty("GmCommon" + ".GlobusCSEmail"));

        hmRepDetails = (HashMap) iter.next();
        String stCaseInfoID = (String) hmRepDetails.get("CASE_INFO_ID");
        log.debug("Fetching requests for case info ID " + stCaseInfoID);
        /* Getting request details for each rep */
        gmDBManager.setPrepareString("gm_pkg_sm_eventbook_txn.gm_fch_event_info", 3);
        gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
        gmDBManager.setInt(1, Integer.parseInt(stCaseInfoID));
        gmDBManager.setString(2, "");
        gmDBManager.execute();
        hmEventInfoDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));

        gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_shipped_sets", 3);
        gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
        gmDBManager.setInt(1, Integer.parseInt(stCaseInfoID));
        gmDBManager.setString(2, "Y");
        gmDBManager.execute();
        alShippedSetsInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
        for (int i = 0; i < alShippedSetsInfo.size(); i++) {
          HashMap hmdata = (HashMap) alShippedSetsInfo.get(i);
          if (GmCommonClass.parseNull((String) hmdata.get("DELCARRIER")).equals("FedEx")) {
            FedexFl = true;
            break;
          }
        }

        gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_shipped_items", 2);
        gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
        gmDBManager.setInt(1, Integer.parseInt(stCaseInfoID));
        gmDBManager.execute();
        alShippedItemsInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
        // gmDBManager.commit();

        if (!FedexFl) {
          for (int i = 0; i < alShippedItemsInfo.size(); i++) {
            HashMap hmdata = (HashMap) alShippedItemsInfo.get(i);
            if (GmCommonClass.parseNull((String) hmdata.get("DELCARRIER")).equals("FedEx")) {
              FedexFl = true;
              break;
            }
          }
        }
        if (FedexFl)
          hmParams.put("FEDEXFLAG", "Y");
        else
          hmParams.put("FEDEXFLAG", "N");

        hmParams.put("ALSHIPPEDSETS", alShippedSetsInfo);
        hmParams.put("ALSHIPPEDITEMS", alShippedItemsInfo);

        hmParams.putAll(hmEventInfoDetails);
        hmParams.putAll(hmRepDetails);
        GmEmailProperties emailProps = getEmailProperties(TEMPLATE_NAME, "");
        String strSubject = emailProps.getSubject();
        String strEmailID = GmCommonClass.parseNull((String) hmRepDetails.get("EMAIL"));
        String strLoanToEmailID =
            GmCommonClass.parseNull((String) hmEventInfoDetails.get("LOANTOEMAILID"));
        String strEmail = strEmailID + "," + strLoanToEmailID;
        df = new SimpleDateFormat(gmDataStoreVO.getCmpdfmt());
        strSubject = GmCommonClass.replaceAll(strSubject, "#<RQDATE>", df.format(new Date()));
        emailProps.setReplyTo(GmCommonClass.parseNull(rsCompanyProperties
            .getProperty("EVENTREPLYTOEMAILID")));
        emailProps.setRecipients(strEmail);
        emailProps.setSubject(strSubject);

        if (alShippedSetsInfo.size() > 0 || alShippedItemsInfo.size() > 0) {
          GmJasperJobMail jasperMail = new GmJasperJobMail();
          jasperMail.setJasperReportName("/GmEventShipment.jasper");
          jasperMail.setAdditionalParams(hmParams);
          jasperMail.setReportData(null);
          jasperMail.setEmailProperties(emailProps);

          hmReturn = jasperMail.sendMail();
        }
        gmDBManager.setPrepareString("gm_pkg_sm_eventbook_txn.gm_sav_evnt_shpd_email_sts", 1);
        gmDBManager.setInt(1, Integer.parseInt(stCaseInfoID));
        gmDBManager.execute();
        gmDBManager.commit();
        // gmDBManager.close();
      } catch (Exception e) {
        gmDBManager.close();
        log.error("Exception in job body: " + e.getMessage());
        hmReturn.put(GmJob.EXCEPTION, e);
      }
      Exception ex = (Exception) hmReturn.get(GmJob.EXCEPTION);
      if (ex != null) {
        log.error("Exception in sendJasperMail");
        setJobStatus(GmJob.JOB_STATUS_FAIL);
        setJobException(GmCommonClass.getExceptionStackTrace(ex, "<br>"));
        setAdditionalParam((String) hmReturn.get("ORIGINALMAIL"));
        sendJobExceptionEmail();
        throw ex;
      }
    }
  }



}
