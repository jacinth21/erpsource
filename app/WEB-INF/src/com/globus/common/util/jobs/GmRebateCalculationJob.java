package com.globus.common.util.jobs;

import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.util.beans.GmRebateCalculationBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author mmuthusamy
 *
 */
public class GmRebateCalculationJob extends GmActionJob {

  /*
   * (non-Javadoc)
   * 
   * @see com.globus.common.util.jobs.GmActionJob#execute(org.quartz.JobDataMap)
   */
  @Override
  public void execute(JobDataMap jobDataMap) throws AppError {

    // to get the data store information from jobDataMap object
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);

    GmRebateCalculationBean gmRebateCalculationBean = new GmRebateCalculationBean(gmDataStoreVO);

    // to call the Discount order job procedure
    gmRebateCalculationBean.processDiscountOrder();

  }

}
