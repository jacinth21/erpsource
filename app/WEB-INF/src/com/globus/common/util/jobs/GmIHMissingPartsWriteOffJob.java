package com.globus.common.util.jobs;

import org.quartz.JobDataMap;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmIHMissingPartsWriteOffJob extends GmActionJob {

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    // TODO Auto-generated method stub
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);

    gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_sav_ihtxn_writeoff", 0);
    gmDBManager.execute();
    gmDBManager.commit();

  }


}
