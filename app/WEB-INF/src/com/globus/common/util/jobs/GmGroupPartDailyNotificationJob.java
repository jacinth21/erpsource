/**
 * FileName    : GmReceiveRequestsEmailJob.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Mar 24, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
 
 
import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties; 
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

 
public class GmGroupPartDailyNotificationJob extends GmActionJob {

	public static final String TEMPLATE_NAME = "GmGroupPartDailyNotificationJob";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
		GmDBManager gmDBManager = new GmDBManager();
		log.debug("jobDataMap = " + jobDataMap);
		
		gmDBManager.setPrepareString("gm_pkg_group_part_notification.gm_fch_part_add", 0);
		 
		gmDBManager.execute();  
		
		gmDBManager.setPrepareString("gm_pkg_group_part_notification.gm_fch_group_publish", 0);
		 
		gmDBManager.execute();   

		gmDBManager.setPrepareString("gm_pkg_group_part_notification.gm_fch_part_delete", 0);
				 
		gmDBManager.execute();  
		
		gmDBManager.close();
		 
	}
	
	 


}
