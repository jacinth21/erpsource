/**
 * FileName : GmScanShippedStatusJob.java Description : This Job is used to update the Status Flag
 * as '40' in t907_shipping_info table at one shot for the Many Transactions which are ready to Ship
 * out. Author : HReddi Date : Jan 06, 2014 Copyright : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.fedex.client.GmShipCompletionClient;
import com.globus.common.fedex.client.GmShipCompletionInterface;
import com.globus.common.fedex.client.GmUPSTrackingClient;
import com.globus.operations.shipping.beans.GmShipScanReqRptBean;
import com.globus.operations.shipping.beans.GmShipScanReqTransBean;
import com.globus.operations.shipping.beans.GmShipTrackingInfoBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmScanShippedStatusJob extends GmActionJob {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    log.debug("Enterring into the GmScanShippedStatusJob = " + jobDataMap);
    gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    HashMap hmParam = GmCommonClass.parseNullHashMap((HashMap) jobDataMap.get("HMPARAM"));
    HashMap hmFinal = new HashMap();
    ArrayList alExceptionLists = new ArrayList();
    GmUPSTrackingClient gmUPSTrackingClient = new GmUPSTrackingClient();
    String strTransID = "";
    String strTransException = "";
    // This strShipJobParam(having the value as "Y") parameter is ment for the job runs at 20:30 as
    // final one after FedEx pickup the Shipments.
    String strShipJobParam = GmCommonClass.parseNull((String) hmParam.get("SHIPFINAL"));
    String strShipCarrier = GmCommonClass.parseNull((String) hmParam.get("CAREER"));
    if (strShipCarrier.equalsIgnoreCase("UPS")) {
      // This method generate the Xml File for UPS. File having Ship out transaction details.
      gmUPSTrackingClient.generateUPSTrackingFile();
    }
    String strClassName =
        GmCommonClass.parseNull(GmCommonClass.getShipCarrierString(strShipCarrier
            + "_COMPLETION_CLASS"));
    GmShipCompletionInterface shipCmpClient = GmShipCompletionClient.getInstance(strClassName);
    String strShipOutException =
        "<B>The following transaction(s) are failed during shipout process: </B> <BR/>";
    hmFinal = shipComplete(shipCmpClient, strShipJobParam, strShipCarrier);
    alExceptionLists = GmCommonClass.parseNullArrayList((ArrayList) hmFinal.get("ALEXCEPTIONLIST"));

    int iErrSize = alExceptionLists.size();
    log.debug("alExceptionLists ===== " + alExceptionLists);
    Exception ex = (Exception) hmFinal.get(GmJob.EXCEPTION);
    String strFinalException =
        strShipOutException
            + "<BR/><table border='1' cellspacing='0' cellpadding='0'><tr><td><B>Transaction ID</B></td><td><B>Exception Message</B></td></tr>";
    if (iErrSize > 0) {
      HashMap hmValues = new HashMap();
      for (Iterator iter = alExceptionLists.iterator(); iter.hasNext();) {
        hmValues = (HashMap) iter.next();
        strTransID = GmCommonClass.parseNull((String) hmValues.get("TRANSID"));
        strTransException = GmCommonClass.parseNull((String) hmValues.get("ERRMSG"));
        strFinalException +=
            "<tr><td width='150'>" + strTransID + "</td><td>" + strTransException + "</td></tr>";
      }
    }
    strFinalException = strFinalException + "</table>";

    HashMap hmParams = new HashMap();
    ArrayList alBody = new ArrayList();
    String strStartTime = getJobStartTime();
    String strEndTime = getJobEndTime();
    hmParams.put("EXCEPTION", strFinalException);
    hmParams.put("JOBNAME", "GmScanShippedStatusJob");
    hmParams.put("ENDTIME", strEndTime);
    hmParams.put("STARTTIME", strStartTime);
    hmParams.put("STATUS", "Fail");
    hmParams.put("COMPANY_ID", gmDataStoreVO.getCmpid());
    alBody.add(hmParams);
    if (iErrSize > 0) {
      GmCommonClass.sendEmailFromTemplate("GmScanShippedStatus", alBody, hmParams);
      throw ex;
    }
  }

  /**
   * @param strShipJobParam
   * @return HashMap
   * @throws Exception
   */
  public HashMap shipComplete(GmShipCompletionInterface shipCmpClient, String strShipJobParam,
      String strShipCarrier) throws Exception {
    Boolean btrckFl;
    HashMap hmReturn = new HashMap();
    ArrayList alExceptionList = new ArrayList();
    ArrayList alReadyToPick = new ArrayList();
    ArrayList alExcepList = new ArrayList();
    ArrayList alCommonExcpList = new ArrayList();
    GmShipScanReqRptBean gmShipScanReqRptBean = new GmShipScanReqRptBean(gmDataStoreVO);
    GmShipTrackingInfoBean gmShipTrackingInfoBean = new GmShipTrackingInfoBean();
    GmShipScanReqTransBean gmShipScanReqTransBean = new GmShipScanReqTransBean(gmDataStoreVO);
    String strUserID = ""; // Manual Load
    String strRefID = "";
    String strShipMode = "";
    // To skip the validation if the tracking number is same
    String strAutoShipJobFl = "Y";
    alReadyToPick =
        GmCommonClass
            .parseNullArrayList(gmShipScanReqRptBean.fetchReadyToPickTrans(strShipCarrier));
    if (alReadyToPick.size() > 0) {
      for (Iterator iter = alReadyToPick.iterator(); iter.hasNext();) {
        try {
          HashMap hmDetails = new HashMap();
          hmDetails = (HashMap) iter.next();
          strRefID = GmCommonClass.parseNull((String) hmDetails.get("TRANSID"));
          strShipMode = GmCommonClass.parseNull((String) hmDetails.get("SHIPMODE"));
          if (!strShipJobParam.equals("Y")) {
            btrckFl = shipCmpClient.complete(hmDetails);
          }
          hmDetails.put("TRANSID", strRefID);
          hmDetails.put("USERID", strUserID);
          hmDetails.put("AUTOSHIPFL", strAutoShipJobFl);
          
          gmShipScanReqTransBean.saveShipoutTrans(hmDetails);
        } catch (Exception e) {
          log.error("Exception in job body: " + e.getMessage() + " and exception strRefID----"
              + strRefID);
          hmReturn.put(GmJob.EXCEPTION, e);
          HashMap hmRefValue = new HashMap();
          hmRefValue.put("TRANSID", strRefID);
          hmRefValue.put("ERRMSG", e.getMessage());
          alExceptionList.add(hmRefValue);
          hmReturn.put("ALEXCEPTIONLIST", alExceptionList);
        }
      }
    }
	
	try {
		//Get the Part Exception List and add it in the Common Exception List
		if(alExceptionList.size() > 0){
			alCommonExcpList.addAll(alExceptionList);
		}
		//Get the Insert Exception List and add it in the Common Exception List
		alExcepList = gmShipScanReqRptBean.getInsertException();
		if(alExcepList.size()> 0){
			alCommonExcpList.addAll(alExcepList);
		}
		//Put Common Exception List in the Hashmap
		hmReturn.put("ALEXCEPTIONLIST", alCommonExcpList);
    } catch (Exception exception) {
		if(alExceptionList.size() > 0){
			hmReturn.put("ALEXCEPTIONLIST", alExceptionList);
		}
		
	}
    return hmReturn;
  }

}
