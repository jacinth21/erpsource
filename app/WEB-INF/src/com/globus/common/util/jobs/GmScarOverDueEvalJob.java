/**
 * FileName    : GmScarOverDueEvalJob.java
 * Description :
 * Author      : Jignesh Shah
 * Date        : Oct 24, 2013 
 * Copyright   : Globus Medical Inc
 */

package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.common.util.jobs.GmJob;
import com.globus.quality.capa.beans.GmCapaTransBean;


public class GmScarOverDueEvalJob extends GmActionJob {

	public static final String TEMPLATE_NAME = "GmScarOverDueEvalJob";
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
		String strTo = "";
		String strCc = "";
		String strScarId = "";
		String strResponseDt = "";
		String strMessageBody = "";
		String strMimeType = "";
		String strFrom = "";
		String strLog = "";
		
		ArrayList  alResult = new ArrayList();
		
		GmCapaTransBean gmCapaTransBean = new GmCapaTransBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		
		GmDBManager gmDBManager = new GmDBManager();
		log.debug("jobDataMap = " + jobDataMap);
		gmDBManager.setPrepareString("gm_pkg_qa_scar_rpt.gm_fch_scar_eval_info", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alResult = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1)));
		gmDBManager.close();
		HashMap hmScarOverDueDtls = new HashMap();
		HashMap hmParams = new HashMap();
		HashMap hmReturn = new HashMap();
		
		for(Iterator iter = alResult.iterator(); iter.hasNext();)
		{
			try
			{
				hmScarOverDueDtls = (HashMap)iter.next();
				
				String strDay = (String)hmScarOverDueDtls.get("DAY");
				
				GmEmailProperties emailProps = new GmEmailProperties();
				
				strScarId = GmCommonClass.parseNull((String)hmScarOverDueDtls.get("SCARID"));
				strResponseDt = GmCommonClass.parseNull((String)hmScarOverDueDtls.get("RESPONSEDT"));
				
				if(!strScarId.equals("")){					
	                
					strTo = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."+ GmEmailProperties.TO));
					strCc = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."+ GmEmailProperties.CC));
					strFrom = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."+ GmEmailProperties.FROM));
					strMimeType = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));
					strLog = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + "LogReason20"));
					strMessageBody = "This is just a reminder that the above referenced SCAR response is still pending. The response is due back on " +strResponseDt+ ".";
					if (strDay.equals("30")){
						strMessageBody = "The response to the above referenced SCAR is overdue. The response was due back on " +strResponseDt+ ".";
						strLog = GmCommonClass.parseNull(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + "LogReason30"));
					}
					
					hmScarOverDueDtls.put("SCARMESSAGE", strMessageBody);
					
					emailProps.setMimeType(strMimeType);
					emailProps.setRecipients(strTo);
					emailProps.setCc(strCc);
					emailProps.setSender(strFrom);
					emailProps.setSubject(strScarId);
					
					GmJasperMail jasperMail = new GmJasperMail();
					jasperMail.setJasperReportName("/GmScarOverDueEvalJob.jasper");
					jasperMail.setAdditionalParams(hmScarOverDueDtls);
					jasperMail.setReportData(null);
					jasperMail.setEmailProperties(emailProps);
					hmReturn = jasperMail.sendMail();
					
					if (!strLog.equals("")) {
						GmDBManager gmLogDBManager = new GmDBManager();
                        log.debug("The log value is  ****** "+strLog+ " ***** "+strScarId);
                        gmCommonBean.saveLog(gmLogDBManager, strScarId, strLog, "30301", "4000318");//4000318-->Code id for Scar Log
                        gmLogDBManager.commit();
                  }
				}
				
			}
			catch(Exception e)
			{
				log.error("Exception in job body: " + e.getMessage());
				hmReturn.put(GmJob.EXCEPTION, e);
			}
			Exception ex = (Exception)hmReturn.get(GmJob.EXCEPTION);
			if(ex != null)
			{
				log.error("Exception in sendJasperMail");
				setJobStatus(GmJob.JOB_STATUS_FAIL);
				setJobException(GmCommonClass.getExceptionStackTrace(ex,"<br>"));
				setAdditionalParam((String)hmReturn.get("ORIGINALMAIL"));
				sendJobExceptionEmail();
				throw ex;
			}
		}
		
	}
}
