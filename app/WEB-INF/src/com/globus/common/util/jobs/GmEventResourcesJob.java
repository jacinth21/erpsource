/**
 * FileName : GmEventResourcesJob.java Description : Author : Jignesh Shah Copyright : Globus
 * Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author sthadeshwar
 * 
 */
public class GmEventResourcesJob extends GmLoanerRequestsEmailJob {

  public static final String TEMPLATE_NAME = "GmEventResourcesJob";
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    log.debug("jobDataMap = " + jobDataMap);
    ArrayList alCaseInfoId = new ArrayList();
    ArrayList alShippedSetsInfo = new ArrayList();
    ArrayList alShippedItemsInfo = new ArrayList();

    HashMap hmRepDetails = new HashMap();
    HashMap hmParams = new HashMap();
    HashMap hmReturn = new HashMap();
    /*
     * String strPath = GmCommonClass.getString("GMSUBREPORTLOCATION");
     * log.debug("strPath::"+strPath); hmParams.put("SUBREPORT_DIR", strPath);
     */
    // hmParams = getShippingDetails();

    /* Getting all the rep id's who have submitted requests today */
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_end_list", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alCaseInfoId = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.commit();
    for (Iterator iter = alCaseInfoId.iterator(); iter.hasNext();) {
      try {
        hmParams.put("GMCSPHONE", GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSPhone"));
        hmParams.put("GMCSEMAIL", GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSEmail"));

        hmRepDetails = (HashMap) iter.next();
        String stCaseInfoID = (String) hmRepDetails.get("CASE_INFO_ID");
        log.debug("Fetching requests for  " + stCaseInfoID);
        /* Getting request details for each rep */
        gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_shipped_sets", 3);
        gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
        gmDBManager.setInt(1, Integer.parseInt(stCaseInfoID));
        gmDBManager.setString(2, "N");
        gmDBManager.execute();
        alShippedSetsInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

        gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_shipped_items", 2);
        gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
        gmDBManager.setInt(1, Integer.parseInt(stCaseInfoID));
        gmDBManager.execute();
        alShippedItemsInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

        gmDBManager.commit();
        hmParams.put("SHIPPEDSETS", alShippedSetsInfo);
        hmParams.put("SHIPPEDITEMS", alShippedItemsInfo);
        hmParams.putAll(hmRepDetails);

        GmEmailProperties emailProps = getEmailProperties(TEMPLATE_NAME, "");
        String strSubject = emailProps.getSubject();
        // strSubject = GmCommonClass.replaceAll(strSubject, "#<RQDATE>", df.format(new Date()));
        emailProps.setRecipients((String) hmRepDetails.get("LOANTOEMAILID"));
        emailProps.setSubject(strSubject);
        emailProps.setReplyTo(GmCommonClass.parseNull(GmCommonClass
            .getString("EVENTREPLYTOEMAILID")));

        if (alShippedSetsInfo.size() > 0 || alShippedItemsInfo.size() > 0) {
          GmJasperMail jasperMail = new GmJasperMail();
          jasperMail.setJasperReportName("/GmEventResources.jasper");
          jasperMail.setAdditionalParams(hmParams);
          jasperMail.setReportData(null);
          jasperMail.setEmailProperties(emailProps);
          hmReturn = jasperMail.sendMail();
        }

      } catch (Exception e) {
        log.error("Exception in job body: " + e.getMessage());
        hmReturn.put(GmJob.EXCEPTION, e);
      }
      Exception ex = (Exception) hmReturn.get(GmJob.EXCEPTION);
      if (ex != null) {
        log.error("Exception in sendJasperMail");
        setJobStatus(GmJob.JOB_STATUS_FAIL);
        setJobException(GmCommonClass.getExceptionStackTrace(ex, "<br>"));
        setAdditionalParam((String) hmReturn.get("ORIGINALMAIL"));
        sendJobExceptionEmail();
        throw ex;
      }
    }
  }



}
