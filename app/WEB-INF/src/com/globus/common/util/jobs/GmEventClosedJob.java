package com.globus.common.util.jobs;

import org.quartz.JobDataMap;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmEventClosedJob extends GmActionJob {

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_txn.gm_sav_event_closed", 0);
    gmDBManager.execute();
    gmDBManager.commit();
  }
}
