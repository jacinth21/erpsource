/**
 * FileName    : GmGroupPartMapJob.java 
 * Description : This class is called by the Quartz job to send email notification if there is 
 * any new group published or any new part added to the existing group
 * Author      : gpalani (Modified)
 * Date        : May 20, 2018
 * Copyright   : Globus Medical Inc
 */

package com.globus.common.util.jobs;

import org.apache.commons.validator.EmailValidator;
import org.quartz.JobDataMap;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.db.GmDBManager;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.ArrayList;

import oracle.jdbc.driver.OracleTypes;

public class GmGroupPartMapJob extends GmActionJob  {

	public static final String TEMPLATE_NAME = "GmGroupPartMapJob";
	public void execute(JobDataMap jobDataMap) throws Exception  {
	 
			ArrayList  alGroup = new ArrayList();
			ArrayList  alPart = new ArrayList();
			ArrayList  alAccount = new ArrayList();
			HashMap hmParams = new HashMap();
			HashMap hmReturn = new HashMap();
			GmDBManager gmDBManager = new GmDBManager((getGmDataStoreVO(jobDataMap)));
			// Calling the new procedure to log the Group part email notification job.
			gmDBManager.setPrepareString("gm_pkg_group_part_email.gm_group_part_map_email_flag ", 1);
			gmDBManager.setString(1,"106620"); //106620 - Group Part Email Notification Started: GRPFLA
			gmDBManager.execute();
			
			gmDBManager.setPrepareString("gm_pkg_group_part_email.gm_new_group_part_map_email", 4);	
			gmDBManager.setString(1,"106620"); //106620 - Group Part Email Notification Started. Code Group: GRPFLA
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
			gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
			gmDBManager.execute();
			
			alGroup = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
			alPart = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
			alAccount = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
				
			
			hmParams.put("ALGROUP",alGroup);
			hmParams.put("ALPART",alPart);
			hmParams.put("ALACCOUNT",alAccount);
			
			try{	
				GmEmailProperties emailProps = getEmailProperties(TEMPLATE_NAME);
				String strSubject = emailProps.getSubject();
				emailProps.setRecipients(GmCommonClass.getEmailProperty("GmGroupPartMapJob." + GmEmailProperties.TO));
				emailProps.setSubject(strSubject);
				emailProps.setCc(GmCommonClass.getEmailProperty("GmGroupPartMapJob." + GmEmailProperties.CC));
					
				GmJasperMail jasperMail = new GmJasperMail();
				jasperMail.setJasperReportName("/GmGroupPartMapJob.jasper");
				jasperMail.setAdditionalParams(hmParams);
				jasperMail.setReportData(null);
				jasperMail.setEmailProperties(emailProps);
				hmReturn = jasperMail.sendMail();
				
				gmDBManager.setPrepareString("gm_pkg_group_part_email.gm_group_part_map_email_flag", 1);
				gmDBManager.setString(1,"106621"); //106621 - Group Part Email Sent. Code Group: GRPFLA
				gmDBManager.execute();

				
			}catch(Exception e){
				gmDBManager.close();
				log.error("Exception in job body: " + e.getMessage());
				hmReturn.put(GmJob.EXCEPTION, e);
			}
				Exception ex = (Exception)hmReturn.get(GmJob.EXCEPTION);
				if(ex != null){
					log.error("Exception in sendJasperMail ");
					setJobStatus(GmJob.JOB_STATUS_FAIL);
					setJobException(GmCommonClass.getExceptionStackTrace(ex,"<br>"));
					setAdditionalParam((String)hmReturn.get("ORIGINALMAIL"));
					sendJobExceptionEmail();
					gmDBManager.setPrepareString("gm_pkg_group_part_email.gm_group_part_map_email_flag", 1);
					gmDBManager.setString(1,"106622"); //106622 - Group Part Email Job Failed. Code Group: GRPFLA
					gmDBManager.execute();

					throw ex;
				}
				gmDBManager.commit();
			}
	
	//GmEmailProperties will helps to get values of TO and CC mail from EmailTemplateProperties
	public GmEmailProperties getEmailProperties(String strTemplate){
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
		emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT));
		emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MIME_TYPE));
		emailProps.setCc(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.CC));
		emailProps.setRecipients(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.TO));
		return emailProps;
	}
}


