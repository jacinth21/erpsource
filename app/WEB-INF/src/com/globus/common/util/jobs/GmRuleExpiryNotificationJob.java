/**
 * FileName    : GmReceiveRequestsEmailJob.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Mar 24, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author Xun
 *
 */
public class GmRuleExpiryNotificationJob extends GmActionJob {

	public static final String TEMPLATE_NAME = "GmRuleExpiryNotificationJob";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
	    	GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		log.debug("jobDataMap = " + jobDataMap);
		ArrayList  alRuleDtls = new ArrayList(); 

		HashMap hmRuleDetails = new HashMap();
		HashMap hmParams = new HashMap();
		HashMap hmReturn = new HashMap();
 
		 
		gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_rule_expiry_details", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();  
		alRuleDtls = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1));
		gmDBManager.close();
		for(Iterator iter = alRuleDtls.iterator(); iter.hasNext();)
		{
			try
			{ 
				hmRuleDetails = (HashMap)iter.next();
				String strRuleName = (String)hmRuleDetails.get("RULE_NAME");
				log.debug("Fetching strRuleName " + strRuleName);
				 
				hmParams.putAll(hmRuleDetails);

				GmEmailProperties gmEmailProperties = getEmailProperties(TEMPLATE_NAME);
				String strMessage = gmEmailProperties.getMessage();
				strMessage = GmCommonClass.replaceAll(strMessage, "#<RULE_NAME>", (String)hmParams.get("RULE_NAME"));
				strMessage = GmCommonClass.replaceAll(strMessage, "#<EXPDATE>", (String)hmParams.get("EXPDATE"));
				gmEmailProperties.setRecipients((String)hmParams.get("TO_EMAIL"));
				gmEmailProperties.setCc((String)hmParams.get("CC_EMAIL"));
				gmEmailProperties.setMessage(strMessage+"<br><br><br><br>"+gmEmailProperties.getMessageFooter()); 
				GmCommonClass.sendMail(gmEmailProperties);
				
 		}
			catch(Exception e)
			{
				String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
				log.error("Exception e "+strErrorMsg);
				hmReturn.put(GmJob.EXCEPTION, e);
			}
			Exception ex = (Exception)hmReturn.get(GmJob.EXCEPTION);
			if(ex != null)
			{
				log.error("Exception in sendJasperMail");
				setJobStatus(GmJob.JOB_STATUS_FAIL);
				setJobException(GmCommonClass.getExceptionStackTrace(ex,"<br>"));
				setAdditionalParam((String)hmReturn.get("ORIGINALMAIL"));
				sendJobExceptionEmail();
				throw ex;
			}
		}
	}
	
	
	public GmEmailProperties getEmailProperties(String strTemplate)
	{
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
		emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT));
		emailProps.setMessage(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MESSAGE));
		emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MIME_TYPE));
		emailProps.setMessageFooter(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MESSAGE_FOOTER));
		return emailProps;
	}


}
