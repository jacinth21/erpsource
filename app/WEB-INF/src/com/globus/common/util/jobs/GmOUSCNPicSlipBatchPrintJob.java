package com.globus.common.util.jobs;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.quartz.JobDataMap;

import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.util.jobs.GmActionJob;

public class GmOUSCNPicSlipBatchPrintJob extends GmActionJob {

	public void execute(JobDataMap jobDataMap) throws Exception {	
		
		GmBatchBean gmBatchBean = new GmBatchBean();
		ArrayList alCNList = new ArrayList();
		ArrayList alResult = new ArrayList();
		HashMap hmCNPickSlip = new HashMap();
		HashMap hmLoop = new HashMap();
		HashMap hmStatus = new HashMap();
		String resourcename = "GmConsignItemServlet";
		String strPara = "&hAction=PicSlip&type=savePDFfile&ruleSource=50181";
		String strAddtlPara = "";
		String strPDFFileName = "";
		String strConsignID = "";
		String strCNPickSlipPDFLocation = "";
		String strBatchID = "";
		int intCNListLength = 0;
		int intArLength = 0;		
		
		// Fetch the OUS consign details.
		alResult = GmCommonClass.parseNullArrayList((ArrayList) gmBatchBean.fetchOUSReplenishCN());
		intArLength = alResult.size();
		log.error("intArLength is:" + intArLength);
		
		if (intArLength > 0) {
			// Save the Batch Details.
			String strConsignIDs = "";
			for (int i = 0; i < intArLength; i++) {
				hmLoop = (HashMap) alResult.get(i);
				strConsignID = GmCommonClass.parseNull((String) hmLoop.get("CID"));
				strConsignIDs +=  strConsignID + "|";
			}			
			hmCNPickSlip.put("INPUTSTRING", strConsignIDs);
			hmCNPickSlip.put("USERID", "30301");   // JOB - Manual load User ID
			strBatchID = gmBatchBean.saveCNPickSlipPrint(hmCNPickSlip);
			log.error("strBatchID is>>>> " + strBatchID);
			// Fetch the Batch, consign Details.
			hmCNPickSlip.put("BATCHID", strBatchID);
			hmCNPickSlip.put("BATCHTYPE", "1701");  // CN pick slip Batch
			hmCNPickSlip.put("BATCHSTATUS", "18743"); // Process in progress			
			strCNPickSlipPDFLocation = GmCommonClass.getString("BATCH_CN_PICKSLIP_PRINT");
			alCNList = GmCommonClass.parseNullArrayList((ArrayList) gmBatchBean.fetchCNBatchDetailsReport(hmCNPickSlip));
			intCNListLength = alCNList.size();
			String strExceptionStr = "";
			log.error("intCNListLength is:" + intCNListLength);
			if (intCNListLength > 0) {
				for (int i = 0; i < intCNListLength; i++) {
					strAddtlPara = "";
					hmLoop = (HashMap) alCNList.get(i);
					strBatchID = GmCommonClass.parseNull((String) hmLoop.get("BATCHID"));
					String strConsign = GmCommonClass.parseNull((String) hmLoop.get("REFID"));
					
					try{
						
					strAddtlPara = "&hConsignId=" + strConsign;
					hmLoop.put("RESOURCE", resourcename);
					hmLoop.put("PARAMETER", strPara + strAddtlPara);

					boolean bPDFFileCreated = false;
					strPDFFileName = strCNPickSlipPDFLocation + "\\" + strConsign + ".pdf";
					hmLoop.put("PDFFILENAME", strPDFFileName);
					hmLoop.put("REFTYPE", "1701"); // CN pick slip Batch print type
					log.error("strPDFFileName is:" + strPDFFileName);
					// save the PDF file
					bPDFFileCreated = gmBatchBean.saveCNPickSlipasPDF(hmLoop);	
					log.error("bPDFFileCreated is:" + bPDFFileCreated);
					// to save the status for files created
					hmLoop.put("ACTIONTYPE", "1801"); // 1801 - File
					gmBatchBean.saveBatchLog(hmLoop);
					log.error("saveBatchLog is done"  );
					hmLoop.put("RULEID", "OUS_CN");
					// to print the PDF file
					gmBatchBean.batchPrint(hmLoop);
					log.error("batchPrint is done"  );
					// to save the status for printed successfully
					hmLoop.remove("ACTIONTYPE");
					hmLoop.put("ACTIONTYPE", "1802"); // 1802 Print
					gmBatchBean.saveBatchLog(hmLoop);
					log.error("saveBatchLog is done"  );
					// after print the PDF file to be deleted.
					File newPDFFile = new File(strPDFFileName);
					if(newPDFFile.exists()){
						newPDFFile.deleteOnExit();
					}
                }
                catch(Exception ex)
                {
                    strExceptionStr += " Batch ID : " + strBatchID + " strConsign : " + strConsign + " Failed. Error Msg : " +ex.getMessage();
                    log.error("Exception is:" + strExceptionStr);
                } 
				hmStatus.put("BATCHID", strBatchID);
				hmStatus.put("USERID", "30301"); 
				gmBatchBean.saveBatchStatus(hmStatus);
				}
			}
		}
	}

}
