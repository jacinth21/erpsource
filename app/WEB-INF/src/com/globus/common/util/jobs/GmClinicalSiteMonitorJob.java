/**
 * FileName    : GmNCMREvaluationDueEmailJob.java 
 * Description :
 * Author      : Himanshu
 * Date        : Mar 5, 2010 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
 
import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties; 
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.clinical.beans.GmClinicalSiteMonitorBean;

/**
 * @author Himanshu
 *
 */
public class GmClinicalSiteMonitorJob extends GmActionJob {
	public static final String TEMPLATE_NAME = "SiteMonitorVisitReminderEmail";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	GmClinicalSiteMonitorBean gmClinicalSiteMonitorBean = new GmClinicalSiteMonitorBean();
	
	public void execute(JobDataMap jobDataMap) throws Exception 
	{
		GmClinicalSiteMonitorBean gmClinicalSiteMonitorBean = new GmClinicalSiteMonitorBean();
		updateMonitorStatus(gmClinicalSiteMonitorBean);
		log.debug("Inside sendReminderEmail starts");
		sendReminderEmail(gmClinicalSiteMonitorBean);
		
	}
	
	public void updateMonitorStatus(GmClinicalSiteMonitorBean gmClinicalSiteMonitorBean)  throws Exception {
		log.debug("Inside updateMonitorStatus");
		gmClinicalSiteMonitorBean.saveMonitorStatus();
		log.debug("Inside updateMonitorStatus done");
	}
	public void sendReminderEmail(GmClinicalSiteMonitorBean gmClinicalSiteMonitorBean)  throws Exception {
		log.debug("Inside updateMonitorStatus");
		gmClinicalSiteMonitorBean.sendReminderEmail();
	}
	
	
	


}
