package com.globus.common.util.jobs;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmDataDownloadTransBean;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.db.GmDBManager;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.jobs.GmJob;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAPInvMatchAutomationJob extends GmActionJob {

	GmDataStoreVO gmDataStoreVO;

	public void execute(JobDataMap jobDataMap) throws Exception {
		gmDataStoreVO = getGmDataStoreVO(jobDataMap);
		String strBatchid = "";
		HashMap hmTemp = new HashMap();
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		String strcomid = gmDataStoreVO.getCmpid();
		hmReturn = updateInvoiceSts(gmDBManager, strcomid);
		hmTemp.put("GMDBMANAGER", gmDBManager);
		strBatchid = (String) hmReturn.get("BATCHID");
		int intBatchID = Integer.parseInt(strBatchid);		
		ArrayList arraysize = (ArrayList) hmReturn.get("invbatchdetails");
		log.debug("ARRAYSIZEVALUE" + arraysize.size() + " intBatchID--> "+intBatchID);
		if (arraysize.size() > 0 && !strBatchid.equals("0")) {
			GmDataDownloadTransBean gmDataDownloadTransBean = new GmDataDownloadTransBean(getGmDataStoreVO(jobDataMap));
			gmDataDownloadTransBean.prepareDownloadFile(hmTemp, intBatchID);
			// ArrayList al = (ArrayList) hmReturn.get("invbatchdetails");
			sendEmail(hmReturn);
		} else {
			log.debug("No Records Found");
		}
	}

/**
	 * fetch the data
	 * 
	 * @param strcomid
	 * 
	 * @return HashMap
	 * @method This method will get the values of accepted quantity greater than
	 *         order quantity.
*/
	public HashMap updateInvoiceSts(GmDBManager gmDBManager, String strcomid) throws Exception{
		String strComid = strcomid;		
		ArrayList alList = new ArrayList();
		gmDBManager.setPrepareString("gm_pkg_ap_invoice_match_jobs.gm_update_invoice_sts", 4);
		gmDBManager.setString(1, strComid);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
		gmDBManager.registerOutParameter(4, java.sql.Types.CHAR);
		gmDBManager.execute();
		String strPaymentTotal1 = GmCommonClass.parseNull(gmDBManager.getString(4));
		log.debug("strPaymentTotal1--> "+strPaymentTotal1);
		BigDecimal strPaymentTotal = new BigDecimal(strPaymentTotal1);
		String strBatchID = GmCommonClass.parseZero(gmDBManager.getString(3));
		log.debug("strBatchID--> "+strBatchID);
		if(!strBatchID.equals("0")){
			alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		}
		HashMap hmReturn = new HashMap();
		hmReturn.put("invbatchdetails", alList);
		hmReturn.put("STRPAYMENTTOTAL", strPaymentTotal);
		hmReturn.put("SIZE", alList.size());
		hmReturn.put("BATCHID", strBatchID);
		gmDBManager.commit();
		return hmReturn;
	}

	/**
	 * sendEmail
	 * 
	 * @throws Exception
	 * @method If any Exception Thrown,accpted quantity data greater than order
	 *         quantity Will be notified by an E-mail.
	 */
	public void sendEmail(HashMap invbatchdetails) throws Exception {
		HashMap hmReturn = new HashMap();
		String TEMPLATE_NAME = "GmAPInviceMatch";
		String StrMsgBody = GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."+ GmEmailProperties.MESSAGE_BODY);
		invbatchdetails.put("MSGBDY", StrMsgBody);
		ArrayList al = (ArrayList) invbatchdetails.get("invbatchdetails");
		String strEmailFooter = "**This is an automatically  generated email,please do not reply to this mail**";
		invbatchdetails.put("MSGEND", strEmailFooter);
		HashMap hmRepDet = new HashMap();
		try {
			GmEmailProperties emailProps = new GmEmailProperties();
			emailProps.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME+ "." + GmEmailProperties.FROM));
			emailProps.setRecipients(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "."+ GmEmailProperties.TO));
			emailProps.setSubject(GmCommonClass.getEmailProperty(TEMPLATE_NAME+ "." + GmEmailProperties.SUBJECT));
			emailProps.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME+ "." + GmEmailProperties.MIME_TYPE));
			GmJasperMail jasperMail = new GmJasperMail();
			jasperMail.setJasperReportName("/GmApInvoice.jasper");
			// jasperMail.setAdditionalParams(hmReturn1);
			jasperMail.setAdditionalParams(invbatchdetails);
			jasperMail.setReportData(al);
			jasperMail.setEmailProperties(emailProps);
			hmRepDet = jasperMail.sendMail();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
