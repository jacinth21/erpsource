/**
 * FileName : GmEventResourcesJob.java Description : Author : Jignesh Shah Copyright : Globus
 * Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author sthadeshwar
 * 
 */
public class GmEventMissingPartJob extends GmLoanerRequestsEmailJob {

  public static final String TEMPLATE_NAME = "GmEventMissingPartJob";
  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    log.debug("jobDataMap = " + jobDataMap);
    ArrayList alReqInfo = new ArrayList();
    ArrayList alSetsInfo = new ArrayList();
    ArrayList alRetSetsInfo = new ArrayList();
    ArrayList alItemsInfo = new ArrayList();

    HashMap hmReqDetails = new HashMap();
    HashMap hmParams = new HashMap();
    HashMap hmReturn = new HashMap();
    /*
     * String strPath = GmCommonClass.getString("GMSUBREPORTLOCATION");
     * log.debug("strPath::"+strPath); hmParams.put("SUBREPORT_DIR", strPath); //hmParams =
     * getShippingDetails();
     */

    /* Getting all the rep id's who have submitted requests today */
    gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_missing_part_event_list", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReqInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    for (Iterator iter = alReqInfo.iterator(); iter.hasNext();) {
      try {
        hmParams.put("GMEVENTMISSINGPHONE", GmCommonClass.getEmailProperty("GmCommon"
            + ".GlobusCSPhone"));
        hmParams.put("GMEVENTMISSINGEMAIL", GmCommonClass.getEmailProperty("GmEventMissingPartJob"
            + ".GlobusEmail"));

        hmReqDetails = (HashMap) iter.next();
        String strProdReqID = (String) hmReqDetails.get("PRODREQID");
        log.debug("Fetching requests for ProdReqID " + strProdReqID);

        gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_missing_part_event_sets", 2);
        gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
        gmDBManager.setInt(1, Integer.parseInt(strProdReqID));
        gmDBManager.execute();
        alSetsInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
        gmDBManager.close();

        gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_missing_parts", 2);
        gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
        gmDBManager.setInt(1, Integer.parseInt(strProdReqID));
        gmDBManager.execute();
        alItemsInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
        gmDBManager.close();

        gmDBManager.setPrepareString("gm_pkg_sm_eventbook_rpt.gm_fch_event_unreturn_sets", 2);
        gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
        gmDBManager.setInt(1, Integer.parseInt(strProdReqID));
        gmDBManager.execute();
        alRetSetsInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));

        gmDBManager.close();
        hmParams.put("ALMISSINGSETITEMLIST", alSetsInfo);
        hmParams.put("ALMISSINGITEMLIST", alItemsInfo);
        hmParams.put("ALSETLIST", alRetSetsInfo);
        hmParams.putAll(hmReqDetails);

        GmEmailProperties emailProps = getEmailProperties(TEMPLATE_NAME, "");
        String strSubject = emailProps.getSubject();
        emailProps.setRecipients((String) hmReqDetails.get("LOANTOEMAILID"));
        emailProps.setSubject(strSubject);
        emailProps.setReplyTo(GmCommonClass.parseNull(GmCommonClass
            .getString("EVENTREPLYTOEMAILID")));

        if (alSetsInfo.size() > 0 || alItemsInfo.size() > 0 || alRetSetsInfo.size() > 0) {
          GmJasperMail jasperMail = new GmJasperMail();
          jasperMail.setJasperReportName("/GmEventReturnsMissing.jasper");
          jasperMail.setAdditionalParams(hmParams);
          jasperMail.setReportData(null);
          jasperMail.setEmailProperties(emailProps);
          hmReturn = jasperMail.sendMail();
        }

      } catch (Exception e) {
        log.error("Exception in job body: " + e.getMessage());
        hmReturn.put(GmJob.EXCEPTION, e);
      }
      Exception ex = (Exception) hmReturn.get(GmJob.EXCEPTION);
      if (ex != null) {
        log.error("Exception in sendJasperMail");
        setJobStatus(GmJob.JOB_STATUS_FAIL);
        setJobException(GmCommonClass.getExceptionStackTrace(ex, "<br>"));
        setAdditionalParam((String) hmReturn.get("ORIGINALMAIL"));
        sendJobExceptionEmail();
        throw ex;
      }
    }
  }



}
