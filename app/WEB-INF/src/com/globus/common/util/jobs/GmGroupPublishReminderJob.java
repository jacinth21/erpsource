/**
 * FileName    : GmReceiveRequestsEmailJob.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Mar 24, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ResourceBundle;
 
import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties; 
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * @author Xun
 *
 */
public class GmGroupPublishReminderJob extends GmActionJob {

	public static final String TEMPLATE_NAME = "GmGroupPublishReminderJob";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
		GmDBManager gmDBManager = new GmDBManager();
		log.debug("jobDataMap = " + jobDataMap);
		ArrayList  alPublishDtls  = new ArrayList(); 

		HashMap hmPublishDetails = new HashMap();
		HashMap hmParams = new HashMap();
		HashMap hmReturn = new HashMap();
		HashMap hmTemp = new HashMap();
		String strOwner = "";
		String strGroupNM = "";
		String strCreatedDate = ""; 
		String strTempOwner = "";
		StringBuffer  strValue = new StringBuffer();
		int i = 0;
		 
		gmDBManager.setPrepareString("gm_pkg_pd_group_pricing.gm_fch_publish_reminder", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();  
		alPublishDtls = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1));
		gmDBManager.close();
		for(Iterator iter = alPublishDtls.iterator(); iter.hasNext();)
		{
			try
			{ 
				hmPublishDetails = (HashMap)iter.next(); 
				
				strOwner = (String)hmPublishDetails.get("OWNER");
				strGroupNM = (String) hmPublishDetails.get("GROUPNM");
				strCreatedDate =(String) hmPublishDetails.get("CREATED_DATE");  
				log.debug("Fetching strGroupNM " + strGroupNM); 
				hmParams.putAll(hmPublishDetails);

				GmEmailProperties gmEmailProperties = getEmailProperties(TEMPLATE_NAME);
			 	String strMessage = gmEmailProperties.getMessage();
			 	strValue.append("Group ");
			 	strValue.append(strGroupNM);
			 	strValue.append(" which was created on ");
			 	strValue.append(strCreatedDate);
			 	strValue.append("<BR> "); 
				
				strMessage = GmCommonClass.replaceAll(strMessage, "#<MESSAGES>", strValue.toString()); 
				gmEmailProperties.setRecipients((String)hmParams.get("TO_EMAIL")); 
				gmEmailProperties.setCc((String)hmParams.get("CC_EMAIL"));
			//	gmEmailProperties.setMessage(strMessage+"<br><br><br><br>"+gmEmailProperties.getMessageFooter()); 
				
				//check user then iterate, reset strmessage
				 
			      if(i < alPublishDtls.size()&& i!= alPublishDtls.size()-1)
			      {
			        hmTemp = (HashMap)(alPublishDtls.get(i+1));
			        strTempOwner = (String)hmTemp.get("OWNER");
			            if(!strTempOwner.equals(strOwner))
			            {	log.debug("strMessage>>>>>>>>>>> " + strMessage); 
			            	gmEmailProperties.setMessage(strMessage);
			            	GmCommonClass.sendMail(gmEmailProperties);
			            	strValue.setLength(0);
			            }
			      }
			      else
			      {        	gmEmailProperties.setMessage(strMessage);
			            	GmCommonClass.sendMail(gmEmailProperties);
			            	strValue.setLength(0); 
			       }
			      i++;
			//	GmCommonClass.sendMail(gmEmailProperties);
				
 		}
			catch(Exception e)
			{
				log.error("Exception in job body: " + e.getMessage());
				hmReturn.put(GmJob.EXCEPTION, e);
			}
			Exception ex = (Exception)hmReturn.get(GmJob.EXCEPTION);
			if(ex != null)
			{ 
				setJobStatus(GmJob.JOB_STATUS_FAIL);
				setJobException(GmCommonClass.getExceptionStackTrace(ex,"<br>"));
				setAdditionalParam((String)hmReturn.get("ORIGINALMAIL"));
				sendJobExceptionEmail();
				throw ex;
			}
		}
	}
	
	
	public GmEmailProperties getEmailProperties(String strTemplate)
	{
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
		emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT));
		emailProps.setMessage(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MESSAGE));
		emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MIME_TYPE));
	//	emailProps.setMessageFooter(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MESSAGE_FOOTER));
		return emailProps;
	}


}
