package com.globus.common.util.jobs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.logon.auth.GmLDAPManager;
import com.globus.logon.beans.GmUserBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSyncPortalUsersWithADJob extends GmActionJob {
  // Instantiating the Logger
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.
  ArrayList alUsersStillActive = new ArrayList();

  HashMap hmUsersNotAvlInADEmail = new HashMap();
  HashMap hmUsersToBeInactivatedEmail = new HashMap();
  List alActiveUsers = new ArrayList();

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    String strCompanyId = gmDataStoreVO.getCmpid();
    HashMap hmParam = GmCommonClass.parseNullHashMap((HashMap) jobDataMap.get("HMPARAM"));
    String strLdapId = GmCommonClass.parseNull((String) hmParam.get("LDAPID"));
    GmLDAPManager gmLDAPManager = new GmLDAPManager(strLdapId);
    GmUserBean gmUserBean = new GmUserBean(gmDataStoreVO);
    alActiveUsers = gmUserBean.fetchActiveUsers(strLdapId);
    log.debug("alUsersActiveinPortal: " + alActiveUsers.size());
    HashMap hmActiveUser = new HashMap();
    Map hmLDAPUser = new HashMap();

    String strUserId = "";
    String strUserName = "";

    Iterator itrActiveUsers = alActiveUsers.iterator();
    while (itrActiveUsers.hasNext()) {
      hmActiveUser = (HashMap) itrActiveUsers.next();
      strUserId = GmCommonClass.parseNull((String) hmActiveUser.get("USERID"));
      strUserName = GmCommonClass.parseNull((String) hmActiveUser.get("USERNAME"));
      hmLDAPUser = GmCommonClass.parseNullHashMap((HashMap) gmLDAPManager.getUser(strUserName));

      if (hmLDAPUser.size() == 0) {
        gmUserBean.saveInActivateDBUser(strUserId);
        hmUsersNotAvlInADEmail.put(strUserId, strUserName);
      } else {
        String strUserAccountControl =
            GmCommonClass.parseNull((String) hmLDAPUser.get("USERACCOUNTCONTROL"));
        String strDescription =
            GmCommonClass.parseNull((String) hmLDAPUser.get("DESCRIPTION")).toLowerCase();

        if (strUserAccountControl.equalsIgnoreCase("inactive")
            || strDescription.contains("disable") || strDescription.contains("expire")) {
          gmUserBean.saveInActivateDBUser(strUserId);
          hmUsersToBeInactivatedEmail.put(strUserId, strUserName);
        } else {
          alUsersStillActive.add(strUserId);
        }
      }
    }
    sendEmail(strCompanyId);
  }

  private void sendEmail(String strCompanyId) throws Exception {
    String strEmailFileName = "GmSyncAdDb";
    String strCompanyLocale = GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(strCompanyId));
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    String strToEmailId =
        gmResourceBundleBean.getProperty(strEmailFileName + "." + GmEmailProperties.TO);
    GmEmailProperties emailProps = new GmEmailProperties();

    StringTokenizer strEmailtokens = new StringTokenizer(strToEmailId, "^^");

    if (strEmailtokens.countTokens() > 1) {
      String strToEmail = strEmailtokens.nextToken();
      emailProps.setRecipients(strToEmail);
      emailProps.setCc(strEmailtokens.nextToken());
    } else {
      emailProps.setRecipients(strToEmailId);
      emailProps.setCc(gmResourceBundleBean.getProperty(strEmailFileName + "."
          + GmEmailProperties.CC));
    }
    emailProps.setSender(gmResourceBundleBean.getProperty(strEmailFileName + "."
        + GmEmailProperties.FROM));
    emailProps.setMimeType(gmResourceBundleBean.getProperty(strEmailFileName + "."
        + GmEmailProperties.MIME_TYPE));
    String strSubject =
        gmResourceBundleBean.getProperty(strEmailFileName + "." + GmEmailProperties.SUBJECT);
    emailProps.setSubject(strSubject);

    StringBuffer stringBody = new StringBuffer();
    stringBody.append("<b>Total number of users from portal that are validated: </b> "
        + alActiveUsers.size());
    stringBody.append("<br><br><b> Total number of users that are made inactive in portal: </b> "
        + hmUsersToBeInactivatedEmail.size());
    stringBody.append("<br><br><b> List of users made inactive in portal:</b><br><br> "
        + hmUsersToBeInactivatedEmail);
    stringBody
        .append("<br><br><b> Total number of users converted to DB users and inactivated:</b> "
            + hmUsersNotAvlInADEmail.size());
    stringBody
        .append("<br><br><b> List of users converted to DB users and inactivated:</b><br><br> "
            + hmUsersNotAvlInADEmail);
    emailProps.setMessage(stringBody.toString());
    GmCommonClass.sendMail(emailProps);
  }

}
