/**
 * FileName    : GmJob.java 
 * Description :
 * Author      : vprasath
 * Date        : Feb 26, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.quartz.Job;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author vprasath
 *
 */
public abstract class GmJob implements Job {
	
	public static final String JOBNAME = "JOBNAME";
	public static final String STARTTIME = "STARTTIME";
	public static final String ENDTIME = "ENDTIME";
	public static final String STATUS = "STATUS";
	public static final String EXCEPTION = "EXCEPTION";
	public static final String ADDITIONALPARAM = "ADDITIONALPARAM";
	public static final String JOB_STATUS_SUCCESS = "Success";
	public static final String JOB_STATUS_FAIL = "Fail";
	public static final String JOB_STATUS_MAIL = "GmJobStatusMail";
	
	private String jobName = "";
	private String jobStartTime = "";
	private String jobEndTime = "";
	SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss"); 
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return jobName;
	}
	/**
	 * @param jobName the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	/**
	 * @return the jobStartTime
	 */
	protected String getJobStartTime() {
		return jobStartTime;
	}
	/**
	 * @param jobStartTime the jobStartTime to set
	 */
	protected void setJobStartTime(String jobStartTime) {
		this.jobStartTime = jobStartTime;
	}
	/**
	 * @return the jobEndTime
	 */
	protected String getJobEndTime() {
		return jobEndTime;
	}
	/**
	 * @param jobEndTime the jobEndTime to set
	 */
	protected void setJobEndTime(String jobEndTime) {
		this.jobEndTime = jobEndTime;
	}
	
	public void sendJobStatusEmail(String strStatus, HashMap hmStatusData)
	{
		String[] cc = null;
		String strFormedJobEmail = "";
		
		
		HashMap hmJobStatus = new HashMap();
		String[] strRecipients = {(String)hmStatusData.get("gmITMail")};
		
		StringBuffer strJobEmailData = new StringBuffer();
		String strDateFormat = (new SimpleDateFormat("MM/dd/yyyy").format(new java.util.Date()));

		try{		
			strJobEmailData.append("<tr> <td height=15 ></td> </tr>");
			strJobEmailData.append("<tr> <td height=15 ><B>Shipment Job Status<B></td> </tr>");
			strJobEmailData.append("<tr> <td height=15 >Period :-"+strDateFormat+" </td></tr>");
			strJobEmailData.append("<tr> <td height=15 >Start Time :-" +getJobStartTime()+"</td></tr>");
			strJobEmailData.append("<tr> <td height=15 >End Time :-" +getJobEndTime()+"</td></tr>");
			strJobEmailData.append("<tr> <td height=15 >MESSAGE "+strStatus+"</td></tr>");
			strFormedJobEmail = "<table>".concat(strJobEmailData.toString())+ "</table>";
			
			hmJobStatus.put("strFrom", (String)hmStatusData.get("gmFromMailValue"));
			hmJobStatus.put("strTo", strRecipients);
			hmJobStatus.put("strCc", cc);
			hmJobStatus.put("strSubject", (String)hmStatusData.get("strClass")+ " - "+strStatus);
			hmJobStatus.put("strMessageDesc", strFormedJobEmail);
			hmJobStatus.put("strMimeType", "text/html");
			
			GmCommonClass.sendMail(hmJobStatus);
		}catch(Exception ex){
			ex.printStackTrace();
		}		
	}
	
	public static String getClassName(String strClasName)
	{
		String[] strPkgs = null; 
		strPkgs	= strClasName.split("\\.");
		return strPkgs[(strPkgs.length)-1];
	}
	
	public void sendJobStatusEmail(ArrayList alBody, HashMap hmAdditionalParams)
	{
		try
		{
			GmCommonClass.sendEmailFromTemplate(JOB_STATUS_MAIL, alBody, hmAdditionalParams);
		}
		catch (Exception e)
		{
			log.error("Exception occured while sending job status email : " + e.getMessage(), e);
		}
	}
	

	



}
