package com.globus.common.util.jobs;

import org.quartz.JobDataMap;

import com.globus.common.db.GmDBManager;

public class GmNonUsageReqJob extends GmActionJob {

	public void execute(JobDataMap jobDataMap) throws Exception {
		// TODO Auto-generated method stub

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_op_charges.gm_sav_non_usage_charge", 0);
		gmDBManager.execute();
		gmDBManager.commit();

	}

}
