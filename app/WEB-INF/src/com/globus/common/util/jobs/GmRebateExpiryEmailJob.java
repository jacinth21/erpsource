package com.globus.common.util.jobs;


import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmEmailProperties;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.sales.pricing.beans.GmRebateReportBean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


/**
 * GmRebateExpiryEmailJob - This class used for send mail to remind RebateExpiry details.
 * @author RSelvaraj
 *
 */
public class GmRebateExpiryEmailJob extends GmActionJob{
	
	public static final String TEMPLATE_NAME = "GmRebateExpiryEmailJob";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	/* execute - This override method used to fetch email related information and send mail based on rebate expiry range
	 * 
	 */
	@Override
	public void execute(JobDataMap jobDataMap)throws Exception {
		
		 GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
		 
		 String strCompanyId = gmDataStoreVO.getCmpid();
		 GmRebateReportBean gmRebateReportBean = new GmRebateReportBean(gmDataStoreVO);
		 
		 
		 GmJasperMail jasperMail;
		 ArrayList alRules = new ArrayList();
		 ArrayList alDtl = new ArrayList();
		 ArrayList alEmailDetails = new ArrayList();
		 HashMap hmReturn = new HashMap();
		 HashMap hmRuleValues = new HashMap();
		 
		 String strToEmailId="";
		 String strFromEmailId="";
		 String strCcEmailId="";
		 String strSubjectEmailId="";
		 String strMimeType="";
		
		 // Fetch to,from,cc,subject and content type 
		 alEmailDetails=GmCommonClass.fetchRuleValuesByGroup("REBATE_EXP_EMAIL",strCompanyId);
		 
		 for(Iterator iter = alEmailDetails.iterator(); iter.hasNext();)
		 {
			 
			 hmRuleValues = (HashMap)iter.next();
				 
			 String strRuleID = (String)hmRuleValues.get("RULEID");
			  
			 if(strRuleID.equals("REBATE_EMAIL_TO"))
			 {
				 strToEmailId = (String)hmRuleValues.get("RULEVALUE");
			 }
			 else if(strRuleID.equals("REBATE_EMAIL_FROM"))
			 {
				 strFromEmailId = (String)hmRuleValues.get("RULEVALUE");
			 }
			 else if(strRuleID.equals("REBATE_EMAIL_CC"))
			 {
				 strCcEmailId = (String)hmRuleValues.get("RULEVALUE");
			 }
			 
			 else if(strRuleID.equals("REBATE_EMAIL_CONTYP"))
			 {
				 strMimeType = (String)hmRuleValues.get("RULEVALUE");
			 }
			 
			 else if(strRuleID.equals("REBATE_EMAIL_SUBJECT"))
			 {
				 strSubjectEmailId = (String)hmRuleValues.get("RULEVALUE");
			 }
			 
			 
		 }// end for loop
		 
		 		 
	
		 GmEmailProperties emailProps = new GmEmailProperties();
		
		 // Set from,to,cc and content type to emailprops except subject 
		 emailProps.setSender(strFromEmailId);
		 emailProps.setRecipients(strToEmailId);
		 emailProps.setCc(strCcEmailId);
		 emailProps.setMimeType(strMimeType);
		 
		
		// get rebate expiry range based on company id
		 alRules=GmCommonClass.fetchRuleValuesByGroup("REBATE_EXP_EMAIL_RG",strCompanyId);
		 
		 
		 if(alRules.size()>0)
		 {
			
			for (Iterator iter = alRules.iterator(); iter.hasNext();)
			 {
				 try{
				 
					 hmRuleValues = (HashMap)iter.next();
					 
					 String strVal = (String)hmRuleValues.get("RULEVALUE");
					 alDtl = gmRebateReportBean.fetchRebateExpiryDetails(strVal,strCompanyId); 
			 
					 String strSubject = GmCommonClass.replaceAll(strSubjectEmailId, "#<RANGE>", strVal);
					 emailProps.setSubject(strSubject);
			        
			        if (alDtl.size() > 0)
			        	
			        {
			        	 jasperMail = new GmJasperMail();
			             jasperMail.setJasperReportName("/GmRebateExpiryNotify.jasper");
			             jasperMail.setReportData(alDtl);
			             jasperMail.setEmailProperties(emailProps);
			             hmReturn = jasperMail.sendMail();
			        }
			     
				 } catch (Exception e) {
			        e.printStackTrace();
			     }
				 
			 }// end of for loop
			 
		 }// end of if condition
		 	 
	}// end of execute() method
	
}
