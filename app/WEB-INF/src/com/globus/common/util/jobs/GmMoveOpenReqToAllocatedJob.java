/**
 * FileName    : GmMoveOpenReqToAllocatedJob.java 
 * Description : This job will allocate sets to open requests if the set is available 
 *             : and planned shipping date less than or equal to 7 days from today
 * Author      : Dhana Reddy
 */
package com.globus.common.util.jobs;
import oracle.jdbc.driver.OracleTypes;
import org.quartz.JobDataMap;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;

public class GmMoveOpenReqToAllocatedJob extends GmActionJob {
	
	public void execute(JobDataMap jobDataMap) throws AppError {

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_move_open_to_allocated", 0);
		gmDBManager.execute();
		gmDBManager.commit();
	}

}
