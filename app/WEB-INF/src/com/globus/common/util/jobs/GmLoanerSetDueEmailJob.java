package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmLoanerSetDueEmailJob extends GmLoanerRequestsEmailJob {

  public static final String TEMPLATE_NAME = "GmLoanerSetDueMailJob";

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);

    ArrayList alRepId = new ArrayList();
    HashMap hmRepDetails = new HashMap();
    HashMap hmParams = new HashMap();
    HashMap hmReturn = new HashMap();
    String strTempHeader = "";
    String strEmailHeaderName = "";
    String strRepId = "";
    String strRepName = "";
    String strRepEmail = "";
    String strAssrepID = "";
    String strAssRepName = "";
    String strAssRepEmail = "";
    String strTransdateFmt = "";
    
    ArrayList alLoanerSetDetails = new ArrayList();
    // to run same job multiple time with different condition
    HashMap hmParam = GmCommonClass.parseNullHashMap((HashMap) jobDataMap.get("HMPARAM"));
    log.debug("hmParam==>" + hmParam);
    String strParam = GmCommonClass.parseNull((String) hmParam.get("LoanerDue"));
    hmParams.put("GMCSPHONE", GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSPhone"));
    hmParams.put("GMCSEMAIL", GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSEmail"));
    strEmailHeaderName = GmCommonClass.getEmailProperty("GmLoanerSetDue" + ".EmailHeader");
    

    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_loaner_pending_retn_rid", 2);
    gmDBManager.setString(1, strParam);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alRepId = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    Iterator iter = alRepId.iterator();
    while (iter.hasNext()) {
      try {
        hmRepDetails = (HashMap) iter.next();

        strRepId = GmCommonClass.parseNull((String) hmRepDetails.get("REP_ID"));
        strRepName = (String) hmRepDetails.get("REP_NAME");
        strRepEmail = (String) hmRepDetails.get("REP_EMAIL");

        strAssrepID = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPID"));
        strAssRepName = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPNM"));
        strAssRepEmail = GmCommonClass.parseNull((String) hmRepDetails.get("ASSOCREPEMAIL"));
        strTransdateFmt = GmCommonClass.parseNull((String) hmRepDetails.get("TRANSDTFMT"));
        log.debug("hmRepDetails == "+ hmRepDetails);
        /* Getting Loaner Set details for each rep */
        String strConsignToId =
            GmCommonClass.parseNull((String) hmRepDetails.get("CONSIGNED_TOID"));
        if (!strAssrepID.equals("") || !strRepId.equals("")) { // Added condition to skip the email for loaners which used in inhouse event        
        gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_loaner_pending_retn_det", 5);
        gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
        gmDBManager.setString(1, strRepId);
        gmDBManager.setString(2, strAssrepID);
        gmDBManager.setString(3, strConsignToId);
        gmDBManager.setString(4, strParam);
        gmDBManager.execute();

        alLoanerSetDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
        int intSize = alLoanerSetDetails.size();
        hmParams.put("QTY", Integer.toString(intSize));

        hmParams.putAll(hmRepDetails);
        hmParams.put("TRANSDATEFMT", strTransdateFmt);
        log.debug("hmParams == "+ hmParams);
        GmEmailProperties emailProps = getEmailProperties(TEMPLATE_NAME, strRepId);

        /*
         * The associate rep code has been added for PMT-4031. The functionality is to send an email
         * to Associate rep instead of Rep. And Sales rep should be in CC. The same code changes has
         * been added to below files as well. GmLoanerSetDueEmailJob.java (Due job),
         * GmLoanerBean.java (Transfer), GmTicketBean (missing charge)
         */
        if (!strAssrepID.equals("")) {
          strRepName = strAssRepName;
          strRepEmail = strAssRepEmail;
        }
        
        String strTransDatetFmt = GmCommonClass.parseNull((String) hmParams.get("TRANSDATEFMT"));    
        String strDateFormat = GmCommonClass.getStringFromDate((java.util.Date)hmRepDetails.get("D_DATE"),strTransDatetFmt);
        
        String strSubject = emailProps.getSubject();
        strSubject = GmCommonClass.replaceAll(strSubject, "#<REP_NAME>", strRepName);
        strSubject =
            GmCommonClass.replaceAll(strSubject, "#<DUE_DATE>",
            		strDateFormat);
        // to replace the total Set Qty
        strSubject =
            GmCommonClass.replaceAll(strSubject, "#<TOTAL_SET>", Integer.toString(intSize));
        strTempHeader =
            "The following sets are due back to Globus on <b>"
                + strDateFormat
                + "</b> <br> Please ensure that the following sets are returned promptly to avoid any late fees:";
        if (strParam.equals("OVERDUE")) {
          strSubject = Integer.toString(intSize) + " Loaner Set(s) Now Overdue - " + strRepName;
          strTempHeader =
              "The following sets are due back to Globus.<br> Please ensure that the sets are returned promptly, as late fees accrue each day<br> the sets are not returned.";
        } else if (strParam.equals("ALLOVERDUE")) {
          strSubject = Integer.toString(intSize) + " Loaner Set(s) Overdue - " + strRepName;
          strTempHeader =
              "The following sets are due back to Globus.<br> Please ensure that the sets are returned promptly, as late fees accrue each day<br> the sets are not returned.";
        }
        emailProps.setRecipients(strRepEmail);
        emailProps.setSubject(strSubject);
        emailProps.setEmailHeaderName(strEmailHeaderName);
        hmParams.put("THEADER", strTempHeader);
        GmJasperMail jasperMail = new GmJasperMail();
        jasperMail.setJasperReportName("/GmLoanerSetDueMailJob.jasper");
        jasperMail.setAdditionalParams(hmParams);
        jasperMail.setReportData(alLoanerSetDetails);
        jasperMail.setEmailProperties(emailProps);
        hmReturn = jasperMail.sendMail();
        }

      } catch (Exception e) {

        log.error("Exception in job body: " + e.getMessage());
        hmReturn.put(GmJob.EXCEPTION, e);
      }
      Exception ex = (Exception) hmReturn.get(GmJob.EXCEPTION);
      if (ex != null) {
        log.error("Exception in sendJasperMail " + ex.getMessage());
        setJobStatus(GmJob.JOB_STATUS_FAIL);

        setJobException(GmCommonClass.getExceptionStackTrace(ex, "<br>"));
        setAdditionalParam((String) hmReturn.get("ORIGINALMAIL"));
        sendJobExceptionEmail();
        throw ex;
      }
    }
    gmDBManager.close();
  }

}
