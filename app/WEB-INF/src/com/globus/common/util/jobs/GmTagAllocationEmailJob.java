/**
 * FileName    : GmTagAllocationEmailJob.java 
 * Description :
 * Author      : rshah
 * Date        : Nov 09, 2011 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ResourceBundle;
 
import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties; 
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * @author RShah
 *
 */
public class GmTagAllocationEmailJob extends GmActionJob {

	public static final String TEMPLATE_NAME = "GmTagAllocationJob";
	Logger log = GmLogger.getInstance(this.getClass().getName());
	SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
		GmDBManager gmDBManager = new GmDBManager();
		log.debug("jobDataMap = " + jobDataMap);
		ArrayList  alCaseRepDtls = new ArrayList(); 
		ArrayList  alSetTagDtls = new ArrayList();
		ArrayList  alTagAllocDtls = new ArrayList();
		ArrayList  alDtls = new ArrayList();
		ArrayList  alExceptionList = new ArrayList();
		
		HashMap hmRepCaseDtl = new HashMap(); 
		HashMap hmRepSetEmailList = new HashMap();		
		HashMap hmEmailDtl = new HashMap();
		
		String strCurrRepId = "";
		String strPrevRepId = "";
		String strCurrRepName = "";
		String strCurrRepEmailId = "";
		String strPrevRepEmail = "";
		String strPrevRepName = "";
		
		gmDBManager.setPrepareString("gm_pkg_sm_tag_allocation.gm_fch_all_caserep_dtl", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();		
		alCaseRepDtls = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1));
		
		gmDBManager.setPrepareString("gm_pkg_sm_tag_allocation.gm_fch_all_tagset_status", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();		
		alSetTagDtls = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1));
		
		log.debug("alCaseRepDtls = " + alCaseRepDtls);
		log.debug("alSetTagDtls = " + alSetTagDtls);
		
		for(Iterator iterRep = alCaseRepDtls.iterator(); iterRep.hasNext();)
		{
			hmRepCaseDtl = (HashMap)iterRep.next();
			strCurrRepId = GmCommonClass.parseNull((String)hmRepCaseDtl.get("CASEREPID"));
			strCurrRepName = GmCommonClass.parseNull((String)hmRepCaseDtl.get("REPNAME"));
			strCurrRepEmailId = GmCommonClass.parseNull((String)hmRepCaseDtl.get("REPEMAIL"));
			if (!strCurrRepId.equals(strPrevRepId))
			{
				log.debug("alRepEmailDtls" + alTagAllocDtls);	
				if (!alTagAllocDtls.isEmpty())
				{
					//call method and pass the alFinalDtl to update email flag
					hmEmailDtl.put("REPNAME", strPrevRepName);
					hmEmailDtl.put("ALTAGALLOCDTLS", alTagAllocDtls);
					hmEmailDtl.put("REPEMAIL", strPrevRepEmail);
					try
					{
						alDtls.addAll(alTagAllocDtls);
						sendTagAllocationEmail(hmEmailDtl);
						//saveTagEmailNotification(alTagAllocDtls);
						
					}
					catch(Exception ex)
					{
						alExceptionList.addAll(alTagAllocDtls);
					}
						alTagAllocDtls.clear();
						
				}
				strPrevRepId = strCurrRepId;
				strPrevRepEmail = strCurrRepEmailId;
				strPrevRepName = strCurrRepName;
				hmRepSetEmailList = fetchRepSetTagList(hmRepCaseDtl,alSetTagDtls);
				
				HashMap hmSetTagDtl = new HashMap();
				HashMap hmRepSetTagDtl = new HashMap();
				
				if (!hmRepSetEmailList.isEmpty())
				{
					hmRepSetTagDtl = (HashMap)hmRepSetEmailList.get("HMREPSETTAGDTL");
					hmSetTagDtl = (HashMap)hmRepSetEmailList.get("HMSETTAGDTL");
				
					alSetTagDtls.remove(hmSetTagDtl);
					alTagAllocDtls.add(hmRepSetTagDtl);
				}				
			}
			else 
			{
				hmRepSetEmailList = fetchRepSetTagList(hmRepCaseDtl,alSetTagDtls);
				//log.debug("alSetTagDtls size = " + alSetTagDtls.size());
				HashMap hmSetTagDtl = new HashMap();
				HashMap hmRepSetTagDtl = new HashMap();
				
				if (!hmRepSetEmailList.isEmpty())
				{
					hmRepSetTagDtl = (HashMap)hmRepSetEmailList.get("HMREPSETTAGDTL");
					hmSetTagDtl = (HashMap)hmRepSetEmailList.get("HMSETTAGDTL");
				
					alSetTagDtls.remove(hmSetTagDtl);
					alTagAllocDtls.add(hmRepSetTagDtl);
				}
			}			
			if(alSetTagDtls.size() == 0)
			{
				log.debug("alRepEmailDtls" + alTagAllocDtls);
				if (!alTagAllocDtls.isEmpty())
				{
					//call method and pass the alFinalDtl to update email flag
					hmEmailDtl.put("REPNAME", strCurrRepName);
					hmEmailDtl.put("ALTAGALLOCDTLS", alTagAllocDtls);
					hmEmailDtl.put("REPEMAIL", strCurrRepEmailId);
					try
					{
						alDtls.addAll(alTagAllocDtls);
						sendTagAllocationEmail(hmEmailDtl);						 
					}
					catch(Exception ex)
					{
						alExceptionList.addAll(alTagAllocDtls);						
					}
					alTagAllocDtls.clear();
				}
				break;
			}			
		}
		log.debug("alExceptionList = "+alExceptionList);
		log.debug("alDtls = " + alDtls);		
		alDtls.removeAll(alExceptionList);
		saveTagEmailNotification(alDtls);
	}
	/*
	 * fetchRepSetTagList is used to return the compared case set id which has details for the set and tag.
	 * 
	 */
	public HashMap fetchRepSetTagList(HashMap hmParam, ArrayList alSetTagDtls) throws AppError{
		
		ArrayList alRepSetTagDtls = new ArrayList();
		
		HashMap hmSetTagDtl = new HashMap();
		HashMap hmReturn = new HashMap();
		
		
		String strRepCaseSetId = "";
		String strSetId = "";
		String strSetName = "";		
		String strAcctName = "";
		String strLcnType = "";
		String strCaseId = "";
		
		String strTagCaseSetId = "";
		String strCurrTag = "";
		String strPrevTag = "";
		String strLogInfoId = "";
		int irmCnt=0;
		
		
		strRepCaseSetId = GmCommonClass.parseNull((String)hmParam.get("CASESETID"));
		strSetId = GmCommonClass.parseNull((String)hmParam.get("SETID"));
		strSetName = GmCommonClass.parseNull((String)hmParam.get("SETNAME"));		
		strAcctName = GmCommonClass.parseNull((String)hmParam.get("ACCTNAME"));
		strLcnType = GmCommonClass.parseNull((String)hmParam.get("LCNTYPE"));
		strCaseId = GmCommonClass.parseNull((String)hmParam.get("CASEID"));
		
		Iterator iterTag = alSetTagDtls.iterator();
		
		while(iterTag.hasNext())
		{	
			irmCnt = 0;
			hmSetTagDtl = (HashMap)iterTag.next();
			strTagCaseSetId = GmCommonClass.parseNull((String)hmSetTagDtl.get("CASESETID"));
			if (strTagCaseSetId.equals(strRepCaseSetId))
			{
				HashMap hmRepSetTagDtl = new HashMap();
				strCurrTag = (String)hmSetTagDtl.get("CURRTAG");
				strPrevTag = (String)hmSetTagDtl.get("PREVTAGID");
				strLogInfoId = (String)hmSetTagDtl.get("INFOLOGID");
				hmRepSetTagDtl.put("INFOLOGID", strLogInfoId);
				hmRepSetTagDtl.put("CASESETID", strTagCaseSetId);
				hmRepSetTagDtl.put("CASEID", strCaseId);
				hmRepSetTagDtl.put("SETID", strSetId);
				hmRepSetTagDtl.put("SETNAME", strSetName);
				hmRepSetTagDtl.put("CURRTAG", strCurrTag);
				hmRepSetTagDtl.put("PREVTAGID", strPrevTag);
				hmRepSetTagDtl.put("LCNTYPE", strLcnType);
				hmRepSetTagDtl.put("ACCTNAME", strAcctName);
				hmReturn.put("HMREPSETTAGDTL", hmRepSetTagDtl);
				irmCnt = 1;
				break;
			}
		}
		if (irmCnt == 1 )
		{
			hmReturn.put("HMSETTAGDTL", hmSetTagDtl);
		}
		return hmReturn;
	}
	/*
	 * sendTagAllocationEmail is used to send an email to the respective rep.
	 * 
	 */ 
	public void sendTagAllocationEmail(HashMap hmParam) throws Exception {
		
		GmJasperMail jasperMail = new GmJasperMail();
		GmEmailProperties emailProps = new GmEmailProperties();
		
		String strRepEmail = (String) hmParam.get("REPEMAIL");
		 
		ArrayList alRepTagList = new ArrayList();
		
		HashMap hmReturnDetails = new HashMap();
		
		String strEmailPropName = "GmTagAllocationEmail";
		String strJasperName = "/GmTagAllocationEmail.jasper";
		
		String strSubject = "";
		String strHeaderMsg = "";
		String strCc = "";
		
		strHeaderMsg = GmCommonClass.getEmailProperty(strEmailPropName + "." + GmEmailProperties.MESSAGE);
		strSubject = GmCommonClass.getEmailProperty(strEmailPropName + "." + GmEmailProperties.SUBJECT);
		//strSubject = GmCommonClass.replaceAll(strSubject, "#<RAID>", strRAId);
		strCc = GmCommonClass.getEmailProperty(strEmailPropName + "." + GmEmailProperties.CC);
		
		log.debug("From = "+GmCommonClass.getEmailProperty(strEmailPropName + "." + GmEmailProperties.FROM));
		emailProps.setCc(strCc);
		emailProps.setRecipients(strRepEmail);
		emailProps.setMimeType(GmCommonClass.getEmailProperty(strEmailPropName + "." + GmEmailProperties.MIME_TYPE));
		emailProps.setSender(GmCommonClass.getEmailProperty(strEmailPropName + "." + GmEmailProperties.FROM));
		emailProps.setSubject(strSubject);
		
		strHeaderMsg="The following tags are allocated/deallocated for respective case. ";
		hmReturnDetails.put("EMAILHEADER", strHeaderMsg);
		hmReturnDetails.put("REPNAME", (String) hmParam.get("REPNAME"));		
		//hmReturnDetails.put("TAGALLOCATIONLIST", (ArrayList) hmParam.get("ALTAGALLOCDTLS"));
		
		log.debug("hmReturnDetails = "+hmReturnDetails);
		
		jasperMail.setJasperReportName(strJasperName);
		jasperMail.setAdditionalParams(hmReturnDetails);
		jasperMail.setReportData((ArrayList) hmParam.get("ALTAGALLOCDTLS"));
		jasperMail.setEmailProperties(emailProps);
		hmReturnDetails = jasperMail.sendMail();		
	 }
	/*
	 * saveTagEmailNotification is used to update the email notification to Y.
	 * 
	 */ 
	public void saveTagEmailNotification(ArrayList alTagEmailNotify) throws AppError {
		GmDBManager gmDBManager = new GmDBManager();
		HashMap hmSetTagDtl = new HashMap();
		Iterator iterTag = alTagEmailNotify.iterator();
		StringBuffer strLogInfoId = new StringBuffer();
		while(iterTag.hasNext())
		{
			hmSetTagDtl = (HashMap)iterTag.next();
			strLogInfoId.append((String)hmSetTagDtl.get("INFOLOGID"));
			strLogInfoId.append(",");
		}
		gmDBManager.setPrepareString("gm_pkg_sm_setmgmt_txn.gm_cm_sav_email_log", 2);
		gmDBManager.setString(1, strLogInfoId.toString());
		gmDBManager.setString(2, "Y");
		gmDBManager.execute();
		gmDBManager.commit();
	}
}
