/**
 * FileName : GmResetLoanerSetPriorityJob.java 
 * Description : Job to reset the Loaner set priority
 * Author : Arajan
 */
package com.globus.common.util.jobs;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmResetLoanerSetPriorityJob extends GmActionJob {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);

    /* Reset the priority */
    gmDBManager.setPrepareString("gm_pkg_op_ln_priority_engine.gm_upd_loaner_set_priority", 0);
    gmDBManager.execute();
    gmDBManager.commit();
  }
}
