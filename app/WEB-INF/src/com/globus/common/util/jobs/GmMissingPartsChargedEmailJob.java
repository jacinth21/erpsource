package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.quartz.JobDataMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.common.jira.beans.GmJIRAHelperBean;

public class GmMissingPartsChargedEmailJob extends GmActionJob {

	public void execute(JobDataMap jobDataMap) throws Exception {
		// TODO Auto-generated method stub
		GmDBManager gmDBManager = new GmDBManager();
		ArrayList alReqDetails = new ArrayList();
		HashMap hmReqDetails = new HashMap();
		String strReqId = "";
		ArrayList alList = new ArrayList();
		
		log.debug("missing parts charged job started");
		gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_missingparts_req_det", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setInt(1, 0);
		gmDBManager.setString(2, "Y");
		gmDBManager.execute();

		alReqDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
		
		log.debug("alReqDetails: " + alReqDetails);
		
		
		for (Iterator iter = alReqDetails.iterator(); iter.hasNext();) {

			hmReqDetails = (HashMap) iter.next();
			strReqId = (String) hmReqDetails.get("REQ_ID");
			log.debug("strReqId " + strReqId);

			gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_sav_missingparts_writeoff", 2);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.setString(1, strReqId);
			gmDBManager.execute();
			
			alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
			
			log.debug("alList: " + alList);
			
			if(alList != null && alList.size() > 0)
			{
				//parse(alList);
			}
		}
		gmDBManager.commit();

	}
	
	private void parse( ArrayList alDetails) throws AppError
	{
		Iterator itrDetails = alDetails.iterator();
		ArrayList alDetailCopy = new ArrayList(alDetails);
		ArrayList alTicketDetails = new ArrayList();
		String strRequestDetailsID = "";
		
		boolean bsize = false;
		
		if (alDetails.size() == 1)
		{
			bsize = true;
		}
		
		log.debug("alTicketDetails : " + alTicketDetails);
		
		while (itrDetails.hasNext())
		{
			if (alTicketDetails.size() > 0 )
			 {		
				addCommentsAndClose(strRequestDetailsID, alTicketDetails);				
				alTicketDetails = new ArrayList();
			 }

			Iterator itrDetailCopy = alDetailCopy.iterator();
			HashMap hmMap = (HashMap)itrDetails.next();
			strRequestDetailsID = GmCommonClass.parseNull((String)hmMap.get("REQ_DET_ID"));
			
			log.debug("strRequestDetailsID: "+ strRequestDetailsID);
			
			 while (itrDetailCopy.hasNext())
			 {
				 HashMap hmTemp = (HashMap)itrDetailCopy.next();
				 String strTemp = GmCommonClass.parseNull((String)hmTemp.get("REQ_DET_ID"));
				 
				 log.debug("strTemp.equals(strRequestDetailsID): "+ strTemp.equals(strRequestDetailsID));
				 
				 if (strTemp.equals(strRequestDetailsID))
				 {
					 
					 alTicketDetails.add(hmTemp);
					 itrDetailCopy.remove();
				 }
			 }
			 
			 if (bsize && alTicketDetails.size() > 0 )
			 {
				 addCommentsAndClose(strRequestDetailsID, alTicketDetails);
			 }
		}
	}
	
	private void addCommentsAndClose(String strRequestDetailsID, ArrayList alTicketDetails) throws AppError
	{
		String strTicketID = "";
		GmJIRAHelperBean gmJIRAHelperBean = new GmJIRAHelperBean();

		strTicketID = gmJIRAHelperBean.checkTicketExists(strRequestDetailsID, "10056");
			  
			  log.debug("strTicketID Created Already: " + strTicketID);
			 
			 if (!strTicketID.equals(""))
			 {
				 gmJIRAHelperBean.updateReconDetailsInMT(strTicketID, new ArrayList(), alTicketDetails, "These Missing parts are past due date, You will be charged with the above amount") ;
				 gmJIRAHelperBean.closeIssue(strTicketID,"10056");
			 }		
	}
}
