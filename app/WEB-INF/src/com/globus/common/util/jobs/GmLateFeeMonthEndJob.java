package com.globus.common.util.jobs;

import org.quartz.JobDataMap;

import com.globus.common.db.GmDBManager;

public class GmLateFeeMonthEndJob extends GmActionJob{

	public void execute(JobDataMap jobDataMap) throws Exception {
		// This job runs monthly once to calculate loaner late fee.
		
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_op_charges.gm_sav_loaner_late_charge", 0);
		gmDBManager.execute();
		gmDBManager.commit();
	}

}
