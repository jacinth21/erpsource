package com.globus.common.util.jobs;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionException;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmBBAShipmentEmailJob extends GmActionJob {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
  /**
   * execute - This method will be used to send mail based on Job
   * 
   * @param JobExecutionContext
   * @exception JobExecutionException
   */
  String strFormedEmail = "";
  int shipmentRepCnt = Integer.parseInt(GmCommonClass.getString("GMSHIPMENTREPCNT"));
  SimpleDateFormat strFormatter1 = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
  SimpleDateFormat strFormatter2 = new SimpleDateFormat("MM/dd/yyyy");

  String strRepCCEmail = "";
  String strShipToEmail = "";
  String strSource = "";
  String strShipToId = "";
  String strShipToType = "";
  String strShipToEmailID = "";
  String strVisitedFl = "";
  String strShiptoVisitedFl = "";
  String strExcpTime = "";
  String strFedxFl = "";
  String strTransId = "";
  String strcarrier = "";
  String strMode = "";
  String strRuleSource = "";
  String strTrackNumber = "";
  String strFromMailValue = "";
  String strSubjectValue = "";
  String strCCMail = "";

  String strGmShipDesc = "";
  String strGmShipValue = "";
  String strGmPhone = "";
  String strFedexDesc = "";
  String strFedexValue = "";
  String strGmMailFooter = "";
  String strAddress = "";
  String strDCarr = "";

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmShipmentEmailJob gmShipmentEmailJob = new GmShipmentEmailJob();
    strFormatter1 = new SimpleDateFormat(gmDataStoreVO.getCmpdfmt() + " hh:mm:ss");
    strFormatter2 = new SimpleDateFormat(gmDataStoreVO.getCmpdfmt());

    String strTrackVisitedFl = "";
    String strJobEndTime = "";
    String strShipToNm = "";
    String strJobStatusFl = "SUCCESS";
    String strShipCCEmail = "";
    String strJob = GmJob.getClassName(this.getClass().getName());
    StringBuffer strHtmlString = new StringBuffer();

    HashMap hmShippedData = new HashMap();
    HashMap hmShippedRowData = new HashMap();
    HashMap hmSourceMap = new HashMap();
    HashMap hmStaticVal = new HashMap();

    ArrayList alShippedData = new ArrayList();
    ArrayList alExceptionList = new ArrayList();
    ArrayList alRuleList = new ArrayList();

    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean(gmDataStoreVO);
    GmShippingTransBean gmShippingTransBean = new GmShippingTransBean(gmDataStoreVO);
    String strCompanyLocale =
        GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid()));
    GmResourceBundleBean rsCompanyBundle =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    GmResourceBundleBean rsPaperworkBundle =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    GmCommonClass gmCommonClass = new GmCommonClass();

    ArrayList alSourceList = new ArrayList();
    ArrayList alShipValue = new ArrayList();
    HashMap hmShipMap = new HashMap();
    HashMap hmReturn = new HashMap();
    hmShippedData = gmShippingInfoBean.ShippedOrderData();
    alRuleList = (ArrayList) hmShippedData.get("alRuleList");
    alShippedData = (ArrayList) hmShippedData.get("alShippingList");
    ArrayList alTempList = new ArrayList(alShippedData);
    strRuleSource = GmCommonClass.parseNull(rsPaperworkBundle.getProperty("SHIPMENT.TRANSID"));
    log.debug("@@@@ strRuleSource @@@@"+strRuleSource);

    hmStaticVal = getShipmentStaticValues(alRuleList);
    StringBuffer strRule = (StringBuffer) hmStaticVal.get("strRule");
    strCCMail = (String) hmStaticVal.get("gmCSMail");
    strGmPhone = (String) hmStaticVal.get("SHIPPONE");
    strFedexDesc = (String) hmStaticVal.get("FEDEXDESC");
    strGmShipDesc = (String) hmStaticVal.get("SHIPDESC");
    strFedexValue = (String) hmStaticVal.get("FEDEXPHONE");
    strGmShipValue = (String) hmStaticVal.get("SHIPMAIL");
    strGmMailFooter = (String) hmStaticVal.get("GMFOOTER");
    strSubjectValue = (String) hmStaticVal.get("gmSubjectValue");
    strFromMailValue = (String) hmStaticVal.get("gmFromMailValue");

    hmStaticVal.put("strClass", strJob);

    Iterator itr = alShippedData.iterator();
    /*
     * Logic Explanation :
     * 
     * - Here the fetchShippedData method will get the two arraylist. One having all the details of
     * shipped data and other having all the static values from rule table for sending mails. - Loop
     * has been traversed based on the records of alShippedData. From which we are getting the
     * values for the first record. - Then shiptoid and the alShippedData has been passed in
     * getShipValues() to get the arraylist(alShipValue) of only shipped records for that
     * rep/distributor. - After that we loop through alShipValue and pass the source and alShipValue
     * in getSourceValue() to get the arraylist(alSourceList) for specific source. - After that we
     * loop through alSourceList and pass the track and alSourceList in getTrackValue() to get the
     * arraylist(alTrackList) for specific source. - alTrackList will be helpful for preparing the
     * HTML String for email template. - if any exception occure in between the mail will be sent to
     * the respective department with the details otherwise shipment mails will be sent to
     * respective rep/distributor. - Job status mails will be sent to IT at the end of this method.
     */
    while (itr.hasNext()) {
      hmShippedRowData = (HashMap) itr.next();
      strFormedEmail = "";
      strShipToEmail = (String) hmShippedRowData.get("SHIPEMAIL");
      strShipCCEmail = (String) hmShippedRowData.get("CCLIST");
      strShipToNm = (String) hmShippedRowData.get("SHIPTONM");
      strShipToId = (String) hmShippedRowData.get("SHIPTOID");
      strShipToType = (String) hmShippedRowData.get("SHIPTOTYPE");
      strSource = (String) hmShippedRowData.get("SOURCEID");
      strTransId = (String) hmShippedRowData.get("REFID");
      strcarrier = (String) hmShippedRowData.get("SCARR");
      strMode = (String) hmShippedRowData.get("SMODE");
      strTrackNumber = (String) hmShippedRowData.get("TRACK");
      strAddress = (String) hmShippedRowData.get("ADDRESS");
      strDCarr = (String) hmShippedRowData.get("DCRR");
      strShiptoVisitedFl = GmCommonClass.parseNull((String) hmShippedRowData.get("SHIPVISITED"));
      String strCompanyName = GmCommonClass.parseNull(rsCompanyBundle.getProperty("GMCOMPANYNAME"));
      String strComanyFooter = (strCompanyName.equals("")) ? "Globus Medical" : strCompanyName;

      // The following block of code is written for Showing the FedEx Contact Information in email.
      if (!strShiptoVisitedFl.equals("Y")) {
        strHtmlString = new StringBuffer();
        hmReturn = getShipValues(strShipToEmail, alShippedData);
        alShipValue = (ArrayList) hmReturn.get("ALSHIP");
        strFedxFl = (String) hmReturn.get("FEDXFL");
      }

      if (strRuleSource.indexOf("|" + strSource + "|") != -1) {
        try {
          HashMap hmSendMailData = new HashMap();
          hmSendMailData.put("strShipToId", strShipToId);
          hmSendMailData.put("strShipToNm", strShipToNm);
          hmSendMailData.put("EmailData", alShipValue);
          hmSendMailData.put("strShipToEmail", strShipToEmail);
          hmSendMailData.put("strShipCCEmail", strShipCCEmail);
          hmSendMailData.put("strShipToType", strShipToType);
          hmSendMailData.put("FEDXFL", strFedxFl);
          hmSendMailData.put("strTransID", strTransId);
          hmSendMailData.put("strCarrier", strcarrier);
          hmSendMailData.put("strMode", strMode);
          hmSendMailData.put("strTrackNumber", strTrackNumber);
          hmSendMailData.put("SHIPDESC", strGmShipDesc);
          hmSendMailData.put("SHIPMAIL", strGmShipValue);
          hmSendMailData.put("SHIPPONE", strGmPhone);
          hmSendMailData.put("FEDEXDESC", strFedexDesc);
          hmSendMailData.put("FEDEXPHONE", strFedexValue);
          hmSendMailData.put("GMFOOTER", strGmMailFooter);
          hmSendMailData.put("FEDXFL", strFedxFl);
          hmSendMailData.put("FOOTER", strComanyFooter);
          hmSendMailData.put("ADDRESS", strAddress);
          hmSendMailData.put("SOURCE", strSource);
          hmSendMailData.put("DCARR", strDCarr);
          generateHTMLReport(hmSendMailData);
          gmShippingTransBean.saveShippedData(updateShipmentEmail(alShipValue));
        } catch (Exception ex) {
          HashMap hmExcpVal = new HashMap();
          strJobStatusFl = "FAILED";
          strExcpTime = (strFormatter1.format(new java.util.Date()));
          hmExcpVal.put("EXCEPTION", GmCommonClass.getExceptionStackTrace(ex, "<br>"));
          hmExcpVal.put("strExcpTime", strExcpTime);
          hmExcpVal.put("strShipToNm", strShipToNm);
          hmExcpVal.put("strEmailData", strFormedEmail);
          alExceptionList.addAll(alShipValue);
          try {
            generateExcpReport(hmStaticVal, hmExcpVal);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }
  }

  /**
   * updateShipmentEmail - This method will be used to update the flag for shippped email
   * 
   * @param ArrayList
   * @return HashMap
   */

  private HashMap updateShipmentEmail(ArrayList alList) {
    String strDbRefId = "";
    String strDbSource = "";
    String strDbValue = "";
    Iterator itrShipments = alList.iterator();
    HashMap hmReturn = new HashMap();
    HashMap hmEmailShipment = new HashMap();
    while (itrShipments.hasNext()) {
      hmEmailShipment = (HashMap) itrShipments.next();
      strDbRefId = (String) hmEmailShipment.get("REFIDACTUAL") + ",";
      strDbSource = (String) hmEmailShipment.get("SOURCEID") + "|";
      strDbValue = strDbValue + strDbRefId + strDbSource;
    }
    hmReturn.put("REFID", strDbValue);
    return hmReturn;
  }

  /**
   * handleException - This method will be used to handle the generated exceptions and remove those
   * record from the shipment list so the flag for those records wont updated.
   * 
   * @param ArrayList, ArrayList
   * 
   */
  private void handleException(ArrayList alShipment, ArrayList alExceptions) {
    Iterator itrShipments = alShipment.iterator();
    while (itrShipments.hasNext()) {
      HashMap hmMap = (HashMap) itrShipments.next();
      Iterator itrExceptions = alExceptions.iterator();
      while (itrExceptions.hasNext()) {
        HashMap hmTempMap = (HashMap) itrExceptions.next();
        if (hmMap == hmTempMap) {
          itrShipments.remove();
        }
      }
    }
  }

  /**
   * generateExcpReport - This method will be used to generate exception template and send mails for
   * the records having exception
   * 
   * @param HashMap, HashMap
   * @exception Exception
   */
  private void generateExcpReport(HashMap hmExcpMailData, HashMap hmExcp) throws Exception {
    HashMap hmMailValues = new HashMap();
    StringBuffer strFormedExcpEmail = new StringBuffer();
    StringBuffer strExcpEmailData = new StringBuffer();
    String[] cc = {(String) hmExcpMailData.get("gmITMail")};
    String[] strRecipients = {(String) hmExcpMailData.get("gmCSMail")};

    strExcpEmailData.append("<tr> <td height=15 ></td> </tr>");
    strExcpEmailData.append("<tr> <td height=15 ><B>Shipment Status<B></td> </tr>");
    strExcpEmailData.append("<tr> <td height=15 >Exception Occure For :"
        + (String) hmExcp.get("strShipToNm") + " </td></tr>");
    strExcpEmailData.append("<tr> <td height=15 >Exception Time :"
        + (String) hmExcp.get("strExcpTime") + "</td></tr>");
    strExcpEmailData.append("<tr> <td height=15 >MESSAGE FAILED</td></tr>");
    strExcpEmailData.append("<tr> <td height=15 >Exception Message :"
        + (String) hmExcp.get("EXCEPTION") + "</td></tr>");
    strFormedExcpEmail.append("<table>".concat(strExcpEmailData.toString()) + "</table>");
    strFormedExcpEmail.append("<tr> <td height=15 ></td> </tr>");
    strFormedExcpEmail.append((String) hmExcp.get("strEmailData"));
    hmMailValues.put("strFrom", hmExcpMailData.get("gmFromMailValue"));
    hmMailValues.put("strTo", strRecipients);
    hmMailValues.put("strCc", cc);
    hmMailValues.put("strSubject", (String) hmExcpMailData.get("strClass") + " - FAILED");
    hmMailValues.put("strMessageDesc", strFormedExcpEmail.toString());
    hmMailValues.put("strMimeType", "text/html");
    GmCommonClass.sendMail(hmMailValues);
  }

  /**
   * generateHTMLReport - This method will be used to generate email template and send mails for
   * those records
   * 
   * @param HashMap
   * @exception Exception
   */

  private void generateHTMLReport(HashMap hmSendMailData) throws Exception {
    GmEmailProperties gmEmailProperties = new GmEmailProperties();
    String strDateFormat = (strFormatter2.format(new java.util.Date()));
    GmJasperReport gmJasperReport = new GmJasperReport();
    GmCustomerBean gmCustomerBean = new GmCustomerBean();

    HashMap hmResult = new HashMap();
    HashMap hmReportHeaderDetails = new HashMap();
    String strFilename = "";
    String strSubject = "";
    String strFinalContent = "";
    String strShipAttachments = "";
    String strTransctionId = GmCommonClass.parseNull((String) hmSendMailData.get("strTransID"));
    String strTransCarrier = GmCommonClass.parseNull((String) hmSendMailData.get("strCarrier"));
    String strTransMode = GmCommonClass.parseNull((String) hmSendMailData.get("strMode"));
    String strTransTrackNum =
        GmCommonClass.parseNull((String) hmSendMailData.get("strTrackNumber"));
    String strShipToNm = GmCommonClass.parseNull((String) hmSendMailData.get("strShipToNm"));
    String strShipToEmailID =
        GmCommonClass.parseNull((String) hmSendMailData.get("strShipToEmail"));
    String strAddress = GmCommonClass.parseNull((String) hmSendMailData.get("ADDRESS"));
    String strShipToType = GmCommonClass.parseNull((String) hmSendMailData.get("strShipToType"));
    if (strShipToType.equals("4122")) {
      strAddress = strAddress.substring(6, strAddress.length());
    }
    String strSource = GmCommonClass.parseNull((String) hmSendMailData.get("SOURCE"));
    String strSourceName = GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strSource));
    String strShipToId = GmCommonClass.parseNull((String) hmSendMailData.get("strShipToId"));
    String strDCArr = GmCommonClass.parseNull((String) hmSendMailData.get("DCARR"));
    strSubject = "Details of Shipment to " + strShipToNm + " on ";
    strSubject = strSubject + strDateFormat;
    String[] cc = GmCommonClass.StringtoArray(strCCMail, ";");
    String[] strRecipients = {strShipToEmailID};
    // Shipping document applicable for orders
    if (strSource.equals("50180")) {
      // Calling the following method to generate the Shipping DOcument in corresponding location
      // and will return the file name based on the transaction id.
      strFilename = genearetShippingDocument(strTransctionId);

      // Calling the following method to File Attachment Details for the corresponding transaction
      strShipAttachments = generateFileAttachments(strFilename, strTransctionId);
    }
    // Setting the FOllowing Data based on the Particular transaction id instead of showing all the
    // transaction data in email.
    HashMap hmTransValues = new HashMap();
    hmTransValues.put("SHIPTOTYPE", strShipToType);
    hmTransValues.put("SHIPTOID", strShipToId);
    hmTransValues.put("REFIDACTUAL", strTransctionId);
    hmTransValues.put("REFID", strTransctionId);
    hmTransValues.put("SOURCE", strSourceName);
    hmTransValues.put("SOURCEID", strSource);
    hmTransValues.put("SHIPTONM", strShipToNm);
    hmTransValues.put("SHIPEMAIL", strShipToEmailID);
    hmTransValues.put("CCLIST", "");
    hmTransValues.put("SMODE", strTransMode);
    hmTransValues.put("SCARR", strTransCarrier);
    hmTransValues.put("TRACK", strTransTrackNum);
    hmTransValues.put("SETID", "");
    hmTransValues.put("SETNAME", "");
    hmTransValues.put("ETCHID", "");
    hmTransValues.put("ADDRESS", strAddress);
    hmTransValues.put("DCRR", strDCArr);

    ArrayList alMyList = new ArrayList();
    alMyList.add(hmTransValues);
    hmSendMailData.put("EmailData", alMyList);

	gmEmailProperties.setSender("sales@bonebank.com");
    gmEmailProperties.setRecipients(StringUtils.join(strRecipients, ","));
    gmEmailProperties.setCc(StringUtils.join(cc, ","));
    gmEmailProperties.setMimeType("text/html");
    gmEmailProperties.setMessage(strFinalContent);
    gmEmailProperties.setSubject(strSubject);
    gmEmailProperties.setAttachment(strShipAttachments);

    GmJasperMail jasperMail = new GmJasperMail();
    jasperMail.setJasperReportName("/GmShipmentEmail.jasper");
    jasperMail.setAdditionalParams(hmSendMailData);
    jasperMail.setReportData((ArrayList) hmSendMailData.get("EmailData"));
    jasperMail.setEmailProperties(gmEmailProperties);
    HashMap hmReturn = jasperMail.sendMail();
    // GmCommonClass.sendMail(gmEmailProperties);
  }

  /**
   * getShipValues - This method will be used to group the arraylist based on the rep id
   * 
   * @param String shipToId, ArrayList alList
   * @return ArrayList
   */
  private HashMap getShipValues(String shipToId, ArrayList alList) {
    int shipmentCnt = 0; // Check with Prasath, need to set from properties file.
    int fedexCnt = 0;
    Iterator itrValues = alList.iterator();
    HashMap hmValues = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alShipList = new ArrayList();
    while (itrValues.hasNext()) {
      hmValues = (HashMap) itrValues.next();
      String strShiptoVisitedFl = GmCommonClass.parseNull((String) hmValues.get("SHIPVISITED"));
      String strDeliverCrr = GmCommonClass.parseNull((String) hmValues.get("DCRR"));
      if (shipToId.equals(hmValues.get("SHIPEMAIL")) && !strShiptoVisitedFl.equals("Y")) {
        alShipList.add(hmValues);
        hmValues.put("SHIPVISITED", "Y");
        shipmentCnt++;
        if (strDeliverCrr.equals("5001")) {
          fedexCnt++;
        }
      }
      if (shipmentCnt == shipmentRepCnt)
        break;
    }
    if (fedexCnt > 0)
      hmReturn.put("FEDXFL", "Y");
    hmReturn.put("ALSHIP", alShipList);
    return hmReturn;
  }

  /**
   * getShipmentStaticValues - This method will be used to get the static records from the rule
   * table
   * 
   * @param ArrayList
   * @return HashMap
   */

  private HashMap getShipmentStaticValues(ArrayList alRuleList) {
    Iterator itrRule = alRuleList.iterator();
    String strGmShipDesc = "";
    String strGmShipValue = "";
    String strFedexDesc = "";
    String strFedexValue = "";
    String strGmPhone = "";
    String strGmFromMailDesc = "";
    String strGmFromMailValue = "";
    String strGmSubjectDesc = "";
    String strGmSubjectValue = "";
    String strGmMailFooter = "";
    String strCSMail = "";
    String strITMail = "";
    HashMap hmRule = new HashMap();
    StringBuffer strRule = new StringBuffer();

    while (itrRule.hasNext()) {
      hmRule = (HashMap) itrRule.next();
      String ruleId = (String) hmRule.get("RULEID");
      if (ruleId.equals("GMSHIPINFO")) {
        strGmShipDesc = (String) hmRule.get("DESCRIPTION");
        strGmShipValue = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMPHONE")) {
        strGmPhone = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("FEDEXPHONE")) {
        strFedexDesc = (String) hmRule.get("DESCRIPTION");
        strFedexValue = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMFROMMAIL")) {
        strGmFromMailDesc = (String) hmRule.get("DESCRIPTION");
        strGmFromMailValue = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("EMAILSUB")) {
        strGmSubjectDesc = (String) hmRule.get("DESCRIPTION");
        strGmSubjectValue = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMMAILFOOTER")) {
        strGmMailFooter = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMCSMAILID")) {
        strCSMail = (String) hmRule.get("RULEVALUE");
      }
      if (ruleId.equals("GMITMAILID")) {
        strITMail = (String) hmRule.get("RULEVALUE");
      }
    }

    hmRule.put("strRule", strRule);
    hmRule.put("gmFromMailDesc", strGmFromMailDesc);
    hmRule.put("gmFromMailValue", strGmFromMailValue);
    hmRule.put("gmSubjectValue", strGmSubjectValue);
    hmRule.put("gmCSMail", strCSMail);
    hmRule.put("gmITMail", strITMail);
    hmRule.put("SHIPDESC", strGmShipDesc);
    hmRule.put("SHIPMAIL", strGmShipValue);
    hmRule.put("SHIPPONE", strGmPhone);
    hmRule.put("FEDEXDESC", strFedexDesc);
    hmRule.put("FEDEXPHONE", strFedexValue);
    hmRule.put("GMFOOTER", strGmMailFooter);
    return hmRule;
  }

  /**
   * genearetShippingDocument - This method will be used to generate Shipping Document from Jasper
   * to PDF Format in specified location
   * 
   * @param String strTransID
   * @exception Exception
   */
  public String genearetShippingDocument(String strTransId) throws Exception {
    String strFileName = "";
    String strRuleDOmain = "";
    String strOrdPackJasName = "";
    String strProjectType = "";
    String strApplnDateFmt = "";
    String strTodaysDate = "";
    String strCompId = "";
    String strCompanyName = "";
    String strCompanyShortName = "";
    String strCompanyAddress = "";
    String strCompanyPhone = "";
    String strHeadAddress = "";
    String strCountryCode = GmCommonClass.countryCode;

    Date dtFromDate = null;
    Date dtPrintDate = new Date();

    HashMap hmResult = new HashMap();
    HashMap hmReportHeaderDetails = new HashMap();
    GmCustomerBean gmCustomerBean = new GmCustomerBean();
    GmJasperReport gmJasperReport = new GmJasperReport();

    strRuleDOmain =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("1001_DOMAINDB", "DOMAINPACKSLIP"));
    strOrdPackJasName =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("ORDPACKJAS", "DOMAINJASPER"));
    String strPackHeader =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("FDA_REGIST_VAL", "PACKSLIP"));
    String strPackFooter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("FDA_FOOTER_VAL", "PACKSLIP"));
    if (!strRuleDOmain.equals("")) {
      strProjectType = "PACKSLIP";
    }
    strApplnDateFmt = GmCommonClass.parseNull(GmCommonClass.getRuleValue("DATEFMT", "DATEFORMAT"));
    SimpleDateFormat dfDateFmt = new SimpleDateFormat(strApplnDateFmt + " HH:mm:ss");
    hmResult =
        GmCommonClass.parseNullHashMap(gmCustomerBean.loadOrderDetails(strTransId, strProjectType));
    HashMap hmorderDetails = GmCommonClass.parseNullHashMap((HashMap) hmResult.get("ORDERDETAILS"));
    ArrayList alCartDetails =
        GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("CARTDETAILS"));
    String strOrdID = GmCommonClass.parseNull((String) hmorderDetails.get("ID"));
  //  String strOrdDate = GmCommonClass.parseNull((String) hmorderDetails.get("ODT"));
    String strOrdDate = GmCommonClass.getStringFromDate((java.util.Date)hmorderDetails.get("ODT"),strApplnDateFmt); 

    String strShipAddress = GmCommonClass.parseNull((String) hmorderDetails.get("SHIPADD"));
    String strCustPO = GmCommonClass.parseNull((String) hmorderDetails.get("PO"));
    String strShipCarrier = GmCommonClass.parseNull((String) hmorderDetails.get("CARRIER"));
    String strShipMode = GmCommonClass.parseNull((String) hmorderDetails.get("SHIPMODE"));
    String strAttenOver = GmCommonClass.parseNull((String) hmorderDetails.get("ATTENTO"));

   

    strTodaysDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
    dtFromDate = GmCommonClass.getStringToDate(strOrdDate, strApplnDateFmt);
    // dtPrintDate = GmCommonClass.getStringToDate(strTodaysDate,strApplnDateFmt);
    String strPrintDate = dfDateFmt.format(dtPrintDate); // To get the current date with time
    strCompId = GmCommonClass.parseNull((String) hmorderDetails.get("COMPID"));
    if (strCountryCode.equals("en")) {
      if (!strCompId.equals("")) {
        strCompanyName = GmCommonClass.parseNull(GmCommonClass.getString(strCompId + "_COMPNAME"));
        strCompanyShortName =
            GmCommonClass.parseNull(GmCommonClass.getString(strCompId + "_COMP_SHORT_NAME"));
        strCompanyAddress =
            GmCommonClass.parseNull(GmCommonClass.getString(strCompId + "_COMPADDRESS"));
        strCompanyPhone = GmCommonClass.parseNull(GmCommonClass.getString(strCompId + "_PH"));
      }
      strHeadAddress = strCompanyName + "," + strCompanyAddress + "," + strCompanyPhone;
    } else {
      // OUS code merge for Global_Hormonization
      strCompanyAddress = GmCommonClass.getString("GMCOMPANYADDRESS");
      strCompanyName = GmCommonClass.getString("GMCOMPANYNAME");
      strCompanyPhone = GmCommonClass.getString("GMCOMPANYPHONE");
      strHeadAddress = strCompanyName + "," + strCompanyAddress;
      if (strCountryCode.equals("sa")) {
        strHeadAddress = strHeadAddress + "," + "Phone no:" + strCompanyPhone;
      }
    }

    hmReportHeaderDetails.put("ORDERID", strOrdID);
    hmReportHeaderDetails.put("ORDDATE", strOrdDate);
    hmReportHeaderDetails.put("SHIPADDRESS", strShipAddress);
    hmReportHeaderDetails.put("CUSTPO", strCustPO);
    hmReportHeaderDetails.put("SHIPMODE", strShipMode);
    hmReportHeaderDetails.put("SHIPCARRIER", strShipCarrier);
    hmReportHeaderDetails.put("ATTENTIONOVER", strAttenOver);
    hmReportHeaderDetails.put("PRINTDATE", strPrintDate);
    hmReportHeaderDetails.put("HEADERADDRESS", strHeadAddress);
    hmReportHeaderDetails.put("PHEADER", strPackHeader);
    hmReportHeaderDetails.put("PFOOTER", strPackFooter);
    // Method to generate the PDF file as Shipping document from JASPER Report
    strFileName =
        createPDFFile(strTransId, gmJasperReport, new HashMap(hmReportHeaderDetails),
            strOrdPackJasName, alCartDetails);
    return strFileName;
  }

  /**
   * generateFileAttachments - This method will be used to generate Attachments[Shipping Doc and
   * Other files] for the transaction
   * 
   * @param String strFileName, String strTransactionID
   * @exception Exception
   */
  public String generateFileAttachments(String strFileName, String strTransId) throws Exception {
    String strShipAttachemnts = "";
    String strFilename = "";
    String strFilePath = "";
    String strfileAttachments = "";
    String strShippingDocument = "";
    // the following code is written for sending the attachment of Shipping Document
    strFilePath = GmCommonClass.parseNull(GmCommonClass.getString("ORDERSHIPMENT"));
    strFilePath = strFilePath + strTransId;
    File folder = new File(strFilePath);
    if (!folder.exists()) {
      folder.mkdir();
    }
    File[] listOfShipDocs = folder.listFiles();
    for (int i = 0; i < listOfShipDocs.length; i++) {
      if (listOfShipDocs[i].isFile() && listOfShipDocs[i].getName().contains(strTransId)) {
        strShippingDocument =
            strShippingDocument + strFilePath + "\\" + listOfShipDocs[i].getName() + ",";
      }
    }

    // the following code is written for sending the other attachments along with the Shipping
    // Document
    String strAttachedFilePath =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue("FILE", "MAILATTACHMENT"));
    strAttachedFilePath = strAttachedFilePath + strTransId + "\\";
    File attachedFileFolder = new File(strAttachedFilePath);
    if (attachedFileFolder.exists()) {
      File[] listOfAttachedFiles = attachedFileFolder.listFiles();
      for (int i = 0; i < listOfAttachedFiles.length; i++) {
        if (listOfAttachedFiles[i].isFile()) {
          strfileAttachments =
              strfileAttachments + strAttachedFilePath + listOfAttachedFiles[i].getName() + ",";
        }
      }
    }

    strShipAttachemnts = strShippingDocument + "," + strfileAttachments;
    return strShipAttachemnts;
  }

  /**
   * createPDFFile - This method will be used to generate email template and send mails for those
   * records
   * 
   * @param String strFile , gmJasperreport , HashMap HmParam , String strJasperName , ArrayList
   *        alDataList
   * @exception Exception
   */

  public String createPDFFile(String strFile, GmJasperReport gmJasperReport, HashMap hmParam,
      String strJasperName, ArrayList alDataList) throws Exception {
    String fileName = "";
    String strFilePath = GmCommonClass.getString("ORDERSHIPMENT");
    strFilePath = strFilePath + strFile;
    fileName = strFilePath + "\\" + strFile + ".pdf";
    File newFile = new File(strFilePath);
    if (!newFile.exists()) {
      newFile.mkdirs();
    }
    gmJasperReport.setJasperReportName(strJasperName);
    gmJasperReport.setHmReportParameters(hmParam);
    gmJasperReport.setReportDataList(alDataList);
    gmJasperReport.exportJasperToPdf(fileName);
    return fileName;
  }
}
