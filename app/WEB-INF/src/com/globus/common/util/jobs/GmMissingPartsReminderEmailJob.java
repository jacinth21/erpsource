package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.quartz.JobDataMap;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.db.GmDBManager;
import com.globus.operations.logistics.beans.GmTicketBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmMissingPartsReminderEmailJob extends GmActionJob {

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    // TODO Auto-generated method stub
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
    GmCommonBean gmCommonBean = new GmCommonBean(gmDataStoreVO);
    GmTicketBean gmTicketBean = new GmTicketBean(gmDataStoreVO);
    ArrayList alReqDetails = new ArrayList();
    HashMap hmReqDetails = new HashMap();
    ArrayList alDetails = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmRuleData = new HashMap();
    String strMsg = "";
    int intEmailNo = 0;
    String strReqId = "";
    String strReqDetId = "";
    String strSkipFlg = "";
    String strRuleData = "";
    log.debug("called reminder job");
    DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    hmRuleData.put("RULEID", "FINAL");
    // hmRuleData.put("RULEID", "SECOND");
    hmRuleData.put("RULEGROUP", "EMAIL");
    strRuleData = gmCommonBean.getRuleValue(hmRuleData);

    gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_missingparts_req_det", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    // second reminder to be sent 7 days after the manual email was sent
    // no second reminder to be sent. only final
    gmDBManager.setInt(1, Integer.parseInt(strRuleData));
    gmDBManager.setString(2, "N");
    gmDBManager.execute();

    alReqDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    GmCalenderOperations.setTimeZone(gmDataStoreVO.getCmptzone());
    String strTodaysDate = GmCalenderOperations.getCurrentDate("dd/MM/yyyy");
    strTodaysDate = strTodaysDate.substring(0, 2);
    log.debug("strTodaysDate:  " + strTodaysDate + " strRuleData: " + strRuleData);
    // hmRuleData.put("RULEID", "FINAL");
    // hmRuleData.put("RULEGROUP","EMAIL");
    // strRuleData = gmCommonBean.getRuleValue(hmRuleData);
    strTodaysDate = "1";

    if (strTodaysDate.equals(strRuleData)) { // set parameters for final reminder
      strMsg =
          "We have not received the above-mentioned part(s). Please return the part(s) by the Expected Return Date to avoid charges.";
      intEmailNo = 3;
    } /*
       * else {// set parameters for second reminder strMsg =
       * "Please help us in resolving this issue with the missing parts by the Expected Return Date. Thank you in advance for your help."
       * ; intEmailNo = 2; }
       */

    for (Iterator iter = alReqDetails.iterator(); iter.hasNext();) {
      hmReqDetails = (HashMap) iter.next();
      strReqId = (String) hmReqDetails.get("REQ_ID");
      strReqDetId = (String) hmReqDetails.get("REQ_DETAIL_ID");
      strSkipFlg = (String) hmReqDetails.get("SKIPFLG"); // checking in the query for email_date NOT
                                                         // NULL

      log.debug("strReqId " + strReqId);
      log.debug("strReqDetId " + strReqDetId);
      // if the 2nd mail date is greater than final email date then skip that req id. as final email
      // wud have been already sent.
      if (strTodaysDate.equals(strRuleData)) {
        HashMap hmParam = new HashMap();
        hmParam.put("REQID", strReqId);
        hmParam.put("REQDETID", strReqDetId);
        hmParam.put("MSG", strMsg);
        hmParam.put("USERID", "30301");
        hmParam.put("USERNAME", GmCommonClass.getJiraConfig("PORTAL_JIRA_USER"));

        hmReturn = gmTicketBean.sendMissingPartsEmail(gmDBManager, intEmailNo, hmParam);

        gmCommonBean.saveLog(gmDBManager, strReqId, "Missing parts Final Reminder sent to "
            + hmReturn.get("REPNAME"), "30301", "1247");
      } /*
         * else if (strSkipFlg.equals("N")) { hmReturn =
         * gmInHouseSetsTransBean.sendMissingPartsEmail(gmDBManager, strReqId, strReqDetId, strMsg,
         * intEmailNo); gmCommonBean.saveLog(gmDBManager, strReqId,
         * "Missing parts 2nd Reminder sent to " + hmReturn.get("REPNAME"), "30301", "1247"); }
         */
    }
    gmDBManager.commit();
  }

}
