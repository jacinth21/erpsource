package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmClinicalQueriesEmailJob extends GmActionJob{

	public void execute(JobDataMap jobDataMap) throws Exception {
		// TODO Auto-generated method stub
		GmDBManager gmDBManager = new GmDBManager();
		GmEmailProperties emailProps = new GmEmailProperties();
		ArrayList alSiteDetails = new ArrayList();
		ArrayList alPatientDetails = new ArrayList();
		HashMap hmSiteDetails = new HashMap();
		HashMap hmParams = new HashMap();
		String strStudyName ="";
		String strToEmail ="";
		String strCCEmail ="";
		String strOutstIssues="";
		String strSubject="";
		String strCC="";
		String strSiteId="";
		
		Logger log = GmLogger.getInstance(this.getClass().getName());
		emailProps.setSender(GmCommonClass.getEmailProperty("GmQueryEmail." + GmEmailProperties.FROM));
		emailProps.setMimeType(GmCommonClass.getEmailProperty("GmQueryEmail." + GmEmailProperties.MIME_TYPE));
		strCC =GmCommonClass.getEmailProperty("GmQueryEmail." + GmEmailProperties.CC);
		strSubject = GmCommonClass.getEmailProperty("GmQueryEmail." + GmEmailProperties.SUBJECT);
		
		gmDBManager.setPrepareString("gm_pkg_cm_query_info.gm_fch_site_info", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);				
		gmDBManager.execute();
		
		alSiteDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		for (Iterator iter = alSiteDetails.iterator(); iter.hasNext();) {
			hmSiteDetails = (HashMap) iter.next();
			
			strStudyName = (String)hmSiteDetails.get("STUDYNM");
			strToEmail = GmCommonClass.parseNull((String)hmSiteDetails.get("TO_EMAIL_ID"));
			strCCEmail = (String)hmSiteDetails.get("CC_EMAIL_IDS");
			strOutstIssues = (String)hmSiteDetails.get("OUTSTANDING_ISSUES");
			strSiteId =  (String)hmSiteDetails.get("SITE_ID");
				
			strSubject = GmCommonClass.replaceAll(strSubject, "#<Study_name>", strStudyName);
			emailProps.setSubject(strSubject);
			//strToEmail = strToEmail.equals("") ?"bgupta@globusmedical.com":strToEmail+","+"bgupta@globusmedical.com";
			emailProps.setRecipients(strToEmail);
			emailProps.setCc(strCC+","+strCCEmail);
			
			gmDBManager.setPrepareString("gm_pkg_cm_query_info.gm_fch_patient_info", 2);
			gmDBManager.setString(1, strSiteId);
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.execute();
			
			alPatientDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
			gmDBManager.close();
			hmParams.put("OUTSTANDING_ISSUES", strOutstIssues);
			hmParams.put("GMCLPHONE", GmCommonClass.getEmailProperty("GmQueryEmail" + ".GMCLPHONE"));
			hmParams.put("GMCLEMAIL", GmCommonClass.getEmailProperty("GmQueryEmail" + ".GMCLEMAIL"));
			GmJasperMail jasperMail = new GmJasperMail();
			jasperMail.setJasperReportName("/GmClinicalQueriesEmailJob.jasper");
			jasperMail.setAdditionalParams(hmParams);
			jasperMail.setReportData(alPatientDetails);
			jasperMail.setEmailProperties(emailProps);
			jasperMail.sendMail();
		}
	}

}
