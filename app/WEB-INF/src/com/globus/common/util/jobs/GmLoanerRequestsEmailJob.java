/**
 * FileName : GmLoanerRequestsEmailJob.java Description : Author : sthadeshwar Date : Mar 24, 2009
 * Copyright : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRepEmailProperties;
import com.globus.common.beans.GmResourceBundleBean;


/**
 * @author sthadeshwar
 * 
 */
public abstract class GmLoanerRequestsEmailJob extends GmActionJob {

  Logger log = GmLogger.getInstance(this.getClass().getName());
  SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");

  public GmEmailProperties getEmailProperties(String strTemplate, String strRepId) throws AppError {
    String strCompanyId = getCompanyId();
    strCompanyId = strCompanyId.equals("") ? "1000" : strCompanyId;
    String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompanyId());
    // getting the email properties object
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    GmEmailProperties emailProps = new GmRepEmailProperties(strRepId, strTemplate);
    emailProps.setSender(gmResourceBundleBean.getProperty(strTemplate + "." + GmEmailProperties.FROM));
    emailProps.setCc(gmResourceBundleBean.getProperty(strTemplate + "." + GmEmailProperties.CC));
    // emailProps.setBcc(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.BCC));
    emailProps.setSubject(gmResourceBundleBean.getProperty(strTemplate + "."
        + GmEmailProperties.SUBJECT));
    emailProps.setMimeType(gmResourceBundleBean.getProperty(strTemplate + "."
        + GmEmailProperties.MIME_TYPE));
    return emailProps;
  }

}
