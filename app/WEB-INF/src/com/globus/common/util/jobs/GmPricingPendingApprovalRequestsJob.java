/**
 * FileName    : GmPricingPendingApprovalRequestsJob.java
 * Description :
 * Author      : Karthik Somanathan
 * Date        : Apr 25, 2018 
 * Copyright   : Globus Medical Inc
 */

package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.jobs.GmActionJob;
import com.globus.common.util.jobs.GmJob;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmPricingPendingApprovalRequestsJob extends GmActionJob {

	public static final String TEMPLATE_NAME = "GmPricingPendingRequestsJob";
	Logger log = GmLogger.getInstance(this.getClass().getName());

	public void execute(JobDataMap jobDataMap) throws Exception 
	{
		String strToEmail = "";
		String strCc = "";
		String strMimeType = "";
		String strFrom = "";
		String strsubject ="";
		String strADId = "";
		String strADEmail = "";
		ArrayList  alPRDetails = new ArrayList();
		ArrayList  alPendingApprovalAD = new ArrayList();
				
		GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
		GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);
		log.debug("jobDataMap = " + jobDataMap);
		
		gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_pending_approval_ad_id", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alPendingApprovalAD = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1)));
		gmDBManager.close();
		
		HashMap hmPriceRqtRepDtls = new HashMap();
		HashMap hmParams = new HashMap();
		HashMap hmReturn = new HashMap();
		
		for(Iterator iter = alPendingApprovalAD.iterator(); iter.hasNext();)
		{
			try
			{
				hmPriceRqtRepDtls = (HashMap)iter.next();
				strADId = GmCommonClass.parseNull((String)hmPriceRqtRepDtls.get("ADID"));
				strADEmail = GmCommonClass.parseNull((String)hmPriceRqtRepDtls.get("ADVPEMAIL"));
				if(!strADEmail.equals("")) 
					{
					gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_pr_pending_aprvl_req", 2);
					gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
					gmDBManager.setString(1, strADId);
					gmDBManager.execute();
					alPRDetails = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2)));
					gmDBManager.close();
					
					hmParams.put("ALADAPPROVALDTL", alPRDetails);
					
					GmEmailProperties emailProps = getEmailProperties(TEMPLATE_NAME);
					strsubject  = emailProps.getSubject();
					strFrom 	= emailProps.getSender();
					strMimeType = emailProps.getMimeType();
					
					emailProps.setMimeType(strMimeType);
					emailProps.setRecipients(strADEmail);
					emailProps.setSender(strFrom);
					emailProps.setSubject(strsubject);
					
					GmJasperMail jasperMail = new GmJasperMail();
					jasperMail.setJasperReportName("/GmPriceRequestApprovalRemainderEmail.jasper");
					jasperMail.setAdditionalParams(hmParams);
					jasperMail.setReportData(null);
					jasperMail.setEmailProperties(emailProps);
					
					hmReturn = jasperMail.sendMail();
					}
				}
			catch(Exception e)
			{
				log.error("Exception in GmPricingPendingInitiatedRequestsJob body: " + e.getMessage());
				hmReturn.put(GmJob.EXCEPTION, e);
			}
			Exception ex = (Exception)hmReturn.get(GmJob.EXCEPTION);
			if(ex != null)
			{
				log.error("Exception in sendJasperMail");
				setJobStatus(GmJob.JOB_STATUS_FAIL);
				setJobException(GmCommonClass.getExceptionStackTrace(ex,"<br>"));
				setAdditionalParam((String)hmReturn.get("ORIGINALMAIL"));
				sendJobExceptionEmail();
				throw ex;
			}
		}
		
	}
	
	//GmEmailProperties will helps to get values of From and Subject from EmailTemplateProperties
			public GmEmailProperties getEmailProperties(String strTemplate){
				GmEmailProperties emailProps = new GmEmailProperties();
				emailProps.setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
				emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT));
				emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MIME_TYPE));
				return emailProps;
			}
			
}
