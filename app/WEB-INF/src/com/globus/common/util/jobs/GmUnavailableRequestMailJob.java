/**
 * FileName : GmReceiveRequestsEmailJob.java Description : Author : sthadeshwar Date : Mar 24, 2009
 * Copyright : Globus Medical Inc
 */
package com.globus.common.util.jobs;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;

import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author sthadeshwar
 * 
 */
public class GmUnavailableRequestMailJob extends GmLoanerRequestsEmailJob {

  public static final String TEMPLATE_NAME = "GmUnavailableRequestMailJob";

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);

    ArrayList alRepId = new ArrayList();
    ArrayList alRequestDetails = new ArrayList();

    HashMap hmRepDetails = new HashMap();
    HashMap hmParams = new HashMap();
    HashMap hmReturn = new HashMap();

    // hmParams = getShippingDetails();

    /* Getting all the rep id's who have submitted requests today */
    gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_unavailable_loaner_reps", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alRepId = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    for (Iterator iter = alRepId.iterator(); iter.hasNext();) {
      try {
        hmParams.put("GMCSPHONE", GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSPhone"));
        hmParams.put("GMCSEMAIL", GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSEmail"));

        hmRepDetails = (HashMap) iter.next();
        String strRepId = (String) hmRepDetails.get("REP_ID");
        /* Getting request details for each rep */
        gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_unavailable_requests", 2);
        gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
        gmDBManager.setString(1, strRepId);
        gmDBManager.execute();

        alRequestDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
        gmDBManager.close();

        hmParams.putAll(hmRepDetails);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1); // Adding 1 day to current date

        GmEmailProperties emailProps = getEmailProperties(TEMPLATE_NAME, strRepId);
        String strSubject = emailProps.getSubject();
        strSubject =
            GmCommonClass.replaceAll(strSubject, "#<REP_NAME>", (String) hmParams.get("REP_NAME"));
        strSubject = GmCommonClass.replaceAll(strSubject, "#<PSDATE>", df.format(cal.getTime()));

        emailProps.setRecipients((String) hmRepDetails.get("EMAIL"));
        emailProps.setSubject(strSubject);

        GmJasperMail jasperMail = new GmJasperMail();
        jasperMail.setJasperReportName("/GmEmailSetUnavailable.jasper");
        jasperMail.setAdditionalParams(hmParams);
        jasperMail.setReportData(alRequestDetails);
        jasperMail.setEmailProperties(emailProps);

        hmReturn = jasperMail.sendMail();

        // HashMap hmReportData = new HashMap();
        // hmReportData.put("FILENAME", "/GmEmailSetUnavailable.jasper");
        // hmReportData.put("PARAMS", hmParams);
        // hmReportData.put("DETAILS", alRequestDetails);
        // hmReportData.put("EMAILPROPS", emailProps);
        //
        // hmReturn = (HashMap)GmCommonClass.sendRequest("/GmJasperMailServlet", hmReportData,
        // true);
        // log.error("Request for sending jasper mail sent ...");

        // GmJasperReport gmJasperReport = new GmJasperReport();
        // hmReturn = gmJasperReport.sendJasperMail("/GmEmailSetUnavailable.jasper", hmParams,
        // alRequestDetails, emailProps);

      } catch (Exception e) {
        log.error("Exception in job body: " + e.getMessage());
        hmReturn.put(GmJob.EXCEPTION, e);
      }
      Exception ex = (Exception) hmReturn.get(GmJob.EXCEPTION);
      if (ex != null) {
        log.error("Exception in sendJasperMail " + ex.getMessage());
        setJobStatus(GmJob.JOB_STATUS_FAIL);
        setJobException(GmCommonClass.getExceptionStackTrace(ex, "<br>"));
        setAdditionalParam((String) hmReturn.get("ORIGINALMAIL"));
        sendJobExceptionEmail();
        throw ex;
      }
    }
    gmDBManager.close();
  }


}
