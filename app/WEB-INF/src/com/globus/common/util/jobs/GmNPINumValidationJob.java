/**
 * FileName : GmNPINumValidationJob.java Description : To validate the NPI details saved for orders
 * Author : arajan
 */
package com.globus.common.util.jobs;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmNPINumValidationJob extends GmActionJob {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  @Override
  public void execute(JobDataMap jobDataMap) throws Exception {
    GmDataStoreVO gmDataStoreVO = getGmDataStoreVO(jobDataMap);
    HashMap hmParam = GmCommonClass.parseNullHashMap((HashMap) jobDataMap.get("HMPARAM"));
    String strCountry = GmCommonClass.parseNull((String) hmParam.get("COUNTRY"));
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);

    if (strCountry.equalsIgnoreCase("US")) {
      // To void the NPI records if the order is voided
      gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_sav_void_npi", 0);
      gmDBManager.execute();
      // For validating US record
      gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_upd_npi_valid_data_us", 0);
      gmDBManager.execute();
    }
    if (strCountry.equalsIgnoreCase("OUS")) {
      // To void the NPI records if the order is voided
      gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_sav_void_npi", 0);
      gmDBManager.execute();
      // For validating OUS record
      gmDBManager.setPrepareString("gm_pkg_cm_npi_process_txn.gm_upd_npi_valid_data_ous", 0);
      gmDBManager.execute();
    }
    gmDBManager.commit();

  }

}
