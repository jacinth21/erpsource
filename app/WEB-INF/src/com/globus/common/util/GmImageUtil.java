/**
 * Description : This Class is Used for evaluating a template. 
 * Copyright : Globus Medical Inc
 *   
 */
package com.globus.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

public class GmImageUtil {

	public static HashMap getImageHeightWidth(String url ,String format)
	  throws Exception {
		System.out.println("Entered in to getImageProperties");
		HashMap propertyMap=null;
		int width=0;
		int height=0;
		try {
			
			InputStream inputStream = getImageInputStream(url);
			
			propertyMap=new HashMap();
			Iterator readers = ImageIO.getImageReadersBySuffix(format);
	        ImageReader reader = (ImageReader) readers.next();
	        ImageInputStream iis = ImageIO.createImageInputStream(inputStream);
        reader.setInput(iis, true);
        width = reader.getWidth(0);
        height = reader.getHeight(0);
        propertyMap.put("HEIGHT",height+"");
        propertyMap.put("WIDTH",width+"");
						
		}catch(Exception e){
		e.printStackTrace();
		return null;
		}
		System.out.println("Exiting in to getImageProperties");
	return	propertyMap;
	}
	
	  public static InputStream getImageInputStream(String url) throws Exception
	  {
	    InputStream stream = null;

	    if(url.startsWith("http:"))
	    {
	      stream = new URL(url).openConnection().getInputStream();
	    }
	    else
	    {
	      stream = new FileInputStream(new File(url));
	    }

	    return stream;
	  }
}



