package com.globus.common.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org._1sync.create.BooleanType;
import org._1sync.create.Envelope;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;


// TODO: Auto-generated Javadoc
/**
 * The Class GmUDIUtil.
 */
public class GmUDIUtil {

  /** The log. */
  Logger log = GmLogger.getInstance(this.getClass().getName());
  public static final String CREATE_UDI_XML_FAILED = "GmUDISubCreateXmlFailedEmail";


  /**
   * Create1ws item xml.
   * 
   * @param strXMLPath the str xml path
   * @param envelope the envelope
   * @throws JAXBException the JAXB exception
   */
  public void createXML(String strXMLPath, Envelope envelope) throws JAXBException {
    String strHttpXsdPath =
        GmCommonClass.parseNull(GmCommonClass.getUDIFdaXmlConfig("CREATE_UDI_XML_HTTP_XSD_PATH"));

    File file = new File(strXMLPath);
    JAXBContext jaxbContext = JAXBContext.newInstance(Envelope.class);
    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
    // output pretty printed
    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    jaxbMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, strHttpXsdPath);
    jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
    jaxbMarshaller.marshal(envelope, file);// marshal(envelope,file);


  }

  /**
   * Gets the unique temp dir.
   * 
   * @param strReqConTempFilePath the str req con temp file path
   * @return the unique temp dir
   */
  public static String getUniqueTempDir(String strReqConTempFilePath) {
    String strTempDirName = Long.toString(System.nanoTime());
    String strPathTempDir = strReqConTempFilePath + strTempDirName + "\\";
    File outFile = new File(strPathTempDir);
    outFile.mkdir();
    return strPathTempDir;
  }

  /**
   * Copy files to dest and del from src.
   * 
   * @param strSrcFilePath the str src file path
   * @param strDestPathTempDir the str dest path temp dir
   * @throws AppError the app error
   * @throws Exception the exception
   */
  public static void copyFilesToDestAndDelFromSrc(String strSrcFilePath, String strDestPathTempDir)
      throws AppError, Exception {
    String strFileName = "";
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
    File folderCopy = new File(strSrcFilePath);
    File[] folderCopylistOfFiles = folderCopy.listFiles();

    for (int i = 0; i < folderCopylistOfFiles.length; i++) {
      if (folderCopylistOfFiles[i].isFile()) {
        strFileName = folderCopylistOfFiles[i].getName();
        folderCopylistOfFiles[i].lastModified();
        // Copy files from destination folder in AS2 to Temp Folder Created for this Job
        gmCommonUploadBean.copyFile(strSrcFilePath + strFileName, strDestPathTempDir + strFileName);


        // Delete files from source folder
        gmCommonUploadBean.deleteFile(strSrcFilePath + strFileName);
      } else if (folderCopylistOfFiles[i].isDirectory()) {
        System.out.println("Directory: " + strFileName);
      }


    }

  }

  /**
   * Sort file by last modified date.
   * 
   * @param folderCopylistOfFiles the folder copylist of files
   */
  public static void sortFileByLastModifiedDate(File[] folderCopylistOfFiles) {
    Arrays.sort(folderCopylistOfFiles, new Comparator() {
      public int compare(Object file1, Object file2) {

        if (((File) file1).lastModified() < ((File) file2).lastModified()) {
          return -1;
        } else if (((File) file1).lastModified() > ((File) file2).lastModified()) {
          return +1;
        } else {
          return 0;
        }
      }

    });
  }

  /**
   * Gets the xml string val.
   * 
   * @param strDEAttr the str de attr
   * @return the xml string val
   */
  public static String getXmlStringVal(String strDEAttr) {

    String val = GmCommonClass.parseNull(strDEAttr).equals("Y") ? "TRUE" : "FALSE";

    return val;

  }

  /**
   * Gets the xml string val.
   * 
   * @param strDEAttr the str de attr
   * @return the xml string val
   */
  public static String getXmlYorN(String strDEAttr) {

    // PMT-14581
   // String val = GmCommonClass.parseNull(strDEAttr).equals("Y") ? "YES" : "NO";
	  String val = GmCommonClass.parseNull(strDEAttr).equals("Y") ? "TRUE" : "FALSE";

    return val;

  }

  /**
   * Gets the xml boolean type val.
   * 
   * @param strDEAttr the str de attr
   * @return the xml boolean type val
   */
  public static BooleanType getXmlBooleanTypeVal(String strDEAttr) {

    BooleanType val =
        GmCommonClass.parseNull((strDEAttr)).equals("Y") ? BooleanType.TRUE : BooleanType.FALSE;

    return val;

  }

  /**
   * Validate xml schema.
   * 
   * @param xsdPath the xsd path
   * @param xmlPath the xml path
   * @return true, if successful
   */
  public static boolean validateXMLSchema(String xsdPath, String xmlPath) {

    // Alternate Code from @ http://www.tutorialspoint.com/xsd/xsd_validation.htm
    try {
      SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      Schema schema = factory.newSchema(new File(xsdPath));
      Validator validator = schema.newValidator();
      validator.validate(new StreamSource(new File(xmlPath)));
    } catch (IOException e) {
      System.out.println("Exception: " + e.getMessage());
      return false;
    } catch (SAXException e1) {
      System.out.println("SAX Exception: " + e1.getMessage());
      return false;
    }
    return true;
  }


  /*
   * 
   * Converts XMLGregorianCalendar to java.util.Date in Java
   */

  /**
   * To date.
   * 
   * @param calendar the calendar
   * @return the date
   */
  public static Date toDate(XMLGregorianCalendar calendar) {

    if (calendar == null) {

      return null;

    }

    return calendar.toGregorianCalendar().getTime();

  }


  /**
   * Gets the string to date.
   * 
   * @param strDate the str date
   * @param strformat the strformat
   * @return the string to date
   */
  public static Date getStringToDate(String strDate, String strformat) {

    Date date = null;
    if (!strDate.equals("")) {
      DateFormat formatter = new SimpleDateFormat(strformat);
      try {
        date = formatter.parse(strDate);
      } catch (ParseException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    }
    return date;

  }

  /**
   * Date to xml date.
   * 
   * @param date the date
   * @return the XML gregorian calendar
   */
  public static XMLGregorianCalendar dateToXmlDate(Date date) {
    // DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    String strDate = dateFormat.format(date);
    try {
      XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(strDate);
      return xmlDate;
    } catch (DatatypeConfigurationException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Send jira email.
   * 
   * @param hmGeneralParam the hm general param
   */
  public static void sendJiraEmail(HashMap hmGeneralParam) {
    try {
      String strFileName = GmCommonClass.parseNull((String) hmGeneralParam.get("strFileName"));
      String strMsgId = GmCommonClass.parseNull((String) hmGeneralParam.get("strMsgId"));
      String strXMLType = GmCommonClass.parseNull((String) hmGeneralParam.get("XMLTYPE"));
      String strTempStringforCreateandResponse =
          GmCommonClass.parseNull((String) hmGeneralParam.get("strTempStringforCreateandResponse"));
      String strDestFileName =
          GmCommonClass.parseNull((String) hmGeneralParam.get("strDestFileName"));
      String strDestPathAs2 =
          GmCommonClass.parseNull((String) hmGeneralParam.get("strDestPathAs2"));
      String strRequestXsdPath =
          GmCommonClass.parseNull((String) hmGeneralParam.get("strRequestXsdPath"));
      Exception ex = (Exception) hmGeneralParam.get("exception");


      GmEmailProperties emailProps = new GmEmailProperties();
      emailProps.setSender(GmCommonClass.getEmailProperty(CREATE_UDI_XML_FAILED + "."
          + GmEmailProperties.FROM));
      emailProps.setRecipients(GmCommonClass.getEmailProperty(CREATE_UDI_XML_FAILED + "."
          + GmEmailProperties.TO));
      emailProps.setCc(GmCommonClass.getEmailProperty(CREATE_UDI_XML_FAILED + "."
          + GmEmailProperties.CC));
      emailProps.setMimeType(GmCommonClass.getEmailProperty(CREATE_UDI_XML_FAILED + "."
          + GmEmailProperties.MIME_TYPE));

      String strMsgBody =
          GmCommonClass.getEmailProperty(CREATE_UDI_XML_FAILED + "."
              + GmEmailProperties.MESSAGE_BODY);
      strMsgBody = GmCommonClass.replaceAll(strMsgBody, "#<FILENAME>", strFileName);
      strMsgBody = GmCommonClass.replaceAll(strMsgBody, "#<DESTINATIONFILENAME>", strDestFileName);
      strMsgBody = GmCommonClass.replaceAll(strMsgBody, "#<DESTINATIONPATHAS2>", strDestPathAs2);
      strMsgBody = GmCommonClass.replaceAll(strMsgBody, "#<REQUESTXSDPATH>", strRequestXsdPath);
      strMsgBody = GmCommonClass.replaceAll(strMsgBody, "#<MESSAGEID>", strMsgId);
      strMsgBody =
          GmCommonClass.replaceAll(strMsgBody, "#<TEMPSTRINGFORCREATEANDRESPONSE>",
              strTempStringforCreateandResponse);
      strMsgBody =
          GmCommonClass.replaceAll(strMsgBody, "#<UDISUBXMLFILEERROR>",
              GmCommonClass.getExceptionStackTrace(ex, "<br>"));
      strMsgBody = GmCommonClass.replaceAll(strMsgBody, "#<FILENAME>", strFileName);



      String strMessage =
          GmCommonClass.getEmailProperty(CREATE_UDI_XML_FAILED + "." + GmEmailProperties.MESSAGE);
      strMessage = GmCommonClass.replaceAll(strMessage, "#<MessageBody>", strMsgBody);
      // emailProps.setSubject();

      String strSubject =
          GmCommonClass.getEmailProperty(CREATE_UDI_XML_FAILED + "." + GmEmailProperties.SUBJECT);
      strSubject = GmCommonClass.replaceAll(strSubject, "#<XMLTYPE>", strXMLType);

      emailProps.setMessage(strMessage);
      emailProps.setSubject(strSubject);
      emailProps.setAttachment(strDestFileName);

      GmCommonClass.sendMail(emailProps);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

}
