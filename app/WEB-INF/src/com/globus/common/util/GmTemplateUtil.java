/**
 * Description : This Class is Used for evaluating a template. Copyright : Globus Medical Inc
 * 
 */
package com.globus.common.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.tools.generic.ComparisonDateTool;
import org.apache.velocity.tools.generic.EscapeTool;
import org.apache.velocity.tools.generic.MathTool;
import org.apache.velocity.tools.generic.NumberTool;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;

public class GmTemplateUtil {

  public List dataList;
  public HashMap dropDownMaster;
  public String templateName = "";
  public String templateSubDir = "";
  public String fileSeparator = "/";// File.separator;
  public HashMap templateMap = new HashMap();
  String newLineChar = "";
  private GmResourceBundleBean resourceBundle = null;


  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * This Method is used to set the resource bundle object to get the values from the property
   * files.
   * 
   * @param mapKey , dataList
   * 
   */
  public void setResourceBundle(GmResourceBundleBean resourceBundle) throws AppError {
    this.resourceBundle = resourceBundle;
  }

  /**
   * This Method is used to get the DataList by passing the key that was used to set the DataList
   * initially.
   * 
   * @return the List
   */
  public List getDataList(String mapKey) {
    return (List) templateMap.get(mapKey);
  }

  /**
   * This Method is used to set the DataList by passing the key and the List Object. The mapKey
   * should not be null and it has to be unique. When a duplicate Key is used, AppError will be
   * thrown. This List contains the data that has to be rendered in the template.
   * 
   * @param mapKey , dataList
   * 
   */
  public void setDataList(String mapKey, List dataList) throws AppError {
    if (!templateMap.containsKey(mapKey)) {
      templateMap.put(mapKey, dataList);
    } else {
      throw new AppError("APP", "10000");
    }
  }

  /**
   * This Method is used to get the DataMap by passing the key that was used to set the DataMap
   * initially.
   * 
   * @return the HashMap
   */
  public HashMap getDataMap(String mapKey) {
    return (HashMap) templateMap.get(mapKey);
  }

  /**
   * This Method is used to set the DataMap by passing the key and the HashMap. The mapKey should
   * not be null and it has to be unique. When a duplicate Key is used, AppError will be thrown.
   * This HashMap contains the data that has to be used in the template.
   * 
   * @param mapKey , dataList
   */
  public void setDataMap(String mapKey, HashMap dataMap) throws AppError {
    if (mapKey != null && !templateMap.containsKey(mapKey)) {
      templateMap.put(mapKey, dataMap);
    } else {
      throw new AppError("APP", "10000");
    }
  }

  /**
   * This Method is used to get the DropDown master(HashNao) by passing the key that was used to set
   * the Dropdown values initially. The DropDownMasterMap will hold the master data that will be
   * used to populate the Combobox values.
   * 
   * @return the List
   */
  public HashMap getDropDownMasterMap(String mapKey) {
    return (HashMap) templateMap.get(mapKey);
  }

  /**
   * This Method is used to set the DropDown master(ArrayList) . MapKey will be passed in which is
   * unique and not null. dropDownMasterList will be arrayList of HashMap which will have the
   * dropdown values.
   * 
   * mapKey is passed in, which will be unique and not null. dropDownMasterList will have the Master
   * data to populate Combobox.
   */
  public void setDropDownMaster(String mapKey, ArrayList dropDownMasterList) throws AppError {
    if (mapKey != null && !templateMap.containsKey(mapKey)) {
      templateMap.put(mapKey, dropDownMasterList);
    } else {
      throw new AppError("APP", "10000");
    }
  }

  /**
   * This Method is used to set the DropDown master(HashMap) . MapKey will be passed in which is
   * unique and not null. dropDownMasterMap will have the master data to populate the combobox in
   * the template.
   */
  public void setDropDownMaster(String mapKey, HashMap dropDownMasterMap) throws AppError {
    log.debug("setDropDownMaster " + mapKey + " Map" + templateMap);
    if (mapKey != null && !templateMap.containsKey(mapKey)) {
      templateMap.put(mapKey, dropDownMasterMap);
    } else {
      log.debug("setDropDownMaster Else Block");
      throw new AppError("APP", "10000");
    }
  }

  /**
   * This Method is used to get the DropDown master(List) by passing the key that was used to set
   * the Dropdown values initially. The DropDownMasterList will hold the master data that will be
   * used to populate the Combobox values.
   * 
   * @return the List
   */

  public List getDropDownMasterList(String mapKey) {
    return (ArrayList) templateMap.get(mapKey);
  }

  /**
   * This method is used to get the templateName.
   * 
   * @return the templateName
   */
  public String getTemplateName() {
    return templateName;
  }

  /**
   * This method is used to set the TemplateName that will be used to generate the output.
   * 
   * @param templateName
   * 
   */
  public void setTemplateName(String templateName) {
    this.templateName = templateName;
  }

  /**
   * This Method is used to get the templateSubDirectory. TemplateSubDirectory will be set, when the
   * the template to be read is part of the subdirectory in base template directory.
   * 
   * @return the templateSubDir
   */
  public String getTemplateSubDir() {
    return templateSubDir;
  }

  /**
   * This Method is used to set the templateSubDirectory. TemplateSubDirectory will be set, when the
   * the template to be read is part of the subdirectory in base template directory.
   * 
   * @param templateSubDir
   * 
   */
  public void setTemplateSubDir(String templateSubDir) {
    this.templateSubDir = templateSubDir;
  }

  /**
   * @return the fileSeparator
   */
  public String getFileSeparator() {
    return fileSeparator;
  }

  /**
   * @param fileSeparator the fileSeparator to set
   */
  public void setFileSeparator(String fileSeparator) {
    this.fileSeparator = fileSeparator;
  }

  /**
   * This method is used to set the newLineCharacter that will be used while reading the template.
   * 
   * @param blUseNewLineChar the blUseNewLineChar to set
   */
  public void setNewLineChar(String newLineChar) {
    this.newLineChar = newLineChar;
  }

  /**
   * @return the blUseNewLineChar
   */
  public String getNewLineChar() {
    return newLineChar;
  }


  /**
   * This is the coreMethod that is used to populate data in the template. All the master/required
   * information are set before calling this method. This method will TemplateSubDirectory will be
   * set, when the the template to be read is part of the subdirectory in base template directory.
   * 
   * @param templateSubDir
   * 
   */
  public String generateOutput() throws AppError {

    log.debug("Enter generateOutput >> ");
    StringWriter stringWriter = new StringWriter();

    try {

      String templateTxt = readTemplateFile(this.getTemplateName(), this.getTemplateSubDir());

      Velocity.init();
      VelocityContext context = new VelocityContext(templateMap);
      context.put("esc", new EscapeTool());
      context.put("number", new NumberTool());
      context.put("date", new ComparisonDateTool());
      context.put("math", new MathTool());// This tool is for floating point arithmetic
      context.put("msg", this.resourceBundle);// to get the value from the property file using KEY .
      Velocity.evaluate(context, stringWriter, "log", templateTxt);
    } catch (Exception metInvExp) {

      log.error("MethodInvocationException while evaulating velocity template", metInvExp);
      throw new AppError(metInvExp);
    }

    String output = stringWriter.getBuffer().toString();

    return output;
  }

  private String readTemplateFile(String templateFile, String subDirectory) throws Exception {

    StringBuffer buf = null;
    String strBody = null;
    String templateDir = GmCommonClass.getString("TEMPLATEDIR");
    String newLineChar = this.getNewLineChar();

    try {
      templateDir +=
          (subDirectory != null && subDirectory.length() > 0 ? (this.getFileSeparator() + subDirectory)
              : "")
              + this.getFileSeparator() + templateFile;

      log.debug(" # # # templateDir " + templateDir);
      InputStream inputStream = getClass().getResourceAsStream(templateDir);
      BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(inputStream));
      buf = new StringBuffer();

      while ((strBody = bufferedreader.readLine()) != null) {
        buf.append(strBody.trim());
        if (newLineChar != null && newLineChar.length() > 0)
          buf.append(newLineChar);
      }

      bufferedreader.close();
    } catch (Exception e) {
      log.error("Error occurred while reading file " + templateFile + e);
      throw e;
    }
    log.debug("Exit getTemplateString << ");
    return buf.toString();
  }
}
