package com.globus.common.util;

import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.prodmgmnt.beans.GmShareEngineTransBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmSalesDashReqVO;

/**
 * GmProdCatCloudBean - This class used to call write all the cloud related
 * method
 * 
 * @author mmuthusamy
 *
 */
public class GmProdCatCloudBean extends GmBean {

	public GmProdCatCloudBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * syncFilestoCloud - this method used to get the file details from spineIT and
	 * Sync into cloud
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public void syncUploadedFilestoCloud(HashMap hmParam) throws AppError, Exception {
		GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean(getGmDataStoreVO());
		GmCloudManager gmCloudManager = new GmCloudManager();
		
		String strFileIds = GmCommonClass.parseNull((String) hmParam.get("FILEIDS"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strAction = GmCommonClass.parseNull((String) hmParam.get("ACTION"));
		int intCloudType = (int) hmParam.get("CLOUD_TYPE");
		String strMethodName = GmCommonClass.parseNull((String) hmParam.get("CLOUD_METHOD_NAME"));

		String strFileJSONData = "";

		strFileJSONData = gmCommonUploadBean.fetchJSONUploadFilesInfo(strFileIds);
		log.debug(strFileIds + " File JSON Data are ==> " + strFileJSONData);
		// to get the Azure web service URL
		String strCloudURL = GmCommonClass.parseNull(System.getProperty("CLOUD_AZURE_WEB_SERVICE_URL"));
		// to call Azure web service and store file information
		gmCloudManager.initiateAzureCloudServiceCall(strCloudURL, intCloudType, strMethodName, strFileJSONData, strFileIds, strAction, strUserId);

		// to update the cloud Sync flag
		updateCloudSyncFlag (strFileIds, "Y", strUserId);
	}
	
	/**
	 * shareCollateralviaCloud - This method used to share the marketing collateral details.
	 * 
	 * @param hmParam
	 * @throws AppError
	 * @throws Exception
	 */
	public void shareCollateralviaCloud (HashMap hmParam) throws AppError, Exception {
		log.debug("REQUEST_JSON_DATA == > "+ hmParam);
		GmCloudManager gmCloudManager = new GmCloudManager();
		
		String strRequestJSON = GmCommonClass.parseNull((String) hmParam.get("REQUEST_JSON_DATA"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strAction = GmCommonClass.parseNull((String) hmParam.get("ACTION"));
		int intCloudType = (int) hmParam.get("CLOUD_TYPE");
		String strMethodName = GmCommonClass.parseNull((String) hmParam.get("CLOUD_METHOD_NAME"));
		String strShareID = GmCommonClass.parseNull((String)hmParam.get("strShareID"));

		// to get the Azure web service URL
		String strCloudURL = GmCommonClass.parseNull(System.getProperty("CLOUD_AZURE_WEB_SERVICE_URL"));
				
		// to call Azure web service and share marketing collateral details
		gmCloudManager.initiateAzureCloudServiceCall(strCloudURL, intCloudType, strMethodName, strRequestJSON, strShareID, strAction, strUserId);
		
	}

	/**
	 * updateCloudSyncFlag - This method used to update cloud Sync flag details
	 * based on file list ids
	 * 
	 * @param strFileListIds
	 * @param strNewSyncFl
	 * @param strUserId
	 * @return
	 * @throws AppError
	 */
	public void updateCloudSyncFlag (String strFileIds, String strSyncFlag, String strUserId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_upload_info.gm_upd_cloud_synced_fl", 3);
		gmDBManager.setString(1, strFileIds);
		gmDBManager.setString(2, strSyncFlag);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	
	}

}
