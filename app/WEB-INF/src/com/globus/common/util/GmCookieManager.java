package com.globus.common.util;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Cookie;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * This class provides a common utility to manage cookies for the servlet.
 *
 * @author Rick Schmid 4/25/2012
 */
public class GmCookieManager {
	
	private static final int MAX_AGE = (365 * 24 * 60 * 60);// one year
	public static final int MAX_AGE_8 = (8 * 60 * 60);// 8 hr

	/**
	 * No argument constructor.
	 */
	public void GmCookieManager() {}
	
	/**
	 *  Gettter for the specified cookie.
	 *  
	 *  Parameters: HttpServletRequest, String (cookie name).
	 *  
	 *  Returns: String - The value of the cookie specified.
	 */
	public String getCookieValue(HttpServletRequest req, String strCookieName) {
		// Return Cookie Value	
		String stCookieValue; 
		Cookie[] cookies = req.getCookies();    
		if (cookies != null) {       
			for ( int i =0 ; i < cookies.length; i++) {    
				Cookie cookie = cookies[i];
				
				if (strCookieName.equals(cookie.getName())) {
					stCookieValue =  cookie.getValue();
					if (stCookieValue.equals("null"))
					{
						stCookieValue = null;
					}
					return stCookieValue;             
					}      
				}   
			}    
		return null;
	}
	
	/**
	 *  Settter for the specified cookie.
	 *  
	 *  Parameters: HttpServletResponse, String (cookie name), String (cookie value)
	 */
	public void setCookieValue(HttpServletResponse res,
			String strCookieName, String strCookieValue) {
		// Set Cookie value
		Cookie saveCookie = new Cookie(strCookieName, strCookieValue);    
		saveCookie.setMaxAge(MAX_AGE);
		res.addCookie(saveCookie);
	}
	
	/**
	 *  Settter for the specified cookie.
	 *  
	 *  Parameters: HttpServletResponse, String (cookie name), String (cookie value), int (max age)
	 */
	public void setCookieValue(HttpServletResponse res,
			String strCookieName, String strCookieValue, int iMaxAge) {
		// Set Cookie value
		Cookie saveCookie = new Cookie(strCookieName, strCookieValue);
		saveCookie.setMaxAge(MAX_AGE);
		if(iMaxAge > 0)
		{
			saveCookie.setMaxAge(MAX_AGE_8);		
		}
		res.addCookie(saveCookie);
	}
	
	/**
	 *  Deletes the specified cookie by setting the max age to zero.
	 *  
	 *  Parameters: HttpServletResponse, String (cookie name)
	 */
	public void deleteCookie(HttpServletResponse res,
			String strCookieName) {
		// Delete Cookie
		Cookie deleteCookie = new Cookie(strCookieName,"");    
		deleteCookie.setMaxAge(0);
		res.addCookie(deleteCookie);
	}
	
	public void deleteCookie(HttpServletRequest req,HttpServletResponse res) {
		// Delete Cookie
		Cookie[] cookies = req.getCookies();    
		if (cookies != null) 
		{       
			for ( int i =0 ; i < cookies.length; i++) 
			{    
				Cookie bicookie = cookies[i];
				bicookie.setMaxAge(0);
				bicookie.setPath("/");
				res.addCookie(bicookie);
			}
		}		
	}
}
