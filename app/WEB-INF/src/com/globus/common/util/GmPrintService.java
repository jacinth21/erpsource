package com.globus.common.util;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.print.attribute.Attribute;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Copies;
import javax.print.event.PrintJobAdapter;
import javax.print.event.PrintJobEvent;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import com.sun.pdfview.PDFRenderer;

/**
 * This class provides a common utility to print file
 * 
 * 
 */
public class GmPrintService {
  Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
  private String printerName;
  private int NumofCopies;
  private Attribute paperSize;
  private String fileName;
  private String fontName;
  private int fontSize;
  private int imgArea_x_offset;
  private int imgArea_y_offset;
  private int imgArea_width_offset;
  private int imgArea_height_offset;
  private int graphics2D_x;
  private int graphics2D_y;
  // to set the paper size and width
  private int paperSizeWidth = 0;
  private int paperSizeHeight = 0;

  /**
   * @return the fontName
   */
  public String getFontName() {
    return fontName;
  }

  /**
   * @param fontName the fontName to set
   */
  public void setFontName(String fontName) {
    this.fontName = fontName;
  }

  /**
   * @return the fontSize
   */
  public int getFontSize() {
    return fontSize;
  }

  /**
   * @param fontSize the fontSize to set
   */
  public void setFontSize(int fontSize) {
    this.fontSize = fontSize;
  }

  /**
   * @return the imgArea_x_offset
   */
  public int getImgArea_x_offset() {
    return imgArea_x_offset;
  }

  /**
   * @param imgAreaXOffset the imgArea_x_offset to set
   */
  public void setImgArea_x_offset(int imgAreaXOffset) {
    imgArea_x_offset = imgAreaXOffset;
  }

  /**
   * @return the imgArea_y_offset
   */
  public int getImgArea_y_offset() {
    return imgArea_y_offset;
  }

  /**
   * @param imgAreaYOffset the imgArea_y_offset to set
   */
  public void setImgArea_y_offset(int imgAreaYOffset) {
    imgArea_y_offset = imgAreaYOffset;
  }

  /**
   * @return the imgArea_width_offset
   */
  public int getImgArea_width_offset() {
    return imgArea_width_offset;
  }

  /**
   * @param imgAreaWidthOffset the imgArea_width_offset to set
   */
  public void setImgArea_width_offset(int imgAreaWidthOffset) {
    imgArea_width_offset = imgAreaWidthOffset;
  }

  /**
   * @return the imgArea_height_offset
   */
  public int getImgArea_height_offset() {
    return imgArea_height_offset;
  }

  /**
   * @param imgAreaHeightOffset the imgArea_height_offset to set
   */
  public void setImgArea_height_offset(int imgAreaHeightOffset) {
    imgArea_height_offset = imgAreaHeightOffset;
  }

  /**
   * @return the graphics2D_x
   */
  public int getGraphics2D_x() {
    return graphics2D_x;
  }

  /**
   * @param graphics2dX the graphics2D_x to set
   */
  public void setGraphics2D_x(int graphics2dX) {
    graphics2D_x = graphics2dX;
  }

  /**
   * @return the graphics2D_y
   */
  public int getGraphics2D_y() {
    return graphics2D_y;
  }

  /**
   * @param graphics2dY the graphics2D_y to set
   */
  public void setGraphics2D_y(int graphics2dY) {
    graphics2D_y = graphics2dY;
  }

  /**
   * @return the printerName
   */
  public String getPrinterName() {
    return printerName;
  }

  /**
   * @param printerName the printerName to set
   */
  public void setPrinterName(String printerName) {
    this.printerName = printerName;
  }

  /**
   * @return the numofCopies
   */
  public int getNumofCopies() {
    return NumofCopies;
  }

  /**
   * @param numofCopies the numofCopies to set
   */
  public void setNumofCopies(int numofCopies) {
    NumofCopies = numofCopies;
  }

  /**
   * @return the paperSize
   */
  public Attribute getPaperSize() {
    return paperSize;
  }

  /**
   * @param paperSize the paperSize to set
   */
  public void setPaperSize(Attribute paperSize) {
    this.paperSize = paperSize;
  }

  /**
   * @return the fileName
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * @param fileName the fileName to set
   */
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }



  /**
   * @return the paperSizeWidth
   */
  public int getPaperSizeWidth() {
    return paperSizeWidth;
  }

  /**
   * @param paperSizeWidth the paperSizeWidth to set
   */
  public void setPaperSizeWidth(int paperSizeWidth) {
    this.paperSizeWidth = paperSizeWidth;
  }

  /**
   * @return the paperSizeHeight
   */
  public int getPaperSizeHeight() {
    return paperSizeHeight;
  }

  /**
   * @param paperSizeHeight the paperSizeHeight to set
   */
  public void setPaperSizeHeight(int paperSizeHeight) {
    this.paperSizeHeight = paperSizeHeight;
  }

  /**
   * A method to print a file.
   * 
   * @param fileName The path of the PDF file to print.
   * @throws java.io.IOException
   */

  public synchronized void print() throws Exception {
	log.debug("Starting of Regular printing print()...");
    log.debug("Regular printing fileName === " + fileName);
    FileInputStream fileInputStream = new FileInputStream(fileName);
    try {
      byte[] pdfContent = new byte[fileInputStream.available()];
      log.debug("Regular printing after pdfContent...");
      fileInputStream.read(pdfContent, 0, fileInputStream.available());
      log.debug("Regular printing after fileInputStream...");
      ByteBuffer buffer = ByteBuffer.wrap(pdfContent);
      log.debug("Regular printing after buffer...");
      final PDFFile pdfFile = new PDFFile(buffer);
      log.debug("Regular printing after pdfFile...");
      Printable printable = new Printable() {

        @Override
        public int print(Graphics graphics, PageFormat pageFormat,

        int pageIndex) throws PrinterException {
        	 log.debug("Regular printing after print Override method...");
          int pagenum = pageIndex + 1;
          log.debug("Regular printing after pagenum..."+pagenum);
          if ((pagenum >= 1) && (pagenum <= pdfFile.getNumPages())) {

            Graphics2D graphics2D = (Graphics2D) graphics;
            log.debug("Regular printing after graphics2D...");
            PDFPage page = pdfFile.getPage(pagenum);
            log.debug("Regular printing after page...");
            /**
             * Constructs a new <code>Rectangle</code> whose upper-left corner is specified as
             * {@code (x,y)} and whose width and height are specified by the arguments of the same
             * name.
             * 
             * @param x the specified X coordinate
             * @param y the specified Y coordinate
             * @param width the width of the <code>Rectangle</code>
             * @param height the height of the <code>Rectangle</code>
             */
            Rectangle imageArea =
                new Rectangle((int) pageFormat.getImageableX() + imgArea_x_offset,
                    (int) pageFormat.getImageableY() + imgArea_y_offset,
                    (int) pageFormat.getImageableWidth() + imgArea_width_offset,
                    (int) pageFormat.getImageableHeight() + imgArea_height_offset);
            log.debug("Regular printing after imageArea...");

            graphics2D.translate(graphics2D_x, graphics2D_y);
            Font font = new Font(fontName, Font.PLAIN, fontSize);
            graphics2D.setFont(font);
            log.debug("Regular printing after setFont...");
            PDFRenderer pdfRenderer = new PDFRenderer(page, graphics2D, imageArea, null, null);
            try {
            	 log.debug("Regular printing after pdfRenderer TRY...");
              page.waitForFinish();
              log.debug("Regular printing after waitForFinish...");
              pdfRenderer.run();
              log.debug("Regular printing after pdfRenderer...");

            } catch (InterruptedException exception) {
            	 log.debug("Regular printing after InterruptedException...");
              exception.printStackTrace();
            }

            return PAGE_EXISTS;

          } else {

            return NO_SUCH_PAGE;

          }

        }

      };

      PrinterJob printJob = PrinterJob.getPrinterJob();
      log.debug("Regular printing after printJob...");

      PageFormat pageFormat = PrinterJob.getPrinterJob().defaultPage();

      Book book = new Book();

      book.append(printable, pageFormat, pdfFile.getNumPages());

      printJob.setPageable(book);

      // PMT-19992 : OUS print bottom space issue fix.
      // to get the default paper size
      Paper paper = new Paper();
      double d_paperWidth = paper.getWidth();
      double d_paperHeight = paper.getHeight();
      // to get the dynamic values
      if (getPaperSizeHeight() != 0) {
        d_paperWidth = getPaperSizeWidth();
        d_paperHeight = getPaperSizeHeight();
        // to set the size
        paper.setSize(d_paperWidth, d_paperHeight);
        log.debug(" Dynamic Height set " + d_paperHeight + " Width set " + d_paperWidth);
      }
      paper.setImageableArea(0, 0, d_paperWidth, d_paperHeight);
      log.debug(" D Paper Width " + d_paperWidth + " D Paper Height " + d_paperHeight);
      pageFormat.setPaper(paper);

      // To get the all the print services and set the service
      PrintService[] printServices = PrinterJob.lookupPrintServices();

      for (int count = 0; count < printServices.length; ++count) {
        log.debug("printerName === " + printerName);
        if (printerName.equalsIgnoreCase(printServices[count].getName())) {

          printJob.setPrintService(printServices[count]);
          break;

        }

      }

      PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
      log.debug("NumofCopies === " + NumofCopies);
      aset.add(new Copies(NumofCopies));
      log.debug("Regular printing after NumofCopies...");

      printJob.print(aset);
      log.debug("Regular printing after print...");
      fileInputStream.close();
      log.debug("Regular printing after fileInputStream CLOSE...");
    } catch (Exception ex) {
      ex.printStackTrace();
      log.error("Exception while export to pdf " + ex);
      throw ex;
    } finally {
      // releases system resources associated with this stream
      if (fileInputStream != null) {
        fileInputStream.close();
      }
    }
  }

  public static byte[] fileread(String filename) throws Exception {

    File file = new File(filename);

    byte[] b = new byte[(int) file.length()];

    FileInputStream fileInputStream = new FileInputStream(file);
    fileInputStream.read(b);

    return b;
  }

  /**
   * This method is used to print the byte stream
   * 
   * @param content
   * @throws Exception
   */
  public void printByteStream(byte[] content) throws Exception {
	  	
	    if(GmCommonClass.shipPrintThruJMS.equalsIgnoreCase("YES"))
	    {
	    	log.debug("strPrintThruJMS printByteStream()= " + GmCommonClass.shipPrintThruJMS);
		  	HashMap hmPrintParam = new HashMap();
			GmQueueProducer qprod = new GmQueueProducer();
		    GmMessageTransferObject tf = new GmMessageTransferObject();
		  	String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("SHIPPING_LBL_PRINTER_JOB"));
		    String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_SHIPPING_LBL_PRINTER_QUEUE"));
		    log.debug("strConsumerClass = " + strConsumerClass + "--" + strQueueName);
		    hmPrintParam.put("BYTECONTENT", content);
		    hmPrintParam.put("PRINTERNAME", printerName);
		    log.debug("Label Printer JMS Set message = " + hmPrintParam);
		    tf.setMessageObject(hmPrintParam);
		    log.debug("Label Printer JMS consumer class = " + strConsumerClass);
		    tf.setConsumerClass(strConsumerClass);
		    log.debug("Label Printer JMS send message = " + strQueueName);
		    qprod.sendMessage(tf, strQueueName);
		    log.debug("End of Label Printer..");
	    }else{
		    PrintService[] printServices = PrinterJob.lookupPrintServices();
		    PrintService service = null;
		    for (int count = 0; count < printServices.length; ++count) {
	
		      if ((printServices[count].getName()).indexOf(printerName) != -1) {
	
		        service = printServices[count];
	
		        break;
	
		      }
	
		    }
		    // PrintServiceLookup.lookupDefaultPrintService();
	
		    PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
		    pras.add(new Copies(1));
	
	
		    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
		    Doc doc = new SimpleDoc(content, flavor, null);
	
		    DocPrintJob job = service.createPrintJob();
		    PrintJobWatcher pjw = new PrintJobWatcher(job);
		    job.print(doc, pras);
		    pjw.waitForDone();
		    // in.close();
	
		    // send FF to eject the page
		    InputStream ff = new ByteArrayInputStream("\f".getBytes());
		    DocFlavor flavor2 = DocFlavor.INPUT_STREAM.AUTOSENSE;
		    Doc docff = new SimpleDoc(ff, flavor2, null);
		    DocPrintJob jobff = service.createPrintJob();
		    pjw = new PrintJobWatcher(jobff);
		    jobff.print(docff, null);
		    pjw.waitForDone();
	   }
  }
  
  /**
   * This method is used to print the byte stream
   * 
   * @param content
   * @throws Exception
   */
  public void printByteData(HashMap hmParam) throws Exception {
	 log.debug("Starting of Label printing printByteData()...");
	String printerNm = (String) hmParam.get("PRINTERNAME");
	 log.debug("Label printing printerNm= "+printerNm);
    PrintService[] printServices = PrinterJob.lookupPrintServices();
    PrintService service = null;
    
    for (int count = 0; count < printServices.length; ++count) {
      if ((printServices[count].getName()).indexOf(printerNm) != -1) {
        service = printServices[count];
        break;
      }
    }
    log.debug("Label printing after print services...");
    PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
    pras.add(new Copies(1));
    log.debug("Label printing after print copies...");
    DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
    Doc doc = new SimpleDoc((byte[])hmParam.get("BYTECONTENT"), flavor, null);
    log.debug("Label printing after SimpleDoc...");
    DocPrintJob job = service.createPrintJob();
    log.debug("Label printing after DocPrintJob...");
    PrintJobWatcher pjw = new PrintJobWatcher(job);
    job.print(doc, pras);
    log.debug("Label printing after print...");
    pjw.waitForDone();
    log.debug("Label printing after waitForDone...");
    // in.close();

    // send FF to eject the page
    InputStream ff = new ByteArrayInputStream("\f".getBytes());
    log.debug("Label printing after ff input stream...");
    DocFlavor flavor2 = DocFlavor.INPUT_STREAM.AUTOSENSE;
    Doc docff = new SimpleDoc(ff, flavor2, null);
    log.debug("Label printing after ff docff...");
    DocPrintJob jobff = service.createPrintJob();
    log.debug("Label printing after ff DocPrintJob...");
    pjw = new PrintJobWatcher(jobff);
    jobff.print(docff, null);
    log.debug("Label printing after ff print...");

    pjw.waitForDone();
    log.debug("Label printing after ff waitForDone...");
  }
}


/**
 * This class is used to synchronize printer job Need to check whether this class is required or not
 *
 */
class PrintJobWatcher {
  boolean done = false;

  PrintJobWatcher(DocPrintJob job) {
    job.addPrintJobListener(new PrintJobAdapter() {
      @Override
      public void printJobCanceled(PrintJobEvent pje) {
        allDone();
      }

      @Override
      public void printJobCompleted(PrintJobEvent pje) {
        allDone();
      }

      @Override
      public void printJobFailed(PrintJobEvent pje) {
        allDone();
      }

      @Override
      public void printJobNoMoreEvents(PrintJobEvent pje) {
        allDone();
      }

      void allDone() {
        synchronized (PrintJobWatcher.this) {
          done = true;
          PrintJobWatcher.this.notify();
        }
      }
    });
  }

  public synchronized void waitForDone() {
    try {
      while (!done) {
        wait();
      }
    } catch (InterruptedException e) {
    }
  }
}
