package com.globus.common.util.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;







import org.apache.log4j.Logger;
import org.apache.poi.hssf.record.ExtendedFormatRecord;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRuleBean;

public class GmExcelManipulation {
	   Logger log = GmLogger.getInstance(this.getClass().getName());
	private String excelFileNameToWrite = "";	
	private int sheetNum =0;
	private int rowNum = 0;
	private int colNum =0;
	private List dataList;
	private int maxColumn = 0;
	
	private HashMap hmColumnType = new HashMap();
	public final static int CELL_TYPE_FORMULA = HSSFCell.CELL_TYPE_FORMULA;
	public final static int CELL_TYPE_BLANK = HSSFCell.CELL_TYPE_BLANK;
	public final static int CELL_TYPE_BOOLEAN = HSSFCell.CELL_TYPE_BOOLEAN; 
	public final static int CELL_TYPE_ERROR = HSSFCell.CELL_TYPE_ERROR;
	public final static int CELL_TYPE_NUMERIC = HSSFCell.CELL_TYPE_NUMERIC;	
	public final static int CELL_TYPE_STRING =  HSSFCell.CELL_TYPE_STRING;
	public final static int CELL_TYPE_DATE = 6;
	private boolean isLeftBorderRequired = false;
	private boolean isRightBorderRequired = false;
	private boolean isTopBorderRequired = false;
	private boolean isBottomBorderRequired = false;
	public final static short BORDER_THIN = HSSFCellStyle.BORDER_THIN;
	public final static short BORDER_THICK = HSSFCellStyle.BORDER_THICK;
	public final static short BORDER_DASHED = HSSFCellStyle.BORDER_DASHED;
	private HashMap hmAddFormatToColumn = new HashMap();
	private short borderStyle = BORDER_THIN;
	private HashMap hmStyles = new HashMap();
	private HSSFCellStyle defaultCellStyle = null;
	public final static String CURRENCY_FORMAT ="CURRENCY";
	private HSSFFont font = null;
	private boolean isBold = false;
	private short fontStyleBold = HSSFFont.BOLDWEIGHT_BOLD;
	public void setBorderStyle(short value)
	{
		borderStyle = value;
	}
	
	public void setLeftBorder(boolean value)
	{
		isLeftBorderRequired = value;
	}
	
	public void setRightBorder(boolean value)
	{
		isRightBorderRequired = value;
	}
	
	public void setTopBorder(boolean value)
	{
		isTopBorderRequired = value;
	}
	
	public void setBottomBorder(boolean value)
	{
		isBottomBorderRequired = value;
	}
	
	public void setFontBold(boolean value) {
		isBold = value;
	}
	
	public void setColumnType(String strColumnName, int iColumnType)
	{
		hmColumnType.put(strColumnName, ""+iColumnType);
	}
	
	 private int getColumnType(String strColumnName)
	 {
		  String strType = GmCommonClass.parseNull((String)hmColumnType.get(strColumnName));
		  strType = strType.equals("") ? "1" : strType; 
		  int type = Integer.parseInt(strType);
		  type = type < 0 || type > 6 ? 1 : type;
		  return type;		
		 }
	 
	 
	
	public static void delete(String fileName) throws Exception {

	    try {
	      // Construct a File object for the file to be deleted.
	      File target = new File(fileName);
	      if (!target.exists())
	      {
	        return;
	      }
	      // Quick, now, delete it immediately:
	      target.delete();	      
	    } catch (SecurityException e) {
	      e.printStackTrace();
	     throw e;	
	    }
	  }
		
	 public static void copyFile(String strIn, String strOut) throws Exception {
		 	File fOut = new File(strOut);		
		    FileInputStream fis  = new FileInputStream(new File(strIn));
		    FileOutputStream fos = new FileOutputStream(fOut);
		    try {
		        byte[] buf = new byte[1024];
		        int i = 0;
		        while ((i = fis.read(buf)) != -1) {
		            fos.write(buf, 0, i);
		        }
		    } 
		    catch (Exception e) {
		        throw e;
		    }
		    finally {
		        if (fis != null) fis.close();
		        if (fos != null) fos.close();
		    }
		  }

	 public void setExcelFileName(String excelFileNameToWrite)
	{
		this.excelFileNameToWrite = excelFileNameToWrite;
	}
	
	public void setSheetNumber(int sheetNum)
	{
		this.sheetNum = sheetNum;
	}
	
	public void setRowNumber(int rowNum)
	{
		this.rowNum = rowNum;
	}
	
	public void setColumnNumber(int colNum)
	{
		this.colNum = colNum;
	}
	
	public void writeToExcel(String strValue) throws Exception
	{		 
		try
		{
		HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(excelFileNameToWrite));
	    HSSFSheet sheet = wb.getSheetAt(sheetNum);
	    HSSFRow row = sheet.getRow(rowNum);
	    defaultCellStyle = wb.createCellStyle();
	    font = wb.createFont();
	    
	    
	    if(isLeftBorderRequired)
	    {    	
	    	defaultCellStyle.setBorderLeft(borderStyle);
	    }
	    if(isRightBorderRequired)
	    {    	
	    	defaultCellStyle.setBorderRight(borderStyle);
	    }
	    if(isTopBorderRequired)
	    {    	
	    	defaultCellStyle.setBorderTop(borderStyle);
	    }
	    if(isBottomBorderRequired)
	    {    	
	    	defaultCellStyle.setBorderBottom(borderStyle);
	    }
	    if(isBold) {
	    	font.setBoldweight(fontStyleBold);
	    }
	    
	     row = sheet.createRow(rowNum);
	     HSSFCell cell = row.getCell((short)colNum);    	
	     cell = row.createCell((short)colNum);	    	    
    	 cell.setCellType(HSSFCell.CELL_TYPE_STRING);
    	 defaultCellStyle.setFont(font);
    	 cell.setCellStyle(defaultCellStyle);
		 cell.setCellValue(strValue);
		
		
		 
		// Write the output to a file
		 FileOutputStream fileOut = new FileOutputStream(excelFileNameToWrite);		 
		    wb.write(fileOut);
		    fileOut.close();
		 }
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw ex;
		}		
	}
	
	public void writeToExcel(List alList) throws Exception
	{		
	
    this.dataList = alList;
    
	POIFSFileSystem fs      =
        new POIFSFileSystem(new FileInputStream(excelFileNameToWrite));
    HSSFWorkbook wb = new HSSFWorkbook(fs);
    HSSFSheet sheet = wb.getSheetAt(sheetNum); 
    
    defaultCellStyle = wb.createCellStyle();
    
    if(isLeftBorderRequired)
    {    	
    	defaultCellStyle.setBorderLeft(borderStyle);
    }
    if(isRightBorderRequired)
    {    	
    	defaultCellStyle.setBorderRight(borderStyle);
    }
    if(isTopBorderRequired)
    {    	
    	defaultCellStyle.setBorderTop(borderStyle);
    }
    if(isBottomBorderRequired)
    {    	
    	defaultCellStyle.setBorderBottom(borderStyle);
    }
    setExtraCellStyle(wb);
    
    populateData(sheet);

    // Write the output to a file
    FileOutputStream fileOut = new FileOutputStream(excelFileNameToWrite);
    wb.write(fileOut);
    fileOut.close();
		
	}
	
	 public void setCursorToNextEditableRow() throws Exception
	 {
		 boolean isNonEditableCell = false;
		 POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(excelFileNameToWrite));
		 HSSFWorkbook wb = new HSSFWorkbook(fs);
		 HSSFSheet sheet = wb.getSheetAt(sheetNum);
		 int localRowNum = rowNum;

		 while(!isNonEditableCell)
		   {
			   HSSFRow row = sheet.getRow(localRowNum);
			   if(row == null)
			   {
				   row = sheet.createRow(localRowNum);
			   } 
			   HSSFCell cell = row.getCell((short)colNum);			  
			   if(cell == null)
			   {
				   isNonEditableCell = true;
				   rowNum = localRowNum;
			   }
			   localRowNum ++;
		   }
	 }
	
	 public void setMaxColumnNumberToRead(int maxColumn)
	 {
		 this.maxColumn = maxColumn;
	 }
	 
	 public List readFromExcel() throws Exception
	 {
		 List alData = new ArrayList();
		 POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(excelFileNameToWrite));
		 HSSFWorkbook wb = new HSSFWorkbook(fs);
		 HSSFSheet sheet = wb.getSheetAt(sheetNum);
		 int localRowNum = rowNum;
		 int localColumnNum;
		 HashMap hmMap = new HashMap();
		 
		 boolean rowNotExists = false;
		 while(!rowNotExists)
		 {
			localColumnNum =  colNum;
			hmMap = new LinkedHashMap();
			
			HSSFRow row = sheet.getRow(localRowNum);
		   if(row == null)
		   {
			   row = sheet.createRow(localRowNum);
		   }
		   	HSSFCell tempCell = row.getCell((short)colNum);
		    if(tempCell != null)
			   {
		    	 while(localColumnNum <= maxColumn)
				   {		    		 
					   HSSFCell cell = row.getCell((short)localColumnNum);				   
					   if(cell == null)
					   {
						   cell = row.createCell((short)localColumnNum);
					   }
					   hmMap.put("COLUMN" +localColumnNum, cell.getStringCellValue());
					   localColumnNum ++;					  
				   }				   
			   }		
		    else
		    {
		    	rowNotExists = true;
		    }
		       alData.add(hmMap);
			   localRowNum++;			   
		 } 
		 return alData;
	 }
	 
	 private void populateData(HSSFSheet sheet) throws ParseException
	 {
		 int localRownum = rowNum;
		 
		 int dataIndex = 0;
		 int maxRow = dataList.size();
		 HashMap hmRow = new HashMap();
		 Iterator itrRow;
		 String strCellKey = "";
		 String strCellValue = "";
		 
		 while (localRownum < (maxRow +rowNum))
		 {
			 int localColNum = colNum;
			 HSSFRow row = sheet.getRow(localRownum);
			    if(row == null)
			    {
			    	row = sheet.createRow(localRownum);
			    }	
			
			    hmRow = (HashMap)dataList.get(dataIndex);
			    itrRow = hmRow.keySet().iterator();
			    while(itrRow.hasNext())
			    {
			    	strCellKey = (String) itrRow.next();
			    	strCellValue = (String)hmRow.get(strCellKey);			    				    			    		
			    	HSSFCell cell = row.getCell((short)localColNum);
			    	if(cell == null)
				    {
				    	cell = row.createCell((short)localColNum);
				    }
			    	int type =getColumnType(strCellKey);	
			    	
			    	cell.setCellStyle(getCellStyle(strCellKey))	;		    	
			    	cell.setCellType(type);
			    	setColumnValue(cell, type, GmCommonClass.parseNull(strCellValue));
					 localColNum ++;
			    }
			    localRownum ++;
			    dataIndex++;
		    }			    
	}	
  public void setExtraFormatToColumn(String strColumnName, String strFormat)
  {
	  hmAddFormatToColumn.put(strColumnName, strFormat);
  }
  
  private void setExtraCellStyle(HSSFWorkbook wb)
  {
	  Iterator itr =  hmAddFormatToColumn.keySet().iterator();
	  
	  while(itr.hasNext())
	  {
		  HSSFCellStyle cellStyle =  wb.createCellStyle();
		  
		  String strKey = (String)itr.next();
		  String strValue = (String)hmAddFormatToColumn.get(strKey);
	    
	    if(isLeftBorderRequired)
	    {    	
	    	cellStyle.setBorderLeft(borderStyle);
	    }
	    if(isRightBorderRequired)
	    {    	
	    	cellStyle.setBorderRight(borderStyle);
	    }
	    if(isTopBorderRequired)
	    {    	
	    	cellStyle.setBorderTop(borderStyle);
	    }
	    if(isBottomBorderRequired)
	    {    	
	    	cellStyle.setBorderBottom(borderStyle);
	    }
	    
	    if(strValue.equals(CURRENCY_FORMAT))
		  {			  
			  cellStyle.setDataFormat((short)8);			 			
		  }
	    hmStyles.put(strKey,cellStyle);
	  }
  }
  
	private HSSFCellStyle getCellStyle(String strColumnName)
	{
		return hmStyles.get(strColumnName) == null ? defaultCellStyle : (HSSFCellStyle)hmStyles.get(strColumnName);
	}

	private void setColumnValue(HSSFCell cell, int type, String strValue)	throws ParseException 
	 {   
		 switch(type)
		 {
		 case CELL_TYPE_BOOLEAN : 
			  boolean b = strValue.equalsIgnoreCase("true") ? true : false; 
			 cell.setCellValue(b);
		  break;		 
		 case  CELL_TYPE_NUMERIC :
			 String value =  GmCommonClass.parseZero(strValue);
			 double d = Double.parseDouble(value);
			 cell.setCellValue(d);
			 break;		
		 case CELL_TYPE_DATE :
			 Date date =  GmCalenderOperations.getDate(strValue);
			 cell.setCellValue(date);
			 break;
		 default : 			 
			 cell.setCellValue(strValue); 			 
		 break;	 		 		 
		 }
	 }
	
	/*Created new method to read the date column for the Lot expiration extension.
	 * To read the date column in excel, use POI method getDateCellValue()
	 * getDateCellValue() -> Currently in POI jar, this method does not recognize if the excel column has "DATE" Format.
	 * It is considering it as a numeric value. In order to read a date column using POI jar, set the format as MM/DD/YYYY in "Custom" type in Excel Format option
	 * More research needs to be done to explore how this method can be used to read "DATE" format in Excel.
	 * Author: gpalani (PMT-25212)
	 */
	 public List readFromExcel_lot_expiry(String strUserID) throws Exception
	 {
		 List alData = new ArrayList();
		 GmEmailProperties emailProps = new GmEmailProperties();
		 HashMap hmEmailProperty = new HashMap();
		 POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(excelFileNameToWrite));
		 HSSFWorkbook wb = new HSSFWorkbook(fs);
		 HSSFSheet sheet = wb.getSheetAt(sheetNum);
		 GmRuleBean gmRuleBean=new GmRuleBean();
		 int localRowNum = rowNum;
		 int localColumnNum;
		 HashMap hmMap = new HashMap();
		 String strUserMailId = "";
		 strUserMailId = gmRuleBean.fetchEmailId(strUserID);
		 boolean rowNotExists = false;
		 String lot_expiry=null;
		 while(!rowNotExists)
		 {
			localColumnNum =  colNum;
			hmMap = new LinkedHashMap();
			HSSFRow row = sheet.getRow(localRowNum);
		   if(row == null)
		   {
			   row = sheet.createRow(localRowNum);
		   }
		   	HSSFCell tempCell = row.getCell((short)colNum);
		    if(tempCell != null)
			   {
		    	 while(localColumnNum <= maxColumn)
				   {	
		    		try
		    		{
		    		HSSFCell cell = row.getCell((short)localColumnNum);

		    		// First 2 columns are part number and Lot number
						   if(localColumnNum !=2)
						   {
						   hmMap.put("COLUMN" +localColumnNum, cell.getStringCellValue());
						   localColumnNum ++;
						   }
						   
				    /*The 3rd column is  date type so we are using separate method to read the data. Using POI there are
					* challenge in checking the cell type is date in POI so a separate condition is checked to handle this scenario.
					*/
						   if(localColumnNum == 2)
						   {	
			    		
							    cell = row.getCell((short)localColumnNum);	
							    hmMap.put("COLUMN" +localColumnNum, cell.getDateCellValue());
 						        localColumnNum ++;					  
						   }
						   
		    		}
		    		
		    	catch(Exception e)
		    	
		    		{
		    		    log.debug("Exception when sending email from Lot Expiry"+e);

		    		    hmEmailProperty.put("strFrom", GmCommonClass.parseNull(GmCommonClass.getRuleValue("FROMID", "EMAIL_DETAILS")));
		    		    hmEmailProperty.put("strTo", strUserMailId);
		    		    hmEmailProperty.put("strCc", strUserMailId);
		    		    hmEmailProperty.put("strSubject", GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOT_EMAIL_SUB", "LOT_EXPIRY")));
		    		    hmEmailProperty.put("strMessageDesc", GmCommonClass.parseNull(GmCommonClass.getRuleValue("LOT_EMAIL_MSG", "LOT_EXPIRY")));
		    		    hmEmailProperty.put("strMimeType", "text/html");
		    		    GmCommonClass.sendMail(hmEmailProperty);
		    		}
					  
				   }	
			   }		
		    else
		    {
		    	rowNotExists = true;
		    }
		       alData.add(hmMap);
			   localRowNum++;			   
		 } 
		 return alData;
	 }
	 
}
