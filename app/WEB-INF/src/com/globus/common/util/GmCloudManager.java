package com.globus.common.util;

import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

/**
 * GmCloudManager This class used to configure and send cloud REST api request
 * 
 * @author mmuthusamy
 *
 */
public class GmCloudManager {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	public static final String TEMPLATE_NAME = "GmCloudNotification";

	/**
	 * initiateCloudServiceCall - This method used to call the cloud web service
	 * based on function name and push JSON data
	 * 
	 * @param strCloudURL
	 * @param intCloudType
	 * @param strFunctionName
	 * @param strJSONData
	 * @param strRefID
	 * @param strAction
	 * @param strUserId
	 * @throws Exception
	 */

	public void initiateAzureCloudServiceCall(String strCloudURL, int intCloudType, String strFunctionName,
			String strJSONData, String strRefId, String strAction, String strUserId) throws AppError {

		try {

			// 1. to Check the cloud URL
			if (strCloudURL.equals("")) {
				log.error(" Cloude URL empty == strFunctionName " + strFunctionName + " strRefId " + strRefId
						+ " strAction " + strAction);
				// send email
				throw new Exception();
			}

			// 2. to get the Http Connection
			HttpURLConnection connection = getHttpAzureCloudConnection(strFunctionName, strCloudURL);

			// 3. to push the JSON data to cloud
			OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
			wr.write(strJSONData);
			wr.flush();
			wr.close();

			// 4. get the response
			int intResponseCode = connection.getResponseCode();
			log.debug(" after call azureServieCall with Auth ==> " + intResponseCode);

			// 5. To store cloud call details

			saveCloudWebServiceDtls(strCloudURL, strFunctionName, strJSONData, strRefId, strAction, intCloudType,
					intResponseCode, connection.getResponseMessage(), strUserId);

			// 6. any error notify to IT user
			if (intResponseCode != 200) {
				// to print logger
				log.error(connection.getResponseMessage());
				// notify to IT
				throw new Exception();
			}

		} catch (Exception ex) {
			sendEmail(strCloudURL, strFunctionName, strJSONData, strRefId);
		}

	}

	/**
	 * getHttpCloudConnection - This method used to create the HTTP URL cloud
	 * connection
	 * 
	 * @param strFunctionName
	 * @return
	 * @throws Exception
	 */
	private HttpURLConnection getHttpAzureCloudConnection(String strFunctionName, String strCloudURL) throws Exception {
		String strFunctionAuthKey = GmCommonClass.parseNull(System.getProperty("CLOUD_AZURE_FUNCTION_AUTH_KEY"));

		HttpURLConnection connection = null;

		// to call the Web service
		URL url;

		ObjectOutputStream out;
		// single object
		log.debug("strURL ==> " + strCloudURL + strFunctionName);

		// Creating the URL.
		url = new URL(strCloudURL + strFunctionName);
		connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "text/html");
		// Azure parameter

		//
		connection.setRequestProperty("x-functions-key", strFunctionAuthKey);

		// additional parameter

		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		//
		return connection;
	}

	/**
	 *
	 * saveCloudWebServiceDtls - This method used to store the cloud web service
	 * call details
	 *
	 * @param strCloudURL
	 * @param strMethodName
	 * @param strJSONRequest
	 * @param intCloudType
	 * @param intResponseCode
	 * @param strResponseMsg
	 * @param strUserId
	 * @throws AppError
	 */
	public void saveCloudWebServiceDtls(String strCloudURL, String strMethodName, String strJSONRequest,
			String strRefId, String strAction, int intCloudType, int intResponseCode, String strResponseMsg,
			String strUserId) throws AppError {

		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cm_cloud_ws_log.gm_sav_cloud_web_service_dtl", 9);
		gmDBManager.setString(1, strCloudURL);
		gmDBManager.setString(2, strMethodName);
		gmDBManager.setString(3, strJSONRequest);
		gmDBManager.setString(4, strRefId);
		gmDBManager.setString(5, strAction);
		gmDBManager.setInt(6, intCloudType);
		gmDBManager.setInt(7, intResponseCode);
		gmDBManager.setString(8, strResponseMsg);
		gmDBManager.setString(9, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();

	}

	/**
	 * sendEmail - This method used to send notification email to IT
	 * 
	 * @param strCloudURL
	 * @param strFunctionName
	 * @param strJSONData
	 * @param strRefId
	 * @throws AppError
	 */

	public void sendEmail(String strCloudURL, String strFunctionName, String strJSONData, String strRefId)
			throws AppError {
		GmEmailProperties gmEmailProperties = getEmailProperties(TEMPLATE_NAME);
		String strMessage = gmEmailProperties.getMessage();
		strMessage = GmCommonClass.replaceAll(strMessage, "#<CLOUD_URL>", strCloudURL);
		strMessage = GmCommonClass.replaceAll(strMessage, "#<REF_ID>", strRefId);
		strMessage = GmCommonClass.replaceAll(strMessage, "#<JSON_DATA>", strJSONData);
		gmEmailProperties.setSender(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.FROM));
		gmEmailProperties.setMessage(strMessage);
		gmEmailProperties
				.setMimeType(GmCommonClass.getEmailProperty(TEMPLATE_NAME + "." + GmEmailProperties.MIME_TYPE));

		try {
			GmCommonClass.sendMail(gmEmailProperties);
		} catch (Exception ex) {

		}

	}

	/**
	 * getEmailProperties - this method used to get the template data from
	 * properties
	 * 
	 * @param strTemplate
	 * @return
	 */

	public GmEmailProperties getEmailProperties(String strTemplate) {
		GmEmailProperties emailProps = new GmEmailProperties();
		emailProps.setSender(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.FROM));
		emailProps.setSubject(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.SUBJECT));
		emailProps.setMessage(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MESSAGE));
		emailProps.setMimeType(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.MIME_TYPE));
		emailProps.setRecipients(GmCommonClass.getEmailProperty(strTemplate + "." + GmEmailProperties.TO));
		return emailProps;
	}
}
