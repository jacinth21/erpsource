package com.globus.common.util.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.beans.GmJobSetupBean;
import com.globus.common.util.forms.GmJobSetupForm;
import com.globus.operations.forecast.beans.GmDemandReportBean;
import com.globus.common.beans.GmLogger;
import org.apache.log4j.Logger;




public class GmJobSetupAction extends Action
{
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
    	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
        try {
        GmJobSetupForm gmJobSetupForm = (GmJobSetupForm)form;
        gmJobSetupForm.loadSessionParameters(request);
        GmJobSetupBean gmJobSetupBean = new GmJobSetupBean();
        GmCommonBean gmCommonBean = new GmCommonBean();
        GmDemandReportBean gmDemandReportBean = new GmDemandReportBean();
         

        String strOpt = gmJobSetupForm.getStrOpt();
        String strJobId = gmJobSetupForm.getJobId();
        String strRefId = gmJobSetupForm.getRefId();
        String strRefType = gmJobSetupForm.getRefType();
        
        HashMap hmTemp = new HashMap();
        HashMap hmParam = new HashMap ();
        HashMap hmValues = new HashMap();
        ArrayList alJobList = new ArrayList();
        ArrayList alJobSchedule = new ArrayList();
        ArrayList alLogReasons = new ArrayList();
        log.debug(" strOpt is " + strOpt);
        
        hmParam = GmCommonClass.getHashMapFromForm(gmJobSetupForm);
        
        if (strOpt.equals("save"))
        {
            strJobId = gmJobSetupBean.saveJob (hmParam);
        }

        if (strOpt.equals("rerun"))
        {
            hmParam = GmCommonClass.getHashMapFromForm(gmJobSetupForm);
            gmJobSetupBean.saveReRun(hmParam);
        }

        if (strOpt.equals("edit") || !strJobId.equals(""))
        {
            hmValues = gmJobSetupBean.fetchJobInfo(strRefId, strRefType);
            log.debug(" values fetched for edit " +hmValues);
            gmJobSetupForm = (GmJobSetupForm)GmCommonClass.getFormFromHashMap(gmJobSetupForm,hmValues);
            strJobId = GmCommonClass.parseNull((String)hmValues.get("JOBID"));
        }
        
        if (strOpt.equals("executeload"))
        {
        	gmDemandReportBean.executeJob(strRefId, hmParam);
        }
        
        log.debug(" values in gmJobSetupForm form --> " + hmParam);        
        alJobList = gmJobSetupBean.loadJobList(hmTemp);
        alJobSchedule = GmCommonClass.getCodeList("JOBSH");
        
        gmJobSetupForm.setAlJobList(alJobList);
        gmJobSetupForm.setAlJobSchedule(alJobSchedule);
        
        //      displaying comments
        alLogReasons = gmCommonBean.getLog(strJobId,"1223");
        gmJobSetupForm.setAlLogReasons(alLogReasons);
        }
        catch( Exception exp ){
             exp.printStackTrace();
             throw new AppError(exp);
        }
        return mapping.findForward("success");
    }
        

}
