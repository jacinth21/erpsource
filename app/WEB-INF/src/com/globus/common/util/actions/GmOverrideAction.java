package com.globus.common.util.actions;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.beans.GmOverrideBean;
import com.globus.common.util.forms.GmOverrideForm;

public class GmOverrideAction extends GmAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			Logger log = GmLogger.getInstance(this.getClass().getName());

			GmOverrideForm gmOverrideForm = (GmOverrideForm) form;
			gmOverrideForm.loadSessionParameters(request);

			GmOverrideBean gmOverrideBean = new GmOverrideBean();
			GmCommonBean gmCommonBean = new GmCommonBean();
			HashMap hmParam = new HashMap();
			HashMap hmReturn = new HashMap();
			HashMap hmRequestDetails = new HashMap();
            ArrayList  alRequestDetaillist = new ArrayList();
			String strOpt = gmOverrideForm.getStrOpt();
			String strOverrideTypeId = gmOverrideForm.getOverrideTypeId();
			String strCode = "";
			String strLogId = "";
			String strTypeChar = "";
			hmParam = GmCommonClass.getHashMapFromForm(gmOverrideForm);

			if (gmOverrideForm.getPartType().indexOf(".") != -1) {
				throw new AppError(new SQLException("Error Occured", null, 20069));
			}
				
			if (strOpt.equals("save")) {
				hmReturn = gmOverrideBean.saveOverrideDetails(hmParam);
				gmOverrideForm.setRefIdForLogReason((String) hmReturn.get("GROWTHDETAILSID"));
				gmOverrideForm.setMsg((String) hmReturn.get("MSG"));
			} else {
				gmOverrideForm.setRefIdForLogReason(gmOverrideBean.fetchRefID(hmParam));
			}
			log.debug(" values inside action asdfasdf" + hmParam);
			gmOverrideForm.setAlTypes(GmCommonClass.getCodeList("POOVR"));
			gmOverrideForm.setAlRefMonths(GmCommonClass.getCodeList("GRFMO", gmOverrideForm.getRefType()));
			gmOverrideForm.setOverrideType(GmCommonClass.getCodeName(strOverrideTypeId));
			strLogId = gmOverrideForm.getRefIdForLogReason();
			log.debug("strLogId: "+strLogId);
			if (strOverrideTypeId.equals("20395")) {
				strCode = "1002";
			} else if (strOverrideTypeId.equals("20396")) {
				strCode = "1001";
			} else if (strOverrideTypeId.equals("20397")) {
				strCode = "1003";
			} else if (strOverrideTypeId.equals("20398") || strOverrideTypeId.equals("20399")) {
				strCode = "1002";	
				HashMap tempRHashMap = new HashMap(hmParam);
	    		tempRHashMap.put("OVERRIDETYPEID", "20395");
	    		strLogId = gmOverrideBean.fetchRefID(tempRHashMap);	 
			}
			
			 if(strOpt.equals("savecomments"))
			 {				   		   
				hmParam.put("STRCODE", strCode);
				hmParam.put("STRLOGID", strLogId);
				gmOverrideBean.saveComments(hmParam);				 
			 }
			 if(!gmOverrideForm.getCurrentValue().equals(""))
			 strTypeChar = Character.isWhitespace(gmOverrideForm.getCurrentValue().charAt(gmOverrideForm.getCurrentValue().length() - 1))
					|| Character.isDigit(gmOverrideForm.getCurrentValue().charAt(gmOverrideForm.getCurrentValue().length() - 1)) ? "" : String
					.valueOf(gmOverrideForm.getCurrentValue().charAt(gmOverrideForm.getCurrentValue().length() - 1));

			log.debug("strTypeChar :" + strTypeChar);
			if (!strTypeChar.equals("")) {
				gmOverrideForm.setRefMonth(GmCommonClass.parseNull(GmCommonClass.getCodeID(strTypeChar, "GRFMO")));
			}
			log.debug("Months inside action " + gmOverrideForm.getAlRefMonths());
			// displaying comments
			log.debug("strLogId: "+strLogId);
			gmOverrideForm.setAlLogReasons(gmCommonBean.getLog(strLogId, "1224"));
			gmOverrideForm.setAlTrail(gmOverrideBean.fetchOverrideHistory(strLogId, strCode));
			hmRequestDetails = gmOverrideBean.fetchGrowthRequestdetail(hmParam);			
			gmOverrideForm.setAlRequest((ArrayList)hmRequestDetails.get("RDREQUEST"));
			log.debug("REQUEST:"+hmRequestDetails.get("RDREQUEST"));
			log.debug("getPdisablevalue ***** :"+gmOverrideForm.getPdisablevalue());
			
			gmOverrideForm.setRequestCount((String)hmRequestDetails.get("REQCOUNT"));

		} catch (Exception e) {
			e.printStackTrace();
			throw new AppError(e);
		}
 
		return mapping.findForward("success");
	}

}
