package com.globus.common.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.sales.GmConsignDetailByPartVO;
import com.globus.valueobject.sales.GmConsignDetailVO;
import com.globus.valueobject.sales.GmSalesReportFilterVO;

public class GmSalesReportUtil {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/**populateJSONOutput - This method is used to populate JSON formatted String from HashMap
	 * @param HashMap
	 * @param String
	 */
	public String populateJSONOutput(HashMap hmParam){
		Iterator itrKeys = hmParam.keySet().iterator();
		String strReturn = "";
		String strQuote ="\"";
		String strEqual="=";
		String strComma = ",";
		String strColon = ":";
		String strOpen = "[";
		String strClose = "]";
		String strPrefix ="{";
		String strSuffix = "}";
		
		while(itrKeys.hasNext())
		{
			String strKey = (String)itrKeys.next();
			String strValue = GmCommonClass.parseNull(String.valueOf(hmParam.get(strKey)));
			strReturn+=strQuote.concat(strKey).concat(strQuote).concat(strColon)
			.concat(strOpen).concat(strValue).concat(strClose).concat(strComma);
		}
		if(strReturn.endsWith(strComma))
			strReturn =strReturn.substring(0,strReturn.length()-1);
			
		return strPrefix.concat(strReturn).concat(strSuffix);
	}
	/**populateStringFromListMap - This method is used to populate String from ArrayList
	 * @param ArrayList
	 * @param String
	 */
	public String populateStringFromListMap(ArrayList alParam){
		Iterator itrHashMap = alParam.iterator();
		String strReturn = "";
		String strHmString = "";
		String strSuffix = ",";
		while(itrHashMap.hasNext())
		{
			strHmString = populateStringFromMap((HashMap)itrHashMap.next());
			strReturn+=strHmString.concat(strSuffix);
		}
		
		if(strReturn.endsWith(strSuffix)){
			strReturn =strReturn.substring(0,strReturn.length()-1);
		}
		return strReturn;
	}
	/**populateStringFromList - This method is used to populate String from ArrayList
	 * @param ArrayList, String
	 * @param String
	 */
	public String populateStringFromList(ArrayList alParam,String strPrepend){
		
		Iterator itrKeys = alParam.iterator();
		String strReturn = "";
		String strSuffix = ",";
		String strQuote = "\"";
		while(itrKeys.hasNext())
		{
			String strKey = (String)itrKeys.next();
			strReturn+=strKey.concat(strSuffix);
		}
		if(strReturn.endsWith(strSuffix))
			strReturn =strReturn.substring(0,strReturn.length()-1);
		
		return strPrepend.concat(strReturn).concat(strQuote);
	}
	/**populateStringFromMap - This method is used to populate String from HashMap
	 * @param HashMap
	 * @param String
	 */
	public String populateStringFromMap(HashMap hmParam){
		
		Iterator itrKeys = hmParam.keySet().iterator();
		String strReturn = "";
		String strQuote ="\"";
		String strColon=":";
		String strComma = ",";
		String strPrefix ="{";
		String strSuffix = "}";
		
		while(itrKeys.hasNext())
		{
			String strKey = (String)itrKeys.next();
			String strValue = GmCommonClass.parseNull(String.valueOf(hmParam.get(strKey)));
			strReturn+=strQuote.concat(strKey).concat(strQuote).concat(strColon).concat(strQuote).concat(strValue).concat(strQuote)
			.concat(strComma);
		}
		if(strReturn.endsWith(strComma))
			strReturn =strReturn.substring(0,strReturn.length()-1);
			
		return strPrefix.concat(strReturn).concat(strSuffix);
	}
	/* This is the method to populate the HashMap Key for Field Inv Report(MIM App).
	 * The returned HashMap contains all the key info's to get the field inv report details.
	 */
	public HashMap populateSalesReportParam(String strAction , GmSalesReportFilterVO gmSalesReportFilterVO, GmConsignDetailVO gmConsignDetailVO,GmConsignDetailByPartVO gmConsignDetailsByPartVO) throws AppError{
		
		HashMap hmParam = new HashMap();
		if(strAction.equals("LoadDCONS") || strAction.equals("LoadGCONS") || strAction.equals("LoadAD") || strAction.equals("LoadVP")){
			hmParam = populateFIFieldSalesDrillParam( gmSalesReportFilterVO);
		}else if(strAction.equals("LoadRep") || strAction.equals("LoadPart") || strAction.equals("LoadGroup") || strAction.equals("LoadDistributor")){
			hmParam = populateFISalesRepDrillParam( gmSalesReportFilterVO);
		}else if(strAction.equals("LoadCons")){
			hmParam = populateFIConsignDetDrillParam( gmConsignDetailVO);
		}else if(strAction.equals("BYSETPART")){
			hmParam = populateFIConsignDetailByPart( gmConsignDetailsByPartVO);
		}
		return hmParam; 
	}
	
	/* This is the method to populate the HashMap Key for Field Inv Report(MIM App).
	 * The returned HashMap contains all the key info's to get the field inv report details.
	 */
	private HashMap populateFIFieldSalesDrillParam(GmSalesReportFilterVO gmSalesReportFilterVO) throws AppError{

		HashMap hmParam = new HashMap();
		
		//Get the below params , from the input VO 
		hmParam.put("Condition",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getCondition()));
		hmParam.put("AccessFilter",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getAccessfilter()));
		
		hmParam.put("SalesType",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getSalestype())); //50300-Sales Consignment, 50301-Loaner
		hmParam.put("ConsignmentType",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getContype()));//50400-Revenue, 50401-Procedure, 50403-Units
		hmParam.put("TURNS",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getTurns()));        //1-30 days, 2-60 days,3-90 days
		hmParam.put("SHOWAI",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getShowai()));      //"Y"
		hmParam.put("FromMonth",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getFrmmonth())); //10
		hmParam.put("FromYear",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getFrmyear()));   //2014
		hmParam.put("ToMonth",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getTomonth()));    //01
		hmParam.put("ToYear",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getToyear()));      //2015
		hmParam.put("hAction",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getAction()));     //LoadGCONS, LoadDCONS
		
		hmParam.put("SYSTEMONLYCONDITION",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getSysonlycon()));//"ONLYSYSTEM"
		hmParam.put("BASELINENAME",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getBaselinenm()));// "Baseline Sets"
		hmParam.put("Type",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getType()));//"1"
		hmParam.put("LOANERTYPEDISABLED",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getLoantypdisable()));//"disabled"
		hmParam.put("PartNumber",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getPnum()));//""
		hmParam.put("ConsignmentPeriod",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getConperiod()));//"50461"
		hmParam.put("hSetNumber",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getHsetnum()));//""
		hmParam.put("DistributorID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getDid()));//""
		hmParam.put("ConsignmentSummaryType",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getConsummtype()));//""
		hmParam.put("Region",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getRegion()));//""
		hmParam.put("RepID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getRepid()));//""
		hmParam.put("Chk_ShowArrowFl",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getShowarrowfl()));//""
		hmParam.put("ConsignmentSetType",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getConsettype()));//"20160"
		hmParam.put("hPartNumber",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getHpnum()));//""
		hmParam.put("ADID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getAdid()));//""
		hmParam.put("AccountID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getAcctid()));//""
		hmParam.put("LOANERTYPE",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getLoanertype()));//""
		hmParam.put("SetNumber",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getSetnum()));//""
		hmParam.put("HEADERPREFIX",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getHeadprefix()));//"Sales Consignment"
		hmParam.put("Status",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getStatus()));//"0"
		hmParam.put("hcboName",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getHcbonm()));//""
		hmParam.put("VPID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getVpid()));//""
		hmParam.put("Chk_HideAmountFl",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getHideamtfl()));//""
		hmParam.put("TURNSLABEL",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getTurnslabel()));//"#Cases<BR> per M/Set"
		hmParam.put("State",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getState()));//""
		hmParam.put("SHOWAIDISABLED",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getShowdisabled()));//""
		hmParam.put("TerritoryID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getTerrid()));//""		
		hmParam.put("Chk_ShowPercentFl",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getShowpercentfl()));//""
		hmParam.put("SetId",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getSetid()));//""
		
		return hmParam;
	}	
	
	/* This is the method to populate the HashMap Key for Field Inv Report(MIM App).
	 * The returned HashMap contains all the key info's to get the field inv report details.
	 */
	private HashMap populateFISalesRepDrillParam(GmSalesReportFilterVO gmSalesReportFilterVO) throws AppError{
		HashMap hmParam = new HashMap();
		
		//Get the below params , from the input VO 
		hmParam.put("Condition",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getCondition()));
		hmParam.put("AccessFilter",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getAccessfilter()));
		
		hmParam.put("ADID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getAdid())); //1142
		hmParam.put("AccountID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getAcctid()));     //2015
		hmParam.put("Type",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getType()));  //LoadCons
		hmParam.put("FromMonth",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getFrmmonth())); //10
		hmParam.put("FromYear",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getFrmyear()));   //2014
		hmParam.put("ToMonth",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getTomonth()));    //01
		hmParam.put("ToYear",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getToyear()));      //2015
		hmParam.put("PartNumber",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getPnum()));     //1-30 days, 2-60 days,3-90 days
		hmParam.put("SetNumber",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getSetnum()));    //300.003
		hmParam.put("DistributorID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getDid()));   //8			
		hmParam.put("GroupAccId",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getGroupaccid())); //231	
		hmParam.put("SalesType",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getSalestype())); //50300-Sales Consignment, 50301-Loaner		
		hmParam.put("TerritoryID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getTerrid()));  //2014	
		hmParam.put("RepID",GmCommonClass.parseNull((String)gmSalesReportFilterVO.getRepid()));   //421
		
		String strAction = GmCommonClass.parseNull((String)gmSalesReportFilterVO.getAction()); 
		if(strAction.equals("LoadDistributor")){
			hmParam.put("RPTTYPE","DIST");
		}
		return hmParam;
	}
	/* This is the method to populate the HashMap Key for Field Inv Report(MIM App).
	 * The returned HashMap contains all the key info's to get the field inv report details.
	*/
	private HashMap populateFIConsignDetDrillParam( GmConsignDetailVO gmConsignDetailVO) throws AppError{
		
		HashMap hmParam = new HashMap();
		//Get the below params , from the input VO 
		hmParam.put("Condition",GmCommonClass.parseNull((String)gmConsignDetailVO.getCondition()));
		hmParam.put("AccessFilter",GmCommonClass.parseNull((String)gmConsignDetailVO.getAccessfilter()));
		
		hmParam.put("TURNS",GmCommonClass.parseNull((String)gmConsignDetailVO.getTurns()));         //1-30 days, 2-60 days,3-90 days
		hmParam.put("SALESTYPE",GmCommonClass.parseNull((String)gmConsignDetailVO.getSalestype())); //50300-Sales Consignment, 50301-Loaner		
		hmParam.put("ADID",GmCommonClass.parseNull((String)gmConsignDetailVO.getAdid())); //1142
		hmParam.put("AccountID",GmCommonClass.parseNull((String)gmConsignDetailVO.getAcctid()));     //2015
		hmParam.put("Type",GmCommonClass.parseNull((String)gmConsignDetailVO.getType()));  //LoadCons
		hmParam.put("TerritoryID",GmCommonClass.parseNull((String)gmConsignDetailVO.getTerrid()));   //2014
		hmParam.put("SetNumber",GmCommonClass.parseNull((String)gmConsignDetailVO.getSetnum()));     //300.003
		hmParam.put("DistributorID",GmCommonClass.parseNull((String)gmConsignDetailVO.getDid()));    //8		
        
        return hmParam;
	}
	
	private HashMap populateFIConsignDetailByPart( GmConsignDetailByPartVO gmConsignDetailsByPartVO) throws AppError{
		
		HashMap hmParam = new HashMap();
		//Get the below params , from the input VO 
		//hmParam.put("Condition",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getCondition()));
		//hmParam.put("AccessFilter",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getAccessfilter()));
		
		hmParam.put("SetNumber",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getSetid()));         //1-30 days, 2-60 days,3-90 days
		hmParam.put("PID",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getPid())); //50300-Sales Consignment, 50301-Loaner		
		hmParam.put("PDESC",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getPdesc())); //1142
		hmParam.put("SQTY",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getSqty()));     //2015
		
		hmParam.put("PRODFAMILY",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getProdfamily()));  //LoadCons
		hmParam.put("CRTFL",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getCrtfl()));   //2014
		hmParam.put("SETUSAGE",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getSetusage()));     //300.003
		hmParam.put("DistributorID",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getDid()));    //8	
		hmParam.put("PLINE",GmCommonClass.parseNull((String)gmConsignDetailsByPartVO.getPline()));    //8	
        
        return hmParam;
	}
}
