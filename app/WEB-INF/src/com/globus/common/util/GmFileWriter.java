/**
 * Description : This Class is Used for create file. 
 * Copyright : Globus Medical Inc
 *   
 */
package com.globus.common.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter; 

public class GmFileWriter {

	public void writeFile(String path,String fileName,String fileFormat,String data) throws  Exception {
		
		/* create a fileOutputStream object */
		FileOutputStream fileOutputStream = new FileOutputStream(path+fileName + "." + fileFormat);

		/* Encapsulate within printWriter */
		PrintWriter out = new PrintWriter(new BufferedOutputStream(fileOutputStream));

		/* if file Format is CSV */
		if (fileFormat.equals("CSV")) {
			writeCSV(out,data);			
		} else {
			out.print(data);
		}
			/* flush the output stream to write data */
			out.flush();
			out.close();
	} // End of write Function
	
	public void writeCSV(PrintWriter out,String data)  throws  Exception {
		 
			/* write the data */
			out.print(data);
			/* carriage return appended */
			out.print("\r");
			/* flush the output stream to write data */
			out.flush(); 
		 
	} // End of writeCSV function
}



