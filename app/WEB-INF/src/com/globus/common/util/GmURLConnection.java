package com.globus.common.util;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.valueobject.common.GmDataStoreVO;



public class GmURLConnection extends GmBean {

  public GmURLConnection() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmURLConnection(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  public void urlconnection(String resourcename, String strPara) throws Exception {
    ObjectOutputStream out = null;
    ObjectInputStream in = null;
    HttpURLConnection conn = null;
    HashMap hmReturn = new HashMap();
    String strport = GmCommonClass.parseNull(GmCommonClass.getString("JMS_PORT"));
    try {
      InetAddress localHost = InetAddress.getLocalHost();
      String strCompanyLocale = GmCommonClass.getCompanyLocale(getCompId());
      GmResourceBundleBean gmResourceBundleBean =
          GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
      
      
       /*String strUrl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("URLADDRESS"));
       String strUrl = GmCommonClass.parseNull((String) GmCommonClass.getString("strUrl"));
       String ipAddress = strUrl.equals("") ? "http://" + localHost.getHostAddress() : strUrl;
       strport = strport.equals("") ? "" : strport;
       URL url = new URL(ipAddress + strport + "/" + resourcename);*/
      
     // Get property from JBOSS Server environment 
      String strUrl = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("URLADDRESS"));
      String hostURL = strUrl.equals("") ? "http://" + System.getProperty("HOST_NAME") : strUrl;
      strport = strport.equals("") ? ":8080" : strport;
      URL url = new URL(hostURL + strport + "/" + resourcename);
      
   
      

      conn = (HttpURLConnection) url.openConnection();

      conn.setDoOutput(true);

      out = new ObjectOutputStream(conn.getOutputStream());

      out.writeObject(strPara);
      out.close();

      in = new ObjectInputStream(conn.getInputStream());
      in.close();
      conn.disconnect();
    } catch (Exception e) {
      if (out != null) {
        out.close();
      }

      if (in != null) {
        in.close();
      }

      if (conn != null) {
        conn.disconnect();
      }
      e.printStackTrace();
      throw e;
    }

  }
}
