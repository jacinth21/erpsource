package com.globus.common.util;


import java.util.concurrent.ConcurrentHashMap;

import org.apache.amber.oauth2.as.issuer.MD5Generator;
import org.apache.amber.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.amber.oauth2.as.request.OAuthAuthzRequest;

import com.globus.common.beans.AppError;
import com.globus.valueobject.logon.GmUserVO;

public class GmTokenManager {
	
  	private static ConcurrentHashMap<String, GmUserVO> tokens = null;  

     public static ConcurrentHashMap<String, GmUserVO> getInstance() {
        if(tokens == null){
        	tokens = new ConcurrentHashMap<String, GmUserVO>();
        }
        return tokens;
     }
     
	 public static boolean verify(String strToken){
		 boolean flag = false;
		 ConcurrentHashMap<String, GmUserVO> hm = getInstance();
		  
		 if(strToken ==  null || strToken.equals("")){
			 flag = false;
		 }
		 GmUserVO tk = hm.get(strToken);
		 if(tk ==  null)
		 {
			 flag = false;
		 }
		 else
		 {
			 flag = true;			 
		 }
		 return flag;
	 }
	 
	 public static String getNewToken() throws AppError
	  {
		  String strToken = "";		  
		  OAuthAuthzRequest oauthRequest = null;
		  try {
			  OAuthIssuerImpl oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());
			  strToken = oauthIssuerImpl.authorizationCode();
		  }
		  catch(Exception ex)
		  {
			  ex.printStackTrace();
			  throw new AppError(ex);
		  }
		  return strToken;
	  }
	 
	 public static void addToken(String strToken, GmUserVO gmUserVO) 
	 {
		 tokens = getInstance();
		 tokens.put(strToken, gmUserVO);
	 }
	 
	 public static GmUserVO getTokenValue(String strToken)	 
	 {
		 return tokens.get(strToken);
	 }
	 
}



