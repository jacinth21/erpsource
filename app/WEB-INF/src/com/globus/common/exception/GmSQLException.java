package com.globus.common.exception;

import java.sql.SQLException;

public class GmSQLException extends SQLException{

	public GmSQLException(SQLException sqlExp) {
		super(sqlExp.getMessage(),sqlExp.getSQLState(),sqlExp.getErrorCode());
	}

	public GmSQLException(String reason) {
		super(reason);
		
	}

	public GmSQLException(String reason, String SQLState) {
		super(reason, SQLState);
		
	}

	public GmSQLException(String reason, String SQLState, int vendorCode) {
		super(reason, SQLState, vendorCode);
		
	}

	public String getMessage()
	{
		String strMessage = super.getMessage();
		int iErrorCode = super.getErrorCode();
		String strORANumber = "ORA-" + iErrorCode + ":";
		strMessage = strMessage.replaceAll(strORANumber, "");		
		strMessage = strMessage.substring(0,strMessage.indexOf("\n"));
		return strMessage;
	}
	
}
