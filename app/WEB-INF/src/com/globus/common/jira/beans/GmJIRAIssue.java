package com.globus.common.jira.beans;

public class GmJIRAIssue {
	
	private String project ="";
	private String type ="";
	private String summary ="";
	private String reporter = "";
	private String assignee = "";
	private String description ="";
	private String requestId = "";
	private String requestDetailId = "";
	private String doId = "";
	private String dueDate = "";
	private String status = "";
	private String key = "";
	private String comment = "";
	private String attachment = "";
	private String attachmentFileName = "";
	private String partNumber = "";
	private String projectName = "";
	private String partCustFieldId = "";
	private String projectCustFieldId = "";
	private String reqCustFieldId = "";
	
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getAttachmentFileName() {
		return attachmentFileName;
	}
	public void setAttachmentFileName(String attachmentFileName) {
		this.attachmentFileName = attachmentFileName;
	}
	
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getRequestDetailId() {
		return requestDetailId;
	}
	public void setRequestDetailId(String requestDetailId) {
		this.requestDetailId = requestDetailId;
	}
	public String getDoId() {
		return doId;
	}
	public void setDoId(String doId) {
		this.doId = doId;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getReporter() {
		return reporter;
	}
	public void setReporter(String reporter) {
		this.reporter = reporter;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getPartCustFieldId() {
		return partCustFieldId;
	}
	public void setPartCustFieldId(String partCustFieldId) {
		this.partCustFieldId = partCustFieldId;
	}
	public String getProjectCustFieldId() {
		return projectCustFieldId;
	}
	public void setProjectCustFieldId(String projectCustFieldId) {
		this.projectCustFieldId = projectCustFieldId;
	}
	public String getReqCustFieldId() {
		return reqCustFieldId;
	}
	public void setReqCustFieldId(String reqCustFieldId) {
		this.reqCustFieldId = reqCustFieldId;
	}
	
}
