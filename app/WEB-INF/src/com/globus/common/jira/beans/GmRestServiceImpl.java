/**
 * 
 */
package com.globus.common.jira.beans;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.joda.time.DateTime;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClientFactory;
import com.atlassian.jira.rest.client.api.domain.BasicIssue;
import com.atlassian.jira.rest.client.api.domain.Comment;
import com.atlassian.jira.rest.client.api.domain.Issue;
import com.atlassian.jira.rest.client.api.domain.Transition;
import com.atlassian.jira.rest.client.api.domain.User;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.api.domain.input.TransitionInput;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.atlassian.util.concurrent.Promise;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;

/**
 * @author vprasath
 *
 */
public class GmRestServiceImpl implements GmJIRAServiceInterface {
	
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	
	 private  JiraRestClientFactory restClientFactory = null; 
	 private  JiraRestClient restClient = null;	
	 private URI jiraServerUri = null;
	 private static String strUserName = null;
	 private static String strUserPass = null;
	 private String BASE_URL = null;
	
	 /**
	 * 
	 */
	public GmRestServiceImpl() throws AppError {
		try {
			restClientFactory = new AsynchronousJiraRestClientFactory();
			BASE_URL = GmCommonClass.getJiraConfig("REST_URL");
			jiraServerUri=  new URI(BASE_URL);
		}
		catch(Exception ex) {
			throw new AppError(AppError.APPLICATION,
					"Cannot Create Issue, Exceptions in initializing JIRA", "E");
		}
	}

	/* (non-Javadoc)
	 * @see com.globus.common.jira.beans.GmJIRAServiceInterface#createIssue(com.globus.common.jira.beans.GmJIRAIssue)
	 */
	@Override
	public String createIssue(GmJIRAIssue gmJIRAIssue) throws Exception {
			
			Issue issue = createIssue(gmJIRAIssue.getProject(), Long.parseLong(gmJIRAIssue.getType()), gmJIRAIssue.getSummary(), gmJIRAIssue.getDescription(), gmJIRAIssue.getAssignee(), gmJIRAIssue.getReporter());
			
			String strAttachment = GmCommonClass.parseNull(gmJIRAIssue.getAttachment());
			 
			if (!gmJIRAIssue.getStatus().equals(""))
			 {
				 progressWorkflowAction( issue, "Update Status");
			 }
			 
			 if (!strAttachment.equals(""))
			 {
				String strFileNames[] = new String[10];			
				String strExtension = strAttachment.substring(strAttachment.indexOf('.'));			
				String strFileName = gmJIRAIssue.getAttachmentFileName();
				
				if(strFileName == null || strFileName.equals("")){
					strFileName = "Attachment_" + issue.getKey();
				}
				 strFileNames[0] = strFileName+strExtension;
				  ;
				 try {			 
				 addAttachmentToIssue(issue.getKey(), strAttachment);
				 }
				 catch(Exception ex)
				 {
					 ex.getMessage();
				 }
			 }
			 log.debug("issue createIssue=>"+issue.getKey());
		return issue.getKey();
	}

	 private void progressWorkflowAction( Issue issue, String strStatus) {
		 	
			try {
			// now let's start progress on this issue
				// PC-2521 - Close NCTKT when corresponding Evaluation ID is closed.
				String transitionsUri = issue.getTransitionsUri().toString(); // getting transitions URL
				//PC-4589 Close NCTKT and update comment
				String searchKey = "/rest";
				String splitUrl =transitionsUri.substring(transitionsUri.indexOf(searchKey),transitionsUri.length());
				String url = BASE_URL + splitUrl; // concat the base url and splited url like Rest call
				URI urls =  new URI(url.toString());
				final Iterable<Transition> transitions = restClient.getIssueClient().getTransitions(urls).claim();
				log.debug("progressWorkflowAction==>1");
				final Transition startProgressTransition = getTransitionByName(transitions, strStatus);
				log.debug("progressWorkflowAction==>2");
				if(startProgressTransition != null) {  // transition is null checking 
					restClient.getIssueClient().transition(urls, new TransitionInput(startProgressTransition.getId())).claim();
					log.debug("progressWorkflowAction==>3");
				}
			}
			catch(Exception ex) {
				log.debug("exception in  progressWorkflowAction "+ex.getMessage());
		 		ex.getMessage();
			}

	 }	 
	
	 private  Transition getTransitionByName(Iterable<Transition> transitions, String transitionName) {
			for (Transition transition : transitions) {
				if (transition.getName().equals(transitionName)) {
					return transition;
				}
			}
			return null;
		}

	 public  boolean addAttachmentToIssue(String issueKey, String fullfilename) throws IOException{
			String jira_attachment_authentication = new String(org.apache.commons.codec.binary.Base64.encodeBase64((strUserName+":"+strUserPass).getBytes()));
			CloseableHttpClient httpclient = HttpClients.createDefault();
			
			HttpPost httppost = new HttpPost(BASE_URL+"/rest/api/latest/issue/"+issueKey+"/attachments");
			//
			httppost.setHeader("X-Atlassian-Token", "nocheck");
			httppost.setHeader("Authorization", "Basic "+jira_attachment_authentication);
			
			File fileToUpload = new File(fullfilename);
			FileBody fileBody = new FileBody(fileToUpload);
			
			HttpEntity entity = MultipartEntityBuilder.create()
					.addPart("file", fileBody)
					.build();
			
			httppost.setEntity(entity);
	        String mess = "executing request " + httppost.getRequestLine();
	        
	        
	        CloseableHttpResponse response;
			
	        try {
				response = httpclient.execute(httppost);
			} finally {
				httpclient.close();
			}
	        
			if(response.getStatusLine().getStatusCode() == 200)
				return true;
			else
				return false;
		}
				
				
	/* (non-Javadoc)
	 * @see com.globus.common.jira.beans.GmJIRAServiceInterface#updateIssue(com.globus.common.jira.beans.GmJIRAIssue)
	 */
	@Override
	public String updateIssue(GmJIRAIssue gmJIRAIssue) throws Exception {
		// TODO Auto-generated method stub
		Promise<Issue> promiseIssue= restClient.getIssueClient().getIssue(gmJIRAIssue.getKey());
		Issue issue = promiseIssue.claim();
        String strKey = issue.getKey();
        IssueInputBuilder input = new IssueInputBuilder();
        input.setFieldValue(gmJIRAIssue.getReqCustFieldId(), gmJIRAIssue.getRequestId());
        input.setFieldValue(gmJIRAIssue.getPartCustFieldId(), gmJIRAIssue.getPartNumber());
        input.setFieldValue(gmJIRAIssue.getProjectCustFieldId(), gmJIRAIssue.getProjectName());
        IssueInput issueInput = input.build();
        restClient.getIssueClient().updateIssue(strKey, issueInput).claim();
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.globus.common.jira.beans.GmJIRAServiceInterface#updateComment(com.globus.common.jira.beans.GmJIRAIssue)
	 */
	@Override
	public String updateComment(GmJIRAIssue gmJIRAIssue) throws Exception {

	        Promise<Issue> promiseIssue= restClient.getIssueClient().getIssue(gmJIRAIssue.getKey());
	        Issue issue = promiseIssue.claim();
	        String strKey = issue.getKey();
		// Adding a comment
	     // PC-2521 - Close NCTKT when corresponding Evaluation ID is closed.
	        String commentUrl = issue.getSelf().toString(); // getting self url for update to comments
	        //PC-4589 Close NCTKT and update comment
	        String searchKey = "/rest";
			String splitUrl =commentUrl.substring(commentUrl.indexOf(searchKey),commentUrl.length());
			String url = BASE_URL + splitUrl +"/comment/"; // after concat need to add comment then only we can update comments
			URI urls =  new URI(url.toString());
	        Promise<Void> promiseIssues = restClient.getIssueClient().addComment(urls, Comment.valueOf(gmJIRAIssue.getComment()));		 
	        promiseIssues.claim();
		 
		return strKey;
	}
	



	/* (non-Javadoc)
	 * @see com.globus.common.jira.beans.GmJIRAServiceInterface#getIssue(java.lang.String)
	 */
	@Override
	public GmJIRAIssue getIssue(String strKey) throws Exception {
		    Promise<Issue> promiseIssue= restClient.getIssueClient().getIssue(strKey);
	        Issue issue = promiseIssue.claim();
		return convertIssueToGmIssue(issue);
	}

	private GmJIRAIssue convertIssueToGmIssue(Issue rIssue) {
		GmJIRAIssue gmJIRAIssue = new GmJIRAIssue();

		gmJIRAIssue.setProject(rIssue.getProject().getName());
		gmJIRAIssue.setType(rIssue.getIssueType().getName());
		gmJIRAIssue.setAssignee(rIssue.getAssignee().getName());
		gmJIRAIssue.setReporter(rIssue.getReporter().getName());
		gmJIRAIssue.setDescription(rIssue.getDescription());
		gmJIRAIssue.setSummary(rIssue.getSummary());
		DateTime date = rIssue.getDueDate();
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		gmJIRAIssue.setDueDate(formatter.format(date));

		return gmJIRAIssue;
	}
	
	/* (non-Javadoc)
	 * @see com.globus.common.jira.beans.GmJIRAServiceInterface#createUser(com.globus.common.jira.beans.GmJIRAUser)
	 */
	@Override
	public String createUser(GmJIRAUser gmJIRAUser) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.globus.common.jira.beans.GmJIRAServiceInterface#getUser(java.lang.String)
	 */
	@Override
	public GmJIRAUser getUser(String strUserName) throws Exception {
		// Invoke the JRJC Client
		
		log.debug("strUserName: "+strUserName);
        Promise<User> promiseUser = restClient.getUserClient().getUser(strUserName);
		log.debug("1");
		User user = null;
		try {
         user = promiseUser.claim();
		}
		catch(Exception ex)
		{
			log.error(ex.getMessage());
			ex.printStackTrace();
		}
		log.debug("2");
		GmJIRAUser gmJIRAUser = convertUserToGmJIRAUser(user);
		log.debug("gmJIRAUser.getUserName():"+gmJIRAUser.getUserName());
		log.debug("gmJIRAUser.getFullName():"+gmJIRAUser.getFullName());
		log.debug("gmJIRAUser.getEmailAddress():"+gmJIRAUser.getEmail());
		
		return gmJIRAUser;
	}
	
	private GmJIRAUser convertUserToGmJIRAUser(User user) {
		GmJIRAUser gmJIRAUser = new GmJIRAUser();

		if (user != null) {
			gmJIRAUser.setUserName(user.getName());
			gmJIRAUser.setFullName(user.getDisplayName());
			gmJIRAUser.setEmail(user.getEmailAddress());
		}
		return gmJIRAUser;
	}

	/* (non-Javadoc)
	 * @see com.globus.common.jira.beans.GmJIRAServiceInterface#closeIssue(com.globus.common.jira.beans.GmJIRAIssue)
	 */
	@Override
	public void closeIssue(GmJIRAIssue gmJIRAIssue) throws Exception {
		 Promise<Issue> promiseIssue= restClient.getIssueClient().getIssue(gmJIRAIssue.getKey());
	        Issue issue = promiseIssue.claim();
	     // PC-2521 - Close NCTKT when corresponding Evaluation ID is closed.
			progressWorkflowAction(issue,"Done"); // before is Close is there but our JIRA status process is Done only is there that why i change Done
	}

	/* (non-Javadoc)
	 * @see com.globus.common.jira.beans.GmJIRAServiceInterface#loginToJIRA(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean loginToJIRA(String strUserName, String strPassword) throws Exception {
		boolean success = false;
		try {
			this.strUserName = strUserName;
			this.strUserPass = strPassword;
			restClient= restClientFactory.createWithBasicHttpAuthentication(jiraServerUri, strUserName, strPassword);
		} catch (Exception ex) {
			throw new AppError(
					AppError.APPLICATION,
					"Cannot Create Issue, JIRA User name or Password does not exist",
					"E");
		}
		return success;
	}
	
	 private  Issue createIssue(String project, Long key, String summary, String description, String strAssigneeName, String strReporterName) throws Exception
	    {
	        IssueInputBuilder issueBuilder = new IssueInputBuilder(project, key, summary);
	        issueBuilder.setDescription(description);
	        issueBuilder.setAssigneeName(strAssigneeName);
	        issueBuilder.setReporterName(strReporterName);
	        IssueInput issueInput = issueBuilder.build();
	       
	        Promise<BasicIssue> promise = restClient.getIssueClient().createIssue(issueInput);
	        BasicIssue basicIssue = promise.claim();
	        Promise<Issue> promiseJavaIssue = restClient.getIssueClient().getIssue(basicIssue.getKey());
	        Issue issue = promiseJavaIssue.claim();
	        return issue;
	    }


}
