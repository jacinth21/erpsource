package com.globus.common.jira.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.crypt.AESEncryptDecrypt;
import com.globus.common.db.GmDBManager;

public class GmJIRAHelperBean
{
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	
	public String createJIRAMissingTicket(String strRepName, String strUser, ArrayList alTicketDetails) throws AppError
	{
		log.debug("strRepName: " + strRepName);
		GmJIRAManager gmJIRAManager = new GmJIRAManager();
		GmJIRAIssue gmJIRAIssue = new GmJIRAIssue();
		
		AESEncryptDecrypt aesEncryptDecrypt = new AESEncryptDecrypt();
		strUser = aesEncryptDecrypt.decryptText(strUser);
		
		
		
		String strReporter = gmJIRAManager.checkAndcreateUser(strUser);
		String strAssignee = gmJIRAManager.checkAndcreateUser(strRepName);
		
		log.debug("strReporter: "+strReporter);
		log.debug("strAssignee: "+strAssignee);
		
		GmJIRAUser gmJIRAUser = new GmJIRAUser();
		
		try {
			gmJIRAUser = gmJIRAManager.getUser(strReporter);
			strReporter = gmJIRAUser.getUserName();
			if(strReporter == null || strReporter.equals(""))
			{
				gmJIRAUser.setUserName(GmCommonClass.getJiraConfig("DEFAULT_ISSUE_REPORTER"));	
			}
		}
		catch(Exception ex)
		{
			gmJIRAUser = new GmJIRAUser();
			gmJIRAUser.setUserName(GmCommonClass.getJiraConfig("DEFAULT_ISSUE_REPORTER"));			
		}
		gmJIRAIssue.setReporter(gmJIRAUser.getUserName());
		log.debug("strReporter: "+gmJIRAUser.getUserName());
		
		try {
			gmJIRAUser = gmJIRAManager.getUser(strAssignee);
			strAssignee = gmJIRAUser.getUserName();
			if(strAssignee == null || strAssignee.equals(""))
			{
				gmJIRAUser.setUserName(GmCommonClass.getJiraConfig("DEFAULT_ISSUE_ASSIGNEE"));	
			}			
		}
		catch(Exception ex)
		{
			gmJIRAUser = new GmJIRAUser();
			gmJIRAUser.setUserName(GmCommonClass.getJiraConfig("DEFAULT_ISSUE_ASSIGNEE"));			
		}
		gmJIRAIssue.setAssignee(gmJIRAUser.getUserName());
		
		log.debug("strAssignee: "+gmJIRAUser.getUserName());
		
		String strKey = "";
		
		HashMap hmMeta = (HashMap)alTicketDetails.get(0);
		
		log.debug("hmMeta: " + hmMeta);
		
		
		String strRtnDate = GmCommonClass.parseNull((String)hmMeta.get("RETDT"));
		String strExprDt = GmCommonClass.parseNull((String)hmMeta.get("EXPRETDT"));
		String strReqID = GmCommonClass.parseNull((String)hmMeta.get("REQ_ID"));
		String strReqDetId = GmCommonClass.parseNull((String)hmMeta.get("REQ_DET_ID"));
		
		gmJIRAIssue.setRequestId(strReqID);		
		gmJIRAIssue.setDueDate(strExprDt);
		 
		String strSummary = strReqID;
		//gmJIRAIssue.setAttachmentFileName("Missing_Tickets_For_"+strSummary);
		strSummary = GmCommonClass.getJiraConfig("MISSING_TICKET_SUMMARY") + " " + strSummary;
		strSummary = GmCommonClass.parseNull(strSummary).trim();
		gmJIRAIssue.setSummary(strSummary);
			
		gmJIRAIssue.setDescription("Please refer the attached file for the details");
		gmJIRAIssue.setProject(GmCommonClass.getJiraConfig("PROJECT_FOR_LOANER_ISSUES"));
		gmJIRAIssue.setType(GmCommonClass.getJiraConfig("MISSING_TICKET_TYPE"));
		gmJIRAIssue.setStatus(GmCommonClass.getJiraConfig("MISSING_TICKET_INI_STATUS"));

		strKey = gmJIRAManager.createIssue(gmJIRAIssue);
		
		
		log.debug("Key generated: " + strKey );
		
		return strKey;
	}
	
	public String getMissingTicketDescription(ArrayList alTicketDetails )
	{
		Iterator itrTickets = alTicketDetails.iterator();
		String strSet = "";
		String strPreviousSet = "";
		
			String strMessageDesc = "";
			
			
			while (itrTickets.hasNext())
			{
				HashMap hmTemp = (HashMap)itrTickets.next();
				
				strSet = GmCommonClass.parseNull((String)hmTemp.get("SETDET"));
				
				if(!strSet.equals(strPreviousSet))
				{
					strMessageDesc = strMessageDesc + "<BR><BR>";
					strMessageDesc = strMessageDesc + "Please be advised that, " + strSet + " is missing the following parts";
					strMessageDesc = strMessageDesc + "<BR><BR>";
					strPreviousSet = strSet;
				}	
				
				
				String strPnum = GmCommonClass.parseNull((String)hmTemp.get("PARTNUMBER"));
				String strMissQty = GmCommonClass.parseNull((String)hmTemp.get("MISSQTY"));

				strMessageDesc = strMessageDesc +  strPnum +" - " + strMissQty;
				strMessageDesc = strMessageDesc + "<BR>";
			}
			
			strMessageDesc = strMessageDesc +"<BR>"; 
			strMessageDesc = strMessageDesc +"Please help us in resolving this issue with the missing parts by the Expected Return Date mentioned in the attached PDF.";
			strMessageDesc = strMessageDesc +"<BR>Thank you in advance for your help.";
			strMessageDesc = strMessageDesc +" Refer attached file for additional details.";
			strMessageDesc = strMessageDesc +"<BR><BR>Globus Contact Information: ";
			
			strMessageDesc = strMessageDesc +"<BR><BR>Email: " +GmCommonClass.getEmailProperty("GmCommon" + ".GlobusTicketEmail");
			strMessageDesc = strMessageDesc +"<BR>Phone: " + GmCommonClass.getEmailProperty("GmCommon" + ".GlobusCSPhone");
			
			strMessageDesc = strMessageDesc +"<BR><BR>" + GmCommonClass.getEmailProperty("GmMissingParts" + ".Footer");
			
			return strMessageDesc;
	}
	
	public void saveTicket(GmDBManager gmDBManager, HashMap hmParam) throws AppError 
	{
		log.debug("hmParam: " + hmParam);
		
		String strTicketID = GmCommonClass.parseNull((String)hmParam.get("TICKETID"));
		String strRefID = GmCommonClass.parseNull((String)hmParam.get("REFID"));
		String strRefType = GmCommonClass.parseNull((String)hmParam.get("REFTYPE"));
		String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_sav_jira_ticket", 4);
		gmDBManager.setString(1, strTicketID);
		gmDBManager.setString(2, strRefID);
		gmDBManager.setString(3, strRefType);
		gmDBManager.setString(4, strUserId);

		gmDBManager.execute();
		gmDBManager.commit();
		
		
	}
	
	public String checkTicketExists(String strRefID, String strRefType) throws AppError 
	{
		String strTicket = "";
		
		GmDBManager gmDBManager = new GmDBManager();	
		gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_jira_ticket", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strRefID);
		gmDBManager.setString(2, strRefType);		

		gmDBManager.execute();
		strTicket = gmDBManager.getString(3);
		gmDBManager.close();
		
		return GmCommonClass.parseNull(strTicket);
	}
	
	public void updateReconDetailsInMT(String strTicketID, ArrayList alReconList, ArrayList alMissingList, String strExtraTextToBeAdded) throws AppError
	{
		GmJIRAManager gmJIRAManager = new GmJIRAManager();
		GmJIRAIssue gmJIRAIssue = new GmJIRAIssue();
		Iterator alReconItr = alReconList.iterator();
		Iterator alMissinItr = alMissingList.iterator();
		
		String strMessageDesc = "";
		
		if (alMissingList.size() ==0)
		{
			strMessageDesc =strMessageDesc + "";
		}
		else
		{
			strMessageDesc = strMessageDesc +"Please be advised that the following Parts are still missing: \n";
		
			while (alMissinItr.hasNext())
			{
				HashMap hmTemp = (HashMap)alMissinItr.next();
				String strPnum = GmCommonClass.parseNull((String)hmTemp.get("PNUM"));
				String strQty = GmCommonClass.parseNull((String)hmTemp.get("QTY"));
				String strPrice = GmCommonClass.parseNull((String)hmTemp.get("PRICE"));
				String strEPrice = GmCommonClass.parseNull((String)hmTemp.get("EPRICE"));
				
				if (strPnum.equals(""))
				{
					strPnum = GmCommonClass.parseNull((String)hmTemp.get("PARTNUMBER"));
				}
				
				if (strQty.equals(""))
				{
					strQty = GmCommonClass.parseNull((String)hmTemp.get("MISSQTY"));
				}
				
				
				
				strMessageDesc = strMessageDesc +"Part Number: " + strPnum+" Qty: "+strQty+" Price: $"+GmCommonClass.getStringWithCommas(strPrice)+"Extended Price: $"+GmCommonClass.getStringWithCommas(strEPrice)+"\n";
			}
		}

		strMessageDesc = strMessageDesc +"\n";
		
		if (alReconList.size() == 0)
		{
			strMessageDesc =strMessageDesc + "";
		}
		else
		{
		strMessageDesc = strMessageDesc +"Please be advised that the following parts are reconciled:\n";
		strMessageDesc = strMessageDesc +"\n";			
		
		
		while (alReconItr.hasNext())
		{
			HashMap hmTemp = (HashMap)alReconItr.next();
			String strPnum = GmCommonClass.parseNull((String)hmTemp.get("PNUM"));
			String strQty = GmCommonClass.parseNull((String)hmTemp.get("QTY"));
			String strRecon = GmCommonClass.parseNull((String)hmTemp.get("RECONCILED_AS"));
			strMessageDesc = strMessageDesc +"Part Number: "+strPnum+" -  Qty: "+strQty+" - Reconciled as: "+strRecon+"\n";
		}		 
		}

		if (!strExtraTextToBeAdded.equals(""))
		{
			strMessageDesc = strMessageDesc +"\n";
			strMessageDesc = strMessageDesc + strExtraTextToBeAdded;
		}
		log.debug("strMessageDesc: " + strMessageDesc);
		gmJIRAIssue.setKey(strTicketID);
		gmJIRAIssue.setComment(strMessageDesc);
		gmJIRAManager.updateComment(gmJIRAIssue);
	}
	
	public void closeIssue(String strTicketID, String strType) throws AppError
	{
		GmJIRAIssue gmJIRAIssue = new GmJIRAIssue();
		GmJIRAManager gmJIRAManager = new GmJIRAManager();
		
		if (strType.equals("10056") || strType.equals("110520")) // Missing Ticket, 110520 - NCMR Evaluation
		{
			gmJIRAIssue.setKey(strTicketID);			
			gmJIRAManager.closeIssue(gmJIRAIssue);
		}
	}
	
	/*
	 * Description: To create JIRA ticket for NCMR when Evaluation ID created
	 * 
	 * Author: Vinoth
	 */
	public String createJIRANCMRTicket(HashMap hmParam) throws AppError
	{
		
		GmJIRAManager gmJIRAManager = new GmJIRAManager();
		GmJIRAIssue gmJIRAIssue = new GmJIRAIssue();
		String strAssignee = "";
		String strReporter = "";
		
		String strUser = GmCommonClass.parseNull((String)hmParam.get("USERNM"));
		String strRepName = GmCommonClass.parseNull((String)hmParam.get("PARTYNAME"));
        
		String strUnAssignFl = GmCommonClass.parseNull((String)hmParam.get("UNASSIGNFL"));
		
		if(!strUser.equals("")){
		strReporter = GmCommonClass.parseNull(gmJIRAManager.checkAndcreateUser(strUser));
		}
		
		if(!strRepName.equals("")) {
			strAssignee = GmCommonClass.parseNull(gmJIRAManager.checkAndcreateUser(strRepName));
		}
		
		log.debug("createJIRANCMRTicket=strReporter: "+strReporter);
		log.debug("createJIRANCMRTicket=strAssignee: "+strAssignee);
		
		GmJIRAUser gmJIRAUser = new GmJIRAUser();
		
		try {
			gmJIRAUser = gmJIRAManager.getUser(strReporter);
			strReporter = gmJIRAUser.getUserName();
			if(strReporter == null || strReporter.equals(""))
			{
				gmJIRAUser.setUserName(GmCommonClass.getJiraConfig("DEFAULT_REPORTER"));	
			}
		}
		catch(Exception ex)
		{
			gmJIRAUser = new GmJIRAUser();
			gmJIRAUser.setUserName(GmCommonClass.getJiraConfig("DEFAULT_REPORTER"));			
		}
		gmJIRAIssue.setReporter(gmJIRAUser.getUserName());
		log.debug("createJIRANCMRTicket=strReporter: "+gmJIRAUser.getUserName());
		
		try {
			gmJIRAUser = gmJIRAManager.getUser(strAssignee);
			strAssignee = gmJIRAUser.getUserName();
			if((strAssignee == null || strAssignee.equals("")) && (!strUnAssignFl.equals("Y")))
			{
				gmJIRAUser.setUserName(GmCommonClass.getJiraConfig("DEFAULT_ISSUE_ASSIGNEE"));	
			}			
		}
		catch(Exception ex)
		{
			gmJIRAUser = new GmJIRAUser();
			gmJIRAUser.setUserName(GmCommonClass.getJiraConfig("DEFAULT_ISSUE_ASSIGNEE"));			
		}
		gmJIRAIssue.setAssignee(gmJIRAUser.getUserName());
		
		log.debug("createJIRANCMRTicket=strAssignee: "+gmJIRAUser.getUserName());
		
		String strKey = "";
		String strEvalId = GmCommonClass.parseNull((String)hmParam.get("EVALID"));
		String strSummary = GmCommonClass.parseNull((String)hmParam.get("SUMMARY"));
		String strDescription = GmCommonClass.parseNull((String)hmParam.get("DESCRIPTION"));
		String strProject = GmCommonClass.parseNull((String)hmParam.get("PROJECT"));
		String strType = GmCommonClass.parseNull((String)hmParam.get("TYPE"));
		String strStatus = GmCommonClass.parseNull((String)hmParam.get("STATUS"));
		String strPartNum = GmCommonClass.parseNull((String)hmParam.get("PNUM"));
		String strProjectName = GmCommonClass.parseNull((String) hmParam.get("PROJNM"));
		String strPartCustFieldId = GmCommonClass.parseNull((String)hmParam.get("PARTCUSTFIELDID"));
		String strProjCustFieldId = GmCommonClass.parseNull((String)hmParam.get("PROJCUSTFIELDID"));
		String strReqCustFieldId = GmCommonClass.parseNull((String)hmParam.get("REQCUSTFIELDID"));
		
		gmJIRAIssue.setRequestId(strEvalId); 
		gmJIRAIssue.setSummary(strSummary);
		gmJIRAIssue.setDescription(strDescription);
		gmJIRAIssue.setProject(strProject);
		gmJIRAIssue.setType(strType);
		gmJIRAIssue.setStatus(strStatus);
		gmJIRAIssue.setPartNumber(strPartNum);
		gmJIRAIssue.setProjectName(strProjectName);
		gmJIRAIssue.setPartCustFieldId(strPartCustFieldId);
		gmJIRAIssue.setProjectCustFieldId(strProjCustFieldId);
		gmJIRAIssue.setReqCustFieldId(strReqCustFieldId);
		
		try {
			strKey = gmJIRAManager.createIssue(gmJIRAIssue);
			log.debug("Key generated: EVAL " + strKey );
			
			gmJIRAIssue.setKey(strKey);
			log.debug("Key generated: EVAL1 " + strKey );
			gmJIRAManager.updateIssue(gmJIRAIssue);
			log.debug("Key generated: EVAL2 " + strKey );
			
		}
		catch(Exception ex) {
			log.debug("JiraHelperBean NCMR MEthod to create ticket  "+ex.getMessage());
			ex.getMessage();
		}
		return strKey;
	}
	
	/*
	 * Description: To save ticket Id for the Evaluation Id
	 * 
	 * Author: Vinoth
	 */
	public void saveTicket(HashMap hmParam) throws AppError 
	{
		log.debug("saveTicket hmParam=> "+hmParam);
		GmDBManager gmDBManager = new GmDBManager();	
		String strTicketID = GmCommonClass.parseNull((String)hmParam.get("TICKETID"));
		String strRefID = GmCommonClass.parseNull((String)hmParam.get("REFID"));
		String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		
		gmDBManager.setPrepareString("gm_pkg_op_ncmr.gm_sav_ncmr_jira_ticket", 3);
		gmDBManager.setString(1, strTicketID);
		gmDBManager.setString(2, strRefID);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}
	
	/*
	 * Description: Check whether ticket available for Evaluation Id
	 * 
	 * Author: Vinoth
	 */
	public String checkTicketExists(String strRefID) throws AppError 
	{
		String strTicket = "";
		
		GmDBManager gmDBManager = new GmDBManager();	
		gmDBManager.setPrepareString("gm_pkg_op_ncmr.gm_fch_ticket_id", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strRefID);

		gmDBManager.execute();
		strTicket = gmDBManager.getString(2);
		gmDBManager.close();
		
		return GmCommonClass.parseNull(strTicket);
	}
	
	/*
	 * Description: Update Comments in NCMR Jira Ticket when Evaluation closed
	 * 
	 * Author: Vinoth
	 */
	public void updateIssueComments(String strTicketID, String strMsg) throws AppError 
	{
		GmJIRAIssue gmJIRAIssue = new GmJIRAIssue();
		GmJIRAManager gmJIRAManager = new GmJIRAManager();
		gmJIRAIssue.setKey(strTicketID);
		gmJIRAIssue.setComment(strMsg);		
		gmJIRAManager.updateComment(gmJIRAIssue);
	}
	
	//Method to check existing ticket available for Request detail Id and Ref Type
	public String checkTicketExists(String strRefID, String strRefType, GmDBManager gmDBManager) throws AppError 
	{
		String strTicket = "";
		
		gmDBManager.setPrepareString("gm_pkg_cm_loaner_jobs.gm_fch_jira_ticket", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strRefID);
		gmDBManager.setString(2, strRefType);		

		gmDBManager.execute();
		strTicket = gmDBManager.getString(3);
		gmDBManager.close();
		
		return GmCommonClass.parseNull(strTicket);
	}
}