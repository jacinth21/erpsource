package com.globus.common.jira.beans;


public interface GmJIRAServiceInterface {

	public String createIssue(GmJIRAIssue gmJIRAIssue) throws Exception;
	
	public String updateIssue(GmJIRAIssue gmJIRAIssue) throws Exception;
	
	public String updateComment(GmJIRAIssue gmJIRAIssue) throws Exception;
	
	public GmJIRAIssue getIssue(String strKey) throws Exception;
	
	public String createUser(GmJIRAUser gmJIRAUser)throws Exception;
	
	public GmJIRAUser getUser(String strUserName) throws Exception;
	
	public void closeIssue(GmJIRAIssue gmJIRAIssue) throws Exception;
	
	public boolean loginToJIRA(String strUserName, String strPassword) throws Exception;
}
