package com.globus.common.jira.beans;

import java.io.File;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import _soapclient.JiraSoapService;

import com.atlassian.jira.rpc.exception.RemoteAuthenticationException;
import com.atlassian.jira.rpc.exception.RemotePermissionException;
import com.atlassian.jira.rpc.exception.RemoteValidationException;
import com.atlassian.jira.rpc.soap.beans.RemoteAttachment;
import com.atlassian.jira.rpc.soap.beans.RemoteAvatar;
import com.atlassian.jira.rpc.soap.beans.RemoteComment;
import com.atlassian.jira.rpc.soap.beans.RemoteComponent;
import com.atlassian.jira.rpc.soap.beans.RemoteConfiguration;
import com.atlassian.jira.rpc.soap.beans.RemoteCustomFieldValue;
import com.atlassian.jira.rpc.soap.beans.RemoteEntity;
import com.atlassian.jira.rpc.soap.beans.RemoteField;
import com.atlassian.jira.rpc.soap.beans.RemoteFieldValue;
import com.atlassian.jira.rpc.soap.beans.RemoteFilter;
import com.atlassian.jira.rpc.soap.beans.RemoteGroup;
import com.atlassian.jira.rpc.soap.beans.RemoteIssue;
import com.atlassian.jira.rpc.soap.beans.RemoteIssueType;
import com.atlassian.jira.rpc.soap.beans.RemoteNamedObject;
import com.atlassian.jira.rpc.soap.beans.RemotePermission;
import com.atlassian.jira.rpc.soap.beans.RemotePermissionScheme;
import com.atlassian.jira.rpc.soap.beans.RemotePriority;
import com.atlassian.jira.rpc.soap.beans.RemoteProject;
import com.atlassian.jira.rpc.soap.beans.RemoteProjectRole;
import com.atlassian.jira.rpc.soap.beans.RemoteProjectRoleActors;
import com.atlassian.jira.rpc.soap.beans.RemoteResolution;
import com.atlassian.jira.rpc.soap.beans.RemoteRoleActors;
import com.atlassian.jira.rpc.soap.beans.RemoteScheme;
import com.atlassian.jira.rpc.soap.beans.RemoteSecurityLevel;
import com.atlassian.jira.rpc.soap.beans.RemoteServerInfo;
import com.atlassian.jira.rpc.soap.beans.RemoteStatus;
import com.atlassian.jira.rpc.soap.beans.RemoteUser;
import com.atlassian.jira.rpc.soap.beans.RemoteVersion;
import com.atlassian.jira.rpc.soap.beans.RemoteWorklog;
import com.atlassian.jira_soapclient.SOAPSession;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFileReader;

public class GmSoapServiceImpl implements JiraSoapService,
		GmJIRAServiceInterface {

	private String BASE_URL = "";
	private JiraSoapService jiraSoapService;
	private String authToken;
	private SOAPSession soapSession;

	public GmSoapServiceImpl() throws AppError {
		try {
			BASE_URL = GmCommonClass.getJiraConfig("WSDL_URL");
			soapSession = new SOAPSession(new URL(BASE_URL));
		} catch (Exception ex) {
			throw new AppError(AppError.APPLICATION,
					"Cannot Create Issue, Exceptions in initializing JIRA", "E");

		}
	}

	public boolean loginToJIRA(String strUserName, String strPassword)
			throws AppError {
		boolean success = false;
		try {
			soapSession.connect(strUserName, strPassword);
			jiraSoapService = soapSession.getJiraSoapService();
			authToken = soapSession.getAuthenticationToken();
			success = true;
		} catch (Exception ex) {
			throw new AppError(
					AppError.APPLICATION,
					"Cannot Create Issue, JIRA User name or Password does not exist",
					"E");
		}
		return success;
	}

	public void addActorsToProjectRole(String arg0, String[] arg1,
			RemoteProjectRole arg2, RemoteProject arg3, String arg4)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.addActorsToProjectRole(arg0, arg1, arg2, arg3, arg4);
	}

	public boolean addAttachmentsToIssue(String arg0, String arg1,
			String[] arg2, byte[][] arg3) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.addAttachmentsToIssue(arg0, arg1, arg2, arg3);
	}

	public boolean addBase64EncodedAttachmentsToIssue(String arg0, String arg1,
			String[] arg2, String[] arg3) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.addBase64EncodedAttachmentsToIssue(arg0, arg1,
				arg2, arg3);
	}

	public void addComment(String arg0, String arg1, RemoteComment arg2)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.addComment(arg0, arg1, arg2);
	}

	public void addDefaultActorsToProjectRole(String arg0, String[] arg1,
			RemoteProjectRole arg2, String arg3) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.addDefaultActorsToProjectRole(arg0, arg1, arg2, arg3);

	}

	public RemotePermissionScheme addPermissionTo(String arg0,
			RemotePermissionScheme arg1, RemotePermission arg2,
			RemoteEntity arg3) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.addPermissionTo(arg0, arg1, arg2, arg3);
	}

	public void addUserToGroup(String arg0, RemoteGroup arg1, RemoteUser arg2)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.addUserToGroup(arg0, arg1, arg2);
	}

	public RemoteVersion addVersion(String arg0, String arg1, RemoteVersion arg2)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.addVersion(arg0, arg1, arg2);
	}

	public RemoteWorklog addWorklogAndAutoAdjustRemainingEstimate(String arg0,
			String arg1, RemoteWorklog arg2) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.addWorklogAndAutoAdjustRemainingEstimate(arg0,
				arg1, arg2);
	}

	public RemoteWorklog addWorklogAndRetainRemainingEstimate(String arg0,
			String arg1, RemoteWorklog arg2) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.addWorklogAndRetainRemainingEstimate(arg0, arg1,
				arg2);
	}

	public RemoteWorklog addWorklogWithNewRemainingEstimate(String arg0,
			String arg1, RemoteWorklog arg2, String arg3)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.addWorklogAndAutoAdjustRemainingEstimate(arg0,
				arg1, arg2);
	}

	public void archiveVersion(String arg0, String arg1, String arg2,
			boolean arg3) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.archiveVersion(arg0, arg1, arg2, arg3);
	}

	public RemoteGroup createGroup(String arg0, String arg1, RemoteUser arg2)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.createGroup(arg0, arg1, arg2);
	}

	public RemoteIssue createIssue(String arg0, RemoteIssue arg1)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.createIssue(arg0, arg1);
	}

	public RemoteIssue createIssueWithSecurityLevel(String arg0,
			RemoteIssue arg1, long arg2) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.createIssueWithSecurityLevel(arg0, arg1, arg2);
	}

	public RemotePermissionScheme createPermissionScheme(String arg0,
			String arg1, String arg2) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.createPermissionScheme(arg0, arg1, arg2);
	}

	public RemoteProject createProject(String arg0, String arg1, String arg2,
			String arg3, String arg4, String arg5, RemotePermissionScheme arg6,
			RemoteScheme arg7, RemoteScheme arg8) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.createProject(arg0, arg1, arg2, arg3, arg4,
				arg5, arg6, arg7, arg8);
	}

	public RemoteProject createProjectFromObject(String arg0, RemoteProject arg1)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.createProjectFromObject(arg0, arg1);
	}

	public RemoteProjectRole createProjectRole(String arg0,
			RemoteProjectRole arg1) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.createProjectRole(arg0, arg1);
	}

	public RemoteUser createUser(String arg0, String arg1, String arg2,
			String arg3, String arg4) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.createUser(arg0, arg1, arg2, arg3, arg4);
	}

	public void deleteGroup(String arg0, String arg1, String arg2)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.deleteGroup(arg0, arg1, arg2);

	}

	public void deleteIssue(String arg0, String arg1) throws RemoteException,
			RemotePermissionException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.deleteIssue(arg0, arg1);

	}

	public RemotePermissionScheme deletePermissionFrom(String arg0,
			RemotePermissionScheme arg1, RemotePermission arg2,
			RemoteEntity arg3) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.deletePermissionFrom(arg0, arg1, arg2, arg3);
	}

	public void deletePermissionScheme(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.deletePermissionScheme(arg0, arg1);

	}

	public void deleteProject(String arg0, String arg1) throws RemoteException,
			RemotePermissionException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {
		jiraSoapService.deleteProject(arg0, arg1);

	}

	public void deleteProjectAvatar(String arg0, long arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {
		// jiraSoapService.deleteProjectAvatar(arg0, arg1);
	}

	public void deleteProjectRole(String arg0, RemoteProjectRole arg1,
			boolean arg2) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {
		jiraSoapService.deleteProjectRole(arg0, arg1, arg2);

	}

	public void deleteUser(String arg0, String arg1) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.deleteUser(arg0, arg1);
	}

	public void deleteWorklogAndAutoAdjustRemainingEstimate(String arg0,
			String arg1) throws RemoteException, RemotePermissionException,
			RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.deleteWorklogAndAutoAdjustRemainingEstimate(arg0, arg1);
	}

	public void deleteWorklogAndRetainRemainingEstimate(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {
		jiraSoapService.deleteWorklogAndRetainRemainingEstimate(arg0, arg1);

	}

	public void deleteWorklogWithNewRemainingEstimate(String arg0, String arg1,
			String arg2) throws RemoteException, RemotePermissionException,
			RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {
		jiraSoapService.deleteWorklogWithNewRemainingEstimate(arg0, arg1, arg2);

	}

	public RemoteComment editComment(String arg0, RemoteComment arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.editComment(arg0, arg1);
	}

	public RemotePermission[] getAllPermissions(String arg0)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getAllPermissions(arg0);
	}

	public RemoteScheme[] getAssociatedNotificationSchemes(String arg0,
			RemoteProjectRole arg1) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getAssociatedNotificationSchemes(arg0, arg1);
	}

	public RemoteScheme[] getAssociatedPermissionSchemes(String arg0,
			RemoteProjectRole arg1) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getAssociatedPermissionSchemes(arg0, arg1);
	}

	public RemoteAttachment[] getAttachmentsFromIssue(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getAttachmentsFromIssue(arg0, arg1);
	}

	public RemoteNamedObject[] getAvailableActions(String arg0, String arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getAvailableActions(arg0, arg1);
	}

	public RemoteComment getComment(String arg0, long arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getComment(arg0, arg1);
	}

	public RemoteComment[] getComments(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getComments(arg0, arg1);
	}

	public RemoteComponent[] getComponents(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getComponents(arg0, arg1);
	}

	public RemoteConfiguration getConfiguration(String arg0)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getConfiguration(arg0);
	}

	public RemoteField[] getCustomFields(String arg0) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getCustomFields(arg0);
	}

	public RemoteRoleActors getDefaultRoleActors(String arg0,
			RemoteProjectRole arg1) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getDefaultRoleActors(arg0, arg1);
	}

	public RemoteFilter[] getFavouriteFilters(String arg0)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getFavouriteFilters(arg0);
	}

	public RemoteField[] getFieldsForAction(String arg0, String arg1,
			String arg2) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getFieldsForAction(arg0, arg1, arg2);
	}

	public RemoteField[] getFieldsForEdit(String arg0, String arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getFieldsForEdit(arg0, arg1);
	}

	public RemoteGroup getGroup(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getGroup(arg0, arg1);
	}

	public RemoteIssue getIssue(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getIssue(arg0, arg1);
	}

	public RemoteIssue getIssueById(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getIssueById(arg0, arg1);
	}

	public long getIssueCountForFilter(String arg0, String arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getIssueCountForFilter(arg0, arg1);
	}

	public RemoteIssueType[] getIssueTypes(String arg0) throws RemoteException,
			RemotePermissionException, RemoteAuthenticationException {

		return jiraSoapService.getIssueTypes(arg0);
	}

	public RemoteIssueType[] getIssueTypesForProject(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException {

		return jiraSoapService.getIssueTypesForProject(arg0, arg1);
	}

	public RemoteIssue[] getIssuesFromFilter(String arg0, String arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getIssuesFromFilter(arg0, arg1);
	}

	public RemoteIssue[] getIssuesFromFilterWithLimit(String arg0, String arg1,
			int arg2, int arg3) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getIssuesFromFilterWithLimit(arg0, arg1, arg2,
				arg3);
	}

	public RemoteIssue[] getIssuesFromJqlSearch(String arg0, String arg1,
			int arg2) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return null;
	}

	public RemoteIssue[] getIssuesFromTextSearch(String arg0, String arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getIssuesFromTextSearch(arg0, arg1);
	}

	public RemoteIssue[] getIssuesFromTextSearchWithLimit(String arg0,
			String arg1, int arg2, int arg3) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getIssuesFromTextSearchWithLimit(arg0, arg1,
				arg2, arg3);
	}

	public RemoteIssue[] getIssuesFromTextSearchWithProject(String arg0,
			String[] arg1, String arg2, int arg3) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getIssuesFromTextSearchWithProject(arg0, arg1,
				arg2, arg3);
	}

	public RemoteScheme[] getNotificationSchemes(String arg0)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getNotificationSchemes(arg0);
	}

	public RemotePermissionScheme[] getPermissionSchemes(String arg0)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getPermissionSchemes(arg0);
	}

	public RemotePriority[] getPriorities(String arg0) throws RemoteException,
			RemotePermissionException, RemoteAuthenticationException {

		return jiraSoapService.getPriorities(arg0);
	}

	public RemoteAvatar getProjectAvatar(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return null;
	}

	public RemoteAvatar[] getProjectAvatars(String arg0, String arg1,
			boolean arg2) throws RemoteException, RemotePermissionException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return null;
	}

	public RemoteProject getProjectById(String arg0, long arg1)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getProjectById(arg0, arg1);
	}

	public RemoteProject getProjectByKey(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getProjectByKey(arg0, arg1);
	}

	public RemoteProjectRole getProjectRole(String arg0, long arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getProjectRole(arg0, arg1);
	}

	public RemoteProjectRoleActors getProjectRoleActors(String arg0,
			RemoteProjectRole arg1, RemoteProject arg2) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getProjectRoleActors(arg0, arg1, arg2);
	}

	public RemoteProjectRole[] getProjectRoles(String arg0)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getProjectRoles(arg0);
	}

	public RemoteProject getProjectWithSchemesById(String arg0, long arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getProjectWithSchemesById(arg0, arg1);
	}

	public RemoteProject[] getProjectsNoSchemes(String arg0)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getProjectsNoSchemes(arg0);
	}

	public Calendar getResolutionDateById(String arg0, long arg1)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return null;
	}

	public Calendar getResolutionDateByKey(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return null;
	}

	public RemoteResolution[] getResolutions(String arg0)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException {

		return jiraSoapService.getResolutions(arg0);
	}

	public RemoteFilter[] getSavedFilters(String arg0) throws RemoteException,
			RemotePermissionException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getSavedFilters(arg0);
	}

	public RemoteSecurityLevel getSecurityLevel(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getSecurityLevel(arg0, arg1);
	}

	public RemoteSecurityLevel[] getSecurityLevels(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getSecurityLevels(arg0, arg1);
	}

	public RemoteScheme[] getSecuritySchemes(String arg0)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getSecuritySchemes(arg0);
	}

	public RemoteServerInfo getServerInfo(String arg0) throws RemoteException {

		return jiraSoapService.getServerInfo(arg0);
	}

	public RemoteStatus[] getStatuses(String arg0) throws RemoteException,
			RemotePermissionException, RemoteAuthenticationException {

		return jiraSoapService.getStatuses(arg0);
	}

	public RemoteIssueType[] getSubTaskIssueTypes(String arg0)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException {

		return jiraSoapService.getSubTaskIssueTypes(arg0);
	}

	public RemoteIssueType[] getSubTaskIssueTypesForProject(String arg0,
			String arg1) throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException {

		return jiraSoapService.getSubTaskIssueTypesForProject(arg0, arg1);
	}

	public RemoteUser getUser(String arg0, String arg1) throws RemoteException,
			RemotePermissionException, RemoteAuthenticationException {

		return jiraSoapService.getUser(arg0, arg1);
	}

	public RemoteVersion[] getVersions(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getVersions(arg0, arg1);
	}

	public RemoteWorklog[] getWorklogs(String arg0, String arg1)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.getWorklogs(arg0, arg1);
	}

	public boolean hasPermissionToCreateWorklog(String arg0, String arg1)
			throws RemoteException, RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.hasPermissionToCreateWorklog(arg0, arg1);
	}

	public boolean hasPermissionToDeleteWorklog(String arg0, String arg1)
			throws RemoteException, RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.hasPermissionToDeleteWorklog(arg0, arg1);
	}

	public boolean hasPermissionToEditComment(String arg0, RemoteComment arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.hasPermissionToEditComment(arg0, arg1);
	}

	public boolean hasPermissionToUpdateWorklog(String arg0, String arg1)
			throws RemoteException, RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.hasPermissionToUpdateWorklog(arg0, arg1);
	}

	public boolean isProjectRoleNameUnique(String arg0, String arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.isProjectRoleNameUnique(arg0, arg1);
	}

	public String login(String arg0, String arg1) throws RemoteException,
			RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.login(arg0, arg1);
	}

	public boolean logout(String arg0) throws RemoteException {

		return jiraSoapService.logout(arg0);
	}

	public RemoteIssue progressWorkflowAction(String arg0, String arg1,
			String arg2, RemoteFieldValue[] arg3) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.progressWorkflowAction(arg0, arg1, arg2, arg3);
	}

	public void refreshCustomFields(String arg0) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.refreshCustomFields(arg0);
	}

	public void releaseVersion(String arg0, String arg1, RemoteVersion arg2)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.releaseVersion(arg0, arg1, arg2);
	}

	public void removeActorsFromProjectRole(String arg0, String[] arg1,
			RemoteProjectRole arg2, RemoteProject arg3, String arg4)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.removeActorsFromProjectRole(arg0, arg1, arg2, arg3,
				arg4);
	}

	public void removeAllRoleActorsByNameAndType(String arg0, String arg1,
			String arg2) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.removeAllRoleActorsByNameAndType(arg0, arg1, arg2);
	}

	public void removeAllRoleActorsByProject(String arg0, RemoteProject arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.removeAllRoleActorsByProject(arg0, arg1);
	}

	public void removeDefaultActorsFromProjectRole(String arg0, String[] arg1,
			RemoteProjectRole arg2, String arg3) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.removeDefaultActorsFromProjectRole(arg0, arg1, arg2,
				arg3);
	}

	public void removeUserFromGroup(String arg0, RemoteGroup arg1,
			RemoteUser arg2) throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.removeUserFromGroup(arg0, arg1, arg2);
	}

	public void setNewProjectAvatar(String arg0, String arg1, String arg2,
			String arg3) throws RemoteException, RemotePermissionException,
			com.atlassian.jira.rpc.exception.RemoteException {

		// jiraSoapService.setNewProjectAvatar(arg0, arg1, arg2, arg3);
	}

	public void setProjectAvatar(String arg0, String arg1, long arg2)
			throws RemoteException, RemotePermissionException,
			com.atlassian.jira.rpc.exception.RemoteException {

		// jiraSoapService.setProjectAvatar(arg0, arg1, arg2);
	}

	public RemoteGroup updateGroup(String arg0, RemoteGroup arg1)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.updateGroup(arg0, arg1);
	}

	public RemoteIssue updateIssue(String arg0, String arg1,
			RemoteFieldValue[] arg2) throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.updateIssue(arg0, arg1, arg2);
	}

	public RemoteProject updateProject(String arg0, RemoteProject arg1)
			throws RemoteException, RemotePermissionException,
			RemoteValidationException, RemoteAuthenticationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		return jiraSoapService.updateProject(arg0, arg1);
	}

	public void updateProjectRole(String arg0, RemoteProjectRole arg1)
			throws RemoteException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.updateProjectRole(arg0, arg1);
	}

	public void updateWorklogAndAutoAdjustRemainingEstimate(String arg0,
			RemoteWorklog arg1) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.updateWorklogAndAutoAdjustRemainingEstimate(arg0, arg1);
	}

	public void updateWorklogAndRetainRemainingEstimate(String arg0,
			RemoteWorklog arg1) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.updateWorklogAndRetainRemainingEstimate(arg0, arg1);
	}

	public void updateWorklogWithNewRemainingEstimate(String arg0,
			RemoteWorklog arg1, String arg2) throws RemoteException,
			RemotePermissionException, RemoteValidationException,
			com.atlassian.jira.rpc.exception.RemoteException {

		jiraSoapService.updateWorklogWithNewRemainingEstimate(arg0, arg1, arg2);
	}
	
	

	private RemoteIssue covertGmIssueToRemoteIssue(GmJIRAIssue gmJIRAIssue)
			throws Exception {
		RemoteIssue remoteIssue = new RemoteIssue();

		Calendar cal = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date = formatter.parse(gmJIRAIssue.getDueDate());
		cal.setTime(date);

		remoteIssue.setProject(gmJIRAIssue.getProject());
		remoteIssue.setType(gmJIRAIssue.getType());
		remoteIssue.setSummary(gmJIRAIssue.getSummary());
		remoteIssue.setAssignee(gmJIRAIssue.getAssignee());
		remoteIssue.setReporter(gmJIRAIssue.getReporter());
		remoteIssue.setDescription(gmJIRAIssue.getDescription());
		remoteIssue.setDuedate(cal);

		String strDoid = gmJIRAIssue.getDoId();
		String strReqID = gmJIRAIssue.getRequestId();
		String strReqDetID = gmJIRAIssue.getRequestDetailId();

		RemoteCustomFieldValue customFieldValue = null;
		RemoteCustomFieldValue customFieldValue2 = null;
		RemoteCustomFieldValue customFieldValue3 = null;
		RemoteCustomFieldValue[] customFieldValues = new RemoteCustomFieldValue[1];
		int indexCount = 0;
		
		// Add custom fields
		
		if( strReqID != null && !strReqID.equals(""))
		{
			customFieldValue2 = new RemoteCustomFieldValue(
					GmCommonClass.getJiraConfig("REQID_CUSTOM_FIELD_ID"), "",
					new String[] { strReqID });
			customFieldValues[indexCount] = customFieldValue2;
		}
		                                                                        
		
		/*if (strDoid != null && !strDoid.equals(""))
		{
			 customFieldValue = new RemoteCustomFieldValue(
						GmCommonClass.getJiraConfig("DOID_CUSTOM_FIELD_ID"), "",
						new String[] { strDoid });
			 customFieldValues[indexCount++] = customFieldValue;
		}
		
		
		
		
		if(strReqDetID != null && strReqDetID.equals(""))
		{
			customFieldValue3 = new RemoteCustomFieldValue(
					GmCommonClass.getJiraConfig("REQDID_CUSTOM_FIELD_ID"), "",
					new String[] { strReqDetID });
			customFieldValues[indexCount++] = customFieldValue3;
		}*/

		
		remoteIssue.setCustomFieldValues(customFieldValues);

		return remoteIssue;

	}

	private GmJIRAIssue convertRemoteIssueToGmIssue(RemoteIssue rIssue) {
		GmJIRAIssue gmJIRAIssue = new GmJIRAIssue();

		gmJIRAIssue.setProject(rIssue.getProject());
		gmJIRAIssue.setType(rIssue.getType());
		gmJIRAIssue.setAssignee(rIssue.getAssignee());
		gmJIRAIssue.setReporter(rIssue.getReporter());
		gmJIRAIssue.setDescription(rIssue.getDescription());
		gmJIRAIssue.setSummary(rIssue.getSummary());
		Date date = rIssue.getDuedate().getTime();
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		gmJIRAIssue.setDueDate(formatter.format(date));

		return gmJIRAIssue;
	}

	public String createIssue(GmJIRAIssue gmJIRAIssue) throws Exception {
		
		 RemoteIssue rIssue = covertGmIssueToRemoteIssue(gmJIRAIssue);
		 String strAttachment = GmCommonClass.parseNull(gmJIRAIssue.getAttachment());
		 
		 rIssue = createIssue(authToken, rIssue);
		 if (!gmJIRAIssue.getStatus().equals(""))
		 {
			 progressWorkflowAction(authToken, rIssue.getKey(),gmJIRAIssue.getStatus(), null);
		 }
		 
		 if (!strAttachment.equals(""))
		 {
			String strFileNames[] = new String[10];			
			String strExtension = strAttachment.substring(strAttachment.indexOf('.'));			
			String strFileName = gmJIRAIssue.getAttachmentFileName();
			
			if(strFileName == null || strFileName.equals("")){
				strFileName = "Attachment_" + rIssue.getKey();
			}
			 strFileNames[0] = strFileName+strExtension;
			 byte[][] fileArray = getByteArray(strAttachment);
			 try {			 
			 addAttachmentsToIssue(authToken, rIssue.getKey(), strFileNames , fileArray);
			 }
			 catch(Exception ex)
			 {
				 ex.getMessage();
			 }
		 }
		return rIssue.getKey();
	}
	
	private byte[][] getByteArray(String strFileName)
	{
			
		File f = new File(strFileName);
		int size = (int)f.length();		
		byte[][] returnArray = new byte[10][size];		
		returnArray[0] = GmFileReader.load(strFileName);
		
		return returnArray;
	}

	public String createUser(GmJIRAUser gmJIRAUser) throws Exception {
		createUser(authToken, gmJIRAUser.getUserName(), gmJIRAUser
				.getPassword(), gmJIRAUser.getFullName(), gmJIRAUser.getEmail());
		return gmJIRAUser.getUserName();
	}

	public GmJIRAIssue getIssue(String strKey) throws Exception {
		RemoteIssue rIssue = getIssue(authToken, strKey);
		return convertRemoteIssueToGmIssue(rIssue);
	}

	public GmJIRAUser getUser(String strUserName) throws Exception {
		RemoteUser rUser = getUser(authToken, strUserName);
		GmJIRAUser gmJIRAUser = convertRemoteUserToGmJIRAUser(rUser);
		return gmJIRAUser;
	}

	
	private GmJIRAUser convertRemoteUserToGmJIRAUser(RemoteUser remoteUser) {
		GmJIRAUser gmJIRAUser = new GmJIRAUser();

		if (remoteUser != null) {
			gmJIRAUser.setUserName(remoteUser.getName());
			gmJIRAUser.setFullName(remoteUser.getFullname());
			gmJIRAUser.setEmail(remoteUser.getEmail());
		}
		return gmJIRAUser;
	}

	public String updateIssue(GmJIRAIssue gmJIRAIssue) throws Exception {
		// TODO
		return null;
	}

	public String updateComment(GmJIRAIssue gmJIRAIssue) throws Exception {

		// Adding a comment
		final RemoteComment comment = new RemoteComment();
		comment.setBody(gmJIRAIssue.getComment());
		jiraSoapService.addComment(authToken, gmJIRAIssue.getKey(), comment);
		
		return gmJIRAIssue.getKey();
	}

	public void closeIssue(GmJIRAIssue gmJIRAIssue) throws Exception
	{
		RemoteIssue rIssue = getIssue(authToken, gmJIRAIssue.getKey());
		String strCurrentStatus = GmCommonClass.parseNull(rIssue.getStatus());
		String strActionId = "";
		
		if (!strCurrentStatus.equals(""))
		{
			strActionId = GmCommonClass.getJiraConfig("CLOSE_TKT_ACTION_FROM_"+strCurrentStatus);
		}
		
		if(!strActionId.equals(""))
		{
			progressWorkflowAction(authToken, gmJIRAIssue.getKey(),strActionId, null);
		}
	
	}
}
