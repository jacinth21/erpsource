package com.globus.common.jira.beans;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmJIRAManager 
{

	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	
	public final static int SOAP_SERVICE = 0;
	public final static int REST_SERVICE = 1;
	
	private final static int DEFAULT_SERVICE = REST_SERVICE;
	
    private GmJIRAServiceInterface gmJIRAServiceInterface;
    private String strUserName = "";
    private String strPassWord ="";
    private int service =0;
    
	public GmJIRAManager() throws AppError{
		try {
		setService(DEFAULT_SERVICE);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw new AppError(ex);
		}
	}
	
	public void setService(int service) throws AppError
	{
		try
		{
		if(this.service != service)
		{
			this.service = service;
		}
		
		this.strUserName = GmCommonClass.getJiraConfig("PORTAL_JIRA_USER");
		this.strPassWord = GmCommonClass.getJiraConfig("PORTAL_JIRA_PASSWORD");
		
		gmJIRAServiceInterface = getService();
		gmJIRAServiceInterface.loginToJIRA(this.strUserName, this.strPassWord);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw new AppError(ex);
		}
	}
	
	private GmJIRAServiceInterface getService() throws AppError
	{
		GmJIRAServiceInterface gmJIRAServiceInterface = null;
		
		if(service == GmJIRAManager.SOAP_SERVICE)
		{
			gmJIRAServiceInterface =  new GmSoapServiceImpl();
		}
		else if (service == GmJIRAManager.REST_SERVICE)
		{
			gmJIRAServiceInterface = new GmRestServiceImpl();			
		}
		else
		{
			gmJIRAServiceInterface = null;
		}
		
		return gmJIRAServiceInterface;
	}
	
	public String createIssue(GmJIRAIssue gmJIRAIssue) throws AppError {
		try {		
			return gmJIRAServiceInterface.createIssue(gmJIRAIssue);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw new AppError(ex);
		}
	}
	
	public String createUser(GmJIRAUser gmJIRAUser) throws AppError{
		try
		{
			return gmJIRAServiceInterface.createUser(gmJIRAUser);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw new AppError(ex);
		}
	}
	
	public String checkAndcreateUser(String strUserName) throws AppError{
		try
		{
			GmJIRAUser gmJIRAUser = new GmJIRAUser();
			String strJiraUserName = getUserName(strUserName);
			
			log.debug("strJiraUserName: " + strJiraUserName);
			gmJIRAUser = getUser(strJiraUserName);
			log.debug("gmJIRAUser.getUserName(): " + gmJIRAUser.getUserName());
			
		/*	if (gmJIRAUser.getUserName().equals(""))
			{			
				log.debug("strJiraUserName: " + strJiraUserName);
				log.debug("strUserName: " + strUserName);
				gmJIRAUser.setFullName(strUserName);				
				gmJIRAUser.setUserName(strJiraUserName);
				gmJIRAUser.setPassword(GmCommonClass.getJiraConfig("DEFAULT_USER_PASSWORD"));
				gmJIRAUser.setEmail(GmCommonClass.getJiraConfig("DEFAULT_USER_EMAIL"));
				strJiraUserName = createUser(gmJIRAUser);
			}*/

			return strJiraUserName;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();			
			throw new AppError(ex);
		}
		
	}
	
	private String getUserName(String strFullName)
	{
		log.debug("strFullName: " + strFullName);
		log.debug("name:" + strFullName.substring(0, 1).toLowerCase() +  strFullName.substring(strFullName.lastIndexOf(" ") + 1 ).toLowerCase());
		String strRturnName = "";
		
		if(strFullName.lastIndexOf(" ") != -1)
		{
			strRturnName =  strFullName.substring(0, 1).toLowerCase() +  strFullName.substring(strFullName.lastIndexOf(" ") + 1 ).toLowerCase();
		}
		else
		{
			strRturnName = strFullName.toLowerCase();
		}
		
		return strRturnName;
	}

	public GmJIRAIssue getIssue(String strKey) throws AppError {
		try
		{
			return gmJIRAServiceInterface.getIssue(strKey);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw new AppError(ex);
		}
	}

	public GmJIRAUser getUser(String strUserName) throws AppError {
		try
		{
			return gmJIRAServiceInterface.getUser(strUserName);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw new AppError(ex);
		}
	}

	public String updateIssue(GmJIRAIssue gmJIRAIssue) throws AppError{
		try
		{
			return gmJIRAServiceInterface.updateIssue(gmJIRAIssue);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw new AppError(ex);
		}
	}
	
	public void closeIssue(GmJIRAIssue gmJIRAIssue) throws AppError
	{
		try
		{
			gmJIRAServiceInterface.closeIssue(gmJIRAIssue);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			// PC-2521 - Close NCTKT when corresponding Evaluation ID is closed.
//			throw new AppError(ex); //  We comment this throw exception because of we are working normal flow not in JMS configuration so excption came means flow not affect now
		}
	}
	
	public String updateComment(GmJIRAIssue gmJIRAIssue) throws AppError{
		try
		{
			return gmJIRAServiceInterface.updateComment(gmJIRAIssue);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			return ex.getMessage();
			// PC-2521 - Close NCTKT when corresponding Evaluation ID is closed.
//			throw new AppError(ex); //  We comment this throw exception because of we are working normal flow not in JMS configuration so excption came means flow not affect now  
		}
	}
}
