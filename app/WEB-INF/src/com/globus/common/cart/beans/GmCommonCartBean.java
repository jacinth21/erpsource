/*
 * Module: GmCommonCartBean.java Author: Dhinakaran James Project: Globus Medical App Date-Written:
 * 19 Jul 2007 Description:
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.common.cart.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.StringTokenizer;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.db.GmDBManager;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.prodmgmnt.beans.GmPartNumSearchBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmCommonCartBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  GmCommonClass gmCommon = new GmCommonClass();

  public GmCommonCartBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmCommonCartBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * loadOrderCartLists - This method will return ArrayLists for the Cart in the Phone order screen
   * 
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadOrderCartLists(HashMap hmParam) throws AppError {
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmRuleBean gmRuleBean = new GmRuleBean();
    HashMap hmReturn = new HashMap();

    ArrayList alOrderPartType = new ArrayList();
    ArrayList alConstructs = new ArrayList();
    ArrayList alColumnRule = new ArrayList();
    ArrayList alAdjCodes = new ArrayList();

    try {
      alOrderPartType = gmCommon.getCodeList("ORDTP");
      alConstructs = gmSales.reportGPOConstructList(hmParam);
      alColumnRule = gmRuleBean.fetchColumnRules("ORDCOL");
      log.debug(" alColumnRule1111>>>>>>>>>>>>>>>>e " + alColumnRule);
      alAdjCodes = gmCommon.getCodeList("PRIADJ");

      hmReturn.put("ORDPARTTP", alOrderPartType);
      hmReturn.put("CONSTRUCTS", alConstructs);
      hmReturn.put("COLRULE", alColumnRule);
      hmReturn.put("ADJCODES", alAdjCodes);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadItemOrderLists", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadOrderCartLists

  /**
   * getPartPriceForAccount - To fetcht the part number price for the given account.
   * 
   * @param Account Id ,Part Number
   * @return String
   * @exception AppError
   **/
  public HashMap getPartPriceForAccount(String strAccId, String strPartNum) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_rpt.gm_fch_acc_part_price", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strAccId);
    gmDBManager.setString(2, strPartNum);

    gmDBManager.execute();
    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(3)));
    gmDBManager.close();

    log.debug("Price Returned:" + hmReturn);
    return hmReturn;
  }

  /**
   * loadPartNum - This method will be used by report and setup pages of Part # for querying
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadPartNum(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());

    String strPartCompFiltr = "";
    String strFromType = "";
    String strCountryCode = "";
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
    String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACC"));
    String strRepId = GmCommonClass.parseNull((String) hmParam.get("REP"));
    String strGpoId = GmCommonClass.parseNull((String) hmParam.get("GPOID"));
    String strRegExp = GmCommonClass.parseNull((String) hmParam.get("REGEXP"));
    String strFromPg = GmCommonClass.parseNull((String) hmParam.get("FROMPG"));
    String strFromScreen = GmCommonClass.parseNull((String) hmParam.get("FROMSCREENPAGE"));
    strRegExp = strRegExp.equals("") ? "LIKEPRE" : strRegExp;

    GmCommonClass gmCom = new GmCommonClass();
    ArrayList alReturn = new ArrayList();
    String strCheck = "";
    String strPartNumReg = "";
    HashMap hmParams = new HashMap();
    String strPartNumQuery = getPartNumQuery(strPartNum, strRegExp,"t205");
    String strFilterPartNum = strPartNum.replace(',', '|');
    String strPartMapType = GmCommonClass.getRuleValue("RFS", "PART_MAP_TYPE");
    strPartCompFiltr =
        gmPartSearchBean.getPartCompanyFilter(strFilterPartNum, "t205", strPartMapType);
    String strCompanyID = getCompId();
    
    GmResourceBundleBean gmResourceBundleBean =
    		gmCom.getResourceBundleBean("properties.Company", gmCom.getCompanyLocale(strCompanyID));
    strCountryCode = GmCommonClass.parseNull((String)gmResourceBundleBean.getProperty("COUNTRYCODE"));


    if (strAccId.equals("0") && strRepId.equals("0") && strGpoId.equals("0")) {
      strCheck = "Y";
      hmParams.put("PARTNUM", strPartNum);
      hmParams.put("SEARCH", strRegExp);

      strPartNumReg = gmCom.createRegExpString(hmParams);
    } else {
      strPartNumReg = "^".concat(strPartNum).concat("$");
    }


    if (strFromPg.equals("MA")) {
      strFromType = GmCommonClass.parseNull((String) hmParam.get("HMAFROMTYPE"));
      log.debug(" From Cost TYpe " + strFromType);
      gmDBManager.setPrepareString("gm_pkg_cm_cart.gm_ac_fch_maqty", 3);

      gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

      gmDBManager.setString(1, strPartNum);
      gmDBManager.setString(2, strFromType);
      gmDBManager.execute();
      alReturn =
          GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
              .getObject(3)));
      gmDBManager.close();
    } else {
      if (!strPartNumQuery.equals("")) {
        StringBuffer sbQuery = new StringBuffer();
        sbQuery.append(" SELECT	t205.c205_part_number_id PARTNUM, t205.c205_part_num_desc PDESC");
        sbQuery.append(" , NVL(get_qty_in_stock(t205.c205_part_number_id),'0') STOCK ");
        sbQuery
            .append(" , GET_PART_PRICE(t205.c205_part_number_id,'C') CONSPRICE, GET_QTY_IN_INV(t205.c205_part_number_id,'56001') RWQTY ");
        if (!strCheck.equals("Y")) {
          HashMap hmMap = getPartPriceForAccount(strAccId, strPartNum);

          String strPrice = GmCommonClass.parseZero((String) hmMap.get("PRICE"));
          String strUnitPrice = GmCommonClass.parseZero((String) hmMap.get("UNITPRICE"));
          String strUnitAdj = GmCommonClass.parseZero((String) hmMap.get("UNITADJ"));
          String strAdjCode = GmCommonClass.parseZero((String) hmMap.get("ADJCODE"));
          String strWaste = GmCommonClass.parseZero((String) hmMap.get("WASTE"));
          String strRevision = GmCommonClass.parseZero((String) hmMap.get("REVISION"));
          sbQuery.append(", " + strPrice);
          sbQuery.append(" PRICE ");
          sbQuery.append(", " + strUnitPrice);
          sbQuery.append(" UNITPRICE ");
          sbQuery.append(", " + strUnitAdj);
          sbQuery.append(" UNITADJ ");
          sbQuery.append(", " + strAdjCode);
          sbQuery.append(" ADJCODE ");
          sbQuery.append(", " + strWaste);
          sbQuery.append(" WASTE ");
          sbQuery.append(", " + strRevision);
          sbQuery.append(" REVISION ");
          sbQuery.append(", get_cs_fch_loaner_status('");
          sbQuery.append(strPartNum);
          sbQuery.append("','");
          sbQuery.append(strRepId);
          sbQuery.append("') LOANERSTATUS ");
          sbQuery.append(",NVL(get_ac_cogs_value('");
          sbQuery.append(strPartNum);
          sbQuery.append("',4906 ),");
          // OUS code merge - get_ac_cogs_value return 0 for OUS
          sbQuery.append(!strCountryCode.equals("en") ? "0" : "NULL");
          sbQuery.append(" )LOANCOGS ");
        }



        // Not used anywhere. to be used later
        sbQuery.append(" ,decode('");
        sbQuery.append(strCheck);
        sbQuery.append("','Y','0',GET_PART_PRICE(t205.c205_part_number_id,'L')) LPRICE ");
        sbQuery
            .append(" ,  TO_CHAR(CURRENT_DATE,get_compdtfmt_frm_cntx ()) CURRDATE ,decode(gm_pkg_pd_partnumber.get_rfs_info(t205.c205_part_number_id),NULL,'N','Y') RFL, ");
        if(strFromScreen.equals("SALESORDER")) {//PC-3867-Making mandatory to record lot for joint reconstruction parts
        	sbQuery.append(" decode(gm_pkg_op_do_process_trans.get_control_number_needed_fl(t205.c205_part_number_id),'Y','Y',gm_pkg_op_do_process_trans.get_ctrl_num_needed_fl_by_division(t205.c205_part_number_id)) CTRL_NEEDED_FL ");
        }else {
        sbQuery.append(" gm_pkg_op_do_process_trans.get_control_number_needed_fl(t205.c205_part_number_id) CTRL_NEEDED_FL "); // Added
        }                                                                                                                // control
                                                                                                                           // number
                                                                                                                           // needed
                                                                                                                           // flag
        sbQuery.append(", t205.C205_PRODUCT_MATERIAL PRODUCT_MATERIAL ");                                                  // for
        sbQuery.append(" FROM t205_part_number t205");                                                                     // parts.
        // sbQuery.append(" WHERE REGEXP_LIKE(c205_part_number_id,'");sbQuery.append(strPartNumReg);sbQuery.append("')");
        sbQuery.append(" WHERE C205_ACTIVE_FL = 'Y' ");
        sbQuery.append(strPartNumQuery);
        sbQuery.append(strPartCompFiltr);
        // sbQuery.append(" AND INSTR (c205_part_number_id, '.', 1, 2) = 0 "); // this is to filter
        // subcomponent. to be removed later
        // sbQuery.append(" ORDER BY c205_part_number_id ");
        log.debug("QUERY:" + sbQuery.toString());
        alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
      }
    }
    return alReturn;
  } // End of loadPartNum
  /**
   * loadTissuePartNum - This method to identify the parts is tissue or not PMT-41499
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   **/
  public String loadTissuePartNum(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strTissueParts = "";
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));    
    String strRegExp = GmCommonClass.parseNull((String) hmParam.get("REGEXP"));
    strRegExp = strRegExp.equals("") ? "LIKEPRE" : strRegExp;  
    HashMap hmReturn = new HashMap();   
    String strPartNumQuery = getPartNumQuery(strPartNum, strRegExp,"v205");   
    String strCompanyID = getCompId();    
      if (!strPartNumQuery.equals("")) {
        StringBuffer sbQuery = new StringBuffer();
        sbQuery.append(" SELECT RTRIM(XMLAGG(XMLELEMENT(e,v205.c205_part_number_id  || ',')).EXTRACT('//text()').getclobval(),',') TISSUEPARTS ");
        sbQuery.append(" FROM v205_parts_with_ext_vendor v205 WHERE v205.c1900_company_id = "+strCompanyID );        
        sbQuery.append(strPartNumQuery);      
        log.debug("QUERY:" + sbQuery.toString());
        hmReturn = GmCommonClass.parseNullHashMap(gmDBManager.querySingleRecord(sbQuery.toString()));
        log.debug("hmReturn:" + hmReturn);
        if(hmReturn.size() > 0){
        	strTissueParts = GmCommonClass.parseNull((String)hmReturn.get("TISSUEPARTS"));
        	strTissueParts = strTissueParts.equals("")?"":(","+strTissueParts+",");
        	log.debug("strTissueParts:" + strTissueParts);
        	} 
      }
    
    return strTissueParts;
  } // End of loadPartNum



  public String getPartNumQuery(String strPartNum, String strRegExp,String strPnumAlias) {
    StringBuffer sbQuery = new StringBuffer();
    String strPartNumReg = "^".concat(strPartNum);

    if (strPartNum.indexOf(",") != -1 || strPartNum.length() >= 7 || strPartNum.indexOf(".") == -1
        || strRegExp.equalsIgnoreCase("NO")) {
      strPartNum = GmCommonClass.getStringWithQuotes(strPartNum);
      sbQuery.append(" AND "+strPnumAlias+".c205_part_number_id IN ('");
      sbQuery.append(strPartNum);
      sbQuery.append("')");
    } else if (strPartNum.indexOf(".") != -1
        && strPartNum.substring(strPartNum.indexOf(".")).length() > 1) {	
      sbQuery.append("  AND REGEXP_LIKE("+strPnumAlias+".c205_part_number_id,REGEXP_REPLACE('");
      sbQuery.append(strPartNumReg);
      sbQuery.append("','[+]','\\+'))");

    	
    }
    log.debug(" Filter Query for pnum search " + sbQuery.toString());

    return sbQuery.toString();
  }


  /**
   * loadCartWithCapPrice - This method will
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadCartWithCapPrice(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PARTNUMS"));
    String strConsValue = GmCommonClass.parseNull((String) hmParam.get("CONSVALUE"));

    String strConsId = GmCommonClass.parseNull((String) hmParam.get("CONSID"));

    double dbConsValue = Double.parseDouble(strConsValue);

    ArrayList alList = null;
    ArrayList alTotalList = new ArrayList();
    ArrayList alLoop = new ArrayList();
    ArrayList alGroupPrice = new ArrayList();
    ArrayList alExcess = new ArrayList();
    ArrayList alAddOn = new ArrayList();
    ArrayList alClone = new ArrayList();

    HashMap hmLoop = new HashMap();
    HashMap hmTempLoop = new HashMap();
    HashMap hmFinal = new HashMap();
    HashMap hmExcess = new HashMap();
    HashMap hmAddOn = new HashMap();

    String strTokenString = "";
    int intSize = 0;
    int intAlSize = 0;
    int n = 0;
    int intOldQty = 0;

    String strArPNum = "";
    String strArQty = "";
    String strArGpId = "";
    String strArPDesc = "";

    String strPNum = "";
    String strGroupId = "";
    String strValue = "";
    String strType = "";
    String strNextGroupId = "";
    String strPrice = "";
    String strStock = "";
    String strConQty = "";
    String strExcQty = "";
    String strTPrice = "";
    String strLoanCogs = "";

    int intValue = 0;
    int intCnt = 0;
    int intQty = 0;
    int intGrpQty = 0;
    int intConQty = 0;
    int intExcQty = 0;
    int intRunQty = 0;
    int intRemConQty = 0;
    int intPartExcQty = 0;

    int m = 0;

    double dbGrpTotal = 0.0;
    double dbValue = 0.0;
    log.debug("-------");
    try {

      StringTokenizer strTok = new StringTokenizer(strInputStr, "|");
      while (strTok.hasMoreTokens()) {
        strTokenString = strTok.nextToken();
        String[] al = strTokenString.split(",");
        alList = new ArrayList(Arrays.asList(al));
        alTotalList.add(alList);
      }
      strPartNum = strPartNum.replaceAll(",", "','");
      alReturn = getCapitatedPricingData(strPartNum, strConsId);

      if (alReturn != null) {
        intSize = alReturn.size();
      }
      if (alTotalList != null) {
        intAlSize = alTotalList.size();
      }

      // Snippet_2 - To set the Group ID and Part Description to Parts in the Input String
      if (intSize > 0) {
        for (int i = 0; i < intSize; i++) {
          hmLoop = (HashMap) alReturn.get(i);
          strPNum = (String) hmLoop.get("PNUM");
          strGroupId = GmCommonClass.parseNull((String) hmLoop.get("GROUPID"));
          strArPDesc = GmCommonClass.parseNull((String) hmLoop.get("PDESC"));
          strStock = GmCommonClass.parseNull((String) hmLoop.get("STOCK"));
          strLoanCogs = GmCommonClass.parseNull((String) hmLoop.get("LOANCOGS"));

          for (int j = 0; j < intAlSize; j++) {
            alLoop = (ArrayList) alTotalList.get(j);
            strArPNum = (String) alLoop.get(0);
            if (strArPNum.equals(strPNum)) {
              alLoop.add(6, strGroupId);
              alLoop.add(7, strArPDesc);
              alLoop.add(8, strStock);
              alLoop.add(9, strLoanCogs);
            }
          }
        }
      }
      // End of Snippet_2--------------------------------------------------

      int k = 0;
      if (intSize > 0) {

        for (int i = 0; i < intSize; i++, k++) {
          hmLoop = (HashMap) alReturn.get(i);
          // log.debug(hmLoop);
          if (i < intSize - 1) {
            hmTempLoop = (HashMap) alReturn.get(i + 1);
            strNextGroupId = GmCommonClass.parseNull((String) hmTempLoop.get("GROUPID"));
          }

          strPNum = (String) hmLoop.get("PNUM");
          strGroupId = GmCommonClass.parseNull((String) hmLoop.get("GROUPID"));


          strValue = GmCommonClass.parseZero((String) hmLoop.get("VALUE"));
          strType = GmCommonClass.parseNull((String) hmLoop.get("TYPE"));
          strConQty = GmCommonClass.parseZero((String) hmLoop.get("CQTY"));

          intConQty = Integer.parseInt(strConQty);
          intValue = Integer.parseInt(strValue);

          dbValue = 0.0;

          for (int j = 0; j < intAlSize; j++) {
            alLoop = (ArrayList) alTotalList.get(j);
            strArGpId = (String) alLoop.get(6);
            if (strArGpId.equals(strGroupId)) {
              intQty = Integer.parseInt((String) alLoop.get(1));
              intGrpQty = intGrpQty + intQty;
            }
          }// End of inner FOR Loop

          // Check for Excess Parts in Groups
          if (intConQty < intGrpQty) {
            hmExcess = new HashMap();
            // log.debug("Excess for Group:"+strGroupId+" :By:"+(intGrpQty-intConQty));
            hmExcess.put("GROUPID", strGroupId);
            hmExcess.put("EXQTY", "" + (intGrpQty - intConQty));
            hmExcess.put("CONQTY", strConQty);
            alExcess.add(hmExcess);
            intGrpQty = intConQty;
          }

          if (strType.equals("30")) {
            dbValue = intValue;
          } else if (strType.equals("31")) {
            dbValue = dbConsValue * intValue / 100;
            dbValue = dbValue / intGrpQty;
          }

          dbGrpTotal = dbValue * intGrpQty;
          dbConsValue = dbConsValue - dbGrpTotal;

          hmFinal = new HashMap();
          hmFinal.put("GPID", strGroupId);
          hmFinal.put("GPPRICE", "" + dbValue);
          alGroupPrice.add(k, hmFinal);
          intGrpQty = 0;

          n = 0;
          for (m = i; m < intSize; m++) {
            n++;
            hmLoop = (HashMap) alReturn.get(m);
            strArGpId = GmCommonClass.parseNull((String) hmLoop.get("GROUPID"));
            if (!strArGpId.equals(strGroupId)) {
              m--;
              break;
            }
          }
          i = m;
        }// End of Outer FOR Loop

      }// End of intSize

      intSize = alGroupPrice.size();
      // Snippet_3 - To set the Price to the Parts that have common Group IDs
      if (intSize > 0) {
        for (int i = 0; i < intSize; i++) {
          hmLoop = (HashMap) alGroupPrice.get(i);
          strPrice = (String) hmLoop.get("GPPRICE");
          strGroupId = GmCommonClass.parseNull((String) hmLoop.get("GPID"));
          for (int j = 0; j < intAlSize; j++) {
            alLoop = (ArrayList) alTotalList.get(j);
            strArGpId = (String) alLoop.get(6);
            if (strArGpId.equals(strGroupId) && !strGroupId.equals("")) {
              alLoop.set(5, strPrice);
            }
          }
        }
      }
      // End of Snippet_3--------------------------------------------------

      log.debug("alExcess" + alExcess);
      // Snippet_4 - Code to Calculate Excess by Part
      intSize = alExcess.size();
      n = 0;
      if (intSize > 0) {
        for (int i = 0; i < intSize; i++) {
          hmLoop = (HashMap) alExcess.get(i);
          strGroupId = GmCommonClass.parseNull((String) hmLoop.get("GROUPID"));
          strConQty = GmCommonClass.parseZero((String) hmLoop.get("CONQTY"));
          strExcQty = GmCommonClass.parseZero((String) hmLoop.get("EXQTY"));

          intExcQty = Integer.parseInt(strExcQty);
          intConQty = Integer.parseInt(strConQty);
          n = 0;
          for (int j = 0; j < intAlSize; j++) {
            intRunQty = 0;
            intPartExcQty = 0;
            intOldQty = 0;
            alLoop = (ArrayList) alTotalList.get(j);
            strArGpId = (String) alLoop.get(6);
            if (strArGpId.equals(strGroupId)) {
              strPNum = (String) alLoop.get(0);
              intQty = Integer.parseInt((String) alLoop.get(1));
              n++;
              if (n == 1) {
                intRunQty = intQty - intConQty;
                intExcQty = intRunQty > 0 ? intExcQty - intRunQty : intExcQty;
                intPartExcQty = intRunQty > 0 ? intQty - intConQty : 0;
                intConQty = intConQty - intQty;
                intRemConQty = intConQty > 0 ? intConQty : 0;
              } else {
                intRunQty = intQty - intRemConQty;
                intExcQty = intRunQty > 0 ? intConQty - intRunQty : intConQty;
                intPartExcQty = intRunQty > 0 ? intQty - intRemConQty : 0;
                intConQty = intRemConQty - intQty > 0 ? intRemConQty - intQty : 0;
                intRemConQty = intConQty > 0 ? intConQty : 0;
              }
              intOldQty = intPartExcQty > 0 ? intQty - intPartExcQty : intQty;
              if (intOldQty > 0) {
                alLoop.set(1, "" + intOldQty);
                alClone = (ArrayList) alLoop.clone();
                hmAddOn = new HashMap();
                hmAddOn.put("EXTRAL", alClone);
                hmAddOn.put("EXQTY", "" + intPartExcQty);
                alAddOn.add(hmAddOn);
              } else if (intOldQty == 0) {
                alLoop.set(6, "");// setting GroupId to Null
                strTPrice = getNonConstructPrice("160", (String) alLoop.get(0));
                alLoop.set(5, GmCommonClass.parseZero(strTPrice)); // Setting Price for
                                                                   // non-Construct Items
              }


            }// End of Group ID
          }// End of FOR inner Loop
        }// End of FOR Outer Loop
      }
      // End of Snippet_4--------------------------------------------------
      log.debug(alAddOn);
      // Snippet_5 - Code to insert extra items
      intSize = alAddOn.size();
      hmLoop = new HashMap();
      String strTStr = "";


      if (intSize > 0) {
        for (int i = 0; i < intSize; i++) {
          hmLoop = (HashMap) alAddOn.get(i);
          strTStr = (String) hmLoop.get("EXQTY");
          if (!strTStr.equals("0")) {
            ArrayList alTLoop = new ArrayList();
            alTLoop = (ArrayList) hmLoop.get("EXTRAL");
            alTLoop.set(1, strTStr);// setting Part qty
            alTLoop.set(6, "");// setting GroupId to Null
            strTPrice = getNonConstructPrice("160", (String) alTLoop.get(0));
            alTLoop.set(5, GmCommonClass.parseZero(strTPrice)); // Setting Price for non-Construct
                                                                // Items
            alTotalList.add(intAlSize, alTLoop);
            intAlSize++;
          }
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
      // GmLogError.log("Exception in GmCommonCartBean:loadCartWithCapPrice","Exception is:"+e);
    }
    return alTotalList;
  } // End of loadCartWithCapPrice


  /**
   * getCapitatedPricingData - This method will
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList getCapitatedPricingData(String strPartNum, String strConsId) throws AppError {
    ArrayList alReturn = new ArrayList();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT a.c205_part_number_id PNUM , GRP.GROUPID, GRP.EXEC_SEQ, GRP.MFL, GRP.VALUE, GRP.TYPE, GRP.CQTY, GRP.CONID ");
      sbQuery
          .append(" ,NVL(get_qty_in_stock(a.c205_part_number_id),'0') STOCK, a.C205_PART_NUM_DESC PDESC ");
      sbQuery.append(" ,get_ac_cogs_value(a.c205_part_number_id,4906) LOANCOGS ");
      sbQuery.append(" FROM t205_part_number a, ");
      sbQuery
          .append(" (SELECT c.C205_PART_NUMBER_ID C205_PART_NUMBER_ID, c.C4010_GROUP_ID GROUPID, d.C742_EXEC_SEQ EXEC_SEQ, d.C742_MANDATORY_FL MFL, ");
      sbQuery
          .append(" d.C742_VALUE VALUE, d.C901_TYPE TYPE , d.C742_CRITICAL_QTY CQTY, e.C741_CONSTRUCT_ID CONID ");
      sbQuery
          .append(" FROM T4010_GROUP b, T4011_GROUP_DETAIL c , T742_CONSTRUCT_GROUP_MAPPING d, T741_CONSTRUCT e ");
      sbQuery.append(" where b.C4010_GROUP_ID = c.C4010_GROUP_ID ");
      sbQuery.append(" and b.C4010_GROUP_ID = d.C4010_GROUP_ID ");
      sbQuery.append(" and b.C901_TYPE = 40046 and d.C741_CONSTRUCT_ID = e.C741_CONSTRUCT_ID ");
      sbQuery.append(" and e.C741_CONSTRUCT_ID = ");
      sbQuery.append(strConsId);
      sbQuery.append(") GRP ");
      sbQuery.append(" WHERE a.C205_PART_NUMBER_ID IN ('");
      sbQuery.append(strPartNum);
      sbQuery.append("') and a.C205_PART_NUMBER_ID = GRP.C205_PART_NUMBER_ID (+) ");
      sbQuery.append(" order by GRP.EXEC_SEQ ");

      // log.debug(sbQuery.toString());

      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCommonCartBean:getCapitatedPricingData", "Exception is:" + e);
    }
    return alReturn;
  } // End of getCapitatedPricingData


  public String getNonConstructPrice(String strGpoId, String strPartNum) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    HashMap hmReturn = new HashMap();
    StringBuffer sbQuery = new StringBuffer();

    sbQuery.append(" SELECT ");
    sbQuery.append(" get_account_part_pricing(");
    sbQuery.append("0");
    sbQuery.append(",'");
    sbQuery.append(strPartNum);
    sbQuery.append("',");
    sbQuery.append(strGpoId);
    sbQuery.append(") PRICE ");
    sbQuery.append(" FROM DUAL ");

    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());

    String strReturn = (String) hmReturn.get("PRICE");

    return strReturn;
  }

  /*
   * To fetch the setdetails when initiating a loaner thru item/loaner initiate screen.
   */
  public ArrayList loadSetDetails(String strSetId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    ArrayList alReturn = new ArrayList();
    if (!strSetId.equals("")) {
      gmDBManager.setPrepareString("gm_pkg_cm_cart.gm_fch_setdetails", 2);

      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

      gmDBManager.setString(1, strSetId);
      gmDBManager.execute();
      alReturn =
          GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
              .getObject(2)));
      gmDBManager.close();
    }
    return alReturn;
  }

  /*
   * To fetch the valid tag id/etch id based on the refid, type and partnumber
   * 
   * @return String
   * 
   * @param Refid, Type, Pnum
   */
  public String fetchRefIdFlag(String strRefId, String strType, String strPnum) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strValidFl = "";
    log.debug("strRefId===" + strRefId);
    log.debug("strType===" + strType);
    log.debug("strPnum===" + strPnum);

    gmDBManager.setFunctionString("gm_pkg_cm_cart.get_refid_flag", 3);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);

    gmDBManager.setString(2, strRefId);
    gmDBManager.setString(3, strType);
    gmDBManager.setString(4, strPnum);
    gmDBManager.execute();
    strValidFl = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strValidFl;
  }

  /*
   * To fetch the Net Unit Price based on the Adjustment code selection
   * 
   * @Return String strNetUnitPrice
   * 
   * @param Account ID and Adjustment Code ID , Order ID and Part Number
   * 
   * @author: HReddi
   */
  public String fetchNetUnitPrice(String strAccountId, String strAdjCodeId, String strItemOrderID)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strNetUnitPrice = "";
    gmDBManager.setFunctionString("gm_pkg_sm_adj_rpt.get_adj_net_unit_price", 3);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strAccountId);
    gmDBManager.setString(3, strAdjCodeId);
    gmDBManager.setString(4, strItemOrderID);
    gmDBManager.execute();
    strNetUnitPrice = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strNetUnitPrice;
  }

  /*
   * To fetch the Unit Price based on the Adjustment code selection
   * 
   * @Return HashMap hmResult
   * 
   * @param Account ID and Part Number
   * 
   * @author: JReddy
   */
  public HashMap fetchAccPartListPrice(String strAcctID, String strPartNumber, String strAdjCodeId)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmResult = new HashMap();
    strPartNumber = strPartNumber.substring(0, strPartNumber.indexOf("@"));
    gmDBManager.setPrepareString("gm_pkg_sm_price_adj_rpt.gm_fch_acc_part_list_price", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strAcctID);
    gmDBManager.setString(2, strPartNumber);
    gmDBManager.setString(3, strAdjCodeId);
    gmDBManager.execute();
    hmResult =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(4)));
    gmDBManager.close();
    return hmResult;
  }

  /*
   * To fetch the part desc
   * 
   * @Return String strPartDesc
   * 
   * @param Part Number
   * 
   * @author: Aprasath
   */
  public String fetchPartDesc(String strPartNum) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPartDesc = "";
    gmDBManager.setFunctionString("gm_pkg_cm_cart.gm_fch_part_desc", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strPartNum);
    gmDBManager.execute();
    strPartDesc = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    return strPartDesc;
  }

  /*
   * To fetch all the parts details from a particular consignment/Tag ID
   * 
   * @Return ArrayList alReturn
   * 
   * @author: D Sandeep
   */
  public ArrayList fetchConsignDtls(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alReturn = new ArrayList();
    String strConsignId = GmCommonClass.parseNull((String) hmParam.get("CONSINID"));
    gmDBManager.setPrepareString("gm_pkg_cm_cart.gm_fch_part_from_consign", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strConsignId);
    gmDBManager.execute();
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alReturn;
  }
  
  /**
   * loadTissueSetDetails - This method to identify the set is tissue or not PMT-400240
   * 
   * @param Set ID 
   * @return String
   * @exception AppError
   **/
  public String loadTissueSetDetails(String strSetId) throws AppError {
	  
	  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	  String strTissueSetId = "";
	  HashMap hmReturn = new HashMap();
      StringBuffer sbQuery = new StringBuffer();
      
      sbQuery.append(" SELECT c207_set_id tissuesetid");
      sbQuery.append(" FROM t207_set_master");
      sbQuery.append(" WHERE c207_set_id = '");
      sbQuery.append(strSetId);
      sbQuery.append("' AND c207_tissue_fl IS NOT NULL ");
      sbQuery.append("  AND c207_void_fl IS NULL");
     
      hmReturn = GmCommonClass.parseNullHashMap(gmDBManager.querySingleRecord(sbQuery.toString()));
      log.debug("hmReturn:" + hmReturn);
      if(hmReturn.size() > 0){
    	  strTissueSetId = GmCommonClass.parseNull((String)hmReturn.get("TISSUESETID"));
    	  strTissueSetId = strTissueSetId.equals("")?"":(","+strTissueSetId+",");
      	log.debug("strTissueSetId:" + strTissueSetId);
      	} 
      return strTissueSetId;
  }
  /**PC-4792 
	 * loadPartNumFromQNWarehouse - This method used to load part Number details from Quarantine inventory  
	 * @param hmParam
	 * @return alReturn
	 */
	public ArrayList loadPartNumFromQNWarehouse(HashMap hmParam) {
		ArrayList alReturn = new ArrayList();
		String strPartCompFiltr = "";
		String strFromType = "";
		String strCountryCode = "";
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACC"));
		String strRepId = GmCommonClass.parseNull((String) hmParam.get("REP"));
		String strGpoId = GmCommonClass.parseNull((String) hmParam.get("GPOID"));
		String strRegExp = GmCommonClass.parseNull((String) hmParam.get("REGEXP"));
		String strFromPg = GmCommonClass.parseNull((String) hmParam.get("FROMPG"));
		strRegExp = strRegExp.equals("") ? "LIKEPRE" : strRegExp;

		GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());
		String strPartNumQuery = getPartNumQuery(strPartNum, strRegExp,"t205");
	    String strFilterPartNum = strPartNum.replace(',', '|');
	    String strPartMapType = GmCommonClass.getRuleValue("RFS", "PART_MAP_TYPE");
	    strPartCompFiltr =
	        gmPartSearchBean.getPartCompanyFilter(strFilterPartNum, "t205", strPartMapType);
	    String strCompanyID = getCompId();
		String strCheck = "Y";
		

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		//GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT	t205.c205_part_number_id PARTNUM, t205.c205_part_num_desc PDESC");
		sbQuery.append(
				" ,   NVL(get_qty_in_quarantine(t205.c205_part_number_id),'0') QTY ,'QN' WHTYPE, '90813' WHINV ,'1' REQQTY");
		sbQuery.append(
				" ,   GET_PART_PRICE(t205.c205_part_number_id,'C') CONSPRICE , DECODE('Y','Y','0',GET_PART_PRICE(t205.c205_part_number_id,'L')) LPRICE ");
		sbQuery.append(
				" ,   TO_CHAR(CURRENT_DATE,get_compdtfmt_frm_cntx ()) CURRDATE ,  DECODE(gm_pkg_pd_partnumber.get_rfs_info(t205.c205_part_number_id),NULL,'N','Y') RFL ");
		sbQuery.append(
				" ,   gm_pkg_op_do_process_trans.get_control_number_needed_fl(t205.c205_part_number_id) CTRL_NEEDED_FL ");
		sbQuery.append(" FROM t205_part_number t205 ");
		sbQuery.append(" WHERE C205_ACTIVE_FL          = 'Y' ");
		sbQuery.append(strPartNumQuery);
        sbQuery.append(strPartCompFiltr);
		
		/*
		 * sbQuery.append(" AND t205.c205_part_number_id  IN ('");
		 * sbQuery.append(strPartNum);
		 * sbQuery.append("') AND t205.C205_PART_NUMBER_ID IN ( "); sbQuery.
		 * append(" SELECT T2023.C205_PART_NUMBER_ID  FROM T2023_PART_COMPANY_MAPPING T2023 "
		 * ); sbQuery.append(" WHERE T2023.C205_PART_NUMBER_ID IN ('");
		 * sbQuery.append(strPartNum); sbQuery.append("')"); sbQuery.
		 * append(" AND T2023.C2023_VOID_FL  IS NULL  AND T2023.C1900_COMPANY_ID  =" +
		 * strCompanyID);
		 * sbQuery.append(" AND T2023.C901_PART_MAPPING_TYPE IN ( 105360,105361 )");
		 * sbQuery.append(")");
		 */

		log.debug("QUERY:" + sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		log.debug("alReturn:" + alReturn);
		return alReturn;

	}
	/**PC-4792 
	 * loadPartNumFromFGWarehouse - This method used to load part Number details from Finished Goods inventory  
	 * @param hmParam
	 * @return alReturn
	 */
	public ArrayList loadPartNumFromFGWarehouse(HashMap hmParam) {
		ArrayList alReturn = new ArrayList();
		String strPartCompFiltr = "";
		String strFromType = "";
		String strCountryCode = "";
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACC"));
		String strRepId = GmCommonClass.parseNull((String) hmParam.get("REP"));
		String strGpoId = GmCommonClass.parseNull((String) hmParam.get("GPOID"));
		String strRegExp = GmCommonClass.parseNull((String) hmParam.get("REGEXP"));
		String strFromPg = GmCommonClass.parseNull((String) hmParam.get("FROMPG"));
		strRegExp = strRegExp.equals("") ? "LIKEPRE" : strRegExp;

		GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());
		String strPartNumQuery = getPartNumQuery(strPartNum, strRegExp,"t205");
	    String strFilterPartNum = strPartNum.replace(',', '|');
	    String strPartMapType = GmCommonClass.getRuleValue("RFS", "PART_MAP_TYPE");
	    strPartCompFiltr =
	        gmPartSearchBean.getPartCompanyFilter(strFilterPartNum, "t205", strPartMapType);
	    String strCompanyID = getCompId();
		String strCheck = "Y";
		

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		//GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT	t205.c205_part_number_id PARTNUM, t205.c205_part_num_desc PDESC");
		sbQuery.append(" ,   NVL(get_qty_in_stock(t205.c205_part_number_id),'0')  QTY ,'FG' WHTYPE, '90800' WHINV ,'1' REQQTY ");
		sbQuery.append(
				" ,   GET_PART_PRICE(t205.c205_part_number_id,'C') CONSPRICE , DECODE('Y','Y','0',GET_PART_PRICE(t205.c205_part_number_id,'L')) LPRICE ");
		sbQuery.append(
				" ,   TO_CHAR(CURRENT_DATE,get_compdtfmt_frm_cntx ()) CURRDATE ,  DECODE(gm_pkg_pd_partnumber.get_rfs_info(t205.c205_part_number_id),NULL,'N','Y') RFL ");
		sbQuery.append(
				" ,   gm_pkg_op_do_process_trans.get_control_number_needed_fl(t205.c205_part_number_id) CTRL_NEEDED_FL ");
		sbQuery.append(" FROM t205_part_number t205 ");
		sbQuery.append(" WHERE C205_ACTIVE_FL          = 'Y' ");
		sbQuery.append(strPartNumQuery);
        sbQuery.append(strPartCompFiltr);
		
		/*
		 * sbQuery.append(" AND t205.c205_part_number_id  IN ('");
		 * sbQuery.append(strPartNum);
		 * sbQuery.append("') AND t205.C205_PART_NUMBER_ID IN ( "); sbQuery.
		 * append(" SELECT T2023.C205_PART_NUMBER_ID  FROM T2023_PART_COMPANY_MAPPING T2023 "
		 * ); sbQuery.append(" WHERE T2023.C205_PART_NUMBER_ID IN ('");
		 * sbQuery.append(strPartNum); sbQuery.append("')"); sbQuery.
		 * append(" AND T2023.C2023_VOID_FL  IS NULL  AND T2023.C1900_COMPANY_ID  =" +
		 * strCompanyID);
		 * sbQuery.append(" AND T2023.C901_PART_MAPPING_TYPE IN ( 105360,105361 )");
		 * sbQuery.append(")");
		 */

		log.debug("QUERY:" + sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		log.debug("alReturn:" + alReturn);
		return alReturn;

	}
	/**PC-4792 
	 * loadPartNumFromBLWarehouse - This method used to load part Number details from Bulk inventory  
	 * @param hmParam
	 * @return alReturn
	 */
	public ArrayList loadPartNumFromBLWarehouse(HashMap hmParam) {
		ArrayList alReturn = new ArrayList();
		String strPartCompFiltr = "";
		String strFromType = "";
		String strCountryCode = "";
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACC"));
		String strRepId = GmCommonClass.parseNull((String) hmParam.get("REP"));
		String strGpoId = GmCommonClass.parseNull((String) hmParam.get("GPOID"));
		String strRegExp = GmCommonClass.parseNull((String) hmParam.get("REGEXP"));
		String strFromPg = GmCommonClass.parseNull((String) hmParam.get("FROMPG"));
		strRegExp = strRegExp.equals("") ? "LIKEPRE" : strRegExp;
		GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());
		String strPartNumQuery = getPartNumQuery(strPartNum, strRegExp,"t205");
	    String strFilterPartNum = strPartNum.replace(',', '|');
	    String strPartMapType = GmCommonClass.getRuleValue("RFS", "PART_MAP_TYPE");
	    strPartCompFiltr =
	        gmPartSearchBean.getPartCompanyFilter(strFilterPartNum, "t205", strPartMapType);
	    String strCompanyID = getCompId();
		String strCheck = "Y";
		

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT	t205.c205_part_number_id PARTNUM, t205.c205_part_num_desc PDESC");
		sbQuery.append(" ,   NVL(get_qty_in_bulk(t205.c205_part_number_id),'0')  QTY ,'BL' WHTYPE, '90814' WHINV ,'1' REQQTY");
		sbQuery.append(
				" ,   GET_PART_PRICE(t205.c205_part_number_id,'C') CONSPRICE , DECODE('Y','Y','0',GET_PART_PRICE(t205.c205_part_number_id,'L')) LPRICE ");
		sbQuery.append(
				" ,   TO_CHAR(CURRENT_DATE,get_compdtfmt_frm_cntx ()) CURRDATE ,  DECODE(gm_pkg_pd_partnumber.get_rfs_info(t205.c205_part_number_id),NULL,'N','Y') RFL ");
		sbQuery.append(
				" ,   gm_pkg_op_do_process_trans.get_control_number_needed_fl(t205.c205_part_number_id) CTRL_NEEDED_FL ");
		sbQuery.append(" FROM t205_part_number t205 ");
		sbQuery.append(" WHERE C205_ACTIVE_FL          = 'Y' ");
		sbQuery.append(strPartNumQuery);
        sbQuery.append(strPartCompFiltr);
		
		/*
		 * sbQuery.append(" AND t205.c205_part_number_id  IN ('");
		 * sbQuery.append(strPartNum);
		 * sbQuery.append("') AND t205.C205_PART_NUMBER_ID IN ( "); sbQuery.
		 * append(" SELECT T2023.C205_PART_NUMBER_ID  FROM T2023_PART_COMPANY_MAPPING T2023 "
		 * ); sbQuery.append(" WHERE T2023.C205_PART_NUMBER_ID IN ('");
		 * sbQuery.append(strPartNum); sbQuery.append("')"); sbQuery.
		 * append(" AND T2023.C2023_VOID_FL  IS NULL  AND T2023.C1900_COMPANY_ID  =" +
		 * strCompanyID);
		 * sbQuery.append(" AND T2023.C901_PART_MAPPING_TYPE IN ( 105360,105361 )");
		 * sbQuery.append(")");
		 */

		log.debug("QUERY:" + sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		log.debug("alReturn:" + alReturn);
		return alReturn;
	}
	/**PC-4792 
	 * loadPartNumFromIHWarehouse - This method used to load part Number details from Inhouse inventory  
	 * @param hmParam
	 * @return alReturn
	 */
	public ArrayList loadPartNumFromIHWarehouse(HashMap hmParam) {
		GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());
		ArrayList alReturn = new ArrayList();
		String strPartCompFiltr = "";
		String strFromType = "";
		String strCountryCode = "";
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACC"));
		String strRepId = GmCommonClass.parseNull((String) hmParam.get("REP"));
		String strGpoId = GmCommonClass.parseNull((String) hmParam.get("GPOID"));
		String strRegExp = GmCommonClass.parseNull((String) hmParam.get("REGEXP"));
		String strFromPg = GmCommonClass.parseNull((String) hmParam.get("FROMPG"));
		strRegExp = strRegExp.equals("") ? "LIKEPRE" : strRegExp;

		String strPartNumQuery = getPartNumQuery(strPartNum, strRegExp,"t205");
	    String strFilterPartNum = strPartNum.replace(',', '|');
	    String strPartMapType = GmCommonClass.getRuleValue("RFS", "PART_MAP_TYPE");
	    strPartCompFiltr =
	        gmPartSearchBean.getPartCompanyFilter(strFilterPartNum, "t205", strPartMapType);
	    String strCompanyID = getCompId();
		String strCheck = "Y";
		

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT	t205.c205_part_number_id PARTNUM, t205.c205_part_num_desc PDESC");
		sbQuery.append(" ,   NVL(get_qty_in_inhouse(t205.c205_part_number_id),'0')  QTY ,'IH' WHTYPE, '90816' WHINV ,'1' REQQTY ");
		sbQuery.append(
				" ,   GET_PART_PRICE(t205.c205_part_number_id,'C') CONSPRICE , DECODE('Y','Y','0',GET_PART_PRICE(t205.c205_part_number_id,'L')) LPRICE ");
		sbQuery.append(
				" ,   TO_CHAR(CURRENT_DATE,get_compdtfmt_frm_cntx ()) CURRDATE ,  DECODE(gm_pkg_pd_partnumber.get_rfs_info(t205.c205_part_number_id),NULL,'N','Y') RFL ");
		sbQuery.append(
				" ,   gm_pkg_op_do_process_trans.get_control_number_needed_fl(t205.c205_part_number_id) CTRL_NEEDED_FL ");
		sbQuery.append(" FROM t205_part_number t205 ");
		sbQuery.append(" WHERE C205_ACTIVE_FL          = 'Y' ");
		sbQuery.append(strPartNumQuery);
        sbQuery.append(strPartCompFiltr);
		/*
		 * sbQuery.append(" AND t205.c205_part_number_id  IN ('");
		 * sbQuery.append(strPartNum);
		 * sbQuery.append("') AND t205.C205_PART_NUMBER_ID IN ( "); sbQuery.
		 * append(" SELECT T2023.C205_PART_NUMBER_ID  FROM T2023_PART_COMPANY_MAPPING T2023 "
		 * ); sbQuery.append(" WHERE T2023.C205_PART_NUMBER_ID IN ('");
		 * sbQuery.append(strPartNum); sbQuery.append("')"); sbQuery.
		 * append(" AND T2023.C2023_VOID_FL  IS NULL  AND T2023.C1900_COMPANY_ID  =" +
		 * strCompanyID);
		 * sbQuery.append(" AND T2023.C901_PART_MAPPING_TYPE IN ( 105360,105361 )");
		 * sbQuery.append(")");
		 */

		log.debug("QUERY:" + sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		log.debug("alReturn:" + alReturn);
		return alReturn;
	}

	/**PC-4792 
	 * loadPartNumFromRWWarehouse - This method used to load part Number details from Restricted Warehouse inventory  
	 * @param hmParam
	 * @return alReturn
	 */
	public ArrayList loadPartNumFromRWWarehouse(HashMap hmParam) {
		
		GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());
		
		ArrayList alReturn = new ArrayList();
		String strPartCompFiltr = "";
		String strFromType = "";
		String strCountryCode = "";
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACC"));
		String strRepId = GmCommonClass.parseNull((String) hmParam.get("REP"));
		String strGpoId = GmCommonClass.parseNull((String) hmParam.get("GPOID"));
		String strRegExp = GmCommonClass.parseNull((String) hmParam.get("REGEXP"));
		String strFromPg = GmCommonClass.parseNull((String) hmParam.get("FROMPG"));
		strRegExp = strRegExp.equals("") ? "LIKEPRE" : strRegExp;
		String strPartNumQuery = getPartNumQuery(strPartNum, strRegExp,"t205");
	    String strFilterPartNum = strPartNum.replace(',', '|');
	    String strPartMapType = GmCommonClass.getRuleValue("RFS", "PART_MAP_TYPE");
	    strPartCompFiltr =
	        gmPartSearchBean.getPartCompanyFilter(strFilterPartNum, "t205", strPartMapType);
	    String strCompanyID = getCompId();
		String strCheck = "Y";
	

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT	t205.c205_part_number_id PARTNUM, t205.c205_part_num_desc PDESC");
		sbQuery.append(" ,    GET_QTY_IN_INV(t205.c205_part_number_id,'56001')  QTY ,'RW' WHTYPE, '56001' WHINV ,'1' REQQTY");
		sbQuery.append(
				" ,   GET_PART_PRICE(t205.c205_part_number_id,'C') CONSPRICE , DECODE('Y','Y','0',GET_PART_PRICE(t205.c205_part_number_id,'L')) LPRICE ");
		sbQuery.append(
				" ,   TO_CHAR(CURRENT_DATE,get_compdtfmt_frm_cntx ()) CURRDATE ,  DECODE(gm_pkg_pd_partnumber.get_rfs_info(t205.c205_part_number_id),NULL,'N','Y') RFL ");
		sbQuery.append(
				" ,   gm_pkg_op_do_process_trans.get_control_number_needed_fl(t205.c205_part_number_id) CTRL_NEEDED_FL ");
		sbQuery.append(" FROM t205_part_number t205 ");
		sbQuery.append(" WHERE C205_ACTIVE_FL          = 'Y' ");
		sbQuery.append(strPartNumQuery);
        sbQuery.append(strPartCompFiltr);
		/*
		 * sbQuery.append(" AND t205.c205_part_number_id  IN ('");
		 * sbQuery.append(strPartNum);
		 * sbQuery.append("') AND t205.C205_PART_NUMBER_ID IN ( "); sbQuery.
		 * append(" SELECT T2023.C205_PART_NUMBER_ID  FROM T2023_PART_COMPANY_MAPPING T2023 "
		 * ); sbQuery.append(" WHERE T2023.C205_PART_NUMBER_ID IN ('");
		 * sbQuery.append(strPartNum); sbQuery.append("')"); sbQuery.
		 * append(" AND T2023.C2023_VOID_FL  IS NULL  AND T2023.C1900_COMPANY_ID  =" +
		 * strCompanyID);
		 * sbQuery.append(" AND T2023.C901_PART_MAPPING_TYPE IN ( 105360,105361 )");
		 * sbQuery.append(")");
		 */

		log.debug("QUERY:" + sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		log.debug("alReturn:" + alReturn);
		return alReturn;
	}

	/**PC-4792 
	 * loadPartNumFromPNWarehouse - This method used to load part Number details from Repacking inventory  
	 * @param hmParam
	 * @return alReturn
	 */
	public ArrayList loadPartNumFromPNWarehouse(HashMap hmParam) {
		// TODO Auto-generated method stub
		ArrayList alReturn = new ArrayList();
		GmPartNumSearchBean gmPartSearchBean = new GmPartNumSearchBean(getGmDataStoreVO());
		String strPartCompFiltr = "";
		String strFromType = "";
		String strCountryCode = "";
		String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
		String strAccId = GmCommonClass.parseNull((String) hmParam.get("ACC"));
		String strRepId = GmCommonClass.parseNull((String) hmParam.get("REP"));
		String strGpoId = GmCommonClass.parseNull((String) hmParam.get("GPOID"));
		String strRegExp = GmCommonClass.parseNull((String) hmParam.get("REGEXP"));
		String strFromPg = GmCommonClass.parseNull((String) hmParam.get("FROMPG"));
		strRegExp = strRegExp.equals("") ? "LIKEPRE" : strRegExp;
		String strPartNumQuery = getPartNumQuery(strPartNum, strRegExp,"t205");
	    String strFilterPartNum = strPartNum.replace(',', '|');
	    String strPartMapType = GmCommonClass.getRuleValue("RFS", "PART_MAP_TYPE");
	    strPartCompFiltr =
	        gmPartSearchBean.getPartCompanyFilter(strFilterPartNum, "t205", strPartMapType);
	    String strCompanyID = getCompId();
		String strCheck = "Y";

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT	t205.c205_part_number_id PARTNUM, t205.c205_part_num_desc PDESC");
		sbQuery.append(
				" ,   NVL(get_qty_in_packaging(t205.c205_part_number_id),'0')  QTY ,'PN' WHTYPE, '90815' WHINV ,'1' REQQTY");
		sbQuery.append(
				" ,   GET_PART_PRICE(t205.c205_part_number_id,'C') CONSPRICE , DECODE('"+strCheck+"','Y','0',GET_PART_PRICE(t205.c205_part_number_id,'L')) LPRICE ");
		sbQuery.append(
				" ,   TO_CHAR(CURRENT_DATE,get_compdtfmt_frm_cntx ()) CURRDATE ,  DECODE(gm_pkg_pd_partnumber.get_rfs_info(t205.c205_part_number_id),NULL,'N','Y') RFL ");
		sbQuery.append(
				" ,   gm_pkg_op_do_process_trans.get_control_number_needed_fl(t205.c205_part_number_id) CTRL_NEEDED_FL ");
		sbQuery.append(" FROM t205_part_number t205 ");
		sbQuery.append(" WHERE C205_ACTIVE_FL          = 'Y' ");
		sbQuery.append(strPartNumQuery);
        sbQuery.append(strPartCompFiltr);
		
		
		/*
		 * sbQuery.append(" AND t205.c205_part_number_id  IN ('");
		 * sbQuery.append(strPartNum);
		 * sbQuery.append("') AND t205.C205_PART_NUMBER_ID IN ( "); sbQuery.
		 * append(" SELECT T2023.C205_PART_NUMBER_ID  FROM T2023_PART_COMPANY_MAPPING T2023 "
		 * ); sbQuery.append(" WHERE T2023.C205_PART_NUMBER_ID IN ('");
		 * sbQuery.append(strPartNum); sbQuery.append("')"); sbQuery.
		 * append(" AND T2023.C2023_VOID_FL  IS NULL  AND T2023.C1900_COMPANY_ID  =" +
		 * strCompanyID);
		 * sbQuery.append(" AND T2023.C901_PART_MAPPING_TYPE IN ( 105360,105361 )");
		 * sbQuery.append(")");
		 */

		log.debug("QUERY:" + sbQuery.toString());
		alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		log.debug("alReturn:" + alReturn);
		return alReturn;
	}
}// end of class GmCommonCartBean
