/*****************************************************************************
 * File : GmCommonCartServlet Version : 1.0
 ******************************************************************************/
package com.globus.common.cart.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.cart.beans.GmCommonCartBean;
import com.globus.common.servlets.GmServlet;
import com.globus.operations.beans.GmLoanerBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.prodmgmnt.beans.GmProjectBean;

public class GmCommonCartServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCartPath = GmCommonClass.getString("GMCART");
    String strDispatchTo = strCartPath.concat("/GmCommonCart.jsp");
    String strTagColumn = GmCommonClass.parseNull(request.getParameter("hTagColumn"));
    String strCapFlColumn = GmCommonClass.parseNull(request.getParameter("hCapFlColumn"));
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    ArrayList alReturn = new ArrayList();

    ArrayList alWareHouse = new ArrayList();

    GmCommonCartBean gmCart = new GmCommonCartBean(getGmDataStoreVO());
    GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
    String strConstructId = "";
    String strInputStr = "";
    String strPartNumStr = "";
    String strConsValue = "";
    String strGpoId = "";
    String strScreen = "";
    String strCompanyId = getGmDataStoreVO().getCmpid();
    strScreen = GmCommonClass.parseZero(request.getParameter("hScreen"));
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmLoanerBean gmLoanerBean = new GmLoanerBean(getGmDataStoreVO());
    GmInvLocationBean gmInvLocationBean = new GmInvLocationBean(getGmDataStoreVO());
    try {

      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      log.debug(" Action is " + strAction);

      if (strAction.equals("Load")) {
        hmParam.put("PID", "00");
        hmReturn = gmCart.loadOrderCartLists(hmParam);
        String strCtrlTabIndex = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany("CONTROL_TAB_INDEX", "SURG_DT_INFO", strCompanyId));
	    request.setAttribute("CTRLTABINDEX", strCtrlTabIndex);
        request.setAttribute("hmReturn", hmReturn);
      } else if (strAction.equals("UpdatePrice")) {
        strGpoId = GmCommonClass.parseZero(request.getParameter("hGpoId"));
        hmParam.put("PID", strGpoId);
        log.debug(hmParam);
        hmReturn = gmCart.loadOrderCartLists(hmParam);
        request.setAttribute("hmReturn", hmReturn);

        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
        strPartNumStr = GmCommonClass.parseNull(request.getParameter("hPartNumStr"));
        strConstructId = GmCommonClass.parseNull(request.getParameter("Cbo_Construct"));
        strConsValue = GmCommonClass.parseZero(request.getParameter("hConsValue"));

        hmParam.put("INPUTSTR", strInputStr);
        hmParam.put("PARTNUMS", strPartNumStr);
        hmParam.put("CONSID", strConstructId);
        hmParam.put("CONSVALUE", strConsValue);

        alReturn = gmCart.loadCartWithCapPrice(hmParam);
        request.setAttribute("alReturn", alReturn);
        request.setAttribute("hConstructId", strConstructId);
        request.setAttribute("hGpoId", strGpoId);
        request.setAttribute("hConsValue", strConsValue);
      } else if (strAction.equals("ReqCart")) {
        // commented since wants to get active set list for loaners
        // alReturn = gmProj.loadSetMap("1601");
        alWareHouse = GmCommonClass.parseNullArrayList(gmInvLocationBean.fetchWareHouse(""));

        request.setAttribute("alWareHouse", alWareHouse);

        if (strScreen.equals("Account")) {
          strDispatchTo = strCartPath.concat("/GmAccountRequestCart.jsp");
        } else if (strScreen.equals("gmLoanerRequestInitiate") || strScreen.equals("gmRequestInitiate")){
            alReturn = gmLoanerBean.getLoanerRequestActiveSets();
            request.setAttribute("alSets", alReturn);
            strDispatchTo = strCartPath.concat("/GmRequestCart.jsp");
          } else if (strScreen.equals("gmStockInitiate")){
          strDispatchTo = strCartPath.concat("/GmStockRequestCart.jsp");
        }else {
          alReturn = gmCommonBean.getActiveLoanerSets();
          request.setAttribute("alSets", alReturn);
          strDispatchTo = strCartPath.concat("/GmRequestCart.jsp");
        }
      }
      if (strScreen.equals("QUOTE")) {
        strDispatchTo = strCartPath.concat("/GmQuoteOrderCart.jsp");
      }
      request.setAttribute("TagColumn", strTagColumn);
      request.setAttribute("CapFlColumn", strCapFlColumn);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmCommonCartServlet
