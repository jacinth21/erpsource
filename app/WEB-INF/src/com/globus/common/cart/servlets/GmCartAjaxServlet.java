/*****************************************************************************
 * File : GmCartAjaxServlet Version : 1.0
 ******************************************************************************/
package com.globus.common.cart.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmXMLParserUtility;
import com.globus.common.cart.beans.GmCommonCartBean;
import com.globus.common.servlets.GmServlet;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDORptBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.operations.requests.beans.GmInHouseTransactionBean;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.party.beans.GmAddressBean;
import com.globus.party.beans.GmPartyInfoBean;
import com.globus.quality.beans.GmSetBundleMappingBean;

public class GmCartAjaxServlet extends GmServlet {

  private ServletContext context;
  private final HashMap hmCart = new HashMap();
  private HashMap hmModeValues = new HashMap();
  private ArrayList alCart = new ArrayList();
  private final ArrayList alModeValues = new ArrayList();


  String strComnPath = GmCommonClass.getString("GMCOMMON");

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    this.context = config.getServletContext();
  }

  public String createAjaxString(ArrayList alList, String strId, String strName) {
    HashMap hmTemp = new HashMap();

    StringBuffer sbVal = new StringBuffer();
    String strID = "";
    String strNAME = "";

    for (int i = 0; i < alList.size(); i++) {
      hmTemp = (HashMap) alList.get(i);
      strID = GmCommonClass.parseNull((String) hmTemp.get(strId));
      strNAME = GmCommonClass.parseNull((String) hmTemp.get(strName));

      sbVal.append("obj.options[obj.options.length] = new Option('");
      sbVal.append(strNAME.replaceAll("'", ""));
      sbVal.append("','");
      sbVal.append(strID);
      sbVal.append("');\n");
    }
    log.debug("ajax string " + sbVal.toString());
    return sbVal.toString();
  }

  /*
   * public String createAjaxString (ArrayList alList,String strId, String strName){ HashMap hmTemp
   * = new HashMap(); StringBuffer sbVal = new StringBuffer(); String strID = ""; String strNAME =
   * "";
   * 
   * sbVal.append("obj.options[obj.options.length] = new Option('");
   * sbVal.append("[Choose One]','0"); sbVal.append("');\n"); for (int i = 0; i < alList.size();
   * i++){ hmTemp = (HashMap)alList.get(i); strID =
   * GmCommonClass.parseNull((String)hmTemp.get(strId)); strNAME =
   * GmCommonClass.parseNull((String)hmTemp.get(strName));
   * 
   * sbVal.append("obj.options[obj.options.length] = new Option('");
   * sbVal.append(strNAME.replaceAll("'", "")); sbVal.append("','"); sbVal.append(strID);
   * sbVal.append("');\n"); //System.out.println("sbVal "+sbVal); }
   * 
   * return sbVal.toString(); }
   */
  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
    String strSource = GmCommonClass.parseNull(request.getParameter("hSource"));
    String[] strSourceArr;
    strSourceArr = strSource.split(",");
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    String strXMLString = "";
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    HashMap hmParam;
    GmAddressBean gmAddressBean = new GmAddressBean(getGmDataStoreVO());
    GmPartyInfoBean gmPartyInfoBean = new GmPartyInfoBean(getGmDataStoreVO());
    String strRepId = GmCommonClass.parseNull(request.getParameter("hRepId"));
    String strEmpId = GmCommonClass.parseNull(request.getParameter("hEmpId"));
    String strInactiveFl = GmCommonClass.parseNull(request.getParameter("hInActiveFl"));
    String strAccountType = "";
    String strPartyId = GmCommonClass.parseNull(request.getParameter("hPartyId"));



    hmParam = new HashMap();
    GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();

    try {
      if (strAction.equalsIgnoreCase("fetchAdd")) {
        ArrayList alAddress = new ArrayList();
        alAddress = gmAddressBean.fetchPartyAddress(strRepId, strInactiveFl);
        log.debug("alAddress " + alAddress);
        // strXMLString = gmXMLParserUtility.createXMLString(alAddress);
        strXMLString = createAjaxString(alAddress, "ID", "NAME");
      } else if (strAction.equalsIgnoreCase("fetchShipAdd")) { // This is for shipping account
        ArrayList alAddress = new ArrayList();
        alAddress = gmAddressBean.fetchShipAddress(strPartyId, strInactiveFl);
        strXMLString = createAjaxString(alAddress, "ID", "NAME");
      } else if (strAction.equalsIgnoreCase("fetchEmpAdd")) {
        ArrayList alAddress = new ArrayList();
        alAddress = gmAddressBean.fetchEmpPartyAddress(strEmpId, strInactiveFl);
        log.debug("alAddress " + alAddress);
        // strXMLString = gmXMLParserUtility.createXMLString(alAddress);
        strXMLString = createAjaxString(alAddress, "ID", "NAME");
      } else if (strAction.equalsIgnoreCase("validateRepAdd")) {
        String strAddressId = GmCommonClass.parseNull(request.getParameter("hAddrId"));
        strXMLString = gmAddressBean.validateRepAddress(strAddressId);
      } else {
        ArrayList alAccount = new ArrayList();
        // 50256: Inter-Company Transfer; 50255: US customers
        // Need to get account type from rules table
        // Locale
        String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
        GmResourceBundleBean gmResourceBundleBean =
            GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
        strSource = strSource.equals("") ? "50255" : strSource;
        strAccountType =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty("ACCOUNT_ACTIVITY."
                + strSource));
        if (strSource.equals("50256")) {// ICT
          hmParam.put("DISTTYP", "70103");
          alAccount = gmCustomerBean.getDistributorList(hmParam);
        } else if (strSource.equals("26240213")) {// 26240213 = Company Dealer
          alAccount = gmPartyInfoBean.fetchPartyNameList("26230725", "");// 26230725=Dealer
                                                                         // Type
        } else {
          alAccount = gmSales.reportAccountByType(strAccountType);
        }
        strXMLString = gmXMLParserUtility.createXMLString(alAccount);
        // log.debug(" XML String is " + strXMLString);

        response.setContentType("text/xml");
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Cache-Control", "no-cache");
      }
    } catch (Exception exp) {
      exp.printStackTrace();
    }

    PrintWriter writer = response.getWriter();
    writer.println(strXMLString);
    writer.close();

  }

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    instantiate(request, response);
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    HashMap hmapResult = new HashMap();
    String strXMLString = "";
    String partDesc = "";
    StringBuffer sbVal = new StringBuffer();
    String strRefId = "";
    String strOrdType = "";
    String strValidFl = "";
    String strPnum = "";
    String strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
    // PMT-4359 -- Getting the Rep ID, Shipto type and Address ID from the Javascript
    String strRepAccid = GmCommonClass.parseNull(request.getParameter("hRepAccID"));
    String strRepAddID = GmCommonClass.parseNull(request.getParameter("haddrId"));
    String strCarrier = GmCommonClass.parseNull(request.getParameter("hCarrier"));
    String strAcctID = GmCommonClass.parseNull(request.getParameter("hAcctID"));
    String strAdjCodeId = GmCommonClass.parseNull(request.getParameter("hAdjCOdeID"));
    String strPartNumber = GmCommonClass.parseNull(request.getParameter("hPartNumber"));
    String strRowPart[] = strPartNumber.split("@");
    String strItemOrderID = GmCommonClass.parseNull(request.getParameter("hItemID"));

    String strOrderId = GmCommonClass.parseNull(request.getParameter("hOrderId"));
	String strSetId = GmCommonClass.parseNull(request.getParameter("setid"));
	String strSetBundleId = GmCommonClass.parseNull(request.getParameter("setbundleid"));
    String strShipTo = "";
    String strPreffCarrier = "";
    String strDefMode = "";
    String strNetUnitPrice = "";
    String strPartyId = GmCommonClass.parseNull(request.getParameter("hPartyId"));
    String strUnitPrice = "";
    String strConsignId = "";
    String strValidMsg = "";  // PMT-24605 To get the plantValid value
    String strTissueParts = "";
    String strTissueSets = ""; //PMT-40240 - to get Tissue setid
    String strInhouseType ="";
    String strFrmGrid="";
    try {
      GmCommonCartBean gmCart = new GmCommonCartBean(getGmDataStoreVO());
      GmSetBundleMappingBean gmSetBundle = new GmSetBundleMappingBean(getGmDataStoreVO());
      GmAddressBean gmAddressBean = new GmAddressBean(getGmDataStoreVO());
      GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean(getGmDataStoreVO());
      GmDORptBean gmDORptBean = new GmDORptBean(getGmDataStoreVO());
      GmXMLParserUtility gmXMLParserUtility = new GmXMLParserUtility();
      GmInHouseTransactionBean gmInHouseTransactionBean = new GmInHouseTransactionBean(getGmDataStoreVO());
      StringBuffer sbXML = new StringBuffer();

      HashMap hmParam = parseRequest(request);
      log.debug(" hmParam values is " + hmParam);
      int intCartSize = 0;
      strRefId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
      strOrdType = GmCommonClass.parseNull((String) hmParam.get("ORDTYPE"));
      strPnum = GmCommonClass.parseNull((String) hmParam.get("PNUM"));
      strShipTo = GmCommonClass.parseNull((String) hmParam.get("SHIPTO"));
      strInhouseType = GmCommonClass.parseNull((String) hmParam.get("INHOUSETYPE"));
      strFrmGrid = GmCommonClass.parseNull((String) hmParam.get("FRMGRID")); 
      HashMap hmResult = new HashMap();
      String strThirdPartyNm = "";
      String strFreightAmount = "";
      if (!strRefId.equals("")) {
        strXMLString = gmCart.fetchRefIdFlag(strRefId, strOrdType, strPnum);
      } else {     
        if (strAction.equalsIgnoreCase("Set")) { // fetch set details for product loaner        
          alCart = gmCart.loadSetDetails(strSetId);
          strTissueSets = gmCart.loadTissueSetDetails(strSetId); //PMT-40240 -fetch Tissue Set id
          log.debug("alcart details1 " + alCart.get(0));
        }else if (strAction.equalsIgnoreCase("SetBundle")) { // fetch set details for product loaner            
            alCart = gmSetBundle.loadSetBundleDetails(strSetBundleId,strSetId);
            log.debug("alcart details bundle " + alCart.get(0));
          }  
        else if (strAction.equalsIgnoreCase("fetchdefaultcarrier")) { // PMT-4359 -- Fetching the
                                                                        // preferred Carrier/Rule
                                                                        // carrier for the Rep,
                                                                        // Account/Dist,N/A and
                                                                        // Employee
          strXMLString =
              gmAddressBean.fetchPrefCarrier(strShipTo, strRepAccid, strRepAddID, strAcctID);
        } else if (strAction.equalsIgnoreCase("fetchCarrierModes")) { // PMT-5320 -- Fetching the
                                                                      // preferred Carrier/Rule
                                                                      // carrier for the Rep,
                                                                      // Account/Dist,N/A and
                                                                      // Employee
          hmModeValues = gmShippingInfoBean.fetchCarrierBasedModes(strCarrier, strAcctID);
          alCart = (ArrayList) hmModeValues.get("SHIMODELIST");
          strDefMode = (String) hmModeValues.get("DEFAULTMODE");
          log.debug(" strDefMode is >>>>>>>>>>>>>>>>>" + strDefMode);
        } else if (strAction.equalsIgnoreCase("fetchNetUnitPrice")) { // PAC-142 [ Fetching the Net
                                                                      // Unit Price based on the
                                                                      // Adjustment Code for the
                                                                      // Corresponding Account,
                                                                      // PartNumber, Order ID]
          strNetUnitPrice = gmCart.fetchNetUnitPrice(strAcctID, strAdjCodeId, strItemOrderID);
          strNetUnitPrice = strNetUnitPrice + "@" + strRowPart[1];
        } else if (strAction.equalsIgnoreCase("fetchAccPartListPrice")) { // fetch unit price
          HashMap hmMap = gmCart.fetchAccPartListPrice(strAcctID, strPartNumber, strAdjCodeId);
          strUnitPrice = GmCommonClass.parseNull((String) hmMap.get("UNITPRICE"));
          strUnitPrice = strUnitPrice + "@" + strRowPart[1];
        } else if (strAction.equalsIgnoreCase("fetchThirdPatyAccount")) {
          hmResult =
              GmCommonClass.parseNullHashMap(gmAddressBean.fetchThirdPatyAccount(strPartyId));
          strThirdPartyNm = GmCommonClass.parseNull((String) hmResult.get("THIRDPARTY"));
          strFreightAmount = GmCommonClass.parseNull((String) hmResult.get("FREIGHTAMT"));
        } else if (strAction.equalsIgnoreCase("fetchPartDesc")) {
          partDesc = gmCart.fetchPartDesc(strPnum);
        } else if (strAction.equalsIgnoreCase("ConsignLoad")) { // PMT-17225 [to fetch all the
                                                                // parts details from a particular
                                                                // consignment/Tag ID]

          strConsignId = GmCommonClass.parseNull(request.getParameter("consinId"));
          hmParam.put("CONSIGNID", strConsignId);
          alCart = gmCart.fetchConsignDtls(hmParam);
        } else { // fetch part num details for consignment/in-house
          // Validate the part number
          strValidMsg = gmDORptBean.validatePartNumPlant(hmParam);
          alCart = gmCart.loadPartNum(hmParam);
		  strTissueParts = gmCart.loadTissuePartNum(hmParam);
        }
        if(strFrmGrid.equals("Y")) {
		  if(strInhouseType.equals("111800")) { // Quarantine - Inhouse 
			  alCart = gmCart.loadPartNumFromQNWarehouse(hmParam); 
		  //strXMLString = gmInHouseTransactionBean.getXmlGridData(alCart); 
		  }else if(strInhouseType.equals("111802")) { // Finished Goods - Inhouse 
			  alCart =  gmCart.loadPartNumFromFGWarehouse(hmParam); 
		  //strXMLString = gmInHouseTransactionBean.getXmlGridData(alCart); 
		}else if(strInhouseType.equals("111801")) { // Bulk - Inhouse 
			  alCart = gmCart.loadPartNumFromBLWarehouse(hmParam); 
		  //strXMLString = gmInHouseTransactionBean.getXmlGridData(alCart); 
		}else if(strInhouseType.equals("111803")) { // Repackaging - Inhouse 
			  alCart = gmCart.loadPartNumFromPNWarehouse(hmParam); 
		  //strXMLString = gmInHouseTransactionBean.getXmlGridData(alCart); 
		}else if(strInhouseType.equals("111804")) { // IH Inventory - Inhouse 
			  alCart = gmCart.loadPartNumFromIHWarehouse(hmParam); 
		  //strXMLString = gmInHouseTransactionBean.getXmlGridData(alCart); 
		}else if(strInhouseType.equals("111805")) { // Restricted Warehouse -Inhouse 
			  alCart = gmCart.loadPartNumFromRWWarehouse(hmParam); 
		  //strXMLString =gmInHouseTransactionBean.getXmlGridData(alCart); 
		  }
		 
		  }
        if (alCart != null) {
          intCartSize = alCart.size();
        }
        log.debug(" intCartSize " + intCartSize);
        // PMT-4359 -- for this fetchdefaultcarrier action type we are just returning the carrier
        // value.
        if (strAction.equalsIgnoreCase("fetchCarrierModes")) {
          strXMLString = gmXMLParserUtility.createXMLString(alCart);
          strXMLString = "<response>" + strXMLString + "<mode>" + strDefMode + "</mode></response>";
        } else if (strAction.equalsIgnoreCase("fetchNetUnitPrice")) {
          strXMLString = gmXMLParserUtility.createXMLString(alCart);
          strXMLString =
              "<response>" + strXMLString + "<unitPrice>" + strNetUnitPrice
                  + "</unitPrice></response>";
        } else if (strAction.equalsIgnoreCase("fetchAccPartListPrice")) {
          strXMLString = gmXMLParserUtility.createXMLString(alCart);
          strXMLString =
              "<response>" + strXMLString + "<unitPrice>" + strUnitPrice
                  + "</unitPrice></response>";
        } else if (strAction.equalsIgnoreCase("fetchPartDesc")) {
          strXMLString = partDesc;
        } else if (!strAction.equalsIgnoreCase("fetchdefaultcarrier")) {
          strXMLString = gmXMLParserUtility.createXMLString(alCart);
          strXMLString =
              "<response>" + strXMLString + "<unitPrice>" + strNetUnitPrice
              + "</unitPrice>" + "<plantValid><validMsg>" + strValidMsg + "</validMsg></plantValid>" // PMT-24605 To get the plantValid value
          + "<tissueparts><parts>" + strTissueParts + "</parts></tissueparts>"
          		+ "<tissueSets><sets>" + strTissueSets + "</sets></tissueSets></response>"; // PMT-24605 To get the plantValid value
        } else if (!strAction.equalsIgnoreCase("fetchdefaultcarrier")
            && !strAction.equalsIgnoreCase("fetchThirdPatyAccount")) {
          strXMLString = gmXMLParserUtility.createXMLString(alCart);
        }
        if (strAction.equalsIgnoreCase("fetchThirdPatyAccount")) {
          strXMLString = gmXMLParserUtility.createXMLString(alCart);
          strXMLString =
              "<response>" + strXMLString + "<thirdparty>" + strThirdPartyNm
                  + "</thirdparty><freightamt>" + strFreightAmount + "</freightamt></response>";

        }
      }
     
      if(strFrmGrid.equals("N")) {
      if(strInhouseType.equals("111800")) { // Quarantine - Inhouse
      	alCart = gmCart.loadPartNumFromQNWarehouse(hmParam);
      }else if(strInhouseType.equals("111802")) { // Finished Goods - Inhouse
      	alCart = gmCart.loadPartNumFromFGWarehouse(hmParam);
      }else if(strInhouseType.equals("111801")) { // Bulk - Inhouse
      	alCart = gmCart.loadPartNumFromBLWarehouse(hmParam);
      }else if(strInhouseType.equals("111803")) { // Repackaging - Inhouse
      	alCart = gmCart.loadPartNumFromPNWarehouse(hmParam);
      }else if(strInhouseType.equals("111804")) { // IH Inventory - Inhouse
      	alCart = gmCart.loadPartNumFromIHWarehouse(hmParam);
      }else if(strInhouseType.equals("111805")) { // Restricted Warehouse -Inhouse
      	alCart = gmCart.loadPartNumFromRWWarehouse(hmParam);
      }
      log.debug("alCart==="+alCart);
      log.debug("alCart=sizee=="+alCart.size());
      if (alCart != null) {
          intCartSize = alCart.size();
          log.debug("alCart=sizee=="+alCart.size());
          if(intCartSize != 0) {
        	    log.debug("intCartSize=sizee=="+intCartSize);
          strXMLString =gmInHouseTransactionBean.getXmlGridData(alCart); 
          }else {
          	strXMLString="";
          }
        }
      }
      log.debug(strXMLString);
      response.setContentType("text/xml;charset=utf-8");
      response.setHeader("Cache-Control", "no-cache");

      PrintWriter writer = response.getWriter();
      writer.println(strXMLString);
      writer.close();

    }// End of try
    catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("hAppErrorMessage", e.getMessage());
      String strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }
}
