package com.globus.common.crypt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyStore;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * @author Joe Prasanna Kumar This program provides the following cryptographic
 *         functionalities 1. Encryption using AES 2. Decryption using AES
 * 
 * High Level Algorithm : 1. Generate a AES key (specify the Key size during
 * this phase) 2. Create the Cipher 3. To Encrypt : Initialize the Cipher for
 * Encryption 4. To Decrypt : Initialize the Cipher for Decryption
 * 
 * 
 */

public class AESEncryptDecrypt {
	private static Cipher aesCipher = null;
	private static SecretKey secretKey = null;
	private static final String ALGORITHM = "AES";
	private static final String KEYSTORE_FILE = "c:\\com\\store\\keystore.txt";
	private sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
	private sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
	private String charSet = "UTF-8";

	/*
	 * Singleton to return SecretKey. SecretKey is really the key :)
	 * It locks and unlocks the data
	 */
	private static SecretKey getSecretKey() {
			try {
				FileInputStream fis = new FileInputStream(KEYSTORE_FILE);
				int kl = fis.available();
				byte[] kb = new byte[kl];
				fis.read(kb);
				secretKey = new SecretKeySpec(kb, ALGORITHM);
				fis.close();
			} catch (Exception exp) {
				try {
					if (secretKey == null) {
						KeyGenerator keyGen = KeyGenerator.getInstance(ALGORITHM);
						keyGen.init(128);
						secretKey = keyGen.generateKey();
						writeKey();
					}
				} catch (Exception exp1) {
					exp1.printStackTrace();
				}
			}
		return secretKey;
	}

	/*
	 * Will re-init the secretkey to NULL
	 */
	public static void resetSecretKey(){
		secretKey = null;
	}
	
	/*
	 * Writes the generated key to output file
	 */
	private static void writeKey() throws Exception {
		FileOutputStream fos = new FileOutputStream(KEYSTORE_FILE);
		byte[] kb = secretKey.getEncoded();
		fos.write(kb);
		fos.close();
	}

	
	/*
	 * Singleton to return a Cipher which implements the AES algorithm
	 */
	private static Cipher getAESCipher() {
		if (aesCipher == null) {
			try {
				aesCipher = Cipher.getInstance(ALGORITHM);
			} catch (Exception exp) {
				exp.printStackTrace();
			}
		}
		return aesCipher;
	}

	/**
	 * Encrypt the strDataToEncrypt 
	 * 1. Declare / Initialize the Data. Here the data is of type String 
	 * 2. Convert the Input Text to Bytes 
	 */
	public String encryptText(String strDataToEncrypt) {
		String strCipherText = "";
		Cipher aesCipher = getAESCipher();
		SecretKey secKey = getSecretKey();

		try {
			aesCipher.init(Cipher.ENCRYPT_MODE, secKey);

			byte[] byteDataToEncrypt = strDataToEncrypt.getBytes();
			byte[] byteCipherText = aesCipher.doFinal(byteDataToEncrypt);
			strCipherText = URLEncoder.encode(encoder.encode(byteCipherText),charSet);
		}

		catch (Exception exp) {
			exp.printStackTrace();
		}
		return strCipherText;
	}

	/**
	 * Decrypt the Data 
	 * 1. Initialize the Cipher for Decryption
	 * 2. Decrypt the cipher bytes using doFinal method
	 */
	public String decryptText(String encryptText) {
		String strDecryptedText = new String();
		Cipher aesCipher = getAESCipher();
		SecretKey secKey = getSecretKey();

		try {
			aesCipher.init(Cipher.DECRYPT_MODE, secKey, aesCipher.getParameters());
			byte[] byteCipherText = decoder.decodeBuffer(URLDecoder.decode(encryptText,charSet));
			byte[] byteDecryptedText = aesCipher.doFinal(byteCipherText);
			strDecryptedText = new String(byteDecryptedText);

		} catch (Exception exp) {
			exp.printStackTrace();
		}

		return strDecryptedText;
	}

}
