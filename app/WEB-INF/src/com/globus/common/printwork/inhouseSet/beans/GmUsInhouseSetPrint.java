package com.globus.common.printwork.inhouseSet.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.operations.loaners.beans.GmSetLotMasterBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmUsInhouseSetPrint implements GmInhouseSetPrintInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  @Override
  public ArrayList fetchConsignSetDtls(HashMap hmInData) {
    // TODO Auto-generated method stub
    ArrayList alReturn = new ArrayList();
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmInData.get("gmDataStoreVO");
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(gmDataStoreVO);
    GmSetLotMasterBean gmSetLotMasterBean =  new GmSetLotMasterBean(gmDataStoreVO);
    String strConsignId = GmCommonClass.parseNull((String) hmInData.get("CONSIGNID"));
    GmResourceBundleBean gmResourceBundleBean =
        (GmResourceBundleBean) hmInData.get("gmResourceBundleBean");
    String strAction = GmCommonClass.parseNull((String) hmInData.get("ACTION"));
    String strlottrkfl = GmCommonClass.parseNull((String) hmInData.get("LOTTRKFL"));
    if(strlottrkfl.equals("Y")){
        alReturn = gmSetLotMasterBean.getSetConsignLotDetails(strConsignId);
    } else {
    	alReturn = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
    }
    return alReturn;
  }

}
