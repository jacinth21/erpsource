package com.globus.common.printwork.inhouseSet.beans;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.AppError;

public interface GmInhouseSetPrintInterface {
	public ArrayList fetchConsignSetDtls (HashMap hmInData) throws AppError;
}