package com.globus.common.printwork.inhouseSet.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmBBAInhouseSetPrint implements GmInhouseSetPrintInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  @Override
  public ArrayList fetchConsignSetDtls(HashMap hmInData) throws AppError {
    // TODO Auto-generated method stub
    ArrayList alReturn = new ArrayList();
    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmInData.get("gmDataStoreVO");
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(gmDataStoreVO);
    String strConsignId = GmCommonClass.parseNull((String) hmInData.get("CONSIGNID"));
    GmResourceBundleBean gmResourceBundleBean =
        (GmResourceBundleBean) hmInData.get("gmResourceBundleBean");
    String strAction = GmCommonClass.parseNull((String) hmInData.get("ACTION"));
    alReturn = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");
    return alReturn;
  }

}
