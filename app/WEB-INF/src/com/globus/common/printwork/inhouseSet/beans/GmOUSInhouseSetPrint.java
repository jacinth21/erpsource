package com.globus.common.printwork.inhouseSet.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.operations.loaners.beans.GmSetLotMasterBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmOUSInhouseSetPrint implements GmInhouseSetPrintInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  @Override
  public ArrayList fetchConsignSetDtls(HashMap hmInData) {
    // TODO Auto-generated method stub
    ArrayList alReturn = new ArrayList();

    GmDataStoreVO gmDataStoreVO = (GmDataStoreVO) hmInData.get("gmDataStoreVO");
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean(gmDataStoreVO);
    String strConsignId = GmCommonClass.parseNull((String) hmInData.get("CONSIGNID"));
    GmResourceBundleBean gmResourceBundleBean =
        (GmResourceBundleBean) hmInData.get("gmResourceBundleBean");
    GmSetLotMasterBean gmSetLotMasterBean = new GmSetLotMasterBean(gmDataStoreVO);
    String strAction = GmCommonClass.parseNull((String) hmInData.get("ACTION"));
    //
    String strShowCnumLSS = "";
    String strShowLSalesInDB = "";
    String strLoanerstatusFlag = "";
    String strLoanerPaperworkFalg = "";

    strShowCnumLSS =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LOANER.SHOW_CNUM_LN_STK_SHT"));
    strShowLSalesInDB =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("LOANER.SHOW_4127_INDB"));
    strLoanerstatusFlag = GmCommonClass.parseNull((String) hmInData.get("LOANERSTATUSFLAG"));
    strLoanerPaperworkFalg = GmCommonClass.parseNull((String) hmInData.get("LOANERPAPERWORKFLAG"));


    if (strAction.equals("ViewPrint") && strShowCnumLSS.equalsIgnoreCase("NO")) {

      if (strLoanerPaperworkFalg.equals("YES")) {
        alReturn = gmSetLotMasterBean.getSetConsignLotDetails(strConsignId);
      } else {
        alReturn = gmInHouseRptBean.getSetConsignDetails(strConsignId, "", strShowCnumLSS);
      }


    } else if (strAction.equals("ViewPrint") || strAction.equals("ViewLetterVer")
        && (strShowLSalesInDB.equalsIgnoreCase("YES") || strShowCnumLSS.equalsIgnoreCase("YES"))) {// OUS
                                                                                                   // Code
                                                                                                   // Merge
      alReturn = gmInHouseRptBean.getSetConsignDetails(strConsignId);


    } else {

      if (strLoanerPaperworkFalg.equals("YES")) {
        alReturn = gmSetLotMasterBean.getSetConsignLotDetails(strConsignId);
      } else {
        alReturn = gmInHouseRptBean.getSetConsignDetails(strConsignId, "", strAction);
      }

    }
    return alReturn;
  }
}
