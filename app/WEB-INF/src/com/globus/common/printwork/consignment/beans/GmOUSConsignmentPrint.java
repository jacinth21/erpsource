package com.globus.common.printwork.consignment.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmICTSummaryBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmOUSConsignmentPrint implements GmConsignmentPrintInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  @Override
  public HashMap fetchConsignmentDtls(HashMap hmInData) throws AppError {
    // TODO Auto-generated method stub
    String strConsignId = GmCommonClass.parseNull((String) hmInData.get("TXNID"));
    String strUserId = GmCommonClass.parseNull((String) hmInData.get("USERID"));
    String strCountryId = GmCommonClass.parseNull((String) hmInData.get("COUNTRYID"));
    GmDataStoreVO gmDataStoreVo = (GmDataStoreVO) hmInData.get("DATASTOREVO");
    GmICTSummaryBean gmICTSummaryBean = new GmICTSummaryBean(gmDataStoreVo);
    HashMap hmReturn = new HashMap();

    hmReturn = gmICTSummaryBean.fetchConsignmentDetails(strConsignId, strUserId);

    return hmReturn;
  }


}
