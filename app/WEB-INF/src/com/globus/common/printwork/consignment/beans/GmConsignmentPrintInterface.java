package com.globus.common.printwork.consignment.beans;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.globus.common.beans.AppError;

public interface GmConsignmentPrintInterface {
	public HashMap fetchConsignmentDtls(HashMap hmInData) throws AppError;
}