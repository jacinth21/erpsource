package com.globus.common.printwork.consignment.beans;

import java.util.HashMap;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.jdbc.support.DatabaseStartupValidator;

import com.globus.accounts.beans.GmICTSummaryBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.operations.shipping.beans.GmPrintPaperworkBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmUsConsignmentPrint implements GmConsignmentPrintInterface {

	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

	@Override
	public HashMap fetchConsignmentDtls(HashMap hmInData) throws AppError {
		// TODO Auto-generated method stub
		String  strConsignId = GmCommonClass.parseNull((String)hmInData.get("TXNID")); 
		String  strUserId = GmCommonClass.parseNull((String)hmInData.get("USERID"));
		String  strCountryId = GmCommonClass.parseNull((String)hmInData.get("COUNTRYID"));
		
		String  strOrdId = GmCommonClass.parseNull((String)hmInData.get("ORDID")); 
		
		GmDataStoreVO gmDataStoreVo = (GmDataStoreVO)  hmInData.get("DATASTOREVO");
		GmICTSummaryBean gmICTSummaryBean = new GmICTSummaryBean(gmDataStoreVo);
		HashMap hmReturn = new HashMap();
		log.debug("fetchConsignmentDtls  strOrdId  "+strOrdId);
		log.debug("fetchConsignmentDtls  strOrdId  "+strOrdId.length());
		if(strOrdId.equals("") || strOrdId.equalsIgnoreCase("NULL")|| strOrdId.length()==0){
			hmReturn = gmICTSummaryBean.fetchConsignmentDetails(strConsignId, strUserId);
		}else
		{
			hmReturn = gmICTSummaryBean.fetchOUSOrderDetails(strOrdId, strUserId);
		}
		return hmReturn;
	}

}
