package com.globus.common.printwork.invoice.beans;

import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import com.globus.accounts.beans.GmInvoiceBean;
import com.globus.accounts.beans.GmInvoicePrintBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmInvoiceListPriceLayoutPrint implements GmInvoicePrintLayoutInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to
  public String loadOrderItemLayoutSQL(String strOrderId ,String strAction, GmDataStoreVO gmDataStoreVO)
      throws AppError {
	  //Calls the method-loadOrderItemListPriceSQL for Default List Price--PC-643
	  GmInvoicePrintInterface gmInvoicePrintInterface = (GmInvoicePrintInterface) GmInvoiceBean.getInvoicePrintClass(gmDataStoreVO.getCmpid());
	  String strQuery=gmInvoicePrintInterface.loadOrderItemListPriceSQL(strOrderId, strAction, gmDataStoreVO);
	  return strQuery;
  }

}
