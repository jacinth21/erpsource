package com.globus.common.printwork.invoice.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmInvoicePrintBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmBBAInvoicePrint implements GmInvoicePrintInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  @Override
  public HashMap fetchInvoiceDtls(HashMap hmInData) throws AppError {
    // TODO Auto-generated method stub
    HashMap hmReturn = new HashMap();
    String strPartyNm = "";
    String strPartyFlag = "";
    HashMap hmInvoiceReturn = GmCommonClass.parseNullHashMap((HashMap) hmInData.get("hmReturn"));
    String strCompanyLocale = (String) hmInData.get("CompanyLocale");
    String strLogoImage = "";
    String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");

    HashMap hmOrderDtls = (HashMap) hmInvoiceReturn.get("ORDERDETAILS");

    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);

    strPartyNm = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYNAME"));
    strPartyNm = "-Please make checks payable to <b>" + strPartyNm + "</b>";
    strPartyFlag = GmCommonClass.parseNull((String) hmInvoiceReturn.get("PARTYFLAG"));



    if (strPartyFlag.equals("")) {
      String strDivisionId = GmCommonClass.parseNull((String) hmOrderDtls.get("DIVISION_ID"));
      String strCompanyName =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYNAME"));
      String strCompanyAddress =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYADDRESS"));
      String strCompanyPhone =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYPHONE"));
      String strCompanyRemitAddress =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMREMIT"));
      String strHeadAddress = "";
      String strRemitAddress = "";

      if (!strDivisionId.equals("")) {
        strCompanyAddress =
            GmCommonClass.parseNull(gmResourceBundleBean
                .getProperty(strDivisionId + ".COMPADDRESS"));
        strCompanyName =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".COMPNAME"));
        strCompanyPhone =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".PH"));
        strCompanyRemitAddress =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId
                + ".COMP_REMIT_TO"));
        strLogoImage =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".LOGO"));
      } else {
        strLogoImage = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COMP_LOGO"));
      }
      strCompanyAddress = strCompanyAddress.replaceAll("/", "\n<br>");
      String strRemitName =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMREMITHEAD"));
      strCompanyRemitAddress = strCompanyRemitAddress.replaceAll("/", "\n<br>");

      strHeadAddress =
          "<b>" + strCompanyName + "</b><BR>" + strCompanyAddress + "<br>" + strCompanyPhone;
      strRemitAddress = "<u><b>" + strRemitName + "</b></u><br>" + strCompanyRemitAddress;
      strPartyNm = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYNAME"));
      strPartyNm = "-Please make checks payable to <b>" + strPartyNm + "</b>";
      strPartyFlag = "";

      hmReturn.put("PARTYFLAG", strPartyFlag);
      hmReturn.put("PARTYNAME", strPartyNm);
      hmReturn.put("HEADERADDRESS", strHeadAddress);
      hmReturn.put("REMITADDRESS", strRemitAddress);
      hmReturn.put("LOGOIMAGE", strImagePath + strLogoImage + ".gif");
    }


    return hmReturn;
  }

  @Override
  public ArrayList fetchInvoiceArray(HashMap hmInData) {
    // TODO Auto-generated method stub
    ArrayList alResult = new ArrayList();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmInData.get("ALORDERLIST"));
    return alResult;
  }

  @Override
  public String loadOrderItemSQL(String strOrderId, String strAction, GmDataStoreVO gmDataStoreVO)
      throws AppError {
    // TODO Auto-generated method stub
    String strItemSQL = "";
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(gmDataStoreVO);
    strItemSQL = gmInvoicePrintBean.loadOrderItemSQL(strOrderId, strAction);
    return strItemSQL;
  }

@Override
public String loadOrderItemListPriceSQL(String strOrderId, String strAction, GmDataStoreVO gmDataStoreVO) {
	// TODO Auto-generated method stub
	
	return "";
}
}
