package com.globus.common.printwork.invoice.beans;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.beans.AppError;
import com.globus.valueobject.common.GmDataStoreVO;

public interface GmInvoicePrintInterface {
  public HashMap fetchInvoiceDtls(HashMap hmInData) throws AppError;
  public ArrayList fetchInvoiceArray(HashMap hmInData) throws AppError;

  public String loadOrderItemSQL(String strOrderId, String strAction, GmDataStoreVO gmDataStoreVO)
      throws AppError;
  public String loadOrderItemListPriceSQL(String strOrderId, String strAction, GmDataStoreVO gmDataStoreVO)
	      throws AppError;
}
