package com.globus.common.printwork.invoice.beans;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmInvoicePrintBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.sales.GmCompanyVO;

public class GmHQInvoicePrint implements GmInvoicePrintInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  @Override
  public HashMap fetchInvoiceDtls(HashMap hmInData) throws AppError {
    // TODO Auto-generated method stub
    HashMap hmReturn = new HashMap();
    HashMap hmOrderDtls = new HashMap();
    HashMap hmInvoiceReturn = GmCommonClass.parseNullHashMap((HashMap) hmInData.get("hmReturn"));
    hmOrderDtls = (HashMap) hmInvoiceReturn.get("ORDERDETAILS");
    String strCompanyRemitAddress = "";
    String strRemitName = "";
    String strHeadAddress = "";
    String strRemitAddress = "";
    String strThirdParty = "";
    String strRemitMailAddress = "";
    String strCompanyName = "";
    String strCompanyAddress = "";
    String strCompanyPhone = "";
    String strPartyNm = "";
    String strCompanyRemitMailAddress = "";
    String strRemitMailName = "";
    String strLogoImage = "";
    String strCompSeal = "";
    String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");
    String strVATId = "";
    String strShowPhoneNum = "";
    String strAccCurrId = "";
    String strCompPH = "";
    String strCompFax = "";

    String strCompanyLocale = (String) hmInData.get("CompanyLocale");
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    String strThirdPartyId = GmCommonClass.parseNull((String) hmOrderDtls.get("THIRDPARTYID"));
    String strPartyFlag = GmCommonClass.parseNull((String) hmInvoiceReturn.get("PARTYFLAG"));
    String strCompanyId = GmCommonClass.parseNull((String) hmOrderDtls.get("COMPANY_ID"));
    strCompanyId = strCompanyId.equals("") ? "1018" : strCompanyId;
    strAccCurrId = GmCommonClass.parseNull((String) hmOrderDtls.get("CURRENCYID"));
    // date format getting based on company
    GmCompanyVO compVO = (new GmCommonBean()).getCompanyVO(strCompanyId);
    String strApplnDateFmt = compVO.getCmpdfmt();
    // Currency based date format
    String strAccountCurrencyDateFmt =
        GmCommonClass.parseNull((String) hmOrderDtls.get("ACC_CURR_DATEFMT"));
    strAccountCurrencyDateFmt =
        strAccountCurrencyDateFmt.equals("") ? strApplnDateFmt : strAccountCurrencyDateFmt;
    String strInvoiceDt =
        GmCommonClass.getStringFromDate((java.sql.Date) hmOrderDtls.get("INVDT"),
            strAccountCurrencyDateFmt);
    String strDivisionId = GmCommonClass.parseNull((String) hmOrderDtls.get("DIVISION_ID"));
    if (strPartyFlag.equals("")) {

      String strTaxStartDateFl = "N";
      // GmCommonClass.parseNull((String) hmOrderDtls.get("TAX_START_DATE_FL"));
      String strAdjStartDateFl = "N";
      // GmCommonClass.parseNull((String) hmOrderDtls.get("ADJ_START_DATE_FL"));
      String strTempDiv = strDivisionId;


      strCompanyAddress = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMADDRESS"));
      strShowPhoneNum = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("SHOWPHONE"));

      if (!strDivisionId.equals("")) {
        strCompanyAddress =
            GmCommonClass.parseNull(gmResourceBundleBean
                .getProperty(strDivisionId + ".COMPADDRESS"));
        strCompanyName =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".COMPNAME"));
        if (!strShowPhoneNum.equals("NO")) {
          strCompanyPhone =
              GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".PH"));
        }
        strCompanyAddress = strCompanyAddress.replaceAll("/", "\n<br>");
        strCompanyRemitAddress =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId
                + ".COMP_REMIT_TO"));
        strLogoImage =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strTempDiv + ".LOGO"));
        strCompSeal = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strTempDiv + ".COMP_SEAL"));
        strVATId =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".VATID"));
      }

      strRemitName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMREMITHEAD"));
      strCompanyRemitAddress = strCompanyRemitAddress.replaceAll("/", "\n<br>");

      strHeadAddress =
          "<b>" + strCompanyName + "</b><BR>" + strCompanyAddress + "<br>" + strCompanyPhone
              + "\n<br>" + strVATId;
      strRemitAddress = "<u><b>" + strRemitName + "</b></u><br>" + strCompanyRemitAddress;

      hmReturn.put("PARTYNAME",
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYNAME")));
      hmReturn.put("REMITADDRESS", strRemitAddress);

      strPartyNm =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strCompanyId + ".COMPNAME"));
      strPartyNm = " <b>-Please Remit to " + strPartyNm + "</b>";

      strCompanyRemitAddress =
          GmCommonClass
              .parseNull(gmResourceBundleBean.getProperty(strCompanyId + ".COMP_REMIT_TO"));
      strRemitName =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strCompanyId + ".GMREMITHEAD"));
      strCompanyRemitAddress = strCompanyRemitAddress.replaceAll("/", "\n<br>");
      strRemitAddress = "<u><b>" + strRemitName + "</b></u><br>" + strCompanyRemitAddress;
      strCompanyRemitMailAddress =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strAccCurrId
              + ".COMP_REMIT_TO_MAIL"));
      strRemitMailName =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strAccCurrId
              + ".GMREMITHEAD_MAIL"));
      strLogoImage =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strTempDiv + ".LOGO"));
      strCompSeal = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strTempDiv + ".COMP_SEAL"));
      strCompanyRemitMailAddress = strCompanyRemitMailAddress.replaceAll("/", "\n<br>");
      strRemitMailAddress =
          "<u><b>" + strRemitMailName + "</b></u><br>" + strCompanyRemitMailAddress;

      hmReturn.put("PARTYNAME", strPartyNm);
      hmReturn.put("REMITADDRESS", strRemitAddress);
      hmReturn.put("REMITMAILADDRESS", strRemitMailAddress);
      
      hmReturn.put("SEALIMAGE", strImagePath + strCompSeal + ".gif");
      hmReturn.put("LOGOIMAGE", strImagePath + strLogoImage + ".gif");
      hmReturn.put("PARTYFLAG", "");
      hmReturn.put("HEADERADDRESS", strHeadAddress);
    }
    // hmReturn.put("INVDT", strInvoiceDt);
    // To setting the Company information
    hmReturn.put("GLOBUSVATNUM",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYBANK_CODE")));
    if(strDivisionId.equals("2006")){
    	 strCompPH = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".PH"));
    	 strCompFax = GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".FAX"));
    	 hmReturn.put("GMCOMPANYPHONE",strCompPH);
    	 hmReturn.put("GMCOMPANYFAX",strCompFax);
    }else{
    hmReturn.put("GMCOMPANYPHONE",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYPHONE")));
    hmReturn.put("GMCOMPANYFAX",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYFAX")));
    }
    hmReturn.put("GMCOMPANYEMAIL",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYEMAIL")));
    hmReturn.put("GMCOMPANYURL",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYURL")));
    hmReturn.put("GMCOMPANYBANK",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYBANK")));
    hmReturn.put("GMCOMPANYIBAN",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYIBAN")));
    hmReturn.put("GMCOMPANYBRANCH",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYBRANCH")));
    hmReturn.put("GMCOMPANYBIC",
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYBIC")));

    return hmReturn;
  }

  @Override
  public ArrayList fetchInvoiceArray(HashMap hmInData) {
    // TODO Auto-generated method stub
    ArrayList alResult = new ArrayList();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmInData.get("ALORDERLIST"));
    return alResult;
  }

  @Override
  public String loadOrderItemSQL(String strOrderId, String strAction, GmDataStoreVO gmDataStoreVO)
      throws AppError {
    // TODO Auto-generated method stub
    String strItemSQL = "";
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(gmDataStoreVO);
    strItemSQL = gmInvoicePrintBean.loadOrderItemSQL(strOrderId, strAction);
    return strItemSQL;
  }

@Override

	public String loadOrderItemListPriceSQL(String strOrderId, String strAction, GmDataStoreVO gmDataStoreVO) {
		return "";
	}

}
