package com.globus.common.printwork.invoice.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.log4j.Logger;

import com.globus.accounts.beans.GmInvoicePrintBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.sales.GmCompanyVO;

public class GmOUSInvoicePrint implements GmInvoicePrintInterface {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  @Override
  public HashMap fetchInvoiceDtls(HashMap hmInData) throws AppError {
    // TODO Auto-generated method stub
    HashMap hmReturn = new HashMap();
    String strPartyNm = "";
    String strPartyFlag = "";
    String strLogoImage = "";
    String strImagePath = System.getProperty("ENV_PAPERWORKIMAGES");


    HashMap hmInvoiceReturn = GmCommonClass.parseNullHashMap((HashMap) hmInData.get("hmReturn"));
    String strCompanyLocale = (String) hmInData.get("CompanyLocale");
    GmResourceBundleBean gmResourceBundleBean =
        GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
    HashMap hmOrderDtls = (HashMap) hmInvoiceReturn.get("ORDERDETAILS");

    strPartyNm = GmCommonClass.parseNull(GmCommonClass.getString("GMCOMPANYNAME"));
    strPartyNm = "-Please make checks payable to <b>" + strPartyNm + "</b>";
    strPartyFlag = GmCommonClass.parseNull((String) hmInvoiceReturn.get("PARTYFLAG"));

    if (strPartyFlag.equals("")) {
      String strHeadAddress = "";
      String strRemitAddress = "";
      String strEmailRemitAddress = "";
      String strDivisionId = GmCommonClass.parseNull((String) hmOrderDtls.get("DIVISION_ID"));
      String straddress =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYADDRESS"));
      String strGMName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMCOMPANYNAME"));
      straddress = "&nbsp;" + straddress.replaceAll("/", "\n<br>&nbsp;");

      String strRremitaddr = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMREMIT"));
      String strRemitName =
          GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMREMITHEAD"));
      strLogoImage = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("COMP_LOGO"));
      if (!strDivisionId.equals("")) {
        straddress =
            GmCommonClass.parseNull(gmResourceBundleBean
                .getProperty(strDivisionId + ".COMPADDRESS"));
        strGMName =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".COMPNAME"));
        strRremitaddr =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId
                + ".COMP_REMIT_TO"));
        strLogoImage =
            GmCommonClass.parseNull(gmResourceBundleBean.getProperty(strDivisionId + ".LOGO"));
      }
      strRremitaddr = "&nbsp;" + strRremitaddr.replaceAll("/", "\n&nbsp;&nbsp;<br>&nbsp;");
      straddress = "&nbsp;" + straddress.replaceAll("/", "\n<br>&nbsp;");
      strHeadAddress = "<br>&nbsp;&nbsp;<b>" + strGMName + "</b><BR>&nbsp;" + straddress;
      strRemitAddress = "<u><b>" + strRemitName + "</b></u>&nbsp;&nbsp;<br>&nbsp;" + strRremitaddr;
      strPartyNm = strGMName;
      strPartyFlag = "";

      // OUS code
      ArrayList alDOControlNum = new ArrayList();
      ArrayList alDOCNums = new ArrayList();
      HashMap hmConstructs = new HashMap();
      HashMap hmDoCnum = new HashMap();

      HashMap hmOrderDetails = new HashMap();
      HashMap hmOrderAttrib = null;
      HashMap hmAccAttrib = null;
      HashMap hmInvAttrib = null;
      String strPayNm = "";
      String strOrdId = "";
      String strAckOrdOn = "";
      String strOriginal = "";
      String strDuplicate = "";
      String subReportPath = "";
      String strPayTermId = "";
      String strInvoiceCompanyId = GmCommonClass.parseNull((String) hmInData.get("CompanyId"));
      // date format getting based on company
      GmCompanyVO compVO = (new GmCommonBean()).getCompanyVO(strInvoiceCompanyId);
      String strApplnDateFmt = compVO.getCmpdfmt();
      // Locale
      GmResourceBundleBean rbPaperWork =
          GmCommonClass.getResourceBundleBean("properties.Paperwork", strCompanyLocale);
      GmResourceBundleBean rbCompany =
          GmCommonClass.getResourceBundleBean("properties.Company", strCompanyLocale);
      int intInvoiceCopySize =
          Integer.valueOf(GmCommonClass.parseZero(rbPaperWork
              .getProperty("INVOICE.GM_INV_PARTNOCNT")));
      java.sql.Date dtOrderDate = null;
      if (hmInvoiceReturn != null) {

        hmOrderDetails =
            GmCommonClass.parseNullHashMap((HashMap) hmInvoiceReturn.get("ORDERDETAILS"));
        hmOrderAttrib =
            GmCommonClass.parseNullHashMap((HashMap) hmInvoiceReturn.get("ORDER_ATTRIB"));
        hmInvAttrib =
            GmCommonClass.parseNullHashMap((HashMap) hmInvoiceReturn.get("INVOICE_ATTRIB"));
        hmAccAttrib =
            GmCommonClass.parseNullHashMap((HashMap) hmInvoiceReturn.get("ACCOUNT_ATTRIB"));
        subReportPath = GmCommonClass.getString("GMJASPERTEMPLATEPATH");
        subReportPath = subReportPath + "invoice";
        /*
         * strAckOrdOn = GmCommonClass.parseNull((String) hmOrderAttrib.get("ACKORDERON"));
         * hmOrderDetails.put("ACKORDERON", strAckOrdOn);
         */
        hmOrderDetails.put("KONTO", "");
        hmOrderDetails.put("EAN", "");
        hmOrderDetails.put("APPLNDATEFMT", strApplnDateFmt);
        hmOrderDetails.put("INVTITLE",
            GmCommonClass.parseNull((String) hmInData.get("INVOICETITLE")));
        alDOControlNum =
            GmCommonClass.parseNullArrayList((ArrayList) hmInvoiceReturn.get("DOCONTROLNUMS"));
        int alSize = alDOControlNum.size();
        // In the below loop , remove the empty control num hashmap value and add another arraylist.
        // Arraylist alDOCNums does not have empty CNUM value.
        for (int i = 0; i < alSize; i++) {
          hmDoCnum = (HashMap) alDOControlNum.get(i);
          if (!GmCommonClass.parseNull((String) hmDoCnum.get("CNUM")).equals("")) {
            alDOCNums.add(hmDoCnum);
          }
        }
        hmOrderDetails.put("ALDOCNUM", alDOCNums);
        hmOrderDetails.put("SUBREPORT_DIR", subReportPath);
        strPayTermId = GmCommonClass.parseNull((String) hmOrderDetails.get("PAY"));
        strPayNm = GmCommonClass.parseNull(rbPaperWork.getProperty("INVOICE." + strPayTermId));
        if (strPayNm.equals("")) {
          strPayNm = GmCommonClass.parseNull((String) hmOrderDetails.get("PAYNM"));
          strPayNm = strPayNm.equals("") ? "Net 30 Days" : strPayNm;
        }
        hmOrderDetails.put("PAYNM", strPayNm);
        hmOrderDetails.put("ID", GmCommonClass.parseNull((String) hmInvoiceReturn.get("ID")));
        hmOrderDetails.put("ORDERDATE", GmCommonClass.getStringFromDate(
            (java.sql.Date) hmInvoiceReturn.get("ORDERDATE"), strApplnDateFmt));
        hmOrderDetails.put("DUEDT", GmCommonClass.getStringFromDate(
            (java.sql.Date) hmOrderDetails.get("DUEDT"), strApplnDateFmt));
        hmOrderDetails.put("ORDERMODE",
            GmCommonClass.parseNull((String) hmInvoiceReturn.get("ORDERMODE")));
        hmOrderDetails.put("CREATEDBY",
            GmCommonClass.parseNull((String) hmInvoiceReturn.get("CREATEDBY")));
        hmOrderDetails.put("SHIPMODE",
            GmCommonClass.parseNull((String) hmInvoiceReturn.get("SHIPMODE")));
        String strAccID = GmCommonClass.parseNull((String) hmOrderDetails.get("ACCID"));
        // 101185 - VAT # For Account
        String strAccountVATNum =
            GmCommonClass.parseNull(GmCommonClass.getAccountAttributeValue(strAccID, "101185"));
        //PC-4535
        String strBillCountry = GmCommonClass.parseNull((String)hmOrderDtls.get("BILLCNTRY"));
        String strCountryRuleValue = GmCommonClass.parseNull(GmCommonClass.getRuleValueByCompany(strBillCountry,"PAPER_WORK_BY_CNTRY",strInvoiceCompanyId));
        if (strCountryRuleValue.equals("Y")) {
        	hmOrderDetails.put("GMBANKNAME",
        			GmCommonClass.parseNull(rbPaperWork.getProperty(strBillCountry+".GMBANKNAME")));
        	hmOrderDetails.put("GMACCOUNTNAME",
        			GmCommonClass.parseNull(rbPaperWork.getProperty(strBillCountry+".GMACCOUNTNAME")));
        	hmOrderDetails.put("GMACCOUNTNUM",
        			GmCommonClass.parseNull(rbPaperWork.getProperty(strBillCountry+".GMACCOUNTNUM")));
        	hmOrderDetails.put("GMSWIFTNUM",
        			GmCommonClass.parseNull(rbPaperWork.getProperty(strBillCountry+".GMSWIFTNUM")));	
        }//PC-4535
        	
        hmOrderDetails.put("ACCOUNTVATNUM", strAccountVATNum);
        
        hmOrderDetails.put("GMCOMPANYNAME",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYNAME")));
        hmOrderDetails.put("GMCOMPANYADDRESS",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYADDRESS")));
        hmOrderDetails.put("GMCOMPANYPHONE",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYPHONE")));
        hmOrderDetails.put("GMCOMPANYFAX",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYFAX")));
       // PMT 35149 put CompanyNIP value in hmOrderDetails instead of hardcoding.
        hmOrderDetails.put("GMCOMPANYNIP",  
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYNIP")));
        hmOrderDetails.put("GMCOMPANYEMAIL",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYEMAIL")));
        hmOrderDetails.put("GMCOMPANYURL",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYURL")));
        hmOrderDetails.put("GMCOMPANYBANK",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYBANK")));
        hmOrderDetails.put("GMCOMPANYIBAN",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYIBAN")));
        hmOrderDetails.put("GMCOMPANYBRANCH",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYBRANCH")));
        hmOrderDetails.put("GMCOMPANYBIC",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYBIC")));
        hmOrderDetails.put("GM_INV_PARTNOCNT", Integer.valueOf(GmCommonClass.parseZero(rbPaperWork
            .getProperty("INVOICE.GM_INV_PARTNOCNT"))));
        hmOrderDetails.put("GMKVKNUMBER",
            GmCommonClass.parseNull(rbPaperWork.getProperty("INVOICE.GMKVKNUMBER")));
        hmOrderDetails.put("GMBTWNUMBER",
            GmCommonClass.parseNull(rbPaperWork.getProperty("INVOICE.GMBTWNUMBER")));
        hmOrderDetails.put("GMTVANUMBER",
            GmCommonClass.parseNull(rbPaperWork.getProperty("INVOICE.GMTVANUMBER")));
        hmOrderDetails.put("GMCOMPANYBANK_ACC_NO",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYBANK_ACC_NO")));
        hmOrderDetails.put("GMCOMPANYBANK_CODE",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYBANK_CODE")));
       // PC-4584 : TVA Update for Luxembourg(101922) customers in GM Belgium(1004)
        if(strBillCountry.equals("101922") && strInvoiceCompanyId.equals("1004")) {
        	 hmOrderDetails.put("GLOBUSVATNUM",
        	            GmCommonClass.parseNull(rbCompany.getProperty("101922.GMCOMPANYBANK_CODE")));
        }else{
        hmOrderDetails.put("GLOBUSVATNUM",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYBANK_CODE")));
        }
        hmOrderDetails.put("GMCOMPANYUID",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYUID")));
        hmOrderDetails.put("GMCOMPANYREGN",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCOMPANYREGN")));
        // Code detail
        hmOrderDetails.put("GMCODEBANQUE",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCODEBANQUE")));
        hmOrderDetails.put("GMCODEGUICHET",
            GmCommonClass.parseNull(rbCompany.getProperty("GMCODEGUICHET")));
        hmOrderDetails.put("GMNUMDECOMPTE",
            GmCommonClass.parseNull(rbCompany.getProperty("GMNUMDECOMPTE")));
        hmOrderDetails.put("GMCLERIB", GmCommonClass.parseNull(rbCompany.getProperty("GMCLERIB")));
        
        strOriginal = GmCommonClass.parseNull((String) hmInvoiceReturn.get("DOCTYPE_ORIGINAL"));
        strDuplicate = GmCommonClass.parseNull((String) hmInvoiceReturn.get("DOCTYPE_COPY"));
        hmOrderDetails.put("ORDERCOMMENTS",
            GmCommonClass.parseNull((String) hmInvoiceReturn.get("ORDERCOMMENTS")));

        hmOrderDetails.put("ORDER_COMMENT",
            GmCommonClass.parseNull((String) hmInvoiceReturn.get("ORDER_COMMENT")));
        hmOrderDetails.put("INV_COMMENTS",
            GmCommonClass.parseNull((String) hmOrderDetails.get("COMMENTS")));
        hmOrderDetails.put("KONTO",
                GmCommonClass.parseNull(rbCompany.getProperty("KONTO")));
        // This above comment value is coming from T501_ORDER.C501_COMMENT colum.
      }
      // This code is to put the Invoice attributes like VAT,PAN,DRUGLIC# into hmOrderDetails.
      if (hmInvAttrib != null && hmInvAttrib.size() > 0) {
        Set atrbSet = hmInvAttrib.keySet();
        String strAtribName = "";
        for (Iterator it = atrbSet.iterator(); it.hasNext();) {
          strAtribName = (String) it.next();
          hmOrderDetails.put(strAtribName, hmInvAttrib.get(strAtribName));
        }
      }
      if (hmOrderAttrib != null && hmOrderAttrib.size() > 0) {
        // Below is for attribute condition checking in india invoice. We need to show either VAT or
        // CST.
        // At the same time we dont have both values.so by default adding both values.
        hmOrderDetails.put("TINNUM", "");
        hmOrderDetails.put("CSTNUM", "");
        Set atrbSet = hmOrderAttrib.keySet();
        String strAtribName = "";
        for (Iterator it = atrbSet.iterator(); it.hasNext();) {
          strAtribName = (String) it.next();
          hmOrderDetails.put(strAtribName, hmOrderAttrib.get(strAtribName));
        }
      }
      if (hmAccAttrib != null && hmAccAttrib.size() > 0) {
        Set atrbSet = hmAccAttrib.keySet();
        String strAtribName = "";
        for (Iterator it = atrbSet.iterator(); it.hasNext();) {
          strAtribName = (String) it.next();
          hmOrderDetails.put(strAtribName, hmAccAttrib.get(strAtribName));
        }
      }
     
      //Get the email address from property file (PC-180) and show to invoice paperwork
      strEmailRemitAddress = GmCommonClass.parseNull(rbCompany.getProperty("COMP_REMIT_TO_EMAIL"));
      hmReturn.putAll(hmOrderDetails);
      hmReturn.put("PARTYFLAG", strPartyFlag);
      hmReturn.put("PARTYNAME", strPartyNm);
      hmReturn.put("HEADERADDRESS", strHeadAddress);
      hmReturn.put("REMITADDRESS", strRemitAddress);
      hmReturn.put("LOGOIMAGE", strImagePath + strLogoImage + ".gif");
      hmReturn.put("REMITMAILADDRESS", strEmailRemitAddress);
    }  
    return hmReturn;
  }

  @Override
  public ArrayList fetchInvoiceArray(HashMap hmInData) {
    // TODO Auto-generated method stub
    ArrayList alResult = new ArrayList();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmInData.get("ALORDERLIST"));
    return alResult;

  }

  @Override
  public String loadOrderItemSQL(String strOrderId, String strAction, GmDataStoreVO gmDataStoreVO)
      throws AppError {
    // TODO Auto-generated method stub
    String strItemSQL = "";
    GmInvoicePrintBean gmInvoicePrintBean = new GmInvoicePrintBean(gmDataStoreVO);
    strItemSQL = gmInvoicePrintBean.loadOrderItemSQL(strOrderId, strAction);
    return strItemSQL;
  }

@Override
public String loadOrderItemListPriceSQL(String strOrderId, String strAction, GmDataStoreVO gmDataStoreVO) {
	return "";
}

}
