package com.globus.common.printwork.invoice.beans;

import java.util.ArrayList;
import java.util.HashMap;
import com.globus.common.beans.AppError;
import com.globus.valueobject.common.GmDataStoreVO;

public interface GmInvoicePrintLayoutInterface {
  public String loadOrderItemLayoutSQL(String strOrderId, String strAction, GmDataStoreVO gmDataStoreVO)
      throws AppError;  
}
