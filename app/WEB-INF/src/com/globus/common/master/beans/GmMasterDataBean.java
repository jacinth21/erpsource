package com.globus.common.master.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.master.GmAccountsVO;
import com.globus.valueobject.common.master.GmAcctGpoMapVO;
import com.globus.valueobject.common.master.GmAssocRepMapVO;
import com.globus.valueobject.common.master.GmDistributorVO;
import com.globus.valueobject.common.master.GmSalesRepVO;
import com.globus.valueobject.logon.GmUserVO;

public class GmMasterDataBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();
  GmLogger log = GmLogger.getInstance();

  // this will be removed all place changed with GmDataStoreVO constructor
  // op changess start
  public GmMasterDataBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmMasterDataBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // op changess end
  /**
   * fetchAccountsDetail
   * 
   * @return ArrayList
   * @method This method will returns Account values while calling web service.The Account Values
   *         will be retrieved based on Active Acount id.
   */
  public List<GmAccountsVO> fetchAccountsDetail(HashMap hmParams) throws AppError {
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess end
    ArrayList alResult = new ArrayList();
    List<GmAccountsVO> listGmAccountVO;
    GmAccountsVO gmAccountsVO = (GmAccountsVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_cm_master_data_info.gm_fch_accounts_info", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmAccountVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmAccountsVO,
            gmAccountsVO.getAccountsProperties());
    gmDBManager.close();
    return listGmAccountVO;
  }

  /**
   * fetchSalesRepsDetail
   * 
   * @return ArrayList
   * @method This method will returns Sales Rep values while calling web service.The Account Values
   *         will be retrieved based on Active Sales Rep id.
   */
  public List<GmSalesRepVO> fetchSalesRepsDetail(HashMap hmParams) throws AppError {
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess end
    ArrayList alResult = new ArrayList();
    List<GmSalesRepVO> listGmSalesRepVO;
    GmSalesRepVO gmSalesRepVO = (GmSalesRepVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_cm_master_data_info.gm_fch_salesrep_info", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmSalesRepVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmSalesRepVO,
            gmSalesRepVO.getSalesRepsProperties());
    gmDBManager.close();
    return listGmSalesRepVO;
  }

  /**
   * fetchDistributorsDetail
   * 
   * @return ArrayList
   * @method This method will returns Distributor values while calling web service.The Account
   *         Values will be retrieved based on Active Distributor id.
   */
  public List<GmDistributorVO> fetchDistributorsDetail(HashMap hmParams) throws AppError {
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess end
    ArrayList alResult = new ArrayList();
    List<GmDistributorVO> listGmDistributorVO;
    GmDistributorVO gmDistributorVO = (GmDistributorVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_cm_master_data_info.gm_fch_distributor_info", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmDistributorVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmDistributorVO,
            gmDistributorVO.getDistributorProperties());
    gmDBManager.close();
    return listGmDistributorVO;
  }

  /**
   * fetchAcctGpoMapDetail
   * 
   * @return ArrayList
   * @method This method will returns Account Group Mapping values while calling web service.The
   *         Account Values will be retrieved based on Active Account id.
   */
  public List<GmAcctGpoMapVO> fetchAcctGpoMapDetail(HashMap hmParams) throws AppError {
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess start
    List<GmAcctGpoMapVO> listGmAcctGpoMapVO;
    GmAcctGpoMapVO gmAcctGpoMapVO = (GmAcctGpoMapVO) hmParams.get("VO");
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_cm_master_data_info.gm_fch_acctgpomap_info", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmAcctGpoMapVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmAcctGpoMapVO,
            gmAcctGpoMapVO.getAcctGpoMappingProperties());
    gmDBManager.close();
    return listGmAcctGpoMapVO;
  }

  /**
   * fetchUserDetail
   * 
   * @return ArrayList
   * @method This method will returns User values while calling web service.The User Values will be
   *         retrieved based on Active User id.
   */
  public ArrayList fetchUserDetail(HashMap hmParams) throws AppError {
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess start
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_cm_master_data_info.gm_fch_user_info", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(5)));
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchASSalesRepDetail
   * 
   * @return ArrayList
   * @method This method will returns Assoc.SalesRep values while calling web service. The device
   *         will be receive the Active Assoc.SalesRep ids to sync.
   */
  public List<GmAssocRepMapVO> fetchAssocRepDetail(HashMap hmParams) throws AppError {
    // op changess start
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // op changess start
    ArrayList alResult = new ArrayList();
    List<GmAssocRepMapVO> listGmAssocRepMapVO;
    GmAssocRepMapVO gmAssocRepMapVO = (GmAssocRepMapVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_cm_master_data_info.gm_fch_assocrep_info", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmAssocRepMapVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmAssocRepMapVO,
            gmAssocRepMapVO.getAssocRepMappingProperties());
    gmDBManager.close();
    return listGmAssocRepMapVO;
  }
}
