package com.globus.common.displaytag.beans;

import java.util.HashMap;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTStatusLogWrapper extends TableDecorator {

	private String strRefID = "";
	private String strTypeNum = "";
	private HashMap hmRow = new HashMap();

	public DTStatusLogWrapper() {
		super();
	}

	public StringBuffer strValue = new StringBuffer();

	public String getREFID() {

		hmRow = (HashMap) this.getCurrentRowObject();
		strRefID = String.valueOf(hmRow.get("REFID"));
		strTypeNum = String.valueOf(hmRow.get("SOURCE")).replaceAll(" ", "");
		strValue.setLength(0);
		strValue.append("  <a href=javascript:fnPrintRef('").append(strRefID).append("','").append(strTypeNum).append("')> ").append(strRefID).append(" </a>");

		return strValue.toString();
	}
}