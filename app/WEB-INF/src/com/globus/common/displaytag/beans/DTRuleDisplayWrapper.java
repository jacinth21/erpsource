package com.globus.common.displaytag.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Date;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;

public class DTRuleDisplayWrapper extends TableDecorator {

	private String strPictureAvailability = "";
	private String strRuleID = "";
	private HashMap hmRow = new HashMap();
	
	public StringBuffer strValue = new StringBuffer();
	
	public String getPICTURE() {

		hmRow = (HashMap) this.getCurrentRowObject();
		strPictureAvailability = GmCommonClass.parseNull(String.valueOf(hmRow.get("PICTURE")));
		strRuleID = GmCommonClass.parseNull(String.valueOf(hmRow.get("ID")));
			
		strValue.setLength(0);
		
		if (strPictureAvailability.equals("Y"))
				{					
					strValue.append("  <a href=javascript:fnOpenPicture('" + strRuleID + "')>  <img src=/images/product_icon.jpg > </a>");
				} 
		return strValue.toString();
	}
	
	public String getID() {

		hmRow = (HashMap) this.getCurrentRowObject(); 
		strRuleID = GmCommonClass.parseNull(String.valueOf(hmRow.get("ID")));
		strValue.setLength(0);		 
		 
		//No need to show image for BBA rules
		if (!strRuleID.equals("RFS") && !strRuleID.equals("CNV") && !strRuleID.equals("BBA"))		
		{
			strValue.append("  <a href= javascript:fnCallRuleSummary('");
			strValue.append(strRuleID);
			strValue.append("')>  <img src=/images/ordsum.gif alt='Rule Summary'> </a>");
		}
		return strValue.toString();
	}
	public String getHOLDTXN()
	{
		hmRow = (HashMap) this.getCurrentRowObject();
		String strHoldTxn = GmCommonClass.parseNull(String.valueOf(hmRow.get("HOLDTXN")));
		if(strHoldTxn.equals("Y"))
			strHoldTxn = "YES";
		else
			strHoldTxn = "NO";
		return strHoldTxn;
	}
	public String getUPDDATE()
	{
		GmDataStoreVO gmDataStoreVO =
		        (GmDataStoreVO) getPageContext().getRequest().getAttribute("gmDataStoreVO");
		    gmDataStoreVO =
		        ((gmDataStoreVO == null) || ((gmDataStoreVO.getCmpdfmt()).equals(""))) ? GmCommonClass
		            .getDefaultGmDataStoreVO() : gmDataStoreVO;
		String  strApplJSDateFmt=GmCommonClass.parseNull((String)getPageContext().getSession().getAttribute("strSessJSDateFmt"));
		String  strApplDateFmt=gmDataStoreVO.getCmpdfmt();		
		String strDateFormat = "MM/dd/yyyy";
		Date UpdateDate = null;	
		String strUpdateDate = "";
		hmRow = (HashMap) this.getCurrentRowObject();
		String strUpdatedDate = "";
		strUpdatedDate = GmCommonClass.parseNull(String.valueOf(hmRow.get("UPDDATE")));
		try{
			 UpdateDate = GmCommonClass.getStringToDate(strUpdatedDate,strDateFormat);
			 strUpdateDate =GmCommonClass.getStringFromDate(UpdateDate,strApplDateFmt) ;  
			}
			catch(Exception e){
				
			}
		return strUpdateDate;
	}
}	 