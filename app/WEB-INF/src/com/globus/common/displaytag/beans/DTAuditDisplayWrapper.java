package com.globus.common.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

public class DTAuditDisplayWrapper extends TableDecorator{

	private DynaBean db ;    
    private String strValue = "";
    
    public String getRVALUE()
    {
        db =  	(DynaBean) this.getCurrentRowObject();     
		 
		strValue = String.valueOf(db.get("RVALUE")).replaceAll("<","&lt");;
		 
 		 
		return strValue ;
       
    }
    
	/**
	 * PC-5504 - this method used to append radio button with reqid
	 * 
	 * @return
	 */
	public String getREQID() {
		String strReqId = "";
		String strDisabled ="";
		db = (DynaBean) this.getCurrentRowObject();
		strReqId = String.valueOf(db.get("REQID"));
		strValue = String.valueOf(db.get("RVALUE")).replaceAll("<", "&lt");
		String strVoidFl = String.valueOf(db.get("VOID_FL"));
		// strCustPO =String.valueOf(db.get("PO"));
		// strStatusFl =String.valueOf(db.get("SFL"));
		if(strVoidFl.equals("Y"))
    	{
    		strDisabled = "disabled";
    	}
		StringBuffer strRadioBtn = new StringBuffer();
		strRadioBtn.append("<input type=\"radio\" value=\"");
		strRadioBtn.append(strReqId);
		strRadioBtn.append("\" id=\"");
		strRadioBtn.append(strReqId);
		strRadioBtn.append(
				"\" name=\"rad\" "+ strDisabled +" onClick=\"javascript:fnSelect('" + strReqId + "','" + strValue + "')\" >&nbsp;");
		strRadioBtn.append(strReqId);
		return strRadioBtn.toString();
	}
}
