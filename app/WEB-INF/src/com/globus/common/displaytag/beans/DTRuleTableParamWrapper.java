package com.globus.common.displaytag.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import org.displaytag.model.TableModel;

import com.globus.common.beans.GmCommonClass;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmRuleParamsSetupForm;

import javax.servlet.jsp.PageContext;

public class DTRuleTableParamWrapper extends TableDecorator{
	
	private String codeGrpId = "";
	private String ruleGrpId = "";
	private String strInputString = "";
	private ArrayList lresult=null;
	private int intCount=0;
	private HashMap hmValues = new HashMap();   
	public StringBuffer sbValue = new StringBuffer();
    private String strLabelNM = "";
    private String strInvSource = "";
    private String strLabelID = "";
    private String strRAID = "";
    private String strCntrlTyp = "";
    private String strCdGrp ="";
    private String strOptKey = "";
    private String strOptValue = "";
    private Iterator alListIter;
	private List alReturn;
	private GmRuleParamsSetupForm gmRuleParamsSetupForm ;
    
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    
    public void init(PageContext pageContext, Object decorated, TableModel tableModel) {
		super.init(pageContext, decorated, tableModel);
		gmRuleParamsSetupForm = (GmRuleParamsSetupForm) getPageContext().getAttribute("frmRuleParamsSetup", PageContext.REQUEST_SCOPE);		
		alReturn = gmRuleParamsSetupForm.getAlReturn();
		strInvSource = gmRuleParamsSetupForm.getStrInvSource();
	}
    
    public String getLABELID() {
    	hmValues =   (HashMap) this.getCurrentRowObject();
		strLabelID = GmCommonClass.parseNull(String.valueOf(hmValues.get("LABELID")));
		if(strLabelID.equals("null"))	strLabelID ="";
		sbValue.setLength(0); 
		sbValue.append(" <input type='hidden' name='hLabelID"+intCount+"' value='");
		sbValue.append(strLabelID+"'>"); 
		return sbValue.toString();
	}
 
    public String getLABELNM() {
    	hmValues =   (HashMap) this.getCurrentRowObject();
		strLabelNM = GmCommonClass.parseNull(String.valueOf(hmValues.get("LABELNM")));
		sbValue.setLength(0);
		sbValue.append("<font style=\"FONT-WEIGHT:bold;font-size:10px;\">");		
		sbValue.append(strLabelNM);
		sbValue.append("</font>");
		return sbValue.toString();
	}
    
    public String getRULVALUE() {
    	hmValues =   (HashMap) this.getCurrentRowObject();
		strRAID = GmCommonClass.parseNull(String.valueOf(hmValues.get("RULVALUE")));
		strLabelID = GmCommonClass.parseNull(String.valueOf(hmValues.get("LABELID")));
		strCntrlTyp = GmCommonClass.parseNull(String.valueOf(hmValues.get("CNTRLTYP")));
		strCdGrp = GmCommonClass.parseNull(String.valueOf(hmValues.get("CDGRP")));
		if(strInvSource.equals("70105")){//Checking For ICS
			if(strLabelID.equals("50212")){//Label ID for Term Of Sale
				strCntrlTyp = "SELECT";//Setting Control as Select to display term of sale column as drop down.
			}	
		}
		HashMap hmStatusList = new HashMap();
		if(strRAID.equals("null"))	strRAID ="";
		sbValue.setLength(0);
		if (strCntrlTyp.equals("")){		
			sbValue.append("&nbsp;:&nbsp;<input type = text size=55 maxlength=255 name = ruleValue");
			sbValue.append(String.valueOf(intCount) );
			sbValue.append(" value='");
			sbValue.append(strRAID);
			sbValue.append("'>");
		}
		else if (strCntrlTyp.equals("SELECT")){
			//sbValue.setLength(0);
			alListIter = alReturn.iterator();
			sbValue.append("&nbsp;:&nbsp;<select name='ruleValue");
			sbValue.append(String.valueOf(intCount));
			sbValue.append("'> ");
			sbValue.append(" <option value='0'>[Choose One] </option>");
			while (alListIter.hasNext()) {
				hmStatusList = (HashMap) alListIter.next();
				strOptKey = (String) hmStatusList.get("CODEID");
				strOptValue = (String) hmStatusList.get("CODENM");
				if (strRAID.equals(strOptKey)) {
					strOptKey += " selected";
				}
				sbValue.append(" <option value= " + strOptKey + " > " + strOptValue + "</option>");
			}
			sbValue.append(" </select>");			
		}
		intCount++;
		return sbValue.toString(); 
	}

}
