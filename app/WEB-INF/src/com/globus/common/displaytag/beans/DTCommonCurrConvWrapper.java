/**
 * FileName : DTCommonCurrConvWrapper.java Description : Author : Himanshu Patel Date : Jan 7,2010
 * Copyright : Globus Medical Inc Modified by : Himanshu - To add method for sorting date
 */
package com.globus.common.displaytag.beans;

import java.sql.Date;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class DTCommonCurrConvWrapper extends TableDecorator {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  private String strCurrId = "";
  private DynaBean db = null;
  private String strClogFlag = "";
  private String strImgSrc;
  String strImagePath = GmCommonClass.getString("GMIMAGES");
  private String strCurrIdFlag = "";
  private String strTransCnt = "";
  private String strSourceID = "";
  private String strFailureFl = "";

  public DTCommonCurrConvWrapper() {
    super();
  }

  public String getCURRID() {
    db = (DynaBean) this.getCurrentRowObject();
    strCurrId = GmCommonClass.parseNull(String.valueOf(db.get("CURRID")));
    StringBuffer sbHtml = new StringBuffer();
    strClogFlag = GmCommonClass.parseNull(String.valueOf(db.get("CLOG")));
    strCurrIdFlag = GmCommonClass.parseNull(String.valueOf(db.get("CURRIDFLAG")));
    strTransCnt = GmCommonClass.parseNull(String.valueOf(db.get("TRANSCNT")));
    strSourceID = GmCommonClass.parseNull(String.valueOf(db.get("SOURCE_ID")));
    strFailureFl = GmCommonClass.parseNull(String.valueOf(db.get("FAILURE_FL")));
    log.debug("strClogFlag : : " + strClogFlag);
    log.debug("strCurrIdFlag : : " + strCurrIdFlag);
    log.debug("strSourceID : : " + strSourceID);
    log.debug("strFailureFl : : " + strCurrIdFlag);

    if (strSourceID.equals("106910")) {
      sbHtml.append("<span style=\"background-color:green;width:50px;\"></span>");
    } else if (strSourceID.equals("106911")) {
      if (strFailureFl.equals("Y")) {
        sbHtml.append("<span style=\"background-color:yellow;width:50px;\"></span>");
      } else {
        sbHtml.append("");
      }

    } else {
      if (!strCurrIdFlag.equals("Y")) {
        sbHtml.append("<a href= javascript:fnEditCurrId('");
        sbHtml.append(strCurrId);
        sbHtml
            .append("');> <img width='39' height='19' title='Click here to edit details' style='cursor:hand' src='/images/edit.jpg'/> </a>");
      } else {
        if (strClogFlag.equals("N")) {
          strImgSrc = strImagePath + "/phone_icon.jpg";
        } else {
          strImgSrc = strImagePath + "/phone-icon_ans.gif";
        }
        log.debug("strImgSrc : : " + strImgSrc);
        sbHtml.append("<a href= javascript:fnCommentCurrId('");
        sbHtml.append(strCurrId);
        sbHtml
            .append("');> <img width='35' height='19' title='Click here to add/see the comments' style='cursor:hand' src="
                + strImgSrc + " /> </a>");
      }

    }
    return sbHtml.toString();
  }

  // To Convert the String to Date so do the sorting according to Date
  public Date getCURRFROMDT() {
    Date dtCurrFrom = null;
    db = (DynaBean) this.getCurrentRowObject();
    if (db.get("DTCURRFROMDT") != null) {
      dtCurrFrom = new Date(((java.util.Date) db.get("DTCURRFROMDT")).getTime());
    }

    return dtCurrFrom;
  }

  // To Convert the String to Date so do the sorting according to Date
  public Date getDTCURRTODT() {
    Date dtCurrTo = null;
    db = (DynaBean) this.getCurrentRowObject();
    if (db.get("DTCURRTODT") != null) {
      dtCurrTo = new Date(((java.util.Date) db.get("DTCURRTODT")).getTime());
    }


    return dtCurrTo;
  }

  // To enable window opener to show transaction details
  public String getTRANSCNT() {
    db = (DynaBean) this.getCurrentRowObject();
    strCurrId = GmCommonClass.parseNull(String.valueOf(db.get("CURRID")));
    strTransCnt = GmCommonClass.parseNull(String.valueOf(db.get("TRANSCNT")));

    return "<a href= javascript:fnOpenTransDtls('" + strCurrId + "')>" + strTransCnt + "</a>";
  }



}
