package com.globus.common.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

public class DTRuleRptWrapper extends TableDecorator{

    
    private DynaBean db ;    
    private int intRuleId = 0; 
    private String strRuleName = "";
    
    public String getNAME()
    {
        db =  	(DynaBean) this.getCurrentRowObject();     
		intRuleId = Integer.parseInt(String.valueOf(db.get("ID"))); 
		strRuleName = String.valueOf(db.get("NAME"));
		StringBuffer strValue = new StringBuffer();
		
		strValue.append("&nbsp; <a href= javascript:fnCallRuleSummary('");
		strValue.append(intRuleId);
		strValue.append("')>  <img src=/images/ordsum.gif     alt='Rule Summary'> </a>");
		
		strValue.append("  <a href= javascript:fnCallRuleEdit('");
		strValue.append(intRuleId);
		strValue.append("')>  <img src=/images/edit.jpg    title='Click to Edit' alt='Rule Edit'> </a>");
		
        strValue.append("&nbsp;  "); 
 		strValue.append( strRuleName);  
     	
		return strValue.toString();
       
    }
   
    
}
