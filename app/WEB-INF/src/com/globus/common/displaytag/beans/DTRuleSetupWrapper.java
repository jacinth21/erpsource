/**
 * FileName    : DTRuleSetupWrapper.java 
 * Description :
 * Author      : rshah
 * Date        : Jun 3, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.displaytag.beans;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author rshah
 *
 */
public class DTRuleSetupWrapper extends TableDecorator{
	private HashMap hmParam = new HashMap();
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	Logger log = GmLogger.getInstance(this.getClass().getName());
	String strRes = "";
	private String strPicture = "";
	
	public DTRuleSetupWrapper() {
		super();
	}
	public String getRULECONSQID()  
	{
		hmParam = (HashMap)this.getCurrentRowObject();
		String strSeq = hmParam.get("RULECONSQID").toString();
		String strRuleId = hmParam.get("RULEID").toString();
		StringBuffer strBufVal = new StringBuffer();
		strBufVal.append("<img style='cursor:hand' src='");
		strBufVal.append(strImagePath+"/edit.jpg'");
   		strBufVal.append(" title='Click to Edit' width='39' height='19'" ); 
	   	strBufVal.append(" onClick =fnEdtSeq('"+strSeq+"','"+strRuleId+"');> </img> ");
   		strRes = strBufVal.toString(); 
		
		//return  "<a href= javascript:fnViewInv('" + strPO + "','" + strId + "')>" + "Y" + "</a>" ; ,'" +strSessNm+ "','" + 2 + "','" + 2 + "','" + 2 + "','" + 2 + "' ----  "','"+strSessNm+"','"+strSessStartDt+"','"+strSessEndDt+"','"+strSessOwner+"','"+strSessStatus+
 		return strRes;
	}
	
	public String getPICTURE() {

		hmParam = (HashMap) this.getCurrentRowObject();
		strPicture = GmCommonClass.parseNull(String.valueOf(hmParam.get("PICTURE")));
		String strRuleID = GmCommonClass.parseNull(String.valueOf(hmParam.get("RULEID")));
		String strFileID = GmCommonClass.parseNull(String.valueOf(hmParam.get("FILEID")));	
		
		StringBuffer strBufVal = new StringBuffer();
		
		if (!strPicture.equals(""))
				{					
				strBufVal.append("  <a href=/GmCommonFileOpenServlet?uploadString=CONSUPLOADHOME&sId="+ strFileID +">"+strPicture+"</a>");
				} 
		return strBufVal.toString();
	}
	
}
