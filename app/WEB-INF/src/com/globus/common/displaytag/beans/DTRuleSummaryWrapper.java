package com.globus.common.displaytag.beans;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

public class DTRuleSummaryWrapper extends TableDecorator{

    
    private DynaBean db ;    
    private int intCount = 0; 
    private String strRuleCon = "";
    
    public String getCONDITIONS()
    {
        db =  	(DynaBean) this.getCurrentRowObject();     
		 
		strRuleCon = String.valueOf(db.get("CONDITIONS"));
		StringBuffer strValue = new StringBuffer();
		
		if(intCount > 0) strRuleCon = "AND " + strRuleCon; 
		 
 		strValue.append( strRuleCon);  
 		intCount++;
		return strValue.toString();
       
    }
   
    
}
