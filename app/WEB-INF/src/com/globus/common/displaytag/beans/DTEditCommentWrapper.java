package com.globus.common.displaytag.beans;

import java.math.BigDecimal;

import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;

import com.globus.common.beans.GmCommonClass;

public class DTEditCommentWrapper extends TableDecorator {
	private DynaBean db;
	private String strLogID = null;
	String strImagePath = GmCommonClass.getString("GMIMAGES");
	 
	public StringBuffer strValue = new StringBuffer();
	private int intCount = 0;
	private String strImgSrc="";
	private String strImgSrc1="";
	public DTEditCommentWrapper() {
		super();
	}

	public String getLOGID() {
		db = (DynaBean) this.getCurrentRowObject();
		strLogID = GmCommonClass.parseNull((BigDecimal) db.get("LOGID")+"");
		System.out.println("::::LOGID:::At Wrapper::"+strLogID);
		strImgSrc=strImagePath+"/edit_icon.gif";
		strValue.setLength(0);
		
		strValue.append("<a href=javascript:fnEditComment('");  //fnEditComment editcomment
		strValue.append(strLogID);
		strValue.append("')>");
		
		strValue.append("<img id='imgEdit' style='cursor:hand' name='LOGID' title='Click to add comments'   src='");
		strValue.append(strImgSrc );
		strValue.append("' > ");
		strValue.append("</img> </a>");
		intCount++;
		return strValue.toString();
	}

	public String getAUDIT_LOG() {
		
		String strAuditLog="";
		
		db = (DynaBean) this.getCurrentRowObject();
		strAuditLog = GmCommonClass.parseNull((String) db.get("AUDIT_LOG"));
		
		strValue.setLength(0);
		if(strAuditLog.equals("Y")){
			strImgSrc=strImagePath+"/icon_History.gif";			
			
			strValue.append("<a href=javascript:fnLoadAuditTrail('");  
			strValue.append(strLogID);
			strValue.append("','902')>");
			
			strValue.append("<img id='auditTrail' style='cursor:hand' name='AUDITTRAIL' title='Click to View History'   src='");
			strValue.append(strImgSrc );
			strValue.append("' > ");
			strValue.append("</img> </a>");
			intCount++;
		}
			return strValue.toString();
	}
		
		public String getVOID() {
			db = (DynaBean) this.getCurrentRowObject();
			strLogID = GmCommonClass.parseNull((BigDecimal) db.get("LOGID")+"");
			System.out.println("::::LOGID:::At Wrapper::"+strLogID);
			strImgSrc1=strImagePath+"/void.jpg";
			//String strVoidimage="images/void.jpg";
			strValue.setLength(0);
		
		strValue.append("<a href=javascript:fnVoid('"+strLogID+"')>");
		strValue.append("<img id='imgEdit1' style='cursor:hand' name='VOID' title='Click to void comments'   src='");
		strValue.append(strImgSrc1);
		strValue.append("' > ");
		strValue.append("</img> </a>");
		
		intCount++;
		return strValue.toString();
	}

}