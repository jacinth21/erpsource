package com.globus.common.displaytag.beans;

import org.apache.commons.lang.math.NumberUtils;
import org.displaytag.model.DefaultComparator;
	
	public class DTComparator extends DefaultComparator {
		 
		 public int compare(Object object1, Object object2) {
		    int returnValue = 0;
		    	
		    if((object1 instanceof Comparable) && (object2 instanceof
		Comparable))
		     {
		    	 if((object1 instanceof String) &&  (object2 instanceof String))
		    	 {
		    		  String str1 = String.valueOf(object1);
		    		  String str2 = String.valueOf(object2);

		    		  if((str1.indexOf("<a href") != -1 && str2.indexOf("<a href") != -1))  
		    		   {
			    		  str1 = str1.substring(0,str1.lastIndexOf(">") -1);
			    		  str2 = str2.substring(0,str2.lastIndexOf(">")- 1);		    		
			    		  str1 = str1.substring(str1.lastIndexOf(">") +1, str1.lastIndexOf("<"));
			    		  str2 = str2.substring(str2.lastIndexOf(">")+1, str2.lastIndexOf("<"));
		    		   }
		    		   else if((str1.indexOf("<input") != -1 && str2.indexOf("<input") != -1))
		    		   {
		    			   	  str1 = str1.substring(str1.lastIndexOf(">"));
				    		  str2 = str2.substring(str2.lastIndexOf(">"));
		    		   }
		    		  returnValue = compare(str1, str2);
		    	 }		
		         else
		         {
		             returnValue = ((Comparable)object1).compareTo(object2);
		         }
		     } else
		     if(object1 == null && object2 == null)
		         returnValue = 0;
		     else
		     if(object1 == null && object2 != null)
		         returnValue = 1;
		     else
		     if(object1 != null && object2 == null)
		         returnValue = -1;
		     else
		         returnValue = object1.toString().compareTo(object2.toString());
		    return returnValue;
		 }
		 
		 private int compare(String object1, String object2)
		 {
			 if((object1 instanceof String) && NumberUtils.isNumber((String)object1)
						&& (object2 instanceof String) && NumberUtils.isNumber((String)object2))
						         {						        	
						             Double d1 = new Double((String)object1);
						             Double d2 = new Double((String)object2);	
						             return d1.compareTo(d2);	
						         }
			 return object1.compareTo(object2);	 
		 }
		  				 
		}

