package com.globus.common.db;

import java.sql.SQLException;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmLogger;

public class NamingSQLException extends SQLException
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    
    public NamingSQLException(Exception ex)
    {
        if(ex instanceof NamingException)
        {
          log.error("Naming Exception Happened:"+ex.getMessage());
        }
    }
}
