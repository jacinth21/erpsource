/**
 * 
 */
package com.globus.common.db;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hornetq.utils.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;
import redis.clients.jedis.Tuple;
import redis.clients.jedis.exceptions.JedisException;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
public class GmCacheManager extends GmDBManager {

  private Jedis jedisCache = null;
  private Jedis jedisWriteCache = null;
  private Set<String> keySet = null;
  private boolean isFirstTime = true;
  private boolean isRedisActive = false;
  private ArrayList alReturn = new ArrayList();
  private String strKey = "";
  private String strSearchPrefix = "";
  private String strSearchType = "";
  private final String strIDColumnName = "ID";
  private final String strValueColumnName = "NAME";
  private final String strValueColumnAltName = "ALTNAME";
  private static final String strSlaveServerName = System.getProperty("ENV_REDIS_SLAVE_HOST");
  private static final String strSlaveServerPort = GmCommonClass.getString("REDIS_SLAVE_PORT");
  private static final String strJedisPass = System.getProperty("REDIS_PASS");

  private final int slaveServerPort = Integer.parseInt(strSlaveServerPort);
  private static final String strMasterServerName = GmCommonClass
      .getString("REDIS_MASTER_HOST_NAME");
  private static final String strMasterServerPort = GmCommonClass.getString("REDIS_MASTER_PORT");
  private final int masterServerPort = Integer.parseInt(strMasterServerPort);
  private int iMaxFetchCount = -1;


  /**
   * Creating new Jedis object to write data in master server.
   */
  public void GmWriteCacheManager() {
    jedisWriteCache = new Jedis(strMasterServerName, masterServerPort);
    try {
    	log.debug("strJedisPass in GmWriteCacheManager"+strJedisPass);
    	jedisWriteCache.auth(strJedisPass);
      } 
    catch (Exception e) {
    	log.error("Get jedis error : " + e);

      }
    isRedisActive = true;
    // ensureThatWeCanConnectToRedis();
  }

  /**
   * @param gmDataStoreVO
   */
  public GmCacheManager(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
    jedisCache = new Jedis(strSlaveServerName, slaveServerPort);
    try {
    	log.debug("strJedisPass in GmCacheManager"+strJedisPass);
    	jedisCache.auth(strJedisPass);
    	log.debug("Jedis Auth status"+jedisCache.auth(strJedisPass));
      } 
    catch (Exception e) {
    	log.error("Get jedis error : " + e);

      }
    isRedisActive = true;
    // ensureThatWeCanConnectToRedis();
  }

  /**
   * @param enDNSName_
   * @param gmDataStoreVO
   */
  public GmCacheManager(GmDNSNamesEnum enDNSName_, GmDataStoreVO gmDataStoreVO) {
    super(enDNSName_, gmDataStoreVO);
    jedisCache = new Jedis(strSlaveServerName, slaveServerPort);
    jedisCache = new Jedis(strSlaveServerName, slaveServerPort);
    try {
    	log.debug("strJedisPass in GmCacheManager1"+strJedisPass);

    	jedisCache.auth(strJedisPass);
      } 
    catch (Exception e) {
    	log.error("Get jedis error : " + e);

      }
    isRedisActive = true;
    // ensureThatWeCanConnectToRedis();
  }

  /**
   * @param enDNSName_
   */
  public GmCacheManager(GmDNSNamesEnum enDNSName_) {
    super(enDNSName_);
    jedisCache = new Jedis(strSlaveServerName, slaveServerPort);
    try {
    	log.debug("strJedisPass in GmCacheManager2"+strJedisPass);

    	jedisCache.auth(strJedisPass);
      } 
    catch (Exception e) {
    	log.error("Get jedis error : " + e);

      }
    isRedisActive = true;
    // ensureThatWeCanConnectToRedis();
  }

  /**
   * 
   */
  public GmCacheManager() {
    jedisCache = new Jedis(strSlaveServerName, slaveServerPort);
    try {
    	log.debug("strJedisPass in GmCacheManager3"+strJedisPass);

    	jedisCache.auth(strJedisPass);
      } 
    catch (Exception e) {
    	log.error("Get jedis error : " + e);

      }
    isRedisActive = true;
    // ensureThatWeCanConnectToRedis();
  }


  /**
   * @return
   */
  public String getStrSearchPrefix() {
    return strSearchPrefix;
  }

  /**
   * @param strSearchPrefix
   */
  public void setSearchPrefix(String strSearchPrefix) {
    strSearchPrefix = strSearchPrefix.toLowerCase();
    this.strSearchPrefix = strSearchPrefix;
  }

  /**
   * @return
   */
  public int getMaxFetchCount() {
    return iMaxFetchCount;
  }

  /**
   * @param iMaxFetchCount
   */
  public void setMaxFetchCount(int iMaxFetchCount) {
    this.iMaxFetchCount = iMaxFetchCount;
  }



  /**
   * 
   */
  public void ensureThatWeCanConnectToRedis() {
    try {
      jedisCache.ping();
      isRedisActive = true;
    } catch (JedisException ex) {
      GmLogError.log("Exception in GmCacheManager ",
          "Exception is:" + GmCommonClass.getExceptionStackTrace(ex, "\n"));
    }
  }

  /**
   * @return
   */
  private String getKey() {
    return strKey;
  }

  /**
   * @param strKey
   */
  public void setKey(String strKey) {
    this.strKey = strKey;
  }

  /**
   * @return the strSearchType
   */
  public String getStrSearchType() {
    return strSearchType;
  }

  /**
   * @param strSearchType the strSearchType to set
   */
  public void setStrSearchType(String strSearchType) {
    this.strSearchType = strSearchType;
  }

  @Override
  public Object getObject(int intResultSetIndex) throws AppError {
    if (isFirstTime)
      return super.getObject(intResultSetIndex);
    else
      return null;
  }

  @Override
  public String getString(int intResultSetIndex) throws AppError {
    if (isFirstTime)
      return super.getString(intResultSetIndex);
    else
      return "";
  }

  @Override
  public int getInt(int intResultSetIndex) throws AppError {
    if (isFirstTime)
      return super.getInt(intResultSetIndex);
    else
      return -1;
  }



  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return ArrayList of HashMaps containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */
  @Override
  public ArrayList returnArrayList(ResultSet rs) throws AppError {

    if (isFirstTime) {
      // If Firsttime, fetch from Database
      alReturn = super.returnArrayList(rs);
      // WRITE THE RESULT TO CACHE AS THIS IS THE FIRST TIME THIS DATA SET IS ACCESSED
      if (isRedisActive) {
        try {
          writeToCache(alReturn, strSearchPrefix);
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
    if (isRedisActive) {
      // READ FROM CACHE
      try {
        alReturn = readFromCacheAsList(strKey, strSearchPrefix, iMaxFetchCount);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (ClassNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    return alReturn;
  } // END of method returnArrayList


  /**
   * @param rs
   * @return
   * @throws AppError
   */
  public String returnJsonString(ResultSet rs) throws AppError {
    if (isRedisActive)
      checkCacheAvailability();
    String strReturn = "";

    if (isFirstTime) {
      // If Firsttime, fetch from Database
      alReturn = super.returnArrayList(rs);
      // WRITE THE RESULT TO CACHE AS THIS IS THE FIRST TIME THIS DATA SET IS ACCESSED
      if (isRedisActive) {
        try {
          writeToCache(alReturn, strSearchPrefix);
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }
    if (isRedisActive) {
      // READ FROM CACHE
      try {
        strReturn = readFromCacheAsString(strKey, strSearchPrefix, iMaxFetchCount);
      } catch (Exception ex) {
        ex.printStackTrace();
        super.execute();
        alReturn = super.returnArrayList(rs);
      }
    }

    strReturn =
        (strReturn != null && !strReturn.equals("")) ? strReturn : JSONArray.toJSONString(alReturn);

    return strReturn;
  }// END of method returnArrayList

  /**
   * @param strQuery
   * @return
   * @throws AppError
   */
  public String returnJsonString(String strQuery) throws AppError {
    if (isRedisActive)
      checkCacheAvailability();
    String strReturn = "";
    if (isFirstTime) {
      // If Firsttime, fetch from Database
      alReturn = super.queryMultipleRecords(strQuery);     
      // WRITE THE RESULT TO CACHE AS THIS IS THE FIRST TIME THIS DATA SET IS ACCESSED
      if (isRedisActive) {
        try {
          writeToCache(alReturn, strSearchPrefix);
          isFirstTime = false;
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
    }
    if (isRedisActive) {
      // READ FROM CACHE
      try {
        strReturn = readFromCacheAsString(strKey, strSearchPrefix, iMaxFetchCount);
      } catch (Exception ex) {
        ex.printStackTrace();
        super.execute();
        alReturn = super.queryMultipleRecords(strQuery);
      }
    }
    strReturn =
        (strReturn != null && !strReturn.equals("")) ? strReturn : JSONArray.toJSONString(alReturn);
    return strReturn;
  } // END of method returnArrayList

  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return ArrayList of HashMaps containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   * @throws IOException
   */
  @Override
  public ArrayList queryMultipleRecords(String astrQuery) throws AppError {
    if (isRedisActive)
      checkCacheAvailability();
    if (isFirstTime) {
      // If Firsttime, fetch from Database
      alReturn = super.queryMultipleRecords(astrQuery);
      // WRITE THE RESULT TO CACHE AS THIS IS THE FIRST TIME THIS DATA SET IS ACCESSED
      if (isRedisActive) {
        try {

          try {
            writeToCache(alReturn, strSearchPrefix);
            isFirstTime = false;
          } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        } catch (JedisException ex) {
          log.debug(" Error in queryMultipleRecords :: " + ex.toString());
          log.error("Not able to write cache for key " + getKey());
        }
      }
    }
    if (isRedisActive) {
      // READ FROM CACHE
      try {
        try {
          alReturn = readFromCacheAsList(strKey, strSearchPrefix, iMaxFetchCount);
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (ClassNotFoundException e) {
          // TODO Auto-generated catch block
          log.debug(" Error in queryMultipleRecords - class not found :: " + e.toString());
          e.printStackTrace();
        }
      } catch (JedisException ex) {
        log.debug(" Error in queryMultipleRecords :: jdesis  " + ex.toString());
      }
    }


    return alReturn;
  } // END of method returnArrayList

  /**
   * @param strKey
   * @param strPrefix
   * @param iCount
   * @return
   * @throws AppError
   */
  private List returnAutoCompleteList(String strKey, String strPrefix, int iCount) throws AppError {
    // 1. Find the position of the prefix in the sorted set in Redis
    if (null == strPrefix) {
      return Collections.emptyList();
    }
    int prefixLength = strPrefix.length();
    Long start = jedisCache.zrank(strKey, strPrefix);
    if (start < 0 || prefixLength == 0) {
      return new ArrayList();
    }
    List<Map> results = new ArrayList<Map>();
    int found = 0, rangeLength = 50, maxNeeded = iCount;
    while (found < maxNeeded) {
      Set<String> rangeResults = jedisCache.zrange(strKey, start, start + rangeLength - 1);
      start += rangeLength;
      if (rangeResults.isEmpty()) {
        break;
      }
      for (final String entry : rangeResults) {
        int minLength = Math.min(entry.length(), prefixLength);
        if (!entry.substring(0, minLength).equalsIgnoreCase(strPrefix.substring(0, minLength))) {
          maxNeeded = results.size();
          break;
        }
        if (entry.endsWith(GmAutoCompleteReportBean.strAutoSearchDelimiter)
            && results.size() < maxNeeded) {
          String strAcc = entry.substring(0, entry.length() - 1);     
          StringTokenizer stringTokenizer = new StringTokenizer(strAcc, ":");
          stringTokenizer.nextToken();
                 
          Map map = new HashMap();
          map.put(strValueColumnName, stringTokenizer.nextToken());
          map.put(strIDColumnName, stringTokenizer.nextToken());

          results.add(map);
        }
      }
    }  
    return results;
  }


  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return ArrayList of HashMaps containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */

  /**
   * @param strKey
   * @param strPrefix
   * @param iCount
   * @return
   * @throws AppError
   */
  private String returnAutoCompleteJSONString(String strKey, String strPrefix, int iCount)
      throws AppError {
    // 1. Find the position of the prefix in the sorted set in Redis
    Long start = null;
    int inDelimiterLen = GmAutoCompleteReportBean.strAutoSearchDelimiter.length();
    inDelimiterLen = inDelimiterLen - 1;
    String strIds = ""; 
    if (null == strPrefix) {
      return "";
    }
    int prefixLength = strPrefix.length();
    try {
      start = jedisCache.zrank(strKey, strPrefix);

      if (start == null) {
        start = (long) -1;
      }
    } catch (Exception ex) {
      log.debug("OOPS...." + ex.getMessage());
    }

    if (start < 0 || prefixLength == 0) {
      return "";
    }
    List<JSONObject> results = new ArrayList<JSONObject>();
    int found = 0, rangeLength = 50, maxNeeded = iCount;
    while (found < maxNeeded) {
      Set<String> rangeResults = jedisCache.zrange(strKey, start, start + rangeLength - 1);
      start += rangeLength;
      if (rangeResults.isEmpty()) {
        break;
      }
      for (final String entry : rangeResults) {
        int minLength = Math.min(entry.length(), prefixLength);
        if (!entry.substring(0, minLength).equalsIgnoreCase(strPrefix.substring(0, minLength))) {
          maxNeeded = results.size();
          break;
        }
        if (entry.endsWith(GmAutoCompleteReportBean.strAutoSearchDelimiter)
            && results.size() < maxNeeded) {
          String strAcc = entry.substring(0, entry.length() - 1);       
          StringTokenizer stringTokenizer = new StringTokenizer(strAcc, ":");
          stringTokenizer.nextToken();  
          JSONObject jsonObject = new JSONObject();
          try {
            jsonObject.put("NAME", stringTokenizer.nextToken());
            strIds = stringTokenizer.nextToken();       
            if (!strIds.equals(""))
              strIds = strIds.substring(0, strIds.length() - inDelimiterLen);
            jsonObject.put("ID", strIds);
            results.add(jsonObject);
          } catch (Exception ex) {
            log.debug("returnAutoCompleteJSONString...." + ex.getMessage());
          }

        }
      }
    }
    return JSONArray.toJSONString(results);
  }

  @Override
  public void close() {
    super.close();
    jedisCache.close();
  }

  /**
   * @throws AppError
   */
  private void checkCacheAvailability() throws AppError {
    String strKey = getKey();
    try {
      keySet = jedisCache.keys(strKey);
    } catch (JedisException ex) {
      isRedisActive = false;
      log.debug(" Error in checkCacheAvailability :: " + ex.toString());
    }

    if (keySet != null && keySet.size() > 0) {
      isFirstTime = false;
    }
  }

  @Override
  public void execute() throws AppError {
    // setKey("");
    if (isRedisActive)
      checkCacheAvailability();

    if (isFirstTime) {
      super.execute();
    }
  }

  /**
   * @param alList
   * @throws IOException
   */
  private void serializeListToCache(ArrayList alList) throws AppError, IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    ObjectOutputStream out = new ObjectOutputStream(baos);
    GmDataCarrier gmDataCarrier = new GmDataCarrier();
    gmDataCarrier.setAlList(alList);
    out.writeObject(gmDataCarrier);
    byte[] byteValue = baos.toByteArray();
    // initialize jedis object to write data in master server
    GmWriteCacheManager();
    if (isRedisActive)
      jedisWriteCache.set(getKey().getBytes(), byteValue);
  }


  /**
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   */
  private ArrayList deSerializeListFromCache() throws IOException, ClassNotFoundException {
    try {
      ByteArrayInputStream bais = new ByteArrayInputStream(jedisCache.get(getKey().getBytes()));
      ObjectInputStream oin = new ObjectInputStream(bais);
      GmDataCarrier gmDataCarrier = (GmDataCarrier) oin.readObject();
      alReturn = gmDataCarrier.getAlList();
    } catch (JedisException ex) {
      log.debug(" Error in deserila list :: " + ex.getMessage());
    }

    return alReturn;
  }

  /**
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   */
  private String deSerializeJSONStringFromCache() throws IOException, ClassNotFoundException {
    ByteArrayInputStream bais = new ByteArrayInputStream(jedisCache.get(getKey().getBytes()));
    ObjectInputStream oin = new ObjectInputStream(bais);
    GmDataCarrier gmDataCarrier = (GmDataCarrier) oin.readObject();
    return JSONArray.toJSONString(gmDataCarrier.getAlList());
  }

  /**
   * @param alList
   * @param strSearchPrefix
   * @throws IOException
   */
  private void writeToCache(ArrayList alList, String strSearchPrefix) throws IOException {
    // Write the result in the cache server depending on user input for Auto Complete / serialize
    // and store
    // serialize and store if search prefix is not available
    if (strSearchPrefix == null || strSearchPrefix.equals("")) {
      serializeListToCache(alList);
    } else if (strSearchPrefix != null && !strSearchPrefix.equals("")) {
      writeAutoCompleteToCache(alList);
    }
  }


  /**
   * @param strKey
   * @param strSearchPrefix
   * @param iMaxFetchCount
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   */
  private ArrayList readFromCacheAsList(String strKey, String strSearchPrefix, int iMaxFetchCount)
      throws IOException, ClassNotFoundException {
    ArrayList alList = new ArrayList();    
    // if search prefix is not available and not first time, check and deserialize the result from
    // cache
    if (strSearchPrefix == null || strSearchPrefix.equals("")) {
      alList = deSerializeListFromCache();
    }
    // if searchprefix available, search from cache and return only the subset
    else if (strSearchPrefix != null && !strSearchPrefix.equals("")) {
      alList = (ArrayList) returnAutoCompleteList(strKey, strSearchPrefix, iMaxFetchCount);
    }
    return alList;
  }

  /**
   * @param strKey
   * @param strSearchPrefix
   * @param iMaxFetchCount
   * @return
   * @throws IOException
   * @throws ClassNotFoundException
   */
  private String readFromCacheAsString(String strKey, String strSearchPrefix, int iMaxFetchCount)
      throws IOException, ClassNotFoundException {
    log.debug("strSearchType>>>>>>>>" + strSearchType);
    String strReturn = "";
    // if search prefix is not available and not first time, check and deserialize the result from
    // cache
    if (strSearchPrefix == null || strSearchPrefix.equals("")) {
      strReturn = deSerializeJSONStringFromCache();
    }
    // if searchprefix available, search from cache and return only the subset
    else {
      strReturn = returnAutoCompleteLikeSearchJSON(strKey, strSearchPrefix, iMaxFetchCount);
    }
    return strReturn;
  }



  /**
   * @param alList
   */
  private void writeAutoCompleteToCache(ArrayList alList) {

    Iterator alIterator = alList.iterator();
    double dmultiplier = 1000;
    // double dScore = Math.abs((Math.random() * dmultiplier));
    double dScore = dmultiplier;
    // initialize jedis object to write data in master server
    GmWriteCacheManager();
    while (alIterator.hasNext()) {
      dScore = dScore + 1;
      Map m = (Map) alIterator.next();
      String strID = (String) m.get(strIDColumnName);
      String strName = (String) m.get(strValueColumnName);
      String strAltName = (String) m.get(strValueColumnAltName);  
      String strWord = preprareStringToBeAdded(strID, strName, strAltName);  
      if (!strWord.isEmpty() && !strWord.startsWith("#")) {
        addWord(strWord, dScore);
      }
    }

  }

  /**
   * @param word
   * @param dScore
   */
  private void addWord(final String word, double dScore) {
    // Add all the possible prefixes of the given word and also the given
    // word with a * suffix.
    jedisWriteCache.zadd(strKey, dScore, word + GmAutoCompleteReportBean.strAutoSearchDelimiter);
  }


  /**
   * @param strID
   * @param strName
   * @return
   */
  private String preprareStringToBeAdded(String strID, String strName, String strAltName) {	  
	  if(GmCommonClass.parseNull(strAltName).equals("")){
		  return strName.toLowerCase() + ":" + strName + ":" + strID;
	  }	  
	  return strAltName.toLowerCase() + ":" + strAltName + ":" + strName.toLowerCase() + ":" + strName + ":" + strID;
  }
  
  /**
   * @param strID
   * @param strName
   * @return
   */
  private String preprareStringToBeAdded(String strID, String strName) {
    return strName.toLowerCase() + ":" + strName + ":" + strID;
  }

  /**
   * @param strKey
   * @param strKeyToBeRemoved
   * @param strKeyToBeAdded
   * @param strValueToBeAdded
   */
  public String updateSortedSetInCache(String strKey, String strKeyToBeRemoved,
      String strKeyToBeAdded, String strValueToBeAdded) {
    String strNewDataFl = "";
    // initialize jedis object to write data in master server
    GmWriteCacheManager();
    log.debug(" Key " + strKey + " Old Name " + strKeyToBeRemoved + " New Name " + strKeyToBeAdded);
    setKey(strKey);
    checkCacheAvailability();
    if (!isFirstTime && isRedisActive) {
      String strRemoveWord = preprareStringToBeAdded(strValueToBeAdded, strKeyToBeRemoved);
      Double drem =
          jedisWriteCache.zscore(strKey, strRemoveWord
              + GmAutoCompleteReportBean.strAutoSearchDelimiter);
      drem = GmCommonClass.parseDouble(drem);
      log.debug(" drem.doubleValue() " + drem.doubleValue());
      if (drem.doubleValue() != 0.0) {
        removeWord(strKey, strRemoveWord, drem.doubleValue());
        String strAddWord = preprareStringToBeAdded(strValueToBeAdded, strKeyToBeAdded);
        addWord(strAddWord, drem.doubleValue());
      } else {
        strNewDataFl = "Y";
        jedisWriteCache.del(strKey);
      }
    }
    return strNewDataFl;
  }

  /**
   * @param strKey
   * @param strRemoveWord
   * @param remVal
   */
  private void removeWord(String strKey, String strRemoveWord, double remVal) {
    jedisWriteCache.zrem(strKey, strRemoveWord + GmAutoCompleteReportBean.strAutoSearchDelimiter);
    // jedisCache.zremrangeByScore(strKey, remVal, remVal);
  }

  /**
   * @param strKey
   * @return
   * @throws AppError
   */
  private Double getAvailableScoreVal(String strKey) throws AppError {
    Set<String> lastElement = jedisCache.zrange(strKey, -1, -1);
    Iterator<String> itr = lastElement.iterator();
    Double dAvail = null;
    if (itr.hasNext()) {
      String strLastElement = itr.next();
      log.debug("strLastElement " + strLastElement);
      dAvail = jedisCache.zscore(strKey, strLastElement);
      log.debug(" my Values " + dAvail.doubleValue());
    }
    return dAvail;
  }

  /**
   * @param strKey
   * @param strRemoveKey
   */
  public void removeKey(String strKey) {
    GmWriteCacheManager();
    jedisWriteCache.del(strKey);
  }

  /**
   * To set expiry time for the Redis key
   * 
   * @param strKey
   * @param strRemoveKey
   */
  public void setExpireTime(String strKey, int intSecondds) {
    GmWriteCacheManager();
    jedisWriteCache.expire(strKey, intSecondds);
  }

  /**
   * removePatternKeys - This method used to delete the keys based on pattern search
   * 
   * @param strPattern
   * @throws AppError
   */
  public void removePatternKeys(String strPattern) throws AppError {
    // to check the Redis server availability
    checkCacheAvailability();
    // Redis server active then, remove the keys otherwise return
    if (isRedisActive) {
      GmWriteCacheManager();
      Set<String> removekeyStr = jedisWriteCache.keys("*" + strPattern + "*");
      for (final String strDeletKey : removekeyStr) {
        log.debug(" Keys to be deleted " + strDeletKey);
        jedisWriteCache.del(strDeletKey);
      }
    }


  }

  /**
   * @param strKey
   * @param strPrefix
   * @param intCount
   * @return
   * @throws AppError
   * @throws IOException
   * @throws JsonMappingException
   * @throws JsonGenerationException
   * @throws JSONException
   */
  public String returnAutoCompleteLikeSearchJSON(String strKey, String strPrefix, int intCount)
      throws AppError {
    // 1. Find the position of the prefix in the sorted set in Redis
    log.debug("inside returnAutoCompleteLikeSearchJSON.........");
    ObjectMapper objectMapper = new ObjectMapper();
    JSONParser jsonParser = new JSONParser();
    List<JSONObject> lResults = new ArrayList<JSONObject>();
    ScanParams scanParams = new ScanParams();
    String strSeachDelim = GmAutoCompleteReportBean.strAutoSearchDelimiter;
    String strResult = "";
    int intDelimiterLen = strSeachDelim.length();
    int intElementCnt = 0;
    intDelimiterLen = intDelimiterLen - 1;
    String strIds = "";
    String strName = "";
    String strOldIds = "";

    if (null == strPrefix) {
      return "";
    }
    int intPrefixLength = strPrefix.length();
    try {
      // Get the total number of elements in the sorted set
      intElementCnt = jedisCache.zcard(strKey).intValue();
    } catch (Exception ex) {
      log.debug("returnAutoCompleteLikeSearchJSON...." + ex.getMessage());
    }
    // No need to proceed if the prefix is not available or the value is not available int the key
    if (intElementCnt == 0 || intPrefixLength == 0) {
      return "";
    }

    /*
     * Parameters to be passed to the zscan; count - the count, which forces to scan all the
     * elements in the cache; match - the value that needs to be searched in the cache
     */
    scanParams.count(intElementCnt);
    scanParams.match("*" + strPrefix + "*" + strSeachDelim + "*");
    // To get the values from the redis sorted set based on the characters entering in the
    // Autocomplete box
    ScanResult<Tuple> rangeResults = jedisCache.zscan(strKey, String.valueOf(0), scanParams);
    if (rangeResults.equals("")) {
      return "";
    }
    try {
      List<Tuple> lResult = rangeResults.getResult();
      // Sort the values in the result as it is not guaranteed that the value returning from the
      // sorted set is in order
      Collections.sort(lResult);
      // Convert the value getting in the lResult to string
      strResult = objectMapper.writeValueAsString(lResult);
      String strJSONVal = null;
      // Remove first and last square brackets to get it as a JSON string
      strResult = strResult.substring(1);
      strJSONVal = strResult.substring(0, strResult.length() - 1);
      String strJSONValTemp = strJSONVal;
      // Need to get only the number of records mentioned for the autocomplete box
      while (strJSONValTemp.length() > 0 && lResults.size() < intCount) {
        String strJsonsubstr = strJSONValTemp.substring(0, strJSONValTemp.indexOf("}") + 1);
        if (strJSONValTemp.indexOf("}") + 1 != strJSONValTemp.length()) {
          strJSONValTemp =
              strJSONValTemp.substring(strJSONValTemp.indexOf("}") + 2, strJSONValTemp.length());
        } else {
          strJSONValTemp = "";
        }
        // JSONObject jsnObjectStr = new JSONObject(strJsonsubstr);
        // String strElement = jsnObjectStr.getString("element");

        JSONObject jsnObjectStr = (JSONObject) jsonParser.parse(strJsonsubstr);
        String strElement = GmCommonClass.parseNull((String) jsnObjectStr.get("element"));

        JSONObject jsonObject = new JSONObject();

        if (strElement.endsWith(strSeachDelim) && lResults.size() < intCount) {
          String strAcc = strElement.substring(0, strElement.length() - 1);
          StringTokenizer strTokenizer = new StringTokenizer(strAcc, ":");          
          strTokenizer.nextToken();
          if(strTokenizer.countTokens() >3){// if the auto complete search with name and keywords(alt name).//PMT-23069
           strTokenizer.nextToken();
           strTokenizer.nextToken();
          }
          strName = strTokenizer.nextToken();          
          jsonObject.put("NAME", strName);
          strIds = strTokenizer.nextToken();
          if (!strIds.equals("") && !strOldIds.equals(strIds)){
        	  strOldIds = strIds; 
	          strIds = strIds.substring(0, strIds.length() - intDelimiterLen);          
	          jsonObject.put("ID", strIds);
	          lResults.add(jsonObject);
	     }
        }
      }
    } catch (Exception ex) {
      log.debug("returnAutoCompleteLikeSearchJSON...." + ex.getMessage());
    }
    return JSONArray.toJSONString(lResults);
  }
}
