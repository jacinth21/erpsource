/**
 * Utility Class for handling Connections, Callable Statement and Prepared Statement. Would be used
 * for executing Procedures and Prepared Statement
 * 
 * Usage : 1. Initialize the DBManager by passing the procedure name along and the number of
 * elements to the procedure as parameter This would initialize a Connection and a Callable
 * Statement (callableStatement) for the procedure GmDBManager gmDBManager = new
 * GmDBManager("gm_pd_sav_partnumber",24);
 * 
 * 2. Set all the parameters for the procdure through the instance gmDBManager created earlier
 * gmDBManager.setString(1,strPartNumber);
 * gmDBManager.setInt(8,Integer.parseInt(strMeasuringDevice));
 * 
 * 3. Execute the procedure by calling executeStatement gmDBManager.executeStatement();
 * 
 * 4. If the connection need to be sent to another method say saveLog, then call getActiveConnection
 * gmCommonBean.saveLog(gmDBManager.getActiveConnection(),strPartNumber , strLogReason,strUsername,
 * "1218");
 * 
 * 5. The Transaction can be explicitly commited by calling commitConnection
 * gmDBManager.commitConnection();
 * 
 * Advantages : 1. All the DB Connections are centrally controlled and managed by DBManager so we
 * could avoid any stale connection issues
 * 
 */
package com.globus.common.db;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.OraclePreparedStatement;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmDBManager extends DBConnectionWrapper {
  public static final long serialVersionUID = 1232323453L;


  /*
   * Following is an enumeration concept where we can specify respective connections. So for OLTP
   * and JOBs it refers different connections.
   */
  public enum GmDNSNamesEnum {
    WEBGLOBUS("jdbc/globus"), GMJOBS("jdbc/globus_jobs"), DMWEBGLOBUS("jdbc/globus_dm"), EXTTIME_WEBGLOBUS(
        "jdbc/globus_extweb");

    private String strDNSName;

    private GmDNSNamesEnum(String value) {
      this.strDNSName = value;
    }

    public String getDNSName() {
      return this.strDNSName;
    }
  };

  // This property is moved to DBConnectionWrapper
  // private GmDNSNamesEnum enDNSName = GmDNSNamesEnum.WEBGLOBUS;
  /*
   * Default constructor which will refer the WEBGLOBUS to connect
   */
  public GmDBManager() {
    this.enDNSName = enDNSName;
  }

  /*
   * parameterized constructor which will refer the parameter to connect
   */
  public GmDBManager(GmDNSNamesEnum enDNSName_) {
    this.enDNSName = enDNSName_;
  }


  /**
   * Constructor will set time zone and it will used to set in DB connection session
   * 
   * @param gmDataStoreVO
   */
  public GmDBManager(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
    this.enDNSName = enDNSName;
  }

  /**
   * Constructor will set time zone and it will used to set in DB connection session
   * 
   * @param enDNSName_
   * @param gmDataStoreVO
   */
  public GmDBManager(GmDNSNamesEnum enDNSName_, GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
    this.enDNSName = enDNSName_;
  }

  /**
   * GmDBManager - Initializes the CallableStatement and Connection for the procedure
   * 
   * @param strProcedureName , intProcedureParameters
   * @throws AppError
   */
  public void setPrepareString(String strProcedureName, int intProcedureParameters) throws AppError {
    this.strProcedureName = strProcedureName;
    this.intProcedureParameters = intProcedureParameters;
    try {

      if (strProcedureName != null && intProcedureParameters != -1) {
        instantiate(this.enDNSName);
        strPrepareString = this.getStrPrepareString(strProcedureName, intProcedureParameters);
        preparedStatement = conn.prepareCall(strPrepareString);
        callableStatement = (CallableStatement) preparedStatement;
        setTimeOut();      
      }
    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    }
  }

  /**
   * GmDBManager - Initializes the CallableStatement and Connection for the procedure
   * 
   * @param strProcedureName , intProcedureParameters
   * @throws AppError
   */
  public void setFunctionString(String strFunctionName, int intFunctionParameters) throws AppError {
    this.strFunctionName = strFunctionName;
    this.intFunctionInParameters = intFunctionParameters;
    try {

      if (strFunctionName != null && intFunctionInParameters != -1) {
        instantiate(this.enDNSName);
        strPrepareString =
            "{?=" + this.getStrPrepareString(strFunctionName, intFunctionInParameters) + "}";
        preparedStatement = conn.prepareCall(strPrepareString);
        callableStatement = (CallableStatement) preparedStatement;
        setTimeOut();
      }
    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    }
  }

  public void setPrepareString(String strSQL) throws AppError {
    this.strSQL = strSQL;
    try {
      if (strSQL != null) {
        instantiate(this.enDNSName);
        preparedStatement = conn.prepareStatement(strSQL);
        setTimeOut();
      }
    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    }
  }

  /**
   * instantiate - instantiates the Connection and CallableStatement / PreparedStatement
   * 
   * @throws AppError
   */
  private void instantiate(GmDNSNamesEnum enDNSName) throws AppError {
    try {
      if (conn == null) {
        getConnection(enDNSName);
        conn.setAutoCommit(false);
      }

    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    }
  }



  /**
   * finalize - calls terminate to close any open connection or callablestatement
   */
  @Override
  protected void finalize() throws Throwable {
    // log.debug(" INside finalize ");
    terminate();
  }

  /**
   * execute - will execute the active CallableStatement
   * 
   * @throws AppError
   */
  public void execute() throws AppError {
    try {
      callableStatement.execute();
    }

    catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    }
  }

  /**
   * commit - will commitConnection the active conn
   * 
   * @throws AppError
   */
  public void commit() throws AppError {
    try {
      conn.commit();
    }

    catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      terminate();
    }
  }

  /**
   * commit - will commitConnection the active conn
   * 
   * @throws AppError
   */
  public void close() throws AppError {
    try {
      terminate();
    }

    catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      terminate();
    }
  }

  /**
   * terminate - will close any active callablestatement will be if fetch called in a loop
   * 
   * @throws AppError
   */
  public void closeCallableStatement() throws AppError {
    try {
      if (callableStatement != null) {
        callableStatement.close();

      }// Enf of if (callableStatement != null)

      if (preparedStatement != null) {
        preparedStatement.close();
      }

    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    }// End of catch
    finally {
      preparedStatement = null;
      callableStatement = null;
    }
  }

  /**
   * setString - sets the String at the intParameterIndex in the CallableStatement
   */
  public void setString(int intParameterIndex, String strValue) throws AppError {
    try {
      preparedStatement.setString(intParameterIndex, strValue);
    } catch (Exception exp) {
      terminate();
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    }
  }

  public void setFormOfUse(int intParameterIndex, short s) throws AppError {
    try {
      // ((OraclePreparedStatement)preparedStatement).setFormOfUse(intParameterIndex, Const.NCHAR);
      ((OraclePreparedStatement) preparedStatement).setFormOfUse(intParameterIndex,
          ((OraclePreparedStatement) preparedStatement).FORM_NCHAR);
    } catch (Exception exp) {
      terminate();
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    }
  }

  /**
   * setInt - sets the intValue at the intParameterIndex in the CallableStatement
   */

  public void setInt(int intParameterIndex, int intValue) throws AppError {
    try {
      preparedStatement.setInt(intParameterIndex, intValue);
    } catch (Exception exp) {
      terminate();
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    }
  }

  /**
   * setInt - sets the intValue at the intParameterIndex in the CallableStatement
   */

  public void setDouble(int intParameterIndex, double doubleValue) throws AppError {
    try {
      preparedStatement.setDouble(intParameterIndex, doubleValue);
    } catch (Exception exp) {
      terminate();
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    }
  }

  /**
   * setDate - sets the dateValue at the intParameterIndex in the CallableStatement
   */

  public void setDate(int intParameterIndex, Date dtValue) throws AppError {
    try {
      if (dtValue == null) {
        preparedStatement.setDate(intParameterIndex, null);
      } else {
        java.sql.Date sqlDate = new java.sql.Date(dtValue.getTime());
        preparedStatement.setDate(intParameterIndex, sqlDate);
      }
    } catch (Exception exp) {
      terminate();
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    }
  }

  /**
   * registerOurParameter - sets the registerOurParameter at the intParameterIndex in the
   * CallableStatement
   */
  public void registerOutParameter(int intParameterIndex, int intOracleType) throws AppError {
    try {
      callableStatement.registerOutParameter(intParameterIndex, intOracleType);
    }

    catch (Exception exp) {
      terminate();
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    }
  }

  /**
   * getObject - gets the OUT object
   */
  public Object getObject(int intResultSetIndex) throws AppError {
    Object objResult = null;
    try {
      objResult = callableStatement.getObject(intResultSetIndex);
    } catch (Exception exp) {
      terminate();
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    }
    return objResult;
  }

  /**
   * getString - gets the OUT String
   */
  public String getString(int intResultSetIndex) throws AppError {
    String strResult = "";
    try {
      strResult = callableStatement.getString(intResultSetIndex);
    } catch (Exception exp) {
      terminate();
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    }
    return strResult;
  }

  /**
   * getInt - gets the OUT int
   */
  public int getInt(int intResultSetIndex) throws AppError {
    int result;
    try {
      result = callableStatement.getInt(intResultSetIndex);
    } catch (Exception exp) {
      terminate();
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    }
    return result;
  }

  /**
   * querySingleRecord - Uses the querySingleRecord of DBConnectionWrapper
   * 
   * @return HashMap
   * @throws AppError
   */
  public HashMap querySingleRecord() throws AppError {
    ResultSet rs = null;
    HashMap hmReturn = new HashMap();
    try {
      rs = preparedStatement.executeQuery();
      hmReturn = this.returnHashMap(rs);
      rs = null;
    } catch (Exception exp) {
      // terminate();
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    } finally {
      terminate();
      try {
        if (rs != null)
          rs.close();
      } catch (Exception exp) {
        exp.printStackTrace();
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
        log.error("Exception e " + strErrorMsg);
      }
    }
    return hmReturn;
  }

  /**
   * queryMultipleRecord - Uses the queryMultipleRecord of DBConnectionWrapper
   * 
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList queryMultipleRecord() throws AppError {
    ResultSet rs = null;
    ArrayList alReturn = new ArrayList();
    try {
      setTimeOut();
      rs = preparedStatement.executeQuery();
      alReturn = this.returnArrayList(rs);
      rs = null;
    } catch (Exception exp) {
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    } finally {
      terminate();
      try {
        if (rs != null)
          rs.close();
      } catch (Exception exp) {
        exp.printStackTrace();
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
        log.error("Exception e " + strErrorMsg);
      }
    }
    return alReturn;
  }

  /**
   * queryDisplayTagRecord - Uses the queryDisplayTagRecord of DBConnectionWrapper
   * 
   * @return RowSetDynaClass
   * @throws AppError
   */
  public RowSetDynaClass queryDisplayTagRecord() throws AppError {
    ResultSet rs = null;
    RowSetDynaClass rdReturn = null;
    try {
      setTimeOut();
      rs = preparedStatement.executeQuery();
      rdReturn = this.returnRowSetDyna(rs);
      rs = null;
    } catch (Exception exp) {
      exp.printStackTrace();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(exp);
    } finally {
      terminate();
      try {
        if (rs != null)
          rs.close();
      } catch (Exception exp) {
        exp.printStackTrace();
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(exp);
        log.error("Exception e " + strErrorMsg);
      }
    }
    return rdReturn;
  }

  /**
   * getGmDBManager - Uses to get the object of GmDBManager based on JNDI(constant prop.->
   * JNDI_CONNECTION)
   * 
   * @return GmDBManager
   */
  static public GmDBManager getGmDBManager(String strJNDIConnection) {
    if (strJNDIConnection.equals("DMJNDI")) {
      return new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
    } else {
      return new GmDBManager();
    }
  }
}
