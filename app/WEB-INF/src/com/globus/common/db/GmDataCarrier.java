/**
 * 
 */
package com.globus.common.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author vprasath
 *
 */
public class GmDataCarrier implements Serializable {


  /**
   * 
   */
  public GmDataCarrier() {
    // TODO Auto-generated constructor stub
  }

  private ArrayList<HashMap<String, String>> alList = new ArrayList<HashMap<String, String>>();

  /**
   * @return
   */
  public ArrayList<HashMap<String, String>> getAlList() {
    return alList;
  }

  /**
   * @param alList
   */
  public void setAlList(ArrayList<HashMap<String, String>> alList) {
    this.alList = alList;
  }



}
