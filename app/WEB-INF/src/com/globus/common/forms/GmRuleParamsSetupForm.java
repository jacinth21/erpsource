
/**
 * FileName    : GmRuleParamsSetupForm.java 
 * Description : extends GmCommonForm.java
 * Author      : Velu
 * Date        : Jul 31 2010 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.forms;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class GmRuleParamsSetupForm extends GmLogForm {
	
	private static final long serialVersionUID = 1234567890L;
	
    private String codeGrpId = "";
    private String ruleGrpId = "";
    private String strInputString = "";
    private String strInvSource = "";
    private List lResult=null;
    private String ruleValue="";
    private String hruleGrpId="";
    private String hcodeGrpId="";
    private String header="";
    private HashMap hmResult=null;
    private String rulevalidation ="";
    private String strAccountType="";
    private ArrayList alPayment =new ArrayList();
    
    public String getStrInvSource() {
		return strInvSource;
	}
	public void setStrInvSource(String strInvSource) {
		this.strInvSource = strInvSource;
	}
	
    public ArrayList getAlPayment() {
		return alPayment;
	}
    public void setAlPayment(ArrayList alPayment) {
		this.alPayment = alPayment;
	}


	public String getHeader() {
		return header;
	}


	public void setHeader(String header) {
		this.header = header;
	}


	private List alReturn = new ArrayList();
    
    public List getAlReturn() {
		return alReturn;
	}


	public void setAlReturn(List alReturn) {
		this.alReturn = alReturn;
	}
    /**
	 * @return the ruleValue
	 */
	public String getRuleValue() {
		return ruleValue;
	}


	/**
	 * @return the hcodeGrpId
	 */
	public String getHcodeGrpId() {
		return hcodeGrpId;
	}


	/**
	 * @param hcodeGrpId the hcodeGrpId to set
	 */
	public void setHcodeGrpId(String hcodeGrpId) {
		this.hcodeGrpId = hcodeGrpId;
	}


	/**
	 * @param ruleValue the ruleValue to set
	 */
	public void setRuleValue(String ruleValue) {
		this.ruleValue = ruleValue;
	}


	/**
	 * @return the hruleGrpId
	 */
	public String getHruleGrpId() {
		return hruleGrpId;
	}


	/**
	 * @param hruleGrpId the hruleGrpId to set
	 */
	public void setHruleGrpId(String hruleGrpId) {
		this.hruleGrpId = hruleGrpId;
	}


	
    

	/**
	 * @return the lResult
	 */
	public List getlResult() {
		return lResult;
	}


	/**
	 * @param lResult the lResult to set
	 */
	public void setlResult(List lResult) {
		this.lResult = lResult;
	}

	/**
	 * @return the codeGrpId
	 */
	public String getCodeGrpId() {
		return codeGrpId;
	}


	/**
	 * @param codeGrpId the codeGrpId to set
	 */
	public void setCodeGrpId(String codeGrpId) {
		this.codeGrpId = codeGrpId;
	}

	
	/**
	 * @return the ruleGrpId
	 */
	public String getRuleGrpId() {
		return ruleGrpId;
	}


	/**
	 * @param ruleGrpId the ruleGrpId to set
	 */
	public void setRuleGrpId(String ruleGrpId) {
		this.ruleGrpId = ruleGrpId;
	}


	/**
	 * @return the strInputString
	 */
	public String getStrInputString() {
		return strInputString;
	}


	/**
	 * @param strInputString the strInputString to set
	 */
	public void setStrInputString(String strInputString) {
		this.strInputString = strInputString;
	}


	/**
	 * @return the hmResult
	 */
	public HashMap getHmResult() {
		return hmResult;
	}


	/**
	 * @param hmResult the hmResult to set
	 */
	public void setHmResult(HashMap hmResult) {
		this.hmResult = hmResult;
	}


	/**
	 * @return the rulevalidation
	 */
	public String getRulevalidation() {
		return rulevalidation;
	}

	/**
	 * @param rulevalidation the rulevalidation to set
	 */
	public void setRulevalidation(String rulevalidation) {
		this.rulevalidation = rulevalidation;
	}
	
	/**
	 * @return the account type
	 */
	public String getStrAccountType() {
		return strAccountType;
	}
	
	/**
	 * @param account type to set
	 */
	public void setStrAccountType(String strAccountType) {
		this.strAccountType = strAccountType;
	}
	
	
}