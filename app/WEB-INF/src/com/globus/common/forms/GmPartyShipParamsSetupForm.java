 
package com.globus.common.forms;
import java.util.ArrayList;
import java.util.HashMap;


public class GmPartyShipParamsSetupForm extends GmLogForm {
	 
    private String partyID = "";
    private String thirdParty = "";
    private String hfeightCharge = "";
    private String shipCarrier = ""; 
    private String shipMode = ""; 
    private String stdShipPayee = ""; 
    private String rushShipPayee = ""; 
    private String stdShipChargeType = "";  
    private String rushShipChargeType = ""; 
    private String stdShipValue = "";
    private String rushShipValue = ""; 
    private String screenType = "";
    private String accountId = "";    
    private String Txt_AccId = "";
    private String Cbo_AccId = "";
    private String strAccLbl = "";
    private String gpofl = "";
    private String shipmentEmail = "";
    
   
	private HashMap hmResult = new HashMap();
    private ArrayList alShipCarrier =new ArrayList();
    private ArrayList alShipmode =new ArrayList();
    private ArrayList alStdShipPayee =new ArrayList();
    private ArrayList alStdShipChargeType =new ArrayList();
    private ArrayList alRuchShipPayee =new ArrayList();
    private ArrayList alRuchShipChargeType =new ArrayList();
    private ArrayList alAccountList = new ArrayList();
    
      
    
    /**
     * @return the gpofl
     */
    public String getGpofl() {
      return gpofl;
    }
    /**
     * @param gpofl the gpofl to set
     */
    public void setGpofl(String gpofl) {
      this.gpofl = gpofl;
    }
    /**
     * @return the partyID
     */
    public String getPartyID() {
      return partyID;
    }
    /**
     * @param partyID the partyID to set
     */
    public void setPartyID(String partyID) {
      this.partyID = partyID;
    }
    /**
     * @return the thirdParty
     */
    public String getThirdParty() {
      return thirdParty;
    }
    /**
     * @param thirdParty the thirdParty to set
     */
    public void setThirdParty(String thirdParty) {
      this.thirdParty = thirdParty;
    }
    /**
     * @return the hfeightCharge
     */
    public String getHfeightCharge() {
      return hfeightCharge;
    }
    /**
     * @param hfeightCharge the hfeightCharge to set
     */
    public void setHfeightCharge(String hfeightCharge) {
      this.hfeightCharge = hfeightCharge;
    }
    /**
     * @return the shipCarrier
     */
    public String getShipCarrier() {
      return shipCarrier;
    }
    /**
     * @param shipCarrier the shipCarrier to set
     */
    public void setShipCarrier(String shipCarrier) {
      this.shipCarrier = shipCarrier;
    }
    /**
     * @return the shipMode
     */
    public String getShipMode() {
      return shipMode;
    }
    /**
     * @param shipMode the shipMode to set
     */
    public void setShipMode(String shipMode) {
      this.shipMode = shipMode;
    }
    /**
     * @return the stdShipPayee
     */
    public String getStdShipPayee() {
      return stdShipPayee;
    }
    /**
     * @param stdShipPayee the stdShipPayee to set
     */
    public void setStdShipPayee(String stdShipPayee) {
      this.stdShipPayee = stdShipPayee;
    }
    /**
     * @return the rushShipPayee
     */
    public String getRushShipPayee() {
      return rushShipPayee;
    }
    /**
     * @param rushShipPayee the rushShipPayee to set
     */
    public void setRushShipPayee(String rushShipPayee) {
      this.rushShipPayee = rushShipPayee;
    }
    /**
     * @return the stdShipChargeType
     */
    public String getStdShipChargeType() {
      return stdShipChargeType;
    }
    /**
     * @param stdShipChargeType the stdShipChargeType to set
     */
    public void setStdShipChargeType(String stdShipChargeType) {
      this.stdShipChargeType = stdShipChargeType;
    }
    /**
     * @return the rushShipChargeType
     */
    public String getRushShipChargeType() {
      return rushShipChargeType;
    }
    /**
     * @param rushShipChargeType the rushShipChargeType to set
     */
    public void setRushShipChargeType(String rushShipChargeType) {
      this.rushShipChargeType = rushShipChargeType;
    }
    /**
     * @return the stdShipValue
     */
    public String getStdShipValue() {
      return stdShipValue;
    }
    /**
     * @param stdShipValue the stdShipValue to set
     */
    public void setStdShipValue(String stdShipValue) {
      this.stdShipValue = stdShipValue;
    }
    /**
     * @return the rushShipValue
     */
    public String getRushShipValue() {
      return rushShipValue;
    }
    /**
     * @param rushShipValue the rushShipValue to set
     */
    public void setRushShipValue(String rushShipValue) {
      this.rushShipValue = rushShipValue;
    }   
    
    /**
	 * @return the screenType
	 */
	public String getScreenType() {
		return screenType;
	}
	/**
	 * @param screenType the screenType to set
	 */
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	/**
     * @return the hmResult
     */
    public HashMap getHmResult() {
      return hmResult;
    }
    /**
     * @param hmResult the hmResult to set
     */
    public void setHmResult(HashMap hmResult) {
      this.hmResult = hmResult;
    }
    /**
     * @return the alShipCarrier
     */
    public ArrayList getAlShipCarrier() {
      return alShipCarrier;
    }
    /**
     * @param alShipCarrier the alShipCarrier to set
     */
    public void setAlShipCarrier(ArrayList alShipCarrier) {
      this.alShipCarrier = alShipCarrier;
    }
    /**
     * @return the alShipmode
     */
    public ArrayList getAlShipmode() {
      return alShipmode;
    }
    /**
     * @param alShipmode the alShipmode to set
     */
    public void setAlShipmode(ArrayList alShipmode) {
      this.alShipmode = alShipmode;
    }
    /**
     * @return the alStdShipPayee
     */
    public ArrayList getAlStdShipPayee() {
      return alStdShipPayee;
    }
    /**
     * @param alStdShipPayee the alStdShipPayee to set
     */
    public void setAlStdShipPayee(ArrayList alStdShipPayee) {
      this.alStdShipPayee = alStdShipPayee;
    }
    /**
     * @return the alStdShipChargeType
     */
    public ArrayList getAlStdShipChargeType() {
      return alStdShipChargeType;
    }
    /**
     * @param alStdShipChargeType the alStdShipChargeType to set
     */
    public void setAlStdShipChargeType(ArrayList alStdShipChargeType) {
      this.alStdShipChargeType = alStdShipChargeType;
    }
    /**
     * @return the alRuchShipPayee
     */
    public ArrayList getAlRuchShipPayee() {
      return alRuchShipPayee;
    }
    /**
     * @param alRuchShipPayee the alRuchShipPayee to set
     */
    public void setAlRuchShipPayee(ArrayList alRuchShipPayee) {
      this.alRuchShipPayee = alRuchShipPayee;
    }
    /**
     * @return the alRuchShipChargeType
     */
    public ArrayList getAlRuchShipChargeType() {
      return alRuchShipChargeType;
    }
    /**
     * @param alRuchShipChargeType the alRuchShipChargeType to set
     */
    public void setAlRuchShipChargeType(ArrayList alRuchShipChargeType) {
      this.alRuchShipChargeType = alRuchShipChargeType;
    }
	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}	
	/**
	 * @return the txt_AccId
	 */
	public String getTxt_AccId() {
		return Txt_AccId;
	}
	/**
	 * @param txt_AccId the txt_AccId to set
	 */
	public void setTxt_AccId(String txt_AccId) {
		Txt_AccId = txt_AccId;
	}
	/**
	 * @return the cbo_AccId
	 */
	public String getCbo_AccId() {
		return Cbo_AccId;
	}
	/**
	 * @param cbo_AccId the cbo_AccId to set
	 */
	public void setCbo_AccId(String cbo_AccId) {
		Cbo_AccId = cbo_AccId;
	}
	/**
	 * @return the alAccountList
	 */
	public ArrayList getAlAccountList() {
		return alAccountList;
	}
	/**
	 * @param alAccountList the alAccountList to set
	 */
	public void setAlAccountList(ArrayList alAccountList) {
		this.alAccountList = alAccountList;
	}
  /**
   * @return the strAccLbl
   */
  public String getStrAccLbl() {
    return strAccLbl;
  }
  /**
   * @param strAccLbl the strAccLbl to set
   */
  public void setStrAccLbl(String strAccLbl) {
    this.strAccLbl = strAccLbl;
  }
    /**
     * @return the shipmentEmail
     */
    public String getShipmentEmail() {
        return shipmentEmail;
    }
    /**
     * @param shipmentEmail the shipmentEmail to set
     */
    public void setShipmentEmail(String shipmentEmail) {
        this.shipmentEmail = shipmentEmail;
    }	
}