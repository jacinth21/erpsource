/**
 * 
 */
package com.globus.common.forms;


/**
 * @author tsekaran
 *
 */
public class GmShipmentPackSlipForm  extends GmLogForm {
	

	  private String hOrderId = "";
	  private String type = "";
	  private String transId = "";
	  private String txnType = "";
	  private String hGUId = "";

	  


	/**
	 * @return the hGUId
	 */
	public String gethGUId() {
		return hGUId;
	}

	/**
	 * @param hGUId the hGUId to set
	 */
	public void sethGUId(String hGUId) {
		this.hGUId = hGUId;
	}

	/**
	 * @return the transId
	 */
	public String getTransId() {
		return transId;
	}

	/**
	 * @param transId the transId to set
	 */
	public void setTransId(String transId) {
		this.transId = transId;
	}

	/**
	 * @return the txnType
	 */
	public String getTxnType() {
		return txnType;
	}

	/**
	 * @param txnType the txnType to set
	 */
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}

	/**
	   * @return the hOrderId
	   */
	  public String gethOrderId() {
	    return hOrderId;
	  }

	  /**
	   * @param hOrderId the hOrderId to set
	   */
	  public void sethOrderId(String hOrderId) {
	    this.hOrderId = hOrderId;
	  }

	  /**
	   * @return the type
	   */
	  public String getType() {
	    return type;
	  }

	  /**
	   * @param type the type to set
	   */
	  public void setType(String type) {
	    this.type = type;
	  }
}
