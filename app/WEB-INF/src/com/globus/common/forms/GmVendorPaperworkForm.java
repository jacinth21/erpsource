package com.globus.common.forms;

public class GmVendorPaperworkForm extends GmLogForm {

	
	  private String PoID = "";
	  private int VendorID = 0;
	  private String GuId = "";
	  private String UserId = "";
	  
	  /**
		 * @return the PoID
		 */
	public String getPoID() {
		return PoID;
	}
	
	/**
	 * @param PoID the PoID to set
	 */
	public void setPOID(String poID) {
		PoID = poID;
	}
	
	/**
	 * @return the VendorID
	 */
	public int getVendorID() {
		return VendorID;
	}
	
	/**
	 * @param VendorID the VendorID to set
	 */
	public void setVendorID(int vendorID) {
		VendorID = vendorID;
	}
	
	/**
	 * @return the GuId
	 */
	public String getGuId() {
		return GuId;
	}
	
	/**
	 * @param GuId the GuId to set
	 */
	public void setGuId(String guId) {
		GuId = guId;
	}

	 /**
	 * @return the UserId
	 */
	public String getUserId() {
		return UserId;
	}

	/**
	 * @param UserId the UserId to set
	 */
	public void setUserId(String userId) {
		UserId = userId;
	}
	
	
	  
	  
}
