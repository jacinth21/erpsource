package com.globus.common.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCancelForm;

public class GmDownloadForm extends GmCancelForm{

	private String vendorId = "";
	private String poNumber ="";
	private String invoiceNumber ="";
	private String invoiceDateFrom ="";
	private String invoiceDateTo="";
	private String hinputStr="";
	private String hdownloadAmtTotal="";
	
	private String batchNumber ="";
	private String batchDateFrom ="";
	private String batchDateTo ="";
	
	private String hlocation=""; 
	
	private ArrayList alVendorList = new ArrayList();
	private List returnList = new ArrayList(); 

	private HashMap hmInvoiceDownload = new HashMap();
	
	private String enableDownload = "false";
	private String enableRollbackPayment = "false";
	
	/**
	 * @return the enableDownload
	 */
	public String getEnableDownload() {
		return enableDownload;
	}


	/**
	 * @param enableDownload the enableDownload to set
	 */
	public void setEnableDownload(String enableDownload) {
		this.enableDownload = enableDownload;
	}


	/**
	 * @return the enableRollbackPayment
	 */
	public String getEnableRollbackPayment() {
		return enableRollbackPayment;
	}


	/**
	 * @param enableRollbackPayment the enableRollbackPayment to set
	 */
	public void setEnableRollbackPayment(String enableRollbackPayment) {
		this.enableRollbackPayment = enableRollbackPayment;
	}


	public String getVendorId() {
		return vendorId;
	}


	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}


	public ArrayList getAlVendorList() {
		return alVendorList;
	}


	public void setAlVendorList(ArrayList alVendorList) {
		this.alVendorList = alVendorList;
	}


	public List getReturnList() {
		return returnList;
	}


	public void setReturnList(List returnList) {
		this.returnList = returnList;
	}


	public String getPoNumber() {
		return poNumber;
	}


	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}


	public String getInvoiceDateFrom() {
		return invoiceDateFrom;
	}


	public void setInvoiceDateFrom(String invoiceDateFrom) {
		this.invoiceDateFrom = invoiceDateFrom;
	}


	public String getInvoiceDateTo() {
		return invoiceDateTo;
	}


	public void setInvoiceDateTo(String invoiceDateTo) {
		this.invoiceDateTo = invoiceDateTo;
	}


	public String getInvoiceNumber() {
		return invoiceNumber;
	}


	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}


	public String getHinputStr() {
		return hinputStr;
	}


	public void setHinputStr(String hinputStr) {
		this.hinputStr = hinputStr;
	}
 
	public String getHdownloadAmtTotal() {
		return hdownloadAmtTotal;
	}


	public void setHdownloadAmtTotal(String hdownloadAmtTotal) {
		this.hdownloadAmtTotal = hdownloadAmtTotal;
	}


	public HashMap getHmInvoiceDownload() {
		return hmInvoiceDownload;
	}


	public void setHmInvoiceDownload(HashMap hmInvoiceDownload) {
		this.hmInvoiceDownload = hmInvoiceDownload;
	}


	public String getHlocation() {
		return hlocation;
	}


	public void setHlocation(String hlocation) {
		this.hlocation = hlocation;
	}


	public String getBatchNumber() {
		return batchNumber;
	}


	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}


	public String getBatchDateFrom() {
		return batchDateFrom;
	}


	public void setBatchDateFrom(String batchDateFrom) {
		this.batchDateFrom = batchDateFrom;
	}


	public String getBatchDateTo() {
		return batchDateTo;
	}


	public void setBatchDateTo(String batchDateTo) {
		this.batchDateTo = batchDateTo;
	} 
}
