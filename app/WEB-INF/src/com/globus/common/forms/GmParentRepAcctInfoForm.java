package com.globus.common.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

// TODO: Auto-generated Javadoc
/**
 * The Class GmParentRepAcctInfoForm.
 */
public class GmParentRepAcctInfoForm extends GmCommonForm{

	/** The acct id. */
	private String acctId = "";

	/**
	 * Gets the acct id.
	 *
	 * @return the acct id
	 */
	public String getAcctId() {
		return acctId;
	}

	/**
	 * Sets the acct id.
	 *
	 * @param acctId the new acct id
	 */
	public void setAcctId(String acctId) {
		this.acctId = acctId;
	}
}
