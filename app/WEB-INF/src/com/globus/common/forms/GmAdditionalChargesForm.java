/**
 * FileName    : GmAdditionalChargesForm.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Mar 20, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.forms;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author sthadeshwar
 *
 */
public class GmAdditionalChargesForm extends GmCommonForm {

	private String additionalChargeId = "";
	private HashMap hmCodeList = new HashMap();
	private ArrayList alReturn = new ArrayList();
	private String shipCost="";
	private String orderId="";
	private String userId="";
	private String comments="";
	/**
	 * @return the additionalChargeId
	 */
	public String getAdditionalChargeId() {
		return additionalChargeId;
	}
	/**
	 * @param additionalChargeId the additionalChargeId to set
	 */
	public void setAdditionalChargeId(String additionalChargeId) {
		this.additionalChargeId = additionalChargeId;
	}
	/**
	 * @return the hmCodeList
	 */
	public HashMap getHmCodeList() {
		return hmCodeList;
	}
	/**
	 * @param hmCodeList the hmCodeList to set
	 */
	public void setHmCodeList(HashMap hmCodeList) {
		this.hmCodeList = hmCodeList;
	}
	/**
	 * @return the alReturn
	 */
	public ArrayList getAlReturn() {
		return alReturn;
	}
	/**
	 * @param alReturn the alReturn to set
	 */
	public void setAlReturn(ArrayList alReturn) {
		this.alReturn = alReturn;
	}
	/**
	 * @return the shipCost
	 */
	public String getShipCost() {
		return shipCost;
	}
	/**
	 * @param shipCost the shipCost to set
	 */
	public void setShipCost(String shipCost) {
		this.shipCost = shipCost;
	}
	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}
	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
}
