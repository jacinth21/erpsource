/**
 * 
 */
package com.globus.common.forms;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vprasath
 */
public class GmCommonContactForm extends GmLogForm
{
   // private static final long serialVersionUID = 2L;
    
    private ArrayList alNames         = new ArrayList();
    private ArrayList alContactModes   = new ArrayList();
    private ArrayList alContactTypes   = new ArrayList();
    private ArrayList alPreferences   = new ArrayList();
    private List ldtResult = null;
    
    private String contactValue  = "";
    private String inActiveFlag  = "";
    private String partyId = "";
    private String partyType = "";
    private String contactMode = "";
    private String contactType = "";
    private String preference  = "";
    private String hcontactID  = "";
    /**
     * @return Returns the alContactModes.
     */
    public ArrayList getAlContactModes()
    {
        return alContactModes;
    }
    /**
     * @param alContactModes The alContactModes to set.
     */
    public void setAlContactModes(ArrayList alContactModes)
    {
        this.alContactModes = alContactModes;
    }
    /**
     * @return Returns the alContactTypes.
     */
    public ArrayList getAlContactTypes()
    {
        return alContactTypes;
    }
    /**
     * @param alContactTypes The alContactTypes to set.
     */
    public void setAlContactTypes(ArrayList alContactTypes)
    {
        this.alContactTypes = alContactTypes;
    }
    /**
     * @return Returns the alNames.
     */
    public ArrayList getAlNames()
    {
        return alNames;
    }
    /**
     * @param alNames The alNames to set.
     */
    public void setAlNames(ArrayList alNames)
    {
        this.alNames = alNames;
    }
    /**
     * @return Returns the alPreferences.
     */
    public ArrayList getAlPreferences()
    {
        return alPreferences;
    }
    /**
     * @param alPreferences The alPreferences to set.
     */
    public void setAlPreferences(ArrayList alPreferences)
    {
        this.alPreferences = alPreferences;
    }
    /**
     * @return Returns the contactMode.
     */
    public String getContactMode()
    {
        return contactMode;
    }
    /**
     * @param contactMode The contactMode to set.
     */
    public void setContactMode(String contactMode)
    {
        this.contactMode = contactMode;
    }
    /**
     * @return Returns the contactType.
     */
    public String getContactType()
    {
        return contactType;
    }
    /**
     * @param contactType The contactType to set.
     */
    public void setContactType(String contactType)
    {
        this.contactType = contactType;
    }
    /**
     * @return Returns the contactValue.
     */
    public String getContactValue()
    {
        return contactValue;
    }
    /**
     * @param contactValue The contactValue to set.
     */
    public void setContactValue(String contactValue)
    {
        this.contactValue = contactValue;
    }
    /**
     * @return Returns the hcontactID.
     */
    public String getHcontactID()
    {
        return hcontactID;
    }
    /**
     * @param hcontactID The hcontactID to set.
     */
    public void setHcontactID(String hcontactID)
    {
        this.hcontactID = hcontactID;
    }
    /**
     * @return Returns the inActiveFlag.
     */
    public String getInActiveFlag()
    {
        return inActiveFlag;
    }
    /**
     * @param inActiveFlag The inActiveFlag to set.
     */
    public void setInActiveFlag(String inActiveFlag)
    {
        this.inActiveFlag = inActiveFlag;
    }
    /**
     * @return Returns the ldtResult.
     */
    public List getLdtResult()
    {
        return ldtResult;
    }
    /**
     * @param ldtResult The ldtResult to set.
     */
    public void setLdtResult(List ldtResult)
    {
        this.ldtResult = ldtResult;
    }
    /**
     * @return Returns the partyId.
     */
    public String getPartyId()
    {
        return partyId;
    }
    /**
     * @param partyId The partyId to set.
     */
    public void setPartyId(String partyId)
    {
        this.partyId = partyId;
    }
    /**
     * @return Returns the partyType.
     */
    public String getPartyType()
    {
        return partyType;
    }
    /**
     * @param partyType The partyType to set.
     */
    public void setPartyType(String partyType)
    {
        this.partyType = partyType;
    }
    /**
     * @return Returns the preference.
     */
    public String getPreference()
    {
        return preference;
    }
    /**
     * @param preference The preference to set.
     */
    public void setPreference(String preference)
    {
        this.preference = preference;
    }   

    }
