package com.globus.common.forms;

import java.util.ArrayList;
import java.util.List;
 

public class GmQuestionAnswerForm extends GmCancelForm
{
     
	
    private String hinputAnswer = "";
    
    private ArrayList alQuestion = new ArrayList();
    private ArrayList alAnswer = new ArrayList();
    private ArrayList alAnswerGroup = new ArrayList();
    private ArrayList alQuestionAnswer= new ArrayList();
    
    
	/**
	 * @return the hinputAnswer
	 */
	public String getHinputAnswer() {
		return hinputAnswer;
	}
	/**
	 * @param hinputAnswer the hinputAnswer to set
	 */
	public void setHinputAnswer(String hinputAnswer) {
		this.hinputAnswer = hinputAnswer;
	}
	/**
	 * @return the alQuestion
	 */
	public ArrayList getAlQuestion() {
		return alQuestion;
	}
	/**
	 * @param alQuestion the alQuestion to set
	 */
	public void setAlQuestion(ArrayList alQuestion) {
		this.alQuestion = alQuestion;
	}
	/**
	 * @return the alAnswer
	 */
	public ArrayList getAlAnswer() {
		return alAnswer;
	}
	/**
	 * @param alAnswer the alAnswer to set
	 */
	public void setAlAnswer(ArrayList alAnswer) {
		this.alAnswer = alAnswer;
	}
	/**
	 * @return the alAnswerGroup
	 */
	public ArrayList getAlAnswerGroup() {
		return alAnswerGroup;
	}
	/**
	 * @param alAnswerGroup the alAnswerGroup to set
	 */
	public void setAlAnswerGroup(ArrayList alAnswerGroup) {
		this.alAnswerGroup = alAnswerGroup;
	}
	/**
	 * @return the alQuestionAnswer
	 */
	public ArrayList getAlQuestionAnswer() {
		return alQuestionAnswer;
	}
	/**
	 * @param alQuestionAnswer the alQuestionAnswer to set
	 */
	public void setAlQuestionAnswer(ArrayList alQuestionAnswer) {
		this.alQuestionAnswer = alQuestionAnswer;
	}
	 
}