/**
 * FileName    : GmRuleConditionForm.java 
 * Description :
 * Author      : rshah
 * Date        : Jun 3, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.forms;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author rshah
 *
 */
public class GmRuleConditionForm extends GmCommonForm{
	private String ruleId = "";
	private String ruleName = "";
	private String expiryDate = "";
	private String initiatedBy = "";
	private String initiatedDate = "";
	private String activeFl = "";
	private String selectedTxn = "";
	private String conditionsInputStr = "";
	private String selectAll = "";
	private String transStr = "";
	private String strAction = "";
	private String consequenceGrp = "";
	private String consequenceId = "";
	private String consequenceStr = "";
	private String ruleConsequenceId = "";
	private String ruleErrorMsg = "";
	private String ruleComment = "";
	private String ruleEmailNotify = "";
	private String[] selectedTrans = new String[15];
	private String enableCreateRule = "false";
	private String enableVoidRule = "false";
	private String editFlag = "";
	private String emailId="";
	private String ruleTxnType = "";
	private ArrayList alTransactionList = new ArrayList();
	private ArrayList alConsTabList = new ArrayList();
	private ArrayList alConsequenceList = new ArrayList();
	private ArrayList alDbConsequenceList = new ArrayList();
	private ArrayList alSelectedConsList = new ArrayList();
	private HashMap hmRules = new HashMap();
	private HashMap hmConsequence = new HashMap();
	private HashMap hmTrans = new HashMap();
	private ArrayList alOperators = new ArrayList();
	private ArrayList alConditions = new ArrayList();
	private ArrayList alCountries = new ArrayList();
	private ArrayList alVendors = new ArrayList();
	private ArrayList alSetList = new ArrayList();
	private ArrayList alRuleTxnType = new ArrayList();
	
	/**
	 * @return the hmRules
	 */
	public HashMap getHmRules() {
		return hmRules;
	}
	/**
	 * @param hmRules the hmRules to set
	 */
	public void setHmRules(HashMap hmRules) {
		this.hmRules = hmRules;
	}
	/**
	 * @return the ruleName
	 */
	public String getRuleName() {
		return ruleName;
	}
	/**
	 * @param ruleName the ruleName to set
	 */
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}
	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	/**
	 * @return the initiatedBy
	 */
	public String getInitiatedBy() {
		return initiatedBy;
	}
	/**
	 * @param initiatedBy the initiatedBy to set
	 */
	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}
	/**
	 * @return the initiatedDate
	 */
	public String getInitiatedDate() {
		return initiatedDate;
	}
	/**
	 * @param initiatedDate the initiatedDate to set
	 */
	public void setInitiatedDate(String initiatedDate) {
		this.initiatedDate = initiatedDate;
	}
	/**
	 * @return the activeFl
	 */
	public String getActiveFl() {
		return activeFl;
	}
	/**
	 * @param activeFl the activeFl to set
	 */
	public void setActiveFl(String activeFl) {
		this.activeFl = activeFl;
	}
	/**
	 * @return the selectedTxn
	 */
	public String getSelectedTxn() {
		return selectedTxn;
	}
	/**
	 * @param selectedTxn the selectedTxn to set
	 */
	public void setSelectedTxn(String selectedTxn) {
		this.selectedTxn = selectedTxn;
	}
	/**
	 * @return the conditionsInputStr
	 */
	public String getConditionsInputStr() {
		return conditionsInputStr;
	}
	/**
	 * @param conditionsInputStr the conditionsInputStr to set
	 */
	public void setConditionsInputStr(String conditionsInputStr) {
		this.conditionsInputStr = conditionsInputStr;
	}
	/**
	 * @param alTransactionList the alTransactionList to set
	 */
	public void setAlTransactionList(ArrayList alTransactionList) {
		this.alTransactionList = alTransactionList;
	}
	/**
	 * @return the selectedTrans
	 */
	public String[] getSelectedTrans() {
		return selectedTrans;
	}
	/**
	 * @param selectedTrans the selectedTrans to set
	 */
	public void setSelectedTrans(String[] selectedTrans) {
		this.selectedTrans = selectedTrans;
	}
	/**
	 * @return the alTransactionList
	 */
	public ArrayList getAlTransactionList() {
		return alTransactionList;
	}
	/**
	 * @return the selectAll
	 */
	public String getSelectAll() {
		return selectAll;
	}
	/**
	 * @param selectAll the selectAll to set
	 */
	public void setSelectAll(String selectAll) {
		this.selectAll = selectAll;
	}
	
	/**
	 * @return the ruleId
	 */
	public String getRuleId() {
		return ruleId;
	}
	/**
	 * @param ruleId the ruleId to set
	 */
	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}
	/**
	 * @return the transStr
	 */
	public String getTransStr() {
		return transStr;
	}
	/**
	 * @param transStr the transStr to set
	 */
	public void setTransStr(String transStr) {
		this.transStr = transStr;
	}
	/**
	 * @return the strAction
	 */
	public String getStrAction() {
		return strAction;
	}
	/**
	 * @param strAction the strAction to set
	 */
	public void setStrAction(String strAction) {
		this.strAction = strAction;
	}
	/**
	 * @return the hmConsequence
	 */
	public HashMap getHmConsequence() {
		return hmConsequence;
	}
	/**
	 * @param hmConsequence the hmConsequence to set
	 */
	public void setHmConsequence(HashMap hmConsequence) {
		this.hmConsequence = hmConsequence;
	}
	/**
	 * @return the alConsTabList
	 */
	public ArrayList getAlConsTabList() {
		return alConsTabList;
	}
	/**
	 * @param alConsTabList the alConsTabList to set
	 */
	public void setAlConsTabList(ArrayList alConsTabList) {
		this.alConsTabList = alConsTabList;
	}
	/**
	 * @return the consequenceGrp
	 */
	public String getConsequenceGrp() {
		return consequenceGrp;
	}
	/**
	 * @param consequenceGrp the consequenceGrp to set
	 */
	public void setConsequenceGrp(String consequenceGrp) {
		this.consequenceGrp = consequenceGrp;
	}
	/**
	 * @return the alConsequenceList
	 */
	public ArrayList getAlConsequenceList() {
		return alConsequenceList;
	}
	/**
	 * @param alConsequenceList the alConsequenceList to set
	 */
	public void setAlConsequenceList(ArrayList alConsequenceList) {
		this.alConsequenceList = alConsequenceList;
	}
	/**
	 * @return the consequenceId
	 */
	public String getConsequenceId() {
		return consequenceId;
	}
	/**
	 * @param consequenceId the consequenceId to set
	 */
	public void setConsequenceId(String consequenceId) {
		this.consequenceId = consequenceId;
	}
	/**
	 * @return the consequenceStr
	 */
	public String getConsequenceStr() {
		return consequenceStr;
	}
	/**
	 * @param consequenceStr the consequenceStr to set
	 */
	public void setConsequenceStr(String consequenceStr) {
		this.consequenceStr = consequenceStr;
	}
	/**
	 * @return the alDbConsequenceList
	 */
	public ArrayList getAlDbConsequenceList() {
		return alDbConsequenceList;
	}
	/**
	 * @param alDbConsequenceList the alDbConsequenceList to set
	 */
	public void setAlDbConsequenceList(ArrayList alDbConsequenceList) {
		this.alDbConsequenceList = alDbConsequenceList;
	}
	/**
	 * @return the ruleConsequenceId
	 */
	public String getRuleConsequenceId() {
		return ruleConsequenceId;
	}
	/**
	 * @param ruleConsequenceId the ruleConsequenceId to set
	 */
	public void setRuleConsequenceId(String ruleConsequenceId) {
		this.ruleConsequenceId = ruleConsequenceId;
	}
	/**
	 * @return the alSelectedConsList
	 */
	public ArrayList getAlSelectedConsList() {
		return alSelectedConsList;
	}
	/**
	 * @param alSelectedConsList the alSelectedConsList to set
	 */
	public void setAlSelectedConsList(ArrayList alSelectedConsList) {
		this.alSelectedConsList = alSelectedConsList;
	}
	/**
	 * @return the ruleErrorMsg
	 */
	public String getRuleErrorMsg() {
		return ruleErrorMsg;
	}
	/**
	 * @param ruleErrorMsg the ruleErrorMsg to set
	 */
	public void setRuleErrorMsg(String ruleErrorMsg) {
		this.ruleErrorMsg = ruleErrorMsg;
	}
	/**
	 * @return the ruleComment
	 */
	public String getRuleComment() {
		return ruleComment;
	}
	/**
	 * @param ruleComment the ruleComment to set
	 */
	public void setRuleComment(String ruleComment) {
		this.ruleComment = ruleComment;
	}
	/**
	 * @return the ruleEmailNotify
	 */
	public String getRuleEmailNotify() {
		return ruleEmailNotify;
	}
	/**
	 * @param ruleEmailNotify the ruleEmailNotify to set
	 */
	public void setRuleEmailNotify(String ruleEmailNotify) {
		this.ruleEmailNotify = ruleEmailNotify;
	}
	public ArrayList getAlOperators() {
		return alOperators;
	}
	public void setAlOperators(ArrayList alOperators) {
		this.alOperators = alOperators;
	}
	public ArrayList getAlConditions() {
		return alConditions;
	}
	public void setAlConditions(ArrayList alConditions) {
		this.alConditions = alConditions;
	}
	public ArrayList getAlCountries() {
		return alCountries;
	}
	public void setAlCountries(ArrayList alCountries) {
		this.alCountries = alCountries;
	}
	public ArrayList getAlVendors() {
		return alVendors;
	}
	public void setAlVendors(ArrayList alVendors) {
		this.alVendors = alVendors;
	}
	public ArrayList getAlSetList() {
		return alSetList;
	}
	public void setAlSetList(ArrayList alSetList) {
		this.alSetList = alSetList;
	}
	/**
	 * @return the enableCreateRule
	 */
	public String getEnableCreateRule() {
		return enableCreateRule;
	}
	/**
	 * @param enableCreateRule the enableCreateRule to set
	 */
	public void setEnableCreateRule(String enableCreateRule) {
		this.enableCreateRule = enableCreateRule;
	}
	/**
	 * @return the enableVoidRule
	 */
	public String getEnableVoidRule() {
		return enableVoidRule;
	}
	/**
	 * @param enableVoidRule the enableVoidRule to set
	 */
	public void setEnableVoidRule(String enableVoidRule) {
		this.enableVoidRule = enableVoidRule;
	}
	/**
	 * @return the editFlag
	 */
	public String getEditFlag() {
		return editFlag;
	}
	/**
	 * @param editFlag the editFlag to set
	 */
	public void setEditFlag(String editFlag) {
		this.editFlag = editFlag;
	}
	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}
	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}	
	
	/**
	 * @return the ruleTxnType
	 */
	public String getRuleTxnType() {
		return ruleTxnType;
	}
	/**
	 * @param ruleTxnType the ruleTxnType to set
	 */
	public void setRuleTxnType(String ruleTxnType) {
		this.ruleTxnType = ruleTxnType;
	}
	/**
	 * @return the alRuleTxnType
	 */
	public ArrayList getAlRuleTxnType() {
		return alRuleTxnType;
	}
	/**
	 * @param alRuleTxnType the alRuleTxnType to set
	 */
	public void setAlRuleTxnType(ArrayList alRuleTxnType) {
		this.alRuleTxnType = alRuleTxnType;
	}
	/**
	 * @return the hmTrans
	 */
	public HashMap getHmTrans() {
		return hmTrans;
	}
	/**
	 * @param hmTrans the hmTrans to set
	 */
	public void setHmTrans(HashMap hmTrans) {
		this.hmTrans = hmTrans;
	}
	


}
