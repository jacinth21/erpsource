//Source file: C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\common\forms\\GmStatusLogForm.java

package com.globus.common.forms;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author Angela
 */
public class GmStatusLogForm extends GmCommonForm {
	private ArrayList alStatus = new ArrayList();
	private ArrayList alTypes = new ArrayList();
	private ArrayList alUsers = new ArrayList();
	private ArrayList alReportList = new ArrayList();
	private ArrayList aldetailList = new ArrayList();
	private ArrayList alLocations = new ArrayList();
	private ArrayList alSource = new ArrayList();
	private HashMap hmTypeVal = new HashMap();

	private String statusNum = "";
	private String typeNum = "";
	private String userNum = "";
	private String startDate = "";
	private String endDate = "";
	private String refID = "";
	private String fromLocationID = "";
	private String toLocationID = "";
	private String source = "";
	private String txnID = "";
	private String strDateDiff = "";

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public ArrayList getAlLocations() {
		return alLocations;
	}

	public void setAlLocations(ArrayList alLocations) {
		this.alLocations = alLocations;
	}

	public String getFromLocationID() {
		return fromLocationID;
	}

	public void setFromLocationID(String fromLocationID) {
		this.fromLocationID = fromLocationID;
	}

	public String getToLocationID() {
		return toLocationID;
	}

	public void setToLocationID(String toLocationID) {
		this.toLocationID = toLocationID;
	}

	public ArrayList getAlStatus() {
		return alStatus;
	}

	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}

	public ArrayList getAlTypes() {
		return alTypes;
	}

	public void setAlTypes(ArrayList alTypes) {
		this.alTypes = alTypes;
	}

	public ArrayList getAlUsers() {
		return alUsers;
	}

	public void setAlUsers(ArrayList alUsers) {
		this.alUsers = alUsers;
	}

	public ArrayList getAlReportList() {
		return alReportList;
	}

	public void setAlReportList(ArrayList alReportList) {
		this.alReportList = alReportList;
	}

	public ArrayList getAldetailList() {
		return aldetailList;
	}

	public void setAldetailList(ArrayList aldetailList) {
		this.aldetailList = aldetailList;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getRefID() {
		return refID;
	}

	public void setRefID(String refID) {
		this.refID = refID;
	}

	public String getStatusNum() {
		return statusNum;
	}

	public void setStatusNum(String statusNum) {
		this.statusNum = statusNum;
	}

	public String getTypeNum() {
		return typeNum;
	}

	public void setTypeNum(String typeNum) {
		this.typeNum = typeNum;
	}

	public String getUserNum() {
		return userNum;
	}

	public void setUserNum(String userNum) {
		this.userNum = userNum;
	}


	/**
	 * @param txnID the txnID to set
	 */
	public void setTxnID(String txnID) {
		this.txnID = txnID;
	}

	/**
	 * @return the txnID
	 */
	public String getTxnID() {
		return txnID;
	}

	/**
	 * @param alSource the alSource to set
	 */
	public void setAlSource(ArrayList alSource) {
		this.alSource = alSource;
	}

	/**
	 * @return the alSource
	 */
	public ArrayList getAlSource() {
		return alSource;
	}

	/**
	 * @param hmTypeVal the hmTypeVal to set
	 */
	public void setHmTypeVal(HashMap hmTypeVal) {
		this.hmTypeVal = hmTypeVal;
	}

	/**
	 * @return the hmTypeVal
	 */
	public HashMap getHmTypeVal() {
		return hmTypeVal;
	}

	/**
	 * @param strDateDiff the strDateDiff to set
	 */
	public void setStrDateDiff(String strDateDiff) {
		this.strDateDiff = strDateDiff;
	}

	/**
	 * @return the strDateDiff
	 */
	public String getStrDateDiff() {
		return strDateDiff;
	}

}