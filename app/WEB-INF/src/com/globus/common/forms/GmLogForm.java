package com.globus.common.forms;

import java.util.ArrayList;

public class GmLogForm extends GmCommonForm
{

    private String txt_LogReason = "";
    private ArrayList alLogReasons = new ArrayList();
    /**
     * @return Returns the alLogReasons.
     */
    public ArrayList getAlLogReasons()
    {
        return alLogReasons;
    }
    /**
     * @param alLogReasons The alLogReasons to set.
     */
    public void setAlLogReasons(ArrayList alLogReasons)
    {
        this.alLogReasons = alLogReasons;
    }
    /**
     * @return Returns the txt_LogReason.
     */
    public String getTxt_LogReason()
    {
        return txt_LogReason;
    }
    /**
     * @param txt_LogReason The txt_LogReason to set.
     */
    public void setTxt_LogReason(String txt_LogReason)
    {
        this.txt_LogReason = txt_LogReason;
    }
    
}
