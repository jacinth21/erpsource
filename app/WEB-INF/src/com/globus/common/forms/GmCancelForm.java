package com.globus.common.forms;

public class GmCancelForm extends GmLogForm
{
    private String hTxnId = "";
    private String hTxnName = "";
    private String hCancelType = "";
    /**
     * @return Returns the hCancelType.
     */
    public String getHCancelType()
    {
        return hCancelType;
    }
    /**
     * @param cancelType The hCancelType to set.
     */
    public void setHCancelType(String cancelType)
    {
        hCancelType = cancelType;
    }
    /**
     * @return Returns the hTxnId.
     */
    public String getHTxnId()
    {
        return hTxnId;
    }
    /**
     * @param txnId The hTxnId to set.
     */
    public void setHTxnId(String txnId)
    {
        hTxnId = txnId;
    }
    /**
     * @return Returns the hTxnName.
     */
    public String getHTxnName()
    {
        return hTxnName;
    }
    /**
     * @param txnName The hTxnName to set.
     */
    public void setHTxnName(String txnName)
    {
        hTxnName = txnName;
    }
}
