package com.globus.common.forms;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Xun
 */

public class GmAuditPriceLogForm extends GmCommonForm
{
     
    private List ldtResult = null;
    private String txnId;
    private String auditId;
   
	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	/**
     * @return Returns the ldtResult.
     */
    public List getLdtResult()
    {
        return ldtResult;
    }
 
    /**
     * @param ldtResult The ldtResult to set.
     */
    public void setLdtResult(List ldtResult)
    {
        this.ldtResult = ldtResult;
    }

	public String getAuditId() {
		return auditId;
	}

	public void setAuditId(String auditId) {
		this.auditId = auditId;
	}   
}