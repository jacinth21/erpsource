package com.globus.common.forms;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmDateConverter;
import com.globus.common.beans.GmFramework;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 */
public class GmCommonForm extends ActionForm {
  private static final long serialVersionUID = 1234567890L;

  protected String haction = "";
  protected String strOpt = "";
  private String userId = "";
  private String deptId = "";
  private String sessPartyId = "";
  private String applnDateFmt = "";
  private String applnCurrFmt = "";
  private String applnCurrSign = "";
  private String browserType = "";
  private String clientSysType = "";
  private String partyId = "";

  public String getBrowserType() {
    return browserType;
  }


  public void setBrowserType(String browserType) {
    this.browserType = browserType;
  }

  public String getClientSysType() {
    return clientSysType;
  }


  public void setClientSysType(String clientSysType) {
    this.clientSysType = clientSysType;
  }


  public String getApplnCurrFmt() {
    return applnCurrFmt;
  }


  public void setApplnCurrFmt(String applnCurrFmt) {
    this.applnCurrFmt = applnCurrFmt;
  }


  public String getApplnCurrSign() {
    return applnCurrSign;
  }


  public void setApplnCurrSign(String applnCurrSign) {
    this.applnCurrSign = applnCurrSign;
  }

  public String getDeptId() {
    return deptId;
  }


  public void setDeptId(String deptId) {
    this.deptId = deptId;
  }


  public void reset() {
    System.out.println("Inside SQL");
  }


  /**
   * @return Returns the haction.
   */
  public String getHaction() {
    return haction;
  }


  /**
   * @param haction The haction to set.
   */
  public void setHaction(String haction) {
    this.haction = haction;
  }


  /**
   * @return Returns the strOpt.
   */
  public String getStrOpt() {
    return strOpt;
  }

  /**
   * @param strOpt The strOpt to set.
   */
  public void setStrOpt(String strOpt) {
    this.strOpt = strOpt;
  }

  /**
   * @return Returns the userId.
   */
  public String getUserId() {
    return userId;
  }

  /**
   * @param userId The userId to set.
   */
  public void setUserId(String userId) {
    this.userId = userId;
  }

  /**
   * @return the sessPartyId
   */
  public String getSessPartyId() {
    return sessPartyId;
  }


  /**
   * @param sessPartyId the sessPartyId to set
   */
  public void setSessPartyId(String sessPartyId) {
    this.sessPartyId = sessPartyId;
  }


  public String getApplnDateFmt() {
    return applnDateFmt;
  }


  public void setApplnDateFmt(String applnDateFmt) {
    this.applnDateFmt = applnDateFmt;
  }

  public String getPartyId() {
    return partyId;
  }


  public void setPartyId(String partyId) {
    this.partyId = partyId;
  }

  /**
   * @param request get all the session attributs required in the bean and set it to Action form
   */
  public void loadSessionParameters(HttpServletRequest request) {
    HttpSession session = request.getSession();
    GmDataStoreVO gmDataStoreVO = GmFramework.getCompanyParams(request);
    String strDateFmt = gmDataStoreVO.getCmpdfmt();
    strDateFmt = (!strDateFmt.equals(""))?strDateFmt:GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
    setUserId(GmCommonClass.parseNull((String) session.getAttribute("strSessUserId")));
    setDeptId(GmCommonClass.parseNull((String) session.getAttribute("strSessDeptSeq")));
    setApplnDateFmt(strDateFmt);
    setSessPartyId(GmCommonClass.parseNull((String) session.getAttribute("strSessPartyId")));
    setApplnCurrFmt(GmCommonClass.parseNull((String) session.getAttribute("strSessApplCurrFmt")));
    setApplnCurrSign(GmCommonClass.parseNull((String) session.getAttribute("strSessCurrSymbol")));
    setBrowserType(GmCommonClass.parseNull((String) session.getAttribute("strSessBrowserType")));
    setClientSysType(GmCommonClass.parseNull((String) session.getAttribute("strSessClientSysType")));
  }

  @Override
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    // TODO Auto-generated method stub
    super.reset(mapping, request);
    HttpSession session = request.getSession();
    GmDataStoreVO gmDataStoreVO = GmFramework.getCompanyParams(request);
    String strDateFmt = gmDataStoreVO.getCmpdfmt();
    strDateFmt = (!strDateFmt.equals(""))?strDateFmt:GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
    setApplnDateFmt(strDateFmt);
    setApplnCurrFmt(GmCommonClass.parseNull((String) session.getAttribute("strSessApplCurrFmt")));
    setApplnCurrSign(GmCommonClass.parseNull((String) session.getAttribute("strSessCurrSymbol")));
    ConvertUtils.register(new GmDateConverter(getApplnDateFmt()), Date.class);


  }

}
