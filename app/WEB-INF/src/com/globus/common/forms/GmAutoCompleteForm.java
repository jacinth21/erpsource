package com.globus.common.forms;


/**
 * @author mmuthusamy
 */

public class GmAutoCompleteForm extends GmCommonForm {

  private String strSearchKey = "";
  private String strMaxCount = "";

  /**
   * @return the strKey
   */
  public String getStrSearchKey() {
    return strSearchKey;
  }

  /**
   * @param strKey the strKey to set
   */
  public void setStrSearchKey(String strSearchKey) {
    this.strSearchKey = strSearchKey;
  }

  /**
   * @return the strMaxCount
   */
  public String getStrMaxCount() {
    return strMaxCount;
  }

  /**
   * @param strMaxCount the strMaxCount to set
   */
  public void setStrMaxCount(String strMaxCount) {
    this.strMaxCount = strMaxCount;
  }

}
