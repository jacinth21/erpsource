package com.globus.common.forms;

import org.apache.struts.upload.FormFile;

/**
 * @author Velu
 */
public class GmUploadForm extends GmLogForm {
	private String strXmlData = "";
	private String strRefID = "";
	private String refType = "";
	private String strTypeName = "";
	private String strOverrideFl = "";
	private FormFile file;
	private String fileName = "";
	private String logType = "";
	private int intRecSize = 0;
	private String message= "";
	private String strUpdAccessFl ="";
	

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the logType
	 */
	public String getLogType() {
		return logType;
	}
	/**
	 * @param logType the logType to set
	 */
	public void setLogType(String logType) {
		this.logType = logType;
	}
	/**
	 * @return the intRecSize
	 */
	public int getIntRecSize() {
		return intRecSize;
	}
	/**
	 * @param intRecSize the intRecSize to set
	 */
	public void setIntRecSize(int intRecSize) {
		this.intRecSize = intRecSize;
	}
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return the strTypeName
	 */
	public String getStrTypeName() {
		return strTypeName;
	}
	/**
	 * @param strTypeName the strTypeName to set
	 */
	public void setStrTypeName(String strTypeName) {
		this.strTypeName = strTypeName;
	}
	/**
	 * @return the strOverrideFl
	 */
	public String getStrOverrideFl() {
		return strOverrideFl;
	}
	/**
	 * @param strOverrideFl the strOverrideFl to set
	 */
	public void setStrOverrideFl(String strOverrideFl) {
		this.strOverrideFl = strOverrideFl;
	}
	/**
	 * @return the file
	 */
	public FormFile getFile() {
		return file;
	}
	/**
	 * @param file the file to set
	 */
	public void setFile(FormFile file) {
		this.file = file;
	}
	/**
	 * @return the strXmlData
	 */
	public String getStrXmlData() {
		return strXmlData;
	}
	/**
	 * @return the refType
	 */
	public String getRefType() {
		return refType;
	}
	/**
	 * @param refType the refType to set
	 */
	public void setRefType(String refType) {
		this.refType = refType;
	}
	/**
	 * @param strXmlData the strXmlData to set
	 */
	public void setStrXmlData(String strXmlData) {
		this.strXmlData = strXmlData;
	}
	/**
	 * @return the strRefID
	 */
	public String getStrRefID() {
		return strRefID;
	}
	/**
	 * @param strRefID the strRefID to set
	 */
	public void setStrRefID(String strRefID) {
		this.strRefID = strRefID;
	}
	
	/**
	 * @param strUpdAccessFl the strUpdAccessFl to set
	 */
	public void setStrUpdAccessFl(String strUpdAccessFl) {
		this.strUpdAccessFl = strUpdAccessFl;
	}
	/**
	 * @return the strUpdAccessFl
	 */
	public String getStrUpdAccessFl() {
		return strUpdAccessFl;
	}
}