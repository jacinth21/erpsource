/**
 * 
 */
package com.globus.common.forms;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmLogger;

/**
	 * 
	 */

public class GmCommonCurrConvForm extends GmLogForm {
  Logger log = GmLogger.getInstance(this.getClass().getName());
  private static final long serialVersionUID = 1L;

  private ArrayList alCurrFrom = new ArrayList();
  private ArrayList alCurrTo = new ArrayList();
  private List alCurrConv = new ArrayList();
  private ArrayList alConvType = new ArrayList();
  private ArrayList alTransList = new ArrayList();
  private ArrayList alSource = new ArrayList();


  private String currFrom = "";
  private String currTo = "";
  private String currId = "";
  private String currValue = "";
  private String addIdReturned = ""; // added this parameter to be returned back on the opening jsp.
  private Date dtCurrFromDT = null;
  private Date dtCurrToDT = null;
  private String convType = "";
  private String monthRateFl = "";

  private String transIds = "";
  private String transcnt = "";
  private String currfromnm = "";
  private String currtonm = "";
  private String createdby = "";
  private String updatedby = "";
  private Date dtcreateddate = null;
  private Date dtupdateddt = null;

  private String[] checkSelectedSource = new String[10];
  private String selectedSource = "";
  private String sourceInputStr = "";
  
  private Date fromDate = null;
  private Date toDate = null;
  private String fromCurrency = "";
  private String toCurrency = "";
  private String gridData = "";
  ArrayList alCurrency = new ArrayList();

  private String fromMonth = GmCalenderOperations.getCurrentMM();
  private String fromYear = String.valueOf(GmCalenderOperations.getCurrentYear());
  private String toMonth = GmCalenderOperations.getCurrentMM();
  private String toYear = String.valueOf(GmCalenderOperations.getCurrentYear());

  ArrayList alMonthList = new ArrayList();
  ArrayList alYearList = new ArrayList();

  private String selectAll = "";
  private String selectedCurrency = "";
  private String[] checkSelectedCurrency = new String [10];
  private String strEditSubmitBtnAccess  = "";

  HashMap hmCrossTabData = new HashMap();


  /**
   * @return the alCurrFrom
   */
  public ArrayList getAlCurrFrom() {
    return alCurrFrom;
  }

  /**
   * @param alCurrFrom the alCurrFrom to set
   */
  public void setAlCurrFrom(ArrayList alCurrFrom) {
    this.alCurrFrom = alCurrFrom;
  }

  /**
   * @return the alCurrTo
   */
  public ArrayList getAlCurrTo() {
    return alCurrTo;
  }

  /**
   * @param alCurrTo the alCurrTo to set
   */
  public void setAlCurrTo(ArrayList alCurrTo) {
    this.alCurrTo = alCurrTo;
  }

  /**
   * @return the addIdReturned
   */
  public String getAddIdReturned() {
    return addIdReturned;
  }

  /**
   * @param addIdReturned the addIdReturned to set
   */
  public void setAddIdReturned(String addIdReturned) {
    this.addIdReturned = addIdReturned;
  }

  /**
   * @return the currId
   */
  public String getCurrId() {
    return currId;
  }

  /**
   * @param currId the currId to set
   */
  public void setCurrId(String currId) {
    this.currId = currId;
  }

  /**
   * @return the currFrom
   */
  public String getCurrFrom() {
    return currFrom;
  }

  /**
   * @param currFrom the currFrom to set
   */
  public void setCurrFrom(String currFrom) {
    this.currFrom = currFrom;
  }

  /**
   * @return the currTo
   */
  public String getCurrTo() {
    return currTo;
  }

  /**
   * @param currTo the currTo to set
   */
  public void setCurrTo(String currTo) {
    this.currTo = currTo;
  }

  /**
   * @return the alCurrConv
   */
  public List getAlCurrConv() {
    return alCurrConv;
  }

  /**
   * @param alCurrConv the alCurrConv to set
   */
  public void setAlCurrConv(List alCurrConv) {
    this.alCurrConv = alCurrConv;
  }

  /**
   * @return the currValue
   */
  public String getCurrValue() {
    return currValue;
  }

  /**
   * @param currValue the currValue to set
   */
  public void setCurrValue(String currValue) {
    this.currValue = currValue;
  }

  public Date getDtCurrFromDT() {
    return dtCurrFromDT;
  }

  public void setDtCurrFromDT(Date dtCurrFromDT) {
    this.dtCurrFromDT = dtCurrFromDT;
  }

  public Date getDtCurrToDT() {
    return dtCurrToDT;
  }

  public void setDtCurrToDT(Date dtCurrToDT) {
    this.dtCurrToDT = dtCurrToDT;
  }


  public String getConvType() {
    return convType;
  }

  public void setConvType(String convType) {
    this.convType = convType;
  }

  public ArrayList getAlConvType() {
    return alConvType;
  }

  public void setAlConvType(ArrayList alConvType) {
    this.alConvType = alConvType;
  }

  /**
   * @param monthRateFl the monthRateFl to set
   */
  public void setMonthRateFl(String monthRateFl) {
    this.monthRateFl = monthRateFl;
  }

  /**
   * @return the monthRateFl
   */
  public String getMonthRateFl() {
    return monthRateFl;
  }

  /**
   * @return the transIds
   */
  public String getTransIds() {
    return transIds;
  }

  /**
   * @param transIds the transIds to set
   */
  public void setTransIds(String transIds) {
    this.transIds = transIds;
  }

  /**
   * @return the transcnt
   */
  public String getTranscnt() {
    return transcnt;
  }

  /**
   * @param transcnt the transcnt to set
   */
  public void setTranscnt(String transcnt) {
    this.transcnt = transcnt;
  }

  /**
   * @return the alTransList
   */
  public ArrayList getAlTransList() {
    return alTransList;
  }

  /**
   * @param alTransList the alTransList to set
   */
  public void setAlTransList(ArrayList alTransList) {
    this.alTransList = alTransList;
  }

  /**
   * @return the currfromnm
   */
  public String getCurrfromnm() {
    return currfromnm;
  }

  /**
   * @param currfromnm the currfromnm to set
   */
  public void setCurrfromnm(String currfromnm) {
    this.currfromnm = currfromnm;
  }

  /**
   * @return the currtonm
   */
  public String getCurrtonm() {
    return currtonm;
  }

  /**
   * @param currtonm the currtonm to set
   */
  public void setCurrtonm(String currtonm) {
    this.currtonm = currtonm;
  }

  /**
   * @return the createdby
   */
  public String getCreatedby() {
    return createdby;
  }

  /**
   * @param createdby the createdby to set
   */
  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  /**
   * @return the updatedby
   */
  public String getUpdatedby() {
    return updatedby;
  }

  /**
   * @param updatedby the updatedby to set
   */
  public void setUpdatedby(String updatedby) {
    this.updatedby = updatedby;
  }

  /**
   * @return the dtcreateddate
   */
  public Date getDtcreateddate() {
    return dtcreateddate;
  }

  /**
   * @param dtcreateddate the dtcreateddate to set
   */
  public void setDtcreateddate(Date dtcreateddate) {
    this.dtcreateddate = dtcreateddate;
  }

  /**
   * @return the dtupdateddt
   */
  public Date getDtupdateddt() {
    return dtupdateddt;
  }

  /**
   * @param dtupdateddt the dtupdateddt to set
   */
  public void setDtupdateddt(Date dtupdateddt) {
    this.dtupdateddt = dtupdateddt;
  }

  /**
   * @return the alSource
   */
  public ArrayList getAlSource() {
    return alSource;
  }

  /**
   * @param alSource the alSource to set
   */
  public void setAlSource(ArrayList alSource) {
    this.alSource = alSource;
  }

  /**
   * @return the selectedSource
   */
  public String getSelectedSource() {
    return selectedSource;
  }

  /**
   * @param selectedSource the selectedSource to set
   */
  public void setSelectedSource(String selectedSource) {
    this.selectedSource = selectedSource;
  }

  /**
   * @return the checkSelectedSource
   */
  public String[] getCheckSelectedSource() {
    return checkSelectedSource;
  }

  /**
   * @param checkSelectedSource the checkSelectedSource to set
   */
  public void setCheckSelectedSource(String[] checkSelectedSource) {
    this.checkSelectedSource = checkSelectedSource;
  }

  /**
   * @return the sourceInputStr
   */
  public String getSourceInputStr() {
    return sourceInputStr;
  }

  /**
   * @param sourceInputStr the sourceInputStr to set
   */
  public void setSourceInputStr(String sourceInputStr) {
    this.sourceInputStr = sourceInputStr;
  }
  /**
   * @return the hmCrossTabData
   */
  public HashMap getHmCrossTabData() {
    return hmCrossTabData;
  }

  /**
   * @param hmCrossTabData the hmCrossTabData to set
   */
  public void setHmCrossTabData(HashMap hmCrossTabData) {
    this.hmCrossTabData = hmCrossTabData;
  }


  /**
   * @return the selectAll
   */
  public String getSelectAll() {
    return selectAll;
  }

  /**
   * @param selectAll the selectAll to set
   */
  public void setSelectAll(String selectAll) {
    this.selectAll = selectAll;
  }

  /**
   * @return the alCurrency
   */
  public ArrayList getAlCurrency() {
    return alCurrency;
  }

  /**
   * @param alCurrency the alCurrency to set
   */
  public void setAlCurrency(ArrayList alCurrency) {
    this.alCurrency = alCurrency;
  }

  /**
   * @return the fromDate
   */
  public Date getFromDate() {
    return fromDate;
  }

  /**
   * @param fromDate the fromDate to set
   */
  public void setFromDate(Date fromDate) {
    this.fromDate = fromDate;
  }

  /**
   * @return the toDate
   */
  public Date getToDate() {
    return toDate;
  }

  /**
   * @param toDate the toDate to set
   */
  public void setToDate(Date toDate) {
    this.toDate = toDate;
  }

  /**
   * @return the fromCurrency
   */
  public String getFromCurrency() {
    return fromCurrency;
  }

  /**
   * @param fromCurrency the fromCurrency to set
   */
  public void setFromCurrency(String fromCurrency) {
    this.fromCurrency = fromCurrency;
  }

  /**
   * @return the toCurrency
   */
  public String getToCurrency() {
    return toCurrency;
  }

  /**
   * @param toCurrency the toCurrency to set
   */
  public void setToCurrency(String toCurrency) {
    this.toCurrency = toCurrency;
  }

  /**
   * @return the gridData
   */
  public String getGridData() {
    return gridData;
  }

  /**
   * @param gridData the gridData to set
   */
  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  /**
   * @return the fromMonth
   */
  public String getFromMonth() {
    return fromMonth;
  }

  /**
   * @param fromMonth the fromMonth to set
   */
  public void setFromMonth(String fromMonth) {
    this.fromMonth = fromMonth;
  }

  /**
   * @return the fromYear
   */
  public String getFromYear() {
    return fromYear;
  }

  /**
   * @param fromYear the fromYear to set
   */
  public void setFromYear(String fromYear) {
    this.fromYear = fromYear;
  }

  /**
   * @return the toMonth
   */
  public String getToMonth() {
    return toMonth;
  }

  /**
   * @param toMonth the toMonth to set
   */
  public void setToMonth(String toMonth) {
    this.toMonth = toMonth;
  }

  /**
   * @return the toYear
   */
  public String getToYear() {
    return toYear;
  }

  /**
   * @param toYear the toYear to set
   */
  public void setToYear(String toYear) {
    this.toYear = toYear;
  }

  /**
   * @return the alMonthList
   */
  public ArrayList getAlMonthList() {
    return alMonthList;
  }

  /**
   * @param alMonthList the alMonthList to set
   */
  public void setAlMonthList(ArrayList alMonthList) {
    this.alMonthList = alMonthList;
  }

  /**
   * @return the alYearList
   */
  public ArrayList getAlYearList() {
    return alYearList;
  }

  /**
   * @param alYearList the alYearList to set
   */
  public void setAlYearList(ArrayList alYearList) {
    this.alYearList = alYearList;
  }


  /**
   * @return the checkSelectedCurrency
   */
  public String[] getCheckSelectedCurrency() {
    return checkSelectedCurrency;
  }

  /**
   * @param checkSelectedCurrency the checkSelectedCurrency to set
   */
  public void setCheckSelectedCurrency(String[] checkSelectedCurrency) {
    this.checkSelectedCurrency = checkSelectedCurrency;
  }

  /**
   * @return the selectedCurrency
   */
  public String getSelectedCurrency() {
    return selectedCurrency;
  }

  /**
   * @param selectedCurrency the selectedCurrency to set
   */
  public void setSelectedCurrency(String selectedCurrency) {
    this.selectedCurrency = selectedCurrency;
  }

  public String getStrEditSubmitBtnAccess() {
		return strEditSubmitBtnAccess;
	}

  public void setStrEditSubmitBtnAccess(String strEditSubmitBtnAccess) {
	this.strEditSubmitBtnAccess = strEditSubmitBtnAccess;
  }

  
}
