package com.globus.common.forms;

import java.util.Date;

public class GmBatchInfoIncludeForm extends GmCommonForm {

  private String batchID = "";
  private String batchStatusNm = "";
  private String batchStatus = "";
  private String batchTypeNm = "";
  private String batchType = "";
  private String batchCreatedByNm = "";
  private String batchCreatedBy = "";
  private String batchReleasedByNm = "";
  private String batchReleasedBy = "";
  private String batchInitiatedByNm = "";
  private String batchInitiatedBy = "";
  private Date batchReleasedDt = null;
  private Date batchInitiatedDt = null;
  private String batchLastUpdatedByNm = "";
  private String batchLastUpdatedBy = "";


  /**
   * @return the batchID
   */
  public String getBatchID() {
    return batchID;
  }

  /**
   * @param batchID the batchID to set
   */
  public void setBatchID(String batchID) {
    this.batchID = batchID;
  }

  /**
   * @return the batchStatusNm
   */
  public String getBatchStatusNm() {
    return batchStatusNm;
  }

  /**
   * @param batchStatusNm the batchStatusNm to set
   */
  public void setBatchStatusNm(String batchStatusNm) {
    this.batchStatusNm = batchStatusNm;
  }

  /**
   * @return the batchStatus
   */
  public String getBatchStatus() {
    return batchStatus;
  }

  /**
   * @param batchStatus the batchStatus to set
   */
  public void setBatchStatus(String batchStatus) {
    this.batchStatus = batchStatus;
  }

  /**
   * @return the batchTypeNm
   */
  public String getBatchTypeNm() {
    return batchTypeNm;
  }

  /**
   * @param batchTypeNm the batchTypeNm to set
   */
  public void setBatchTypeNm(String batchTypeNm) {
    this.batchTypeNm = batchTypeNm;
  }

  /**
   * @return the batchType
   */
  public String getBatchType() {
    return batchType;
  }

  /**
   * @param batchType the batchType to set
   */
  public void setBatchType(String batchType) {
    this.batchType = batchType;
  }

  /**
   * @return the batchCreatedByNm
   */
  public String getBatchCreatedByNm() {
    return batchCreatedByNm;
  }

  /**
   * @param batchCreatedByNm the batchCreatedByNm to set
   */
  public void setBatchCreatedByNm(String batchCreatedByNm) {
    this.batchCreatedByNm = batchCreatedByNm;
  }

  /**
   * @return the batchCreatedBy
   */
  public String getBatchCreatedBy() {
    return batchCreatedBy;
  }

  /**
   * @param batchCreatedBy the batchCreatedBy to set
   */
  public void setBatchCreatedBy(String batchCreatedBy) {
    this.batchCreatedBy = batchCreatedBy;
  }

  /**
   * @return the batchReleasedByNm
   */
  public String getBatchReleasedByNm() {
    return batchReleasedByNm;
  }

  /**
   * @param batchReleasedByNm the batchReleasedByNm to set
   */
  public void setBatchReleasedByNm(String batchReleasedByNm) {
    this.batchReleasedByNm = batchReleasedByNm;
  }

  /**
   * @return the batchReleasedBy
   */
  public String getBatchReleasedBy() {
    return batchReleasedBy;
  }

  /**
   * @param batchReleasedBy the batchReleasedBy to set
   */
  public void setBatchReleasedBy(String batchReleasedBy) {
    this.batchReleasedBy = batchReleasedBy;
  }

  /**
   * @return the batchInitiatedByNm
   */
  public String getBatchInitiatedByNm() {
    return batchInitiatedByNm;
  }

  /**
   * @param batchInitiatedByNm the batchInitiatedByNm to set
   */
  public void setBatchInitiatedByNm(String batchInitiatedByNm) {
    this.batchInitiatedByNm = batchInitiatedByNm;
  }

  /**
   * @return the batchInitiatedBy
   */
  public String getBatchInitiatedBy() {
    return batchInitiatedBy;
  }

  /**
   * @param batchInitiatedBy the batchInitiatedBy to set
   */
  public void setBatchInitiatedBy(String batchInitiatedBy) {
    this.batchInitiatedBy = batchInitiatedBy;
  }


  /**
   * @return the batchLastUpdatedByNm
   */
  public String getBatchLastUpdatedByNm() {
    return batchLastUpdatedByNm;
  }

  /**
   * @param batchLastUpdatedByNm the batchLastUpdatedByNm to set
   */
  public void setBatchLastUpdatedByNm(String batchLastUpdatedByNm) {
    this.batchLastUpdatedByNm = batchLastUpdatedByNm;
  }

  /**
   * @return the batchLastUpdatedBy
   */
  public String getBatchLastUpdatedBy() {
    return batchLastUpdatedBy;
  }

  /**
   * @param batchLastUpdatedBy the batchLastUpdatedBy to set
   */
  public void setBatchLastUpdatedBy(String batchLastUpdatedBy) {
    this.batchLastUpdatedBy = batchLastUpdatedBy;
  }

  /**
   * @return the batchReleasedDt
   */
  public Date getBatchReleasedDt() {
    return batchReleasedDt;
  }

  /**
   * @param batchReleasedDt the batchReleasedDt to set
   */
  public void setBatchReleasedDt(Date batchReleasedDt) {
    this.batchReleasedDt = batchReleasedDt;
  }

  /**
   * @return the batchInitiatedDt
   */
  public Date getBatchInitiatedDt() {
    return batchInitiatedDt;
  }

  /**
   * @param batchInitiatedDt the batchInitiatedDt to set
   */
  public void setBatchInitiatedDt(Date batchInitiatedDt) {
    this.batchInitiatedDt = batchInitiatedDt;
  }



}
