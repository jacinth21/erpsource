package com.globus.common.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmBatchReportForm extends GmCommonForm{

	private String batchID = "";
	private String batchFromDt = "";
	private String batchToDt = "";
	private String batchStatus = "";
	private String batchType = "";
	private String screenType = "";
	private String gridData = "";
	private String batchScreenType = "";
	private String jspHeader = "Process Batch";
	
	private ArrayList alBatchStatus = new ArrayList();
	private ArrayList alBatchType = new ArrayList();
	
	
	/**
	 * @return the batchID
	 */
	public String getBatchID() {
		return batchID;
	}
	/**
	 * @param batchID the batchID to set
	 */
	public void setBatchID(String batchID) {
		this.batchID = batchID;
	}
	/**
	 * @return the batchFromDt
	 */
	public String getBatchFromDt() {
		return batchFromDt;
	}
	/**
	 * @param batchFromDt the batchFromDt to set
	 */
	public void setBatchFromDt(String batchFromDt) {
		this.batchFromDt = batchFromDt;
	}
	/**
	 * @return the batchToDt
	 */
	public String getBatchToDt() {
		return batchToDt;
	}
	/**
	 * @param batchToDt the batchToDt to set
	 */
	public void setBatchToDt(String batchToDt) {
		this.batchToDt = batchToDt;
	}
	/**
	 * @return the batchStatus
	 */
	public String getBatchStatus() {
		return batchStatus;
	}
	/**
	 * @param batchStatus the batchStatus to set
	 */
	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}
	/**
	 * @return the batchType
	 */
	public String getBatchType() {
		return batchType;
	}
	/**
	 * @param batchType the batchType to set
	 */
	public void setBatchType(String batchType) {
		this.batchType = batchType;
	}
	/**
	 * @return the screenType
	 */
	public String getScreenType() {
		return screenType;
	}
	/**
	 * @param screenType the screenType to set
	 */
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the batchScreenType
	 */
	public String getBatchScreenType() {
		return batchScreenType;
	}
	/**
	 * @param batchScreenType the batchScreenType to set
	 */
	public void setBatchScreenType(String batchScreenType) {
		this.batchScreenType = batchScreenType;
	}
	/**
	 * @return the jspHeader
	 */
	public String getJspHeader() {
		return jspHeader;
	}
	/**
	 * @param jspHeader the jspHeader to set
	 */
	public void setJspHeader(String jspHeader) {
		this.jspHeader = jspHeader;
	}
	/**
	 * @return the alBatchStatus
	 */
	public ArrayList getAlBatchStatus() {
		return alBatchStatus;
	}
	/**
	 * @param alBatchStatus the alBatchStatus to set
	 */
	public void setAlBatchStatus(ArrayList alBatchStatus) {
		this.alBatchStatus = alBatchStatus;
	}
	/**
	 * @return the alBatchType
	 */
	public ArrayList getAlBatchType() {
		return alBatchType;
	}
	/**
	 * @param alBatchType the alBatchType to set
	 */
	public void setAlBatchType(ArrayList alBatchType) {
		this.alBatchType = alBatchType;
	}
	
	
}
