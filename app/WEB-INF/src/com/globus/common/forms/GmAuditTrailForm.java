package com.globus.common.forms;

import java.util.List;

/**
 * @author Xun
 */

public class GmAuditTrailForm extends GmCommonForm {

  private List ldtResult = null;
  private String txnId;
  private String auditId;
  private String hJNDIConnection = "";

  private String strDynamicAuditTrailFl = "";
  private String strProcedureRuleId = "";
  private String strAuditType = "";
  private String hInputStr ="";
  private String strButtonAccessFl ="";

  /**
   * @return the hJNDIConnection
   */
  public String gethJNDIConnection() {
    return hJNDIConnection;
  }

  /**
   * @param hJNDIConnection the hJNDIConnection to set
   */
  public void sethJNDIConnection(String hJNDIConnection) {
    this.hJNDIConnection = hJNDIConnection;
  }

  public String getTxnId() {
    return txnId;
  }

  public void setTxnId(String txnId) {
    this.txnId = txnId;
  }

  /**
   * @return Returns the ldtResult.
   */
  public List getLdtResult() {
    return ldtResult;
  }

  /**
   * @param ldtResult The ldtResult to set.
   */
  public void setLdtResult(List ldtResult) {
    this.ldtResult = ldtResult;
  }

  public String getAuditId() {
    return auditId;
  }

  public void setAuditId(String auditId) {
    this.auditId = auditId;
  }

  /**
   * @return the strDynamicAuditTrailFl
   */
  public String getStrDynamicAuditTrailFl() {
    return strDynamicAuditTrailFl;
  }

  /**
   * @param strDynamicAuditTrailFl the strDynamicAuditTrailFl to set
   */
  public void setStrDynamicAuditTrailFl(String strDynamicAuditTrailFl) {
    this.strDynamicAuditTrailFl = strDynamicAuditTrailFl;
  }

  /**
   * @return the strProcedureRuleId
   */
  public String getStrProcedureRuleId() {
    return strProcedureRuleId;
  }

  /**
   * @param strProcedureRuleId the strProcedureRuleId to set
   */
  public void setStrProcedureRuleId(String strProcedureRuleId) {
    this.strProcedureRuleId = strProcedureRuleId;
  }

  /**
   * @return the strAuditType
   */
  public String getStrAuditType() {
    return strAuditType;
  }

  /**
   * @param strAuditType the strAuditType to set
   */
  public void setStrAuditType(String strAuditType) {
    this.strAuditType = strAuditType;
  }

/**
 * @return the hInputStr
 */
public String gethInputStr() {
	return hInputStr;
}

/**
 * @param hInputStr the hInputStr to set
 */
public void sethInputStr(String hInputStr) {
	this.hInputStr = hInputStr;
}

/**
 * @return the strButtonAccessFl
 */
public String getStrButtonAccessFl() {
	return strButtonAccessFl;
}

/**
 * @param strButtonAccessFl the strButtonAccessFl to set
 */
public void setStrButtonAccessFl(String strButtonAccessFl) {
	this.strButtonAccessFl = strButtonAccessFl;
}

}
