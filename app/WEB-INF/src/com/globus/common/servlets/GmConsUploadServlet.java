/**
 * FileName    : GmConsUploadServlet.java 
 * Description :
 * Author      : rshah
 * Date        : Jun 12, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadException;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

/**
 * @author rshah
 *
 */
public class GmConsUploadServlet extends GmUploadServlet {
	private static String strDefaultFileToDispatch = "";
	private static String strErrorFileToDispatch = "";
	private String strFileToDispatch = "";
	private static String strComnPath = GmCommonClass.getString("GMCOMMON");
	private static String strUploadHm = GmCommonClass.getString("CONSUPLOADHOME");
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public void loadFileProperties(String strFileNm)throws AppError {
		log.debug("strUploadHm........"+strUploadHm);
		setStrUploadHome(strUploadHm);
		String fileExt = strFileNm.substring(strFileNm.lastIndexOf('.'));
		strFileNm = strFileNm.substring(strFileNm.lastIndexOf('\\')+1).substring(0, strFileNm.substring(strFileNm.lastIndexOf('\\')+1).lastIndexOf('.'));
		log.debug("fileExt.....2"+fileExt+"....fileNm....."+strFileNm);		
		
		File strFile = new File ( strUploadHm,strFileNm+fileExt );
		if(strFile.exists())
		{
			String strNewFile=strFileNm+fileExt;
			
			File strFileDir = new File(strUploadHm);
			String strFileList[] = strFileDir.list();
			log.debug(".............................."+strFileList.length);
			for(int i=1; i<=strFileList.length+1; i++)
			{				
				String strTempFile = strNewFile.substring(0, strNewFile.substring(strNewFile.lastIndexOf('\\')+1).lastIndexOf('.'))+"_VER_"+i+fileExt;
				File strFileChk = new File(strUploadHm,strTempFile );
				if(!strFileChk.exists())
				{
					strFileNm = strNewFile.substring(0, strNewFile.substring(strNewFile.lastIndexOf('\\')+1).lastIndexOf('.'))+"_VER_"+i;
					log.debug("finally...."+strFileNm);
					break;
				}
			}
						
		}
		
		setStrFileName(strFileNm);
		setStrWhitelist(GmCommonClass.getString("CONSIMAGEWHITELIST"));
		strDefaultFileToDispatch = "/common/GmConsequenceFileUpload.jsp";
		strErrorFileToDispatch = strComnPath + "/GmError.jsp";
	}

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

	}

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		MultipartFormDataRequest mrequest = null;
		instantiate(request, response);
		checkSession(response, session); // Checks if the current session is
		// valid, else redirecting to
		// SessionExpiry page

		try {
			String strAction = "";
			String strFileNm = "";
			String strRuleId = "";
			String strOpt = "";
			String strConsGrpId = "";
			String strConsGrp = "";
			String strHAction = "";
			String strDispatchPath = "";
			String strInputStr = "";
			String strRuleConsId = "";
			strRuleId = GmCommonClass.parseNull((String) request.getParameter("ruleId"));			
			strOpt  = GmCommonClass.parseNull((String) request.getParameter("strOpt"));
			strConsGrpId = GmCommonClass.parseNull((String) request.getParameter("consequenceId"));
			strConsGrp = GmCommonClass.parseNull((String) request.getParameter("consequenceGrp"));
			strAction = GmCommonClass.parseNull((String) request.getParameter("strAction"));
			
			if (MultipartFormDataRequest.isMultipartFormData(request)) 
			{
				mrequest = new MultipartFormDataRequest(request);
				strHAction = GmCommonClass.parseNull((String) mrequest.getParameter("hAction"));
				strFileNm = GmCommonClass.parseNull((String) mrequest.getParameter("fileNm"));
				strRuleId = GmCommonClass.parseNull((String) mrequest.getParameter("ruleId"));			
				strOpt  = GmCommonClass.parseNull((String) mrequest.getParameter("strOpt"));
				strConsGrpId = GmCommonClass.parseNull((String) mrequest.getParameter("consequenceId"));
				strConsGrp = GmCommonClass.parseNull((String) mrequest.getParameter("consequenceGrp"));
				strAction = GmCommonClass.parseNull((String) mrequest.getParameter("strAction"));
				strInputStr = GmCommonClass.parseNull((String) mrequest.getParameter("inputStr"));
				strRuleConsId = GmCommonClass.parseNull((String) mrequest.getParameter("ruleConsequenceId"));
				log.debug("strHAction: "+ strHAction+ "..strFileNm." +strFileNm+"...strRuleConsId..."+strRuleConsId);
				
			}
			log.debug("strAction: "+ strAction+ " strRuleId: "+ strRuleId+ " strOpt: "+ strOpt +"..strConsGrpId." +strConsGrpId);
			if (strHAction.equals("upload")) 
			{
				strFileNm = strFileNm.substring(strFileNm.lastIndexOf('\\')+1);				
				loadFileProperties(strFileNm);
				uploadFile(mrequest, request);
			}
			
			if(!strInputStr.equals(""))
			{
				/* eg. Split the string like - 91356;C:\Documents and Settings\rshah\Desktop\CriticalFlagError.JPG|91357;sdfadf|
				 * and build the string like - 91356;riticalFlagError_VER8.JPG|91357;sdfadf|
				*/
				String[] strInputStrArray = strInputStr.split("\\|");
				String[] strInputStrSubArray = strInputStrArray[0].split("\\;");
				strInputStrSubArray[1] = getStrFileName();
				String strInputSubString = arrayToString(strInputStrSubArray, ";");
				strInputStrArray[0] = strInputSubString.substring(0,strInputSubString.length()-1);
				strInputStr = arrayToString(strInputStrArray, "|");
				//strInputStr = strInputSubString +"|"+strInputStrArray[1]+"|"; 
				log.debug("str.."+strInputStr+"...strInputStrArray[0]..."+strInputStrArray[0]);
			}
			strDispatchPath = "/gmRuleConsequence.do?ruleId="+strRuleId+"&ruleConsequenceId="+strRuleConsId+"&consequenceStr="+strInputStr+"&strAction="+strAction+"&hAction="+strHAction+"&strOpt="+strOpt+"&consequenceId="+strConsGrpId+"&consequenceGrp="+strConsGrp;
			request.setAttribute("mrequest", mrequest);
			dispatch(strDispatchPath, request, response);
		} catch (AppError e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			request.setAttribute("hType", e.getType());
			dispatch(strFileToDispatch, request, response);
		} catch (UploadException e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		} catch (Exception uex) {
			session.setAttribute("hAppErrorMessage", uex.getMessage());
			strFileToDispatch = strErrorFileToDispatch;
			gotoPage(strFileToDispatch, request, response);
		}
	}
	/*
	 *	Function to convert Array to String based on given separator 
	 * 
	 * */
	public String arrayToString(String[] str, String sep) {
	    StringBuffer strResult = new StringBuffer();
	    if (str.length > 0) {
	    	strResult.append(str[0]);
	        for (int i=1; i<str.length; i++) {
	        	strResult.append(sep);
	        	strResult.append(str[i]);
	        	strResult.append(sep);
	        }
	    }
	    return strResult.toString();
	}
}
