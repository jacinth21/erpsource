package com.globus.common.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.GmCommonClass;

public class GmShippingServlet extends GmServlet {

	  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  
	  }
	  
	public HashMap loadShipReqParams (HttpServletRequest request){
		
		HashMap hmReturn = new HashMap();
		
		String strShipTo = GmCommonClass.parseNull(request.getParameter("shipTo"));
		String strShipToId = GmCommonClass.parseNull(request.getParameter("names"));
		//log.debug("strShipToId "+strShipToId );
		//String[] strArr = new  String[2]; 
		//strArr = strShipToId.split("|");
		//String RepId = strArr[0];
		//String PartyId = strArr[1];
		String strShipCarr = GmCommonClass.parseNull(request.getParameter("shipCarrier"));
		String strShipMode = GmCommonClass.parseNull(request.getParameter("shipMode"));
		String strAddressId = GmCommonClass.parseNull(request.getParameter("addressid"));
		String strShipId = GmCommonClass.parseNull(request.getParameter("shipId"));
		String strOverrideAttnTo = GmCommonClass.parseNull(request.getParameter("overrideAttnTo"));
		String strShipInstruction = GmCommonClass.parseNull(request.getParameter("shipInstruction"));
		log.debug("strShipId "+strShipId +" =strOverrideAttnTo= "+strOverrideAttnTo+" =strShipInstruction= "+strShipInstruction);
		log.debug("shipping values are "+strShipTo +" "+ strShipToId+" "+strShipCarr +" "+strShipMode +" "+strAddressId);
		hmReturn.put("SHIPTO", strShipTo);
		hmReturn.put("SHIPTOID", strShipToId);
		hmReturn.put("SHIPCARRIER",strShipCarr);
		hmReturn.put("SHIPMODE",strShipMode);
		hmReturn.put("ADDRESSID", strAddressId);	
		hmReturn.put("SHIPID", strShipId);
		hmReturn.put("OVERRIDEATTNTO", strOverrideAttnTo);
		hmReturn.put("SHIPINSTRUCTION", strShipInstruction);
		
		return hmReturn;
	}
}
