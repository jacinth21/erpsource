/**
 * FileName    : GmUploadServlet.java 
 * Description :
 * Author      : tchandure
 * Date        : Oct 30, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.servlets;

import java.io.IOException;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadException;
import javazoom.upload.UploadFile;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;

/**
 * @author tchandure
 * @modified by Satyajit Thadeshwar
 */
public class GmUploadServlet extends GmServlet {
	private String strWhitelist = "";
	private String strUploadHome = "";
	private String strFileName = "";


	/**
	 * Upload the file on server
	 */

	public boolean uploadFile(MultipartFormDataRequest mrequest, HttpServletRequest request) 
	throws UploadException, IOException, AppError {
		boolean flag = true;
		String strMessage ="";
		String strFileSize ="";
		
		UploadBean uploadBean = new UploadBean();

		uploadBean.setOverwrite(true);
		uploadBean.setWhitelist(strWhitelist);
		uploadBean.setFolderstore(strUploadHome);
		log.debug("strUploadHome = " + strUploadHome+" strFileName = "+strFileName);

		Hashtable hmFiles = mrequest.getFiles();

		if ((hmFiles != null) && (!hmFiles.isEmpty())) {

			UploadFile file = (UploadFile) hmFiles.get("uploadfile");
			long filesize = file.getFileSize();

			//Based on the constant Properties max file size can be determined.
			strFileSize = GmCommonClass.parseNull((String) GmCommonClass.getString("MAXFILESIZE"));
			log.debug("filesize =>>>>>>>>>>>>>>>>>>>>>>>>" + filesize);
			if(filesize >Integer.parseInt(strFileSize))
	 	        {
	 	        	strMessage = "The picture size is too big, you can not upload!";
	 	        	 throw new AppError(strMessage , "",'E'); }
			
			if(!strFileName.equals("")) {
				String filePath = file.getFileName();
				String fileExt = filePath.substring(filePath.lastIndexOf("."));
				log.debug("filePath = " + filePath);
				log.debug("fileExt = " + fileExt);
				file.setFileName(strFileName + fileExt);
			}

			uploadBean.store(mrequest, "uploadfile");
			strFileName = file.getFileName();
			log.debug("strFileName = " + strFileName);
		}

		return flag;
	}
	
	/**
	 * Upload the file on server
	 */
/*
	public boolean uploadFile(MultipartFormDataRequest mrequest, HttpServletRequest request, boolean flag) 
	throws UploadException, IOException {		

		UploadBean uploadBean = new UploadBean();

		uploadBean.setOverwrite(false);
		uploadBean.setWhitelist(strWhitelist);
		uploadBean.setFolderstore(strUploadHome);
		log.debug("strUploadHome = " + strUploadHome+" strFileName = "+strFileName);

		Hashtable hmFiles = mrequest.getFiles();
		log.debug(hmFiles);
		if ((hmFiles != null) && (!hmFiles.isEmpty())) {

			UploadFile file = (UploadFile) hmFiles.get("uploadfile");

			if(!strFileName.equals("")) {
				String filePath = file.getFileName();
				String fileExt = filePath.substring(filePath.lastIndexOf("."));
				log.debug("filePath = " + filePath);
				log.debug("fileExt = " + fileExt);
				file.setFileName(strFileName + fileExt);
			}

			uploadBean.store(mrequest, "uploadfile");
			strFileName = file.getFileName();
			log.debug("strFileName = " + strFileName);
		}

		return flag;
	}
	*/
	public String getStrFileName() {
		return strFileName;
	}

	public void setStrFileName(String strFileName) {
		this.strFileName = strFileName;
	}

	public String getStrWhitelist() {
		return strWhitelist;
	}

	public void setStrWhitelist(String strWhitelist) {
		this.strWhitelist = strWhitelist;
	}

	public String getStrUploadHome() {
		return strUploadHome;
	}

	public void setStrUploadHome(String strUploadHome) {
		this.strUploadHome = strUploadHome;
	}

}
