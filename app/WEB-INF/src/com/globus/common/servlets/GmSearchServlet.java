package com.globus.common.servlets;

import com.globus.common.beans.*;
import com.globus.custservice.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;


/*****************************************************************************
 * File			 : GmPartSearchServlet.java
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/

public class GmSearchServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(true);
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
		// String strDispatchTo = strCustPath.concat("/GmPartSearch.jsp");
		String strDispatchTo = strCustPath.concat("/GmCustSvcSearch.jsp");
		GmLogger log = GmLogger.getInstance(); // Gets an instance of Logger class
		log.debug("Inside Search Servlet");
		
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			instantiate(request, response);
				GmSearchBean gmSearch = new GmSearchBean(getGmDataStoreVO());
				GmCustomerBean gmCust = new GmCustomerBean(getGmDataStoreVO());
				
				HashMap hmReturn = new HashMap();
				HashMap hmPartParam = new HashMap();
				HashMap hmRepParam = new HashMap();
				HashMap hmAccParam = new HashMap();
				ArrayList alSearchReport;
				ArrayList alRepSearchResult;
				ArrayList alAccSearchResult;
				String strPartNum = "";
				String strDesc = "";
				String strRepName = "";
				String strRepNum = "";
				String strRepPhNum = "";
				String strAccName = "";
				String strAccId = "";
				String strAccCity = "";
				String strCount = "";
				String strShipTo = "";
				String strShipToId = "";
				String strShipAdd = "";
				String strExcSubComp = "";
				String strScrType = "";
				
				String strAction = request.getParameter("hAction");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;
				
				log.debug("strAction value inside Servlet is  " + strAction);
				
				if (strAction.equals("LoadPartSearch"))
				{
					log.debug("Inside Load Search ");
					strPartNum = GmCommonClass.parseNull(request.getParameter("Txt_PartNum"));
					strDesc = GmCommonClass.parseNull(request.getParameter("Txt_Desc"));
					String strExFlag = GmCommonClass.parseNull(request.getParameter("hExcSubComp"));
					
					request.setAttribute("hPartNum",strPartNum);
					request.setAttribute("hDesc",strDesc);
					request.setAttribute("hSearchParam","PartNum");
					request.setAttribute("ExcSubComp",strExFlag);
				}
				
				
				if (strAction.equals("LoadSearch"))
				{
					log.debug("Inside Load Search ");
					strPartNum = request.getParameter("Txt_PartNum")==null?"":request.getParameter("Txt_PartNum");
					strDesc = request.getParameter("Txt_Desc")==null?"":request.getParameter("Txt_Desc");
					request.setAttribute("hPartNum",strPartNum);
					request.setAttribute("hDesc",strDesc);
				}
								
				if (strAction.equals("SearchPart"))
				{
					log.debug("Insude Search Part");
					strPartNum = request.getParameter("Txt_PartNum")==null?"":request.getParameter("Txt_PartNum");
					strDesc = request.getParameter("Txt_Desc")==null?"":request.getParameter("Txt_Desc");
					strCount = request.getParameter("hCount")==null?"":request.getParameter("hCount");
					//strCount = GmCommonClass.parseNull((String)session.getAttribute("hCnt"));
					strAccId = request.getParameter("Txt_AccId")==null?"":request.getParameter("Txt_AccId");
					strExcSubComp = request.getParameter("hExcSubComp")==null?"":request.getParameter("hExcSubComp");
					hmPartParam.put("PARTNUM",strPartNum);
					hmPartParam.put("PARTDESC",strDesc);
					hmPartParam.put("EXC_SUB_COMP",strExcSubComp);
					hmReturn = gmSearch.loadPartSearchReport(hmPartParam);
					
					request.setAttribute("hSearchParam","PartNum");
					request.setAttribute("hPartNum",strPartNum);
					request.setAttribute("hDesc",strDesc);
					request.setAttribute("hCount",strCount);
					request.setAttribute("ExcSubComp",strExcSubComp);
					//session.setAttribute("hCount",strCount);
					log.debug("strCount inside Search Servlet uis **** " +strCount  );
					request.setAttribute("hAccId",strAccId);
					
					log.debug("Count value ins GmSearchServlet is " + strCount);
					alSearchReport = (ArrayList)hmReturn.get("SEARCHREPORT");
					request.setAttribute("alSearchReport",alSearchReport);
				}
				
				if (strAction.equals("SearchRep"))
				{
					log.debug("Insude Search Rep");
					strRepName = request.getParameter("Txt_RepName")==null?"":request.getParameter("Txt_RepName");
					strRepNum = request.getParameter("Txt_RepNum")==null?"":request.getParameter("Txt_RepNum");
					strRepPhNum = request.getParameter("Txt_RepPhNum")==null?"":request.getParameter("Txt_RepPhNum");
					log.debug("Str Rep Name inside Servlet is " + strRepName);
					log.debug("Str Rep Num inside Servlet is " + strRepNum);
					
					hmRepParam.put("REPNAME",strRepName);
					hmRepParam.put("REPNUM",strRepNum);
					hmRepParam.put("REPPHNUM",strRepPhNum);
					alRepSearchResult = gmSearch.loadRepSearchReport(hmRepParam);
					
					request.setAttribute("hSearchParam","SalesRep");
					request.setAttribute("hRepName",strRepName);
					request.setAttribute("hRepNum",strRepNum);
					request.setAttribute("hRepPhNum",strRepPhNum);
					request.setAttribute("alRepSearchResult",alRepSearchResult);
				}
				
				if (strAction.equals("SearchAcc"))
				{
					log.debug("Insude SearchAcc");
					strAccName = request.getParameter("Txt_AccName")==null?"":request.getParameter("Txt_AccName");
					strAccId = request.getParameter("Txt_AccountId")==null?"":request.getParameter("Txt_AccountId");
					strAccCity = request.getParameter("Txt_AccCity")==null?"":request.getParameter("Txt_AccCity");
					log.debug("Str Acc Name inside Servlet is " + strAccName);
					log.debug("Str Acc Id inside Servlet is " + strAccId);
					
					hmAccParam.put("ACCNAME",strAccName);
					hmAccParam.put("ACCID",strAccId);
					hmAccParam.put("ACCCITY",strAccCity);
					alAccSearchResult = gmSearch.loadAccSearchReport(hmAccParam);
					
					request.setAttribute("hSearchParam","AccountDetails");
					request.setAttribute("hAccName",strAccName);
					request.setAttribute("hAccId",strAccId);
					request.setAttribute("hAccCity",strAccCity);
					request.setAttribute("alAccSearchResult",alAccSearchResult);
				}
			/*	
				if (strAction.equals("getAddress"))
				{
					strAccId = request.getParameter("hAccId")==null?"":request.getParameter("hAccId");
					strRepNum = request.getParameter("hRepId")==null?"":request.getParameter("hRepId");
					strShipTo = request.getParameter("hShip")==null?"":request.getParameter("hShip");
					strShipToId = request.getParameter("hShipId")==null?"0":request.getParameter("hShipId");
					strShipAdd = gmCust.getShipAddress(strAccId,strRepNum,strShipTo,strShipToId);
					request.setAttribute("hShipAdd",strShipAdd);
					strDispatchTo = strCustPath.concat("/GmShipAdd.jsp");
					log.debug(" Inside Address "+ strAction);
				}
				*/
				//written for new shipping . above "if (strAction.equals("getAddress"))" will be deleted later
				if (strAction.equals("getAddress"))
				{	
					log.debug(" Inside Address "+ strAction);
					strShipTo = request.getParameter("hShip")==null?"":request.getParameter("hShip");
					strShipToId = request.getParameter("hShipId")==null?"0":request.getParameter("hShipId");
					strScrType = request.getParameter("hScrType")==null?"0":request.getParameter("hScrType");
					strShipAdd = gmCust.getAddress(strShipTo,strShipToId);
					request.setAttribute("hShipAdd",strShipAdd);
					request.setAttribute("hScrType",strScrType);
					strDispatchTo = strCustPath.concat("/GmShipAdd.jsp");
					
				}
				/*if (!strAction.equals("Load"))
				{				
					request.setAttribute("hmReturn",hmReturn);
				} */
								
				dispatch(strDispatchTo,request,response);

		}// End of try
		catch (Exception e)
		{
			 e.printStackTrace();
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of GmPartSearchServlet