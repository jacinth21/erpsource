/**
 * FileName    : GmFilterAjaxServlet.java 
 * Description :
 * Author      : rshah
 * Date        : Jun 4, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFilterBean;

/**
 * @author rshah
 * 
 */
public class GmFilterAjaxServlet extends GmServlet {
	
	private ServletContext context;
	String strComnPath = GmCommonClass.getString("GMCOMMON");

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.context = config.getServletContext();
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String strRuleId = "";
		String strDispatchTo = strComnPath.concat("/GmFilterInclude.jsp");

		HashMap hmReturn = new HashMap();
		HashMap hmConditionValues = new HashMap();
		HashMap hmConditionOpr = new HashMap();
		ArrayList alConditions = new ArrayList();
		ArrayList alRuleConList = new ArrayList();
		GmFilterBean gmFilterBean = new GmFilterBean();
		try {
			strRuleId = GmCommonClass.parseNull((String) request.getParameter("ruleId"));
			
			if (!strRuleId.equals("0") && !strRuleId.equals("")) {
				alRuleConList = GmCommonClass.parseNullArrayList(gmFilterBean.fetchRulesCondition(strRuleId));
				hmReturn.put("ALRULECONLIST", alRuleConList);
			}

			alConditions = gmFilterBean.fetchConditions();

			for (int count = 0; count < alConditions.size(); count++) {
				HashMap hmCondition = (HashMap) alConditions.get(count);
				hmConditionOpr.put((String) hmCondition.get("CODEID"), gmFilterBean.fetchOperators((String) hmCondition.get("CODEID"), "CM_RULE"));
				hmConditionValues.put((String) hmCondition.get("CODEID"), gmFilterBean.fetchValues((String) hmCondition.get("CODEID"), "CM_RULE"));
			}
			request.setAttribute("CONDITIONS", alConditions);
			request.setAttribute("hmConditionOpr", hmConditionOpr);
			request.setAttribute("hmConditionValues", hmConditionValues);
			request.setAttribute("hmReturn", hmReturn);

			dispatch(strDispatchTo, request, response);

		}// End of try
		catch (Exception e) {
			session.setAttribute("hAppErrorMessage", e.getMessage());
			strDispatchTo = strComnPath + "/GmError.jsp";
			gotoPage(strDispatchTo, request, response);
		}// End of catch

	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		doPost(request, response);
	}
}
