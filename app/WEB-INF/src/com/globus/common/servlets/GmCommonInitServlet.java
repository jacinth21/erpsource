package com.globus.common.servlets;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonInitHandler;
import com.globus.common.beans.GmJobSchedulerBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.email.GmEmailBean;

/**
 * This class will be called @ servlet load and from the URL .. Used to load common and Log4j
 * information
 */

public class GmCommonInitServlet extends HttpServlet {
  Logger logger = null;

  public GmCommonInitServlet() {
    logger = GmLogger.getInstance(this.getClass().getName());
  }

  /**
   * This method will be invoked on Startup. This method will invoke the loadProperties() method
   * that will load all the common propertied information
   */

  @Override
  public void init() throws ServletException {
    GmCommonInitHandler gmCommonInitHandler = new GmCommonInitHandler();
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmJobSchedulerBean gmJobSchedulerBean = new GmJobSchedulerBean();
    GmEmailBean gmEmailBean = new GmEmailBean();
    // String ConfigDir = getServletContext().getRealPath("/config/");
    String logPropFile = System.getProperty("ENV_LOG4J");
    String commonPropFile = getInitParameter("common-init-file");
    String envValue = getInitParameter("config-env-name");
    String ConfigDir = envValue;

    try {
      gmCommonInitHandler.setConfigDir(ConfigDir);
      gmCommonInitHandler.setlogPropFile(logPropFile);
      gmCommonInitHandler.setcommonPropFile(commonPropFile);
      gmCommonInitHandler.loadProperties();
      gmCommonBean.loadCompanyInfo();// to load Company static list and map
      gmCommonBean.loadPlantInfo();// to load Plant static list and map
      gmCommonBean.populatePlantCompanyStaticMap(); // to load Plant Company static list and map
      gmJobSchedulerBean.loadCompanyScheduledJobs(); // load scheduled jobs
      // gmCommonBean.loadDistributorList(); // Used by Transfer module (Commented for OP-793 , as
      // the distributors are fetched based on company ID)
      gmCommonBean.loadEmployeeList(); // Used by Transfer module
      gmCommonBean.loadClinicalStudyList(); // Used by Clinical module
      gmCommonBean.loadMonthList(); // started using in Demand Sheet Summary
      gmCommonBean.loadYearList(); // started using in Demand Sheet Summary
      gmCommonBean.loadOperatorList(); // Used by CrossTab report in filter
      // gmCommonBean.loadActiveLoanerSets();// to load list of active loaner setids (Commented as
      // Part of Loaner Process Changes.Set will be filtered Based on Company ID)
      gmCommonBean.loadActiveInHouseLoanerSets();// to load list of active inhouse loaner setids
      gmCommonBean.isWhitelistedServer();// Check the sever
      gmCommonBean.isTestingServer(); // get the local system info
      gmCommonBean.fetchLdapUser();// get system rule values from t906b table and stored in static
                                   // HashMap to configure AD
      gmCommonBean.setCountryCode();// setting the country code
      gmCommonBean.setContextURL();//setting for jasper email
      gmCommonBean.setJasperImageURL();//setting for jasper creation
      gmCommonBean.setDbConnectionTimeOut();//setting DB connection timeout
      gmCommonBean.setDbConnectionTimeOutEXT();//setting Extended DB connection timeout
      gmEmailBean.initializeEmailComponent();// Initialize properties and objects required to send email 
      gmCommonBean.setShippingPrintService();//setting shipping print service

      logger.debug("Sucessfully Loaded All the Reference Data in Init method.");
    } catch (Exception exc) {
      exc.printStackTrace();
      logger.error("PartyReferenceInitServlet:init():ERROR : Error Loading Properties Files.:"
          + exc.getMessage());
      throw new ServletException(exc);
    }
    boolean blWhitelistserver = GmCommonClass.blWhitelistserver;
    boolean blTestingserver = GmCommonClass.blTestingserver;
    String toEmail = GmCommonClass.toEmail;


    if (!blWhitelistserver && !blTestingserver && (toEmail == null || toEmail.equals(""))) {
      throw new ServletException(
          "System not found in the rule table for any of the groups WHITELISTEDSERVER, TESTINGSERVER, SYSTEM");
    }

    logger.debug("Exit Init()");
  }


  /**
   * This method will be invoked for every request submitted into this Servlet. This method will
   * invoke the loadProperties() method that will load the common.Properties
   * 
   * This method will get invoked when any of the common property value changed in the due course of
   * the project.
   * 
   * This method will have to send a response containing some basic HTML page with the message
   * saying that the Property values have been successfully reloaded.
   * 
   * @param req
   * @param res
   * @throws ServletException
   */

  @Override
  public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException {
    GmCommonInitHandler gmCommonInitHandler = new GmCommonInitHandler();
    GmJobSchedulerBean gmJobSchedulerBean = new GmJobSchedulerBean();
    GmCommonBean gmCmBean = new GmCommonBean();

    // String ConfigDir = getServletContext().getRealPath("/config/");
    String logPropFile = "";
    String commonPropFile = getInitParameter("common-init-file");
    String envValue = getInitParameter("evnValue");
    String ConfigDir = envValue;

    logger.debug(" inside service of init ");

    try {
      logger.debug("PartyReferenceInitServlet --> Entered service()");

      PrintWriter out = res.getWriter();
      gmCommonInitHandler.setConfigDir(ConfigDir);
      gmCommonInitHandler.setlogPropFile(logPropFile);
      gmCommonInitHandler.setcommonPropFile(commonPropFile);
      gmCommonInitHandler.loadProperties();
      gmJobSchedulerBean.loadCompanyScheduledJobs();
      gmCmBean.setShippingPrintService();//setting shipping print service
      out.println("Sucessfully Loaded All the Property Files And the Reference Data in the Memory");
      out.close();
      logger
          .debug("The Property files and the References were loaded into the memory successfully.");

    } catch (Exception exc) {
      try {
        logger.error("GmCommonInitServlet:service():ERROR : Error Loading Properties Files.:"
            + exc.getMessage());
        logger.debug("There was an error while loading the Property Files and References.");
        logger.debug("Please resolve them and run the Servlet again.");
        PrintWriter out = res.getWriter();
        out.println("There was an error while loading the Property Files and References.");
        out.println("Please resolve them and run the Servlet again.");
        out.close();
      }

      catch (Exception e) {
        logger.error("Error occurred in rendering the error message.");
      }
    }
    logger.debug(" Exit service of init ");
  }
}
