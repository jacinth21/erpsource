/**
 * This class is the extension of every servlet
 * 
 * @author $Author:$
 * @version $Revision:$ <br>
 *          $Date:$
 */

package com.globus.common.servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSecurityBean;
import com.globus.common.filters.GmAccessFilter;
import com.globus.common.util.GmCookieManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmServlet extends HttpServlet {
  protected String strComnErrorPath = GmCommonClass.getString("GMCOMMON") + "/GmError.jsp";
  protected HttpSession session;
  // protected HashMap hmParam = new HashMap();
  protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the
                                                                         // Logger Class.;
  protected String strAction = "";
  protected String strOpt = "";
  protected String strUserId = "";
  protected String strUserNm = "";
  protected String strApplDateFmt = "";
  protected String strJSDateFmt = "";
  protected String strSessCurrSymbol = "";
  protected String strSessApplCurrFmt = "";
  private GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();

  /**
   * @return
   */
  public GmDataStoreVO getGmDataStoreVO() {
    return this.gmDataStoreVO;
  }


  // The method is overloaded to fix the multipart form data issue.
  // when a request comes by clicking left link instantiate method with 2 parameter will be called.
  // when a submit from place like file upload,set master setup instantiate method with 3 parameter
  // will be called.
  protected void instantiate(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response, null);
  }


  protected void instantiate(HttpServletRequest request, HttpServletResponse response,
      MultipartFormDataRequest mrequest) throws ServletException, IOException {
    session = request.getSession(false);
    checkSession(response, session); // Checks if the current session is valid, else redirecting to
                                     // SessionExpiry page
    request.setCharacterEncoding("UTF-8");
    response.setContentType("text/html; charset=UTF-8");
    response.setCharacterEncoding("UTF-8");
    strUserId = (String) session.getAttribute("strSessUserId");
    strUserNm = (String) session.getAttribute("strSessShName");
    strApplDateFmt = (String) session.getAttribute("strSessApplDateFmt");
    strJSDateFmt = (String) session.getAttribute("strSessJSDateFmt");
    strSessApplCurrFmt = (String) session.getAttribute("strSessApplCurrFmt");
    strSessCurrSymbol = (String) session.getAttribute("strSessCurrSymbol");

    strAction = GmCommonClass.parseNull(request.getParameter("hAction"));
    strOpt = GmCommonClass.parseNull(request.getParameter("hOpt"));
    log.debug(" User id " + strUserId + " User Name " + strUserNm + " Action " + strAction
        + " Opt " + strOpt);
    if (strAction.equals("") && !strOpt.equals("")) {
      strAction = strOpt;
    } else if (strAction.equals("") && strOpt.equals("")) {
      strAction = "Load";
    }
    // Getting Company info.
    if (mrequest != null) {
      gmDataStoreVO = GmFramework.getCompanyParams(request, mrequest);
    } else {
      gmDataStoreVO = GmFramework.getCompanyParams(request);
    }

    log.debug("the value insdie gmDataStoreVO in GmServlet***** " + gmDataStoreVO);
    if (!gmDataStoreVO.getCmpdfmt().equals("")) {
      strApplDateFmt = gmDataStoreVO.getCmpdfmt();
    }
  }


  public static String getCookieValue(HttpServletRequest request, String name) {
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (int i = 0; i < cookies.length; i++) {
        Cookie cookie = cookies[i];

        if (name.equals(cookie.getName())) {
          return cookie.getValue();
        }
      }
    }
    return null;
  }

  public void checkSession(HttpServletResponse res, HttpSession session) throws ServletException,
      IOException {
    if (session == null) {
      res.sendRedirect(GmCommonClass.getString("GMCOMMON").concat("/GmSessionExpiry.jsp"));
      log.debug(" Session is null ");
      return;
    } else if ((String) session.getAttribute("strSessUserId") == null) {
      res.sendRedirect(GmCommonClass.getString("GMCOMMON").concat("/GmSessionExpiry.jsp"));
      return;
    }
  }

  public void gotoPage(String address, HttpServletRequest req, HttpServletResponse res)
      throws ServletException, IOException {
    // req.getRequestDispatcher(address).forward(req,res); // use this instead of send Redirect.
    // Modified by VPrasath.
	  log.debug("gotoPage address "+address);
    // Append companyinfo in request
    address = GmFramework.appendCompanyInfo(address, req);
    res.sendRedirect(res.encodeRedirectUrl(address));
  }

  public void gotoPageOld(String address, HttpServletRequest req, HttpServletResponse res)
      throws ServletException, IOException {
    // Append companyinfo in request
	  log.debug("gotoPageOld address"+address+" req "+req);
    address = GmFramework.appendCompanyInfo(address, req);
    res.sendRedirect(res.encodeRedirectUrl(address));
  }

  public void gotoStrutsPage(String address, HttpServletRequest req, HttpServletResponse res)
      throws ServletException, IOException {
    res.setContentType("text/html; charset=UTF-8");// TSK-8883:Setting content type to UTF-8 to
                                                   // support the unicode chars (Japan chars) on
                                                   // JBOSS wildfly with JVM 1.8
    req.getRequestDispatcher(address).include(req, res); // use this instead of send Redirect.
                                                         // Modified by VPrasath.
  }

  public void dispatch(String address, HttpServletRequest req, HttpServletResponse res)
      throws ServletException, IOException {
	  log.debug("dispatch "+address);	  
    GmSecurityBean gmSecurityBean = new GmSecurityBean();
    HashMap hmReturn = new HashMap();
    String strAddress = new String(address);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    boolean blAccess;

    // To get the security information from the session
    HttpSession session = req.getSession(false);
    hmReturn = GmCommonClass.parseNullHashMap((HashMap) session.getAttribute("hmAccessLevel"));
	  log.debug("hmReturn "+hmReturn);    
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
	  log.debug("strDepartMentID "+strDepartMentID);
    blAccess = gmSecurityBean.checkScreenAccess(hmReturn, address, strDepartMentID);
	  log.debug("blAccess "+blAccess);
    if (blAccess == false) {
      session.setAttribute("hAppErrorMessage", "No Access to View the page");
      strAddress = strComnPath + "/GmError.jsp";
    }
log.debug("dispatch strAddress>> "+strAddress);
    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(strAddress);
    // dispatcher.forward(req,res);
    // modified by vprasath
    dispatcher.include(req, res);

  }

  public String getAccessCondition(HttpServletRequest req, HttpServletResponse res) {
    // To get the Session Information
    HttpSession session = req.getSession(false);

    // Holds the where condition information
    StringBuffer strQuery = new StringBuffer();
    HashMap hmParam = new HashMap();
    GmAccessFilter gmAccessFilter = new GmAccessFilter();

    // To get the Employee Information
    String strEmpId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    int intAccLevel =
        Integer.parseInt(GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    int intOverrideAccLevel =
        Integer.parseInt(GmCommonClass.parseZero((String) req.getAttribute("strOverrideAccLvl")));
    int intOverrideASSAccLevel =
        Integer
            .parseInt(GmCommonClass.parseZero((String) req.getAttribute("strOverrideASSAccLvl")));
    String strRptRepId = GmCommonClass.parseNull((String) session.getAttribute("strSessRptRepId"));

    String strOverrideAccessTo =
        GmCommonClass.parseNull((String) session.getAttribute("strSessOverrideAcsTo"));
    String strOverrideAccessId =
        GmCommonClass.parseNull((String) session.getAttribute("strSessOverrideAcsId"));
    String strOverrideAccessLvl =
        GmCommonClass.parseNull((String) session.getAttribute("strSessOverrideAcsLvl"));
    String strPrimaryCmpyId =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
    String strBBASalesAcsFl =
        GmCommonClass.parseNull((String) session.getAttribute("strSessBBASalesAcsFl"));
    String strAddnlCompFl =
            GmCommonClass.parseNull((String) session.getAttribute("strSessAddnlCompFl"));
    
    hmParam.put("PRIMARY_CMPY", strPrimaryCmpyId);
    hmParam.put("BBA_SALES_ACS_FL", strBBASalesAcsFl);
    hmParam.put("USER_ID", strEmpId);
    hmParam.put("ACCES_LVL", intAccLevel + "");
    hmParam.put("DEPT_ID", strDepartMentID);
    hmParam.put("OVERRIDE_ACCS_LVL", intOverrideAccLevel + "");
    hmParam.put("OVERRIDE_ASS_LVL", intOverrideASSAccLevel + "");
    hmParam.put("RPT_REPID", strRptRepId);

    hmParam.put("OVERRIDE_ACS_TO", strOverrideAccessTo);
    hmParam.put("OVERRIDE_ACS_ID", strOverrideAccessId);
    hmParam.put("OVERRIDE_ACS_LVL", strOverrideAccessLvl);
    hmParam.put("ADDNL_COMP_IN_SALES", strAddnlCompFl);
    return this.getAccessFilter(hmParam);
  }

  /*
   * Below Method is used to append the Access filter based on the user role type
   * Modification: Changing IN clause to EXISTS to reduce sql execution time
   * @ Author: gpalani. PMT-20656 Jun 2018
   */
  public String getAccessFilter(HashMap hmParam) {

    StringBuffer strQuery = new StringBuffer();
    GmCommonClass gmcc = new GmCommonClass();

    String strPrimaryCmpyId = GmCommonClass.parseNull((String) hmParam.get("PRIMARY_CMPY"));
    String strBBASalesAcsFl = GmCommonClass.parseNull((String) hmParam.get("BBA_SALES_ACS_FL"));
    String strAddnlCompFl =   GmCommonClass.parseNull((String) hmParam.get("ADDNL_COMP_IN_SALES"));
    String strEmpId = gmcc.parseNull((String) hmParam.get("USER_ID"));
    int intAccLevel = Integer.parseInt(gmcc.parseZero((String) hmParam.get("ACCES_LVL")));
    String strDepartMentID = gmcc.parseNull((String) hmParam.get("DEPT_ID"));
    int intOverrideAccLevel =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ACCS_LVL")));
    int intOverrideASSAccLevel =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ASS_LVL")));
    String strRptRepId = gmcc.parseNull((String) hmParam.get("RPT_REPID"));
    String strCompanyIds = "";
    String strOverrideAccessTo = gmcc.parseNull((String) hmParam.get("OVERRIDE_ACS_TO"));
    String strOverrideAccessId = gmcc.parseNull((String) hmParam.get("OVERRIDE_ACS_ID"));
    int intSessOverrideAcsLvl =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ACS_LVL")));

    
    // PC-318: To remove the associate sales rep override access level 
    // To show the correct sales amount (Daily Sales/ Dashboard/ Quota reports)
    // So, Remove the below override access level if condition.
    
    /*if (intOverrideASSAccLevel == 7) {
      strEmpId = strRptRepId;
      intAccLevel = 1;
    }*/

    if (strOverrideAccessTo.equals("VP")) {
      // Assign VP Access
      intAccLevel = intSessOverrideAcsLvl;
      strDepartMentID = "S";
      strCompanyIds = strOverrideAccessId;
    } else if (strOverrideAccessTo.equals("AD")) {
      // AD - Assign AD Access for other than sales dept users from OUS
      // BBA - Assign override access for other than sales dept users from BBA
      intAccLevel = intSessOverrideAcsLvl;
      strDepartMentID = "S";
      strCompanyIds = strOverrideAccessId;
    }

    // *********************************************************
    // Will Execute the below Query if the Department is Sales
    // *********************************************************
    if (strDepartMentID.equals("S")) {

      switch (intAccLevel) {

      // Sales Rep Filter Condition
        case 1: {
          strQuery.append(" T501.C703_SALES_REP_ID = " + strEmpId);
          log.debug("gmServlet****:ACCESS_LEVEL:1");

          break;
        }

        // Distributor Principal Filter Condition
        case 2: {
          strQuery.append("  EXISTS ");
          strQuery.append("( SELECT T703S.C703_SALES_REP_ID FROM T703_SALES_REP T703S ");
          strQuery.append("  WHERE T703S.C701_DISTRIBUTOR_ID IN ( SELECT C701_DISTRIBUTOR_ID");
          strQuery.append("  FROM T101_USER WHERE C101_USER_ID = " + strEmpId + " )  ");
          strQuery.append("  AND T703S.C703_SALES_REP_ID=T501.C703_SALES_REP_ID ) ");
          log.debug("gmServlet****:ACCESS_LEVEL:2");
          break;
        }

        // AD Filter Condition
        case 3: {
          strQuery.append(" T501.C501_AD_ID  = " + strEmpId);// changed as part of PMT-28
          log.debug("gmServlet****:ACCESS_LEVEL:3");
    
          break;
        }
        // VP Filter Condition
        case 4: {
          strQuery.append(" T501.C501_VP_ID  = " + strEmpId);
          log.debug("gmServlet****:ACCESS_LEVEL:4");
        
          break;
        }

        // other AD's like Kirk Tovey want to see other region
        case 6: {
          strQuery.append("  EXISTS ");
          strQuery.append("( SELECT T703S.C703_SALES_REP_ID FROM T703_SALES_REP T703S ");
          strQuery.append("  WHERE T703S.C701_DISTRIBUTOR_ID IN ( SELECT C701_DISTRIBUTOR_ID");
          strQuery.append("  FROM  T701_DISTRIBUTOR WHERE C701_REGION IN ( ");
          strQuery.append("  SELECT C901_REGION_ID FROM T708_REGION_ASD_MAPPING ");
          strQuery.append("  WHERE  C901_USER_ROLE_TYPE = 8002 ");
          strQuery.append("  AND   C708_DELETE_FL IS NULL ");
          strQuery.append("  AND   C101_USER_ID = " + strEmpId + " )) ");
          strQuery.append("  AND T703S.C703_SALES_REP_ID=T501.C703_SALES_REP_ID )");
          
          log.debug("gmServlet****:ACCESS_LEVEL:6");
          break;
        }

        // ASS Filter condition
        case 7: {
          // PC-318: Associate sales rep - To change the query instead of
          // account to modified Sales rep to get the data.

        	 // To join the t704a and T704 table	
            strQuery.append("  EXISTS  ");
            strQuery.append("  ( SELECT distinct t704.c703_sales_rep_id FROM ");
            strQuery.append(" t704a_account_rep_mapping t704a, t704_account t704 ");
            // to add below tables to get the rep id - based on user
            //strQuery.append(" , t101_user t101u, t101_party t101p, t703_sales_rep t703 ");
            strQuery.append(" WHERE t704.c704_account_id    = t704a.c704_account_id ");
            // to join user and party details
            // **** To remove the User and sales rep join (Query execute takes more times)                     
            /*strQuery.append(" AND t101u.c101_party_id = t101p.c101_party_id ");
            strQuery.append(" AND t101p.c101_party_id  = t703.c101_party_id ");
            strQuery.append(" AND t704a.c703_sales_rep_id = t703.c703_sales_rep_id ");
            strQuery.append(" AND t101u.c101_user_id = " + strEmpId);*/
            
            strQuery.append(" AND t704a.c703_sales_rep_id = " + strEmpId);
            strQuery.append(" AND t704.c703_sales_rep_id   = t501.c703_sales_rep_id " );
            strQuery.append(" AND t704.c704_void_fl      IS NULL ");
            strQuery.append(" AND t704a.c704a_void_fl    IS NULL ) ");
            // to remove the void flag check - Once added query execute takes more time
            /*strQuery.append(" and t101p.c101_void_fl  is null ");
            strQuery.append(" and t703.c703_void_fl  is null ) "); */
          log.debug("gmServlet****:ACCESS_LEVEL:7");
          
          break;
        }

        // other more filter users like Agmar Esser want to see other region
        case 8: {
        
          strQuery.append("  EXISTS");
          strQuery.append("  (SELECT DISTINCT V700S.REP_ID ");
          strQuery.append("  FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
          strQuery.append("  WHERE V700S.REP_COMPID IN ( " + strCompanyIds + ")");
          strQuery.append("  AND V700S.REP_ID=T501.C703_SALES_REP_ID )");
          log.debug("gmServlet****:ACCESS_LEVEL:8");

          break;
        }

      }

    }
  
   
    log.debug("strCondition := " + strQuery.toString());
    return strQuery.toString();
  }

  /*
   * This function is used to get filter condition and data based on the request specified the user
   * defaults filter based on access contion and adds other filter
   */
  public String getAccessCondition(HttpServletRequest req, HttpServletResponse res, boolean flag) {
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    // Holds the where condition information
    String strQuery = "";
    StringBuffer strCondtion = new StringBuffer();

    strQuery = getAccessCondition(req, res);
    if (flag) {
      strCondtion = gmAccessFilter.getHierarchyWhereConditionT501(req, res, strQuery);
      return strCondtion.toString();
    }
    log.debug("strCondition := " + strCondtion.toString());
    log.debug("strQuery := " + strQuery);

    return strQuery;
  }

  /**
   * getSalesFilters - This method will append sales filter based on user access condition
   * 
   * @return String with access condtion
   * @exception AppError
   **/
  public String getSalesFilter(HttpServletRequest req, HttpServletResponse res) throws AppError {
    String strFilter = "";
    String strRegn = "";
    String strDist = "";
    String strTerr = "";
    String strPgrp = "";
    String strCRegn = "";
    String strCDist = "";
    String strCTerr = "";
    String strCPgrp = "";
    String strCGrpAllDist = "";
    String strCookieFlag = "";
    String strGrpAllDist = "";
    String strDistFilterUnChk = "";
    StringBuffer strTemp = new StringBuffer();
    GmCookieManager gmCookieManager = new GmCookieManager();

    strCookieFlag = GmCommonClass.parseNull(req.getParameter("hCookieFlag"));

    // while clicking the Go/Load button at that time we no need to get filter values from cookies.
    if (!strCookieFlag.equals("NO")) {
      strCRegn = GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "chRegnFilter"));
      strCDist = GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "chDistFilter"));
      strCTerr = GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "chTerrFilter"));
      strCPgrp = GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "chPgrpFilter"));
      strCGrpAllDist = GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "chGrpAllDist"));
    }
    strRegn = strCRegn.equals("") ? null : strCRegn;
    strDist = strCDist.equals("") ? null : strCDist;
    strTerr = strCTerr.equals("") ? null : strCTerr;
    strPgrp = strCPgrp.equals("") ? null : strCPgrp;
    strGrpAllDist = strCGrpAllDist.equals("") ? null : strCGrpAllDist;

    strRegn = strRegn == null ? GmCommonClass.parseNull(req.getParameter("hRegnFilter")) : strRegn;
    strTerr = strTerr == null ? GmCommonClass.parseNull(req.getParameter("hTerrFilter")) : strTerr;
    strDist = strDist == null ? GmCommonClass.parseNull(req.getParameter("hDistFilter")) : strDist;
    strPgrp = strPgrp == null ? GmCommonClass.parseNull(req.getParameter("hPgrpFilter")) : strPgrp;
    strGrpAllDist =
        strGrpAllDist == null ? GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Dist"))
            : strGrpAllDist;
    strDistFilterUnChk = GmCommonClass.parseNull(req.getParameter("hDistFilterUnChk"));

    req.setAttribute("hRegnFilter", strRegn);
    req.setAttribute("hDistFilter", strDist);
    req.setAttribute("hTerrFilter", strTerr);
    req.setAttribute("hPgrpFilter", strPgrp);
    req.setAttribute("hDistFilterUnChk", strDistFilterUnChk);


    HashMap hmParam = new HashMap();
    hmParam.put("REGN_FILTER", strRegn);
    // If distributor select all value is checked , we are getting values based on division,region
    // and zone
    // so we passing distributor filter as empty
    if (strGrpAllDist.equals("Y")) {
      hmParam.put("DIST_FILTER", "");
    } else {
      hmParam.put("DIST_FILTER", strDist);
    }
    hmParam.put("TERR_FILTER", strTerr);
    hmParam.put("PGRP_FILTER", strPgrp);
    // If distributor checked value is greater than dist value, we are passing unchecked values
    hmParam.put("DIST_UNCHK_FILTER", strDistFilterUnChk);

    return this.getSalesFilter(hmParam);
  }

  public String getSalesFilter(HashMap hmParam) throws AppError {
    StringBuffer strTemp = new StringBuffer();
    String strFilter = "";
    String strRegn = GmCommonClass.parseNull((String) hmParam.get("REGN_FILTER"));
    String strDist = GmCommonClass.parseNull((String) hmParam.get("DIST_FILTER"));
    String strTerr = GmCommonClass.parseNull((String) hmParam.get("TERR_FILTER"));
    String strPgrp = GmCommonClass.parseNull((String) hmParam.get("PGRP_FILTER"));
    String strDistFilterUnChk = GmCommonClass.parseNull((String) hmParam.get("DIST_UNCHK_FILTER"));

    if (!strRegn.equals("")) {
      strTemp.append(" AND V700.REGION_ID IN (");
      strTemp.append(strRegn);
      strTemp.append(")");
    }

    if (!strDist.equals("")) {
      strTemp.append(" AND V700.D_ID IN (");
      strTemp.append(strDist);
      strTemp.append(")");
    } else if (!strDistFilterUnChk.equals("")) {
      strTemp.append(" AND V700.D_ID NOT IN (");
      strTemp.append(strDistFilterUnChk);
      strTemp.append(")");
    }

    if (!strTerr.equals("")) {
      strTemp.append(" AND V700.TER_ID IN (");
      strTemp.append(strTerr);
      strTemp.append(")");
    }

    // To fecth product group filter
    if (!strPgrp.equals("")) {
      strPgrp = strPgrp.replaceAll(",", "','");
      strTemp.append(" AND ");
      strTemp.append(" EXISTS ");
      strTemp.append(" (SELECT T208.C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      strTemp.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      strTemp.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");
      strTemp.append(" AND T207.C207_SET_ID IN ('");
      strTemp.append(strPgrp);
      strTemp.append("')");
      strTemp.append(" AND T208.C205_PART_NUMBER_ID= T502.C205_PART_NUMBER_ID");
      strTemp.append("')");
    }
    strFilter = strTemp.toString();


    return strFilter;
  } // End of getSalesFilters

  /*
   * Method used to parse the request and populate a hashmap with the parameters and values Not yet
   * tested, dont use this method until this line of comment is removed.
   */
  public HashMap parseRequest(HttpServletRequest request) {
    HashMap hmRequest = new HashMap();
    String paramName = "";
    String paramValue = "";
    String subStr = "";
    Enumeration enumReqParamNames = request.getParameterNames();

    while (enumReqParamNames.hasMoreElements()) {
      paramName = (String) enumReqParamNames.nextElement();
      paramValue = request.getParameter(paramName);

      if (paramName.indexOf("_") != -1) {
        subStr = paramName.substring(0, paramName.indexOf("_") + 1);
      }

      paramName = paramName.replaceAll(subStr, "");
      paramName = paramName.replaceAll("Txt", "");
      paramName = paramName.replaceAll("Chk", "");
      paramName = paramName.replaceAll("Cbo", "");

      if (paramName.indexOf("h") == 0) {
        paramName = paramName.replaceAll("h", "");
      }

      paramName = paramName.toUpperCase();

      hmRequest.put(paramName, paramValue);
    }
    return hmRequest;
  }

  /*
   * Checks if the Exception is type of AppError. If so, it'll get the exception object from
   * Apperror object and call Logger.error. Else if it is of type Exception, just call Logger.error
   */
  protected void checkAndLogError(Exception exception, HttpServletRequest request) {
    if (exception instanceof AppError) {
      AppError e = (AppError) exception;
      exception = e.getException();
    }

    GmLogger.error(exception, request);
  }

  /*
   * This function is used to get filter condition and data based on the request specified the user
   * defaults filter based on access contion and adds other filter
   */
  public String getAccessFilter(HttpServletRequest req, HttpServletResponse res) {
    HttpSession session = req.getSession(false);
    HashMap hmParam = new HashMap();
    String strFilter = "";
    String strDeptId = "";
    String strAccessID = "";
    String strUserRoleType = "";

    strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    int accessId =
        Integer.parseInt(GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    log.debug(" in access filter " + strDeptId + " : " + accessId);
    if (strDeptId.equalsIgnoreCase("ICS")) {
      if (accessId < 3) {
        strUserRoleType = "8003"; // ICS Employee
      } else {
        strUserRoleType = "8005"; // ICS VP
      }
    }
    hmParam.put("SessUserId", session.getAttribute("strSessUserId"));
    hmParam.put("SessAccLvl", accessId + "");
    hmParam.put("SessDeptId", strDeptId);
    hmParam.put("UserRoleType", strUserRoleType);
    strFilter = GmAccessFilter.getAccessFilter(hmParam);

    return strFilter;
  }

  public Date getDateFromStr(String strDate) {
    Date date = null;
    try {
      if (strDate != null && strDate.length() > 0) {
        DateFormat formatter = new SimpleDateFormat(strApplDateFmt);
        date = formatter.parse(strDate);
        log.debug("date passed:" + date.toString());
      }
    } catch (Exception exp) {
      exp.printStackTrace();
    }

    return date;
  }
}
