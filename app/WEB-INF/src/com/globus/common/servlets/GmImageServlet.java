
      package com.globus.common.servlets;
      
      import java.awt.Dimension;
      import java.io.IOException;
      import java.util.List;      
      import javax.servlet.ServletException;
      import javax.servlet.ServletOutputStream;
      import javax.servlet.http.HttpServletRequest;
      import javax.servlet.http.HttpServletResponse;
      import org.apache.log4j.Logger;
      import com.globus.common.beans.GmLogger;
      import net.sf.jasperreports.engine.JRConstants;
      import net.sf.jasperreports.engine.JRElement;
      import net.sf.jasperreports.engine.JRException;
      import net.sf.jasperreports.engine.JRImageRenderer;
      import net.sf.jasperreports.engine.JRPrintImage;
      import net.sf.jasperreports.engine.JRRenderable;
      import net.sf.jasperreports.engine.JRWrappingSvgRenderer;
      import net.sf.jasperreports.engine.export.JRHtmlExporter;
      import net.sf.jasperreports.engine.util.JRTypeSniffer;
      import net.sf.jasperreports.j2ee.servlets.BaseHttpServlet;
      
      
      /**
       * @author Arockia Prasath
       * @version 
       * This will resolve Dynamic image issue. It will show correct image when you execute the jasper report multiple time in single request
       */
      public class GmImageServlet extends BaseHttpServlet
      {
      	private static final long serialVersionUID = JRConstants.SERIAL_VERSION_UID;
      
      
      	/**
      	 *
      	 */
      	public static final String IMAGE_NAME_REQUEST_PARAMETER = "image";
      	public static final String IMAGE_TYPE_REQUEST_PARAMETER = "imageData";
      
      			
      	/**
      	 *
      	 */
      	public void service(
      		HttpServletRequest request,
      		HttpServletResponse response
      		) throws IOException, ServletException
      	{
      		Logger log = GmLogger.getInstance(this.getClass().getName());
      		
      		byte[] imageData = null;
      		String imageMimeType = null;
      
      		String imageName = request.getParameter(IMAGE_NAME_REQUEST_PARAMETER);
      		String imageDataKey = request.getParameter(IMAGE_TYPE_REQUEST_PARAMETER);
      		
      		if ("px".equals(imageName))
      		{
      			try
      			{
      				JRRenderable pxRenderer = 
      					JRImageRenderer.getInstance("net/sf/jasperreports/engine/images/pixel.GIF");
      				imageData = pxRenderer.getImageData();
     				imageMimeType = JRRenderable.MIME_TYPE_GIF;
      			}
      			catch (JRException e)
      			{
      				throw new ServletException(e);
      			}
      		}
      		else
      		{
      			
					
				List jasperPrintList = null;
				synchronized (GmImageServlet.class) {
      			jasperPrintList=(List)request.getSession().getAttribute(imageDataKey);
      			
      			if(jasperPrintList == null)
      			{
      				jasperPrintList = BaseHttpServlet.getJasperPrintList(request);
      				request.getSession().setAttribute(imageDataKey,jasperPrintList);
      			} 			
      			
      			
      			if (jasperPrintList == null)
      			{
      				throw new ServletException("No JasperPrint documents found on the HTTP session.");
      			}
      			
    			JRPrintImage image = JRHtmlExporter.getImage(jasperPrintList, imageName);
    			JRRenderable renderer = image.getRenderer();
     			if (renderer.getType() == JRRenderable.TYPE_SVG)
     			{
     				renderer = 
     					new JRWrappingSvgRenderer(
     						renderer, 
     						new Dimension(image.getWidth(), image.getHeight()),
     						JRElement.MODE_OPAQUE == image.getMode() ? image.getBackcolor() : null
     						);
     			}
     		
     			imageMimeType = JRTypeSniffer.getImageMimeType(renderer.getImageType());
     			
     			try
     			{
     				imageData = renderer.getImageData();
     			}
     			catch (JRException e)
     			{
     				throw new ServletException(e);
     			}
      			}
     		}
     
     		if (imageData != null && imageData.length > 0)
     		{
     			if (imageMimeType != null) 
     			{
     				response.setHeader("Content-Type", imageMimeType);
     			}
     			response.setContentLength(imageData.length);
     			ServletOutputStream ouputStream = response.getOutputStream();
     			ouputStream.write(imageData, 0, imageData.length);
     			ouputStream.flush();
     			ouputStream.close();
     		}
     	}
     
     
     }
