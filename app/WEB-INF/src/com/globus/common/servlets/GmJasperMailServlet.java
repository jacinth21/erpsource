/**
 * FileName    : GmJasperMailServlet.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Apr 24, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.servlets;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;

/**
 * @author sthadeshwar
 *
 */
public class GmJasperMailServlet extends GmServlet {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		ObjectInputStream resultStream = null;
		ObjectOutputStream sendStream = null; 
		HashMap hmReturn = new HashMap();;
		try
		{
			log.debug("Running GmJasperMailServlet ... ");
			
			HashMap hmReportData = new HashMap();
			resultStream = new ObjectInputStream(request.getInputStream());   
			hmReportData = (HashMap) resultStream.readObject();
			
			log.debug("hmReportData = " + hmReportData);
			String strReportName = (String)hmReportData.get("FILENAME");
			HashMap hmParams = (HashMap)hmReportData.get("PARAMS");
			ArrayList alRequestDetails = (ArrayList)hmReportData.get("DETAILS");
			GmEmailProperties emailProps = (GmEmailProperties)hmReportData.get("EMAILPROPS");
			
			GmJasperReport gmJasperReport = new GmJasperReport();
			hmReturn = gmJasperReport.sendJasperMail(strReportName, hmParams, alRequestDetails, emailProps, request);

			sendStream = new ObjectOutputStream(response.getOutputStream()); 
			sendStream.writeObject(hmReturn);
			sendStream.flush();   
			sendStream.close();
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
