/**
 * FileName : GmCommonOpenServlet.java Description : Author : tchandure Date : Sep 16, 2008
 * Copyright : Globus Medical Inc
 */
package com.globus.common.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;

/**
 * @author tchandure
 * 
 */
public class GmCommonFileOpenServlet extends GmServlet {
  private static String strComnPath = GmCommonClass.getString("GMCOMMON");

  /**
	 * 
	 */
  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // Download the file from server
    HttpSession session = request.getSession(false);
    String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
    String strUploadString = GmCommonClass.parseNull(request.getParameter("uploadString"));
    String strAppendPath = GmCommonClass.parseNull(request.getParameter("appendPath"));
    String strID = GmCommonClass.parseNull(request.getParameter("sId"));
    String strFileName = GmCommonClass.parseNull(request.getParameter("fileName"));
    
    String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
    String strCompanyLocale = GmCommonClass.parseNull(request.getParameter("compLocale"));
    strCompanyLocale = strCompanyLocale.equals("") ? "US" : strCompanyLocale;
    if (strUploadString.equals("") && strID.equals("")) {
      strUploadString = GmCommonClass.parseNull((String) request.getAttribute("uploadString"));
      strID = GmCommonClass.parseNull((String) request.getAttribute("sId"));
      strFileName = GmCommonClass.parseNull((String) request.getAttribute("fileName"));
    }
    log.debug("file name=" + strFileName);
    strUploadString = strUploadString.equals("") ? "UPLOADHOME" : strUploadString;

    String strUploadHome = GmCommonClass.getString(strUploadString);
    
    if (strOpt.equals("IPADORDERS")) {
        strUploadHome = strUploadHome + strCompanyLocale + "\\";
      }
    
    if (!strAppendPath.equals("")) {
      strUploadHome = strUploadHome + strAppendPath + "\\";
    }
    log.debug("strUploadHome.." + strUploadHome);
    log.debug("strID..." + strID);
    // Fetching the file name
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
    File file = null;
    FileInputStream istr = null;
    try {
      if (strFileName.equals("")) {
        strFileName = gmCommonUploadBean.fetchFileName(strID);
      }
      ServletOutputStream out = response.getOutputStream();
      response.setContentType("application/binary");
      response.setHeader("Content-disposition", "attachment;filename=" + strFileName);
      log.debug("file name......" + strUploadHome + strFileName);
      try {
        file = new File(strUploadHome + strFileName);
        istr = new FileInputStream(file);
      } catch (FileNotFoundException ffe) {
        if (strUploadString.equals("UPLOADIMAGESHOME")) {
          file = new File(strUploadHome + "default.jpg");
          istr = new FileInputStream(file);
        }
        throw new AppError("", "20686");
      }

      int curByte = -1;
      while ((curByte = istr.read()) != -1)
        out.write(curByte);

      istr.close();
      out.flush();
      out.close();
    } catch (AppError e) {
      log.error(e.getMessage(), e);
      session.setAttribute("hAppErrorMessage", e.getMessage());
      gotoPage(strErrorFileToDispatch, request, response);
    } catch (IOException e) {
      log.error(e.getMessage(), e);
      session.setAttribute("hAppErrorMessage", e.getMessage());

      gotoPage(strErrorFileToDispatch, request, response);
    }
  }
}
