/**
 * Description : This servlet is used for generating barcode and streaming out the image to front-end
 * Copyright : Globus Medical Inc
 *   
 */
package com.globus.common.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ByteArrayOutputStream;

import java.awt.Font;
import java.awt.image.BufferedImage;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.krysalis.barcode4j.BarcodeDimension;
import org.krysalis.barcode4j.impl.datamatrix.DataMatrixBean;
import org.krysalis.barcode4j.impl.datamatrix.SymbolShapeHint;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

import com.globus.common.beans.GmCommonClass;

import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.linear.code39.Code39Barcode;
import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.output.OutputException;

/**
 * @author bvidyasankar
 * 
 */
public class GmCommonBarCodeServlet extends GmServlet {
	private static String strComnPath = GmCommonClass.getString("GMCOMMON");

	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/*
	 * Barcode is generated in this service method using RefID & SOURCE as input
	 * for BarcodeFactory.createCode39() method and flushed out to .jsp page as
	 * image file.
	 * 
	 */
	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		log.debug("Entering service of  GmCommonBarCodeServlet....");
		HttpSession session = request.getSession(false);
		String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
		String strBarcode = "";
		String strID = GmCommonClass.parseNull((String) request.getParameter("ID"));		
		strBarcode = strID ;
		log.debug("sbBarcode: " + strBarcode);
		ServletOutputStream out = response.getOutputStream();
		Barcode barcode = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		String strType = GmCommonClass.parseNull(request.getParameter("type"));
		if(!"2D".equalsIgnoreCase(strType))
		{
		try {
				response.setContentType("application/binary");
			response.setHeader("Content-disposition", "attachment;filename=" + "barcode.png");

			try {
				barcode = BarcodeFactory.createCode39(strBarcode, false);
				barcode.setDrawingText(false);
				BarcodeImageHandler.writePNG(barcode, bos);
			} catch (BarcodeException be) {
				log.error(be.getMessage(), be);
				session.setAttribute("hAppErrorMessage", be.getMessage());
				gotoPage(strErrorFileToDispatch, request, response);
			} catch (OutputException e) {
				log.error(e.getMessage(), e);
				session.setAttribute("hAppErrorMessage", e.getMessage());
				gotoPage(strErrorFileToDispatch, request, response);
			}
			out.write(bos.toByteArray());
			bos.close();
			out.flush();
			out.close();
			} catch (IOException ioe) {
			log.error(ioe.getMessage(), ioe);
			session.setAttribute("hAppErrorMessage", ioe.getMessage());
			gotoPage(strErrorFileToDispatch, request, response);
		}
		}
		// BarCode 2D with Barcode4j.jar
		else if("2D".equalsIgnoreCase(strType))
		{
		log.debug("2D BarCode");
		strBarcode = strID ;
		log.debug("sbBarcode: " + strBarcode);
		response.setContentType("image/gif");
		 
		DataMatrixBean bean = new DataMatrixBean();
		final int dpi = 300;   
		bean.setModuleWidth(0.330); //UnitConv.in2mm(2.0f / dpi));         
		bean.doQuietZone(true);                   
		bean.setShape(SymbolShapeHint.FORCE_SQUARE);
		BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/gif", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);                      canvas.establishDimensions(new BarcodeDimension(25,25));  
		bean.generateBarcode(canvas, strBarcode);
		canvas.finish();     
		}
		 	
	}
}
