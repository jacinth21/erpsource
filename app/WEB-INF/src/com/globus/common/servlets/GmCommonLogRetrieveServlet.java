/*****************************************************************************
 * File : GmPatientMasterServlet Desc : This Servlet is used to handle the patient master
 * information
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.common.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.util.GmTemplateUtil;
import com.globus.logon.beans.GmLogonBean;


public class GmCommonLogRetrieveServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strDispatchTo = "";
    String strValue = "";
    ArrayList alEmpList = new ArrayList();



    RowSetDynaClass resultSet = null;
    HashMap hmFilterReturn = new HashMap();

    try {
      instantiate(request, response);
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
      String strAction = request.getParameter("hAction");
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      GmResourceBundleBean gmResourceBundleBeanlbl =
          GmCommonClass.getResourceBundleBean("properties.labels.accounts.GmCallLogList",
              strSessCompanyLocale);

      String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
      String strTodaysDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);

      String strMonthBeginDate = "";
      // strMonthBeginDate = strTodaysDate.replaceAll("/\\d\\d/","/01/");
      int currMonth = 0;
      currMonth = GmCalenderOperations.getCurrentMonth() - 1;
      strMonthBeginDate =
          GmCommonClass.parseNull(GmCalenderOperations.getAnyDateOfMonth(currMonth, 1,
              strApplnDateFmt));
      strAction = (strAction == null) ? (String) session.getAttribute("strSessOpt") : strAction;

      String strFrmDate = GmCommonClass.parseNull(request.getParameter("Txt_FromDate"));
      strFrmDate = strFrmDate.equals("") ? strMonthBeginDate : strFrmDate;
      Date dtFrmDate = getDateFromStr(strFrmDate);
      String strToDate = GmCommonClass.parseNull(request.getParameter("Txt_ToDate"));
      strToDate = strToDate.equals("") ? strTodaysDate : strToDate;
      Date dtToDate = getDateFromStr(strToDate);
      String strID = GmCommonClass.parseNull(request.getParameter("Txt_ID"));
      String strInvID = GmCommonClass.parseNull(request.getParameter("INV_ID"));
      String strUsrId = GmCommonClass.parseZero(request.getParameter("Cbo_UsrNmList"));
      String strComments = GmCommonClass.parseNull(request.getParameter("Txt_Comments"));

      alEmpList = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList());

      hmFilterReturn.put("FromDate", strFrmDate);
      hmFilterReturn.put("ToDate", strToDate);
      hmFilterReturn.put("ID", strID);
      hmFilterReturn.put("INVID", strInvID);
      hmFilterReturn.put("Type", strAction);
      hmFilterReturn.put("DtFmt", strApplnDateFmt);
      hmFilterReturn.put("UsrId", strUsrId);
      hmFilterReturn.put("Comments", strComments);
      hmFilterReturn.put("CALLFROM", "PO");

      String strCallLogRptHeader = GmCommonClass.parseNull(GmCommonClass.getCodeAltName(strAction));
      if (strCallLogRptHeader.equals("")) {
        strCallLogRptHeader =
            GmCommonClass.parseNull(gmResourceBundleBeanlbl.getProperty("LBL_CALL_LOG_REPORT"));
      }
      // To get the list of vendor information
      resultSet = gmCommonBean.getLog(hmFilterReturn);
      String strGridXmlData = generateOutPut((ArrayList) resultSet.getRows(), hmFilterReturn);
      request.setAttribute("XMLGRIDDATA", strGridXmlData);
      request.setAttribute("results", resultSet);
      request.setAttribute("hFrom", dtFrmDate);
      request.setAttribute("hTo", dtToDate);
      request.setAttribute("hID", strID);
      request.setAttribute("hInvID", strInvID);
      request.setAttribute("hAction", strAction);
      request.setAttribute("hUsrId", strUsrId);
      request.setAttribute("hComments", strComments);
      request.setAttribute("alEmpList", alEmpList);
      request.setAttribute("JspHeader", strCallLogRptHeader);
      dispatch(strAcctPath.concat("/GmCallLogList.jsp"), request, response);
    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
  
  /**
   * generateOutPut
   * @param HashMap hmParam
   * @param ArrayList alGridData
   * @exception AppError
   */    
  private String generateOutPut(ArrayList alGridData, HashMap hmParam) throws Exception {

	    GmTemplateUtil templateUtil = new GmTemplateUtil();
	    String strSessCompanyLocale =
	        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
	    templateUtil.setDataList("alReturn", alGridData);
	    templateUtil.setDataMap("hmParam", hmParam);
	    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
	        "properties.labels.accounts.GmCallLogList", strSessCompanyLocale));
	    templateUtil.setTemplateSubDir("common/templates");
	    templateUtil.setTemplateName("GmCallLogList.vm");
	    return templateUtil.generateOutput();
	  }
  
}// End of GmSalesRepServlet
