package com.globus.common.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionServlet;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmStrutsActionServlet extends ActionServlet // throws AppError
{
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
    
    public void init(ServletConfig config)
    {
        try {
        log.debug(" inside GmStrutsActionServlet ");
        super.init(config);
        }
        
        catch(Exception e){
            String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
            log.error("Exception e "+strErrorMsg);
            // throw new AppError(e);
        }

    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
    {
        doPost(request,response);
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException 
    {
        GmServlet gmServlet = new GmServlet();
        HttpSession session = request.getSession(false);
        String strSkipSessionCheck = GmCommonClass.parseNull(request.getParameter("skipSessionCheck"));
        log.debug(" INside POst of Struts servlet Testing from offshore ");
        try {        	
        	if(strSkipSessionCheck.equals(""))
        	{
        		gmServlet.checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
        	}
            super.doPost(request,response);
        } 
        catch(Exception e)
        {
        	e.printStackTrace();
        	String strErrMessage = e.getMessage();
            strErrMessage = strErrMessage.replaceAll("com.globus.common.beans.AppError:","");
            
            String strUserId	= GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
            String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
            log.error("Logged in user : "+strUserId);
            log.error("Exception e "+strErrorMsg);
            
            session.setAttribute("hAppErrorMessage",strErrMessage);            
            gmServlet.gotoStrutsPage("/common/GmError.jsp",request,response);           
        }
        // super.doPost(request,response);
    }
}
