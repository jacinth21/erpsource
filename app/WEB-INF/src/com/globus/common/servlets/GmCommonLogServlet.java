/*****************************************************************************
 * File : GmCommonLogServlet Desc : This Servlet is used to handle the patient master information
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.common.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonLogInterface;
import com.globus.operations.beans.GmLotCodeRptBean;


public class GmCommonLogServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    String strAcctPath = GmCommonClass.getString("GMACCOUNTS");
    String strComnPath = GmCommonClass.getString("GMCOMMON");

    String strDispatchTo = "";
    String strValue = "";
    String strGetID = "";
    String strGetComment = "";
    String strJNDIConnection = "";
    String strBeanId = "";
    String strDispatch = "/GmCallLogDetails.jsp";
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmLotCodeRptBean gmLotCodeRptBean = new GmLotCodeRptBean(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;
    List alList = null;

    try {
      // checkSession(response,session); // Checks if the current session is valid, else redirecting
      // to SessionExpiry page
      String strAction = request.getParameter("hAction");

      strAction = (strAction == null) ? "Load" : strAction;


      String strLogID = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strType = GmCommonClass.parseNull(request.getParameter("hType"));
      String strID = GmCommonClass.parseNull(request.getParameter("hID"));
      String strMode = GmCommonClass.parseNull(request.getParameter("hMode"));
      String strScreen = GmCommonClass.parseNull(request.getParameter("hScreen"));
      strLogID = GmCommonClass.parseNull(request.getParameter("hLogId"));
      strBeanId = GmCommonClass.parseNull(request.getParameter("beanKeyId"));
      strBeanId = strBeanId.equals("")? "LOG": strBeanId;
      strJNDIConnection = GmCommonClass.parseNull(request.getParameter("hJNDIConnection"));
      log.debug(":::my String ID::::" + strLogID + " ::: strDBConnType ::: " + strJNDIConnection);

      HashMap hmComment = new HashMap();
      if (strAction.equals("Save")) {
        strValue = GmCommonClass.parseNull(request.getParameter("Txt_Reason"));
        if (!strValue.equals("")) {
          gmCommonBean.saveLog(strLogID, strID, strValue, strUserId, strType, strJNDIConnection);

        }


      } else if (strAction.equals("editcomment")) {
        log.debug("Edit comments");
        if (!strLogID.equals("")) {
          hmComment = gmCommonBean.fetchLog(strLogID, strJNDIConnection);
          strGetID = (String) hmComment.get("LOGID");

          strGetComment = (String) hmComment.get("COMMENTS");
        }
      }

      if (strScreen.equals("LotCodeRpt")) {
        alList = gmLotCodeRptBean.fetchLogDtls(strID).getRows();
        strDispatch = "/GmTxnLogDtls.jsp";
      } else {
        //to fetch log details in loaner request screen
        GmCommonLogInterface gmCommonLogInterface =
            (GmCommonLogInterface) GmCommonClass.getSpringBeanClass("xml/Get_Call_Log_Beans.xml",
                strBeanId);
        alList = gmCommonLogInterface.getCallLog(strID, strType, strJNDIConnection).getRows();
        if(strBeanId.equals("LOANESCL")){
          strDispatch = "/GmTxnLogDtls.jsp";
        }
      }
      // To load the default value and Combo Box value information
      // hmReturn.put("COMBOVALUE",gmPatient.loadPatientComboValue(strStudyID));
      request.setAttribute("hType", strType);
      request.setAttribute("hID", strID);
      request.setAttribute("hMode", strMode);
      request.setAttribute("results", alList);
      request.setAttribute("hAction", "");
      request.setAttribute("hJNDIConnection", strJNDIConnection);

      request.setAttribute("COMMENTS", strGetComment);
      request.setAttribute("LOGID", strGetID);

      dispatch(strAcctPath.concat(strDispatch), request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmCommonLogServlet
