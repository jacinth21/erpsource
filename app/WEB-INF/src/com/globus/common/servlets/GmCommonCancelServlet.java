package com.globus.common.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.accounts.tax.beans.GmSalesTaxRptBean;
import com.globus.accounts.tax.beans.GmSalesTaxTransBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonCancelBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmProdCatUtil;
import com.globus.common.util.excel.GmExcelManipulation;
import com.globus.custservice.beans.GmDOBean;
import com.globus.logon.auth.GmLDAPManager;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.logistics.beans.GmLoanerDisputedBean;
import com.globus.operations.returns.beans.GmProcessReturnsBean;
import com.globus.sales.event.beans.GmEventReportBean;
import com.globus.sales.pricing.beans.GmPricingRequestBean;
import com.globus.valueobject.accounts.GmCancelTaxResponseVo;

public class GmCommonCancelServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    instantiate(request, response);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strCustPath = GmCommonClass.getString("GMCUSTSERVICE");
    String strDispatchTo = strComnPath.concat("/GmCommonCancel.jsp");
    GmCommonCancelBean gmCommonCancel = new GmCommonCancelBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();
    GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean(getGmDataStoreVO());
    GmCalenderOperations gmCal = new GmCalenderOperations();
    GmLDAPManager gmLDAPManager = null;
    HashMap hmUserDetails = null;
    HashMap hmAccess = new HashMap();
    String strFunctionId = "";
    RowSetDynaClass resultSet = null;
    ArrayList alCancelReason = new ArrayList();
    ArrayList alTypes = new ArrayList();

    String strTxnId = new String();
    String strTxnName = new String();
    String strCancelReason = new String();
    String strCancelType = "";
    String strComments = new String();
    String strUserId = new String();
    String strMonthBeginDate = "";
    String strApplnDateFmt = getGmDataStoreVO().getCmpdfmt();
    // strMonthBeginDate = strTodaysDate.replaceAll("/\\d\\d/","/01/");
    int currMonth = 0;
    currMonth = GmCalenderOperations.getCurrentMonth() - 1;
    strMonthBeginDate =
        GmCommonClass.parseNull(GmCalenderOperations.getAnyDateOfMonth(currMonth, 1,
            strApplnDateFmt));
    String strTodaysDate = GmCalenderOperations.getCurrentDate(strApplnDateFmt);
    String strCancelTypeNM = "";
    String strCancelTypeFromLeftMenu = "";
    String strTransactioName = "";
    String strOptVar1 = "";
    String strCancelTypeName = "";
    String strEnableEdit = "";
    String strCancelReturnVal = "";
    String hSkipCmnCnl = "";
    String strHComments = "";
    String strInHousePurpose = "";
    // Voiding IRB Setup
    String strRedirectURL = "";
    String strAddnlParam = "";
    String strDisplayNm = "";
    // Back to Process Transaction screen to Process CN
    String strHScreenFrom = "";
    // To throw Access permission error
    String strRuleId = "";
    String strCompanyInfo="";
    boolean errorFl = true;

    try {
      checkSession(response, session); // Checks if the current session is valid, else redirecting
                                       // to SessionExpiry page
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      String strAction = request.getParameter("hAction");
      String strJNDIConnection = GmCommonClass.parseNull(request.getParameter("hJNDIConnection"));
      alTypes = GmCommonClass.getCodeList("CNCLT");
      request.setAttribute("hTypes", alTypes);
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      log.debug(" Action before is " + strAction);
      strAction =
          GmCommonClass.parseNull((strAction = (strAction == null) ? "Load" : strAction)) == "" ? "Load"
              : strAction;

      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      if (!strOpt.equals("") && strOpt.indexOf("@") != -1) {
        /*
         * The input would be cancelreport + the cancel type. For eg if the Rollback Set Consignment
         * report has to be opened, then hopt value will be as cancelreport@RBSCN
         */

        strOptVar1 = strOpt.substring(0, strOpt.indexOf("@"));
        strCancelTypeFromLeftMenu = strOpt.substring(strOpt.indexOf("@") + 1);

        if (strCancelTypeFromLeftMenu != null && strCancelTypeFromLeftMenu.indexOf("@") != -1) {

          String strTemp =
              strCancelTypeFromLeftMenu.substring(0, strCancelTypeFromLeftMenu.indexOf("@"));
          strTransactioName =
              strCancelTypeFromLeftMenu.substring(strCancelTypeFromLeftMenu.indexOf("@") + 1);

          strCancelTypeFromLeftMenu = strTemp;

        }
        log.debug(" Opt var1 is " + strOptVar1 + " strCancelTypeFromLeftMenu value is "
            + strCancelTypeFromLeftMenu);
      } else {
        strCancelTypeFromLeftMenu = GmCommonClass.parseNull(request.getParameter("Cbo_Type"));
      }
      log.debug(" Action is " + strAction + " Opt value is " + strOpt);
      strTxnId = GmCommonClass.parseNull(request.getParameter("hTxnId"));

      if ((strAction.equals("Load") || strAction.equals("cancel")) && strTxnId.equals("")
          && !strOptVar1.equals("cancelreport")) {
        throw new AppError("", "20684");
      }
      HashMap hmParam = new HashMap();
      ArrayList alReasons = new ArrayList();
      ArrayList alUsers = new ArrayList();
      if (strAction.equals("reportForCancel") || strAction.equals("reasonLoad")) {
        strTxnId = GmCommonClass.parseNull(request.getParameter("Txt_TxnId"));
        String strFromDate =
            request.getParameter("Txt_FromDate") == null ? strMonthBeginDate : request
                .getParameter("Txt_FromDate");
        String strToDate =
            request.getParameter("Txt_ToDate") == null ? strTodaysDate : request
                .getParameter("Txt_ToDate");
        strCancelTypeFromLeftMenu =
            GmCommonClass.parseNull(request.getParameter("hCancelTypeFromMenu"));
        strCancelTypeName = GmCommonClass.parseNull(request.getParameter("hCancelTypeName"));
        strCancelReason = GmCommonClass.parseNull(request.getParameter("Cbo_Reason"));
        String strCancelUser = GmCommonClass.parseNull(request.getParameter("Cbo_UserFilter"));
        String strCancelComment = GmCommonClass.parseNull(request.getParameter("Txt_Comment"));

        alReasons = GmCommonClass.getCodeList(strCancelTypeFromLeftMenu);
        alUsers = gmLogonBean.getEmployeeList();
        hmParam.put("TXNID", strTxnId);
        hmParam.put("FROMDATE", strFromDate);
        hmParam.put("TODATE", strToDate);
        hmParam.put("CANCELTYPEFROMMENU", strCancelTypeFromLeftMenu);
        hmParam.put("CANCELTYPENAME", strCancelTypeName); // strCancelTypeName will be displayed as
                                                          // Title in the JSP
        hmParam.put("CANCELREASON", strCancelReason);
        hmParam.put("CANCELUSER", strCancelUser);
        hmParam.put("CANCELCOMMENT", strCancelComment);
        request.setAttribute("HMPARAM", hmParam);
        request.setAttribute("hAction", strAction);
        request.setAttribute("hReasons", alReasons);
        request.setAttribute("TRANSACTIONNAME", strTransactioName);
        request.setAttribute("alUsers", alUsers);
      }
      if (strAction.equals("Load")) {
        strCancelType = GmCommonClass.parseNull(request.getParameter("hCancelType"));
        strRuleId = strCancelType;
        String strOrederType = GmCommonClass.parseNull(request.getParameter("hOrderType"));
        // 2518- Order Type = Draft
        if (strOrederType.equals("2518")) {
          errorFl = false;
        } else {
          errorFl = true;
        }
        strInHousePurpose = GmCommonClass.parseNull(request.getParameter("strRequestPurpose"));
        // To get the function Id for cancelTypes based on InHousePurpose
        if (!strInHousePurpose.equals("")) {
          strRuleId = strCancelType + "_" + strInHousePurpose;
        }
        log.debug("strCancelType::" + strCancelType + ":::" + strInHousePurpose + "Rule ID:::"
            + strRuleId);
        // Getting Function ID from rule table
        strFunctionId =
            GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRuleId, "ACCESSPERMISSION"));

        strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
        if (!strFunctionId.equals("")) {
          hmAccess =
              GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,
                  strFunctionId));
          String strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
          String strReadAccess = GmCommonClass.parseNull((String) hmAccess.get("READFL"));
          String strVoidAccess = GmCommonClass.parseNull((String) hmAccess.get("VOIDFL"));
          log.debug("strVoidAccess:::" + strVoidAccess + ":::strUserId:::" + strUserId);
          if (!strVoidAccess.equals("Y") && !strUpdateAccess.equals("Y") && errorFl) {
            throw new AppError("", "20685");
          }
        }
        strTxnName = GmCommonClass.parseNull(request.getParameter("hTxnName"));

        strTxnName = GmCommonClass.formatSpecialCharFromDB(strTxnName);
        String strSrc = GmCommonClass.parseNull(request.getParameter("hSrcDir"));
        String strDest = GmCommonClass.parseNull(request.getParameter("hDestDir"));
        String strFileName = GmCommonClass.parseNull(request.getParameter("hFileName"));
        strEnableEdit = GmCommonClass.parseNull(request.getParameter("hEnableEdit"));
        strCancelReturnVal = GmCommonClass.parseNull(request.getParameter("hCancelReturnVal"));
        strRedirectURL = GmCommonClass.parseNull(request.getParameter("hRedirectURL"));
        // Back to Process Transaction screen to Process CN
        strHScreenFrom = GmCommonClass.parseNull(request.getParameter("hScreenFrom"));
        log.debug("hRedirectURL==" + strRedirectURL + "hScreenFrom==" + strHScreenFrom);
        strHComments = GmCommonClass.parseNull(request.getParameter("hLogReason"));
        strAddnlParam = GmCommonClass.parseNull(request.getParameter("hAddnlParam"));
        strDisplayNm = GmCommonClass.parseNull(request.getParameter("hDisplayNm"));

        hSkipCmnCnl = GmCommonClass.parseNull(request.getParameter("hSkipCmnCnl"));
        log.debug("hSkipCmnCnl:::" + hSkipCmnCnl);
        // strCancelTypeNM would be set as the page header. This is the code id for the
        // corresponding code_nm_alt.
        // Eg: RBSCN - Rollback Set Consignment
        strCancelTypeNM = GmCommonClass.parseNull(gmCommon.getCodeNameFromAlt(strCancelType));
        // PC-3573 DO reject reasons should be in Japanese
        alCancelReason = strCancelType.equals("VDRES") ? gmCommon.getCodeList(strCancelType,gmCommonCancel.getGmDataStoreVO()):gmCommon.getCodeList(strCancelType); // 
        log.debug("strCancelReturnVal on load " + strCancelReturnVal);
        log.debug(" Cancel TYpe is " + strCancelType + " Transaction ID " + strTxnId
            + " Cancel Reason List size " + alCancelReason.size() + "strEnableEdit" + strEnableEdit);
        request.setAttribute("STRCANCELTYPE", strCancelType);
        request.setAttribute("STRTXNID", strTxnId);
        request.setAttribute("STRTXNNAME", strTxnName);
        request.setAttribute("ALCANCELREASON", alCancelReason);
        request.setAttribute("STRHEADERFROMCODEID", strCancelTypeNM);
        request.setAttribute("STRSRC", strSrc);
        request.setAttribute("STRDEST", strDest);
        request.setAttribute("STRFILENAME", strFileName);
        request.setAttribute("TRANSACTIONNAME", strTransactioName);
        request.setAttribute("ENABLEEDIT", strEnableEdit);
        request.setAttribute("CANCELRETURNVAL", strCancelReturnVal);
        request.setAttribute("HLOGREASON", strHComments);
        // For Back Button
        request.setAttribute("REDIRECTURL", strRedirectURL);
        request.setAttribute("ADDITIONALPARAM", strAddnlParam);
        request.setAttribute("DISPLAYNAME", strDisplayNm);
        request.setAttribute("HSKIPCMNCNL", hSkipCmnCnl);
        request.setAttribute("JNDICONNTCTION", strJNDIConnection);
        // Back to Process Transaction screen to Process CN
        request.setAttribute("hScreenFrom", strHScreenFrom);


      }

      else if (strAction.equals("cancel")) {

        HashMap hmReturn = new HashMap();
        GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());
        GmOperationsBean gmOperationsBean = new GmOperationsBean(getGmDataStoreVO());
        strTxnName = GmCommonClass.parseNull(request.getParameter("hTxnName"));
        strCancelReason = GmCommonClass.parseNull(request.getParameter("Cbo_Reason"));
        strCancelType = GmCommonClass.parseNull(request.getParameter("hCancelType"));
        strComments = GmCommonClass.parseNull(request.getParameter("Txt_Comments"));
        strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
        strCancelReturnVal = GmCommonClass.parseNull(request.getParameter("hCancelReturnVal"));

        // Voiding IRB Setup Back Button
        strRedirectURL = GmCommonClass.parseNull(request.getParameter("hRedirectURL"));
        strAddnlParam = GmCommonClass.parseNull(request.getParameter("hAddnlParam"));
        strDisplayNm = GmCommonClass.parseNull(request.getParameter("hDisplayNm"));
        hSkipCmnCnl = GmCommonClass.parseNull(request.getParameter("hSkipCmnCnl"));

        // Back to Process Transaction screen to Process CN
        strHScreenFrom = GmCommonClass.parseNull(request.getParameter("hScreenFrom"));

        alCancelReason = gmCommon.getCodeList(strCancelType);
        // strCancelTypeNM would be set as the page header. This is the code id for the
        // corresponding code_nm_alt.
        // Eg: RBSCN - Rollback Set Consignment
        strCancelTypeNM = GmCommonClass.parseNull(gmCommon.getCodeNameFromAlt(strCancelType));

        hmParam.put("TXNID", strTxnId);
        hmParam.put("TXNNAME", strTxnName);
        hmParam.put("CANCELREASON", strCancelReason);
        hmParam.put("CANCELTYPE", strCancelType);
        hmParam.put("COMMENTS", strComments);
        hmParam.put("USERID", strUserId);
        hmParam.put("CANCELRETURNVAL", strCancelReturnVal);
        hmParam.put("JNDI", strJNDIConnection);
        hmParam.put("HSKIPCMNCNL", hSkipCmnCnl);
        hmParam.put("TRANSACTIONNAME", strTransactioName);
        hmParam.put("REQUESTVALUE",
            GmCommonClass.parseNullHashMap((HashMap) session.getAttribute("requestvalue")));
        hmParam.put("HSRCDIR", GmCommonClass.parseNull(request.getParameter("hSrcDir")));
        hmParam.put("HDESTDIR", GmCommonClass.parseNull(request.getParameter("hDestDir")));
        hmParam.put("HFILENAME", GmCommonClass.parseNull(request.getParameter("hFileName")));
        hmParam.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
        strCompanyInfo =GmCommonClass.parseNull(request.getParameter("companyInfo"));
        hmParam.put("COMPANYINFO", strCompanyInfo);
        hmReturn = processCancel(hmParam);

        alCancelReason =
            GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("ALCANCELREASON"));
        hmParam = GmCommonClass.parseNullHashMap((HashMap) hmReturn.get("HMPARAM"));
        strCancelTypeNM = GmCommonClass.parseNull((String) hmReturn.get("STRHEADERFROMCODEID"));
        strTransactioName = GmCommonClass.parseNull((String) hmReturn.get("TRANSACTIONNAME"));

        request.setAttribute("ALCANCELREASON", alCancelReason);
        request.setAttribute("HMPARAM", hmParam);
        request.setAttribute("hAction", "SUCCESSCANCEL");
        request.setAttribute("STRHEADERFROMCODEID", strCancelTypeNM);
        request.setAttribute("TRANSACTIONNAME", strTransactioName);

      } else if (strAction.equals("reportForCancel")) {
        log.debug(" Values in hashMap for Cancel is " + hmParam);
        ArrayList alReturn = new ArrayList();
        alReturn = gmCommonCancel.viewCancelInfo(hmParam);
        log.debug(" values in alReturn " + alReturn);
        request.setAttribute("ALRETURN", alReturn);
        strDispatchTo = strComnPath.concat("/GmCancelReport.jsp");
      } else if (strAction.equals("reasonLoad")) {
        log.debug(" Values in hashMap for Cancel is " + hmParam);

        strDispatchTo = strComnPath.concat("/GmCancelReport.jsp");
      }
      if (strOptVar1.equals("cancelreport") && strAction.equals("Load")) {
        String strFromDate =
            request.getParameter("Txt_FromDate") == null ? strMonthBeginDate : request
                .getParameter("Txt_FromDate");
        String strToDate =
            request.getParameter("Txt_ToDate") == null ? strTodaysDate : request
                .getParameter("Txt_ToDate");
        // strCancelTypeName will be displayed as Title in the JSP
        strCancelTypeName = gmCommon.getCodeNameFromAlt(strCancelTypeFromLeftMenu);
        alReasons = GmCommonClass.getCodeList(strCancelTypeFromLeftMenu);
        alUsers = gmLogonBean.getEmployeeList();
        request.setAttribute("alUsers", alUsers);
        log.debug(" strOpt is " + strOpt);
        request.setAttribute("FROMDATE", strFromDate);
        request.setAttribute("TODATE", strToDate);
        request.setAttribute("hCancelTypeFromMenu", strCancelTypeFromLeftMenu);
        request.setAttribute("hCancelTypeName", strCancelTypeName);
        request.setAttribute("hReasons", alReasons);
        request.setAttribute("TRANSACTIONNAME", strTransactioName);
        request.setAttribute("alUsers", alUsers);
        strDispatchTo = strComnPath.concat("/GmCancelReport.jsp");
      }
      if (strCancelType.equals("RBSCN")) {
        strDispatchTo = strComnPath.concat("/GmCommonCancel.jsp");
      }

      log.debug(" Dispatch to is " + strDispatchTo);
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (AppError e) {
      log.debug(" Exception message " + e.getMessage());
      request.setAttribute("hRedirectURL", strRedirectURL);
      request.setAttribute("hAddnlParam", strAddnlParam);
      request.setAttribute("hDisplayNm", strDisplayNm);
      request.setAttribute("hScreenFrom", strHScreenFrom);

      session.setAttribute("hAppErrorMessage", e.getMessage());
      request.setAttribute("hType", e.getType());
      strDispatchTo = strComnPath + "/GmError.jsp";
      dispatch(strDispatchTo, request, response);

    } catch (Exception e) {
      log.debug(" Inside Exception e " + e.getMessage());
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }

    // End of catch
  }// End of service method

  /**
   * processCancel this method will do cancel the transactions.
   * 
   * @param HashMap
   * @return HashMap
   * @exception AppError,Exception
   */
  public HashMap processCancel(HashMap hmParam) throws AppError, Exception {

    GmPricingRequestBean gmPricingRequestBean = new GmPricingRequestBean(getGmDataStoreVO());
    GmCommonClass gmCommon = new GmCommonClass();
    GmCommonCancelBean gmCommonCancel = new GmCommonCancelBean(getGmDataStoreVO());// getGmDataStoreVO()
    GmOperationsBean gmOperationsBean = new GmOperationsBean(getGmDataStoreVO());
    GmProcessReturnsBean gmProcessReturnsBean = new GmProcessReturnsBean(getGmDataStoreVO());
    GmLoanerDisputedBean gmLoanerDisputedBean = new GmLoanerDisputedBean(getGmDataStoreVO());
    GmLDAPManager gmLDAPManager = null;
    HashMap hmReturn = new HashMap();
    HashMap hmEmailParam = new HashMap();
    HashMap hmUserDetails = null;
    ArrayList alCancelReason = new ArrayList();

    log.debug(" Values in hashMap for processCancel is " + hmParam);
    String strCancelType = GmCommonClass.parseNull((String) hmParam.get("CANCELTYPE"));
    String strTxnName = GmCommonClass.parseNull((String) hmParam.get("TXNNAME"));
    String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strCancelReason = GmCommonClass.parseNull((String) hmParam.get("CANCELREASON"));
    String strTransactioName = GmCommonClass.parseNull((String) hmParam.get("TRANSACTIONNAME"));
    String hSkipCmnCnl = GmCommonClass.parseNull((String) hmParam.get("HSKIPCMNCNL"));
    String strComments = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strCancelTypeNM = GmCommonClass.parseNull(gmCommon.getCodeNameFromAlt(strCancelType));
    alCancelReason = gmCommon.getCodeList(strCancelType);
    try {
      if (hSkipCmnCnl.equals("YES")) {
        if (strCancelType.equals("DNYRST")) {
          hmParam.putAll(GmCommonClass.parseNullHashMap((HashMap) hmParam.get("REQUESTVALUE")));
          gmPricingRequestBean.saveDeniedPricingRequest(hmParam);
          strCancelTypeNM = GmCommonClass.parseNull(gmCommon.getCodeNameFromAlt(strCancelType));
          strCancelTypeNM =
              strCancelTypeNM.concat(" for <U><I>").concat((String) hmParam.get("STRREQUESTID"))
                  .concat("</I></U> performed ");
          throw new AppError(strCancelTypeNM, "", 'S');
        }
      } else {
        gmCommonCancel.UpdateCancel(hmParam);
      }
    } catch (AppError e) {
      // removed below code for PMT-16393 User Profile Changes
      // To remove LDAP server checks, while doing deactivate or activate AD user

      // GmLdapBean gmLdapBean = new GmLdapBean(getGmDataStoreVO());
      // String strLdapId = "";
      // if (strCancelType.equals("USRTRM")) {// terminate user from AD
      // strLdapId = gmLdapBean.getLdapId(strTxnId);
      // gmLDAPManager = new GmLDAPManager(strLdapId);
      //
      // hmUserDetails = GmCommonClass.parseNullHashMap(gmLDAPManager.getADUserDetails(strTxnName));
      //
      // // Disable User in AD, only if he exists in AD.
      // if (hmUserDetails.size() > 0) {
      // gmLDAPManager.disableUser(strTxnName);
      // }
      // log.debug("AD inactive user for " + strTxnName);
      //
      // } else if (strCancelType.equals("USRRLB")) {// User roll back from AD
      // strLdapId = gmLdapBean.getLdapId(strTxnId);
      // gmLDAPManager = new GmLDAPManager(strLdapId);
      //
      // hmUserDetails = GmCommonClass.parseNullHashMap(gmLDAPManager.getADUserDetails(strTxnName));
      //
      // // Enable User in AD, only if he exists in AD.
      // if (hmUserDetails.size() > 0) {
      // gmLDAPManager.enableUser(strTxnName);
      // }
      //
      // log.debug("AD rollback user for " + strTxnName);
      // }

      if (strCancelType.equals("VDRGU")) { // delete the file (move to obsolete)
        String strSrc = GmCommonClass.parseNull((String) hmParam.get("HSRCDIR"));
        String strDest = GmCommonClass.parseNull((String) hmParam.get("HDESTDIR"));
        String strFileName = GmCommonClass.parseNull((String) hmParam.get("HFILENAME"));
        GmExcelManipulation.copyFile(strSrc + strFileName, strDest + strFileName);
        GmExcelManipulation.delete(strSrc + strFileName);
      }
      if (strCancelType.equals("LNSWAP")) {// Need to send an email if an RA is created
        // fetch the ra id
        String strRAID = GmCommonClass.parseNull(gmOperationsBean.fetchRAId(strTxnId));
        log.debug("strRaId  is " + strRAID);
        if (!strRAID.equals("")) {
          log.debug("Sending RA email");
          hmReturn = gmProcessReturnsBean.loadCreditReport(strRAID, "PRINT");
          hmReturn.put("EMAILFILENM", "GmRAInitialEmail");
          hmReturn.put("JASPERNAME", "/GmRAStatusEmail.jasper");
          hmReturn.put("ISCREDIT", "NOTCREDIT");
          hmReturn.put("SUBREPORT_DIR", hmParam.get("SUBREPORT_DIR"));
          gmProcessReturnsBean.sendRAStatusEmail(hmReturn);
        }
      }

      if (strCancelType.equals("CANEVE")) {
        log.debug("-----welcome to Jasper code----");
        GmEventReportBean gmEventRptBean = new GmEventReportBean(getGmDataStoreVO());
        gmEventRptBean.sendEventCancellationEmail(strTxnId, strCancelReason, strUserId);
      }
      if (strCancelType.equals("VILNRQ") || strCancelType.equals("VLNRQ")
          || strCancelType.equals("VDIHLN") || strCancelType.equals("ITEMRQ")
          || strCancelType.equals("SETRQ")) {
        GmEventReportBean gmEventRptBean = new GmEventReportBean(getGmDataStoreVO());
        // PMT-6432 :we are putting the below values in to Hash map, this Hash map values we are
        // passing as parameters to the sendEventSetRejectionEmail method.
        hmParam.put("TXNID", strTxnId);
        hmParam.put("CANCELACTION", "CANCEL");
        hmParam.put("CANCELTYPE", strCancelType);
        hmParam.put("COMMENTS", strComments);
        gmEventRptBean.sendEventSetRejectionEmail(hmParam);
      }

      if (strCancelType.equals("VUPDF")) {
        /*
         * strCancelType - VUPDF is used for Product catalog void the records into database and
         * deleteProdCatFiles method is used to delete the files from the system folder for voided
         * records strTxnId = comma seprated string (like = FileListId,FileListId,FileListId)
         */
        GmProdCatUtil gmProdCatUtil = new GmProdCatUtil();
        // PC-4295: spineIT upload details added to cloud
        // to pass the User id
        gmProdCatUtil.deleteProdCatFiles(strTxnId, strUserId);
      }
      if (strCancelType.equals("VDRES")) {
        /*
         * strCancelType - VDRES is used for sales orders void the records from DB and syncDOFile
         * method is used to delete the files from the source folder for voided records strTxnId =
         * order id
         */
        GmDOBean gmDOBean = new GmDOBean(getGmDataStoreVO());
        gmDOBean.syncDOFile(strTxnId, "deletefile");
      }
      // to void the Invoice at AvaTax web server.
      if (strCancelType.equals("VDINV")) {
        // check the invoice weather - taxable invoice or not
        GmSalesTaxTransBean gmSalesTaxTransBean = new GmSalesTaxTransBean();
        GmSalesTaxRptBean gmSalesTaxRptBean = new GmSalesTaxRptBean(getGmDataStoreVO());
        HashMap hmAvaTaxParam = new HashMap();
        hmAvaTaxParam = gmSalesTaxRptBean.fetchTaxInvoiceDtls(strTxnId);
        String strInvTaxFl = GmCommonClass.parseNull((String) hmAvaTaxParam.get("TAX_FL"));
        hmAvaTaxParam.put("CANCELCODE", "DocVoided");
        hmAvaTaxParam.put("DOCCODE", strTxnId); // Invoice number
        // to avoid the calling the non tax invoices
        if (strInvTaxFl.equals("Y")) {
          GmCancelTaxResponseVo gmCancelTaxResponseVo = new GmCancelTaxResponseVo();
          try {
            gmCancelTaxResponseVo = gmSalesTaxTransBean.cancelSalesTax(hmAvaTaxParam);
          } catch (Exception ex) {
            log.debug(" Exception  " + ex);
          }
        }
      }
      if (strCancelType.equals("LNDSPT"))//Loaner Disputed
      {
          hmEmailParam.put("USERID", strUserId);
	      hmEmailParam.put("CANCELTYPE", strCancelType);
	      hmEmailParam.put("CONSID", strTxnId);
	      hmEmailParam.put("COMPANYINFO", GmCommonClass.parseNull((String) hmParam.get("COMPANYINFO")));
	      hmEmailParam.put("ACTION", "DISPUT");
	
	      //JMS call to send mail for moving set to Disputed Status
	      gmLoanerDisputedBean.initDisputedEmailJMS(hmEmailParam);
      }                                                           

      throw e;
      // throw e.getException();
    }
    hmReturn.put("ALCANCELREASON", alCancelReason);
    hmReturn.put("HMPARAM", hmParam);
    hmReturn.put("hAction", "SUCCESSCANCEL");
    hmReturn.put("STRHEADERFROMCODEID", strCancelTypeNM);
    hmReturn.put("TRANSACTIONNAME", strTransactioName);

    return hmReturn;
  }
}// End of GmCommonCancelServlet
