/**
 * @author karthik
 * class Name: GmEmailBean.java
 * Description: This class will set the AWS credential to the Statis variable
 */

package com.globus.common.email;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmEmailBean extends GmBean {
	public static GmEmailManagerInterface gmEmailManager ;
	public static boolean awscredential;
	public static AmazonSimpleEmailService awsClient;
	public static String emailServiceType;
	public static String FromId;
	public static String emailConsumerClass;
	public static String emailQueueName;
	public static String emailHeader;
	//PC-2939
	public static GmEmailManagerInterface gmEmailSMTPManager = new GmEmailThroughGlobusSMTP();
	
  Logger log = GmLogger.getInstance(this.getClass().getName());
  public GmEmailBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmEmailBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  }
  
  /**
   * initializeEmailComponent(): This method will load the Email credential
   * 
   * @return void
   * @exception AppError
   */
  public void initializeEmailComponent() {
	  ProfileCredentialsProvider credentialsProvider = new ProfileCredentialsProvider();	
	   emailConsumerClass = GmCommonClass.parseNull(GmCommonClass.getString("EMAIL_CONSUMER_CLASS"));
	   emailQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_EMAIL_LOC_QUEUE"));
	   emailHeader = GmCommonClass.parseNull(GmCommonClass.getString("EMAIL_HEADER"));
	  try {
		      /* Instantiate an Amazon SES client, which will make the service 
		       * call with the supplied AWS credentials.
		       * From id : spineitcomms@spineit.net
		       */
		  	  GmEmailBean.FromId  = GmCommonClass.parseNull(GmCommonClass.getString("SPINEIT_FROM_EMAIL"));
		      AmazonSimpleEmailService client = 
		              AmazonSimpleEmailServiceClientBuilder.standard()
		              .withCredentials(new AWSStaticCredentialsProvider(credentialsProvider.getCredentials()))
		              .withRegion(Regions.US_EAST_1)	                    
		              .build();
		      GmEmailBean.awsClient = client;
		      String strawscredential=  System.getProperty("ENV_AWSMAIL");
		      /*if the amazon client is installed in the server and the AWSEMAIL key should be "Y" to send the email via Amazon Web Services
		       */
			  if (!client.equals("") && strawscredential.equalsIgnoreCase("Y")){
				   GmEmailBean.awscredential = true;
				   GmEmailBean.emailServiceType = GmCommonClass.parseNull(GmCommonClass.getString("EMAIL_AWS"));
				   GmEmailBean.gmEmailManager =  GmEmailFactory.getInstance();
			  }
	     } catch (Exception amazonClientException) {
	    	 	  GmEmailBean.awscredential = false;
	    	 	  GmEmailBean.emailServiceType = GmCommonClass.parseNull(GmCommonClass.getString("EMAIL_SMTP"));
	    	 	  GmEmailBean.gmEmailManager =  GmEmailFactory.getInstance();
	              log.debug("client in exception");
	              amazonClientException.printStackTrace();
	        }
	  log.debug("Email Manager Object: " +  GmEmailBean.gmEmailManager.toString());
  }
  
  /** saveMailDtls(): This method save the Email details in email tracking *
   * This method is moved from GmCommonClass.java file with the Amazon Webservice Changes*/

  public static void saveMailDtls(HashMap hmParams) throws Exception {
    String strMailFrom = GmCommonClass.parseNull((String) hmParams.get("EMAILFROM"));
    String strMailTo = GmCommonClass.parseNull((String) hmParams.get("EMAILTO"));
    String strMailCC = GmCommonClass.parseNull((String) hmParams.get("EMAILCC"));
    strMailCC = strMailCC.equals("null") ? "" : strMailCC;
    String strMailSub = GmCommonClass.parseNull((String) hmParams.get("EMAILSUBJECT"));
    String strMailMsg = GmCommonClass.parseNull((String) hmParams.get("EMAILMESSAGE"));

    try {
      GmDBManager gmDBManager = new GmDBManager();
      gmDBManager.setPrepareString("gm_save_email_details", 5);
      gmDBManager.setString(1, strMailFrom);
      gmDBManager.setString(2, strMailTo);
      gmDBManager.setString(3, strMailCC);
      gmDBManager.setString(4, strMailSub);
      gmDBManager.setString(5, strMailMsg);

      gmDBManager.execute();
      gmDBManager.commit();
    } catch (Exception e) {
      GmLogError.log("Exception in GmCommonClass: saveMailDtls", " Exception is:" + e);
      throw new Exception(e);
    }

  }
  
}
