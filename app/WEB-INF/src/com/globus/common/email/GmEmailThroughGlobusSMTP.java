package com.globus.common.email;

import java.util.HashMap;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

/**
 * @author karthik
 *
 */
public class GmEmailThroughGlobusSMTP extends GmEmailManagerAbstract {
	/* sendEmail():This method will be call through the GmEmailManagerAbstract and the mail will be send through SMTP server 
	 * if the AWS credential is not available in the server
	 * */
	public void sendEmail() throws Exception{
		try {
			HashMap hmparam = new HashMap();
			Session session = Session.getDefaultInstance(new Properties());
			Message msg = new MimeMessage(session);
			msg = getEmailMessage();
		      if (msg.getAllRecipients().length > 0) {
		        Transport.send(msg);
		        log.debug("Mail sent through SMTP..");
		      	}
        } catch (Exception ex) {
             log.debug("Error message: " + ex.getMessage());
             ex.printStackTrace();
             throw new Exception(ex);
        }
	}
}