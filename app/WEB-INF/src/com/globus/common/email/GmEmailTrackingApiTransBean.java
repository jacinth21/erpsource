/**
 * @author mmuthusamy
 * class Name: GmEmailTrackingApiTransBean.java
 * Description: This class used to send email via sendGrid api
 */

package com.globus.common.email;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GmEmailTrackingApiTransBean extends GmBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * @param gmDataStoreVO
	 */
	public GmEmailTrackingApiTransBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
	}

	/**
	 * @param hmParam
	 * @throws AppError
	 */
	
	public void sendEmail(HashMap hmParam) throws AppError, Exception {

		String strEmailJson = "";

		StringBuffer sbQuery = new StringBuffer();
		String strMessageId = "";

		// 1 to create the api Connection
		log.debug(" before call connection ");
		HttpURLConnection connection = createApiConnection();

		log.debug(" before call email JSON ");
		// 2 to pass the data inside the WS
		strEmailJson = getEmailJSON(hmParam);

		log.debug(" before call send email web service " + strEmailJson);

		// 3 to call the web service
		strMessageId = processSendEmail(connection, strEmailJson);
		log.debug(" strMessageId ==> " + strMessageId);
		hmParam.put("TRACK_MESSAGE_ID", strMessageId);
		// 4 to save the email details to tracking table
		
		try {
			log.debug(" before calling the email tracking Dtls");
			saveEmailTrackingDtls(hmParam);
			log.debug (" After saved email dtls ");

		} catch (Exception ex) {

			log.error(" Exception Dtls ==> "+ex.toString());
		}
		
	}

	/**
	 * @return
	 * @throws AppError
	 */
	
	private HttpURLConnection createApiConnection() throws AppError,Exception {
		HttpURLConnection connection = null;

		// to call the Web service
		URL url;

		ObjectOutputStream out;
		String strEmailTrackURL = GmCommonClass.parseNull((String) System.getProperty("EMAIL_TRACKING_URL"));
		log.debug("strEmailTrackURL ==> "+ strEmailTrackURL);
		
		// Creating the URL.
		url = new URL(strEmailTrackURL); 
		connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");

		// connection.setRequestProperty("Authorization", strEncoded); // Add
		// authorization header
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		return connection;
	}

	/**
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	
	private String getEmailJSON(HashMap hmParam) throws AppError,Exception {
		String strReturnEmailJson = "";
		//
		String strFromEmailId = "";
		String strToEmailIds = "";
		String strSubject = "";
		String strMessage = "";

		String[] strTo = (String[]) hmParam.get("TO");
		String[] strCc = (String[]) hmParam.get("CC");

		for (int i = 0; i < strTo.length; i++) {
			strToEmailIds = strToEmailIds + strTo[i] + ",";
		}

		log.debug(" To Email ids ==> "+ strToEmailIds);
		strSubject = GmCommonClass
				.parseNull((String) hmParam.get("STRSUBJECT"));
		strMessage = GmCommonClass.parseNull((String) hmParam
				.get("STRMESSAGEDESC"));
		// to format the JSON string
		strMessage = getJSOnStr (strMessage);
		
		log.debug(" strMessage ==> "+ strMessage);
		strReturnEmailJson = "{\"fromEmailId\" : \"\",  \"recipient\": \""
				+ strToEmailIds + "\", \"emailSubject\": \"" + strSubject
				+ "\", \"emailBody\": "+ strMessage + "}";

		return strReturnEmailJson;
	}

	
	
	/**
	 * @param obj
	 * @return
	 * @throws Exception
	 */
	
	public static String getJSOnStr(Object obj) throws Exception{
		ObjectMapper mapper = new ObjectMapper();
		Writer strWriter = new StringWriter();
		mapper.writeValue(strWriter, obj);
		return strWriter.toString();
		
	}
	
	/**
	 * @param connection
	 * @param strEmailJson
	 * @return
	 * @throws AppError
	 */
	
	private String processSendEmail(HttpURLConnection connection,
			String strEmailJson) throws AppError,Exception {
		// 3 to call the web service
		String strOutput = "";
		StringBuffer sbQuery = new StringBuffer();
		String strMessageId = "";

		OutputStreamWriter wr = new OutputStreamWriter(
				connection.getOutputStream());
		wr.write(strEmailJson);
		wr.flush();
		wr.close();

		int intResponseCode = connection.getResponseCode();
		log.debug(" after call send email web service " + intResponseCode);

		BufferedReader br = new BufferedReader(new InputStreamReader(
				(connection.getInputStream())));

		while ((strOutput = br.readLine()) != null) {
			sbQuery.append(strOutput);
		}

		strMessageId = sbQuery.toString();
		connection.disconnect();
		return strMessageId;
	}
	
	/**
	 * @param hmParam
	 * @throws AppError
	 */

	public void saveEmailTrackingDtls(HashMap hmParam) throws AppError, Exception {
		String strTransactionId = "";
		String strEmailTrackingType = "";
		String strEmailNotificationFl = "";
		String strTrackingMsgId = "";
		String strSubject = "";
		String strMessage = "";

		strTransactionId = GmCommonClass.parseNull((String) hmParam.get("TRANSACTION_ID"));
		strEmailTrackingType = GmCommonClass.parseNull((String) hmParam.get("EMAIL_TRACKING_TYPE"));
		strTrackingMsgId = GmCommonClass.parseNull((String) hmParam.get("TRACK_MESSAGE_ID"));
		strEmailNotificationFl = GmCommonClass.parseNull((String) hmParam.get("EMAIL_TRACKING_FL"));

		String[] strToIds = (String[]) hmParam.get("TO");
		String[] strCcIds = (String[]) hmParam.get("CC");

		String strMailTo = Arrays.toString(strToIds);
		strMailTo = strMailTo.replaceAll("\\[", "").replaceAll("\\]", "");
		String strMailCC = Arrays.toString(strCcIds);
		strMailCC = strMailCC.replaceAll("\\[", "").replaceAll("\\]", "");

		String strMailFrom = GmCommonClass.parseNull((String) hmParam.get("EMAILFROM"));
		strMailCC = strMailCC.equals("null") ? "" : strMailCC;
		strSubject = GmCommonClass.parseNull((String) hmParam.get("STRSUBJECT"));
		strMessage = GmCommonClass.parseNull((String) hmParam.get("STRMESSAGEDESC"));
		
		// to format the JSON string
		strMessage = getJSOnStr(strMessage);

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_cm_email_tracking_response_load.gm_sav_email_track_dtls", 9);
		gmDBManager.setString(1, strTransactionId);
		gmDBManager.setString(2, strTrackingMsgId);
		gmDBManager.setString(3, strMailTo);
		gmDBManager.setString(4, strMailCC);
		gmDBManager.setString(5, strSubject);
		gmDBManager.setString(6, strMessage);
		gmDBManager.setString(7, "");// Send by
		gmDBManager.setString(8, strEmailNotificationFl);// Send by
		gmDBManager.setString(9, "30301");// Send by

		gmDBManager.execute();
		gmDBManager.commit();
	}

}
