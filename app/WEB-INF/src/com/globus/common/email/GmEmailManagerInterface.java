package com.globus.common.email;

import java.util.HashMap;

/**
 * @author karthik
 * Class Name:GmEmailManagerInterface
 * Description: Declaring the main methods to send a SpineIT mail via AWS/SMTP protocol 
 */

public interface GmEmailManagerInterface 
{
  public void setUpEmail(HashMap hmParam) throws Exception;

  public void sendEmail() throws Exception;
	 
  public void saveMailInDB() throws Exception;
}
