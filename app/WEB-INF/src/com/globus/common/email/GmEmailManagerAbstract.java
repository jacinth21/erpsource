package com.globus.common.email;

import java.util.HashMap;
import java.io.ByteArrayOutputStream;
import java.io.File;
//import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import com.globus.common.beans.GmCommonClass;
import org.apache.log4j.Logger;
import com.globus.common.beans.GmLogger;

/**
 * @author karthik
 *
 */

public abstract class GmEmailManagerAbstract implements GmEmailManagerInterface {
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/*Initialize all the variables as class variables
	 * */
	protected String [] strTo;
	protected String [] strCc;
	protected String [] strBcc;
	protected String [] strReplyTo;
	protected HashMap hmParam = new HashMap();
	protected String strFrom ="";
	protected String strFromID = "";
	protected String strSubject ="";
	protected String strMessageDesc ="";
	protected String strAttachment ="";
	protected String strMimeType ="";
	protected String toEmail ="";
	protected String strTestEmailSubject ="";
	protected String strTestEmailID ="";
	protected boolean blWhitelistserver;
	protected boolean blTestingserver;
	
	//protected String[] SystName = new String[2];
	
	protected HashMap hmparams = new HashMap();
	protected String strMailSubject ="";
	protected String strEmailFooter ="";
	protected String strEmailMessageFooter="";
	protected boolean boolReturn;
	protected boolean bMachineEmail;
	protected String[] strToIds = null;
	protected String[] strCcIds = null;
	protected String[] strBccIds = null;
	protected String strSMTPServerIP = "";
	protected String stremailDomain = null;
	protected String systemName = "";
	protected String strSystemNm = "";
	protected InternetAddress[] strToRecipient ;
	protected boolean isToIdPresent = false;
	protected boolean isCCIdPresent = false;
	protected InternetAddress[] strRecipient = null;
	protected boolean isBCCIdPresent = false;
	protected InternetAddress[] bcclist = null;
	protected boolean isReplyToIdPresent = false;
	protected InternetAddress[] replyToIds = null;
	protected HashMap hmreturn = new HashMap();
	protected boolean isPresent = false;
	protected String strEmailHeaderNm ="";
	protected String strFromOverrideFl = "";

	/*
	 * setUpEmail(): This method will call the two methods to initialize and validate the input parameters
	 * */
	public void setUpEmail(HashMap hmParam) throws Exception
	{
		instatiateParameters(hmParam);
		validateInput();
	}
	
	/*
	 * instatiateParameters(): This Method used to assign all the input variables from the hashmap passed from GmEmailConsumerJob class.
	 * */
	public void instatiateParameters(HashMap hmParam)  {	
	   strTo = (String[])hmParam.get("TO");
	   strCc = (String[]) hmParam.get("CC");
	   strBcc = (String[]) hmParam.get("BCC");
	   strReplyTo = (String[]) hmParam.get("STRREPLYTO");
	  
	   strFrom = (String) hmParam.get("STRFROM");
	   strFromID = strFrom;
	   /*Get Default From Mail address*/
	   //PC-2939 -[This Override flag condition is used to set default(spineitcomms@spineit.net) FROM email address] 
	   strFromOverrideFl = GmCommonClass.parseNull((String) hmParam.get("FROMOVERRIDEFL"));
	   if(!strFromOverrideFl.equals("Y")){
	   strFrom = GmEmailBean.FromId;
	   }
	   strSubject = (String) hmParam.get("STRSUBJECT");
	   strMessageDesc = (String) hmParam.get("STRMESSAGEDESC");
	   strAttachment = (String) hmParam.get("STREMAILATTACHMENT");
	   strMimeType = (String) hmParam.get("STRMIMETYPE");
	  
	   blWhitelistserver = ((Boolean)hmParam.get("WHITELISTSERVER")).booleanValue(); 
	   blTestingserver = ((Boolean)hmParam.get("TESTINGSERVER")).booleanValue(); 
	   toEmail = (String) hmParam.get("TOEMAIL");
	   strTestEmailSubject = (String) hmParam.get("TESTEMAILSUBJECT");
	   strTestEmailID = (String) hmParam.get("TESTEMAILID");
	   strMailSubject = strSubject;
	   strEmailHeaderNm = GmCommonClass.parseNull((String) hmParam.get("EMAILHEADERNM"));
		
	}
	/*
	 * validateInput(): This Method used to Validate all the Email recipients and validate the email id in the rules.
	 * */
	public void validateInput() throws Exception{
		try {
			
		       //parseInputs():Validate the To , CC, ReplyTo recipients address
			 	   strToIds = strTo;
			 	   strCcIds = strCc;
			 	   strBccIds = strBcc;
			 	   
			       strToRecipient 	  = parseInputs(strToIds);
			       log.debug("strToRecipient: " + strToRecipient);
			       isToIdPresent 	  = isPresent;
			       strRecipient   	  = parseInputs(strCcIds);
			       log.debug("strRecipient " + strRecipient);
			       isCCIdPresent  	  = isPresent;
			       bcclist 		  	  = parseInputs(strBccIds);
			       log.debug("bcclist: " + bcclist);
			       isBCCIdPresent 	  = isPresent;
			       replyToIds  	  	  = parseInputs(strReplyTo);
			       log.debug("bcclist: " + bcclist);
			       isReplyToIdPresent = isPresent;
			       
			       /*Check the Actual From Email address is available in the list,If this is available then assign the ActualFromId's to Reply to id's
			        * */
			       
		       		String strReplyToEmails =
		    	        GmCommonClass.parseNull(GmCommonClass.getString("REPLY_TO_EMAILS"));
		    	    int intEmailCheck = strReplyToEmails.indexOf("|" + strFromID + "|");
		    	    if (intEmailCheck != -1) {
		    	    	replyToIds =  InternetAddress.parse(strFromID) ;
		    	    }
		 } catch (Exception e) {
			 log.debug("Exception in validateInput" );
		      e.printStackTrace();
		      throw new Exception(e);
		      
		 }	
	}
	
	/*
	 * parseInputs():This method is used to validate the Email recipients.
     * */
	private InternetAddress[] parseInputs(String[] strInput) throws Exception
	{
		InternetAddress[] strOutput = null;
		if (strInput != null && strInput.length > 0) {
        for (int i = 0; i < strInput.length; i++) {
	       if (strInput[i] != null && !(strInput[i].trim().equals(""))) {
	    	   if (strOutput == null){
	    			strOutput = new InternetAddress[strInput.length];
	        	}
	    	   strOutput[i] = new InternetAddress(strInput[i]);
	    	   isPresent = true;
	       }
     	}
	  }  
	 return strOutput;
	}
	
	/*
	 * getEmailMessage(): This method is used to get all the email recipients from the Message object
	 * and used to send the mail through AWS/SMTP
	 * */
	public Message getEmailMessage() throws Exception{
		Session session = Session.getDefaultInstance(new Properties());
		Message msg = new MimeMessage(session);
		try{
			//This Method is used to validate the Domain
			 validateDomainaddress();
			 Properties props = new Properties();
			  props.put("mail.transport.protocol", "SMTP");
		      props.put("mail.smtp.host", strSMTPServerIP);
			  Session sessionObj = Session.getInstance(props, null);
			  msg = new MimeMessage(sessionObj);
		      msg.setFrom( new InternetAddress(strFrom));
		      /*
		       * if the Email Recipients are available then set all the recipient address,
		       * subject of the mail to the message object*/
		       
		      log.debug("msg 1 " + msg.toString());
		      if (isToIdPresent){
		        msg.setRecipients(Message.RecipientType.TO, strToRecipient);
		        log.debug("strToRecipient: " + strToRecipient);
		      }
		      log.debug("msg 3 " + msg.toString()); 
		      log.debug("msg 3 " + strRecipient);
		      if (isCCIdPresent){
		          msg.setRecipients(Message.RecipientType.CC, strRecipient);
		      }
		      log.debug("msg 4 " + strRecipient);
		      if (isBCCIdPresent){
		          msg.setRecipients(Message.RecipientType.BCC, bcclist);
		      }
		      log.debug("msg 5 " + bcclist);
		      if (isReplyToIdPresent){
		          msg.setReplyTo(replyToIds);
		      }
		      log.debug("msg 6 " + replyToIds);
			  msg.setSubject(strSubject);
			  
			  msg.setSentDate(new java.util.Date());
			  
	      	  //Check and set MimeType	
			 if (strMimeType.equals("")) {
		        strMimeType = "text/html";
		      }
		      //to fix the special char issue
		      strMimeType += "; charset=UTF-8";
		      MimeBodyPart mbp1 = new MimeBodyPart(); 
			  strEmailFooter = GmCommonClass.parseNull(GmCommonClass.getString("SERVEREMAILFOOTER"));
			  strEmailMessageFooter = GmCommonClass.parseNull(GmCommonClass.getString("EMAILMESSAGEFOOTER"));
			  strSystemNm = strSystemNm+ " " +GmEmailBean.emailServiceType;
			  /*Create Header Email*/
			  String strHeaderEmail = GmCommonClass.parseNull(GmEmailBean.emailHeader);
			  if(strHeaderEmail.equalsIgnoreCase("Y")){
				  strMessageDesc = GmCommonClass.parseNull(createHeaderEmail(strMessageDesc));
			  }
		      strMessageDesc +=  strEmailFooter.replaceAll("<systemname>", strSystemNm) +strEmailMessageFooter;
		     
		      mbp1.setContent(strMessageDesc, strMimeType);
	      	  /*
		       * Check if the file attachments are available in the Mail,
		       * if the attachments are available then add it to message object
		       */
		      MimeBodyPart mbp = new MimeBodyPart();
		      Multipart mp = new MimeMultipart();
		      if (strAttachment != null && !strAttachment.equals("")) {
	
		        String[] filters = strAttachment.split(",");
		        for (int i = 0; i < filters.length; i++) {
	
		          strAttachment = filters[i];
		          File newFile = new File(strAttachment);
		          mbp = new MimeBodyPart();
		          if (newFile.exists()) {
		            DataSource source = new FileDataSource(strAttachment);
		            mbp.setDataHandler(new DataHandler(source));
	
		            if (strAttachment.indexOf("\\") != -1) {
		              strAttachment = strAttachment.substring(strAttachment.lastIndexOf("\\") + 1);
		            }
		            mbp.setFileName(strAttachment);
		            mp.addBodyPart(mbp);
		          }
		        }
		      }
		      mp.addBodyPart(mbp1);
		      msg.setContent(mp);
		}  catch (Exception e) {
		      e.printStackTrace();
		      throw new Exception(e);
		 }
		return msg;
	}
	
	 public String createHeaderEmail(String strMessage) throws Exception {
         String strImageURL = GmCommonClass.contextURL;
		 String strEmailSubjects =
	    	        GmCommonClass.parseNull(GmCommonClass.getString("EMAIL_SUBJECTS"));
	 	  int intHeadercheck = strEmailSubjects.indexOf("|" + strSubject + "|");
		  StringBuffer strBuffer = new StringBuffer();
		  strBuffer.append("<html><head><title></title><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>");
		  strBuffer.append("<style type='text/css'>a {text-decoration: none}</style></head>");
		  strBuffer.append("<body text='#000000' link='#000000' alink='#000000' vlink='#000000'>");
		  strBuffer.append("<table cellpadding='0' cellspacing='0' style='min-width:850px;border-style: solid; border-width:1px;border-color: #00366e;'>");
		/*  strBuffer.append("<tr style='background-color:#00366e;font-family:Trebuchet MS;font-size:25px;color:#FFFFFF;'>"
		  		+ "<td style='background-color:#00366e;padding:1em;'>"
		  		+ "<img src='"+strImageURL+"globus-medical-logo.png' alt='Globus Medical' /></td>");*/
		  strBuffer.append("<tr style='background-color:#00366e;font-family:Trebuchet MS;font-size:25px;color:#FFFFFF;'>"
			  		+ "<td style='background-color:#00366e;padding:1em;'>"
			  		+ "<img src='"+strImageURL+"globus-medical-logo.png' alt='Globus Medical' /></td>");
		  strBuffer.append("<td align='right' style='background-color:#00366e;padding:1em;'>"+strEmailHeaderNm+"</td>");
		  strBuffer.append("</tr>");
		  strBuffer.append("<tr><td colspan='4'>");
		  strBuffer.append(strMessage);
		  strBuffer.append("</td></tr>");
		  strBuffer.append("</table></body></html>");
		  String strErrMessage = strBuffer.toString();
		  if (intHeadercheck != -1){
			  strErrMessage = strMessage;
		  }
		  return strErrMessage;
		  }
	 
	/*
     * validateDomainaddress(): This Method is used to validate the Domain
     */
	public void validateDomainaddress() throws Exception{
		try{
		      /*
		       * Get the Domain from the From email Address. For the Domain, check if there is any email
		       * Server Configured in Rules*
		       */
		      if (strFrom.indexOf("@") != -1) {
		        stremailDomain =
		            strFrom.substring(strFrom.indexOf("@") + 1, strFrom.length()).toUpperCase().trim();

		        strSMTPServerIP =
		            GmCommonClass.parseNull(GmCommonClass.getRuleValue(stremailDomain, "MAIL_SERVER"));
		      }
		      // If No Email Server configured for the Domain, then take the default email Server from Config File.
		      if (strSMTPServerIP.equals(""))
		        strSMTPServerIP = GmCommonClass.getString("MAIL_SERVER");
		      
		      /*InetAddress iadd = InetAddress.getLocalHost();
		      systemName = iadd.toString().toLowerCase();
		      SystName = systemName.split("/");
		      strSystemNm = SystName[0];*/
		      
		   
		   // Get property from JBOSS Server environment
		      strSystemNm = System.getProperty("HOST_NAME");
		      log.debug("HostName "+strSystemNm);
		      
		}  catch (Exception e) {
		      e.printStackTrace();
		      throw new Exception(e);
		 }
	}
	
	public abstract void sendEmail() throws Exception;
	
	/*
     * saveMailInDB(): This Method is used to save the Email details in the Email Log table
     */
	@Override
	public void saveMailInDB() throws Exception {
		 try {
		  String strMailTo = Arrays.toString(strToIds);
	      strMailTo = strMailTo.replaceAll("\\[", "").replaceAll("\\]", "");
	      String strMailCC = Arrays.toString(strCcIds);
	      strMailCC = strMailCC.replaceAll("\\[", "").replaceAll("\\]", "");

	      hmparams.put("EMAILFROM", strFrom);
	      hmparams.put("EMAILTO", strMailTo);
	      hmparams.put("EMAILCC", strMailCC);
	      hmparams.put("EMAILSUBJECT", strMailSubject);
	      hmparams.put("EMAILMESSAGE", strMessageDesc);
	     
		  GmEmailBean.saveMailDtls(hmparams);
		  
	      } catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
	}
}
