package com.globus.common.email;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.globus.common.email.GmEmailBean;
import com.globus.common.beans.GmLogger;

/**
 * @author karthik
 *
 */
public class GmEmailThroughAWS extends GmEmailManagerAbstract {
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	@Override
	/* sendEmail(): This method will be call through the GmEmailManagerAbstract class based on the AWS credential available in the server
	 * and the mail will be send through Amazon Web Services (AWS)
	 * */
	public void sendEmail() throws Exception  {
		   try {
			    HashMap hmparam = new HashMap();
				Session session = Session.getDefaultInstance(new Properties());
				Message msg = new MimeMessage(session);
				msg = getEmailMessage();
				log.debug("Inside GmEmailThroughAWS"+msg.getAllRecipients());
				 if (msg.getAllRecipients().length > 0) {
			            AmazonSimpleEmailService client = GmEmailBean.awsClient;
			            // Send the email.
			            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			            msg.writeTo(outputStream);
			            RawMessage rawMessage = 
			            		new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
	
			            SendRawEmailRequest rawEmailRequest = 
			            		new SendRawEmailRequest(rawMessage);
			            		 //   .withConfigurationSetName(CONFIGURATION_SET);
			            client.sendRawEmail(rawEmailRequest);
			            log.debug("Mail sent through AWS..");
				  }
		        } catch (Exception ex) {
		           log.debug("Error message: " + ex.getMessage());
		           ex.printStackTrace();
		           throw new Exception(ex);
		        }
	}

}
