package com.globus.common.email;

import com.globus.common.email.GmEmailManagerInterface;
import com.globus.common.email.GmEmailBean;

/**
 * @author karthik
 * class Name: GmEmailFactory.java
 * Description: This class will determine the Mail Protocol
 */

public abstract  class GmEmailFactory {
	/*
	 * getInstance() : This method will determine the Mail Protocol
	 * if the AWS credential is available in the server then the mail will be sent through AWS protocol ,if the AWS credential is
	 * not available in the server then  the mail will be sent through SMTP protocol
	 * */
	public static GmEmailManagerInterface getInstance()
	{
		if(GmEmailBean.awscredential)
		{
			return new GmEmailThroughAWS();
		}
		else
		{
			return new GmEmailThroughGlobusSMTP();
		}
	}

}
