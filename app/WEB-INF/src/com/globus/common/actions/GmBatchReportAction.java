package com.globus.common.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmBatchReportForm;

public class GmBatchReportAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code

  /**
   * loadBatchRpt
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws AppError
   */
  public ActionForward loadBatchRpt(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmBatchReportForm gmBatchReportForm = (GmBatchReportForm) actionForm;
    GmBatchBean gmBatchBean = new GmBatchBean(getGmDataStoreVO());
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();

    gmBatchReportForm.loadSessionParameters(request);

    HashMap hmParam = new HashMap();
    ArrayList alBatchStatus = new ArrayList();
    ArrayList alBatchType = new ArrayList();
    ArrayList alResult = new ArrayList();
    String strForward = "GmBatchReport";
    String strScreenType = "";
    String strApplDateFmt = "";
    String strMonthFirstDt = "";
    String strTodayDt = "";
    String strOpt = "";
    String strBatchFromDt = "";
    String strBatchToDt = "";
    String strRptHeader = "";
    String strXmlData = "";
    String strBatchStatus = "";
    int currMonth = 0;

    hmParam = GmCommonClass.getHashMapFromForm(gmBatchReportForm);

    gmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
    strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
    // from date to set 3 month previous date
    currMonth = gmCalenderOperations.getCurrentMonth() - 7;
    String strToday = GmCalenderOperations.getCurrentDate("dd");
    strMonthFirstDt =
        gmCalenderOperations.getAnyDateOfMonth(currMonth, Integer.parseInt(strToday),
            strApplDateFmt);
    strTodayDt = GmCommonClass.parseNull(gmCalenderOperations.getCurrentDate(strApplDateFmt));

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strBatchFromDt = GmCommonClass.parseNull((String) hmParam.get("BATCHFROMDT"));
    strBatchToDt = GmCommonClass.parseNull((String) hmParam.get("BATCHTODT"));
    strScreenType = GmCommonClass.parseNull((String) hmParam.get("BATCHSCREENTYPE"));
    strBatchStatus = GmCommonClass.parseNull((String) hmParam.get("BATCHSTATUS"));


    if (strBatchFromDt.equals("") && strOpt.equals("")) {
      gmBatchReportForm.setBatchFromDt(strMonthFirstDt);
      hmParam.put("BATCHFROMDT", strMonthFirstDt);
    }
    if (strBatchToDt.equals("") && strOpt.equals("")) {
      gmBatchReportForm.setBatchToDt(strTodayDt);
      hmParam.put("BATCHTODT", strTodayDt);
    }

    // default it load the Pending Processing data.
    if (strBatchStatus.equals("")) {
      gmBatchReportForm.setBatchStatus("18742"); // Pending Processing
      hmParam.put("BATCHSTATUS", "18742");
    }
    strRptHeader = GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strScreenType));
    // to set the header as Process Batch
    if (strRptHeader.equals("")) {
      strRptHeader = "Process Batch";
    }
    gmBatchReportForm.setJspHeader(strRptHeader);
    alBatchStatus = GmCommonClass.parseNullArrayList((GmCommonClass.getCodeList("BATSTA")));
    alBatchType = GmCommonClass.parseNullArrayList((GmCommonClass.getCodeList("BATTYP")));

    gmBatchReportForm.setAlBatchStatus(alBatchStatus);
    gmBatchReportForm.setAlBatchType(alBatchType);

    alResult = GmCommonClass.parseNullArrayList(gmBatchBean.fetchBatchReport(hmParam));

    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strRBPath = "properties.labels.common.GmBatchReport";
    // to set the template name and Path and Resource bundle path
    hmParam.put("TEMPNAME", "GmBatchReport.vm");
    hmParam.put("TEMPPATH", "common/templates");
    hmParam.put("COMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("RBPATH", strRBPath);

    strXmlData = gmBatchBean.getXmlGridData(alResult, hmParam);
    gmBatchReportForm.setGridData(strXmlData);

    return actionMapping.findForward(strForward);
  }
}
