package com.globus.common.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.forms.GmCorporateAjaxForm;
import com.globus.common.util.GmWSUtil;
import com.globus.corporate.beans.GmCorporateBean;

public class GmCorporateAjaxAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadCompanyList - This method is used to get company list by calling AJAX.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   */
  public ActionForward loadCompanyList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strJSON = "";
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmParam = new HashMap();
    instantiate(request, response);
    GmCorporateAjaxForm gmCorporateAjaxForm = (GmCorporateAjaxForm) actionForm;
    gmCorporateAjaxForm.loadSessionParameters(request);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmPartyBean gmPartyBean = new GmPartyBean(getGmDataStoreVO());
    String strPartyID = getGmDataStoreVO().getPartyid();
    hmParam.put("PARTYID", strPartyID);
    strJSON = gmWSUtil.parseArrayListToJson(gmPartyBean.fetchPartyCompany(hmParam));
    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return actionMapping.findForward("success");
  }

  /**
   * loadPlantList - This method is used to get company list by calling AJAX.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   * @throws IOException
   */
  public ActionForward loadPlantList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {


    String strJSON = "";
    GmWSUtil gmWSUtil = new GmWSUtil();
    HashMap hmSessParams = new HashMap();

    instantiate(request, response);
    GmCorporateAjaxForm gmCorporateAjaxForm = (GmCorporateAjaxForm) actionForm;

    hmSessParams = GmCommonClass.getCurrSymbolFormatfrmComp(getGmDataStoreVO().getCmpid());
    String strCurrSign = GmCommonClass.parseNull((String) hmSessParams.get("CMPCURRSMB"));
    String strCurrFmt = GmCommonClass.parseNull((String) hmSessParams.get("CMPCURRFMT"));

    strCurrSign = strCurrSign.equals("") ? "$" : strCurrSign;
    strCurrFmt = strCurrFmt.equals("") ? "#,###,##0.00" : strCurrFmt;
    session.setAttribute("strSessCurrSymbol", strCurrSign);
    session.setAttribute("strSessApplCurrFmt", strCurrFmt);

    // setting locale as null to show portal labels in English
    String strSessCompanyLocale = "";
    // The below Language id is used to show the Japan Master data in english on sales sales reports
    // and in dropdowns for Non Globus Medical Japan as primary company
    String strSessCompanyLangId = "103097";

    String strSwitchUserFl =
        GmCommonClass.parseNull((String) session.getAttribute("strSwitchUserFl"));
    String strPrimaryCmpyId =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
    String strSelectedCompany = getGmDataStoreVO().getCmpid();
    String strMultiLangCmpyId =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strPrimaryCmpyId, "MULTILINGUAL"));
    // For multilingual changes, getting company locale and setting it to session for the
    // multi-company users. Getting the session only when their selecting a company from the company
    // dropdown and that should be a primary company for that user.
    // e.g User Primary company is Japan(1022) and selected japan - locale will be JP(Japan)
    // User Primary company is Japan(1022) and selected other company - locale will be US(English)
    if ((strPrimaryCmpyId.equals(strMultiLangCmpyId) && strSelectedCompany
        .equals(strMultiLangCmpyId))) {
      strSessCompanyLocale =
          GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));
      strSessCompanyLangId =
          GmCommonClass.parseNull(GmCommonClass.getCompanyLanguage(getGmDataStoreVO().getCmpid()));
    }
    // If the user who's primary company is Japan,logged in as Switch user , we will show all labels
    // in English on portal
    if (strSwitchUserFl.equals("Y")) {
      strSessCompanyLocale = "";
      strSessCompanyLangId = "103097";
    }

    session.setAttribute("strSessCompanyLocale", strSessCompanyLocale);
    session.setAttribute("strSessCompanyLangId", strSessCompanyLangId);

    gmCorporateAjaxForm.loadSessionParameters(request);
    String strPartyId = gmCorporateAjaxForm.getSessPartyId();
    GmCorporateBean gmCorporateBean = new GmCorporateBean(getGmDataStoreVO());
    //PMT-34024 Party Plant Mapping
    strJSON = gmWSUtil.parseArrayListToJson(gmCorporateBean.loadPartyPlantMapping(strPartyId, strSelectedCompany)); //to load the plant list based on party mapping.

    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return actionMapping.findForward("success");
  }
}
