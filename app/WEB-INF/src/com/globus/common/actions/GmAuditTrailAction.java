package com.globus.common.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAuditTrailBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmAuditTrailForm;


public class GmAuditTrailAction extends GmAction {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      GmAuditTrailForm gmAuditTrailForm = (GmAuditTrailForm) form;

      GmAuditTrailBean gmAuditTrailBean = new GmAuditTrailBean(getGmDataStoreVO());

      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      String strRequestId = GmCommonClass.parseNull(gmAuditTrailForm.getTxnId());
      String strAuditId = GmCommonClass.parseNull(gmAuditTrailForm.getAuditId());
      String strJNDIConnection = GmCommonClass.parseNull(gmAuditTrailForm.gethJNDIConnection());
      // String strAuditId = "1006"; //for request table
      HashMap hmParam =
          GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmAuditTrailForm));
      RowSetDynaClass rdResult = null;
      log.debug(" strRequestId ***" + strRequestId);
      log.debug(" strAuditId ***" + strAuditId);

      if (!gmAuditTrailForm.getStrDynamicAuditTrailFl().equals("Y")) {
        // Existing flow
        rdResult = gmAuditTrailBean.fetchQtyHistory(strRequestId, strAuditId, strJNDIConnection);

      } else {
        // to get the audit trail from custom table
        rdResult = gmAuditTrailBean.fetchDynamicAuditTrailLog(hmParam);

      }
      gmAuditTrailForm.setLdtResult(rdResult.getRows());
      log.debug("rdResult is " + rdResult.getRows().size());

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("success");
  }
}
