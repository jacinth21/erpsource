package com.globus.common.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyShipParamsSetupBean;
import com.globus.common.forms.GmPartyShipParamsSetupForm;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmPartyShipParamsSetupAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public ActionForward setPartyShipParam(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartyShipParamsSetupForm gmPartyShipParamsSetupForm = (GmPartyShipParamsSetupForm) form;
    gmPartyShipParamsSetupForm.loadSessionParameters(request);

    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    GmPartyShipParamsSetupBean gmPartyShipParamsSetupBean =
        new GmPartyShipParamsSetupBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartyShipParamsSetupForm);

    ArrayList alShipCarrier = new ArrayList();
    ArrayList alShipmode = new ArrayList();
    ArrayList alStdShipPayee = new ArrayList();
    ArrayList alStdShipChargeType = new ArrayList();
    ArrayList alRuchShipPayee = new ArrayList();
    ArrayList alRuchShipChargeType = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    HashMap hmResult = new HashMap();
    ArrayList alAccountList = new ArrayList();

    alAccountList = gmSales.reportAccount();
    gmPartyShipParamsSetupForm.setAlAccountList(alAccountList);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strScreenType = GmCommonClass.parseNull(gmPartyShipParamsSetupForm.getScreenType());
    String strAccId = GmCommonClass.parseNull(gmPartyShipParamsSetupForm.getAccountId());
    String strPartyId = GmCommonClass.parseNull(gmPartyShipParamsSetupForm.getPartyID());
    strPartyId =
        strPartyId.equals("") ? gmPartyShipParamsSetupBean.getAccountPartyId(strAccId) : strPartyId;
    String strForward = "gmPartyShipParamsSetup";
    gmPartyShipParamsSetupForm.setAccountId(strAccId);
    request.setAttribute("hAccountID", strAccId);
    request.setAttribute("hPartyID", strPartyId);
    hmParam.put("PARTYID", strPartyId);

    String strSesPartyId = GmCommonClass.parseNull(gmPartyShipParamsSetupForm.getSessPartyId());

    HashMap hmShipSetupAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId,
            "SHIP_PARAM_SUB_ACC"));
    String strAccessFl = GmCommonClass.parseNull((String) hmShipSetupAccess.get("UPDFL"));
    request.setAttribute("hSubmitAccess", strAccessFl);
    // To Get Rule value
    String strAccLbl = GmCommonClass.parseNull(GmCommonClass.getRuleValue("LABEL", "THIRDPARTY"));
    gmPartyShipParamsSetupForm.setStrAccLbl(strAccLbl);

    if (strOpt.equals("save")) {
      gmPartyShipParamsSetupBean.savePartyShipParam(hmParam);
      gmPartyShipParamsSetupForm.setStrOpt("fetchParams");
      strOpt = "fetchParams";
    }
    if (strOpt.equals("fetchParams")) {
      alShipCarrier =
          GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DCAR", getGmDataStoreVO()));
      alShipmode =
          GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DMODE", getGmDataStoreVO()));
      alStdShipPayee = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("STDSP"));
      alStdShipChargeType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SHCHGT"));
      alRuchShipPayee = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("STDSP"));
      alRuchShipChargeType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SHCHGT"));
      gmPartyShipParamsSetupForm.setAlShipCarrier(alShipCarrier);
      gmPartyShipParamsSetupForm.setAlShipmode(alShipmode);
      request.setAttribute("alShipmode", alShipmode);
      gmPartyShipParamsSetupForm.setAlStdShipPayee(alStdShipPayee);
      gmPartyShipParamsSetupForm.setAlStdShipChargeType(alStdShipChargeType);
      gmPartyShipParamsSetupForm.setAlRuchShipPayee(alRuchShipPayee);
      gmPartyShipParamsSetupForm.setAlRuchShipChargeType(alRuchShipChargeType);
      hmResult = gmPartyShipParamsSetupBean.fetchPartyShipParam(hmParam);
      alLogReasons = gmCommonBean.getLog(strPartyId, "1300");

    }
    gmPartyShipParamsSetupForm =
        (GmPartyShipParamsSetupForm) GmCommonClass.getFormFromHashMap(gmPartyShipParamsSetupForm,
            hmResult);

    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    if (strScreenType.equals("button") || strOpt.equals("save") || strOpt.equals("edit")) {

      strForward = "gmPartyShipParamsContainer";
    }
    if (strScreenType.equals("group")) {
      strForward = "gmPartyShipParamsSetup";
    }

    gmPartyShipParamsSetupForm.setAlLogReasons(alLogReasons);
    return mapping.findForward(strForward);
  }
}
