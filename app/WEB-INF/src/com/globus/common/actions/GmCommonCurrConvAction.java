/**
 * ClassName : GmCommonCurrConvAction.java Description: Action class for currency conversion master
 * screen and report to save,edit and generate the report. Author : HPatel Version : 1 Date:
 * 01/14/2010 Copyright : Globus Medical Inc Change History : Created
 */
package com.globus.common.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonCurrConvBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmCommonCurrConvForm;


public class GmCommonCurrConvAction extends GmAction {
  String strForward = "view";

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    HttpSession session = request.getSession(false);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.

    GmCommonCurrConvBean gmCommonCurrConvBean = new GmCommonCurrConvBean(getGmDataStoreVO());
    GmCommonCurrConvForm gmCommonCurrConvForm = (GmCommonCurrConvForm) form;
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    RowSetDynaClass rdCurrConv = null;
    String strAccessFlag = "";
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alLogReasons = new ArrayList();
    gmCommonCurrConvForm.loadSessionParameters(request);
    String strCurrId = GmCommonClass.parseNull(gmCommonCurrConvForm.getCurrId());

    String strApplnDateFormat = getGmDataStoreVO().getCmpdfmt();
    SimpleDateFormat dateFormat = new SimpleDateFormat(strApplnDateFormat);

    // Added to handle the correct character set for conversion from/to drop downs
    request.setCharacterEncoding("UTF-8");
    response.setContentType("text/html; charset=UTF-8");
    response.setCharacterEncoding("UTF-8");

    String strOpt = gmCommonCurrConvForm.getStrOpt();
    strForward = strOpt;

    log.debug(" stropt is " + strOpt);
    if (strOpt.equals("save")) {
      hmParam =
          GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmCommonCurrConvForm));
      log.debug(" values inside hmParam " + hmParam);
      strCurrId = GmCommonClass.parseNull(gmCommonCurrConvBean.saveCurrencyConvDtl(hmParam));
      gmCommonCurrConvForm.setCurrId(strCurrId);
      log.debug("strCurrId:" + strCurrId);
      strForward = "view";
    } else if (strOpt.equals("edit") || strOpt.equals("new")) {
      if (strOpt.equals("edit")) {
    	  String strPartyId =  gmCommonCurrConvForm.getSessPartyId();
    	
        hmParam = GmCommonClass.parseNullHashMap(gmCommonCurrConvBean.fetchCurrConvDtl(strCurrId));
        gmCommonCurrConvForm =
            (GmCommonCurrConvForm) GmCommonClass.getFormFromHashMap(gmCommonCurrConvForm, hmParam);
      
        hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId, "CURR_CONV_EDIT_ACC"));  	// Allow Security Event to Submit Button in PMT-32391
		strAccessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		
		if(!strAccessFlag.equals("Y")){
			gmCommonCurrConvForm.setStrEditSubmitBtnAccess("true");            // Not Allow security event, Set value disable is true to Submit Button in PMT-32391
		}
        gmCommonCurrConvForm.setCurrValue(gmCommonCurrConvForm.getCurrValue());
      }
      if (strOpt.equals("new")) {
        hmParam =
            GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmCommonCurrConvForm));
        String strConvType = GmCommonClass.parseNull(gmCommonCurrConvForm.getConvType());
        String strRateFlag =
            GmCommonClass.parseNull(gmCommonCurrConvBean.fetchMonthRateFl(hmParam));

        gmCommonCurrConvForm.setMonthRateFl(strRateFlag);

        int currMonth = GmCalenderOperations.getCurrentMonth() - 1; // 0 - 11 = Jan to Dec
        Date dtCurrFromDT = new Date();
        Date dtCurrToDT = new Date();

        if (strRateFlag.equals("N")) {
          if (strConvType.equals("101104")) { // Conversion Type = Purchase
            // Set to first/last of current month
            dtCurrFromDT =
                dateFormat.parse(GmCalenderOperations.getFirstDayOfMonth(currMonth,
                    strApplnDateFormat));
            dtCurrToDT =
                dateFormat.parse(GmCalenderOperations.getLastDayOfMonth(currMonth,
                    strApplnDateFormat));
          }
          if (strConvType.equals("101101") || strConvType.equals("101102")) { // Conversion Type =
                                                                              // Average or Spot
            // Set to first/last of previous month
            dtCurrFromDT =
                dateFormat.parse(GmCalenderOperations.getFirstDayOfMonth(currMonth - 1,
                    strApplnDateFormat));
            dtCurrToDT =
                dateFormat.parse(GmCalenderOperations.getLastDayOfMonth(currMonth - 1,
                    strApplnDateFormat));
          }
        }
        if (strRateFlag.equals("Y")) {
          if (strConvType.equals("101104")) { // Conversion Type = Purchase
            // Set to first/last of next month
            dtCurrFromDT =
                dateFormat.parse(GmCalenderOperations.getFirstDayOfMonth(currMonth + 1,
                    strApplnDateFormat));
            dtCurrToDT =
                dateFormat.parse(GmCalenderOperations.getLastDayOfMonth(currMonth + 1,
                    strApplnDateFormat));
          }
          if (strConvType.equals("101101") || strConvType.equals("101102")) { // Conversion Type =
                                                                              // Average or Spot
            // Set to first/last of previous month
            dtCurrFromDT =
                dateFormat.parse(GmCalenderOperations.getFirstDayOfMonth(currMonth - 1,
                    strApplnDateFormat));
            dtCurrToDT =
                dateFormat.parse(GmCalenderOperations.getLastDayOfMonth(currMonth - 1,
                    strApplnDateFormat));
          }
        }
        // Set date in the form if other than Constant (no default date for Constant)
        if (!strConvType.equals("101103")) { // Conversion Type != Constant
          gmCommonCurrConvForm.setDtCurrFromDT(dtCurrFromDT);
          gmCommonCurrConvForm.setDtCurrToDT(dtCurrToDT);
        }
      }
      strForward = "view";
    } else if (strOpt.equals("report")) {
      gmCommonCurrConvForm.setAlCurrFrom(GmCommonClass.parseNullArrayList(GmCommonClass
          .getCodeList("CURRN")));
      gmCommonCurrConvForm.setAlCurrTo(GmCommonClass.parseNullArrayList(GmCommonClass
          .getCodeList("CURRN")));
      gmCommonCurrConvForm.setAlSource(GmCommonClass.parseNullArrayList(GmCommonClass
          .getCodeList("ERSRC")));
      strForward = "report";
      hmParam =
          GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmCommonCurrConvForm));
      rdCurrConv = gmCommonCurrConvBean.fetchAllCurrConvDtl(hmParam);
      gmCommonCurrConvForm.setAlCurrConv(rdCurrConv.getRows());
      gmCommonCurrConvForm.setStrOpt("report");
    } else if (strOpt.equals("TransRpt")) {

      strForward = "transreport";
      ArrayList alTransList = new ArrayList();
      HashMap hmCurrConvDtls = new HashMap();
      hmParam =
          GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmCommonCurrConvForm));

      strCurrId = GmCommonClass.parseNull((String) hmParam.get("CURRID"));
      // fetch the currency conversion and transaction details
      hmCurrConvDtls = gmCommonCurrConvBean.fetchCurrConvDtl(strCurrId);
      // to get the code name alt to show the currency
      String strCurrFmNm =
          GmCommonClass.getCodeAltName(GmCommonClass.parseNull((String) hmCurrConvDtls
              .get("CURRFROM")));
      String strCurrToNm =
          GmCommonClass.getCodeAltName(GmCommonClass.parseNull((String) hmCurrConvDtls
              .get("CURRTO")));

      hmCurrConvDtls.put("CURRFROMNM", strCurrFmNm);
      hmCurrConvDtls.put("CURRTONM", strCurrToNm);

      gmCommonCurrConvForm =
          (GmCommonCurrConvForm) GmCommonClass.getFormFromHashMap(gmCommonCurrConvForm,
              hmCurrConvDtls);

      gmCommonCurrConvForm.setAlTransList(GmCommonClass
          .parseNullArrayList((ArrayList) hmCurrConvDtls.get("ALTRANSLIST")));

    }else if(strOpt.equals("AvgRptLoad")){
      hmParam =
          GmCommonClass.parseNullHashMap(GmCommonClass.getHashMapFromForm(gmCommonCurrConvForm));
        HashMap hmCrossTabData = new HashMap();
        ArrayList alCurrency = new ArrayList();
        ArrayList alMonthList = new ArrayList();
        ArrayList alYearList = new ArrayList();
        ArrayList alExcludeCurr = new ArrayList();


        String strSessCompanyLocale =
            GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
        strOpt = GmCommonClass.parseNull(gmCommonCurrConvForm.getStrOpt());

        alMonthList = GmCommonClass.parseNullArrayList(gmCommonBean.getMonthList());
        alYearList = GmCommonClass.parseNullArrayList(gmCommonBean.getYearList());
        alCurrency = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CURRN"));
        int alSize = alCurrency.size();
        for (int i = 0; i < alSize; i++) {//to avoid USD Currency
          HashMap hmTemp = GmCommonClass.parseNullHashMap((HashMap) alCurrency.get(i));
          String strCodeId = GmCommonClass.parseNull((String) hmTemp.get("CODEID"));
          if (!strCodeId.equals("1")) {//1 -- USD
                alExcludeCurr.add(alCurrency.get(i));
              }

      }

        gmCommonCurrConvForm.setAlMonthList(alMonthList);
        gmCommonCurrConvForm.setAlYearList(alYearList);
        gmCommonCurrConvForm.setAlCurrency(alExcludeCurr);

        hmCrossTabData = gmCommonCurrConvBean.loadExchangeRateAvgReport(hmParam);
        gmCommonCurrConvForm.setHmCrossTabData(hmCrossTabData);
        strForward = "GmExchangeRateAvgRpt";
      } else if (strOpt.equals("reportScreenLoad")) {//To currency conversion report screen to load without data
    	  
    	// Getting the current date from GmCalenderOperation.
          GmCalenderOperations.setTimeZone(getGmDataStoreVO().getCmptzone());
          String strTodayDate = GmCalenderOperations.getCurrentDate(strApplnDateFormat);

          int currMonth = GmCalenderOperations.getCurrentMonth() - 1;
          String strFirstDayofMonth =
              GmCalenderOperations.getAnyDateOfMonth(currMonth, 1, strApplnDateFormat);

          gmCommonCurrConvForm.setDtCurrFromDT(GmCalenderOperations.getDate(strFirstDayofMonth,
        		  strApplnDateFormat));
          gmCommonCurrConvForm
              .setDtCurrToDT(GmCalenderOperations.getDate(strTodayDate, strApplnDateFormat));    	  
              strForward = "report";
      }

    

    alLogReasons = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strCurrId, "1253"));
    gmCommonCurrConvForm.setAlLogReasons(alLogReasons);
    gmCommonCurrConvForm.setAlCurrFrom(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("CURRN")));
    gmCommonCurrConvForm.setAlCurrTo(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("CURRN")));
    gmCommonCurrConvForm.setAlConvType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList(
        "CCONTP", getGmDataStoreVO())));
    gmCommonCurrConvForm.setAlSource(GmCommonClass.parseNullArrayList(GmCommonClass
        .getCodeList("ERSRC")));

    return mapping.findForward(strForward);
  }

}
