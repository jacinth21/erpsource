/**
 * FileName    : GmAdditionalChargesAction.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Mar 20, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.actions;

import java.util.ArrayList; 
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmAdditionalChargesForm;
import com.globus.operations.shipping.beans.GmShippingTransBean;
import com.globus.common.beans.GmCommonBean;

/**
 * @author sthadeshwar
 *
 */
public class GmAdditionalChargesAction extends GmDispatchAction {

Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public ActionForward loadAdditionalChargesForm(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	throws Exception 
	{
		GmAdditionalChargesForm gmAdditionalChargesForm = (GmAdditionalChargesForm)form;
		String strAddChargesId = gmAdditionalChargesForm.getAdditionalChargeId();
		log.debug("Additional Charges for : " + strAddChargesId);
		ArrayList alIncidents = GmCommonClass.getCodeList("ADDCH", strAddChargesId);

		gmAdditionalChargesForm.setAlReturn(alIncidents);
		gmAdditionalChargesForm.setHmCodeList(GmCommonClass.getAltGroupDetails(alIncidents));

		return mapping.findForward("success");
	}
	
	/**
	 * updateShipCost - This method is used to update ship cost
	 * @param hmParam
	 */
	public ActionForward updateShipCost(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws Exception{
		
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmShippingTransBean gmShippingTransBean = new GmShippingTransBean();
		GmAdditionalChargesForm gmAdditionalChargesForm = (GmAdditionalChargesForm)form;
		HashMap hmParam = new HashMap();
		String strUserId = (String)request.getSession().getAttribute("strSessUserId"); 
		String strOrderId = gmAdditionalChargesForm.getOrderId();
		String strShipCost = gmAdditionalChargesForm.getShipCost();
		String strLogReason = gmAdditionalChargesForm.getComments();
		hmParam.put("ORDERID",strOrderId);
		hmParam.put("SHIPCOST",strShipCost);
		hmParam.put("USERID",strUserId);
		//This method is used to update the shipcost
		gmShippingTransBean.updateOrderShipcost(hmParam);
		String strURL =
		        "/GmOrderItemServlet?hAction=EditPrice&hOrderId="+strOrderId;
		if (!strLogReason.equals("")) {
	          gmCommonBean.saveLog(strOrderId, strLogReason, strUserId, "2901");
	        }
		return mapping.findForward("success");
		
	}
}
