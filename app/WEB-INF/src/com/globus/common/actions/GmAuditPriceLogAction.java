package com.globus.common.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmAuditPriceLogBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmAuditPriceLogForm;


public class GmAuditPriceLogAction extends GmDispatchAction {
  public ActionForward fetchPriceHistory(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    instantiate(request, response);
    GmAuditPriceLogForm gmAuditPriceLogForm = (GmAuditPriceLogForm) form;
    GmAuditPriceLogBean gmAuditPriceLogBean = new GmAuditPriceLogBean(getGmDataStoreVO());

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                 // Class.

    String strRequestId = GmCommonClass.parseNull(gmAuditPriceLogForm.getTxnId());
    String strAuditId = GmCommonClass.parseNull(gmAuditPriceLogForm.getAuditId());

    RowSetDynaClass rdResult = null;

    HashMap hmParams = GmCommonClass.getHashMapFromForm(gmAuditPriceLogForm);
    rdResult = gmAuditPriceLogBean.fetchPriceHistory(hmParams);
    gmAuditPriceLogForm.setLdtResult(rdResult.getRows());

    return mapping.findForward("fetchPriceLog");
  }
}
