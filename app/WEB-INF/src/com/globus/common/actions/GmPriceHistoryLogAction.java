package com.globus.common.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPriceHistoryLogBean;
import com.globus.common.forms.GmAuditTrailForm;

public class GmPriceHistoryLogAction extends GmAction {

  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception, AppError {
    try {
      instantiate(request, response);
      GmAuditTrailForm gmAuditTrailForm = (GmAuditTrailForm) form;

      GmPriceHistoryLogBean gmPriceHistoryLogBean = new GmPriceHistoryLogBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
  
      String strRequestId = GmCommonClass.parseNull(gmAuditTrailForm.getTxnId());
      String strmModeCheck = GmCommonClass.parseNull(gmAuditTrailForm.getAuditId());
      String strJNDIConnection = GmCommonClass.parseNull(gmAuditTrailForm.gethJNDIConnection());
      String strInputStr = GmCommonClass.parseNull(gmAuditTrailForm.gethInputStr());
     
      String strBtnAccessFl = GmCommonClass.parseNull((String) gmAuditTrailForm.getStrButtonAccessFl());
      request.setAttribute("BTNACCESSFL", strBtnAccessFl);
      // String strAuditId = "1006"; //for request table
      if(strmModeCheck.equals("Save")) {//PC-5504 - to update the selected price
    	  gmPriceHistoryLogBean.savePartHistoryPricing(strInputStr);	  
      }
      RowSetDynaClass rdResult = null;
      rdResult = gmPriceHistoryLogBean.fetchPriceLogHistory(strRequestId, strJNDIConnection);
      gmAuditTrailForm.setLdtResult(rdResult.getRows());
      gmAuditTrailForm.setLdtResult(rdResult.getRows());
      gmAuditTrailForm.setStrButtonAccessFl(strBtnAccessFl);
      log.debug("rdResult is " + rdResult.getRows().size());
      log.debug("strBtnAccessFl is " + strBtnAccessFl);
     
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    return mapping.findForward("gmPriceHistoryLog");
  }
}
