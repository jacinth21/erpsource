package com.globus.common.actions;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.beans.GmLogger;
import com.globus.common.filters.GmAccessFilter;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAction extends Action {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  private GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();

  /**
   * @return
   */
  public GmDataStoreVO getGmDataStoreVO() {
    return this.gmDataStoreVO;
  }


  public GmAction() {
    super();
  }

  /**
   * @param request, response
   * @return String filter Method for the populating GmDataStoreVO from request parameter. user
   */
  protected void instantiate(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    gmDataStoreVO = GmFramework.getCompanyParams(request);

  }

  /**
   * actionRedirect - this method will used to append given companyInfo into URL and redirect to
   * given URL
   * 
   * @param strURL, request
   * @return ActionRedirect
   */
  protected ActionRedirect actionRedirect(String strURL, HttpServletRequest request) {

    ActionRedirect actionRedirect =
        new ActionRedirect(GmFramework.appendCompanyInfo(strURL, request));
    return actionRedirect;
  }

  public String getSalesAccessCondition(HttpServletRequest req, HttpServletResponse res) {
    // To get the Session Information
    HttpSession session = req.getSession(false);

    // Holds the where condition information
    String strQuery = new String();
    HashMap hmParam = new HashMap();
    String strIgnoreOvrdeAcs = "";

    // To get the Employee Information
    String strEmpId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    int intAccLevel =
        Integer.parseInt(GmCommonClass.parseNull((String) session.getAttribute("strSessAccLvl")));
    String strDepartMentID =
        GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));

    strIgnoreOvrdeAcs = GmCommonClass.parseNull((String) req.getAttribute("IGNORE_OVERRIDE_ACS"));

    String strPrimaryCmpyId =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
    String strBBASalesAcsFl =
        GmCommonClass.parseNull((String) session.getAttribute("strSessBBASalesAcsFl"));
  
    /*
     * Fetching the Additional company flag to view BBA sale  
     * @ Author: gpalani. PMT-20656 Jun 2018
     */
    String strAddnlCompFl =
            GmCommonClass.parseNull((String) session.getAttribute("strSessAddnlCompFl"));
    log.debug("strAddnlCompFl in GmAction"+strAddnlCompFl);

    // When ever we donot want to include th overrid access condition, this parameter will be set to
    // Y. In which case we will ignore the override access.
    if (!strIgnoreOvrdeAcs.equals("Y")) {

      String strOverrideAccessTo =
          GmCommonClass.parseNull((String) session.getAttribute("strSessOverrideAcsTo"));
      String strOverrideAccessId =
          GmCommonClass.parseNull((String) session.getAttribute("strSessOverrideAcsId"));
      String strOverrideAccessLvl =
          GmCommonClass.parseNull((String) session.getAttribute("strSessOverrideAcsLvl"));

      hmParam.put("OVERRIDE_ACS_TO", strOverrideAccessTo);
      hmParam.put("OVERRIDE_ACS_ID", strOverrideAccessId);
      hmParam.put("OVERRIDE_ACS_LVL", strOverrideAccessLvl);
      hmParam.put("BBA_SALES_ACS_FL", strBBASalesAcsFl);
      hmParam.put("ADDNL_COMP_IN_SALES", strAddnlCompFl);
    }

    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    hmParam.put("USER_ID", strEmpId);
    hmParam.put("ACCS_LVL", intAccLevel + "");
    hmParam.put("DEPT_ID", strDepartMentID);
    hmParam.put("PRIMARY_CMPY", strPrimaryCmpyId);
    hmParam.put("IGNORE_OVERRIDE_ACS", strIgnoreOvrdeAcs);

    strQuery = this.getSalesAccessCondition(hmParam);

    log.debug("strEmpId: " + strEmpId + " intAccLevel : " + intAccLevel + " strDepartMentID :"
        + strDepartMentID);

    return strQuery.toString();
  }

  public String getSalesAccessCondition(HashMap hmParam) {

    String strDepartMentID = (String) hmParam.get("DEPT_ID");
    int intAccLevel = Integer.parseInt(GmCommonClass.parseZero((String) hmParam.get("ACCS_LVL")));
    String strEmpId = (String) hmParam.get("USER_ID");
    GmCommonClass gmcc = new GmCommonClass();

    String strIgnoreOvrdeAcs = GmCommonClass.parseNull((String) hmParam.get("IGNORE_OVERRIDE_ACS"));
    String strPrimaryCmpyId = GmCommonClass.parseNull((String) hmParam.get("PRIMARY_CMPY"));
    String strBBASalesAcsFl = GmCommonClass.parseNull((String) hmParam.get("BBA_SALES_ACS_FL"));
    String strCompanyIds = "";
    String strOverrideAccessTo = gmcc.parseNull((String) hmParam.get("OVERRIDE_ACS_TO"));
    String strOverrideAccessId = gmcc.parseNull((String) hmParam.get("OVERRIDE_ACS_ID"));
    int intSessOverrideAcsLvl =
        Integer.parseInt(gmcc.parseZero((String) hmParam.get("OVERRIDE_ACS_LVL")));

    if (strOverrideAccessTo.equals("VP")) {
      // Assign VP Access
      intAccLevel = intSessOverrideAcsLvl;
      strDepartMentID = "S";
      strCompanyIds = strOverrideAccessId;
    } else if (strOverrideAccessTo.equals("AD")) {
      // AD - Assign AD Access for other than sales dept users from OUS
      // BBA - Assign override access for other than sales dept users from BBA
      intAccLevel = intSessOverrideAcsLvl;
      strDepartMentID = "S";
      strCompanyIds = strOverrideAccessId;
    }

    StringBuffer strQuery = new StringBuffer();

    // *********************************************************
    // Will Execute the below Query if the Department is Sales
    // *********************************************************
    if (strDepartMentID.equals("S")) {

      switch (intAccLevel) {

      // Sales Rep Filter Condition
        case 1: {
          strQuery.append(" T501.C703_SALES_REP_ID = " + strEmpId);
          break;
        }

        // Distributor Principal Filter Condition
        case 2: {
          strQuery.append(" EXISTS ");
          strQuery.append("( SELECT T703S.C703_SALES_REP_ID FROM T703_SALES_REP T703S");
          strQuery.append("  WHERE C701_DISTRIBUTOR_ID IN ( SELECT C701_DISTRIBUTOR_ID");
          strQuery.append("  FROM T101_USER WHERE C101_USER_ID = " + strEmpId + " )  ");
          strQuery.append("  AND T703S.C703_SALES_REP_ID=T501.C703_SALES_REP_ID )");
          log.debug("**GmAction*** Access Level 2");
          break;
        }

        // AD Filter Condition
        case 3: {
      
          strQuery.append(" EXISTS ");
          strQuery.append("( SELECT T703S.C703_SALES_REP_ID FROM T703_SALES_REP T703S ");
          strQuery.append("  WHERE C701_DISTRIBUTOR_ID IN ( SELECT C701_DISTRIBUTOR_ID");
          strQuery.append("  FROM T708_REGION_ASD_MAPPING T708 , T701_DISTRIBUTOR T701 ");
          strQuery.append("  WHERE T708.C901_REGION_ID = T701.C701_REGION ");
          strQuery.append("  AND T708.C708_DELETE_FL IS NULL ");
          strQuery.append("  AND    T708.C101_USER_ID = " + strEmpId + " )  ");
          strQuery.append("  AND T703S.C703_SALES_REP_ID=T501.C703_SALES_REP_ID )");
          log.debug("**GmAction*** Access Level 3");
          break;
        }
        // VP Filter Condition
        case 4: {
          strQuery.append(" EXISTS ");
          strQuery.append("( SELECT T703S.C703_SALES_REP_ID FROM T703_SALES_REP T703S");
          strQuery.append("  WHERE C701_DISTRIBUTOR_ID IN ( SELECT C701_DISTRIBUTOR_ID");
          strQuery.append("  FROM  T701_DISTRIBUTOR WHERE C701_REGION IN ( ");
          strQuery.append("  SELECT C901_REGION_ID FROM T708_REGION_ASD_MAPPING ");
          strQuery.append("  WHERE  C901_USER_ROLE_TYPE = 8001 ");
          strQuery.append("  AND C708_DELETE_FL IS NULL ");
          strQuery.append("  AND C101_USER_ID = " + strEmpId + " )) ");
          strQuery.append("  AND T703S.C703_SALES_REP_ID=T501.C703_SALES_REP_ID ) ");
          log.debug("**GmAction*** Access Level 4");
          break;
        }

        // other AD's like Kirk Tovey want to see other region
        case 6: {
          strQuery.append(" EXISTS ");
          strQuery.append("( SELECT T703S.C703_SALES_REP_ID FROM T703_SALES_REP T703S ");
          strQuery.append("  WHERE C701_DISTRIBUTOR_ID IN ( SELECT C701_DISTRIBUTOR_ID");
          strQuery.append("  FROM  T701_DISTRIBUTOR WHERE C701_REGION IN ( ");
          strQuery.append("  SELECT C901_REGION_ID FROM T708_REGION_ASD_MAPPING ");
          strQuery.append("  WHERE C901_USER_ROLE_TYPE = 8002 ");
          strQuery.append("  AND   C708_DELETE_FL IS NULL ");
          strQuery.append("  AND   C101_USER_ID = " + strEmpId + " )) ");
          strQuery.append("  AND   T703S.C703_SALES_REP_ID=T501.C703_SALES_REP_ID ) ");
          log.debug("**GmAction*** Access Level 6");
          break;
        }

        // ASS Filter condition
        case 7: {
          strQuery.append(" EXISTS "); 	
          strQuery.append("  ( SELECT T704AS.C704_ACCOUNT_ID FROM T704A_ACCOUNT_REP_MAPPING T704AS ");
          strQuery.append("  WHERE T704AS.C704A_VOID_FL is NULL AND T704AS.C703_SALES_REP_ID = " + strEmpId+ " ");
          strQuery.append("  AND T704AS.C704_ACCOUNT_ID=T501.C704_ACCOUNT_ID ) ");
          log.debug("**GmAction*** Access Level 7");
          break;
        }

        // other more filter users like Agmar Esser want to see other region
        case 8: {
          strQuery.append(" EXISTS "); 	
          strQuery.append("  (SELECT DISTINCT V700S.REP_ID ");
          strQuery.append("  FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
          strQuery.append("  WHERE V700S.REP_COMPID IN ( " + strCompanyIds + ") ");
          strQuery.append("  AND V700S.REP_ID=T501.C703_SALES_REP_ID )");
          log.debug("**GmAction*** Access Level 8");
          break;
        }

      }
    }

   
    return strQuery.toString();
  }

  /*
   * This function is used to get filter condition and data based on the request specified the user
   * defaults filter based on access contion and adds other filter
   */
  public String getSalesAccessCondition(HttpServletRequest req, HttpServletResponse res,
      boolean flag) {
    GmAccessFilter gmAccessFilter = new GmAccessFilter();
    // Holds the where condition information
    String strQuery = "";
    StringBuffer strCondtion = new StringBuffer();

    strQuery = getSalesAccessCondition(req, res);
    if (flag) {
      strCondtion = gmAccessFilter.getHierarchyWhereConditionT501(req, res, strQuery);
      return strCondtion.toString();
    }
    log.debug("strCondition := " + strCondtion.toString());
    return strQuery;
  }


  public String getAccessFilter(HttpServletRequest req, HttpServletResponse res) throws AppError {
    HttpSession session = req.getSession(false);
    HashMap hmParam = new HashMap();
    String strFilter = "";
    String strDeptId = "";
    String strAccessID = "";
    String strUserRoleType = "";
    int accessId = 0;
    try {
      strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));

      if ((String) session.getAttribute("strSessAccLvl") != null)
        accessId = Integer.parseInt((String) session.getAttribute("strSessAccLvl"));

      if (strDeptId.equalsIgnoreCase("ICS")) {
        if (accessId < 3) {
          strUserRoleType = "8003"; // ICS Employee
        } else {
          strUserRoleType = "8004"; // ICS VP
        }
      }
      hmParam.put("SessUserId", session.getAttribute("strSessUserId"));
      hmParam.put("SessAccLvl", accessId + "");
      hmParam.put("SessDeptId", strDeptId);
      hmParam.put("UserRoleType", strUserRoleType);
      strFilter = GmAccessFilter.getAccessFilter(hmParam);

    } catch (Exception e) {
      throw new AppError(e);
    }
    return strFilter;
  }
}
