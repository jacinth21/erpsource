package com.globus.common.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAPSageRptBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmDataDownloadRptBean;
import com.globus.common.beans.GmDataDownloadTransBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmDownloadForm;
import com.globus.operations.beans.GmVendorBean;

public class GmDownloadAction extends GmDispatchAction {

  public ActionForward loadInvoiceBatch(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    try {
      instantiate(request, response);
      GmDownloadForm gmDownloadForm = (GmDownloadForm) form;
      gmDownloadForm.loadSessionParameters(request);
      GmAPSageRptBean gmAPSageRptBean = new GmAPSageRptBean(getGmDataStoreVO());
      GmDataDownloadTransBean gmDataDownloadTransBean =
          new GmDataDownloadTransBean(getGmDataStoreVO());
      GmDataDownloadRptBean gmDataDownloadRptBean = new GmDataDownloadRptBean(getGmDataStoreVO());
      GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());
      GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

      Logger log = GmLogger.getInstance(this.getClass().getName());

      String strOpt = gmDownloadForm.getStrOpt();
      String strPartyId = gmDownloadForm.getSessPartyId();
      String accessFlag = "";

      String strBatch = gmDownloadForm.getBatchNumber();
      RowSetDynaClass rdInvoiceDetails = null;
      RowSetDynaClass rdBatchList = null;

      HashMap hmParam = new HashMap();
      HashMap hmReturn = new HashMap();
      HashMap hmAccess = new HashMap();
      HashMap hmInvoiceDownload = new HashMap();

      log.debug(" Testing. ..... ");
      hmParam = GmCommonClass.getHashMapFromForm(gmDownloadForm);

      if (strOpt.equals("save") || strOpt.equals("batchDtlsFromRpt")) {

        if (strOpt.equals("save")) {
          hmParam.put("DOWNLOADTYPE", "50740"); // invoice
          int batchID = gmDataDownloadTransBean.saveDownloadDtls(hmParam);
          hmReturn = gmDataDownloadRptBean.fetchBatchDtls(batchID);
        } else
          hmReturn = gmDataDownloadRptBean.fetchBatchDtls(Integer.parseInt(strBatch));
        rdBatchList = (RowSetDynaClass) hmReturn.get("RDBATCHLIST");
        hmInvoiceDownload = (HashMap) hmReturn.get("HMBATCHDETAIL");
        String strLocation = (String) hmReturn.get("LOCATION");
        log.debug(" hmInvoiceDownload>>>>>>>>>>>>>. ..... " + hmInvoiceDownload);
        log.debug(" hmParam>>>>>>>>>>>>>. ..... " + hmParam);
        gmDownloadForm.setHmInvoiceDownload(hmInvoiceDownload);
        gmDownloadForm.setReturnList(rdBatchList.getRows());
        gmDownloadForm.setHlocation(strLocation);

        return mapping.findForward("batchdetail");

      }
      if (strOpt.equals("batchreport") || strOpt.equals("rptreload")) {

        if (strOpt.equals("rptreload")) {
          rdBatchList = gmDataDownloadRptBean.fetchBatchReport(hmParam);
          gmDownloadForm.setReturnList(rdBatchList.getRows());
        }
        return mapping.findForward("batchreport");
      }
      if (strOpt.equals("reload")) {

        rdInvoiceDetails = gmAPSageRptBean.fetchDownloadInvoiceDtls(hmParam);

        log.debug(" rdSubResult.getRows(). ..... " + rdInvoiceDetails.getRows().size());
        gmDownloadForm.setReturnList(rdInvoiceDetails.getRows());

      }

      // code for access rights
      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1007"); // get only based on
                                                                               // the PRJSTATUSID
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      log.debug("Permissions = " + hmAccess);
      if (accessFlag.equals("Y")) {
        gmDownloadForm.setEnableDownload("true");
      }

      hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "1008"); // get only based on
                                                                               // the PRJSTATUSID
      accessFlag = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
      log.debug("enableRollback accessFlag:" + accessFlag);
      log.debug("Permissions = " + hmAccess);
      if (accessFlag.equals("Y")) {
        log.debug("enableRollback");
        gmDownloadForm.setEnableRollbackPayment("true");
      }

      // Load Vendor List drop down
      HashMap hmTemp = gmVendorBean.getVendorList(new HashMap());
      gmDownloadForm.setAlVendorList((ArrayList) hmTemp.get("VENDORLIST"));

    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }

    return mapping.findForward("success");
  }

}
