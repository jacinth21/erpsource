package com.globus.common.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmUploadForm;
import com.globus.common.util.GmTemplateUtil;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmUploadAction extends GmAction {
  private static String strComnPath = GmCommonClass.getString("GMCOMMON");

  /**
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  @Override
  public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    HttpSession session = request.getSession(false);
    String strErrorFileToDispatch = strComnPath + "/GmError.jsp";
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    GmUploadForm gmUploadForm = (GmUploadForm) actionForm;
    instantiate(request, response);
    gmUploadForm.loadSessionParameters(request);
    Logger log = GmLogger.getInstance(this.getClass().getName());// Code
    String strOpt = GmCommonClass.parseNull(gmUploadForm.getStrOpt());
    String strRefID = GmCommonClass.parseNull(gmUploadForm.getStrRefID());
    String strRefType = GmCommonClass.parseNull(gmUploadForm.getRefType());
    String strUserID = GmCommonClass.parseNull(gmUploadForm.getUserId());
    String strPartyId = GmCommonClass.parseNull(gmUploadForm.getSessPartyId());
    String strLogType = GmCommonClass.parseNull(gmUploadForm.getLogType());

    String strUploadHome = GmCommonClass.getString("COMP_PROJECT");
    String strApplnDateFmt =
        GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
    String strForward = "UploadDetail";
    String strMessage = "";
    String strFileName = "";
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alDetails = new ArrayList();
    ArrayList alLogReason = new ArrayList();

    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();
    hmParam = GmCommonClass.getHashMapFromForm(gmUploadForm);
    String strCodeName = GmCommonClass.parseNull(GmCommonClass.getCodeNameFromCodeId(strRefType));
    String strLogReason = GmCommonClass.parseNull(gmUploadForm.getTxt_LogReason());

    strUploadHome = strUploadHome + "\\" + strCodeName + "\\" + strRefID + "\\";
    log.debug("strUploadHome::" + strUploadHome + "::strCodeName::" + strCodeName);

    // When got common error from upload screen and click back to previous screen then it should go
    // to upload screen.
    String strRedirectURL = GmCommonClass.parseNull(request.getParameter("hRedirectURL"));
    String strDisplayNm = GmCommonClass.parseNull(request.getParameter("hDisplayNm"));
    request.setAttribute("hRedirectURL", strRedirectURL);
    request.setAttribute("hDisplayNm", strDisplayNm);
    log.debug("URL::" + strRedirectURL + "::" + strDisplayNm);
    String strUserGrpName =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRefType, "ACCS_USER_GRP"));
    hmAccess =
        gmAccessControlBean.getUserAccessPermissions(strPartyId, "UPLOAD_ACCESS", strUserGrpName);
    String strAccessFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    log.debug("strAccessFl::" + strAccessFl + "::" + strUserGrpName + "::hmAccess::" + hmAccess);



    if (strOpt.equals("Validate")) {
      strFileName = gmUploadForm.getFileName();
      strFileName = strFileName.substring(strFileName.lastIndexOf("\\") + 1);
      String strFlag = gmCommonUploadBean.validateFileNameSts(strRefID, strRefType, strFileName);
      hmParam.put("FILENAME", strFileName);
      hmParam.put("STRREFID", strRefID);
      hmParam.put("REFTYPE", strRefType);
      GmCommonClass.getFormFromHashMap(gmUploadForm, hmParam);
      response.setContentType("text/xml");
      PrintWriter pw = response.getWriter();
      pw.write(strFlag);
      pw.flush();
      pw.close();
      return null;
    }
    if (strOpt.equals("Upload")) {
      FormFile strFile = gmUploadForm.getFile();
      strFileName = strFile.getFileName();
      log.debug("Form File::" + strFile + "::strFileName::" + strFileName);
      if (!strFileName.equals("")) {
        log.debug("validateFile....");
        gmCommonUploadBean.validateFile(strFile);
        try {
          log.debug("uploadFile....");
          String strFileName1 = strUploadHome + "\\" + strFileName;
          gmCommonUploadBean.uploadFile(strFile, strUploadHome, strFileName1);
        } catch (Exception e) {
          throw new AppError("", "20675");
        }

        strFileName = strFileName.substring(strFileName.lastIndexOf("\\") + 1);
        gmCommonUploadBean.saveUploadUniqueInfo(strRefID, strRefType, strFileName, strUserID);
        if (!strRefID.equals("")) {
          strMessage = "File Uploaded Successfully";
        }
        gmUploadForm.setMessage(strMessage);
      }
      strOpt = "Fetch";
    }
    if (strOpt.equals("Delete")) {
      String strID = GmCommonClass.parseNull(request.getParameter("strID"));
      strID = strID + "|";
      gmCommonUploadBean.deleteUploadInfo(strID, strUserID);
      strOpt = "Fetch";
    }
    if (strOpt.equals("Download")) {
      File file = null;
      String strUploadString = "";
      FileInputStream istr = null;
      String strID = GmCommonClass.parseNull(request.getParameter("strID"));
      strFileName = gmCommonUploadBean.fetchFileName(strID);
      ServletOutputStream out = response.getOutputStream();
      response.setContentType("application/binary");
      response.setHeader("Content-disposition", "attachment;filename=" + strFileName);
      log.debug("file name......" + strUploadHome + strFileName);
      try {
        file = new File(strUploadHome + strFileName);
        istr = new FileInputStream(file);
      } catch (FileNotFoundException ffe) {
        log.debug("CATCH EXE" + ffe);
        response.sendRedirect(response.encodeRedirectUrl(strErrorFileToDispatch));
        throw new AppError("", "20676");
      }
      response.setContentLength((int) file.length());
      int curByte = -1;
      while ((curByte = istr.read()) != -1)
        out.write(curByte);
      istr.close();
      out.flush();
      out.close();
    }
    if (strOpt.equals("Save")) {
      if (!strLogReason.equals("")) {
        gmCommonBean.saveLog(strRefID, strLogReason, strUserID, strLogType);
      }
      strOpt = "Fetch";
    }
    if (strOpt.equals("Fetch") || strOpt.equals("Summary")) {// To fetch the details to the Summary
                                                             // screen and Upload screen
      log.debug("strOpt::" + strOpt);
      String strSessCompanyLocale =
  	        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      strRefType = gmCommonUploadBean.validateUploadID(strRefID, strRefType);
      alDetails = gmCommonUploadBean.fetchUploadInfo(strRefType, strRefID);
      hmParam.put("STROPT", strOpt);
      hmParam.put("APPLNDATEFMT", strApplnDateFmt);
      hmParam.put("ACCESSFL", strAccessFl);
      hmParam.put("SUBDIR", "quality/complaint/templates");
      hmParam.put("TEMPLATENAME", "GmCMPUpload.vm");
      hmParam.put("strSessCompanyLocale", strSessCompanyLocale);
      
      String strXmlData = generateOutPut(alDetails, hmParam);
      log.debug("strXmlData::" + strXmlData);
      gmUploadForm.setIntRecSize(alDetails.size());
      gmUploadForm.setStrXmlData(strXmlData);
      hmParam.put("FILENAME", strFileName);
      hmParam.put("STRREFID", strRefID);
      hmParam.put("REFTYPE", strRefType);
      hmParam.put("STRXMLDATA", strXmlData);
      hmParam.put("MESSAGE", strMessage);


    }

    if (strRefType.equals("-20311")) {
      throw new AppError("", "20677");
    }

    if (!strRefID.equals("") && !strLogType.equals("")) {
      alLogReason = GmCommonClass.parseNullArrayList(gmCommonBean.getLog(strRefID, strLogType));
      gmUploadForm.setAlLogReasons(alLogReason);
    }
    GmCommonClass.getFormFromHashMap(gmUploadForm, hmParam);
    gmUploadForm.setStrUpdAccessFl(strAccessFl);
    return actionMapping.findForward(strForward);
  }

  private String generateOutPut(ArrayList alGridData, HashMap hmParam) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    
    String strSubDir = GmCommonClass.parseNull((String) hmParam.get("SUBDIR"));
    String strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATENAME"));
    String strSessCompanyLocale =
            GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
       
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
            "properties.labels.quality.complaint.GmCMPUpload", strSessCompanyLocale));
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir(strSubDir);
    templateUtil.setTemplateName(strTemplateName);
    return templateUtil.generateOutput();
  }
}
