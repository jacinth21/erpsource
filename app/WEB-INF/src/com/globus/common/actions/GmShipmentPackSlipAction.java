/**
 * 
 */
package com.globus.common.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.beans.GmAccountingBean;
import com.globus.accounts.beans.GmICTSummaryBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.forms.GmShipmentPackSlipForm;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmDOReportBean;
import com.globus.operations.beans.GmLogisticsBean;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.shipping.beans.GmShippingReportBean;

import org.apache.log4j.Logger;

/**
 * @author tsekaran
 * 
 */
public class GmShipmentPackSlipAction extends GmDispatchAction {

  HashMap hmParam = new HashMap();
  ArrayList alList = new ArrayList();
  HashMap hmReturn = new HashMap();

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger

  public ActionForward loadOrderPackSlip(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmShipmentPackSlipForm gmShipmentPackSlipForm = (GmShipmentPackSlipForm) actionForm;

    GmShippingReportBean gmShippingReportBean = new GmShippingReportBean();
    GmCustomerBean gmCustomerBean = new GmCustomerBean();
    GmCommonClass gmCommon = new GmCommonClass();
    GmDOReportBean gmDOReportBean = new GmDOReportBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    ArrayList alList = new ArrayList();
    ArrayList alCartDetails = new ArrayList();
    String strOrderId = "";
    String strProjectType = "";
    String strType = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmShipmentPackSlipForm);

    String strGUId = GmCommonClass.parseNull((String) hmParam.get("HGUID"));
    strOrderId = gmShippingReportBean.getRefIDbyGUID(strGUId);
    log.debug(" strGUId  -----" + strGUId);

    strProjectType = "1500";
    alList = gmCommon.getCodeList("PKPRN");
    hmReturn = gmCustomerBean.loadOrderDetails(strOrderId, strProjectType);
    // PMT-38134 Packing Slip is Blank in Details of Shipment Emails
    alCartDetails = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("CARTDETAILS"));
    // In order to remove the usage code part numbers and not list them on the Pack Slip
    alCartDetails = gmDOReportBean.fetchPopulatedCartDetails(alCartDetails, strOrderId, "50180");
    hmReturn.put("CARTDETAILS", alCartDetails);

    request.setAttribute("hmReturn", hmReturn);
    request.setAttribute("alcomboList", alList);
    request.setAttribute("ProjectType", strProjectType);
    return actionMapping.findForward("GmOrderPackSlip");
  }

  public ActionForward loadLoanerPackslip(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError {

    // session = request.getSession(false);
    GmShipmentPackSlipForm gmShipmentPackSlipForm = (GmShipmentPackSlipForm) actionForm;
    GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean();
    ArrayList alConsignSetDetails = new ArrayList();
    GmShippingReportBean gmShippingReportBean = new GmShippingReportBean();
    HashMap hmJasperValues = new HashMap();

    HashMap hmCONSIGNMENT = new HashMap();

    String strCompAddress = "";
    String strCompPhone = "";
    String strCompFax = "";
    String strCountryCode = "";
    String strCompanyName = "";
    String strJasperName = "";
    String strCompanyId = "";
    String strCustServPhone = "";
    String strCompanyPhone = "";
    String strCompanyShortNm = "Globus";
    String strCompNm = "Globus Medical";
    String strSetPriority = "";
    String strLnSetPriority = "";
    String strCompleteAddress = "";

    String strConsignId = "";
    String strTransType = "";
    String strType = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmShipmentPackSlipForm);

    String strGUId = GmCommonClass.parseNull((String) hmParam.get("HGUID"));


    strConsignId = gmShippingReportBean.getRefIDbyGUID(strGUId);


    hmCONSIGNMENT = gmInHouseRptBean.getConsignDetails(strConsignId);

    request.setAttribute("hmCONSIGNMENT", hmCONSIGNMENT);

    request.setAttribute("TRANSID", strConsignId);

    strTransType = GmCommonClass.parseNull((String) hmParam.get("TXNTYPE"));

    request.setAttribute("hType", strTransType);

    alConsignSetDetails = gmInHouseRptBean.getSetConsignDetails(strConsignId, "");

    request.setAttribute("CONSIGNSETDETAILS", alConsignSetDetails);



    // The following Code is added for showing the Loaner Print Letter based on the Country Code.
    strCompAddress = GmCommonClass.parseNull(GmCommonClass.getString("GMCOMPANYADDRESS"));
    strCompPhone = GmCommonClass.parseNull(GmCommonClass.getString("GMCOMPANYPHONE"));
    strCompFax = GmCommonClass.parseNull(GmCommonClass.getString("GMCOMPANYFAX"));
    strCountryCode = GmCommonClass.parseNull(GmCommonClass.getString("COUNTRYCODE"));
    strCompanyName = GmCommonClass.parseNull(GmCommonClass.getString("GMCOMPANYNAME"));
    strJasperName =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCountryCode, "JAS_LNR_ACK"));
    strCompanyId = GmCommonClass.parseNull((String) hmCONSIGNMENT.get("COMPANYID"));
    strCustServPhone = "";
    strCompanyPhone = "";
    strCompanyShortNm = "Globus";
    strCompNm = "Globus Medical";
    strCompanyId = strCompanyId.equals("") ? "100800" : strCompanyId;

    if (!strCountryCode.equals("en")) {
      strCompleteAddress =
          strCompanyName + "/" + strCompAddress + "/Phone no:" + strCompPhone + "/FAX:"
              + strCompFax;
    } else if (strCountryCode.equals("en")) {
      strCompanyName =
          GmCommonClass.parseNull((String) GmCommonClass.getString(strCompanyId + "_COMPNAME"));
      strCompAddress =
          GmCommonClass.parseNull((String) GmCommonClass.getString(strCompanyId + "_COMPADDRESS"));
      strCompanyPhone =
          GmCommonClass.parseNull((String) GmCommonClass.getString(strCompanyId + "_PH"));
      strCompFax = GmCommonClass.parseNull((String) GmCommonClass.getString(strCompanyId + "_FAX"));
      strCompPhone =
          GmCommonClass.parseNull((String) GmCommonClass.getString(strCompanyId + "_CUST_SERVICE"));
      strCompanyShortNm =
          GmCommonClass.parseNull((String) GmCommonClass.getString(strCompanyId
              + "_COMP_SHORT_NAME"));
      strCompAddress = strCompAddress + "/" + strCompanyPhone;
      strCompleteAddress = strCompanyName + "/" + strCompAddress + "/" + strCompFax;
      strCompNm = strCompanyName;
    }
    hmJasperValues.put("COMPADDRESS", strCompAddress);
    hmJasperValues.put("COMPADDRESS", strCompAddress);
    hmJasperValues.put("COMPPHONE", strCompPhone);
    hmJasperValues.put("COMPFAX", strCompFax);
    hmJasperValues.put("COMPDETAILS", strCompleteAddress);
    hmJasperValues.put("JASNAME", strJasperName);
    hmJasperValues.put("COMPSHNM", strCompanyShortNm);
    hmJasperValues.put("COMPANYNAME", strCompNm);
    hmJasperValues.put("COMPID", strCompanyId);

    request.setAttribute("HMJASPERPARAMS", hmJasperValues);


    return actionMapping.findForward("GmLoanerPackSlip");
  }



  public ActionForward loadConsignmentPackSlip(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    HashMap hmConsignDetail = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmRefDetails = new HashMap();
    HashMap hmParam = new HashMap();
    String strPkSlpHdr = "";
    String strDistId = "";
    String strTransType = "";
    String strTxnId = "";
    ArrayList alSetLoad = new ArrayList();
    GmShippingReportBean gmShippingReportBean = new GmShippingReportBean();
    GmCustomerBean gmCustomerBean = new GmCustomerBean();
    GmICTSummaryBean gmICTSummaryBean = new GmICTSummaryBean();
    GmShipmentPackSlipForm gmShipmentPackSlipForm = (GmShipmentPackSlipForm) actionForm;
    GmDOReportBean gmDOReportBean = new GmDOReportBean(getGmDataStoreVO());
    hmParam = GmCommonClass.getHashMapFromForm(gmShipmentPackSlipForm);

    String strGUId = GmCommonClass.parseNull((String) hmParam.get("HGUID"));

    strTxnId = gmShippingReportBean.getRefIDbyGUID(strGUId);
    HashMap hmConParam = new HashMap();
    hmConParam.put("CONSIGNID", strTxnId);
    hmConParam.put("USERID", "30301");
    hmConParam.put("SKIPPLANTCHECK", "Y");

    hmRefDetails = gmICTSummaryBean.fetchConsignmentDetails(hmConParam);
    hmResult = GmCommonClass.parseNullHashMap((HashMap) hmRefDetails.get("HMCONSIGNDETAILS"));
    strDistId = GmCommonClass.parseNull((String) hmResult.get("DISTID"));
    strTransType = GmCommonClass.parseNull((String) hmResult.get("TYPE"));
    hmReturn = GmCommonClass.parseNullHashMap((HashMap) hmRefDetails.get("HMCONSIGNCHILDDETAILS"));
    // PMT-38134 Packing Slip is Blank in Details of Shipment Emails
    alSetLoad = GmCommonClass.parseNullArrayList((ArrayList) hmReturn.get("SETLOAD"));
    // In order to remove the usage code part numbers and not list them on the Pack Slip
    alSetLoad = gmDOReportBean.fetchPopulatedCartDetails(alSetLoad, strTxnId, "50181");
    hmReturn.put("SETLOAD", alSetLoad);

    hmConsignDetail = gmCustomerBean.loadConsignDetails(strTxnId, "30301");
    hmReturn.put("SETDETAILS", hmConsignDetail);

    strPkSlpHdr = GmCommonClass.getRuleValue(strDistId, "PACKSLIP/SHIPLIST");

    if (strTransType.equals("9110")) {
      strPkSlpHdr = strTransType;
    }

    request.setAttribute("PackSlipHeader", strPkSlpHdr);
    request.setAttribute("hmReturn", hmReturn);

    return actionMapping.findForward("GmConsignmentPackSlip");
  }

  public ActionForward loadLoanerExtPackSlip(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError {

    GmShipmentPackSlipForm gmShipmentPackSlipForm = (GmShipmentPackSlipForm) actionForm;
    GmLogisticsBean gmLogis = new GmLogisticsBean();
    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    String strConsignId = "";

    GmShippingReportBean gmShippingReportBean = new GmShippingReportBean();

    hmParam = GmCommonClass.getHashMapFromForm(gmShipmentPackSlipForm);

    String strGUId = GmCommonClass.parseNull((String) hmParam.get("HGUID"));


    strConsignId = gmShippingReportBean.getRefIDbyGUID(strGUId);

    hmParam.put("CONSIGNID", strConsignId);

    hmResult = gmLogis.viewInHouseTransItemsDetails(hmParam);

    hmReturn.put("SETLOAD", hmResult.get("SETLOAD"));
    hmResult = gmLogis.viewInHouseTransDetails(strConsignId);

    hmReturn.put("CONDETAILS", hmResult);
    request.setAttribute("hmReturn", hmReturn);
    request.setAttribute("hAction", "PicSlip");
    request.setAttribute("source", "50183"); // Loaner Extn

    return actionMapping.findForward("GmLoanerExtPackSlip");
  }


}
