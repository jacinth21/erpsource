/**
 * 
 */
package com.globus.common.actions;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAutoCompletePartyListRptBean;
import com.globus.common.beans.GmAutoCompleteReportBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmAutoCompleteForm;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.prodmgmnt.actions.GmSetMapAutoCompleteAction;
import com.globus.prodmgmnt.beans.GmSetMapAutoCompleteRptBean;
import com.globus.quality.beans.GmSetBundleMappingBean;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.operations.beans.GmVendorPortalMapBean;
import com.globus.common.db.GmCacheManager;
import com.globus.operations.beans.GmLotExpiryReportBean;
/**
 * @author vprasath
 * 
 */
public class GmAutoCompleteAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code

  /**
   * loadFieldSalesList - Fetch the field sales information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadFieldSalesList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
    String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DIST_LIST", "Y");
    String strResults =
        gmAutoCompleteReportBean.loadFieldSalesList(strKey, strSearchKey,
            Integer.parseInt(strMaxCount));
    response.getWriter().write(strResults);
    return null;
  }

  /**
   * loadSalesSetup - Fetch the salesrep setup information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadSalesRepList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
    hmParam.put("SEARCHTYPE", strSearchType);
    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
    String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("REP_LIST", "Y");
    String strResults =
        gmAutoCompleteReportBean.loadSalesRepList(strKey, strSearchKey,
            Integer.parseInt(strMaxCount), hmParam);
    response.getWriter().write(strResults);
    return null;
  }


  /**
   * loadSetList - Fetch the loadSetList setup information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadSetNameList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    String strKey = "";
    String strParameter = GmCommonClass.parseNull(request.getParameter("strParameter"));
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    Map hmMap = new HashMap();
    GmWSUtil gmWSUtil = new GmWSUtil();
    hmMap = gmWSUtil.parseJsonToMap(strParameter);
    strParameter = GmCommonClass.parseNull((String) hmMap.get("STROPT"));
    String strType = strParameter.equals("SET") || strParameter.equals("SETMAP") ? "1601" : "1602";
    // 1601 : To get the Set Mapping List values
    // 1602 : To get the BOM Mapping List values
    if (strType.equals("1601")) {
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SET_LST", "Y");
    } else {
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("BOM_LST", "Y");
    }
    request.setAttribute("STRTYPE", strType);
    request.setAttribute("STRKEY", strKey);
    GmSetMapAutoCompleteAction gmSetMapAutoCompleteAction = new GmSetMapAutoCompleteAction();
    gmSetMapAutoCompleteAction.loadSetNameList(actionMapping, actionForm, request, response);
    return null;
  }

  /**
   * loadRepAccountList - Fetch the Rep Account information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadRepAccountList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
    String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("REP_ACCOUNT_LIST", "Y");
    String strResults =
        gmAutoCompleteReportBean
            .loadRepAccList(strKey, strSearchKey, Integer.parseInt(strMaxCount));
    response.getWriter().write(strResults);
    return null;
  }

  /**
   * loadPartyNameList - Fetch the Party Name List from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadPartyNameList(ActionMapping actionMapping, ActionForm actionForm, // loadParentAccList
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strPartyType = GmCommonClass.parseNull(request.getParameter("party_type"));
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    String strParameter = GmCommonClass.parseNull(request.getParameter("strParameter"));
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
    String strKey = "";
    String strAccount ="";
    StringTokenizer stToken = new StringTokenizer(strParameter, "|");
    while (stToken.hasMoreElements()) {
    	strAccount = stToken.nextToken();
    }
    if(strAccount.equals("107462")){
    	strPartyType="4000877";
    }else if(strAccount.equals("107460")){
    	strPartyType="7011";
    }else if(strAccount.equals("107461")){
    	strPartyType="7003";
    }
    
    if (strPartyType.equals("4000877")) // 4000877 - Parent Account Account
    {
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("PARENT_ACCOUNT_LIST", "Y");
    } else if (strPartyType.equals("26230725")) { // 26230725 - Dealer
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DEALER_LIST", "Y");
    } else if (strPartyType.equals("7003")) { // 7003 - Group Price Book
      log.debug("Group price book list mm");
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("GROUP_PRICE_BOOK_LIST", "Y");
    } else if (strPartyType.equals("7010")) { // 7010 - IDN
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("IDN_LIST", "Y");
    } else if (strPartyType.equals("7011")) { // 7011 - GPO
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("GPO_LIST", "Y");
    }else if (strPartyType.equals("26241117")) { // 26241117 - RPC  //PC-2039 Add new fields on the Pricing Parameter tab in Account Setup screen
        strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("RPC_LIST", "Y");
      }else if (strPartyType.equals("26241118")) { // 26241118 - Heatlh System
          strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("HLTH_SYSTEM_LIST", "Y");
      }
    String strResults =
        gmAutoCompleteReportBean.loadPartyList(strKey, strSearchKey, Integer.parseInt(strMaxCount),
            strPartyType);
    response.getWriter().write(strResults);
    return null;
  }

  /**
   * loadProjectNameList - Fetch the Project List information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadProjectNameList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    String strKey = "";
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strProjId = GmCommonClass.parseNull(request.getParameter("searchBy"));  
    if (!strProjId.equals("0") && !strProjId.equals(""))
    {
    	  strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("PROJ_ID_LST", "Y");
    }
    else
    {
    	 strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("PROJ_LST", "Y");
    }
    request.setAttribute("STRKEY", strKey);
    request.setAttribute("PROJID", strProjId);
    GmSetMapAutoCompleteAction gmSetMapAutoCompleteAction = new GmSetMapAutoCompleteAction();
    gmSetMapAutoCompleteAction.loadProjectNameList(actionMapping, actionForm, request, response);
    return null;
  }

  /**
   * loadInspectionSheetList - Fetch the Inspection sheet list information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadInspectionSheetList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    String strKey = "";
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("INSP_SHEET_LIST", "Y");
    request.setAttribute("STRKEY", strKey);
    GmSetMapAutoCompleteAction gmSetMapAutoCompleteAction = new GmSetMapAutoCompleteAction();
    gmSetMapAutoCompleteAction
        .loadInspectionSheetList(actionMapping, actionForm, request, response);
    return null;
  }

  /**
   * fetchEmployeeWithOrders - Fetch the Active and Inactive Employee who have raised orders.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward fetchEmployeeWithOrders(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    GmAutoCompletePartyListRptBean gmAutoCompletePartyListRptBean =
        new GmAutoCompletePartyListRptBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
    hmParam.put("SEARCHTYPE", strSearchType);
    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
    String strKey = gmAutoCompletePartyListRptBean.getAutoCompleteKeys("ORDER_EMP_LIST", "Y");
    String strResults =
        gmAutoCompletePartyListRptBean.fetchEmployeeWithOrders(strKey, strSearchKey,
            Integer.parseInt(strMaxCount), hmParam);

    response.getWriter().write(strResults);
    return null;

  }

  /**
   * loadAccountList - Fetch the Account information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadAccountList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
    String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
    hmParam.put("SEARCHTYPE", strSearchType);
    String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("REP_ACCOUNT_LIST_BY_PLANT", "Y");
    String strResults =
        gmAutoCompleteReportBean.loadAccList(strKey, strSearchKey, Integer.parseInt(strMaxCount),
            hmParam);
    response.getWriter().write(strResults);
    return null;
  }


  /**
   * loadDemandSheetList - Fetch the demand sheet information from cache/DB.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadDemandSheetList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean(getGmDataStoreVO());
    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
    String strGopLoadFl = GmCommonClass.parseNull(request.getParameter("GOPLOAD"));
    String strGopLoadGrowthFl = GmCommonClass.parseNull(request.getParameter("GOPLOADGRW"));

    String strKey = "";
    HashMap hmDemandParam = new HashMap();
    if (strGopLoadFl.equals("Y") && strGopLoadGrowthFl.equals("Y")) {
      hmDemandParam.put("EXCLUDE_TEMPLATE", "YES");
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("OP_DEMAND_SHEET_SETUP_ALL", "Y");
    } else {
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DEMAND_SHEET_SETUP_ALL", "Y");
    }
    hmDemandParam.put("SEARCHTYPE", GmCommonClass.parseNull(request.getParameter("searchType")));
    hmDemandParam.put("KEY", strKey);
    hmDemandParam.put("SEARCH_KEY", strSearchKey);
    hmDemandParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
    hmDemandParam.put("FILTER_INACTIVE", "N");
    hmDemandParam.put("GOPLOADFL", strGopLoadFl);
    hmDemandParam.put("GOPLOADGRW", strGopLoadGrowthFl);
    String strResults = gmAutoCompleteReportBean.loadDemandSheetList(hmDemandParam);
    response.getWriter().write(strResults);
    return null;
  }

  public ActionForward loadSurgeonName(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
    String strKey = "";
    if (strOpt.equals("npilist")) {
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("NPI_LIST", "Y");
    } else {
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SURGEON_LIST", "Y");
    }
    hmParam.put("KEY", strKey);
    hmParam.put("SEARCH_KEY", strSearchKey);
    hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
    hmParam.put("STROPT", strOpt);
    String strResults = gmAutoCompleteReportBean.loadSurgeonName(hmParam);
    log.debug("strResults>>>>>>>>>>>>>>" + strResults);
    response.getWriter().write(strResults);
    return null;
  }

  /**
   * loadTTPList - Fetch the TTP information from cache/DB.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadTTPList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    String strOpt = GmCommonClass.parseNull(request.getParameter("STROPT"));
    log.debug("strOpt=" + strOpt);
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String term = GmCommonClass.parseNull(request.getParameter("term"));
    strSearchKey = strSearchKey.equals("") ? term : strSearchKey;
    String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
    hmParam.put("SEARCHTYPE", strSearchType);
    String strKey = "";
    if (strOpt.equals("GOP")) {
      strKey = "GOP_TTP_LIST";
    } else {
      strKey = "TTP_LIST";
    }
    hmParam.put("KEY", strKey);
    hmParam.put("SEARCH_KEY", strSearchKey);
    hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
    hmParam.put("STROPT", strOpt);
    String strResults = gmAutoCompleteReportBean.loadTTPList(hmParam);
    response.getWriter().write(strResults);
    return null;
  }


  /**
   * loadARAccountList - to load all the AR Account details (Company Customer, ICA, ICA, ICT)
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadARAccountList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
    String strParameter = GmCommonClass.parseNull(request.getParameter("strParameter"));
    String strAccountSource = "";
    // to get the Account source values
    StringTokenizer stToken = new StringTokenizer(strParameter, "|");
    while (stToken.hasMoreElements()) {
      strAccountSource = stToken.nextToken();
    }

    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());

    String term = GmCommonClass.parseNull(request.getParameter("term"));
    strSearchKey = strSearchKey.equals("") ? term : strSearchKey;
    String strKey = "";

    hmParam.put("KEY", strKey);
    hmParam.put("SEARCH_KEY", strSearchKey);
    hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
    hmParam.put("SOURCETYPE", strAccountSource);

    String strResults = gmAutoCompleteReportBean.loadARAccountList(hmParam);
    response.getWriter().write(strResults);
    return null;
  }
  
  
  /**
   * loadDemandMasterSheetList - Fetch the demand sheet information from cache/DB.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  //To load the world wide Consign,In-House, Sales Sets in DS PAR setup part screen.
  public ActionForward loadDemandMasterSheetList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

	  instantiate(request, response);
	    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
	    GmAutoCompleteReportBean gmAutoCompleteReportBean =
	            new GmAutoCompleteReportBean(getGmDataStoreVO());;
	    HashMap hmParam = new HashMap();
	    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
	    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
	    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
	    String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
	    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
	    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
	    String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DEMAND_SHEET_PAR_SETUP", "Y");
	    hmParam.put("KEY", strKey);
	    hmParam.put("SEARCH_KEY", strSearchKey);
	    hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
	    String strResults =
	    		gmAutoCompleteReportBean.fetchWorldWideSheet(hmParam);
	    response.getWriter().write(strResults);
	    return null;
  }
  
  
  /**
   * loadVendorNameList - Fetch the Project List information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadVendorNameList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
	  

		    instantiate(request, response);
		    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
		    HashMap hmParam = new HashMap();
		    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
		    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
		    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
		    String strTerm = GmCommonClass.parseNull((String)request.getParameter("term"));
		    GmAutoCompleteReportBean gmAutoCompleteReportBean =
		        new GmAutoCompleteReportBean(getGmDataStoreVO());
		    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
		    String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("VENDOR_LIST", "Y");
		    String strResults =
		        gmAutoCompleteReportBean.loadVendList(strKey, strSearchKey,
		            Integer.parseInt(strMaxCount));
		    response.getWriter().write(strResults);
		    return null;
		  }
  
  /**
   * loadSetIdList - Fetch the loadSetList setup information from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadSetIdNameList(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    String strKey = "";
    String strType = "";
    String strParameter = GmCommonClass.parseNull(request.getParameter("strParameter"));
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    Map hmMap = new HashMap();
    GmWSUtil gmWSUtil = new GmWSUtil();
    hmMap = gmWSUtil.parseJsonToMap(strParameter);
    strParameter = GmCommonClass.parseNull((String) hmMap.get("STROPT"));
    strType =  "1601";
    strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SET_ID_LST", "Y");
    request.setAttribute("STRTYPE", strType);
    request.setAttribute("STRKEY", strKey);
    GmSetMapAutoCompleteAction gmSetMapAutoCompleteAction = new GmSetMapAutoCompleteAction();
    gmSetMapAutoCompleteAction.loadSetIdNameList(actionMapping, actionForm, request, response);
    return null;
  }
  

  /**
   * loadAccountNetByLot - Fetch the account list from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ActionForward loadAccountByType(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

	  instantiate(request, response);
	    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
	    GmAutoCompleteReportBean gmAutoCompleteReportBean =
	            new GmAutoCompleteReportBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
	    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
	    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
	    String strLocationType = GmCommonClass.parseNull(request.getParameter("strParameter"));
	    
	    String strAccountSource = "";
	    StringTokenizer stToken = new StringTokenizer(strLocationType, "|");
	    while (stToken.hasMoreElements()) {
	      strAccountSource = stToken.nextToken();
	    }
	    
	    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
	    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
	    String strKey = "";
	    if(strAccountSource.equals("26230710")){
	    	  strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DEALER_ACCOUNT", "Y");
	    }else{
	    	  strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DIRECT_ACCOUNT", "Y");
	    }
	    hmParam.put("KEY", strKey);
	    hmParam.put("SEARCH_KEY", strSearchKey);
	    hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
	    hmParam.put("ACCOUNT_TYPE", strAccountSource);
	    String strResults =
	    		gmAutoCompleteReportBean.fetchAccountByType(hmParam);
	    response.getWriter().write(strResults);
	    return null;
  }
  /**
   * loadDemandGroupList - Fetch the Group list from Redis server.
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return
   * @throws AppError
   * @throws Exception
   */
	public ActionForward loadDemandGroupList(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
		GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(
				getGmDataStoreVO());
		;
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
		String strType = "";
		String strSearchKey = GmCommonClass.parseNull((String) hmParam
				.get("STRSEARCHKEY"));
		String strMaxCount = GmCommonClass.parseZero((String) hmParam
				.get("STRMAXCOUNT"));
		String strSearchType = GmCommonClass.parseNull(request
				.getParameter("searchType"));
		strType = GmCommonClass.parseNull((String) request
				.getAttribute("STRTYPE"));
		String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
		strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
		String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys(
				"DEMAND_GROUP_LIST", "Y");
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);

		String strResults = gmAutoCompleteReportBean
				.fetchDemandGroupList(hmParam);

		response.getWriter().write(strResults);
		return null;
	}

	/**
	 * loadSystemList - Fetch the System list from Redis server.
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadSystemList(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
		GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(
				getGmDataStoreVO());
		;
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
		
		String strSearchKey = GmCommonClass.parseNull((String) hmParam
				.get("STRSEARCHKEY"));
		String strMaxCount = GmCommonClass.parseZero((String) hmParam
				.get("STRMAXCOUNT"));
		String strSearchType = GmCommonClass.parseNull(request
				.getParameter("searchType"));
		String strType = "GROUPPARTMAPPING";
		String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
		strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
		String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys(
				"SYSTEM_LIST", "Y");
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		// to set the type and pass the Group part mapping
		hmParam.put("TYPE", "1600");
		hmParam.put("GROUPPART", strType);
		String strResults = gmAutoCompleteReportBean.fetchSystemList(hmParam);
        response.getWriter().write(strResults);
		return null;
	}
	
	/**
	 * loadSetBundleNameList - Fetch the Set bundle name list from Redis server.
	 * 
	 * @param actionMapping, @param actionForm, @param request, @param response, @return
	 * @throws AppError, @throws Exception
	 */
	public ActionForward loadSetBundleNameList(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
		GmSetBundleMappingBean gmSetBundle = new GmSetBundleMappingBean(getGmDataStoreVO());
		GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
		String strType = "";
		String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
		String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
		String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
		strType = GmCommonClass.parseNull((String) request.getAttribute("STRTYPE"));
		String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
		strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
		String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SET_BUNDLE_LIST", "Y");
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);

		String strResults = gmSetBundle.fetchSetBundleNameList(hmParam);

		response.getWriter().write(strResults);
		return null;
	} 
	/**
	 * loadSetBundleNmList - Fetch the Set bundle name list from Redis server.
	 * 
	 * @param actionMapping, @param actionForm, @param request, @param response, @return
	 * @throws AppError, @throws Exception
	 */
	public ActionForward loadSetBundleNmList(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
		GmSetBundleMappingBean gmSetBundle = new GmSetBundleMappingBean(getGmDataStoreVO());
		GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
		String strType = "";
		String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
		String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
		String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
		strType = GmCommonClass.parseNull((String) request.getAttribute("STRTYPE"));
		String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
		strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
		String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SET_BUNDLE_NAME_LIST", "Y");
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);

		String strResults = gmSetBundle.fetchSetBundleNmList(hmParam);

		response.getWriter().write(strResults);
		return null;
	} 
	public ActionForward loadSetNameListKit(ActionMapping actionMapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
		GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
		GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
		String strType = "";
		String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
		String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
		String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
		strType = GmCommonClass.parseNull((String) request.getAttribute("STRTYPE"));
		String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
		strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
		String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("KIT_MAPPING_SET", "Y");
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);

		String strResults = gmKitMappingBean.fetchSetNmList(hmParam);

		response.getWriter().write(strResults);
		return null;
		   // gmSetMapAutoCompleteAction.loadSetNameList(actionMapping, actionForm, request, response);
		
		  }
	public ActionForward loadKitNameList(ActionMapping actionMapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		HttpSession session = request.getSession(false);
		String strPrimaryCmpyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
		GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
		GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
		GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
		String strType = "";
		String strKey = "";
		String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
		String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
		String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
		strType = GmCommonClass.parseNull((String) request.getAttribute("STRTYPE"));
		String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
		String strActiveFl = GmCommonClass.parseNull((String) request.getParameter("activeFl"));
		strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
		if(strActiveFl.equals("Y")){
			strKey = gmKitMappingBean.getAutoCompleteKeys("KIT_NAME_ACTIVE_LIST_"+strPrimaryCmpyId, "Y",strPrimaryCmpyId);
		}else{
			 strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("KIT_NAME_LIST", "Y");
		}
		hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);
		hmParam.put("ACTIVEFL", strActiveFl);
		hmParam.put("COMPANYID", strPrimaryCmpyId);
		String strResults = gmKitMappingBean.fetchKitNmList(hmParam);

		response.getWriter().write(strResults);
		return null;
		   // gmSetMapAutoCompleteAction.loadSetNameList(actionMapping, actionForm, request, response);
		
		  }
		  
	/**
	 * PMT-35870 - Mapping Vendor Portal User to Vendor
	 * 
	 * loadVendorList - Fetch the Vendor list from Redis server.
	 * @param actionMapping, actionForm, request, response
	 * @return
	 * @throws AppError, Exception
	 */
	public ActionForward loadVendorList(ActionMapping actionMapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

				instantiate(request, response);
				HttpSession session = request.getSession(false);
				String strPrimaryCmpyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
				GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
				GmVendorPortalMapBean gmVendorMappingBean = new GmVendorPortalMapBean(getGmDataStoreVO());
				GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
				HashMap hmParam = new HashMap();
				hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
				String strType = "";
				String strKey = "";
				String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
				String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
				String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
				strType = GmCommonClass.parseNull((String) request.getAttribute("STRTYPE"));
				String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
				String strActiveFl = GmCommonClass.parseNull((String) request.getParameter("activeFl"));
				strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
					strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("VENDOR_USER_LIST", "Y");  
				hmParam.put("KEY", strKey);
				hmParam.put("SEARCH_KEY", strSearchKey);
				hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
				hmParam.put("TYPE", strType);
				hmParam.put("ACTIVEFL", strActiveFl);
				hmParam.put("COMPANYID", strPrimaryCmpyId);
				String strResults = gmVendorMappingBean.fchNameList(hmParam);
				log.debug("strResults"+strResults);
				response.getWriter().write(strResults);
				return null;
		  }
	
	/**
	 * loadLoanerSetList - Fetch the Loaner Set list from Redis server for PMT-37080.
	 * 
	 * @param actionMapping, @param actionForm, @param request, @param response, @return
	 * @throws AppError, @throws Exception
	 */
	public ActionForward loadLoanerSetList(ActionMapping actionMapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception{
		
		instantiate(request, response);
	    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
	    GmAutoCompleteReportBean gmAutoCompleteReportBean =
		        new GmAutoCompleteReportBean(getGmDataStoreVO());
	    GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean = new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());
	    HashMap hmParam = new HashMap();
	    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
		String strPlantId = GmCommonClass.parseNull(getGmDataStoreVO().getPlantid());
	    String strType = "";
	    String strKey = "";
	    String strSearchKey = "";
	    String strMaxCount = "";
	    String strTerm = "";
	    String strSearchType = "";
	    
	    strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
	    strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
	    strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
	    strType = GmCommonClass.parseNull((String) request.getAttribute("STRTYPE"));
		strTerm = GmCommonClass.parseNull(request.getParameter("term"));
		
		strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
	    strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("PRODUCT_LOANER_SET_"+strPlantId, "");
	    
	    hmParam.put("KEY", strKey);
		hmParam.put("SEARCH_KEY", strSearchKey);
		hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		hmParam.put("TYPE", strType);
		String strResults =
				gmSetMapAutoCompleteRptBean.loadLoanerSetList(hmParam);
	    response.getWriter().write(strResults);	
		return null;
		
	}
	/**
	 * PMT-41621 set list redis
	 * 
	 * loadSetMapList - Fetch the SetMap list from Redis server.
	 * @param actionMapping, actionForm, request, response
	 * @return
	 * @throws AppError, Exception
	 */
	
	public ActionForward loadSetMapList(ActionMapping actionMapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
		
		 log.debug("inside the redis loadSetMapList Autocomplete Action***---->> ");
               instantiate(request, response);
               HttpSession session = request.getSession(false);
               String strPrimaryCmpyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
               
               GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
               GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
               
               GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
               HashMap hmParam = new HashMap();
               hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
               String strType = "";
               String strKey = "";
               String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
               log.debug("strSearchKey ***---->> "+strSearchKey);
               String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
               log.debug("strMaxCount ***---->> "+strMaxCount);
               String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
               strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
               strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("CONSIGN_SET_LIST", "Y"); 
               log.debug("strKey ***---->> "+strKey);
               hmParam.put("KEY", strKey);
               hmParam.put("SEARCH_KEY", strSearchKey);
               hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
               log.debug("strPrimaryCmpyId-------     >>   "+strPrimaryCmpyId);
               hmParam.put("COMPANYID", strPrimaryCmpyId);
               String strResults = gmAutoCompleteReportBean.loadSetmapList(hmParam);
               gmCacheManager.removePatternKeys("CONSIGN_SET_LIST");   
              
               response.getWriter().write(strResults);
               
               return null;
        }
	/**  PMT-33405: Lot Expiry Dashboard
	   * loadAllCompanyProjectNameList - Fetch the Project List information from Redis server.
	   * 
	   * @param actionMapping
	   * @param actionForm
	   * @param request
	   * @param response
	   * @return
	   * @throws AppError
	   * @throws Exception
	   */
	  public ActionForward loadAllCompanyProjectNameList(ActionMapping actionMapping, ActionForm actionForm,
	      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

			instantiate(request, response);
			HttpSession session = request.getSession(false);
			String strPrimaryCmpyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
			GmLotExpiryReportBean gmLotExpiryReportBean = new GmLotExpiryReportBean(getGmDataStoreVO());
			GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
			GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
			
			HashMap hmParam = new HashMap();
			hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
			String strType = "";
			String strKey = "";
			String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
			String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
			String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
			strType = GmCommonClass.parseNull((String) request.getAttribute("STRTYPE"));
			String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
			strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
			strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("PROJECT_LIST", "Y");  
			
			hmParam.put("KEY", strKey);
			hmParam.put("SEARCH_KEY", strSearchKey);
			hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
			hmParam.put("TYPE", strType);
			hmParam.put("COMPANYID", strPrimaryCmpyId);
			String strResults = gmLotExpiryReportBean.loadProjectNameList(hmParam,strSearchKey);

			response.getWriter().write(strResults);
			return null;
	  }
	  /**  PMT-53468: Track Surgical Activity by Surgeon Name in OUS countries
	   * loadSurgeonNameWId - Fetch the NPI id and Surgeon name information from Redis server.
	   * 
	   * @param actionMapping
	   * @param actionForm
	   * @param request
	   * @param response
	   * @return
	   * @throws AppError
	   * @throws Exception
	   */
	  public ActionForward loadSurgeonNameWId(ActionMapping actionMapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

		    instantiate(request, response);
		    GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
		    HashMap hmParam = new HashMap();
		    hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
		    String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
		    String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
		    String strOpt = GmCommonClass.parseNull(request.getParameter("strOpt"));
		    GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
		    
		    String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
		    strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
		    String strKey = "";
		    if (strOpt.equals("npilist")) {
		        strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("NPI_PARTY_LIST", "Y");
		    } else {
		        strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SURGEON_PARTY_LIST", "Y");
		    }
		    
		    hmParam.put("KEY", strKey);
		    hmParam.put("SEARCH_KEY", strSearchKey);
		    hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
		    hmParam.put("STROPT", strOpt);
		    String strResults = gmAutoCompleteReportBean.loadSurgeonNameWId(hmParam);
		    
		    response.getWriter().write(strResults);
		    return null;
		  }
	  
	  /** PC-4792 - Inhouse Transaction - Quarantine Expense
	 * loadEmployeeList -This method used to load employee list
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadEmployeeList(ActionMapping actionMapping, ActionForm actionForm,
		      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

		  instantiate(request, response);
			HttpSession session = request.getSession(false);
			String strPrimaryCmpyId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
			GmLotExpiryReportBean gmLotExpiryReportBean = new GmLotExpiryReportBean(getGmDataStoreVO());
			GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
			GmAutoCompleteForm gmAutoCompleteForm = (GmAutoCompleteForm) actionForm;
			
			HashMap hmParam = new HashMap();
			hmParam = GmCommonClass.getHashMapFromForm(gmAutoCompleteForm);
			String strType = "";
			String strKey = "";
			String strSearchKey = GmCommonClass.parseNull((String) hmParam.get("STRSEARCHKEY"));
			String strMaxCount = GmCommonClass.parseZero((String) hmParam.get("STRMAXCOUNT"));
			String strSearchType = GmCommonClass.parseNull(request.getParameter("searchType"));
			strType = GmCommonClass.parseNull((String) request.getAttribute("STRTYPE"));
			String strTerm = GmCommonClass.parseNull(request.getParameter("term"));
			strSearchKey = strSearchKey.equals("") ? strTerm : strSearchKey;
			strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("EMPLOYEE_LIST", "Y");  
			
			hmParam.put("KEY", strKey);
			hmParam.put("SEARCH_KEY", strSearchKey);
			hmParam.put("MAX_CNT", Integer.parseInt(strMaxCount));
			hmParam.put("TYPE", strType);
			hmParam.put("COMPANYID", strPrimaryCmpyId);
			// The following code added for Getting the Distributor, Sales Rep and Employee List based on the PLANT too if the Selected Company is EDC
			 String strCompanyId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
			 String	 strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "LOANERINITFILTER")) ;
			 strCompanyPlant = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue(strCompanyId, "ITEMINITFILTER")) ;
			 
			 hmParam.put("FILTER", "Active");
			 hmParam.put("USERCODE", "300");
			 hmParam.put("DEPT", "");
			 hmParam.put("STATUS", "311");    
			    if(strCompanyPlant.equals("Plant")){
			    	hmParam.put("DISTFILTER", "COMPANY-PLANT");
			    	hmParam.put("REPFILTER", "COMPANY-PLANT");
			    	hmParam.put("EMPFILTER", "COMPANY-PLANT");
			    }	
			  log.debug("hmParam-------     >>   "+hmParam);
			String strResults = gmAutoCompleteReportBean.loadEmployeeList(hmParam);
			 log.debug("strResults-------     >>   "+strResults);
			response.getWriter().write(strResults);
		    return null;
		  }
}
