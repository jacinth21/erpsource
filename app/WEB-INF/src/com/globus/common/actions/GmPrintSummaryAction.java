package com.globus.common.actions;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCompanyReportBean;
import com.globus.common.beans.GmLogger;
import com.globus.custservice.beans.GmDOReportBean;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author ASophia
 *
 */
public class GmPrintSummaryAction extends GmDispatchAction{
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	/*
	 * loadDOSummary : This method is used to get the company information 
	 *                 from order id and pass  it to GmEditOrderServlet
	 *
	 * @param actionMapping
     * @param actionForm
	 * @param request
     * @param response
	 * @return
	 * @throws Exception
	 * 
	 * */
	public ActionForward loadDOSummary(ActionMapping actionMapping, ActionForm actionForm,
				HttpServletRequest request, HttpServletResponse response)
						throws Exception{
			instantiate(request,response);
			
			GmCompanyReportBean gmCompanyReportBean = new GmCompanyReportBean();
			GmDOReportBean gmDoReportBean = new GmDOReportBean(getGmDataStoreVO());
			
			String strSessOrderId ="";
			String strCmpnyDtls ="";
			String strURL="";
			String strParentId = "";
			
		    strSessOrderId = GmCommonClass.parseNull(request.getParameter("hOrdId"));
		    strCmpnyDtls = gmCompanyReportBean.fetchCompanyDtlsFromOrder(strSessOrderId);
		    strParentId = gmDoReportBean.fetchParentOrderId(strSessOrderId);
		    strURL = "GmEditOrderServlet?hMode=PrintPrice&hOrdId="+strParentId;
			strURL= strURL+"&companyInfo="+URLEncoder.encode(strCmpnyDtls);
			
			return actionRedirect(strURL,request);
	}
	
	/*
	 * loadInvoicePrint : This method is used to get the company information 
	 *                    from order id and pass  it to GmInvoiceInfoServlet
	 *
	 * @param actionMapping
     * @param actionForm
	 * @param request
     * @param response
	 * @return
	 * @throws Exception
	 * 
	 * */
	public ActionForward loadInvoicePrint(ActionMapping actionMapping, ActionForm actionForm,
				HttpServletRequest request, HttpServletResponse response)
						throws Exception{
			instantiate(request,response);
			GmCompanyReportBean gmCompanyReportBean = new GmCompanyReportBean();
			String strInvId ="";
			String strCmpnyDtls ="";
			String strURL="";
			
		    strInvId = GmCommonClass.parseNull(request.getParameter("hInv"));
			strCmpnyDtls = gmCompanyReportBean.fetchCompanyDtlsFromInvoice(strInvId);
		    strURL = "GmInvoiceInfoServlet?hAction=Print&hInv="+strInvId;
			strURL= strURL+"&companyInfo="+URLEncoder.encode(strCmpnyDtls);
			
			return actionRedirect(strURL,request);
	}

}
