/**
 * 
 */
package com.globus.common.actions;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionRedirect;
import org.apache.struts.actions.DispatchAction;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmFramework;
import com.globus.common.filters.GmAccessFilter;
import com.globus.valueobject.common.GmDataStoreVO;


/**
 * @author vprasath
 * 
 */
public class GmDispatchAction extends DispatchAction {
  protected HttpSession session;
  private GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();


  /**
   * @return
   */
  public GmDataStoreVO getGmDataStoreVO() {
    return this.gmDataStoreVO;
  }



  /**
     * 
     */
  public GmDispatchAction() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @author rshah
   * @param request, response
   * @return String filter Method for the providing the access permission based on the logged in
   *         user
   */
  protected void instantiate(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    session = request.getSession(false);
    gmDataStoreVO = GmFramework.getCompanyParams(request);
    checkSession(response, session); // Checks if the current session is valid, else redirecting to
                                     // SessionExpiry page
    request.setCharacterEncoding("UTF-8");
    response.setContentType("text/html; charset=UTF-8");
    response.setCharacterEncoding("UTF-8");

  }

  /**
   * actionRedirect - this method will used to append given companyInfo into URL and redirect to
   * given URL
   * 
   * @param strURL, request
   * @return ActionRedirect
   */
  protected ActionRedirect actionRedirect(String strURL, HttpServletRequest request) {

    ActionRedirect actionRedirect =
        new ActionRedirect(GmFramework.appendCompanyInfo(strURL, request));
    return actionRedirect;
  }

  public String getAccessFilter(HttpServletRequest req, HttpServletResponse res) throws AppError {
    HttpSession session = req.getSession(false);
    HashMap hmParam = new HashMap();
    String strFilter = "";
    String strDeptId = "";
    String strAccessID = "";
    String strUserRoleType = "";
    int accessId = 0;
    try {
      strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));

      if ((String) session.getAttribute("strSessAccLvl") != null)
        accessId = Integer.parseInt((String) session.getAttribute("strSessAccLvl"));

      if (strDeptId.equalsIgnoreCase("ICS")) {
        if (accessId < 3) {
          strUserRoleType = "8003"; // ICS Employee
        } else {
          strUserRoleType = "8004"; // ICS VP
        }
      }
      hmParam.put("SessUserId", session.getAttribute("strSessUserId"));
      hmParam.put("SessAccLvl", accessId + "");
      hmParam.put("SessDeptId", strDeptId);
      hmParam.put("UserRoleType", strUserRoleType);
      strFilter = GmAccessFilter.getAccessFilter(hmParam);

    } catch (Exception e) {
      throw new AppError(e);
    }
    return strFilter;
  }

  public void checkSession(HttpServletResponse res, HttpSession session) throws ServletException,
      IOException {
    if (session == null) {
      res.sendRedirect(GmCommonClass.getString("GMCOMMON").concat("/GmSessionExpiry.jsp"));
      log.debug(" Session is null ");
      return;
    } else if ((String) session.getAttribute("strSessUserId") == null) {
      res.sendRedirect(GmCommonClass.getString("GMCOMMON").concat("/GmSessionExpiry.jsp"));
      return;
    }
  }

}
