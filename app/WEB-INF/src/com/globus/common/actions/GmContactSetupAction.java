package com.globus.common.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonAddressContactBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmPartyBean;
import com.globus.common.forms.GmCommonContactForm;

public class GmContactSetupAction extends Action
{
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response)throws Exception {
        GmCommonContactForm gmContactForm = (GmCommonContactForm)form;
        GmCommonAddressContactBean gmCommonAddressContactBean = new GmCommonAddressContactBean();
        GmCommonBean gmCommonBean = new GmCommonBean();
        GmPartyBean gmPartyBean = new GmPartyBean();
        Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

        HashMap hmParam = new HashMap ();
        HashMap hmValues = new HashMap();
        ArrayList alLogReasons = new ArrayList();
        List lResult = null;

        String strContactId = "";
        String strPartyId = gmContactForm.getPartyId();
        String strPartyType = gmContactForm.getPartyType() == "" ? gmContactForm.getStrOpt() : gmContactForm.getPartyType();
        gmContactForm.setPartyType(strPartyType);
        
        gmContactForm.loadSessionParameters(request);
        String strOpt = gmContactForm.getStrOpt();
        hmParam = GmCommonClass.getHashMapFromForm(gmContactForm);
        
        log.debug(" stropt is " + strOpt);
        
        if (strOpt.equals("save"))
        {
            log.debug(" values to save " + hmParam);
            strContactId = gmCommonAddressContactBean.saveContact(hmParam);
            gmContactForm.setHcontactID(strContactId);
        }
        else if (strOpt.equals("edit"))
        {
            strContactId = gmContactForm.getHcontactID();
            hmValues = gmCommonAddressContactBean.fetchEditContactInfo(strContactId);
            log.debug(" values fetched for edit " +hmValues);
            gmContactForm = (GmCommonContactForm)GmCommonClass.getFormFromHashMap(gmContactForm,hmValues);
        }
        
        if (!strPartyId.equals("")){
            log.debug(" strPartyId --> " +strPartyId);
            lResult = gmCommonAddressContactBean.fetchContactInfo(strPartyId).getRows();
            log.debug(" size of lResult " + lResult.size());
            gmContactForm.setLdtResult(lResult);
        }
        
        gmContactForm.setAlNames(gmPartyBean.reportPartyNameList(strPartyType));
        gmContactForm.setAlContactTypes(GmCommonClass.getCodeList("COTTP"));
        gmContactForm.setAlPreferences(GmCommonClass.getCodeList("COTPT"));
        gmContactForm.setAlContactModes(GmCommonClass.getCodeList("COTMD"));
        log.debug( " values inside ALnames " +gmPartyBean.reportPartyNameList(strPartyType) );
        
        //      displaying comments
        alLogReasons = gmCommonBean.getLog(strPartyId,"1231");
        gmContactForm.setAlLogReasons(alLogReasons);
        
        return mapping.findForward("success");
    }

}
