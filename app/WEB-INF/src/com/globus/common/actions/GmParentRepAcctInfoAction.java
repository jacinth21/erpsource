package com.globus.common.actions;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmParentRepAccountBean;
import com.globus.common.forms.GmParentRepAcctInfoForm;
import com.globus.common.util.GmWSUtil;

public class GmParentRepAcctInfoAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code

  /**
   * Load parent rep account info using AjaX.
   * 
   * @param actionMapping the action mapping
   * @param actionForm the action form
   * @param request the request
   * @param response the response
   * @return the action forward
   * @throws Exception the exception
   */
  public ActionForward loadParentRepAccountInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String strJSON = "";
    GmWSUtil gmWSUtil = new GmWSUtil();
    instantiate(request, response);
    GmParentRepAcctInfoForm gmParentRepAcctInfoForm = (GmParentRepAcctInfoForm) actionForm;

    GmParentRepAccountBean gmParentRepAccountBean = new GmParentRepAccountBean(getGmDataStoreVO());
    strJSON =
        GmCommonClass.parseNull(gmWSUtil.parseArrayListToJson(gmParentRepAccountBean
            .fetchParentRepAccountInfo(gmParentRepAcctInfoForm.getAcctId())));
    log.debug("Paretn Acct strJSON:" + strJSON);
    if (!strJSON.equals("")) {
      response.setContentType("text/plain");
      PrintWriter pw = response.getWriter();
      pw.write(strJSON);
      pw.flush();
      return null;
    }
    return actionMapping.findForward("success");
  }
}
