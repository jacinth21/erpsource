//Source file: C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\common\actions\\GmStatusLogAction.java

package com.globus.common.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmStatusLogForm;
import com.globus.common.beans.GmStatusLogBean;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForward;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;

import com.globus.common.beans.GmCommonClass;
import com.globus.logon.beans.GmLogonBean;

public class GmStatusLogAction extends GmAction {

	/**
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return org.apache.struts.action.ActionForward
	 */
	public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			instantiate(request, response);
			GmStatusLogForm gmStatusLogForm = (GmStatusLogForm) actionForm;
			gmStatusLogForm.loadSessionParameters(request);
			GmStatusLogBean gmStatusLogBean = new GmStatusLogBean(getGmDataStoreVO());
			Logger log = GmLogger.getInstance(this.getClass().getName());// Code
			String strOpt = GmCommonClass.parseNull(gmStatusLogForm.getStrOpt());
			String strHAction = GmCommonClass.parseNull(gmStatusLogForm.getHaction());

			 log.debug("strOpt:" + strOpt + " --strHAction:" + strHAction);

			HashMap hmParams = new HashMap();
			ArrayList alDetails = new ArrayList();
			ArrayList alReportList = new ArrayList();
			//ArrayList alStatus = new ArrayList();
			ArrayList alTypes = new ArrayList();
			ArrayList alUsers = new ArrayList();
			ArrayList alLocations = new ArrayList();
			ArrayList alType = new ArrayList();
			// String strStartDate = "";
			// String strEndDate = "";
			String strTypeNum = "";
			// String strSource = "";
			// String strUserNum = "";
			String strRefID = "";
			String strToLocationID = "";
			String strFromLocationID = "";
			// String strUserID = "";
			String strDateDiff = "";
			HashMap hmSaveTo = new HashMap();
			HashMap hmSaveFrom = new HashMap();
			HashMap hmMap = new HashMap();
			HashMap hmTypeVal = new HashMap();
			
			GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
			GmCommonClass gmCommonClass = new GmCommonClass();
			
			// Create default Start and End Dates
			String strApplDateFmt = getGmDataStoreVO().getCmpdfmt();
			String strSessTodaysDate = GmCalenderOperations.getCurrentDate(strApplDateFmt);;
			String strStartDate = GmCommonClass.parseNull(request.getParameter("startDate"));
			String strEndDate = GmCommonClass.parseNull(request.getParameter("endDate"));
            strStartDate = strStartDate.equals("") ? strSessTodaysDate : strStartDate;
            strEndDate = strEndDate.equals("") ? strSessTodaysDate : strEndDate;
            // To get the value from rule table to compare dates
            strDateDiff = GmCommonClass.parseNull((String)GmCommonClass.getRuleValue("LOG_REP","LOG_REP"));
            gmStatusLogForm.setStrDateDiff(strDateDiff);
           
			request.setAttribute("FROMDATE", strStartDate);
			request.setAttribute("TODATE", strEndDate);

			if (strOpt.equals("DETAIL")) {

				if (strHAction.equals("reload") || strHAction.equals("saveLocations")) {
					alLocations = GmCommonClass.getCodeList("LOCAT");
					strRefID = GmCommonClass.parseNull(gmStatusLogForm.getRefID());
					gmStatusLogForm.setAlLocations(alLocations);
					log.debug(" strRefID " + strRefID);
					if (strHAction.equals("saveLocations")) {

						strToLocationID = GmCommonClass.parseNull(gmStatusLogForm.getToLocationID());
						strFromLocationID = GmCommonClass.parseNull(gmStatusLogForm.getFromLocationID());
						// log.debug("strToLocationID : " + strToLocationID);
						hmSaveFrom = GmCommonClass.getHashMapFromForm(gmStatusLogForm);
						hmSaveFrom.put("TYPENUM", strFromLocationID);

						hmSaveTo = GmCommonClass.getHashMapFromForm(gmStatusLogForm);
						// log.debug("hmSaveFrom : " + hmSaveFrom);
						if (!strToLocationID.equals("0")) {

							gmStatusLogBean.saveStatusDetails(hmSaveFrom);

							hmSaveTo.put("TYPENUM", strToLocationID);
							hmSaveTo.put("USERID", "");
							hmSaveTo.put("UPDDATE", null);
							hmSaveTo.put("TYPENUM", strToLocationID);
							gmStatusLogBean.saveStatusDetails(hmSaveTo);
						} else {
							gmStatusLogBean.updateStatusDetails(hmSaveFrom);
						}
					}
				} else {
					gmStatusLogForm.setHaction("noList");
				}

				// this code sets values back to screen
				gmStatusLogForm = (GmStatusLogForm) GmCommonClass.getFormFromHashMap(gmStatusLogForm, hmParams);
				log.debug("1........");
				alDetails = GmCommonClass.parseNullArrayList(gmStatusLogBean.fetchStatusLogDetail(strRefID));
				log.debug("alDetails.size()........."+alDetails.size());
				if (alDetails.size() > 0) {
					HashMap hmTemp = (HashMap) alDetails.get(0);

					gmStatusLogForm.setSource((String) hmTemp.get("SOURCEID"));
					// log.debug("(String)hmTemp.get('SOURCEID'):" + (String)
					// hmTemp.get("SOURCEID"));
					// log.debug("hmTemp:" + hmTemp);
				}
				gmStatusLogForm.setAldetailList(alDetails);

				return actionMapping.findForward("detail");
			}

			else if (strOpt.equals("REPORT")) {// if
				// (strHAction.equals("loadstatus")||strHAction.equals("rptReload"))

				hmMap.put("USERCODE", "300,301");
                hmMap.put("DEPT", "");
                hmMap.put("STATUS", "311,312,314,317");
				alTypes = gmCommonClass.getCodeList("CMLOG");
				alUsers = gmLogonBean.getEmployeeList(hmMap);
				
				hmTypeVal = GmCommonClass.getAltGroupDetails(alTypes);
				gmStatusLogForm.setHmTypeVal(hmTypeVal);

				gmStatusLogForm.setAlTypes(alTypes);
				gmStatusLogForm.setAlUsers(alUsers);

				if (!strHAction.equals("rptReload")) {
					gmStatusLogForm.setHaction("initiating");
				}

				hmParams = GmCommonClass.getHashMapFromForm(gmStatusLogForm);
				if (strHAction.equals("rptReload")) {
					// log.debug("Forms hmParams: " + hmParams);
					alReportList = GmCommonClass.parseNullArrayList(gmStatusLogBean.reportStatusLogList(hmParams));
					// log.debug("alReportList:" + alReportList);
					gmStatusLogForm.setAlReportList(alReportList);
				}

				gmStatusLogForm.setStrOpt("REPORT");
				hmParams = GmCommonClass.getHashMapFromForm(gmStatusLogForm);
				gmStatusLogForm = (GmStatusLogForm) GmCommonClass.getFormFromHashMap(gmStatusLogForm, hmParams);
				return actionMapping.findForward("report");
			}
		} catch (Exception exp) {
			exp.printStackTrace();
			throw new AppError(exp);
		}
		return actionMapping.findForward("detail");
	}
}