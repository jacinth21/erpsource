package com.globus.common.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBatchBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmBatchInfoIncludeForm;

public class GmBatchInfoIncludeAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code

  /**
   * execute
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */
  @Override
  public ActionForward execute(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmBatchInfoIncludeForm gmBatchInfoIncludeForm = (GmBatchInfoIncludeForm) actionForm;
    GmBatchBean gmBatchBean = new GmBatchBean(getGmDataStoreVO());

    gmBatchInfoIncludeForm.loadSessionParameters(request);

    HashMap hmParam = new HashMap();
    HashMap hmHeaderInfo = new HashMap();
    String strActForward = "";
    String strBatchId = "";
    String strOpt = "";


    hmParam = GmCommonClass.getHashMapFromForm(gmBatchInfoIncludeForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strBatchId = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));

    log.debug(" strBatchId --> " + strBatchId);

    hmHeaderInfo = gmBatchBean.fetchBatchHeader(hmParam);
    GmCommonClass.getFormFromHashMap(gmBatchInfoIncludeForm, hmHeaderInfo);

    strActForward = GmCommonClass.parseNull(request.getParameter("RE_FORWARD"));
    strActForward = strActForward.equals("") ? "GmBatchInfoInclude" : strActForward;
    log.debug("strActForward===========> " + strActForward);
    return actionMapping.findForward(strActForward);
  }
}
