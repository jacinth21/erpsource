/**
 * FileName    : GmRuleConsequenceAction.java 
 * Description :
 * Author      : rshah
 * Date        : Jun 10, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.forms.GmRuleConditionForm;

/**
 * @author rshah
 *
 */
public class GmRuleConsequenceAction extends GmAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	   
	   /**
	    * @return ActionForward
	    * @roseuid 48F65C7B01BA
	    */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {

		GmRuleConditionForm gmRuleConditionForm = (GmRuleConditionForm) form;
		gmRuleConditionForm.loadSessionParameters(request);
		instantiate(request,response);
		GmRuleBean gmRuleBean = new GmRuleBean(getGmDataStoreVO());
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
		String strRuleId = "";
		String strOpt = "";
		String strAction = "";
		String strConsGroup = ""; 
		String strConsGrpId = "";
		String strHAction = "";
		String strRuleConsId = "";
		String accessFlag = "";
		String strPartyId = "";
		String strEmailId = "";
		HashMap hmConsequence = new HashMap(); 
		HashMap hmAccess = new HashMap();
		ArrayList alConsTabList = new ArrayList();
		ArrayList alConsequenceList = new ArrayList();
		ArrayList alDbConsequenceList = new ArrayList();
		ArrayList alSelectedConsList = new ArrayList();
		//HashMap hmControls = new HashMap();
		
		strOpt = gmRuleConditionForm.getStrOpt();
		strRuleId = gmRuleConditionForm.getRuleId();
		strAction = gmRuleConditionForm.getStrAction();
		strHAction = gmRuleConditionForm.getHaction();
		strConsGroup = gmRuleConditionForm.getConsequenceGrp();
		strConsGrpId = gmRuleConditionForm.getConsequenceId(); //id of CQMSG, CQPIC.....
		strRuleConsId = gmRuleConditionForm.getRuleConsequenceId();
		strPartyId = gmRuleConditionForm.getSessPartyId();
		
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmRuleConditionForm);
		//log.debug("StrOpt.....1....."+strOpt+"..Rule id.."+gmRuleConditionForm.getRuleId());
		log.debug("hmParam in gmRuleConsequenceForm = " + hmParam);
		
		
		if ( strRuleId.equals("") || strRuleId.equals("0"))
        {
			gmRuleConditionForm.setRuleErrorMsg("Rule Id not exist, create the rule");
			log.debug("Forwarding to = >GmRuleConsequenceTab<");			
			return mapping.findForward("GmRuleConsequenceTab");
        }
		if(strOpt.equals("save"))
		{
			gmRuleBean.saveConsequences(hmParam);
			strOpt = "load"; //For time being
			strAction = "reload";
			gmRuleConditionForm.setStrOpt("strOpt");
		}
		if(strAction.equals("reload")||strAction.equals("upload"))
		{
			if (strConsGrpId.equals("91347")) // For picture data..
			{
				alDbConsequenceList = GmCommonClass.parseNullArrayList(gmRuleBean.fetchAllPicConsequence(strRuleId, strConsGrpId));
				if(strOpt.equals("edit") && !strRuleConsId.equals(""))
				{
					alSelectedConsList = GmCommonClass.parseNullArrayList(gmRuleBean.fetchPicConsequence(strRuleConsId));
					gmRuleConditionForm.setAlSelectedConsList(alSelectedConsList);
				}
			}
			else
			{
				alDbConsequenceList = GmCommonClass.parseNullArrayList(gmRuleBean.fetchConsequence(strRuleId, strConsGrpId));
			}
			strEmailId = GmCommonClass.parseNull(gmRuleBean.fetchEmailId(gmRuleConditionForm.getUserId()));
			alConsequenceList =   GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList(strConsGroup));
			gmRuleConditionForm.setAlConsequenceList(alConsequenceList);
			gmRuleConditionForm.setAlDbConsequenceList(alDbConsequenceList);
			gmRuleConditionForm.setEmailId(strEmailId);
			log.debug("alConsequenceList...."+alConsequenceList);	
			strOpt = "GmConsequenceBuilder";			
		}
		
		alConsTabList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CONSQ"));
		gmRuleConditionForm.setAlConsTabList(alConsTabList);
		log.debug("strOpt in cosequ Actio..... = " + strOpt);
		
		hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PD_CREATE_RULE");// PMT-51299-Product Development Create Rule access
		accessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		log.debug("Permissions = " + hmAccess);
		if(accessFlag.equals("Y"))
		{
			gmRuleConditionForm.setEnableCreateRule("true");
		}
		
		hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PD_VOID_RULE"); // PMT-51299-Product Development Void Rule access
		accessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		log.debug("enableRollback accessFlag:" + accessFlag);
		log.debug("Permissions = " + hmAccess);
		if(accessFlag.equals("Y"))
		{ 
			gmRuleConditionForm.setEnableVoidRule("true");
		}
		
		if(strOpt.equals("") || strOpt.equals("load"))
		{
			strOpt = "GmRuleConsequenceTab";
		}
		if(strConsGrpId.equals("91347"))
		{
			strOpt = "GmConsequenceFileUpload";
		}
		log.debug("Forwarding to = >" + strOpt + "<");
		return mapping.findForward(strOpt);
       
	}

}
