package com.globus.common.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.forms.GmVendorPaperworkForm;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.beans.GmPurchaseBean;
import com.globus.operations.beans.GmVendorPaperworkBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmVendorPaperworkAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger

	
	// This Method is to load the PO Paperwork to view it in vendor portal
	public ActionForward loadPOPaperwork(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String strPoID = "";
		String strVendorID = "";
		String strUserID = "";
		String strGuId = "";
		HashMap hmResult = new HashMap();
		HashMap hmReturn = new HashMap();
		GmDataStoreVO gmDataStoreVO = null;

		String strComapnyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));

		if (strComapnyInfo.equals("")) {
			gmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
		} else {
			gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strComapnyInfo);
			
		}

		GmPurchaseBean gmPurc = new GmPurchaseBean(gmDataStoreVO);
		GmVendorPaperworkBean gmVendorPaperworkBean = new GmVendorPaperworkBean(gmDataStoreVO);

		strUserID = GmCommonClass.parseNull((String) request.getParameter("huserId"));
		strGuId = GmCommonClass.parseNull((String) request.getParameter("hGUId"));
		hmResult = gmVendorPaperworkBean.fetchPODetailsbyGUID(strGuId);
		strPoID = GmCommonClass.parseNull((String) hmResult.get("POID"));
		strVendorID = GmCommonClass.parseNull((String) hmResult.get("VENDORID"));
		hmReturn = gmPurc.viewPO(strPoID, strVendorID, strUserID);
		request.setAttribute("hmReturn", hmReturn);

		return actionMapping.findForward("GmPOPrint");
	}

	// This Method is to load the WO Paperwork to view it in vendor portal
	public ActionForward loadWOPaperwork(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		String strWOId = "";
		String strDocRev = "";
		String strDocFooter = "";
		String strDocFl = "";
		String strForward = "";
		String strUserID = "";
		String strGuId = "";

		HttpSession session = request.getSession(true);

		HashMap hmResult = new HashMap();
		HashMap hmReturn = new HashMap();
		GmDataStoreVO gmDataStoreVO = null;

		String strComapnyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));

		if (strComapnyInfo.equals("")) {
			gmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
		} else {
			gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strComapnyInfo);
		}

		GmPurchaseBean gmPurc = new GmPurchaseBean(gmDataStoreVO);
		GmVendorPaperworkBean gmVendorPaperworkBean = new GmVendorPaperworkBean(gmDataStoreVO);
		GmCommonBean gmCommonBean = new GmCommonBean(gmDataStoreVO);

		String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
		GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork",
				strCompanyLocale);

		strUserID = GmCommonClass.parseNull((String) request.getParameter("huserId"));
		strGuId = GmCommonClass.parseNull((String) request.getParameter("hGUId"));
		String strUserName = gmCommonBean.getUserName(strUserID);
		session.setAttribute("strSessShName", strUserName);
		session.setAttribute("strSessUserId", strUserID);

		hmResult = gmVendorPaperworkBean.fetchWODetailsbyGUID(strGuId);

		strWOId = GmCommonClass.parseNull((String) hmResult.get("WOID"));
		strDocRev = GmCommonClass.parseNull((String) hmResult.get("DOCREV"));
		strDocFooter = GmCommonClass.parseNull((String) hmResult.get("DOCFOOTER"));
		strDocFl = strDocFooter.substring(strDocFooter.length() - 1);

		hmReturn = gmPurc.viewWO(strWOId, strUserID);
		HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), "");

		hmReturn.put("LOGO", GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_LOGO")));
		hmReturn.put("gmResourceBundleBean", gmResourceBundleBean);
		request.setAttribute("hmReturn", hmReturn);
		request.setAttribute("hAction", "PrintWO");
		request.setAttribute("hWOId", strWOId);

		hmReturn.put("SUBREPORT_DIR",
				session.getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION")) + "\\");

		request.setAttribute("hmReturn", hmReturn);

		if (strDocFl.equals("Y")) {
			String strRptName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.JASPERNM"));
			request.setAttribute("STRRPTNAME", strRptName);
            
			strForward = "GmWOPrint";

		} else {
			strForward = "GmWOPrint".concat(strDocRev);
		}
		return actionMapping.findForward(strForward);
	}

	
	//PC-2851 Ability to Print All WO's Paperwork in a PO
	// This Method is to load the WO All Paperwork to view it in vendor portal
	public ActionForward loadAllWOPaperwork(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
				HttpServletResponse response) throws Exception {

			String strDocRev = "";
			String strDocFooter = "";
			String strDocFl = "";
			String strForward = "";
			String strUserID = "";
			String strGuId = "";
			String strPoID = "";

			HttpSession session = request.getSession(true);

			HashMap hmResult = new HashMap();
			HashMap hmReturn = new HashMap();
			ArrayList alReturn = new ArrayList();
			GmDataStoreVO gmDataStoreVO = null;

			String strComapnyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));

			if (strComapnyInfo.equals("")) {
				gmDataStoreVO = GmCommonClass.getDefaultGmDataStoreVO();
			} else {
				gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strComapnyInfo);
			}

			GmPurchaseBean gmPurc = new GmPurchaseBean(gmDataStoreVO);
			GmVendorPaperworkBean gmVendorPaperworkBean = new GmVendorPaperworkBean(gmDataStoreVO);
			GmCommonBean gmCommonBean = new GmCommonBean(gmDataStoreVO);

			String strCompanyLocale = GmCommonClass.getCompanyLocale(gmDataStoreVO.getCmpid());
			GmResourceBundleBean gmResourceBundleBean = GmCommonClass.getResourceBundleBean("properties.Paperwork",
					strCompanyLocale);

			strUserID = GmCommonClass.parseNull((String) request.getParameter("huserId"));
			strGuId = GmCommonClass.parseNull((String) request.getParameter("hGUId"));
			String strUserName = gmCommonBean.getUserName(strUserID);
			session.setAttribute("strSessShName", strUserName);
			session.setAttribute("strSessUserId", strUserID);

			hmResult = gmVendorPaperworkBean.fetchPODetailsbyGUID(strGuId);
			strPoID = GmCommonClass.parseNull((String) hmResult.get("POID"));
			
			strDocRev = GmCommonClass.parseNull((String) hmResult.get("DOCREV"));
			strDocFooter = GmCommonClass.parseNull((String) hmResult.get("DOCFOOTER"));
			strDocFl = strDocFooter.substring(strDocFooter.length() - 1);

			alReturn = gmPurc.getAllWOForPrint(strPoID);
			hmReturn.put("SUBREPORT_DIR",
					session.getServletContext().getRealPath(GmCommonClass.getString("GMJASPERLOCATION")) + "\\");
			hmReturn.put("ALLWO", alReturn);
			HashMap hmCompanyAddress = GmCommonClass.fetchCompanyAddress(gmDataStoreVO.getCmpid(), "");
			hmReturn.put("LOGO", GmCommonClass.parseNull((String) hmCompanyAddress.get("COMP_LOGO")));
			hmReturn.put("gmResourceBundleBean", gmResourceBundleBean);
			request.setAttribute("hmReturn", hmReturn);

			if (strDocFl.equals("Y")) {
				String strRptName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("WO.JASPERNM"));
				request.setAttribute("STRRPTNAME", strRptName);

				strForward = "GmWOPrintAll";

			} else {
				strForward = "GmWOPrintAll".concat(strDocRev);
			}
			return actionMapping.findForward(strForward);
		}
}
