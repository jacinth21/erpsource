package com.globus.common.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.accounts.physicalaudit.beans.GmPATagInfoBean;
import com.globus.accounts.physicalaudit.forms.GmLockTagForm;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;


public class GmManualTagPrintAction extends GmAction {
  @Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    instantiate(request, response);
    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());
      ArrayList alReturn = new ArrayList();
      GmPATagInfoBean gmPATagInfoBean = new GmPATagInfoBean(getGmDataStoreVO());
      GmLockTagForm gmLockTagForm = (GmLockTagForm) form;
      gmLockTagForm.loadSessionParameters(request);
      HashMap hmParam = new HashMap();
      ArrayList alFinalList = new ArrayList();
      ArrayList alTempList = new ArrayList();
      GmJasperReport gmJasperReport = null;
      String strDestFileName = "";
      String strTempDestFileName = "";

      String year = GmCommonClass.parseNull(request.getParameter("year"));
      String strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid());
      final String FILEPATH = GmCommonClass.getString("FILEPATH") + strCompanyLocale + "\\" + year;
      File newFile = new File(FILEPATH);
      if (!newFile.exists()) {
        newFile.mkdirs();
      }

      String FILEEXT = ".html";
      int fileSize = 1;
      int splitSize = 0;
      String strPhysicalAuditID = "";
      ArrayList alPhysicalAuditList = new ArrayList();
      HashMap hmPhysicalAudit = new HashMap();
      ArrayList alLocationList = new ArrayList();

      hmParam = GmCommonClass.getHashMapFromForm(form);
      strPhysicalAuditID = gmLockTagForm.getPhysicalAuditID();
      alPhysicalAuditList = gmPATagInfoBean.fetchOpenPhysicalAudits();
      gmLockTagForm.setPhysicalAuditList(alPhysicalAuditList);
      gmLockTagForm.setAlType(GmCommonClass.getCodeList("PRTAG"));
      String strUserStatusFlag =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(gmLockTagForm.getUserId(),
              "PA_CREATEHTML"));
      log.debug("strUserStatusFlag = " + strUserStatusFlag);
      if (!strUserStatusFlag.equals("Y")) {
        throw new AppError("", "20674");
      }

      if (strPhysicalAuditID.equals("") && alPhysicalAuditList.size() > 0) {
        hmPhysicalAudit = (HashMap) alPhysicalAuditList.get(0);
        strPhysicalAuditID = (String) hmPhysicalAudit.get("CODEID");

      }
      alLocationList = gmPATagInfoBean.fetchLocations(strPhysicalAuditID);
      gmLockTagForm.setLocationList(alLocationList);
      gmLockTagForm.setAuditUserList(gmPATagInfoBean.fetchCountedBy(strPhysicalAuditID));

      if (gmLockTagForm.getStrOpt().equals("HTML") || gmLockTagForm.getStrOpt().equals("PDF")) {
        splitSize = Integer.parseInt(GmCommonClass.parseZero(gmLockTagForm.getSplitSize()));
        hmParam.put("MISSINGFLAG", "Y");
        alReturn = (ArrayList) gmPATagInfoBean.fetchTags(hmParam).getRows();
        GmCommonClass gmCommonClass = new GmCommonClass();
        alFinalList = gmCommonClass.splitArrayList(alReturn, splitSize);
        strTempDestFileName =
            FILEPATH + GmCommonClass.getCodeAltName(gmLockTagForm.getLocationID());
        strDestFileName = strTempDestFileName;

        for (int count = 0; count < alFinalList.size(); count++) {
          alTempList = (ArrayList) alFinalList.get(count);
          splitSize = alTempList.size() < splitSize ? alTempList.size() : splitSize;
          if (gmLockTagForm.getStrOpt().equals("PDF")) {
            FILEEXT = ".pdf";
          }
          strDestFileName =
              strDestFileName + (count + 1) + "_" + fileSize + "_" + (fileSize + (splitSize - 1));
          strDestFileName = strDestFileName + FILEEXT;
          fileSize = fileSize + splitSize;
          log.debug("Size: " + alTempList.size() + " : " + strDestFileName);
          synchronized (this) {
            gmJasperReport = new GmJasperReport();
            gmJasperReport.setRequest(request);
            gmJasperReport.setResponse(response);
            gmJasperReport.setJasperReportName("/GmPATagPrint.jasper");
            gmJasperReport.setHmReportParameters(hmParam);
            // alTempList = new ArrayList();
            gmJasperReport.setReportDataList(alTempList);

            if (gmLockTagForm.getStrOpt().equals("PDF")) {
              gmJasperReport.exportJasperReportToPdf(strDestFileName);
            } else {
              gmJasperReport.exportJasperReportToHtml(strDestFileName);
            }
          }
          alTempList = null;
          strDestFileName = strTempDestFileName;
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new AppError(ex);
    }

    return mapping.findForward("success");
  }
}
