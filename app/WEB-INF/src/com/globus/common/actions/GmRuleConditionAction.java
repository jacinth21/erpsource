package com.globus.common.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmRuleBean;
import com.globus.common.forms.GmRuleConditionForm;

public class GmRuleConditionAction extends GmAction{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	   
	   /**
	    * @return ActionForward
	    * @roseuid 48F65C7B01BA
	    */
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws Exception {

		GmRuleConditionForm gmRuleConditionForm = (GmRuleConditionForm) form;
		gmRuleConditionForm.loadSessionParameters(request);
		instantiate(request,response);
		GmRuleBean gmRuleBean = new GmRuleBean(getGmDataStoreVO());
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
		String strRuleId = "0";
		String strOpt = "";
		String strDeptId = "";
		String strUserShName = "";
		String strTodaysDate = "";
		String strActiveFl = "";
		String hinputStringTrans = "";
		String strAction = "";
		String strPartyId = "";
 	    String accessFlag = "";
 	    String strSpecificTrans = "";
 	    ArrayList alTransactionList = new ArrayList();
		ArrayList alRuleTxnType = new ArrayList();
		ArrayList alSpecificTrans  = new ArrayList();
		ArrayList alRule = new ArrayList();
		HashMap hmValues = new HashMap(); 
		HashMap hmAccess = new HashMap();
		HashMap hmRuleHead = new HashMap();
		HashMap hmSpecificTrans = new HashMap();
		strOpt = gmRuleConditionForm.getStrOpt();
		strDeptId = gmRuleConditionForm.getDeptId();
		strRuleId = gmRuleConditionForm.getRuleId();
		strAction = gmRuleConditionForm.getStrAction();
		strPartyId = gmRuleConditionForm.getSessPartyId();
		
		//log.debug("strRuleId = "+strRuleId+" strOpt = " + strOpt+"..strDept = "+strDeptId+"....strUserShName = "+strUserShName);
		//log.debug("strAction...."+strAction+"...gmRuleConditionForm.getTransStr()...."+gmRuleConditionForm.getTransStr());
		HashMap hmParam = GmCommonClass.getHashMapFromForm(gmRuleConditionForm);
		//log.debug("hmParam in gmRuleConditionaction = " + hmParam);
		
		if(strOpt.equals("save")) {
			strRuleId = GmCommonClass.parseNull(gmRuleBean.saveConditions(hmParam));	
			//if(strRuleId.equals(""))
			
		}	
		if(strAction.equals("Reload") && !strRuleId.equals("")) {
			hmValues = GmCommonClass.parseNullHashMap(gmRuleBean.fetchRules(strRuleId));
			String strSelectedTxn = GmCommonClass.parseNull((String)hmValues.get("TRANSACTION"));
			gmRuleConditionForm.setSelectedTrans(strSelectedTxn.split(","));
			gmRuleConditionForm.setHmRules(hmValues);
			alRule = GmCommonClass.parseNullArrayList((ArrayList)hmValues.get("ALRULE"));
			for (int j=0;j<alRule.size();j++)
			{
				hmRuleHead = (HashMap)alRule.get(j);
				gmRuleConditionForm.setRuleTxnType(GmCommonClass.parseNull((String)hmRuleHead.get("RULETXNTYPE")));
			}
			strOpt = "load";			
		}
		alTransactionList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RLTXN"));
		alRuleTxnType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("RLTRS"));
		
		for (int count = 0; count < alRuleTxnType.size(); count++) {
			HashMap hmTrans = (HashMap) alRuleTxnType.get(count);
			alSpecificTrans = GmCommonClass.getCodeList("RLTXN", (String) hmTrans.get("CODEID"));
			for(int i=0; i<alSpecificTrans.size();i++)
			{				
				HashMap hmTempTrans = (HashMap)alSpecificTrans.get(i);
				strSpecificTrans  = strSpecificTrans + (String)hmTempTrans.get("CODEID")+",";				
			}	
			hmSpecificTrans.put((String) hmTrans.get("CODEID"), strSpecificTrans.substring(0,strSpecificTrans.length()-1));
			strSpecificTrans="";
			
			//log.debug("hmSpecificTrans = "+hmSpecificTrans);
		}
		
		gmRuleConditionForm.setAlTransactionList(alTransactionList);
		gmRuleConditionForm.setInitiatedBy(strUserShName);
		gmRuleConditionForm.setInitiatedDate(strTodaysDate);
		gmRuleConditionForm.setActiveFl(strActiveFl);
		gmRuleConditionForm.setRuleId(strRuleId);
		gmRuleConditionForm.setAlRuleTxnType(alRuleTxnType);
		gmRuleConditionForm.setHmTrans(hmSpecificTrans);
		   //code for access rights
		hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PD_CREATE_RULE"); // PMT-51299-Product Development Create Rule access
		accessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		log.debug("Permissions = " + hmAccess);
		if(accessFlag.equals("Y"))
		{
			gmRuleConditionForm.setEnableCreateRule("true");
		}
		
		hmAccess = gmAccessControlBean.getAccessPermissions(strPartyId, "PD_VOID_RULE"); // PMT-51299-Product Development Void Rule access
		accessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		log.debug("enableRollback accessFlag:" + accessFlag);
		log.debug("Permissions = " + hmAccess);
		if(accessFlag.equals("Y"))
		{ 
			gmRuleConditionForm.setEnableVoidRule("true");
		}
		
		if(strOpt.equals(""))
		{
			strOpt = "GmRuleContainer";
		}
		//log.debug("gmRuleConditionForm....Rule id.."+gmRuleConditionForm.getRuleId());
		//log.debug("Forwarding to = >" + strOpt + "<");
		return mapping.findForward(strOpt);
	}
}
