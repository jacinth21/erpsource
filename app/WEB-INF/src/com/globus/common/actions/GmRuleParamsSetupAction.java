/**
 * FileName    : GmRuleParamsSetupAction.java 
 * Description :
 * Author      : Velu
 * Date        : Jul 31 2010 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.forms.GmRuleParamsSetupForm;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.party.beans.GmDealerInvoiceParamsBean;
import com.globus.accounts.beans.GmAccountsBean;

import java.util.Iterator;
import java.util.List;
import java.util.HashMap;




public class GmRuleParamsSetupAction extends GmAction {

	public ActionForward execute(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws Exception {
	
		Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmAccountsBean gmAccountsBean = new GmAccountsBean(getGmDataStoreVO());
		GmCommonClass gmCommon = new GmCommonClass();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();	
		HashMap hmParam = new HashMap();
		HashMap hmResult = new HashMap();
		GmRuleParamsSetupForm gmRuleParamsSetupForm = (GmRuleParamsSetupForm)form;
		instantiate(request, response);
		GmCustSalesSetupBean gmCustSalesSetupBean = new GmCustSalesSetupBean(getGmDataStoreVO());
		gmRuleParamsSetupForm.loadSessionParameters(request);		
		String strOpt = gmRuleParamsSetupForm.getStrOpt();
		String strDispacthTo = "gmRuleParamSetupAction";
		String strCodeGrpID = "";
		String strRuleGrpID = "";
		String strAccountType = "";
		ArrayList alPayment = new ArrayList();
		List alResult = null;
		 String strRuleval ="";
		 HashMap hmReturn = new HashMap();
		ArrayList alReturn = new ArrayList ();
		RowSetDynaClass rowSetDynaClass = null;
		
		
		
		hmParam =(HashMap)GmCommonClass.getHashMapFromForm(gmRuleParamsSetupForm);
		strCodeGrpID = GmCommonClass.parseNull((String) hmParam.get("CODEGRPID"));
		// to get the Rule Group id
		strRuleGrpID = GmCommonClass.parseNull((String)hmParam.get("RULEGRPID"));
		String strSesPartyId = GmCommonClass.parseNull((String)gmRuleParamsSetupForm.getSessPartyId());
		 // INV_PARAM_SUBMIT_ACC - Users in this security group will have the ability to enter Invoice Parameter information
	      HashMap hmShipSetupAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strSesPartyId,"INV_PARAM_SUBMIT_ACC"));
	      String strInvSubmitFl = GmCommonClass.parseNull((String) hmShipSetupAccess.get("UPDFL"));
	      request.setAttribute("SUBMITACCFL", strInvSubmitFl);
	      
	  	//PMT-35167 Check for input in required fields when marking XML file in Invoice Parameter
			if (strOpt.equals("save")) {
			   gmAccountsBean.checkInvoiceXMLParams(hmParam);
			}
		
        //Add ICT Parameter values save functionalities
		if (strOpt.equals("save") || strOpt.equals("edit")) {
			gmCommonBean.saveRuleParams(hmParam);
			strOpt = "edit";
		} 
		// get rule value for Mandatory controls
		 strRuleval = GmCommonClass.parseNull((String) GmCommonClass.getRuleValue(strCodeGrpID, "CTRLVLD")) ; 
		 gmRuleParamsSetupForm.setRulevalidation(strRuleval); 

		//Add ICT Parameter values fetch functionalities  
		alResult =  gmCommonBean.fetchEditRuleParamsinfo(hmParam);
		alPayment = gmCommon.getCodeList("PAYM");//getting code lookup values for Term of Sale drop down.	
		gmRuleParamsSetupForm.setlResult(alResult);
		gmRuleParamsSetupForm.setAlReturn(alPayment);
		
		// check if any of the rows needs a select box. if yes then fetch the code group for them
		//this code will need to be altered if you need more than one select box
		
		
		Iterator iter = alResult.iterator();
		log.debug ("alResult is "+alResult.size());
		while(iter.hasNext()){	
			hmResult = (HashMap)iter.next();
			String strCntrlTyp = GmCommonClass.parseNull((String)hmResult.get("CNTRLTYP"));
			String strCdGrp = GmCommonClass.parseNull((String)hmResult.get("CDGRP"));
			String strCdId = GmCommonClass.parseNull((String)hmResult.get("LABELID"));
			if (strCntrlTyp.equalsIgnoreCase("SELECT")){
				if (!strCdGrp.equals("")){
					alReturn = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList(strCdGrp));
					if(strCodeGrpID.equals("SHPPM")){
						hmReturn.put(strCdId, alReturn);
					}else{
						gmRuleParamsSetupForm.setAlReturn(alReturn);
					}
				}
			}
		}
		//Account attribute set up.
		if(strCodeGrpID.equals("SHPPM")){
			// to get the Account setup - Parameter Comments log details.
			alReturn = gmCommonBean.getLog(strRuleGrpID,"1290"); //1290: Account Parameter Setup
			gmRuleParamsSetupForm.setAlLogReasons(alReturn);
			// to get the Account type and set value to Form.
			strAccountType=GmCommonClass.parseNull(gmCustSalesSetupBean.getAccountType(strRuleGrpID));
			gmRuleParamsSetupForm.setStrAccountType(strAccountType);			
			strDispacthTo ="accountParamSetup";
		}
		gmRuleParamsSetupForm.setHmResult(hmReturn);
		return mapping.findForward(strDispacthTo);
	} 
}