package com.globus.common.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.operations.shipping.beans.GmShippingInfoBean;
import com.globus.operations.shipping.forms.GmShipDetailsForm;
import com.globus.party.beans.GmAddressBean;

public class GmShippingAction extends GmAction {
  // Code to Initialize the Logger Class.;

  protected Logger log = GmLogger.getInstance(this.getClass().getName());

  public HashMap loadShipReqParams(HttpServletRequest request) {

    HashMap hmReturn = new HashMap();
    String strShipTo = GmCommonClass.parseNull(request.getParameter("shipTo"));
    String strShipToId = GmCommonClass.parseNull(request.getParameter("names"));
    // log.debug("strShipToId "+strShipToId );
    // String[] strArr = new String[2];
    // strArr = strShipToId.split("|");
    // String RepId = strArr[0];
    // String PartyId = strArr[1];
    String strShipCarr = GmCommonClass.parseNull(request.getParameter("shipCarrier"));
    String strShipMode = GmCommonClass.parseNull(request.getParameter("shipMode"));
    String strAddressId = GmCommonClass.parseNull(request.getParameter("addressid"));
    String strShipId = GmCommonClass.parseNull(request.getParameter("shipId"));
    String strOverrideAttnTo = GmCommonClass.parseNull(request.getParameter("overrideAttnTo"));
    String strShipInstruction = GmCommonClass.parseNull(request.getParameter("shipInstruction"));
    log.debug("shipping values are " + strShipTo + " " + strShipToId + " " + strShipCarr + " "
        + strShipMode + " " + strAddressId);
    hmReturn.put("SHIPTO", strShipTo);
    hmReturn.put("SHIPTOID", strShipToId);
    hmReturn.put("SHIPCARRIER", strShipCarr);
    hmReturn.put("SHIPMODE", strShipMode);
    hmReturn.put("ADDRESSID", strAddressId);
    hmReturn.put("SHIPID", strShipId);
    hmReturn.put("OVERRIDEATTNTO", strOverrideAttnTo);
    hmReturn.put("SHIPINSTRUCTION", strShipInstruction);
    return hmReturn;
  }

  public GmShipDetailsForm loadDrpDwnValues(GmShipDetailsForm gmShipDetailsForm) throws Exception {
    // log.debug("inside GmShippingAction.GmShipDetailsForm action class");
    HashMap hmcboDetails = new HashMap();
    HashMap hmNames = new HashMap();
    HashMap hmValues = new HashMap();
    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean(getGmDataStoreVO());
    String strOpt = GmCommonClass.parseNull(gmShipDetailsForm.getStrOpt());
    if (strOpt.equalsIgnoreCase("AreaSet")) {
      HashMap hmParams = GmCommonClass.getHashMapFromForm(gmShipDetailsForm);
      hmcboDetails = gmShippingInfoBean.loadAreaSetShipping(hmParams);
    } else {
      hmcboDetails = gmShippingInfoBean.loadShipping();
    }

    gmShipDetailsForm.setAlShipTo((ArrayList) hmcboDetails.get("SHIPTO"));
    gmShipDetailsForm.setAlShipCarr((ArrayList) hmcboDetails.get("CARRIER"));
    gmShipDetailsForm.setAlShipMode((ArrayList) hmcboDetails.get("MODE"));
    hmNames = (HashMap) hmcboDetails.get("NAMES");
    gmShipDetailsForm.setHmNames(hmNames);
    // request.setAttribute("NAMES", hmNames);
    return gmShipDetailsForm;
  }

  public GmShipDetailsForm fetchShippingDetails(GmShipDetailsForm gmShipDetailsForm)
      throws Exception {
    GmShippingInfoBean gmShippingInfoBean = new GmShippingInfoBean(getGmDataStoreVO());
    GmAddressBean gmAddressBean = new GmAddressBean(getGmDataStoreVO());
    String strInactiveFl = "";
    HashMap hmValues = new HashMap();
    String strShipId = gmShipDetailsForm.getShipId();
    String strStrOpt = gmShipDetailsForm.getStrOpt();
    String strRefId = gmShipDetailsForm.getRefId();
    String strSource = gmShipDetailsForm.getSource();
    String strAsscTxnID = gmShipDetailsForm.getAsscTxnID();
    String strLoanerReqID = "";  //assigned to db data set to form for  PMT-42950
    String strAction = strStrOpt.equalsIgnoreCase("fetch") ? "fetch" : "";

    // modify - when clicked on Go from modify shipdetails screen -- will throw an error if rec not
    // found
    // fetch - rec may or may not be present. will not throw any error
    if (strStrOpt.equalsIgnoreCase("modify") || strStrOpt.equalsIgnoreCase("fetch")) {
      hmValues = gmShippingInfoBean.fetchShippingDetails(strRefId, strSource, strAction);
      //PMT#40555-tissue-part-shipping-validation[To fetch tissue flag details for modifying shipping details screen]
      String strhmapShipId = GmCommonClass.parseNull((String)hmValues.get("SHIPID"));
      String strTissueFlag=gmShippingInfoBean.fetchTissueFlag(strhmapShipId);
      hmValues.put("TISSUEFL", strTissueFlag);
   } else if (!strShipId.equals("") || strStrOpt.equalsIgnoreCase("report")) {
      hmValues = gmShippingInfoBean.fetchShippingDetailsByShipId(strShipId);
    //PMT#40555-tissue-part-shipping-validation
      String strTissueFlag=gmShippingInfoBean.fetchTissueFlag(strShipId);
      hmValues.put("TISSUEFL", strTissueFlag);
     }
    strAsscTxnID = GmCommonClass.parseNull((String)hmValues.get("ASSCTXNID"));//set the ASSCTXNID to string
    hmValues.put("ASSCTXNID", strAsscTxnID);
    gmShipDetailsForm.setAsscTxnID(GmCommonClass.parseNull(strAsscTxnID));//set the ASSCTXNID to Form
    gmShipDetailsForm.setStrOrderType(GmCommonClass.parseNull((String)hmValues.get("ORDER_TYPE")));
    gmShipDetailsForm =
        (GmShipDetailsForm) GmCommonClass.getFormFromHashMap(gmShipDetailsForm, hmValues);
    String strShipto = gmShipDetailsForm.getShipTo();
    String strShiptoId = gmShipDetailsForm.getNames();
    strShipId = GmCommonClass.parseNull(gmShipDetailsForm.getShipId());
    log.debug("strShipId " + strShipId);
    
    strLoanerReqID = GmCommonClass.parseNull((String)hmValues.get("LOANERREQUESTID"));//set the strLoanerReqID to string from  For PMT-42950
    gmShipDetailsForm.setLoanerReqId(strLoanerReqID);
    HashMap hmNames = new HashMap();
    hmNames = gmShipDetailsForm.getHmNames();
    // fetch the tracking numbers.
    if (!strShipId.equals("")) {
      gmShipDetailsForm.setAlTracknos(GmCommonClass.parseNullArrayList(gmShippingInfoBean
          .fetchTrackingNumbers(strShipId)));
    }

    if (strShipto.equals("4121")) {// Sales rep
      gmShipDetailsForm.setAlShipToId((ArrayList) hmNames.get("REPLIST"));
      gmShipDetailsForm.setAlAddress(gmAddressBean.fetchPartyAddress(strShiptoId, strInactiveFl));

    } else if (strShipto.equals("4120")) {// Distributor
      gmShipDetailsForm.setAlShipToId((ArrayList) hmNames.get("DISTLIST"));
    } else if (strShipto.equals("4122")) {// Hospital
      gmShipDetailsForm.setAlShipToId((ArrayList) hmNames.get("ACCLIST"));
    } else if (strShipto.equals("4123")) {// Employee
      gmShipDetailsForm.setAlShipToId((ArrayList) hmNames.get("EMPLIST"));
    } else if (strShipto.equals("4000642")) {// Shipping account
      gmShipDetailsForm.setAlShipToId(GmCommonClass.parseNullArrayList((ArrayList) hmNames
          .get("SHIPACCTLIST")));
    } else if (strShipto.equals("26240419")) {// Shipping Plant
      gmShipDetailsForm.setAlShipToId(GmCommonClass.parseNullArrayList((ArrayList) hmNames
          .get("PLANTLIST")));
    } else if (strShipto.equals("107801")) {// Dealer
      gmShipDetailsForm.setAlShipToId(GmCommonClass.parseNullArrayList((ArrayList) hmNames
          .get("DEALERLIST")));
    }
    return gmShipDetailsForm;
  }

  /**
   * loadAttribValue This method is merged from OUS side for attribute detail
   * 
   * @param HttpServletRequest request
   * @return HashMap
   * @exception AppError
   */
  public HashMap loadAttribValue(HttpServletRequest request) throws AppError {

    HashMap hmReturn = new HashMap();
    String strInputString = "";
    ArrayList alAttrib = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("LNATB"));
    for (int cnt = 0; cnt < alAttrib.size(); cnt++) {
      String strAttVal = GmCommonClass.parseNull(request.getParameter("ATTRIBVAL" + (cnt + 1)));
      String strhAttId = GmCommonClass.parseNull(request.getParameter("HATTRIBID" + (cnt + 1)));
      if (!strAttVal.equals(""))
        strInputString = strInputString + strhAttId + "^" + strAttVal + "|";
    }
    hmReturn.put("ATTINPSTR", strInputString);
    return hmReturn;
  }
  
  /**loadTissueShipReqParams - this method used to load the tissue Parts shipping Details
 * @param request
 * @return
 */
public HashMap loadTissueShipReqParams(HttpServletRequest request) {

	    HashMap hmTissueReturn = new HashMap();
	    String strTissueShipTo = GmCommonClass.parseNull(request.getParameter("tissueShipTo"));
	    String strTissueShipToId = GmCommonClass.parseNull(request.getParameter("tissueNames"));
	    String strTissueShipCarr = GmCommonClass.parseNull(request.getParameter("tissueShipCarrier"));
	    String strTissueShipMode = GmCommonClass.parseNull(request.getParameter("tissueShipMode"));
	    String strTissueAddressId = GmCommonClass.parseNull(request.getParameter("tissueAddressId"));
	    String strTissueShipId = GmCommonClass.parseNull(request.getParameter("tissueShipId"));
	    String strTissueOverrideAttnTo = GmCommonClass.parseNull(request.getParameter("tissueOverridenAttnTo"));
	    String strTissueShipInstruction = GmCommonClass.parseNull(request.getParameter("tissueShipInstruction"));
	    log.debug("strTissueShipId " + strTissueShipId);
	    log.debug("Tissue shipping values are " + strTissueShipTo + " " + strTissueShipToId + " " + strTissueShipCarr + " "
	        + strTissueShipMode + " " + strTissueAddressId);
	    hmTissueReturn.put("TISSUESHIPTO", strTissueShipTo);
	    hmTissueReturn.put("TISSUESHIPTOID", strTissueShipToId);
	    hmTissueReturn.put("TISSUESHIPCARRIER", strTissueShipCarr);
	    hmTissueReturn.put("TISSUESHIPMODE", strTissueShipMode);
	    hmTissueReturn.put("TISSUEADDRESSID", strTissueAddressId);
	    hmTissueReturn.put("TISSUESHIPID", strTissueShipId);
	    hmTissueReturn.put("TISSUEOVERRIDEATTNTO", strTissueOverrideAttnTo);
	    hmTissueReturn.put("TISSUESHIPINSTRUCTION", strTissueShipInstruction);
	    return hmTissueReturn;
	  }
}
