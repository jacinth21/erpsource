package com.globus.common.formbeans;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;

/**
 * @author vprasath
 */
public class GmCommonForm extends ActionForm
{
    private static final long serialVersionUID = 1234567890L;

    private String            action           = "";
    private String            strOpt           = "";
    private String            userId           = "";
    private String            txt_LogReason    = "";
    private ArrayList         alLogReasons  = new ArrayList();

    public void reset()
    {
        System.out.println("Inside SQL");
    }
    /**
     * @return Returns the action.
     */
    public String getAction()
    {
        return action;
    }

    /**
     * @param action
     *            The action to set.
     */
    public void setAction(String action)
    {
        this.action = action;
    }

    /**
     * @return Returns the strOpt.
     */
    public String getStrOpt()
    {
        return strOpt;
    }

    /**
     * @param strOpt
     *            The strOpt to set.
     */
    public void setStrOpt(String strOpt)
    {
        this.strOpt = strOpt;
    }

    /**
     * @return Returns the userId.
     */
    public String getUserId()
    {
        return userId;
    }
    /**
     * @param userId The userId to set.
     */
    public void setUserId(String userId)
    {
        this.userId = userId;
    }
    /**
     * @return Returns the txt_LogReason.
     */
    public String getTxt_LogReason()
    {
        return txt_LogReason;
    }

    /**
     * @param txt_LogReason The txt_LogReason to set.
     */
    public void setTxt_LogReason(String txt_LogReason)
    {
        this.txt_LogReason = txt_LogReason;
    }
    
    /**
     * @param request
     *            get all the session attributs required in the bean and set it to Action form
     */
    public void loadSessionParameters(HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        setUserId((String) session.getAttribute("strSessUserId"));
    }

    /**
     * @return Returns the alLogReasons.
     */
    public ArrayList getAlLogReasons()
    {
        return alLogReasons;
    }

    /**
     * @param alLogReasons The alLogReasons to set.
     */
    public void setAlLogReasons(ArrayList alLogReasons)
    {
        this.alLogReasons = alLogReasons;
    }
    
}
