package com.globus.common.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.globus.common.beans.GmLogger; 

import org.apache.commons.validator.routines.DateValidator;
import org.apache.log4j.Logger; 


public class GmCommonSort {

    Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
    static int ASC = 1;
    static int DESC = 2;
    
    /**
     *  sortArrayList - This method gets an ArrayList of HashMaps and sorts them as per the specified column in the order (ascending or descending) 
     *  @param alInputList
     *  @param strColumnName
     *  @param strOrder
     *  @return List 
     */  
    
    public static List sortArrayList(List list, final String strSortColumn, final String strVarType, final int intOrderType){
        Collections.sort(list, new Comparator() {
        public int compare(Object obj1, Object obj2) {
        
        HashMap hmTemp1 = (HashMap) obj1;
        HashMap hmTemp2 = (HashMap) obj2; 
        String strTempDate = "01/01/2003"; // If one of the date parameters is null, then it is assigned Jan 1 2003.

        String strVal1 = GmCommonClass.parseNull((String) hmTemp1.get(strSortColumn));
        String strVal2 = GmCommonClass.parseNull((String) hmTemp2.get(strSortColumn));
        
        if (strVarType.equals("String"))
        {
            if (intOrderType == ASC)
                return strVal1.compareTo(strVal2);
            else
                return strVal2.compareTo(strVal1);
        }
        
        else if (strVarType.equals("Date"))
        {
        	DateValidator dateVal = DateValidator.getInstance();
            int returnval = 0;
            
            strVal1 = strVal1.equals("") ? strTempDate : strVal1;
            strVal2 = strVal2.equals("") ? strTempDate : strVal2;

            Date firstDate = dateVal.validate(strVal1);
            Date secondDate = dateVal.validate(strVal2);
            
            if (intOrderType == ASC)
            	returnval =  dateVal.compareDates(firstDate,secondDate,null);
            else
            	returnval =  dateVal.compareDates(secondDate,firstDate,null);
            
            return returnval;
        }

        else 
        {
            double dblVal1 = Double.parseDouble(GmCommonClass.parseZero(strVal1));
            double dblVal2 = Double.parseDouble(GmCommonClass.parseZero(strVal2));
            int returnval = 0;
            
            if (dblVal1 == dblVal2)
            {
                returnval = 0;
            }

            else if (dblVal1 < dblVal2)
                {
                if (intOrderType == ASC)
                    returnval = -1;
                else
                    returnval = 1;
                }

            else if (dblVal1 > dblVal2)
            {
                if (intOrderType == ASC)
                    returnval = 1;
                else
                    returnval = -1;
            }

                return returnval;

        }
        }
        });
        return list;
        } 

}
