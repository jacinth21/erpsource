/**
 * This class contains methods that are common to all components
 * @author $Author:$
 * @version $Revision:$  <br>
 * $Date:$
 */

package com.globus.common.beans;

// Java Classes
import java.io.File;
import java.io.IOException; 
import java.util.HashMap; 

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException; 
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
 
public class GmAppErrorBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); 
  
	/**
	 * getApperrorMsg - This method will read the data from xml file
	 * 
	 * @return HashMap
	 * @exception SAXException, IOException, ParserConfigurationException
	 */ 
	
	public HashMap getApperrorMsg(String strXmlFile, String strRoot) throws SAXException, IOException, ParserConfigurationException
	{
		File file = new File(strXmlFile);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		HashMap masterhmap = new HashMap();

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = (Document) db.parse(file);
		doc.getDocumentElement().normalize();

		NodeList nodeRoot = doc.getElementsByTagName("APPERROR");
 
		Node nodeRootNode = nodeRoot.item(0);

			if (nodeRootNode.getNodeType() == Node.ELEMENT_NODE) {
				Element nodeRootNodeElem = (Element) nodeRootNode;
				NodeList nodeLst = nodeRootNodeElem.getElementsByTagName(strRoot);
				//getElementsByTagName method searches recursively through all descendant nodes of the current node searching for node elements with the specified name.
				for (int s = 0; s < nodeLst.getLength(); s++) {
					Node fstNode = nodeLst.item(s);

					if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
						
						Element nodeApp = (Element) fstNode;
						
						NodeList nodeMsgElmntLst = nodeApp.getElementsByTagName("MESSAGE");
						Element nodeMsgElmnt = (Element) nodeMsgElmntLst.item(0);
						NodeList nodeMsg = nodeMsgElmnt.getChildNodes();
						
						NodeList nodeNameElmntLst = nodeApp.getElementsByTagName("REASON");
						Element nodeReasonElmnt = (Element) nodeNameElmntLst.item(0);
						NodeList nodeReason = nodeReasonElmnt.getChildNodes();
						
						NodeList nodeActionElmntLst = nodeApp.getElementsByTagName("ACTION");
						Element nodeActionElmnt = (Element) nodeActionElmntLst.item(0);
						NodeList nodeAction = nodeActionElmnt.getChildNodes(); 
						
						masterhmap.put("MESSAGE", ((Node) nodeMsg.item(0)).getNodeValue());
						masterhmap.put("REASON", ((Node) nodeReason.item(0)).getNodeValue());
						masterhmap.put("ACTION", ((Node) nodeAction.item(0)).getNodeValue());
						 

					}
					 
				}
			}
	 

		return masterhmap; 
		
	}
	 

}  
