/**
 * FileName    : GmEmailProperties.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Apr 1, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.beans;

import java.io.Serializable;

import org.apache.log4j.Logger;

/**
 * @author sthadeshwar
 *
 */
public class GmEmailProperties implements Serializable {

//	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	public static final String MAIL_PROPERTIES_BUNDLE = "EmailTemplate";
	public static final String TO = "To";
	public static final String FROM = "From";
	public static final String CC = "Cc";
	public static final String BCC = "Bcc";
	
	public static final String MIME_TYPE = "MimeType";
	public static final String SUBJECT = "Subject";
	public static final String MESSAGE_BODY = "MessageBody";
	public static final String MESSAGE = "Message";
	public static final String MESSAGE_HEADER = "MessageHeader";
	public static final String MESSAGE_FOOTER = "MessageFooter";
	public static final String ATTACHMENT = "Attachment";  
	public static final String EMAIL_HEADER_NM = "EmailHeaderName";
	
	private String to;
	private String from;
	
	private String cc ="";
	private String bcc;
	private String mimeType;
	private String subject;
	private String message;
	private String messageFooter;
	private String token = ",";
	private String attachment ="";
	private String replyTo ="";
	private String startTime = "";
	private String endime = "";
	private String eventItemId = "";
	private String location = "";
	private String category = "";		
	private String notifyfl = "";
	private String emailHeaderName = "";

	
	
	public String getNotifyfl() {
		return notifyfl;
	}
	public void setNotifyfl(String notifyfl) {
		this.notifyfl = notifyfl;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndime() {
		return endime;
	}
	public void setEndime(String endime) {
		this.endime = endime;
	}
	public String getEventItemId() {
		return eventItemId;
	}
	public void setEventItemId(String eventItemId) {
		this.eventItemId = eventItemId;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	
	/**
	 * @return the to
	 */
	public String[] getReplyTo() {
		if(replyTo != null && !replyTo.equals(""))
		{	
			replyTo = replyTo.replaceAll(";", ",");
			replyTo = replyTo.replaceAll(" ", "");
			if(replyTo.endsWith(","))
				replyTo = replyTo.substring(0, replyTo.lastIndexOf(","));			
			return replyTo.split(token+"+");
		}
		else
			return null;
	}
	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}
	
	
	public String toString()
	{
		return "To = " + to + " | From = " + from + " | Cc = " + cc + " | Bcc = " + bcc + " | Mime = " + mimeType + " | Subject = " + subject;
	}
	/**
	 * @return the to
	 */
	public String[] getRecipients() {
		if(to != null && !to.equals(""))
		{	
				to = to.replaceAll(";", ",");
				to = to.replaceAll(" ", "");
			if(to.endsWith(","))
				to = to.substring(0, to.lastIndexOf(","));
			return to.split(token+"+");
		}
		else
			return null;
	}
	/**
	 * @param to the to to set
	 */
	public void setRecipients(String to) {
		this.to = to;
	}
	/**
	 * @return the from
	 */
	public String getSender() {
		return from;
	}
	/**
	 * @param from the from to set
	 */
	public void setSender(String from) {
		//When having more than one Email ID it enters this section 
		if(from.indexOf(",")>0){
            int intCommaPos = from.indexOf(",");
			if(intCommaPos>from.indexOf("@")){
				 String[] mailID = from.split(",");
				 from = mailID[0];//When having more than one EmailID we are considering First Email ID
			}
		}
		this.from = from;
	}
	/**
	 * @return the cc
	 */
	public String[] getCc() {
		if(cc != null && !cc.equals(""))
		{
			cc = cc.replaceAll(";", ",");
			cc = cc.replaceAll(" ", "");
			if(cc.endsWith(","))
				cc = cc.substring(0, cc.lastIndexOf(","));
			return cc.split(token+"+");
		}
		else
			return null;
	}
	
	/**
	 * @return the cc
	 */
	public String getCcString() {		
			return cc;
	}
	/**
	 * @param cc the cc to set
	 */
	public void setCc(String cc) {
		if (cc != null && !cc.trim().equals("")) 
		{
			this.cc = cc + "," + this.cc;
			
			 if(this.cc.endsWith(","))
				 this.cc = this.cc.substring(0, this.cc.lastIndexOf(","));			 
		}
		
	
	}
	/**
	 * @return the bcc
	 */
	public String[] getBcc() {
		if(bcc != null && !bcc.equals(""))
		{
			bcc = bcc.replaceAll(";", ",");
			bcc = bcc.replaceAll(" ", "");
			 if(bcc.endsWith(","))
				 bcc = bcc.substring(0, bcc.lastIndexOf(","));					 
			return bcc.split(token+"+");
		}
		else
			return null;
	}
	/**
	 * @param bcc the bcc to set
	 */
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		if(mimeType == null || mimeType.equals(""))
			return "text/plain";
		else
			return mimeType;
	}
	/**
	 * @param mimeType the mimeType to set
	 */
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the messageBody
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param messageBody the messageBody to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return the messageFooter
	 */
	public String getMessageFooter() {
		return messageFooter;
	}
	/**
	 * @param messageFooter the messageFooter to set
	 */
	public void setMessageFooter(String messageFooter) {
		this.messageFooter = messageFooter;
	}
	/**
	 * @return the emailHeaderName
	 */
	public String getEmailHeaderName() {
		return emailHeaderName;
	}
	/**
	 * @param emailHeaderName the emailHeaderName to set
	 */
	public void setEmailHeaderName(String emailHeaderName) {
		this.emailHeaderName = emailHeaderName;
	}
	
	
	

}
