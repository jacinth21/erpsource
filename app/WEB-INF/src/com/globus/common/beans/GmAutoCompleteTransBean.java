package com.globus.common.beans;

import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.prodmgmnt.beans.GmSetMapAutoCompleteRptBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.quality.beans.GmSetBundleMappingBean;
public class GmAutoCompleteTransBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * @param gmDataStoreVO
   * @throws AppError
   */

  public GmAutoCompleteTransBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
  }


  /**
   * loadFieldSalesMasterData - Sync field sales information to Redis server.
   * 
   * @throws AppError
   */
  public void syncFieldSalesMasterData(HashMap hmparam) throws AppError {
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strId = GmCommonClass.parseNull((String) hmparam.get("ID"));
    String strOldName = GmCommonClass.parseNull((String) hmparam.get("OLD_NM"));
    String strNewName = GmCommonClass.parseNull((String) hmparam.get("NEW_NM"));
    log.debug(" hmparam " + hmparam);
    GmCacheManager gmCacheManager = new GmCacheManager();
    String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DIST_LIST", "Y");
    String strNewDataFl =
        gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
    // if new distributor added then reinsert the data.
    if (strNewDataFl.equalsIgnoreCase("Y")) {
      GmCacheManager gmTmpCacheManager = new GmCacheManager();
      gmTmpCacheManager.setKey(strKey);
      gmTmpCacheManager.setSearchPrefix("Glo");
      gmTmpCacheManager.setMaxFetchCount(5);
      gmTmpCacheManager.returnJsonString(gmAutoCompleteReportBean
          .getDistributorQueryStr(new HashMap()));
    }
    // to remove the A/R Dist list from cache server
    gmCacheManager.removePatternKeys("AR_DIST_LIST*" + getCompId() + "*");
  }


  /**
   * syncSalesSetupMasterData - Sync sales setup information to Redis server.
   * 
   * @throws AppError
   */
  public void syncSalesRepMasterData(HashMap hmparam) throws AppError {
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());

    String strId = GmCommonClass.parseNull((String) hmparam.get("ID"));
    String strOldName = GmCommonClass.parseNull((String) hmparam.get("OLD_NM"));
    String strNewName = GmCommonClass.parseNull((String) hmparam.get("NEW_NM"));
    String strOldActiveFl = GmCommonClass.parseNull((String) hmparam.get("OLD_ACTIVE_FL"));
    String strNewActiveFl = GmCommonClass.parseNull((String) hmparam.get("NEW_ACTIVE_FL"));
    String strOldCatgryId = GmCommonClass.parseNull((String) hmparam.get("OLD_CATGRY"));
    String strNewCatgryId = GmCommonClass.parseNull((String) hmparam.get("NEW_CATGRY"));
    log.debug(" hmparam " + hmparam);
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("REP_LIST", "Y");
    String strNewDataFl =
        gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
    // if new distributor added then reinsert the data.
    if (strNewDataFl.equalsIgnoreCase("Y")) {
      GmCacheManager gmTmpCacheManager = new GmCacheManager(getGmDataStoreVO());
      gmTmpCacheManager.setKey(strKey);
      gmTmpCacheManager.setSearchPrefix("Glo");
      gmTmpCacheManager.setMaxFetchCount(5);
      gmTmpCacheManager.returnJsonString(gmAutoCompleteReportBean
          .getSalesRepQueryStr(new HashMap()));
    }
    // updating the sales rep data to redis server when changing active/inactive or category
    if (!strOldActiveFl.equals(strNewActiveFl) || !strOldCatgryId.equals(strNewCatgryId)) {
      syncSalesRepActiveData(hmparam);

    }
  }


  /**
   * syncSalesSetupMasterData - Sync sales setup information to Redis server.
   * 
   * @throws AppError
   */
  public void syncSalesRepActiveData(HashMap hmparam) throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    GmAutoCompletePartyListRptBean gmAutoCompletePartyListRptBean =
        new GmAutoCompletePartyListRptBean(getGmDataStoreVO());
    String strKey = "";
    String strUSCompanyStr =
        GmCommonClass.parseNull(GmCommonClass.getString("EVENTLOANTONAMECOMP"));
    String strCompanyLocale = getCompId();
    int intCmpnyCheck = strUSCompanyStr.indexOf("|" + strCompanyLocale + "|");
    if (intCmpnyCheck == -1) {
      // Removing the strkey and adding the key to redis server.
      strKey = "GETOUSREPLISTFOREVENT";
      gmCacheManager.removeKey(strKey);
      gmAutoCompletePartyListRptBean.getOUSSalesList("Y");
    } else {
      // /Removing the strkey and adding the key to redis server
      strKey = "GETUSREPLISTFOREVENT";
      gmCacheManager.removeKey(strKey);
      gmAutoCompletePartyListRptBean.getUSRepList("Y");
    }

  }


  /**
   * syncRepAccMasterData - Sync Rep Account information to Redis server.
   * 
   * @throws AppError
   */
  public void syncRepAccMasterData(HashMap hmparam) throws AppError {
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strId = GmCommonClass.parseNull((String) hmparam.get("ID"));
    String strOldName = GmCommonClass.parseNull((String) hmparam.get("OLD_NM"));
    String strNewName = GmCommonClass.parseNull((String) hmparam.get("NEW_NM"));
    String strOAccType = GmCommonClass.parseNull((String) hmparam.get("OLD_TYPE"));
    String strAccType = GmCommonClass.parseNull((String) hmparam.get("NEW_TYPE"));
    HashMap hmRuleData = new HashMap();
    String strCompanyID = "";
    if (!strOldName.equals(strNewName)) {
      GmCacheManager gmCacheManager = new GmCacheManager();
      String strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("REP_ACCOUNT_LIST", "Y");
      String strNewDataFl =
          gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
      // if new distributor added then reinsert the data.
      if (strNewDataFl.equalsIgnoreCase("Y")) {
        GmCacheManager gmTmpCacheManager = new GmCacheManager();
        gmTmpCacheManager.setKey(strKey);
        gmTmpCacheManager.setSearchPrefix("Glo");
        gmTmpCacheManager.setMaxFetchCount(5);
        gmTmpCacheManager.returnJsonString(gmAutoCompleteReportBean
            .getRepAccQueryStr(new HashMap()));
      } 
      // If the account name is modified or created newly, then need to add it in the below redis
      // key
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("REP_ACCOUNT_LIST_BY_PLANT", "Y");
      strNewDataFl = gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
      // if new account is added then reinsert the data.
      if (strNewDataFl.equalsIgnoreCase("Y")) {
        GmCacheManager gmTmpCacheManager = new GmCacheManager();
        gmTmpCacheManager.setKey(strKey);
        gmTmpCacheManager.setSearchPrefix("Glo");
        gmTmpCacheManager.setMaxFetchCount(5);
        // to get the plant id (based on company)
        strCompanyID = getCompId();
        hmRuleData.put("RULEID", strCompanyID);
        hmRuleData.put("RULEGROUP", "LOANERINITFILTER");
        String strCompPlantFl = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData));
        hmRuleData.put("PLANT_LEVEL_ACC", strCompPlantFl);
        gmTmpCacheManager.returnJsonString(gmAutoCompleteReportBean.getRepAccQueryStr(hmRuleData));
      }
      // to remove the A/R account list
      gmCacheManager.removePatternKeys("AR_REP_ACCOUNT_LIST*" + getCompId() + "*");
    }
    
    //PMT-39047-Checks if the new Old acc type is not equal to new acc type
    else if (!strOAccType.equals(strAccType)) {
  	 GmCacheManager gmCacheManager = new GmCacheManager();
     gmCacheManager.removePatternKeys("AR_REP_ACCOUNT_LIST*" + getCompId() + "*");
    }
}

  /**
   * syncPartyMasterData - Sync Party information to Redis server.
   * 
   * @throws AppError
   */
  public void syncPartyMasterData(HashMap hmparam) throws AppError {
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strId = GmCommonClass.parseNull((String) hmparam.get("ID"));
    String strOldName = GmCommonClass.parseNull((String) hmparam.get("OLD_NM"));
    String strNewName = GmCommonClass.parseNull((String) hmparam.get("NEW_NM"));
    String strPartyType = GmCommonClass.parseNull((String) hmparam.get("PARTY_TYPE"));
    String strKey = "";
    if (!strOldName.equals(strNewName)) {
      GmCacheManager gmCacheManager = new GmCacheManager();
      if (strPartyType.equals("4000877")) // 4000877 - Parent Account Account
      {
        strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("PARENT_ACCOUNT_LIST", "Y");
      } else if (strPartyType.equals("26230725")) { // 26230725 - Dealer
        strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DEALER_LIST", "Y");
        // to remove the Dealer list from cache server
        gmCacheManager.removePatternKeys("AR_DEALER_LIST*" + getCompId() + "*");
      } else if (strPartyType.equals("7003")) { // 7003 - Group Price Book
        strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("GROUP_PRICE_BOOK_LIST", "Y");
      } else if (strPartyType.equals("7010")) { // 7010 - IDN

        strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("IDN_LIST", "Y");
      } else if (strPartyType.equals("7011")) { // 7011 - GPO
        strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("GPO_LIST", "Y");
      }
      String strNewDataFl =
          gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
      // if new distributor added then reinsert the data.
      if (strNewDataFl.equalsIgnoreCase("Y")) {
        GmCacheManager gmTmpCacheManager = new GmCacheManager();
        gmTmpCacheManager.setKey(strKey);
        gmTmpCacheManager.setSearchPrefix("Glo");
        gmTmpCacheManager.setMaxFetchCount(5);
        gmTmpCacheManager.returnJsonString(gmAutoCompleteReportBean.getPartyQueryStr(strPartyType));
      }
    }

  }

  /**
   * syncSalesSetupMasterData - Sync SET/BOM setup information to Redis server.
   * 
   * @throws AppError
   */
  public void syncSetBomMasterData(HashMap hmparam) throws AppError {
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
        new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());

    String strId = GmCommonClass.parseNull((String) hmparam.get("ID"));
    String strOldName = GmCommonClass.parseNull((String) hmparam.get("OLD_NM"));
    String strNewName = GmCommonClass.parseNull((String) hmparam.get("NEW_NM"));
    String strType = GmCommonClass.parseNull((String) hmparam.get("TYPE"));
    String strKey = "";
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    if (strType.equals("1601")) {// Set
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SET_LST", "Y");
    } else {
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("BOM_LST", "Y");
    }
    String strNewDataFl =
        gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
    // if new SET or BOM is created, need to update the Redis Cache
    if (strNewDataFl.equalsIgnoreCase("Y")) {
      GmCacheManager gmTmpCacheManager = new GmCacheManager(getGmDataStoreVO());
      gmTmpCacheManager.setKey(strKey);
      gmTmpCacheManager.setSearchPrefix("Glo");
      gmTmpCacheManager.setMaxFetchCount(5);
      gmTmpCacheManager.returnJsonString(gmSetMapAutoCompleteRptBean.getLoadSetMap(hmparam));
    }
  }

  /**
   * syncProjectMasterData - Sync project setup information to Redis server.
   * 
   * @throws AppError
   */
  public void syncProjectMasterData(HashMap hmparam) throws AppError {
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
        new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());

    String strId = GmCommonClass.parseNull((String) hmparam.get("ID"));
    String strOldName = GmCommonClass.parseNull((String) hmparam.get("OLD_NM"));
    String strNewName = GmCommonClass.parseNull((String) hmparam.get("NEW_NM"));
    String strKey = "";
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("PROJ_LST", "Y");

    String strNewDataFl =
        gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
    // if new Project is created or project name is changed, need to update the Redis Cache
    if (strNewDataFl.equalsIgnoreCase("Y")) {
      GmCacheManager gmTmpCacheManager = new GmCacheManager(getGmDataStoreVO());
      gmTmpCacheManager.setKey(strKey);
      gmTmpCacheManager.setSearchPrefix("Glo");
      gmTmpCacheManager.setMaxFetchCount(5);
      hmparam.put("COLUMN", "ID");
      hmparam.put("STATUSID", "20301");// Approved
      gmTmpCacheManager.returnJsonString(gmSetMapAutoCompleteRptBean
          .loadProjectNameQueryStr(hmparam));
    }
  }

  /**
   * syncInspectionSheetMasterData - Sync Inspection sheet information to Redis server.
   * 
   * @throws AppError
   */
  public void syncInspectionSheetMasterData(HashMap hmparam) throws AppError {
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
        new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());

    String strId = GmCommonClass.parseNull((String) hmparam.get("ID"));
    String strOldName = GmCommonClass.parseNull((String) hmparam.get("OLD_NM"));
    String strNewName = GmCommonClass.parseNull((String) hmparam.get("NEW_NM"));
    String strKey = "";
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("INSP_SHEET_LIST", "Y");

    String strNewDataFl =
        gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
    // if new inspection sheet is created or inspection sheet name is changed, need to update the
    // Redis Cache
    if (strNewDataFl.equalsIgnoreCase("Y")) {
      GmCacheManager gmTmpCacheManager = new GmCacheManager(getGmDataStoreVO());
      gmTmpCacheManager.setKey(strKey);
      gmTmpCacheManager.setSearchPrefix("Glo");
      gmTmpCacheManager.setMaxFetchCount(5);
      hmparam.put("ARTIFACTSTYPE", "91184");
      gmTmpCacheManager.returnJsonString(gmSetMapAutoCompleteRptBean
          .loadInspectionSheetListQueryStr(hmparam));
    }
  }

  /**
   * 
   * saveDetailsToCache - This method used to save the details to cache server
   * 
   * @param hmCacheDetails
   * @throws AppError
   */
  public void saveDetailsToCache(HashMap hmCacheDetails) throws AppError {
    String strCacheQueueName = GmCommonClass.getJmsConfig("JMS_CACHE_QUEUE");
    log.debug("strCacheQueueName>>"+strCacheQueueName);
    // to call the cache (in JMS)
    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
    String strCacheConsumerClass =
        GmCommonClass
            .parseNull(GmJMSConsumerConfigurationBean.getJmsConfig("REDIS_CONSUMER_CLASS"));
    hmCacheDetails.put("CONSUMERCLASS", strCacheConsumerClass);
    hmCacheDetails.put("QUEUE_NAME", strCacheQueueName);
    gmConsumerUtil.sendMessage(hmCacheDetails);
  }

  /**
   * syncCodeLookupMasterData - to remove the code lookup keys
   * 
   * @param hmParam
   * @throws AppError
   */
  public void syncCodeLookupMasterData(HashMap hmParam) throws AppError {
    String strCodeGrp = GmCommonClass.parseNull((String) hmParam.get("CODE_GRP"));
    // To append comma(,) with Code Group because Code Groups split based on comma.
    strCodeGrp = strCodeGrp + ",";
    StringTokenizer strToken = new StringTokenizer(strCodeGrp, ",");
    while (strToken.hasMoreTokens()) {
      String strCodeGroup = (String) strToken.nextElement();
      log.debug(" remove the data to cache " + strCodeGroup);
      if (!strCodeGroup.equals("")) {
        GmCacheManager gmCacheManager = new GmCacheManager();
        gmCacheManager.removePatternKeys(strCodeGroup);
      }
    }


  }

  /**
   * syncCodeLookupMasterData - to remove the code lookup keys
   * 
   * @param hmParam
   * @throws AppError
   */
  public void syncDemandSheetMasterData(HashMap hmParam) throws AppError {
    String strCodeGrp = GmCommonClass.parseNull((String) hmParam.get("CODE_GRP"));
    log.debug(" remove the data to cache " + strCodeGrp);
    if (!strCodeGrp.equals("")) {
      GmCacheManager gmCacheManager = new GmCacheManager();
      gmCacheManager.removePatternKeys(strCodeGrp);
    }

    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean(getGmDataStoreVO());

    String strId = GmCommonClass.parseNull((String) hmParam.get("ID"));
    String strOldName = GmCommonClass.parseNull((String) hmParam.get("OLD_NM"));
    String strNewName = GmCommonClass.parseNull((String) hmParam.get("NEW_NM"));
    //
    String strFilterInActive = GmCommonClass.parseNull((String) hmParam.get("FILTER_INACTIVE"));
    String strExcludeTemplate = GmCommonClass.parseNull((String) hmParam.get("EXCLUDE_TEMPLATE"));

    String strKey = "";
    if (!strOldName.equals(strNewName)) {
      GmCacheManager gmCacheManager = new GmCacheManager();
      log.debug(" ALL ");
      gmCacheManager.setKey(strKey);
      gmCacheManager.removePatternKeys("EDIT_DEMANDSHEET");
      // 1. all values
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DEMAND_SHEET_SETUP_ALL", "Y");
      String strNewDataFl =
          gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
      // insert the new keys
      if (strNewDataFl.equalsIgnoreCase("Y")) {
        GmCacheManager gmTmpCacheManager = new GmCacheManager();
        gmTmpCacheManager.setKey(strKey);
        gmTmpCacheManager.setSearchPrefix("Glo");
        gmTmpCacheManager.setMaxFetchCount(5);
        gmTmpCacheManager.returnJsonString(gmDemandSetupBean.loadDemandSheetStr(hmParam));
      }
      log.debug(" INACTIVE ");
      // 2. inactive key set
      hmParam.put("FILTER_INACTIVE", "YES");
      strNewDataFl = gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
      // insert the new keys
      if (strNewDataFl.equalsIgnoreCase("Y")) {
        GmCacheManager gmTmpCacheManager = new GmCacheManager();
        gmTmpCacheManager.setKey(strKey);
        gmTmpCacheManager.setSearchPrefix("Glo");
        gmTmpCacheManager.setMaxFetchCount(5);
        gmTmpCacheManager.returnJsonString(gmDemandSetupBean.loadDemandSheetStr(hmParam));
      }
      hmParam.put("FILTER_INACTIVE", "");
      // 3. exclude template
      log.debug(" EXCLUDE TEMPLATE ");
      hmParam.put("EXCLUDE_TEMPLATE", "YES");
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DEMAND_SHEET_SETUP_EXCLUDE", "Y");
      strNewDataFl = gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
      // insert the new keys
      if (strNewDataFl.equalsIgnoreCase("Y")) {
        GmCacheManager gmTmpCacheManager = new GmCacheManager();
        gmTmpCacheManager.setKey(strKey);
        gmTmpCacheManager.setSearchPrefix("Glo");
        gmTmpCacheManager.setMaxFetchCount(5);
        gmTmpCacheManager.returnJsonString(gmDemandSetupBean.loadDemandSheetStr(hmParam));
      }
      log.debug(" End the syncDemandSheetMasterData");
    }

  }

  /**
   * syncTTPMasterData - Sync TTP information to Redis server.
   * 
   * @param hmParam
   * @throws AppError
   */

  public void syncTTPMasterData(HashMap hmparam) throws AppError {
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    String strId = GmCommonClass.parseNull((String) hmparam.get("ID"));
    String strOldName = GmCommonClass.parseNull((String) hmparam.get("OLD_NM"));
    String strNewName = GmCommonClass.parseNull((String) hmparam.get("NEW_NM"));
    String strOpt = GmCommonClass.parseNull((String) hmparam.get("STROPT"));
    GmCacheManager gmCacheManager = null;
    String strKey = "";
    if (strOpt.equals("GOP")) {
      gmCacheManager = new GmCacheManager(GmDNSNamesEnum.DMWEBGLOBUS);
      gmCacheManager.removePatternKeys("LOADTTPLIST:AUTO");
      strKey = "GOP_TTP_LIST";
    } else {
      gmCacheManager = new GmCacheManager();
      strKey = "TTP_LIST";
    }
    String strNewDataFl =
        gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
    // insert the new keys.
    if (strNewDataFl.equalsIgnoreCase("Y")) {
      gmCacheManager.setKey(strKey);
      gmCacheManager.setSearchPrefix("Glo");
      gmCacheManager.setMaxFetchCount(5);
      gmCacheManager.returnJsonString(gmAutoCompleteReportBean.loadTTPListStr(hmparam));
    }

  }
  
  /**
   * syncSetIdMasterData - Sync SET-ID setup information to Redis server.
   * 
   * @throws AppError
   */
  public void syncSetIdMasterData(HashMap hmparam) throws AppError {
    GmAutoCompleteReportBean gmAutoCompleteReportBean =
        new GmAutoCompleteReportBean(getGmDataStoreVO());
    GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
        new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());

    String strId = GmCommonClass.parseNull((String) hmparam.get("ID"));
    String strOldName = GmCommonClass.parseNull((String) hmparam.get("OLD_NM"));
    String strNewName = GmCommonClass.parseNull((String) hmparam.get("NEW_NM"));
    String strType = GmCommonClass.parseNull((String) hmparam.get("TYPE"));
    String strKey = "";
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
      strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SET_ID_LST", "Y");
    String strNewDataFl =
        gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
    // if new SET is created, need to update the Redis Cache
    if (strNewDataFl.equalsIgnoreCase("Y")) {
      GmCacheManager gmTmpCacheManager = new GmCacheManager(getGmDataStoreVO());
      gmTmpCacheManager.setKey(strKey);
      gmTmpCacheManager.setSearchPrefix("Glo");
      gmTmpCacheManager.setMaxFetchCount(5);
      gmTmpCacheManager.returnJsonString(gmSetMapAutoCompleteRptBean.getLoadSetIdMap(hmparam));
    }
  }
  /**
   * syncNPIDetails - syncNPIDetails to fetch the valid records in cache.
   * 
   * @param hmParam
   * @throws AppError
   */

public void syncNPIDetails(HashMap hmParam) {
	GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
   String strId = GmCommonClass.parseNull((String) hmParam.get("ID"));
   String strOldName = GmCommonClass.parseNull((String) hmParam.get("OLD_NM"));
   String strNewName = GmCommonClass.parseNull((String) hmParam.get("NEW_NM"));
   GmCacheManager gmCacheManager = new GmCacheManager();
   String  strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("NPI_LIST", "Y");
   hmParam.put("STROPT","npilist");
   String strNewDataFl =
       gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
   // if new npilist added then reinsert the data.
   if (strNewDataFl.equalsIgnoreCase("Y")) {
     GmCacheManager gmTmpCacheManager = new GmCacheManager();
     gmTmpCacheManager.setKey(strKey);
     gmTmpCacheManager.setSearchPrefix("Glo");
     gmTmpCacheManager.setMaxFetchCount(5);
     gmTmpCacheManager.returnJsonString(gmAutoCompleteReportBean.getLoadSurgeonNameQueryStr(hmParam));
   }
   strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SURGEON_LIST", "Y");
   hmParam.put("STROPT","");
   strNewDataFl =gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
   // if new surgeon added then reinsert the data.
   if (strNewDataFl.equalsIgnoreCase("Y")) {
     GmCacheManager gmTmpCacheManager = new GmCacheManager();
     gmTmpCacheManager.setKey(strKey);
     gmTmpCacheManager.setSearchPrefix("Glo");
     gmTmpCacheManager.setMaxFetchCount(5);
     gmTmpCacheManager.returnJsonString(gmAutoCompleteReportBean.getLoadSurgeonNameQueryStr(hmParam));
   }
   strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("NPI_PARTY_LIST", "Y");
   hmParam.put("STROPT","npilist");
   strNewDataFl = gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
   // if new npilist added then reinsert the data.
   if (strNewDataFl.equalsIgnoreCase("Y")) {
     GmCacheManager gmTmpCacheManager = new GmCacheManager();
     gmTmpCacheManager.setKey(strKey);
     gmTmpCacheManager.setSearchPrefix("Glo");
     gmTmpCacheManager.setMaxFetchCount(5);
     gmTmpCacheManager.returnJsonString(gmAutoCompleteReportBean.getLoadSurgeonNameWIdQueryStr(hmParam));
   }
   strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SURGEON_PARTY_LIST", "Y");
   hmParam.put("STROPT","");
   strNewDataFl =gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
   // if new surgeon added then reinsert the data.
   if (strNewDataFl.equalsIgnoreCase("Y")) {
     GmCacheManager gmTmpCacheManager = new GmCacheManager();
     gmTmpCacheManager.setKey(strKey);
     gmTmpCacheManager.setSearchPrefix("Glo");
     gmTmpCacheManager.setMaxFetchCount(5);
     gmTmpCacheManager.returnJsonString(gmAutoCompleteReportBean.getLoadSurgeonNameWIdQueryStr(hmParam));
   }
	
}
/**
 * synGroupList - synGroupList to fetch the valid records in cache.
 * 
 * @param hmParam
 * @throws AppError
 */
public void synGroupList(HashMap hmParam) {
	
	GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
   String strId = GmCommonClass.parseNull((String) hmParam.get("ID"));
   String strOldName = GmCommonClass.parseNull((String) hmParam.get("OLD_NM"));
   String strNewName = GmCommonClass.parseNull((String) hmParam.get("NEW_NM"));
   GmCacheManager gmCacheManager = new GmCacheManager();
   String  strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("DEMAND_GROUP_LIST", "Y");
   String strNewDataFl =gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
   
   if (strNewDataFl.equalsIgnoreCase("Y")) {                 
     GmCacheManager gmTmpCacheManager = new GmCacheManager();
     gmTmpCacheManager.setKey(strKey);
     gmTmpCacheManager.setSearchPrefix("Glo");
     gmTmpCacheManager.setMaxFetchCount(100);
     gmTmpCacheManager.returnJsonString(gmAutoCompleteReportBean.fetchDemandGroupList(hmParam));
   }
  
}
/**
 * synSetBundleList - synSetBundleList to fetch the valid records in cache.
 * 
 * @param hmParam
 * @throws AppError
 */
public void synSetBundleList(HashMap hmParam) {

   GmSetBundleMappingBean gmSetBundle = new GmSetBundleMappingBean(getGmDataStoreVO());
   GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
   String strId = GmCommonClass.parseNull((String) hmParam.get("ID"));
   String strOldName = GmCommonClass.parseNull((String) hmParam.get("OLD_NM"));
   String strNewName = GmCommonClass.parseNull((String) hmParam.get("NEW_NM"));
   GmCacheManager gmCacheManager = new GmCacheManager();
   String  strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SET_BUNDLE_NAME_LIST", "Y");
   String strNewDataFl =gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
   
   if (strNewDataFl.equalsIgnoreCase("Y")) {                 
     GmCacheManager gmTmpCacheManager = new GmCacheManager();
     gmTmpCacheManager.setKey(strKey);
     gmTmpCacheManager.setSearchPrefix("Glo");
     gmTmpCacheManager.setMaxFetchCount(100);
     gmTmpCacheManager.returnJsonString(gmSetBundle.fetchSetBundleNmList(hmParam));
   }  
  }
/**
 * synSetBundleNameKeyList - synSetBundleNameKeyList to fetch the valid records in cache.
 * 
 * @param hmParam
 * @throws AppError
 */
public void synSetBundleNameKeyList(HashMap hmParam) {
   GmSetBundleMappingBean gmSetBundle = new GmSetBundleMappingBean(getGmDataStoreVO());
   GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
   String strId = GmCommonClass.parseNull((String) hmParam.get("ID"));
   String strOldName = GmCommonClass.parseNull((String) hmParam.get("OLD_NM"));
   String strNewName = GmCommonClass.parseNull((String) hmParam.get("NEW_NM"));
   GmCacheManager gmCacheManager = new GmCacheManager();
   String  strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("SET_BUNDLE_LIST", "Y");
   String strNewDataFl =gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
   
   if (strNewDataFl.equalsIgnoreCase("Y")) {                 
     GmCacheManager gmTmpCacheManager = new GmCacheManager();
     gmTmpCacheManager.setKey(strKey);
     gmTmpCacheManager.setSearchPrefix("Glo");
     gmTmpCacheManager.setMaxFetchCount(100);
     gmTmpCacheManager.returnJsonString(gmSetBundle.fetchSetBundleNameList(hmParam));
   }  
  }

public void synKitNameList(HashMap hmParam) {
	   GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
	   GmAutoCompleteReportBean gmAutoCompleteReportBean = new GmAutoCompleteReportBean(getGmDataStoreVO());
	   String strId = GmCommonClass.parseNull((String) hmParam.get("ID"));
	   String strOldName = GmCommonClass.parseNull((String) hmParam.get("OLD_NM"));
	   String strNewName = GmCommonClass.parseNull((String) hmParam.get("NEW_NM"));
	   GmCacheManager gmCacheManager = new GmCacheManager();
	   String  strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("KIT_NAME_LIST", "Y");
	   String strNewDataFl =gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);  
	   String  strKeys = gmAutoCompleteReportBean.getAutoCompleteKeys("KIT_NAME_ACTIVE_LIST_"+getCompId(), "Y");
	   String strNewDataFlval =gmCacheManager.updateSortedSetInCache(strKeys, strOldName, strNewName, strId);
	   
	   if (strNewDataFl.equalsIgnoreCase("Y")) {                 
	     GmCacheManager gmTmpCacheManager = new GmCacheManager();
	     gmTmpCacheManager.setKey(strKey);
	     gmTmpCacheManager.setSearchPrefix("Glo");
	     gmTmpCacheManager.setMaxFetchCount(100);
	     gmTmpCacheManager.returnJsonString(gmKitMappingBean.fetchSetNmList(hmParam));
	   }  
	  }


/**
 * syncLoanerSetDetails - Sync Loaner Set Name List information to Redis server.
 * 
 * @throws AppError
 */
public void syncLoanerSetDetails(HashMap hmparam) throws AppError {
  GmAutoCompleteReportBean gmAutoCompleteReportBean =
      new GmAutoCompleteReportBean(getGmDataStoreVO());
  GmSetMapAutoCompleteRptBean gmSetMapAutoCompleteRptBean =
      new GmSetMapAutoCompleteRptBean(getGmDataStoreVO());

  String strId = GmCommonClass.parseNull((String) hmparam.get("ID"));
  String strOldName = GmCommonClass.parseNull((String) hmparam.get("OLD_NM"));
  String strNewName = GmCommonClass.parseNull((String) hmparam.get("NEW_NM"));
  String strType = GmCommonClass.parseNull((String) hmparam.get("TYPE"));
  String strKey = "";
  String strPlantId = GmCommonClass.parseNull(getGmDataStoreVO().getPlantid());

  GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
  
  strKey = gmAutoCompleteReportBean.getAutoCompleteKeys("PRODUCT_LOANER_SET_"+strPlantId, "");
  
  String strNewDataFl =
      gmCacheManager.updateSortedSetInCache(strKey, strOldName, strNewName, strId);
  // if new SET or BOM is created, need to update the Redis Cache
  if (strNewDataFl.equalsIgnoreCase("Y")) {
    GmCacheManager gmTmpCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmTmpCacheManager.setKey(strKey);
    gmTmpCacheManager.setSearchPrefix("Glo");
    gmTmpCacheManager.setMaxFetchCount(100);
    gmTmpCacheManager.returnJsonString(gmSetMapAutoCompleteRptBean.loadLoanerSetList(hmparam));
  }
}

}
