package com.globus.common.beans;

import java.sql.ResultSet;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPriceHistoryLogBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  /**
   * fetchPriceLogHistory - This method will load required date history Info
   * 
   * @param strRequestId
   * @return ArrayList
   * @exception AppError
   */
  // this will be removed all place changed with Data Store VO constructor
  public GmPriceHistoryLogBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPriceHistoryLogBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public RowSetDynaClass fetchPriceLogHistory(String strRequestId, String strJNDIConnection)
      throws AppError {

    RowSetDynaClass rdResult = null;

    GmDBManager gmDBManager = GmDBManager.getGmDBManager(strJNDIConnection);
    gmDBManager.setPrepareString("gm_pkg_sm_pricerequest_rpt.gm_fch_price_log_history", 2);
    if ((strRequestId != null && strRequestId.equals(""))) {
      strRequestId = null;
    }

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return rdResult;
  }

	/**
	 * PC-5504 - to update the selected price savePartHistoryPricing
	 * 
	 * @param strInputStr
	 */
	public void savePartHistoryPricing(String strInputStr) {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strUserId = getGmDataStoreVO().getPartyid();
		gmDBManager.setPrepareString("gm_pkg_revert_account_part_pricing.gm_upd_part_history_pricing", 2);
		gmDBManager.setString(1, strInputStr);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		gmDBManager.commit();
	}
}
