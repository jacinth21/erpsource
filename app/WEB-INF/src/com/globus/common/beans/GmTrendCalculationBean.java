package com.globus.common.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;

import oracle.jdbc.driver.OracleTypes;


import org.apache.log4j.Logger;

public class GmTrendCalculationBean {
   	static Logger logger = GmLogger.getInstance("com.globus.accounting.test"); // Initializing
   	
	private static void init()
	{
		try
		{

		   	GmCommonInitHandler gmCommonInitHandler = new GmCommonInitHandler();
	
		   	
		   	//String ConfigDir = getServletContext().getRealPath("/config/");
		   	String logPropFile	 	= System.getProperty("ENV_LOG4J");
		   	String commonPropFile 	= "constants.properties";
		   	//String mail = "mail.properties";
		   	String envValue 	  	= "C:\\com";
		   	String ConfigDir 		= envValue;
		   	
		   	logger.debug(" inside service of init ");

		   	try {
		   		logger.debug("PartyReferenceInitServlet --> Entered service()");
		   		gmCommonInitHandler.setConfigDir(ConfigDir);
		   		gmCommonInitHandler.setlogPropFile(logPropFile);
		   		gmCommonInitHandler.setcommonPropFile(commonPropFile);
		   		gmCommonInitHandler.loadProperties();
		   		
		   		
		   		logger.debug("Sucessfully Loaded All the Property Files And the Reference Data in the Memory");
		   		
		   		logger.debug("The Property files and the References were loaded into the memory successfully.");

		   	} catch (Exception exc) {
		   		try
				{
		   			logger.error("GmCommonInitServlet:service():ERROR : Error Loading Properties Files.:"	+ exc.getMessage());
		   			logger.debug("There was an error while loading the Property Files and References.");
		   			logger.debug("Please resolve them and run the Servlet again.");
		   			
		   			logger.debug("There was an error while loading the Property Files and References.");
		   			logger.debug("Please resolve them and run the Servlet again.");
		   			
		   		}

		   		catch(Exception e)
				{
		   			logger.error("Error occurred in rendering the error message.");
		   		}
		   	}
		   	logger.debug(" Exit service of init ");
	   

		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public static void main(String as[]) throws Exception
	{		
	   init();			
	   ArrayList alResult = new ArrayList();
	   DBConnectionWrapper dbCon = new DBConnectionWrapper();
	   GmCrossTabReport gmCrossTabReport = new GmCrossTabReport();
	   HashMap hmResult = new HashMap();
	   Connection conn = null;
	   CallableStatement csmt = null;
	   
	   GmTrendCalculationBean gmTrendCalculationBean = new GmTrendCalculationBean();		
	   Logger log = GmLogger.getInstance(gmTrendCalculationBean.getClass().getName()); // Initializing   
	   log.debug("Enter"); 
	   
	   conn = dbCon.getOracleStaticConnection();
       String strPrepareString = dbCon.getStrPrepareString("gm_pkg_op_backorder_rpt.gm_fch_trend_summary", 10);
       csmt = conn.prepareCall(strPrepareString, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
	   csmt.registerOutParameter(6,OracleTypes.CURSOR);
	   csmt.registerOutParameter(7,OracleTypes.CURSOR);
	   csmt.registerOutParameter(8,OracleTypes.CURSOR);
	   csmt.registerOutParameter(9,OracleTypes.CURSOR);
	   csmt.registerOutParameter(10,OracleTypes.CURSOR);
	       
	   csmt.setString(1,"TREND,40021|");       
	   csmt.setString(2,"124.000");
	   csmt.setString(3,"924.901");
	   csmt.setString(4,"50271");
	   csmt.setString(5,"");
       
	   csmt.execute();       
	  
	   ResultSet rs = (ResultSet)csmt.getObject(7);
	   log.debug("rs.FetchSize()"+rs.getFetchSize());
	   rs.setFetchSize(100);
	   log.debug("rs.FetchSize()"+rs.getFetchSize());
	   log.debug("rs.getType()"+rs.getType());
	//   log.debug("DB Con:"+dbCon.returnArrayList(rs));
	 //  rs.absolute(1);
	
	   hmResult = gmCrossTabReport.GenerateCrossTabReport((ResultSet)csmt.getObject(6), rs);
	   log.debug("Cross Tab :"+hmResult);

	   // gmTrendCalculationBean.addRowTotal((ArrayList)hmResult.get("Details"),"Total Demand");
	  //log.debug("After adding the Total Column:"+hmResult);
	   
	   gmTrendCalculationBean.addAverageColumn((ArrayList)hmResult.get("Details"),"Total",3);
	  log.debug("After adding the Average Column:"+hmResult);
	   
	   gmTrendCalculationBean.addShelfPOColumns((ArrayList)hmResult.get("Details"));
	  log.debug("After adding the PO & Shelf Column:"+hmResult);

	  gmTrendCalculationBean.calculateTrend((ArrayList)hmResult.get("Details"));
	  log.debug("After adding the Trend:"+hmResult);
	   
	 // divideTotal(alResult,"Total",3);
       alResult = dbCon.returnArrayList((ResultSet)csmt.getObject(8));
     // log.debug("Demand Header:"+alResult);
       alResult = dbCon.returnArrayList((ResultSet)csmt.getObject(9));
     //  log.debug("Demand Details:"+alResult);
       alResult = dbCon.returnArrayList((ResultSet)csmt.getObject(10));
     //  log.debug("Part Details:"+alResult);    
       conn.close();
	       
       log.debug("Exit");
	}

	  private void calculateTrend(ArrayList alResult)
	  {
		  Iterator itrAlResult = alResult.iterator();			   														
			double dAverage = 0;
			double dShelf = 0;
			SimpleDateFormat formatMonth = new SimpleDateFormat("MMM");
			SimpleDateFormat formatYear = new SimpleDateFormat("yy");
			double dTrend = 0;
			Calendar c = null;
			
			while(itrAlResult.hasNext())
			{
				HashMap hmRow = (HashMap)itrAlResult.next();
				dAverage = Double.parseDouble(((String)hmRow.get("Average")));
				dShelf = Double.parseDouble(((String)hmRow.get("Shelf")));

			  for (int i=0;i<=2;i++)
				{
				    c = Calendar.getInstance();
				    dShelf = dTrend = dShelf - dAverage;
					c.add(Calendar.MONTH, i);
					hmRow.put("Trend "+formatMonth.format(c.getTime())+ " "+formatYear.format(c.getTime()) , ""+dTrend);
				}
			}			
	  }
	  
		private  void addRowTotal(ArrayList alResult, String strColumnName)
		{
			Iterator itrAlResult = alResult.iterator();
			Iterator itrHmKeys = null;
			
			
			
			while(itrAlResult.hasNext())
			{
				double dQty = 0;
				HashMap hmRow = (HashMap)itrAlResult.next();
				itrHmKeys = hmRow.keySet().iterator();
				while(itrHmKeys.hasNext())
				{
				   String strKey = (String)itrHmKeys.next();
				   try{
				   dQty += Double.parseDouble((String)hmRow.get(strKey));
				   }
				   catch(Exception ex)
				   {
					   ex.printStackTrace();
					   logger.debug("dQty: "+dQty);
					   continue;
				   }
				}				
				hmRow.put(strColumnName,""+dQty);													
		}
	}
		
		private void addShelfPOColumns(ArrayList alResult)
		{
			Iterator itrAlResult = alResult.iterator();			   											
			DecimalFormat df = new DecimalFormat("#.##");
			
			while(itrAlResult.hasNext())
			{
				HashMap hmRow = (HashMap)itrAlResult.next();			
				hmRow.put("Shelf","500");
				hmRow.put("PO","100");				
		    }
		}
		
		private  void addAverageColumn(ArrayList alResult, String strColumnName, int iDivisor)
		{
			Iterator itrAlResult = alResult.iterator();			   								
			double dResult = 0;
			DecimalFormat df = new DecimalFormat("#.##");
			
			while(itrAlResult.hasNext())
			{
				HashMap hmRow = (HashMap)itrAlResult.next();
				//logger.debug("hmRow"+hmRow);
				dResult = Double.parseDouble(((String)hmRow.get(strColumnName)));
				//logger.debug("QTY"+lQty);
				dResult = dResult / iDivisor;		
			//	logger.debug("QTY"+lQty);
				hmRow.put("Average",df.format(dResult));	
				//logger.debug("hmRow"+hmRow);
				
		    }
	}
}
