package com.globus.common.beans;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class GmResourceBundleBean {

  private static long lastCleared = 0L;
  private static long clearIntervalMillis = -1L;
  private ResourceBundle bundle;
  private final GmLogger log = GmLogger.getInstance();

  public GmResourceBundleBean() {
    bundle = null;
  }

  public GmResourceBundleBean(String res, Locale locale) {
    this(res, locale, -1);
  }

  public GmResourceBundleBean(String res, Locale locale, int clearInterval)
      throws NullPointerException, MissingResourceException {
    try {
      bundle = null;

      clearIntervalMillis = clearInterval * 1000;
      if (lastCleared == 0L)
        lastCleared = System.currentTimeMillis();

      // res = "com.globus.common.beans."+res;
      log.debug(" value of res  " + res + " and locale are " + locale);
      bundle = ResourceBundle.getBundle(res, locale);
      long timeNow = System.currentTimeMillis();
      // log.debug( (" Reading Title from RB --> " + bundle.getString("Title")));
    }

    catch (Exception exp) {
      exp.printStackTrace();
    }
    // if(clearIntervalMillis > -1L && timeNow - lastCleared >= clearIntervalMillis)
  }

  public ResourceBundle getBundle() {
    return bundle;
  }

  public String getProperty(String prop) {
    try {
      if (prop == null) {
        return null;
      } else {
        // To read the Japanese characters from the property file as UTF-8 encoded format
        String retProp =
            GmCommonClass.parseNull(GmCommonClass.encodeToUTF8(bundle.getString(prop)));
        return retProp;
      }
    } catch (MissingResourceException missingresourceexception) {
      log.error(" Missing Resource Exception from ResourceBundle " + missingresourceexception);
    } catch (ClassCastException classcastexception) {
      log.error(" Class Cast Exception from ResourceBundle " + classcastexception);
    } catch (Exception exception) {
      log.error(" Exception from ResourceBundle " + exception);
    }
    return null;
  }

  public String getProperty(int property) {
    String uriProp = String.valueOf(property);
    return getProperty(uriProp);
  }
}
