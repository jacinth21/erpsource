package com.globus.common.beans;

// Source file: C:\\SAM\\CODEGEN\\com\\ge\\nbc\\common\\util\\init\\handler\\CommonInitHandler.java

import java.lang.reflect.Field;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.globus.common.crypt.AESEncryptDecrypt;
import com.globus.logon.beans.GmLogonBean;

/**
 * This class will be called from CommonInitServlet, Used to load common and Log4j information
 */

public class GmCommonInitHandler {

  Logger logger = null;
  // This attribute represents logPropFile
  private String logPropFile;
  // This attribute represents Common Property Folder information
  private String ConfigDir;
  // This attribute represents Common Property File name
  private String commonPropFile;

  public GmCommonInitHandler() {
    logger = GmLogger.getInstance(this.getClass().getName());
  }


  /**
   * This method returns the value for the class level attribute, log4JFileName
   * 
   * @return Returns the value for the attribute, log4JFileName.
   */

  public String getlogPropFile() {
    return logPropFile;
  }


  /**
   * This method sets the value of log4JFileName with the value that is passed in as a parameter to
   * this method.
   * 
   * @param log4JFileName a String that represents the value that needs to be set to log4JFileName
   */

  public void setlogPropFile(String logPropFile) {
    this.logPropFile = logPropFile;
  }

  /**
   * This method returns the value for the class level attribute, log4JFileName
   * 
   * @return Returns the value for the attribute, log4JFileName.
   */

  public String getcommonPropFile() {
    return commonPropFile;
  }



  /**
   * This method sets the value of log4JFileName with the value that is passed in as a parameter to
   * this method.
   * 
   * @param log4JFileName a String that represents the value that needs to be set to log4JFileName
   */

  public void setcommonPropFile(String commonPropFile) {
    logger.debug("Common Property name ***** " + commonPropFile);
    this.commonPropFile = commonPropFile;
  }



  /**
   * This method returns the value for the class level attribute, Common Property Folder information
   * 
   * @return Returns the value for the attribute, CommonPropertyFileName.
   */

  public String getConfigDir() {
    return ConfigDir;
  }

  /**
   * This method sets the value of ConfigDir with the value that is passed in as a parameter to this
   * method.
   * 
   * @param CommonPropertyFileName a String that represents the value that needs to be set to
   *        CommonPropertyFileName
   */

  public void setConfigDir(String ConfigDir) {
    this.ConfigDir = ConfigDir;
  }


  /**
   * This method will load the property files into memory using the methods in the PropertyUtil
   * class. We need to load the log4j.properties and the common.properties file into the memory. The
   * property file names will be configured in the web.xml.
   * 
   * @throws Exception
   * @roseuid 41AB580A0110
   */

  public void loadProperties() throws Exception {
    logger = GmLogger.getInstance(this.getClass().getName());
    logger.debug("loading the properties :");

    try {
      // ****************************************************
      // Below code to load Log 4 J Property
      // ***************************************************

      if (logPropFile != null) {
        logger.debug("Log 4 J Property info" + ConfigDir + logPropFile);
        GmLogger.configure(ConfigDir);
        logger.debug("PartyReferenceInitServlet:loadProperties():Sucessfully Configured Log4J");
      } else {
        logger
            .debug("PartyReferenceInitServlet:loadProperties(): could not find Log4J properties.");
      }
      Class type = ResourceBundle.class;
      Field cacheList = type.getDeclaredField("cacheList");
      cacheList.setAccessible(true);
      ((Map) cacheList.get(ResourceBundle.class)).clear();

      GmCommonClass.rb = ResourceBundle.getBundle(System.getProperty("JBOSS_ENV"));
      GmCommonClass.rbShipCarrier = ResourceBundle.getBundle("ShippingCarrier");
      GmCommonClass.rbWikiTitle = ResourceBundle.getBundle("WikiTitle");
      GmCommonClass.rbJiraConfig = ResourceBundle.getBundle("JiraConfig");
      GmCommonClass.rbBusinessObjects = ResourceBundle.getBundle("BusinessObjects");
      AppError.rbApp = ResourceBundle.getBundle("AppError");
      GmLogError.rbApp = ResourceBundle.getBundle("Constants");
      GmLogonBean.rbAuthentication = ResourceBundle.getBundle("Authentication");
      AESEncryptDecrypt.resetSecretKey();
      String strval = GmCommonClass.rbWikiTitle.getString("TTP_REPORT");
      GmCommonClass.rbJmsConfig = ResourceBundle.getBundle(System.getProperty("ENV_JMS"));
      GmCommonClass.rbUdiFdaXml = ResourceBundle.getBundle(System.getProperty("ENV_UDI_CONF"));  //"UdiFdaXml"
	  // PMT-22472 : Dynamic JS path getting from properties : To reset the static object
	  GmFilePathConfigurationBean.rbFilePathConfig = ResourceBundle.getBundle("FilePathConfig");
	  GmCommonClass.strCompanyName = GmCommonClass.parseNull((String)GmCommonClass.getString("GMCOMPANYNAME"));
     // GmCommonClass.strServerName = GmCommonClass.parseNull((String)GmCommonClass.getString("SERVERNAME"));
      logger.debug(" strVal " + strval);
      // logger.debug(" strval " + strval);

      /* Configuring the Log4j properties */
      // GmLog.configure(System.getProperty("config"));
      /* Binding Logger */
      // ****************************************************
      // Below code to load Common Property
      // ***************************************************
      /*
       * if (commonPropFile != null) { logger.debug("log4j-init-file ***** " + logPropFile );
       * logger.debug("About to load the common.properties from " +commonPropFile);
       * PropertyUtil.loadPropertyFile(ConstantUtil.APP_COMMON, ConfigDir + "/" + commonPropFile);
       * logger.debug(" ***** Sucessfully loaded the common.properties *****" +
       * PropertyUtil.getProperties(ConstantUtil.APP_COMMON)); }else {
       * logger.error("loadProperties(): could not find COMMON properties."); }
       */
    }

    catch (Exception ex) {
      logger.error("Error Processing the loadProperties", ex);
    }
    logger.debug("Exiting After loading properties");
  }

}
