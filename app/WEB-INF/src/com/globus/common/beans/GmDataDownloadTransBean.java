package com.globus.common.beans;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDataDownloadTransBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.
  public GmDataDownloadTransBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmDataDownloadTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }


  public int saveDownloadDtls(HashMap hmParam) throws AppError, IOException {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmTemp = new HashMap();
    String strPaymentIDs = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String strDownloadTotalAmt = GmCommonClass.parseNull((String) hmParam.get("HDOWNLOADAMTTOTAL"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("DOWNLOADTYPE"));// "50740"; //
                                                                                   // "INVC";
                                                                                   // invoice
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("GM_PKG_CM_DATA_DOWNLOAD.GM_DD_SAV_DOWNLOAD_DTLS", 5);
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);
    gmDBManager.setString(1, strPaymentIDs);
    gmDBManager.setInt(2, Integer.parseInt(strType));
    gmDBManager.setDouble(3, Double.parseDouble(strDownloadTotalAmt));
    gmDBManager.setString(4, strUserId);

    gmDBManager.execute();
    String strBatchID = GmCommonClass.parseNull(gmDBManager.getString(5));
    int intBatchID = Integer.parseInt(strBatchID);
    // gmDBManager.commit();
    log.debug(" hash in " + gmDBManager.hashCode());
    hmTemp.put("GMDBMANAGER", gmDBManager);
    prepareDownloadFile(hmTemp, intBatchID);
    gmDBManager.commit();
    return intBatchID;
  }

  public void prepareDownloadFile(HashMap hmTemp, int intBatchID) throws AppError, IOException {
    RowSetDynaClass rd = null;
    // GmDBManager gmDBManager = new GmDBManager();
    GmDBManager gmDBManager = (GmDBManager) hmTemp.get("GMDBMANAGER");
    log.debug(" hash out " + gmDBManager.hashCode());
    // GM_PKG_CM__DATA_DOWNLOAD.GM_DD_FCH_DATA_DOWNLOAD
    log.debug("hmTemp:" + hmTemp);
    log.debug("strBatchId:" + intBatchID);

    String strCompanyId = getGmDataStoreVO().getCmpid();
    String location = "";
    String strApInvXMLFolderLoc = "";
    HashMap hmReturn = new HashMap();
    HashMap hmBatchDetails = new HashMap();

    ArrayList alBatchList = new ArrayList();

    gmDBManager.setPrepareString("GM_PKG_CM_DATA_DOWNLOAD.GM_DD_FCH_DATA_DOWNLOAD", 3);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.setInt(1, intBatchID);

    gmDBManager.execute();

    hmBatchDetails =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    alBatchList =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));

    // gmDBManager.close();

    hmReturn.put("HMBATCHDETAIL", hmBatchDetails);
    hmReturn.put("ALBATCHLIST", alBatchList);

    log.debug("hmBatchDetails:" + hmBatchDetails);
    log.debug("alBatchList:" + alBatchList);
    log.debug("alBatchList size:" + alBatchList.size());

    // String location = System.getProperty("ENV_GMSAGEFILELOC");

    // Batch details will store in different folder for different companies.
    //Added new rule for PMT-51173: A/P XML Changes
    strApInvXMLFolderLoc = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strCompanyId, "AP_INVXML_FOLDER_LOC"));
 
    if (strApInvXMLFolderLoc.equals("Y")) {
      location = System.getProperty("ENV_GMSAGEFILELOC");
    } else {
      location = GmCommonClass.getString("GMSAGEFILELOCFOROUS");
      String StrShrtCmpName = GmCommonClass.getCompanyLocale(strCompanyId);
      location = location + "\\G" + StrShrtCmpName + "\\";
      File file = new File(location);
      if (!file.exists()) {
        file.mkdirs();
      }

    }


    String file_name = GmCommonClass.parseNull((String) hmBatchDetails.get("FILE_NAME"));// get
    // from
    // batch
    // details
    // hashmap;
    String batch_type = GmCommonClass.parseNull((String) hmBatchDetails.get("DOWNLOAD_TYPE"));
    log.debug("location:" + location);
    log.debug("fileName:" + file_name);
    log.debug("batch_type:" + batch_type);
    try {
      File f = new File(location + file_name);

      FileWriter fw = new FileWriter(f);

      if (batch_type.equals("50740")) {

        // fw.write("<ap_invoice>\n"); // Root element start
        fw.write("<doc>\n");
        // String strHeader = prepareXMLHeaderPart(hmBatchDetails, "batch");
        // fw.write(strHeader);
        String strDetail =
            prepareXMLAPDetailPart(alBatchList, "PendingVoucherHeader", "PendingVoucherDetail");// "invoicedetail");
        fw.write(strDetail);
        // fw.write("</ap_invoice>");
        fw.write("</doc>");

      }
      fw.close();
      f.setReadOnly();
    } catch (Exception exp) {
      exp.printStackTrace();
      throw new AppError(exp);
    }
    // f.close();

  }

  public String prepareXMLHeaderPart(HashMap hmHeader, String headerName) {

    Set s = hmHeader.keySet();
    Iterator it = s.iterator();

    String keyName = new String();
    StringBuffer batchInfo = new StringBuffer();

    batchInfo.append("<");
    batchInfo.append(headerName);
    batchInfo.append(">\n");

    while (it.hasNext()) {
      keyName = (String) it.next();
      batchInfo.append("<");
      batchInfo.append(keyName);
      batchInfo.append(">");
      batchInfo.append(hmHeader.get(keyName));
      batchInfo.append("</");
      batchInfo.append(keyName);
      batchInfo.append(">\n");
    }

    batchInfo.append("</");
    batchInfo.append(headerName);
    batchInfo.append(">\n");

    return batchInfo.toString();
  }

  public String prepareXMLDetailPart(ArrayList alDetails, String detailName) {

    int iSize = alDetails.size();
    HashMap hmDetail = null;

    Set s = null;
    Iterator it = null;
    String strNameDtls = "";

    StringBuffer detailInfo = new StringBuffer();
    String keyName = new String();
    for (int i = 0; i < iSize; i++) {
      hmDetail = (HashMap) alDetails.get(i);
      s = hmDetail.keySet();
      it = s.iterator();
      detailInfo.append("<");
      detailInfo.append(detailName);
      detailInfo.append(">\n");

      while (it.hasNext()) {
        keyName = (String) it.next();
        detailInfo.append("<");
        detailInfo.append(keyName);
        detailInfo.append(">");
        strNameDtls = (String) hmDetail.get(keyName);
        strNameDtls = strNameDtls.replaceAll("&", "");
        detailInfo.append(strNameDtls);

        detailInfo.append("</");
        detailInfo.append(keyName);
        detailInfo.append(">\n");
      }
      detailInfo.append("</");
      detailInfo.append(detailName);
      detailInfo.append(">\n");
    }
    return detailInfo.toString();

  }

  public String prepareXMLAPDetailPart(ArrayList alDetails, String headerName, String detailName) {

    int iSize = alDetails.size();
    HashMap hmDetail = null;

    // Key Information
    HashMap hmKeyInfo = new HashMap();

    hmKeyInfo.put("SAGE_VENDOR_ID", "VendID");
    hmKeyInfo.put("INVOICE_DT", "TranDate");
    hmKeyInfo.put("INVOICE_NUMBER", "TranNo");
    hmKeyInfo.put("LOCAL_PAYMENT_AMT", "TranAmt");  //PC-5167(AP Invoice XML show Local amount) to save the vendor currency amount into xml file. 
    
    hmKeyInfo.put("INVOICE_COMMENT", "TranCmnt");
    hmKeyInfo.put("LOCAL_PAYMENT_AMT_EXT", "ExtAmt"); //PC-5167(AP Invoice XML show Local amount) to save the vendor currency amount into xml file.  
    hmKeyInfo.put("BATCH_NUMBER_EXT", "ExtCmnt");
    hmKeyInfo.put("INVOICE_NUMBER_EXT", "TranNo");
    hmKeyInfo.put("POSTDATE", "PostDate");

    Set s = null;
    Iterator it = null;
    String strNameDtls = "";

    StringBuffer invoiceHdr = new StringBuffer();
    StringBuffer invoiceDetails = new StringBuffer();
    StringBuffer finalString = new StringBuffer();


    String keyName = new String();
    for (int i = 0; i < iSize; i++) {
      hmDetail = (HashMap) alDetails.get(i);
      s = hmDetail.keySet();
      it = s.iterator();
      invoiceHdr.append("<");
      invoiceHdr.append(headerName);
      invoiceHdr.append(">\n");

      invoiceDetails.append("<");
      invoiceDetails.append(detailName);
      invoiceDetails.append(">\n");

      while (it.hasNext()) {
        keyName = (String) it.next();
        if (!keyName.equals("VENDOR_NAME") && !keyName.equals("INVOICE_AMOUNT") && !keyName.equals("INVOICE_AMOUNT_EXT")) {
          if (keyName.indexOf("_EXT") != -1) {
            invoiceDetails.append("<");
            invoiceDetails.append((String) hmKeyInfo.get(keyName));
            invoiceDetails.append(">");
            strNameDtls = (String) hmDetail.get(keyName);
            // strNameDtls = strNameDtls.replaceAll("&", "");
            invoiceDetails.append(strNameDtls);

            invoiceDetails.append("</");
            invoiceDetails.append((String) hmKeyInfo.get(keyName));
            invoiceDetails.append(">\n");
          } else {
            invoiceHdr.append("<");
            invoiceHdr.append((String) hmKeyInfo.get(keyName));
            invoiceHdr.append(">");
            strNameDtls = (String) hmDetail.get(keyName);
            // strNameDtls = strNameDtls.replaceAll("&", "");
            if (strNameDtls.equals(""))
              strNameDtls = "-";
            invoiceHdr.append(strNameDtls);

            invoiceHdr.append("</");
            invoiceHdr.append((String) hmKeyInfo.get(keyName));
            invoiceHdr.append(">\n");
          }
        }
      }
      invoiceHdr.append("</");
      invoiceHdr.append(headerName);
      invoiceHdr.append(">\n");

      // detailInfoHdr.append(detailInfo);
      // detailInfo.append(detailInfoHdr);
      invoiceDetails.append("</");
      invoiceDetails.append(detailName);
      invoiceDetails.append(">\n");

      finalString.append(invoiceHdr.toString() + invoiceDetails.toString());
      invoiceHdr.setLength(0);
      invoiceDetails.setLength(0);
    }
    return finalString.toString();

  }

}
