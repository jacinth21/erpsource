/**************************************************************************************************
 * File : DBConnectionWrapper.java Screen ID : Demo Screen Desc : This is DBConnectionWrapper Class
 * For Database Connection Version : 1.0
 *************************************************************************************************/
package com.globus.common.beans;

// SAM Classes
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.commons.beanutils.WrapDynaBean;
import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.GmDataVO;

/**
 * This is the Database utility class which is used for handling all the database operations
 * throughout the application It essentially provides methods to execute DML statements using Normal
 * Statements, Prepared Statements. It also provides methods for standard transactional handling
 */


public class DBConnectionWrapper implements Serializable {
  private static Context ic;
  protected Connection conn = null;
  protected String strPrepareString = null;
  protected String strSQL = null;
  protected String strProcedureName = null;
  protected int intProcedureParameters = -1;
  protected String strFunctionName = null;
  protected int intFunctionInParameters = -1;
  protected CallableStatement callableStatement = null;
  protected PreparedStatement preparedStatement = null;
  protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the
                                                                         // Logger Class.
  protected GmDNSNamesEnum enDNSName = GmDNSNamesEnum.WEBGLOBUS;
  protected String strTimeZone = "";
  protected String strCompanyID = "";
  protected String strCompDateFmt = "";
  protected String strPlantID = "";
  protected String strPartyID = "";
  protected String strCompLangID = "103097"; // Default as English(103097)

  static {
    try {
      ic = new InitialContext();
    } catch (Exception e) {

    }
  }

  /**
   * Default constructor
   * 
   * @throws AppError This is the standard Application error object
   */

  public DBConnectionWrapper() {

  } // END of DBSession constructor

  /**
   * constructor - set company info
   * 
   * @paramGmDataStoreVO
   * @throws AppError This is the standard Application error object
   */
  public DBConnectionWrapper(GmDataStoreVO gmDataStoreVO) {
    this.strTimeZone = GmCommonClass.parseNull(gmDataStoreVO.getCmptzone());
    this.strCompanyID = GmCommonClass.parseNull(gmDataStoreVO.getCmpid());
    this.strCompDateFmt = GmCommonClass.parseNull(gmDataStoreVO.getCmpdfmt());
    this.strPlantID = GmCommonClass.parseNull(gmDataStoreVO.getPlantid());
    this.strPartyID = GmCommonClass.parseNull(gmDataStoreVO.getPartyid());
    this.strCompLangID = GmCommonClass.parseNull(gmDataStoreVO.getCmplangid());
  }

  /**
   * The function is used to establish the connection
   * 
   * @throws AppError This is the standard Application error object
   */
  public Connection getConnection() throws AppError {
    return getConnection(enDNSName);
  }

  /**
   * The function is used to establish the connection
   * 
   * @throws AppError This is the standard Application error object
   */
  public Connection getConnection(GmDNSNamesEnum enDNSName) throws AppError {
    // Connection connection = null;
    // String dsName = "jdbc/globus";
    try {
      DataSource ds = (DataSource) ic.lookup(enDNSName.getDNSName());
      conn = ds.getConnection();
      conn.setAutoCommit(false);
      // If time zone is not empty then DB session time zone will alter with given time zone and
      // application context will set
      // Company Id, Plant Id, Time Zone, date format
      if (!strTimeZone.equals("")) {
        setSessionTimeZone(strTimeZone);
        loadAppContext(conn);
      }

    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    }

    return conn;
  }


  /**
   * This method will set DB session time zone for every connection
   * 
   * @param strTimeZone
   */
  private void setSessionTimeZone(String strTimeZone) {
    Statement stmt = null;
    if (conn != null) {
      try {
        stmt = conn.createStatement();
        stmt = setTimeOut(stmt);
        stmt.execute("alter session set time_zone=\'" + strTimeZone + "\'");
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
  }



  /**
   * This method will be called every time connection is established. This method will set the
   * Required Parameters to the Oracle Session Context. These values will / can be accessed in the
   * Oracle PL/SQL Block.
   */
  public void loadAppContext(Connection conn) throws SQLException, AppError {
    CallableStatement csmt = null;
    strPrepareString = getStrPrepareString("gm_pkg_cor_client_context.gm_sav_client_context", 6);
    csmt = conn.prepareCall(strPrepareString);
    csmt.setString(1, strCompanyID);
    csmt.setString(2, strTimeZone);
    csmt.setString(3, strCompDateFmt);
    csmt.setString(4, strPlantID);
    csmt.setString(5, strPartyID);
    csmt.setString(6, strCompLangID);
    csmt.execute();
    /* oracle.jdbc.driver.OracleConnection nativeOraConn = null; */

    /*
     * As the default connection will be wrapper connection [provided by Jboss or Weblogic] we
     * should first check if the underlying connection is of type OracleConnection, only the Unwrap
     * it . The Application connect can be set only in the oracle.jdbc.driver.OracleConnection
     * object, hence we are type casting it. Without unwrapping the Connection, we will not be able
     * to type cast. JDBC allows to use only CLIENTCONTEXT as the Context name, no other name is
     * allowed.
     */

    /*
     * if (conn.isWrapperFor(oracle.jdbc.driver.OracleConnection.class)) {
     * log.debug("Oralce Connection executing");
     * 
     * nativeOraConn = conn.unwrap(oracle.jdbc.driver.OracleConnection.class);
     * nativeOraConn.setSessionTimeZone(strTimeZone);
     * 
     * nativeOraConn.setApplicationContext("CLIENTCONTEXT", "COMPANY_ID", strCompanyID);
     * nativeOraConn.setApplicationContext("CLIENTCONTEXT", "COMP_TIME_ZONE", strTimeZone);
     * nativeOraConn.setApplicationContext("CLIENTCONTEXT", "COMP_DATE_FMT", strCompDateFmt);
     * nativeOraConn.setApplicationContext("CLIENTCONTEXT", "PLANT_ID", strPlantID); }
     */
  }

  public Connection getOracleStaticConnection() {
    String url = "jdbc:oracle:thin:@gmiAsrv01:1521:gmiprod";
    // String url = "jdbc:oracle:thin:@gmiAsrv02:1521:gmiTest01"; // Connection to test DB
    String driver = "oracle.jdbc.driver.OracleDriver";

    String USERNAME = new String("globus_app");
    String PASSWORD = new String("appapp");
    Connection connection = null;

    try {
      Class.forName(driver);
      connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
    }
    return connection;
  }


  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return ArrayList of HashMaps containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */
  public ArrayList queryMultipleRecords(String astrQuery) throws AppError {
    String str = null;
    ResultSetMetaData meta;
    ArrayList alistResult;
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    HashMap hmapResult;
    try {
      conn = getConnection();

      stmt = conn.createStatement();
      stmt = setTimeOut(stmt);
      rs = stmt.executeQuery(astrQuery);
      meta = rs.getMetaData();

      alistResult = new ArrayList(1);

      int count = meta.getColumnCount();
      int size = (int) (count * 1.33);

      while (rs.next()) {
        hmapResult = new HashMap(size);
        for (int i = 1; i <= count; i++) {
          if (meta.getColumnTypeName(i).equals("DATE")) {
            hmapResult.put(meta.getColumnName(i), rs.getDate(i));
          } else {
            str = rs.getString(i);

            if (str == null) {
              str = "";
            }
            hmapResult.put(meta.getColumnName(i), str.trim());
          }
        }

        alistResult.add(hmapResult);
        hmapResult = null;
      }
      // stmt = null;
      // rs = null;
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
        if (stmt != null && !stmt.isClosed()) {
          stmt.close();
        }
        if (conn != null && !conn.isClosed()) {
          conn.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return alistResult;
  } // END of method queryMultipleRecords


  /**
   * This function is used to execute SQL "SELECT" queries. The function fetches multiple rows and
   * creates a hashmap and sends it back
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return HashMap containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */
  public HashMap queryMultipleHashMap(String astrQuery) throws AppError {
    String str1 = null;
    String str2 = null;
    ResultSetMetaData meta;
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    HashMap hmapResult = null;
    try {
      conn = getConnection();

      stmt = conn.createStatement();
      stmt = setTimeOut(stmt);
      rs = stmt.executeQuery(astrQuery);
      meta = rs.getMetaData();

      int count = meta.getColumnCount();
      int size = (int) (count * 1.33);
      hmapResult = new HashMap(size);

      while (rs.next()) {
        for (int i = 1; i <= count; i += 2) {
          str1 = rs.getString(i);
          str2 = rs.getString(i + 1);

          if (str1 == null) {
            str1 = "";
          }
          if (str2 == null) {
            str2 = "";
          }
        }
        hmapResult.put(str1.trim(), str2.trim());
      }
      // stmt = null;
      // rs = null;
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
        if (stmt != null && !stmt.isClosed()) {
          stmt.close();
        }
        if (conn != null && !conn.isClosed()) {
          conn.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return hmapResult;
  } // END of method queryMultipleRecords


  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it. For this method connection will be passed by the user
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @param conn This parameter is a connection object to be used for quering purpose.
   * @return ArrayList of HashMaps containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */
  public ArrayList queryMultipleRecords(String astrQuery, Connection conn) throws AppError {
    String str = null;
    ResultSetMetaData meta;
    ArrayList alistResult;
    HashMap hmapResult;
    Statement stmt = null;
    ResultSet rs = null;
    try {
      stmt = conn.createStatement();
      rs = stmt.executeQuery(astrQuery);
      meta = rs.getMetaData();
      alistResult = new ArrayList(1);

      int count = meta.getColumnCount();
      int size = (int) (count * 1.33);

      while (rs.next()) {
        hmapResult = new HashMap(size);
        for (int i = 1; i <= count; i++) {
          str = rs.getString(i);

          if (str == null) {
            str = "";
          }
          hmapResult.put(meta.getColumnName(i), str.trim());
        }
        alistResult.add(hmapResult);
        hmapResult = null;
      }
      // stmt = null;
      // rs = null;
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
        if (stmt != null && !stmt.isClosed()) {
          stmt.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return alistResult;
  } // END of method queryMultipleRecords

  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return Hashtable containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */


  public HashMap querySingleRecord(String astrQuery) throws AppError {
    String str = null;
    ResultSetMetaData meta;
    HashMap hmResult;
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    try {
      conn = getConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery(astrQuery);
      meta = rs.getMetaData();
      int count = meta.getColumnCount();
      hmResult = new HashMap(1);

      while (rs.next()) {
        int size = (int) (count * 1.33);
        for (int i = 1; i <= count; i++) {
          if (meta.getColumnTypeName(i).equals("DATE")) {
            hmResult.put(meta.getColumnName(i), rs.getDate(i));
          } else {
            str = rs.getString(i);

            if (str == null) {
              str = "";
            }
            hmResult.put(meta.getColumnName(i), str.trim());
          }
        }
      }
      // stmt = null;
      // rs = null;
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
        if (stmt != null && !stmt.isClosed()) {
          stmt.close();
        }
        if (conn != null && !conn.isClosed()) {
          conn.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return hmResult;
  } // END of method executeResultSet

  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return Hashtable containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */


  public String queryJSONRecord(String astrQuery) throws AppError {
    String str = null;
    ResultSetMetaData meta;
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    try {
      conn = getConnection();
      stmt = conn.createStatement();
      rs = stmt.executeQuery(astrQuery);
      meta = rs.getMetaData();
      int count = meta.getColumnCount();

      while (rs.next()) {
        for (int i = 1; i <= count; i++) {
          str = rs.getString(i);
        }
      }
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
        if (stmt != null && !stmt.isClosed()) {
          stmt.close();
        }
        if (conn != null && !conn.isClosed()) {
          conn.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return str;
  } // END of method executeResultSet

  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return Hashtable containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */

  public Map querySingleRecord(PreparedStatement prepStmt) throws AppError {
    String str = null;
    ResultSetMetaData meta;
    HashMap hmResult;
    Connection conn = null;
    ResultSet rs = null;
    try {
      rs = prepStmt.executeQuery();
      meta = rs.getMetaData();
      int count = meta.getColumnCount();
      hmResult = new HashMap(1);

      while (rs.next()) {
        int size = (int) (count * 1.33);
        for (int i = 1; i <= count; i++) {
          if (meta.getColumnTypeName(i).equals("DATE")) {
            hmResult.put(meta.getColumnName(i), rs.getDate(i));
          } else {
            str = rs.getString(i);

            if (str == null) {
              str = "";
            }
            hmResult.put(meta.getColumnName(i), str.trim());
          }
        }
      }
      // prepStmt = null;
      // rs = null;
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
        if (prepStmt != null && !prepStmt.isClosed()) {
          prepStmt.close();
        }
        if (conn != null && !conn.isClosed()) {
          conn.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return hmResult;
  }

  /**
   * This method returns a PreparedString used to interface with the Oracle stored procedures.
   * 
   * @return String
   * @throws SQLException
   */

  public String getStrPrepareString(String strProcedureName, int intNoArgs) throws SQLException {
    String strPrepareString = "call " + strProcedureName + "(";
    for (int i = 0; i < intNoArgs; i++) {
      strPrepareString += "?";
      if (i != intNoArgs - 1)
        strPrepareString += ",";
    }
    strPrepareString += ")";

    return strPrepareString;
  }// END of getStrPrepareString method

  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return ArrayList of HashMaps containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */
  public ArrayList returnArrayList(ResultSet rs) throws AppError {
    String str = null;
    ResultSetMetaData meta;
    ArrayList alistResult;

    try {
      meta = rs.getMetaData();
      int count = meta.getColumnCount();
      alistResult = new ArrayList(1);

      while (rs.next()) {
        int size = (int) (count * 1.33);
        HashMap hmapResult = new LinkedHashMap(size);
        for (int i = 1; i <= count; i++) {
          if (meta.getColumnTypeName(i).equals("DATE")) {
            hmapResult.put(meta.getColumnName(i), rs.getDate(i));
          } else {
            str = rs.getString(i);

            if (str == null) {
              str = "";
            }
            hmapResult.put(meta.getColumnName(i), str.trim());
          }
        }
        alistResult.add(hmapResult);
        hmapResult = null;
      }
      // rs = null;
    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return alistResult;
  } // END of method returnArrayList

  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return ListVO containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */
  public List returnVOList(ResultSet rs, GmDataVO gmDataVO, Properties properties) throws AppError {
    String str = null;
    ResultSetMetaData meta;
    List<GmDataVO> alReturn;
    WrapDynaBean dataStore = null;

    try {
      meta = rs.getMetaData();
      int count = meta.getColumnCount();
      alReturn = new ArrayList<GmDataVO>();

      while (rs.next()) {
        dataStore = new WrapDynaBean(gmDataVO.getClass().newInstance());
        for (int i = 1; i <= count; i++) {
          String strKey = meta.getColumnName(i);
          if (properties.containsKey(strKey)) {
            str = rs.getString(i);

            if (str == null) {
              str = "";
            }
            dataStore.set(((String) properties.get(strKey)).toLowerCase(), str.trim());
          }
        }
        alReturn.add((GmDataVO) dataStore.getInstance());
      }
      // rs = null;
    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return alReturn;
  } // END of method returnVOList

  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return ArrayList of HashMaps containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */
  public HashMap returnConvertedHashMap(ResultSet rs) throws AppError {
    String str1 = null;
    String str2 = null;
    ResultSetMetaData meta;
    HashMap hmapResult = null;
    try {
      meta = rs.getMetaData();

      int count = meta.getColumnCount();
      int size = (int) (count * 1.33);
      hmapResult = new HashMap(size);

      while (rs.next()) {
        for (int i = 1; i <= count; i += 2) {
          str1 = rs.getString(i);
          str2 = rs.getString(i + 1);

          if (str1 == null) {
            str1 = "";
          }
          if (str2 == null) {
            str2 = "";
          }
        }
        hmapResult.put(str1.trim(), str2.trim());
      }
      // rs = null;
    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return hmapResult;
  } // END of method returnArrayList

  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it.
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return ArrayList of HashMaps containing all the values returned in the resultset.
   * @throws AppError This is the standard Application error object
   */
  public RowSetDynaClass returnRowSetDyna(ResultSet rs) throws AppError {
    RowSetDynaClass resultSet = null;
    try {
      resultSet = new RowSetDynaClass(rs, false);

      // rs = null;
    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return resultSet;
  } // END of method returnArrayList

  /**
   * This function is used to execute SQL "SELECT" queries. The function allows to query the
   * database and get the result data from it and returns the result set value
   * 
   * @param astrQuery This parameter is a string containing plain SQL query.
   * @return RowSetDynaClass object.
   * @throws AppError This is the standard Application error object
   */
  public RowSetDynaClass QueryDisplayTagRecordset(String astrQuery) throws AppError {
    String str = null;
    ResultSetMetaData meta;
    ArrayList alistResult;
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    RowSetDynaClass resultSet = null;

    try {
      conn = getConnection();
      stmt = conn.createStatement();
      stmt = setTimeOut(stmt);
      rs = stmt.executeQuery(astrQuery);

      // To assign the value to Dynamic tag result set object
      resultSet = new RowSetDynaClass(rs, false);

      // stmt = null;
      // rs = null;
    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
        if (stmt != null && !stmt.isClosed()) {
          stmt.close();
        }
        if (conn != null && !conn.isClosed()) {
          conn.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return resultSet;
  } // END of method queryMultipleRecords

  public HashMap returnHashMap(ResultSet rs) throws AppError {
    String str = null;
    ResultSetMetaData meta;
    HashMap hmResult;
    Connection conn = null;
    Statement stmt = null;

    try {
      meta = rs.getMetaData();
      int count = meta.getColumnCount();
      hmResult = new HashMap(1);

      if (rs.next()) {
        int size = (int) (count * 1.33);
        for (int i = 1; i <= count; i++) {
          if (meta.getColumnTypeName(i).equals("DATE")) {
            hmResult.put(meta.getColumnName(i), rs.getDate(i));
          } else {
            str = rs.getString(i);

            if (str == null) {
              str = "";
            }
            hmResult.put(meta.getColumnName(i), str.trim());
          }
        }
      }
      // stmt = null;
      // rs = null;
    } catch (Exception e) {
      terminate();
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        if (rs != null && !rs.isClosed()) {
          rs.close();
        }
        if (stmt != null && !stmt.isClosed()) {
          stmt.close();
        }
        if (conn != null && !conn.isClosed()) {
          conn.close();
        }
      } catch (Exception e) {
        String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
        log.error("Exception e " + strErrorMsg);
        throw new AppError(e);
      }
    }
    return hmResult;
  } // END of method executeResultSet

  /**
   * terminate - will close any active callablestatement or connection will be called from finally
   * 
   * @throws AppError
   */
  protected void terminate() throws AppError {
    try {
      if (callableStatement != null && !callableStatement.isClosed()) {
        callableStatement.close();

      }// Enf of if (callableStatement != null)

      if (preparedStatement != null && !preparedStatement.isClosed()) {
        preparedStatement.close();
      }

      if (conn != null && !conn.isClosed()) {
        conn.rollback();
        conn.close(); /* closing the Connections */
      }
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    }// End of catch
    finally {
      preparedStatement = null;
      callableStatement = null;
      conn = null;
    }
  }

  /**
   * Set timeout - will call for preparedStatement and callablestatement connection time out
   * 
   * @throws AppError
   */
  public void setTimeOut() throws AppError {
    try {
      if (preparedStatement != null && !preparedStatement.isClosed()) {

        if ((this.enDNSName.getDNSName()).equalsIgnoreCase(GmDNSNamesEnum.WEBGLOBUS.getDNSName()))

        {

          preparedStatement.setQueryTimeout(GmCommonClass.dbConnectionTimeOut);
        }

        if ((this.enDNSName.getDNSName()).equalsIgnoreCase(GmDNSNamesEnum.DMWEBGLOBUS.getDNSName()))

        {

          preparedStatement.setQueryTimeout(GmCommonClass.dbConnectionTimeOutEXT);

        }

        if ((this.enDNSName.getDNSName()).equalsIgnoreCase(GmDNSNamesEnum.GMJOBS.getDNSName()))

        {

          preparedStatement.setQueryTimeout(GmCommonClass.dbConnectionTimeOut);

        }



      }
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    }// End of catch
  }

  /**
   * Set timeout - will call for query execution connection time out
   * 
   * @throws AppError
   */
  public Statement setTimeOut(Statement stmt) throws AppError {
    try {
      if (stmt != null && !stmt.isClosed()) {

        if ((this.enDNSName.getDNSName()).equalsIgnoreCase(GmDNSNamesEnum.WEBGLOBUS.getDNSName()))

        {
          stmt.setQueryTimeout(GmCommonClass.dbConnectionTimeOut);
        }

        if ((this.enDNSName.getDNSName()).equalsIgnoreCase(GmDNSNamesEnum.DMWEBGLOBUS.getDNSName())) {
          stmt.setQueryTimeout(GmCommonClass.dbConnectionTimeOutEXT);
        }

        if ((this.enDNSName.getDNSName()).equalsIgnoreCase(GmDNSNamesEnum.GMJOBS.getDNSName()))

        {
          stmt.setQueryTimeout(GmCommonClass.dbConnectionTimeOut);
        }


      }
      return stmt;
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    }// End of catch
  }
  
  
	/**
	 * PC-5101: Score card DO classification
	 * 
	 * This method is used to execute SQL "SELECT" queries. We are avoid to getting
	 * the connection object - if already connection available.
	 * 
	 * @param strQuery This parameter is a string containing plain SQL query.
	 * @return String (JSON data)
	 * 
	 * @throws AppError This is the standard Application error object
	 */

	public String queryJSONRecordWithExistingConnection(String strQuery) throws AppError {
		String str = null;
		ResultSetMetaData meta;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			if (conn == null) {
				log.debug(" Inside New Connection ===> " + conn);
				conn = getConnection();
			}

			stmt = conn.createStatement();
			rs = stmt.executeQuery(strQuery);
			meta = rs.getMetaData();
			int count = meta.getColumnCount();

			while (rs.next()) {
				for (int i = 1; i <= count; i++) {
					str = rs.getString(i);
				}
			}
		} catch (Exception e) {
			String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
			log.error("Exception e " + strErrorMsg);
			throw new AppError(e);
		} finally {
			try {
				if (rs != null && !rs.isClosed()) {
					rs.close();
				}
				if (stmt != null && !stmt.isClosed()) {
					stmt.close();
				}
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (Exception e) {
				String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
				log.error("Exception e " + strErrorMsg);
				throw new AppError(e);
			}
		}
		return str;
	} // END of method executeResultSet

}
