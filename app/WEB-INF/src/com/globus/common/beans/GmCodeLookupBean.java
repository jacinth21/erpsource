package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCodeLookupBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public GmCodeLookupBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmCodeLookupBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * This method used to fetch code lookup details for product catalog
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public ArrayList fetchCodeLookup(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_fch_codelist_prodcat", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));

    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchCodeLookupExclusion - This method used to fetch code lookup exclusion details while
   * syncing App Data in Ipad
   * 
   * @param hmParams
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCodeLookupExclusion(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_fch_codelist_exclusion", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    log.debug("fetchCodeLookupExclusion bean" + alResult);
    gmDBManager.close();
    return alResult;
  }

  /**
   * fetchCodeLookupAccessList - This method used to fetch code lookup access table while syncing
   * App Data in Ipad
   * 
   * @param hmParams
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCodeLookupAccessList(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_fch_code_access_list", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    log.debug("fetchCodeLookupAccessList bean" + alResult);
    gmDBManager.close();
    return alResult;
  }
  
  /**
   * fetchSecurityEvents - This method is used to get the mobile security events
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public ArrayList fetchSecurityEvents(HashMap hmParams) throws AppError {
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    ArrayList alResult = new ArrayList();
	    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_fch_security_event_list", 6);
	    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
	    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
	    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
	    gmDBManager.setString(4, (String) hmParams.get("UUID"));
	    gmDBManager.setString(5, (String) hmParams.get("PARTYID"));
	    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alResult =
	        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
	            .getObject(6)));
	    log.debug("fetchSecurityEvents bean" + alResult);
	    gmDBManager.close();
	    return alResult;
	  }
}
