// Source file:
// C:\\projects\\GlobusMed\\src\\GlobusMedEar\\globusMedApp\\WEB-INF\\src\\com\\globus\\common\\beans\\GmStatusLogBean.java

package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmStatusLogBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /*
   * Initialize the Logger Class.This will be removed all place changed with GmDataStoreVO
   * constructor
   */

  public GmStatusLogBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmStatusLogBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * reportStatusLogList - This method will return an ArrayList
   * 
   * @param hmParams
   * @return ArrayList alResult
   * @exception AppError
   */
  public ArrayList reportStatusLogList(HashMap hmParams) throws AppError {

    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strSource = GmCommonClass.parseNull((String) hmParams.get("TYPENUM"));
    String strUser = GmCommonClass.parseNull((String) hmParams.get("USERNUM"));
    // String strType = GmCommonClass.parseNull((String) hmParams.get("STATUSNUM"));
    String strStartDate = GmCommonClass.parseNull((String) hmParams.get("STARTDATE"));
    String strEndDate = GmCommonClass.parseNull((String) hmParams.get("ENDDATE"));
    String strTxnId = GmCommonClass.parseNull((String) hmParams.get("TXNID"));
    String strType = GmCommonClass.parseNull((String) hmParams.get("SOURCE"));
    strSource = (!strSource.equals("0")) ? strSource : "";
    strType = (!strType.equals("0")) ? strType : "";
    strUser = (!strUser.equals("0")) ? strUser : "";
    String strCompDateFrmt = getCompDateFmt();
    String strCompPlantFilter = "";
    strCompPlantFilter =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(getCompId(), "STATUS_LOG_REPORT"));// STATUS_LOG_REPORT-Rule
                                                                                              // Value:=
                                                                                              // plant.
    strCompPlantFilter = strCompPlantFilter.equals("") ? getCompId() : getCompPlantId();
    log.debug("hmParams:" + hmParams);
    // log.debug("--strSource :" + strSource + "--strUser :" + strUser + "--strType :" + strType +
    // "--strStartDate :" + strStartDate + "--strEndDate :"+ strEndDate);
    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT	T905.C905_REF_ID REFID, T901B.C901_CODE_NM REFTYPE, T901A.C901_CODE_NM SOURCE ");
    sbQuery.append(" ,T101.C101_USER_F_NAME || '' || T101.C101_USER_L_NAME UPDUSER ");
    sbQuery.append(" ,T905.C901_SOURCE SOURCEID, TO_CHAR(T905.C905_UPDATED_DATE,'"
        + strCompDateFrmt + "'||' HH24:Mi AM') UPDDATE");
    sbQuery
        .append(" FROM T905_STATUS_DETAILS T905, T101_USER T101,T901_CODE_LOOKUP T901A,T901_CODE_LOOKUP T901B");
    sbQuery.append(" WHERE T905.C905_UPDATED_BY = T101.C101_USER_ID ");
    sbQuery.append(" AND T905.C901_SOURCE = T901A.C901_CODE_ID ");
    sbQuery.append(" AND T905.C901_TYPE = T901B.C901_CODE_ID ");
    sbQuery.append(" and trunc(t905.c905_updated_date)   >= TO_DATE('");
    sbQuery.append(strStartDate);
    sbQuery.append("','" + strCompDateFrmt + "')");
    sbQuery.append(" AND trunc(t905.c905_updated_date)  <= TO_DATE('");
    sbQuery.append(strEndDate);
    sbQuery.append("','" + strCompDateFrmt + "')");
    // Fetch records based company/plant for EDC countries
    sbQuery.append("AND (T905.C1900_company_id  = '" + getCompId()
        + "' OR T905.c1900_company_id  IN(");
    sbQuery
        .append("SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE C5041_PRIMARY_FL = 'Y' AND c5041_void_fl IS NULL AND c5040_plant_id ='"
            + strCompPlantFilter + "')"); //Adding primary_FL for PMT-30285
    sbQuery.append(") ");
    if (!strUser.equals("")) {
      sbQuery.append(" AND T905.C905_UPDATED_BY = '");
      sbQuery.append(strUser);
      sbQuery.append("'");
    }
    if (!strSource.equals("")) {
      sbQuery.append(" AND T905.C901_SOURCE = '");
      sbQuery.append(strSource);
      sbQuery.append("'");
    }
    if (!strType.equals("")) {
      sbQuery.append(" AND T905.C901_TYPE = '");
      sbQuery.append(strType);
      sbQuery.append("'");
    }
    if (!strTxnId.equals("")) {
      sbQuery.append(" AND T905.C905_REF_ID = '");
      sbQuery.append(strTxnId);
      sbQuery.append("'");
    }
    sbQuery.append(" ORDER BY T905.C905_UPDATED_DATE DESC");

    log.debug(" Query to get Status Log Report is " + sbQuery.toString() + "size of AList "
        + alResult.size());
    alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());


    return alResult;

  } // End of fetchPOC.

  /**
   * This method will return an ArrayList Will call the procedure gm_fch_status_log_detail
   * 
   * @param strReqID
   * @return HashMap
   */
  public ArrayList fetchStatusLogDetail(String strRefID) throws Exception {
    log.debug("Enter");
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_status_log.gm_fch_status_log_detail", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRefID);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug(" alResult : " + alResult);

    log.debug("Exit");
    return alResult;
  }

  public void saveStatusDetails(HashMap hmParams) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strRefId = GmCommonClass.parseNull((String) hmParams.get("REFID"));
    String strStatus = GmCommonClass.parseNull((String) hmParams.get("STATUSNUM"));
    String strUser = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strType = GmCommonClass.parseNull((String) hmParams.get("TYPENUM"));
    String strSource = GmCommonClass.parseNull((String) hmParams.get("SOURCE"));
    String strUpdDate = GmCommonClass.parseNull((String) hmParams.get("UPDDATE"));
    log.debug("String strUser: " + strUser);
    gmDBManager.setPrepareString("gm_pkg_cm_status_log.gm_sav_status_details", 7);
    gmDBManager.setString(1, strRefId);
    gmDBManager.setString(2, strStatus);
    gmDBManager.setString(3, strUser);
    gmDBManager.setString(4, "");
    gmDBManager.setString(5, strUpdDate); // today's date
    gmDBManager.setString(6, strType);
    gmDBManager.setString(7, strSource);
    gmDBManager.execute();

    gmDBManager.commit();
    return;
  }// End of saveStatusDetails;

  public void updateStatusDetails(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strRefId = GmCommonClass.parseNull((String) hmParams.get("REFID"));

    String strUser = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    String strType = GmCommonClass.parseNull((String) hmParams.get("TYPENUM"));
    String strSource = GmCommonClass.parseNull((String) hmParams.get("SOURCE"));
    gmDBManager.setPrepareString("gm_pkg_cm_status_log.gm_upd_status_details", 4);

    gmDBManager.setString(1, strRefId);
    gmDBManager.setString(2, strUser);
    gmDBManager.setString(3, strType);
    gmDBManager.setString(4, strSource);
    gmDBManager.execute();

    gmDBManager.commit();
    return;
  }
}
