/**
 * FileName    : GmSearchCriteria.java 
 * Description :
 * Author      : vprasath
 * Date        : Feb 13, 2008 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.DynaBean;
import org.apache.log4j.Logger;

public class GmSearchCriteria {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	//Declare numeric Comparision literals
	public static final int LESSTHAN = -1;
	public static final int LESSTHANOREQUALTO = -2;
	public static final int GREATERTHAN = 1;
	public static final int GREATERTHANOREQUALTO = 2;
	//Declare String Comparision literals
	public static final int EQUALS = 10;
	public static final int LIKE = 11;
	public static final int IN = 12;
	public static final int NOTEQUALS = 13;
	
	public static final int HASHMAP = 1000;
	public static final int DYNABEAN = 1001;
	public static final int STRING = 1002;

	public static final int DATE_TYPE = 102;
	public static final int STRING_TYPE = 101;
	public static final int NUMBER_TYPE = 100;
	
	private String searchKey = "";
	private String searchValue = "";
	private int intOperator = 10;
	private int searchObject =  HASHMAP;
	private int intSearchValueType = NUMBER_TYPE;
	
	private ArrayList alSearchList = new ArrayList();
	
	public String getSearchKey() {
		return searchKey;
	}
	private void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}
	public String getSearchValue() {
		return searchValue;
	}
	private void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	public int getIntOperator() {
		return intOperator;
	}
	private void setIntOperator(int intOperator) {
		this.intOperator = intOperator;
	}
	public int getSearchValueType() {
		return intSearchValueType;
	}
	public void setSearchValueType(int intSearchValueType) {
		this.intSearchValueType = intSearchValueType;
	}
	public void addSearchCriteria(String strKey, String strValue, String operator) {		
		addSearchCriteria(strKey,strValue,Integer.parseInt(operator));
	}
	
	public void addSearchCriteria(String strKey, String strValue, int operator) {	
		GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
		gmSearchCriteria.setSearchKey(strKey);
		gmSearchCriteria.setSearchValue(strValue);
		gmSearchCriteria.setIntOperator(operator);		
		alSearchList.add(gmSearchCriteria);
	}
	
	public void addSearchCriteria(String strKey, String strValue, String operator, int type) {	
		GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
		gmSearchCriteria.setSearchKey(strKey);
		gmSearchCriteria.setSearchValue(strValue);
		gmSearchCriteria.setIntOperator(Integer.parseInt(operator));
		gmSearchCriteria.setSearchValueType(type);
		alSearchList.add(gmSearchCriteria);
	}
	
	public void setSearchObject(int searchObject)
	{
		this.searchObject = searchObject;
	}
	
	private String get(String strKey, Object obj)
	{
		String strResultValue = "";
			
		if(obj instanceof  HashMap)
		{
			strResultValue =  String.valueOf(((HashMap)obj).get(strKey));
		}
		else if (obj instanceof DynaBean)
		{
			strResultValue = String.valueOf(((DynaBean)obj).get(strKey));
		}
		else if(obj instanceof String)
		{
			strResultValue = (String)obj;
		}
   	 
		return strResultValue;
	}
	
	 public List query(List list){
    	 Iterator itrList = list.iterator();    	 
         GmSearchCriteria searchCriteria = null;
         String strSearchValue = "";
         String strValueInHashMap = "";
         Iterator sItr = null;
         Object obj = null;
         int intOperator;
         
         while(itrList.hasNext())
         {        	
        	obj =  itrList.next();
         	sItr = alSearchList.iterator();
         	
         	while(sItr.hasNext())
             {
         		 searchCriteria = (GmSearchCriteria) sItr.next();         		
         		 strSearchValue = searchCriteria.getSearchValue();
         		 strValueInHashMap = get(searchCriteria.getSearchKey(), obj);
         		 intOperator =searchCriteria.getIntOperator();
         		 int intSearchValueType = searchCriteria.getSearchValueType();
	         	  if(!isResultAvailable(strValueInHashMap,strSearchValue,intOperator, intSearchValueType))
	             	{	         		  
	             		itrList.remove();
	             		break;
	             	}        	
             }                             
         }
                  
      return list;
    }

    private double getDoubleValue(String strValue)
    {    	    
    	strValue = strValue == null || strValue.equals("") ? "0" : strValue;	    
    	return Double.parseDouble(strValue);    	
    }
    	
    private boolean isResultAvailable(String strValueInHashMap, String strSearchValue, int intOperator, int intSearchValueType)
    {
    	 boolean bValueFound = false;
    	//log.debug("hmSearchValueType.get('TYPE') = " + hmSearchValueType.get("TYPE"));
    	 switch(intOperator)
  		{         		
  		case GmSearchCriteria.GREATERTHAN:
  			
  			if(intSearchValueType == DATE_TYPE)
  			{
  				bValueFound = GmCalenderOperations.isGreaterThan(strValueInHashMap, strSearchValue, "MM/dd/yyyy");
  			}
  			else if(intSearchValueType == NUMBER_TYPE)
  			{
  				bValueFound = getDoubleValue(strValueInHashMap)  > getDoubleValue(strSearchValue);
  			}
  			else if(intSearchValueType == STRING_TYPE)
  			{
  	  			int result = 0;
  				if(strValueInHashMap != null)
  				{
  					result = strValueInHashMap.compareTo(strSearchValue);
  					if(result > 0)
  						bValueFound = true;
  					else
  						bValueFound = false;
  				}
  			}
  			break;
  		case GmSearchCriteria.GREATERTHANOREQUALTO:
  			if(intSearchValueType == DATE_TYPE)
  			{
  				bValueFound = GmCalenderOperations.isGreaterThan(strValueInHashMap, strSearchValue, "MM/dd/yyyy") || GmCalenderOperations.isEqual(strValueInHashMap, strSearchValue, "MM/dd/yyyy");
  			}
  			else if(intSearchValueType == NUMBER_TYPE)
  			{
  				bValueFound = getDoubleValue(strValueInHashMap)  >= getDoubleValue(strSearchValue);
  			}
  			else if(intSearchValueType == STRING_TYPE)
  			{
  				int result = 0;
  				if(strValueInHashMap != null)
  				{
  					result = strValueInHashMap.compareTo(strSearchValue);
  					if(result >= 0)
  						bValueFound = true;
  					else
  						bValueFound = false;
  				}
  			}
  			break;
  		case GmSearchCriteria.LESSTHAN:
  			if(intSearchValueType == DATE_TYPE)
  			{
  				bValueFound = GmCalenderOperations.isLessThan(strValueInHashMap, strSearchValue, "MM/dd/yyyy");
  			}
  			else if(intSearchValueType == NUMBER_TYPE)
  			{
  				bValueFound = getDoubleValue(strValueInHashMap)  < getDoubleValue(strSearchValue);
  			}
  			else if(intSearchValueType == STRING_TYPE)
  			{
  				int result = 0;
  				if(strValueInHashMap != null)
  				{
  					result = strValueInHashMap.compareTo(strSearchValue);
  					if(result < 0)
  						bValueFound = true;
  					else
  						bValueFound = false;
  				}
  			}
  			break;
  		case GmSearchCriteria.LESSTHANOREQUALTO:
  			if(intSearchValueType == DATE_TYPE)
  			{
  				bValueFound = GmCalenderOperations.isLessThan(strValueInHashMap, strSearchValue, "MM/dd/yyyy") || GmCalenderOperations.isEqual(strValueInHashMap, strSearchValue, "MM/dd/yyyy");
  			}
  			else if(intSearchValueType == NUMBER_TYPE)
  			{
  				bValueFound = getDoubleValue(strValueInHashMap)  <= getDoubleValue(strSearchValue);
  			}
  			else if(intSearchValueType == STRING_TYPE)
  			{
  				int result = 0;
  				if(strValueInHashMap != null)
  				{
  					result = strValueInHashMap.compareTo(strSearchValue);
  					if(result <= 0)
  						bValueFound = true;
  					else
  						bValueFound = false;
  				}
  			}
  			break;
  		case GmSearchCriteria.EQUALS:
  			if(intSearchValueType == DATE_TYPE)
  			{
  				bValueFound = GmCalenderOperations.isEqual(strValueInHashMap, strSearchValue, "MM/dd/yyyy");
  			}
  			else if(intSearchValueType == NUMBER_TYPE)
  			{
  				try
  				{
  					bValueFound = getDoubleValue(strValueInHashMap)  == getDoubleValue(strSearchValue);
  				}
  				catch(NumberFormatException nfe)
  				{
  					bValueFound = strValueInHashMap.equalsIgnoreCase(strSearchValue);	
  				}
  			}
  			else if(intSearchValueType == STRING_TYPE)
  			{
  				bValueFound = strValueInHashMap.equals(strSearchValue);
  			}
  			break;  
  		case GmSearchCriteria.NOTEQUALS:	
  			if(intSearchValueType == DATE_TYPE)
  			{
  				bValueFound = !GmCalenderOperations.isEqual(strValueInHashMap, strSearchValue, "MM/dd/yyyy");
  			}
    	 	else if(intSearchValueType == NUMBER_TYPE)
  			{
  				try
  				{
  					bValueFound = getDoubleValue(strValueInHashMap)  != getDoubleValue(strSearchValue);
  				}
  				catch(NumberFormatException nfe)
  				{
  					bValueFound = !(strValueInHashMap.equals(strSearchValue));	
  				}
  			}
    	 	else if(intSearchValueType == STRING_TYPE)
    	 	{
    	 		bValueFound = !(strValueInHashMap.equalsIgnoreCase(strSearchValue));
    	 	}
  			break;  
  		case GmSearchCriteria.IN:
  			if(intSearchValueType == STRING_TYPE)
  			{
  				String strArr[] = strSearchValue.split(",");
  				for(int i=0;i< strArr.length;i++)
  				{
  					if(strValueInHashMap.equalsIgnoreCase(strArr[i]))
  					{
  						bValueFound = true;
  						break;
  					}
  				}
  			}
  			break;
  		case GmSearchCriteria.LIKE:
  			String strArray[] = strSearchValue.split(",");
  			for(int i=0;i< strArray.length;i++)
  			{  				
  				if(strValueInHashMap.startsWith((strArray[i])))
  				{
  					bValueFound = true;
  					break;
  				}
  			}  			
  			break;	
  		}
    	
    	//log.debug(strValueInHashMap + " is " + intOperator + " than " + strSearchValue + " | result = " + bValueFound);
    	return bValueFound;
    }

    public String toString()
    {
    	return searchKey + " " + intOperator + " " + searchValue + " | " + intSearchValueType;
    }
}
