package com.globus.common.beans;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;

/*Name:GmCommonApplication Bean
 * Use:To get properties values for Upload DO Document.
 * 
 * */
/**
 * @author pvigneshwaran
 *
 */
public class GmCommonApplicationBean extends GmBean{
	  public GmCommonApplicationBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	public static Logger log = GmLogger
		      .getInstance("com.globus.common.beans.GmCommonApplicationBean");
		  public static ResourceBundle rbApplicationConfig;
		  public static ResourceBundle rbLotExpiryConfig;
		  
		  /**
		   * getApplicationConfig - This method returns a string from  Properties file based on
		   * the input as a keyword
		   * 
		   * @param strKey - Keyword
		   * @return String object
		   * @throws AppError
		   */

		  public static String getApplicationConfig(String strKey) throws AppError {
		    if (rbApplicationConfig== null) {
		    	rbApplicationConfig = ResourceBundle.getBundle(System.getProperty("ENV_APP_CONF"));
		    }
		 
		    return rbApplicationConfig.getString(strKey);
		  } // End 
		 
		  /**
		   * getLotExpiryConfig - This method returns a string from  Properties file based on
		   * the input as a keyword
		   * @param strKey
		   * @return
		   * @throws AppError
		 */
		public static String getLotExpiryConfig(String strKey) throws AppError {
			    if (rbLotExpiryConfig== null) {
			    	rbLotExpiryConfig = ResourceBundle.getBundle(System.getProperty("ENV_LOT_CONF")); //("LotExpiry")
			    }
			    return rbLotExpiryConfig.getString(strKey);
			  } // End 

}
