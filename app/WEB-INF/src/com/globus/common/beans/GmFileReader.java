/**************************************************************************************************
 * File							: GmFileReader.java
 * Screen ID					: Demo Screen
 * Desc							: This is the File reading utility
 * Version						: 1.0
 *************************************************************************************************/
package com.globus.common.beans;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
/*import java.net.InetAddress;*/
import java.net.URL;

/**
 * This is the File reading utility class which is used for reading comma separated files, the application
 * provides a method to read a file and store the values in the format of ArrayList using File, BufferedReader and FileReader. 
 * 
 */

public class GmFileReader {
	// This method is used for reading the .CVS file and return the ArrayList
	public ArrayList readFile(String infile, int element_no) throws IOException {
		String data = "";
		File file = new File(infile);

		BufferedReader bufRdr;
		ArrayList outside = new ArrayList();
		bufRdr = new BufferedReader(new FileReader(file));
		while ((data = bufRdr.readLine()) != null) {
			String[] arr = data.split(",");
			ArrayList inside = new ArrayList(Arrays.asList(arr));
			if (arr.length != element_no) {
				for (int i = arr.length; i <= element_no; i++) {
					inside.add("");
				}
			}

			outside.add(inside);

		}
		bufRdr.close();
		return outside;
	}
	
	public File findFile(String rootDir, String fileNamePattern)
	{
		File file = null;
		File searchDir = new File(rootDir);
		File dirFiles[] =  searchDir.listFiles();
		
		if(dirFiles != null)
		{

			for (int i = 0; i < dirFiles.length; ++i) { 

			if (dirFiles[i].getName().toLowerCase().contains(fileNamePattern.toLowerCase())) { 
				file =dirFiles[i];
				break;
			} 
		}
		
	}
		return file;
	
} 
	
	public String getContentType(File f)
	{
		String strContentType = "";
		String strFileName = "";
		if (f == null)
		{
			return strContentType;
		}
		strFileName = f.getName().toLowerCase();
		if(strFileName.endsWith("pdf"))
		{
			strContentType = "application/pdf";
		}
		else if(strFileName.endsWith("docx") || strFileName.endsWith("doc"))
		{
			strContentType = "application/vnd.ms-word";
		}
		else if(strFileName.endsWith("xlsx") || strFileName.endsWith("xls"))
		{
			strContentType = "application/vnd.ms-excel";
		}
		
		return strContentType;
	}
	
	public final static byte[] load(String fileName)
	  {
	    try { 
	      FileInputStream fin=new FileInputStream(fileName);
	      int size = (int)new File(fileName).length();
	      return load(fin,size);
	    }
	    catch (Exception e) {
	 
	      return new byte[0];
	    }
	  }

	  public final static byte[] load(File file)
	  {
	    try { 
	      FileInputStream fin=new FileInputStream(file);
	      int size = (int)file.length();
	      return load(fin,size);
	    }
	    catch (Exception e) {
	     
	      return new byte[0];
	    }
	  }

	  private final static byte[] load(FileInputStream fin,int size)
	  {
	    byte readBuf[] = new byte[size];
	  
	    try { 
	      ByteArrayOutputStream bout = new ByteArrayOutputStream();
	    
	      int readCnt = fin.read(readBuf);
	      while (0 < readCnt) {
	        bout.write(readBuf, 0, readCnt);
	        readCnt = fin.read(readBuf);
	      }
	      
	      fin.close();
	      
	      return bout.toByteArray();
	    }
	    catch (Exception e) {
	     
	      return new byte[0];
	    }
	  }
	  
	  public static String findVersionInfo() throws IOException {
		
		    /*InetAddress iadd = InetAddress.getLocalHost();
		    String strSystemNm = iadd.toString().toLowerCase();
		    String[] SystName = new String[2];
		    SystName = strSystemNm.split("/");
		    strSystemNm = SystName[0];*/
		    
		    // Get property from JBOSS Server environment
		      String strSystemNm = System.getProperty("HOST_NAME");
		      
		    String strFilePath = GmCommonClass.getString("MANIFEST_VERSION_DETAILS");
		    String strCompanyName = GmCommonClass.strCompanyName;
		    //String strServerName = GmCommonClass.strServerName;
		    String strManifestUrlPath = GmCommonClass.getString("MANIFEST_URL_PATH");
		    String strManifestDetails = GmCommonClass.getString("MANIFEST_DETAILS");
		    //String strHost = GmCommonClass.getString("HOST");
		    String strImplementationVersion = "";
		    String strImplementationTitle = "";
		    String strKey;
		    String[] strSplitArr;
		    
			   
		   Enumeration<URL> resources = Thread.currentThread().getContextClassLoader().getResources(strFilePath);
			    while (resources.hasMoreElements()) {
			    	URL manifestUrl = resources.nextElement();
			        if(manifestUrl.getPath().indexOf(strManifestUrlPath) != -1) {
			        	Manifest manifest = new Manifest(manifestUrl.openStream());
				        Attributes mainAttributes = manifest.getMainAttributes();
				        strImplementationTitle = mainAttributes.getValue("Implementation-Title");
				        Iterator itr = mainAttributes.keySet().iterator();
				      
				        while (itr.hasNext()){
				        	  strKey = itr.next().toString();
				        	  strSplitArr =  strManifestDetails.split("[\\|\\s]+");
				        	for(String s: strSplitArr){
				        	 if (strKey.equals(s) && !strKey.isEmpty()){
				        		 if(strKey.equals("Implementation-Version")){
				        			 strImplementationVersion += "<span>Imp-Version :" + "&nbsp;" + mainAttributes.getValue(strKey) + "</span>";
				        		 }
				        		 else{
				        			 strImplementationVersion += "<span>"+  strKey + ":" + "&nbsp;" + mainAttributes.getValue(strKey) + "</span>";
				        		 }
				        		
				       		   }
				        	}
				        }
				    }
			    }
			    strImplementationVersion += "<span>Host" + "&nbsp;" + ":" + "&nbsp;" + strSystemNm + "</span>" ;
			    return strImplementationVersion ;
			}

	  
}
