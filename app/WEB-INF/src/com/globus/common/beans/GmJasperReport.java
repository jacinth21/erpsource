package com.globus.common.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXhtmlExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

import org.apache.log4j.Logger;

/**
 * 
 * @author axiang
 * @version 05/29/2008
 */
public class GmJasperReport {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  private String strJasperReportName = "";
  private HashMap hmReportParameters = null;
  private List reportDataList = null;
  private HttpServletRequest request = null;
  private HttpServletResponse response = null;
  private String strJasperRealLocation = "";
  private int intpageHeight = 970;
  // In JBOSS server After export Jasper reports not show the Print button.
  private boolean blDisplayImage = false;
  // To resolve the WO's sign image printing issue.PBUG-66.
  private boolean blUseNewSession = false;
  // To load dynamic resource bundle for Paperwork properties
  private ResourceBundle rbPaperwork = null;


  public boolean isBlUseNewSession() {
    return blUseNewSession;
  }

  public void setBlUseNewSession(boolean blUseNewSession) {
    this.blUseNewSession = blUseNewSession;
  }

  public boolean isBlDisplayImage() {
    return blDisplayImage;
  }

  public void setBlDisplayImage(boolean blDisplayImage) {
    this.blDisplayImage = blDisplayImage;
  }

  public void setJasperReportName(String strReportName) {
    this.strJasperReportName = strReportName;
  }

  public String getJasperReportName() {
    return strJasperReportName;
  }

  public void setHmReportParameters(HashMap hmParameters) {
    this.hmReportParameters = hmParameters;
  }

  public void setPageHeight(int intPHeight) {
    this.intpageHeight = intPHeight;
  }

  public HashMap getHmReportParameters() {
    return hmReportParameters;
  }

  public void setReportDataList(List dataList) {
    this.reportDataList = dataList;
  }

  public List getReportDataList() {
    return reportDataList;
  }

  public HttpServletRequest getRequest() {
    return request;
  }

  public void setRequest(HttpServletRequest request) {
    this.request = request;
  }

  public HttpServletResponse getResponse() {
    return response;
  }

  public void setResponse(HttpServletResponse response) {
    this.response = response;
  }



  /**
   * @return the rbPaperwork
   */
  public ResourceBundle getRbPaperwork() {
    return rbPaperwork;
  }

  /**
   * @param rbPaperwork the rbPaperwork to set
   */
  public void setRbPaperwork(ResourceBundle rbPaperwork) {
    this.rbPaperwork = rbPaperwork;
  }

  /**
   * in the future, when we use another jasper location, "path", and set it in the
   * method:setJasperRealLocation(String path), instead of returns this default
   * strJasperRealLocation, it will return the value of "path";
   */
  public String getJasperRealLocation() {
    if (strJasperRealLocation == null || strJasperRealLocation.equals("")) {
      strJasperRealLocation =
          request.getSession().getServletContext()
              .getRealPath(GmCommonClass.getString("GMJASPERLOCATION"));
    }
    return strJasperRealLocation;
  }

  public void setJasperRealLocation(String path) {
    this.strJasperRealLocation = path;
  }

  /**
   * @method createJasperReport - This method used to create the HTML Jasper report.
   * @Use - When to call this method to view the HTML jasper report in the screen.
   * @Do - After called this method don't redirected any class or JSP.(Set return as null).
   * @exception AppError
   */
  public void createJasperReport() throws Exception {
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    JasperPrint jp = null;
    JRExporter exporter = null;


    InputStream reportStream;
    ServletOutputStream servletOutputStream = response.getOutputStream();
    reportStream =
        request.getSession().getServletContext()
            .getResourceAsStream(strJasperPath + strJasperReportName);
    hmReportParameters.put("JASPERIMAGEURL", GmCommonClass.jasperImageURL);
    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }
    if (reportDataList != null) {
      jp =
          JasperFillManager.fillReport(reportStream, hmReportParameters,
              new JRBeanCollectionDataSource(reportDataList));
    } else {
      /*
       * if there is/are subreport(s) in detail section and we don't want to print out duplicated
       * sub reports, set this value as "new JREmptyDataSource()" when use reportDataList in the
       * detail section.
       */

      jp = JasperFillManager.fillReport(reportStream, hmReportParameters, new JREmptyDataSource());
    }

    jp.setPageHeight(intpageHeight);

    exporter = new JRHtmlExporter();

    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);

    request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jp);
    exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, new HashMap());

    exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath()
        + "/GmImageServlet?time=" + new java.util.Date().getTime() + "&image=");
    exporter.exportReport();

    servletOutputStream.close();
  }

  public void createJasperHtmlReport() throws Exception {
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    JasperPrint jp = null;
    JRExporter exporter = null;

    InputStream reportStream;

    PrintWriter out = response.getWriter();

    StringWriter destination = new StringWriter();

    reportStream =
        request.getSession().getServletContext()
            .getResourceAsStream(strJasperPath + strJasperReportName);
    hmReportParameters.put("JASPERIMAGEURL", GmCommonClass.jasperImageURL);
    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }
    if (reportDataList != null) {
      jp =
          JasperFillManager.fillReport(reportStream, hmReportParameters,
              new JRBeanCollectionDataSource(reportDataList));
    } else {
      /*
       * if there is/are subreport(s) in detail section and we don't want to print out duplicated
       * sub reports, set this value as "new JREmptyDataSource()" when use reportDataList in the
       * detail section.
       */

      jp = JasperFillManager.fillReport(reportStream, hmReportParameters, new JREmptyDataSource());
    }

    jp.setPageHeight(intpageHeight);

    exporter = new JRHtmlExporter();

    request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jp);
    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
    exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, destination);

    request.getSession().setAttribute("IMAGES_MAP", new HashMap());

    exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, new HashMap());
    // images should be load as dynamically, so append the time parameter.
    exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath()
        + "/GmImageServlet?time=" + new java.util.Date().getTime() + "&image=");

    exporter.exportReport();

    response.setHeader("Cache-Control", "no-store");
    response.setHeader("Pragma", "no-cache");
    response.setDateHeader("Expires", 0);
    out.println("<meta http-equiv=\"Content-Type\"content=\"text/html; charset=iso-8859-1\">");
    out.println(destination.toString());

    out.flush();
    // out.close(); Committed for BUG-3801
  }

  public void createJasperPdfReport() throws Exception {
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    JasperPrint jp = null;

    InputStream reportStream;

    response.setHeader("Content-Disposition", "attachment;filename=download.pdf");

    ServletOutputStream servletOutputStream = response.getOutputStream();
    reportStream =
        request.getSession().getServletContext()
            .getResourceAsStream(strJasperPath + strJasperReportName);
    hmReportParameters.put("JASPERIMAGEURL", GmCommonClass.jasperImageURL);
    hmReportParameters.put("IS_IGNORE_PAGINATION", Boolean.FALSE);
    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }
    if (reportDataList != null) {
      jp =
          JasperFillManager.fillReport(reportStream, hmReportParameters,
              new JRBeanCollectionDataSource(reportDataList));
    } else {
      /*
       * if there is/are subreport(s) in detail section and we don't want to print out duplicated
       * sub reports, set this value as "new JREmptyDataSource()" when use reportDataList in the
       * detail section.
       */
      jp = JasperFillManager.fillReport(reportStream, hmReportParameters, new JREmptyDataSource());
    }



    byte[] pdfByteArray = JasperExportManager.exportReportToPdf(jp);

    servletOutputStream.write(pdfByteArray, 0, pdfByteArray.length);
    servletOutputStream.flush();

    servletOutputStream.close();
  }

  public String getHtmlReport() throws JRException, FileNotFoundException, IOException {
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    JasperPrint jp = null;
    JRExporter exporter = null;
    StringBuffer strBuffer = new StringBuffer();

    InputStream reportStream;
    if(hmReportParameters != null){
        hmReportParameters.put("JASPERIMAGEURL", GmCommonClass.jasperImageURL);
        hmReportParameters.put("CONTEXTURL", GmCommonClass.contextURL);
     }    
    File file1 = new File(GmCommonClass.getString("GMJASPERTEMPLATEPATH") + strJasperReportName);
    if (request == null || file1.exists()) {
      String jasperPath = GmCommonClass.getString("GMJASPERTEMPLATEPATH");
      reportStream = new FileInputStream(jasperPath + strJasperReportName);
      log.debug("Jasper location = " + jasperPath + strJasperReportName);
    } else {
      reportStream =
          request.getSession().getServletContext()
              .getResourceAsStream(strJasperPath + strJasperReportName);
      log.debug("Jasper location = " + strJasperPath + strJasperReportName);
    }
    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }

    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }

    if (reportDataList != null) {
      jp =
          JasperFillManager.fillReport(reportStream, hmReportParameters,
              new JRBeanCollectionDataSource(reportDataList));
    } else {
      /*
       * if there is/are subreport(s) in detail section and we don't want to print out duplicated
       * sub reports, set this value as "new JREmptyDataSource()" when use reportDataList in the
       * detail section.
       */

      jp = JasperFillManager.fillReport(reportStream, hmReportParameters, new JREmptyDataSource());
    }

    jp.setPageHeight(intpageHeight);


    exporter = new JRHtmlExporter();

    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
    exporter.setParameter(JRExporterParameter.OUTPUT_STRING_BUFFER, strBuffer);

    // request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jp);
    // exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, new HashMap());

    // exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath() +
    // "/GmImageServlet?image=");
    // exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, getImageURI());

    // To resolve the WO's sign image printing issue.PBUG-66.
    String strSessionAttr = null;
    if (blUseNewSession) {
      request.getSession().setAttribute(
          ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE + new java.util.Date().getTime(), jp);
      strSessionAttr =
          "&jrprint=" + ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE
              + new java.util.Date().getTime();
    }

    // when its called from JSP page - show the Jasper reports Images.
    if (blDisplayImage) {
      request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jp);
      exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, new HashMap());

      exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath()
          + "/GmImageServlet?time=" + new java.util.Date().getTime() + strSessionAttr + "&image=");
    } else {
      exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, new Boolean(false));
    }
    exporter.exportReport();
    return strBuffer.toString();
  }

  /**
   * @method exportJasperReportToHtml - This method used to create the HTML File.
   * @param String
   * @exception AppError
   */
  public void exportJasperReportToHtml(String strDestFileName) throws Exception {
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    JasperPrint jp = null;

    InputStream reportStream;
    JRExporter exporter = new JRHtmlExporter();
    reportStream =
        request.getSession().getServletContext()
            .getResourceAsStream(strJasperPath + strJasperReportName);
    hmReportParameters.put("JASPERIMAGEURL", GmCommonClass.jasperImageURL);
    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }
    if (reportDataList != null) {
      jp =
          JasperFillManager.fillReport(reportStream, hmReportParameters,
              new JRBeanCollectionDataSource(reportDataList));
    } else {
      /*
       * if there is/are subreport(s) in detail section and we don't want to print out duplicated
       * sub reports, set this value as "new JREmptyDataSource()" when use reportDataList in the
       * detail section.
       */
      jp = JasperFillManager.fillReport(reportStream, hmReportParameters, new JREmptyDataSource());
    }

    jp.setPageHeight(intpageHeight);

    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, strDestFileName);
    exporter.exportReport();
    reportStream.close();
  }

  public HashMap sendJasperMail(String strReportName, HashMap hmParams, ArrayList alReportData,
      GmEmailProperties emailProperties, HttpServletRequest request) throws Exception {
    HashMap hmReturn = new HashMap();
    try {
      String strMessageDesc = "Original Mail Not Found.";
      // added for subreport path when using subreports in Jasper email
      if (hmParams != null) {
        hmParams.put(
            "SUBREPORT_DIR",
            request.getSession().getServletContext()
                .getRealPath(GmCommonClass.getString("GMJASPERLOCATION")));
        hmParams.put("CONTEXTURL", GmCommonClass.contextURL);
      }      
      setJasperReportName(strReportName);
      setHmReportParameters(hmParams);
      setReportDataList(alReportData);
      setRequest(request);
      strMessageDesc = getHtmlReport();

      hmReturn.put("ORIGINALMAIL", strMessageDesc);

      emailProperties.setMessage(strMessageDesc);

      log.debug("Sending jasper mail to : " + emailProperties);
      GmCommonClass.sendMail(emailProperties);

    } catch (Exception e) {
      e.printStackTrace();
      hmReturn.put("EXCEPTION", e);
    }
    return hmReturn;
  }

  /**
   * @method exportJasperReportToPdf - This method used to create the PDF file.
   * @param String
   * @exception AppError
   */
  public void exportJasperReportToPdf(String strDestFileName) throws Exception {
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    JasperPrint jp = null;
    JRExporter exporter = new JRPdfExporter();
    InputStream reportStream;

    reportStream =
        request.getSession().getServletContext()
            .getResourceAsStream(strJasperPath + strJasperReportName);
    hmReportParameters.put("IS_IGNORE_PAGINATION", Boolean.FALSE);
    hmReportParameters.put("JASPERIMAGEURL", GmCommonClass.jasperImageURL);
    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }
    if (reportDataList != null) {
      jp =
          JasperFillManager.fillReport(reportStream, hmReportParameters,
              new JRBeanCollectionDataSource(reportDataList));
    } else {
      /*
       * if there is/are subreport(s) in detail section and we don't want to print out duplicated
       * sub reports, set this value as "new JREmptyDataSource()" when use reportDataList in the
       * detail section.
       */
      jp = JasperFillManager.fillReport(reportStream, hmReportParameters, new JREmptyDataSource());
    }

    // jp.setPageHeight(intpageHeight);
    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, strDestFileName);
    exporter.exportReport();
    reportStream.close();
  }

  public void exportJasperReportToRTF() throws Exception {
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    JasperPrint jp = null;

    InputStream reportStream;

    response.setHeader("Content-Disposition", "attachment;filename=download.rtf");
    ServletOutputStream servletOutputStream = response.getOutputStream();
    File file1 = new File(GmCommonClass.getString("GMJASPERTEMPLATEPATH") + strJasperReportName);

    if (request == null || file1.exists()) {
      String jasperPath = GmCommonClass.getString("GMJASPERTEMPLATEPATH");
      reportStream = new FileInputStream(jasperPath + strJasperReportName);

    } else {
      reportStream =
          request.getSession().getServletContext()
              .getResourceAsStream(strJasperPath + strJasperReportName);

    }
    hmReportParameters.put("IS_IGNORE_PAGINATION", Boolean.FALSE);
    hmReportParameters.put("JASPERIMAGEURL", GmCommonClass.jasperImageURL);
    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }
    if (reportDataList != null) {

      jp =
          JasperFillManager.fillReport(reportStream, hmReportParameters,
              new JRBeanCollectionDataSource(reportDataList));

    } else {
      jp = JasperFillManager.fillReport(reportStream, hmReportParameters, new JREmptyDataSource());
    }

    JRRtfExporter exporter = new JRRtfExporter();
    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);

    exporter.exportReport();
    servletOutputStream.close();
  }

  public void exportJasperToPdf(String strDestFileName) throws Exception {
    String strJasperPath = GmCommonClass.getString("GMJASPERFILELOCATION") + strJasperReportName;
    JasperPrint jp = null;
    JRExporter exporter = new JRPdfExporter();
    hmReportParameters.put("IS_IGNORE_PAGINATION", Boolean.FALSE);
    hmReportParameters.put("JASPERIMAGEURL", GmCommonClass.jasperImageURL);
    InputStream inputStream;
    File file1 = new File(GmCommonClass.getString("GMJASPERTEMPLATEPATH") + strJasperReportName);
    if (file1.exists()) {
      String jasperPath = GmCommonClass.getString("GMJASPERTEMPLATEPATH");
      inputStream = new FileInputStream(jasperPath + strJasperReportName);
      log.debug("com location = " + jasperPath + strJasperReportName);
    } else {
      inputStream = getClass().getResourceAsStream(strJasperPath);
      log.debug("Jasper location = " + strJasperPath + strJasperReportName);
    }
    try {

      // Properties values setting to Hash map
      if (rbPaperwork != null) {
        hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
      }

      if (reportDataList != null) {
        jp =
            JasperFillManager.fillReport(inputStream, hmReportParameters,
                new JRBeanCollectionDataSource(reportDataList));
      } else {
        /*
         * if there is/are subreport(s) in detail section and we don't want to print out duplicated
         * sub reports, set this value as "new JREmptyDataSource()" when use reportDataList in the
         * detail section.
         */
        jp = JasperFillManager.fillReport(inputStream, hmReportParameters, new JREmptyDataSource());
      }
      // JasperExportManager.exportReportToPdfFile(jp,strDestFileName);
      exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
      exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, strDestFileName);
      exporter.exportReport();
      inputStream.close();
    } catch (Exception ex) {
      ex.printStackTrace();
      log.error("Exception while export to pdf " + ex);
      throw ex;
    } finally {
      // releases system resources associated with this stream
      if (inputStream != null)
        inputStream.close();
    }
  }

  /**
   * @method getXHtmlReport - This method is used for watermark and frames inside the jasper.
   * @exception JRException,FileNotFoundException,IOException
   */
  public String getXHtmlReport() throws JRException, FileNotFoundException, IOException {
    log.debug("Its me XHTML");
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    JasperPrint jp = null;
    // JRExporter exporter = null;
    JRXhtmlExporter xhtmlExporter = null;
    StringBuffer strBuffer = new StringBuffer();
    if(hmReportParameters != null){
        hmReportParameters.put("JASPERIMAGEURL", GmCommonClass.jasperImageURL);
     }
    InputStream reportStream;

    File file1 = new File(GmCommonClass.getString("GMJASPERTEMPLATEPATH") + strJasperReportName);
    if (request == null || file1.exists()) {
      String jasperPath = GmCommonClass.getString("GMJASPERTEMPLATEPATH");
      reportStream = new FileInputStream(jasperPath + strJasperReportName);
      log.debug("Jasper location = " + jasperPath + strJasperReportName);
    } else {
      reportStream =
          request.getSession().getServletContext()
              .getResourceAsStream(strJasperPath + strJasperReportName);
      log.debug("Jasper location = " + strJasperPath + strJasperReportName);
    }
    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }

    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }

    if (reportDataList != null) {
      jp =
          JasperFillManager.fillReport(reportStream, hmReportParameters,
              new JRBeanCollectionDataSource(reportDataList));
    } else {
      /*
       * if there is/are subreport(s) in detail section and we don't want to print out duplicated
       * sub reports, set this value as "new JREmptyDataSource()" when use reportDataList in the
       * detail section.
       */

      jp = JasperFillManager.fillReport(reportStream, hmReportParameters, new JREmptyDataSource());
    }

    jp.setPageHeight(intpageHeight);


    xhtmlExporter = new JRXhtmlExporter();

    xhtmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
    xhtmlExporter.setParameter(JRExporterParameter.OUTPUT_STRING_BUFFER, strBuffer);

    // request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jp);
    // exporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, new HashMap());

    // exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath() +
    // "/GmImageServlet?image=");
    // exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, getImageURI());

    // To resolve the WO's sign image printing issue.PBUG-66.
    String strSessionAttr = null;
    if (blUseNewSession) {
      request.getSession().setAttribute(
          ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE + new java.util.Date().getTime(), jp);
      strSessionAttr =
          "&jrprint=" + ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE
              + new java.util.Date().getTime();
    }

    // when its called from JSP page - show the Jasper reports Images.
    if (blDisplayImage) {
      request.getSession().setAttribute(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jp);
      xhtmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_MAP, new HashMap());

      xhtmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, request.getContextPath()
          + "/GmImageServlet?time=" + new java.util.Date().getTime() + strSessionAttr + "&image=");
    } else {
      xhtmlExporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, new Boolean(
          false));
    }
    //below code is used to prevent the multipage issue 
    xhtmlExporter.setParameter(JRHtmlExporterParameter.BETWEEN_PAGES_HTML, 
    		"<div style='page-break-after:always'></div>"); 
    
    xhtmlExporter.exportReport();
    return strBuffer.toString();
  }


  /**
   * @method exportJasperReportByType - This method is used export jasper report by
   *         type(PDF,EXCEl,RTF).
   * @exception JRException,FileNotFoundException,IOException
   */
  public void exportJasperReportByType(String strFileName, String strFileType) throws JRException,
      FileNotFoundException, IOException {
    String strJasperPath = GmCommonClass.getString("GMJASPERLOCATION");
    String strFileSuffix = "";
    JasperPrint jp = null;

    InputStream reportStream;
    JRExporter exporter;
    if (strFileType.equals("PDF")) { // For PDF Export
      strFileSuffix = ".pdf";
      exporter = new JRPdfExporter();
      exporter.setParameter(JRPdfExporterParameter.PDF_JAVASCRIPT, "this.zoom = 100;");
    } else if (strFileType.equals("EXCEL")) { // For Excel Export
      strFileSuffix = ".xlsx";
      exporter = new JRXlsxExporter();

      exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER, Boolean.FALSE);
      exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
      exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
          Boolean.FALSE);
      exporter.setParameter(JRXlsExporterParameter.IS_COLLAPSE_ROW_SPAN, Boolean.TRUE);

      exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS,
          Boolean.FALSE);
      exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
      exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);

      exporter.setParameter(JRXlsExporterParameter.IS_IMAGE_BORDER_FIX_ENABLED, Boolean.TRUE);
      exporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, Boolean.FALSE);

      exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_GRAPHICS, Boolean.FALSE);
    } else { // For RTF Export
      strFileSuffix = ".rtf";
      exporter = new JRRtfExporter();

    }

    response.setHeader("Content-Disposition", "attachment;filename=" + strFileName + strFileSuffix);
    ServletOutputStream servletOutputStream = response.getOutputStream();
    File file1 = new File(GmCommonClass.getString("GMJASPERTEMPLATEPATH") + strJasperReportName);

    if (request == null || file1.exists()) {
      String jasperPath = GmCommonClass.getString("GMJASPERTEMPLATEPATH");
      reportStream = new FileInputStream(jasperPath + strJasperReportName);

    } else {
      reportStream =
          request.getSession().getServletContext()
              .getResourceAsStream(strJasperPath + strJasperReportName);

    }
    hmReportParameters.put("IS_IGNORE_PAGINATION", Boolean.FALSE);
    hmReportParameters.put("JASPERIMAGEURL", GmCommonClass.jasperImageURL);
    // Properties values setting to Hash map
    if (rbPaperwork != null) {
      hmReportParameters.put(JRParameter.REPORT_RESOURCE_BUNDLE, rbPaperwork);
    }
    if (reportDataList != null) {

      jp =
          JasperFillManager.fillReport(reportStream, hmReportParameters,
              new JRBeanCollectionDataSource(reportDataList));

    } else {
      jp = JasperFillManager.fillReport(reportStream, hmReportParameters, new JREmptyDataSource());
    }


    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);

    exporter.exportReport();
    servletOutputStream.close();
  }
}
