/******************************************************************************
* File						: PopChartUtils.java
* Screen ID					:
* Desc						: This class is a helper component for
* 							  generating graphs on the fly using
*							  the Popchart Image Server pro
* Version					: V1.0
******************************************************************************/

package com.globus.common.beans;

import java.util.*;
import java.util.HashMap;
import java.util.ArrayList;

import com.globus.common.beans.GmCommonClass; 

import com.corda.CordaEmbedder;

public class PopChartUtils
{
	private String strPopFileName;
	private String strOutputType;
	private String strpcScript; 
	private int intWidth; 
	private int intHeight;
	private double dblTotalSales = 0.0;
	private double dblDailyAverage = 0.0;
	
	HashMap hmReturn = new HashMap();
	
	public void setPopFileName(String strValue)
	{
		strPopFileName = strValue;
	}
	
	public void setOutputType(String strValue)
	{
		strOutputType = strValue;
	}
	
	public void setPCScript(String strValue)
	{
		strpcScript = strValue;
	}
	public void setHeight(int intValue)
	{
		intHeight = intValue;
	}
	
	public void setWidth(int intValue)
	{
		intWidth = intValue;
	}
	
	public double getTotalAmt()
	{
		return dblTotalSales;
	}

	public double getAvgAmt()
	{
		return dblDailyAverage;
	}

	// Builds the US Map  
	// Takes the Arraylist of value as inparameter
	// Assumes Arrayliss contains hashmap 
	// Hash map has following key CD & AMT
	public String buildUSMapScript(ArrayList alresult)
	{
		StringBuffer sbSateScript = new StringBuffer();
		
		
		// To get the state Sales Information
		sbSateScript.append("main.ParamDelimiter(|)");
		sbSateScript.append("US.setShapeValues(");
		for (int d=0;d<alresult.size();d++)
		{
			hmReturn = (HashMap)alresult.get(d);
			sbSateScript.append(hmReturn.get("CD"));
			sbSateScript.append("|");
			sbSateScript.append(hmReturn.get("AMT"));
			sbSateScript.append(";");
		}
		sbSateScript.append(")");
		// State Information
		sbSateScript.append("main.ParamDelimiter(,)");
		
		return sbSateScript.toString();
	}

	// Builds the Bar Map  
	// Takes the Arraylist of value as inparameter
	// Assumes Arrayliss contains hashmap 
	// Hash map has following key CD & NAME
	// Note:-  method to be modified to support multiple group
	// Currently it intGroup and  intItem value not used and shoudl be passed as 1
	public String buildBarChartScript(ArrayList alresult, String strgraphName, 
			int intGroup, int intItem )
	{
		
		StringBuffer sbScriptGrp = new StringBuffer();
		StringBuffer sbScriptItem = new StringBuffer();
		
		// To get the Top Agent Information
		sbScriptGrp.append(strgraphName + ".setcategories(");
		sbScriptItem.append(strgraphName + ".setseries(Item 1;");
		for (int e=0;e<alresult.size();e++)
		{
			hmReturn = (HashMap)alresult.get(e);
			sbScriptGrp.append(getSplitName((String)hmReturn.get("NAME"),30));
			sbScriptGrp.append(";");
			sbScriptItem.append(hmReturn.get("AMT"));
			sbScriptItem.append(";");
		}
		sbScriptGrp.append(")");
		sbScriptItem.append(")");
		
		return sbScriptGrp.toString() + sbScriptItem.toString() ;
	}

	// Builds the Pie Chart  
	// Takes the Arraylist of value as inparameter
	// Assumes Arrayliss contains hashmap 
	// Hash map has following key CD & NAME
	// Note:-  method to be modified to support multiple group
	// Currently it intGroup and  intItem value not used and shoudl be passed as 1
	public String buildPieChartScript(ArrayList alresult, String strgraphName, 
			int intGroup, int intItem )
	{
		
		StringBuffer sbScriptGrp = new StringBuffer();
		StringBuffer sbScriptItem = new StringBuffer();
		
		// To get the Top Agent Information
		sbScriptGrp.append(strgraphName + ".setcategories()");
		
		for (int e=0;e<alresult.size();e++)
		{
			hmReturn = (HashMap)alresult.get(e);
			sbScriptItem.append(strgraphName + ".setseries(");
			sbScriptItem.append((String)hmReturn.get("NAME"));
			sbScriptItem.append(";");
			sbScriptItem.append(hmReturn.get("AMT"));
			sbScriptItem.append(")");
		}
		
		return sbScriptGrp.toString() + sbScriptItem.toString() ;
	}
	
	// Builds the Line Chart
	// Takes the Arraylist of value as inparameter
	// Assumes Arrayliss contains hashmap 
	// Hash map has following key CD & NAME
	// Note:-  method to be modified to support multiple group
	// Currently it intGroup and  intItem value not used and shoudl be passed as 1
	public String buildLineChartScript(ArrayList alresult, String strgraphName, 
			int intWorkingDays, int intGroup, int intItem )
	{
		
		StringBuffer sbScriptGrp = new StringBuffer();
		StringBuffer sbScriptItem = new StringBuffer();
		
		String strTemp;
		int intLength;
		// To get the Top Agent Information
		sbScriptGrp.append(strgraphName + ".setcategories(");
		sbScriptItem.append(strgraphName + ".setseries(;");
		intLength = alresult.size();
		for (int e=0;e<intLength ;e++)
		{
			hmReturn = (HashMap)alresult.get(e);
			sbScriptGrp.append(getSplitName((String)hmReturn.get("DAY"),30));
			sbScriptGrp.append(";");
			strTemp = (String)hmReturn.get("AMT");
			sbScriptItem.append(hmReturn.get("AMT"));
			sbScriptItem.append(";");
			
			dblTotalSales = dblTotalSales + Double.parseDouble(strTemp);
			
		}
		sbScriptGrp.append(")");
		sbScriptItem.append(")");
		
		dblDailyAverage = dblTotalSales / intWorkingDays;
		
		return sbScriptGrp.toString() + sbScriptItem.toString() ;
	}

	// Used to build the bar row chart
	// Takes the Arraylist of value as inparameter
	// Assumes Arrayliss contains hashmap 
	// Hash map has following key CD & NAME
	// Note:-  method to be modified to support multiple group
	// Currently it intGroup and  intItem value not used and shoudl be passed as 1
	public String buildBar_Row_ChartScript(ArrayList alresult, String strgraphName, 
			int intGroup, int intItem )
	{
		
		StringBuffer sbScriptGrp = new StringBuffer();
		StringBuffer sbScriptItem = new StringBuffer();
		
		// To get the Top Agent Information
		sbScriptGrp.append(strgraphName + ".setcategories()");
		
		for (int e=0;e<alresult.size();e++)
		{
			hmReturn = (HashMap)alresult.get(e);
			sbScriptItem.append(strgraphName +".setseries(");
			sbScriptItem.append((String)hmReturn.get("NAME"));
			sbScriptItem.append(";");
			sbScriptItem.append(hmReturn.get("AMT"));
			sbScriptItem.append(")");
		}
		
		return sbScriptGrp.toString() + sbScriptItem.toString() ;
	}

	// Used to build the bar row chart
	// Takes the Arraylist of value as inparameter
	// Assumes Arrayliss contains hashmap 
	// Hash map has following key CD & NAME
	// Note:-  method to be modified to support multiple group
	// Currently it intGroup and  intItem value not used and shoudl be passed as 1
	public String buildBar_RowStack_ChartScript(HashMap hminresult, String strgraphName, 
			int intGroup, int intItem )
	{
		ArrayList alcategories = new ArrayList();
		ArrayList alsetseries = new ArrayList();
		
		StringBuffer sbcategories = new StringBuffer();
		StringBuffer sbseries = new StringBuffer();
		
		String strName = "" ;
		String strCurrentName = "";
		String strDate = "";
		boolean blLoop = true ;
		HashMap hmDate = new HashMap(); 
		
		int intCount = 0;
		int intDateCount = 0 ;
		
		alcategories = (ArrayList)hminresult.get("DATELIST");
		alsetseries = (ArrayList)hminresult.get("SERIES");
		
		// To get the Top Agent Information
		sbcategories.append(strgraphName + ".setcategories(");
		
		// To get the date information
		intDateCount = alcategories.size();
		for (int e=0;e<intDateCount;e++)
		{
			hmReturn = (HashMap)alcategories.get(e);
			sbcategories.append((String)hmReturn.get("R_DATE"));
			sbcategories.append("; ");
		}
		
		// To get the bar detail information
		for (int j=0;j<alsetseries.size();j++)
		{
			hmReturn = (HashMap)alsetseries.get(j);
			
			strCurrentName = (String)hmReturn.get("NAME");
			if (!strName.equals(strCurrentName))
			{
				intCount = 0;
				sbseries.append(")");
				sbseries.append(strgraphName + ".setseries(");
				sbseries.append(strCurrentName);
				sbseries.append(";");
				strName = strCurrentName; 
			}

			while (blLoop == true) 
			{
				hmDate = (HashMap)alcategories.get(intCount);
				strDate = (String)hmDate.get("R_DATE");
				
				if ( strDate.equals((String)hmReturn.get("R_DATE")) )
				{
					sbseries.append((String)hmReturn.get("AMT"));
					sbseries.append(";");
					blLoop = false;
				}
				else
				{
					sbseries.append(";");
				}
				
				intCount++;
				
				if (intCount >= intDateCount)
				{
					blLoop = false;
				}
				
			}// End of While Loop 
			
			blLoop = true;
		}// End of For Loop
		
		
		if (!sbseries.equals(""))
		{
			sbseries.append(")");
		}
		// To get the value information 
		//System.out.println("**********");
		//System.out.println(sbcategories.toString() + sbseries.toString() );
		return sbcategories.toString() + sbseries.toString() ;
	}

	public String getPopChartHTML()
	{
	
		CordaEmbedder myImage = new CordaEmbedder();
		   
		myImage.externalServerAddress = GmCommonClass.getString("POP_EXTERNAL_ADDRESS");
		myImage.internalCommPortAddress = GmCommonClass.getString("POP_INTERNAL_ADDRESS");
		myImage.appearanceFile = "apfiles/" + strPopFileName ;
		//myImage.userAgent = request.getHeader("USER-AGENT");
		
		myImage.width = intWidth;
		myImage.height = intHeight;
		myImage.returnDescriptiveLink = false;
		myImage.language = "EN";
		myImage.pcScript = strpcScript;
		myImage.outputType = strOutputType;
		
		return myImage.getEmbeddingHTML();
	}
	
	 private String getSplitName(String strName,int intLength)
	 {
         int INTFIX = intLength;
         int intCnt = 0;
         int intLen;
         int intInd = 0;
         int intNewLineCnt = 0;
         StringBuffer strFinal = new StringBuffer(strName);
         String strValue = "";
         intLen = strFinal.length();
         if (intLen <= INTFIX)
         {
	         strValue = strFinal.toString();
	         return strValue;
         }
         else
         {
	         for ( intCnt=0;intCnt<intLen;intCnt++)
	         {
                 intInd++;
                 /* if (strName.charAt(intCnt) == ' ')
                 {
                     intInd = 0;
                 }*/
                 if (intInd > INTFIX)
                 {
                 	 strFinal.insert(intCnt + intNewLineCnt * 4,"\n"); // 4 is shift in the insert position at every insert
                     intNewLineCnt ++;
                     intInd = 1;
                 } //End of If
	         }        //End of for
	         strValue = strFinal.toString();
	         
	         return strValue;
         } //End of If
	 } //End of function getSplitName
}
