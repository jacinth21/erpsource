/**
 * This class contains methods that are common to all components
 * 
 * @author $Author:$
 * @version $Revision:$ <br>
 *          $Date:$
 */

package com.globus.common.beans;

// Java Classes
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmCacheManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAutoCompletePartyListRptBean extends GmAutoCompleteReportBean {
  // Code to Initialize the Logger Class.
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmAutoCompletePartyListRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmAutoCompletePartyListRptBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  }

  /**
   * getSalesFilterDist - Below code to fetch the distributor information from Redis
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getSalesFilterDist(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();

    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    String strStatus = "";
    String strCondition = "";
    String strCompExclusion = "";
    String strDistTypeExclusion = "";
    String strDistFilter = "";
    String strFilterDistByComp = "";
    HashMap hmExclusion = new HashMap();

    strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));
    hmExclusion = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("EXCLUSION_MAP"));

    strCompExclusion = GmCommonClass.parseNull((String) hmExclusion.get("COMPANY"));
    strDistTypeExclusion = GmCommonClass.parseNull((String) hmExclusion.get("DIST_TYPE"));
    strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DISTFILTER"));
    strFilterDistByComp = GmCommonClass.parseNull((String) hmParam.get("FLTR_DIST_BY_COMP"));


    sbQuery.append(" SELECT   DISTINCT D_ID ID,DECODE('" + getComplangid()
        + "', '103097',NVL(D_NAME_EN, D_NAME), D_NAME) NM,");

    sbQuery.append(" REGION_ID PID, AD_ID ADID, VP_ID VPID, COMPID COMPID  ");
    sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700  ");

    if (strStatus.equals("Active")) {
      sbQuery.append(" , t701_distributor t701  ");
    }
    sbQuery.append(" WHERE  D_ID IS NOT NULL AND DISTTYPEID NOT IN (70103,70106,70104) ");
    // For other than the more filters section
    if (strFilterDistByComp.equals("YES")) {
      // Filtering Distributor based on all the companies associated with the plant.
      if (strDistFilter.equals("COMPANY-PLANT")) {
        sbQuery
            .append(" AND  D_COMPID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = "
                + getCompPlantId() + "  AND C5041_VOID_FL IS NULL )");
      } else {
        sbQuery.append(" AND D_COMPID = '" + getCompId() + "' ");
      }
    }
    // Changed as part of Consignment Net report by Field sales. Should
    // display all the distributors irrespective of account.
    // sbQuery.append(" WHERE v700.AC_ID IS NOT NULL ");
    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" AND ");
      sbQuery.append(strCondition);
    }
    if (strStatus.equals("Active")) {
      sbQuery.append(" and t701.C701_DISTRIBUTOR_ID = V700.D_ID and t701.C701_ACTIVE_FL = 'Y'  ");
      sbQuery.append(" AND  (T701.c901_ext_country_id is NULL ");
      sbQuery
          .append(" OR T701.c901_ext_country_id  in (select country_id from v901_country_codes))");
    }
    sbQuery.append(strCompExclusion);
    sbQuery.append(strDistTypeExclusion);

    sbQuery.append(" ORDER BY UPPER(NM)");
    log.debug("Dist sbQuery.toString() ==" + sbQuery.toString());
    gmCacheManager.setKey("getSalesFilterDist:" + getCompId());
    alResult = gmCacheManager.queryMultipleRecords(sbQuery.toString());

    return alResult;
  } // End of getSalesFilterDist

  /**
   * getUSRepList - Below code to fetch the US RepList information from Redis
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getUSRepList(String strAccFlg) throws AppError {

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setPrepareString("gm_pkg_cm_party_rpt.gm_fch_us_rep_list", 2);
    gmCacheManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmCacheManager.setString(1, strAccFlg);
    gmCacheManager.setKey("GETUSREPLISTFOREVENT");
    gmCacheManager.execute();
    rs = (ResultSet) gmCacheManager.getObject(2);
    alReturn = gmCacheManager.returnArrayList(rs);
    gmCacheManager.close();

    return alReturn;
  }

  /**
   * getEmployeeList - Below code to fetch the Employe list information from Redis
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getEmployeeList(HashMap hmParam) throws AppError {
    String strUN = GmCommonClass.parseNull((String) hmParam.get("USERTYPE"));
    String strUserStatus = GmCommonClass.parseNull((String) hmParam.get("USERSTATUS"));
    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setPrepareString("gm_pkg_cm_party_rpt.gm_fch_employ_list", 3);
    gmCacheManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmCacheManager.setString(1, strUN);
    gmCacheManager.setString(2, strUserStatus);
    gmCacheManager.setKey("GETEMPLOYEELISTFOREVENT");
    gmCacheManager.execute();
    rs = (ResultSet) gmCacheManager.getObject(3);
    alReturn = gmCacheManager.returnArrayList(rs);

    gmCacheManager.close();
    return alReturn;
  }

  /**
   * getOUSSalesList - Below code to fetch the OUS Sales list information from Redis
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getOUSSalesList(String strAccFlg) throws AppError {

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setPrepareString("gm_pkg_cm_party_rpt.gm_fch_ous_sales_list", 2);
    gmCacheManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmCacheManager.setString(1, strAccFlg);
    gmCacheManager.setKey("GETOUSREPLISTFOREVENT");
    gmCacheManager.execute();
    rs = (ResultSet) gmCacheManager.getObject(2);
    alReturn = gmCacheManager.returnArrayList(rs);
    gmCacheManager.close();

    return alReturn;
  }
  /**
   * getJPNSalesList - Below code to fetch the japanese sales rep in japanese character
   * 
   * @return ArrayList
   * @exception AppError
   */
  //PMT-57205 Demo Sets - Loan TO Dropdown should Load Japanese Sales rep
  public ArrayList getJPNSalesList(String strFlg) throws AppError {
    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    
	String strComId =  getGmDataStoreVO().getCmpid();
	String strLangId = GmCommonClass.parseNull(getGmDataStoreVO().getCmplangid());
    gmCacheManager.setPrepareString("gm_pkg_cm_party_rpt.gm_fch_jpn_sales_list", 4);
    gmCacheManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmCacheManager.setString(1, strFlg);
    gmCacheManager.setString(2, strComId);
    gmCacheManager.setString(3, strLangId);
    gmCacheManager.setKey("GETJPNREPLISTFOREVENT");
    gmCacheManager.execute();
    rs = (ResultSet) gmCacheManager.getObject(4);
    alReturn = gmCacheManager.returnArrayList(rs);
    gmCacheManager.close();
    return alReturn;
   
  }

  /**
   * fetchEmployeeWithOrders - Method to fetch the Employee name who raised the order from Redis
   * 
   * @return String
   * @exception AppError
   */
  public String fetchEmployeeWithOrders(String strKey, String strSearchPrefix, int iMaxCount,
      HashMap hmParam) // loadParentAcctList
      throws AppError {
    int intSeconds = Integer.parseInt(GmCommonClass.getString("EXPIRE_TIME"));
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(strSearchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
    String strReturn = gmCacheManager.returnJsonString(fetchEmployeeWithOrdersStr(new HashMap()));
    gmCacheManager.setExpireTime(strKey, intSeconds);
    return strReturn;
  }

  /**
   * fetchEmployeeWithOrdersStr - Method to return the query to get the Employee name who raised the
   * order from Redis
   * 
   * @return String
   * @exception AppError
   */
  public String fetchEmployeeWithOrdersStr(HashMap hmParam) throws AppError {
    StringBuffer strQuery = new StringBuffer();
    String strCompanyId = getCompId();
    // 311:Active; 317:Inactive
    strQuery
        .append(" SELECT TO_CHAR(t1501.c101_mapped_party_id) ID, get_user_name(t1501.C101_MAPPED_PARTY_ID) NAME ");
    strQuery.append(" FROM t101_user t101, t1501_group_mapping t1501 ");
    strQuery
        .append(" WHERE t1501.c1501_void_fl IS NULL and t101.c101_user_id = c101_mapped_party_id ");
    strQuery
        .append(" AND t101.c901_user_status in (311,317)  and t1501.c1500_group_id in ( '100022', '100023', '1000001')");
    strQuery.append(" AND t1501.c1900_company_id = " + strCompanyId);
    strQuery.append(" UNION ALL ");
    strQuery.append(" SELECT TO_CHAR(t501.c501_created_by) ID, ACCT_USER.NM NAME ");
    strQuery
        .append(" FROM t101_user t101, t501_order t501,( SELECT C101_MAPPED_PARTY_ID ID, get_user_name(C101_MAPPED_PARTY_ID) NM ");
    strQuery.append(" FROM t1501_group_mapping WHERE c1501_void_fl IS NULL ");
    strQuery
        .append(" AND c1500_group_id  IN ( '100009', '100010', '100011', '100012', '100017', '100019', '100000040') ");
    strQuery.append(" AND c1900_company_id = " + strCompanyId + ") ACCT_USER ");
    strQuery.append(" WHERE t501.c501_void_fl IS NULL AND t501.c501_created_by = ACCT_USER.ID ");
    strQuery
        .append(" AND t101.c101_user_id = ACCT_USER.ID AND t101.c901_user_status IN (311,317) ");
    strQuery.append(" GROUP BY t501.c501_created_by,ACCT_USER.NM ");
    strQuery.append(" UNION ALL ");
    strQuery.append(" SELECT TO_CHAR(t703.c703_sales_rep_id) ID, t703.C703_SALES_REP_NAME NAME ");
    strQuery.append(" FROM t501_order t501,t703_sales_rep t703,t101_user t101 ");
    strQuery
        .append(" WHERE t703.c703_void_fl IS NULL AND t101.c101_user_id = t703.c703_sales_rep_id ");
    strQuery.append(" AND t101.c901_user_status IN (311,317) AND t501.c501_void_fl IS NULL ");
    strQuery
        .append(" AND t703.c703_SALES_REP_ID = t501.c501_created_by AND t703.c703_ACTIVE_FL = 'Y' ");
    strQuery.append(" AND t703.C1900_COMPANY_ID = " + strCompanyId
        + " GROUP BY t703.c703_sales_rep_id,t703.c703_sales_rep_name ORDER BY NAME");
    log.debug("sbquery>>>>>>>>>>>" + strQuery.toString());
    return strQuery.toString();
  }

}

// End of GmCommonRptBean
