/*
 * Name: GmJmsConfigurationBean.java Author: mmuthusamy
 */

package com.globus.common.beans;


import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmFilePathConfigurationBean extends GmBean {
  public static Logger log = GmLogger
      .getInstance("com.globus.common.beans.GmFilePathConfigurationBean");
  public static ResourceBundle rbFilePathConfig;


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmFilePathConfigurationBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }



  /**
   * getFilePathConfig - This method returns a string from the File path configuration Properties
   * file based on the input keyword
   * 
   * @param strKey - Keyword
   * @return String object
   * @throws AppError
   */

  public static String getFilePathConfig(String strKey) throws AppError {
    if (rbFilePathConfig == null) {
      rbFilePathConfig = ResourceBundle.getBundle("FilePathConfig");
    }
    return rbFilePathConfig.getString(strKey);
  } // End of getFilePathConfig Method


}// end of class GmFilePathConfigurationBean
