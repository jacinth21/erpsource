/**
 * FileName    : GmJasperMail.java 
 * Description :
 * Author      : sthadeshwar
 * Date        : Apr 30, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.beans;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

/**
 * @author sthadeshwar
 *
 */
public class GmJasperMail implements Serializable  {

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	private String jasperReportName;
	private HashMap additionalParams;
	private ArrayList reportData;
	private GmEmailProperties emailProperties;
	/**
	 * @return the jasperReportName
	 */
	public String getJasperReportName() {
		return jasperReportName;
	}
	/**
	 * @param jasperReportName the jasperReportName to set
	 */
	public void setJasperReportName(String jasperReportName) {
		this.jasperReportName = jasperReportName;
	}
	/**
	 * @return the additionalParams
	 */
	public HashMap getAdditionalParams() {
		return additionalParams;
	}
	/**
	 * @param additionalParams the additionalParams to set
	 */
	public void setAdditionalParams(HashMap additionalParams) {
		this.additionalParams = additionalParams;
	}
	/**
	 * @return the reportData
	 */
	public ArrayList getReportData() {
		return reportData;
	}
	/**
	 * @param reportData the reportData to set
	 */
	public void setReportData(ArrayList reportData) {
		this.reportData = reportData;
	}
	/**
	 * @return the emailProperties
	 */
	public GmEmailProperties getEmailProperties() {
		return emailProperties;
	}
	/**
	 * @param emailProperties the emailProperties to set
	 */
	public void setEmailProperties(GmEmailProperties emailProperties) {
		this.emailProperties = emailProperties;
	}
	
	public HashMap sendMail()
	{
		HashMap hmReturn = new HashMap();
		try
		{
			ObjectOutputStream out = null;

			/*InetAddress localHost = InetAddress.getLocalHost();
			String ipAddress = localHost.getHostAddress();
			URL url = new URL("http://" + ipAddress + "/GmJasperMailServlet");*/
			
		
			// Get property from JBOSS Server environment
			String hostURL = System.getProperty("HOST_URL");
			URL url = new URL(hostURL + "GmJasperMailServlet");
			log.debug("hostURL"+hostURL);
			
			URLConnection con = url.openConnection();

			con.setDoOutput(true);
			con.setDoInput(true);
			if(additionalParams != null){
			additionalParams.put("CONTEXTURL", GmCommonClass.contextURL);
			}
			HashMap hmReportData = new HashMap();
			hmReportData.put("FILENAME", jasperReportName);
			hmReportData.put("PARAMS", additionalParams);
			hmReportData.put("DETAILS", reportData);
			hmReportData.put("EMAILPROPS", emailProperties);
		
			log.debug(" hmReportData  " + hmReportData);		

			
			out = new ObjectOutputStream(con.getOutputStream());   
			out.writeObject(hmReportData);

			ObjectInputStream in = new ObjectInputStream(con.getInputStream());   
			hmReturn = (HashMap)in.readObject();   

			if(out != null) 
			{
				out.flush();   
				out.close();
			}
			log.debug("Jasper mail sent ...");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			hmReturn.put("EXCEPTION", e);
		}
		return hmReturn;
	}
}
