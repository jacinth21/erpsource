package com.globus.common.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

import org.apache.log4j.Logger;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.struts.action.ActionForm;

public class GmCommonAddressContactBean {
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code

	// Class.
	/*
	 * 
	 */
	/**
	 * fetchInfo - This method will be used to fetch the contact information for
	 * a specific party
	 * 
	 * @param strPartyId -
	 *            party for which the contact info has to be fetched
	 * @return RowSetDynaClass - will be used by display tag
	 * @exception AppError
	 */
	public RowSetDynaClass fetchContactInfo(String strPartyId) throws AppError {

		RowSetDynaClass rsDynaResult = null;
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_cm_contact.gm_cm_fch_contactInfo", 2);
		log.debug(" party id  " + strPartyId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strPartyId);
		gmDBManager.execute();
		rsDynaResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();

		return rsDynaResult;
	}

	/**
	 * fetchEditContactInfo - This method will be used to fetch the information
	 * to edit a specific contact entry
	 * 
	 * @return void
	 * @exception AppError
	 */

	public HashMap fetchEditContactInfo(String strContactId) throws AppError {
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();

		gmDBManager.setPrepareString("gm_pkg_cm_contact.gm_cm_fch_editcontactinfo", 2);

		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strContactId);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();

		return hmReturn;
	}

	/**
	 * saveContact - This method will be used to save Contact Information on
	 * T107_CONTACT
	 * 
	 * @return void
	 * @exception AppError
	 */
	public String saveContact(HashMap hmParam) throws AppError {
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmDBManager gmDBManager = new GmDBManager();
		String strContacIdFromDB = "";

		// String strPartyId = form.getPartyId(); assign the partyId to variable
		// strPartyId. Similar operation for other
		// variables
		String strContactId = GmCommonClass.parseNull((String) hmParam.get("HCONTACTID"));
		String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
		String strContactMode = GmCommonClass.parseNull((String) hmParam.get("CONTACTMODE"));
		String strContactType = GmCommonClass.parseNull((String) hmParam.get("CONTACTTYPE"));
		String strContactValue = GmCommonClass.parseNull((String) hmParam.get("CONTACTVALUE"));
		String strPreference = GmCommonClass.parseNull((String) hmParam.get("PREFERENCE"));
		String strActiveFlag = GmCommonClass.getCheckBoxValue((String) hmParam.get("INACTIVEFLAG"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));

		gmDBManager.setPrepareString("gm_pkg_cm_contact.gm_cm_sav_contactInfo", 9);
		gmDBManager.registerOutParameter(9, java.sql.Types.CHAR);

		/*
		 * register out parameter and set input parameters
		 */

		gmDBManager.setString(1, strContactId);
		gmDBManager.setString(2, strPartyId);
		gmDBManager.setString(3, strContactMode);
		gmDBManager.setString(4, strContactType);
		gmDBManager.setString(5, strContactValue);
		gmDBManager.setString(6, strPreference);
		gmDBManager.setString(7, strActiveFlag);
		gmDBManager.setString(8, strUserId);
		gmDBManager.execute();

		strContacIdFromDB = gmDBManager.getString(9);

		if (!strLog.equals("")) {
			gmCommonBean.saveLog(gmDBManager, strPartyId, strLog, strUserId, "1231"); // 1231
																						// is
																						// for
																						// contact
		}

		gmDBManager.commit();
		return strContacIdFromDB;
	}

}
