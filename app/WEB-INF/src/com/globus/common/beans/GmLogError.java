/******************************************************************************
 * File       :     GmLogError.java
 * Description:     This is a common utility file to log the messages into a file
 * Author:
 * Modifications:
   Version:                 Date:               Change Description:
    1.0                     Nov 20 2002            Initial Version
 *******************************************************************************/

package com.globus.common.beans;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;

/*
 * This class is used to log the user defined messages into a File. (instead of console)
 * Shared by all the other classes.
 * Generic to all Applications.
 * The Application name is passed from the calling program. If that is not
 * found a default file name of "NBCLog.txt" is assumed.
 */

public class GmLogError
{
    //constant values
    public static ResourceBundle rbApp = ResourceBundle.getBundle(System.getProperty("JBOSS_ENV"));
    
    
	private static final String DEFAULT_LOG_FILE = "GlobusErrorLog.txt";
    //private static final String PROPERTY_FILE = "Constants";
    private static final boolean bolDEBUG = false;
    private static String strFileName = "";
    private static String strLogRoot;
    private static String strFileSeperator;
    private static String strServerName = rbApp.getString("GMSERVERNAME");
    private static String strMailIds = rbApp.getString("GMERRORMAIL");
    
   /*
     * Logs the given message with timestamp.
     * @param  strMessage - String to be logged
     * @param  objObject - Object to be logged (Could be a vector, hashtable, user defined bean, etc)
     * @param  strAppName - App name determines the name of the log file to use
     * @return None
     * @exception None

     */
    public static synchronized void log(String strMsg, Object objObject)
    {
        BufferedWriter br = null;
        StringBuffer sb = new StringBuffer();
		GmCommonClass gmCommon = new GmCommonClass();

		strFileName = "c:\\GlobusErrorLog.txt";

        try
        {
            br = new BufferedWriter(new FileWriter(strFileName,true));
			sb.append("\n");
			sb.append(new Date().toString());
            sb.append("\t");
			sb.append(strMsg);
			sb.append("\n");
			sb.append("\t\t\t\t");
			sb.append(objObject.toString());
			sb.append("\n");
			sb.append("---------------------END----------------------");
            br.write(sb.toString());

			//String strMailIds = "djames@globusmedical.com;richardk@globusmedical.com;jkumar@globusmedical.com;vprasath@globusmedical.com";
			String strCCMailIds = "";
			gmCommon.sendMail("System@globusmedical.com",GmCommonClass.StringtoArray(strMailIds, ";"),GmCommonClass.StringtoArray(strCCMailIds, ";"),
                     strMsg,objObject.toString());					
	
        }catch(Exception exp)
        {
            if(bolDEBUG)
            {
                System.out.println("Could not log the message..." + objObject+ "\n" + exp);
            }
        }
        finally
        {
            try
            {
                if (br!=null) br.close();
                sb = null;
            }
            catch (Exception exp)
            {
                //do nothing
                if (bolDEBUG)
                {
                    System.out.println("Failure in closing the file writer...\n" + exp);
                }
            }
        }
    }//End of method log

    //To take care of int and double -- Note: char will be converted to its ASCII equivalent
    public static synchronized void log(String strMsg, double dblTemp)
    {
		log(strMsg,new Double(dblTemp));
	}

	//To take care of boolean
	public static synchronized void log(String strMsg, boolean boolTemp)
	{
		log(strMsg,new Boolean(boolTemp));
	}

    /*
	 * Logs the given exception as a stacktrace with timestamp.
	 * @param  ePrintStackTrace Exception
	 * @return None
	 * @exception None
     */
    public static synchronized void log(String strMsg, Exception ePrintStackTrace)
    {
		log(strMsg,stack2string(ePrintStackTrace));
	}
	/*
	 * Internal method to convert the exception stack trace to a string
	 * @param  Exception e
	 * @return String
	 * @exception None
 	 */
    private static String stack2string(Exception e)
    {
		try
		{
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			return "------ PRINT STACK TRACE\r\n" + sw.toString() + "------END OF PRINT STACK TRACE\r\n";
		}
		catch(Exception e2)
		{
			return "------\r\n Exception Occured: "+e.getMessage() + "------\r\n";
		}
  	}//End of stack2string
    
    public static synchronized void logVisit(HashMap hmValues)
    {
        BufferedWriter br = null;
        StringBuffer sb = new StringBuffer();
            GmCommonClass gmCommon = new GmCommonClass();
            String strPage = GmCommonClass.parseNull((String)hmValues.get("PAGE"));
            String strPageNm = GmCommonClass.parseNull((String)hmValues.get("PAGENM"));
            String strUser = GmCommonClass.parseNull((String)hmValues.get("USER"));
            String strDeptId = GmCommonClass.parseNull((String)hmValues.get("DEPTID"));
            String strLoginTs = GmCommonClass.parseNull((String)hmValues.get("LOGINTS"));
            
            strFileName = "c:\\GlobusVisitLog.txt";

        try
        {
            br = new BufferedWriter(new FileWriter(strFileName,true));
            sb.append("\n");
            sb.append(new Date().toString());
                  sb.append("\t");
                  sb.append(strPageNm);
                  sb.append("\t");
                  sb.append(strPage);
                  sb.append("\t");
                  sb.append(strUser);
                  sb.append("\t");
                  sb.append(strDeptId);
                  sb.append("\t");
                  sb.append(strLoginTs);
             //Commenting below code for TSK-22466 to remove unwanted log writing     
            //br.write(sb.toString());
                  
        }catch(Exception exp)
        {
            if(bolDEBUG)
            {
                System.out.println("Could not log the message...");
            }
        }
        finally
        {
            try
            {
                if (br!=null) br.close();
                sb = null;
            }
            catch (Exception exp)
            {
                //do nothing
                if (bolDEBUG)
                {
                    System.out.println("Failure in closing the file writer...\n" + exp);
                }
            }
        }
    }//End of method logVisit

}//EOF