package com.globus.common.beans;

/*import java.net.InetAddress;*/
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.email.GmEmailBean;
import com.independentsoft.exchange.Appointment;
import com.independentsoft.exchange.AppointmentPropertyPath;
import com.independentsoft.exchange.Attendee;
import com.independentsoft.exchange.Body;
import com.independentsoft.exchange.BodyType;
import com.independentsoft.exchange.BusyStatus;
import com.independentsoft.exchange.CancelItem;
import com.independentsoft.exchange.GetServerTimeZonesResponse;
import com.independentsoft.exchange.ItemChange;
import com.independentsoft.exchange.ItemId;
import com.independentsoft.exchange.Mailbox;
import com.independentsoft.exchange.Property;
import com.independentsoft.exchange.RequestServerVersion;
import com.independentsoft.exchange.SendMeetingOption;
import com.independentsoft.exchange.Service;
import com.independentsoft.exchange.ServiceException;
import com.independentsoft.exchange.StandardFolder;
import com.independentsoft.exchange.StandardFolderId;
import com.independentsoft.exchange.TimeZoneDefinition;


public class GmExchangeManager {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  private final String strSimpleDateFmt = GmCommonClass.getString("CRM_EVENT_DATE_FMT");
  private Mailbox CRMMailbox = null;
  private String strSenderPass = "";
  private String strEmailFooter = GmCommonClass.parseNull(GmCommonClass.getString("SERVEREMAILFOOTER"));


  public void setMailProperties(String strMailboxId, String strSenderPWD) {
    CRMMailbox = new Mailbox(GmCommonClass.getString(strMailboxId));
    strSenderPass = GmCommonClass.getString(strSenderPWD);
  }

  /**
   * This method will return the service for meeting request using JWebservices.Exchange
   * url,username and password are match then only get the services.
   * 
   * @param strLoginUserName
   * @return
   */
  private Service getService(String strLoginUserName) {
    Service service =
        new Service(GmCommonClass.getString("EXCHANGE_URL"), strLoginUserName, strSenderPass);
    log.debug("service >>>>>>>>>>>" + service);
    return service;
  }

  /**
   * To get the body type for meeting request.
   * 
   * @param strMimeType
   * @return
   */
  private BodyType getBodyType(String strMimeType) {
    if (strMimeType.equalsIgnoreCase("HTML"))
      return BodyType.HTML;
    else if (strMimeType.equalsIgnoreCase("TEXT"))
      return BodyType.TEXT;
    else
      return BodyType.NONE;
  }
  
  /** This method will append system name in message body footer
 * @param strBody
 * @param strMimeType
 * @return
 */
 private Body getBody(String strBody, String strMimeType){
	 strBody +=  strEmailFooter.replaceAll("<systemname>",  getLocalHost());
	 return new Body(strBody, getBodyType(strMimeType));
  }
  
  /** This method return server name
 * @return
 */
 private String getLocalHost(){
	  String strSystemNm = "";
	  try{
		    /*InetAddress iadd = InetAddress.getLocalHost();
		    strSystemNm = iadd.toString().toLowerCase();
		    String[] SystName = new String[2];
		    SystName = strSystemNm.split("/");
		    strSystemNm = SystName[0];*/
		  
		  // Get property from JBOSS Server environment
	      strSystemNm = System.getProperty("HOST_NAME");
	      log.debug("HostName "+strSystemNm);
	      
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	  
	    return strSystemNm;
  }

  /**
   * This method will create the meeting request using JWebServices.Need to pass all the related
   * informations for the mail creation.Mail notification will send all the recipients with enough
   * informations.
   * 
   * @param gmEmailProperties
   * @return
   * @throws AppError
   */
  public String sendMeetingRequest(GmEmailProperties gmEmailProperties) throws AppError,
      ServiceException, ParseException {
    ItemId itemId = null;
    String strNotifyFl = gmEmailProperties.getNotifyfl();
    SimpleDateFormat dateFormat = new SimpleDateFormat(strSimpleDateFmt);
    dateFormat.setTimeZone(java.util.TimeZone.getTimeZone("AMERICA/NEW_YORK"));
    Date startTime = dateFormat.parse(gmEmailProperties.getStartTime());
    Date endTime = dateFormat.parse(gmEmailProperties.getEndime());
    
    strEmailFooter = GmCommonClass.parseNull(GmCommonClass.getString("SERVEREMAILFOOTER"));
    Appointment appointment = new Appointment();
    appointment.setSubject(gmEmailProperties.getSubject());
    appointment.setBody(getBody(gmEmailProperties.getMessage(),gmEmailProperties.getMimeType()));
    appointment.setStartTime(startTime);
    appointment.setEndTime(endTime);
    // appointment.setMeetingTimeZone( new TimeZone(StandardTimeZone.EASTERN));
    appointment.getCategories().add(gmEmailProperties.getCategory());
    appointment.setLocation(gmEmailProperties.getLocation());
    appointment.setBusyStatus(BusyStatus.TENTATIVE);
    appointment.setResponseRequested(true);

    // Public Calendar
    Service service = getService(gmEmailProperties.getSender());
    // Mailbox CRMMailbox = new Mailbox(GmCommonClass.getString("GMI_CRM_MAILBOX"));
    StandardFolderId publicCalendarFolderId =
        new StandardFolderId(StandardFolder.CALENDAR, CRMMailbox);
    String[] strArrayRecipients = gmEmailProperties.getRecipients();

    setEasternTimeZone(service);

    for (String strEmail : strArrayRecipients) {
      log.debug("====strEmail=======" + strEmail);
      appointment.getRequiredAttendees().add(new Attendee(strEmail));
    }

    if (strNotifyFl.equals("Y")) {
      itemId = service.sendMeetingRequest(appointment, publicCalendarFolderId);
    } else {
      itemId = service.createItem(appointment, publicCalendarFolderId);
    }
    log.debug("itemId ===> " + itemId);

    return itemId.toString();
  }


  /**
   * This method will confirm the meeting request using JWebServices.Based on the item id update the
   * existing meeting
   * 
   * @param gmEmailProperties
   * @return
   * @throws AppError
   */
  public String confirmMeetingRequest(GmEmailProperties gmEmailProperties) throws AppError,
      ServiceException {
    String strNotifyFl = gmEmailProperties.getNotifyfl();
    Service service = getService(gmEmailProperties.getSender());
    Appointment appointment = service.getAppointment(gmEmailProperties.getEventItemId());

    Property proBusyStatus = new Property(AppointmentPropertyPath.BUSY_STATUS, BusyStatus.BUSY);
    List<Property> properties = new ArrayList<Property>();
    properties.add(proBusyStatus);
    setEasternTimeZone(service);
    ItemId itemId = appointment.getItemId();
    log.debug("itemId===> " + itemId);
    if (strNotifyFl.equals("Y")) {
      itemId = service.updateItem(itemId, properties, SendMeetingOption.SEND_TO_ALL);
    } else {
      itemId = service.updateItem(itemId, properties);
    }
    return itemId.toString();
  }



  /**
   * This method will update the meeting request using JWebServices.Based on the item id update the
   * existing meeting and the notification will send all the recipients. *
   * 
   * @param gmEmailProperties
   * @return
   * @throws AppError
   */
  public String updateMeetingRequest(GmEmailProperties gmEmailProperties) throws AppError,
      ServiceException, ParseException {
    String strNotifyFl = gmEmailProperties.getNotifyfl();
    Service service = getService(gmEmailProperties.getSender());
    SimpleDateFormat dateFormat = new SimpleDateFormat(strSimpleDateFmt);
    dateFormat.setTimeZone(java.util.TimeZone.getTimeZone("AMERICA/NEW_YORK"));
    Date startTime = dateFormat.parse(gmEmailProperties.getStartTime());
    Date endTime = dateFormat.parse(gmEmailProperties.getEndime());
    List<String> alCategory = new ArrayList<String>();
    alCategory.add(gmEmailProperties.getCategory());


    Appointment appointment = service.getAppointment(gmEmailProperties.getEventItemId());

    Property proSubject =
        new Property(AppointmentPropertyPath.SUBJECT, gmEmailProperties.getSubject());
    Property proBody =
        new Property(AppointmentPropertyPath.BODY,getBody(gmEmailProperties.getMessage(),gmEmailProperties.getMimeType()));
    Property proStartTime = new Property(AppointmentPropertyPath.START_TIME, startTime);
    Property proEndTime = new Property(AppointmentPropertyPath.END_TIME, endTime);
    Property proCategory = new Property(AppointmentPropertyPath.CATEGORIES, alCategory);
    // Property proMeetingTimeZone = new Property(AppointmentPropertyPath.MEETING_TIME_ZONE, new
    // TimeZone(StandardTimeZone.EASTERN));

    setEasternTimeZone(service);

    List<Property> properties = new ArrayList<Property>();
    properties.add(proSubject);
    properties.add(proBody);
    properties.add(proStartTime);
    properties.add(proEndTime);
    properties.add(proCategory);
    // properties.add(proMeetingTimeZone);
    // appointment.setLocation(gmEmailProperties.getLocation());
    String[] strArrayRecipients = gmEmailProperties.getRecipients();

    ItemId itemId = appointment.getItemId();
    log.debug("itemId===> " + itemId);
    itemId = service.updateItem(itemId, properties);

    // remove
    ItemChange itemChangeRemove = new ItemChange(itemId);
    itemChangeRemove.getPropertiesToDelete().add(AppointmentPropertyPath.REQUIRED_ATTENDEES);
    itemChangeRemove.getPropertiesToDelete().add(AppointmentPropertyPath.OPTIONAL_ATTENDEES);
    itemId = service.updateItem(itemChangeRemove);

    // Add
    ItemChange itemChangeAdd = new ItemChange(itemId);
    for (String strEmail : strArrayRecipients) {
      log.debug("====strEmail=======" + strEmail);
      itemChangeAdd.getPropertiesToAppend().add(
          new Property(AppointmentPropertyPath.REQUIRED_ATTENDEES, new Attendee(strEmail)));
    }

    if (strNotifyFl.equals("Y")) {
      service.updateItem(itemChangeAdd, SendMeetingOption.SEND_TO_ALL_AND_SAVE_COPY);
    } else {
      service.updateItem(itemChangeAdd);
    }

    return itemId.toString();
  }

  /**
   * This method will cancel the meeting request using JWebServices.Based on the item id cancel the
   * existing meeting. *
   * 
   * @param gmEmailProperties
   * @return
   * @throws AppError
   * @throws ServiceException
   */
  public boolean cancelMeetingRequest(GmEmailProperties gmEmailProperties) throws AppError,
      ServiceException {
    Service service = getService(gmEmailProperties.getSender());

    Appointment appointment = service.getAppointment(gmEmailProperties.getEventItemId());
    setEasternTimeZone(service);
    ItemId itemId = appointment.getItemId();
    CancelItem cancelItem = new CancelItem(itemId);
    service.cancelMeetingRequest(cancelItem);
    return true;
  }

  private void setEasternTimeZone(Service service) throws ServiceException {
    service.setRequestServerVersion(RequestServerVersion.EXCHANGE_2010_SP1);
    TimeZoneDefinition timeZone = getTimeZone(service, "Eastern Time"); // find time zone based on
                                                                        // name
    service.setTimeZoneContext(timeZone);
  }

  private TimeZoneDefinition getTimeZone(Service service, String name) throws ServiceException {
    GetServerTimeZonesResponse response = service.getServerTimeZones();

    for (TimeZoneDefinition timeZone : response.getTimeZoneDefinitions()) {
      if (timeZone.getName().indexOf(name) > 0) {
        return timeZone;
      }
    }

    return null;
  }
}
