/*
 * Module: GmWidgetBean.java Author: Basudev Vidyasankar Date-Written: 26 Dec 2008 Revision Log
 * (mm/dd/yy initials description) ---------------------------------------------------------
 * mm/dd/yy xxx What you changed
 */
package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author bvidyasankar
 * 
 */
public class GmWidgetBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  // this will be removed all place changed with Data Store VO constructor
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmWidgetBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * createWidget - This method will be used by widgets in the front end to display corresponding
   * information
   * 
   * @param HashMap hmParam
   * @return HashMap
   * @exception AppError
   */
  public HashMap createWidget(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    String widgetID = GmCommonClass.parseNull((String) hmParam.get("WIDGETID"));
    GmWidgetBean gmWidgetBean = new GmWidgetBean(getGmDataStoreVO());

    if (widgetID.equals("1")) {
      hmReturn = gmWidgetBean.getDHRStatusCount();
    }
    return hmReturn;
  }

  /**
   * getDHRStatusCount - This method is to get the count of pending instructions of various DHR
   * statuses
   * 
   * @param HashMap hmParam
   * @return ArrayList
   * @exception AppError
   */

  public HashMap getDHRStatusCount() throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_dhr_rpt.gm_fch_dhr_status_cnt", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();

    hmReturn =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(1)));
    gmDBManager.close();
    return hmReturn;
  }
}
