package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAuditPriceLogBean extends GmBean {
  // this will be removed all place changed with Data Store VO constructor

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAuditPriceLogBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * fetchPriceHistory - This method will load price log information
   * 
   * @param hmParam - hmParam
   * @return HashMap
   * @exception AppError
   */
  public RowSetDynaClass fetchPriceHistory(HashMap hmParam) throws AppError {

    RowSetDynaClass rdResult = null;
    String strRequestId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strAuditId = GmCommonClass.parseNull((String) hmParam.get("AUDITID"));;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pd_audit_price_log.gm_fch_price_history", 3);

    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.setString(2, strAuditId);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return rdResult;
  }

}
