package com.globus.common.beans;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;


import com.globus.valueobject.common.GmEgnyteSendVO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class GmEgnyteClient {
	
	
	private String strEgnyteAuthToken;
	private String strEgnyteWebResource;
	private GmEgnyteSendVO objEgnyteSendVO;
	private String strEgnyteContentType="application/json";
	
	/**
	 * @return the strEgnyteAuthToken
	 */
	public String getStrEgnyteAuthToken() {
		return strEgnyteAuthToken;
	}

	/**
	 * @param strEgnyteAuthToken the strEgnyteAuthToken to set
	 */
	public void setStrEgnyteAuthToken(String strEgnyteAuthToken) {
		this.strEgnyteAuthToken = strEgnyteAuthToken;
	}

	/**
	 * @return the strEgnyteWebResource
	 */
	public String getStrEgnyteWebResource() {
		return strEgnyteWebResource;
	}

	/**
	 * @param strEgnyteWebResource the strEgnyteWebResource to set
	 */
	public void setStrEgnyteWebResource(String strEgnyteWebResource) {
		this.strEgnyteWebResource = strEgnyteWebResource;
	}

	/**
	 * @return the objEgnyteSendVO
	 */
	public GmEgnyteSendVO getObjEgnyteSendVO() {
		return objEgnyteSendVO;
	}

	/**
	 * @param objEgnyteSendVO the objEgnyteSendVO to set
	 */
	public void setObjEgnyteSendVO(GmEgnyteSendVO objEgnyteSendVO) {
		this.objEgnyteSendVO = objEgnyteSendVO;
	}

	/**
	 * @return the strEgnyteContentType
	 */
	public String getStrEgnyteContentType() {
		return strEgnyteContentType;
	}

	/**
	 * @param strEgnyteContentType the strEgnyteContentType to set
	 */
	public void setStrEgnyteContentType(String strEgnyteContentType) {
		this.strEgnyteContentType = strEgnyteContentType;
	}

	public String createEgnyteLink(){
		strEgnyteAuthToken = System.getProperty("ENV_EGNYTEAUTHTOKEN");	
		strEgnyteContentType = GmCommonClass.getString("EGNYTECONTENTTYPE");	
		strEgnyteWebResource = GmCommonClass.getString("EGNYTEWEBRESOURCE");	
		
		Client client = Client.create();
		
		WebResource webResource = client.resource(UriBuilder.fromUri(strEgnyteWebResource).build());
		webResource.header("Authorization", strEgnyteAuthToken);
		webResource.header("Content-Type", strEgnyteContentType);
				
		webResource.type(MediaType.APPLICATION_FORM_URLENCODED);
		ClientResponse response = webResource.header("Authorization", strEgnyteAuthToken)
											.header("Content-Type", strEgnyteContentType)
											 .type(MediaType.APPLICATION_JSON)
											 .post(ClientResponse.class, objEgnyteSendVO);
		
		
		return response.getEntity(String.class);
	}
 
}