/**
 * ClassName : GmCommonCurrConvBean.java Description: bean class for saving and fetching currency
 * conversion detail of master screen and report. Author : HPatel Version : 1 Date: 01/14/2010
 * Copyright : Globus Medical Inc Change History : Created
 */
package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * Bean class for saving and fetching currency conversion detail
 * 
 * @author Himanshu
 */
public class GmCommonCurrConvBean extends GmBean {
  // this will be removed all place changed with Data Store VO constructor
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmCommonCurrConvBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // Code to Initialize the Logger Class.
  Logger log = GmLogger.getInstance(this.getClass().getName());
  public static ResourceBundle rbOandaConfig;
  

  /**
   * This method returns a string value from the OANDA APT URL Properties file based on the input as
   * a keyword
   * 
   * @param strKey - Keyword to fetch the property value from the OANDA URL property.
   * @return String
   */

  public static String getOandaApiConfig(String strKey) {
    if (rbOandaConfig == null) {
      rbOandaConfig = ResourceBundle.getBundle(System.getProperty("ENV_OANDA_CONF"));  //"OandaApi4j"
    }
    return rbOandaConfig.getString(strKey);
  }


  /**
   * Fatches the currency conversion detail using currId from db
   * 
   * 
   * @param strCurrId
   * @return java.lang.String
   * @throws com.globus.common.beans.AppError
   */
  public HashMap fetchCurrConvDtl(String strCurrId) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alTransDetails = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_cm_currency.gm_fch_curr_conv_dtl", 3);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCurrId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    // to get the transaction details
    alTransDetails =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));

    hmReturn.put("ALTRANSLIST", alTransDetails);
    // To form as comma seperated string to show it at the time of edit
    if (alTransDetails.size() > 0) {
      String strTransIds = GmCommonClass.createStringFromListbyKey(alTransDetails, "TRANSID");
      hmReturn.put("TRANSIDS", strTransIds);

    }
    gmDBManager.close();

    return hmReturn;
  }

  /**
   * fetchAllCurrConvDtl - This method will return an ArrayList having all the details form db
   * 
   * @param hmParams
   * @return ArrayList alResult
   * @exception AppError
   */
  public RowSetDynaClass fetchAllCurrConvDtl(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass rdCurrConv = null;
    String strCurrId = GmCommonClass.parseNull((String) hmParam.get("CURRID"));
    String strCurrFrom = GmCommonClass.parseZero((String) hmParam.get("CURRFROM"));
    String strCurrTo = GmCommonClass.parseZero((String) hmParam.get("CURRTO"));
    String strConvType = GmCommonClass.parseZero((String) hmParam.get("CONVTYPE"));
    Date dtCurrFromDT = (Date) hmParam.get("DTCURRFROMDT");
    Date dtCurrToDT = (Date) hmParam.get("DTCURRTODT");
    String strSource = GmCommonClass.parseNull((String) hmParam.get("SOURCEINPUTSTR"));

    gmDBManager.setPrepareString("gm_pkg_cm_currency.gm_fch_all_curr_conv_dtl", 7);
    gmDBManager.setString(1, strCurrFrom);
    gmDBManager.setString(2, strCurrTo);
    gmDBManager.setString(3, strConvType);
    gmDBManager.setDate(4, dtCurrFromDT);
    gmDBManager.setDate(5, dtCurrToDT);
    gmDBManager.setString(6, strSource);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.execute();
    rdCurrConv = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(7));
    log.debug("Get rdCurrConv:" + rdCurrConv.getDynaProperty("CURRID"));
    gmDBManager.close();
    return rdCurrConv;

  } // End of fetchPOC.

  /**
   * fetchMonthRateFl - This method will return String for weather or not the exchange rate exists
   * for the appropriate date range.
   * 
   * @param hmParams
   * @return String Flag for range exists (Y,N)
   * @exception AppError
   */
  public String fetchMonthRateFl(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strRateFlag = "";

    // Get Currency From, To, and Type from hmParam
    String strCurrFrom = GmCommonClass.parseZero((String) hmParam.get("CURRFROM"));
    String strCurrTo = GmCommonClass.parseZero((String) hmParam.get("CURRTO"));
    String strConvType = GmCommonClass.parseZero((String) hmParam.get("CONVTYPE"));

    gmDBManager.setPrepareString("gm_pkg_cm_currency.gm_fch_month_ratefl", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CHAR);
    gmDBManager.setString(1, strCurrFrom);
    gmDBManager.setString(2, strCurrTo);
    gmDBManager.setString(3, strConvType);

    gmDBManager.execute();
    strRateFlag = GmCommonClass.parseNull(gmDBManager.getString(4));
    gmDBManager.close();
    return strRateFlag;
  }

  /**
   * Method to save the currency conversion information to db
   * 
   * @param hmParam
   * @return java.lang.String - currId
   * @throws com.globus.common.beans.AppError
   */
  public String saveCurrencyConvDtl(HashMap hmParam) throws AppError {
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strCurrId = GmCommonClass.parseNull((String) hmParam.get("CURRID"));
    String strCurrFrom = GmCommonClass.parseNull((String) hmParam.get("CURRFROM"));
    String strCurrTo = GmCommonClass.parseNull((String) hmParam.get("CURRTO"));
    Date dtCurrFromDT = (Date) hmParam.get("DTCURRFROMDT");
    Date dtCurrToDT = (Date) hmParam.get("DTCURRTODT");
    String strCurrValue = GmCommonClass.parseNull((String) hmParam.get("CURRVALUE"));
    String strLog = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strConvType = GmCommonClass.parseNull((String) hmParam.get("CONVTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    // holding the transaction ids
    String strTransIdInputStr = GmCommonClass.parseNull((String) hmParam.get("TRANSIDS"));
    log.debug("user id:" + strUserId);
    log.debug("strLog:" + strLog);

    gmDBManager.setPrepareString("gm_pkg_cm_currency.gm_sav_curr_conv_dtl", 9);
    gmDBManager.registerOutParameter(1, java.sql.Types.CHAR);

    gmDBManager.setString(1, strCurrId);
    gmDBManager.setString(2, strCurrFrom);
    gmDBManager.setString(3, strCurrTo);
    gmDBManager.setDate(4, dtCurrFromDT);
    gmDBManager.setDate(5, dtCurrToDT);
    gmDBManager.setString(6, strCurrValue);
    gmDBManager.setString(7, strConvType);
    gmDBManager.setString(8, strUserId);
    gmDBManager.setString(9, strTransIdInputStr);
    gmDBManager.execute();
    strCurrId = gmDBManager.getString(1);
    log.debug("strCurrId is (Inside bean): " + strCurrId);
    if (!strLog.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strCurrId, strLog, strUserId, "1253");
    }

    gmDBManager.commit();

    return strCurrId;
  }



  /**
   * Method to save the real time currency conversion information from OANDA
   * 
   * @param hmParam
   * @throws com.globus.common.beans.AppError
   */
  public void saveRealTimeCurrConvDtls(HashMap hmParam) throws AppError {

    log.debug("hmParam==" + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBaseCurr = GmCommonClass.parseNull((String) hmParam.get("BASE_CURR"));
    String strQuoteCurr = GmCommonClass.parseNull((String) hmParam.get("QUOTE_CURR"));
    String strExchangeValue = GmCommonClass.parseNull((String) hmParam.get("AVERAGE_BID_RATE"));
    String strSource = GmCommonClass.parseNull((String) hmParam.get("SOURCE"));
    String strFailureFl = GmCommonClass.parseNull((String) hmParam.get("FAILURE_FL"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_cm_exchange_rate.gm_sav_exchange_rate_dtl", 6);

    gmDBManager.setString(1, strBaseCurr);
    gmDBManager.setString(2, strQuoteCurr);
    gmDBManager.setString(3, strExchangeValue);
    gmDBManager.setString(4, strSource);
    gmDBManager.setString(5, strFailureFl);
    gmDBManager.setString(6, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * loadExchangeRateAvgReport : Method to fetch the exchange rate average report
   * 
   * @param hmParam
   * @throws com.globus.common.beans.AppError
   */

  public HashMap loadExchangeRateAvgReport(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCrossTabReport gmCrossTabReport = new GmCrossTabReport(getGmDataStoreVO());

    String strFromMonth = GmCommonClass.parseNull((String) hmParam.get("FROMMONTH"));
    String strFromYear = GmCommonClass.parseNull((String) hmParam.get("FROMYEAR"));
    String strToMonth = GmCommonClass.parseNull((String) hmParam.get("TOMONTH"));
    String strToYear = GmCommonClass.parseNull((String) hmParam.get("TOYEAR"));
    String strCurrency = GmCommonClass.parseNull((String) hmParam.get("CURRENCY"));
    String strSelectedCurr = GmCommonClass.parseNull((String) hmParam.get("SELECTEDCURRENCY"));

    StringBuffer sbHeaderQuery = new StringBuffer();
    StringBuffer sbDetailQuery = new StringBuffer();
    StringBuffer sbWhereCondtion = new StringBuffer();

    HashMap hmCrossTabDtls = new HashMap();


    sbWhereCondtion.append("");
    // Code to frame the Where Condition
    if (strFromMonth != null && strFromYear != null) {

      if (!strFromMonth.equals("") && !strFromYear.equals("")) {
        sbWhereCondtion.append(" C907_FROM_DATE >= TO_DATE('");
        sbWhereCondtion.append(strFromMonth + "/" + strFromYear);
        sbWhereCondtion.append("','MM/YYYY') ");
      }
    }

    if (strToMonth != null && strToYear != null) {

      if (!strToMonth.equals("") && !strToYear.equals("")) {
        if (sbWhereCondtion != null) {
          sbWhereCondtion.append(" AND");
        }
        sbWhereCondtion.append(" C907_FROM_DATE  <= LAST_DAY(TO_DATE('");
        sbWhereCondtion.append(strToMonth + "/" + strToYear);
        sbWhereCondtion.append("','MM/YYYY')) ");
      }
    }
    if (!strSelectedCurr.equals("")) {
      sbWhereCondtion.append(" AND C907_CURR_CD_FROM IN(");
      if (strSelectedCurr.contains(",")) {
        String[] strSelectedCurrArr;
        strSelectedCurrArr = strSelectedCurr.split(",");
        for (int i = 0; i < strSelectedCurrArr.length; i++) {

          sbWhereCondtion.append("'" + strSelectedCurrArr[i] + "'");
          if (strSelectedCurrArr.length != i + 1) {

            sbWhereCondtion.append(",");
          }
        }

      } else {
        sbWhereCondtion.append("'" + strSelectedCurr + "'");
      }
      sbWhereCondtion.append(")");
    }

    sbHeaderQuery.append(" SELECT DISTINCT TO_CHAR(C907_FROM_DATE,'Mon YY') R_DATE ");
    sbHeaderQuery.append(" FROM T907_CURRENCY_CONV  WHERE ");
    sbHeaderQuery.append(sbWhereCondtion.toString());
    sbHeaderQuery.append(" ORDER BY TO_DATE(TO_CHAR(C907_FROM_DATE,'Mon YY'),'MON YY') ");

    sbDetailQuery
        .append(" SELECT C907_CURR_CD_FROM ID ,GET_CODE_NAME_ALT(C907_CURR_CD_FROM) NAME ");
    sbDetailQuery
        .append(" , TO_CHAR(C907_FROM_DATE,'Mon YY') R_DATE, ROUND(AVG(C907_CURR_VALUE),6) AMOUNT ");
    sbDetailQuery.append(" FROM T907_CURRENCY_CONV WHERE ");
    sbDetailQuery.append(sbWhereCondtion.toString());
    sbDetailQuery.append(" AND C901_SOURCE = '106910' "); // 106910 SpineIT Average
    sbDetailQuery.append(" AND C907_VOID_FL IS NULL ");
    sbDetailQuery.append(" GROUP BY C907_CURR_CD_FROM,C907_FROM_DATE ");
    sbDetailQuery.append(" ORDER BY C907_CURR_CD_FROM,C907_FROM_DATE ");

    log.debug("Exchange Rate Avg Header SQL -- " + sbHeaderQuery.toString());
    log.debug("Exchange Rate Avg Detail SQL -- " + sbDetailQuery.toString());

    hmCrossTabDtls =
        gmCrossTabReport.GenerateCrossTabReport(sbHeaderQuery.toString(), sbDetailQuery.toString());

    return hmCrossTabDtls;

  } 
  
  /**
   * sendRealTimeERFailureNotifyMail : Method to send real time exchange rate 
   * 
   * @param hmParam
   * @throws com.globus.common.beans.AppError
   */
  
  public void sendRealTimeERFailureNotifyMail(HashMap hmParam) throws AppError {

    log.debug("hmParam==" + hmParam);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBaseCurr = GmCommonClass.parseNull((String) hmParam.get("BASE_CURR"));
    String strQuoteCurr = GmCommonClass.parseNull((String) hmParam.get("QUOTE_CURR"));
    String strFailureMsg = GmCommonClass.parseNull((String) hmParam.get("FAILURE_MSG"));

    gmDBManager.setPrepareString("gm_pkg_cm_exchange_rate.gm_send_exch_rate_failure_mail", 3);

    gmDBManager.setString(1, strBaseCurr);
    gmDBManager.setString(2, strQuoteCurr);
    gmDBManager.setString(3, strFailureMsg);
    gmDBManager.execute();
    gmDBManager.close();

  }
  /**
   * getCurrentDateDiff : Method to get the difference date from the current date
   * 
   * @param int , String
   * @throws com.globus.common.beans.AppError
   */
  public String getCurrentDateDiff(int dateDiff, String strDateFmt) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPrevDate="";
    gmDBManager.setPrepareString("gm_pkg_cm_exchange_rate.gm_get_current_date", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setInt(1, dateDiff);
    gmDBManager.setString(2, strDateFmt);
    gmDBManager.execute();
    strPrevDate = GmCommonClass.parseNull((String)gmDBManager.getString(3));
    gmDBManager.close();
    return strPrevDate;

  }
}
