/*
 * Module: GmCommonUploadBean.java Author: Tarika Project: Globus Medical App Date-Written: Sep 2008
 * Security: Unclassified Description: This beans will be used for fetching and saving information
 * about file
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */
package com.globus.common.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javazoom.upload.UploadException;
import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCommonUploadBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code
  byte[] buffer = new byte[1024];

  public GmCommonUploadBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmCommonUploadBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * saveUploadInfo - This Method is used to Saving the file info
   * 
   * @param GmDBManager gmDBManager
   * @param String refid
   * @param String type
   * @param String filename
   * @param String user
   * 
   * @return void
   * @exception AppError
   */
  public void saveUploadInfo(GmDBManager gmDBManager, String refid, String type, String filename,
      String user) throws AppError {
    // Saving the file info

    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_sav_upload_info", 4);
    gmDBManager.setString(1, refid);
    gmDBManager.setString(2, filename);
    gmDBManager.setString(3, user);
    gmDBManager.setString(4, type);

    gmDBManager.execute();

  }

  /**
   * saveUploadInfo - This Method is used to Saving the file info
   * 
   * @param GmDBManager gmDBManager
   * @param String refid
   * @param String type
   * @param String filename
   * @param String user
   * 
   * @return void
   * @exception AppError
   */
  public void saveUploadInfo(String refid, String type, String filename, String user)
      throws AppError {
    // Saving the file info
    GmDBManager gmDBManager = new GmDBManager();
    saveUploadInfo(gmDBManager, refid, type, filename, user);
    gmDBManager.commit();
  }

  /**
   * saveUploadInfo - This Method is used to Saving the file info
   * 
   * @param GmDBManager gmDBManager
   * @param String refid
   * @param String type
   * @param String filename
   * @param String user
   * 
   * @return void
   * @exception AppError
   */
  public void saveUploadUniqueInfo(String refid, String type, String filename, String user)
      throws AppError {
    // Saving the file info
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    saveUploadUniqueInfo(gmDBManager, refid, type, filename, user);
    gmDBManager.commit();

  }

  public void saveUploadUniqueInfo(GmDBManager gmDBManager, String refid, String type,
      String filename, String user) throws AppError {

    HashMap hmParam = new HashMap();
    hmParam.put("REFID", refid);
    hmParam.put("FILENAME", filename);
    hmParam.put("USERID", user);
    hmParam.put("REFTYPE", type);
    hmParam.put("FILETITLE", "");
    hmParam.put("FILESIZE", "");
    hmParam.put("FILESEQNO", "");
    hmParam.put("REFGRP", "");

    saveUploadUniqueInfo(gmDBManager, hmParam);
  }

  /**
   * saveUploadUniqueInfo - This Method is used to Saving the file info
   * 
   * @param HashMap hmParam
   * @return void
   * @exception AppError
   * @author Jignesh Shah
   */
  public String saveUploadUniqueInfo(HashMap hmParam) throws AppError {
    // Saving the file info
    String strFileId = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    strFileId = saveUploadUniqueInfo(gmDBManager, hmParam);
    gmDBManager.commit();
    return strFileId;
  }

  /**
   * saveUploadUniqueInfo - This Method is used to Saving the file info
   * 
   * @param GmDBManager gmDBManager, HashMap hmParam
   * @return void
   * @exception AppError
   * @author Jignesh Shah
   */

  public String saveUploadUniqueInfo(GmDBManager gmDBManager, HashMap hmParam) throws AppError {
    String strFileId = "";

    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_sav_upload_unique_info", 13);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParam.get("FILEID")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParam.get("REFID")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParam.get("FILENAME")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParam.get("USERID")));
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParam.get("REFTYPE")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParam.get("FILETITLE")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParam.get("FILESIZE")));
    gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParam.get("FILESEQNO")));
    gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParam.get("REFGRP")));
    gmDBManager.setString(10, GmCommonClass.parseNull((String) hmParam.get("SUBTYPE")));
    gmDBManager.setString(11, GmCommonClass.parseNull((String) hmParam.get("TITLETYPE")));
    gmDBManager.setString(13, GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER")));

    gmDBManager.registerOutParameter(12, OracleTypes.VARCHAR);
    gmDBManager.execute();

    strFileId = GmCommonClass.parseNull(gmDBManager.getString(12));
    return strFileId;
  } // End of saveUploadUniqueInfo

  /**
   * fetchUploadInfo - This Method is used to fetch the file info
   * 
   * @param GmDBManager gmDBManager
   * @param String type
   * @param String strStudyListId
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchUploadInfo(String type, String strListId) throws AppError {
    return fetchUploadInfo(type, strListId, "");
  }

  /**
   * fetchUploadInfo - This Method is used to fetch the file info
   * 
   * @param String type, String strListId, String strRefGrp
   * @return ArrayList
   * @exception AppError
   * @author Jignesh Shah
   */
  public ArrayList fetchUploadInfo(String type, String strListId, String strRefGrpId)
      throws AppError {
    HashMap hmParam = new HashMap();

    hmParam.put("ARTIFACTSTYPE", type);
    hmParam.put("REFID", strListId);
    hmParam.put("UPLOADTYPE", strRefGrpId);

    return fetchUploadInfo(hmParam);
  } // End of fetchUploadInfo

  /**
   * fetchUploadInfo - This Method is used to fetch the file info
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   * @author Anilkumar
   */
  public ArrayList fetchUploadInfo(HashMap hmParam) throws AppError {

    ArrayList alReturn = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());


    String strRefType = GmCommonClass.parseNull((String) hmParam.get("ARTIFACTSTYPE"));
    String strListId = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strRefGrpId = GmCommonClass.parseNull((String) hmParam.get("UPLOADTYPE"));
    String strSubTypeId = GmCommonClass.parseNull((String) hmParam.get("SUBTYPE"));

    strRefType = strRefType.equals("0") ? "" : strRefType;
    strSubTypeId = strSubTypeId.equals("0") ? "" : strSubTypeId;
    strListId = strListId.equals("0") ? "" : strListId;
    strRefGrpId = strRefGrpId.equals("0") ? "" : strRefGrpId;
    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_fch_upload_info", 5);
    gmDBManager.setString(1, strRefType);
    gmDBManager.setString(2, strListId);
    gmDBManager.setString(3, strRefGrpId);
    gmDBManager.setString(4, strSubTypeId);
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();

    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(5)));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * fetchUploadInfo - This Method is used to fetch the file info
   * 
   * @param GmDBManager gmDBManager
   * @param String type
   * @param String strStudyListId
   * @return ArrayList
   * @exception AppError
   */
  public void validateFileName(String type, String strStudyListId, String strFile) throws AppError {
    // validate the file info

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_validate_file_name", 3);
    gmDBManager.setString(1, type);
    gmDBManager.setString(2, strStudyListId);
    gmDBManager.setString(3, strFile);
    gmDBManager.execute();
    gmDBManager.close();

  }

  /**
   * fetchFileName - This Method is used to fetch the file name
   * 
   * @param String strID
   * 
   * @return String
   * @exception AppError
   */
  public String fetchFileName(String strID) throws AppError {
    // Fetching the file name
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_fch_upload_file_name", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strID);
    gmDBManager.execute();
    String retValue = gmDBManager.getString(2);

    gmDBManager.close();

    return retValue;

  }

  public String fetchFileType(String strID) throws AppError {
    // Fetching the file name
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_fch_upload_file_type", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strID);
    gmDBManager.execute();
    String retValue = gmDBManager.getString(2);

    gmDBManager.close();

    return retValue;

  }

  public void deleteUploadInfo(String strInputStr, String strUserID) throws AppError {
    // void the particular record
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_void_upload_info", 2);
    gmDBManager.setString(1, strInputStr);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * validateUploadID - This Method is used to check RefId in DB.
   * 
   * @param String strRefId
   * @return String strRefType
   * @exception AppError
   */
  public String validateUploadID(String strRefID, String strType) throws AppError {

    // validate the file info
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_process.gm_validate_id", 4);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strRefID);
    gmDBManager.setString(2, strType);
    // When load CAPA/SCAR/COM/RSR screen the default value is true and upload screen only false. It
    // should throw error in action.
    gmDBManager.setString(4, "false");
    gmDBManager.execute();
    String strRefType = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.close();

    return strRefType;
  }

  /**
   * validateFile - This Method is used to validate the file.
   * 
   * @param String formFile
   * @exception AppError
   */
  public void validateFile(FormFile formFile) throws AppError {
    String strWhiteList = GmCommonClass.getString("COMP_UPLOAD_WHITELIST");
    String strFileName = formFile.getFileName();
    String strfileExt = strFileName.substring(strFileName.lastIndexOf("."));
    String strUploadFileExt = "*" + strfileExt;
    String strExtList[] = strWhiteList.split(",");
    int intMaxFileSize = Integer.parseInt(GmCommonClass.getString("CMP_MAXFILESIZE"));
    int intFileSize = formFile.getFileSize();
    boolean validFlag = false;

    for (int i = 0; i < strExtList.length; i++) {
      String strAllowedExt = strExtList[i];
      if (strAllowedExt.equalsIgnoreCase(strUploadFileExt)) {
        validFlag = true;
        break;
      }
    }
    if (!validFlag) {
      throw new AppError(
          "Upload not successful, as the file type(extension) is not supported.<br>Allowed extension(s):"
              + strWhiteList, "", 'E');
    } else if (intFileSize == 0) {
      throw new AppError("", "20680");
    } else if (intFileSize > intMaxFileSize) {
      throw new AppError("", "20681");
    }
  }// End of validateFile method

  /**
   * uploadFile - This Method is used to upload the file
   * 
   * @param String formFile
   * @param String strUploadHome
   * @param String strFileNameToPopulate
   * @exception UploadException,IOException
   */
  public void uploadFile(FormFile formFile, String strUploadHome, String strFileNameToPopulate)
      throws UploadException, IOException,AppError {
    FileOutputStream outputStream = null;
    File file = new File(strUploadHome);
    if (!file.exists()) {
      file.mkdirs();
    }
    try {
      outputStream = new FileOutputStream(new File(strFileNameToPopulate));
      outputStream.write(formFile.getFileData());
    } catch (Exception exp) {
        log.error("CATCH EXE" + exp.getMessage());
        exp.printStackTrace();
        throw new AppError("", "20966");
    } 
    
    finally {
      if (outputStream != null) {
        outputStream.close();
      }
    }
  }// End of uploadFile method

  /**
   * validateFileNameSts - This Method is used to fetch the file whether having or not
   * 
   * @param String strRefId
   * @param String strFileName
   * @return String flag
   * @exception AppError
   */
  public String validateFileNameSts(String strRefID, String strType, String strFileName)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_fch_file_name_status", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strRefID);
    gmDBManager.setString(2, strType);
    gmDBManager.setString(3, strFileName);
    gmDBManager.execute();
    String strFlag = GmCommonClass.parseNull(gmDBManager.getString(4));
    log.debug("strFlag from DB::" + strFlag);
    gmDBManager.close();
    return strFlag;
  }// End of validateFileNameSts method

  /**
   * saveUploadBulkInfo - To save the Uploaded files details. FILEINPUTSTR =
   * FileTitle^FileSeqNo^FileListId|FileTitle^FileSeqNo^FileListId|
   * 
   * @param HashMap hmParams
   * @return void
   * @throws AppError
   * @author Jignesh Shah
   */

  public void saveUploadBulkInfo(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_save_upload_bulk_info", 2);
    gmDBManager.setString(1, (String) hmParams.get("FILEINPUTSTR"));
    gmDBManager.setString(2, (String) hmParams.get("USERID"));
    gmDBManager.execute();
    gmDBManager.commit();
  } // End of saveUploadBulkInfo

  /**
   * fetchUploadFilesInfo - to fetch upload files details. this method is used to get upload files
   * details and based on the info upload the file from folder. inputSting =
   * FileListId,FileListId,FileListId,
   * 
   * @param String inputSting
   * @return ArrayList
   * @throws AppError
   * @author Jignesh Shah
   */
  public ArrayList fetchUploadFilesInfo(String inputSting) throws AppError {
    ArrayList alReturn = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_fch_upload_file_info", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, inputSting);
    gmDBManager.execute();
    alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();
    return alReturn;
  } // End of fetchVoidedFilesInfo

 
  /**
   * saveFileAttributes - To save the Uploaded files Attributes. inputString =
   * FileId^Y^ShareType|FileId^N^ShareType
   * 
   * @param HashMap hmparam
   * @return void
   * @throws AppError
   * @author Anilkumar
   */
  public void saveFileAttributes(HashMap hmparam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString(" gm_pkg_upload_info.gm_sav_file_attributes", 2);
    gmDBManager.setString(1, (String) hmparam.get("INPUTSTRING"));
    gmDBManager.setString(2, (String) hmparam.get("USERID"));
    gmDBManager.execute();
    gmDBManager.commit();
  }// END of saveShareableArtifacts

  /**
   * unZipFile - To unzip the file in given destination folder.
   * 
   * @param String zipFile, String destination folder
   * @return void
   * @throws AppError
   */
  public void unZipFile(String zipFile, String outputFolder) throws AppError {

    byte[] buffer = new byte[1024];
    String strZipFileName =
        zipFile.substring(zipFile.lastIndexOf("\\") + 1, zipFile.lastIndexOf("."));

    try {
      // create output directory is not exists
      File folder = new File(outputFolder);

      if (!folder.exists()) {
        folder.mkdir();
      } else {

        // folder needs to clean before unzip new one.
        File systemFolder = new File(outputFolder + File.separator + strZipFileName);
        if (systemFolder.exists()) {
          File[] files = systemFolder.listFiles();
          for (File f : files) {
            if (f.isFile() && f.exists()) {
              f.delete();
            }
          }
        }
      }

      // get the zip file content
      ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
      // get the zipped file list entry
      ZipEntry ze = null;
      while ((ze = zis.getNextEntry()) != null) {
        String fileName = ze.getName();

        if (!ze.isDirectory() && fileName.indexOf(strZipFileName) != -1) {
          File newFile = new File(outputFolder + File.separator + fileName);

          log.debug("file unzip : " + newFile.getAbsoluteFile());

          new File(newFile.getParent()).mkdirs();

          FileOutputStream fos = new FileOutputStream(newFile);

          int len;
          while ((len = zis.read(buffer)) > 0) {
            fos.write(buffer, 0, len);
          }

          fos.close();
        }
      }

      zis.closeEntry();
      zis.close();


    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  /**
   * getFileNames - To get file name with comma separator from the folder.
   * 
   * @param int files count inside zip, String destination folder
   * @return String
   * @throws AppError
   */
  public String getFileNames(String strFileName, String outputFolder) throws AppError {
    int ifirst = 1;
    int iSecond = 2;
    int iThird = 3;
    int iFourth = 4;
    int intFileCnt = 0;

    String files = "";
    String strFileFolder = strFileName.substring(0, strFileName.lastIndexOf("."));
    File folder = new File(outputFolder + File.separator + strFileFolder);
    if (!folder.exists()) {
      return files;
    }
    File[] alFiles = folder.listFiles();
    intFileCnt = alFiles.length;
    log.debug("alFiles size" + intFileCnt);

    if (intFileCnt <= 0) {
      return files;
    }

    if (intFileCnt > 4) {
      iSecond = intFileCnt / 4;
      iThird = ((intFileCnt / 2) + intFileCnt) / 2;
      iFourth = intFileCnt - 1;

      if (alFiles[ifirst].isFile()) {
        files = alFiles[ifirst].getName();
      }

      if (alFiles[iSecond].isFile()) {
        files = files + "," + alFiles[iSecond].getName();
      }

      if (alFiles[iThird].isFile()) {
        files = files + "," + alFiles[iThird].getName();
      }

      if (alFiles[iFourth].isFile()) {
        files = files + "," + alFiles[iFourth].getName();
      }
    }

    return files;
  }

  /**
   * processZip - To unzip the zip file and return preview files from it
   * 
   * @param String file path, String destination filename
   * @return String
   * @throws AppError
   */
  public String processZip(String strFilePath, String strFileName) throws AppError {

    String strZipOutputDir = GmCommonClass.getString("ZIPOUTPUTDIR");
    String strFileNames = "";

    unZipFile(strFilePath + strFileName, strZipOutputDir);
    strFileNames = getFileNames(strFileName, strZipOutputDir);
    log.debug("strFileNames === " + strFileNames);
    return strFileNames;
  }

  /**
   * Update Delete flag - This Method is used to Update delete flag when the file upload is failed.
   * 
   * @param String strRefId
   * @param String strRefType
   * @param String strFileName
   * @param String strUserid
   * @exception AppError
   */
  public void deleteFile(String strRefId, String strRefType, String strFileName, String strUserId)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_upload_info.gm_void_upload_file_name", 4);
    gmDBManager.setString(1, strRefId);
    gmDBManager.setString(2, strRefType);
    gmDBManager.setString(3, strFileName);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();
  }// End of deleteFile method

  /**
   * copyFiles - To Copy File from source folder to destination folder
   * 
   * @param String source filename, String destination filename
   * @return
   * @throws AppError
   */
  public void copyFile(String sourceFile, String destnFile) throws AppError, Exception {

    File inFile = new File(sourceFile);
    File outFile = new File(destnFile);
    InputStream in = null;
    OutputStream out = null;
    log.debug("sourceFile==>>" + sourceFile);
    log.debug("destnFile==>>" + destnFile);
    if (inFile.exists()) {
      new File(outFile.getParent()).mkdirs();
      in = new FileInputStream(inFile);
      out = new FileOutputStream(outFile);
      int len;
      while ((len = in.read(buffer)) > 0) {
        out.write(buffer, 0, len);
      }
    }
    in.close();
    out.close();

  }// End Of copyFiles

  /**
   * deleteFiles - To delete File from the given Folder
   * 
   * @param String filename
   * @return
   * @throws AppError
   */
  public void deleteFile(String file) throws AppError {

    File delFileName = new File(file);
    if (delFileName.exists()) {
      delFileName.delete();
    }
  }// End Of deleteFiles

  public ArrayList fetchUploadInfo(String type, String strListId, String strRefGrpId,
      String strPartGrpID) throws AppError {
    HashMap hmParam = new HashMap();
    ArrayList alReturn = new ArrayList();
    ArrayList alGroupImagesList = null;
    ArrayList alPartNumImagesList = null;

    // Calling the bean method with part num & type to get the Parts images list
    hmParam.put("ARTIFACTSTYPE", type);
    hmParam.put("REFID", strListId);
    hmParam.put("UPLOADTYPE", strRefGrpId);
    alPartNumImagesList = GmCommonClass.parseNullArrayList(fetchUploadInfo(hmParam));

    // Add PartNum images list in returning List.
    alReturn.addAll(alPartNumImagesList);
    // Calling the bean method with part Group ID & type to get the Group images list
    // BUG-5142 Here we are calling the Group Image fetch only when the Part Image List size =0.
    // So when the part is having image and if it's priced by Group, we will not call the group
    // image .
    // We will call the group image fetch only when the Part Image is not available and the Part's
    // Group is priced by Group.
    if (!strPartGrpID.equals("") && alPartNumImagesList.size() == 0) {
      hmParam.put("REFID", strPartGrpID);
      hmParam.put("UPLOADTYPE", "103094"); // Group type
      alGroupImagesList = GmCommonClass.parseNullArrayList(fetchUploadInfo(hmParam));
      // Add Group images list in returning List.
      alReturn.addAll(alGroupImagesList);
    }

    return alReturn;
  }

	/**
	 * fetchJSONUploadFilesInfo - to fetch upload files details. this method is used
	 * to get upload files details and based on the upload file ids (FileListId)
	 * 
	 * @param String strFileListIds
	 * @return JSON string
	 * @throws AppError
	 * @author mmuthusamy
	 */
	public String fetchJSONUploadFilesInfo(String strFileListIds) throws AppError {
		String strFileJSON;
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_upload_info.gm_fch_upload_json_file_info", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.setString(1, strFileListIds);
		gmDBManager.execute();
		strFileJSON = GmCommonClass.parseNull(gmDBManager.getString(2));
		gmDBManager.close();
		return strFileJSON;
	} // End of fetchJSONUploadFilesInfo

}
