package com.globus.common.beans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;

import org.apache.commons.validator.routines.DateValidator;

public class GmCalenderOperations {
  public static TimeZone timeZone = TimeZone.getDefault();


  /**
   * setTimeZone - set time zone for Calendar and simpleDateFormat
   * 
   * @param strTimeZone
   * @return
   */
  public static void setTimeZone(String strTimeZone) {
    timeZone = TimeZone.getTimeZone(strTimeZone);
  }

  /**
   * getCalendarTZ - get Calendar Object with time zone
   * 
   * @param
   * @return Calendar
   */
  public static Calendar getCalendarTZ() {
    Calendar c = GregorianCalendar.getInstance();
    c.setTimeZone(timeZone);
    return c;
  }

  /**
   * getSimpleDateFormatTZ - get Simple Date Format Object with time zone
   * 
   * @param
   * @return SimpleDateFormat
   */
  public static SimpleDateFormat getSimpleDateFormatTZ(String strFormat) {
    SimpleDateFormat fmtTz = new SimpleDateFormat(strFormat);
    fmtTz.setTimeZone(timeZone);
    return fmtTz;
  }

  public static final int FIRSTDAY = 1;
  public static final int LASTDAY = 31;

  private static String convertDate(Date dt, String strFormat) {
    Date zDate = null;
    SimpleDateFormat fmt = new SimpleDateFormat(strFormat);
    String dateStr = fmt.format(dt);

    SimpleDateFormat fmtTz = getSimpleDateFormatTZ(strFormat);

    try {
      zDate = fmtTz.parse(dateStr);
    } catch (ParseException pe) {

    }
    return fmtTz.format(zDate);
  }

  /**
   * addDays - return the string format of a date with the 'days' added to the current date
   * 
   * @param days, strFormat
   * @return
   */
  public static String addDays(int days, String strFormat) {
    Calendar c = getCalendarTZ();
    c.add(Calendar.DATE, days);
    SimpleDateFormat fmtTz = getSimpleDateFormatTZ(strFormat);
    return fmtTz.format(c.getTime());
  }

  /**
   * addDays - return the string format of a date with the 'days' added to the current date
   * 
   * @param days
   * @return
   */
  public static String addDays(int days) {
    String dateStr = addDays(days, "MM/dd/yyyy");
    return dateStr;
  }

  /**
   * addMonths - this method will add 'months' to todays date and return date in string format
   * 
   * @param months
   * @return
   */
  public static String addMonths(int months, String strformat) {
    DateFormat dateFormat = getSimpleDateFormatTZ(strformat);
    Calendar cal = getCalendarTZ();
    cal.add(cal.MONTH, -1);
    String strDate = dateFormat.format(cal.getTime()).toString();
    return strDate;
  }

  /**
   * addMonths - this method will add 'months' to todays date and return date in string format
   * 
   * @param months
   * @return
   */
  public static String addMonths(int months) {

    String strDate = addMonths(months, "MM/dd/yyyy");
    return strDate;
  }

  // need to check
  public static Date getDate(String strDate) throws ParseException {
    SimpleDateFormat fmt = new SimpleDateFormat("MM/dd/yyyy");
    Date cDate = fmt.parse(strDate);
    return cDate;
  }

  // Over ride method to get date by format
  public static Date getDate(String strDate, String strformat) throws ParseException {
    SimpleDateFormat fmt = new SimpleDateFormat(strformat);
    Date cDate = fmt.parse(strDate);
    return cDate;
  }

  public static String getCurrentDate(String strFormat) {
    Calendar c = getCalendarTZ();
    SimpleDateFormat fmt = getSimpleDateFormatTZ(strFormat);
    String dateStr = fmt.format(c.getTime());
    return dateStr;
  }

  /**
   * formDateString - Form the String format of a date for the given month, year and the day
   * 
   * @param month
   * @param year
   * @param day - ( First Day or Last Day of the month)
   */

  public static String formDateString(String month, String year, int day) {
    SimpleDateFormat fmt = getSimpleDateFormatTZ("MM/dd/yyyy");
    Calendar c = Calendar.getInstance();
    int imon = Integer.parseInt(GmCommonClass.parseZero(month)) - 1;
    int iyear = Integer.parseInt(GmCommonClass.parseZero(year));
    String strReturn = "";


    c.set(Calendar.MONTH, imon);
    c.set(Calendar.YEAR, iyear);

    if (day == GmCalenderOperations.LASTDAY) {
      c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
    } else if (day == GmCalenderOperations.FIRSTDAY) {
      c.set(Calendar.DAY_OF_MONTH, 1);
    } else {
      c.set(Calendar.DAY_OF_MONTH, day);
    }

    strReturn = fmt.format(c.getTime());

    return strReturn;
  }

  /**
   * getCurrentMonth - returns the current month
   * 
   * @return
   */
  public static int getCurrentMonth() {
    return Integer.parseInt(getCurrentMM());
  }

  /**
   * getCurrentMM - returns the current month
   * 
   * @return
   */
  public static String getCurrentMM() {
    Calendar c = getCalendarTZ();
    SimpleDateFormat fmt = getSimpleDateFormatTZ("MM");
    return fmt.format(c.getTime());
  }

  /**
   * getCurrentMonth - returns the current Year
   * 
   * @return
   */
  public static int getCurrentYear() {
    Calendar c = getCalendarTZ();
    SimpleDateFormat fmt = getSimpleDateFormatTZ("yyyy");
    return Integer.parseInt(fmt.format(c.getTime()));
  }

  /**
   * calculateRange - calculate the months between the given todate and the range.
   * 
   * @param intRange - months range to be calculated
   * @param strToMonth
   * @param strFromMonth
   * @param bExcludeCurrentMonth - is used because this method can be used to calculate the range
   *        excluding the current month by setting this flag to true.
   * @return
   * @throws AppError
   */

  public static HashMap calculateRange(int intRange, String strToMonth, String strToYear,
      boolean bExcludeCurrentMonth) {
    Date toDate = new Date();
    Date fromDate = new Date();
    Calendar c = getCalendarTZ();
    HashMap hmReturn = new HashMap();
    String strFromMonth = "";
    String strFromYear = "";

    /*
     * In Calendar Class the Months are from 0(January) to 11(December) To Map the MONTH passed to
     * MONTH of 'Calender Class' reduce the strToMonth
     */

    int intToMonth = Integer.parseInt(strToMonth) - 1;
    int intToYear = Integer.parseInt(strToYear);

    /*
     * if intRange is 3 and strToMonth is 3(April), then strFromMonth is 3-3 = 0(January) so the
     * date range will be from 1st of jan to 31st of april (Which is actually 4 months) To avoid
     * this reduce the intRange Passed
     */

    intRange--;

    toDate.setMonth(intToMonth);
    toDate.setYear(intToYear);
    c.setTime(toDate);
    /*
     * Exclude the current month if the bExcludeCurrentMonth is true and strToMonth passed is
     * current month.
     */

    if (toDate.getMonth() == (getCalendarTZ()).getTime().getMonth() && bExcludeCurrentMonth) {
      c.add(Calendar.MONTH, -1);
      toDate = c.getTime();
    }

    /*
     * Calculate the from month by subtracting the range from to month
     */
    c.add(Calendar.MONTH, -intRange);
    fromDate = c.getTime();

    /*
     * Append "0" to the months if they are less than or equals to "9" because the database query
     * used will expect "MM" for month.
     */
    strFromMonth =
        (fromDate.getMonth() + 1) < 9 ? "0" + (fromDate.getMonth() + 1) : String.valueOf((fromDate
            .getMonth() + 1));
    strToMonth =
        (toDate.getMonth() + 1) < 9 ? "0" + (toDate.getMonth() + 1) : String.valueOf((toDate
            .getMonth() + 1));

    strFromYear = String.valueOf(fromDate.getYear());
    strToYear = String.valueOf(toDate.getYear());

    hmReturn.put("FromMonth", strFromMonth);
    hmReturn.put("FromYear", strFromYear);
    hmReturn.put("ToMonth", strToMonth);
    hmReturn.put("ToYear", strToYear);

    return hmReturn;
  }

  // For code merge
  public static String getFirstDayOfMonth(int intMonth, String strApplnDateFmt) {
    SimpleDateFormat fmt = getSimpleDateFormatTZ(strApplnDateFmt);
    // Get the time right now
    Calendar calendar = getCalendarTZ();
    calendar.set(Calendar.MONTH, intMonth);
    // Set it to the first day of whatever month it is now
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    return fmt.format(calendar.getTime());
  }

  public static String getFirstDayOfMonth(int intMonth) {

    String strFirstDayofMonth = getFirstDayOfMonth(intMonth, "MM/dd/yyyy");
    return strFirstDayofMonth;
  }

  public static String getLastDayOfMonth(int intMonth, String strApplnDateFmt) {
    SimpleDateFormat fmt = getSimpleDateFormatTZ(strApplnDateFmt);
    // Get the time right now
    Calendar calendar = getCalendarTZ();
    calendar.set(Calendar.MONTH, intMonth);
    // Set it to the first day of whatever month it is now
    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
    return fmt.format(calendar.getTime());
  }

  public static String getLastDayOfMonth(int intMonth) {
    String strLastDayofMonth = getLastDayOfMonth(intMonth, "MM/dd/yyyy");
    return strLastDayofMonth;
  }

  public static String getCalculatedDateFromToDate(int intDuration, int intDateType) {
    SimpleDateFormat fmt = getSimpleDateFormatTZ("MM/dd/yyyy");
    Calendar c = getCalendarTZ();

    if (intDateType == Calendar.MONTH) {
      c.add(Calendar.MONTH, -1 * intDuration);
    } else if (intDateType == Calendar.DAY_OF_MONTH) {
      c.add(Calendar.DAY_OF_MONTH, -1 * intDuration);
    } else if (intDateType == Calendar.YEAR) {
      c.add(Calendar.YEAR, -1 * intDuration);
    }
    Date cDate = c.getTime();
    String dateStr = fmt.format(cDate);
    return dateStr;
  }
  
  public static String getCalculatedDateFromToDate(int intDuration, int intDateType, String strApplnDateFmt) {
      SimpleDateFormat fmt = getSimpleDateFormatTZ(strApplnDateFmt);
      Calendar c = getCalendarTZ();

      if (intDateType == Calendar.MONTH) {
        c.add(Calendar.MONTH, -1 * intDuration);
      } else if (intDateType == Calendar.DAY_OF_MONTH) {
        c.add(Calendar.DAY_OF_MONTH, -1 * intDuration);
      } else if (intDateType == Calendar.YEAR) {
        c.add(Calendar.YEAR, -1 * intDuration);
      }
      Date cDate = c.getTime();
      String dateStr = fmt.format(cDate);
      return dateStr;
    }


  public static boolean isEqual(String lhsDate, String rhsDate, String dateFormat) {
    boolean result = false;
    try {
      DateValidator dateVal = DateValidator.getInstance();
      Date d1 = dateVal.validate(lhsDate, dateFormat);
      Date d2 = dateVal.validate(rhsDate, dateFormat);
      int dateCompare = dateVal.compareDates(d1, d2, null);
      if (dateCompare == 0)
        result = true;
      else
        result = false;
    } catch (Exception pe) {
      // pe.printStackTrace();
    }
    return result;
  }

  public static boolean isLessThan(String lhsDate, String rhsDate, String dateFormat) {
    boolean result = false;
    try {
      DateValidator dateVal = DateValidator.getInstance();
      Date d1 = dateVal.validate(lhsDate, dateFormat);
      Date d2 = dateVal.validate(rhsDate, dateFormat);
      int dateCompare = dateVal.compareDates(d1, d2, null);
      if (dateCompare == -1)
        result = true;
      else
        result = false;
    } catch (Exception pe) {
      // pe.printStackTrace();
    }
    return result;
  }

  public static boolean isGreaterThan(String lhsDate, String rhsDate, String dateFormat) {
    boolean result = false;
    try {
      DateValidator dateVal = DateValidator.getInstance();
      Date d1 = dateVal.validate(lhsDate, dateFormat);
      Date d2 = dateVal.validate(rhsDate, dateFormat);
      int dateCompare = dateVal.compareDates(d1, d2, null);

      if (dateCompare == 1)
        result = true;
      else
        result = false;
    } catch (Exception pe) {
      // pe.printStackTrace();
    }
    return result;
  }

  /*
   * Gets the current quarter code. For eg, if today is July 11, quarter code will be 3
   */
  public static int getCurrentQuarterCode() {


    Calendar calendar = getCalendarTZ();
    int month = calendar.get(Calendar.MONTH) + 1;
    System.out.println(" month " + month);
    int quarter = monthCodeToQuarter(month);
    return quarter;
  }

  /*
   * Gets the first day of current quarter. For eg, if today is July 11, quarter start day will be
   * July 1
   */
  public static String getFirstDayOfCurrentQuarter() {
    int quarterCode = getCurrentQuarterCode();
    int quarterStart = 0;

    switch (quarterCode) {
      case 1:
        quarterStart = 1;
        break;
      case 2:
        quarterStart = 4;
        break;
      case 3:
        quarterStart = 7;
        break;
      case 4:
        quarterStart = 10;
        break;
      default:
        throw new IllegalArgumentException("getFirstDayOfQuarter: invalid month code.");
    }

    return getFirstDayOfMonth(quarterStart - 1); // negating so we get the correct month
  }

  /*
   * Gets the last day of current quarter. For eg, if today is July 11, quarter start day will be
   * Sep 30
   */
  public static String getLastDayOfCurrentQuarter() {
    int quarterCode = getCurrentQuarterCode();
    int quarterEnd = 0;

    switch (quarterCode) {
      case 1:
        quarterEnd = 3;
        break;
      case 2:
        quarterEnd = 6;
        break;
      case 3:
        quarterEnd = 9;
        break;
      case 4:
        quarterEnd = 12;
        break;
      default:
        throw new IllegalArgumentException("getFirstDayOfQuarter: invalid month code.");
    }

    return getLastDayOfMonth(quarterEnd - 1);// negating so we get the correct month
  }

  /*
   * Depending on the month code, we get the corresponding quarter number, for eg, if we send July
   * (7), we get back 3
   */
  public static int monthCodeToQuarter(final int code) {

    switch (code) {
      case 1:
      case 2:
      case 3:
        return 1;
      case 4:
      case 5:
      case 6:
        return 2;
      case 7:
      case 8:
      case 9:
        return 3;
      case 10:
      case 11:
      case 12:
        return 4;
      default:
        throw new IllegalArgumentException("monthCodeToQuarter: invalid month code.");
    }
  }

  /*
   * Depending on the month code, we get the corresponding quarter number, for eg, if we send July
   * (7), we get back 3
   */
  public static String getMonForMM(String strCode) {

    int code = Integer.parseInt(GmCommonClass.parseZero(strCode));
    switch (code) {
      case 1:
        return "Jan";
      case 2:
        return "Feb";
      case 3:
        return "Mar";
      case 4:
        return "Apr";
      case 5:
        return "May";
      case 6:
        return "Jun";
      case 7:
        return "Jul";
      case 8:
        return "Aug";
      case 9:
        return "Sep";
      case 10:
        return "Oct";
      case 11:
        return "Nov";
      case 12:
        return "Dec";
      default:
        throw new IllegalArgumentException("monthCodeToQuarter: invalid month code.");
    }
  }

  /*
   * This method will add intMonthAdd months to the strDate. If blIncludeCurrentMonth is true, then
   * current month is included in the addition Eg: If strDate is 02/02/2009 and intMonthAdd = 3 and
   * blIncludeCurrentMonth = true, then output is 04/02/2009 If strDate is 02/02/2009 and
   * intMonthAdd = 3 and blIncludeCurrentMonth = false, then output is 05/02/2009
   */
  public static String addMonthsFromDate(String strDate, int intMonthAdd,
      boolean blIncludeCurrentMonth) {

    int intdiff = 1;

    if (blIncludeCurrentMonth) {
      intdiff = 2; // Because for adding 2 months, in Java we need to add 1.
    }

    intMonthAdd = intMonthAdd - intdiff;

    String DATE_FORMAT = "MM/dd/yyyy";
    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);
    Calendar c1 = Calendar.getInstance();
    String[] strDateSplit = strDate.split("/");
    c1.set(Integer.parseInt(strDateSplit[2]), Integer.parseInt(strDateSplit[0]),
        Integer.parseInt(strDateSplit[1]));
    // c1.set(1999, 0 , 20); // 1999 jan 20
    c1.add(Calendar.MONTH, intMonthAdd);
    return sdf.format(c1.getTime());

  }

  /*
   * This method used to get the dynamic date format Eg: If the day is passed null, then get the
   * first day; else get whichever number is passed. For e.g. if I pass 15, I need to get 15th date
   * of the moth. If I pass 2, I should get 2nd day of the month.
   */
  public static String getAnyDateOfMonth(int intMonth, int intDay, String strDateFmt) {
    if (intDay == 0) {
      intDay = 1;
    }
    SimpleDateFormat fmt = getSimpleDateFormatTZ(strDateFmt);
    // Get the time right now
    Calendar calendar = getCalendarTZ();
    calendar.set(Calendar.MONTH, intMonth);
    // Set it to the first day of whatever month it is now
    calendar.set(Calendar.DAY_OF_MONTH, intDay);
    return fmt.format(calendar.getTime());
  }

}
