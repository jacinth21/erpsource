package com.globus.common.beans;

import com.globus.common.beans.AppError;
import org.apache.commons.beanutils.RowSetDynaClass;

public interface GmCommonLogInterface {

  public RowSetDynaClass getCallLog(String strID, String strType, String strJNDIConnection)
      throws AppError;

}
