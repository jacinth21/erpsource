/**
 * This class contains methods that are common to PARTY Component
 * 
 * @author $Author:$
 * @version $Revision:$ <br>
 *          $Date:$
 */

package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.GmPartyContactVO;
import com.globus.valueobject.common.GmPartyVO;

public class GmPartyBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger

  // this will be removed all place changed with Data Store VO constructor

  public GmPartyBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPartyBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // Class.

  /**
   * reportProject - This method will fetch party information from party table
   * 
   * @param String strType -- Type for party to be retrieved
   * 
   * @return ArrayList List of party name
   * @exception AppError
   **/
  public ArrayList reportPartyNameList(String strType) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;

    try {
      gmDBManager.setPrepareString("GM_REPORT_PARTY", 3);
      /*
       * register out parameter and set input parameters
       */
      gmDBManager.registerOutParameter(2, java.sql.Types.VARCHAR);
      gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
      gmDBManager.setString(1, strType);

      gmDBManager.execute();
      strBeanMsg = gmDBManager.getString(2);
      rs = (ResultSet) gmDBManager.getObject(3);
      alReturn = gmDBManager.returnArrayList(rs);
      gmDBManager.close();

    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    }

    return alReturn;
  }

  public Object fetchPartyRuleParam(String strPartyId, String strType, String strReturnType)
      throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_cm_party.gm_cm_fch_partyruleparam", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strType);
    gmDBManager.setString(2, strPartyId);

    gmDBManager.execute();
    if (strReturnType != null && strReturnType.equalsIgnoreCase("Print")) {
      hmReturn = gmDBManager.returnConvertedHashMap((ResultSet) gmDBManager.getObject(3));
      gmDBManager.close();
      return hmReturn;
    } else {
      ArrayList hmReturnList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
      gmDBManager.close();
      return hmReturnList;
    }
  }

  public String createParty(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    int intPartyId = 0;

    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strFName = GmCommonClass.parseNull((String) hmParam.get("FIRSTNAME"));
    String strLName = GmCommonClass.parseNull((String) hmParam.get("LASTNAME"));
    String strFullName = GmCommonClass.parseNull((String) hmParam.get("FULLNAME"));
    String strPartyType = GmCommonClass.parseNull((String) hmParam.get("PARTYTYPE"));
    String strActiveFlag = GmCommonClass.parseNull((String) hmParam.get("ACTIVEFLAG"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_cm_party.gm_cm_sav_party_id", 8);
    gmDBManager.registerOutParameter(8, java.sql.Types.CHAR);

    gmDBManager.setString(1, strPartyId);
    gmDBManager.setString(2, strFName);
    gmDBManager.setString(3, strLName);
    gmDBManager.setString(4, strFullName);
    gmDBManager.setString(5, strPartyType);
    gmDBManager.setString(6, strActiveFlag);
    gmDBManager.setString(7, strUserId);

    gmDBManager.execute();
    intPartyId = gmDBManager.getInt(8);
    gmDBManager.commit();

    strPartyId = String.valueOf(intPartyId);
    return strPartyId;
  }

  /**
   * This method used for web service which is used to fetch fetch Party Details for party id for
   * party type 7000 and this method will return 1000 records at a time from procedure. the records
   * returned from procedure are of surgeon type.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public List<GmPartyVO> fetchParties(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    List<GmPartyVO> listGmPartyVO;
    GmPartyVO gmPartyVO = (GmPartyVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_cm_party.gm_fch_all_parties", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmPartyVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmPartyVO,
            gmPartyVO.getSystemProperties());
    gmDBManager.close();
    return listGmPartyVO;
  }

  /**
   * This method used for web service which is used to fetch fetch Party Contact Details (only for
   * mode E-mail) for party id for party type 7000 and this method will return 1000 records at a
   * time from procedure. the records returned from procedure are of surgeon type.
   * 
   * @param hmParams
   * @return
   * @throws AppError
   */
  public List<GmPartyContactVO> fetchPartyContacts(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();
    List<GmPartyContactVO> listGmPartyContactVO;
    GmPartyContactVO gmPartyContactVO = (GmPartyContactVO) hmParams.get("VO");
    gmDBManager.setPrepareString("gm_pkg_pdpc_prodcatrpt.gm_fch_all_party_contact", 5);
    gmDBManager.setString(1, (String) hmParams.get("TOKEN"));
    gmDBManager.setString(2, (String) hmParams.get("LANGUAGE"));
    gmDBManager.setString(3, (String) hmParams.get("PAGENO"));
    gmDBManager.setString(4, (String) hmParams.get("UUID"));
    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.execute();
    listGmPartyContactVO =
        gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(5), gmPartyContactVO,
            gmPartyContactVO.getSystemProperties());
    gmDBManager.close();
    return listGmPartyContactVO;
  }

  /**
   * This method is used to fetch the companies for the header section in Enterprise portal..
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public ArrayList fetchPartyCompany(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alResult = new ArrayList();

    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strPartyType = GmCommonClass.parseNull((String) hmParam.get("COMPTYP"));

    gmDBManager.setPrepareString("gm_pkg_cm_party.gm_cm_fch_partycompany", 3);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.setString(2, strPartyType);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();

    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(3)));


    gmDBManager.close();

    return alResult;
  }

  /**
   * This method is used to fetch the companies other than default company for the user in user
   * profile available Companies section.This method is calling
   * gm_pkg_cm_party.gm_cm_fch_nonasstcompany procedure to fetch the available companies for the
   * particular user in user profile screen.
   * 
   * @param hmParam :passing party id of the user.
   * @return alResult : returns the avaialble companies for the user based on party id.
   * @throws AppError
   */
  public ArrayList fetchNonAsstCompany(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alAssCompany = new ArrayList();

    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));

    gmDBManager.setPrepareString("gm_pkg_cm_party.gm_cm_fch_nonasstcompany", 2);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);

    gmDBManager.execute();
    alResult =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));

    gmDBManager.close();
    return alResult;
  }

  /**
   * This method is used to save the default party company for the New user in user profile
   * screen.This method is calling gm_pkg_cm_party.gm_cm_sav_partycompany procedure to save the
   * default company for the new user.
   * 
   * @param hmParam:passing company id ,party id of the user,party type and login user id.
   * @return alResult: returns the default company for the user.
   * @throws AppError
   */
  public void savePartyCompany(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));

    gmDBManager.setPrepareString("gm_pkg_cm_party.gm_cm_sav_partycompany", 4);
    gmDBManager.setString(1, strCompanyId);
    gmDBManager.setString(2, "105400");// Primary
    gmDBManager.setString(3, strPartyId);
    gmDBManager.setString(4, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * This method is used to save the associate companies for the user in user profile screen.Thsi
   * method calling gm_pkg_cm_party.gm_cm_sav_asstcompany procedure to save the associated companies
   * in the user profile screen.we are calling this method when trying to add available companies to
   * associated companies in the user profile screen.
   * 
   * @param hmParam :passing the associate companies ,party id , Login user id and party type.
   * @return alResult :returns the associated companies.
   * @throws AppError
   */
  public void saveAsstCompany(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strAssCompany = GmCommonClass.parseNull((String) hmParam.get("STRASSCOMPANY"));
    String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_cm_party.gm_cm_sav_asstcompany", 4);
    gmDBManager.setString(1, strAssCompany);
    gmDBManager.setString(2, "105401"); // non primary
    gmDBManager.setString(3, strPartyId);
    gmDBManager.setString(4, strUserId);

    gmDBManager.execute();
    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strPartyId, strLogReason, strUserId, "10304548");
    }

    gmDBManager.commit();
  }

  /**
   * This method is used to update the security groups based on the change of associated company.
   * 
   * @param hmParam
   * @return
   * @throws AppError
   */
  public void saveSecurityGrpMap(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strPartyId = GmCommonClass.parseNull((String) hmParam.get("PARTYID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    gmDBManager.setPrepareString("gm_pkg_cm_party.gm_upd_group_mapping", 2);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.setString(2, strUserId);

    gmDBManager.execute();
    gmDBManager.commit();
  }

}
