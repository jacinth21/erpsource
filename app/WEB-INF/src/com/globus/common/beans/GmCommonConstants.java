package com.globus.common.beans;

public class GmCommonConstants {
	public static final String ACCOUNTING = "com.globus.accounts";
	public static final String CLINICAL = "com.globus.clinical";
	public static final String COMMON = "com.globus.common";
	public static final String CUSTOMERSERVICE = "com.globus.custservice";
	public static final String LOGON = "com.globus.logon";
	public static final String MANUFACTURING = "com.globus.manufact";
	public static final String OPERATIONS = "com.globus.operations";
	public static final String PRODMGMT = "com.globus.prodmgmnt";
	public static final String SALES = "com.globus.sales";
	public static final String JSP = "jsp";
	
	public static final String DEPT_ACCOUNTS = "2000";
	public static final String DEPT_CLINICALAFFAIRS = "2007";	
	public static final String DEPT_CUSTSERVICE = "2001";
	public static final String DEPT_LOGISTICS = "2014";
	public static final String DEPT_BIOMATERIALS = "2008";
	public static final String DEPT_OPERATIONS = "2003";
	public static final String DEPT_PRODDEV = "2004";
	public static final String DEPT_PURCHASING = "2010";
	public static final String DEPT_QUALITY = "2009";	
	public static final String DEPT_SALES = "2005";
	public static final String DEPT_SHIPPING = "2012";	
	
}
