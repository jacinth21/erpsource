/**
 * FileName    : GmRuleBean.java 
 * Description :
 * Author      : rshah
 * Date        : Jun 3, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.beans;

import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author rshah
 *
 */
public class GmRuleBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	GmCommonBean gmCommonBean = new GmCommonBean();
	
	  public GmRuleBean() {
		  super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmRuleBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	
	/**
     * saveConditions - This method saves the condition
     * @param hmParam - parameters to be saved
     * @exception AppError
     * @return String
     */
	public String saveConditions(HashMap hmParam) throws AppError
	{
		GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());
        String strRuleNm = GmCommonClass.parseNull((String)hmParam.get("RULENAME"));
        String strExpDt = GmCommonClass.parseNull((String)hmParam.get("EXPIRYDATE"));
        String strIniBy = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        String strIniDt = GmCommonClass.parseNull((String)hmParam.get("INITIATEDDATE"));
        String strActv = GmCommonClass.getCheckBoxValue((String)hmParam.get("ACTIVEFL"));
        String strRuleEmailNotify = GmCommonClass.getCheckBoxValue((String)hmParam.get("RULEEMAILNOTIFY"));
        String strRuleComment = GmCommonClass.parseNull((String)hmParam.get("RULECOMMENT"));
        String strInputTrans = GmCommonClass.parseNull((String)hmParam.get("TRANSSTR"));
        String strCondition = GmCommonClass.parseNull((String)hmParam.get("CONDITIONSINPUTSTR"));
        String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        String strRuleId = GmCommonClass.parseNull((String)hmParam.get("RULEID"));
        String strRuleTxnType = GmCommonClass.parseNull((String)hmParam.get("RULETXNTYPE"));
        log.debug("strRuleTxnType = "+strRuleTxnType);
        log.debug("strRuleId = "+strRuleId);
        log.debug("strInputTrans = "+strInputTrans);
        gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_sav_rule_condition",12);        
        gmDBManager.registerOutParameter(12,java.sql.Types.CHAR);
        gmDBManager.setString(1,strRuleNm);
        gmDBManager.setString(2,strExpDt);
        gmDBManager.setString(3,strIniBy);
        gmDBManager.setString(4,strIniDt);
        gmDBManager.setString(5,strActv);
        gmDBManager.setString(6,strRuleEmailNotify);
        gmDBManager.setString(7,strRuleComment);        
        gmDBManager.setString(8,strInputTrans);        
        gmDBManager.setString(9,strCondition);
        gmDBManager.setString(10,strUserId);
        gmDBManager.setString(11,strRuleTxnType);
        gmDBManager.setString(12,strRuleId);
        
        gmDBManager.execute();        
        strRuleId = gmDBManager.getString(12);
        log.debug("strRule Id..."+ strRuleId);
        gmDBManager.commit();
		return strRuleId;
	}
	/**
     * fetchRules - This method will be used to fetch the Rules. 
     * @param strRuleId
     * @return HashMap 
     * @exception AppError
     */
    
    public HashMap fetchRules(String strRuleId) throws AppError
    {
    	ArrayList alRule  = new ArrayList();
    	ArrayList alCondition  = new ArrayList();
    	ArrayList alTransaction  = new ArrayList();
    	String strTransaction = "";
    	HashMap hmReturn = new HashMap();
    	
    	GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());
        gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_rule",3);
        gmDBManager.setInt(1,Integer.parseInt(strRuleId));
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
        gmDBManager.registerOutParameter(3,java.sql.Types.CHAR);
        log.debug("rule id...."+strRuleId);
	    gmDBManager.execute();
	    alRule = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
	    strTransaction = gmDBManager.getString(3);
	    log.debug("alRule..."+alRule);
	    log.debug("strTransaction..."+strTransaction);
	    hmReturn.put("ALRULE", alRule);
	    hmReturn.put("TRANSACTION", strTransaction);
	    gmDBManager.close();
    	return hmReturn;
    }
    /**
     * saveConsequences - This method saves the Consequences
     * @param hmParam - parameters to be saved
     * @exception AppError
     * @return void
     */
    public void saveConsequences(HashMap hmParam) throws AppError
    {
    	GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());
        String strRuleId = GmCommonClass.parseNull((String)hmParam.get("RULEID"));
        String strRuleConsId = GmCommonClass.parseNull((String)hmParam.get("RULECONSEQUENCEID"));
        String strConsGrpId = GmCommonClass.parseNull((String)hmParam.get("CONSEQUENCEID"));
        String strInputTrans = GmCommonClass.parseNull((String)hmParam.get("CONSEQUENCESTR"));        
        String strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
        log.debug(" IN rule bean....strConsGrpId = "+strConsGrpId);
        log.debug("strRuleId = "+strRuleId);
        log.debug("CONSEQUENCESTR = "+strInputTrans +"...strRuleConsId  = "+strRuleConsId);
        gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_sav_consequences",5);        
        gmDBManager.setString(1,strRuleId);
        gmDBManager.setString(2,strRuleConsId);
        gmDBManager.setString(3,strConsGrpId);
        gmDBManager.setString(4,strInputTrans);        
        gmDBManager.setString(5,strUserId);
        gmDBManager.execute();        
        gmDBManager.commit();
    
    }
    
    /**
     * fetchConsequence - This method will be used to fetch the Consequence. 
     * @param strRuleId,strConsGrpId
     * @return ArrayList 
     * @exception AppError
     */
    
    public ArrayList fetchConsequence(String strRuleId, String strConsGrpId) throws AppError
    {
    	ArrayList alDbConsequences  = new ArrayList();
    	GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());
        gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_consequences",3);
        gmDBManager.setInt(1,Integer.parseInt(strRuleId));
        gmDBManager.setInt(2,Integer.parseInt(strConsGrpId));
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
        log.debug("rule id...."+strRuleId+"...strConsGrpId..."+strConsGrpId);
	    gmDBManager.execute();
	    alDbConsequences = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
	    log.debug("alDbConsequences..."+alDbConsequences);
	    gmDBManager.close();
    	return alDbConsequences;
    }
    /**
     * fetchAllPicConsequence - This method will be used to fetch the Consequence. 
     * @param strRuleId,strConsGrpId
     * @return ArrayList 
     * @exception AppError
     */
    
    public ArrayList fetchAllPicConsequence(String strRuleId, String strConsGrpId) throws AppError
    {
    	ArrayList alDbConsequences  = new ArrayList();
    	GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());
        gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_allpic_consequences",3);
        gmDBManager.setInt(1,Integer.parseInt(strRuleId));
        gmDBManager.setInt(2,Integer.parseInt(strConsGrpId));
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
        log.debug("rule id...."+strRuleId+"...strConsGrpId..."+strConsGrpId);
	    gmDBManager.execute();
	    alDbConsequences = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
	    log.debug("alDbConsequences..."+alDbConsequences);
	    gmDBManager.close();
    	return alDbConsequences;
    }
    /**
     * fetchPicConsequence - This method will be used to fetch the Consequence. 
     * @param strRuleId,strConsGrpId
     * @return ArrayList 
     * @exception AppError
     */
    
    public ArrayList fetchPicConsequence(String strRuleConsId) throws AppError
    {
    	ArrayList alSelectedConsequences  = new ArrayList();
    	GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());
        gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_pic_consequences",2);
        gmDBManager.setInt(1,Integer.parseInt(strRuleConsId));
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
        log.debug("ruleCons id...."+strRuleConsId);
	    gmDBManager.execute();
	    alSelectedConsequences = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
	    log.debug("alSelectedConsequences..."+alSelectedConsequences);
	    gmDBManager.close();
    	return alSelectedConsequences;
    }
    
    public ArrayList fetchPictureMessages(String strRuleID, String strFileID) throws AppError
    {
      ArrayList alPictureMessages = new ArrayList();
      GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());
        gmDBManager.setPrepareString("gm_pkg_cm_rule_engine.gm_fch_picture_messages",3);
        gmDBManager.setInt(1,Integer.parseInt(strRuleID));
        gmDBManager.setString(2,strFileID);
        gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
          gmDBManager.execute();
          alPictureMessages = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
          return alPictureMessages;
    }
    
    /**
     * fetchCNCountry  - This method will be used to fetch the country for consignment. 
     * @param strCNId,
     * @return String
     * @exception AppError
     */
    
    public String fetchCNCountry(String strRuleConsId) throws AppError
    {
    	String strCountry="";
    	GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_cm_rule_conditions.gm_fch_country",2);
        gmDBManager.setString(1,strRuleConsId);
        gmDBManager.registerOutParameter(2,java.sql.Types.CHAR);
        log.debug("strRuleConsId id...."+strRuleConsId);
	    gmDBManager.execute();
	    strCountry = gmDBManager.getString(2);
	    log.debug("strCountry..."+strCountry);
	    gmDBManager.close();
    	return strCountry;
    }
    
    /**
	 * fetchRulesReport - This method will be used to fetch the Rule Report.
	 * 
	 * @param hmParam
	 * @return RowSetDynaClass
	 * @exception AppError
	 */

	public RowSetDynaClass fetchRulesReport(HashMap hmParam) throws AppError {
		RowSetDynaClass rdRulesRpt = null;
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strTxnID = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
		String strInitBy = GmCommonClass.parseNull((String) hmParam.get("INITIATEDBY"));
		String strConseq = GmCommonClass.parseNull((String) hmParam.get("CONSEQUENCEID"));
		String strActiveFl = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strInputSting= GmCommonClass.parseNull((String) hmParam.get("INPUTSTR")); 
		log.debug("strInputSting = "+strInputSting);
		gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_rule_report", 6);
		gmDBManager.setString(1, strTxnID); //setInt(1, Integer.parseInt(strVendorID));
		gmDBManager.setString(2, strConseq);
		gmDBManager.setString(3, strInitBy); 
		gmDBManager.setString(4, strActiveFl);
		gmDBManager.setString(5, strInputSting);
		gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
		 
		gmDBManager.execute();
		rdRulesRpt = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(6));
		log.debug("rdRulesRpt..." + rdRulesRpt);
		gmDBManager.close();
		return rdRulesRpt;
	}
	
	/**
	 * fetchRuleShipoutDetails - This method will be used to fetch the Rule shipout details for specific ref id.
	 * @param String strRefId, String strSource
	 * @return HashMap
	 * @exception AppError
	 */

	public HashMap fetchRuleShipoutDetails(String strRefId, String strSource) throws AppError {
		HashMap hmShipOut = new HashMap();
		ArrayList alData = new ArrayList();
		String strReTxn = "";
		GmDBManager gmDBManager = new GmDBManager();
		 
		gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_rule_shipout_details", 4);
		gmDBManager.setString(1, strRefId); //setInt(1, Integer.parseInt(strVendorID));
		gmDBManager.setInt(2, Integer.parseInt(strSource));		
		gmDBManager.registerOutParameter(3,java.sql.Types.CHAR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		 
		gmDBManager.execute();
		strReTxn = gmDBManager.getString(3);
		log.debug("RE_TXN = "+strReTxn);
		alData = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
		log.debug("alData..." + alData);
		gmDBManager.close();
		hmShipOut.put("RE_TXN", strReTxn);
		hmShipOut.put("ALDATA", alData);
		return hmShipOut;
	}
	
	/**
	 * fetchRuleSummary - This method will be used to fetch the Rule Report.
	 * 
	 * @param hmParam
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchRuleSummary(HashMap hmParam) throws AppError {
		
		HashMap hmRuleDetails = new HashMap();
		HashMap hmConsequences = new HashMap();
		RowSetDynaClass rdConditionResult = null;
		RowSetDynaClass rdTransactionResult = null;
		HashMap hmReturn = new HashMap();
		

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		String strRuleID = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
		 
		log.debug(" values in hmParam " + hmParam);
		gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_rule_summary", 5);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);

		gmDBManager.setInt(1, Integer.parseInt(strRuleID)); 
		gmDBManager.execute();

		hmRuleDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		hmConsequences = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		rdConditionResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));
		rdTransactionResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));

		gmDBManager.close();
		hmReturn.put("RULEDETAIL", hmRuleDetails);
		hmReturn.put("CONSEQUENCES", hmConsequences);
		hmReturn.put("CONDITION", rdConditionResult);
		hmReturn.put("TRANSACTION", rdTransactionResult); 
		return hmReturn;

	}
	/**
     * fetchShipOutCountryID  - This method will be used to fetch the country ID  by shipping detail.  
     * @param strShipToId, strNameId, strAddressId
     * @exception AppError
     */
    
    public String fetchShipOutCountryID(String strShipToId, String strNameId, String strAddressId) throws AppError
    {
    	String strCountry="";
    	GmDBManager gmDBManager = new GmDBManager (); 
    	gmDBManager.setFunctionString ("gm_pkg_cm_rule_conditions.get_cs_ship_country", 3); 
    	gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    	gmDBManager.setString(2, strShipToId);
    	gmDBManager.setString(3, strNameId);
    	gmDBManager.setString(4, strAddressId); 
    	
	    gmDBManager.execute();
	    strCountry = gmDBManager.getString(1); 
	    gmDBManager.close();
    	return strCountry;
    }
    
    /**
     * fetchShipoutCountry  - This method will be used to fetch the country for ship out ids.  
     * @exception AppError
     */
    
    public String fetchShipOutCountry(String strAddressId) throws AppError
    {
    	String strCountry="";
    	GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_cm_rule_conditions.gm_fch_shioutcountry",2);
        gmDBManager.setString(1,strAddressId);
        gmDBManager.registerOutParameter(2,java.sql.Types.CHAR);
        log.debug("strRuleConsId id...."+strAddressId);
	    gmDBManager.execute();
	    strCountry = gmDBManager.getString(2);
	    log.debug("strCountry..."+strCountry);
	    gmDBManager.close();
    	return strCountry;
    }
    
    /**
     * fetchShipId  - This method will be used to fetch the shipid. 
     * @param strCNId,
     * @return String
     * @exception AppError
     */
    
    public String fetchShippingId(String strRefId, String strSource) throws AppError
    {
    	String strShipId="";
    	GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setFunctionString("gm_pkg_cm_shipping_info.get_shipping_id",3);
        gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
        gmDBManager.setString(2,strRefId);
        gmDBManager.setString(3,strSource);
        gmDBManager.setString(4,"");
        gmDBManager.execute();
        strShipId = gmDBManager.getString(1);
	    log.debug("strShipId..."+strShipId);
	    gmDBManager.close();
    	return strShipId;
    }
	
    /**
     * fetchLoanerType  - This method will be used to fetch the type of loaner. 
     * @param strRefId,
     * @return String
     * @exception AppError
     */
    
    public String fetchLoanerType(String strRefId) throws AppError
    {
    	String strLoanerType="";
    	GmDBManager gmDBManager = new GmDBManager ();
    	log.debug("strRefId..."+strRefId);
        gmDBManager.setFunctionString("gm_pkg_cm_rule_conditions.get_loaner_type",1);
        gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
        gmDBManager.setString(2,GmCommonClass.parseNull(strRefId));
        gmDBManager.execute();
        strLoanerType = gmDBManager.getString(1);
	    log.debug("strLoanerType..."+strLoanerType);
	    gmDBManager.close();
    	return strLoanerType;
    }
    
    /**
     * fetchEmailId  - This method will be used to fetch the email id of logged in user. 
     * @param strUserId,
     * @return String
     * @exception AppError
     */
    
    public String fetchEmailId(String strUserId) throws AppError
    {
    	String strEmailId="";
    	GmDBManager gmDBManager = new GmDBManager (getGmDataStoreVO());
    	log.debug("strUserId..."+strUserId);
        gmDBManager.setFunctionString("get_user_emailid",1);
        gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
        gmDBManager.setString(2,GmCommonClass.parseNull(strUserId));
        gmDBManager.execute();
        strEmailId = gmDBManager.getString(1);
	    log.debug("emailid..."+strEmailId);
	    gmDBManager.close();
    	return strEmailId;
    }
    
    public String fetchTransId(String strType, String strLoanerType) throws AppError
    {
    	int iTrans = Integer.parseInt(strType);
    	int iLoanerType = Integer.parseInt(strLoanerType);
    	String strTransReturn = "";
    	log.debug("iLoanerType = "+iLoanerType);
    	log.debug("iTrans = "+iTrans);
    	switch (iTrans) {
            case 4114:  strTransReturn="50911"; break;
            case 4113:  strTransReturn="50907"; break;
            case 4115:  strTransReturn="50915"; break;
            case 4116:  strTransReturn="50914"; break;
            case 4110:  strTransReturn="50927"; break; //item consignement
            case 50157:	
            	if(iLoanerType == 4127)
            			strTransReturn="50916";
            	else			
            		strTransReturn="50923"; 
            	break; //Loaner to Shelf
            		
            case 50158:
            	if(iLoanerType == 4127)
        			strTransReturn="50917";
            	else			
            		strTransReturn="50924"; 
            	break; //Loaner to quarantine
            case 50155:	
            	if(iLoanerType == 4127)
            		strTransReturn="50906";
            	else			
            		strTransReturn="50913"; 
            	break; //Shelf to loaner
            case 50160:	strTransReturn="50910"; break; //Shelf to build set
            case 50161:	strTransReturn="50919"; break; //build set to shelf
            case 50162:	strTransReturn="50918"; break; //build set to quarantine
            case 4112:	strTransReturn="50928"; break; //inhouse consignment
            case 40054:	strTransReturn="50908"; break; //For FG to Raw material
            case 40053:	strTransReturn="50926"; break; //For Raw material to FG
            case 1006575:	strTransReturn="50951"; break; //IHLE
            case 1006573:	strTransReturn="50948"; break; //For IHIS
            default: log.debug("No Transaction Type");break;
        }
    	
        return strTransReturn;

    }
    /**
	 * fetchAnalysisRuleReport - This method will be used to fetch the Analysis Report.
	 * 
	 * @param hmParam
	 * @return RowSetDynaClass
	 * @exception AppError
	 */

	public RowSetDynaClass fetchAnalysisRuleReport(HashMap hmParam) throws AppError {
		RowSetDynaClass rdRulesAnaRpt = null;
		GmDBManager gmDBManager = new GmDBManager();
		String strSetType = GmCommonClass.parseNull((String) hmParam.get("REPORTTYPE"));
		String strInputSting= GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING")); 
		log.debug("strInputSting = "+strInputSting + "strSetType: "+ strSetType);
		gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_analysis_rpt", 3);
		gmDBManager.setString(1, strSetType);//to be changed 
		gmDBManager.setString(2, strInputSting);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();
		
		rdRulesAnaRpt = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
		log.debug("rdRulesRpt..." + rdRulesAnaRpt.getRows());
		gmDBManager.close();
		return rdRulesAnaRpt;
	}
    
	 /**
	 * saveLog - This method will be used to save the log for the rules.
	 * 
	 * @param hmParam
	 * @return RowSetDynaClass
	 * @exception AppError
	 */

	public void saveRulesLog(HashMap hmParam) throws AppError 
	{
		String strInputString = GmCommonClass.parseNull((String)hmParam.get("INPUTSTRING"));
		String strOverrideFl = GmCommonClass.parseNull((String)hmParam.get("OVERRIDEFL"));
		String strOverrideComments = GmCommonClass.parseNull((String)hmParam.get("OVERRIDECOMMENTS"));
		String strTxnId = GmCommonClass.parseNull((String)hmParam.get("TXNID"));
		String strUserID = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_sav_rules_log", 5);
		gmDBManager.setString(1, strTxnId); 
		gmDBManager.setString(2, strInputString);
		gmDBManager.setString(3, strOverrideFl);
		gmDBManager.setString(4, strOverrideComments);
		gmDBManager.setString(5, strUserID);		
		gmDBManager.execute();
		gmDBManager.commit();
	}
	
	public ArrayList fetchRulesForTransaction(Map hmParam) throws AppError
	{
		ArrayList alList = new ArrayList();
		String strParameters = "";
		String strRequestor = GmCommonClass.parseNull((String)hmParam.get("requestor"));
		String strOutputReq = GmCommonClass.parseNull((String)hmParam.get("OutputReq"));
		
		strRequestor = strRequestor.equals("") ? "Resource" : strRequestor;
		boolean bOutputRequired =  strOutputReq.equals("false") ? false : true;
		hmParam.put("requestor", strRequestor);

		try {
			Iterator itr =  hmParam.keySet().iterator();
			
			while(itr.hasNext())
			{
				String strKey = (String)itr.next();
				String strValue = (String)hmParam.get(strKey) ;
				strParameters += "&" + strKey + "=" + strValue;
			}
			
			log.debug("strParameters: " + strParameters);
			
			ObjectOutputStream out = null;
			
			//PMT-33439-Apache & PHP Changes on ERP server (3rd Set)
			//InetAddress localHost = InetAddress.getLocalHost();
			//String ipAddress = localHost.getHostAddress();
			
			String ipAddress = System.getProperty("HOST_URL");

	  		URL url = new URL(ipAddress + "gmRuleEngine.do");
	  		log.debug("ipAddress"+ipAddress);
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			//PMT-33439
			
		    conn.setDoOutput(true);
		    conn.setDoInput(true);
		    conn.setRequestMethod("POST");
		    conn.setUseCaches(false);

		    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
		    writer.write(strParameters+"&skipSessionCheck=YES");
		    writer.flush();
		    writer.close(); 
		   
		    
		    	ObjectInputStream objectInputStream = new ObjectInputStream(conn.getInputStream());
		    	if(bOutputRequired)
			    {
		    		alList = (ArrayList)objectInputStream.readObject();
			    }
		    	objectInputStream.close();
		    
		    
		}
		catch(EOFException ex)
		{
			ex.printStackTrace();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw new AppError(ex);
		}
		return alList;
	}	
	
	public void printRulesForTransaction(Map hmRuleParam) throws AppError
	{
		log.debug("In printRulesForTransaction");
		hmRuleParam.put("RE_FORWARD", "gmPrintRules");
		hmRuleParam.put("OVERRIDEFL","on");
		hmRuleParam.put("requestor", "ResourceWithForward");
		hmRuleParam.put("OutputReq", "false");
		fetchRulesForTransaction(hmRuleParam);
	}
	
		
		/** This method used to fetch Rule details for Sharable Artifact
		 * @param hmParams
		 * @return
		 * @throws AppError
		 */
		public ArrayList fetchRules(HashMap hmParams)throws AppError
		{
			GmDBManager gmDBManager = new GmDBManager();
			ArrayList alResult = new ArrayList(); 
			gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_all_rulelist_prodcat",5);
			gmDBManager.setString(1,(String)hmParams.get("TOKEN"));
			gmDBManager.setString(2,(String)hmParams.get("LANGUAGE"));
			gmDBManager.setString(3,(String)hmParams.get("PAGENO"));
			gmDBManager.setString(4,(String)hmParams.get("UUID"));
			gmDBManager.registerOutParameter(5, OracleTypes.CURSOR); 
			gmDBManager.execute();	
			alResult = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5)));
			gmDBManager.close();
			return alResult;
		}

		/** This method used to fetch Common Column Details for Sales Order Screen
		 * @param String strRuleGroup
		 * @return ArrayList
		 * @author hreddy
		 * @throws AppError
		 */
		public ArrayList fetchColumnRules(String strRuleGroup)throws AppError{
			GmDBManager gmDBManager = new GmDBManager();
			ArrayList alResult = new ArrayList(); 
			gmDBManager.setPrepareString("gm_fch_column_rules",2);
			gmDBManager.setString(1,strRuleGroup); 
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); 
			gmDBManager.execute();	
			alResult = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
			gmDBManager.close();	
			return alResult;
		}

		public String fetchRevisionNumber(String strPnum, String strControlNumber) {
			GmDBManager gmDBManager = new GmDBManager();
			String strRevNum = "";
			
			gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_rev_number",3);
			gmDBManager.setString(1,strPnum);
			gmDBManager.setString(2,strControlNumber);
			gmDBManager.registerOutParameter(3, java.sql.Types.VARCHAR);
			gmDBManager.execute();
			strRevNum = GmCommonClass.parseNull(gmDBManager.getString(3));
			gmDBManager.close();
			return strRevNum;
		}
}
