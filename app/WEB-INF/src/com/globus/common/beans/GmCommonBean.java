/**
 * This class contains methods that are common to all components
 * 
 * @author $Author:$
 * @version $Revision:$ <br>
 *          $Date:$
 */

package com.globus.common.beans;

// Java Classes
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import oracle.jdbc.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.globus.clinical.beans.GmStudyBean;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.corporate.beans.GmCompanyBean;
import com.globus.custservice.beans.GmCustSalesSetupBean;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.it.beans.GmLdapBean;
import com.globus.logon.auth.GmLDAPManager;
import com.globus.logon.beans.GmLogonBean;
import com.globus.sales.beans.GmSalesFilterConditionBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.common.GmPlantVO;
import com.globus.valueobject.sales.GmCompanyVO;
import com.globus.common.beans.GmCommonLogInterface;

public class GmCommonBean extends GmBean implements GmCommonLogInterface{
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.

  public GmCommonBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmCommonBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  }

  // static ArrayList alStaticDistributorList;
  static ArrayList alStaticEmployeeList;
  static ArrayList alStaticClinicalStudyList;
  static ArrayList alStaticMonthList;
  static ArrayList alStaticYearList;
  static ArrayList alStaticOperatorList;
  static ArrayList alStaticActiveLoanerSets;
  static ArrayList alStaticActiveInHouseLoanerSets;
  static ArrayList alCompany;
  static ArrayList alPlant;
  static HashMap<String, GmCompanyVO> hmCompany;
  static HashMap<String, GmPlantVO> hmPlant;
  static HashMap<String, ArrayList> hmPlantComp;


  private String getLogQuery(String strID, String strType) {
    StringBuffer sbQuery = new StringBuffer();
    String strIdParsed = GmCommonClass.getStringWithQuotes(strID);
    sbQuery.append("SELECT  GET_USER_NAME(C902_CREATED_BY) UNAME, C902_COMMENTS COMMENTS, ");
    sbQuery.append(" TO_CHAR(C902_CREATED_DATE,'" + getCompDateFmt() + "'|| ':HH:MI:SS') DT ");
    sbQuery
        .append(" ,C902_REF_ID  ID, GET_CODE_NAME(C902_TYPE) COMMENTTYPE, C902_LOG_ID LOGID, GET_AUDIT_LOG_FLAG(c902_log_id,'902') AUDIT_LOG ");
    sbQuery.append(" FROM  T902_LOG ");
    if (strType != null && strType.equals("1200")) { // 1200 Orders LOG
      sbQuery
          .append(" WHERE  C902_REF_ID IN (SELECT c501_order_id FROM t501_order  WHERE (c501_ref_id  IN ('"
              + strIdParsed
              + "') OR c501_order_id  IN ('"
              + strIdParsed
              + "' )) AND c501_void_fl IS NULL ");
      sbQuery.append(" UNION SELECT c501_ref_id FROM t501_order WHERE (c501_ref_id  IN ('"
          + strIdParsed + "') OR c501_order_id  IN ('" + strIdParsed
          + "' )) AND c501_void_fl IS NULL ) ");
    } else {
      sbQuery.append(" WHERE  C902_REF_ID IN ('" + strIdParsed + "')");
    }

    if (strType != null) {
      sbQuery.append(" AND C902_TYPE = " + strType);
    }
    sbQuery.append(" AND C902_VOID_FL IS NULL  ORDER BY C902_LOG_ID desc");

    return sbQuery.toString();
  }

  /**
   * This method returns an ArrayList containing Comments and other information Entered by the user
   * 
   * @param strID - Code id to be added to the filter
   * @param strType - to pass the type of transaction
   * @exception AppError
   * @return ArrayList object
   */
  public ArrayList getLog(String strID, String strType) throws AppError {
    return getLog(strID, strType, "");
  }

  public ArrayList getLog(String strID, String strType, String strJNDIConnection) throws AppError {
    ArrayList arList = new ArrayList();
    GmDBManager gmDBManager = GmDBManager.getGmDBManager(strJNDIConnection);
    String strQuery = "";

    strQuery = this.getLogQuery(strID, strType);

    arList = gmDBManager.queryMultipleRecords(strQuery);

    return arList;
  }

  public RowSetDynaClass getCallLog(String strID, String strType) throws AppError {
    return getCallLog(strID, strType, "");
  }

  public RowSetDynaClass getCallLog(String strID, String strType, String strJNDIConnection)
      throws AppError {
    GmDBManager gmDBManager = GmDBManager.getGmDBManager(strJNDIConnection);
    RowSetDynaClass resultSet = null;
    String strQuery = "";

    strQuery = this.getLogQuery(strID, strType);
    resultSet = gmDBManager.QueryDisplayTagRecordset(strQuery);

    return resultSet;
  }

  /**
   * This method returns an RowSetDynaClass containing Comments and other information for the
   * selected date range or for the selected id
   * 
   * @param HashMap hmFilterCondition Holds filter information
   * 
   * @exception AppError
   * @return RowSetDynaClass object
   */
  
 public RowSetDynaClass getLog(HashMap hmFilterCondition) throws AppError {
    StringBuffer sbQuery = new StringBuffer();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    RowSetDynaClass resultSet = null;

    String strFrmDate = (String) hmFilterCondition.get("FromDate");
    String strToDate = (String) hmFilterCondition.get("ToDate");
    String strID = (String) hmFilterCondition.get("ID");
    String strInvID = GmCommonClass.parseNull((String) hmFilterCondition.get("INVID"));
    String strType = (String) hmFilterCondition.get("Type");
    String strDtFmt = (String) hmFilterCondition.get("DtFmt");
    String strUserId = (String) hmFilterCondition.get("UsrId");
    String strComments = (String) hmFilterCondition.get("Comments");
    String strCallFrom = (String) hmFilterCondition.get("CALLFROM");

    if (strType.equals("1203")) { // 1203 PO CallLog

      sbQuery.append("SELECT T902.* FROM T401_PURCHASE_ORDER T401,( ");

    }

    sbQuery.append("SELECT  GET_USER_NAME(T902.C902_CREATED_BY) UNAME,  REPLACE(REPLACE (T902.C902_COMMENTS,CHR(13),' '),CHR(10),' ') COMMENTS, ");
    sbQuery.append(" TO_CHAR(T902.C902_CREATED_DATE,'");
    sbQuery.append(strDtFmt);
    sbQuery.append(":HH:MI:SS') DT, ");
    sbQuery.append(" GET_LOG_ACCOUNT_NAME(T902.C902_REF_ID, T902.C902_TYPE) A_NAME");
    sbQuery.append(" ,T902.C902_REF_ID  ID ");
    if(strType.equals("1200")){
    	sbQuery.append(" ,t501.c503_invoice_id INVID");
    	  sbQuery.append(" FROM  T902_LOG T902,t501_order t501 ");
    }else{
    sbQuery.append(" FROM  T902_LOG T902");
    }
    sbQuery.append(" WHERE T902.C902_TYPE = ");
    sbQuery.append(strType);
 
    if(strType.equals("1200")){
    	sbQuery.append(" and t501.C501_ORDER_ID  = T902.C902_REF_ID and t501.C501_VOID_FL is null");
    }
    if (!strFrmDate.equals("") && !strToDate.equals("")) {
      sbQuery.append(" AND TRUNC(T902.C902_CREATED_DATE)  >= TO_DATE('");
      sbQuery.append(strFrmDate);
      sbQuery.append("', '" + strDtFmt + "' ) ");
      sbQuery.append(" AND TRUNC(T902.C902_CREATED_DATE)  <= TO_DATE('");
      sbQuery.append(strToDate);
      sbQuery.append("', '" + strDtFmt + "' ) ");
    }

    if (!strID.equals("")) {
      sbQuery.append(" AND T902.C902_REF_ID = '");
      sbQuery.append(strID);
      sbQuery.append("' ");
    }
    if (!strUserId.equals("0")) {
      sbQuery.append(" AND T902.C902_CREATED_BY = '");
      sbQuery.append(strUserId);
      sbQuery.append("' ");
    }
    if (!strComments.equals("")) {
      sbQuery.append(" AND UPPER(T902.C902_COMMENTS) like Upper('%");
      sbQuery.append(strComments);
      sbQuery.append("%') ");
    }
    sbQuery.append(" AND T902.c1900_company_id = " + getCompId());
    if (strType.equals("1203")) { // 1203 PO CallLog

      sbQuery.append(" )T902 WHERE T401.C401_PURCHASE_ORD_ID = T902.ID ");
      sbQuery.append(" AND T401.C1900_COMPANY_ID = " + getCompId() + " ");
      sbQuery.append(" ORDER BY UNAME,DT ");
    } else {
      sbQuery.append(" ORDER BY A_NAME, C902_CREATED_DATE ");
    }
    log.debug("sbQuery.toString()"+sbQuery.toString());
    resultSet = gmDBManager.QueryDisplayTagRecordset(sbQuery.toString());

    return resultSet;

  }

  /**
   * This method returns an RowDynaSet containing Comments and other information Entered by the user
   * 
   * @param strID - Code id to be added to the filter
   * @exception AppError
   * @return ArrayList object
   */
  public ArrayList getLog(String strID) throws AppError {
    return getLog(strID, null);
  }

  /**
   * This method returns an RowDynaSet containing Comments and other information Entered by the user
   * 
   * @param strID - Code id to be added to the filter
   * @exception AppError
   * @return ArrayList object
   */
  public HashMap getUpdateHistory(String TableName, String strColName, String strID)
      throws AppError {
    String strTableId = TableName.substring(1, 4);
    String strCreatedBy = "C".concat(strTableId).concat("_CREATED_BY");
    String strCreatedDate = "C".concat(strTableId).concat("_CREATED_DATE");
    String strLastUpdatedBy = "C".concat(strTableId).concat("_LAST_UPDATED_BY");
    String strLastUpdatedDate = "C".concat(strTableId).concat("_LAST_UPDATED_DATE");

    HashMap hmUpdHistory = new HashMap();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbQuery.append(" SELECT GET_USER_NAME(");
    sbQuery.append(strCreatedBy);
    sbQuery.append(") CREATEDBY , ");
    sbQuery.append(strCreatedDate);
    sbQuery.append(" CREATEDDATE , GET_USER_NAME(");
    sbQuery.append(strLastUpdatedBy);
    sbQuery.append(") LASTUPDATEDBY , ");
    sbQuery.append(strLastUpdatedDate);
    sbQuery.append(" LASTUPDATEDDATE ");
    sbQuery.append(" FROM ");
    sbQuery.append(TableName);
    sbQuery.append(" WHERE ");
    sbQuery.append(strColName);
    sbQuery.append(" = '");
    sbQuery.append(strID);
    sbQuery.append("'");

    hmUpdHistory = gmDBManager.querySingleRecord(sbQuery.toString());
    return hmUpdHistory;
  }

  /**
   * checkUserGroup - This method will check the user is there in the given grp
   * 
   * @param String userId
   * @return String
   * @exception AppError
   */
  public String checkUserExists(String strUserId, String strGrpId) throws AppError {
    String strReturn = "";
    log.debug("strUserId: " + strUserId + "strGrpId : " + strGrpId);
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_cm_accesscontrol.gm_fch_user_grp_chk", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.NUMBER);
    gmDBManager.setString(2, strUserId);
    gmDBManager.setString(3, strGrpId);
    gmDBManager.execute();
    strReturn = gmDBManager.getString(1);
    gmDBManager.close();

    return strReturn;

  }

  /**
   * fetchLog - This method will get the comments based on the logid
   * 
   * 
   * @param String strLogID
   * @return HashMap
   * @exception AppError
   */
  public HashMap fetchLog(String strLogID, String strJNDIConnection) throws AppError {

    log.debug("FetchLog");
    HashMap hmLogDetail = new HashMap();
    String strLog = GmCommonClass.parseNull(strLogID);

    GmDBManager gmDBManager = GmDBManager.getGmDBManager(strJNDIConnection);
    gmDBManager.setPrepareString("GM_FETCH_LOG", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strLogID);
    gmDBManager.execute();

    hmLogDetail = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));

    gmDBManager.close();
    return hmLogDetail;

  }

  public void saveLog(String strId, String strComments, String strUserID, String strType)
      throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";

    String strItemId = "";
    String strContNum = "";

    gmDBManager.setPrepareString("GM_UPDATE_LOG", 5);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);

    gmDBManager.setString(1, strId);
    gmDBManager.setString(2, strComments);
    gmDBManager.setString(3, strType);
    gmDBManager.setString(4, strUserID);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(5);
    gmDBManager.commit();


  } // end of saveControlOrderDetails


  public void saveLog(String strLogId, String strId, String strComments, String strUserID,
      String strType, String strJNDIConnection) throws Exception {
    GmDBManager gmDBManager = GmDBManager.getGmDBManager(strJNDIConnection);
    String strMsg = "";

    gmDBManager.setPrepareString("GM_SAVE_LOG", 6);
    gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);

    gmDBManager.setString(1, strId);
    gmDBManager.setString(2, strComments);
    gmDBManager.setString(3, strType);
    gmDBManager.setString(4, strUserID);
    gmDBManager.setInt(5, Integer.parseInt(GmCommonClass.parseZero(strLogId)));
    gmDBManager.execute();
    strMsg = gmDBManager.getString(6);

    gmDBManager.commit();
  }

  /**
   * saveComments - This method will be used to Save Comments entered by the user
   * 
   * @param GmDBManager gmDBManager
   * @param String strId
   * @param String strComments
   * @param String strUserID
   * @param String strType
   * @return HashMap
   * @exception AppError
   */
  public void saveLog(GmDBManager gmDBManager, String strId, String strComments, String strUserID,
      String strType) throws AppError {
    gmDBManager.setPrepareString("GM_UPDATE_LOG", 5);
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);

    gmDBManager.setString(1, strId);
    gmDBManager.setString(2, strComments);
    gmDBManager.setString(3, strType);
    gmDBManager.setString(4, strUserID);

    gmDBManager.execute();

    // gmDBManager.commit();
  } // end of saveComments

  /**
   * saveComments - This method will be used to Save Comments entered by the user
   * 
   * @param String strUsername
   * @param String strPassword
   * @return HashMap
   * @exception AppError
   */
  public void saveLog(Connection conn, String strId, String strComments, String strUserID,
      String strType) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strMsg = "";

    gmDBManager.setPrepareString("GM_UPDATE_LOG", 5);

    /*
     * register out parameter and set input parameters
     */
    gmDBManager.registerOutParameter(5, java.sql.Types.CHAR);

    gmDBManager.setString(1, strId);
    gmDBManager.setString(2, strComments);
    gmDBManager.setString(3, strType);
    gmDBManager.setString(4, strUserID);

    gmDBManager.execute();
    strMsg = gmDBManager.getString(5);
    gmDBManager.close();


  } // end of saveLog

  /**
   * This method returns an ArrayList containing Account list Account Name retrieved based on Access
   * Condition
   * 
   * @param strAccessCondition - Contains Access condition based on access level
   * @exception AppError
   * @return ArrayList object
   */
  public ArrayList getAccountList(String strAccessCondition) throws AppError {

    GmSalesFilterConditionBean gmSalesFilterConditionBean =
        new GmSalesFilterConditionBean(getGmDataStoreVO());

    ArrayList arList = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strCompLangId = GmCommonClass.parseNull(getComplangid());
    String strAccountNmColumn =
        gmSalesFilterConditionBean.getAccountNameFilterClause(strCompLangId, "T704");

    sbQuery.append("SELECT  T704.C704_ACCOUNT_ID ID, ");
    sbQuery.append(" " + strAccountNmColumn + " NAME ");
    sbQuery.append(" FROM  T704_ACCOUNT  T704 ");
    /*
     * AccessConditions are changed for VP and AD as part of PMT-28. So,VP and AD Ids are getting
     * from view with alias name of t501.
     */
    sbQuery
        .append(" ,(SELECT DISTINCT ac_id c704_account_id, rep_id c703_sales_rep_id, ad_id c501_ad_id, vp_id c501_vp_id ");
    sbQuery.append(" FROM v700_territory_mapping_detail) t501 ");
    // sbQuery.append(" WHERE C704_VOID_FL IS NULL ");

    // Filter Condition to fetch record based on access code
    if (!strAccessCondition.toString().equals("")) {
      sbQuery.append(" WHERE ");
      sbQuery.append(strAccessCondition);
    } else {
      sbQuery.append(" WHERE (T704.c901_ext_country_id is NULL ");
      sbQuery
          .append(" OR T704.c901_ext_country_id  in (select country_id from v901_country_codes))");
    }
    sbQuery.append(" AND t704.c704_account_id = t501.c704_account_id ");
    // Added Company filter
    sbQuery.append(" AND  T704.C1900_COMPANY_ID =" + getCompId());

    sbQuery.append(" ORDER BY UPPER(NAME) ");
    log.debug(" getAccountList inside GmCommonBean is  " + sbQuery.toString());
    arList = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return arList;
  }

  public ArrayList getRegions(String strCondition) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("CONDITION", strCondition);
    return getRegions(hmParam);

  }

  public ArrayList getRegions(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      String strCondition = "";
      String strCompExclusion = "";
      String strDistTypeExclusion = "";
      HashMap hmExclusion = new HashMap();

      hmExclusion = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("EXCLUSION_MAP"));
      strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));
      strCompExclusion = GmCommonClass.parseNull((String) hmExclusion.get("COMPANY"));

      strDistTypeExclusion = GmCommonClass.parseNull((String) hmExclusion.get("DIST_TYPE"));


      // Below code to fetch region information
      sbQuery.append(" SELECT  DISTINCT  REGION_ID ID ");
      sbQuery.append(" , REGION_NAME || ' (' || AD_NAME  || ')'NM ");
      sbQuery.append(" , VP_ID || '_' || REGION_ID VP_REGION_ID, AD_ID ADID , VP_ID VPID");
      sbQuery.append(" FROM  V700_TERRITORY_MAPPING_DETAIL V700 ");
      sbQuery
          .append(" WHERE  v700.AC_ID IS NOT NULL AND V700.DISTTYPEID NOT IN (70103,70106,70104) ");

      // Filter Condition to fetch record based on access code
      if (!strCondition.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      sbQuery.append(strCompExclusion);
      sbQuery.append(strDistTypeExclusion);

      sbQuery.append(" ORDER BY UPPER(NM)  ");
      log.debug("region sbQuery.toString() ==" + sbQuery.toString());
      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      // code added to avoid emails
      log.error("Exception in GmCommonBean:getRegions :-" + e);
      // GmLogError.log("Exception in GmCommonBean:getRegions", "Exception is:" + e);
    }
    return alResult;
  }

  /*
   * Fetch all VP level Zones
   */

  public ArrayList getZones(String strCondition) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("CONDITION", strCondition);
    return getZones(hmParam);
  }

  public ArrayList getZones(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      String strCondition = "";
      String strCompExclusion = "";
      String strDistTypeExclusion = "";
      HashMap hmExclusion = new HashMap();

      hmExclusion = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("EXCLUSION_MAP"));

      strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));
      strCompExclusion = GmCommonClass.parseNull((String) hmExclusion.get("COMPANY"));
      strDistTypeExclusion = GmCommonClass.parseNull((String) hmExclusion.get("DIST_TYPE"));


      // Below code to fetch Zone information
      sbQuery.append(" SELECT  DISTINCT  VP_ID ID ");
      sbQuery.append(" , GP_NAME || ' (' || VP_NAME  || ')'NM ");
      sbQuery.append(" FROM  V700_TERRITORY_MAPPING_DETAIL V700 ");
      sbQuery
          .append(" WHERE  v700.AC_ID IS NOT NULL AND V700.DISTTYPEID NOT IN (70103,70106,70104) ");

      // Filter Condition to fetch record based on access code
      if (!strCondition.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      sbQuery.append(strCompExclusion);
      sbQuery.append(strDistTypeExclusion);

      sbQuery.append(" ORDER BY UPPER(NM)  ");
      log.debug("zone sbQuery.toString() ==" + sbQuery.toString());
      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      // code added to avoid emails
      log.error("Exception in GmCommonBean:getZones :-" + e);
      // GmLogError.log("Exception in GmCommonBean:getRegions", "Exception is:" + e);
    }
    return alResult;
  }

  /**
   * getSalesFilterLists - This method will
   * 
   * @return HashMap
   * @exception AppError
   */

  public HashMap getSalesFilterLists(String strCondition) throws AppError {

    HashMap hmParam = new HashMap();
    hmParam.put("CONDITION", strCondition);
    return getSalesFilterLists(hmParam);
  }

  public HashMap getSalesFilterLists(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alZone = new ArrayList();
    ArrayList alRegn = new ArrayList();
    ArrayList alDist = new ArrayList();
    ArrayList alRep = new ArrayList();
    ArrayList alAcct = new ArrayList();
    ArrayList alTerr = new ArrayList();
    ArrayList alPgrp = new ArrayList();
    ArrayList alDiv = new ArrayList();
    ArrayList alComp = new ArrayList();
    ArrayList alZonesByCompDiv = new ArrayList();
    ArrayList alRegnByZone = new ArrayList();
    ArrayList alCompCurrTyp = new ArrayList();
    String strCondition = "";
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));
      String strActiveDistFl = GmCommonClass.parseNull((String) hmParam.get("ACTIVEDISTFL"));
      String strScreen = GmCommonClass.parseNull((String) hmParam.get("SCREENNAME"));

      // Below code to fetch VP Zones
      alZone = getZones(hmParam);
      hmReturn.put("ZONE", alZone);

      // Below code to fetch region information
      alRegn = getRegions(hmParam);
      hmReturn.put("REGN", alRegn);

      // Below code to fetch distributor information
      alDist =
          (strActiveDistFl.equals("Y")) ? (strScreen.equals("") ? GmCommonClass
              .parseNullArrayList(gmCustomerBean.getDistributorList("Active")) : GmCommonClass
              .parseNullArrayList(gmCustomerBean.getDistributorList(hmParam)))
              : getSalesFilterDist(hmParam);
      hmReturn.put("DIST", alDist);

      // Below code to fetch Rep information
      alRep = getSalesFilterRep(hmParam);
      hmReturn.put("REP", alRep);

      alDiv = getDivisions(hmParam);
      hmReturn.put("DIV", alDiv);

      alComp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("COMP"));
      hmReturn.put("COMP", alComp);

      alZonesByCompDiv = getZonesByCompDiv(strCondition);
      hmReturn.put("ZONEBYCOMPDIV", alZonesByCompDiv);

      alRegnByZone = getRegnByZone(strCondition);
      hmReturn.put("REGNBYZONE", alRegnByZone);

      // Below code to fetch Account information
      alAcct = getSalesFilterAcct(strCondition);
      hmReturn.put("ACCT", alAcct);
      // Below code used to fetch Currency Type details
      alCompCurrTyp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CURRTY"));
      hmReturn.put("CURRTYP", alCompCurrTyp);

      sbQuery.setLength(0);
      sbQuery.append(" SELECT ");
      sbQuery.append(" DISTINCT  TER_ID ID ");
      sbQuery.append(" ,v700.TER_NAME || ' - (' || NVL(v700.REP_NAME, '*No Rep Name')  || ')' NM ");
      sbQuery.append(" ,D_ID PID, COMPID COMPID ");
      sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700   ");
      sbQuery.append(" WHERE  v700.AC_ID IS NOT NULL ");
      // Filter Condition to fetch record based on access code
      if (!strCondition.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      sbQuery.append(" ORDER BY NM ");

      log.debug(" Territory Name " + sbQuery.toString());
      alTerr = gmDBManager.queryMultipleRecords(sbQuery.toString());

      hmReturn.put("TERR", alTerr);

      sbQuery.setLength(0);
      sbQuery.append(" SELECT   C207_SET_ID ID, C207_SET_NM NM ");
      sbQuery.append(" FROM T207_SET_MASTER WHERE C901_SET_GRP_TYPE ='1600' ");
      sbQuery.append(" and C207_VOID_FL is null ");
      sbQuery.append(" ORDER BY C207_SET_NM ");
      alPgrp = gmDBManager.queryMultipleRecords(sbQuery.toString());

      hmReturn.put("PGRP", alPgrp);

    } catch (Exception e) {
      GmLogError.log("Exception in GmCommonBean:getSalesFilterLists", "Exception is:" + e);
    }
    return hmReturn;
  } // End of getSalesFilterLists

  /**
   * getSalesFilterDist - Below code to fetch distributor information
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getSalesFilterDist(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      String strStatus = "";
      String strCondition = "";
      String strCompExclusion = "";
      String strDistTypeExclusion = "";
      String strDistFilter = "";
      String strFilterDistByComp = "";
      HashMap hmExclusion = new HashMap();

      strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
      strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));
      hmExclusion = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("EXCLUSION_MAP"));

      strCompExclusion = GmCommonClass.parseNull((String) hmExclusion.get("COMPANY"));
      strDistTypeExclusion = GmCommonClass.parseNull((String) hmExclusion.get("DIST_TYPE"));
      strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DISTFILTER"));
      strFilterDistByComp = GmCommonClass.parseNull((String) hmParam.get("FLTR_DIST_BY_COMP"));


      sbQuery.append(" SELECT   DISTINCT D_ID ID,DECODE('"
    		 + getComplangid()
    		 +"', '103097',NVL(D_NAME_EN, D_NAME), D_NAME) NM,");
            
      sbQuery.append(" REGION_ID PID, AD_ID ADID, VP_ID VPID, COMPID COMPID  ");
      sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700  ");

      if (strStatus.equals("Active")) {
        sbQuery.append(" , t701_distributor t701  ");
      }
      sbQuery.append(" WHERE  D_ID IS NOT NULL AND DISTTYPEID NOT IN (70103,70106,70104) ");
      // For other than the more filters section
      if (strFilterDistByComp.equals("YES")) {
        // Filtering Distributor based on all the companies associated with the plant.
        if (strDistFilter.equals("COMPANY-PLANT")) {
          sbQuery
              .append(" AND  D_COMPID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = "
                  + getCompPlantId() + "  AND C5041_VOID_FL IS NULL )");
        } else {
          sbQuery.append(" AND D_COMPID = '" + getCompId() + "' ");
        }
      }
      // Changed as part of Consignment Net report by Field sales. Should
      // display all the distributors irrespective of account.
      // sbQuery.append(" WHERE v700.AC_ID IS NOT NULL ");
      // Filter Condition to fetch record based on access code
      if (!strCondition.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      if (strStatus.equals("Active")) {
        sbQuery.append(" and t701.C701_DISTRIBUTOR_ID = V700.D_ID and t701.C701_ACTIVE_FL = 'Y'  ");
        sbQuery.append(" AND  (T701.c901_ext_country_id is NULL ");
        sbQuery
            .append(" OR T701.c901_ext_country_id  in (select country_id from v901_country_codes))");
      }
      sbQuery.append(strCompExclusion);
      sbQuery.append(strDistTypeExclusion);

      sbQuery.append(" ORDER BY UPPER(NM)");
      log.debug("Dist sbQuery.toString() ==" + sbQuery.toString());
      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCommonBean:getSalesFilterDist", "Exception is:" + e);
    }
    return alResult;
  } // End of getSalesFilterDist

  public ArrayList getSalesFilterDist(String strCondition, String strStatus) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("CONDITION", strCondition);
    hmParam.put("STATUS", strStatus);

    return getSalesFilterDist(hmParam);
  }

  public ArrayList getSalesFilterDist(String strCondition) throws AppError {

    return getSalesFilterDist(strCondition, "All");
  }

  /**
   * getSalesFilterRep - Below code to fetch Rep information
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getSalesFilterRep(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    String strStatus = "";
    String strCondition = "";
    String strCompExclusion = "";
    String strDistTypeExclusion = "";
    String strRepFilter = "";
    HashMap hmExclusion = new HashMap();

    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
      strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));
      hmExclusion = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("EXCLUSION_MAP"));
      strRepFilter = GmCommonClass.parseNull((String) hmParam.get("REPFILTER"));

      strCompExclusion = GmCommonClass.parseNull((String) hmExclusion.get("COMPANY"));
      strDistTypeExclusion = GmCommonClass.parseNull((String) hmExclusion.get("DIST_TYPE"));
      sbQuery.append(" SELECT   DISTINCT REP_ID ID,DECODE( '"
    				    + getComplangid()
    				    +"', '103097',NVL(REP_NAME_EN, REP_NAME), REP_NAME) NM , D_ID DID,");
      sbQuery.append("DECODE( '"
			    + getComplangid()
			    +"', '103097',NVL(D_NAME_EN, D_NAME), D_NAME) DNAME,");
      
      sbQuery.append(" REGION_ID PID, AD_ID ADID, VP_ID VPID ");
      sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700  ");
      if (strStatus.equals("Active")) {
        sbQuery.append(" , t703_sales_rep t703  ");
      }
      sbQuery.append(" WHERE  REP_ID IS NOT NULL AND DISTTYPEID NOT IN (70103,70106,70104) ");
      // Filtering Sales Rep based on all the companies associated with the plant.
      if (strRepFilter.equals("COMPANY-PLANT")) {
        sbQuery
            .append(" AND  REP_COMPID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = "
                + getCompPlantId() + "  AND C5041_VOID_FL IS NULL )");
      } else {
        sbQuery.append(" AND REP_COMPID = '" + getCompId() + "' ");
      }

      // Changed as part of Consignment Net report by Field sales. Should
      // display all the distributors irrespective of account.
      // sbQuery.append(" WHERE v700.AC_ID IS NOT NULL ");
      // Filter Condition to fetch record based on access code
      if (!strCondition.toString().equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      if (strStatus.equals("Active")) {
        sbQuery.append(" and t703.C703_SALES_REP_ID = V700.REP_ID and t703.C703_ACTIVE_FL = 'Y'  ");
        sbQuery.append(" AND  (T703.c901_ext_country_id is NULL ");
        sbQuery
            .append(" OR T703.c901_ext_country_id  in (select country_id from v901_country_codes))");
      }

      sbQuery.append(strCompExclusion);
      sbQuery.append(strDistTypeExclusion);

      sbQuery.append(" ORDER BY UPPER(NM) ");
      log.debug("Sales Rep List....." + sbQuery.toString());
      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCommonBean:getSalesFilterDist", "Exception is:" + e);
    }
    return alResult;
  } // End of getSalesFilterDist

  public ArrayList getSalesFilterRep(String strCondition, String strStatus) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("CONDITION", strCondition);
    hmParam.put("STATUS", strStatus);

    return getSalesFilterRep(hmParam);
  }

  public ArrayList getSalesFilterRep(String strCondition) throws AppError {

    return getSalesFilterRep(strCondition, "All");
  }

  /**
   * getSalesFilterDist - Below code to fetch Account information
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getSalesFilterAcct(String strCondition) throws AppError {
    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT    DISTINCT AC_ID ID, AC_NAME NM, AC_NAME NAME, AC_ID||'-'|| AC_NAME AC_FULL_INFO, AD_ID ADID, VP_ID VPID, D_ID DID ");
    sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700  ");

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" WHERE ");
      sbQuery.append(strCondition);
      sbQuery.append(" and ac_id is not null  ");
    } else {
      sbQuery.append("where ac_id is not null  ");
    }
    sbQuery.append(" ORDER BY NM");
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    log.debug(" alReturn inside getSalesFilterAcct is .................. " + sbQuery.toString());

    return alReturn;
  } // End of getSalesFilterDist

  /**
   * getDistributorList - This method will return the Distributor list
   * 
   * @param HashMap
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getDistributorList(HashMap hmParam) throws AppError {

    String strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DISTFILTER"));

    ArrayList alDistributorList = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    sbQuery
        .append(" SELECT T701.C701_DISTRIBUTOR_ID CODEID, DECODE('"
    		 + getComplangid()
    		 +"', '103097',NVL(T701.C701_DISTRIBUTOR_NAME_EN, T701.C701_DISTRIBUTOR_NAME), T701.C701_DISTRIBUTOR_NAME) CODENM,");
    sbQuery.append(" 'D'||T701.C701_DISTRIBUTOR_ID DID, DECODE( '"
    + getComplangid()
    +"', '103097',NVL(T701.C701_DISTRIBUTOR_NAME_EN, T701.C701_DISTRIBUTOR_NAME), T701.C701_DISTRIBUTOR_NAME) NAME ");
    sbQuery.append(" FROM T701_DISTRIBUTOR T701  ");
    sbQuery.append(" WHERE (T701.C901_EXT_COUNTRY_ID IS NULL ");
    sbQuery.append(" OR T701.C901_EXT_COUNTRY_ID IN (SELECT COUNTRY_ID FROM V901_COUNTRY_CODES))");
    // Filtering Distributor based on all the companies associated with the plant.
    if (strDistFilter.equals("COMPANY-PLANT")) {
      sbQuery
          .append(" AND ( T701.C1900_COMPANY_ID IN ( SELECT T5041.C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = '"
              + getCompPlantId() + "'  AND C5041_VOID_FL IS NULL )");
      sbQuery.append(" OR T701.C1900_COMPANY_ID IS NULL )  ");
      sbQuery
          .append("  AND T701.C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
      sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
          + getCompId() + "') ");

    } else {
      sbQuery.append("  AND ( T701.C1900_COMPANY_ID IS NULL OR T701.C1900_COMPANY_ID = '"
          + getCompId() + "') ");
      sbQuery
          .append("  AND T701.C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
      sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
          + getCompId() + "') ");
    }
    sbQuery.append(" AND T701.C701_VOID_FL IS NULL ORDER BY T701.C701_DISTRIBUTOR_NAME ");

    log.debug("Query to fetch distributor list " + sbQuery.toString());

    alDistributorList = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alDistributorList;
  }

  /**
   * getDistributorList - This method will return the Distributor list
   * 
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getDistributorList() throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("DISTFILTER", "");
    return getDistributorList(hmParam);
  }

  /**
   * loadEmployeeList - This method will load the Employee list and populate the same into a static
   * ArrayList
   * 
   * @return void
   * @exception AppError
   */
  public void loadEmployeeList() throws AppError {
    alStaticEmployeeList = new ArrayList();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT C101_USER_F_NAME ||' '||C101_USER_L_NAME CODENM, C101_USER_ID CODEID  ");
      sbQuery.append(" FROM T101_USER ORDER BY C101_USER_F_NAME");
      alStaticEmployeeList = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmLogonBean:getEmployeeList", "Exception is:" + e);
    }

  }

  /**
   * getEmployeeList - This method will return the Employee list
   * 
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getEmployeeList() throws AppError {
    GmLogonBean gmLogonBean = new GmLogonBean(getGmDataStoreVO());
    // To fetch the employee list based on company
    return gmLogonBean.getEmployeeList();
  }

  /**
   * loadEmployeeList - This method will load the Employee list and populate the same into a static
   * ArrayList
   * 
   * @return void
   * @exception AppError
   */
  public ArrayList loadEmployeeList(String strDeptAccess, String strDeptID) throws AppError {
    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_CM_FCH_EMPLOYEE_LIST", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull(strDeptAccess));
    gmDBManager.setString(2, GmCommonClass.parseNull(strDeptID));
    gmDBManager.execute();
    ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    log.debug("Exit");
    return alReturn;
  }

  /**
   * loadEmployeeList - This method will load the Employee list and populate the same into a static
   * ArrayList
   * 
   * @return void
   * @exception AppError
   */
  public ArrayList loadEmployeeList(String strDeptID, int strFromAccLvl, int strToAccLvl)
      throws AppError {
    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_CM_FCH_EMPLOYEE_LIST_BY_AL", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull(strDeptID));
    gmDBManager.setInt(2, strFromAccLvl);
    gmDBManager.setInt(3, strToAccLvl);
    gmDBManager.execute();
    ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    log.debug("alReturn:" + alReturn);

    gmDBManager.close();
    log.debug("Exit");
    return alReturn;
  }

  /**
   * loadClinicalStudyList - This method will load the Clinical Study list and populate the same
   * into a static ArrayList
   * 
   * @return void
   * @exception AppError
   */
  public void loadClinicalStudyList() throws AppError {
    GmStudyBean gmStudyBean = new GmStudyBean();
    alStaticClinicalStudyList = new ArrayList();
    alStaticClinicalStudyList = GmCommonClass.parseNullArrayList(gmStudyBean.reportStudyNameList());
  }

  /**
   * getClinicalStudyList - This method will return the Clinical Study list which is loaded through
   * loadClinicalStudyList
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getClinicalStudyList() {
    return alStaticClinicalStudyList;
  }

  /**
   * loadMonthList - This method will load the Months in the Year
   * 
   * @return void
   * @exception AppError
   */
  public void loadMonthList() throws AppError {
    alStaticMonthList = new ArrayList();
    alStaticMonthList = GmCommonClass.getCodeList("MONTH");
  }

  /**
   * getMonthList - This method will return the Months in a year
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getMonthList() {
    return alStaticMonthList;
  }

  /**
   * loadYearList - This method will load the Years in DB for drop down
   * 
   * @return void
   * @exception AppError
   */
  public void loadYearList() throws AppError {
    alStaticYearList = new ArrayList();
    alStaticYearList = GmCommonClass.getCodeList("YEAR");
  }

  /**
   * getYearList - This method will return the Years from DB to be used in drop down
   * 
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList getYearList() {
    return alStaticYearList;
  }

  public void saveDeleteMyTemp(GmDBManager gmDBManager) throws AppError {
    log.debug("Enter");
    gmDBManager.setPrepareString("gm_cm_sav_delete_temp_list", 0);
    gmDBManager.execute();
    log.debug("Exit");
  }

  /**
   * This method will fetch the rule value for the corresponding Rule Id and Rule Group
   * 
   * @author jkumar
   * @date Oct 28, 2008
   * @param HashMap
   */
  public String getRuleValue(HashMap hmRuleData) throws AppError {

    String strRuleId = GmCommonClass.parseNull((String) hmRuleData.get("RULEID"));
    String strRuleGroup = GmCommonClass.parseNull((String) hmRuleData.get("RULEGROUP"));
    String strRuleValue = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_rule_value", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strRuleId);
    gmDBManager.setString(3, strRuleGroup);
    gmDBManager.execute();
    strRuleValue = gmDBManager.getString(1);
    gmDBManager.close();

    return strRuleValue;
  }

  /**
   * This method will parse jobscheduler xml file
   * 
   * @param String XMLfilename
   * @param String xml root element
   * @return HashMap xml parsed data
   */
  public HashMap getScheduledJobs(String strXmlFile, String strRoot) throws SAXException,
      IOException, ParserConfigurationException {
    File file = new File(strXmlFile);
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    HashMap masterhmap = new HashMap();

    DocumentBuilder db = dbf.newDocumentBuilder();
    Document doc = db.parse(file);
    doc.getDocumentElement().normalize();

    NodeList nodeRoot = doc.getElementsByTagName(strRoot);
    for (int n = 0; n < nodeRoot.getLength(); n++) {
      Node nodeRootNode = nodeRoot.item(n);

      if (nodeRootNode.getNodeType() == Node.ELEMENT_NODE) {
        Element nodeRootNodeElem = (Element) nodeRootNode;
        // Param attributes from job scheduler

        HashMap hmGlobalParam = new HashMap();
        NodeList nodeGlbParamLst = nodeRootNodeElem.getElementsByTagName("Global-Param");

        for (int s = 0; s < nodeGlbParamLst.getLength(); s++) {
          Node fstNode = nodeGlbParamLst.item(s);

          if (fstNode.getNodeType() == Node.ELEMENT_NODE) {

            Element nodeJob = (Element) fstNode;

            {
              HashMap hmParamAttributes = new HashMap();
              ArrayList alParamList = new ArrayList();



              NodeList nodeParamElmntLst = nodeJob.getElementsByTagName("Param");


              hmGlobalParam = parseParamToHashMap(nodeParamElmntLst);
            }
          }
        }

        NodeList nodeLst = nodeRootNodeElem.getElementsByTagName("Job");

        for (int s = 0; s < nodeLst.getLength(); s++) {
          Node fstNode = nodeLst.item(s);

          if (fstNode.getNodeType() == Node.ELEMENT_NODE) {

            Element nodeJob = (Element) fstNode;

            NodeList nodeNameElmntLst = nodeJob.getElementsByTagName("Name");
            Element nodeNameElmnt = (Element) nodeNameElmntLst.item(0);
            NodeList nodeNm = nodeNameElmnt.getChildNodes();

            NodeList nodeClassElmntLst = nodeJob.getElementsByTagName("Class");
            Element nodeClassElmnt = (Element) nodeClassElmntLst.item(0);
            NodeList nodeClass = nodeClassElmnt.getChildNodes();

            HashMap hmJobAttributes = new HashMap();
            HashMap hmTriggerAttributes = null;
            ArrayList alTriggerList = new ArrayList();
            hmJobAttributes.put("Name", nodeNm.item(0).getNodeValue());
            hmJobAttributes.put("Class", nodeClass.item(0).getNodeValue());

            NodeList nodeTriggerElmntLst = nodeJob.getElementsByTagName("Trigger");

            for (int triggerCount = 0; triggerCount < nodeTriggerElmntLst.getLength(); triggerCount++) {
              Node childNode = nodeTriggerElmntLst.item(triggerCount);

              if (childNode.getNodeType() == Node.ELEMENT_NODE) {

                Element nodeTrigger = (Element) childNode;

                hmTriggerAttributes = new HashMap();
                hmTriggerAttributes.put("Name",
                    getElNodeValue(nodeTrigger.getElementsByTagName("Name")));
                hmTriggerAttributes.put("Expression",
                    getElNodeValue(nodeTrigger.getElementsByTagName("Expression")));
                alTriggerList.add(hmTriggerAttributes);
              }

            }
            hmJobAttributes.put("Trigger", alTriggerList);

            hmJobAttributes.put("HMGLOBALPARAM", hmGlobalParam);
            hmJobAttributes.put("HMPARAM",
                parseParamToHashMap(nodeJob.getElementsByTagName("Param")));

            masterhmap.put(nodeNm.item(0).getNodeValue(), hmJobAttributes);

          }
          // return the Studyhmap for all column
        }

      }
    }

    return masterhmap;


  }

  /**
   * This method will parse Parameter value into HashMap
   * 
   * @param NodeList nodeParamElmntLst
   * @return HashMap xml parsed data
   */
  public HashMap parseParamToHashMap(NodeList nodeParamElmntLst) throws AppError {
    HashMap hmParamAttributes = new HashMap();
    ArrayList alParamList = new ArrayList();

    for (int paramCount = 0; paramCount < nodeParamElmntLst.getLength(); paramCount++) {
      Node childNode = nodeParamElmntLst.item(paramCount);

      if (childNode.getNodeType() == Node.ELEMENT_NODE) {

        Element nodeParam = (Element) childNode;

        // adding all parameter values to HashMap
        hmParamAttributes.put(getElNodeValue(nodeParam.getElementsByTagName("Param-Name")),
            getElNodeValue(nodeParam.getElementsByTagName("Param-Value")));
      }
    }
    return hmParamAttributes;
  }

  /**
   * This method will get Element Node value
   * 
   * @param NodeList Element node
   * @return String Nove value
   */
  public String getElNodeValue(NodeList nodeList) throws AppError {
    String val = "";
    if (nodeList != null) {
      Element element = (Element) nodeList.item(0);
      NodeList elNodeList = element.getChildNodes();
      val = elNodeList.item(0).getNodeValue();
    }
    return val;
  }


  /**
   * This method will load the operators in DB for drop down
   * 
   * @return void
   * @exception AppError
   */
  public void loadOperatorList() throws AppError {
    alStaticOperatorList = new ArrayList();
    alStaticOperatorList = GmCommonClass.getCodeList("OPERS");
    log.debug("loaded operators = " + alStaticOperatorList);
  }

  /**
   * This method will load the operators in DB for drop down
   * 
   * @return ArrayList
   */
  public ArrayList getOperatorList() {
    return alStaticOperatorList;
  }

  public int getNoOfWorkingdays(String type) throws AppError {
    int workingDays = 0;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("get_workingdays", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, type);
    gmDBManager.execute();
    workingDays = Integer.parseInt(gmDBManager.getString(1));
    // workingDays = ((Integer)gmDBManager.getObject(1)).intValue();
    gmDBManager.close();
    return workingDays;
  }

  /**
   * 
   * @param strDeptAccess
   * @param strDeptID
   * @return
   * @throws AppError
   */
  public ArrayList getDateList() throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_common.gm_fch_date_lst", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * Add ICT Parameter fetch the values based on codeGrpID,ruleGrpID
   * 
   * @param HashMap
   * @return RowSetDynaClass
   * @throws AppError
   */

  public ArrayList fetchEditRuleParamsinfo(HashMap hmParam) throws AppError {

    String strCodeGrpID = GmCommonClass.parseNull((String) hmParam.get("CODEGRPID"));
    String strruleGrpID = GmCommonClass.parseNull((String) hmParam.get("RULEGRPID"));
    // RowSetDynaClass rsDynaResult = null;
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_rule_params.gm_fch_edit_rule_params_info", 3);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR); // Return the cursor
    gmDBManager.setString(1, strCodeGrpID);
    gmDBManager.setString(2, strruleGrpID);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * Add ICT Parameter save values based on strRuleGrpID
   * 
   * @param HashMap
   * @return void
   * @throws AppError
   */

  public void saveRuleParams(HashMap hmRuleData) throws AppError, Exception {
    String strRuleGrpID = GmCommonClass.parseNull((String) hmRuleData.get("RULEGRPID"));
    String strInputString = GmCommonClass.parseNull((String) hmRuleData.get("STRINPUTSTRING"));
    String strUserID = GmCommonClass.parseNull((String) hmRuleData.get("USERID"));
    String strCodeGrpID = GmCommonClass.parseNull((String) hmRuleData.get("CODEGRPID"));
    String strCommentsLog = GmCommonClass.parseNull((String) hmRuleData.get("TXT_LOGREASON"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCustSalesSetupBean gmCustSalesSetupBean = new GmCustSalesSetupBean(getGmDataStoreVO());
    if (strCodeGrpID.equals("SHPPM")) {
      // to save account parameters in account attribute table
      gmCustSalesSetupBean.saveAccountAttribute(gmDBManager, strRuleGrpID, strUserID,
          strInputString, "");
      // to sync the invoice parameter
      gmCustSalesSetupBean.saveSyncAccountAttr(gmDBManager, strRuleGrpID, "INVOICE_PARAM",
          strUserID);
      // save the comment log details.
      if (!strCommentsLog.equals("")) {
        saveLog(gmDBManager, strRuleGrpID, strCommentsLog, strUserID, "1290"); // 1290: Account
                                                                               // Parameter Setup
      }
    } else {
      gmDBManager.setPrepareString("gm_pkg_cm_rule_params.gm_save_ruleparams", 3);
      gmDBManager.setString(1, strRuleGrpID);
      gmDBManager.setString(2, strInputString);
      gmDBManager.setString(3, strUserID);
      gmDBManager.execute();
    }
    gmDBManager.commit();
  }

  public void saveCodeLookupValue(HashMap hmParam) throws AppError {
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_code_lookup.gm_sav_code_lookup", 2);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strUserID);
    gmDBManager.execute();
    log.debug("after================saveCodeLookupValue");
    gmDBManager.commit();
  }

  /**
   * fetchTransactionRules: To fetch the rule details from the rules table
   * 
   * @param HashMap
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchTransactionRules(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    hmReturn = fetchTransactionRules(hmParam, null);
    return hmReturn;
  }

  /**
   * fetchTransactionRules: To fetch the rule details from the rules table
   * 
   * @param HashMap, GmDataStoreVO
   * @return HashMap
   * @throws AppError
   */
  public HashMap fetchTransactionRules(HashMap hmParam, GmDataStoreVO gmDataStoreVO)
      throws AppError {

    String strRuleID = GmCommonClass.parseNull((String) hmParam.get("RULEID"));
    String strRuleGrp = GmCommonClass.parseNull((String) hmParam.get("RULEGRP"));
    String strCompanyID = "";
    if (gmDataStoreVO != null) {
      strCompanyID = GmCommonClass.parseNull(gmDataStoreVO.getCmpid());
    }

    ArrayList alReturn = new ArrayList();
    HashMap hmTemp = new HashMap();
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_rule_params.gm_fch_trans_rules", 4);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR); // Return the cursor
    gmDBManager.setString(1, strRuleID);
    gmDBManager.setString(2, strRuleGrp);
    gmDBManager.setString(4, strCompanyID);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));

    if (alReturn != null) {
      for (Iterator it = alReturn.iterator(); it.hasNext();) {
        hmTemp = (HashMap) it.next();
        hmReturn.put(hmTemp.get("RULE_ID"), hmTemp.get("RULE_VALUE"));
      }
    }
    gmDBManager.close();
    return hmReturn;
  }

  public ArrayList getActiveLoanerSets(String strType) throws AppError {
    ArrayList alLoanerList = new ArrayList();
    if (strType != null && strType.equals("4119")) {
      return alStaticActiveInHouseLoanerSets;
    } else {
      loadActiveLoanerSets();
      return alStaticActiveLoanerSets;
    }
  }

  public ArrayList getActiveLoanerSets() throws AppError {
    return getActiveLoanerSets("");
  }

  /**
   * method fetches the active loaner set list in use.
   * 
   * @return
   * @throws AppError
   */
  public void loadActiveInHouseLoanerSets() throws AppError {
    alStaticActiveInHouseLoanerSets = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_common.gm_fch_active_ih_loaner_sets", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR); // Return the cursor
    gmDBManager.execute();
    alStaticActiveInHouseLoanerSets =
        gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    // return alStaticActiveLoanerSets;
  }



  /**
   * method fetches the active loaner set list in use.
   * 
   * @return
   * @throws AppError
   */
  public void loadActiveLoanerSets() throws AppError {
    alStaticActiveLoanerSets = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_common.gm_fch_active_loaner_sets", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR); // Return the cursor
    gmDBManager.execute();
    alStaticActiveLoanerSets = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    // return alStaticActiveLoanerSets;
  }

  /**
   * method fecthes all transaction for the Process Transaction
   * 
   * @return HashMap
   * @author rjayakumar
   * @throws AppError
   */
  public HashMap fetchTransDetails(String strTransId) throws AppError {
    HashMap hmTransDetails = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_common.gm_fch_trans_details ", 2);
    gmDBManager.setString(1, strTransId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR); // Return the cursor
    gmDBManager.execute();
    hmTransDetails = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmTransDetails;
  }

  public String getGroupDesc(String strDeptID) throws AppError {
    String strGroupDesc = "";
    strDeptID = GmCommonClass.parseNull(strDeptID).equals("0") ? "" : strDeptID;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("gm_pkg_it_user.GET_GROUP_DESC", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strDeptID);
    gmDBManager.execute();
    strGroupDesc = gmDBManager.getString(1);
    gmDBManager.close();
    return strGroupDesc;
  }

  public ArrayList loadAccountList(String strCondition) throws AppError {

    ArrayList alReturn = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();

    sbQuery
        .append(" SELECT    DISTINCT AC_ID ID,AC_NAME ACNAME, AC_ID||'-'|| AC_NAME NM, AD_ID ADID ");
    sbQuery.append(" , VP_ID VPID, D_ID DID, REP_ID repid, REP_NAME rname ");
    sbQuery.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700, T704_ACCOUNT T704  ");

    // Filter Condition to fetch record based on access code
    if (!strCondition.toString().equals("")) {
      sbQuery.append(" WHERE ");
      sbQuery.append(strCondition);
      sbQuery.append(" and ac_id is not null  ");
    } else {
      sbQuery.append("where ac_id is not null  ");
    }
    sbQuery.append(" AND v700.ac_id = c704_account_id ");
    sbQuery.append(" AND c704_active_fl = 'Y' ");
    sbQuery.append(" AND (T704.c901_ext_country_id is NULL ");
    sbQuery.append(" OR T704.c901_ext_country_id  in (select country_id from v901_country_codes))");
    sbQuery.append(" ORDER BY AC_NAME ");
    log.debug(" Query to get accounts is " + sbQuery.toString());
    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
    log.debug(" alReturn inside GmCommonBean is  .................. " + sbQuery.toString());

    return alReturn;
  }


  public ArrayList getDivisions(String strCondition) throws AppError {
    HashMap hmParam = new HashMap();
    hmParam.put("CONDITION", strCondition);
    return getDivisions(hmParam);

  }

  public ArrayList getDivisions(HashMap hmParam) throws AppError {
    ArrayList alResult = new ArrayList();
    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();
      String strCondition = "";
      String strDivisonExclusion = "";
      String strCompanyExclusion = "";
      HashMap hmExclusion = new HashMap();

      hmExclusion = GmCommonClass.parseNullHashMap((HashMap) hmParam.get("EXCLUSION_MAP"));

      strCondition = GmCommonClass.parseNull((String) hmParam.get("CONDITION"));
      strDivisonExclusion = GmCommonClass.parseNull((String) hmExclusion.get("DIVISION"));
      strCompanyExclusion = GmCommonClass.parseNull((String) hmExclusion.get("COMPANY"));

      // Below code to fetch Zone information
      sbQuery.append(" SELECT  DISTINCT  DIVID ID ");
      sbQuery.append(" , DIVNAME NM, COMPID COMPID ,COMPNAME COMPNAME");
      sbQuery.append(" FROM  V700_TERRITORY_MAPPING_DETAIL V700 ");
      sbQuery.append(" WHERE V700.DISTTYPEID NOT IN (70103,70106,70104) ");

      // Filter Condition to fetch record based on access code
      if (!strCondition.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      sbQuery.append(strDivisonExclusion);
      sbQuery.append(strCompanyExclusion);
      sbQuery.append(" ORDER BY NM  DESC ");
      log.debug("Division sbQuery.toString() ==" + sbQuery.toString());
      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      // code modified to avoid emails.
      log.error("Exception in getDivisions :- " + e);
      // GmLogError.log("Exception in getDivisions", "Exception is:" + e);
    }
    return alResult;
  }


  public ArrayList getCompanies(String strCondition) throws AppError {
    ArrayList alResult = new ArrayList();
    strCondition = GmCommonClass.parseNull(strCondition);
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();

      // Below code to fetch Zone information
      sbQuery.append(" SELECT  DISTINCT  COMPID CODEID ");
      sbQuery.append(" , COMPNAME CODENM ");
      sbQuery.append(" FROM  V700_TERRITORY_MAPPING_DETAIL V700 ");

      // Filter Condition to fetch record based on access code
      if (!strCondition.equals("")) {
        sbQuery.append(" WHERE ");
        sbQuery.append(strCondition);
      }
      sbQuery.append(" ORDER BY CODENM  ");
      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
      log.debug("companies sbQuery.toString() ==" + sbQuery.toString());
    } catch (Exception e) {
      // code modified to avoid emails.
      log.error("Exception in getCompanies :- " + e);
      // GmLogError.log("Exception in getCompanies", "Exception is:" + e);
    }
    return alResult;
  }

  /*
   * Fetch all VP level Zones
   */
  public ArrayList getZonesByCompDiv(String strCondition) throws AppError {
    ArrayList alResult = new ArrayList();
    strCondition = GmCommonClass.parseNull(strCondition);
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();

      // Below code to fetch Zone information
      sbQuery.append(" SELECT  DISTINCT  GP_ID ID ");
      sbQuery.append(" , GP_NAME || ' (' || VP_NAME  || ')'NM ");
      sbQuery.append(" , DIVID COMPDIVID, COMPID ");
      sbQuery.append(" FROM  V700_TERRITORY_MAPPING_DETAIL V700 ");
      sbQuery.append(" WHERE V700.DISTTYPEID NOT IN (70103,70106,70104) ");

      // Filter Condition to fetch record based on access code
      if (!strCondition.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      sbQuery.append(" ORDER BY NM ");
      log.debug("zone by company div sbQuery.toString() ==" + sbQuery.toString());
      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      // code modified to avoid emails.
      log.error("Exception in getZonesByCompDiv :- " + e);
      // GmLogError.log("Exception in getZonesByCompDiv", "Exception is:" + e);
    }
    return alResult;
  }

  public ArrayList getRegnByZone(String strCondition) throws AppError {
    ArrayList alResult = new ArrayList();
    strCondition = GmCommonClass.parseNull(strCondition);
    try {

      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      StringBuffer sbQuery = new StringBuffer();

      // Below code to fetch Zone information
      sbQuery.append(" SELECT  DISTINCT  REGION_ID ID ");
      sbQuery
          .append(" , REGION_NAME || ' (' || AD_NAME  || ')'NM, GP_ID ZONEID, DIVID COMPDIVID, COMPID COMPID");
      sbQuery.append(" FROM  V700_TERRITORY_MAPPING_DETAIL V700 ");
      sbQuery.append(" WHERE  V700.DISTTYPEID NOT IN (70103,70106,70104) ");

      // Filter Condition to fetch record based on access code
      if (!strCondition.equals("")) {
        sbQuery.append(" AND ");
        sbQuery.append(strCondition);
      }
      sbQuery.append(" ORDER BY NM ");
      log.debug("Region by zone sbQuery.toString() ==" + sbQuery.toString());
      alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
    } catch (Exception e) {
      // code modified to avoid emails.
      log.error("Exception in getRegnByZone :- " + e);
      // GmLogError.log("Exception in getRegnByZone", "Exception is:" + e);
    }
    return alResult;
  }

  /**
   * getSalesFilterLists - This method will
   * 
   * @return HashMap
   * @exception AppError
   */

  /**
   * method isWhitelistedServer - will set the Server and System info.
   * 
   * @return
   * @throws Exception
   */
  public void isWhitelistedServer() throws Exception {
    InetAddress iadd = InetAddress.getLocalHost();
    String strSystemNm = iadd.toString().toLowerCase();
    String[] SystName = new String[2];
    SystName = strSystemNm.split("/");
    strSystemNm = SystName[0];
    String strServer =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strSystemNm, "WHITELISTSERVER"));
    if (strServer.equals("TRUE")) {
      GmCommonClass.blWhitelistserver = true;
      log.debug("This is a White Listed Server and email id's will not be filtered");
    } else {
      GmCommonClass.blWhitelistserver = false;
      String toEmail = GmCommonClass.parseNull(GmCommonClass.getRuleValue(strSystemNm, "SYSTEM"));
      GmCommonClass.toEmail = toEmail;
      log.debug("This is a local server and all the emails will be forwaded to " + toEmail);
    }
  }

  /**
   * method isTestingServer - will set the Server and System info.
   * 
   * @return
   * @throws Exception
   */

  public void isTestingServer() throws Exception {
    InetAddress iadd = InetAddress.getLocalHost();
    String strSystemNm = iadd.toString().toLowerCase();
    String[] SystName = new String[2];
    SystName = strSystemNm.split("/");
    strSystemNm = SystName[0];
    String strServer =
        GmCommonClass.parseNull(GmCommonClass.getRuleValue(strSystemNm, "TESTINGSERVER"));

    if (strServer.equals("TRUE")) {
      GmCommonClass.blTestingserver = true;
      GmCommonClass.strTestEmailSubject =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strSystemNm, "EMAILSUBJECT"));
      GmCommonClass.strTestEmailID =
          GmCommonClass.parseNull(GmCommonClass.getRuleValue(strSystemNm, "TESTEMAIL"));
      log.debug("This is a testing server and all the emails that contains subject "
          + GmCommonClass.strTestEmailSubject + " will be forwaded to "
          + GmCommonClass.strTestEmailID);
    } else {
      GmCommonClass.blTestingserver = false;
    }
  }

  public ArrayList loadEmployeeListWithUserGroup(String strDeptID, int strFromAccLvl,
      int strToAccLvl) throws AppError {
    log.debug("Enter");
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_CM_FCH_EMPLOYEE_LIST_GRP", 4);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull(strDeptID));
    gmDBManager.setInt(2, strFromAccLvl);
    gmDBManager.setInt(3, strToAccLvl);
    gmDBManager.execute();
    ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    log.debug("alReturn:" + alReturn);
    gmDBManager.close();
    log.debug("Exit");
    return alReturn;
  }

  /**
   * fetchSystemRules - This method - will fetch the AD system rule value from T906b
   * 
   * @return HashMap - System rule values for AD configuration
   * @param String - parameter system groupID
   * @exception AppError
   */
  public HashMap fetchSystemRules(String strGroupID, String strLdapId) throws AppError {
    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmLoop = new HashMap();
    HashMap hmLdapDtls = new HashMap();
    ArrayList alLdapDtls = new ArrayList();
    HashMap hmTemp = new HashMap();

    String strKey = "";
    String strValue = "";
    GmLdapBean gmLdapBean = new GmLdapBean(getGmDataStoreVO());
    alLdapDtls = GmCommonClass.parseNullArrayList(gmLdapBean.fetchLdapDetails(strLdapId, "Y"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    // to get the LDAP master data

    for (int k = 0; k < alLdapDtls.size(); k++) {
      hmLdapDtls = new HashMap();
      hmLoop = (HashMap) alLdapDtls.get(k);
      strLdapId = GmCommonClass.parseNull((String) hmLoop.get("ID"));
      gmDBManager.setPrepareString("GM_PKG_CM_RULE_PARAMS.GM_FCH_SYSTEM_RULES", 3);
      gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
      gmDBManager.setString(1, strGroupID);
      gmDBManager.setString(2, strLdapId);
      gmDBManager.execute();
      alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
      for (int i = 0; i < alReturn.size(); i++) {
        hmLoop = (HashMap) alReturn.get(i);
        strKey = GmCommonClass.parseNull((String) hmLoop.get("KEY"));
        strValue = GmCommonClass.parseNull((String) hmLoop.get("VALUE"));
        hmLdapDtls.put(strKey, strValue);
      }
      hmReturn.put(strLdapId, hmLdapDtls);
    }
    gmDBManager.close();

    log.debug(" final out put " + hmReturn);

    return hmReturn;
  }


  /**
   * saveSystemRules - This method w save / update the rule value in T906b
   * 
   * @param hmParam - parameters to be saved / updated
   * @exception AppError
   */
  public void saveSystemRules(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmSecurityBean gmSecurityBean = new GmSecurityBean(getGmDataStoreVO());

    String strName = GmCommonClass.parseNull((String) hmParam.get("NAME"));
    String strUserName = GmCommonClass.parseNull((String) hmParam.get("USERNAME"));
    String strPassword = GmCommonClass.parseNull((String) hmParam.get("PASSWORD"));
    String strHostName = GmCommonClass.parseNull((String) hmParam.get("HOSTNAME"));
    String strPort = GmCommonClass.parseNull((String) hmParam.get("PORT"));
    String strUserDN = GmCommonClass.parseNull((String) hmParam.get("USERDN"));
    String strBase = GmCommonClass.parseNull((String) hmParam.get("BASE"));
    String strLdapId = GmCommonClass.parseNull((String) hmParam.get("LDAPID"));
    String strGrpId = "LDAPCRD";
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    strPassword = gmSecurityBean.encrypt(strPassword, strUserName);

    gmDBManager.setPrepareString("GM_PKG_CM_RULE_PARAMS.GM_SAV_LDAPCRD", 10);

    gmDBManager.setString(1, strName);
    gmDBManager.setString(2, strUserName);
    gmDBManager.setString(3, strPassword);
    gmDBManager.setString(4, strHostName);
    gmDBManager.setString(5, strPort);
    gmDBManager.setString(6, strUserDN);
    gmDBManager.setString(7, strBase);
    gmDBManager.setString(8, strLdapId);
    gmDBManager.setString(9, strGrpId);
    gmDBManager.setString(10, strUserId);
    gmDBManager.execute();


    gmDBManager.commit();

  }

  /**
   * testConnection - This method will test connect the Active Directory
   * 
   * @return String - message will be success or error
   * @param hmParam - parameters to be test connection to Active Directory
   * @exception AppError
   */
  public String testLDAPConnection(HashMap hmParam) throws AppError {
    String strMessage = "";
    String strLdapId = GmCommonClass.parseNull((String) hmParam.get("LDAPID"));
    try {
      GmLDAPManager gmLDAPManager = new GmLDAPManager(hmParam);
      String strUserName = GmCommonClass.parseNull((String) hmParam.get("USERNAME"));
      String strPassword = GmCommonClass.parseNull((String) hmParam.get("PASSWORD"));
      gmLDAPManager.authenticateUser(strUserName, strPassword);
      strMessage = "success";
    } catch (Exception e) {
      e.printStackTrace();
      strMessage = e.getMessage();
    }
    return strMessage;
  }

  /**
   * fetchLdapUser - This method will get the system rule values from T906b table and store in
   * static HashMap
   * 
   * @return
   * @param
   * @exception AppError
   */
  public void fetchLdapUser() throws AppError {
    GmCommonClass.hmLdapCrd = GmCommonClass.parseNullHashMap(fetchSystemRules("LDAPCRD", ""));
    log.debug("AD System Rule values are loaded.");
  }


  /**
   * setCountryCode - This method will get the country code from constants property file and set the
   * value is static variable.
   * 
   * @return
   * @param
   * @exception AppError
   */
  public void setCountryCode() throws AppError {
    GmCommonClass.countryCode = GmCommonClass.parseNull(GmCommonClass.getString("COUNTRYCODE"));
  }
  
  /**
   * Set context URL - This method will load the context URL for image load in jasper
   * 
   * @return void
   * @exception AppError
   */
  public void setContextURL() throws AppError {
    
	  GmCommonClass.contextURL = GmCommonClass.parseNull(GmCommonClass.getString("CONTEXTURL"));
  }
  /**
   * Set jasper URL - This method will load the server image path for image load in jasper
   * 
   * @return void
   * @exception AppError
   */
  public void setJasperImageURL() throws AppError {
    
	  GmCommonClass.jasperImageURL = System.getProperty("ENV_JASPERIMAGEURL");
  }
  /**
   * Set db connection time out from constant.properties
   * 
   * @return void
   * @exception AppError
   */
  public void setDbConnectionTimeOut() throws AppError {
  GmCommonClass.dbConnectionTimeOut = Integer.parseInt(GmCommonClass.parseZero(GmCommonClass.getString("DBCONNECTIONTIMEOUT")));
  }
  
  
  
  /**
   * Set db connection time out for GOP from constant.properties
   * 
   * @return void
   * @exception AppError
   */
  public void setDbConnectionTimeOutEXT() throws AppError {
  GmCommonClass.dbConnectionTimeOutEXT = Integer.parseInt(GmCommonClass.parseZero(GmCommonClass.getString("DBCONNECTIONTIMEOUTEXT")));
  }

  /**
   * fetchUDIRuleParams - This method will return the UDI Rule Parameters.
   * 
   * @return ArrayList
   * @param HashMap
   * @exception AppError
   */
  public ArrayList fetchUDIRuleParams(HashMap hmParams) throws AppError {
    String strCodeGrpID = "";
    strCodeGrpID = GmCommonClass.parseNull((String) hmParams.get("CODEGRPID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_rule_params.gm_fch_rules", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCodeGrpID);
    gmDBManager.execute();
    ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * fetchCommonLogDetails - This method will return the Common Log details for the input ref id &
   * type.
   * 
   * @return ArrayList
   * @param HashMap
   * @exception AppError
   */
  public ArrayList fetchCommonLogDetails(HashMap hmParams) throws AppError {
    String strTxnID = GmCommonClass.parseNull((String) hmParams.get("TXNIDS"));
    String strTxnType = GmCommonClass.parseNull((String) hmParams.get("TXNTYPE"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pd_audit_price_log.gm_fch_common_log_details", 3);
    gmDBManager.setString(1, strTxnID);
    gmDBManager.setString(2, strTxnType);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();

    ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return alReturn;
  }

  /**
   * Fetch User Name Based On the User ID
   * 
   * @param String strUserID
   * @return strUserName
   * @throws AppError the app error
   */
  public String getUserName(String strUserId) throws AppError {
    String strUserName = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setFunctionString("GET_USER_NAME", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strUserId);
    gmDBManager.execute();
    strUserName = gmDBManager.getString(1);
    gmDBManager.close();
    return strUserName;
  }

  /**
   * This method returns the static company list
   * 
   * @param
   * @return ArrayList
   * @throws AppError the app error
   */
  public ArrayList getCompanyList() throws AppError {
    return alCompany;
  }

  /**
   * This method load the static company list & map
   * 
   * @param
   * @return
   * @throws AppError the app error
   */
  public void loadCompanyInfo() throws AppError {
    alCompany = new ArrayList();
    hmCompany = new HashMap<String, GmCompanyVO>();
    GmWSUtil gmWSUtil = new GmWSUtil();

    alCompany = new GmCompanyBean(getGmDataStoreVO()).loadCompanyInfo(null);

    // populate Company static HashMap with VO
    Iterator itrHashMap = alCompany.iterator();

    while (itrHashMap.hasNext()) {
      HashMap hmMap = (HashMap) itrHashMap.next();
      GmCompanyVO gmCompanyVO = new GmCompanyVO();
      gmCompanyVO =
          (GmCompanyVO) gmWSUtil.getVOFromHashMap(hmMap, gmCompanyVO,
              gmCompanyVO.getCompanyProperties());

      hmCompany.put(gmCompanyVO.getCompanyid(), gmCompanyVO);
    }

  }

  /**
   * This method returns CompanyVO from static map
   * 
   * @param String company id
   * @return GmCompanyVO
   * @throws AppError the app error
   */
  public GmCompanyVO getCompanyVO(String strCompanyId) {
    return hmCompany.get(strCompanyId);
  }

  /**
   * This method returns plant list
   * 
   * @param
   * @return ArrayList
   * @throws AppError the app error
   */
  public ArrayList getPlantList() throws AppError {
    return alPlant;
  }

  /**
   * This method load static plant list and map
   * 
   * @param
   * @return
   * @throws AppError the app error
   */
  public void loadPlantInfo() throws AppError {
    alPlant = new ArrayList();
    hmPlant = new HashMap<String, GmPlantVO>();
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_plant_rpt.gm_fch_plant_info", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alPlant = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();

    // populate Plant static HashMap with VO
    Iterator itrHashMap = alPlant.iterator();

    while (itrHashMap.hasNext()) {
      HashMap hmMap = (HashMap) itrHashMap.next();
      GmPlantVO gmPlantVO = new GmPlantVO();
      gmPlantVO =
          (GmPlantVO) gmWSUtil.getVOFromHashMap(hmMap, gmPlantVO, gmPlantVO.getPlantProperties());

      hmPlant.put(gmPlantVO.getPlantid(), gmPlantVO);
    }

  }

  /**
   * This method load plant company mapping info
   * 
   * @param
   * @return
   * @throws AppError the app error
   */
  public ArrayList loadPlantCompanyMapping() throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_plant_rpt.gm_fch_plant_comp_mapping", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alResult;
  }


  /**
   * This method populate plant company static map
   * 
   * @param
   * @return
   * @throws AppError the app error
   */
  public void populatePlantCompanyStaticMap() throws AppError {
    String strCompId = "";
    String strNxtCompId = "";
    HashMap hmLoop = null;
    HashMap hmTempLoop = null;
    ArrayList alTemp = new ArrayList();
    int intDataSize;
    ArrayList alPlantComp = loadPlantCompanyMapping();
    hmPlantComp = new HashMap<String, ArrayList>();
    intDataSize = alPlantComp.size();

    for (int i = 0; i < intDataSize; i++) {
      hmLoop = new HashMap();
      hmLoop = (HashMap) alPlantComp.get(i);
      strCompId = GmCommonClass.parseNull((String) hmLoop.get("COMPID"));

      if (i < intDataSize - 1) {
        hmTempLoop = (HashMap) alPlantComp.get(i + 1);
        strNxtCompId = GmCommonClass.parseNull((String) hmTempLoop.get("COMPID"));
      }

      alTemp.add(hmLoop);

      if (!strCompId.equals(strNxtCompId) || i == intDataSize - 1) {
        log.debug("strCompId=" + strCompId + " plant list =" + alTemp);
        hmPlantComp.put(strCompId, alTemp);
        alTemp = new ArrayList();
      }

    }
  }

  /**
   * This method returns list of plant for the company
   * 
   * @param String company id
   * @return ArrayList
   * @throws AppError the app error
   */
  public ArrayList getPlantCompanyList() throws AppError {
    String strCompId = getGmDataStoreVO().getCmpid();
    ArrayList alPlantComp = new ArrayList();
    if (!strCompId.equals("")) {
      alPlantComp = hmPlantComp.get(strCompId);
    }
    return alPlantComp;

  }

  /**
   * This method get the parent company list details
   * 
   * @param
   * @return ArrayList - Contains the details of Parent company list.
   * @throws AppError the app error
   */
  public ArrayList getParentCompanyList() throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cor_company_rpt.gm_fch_parent_company_info", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alResult;
  }

  /**
   * @method fetchPlantList - To Fetch the Available Plant List for the Selected Company.
   * @return HashMap
   * @param String strCompanyID
   * @throws AppError
   */
  public HashMap fetchCompanyPlantList(String strCompanyID) throws AppError {
    ArrayList alResult = new ArrayList();
    HashMap hmReturn = new HashMap();
    String strDefaultPlant = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ac_tag_info.gm_fch_plant_dtls", 3);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strCompanyID);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    strDefaultPlant = GmCommonClass.parseNull(gmDBManager.getString(3));
    hmReturn.put("PLANTLIST", alResult);
    hmReturn.put("DEFAULTPLANT", strDefaultPlant);
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * This method fetchGroupMappingDetails will fetch the list of users who mapped with the given
   * groups.
   * 
   * @return ArrayList
   * @param String strGroupID - group name to which the users are mapped.
   * @throws AppError
   */
  public ArrayList fetchGroupMappingDetails(String strGroupID) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_cm_fch_grp_map_details", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull(strGroupID));
    gmDBManager.execute();
    ArrayList alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return alReturn;
  }

  /**
   * This method getCurrencySymbolByRegn used to fetch the currency symbols for regions.
   * 
   * @return ArrayList
   * @param HashMap
   * @throws AppError
   */
  public ArrayList getCurrencySymbolByRegn(HashMap hmParam) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    StringBuffer sbQuery = new StringBuffer();
    ArrayList alCurrList = new ArrayList();

    String strRegion = GmCommonClass.parseNull((String) hmParam.get("HREGN_FILTER"));

    sbQuery.append(" SELECT DISTINCT T901.C901_CODE_NM CURRSMB ");
    sbQuery.append(" FROM T1900_COMPANY T1900,T901_CODE_LOOKUP T901 ");
    sbQuery.append(" WHERE T1900.C1900_COMPANY_ID IN ( ");
    sbQuery.append(" SELECT C1900_COMPANY_ID COMPANYID ");
    sbQuery.append(" FROM T707_REGION_COMPANY_MAPPING ");
    if (!strRegion.equals("")) {

      sbQuery.append(" WHERE C901_REGION_ID IN  (" + strRegion + ") ");
      sbQuery.append(" AND C707_VOID_FL IS NULL ");
    }
    sbQuery.append(" ) AND T901.C901_CODE_ID    = T1900.C901_TXN_CURRENCY ");
    sbQuery.append(" AND T901.C901_VOID_FL IS NULL");
    sbQuery.append(" AND C1900_VOID_FL IS NULL  ");

    log.debug("Currency from region Query:" + sbQuery.toString());
    alCurrList = gmDBManager.queryMultipleRecords(sbQuery.toString());

    return alCurrList;
  }
  
  /**
   * This method is used to get the Plant Name Based on Plant Id
   * 
   * @param String strPlantID
   * @return strPlantName
   * @throws AppError the app error
   */
  public String getPlantName(String strPlantId) throws AppError {
	    String strPlantName = "";
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setFunctionString("get_plant_name", 1);
	    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
	    gmDBManager.setString(2, strPlantId);
	    gmDBManager.execute();
	    strPlantName = gmDBManager.getString(1);
	    gmDBManager.close();
	    return strPlantName;
  }
  
  /**
   * Set shipping Print Service call while initializing the server
   * 
   * @return void
   * @exception AppError
   */
  public void setShippingPrintService() throws AppError {
	  GmCommonClass.shipPrintThruJMS = GmCommonClass.parseNull(GmCommonClass.getString("SHIP_PRINT_THRU_JMS"));
	  log.error("GmCommonClass.shipPrintThruJMS=="+GmCommonClass.shipPrintThruJMS);
  }
  
  
} // End of GmCommonBean
