/**
 * 
 */
package com.globus.common.beans;

import org.apache.log4j.Logger;

import com.windowsazure.messaging.AppleRegistration;
import com.windowsazure.messaging.Notification;
import com.windowsazure.messaging.NotificationHub;
import com.windowsazure.messaging.Registration;


/**
 * @author arajan
 * 
 */
public class GmNotificationManager {
  Logger log = GmLogger.getInstance("notification");

  public void sendNotification(String strDeviceTokenStr, String strMessage) {

    strDeviceTokenStr = GmCommonClass.parseNull(strDeviceTokenStr);
    // Get the connection string and hub name from property file
    String strConnectString = GmCommonClass.getString("HUB_CONNECTION_STR");
    String strHubName = GmCommonClass.getString("NOTIFICATION_HUBNAME");
    NotificationHub notificationHub = new NotificationHub(strConnectString, strHubName);
    String strRegId = "";
    String strDeviceToken = "";
    // FutureCallback<Registration> regCallback = null;
    // FutureCallback<NotificationOutcome> notificaltionCallback = null;

    // Get the token separately from the string to loop it to send the notification to each device
    // based on token
    String[] strTokenArr = strDeviceTokenStr.trim().split(",");
    // Create registrations using create registrationid + upsert pattern (removes duplicates
    // deriving from lost responses if registration ids are stored on the device):

    try {
      strRegId = notificationHub.createRegistrationId();
      Registration appleReg = null;
      Notification notification = null;

      for (int i = 0; i < strTokenArr.length; i++) {
        strDeviceToken = GmCommonClass.parseNull(strTokenArr[i]);
        if (!strDeviceToken.trim().equals("")) {
          appleReg = new AppleRegistration(strRegId, strDeviceToken);
          notificationHub.upsertRegistration(appleReg);

          // Create the notification payload
          notification =
              Notification.createAppleNotifiation("{\"aps\":{\"alert\":\"" + strMessage + "\"}}");

          // Send the notification to particular device
          notificationHub.sendDirectNotification(notification, strDeviceToken);
          log.debug("Notification sent successfully to " + strDeviceToken + " with message "
              + strMessage);
        } else {
          log.error("Device token is not available. The Message needs to be sent is: " + strMessage);
        }
      }
    } catch (Exception e) { // TODO Auto-generated catch block
      e.printStackTrace();
      log.error("Error in Notification: " + e.getMessage());
      log.debug("Notification Message is:" + strMessage);
    }
  }
}
