package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.operations.forecast.beans.GmDemandSetupBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmAutoCompleteReportBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public static final String strAutoSearchDelimiter = GmCommonClass.parseNull(GmCommonClass
      .getRuleValue("DELIMITER", "AUTO_COMPLETE")); // 192.168.9.67

  /**
   * @param gmDataStoreVO
   * @throws AppError
   */

  public GmAutoCompleteReportBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super(gmDataStoreVO);
    // TODO Auto-generated constructor stub
  }

  /**
   * loadFieldSalesList - This method to fetch the field sales information based on user search
   * 
   * @param strKey
   * @param searchPrefix
   * @param iMaxCount
   * @return
   * @throws AppError
   */
  public String loadFieldSalesList(String strKey, String searchPrefix, int iMaxCount)
      throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    log.debug(" Key values is " + strKey);
    log.debug(" searchPrefix values is " + searchPrefix);
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    String strReturn = gmCacheManager.returnJsonString(getDistributorQueryStr(new HashMap()));

    return strReturn;
  } // End of loadSetMap

  public String getDistributorQueryStr(HashMap hmParam) throws AppError {
    String strFilter = GmCommonClass.parseNull((String) hmParam.get("FILTER"));
    String strDistTyp = GmCommonClass.parseNull((String) hmParam.get("DISTTYP"));
    String strShipTyp = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DISTFILTER"));

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT  C701_DISTRIBUTOR_ID ID,DECODE('"
            + getComplangid()
            + "', '103097',NVL(C701_DISTRIBUTOR_NAME_EN, C701_DISTRIBUTOR_NAME), C701_DISTRIBUTOR_NAME) NAME, ");
    sbQuery
        .append(" LOWER (DECODE('"
            + getComplangid()
            + "', '103097',NVL(C701_DISTRIBUTOR_NAME_EN, C701_DISTRIBUTOR_NAME), C701_DISTRIBUTOR_NAME)) distname ");
    sbQuery.append(" FROM T701_DISTRIBUTOR ");
    // Instead of function we are getting the division id from master table
    sbQuery.append(" ,(SELECT DISTINCT t710_tmp.c901_area_id, t710_tmp.c901_division_id ");
    sbQuery.append(" FROM t710_sales_hierarchy t710_tmp ");
    sbQuery.append(" WHERE t710_tmp.c710_active_fl = 'Y' ");
    sbQuery.append(" AND t710_tmp.c710_void_fl IS NULL) t710 ");
    sbQuery.append(" WHERE C701_VOID_FL IS NULL AND (c901_ext_country_id is NULL ");
    sbQuery.append(" OR c901_ext_country_id  in (select country_id from v901_country_codes))");
    if (strFilter.equals("Active")) {
      sbQuery.append(" AND ");
      sbQuery.append(" C701_ACTIVE_FL = 'Y'");
    }
    if (!strDistTyp.equals("")) {
      sbQuery.append(" AND c901_distributor_type = ");
      sbQuery.append(strDistTyp);
    }
    if (!strShipTyp.equals("")) {
      sbQuery.append(" AND C701_DISTRIBUTOR_ID in (decode(" + strShipTyp + ",4120," + strRepID
          + ",(SELECT C701_DISTRIBUTOR_ID FROM T101_USER WHERE C101_USER_ID ='" + strRepID
          + "'))) ");

    }
    sbQuery.append(" AND c701_region = t710.c901_area_id (+) ");
    // Filtering Distributor based on all the companies associated with the plant.
    if (strDistFilter.equals("COMPANY-PLANT")) {
      sbQuery
          .append(" AND ( C1900_COMPANY_ID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = '"
              + getCompPlantId() + "'  AND C5041_VOID_FL IS NULL )");
      sbQuery.append(" OR C1900_COMPANY_ID IS NULL )  ");
      sbQuery
          .append("  AND C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
      sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
          + getCompId() + "') ");

    } else {
      sbQuery.append("  AND ( C1900_COMPANY_ID IS NULL OR C1900_COMPANY_ID = '" + getCompId()
          + "') ");
      sbQuery
          .append("  AND C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
      sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
          + getCompId() + "') ");
    }
    sbQuery.append(" ORDER BY distname ");
    log.debug(" Working changes " + sbQuery.toString());
    return sbQuery.toString();
  }


  /**
   * loadSalesRepList - This method to fetch the sales rep setup information based on user search
   * 
   * @param strKey
   * @param searchPrefix
   * @param iMaxCount
   * @return
   * @throws AppError
   */
  public String loadSalesRepList(String strKey, String searchPrefix, int iMaxCount, HashMap hmParam)
      throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
    String strReturn = gmCacheManager.returnJsonString(getSalesRepQueryStr(new HashMap()));
    return strReturn;
  }

  public String getSalesRepQueryStr(HashMap hmParam) throws AppError {

    log.debug(" hmParam ::: >>>" + hmParam);

    ArrayList alReturn = new ArrayList();
    String strFilter = GmCommonClass.parseNull((String) hmParam.get("FILTER"));
    String strShipTyp = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
    String strDivID = GmCommonClass.parseNull((String) hmParam.get("DIVID"));
    String strAccountSrc = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTSRC"));
    String strDirectRepType = GmCommonClass.parseNull((String) hmParam.get("DIRECTREPTYPE"));
    String strRepFilter = GmCommonClass.parseNull((String) hmParam.get("REPFILTER"));
    String strCompanyID = GmCommonClass.parseNull((String) hmParam.get("COMPANY_ID"));

    /*
     * When Company ID is passed in along with the HashMap then we have to filter the Rep based on
     * the passed in company . Use the Company from Context only when the HashMap does not have the
     * Company ID.
     */
    strCompanyID = strCompanyID.equals("") ? getCompId() : strCompanyID;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT C703_SALES_REP_ID REPID,C703_SALES_REP_ID ID, 'S' || C703_SALES_REP_ID SID, t701.C701_DISTRIBUTOR_ID DID,  DECODE('"
            + getComplangid()
            + "' , '103097',NVL(C703_SALES_REP_NAME_EN, C703_SALES_REP_NAME), C703_SALES_REP_NAME) NAME,  ");
    sbQuery.append(" t701.c701_distributor_name DNAME ");
    sbQuery
        .append(" , T101.C101_PARTY_ID  PARTYID ,  T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM PARTYNAME  "); // Party
    sbQuery
        .append(" , lower( DECODE ('"
            + getComplangid()
            + "', '103097', NVL (C703_SALES_REP_NAME_EN, C703_SALES_REP_NAME),  C703_SALES_REP_NAME)) repname ");
    sbQuery.append(" FROM T703_SALES_REP  T703 , T101_PARTY T101, t701_distributor t701 ");
    sbQuery.append(" WHERE  T703.C101_PARTY_ID = T101.C101_PARTY_ID  AND  C703_VOID_FL IS NULL ");
    sbQuery.append(" AND t703.c701_distributor_id = t701.c701_distributor_id ");
    sbQuery.append(" AND t701.c701_void_fl IS NULL ");
    if (strDivID.equals("")) {
      // Filtering Distributor based on all the companies associated with the plant.
      if (strRepFilter.equals("COMPANY-PLANT")) {
        sbQuery
            .append(" AND  T703.C1900_COMPANY_ID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = "
                + getCompPlantId() + "  AND C5041_VOID_FL IS NULL )");
      } else {
        sbQuery.append(" AND  T703.C1900_COMPANY_ID = " + strCompanyID);
      }
    } else {
      if (!strDivID.equals("")) {
        sbQuery.append(" AND T703.c901_ext_country_id is NOT NULL ");
      } else {
        sbQuery.append(" AND (T703.c901_ext_country_id is NULL ");
        sbQuery
            .append(" OR T703.c901_ext_country_id  in (select country_id from v901_country_codes))");
      }
    }
    if (strFilter.equals("Active")) {
      sbQuery.append(" AND ");
      sbQuery.append(" C703_ACTIVE_FL = 'Y' ");
    }
    if (!strShipTyp.equals("")) {
      sbQuery.append(" AND t701.C701_DISTRIBUTOR_ID in (decode(" + strShipTyp + ",4120," + strRepID
          + ",(SELECT C701_DISTRIBUTOR_ID FROM T101_USER WHERE C101_USER_ID ='" + strRepID
          + "'))) ");

    }
    // If Source dropdown is showing in UI, loading Direct SalesReps in Dropdown
    if (!strDirectRepType.equals("")) {
      sbQuery.append(" AND t701.c901_distributor_type IN(" + strDirectRepType + ")");
    }

    sbQuery.append(" ORDER BY repname ");
    log.debug(" Query to fetch Rep Details is GmAutoCompleteBean >>>>  " + sbQuery.toString());
    return sbQuery.toString();
  }



  /**
   * getCodeList - This method returns a list of code names for code group based on company
   * 
   * @param strCodeGrp - Code group name
   * @param strDeptId - strDeptId
   * @param strCompanyId - company id
   * @exception AppError
   * @return ArrayList
   */

  public ArrayList getCodeList(String strCodeGrp, GmDataStoreVO gmDataStoreVO) throws AppError {

    ArrayList arList = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    String strCompanyId = "";
    if (gmDataStoreVO != null) {
      strCompanyId = GmCommonClass.parseNull(gmDataStoreVO.getCmpid());
    }
    GmCacheManager gmCacheManager = new GmCacheManager();
    String strCodeGrpParsed = GmCommonClass.getStringWithQuotes(strCodeGrp);

    sbQuery.append("SELECT  T901.C901_CODE_ID CODEID, T901.C901_CODE_NM CODENM ");
    sbQuery
        .append(",T901.C902_CODE_NM_ALT CODENMALT, T901.C901_CONTROL_TYPE CONTROLTYP,T901.C901_CODE_SEQ_NO CODESEQNO");
    sbQuery.append(" FROM  T901_CODE_LOOKUP T901 ");
    sbQuery.append(" WHERE  T901.C901_CODE_GRP IN ('" + strCodeGrpParsed + "')");
    sbQuery.append(" AND T901.C901_ACTIVE_FL = '1' ");

    sbQuery.append(" AND C901_VOID_FL          IS NULL ");

    // Below Query will filter the Code ID that are in the Exclusion for the Company.
    if (!strCompanyId.equals("")) {
      sbQuery.append(" AND T901.C901_CODE_ID NOT IN ");
      sbQuery.append(" ( ");
      sbQuery.append(" SELECT T901D.C901_CODE_ID ");
      sbQuery.append(" FROM T901D_CODE_LKP_COMPANY_EXCLN T901D, T901_CODE_LOOKUP T901 ");
      sbQuery.append(" WHERE T901D.C901D_VOID_FL   IS NULL ");
      sbQuery.append(" AND T901D.C1900_COMPANY_ID = " + strCompanyId + "");
      sbQuery.append(" AND T901D.C901_CODE_ID     = T901.C901_CODE_ID ");
      sbQuery.append(" AND T901.C901_CODE_GRP    IN ('" + strCodeGrpParsed + "')");
      sbQuery.append(" AND T901.C901_ACTIVE_FL    = '1' ");
      sbQuery.append(" AND T901.C901_VOID_FL     IS NULL ");
      sbQuery.append(" ) ");
    }
    // sbQuery.append(" AND  ( C1900_COMPANY_ID = '" + strCompanyID +
    // "' OR C1900_COMPANY_ID IS NULL  )");
    sbQuery.append(" ORDER BY T901.C901_CODE_SEQ_NO, UPPER(T901.C901_CODE_NM) ");
    log.debug("sbQuery.toString()" + sbQuery.toString());
    gmCacheManager.setKey("getCodeList:" + strCompanyId + ":" + strCodeGrpParsed);
    arList = gmCacheManager.queryMultipleRecords(sbQuery.toString());
    if (arList.size() < 0) {
      throw new AppError(AppError.APPLICATION, "1002");
    }
    return arList;
  }

  /**
   * reportPartyNameList - This method will fetch party information from party table
   * 
   * @param String strType -- Type for party to be retrieved
   * 
   * @return ArrayList List of party name
   * @exception AppError
   **/
  public ArrayList reportPartyNameList(String strType) throws AppError {

    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());

    String strBeanMsg = "";
    String strMsg = "";
    String strPrepareString = null;

    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;

    try {
      gmCacheManager.setPrepareString("GM_REPORT_PARTY", 3);
      /*
       * register out parameter and set input parameters
       */
      gmCacheManager.registerOutParameter(2, java.sql.Types.VARCHAR);
      gmCacheManager.registerOutParameter(3, OracleTypes.CURSOR);
      gmCacheManager.setString(1, strType);
      gmCacheManager.setKey("reportPartyNameList:" + strType + ":" + getCompId());
      gmCacheManager.execute();
      strBeanMsg = gmCacheManager.getString(2);
      rs = (ResultSet) gmCacheManager.getObject(3);
      alReturn = gmCacheManager.returnArrayList(rs);
      gmCacheManager.close();

    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    }

    return alReturn;
  }

  /**
   * reportAccountByType - This method will return arrayList of the account
   * 
   * @param String strAccountType
   * @exception AppError
   */
  public ArrayList reportAccountByType(String strAccountType) throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setPrepareString("gm_pkg_ac_invoice.gm_op_fch_account_bytype", 2);
    gmCacheManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmCacheManager.setString(1, strAccountType);
    gmCacheManager.setKey("reportAccountByType:" + getCompId());
    gmCacheManager.execute();

    ArrayList hmReturnList =
        gmCacheManager.returnArrayList((ResultSet) gmCacheManager.getObject(2));
    gmCacheManager.close();
    return hmReturnList;
  }

  /**
   * @param strKey
   * @param StrCompanyFl
   * @return
   * @throws AppError
   */
  public String getAutoCompleteKeys(String strKey, String StrCompanyFl) throws AppError {
    String strReturnKey = "";
    log.debug(" before strReturnKey ==> " + strKey);
    String strLocale = "";
    if (getComplangid().equals("10306122")) { // 10306122 Japanese
      strLocale = "JP";
    }
    // based on locale to get the Redis key
    GmResourceBundleBean rbRedis =
        GmCommonClass.getResourceBundleBean("properties.redis.GmRedis", strLocale);
    String strPropertiesValue = GmCommonClass.parseNull(rbRedis.getProperty(strKey));
    strPropertiesValue = strPropertiesValue.equals("") ? strKey : strPropertiesValue;
    if (StrCompanyFl.equalsIgnoreCase("Y")) {
      strPropertiesValue = strPropertiesValue + ":" + getCompId();
    }
    strReturnKey = strPropertiesValue + ":AUTO";
    log.debug(" After strReturnKey ==> " + strReturnKey);
    return strReturnKey;
  }

  /**
   * loadRepAccList - This method to fetch the Rep Account information based on user search
   * 
   * @param strKey
   * @param searchPrefix
   * @param iMaxCount
   * @return
   * @throws AppError
   */
  public String loadRepAccList(String strKey, String searchPrefix, int iMaxCount) throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    String strReturn = gmCacheManager.returnJsonString(getRepAccQueryStr(new HashMap()));
    return strReturn;
  }

  /**
   * getRepAccQueryStr - This method will fetch Rep Account information from table
   * 
   * @param HashMap
   * @return Result set- To get data from table
   * @exception AppError
   **/
  public String getRepAccQueryStr(HashMap hmParam) throws AppError {
    StringBuffer strQuery = new StringBuffer();
    // to get the plant id (based on company)
    String strPlantRuleVal = GmCommonClass.parseNull((String) hmParam.get("PLANT_LEVEL_ACC"));
    String strAccType = GmCommonClass.parseNull((String) hmParam.get("ACCOUNT_TYPE"));
    strQuery.append("SELECT * FROM (");
    strQuery.append("SELECT   c704_account_id ID, ");

    if (getComplangid().equals("103097")) { // 103097 Company lang English
      strQuery.append(" NVL(c704_account_nm_en,c704_account_nm) NAME ");
    } else {
      strQuery.append(" c704_account_nm  NAME ");
    }

    strQuery.append(" FROM t704_account WHERE c704_void_fl IS NULL ");

    // based on rule values to load plant/company level accounts
    if (strPlantRuleVal.equals("")) {
      strQuery.append(" AND c1900_company_id = '" + getCompId() + "' ");
    } else {
      strQuery
          .append(" AND c1900_company_id IN (SELECT c1900_company_id FROM t5041_plant_company_mapping ");
      strQuery.append(" WHERE c5040_plant_id = '" + getCompPlantId() + "' ");
      strQuery.append(" AND c5041_void_fl IS NULL AND c5041_primary_fl = 'Y') ");
    }

   if (!strAccType.equals("")) {
      strQuery.append(" AND c901_account_type IN (" + strAccType + ") ");
    }
    strQuery.append(" AND (c901_ext_country_id is NULL ");
    strQuery.append(" OR c901_ext_country_id  in (select country_id from v901_country_codes)) ");
    
    
    if (strAccType.equals("70114")) { 
        strQuery.append(" UNION SELECT c704_account_id ID,NVL(c704_account_nm_en,c704_account_nm) NAME FROM v704_ics_accounts ");
      }
    strQuery.append(" ) ORDER BY UPPER(NAME) ");
    log.debug(" Rep Account query details ==> " + strQuery.toString());
    return strQuery.toString();
  }

  /**
   * loadPartyList - This method to fetch the Party information based on user search
   * 
   * @param strKey
   * @param searchPrefix
   * @param iMaxCount
   * @return
   * @throws AppError
   */
  public String loadPartyList(String strKey, String searchPrefix, int iMaxCount, String strType) // loadParentAcctList
      throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    String strReturn = gmCacheManager.returnJsonString(getPartyQueryStr(strType));
    return strReturn;
  }

  /**
   * getPartyQueryStr - This method will fetch Party information from table
   * 
   * @param String strType -- Type for party to be retrieved
   * @return Result set- To get data from table
   * @exception AppError
   **/
  public String getPartyQueryStr(String strType) throws AppError {
    StringBuffer strQuery = new StringBuffer();
    strQuery.append(" SELECT   c101_party_id ID, ");
    // based on user default company - to get the party name
    if (getComplangid().equals("103097")) { // 103097 Company lang English
      strQuery
          .append(" NVL(c101_party_nm_en, DECODE (c101_last_nm || c101_first_nm, NULL, c101_party_nm, c101_last_nm || ' ' || c101_first_nm)) NAME ");
    } else {
      strQuery
          .append(" DECODE (c101_last_nm || c101_first_nm, NULL, c101_party_nm, c101_last_nm || ' ' || c101_first_nm) NAME ");
    }

    strQuery.append(" FROM t101_party    WHERE c901_party_type = " + strType);
    strQuery.append(" AND c101_delete_fl IS NULL ");
    strQuery.append(" AND c1900_company_id = '" + getCompId() + "' ");
    strQuery.append(" AND c101_void_fl IS NULL ");
    if (strType.equals("7003")) {// 7003 - Group Price Book
      strQuery.append(" AND c101_active_fl IS NULL ");
    } else {
      strQuery.append(" AND NVL(c101_active_fl, '-999') !='N' ");
    }
    strQuery.append(" ORDER BY UPPER (TRIM (NAME)) ");
    log.debug(" Party type list details ==> " + strQuery.toString());
    return strQuery.toString();
  }


  /**
   * loadRepAccList - This method to fetch the Rep Account information based on user search
   * 
   * @param strKey
   * @param searchPrefix
   * @param iMaxCount
   * @return
   * @throws AppError
   */
  public String loadDemandSheetList(HashMap hmParam) throws AppError {
    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
    String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
    String strGopLoadFl = GmCommonClass.parseNull((String) hmParam.get("GOPLOADFL"));
    String strGopLoadGrowthFl = GmCommonClass.parseNull((String) hmParam.get("GOPLOADGRW"));

    int iMaxCount = (Integer) hmParam.get("MAX_CNT");
    String strReturn = "";
    GmDemandSetupBean gmDemandSetupBean = new GmDemandSetupBean(getGmDataStoreVO());
    GmCacheManager gmCacheManager = null;
    if (strGopLoadFl.equals("Y")) {
      gmCacheManager = new GmCacheManager(GmDNSNamesEnum.DMWEBGLOBUS);
    } else {
      gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    }

    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
    if (strGopLoadGrowthFl.equals("Y")) {
      strReturn = gmCacheManager.returnJsonString(loadDemandSheetStrName(hmParam));
    } else {
      strReturn = gmCacheManager.returnJsonString(gmDemandSetupBean.loadDemandSheetStr(hmParam));
    }
    return strReturn;
  }

  public String loadDemandSheetStrName(HashMap hmParam) throws AppError {
    StringBuffer strQuery = new StringBuffer();
    strQuery.append("SELECT t4020.c4020_demand_master_id ID, t4020.c4020_demand_nm NAME ");
    strQuery.append("FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020 ");
    strQuery.append("WHERE t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id ");
    strQuery.append("AND t4020.c4020_void_fl IS NULL ");
    strQuery.append("AND t4020.c4020_inactive_fl IS NULL ");
    strQuery.append("AND t4015.c4015_void_fl IS NULL ");
    strQuery.append("AND t4015.c901_level_id != 102580 "); // company
    strQuery.append("AND t4015.c901_access_type = 102623 "); // editable
    strQuery.append("AND T4020.C901_Demand_Type IN (40020,40021,40022) ");
    strQuery.append("ORDER BY UPPER(t4020.c4020_demand_nm)");
    log.debug(" loadDemandSheetStrName==> " + strQuery.toString());

    return strQuery.toString();
  }

  /**
   * loadAccList - This method to fetch the Rep Account information based on user search
   * 
   * @param strKey
   * @param searchPrefix
   * @param iMaxCount
   * @return
   * @throws AppError
   */
  public String loadAccList(String strKey, String strSearchPrefix, int iMaxCount, HashMap hmParam)
      throws AppError {
    HashMap hmRuleData = new HashMap();
    GmCommonBean gmcommonbean = new GmCommonBean();
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(strSearchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
    String strCompanyID = getCompId();
    // to get the plant id (based on company)
    hmRuleData.put("RULEID", strCompanyID);
    hmRuleData.put("RULEGROUP", "LOANERINITFILTER");
    String strCompPlantFl = GmCommonClass.parseNull(gmcommonbean.getRuleValue(hmRuleData));
    hmParam.put("PLANT_LEVEL_ACC", strCompPlantFl);
    String strReturn = gmCacheManager.returnJsonString(getRepAccQueryStr(hmParam));
    return strReturn;
  }


  public String loadSurgeonName(HashMap hmParam) throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setKey(GmCommonClass.parseNull((String) hmParam.get("KEY")));
    gmCacheManager.setSearchPrefix(GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY")));
    gmCacheManager.setMaxFetchCount((Integer) hmParam.get("MAX_CNT"));
    String strReturn = gmCacheManager.returnJsonString(getLoadSurgeonNameQueryStr(hmParam));
    return strReturn;
  }

  public String getLoadSurgeonNameQueryStr(HashMap hmParam) throws AppError {
    StringBuffer strQuery = new StringBuffer();
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    if (strOpt.equals("npilist")) {
      strQuery.append(" SELECT v6600.c6600_surgeon_npi_id NAME, ");
      strQuery.append(" v6600.partynm ID");
    } else {
      strQuery.append(" SELECT v6600.c6600_surgeon_npi_id ID, ");
      strQuery.append(" v6600.partynm NAME");
    }
    strQuery.append(" FROM V6600_PARTY_SURGEON v6600 ");
    //PMT-39389 Removed company and plant filter
    //strQuery.append(" WHERE v6600.compid =" + getCompId());
    //strQuery.append(" AND v6600.plantid = " + getCompPlantId());
    log.debug(" getLoadSurgeonNameQueryStr ==> " + strQuery.toString());
    return strQuery.toString();
  }

  /**
   * loadTTPList - This method to fetch the TTP information based on user search
   * 
   * @param hmParam
   * @param searchPrefix
   * @param iMaxCount
   * @return String
   * @throws AppError
   */
  public String loadTTPList(HashMap hmParam) throws AppError {
    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
    String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    log.debug("strOpt=" + strOpt);
    int iMaxCount = (Integer) hmParam.get("MAX_CNT");
    GmCacheManager gmCacheManager = null;
    if (strOpt.equals("GOP")) {
      gmCacheManager = new GmCacheManager(GmDNSNamesEnum.DMWEBGLOBUS);
    } else {
      gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    }
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    String strReturn = gmCacheManager.returnJsonString(loadTTPListStr(hmParam));
    return strReturn;
  }

  /**
   * loadTTPListStr - This method to return Query.
   * 
   * @param hmparam
   * @return String
   * @throws AppError
   */
  public String loadTTPListStr(HashMap hmparam) throws AppError {
    StringBuffer strQuery = new StringBuffer();
    strQuery.append("SELECT   c4050_ttp_id ID, c4050_ttp_nm NAME ");
    strQuery.append("FROM t4050_ttp ");
    strQuery.append("WHERE c4050_void_fl IS NULL ");
    strQuery.append("ORDER BY UPPER(c4050_ttp_nm)");
    log.debug(" loadTTPListStr ==> " + strQuery.toString());
    return strQuery.toString();
  }

  /**
   * loadARAccountList - This method to fetch the TTP information based on user search
   * 
   * @param hmParam
   * @return String
   * @throws AppError
   */
  public String loadARAccountList(HashMap hmParam) throws AppError {
    String strSearchValue = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
    String strKey = "";
    String strQuery = "";
    int iMaxCount = (Integer) hmParam.get("MAX_CNT");
    String strSourceType = GmCommonClass.parseZero((String) hmParam.get("SOURCETYPE"));
    if (strSourceType.equals("50256")) { // ICT (Distributor)
      // 70103 - Inter-Company Transfer (ICT)
      strKey = getAutoCompleteKeys("AR_DIST_LIST", "Y");
      hmParam.put("DISTTYP", "70103");
      //
      strQuery = getDistributorQueryStr(hmParam);
    } else if (strSourceType.equals("26240213")) { // Dealer
      // 26230725 Dealer
      strKey = getAutoCompleteKeys("AR_DEALER_LIST", "Y");
      //
      strQuery = getPartyQueryStr("26230725");
    } else {
      // 50255, 50253, 50257
      String strAccountType =
          GmCommonClass.parseZero(GmCommonClass.getRuleValueByCompany(strSourceType,
              "AR_ACCOUNT_SOURCE", getCompId()));
      strKey = getAutoCompleteKeys("AR_REP_ACCOUNT_LIST" + strSourceType, "Y");
      log.debug(" strAccountType --> " + strAccountType);
      hmParam.put("ACCOUNT_TYPE", strAccountType);
      //
      strQuery = getRepAccQueryStr(hmParam);

    }
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(strSearchValue);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    String strReturn = gmCacheManager.returnJsonString(strQuery);
    return strReturn;
  }
  
  
  /**
   * fetchWorldWideSheet - This method to fetch the Demand sheet name
   * 
   * @param hmParam
   * @param searchPrefix
   * @param iMaxCount
   * @return String
   * @throws AppError
   */
  public String fetchWorldWideSheet(HashMap hmParam) throws AppError {
    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
    String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
    int iMaxCount = (Integer) hmParam.get("MAX_CNT");
    GmCacheManager gmCacheManager = null;
      gmCacheManager = new GmCacheManager(GmDNSNamesEnum.DMWEBGLOBUS);
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    String strReturn = gmCacheManager.returnJsonString(fetchWorldWideSheetName(hmParam));
    return strReturn;
  }
  
  /**
   * loadTTPListStr - This method to return Query.
   * 
   * @param hmparam
   * @return String
   * @throws AppError
   */
  public String fetchWorldWideSheetName(HashMap hmparam) throws AppError {
    StringBuffer strQuery = new StringBuffer();
	strQuery.append("SELECT t4020.c4020_demand_master_id ID, t4020.c4020_demand_nm NAME ");
	strQuery.append("FROM t4015_global_sheet_mapping t4015, t4020_demand_master t4020 ");
	strQuery.append(" WHERE t4020.c4015_global_sheet_map_id = t4015.c4015_global_sheet_map_id ");
	strQuery.append("  AND t4020.c4020_void_fl            IS NULL ");
	strQuery.append("AND t4015.c4015_void_fl            IS NULL ");
	strQuery.append(" AND t4015.c901_level_id            = 102580 "); // company (world wide)
	strQuery.append("AND t4020.c4020_inactive_fl        IS NULL ");
    strQuery.append("ORDER BY UPPER(t4020.c4020_demand_nm) "); 
		
        log.debug(" fetchWorldWideSheetName ==> " + strQuery.toString());
    return strQuery.toString();
  }
  /**
   * loadActiveFieldSalesList - This method to fetch the field sales information 
   * 
   * @param strKey
   * @param searchPrefix
   * @param iMaxCount
   * @param hmFilter
   * @return
   * @throws AppError
   */
  public String loadActiveFieldSalesList(String strKey, String searchPrefix, int iMaxCount, HashMap hmFilter)
      throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    
    log.debug(" Key values is " + strKey);
    log.debug(" searchPrefix values is " + searchPrefix);
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    String strReturn = gmCacheManager.returnJsonString(getDistributorQuryStr(hmFilter));

    return strReturn;
  } // End of loadSetMap
  
  public String getDistributorQuryStr(HashMap hmParam) throws AppError {
	    String strFilter = GmCommonClass.parseNull((String) hmParam.get("FILTER"));
	    String strDistTyp = GmCommonClass.parseNull((String) hmParam.get("DISTTYP"));
	    String strShipTyp = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
	    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
	    String strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DISTFILTER"));
	    String strExclDistTyp = GmCommonClass.parseNull((String) hmParam.get("EXCL_DISTTYP"));
	    
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	     StringBuffer sbQuery = new StringBuffer();
	      sbQuery
	          .append(" SELECT  C701_DISTRIBUTOR_ID ID,DECODE('"
	              + getComplangid()
	              + "', '103097',NVL(C701_DISTRIBUTOR_NAME_EN, C701_DISTRIBUTOR_NAME), C701_DISTRIBUTOR_NAME) NAME , 'D' || C701_DISTRIBUTOR_ID DID, NVL(t710.c901_division_id,'100823') DDIV , LOWER(C701_DISTRIBUTOR_NAME) distname");
	      sbQuery.append(" FROM T701_DISTRIBUTOR ");
	      // Instead of function we are getting the division id from master table
	      sbQuery.append(" ,(SELECT DISTINCT t710_tmp.c901_area_id, t710_tmp.c901_division_id ");
	      sbQuery.append(" FROM t710_sales_hierarchy t710_tmp ");
	      sbQuery.append(" WHERE t710_tmp.c710_active_fl = 'Y' ");
	      sbQuery.append(" AND t710_tmp.c710_void_fl IS NULL) t710 ");
	      sbQuery.append(" WHERE C701_VOID_FL IS NULL AND (c901_ext_country_id is NULL ");
	      sbQuery.append(" OR c901_ext_country_id  in (select country_id from v901_country_codes))");
	      if (strFilter.equals("Active")) {
	        sbQuery.append(" AND ");
	        sbQuery.append(" C701_ACTIVE_FL = 'Y'");
	      }
	      if (!strExclDistTyp.equals("")) {
	        sbQuery.append(" AND c901_distributor_type NOT IN ( ");
	        sbQuery.append(strExclDistTyp);
	        sbQuery.append(" ) ");
	      }

	      if (!strDistTyp.equals("")) {
	        sbQuery.append(" AND c901_distributor_type = ");
	        sbQuery.append(strDistTyp);
	      }
	      if (!strShipTyp.equals("")) {
	        sbQuery.append(" AND C701_DISTRIBUTOR_ID in (decode(" + strShipTyp + ",4120," + strRepID
	            + ",(SELECT C701_DISTRIBUTOR_ID FROM T101_USER WHERE C101_USER_ID ='" + strRepID
	            + "'))) ");

	      }
	      sbQuery.append(" AND c701_region = t710.c901_area_id (+) ");
	      // Filtering Distributor based on all the companies associated with the plant.
	      if (strDistFilter.equals("COMPANY-PLANT")) {
	        sbQuery
	            .append(" AND ( C1900_COMPANY_ID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = '"
	                + getCompPlantId() + "'  AND C5041_VOID_FL IS NULL )");
	        sbQuery.append(" OR C1900_COMPANY_ID IS NULL )  ");
	        sbQuery
	            .append("  AND C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
	        sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
	            + getCompId() + "') ");

	      } else {
	        sbQuery.append("  AND ( C1900_COMPANY_ID IS NULL OR C1900_COMPANY_ID = '" + getCompId()
	            + "') ");
	        sbQuery
	            .append("  AND C701_DISTRIBUTOR_ID NOT IN ( SELECT C906_RULE_VALUE FROM T906_RULES WHERE C906_RULE_GRP_ID = 'EXCICT' ");
	        sbQuery.append("  AND C906_RULE_ID = '" + getCompId() + "' AND C1900_COMPANY_ID ='"
	            + getCompId() + "') ");
	      }
	      sbQuery.append(" ORDER BY distname ");
	    return sbQuery.toString();
	  } // End of getDistributorList

  /**
   * loadActiveSetList - This method to load the active set list information
   * 
   * @param strKey
   * @param searchPrefix
   * @param iMaxCount
   * @return
   * @throws AppError
   */
  public String loadActiveSetList(String strKey, String searchPrefix, int iMaxCount)
      throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    log.debug(" Key values is " + strKey);
    log.debug(" searchPrefix values is " + searchPrefix);
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    String strReturn = gmCacheManager.returnJsonString(getSetListQueryStr());
    return strReturn;
  } // End of loadSetMap
  
  public String getSetListQueryStr() throws AppError {
	    
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	     StringBuffer sbQuery = new StringBuffer();
	      
	      sbQuery.append(" SELECT DISTINCT (t504.c207_set_id) SETID, C207_SET_NM NAME");
	      sbQuery.append(" FROM t504_consignment t504, t504a_consignment_loaner t504a , T207_SET_MASTER t207,T2080_SET_COMPANY_MAPPING t2080 ");
	      sbQuery.append(" WHERE t504.c504_consignment_id = t504a.c504_consignment_id ");
	      sbQuery.append(" AND t207.c207_set_id = t504.c207_set_id ");
	      sbQuery.append(" AND T504.C504_VOID_FL IS NULL ");
	      sbQuery.append(" AND t504.c504_type = 4127");
	      sbQuery.append(" AND t504.c504_status_fl = '4' ");
	      sbQuery.append(" AND t504a.C504A_STATUS_FL != '60' ");
	      sbQuery.append(" AND t504a.c504a_void_fl IS NULL ");
	      sbQuery.append(" AND t207.C207_VOID_FL IS NULL ");
	      sbQuery.append(" AND T2080.C207_SET_ID = T207.C207_SET_ID ");
	      sbQuery.append(" AND t2080.c2080_void_fl is null ");
	      sbQuery.append(" AND T2080.C1900_COMPANY_ID in (SELECT C1900_COMPANY_ID FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID= '"+ getCompPlantId()+"' AND C5041_VOID_FL IS NULL) ");
	      sbQuery.append(" AND t504a.C5040_PLANT_ID = "+ getCompPlantId());
	 
	      sbQuery.append("ORDER BY t504.c207_set_id");
	    return sbQuery.toString();
	  } // End of getDistributorList
  
  /**
   * loadActiveDistRepList - This method to fetch the Dest Rep list
   * 
   * @param strKey
   * @param searchPrefix
   * @param iMaxCount
   * @return
   * @throws AppError
   */
  public String loadActiveDistRepList(String strKey, String searchPrefix, int iMaxCount, HashMap hmFilter)
      throws AppError {
    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
    log.debug(" Key values is " + strKey);
    log.debug(" searchPrefix values is " + searchPrefix);
    gmCacheManager.setKey(strKey);
    gmCacheManager.setSearchPrefix(searchPrefix);
    gmCacheManager.setMaxFetchCount(iMaxCount);
    String strReturn = gmCacheManager.returnJsonString(getDistributorRepQuryStr(hmFilter));
    return strReturn;
  } // End of loadSetMap
  
  public String getDistributorRepQuryStr(HashMap hmParam) throws AppError {
	  
	    String strFilter = GmCommonClass.parseNull((String) hmParam.get("FILTER"));
	    String strShipTyp = GmCommonClass.parseNull((String) hmParam.get("SHIPTYPE"));
	    String strRepID = GmCommonClass.parseNull((String) hmParam.get("REPID"));
	    String strDivID = GmCommonClass.parseNull((String) hmParam.get("DIVID"));
	    String strAccountSrc = GmCommonClass.parseNull((String) hmParam.get("ACCOUNTSRC"));
	    String strDirectRepType = GmCommonClass.parseNull((String) hmParam.get("DIRECTREPTYPE"));
	    String strRepFilter = GmCommonClass.parseNull((String) hmParam.get("REPFILTER"));
	    String strCompanyID = GmCommonClass.parseNull((String) hmParam.get("COMPANY_ID"));
	    String strDistId ="310";
	    
	        GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	        StringBuffer sbQuery = new StringBuffer();
	        sbQuery
	            .append(" SELECT C703_SALES_REP_ID REPID,C703_SALES_REP_ID ID, 'S' || C703_SALES_REP_ID SID, t701.C701_DISTRIBUTOR_ID DID,  DECODE('"
	                + getComplangid()
	                + "' , '103097',NVL(C703_SALES_REP_NAME_EN, C703_SALES_REP_NAME), C703_SALES_REP_NAME) NAME,  ");
	        sbQuery.append(" t701.c701_distributor_name DNAME ");
	        sbQuery
	            .append(" , T101.C101_PARTY_ID  PARTYID ,  T101.C101_FIRST_NM || ' ' || T101.C101_LAST_NM PARTYNAME  "); // Party
	        sbQuery.append(" FROM T703_SALES_REP  T703 , T101_PARTY T101, t701_distributor t701 ");
	        sbQuery.append(" WHERE  T703.C101_PARTY_ID = T101.C101_PARTY_ID  AND  C703_VOID_FL IS NULL ");
	        sbQuery.append(" AND t703.c701_distributor_id = "+ strDistId);
	        sbQuery.append(" AND t701.c701_void_fl IS NULL ");
	        if (strDivID.equals("")) {
	          // Filtering Distributor based on all the companies associated with the plant.
	          if (strRepFilter.equals("COMPANY-PLANT")) {
	            sbQuery
	                .append(" AND  T703.C1900_COMPANY_ID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = "
	                    + getCompPlantId() + "  AND C5041_VOID_FL IS NULL )");
	          } else {
	            sbQuery.append(" AND  T703.C1900_COMPANY_ID = "+ getCompId());
	          }
	        } else {
	          if (!strDivID.equals("")) {
	            sbQuery.append(" AND T703.c901_ext_country_id is NOT NULL ");
	          } else {
	            sbQuery.append(" AND (T703.c901_ext_country_id is NULL ");
	            sbQuery
	                .append(" OR T703.c901_ext_country_id  in (select country_id from v901_country_codes))");
	          }
	        }
	        if (strFilter.equals("Active")) {
	          sbQuery.append(" AND ");
	          sbQuery.append(" C703_ACTIVE_FL = 'Y' ");
	        }
	        if (!strShipTyp.equals("")) {
	          sbQuery.append(" AND t701.C701_DISTRIBUTOR_ID in (decode(" + strShipTyp + ",4120,"
	              + strRepID + ",(SELECT C701_DISTRIBUTOR_ID FROM T101_USER WHERE C101_USER_ID ='"
	              + strRepID + "'))) ");

	        }
	        // If Source dropdown is showing in UI, loading Direct SalesReps in Dropdown
	        if (!strDirectRepType.equals("")) {
	          sbQuery.append(" AND t701.c901_distributor_type IN(" + strDirectRepType + ")");
	        }
           log.debug("bQuery.toString()>>>>>>>>>>>>"+sbQuery.toString());
	        sbQuery.append(" ORDER BY UPPER(C703_SALES_REP_NAME) ");
	     	      
	    return sbQuery.toString();
	  } // End of getDistributorList
  

  
  
  public String loadVendList(String strKey, String searchPrefix, int iMaxCount)
	      throws AppError {
	    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
	    log.debug(" Key values is " + strKey);
	    log.debug(" searchPrefix values is " + searchPrefix);
	    gmCacheManager.setKey(strKey);
	    gmCacheManager.setSearchPrefix(searchPrefix);
	    gmCacheManager.setMaxFetchCount(iMaxCount);
	    String strReturn = gmCacheManager.returnJsonString(getVendListQueryStr(new HashMap()));

	    return strReturn;
	  }
	  
	  public String getVendListQueryStr(HashMap hmParam) throws AppError {
		  
		  String strCheckActiveFlag = GmCommonClass.parseNull((String) hmParam.get("CHECKACTIVEFL"));
		    
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		     StringBuffer sbQuery = new StringBuffer();
		   
		     sbQuery
		        .append(" SELECT	T301.C301_VENDOR_ID ID,	T301.C301_VENDOR_NAME NAME, T101.C101_PARTY_ID VID, ");
		    sbQuery
		        .append(" T101.C101_PARTY_NM VNAME, T301.C301_ACTIVE_FL ACTFL FROM T301_VENDOR T301, T101_PARTY T101 ");
		    sbQuery.append(" WHERE T301.C101_PARTY_ID = T101.C101_PARTY_ID ");
		 	  
		    // Appending company filter
		    sbQuery.append(" AND  t301.C1900_COMPANY_ID = " + getCompId());

		    sbQuery.append(" ORDER  BY UPPER(T301.C301_VENDOR_NAME) ");
		     
		    return sbQuery.toString();
		  }
	  /**
	   * fetchAccountName - This method to fetch the account name list
	   * 
	   * @param hmParam
	   * @param searchPrefix
	   * @param iMaxCount
	   * @return String
	   * @throws AppError
	   */
	  public String fetchAccountByType(HashMap hmParam) throws AppError {
		 
	    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
	    String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
	    int iMaxCount = (Integer) hmParam.get("MAX_CNT");
	    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
	    gmCacheManager.setKey(strKey);
	    gmCacheManager.setSearchPrefix(searchPrefix);
	    gmCacheManager.setMaxFetchCount(iMaxCount);
	    String strReturn = gmCacheManager.returnJsonString(reportAccount(hmParam));
	    return strReturn;
	  }
	  /**
	   * reportAccount - This method to fetch the account list by type 
	   * 
	   * @param hmParam
	   * @return String
	   * @throws AppError
	   */
	  public String reportAccount(HashMap hmParam) throws AppError { 
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

	      StringBuffer sbQuery = new StringBuffer();
	      String strAccountType = GmCommonClass.parseNull((String) hmParam.get("ACCOUNT_TYPE"));
	      String strSessCompanyLangId =
	              GmCommonClass.parseNull(GmCommonClass.getCompanyLanguage(getGmDataStoreVO().getCmpid()));
	      String strSessCompanyLocale =
	              GmCommonClass.parseNull(GmCommonClass.getCompanyLocale(getGmDataStoreVO().getCmpid()));
	      sbQuery
	          .append(" SELECT   c704_account_id ID, DECODE('"+ getComplangid() +"','103097',NVL(c704_account_nm_en,c704_account_nm),c704_account_nm) NAME, c703_sales_rep_id repid ");
	      sbQuery.append(" FROM t704_account ");
	      sbQuery.append(" WHERE c704_void_fl IS NULL ");
	      sbQuery.append(" AND c901_account_type = nvl("+ strAccountType +",c901_account_type) AND (c1900_company_id = '"+ getCompId() +"' OR"
	    		  		+ "  c1900_company_id IN (SELECT c1900_company_id FROM t5041_plant_company_mapping WHERE c5040_plant_id = '"+ getCompPlantId() +"'  AND "
	      				+ "c5041_void_fl IS NULL AND c5041_primary_fl = 'Y')) ");
	      sbQuery.append(" ORDER BY UPPER(NAME) ");
	      return sbQuery.toString();
	  }
	  /**
	   * reportAccount - This method to fetch the Group list
	   * 
	   * @param hmParam
	   * @return String
	   * @throws AppError
	   */
	  public String fetchDemandGroupList(HashMap hmParam) throws AppError {
		  String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		    String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		    int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
			// 40045 Forecast/Demand Group
			strType = strType.equals("") ? "40045" : strType;
			hmParam.put("TYPE", strType);
			
		    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		    gmCacheManager.setKey(strKey);
		    gmCacheManager.setSearchPrefix(searchPrefix);
		    gmCacheManager.setMaxFetchCount(iMaxCount);
		    gmCacheManager.setMaxFetchCount(iMaxCount);
		    HashMap hmdata = new HashMap();
		    hmdata.put("TYPE", strType);
		    String strReturn = gmCacheManager.returnJsonString(loadDemandGroupList(hmParam));
		 
		    return strReturn;
		  
	  }
	  /**
	   * reportAccount - This method to fetch the Group list by key, type
	   * 
	   * @param hmParam
	   * @return String
	   * @throws AppError
	   */
	  public String loadDemandGroupList(HashMap hmParam) throws AppError {

		   
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));;
		    String strGroupType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
	
		    StringBuffer sbQuery = new StringBuffer();

		    sbQuery
		        .append(" SELECT T4010.C4010_GROUP_ID ID, T4010.C4010_GROUP_NM NAME , T4010.C901_GROUP_TYPE PID, T4010.c207_set_id SETID  ");
		    sbQuery.append(" FROM T4010_GROUP T4010, T4012_GROUP_COMPANY_MAPPING T4012 ");
		    sbQuery.append(" WHERE T4010.C4010_VOID_FL IS NULL ");
		    sbQuery.append(" AND T4012.C4012_VOID_FL IS NULL ");
		    sbQuery.append(" AND T4010.C4010_GROUP_ID = T4012.C4010_GROUP_ID ");

		    /*
		     * if (!strSetID.equals("") && !strSetID.equals("0")) { sbQuery.append(" AND C207_SET_ID ='");
		     * sbQuery.append(strSetID); sbQuery.append("'"); }
		     */

		    sbQuery.append(" AND T4010.C901_TYPE = "+strGroupType);
		    sbQuery.append(" AND T4012.C1900_COMPANY_ID = "+ getCompId());
		    sbQuery.append(" ORDER BY  UPPER(T4010.C4010_GROUP_NM) ");

		    log.debug("Query inside loadDemandGroupList is " + sbQuery.toString());

		  /*  gmDBManager.setPrepareString(sbQuery.toString());
		    gmDBManager.setString(1, strGroupType);
		    gmDBManager.setString(2, strCompanyId);*/

		    

		    return sbQuery.toString();
		  }
	  
	  /**
	   * reportAccount - This method to fetch the System list 
	   * 
	   * @param hmParam
	   * @return String
	   * @throws AppError
	   */
	  public String fetchSystemList(HashMap hmParam) throws AppError {
		  
		  String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		    String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		    int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		    //String strType= "1600";
		    GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		    gmCacheManager.setKey(strKey);
		    gmCacheManager.setSearchPrefix(searchPrefix);
		    gmCacheManager.setMaxFetchCount(iMaxCount);
		    gmCacheManager.setMaxFetchCount(iMaxCount);
		    HashMap hmdata = new HashMap();
		    hmdata.put("TYPE", strType);
		    String strReturn = gmCacheManager.returnJsonString(loadSetMap(hmParam));
		   
		    return strReturn;
		  
	  }
	  
	  /**
	   * reportAccount - This method to fetch the Setmap list 
	   * 
	   * @param hmParam
	   * @return String
	   * @throws AppError
	   */ 
         
		  public String loadSetmapList(HashMap hmParam) throws AppError {
			  
		         
		        String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		        String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		        int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		        String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		        //String strType= "1600";
		        GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		        gmCacheManager.setKey(strKey);
		        gmCacheManager.setSearchPrefix(searchPrefix);
		        gmCacheManager.setMaxFetchCount(iMaxCount);
		        gmCacheManager.setMaxFetchCount(iMaxCount);
		        HashMap hmdata = new HashMap();
		        hmParam.put("TYPE", "1601");// 1601 is for current set list
		        hmParam.put("SETTYPE", "4070,4078"); //PC-5711 Enable the ability to transfer demo sets consigned in the field
		        String strReturn = gmCacheManager.returnJsonString(loadSetMapList(hmParam));
		        return strReturn;
        
	  }           
	  
	  /**
	   * reportAccount - This method to fetch the System list by type
	   * 
	   * @param hmParam
	   * @return String
	   * @throws AppError
	   */
	public String loadSetMap(HashMap hmValues ) throws AppError {
		    ArrayList alReturn = new ArrayList();
		    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		    
		    String strType = GmCommonClass.parseNull((String) hmValues.get("TYPE"));
		    String strDeptId = GmCommonClass.parseNull((String) hmValues.get("DEPTID"));
		    String strGroupPartMapping = GmCommonClass.parseNull((String) hmValues.get("GROUPPART"));
		    String strCompanyId = getCompId();

       
		    HashMap hmRuleData = new HashMap();
		    hmRuleData.put("RULEID", strDeptId);
		    hmRuleData.put("RULEGROUP", "SETMAPACCESS");

		    String strStatus = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData));
		    strStatus = GmCommonClass.getStringWithQuotes(strStatus);
		   

		    String strSearchSet = GmCommonClass.parseNull((String) hmValues.get("SETNAME"));
		    String strSystem = GmCommonClass.parseZero((String) hmValues.get("SYSTEM"));
		    String strHierarchy = GmCommonClass.parseZero((String) hmValues.get("HIERARCHY"));
		    String strCategory = GmCommonClass.parseZero((String) hmValues.get("CATEGORY"));
		    String strSetType = GmCommonClass.parseZero((String) hmValues.get("SETTYPE"));
		    String strSetStatus = GmCommonClass.parseZero((String) hmValues.get("SETSTATUS"));

		    log.debug(" status is " + strStatus);
		    StringBuffer sbQuery = new StringBuffer();
		    sbQuery
		        .append(" SELECT	T207.C207_SET_ID ID, C207_SET_NM NAME, T207.C207_SET_ID ||'-'|| C207_SET_NM IDNAME, ");
		    sbQuery.append(" GET_CODE_NAME(C207_CATEGORY) CATEG, GET_CODE_NAME(C207_TYPE) TYPE, ");
		    sbQuery.append(" GET_CODE_NAME(C901_STATUS_ID) STAT, C207_REV_LVL REVLVL, ");
		    sbQuery
		        .append(" C207_SET_DESC  PARTNUMDESC, C207_TYPE TYPEID, C207_SET_SALES_SYSTEM_ID SYSTEMID, ");
		    sbQuery.append(" GET_USER_NAME(C207_LAST_UPDATED_BY) UPDBY, C207_LAST_UPDATED_DATE UPDDT ");
		    sbQuery
		        .append(" , GET_SET_NAME(C207_SET_SALES_SYSTEM_ID) SYSTEMNAME, GET_CODE_NAME_ALT(C901_HIERARCHY) HIERARCHY ");
		    sbQuery
		        .append(" , GET_CODE_NAME (DECODE (T207.C207_TYPE,4074, DECODE (T207.C901_HIERARCHY, 20700, '103148', '103150'),  DECODE (T207.C901_HIERARCHY, 20700, '103147', '103149'))) SETTYPENM");
		    sbQuery.append(" , DECODE (T207.C901_CONS_RPT_ID, 20100,'Yes', 'No') BASELINENM");
		    sbQuery.append(" , GET_CODE_NAME (T207A.C901_SHARED_STATUS) SHAREDSTATNM");

		    sbQuery
		        .append(" FROM T207_SET_MASTER T207, T2080_SET_COMPANY_MAPPING T2080, t207a_set_link t207a WHERE C901_SET_GRP_TYPE ='");
		    sbQuery.append(strType);
		    sbQuery.append("' ");
		    sbQuery.append(" AND T207.C207_SET_ID = T2080.C207_SET_ID");
		    sbQuery.append(" AND T2080.C2080_VOID_FL IS NULL");
		    sbQuery.append(" AND C207_VOID_FL IS NULL ");
		    sbQuery.append(" AND C207_OBSOLETE_FL IS NULL ");
		    sbQuery.append("AND t207.c207_set_id  = t207a.c207_link_set_id (+)");
		    if (!strStatus.equals("")) {
		      sbQuery.append(" AND C901_STATUS_ID IN ('");
		      sbQuery.append(strStatus);
		      sbQuery.append("')");
		    }

		    if (!strSearchSet.equals("")) {
		      sbQuery.append(" AND UPPER(C207_SET_NM) LIKE ('%");
		      sbQuery.append(strSearchSet.toUpperCase());
		      sbQuery.append("%')");
		    }

		    if (!strSystem.equals("0")) {
		      sbQuery.append(" AND C207_SET_SALES_SYSTEM_ID IN ('");
		      sbQuery.append(strSystem);
		      sbQuery.append("')");
		    }

		    if (!strHierarchy.equals("0")) {
		      sbQuery.append(" AND C901_HIERARCHY IN ('");
		      sbQuery.append(strHierarchy);
		      sbQuery.append("')");
		    }

		    if (!strCategory.equals("0")) {
		      sbQuery.append(" AND C207_CATEGORY IN ('");
		      sbQuery.append(strCategory);
		      sbQuery.append("')");
		    }

		    if (!strSetType.equals("0")) {
		      sbQuery.append(" AND C207_TYPE IN ('");
		      sbQuery.append(strSetType);
		      sbQuery.append("')");
		    }

		    if (!strSetStatus.equals("0")) {
		      sbQuery.append(" AND C901_STATUS_ID IN ('");
		      sbQuery.append(strSetStatus);
		      sbQuery.append("')");
		    }


		    sbQuery.append(" AND T2080.C1900_COMPANY_ID = '");
		    sbQuery.append(strCompanyId);
		    sbQuery.append("'");
		    sbQuery.append(" ORDER BY ");


		    if (strGroupPartMapping.equals("GROUPPARTMAPPING")) {
		      sbQuery.append(" UPPER(C207_SET_NM), ");
		    }

		    sbQuery.append(" T207.C207_SET_ID");
		    log.debug(" Query for loading set map " + sbQuery.toString());
		   

		    return sbQuery.toString();
		    
		    
		  } // End of loadSetMap
	  
	
	
	/**
	   * reportAccount - This method to fetch the System list by type
	   * 
	   * @param hmParam
	   * @return String
	   * @throws AppError
	   */
	public String loadSetMapList(HashMap hmValues ) throws AppError {
		    ArrayList alReturn = new ArrayList();
		    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		    
		    String strType = GmCommonClass.parseNull((String) hmValues.get("TYPE"));
		    String strDeptId = GmCommonClass.parseNull((String) hmValues.get("DEPTID"));
		    String strGroupPartMapping = GmCommonClass.parseNull((String) hmValues.get("GROUPPART"));
		    String strCompanyId = getCompId();

     
		    HashMap hmRuleData = new HashMap();
		    hmRuleData.put("RULEID", strDeptId);
		    hmRuleData.put("RULEGROUP", "SETMAPACCESS");

		    String strStatus = GmCommonClass.parseNull(gmCommonBean.getRuleValue(hmRuleData));
		    strStatus = GmCommonClass.getStringWithQuotes(strStatus);
		   

		    String strSearchSet = GmCommonClass.parseNull((String) hmValues.get("SETNAME"));
		    String strSystem = GmCommonClass.parseZero((String) hmValues.get("SYSTEM"));
		    String strHierarchy = GmCommonClass.parseZero((String) hmValues.get("HIERARCHY"));
		    String strCategory = GmCommonClass.parseZero((String) hmValues.get("CATEGORY"));
		    String strSetType = GmCommonClass.parseZero((String) hmValues.get("SETTYPE"));
		    String strSetStatus = GmCommonClass.parseZero((String) hmValues.get("SETSTATUS"));

		    log.debug(" status is " + strStatus);
		    StringBuffer sbQuery = new StringBuffer();
		    sbQuery
		        .append(" SELECT	T207.C207_SET_ID ID, C207_SET_NM SETNAME, T207.C207_SET_ID ||' - '|| C207_SET_NM NAME, ");
		    sbQuery.append(" GET_CODE_NAME(C207_CATEGORY) CATEG, GET_CODE_NAME(C207_TYPE) TYPE, ");
		    sbQuery.append(" GET_CODE_NAME(C901_STATUS_ID) STAT, C207_REV_LVL REVLVL, ");
		    sbQuery
		        .append(" C207_SET_DESC  PARTNUMDESC, C207_TYPE TYPEID, C207_SET_SALES_SYSTEM_ID SYSTEMID, ");
		    sbQuery.append(" GET_USER_NAME(C207_LAST_UPDATED_BY) UPDBY, C207_LAST_UPDATED_DATE UPDDT ");
		    sbQuery
		        .append(" , GET_SET_NAME(C207_SET_SALES_SYSTEM_ID) SYSTEMNAME, GET_CODE_NAME_ALT(C901_HIERARCHY) HIERARCHY ");
		    sbQuery
		        .append(" , GET_CODE_NAME (DECODE (T207.C207_TYPE,4074, DECODE (T207.C901_HIERARCHY, 20700, '103148', '103150'),  DECODE (T207.C901_HIERARCHY, 20700, '103147', '103149'))) SETTYPENM");
		    sbQuery.append(" , DECODE (T207.C901_CONS_RPT_ID, 20100,'Yes', 'No') BASELINENM");
		    sbQuery.append(" , GET_CODE_NAME (T207A.C901_SHARED_STATUS) SHAREDSTATNM");

		    sbQuery
		        .append(" FROM T207_SET_MASTER T207, T2080_SET_COMPANY_MAPPING T2080, t207a_set_link t207a WHERE C901_SET_GRP_TYPE ='");
		    sbQuery.append(strType);
		    sbQuery.append("' ");
		    sbQuery.append(" AND T207.C207_SET_ID = T2080.C207_SET_ID");
		    sbQuery.append(" AND T2080.C2080_VOID_FL IS NULL");
		    sbQuery.append(" AND C207_VOID_FL IS NULL ");
		    sbQuery.append(" AND C207_OBSOLETE_FL IS NULL ");
		    sbQuery.append("AND t207.c207_set_id  = t207a.c207_link_set_id (+)");
		    if (!strStatus.equals("")) {
		      sbQuery.append(" AND C901_STATUS_ID IN ('");
		      sbQuery.append(strStatus);
		      sbQuery.append("')");
		    }

		    if (!strSearchSet.equals("")) {
		      sbQuery.append(" AND UPPER(C207_SET_NM) LIKE ('%");
		      sbQuery.append(strSearchSet.toUpperCase());
		      sbQuery.append("%')");
		    }

		    if (!strSystem.equals("0")) {
		      sbQuery.append(" AND C207_SET_SALES_SYSTEM_ID IN ('");
		      sbQuery.append(strSystem);
		      sbQuery.append("')");
		    }

		    if (!strHierarchy.equals("0")) {
		      sbQuery.append(" AND C901_HIERARCHY IN ('");
		      sbQuery.append(strHierarchy);
		      sbQuery.append("')");
		    }

		    if (!strCategory.equals("0")) {
		      sbQuery.append(" AND C207_CATEGORY IN ('");
		      sbQuery.append(strCategory);
		      sbQuery.append("')");
		    }

		    if (!strSetType.equals("0")) {
		      sbQuery.append(" AND C207_TYPE IN (");
		      sbQuery.append(strSetType);
		      sbQuery.append(")");
		    }

		    if (!strSetStatus.equals("0")) {
		      sbQuery.append(" AND C901_STATUS_ID IN ('");
		      sbQuery.append(strSetStatus);
		      sbQuery.append("')");
		    }


		    sbQuery.append(" AND T2080.C1900_COMPANY_ID = '");
		    sbQuery.append(strCompanyId);
		    sbQuery.append("'");
		    sbQuery.append(" ORDER BY ");


		    if (strGroupPartMapping.equals("GROUPPARTMAPPING")) {
		      sbQuery.append(" UPPER(C207_SET_NM), ");
		    }

		    sbQuery.append(" T207.C207_SET_ID");
		    log.debug(" Query for loading set map " + sbQuery.toString());
		   

		    return sbQuery.toString();
		    
		    
		  } // End of loadSetMap
	/**
	 * loadSurgeonNameWId - This method to fetch the NPI list and Surgeon name list
	 * 
	 * @param hmParam
	 * @return String
	 * @throws AppError
	 */
		public String loadSurgeonNameWId(HashMap hmParam) throws AppError {
			GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
			gmCacheManager.setKey(GmCommonClass.parseNull((String) hmParam.get("KEY")));
			gmCacheManager.setSearchPrefix(GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY")));
			gmCacheManager.setMaxFetchCount((Integer) hmParam.get("MAX_CNT"));
			String strReturn = gmCacheManager.returnJsonString(getLoadSurgeonNameWIdQueryStr(hmParam));
			return strReturn;
	  }
	/**
	  * getLoadSurgeonNameWIdQueryStr - This method to fetch the NPI list and Surgeon name list
	  * 
	  * @param hmParam
	  * @return String
	  * @throws AppError
	  */
		public String getLoadSurgeonNameWIdQueryStr(HashMap hmParam) throws AppError {
			StringBuffer strQuery = new StringBuffer();
			String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
			if (strOpt.equals("npilist")) {
				strQuery.append(" SELECT v6600.c6600_surgeon_npi_id NAME, ");
				strQuery.append(" v6600.partynm ||'#'|| v6600.c101_party_id ID ");
			} else {
				strQuery.append(" SELECT v6600.c6600_surgeon_npi_id ||'#'|| v6600.c101_party_id ID, ");
				strQuery.append(" v6600.partynm NAME");
			}
			strQuery.append(" FROM V6600_PARTY_SURGEON v6600 ");
			log.debug(" getLoadSurgeonNameQueryStr ==> " + strQuery.toString());
			return strQuery.toString();
		}
		
		/**PC-4792 - Inhouse Transaction - Quarantine - Inhouse
		 * loadEmployeeList -This method used to load employee list  
		 * @param hmParam
		 * @return strReturn
		 * @throws AppError
		 */
		public String loadEmployeeList(HashMap hmParam) throws AppError {
		  GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		    String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		    String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		    String striMaxCount = GmCommonClass.parseNull((String) hmParam.get("STRMAXCOUNT"));
		    String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		    log.debug("hmParam-------     >>   "+hmParam);
		    int iMaxCount = Integer.parseInt(striMaxCount);
		    gmCacheManager.setKey(strKey);
		    gmCacheManager.setSearchPrefix(searchPrefix);
		    gmCacheManager.setMaxFetchCount(iMaxCount);
		    gmCacheManager.setStrSearchType(GmCommonClass.parseNull((String) hmParam.get("SEARCHTYPE")));
		    String strReturn = gmCacheManager.returnJsonString(loadEmployeeListQuery(hmParam));
		    log.debug("strReturn-------     >>   "+strReturn);
		    return strReturn;
		}
      
		/**PC-4792 - Inhouse Transaction - Quarantine - Inhouse
		 * loadEmployeeList -This method used to load employee list
		 * 
		 * @param hmParam
		 * @return String
		 */
		public String loadEmployeeListQueryStr(HashMap hmParam) {
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		      StringBuffer sbQuery = new StringBuffer();
		      // Added CompanyId for getting EmployeeList depending on company id drop down valu
		      sbQuery
		          .append(" SELECT	t101.C101_USER_F_NAME ||' '||t101.C101_USER_L_NAME NAME, t101.C101_USER_ID ID ,t101.C101_PARTY_ID  PARTYID ");
		      sbQuery.append(" ,C101_USER_F_NAME ||' '||C101_USER_L_NAME CODENM, C101_USER_ID CODEID  ");
		      sbQuery.append(" FROM T101_USER t101 ");
		      sbQuery.append("  , T1019_PARTY_COMPANY_MAP t1019 ");
		      sbQuery.append("WHERE t1019.C101_PARTY_ID = t101.C101_PARTY_ID");
		      sbQuery.append(" AND  t1019.C1900_COMPANY_ID =" + getCompId());
		      sbQuery.append("AND t101.C901_USER_STATUS IN (311) ");
		      sbQuery.append(" ORDER BY C101_USER_F_NAME ");
		      log.debug("sbQuery.toString()-------     >>   "+sbQuery.toString());
		      return sbQuery.toString();
		}
		/**PC-4792 - Inhouse Transaction - Quarantine - Inhouse
		 * loadEmployeeListQuery -This method used to load the shipping section employee list
		 * 
		 * @param hmParam
		 * @return String
		 */
		public String loadEmployeeListQuery(HashMap hmParam) {
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

			String strUsrCode = GmCommonClass.parseNull((String) hmParam.get("USERCODE"));
			String strDept = GmCommonClass.parseNull((String) hmParam.get("DEPT"));
			String strUserSts = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
			// The following code added for Getting the Employee List based on the PLANT too
			String strEmpFilter = GmCommonClass.parseNull((String) hmParam.get("EMPFILTER"));
			String strEmployeeList = GmCommonClass.parseNull((String) hmParam.get("EMPLOYEELIST"));
			String strInactiveUser = GmCommonClass.parseNull((String) hmParam.get("INACTIVE_USER"));

			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append(
					" SELECT	t101.C101_USER_F_NAME ||' '||t101.C101_USER_L_NAME NAME, t101.C101_USER_ID ID  ,t101.C101_PARTY_ID  PARTYID ");
			sbQuery.append(" FROM T101_USER t101 ");
			sbQuery.append("  , T1019_PARTY_COMPANY_MAP t1019 ");
			sbQuery.append("WHERE t1019.C101_PARTY_ID = t101.C101_PARTY_ID");
			sbQuery.append(" AND t101.C901_USER_TYPE IN (");
			sbQuery.append(strUsrCode);
			sbQuery.append(") ");
			// The following code added for Getting the Employee List based on the PLANT too
			// if the
			// Selected Company is EDC
			if (strEmpFilter.equals("COMPANY-PLANT")) {
				sbQuery.append(
						" AND  t1019.C1900_COMPANY_ID IN ( SELECT C1900_COMPANY_ID  FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID = "
								+ getCompPlantId() + "  AND C5041_VOID_FL IS NULL )");
			} else if (strEmployeeList.equalsIgnoreCase("ALLEMPLOYEE")) {
				sbQuery.append(
						" AND  t1019.C1900_COMPANY_ID  IN (SELECT C1900_COMPANY_ID FROM T1900_COMPANY WHERE C1900_VOID_FL IS NULL");
				sbQuery.append(") ");
			} else {
				sbQuery.append(" AND  t1019.C1900_COMPANY_ID = " + getCompId());
				if (!strInactiveUser.equals("Y")) {
					sbQuery.append(" AND  t1019.C1019_VOID_FL IS NULL");
				}
			}
			// Code Commented on 11/20 to avoid using of C101_DEPT_ID
			// Instead c901_Dept_id should be used.
			// if (!strDept.equals(""))
			// {
			// sbQuery.append(" AND C101_DEPT_ID IN ('");
			// sbQuery.append(strDept);
			// sbQuery.append("')");
			// }

			if (!strDept.equals("")) {
				sbQuery.append(" AND t101.C901_DEPT_ID IN (");
				sbQuery.append("  SELECT C901_CODE_ID FROM T901_CODE_LOOKUP WHERE C901_CODE_NM IN ('");
				sbQuery.append(strDept);
				sbQuery.append("')AND C901_CODE_GRP='DEPID') ");
			}
			if (!strUserSts.equals("")) {
				sbQuery.append(" AND t101.C901_USER_STATUS IN ( ");
				sbQuery.append(strUserSts);
				sbQuery.append(") ");
			}
			if (strEmployeeList.equalsIgnoreCase("ALLEMPLOYEE")) {
				sbQuery.append(
						" GROUP BY t101.C101_USER_F_NAME, t101.C101_USER_L_NAME, t101.C101_USER_ID, t101.C101_PARTY_ID ");
			}
			sbQuery.append(" ORDER BY t101.C101_USER_F_NAME ");
			log.debug("Query for getting Employee List " + sbQuery.toString());
			return sbQuery.toString();
		}
}

