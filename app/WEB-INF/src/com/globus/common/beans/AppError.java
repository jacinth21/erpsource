/**************************************************************************************************
 * File							: AppError.java
 * Screen ID					: Error Screen
 * Desc							: This is AppError Class For catching Exceptions
 * Version						: 1.0
 **************************************************************************************************/

package com.globus.common.beans;

//JAVA Classes
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.globus.webservice.common.exception.GmApplicationRuntimeException;

/**
 * This is a <i>user defined exception </i> class which will be used
 * in the SAM application. This class gets the approprite
 * custom error messages from the Resource Bundles.
 * The types of errors are classified into
  <BLOCKQUOTE><pre>
  1. DataBase related error messages
  2. Custom error messages(Business Logic errors)
  3. Unhandled error messages(No custom message)
  </pre></BLOCKQUOTE>
 * DBErr serves as the resource bundle for Database
 * related error messages. AppErr servers as the resource
 * bundle for other custom error messages
 */

public class AppError extends GmApplicationRuntimeException implements Serializable
{
    
    /**
     * This constructor is called for database error messages,
     * AppError objects that are rethrown, unhandled error messages
     * <br><b>Usage</b> <BLOCKQUOTE><pre>
     *      catch(SQLException ex)
     *      {
     *              throw new AppError(ex);
     *      }
     * </pre></BLOCKQUOTE>
     *
     * @param       ex      The exception which is to be interpreted
     */
    public AppError(Exception ex)
    {
    	super(ex);
    	exp = ex;
       
    }//END of AppError constructor
    
    /**
     * This constructor is called for custom error messages
     * <br><b>Usage </b><BLOCKQUOTE><pre>
     *      if (condition)
     *      {
     *              throw new AppError(AppError.APPLICATION, "1000")
     *      }
     * <pre></BLOCKQUOTE>
     *
     * @param       astrContext     The context(module in the project) in which the error occured
     *                                                      This is a 3 character pubic static constant defined
     * @param       astrMsgId               The 4 charater message id for the custom error message
     */
    public AppError(String astrContext, String astrMsgId)

    {
    	super(astrContext,astrMsgId);
        datDateTime = new Date();
        strContext = astrContext;
        strMsgId = astrMsgId;
        try
        {
        	strMsgText = astrMsgId + "^" + rbApp.getString(astrContext + astrMsgId);
		}
		catch(Exception e)
		{
			strMsgText = strMsgId + "^" + "<p></p><p></p>"+rbApp.getString("dbError");
		}
    }//END of AppError constructor

    /**
     * This constructor is called for custom error messages
     * <br><b>Usage </b><BLOCKQUOTE><pre>
     *      if (condition)
     *      {
     *              throw new AppError(AppError.APPLICATION, "1000", 'Type')
     *      }
     * <pre></BLOCKQUOTE>
     *
     * @param       astrContext     The context(module in the project) in which the error occured
     *                                                      This is a 3 character pubic static constant defined
     * @param       astrMsgId               The 4 charater message id for the custom error message
     * 
     * * @param       CType               The 1 charater error Type 'E' for Error 'C' for Success
     */ 
    public AppError(String astrContext, String astrMsgId, char CLType)

    {
    	super(astrContext,astrMsgId,CLType);
    	datDateTime = new Date();
        strContext = astrContext;
        strMsgId = astrMsgId;
        cType = String.valueOf(CLType);
        
        try
        {
            strMsgText =   rbApp.getString(astrMsgId);
        }
        catch(Exception e)
        {
            strMsgText =   astrContext ;
        }
        
    }//END of AppError constructor
    
    /**
     * This constructor is called for custom error messages
     * that need to be appended by data specific information
     * <br><b>Usage </b><BLOCKQUOTE><pre>
     *      if (condition)
     *      {
     *              throw new AppError(AppError.APPLICATION, "1000", "Customer No 100")
     *      }
     * <pre></BLOCKQUOTE>
     *
     * @param       astrContext     The context(module in the project) in which the error occured
     *                              This is a 3 character pubic static constant defined
     * @param       astrMsgId               The 4 charater message id for the custom error message
     * @param       astrCustomise   The data specific part of the error message to be displayed
     */
    public AppError(String astrContext, String astrMsgId, String astrCustomise)
    {
        this(astrContext, astrMsgId);
        if ((astrCustomise != null) & (!astrCustomise.equals("")))
        {
            strMsgText += astrCustomise;
        }

    }//END of AppError constructor

    /**
     * This constructor is called when database error messages have to
     * be customised and need to be appended by data specific information
     * <br><b>Usage </b><BLOCKQUOTE><pre>
     *      if (Exception e)
     *      {
     *              throw new AppError(AppError.APPLICATION, "1000", "Customer No 100", e)
     *      }
     * <pre></BLOCKQUOTE>
     *
     * @param       astrContext     The context(module in the project) in which the error occured
     *                                                      This is a 3 character pubic static constant defined
     * @param       astrMsgId               The 4 charater message id for the custom error message
     * @param       astrCustomise   The data specific part of the error message to be displayed
     * @param       ex                              The exception which is to be interpreted
     */
    public AppError(String astrContext,
                    String astrMsgId, String astrCustomise, Exception ex)
    {
        this(ex);
        datDateTime = new Date();
        strContext = strContext;
        strMsgId = astrMsgId;
        strMsgText = rbApp.getString(astrContext + astrMsgId);
        if ((astrCustomise != null) & (!astrCustomise.equals("")))
        {
            strMsgText += astrCustomise;
        }

    }//END of AppError constructor

    /**
     * Returns the message ID associated to the error
     *
     * @return For custom error messages : The Message ID associated
     * with the message<br>
     * For other error messages : Empty String
     */
    public String getMessageId()
    {
        return strMsgId;
    }

    /**
     * set the message text associated to the error
     *
     * @return For Database error messages : The message text in the DBError properties file<br>
     * For custom error messages: The message text in the AppError properties file
     * along with data specific information in any<br>
     * For other error messages: The toString() version of the exception occured<br>
     */
    public void setStrMsgText(String strMessage)
    {
        strMsgText = strMessage;
    }

    /**
     * Returns the message text associated to the error
     *
     * @return For Database error messages : The message text in the DBError properties file<br>
     * For custom error messages: The message text in the AppError properties file
     * along with data specific information in any<br>
     * For other error messages: The toString() version of the exception occured<br>
     */
    public String getMessage()
    {
        return strMsgText;
    }

    /**
     * Returns the message text associated to the error
     *
     * @return For Database error messages : The message text in the DBError properties file<br>
     * For custom error messages: The message text in the AppError properties file
     * along with data specific information in any<br>
     * For other error messages: The toString() version of the exception occured<br>
     */
    public String getStrMsgText()
    {
        return strMsgText;
    }

    /**
     * The timeStamp at which the error occured
     */
    public Date getTimeStamp()
    {
        return datDateTime;
    }

    /**
     * The context(module) in which the error occured
     */
    public String getContext()
    {
        return strContext;
    }

    /**
     * The context(module) in which the error occured
     */
    public String getType()
    {
        return cType;
    }
    
    /**
     * Gets the actual Exception object
     */
    public Exception getException()
    {
        return exp;
    }

   
}//END of class AppError
