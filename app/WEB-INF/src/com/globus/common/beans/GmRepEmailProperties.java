package com.globus.common.beans;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.db.GmDBManager;

public class GmRepEmailProperties extends GmEmailProperties {
 
 private String strRepId;
 private String strJobName;

	public GmRepEmailProperties(String strRepId, String strJobName) throws AppError
	{
		this.strRepId =GmCommonClass.parseNull(strRepId);
		this.strJobName = GmCommonClass.parseNull(strJobName);
		
		if(!this.strRepId.equals("") && !this.strJobName.equals(""))
		{
			super.setCc(getCCfromContact());
		}
	}
	
	private String getCCfromContact() throws AppError
	{
		StringBuffer strCC = new StringBuffer();	
		String strReturnString = "";
		
	    GmDBManager gmDBManager = new GmDBManager();
	    gmDBManager.setPrepareString("gm_pkg_common.gm_fch_cc_from_contact", 3);
	    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
	    gmDBManager.setString(1, strRepId);
	    gmDBManager.setString(2, strJobName);
	    gmDBManager.execute();
	    strCC.append(GmCommonClass.parseNull(gmDBManager.getString(3)));	    
	    gmDBManager.close();	   
	    
	    if(strCC.toString() != null)
	    {
	    if(strCC.toString().startsWith(","))
	    	strCC.deleteCharAt(strCC.toString().indexOf(","));
	    if(strCC.toString().endsWith(","))
	    	strCC.deleteCharAt(strCC.toString().lastIndexOf(","));	    	    
	    strReturnString =  strCC.toString();
	    }
	  
	    return strReturnString;
	}
}
