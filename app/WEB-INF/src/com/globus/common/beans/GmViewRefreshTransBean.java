package com.globus.common.beans;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmViewRefreshTransBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public GmViewRefreshTransBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmViewRefreshTransBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }


  /**
   * This method used to refresh the materialize view
   * 
   * @param strViewName
   * @throws AppError
   */
  public void updateMaterializeView(String strViewName) throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_view_refresh.gm_refresh_materialize_view", 1);
    gmDBManager.setString(1, strViewName);
    gmDBManager.execute();
    gmDBManager.close();
    log.debug(" *** Materialize View refresh done *** ");
  }
}
