/**
 * 
 */
package com.globus.common.beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.beanutils.Converter;

/**
 * @author rshah
 *
 */
public class GmDateConverter implements Converter{
	private SimpleDateFormat dateFormat;
	  public GmDateConverter(String datePattern) {
	    dateFormat = new SimpleDateFormat(datePattern);
	    dateFormat.setLenient(false);
	  }
	  public Object convert(Class type, Object value) {
	    try 
	    {	    	
	    	if(!((String)value).equals(""))
	    	{
	    		return dateFormat.parse((String) value);
	    	}
	    }
	    catch (ParseException e) 
	    {
	    	e.printStackTrace();
	    }
	    return null;
	  }

}
