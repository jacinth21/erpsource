package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.jms.consumers.beans.GmInitiateVendorJMS;
import com.globus.operations.logistics.beans.GmKitMappingBean;
import com.globus.operations.returns.beans.GmReturnReportBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmCommonCancelBean extends GmBean {

  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.


  // this will be removed all place changed with GmDataStoreVO constructor

  public GmCommonCancelBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmCommonCancelBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * UpdateCancel - This method will be used to save rollback information. It would call the
   * Callforwarding package gm_pkg_common_cancel. gm_cm_sav_cancelrow which handles 1. Rollback 2.
   * Void 3. Delete transactions
   * 
   * @param HashMap hmValue
   * 
   * @return void
   * @exception AppError
   **/
  public void UpdateCancel(HashMap hmValue) throws AppError {
    GmCommonClass gmCommon = new GmCommonClass();
    GmInitiateVendorJMS gmInitiateVendorJMS = new GmInitiateVendorJMS(getGmDataStoreVO());
  	HashMap hmEmailParam = new HashMap();
    String strJNDIConnection = GmCommonClass.parseNull((String) hmValue.get("JNDI"));
    GmDBManager gmDBManagerDM = null;
    GmDBManager gmDBManager = null;
    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
    HashMap hmParam = new HashMap();

    if (strJNDIConnection.equals("DMJNDI")) {
      gmDBManager = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS, getGmDataStoreVO());
    } else {
      gmDBManager = new GmDBManager(getGmDataStoreVO());
    }

    String strTxnId = GmCommonClass.parseNull((String) hmValue.get("TXNID"));
    String strTxnName = GmCommonClass.parseNull((String) hmValue.get("TXNNAME"));
    String strCancelReason = GmCommonClass.parseZero((String) hmValue.get("CANCELREASON"));
    String strCancelType = GmCommonClass.parseNull((String) hmValue.get("CANCELTYPE"));
    String strComments = GmCommonClass.parseNull((String) hmValue.get("COMMENTS"));
    String strUserId = GmCommonClass.parseNull((String) hmValue.get("USERID"));
    String strCancelReturnVal = GmCommonClass.parseNull((String) hmValue.get("CANCELRETURNVAL"));
    // in DO hold (via New order screen) at the time no need to show the Success message.
    String strShowSuccessMsg = GmCommonClass.parseNull((String) hmValue.get("SHOWSUCCESSMSG"));
    strShowSuccessMsg = strShowSuccessMsg.equals("") ? "Y" : strShowSuccessMsg;
    String strCountryCode = GmCommonClass.parseNull(GmCommonClass.countryCode);
    String strTxnValue = strTxnId;
    String strRecMode = "";
    String strConsumerClass = "";
    String strCompanyInfo=GmCommonClass.parseNull((String) hmValue.get("COMPANYINFO"));
    // Below method is to get the receive mode of order
    strRecMode = GmCommonClass.parseNull(fetchOrderReceiveMode(strTxnId));

    if (!strTxnName.equals("")) {
      strTxnValue = strTxnName;
    }

    log.debug("strTxnValue " + strTxnValue + "strCountryCode ***** " + strCountryCode);
    String strPrepareString = null;

    // if the void screen returns a out parameter for custom message then GM_CM_SAV_CANCELROW_RETN
    // prc will be called instead.
    // and the user defined message will be displayed on the screen instead of the usual message
    if(strCancelType.equals("RLBPIC")){
    	 gmDBManager.setPrepareString("gm_pkg_op_set_build.gm_rollback_pendingpick_set",2);
    	 gmDBManager.setString(1, strTxnId);
    	 gmDBManager.setInt(2, Integer.parseInt(GmCommonClass.parseZero(strUserId)));
    	 gmDBManager.execute();
    }
    else{
    	if (strCancelReturnVal.equalsIgnoreCase("true")) {
    	      gmDBManager.setPrepareString("GM_PKG_COMMON_CANCEL.GM_CM_SAV_CANCELROW_RETN", 6);
    	      gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
    	    }
    	    else {
    	      gmDBManager.setPrepareString("GM_PKG_COMMON_CANCEL.GM_CM_SAV_CANCELROW", 5);
    	    }
   
    
    /*
     * register out parameter and set input parameters
     */
    gmDBManager.setString(1, strTxnId);
    gmDBManager.setInt(2, Integer.parseInt(GmCommonClass.parseZero(strCancelReason)));
    gmDBManager.setString(3, strCancelType);
    gmDBManager.setString(4, strComments);
    gmDBManager.setInt(5, Integer.parseInt(GmCommonClass.parseZero(strUserId)));
    // log.debug("Before executing void query Values : "+strTxnId+" : "+
    // Integer.parseInt(GmCommonClass.parseZero(strCancelReason)) + " : " + strCancelType+ " : "+
    // strComments+ " : "+ Integer.parseInt(GmCommonClass.parseZero(strUserId)));

    gmDBManager.execute();
    }
    // log.debug("After executing void query");

    String strCancelTypeNM = GmCommonClass.parseNull(gmCommon.getCodeNameFromAlt(strCancelType));
    strCancelTypeNM =
        strCancelTypeNM.concat(" for <U><I>").concat(strTxnValue).concat("</I></U> performed ");
    if (strCancelReturnVal.equalsIgnoreCase("true")) {

      // If the void type is VDACK, we need to display the custom message based on the return value
      // from the procedure
      if (strCancelType.equals("VDACK") || strCancelType.equals("VSBMAP")) {
        strCancelTypeNM = GmCommonClass.parseNull(gmDBManager.getString(6));
      } else {
        strCancelTypeNM =
            strCancelTypeNM.concat("<br>" + GmCommonClass.parseNull(gmDBManager.getString(6)));
      }
      log.debug("strCancelTypeNM " + strCancelTypeNM);
    }

    // When a Request is voided in US DB,We should void the request in the GOP DB as well
    if (strCancelType.equals("VDREQ") && !strJNDIConnection.equals("DMJNDI")
        && strCountryCode.equals("en")) {
      gmDBManagerDM = new GmDBManager(GmDNSNamesEnum.DMWEBGLOBUS);
      log.debug("Connecting to GOP DB ");
      gmDBManagerDM.setPrepareString("gm_pkg_oppr_inventory_update.gm_op_sav_tpr_clear_request", 5);

      gmDBManagerDM.setString(1, strTxnId);
      gmDBManagerDM.setString(2, "void");
      gmDBManagerDM.setString(3, strUserId);
      gmDBManagerDM.setString(4, "");
      gmDBManagerDM.setString(5, "");
      gmDBManagerDM.execute();
    }
    gmDBManager.commit();

    if (gmDBManagerDM != null) {
      gmDBManagerDM.commit();
    }
    // If an order is voiding an it is created from IPAD, then need to send push notification to the
    // sales rep
    if (strCancelType.equals("VDRES")) {
      log.debug("strRecMode>>" + strRecMode);
      if (strRecMode.equals("26230683")) {// 26230683: IPAD
        strConsumerClass =
            GmCommonClass.parseNull(GmCommonClass.getString("NOTIFICATION_CONSUMER_CLASS"));
        String strQueueName = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_NOTIFICATION_RMT_QUEUE"));
        hmParam.put("REFID", strTxnId);
        hmParam.put(
            "MESSAGE",
            "DO# " + strTxnId + " voided by C/S with Reason as "
                + GmCommonClass.parseNull((GmCommonClass.getCodeNameFromCodeId(strCancelReason))));
        hmParam.put("NOTIFICATION", "ORDER");        
        // This is JMS Code which will process the notification, jms/gmnotificationqueue is the JMS
        // queue using for notification
        GmQueueProducer qprod = new GmQueueProducer();
        GmMessageTransferObject tf = new GmMessageTransferObject();
        tf.setMessageObject(hmParam);
        tf.setConsumerClass(strConsumerClass);
        qprod.sendMessage(tf, strQueueName);
      }
    }
    log.debug("notification sent>");

    // If the cancel type is VDACK and if the return value of procedure is 'N', then throw error
    // message instead of success message
    if (strCancelReturnVal.equalsIgnoreCase("true") && strCancelType.equals("VDACK")
        && strCancelTypeNM.equals("N")) {
      strCancelTypeNM =
          "All the parts in the Acknowledgment order " + strTxnId
              + " is released to BBA order, cannot void it";
      throw new AppError(strCancelTypeNM, "", 'E');
    }
    //if the cancel type is VDDMS then Call Jms
    if(strCancelType.equals("VDDMS")){
    // jms call - Demand Sheet
    GmAutoCompleteTransBean gmAutoCompleteTransBean =new GmAutoCompleteTransBean(GmCommonClass.getDefaultGmDataStoreVO());
    HashMap  hmCacheUpd = new HashMap();
    hmCacheUpd.put("ID", strTxnId);
    hmCacheUpd.put("OLD_NM", "");
    hmCacheUpd.put("NEW_NM", "Glo");
    hmCacheUpd.put("METHOD", "DemandSheet");
    hmCacheUpd.put("companyInfo",strCompanyInfo);
    gmAutoCompleteTransBean.saveDetailsToCache(hmCacheUpd);
    }
    //if the cancel type is VDTTP then Call Jms
    if(strCancelType.equals("VDTTP") ){
    // jms call - TTP Name List
    GmAutoCompleteTransBean gmAutoCompleteTransBean =new GmAutoCompleteTransBean(GmCommonClass.getDefaultGmDataStoreVO());
    HashMap  hmCacheUpd = new HashMap();
    hmCacheUpd.put("ID", strTxnId);
    hmCacheUpd.put("OLD_NM", "");
    hmCacheUpd.put("NEW_NM", "Glo");
    hmCacheUpd.put("METHOD", "synTTPList");
    hmCacheUpd.put("companyInfo",strCompanyInfo);
    gmAutoCompleteTransBean.saveDetailsToCache(hmCacheUpd);
    }
    if(strCancelType.equals("VDGRP") ){
        // jms call - Group List Remove
        GmAutoCompleteTransBean gmAutoCompleteTransBean =new GmAutoCompleteTransBean(GmCommonClass.getDefaultGmDataStoreVO());
        HashMap  hmCacheUpd = new HashMap();
        hmCacheUpd.put("ID", strTxnId);
        hmCacheUpd.put("OLD_NM", "");
        hmCacheUpd.put("NEW_NM", "Glo");
        hmCacheUpd.put("METHOD", "synGroupList");
        hmCacheUpd.put("companyInfo",strCompanyInfo);
        gmAutoCompleteTransBean.saveDetailsToCache(hmCacheUpd);
        }
    
    //vendor po save and mail
    
    if (strCancelType.equals("VDPPO")) {
  	  
  	        hmEmailParam.put("REFID", strTxnId);
	        hmEmailParam.put("REFTYPE", "108440");//potype
	        hmEmailParam.put("REFACTION", "26240621");//PO ACTION
	        hmEmailParam.put("MAILSTATUS", "0"); //to send instant mail
	        hmEmailParam.put("USERID", strUserId);
	        hmEmailParam.put("COMPANYINFO", strCompanyInfo);
	        gmInitiateVendorJMS.initVendorEmailJMS(hmEmailParam);
	        log.debug("hmReturn-==========PO VOID========= "+hmEmailParam);
    }
    /*if (strCancelType.equals("VDWO")) {
	    
	    	hmEmailParam.put("REFID", strTxnId);
	    	hmEmailParam.put("REFTYPE", "108441");//WOTYPE
	    	hmEmailParam.put("REFACTION", "26240622");//WO ACTION
	       	hmEmailParam.put("USERID", strUserId);
	       	hmEmailParam.put("MAILSTATUS", "0"); //to send instant mail
	       	hmEmailParam.put("COMPANYINFO", strCompanyInfo);
	       	gmInitiateVendorJMS.initVendorEmailJMS(hmEmailParam);
	       	log.debug("hmReturn-==========WO VOID========= "+hmEmailParam);
    
    }*/
    
    if(strCancelType.equals("CRBTN")){
        // //To Update redis key to show add set field (PMT-35098)
    	GmReturnReportBean gmReturnReportBean = new GmReturnReportBean(getGmDataStoreVO());
    	HashMap hmMap = gmReturnReportBean.loadReturnHeaderInfo(strTxnValue);
        GmKitMappingBean gmKitMappingBean = new GmKitMappingBean(getGmDataStoreVO());
        log.debug("DISTID---->"+hmMap.get("DISTID"));
        gmKitMappingBean.removeSetKey(GmCommonClass.parseNull((String) hmMap.get("DISTID")));
    }
    if (strShowSuccessMsg.equals("Y")) {
      throw new AppError(strCancelTypeNM, "", 'S');
    }

  } // End of UpdateCancel

  /**
   * viewCancelInfo - This method will be used to fetch cancel information for cancel report It
   * would call the Callforwarding package gm_pkg_common_cancel. gm_cs_cancel_loginfo
   * 
   * @param HashMap hmValue
   * 
   * @return RowSetDynaClass
   * @exception AppError
   **/

  public ArrayList viewCancelInfo(HashMap hmValue) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    String strTxnId = GmCommonClass.parseNull((String) hmValue.get("TXNID"));
    String strFromDate = GmCommonClass.parseNull((String) hmValue.get("FROMDATE"));
    String strToDate = GmCommonClass.parseNull((String) hmValue.get("TODATE"));
    String strCancelTypeFromMenu =
        GmCommonClass.parseNull((String) hmValue.get("CANCELTYPEFROMMENU"));
    String strCancelReason = GmCommonClass.parseNull((String) hmValue.get("CANCELREASON"));
    String strCancelUser = GmCommonClass.parseNull((String) hmValue.get("CANCELUSER"));
    strCancelUser = strCancelUser.equals("0") ? "" : strCancelUser;

    String strCancelComment = GmCommonClass.parseNull((String) hmValue.get("CANCELCOMMENT"));

    strCancelReason = strCancelReason.equals("0") ? "" : strCancelReason;

    ResultSet rs = null;
    ArrayList alReturn = null;

    gmDBManager.setPrepareString("GM_PKG_COMMON_CANCEL.GM_CS_CANCEL_LOGINFO", 8);

    /*
     * register out parameter and set input parameters
     */

    gmDBManager.setString(1, strTxnId);
    gmDBManager.setString(2, strFromDate);
    gmDBManager.setString(3, strToDate);
    gmDBManager.setString(4, strCancelTypeFromMenu);
    gmDBManager.setString(5, strCancelReason);
    gmDBManager.setString(6, strCancelUser);
    gmDBManager.setString(7, strCancelComment);
    gmDBManager.registerOutParameter(8, OracleTypes.CURSOR);
    gmDBManager.execute();

    rs = (ResultSet) gmDBManager.getObject(8);
    alReturn = gmDBManager.returnArrayList(rs);
    gmDBManager.close();
    log.debug(" Size of ArrayList with cancel log info " + alReturn.size());


    return alReturn;
  } // End of methodName

  // This method is to fetch the receive mode of order
  public String fetchOrderReceiveMode(String strTxnId) {
    String strRecMode = "";
    GmDBManager gmManager = new GmDBManager(getGmDataStoreVO());
    gmManager.setFunctionString("get_order_receive_mode", 1);
    gmManager.registerOutParameter(1, OracleTypes.NUMBER);
    gmManager.setString(2, strTxnId);
    gmManager.execute();
    strRecMode = gmManager.getString(1);
    gmManager.close();
    return strRecMode;
  }
}
