package com.globus.common.beans;


import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCompanyReportBean extends GmBean{

Logger log = GmLogger.getInstance(this.getClass().getName());
	
		
	public GmCompanyReportBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmCompanyReportBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }

	
	
	public String fetchCompanyDtlsFromOrder(String strRefId)  throws AppError{
			String strCmpnyDtls = "";
			GmDBManager gmDBManager = new GmDBManager();
			gmDBManager.setPrepareString("gm_pkg_cm_company_rpt.gm_fch_company_dtls_from_order", 2);
			gmDBManager.setString(1, strRefId);
			gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
			gmDBManager.execute();
			strCmpnyDtls = GmCommonClass.parseNull(gmDBManager.getString(2));
			gmDBManager.close();
			return strCmpnyDtls;
	}
	
    public String fetchCompanyDtlsFromInvoice(String strRefId) throws AppError{
			String strCmpnyDtls = "";
			GmDBManager gmDBManager = new GmDBManager();
			gmDBManager.setPrepareString("gm_pkg_cm_company_rpt.gm_fch_company_dtls_from_invoice", 2);
			gmDBManager.setString(1, strRefId);
			gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
			gmDBManager.execute();
			strCmpnyDtls = GmCommonClass.parseNull(gmDBManager.getString(2));
			gmDBManager.close();
			return strCmpnyDtls;
	}
	
	
}
