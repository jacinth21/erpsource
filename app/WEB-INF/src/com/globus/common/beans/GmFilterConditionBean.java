package com.globus.common.beans;

import java.util.HashMap;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;

public class GmFilterConditionBean extends GmBean {

	
	public GmFilterConditionBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmFilterConditionBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	
	
	public String getListDemandSheetFilter(HashMap hmparam) throws AppError{
		String strAccessFilter = GmCommonClass.parseNull((String)hmparam.get("ACCESS_FILTER"));
		StringBuffer sbQuery = new StringBuffer();
		try{
			if(strAccessFilter.length()>0){
				sbQuery.append("AND  t4020.C4020_DEMAND_MASTER_ID IN (  ");
				sbQuery.append("SELECT T4021.C4020_DEMAND_MASTER_ID FROM T4021_DEMAND_MAPPING T4021,  ");
				sbQuery.append("(SELECT DISTINCT REGION_ID, REGION_NAME FROM V700_TERRITORY_MAPPING_DETAIL V700 ");
				sbQuery.append("WHERE "+strAccessFilter+")  V700	 ");
				sbQuery.append("WHERE T4021.C901_REF_TYPE = 40032 "); // ID Corresponds to Region
				sbQuery.append("AND T4021.C4021_REF_ID = V700.REGION_ID ");
				sbQuery.append(") ");
			}
			
		}catch(Exception e){
			throw new AppError(e);
		}
		return sbQuery.toString();
	}
}
