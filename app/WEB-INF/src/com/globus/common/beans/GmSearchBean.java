package com.globus.common.beans;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSearchBean extends GmBean {
  // GmCommonClass gmCommon = new GmCommonClass();
  GmLogger log = GmLogger.getInstance(); // Gets an instance of Logger class

  // this will be removed all place changed with Data Store VO constructor
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSearchBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmSearchBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }


  /**
   * loadPartSearchReport - This method will search Part Details
   * 
   * @param hmPartParam. This HashMap will contain the following String values String strUsername
   *        String strPassword
   * @return HashMap
   * @exception AppError
   **/
  public HashMap loadPartSearchReport(HashMap hmPartParam) throws AppError {
    HashMap hmReturn = new HashMap();
    ArrayList alReturn = new ArrayList();

    String strPartNum = GmCommonClass.parseNull((String) hmPartParam.get("PARTNUM"));
    String strDesc = GmCommonClass.parseNull((String) hmPartParam.get("PARTDESC"));
    String strSubCompFl = GmCommonClass.parseNull((String) hmPartParam.get("EXC_SUB_COMP"));

    try {
      GmDBManager dbCon = null;
      dbCon = new GmDBManager(getGmDataStoreVO());
      String strCompanyID = getCompId();
      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT T205.C205_PART_NUMBER_ID PNUM, C205_PART_NUM_DESC PDESC, 'Y' PARTRADBUTTON ");
      sbQuery.append(" FROM T205_PART_NUMBER T205 ,T2023_PART_COMPANY_MAPPING T2023");
      sbQuery.append(" WHERE  T205.C205_ACTIVE_FL IS NOT NULL ");
      sbQuery.append(" AND t205.C205_PART_NUMBER_ID = t2023.C205_PART_NUMBER_ID ");
      sbQuery.append(" AND t2023.C1900_COMPANY_ID = " + strCompanyID);
      sbQuery.append(" AND t2023.C2023_VOID_FL IS NULL ");

      if (!strPartNum.equals("")) {
        log.debug(" Part NUm is not NULL");
        sbQuery.append(" AND T205.C205_PART_NUMBER_ID like '%");
        sbQuery.append(strPartNum);
        sbQuery.append("%'");
      }

      if (!strDesc.equals("")) {
        log.debug(" Part Desc is not NULL ");
        sbQuery.append(" AND upper(T205.C205_PART_NUM_DESC) like upper('%");
        sbQuery.append(strDesc);
        sbQuery.append("%')");
      }

      if (strSubCompFl.equals("Y")) {
        sbQuery.append(" AND INSTR (C205_PART_NUMBER_ID, '.', 1, 2) = 0 ");

      }

      sbQuery.append("ORDER BY T205.C205_PART_NUMBER_ID");

      log.debug("Query inside loadPartSearchReport of  GmSearchBean is " + sbQuery.toString());

      // Only execute the code if below we have Part # and Part Desc
      if (!strPartNum.equals("") || !strDesc.equals("")) {
        alReturn = dbCon.queryMultipleRecords(sbQuery.toString());
      }

      hmReturn.put("SEARCHREPORT", alReturn);



    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadPartSearchReport", "Exception is:" + e);
    }
    return hmReturn;
  } // End of loadPartSearchReport


  /**
   * loadRepSearchReport - This method will search Rep Details
   * 
   * @param hmRepParam - This HashMap contains the following String values String strRepName String
   *        strRepNum String strRepPhNum
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadRepSearchReport(HashMap hmRepParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strRepName = GmCommonClass.parseNull((String) hmRepParam.get("REPNAME"));
    String strRepNum = GmCommonClass.parseNull((String) hmRepParam.get("REPNUM"));
    String strRepPhNum = GmCommonClass.parseNull((String) hmRepParam.get("REPPHNUM"));

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT  C703_SALES_REP_NAME SREPNAME, C703_SALES_REP_ID SREPID, C703_PHONE_NUMBER SREPPHNUM, 'X' REPRADBUTTON");
      sbQuery.append(" FROM T703_SALES_REP ");
      sbQuery.append(" WHERE  C703_SALES_REP_ID IS NOT NULL");
      sbQuery.append(" AND C703_VOID_FL IS NULL ");

      if (!strRepNum.equals("")) {
        log.debug("strRepNum is not NULL");
        sbQuery.append(" AND  C703_SALES_REP_ID like '%");
        sbQuery.append(strRepNum);
        sbQuery.append("%'");
      }

      if (!strRepName.equals("")) {
        log.debug(" strRepName is not NULL");
        sbQuery.append(" AND  upper(C703_SALES_REP_NAME) like upper ('%");
        sbQuery.append(strRepName);
        sbQuery.append("%')");
      }

      if (!strRepPhNum.equals("")) {
        log.debug("strRepPhNum is not NULL");
        sbQuery.append(" AND  C703_PHONE_NUMBER like '%");
        sbQuery.append(strRepPhNum);
        sbQuery.append("%'");
      }

      sbQuery.append(" ORDER BY C703_SALES_REP_ID ");

      System.out.println("Query inside loadRepSearchReport of  GmSearchBean is "
          + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadRepSearchReport", "Exception is:" + e);
    }
    return alReturn;
  } // End of loadPartSearchReport



  /**
   * loadAccSearchReport - This method will search Rep Details
   * 
   * @param hmAccParam - This HashMap contains the following String values String strAccName String
   *        strAccId String strAccCity
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadAccSearchReport(HashMap hmAccParam) throws AppError {
    ArrayList alReturn = new ArrayList();

    String strAccName = (String) hmAccParam.get("ACCNAME");
    String strAccId = (String) hmAccParam.get("ACCID");
    String strAccCity = (String) hmAccParam.get("ACCCITY");

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      StringBuffer sbQuery = new StringBuffer();
      sbQuery
          .append(" SELECT  C704_ACCOUNT_ID ACCID,C704_ACCOUNT_NM ACCNAME, C704_SHIP_CITY ACCCITY, 'X' ACCRADBUTTON ");
      sbQuery.append(" FROM T704_ACCOUNT ");
      sbQuery.append(" WHERE  C704_ACTIVE_FL IS NOT NULL");

      if (!strAccId.equals("")) {
        log.debug("strAccId is not NULL");
        sbQuery.append(" AND  C704_ACCOUNT_ID like '%");
        sbQuery.append(strAccId);
        sbQuery.append("%'");
      }

      if (!strAccName.equals("")) {
        log.debug(" strAccName is not NULL");
        sbQuery.append(" AND  upper(C704_ACCOUNT_NM) like upper ('%");
        sbQuery.append(strAccName);
        sbQuery.append("%')");
      }

      if (!strAccCity.equals("")) {
        log.debug(" strAccCity is not NULL");
        sbQuery.append(" AND  upper(C704_SHIP_CITY) like upper ('%");
        sbQuery.append(strAccCity);
        sbQuery.append("%')");
      }

      sbQuery.append(" ORDER BY C704_ACCOUNT_ID ");

      log.debug("Query inside loadAccSearchReport of  GmSearchBean is " + sbQuery.toString());
      alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:loadAccSearchReport", "Exception is:" + e);
    }
    return alReturn;
  } // End of loadPartSearchReport


}