/**
 * This class contains methods that are common to all components
 * 
 * @author $Author:$
 * @version $Revision:$ <br>
 *          $Date:$
 */

package com.globus.common.beans;

// Java Classes
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
/*import java.net.InetAddress;*/
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.BasicDynaBean;
import org.apache.commons.beanutils.DynaProperty;
import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.globus.common.email.GmEmailBean;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.common.util.GmTemplateUtil;
import com.globus.it.beans.GmCodeGroupBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.sales.GmCompanyVO;

public class GmCommonClass extends Exception {
  public static int ADD_BUTTON = 1;
  public static int MODIFY_BUTTON = 2;
  public static int DELETE_BUTTON = 3;
  public static int UPDATE_BUTTON = 4;

  // public static DBConnectionWrapper db = new DBConnectionWrapper();

  public static ResourceBundle rb;
  public static ResourceBundle rbWikiTitle;
  public static ResourceBundle rbJiraConfig;
  public static ResourceBundle rbBusinessObjects;
  public static ResourceBundle rbJmsConfig;
  public static ResourceBundle rbShipCarrier;
  public static ResourceBundle rbUdiFdaXml;

 // public static String strServerName;
  public static String strCompanyName;
  


  // public static ResourceBundle rbCurrLocale;
  // To Get the Mail Address Information
  private static ResourceBundle rbAppMail = ResourceBundle.getBundle("mail");

  private static Logger log = GmLogger.getInstance("com.globus.common.beans.GmCommonClass");
  private static final String MINUS = "minus";

  private static final String ZERO = "zero";
  public static boolean blWhitelistserver;
  public static boolean blTestingserver;
  public static String toEmail;
  public static String strTestEmailSubject;
  public static String strTestEmailID;
  public static HashMap hmLdapCrd;
  public static String countryCode;
  public static String contextURL;
  public static String jasperImageURL;
  public static int dbConnectionTimeOut;
  //Adding a new db connection variable for GOP
  public static int dbConnectionTimeOutEXT;
  public static String shipPrintThruJMS;
  

  // Below is for converting number into words.
  private static String amount;
  private static int num;
  private static String[] units = {"", " One", " Two", " Three", " Four", " Five", " Six",
      " Seven", " Eight", " Nine"};
  private static String[] teen = {" Ten", " Eleven", " Twelve", " Thirteen", " Fourteen",
      " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen"};
  private static String[] tens = {" Twenty", " Thirty", " Fourty", " Fifty", " Sixty", " Seventy",
      " Eighty", " Ninety"};
  private static String[] maxs = {"", "", " Hundred", " Thousand", " Lakh", " Crore"};

  private static final String[] groupName = {
  /*
   * American: unit, hundred, thousand, million, billion, trillion, quadrillion, quintillion
   */
  ""/* 1 */, ""/*
                * 100 sto handled specially since it is different for each mulitple.
                */, "tysi\u0105c"/*
                                  * 1000 tysiac tysyi&aogonek;c, varies in a very complex way.
                                  */, "milion"/* 10 ^ 6 */,
      // 1 milion
      // 2 to 4 milliony
      // 5 to 21 milion&oacute;w
      // 22 to 24 miliony
      // 25 to 31 milion&oacute;w
      // 32 to 34 miliony
      // 35 to 41 milion&oacute;w
      // ..
      // 95 to 99 mmilion&oacute;w
      "miliard"/* 10 ^ 9 */,
      // 1 miliard
      // 2 to 4 miliardy
      // 5 to 21 miliard&oacute;w
      // 22 to 24 miliardy
      // 25 to 31 miliard&oacute;w
      // 32 to 34 miliardy
      // 35 to 41 miliard;w
      // ..
      // 95 to 99 miliard&oacute;w
      "bilion"/* 10 ^ 12 guess */,
      // 1 bilion
      // 2 to 4 biliony
      // 5 to 21 bilion&oacute;w
      // 22 to 24 biliony
      // 25 to 31 bilion&oacute;w
      // 32 to 34 biliony
      // 35 to 41 bilion;w
      // ..
      // 95 to 99 bilion&oacute;w
      "biliard"/* 10 ^ 15 guess */,
      // 1 biliard
      // 2 to 4 biliardy
      // 5 to 21 biliard&oacute;w
      // 22 to 24 biliardy
      // 25 to 31 biliard&oacute;w
      // 32 to 34 biliardy
      // 35 to 41 biliard;w
      // ..
      // 95 to 99 biliard&oacute;w
      "trilion"/* 10 ^ 18 guess */,
  // 1 trilion
  // 2 to 4 triliony
  // 5 to 21 trilion&oacute;w
  // 22 to 24 triliony
  // 25 to 31 trilion&oacute;w
  // 32 to 34 triliony
  // 35 to 41 trilion;w
  // ..
  // 95 to 99 trilion&oacute;w
      };

  /* Polish is unusual. It has special words for each multiple of 100 */

  private static final String[] hundredsGroupName = {""
  /* 0 */, "sto"
  /* 100 */, "dwie\u015bcie"
  /* 200 dwiescie dwie&sacute;cie */, "trzysta"
  /* 300 */, "czterysta"
  /* 400 */, "pi\u0119\u0107set"
  /* 500 piecset pi&eogonek;&cacute;set */, "sze\u015b\u0107set"
  /* 600 szescet sze&sacute;&cacute;set */, "siedemset"
  /* 700 */, "osiemset"
  /* 800 */, "dziewi\u0119\u0107set"
  /* 900 dziewiecset dziewi&egonek&cacute;set */,};

  private static final String[] lowName = {
  /* zero is shown as "" since it is never used in combined forms */
  /* 0 .. 19 */
  "", "jeden"
  /* 1 */, "dwa"
  /* 2 */, "trzy"
  /* 3 */, "cztery"
  /* 4 */, "pi\u0119\u0107"
  /* 5 piec pi&eogonek;&cacute; */, "sze\u015b\u0107"
  /* 6 szesc sze&sacute;&cacute; */, "siedem"
  /* 7 */, "osiem"
  /* 8 */, "dziewi\u0119\u0107"
  /* 9 dziewiec dziewi&eogonek;&cacute; */, "dziesi\u0119\u0107"
  /* 10 dziesiec dziesi&eogonek;&cacute; */, "jedena\u015bcie"
  /* 11 jedenascie jedena&sacute;cie */, "dwana\u015bcie"
  /* 12 dwanascie dwana&sacute;cie */, "trzyna\u015bcie"
  /* 13 trzynascie trzyna&sacute;cie */, "czterna\u015bcie"
  /* 14 czternascie czterna&sacute;cie */, "pi\u0119tna\u015bcie"
  /* 15 pietnascie pi&eogonek;etna&sacute;cie */, "szesna\u015bcie"
  /* 16 szesnascie szesna&sacute;cie */, "siedemna\u015bcie"
  /* 17 siedemnascie siedemna&sacute;cie */, "osiemna\u015bcie"
  /* 18 osiemnascie osiemna&sacute;cie */, "dziewi\u0119tna\u015bcie"
  /*
   * 19 dziewietsnascie dziwi&eogonek;tna&sacute;cie
   */};

  /*
   * Polish in unusual. The word for Thousand changes depending on the mulitple
   */

  private static final String[] thousandsGroupName = {""
  /* 0 */, "tysi\u0105c"
  /* 1000 tysiac tysyi&aogonek;c */, "tysi\u0105ce"
  /* 2000 tysiace tysyi&aogonek;ce */, "tysi\u0105ce"
  /* 3000 tysiace tysyi&aogonek;ce */, "tysi\u0105ce"
  /* 4000 tysiace tysyi&aogonek;ce */, "tysi\u0119cy"
  /* 5000 tysiecy tysyi&eogonek;cy */, "tysi\u0119cy"
  /* 6000 tysiecy tysyi&eogonek;cy */, "tysi\u0119cy"
  /* 7000 tysiecy tysyi&eogonek;cy */, "tysi\u0119cy"
  /* 8000 tysiecy tysyi&eogonek;cy */, "tysi\u0119cy"
  /* 9000 tysiecy tysyi&eogonek;cy */};

  private static final String[] tys = {
  /* 0, 10, 20, 30 ... 90 */
  ""
  /* 0 */, ""
  /* 10 */, "dwadzie\u015bcia"
  /* 20 dwadziescia dwadzie&sacute;cia */, "trzydzie\u015bci"
  /* 30 trzydziesci tryzydzie&sacute;ci */, "czterdzie\u015bci"
  /* 40 czterdziesci czterdzie&sacute;ci */, "pi\u0119\u0107dziesi\u0105t"
  /*
   * 50 piecdziesiat pi&egonek;&cacute;diesi&aogonek;t
   */, "sze\u015b\u0107dzies\u0105t"
  /*
   * 60 szescdziesat sze&sacute;&cacute;dzies&aogonek;t
   */, "siedemdziesi\u0105t"
  /* 70 siedemdziesiat siedemdziesi&agononek,t */, "osiemdziesi\u0105t"
  /* 80 osiemdziesiat osiemdziesi&agononek;t */, "dziewi\u0119\u0107dziesi\u0105t"
  /*
   * 90 dziewiecdziesiat dziewi&eogonek;&cacute;dziesi&aogonek;t
   */};

  private static final int[] divisor = {
  /*
   * HowToProcess many of this group is needed to form one of the succeeding group.
   */
  /*
   * American: unit, hundred, thousand, million, billion, trillion, quadrillion, quintillion
   */
  100, 10, 1000, 1000, 1000, 1000, 1000, 1000};

  /**
   * This method returns a string from the Constant Properties file based on the input as a keyword <br>
   * <ol>
   * <li>(SEE CODE).<br>
   * <li><br>
   * </ol>
   * 
   * @param strKey - Keyword
   * @exception
   * @return String object
   */
  public static String getString(String strKey) {
    if (rb == null) {
      rb = ResourceBundle.getBundle(System.getProperty("JBOSS_ENV"));
    }
    return rb.getString(strKey);
  } // End of getString Method

  /**
   * This method returns a string from the Constant Properties file based on the input as a keyword
   * 
   * @param strKey
   * @return
   */
  public static String getShipCarrierString(String strKey) {
    if (rbShipCarrier == null) {
      rbShipCarrier = ResourceBundle.getBundle("ShippingCarrier");
    }
    return rbShipCarrier.getString(strKey);
  } // End of getString Method

  /**
   * This method returns a string from the Constant Properties file based on the input as a keyword
   * 
   * @param strKey
   * @return
   */
  public static String getWikiTitle(String strKey) {
    if (rbWikiTitle == null) {
      rbWikiTitle = ResourceBundle.getBundle("WikiTitle");
    }
    return rbWikiTitle.getString(strKey);
  } // End of getString Method

  /**
   * This method returns a string from the Constant Properties file based on the input as a keyword
   * 
   * @param strKey
   * @return
   */
  public static String getJiraConfig(String strKey) {
    if (rbJiraConfig == null) {
      rbJiraConfig = ResourceBundle.getBundle("JiraConfig");
    }
    return rbJiraConfig.getString(strKey);
  } // End of getString Method

  /**
   * This method returns a string from the Constant Properties file based on the input as a keyword
   * 
   * @param strKey
   * @return
   */
  // Read JMS config properties
  public static String getJmsConfig(String strKey) {
    if (rbJmsConfig == null) {
      rbJmsConfig = ResourceBundle.getBundle(System.getProperty("ENV_JMS"));
    }
    return rbJmsConfig.getString(strKey);
  }

  /**
   * This method will fetch the Business Object Interface App related properties like the BO Server
   * to which to communicate to and on.
   */
  public static String getBusinessObjectsProperty(String strKey) {
    if (rbBusinessObjects == null) {
      rbBusinessObjects = ResourceBundle.getBundle("BusinessObjects");
    }
    return rbBusinessObjects.getString(strKey);
  }

  /**
   * This method returns a string from the Constant Properties file based on the input as a keyword
   * 
   * @param strKey
   * @return
   */
  // Read UDA FDA config properties
  public static String getUDIFdaXmlConfig(String strKey) {
    if (rbUdiFdaXml == null) {
      rbUdiFdaXml = ResourceBundle.getBundle(System.getProperty("ENV_UDI_CONF"));  //"UdiFdaXml"
    }
    return rbUdiFdaXml.getString(strKey);
  }

  /**
   * This method return the Empty value if the value passed as null.
   * 
   * @param string
   * @return
   */
  public static String parseNull(String string) {
    // return (string == null || string.equals("") ||
    // "NULL".equalsIgnoreCase(string)) ? "" : string.trim();
    return (string == null || string.equals("")) ? "" : string.trim();
  }

  /**
   * This method return the Zero value if the value passed as null.
   * 
   * @param string
   * @return
   */
  public static String parseZero(String string) {
    return (string == null || string.equals("")) ? "0" : string.trim();
  }

  /**
   * This method returns an ArrayList containing Code names and Ids based on Code Group as the input <br>
   * <ol>
   * <li>(SEE CODE).<br>
   * <li><br>
   * </ol>
   * 
   * @param strCodeGrp - Code Grp
   * @exception AppError
   * @return ArrayList object
   */
  public static ArrayList getCodeList(String strCodeGrp) throws AppError {
    return getCodeList(strCodeGrp, "", null);
  }

  /**
   * @param strCodeGrp - Code Grp
   * @param gmDataStoreVO - VO object contains the company details
   * @exception AppError
   * @return ArrayList
   */
  public static ArrayList getCodeList(String strCodeGrp, GmDataStoreVO gmDataStoreVO)
      throws AppError {
    return getCodeList(strCodeGrp, "", gmDataStoreVO);
  }

  /**
   * @param strCodeGrp - Code Grp
   * @param strDeptId - strDeptId
   * @exception AppError
   * @return ArrayList
   */
  public static ArrayList getCodeList(String strCodeGrp, String strDeptId) throws AppError {
    return getCodeList(strCodeGrp, strDeptId, null);
  }

  /**
   * @param strCodeGrp - Code Grp
   * @param strDeptId - strDeptId
   * @param gmDataStoreVO - VO object contains the company details
   * @exception AppError
   * @return ArrayList
   */
  public static ArrayList getCodeList(String strCodeGrp, String strDeptId,
      GmDataStoreVO gmDataStoreVO) throws AppError {
    return new GmCodeGroupBean().getCodeList(strCodeGrp, strDeptId, gmDataStoreVO);
  }

  /**
   * @param strCodeGrp - Code Grp
   * 
   * @exception AppError
   * @return ArrayList object
   */
  public static HashMap getAltGroupDetails(ArrayList alParam) throws AppError {
    HashMap hmReturn = new HashMap();
    HashMap hmValues = new HashMap();
    String strCodeNmAlt = "";
    for (Iterator iter = alParam.iterator(); iter.hasNext();) {
      hmValues = (HashMap) iter.next();
      strCodeNmAlt = (String) hmValues.get("CODENMALT");
      if (strCodeNmAlt != null && !strCodeNmAlt.equals("")) {
        hmReturn.put(strCodeNmAlt, getCodeList(strCodeNmAlt));
      }
    }
    return hmReturn;
  }

  /**
   * @param strCodeGrp - Code Grp
   * 
   * @exception AppError
   * @return ArrayList object
   */
  public static HashMap getSearchList(String strCodeGrp) throws AppError {
    ArrayList arList = new ArrayList();
    HashMap hmMap = new HashMap();

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_cm_party_info.gm_fch_search_lookup", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCodeGrp);
    gmDBManager.execute();
    arList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    Iterator arListItr = arList.iterator();
    while (arListItr.hasNext()) {
      HashMap hm = (HashMap) arListItr.next();
      String strCodeID = (String) hm.get("CODEID");
      String strCodeNM = (String) hm.get("CODENM");
      hmMap.put(strCodeNM, strCodeID);
    }
    log.debug("hmSearchMap " + hmMap);
    return hmMap;
  }

  /**
   * This method returns the Study site ID values
   * 
   * @param siteId ,studyId
   * @return strReturn
   * @throws AppError
   */
  public static String getStudySiteId(String siteId, String studyId) throws AppError {
    HashMap hmReturn = new HashMap();
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();
    sbQuery.append(" SELECT c614_study_site_id STUDYSITEID");
    sbQuery.append(" FROM t614_study_site ");
    sbQuery.append(" WHERE c611_study_id ='" + studyId + "' AND c614_site_id = '" + siteId
        + "' AND c614_void_fl IS NULL ");

    hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());

    String strReturn =
        hmReturn.get("STUDYSITEID") == null ? "" : (String) hmReturn.get("STUDYSITEID");
    return strReturn;
  }

  /**
   * This method returns an ArrayList containing Code names and Ids based on Code Group as the input <br>
   * <ol>
   * <li>(SEE CODE).<br>
   * <li><br>
   * </ol>
   * 
   * @param strCodeId - Code Grp
   * @exception AppError
   * @return ArrayList object
   */
  public static String getCodeName(String strCodeId) throws AppError {
    return getCodeID(strCodeId, "");
  }

  /**
   * This method returns the Exception message
   * 
   * @param e
   * @param strNewLine
   * @return String
   */
  public static String getExceptionStackTrace(Exception e, String strNewLine) {
    try {
      StackTraceElement stack[] = e.getStackTrace();
      StringBuffer strErrorBuffer = new StringBuffer();
      // stack[0] contains the method that created the exception.
      // stack[stack.length-1] contains the oldest method call.
      // Enumerate each stack element.
      strErrorBuffer.append(e.getMessage() + strNewLine);
      for (int i = 0; i < stack.length; i++) {
        String filename = stack[i].getFileName();
        if (filename == null) {
          filename = " The source filename is not available ";
        }
        String className = stack[i].getClassName();
        String methodName = stack[i].getMethodName();
        int line = stack[i].getLineNumber();
        strErrorBuffer.append("\t at " + className);
        strErrorBuffer.append("." + methodName + "(");
        strErrorBuffer.append(filename + ":");
        strErrorBuffer.append(line + ")" + strNewLine);
      }
      return strErrorBuffer.toString();
    } catch (Exception ex) {
      return "Exception stack trace not found.";
    }
  }

  /*
   * getExceptionStackTrace is an overloaded version and used to convert the stack trace to string
   * for better logging.
   */
  public static String getExceptionStackTrace(Exception e) {
    StringBuilder strErrorMsg = new StringBuilder();
    String strMsg = "";
    try {
      StringWriter sw = new StringWriter();
      e.printStackTrace(new PrintWriter(sw));

      strErrorMsg.append(sw.toString());
      strMsg = strErrorMsg.toString();

    } catch (Exception ex) {
      strMsg = "Exception stack trace not found.";
    }
    return strMsg;
  }

  /**
   * This method returns an ArrayList containing Code names and Ids based on Code Group as the input <br>
   * <ol>
   * <li>(SEE CODE).<br>
   * <li><br>
   * </ol>
   * 
   * @param strCodeId - Code Grp
   * @exception AppError
   * @return ArrayList object
   */
  public static String getCodeID(String strCodeName, String strCodeGrp) throws AppError {
    String strCodeID = "";
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();
    sbQuery.append(" SELECT C901_CODE_ID CODEID , C901_CODE_NM CODENM ");
    sbQuery.append(" FROM T901_CODE_LOOKUP ");
    sbQuery.append(" WHERE C901_CODE_NM = '" + strCodeName + "'");
    if (!strCodeGrp.equals("")) {
      sbQuery.append(" AND C901_CODE_GRP = '" + strCodeGrp + "'");
    }
    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    if (!strCodeGrp.equals("")) {
      strCodeID = (String) hmResult.get("CODEID");
    } else {
      strCodeID = (String) hmResult.get("CODENM");
    }
    return strCodeID;
  }

  /**
   * This method returns the Code Name for the given code id
   * 
   * @param strCodeId - Code id
   * @exception AppError
   * @return String object
   */
  public static String getCodeNameFromCodeId(String strCodeId) throws AppError {
    String strCodeNm = "";
    StringBuffer sbQuery = new StringBuffer();
    HashMap hmResult = null;
    GmDBManager gmDBManager = new GmDBManager();
    sbQuery.append(" SELECT C901_CODE_NM CODENM ");
    sbQuery.append(" FROM T901_CODE_LOOKUP ");
    sbQuery.append(" WHERE C901_CODE_ID = '" + strCodeId + "'");

    hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    strCodeNm = (String) hmResult.get("CODENM");

    return strCodeNm;
  }

  /**
   * This method returns the code Alt Name for the given code id
   * 
   * @param strCodeId - Code id
   * @exception AppError
   * @return String object
   */
  public static String getCodeAltName(String strCodeId) throws AppError {
    String codeAltNm = "";
    StringBuffer sbQuery = new StringBuffer();
    GmDBManager gmDBManager = new GmDBManager();
    sbQuery.append(" SELECT C902_CODE_NM_ALT CODENMALT ");
    sbQuery.append(" FROM T901_CODE_LOOKUP ");
    sbQuery.append(" WHERE C901_CODE_ID = '" + strCodeId + "'");

    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    codeAltNm = (String) hmResult.get("CODENMALT");

    return codeAltNm;
  }



  /**
   * This method returns a String containing Code names based on Code Name Alt as the input <br>
   * <ol>
   * <li>(SEE CODE).<br>
   * <li><br>
   * </ol>
   * 
   * @param strCodeNameAlt - Code Name Alt
   * @exception AppError
   * @return ArrayList object
   */
  public static String getCodeNameFromAlt(String strCodeNameAlt) throws AppError {
    ArrayList arList = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    String strCodeId = "";
    GmDBManager gmDBManager = new GmDBManager();
    sbQuery.append(" SELECT C901_CODE_ID CODEID, C901_CODE_NM CODENM ");
    sbQuery.append(" FROM T901_CODE_LOOKUP ");
    sbQuery.append(" WHERE C902_CODE_NM_ALT = '" + strCodeNameAlt + "'");

    HashMap hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
    strCodeId = (String) hmResult.get("CODENM");

    return strCodeId;
  }

  /**
   * @desc Used to get the code list without checking active flag.
   * @param strCodeGrp - Code Grp
   * 
   * @exception AppError
   * @return ArrayList object
   */
  public static ArrayList getAllCodeList(String strCodeGrp) throws AppError {
    ArrayList arList = new ArrayList();
    HashMap hmMap = new HashMap();

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_cm_codelookup_util.gm_cm_fch_allcodelist", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCodeGrp);
    gmDBManager.execute();
    arList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("arList.. " + arList);
    return arList;
  }

  /**
   * @desc Used to get the code list for the group which is the alt name for other code id Ex. for
   *       the code group ADDCH there are some codelookup ids we have. Some of them have alt name
   *       like DMGRP, BHGRP etc...which are the code group for some other code lookup values. To
   *       fetch the code id and the codename for the second code group this procedure will be
   *       helpful.
   * @param strCodeId - Code Grp
   * 
   * @exception AppError
   * @return ArrayList object
   */
  public static ArrayList getAllCodeListForAlt(String strCodeId) throws AppError {
    ArrayList arList = new ArrayList();
    HashMap hmMap = new HashMap();

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_cm_codelookup_util.gm_cm_fch_codelistforalt", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    log.debug("strCodeId..." + strCodeId);
    gmDBManager.setString(1, strCodeId);
    gmDBManager.execute();
    arList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("alList.. " + arList);
    return arList;
  }

  /**
   * This method returns a String containing based on Code Group as the input <br>
   * <ol>
   * <li>(SEE CODE).<br>
   * <li><br>
   * </ol>
   * 
   * @param strCodeGrp - Code Grp
   * @exception AppError
   * @return String object
   */

  public static String getStringWithCommas(String strVal) {
    double fltVal = 0;
    String newstr = "";
    try {
      if (strVal != null && !"".equals(strVal)) {
        fltVal = Double.parseDouble(strVal);
      }

      NumberFormat nf = NumberFormat.getNumberInstance();
      nf.setGroupingUsed(true); // don't group by threes
      nf.setMaximumFractionDigits(2);
      nf.setMinimumFractionDigits(2);
      newstr = nf.format(fltVal);
    } catch (NumberFormatException e) {
      newstr = strVal;
    }
    return newstr;
  }

  /**
   * This method returns a String with N Decimal Values
   * 
   * @param strVal - Contains the Input value
   * @param intPrecision - Contains Precision Information
   * @exception AppError
   * @return String object
   */
  public static String getStringWithCommas(String strVal, int intPrecision) {
    double fltVal = 0;
    String newstr = "";
    try {
      if (strVal != null && !"".equals(strVal)) {
        fltVal = Double.parseDouble(strVal);
      }

      NumberFormat nf = NumberFormat.getNumberInstance();
      nf.setGroupingUsed(true); // don't group by threes
      nf.setMaximumFractionDigits(intPrecision);
      nf.setMinimumFractionDigits(intPrecision);
      newstr = nf.format(fltVal);
    } catch (NumberFormatException e) {
      newstr = strVal;
    }
    return newstr;
  }

  /**
   * This method returns a String with currformat passed
   * 
   * @param strCurrVal - Contains the Input Currency Value
   * @param strCurrFormat - Contains Format which needs to convert in
   * @exception AppError
   * @return String object
   */
  public static String getStringWithCommas(String strCurrVal, String strCurrFormat) {
    String strFormattedCurr = "";
    NumberFormat formatter = new DecimalFormat(strCurrFormat);
    strFormattedCurr = formatter.format(new Double(strCurrVal.equals("") ? "0" : strCurrVal));

    return strFormattedCurr;
  }

  public static String[] getMailingList(String strRBID) {
    String[] strMailIDs;
    String strMailID;
    strMailID = rbAppMail.getString(strRBID);
    strMailIDs = GmCommonClass.StringtoArray(strMailID, ";");
    return strMailIDs;
  }

  /**
   * This method used to send mail based on the in parameter send by the user
   * 
   * @param String strFrom - Should send the person thats sending the mail
   * @param String[] strTo - Array of 2 list
   * @param String[] strCc - Array of 2 list
   * @param String strSubject
   * @param String strMessageDesc
   * @exception AppError
   * @return Boolean
   */
  public static boolean sendMail(String strFrom, String[] strTo, String[] strCc, String strSubject,
      String strMessageDesc) throws AppError {

    try {
      boolean boolReturn = false;
      String[] strBcc = null;
      String[] strReplyTo = new String[1];
      strReplyTo[0] = strFrom;
      String strAttachment = "";
      String strMimeType = "";
      String strEmailHeader = "";
      HashMap hmParam = new HashMap(); 
      hmParam.put("strFrom",strFrom);
      hmParam.put("strTo",strTo);
      hmParam.put("strCc",strCc);
      hmParam.put("strBcc",strBcc);
      hmParam.put("strSubject",strSubject);
      hmParam.put("strMessageDesc",strMessageDesc);
      hmParam.put("strMimeType",strMimeType);
      hmParam.put("strAttachment",strAttachment);
      hmParam.put("strReplyTo",strReplyTo);
      hmParam.put("strEmailHeader",strEmailHeader);
     /* boolReturn =
          sendMail(strFrom, strTo, strCc, strBcc, strSubject, strMessageDesc, strMimeType,
              strAttachment, strReplyTo);*/
      boolReturn =
              sendEmail(hmParam);

      return boolReturn;
    } catch (Exception e) {
      // System.out.println("IN SEND MAIL");
      e.printStackTrace();
      throw new AppError(e);
    }

  } // End of sendMail

  /**
   * This method useed to send the mail
   * 
   * @param emailProperties
   * @throws Exception
   */
  public static void sendMail(GmEmailProperties emailProperties) throws Exception {

    // log.debug("emailProperties = " + emailProperties);
    String[] to = emailProperties.getRecipients();
    String[] cc = emailProperties.getCc();
    String[] bcc = emailProperties.getBcc();
    String[] strReplyTo = emailProperties.getReplyTo();
    String strFrom = GmCommonClass.parseNull(emailProperties.getSender());
    String strSubject = GmCommonClass.parseNull(emailProperties.getSubject());
    String strMessageDesc = GmCommonClass.parseNull(emailProperties.getMessage());
    String strEmailAttachment = GmCommonClass.parseNull(emailProperties.getAttachment());
    String strMimeType = GmCommonClass.parseNull(emailProperties.getMimeType());
    String strEmailHeaderNm = GmCommonClass.parseNull(emailProperties.getEmailHeaderName());
    
    HashMap hmParam = new HashMap(); 
    hmParam.put("strFrom",strFrom);
    hmParam.put("strTo",to);
    hmParam.put("strCc",cc);
    hmParam.put("strBcc",bcc);
    hmParam.put("strSubject",strSubject);
    hmParam.put("strMessageDesc",strMessageDesc);
    hmParam.put("strMimeType",strMimeType);
    hmParam.put("strAttachment",strEmailAttachment);
    hmParam.put("strReplyTo",strReplyTo);
    hmParam.put("strEmailHeader",strEmailHeaderNm);

    boolean boolReturn = false;
    /*boolReturn =
        sendMail(strFrom, to, cc, bcc, strSubject, strMessageDesc, strMimeType, strEmailAttachment,
            strReplyTo);*/
    boolReturn =
            sendEmail(hmParam);
  }

  /**
   * This method used to send mail based on the in parameter send by the user
   * 
   * @param HashMap which have all mail related data.
   * @exception AppError
   * @return Boolean
   */
  public static boolean sendMail(HashMap hmValues) throws Exception {
    String strFrom = GmCommonClass.parseNull((String) hmValues.get("strFrom"));
    String[] strTo = (String[]) hmValues.get("strTo");
    String[] strCc = (String[]) hmValues.get("strCc");
    String strSubject = GmCommonClass.parseNull((String) hmValues.get("strSubject"));
    String strMessageDesc = GmCommonClass.parseNull((String) hmValues.get("strMessageDesc"));
    String strMimeType = GmCommonClass.parseNull((String) hmValues.get("strMimeType"));

    String[] strBcc = null;
    String strAttachment = "";
    if (strMimeType.equals(""))
      strMimeType = "text/plain";
    boolean boolReturn = false;
    String[] strReplyTo = new String[1];
    strReplyTo[0] = strFrom;
    String strEmailHeaderName = "";
    
    HashMap hmParam = new HashMap(); 
    hmParam.put("strFrom",strFrom);
    hmParam.put("strTo",strTo);
    hmParam.put("strCc",strCc);
    hmParam.put("strBcc",strBcc);
    hmParam.put("strSubject",strSubject);
    hmParam.put("strMessageDesc",strMessageDesc);
    hmParam.put("strMimeType",strMimeType);
    hmParam.put("strAttachment",strAttachment);
    hmParam.put("strReplyTo",strReplyTo);
    hmParam.put("strEmailHeader",strEmailHeaderName);
    
    // PC-3000: integrate email to sendgrid api
    hmParam.put("TRANSACTION_ID", GmCommonClass.parseNull((String) hmValues.get("TRANSACTION_ID")));
    hmParam.put("EMAIL_TRACKING_TYPE", GmCommonClass.parseNull((String) hmValues.get("EMAIL_TRACKING_TYPE")));
    hmParam.put("EMAIL_TRACKING_FL", GmCommonClass.parseNull((String) hmValues.get("EMAIL_TRACKING_FL")));
    
    /*boolReturn =
        sendMail(strFrom, strTo, strCc, strBcc, strSubject, strMessageDesc, strMimeType,
            strAttachment, strReplyTo);*/
    boolReturn =
            sendEmail(hmParam);

    boolReturn = true;
    return boolReturn;

  } // End of sendMail

  /**
   * This method returns the String array value .
   * 
   * @param s ,sep
   * @return
   */
  public static String[] StringtoArray(String s, String sep) {
    // convert a String s to an Array, the elements
    // are delimited by sep
    StringBuffer buf = new StringBuffer(s);
    int arraysize = 1;
    int len = buf.length();

    for (int i = 0; i < len; i++) {
      if (sep.indexOf(buf.charAt(i)) != -1)
        arraysize++;
    }

    String[] elements = new String[arraysize];

    int y, z = 0;

    if (buf.toString().indexOf(sep) != -1) {
      while (buf.length() > 0) {
        if (buf.toString().indexOf(sep) != -1) {
          y = buf.toString().indexOf(sep);
          if (y != buf.toString().lastIndexOf(sep)) {
            elements[z] = buf.toString().substring(0, y);
            z++;
            buf.delete(0, y + 1);
          } else if (buf.toString().lastIndexOf(sep) == y) {
            elements[z] = buf.toString().substring(0, buf.toString().indexOf(sep));
            z++;
            buf.delete(0, buf.toString().indexOf(sep) + 1);
            elements[z] = buf.toString();
            z++;
            buf.delete(0, buf.length());
          }
        }
      }
    } else {
      elements[0] = buf.toString();
    }
    buf = null;
    return elements;
  } // End of StringtoArray

  /**
   * This method returns a String containing based on Code Group as the input <br>
   * <ol>
   * <li>(SEE CODE).<br>
   * <li><br>
   * </ol>
   * 
   * @param strCodeGrp - Code Grp
   * @exception AppError
   * @return String object
   */

  public static String getStringWithTM(String strVal) {
    String strTM = "";
    String strTemp = "";
    String strTrade = "PIVOT,RETAIN,ALLIANCE,CITADEL,GATEWAY,LAMINEX";
    String strReg = "PROTEX,SUSTAIN,PRESERVE,SECURE,MAINTAIN,XPand,ASSURE,REVERE";
    strVal = strVal.replaceAll(" deg", "&deg;");

    StringTokenizer strToken = new StringTokenizer(strTrade, ",");
    StringTokenizer strRegToken = new StringTokenizer(strReg, ",");

    while (strToken.hasMoreTokens()) {
      strTemp = strToken.nextToken();
      strTM = strVal.replaceAll(strTemp, strTemp.concat("&trade;"));
      if (!strTM.equals(strVal)) {
        break;
      }
    }

    while (strRegToken.hasMoreTokens()) {
      strTemp = strRegToken.nextToken();
      strTM = strTM.replaceAll(strTemp, strTemp.concat("&reg;"));
      if (!strTM.equals(strVal)) {
        break;
      }
    }
    return strTM;
  }

  /**
   * This method returns the value as Red TextPMR.
   * 
   * @param strDispVal
   * @return
   */
  public static String getRedTextPMR(String strDispVal) {
    if (strDispVal.charAt(0) == '-') {

      return ("<span class=RightTextRedSmall>(" + strDispVal.replaceAll("-", "") + ")</span>");
    } else {
      // Space added so the screen is aligned properly with open
      // and close bracket
      return (strDispVal + "&nbsp");
    }
  }

  /**
   * This method returns the value as Red Text.
   * 
   * @param strDispVal
   * @return
   */
  public static String getRedText(String strDispVal) {
    if (strDispVal == null || strDispVal.equals(""))
      return (strDispVal + "&nbsp");

    if (strDispVal.charAt(0) == '-') {

      return ("<span class=RightTextRed>(" + strDispVal.replaceAll("-", "") + ")</span>");
    } else {
      // Space added so the screen is aligned properly with open
      // and close bracket
      return (strDispVal + "&nbsp");
    }
  }

  /**
   * This method returns the Double value
   * 
   * @param dbVal
   * @return
   */
  public static Double parseDouble(Double dbVal) {
    Double dbTemp = new Double(0);
    return (dbVal == null) ? dbTemp : dbVal;
  }

  /**
   * @param strVal
   * @return
   */
  public static String getStringInThousands(String strVal) {
    double fltVal = 0;
    String newstr = "";
    try {
      if (strVal != null && !"".equals(strVal)) {
        fltVal = Double.parseDouble(strVal) / 1000;
      }

      NumberFormat nf = NumberFormat.getNumberInstance();
      nf.setGroupingUsed(true); // don't group by threes
      nf.setMaximumFractionDigits(2);
      nf.setMinimumFractionDigits(2);
      newstr = nf.format(fltVal);
    } catch (NumberFormatException e) {
      newstr = strVal;
    }
    return newstr;
  }

  /**
   * @param fltVal
   * @return
   */
  public static String getStringInThousands(double fltVal) {
    String newstr = "";
    try {
      fltVal = fltVal / 1000;
      NumberFormat nf = NumberFormat.getNumberInstance();
      nf.setGroupingUsed(true); // don't group by threes
      nf.setMaximumFractionDigits(2);
      nf.setMinimumFractionDigits(2);
      newstr = nf.format(fltVal);
    } catch (NumberFormatException e) {
      newstr = "0";
    }
    return newstr;
  }

  /**
   * This method returns the String value in Decimal format.
   * 
   * @param fltVal
   * @return
   */
  public static String getStringInDecFormat(double fltVal) {
    DecimalFormat df = new DecimalFormat("#");
    String newstr = "";
    try {
      if (!Double.isNaN(fltVal)) {
        newstr = df.format(fltVal);
      } else {
        newstr = "0.0";
      }
    } catch (NumberFormatException e) {
      newstr = "0.0";
    }
    return newstr;
  }

  /**
   * This method returns the String value in Percentage format.
   * 
   * @param fltVal
   * @return
   */
  public static String getStringInPerFormat(double fltVal) {
    DecimalFormat df = new DecimalFormat("##0.00%");
    String newstr = "";
    try {
      if (!Double.isNaN(fltVal)) {
        newstr = df.format(fltVal);
      } else {
        newstr = "0.0";
      }
    } catch (NumberFormatException e) {
      newstr = "0.0";
    }
    return newstr;
  }

  /**
   * This method returns the String value as RedTextSmall.
   * 
   * @param strDispVal
   * @return
   */
  public static String getRedTextSmall(String strDispVal) {
    if (strDispVal.charAt(0) == '-') {

      return ("<span class=RightTextRedSalesSmall>(" + strDispVal.replaceAll("-", "") + ")</span>");
    } else {
      // Space added so the screen is aligned properly with open
      // and close bracket
      return (strDispVal + "&nbsp");
    }
  }

  /**
   * This method returns the Fromatted date value.
   * 
   * @param strInputDate
   * @param dtFormat
   * @param strOutputFormat
   * @return
   * @throws ParseException
   */
  public static String getFormattedDate(String strInputDate, String dtFormat, String strOutputFormat)
      throws ParseException {
    Date dtFromDate = new SimpleDateFormat(dtFormat).parse(strInputDate);
    DateFormat formatter = new SimpleDateFormat(strOutputFormat);
    String strFormattedDate = formatter.format(dtFromDate);

    return strFormattedDate;
  }


  /**
   * Replace characters having special meaning <em>inside</em> HTML tags with their escaped
   * equivalents, using character entities such as <tt>'&amp;'</tt>.
   * <P>
   * The escaped characters are :
   * <ul>
   * <li><
   * <li>>
   * <li>"
   * <li>'
   * <li>\
   * <li>&
   * </ul>
   * <P>
   * This method ensures that arbitrary text appearing inside a tag does not "confuse" the tag. For
   * example, <tt>HREF='Blah.do?Page=1&Sort=ASC'</tt> does not comply with strict HTML because of
   * the ampersand, and should be changed to <tt>HREF='Blah.do?Page=1&amp;Sort=ASC'</tt>. This is
   * commonly seen in building query strings. (In JSTL, the c:url tag performs this task
   * automatically.)
   */

  public static String formatSpecialCharFromDB(String strOrig) {
    final StringBuffer result = new StringBuffer();
    char character;
    if (strOrig == null)
      return null;

    for (int ip = 0; ip <= strOrig.length() - 1; ++ip) {
      character = strOrig.charAt(ip);
      if (character == '\'') {
        result.append("\\'");
      } else if (character == '\"') {
        result.append("&QUOT;");
      } else if (character == '&') {
        result.append("&amp;");
      } else {
        result.append(character);
      }

    }
    return result.toString();
  }

  /**
   * @param req ,res
   * @return
   */
  public static String getWindowOperner(HttpServletRequest req, HttpServletResponse res) {
    // Holds the where condition information
    StringBuffer strQuery = new StringBuffer();
    Enumeration params = req.getParameterNames();
    String paramName = null;
    String[] paramValues = null;

    while (params.hasMoreElements()) {
      paramName = (String) params.nextElement();
      paramValues = req.getParameterValues(paramName);

      // Fecth the parameter name & the value
      if (paramValues.length > 0) {
        strQuery.append("&" + paramName + "=");
      }

      for (int i = 0; i < paramValues.length; i++) {
        strQuery.append(paramValues[i].toString());
        // System.out.print(", value " + i + " is " +
        // paramValues[i].toString());
      }
    }

    return strQuery.toString();
  }

  /**
   * This method returns the String woth quotes value
   * 
   * @param strInput
   * @return
   */
  public static String getStringWithQuotes(String strInput) {
    String strInputWithQuotes = "";

    if (strInput == null) {
      return strInputWithQuotes;
    }

    if (!strInput.equals("")) // modified by VPrasath
    {
      strInputWithQuotes = strInput.replaceAll(",", "','"); // replacing
      // the , to
      // ',' so
      // that IN
      // operator
      // works
      // good for multiple inputs
    }

    if (strInput.indexOf(",") == -1) {
      return strInput;
    }

    else {
      return strInputWithQuotes;
    }
  }

  /**
   * This method returns a String which is the difference between the months
   * 
   * @param strFromMonth
   * @param strFromYear
   * @param strToMonth
   * @param strToYear
   * @return String strDifference
   */
  public static String getMonthDiff(String strFromMonth, String strFromYear, String strToMonth,
      String strToYear) {

    if (strFromMonth == null) {
      return "";
    }

    if (strFromMonth.equals("")) {
      return "";
    }

    int intyearDiff = Integer.parseInt(strToYear) - Integer.parseInt(strFromYear);
    int intToMonth = Integer.parseInt(strToMonth) + intyearDiff * 12;
    int diff = intToMonth - Integer.parseInt(strFromMonth);

    diff = diff + 1; // ie if From is Feb'07 and To is March'07, then the
    // diff is 2 and not 1

    String strDifference = String.valueOf(diff);

    return strDifference;
  }

  /**
   * This method returns a HashMap which has the From Month, From Year, To Month and To Year which
   * is a differs with the input integer value
   * 
   * @param intDifference
   * @return HashMap (FROMMONTH, FROMYEAR, TOMONTH, TOYEAR)
   */
  public static HashMap getMonthDiff(int intDifference, boolean excludeCurrentMonth) {
    String strFromMonth = "";
    String strFromYear = "";
    String strToMonth = "";
    String strToYear = "";

    int intMonthDiff = 0;
    int intFromMonth = 0;

    HashMap hmFromToCal = new HashMap();

    if (intDifference == 0) {
      intDifference = 1;
      excludeCurrentMonth = false;
    }

    GregorianCalendar d = new GregorianCalendar();

    int intCurMon = d.get(Calendar.MONTH);
    int intCurYear = d.get(Calendar.YEAR);
    int intFromYear = d.get(Calendar.YEAR);

    intCurMon += 1; // since MONTH considers Jan as 0 and on.
    if (excludeCurrentMonth) {
      intCurMon = ((intCurMon + 12) - 1) % 12;
      if (intCurMon == 0) {
        intCurMon = 12;
        intCurYear--;
        intFromYear--;
      }
    }
    intMonthDiff = intCurMon - intDifference;

    intFromMonth = ((intCurMon + 12) - intDifference) % 12; // calculating
    // the
    // difference in
    // the month
    // values

    intFromMonth++; // adding 1 to the difference thereby including the
    // current month

    if (intMonthDiff < 0) {
      intFromYear--;
    }

    strFromMonth = String.valueOf(intFromMonth);
    strFromYear = String.valueOf(intFromYear);
    strToMonth = String.valueOf(intCurMon);
    strToYear = String.valueOf(intCurYear);

    if (strFromMonth.length() == 1) {
      strFromMonth = "0".concat(strFromMonth);
    }

    if (strToMonth.length() == 1) {
      strToMonth = "0".concat(strToMonth);
    }

    hmFromToCal.put("FromMonth", strFromMonth);
    hmFromToCal.put("FromYear", strFromYear);
    hmFromToCal.put("ToMonth", strToMonth);
    hmFromToCal.put("ToYear", strToYear);

    return hmFromToCal;
  }

  /**
   * parseOptVar - This method will parse the strOpt variable and add the parsed Strings into
   * HashMap
   * 
   * @param String strOpt
   * @return HashMap hmReturn. Keys would be OptVar1, optVar2
   * @exception AppError
   */
  public static HashMap parseOptVar(String strOpt) throws AppError {
    String strOptVar1 = "";
    String strOptVar2 = "";
    HashMap hmReturn = new HashMap();
    if (!strOpt.equals("") && strOpt.indexOf("@") != -1) {
      /*
       * The input would be var1@var2 For eg if the Rollback Set Consignment report has to be
       * opened, then hopt value will be as cancelreport@RBSCN
       */
      strOptVar1 = strOpt.substring(0, strOpt.indexOf("@"));
      strOptVar2 = strOpt.substring(strOpt.indexOf("@") + 1);
    }
    hmReturn.put("strOptVar1", strOptVar1);
    hmReturn.put("strOptVar2", strOptVar2);

    return hmReturn;
  }

  /**
   * getFormFromHashMap - This method will populate the inputForm (actForm) with the values present
   * in the HashMap hmParam
   * 
   * @param ActionForm actForm
   * @param HashMap hmParam
   * @return ActionForm
   * @exception AppError
   */
  public static ActionForm getFormFromHashMap(ActionForm actForm, HashMap hmParam) throws AppError {

    String strMethodName = "";
    String strMethodReturnType = "";
    String strFieldNameFromMethod = "";
    Method[] methods = actForm.getClass().getMethods();
    Class[] types = new Class[1];
    Object[] objTempArr = new Object[1];
    String strSetMethod = "";

    int msize = methods.length;
    try {
      for (int i = 0; i < msize; i++) {
        Method methodInForm = methods[i];
        strMethodName = methodInForm.getName();
        Class clType = methodInForm.getReturnType();
        strMethodReturnType = clType.getName();
        if ((strMethodReturnType.equals("java.lang.String") || strMethodReturnType
            .equals("java.util.Date")) && (strMethodName.startsWith("get"))) {
          strFieldNameFromMethod = strMethodName.substring(3, strMethodName.length());
          if (hmParam.containsKey(strFieldNameFromMethod.toUpperCase())) {
            types[0] = clType;
            strSetMethod =
                "set" + Character.toUpperCase(strFieldNameFromMethod.charAt(0))
                    + strFieldNameFromMethod.substring(1);
            Method method = actForm.getClass().getMethod(strSetMethod, types);
            objTempArr[0] = hmParam.get(strFieldNameFromMethod.toUpperCase());
            if (objTempArr != null) {
              method.invoke(actForm, objTempArr);
            }
          }
        }
      }
    } catch (Exception exp) {
      log.debug("Exception occured " + exp);
      exp.printStackTrace();
    }
    return actForm;
  }

  /**
   * getHashMapFromForm - This method will populate the HashMap with the values present in the form
   * actForm
   * 
   * @param ActionForm actForm
   * @return HashMap hmParam
   * @exception AppError
   */
  public static HashMap getHashMapFromForm(ActionForm actForm) throws AppError {
    String strMethodName = "";
    String strMethodReturnType = "";
    String strFieldNameFromMethod = "";
    Method[] methods = actForm.getClass().getMethods();
    Class[] types = new Class[] {};

    int msize = methods.length;
    HashMap hmParam = new HashMap();
    try {
      for (int i = 0; i < msize; i++) {
        Method methodInForm = methods[i];
        strMethodName = methodInForm.getName();
        Class clType = methodInForm.getReturnType();
        strMethodReturnType = clType.getName();
        if (strMethodReturnType.equals("java.lang.String")
            || strMethodReturnType.equals("java.util.Date")) {
          if (strMethodName.startsWith("get")) {
            strFieldNameFromMethod = strMethodName.substring(3, strMethodName.length());
            Method method = actForm.getClass().getMethod(strMethodName, types);
            hmParam
                .put(strFieldNameFromMethod.toUpperCase(), method.invoke(actForm, new Object[0]));
          }
        }
      }
      log.debug(" valus inside hashmap " + hmParam);
    } catch (Exception exp) {
      exp.printStackTrace();
      log.debug("Exception occured " + exp);
    }
    return hmParam;
  }

  /**
   * parseNullArrayList - returns a new ArrayList object if the Input ArrayList is null
   * 
   * @param alList
   * @return ArrayList
   */
  public static ArrayList parseNullArrayList(ArrayList alList) {
    if (alList == null) {
      return new ArrayList();
    } else
      return alList;
  }

  /**
   * parseNullHashMap - returns a new HashMap object if the Input HashMap is null
   * 
   * @param alList
   * @return ArrayList
   */
  public static HashMap parseNullHashMap(HashMap hmMap) {
    if (hmMap == null) {
      return new HashMap();
    } else
      return hmMap;
  }

  /**
   * getCheckBoxValue - returns "Y" in case the checkbox is turned ON
   * 
   * @param alList
   * @return ArrayList
   */
  public static String getCheckBoxValue(String strCheckValue) {
    String strReturnValue = "";

    if (strCheckValue == null || strCheckValue.equals("") || strCheckValue.equals("off")) {
      strReturnValue = "";
    } else if (strCheckValue.equals("on")) {
      strReturnValue = "Y";
    }
    return strReturnValue;
  }

  /**
   * createInputString - creates a comma seprarated string from the given String array. Output will
   * be like 101.210,101.211,101.113
   * 
   * @param String []
   * @return String
   */
  public static String createInputString(String[] strCheckedValue) {
    String strReturnValue = "";

    if (strCheckedValue == null) {
      return "";
    }

    int intCheckedValueLength = strCheckedValue.length;
    for (int i = 0; i < intCheckedValueLength; i++) {
      if (strCheckedValue[i] != null && !strCheckedValue[i].equals(""))
        strReturnValue = strCheckedValue[i] + "," + strReturnValue;
    }
    if (strReturnValue.length() > 0) {
      strReturnValue = strReturnValue.substring(0, strReturnValue.length() - 1);
      // strReturnValue = getStringWithQuotes(strReturnValue);
    }
    return strReturnValue;
  }


  /**
   * createInputString - creates a comma seprarated string from the given String array. Output will
   * be a hashmap containing values that is used by set_my_double_inlist_ctx procedure to set the
   * "in" values in a where clause
   * 
   * @param String []
   * @return String
   */
  public static String createInputStringWithType(String[] strCheckedValue, String strType) {
    String strReturnValue = "";

    int intCheckedValueLength = strCheckedValue.length;
    for (int i = 0; i < intCheckedValueLength; i++) {
      if (strCheckedValue[i] != null && !strCheckedValue[i].equals("")) {
        strReturnValue = strReturnValue + strType + "," + strCheckedValue[i] + "|";
      }
    }
    return strReturnValue;
  }

  /**
   * createRegExpString - creates a string with regular expression values
   * 
   * @param String
   * @return String
   */

  public static String createRegExpString(HashMap hmParams) {
    String strVariable = (String) hmParams.get("PARTNUM");
    String strExpression = (String) hmParams.get("SEARCH");

    if(strExpression.equals("LIT")){
      strVariable = ("^").concat(strVariable);
      strVariable = strVariable.replaceAll(",", "\\$|^");
      strVariable = strVariable.concat("$");	
	}else if (strExpression.equals("LIKEPRE")) {
      strVariable = ("^").concat(strVariable);
      strVariable = strVariable.replaceAll(",", "|^");
    } else if (strExpression.equals("LIKESUF")) {
      strVariable = strVariable.replaceAll(",", "\\$|");
      strVariable = strVariable.concat("$");
    } else {
      strVariable = strVariable.replaceAll(",", "|");
    }
    return strVariable;
  }

  /**
   * removeSpaces - removes all the spaces in the given input string. For eg, if the input string is
   * "101.107, 101.109" - o/p wud be "101.107,101.109"
   * 
   * @param String
   * @return String
   */
  public static String removeSpaces(String strInput) {
    String strOutput = strInput.replaceAll("\\s", ""); // \s matches all
    // white space
    // characters which
    // is [ \t\n\x0B\f\r
    // ]
    return strOutput;
  }

  /**
   * encodeHTML - this method will read a ? ? ? (in itz unicode) and replace it with a HTML symbol
   * If the Input string is null, it returns "" I/P: SUSTAIN Spacer, Small, 9mm ? ? test O/P:
   * SUSTAIN Spacer, Small, 9mm &deg &reg test
   * 
   * @param String
   * @return String
   */
  public static String encodeHTML(String strInput) {

    if (strInput == null) {
      return "";
    }

    HashMap hmParam = new HashMap();
    String strNetval = "";

    hmParam.put(String.valueOf('\u00B0'), "&deg;");
    hmParam.put(String.valueOf('\u00AE'), "&reg;");
    hmParam.put(String.valueOf('\u2122'), "&trade;");

    Iterator iter = hmParam.keySet().iterator();

    while (iter.hasNext()) {
      strNetval = iter.next().toString();
      strInput = strInput.replaceAll(strNetval, (String) hmParam.get(strNetval));
    }

    return strInput;
  }

  /**
   * replaceForXML - this method will replace and replace it with a HTML symbol If the Input string
   * is null, it returns "" I/P: SUSTAIN Spacer, Small, 9mm ? ? test O/P: SUSTAIN Spacer, Small, 9mm
   * &deg &reg test
   * 
   * @param String
   * @return String
   */
  public static String replaceForXML(String strInput) {
    if (strInput == null) {
      return "";
    }
    strInput = strInput.replaceAll("'", "&#39"); // &#39
    strInput = strInput.replaceAll("&", "&amp;");

    return strInput;
  }

  /**
   * replaceForHTML - this method will replace and replace it with a HTML symbol If the Input string
   * is null, it returns "" *
   * 
   * @param String
   * @return String
   */
  public static String replaceForHTML(String strInput) {
    if (strInput == null) {
      return "";
    }
    strInput = strInput.replaceAll("'", "&#39"); // &#39
    return strInput;
  }

  /**
   * This method split the array list value.
   * 
   * @param alList ,splitCount
   * @return
   */
  public ArrayList splitArrayList(ArrayList alList, int splitCount) {
    ArrayList alReturn = new ArrayList();
    AbstractList alSubList = null;
    ArrayList alTempList = null;

    for (int count = 0; count <= alList.size(); count += splitCount) {
      alTempList = new ArrayList();
      int destCount = count + splitCount > alList.size() ? alList.size() : count + splitCount;
      alSubList = (AbstractList) alList.subList(count, destCount);
      alTempList.addAll(alSubList);
      if (alTempList.size() > 0)
        alReturn.add(alTempList);
    }

    log.debug("alReturn : " + alReturn.size());

    return alReturn;
  }


  /**
   * This method save the load status.
   * 
   * @param hmParams
   * @throws AppError
   */
  public static void saveLoadStatus(HashMap hmParams, GmDataStoreVO gmDataStoreVO) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(gmDataStoreVO);

    String strLogName = (String) hmParams.get("LOGNAME");
    String strStartTime = (String) hmParams.get("STARTTIME");
    String strEndTime = (String) hmParams.get("ENDTIME");
    String strStatusDetails = (String) hmParams.get("STATUSDETAILS");
    String strStatusFlag = (String) hmParams.get("STATUSFL");

    gmDBManager.setPrepareString("gm_cm_sav_load_status", 5);
    gmDBManager.setString(1, strLogName);
    gmDBManager.setString(2, strStartTime);
    gmDBManager.setString(3, strEndTime);
    gmDBManager.setString(4, strStatusDetails);
    gmDBManager.setString(5, strStatusFlag);

    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * This method send the Email from EmailTempleate Properties
   * 
   * @param templateId ,alBody ,hmAdditionalParams
   * @throws Exception
   */
  public static void sendEmailFromTemplate(String templateId, ArrayList alBody,
      HashMap hmAdditionalParams) throws Exception {
    // getting the company id
    String strCompanyId = GmCommonClass.parseNull((String) hmAdditionalParams.get("COMPANY_ID"));
    strCompanyId = strCompanyId.equals("") ? "1000" : strCompanyId;
    // getting the locale and email properties
    String strCompanyLocale = getCompanyLocale(strCompanyId);
    String strMailTo = GmCommonClass.parseNull((String) hmAdditionalParams.get("EXP_MAIL_TO"));//mail id from rule table
    strMailTo = strMailTo.equals("")?"":","+strMailTo;
    GmResourceBundleBean gmResourceBundleBean =
    		GmCommonClass.getResourceBundleBean("properties.Email."+System.getProperty("ENV_TYPE"), strCompanyLocale);
    String strTo = gmResourceBundleBean.getProperty(templateId + "." + GmEmailProperties.TO)+strMailTo;
    String strCc = gmResourceBundleBean.getProperty(templateId + "." + GmEmailProperties.CC);
    String strBcc = gmResourceBundleBean.getProperty(templateId + "." + GmEmailProperties.BCC);
    String strFrom = gmResourceBundleBean.getProperty(templateId + "." + GmEmailProperties.FROM);
    String strSubject = gmResourceBundleBean.getProperty(templateId + "." + GmEmailProperties.SUBJECT);
    String strMimeType =
    		gmResourceBundleBean.getProperty(templateId + "." + GmEmailProperties.MIME_TYPE);

    String strMessage = gmResourceBundleBean.getProperty(templateId + "." + GmEmailProperties.MESSAGE);
    String strMessageHeader =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty(templateId + "."
            + GmEmailProperties.MESSAGE_HEADER));
    strMessage =
        replaceAll(strMessage, "#<" + GmEmailProperties.MESSAGE_HEADER + ">", strMessageHeader);

    String strMessageFooter =
        GmCommonClass.parseNull(gmResourceBundleBean.getProperty(templateId + "."
            + GmEmailProperties.MESSAGE_FOOTER));
    strMessage =
        replaceAll(strMessage, "#<" + GmEmailProperties.MESSAGE_FOOTER + ">", strMessageFooter);

    String strMessageBody =
    		gmResourceBundleBean.getProperty(templateId + "." + GmEmailProperties.MESSAGE_BODY);

    // log.debug("strMessageBody = " + strMessageBody);
    StringBuffer sbBody = new StringBuffer();
    for (Iterator iter = alBody.iterator(); iter.hasNext();) {
      String strBody = strMessageBody;
      HashMap hmRow = (HashMap) iter.next();
      Set keySet = hmRow.keySet();

      String key = "";
      String value = "";
      for (Iterator keys = keySet.iterator(); keys.hasNext();) {
        key = (String) keys.next();
        Object obj = hmRow.get(key);
        if (obj != null && !obj.toString().equals(""))
          value = obj.toString();
        else
          value = "";
        // log.debug("Replacing " + "#<" + key + ">" + " with " + value);
        strBody = replaceAll(strBody, "#<" + key + ">", value);
      }
      sbBody.append(strBody);
    }
    strMessage =
        replaceAll(strMessage, "#<" + GmEmailProperties.MESSAGE_BODY + ">", sbBody.toString());

    Set keySet = hmAdditionalParams.keySet();
    // log.debug("hmAdditionalParams = " + hmAdditionalParams);
    for (Iterator keys = keySet.iterator(); keys.hasNext();) {
      String key = (String) keys.next();
      String value = (String) hmAdditionalParams.get(key);
      strMessage = replaceAll(strMessage, "#<" + key + ">", value);
      strSubject = replaceAll(strSubject, "#<" + key + ">", value);
    }
    // log.debug("strMessage = " + strMessage);

    GmEmailProperties emailProps = new GmEmailProperties();
    emailProps.setSender(strFrom);
    emailProps.setRecipients(strTo);
    emailProps.setCc(strCc);
    emailProps.setBcc(strBcc);
    emailProps.setSubject(strSubject);
    emailProps.setMimeType(strMimeType);
    emailProps.setMessage(strMessage);

    GmCommonClass.sendMail(emailProps);

  }

  /**
   * This method replcae the String value based on old and new string values
   * 
   * @param string , oldString , newString
   * @return
   */
  public static String replaceAll(String string, String oldString, String newString) {
    if (string != null && oldString != null && newString != null) {
      int i = 0;
      if ((i = string.indexOf(oldString, i)) >= 0) {
        char[] string2 = string.toCharArray();
        char[] newString2 = newString.toCharArray();
        int oLength = oldString.length();
        StringBuffer buf = new StringBuffer();
        buf.append(string2, 0, i).append(newString2);
        i += oLength;
        int j = i;
        while ((i = string.indexOf(oldString, i)) > 0) {
          buf.append(string2, j, i - j).append(newString2);
          i += oLength;
          j = i;
        }
        buf.append(string2, j, string2.length - j);
        return buf.toString();
      }
    }
    return string;
  }

  /**
   * This method returns a string from the Constant Properties file based on the input as a keyword
   * 
   * @param strKey
   * @return
   */
  public static String getEmailProperty(String strKey) {
    try {
      ResourceBundle rb = ResourceBundle.getBundle(System.getProperty("ENV_TYPE"));
      return rb.getString(strKey);
    } catch (MissingResourceException e) {
      log.error(strKey + " not found in EmailTemplate.properties");
      return null;
    }
  }

  /**
   * 
   * @param strInput
   * @return
   */
  public static String encodeForJS(String strInput) {
    String strOutput = "";

    if (strInput == null || strInput.equals("")) {
      return "";
    } else {
      strOutput = strInput.replaceAll(" ", "&nbsp;");
    }
    return strOutput;
  }

  /**
   * getStringTillLength - retusns an array of splited strings of original string till the length
   * provided
   * 
   * @param String strToken, int strLength, String strOrg
   * @return String[]
   */
  public static String[] getStringTillLength(String strToken, int strLength, String strOrg) {
    String[] strRetArray = new String[15];
    String strTemp = "";
    String strStringWithOrgLen = "";
    int iIndex = 0;
    int iLengthCnt = 0;
    int iLength = 0;
    while (!strOrg.equals("")) {
      iLength = strOrg.length();
      if (iLength <= strLength) {
        strRetArray[iLengthCnt] = strOrg;
        strOrg = "";
      } else {
        strStringWithOrgLen = strOrg.substring(0, strLength);
        iIndex = strStringWithOrgLen.lastIndexOf(strToken);
        strTemp = strOrg.substring(0, iIndex + 1);
        strRetArray[iLengthCnt] = strTemp;
        strOrg = strOrg.substring(iIndex + 1);
        iLengthCnt++;
      }
    }
    return strRetArray;
  }

  /**
   * This method is used to chop the input data to the length that is passed in. From the chopped
   * string,we will get the string ending with the delimiter and that will be added to the
   * ArrayList.
   * 
   * For eg: From this Input, 123456#12344444#2234555#33333# , we will get a list of
   * [123456#,12344444#,2234555#,33333#]
   * 
   * @param String strInput, int truncateAt, String delimiter
   * @return ArrayList
   */
  public static ArrayList convertStringToList(String strInput, int truncateAt, String delimiter) {

    String strTemp = "";
    ArrayList outList = new ArrayList();

    if (truncateAt == 0) {
      outList.add(strInput);
      return outList;
    }

    if (strInput != null && strInput.length() > truncateAt) {

      while (strInput.length() > truncateAt) {
        if (strInput.indexOf(delimiter) != -1) {

          strTemp = strInput.substring(0, truncateAt);
          strInput = strInput.substring((truncateAt));
          strInput = strTemp.substring(strTemp.lastIndexOf(delimiter) + 1) + strInput;
          strTemp = strTemp.substring(0, strTemp.lastIndexOf(delimiter) + 1);
          outList.add(strTemp);
        }
      }
    }
    outList.add(strInput);
    return outList;
  }

  /**
   * This method return the rule value.
   * 
   * @param strStudyId , strStudyGrpId
   * @return String
   * @throws AppError
   */
  public static String getRuleValue(String strStudyId, String strStudyGrpId) throws AppError {

    String strRuleValue = "";

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("get_rule_value", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strStudyId);
    gmDBManager.setString(3, strStudyGrpId);
    gmDBManager.execute();
    strRuleValue = gmDBManager.getString(1);
    gmDBManager.close();

    return strRuleValue;
  }

  /**
   * This method convert the String to date object and return the value
   * 
   * @param strDate , strformat
   * @return
   * @throws ParseException
   */
  public static Date getStringToDate(String strDate, String strformat) throws ParseException {

    Date date = null;
    if (!strDate.equals("")) {
      DateFormat formatter = new SimpleDateFormat(strformat);
      date = formatter.parse(strDate);
      log.debug("date passed:" + date.toString());
    }
    return date;

  }

  /**
   * This method convert the Date to String in given time zone and return the value
   * 
   * @param dtDate , strOutputFormat, strTimeZone
   * @return String
   */
  public static String getStringFromDate(Date dtDate, String strformat, String strTimeZone)
      throws ParseException {
    DateFormat df = new SimpleDateFormat(strformat);
    df.setTimeZone(TimeZone.getTimeZone(strTimeZone));
    return df.format(dtDate.getTime());
  }

  /**
   * This method convert the Date to String and return the value
   * 
   * @param dtDate , strOutputFormat
   * @return String
   */
  public static String getStringFromDate(Date dtDate, String strOutputFormat) {
    String strFormattedDate = "";
    if (dtDate != null) {
      DateFormat formatter = new SimpleDateFormat(strOutputFormat);
      strFormattedDate = formatter.format(dtDate);
    }
    return strFormattedDate;
  }

  /**
   * This method returns the current date value.
   * 
   * @param strformat
   * @return
   * @throws ParseException
   */

  public static Date getCurrentDate(String strformat) throws ParseException {

    Date date;
    Calendar cal = Calendar.getInstance();
    DateFormat formatter = new SimpleDateFormat(strformat);
    date = formatter.parse((formatter.format(cal.getTime())));

    log.debug("date current:" + date.toString());
    return date;
  }

  /**
   * This method returns the current date value with respect to given time zone
   * 
   * @param strformat, strTimeZone
   * @return
   * @throws ParseException
   */
  public static Date getCurrentDate(String strformat, String strTimeZone) throws ParseException {

    Date date;
    Calendar cal = Calendar.getInstance();
    DateFormat dfmt = new SimpleDateFormat(strformat);

    DateFormat dfmtTz = new SimpleDateFormat(strformat);
    dfmtTz.setTimeZone(TimeZone.getTimeZone(strTimeZone));

    date = dfmt.parse((dfmtTz.format(cal.getTime())));
    return date;
  }

  /**
   * This method return the value Arraylist value from the string.
   * 
   * @param inputString , tokenString
   * @return
   */
  public static ArrayList getListFromString(String inputString, String tokenString) {
    ArrayList tokenizedList = null;
    if (inputString != null) {
      tokenizedList = new ArrayList();
      StringTokenizer stringTokenizer = new StringTokenizer(inputString, tokenString);
      while (stringTokenizer.hasMoreTokens()) {
        tokenizedList.add(stringTokenizer.nextToken());
      }
    }
    return tokenizedList;
  }

  /**
   * convert long integer into Polish words. e.g. -12345 -> "minus twelve thousand forty-five"
   * Handles negative and positive integers on range -Long.MAX_VALUE .. Long.MAX_VALUE; It cannot
   * handle Long.MIN_VALUE;
   * 
   * @param num number to convert to words
   * 
   * @return words
   */
  public static String toPolishWords(long num) {
    if (num == 0) {
      return ZERO;
    }
    boolean negative = (num < 0);
    if (negative) {
      num = -num;
    }

    String s = "";
    // Work least significant digit to most, right to left.
    // until high order part is all 0s.
    for (int group = 0; num > 0; group++) {
      int remdr = (int) (num % divisor[group]);
      num = num / divisor[group];
      if (remdr == 0) {
        continue;
      }
      String t;
      if (remdr == 1) {
        switch (group) {
          case 0:
            /* doing units */
            t = lowName[1];
            break;

          default:
            /* higher numbers, suppress one */
            t = "";
            break;
        }// end switch
      }// end if
      else if (remdr < 10) {
        switch (group) {
          case 1:
            /* doing hundreds, count is part of groupname */
            t = "";
            break;

          default:
            t = lowName[remdr];
            break;
        }// end switch
      }// end if
      else if (remdr < 20) {
        t = lowName[remdr];
      }// end if
      else if (remdr < 100) {
        int units = remdr % 10;
        int tens = remdr / 10;
        t = tys[tens];
        if (units != 0) {
          t += " " + lowName[units];
        }
      }// end if
      else {
        t = toPolishWords(remdr);
      }
      boolean leftPad = (t.length() != 0);
      s = t + (leftPad ? " " : "") + getGroupName(remdr, group) + " " + s;
    }// end for
    s = s.trim();
    if (negative) {
      s = MINUS + " " + s;/* guess word for negative */
    }
    return s;
  }// end inWords

  // -------------------------- STATIC METHODS --------------------------

  /*
   * In Polish the group name depends on the multiplier. There is a different word for hundred in
   * 100, 200, 300 etc.
   */
  private static String getGroupName(int multiplier, int range) {
    switch (range) {
      case 0:/* units */
        return "";

      case 1:/* hundreds */
        /* mulitplier will always be 0 ..9 */
        return hundredsGroupName[multiplier];

      case 2:/* thousands */
        if (10 <= multiplier && multiplier <= 19) {
          multiplier = 9;
        } else if (multiplier >= 20) {
          multiplier = multiplier % 10;
          if (multiplier == 0) {
            multiplier = 9;
          }
        }
        return thousandsGroupName[multiplier];

      default:/* millions on up */

        if (multiplier < 10) {
          switch (multiplier) {
            case 0:
              return "";

            case 1:
              return groupName[range];

            case 2:
            case 3:
            case 4:
              return groupName[range] + 'y';

            default:
              return groupName[range] + "\u00f3w";
          }
        } else {
          // 10 .. 99
          switch (multiplier % 10) {
            case 2:
            case 3:
            case 4:
              return groupName[range] + 'y';

            default:
              return groupName[range] + "\u00f3w";
          }// end switch
        }
    }// end switch
  }// end getGroupName

  public static String convetToPolishWords(double dbNumber, String strCompanyId) throws AppError {
    String strWord = "";
    String strNumber = "";
    HashMap hmCurrency = new HashMap();
    hmCurrency = GmCommonClass.getCurrSymbolFormatfrmComp(strCompanyId);
    String strCmpCurrSymbl = GmCommonClass.parseNull((String) hmCurrency.get("CMPCURRSMB"));

    try {
      dbNumber = roundDigit(dbNumber, 2); // rounded number upto 2 digits
      strNumber = "" + dbNumber;
      strWord = toPolishWords(Long.parseLong(strNumber.substring(0, strNumber.indexOf("."))));
      strNumber = strNumber.substring((strNumber.indexOf(".") + 1), strNumber.length());
      strNumber = (strNumber.length() == 1) ? (strNumber + "0") : strNumber;
      strWord = strWord + " " + strCmpCurrSymbl + " gr " + strNumber + "/100";
    } catch (Exception e) {
      // System.out.println("IN SEND MAIL");
      e.printStackTrace();
      throw new AppError(e);
    }
    return strWord;
  }

  /**
   * This method return the round digit values
   * 
   * @param dblNumber ,noOfDigit
   * @return
   */
  public static double roundDigit(double dblNumber, int noOfDigit) {
    double p = Math.pow(10, noOfDigit);
    dblNumber = dblNumber * p;
    double tmp = Math.round(dblNumber);
    return tmp / p;
  }

  /**
   * This method used to get the shipping properties
   * 
   * @return
   */
  public static HashMap getShippingProperties() {
    Enumeration<String> keys = rb.getKeys();
    HashMap hmResult = new HashMap();

    while (keys.hasMoreElements()) {
      String strKey = keys.nextElement();
      if (strKey.contains("GROUND") || strKey.contains("PRIORITY")) {
        String strValue = rb.getString(strKey);
        hmResult.put(strKey, strValue);
      }

    }
    return hmResult;
  }

  /**
   * This method used to send mail based on the in parameter send by the user
   * 
   * @param from,to,cc,bcc,subject,message and attachment which have all mail related data.
   * @exception AppError
   * @return Boolean
   */
  public static boolean sendMail(String strFrom, String[] strTo, String[] strCc, String[] strBcc,
      String strSubject, String strMessageDesc, String strMimeType, String strAttachment,
      String[] strReplyTo) throws AppError {
	  
	  GmQueueProducer qprod = new GmQueueProducer();
	  GmMessageTransferObject tf = new GmMessageTransferObject();
		HashMap hmJMSParam = new HashMap();
	  	hmJMSParam.put("TO", strTo);
	  	hmJMSParam.put("CC", strCc);
	  	hmJMSParam.put("BCC", strBcc);
	  	hmJMSParam.put("STRREPLYTO", strReplyTo);
	  	hmJMSParam.put("STRFROM", strFrom);
	  	hmJMSParam.put("STRSUBJECT", strSubject);
	  	hmJMSParam.put("STRMESSAGEDESC", strMessageDesc);
	  	hmJMSParam.put("STREMAILATTACHMENT", strAttachment);
	  	hmJMSParam.put("STRMIMETYPE", strMimeType);
	  	hmJMSParam.put("WHITELISTSERVER", (Boolean)blWhitelistserver);
	  	hmJMSParam.put("TESTINGSERVER", (Boolean)blTestingserver);
	  	hmJMSParam.put("TOEMAIL", toEmail);
	  	hmJMSParam.put("TESTEMAILSUBJECT", strTestEmailSubject);
	  	hmJMSParam.put("TESTEMAILID", strTestEmailID);
	  	
		// This is JMS Code which will process the notification, jms/gmemailqueue is the JMS
	    // queue using for notification
	    tf.setMessageObject(hmJMSParam);
	    tf.setConsumerClass(GmEmailBean.emailConsumerClass);
	    qprod.sendMessage(tf,GmEmailBean.emailQueueName);
	    return true;
  } // end fo sendMail method

  /**
   * getLastWorkingDay This method will fetch the last working days
   * 
   * @param Date : dtDate
   * @return Date
   * @exception AppError
   */
  public static Date getLastWorkingDay(Date dtDate) throws AppError {

    Date dtLastWorkingDt = null;

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("get_last_working_day", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.DATE);
    gmDBManager.setDate(2, dtDate);
    gmDBManager.execute();
    dtLastWorkingDt = (Date) gmDBManager.getObject(1);
    gmDBManager.close();

    return dtLastWorkingDt;
  } // End getLastWorkingDay

  /*
   * @param strCodeGrp - Code Grp
   * 
   * @exception AppError
   * 
   * @return ArrayList object
   */
  public static ArrayList getCodeListNotIn(String strCodeGrp) throws AppError {
    return getCodeListNotIn(strCodeGrp, "");
  }

  /**
   * @param strCodeGrp - Code Grp *
   * @exception AppError
   * @return ArrayList object
   */
  public static ArrayList getCodeListNotIn(String strCodeGrp, String strCodeId) throws AppError {
    ArrayList arList = new ArrayList();
    StringBuffer sbQuery = new StringBuffer();
    String strCodeGrpParsed = getStringWithQuotes(strCodeGrp);
    GmCacheManager gmCacheManager = new GmCacheManager();
    sbQuery.append("SELECT  C901_CODE_ID CODEID, C901_CODE_NM CODENM ");
    sbQuery
        .append(",C902_CODE_NM_ALT CODENMALT, C901_CONTROL_TYPE CONTROLTYP,C901_CODE_SEQ_NO CODESEQNO");
    sbQuery.append(" FROM  T901_CODE_LOOKUP ");
    sbQuery.append(" WHERE  C901_CODE_GRP IN ('" + strCodeGrpParsed + "')");
    sbQuery.append(" AND C901_ACTIVE_FL = '1' ");
    if (!strCodeId.equals("")) {
      sbQuery.append(" AND c901_code_id NOT IN (");
      sbQuery.append(strCodeId);
      sbQuery.append(")");
    }
    sbQuery.append(" ORDER BY C901_CODE_SEQ_NO ");
    log.debug("sbQuery.toString()" + sbQuery.toString());
    gmCacheManager.setKey("getCodeListNotIn:" + strCodeGrp + ":" + strCodeId);
    arList = gmCacheManager.queryMultipleRecords(sbQuery.toString());

    if (arList.size() < 0) {
      throw new AppError(AppError.APPLICATION, "1002");
    }
    return arList;
  }

  
  /**
 * fetchRuleValuesByGroup - This method used to fetch rule values with respective group
 * 
 * @param strRuleGrp
 * @param strCompanyId
 * @return
 * @throws AppError
 */
public static ArrayList fetchRuleValuesByGroup(String strRuleGrp,String strCompanyId) throws AppError{
	  
		ArrayList alList = new ArrayList();
		StringBuffer sbQuery = new StringBuffer();
		GmDBManager gmDBManager = new GmDBManager();
		sbQuery.append("SELECT C906_RULE_ID RULEID, C906_RULE_VALUE RULEVALUE ");
		sbQuery.append("FROM T906_RULES ");
		sbQuery.append("WHERE C906_RULE_GRP_ID ='"+strRuleGrp+"'");
		sbQuery.append(" AND C906_VOID_FL IS NULL");
	   
	   if(!strCompanyId.equals(""))
	   {
		   sbQuery.append(" AND C1900_COMPANY_ID ='"+strCompanyId+"'");
	   }
	  
	   log.debug("sbQuery.toString()" + sbQuery.toString());
	   alList = gmDBManager.queryMultipleRecords(sbQuery.toString());
	   
	   return alList;
  }
  
  
  
  
  
  
  
  /**
   * Author Velu
   * 
   * @param alRemoveCodeIDs
   * @param alResult
   * @return alResultTemp
   * @throws AppError Desctiption : When pass Code Ids, remove from hashmap in arraylist and send
   *         the same arraylist.
   */
  public static ArrayList toRemoveCodeIdsFromArrayList(ArrayList alRemoveCodeIDs, ArrayList alResult)
      throws AppError {
    ArrayList alResultTemp = alResult;
    if (alResultTemp != null && alResultTemp.size() > 0) {
      for (int i = 0; i < alResultTemp.size(); i++) {
        HashMap hmTemp = (HashMap) alResultTemp.get(i);
        String strCodeId = parseNull((String) hmTemp.get("CODEID"));
        if (alRemoveCodeIDs.contains(strCodeId))
          alResultTemp.remove(i);
      }
    }
    return alResultTemp;
  } // End toRemoveCodeIdsFromArrayList


  // Below is for converting number into words.
  public static String convertToWords(int n) {
    amount = numToString(n);
    String converted = "";
    int pos = 1;
    boolean hun = false;
    while (amount.length() > 0) {
      if (pos == 1) // TENS AND UNIT POSITION
      {
        if (amount.length() >= 2) // 2DIGIT NUMBERS
        {
          String C = amount.substring(amount.length() - 2, amount.length());
          amount = amount.substring(0, amount.length() - 2);
          converted += digits(C);
        } else if (amount.length() == 1) // 1 DIGIT NUMBER
        {
          converted += digits(amount);
          amount = "";
        }
        pos++; // INCREASING POSITION COUNTER
      } else if (pos == 2) // HUNDRED POSITION
      {
        String C = amount.substring(amount.length() - 1, amount.length());
        amount = amount.substring(0, amount.length() - 1);
        if (converted.length() > 0 && digits(C) != "") {
          converted = (digits(C) + maxs[pos] + " and") + converted;
          hun = true;
        } else {
          if (digits(C) == "")
            ;
          else
            converted = (digits(C) + maxs[pos]) + converted;
          hun = true;
        }
        pos++; // INCREASING POSITION COUNTER
      } else if (pos > 2) // REMAINING NUMBERS PAIRED BY TWO
      {
        if (amount.length() >= 2) // EXTRACT 2 DIGITS
        {
          String C = amount.substring(amount.length() - 2, amount.length());
          amount = amount.substring(0, amount.length() - 2);
          if (!hun && converted.length() > 0)
            converted = digits(C) + maxs[pos] + " and" + converted;
          else {
            if (digits(C) == "")
              ;
            else
              converted = digits(C) + maxs[pos] + converted;
          }
        } else if (amount.length() == 1) // EXTRACT 1 DIGIT
        {
          if (!hun && converted.length() > 0)
            converted = digits(amount) + maxs[pos] + " and" + converted;
          else {
            if (digits(amount) == "")
              ;
            else
              converted = digits(amount) + maxs[pos] + converted;
            amount = "";
          }
        }
        pos++; // INCREASING POSITION COUNTER
      }
    }
    return converted;
  }

  /**
   * @param ArrayList alAttrValues, String strKey
   * @return String
   * @throws AppError Desctiption : From the input ArrayList, Loop through it and get the HashMap.
   *         For the input passing strKey form a string with commoa seperated like
   *         "value1,value2,value3..valuen" & return it. Eg: Input List : [{ATTRVAL=103085,
   *         ID=103119}, {ATTRVAL=103086, ID=103119}, {ATTRVAL=103087, ID=103119}] strKey : ATTRVAL
   *         Returns : 103085,103086,103087
   */
  public static String createStringFromListbyKey(ArrayList alList, String strKey) throws AppError {

    int intSize = 0;
    String strValue = "";
    String strReturn = "";
    intSize = alList.size();
    HashMap hmValues = new HashMap();

    if (intSize > 0) {
      for (int i = 0; i < intSize; i++) {
        hmValues = (HashMap) alList.get(i);
        strValue = GmCommonClass.parseNull((String) hmValues.get(strKey));
        if (!strValue.equals("")) {
          strReturn += strValue + ',';
        }
      }
      strReturn = strReturn.substring(0, strReturn.length() - 1);
    }
    return strReturn;
  }

  private static String digits(String C) // TO RETURN SELECTED NUMBERS IN WORDS
  {
    String converted = "";
    for (int i = C.length() - 1; i >= 0; i--) {
      int ch = C.charAt(i) - 48;
      if (i == 0 && ch > 1 && C.length() > 1)
        converted = tens[ch - 2] + converted; // IF TENS DIGIT STARTS WITH 2 OR MORE IT FALLS UNDER
                                              // TENS
      else if (i == 0 && ch == 1 && C.length() == 2) // IF TENS DIGIT STARTS WITH 1 IT FALLS UNDER
                                                     // TEENS
      {
        int sum = 0;
        for (int j = 0; j < 2; j++)
          sum = (sum * 10) + (C.charAt(j) - 48);
        return teen[sum - 10];
      } else {
        if (ch > 0)
          converted = units[ch] + converted;
      } // IF SINGLE DIGIT PROVIDED
    }
    return converted;
  }

  private static String numToString(int n) // CONVERT THE NUMBER TO STRING
  {
    String num = "";
    while (n != 0) {
      num = ((char) ((n % 10) + 48)) + num;
      n /= 10;
    }
    return num;
  }

  /**
   * @param String strAccAttributeValue
   * @return String
   * @throws AppError Desctiption : Passing Account ID and type to get the attribut value
   */
  public static String getAccountAttributeValue(String strAccId, String strAttribType)
      throws AppError {

    String strAccAttributeValue = "";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("GET_ACCOUNT_ATTRB_VALUE", 2);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strAccId);
    gmDBManager.setString(3, strAttribType);
    gmDBManager.execute();
    strAccAttributeValue = gmDBManager.getString(1);
    gmDBManager.close();

    return strAccAttributeValue;
  }

  /**
   * @param RowSetDynaClass rowSetDynaClass
   * @return ArrayList
   * @throws AppError Desctiption : Passing rowSetDynaClass value and get the value as ArrayList
   */
  public static ArrayList convertDynaToArrayList(RowSetDynaClass rowSetDynaClass) throws AppError {

    HashMap hmReturn = null;
    ArrayList alDynaVal = new ArrayList();
    ArrayList alResult = new ArrayList();
    int size = 0;
    String strDynaValue = "";
    DynaProperty dynaProperty[] = rowSetDynaClass.getDynaProperties();
    alDynaVal = (ArrayList) rowSetDynaClass.getRows();
    size = alDynaVal.size();
    if (size > 0) {
      for (int i = 0; i < size; i++) {
        hmReturn = new HashMap();
        BasicDynaBean dynaBean = (BasicDynaBean) alDynaVal.get(i);
        for (int j = 0; j < dynaProperty.length; j++) {
          if (dynaBean.get(dynaProperty[j].getName()) != null)
            strDynaValue = dynaBean.get(dynaProperty[j].getName()).toString();
          hmReturn.put(dynaProperty[j].getName(), strDynaValue);
        }
        alResult.add(hmReturn);
      }
    }
    return alResult;
  }

  /**
   * Desctiption : Pass days which will be added to current date and pass the desired format
   * required
   * 
   * @param intDays
   * @param dtOutFormat
   * @return
   * @throws ParseException
   */
  public static String getAddDaysToCurrentDateWithFormat(int intDays, Date dtInDate,
      String dtOutFormat) throws ParseException {
    Calendar now = Calendar.getInstance();
    now.setTime(dtInDate);
    now.add(Calendar.DATE, intDays);

    SimpleDateFormat sdf = new SimpleDateFormat(dtOutFormat);
    String date = sdf.format(now.getTime());
    return date;
  }

  /**
   * encodeToUTF8 - this method will set the UTF8 format for the input
   * 
   * @param String
   * @return String
   * @throws UnsupportedEncodingException
   */
  public static String encodeToUTF8(String strInput) throws UnsupportedEncodingException {
    if (strInput == null) {
      return "";
    }
    // ISO-8859-1(Charset) ISO Latin Alphabet No. 1, a.k.a. ISO-LATIN-1(Description)
    // To get an Unicode String (UTF-8)
    strInput = new String(strInput.getBytes("8859_1"), "UTF-8");

    return strInput;
  }

  /**
   * This Temp method to get default Data store VO. It will use to call default bean constructor
   * which is extends GmBean
   * 
   * @return
   */
  public static GmDataStoreVO getDefaultGmDataStoreVO() {
    GmDataStoreVO gmDefaultDataStoreVO = new GmDataStoreVO();
    gmDefaultDataStoreVO.setCmpid(GmCommonClass.getString("COMPANYID"));
    gmDefaultDataStoreVO.setCmpdfmt(GmCommonClass.getString("COMPANYDATEFORMAT"));
    gmDefaultDataStoreVO.setCmptzone(GmCommonClass.getString("COMPANYTIMEZONE"));
    gmDefaultDataStoreVO.setPlantid(GmCommonClass.getString("COMPANYDEFAULTPLANT"));
    gmDefaultDataStoreVO.setCmplangid(GmCommonClass.getString("COMPANYDEFAULTLANG"));
    return gmDefaultDataStoreVO;
  }

  /**
   * getXmlGridData: This method is used to prepare xml string for vm file
   * 
   * @param HashMap
   * @return String
   * @exception AppError
   */

  public static String getXmlGridData(HashMap hmTemplateParam) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmTemplateParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmTemplateParam.get("VMFILEPATH"));
    templateUtil.setDataList("alGridData", (ArrayList) hmTemplateParam.get("DATA_LIST"));
    templateUtil.setDataMap("hmParam", (HashMap) hmTemplateParam.get("DATA_MAP"));
    templateUtil.setTemplateName((String) hmTemplateParam.get("TEMPLATE_NAME"));
    templateUtil.setTemplateSubDir((String) hmTemplateParam.get("TEMPLATE_SUB_DIR"));
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));

    return templateUtil.generateOutput();
  }

  /**
   * getCurrSymbolFormatfrmComp: This method is used to return the currecny format and currecny
   * symbol for the company.Passing the company id to this method and getting the result in HashMap.
   * 
   * @param strCompId:passing the company id from VO.
   * @return HashMap :returns the currecny symbool and currecny format.
   * @exception AppError
   */
  public static HashMap getCurrSymbolFormatfrmComp(String strCompId) throws AppError {

    HashMap hmParam = new HashMap();
    // passing the company id to the static HashMap .
    GmCompanyVO compVO = (new GmCommonBean()).getCompanyVO(strCompId);
    hmParam.put("CMPRPTCURRSMB", compVO.getCmprptcurrsmb());
    hmParam.put("CMPCURRSMB", compVO.getCmpcurrsmb());
    hmParam.put("CMPCURRFMT", compVO.getCmpcurrfmt());
    hmParam.put("CURRTYPE", compVO.getCurrtype());
    hmParam.put("TXNCURRENCYID", compVO.getTxncurrencyid());
    return hmParam;
  }

  /**
   * getCompanyLocale: This method is used to return the locale for the company.
   * 
   * @param strCompId
   * @return String
   * @throws AppError
   */
  public static String getCompanyLocale(String strCompId) throws AppError {

    HashMap hmParam = new HashMap();
    // passing the company id to the static HashMap .
    GmCompanyVO compVO = GmCommonBean.hmCompany.get(strCompId);
    return compVO.getCompanylocale();
  }

  /**
   * getCompanyLanguage: This method is used to return the Language id for the company.
   * 
   * @param strCompId
   * @return String
   * @throws AppError
   */
  public static String getCompanyLanguage(String strCompId) throws AppError {

    HashMap hmParam = new HashMap();
    // passing the company id to the static HashMap .
    GmCompanyVO compVO = GmCommonBean.hmCompany.get(strCompId);
    return compVO.getCmplangid();
  }

  /**
   * getCompanyName: This method is used to return the Company Name for the company.
   * 
   * @param strCompId
   * @return String
   * @throws AppError
   */
  public static String getCompanyName(String strCompId) throws AppError {

    HashMap hmParam = new HashMap();
    // passing the company id to the static HashMap .
    GmCompanyVO compVO = GmCommonBean.hmCompany.get(strCompId);
    return compVO.getCompanynm();
  }


  /**
   * getResourceBundleBean: This method is used to return the resource bundle bean object value
   * 
   * @param strPropertieName
   * @param strLocale
   * @return GmResourceBundleBean
   * @throws AppError
   */
  public static GmResourceBundleBean getResourceBundleBean(String strPropertieName, String strLocale)
      throws AppError {
    GmResourceBundleBean gmResourceBundleBean = null;
    Locale locale = null;
    strLocale = strLocale.equals("") ? "US" : strLocale;
    try {
      locale = new Locale("en", strLocale);
      gmResourceBundleBean = new GmResourceBundleBean(strPropertieName, locale);
    } catch (MissingResourceException mre) {
      locale = new Locale("en");
      gmResourceBundleBean = new GmResourceBundleBean(strPropertieName, locale);
    }
    return gmResourceBundleBean;
  }

  /**
   * fetchCompanyAddress: This method used to fetch the company address details based on company id.
   * 
   * @param strCompanyId
   * @param strDivisionId
   * @return HashMap
   * @throws AppError
   */
  public static HashMap fetchCompanyAddress(String strCompanyId, String strDivisionId)
      throws AppError {
    HashMap hmReturn = new HashMap();
    GmResourceBundleBean gmResourceBundleBean =
        getResourceBundleBean("properties.Company", getCompanyLocale(strCompanyId));
    // Setting the company address details.
    hmReturn.put("GMADDRESS", gmResourceBundleBean.getProperty("GMADDRESS"));
    hmReturn.put("GMREMIT", gmResourceBundleBean.getProperty("GMREMIT"));
    hmReturn.put("GMCOMPANYNAME", gmResourceBundleBean.getProperty("GMCOMPANYNAME"));
    hmReturn.put("GMCOMPANYADDRESS", gmResourceBundleBean.getProperty("GMCOMPANYADDRESS"));
    hmReturn.put("GMCOMPSHIPADDR", gmResourceBundleBean.getProperty("GMCOMPSHIPADDR"));
    hmReturn.put("GMADDICT", gmResourceBundleBean.getProperty("GMADDICT"));
    hmReturn.put("COMP_LOGO", gmResourceBundleBean.getProperty("COMP_LOGO"));
    hmReturn.put("COUNTRYCODE", gmResourceBundleBean.getProperty("COUNTRYCODE"));
    hmReturn.put("GMCOMPANYPHONE", gmResourceBundleBean.getProperty("GMCOMPANYPHONE"));
    hmReturn.put("GMCOMPANYFAX", gmResourceBundleBean.getProperty("GMCOMPANYFAX"));
    hmReturn.put("3FLR_ADDRESS", gmResourceBundleBean.getProperty("3FLR_ADDRESS"));
    
    if (!strDivisionId.equals("")) {
      hmReturn.put("LOGO", gmResourceBundleBean.getProperty(strDivisionId + ".LOGO"));
      hmReturn.put("COMPNAME", gmResourceBundleBean.getProperty(strDivisionId + ".COMPNAME"));
      hmReturn.put("COMP_SHORT_NAME",
          gmResourceBundleBean.getProperty(strDivisionId + ".COMP_SHORT_NAME"));
      hmReturn.put("COMPADDRESS", gmResourceBundleBean.getProperty(strDivisionId + ".COMPADDRESS"));
      hmReturn.put("CUST_SERVICE",
          gmResourceBundleBean.getProperty(strDivisionId + ".CUST_SERVICE"));
      hmReturn.put("PH", gmResourceBundleBean.getProperty(strDivisionId + ".PH"));
      hmReturn.put("FAX", gmResourceBundleBean.getProperty(strDivisionId + ".FAX"));
      hmReturn.put("COMP_REMIT_TO",
          gmResourceBundleBean.getProperty(strDivisionId + ".COMP_REMIT_TO"));
    }
    return hmReturn;
  }

  /**
   * This method it to return the rule value based on company.
   * 
   * @param strStudyId , strStudyGrpId
   * @return String
   * @throws AppError
   */
  public static String getRuleValueByCompany(String strStudyId, String strStudyGrpId,
      String strCompId) throws AppError {

    String strRuleValue = "";

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("get_rule_value_by_company", 3);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strStudyId);
    gmDBManager.setString(3, strStudyGrpId);
    gmDBManager.setString(4, strCompId);
    gmDBManager.execute();
    strRuleValue = gmDBManager.getString(1);
    gmDBManager.close();

    return strRuleValue;
  }

  /**
   * This method used to retun the spring bean object based on company id
   * 
   * @param strBeanName
   * @param strCompanyId
   * @return
   * @throws AppError
   */
  public static Object getSpringBeanClass(String strBeanName, String strCompanyId) throws AppError {
    ApplicationContext context = new ClassPathXmlApplicationContext(strBeanName);
    return context.getBean("COM" + strCompanyId);
  }
  
  
  public static boolean sendEmail(HashMap hmValues) throws AppError {
		  
		  GmQueueProducer qprod = new GmQueueProducer();
		  GmMessageTransferObject tf = new GmMessageTransferObject();
		  
		  String strFrom = GmCommonClass.parseNull((String) hmValues.get("strFrom")); 
		  String[] strTo = (String[]) hmValues.get("strTo");
		  String[] strCc = (String[]) hmValues.get("strCc");
		  String[] strBcc = (String[]) hmValues.get("strBcc");
		  String strSubject = GmCommonClass.parseNull((String) hmValues.get("strSubject"));
		  String strMessageDesc = GmCommonClass.parseNull((String) hmValues.get("strMessageDesc"));
		  String strMimeType = GmCommonClass.parseNull((String) hmValues.get("strMimeType"));
		  String strAttachment = GmCommonClass.parseNull((String) hmValues.get("strAttachment"));
		  String[] strReplyTo = (String[]) hmValues.get("strReplyTo");
		  String strEmailHeader = GmCommonClass.parseNull((String) hmValues.get("strEmailHeader")); 
		  
			HashMap hmJMSParam = new HashMap();
		  	hmJMSParam.put("TO", strTo);
		  	hmJMSParam.put("CC", strCc);
		  	hmJMSParam.put("BCC", strBcc);
		  	hmJMSParam.put("STRREPLYTO", strReplyTo);
		  	hmJMSParam.put("STRFROM", strFrom);
		  	hmJMSParam.put("STRSUBJECT", strSubject);
		  	hmJMSParam.put("STRMESSAGEDESC", strMessageDesc);
		  	hmJMSParam.put("STREMAILATTACHMENT", strAttachment);
		  	hmJMSParam.put("STRMIMETYPE", strMimeType);
		  	hmJMSParam.put("WHITELISTSERVER", (Boolean)blWhitelistserver);
		  	hmJMSParam.put("TESTINGSERVER", (Boolean)blTestingserver);
		  	hmJMSParam.put("TOEMAIL", toEmail);
		  	hmJMSParam.put("TESTEMAILSUBJECT", strTestEmailSubject);
		  	hmJMSParam.put("TESTEMAILID", strTestEmailID);
            hmJMSParam.put("EMAILHEADERNM", strEmailHeader);
		  	
            // PC-3000: integrate email to sendgrid api
            hmJMSParam.put("TRANSACTION_ID", GmCommonClass.parseNull((String) hmValues.get("TRANSACTION_ID")));
            hmJMSParam.put("EMAIL_TRACKING_TYPE", GmCommonClass.parseNull((String) hmValues.get("EMAIL_TRACKING_TYPE")));
            hmJMSParam.put("EMAIL_TRACKING_FL", GmCommonClass.parseNull((String) hmValues.get("EMAIL_TRACKING_FL")));
            
			// This is JMS Code which will process the notification, jms/gmemailqueue is the JMS
		    // queue using for notification
		    tf.setMessageObject(hmJMSParam);
		    tf.setConsumerClass(GmEmailBean.emailConsumerClass);
		    qprod.sendMessage(tf,GmEmailBean.emailQueueName);
		    return true;
	  } // end of sendEmail method
  /**
   * replaceForXMLLessGreaterthan - this method will replace and replace it with a HTML symbol If the Input string
   * is null, it returns "" I/P: SUSTAIN Spacer, Small, 9mm ? ? < test O/P: SUSTAIN Spacer, Small, 9mm
   * &deg &reg &#60 test
   * 
   * @param String
   * @return String
   */
   public static String replaceForXMLLessGreaterthan(String strInput) {
	   if (strInput == null) {
		   return "";
	   }
	   strInput = strInput.replaceAll("'", "&#39"); // &#39
	   strInput = strInput.replaceAll("<", "&#60"); // &#60
	   strInput = strInput.replaceAll(">", "&#62"); // &#62
	   strInput = strInput.replaceAll("&", "&amp;");

	   return strInput;
   }
} // End of GmCommonClass