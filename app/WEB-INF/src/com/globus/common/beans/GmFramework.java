package com.globus.common.beans;

import java.net.URLEncoder;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;

import org.apache.log4j.Logger;

import com.globus.common.util.GmCookieManager;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmFramework {
  private static Logger log = GmLogger.getInstance("com.globus.common.beans.GmFramework");

  /**
   * getCompanyParams - this method will populate company info into GmDataStoreVO This method will
   * call inside instantiate(). So, make sure every servlet/action will call instantiate method.
   * 
   * @param request
   * @return GmDataStoreVO
   * @throws AppError
   */
  public static GmDataStoreVO getCompanyParams(HttpServletRequest request) throws AppError {

    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();
    gmDataStoreVO = getCompanyParams(request, null);
    return gmDataStoreVO;
  }

  /**
   * getCompanyParams - this method will populate company info into GmDataStoreVO This method will
   * call inside instantiate(). So, make sure every servlet/action will call instantiate method.
   * 
   * @param request
   * @return GmDataStoreVO
   * @throws AppError
   */
  public static GmDataStoreVO getCompanyParams(HttpServletRequest request,
      MultipartFormDataRequest mrequest) throws AppError {

    GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();

    String strCompanyInfo = getCompanyInfo(request, mrequest);
    if (!strCompanyInfo.equals("")) {
      gmDataStoreVO = (new GmWSUtil()).populateCompanyInfo(strCompanyInfo);
    }

    request.setAttribute("gmDataStoreVO", gmDataStoreVO);
    return gmDataStoreVO;
  }

  // getCompanyInfo method is overloaded to avoid multipart form data issue.
  // if the MultipartFormDataRequest is available then the below method will be called.

  public static String getCompanyInfo(HttpServletRequest request, MultipartFormDataRequest mrequest)
      throws AppError {
    String strCompanyInfo = "";
    if (mrequest != null) {
      strCompanyInfo = GmCommonClass.parseNull(mrequest.getParameter("companyInfo"));
    } else {
      strCompanyInfo = GmCommonClass.parseNull(request.getParameter("companyInfo"));
    }

    return strCompanyInfo;
  }

  /**
   * appendParam - this method will used to append given companyInfo into URL string
   * 
   * @param String URL string
   * @param HttpServletRequest request
   * @return String AppendedURL
   * @throws AppError
   */
  public static String appendCompanyInfo(String strURL, HttpServletRequest request) throws AppError {
    return appendParam(strURL, "companyInfo", getCompanyInfo(request, null));
  }

  /**
   * appendParam - this method will used to whether given parameter is existing in given URL string.
   * If not exist then append given parameter into URL string
   * 
   * @param String URL string
   * @param String parameter
   * @param String value
   * @return String AppendedURL
   * @throws AppError
   */
  public static String appendParam(String strURL, String strParam, String strValue) {
    String strAppendedURL = strURL;
    if (!strValue.equals("") && !isParamExist(strURL, strParam)) {
      // Remove old parameters with same name has empty value
      strAppendedURL = strAppendedURL.replaceAll("[?|&]" + strParam + "=", "");
      strAppendedURL += getURLDelimiter(strURL) + strParam + "=" + URLEncoder.encode(strValue);
    }
    log.debug(strAppendedURL);
    return strAppendedURL;
  }


  /**
   * isParamExist: This method is used to check whether given parameter is existing in given URL
   * string. Return true if parameter is existing and value is not empty. Otherwise, false.
   * 
   * @param String URL string
   * @param String parameter string
   * @return boolean
   * @exception AppError
   */
  public static boolean isParamExist(String strURL, String strParam) {
    boolean blExist = false;

    strURL = GmCommonClass.parseNull(strURL);
    // check whether the URL has parameter string and given parameter name exist or not.
    if (strURL.indexOf("?") == -1 || strURL.indexOf(strParam) == -1)
      return blExist;

    String query = strURL.substring(strURL.indexOf("?") + 1);
    String[] strParams = query.split("&");
    for (String param : strParams) {
      String[] arryParam = param.split("=");
      String strName = arryParam[0];
      String strValue = arryParam.length > 1 ? arryParam[1] : "";
      if (strName.equals(strParam) && !strValue.trim().equals("")) {
        return true;
      }
    }
    return blExist;
  }

  /**
   * getURLDelimiter: This method is used to determine which delimiter either ? or &.
   * 
   * @param String URL string
   * @return String delimiter character
   * @exception AppError
   */
  public static String getURLDelimiter(String strURL) throws AppError {
    if (strURL == null)
      return "";
    return strURL.indexOf("?") != -1 ? "&" : "?";
  }

  /**
   * getRequestParams: This method used to get the currency type and currency symbol from request or
   * session or cookie.
   * 
   * @param request,response,sesssion,hmParam
   * @exception AppError
   */
  public void getRequestParams(HttpServletRequest request, HttpServletResponse response,
      HttpSession session, HashMap hmParam) throws AppError {

    GmCookieManager gmCookieManager = new GmCookieManager();

    String strCurrType = "";
    String strCurrSymbol = "";

    strCurrType = GmCommonClass.parseNull((String) (request.getAttribute("hCurrType")));
    strCurrType =
        strCurrType.equals("") ? GmCommonClass.parseNull((String) session
            .getAttribute("strSessCurrType")) : strCurrType;
    strCurrType =
        strCurrType.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(request,
            "CCurrType")) : strCurrType;

    gmCookieManager.setCookieValue(response, "CCurrType", strCurrType);
    session.setAttribute("strSessCurrType", strCurrType);

    hmParam.put("CURRTYPE", strCurrType);
    // Getting company language id from session.(For multilingual support)
    // Based on the language id, we will show the Japan Master data(Account,Dist,Rep) in English on
    // Sales Module reports.
    String strSessCompanyLangId =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLangId"));
    hmParam.put("COMP_LANG_ID", strSessCompanyLangId);
    log.debug("hmParam in Framework=" + hmParam);
  }
}
