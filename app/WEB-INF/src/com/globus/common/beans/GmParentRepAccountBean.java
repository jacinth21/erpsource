/*
 * Module: Batch Name: GmBatchBean.java Author:
 */

package com.globus.common.beans;


import java.sql.ResultSet;
import java.util.ArrayList;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmParentRepAccountBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmParentRepAccountBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * Fetch parent rep account info.
   * 
   * @param hmParam the hm param
   * @return the hash map
   * @throws AppError the app error
   */
  public ArrayList fetchParentRepAccountInfo(String strAcctID) throws AppError {
    ArrayList alRepInfo = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_sm_party_price.gm_fch_parent_rep_acct_info", 2);
    gmDBManager.setString(1, strAcctID);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alRepInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("alRepInfo -->" + alRepInfo);
    return alRepInfo;
  }


  /**
   * fetchPartyId - method to fetch the party id based on account id Author : Matt B
   * 
   * @param Acctid
   * @return String
   * @exception AppError
   */
  public String fetchPartyId(String AcctId) throws AppError {

    String strPartyID = " ";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("GET_ACCOUNT_PARTYID", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, AcctId);
    gmDBManager.execute();
    strPartyID = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();
    log.debug(" fetchPartyId.strPartyID ====>" + strPartyID);

    return strPartyID;

  } // end of fetchPartyId method

    /**
     * fetchInternalRepList --Fetches the Internal Rep List for BBA Accounts  
     * @param strInternalreptype
     * @return alInternalRepInfo--Returns the BBA sales Reps
     * @throws AppError
     * PMT-41644
     */
    public ArrayList fetchInternalRepList (String strInternalreptype) throws AppError {
      ArrayList alInternalRepInfo = new ArrayList();
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
      gmDBManager.setPrepareString("gm_pkg_sm_party_price.gm_fch_Internal_Act_Rep", 2);
      gmDBManager.setString(1, strInternalreptype);
      gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
      gmDBManager.execute();
      alInternalRepInfo = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
      gmDBManager.close();
      return alInternalRepInfo;
    }

}// end of class GmBatchBean
