package com.globus.common.beans;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;


import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.webservice.common.resources.GmResource;

public class GmUsageBean extends GmBean {
	
	  
	public GmUsageBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	// this method for save the details for user Tracking  
	public void saveUsageTracking(HashMap hmParams) throws AppError {	
		    GmDBManager gmDBManager = new GmDBManager();	 
		    gmDBManager.setPrepareString("gm_pkg_cm_usage_track.Gm_sav_usage_track", 3);
		    gmDBManager.setString(1, (String) hmParams.get("USERID"));
		    gmDBManager.setString(2, (String) hmParams.get("SCREENNM"));
		    gmDBManager.setString(3, (String) hmParams.get("TRANSID"));
		    gmDBManager.execute();		    
		    gmDBManager.commit();
		    
		  }
	 

}
