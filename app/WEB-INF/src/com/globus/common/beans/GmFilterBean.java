/**
 * FileName    : GmFilterBean.java 
 * Description :
 * Author      : rshah
 * Date        : Jun 4, 2009 
 * Copyright   : Globus Medical Inc
 */
package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;

/**
 * @author rshah
 *
 */
public class GmFilterBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.
	GmCommonBean gmCommonBean = new GmCommonBean();
	/**
     * fetchConditions - This method will be used to fetch the condition for specific group. 
     * @param strConGroup
     * @return ArrayList 
     * @exception AppError
     */
    public ArrayList fetchConditions() throws AppError
    {
        ArrayList alList = new ArrayList();
        
        GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_cm_filter.gm_fch_conditions",1);
	    gmDBManager.registerOutParameter(1,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alList = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(1));
	    gmDBManager.close();
                    
        return alList;
    }
    /**
     * fetchOperatorsAndValues - This method will be used to fetch the operators (drop down). 
     * @param strConGroup
     * @return ArrayList 
     * @exception AppError
     */
    
    public ArrayList fetchOperators(String strFilterId, String strGroup) throws AppError
    {
    	ArrayList alOperators  = new ArrayList();
    	GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_cm_filter.gm_fch_operators",3);
        gmDBManager.setString(1,strFilterId);
        gmDBManager.setString(2,strGroup);
	    gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alOperators = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
	    log.debug("alOperators..."+alOperators);
	    gmDBManager.close();
    	return alOperators;
    }
    
    /**
     * fetchOperatorsAndValues - This method will be used to fetch the operators (drop down). 
     * @param strConGroup
     * @return ArrayList 
     * @exception AppError
     */
    
    public ArrayList fetchValues(String strFilterId, String strGroup) throws AppError
    {
    	ArrayList alValues  = new ArrayList();
    	GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_cm_filter.gm_fch_values",3);
        gmDBManager.setString(1,strFilterId);
        gmDBManager.setString(2,strGroup);
	    gmDBManager.registerOutParameter(3,OracleTypes.CURSOR);
	    gmDBManager.execute();
	    alValues = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
	    log.debug("alValues..."+alValues);
	    gmDBManager.close();
    	return alValues;
    }
    
    /**
     * fetchRulesCondition - This method will be used to fetch the Rules condition. 
     * @param strRuleId
     * @return ArrayList 
     * @exception AppError
     */
    
    public ArrayList fetchRulesCondition(String strRuleId) throws AppError
    {
    	ArrayList alCondition  = new ArrayList();
    	
    	GmDBManager gmDBManager = new GmDBManager ();
        gmDBManager.setPrepareString("gm_pkg_cm_rules.gm_fch_rulecondition",2);
        gmDBManager.setInt(1,Integer.parseInt(strRuleId));
        gmDBManager.registerOutParameter(2,OracleTypes.CURSOR);
        log.debug("rule id...."+strRuleId);
	    gmDBManager.execute();
	    alCondition = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
	    log.debug("alCondition..."+alCondition);
	    gmDBManager.close();
    	return alCondition;
    }
}
