package com.globus.common.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class GmGridFormat extends GmCrossTabFormat {
	
	GmCrossTabDecorator crossTabDecorator = new GmCrossTabDecorator();
	String strOpenHead = "<head>";
	String strCloseHead = "</head>";
	String strOpenRows = "<rows>";
	String strCloseRows = "</rows>";
	HashMap colMasterMap = new HashMap();
	HashMap colMasterKeyMap = new HashMap();
	HashMap dropDownValueMap = new HashMap();
	HashMap colAlignMap = new HashMap();
	String strGeneralColumnAlign = "center";
	HashMap hmAddHeaderColumns = new HashMap();
	
	public String generateGridXML(HashMap hpValue){
		
		ArrayList alFinalResult = null;
		ArrayList alHeader = null;
		String strDivStyle = null;
		String strValue = null;
		StringBuffer sbTableValue = new StringBuffer();
		long startTime = System.currentTimeMillis();
		log.debug("Start Time"+startTime);
		try{
			alFinalResult = (ArrayList) hpValue.get("Details");
			alHeader = (ArrayList) hpValue.get("Header");
			alFinalResult = sortList(alFinalResult, alHeader);
			strDivStyle = blSetFixedHeader == true ? strDivStyle : "\'";
			gmCrossTabDecorator = getDecorator();
			
			alHeader = removeColumns(alHeader);
			alHeader = addColumns(alHeader);
			gmCrossTabDecorator.setHeaderRow(alHeader); 
			int Headersize = alHeader.size();			
			sbTableValue.append(strOpenRows);
			sbTableValue.append(strOpenHead);
			
			long headerTime = System.currentTimeMillis();
			//log.debug("Header Start Time"+headerTime);
			for (int i = 0; i < Headersize; i++) {
				strValue = (String) gmCrossTabDecorator.getHeaderColumn(i);
				sbTableValue.append(printGridHeader(strValue, i));
			}
			//log.debug(" Header End Time"+System.currentTimeMillis() +"Header  Time Taken : "+(System.currentTimeMillis()-headerTime));
			sbTableValue.append(strCloseHead);
			
			long gridTime = System.currentTimeMillis();
			//log.debug("Grid Start Time"+gridTime);
			String strGridData = printGridData(alFinalResult,alHeader);
			//log.debug(" Grid End Time"+System.currentTimeMillis() +"Grid  Time Taken : "+(System.currentTimeMillis()-gridTime));
			sbTableValue.append(strGridData);
			sbTableValue.append(strCloseRows);
			//log.debug(" # # # # sbTableValue "+sbTableValue);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		log.debug("End Time"+System.currentTimeMillis() +"  Time Taken : "+(System.currentTimeMillis()-startTime));
		return sbTableValue.toString();
	}
	private String printGridData(ArrayList alFinalResult, ArrayList alHeader){
		
		int Detailsize=alFinalResult.size();
		StringBuffer sbTableValue = new StringBuffer();
		HashMap hmapValue = new HashMap();
		int Headersize = alHeader.size();
		String strTempHeader = null;
		String Value = null;
		String strValue = null;
		
		try{
			for (int i = 0; i < Detailsize; i++) {
				hmapValue = (HashMap) alFinalResult.get(i);			
				gmCrossTabDecorator.setCurrentRow(hmapValue);
	
				setPreviousValue(0);
	
				if (blSetGrouping) {
					strGroupingCount = GmCommonClass.parseNull((String) gmCrossTabDecorator.get("GROUPINGCOUNT"));
					gmCrossTabDecorator.setGroupingCount(strGroupingCount);
				}
	
				sbTableValue.append("<row id=\""+gmCrossTabDecorator.get("ID")+"\">");
				for (int j = 0; j < Headersize; j++) {
					strTempHeader = (String) alHeader.get(j);
					//log.debug("strTempHeader: " +strTempHeader);
					if(!strTempHeader.trim().equals(""))
					{
					// The Grid Data should be escaped for any Illegal XML Characters.
						Value = GmCommonClass.parseNull((String) gmCrossTabDecorator.get(strTempHeader));
						if (!Value.equals("") &&  Value.indexOf("CDATA") == -1)
						{
							Value = GmCommonClass.replaceForXML((String) gmCrossTabDecorator.get(strTempHeader));	
						}						
					strValue = (String) alHeader.get(j);
					
					if (strValue.equals("Name")) {
						strRowUniqueID = (String) gmCrossTabDecorator.get("ID");
						
						if (blLinkRequiredFlag) {
							sbTableValue.append(printGridCell(Value, strRowUniqueID));
						} else {
							sbTableValue.append(printGridCell(Value,strValue));
						}
					} else {
						sbTableValue.append(printGridCell(Value, strValue));
					}
				}
				}
				sbTableValue.append("</row>");
			}
		}catch(Exception e){
			e.printStackTrace();			
		}
		//log.debug("XML created : "+sbTableValue.toString());
		return sbTableValue.toString();
	}
	public String printGridCell(String Value, String ColumnName) {
		StringBuffer sbTDValue = new StringBuffer();

		if (ColumnName.equals("Total") && !blTotalRequired)
		{
			return "";
		}

		sbTDValue.append("<cell width=\"" + checkColumnWidth(ColumnName)+"\"");
		sbTDValue.append(" class=\"" + gmCrossTabDecorator.getStyle(ColumnName));
		sbTDValue.append(" >");
		sbTDValue.append(Value);
		sbTDValue.append("</cell>");

		return sbTDValue.toString();
	}
	private String printGridHeader(String strColumnName, int intSortIndex) {
		StringBuffer sbGridHeader = new StringBuffer();		
		String strHeaderStyle = getHeadeStyler(strColumnName);
		String strColumnType = getColumnType(strColumnName);
		int columnWidth = checkColumnWidth(strColumnName);
		if(strColumnName.equals("Name"))
			strColumnType="coro";
		String strColumnSortType = getColumnSortType(strColumnName);
		
		String strColumnAlign = getColumnAlign(strColumnName);
		
		ArrayList colMasterList = this.getColMasterList(strColumnName);
		
		//log.debug("# # # # strColumnName :"+strColumnName +" colMasterList: "+colMasterList);
		
		sbGridHeader.append("<column align=\""+strColumnAlign+"\" sort=\""+strColumnSortType+"\" type=\""+strColumnType+"\" width=\"" + columnWidth + "\" class=\"" + strHeaderStyle + "\" > ");
		if (strColumnName.equals("Name")) {

			if (hMapRenameColumn.get("Name") != null && !((String) hMapRenameColumn.get("Name")).equals("")) {
				strColumnName = (String) hMapRenameColumn.get("Name");
			}			
			sbGridHeader.append(strColumnName);		
		}else if (strColumnName.equals("Description")) {

			if (hMapRenameColumn.get("Description") != null && !((String) hMapRenameColumn.get("Description")).equals("")) {
				strColumnName = (String) hMapRenameColumn.get("Description");
			}			
			sbGridHeader.append(strColumnName);		
		}
		else if (strColumnName.equals("Total")) {

			if (!blHideAmount && blTotalRequired) {
				sbGridHeader.append(strColumnName);				
			}
		} else {
			
			sbGridHeader.append(strColumnName);
			
		}
		if(colMasterList!=null && colMasterList.size()>0){
			
			sbGridHeader.append(this.getColumnDropDown(strColumnName,colMasterList, (String)colMasterKeyMap.get(strColumnName+"IDKEY"), 
								(String)colMasterKeyMap.get(strColumnName+"VALUEKEY"),
								(String)colMasterKeyMap.get(strColumnName+"USEKEYASVALUE")));
		}
		sbGridHeader.append("</column>");

		return sbGridHeader.toString();
	}
	
	private String getColumnDropDown(String strColumnName,ArrayList masterData, String idKey, String valueKey
			,String useKeyAsValue){
		
		HashMap tempMap = null;
		String id="";
		String value="";
		StringBuffer colOption = new StringBuffer();
		StringBuffer dropDownValue = new StringBuffer();
		
		if(masterData!=null && masterData.size()>0){
			for(Iterator itr=masterData.iterator();itr.hasNext();){
				tempMap = (HashMap)itr.next();
				id = GmCommonClass.replaceForXML((String)tempMap.get(idKey));
				if(!useKeyAsValue.equalsIgnoreCase("true")){
					value = (String)tempMap.get(valueKey);					
				}else{
					value = id;
				}
				
				dropDownValue.append(value+"#"+id+"#@#");
				
				value=GmCommonClass.replaceForXML(value);
				colOption.append("<option value=\""+id+"\">"+value+"</option>");
			}
			
		}
		this.setDropDownMasterValue(strColumnName, dropDownValue.toString());
//		log.debug(" colOption value is "+colOption);
		return colOption.toString();
	}

	
	
	public ArrayList getColMasterList(String columnName) {
		return (ArrayList)colMasterMap.get(columnName);
	}
	/**
	 * @param colMasterMap the colMasterMap to set
	 */
	public void setColMasterList(String columnName,ArrayList colMasterList) {
		this.colMasterMap.put(columnName, colMasterList);
	}
	
	public ArrayList getColMasterKey(String columnName) {
		return (ArrayList)colMasterMap.get(columnName);
	}
	/**
	 * @param colMasterMap the colMasterMap to set
	 */
	public void setColMasterKey(String columnName,String idKey,String valueKey,String useKeyAsValue) {
		this.colMasterKeyMap.put(columnName+"IDKEY", idKey);
		this.colMasterKeyMap.put(columnName+"VALUEKEY", valueKey);
		this.colMasterKeyMap.put(columnName+"USEKEYASVALUE",useKeyAsValue);
	}
	
	public void setDropDownMasterValue(String columnName,String dropDownValue) {
		this.dropDownValueMap.put(columnName, dropDownValue);
	}
	
	public String getDropDownMasterValue(String columnName) {
		return (String)dropDownValueMap.get(columnName);
	}
	
	public void addColumnAlign(String strColumnName, String strColumnAlign) {
		colAlignMap.put(strColumnName, strColumnAlign);
	}
	public String getColumnAlign(String strColName) {
		String strValue = (String) colAlignMap.get(strColName);

		if (strValue == null) {
			strValue = strGeneralColumnAlign;
		}
		return strValue;
	}
	

	public ArrayList addColumns(ArrayList alHeader) {
	
		Set keySet = hmAddHeaderColumns.keySet();
		for(Iterator itr=keySet.iterator(); itr.hasNext();){
			String strKey = (String)itr.next();
			if(hmAddHeaderColumns.get(strKey)!=null){
				try{
					int colIndex = Integer.parseInt((String)hmAddHeaderColumns.get(strKey));
					if(alHeader.size()>=colIndex){
						alHeader.add(colIndex,strKey);
					}					
				}catch(Exception e){
					// Catching the exception in-case when the value (Position) set to the hashMap is not an integer					
				}
			}
		}
		return alHeader;
	}	
	
	public void setAddColumn(String columnName, int position) {
		hmAddHeaderColumns.put(columnName,position+"");
	}
}
