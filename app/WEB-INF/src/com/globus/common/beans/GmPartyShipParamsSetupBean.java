package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmPartyShipParamsSetupBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.


  /**
   * Initialize the Logger Class. This will be removed all place changed with GmDataStoreVO
   * constructor
   */

  public GmPartyShipParamsSetupBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPartyShipParamsSetupBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  /**
   * Description : This method is used to fetch data from the Order Attribute table. It outer-joins
   * the Code Lookup table and Order ID can also be sent as null or "" to get the rows for
   * displaying on the screens for insert mode Author : James
   */
  public HashMap fetchPartyShipParam(HashMap hmParams) throws AppError {
    String strAccountID = GmCommonClass.parseNull((String) hmParams.get("ACCOUNTID"));
    String strPartyID = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    HashMap hmResult = new HashMap();

    gmDBManager.setPrepareString("gm_pkg_cm_party.gm_fch_party_params", 3);
    gmDBManager.setString(1, strAccountID);
    gmDBManager.setString(2, strPartyID);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR); // header
    gmDBManager.execute();
    hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return hmResult;
  }

  /**
   * Description : Method to save the Part Ship Params information Author : HReddi
   */
  public void savePartyShipParam(HashMap hmParams) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String strLogReason = GmCommonClass.parseNull((String) hmParams.get("TXT_LOGREASON"));
    String strAccountId = GmCommonClass.parseNull((String) hmParams.get("ACCOUNTID"));
    String strPartyId = GmCommonClass.parseNull((String) hmParams.get("PARTYID"));
    String strUserId = GmCommonClass.parseNull((String) hmParams.get("USERID"));
    gmDBManager.setPrepareString("gm_pkg_cm_party.gm_sav_party_params", 13);
    gmDBManager.setString(1, GmCommonClass.parseNull((String) hmParams.get("THIRDPARTY")));
    gmDBManager.setString(2, GmCommonClass.parseNull((String) hmParams.get("SHIPCARRIER")));
    gmDBManager.setString(3, GmCommonClass.parseNull((String) hmParams.get("SHIPMODE")));
    gmDBManager.setString(4, GmCommonClass.parseNull((String) hmParams.get("STDSHIPPAYEE")));
    gmDBManager.setString(5, GmCommonClass.parseNull((String) hmParams.get("STDSHIPCHARGETYPE")));
    gmDBManager.setString(6, GmCommonClass.parseNull((String) hmParams.get("STDSHIPVALUE")));
    gmDBManager.setString(7, GmCommonClass.parseNull((String) hmParams.get("RUSHSHIPPAYEE")));
    gmDBManager.setString(8, GmCommonClass.parseNull((String) hmParams.get("RUSHSHIPCHARGETYPE")));
    gmDBManager.setString(9, GmCommonClass.parseNull((String) hmParams.get("RUSHSHIPVALUE")));
    gmDBManager.setString(10, strAccountId);
    gmDBManager.setString(11, strPartyId);
    gmDBManager.setString(12, GmCommonClass.parseNull((String) hmParams.get("USERID")));
    gmDBManager.setString(13,GmCommonClass.parseNull((String) hmParams.get("SHIPMENTEMAIL")));
    gmDBManager.execute();
    if (!strLogReason.equals("")) {
      gmCommonBean.saveLog(gmDBManager, strPartyId, strLogReason, strUserId, "1300");
    }
    gmDBManager.commit();
  }



  /**
   * @ getAccountPartyId : Returns Party id for the Input Account ID.
   * 
   * @param String strAccId
   * @return String
   * @throws AppError
   * @Desctiption : Passing Account ID to get the Account Party ID value
   */
  public String getAccountPartyId(String strAccId) throws AppError {
    String strAccPartyId = "";
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setFunctionString("gm_pkg_sm_pricparam_setup.get_acc_partyid", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strAccId);
    gmDBManager.execute();
    strAccPartyId = GmCommonClass.parseNull((String)gmDBManager.getString(1));
    gmDBManager.close();
    return strAccPartyId;
  }
}
