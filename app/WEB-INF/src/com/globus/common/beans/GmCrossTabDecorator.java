package com.globus.common.beans;

import java.util.ArrayList;
import java.util.HashMap;

import javax.print.attribute.standard.MediaSize.NA;
import com.globus.common.beans.GmLogger;
import org.apache.log4j.Logger;

public class GmCrossTabDecorator {
	
private HashMap hmRow = new HashMap();	
private ArrayList alHeaderRow = new ArrayList();
private HashMap hmFooterRow = new HashMap();
private HashMap hMapAddStyle = new HashMap();
private String strGeneralStyle = "";
private String strGroupingCount = "";
private boolean blSetGrouping= false;
private boolean blColumnOverRideFlag = false;
private String strOverRideFl = ""; 
private int intRowHeight = 22;
private String strRowClass = "";
private String strTrId = "";
private HashMap hmDecoratorAttributes = new HashMap();
private HashMap hmAdditionalVal = new HashMap();

protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class.

public void setCurrentRow(HashMap hmRow)
 {
	this.hmRow = hmRow; 
 }

protected HashMap getCurrentRow()
{
	return hmRow;
}

public String get(String strColumnName)
{
	return (String)hmRow.get(strColumnName);
}

public void setHeaderRow(ArrayList alHeader)
{
	alHeaderRow = alHeader;
}

protected ArrayList getHeaderRow()
{
	return alHeaderRow;
}

public String getHeaderColumn(int i)
{
	return (String)alHeaderRow.get(i);
}

public void setFooterRow(HashMap hmFooter)
{
	hmFooterRow = hmFooter;
}

protected HashMap getFooterRow()
{
	return hmFooterRow;
}

public String getFooterColumn(String strColumnName)
{
	return (String)hmFooterRow.get(strColumnName);
}

/**
 * This method is used to get the style of columns
 * 
 * @param strColName -
 *            Column Name
 * @exception
 * @return String object
 */
public String getStyle(String strColName) {
	String strValue = "";

	// Check if the column has specific property
	strValue = (String) hMapAddStyle.get(strColName);

	// Check if the table has override property
	if (blColumnOverRideFlag) {
		if (strOverRideFl.equals("Y")) {
			strOverRideFl = ""; // Override applicable only for specific
			// column
			strValue = "ColumnOverride";
			return strValue;

		}
	}
	
	/*
	 * if(strColName.equals("Name")) { if (strValue == null) return
	 * "RightTextSmall"; else return strValue; }
	 */

	if (strValue == null) {
		strValue = strGeneralStyle;
		return strValue;
	}

	// Below code added for group logic
	if (blSetGrouping) {
		if (!strGroupingCount.equals("0")) {
			strValue = strGeneralStyle;
		}
	}

	return strValue;
}
public String getFooterStyle(String strColName) {
	String strValue = "";

	// Check if the column has specific property
	strValue = (String) hMapAddStyle.get(strColName);

	// Check if the table has override property
	if (blColumnOverRideFlag) {
		if (strOverRideFl.equals("Y")) {
			strOverRideFl = ""; // Override applicable only for specific
			// column
			strValue = "ColumnOverride";
			return strValue;

		}
	}
	
	/*
	 * if(strColName.equals("Name")) { if (strValue == null) return
	 * "RightTextSmall"; else return strValue; }
	 */

	if (strValue == null) {
		strValue = strGeneralStyle;
		return strValue;
	}

	// Below code added for group logic
	if (blSetGrouping) {
		if (!strGroupingCount.equals("0")) {
			strValue = strGeneralStyle;
		}
	}

	return strValue;
}

public void setGeneralStyle(String strStyle){
	strGeneralStyle = strStyle;
}
public void setStyleMap(HashMap mapAddStyle) {
	hMapAddStyle = mapAddStyle;
}

public void setGroupingCount(String strGroupingCount) {
	this.strGroupingCount = strGroupingCount;
}

protected String getGroupingCount()
{
	return strGroupingCount;
}

public void setSetGrouping(boolean blSetGrouping) {
	this.blSetGrouping = blSetGrouping;
}

public void setColumnOverRideFlag(boolean blColumnOverRideFlag)
{
	this.blColumnOverRideFlag = blColumnOverRideFlag;
}

public void setOverrideFl(String strOverRideFl)
{
	this.strOverRideFl = strOverRideFl;
}

public void setAllAttributes(HashMap hmDecoratorAttributes)
{
	this.hmDecoratorAttributes = hmDecoratorAttributes;
}

protected Object getAttribute(String key)
{
	return hmDecoratorAttributes.get(key);
}
/**
 * This method will return the open tr information
 * 
 * @param N/A
 * @exception NA
 * @return String object
 */
public String getTRopen() {
	String strValue;
	if (!strGroupingCount.equals("0")) {
		strValue = "<tr  height =" + intRowHeight + "  " + strRowClass + strGroupingCount + " >";
	} else {
		strValue = "<tr  height =" + intRowHeight + " >";
	}
	return strValue;
}

/**
 * This method will return the open tr information
 * 
 * @param N/A
 * @exception NA
 * @return String object
 */
public String getTRwithStyleOpen() {
	String strValue = "<tr style=\"position:relative; top:expression(this.offsetParent.scrollTop);\" height =" + intRowHeight + ">";
	return strValue;
}

/**
 * This method will return the close tr information
 * 
 * @param N/A
 * @exception NA
 * @return String object
 */
public String getTRclose() {
	String strValue = "</tr>";
	return strValue;
}

public void setRowHeight(int intRowHeight)
{
	this.intRowHeight = intRowHeight;
	
}

public void setRowClass(String strRowClass)
{
	this.strRowClass = strRowClass;
}

public void setTrId(String strTrId)
{
	this.strTrId = strTrId;
}
protected String getTrId()
{
	return strTrId;
}
protected int getRowHeight()
{
	return intRowHeight;
}

protected String getRowClass()
{
	return strRowClass;
}

public void setAdditionalValMap(HashMap  hmAdditionalVal) {
	this.hmAdditionalVal = hmAdditionalVal;
}

public HashMap getAdditionalValMap()
{
	return hmAdditionalVal;
}

}
