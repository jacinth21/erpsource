/**
 * This Class will be used to format cross tab report. This will process the record and gives the
 * desired output
 * 
 * @author $Author:$
 * @version $Revision:$ <br>
 *          $Date:$
 */

package com.globus.common.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.net.URLEncoder;


import javax.print.attribute.standard.MediaSize.NA;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import org.apache.log4j.Logger;

public class GmCrossTabFormat {

  public static String CROSSTAB_SALE = "SALES";
  public static String CROSSTAB_SALE_PER = "SALES_PER";

  public static final String FILTER_CONTENT_OVERRIDE_BODY =
      "com.globus.crosstab.filter.GmResponseOverrideFilter.CONTENT_OVERRIDE_BODY";
  public static final String EXPORT_FLAG = "export";

  public static final String BEAN_BUFFER = "buffer";
  public static final String EXPORT_CONTENT_TYPE = "contenttype";
  public static final String EXPORT_FILENAME = "filename";
  public static final String EXPORT_CONTENT_BODY = "body";

  private HttpServletRequest request = null;
  private String exportflag = "";

  String exportFileName;
  String formName = "";
  Logger log = GmLogger.getInstance(this.getClass().getName());
  String strReportTye = CROSSTAB_SALE;
  String strUpImage = "/images/arrow_up.gif";
  String strDownImage = "/images/arrow_down.gif";
  String strHeader = "";
  int intColumnIndex;
  int intSortOrder = GmCommonSort.ASC; // 1 - ASC, 2 - DESC
  boolean blLinkRequiredFlag;
  boolean blShowArrow;
  boolean blHideAmount;
  boolean blNoDivision = false;
  boolean blColunmDrillRequired = false;
  boolean blSetLine = true;
  boolean blDisableLine = false;
  boolean blHeaderImage = false;
  boolean blTotalRequired = true;
  boolean blColumnTotalRequired = true;
  boolean blSetFixedHeader = true;
  boolean blSortRequired = false;
  boolean blRowHighlightRequired = false;
  boolean blNoDivRequired = false;
  boolean blSetGrouping = false;
  boolean blColumnOverRideFlag = false;
  boolean blSkipCalculationForAllColumns = false;

  boolean blAllColDrillDown = false;
  boolean blSelectedColDrillDown = false;
  boolean blDisplayZeros = false;

  String strLinkType = "";
  String strGeneralStyle = "RightTextSmallCT";
  String strGeneralHeaderStyle = "ShadeRightTableCaptionBlueTD";
  String strGeneralColumnType = "ed";
  String strGeneralColumnSortType = "int";
  String strLineStyle = "borderLight";
  String strTableId = "myHighlightTable";
  String strTableRowHighlightStyle = "lightBrown";
  String strRowClass = "";
  String strRowUniqueID = "";
  String strRowOverRideValue = "";
  String strGroupingCount = "0";
  String strOverRideFl = "";
  String strColumnDrillDownScript = "fnColumnDrillDown";



  int intValueType = 1; // 0 -- Unit and 1 -- Amount
  double dblPreviousValue = 0.0;

  // Row and Column Height Specification information
  // Default Values
  int intRowHeight = 22;
  int intNameWidth = 250;
  int intAmountWidth = 60;
  int intPercentWidth = 50;

  int intColSpan = 1;
  int intDisplayColumn = 1;

  int intHierarchyCount = 1;
  String strAllColumnPrecision = null;

  int intDivHeight = 400;
  // String strDivStyle = "style=\'overflow:auto; height =" +
  // getIntDivHeight() + "\'";
  String strDivStyle = "style=\'overflow:auto;";

  HashMap hMapSkipRound = new HashMap();
  HashMap hMapSkipDivThousand = new HashMap();

  HashMap hMapAddLineBefore = new HashMap();
  HashMap hMapAddLineAfter = new HashMap();

  HashMap hMapAddStyle = new HashMap();
  HashMap hMapAddHeaderStyle = new HashMap();
  HashMap hMapColumnType = new HashMap();
  HashMap hMapColumnSortType = new HashMap();
  HashMap hMapSkipCalculation = new HashMap();
  HashMap hMapDivMandatory = new HashMap();
  HashMap hMapSortType = new HashMap();
  HashMap hMapSkipCalculationForColumn = new HashMap();
  HashMap hMapSpecificColDrill = new HashMap();
  HashMap hMapColumnWidth = new HashMap();
  HashMap hMapRenameColumn = new HashMap();
  HashMap hMapAdditionalVal = new HashMap();

  ArrayList alColumnStyle = new ArrayList();
  ArrayList alDrillDownDetails = new ArrayList();
  String defaultSortColumn = null;
  ArrayList alHideColumns = new ArrayList();
  String strVarType = "Numeric";
  String strDecorator = "com.globus.common.beans.GmCrossTabDecorator";
  GmCrossTabDecorator gmCrossTabDecorator = null;
  HashMap hmDecoratorAttributes = new HashMap();
  boolean bExport = false;
  String strExportPath = "";
  HashMap hmReportValue = new HashMap();

  /*
   * For filter functionality
   */
  boolean blDisplayFilter = false;
  int intDefaultFilterValueType = GmSearchCriteria.NUMBER_TYPE;
  HashMap hmFilterValueTypes = new HashMap();
  PageContext pageContext;

  public boolean isDisplayFilter() {
    return blDisplayFilter;
  }

  public HashMap getMapSkipRound() {
    return hMapSkipRound;
  }

  public int getDefaultFilterValueType() {
    return intDefaultFilterValueType;
  }

  public void setDefaultFilterValueType(int intDefaultFilterValueType) {
    this.intDefaultFilterValueType = intDefaultFilterValueType;
  }

  public void setDisplayFilter(boolean blDisplayFilter) {
    this.blDisplayFilter = blDisplayFilter;
  }

  public void setFilterValueType(String strColumn, int intType) {
    hmFilterValueTypes.put(strColumn, new Integer(intType));
  }

  public PageContext getPageContext() {
    return pageContext;
  }

  public void setPageContext(PageContext pageContext) {
    this.pageContext = pageContext;
  }

  public void setDivStyle(String divStyle) {
    strDivStyle = divStyle;
  }

  private String requestURI;

  public void setExport(boolean bExport, PageContext pageContext, String requestURI,
      String exportFileName) {
    this.bExport = bExport;
    this.requestURI = requestURI;
    this.pageContext = pageContext;
    this.exportFileName = exportFileName;
  }

  public void setExport(boolean bExport, PageContext pageContext, String requestURI,
      String exportFileName, String formName) {
    this.bExport = bExport;
    this.requestURI = requestURI;
    this.pageContext = pageContext;
    this.exportFileName = exportFileName;
    this.formName = formName;
  }

  public void setAttribute(Object key, Object value) {
    hmDecoratorAttributes.put(key, value);
  }

  public Object getAttribute(String key) {
    return hmDecoratorAttributes.get(key);
  }

  /**
   * If Zero Values are to be shown in reports displayZeroFlag should be set to true
   */
  public void displayZeros(boolean displayZeroFlag) {
    blDisplayZeros = displayZeroFlag;
  }

  // Contains Header Information
  public void setSortType(String strValue) {
    strVarType = strValue;
  }

  public void setDecorator(String strDecorator) {
    this.strDecorator = strDecorator;
  }

  public GmCrossTabDecorator getDecorator() throws ClassNotFoundException, InstantiationException,
      IllegalAccessException {
    Class c = Class.forName(strDecorator);
    return (GmCrossTabDecorator) c.newInstance();

  }

  // To get the Col Span for the entire row
  // Contains the column list
  private int getColSpan() {
    return intColSpan;
  }

  public void setColumnTotalRequired(boolean columnTotalRequired) {
    blColumnTotalRequired = columnTotalRequired;
  }

  // This boolean is set to enable subtotal during the display
  public void setGroupingRequired(boolean blValue) {
    blSetGrouping = blValue;
  }

  // This boolean is set to enable subtotal during the display
  public void setskipCalculationForAllColumns(boolean blValue) {
    blSkipCalculationForAllColumns = blValue;
  }

  public int getIntDivHeight() {
    return intDivHeight;
  }

  // To get the Display column count
  private int getDisplayColumn(String strColumnName) {
    if (getCalculationRequired(strColumnName)) {
      return intDisplayColumn;
    }

    // 1 indicated that it will have single row
    return 1;
  }

  // Color Specification
  // Contains Header Information
  public void setHeader(String Header) {
    strHeader = Header;
  }

  // Report type specification
  // holds Distributor, Spine Spec, Account and Grooup information
  // LoadDistributor LoadRep LoadAccount LoadGroup
  public void setLinkType(String ReportType) {
    strLinkType = ReportType;
  }

  public void setNoDivRequired(boolean noDivRequired) {
    blNoDivRequired = noDivRequired;
  }

  // Over ride user specific information
  public void setColumnOverRideFlag(boolean OverRidefl) {
    blColumnOverRideFlag = OverRidefl;
  }

  // Drill down for all column
  public void setAllColDrillDown(boolean AllColDrillDown) {
    blAllColDrillDown = AllColDrillDown;
  }

  // Drill down for specific column
  public void setSelectedColDrillDown(boolean SelectedColDrillDown) {
    blSelectedColDrillDown = SelectedColDrillDown;
  }

  // Specific Coll Drill Down info
  public void addSpecificCollDrillDown(String strColumnName) {
    hMapSpecificColDrill.put(strColumnName, "1");
  }

  // Color Specification
  // Contains Header Information
  public void setDisableLine(boolean disableLine) {
    blDisableLine = disableLine;
  }

  public void reNameColumn(String strColumnName, String strColumnValue) {
    hMapRenameColumn.put(strColumnName, strColumnValue);
  }

  // set Skip Division and Rounding Record
  public void addStyle(String strColumnName, String strStyleName, String strHeaderStyleName) {
    hMapAddStyle.put(strColumnName, strStyleName);
    hMapAddHeaderStyle.put(strColumnName, strHeaderStyleName);
  }

  // set Skip Division and Rounding Record
  public void add_RoundOff(String strColumnName, String strPrecision) {
    hMapSkipRound.put(strColumnName, strPrecision);
  }

  public void setRoundOffAllColumnsPrecision(String precision) {
    strAllColumnPrecision = precision;
  }

  public void roundOffAllColumns(ArrayList alHeader, String precision) {
    String strColumnName;
    Iterator itrAlHeader = alHeader.iterator();
    while (itrAlHeader.hasNext()) {
      strColumnName = (String) itrAlHeader.next();
      hMapSkipRound.put(strColumnName, precision);
    }
  }

  // set Skip Division and Rounding Record
  public void addSkip_Div_Round(String strColumnName) {
    hMapSkipRound.put(strColumnName, "0");
    hMapSkipDivThousand.put(strColumnName, "1");
  }

  // set Skip Division
  public void addSkip_Div(String strColumnName) {
    hMapSkipDivThousand.put(strColumnName, "1");
  }

  // set Division as mandatory for the column
  public void addDivMandatory(String strColumnName) {
    hMapDivMandatory.put(strColumnName, "1");
  }

  // set default sort column
  public void setDefaultSortColumn(String defaultSortColumn) {
    this.defaultSortColumn = defaultSortColumn;
  }

  // set sort type (String or Numeric) for the column
  public void addSortType(String strColumnName, String strType) {
    hMapSortType.put(strColumnName, strType);
  }

  // Code to Skip the Calculation (Like Growth Percent, Growth Arrow Etc)
  public void addSkip_Calculation(String strColumnName) {
    hMapSkipCalculation.put(strColumnName, "1");
  }

  // set Skip Division and Rounding Record
  public void addLine(String strColumnName) {
    hMapAddLineBefore.put(strColumnName, "1");
    hMapAddLineAfter.put(strColumnName, "1");
  }

  // Contains Link Information
  public void setRowHeight(int RowHeight) {
    intRowHeight = RowHeight;
  }

  // To Override the general style
  public void setGeneralStyle(String strStyle) {
    strGeneralStyle = strStyle;
  }

  public void setColumnLineStyle(String strStyle) {
    strLineStyle = strStyle;
  }

  // To Override the general Headerstyle
  public void setColumnDrillDownScript(String strJSName) {
    strColumnDrillDownScript = strJSName;
  }

  // Contains Link Information
  public void setNameWidth(int NameWidth) {
    intNameWidth = NameWidth;
  }

  // Code to disable Each column divider
  public void setColumnDivider(boolean Line) {
    blSetLine = Line;
  }

  // Contains Link Information
  public void setAmountWidth(int AmountWidth) {
    intAmountWidth = AmountWidth;

  }

  // Specifies if the Rowhighlight is required. default is true
  public void setRowHighlightRequired(boolean blValue) {
    blRowHighlightRequired = blValue;

  }

  // Sets the style for rowhighlight. default is lightBrown
  public void setTableRowHighlightStyle(String strValue) {
    strTableRowHighlightStyle = strValue;

  }

  // Sets the Table Id. Default value is myHighlightTable
  public void setTableId(String strValue) {
    strTableId = strValue;

  }

  public void setHideAmount(boolean HideAmount) {
    blHideAmount = HideAmount;
  }

  // Contains Link Information
  public void setShowArrow(boolean ShowArrow) {
    blShowArrow = ShowArrow;

  }

  // Contains Link Information
  public void setLinkRequired(boolean blRequired) {
    blLinkRequiredFlag = blRequired;

  }

  // Contains Report Format
  public void setReportType(String strReportType) {
    strReportTye = strReportType;
  }

  // Contains Value Column Format
  // 0 -- Unit and 1 -- Amount
  // Value will default to 1
  public void setValueType(int intValue) {
    intValueType = intValue;
  }

  public void setNoDivision(boolean blValue) {
    blNoDivision = blValue;
  }

  // To set the Previous sales amount
  public void setPreviousValue(double dblPrvValue) {
    dblPreviousValue = dblPrvValue;
  }

  // To set the Previous sales amount
  public void setIsColunm_Drill_Required(boolean blValue) {
    blColunmDrillRequired = blValue;
  }

  // Flag to hold header information
  public void setHeaderImage(boolean blValue) {
    blHeaderImage = blValue;
  }

  // Flag to specify if the Total column is required or not
  public void setTotalRequired(boolean blValue) {
    blTotalRequired = blValue;
  }

  // To hide the columns
  public void setHideColumn(String columnName) {
    alHideColumns.add(columnName);
  }

  // To get the Previous sales amount
  public String getReportType() {
    return strReportTye;
  }

  // To get the Previous sales amount
  public double getPreviousValue() {
    return dblPreviousValue;
  }

  // Contains DIV Style
  public void setFixedHeader(boolean blValue) {
    blSetFixedHeader = blValue;
  }

  // Contains DIV info
  public void setDivHeight(int DivHeight) {
    intDivHeight = DivHeight;
  }

  // This is usually for a string column. skips any calculation
  public void skipColumnCalculation(String strColumnName) {
    hMapSkipCalculationForColumn.put(strColumnName, "1");
  }

  // set the column width
  public void setColumnWidth(String strColumnName, int intColumnWidth) {
    hMapColumnWidth.put(strColumnName, String.valueOf(intColumnWidth));
  }

  public ArrayList removeColumns(ArrayList alHeader) {
    for (int count = 0; count < alHideColumns.size(); count++) {
      alHeader.remove(alHideColumns.get(count));
    }
    return alHeader;
  }

  // To Override the default JavaScript fn Name
  public void setGeneralHeaderStyle(String strStyle) {
    strGeneralHeaderStyle = strStyle;
  }

  public static ArrayList getFilterList(String strFilters) {
    ArrayList alFilters = new ArrayList();
    Logger log = GmLogger.getInstance(GmCrossTabFormat.class.getName());

    if (!strFilters.equals("")) {
      String[] filters = strFilters.split(";");
      log.debug("Total filters : " + filters.length);
      for (int i = 0; i < filters.length; i++) {
        String filterStr = filters[i];
        String[] filter = filterStr.split(",");
        if (filter.length == 3 && !filter[0].equals("[Choose One]") && !filter[0].equals("0")
            && !filter[1].equals("0") && !filter[2].equals("")) {
          alFilters.add(filter);
          // log.debug("Adding filter : " + filter[0] + filter[1] + filter[2]);
        }
      }
    }
    return alFilters;
  }

  private String getFilterJavaScript(HashMap hpValue) {
    StringBuffer sbFilterJS = new StringBuffer();
    GmCommonBean gmCommonBean = new GmCommonBean();
    ArrayList alHeaderColumns = GmCommonClass.parseNullArrayList((ArrayList) hpValue.get("Header"));
    ArrayList alOperatorList = GmCommonClass.parseNullArrayList(gmCommonBean.getOperatorList());

    sbFilterJS
        .append("<script language=\"JavaScript\" src=\"javascript/GmCrossTabFilter.js\"></script>\n");
    sbFilterJS.append("<script>\n");

    int intSize = alHeaderColumns.size();
    log.debug("alHeaderColumns.size() = " + alHeaderColumns.size());
    for (int i = 0; i < intSize; i++) {
      String column = (String) alHeaderColumns.get(i);
      sbFilterJS.append("columns[" + i + "]='" + column + "," + column + "';\n");
    }

    intSize = alOperatorList.size();
    HashMap hmVals = new HashMap();
    for (int k = 0; k < intSize; k++) {
      hmVals = (HashMap) alOperatorList.get(k);
      sbFilterJS.append("operators[" + k + "]='" + hmVals.get("CODENMALT") + ","
          + hmVals.get("CODENM") + "';\n");
    }
    sbFilterJS.append("</script>\n");

    return sbFilterJS.toString();
  }

  private String getFilterTable(HashMap hpValue, PageContext pageContext) {
    StringBuffer sbFilterTable = new StringBuffer();
    GmCommonBean gmCommonBean = new GmCommonBean();

    String hlfilter = "";
    String hfilter = "";

    if (pageContext != null) {
      HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
      hlfilter = GmCommonClass.parseNull(request.getParameter("hlfilter"));
      hfilter = GmCommonClass.parseNull(request.getParameter("hfilter"));
    } else {
      log.error("PageContext not set for cross tab");
    }

    ArrayList alHeaderColumns = GmCommonClass.parseNullArrayList((ArrayList) hpValue.get("Header"));
    ArrayList alOperatorList = GmCommonClass.parseNullArrayList(gmCommonBean.getOperatorList());

    sbFilterTable.append("<input type=\"hidden\" name=\"index\" value=\"1\"/>");
    sbFilterTable.append("<input type=\"hidden\" name=\"hfilter\" value=\"\"/>");
    sbFilterTable.append("<input type=\"hidden\" name=\"hlfilter\" value=\"\"/>");

    sbFilterTable.append("<table cellSpacing=0 cellPadding=0>");
    sbFilterTable.append("<tr><td class='RightTableCaption'>&nbsp;&nbsp;More Filters:</td></tr>");
    sbFilterTable.append("<tr><td align=center>");
    sbFilterTable
        .append("<table id='filter' cellSpacing=0 cellPadding=0 class='CTFilterHeader'><thead>");
    sbFilterTable.append("<tr>");
    sbFilterTable
        .append("<th id='showhide' align='center' width='4%'><a href=javascript:fnToggleFilter();><img border='0' valign='center' src='/images/minus.gif' height='10' width='9'/></a></th>");
    sbFilterTable
        .append("<th align='center' width='4%'><img border='0' valign='center' src='/images/btn_remove.gif' height='10' width='9'/></th>");
    sbFilterTable.append("<th width='40%'>Column</th>");

    sbFilterTable.append("<th width='30%'>Operator</th>\n");
    sbFilterTable.append("<th width='22%'>Value</th>\n");
    sbFilterTable.append("</tr></thead>\n");
    sbFilterTable.append("<tbody>\n");
    sbFilterTable.append("<tr><td colspan='5' class='LLine' height='1'></td></tr>\n");

    ArrayList alFilters =
        GmCrossTabFormat.getFilterList((hlfilter.equals("") ? hfilter : hlfilter));
    if (alFilters.size() == 0) {
      log.debug("Creating one row of blank filter");

      sbFilterTable.append("<tr id='tr1'>\n");
      sbFilterTable.append("<td>1</td>\n");
      sbFilterTable
          .append("<td><a href=javascript:fnRemoveRow(1);><img border='0' valign='center' src='/images/btn_remove.gif' height='10' width='9'/></a></td>\n");
      sbFilterTable.append("<td>\n");
      sbFilterTable.append("<select name='cboCol1'>\n");
      sbFilterTable.append("<option value='0'>[Choose One]</option>\n");
      for (Iterator iter = alHeaderColumns.iterator(); iter.hasNext();) {
        String columnName = (String) iter.next();
        sbFilterTable.append("<option value='" + columnName + "'>" + columnName + "</option>\n");
      }
      sbFilterTable.append("</select>\n");
      sbFilterTable.append("</td>\n");
      sbFilterTable.append("<td>\n");
      sbFilterTable.append("<select name='cboOpt1'>\n");
      sbFilterTable.append("<option value='0'>[Choose One]</option>\n");

      for (Iterator iter = alOperatorList.iterator(); iter.hasNext();) {
        HashMap hmOpt = (HashMap) iter.next();
        String codeNm = (String) hmOpt.get("CODENM");
        String codeNmAlt = (String) hmOpt.get("CODENMALT");
        sbFilterTable.append("<option value='" + codeNmAlt + "'>" + codeNm + "</option>\n");
      }
      sbFilterTable.append("</select>\n");
      sbFilterTable.append("</td>\n");
      sbFilterTable.append("<td>\n");
      sbFilterTable.append("<input type='text' name='txt1' size='10' class='InputArea'/>\n");
      sbFilterTable.append("</td>\n");
      sbFilterTable.append("</tr>\n");
    } else {
      log.debug("Creating user selected filter");
      int i = 1;
      for (Iterator iter = alFilters.iterator(); iter.hasNext();) {
        String[] filter = (String[]) iter.next();

        sbFilterTable.append("<tr id='tr" + i + "'>\n");
        sbFilterTable.append("<td>" + i + "</td>\n");
        sbFilterTable
            .append("<td><a href=javascript:fnRemoveRow("
                + i
                + ");><img border='0' valign='center' src='/images/btn_remove.gif' height='10' width='9'/></a></td>\n");
        sbFilterTable.append("<td>\n");
        sbFilterTable.append("<select name='cboCol" + i + "'>\n");
        sbFilterTable.append("<option value='0'>[Choose One]</option>\n");

        for (Iterator headerIter = alHeaderColumns.iterator(); headerIter.hasNext();) {
          String columnName = (String) headerIter.next();
          String strSelected = "";
          if (columnName.equals(filter[0])) {
            strSelected = "selected";
          }
          sbFilterTable.append("<option value='" + columnName + "' " + strSelected + ">"
              + columnName + "</option>\n");
        }
        sbFilterTable.append("</select>\n");

        sbFilterTable.append("</td>\n");
        sbFilterTable.append("<td>\n");
        sbFilterTable.append("<select name='cboOpt" + i + "'>\n");
        sbFilterTable.append("<option value='0'>[Choose One]</option>\n");

        for (Iterator optIter = alOperatorList.iterator(); optIter.hasNext();) {
          HashMap hmOpt = (HashMap) optIter.next();
          String codeNm = (String) hmOpt.get("CODENM");
          String codeNmAlt = (String) hmOpt.get("CODENMALT");
          String strSelected = "";
          if (codeNmAlt.equals(filter[1])) {
            strSelected = "selected";
          }
          sbFilterTable.append("<option value='" + codeNmAlt + "' " + strSelected + ">" + codeNm
              + "</option>\n");
        }
        sbFilterTable.append("</select>\n");

        sbFilterTable.append("</td>\n");
        sbFilterTable.append("<td>\n");
        sbFilterTable.append("<input type='text' name='txt" + i
            + "' size='10' class='InputArea' value='" + filter[2] + "'/>\n");
        sbFilterTable.append("</td>\n");
        sbFilterTable.append("</tr>\n");
        sbFilterTable.append("<tr><td colspan='5' class='LLine' height='1'></td></tr>");
        i++;
      }

      sbFilterTable.append("<script>document.all.index.value=" + i + ";</script>\n");

    }
    sbFilterTable.append("</tbody>\n");
    sbFilterTable.append("</table>\n");
    sbFilterTable.append("<table>\n");
    sbFilterTable.append("<tr><td align='center'>\n");
    sbFilterTable
        .append("<input type='button' value='Add Row' class='Button' onclick='fnAddRow(document.all.index.value);'/>\n");
    sbFilterTable.append("</td></tr>\n");
    sbFilterTable.append("</table>\n");
    sbFilterTable.append("\n"); // End of inner filter div

    sbFilterTable.append("</td></tr>");
    sbFilterTable.append("<tr><td height='10'></td></tr>");
    sbFilterTable.append("<tr><td class='Line'></td><tr>");
    sbFilterTable.append("</table>");

    return sbFilterTable.toString();
  }

  public String getFilter(HashMap hpValue, PageContext pageContext) throws Exception {

    StringBuffer sbCrossTab = new StringBuffer();

    ArrayList alDetails = (ArrayList) hpValue.get("Details");
    if (isDisplayFilter() && alDetails != null) {

      GmSearchCriteria gmSearchCriteria = new GmSearchCriteria();
      HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
      String hlfilter = GmCommonClass.parseNull(request.getParameter("hlfilter"));
      String hfilter = GmCommonClass.parseNull(request.getParameter("hfilter"));
      String strFilters = hlfilter.equals("") ? hfilter : hlfilter;

      log.debug("strFilters = " + strFilters);

      ArrayList alFilters = getFilterList(strFilters);

      for (Iterator iter = alFilters.iterator(); iter.hasNext();) {
        String[] filter = (String[]) iter.next();
        Integer intType = (Integer) hmFilterValueTypes.get(filter[0]);
        if (intType != null) {
          // log.debug("Putting " + filter[0] + filter[1] + filter[2] + " with type " +
          // intType.intValue());
          gmSearchCriteria.addSearchCriteria(filter[0], filter[2], filter[1], intType.intValue());
        } else {
          // log.debug("Putting " + filter[0] + filter[1] + filter[2] + " with default type " +
          // intDefaultFilterValueType);
          gmSearchCriteria.addSearchCriteria(filter[0], filter[2], filter[1],
              intDefaultFilterValueType);
        }
      }
      gmSearchCriteria.query(alDetails);
      hpValue.put("Details", alDetails);

      sbCrossTab.append(getFilterJavaScript(hpValue));

      if (alDetails != null && alDetails.size() > 0) {
        sbCrossTab.append(getFilterTable(hpValue, pageContext));
      } else {
        log.debug("alDetails is null or has 0 values, filter cannot be created.");
      }
    }
    // sbCrossTab.append(PrintCrossTabReport(hpValue));
    return sbCrossTab.toString();
  }

  /**
   * This method initializes the request, exportflag and decorator parameters used while exporting
   * the report.
   * 
   * @exception ClassNotFoundException, InstantiationException, IllegalAccessException
   */
  private void initializeExport() throws ClassNotFoundException, InstantiationException,
      IllegalAccessException {
    if (bExport) {
      request = (HttpServletRequest) pageContext.getRequest();
      exportflag = request.getParameter(EXPORT_FLAG);

      if ("true".equals(exportflag)) {
        setDisableLine(true);
        hMapAddLineAfter = new HashMap();
        blLinkRequiredFlag = false;
        setDecorator("com.globus.crosstab.beans.GmCrossTabExportDecorator");
        gmCrossTabDecorator = getDecorator();
      }
    }
  }

  /**
   * This method is used to print the Crosstab SQL
   * 
   * @param Haspmap - Contains report header, footer and detaila
   * @exception
   * @return String object
   */
  public String PrintCrossTabReport(HashMap hpValue) throws Exception {

    ArrayList alHeader;
    ArrayList alFinalResult;
    HashMap hmapValue = new HashMap();

    String strValue = "";
    String Value = "";
    String strTempHeader = "";

    StringBuffer sbTableValue = new StringBuffer();

    gmCrossTabDecorator = getDecorator();
    gmCrossTabDecorator.setSetGrouping(blSetGrouping);
    gmCrossTabDecorator.setGeneralStyle(strGeneralStyle);
    gmCrossTabDecorator.setStyleMap(hMapAddStyle);
    gmCrossTabDecorator.setColumnOverRideFlag(blColumnOverRideFlag);
    gmCrossTabDecorator.setRowHeight(intRowHeight);
    gmCrossTabDecorator.setTrId(strTableId);
    gmCrossTabDecorator.setAllAttributes(hmDecoratorAttributes);
    gmCrossTabDecorator.setAdditionalValMap(hMapAdditionalVal);

    // **********************************
    // Initialize the parameters for export

    initializeExport();

    // **********************************
    if (isDisplayFilter() && !"true".equals(exportflag)) {
      sbTableValue.append(getFilter(hpValue, pageContext));
    }

    this.hmReportValue = hpValue;
    // ** Getting the Array List Value **//
    alFinalResult = (ArrayList) hpValue.get("Details");

    if (alFinalResult == null) {
      return "";
    }

    int Detailsize = alFinalResult.size();

    // If not header is found display No Data found information
    if (Detailsize == 0) {
      return getNoData();
    }

    // Below position wil calculate the list of column to be displayed
    // Sets to Zero if not required to display the column
    if (blHideAmount) {
      intDisplayColumn = 0;
    }

    if (blShowArrow) {
      intDisplayColumn = intDisplayColumn + 2;
    }

    if (strReportTye == CROSSTAB_SALE_PER) {
      intDisplayColumn = intDisplayColumn + 1;
    }

    alHeader = (ArrayList) hpValue.get("Header");

    alFinalResult = sortList(alFinalResult, alHeader);
    strDivStyle = blSetFixedHeader == true ? strDivStyle : "\'";
    sbTableValue.append("<div id=tablediv " + strDivStyle + "height= " + getIntDivHeight()
        + " \' >");

    // *********************************
    // To Hide Column
    // ********************************
    alHeader = removeColumns(alHeader);
    gmCrossTabDecorator.setHeaderRow(alHeader);

    // *************************************************************
    // Display the common filter if user has enabled the filter flag
    // *************************************************************
    sbTableValue.append("<TABLE cellSpacing=0 cellPadding=0  border=0 width=\"100%\" id = '");
    sbTableValue.append(strTableId);
    sbTableValue.append("'> ");
    sbTableValue.append("<THEAD>");

    int Headersize = alHeader.size();
    // Holds the col span information
    intColSpan = (Headersize * intDisplayColumn) + 3;
    intColSpan = intColSpan + hMapAddLineAfter.size(); // To include before
    // and after size
    intColSpan = intColSpan + hMapAddLineBefore.size();

    intColSpan = intColSpan - (intDisplayColumn * hMapSkipCalculation.size());

    if (!blTotalRequired) {
      intColSpan--;
    }

    if (blSetFixedHeader) {
      sbTableValue.append(gmCrossTabDecorator.getTRwithStyleOpen());
    } else {
      sbTableValue.append(gmCrossTabDecorator.getTRopen());
    }

    for (int i = 0; i < Headersize; i++) {
      strValue = gmCrossTabDecorator.getHeaderColumn(i);
      sbTableValue.append(printTableHeader(strValue, i));

      // To print the line if requested by the screen
      sbTableValue.append(getAfterColLine(strValue));

    }
    if (strAllColumnPrecision != null) {
      roundOffAllColumns(alHeader, strAllColumnPrecision);
    }
    sbTableValue.append(gmCrossTabDecorator.getTRclose());
    sbTableValue.append("</THEAD>");
    sbTableValue.append("<TBODY>");

    sbTableValue.append(getLine());
    // **********************************
    // To get the Report details
    // **********************************
    if (blSetGrouping) {
      gmCrossTabDecorator.setRowClass("class=ShadeLevel");
    }

    for (int i = 0; i < Detailsize; i++) {
      hmapValue = (HashMap) alFinalResult.get(i);
      gmCrossTabDecorator.setCurrentRow(hmapValue);

      setPreviousValue(0);

      if (blSetGrouping) {
        strGroupingCount = GmCommonClass.parseNull(gmCrossTabDecorator.get("GROUPINGCOUNT"));
        gmCrossTabDecorator.setGroupingCount(strGroupingCount);
      }

      // log.debug(" hmapValue is " +hmapValue );
      sbTableValue.append(gmCrossTabDecorator.getTRopen());
      for (int j = 0; j < Headersize; j++) {
        strTempHeader = (String) alHeader.get(j);
        Value = gmCrossTabDecorator.get(strTempHeader);
        strValue = (String) alHeader.get(j);

        // To print the name else print the total
        if (strValue.equals("Name")) {
          strRowUniqueID = gmCrossTabDecorator.get("ID");

          if (blLinkRequiredFlag) {
            sbTableValue.append(printTableName(Value, strRowUniqueID));
          } else {
            sbTableValue.append(printTableName(Value, null));
          }
        } else {
          if (blColumnOverRideFlag) {
            strOverRideFl = GmCommonClass.parseNull(gmCrossTabDecorator.get(strTempHeader + "CRD"));
            gmCrossTabDecorator.setOverrideFl(strOverRideFl);
          }
          sbTableValue.append(printTableAmount(Value, strValue));
        }

        // To print the line if requested by the screen
        sbTableValue.append(getAfterColLine(strValue));

      }
      sbTableValue.append(gmCrossTabDecorator.getTRclose());
      sbTableValue.append(getLineLight());
    }

    // **********************************
    // To get the Footer details
    // **********************************
    if (blColumnTotalRequired) {
      hmapValue = (HashMap) hpValue.get("Footer");
      gmCrossTabDecorator.setFooterRow(hmapValue);

      sbTableValue.append(getLine());
      setPreviousValue(0);
      sbTableValue.append(gmCrossTabDecorator.getTRopen());
      // log.debug(alHeader);
      for (int j = 0; j < Headersize; j++) {
        strValue = (String) alHeader.get(j);
        Value = gmCrossTabDecorator.getFooterColumn(strValue);

        if (strValue.equals("Name")) {
          sbTableValue.append("<td class=" + gmCrossTabDecorator.getFooterStyle(strValue)
              + " style='FONT-WEIGHT: bold'>");
          sbTableValue.append(Value);
          sbTableValue.append("</td>");
        } else {
          sbTableValue.append(printTableFooterAmount(Value, strValue));
        }

        // To print the line if requested by the screen
        sbTableValue.append(getAfterColLine(strValue));

      }

      sbTableValue.append(gmCrossTabDecorator.getTRclose());
    }

    sbTableValue.append(getLine());
    sbTableValue.append("</tbody>");
    sbTableValue.append("</table>");
    sbTableValue.append("</div>");
    sbTableValue.append(getRowHighlight());

    // **********************************
    // Writes the export hyperlink
    // log.debug(" Total table value = " +sbTableValue);
    writeExport(sbTableValue);

    // **********************************
    return sbTableValue.toString();
  }

  /**
   * This method writes the export hyperlink on crosstab and sets the response parameters in the
   * bean present in the request object.
   * 
   * @param sbTableValue - the table body displayed on JSP
   */
  private void writeExport(StringBuffer sbTableValue) {
    if (bExport) {
      if ("true".equals(exportflag)) // This will get executed when 'Excel' hyperlink is clicked
      {
        Map bean = (Map) request.getAttribute(FILTER_CONTENT_OVERRIDE_BODY);

        if (bean != null) {
          bean.put(EXPORT_CONTENT_TYPE, "application/vnd.ms-excel");
          if (exportFileName == null) {
            exportFileName = "ExcelReport.xls";
          } else {
            exportFileName =
                exportFileName.indexOf(".") != -1 ? exportFileName : exportFileName + ".xls";
          }
          bean.put(EXPORT_FILENAME, exportFileName);
          bean.put(EXPORT_CONTENT_BODY, sbTableValue.toString());
        }
        return; // We dont need to render the download hyperlink again
      }

      Set keySet = request.getParameterMap().keySet();
      Iterator iter = keySet.iterator();
      StringBuffer sbParams = new StringBuffer();
      // If we already have a question mark like for dispatchaction, then dont add one.
      String QUESTION_MARK = requestURI.matches(".*[\\?].*") == true ? "&" : "?";
      while (iter.hasNext()) {
        String param = (String) iter.next();
        String[] value = (String[]) request.getParameterMap().get(param);
        for (int i = 0; i < value.length; i++) {
          // log.debug("parameter = " + param + " value = " + value[i]);
         // sbParams.append(param + "=" + value[i] + "&");
	    sbParams.append(param + "=" + URLEncoder.encode(value[i])+ "&");
        }
      }

      sbParams = new StringBuffer(sbParams.toString().replaceAll("'", "\'"));
      sbParams.append(EXPORT_FLAG + "=true");
      sbTableValue
          .append("<style>div.exportlinks {background-color: #eee;border: 1px dotted #999;padding: 2px 4px 2px 4px;margin: 2px 0 10px 0;width: 40%;} span.excel {background-image: url(../img/ico_file_excel.png);} span.export {	padding: 0 4px 1px 20px;display: inline;display: inline-block;cursor: pointer;}</style>");
      if (formName.equals("") || formName == null) {
        sbTableValue
            .append("<div class='exportlinks'>Export Options : <img src='img/ico_file_excel.png'/>&nbsp;<a href='");
        sbTableValue.append(requestURI);
        sbTableValue.append(QUESTION_MARK);
        sbTableValue.append(sbParams.toString());
        sbTableValue.append("'>Excel</a></div>");
      } else {
        sbTableValue
            .append("<div class='exportlinks'>Export Options : <img src='img/ico_file_excel.png'/>&nbsp;<a href='#'");
        sbTableValue.append(" onclick=fnExcelExport('");
        sbTableValue.append(formName);
        sbTableValue.append("','");
        sbTableValue.append(requestURI);
        sbTableValue.append(QUESTION_MARK);
        sbTableValue.append(sbParams.toString());
        sbTableValue.append("');>Excel</a></div>");
        sbTableValue.append("<script>");
        sbTableValue.append(" function fnExcelExport(vForm,vAction){");
        sbTableValue.append(" var frmObj = eval(\"document.\"+vForm); ");
        sbTableValue.append(" frmObj.action = vAction; ");
        sbTableValue.append(" frmObj.submit(); ");
        sbTableValue.append(" } ");
        sbTableValue.append("</script>");
      }
    }
  }

  /**
   * This method is used to print the Crosstab SQL
   * 
   * @param Haspmap - Contains report header, footer and detaila
   * @exception
   * @return String object
   */
  public String PrintMultiLevelCrossTabReport(HashMap hpValue) throws Exception {
    gmCrossTabDecorator = getDecorator();

    ArrayList alHeader;
    ArrayList alFinalResult;
    ArrayList alAddDetails;
    ArrayList alValidRow;

    HashMap hmapValue = new HashMap();
    HashMap hmapRowValue = new HashMap();

    String strValue = "";
    String Value = "";
    StringBuffer sbTableValue = new StringBuffer();

    String strKeyValue = "";
    String strIDValue = "";

    // ** Getting the Array List Value **//
    alFinalResult = (ArrayList) hpValue.get("Details");
    if (alFinalResult == null) {
      return "";
    }

    int Detailsize = alFinalResult.size();

    // If not header is found display No Data found information
    if (Detailsize == 0) {
      return getNoData();
    }

    // Below position wil calculate the list of column to be displayed
    // Sets to Zero if not required to display the column
    if (blHideAmount) {
      intDisplayColumn = 0;
    }

    if (blShowArrow) {
      intDisplayColumn = intDisplayColumn + 2;
    }

    if (strReportTye == CROSSTAB_SALE_PER) {
      intDisplayColumn = intDisplayColumn + 1;
    }

    // **********************************
    // To get the Header Information
    // ***********************************
    sbTableValue.append("<TABLE cellSpacing=0 cellPadding=0  border=0 width=100% id = '");
    sbTableValue.append(strTableId);
    sbTableValue.append("'> ");
    sbTableValue.append("<TBODY>");

    alHeader = (ArrayList) hpValue.get("Header");
    alValidRow = (ArrayList) hpValue.get("RowDetails");

    int Headersize = alHeader.size();
    int ValidRow = alValidRow.size();
    int DetailCountPos = (ValidRow * 2) + 1; // Holds details row staring
    // point
    setHierarchyCount(ValidRow); // set the hierarchy count for future
    // reference
    boolean blFlag = false;

    // Holds the col span information
    intColSpan = Headersize - (ValidRow * 2); // To reduce unwanted column
    // information (like ad,
    // territory etc not
    // displayed in the screen)

    intColSpan = (intColSpan * intDisplayColumn);
    intColSpan = intColSpan + hMapAddLineAfter.size(); // To include before
    // and after size
    // intColSpan = intColSpan + hMapAddLineBefore.size();

    intColSpan = intColSpan - (intDisplayColumn * hMapSkipCalculation.size());
    // add value based on the drilldown
    // If drilldown added add one row for the drilldown
    if (alDrillDownDetails.size() > 0) {
      intColSpan = intColSpan + 1;
    }

    sbTableValue.append(getLine());
    sbTableValue.append(gmCrossTabDecorator.getTRopen());

    // Defaul to print the name
    sbTableValue.append(printTableHeader("Name", 0));
    sbTableValue.append(getAfterColLine("Name"));
    sbTableValue.append(printTableHeader("Drill", 0));
    // Below code to print header information
    for (int i = DetailCountPos; i < Headersize; i++) {
      strValue = (String) alHeader.get(i);
      sbTableValue.append(printTableHeader(strValue, 0));
      sbTableValue.append(getAfterColLine(strValue));
    }

    sbTableValue.append(gmCrossTabDecorator.getTRclose());

    sbTableValue.append(getLine());
    int intHierarchyPosition = 0;
    int intPrevHierarchyPosition = getHierarchyCount();
    // **********************************
    // To get the Report details
    // **********************************
    for (int i = 0; i < Detailsize; i++) {
      hmapValue = (HashMap) alFinalResult.get(i);
      intHierarchyPosition = Integer.parseInt((String) hmapValue.get("Hierarchy"));

      hmapRowValue = (HashMap) alValidRow.get(intHierarchyPosition);
      // below code to get the key to get the level of info
      strKeyValue = (String) hmapRowValue.get("KEY");
      strIDValue = (String) hmapRowValue.get("ID");

      // below code to get the value
      strKeyValue = (String) hmapValue.get(strKeyValue);
      strIDValue = (String) hmapValue.get(strIDValue);

      setPreviousValue(0);
      sbTableValue.append(printTreeCloseTable(strIDValue, intHierarchyPosition,
          intPrevHierarchyPosition));
      sbTableValue.append(getTRopen(strIDValue, intHierarchyPosition));

      sbTableValue.append(printTableName(strKeyValue, strIDValue, intHierarchyPosition));
      sbTableValue.append(getAfterColLine("Name"));
      sbTableValue.append(printDrillLink(strKeyValue, strIDValue, intHierarchyPosition));
      // sbTableValue.append(printTableHeader("Drill"));

      for (int j = DetailCountPos; j < Headersize; j++) {
        Value = (String) hmapValue.get(alHeader.get(j));
        strValue = (String) alHeader.get(j);

        // To print the name else print the total
        sbTableValue.append(printTableAmount(Value, strValue));

        // To print the line if requested by the screen
        sbTableValue.append(getAfterColLine(strValue));
        sbTableValue.append("\n");

      }
      sbTableValue.append(gmCrossTabDecorator.getTRclose());
      sbTableValue.append(getLineLight());
      sbTableValue.append("\n");
      sbTableValue.append(printTreeOpenTable(strIDValue, intHierarchyPosition,
          intPrevHierarchyPosition));
      intPrevHierarchyPosition = intHierarchyPosition;
    }
    sbTableValue.append(printTreeCloseTable(strIDValue, getHierarchyCount(), 1));

    // **********************************
    // To get the Footer details
    // **********************************
    hmapValue = (HashMap) hpValue.get("Footer");

    sbTableValue.append(getLine());
    setPreviousValue(0);
    sbTableValue.append(gmCrossTabDecorator.getTRopen());

    Value = (String) hmapValue.get("Name");
    sbTableValue.append("<td class=" + gmCrossTabDecorator.getStyle("Name")
        + " style='FONT-WEIGHT: bold'>");
    sbTableValue.append(Value);
    sbTableValue.append("</td>");
    sbTableValue.append(getAfterColLine("Name"));
    sbTableValue.append(printDrillLink("Name", "0", 0));

    for (int j = DetailCountPos; j < Headersize; j++) {
      Value = (String) hmapValue.get(alHeader.get(j));
      strValue = (String) alHeader.get(j);

      sbTableValue.append(printTableFooterAmount(Value, strValue));

      // To print the line if requested by the screen
      sbTableValue.append(getAfterColLine(strValue));

    }

    sbTableValue.append(getLine());
    sbTableValue.append("</tbody>");
    sbTableValue.append("</table>");

    return sbTableValue.toString();
  }

  /**
   * This method is used to print the single line
   * 
   * @param int - RowCount used to span the row
   * @exception
   * @return String object
   */
  private String getLine() {
    String strValue = "";

    if (blDisableLine == false) {
      strValue = "<TR bgcolor=#666666><TH noWrap colSpan=" + getColSpan() + " height=1></TH></TR>";
    }

    return strValue;
  }

  /**
   * This method is used to set the row highlight option. This would be set by default
   * 
   * @param int - RowCount used to span the row
   * @exception
   * @return String object
   */

  private String getRowHighlight() {
    StringBuffer sbRowHighlightScript = new StringBuffer();
    sbRowHighlightScript.setLength(0);

    if (blRowHighlightRequired) {
      sbRowHighlightScript.append(" <script> fnstripedbyid('");
      sbRowHighlightScript.append(strTableId);
      sbRowHighlightScript.append("','");
      sbRowHighlightScript.append(strTableRowHighlightStyle);
      sbRowHighlightScript.append("') </script> ");
    }

    return sbRowHighlightScript.toString();

  }

  /**
   * This method is used to print the single column line
   * 
   * @param int - RowCount used to span the row
   * @exception
   * @return String object
   */
  private boolean getCalculationRequired(String strColName) {
    String strValue = "";
    String strTemp = (String) hMapSkipCalculation.get(strColName);

    if (strTemp == null) {
      return true;
    }
    return false;
  }

  /**
   * This method is used to print the single column line
   * 
   * @param int - RowCount used to span the row
   * @exception
   * @return String object
   */
  private String getAfterColLine(String strColName) {
    String strValue = "";
    String strTemp = (String) hMapAddLineAfter.get(strColName);

    if (strTemp != null) {
      strValue = "<td class=" + strLineStyle + " noWrap width=1></td>";
    }
    return strValue;
  }

  /**
   * This method is used to print the single column line
   * 
   * @param int - RowCount used to span the row
   * @exception
   * @return String object
   */
  private String getBeforeColLine(String strColName) {
    String strValue = "";
    String strTemp = (String) hMapAddLineBefore.get(strColName);

    if (strTemp != null) {
      strValue = "<td class=" + strLineStyle + " noWrap width=1></td>";
    }
    return strValue;
  }

  /**
   * This method is used to get the header style
   * 
   * @param int - RowCount used to span the row
   * @exception
   * @return String object
   */
  public String getHeadeStyler(String strColName) {
    String strValue = (String) hMapAddHeaderStyle.get(strColName);

    if (strValue == null) {
      strValue = strGeneralHeaderStyle;
    }
    return strValue;
  }

  public void addColumnType(String strColumnName, String strColumnType) {
    hMapColumnType.put(strColumnName, strColumnType);

  }

  public String getColumnType(String strColName) {
    String strValue = (String) hMapColumnType.get(strColName);

    if (strValue == null) {
      strValue = strGeneralColumnType;
    }
    return strValue;
  }

  public void addColumnSortType(String strColumnName, String strColumnSortType) {
    hMapColumnSortType.put(strColumnName, strColumnSortType);
  }

  public String getColumnSortType(String strColName) {
    String strValue = (String) hMapColumnSortType.get(strColName);

    if (strValue == null) {
      strValue = strGeneralColumnSortType;
    }
    return strValue;
  }

  /**
   * This method is used to print the single line
   * 
   * @param int - RowCount used to span the row
   * @exception
   * @return String object
   */
  private String getLineLight() {
    String strValue = "";

    if ((blSetLine) && (blDisableLine == false)) {
      strValue = "<TR bgcolor=#cccccc><TD noWrap colSpan=" + getColSpan() + " height=1></TD></TR>";
    }
    return strValue;
  }

  /**
   * This method will return the open tr information
   * 
   * @param N/A
   * @exception NA
   * @return String object
   */
  private String getTRopen(String Value, int intHierarchy) {
    String strValue = "<tr height =" + intRowHeight + " id=tr" + Value + "VP" + intHierarchy;
    if (intHierarchy != 0 && intHierarchy < alColumnStyle.size()) {
      strValue = strValue + "  class=" + getMultiColumnStyle(intHierarchy);
    } else {
      strValue = strValue + "  class=" + getMultiColumnStyle(0);
    }
    strValue = strValue + " >";
    // strValue = strValue + "<TD noWrap width=1 height=20></TD>";
    return strValue;
  }

  /**
   * This method is used to calcuate the % of growth
   * 
   * @param strCurrentSalesValue - Current Month Sales Count
   * @exception
   * @return String object
   */
  private String getGrowthPercent(String strCurrentSalesValue) {
    double dblSalesGrowth = 0;

    if (getPreviousValue() != 0) {
      dblSalesGrowth = (Double.parseDouble(strCurrentSalesValue) - getPreviousValue());
      dblSalesGrowth = (dblSalesGrowth / getPreviousValue()) * 100;
    }

    return String.valueOf(dblSalesGrowth);
  }

  /**
   * This method is used to check if any arithmetic operation has to happen for a column
   * 
   * @param strColumnName
   * @exception
   * @return String object
   */
  private boolean checkSkipCalculationforColumn(String strColName) {
    String strTemp = (String) hMapSkipCalculationForColumn.get(strColName);

    if (strTemp == null) {
      return false;
    }
    return true;
  }

  /**
   * This method is used to check for any td width
   * 
   * @param strColumnName
   * @exception
   * @return String object
   */
  public int checkColumnWidth(String strColName) {
    String strTemp = (String) hMapColumnWidth.get(strColName);

    if (strTemp == null) {
      return intAmountWidth;
    }
    return Integer.parseInt(strTemp);
  }

  /**
   * This method is used to print the Table Amount
   * 
   * @param String - Contains the Amount value
   * @exception
   * @return String object
   */
  private String printTableAmount(String Value, String ColumnName) {
    StringBuffer sbTDValue = new StringBuffer();
    double dblPercentGrowth = 0;
    String strTempValue = "";
    String strTemp = "";


    if (Value == null && blDisplayZeros) {
      Value = "0";
      // Value = blSkipCalculationForAllColumns ? "***":"0";
      // log.debug(" Value " + Value);
    }else if (Value == null) {
    	 Value = "-";
    }

    if (ColumnName.equals("Total") && !blTotalRequired) // If the column is
    // Total and the
    // Total required
    // flag is
    // false, then dont display the total
    {
      return "";
    }

    if (!blHideAmount) {
      sbTDValue.append("<td width=" + checkColumnWidth(ColumnName));
      sbTDValue.append(" class=" + gmCrossTabDecorator.getStyle(ColumnName));
      sbTDValue.append(" >");
      if (Value.length() == 0)
        sbTDValue.append("");
      // sbTDValue.append(" align=right>");
      strTempValue = divColumnValue(Value, ColumnName);
      strTempValue = roundOffValue(strTempValue, ColumnName);

      // All column drill down , currently drilldown only available for
      // zero(0) level group
      if (blAllColDrillDown & strGroupingCount.equals("0")) {
        sbTDValue.append("<a href= javascript:" + strColumnDrillDownScript + "('");
        sbTDValue.append(strRowUniqueID + "','" + ColumnName.replaceAll(" ", "/") + "','"
            + strTempValue + "')>");
        sbTDValue.append(strTempValue + "</a>");
      }

      // specific column drill down
      else if (blSelectedColDrillDown & strGroupingCount.equals("0")) {
        strTemp = (String) hMapSpecificColDrill.get(ColumnName);
        if (strTemp != null) {
          sbTDValue.append("<a href= javascript:" + strColumnDrillDownScript + "('");
          sbTDValue.append(strRowUniqueID + "','" + ColumnName.replaceAll(" ", "/") + "','"
              + strTempValue + "')>");
          sbTDValue.append(strTempValue + "</a>");
        } else {
          sbTDValue.append(strTempValue);
        }
      } else {
        sbTDValue.append(strTempValue);
      }
      sbTDValue.append("</td>");
    }

    // If Calculation required then perform the calculation
    // else skip the calculation
    if (getCalculationRequired(ColumnName)) {
      try {
        // If the report is requested for Cross tab
        // Perform the below
        dblPercentGrowth = Double.parseDouble(getGrowthPercent(Value));

        if (strReportTye == CROSSTAB_SALE_PER && !ColumnName.equals("Total")) {
          sbTDValue.append("<td width=" + intPercentWidth + " class="
              + gmCrossTabDecorator.getStyle(ColumnName) + " align=right>");
          sbTDValue.append(GmCommonClass.getRedTextPMR(GmCommonClass.getStringWithCommas(
              String.valueOf(dblPercentGrowth), 0)));
          sbTDValue.append("%</td>");
        }

        // To Display the Up and Down Arrow
        if (blShowArrow && !ColumnName.equals("Total")) {
          if (dblPercentGrowth < 0) {
            sbTDValue.append("<td class=" + gmCrossTabDecorator.getStyle(ColumnName)
                + " > <img src='" + strDownImage + "' border=0 valign=top></td>");
          } else if (dblPercentGrowth == 0) {
            sbTDValue.append("<td class=" + gmCrossTabDecorator.getStyle(ColumnName) + " > </td>");
          } else {
            sbTDValue.append("<td class=" + gmCrossTabDecorator.getStyle(ColumnName)
                + " ><img src='" + strUpImage + "' border=0 valign=top></td>");
          }
          // This Empty Line to be removed
          sbTDValue.append("<td noWrap width=1></td>");
        }

        // Holds the Previous amount
        setPreviousValue(Double.parseDouble(Value));
      } catch (NumberFormatException exp) {

      }
    }
    // log.debug(" blDisplayZeros " + blDisplayZeros + " blHideAmount " + blHideAmount+ " strVakye "
    // + sbTDValue.toString());
    return sbTDValue.toString();
  }

  /**
   * This method is used to fine tune the column value to be printed
   * 
   * @param String - Contains the Amount value
   * @exception
   * @return String object
   */
  private String divColumnValue(String strValue, String strColumnName) {
    String strTemp = "";
    double dblvalue = 0;
    try {

      if (blSkipCalculationForAllColumns || checkSkipCalculationforColumn(strColumnName)) {
        return strValue;
      }

      if (blNoDivRequired) {
        return strValue;
      }

      if (strValue != null && !strValue.equals("") && Double.parseDouble(strValue) == 0) {
        return "0";
      }

      strTemp = (String) hMapDivMandatory.get(strColumnName);
      if (strTemp != null) {
        dblvalue = Double.parseDouble(strValue) / 1000;
        return "" + dblvalue;
      }

      strTemp = (String) hMapSkipDivThousand.get(strColumnName);

      if (strTemp != null) {
        return strValue;
        // dblvalue = Double.parseDouble(Value)/1000;
      }

      if (intValueType == 0) {
        return strValue;
      } else {
        dblvalue = Double.parseDouble(strValue) / 1000;
        return "" + dblvalue;
      }
    } catch (NumberFormatException exp) {
      return strValue;
    }
  }

  /**
   * This method is used to round off the column value to be printed
   * 
   * @param String - Contains the Amount value
   * @exception
   * @return String object
   */

  private String roundOffValue(String strValue, String strColumnName) {
    String strTempValue = "";
    int skipValue = -1;
    String strTemp = "";

    try {

      // if not a number skip the calculation
      if (blSkipCalculationForAllColumns || checkSkipCalculationforColumn(strColumnName)) {
        strValue = strValue.equals("") ? "0" : strValue;
        
      }

      strTemp = (String) hMapSkipRound.get(strColumnName);
    
      // To Round Off the values
      if (strTemp != null) {
        skipValue = Integer.parseInt(strTemp);
        strTempValue = GmCommonClass.getStringWithCommas(strValue, skipValue);
      } else if (intValueType == 0) {
        strTempValue = GmCommonClass.getStringWithCommas(strValue, 0);
      } else if (intValueType == 1) {// Round off the values with one decimal place
        strTempValue = GmCommonClass.getStringWithCommas(strValue, 1);
      } else {
        strTempValue = GmCommonClass.getStringWithCommas(strValue, 2);
      }

      if ((Double.parseDouble(strTempValue.replaceAll(",", "")) == 0) && !blDisplayZeros) {
        strTempValue = "-";
      }

      if (!strValue.equals("") && !strValue.equals("-") && Double.parseDouble(strValue) == 0
          && blDisplayZeros) {
        strTempValue =
            GmCommonClass.getStringWithCommas("0", Integer.parseInt(strAllColumnPrecision));
      }
      return strTempValue;
    } catch (NumberFormatException exp) {
      return strValue;
    }

  }

  /**
   * This method is used to print the Table Amount
   * 
   * @param String - Contains the Amount value
   * @exception
   * @return String object
   */
  private String printTableFooterAmount(String Value, String ColumnName) {

    StringBuffer sbTDValue = new StringBuffer();
    double dblPercentGrowth = 0;
    String strTempValue = "";
    if (Value == null && !blDisplayZeros) {
      Value = "0";
      // Value = blSkipCalculationForAllColumns ? "***":"0";
      // log.debug(" Value " + Value);
    } else if (Value == null && blDisplayZeros) {
      Value = "-";
    }

    if (ColumnName.equals("Total") && !blTotalRequired) // If the column is
    // Total and the
    // Total required
    // flag is
    // false, then dont display the total
    {
      return "";
    }

    if (!blHideAmount) {
      sbTDValue.append("<td width=" + intAmountWidth + " class="
          + gmCrossTabDecorator.getFooterStyle(ColumnName)
          + " style='FONT-WEIGHT: bold' align=right>");
      strTempValue = divColumnValue(Value, ColumnName);
      strTempValue = roundOffValue(strTempValue, ColumnName);
      sbTDValue.append(strTempValue);
      sbTDValue.append("</td>");
    }
    // If the report is requested for Cross tab
    // Perform the below
    dblPercentGrowth = Double.parseDouble(getGrowthPercent(Value));
    // If Calculation required then perform the calculation
    // else skip the calculation
    if (getCalculationRequired(ColumnName)) {
      if (strReportTye == CROSSTAB_SALE_PER && !ColumnName.equals("Total")) {
        sbTDValue.append("<td width=" + intPercentWidth
            + " class=ShadeBlueTDSmall style='FONT-WEIGHT: bold' align=right>");
        sbTDValue.append(GmCommonClass.getRedTextPMR(GmCommonClass.getStringWithCommas(
            String.valueOf(dblPercentGrowth), 0)));
        sbTDValue.append("%</td>");
      }

      if (blShowArrow && !ColumnName.equals("Total")) {
        if (dblPercentGrowth < 0) {
          sbTDValue.append("<td class=ShadeBlueTD ><img src='" + strDownImage
              + "' border=0 valign=top></td>");
        } else if (dblPercentGrowth == 0) {
          sbTDValue.append("<td class=ShadeBlueTD></td>");
        } else {
          sbTDValue.append("<td class=ShadeBlueTD><img src='" + strUpImage
              + "' border=0 valign=top></td>");
        }
        sbTDValue.append("<td noWrap width=1></td>");
      }
    }
    // Holds the Previous amount
    setPreviousValue(Double.parseDouble(Value));

    return sbTDValue.toString();
  }

  /**
   * This method is used to get the header infromation if sort is required
   * 
   * @param String - Contains the Header value
   * @param int - intColumnIndexVal
   * @exception
   * @return String object
   */
  private String getSortHeader(String Value, int intColumnIndexVal) {
    int intSortOrderLocal = GmCommonSort.DESC;
    String strlSortType;
    String strTemp = "";
    String strScript = "";

    if (!blSortRequired) {
      return strScript;
    }
    // To toggle the sort order type
    if (intColumnIndexVal == intColumnIndex) {
      intSortOrderLocal = intSortOrder == GmCommonSort.ASC ? GmCommonSort.DESC : GmCommonSort.ASC;
    }

    // To specify the default sort type as DESC for numeric values and ASC
    // for String
    else {
      strTemp = GmCommonClass.parseNull((String) hMapSortType.get(Value));

      if (strTemp.equals("String")) {
        intSortOrderLocal = GmCommonSort.ASC;
      }
    }

    strScript =
        " onclick = javascript:fnSort('" + intColumnIndexVal + "','" + intSortOrderLocal + "');  ";

    return strScript;
  }

  /**
   * This method is used to print table header information
   * 
   * @param String - Contains the Header value
   * @exception
   * @return String object
   */
  private String printTableHeader(String Value, int intSortIndex) {
    String strTemp = new String();
    String strReplaceInputString = new String();
    String strSortHeaderScript = "";
    StringBuffer sbTDValue = new StringBuffer();
    int drillsize = 0;
    int valPosition;
    // log.debug(Value);

    strSortHeaderScript = getSortHeader(Value, intSortIndex);
    String strHeaderStyle = getHeadeStyler(Value);

    if (Value.equals("Name")) {

      if (hMapRenameColumn.get("Name") != null
          && !((String) hMapRenameColumn.get("Name")).equals("")) {
        Value = (String) hMapRenameColumn.get("Name");
      }

      sbTDValue.append("<TH width=" + intNameWidth + " class=" + strHeaderStyle + " > <a href=# ");
      sbTDValue.append(strSortHeaderScript);
      sbTDValue.append(" > ");
      sbTDValue.append(Value);
      sbTDValue.append("</a>");
      sbTDValue.append("</TH>");
    }

    else if (Value.equals("Total")) {

      if (!blHideAmount && blTotalRequired) {
        sbTDValue.append("<TH width=" + intAmountWidth + " class=" + strHeaderStyle
            + " > <a href=# ");
        sbTDValue.append(strSortHeaderScript);
        sbTDValue.append(" > ");
        sbTDValue.append(Value);
        sbTDValue.append(" </a>");
        sbTDValue.append(" </TH>");
      }

    }

    else if (Value.equals("Drill")) {
      drillsize = alDrillDownDetails.size() * 15;
      sbTDValue.append("<TH width=" + drillsize + " class=" + getHeadeStyler("Name") + " > ");
      sbTDValue.append(" </TH>");
    } else if (blHeaderImage) {
      sbTDValue
          .append("<TH width=" + intAmountWidth + " class=" + strHeaderStyle + " align=right ");
      // sbTDValue.append("<TH width=" + intAmountWidth + " align=right
      // ");
      // If the report is requested for Cross tab
      // Perform the below
      sbTDValue.append(" colspan=" + getDisplayColumn(Value) + " >  ");
      sbTDValue.append(" <img id='" + Value);
      sbTDValue.append("' src='/images/" + Value + ".gif'  width='25' height='100' ");
      sbTDValue.append(strSortHeaderScript);
      sbTDValue.append("/>");
      // sbTDValue.append(" </a>");
      sbTDValue.append(" </TH>");

    } else {
      sbTDValue.append("<TH width=" + intAmountWidth + " class=" + strHeaderStyle + "");
      // If the report is requested for Cross tab
      // Perform the below
      sbTDValue.append(" colspan=" + getDisplayColumn(Value) + " > <a href=# ");
      sbTDValue.append(strSortHeaderScript);
      sbTDValue.append(" > ");
      sbTDValue.append(Value);
      sbTDValue.append(" </a>");
      sbTDValue.append(" </TH>");

    }

    return sbTDValue.toString();
  }

  /**
   * This method is used to print the Table Name (ID is used to create the Hyper Link)
   * 
   * @param String - Contains the Name and
   * @param String - Pass null if id not required else pass id
   * @exception
   * @return String object
   */
  private String printTableName(String Value, String ID) {
    String strValue =
        "<td width=" + intNameWidth + " STYLE = \"text-align: left\" class="
            + gmCrossTabDecorator.getStyle("Name") + " >&nbsp;";
    // NULL to be replaced with -
    if (Value == null) {
      Value = "";
    }

    if (ID != null) {
      strValue = strValue + printReportLink(Value, ID);

    }
    strValue = strValue + Value;

    strValue = strValue + "</td>";
    return strValue;
  }

  /**
   * This method is used to print the Table Name (ID is used to create the Hyper Link)
   * 
   * @param String - Contains the Name and
   * @param String - Pass null if id not required else pass id
   * @exception
   * @return String object
   */
  private String printTableName(String Value, String ID, int intHierarchy) {
    StringBuffer strValue = new StringBuffer();
    intHierarchy = intHierarchy < alColumnStyle.size() ? intHierarchy : 0;
    strValue.append("<td width=" + intNameWidth);
    strValue.append("  class=" + getMultiColumnStyle(intHierarchy) + " >");

    // NULL to be replaced with -
    if (Value == null) {
      Value = "";
    }

    if (ID == null) {
      strValue.append(GmCommonClass.getStringWithTM(Value));
    } else {
      // to create the tree and a expand collapse function
      if (intHierarchy == 0) {
        strValue.append(getnbspSpace(intHierarchy) + Value);
      } else {
        strValue.append(getnbspSpace(intHierarchy));
        strValue.append("<a class=RightText  title='Click to Expand the Phase' ");
        strValue.append("href=\"javascript:Toggle('" + ID + "VP" + intHierarchy + "')\" >");
        strValue.append(Value);
        strValue.append("</a>");
      }
      // strValue = strValue + Value ;
    }
    strValue.append("</td>");
    return strValue.toString();
  }

  /**
   * This method is used to print the Table Name (ID is used to create the Hyper Link)
   * 
   * @param String - Contains the Name and
   * @param String - Pass null if id not required else pass id
   * @exception
   * @return String object
   */
  private String getnbspSpace(int intHierarchy) {
    int spacerequired = 0;
    String strValue = "";
    spacerequired = (getHierarchyCount() - intHierarchy) - 1;

    for (int j = 0; j < spacerequired; j++) {
      strValue = strValue + "&nbsp;&nbsp;&nbsp;&nbsp";
    }

    return strValue;
  }

  // To print open and close table information
  // To build tree link table structure
  private String printTreeOpenTable(String Value, int intCurrent, int intPrevious) {
    StringBuffer strValue = new StringBuffer();

    // if previous and current has same value then dont generate the tree
    // if its that the top level dont generat the tree
    if ((intCurrent == intPrevious) || (intCurrent == 0)) {
      return "";
    }

    strValue.append("<tr> <td colspan=" + getColSpan() + " >");
    strValue.append("<div style=display:none  id=div" + Value + "VP" + intCurrent + "> ");
    strValue.append("<table width=100% cellpadding=0 cellspacing=0 border=0 id=tab" + Value + "VP"
        + intCurrent + "> ");

    return strValue.toString();
  }

  // To print open and close table information
  // To build tree link table structure
  private String printTreeCloseTable(String Value, int intCurrent, int intPrevious) {
    StringBuffer strValue = new StringBuffer();
    int intPreCurrDiff = (intCurrent - intPrevious);

    // if previous and current has same value then dont generate the tree
    // if its that the top level dont generat the tree
    if ((intCurrent == intPrevious) || (intCurrent == 0)) {
      return "";
    }
    // log.debug(intCurrent + "- RC - RP " + intPrevious);
    if (intCurrent > intPrevious) {
      for (int j = 0; j < intPreCurrDiff; j++) {
        strValue.append("</table></div></td></tr>");
      }
    }

    return strValue.toString();
  }

  // To fetch the link information
  private String printDrillLink(String Value, String ID, int intHierarchy) {
    StringBuffer strValue = new StringBuffer();
    int drillsize = alDrillDownDetails.size();
    String strDrilltype = "";

    strValue.append("<td  align='left' width=" + (drillsize * 15) + ">");
    // 0 maps to no ID and cant provide any drilldown
    if (ID.equals("0")) {
      strValue.append("</td>");
      return strValue.toString();
    }

    for (int j = 0; j < drillsize; j++) {
      strDrilltype = (String) alDrillDownDetails.get(j);
      if (strDrilltype.equals("Account")) {
        strValue.append("<img id='imgEditA' style='cursor:hand' ");
        strValue.append("src='/images/HospitalIcon.gif' ");
        strValue.append("title='View Account information' ");
        // strValue.append(" width='12' height='13' ");
        strValue.append("onClick=\"javascript:fnCallAccount('");
        strValue.append(ID);
        strValue.append("', '");
        strValue.append(intHierarchy);
        strValue.append("' , '" + ID + "' )\"/> ");
        continue;
      } else if (strDrilltype.equals("Rep")) {
        strValue.append("<img id='imgEditR' style='cursor:hand' ");
        strValue.append("src='/images/icon_sales.gif' ");
        strValue.append("title='View ");
        strValue.append(" Spine Specialist Information' ");
        // strValue.append(" width='12' height='13' ");
        strValue.append("onClick=\"javascript:fnCallRepDetails('");
        strValue.append(ID);
        strValue.append("' , '" + ID + "' )\"/> ");
        continue;
      } else if (strDrilltype.equals("Group")) {
        strValue.append("<img id='imgEditG' style='cursor:hand' ");
        strValue.append("src='/images/ordsum.gif' ");
        strValue.append("title='View System Information' ");
        // strValue.append(" width='12' height='13' ");
        strValue.append("onClick=\"javascript:fnCallSetNumber('");
        strValue.append(ID);
        strValue.append("', '");
        strValue.append(intHierarchy);
        strValue.append("' , '" + ID + "' )\"/> ");
        continue;
      } else if (strDrilltype.equals("Part")) {
        strValue.append("<img id='imgEditP' style='cursor:hand' ");
        strValue.append("src='/images/product_icon.jpg' ");
        strValue.append("title='View Part Details' ");
        // strValue.append(" width='12' height='13' ");
        strValue.append("onClick=\"javascript:fnCallPart('");
        strValue.append(ID);
        strValue.append("', '");
        strValue.append(intHierarchy);
        strValue.append("' , '" + ID + "' )\"/> ");
        continue;
      } else if (strDrilltype.equals("Cons")) {
        strValue.append("<img id='imgEditP' style='cursor:hand' ");
        strValue.append("src='/images/consignment.gif' ");
        strValue.append("title='View Consignment Detials' ");
        // strValue.append(" width='12' height='13' ");
        strValue.append("onClick=\"javascript:fnCallCons('");
        strValue.append(ID);
        strValue.append("', '");
        strValue.append(intHierarchy);
        strValue.append("' , '" + ID + "' )\"/> ");
        continue;
      } else if (strDrilltype.equals("Disp")) {
        strValue.append("<img id='imgEditP' style='cursor:hand' ");
        strValue.append("src='/images/new.gif' ");
        strValue.append("title='View Field Sales Details' ");
        // strValue.append(" width='12' height='13' ");
        strValue.append("onClick=\"javascript:fnCallCons('");
        strValue.append(ID);
        strValue.append("', '");
        strValue.append(intHierarchy);
        strValue.append("' , '" + ID + "' )\"/> ");
        continue;
      }
    }
    strValue.append("</td>");
    return strValue.toString();
  }

  // To fetch the link information
  private String printReportLink(String Value, String ID) {
    StringBuffer strValue = new StringBuffer();
    String strDrilltype = "";
    int drillsize = alDrillDownDetails.size();
    int intHierarchy = 0;

    if (drillsize == 0) {
      if (strLinkType.equals("LoadGCONS")) {
        strValue.append("<img id='imgEdit' style='cursor:hand' ");
        strValue.append("src='/images/icon-letter.png' ");
        strValue.append("title='View Field Sales Consignment detail '");
        // strValue.append(" width='14' height='14' ");
        strValue.append("onClick=\"javascript:fnCallCons('");
        strValue.append(ID);
        strValue.append("' , '" + strLinkType + "' )\"/> ");
      }

      if (strLinkType.equals("LoadDCONS") || strLinkType.equals("LoadGCONS")) {
        strValue.append("<img id='imgEdit' style='cursor:hand' ");
        strValue.append("src='/images/consignment.gif' ");
        strValue.append("title='View Consignment detail '");
        // strValue.append(" width='14' height='14' ");
        strValue.append("onClick=\"javascript:fnCallConsD('");
        strValue.append(ID);
        strValue.append("' , '" + strLinkType + "' )\"/> ");
      }

      if (!strLinkType.equals("LoadGroup") && !strLinkType.equals("LoadGroupA")
          && !strLinkType.equals("LoadGCONS")) {
        strValue.append("<img id='imgEdit' style='cursor:hand' ");
        strValue.append("src='/images/ordsum.gif' ");
        strValue.append("title='View Set System Information' ");
        // strValue.append(" width='14' height='14' ");
        strValue.append("onClick=\"javascript:fnCallSetNumber('");
        strValue.append(ID);
        strValue.append("' , '" + strLinkType + "' )\"/> ");

      }

      if (!strLinkType.equals("LoadAccount") && !strLinkType.equals("LoadAccountA")) {
        strValue.append("<img id='imgEdit' style='cursor:hand' ");
        strValue.append("src='/images/HospitalIcon.gif' ");
        strValue.append("title='View Account Information '");
        // strValue.append(" width='14' height='14' ");
        strValue.append("onClick=\"javascript:fnCallAccount('");
        strValue.append(ID);
        strValue.append("' , '" + strLinkType + "' )\"/> ");

      }

      if (!strLinkType.equals("LoadRep") && !strLinkType.equals("LoadRepA")) {
        strValue.append("<img id='imgEdit' style='cursor:hand' ");
        strValue.append("src='/images/icon_sales.gif' ");
        strValue.append("title='View Spine Specialist Information ' ");
        // strValue.append(" width='14' height='14' ");
        strValue.append("onClick=\"javascript:fnCallRepDetails('");
        strValue.append(ID);
        strValue.append("' , '" + strLinkType + "' )\"/> ");

      }

      if (!strLinkType.equals("LoadPart") && !strLinkType.equals("LoadPartA")
          && !strLinkType.equals("LoadGCONS") && !strLinkType.equals("LoadDCONS")) {
        strValue.append("<img id='imgEdit' style='cursor:hand' ");
        strValue.append("src='/images/product_icon.jpg' ");
        strValue.append("title='View Part Information'");
        // strValue.append(" width='14' height='14' ");
        strValue.append("onClick=\"javascript:fnCallPart('");
        strValue.append(ID);
        strValue.append("' , '" + strLinkType + "' )\"/> ");

      }

      // To load distributor information
      if (strLinkType.equals("LoadGroup")) {
        strValue.append("<img id='imgEdit' style='cursor:hand' ");
        strValue.append("src='/images/icon-letter.png' ");
        strValue.append("title='View System Information' ");
        // strValue.append(" width='14' height='14' ");
        strValue.append("onClick=\"javascript:fnCallDisDetails('");
        strValue.append(ID);
        strValue.append("' , '" + strLinkType + "' )\"/> ");

      }
    } else {
      for (int j = 0; j < drillsize; j++) {
        strDrilltype = (String) alDrillDownDetails.get(j);
        if (strDrilltype.equals("LoadGCONS")) {
          strValue.append("<img id='imgEdit' style='cursor:hand' ");
          strValue.append("src='/images/icon-letter.png' ");
          strValue.append("title='View Field Sales Consignment detail '");
          // strValue.append(" width='14' height='14' ");
          strValue.append("onClick=\"javascript:fnCallCons('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
        } else if (strDrilltype.equals("Account")) {
          strValue.append("<img id='imgEditA' style='cursor:hand' ");
          strValue.append("src='/images/HospitalIcon.gif' ");
          strValue.append("title='View Account information' ");
          // strValue.append(" width='12' height='13' ");
          strValue.append("onClick=\"javascript:fnCallAccount('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
          continue;
        } else if (strDrilltype.equals("Rep")) {
          strValue.append("<img id='imgEditR' style='cursor:hand' ");
          strValue.append("src='/images/icon_sales.gif' ");
          strValue.append("title='View ");
          strValue.append(" Spine Specialist Information' ");
          // strValue.append(" width='12' height='13' ");
          strValue.append("onClick=\"javascript:fnCallRepDetails('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
          continue;
        } else if (strDrilltype.equals("Group") || strDrilltype.equals("LoadDCONS")) {
          strValue.append("<img id='imgEditG' style='cursor:hand' ");
          strValue.append("src='/images/ordsum.gif' ");
          strValue.append("title='View System Information' ");
          // strValue.append(" width='12' height='13' ");
          strValue.append("onClick=\"javascript:fnCallSetNumber('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
          continue;
        } else if (strDrilltype.equals("Part")) {
          strValue.append("<img id='imgEditP' style='cursor:hand' ");
          strValue.append("src='/images/product_icon.jpg' ");
          strValue.append("title='View Part Details' ");
          // strValue.append(" width='12' height='13' ");
          strValue.append("onClick=\"javascript:fnCallPart('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
          continue;
        } else if (strDrilltype.equals("OPPart")) {
          strValue.append("<img id='imgEditP' style='cursor:hand' ");
          strValue.append("src='/images/product_icon.jpg' ");
          strValue.append("title='Click For More Info' ");
          // strValue.append(" width='12' height='13' ");
          strValue.append("onClick=\"javascript:fnCallPart('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
          continue;
        } else if (strDrilltype.equals("Cons")) {
          strValue.append("<img id='imgEditP' style='cursor:hand' ");
          strValue.append("src='/images/consignment.gif' ");
          strValue.append("title='View Consignment Details' ");
          // strValue.append(" width='12' height='13' ");
          strValue.append("onClick=\"javascript:fnCallConsD('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
          continue;
        } else if (strDrilltype.equals("Disp")) {
          strValue.append("<img id='imgEditP' style='cursor:hand' ");
          strValue.append("src='/images/product_icon.jpg' ");
          strValue.append("title='View Field Sales Part Details' ");
          // strValue.append(" width='12' height='13' ");
          strValue.append("onClick=\"javascript:fnCallDisp('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
          continue;
        } else if (strDrilltype.equals("AD")) {
          strValue.append("<img id='imgEdit' style='cursor:hand' ");
          strValue.append("src='/images/a.gif' ");
          strValue.append("title='View AD Consignment detail '");
          // strValue.append(" width='14' height='14' ");
          strValue.append("onClick=\"javascript:fnCallAD('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
        }

        else if (strDrilltype.equals("LoanerSet")) {
          strValue.append("<img id='imgEdit' style='cursor:hand' ");
          strValue.append("src='/images/loaner.gif' ");
          strValue.append("title='View Loaner set details '");
          strValue.append(" width='14' height='14' ");
          strValue.append("onClick=\"javascript:fnCallLoanerSetDetails('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
        }

        else if (strDrilltype.equals("LoanerDist")) {
          strValue.append("<img id='imgEdit' style='cursor:hand' ");
          strValue.append("src='/images/icon-letter.png' ");
          strValue.append("title='View Loaner distributor details by set'");
          strValue.append(" width='14' height='14' ");
          strValue.append("onClick=\"javascript:fnCallLoanerDistDetails('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
        } else if (strDrilltype.equals("Actual")) {
          strValue.append("<img id='imgEdit' style='cursor:hand' ");
          strValue.append("src='/images/a.gif' ");
          strValue.append("title='View Actual Values '");
          // strValue.append(" width='14' height='14' ");
          strValue.append("onClick=\"javascript:fnCallActual('");
          strValue.append(ID);
          strValue.append("' , '" + strLinkType + "' )\"/> ");
        }
      }
    }

    return strValue.toString();
  }

  /**
   * This method is used to print the no data information
   * 
   * @param NA
   * @exception NA
   * @return String object
   */
  private String getNoData() {
    String strValue =
        "<table align='center'>" + "<tr><td class='RightTableCaption'> No Data to Display "
            + " </td></tr> </table>";
    strValue = strValue + getLine();
    return strValue;
  }

  /**
   * @return Returns the intHierarchyCount.
   */
  private int getHierarchyCount() {
    return intHierarchyCount;
  }

  /**
   * @param intHierarchyCount The intHierarchyCount to set.
   */
  private void setHierarchyCount(int intHierarchyCount) {
    this.intHierarchyCount = intHierarchyCount;
  }

  /**
   * @return Returns the alColumnStyle.
   */
  private String getMultiColumnStyle(int intHierarchyCount) {
    return (String) alColumnStyle.get(intHierarchyCount);
  }

  /**
   * @param alColumnStyle The alColumnStyle to set.
   */
  public void setMultiColumnStyle(ArrayList alColumnStyle) {
    this.alColumnStyle = alColumnStyle;
  }

  /**
   * @return Returns the alDrillDownDetails. holds drilldown information
   */
  private String getDrillDownDetails(int intposition) {
    return (String) alDrillDownDetails.get(intposition);
  }

  /**
   * @param alDrillDownDetails The alDrillDownDetails to set.
   */
  public void setDrillDownDetails(ArrayList alDrillDownDetails) {
    this.alDrillDownDetails = alDrillDownDetails;
  }

  /**
   * @param strColumnName. The columnName which is currently sorted
   */
  public void setSortColumn(int intColumn) {
    this.intColumnIndex = intColumn;
  }

  /**
   * @param strSortOrderType. Specify if the sort is ASC or DESC
   */
  public void setSortOrder(int intSortOrderType) {
    this.intSortOrder = intSortOrderType;
  }

  /**
   * @param setSortRequired. Specify if the sort is required or not
   */
  public void setSortRequired(boolean blValue) {
    this.blSortRequired = blValue;
  }

  /**
   * This method is used to sort an ArrayList. If the SortColumn is "" or null then no sort would
   * happen
   * 
   * @param alInputList
   * @exception NA
   * @return alSortedList or alInputList
   */
  public ArrayList sortList(ArrayList alInputList, ArrayList alHeaderList) {
    ArrayList alSortedList = new ArrayList();
    String strTemp;

    String strSortColumn = "";

    // Default Sort by the specified column name
    if (intColumnIndex == -1 && defaultSortColumn != null && !defaultSortColumn.equals("")) {
      intColumnIndex = alHeaderList.indexOf(defaultSortColumn);
      intSortOrder = 1;
    }

    // if none of the column name is given for sort and if there is no
    // default sort column name or if the # of columns have shrunk due to filter condn
    if (intColumnIndex == -1 || !blSortRequired || intColumnIndex > alHeaderList.size()) {
      return alInputList;

    }

    strSortColumn = (String) alHeaderList.get(intColumnIndex);
    strTemp = GmCommonClass.parseNull((String) hMapSortType.get(strSortColumn));

    if (strTemp.equals("String")) {
      strVarType = "String";
    } else if (strTemp.equals("Date")) {
      strVarType = "Date";
    }

    alSortedList =
        (ArrayList) GmCommonSort
            .sortArrayList(alInputList, strSortColumn, strVarType, intSortOrder);
    return alSortedList;
  }

  public void setAdditionalValMap(HashMap hmAdditionalVal) {
    this.hMapAdditionalVal = hmAdditionalVal;
  }

  public HashMap getAdditionalValMap() {
    return hMapAdditionalVal;
  }

}
