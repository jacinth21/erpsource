package com.globus.common.beans;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDataDownloadRptBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // Initialize
  // the
  // Logger
  // Class.

  public GmDataDownloadRptBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmDataDownloadRptBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public HashMap fetchBatchDtls(int intBatchID) throws AppError, IOException {

    RowSetDynaClass rdBatchList = null;

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    HashMap hmReturn = new HashMap();
    HashMap hmBatchDetails = new HashMap();
    ArrayList alBatchList = new ArrayList();

    String strCompanyId = getGmDataStoreVO().getCmpid();
    String location = System.getProperty("ENV_GMSAGEFILELOC");

    gmDBManager.setPrepareString("GM_PKG_CM_DATA_DOWNLOAD.GM_DD_FCH_DATA_DOWNLOAD", 3);

    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);

    gmDBManager.setInt(1, intBatchID);

    gmDBManager.execute();

    hmBatchDetails =
        GmCommonClass.parseNullHashMap(gmDBManager.returnHashMap((ResultSet) gmDBManager
            .getObject(2)));
    // alBatchList = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet)
    // gmDBManager.getObject(3)));
    rdBatchList = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    hmReturn.put("HMBATCHDETAIL", hmBatchDetails);
    hmReturn.put("RDBATCHLIST", rdBatchList);

    log.debug("hmBatchDetails:" + hmBatchDetails);
    log.debug("alBatchList:" + alBatchList);
    log.debug("alBatchList size:" + alBatchList.size());

    // Batch details will store based on companies in different folders.
    if (strCompanyId.equals("1000")) {
      location = System.getProperty("ENV_GMSAGEFILELOC");
    } else {
      location = GmCommonClass.getString("GMSAGEFILELOCFOROUS");
      String StrShrtCmpName = GmCommonClass.getCompanyLocale(strCompanyId);
      location = location + "\\G" + StrShrtCmpName + "\\";
    }

    hmReturn.put("LOCATION", location);
    String file_name = GmCommonClass.parseNull((String) hmBatchDetails.get("FILE_NAME"));
    String batch_type = GmCommonClass.parseNull((String) hmBatchDetails.get("DOWNLOAD_TYPE"));
    log.debug("location:" + location);
    log.debug("fileName:" + file_name);
    log.debug("batch_type:" + batch_type);
    return hmReturn;


  }

  public RowSetDynaClass fetchBatchReport(HashMap hmParam) throws AppError {

    RowSetDynaClass rdBatchRpt = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    // String strInput = GmCommonClass.parseNull((String) hmParam.get("HINPUTSTR"));
    String strBatchNumber = GmCommonClass.parseNull((String) hmParam.get("BATCHNUMBER"));
    String strBatchDateFrom = GmCommonClass.parseNull((String) hmParam.get("BATCHDATEFROM"));
    String strBatchDateTo = GmCommonClass.parseNull((String) hmParam.get("BATCHDATETO"));

    log.debug(" values in hmParam " + hmParam);
    if (strBatchNumber.equals(""))
      strBatchNumber = "0";
    gmDBManager.setPrepareString("gm_pkg_cm_data_download.gm_dd_fch_batch_list", 4);
    // gmDBManager.registerOutParameter(3, java.sql.Types.CHAR);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);

    gmDBManager.setInt(1, Integer.parseInt(strBatchNumber));
    gmDBManager.setString(2, strBatchDateFrom);
    gmDBManager.setString(3, strBatchDateTo);

    gmDBManager.execute();

    // String strMAstatus = gmDBManager.getString(3);
    rdBatchRpt = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(4));

    gmDBManager.close();


    return rdBatchRpt;

  }

}
