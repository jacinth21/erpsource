package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;



public class GmAuditTrailBean extends GmBean {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  /**
   * loadPartHistory - This method will load required date history Info
   * 
   * @param strRequestId
   * @return ArrayList
   * @exception AppError
   */
  // this will be removed all place changed with Data Store VO constructor
  public GmAuditTrailBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAuditTrailBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public RowSetDynaClass fetchQtyHistory(String strRequestId, String strAuditId) throws AppError {
    return fetchQtyHistory(strRequestId, strAuditId, "");
  }

  public RowSetDynaClass fetchQtyHistory(String strRequestId, String strAuditId,
      String strJNDIConnection) throws AppError {

    RowSetDynaClass rdResult = null;

    GmDBManager gmDBManager = GmDBManager.getGmDBManager(strJNDIConnection);
    gmDBManager.setPrepareString("gm_pkg_cm_audit_trail.gm_fch_history", 3);
    if ((strRequestId != null && strRequestId.equals(""))) {
      strRequestId = null;
    }

    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRequestId);
    gmDBManager.setString(2, strAuditId);
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();

    return rdResult;
  }

  /**
   * 
   * fetchDynamicAuditTrailLog - This method used to fetch the Audit log information.
   * 
   * @param hmParam
   * @return RowSetDynaclass
   * @throws AppError
   */

  public RowSetDynaClass fetchDynamicAuditTrailLog(HashMap hmParam) throws AppError {

    RowSetDynaClass rdResult = null;

    String strRuleId = GmCommonClass.parseNull((String) hmParam.get("STRPROCEDURERULEID"));
    String strTxnId = GmCommonClass.parseNull((String) hmParam.get("TXNID"));
    String strType = GmCommonClass.parseNull((String) hmParam.get("STRAUDITTYPE"));


    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_audit_trail.gm_fch_dynamic_log_history", 5);


    gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
    gmDBManager.setString(1, strRuleId);
    gmDBManager.setString(2, strTxnId);
    gmDBManager.setString(3, strType);
    gmDBManager.setString(4, getCompDateFmt());
    gmDBManager.execute();

    rdResult = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(5));
    gmDBManager.close();

    return rdResult;
  }
}
