/**
 * This class contains methods that are common to all components
 * 
 * @author $Author:$
 * @version $Revision:$ <br>
 *          $Date:$
 */

package com.globus.common.beans;

// Java Classes
import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.util.GmTemplateUtil;

public class GmCommonControls extends Exception {

  static String strWidth = "";
  static String strHeight = "";

  /**
   * This method returns a String containing based on Code Group as the input <br>
   * <ol>
   * <li>(SEE CODE).<br>
   * <li><br>
   * </ol>
   * 
   * @param strCodeGrp - Code Grp
   * @exception AppError
   * @return String object
   */
  // Optional to specify the width of the Tabindex
  public static void setWidth(int value) {
    strWidth = " style=\"width:" + value + "px;height:" + strHeight + "px;\"";
  }

  public static void setHeight(int value) {
    strHeight = "" + value;
  }

  public static String getChkBoxGroup(String strJSFunction, ArrayList alList, String strGrpNm) {
    return getChkBoxGroup(strJSFunction, alList, strGrpNm, null, null);
  }

  /**
   * This method returns a String containing based on Code Group as the input <br>
   * <ol>
   * <li>(SEE CODE).<br>
   * <li><br>
   * </ol>
   * 
   * @param strCodeGrp - Code Grp
   * @exception AppError
   * @return String object
   */

  public static String getChkBoxGroup(String strJSFunction, ArrayList alList, String strGrpNm,
      HashMap hmMap) {
    return getChkBoxGroup(strJSFunction, alList, strGrpNm, hmMap, null);
  }

  /**
   * This method returns a String containing based on Code Group as the input <br>
   * <ol>
   * <li>(SEE CODE).<br>
   * <li><br>
   * </ol>
   * 
   * @param strCodeGrp - Code Grp
   * @exception AppError
   * @return String object
   */

  public static String getChkBoxGroup(String strJSFunction, ArrayList alList, String strGrpNm,
      HashMap hmMap, String strType) {
    int intSize = 0;
    HashMap hcboVal = null;
    StringBuffer sbHtml = new StringBuffer();
    String strCodeId = "";
    String strLinkId = "";
    String strLabel = "";

    String strID = "ID";
    String strPID = "PID";
    String strNM = "NM";

    if (hmMap != null) {
      strID = (String) hmMap.get("ID");
      strPID = (String) hmMap.get("PID");
      strNM = (String) hmMap.get("NM");
    }
    sbHtml.append("<DIV class='gmFilter' id='Div_");
    sbHtml.append(strGrpNm);
    sbHtml.append("' " + strWidth + " " + strHeight);
    sbHtml.append(">");
    sbHtml.append("<ul style=\"list-style-type: none; padding: 0; margin: 0;\">");

    // clear the width for other controls because of static variable
    strWidth = "";
    strHeight = "";
    try {
      intSize = alList.size();
      hcboVal = new HashMap();
      for (int i = 0; i < intSize; i++) {
        hcboVal = (HashMap) alList.get(i);
        strCodeId = GmCommonClass.parseNull((String) hcboVal.get(strID));
        strLinkId = GmCommonClass.parseNull((String) hcboVal.get(strPID));
        strLabel = GmCommonClass.parseNull((String) hcboVal.get(strNM));
        if (strType != null && strType.equals("IDNM")) {
          strLabel = strLinkId.concat(" / ").concat(strLabel);
        }
        sbHtml.append("<li>");
        sbHtml.append("<input class='RightInput' type='checkbox' name='Chk_Grp");
        sbHtml.append(strGrpNm);
        sbHtml.append("' id='");
        sbHtml.append(strCodeId);
        sbHtml.append("' value='");
        sbHtml.append(strLinkId);
        sbHtml.append("' ");
        if (!strJSFunction.equals("")) {
          sbHtml.append("onClick='javascript:");
          sbHtml.append(strJSFunction);
          sbHtml.append("'");
        }

        sbHtml.append(">&nbsp;<span class='RightTextAS'>");
        sbHtml.append(strLabel);
        sbHtml.append("</span><BR>");
        sbHtml.append("</li>");
      }
      sbHtml.append("</ul>");
      sbHtml.append("</DIV>");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sbHtml.toString();
  }

  public static String getComboXml(ArrayList alParam) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alComboList", alParam);
    templateUtil.setTemplateSubDir("common/templates");
    templateUtil.setTemplateName("GmCommonCombo.vm");
    return templateUtil.generateOutput();

  }

  public static String getOptBoxGroup(String strJSFunction, ArrayList alList, String strGrpNm,
      HashMap hmMap, String strType) {
    int intSize = 0;
    HashMap hcboVal = null;
    StringBuffer sbHtml = new StringBuffer();
    String strCodeId = "";
    String strLinkId = "";
    String strLabel = "";

    String strID = "ID";
    String strPID = "PID";
    String strNM = "NM";

    if (hmMap != null) {
      strID = (String) hmMap.get("ID");
      strPID = (String) hmMap.get("PID");
      strNM = (String) hmMap.get("NM");
    }
    sbHtml.append("<DIV class='gmFilter' align='left' id='Div_");
    sbHtml.append(strGrpNm);
    sbHtml.append("' " + strWidth + " " + strHeight);
    sbHtml.append(">");
    sbHtml.append("<ol style=\"list-style-type: none; padding: 0; margin: 0;\">");

    // clear the width for other controls because of static variable
    strWidth = "";
    strHeight = "";
    try {
      intSize = alList.size();
      hcboVal = new HashMap();
      for (int i = 0; i < intSize; i++) {
        hcboVal = (HashMap) alList.get(i);
        strCodeId = GmCommonClass.parseNull((String) hcboVal.get(strID));
        strLinkId = GmCommonClass.parseNull((String) hcboVal.get(strPID));
        strLabel = GmCommonClass.parseNull((String) hcboVal.get(strNM));
        if (strType != null && strType.equals("IDNM")) {
          strLabel = strLinkId.concat(" / ").concat(strLabel);
        }
        sbHtml.append("<li>");
        sbHtml.append("<input class='RightInput' type='radio' name='Opt_Grp'");
        // sbHtml.append(strGrpNm);
        sbHtml.append(" id='");
        sbHtml.append(strCodeId);
        sbHtml.append("' value='");
        sbHtml.append(strLinkId);
        sbHtml.append("' ");
        if (!strJSFunction.equals("")) {
          sbHtml.append("onClick='javascript:");
          sbHtml.append(strJSFunction);
          sbHtml.append("'");
        }

        sbHtml.append(">&nbsp;<span class='RightTextAS'>");
        sbHtml.append(strLabel);
        sbHtml.append("</span><BR>");
        sbHtml.append("</li>");
      }
      sbHtml.append("</ol>");
      sbHtml.append("</DIV>");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sbHtml.toString();
  }
} // End of GmCommonControls
