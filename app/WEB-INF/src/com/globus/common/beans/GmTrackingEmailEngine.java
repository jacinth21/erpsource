
package com.globus.common.beans;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmLogger;
import com.globus.common.email.GmEmailBean;
import com.globus.common.jms.GmMessageTransferObject;
import com.globus.common.jms.GmQueueProducer;
import com.globus.common.beans.GmCommonBean;
import com.globus.valueobject.common.GmDataStoreVO;

import java.util.HashMap;

/**
 * @author MKosalram GmTrackingEmailEngine --- GmTrackingEmailEngine Send
 *         Tradeshow Attendee Email
 */
public class GmTrackingEmailEngine {
	 Logger log = GmLogger.getInstance(this.getClass().getName());
	 
	 
/**
 * This method used to sendEmailTracking for Sending Tradeshow Attendee Email
 * @return
 * @throws AppError
*/
public String sendEmailTracking(HashMap hmEmailProperty) throws AppError {
	String strResult ="";
	
		try {
			//PC-2939
			hmEmailProperty.put("FROM_OVERRIDE_FL", GmCommonClass.getString("FROM_OVERRIDE_FL"));
					sendEmailJMS(hmEmailProperty);
					strResult = "true";
				} catch (Exception ex) {
					strResult = "false";
					log.error("Exception in sending" + ex.getMessage());
					throw new AppError(ex);
			}
    return strResult;
}

//PC-2939
/**
 * This method used to sendEmailTracking for Sending Tradeshow Attendee Email
 * @return
 * @throws AppError
*/
public boolean sendEmailJMS(HashMap hmEmailProperty) throws AppError {

	  GmQueueProducer qprod = new GmQueueProducer();
	  GmMessageTransferObject tf = new GmMessageTransferObject();
	  
	  String strFrom = GmCommonClass.parseNull((String) hmEmailProperty.get("strFrom")); 
	  String[] strTo = (String[]) hmEmailProperty.get("strTo");
	  String[] strCc = (String[]) hmEmailProperty.get("strCc");
	  String[] strBcc = (String[]) hmEmailProperty.get("strBcc");
	  String strSubject = GmCommonClass.parseNull((String) hmEmailProperty.get("strSubject"));
	  String strMessageDesc = GmCommonClass.parseNull((String) hmEmailProperty.get("strMessageDesc"));
	  String strMimeType = GmCommonClass.parseNull((String) hmEmailProperty.get("strMimeType"));
	  String strAttachment = GmCommonClass.parseNull((String) hmEmailProperty.get("strAttachment"));
	  String[] strReplyTo = (String[]) hmEmailProperty.get("strReplyTo");
	  String strEmailHeader = GmCommonClass.parseNull((String) hmEmailProperty.get("strEmailHeader")); 
	  String strFromOverrideFl = GmCommonClass.parseNull((String) hmEmailProperty.get("FROM_OVERRIDE_FL")); 
	  
		HashMap hmJMSParam = new HashMap();
	  	hmJMSParam.put("TO", strTo);
	  	hmJMSParam.put("CC", strCc);
	  	hmJMSParam.put("BCC", strBcc);
	  	hmJMSParam.put("STRREPLYTO", strReplyTo);
	  	hmJMSParam.put("STRFROM", strFrom);
	  	hmJMSParam.put("STRSUBJECT", strSubject);
	  	hmJMSParam.put("STRMESSAGEDESC", strMessageDesc);
	  	hmJMSParam.put("STREMAILATTACHMENT", strAttachment);
	  	hmJMSParam.put("STRMIMETYPE", strMimeType);
	  	hmJMSParam.put("WHITELISTSERVER", (Boolean)GmCommonClass.blWhitelistserver);
	  	hmJMSParam.put("TESTINGSERVER", (Boolean)GmCommonClass.blTestingserver);
	  	hmJMSParam.put("TOEMAIL", GmCommonClass.toEmail);
	  	hmJMSParam.put("TESTEMAILSUBJECT", GmCommonClass.strTestEmailSubject);
	  	hmJMSParam.put("TESTEMAILID", GmCommonClass.strTestEmailID);
        hmJMSParam.put("EMAILHEADERNM", strEmailHeader);
        hmJMSParam.put("FROMOVERRIDEFL", strFromOverrideFl);
        
	  	
		// This is JMS Code which will process the notification, jms/gmemailqueue is the JMS
	    // queue using for notification
	    tf.setMessageObject(hmJMSParam);
	    tf.setConsumerClass(GmCommonClass.parseNull(GmCommonClass.getString("TRADESHOW_EMAIL_CONSUMER_CLASS")));
	    qprod.sendMessage(tf,GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_EMAIL_LOC_QUEUE")));
	    return true;
} // end of sendEmail method



}
 
