/**
 * FileName : GmAccessControlBean.java Description : GmAccessControlBean.java Author : rshah Date :
 * Feb 25, 2009 Copyright : Globus Medical Inc
 */
package com.globus.common.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.logon.GmSecurityEventVO;

/**
 * @author rshah
 * 
 */
public class GmAccessControlBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // this will be removed all place changed with Data Store VO constructor
	

  public GmAccessControlBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmAccessControlBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());

  /**
   * getPartyList - This method will be used to get the party list of perticular group.
   * 
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList getPartyList(String strGroupId, String strPartyId) throws AppError {
    ArrayList alList = new ArrayList();

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_partylist", 3);
    gmDBManager.setString(1, GmCommonClass.parseNull(strGroupId));
    gmDBManager.setString(2, GmCommonClass.parseNull(strPartyId));
    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.execute();
    alList = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    // log.debug("In setup bean......"+alList);
    return alList;
  }

  /**
   * getAccessPermissions - This method will be used to get the access permissions.
   * This method/DB File used in shipping Device (erpshipping repository).
   * Please make sure the changes are done in that repository too if param added/removed for the DB call.	
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public HashMap getAccessPermissions(String strPartyId, String strFunctionId) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_accesspermissions", 3);

    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.setString(2, strFunctionId);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * getAccessPermissions - This method will be used to get the access permissions.
   * 
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public String getAccessPermission(String strPartyId, String strRequestURI) throws AppError {
    String strReturn = "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_accesspermission", 3);

    gmDBManager.registerOutParameter(3, OracleTypes.CHAR);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.setString(2, strRequestURI);
    gmDBManager.execute();
    strReturn = (String) gmDBManager.getObject(3);
    gmDBManager.close();
    return strReturn;
  }

  /**
   * saveACGroup - This method will be used to save the access control group.
   * 
   * 
   * @exception AppError
   */
  public String saveACGroup(HashMap hmParam) throws AppError {
    String strgroupId = GmCommonClass.parseNull((String) hmParam.get("GROUPID"));
    String strgroupName = GmCommonClass.parseNull((String) hmParam.get("GROUPNAME"));
    String strgroupDesc = GmCommonClass.parseNull((String) hmParam.get("GROUPDESC"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strGroupType = GmCommonClass.parseNull((String) hmParam.get("GROUPTYPE"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_sav_ac_group", 7);
    gmDBManager.setString(1, strgroupId);
    gmDBManager.setString(2, strgroupName);
    gmDBManager.setString(3, strgroupDesc);
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, strOpt);
    gmDBManager.setString(6, strGroupType);
    gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
    gmDBManager.execute();
    strgroupId = GmCommonClass.parseNull(gmDBManager.getString(7));
    gmDBManager.commit();
    return strgroupId;
  }

  /**
   * fetchACGroup - This method will be used to fetch the access control group for specific groupID.
   * 
   * @return HashMap - will be used to set GmAccessControlForm
   * @exception AppError
   */
  public HashMap fetchACGroup(String strGrpID) throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_ac_group", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strGrpID);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchACGroupDetails - This method will be used to fetch the access control groups.
   * 
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList fetchACGroupDetails(String strGroupType) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_ac_group_list", 2);
    gmDBManager.setString(1, strGroupType);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * fetchUserList - This method will be used to fetch the access control groups.
   * 
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList fetchUserList(String strDeptID) throws AppError {
    ArrayList alReturn = new ArrayList();
    strDeptID = GmCommonClass.parseNull(strDeptID).equals("0") ? "" : strDeptID;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_dept_user_list", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strDeptID);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  public String saveUserGroup(HashMap hmParam) throws AppError {

    log.debug("hmParam : " + hmParam);

    String strGrpMappingID = GmCommonClass.parseNull((String) hmParam.get("GRPMAPPINGID"));
    String strUser = GmCommonClass.parseNull((String) hmParam.get("USERNM"));
    String strGroupName = GmCommonClass.parseNull((String) hmParam.get("GROUPNAME"));
    String strDefaultGrp = GmCommonClass.parseNull((String) hmParam.get("DEFAULTGRP"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));

    strDefaultGrp = strDefaultGrp.equals("on") ? "Y" : "";
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_sav_user_group", 6);
    gmDBManager.registerOutParameter(6, java.sql.Types.CHAR);
    gmDBManager.setString(1, strGroupName);
    gmDBManager.setInt(2, Integer.parseInt(strUser));
    gmDBManager.setString(3, strDefaultGrp);
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, strCompanyId);
    gmDBManager.setString(6, strGrpMappingID);
    gmDBManager.execute();
    strGrpMappingID = GmCommonClass.parseNull(gmDBManager.getString(6));
    gmDBManager.commit();
    return strGrpMappingID;
  }

  /**
 * 
 */

  public void saveSecModGroup(HashMap hmParam) throws AppError {
    log.debug("hmParam SecModGroup : " + hmParam);
    String strGroupName = GmCommonClass.parseNull((String) hmParam.get("GROUPNAME"));
    String strGroupId = GmCommonClass.parseNull((String) hmParam.get("MODULENAME"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_sav_secmod_group", 3);
    gmDBManager.setString(1, strGroupId);
    gmDBManager.setString(2, strGroupName);
    gmDBManager.setInt(3, Integer.parseInt(strUserId));
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * fetchGroupMappingDetails - This method will be used to fetch the access control groups.
   * 
   * @return ArrayList - will be used by drop down tag
   * @exception AppError
   */
  public ArrayList fetchGroupMappingDetails(HashMap hmParam) throws AppError {
    ArrayList alReturn = new ArrayList();
    String strGrpMappingID = GmCommonClass.parseNull((String) hmParam.get("GRPMAPPINGID"));
    String strUser = GmCommonClass.parseNull((String) hmParam.get("USERNM"));
    String strGroupName = GmCommonClass.parseNull((String) hmParam.get("GROUPNAME"));
    String strDefaultGrp = GmCommonClass.parseNull((String) hmParam.get("DEFAULTGRP"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strDept = GmCommonClass.parseNull((String) hmParam.get("USERDEPT"));
    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
    strDefaultGrp = strDefaultGrp.equals("on") ? "Y" : "";
    

    
    if (strGrpMappingID.equalsIgnoreCase("")) {
      strGrpMappingID = null;
    }
    if (strUser.equalsIgnoreCase("0")) {
      strUser = null;
    }
    if (strGroupName.equalsIgnoreCase("0")) {
      strGroupName = null;
    }
    if (strUserId.equalsIgnoreCase("")) {
      strUserId = null;
    }
    if (strDefaultGrp.equalsIgnoreCase("")) {
      strDefaultGrp = null;
    }
    if (strDept.equalsIgnoreCase("0")) {
      strDept = null;
    }
    if (strCompanyId.equalsIgnoreCase("0")) {
      strCompanyId = null;
    }

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_user_group", 7);
    gmDBManager.registerOutParameter(7, OracleTypes.CURSOR);
    gmDBManager.setString(1, strGroupName);
    gmDBManager.setString(2, strUser);
    gmDBManager.setString(3, strUserId);
    gmDBManager.setString(4, strGrpMappingID);
    gmDBManager.setString(5, strDept);
    gmDBManager.setString(6, strCompanyId);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(7));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * fetchAccessActionGroupDetails - This method will be used to fetch the access and action control
   * groups.
   * 
   * @return ArrayList - will be used by access control group report
   * @exception AppError
   */
  public ArrayList fetchAccessActionGroupDetails() throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_ac_action_group_list", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * fetchSecurityModuleList - This method will be used to fetch the Security group and module List.
   * 
   * @return ArrayList - will be used by Security group report and module report
   * @exception AppError
   */
  public ArrayList fetchSecurityModuleList(String strGroupType) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_security_module_list", 2);
    gmDBManager.setString(1, strGroupType);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  public ArrayList fetchSecGrpModuleMapList(String groupId) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_sc_module_map_list", 2);
    gmDBManager.setString(1, groupId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  public ArrayList fetchModuleSecGrpMapList(String groupId) throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_module_sc_map_list", 2);
    gmDBManager.setString(1, groupId);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  }

  /*
	 * 
	 */
  public void saveMultiUserGroup(HashMap hmParam) throws AppError {

    log.debug("hmParam : " + hmParam);

    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strInputString = GmCommonClass.parseNull((String) hmParam.get("HINPSTR"));
    String strID = GmCommonClass.parseNull((String) hmParam.get("PID"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("OPT"));
    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
    String strDefGrp = GmCommonClass.parseNull((String) hmParam.get("DEFAULTGRP"));

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_upd_user_group_map", 6);
    gmDBManager.setString(1, strInputString);
    gmDBManager.setString(2, strOpt);
    gmDBManager.setString(3, strID);
    gmDBManager.setString(4, strUserId);
    gmDBManager.setString(5, strCompanyId);
    gmDBManager.setString(6, strDefGrp);
    gmDBManager.execute();
    gmDBManager.commit();
  }

  /**
   * getUserAccessPermissions - This method will be used to get the access permissions.
   * 
   * @return HashMap - will be used by drop down tag
   * @exception AppError
   */
  public HashMap getUserAccessPermissions(String strPartyId, String strFunctionId, String GroupName)
      throws AppError {
    HashMap hmReturn = new HashMap();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_user_accesspermission", 4);

    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.setString(1, strPartyId);
    gmDBManager.setString(2, strFunctionId);
    gmDBManager.setString(3, GroupName);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    return hmReturn;
  }

  /**
   * fetchgroupUser This method will fetch the Issued By List
   * 
   * @param No Param
   * @return ArrayList
   * @exception AppError
   */
  public ArrayList fetchgroupUser(String strgroupId) throws AppError {
    ArrayList alReport = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    StringBuffer sbQuery = new StringBuffer();
    sbQuery
        .append(" SELECT   t1501.c1501_group_mapping_id grpmappingid, Decode(inactiveuser,'Y','InActive -' || pname,pname) rusernm,t1501.c101_mapped_party_id usernm");
    sbQuery.append(",t1500.c1500_group_nm rgroupname,T1501.C1500_GROUP_ID GROUPNAME");
    sbQuery
        .append(" FROM (SELECT t101p.c101_party_id, t101p.c1500_group_id,t101p.c101_first_nm|| ' '|| t101p.c101_last_nm pname, DECODE(t101u.c901_user_status,312,'Y',314,'Y',317,'Y','N') inactiveuser FROM t101_party t101p, t101_user t101u where t101p.c101_party_id = t101u.c101_party_id AND t101p.c101_void_fl is null)");
    sbQuery.append(" t101p,t1501_group_mapping t1501,t1500_group t1500");
    sbQuery
        .append(" WHERE t101p.c101_party_id = t1501.c101_mapped_party_id(+) AND T1500.C1500_GROUP_ID = T1501.C1500_GROUP_ID");
    sbQuery
        .append(" AND T1501.C1500_GROUP_ID = NVL ('"
            + strgroupId
            + "', T1501.C1500_GROUP_ID) AND T1500.C1500_VOID_FL IS NULL AND T1500.C901_GROUP_TYPE = 92264 ORDER BY rusernm");


    alReport = gmDBManager.queryMultipleRecords(sbQuery.toString());

    gmDBManager.close();
    return alReport;
  }

  /**
   * getDefaultGroup This method is used to get the default group in user to security group Mapping
   * screen.
   * 
   * @param String
   * @return Sting
   * @exception AppError
   */

  public String getDefaultGroup(String strUsesrId) throws AppError {

    String strDefaultGroup = "";

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setFunctionString("gm_pkg_it_user.get_Default_group", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.VARCHAR);
    gmDBManager.setString(2, strUsesrId);

    gmDBManager.execute();
    strDefaultGroup = GmCommonClass.parseNull(gmDBManager.getString(1));
    gmDBManager.close();

    return strDefaultGroup;
  }
  
	/**
	 * This method is used to get the mobile security events details
	 * 
	 * @param partyId
	 * @param pageName
	 * @return
	 */
	public List<GmSecurityEventVO> getSecurityEvents(String partyId,  String pageName) {
		
		ArrayList<HashMap<String, String>> alReturn = new ArrayList<HashMap<String, String>>();
		List<GmSecurityEventVO> listGmSecurityEvent = new ArrayList<GmSecurityEventVO>();
		GmSecurityEventVO gmSecurityEventVO = new GmSecurityEventVO();
		GmWSUtil gmWSUtil = new GmWSUtil();
		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_security_events", 3);
		gmDBManager.setString(1, GmCommonClass.parseNull(partyId));
		gmDBManager.setString(2, GmCommonClass.parseNull(pageName));
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.execute();		
		alReturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
	            .getObject(3)));
		gmDBManager.close();

		listGmSecurityEvent = gmWSUtil.getVOListFromHashMapList(alReturn,
				gmSecurityEventVO, gmSecurityEventVO.getMappingProperties());

		return listGmSecurityEvent;
	}
	
	 /**fetchLogDetails:This Method is used to get audit log details
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */
	public ArrayList fetchLogDetails(HashMap hmParam) throws AppError {
		
		    ArrayList alReturn = new ArrayList();
		    String strGrpMappingID = GmCommonClass.parseNull((String) hmParam.get("SECGRP"));
		    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERLIST"));
		    String strCompanyId = GmCommonClass.parseNull((String) hmParam.get("COMPANYID"));
		    if (strGrpMappingID.equalsIgnoreCase("")) {
		      strGrpMappingID = null;
		    }
	
		    if (strUserId.equalsIgnoreCase("")) {
		      strUserId = null;
		    }
		    if (strCompanyId.equalsIgnoreCase("0")) {
		      strCompanyId = null;
		    }
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    gmDBManager.setPrepareString("gm_pkg_cm_accesscontrol.gm_fch_log_details", 4);
		    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		    gmDBManager.setString(1, strGrpMappingID);
		    gmDBManager.setString(2, strUserId);
		    gmDBManager.setString(3, strCompanyId);
		    gmDBManager.execute();
		    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		    gmDBManager.close();
		    return alReturn;	 
	 }
	 
	 }

