/*
 * Module: Batch Name: GmBatchBean.java Author:
 */

package com.globus.common.beans;


import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.print.attribute.standard.MediaName;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmPrintService;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmURLConnection;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmBatchBean extends GmBean {
  GmCommonClass gmCommon = new GmCommonClass();
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

  // this will be removed all place changed with Data Store VO constructor
  public GmBatchBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmBatchBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  // Initialize
  // the
  // Logger
  // Class.
  /**
   * fetchBatchHeader used to fetch all the header details
   * 
   * @author
   * @param hash map
   * @return Hashmap
   */

  public HashMap fetchBatchHeader(HashMap hmParam) throws AppError {
    HashMap hmReturn = new HashMap();
    String strBatchID = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_cm_batch.gm_fch_batch_header", 2);
    gmDBManager.setString(1, strBatchID);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return hmReturn;
  }

  /**
   * getXmlGridData xml content generation for Batch grid
   * 
   * @author
   * @param array list and hash map
   * @return String
   */
  public String getXmlGridData(ArrayList alResult, HashMap hmParam) throws AppError {

    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strTempletName = "";
    String strTempletPath = "";
    String strSessCompanyLocale = "";
    String strRBPath = "";

    strTempletName = GmCommonClass.parseNull((String) hmParam.get("TEMPNAME"));
    strTempletPath = GmCommonClass.parseNull((String) hmParam.get("TEMPPATH"));
    strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("COMPANYLOCALE"));
    strRBPath = GmCommonClass.parseNull((String) hmParam.get("RBPATH"));

    templateUtil.setDataList("alResult", alResult);
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strRBPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir(strTempletPath);
    templateUtil.setTemplateName(strTempletName);

    return templateUtil.generateOutput();
  }

  /**
   * fetchBatchReport used to fetch all the header details
   * 
   * @author
   * @param hash map
   * @return array List
   */
  public ArrayList fetchBatchReport(HashMap hmParam) throws AppError, Exception {
    ArrayList alReturn = new ArrayList();
    String strBatchID = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strBatchType = GmCommonClass.parseZero((String) hmParam.get("BATCHTYPE"));
    String strBatchStatus = GmCommonClass.parseZero((String) hmParam.get("BATCHSTATUS"));
    String strBatchFromDt = GmCommonClass.parseNull((String) hmParam.get("BATCHFROMDT"));
    String strBatchToDt = GmCommonClass.parseNull((String) hmParam.get("BATCHTODT"));
    String strDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    Date dtFromDt = GmCommonClass.getStringToDate(strBatchFromDt, strDateFmt);
    Date dtToDt = GmCommonClass.getStringToDate(strBatchToDt, strDateFmt);

    log.debug(" dtFromDt " + dtFromDt + " dtToDt  " + dtToDt);

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

    gmDBManager.setPrepareString("gm_pkg_cm_batch.gm_fch_batch_rpt", 6);
    gmDBManager.setString(1, strBatchID);
    gmDBManager.setString(2, strBatchType);
    gmDBManager.setString(3, strBatchStatus);
    gmDBManager.setDate(4, dtFromDt);
    gmDBManager.setDate(5, dtToDt);

    gmDBManager.registerOutParameter(6, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(6));
    gmDBManager.close();
    log.debug("alResult.size() -->" + alReturn.size());

    return alReturn;
  }

  /**
   * saveBatch used create a new batch
   * 
   * @author
   * @param hash map
   * @return String
   */
  public String saveBatch(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strBatchType = GmCommonClass.parseNull((String) hmParam.get("BATCHTYPE"));
    String strBatchId = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strInvCloseDt = GmCommonClass.parseNull((String) hmParam.get("INVOICECLOSINGDATE"));
    Date dtIssueFrom = null;
    dtIssueFrom = (java.util.Date) hmParam.get("FROMDATE");
    Date dtIssueTo = null;
    dtIssueTo = (java.util.Date) hmParam.get("TODATE");
    String strBatchStatus = GmCommonClass.parseNull((String) hmParam.get("BATCHSTATUS"));
    // set the default batch status (18741) "In Progress"
    strBatchStatus = strBatchStatus.equals("") ? "18741" : strBatchStatus;

    gmDBManager.setPrepareString("gm_pkg_cm_batch.gm_sav_batch", 7);
    gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
    gmDBManager.setString(1, strBatchType);
    gmDBManager.setString(2, strBatchStatus);
    gmDBManager.setString(3, strUserId);
    gmDBManager.setString(4, strInvCloseDt);
    gmDBManager.setDate(5, dtIssueFrom);
    gmDBManager.setDate(6, dtIssueTo);
    gmDBManager.setString(7, strBatchId);
    gmDBManager.execute();
    strBatchId = GmCommonClass.parseNull(gmDBManager.getString(7));
    gmDBManager.commit();
    return strBatchId;

  }

  /**
   * saveBatchDetails used create details record in T9601_batch details table *
   * 
   * @author : matt B.
   * @param hash map
   * @return String
   */
  public void saveBatchDetails(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBatchId = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strRefid = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strBatchType = GmCommonClass.parseNull((String) hmParam.get("BATCHTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    gmDBManager.setPrepareString("gm_pkg_cm_batch.gm_sav_batch_details", 4);
    gmDBManager.setString(1, strBatchId);
    gmDBManager.setString(2, strRefid);
    gmDBManager.setString(3, strBatchType);
    gmDBManager.setString(4, strUserId);
    gmDBManager.execute();

    gmDBManager.commit();
  }

  /**
   * fetchInvBatchDet used fetch details
   * 
   * @author
   * @param hash map
   * @return array List
   */
  public ArrayList fetchInvBatchDet(HashMap hmParm) throws AppError {
    ArrayList alResult = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strInputStr = GmCommonClass.parseNull((String) hmParm.get("INPUTSTRING"));
    gmDBManager.setPrepareString("gm_pkg_ac_ar_batch_rpt.gm_fch_inv_batch_det", 2);
    gmDBManager.setString(1, strInputStr);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();
    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    log.debug("alResult.size() -->" + alResult.size());

    return alResult;
  }

  /**
   * saveBatchStatus used to update batch status to 'processed' 18744
   * 
   * @author
   * @param hashmap
   * @return String
   */
  public void saveBatchStatus(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBatchId = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strBatchStatus = "18744"; // Processed
    gmDBManager.setPrepareString("gm_pkg_cm_batch.gm_sav_batch_status", 3);
    gmDBManager.setString(1, strBatchStatus);
    gmDBManager.setString(2, strBatchId);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * saveBatchStatus used to update batch status to 'processed' 18744
   * 
   * @author
   * @param hashmap
   * @return String
   */
  public void saveBatchLog(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBatchId = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strInvId = GmCommonClass.parseNull((String) hmParam.get("INVID"));
    String strActionType = GmCommonClass.parseNull((String) hmParam.get("ACTIONTYPE"));
    String strRefType = GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    gmDBManager.setPrepareString("gm_pkg_cm_batch.gm_sav_batch_log", 5);
    gmDBManager.setString(1, strBatchId);
    gmDBManager.setString(2, strInvId);
    gmDBManager.setString(3, strActionType);
    gmDBManager.setString(4, strRefType);
    gmDBManager.setString(5, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

  /**
   * fetchBatchDetailsReport used to fetch batch details information
   * 
   * @author
   * @param hash map
   * @return array List
   */
  public ArrayList fetchBatchDetailsReport(HashMap hmParam) throws AppError, Exception {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBatchID = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strBatchType = GmCommonClass.parseNull((String) hmParam.get("BATCHTYPE"));
    String strBatchStatus = GmCommonClass.parseNull((String) hmParam.get("BATCHSTATUS"));

    gmDBManager.setPrepareString("gm_pkg_cm_batch.gm_fch_batch_dtls", 4);
    gmDBManager.setString(1, strBatchID);
    gmDBManager.setString(2, strBatchType);
    gmDBManager.setString(3, strBatchStatus);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    log.debug(" alReturn size --> " + alReturn.size());
    return alReturn;
  }

  /**
   * fetchOUSReplenishCN used to fetch the OUS consignment details that created on yesterday.
   * 
   * @return
   * @throws AppError
   * @throws Exception
   */
  public ArrayList fetchOUSReplenishCN() throws AppError, Exception {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_op_consignment.gm_fch_ous_replenish_cn", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alReturn;
  }

  /**
   * saveCNPickSlipPrint used to save batch details
   * 
   * @return
   * @param hmParam
   * @throws AppError
   */
  public String saveCNPickSlipPrint(HashMap hmParam) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBatchDetails = GmCommonClass.parseNull((String) hmParam.get("INPUTSTRING"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    gmDBManager.setPrepareString("gm_pkg_cm_batch.gm_sav_ous_replenish_batch", 3);
    gmDBManager.setString(1, strBatchDetails);
    gmDBManager.setString(2, strUserId);
    gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
    gmDBManager.execute();
    String strBatchID = GmCommonClass.parseNull(gmDBManager.getString(3));
    gmDBManager.commit();
    return strBatchID;
  }

  /**
   * fetchCNBatchDetailsReport used to fetch consign batch details information
   * 
   * @author
   * @param hash map
   * @return array List
   */
  public ArrayList fetchCNBatchDetailsReport(HashMap hmParam) throws AppError, Exception {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strBatchID = GmCommonClass.parseNull((String) hmParam.get("BATCHID"));
    String strBatchType = GmCommonClass.parseNull((String) hmParam.get("BATCHTYPE"));
    String strBatchStatus = GmCommonClass.parseNull((String) hmParam.get("BATCHSTATUS"));

    gmDBManager.setPrepareString("gm_pkg_cm_batch.gm_fch_cn_batch_dtls", 4);
    gmDBManager.setString(1, strBatchID);
    gmDBManager.setString(2, strBatchType);
    gmDBManager.setString(3, strBatchStatus);
    gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
    gmDBManager.close();
    log.debug(" fetchCNBatchDetailsReport alReturn size --> " + alReturn.size());
    return alReturn;
  }

  /**
   * saveCNPickSlipasPDF to save CN PickSlip to PDF
   * 
   * @author
   * @param HashMap
   * @return
   * @throws Exception
   */
  public boolean saveCNPickSlipasPDF(HashMap hmParam) throws Exception {
    String strRefID = GmCommonClass.parseNull((String) hmParam.get("REFID"));
    String strUser = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String resourcename = GmCommonClass.parseNull((String) hmParam.get("RESOURCE"));
    String strPara = GmCommonClass.parseNull((String) hmParam.get("PARAMETER"));

    GmURLConnection gmURLConnection = new GmURLConnection(getGmDataStoreVO());
    gmURLConnection.urlconnection(resourcename, strPara);
    return true;
  }

  /**
   * batchPrint to print the pdf file's
   * 
   * @author
   * @param HashMap
   * @throws Exception
   */
  public void batchPrint(HashMap hmParam) throws Exception {

    String pdffileName = GmCommonClass.parseNull((String) hmParam.get("PDFFILENAME"));
    String strRuleId = GmCommonClass.parseNull((String) hmParam.get("RULEID"));
    GmPrintService gmPrintService = new GmPrintService();
    gmPrintService.setNumofCopies(1);
    gmPrintService.setPrinterName(GmCommonClass.getRuleValue(strRuleId, "PRINTER"));
    log.debug("Printer name = " + GmCommonClass.getRuleValue(strRuleId, "PRINTER"));
    gmPrintService.setFileName(pdffileName);
    gmPrintService.setPaperSize(MediaName.ISO_A4_WHITE);
    gmPrintService.setFontName("Verdana");
    gmPrintService.setFontSize(10);
    gmPrintService.setImgArea_x_offset(-9);
    gmPrintService.setImgArea_y_offset(-3);
    gmPrintService.setImgArea_width_offset(-10);
    gmPrintService.setImgArea_height_offset(-12);
    gmPrintService.setGraphics2D_x(20);
    gmPrintService.setGraphics2D_y(10);
    gmPrintService.print();
  }
  

  /**
   * updatePOStatus used to update batch status to 'invoiced' 109547 and 'invoice failed' 109546
   * 
   * @author
   * @param strBatchID , strErrorPOs , strUserId
   * @return String
   */
  public void updatePOStatus(String strBatchID,String strErrorPOs,String strUserId) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_ar_process_batch.gm_upd_batch_po_status", 3);
    gmDBManager.setString(1, strBatchID);
    gmDBManager.setString(2, strErrorPOs);
    gmDBManager.setString(3, strUserId);
    gmDBManager.execute();
    gmDBManager.commit();

  }

}// end of class GmBatchBean
