package com.globus.common.beans;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.common.db.GmDBManager.GmDNSNamesEnum;
import com.globus.sales.beans.GmFooterCalcuationBean;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmCrossTabReport extends GmBean {

  // this will be removed all place changed with Data Store VO constructor
  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */

  public GmCrossTabReport() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public GmCrossTabReport(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  boolean blSetColumnOverrideFl = false;
  int intColumnOverride = 6; // defaults to 6th column

  boolean blSetRowGroupFl = false;
  int intRowGroup = 5; // defaults to 6th column

  boolean blSkipExtraColumn = false;
  boolean blAcceptExtraColumn = false;
  boolean blSetFixedColumn = false;

  HashMap hmExtraColumnParams = new HashMap();
  HashMap hmFixedColumnParams = new HashMap();

  boolean blSetRowOverrideFl = false;
  int intRowOverride = 8; // defaults to 6th column


  // Code to enable extra columns. if we need to set them..
  public void setAcceptExtraColumn(boolean blValue) {
    blAcceptExtraColumn = blValue;
  }

  // Code to enable extra columns. if we need to set them..
  public void setFixedColumn(boolean blValue) {
    blSetFixedColumn = blValue;
  }

  // Code to enable extra column values. Key is the column # and value is what gets appended to the
  // header name
  public void setExtraColumnParams(int intColumnNumber, String strColumnValue) {
    hmExtraColumnParams.put(String.valueOf(intColumnNumber), strColumnValue);
  }

  // Code to enable extra column values. Key is the column # and value is what gets appended to the
  // header name
  public void setFixedParams(int intColumnNumber, String strColumnValue) {
    hmFixedColumnParams.put(String.valueOf(intColumnNumber), strColumnValue);
  }

  // Code to enable override column
  public void setGroupFl(boolean GroupFl) {
    blSetRowGroupFl = GroupFl;
  }

  // Code to get the Override column
  public void setGroupColumn(int GroupColumn) {
    intRowGroup = GroupColumn;
  }

  // Code to enable override column
  public void setOverRideFl(boolean OverRideFl) {
    blSetColumnOverrideFl = OverRideFl;
  }

  // Code to get the Override column
  public void setOverrideColumn(int OverrideColumn) {
    intColumnOverride = OverrideColumn;
  }

  // Code for link value
  public void setSkipExtraColumn(boolean blValue) {
    blSkipExtraColumn = blValue;
  }

  // Code to enable override row
  public void setRowOverRideFl(boolean OverRowRideFl) {
    blSetRowOverrideFl = OverRowRideFl;
  }

  // Code to get the Override row
  public void setOverrideRow(int OverrideRow) {
    intRowOverride = OverrideRow;
  }

  /**
   * reportYTDByDist - This Method is used to fetch the value and arrange the same in the Crosstab
   * format for the ease of use Detail SQL shoul have following information <br>
   * ID, NAME, R_DATE, AMOUNT<br>
   * fetches the report in below format <br>
   * Name Date Amount<br>
   * **********************************<br>
   * D101 Mar 05 5000 <br>
   * D101 Apr 05 1000 <br>
   * D102 Mar 05 2000 <br>
   * D102 Apr 05 3000 <br>
   * Rearranges the data in below format<br>
   * ************************************** <br>
   * Name Mar 05 Apr 05............<br>
   * D101 5000 1000 ............<br>
   * D102 2000 3000 ............<br>
   * 
   * @param String strHeaderQuery Header SQL Information
   * @param String strDetailsReport Footer SQL Information
   * @return HashMap Contains Formaterd SQL Header, Footer and Detail Section
   * @exception AppError
   */
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public HashMap GenerateCrossTabReport(String strHeaderQuery, String strDetailsReport)
      throws AppError {
    return GenerateCrossTabReport(strHeaderQuery, strDetailsReport, GmDNSNamesEnum.WEBGLOBUS);
  }

  public HashMap GenerateCrossTabReport(String strHeaderQuery, String strDetailsReport,
      GmDNSNamesEnum enDNSName_) throws AppError {
    // Database connection object
    Connection conn = null;
    Statement stmt = null;

    Statement headerStmt = null;
    ResultSet rsHeader = null;
    ResultSet rsDetails = null;
    DBConnectionWrapper dbCon = null;
    HashMap hmFinalValue = new HashMap();

    try {
      // stmt = conn.createStatement();

      // ***********************************************************
      // Following section will get the Crosstab Header Information
      // ***********************************************************

      dbCon = new DBConnectionWrapper(getGmDataStoreVO());
      conn = dbCon.getConnection(enDNSName_);

      stmt = conn.createStatement();
      headerStmt = conn.createStatement();
      rsHeader = headerStmt.executeQuery(strHeaderQuery);
      rsDetails = stmt.executeQuery(strDetailsReport);

      hmFinalValue = GenerateCrossTabReport(rsHeader, rsDetails);


    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    } finally {
      try {
        if (rsHeader != null) {
          rsHeader.close();
        }
        if (rsDetails != null) {
          rsDetails.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (headerStmt != null) {
          headerStmt.close();
        }

        if (conn != null) {
          conn.close();
        }
      } catch (Exception e) {
        throw new AppError(e);
      }
    }
    return hmFinalValue;
  }

  public HashMap GenerateCrossTabReport(ResultSet rsHeader, ResultSet rsDetails) throws AppError {
    // Container object to hold retrieved value
    ArrayList alHeader = new ArrayList(1);
    ArrayList alFinalResult = new ArrayList(1);
    ResultSetMetaData resultSetMetaData;

    HashMap hmapHeaderValue = new HashMap(); // Contains footer section information
    HashMap hmapFooterValue = new HashMap(); // Contains header section information
    HashMap hmapResult = new HashMap(); // Contains Column details
    HashMap hmapFinalValue = new HashMap(); // Final value report details

    // Variable to hold temp values
    int columnCount = 1;
    double TotalAmount = 0;
    double FooterTotalAmount = 0;

    String C_Name = "";
    String ColumnPostion = "";
    String strAmount = null;
    String strColumnOverRideValue = null;
    String strFooterAmount = null;
    String strGroupingId = "0";
    String strRowOverRideId = "0";
    String strHashKey = "";
    List alHashKeyFixedColumn = new ArrayList();
    List alHashKeyExtraColumn = new ArrayList();
    int intFixedColumnSize = 0;
    int intExtraColumnSize = 0;

    try {
      // stmt = conn.createStatement();

      // ***********************************************************
      // Following section will get the Crosstab Header Information
      // ***********************************************************
      // Initial Value
      alHeader.add("Name");
      hmapFooterValue.put("Name", "Total");

      // To fetch header information
      while (rsHeader.next()) {
        columnCount++;
        alHeader.add(rsHeader.getString(1));
        hmapHeaderValue.put(rsHeader.getString(1), String.valueOf(columnCount));
        hmapFooterValue.put(rsHeader.getString(1), "0.00");
      }

      // Final Value
      alHeader.add("Total");
      hmapFooterValue.put("Total", "0.00");

      // ***********************************************************
      // Following Section will arrange the crosstab details section
      // ***********************************************************
      int size = (int) (columnCount * 1.33);
      resultSetMetaData = rsDetails.getMetaData();

      if (blSetFixedColumn) {
        alHashKeyFixedColumn = Arrays.asList(hmFixedColumnParams.keySet().toArray());
        intFixedColumnSize = alHashKeyFixedColumn.size();
      }

      if (blAcceptExtraColumn) {
        alHashKeyExtraColumn = Arrays.asList(hmExtraColumnParams.keySet().toArray());
        intExtraColumnSize = alHashKeyExtraColumn.size();
      }

      while (rsDetails.next()) {
        // Below code will get the Column header
        if (!C_Name.equals(rsDetails.getString(2))) {
          // If its not for the first time then create the hashmap
          if (!C_Name.equals("")) {
            hmapResult.put("Total", String.valueOf(TotalAmount));
            alFinalResult.add(hmapResult);

            // To calculate the row total
            strFooterAmount = (String) hmapFooterValue.get("Total");
            FooterTotalAmount = Double.parseDouble(strFooterAmount) + TotalAmount;
            // System.out.println(FooterTotalAmount + " - " + TotalAmount + " - " +strFooterAmount);
            hmapFooterValue.put("Total", String.valueOf(FooterTotalAmount));
          }

          // Clearing & Reassigning the new value
          TotalAmount = 0;
          hmapResult = new HashMap(size);
          C_Name = rsDetails.getString(2);

          hmapResult.put("ID", rsDetails.getString(1)); // Holds Record ID
          hmapResult.put("Name", C_Name);

          if (blSetFixedColumn) {
            for (int i = 0; i < intFixedColumnSize; i++) {
              strHashKey = (String) alHashKeyFixedColumn.get(i);
              hmapResult.put(hmFixedColumnParams.get(strHashKey),
                  rsDetails.getString(Integer.parseInt(strHashKey)));
            }
          }
          // Below code to fetch the Group information
          // For multiple group
          if (blSetRowGroupFl) {
            strGroupingId = rsDetails.getString(intRowGroup);
            hmapResult.put("GROUPINGCOUNT", strGroupingId);
          }

          // Below code to fetch row override information if any
          if (blSetRowOverrideFl) {
            strRowOverRideId = rsDetails.getString(intRowOverride);
            hmapResult.put("ROWOVERRIDEFL", strRowOverRideId);
          }

        }



        ColumnPostion = String.valueOf(hmapHeaderValue.get(rsDetails.getString(3)));
        strAmount = rsDetails.getString(4);
        // Below code will get the Appropriate row information
        /*
         * if(ColumnPostion != null && !ColumnPostion.equals("") && !ColumnPostion.equals("null")) {
         * FindPostion = Integer.parseInt(ColumnPostion); }
         */

        if (blSetColumnOverrideFl) {
          hmapResult.put(rsDetails.getString(3) + "CRD", rsDetails.getString(intColumnOverride));
        }

        if (blAcceptExtraColumn) {
          for (int i = 0; i < intExtraColumnSize; i++) {
            strHashKey = (String) alHashKeyExtraColumn.get(i);
            hmapResult.put(rsDetails.getString(3) + hmExtraColumnParams.get(strHashKey),
                rsDetails.getString(Integer.parseInt(strHashKey)));
          }
        } else {
          // To Fetch Column Value to be displayed in cross tab
          hmapResult.put(rsDetails.getString(3), strAmount);
        }


        // if(!blSetRowGroupFl) {
        // Code to calculate row total
        FooterTotalAmount = 0;
        strAmount = GmCommonClass.parseZero(strAmount);
        try {
          strFooterAmount = "";
          strFooterAmount =
              GmCommonClass.parseZero((String) hmapFooterValue.get(rsDetails.getString(3)));

          TotalAmount = TotalAmount + Double.parseDouble(strAmount);

          if (strFooterAmount != null && !strFooterAmount.equals("")
              && !strFooterAmount.equals("null")) {
            FooterTotalAmount = Double.parseDouble(strFooterAmount) + Double.parseDouble(strAmount);
          }
        } catch (NumberFormatException exp) {
          FooterTotalAmount = Double.parseDouble(strFooterAmount);
        }
        // }
        hmapFooterValue.put(rsDetails.getString(3), String.valueOf(FooterTotalAmount));

      };// End of Crosstab detail section

      // Below if condition to add the final total info
      if (columnCount > 1) {
        try {
          // To add the final Value into the arraylist
          hmapResult.put("Total", String.valueOf(TotalAmount));
          if (hmapResult.get("ID") != null) {
            alFinalResult.add(hmapResult);
          }

          // To calculate the row total
          strFooterAmount = (String) hmapFooterValue.get("Total");
          log.debug("strFooterAmount" + strFooterAmount);
          if (strFooterAmount != null && !strFooterAmount.equals("")
              && !strFooterAmount.equals("null")) {
            FooterTotalAmount = Double.parseDouble(strFooterAmount) + TotalAmount;
          }
        } catch (NumberFormatException exp) {
          // skip the calculation
        }

        hmapFooterValue.put("Total", String.valueOf(FooterTotalAmount));
      }



      // Final Value to be passed to the report
      hmapFinalValue.put("Header", alHeader);
      hmapFinalValue.put("Details", alFinalResult);
      hmapFinalValue.put("Footer", hmapFooterValue);
      // log.debug(" Cross Tab Header " +alHeader);
      // log.debug(" Cross Tab Details " +alFinalResult);
      // log.debug(" Cross Tab Footer values are " +hmapFooterValue);

    } catch (Exception e) {
      e.printStackTrace();
      // terminate(rsHeader);
      terminate(rsDetails);
      throw new AppError(e);
    } finally {
      try {
        if (rsHeader != null) {
          rsHeader.close();
        }
        if (rsDetails != null) {
          rsDetails.close();
        }
      } catch (Exception exp) {
        exp.printStackTrace();
        throw new AppError(exp);
      }
    }
    return hmapFinalValue;
  }

  /*******************************************************************************************************************
   * reportYTDByDist - This Method is used to fetch the value and arrange the same in the Crosstab
   * format for the ease of use Detail SQL shoul have following information <br>
   * ID, NAME, R_DATE, AMOUNT<br>
   * fetches the report in below format <br>
   * 
   * @param String strHeaderQuery Header SQL Information
   * @param String strDetailsReport Footer SQL Information
   * @return HashMap Contains Formaterd SQL Header, Footer and Detail Section
   * @exception AppError
   ******************************************************************************************************************/
  public HashMap getFromToDate() throws AppError {

    HashMap hmResult = new HashMap();
    StringBuffer sbQuery = new StringBuffer();

    try {
      GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

      sbQuery.append("SELECT  TO_CHAR(CURRENT_DATE,'MM') TOMONTH");
      sbQuery.append(" ,TO_CHAR(CURRENT_DATE,'YYYY') TOYEAR");
      sbQuery.append(" ,TO_CHAR(ADD_MONTHS(trunc(CURRENT_DATE,'MONTH') , -11),'MM') FROMMONTH");
      sbQuery.append(" ,TO_CHAR(ADD_MONTHS(trunc(CURRENT_DATE,'MONTH') , -11),'YYYY') FROMYEAR");
      sbQuery.append(" FROM DUAL");

      // System.out.println(" getFromToDate" + sbQuery);
      hmResult = gmDBManager.querySingleRecord(sbQuery.toString());
      hmResult.put("FirstTime", "'YES");
    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesReportBean:getFromToDate", "Exception is:" + e);
      throw new AppError(e);
    }
    return hmResult;
  }

  /**
   * getCurrentYearRange - This Method is used to fetch current year information (Year Start and
   * Year End -- Jan to Dec)
   * 
   * @return HashMap Contains SQL Dates Header, Footer and Detail Section
   * @exception AppError
   */
  public HashMap getCurrentYearRange() throws AppError {
    DBConnectionWrapper dbCon = null;
    HashMap hmResult = new HashMap();
    StringBuffer sbQuery = new StringBuffer();

    try {
      dbCon = new DBConnectionWrapper(getGmDataStoreVO());;

      sbQuery.append("SELECT  '12' TOMONTH");
      sbQuery.append(" ,TO_CHAR(CURRENT_DATE,'YYYY') TOYEAR");
      sbQuery.append(" ,'01' FROMMONTH");
      sbQuery.append(" ,TO_CHAR(CURRENT_DATE,'YYYY')  FROMYEAR");
      sbQuery.append(" FROM DUAL");

      // System.out.println(" getFromToDate" + sbQuery);
      hmResult = dbCon.querySingleRecord(sbQuery.toString());
      hmResult.put("FirstTime", "'YES");
    } catch (Exception e) {
      GmLogError.log("Exception in GmSalesReportBean:getFromToDate", "Exception is:" + e);
      throw new AppError(e);
    }
    return hmResult;
  }

  /**
   * reportYTDByDist - This Method is used to fetch the value and arrange the same in the Crosstab
   * format for the ease of use Can pass Column and row information to format in different approach
   * can have multi level formating and multi level cross tab information fetches the report in
   * below format <br>
   * * Name Date Amount<br>
   * **********************************<br>
   * D101 Mar 05 5000 <br>
   * D101 Apr 05 1000 <br>
   * D102 Mar 05 2000 <br>
   * D102 Apr 05 3000 <br>
   * Rearranges the data in below format<br>
   * ************************************** <br>
   * Name Mar 05 Apr 05............<br>
   * D101 5000 1000 ............<br>
   * D102 2000 3000 ............<br>
   * 
   * @param String strHeaderQuery Header SQL Information
   * @param String strDetailsReport Footer SQL Information
   * @param HashMap hpGroupDefinition holds following information Row Name information Column Name
   *        Information Detail Column details
   * @return HashMap Contains Formaterd SQL Header, Footer and Detail Section
   * @exception AppError
   */
  public HashMap GenerateCrossTabReport(String strHeaderQuery, String strDetailsReport,
      HashMap hpGPDefinition) throws AppError {
    // Database connection object
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;
    ResultSetMetaData meta;
    DBConnectionWrapper dbCon = null;

    // Container object to hold retrieved value
    ArrayList alDetailGroupList = new ArrayList();

    ArrayList alHeader = new ArrayList(1);
    ArrayList alFinalResult = new ArrayList(1);

    ArrayList alRow = new ArrayList(1);
    String strRL1Name = "";
    int intRL1Location = 0;

    ArrayList alColumn = new ArrayList(1);
    String strCL1Name = "";
    int intCL1Location = 0;

    ArrayList alDetailField = new ArrayList();
    String strDetailField = "";
    int intDetailLocation = 0;

    HashMap hmapHeaderValue = new HashMap(); // Contains footer section information
    HashMap hmapFooterValue = new HashMap(); // Contains header section information
    HashMap hmapResult; // Contains Column details
    HashMap hmapFinalValue = new HashMap(); // Final value report details

    // Variable to hold temp values
    int columnCount = 1;
    int returnValue = 0;
    int FindPostion = 0;

    int RowCount = 0;
    int RowHeaderCount = 0;

    double TotalAmount = 0;
    double FooterTotalAmount = 0;

    String C_Name = "";
    String ColumnPostion = "";
    String strAmount = null;
    String strFooterAmount = null;
    String strCurrentVal = "";
    String strPreviousVal = "";
    String strValue = "";

    try {

      // ***********************************************************
      // Following section will get the Crosstab Header Information
      // ***********************************************************
      dbCon = new DBConnectionWrapper(getGmDataStoreVO());

      conn = dbCon.getConnection();
      // conn = dbCon.getOracleStaticConnection();
      stmt = conn.createStatement();

      rs = stmt.executeQuery(strHeaderQuery);

      // Initial Value get the header information
      // Below code to store the header information
      hmapFooterValue.put("Name", "Total");

      while (rs.next()) {
        columnCount++;
        alHeader.add(rs.getString(1));
        hmapHeaderValue.put(rs.getString(1), String.valueOf(columnCount));
        hmapFooterValue.put(rs.getString(1), "0.00");
      }
      RowHeaderCount = alHeader.size() - 1;
      // Final Value
      // alHeader.add("Total");
      hmapFooterValue.put("Total", "0.00");

      // ***********************************************************
      // Following Section will arrange the crosstab based on detail value
      // ***********************************************************
      rs = stmt.executeQuery(strDetailsReport);
      meta = rs.getMetaData();
      int count = meta.getColumnCount();
      int size = (int) (columnCount * 1.33);

      hmapResult = null;
      // Getting default information
      alRow = (ArrayList) hpGPDefinition.get("ROW");
      if (alRow.size() != 0) {
        hmapResult = (HashMap) alRow.get(0);
        strRL1Name = (String) hmapResult.get("KEY");
        intRL1Location = getColumnLocation(strRL1Name, meta);
      }

      alColumn = (ArrayList) hpGPDefinition.get("COLUMN");
      if (alColumn.size() != 0) {
        strCL1Name = (String) alColumn.get(0);
        intCL1Location = getColumnLocation(strCL1Name, meta);
      }

      alDetailField = (ArrayList) hpGPDefinition.get("DETAILFIELD");
      if (alDetailField.size() != 0) {
        strDetailField = (String) alDetailField.get(0);
        intDetailLocation = getColumnLocation(strDetailField, meta);
      }

      hmapResult = null;
      while (rs.next()) {
        strCurrentVal = rs.getString(intRL1Location);
        if (!strCurrentVal.equals(strPreviousVal)) {
          // If record available add the same to the loop
          if (hmapResult != null) {
            alDetailGroupList.add(hmapResult);
          }
          hmapResult = null;
          strPreviousVal = strCurrentVal;

          // Loops thru Meta data value
          hmapResult = new HashMap(size);
          hmapResult.put("Hierarchy", "0"); // Defaults to 1
          for (int i = 1; i <= count; i++) {
            strValue = rs.getString(i);
            if (strValue == null) {
              strValue = "";
            }

            // Skip default value record
            if (i != intCL1Location && intDetailLocation != intCL1Location) {
              hmapResult.put(meta.getColumnName(i), strValue.trim());
            }

          }

          // Below code to add footer row total
          strFooterAmount = (String) hmapFooterValue.get("Total");
          FooterTotalAmount = Double.parseDouble(strFooterAmount) + TotalAmount;
          hmapFooterValue.put("Total", String.valueOf(FooterTotalAmount));

          // Clearing & Reassigning the new value
          TotalAmount = 0;
        }

        strAmount = rs.getString(intDetailLocation);
        hmapResult.put(rs.getString(intCL1Location), strAmount);
        TotalAmount = TotalAmount + Double.parseDouble(strAmount);

        // Below code to fetch final grand total to be validated
        FooterTotalAmount = 0;
        strFooterAmount = "";
        strFooterAmount =
            (hmapFooterValue.get(rs.getString(intCL1Location))) == null ? "0"
                : (String) hmapFooterValue.get(rs.getString(intCL1Location));
        FooterTotalAmount =
            Double.parseDouble(strFooterAmount)
                + Double.parseDouble(rs.getString(intDetailLocation));
        hmapFooterValue.put(rs.getString(intCL1Location), String.valueOf(FooterTotalAmount));

      };// End of Crosstab detail section

      // To get the final value loop thru value
      if (hmapResult != null) {
        alDetailGroupList.add(hmapResult);

        // Below code to add footer row total
        strFooterAmount = (String) hmapFooterValue.get("Total");
        FooterTotalAmount = Double.parseDouble(strFooterAmount) + TotalAmount;
        hmapFooterValue.put("Total", String.valueOf(FooterTotalAmount));

        // Clearing & Reassigning the new value
        TotalAmount = 0;
      }

      // Below to get generate row group information
      alDetailGroupList = GenerateRowGrouping(alDetailGroupList, alHeader, alRow);

      // Below code to test the row total
      alDetailGroupList = GenerateColumnTotal(alDetailGroupList, alHeader);

      // Code to group header
      alHeader = GenerateHeader(alHeader, alRow);

      // Final Value to be passed to the report
      hmapFinalValue.put("Header", alHeader);
      hmapFinalValue.put("Details", alDetailGroupList);
      hmapFinalValue.put("RowDetails", alRow);
      hmapFinalValue.put("Footer", hmapFooterValue);
      // System.out.println("Completed all the data connection");
    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    } finally {
      try {
        if (rs != null) {
          rs.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (Exception e) {
        throw new AppError(e);
      }
    }
    return hmapFinalValue;
  }

  // Below code is used to get column location information
  // for the passed column name
  private int getColumnLocation(String strColumnName, ResultSetMetaData meta) throws AppError {
    try {
      int count = meta.getColumnCount();
      for (int i = 1; i <= count; i++) {
        if (strColumnName.equals(meta.getColumnName(i))) {
          return i;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new AppError(e);
    }

    return 0;
  }

  // Below code is used to get row group total
  // System generate the row group total for the selected group
  private ArrayList GenerateRowGrouping(ArrayList alDetailGroupList, ArrayList alHeader,
      ArrayList alRow) {
    HashMap hmapResult;
    HashMap hmapTotal = new HashMap();
    HashMap hmapValue = new HashMap();

    ArrayList alNewGroupList = new ArrayList();

    double TotalAmount = 0;
    String strValue;
    String strTotalVal;

    String strPreviousValue = "";
    String strCurrentValue;

    String strRowKey;
    String strRowID;
    boolean bFirstTimeFlag = false;
    int GroupLocation = 0;
    String strHeaderValue = "";
    String StrHierarchy = "";

    int RowGroup = alRow.size() - 1;
    int RowCount = alDetailGroupList.size() - 1;
    int RowHeaderCount = alHeader.size() - 1;
    int intTempLocation = 0;
    int intCurrentCount = 0;

    // ************************************************
    // To generate row grouping and the total
    // ************************************************

    // Main Loop to fetch the row grouping information
    // for (int k = 1; k <= RowGroup ; k++)
    for (int k = RowGroup; k >= 1; k--) {
      hmapValue = (HashMap) alRow.get(k);
      strRowKey = (String) hmapValue.get("KEY");
      strRowID = (String) hmapValue.get("ID");

      for (int i = 0; i <= RowCount; i++) {
        hmapResult = (HashMap) alDetailGroupList.get(i);
        strCurrentValue = (String) hmapResult.get(strRowKey);
        StrHierarchy = (String) hmapResult.get("Hierarchy");

        if (!StrHierarchy.equals("0")) {
          continue;
        }
        if (!strPreviousValue.equals(strCurrentValue)) {

          // Code to add the calculate value
          intTempLocation = i;
          if (!strPreviousValue.equals("")) {
            hmapTotal.put("Hierarchy", k + ""); // Defaults to 1

            alNewGroupList.add(GroupLocation, hmapTotal);
            // PrintResultValue(alNewGroupList,alHeader);
            // Added basically to increase the row count for addition
            // row (dynamic adding) and infecting the same into current column
            intCurrentCount++;
          }
          GroupLocation = intTempLocation + intCurrentCount;
          hmapTotal = null;

          // To get the header information
          hmapTotal = GenerateRowHeader(hmapResult, alRow, k);

          // bFirstTimeFlag = true; To remove the unused value
          // System.out.println(strPreviousValue);
          strPreviousValue = strCurrentValue;

        }

        // **************************************
        // To get the column value information
        // **************************************
        for (int j = 0; j <= RowHeaderCount; j++) {
          strHeaderValue = (String) alHeader.get(j);
          strTotalVal = (String) hmapTotal.get(strHeaderValue);

          strValue = (String) hmapResult.get(strHeaderValue);

          strValue = GmCommonClass.parseZero(strValue);
          strTotalVal = GmCommonClass.parseZero(strTotalVal);

          TotalAmount = Double.parseDouble(strTotalVal) + Double.parseDouble(strValue);

          strValue = TotalAmount + "";
          hmapTotal.put(strHeaderValue, strValue);
        }

        // Load the default value
        if (k == RowGroup) {
          alNewGroupList.add(hmapResult);
        }
      }// Inner For loop ends here

      hmapTotal.put("Hierarchy", k + "");

      alNewGroupList.add(GroupLocation, hmapTotal);
      // PrintResultValue(alNewGroupList,alHeader);
      strPreviousValue = "";
      intCurrentCount = 0;
      alDetailGroupList = null;
      alDetailGroupList = (ArrayList) alNewGroupList.clone();

      RowCount = alDetailGroupList.size() - 1;

    }// Column header for loop ends here

    return alNewGroupList;
  }

  // Below code is used to get column location information
  // for the passed column name
  private ArrayList GenerateColumnTotal(ArrayList alDetailGroupList, ArrayList alHeader) {
    HashMap hmapResult;
    double TotalAmount = 0;
    String strValue;

    int RowCount = alDetailGroupList.size() - 1;
    int RowHeaderCount = alHeader.size() - 1;
    // ************************************************
    // To generate row total
    // ************************************************

    for (int i = 0; i <= RowCount; i++) {
      hmapResult = (HashMap) alDetailGroupList.get(i);
      for (int j = 0; j <= RowHeaderCount; j++) {
        strValue = (String) hmapResult.get(alHeader.get(j));
        strValue = GmCommonClass.parseZero(strValue);

        TotalAmount = TotalAmount + Double.parseDouble(strValue);
      }

      strValue = TotalAmount + "";
      hmapResult.put("Total", strValue);
      TotalAmount = 0;
      alDetailGroupList.set(i, hmapResult);
    }

    return alDetailGroupList;
  }

  // Below code is used to get column location information
  // for the passed column name
  private HashMap GenerateRowHeader(HashMap hmapCurrentList, ArrayList alRow, int intLevel) {
    int RowCount = alRow.size() - 1;
    HashMap hmapValue;
    HashMap hmapGroupHeader = new HashMap();

    String strRowKey;
    String strRowID;

    String strKeyValue;
    String strIDValue;

    for (int i = 0; i <= RowCount; i++) {
      hmapValue = (HashMap) alRow.get(i);
      strRowKey = (String) hmapValue.get("KEY");
      strRowID = (String) hmapValue.get("ID");

      strKeyValue = "";
      strIDValue = "";

      if (intLevel <= i) {
        strKeyValue = (String) hmapCurrentList.get(strRowKey);
        strIDValue = (String) hmapCurrentList.get(strRowID);;
      }

      hmapGroupHeader.put(strRowKey, strKeyValue);
      hmapGroupHeader.put(strRowID, strIDValue);

    }

    // System.out.println("********* Header Value -- " + hmapGroupHeader);

    return hmapGroupHeader;
  }

  // --------------------------------------------------------------------------------
  // Below to test the generated value and to me removed once the same is performed
  // --------------------------------------------------------------------------------
  private ArrayList GenerateHeader(ArrayList alHeader, ArrayList alRow) {
    HashMap hmapValue;
    int RowCount = alRow.size() - 1;
    int icount = 0;

    String strKeyValue;
    String strIDValue;

    // ---------------------------------------------
    // Below is the testing code to be removed
    // ----------------------------------------------
    for (int j = 0; j <= RowCount; j++) {
      hmapValue = (HashMap) alRow.get(j);
      strKeyValue = (String) hmapValue.get("KEY");
      strIDValue = (String) hmapValue.get("ID");

      alHeader.add(icount, strKeyValue);
      // icount = icount + 1;
      alHeader.add(icount, strIDValue);
      // icount = icount + 1;
    }
    alHeader.add(0, "Hierarchy");
    alHeader.add("Total");

    // System.out.println(alHeader);
    return alHeader;
  }

  // --------------------------------------------------------------------------------
  // Below to test the generated value and to me removed once the same is performed
  // --------------------------------------------------------------------------------
  private void PrintResultValue(ArrayList alDetailGroupList, ArrayList alHeader) {
    int intHeaderCount = alHeader.size() - 1;
    HashMap hmapValue;
    // ---------------------------------------------
    // Below is the testing code to be removed
    // ----------------------------------------------
    String strDetails = "";

    for (int j = 0; j <= intHeaderCount; j++) {
      strDetails = strDetails + (String) alHeader.get(j) + ",";
    }
    // System.out.println(strDetails);
    strDetails = "";

    for (int j = 0; j <= alDetailGroupList.size() - 1; j++) {
      hmapValue = (HashMap) alDetailGroupList.get(j);
      for (int k = 0; k <= intHeaderCount; k++) {

        strDetails = strDetails + hmapValue.get(alHeader.get(k)) + ",";
      }
      // System.out.println(strDetails);
      strDetails = "";
    }

  }

  /*******************************************************************************************************************
   * linkCrossTab - This code links the new data with cross tab report Passes following parameter
   * 
   * @return HashMap hmCrossTabValue -- Holds the Crosstab value ArrayList alSetLinkValue -- Holds
   *         the new dataelement list HashMap hmLinkDetails
   * @exception AppError
   ******************************************************************************************************************/
  public HashMap linkCrossTab(HashMap hmCrossTabValue, ArrayList alSetLinkValue,
      HashMap hmLinkDetails) {
    // crosstab hold value
    ArrayList alHeader = new ArrayList();
    ArrayList alResult = new ArrayList();
    HashMap hmFooter = new HashMap();

    ArrayList alLinkValue = new ArrayList();

    HashMap hmTempMain = new HashMap();
    HashMap hmTempDetail = new HashMap();
    HashMap hmTempLink = new HashMap();
    HashMap hmTempNew = new HashMap();

    HashMap hmFinalValue = new HashMap();

    String strLinkID = (String) hmLinkDetails.get("LINKID");

    int intLinkRowcount = 0;
    int intcount = 0;
    int intDetailCount = 0;

    double FooterTotalAmount = 0;
    String strFooterAmount = null;

    boolean bFindFlag;


    String strCurID = "";
    String strDetailID = "";
    String strKey = "";
    String strValue = "";
    String strPosition = "";
    int pos = 0;

    alHeader = (ArrayList) hmCrossTabValue.get("Header");
    alResult = (ArrayList) hmCrossTabValue.get("Details");
    hmFooter = (HashMap) hmCrossTabValue.get("Footer");

    // System.out.println("Returns " + alSetLinkValue + alHeader);

    if ((alSetLinkValue.size() == 0 || alHeader.size() == 0)) {
      // System.out.println("Returns null");
      return hmCrossTabValue;
    }

    // System.out.println("After " + alSetLinkValue + hmCrossTabValue);

    // **************************************************************
    // Below code to set the header information (additional header)
    // **************************************************************
    alLinkValue = (ArrayList) hmLinkDetails.get("LINKVALUE");
    strPosition = GmCommonClass.parseNull((String) hmLinkDetails.get("POSITION"));
    String strColunmNameToPos = GmCommonClass.parseNull((String) hmLinkDetails.get("COLUMNNAME"));
    int iColumnPosition = 0;
    intLinkRowcount = alLinkValue.size();


    for (int count = 0; count < alHeader.size(); count++) {
      if (alHeader.get(count).equals(strColunmNameToPos)) {
        iColumnPosition = count;
        iColumnPosition = iColumnPosition - 1;
        break;
      }
      // log.debug("HashMap : " + );
    }

    for (int i = pos; i < intLinkRowcount; i++) {
      hmTempMain = (HashMap) alLinkValue.get(i);
      // log.debug(iColumnPosition + " " + i + " " + strColunmNameToPos);
      if (strPosition.equals("AFTER")) {
        if (!strColunmNameToPos.equals("")) {
          alHeader.add(i + 2 + iColumnPosition, hmTempMain.get("VALUE"));
        } else {
          alHeader.add(hmTempMain.get("VALUE"));
        }
      } else {
        alHeader.add(i + 1 + iColumnPosition, hmTempMain.get("VALUE"));
      }

      hmFooter.put(hmTempMain.get("VALUE"), "0.00");
    }

    intcount = alSetLinkValue.size();
    intDetailCount = alResult.size();
    // Main look holds the new addition
    for (int i = 0; i < intcount; i++) {
      // log.debug(" hmFooter  inside i loop " + hmFooter);
      hmTempMain = (HashMap) alSetLinkValue.get(i);
      // If link Id not equals null then process

      strCurID = (String) hmTempMain.get(strLinkID);

      bFindFlag = false;
      // new loop holds the actual dataset value
      for (int j = 0; j < intDetailCount; j++) {
        // log.debug(" hmFooter after j loop " + hmFooter);
        hmTempDetail = (HashMap) alResult.get(j);
        strDetailID = (String) hmTempDetail.get(strLinkID);

        if (strCurID.equals(strDetailID)) {
          bFindFlag = true;
          // to get the key value for selected parts
          for (int k = 0; k < intLinkRowcount; k++) {

            hmTempLink = (HashMap) alLinkValue.get(k);
            // log.debug("hmTempLink :"+hmTempLink);
            strKey = (String) hmTempLink.get("KEY");
            // log.debug("hmTempMain :"+hmTempMain);
            // strKey = (String) hmTempMain.get(strKey);
            strKey = GmCommonClass.parseNull((String) hmTempMain.get(strKey)); // when do crosstab,
                                                                               // then do
                                                                               // linkcrosstab in
                                                                               // the bean, possibly
                                                                               // be null, so need
                                                                               // parseNull here.
            // log.debug("Key :"+strKey);
            strValue = (String) hmTempLink.get("VALUE");
            // log.debug("  hmTempDetail is " + hmTempDetail);
            hmTempDetail.put(strValue, strKey.trim());

            // Below code to fetch final grand total to be validated
            FooterTotalAmount = 0;
            strFooterAmount = "";

            strFooterAmount = (String) hmFooter.get(strValue);
            try {
              // log.debug(" Footer Total Amount " + strFooterAmount + " Key is " + strKey);
              FooterTotalAmount =
                  Double.parseDouble(strFooterAmount)
                      + Double.parseDouble(GmCommonClass.parseZero(strKey));
              // log.debug (" After calc " +FooterTotalAmount);
            } catch (NumberFormatException exp) {// skip the calculation
                                                 // log.debug("Error");
            }
            hmFooter.put(strValue, String.valueOf(FooterTotalAmount));
          }// new column loops ends here
           // log.debug(" bFindFlag b4 break " + bFindFlag);
          break;
        }
        // System.out.println(" Link: "+hmTempDetail.get(strLinkID));
        if (hmTempDetail.get(strLinkID) != null) {
          alResult.set(j, hmTempDetail);
        }
        // log.debug(" bFindDlag " + bFindFlag + " strCurID " + strCurID + " strDetailID "
        // +strDetailID);
      }// dataset loop ends here
       // log.debug(" Footer Amount " + FooterTotalAmount);
       // *********************************************************
       // If not find and the same to main list
       // Currently used for missed distributor list
       // *********************************************************
      if (bFindFlag == false && !blSkipExtraColumn) {
        hmTempNew = null;
        hmTempNew = new HashMap();
        hmTempNew.put(strLinkID, hmTempMain.get(strLinkID));

        // Hardcoded value NAME to fecth the name info
        hmTempNew.put("Name", hmTempMain.get("NAME"));
        for (int k = 0; k < intLinkRowcount; k++) {
          hmTempLink = (HashMap) alLinkValue.get(k);
          strKey = (String) hmTempLink.get("KEY");
          strKey = GmCommonClass.parseNull((String) hmTempMain.get(strKey));
          strValue = (String) hmTempLink.get("VALUE");

          /*
           * if(strKey != null && !strKey.equals("") && strValue != null && !strValue.equals("")) {
           */
          hmTempNew.put(strValue, strKey.trim());
          // }

          // Below code to fetch final grand total to be validated
          FooterTotalAmount = 0;
          strFooterAmount = "";
          strFooterAmount = (String) hmFooter.get(strValue);
          try {
            FooterTotalAmount = Double.parseDouble(strFooterAmount) + Double.parseDouble(strKey);
          } catch (NumberFormatException exp) {// skip the calculation
                                               // log.debug("Error");
          }
          hmFooter.put(strValue, String.valueOf(FooterTotalAmount));
        }// new column loops ends here
        if (hmTempNew.get(strLinkID) != null) {
          alResult.add(hmTempNew);
        }

      }// End Find Flag Filter
    }// actual value loop ends here

    // Final Value to be passed to the report
    hmFinalValue.put("Header", alHeader);
    hmFinalValue.put("Details", alResult);
    hmFinalValue.put("Footer", hmFooter);
    // System.out.println("Footer : "+ hmFooter);
    // log.debug(" Header values in link " + alHeader);
    // log.debug(" Details values in link " + alResult);
    // log.debug(" Footer values in link " + hmFooter);

    return hmFinalValue;
  }


  public HashMap reCalculateFooter(HashMap hmParam, ArrayList alDataStore, String strAction) {
    Iterator iDataStore = alDataStore.iterator();
    String strColumnName = "";
    String strOperator = "";
    ArrayList alOperands = new ArrayList();
    String strDivident = "";
    String strDivider = "";
    double dColumnValue = -1;
    String dividentValue = "";
    String dividerValue = "";
    String strOperand1 = "";
    String strOperand2 = "";
    double dblValue1 = 0.0;
    double dblValue2 = 0.0;
    double dblNumerator = 0.0;
    double dblDenominator = 0.0;
    double dblPercentage = 0.0;
    double dblAAIPERCENT=0.0;
    double dblAAI=0.0;
    double dblAAIBUDGET=0.0;
    int turns = 0;
    HashMap hmFooter;
    ArrayList alDetails;
    
    hmFooter = hmParam.get("Footer") == null ? new HashMap() : (HashMap) hmParam.get("Footer");
    alDetails =
        hmParam.get("Details") == null ? new ArrayList() : (ArrayList) hmParam.get("Details");
    double detailsSize = alDetails.size();
    String strColumnValue = "";
    GmFooterCalcuationBean calcuationBean = null;
    
    if (!strAction.equals("LoadGCONS")) {
      // Total Value AAI Budget % calculation: Total sum of AAI/Total Sum of AAI Budget
      // PMT-16649 Author: gpalani
      dblAAI = Double.parseDouble(GmCommonClass.parseZero((String) hmFooter.get("AAI")));
      dblAAIBUDGET =
          Double.parseDouble(GmCommonClass.parseZero((String) hmFooter.get("AAI Budget")));
      
      if (dblAAIBUDGET > 0){
		dblAAIPERCENT = (dblAAI / dblAAIBUDGET) * 100;
	  }

      hmFooter.put("% AAI Budget", Double.toString(dblAAIPERCENT));
    }
    
    while (iDataStore.hasNext()) {
      calcuationBean = (GmFooterCalcuationBean) iDataStore.next();
      strColumnName = calcuationBean.getColumnNameToModify();
      strColumnValue = (String) hmFooter.get(strColumnName);
      strOperator = calcuationBean.getOperator();
      alOperands = calcuationBean.getOperands();

      if (strOperator.equals("DIV")) {
        strDivident = (String) alOperands.get(0);
        strDivider = (String) alOperands.get(1);
        dividentValue = (String) hmFooter.get(strDivident);
        // log.debug(" dividentValue " + dividentValue);
        dividerValue = (String) hmFooter.get(strDivider);
        if (dividentValue != null && dividerValue != null && Double.parseDouble(dividentValue) > 0
            && Double.parseDouble(dividerValue) > 0) {
          dColumnValue = Double.parseDouble(dividentValue) / Double.parseDouble(dividerValue);
          strColumnValue = String.valueOf(dColumnValue);
        }
        hmFooter.put(strColumnName, strColumnValue);
      }

      if (strOperator.equals("PERCENTAGE")) {
        ArrayList alNumerator = calcuationBean.getNumerator();
        ArrayList alDenominator = calcuationBean.getDenominator();
        dblNumerator = 0;
        dblDenominator = 0;

        for (int i = 0; i < alNumerator.size(); i++) {
          dblNumerator =
              dblNumerator
                  + Double.parseDouble(GmCommonClass.parseZero((String) hmFooter.get(alNumerator
                      .get(i))));
        }

        for (int i = 0; i < alDenominator.size(); i++) {
          dblDenominator =
              dblDenominator
                  + Double.parseDouble(GmCommonClass.parseZero((String) hmFooter.get(alDenominator
                      .get(i))));
        }
        dblPercentage = (dblDenominator == 0) ? 0 : (dblNumerator * 100 / dblDenominator);
        strColumnValue = String.valueOf(dblPercentage);
        hmFooter.put(strColumnName, strColumnValue);
      }

      if (strOperator.equals("SUB")) {
        strDivident = (String) alOperands.get(0);
        strDivider = (String) alOperands.get(1);
        dividentValue = (String) hmFooter.get(strDivident);
        dividerValue = (String) hmFooter.get(strDivider);
        if (dividentValue != null && dividerValue != null && Double.parseDouble(dividentValue) >= 0
            && Double.parseDouble(dividerValue) >= 0) {
          if (Double.parseDouble(dividentValue) > 0) {
            dColumnValue = Double.parseDouble(dividentValue) - Double.parseDouble(dividerValue);
          } else {
            dColumnValue = 0;
          }
          strColumnValue = String.valueOf(dColumnValue);
        }
        hmFooter.put(strColumnName, strColumnValue);
      }

      if (strOperator.equals("AVERAGE")) {
        if (detailsSize > 0 && strColumnValue != null && !strColumnValue.equals("")) {
          strColumnValue = String.valueOf(Double.parseDouble(strColumnValue) / detailsSize);
        }
        hmFooter.put(strColumnName, strColumnValue);
      }

      /*
       * if(strOperator.equals("LTURNS")) { strDivident = (String) alOperands.get(0); turns =
       * Integer.parseInt(GmCommonClass.parseZero((String)alOperands.get(1))); strDivider = (String)
       * alOperands.get(2);
       * 
       * dividentValue = (String) hmFooter.get(strDivident); //log.debug(" dividentValue " +
       * dividentValue); dividerValue = (String) hmFooter.get(strDivider); if (dividentValue != null
       * && dividerValue != null && Double.parseDouble(dividentValue) > 0 &&
       * Double.parseDouble(dividerValue) > 0) { dColumnValue = Double.parseDouble(dividentValue) *
       * turns *100 / Double.parseDouble(dividerValue); //log.debug(" dColumnValue turns " +
       * dColumnValue); strColumnValue = String.valueOf(dColumnValue); } hmFooter.put(strColumnName,
       * strColumnValue); }
       */

      if (strOperator.equals("LOANERCASESPERMONTH")) {
        strDivident = (String) alOperands.get(0);
        turns = Integer.parseInt(GmCommonClass.parseZero((String) alOperands.get(1)));
        strDivider = (String) alOperands.get(2);
        dividentValue = (String) hmFooter.get(strDivident);

        log.debug(" dividentValue " + dividentValue + " dividerValue " + dividerValue
            + " strDivider -- " + strDivider);
        dividerValue = (String) hmFooter.get(strDivider);
        if (dividentValue != null && dividerValue != null && Double.parseDouble(dividentValue) > 0
            && Double.parseDouble(dividerValue) > 0) {
          if (strDivider.equals("Loaner Sets")) {
            dColumnValue = Double.parseDouble(dividentValue) / Double.parseDouble(dividerValue);
          } else {
            dColumnValue =
                Double.parseDouble(dividentValue)
                    / (Double.parseDouble(dividerValue) / (turns * 2));
          }
          log.debug(" dColumnValue turns " + dColumnValue);
          strColumnValue = String.valueOf(dColumnValue);
        }
        hmFooter.put(strColumnName, strColumnValue);

      }

      if (strOperator.equals("MAKENULL")) {
        hmFooter.put(strColumnName, "0");
      }
    }
    hmParam.put("Footer", hmFooter);
    return hmParam;
  }

  /**
   * terminate - will close any active callablestatement or connection will be called from finally
   * 
   * @throws AppError
   */
  protected void terminate(ResultSet rs) throws AppError {
    log.debug(" closing Crosstab connection ");
    try {
      Statement stmt = rs.getStatement();
      Connection conn = stmt.getConnection();

      if (rs.getStatement() != null) {
        stmt.close();

      }// Enf of if (callableStatement != null)

      if (conn != null) {
        conn.rollback();
        conn.close(); /* closing the Connections */
      }
    } catch (Exception e) {
      e.printStackTrace();
      // throw new AppError(e);
    }// End of catch
  }
}