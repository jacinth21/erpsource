package com.globus.common.beans;

import java.security.MessageDigest;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmSecurityBean extends GmBean {

  /**
   * loadAccessInformation - This method is used to get the Access level information for the
   * selected user Access level is retrieved based on the user and his department id
   * 
   * @param String strUserId
   * @param String strDepID
   * @return HashMap
   * @exception AppError
   **/
  protected Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the
                                                                         // Logger Class.;

  /*
   * Initialize the Logger Class.This will be removed all place changed with GmDataStoreVO
   * constructor
   */

  public GmSecurityBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmSecurityBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public HashMap loadAccessInformation(String strUserID, String strDepID) throws AppError {
    DBConnectionWrapper dbCon = null;
    dbCon = new DBConnectionWrapper();

    StringBuffer sbQuery = new StringBuffer();

    ArrayList alReturn = new ArrayList();
    HashMap hmReturn = new HashMap();
    HashMap hmapValue = new HashMap();

    int intDetailsize;

    try {
      sbQuery.append(" SELECT  T104.C104_CONTROL_NAME ");
      sbQuery.append(" ,GET_CODE_NAME(T105.C901_ACCESS_TYPE) ACCESS_TYPE ");
      sbQuery.append(" FROM 	 T104_CONTROL_LIST T104 ");
      sbQuery.append(" ,T105_SECURITY_DETAILS T105  ");
      sbQuery.append(" WHERE  T104.C104_CONTROL_ID = T105.C104_CONTROL_ID ");
      sbQuery.append(" AND	( T105.C901_DEPARTMENT_ID = ");
      sbQuery.append(strDepID);
      sbQuery.append(" OR T105.C101_USER_ID = ");
      sbQuery.append(strUserID);
      sbQuery.append(" ) ");
      sbQuery.append(" AND  	NVL(T104.C104_DELETE_FL,'N') = 'N' ");

      // To Execute the Query
      alReturn = dbCon.queryMultipleRecords(sbQuery.toString());

      intDetailsize = alReturn.size();

      for (int i = 0; i < intDetailsize; i++) {
        hmapValue = (HashMap) alReturn.get(i);
        hmReturn.put(hmapValue.get("C104_CONTROL_NAME"), hmapValue.get("ACCESS_TYPE"));
      }

    } catch (Exception e) {
      GmLogError.log("Exception in GmCustomerBean:reportPendingOrders", "Exception is:" + e);
      throw new AppError(e);
    } finally {
      dbCon = null;
    }
    return hmReturn;
  }

  /**
   * loadAccessInformation - This method is used to check the Access level for selected user
   * 
   * @param HashMap hmReturn
   * @param String strName
   * @return boolean
   * @exception AppError
   **/
  public boolean checkScreenAccess(HashMap hmReturn, String strName, String strDepartMentID) {

    String strValue = "";
    int intPosition = strName.lastIndexOf("/");
    strName = strName.substring(intPosition + 1);
    strValue = (String) hmReturn.get(strName);

    if (strValue == null) {
      Iterator cntrlNameItr = hmReturn.keySet().iterator();
      while (cntrlNameItr.hasNext()) {
        String strCntrlName = (String) cntrlNameItr.next();
        if (strName.contains(strCntrlName)) {
          strValue = (String) hmReturn.get(strCntrlName);
          break;
        }
      }
    }

    // If the vale is not equals to null and have Full access then
    // retunr true else return false
    if (strValue == null) {
      if (strDepartMentID.equals("S")) {
        return false;
      } else {
        return true;
      }

    } else {
      if (strValue.equals("F")) {
        return true;
      } else {
        return false;
      }
    }

  }

  /**
   * checkCOGSAccess - This method is used to check if the User department has access for COGS
   * Consignment Type. If the User is trying to access COGS type and he doesnt have access, then
   * throw AppError
   * 
   * @param String strType - Consignment Type
   * @param String strDepartMentID - Department Id
   * @return boolean
   * @exception AppError
   **/
  public boolean checkCOGSAccess(String strType, String strDepartMentID) throws AppError {
    boolean blAccess = false;

    if (strDepartMentID.equals("E") || strDepartMentID.equals("Z") || strDepartMentID.equals("A")) {
      blAccess = true;
    }

    if (!blAccess && strType.equals("50420")) {
      throw new AppError("", "20682");
    }
    return blAccess;
  }

  public String saveRememberUser(String strUserNm, String strPassword) throws Exception {
    String uuid = UUID.randomUUID().toString();
    Random generator = new Random();
    String strString = strUserNm + "@" + uuid;

    MessageDigest md = MessageDigest.getInstance("MD5");
    md.update(strString.getBytes());

    byte byteData[] = md.digest();

    // convert the byte to hex format method 1

    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < byteData.length; i++) {
      sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
    }

    String strPasswordKey = "#DWPK" + generator.nextInt();
    String strEncPassword = encrypt(strPassword, strPasswordKey);

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_sav_remember_user", 3);
    gmDBManager.setString(1, strUserNm);
    gmDBManager.setString(2, strEncPassword);
    gmDBManager.setString(3, sb.toString());

    gmDBManager.execute();
    gmDBManager.commit();

    return sb.toString() + strPasswordKey;
  }

  // referred from
  // http://www.coderanch.com/t/496037/java/java/javax-crypto-BadPaddingException-Given-final

  public String encrypt(String value, String strKey) {
    try {
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
      byte[] encBytes = strKey.getBytes("UTF8");
      DESKeySpec keySpecEncrypt = new DESKeySpec(encBytes);
      SecretKey keyEncrypt = keyFactory.generateSecret(keySpecEncrypt);
      Cipher cipher = Cipher.getInstance("DES");
      cipher.init(Cipher.ENCRYPT_MODE, keyEncrypt);
      byte[] plainTxtBytes = value.getBytes();
      byte[] encBytesT = cipher.doFinal(plainTxtBytes);
      return new sun.misc.BASE64Encoder().encode(encBytesT);
    } catch (Exception ex) {
      return value;
    }
  }

  // referred from
  // http://www.coderanch.com/t/496037/java/java/javax-crypto-BadPaddingException-Given-final

  public String decrypt(String value, String strKey) {
    try {
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
      byte[] decBytes = strKey.getBytes("UTF8");
      DESKeySpec keySpecDecrypt = new DESKeySpec(decBytes);
      SecretKey keyDecrypt = keyFactory.generateSecret(keySpecDecrypt);
      Cipher cipher = Cipher.getInstance("DES");
      cipher.init(Cipher.DECRYPT_MODE, keyDecrypt);
      byte[] encBytes = new sun.misc.BASE64Decoder().decodeBuffer(value);
      byte[] plainTxtBytes = cipher.doFinal(encBytes);
      return new String(plainTxtBytes);
    } catch (Exception ex) {
      return value;
    }
  }

  public HashMap fetchUser(String strCookieId) throws AppError {
    HashMap hmResult = new HashMap();

    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_saved_user_crdn", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, strCookieId);
    gmDBManager.execute();
    hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();

    return hmResult;
  }

  public void saveForgotUser(String strUserNm) throws AppError {
    GmDBManager gmDBManager = new GmDBManager();
    gmDBManager.setPrepareString("gm_pkg_it_user.gm_sav_forgot_user", 1);
    gmDBManager.setString(1, strUserNm);
    gmDBManager.execute();
    gmDBManager.commit();
  }
 public ArrayList fetchMicroAppSecurityGroups(String strUserId) throws AppError {
	ArrayList alResult = new ArrayList();
	GmDBManager gmDBManager = new GmDBManager();
	gmDBManager.setPrepareString("gm_pkg_it_user.gm_fch_micro_app_user", 2);
	gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
	gmDBManager.setString(1, strUserId);
	gmDBManager.execute();
	alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
	gmDBManager.close();
	return alResult;
	}

}