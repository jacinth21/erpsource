package com.globus.common.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

public class GmJobSchedulerBean {
  private static SchedulerFactory schedFact = null;
  private static Scheduler sched = null;
  private static Logger schedulerlog = GmLogger
      .getInstance("com.globus.common.beans.GmJobSchedulerBean");

  private static void initialize() throws SchedulerException {
    schedFact = new StdSchedulerFactory();
    sched = schedFact.getScheduler();
    deleteJobs();
    sched.start();

  }

  private static void deleteJobs() throws SchedulerException {
    String[] jobGroups;
    String[] jobsInGroup;
    int i;
    int j;

    jobGroups = sched.getJobGroupNames();
    for (i = 0; i < jobGroups.length; i++) {
      jobsInGroup = sched.getJobNames(jobGroups[i]);

      for (j = 0; j < jobsInGroup.length; j++) {
        sched.deleteJob(jobsInGroup[j], jobGroups[i]);
        schedulerlog.debug("Removing existing Job '" + jobsInGroup[j] + "' from the memory");
      }
    }
  }

  public void loadScheduledJobs(String strCompanyId) throws Exception {

    String strCmpSuffix = "_" + strCompanyId;
    // log.debug("Loading Scheduled Jobs...");
    try {
      schedulerlog.debug("Initializing Scheduler...");
      // initialize();
      schedulerlog.debug("Scheduler Started");

      GmCommonBean commonBean = new GmCommonBean();
      HashMap hmMap =
          commonBean.getScheduledJobs(
              GmCommonClass.getString("GMSCHEDULERXML").replace(".xml", strCmpSuffix + ".xml"),
              "SCHEDULE");
      Iterator jobItr = hmMap.keySet().iterator();
      while (jobItr.hasNext()) {
        String strJobNameKey = (String) jobItr.next();
        HashMap hmJobAttributes = (HashMap) hmMap.get(strJobNameKey);

        String strJobName = (String) hmJobAttributes.get("Name");
        strJobName += strCmpSuffix;

        Class klass = Class.forName("com.globus.common.util.jobs.GmJobRunner");

        JobDetail jobDetail = new JobDetail(strJobName, Scheduler.DEFAULT_GROUP, klass);

        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(strJobName, (String) hmJobAttributes.get("Class"));
        jobDetail.setJobDataMap(jobDataMap);
        // get param attributes and set to the job data map object as hash map
        HashMap hmParam = (HashMap) hmJobAttributes.get("HMPARAM");
        HashMap hmGlobalParam =
            GmCommonClass.parseNullHashMap((HashMap) hmJobAttributes.get("HMGLOBALPARAM"));
        schedulerlog.debug("HMGLOBALPARAM =" + hmJobAttributes.get("HMGLOBALPARAM"));
        jobDataMap.put("HMGLOBALPARAM", hmGlobalParam);
        jobDataMap.put("HMPARAM", hmParam);

        String strTriggerTimezone = GmCommonClass.parseNull((String) hmParam.get("TIMEZONE"));
        strTriggerTimezone =
            (strTriggerTimezone.equals("")) ? GmCommonClass.parseNull((String) hmGlobalParam
                .get("TIMEZONE")) : strTriggerTimezone;


        ArrayList alTrigger = (ArrayList) hmJobAttributes.get("Trigger");
        Iterator triggerItr = alTrigger.iterator();
        int count = 0;

        while (triggerItr.hasNext()) {
          count++;
          HashMap hmTriggerAttributes = (HashMap) triggerItr.next();
          String strTriggerName = (String) hmTriggerAttributes.get("Name");
          strTriggerName += strCmpSuffix;
          String strTriggerExpression = (String) hmTriggerAttributes.get("Expression");
          // CronTrigger cronTrigger =
          // new CronTrigger(strTriggerName, Scheduler.DEFAULT_GROUP, strTriggerExpression);

          schedulerlog.debug("strTriggerTimezone =" + strTriggerTimezone);
          CronTrigger cronTrigger =
              new CronTrigger(strTriggerName, Scheduler.DEFAULT_GROUP, strJobName,
                  Scheduler.DEFAULT_GROUP, strTriggerExpression,
                  TimeZone.getTimeZone(strTriggerTimezone));

          if (count == 1) {
            sched.scheduleJob(jobDetail, cronTrigger);
            schedulerlog.debug("Job " + strJobName + " is loaded with the trigger "
                + strTriggerExpression + " with timezone of " + strTriggerTimezone);
          } else {
            cronTrigger.setJobName(strJobName);
            sched.scheduleJob(cronTrigger);
            schedulerlog.debug("Trigger " + strTriggerName + " is added to the Job "
                + strTriggerExpression + " with timezone of " + strTriggerTimezone);
          }
        }
      }
      schedulerlog.debug("Jobs Loaded");
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new Exception(ex);
    }
  }

  /**
   * This method will get company list from rule table and schedule the job for each countries
   * 
   */
  public void loadCompanyScheduledJobs() throws Exception {
    initialize();
    String strCompanys = GmCommonClass.getRuleValue("COMPANYS", "ONEPOARTJOBS");
    String strCompArray[] = GmCommonClass.StringtoArray(strCompanys, ",");
    for (int i = 0; i < strCompArray.length; i++) {
      loadScheduledJobs(strCompArray[i]);
    }
  }
}
