package com.globus.common.beans;

import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
public class GmBean {
  private String compDateFmt = "";
  private String compTimeZone = "";
  private String compId = "";
  private String compPlantId = "";
  private String complangid = "";

  private GmDataStoreVO gmDataStoreVO = new GmDataStoreVO();

  /**
   * All Bean needs to extend this bean.
   */


  /**
   * @return GmDataStoreVO
   */
  public GmDataStoreVO getGmDataStoreVO() {
    return this.gmDataStoreVO;
  }

  /**
   * @return compDateFmt
   */
  public String getCompDateFmt() {
    return compDateFmt;
  }

  /**
   * @return compTimeZone
   */
  public String getCompTimeZone() {
    return compTimeZone;
  }

  /**
   * @return compId
   */
  public String getCompId() {
    return compId;
  }

  /**
   * @return compPlantId
   */
  public String getCompPlantId() {
    return compPlantId;
  }

  /**
   * @return complangid
   */
  public String getComplangid() {
    return complangid;
  }

  /**
   * Constructor will validate and populate company info. in gmDataStoreVO.
   * 
   * @param gmDataStoreVO
   */
  public GmBean(GmDataStoreVO gmDataStoreVO) throws AppError {
    super();
    this.gmDataStoreVO = gmDataStoreVO;
    this.compDateFmt = gmDataStoreVO.getCmpdfmt();
    this.compTimeZone = gmDataStoreVO.getCmptzone();
    this.compId = gmDataStoreVO.getCmpid();
    this.compPlantId = gmDataStoreVO.getPlantid();
    this.complangid = gmDataStoreVO.getCmplangid();

  }

}
