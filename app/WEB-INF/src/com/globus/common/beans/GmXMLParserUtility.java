package com.globus.common.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class GmXMLParserUtility {
	
	public String createXMLString(ArrayList alCart){
		StringBuffer sbXML = new StringBuffer();
		int intCartSize = alCart.size();
		HashMap hmCart = new HashMap();;
		String strKey = "";
		String strValue = "";
		Iterator itHmCart = null;
		
		 sbXML.append("<data>");		
	        for (int i=0;i<intCartSize;i++)
	        {
	        	hmCart = (HashMap)alCart.get(i);
	        	//Set bset = hmCart.keySet();
	        	//LinkedHashMap lhmap = new LinkedHashMap(hmCart);
	        	itHmCart = hmCart.keySet().iterator();
	        	
	        	sbXML.append("<pnum>");
	        	//for (int j=0; j < intHmCartSize; j++){
	        	while (itHmCart.hasNext()){
	        		strKey = (String)itHmCart.next();
	        	    strValue = (String)hmCart.get(strKey);
			        sbXML.append("<");
			        sbXML.append(strKey.toLowerCase());
			        sbXML.append(">");
			        //sbXML.append(strValue.replaceAll("'|&", ""));
			        sbXML.append(GmCommonClass.replaceForXML(strValue));
			        sbXML.append("</");
			        sbXML.append(strKey.toLowerCase());
			        sbXML.append(">");
	        	}
		        sbXML.append("</pnum>");
	        }
	    sbXML.append("</data>");
	return sbXML.toString();	
	}

	private HashMap convertAccesstoHashMap(ArrayList alAccess)
	{
		Iterator alAccessItr = alAccess.iterator();
		HashMap hmAccess = null;
		HashMap hmResult = new HashMap();
		
		String strFnId ="";
		String strUpdAcc ="";
		String strRdAcc ="";
		String strVdAcc ="";
		
		while(alAccessItr.hasNext())
		{
			hmAccess = new HashMap();
			hmAccess = (HashMap)alAccessItr.next();
			
			strFnId =	GmCommonClass.parseNull((String)hmAccess.get("FN_ID"));
			strUpdAcc =	GmCommonClass.parseNull((String)hmAccess.get("UPD_ACC"));
			strRdAcc =	GmCommonClass.parseNull((String)hmAccess.get("RD_ACC"));
			strVdAcc =	GmCommonClass.parseNull((String)hmAccess.get("VD_ACC"));
			
			hmResult.put(strFnId+"_UPD_ACC", strUpdAcc);
			hmResult.put(strFnId+"_RD_ACC", strRdAcc);
			hmResult.put(strFnId+"_VD_ACC", strVdAcc);
		}
		
		return hmResult;
	}
	
	public String createLeftMenuTree(ArrayList alList ,String strOpt, ArrayList alAccess,String strReadOnlyAccess)
    {
          String strTree = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?> <tree id=\"0\"> ";
          String strNode = ""; 
          int preLevel = 0;
          String strPreviousParent = "";
          Iterator itr = alList.iterator();
          String strType = "";
          int foldersToClose = 0;
          boolean isClosed = false;
          String strOpenFolder = "";
          
          HashMap hmAccessMap = convertAccesstoHashMap(alAccess);
          
          while(itr.hasNext())
          {
        	  isClosed = false;
                HashMap hmMap =  (HashMap)itr.next();
              int level =  Integer.parseInt((String)hmMap.get("LEVEL"));
            strType = (String)hmMap.get("TYPE");
            String strParent =(String)hmMap.get("ROOT");
            String strURL =(String)hmMap.get("URL");
            String strFunctionID =  String.valueOf(hmMap.get("FUNCTION_ID"));
			String strPath = GmCommonClass.parseNull((String) String.valueOf(hmMap.get("PATH")));
            String strGroupId = GmCommonClass.parseNull((String) hmMap.get("GROUPID"));
            	
            if ( level == 1  && preLevel > 0 )
            {
	            while(foldersToClose > 0)
	            {	            	 	         
	                  strTree = strTree + "</item>";
	                  foldersToClose--;	                  
	            }
	            isClosed = true;
            }
            if(!strPreviousParent.equals(strParent))
            {
                 preLevel = 0;
                 strPreviousParent = strParent;
            }            
            if(strType.equals("Folder"))
            {            	 
            	if (level <= 1)
            	{
            		strOpenFolder = " open=\"1\" ";
            	}
            	else 
            	{
            		strOpenFolder = "";
            	}
            	if(strOpt.equals("SEQLOOKUP")){
            		strNode =  "<item text="+ "\""+ String.valueOf(hmMap.get("FUNCTION_NM")) +"-->"+String.valueOf(hmMap.get("SEQ_NO")) + "\""  + " id="+ "\""+ strFunctionID+ "\"" + strOpenFolder + ">";
            		strNode = strNode + "<userdata name=\"SQID\">" + String.valueOf(hmMap.get("SEQ_NO")) + "</userdata>";
            	}else{
            		strNode =  "<item text="+ "\""+ String.valueOf(hmMap.get("FUNCTION_NM")) + "\""  + " id="+ "\""+ strFunctionID + "\"" + strOpenFolder + ">";
            		strNode = strNode + "<userdata name=\"ACCESS\"> TRUE </userdata>";
            	}
                foldersToClose++;
            }            
          else
          {
        	  boolean  hasAccess= String.valueOf(hmAccessMap.get(strFunctionID+"_RD_ACC")).equalsIgnoreCase("Y") ? true : false;
        	  String strColor = "blue";
		    	// get the access permission for Read only access to portal
        	  if (hmAccessMap == null || ( hmAccessMap != null && hmAccessMap.size() == 0 ) || strReadOnlyAccess.equalsIgnoreCase("Y")){
        		  hasAccess = true;  
        	  }
        	
        	  if (!hasAccess)
        	  {
        		  strColor = "lightgray";
        	  }
        	  
        	  if(strOpt.equals("SEQLOOKUP")){
                strNode =  "<item text="+ "\""+ String.valueOf(hmMap.get("FUNCTION_NM"))+"-->"+String.valueOf(hmMap.get("SEQ_NO"))+ "\"" + " id="+ "\""+ strFunctionID+ "\"" +">";
                strNode = strNode + "<userdata name=\"SQID\">" + String.valueOf(hmMap.get("SEQ_NO")) + "</userdata>";
        	  }
        	  else{
        		  if(strGroupId.contains("FAV")){
        			  strNode =  "<item  text=" + " " +"\""+ String.valueOf(hmMap.get("FUNCTION_NM"))+ "\"" + " id="+ "\""+ strFunctionID+ "\"" + " tooltip="+ "\""+ strPath+ "\"" +">";
        		  }else{
        			  strNode =  "<item  text=" + " " +"\""+ String.valueOf(hmMap.get("FUNCTION_NM"))+ "\"" + " id="+ "\""+ strFunctionID+ "\"" +">";
        		  }
        		  
        	  }
        	  
        	  strNode = strNode + "<userdata name=\"FUNCTION_NM\">" + String.valueOf(hmMap.get("FUNCTION_NM")) + "</userdata>";
              strNode = strNode + "<userdata name=\"FUNCTION_ID\">" + strFunctionID + "</userdata>"; 
              strNode = strNode + "<userdata name=\"URL\">" + GmCommonClass.replaceForXML(String.valueOf(hmMap.get("URL"))) + "</userdata>";
              strNode = strNode + "<userdata name=\"STROPT\">" + String.valueOf(hmMap.get("STROPT")) + "</userdata>";
              
              if(hasAccess) 
              {
            	  strNode = strNode + "<userdata name=\"ACCESS\">" + "TRUE" + "</userdata>";  
              }
              else
              {
            	  strNode = strNode + "<userdata name=\"ACCESS\">" + "FALSE" + "</userdata>";
              }
              
              
              strNode = strNode + "</item>";
              
          } 
            if(preLevel  > level && !isClosed) 
            {                      
                  for ( int i = preLevel  ; i > level ;  i--)
                  {     
                        strTree = strTree + " </item> ";
                        foldersToClose--;                        
                  }  
                 
            }
            strTree = strTree +  strNode;
            preLevel = level;
          }
          while(foldersToClose > 0)
          {     
                strTree = strTree + "</item>";
                foldersToClose--;                
          }
          strTree = strTree + " </tree>";
          return strTree;
    }
	
	/**
	 * createXMLString:  This method is used to create an xml string out of a hasmap
	 */
	public String createXMLString(HashMap hmReturn){
		StringBuffer sbXML = new StringBuffer();
		
		HashMap hmCart = new HashMap();;
		String strKey = "";
		String strValue = "";
		Iterator itHmCart = null;
		
		 sbXML.append("<data>");		
	        	itHmCart = hmReturn.keySet().iterator();
	        	
	        	sbXML.append("<pnum>");
	        	while (itHmCart.hasNext()){
	        		strKey = (String)itHmCart.next();
	        	    strValue = GmCommonClass.parseNull((String)hmReturn.get(strKey));
			        sbXML.append("<");
			        sbXML.append(strKey.toLowerCase());
			        sbXML.append(">");
			        sbXML.append(GmCommonClass.replaceForXML(strValue));
			        sbXML.append("</");
			        sbXML.append(strKey.toLowerCase());
			        sbXML.append(">");
	        	}
		        sbXML.append("</pnum>");
	    sbXML.append("</data>");
	return sbXML.toString();	
	}
}
