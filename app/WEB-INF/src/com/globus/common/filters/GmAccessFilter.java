  /* gmAccessFilter is used to append filter condition based on the user role type and access type.
   * Last File Modified for PMT-20656 @ Author: gpalani. Jun 2018
   */
package com.globus.common.filters;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmCookieManager;
import com.globus.sales.actions.GmSalesDispatchAction;


public class GmAccessFilter {

	public static Logger log = GmLogger.getInstance("com.globus.common.filters.GmAccessFilter");
  


  /*
   * This function is used to get filter condition and data based on the request specified the user
   * defaults filter based on access condition and adds other filter
   */
  public static String getAccessFilter(HashMap hmParams) {

    // To get the Employee Information
    String strEmpId = GmCommonClass.parseNull((String) hmParams.get("SessUserId"));
    int intAccLevel =
        Integer.parseInt(GmCommonClass.parseNull((String) hmParams.get("SessAccLvl")));
    String strDepartMentID = GmCommonClass.parseNull((String) hmParams.get("SessDeptId"));
    String strUserRoleType = GmCommonClass.parseNull((String) hmParams.get("UserRoleType"));

    String strAccFilter = "";
    if (strDepartMentID.equalsIgnoreCase("ICS")) {// Condition Added for International Customer
                                                  // Service Department
      strAccFilter = getICSAccessFilter(intAccLevel, strEmpId, strUserRoleType);
    } else if (strDepartMentID.equalsIgnoreCase("R")) // Condition for Clinical Department
    {
      strAccFilter = getClinicalAccessFilter(intAccLevel, strEmpId);
    } else // Condition for Sales Department
    {
      strAccFilter = getSalesAccessFilter(intAccLevel, strEmpId, strUserRoleType, strDepartMentID);
    }
    return strAccFilter;
  }

  /*
   * Below Method is used to append the Access filter based on the user role type
   * Modification: Changing the IN clause to EXISTS for better performance
   * @ Author: gpalani. PMT-20656 Jun 2018
   */
  private static String getSalesAccessFilter(int intAccLevel, String strEmpId,
      String strUserRoleType, String strDepartMentID) {
    StringBuffer strQuery = new StringBuffer();
	//Logger log = GmLogger.getInstance(GmAccessFilter.getClass().getName());


    if ("S".equalsIgnoreCase(strDepartMentID)) {
      switch (intAccLevel) {

      // Sales Rep Filter Condition
        case 1: {
        
          strQuery.append(" V700.REP_ID = " + strEmpId);
          log.debug("***gmAccessFilter:Access Level 1");

          break;
        }

        // Distributor Filter Condition
        case 2: {
          strQuery.append("  EXISTS  ");	
          strQuery.append("  ( SELECT T101S.C701_DISTRIBUTOR_ID");
          strQuery.append("  FROM T101_USER T101S WHERE C101_USER_ID = " + strEmpId);
          strQuery.append("  AND T101S.C701_DISTRIBUTOR_ID=V700.D_ID )" );
          log.debug("***gmAccessFilter:Access Level 2");

          break;
        }

        // AD Filter Condition
        case 3: {
          strQuery.append("  V700.AD_ID = " + strEmpId);
          log.debug("***gmAccessFilter:Access Level 3");
          break;
        }

        // VP Filter Condition
        case 4: {
          strQuery.append("  EXISTS  ");
          strQuery.append(" ( SELECT  T708S.C901_REGION_ID ");
          strQuery.append("  FROM T708_REGION_ASD_MAPPING T708S ");
          strQuery.append("  WHERE T708S.C101_USER_ID = " + strEmpId + "");
          strQuery.append("  AND   T708S.C708_DELETE_FL IS NULL ");
          strQuery.append("  AND   T708S.C901_USER_ROLE_TYPE = 8001 ");
          strQuery.append("  AND   T708S.C901_REGION_ID=V700.REGION_ID )");          
          log.debug("***gmAccessFilter:Access Level 4");

          break;
        }

        // other AD's like Kirk Tovey want to see other region
        case 6: {
          strQuery.append("  EXISTS  ");
          strQuery.append("  (SELECT  T708S.C901_REGION_ID ");
          strQuery.append("  FROM T708_REGION_ASD_MAPPING T708S ");
          strQuery.append("  WHERE T708S.C101_USER_ID = " + strEmpId + "");
          strQuery.append("  AND T708S.C708_DELETE_FL IS NULL ");
          strQuery.append("  AND T708S.C901_USER_ROLE_TYPE = 8002 ");
          strQuery.append("  AND T708S.C901_REGION_ID=V700.REGION_ID )");           
          log.debug("***gmAccessFilter:Access Level 6");

          break;
        }

        // ASS Filter condition
        case 7: {
          strQuery.append("  EXISTS  ");
          strQuery.append("  ( SELECT T704AS.C704_ACCOUNT_ID FROM T704A_ACCOUNT_REP_MAPPING T704AS ");
          strQuery.append("  WHERE T704AS.C704A_VOID_FL is NULL AND T704AS.C703_SALES_REP_ID = " + strEmpId);
          strQuery.append("  AND T704AS.C704_ACCOUNT_ID=V700.AC_ID ) ");
          log.debug("***gmAccessFilter:Access Level 7");

          break;
        }
      }
    }

    return strQuery.toString();
  }

  /*
   * This method is used to get the where clause condition based on the access level of logged in
   * user.
   */
  private static String getClinicalAccessFilter(int intAccLevel, String strUserId) {
    StringBuffer strQuery = new StringBuffer();
    switch (intAccLevel) {
    // Site Coordinator Access Level
      case 1: {
        strQuery
            .append(" (V621.C704_ACCOUNT_ID, v621.c611_study_id) IN (SELECT V616.C704_ACCOUNT_ID, v616.c611_study_id ");
        strQuery.append(" FROM V616_SITE_PARTY_MAPPING V616 ");
        strQuery.append(" WHERE V616.C101_USER_ID  = " + strUserId + " ) ");
        break;
      }
    }
    return strQuery.toString();
  }

  /*
   * This method will be called to get the access filter for the users belonging to ICS
   * (International Customer Service Dept). return accessFilter
   * Changed the IN CLAUSE to EXISTS CLAUSE. PMT-20656 Jun 2018
   */
  private static String getICSAccessFilter(int intAccLevel, String strEmpId, String strUserRoleType) {
    StringBuffer strQuery = new StringBuffer();
    GmAccessFilter gmacc=new GmAccessFilter();


    switch (intAccLevel) {

    // Sales Rep Filter Condition
      case 1: {
        strQuery.append("  EXISTS");
        strQuery.append("  (SELECT  C901_REGION_ID ");
        strQuery.append("  FROM T708_REGION_ASD_MAPPING T708S");
        strQuery.append("  WHERE T708S.C101_USER_ID = " + strEmpId + "");
        strQuery.append("  AND   T708S.C708_DELETE_FL IS NULL ");
        strQuery.append("  AND 	 T708S.C901_USER_ROLE_TYPE = " + strUserRoleType);
        strQuery.append("  AND   T708S.C901_REGION_ID=V700.REGION_ID )");
        log.debug("***gmAccessFilter ICS:Access Level 1");

        
        break;
      }

      // Distributor Filter Condition
      case 2: {
        break;
      }

      // AD Filter Condition
      case 3: {
        break;
      }

      // VP Filter Condition
      case 4: {
        // 8001
    	strQuery.append("  EXISTS");
    	strQuery.append("  (SELECT  T708S.C901_REGION_ID ");
        strQuery.append("  FROM T708_REGION_ASD_MAPPING T708S");
        strQuery.append("  WHERE T708S.C101_USER_ID = " + strEmpId + "");
        strQuery.append("  AND   T708S.C708_DELETE_FL IS NULL ");
        strQuery.append("  AND 	 T708S.C901_USER_ROLE_TYPE = " + strUserRoleType + " ");
        strQuery.append("  AND   T708S.C901_REGION_ID=V700.REGION_ID ) ");
        log.debug("***gmAccessFilter ICS:Access Level 4");

        
        break;
      }

      case 6: {
        break;
      }

    }

    return strQuery.toString();
  }

  /*
   * This Method is called to get the Access details for the party and the function id. This method
   * will return an HashMap which will have the Void Flag,Update Flag,Read Flag for the
   * function/group the party belongs to.
   */
  public HashMap fetchPartyAccessPermission(HashMap hmParams) throws AppError {

    HashMap hmAccessDetails = new HashMap();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    String strPartyId = "";
    String strFunctionId = "";

    if (hmParams != null && hmParams.size() > 0) {

      strPartyId = GmCommonClass.parseNull((String) hmParams.get("PartyId"));
      strFunctionId = GmCommonClass.parseNull((String) hmParams.get("FunctionId"));
      hmAccessDetails = gmAccessControlBean.getAccessPermissions(strPartyId, strFunctionId);
    }
    return hmAccessDetails;
  }

  /*
   * This method is used to add the access condition to separate the Globus sales from others as
   * well as it will also add an access for the respective user to see only their data.
   */
  public StringBuffer getHierarchyWhereConditionT501(HttpServletRequest req,
      HttpServletResponse res, String strCondition) {
    StringBuffer sbWhereCondtion = new StringBuffer();

    // To get the Session Information
    HttpSession session = req.getSession(false);

    // Globus, Algea etc.. sales access
    String strRegnFilter = "";
    String strCompFilter = "";
    String strDivFilter = "";
    String strZoneFilter = "";
    String strDistFilter = "";
    String strTerrFilter = "";
    String strPgrpFilter = "";
    String strSessCompId = "";
    String strSessDivId = "";
    String strCookieFlag = "";
    String strCurrType = "";
    String strCurrSymbol = "";
    String strSessCurrType = "";
    String strRegnCond = "";
    String strSessRegnCond = "";
    String strCCurrSymbol = "";
    String strGrpAllDiv = "";
    String strGrpAllZone = "";
    String strGrpAllRegn = "";
    String strGrpAllDist = "";
    String strDistFilterUnChk = "";
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();

    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();
    // Flag value to call the getCurrSymbolByFilter method.
    Boolean blFlag = getCurrFromCookie(req, res, gmCookieManager);

    // Get from request or from cookie if not found
    strRegnFilter = GmCommonClass.parseNull(req.getParameter("hRegnFilter"));
    strCompFilter = GmCommonClass.parseNull(req.getParameter("hCompFilter"));
    strDivFilter = GmCommonClass.parseNull(req.getParameter("hDivFilter"));
    strZoneFilter = GmCommonClass.parseNull(req.getParameter("hZoneFilter"));
    strDistFilter = GmCommonClass.parseNull(req.getParameter("hDistFilter"));
    strTerrFilter = GmCommonClass.parseNull(req.getParameter("hTerrFilter"));
    strPgrpFilter = GmCommonClass.parseNull(req.getParameter("hPgrpFilter"));
    strSessCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompId"));
    strSessDivId = GmCommonClass.parseNull((String) session.getAttribute("strSessDivId"));
    strCookieFlag = GmCommonClass.parseNull(req.getParameter("hCookieFlag"));
    strCurrType = GmCommonClass.parseNull(req.getParameter("CurrType"));
    strSessCurrType = GmCommonClass.parseNull((String) session.getAttribute("strSessCurrType"));
    strRegnCond = GmCommonClass.parseNull((String) (req.getAttribute("hCondition")));
    strSessRegnCond = GmCommonClass.parseNull((String) session.getAttribute("strSessCondition"));
    strGrpAllDiv = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Div"));
    strGrpAllZone = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Zone"));
    strGrpAllRegn = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Regn"));
    strGrpAllDist = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Dist"));
    strDistFilterUnChk = GmCommonClass.parseNull(req.getParameter("hDistFilterUnChk"));
    String strBBASalesAcsFl = GmCommonClass.parseNull((String) session.getAttribute("strSessBBASalesAcsFl"));
    String strAddnlCompFl =  GmCommonClass.parseNull((String) session.getAttribute("strSessAddnlCompFl"));
    String strSessPrimCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
    // while clicking the Go/Load button at that time we no need to get filter values from cookies.
    if (!strCookieFlag.equals("NO")) {
      strRegnFilter =
          strRegnFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chRegnFilter")) : strRegnFilter;
      strCompFilter =
          strCompFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chCompFilter")) : strCompFilter;
      strDivFilter =
          strDivFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chDivFilter")) : strDivFilter;
      strZoneFilter =
          strZoneFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chZoneFilter")) : strZoneFilter;
      strDistFilter =
          strDistFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chDistFilter")) : strDistFilter;
      strTerrFilter =
          strTerrFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chTerrFilter")) : strTerrFilter;
      strGrpAllDiv =
          strGrpAllDiv.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllDiv")) : strGrpAllDiv;
      strGrpAllZone =
          strGrpAllZone.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllZone")) : strGrpAllZone;
      strGrpAllRegn =
          strGrpAllRegn.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllRegn")) : strGrpAllRegn;
      strGrpAllDist =
          strGrpAllDist.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllDist")) : strGrpAllDist;
      strDistFilterUnChk =
          strDistFilterUnChk.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              req, "chDistFilterUnChk")) : strDistFilterUnChk;


      // Commented because causing an issue for the Field Inventory report.
      // strPgrpFilter = strPgrpFilter.equals("") ?
      // GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "chPgrpFilter")) :
      // strPgrpFilter;
      strSessCompId =
          strSessCompId.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "cstrSessCompId")) : strSessCompId;
      strSessCurrType =
          strSessCurrType.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "CCurrType")) : strSessCurrType;

      strSessRegnCond =
          strSessRegnCond.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "CCondition")) : strSessRegnCond;

    }

    strCurrType = strCurrType.equals("") ? strSessCurrType : strCurrType;
    strRegnCond = strRegnCond.equals("") ? strSessRegnCond : strRegnCond;

    req.setAttribute("hRegnFilter", strRegnFilter);
    req.setAttribute("hCompFilter", strCompFilter);
    req.setAttribute("hDivFilter", strDivFilter);
    req.setAttribute("hZoneFilter", strZoneFilter);
    req.setAttribute("hDistFilter", strDistFilter);
    req.setAttribute("hTerrFilter", strTerrFilter);
    req.setAttribute("hPgrpFilter", strPgrpFilter);
    req.setAttribute("strGrpAllDiv", strGrpAllDiv);
    req.setAttribute("strGrpAllZone", strGrpAllZone);
    req.setAttribute("strGrpAllRegn", strGrpAllRegn);
    req.setAttribute("strGrpAllDist", strGrpAllDist);
    req.setAttribute("hDistFilterUnChk", strDistFilterUnChk);

    strCompFilter = strCompFilter.equals("") ? strSessCompId : strCompFilter; // Globus Medical Inc.
    strDivFilter = strDivFilter.equals("") ? strSessDivId : strDivFilter; // US Div.
    session.setAttribute("strSessCompId", strCompFilter);
    session.setAttribute("strSessDivId", strDivFilter);

    hmParam.put("COMP_FILTER", strCompFilter);
    hmParam.put("DIV_FILTER", strDivFilter);
    hmParam.put("ZONE_FILTER", strZoneFilter);
    hmParam.put("REGN_FILTER", strRegnFilter);
    // If distributor select all value is checked , we are getting values based on division,region
    // and zone
    // so we passing distributor filter as empty
    if (strGrpAllDist.equals("Y")) {
      hmParam.put("DIST_FILTER", "");
    } else {
      hmParam.put("DIST_FILTER", strDistFilter);
    }
    hmParam.put("TERR_FILTER", strTerrFilter);
    hmParam.put("STRCONDITION", strCondition);
    hmParam.put("PGRP_FILTER", strPgrpFilter);
    hmParam.put("CURRTYPE", strCurrType);
    hmParam.put("REGN_CONDITION", strRegnCond);
    // If distributor checked value is greater than distributor unchecked value, we are passing
    // unchecked values
    hmParam.put("DIST_UNCHK_FILTER", strDistFilterUnChk);
  // Fetching the Additional company flag from session.   
    hmParam.put("ADDNL_COMP_IN_SALES", strAddnlCompFl);
    hmParam.put("SHOW_ADNL_COMP_SALES", strBBASalesAcsFl);
    hmParam.put("PRIMARY_CMPY", strSessPrimCompId);


    sbWhereCondtion = this.getHierarchyWhereConditionT501(hmParam);

    strCCurrSymbol = GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "CCurrSymbol"));
    // Decoding the Encoded cookie value.
    try {
      strCCurrSymbol = URLDecoder.decode(strCCurrSymbol, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    // If the regions from the request and cookie are not same Or the Currency symbol is not present
    // in the Cookie get the Currency Symbol from the DB Call else get it from the Cookie.
    if (blFlag.equals(true) || strCCurrSymbol.equals("")) {

      hmReturn = this.getCurrSymbolByFilter(session, hmParam);

      strCurrType = GmCommonClass.parseNull((String) hmReturn.get("CURR_TYPE"));
      strCurrSymbol = GmCommonClass.parseNull((String) hmReturn.get("CURR_SYMBOL"));

    } else {

      strCurrSymbol = strCCurrSymbol;

    }


    req.setAttribute("hCurrSymb", strCurrSymbol);
    req.setAttribute("hCurrType", strCurrType);

    session.setAttribute("strSessCurrType", strCurrType);
    session.setAttribute("strSessCurrSymb", strCurrSymbol);

    gmCookieManager.setCookieValue(res, "CCurrType", strCurrType);
    // Cookies does not allow ASCII codes , so need to encode with UTF-8.
    try {
      strCurrSymbol = URLEncoder.encode(strCurrSymbol, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    gmCookieManager.setCookieValue(res, "CCurrSymbol", strCurrSymbol);

    // Set Cookies
    gmCookieManager.setCookieValue(res, "chRegnFilter", strRegnFilter);
    gmCookieManager.setCookieValue(res, "chCompFilter", strCompFilter);
    gmCookieManager.setCookieValue(res, "chDivFilter", strDivFilter);
    gmCookieManager.setCookieValue(res, "chZoneFilter", strZoneFilter);
    gmCookieManager.setCookieValue(res, "chDistFilter", strDistFilter);
    gmCookieManager.setCookieValue(res, "chTerrFilter", strTerrFilter);
    gmCookieManager.setCookieValue(res, "chPgrpFilter", strPgrpFilter);
    gmCookieManager.setCookieValue(res, "cstrSessCompId", strSessCompId);
    gmCookieManager.setCookieValue(res, "chGrpAllDiv", strGrpAllDiv);
    gmCookieManager.setCookieValue(res, "chGrpAllZone", strGrpAllZone);
    gmCookieManager.setCookieValue(res, "chGrpAllRegn", strGrpAllRegn);
    gmCookieManager.setCookieValue(res, "chGrpAllDist", strGrpAllDist);
    gmCookieManager.setCookieValue(res, "chDistFilterUnChk", strDistFilterUnChk);

    return sbWhereCondtion;
  }


  public StringBuffer getHierarchyWhereConditionT501(HashMap hmParam) {
    StringBuffer sbWhereCondtion = new StringBuffer();
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("STRCONDITION"));
    String strCompFilter = GmCommonClass.parseNull((String) hmParam.get("COMP_FILTER"));
    String strDivFilter = GmCommonClass.parseNull((String) hmParam.get("DIV_FILTER"));
    String strZoneFilter = GmCommonClass.parseNull((String) hmParam.get("ZONE_FILTER"));
    String strRegnFilter = GmCommonClass.parseNull((String) hmParam.get("REGN_FILTER"));
    String strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DIST_FILTER"));
    String strTerrFilter = GmCommonClass.parseNull((String) hmParam.get("TERR_FILTER"));
    String strPgrpFilter = GmCommonClass.parseNull((String) hmParam.get("PGRP_FILTER"));
    String strDistFilterUnChk = GmCommonClass.parseNull((String) hmParam.get("DIST_UNCHK_FILTER"));
    String strPrimaryCmpyId = GmCommonClass.parseNull((String) hmParam.get("PRIMARY_CMPY"));
    String strBBASalesAcsFl = GmCommonClass.parseNull((String) hmParam.get("BBA_SALES_ACS_FL"));
    String strAddnlCompFl = GmCommonClass.parseNull((String) hmParam.get("ADDNL_COMP_IN_SALES"));
   log.debug("strCondition is***"+strCondition);
    sbWhereCondtion.append(strCondition);
    
    /*
     * Adding the company filter check. 100803 -- ALL
     * Only if the All company is selected below conditional code will get added
     * PMT-20656 :Field Inventory performance Improvement
     * Changed the IN CLAUSE to EXISTS CLAUSE. PMT-20656 Jun 2018
     * Author: gpalani Jun 2018
     */  
    
    if(strCompFilter.toString().equals("100803"))
    {
	 	 
    if (strAddnlCompFl.equals("Y")) 
    {

        if (!sbWhereCondtion.toString().equals(""))
        {

        	sbWhereCondtion.append(" AND ");
        }
        sbWhereCondtion.append("  EXISTS ");
        sbWhereCondtion.append("  (SELECT DISTINCT V700S.REP_ID ");
        sbWhereCondtion.append("  FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
        sbWhereCondtion.append("  WHERE V700S.REP_COMPID IN (" + strPrimaryCmpyId + ")");
        sbWhereCondtion.append("  AND V700S.REP_ID=T501.C703_SALES_REP_ID ) AND ");
        
        log.debug("***gmAccessFilter*** BBA User");
      } else if ((!strAddnlCompFl.equals("Y")) && (!strBBASalesAcsFl.equals("Y")))
      {

        if (!sbWhereCondtion.toString().equals("")) 
        {

        	sbWhereCondtion.append(" AND ");
        }
        log.debug("***gmAccessFilter GMNA*** No Access to BBA**V501");
        sbWhereCondtion.append("  EXISTS ");
        sbWhereCondtion.append("  (SELECT DISTINCT V700S.REP_ID ");
        sbWhereCondtion.append("  FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
        // PC-3905: to fix the sales report performance issue
        // Currently ADDNL_COMP_IN_SALES having only BBA company id - this causing query slowness.
        // So, we are removing the rules and hard code the values
        // sbWhereCondtion.append("  WHERE V700S.REP_COMPID NOT IN ( SELECT C906_RULE_ID FROM T906_RULES WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES' AND C906_VOID_FL IS NULL)");
        
        sbWhereCondtion.append("  WHERE V700S.REP_COMPID NOT IN (1001)");
        sbWhereCondtion.append("  AND V700S.REP_ID=T501.C703_SALES_REP_ID ) AND");
      }
 }   
    
    if(!strCompFilter.toString().equals("100803"))
    {
    	 if (!sbWhereCondtion.toString().equals("")) 
         {

         	sbWhereCondtion.append(" AND ");
         }
    }
  
  
    if (!strCompFilter.equals("")
        || (strRegnFilter.equals("") && strZoneFilter.equals("") && strDivFilter.equals(""))) {
      sbWhereCondtion.append("  EXISTS");
      sbWhereCondtion.append("  (SELECT V700S.ac_id ");
      sbWhereCondtion.append(" FROM v700_territory_mapping_detail V700S ");
      log.debug("***gmAccessFilter***strCompFilter");

      // The Code is Changed For PMT-5662 Which is to handle when the Company Dropdown is choosed as
      // All.
      if (strCompFilter.contains(",")) {
        sbWhereCondtion.append(" WHERE V700S.COMPID IN (" + strCompFilter + ")");

      } else {
        sbWhereCondtion.append(" WHERE (V700S.COMPID = DECODE(" + strCompFilter + ",100803, NULL,"
            + strCompFilter + ") OR DECODE(" + strCompFilter + ",100803,1,2) = 1)");
      }
      sbWhereCondtion.append("AND V700S.ac_id=T501.C704_ACCOUNT_ID");
 
      sbWhereCondtion.append(")");

    }
    if (!strRegnFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" EXISTS ");
      sbWhereCondtion.append(" (SELECT V700S.ac_id ");
      sbWhereCondtion.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
      sbWhereCondtion.append(" WHERE V700S.REGION_ID IN (" + strRegnFilter +")");
      sbWhereCondtion.append(" AND V700S.AC_ID=T501.C704_ACCOUNT_ID )");
      log.debug("***gmAccessFilter***strRegnFilter");

      
    }

    if (!strZoneFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" EXISTS ");
      sbWhereCondtion.append("  (SELECT V700S.AC_ID ");
      sbWhereCondtion.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
      sbWhereCondtion.append(" WHERE V700S.GP_ID IN (" + strZoneFilter + ") ");
      sbWhereCondtion.append(" AND V700S.AC_ID=T501.C704_ACCOUNT_ID )");
      log.debug("***gmAccessFilter***strZoneFilter");

    }

    if (!strDivFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" EXISTS (SELECT V700S.REP_ID ");
      sbWhereCondtion.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
      sbWhereCondtion.append(" WHERE V700S.DIVID IN (" + strDivFilter + ") ");
      sbWhereCondtion.append(" AND V700S.REP_ID=T501.C703_SALES_REP_ID )");
      log.debug("***gmAccessFilter***strDivFilter");



    }
    if (!strDistFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" EXISTS (SELECT V700S.REP_ID ");
      sbWhereCondtion.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
      sbWhereCondtion.append(" WHERE V700S.D_ID IN (" + strDistFilter + ") ");
      sbWhereCondtion.append(" AND V700S.REP_ID=T501.C703_SALES_REP_ID ) ");
      log.debug("***gmAccessFilter***strDistFilter");


      
    } else if (!strDistFilterUnChk.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" EXISTS (SELECT V700S.REP_ID ");
      sbWhereCondtion.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
      sbWhereCondtion.append(" WHERE V700S.D_ID NOT IN (" + strDistFilterUnChk+") ");
      sbWhereCondtion.append(" AND V700S.REP_ID=T501.C703_SALES_REP_ID )");
      log.debug("***gmAccessFilter***strDistFilterUnChk");


      
      		
    }
    if (!strTerrFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" EXISTS (SELECT V700S.REP_ID ");
      sbWhereCondtion.append(" FROM V700_TERRITORY_MAPPING_DETAIL V700S ");
      sbWhereCondtion.append(" WHERE V700S.TER_ID IN (" + strTerrFilter + ") ");
      sbWhereCondtion.append(" AND V700S.REP_ID=T501.C703_SALES_REP_ID )");
      log.debug("***gmAccessFilter***strTerrFilter");


    }
    if (!strPgrpFilter.equals("")) {
      strPgrpFilter = strPgrpFilter.replaceAll(",", "','");
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" EXISTS ");
      
      sbWhereCondtion.append(" (SELECT T208.C205_PART_NUMBER_ID FROM T208_SET_DETAILS T208 , ");
      sbWhereCondtion.append("   T207_SET_MASTER T207 WHERE T208.C207_SET_ID = T207.C207_SET_ID ");
      sbWhereCondtion.append("   AND T207.C901_SET_GRP_TYPE = 1600 ");
      sbWhereCondtion.append("   AND T208.C205_PART_NUMBER_ID=T502.C205_PART_NUMBER_ID ");      
      sbWhereCondtion.append(" AND T207.C207_SET_ID IN ('");
      sbWhereCondtion.append(strPgrpFilter);
      sbWhereCondtion.append("') )");
      log.debug("***gmAccessFilter***strPgrpFilter");

    }
    return sbWhereCondtion;

  }

  /*
   * This method is used to add the access condition to separate the Globus sales from others as
   * well as it will also add an access for the respective user to see only their data.
   */
  public StringBuffer getHierarchyWhereConditionV700(HttpServletRequest req,
      HttpServletResponse res, String strCondition) {

    StringBuffer sbWhereCondtion = new StringBuffer();
    // To get the Session Information
    HttpSession session = req.getSession(false);

    // Globus, Algea etc.. sales access
    String strRegnFilter = "";
    String strCompFilter = "";
    String strDivFilter = "";
    String strZoneFilter = "";
    String strDistFilter = "";
    String strTerrFilter = "";
    String strPgrpFilter = "";
    String strSessCompId = "";
    String strSessDivId = "";
    String strCookieFlag = "";
    String strCurrType = "";
    String strCurrSymbol = "";
    String strSessCurrType = "";
    String strCCurrSymbol = "";
    String strGrpAllDiv = "";
    String strGrpAllZone = "";
    String strGrpAllRegn = "";
    String strGrpAllDist = "";
    String strDistFilterUnChk = "";

    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();

    log.debug("strSessCompId in v700"+strSessCompId);
    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();
    // Flag value to call the getCurrSymbolByFilter method.
    Boolean blFlag = getCurrFromCookie(req, res, gmCookieManager);

    // Get from request or from cookie if not found
    strRegnFilter = GmCommonClass.parseNull(req.getParameter("hRegnFilter"));
    strCompFilter = GmCommonClass.parseNull(req.getParameter("hCompFilter"));
    strDivFilter = GmCommonClass.parseNull(req.getParameter("hDivFilter"));
    strZoneFilter = GmCommonClass.parseNull(req.getParameter("hZoneFilter"));
    strDistFilter = GmCommonClass.parseNull(req.getParameter("hDistFilter"));
    strTerrFilter = GmCommonClass.parseNull(req.getParameter("hTerrFilter"));
    strPgrpFilter = GmCommonClass.parseNull(req.getParameter("hPgrpFilter"));
    String strSessPrimCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
    strSessCompId= GmCommonClass.parseNull((String) session.getAttribute("strSessCompId"));

    strSessDivId = GmCommonClass.parseNull((String) session.getAttribute("strSessDivId"));
    log.debug("strSessPrimCompId in v700"+strSessPrimCompId);
    
    String strBBASalesAcsFl =
            GmCommonClass.parseNull((String) session.getAttribute("strSessBBASalesAcsFl"));

        String strAddnlCompFl =
                GmCommonClass.parseNull((String) session.getAttribute("strSessAddnlCompFl"));
    
    
    strCookieFlag = GmCommonClass.parseNull(req.getParameter("hCookieFlag"));
    strCurrType = GmCommonClass.parseNull(req.getParameter("CurrType"));
    strSessCurrType = GmCommonClass.parseNull((String) session.getAttribute("strSessCurrType"));
    strGrpAllDiv = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Div"));
    strGrpAllZone = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Zone"));
    strGrpAllRegn = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Regn"));
    strGrpAllDist = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Dist"));
    strDistFilterUnChk = GmCommonClass.parseNull(req.getParameter("hDistFilterUnChk"));

    // while clicking the Go/Load button at that time we no need to get filter values from cookies.
    if (!strCookieFlag.equals("NO")) {
      strRegnFilter =
          strRegnFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chRegnFilter")) : strRegnFilter;
      strCompFilter =
          strCompFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chCompFilter")) : strCompFilter;
      strDivFilter =
          strDivFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chDivFilter")) : strDivFilter;
      strZoneFilter =
          strZoneFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chZoneFilter")) : strZoneFilter;
      strDistFilter =
          strDistFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chDistFilter")) : strDistFilter;
      strTerrFilter =
          strTerrFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chTerrFilter")) : strTerrFilter;
      strGrpAllDiv =
          strGrpAllDiv.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllDiv")) : strGrpAllDiv;
      strGrpAllZone =
          strGrpAllZone.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllZone")) : strGrpAllZone;
      strGrpAllRegn =
          strGrpAllRegn.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllRegn")) : strGrpAllRegn;
      strGrpAllDist =
          strGrpAllDist.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllDist")) : strGrpAllDist;
      strDistFilterUnChk =
          strDistFilterUnChk.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              req, "chDistFilterUnChk")) : strDistFilterUnChk;

      // Commented because causing an issue for the Field Inventory report.
      // strPgrpFilter = strPgrpFilter.equals("") ?
      // GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "chPgrpFilter")) :
      // strPgrpFilter;
      strSessCompId =
          strSessCompId.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "cstrSessCompId")) : strSessCompId;
      strSessCurrType =
          strSessCurrType.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "CCurrType")) : strSessCurrType;
    }

    strCompFilter = strCompFilter.equals("") ? strSessCompId : strCompFilter; // Globus Medical Inc.
    strDivFilter = strDivFilter.equals("") ? strSessDivId : strDivFilter; // US Div.
    strCurrType = strCurrType.equals("") ? strSessCurrType : strCurrType;

    hmParam.put("COMP_FILTER", strCompFilter);
    hmParam.put("DIV_FILTER", strDivFilter);
    hmParam.put("ZONE_FILTER", strZoneFilter);
    hmParam.put("REGN_FILTER", strRegnFilter);
        // If distributor select all value is checked , we are getting values based on division,region
    // and zone
    // so we passing distributor filter as empty
    if (strGrpAllDist.equals("Y")) {
      hmParam.put("DIST_FILTER", "");
    } else {
      hmParam.put("DIST_FILTER", strDistFilter);
    }
    hmParam.put("TERR_FILTER", strTerrFilter);
    hmParam.put("STRCONDITION", strCondition);
    hmParam.put("CURRTYPE", strCurrType);
    hmParam.put("REGN_CONDITION", strCondition);
    // If distributor checked value is greater than dist value, we are passing unchecked values
    hmParam.put("DIST_UNCHK_FILTER", strDistFilterUnChk);
    hmParam.put("ADDNL_COMP_IN_SALES", strAddnlCompFl);
    hmParam.put("SHOW_ADNL_COMP_SALES", strBBASalesAcsFl);
    hmParam.put("PRIMARY_CMPY", strSessPrimCompId);
    sbWhereCondtion = this.getHierarchyWhereConditionV700(hmParam);

    strCCurrSymbol = GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "CCurrSymbol"));
    // Decoding the Encoded cookie value.
    try {
      strCCurrSymbol = URLDecoder.decode(strCCurrSymbol, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    // If the regions from the request and cookie are not same Or the Currency symbol is not present
    // in the Cookie get the Currency Symbol from the DB Call else get it from the Cookie.
    if (blFlag.equals(true) || strCCurrSymbol.equals("")) {

      hmReturn = this.getCurrSymbolByFilter(session, hmParam);

      strCurrType = GmCommonClass.parseNull((String) hmReturn.get("CURR_TYPE"));
      strCurrSymbol = GmCommonClass.parseNull((String) hmReturn.get("CURR_SYMBOL"));

    } else {

      strCurrSymbol = strCCurrSymbol;
    }


    req.setAttribute("hRegnFilter", strRegnFilter);
    req.setAttribute("hCompFilter", strCompFilter);
    req.setAttribute("hDivFilter", strDivFilter);
    req.setAttribute("hZoneFilter", strZoneFilter);
    req.setAttribute("hDistFilter", strDistFilter);
    req.setAttribute("hTerrFilter", strTerrFilter);
    req.setAttribute("hPgrpFilter", strPgrpFilter);
    req.setAttribute("hCurrSymb", strCurrSymbol);
    req.setAttribute("hCurrType", strCurrType);
    req.setAttribute("hCondition", strCondition);
    req.setAttribute("strGrpAllDiv", strGrpAllDiv);
    req.setAttribute("strGrpAllZone", strGrpAllZone);
    req.setAttribute("strGrpAllRegn", strGrpAllRegn);
    req.setAttribute("strGrpAllDist", strGrpAllDist);
    req.setAttribute("hDistFilterUnChk", strDistFilterUnChk);

    session.setAttribute("strSessCompId", strCompFilter);
    session.setAttribute("strSessDivId", strDivFilter);
    session.setAttribute("strSessCurrType", strCurrType);
    session.setAttribute("strSessCurrSymb", strCurrSymbol);
    session.setAttribute("strSessCondition", strCondition);
    // Set Cookies
    gmCookieManager.setCookieValue(res, "chRegnFilter", strRegnFilter);
    gmCookieManager.setCookieValue(res, "chCompFilter", strCompFilter);
    gmCookieManager.setCookieValue(res, "chDivFilter", strDivFilter);
    gmCookieManager.setCookieValue(res, "chZoneFilter", strZoneFilter);
    gmCookieManager.setCookieValue(res, "chDistFilter", strDistFilter);
    gmCookieManager.setCookieValue(res, "chTerrFilter", strTerrFilter);
    gmCookieManager.setCookieValue(res, "chPgrpFilter", strPgrpFilter);
    gmCookieManager.setCookieValue(res, "cstrSessCompId", strSessCompId);
    gmCookieManager.setCookieValue(res, "CCurrType", strCurrType);
    gmCookieManager.setCookieValue(res, "CCondition", strCondition);
    gmCookieManager.setCookieValue(res, "chGrpAllDiv", strGrpAllDiv);
    gmCookieManager.setCookieValue(res, "chGrpAllZone", strGrpAllZone);
    gmCookieManager.setCookieValue(res, "chGrpAllRegn", strGrpAllRegn);
    gmCookieManager.setCookieValue(res, "chGrpAllDist", strGrpAllDist);
    gmCookieManager.setCookieValue(res, "chDistFilterUnChk", strDistFilterUnChk);

    // Cookies does not allow ASCII codes , so need to encode with UTF-8.
    try {
      strCurrSymbol = URLEncoder.encode(strCurrSymbol, "UTF-8");
    } catch (UnsupportedEncodingException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    gmCookieManager.setCookieValue(res, "CCurrSymbol", strCurrSymbol);

    return sbWhereCondtion;
  }

  /*
   * This method is used to add the access condition to separate the Globus sales from others as
   * well as it will also add an access for the respective user to see only their data.
   */
  public StringBuffer getHierarchyWhereConditionV700(HashMap hmParam) {
    StringBuffer sbWhereCondtion = new StringBuffer();
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("STRCONDITION"));
    String strCompFilter = GmCommonClass.parseNull((String) hmParam.get("COMP_FILTER"));
    String strDivFilter = GmCommonClass.parseNull((String) hmParam.get("DIV_FILTER"));
    String strZoneFilter = GmCommonClass.parseNull((String) hmParam.get("ZONE_FILTER"));
    String strRegnFilter = GmCommonClass.parseNull((String) hmParam.get("REGN_FILTER"));
    String strDistFilter = GmCommonClass.parseNull((String) hmParam.get("DIST_FILTER"));
    String strTerrFilter = GmCommonClass.parseNull((String) hmParam.get("TERR_FILTER"));
    String strDistFilterUnChk = GmCommonClass.parseNull((String) hmParam.get("DIST_UNCHK_FILTER"));
    String strPrimaryCmpyId = GmCommonClass.parseNull((String) hmParam.get("PRIMARY_CMPY"));
    String strBBASalesAcsFl = GmCommonClass.parseNull((String) hmParam.get("BBA_SALES_ACS_FL"));
    String strAddnlCompFl = GmCommonClass.parseNull((String) hmParam.get("ADDNL_COMP_IN_SALES"));


    sbWhereCondtion.append(strCondition);
    log.debug("strCompFilter"+strCompFilter);
    log.debug("strAddnlCompFl"+strAddnlCompFl);
    log.debug("strBBASalesAcsFl"+strBBASalesAcsFl);

    log.debug("strPrimaryCmpyId in v700**"+strPrimaryCmpyId);


  
    if (!strCondition.equals("")) {
      sbWhereCondtion.append(" AND ");
    }

    if (!strCompFilter.equals("")
        || (strRegnFilter.equals("") && strZoneFilter.equals("") && strDivFilter.equals(""))) {
      // The Code is Changed For PMT-5662 Which is to handle when the Company Dropdown is choosed as
      // All.
      if (strCompFilter.contains(",")) {
        sbWhereCondtion.append("  COMPID IN (" + strCompFilter + ")");

      } else {
        sbWhereCondtion.append("  ((COMPID = DECODE(" + strCompFilter + ",100803, NULL,"
            + strCompFilter + ") OR DECODE(" + strCompFilter + ",100803,1,2) = 1)");
      }
      sbWhereCondtion.append(")");
    }

    if (!strRegnFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" REGION_ID IN (" + strRegnFilter + ") ");
    }
    if (!strZoneFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" GP_ID IN (" + strZoneFilter + ") ");
    }
    if (!strDivFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" DIVID IN (" + strDivFilter + ") ");
    }
    if (!strDistFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" D_ID IN (" + strDistFilter + ") ");
    } else if (!strDistFilterUnChk.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" D_ID NOT IN (" + strDistFilterUnChk + ") ");
    }
    if (!strTerrFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" TER_ID IN (" + strTerrFilter + ") ");
    }
    /*
    * Adding the company filter check. 100803 -- ALL
    * Only if the All company is selected below conditional code will get added
    * PMT-20656 :Field Inventory performance Improvement
    * Author: gpalani Jun 2018
    */     
    if(strCompFilter.toString().equals(("100803")))
   	{
    	
           // If the users primary company is 1001(BBA) adding filter condition to only fetch BBA(400643)
           // company sales Or else if the primary company is not 1001(BBA) and not having the access to
           // see BBA company sales then filtering BBA(400643) company sales for the users.
           if (strAddnlCompFl.equals("Y")) 
           {
        	   
        	      log.debug("***gmAccessFilter***BBA User");

             if (!sbWhereCondtion.toString().equals("")) 
             {

         	  sbWhereCondtion.append(" AND ");
             }
                          
            sbWhereCondtion.append("  V700.REP_COMPID IN (" + strPrimaryCmpyId + ") ");
           }
           
           else if ((!strAddnlCompFl.equals("Y")) && (!strBBASalesAcsFl.equals("Y"))) 
           {
               log.debug("***gmAccessFilter GMNA*** No Access to BBA**v700");

              if (!sbWhereCondtion.toString().equals("")) 
             {

         	  sbWhereCondtion.append(" AND ");
             }
              // PC-3905: to fix the sales report performance issue
              // Currently ADDNL_COMP_IN_SALES having only BBA company id - this causing query slowness.
              // So, we are removing the rules and hard code the values 
             //sbWhereCondtion.append(" V700.REP_COMPID  NOT IN ( SELECT C906_RULE_ID FROM T906_RULES WHERE C906_RULE_GRP_ID = 'ADDNL_COMP_IN_SALES' AND C906_VOID_FL IS NULL ) ");
              
             sbWhereCondtion.append(" V700.REP_COMPID  NOT IN (1001) ");
          
           }
   	}
 

    return sbWhereCondtion;

  }

  /*
   * This method is used to add the access condition to separate the Globus sales from others as
   * well as it will also add an access for the respective user to see only their data.
   */
  public StringBuffer getHierarchyWhereConditionT907(HttpServletRequest req,
      HttpServletResponse res, String strCondition) {
    StringBuffer sbWhereCondtion = new StringBuffer();
    // To get the Session Information
    HttpSession session = req.getSession(false);

    // Globus, Algea etc.. sales access
    String strRegnFilter = "";
    String strCompFilter = "";
    String strDivFilter = "";
    String strZoneFilter = "";
    String strDistFilter = "";
    String strTerrFilter = "";
    String strPgrpFilter = "";
    String strSessCompId = "";
    String strSessDivId = "";
    String strCookieFlag = "";
    String strGrpAllDiv = "";
    String strGrpAllZone = "";
    String strGrpAllRegn = "";
    String strGrpAllDist = "";
    String strDistFilterUnChk = "";

    // Get Settings from cookie
    GmCookieManager gmCookieManager = new GmCookieManager();

    // Get from request or from cookie if not found
    strRegnFilter = GmCommonClass.parseNull(req.getParameter("hRegnFilter"));
    strCompFilter = GmCommonClass.parseNull(req.getParameter("hCompFilter"));
    strDivFilter = GmCommonClass.parseNull(req.getParameter("hDivFilter"));
    strZoneFilter = GmCommonClass.parseNull(req.getParameter("hZoneFilter"));
    strDistFilter = GmCommonClass.parseNull(req.getParameter("hDistFilter"));
    strSessCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompId"));
    strSessDivId = GmCommonClass.parseNull((String) session.getAttribute("strSessDivId"));
    strCookieFlag = GmCommonClass.parseNull(req.getParameter("hCookieFlag"));
    strGrpAllDiv = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Div"));
    strGrpAllZone = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Zone"));
    strGrpAllRegn = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Regn"));
    strGrpAllDist = GmCommonClass.parseNull(req.getParameter("Chk_Grp_ALL_Dist"));
    strDistFilterUnChk = GmCommonClass.parseNull(req.getParameter("hDistFilterUnChk"));

    // while clicking the Go/Load button at that time we no need to get filter values from cookies.
    if (!strCookieFlag.equals("NO")) {
      strRegnFilter =
          strRegnFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chRegnFilter")) : strRegnFilter;
      strCompFilter =
          strCompFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chCompFilter")) : strCompFilter;
      strDivFilter =
          strDivFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chDivFilter")) : strDivFilter;
      strZoneFilter =
          strZoneFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chZoneFilter")) : strZoneFilter;
      strDistFilter =
          strDistFilter.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chDistFilter")) : strDistFilter;
      strGrpAllDiv =
          strGrpAllDiv.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllDiv")) : strGrpAllDiv;
      strGrpAllZone =
          strGrpAllZone.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllZone")) : strGrpAllZone;
      strGrpAllRegn =
          strGrpAllRegn.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllRegn")) : strGrpAllRegn;
      strGrpAllDist =
          strGrpAllDist.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "chGrpAllDist")) : strGrpAllDist;
      strDistFilterUnChk =
          strDistFilterUnChk.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(
              req, "chDistFilterUnChk")) : strDistFilterUnChk;

      // Commented because causing an issue for the Field Inventory report.
      strSessCompId =
          strSessCompId.equals("") ? GmCommonClass.parseNull(gmCookieManager.getCookieValue(req,
              "cstrSessCompId")) : strSessCompId;
    }

    // If distributor select all value is checked , we are getting values based on division,region
    // and zone
    // so we passing distributor filter as empty
    if (strGrpAllDist.equals("Y")) {
      strDistFilter = "";
    }

    req.setAttribute("hRegnFilter", strRegnFilter);
    req.setAttribute("hCompFilter", strCompFilter);
    req.setAttribute("hDivFilter", strDivFilter);
    req.setAttribute("hZoneFilter", strZoneFilter);
    req.setAttribute("hDistFilter", strDistFilter);
    req.setAttribute("strGrpAllDiv", strGrpAllDiv);
    req.setAttribute("strGrpAllZone", strGrpAllZone);
    req.setAttribute("strGrpAllRegn", strGrpAllRegn);
    req.setAttribute("strGrpAllDist", strGrpAllDist);
    req.setAttribute("hDistFilterUnChk", strDistFilterUnChk);

    sbWhereCondtion.append(strCondition);

    strCompFilter = strCompFilter.equals("") ? strSessCompId : strCompFilter; // Globus Medical Inc.
    strDivFilter = strDivFilter.equals("") ? strSessDivId : strDivFilter; // US Div.
    session.setAttribute("strSessCompId", strCompFilter);
    session.setAttribute("strSessDivId", strDivFilter);

    if (!strCondition.equals("")) {
      sbWhereCondtion.append(" AND ");
    }

    if (!strCompFilter.equals("")
        || (strRegnFilter.equals("") && strZoneFilter.equals("") && strDivFilter.equals(""))) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" C901_COMPANY_ID IN (" + strCompFilter + ")");
    }

    if (!strRegnFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" C901_AREA_ID IN (" + strRegnFilter + ") ");
    }
    if (!strZoneFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" C901_ZONE_ID IN (" + strZoneFilter + ") ");
    }
    if (!strDivFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" C901_DIVISION_ID IN (" + strDivFilter + ") ");
    }
    if (!strDistFilter.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" C701_DISTRIBUTOR_ID IN (" + strDistFilter + ") ");
    } else if (!strDistFilterUnChk.equals("")) {
      sbWhereCondtion.append(" AND ");
      sbWhereCondtion.append(" C701_DISTRIBUTOR_ID NOT IN (" + strDistFilterUnChk + ") ");
    }

    // Set Cookies
    gmCookieManager.setCookieValue(res, "chRegnFilter", strRegnFilter);
    gmCookieManager.setCookieValue(res, "chCompFilter", strCompFilter);
    gmCookieManager.setCookieValue(res, "chDivFilter", strDivFilter);
    gmCookieManager.setCookieValue(res, "chZoneFilter", strZoneFilter);
    gmCookieManager.setCookieValue(res, "chDistFilter", strDistFilter);
    gmCookieManager.setCookieValue(res, "chTerrFilter", strTerrFilter);
    gmCookieManager.setCookieValue(res, "chGrpAllDiv", strGrpAllDiv);
    gmCookieManager.setCookieValue(res, "chGrpAllZone", strGrpAllZone);
    gmCookieManager.setCookieValue(res, "chGrpAllRegn", strGrpAllRegn);
    gmCookieManager.setCookieValue(res, "chGrpAllDist", strGrpAllDist);
    gmCookieManager.setCookieValue(res, "chDistFilterUnChk", strDistFilterUnChk);

    return sbWhereCondtion;
  }

  /*
   * This method will be a new method that will be written in the GmAccessFilter Class which will be
   * called primarily from Sales Dash board web service calls . This method will get the required
   * access filter that is subsequently used for all the existing Queries for Sales Dash board.
   * 
   * @param hmParam
   * 
   * @return HashMap
   */
  public HashMap getSalesAccessFilter(HashMap hmParam) {
    HashMap hmReturn = new HashMap();
    String strFilterCondition = "";
    String strAccessCondition = "";
    String strSalesFilter = GmCommonClass.parseZero((String) hmParam.get("SALES_FILTER"));

    // Below is used to get the sales filter with V700 sales filter condition. Eg. used for
    // loaner,returns.
    strFilterCondition = (new GmSalesDispatchAction()).getAccessFilter(hmParam);
    hmParam.put("STRCONDITION", strFilterCondition);
    strFilterCondition = (getHierarchyWhereConditionV700(hmParam)).toString();
    if (!strFilterCondition.equals("")) {
      hmReturn.put("AccessFilter", strFilterCondition);
    }

    // Below is used to get the sales filter with T501 sales filter condition. Eg. used for orders.
    strAccessCondition = (new GmServlet()).getAccessFilter(hmParam);
    hmParam.put("STRCONDITION", strAccessCondition);
    strAccessCondition = (getHierarchyWhereConditionT501(hmParam)).toString();
    if (!strAccessCondition.equals("")) {
      hmReturn.put("Condition", strAccessCondition);
    }

    hmReturn.put("CURRTYPE", GmCommonClass.parseNull((String) hmParam.get("CURRTYPE")));
    hmReturn.put("CURR_SYMBOL", GmCommonClass.parseNull((String) hmParam.get("CURR_SYMBOL")));

    return hmReturn;
  }

  /*
   * This method getCurrSymbolByFilter is used to get the currency type and currency symbol based on
   * the regions selected in More Filter
   * 
   * 
   * @param session,hmParam
   * 
   * @return HashMap
   */
  public HashMap getCurrSymbolByFilter(HttpSession session, HashMap hmParam) {

    GmCommonBean gmCommonBean = new GmCommonBean();

    HashMap hmReturn = new HashMap();

    String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strCondition = GmCommonClass.parseNull((String) hmParam.get("REGN_CONDITION"));
    String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    String strPrimaryCmpyId =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));

    String strSessRptCurrSymbol =
        GmCommonClass.parseNull((String) session.getAttribute("strSessRptCurrSymbol"));
    hmReturn =
        getCurrSymbolByCompany(hmParam, strCurrType, strCondition, strDeptId, strPrimaryCmpyId,
            strSessRptCurrSymbol);

    return hmReturn;
  }

  /*
   * getCurrFromCookie : Used to check, if user changed any filter on more filter, if it is so
   * return the flag value as true else false.
   * 
   * @param request,response,GmCookieManager
   * 
   * @return Boolean
   */

  public Boolean getCurrFromCookie(HttpServletRequest req, HttpServletResponse res,
      GmCookieManager gmCookieManager) {

    Boolean blFlag = false;

    String strRegnFilter = GmCommonClass.parseNull(req.getParameter("hRegnFilter"));
    String strDivFilter = GmCommonClass.parseNull(req.getParameter("hDivFilter"));
    String strZoneFilter = GmCommonClass.parseNull(req.getParameter("hZoneFilter"));
    String strCookieFlag = GmCommonClass.parseNull(req.getParameter("hCookieFlag"));
    String strCurrType = GmCommonClass.parseNull(req.getParameter("CurrType"));

    String strCRegnFilter =
        GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "chRegnFilter"));
    String strCDivFilter =
        GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "chDivFilter"));
    String strCZoneFilter =
        GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "chZoneFilter"));
    String strCCurrType = GmCommonClass.parseNull(gmCookieManager.getCookieValue(req, "CCurrType"));

    // When the Load function/ Drill down is called from the sales reports ,Cookie Flag will come as
    // "NO". Then check the Division,Zone,Region filters from the Request and Session are same. If
    // it is so return "true"(To get the Currency Symbol from DB) else return "false" (To get the
    // Currency Symbol from the Cookie).
    if (strCookieFlag.equals("NO")) {

      if ((!strRegnFilter.equals(strCRegnFilter)) || (!strDivFilter.equals(strCDivFilter))
          || (!strZoneFilter.equals(strCZoneFilter)) || (!strCurrType.equals(strCCurrType))) {

        blFlag = true;

      } else {

        blFlag = false;
      }
    } else {

      blFlag = false;
    }

    return blFlag;
  }


  /*
   * This method getCurrSymbolByFilterForIPad is used to get the currency type and currency symbol
   * based on the regions selected in More Filter
   * 
   * 
   * @param hmParam
   */
  public HashMap getCurrSymbolByCompany(HashMap hmParam, String strCurrType, String strCondition,
      String strDeptId, String strPrimaryCmpyId, String strSessRptCurrSymbol) {

    GmCommonBean gmCommonBean = new GmCommonBean();

    HashMap hmReturn = new HashMap();
    ArrayList alCurrList = new ArrayList();
    ArrayList alRegnList = new ArrayList();

    String strRegnList = "";
    // String strCurrType = GmCommonClass.parseNull((String) hmParam.get("CURRTYPE"));
    String strCurrSymbol = "";
    // String strCondition = GmCommonClass.parseNull((String) hmParam.get("REGN_CONDITION"));
    String strDivFilter = GmCommonClass.parseNull((String) hmParam.get("DIV_FILTER"));
    String strZoneFilter = GmCommonClass.parseNull((String) hmParam.get("ZONE_FILTER"));
    String strRegnFilter = GmCommonClass.parseNull((String) hmParam.get("REGN_FILTER"));
    // String strDeptId = GmCommonClass.parseNull((String) session.getAttribute("strSessDeptId"));
    /*
     * String strPrimaryCmpyId = GmCommonClass.parseNull((String)
     * session.getAttribute("strSessCompanyId"));
     */

    strCurrType = GmCommonClass.parseNull(strCurrType);
    strCondition = GmCommonClass.parseNull(strCondition);
    strDeptId = GmCommonClass.parseNull(strDeptId);
    strPrimaryCmpyId = GmCommonClass.parseNull(strPrimaryCmpyId);
    strSessRptCurrSymbol = GmCommonClass.parseNull(strSessRptCurrSymbol);

    // Based on the currency Type dropdown selection showing the Base/Original for the users who is
    // having the access (People other than sales dept will able to toggle the Currency Type)
    if (strPrimaryCmpyId.equals("1000") && (!strDeptId.equals("S")) && strCurrType.equals("105460")) {

      strCurrSymbol = strSessRptCurrSymbol;
      // GmCommonClass.parseNull((String) session.getAttribute("strSessRptCurrSymbol"));

    } else {

      // Check the selected region is (100824)OUS, else set the Currency Type as Base and Get the
      // Reporting
      // Currency symbol from Session
      if (strDivFilter.equals("100824")) {

        // If any regions where not selected, get the regions mapped for the users by using the
        // getRegnByZone method.
        if (strRegnFilter.equals("")) {

          // If Zone is selected append that zone ids into the condition to get the regions.
          if (!strZoneFilter.equals("")) {

            strCondition =
                strCondition.equals("") ? strCondition + " GP_ID IN (" + strZoneFilter + ") "
                    : strCondition + " AND GP_ID IN (" + strZoneFilter + ") ";
          }

          alRegnList = GmCommonClass.parseNullArrayList(gmCommonBean.getRegnByZone(strCondition));


          // Iterate the ArryList to get as comma seperated String
          if (alRegnList.size() != 0) {

            Iterator itrMap = alRegnList.iterator();

            while (itrMap.hasNext()) {

              HashMap hmMap = (HashMap) itrMap.next();
              // To get the region values
              String strValue = GmCommonClass.parseNull(String.valueOf(hmMap.get("ID")));
              strRegnList = strRegnList + strValue + ",";
            }

          }
          log.debug("strRegnList String ::::" + strRegnList);
          // removing the , in end of the String
          strRegnList = strRegnList.replaceAll(",$", "");

          hmParam.put("HREGN_FILTER", strRegnList);

        } else if (!strRegnFilter.equals("")) { // selected regions from more filter

          hmParam.put("HREGN_FILTER", strRegnFilter);
        }

        // Get the currency symbol based on the regions selected/in more filter.
        alCurrList =
            GmCommonClass.parseNullArrayList(gmCommonBean.getCurrencySymbolByRegn(hmParam));


        // If the regions selected/in the more filter has the same type of currency. i.e (For e.g
        // The
        // regions selected are Germany(�) and Netherlands(�) both the regions has same currency
        // then
        // they can see the sales in there original(�) currency, else if the selected regions
        // belongs
        // to multiple currency, user can see the sales in base($) currency ).
        if (alCurrList.size() == 1) {

          strCurrType = "105461"; // 105461 - Original

          // Iterate the ArryList to get as comma seperated String
          if (alCurrList.size() != 0) {

            Iterator itrMap = alCurrList.iterator();

            while (itrMap.hasNext()) {

              HashMap hmMap = (HashMap) itrMap.next();
              // to get the currency symbol
              String strValue = GmCommonClass.parseNull(String.valueOf(hmMap.get("CURRSMB")));
              strCurrSymbol = strCurrSymbol + strValue + ",";
            }
            log.debug(" strCurrSymbol :::" + strCurrSymbol);
          }
          // removing the , in end of the String
          strCurrSymbol = strCurrSymbol.replaceAll(",$", "");


        } else {

          strCurrType = "105460"; // 105460 - Base

          strCurrSymbol = strSessRptCurrSymbol;
          // GmCommonClass.parseNull((String) session.getAttribute("strSessRptCurrSymbol"));

        }

      } else {

        strCurrType = "105460"; // 105460 - Base

        strCurrSymbol = strSessRptCurrSymbol;
        // GmCommonClass.parseNull((String) session.getAttribute("strSessRptCurrSymbol"));

      }
    }

    hmReturn.put("CURR_TYPE", strCurrType);
    hmReturn.put("CURR_SYMBOL", strCurrSymbol);

    return hmReturn;
  }


}
