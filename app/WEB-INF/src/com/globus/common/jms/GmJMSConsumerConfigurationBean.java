/*
 * Name: GmJmsConfigurationBean.java Author: mmuthusamy
 */

package com.globus.common.jms;


import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmJMSConsumerConfigurationBean extends GmBean {
  public static Logger log = GmLogger
      .getInstance("com.globus.common.beans.GmJMSConsumerConfigurationBean");
  public static ResourceBundle rbJmsConfig;


  /**
   * Empty Constructor
   */

  public GmJMSConsumerConfigurationBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmJMSConsumerConfigurationBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }



  /**
   * getJmsConfig - This method returns a string from the JMS configuration Properties file based on
   * the input as a keyword
   * 
   * @param strKey - Keyword
   * @return String object
   * @throws AppError
   */

  public static String getJmsConfig(String strKey) throws AppError {
    if (rbJmsConfig == null) {
      rbJmsConfig = ResourceBundle.getBundle("properties.jms.JMSConsumerConfig");
    }
    // log.debug(" JMS values  " + rbJmsConfig.getString(strKey));
    return rbJmsConfig.getString(strKey);
  } // End of getJmsConfig Method


}// end of class GmJmsConfigurationBean
