package com.globus.common.jms;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;

import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;

public class GmQueueProducer {
  Logger log = GmLogger.getInstance(this.getClass().getName());


  private String JNDI_FACTORY;
  // *************** Connection Factory JNDI name *************************
  private String JMS_FACTORY;
  // *************** Queue Factory JNDI name *************************
  private String QUEUE;
  private String USRNAME;// User name for remote connection
  private String USRPASSWORD;// Password for remote connection

  private String MSGPROTOCOL;// Set the Protocol to use from e.g weblogic uses t3,t3s for SSL, Jboss
                             // uses "remote"
  private String MSGPORT;// Set port where the server will listen for incoming connection



  private QueueConnectionFactory qconFactory;
  private QueueConnection qcon;
  private QueueSession qsession;
  private QueueSender qsender;
  private Queue queue;
  private ObjectMessage msg;
  private InitialContext ic;
  private String strJMSClient;

  private InetAddress localHost;
  private String ipAddress;
  private String JMS_MSGPROTOCOL;
  private String JMS_MSGPORT;
  private String JMS_LOCALQUEUE;
  private String JMS_REMOTEQUEUE;

  // Instance Initializer block to initialize which Client to use.
  {

    // IF the value returned by JMS_CLIENT_TO_USE is JBOSS_REMOTE then its a remote connection to
    // Jboss else if WEBLOGIC is returned then its a local connection to weblogic else if
    // JBOSS_LOCAL then a local connection to Jboss
    strJMSClient = GmCommonClass.parseNull(GmCommonClass.getString("JMS_CLIENT_TO_USE"));
    ipAddress = GmCommonClass.parseNull(System.getProperty("JMS_REMOTE_SERVER_IP"));
    USRNAME = GmCommonClass.parseNull(System.getProperty("JMS_REMOTE_USERNAME"));
    USRPASSWORD = GmCommonClass.parseNull(System.getProperty("JMS_REMOTE_PASSWORD"));
    /*Load the Protocol and Message port from the Jms.properties files 
     */
    JMS_MSGPROTOCOL = GmCommonClass.parseNull(System.getProperty("JMS_MSG_PROTOCOL"));
    JMS_MSGPORT = GmCommonClass.parseNull(System.getProperty("JMS_MSG_PORT"));
    JMS_LOCALQUEUE = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_LOC_QUEUE"));
    JMS_REMOTEQUEUE = GmCommonClass.parseNull(GmCommonClass.getJmsConfig("JMS_RMT_QUEUE"));


    if (strJMSClient.equals("JBOSS_LOCAL")) {
      // *************** Queue Factory JNDI name *************************
      JMS_FACTORY = "/GlobusJMSConnectionFactory";
      QUEUE = JMS_LOCALQUEUE;
      MSGPROTOCOL = JMS_MSGPROTOCOL;
      MSGPORT = JMS_MSGPORT;
    } else if (strJMSClient.equals("WEBLOGIC")) {
      JNDI_FACTORY = "weblogic.jndi.WLInitialContextFactory";
      JMS_FACTORY = "jms/gmjmsconnectionfactory";
      QUEUE = "jms/gmqueue";
      MSGPROTOCOL = JMS_MSGPROTOCOL;
      MSGPORT = JMS_MSGPORT;
    } else if (strJMSClient.equals("JBOSS_REMOTE")) {
      // *************** Connection Factory JNDI name *************************
      JNDI_FACTORY = "org.jboss.naming.remote.client.InitialContextFactory";
      // *************** Queue Factory JNDI name *************************
      JMS_FACTORY = "jms/GlobusJMSRemoteConnectionFactory";
      QUEUE = JMS_REMOTEQUEUE;
      MSGPROTOCOL = JMS_MSGPROTOCOL;
      MSGPORT = JMS_MSGPORT;
    }

  }


  /**
   * init - Will Initialize and start the connection to the queue to which the
   * GmMessageTransferObject will be written asynchronously Use JMS_CLIENT_TO_USE switch to say
   * which client to use for which the values are WEBLOGIC or JBOSS_LOCAL For remote connection to
   * Jboss 7.1 , user name and password are required which are mapped to roles For same JVM
   * connection user name /password will not be required In createQueueSession(false,
   * Session.AUTO_ACKNOWLEDGE) false means that the session created to the Q is not transacted,
   * AUTO_ACKNOWLEDGE means the message has been delivered to JMS server throws
   * NamingException,JMSException
   */
  private void init(Context ctx, String queueName) throws NamingException, JMSException {

    qconFactory = (QueueConnectionFactory) ctx.lookup(JMS_FACTORY);// lookup the queue connection
                                                                   // factory

    // *************** Creating Queue Connection using the User name and Password
    // *************************
    if ((strJMSClient.equals("JBOSS_LOCAL")) || (strJMSClient.equals("WEBLOGIC"))) {
      qcon = qconFactory.createQueueConnection();
    } else {
      strJMSClient.equals("JBOSS_REMOTE");
      qcon = qconFactory.createQueueConnection(USRNAME, USRPASSWORD);
    }

    qsession = qcon.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);// create a queue session
    queue = (Queue) ctx.lookup(queueName);// lookup the queue object
    qsender = qsession.createSender(queue);// create a queue sender
    msg = qsession.createObjectMessage();// create a Object message
    qcon.start();// start the connection

  }

  /**
   * Send method which sends message to the queue
   * 
   * @throws JMSException
   */
  private void send(Object message) throws JMSException {
    msg.setObject((Serializable) message);
    qsender.send(msg);
  }

  /**
   * close method which closes all connection
   * 
   * @throws JMSException NamingException
   */
  private void close() throws JMSException, NamingException {

    if (qsender != null) {
      qsender.close();
    }
    if (qsession != null) {
      qsession.close();
    }
    if (qcon != null) {
      qcon.close();
    }
    /*
     * Mihir Closing ic(Initial context object) will initialize the context again and will not throw
     * Initial context exception when jboss JMS server is restarted and weblogic is not restarted.
     * If weblogic and jboss are restarted the exception will never come.
     */
    if (ic != null) {
      ic.close();

    }

  }

  /**
   * sendMessage method which is used by user to send message. This method does the following jobs
   * Initialize the context Initialize and start the connection to the Q Calls the private sendMsg
   * method to send message Closes all open connection
   * 
   * @throws AppError
   */
  public void sendMessage(Object msg, String strQueueNm) throws AppError {
    try {
      ic = getInitialContext();
      // log.debug("getInitialContext:");
      init(ic, strQueueNm);
      // log.debug("init :");
      sendMsg(msg);
      // log.debug("sendMsg:");
      close();
    } catch (Exception e) {
      String strErrorMsg = GmCommonClass.getExceptionStackTrace(e);
      log.error("Exception e " + strErrorMsg);
      throw new AppError(e);
    } finally {
      try {
        close();
      } catch (Exception e) {
        qsender = null;
        qsession = null;
        qcon = null;
        ic = null;

      }
    }
  }

  // If the sendMessage()method is calling without the stirng parameter, then need to pass the default queue
  public void sendMessage(Object msg) throws AppError {
    sendMessage(msg, QUEUE);
  }

  /**
   * Delegate method sendMsg call send method to send message
   * 
   * @throws IOException JMSException
   */
  private void sendMsg(Object msg) throws IOException, JMSException {
    send(msg);
  }

  /**
   * getInitialContext method initializes the context by setting factory class Fetches local host
   * for Weblogic Set user name Password for remote Q
   * 
   * @throws IOException JMSException
   */
  private InitialContext getInitialContext() throws NamingException, UnknownHostException {
    Hashtable env = new Hashtable();


    if (strJMSClient.equals("JBOSS_LOCAL")) {
      // Local host
      //PMT-33439-Apache & PHP Changes on ERP server (3rd Set)
      //localHost = InetAddress.getLocalHost();
      //ipAddress = localHost.getHostAddress();
      String ipAddress = System.getProperty("HOST_URL");
      log.debug("ipAddress"+ipAddress);//PMT-33439
      
    } else if (strJMSClient.equals("WEBLOGIC")) {
      env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
      // Local host
      //localHost = InetAddress.getLocalHost();
      //ipAddress = localHost.getHostAddress();
      String ipAddress = System.getProperty("HOST_URL");
      log.debug("ipAddress"+ipAddress);
    } else if (strJMSClient.equals("JBOSS_REMOTE")) {

      env.put(Context.INITIAL_CONTEXT_FACTORY, JNDI_FACTORY);
      // *************** UserName & Password for the Initial Context for JNDI *************** //
      env.put(Context.SECURITY_PRINCIPAL, USRNAME);
      env.put(Context.SECURITY_CREDENTIALS, USRPASSWORD);
      // localHost = InetAddress.getLocalHost();
      // ipAddress = localHost.getHostAddress();
      System.out.println("localhost and ip address --> " + localHost + "  - " + ipAddress);
      
    }
    env.put(Context.PROVIDER_URL, MSGPROTOCOL + "://" + ipAddress + ":" + MSGPORT);
    // log.debug("MSGPORT :"+MSGPORT);
    return new InitialContext(env);

  }
}
