package com.globus.common.jms;



import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;

/**
 * @author Mihir Patel
 * 
 */
public class GmConsumerUtil {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  public static final String JMS_CONSUMER_EX_MAIL_TEMPLATE = "GmJMSConsumerExceptionMail";;

  private String batchNo;
  private String consumerClass;
  private String jmsConsumerException;
  private HashMap hmAdditionalParam;



  /**
   * @return the batchNo
   */
  public String getBatchNo() {
    return batchNo;
  }


  /**
   * @param batchNo the batchNo to set
   */
  public void setBatchNo(String batchNo) {
    this.batchNo = batchNo;
  }


  /**
   * @return the consumerClass
   */
  public String getConsumerClass() {
    return consumerClass;
  }


  /**
   * @param consumerClass the consumerClass to set
   */
  public void setConsumerClass(String consumerClass) {
    this.consumerClass = consumerClass;
  }


  /**
   * @return the jmsConsumerException
   */
  public String getJmsConsumerException() {
    return jmsConsumerException;
  }


  /**
   * @param jmsConsumerException the jmsConsumerException to set
   */
  public void setJmsConsumerException(String jmsConsumerException) {
    this.jmsConsumerException = jmsConsumerException;
  }


  /**
   * @return the hmAdditionalParam
   */
  public HashMap getHmAdditionalParam() {
    return hmAdditionalParam;
  }


  /**
   * @param hmAdditionalParam the hmAdditionalParam to set
   */
  public void setHmAdditionalParam(HashMap hmAdditionalParam) {
    this.hmAdditionalParam = hmAdditionalParam;
  }



  /**
   * This method is called to send email when in case of any exception.
   * 
   * @param exception A JMS exception.
   */
   
  public void sendJMSConExceptionEmail(GmMessageTransferObject msgObject, HashMap hmReturn) {

    GmEmailProperties emailProps = new GmEmailProperties();
    String strConsumerClass = GmCommonClass.parseNull(msgObject.getConsumerClass());
    HashMap hmMessageTrans = new HashMap();
    hmMessageTrans = (HashMap) msgObject.getMessageObject();
    String strBatchID = GmCommonClass.parseNull((String) hmMessageTrans.get("BATCHID"));
    String strOrderID = GmCommonClass.parseNull((String) hmMessageTrans.get("ORDERID"));
    String strComapnyId = GmCommonClass.parseNull((String) hmMessageTrans.get("COMPANY_ID"));
    setBatchNo(strBatchID);
    setConsumerClass(strConsumerClass);

    Exception ex = (Exception) hmReturn.get("EXCEPTIONOBJ");
    String mdbClass = GmCommonClass.parseNull((String) hmReturn.get("MDBCLASS"));
    String strMailTo = GmCommonClass.parseNull((String) hmReturn.get("EXP_MAIL_TO"));//mail id from rule table
    //to set dynamic template for mail notification
    String strExcepJMSMailTemplate = GmCommonClass.parseNull((String) hmReturn.get("EXP_JMS_MAIL_TEMPLATE"));
    strExcepJMSMailTemplate = strExcepJMSMailTemplate.equals("")?JMS_CONSUMER_EX_MAIL_TEMPLATE:strExcepJMSMailTemplate;
    // String addParam = GmCommonClass.parseNull((String)hmAdditionalParam.get("PROCESSED"));

    try {


      ArrayList alMailParams = new ArrayList();
      HashMap hmMailParams = new HashMap();
      hmMailParams.put("BATCHNO", batchNo);
      hmMailParams.put("ORDERID", strOrderID);
      hmMailParams.put("CONSUMERCLASS", consumerClass);
      hmMailParams.put("HEADERPARAM", mdbClass);
      hmMailParams.put("COMPANY_ID", strComapnyId);
      hmMailParams.put("JMSCONSUMEREXCEPTION", GmCommonClass.getExceptionStackTrace(ex, "<br>"));
      hmMailParams.put("EXP_MAIL_TO", strMailTo);
      // hmMailParams.put("ADDITIONALPARAM", addParam);
      alMailParams.add(hmMailParams);

      GmCommonClass
          .sendEmailFromTemplate(strExcepJMSMailTemplate, alMailParams, hmMailParams);
      log.error("JMS Exception mail sent: ");
    } catch (Exception e) {
      log.error("Exception in sendJMSConExceptionEmail :");
      log.error(e.getMessage());
    }
  }
  

  /**
   * This method is called to Update the status for Item/Loaner/Set Initiated from Device. This is
   * used to reduce the approval time lag between portal and device.
   * 
   * @param exception A JMS exception.
   */
  public void sendMessage(HashMap hmParam) throws AppError {
    // Class name which will process the message
    String strConsumerClass = GmCommonClass.parseNull((String) hmParam.get("CONSUMERCLASS"));
    String strQueueName = GmCommonClass.parseNull((String) hmParam.get("QUEUE_NAME"));
    // This is JMS Code which will process the share
    GmQueueProducer qprod = new GmQueueProducer();
    GmMessageTransferObject tf = new GmMessageTransferObject();
    tf.setMessageObject(hmParam);
    tf.setConsumerClass(strConsumerClass);

    // to get the queue name from hash map - if Queue name present then pass the queue name
    if (strQueueName.equals("")) {
      qprod.sendMessage(tf);
    } else {
      qprod.sendMessage(tf, strQueueName);
    }
  }// End of sendMessage
}
