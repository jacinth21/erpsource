package com.globus.corporate.beans;

import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import oracle.jdbc.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmCorporateBean extends GmBean{
	 Logger log = GmLogger.getInstance(this.getClass().getName());// Code to

	  // Initialize
	  // the
	  // Logger
	  // Class.
	public GmCorporateBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	
	  /**
	   * This method load plant name based on Party ID info
	   * 
	   * @param
	   * @return
	   * @throws AppError the app error
	   */
	  public ArrayList loadPartyPlantMapping(String strPartyId,String strCompanyId) throws AppError {
	    ArrayList alResult = new ArrayList();
	    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
	    gmDBManager.setPrepareString("gm_pkg_op_plant_rpt.gm_fch_party_plant_mapping", 3);
	    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
	    gmDBManager.setString(1, strPartyId);
	    gmDBManager.setString(2, strCompanyId);
	    gmDBManager.execute();
	    alResult = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(3));
	    gmDBManager.close();
	    return alResult;
	  }
}

