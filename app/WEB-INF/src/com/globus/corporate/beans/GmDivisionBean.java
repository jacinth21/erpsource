package com.globus.corporate.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmDivisionBean extends GmBean{
	
	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	
	
	  /**
	   * Constructor will populate company info.
	   * 
	   * @param gmDataStore
	   */
	  public GmDivisionBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	  
	  
	  /**
	   * fetchDivision: This method is used to fetch the Division's for a company by passing company ID
	   * 
	   * @param HashMap hmParam - Having the company Id to fetch a divisions for that particular company.
	   * @return ArrayList alDivisionList - Having the list of divisions for a company.
	   * @exception AppError
	   */
	  
	  public ArrayList fetchDivision(HashMap hmParam) throws AppError{
		  
		  ArrayList alDivisionList = new ArrayList();
		  String strCompanyId = getCompId();
				  
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		  gmDBManager.setPrepareString("gm_pkg_cor_division_rpt.gm_fch_division", 2);
		  gmDBManager.setString(1, strCompanyId);
		  gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		  gmDBManager.execute();
		  
		  alDivisionList =GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		  gmDBManager.close();
		  
	  return alDivisionList;
		  
	  }


}
