package com.globus.corporate.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmCompanyBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmCompanyBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }


  public ArrayList loadCompanyInfo(String strCompanyID) throws AppError {
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    ArrayList alCompanyInfo = null;

    gmDBManager.setPrepareString("gm_pkg_cor_company_rpt.gm_fch_company_info", 2);
    gmDBManager.setString(1, strCompanyID);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.execute();

    alCompanyInfo =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));

    gmDBManager.close();

    return alCompanyInfo;
  }

  public HashMap fetchCompanyInfo(String strCompanyID) throws AppError {

    log.debug(" # #  strCompanyID" + strCompanyID);
    ArrayList alCompanyInfo = null;
    HashMap hmCompanyInfo = null;

    alCompanyInfo = this.loadCompanyInfo(strCompanyID);

    if (alCompanyInfo.size() > 0) {
      hmCompanyInfo = GmCommonClass.parseNullHashMap((HashMap) alCompanyInfo.get(0));
    }
    return hmCompanyInfo;
  }

  /**
   * This method fetchCompCurrMap will fetch company currency mapping detail.
   * 
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchCompCurrMap() throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cor_company_rpt.gm_fch_comp_currency_map", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull(getCompId()));
    gmDBManager.execute();
    ArrayList alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();

    return alReturn;
  }

  /**
   * This method fetchAssocCompMap will fetch Associated company mapping detail.
   * 
   * @return ArrayList
   * @throws AppError
   */
  public ArrayList fetchAssocCompMap() throws AppError {

    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_cor_company_rpt.gm_fch_assoc_comp_mapping_dtls", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, GmCommonClass.parseNull(getCompId()));
    gmDBManager.execute();
    ArrayList alReturn =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(2)));
    gmDBManager.close();

    return alReturn;
  }
}
