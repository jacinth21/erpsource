package com.globus.manufact.displaytag.beans;
import javax.servlet.jsp.PageContext;
import org.apache.commons.beanutils.DynaBean;
import org.displaytag.decorator.TableDecorator;
import java.util.HashMap;
import org.displaytag.model.TableModel;

import com.globus.common.beans.*;
import com.globus.manufact.forms.GmManufCostForm;

public class DTManufWOSummaryWrapper extends TableDecorator {
	private HashMap hmValues = new HashMap();
	private String strMfgWODhrId = "";
	private String strLotNumber = ""; 

	public DTManufWOSummaryWrapper() {
		super();
	}
	public void init(PageContext context, Object decorated,	TableModel tableModel) {
		// TODO Auto-generated method stub
		super.init(context, decorated, tableModel);
		
	}
	public String getLOTNUM(){	    		
    	hmValues =   (HashMap) this.getCurrentRowObject();
    	strLotNumber = GmCommonClass.parseNull((String)hmValues.get("LOTNUM"));
    	strMfgWODhrId  = GmCommonClass.parseNull((String)hmValues.get("DHRID"));     	
        StringBuffer sbHtml = new StringBuffer();
        if(!strLotNumber.equals("") && !strMfgWODhrId.equals("")){    	
        	sbHtml.append("  <a href=javascript:fnPrintBOM('" + strMfgWODhrId + "')> <img width='14' height='14' title='View BOM Report' border='0' src=/images/b.jpg width=15 height=15></a>");
        }
        sbHtml.append("&nbsp;"+strLotNumber);        
        return sbHtml.toString();
    }  	    
}
