package com.globus.manufact.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.manufact.beans.GmManfBean;



public class GmMFGInitiateWOServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strAction = "";
    String strPartNum = "";
    String strMfgWO = "";
    String strOrdQty = "";
    String strOrdComplete = "";
    String strOrdPending = "";
    String strMfgQty = "";
    String strBOMid = "";
    String strVendId = "";
    String strMfgWOId = "";
    String strUserId = "";
    String strInputStr = "";
    String strMatReqId = "";
    String strQtyToManf = "";
    String strCompFl = "";
    String strLotCode = "";
    String strMfgLotCodeId = "";
    String strBOMId = "";
    String strErrorMessage = "";
    String strBOInput = "";
    String strExpDate = "";
    String strDonorNum = "";
    String strRWflag ="";
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strManfPath = GmCommonClass.getString("GMMANF");
    String strDispatchTo = strManfPath.concat("/GmMFGInitiateWO.jsp");
    RowSetDynaClass resultSet = null;
    boolean bolIsPartOfBom = false;
    HashMap hmParam = new HashMap();
    HashMap hmResult;
    HashMap hmMfgWO;
    HashMap hmPartDetails = new HashMap();
    ArrayList alMatReq;
    ArrayList alReturn;
    String strServletNm = "GmMFGInitiateWOServlet";
    String strInput = "";

    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      instantiate(request, response);
      GmManfBean gmMfgBean = new GmManfBean(getGmDataStoreVO());
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      strRWflag = GmCommonClass.parseNull(request.getParameter("rwflag"));
      strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;
      log.debug("strAction " + strAction);
      log.debug("strOpt " + strOpt);

      if (strAction.equals("FetchAddPart") || strAction.equals("AddPart")) {
        hmParam = new HashMap();
        strMfgWOId = GmCommonClass.parseNull(request.getParameter("hMfgWODHRId"));
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
        strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
        // strLotCode = GmCommonClass.parseNull((String)session.getAttribute("Txt_MfgWODHRId"));
        strMfgLotCodeId = GmCommonClass.parseNull(request.getParameter("Txt_MfgLotCodeId"));

        // Validating if the input parts are elements of the BOM set
        hmParam.put("MFGWODHRID", strMfgWOId);
        hmParam.put("INPUTSTR", strInputStr);
        hmParam.put("UID", strUserId);

        log.debug(" Input String for Adding Mat Req is " + strInputStr);
        bolIsPartOfBom = gmMfgBean.checkPartOfBom(strInputStr, strMfgWOId);
      }

      if (strAction.equals("Load") && strOpt.equals("REQNEW") || strAction.equals("Load")
          && strOpt.equals("REQREL")) {
        // --- Loaded all Material Request Info
        request.setAttribute("ACTION", "Load");
        request.setAttribute("OPT", strOpt);
        strDispatchTo = strManfPath.concat("/GmManfMatRequest.jsp");
      } else if (strAction.equals("Reload") && strOpt.equals("REQNEW")
          || strAction.equals("Reload") && strOpt.equals("REQREL")) {
        // --- Loading details for Material request ---
        // Getting the Material Request Id
        strMfgLotCodeId = GmCommonClass.parseNull(request.getParameter("Txt_MfgLotCodeId"));

        // Loading the Material Request Information
        hmMfgWO = gmMfgBean.loadMatReq(strMfgLotCodeId);
        // ArrayList of Material Requests to be displayed using Display Tag
        alMatReq = (ArrayList) hmMfgWO.get("MATREQDETAILS");
        // HashMap of Part Number details. This part is to be manufactured.
        hmPartDetails = (HashMap) hmMfgWO.get("PARTDETAILS");
        log.debug(" Part Details are " + hmPartDetails);
        strMatReqId = GmCommonClass.parseNull(request.getParameter("hMatReqId"));
        if (!strMatReqId.equals("")) {
          alReturn = gmMfgBean.loadSupplyRecon(strMatReqId, "MATREQ");
          request.setAttribute("MATREQITEMDETAILS", alReturn);
        }

        request.setAttribute("MATREQUEST", alMatReq);
        request.setAttribute("MFGWODHRID", strMfgWOId);
        request.setAttribute("MFGLOTCODEID", strMfgLotCodeId);
        request.setAttribute("PARTDETAILS", hmPartDetails);
        // --- Loaded all Material Request Info
        request.setAttribute("ACTION", strAction);
        request.setAttribute("OPT", strOpt);
        request.setAttribute("MATREQID", strMatReqId);
        strDispatchTo = strManfPath.concat("/GmManfMatRequest.jsp");
      } else if ((strAction.equals("Load") && strOpt.equals("WOINI")) || strAction.equals("Reload")
          && strOpt.equals("WOINI") || (strAction.equals("Load") && strOpt.equals("WOQRY"))
          || strAction.equals("Reload") && strOpt.equals("WOQRY")) {
        strMfgWO = GmCommonClass.parseNull(request.getParameter("Txt_MfgWO"));
        hmMfgWO = gmMfgBean.loadWO(strMfgWO);
        log.debug(" Values inside loaded WO is " + hmMfgWO);

        request.setAttribute("HMLOADWO", hmMfgWO);
        if (strOpt.equals("WOQRY")) {
          alReturn = gmMfgBean.loadWOStatus(strMfgWO);
          request.setAttribute("ALWOSTATUS", alReturn);
        }

        if (strAction.equals("Reload") && strOpt.equals("WOINI")) {
          strBOMId = GmCommonClass.parseNull((String) hmMfgWO.get("BOMID"));
          log.debug(" BOM Id is " + strBOMId);
          alReturn = gmMfgBean.loadBOMDetails(strBOMId);
          request.setAttribute("BOMDETAILS", alReturn);
        }

        request.setAttribute("HMLOADWO", hmMfgWO);
        request.setAttribute("MFGWO", strMfgWO);
        request.setAttribute("ACTION", strAction);
        request.setAttribute("OPT", strOpt);
      } else if (strAction.equals("InitiateWO")) {
        strMfgWO = GmCommonClass.parseNull(request.getParameter("Txt_MfgWO"));
        strPartNum = GmCommonClass.parseNull(request.getParameter("hPartNum"));
        strOrdQty = GmCommonClass.parseNull(request.getParameter("hOrdQty"));
        strOrdComplete = GmCommonClass.parseNull(request.getParameter("hOrdComplete"));
        strOrdPending = GmCommonClass.parseNull(request.getParameter("hOrdPending"));
        strMfgQty = GmCommonClass.parseNull(request.getParameter("Txt_MfgQty"));
        strExpDate = GmCommonClass.parseNull(request.getParameter("Txt_ExpDate"));
        strBOMid = GmCommonClass.parseNull(request.getParameter("Txt_BOMid"));
        strVendId = GmCommonClass.parseNull(request.getParameter("hVendId"));
        strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
        strBOInput = GmCommonClass.parseNull(request.getParameter("hboinputStr"));
        strInput = GmCommonClass.parseNull(request.getParameter("hinputStr"));
        strDonorNum = GmCommonClass.parseNull(request.getParameter("Txt_DonorNum"));// Get the donor
                                                                                    // number from
                                                                                    // the text box

        hmParam = new HashMap();
        hmResult = new HashMap();
        hmMfgWO = new HashMap();
        hmPartDetails = new HashMap();

        hmParam.put("MFGWOID", strMfgWO);
        hmParam.put("PARTNUM", strPartNum);
        hmParam.put("ORDQTY", strOrdQty);
        hmParam.put("ORDDONE", strOrdComplete);
        hmParam.put("ORDPEND", strOrdPending);
        hmParam.put("MFGQTY", strMfgQty);
        hmParam.put("EXPIRYDATE", strExpDate);
        hmParam.put("BOMID", strBOMid);
        hmParam.put("UID", strUserId);
        hmParam.put("VENDID", strVendId);
        hmParam.put("BOINPUTSTR", strBOInput);
        hmParam.put("INPUTSTR", strInput);
        hmParam.put("DONORNUM", strDonorNum); // Pass the donor number to save procedure by puting
                                              // in hashmap
        hmParam.put("RWFLAG", strRWflag);

        // Initating a new MFG Work Order
        hmResult = gmMfgBean.initiateWO(hmParam);

        // --- Loading details for Material request ---
        // Getting the Material Request Id
        strMfgWOId = GmCommonClass.parseNull((String) hmResult.get("MFGWODHRID"));
        strMfgLotCodeId = GmCommonClass.parseNull((String) hmResult.get("LOTCODE"));
        // Loading the Material Request Information
        hmMfgWO = gmMfgBean.loadMatReq(strMfgLotCodeId);
        // ArrayList of Material Requests to be displayed using Display Tag
        alMatReq = (ArrayList) hmMfgWO.get("MATREQDETAILS");
        // HashMap of Part Number details. This part is to be manufactured.
        hmPartDetails = (HashMap) hmMfgWO.get("PARTDETAILS");

        request.setAttribute("MATREQUEST", alMatReq);
        request.setAttribute("MFGWODHRID", strMfgWOId);
        request.setAttribute("PARTDETAILS", hmPartDetails);
        request.setAttribute("MFGLOTCODEID", strMfgLotCodeId);
        // --- Loaded all Material Request Info
        request.setAttribute("ACTION", "Reload");
        request.setAttribute("OPT", "REQREL");        
        strDispatchTo = strManfPath.concat("/GmManfMatRequest.jsp");
      } else if (strAction.equals("AddPart")) {
        /*
         * the boolean variable bolIsPartOfBom (initialized to false) is set in an IF condition few
         * lines above. Since it is used by actions - AddPart and FetchAddPart, I've moved it
         * separately Logic is to get the Input variables and compare it with the parts present in
         * the BOM. If they dont match, then bolIsPartOfBom is set to false. -- Joe
         */

        if (bolIsPartOfBom) {
          HashMap hmTemp = gmMfgBean.addMatReq(hmParam);
          String strNewMatReqId = (String) hmTemp.get("MFGWODHRID");
        }

        else {
          strErrorMessage = " One or More Parts are not part of the BOM. Please validate ";
        }

        hmMfgWO = gmMfgBean.loadMatReq(strMfgLotCodeId);
        // ArrayList of Material Requests to be displayed using Display Tag
        alMatReq = (ArrayList) hmMfgWO.get("MATREQDETAILS");
        // HashMap of Part Number details. This part is to be manufactured.
        hmPartDetails = (HashMap) hmMfgWO.get("PARTDETAILS");

        request.setAttribute("MATREQUEST", alMatReq);
        request.setAttribute("MFGLOTCODEID", strMfgLotCodeId);
        request.setAttribute("PARTDETAILS", hmPartDetails);
        // --- Loaded all Material Request Info
        request.setAttribute("ACTION", "Reload");
        request.setAttribute("OPT", strOpt);
        // Setting the Error Message
        request.setAttribute("ERRORMESSAGE", strErrorMessage);
        strDispatchTo = strManfPath.concat("/GmManfMatRequest.jsp");
      }

      else if (strAction.equals("FetchAddPart")) {
        /*
         * the boolean variable bolIsPartOfBom (initialized to false) is set in an IF condition few
         * lines above. Since it is used by actions - AddPart and FetchAddPart, I've moved it
         * separately Logic is to get the Input variables and compare it with the parts present in
         * the BOM. If they dont match, then bolIsPartOfBom is set to false. -- Joe
         */

        HashMap hmFetchAddPartReturn = new HashMap();
        hmMfgWO = gmMfgBean.loadMatReq(strMfgLotCodeId);
        // ArrayList of Material Requests to be displayed using Display Tag
        alMatReq = (ArrayList) hmMfgWO.get("MATREQDETAILS");
        // HashMap of Part Number details. This part is to be manufactured.
        hmPartDetails = (HashMap) hmMfgWO.get("PARTDETAILS");
        log.debug(" Part Details inside FetchAddPart are " + hmPartDetails);
        strMatReqId = GmCommonClass.parseNull(request.getParameter("hMatReqId"));

        if (bolIsPartOfBom) {
          // HashMap of Part Number details. This part is to be manufactured.
          // hmPartDetails = (HashMap)hmMfgWO.get("PARTDETAILS");
          hmFetchAddPartReturn = gmMfgBean.fetchAddPartDetails(strInputStr);
        }

        else {
          strErrorMessage = " One or More Parts are not part of the BOM. Please validate ";
        }

        request.setAttribute("MATREQUEST", alMatReq);
        request.setAttribute("MFGWODHRID", strMfgWOId);
        request.setAttribute("MFGLOTCODEID", strMfgLotCodeId);
        request.setAttribute("PARTDETAILS", hmPartDetails);
        request.setAttribute("hmFCHADDPART", hmFetchAddPartReturn);
        // --- Loaded all Material Request Info
        request.setAttribute("ACTION", "Reload");
        request.setAttribute("OPT", "FCHUPDPART");
        request.setAttribute("MATREQID", strMatReqId);
        // Setting the Error Message
        request.setAttribute("ERRORMESSAGE", strErrorMessage);
        strDispatchTo = strManfPath.concat("/GmManfMatRequest.jsp");

      }

      else if (strAction.equals("ReleasePart") || strAction.equals("ReleaseBO")) {
        hmParam = new HashMap();
        // strMfgWOId = GmCommonClass.parseNull(request.getParameter("Txt_MfgWODHRId"));
        strMfgLotCodeId = GmCommonClass.parseNull(request.getParameter("Txt_MfgLotCodeId"));
        strMatReqId = GmCommonClass.parseNull(request.getParameter("hMatReqId"));
        strInputStr = GmCommonClass.parseNull(request.getParameter("hInputStr"));
        strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
        strCompFl = request.getParameter("Chk_Complete") == null ? "30" : "40";// "1":"2";

        // hmParam.put("MFGWODHRID",strMfgWOId);
        hmParam.put("MFGLOTCODEID", strMfgLotCodeId);
        hmParam.put("MATREQID", strMatReqId);
        hmParam.put("COMPFL", strCompFl);
        hmParam.put("INPUTSTR", strInputStr);
        hmParam.put("UID", strUserId);

        log.debug(" Values inside Parameter for Releasing material req is " + hmParam);
        if (strAction.equals("ReleasePart")) {
          HashMap hmTemp = gmMfgBean.releaseMatReq(hmParam);
        }

        if (!strMatReqId.equals("")) {
          if (strAction.equals("ReleaseBO")) {
            gmMfgBean.releaseBO(hmParam);
          }
          alReturn = gmMfgBean.loadSupplyRecon(strMatReqId, "MATREQ");
          request.setAttribute("MATREQITEMDETAILS", alReturn);

        }


        // hmMfgWO = gmMfgBean.loadMatReq(strMfgWOId);
        hmMfgWO = gmMfgBean.loadMatReq(strMfgLotCodeId);
        // ArrayList of Material Requests to be displayed using Display Tag
        alMatReq = (ArrayList) hmMfgWO.get("MATREQDETAILS");
        // HashMap of Part Number details. This part is to be manufactured.
        hmPartDetails = (HashMap) hmMfgWO.get("PARTDETAILS");

        request.setAttribute("MATREQUEST", alMatReq);
        // request.setAttribute("MFGWODHRID", strMfgWOId);
        request.setAttribute("MFGLOTCODEID", strMfgLotCodeId);
        request.setAttribute("PARTDETAILS", hmPartDetails);
        request.setAttribute("MATREQID", strMatReqId);
        // --- Loaded all Material Request Info
        strAction = "Reload";
        strOpt = "REQREL";
        request.setAttribute("ACTION", strAction);
        request.setAttribute("OPT", strOpt);
        strDispatchTo = strManfPath.concat("/GmManfMatRequest.jsp");
      }
      /*
       * strOpt.equals("MATREQDET") this piece of code is added for PMT-2016. as this is common
       * block for Add Suppliers left link and for this PMT, we are just differentiating for
       * PMT-2016 as we are showing N/A for Meterial Request ID in print BOM Screen.
       */
      else if (strAction.equals("PrintBOM") || strOpt.equals("MATREQDET")) {
        HashMap hmTemp = new HashMap();
        HashMap hmProductInfo = new HashMap();
        strMatReqId = GmCommonClass.parseNull(request.getParameter("hMatReqId"));
        strMfgWOId = GmCommonClass.parseNull(request.getParameter("hDHRId"));
        if (strOpt.equals("MATREQDET"))
          strMatReqId = "";
        log.debug(" MatReqid " + strMatReqId + " Wo ID " + strMfgWOId);
        hmTemp = gmMfgBean.getBOMPrintDetails(strMatReqId, strMfgWOId);
        log.debug(" Value of hmTemp is " + hmTemp);
        resultSet = (RowSetDynaClass) hmTemp.get("BOMINFO");
        hmProductInfo = (HashMap) hmTemp.get("BOMPRODUCTINFO");
        request.setAttribute("hmProductInfo", hmProductInfo);
        request.setAttribute("BOMREPORT", resultSet);
        request.setAttribute("hMatReqId", strMatReqId);
        strQtyToManf = GmCommonClass.parseNull(request.getParameter("hQtyToManf"));
        request.setAttribute("hQtyToManf", strQtyToManf);
        request.setAttribute("hOpt", strOpt);
        strDispatchTo = strManfPath.concat("/GmBOMPrint.jsp");
      }
      request.setAttribute("RWFLAG",strRWflag );
      log.debug(" value of strDispatchTo " + strDispatchTo);
      dispatch(strDispatchTo, request, response);
    }

    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch

  }
}
