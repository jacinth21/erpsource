package com.globus.manufact.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.manufact.beans.GmManfBean;

public class GmMFGMatTransServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strManfPath = GmCommonClass.getString("GMMANF");
    String strDispatchTo = strManfPath.concat("/GmMFGTransMat.jsp");

    String strAction = "";
    String strOpt = "";
    String strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    String strIds = "";

    ArrayList alReturn;
    HashMap hmReturn = new HashMap();
    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      instantiate(request, response);
      GmManfBean gmMfgBean = new GmManfBean(getGmDataStoreVO());
      strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      if (strAction.equals("Load")) {
        alReturn = gmMfgBean.loadPendingMatTransfers();
        hmReturn.put("REPORT", alReturn);
        request.setAttribute("ACTION", "Load");
        request.setAttribute("OPT", strOpt);
        request.setAttribute("hmReturn", hmReturn);
      } else if (strAction.equals("Transfer")) {
        strIds = GmCommonClass.parseNull(request.getParameter("hIds"));
        strIds = gmMfgBean.saveMatTransfers(strIds, strUserId);

        alReturn = gmMfgBean.loadPendingMatTransfers();
        hmReturn.put("REPORT", alReturn);
        request.setAttribute("ACTION", "Load");
        request.setAttribute("OPT", strOpt);
        request.setAttribute("hmReturn", hmReturn);
      }
      dispatch(strDispatchTo, request, response);
    }

    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch

  }// End of Service
} // End of GmMFGMatTransServlet
