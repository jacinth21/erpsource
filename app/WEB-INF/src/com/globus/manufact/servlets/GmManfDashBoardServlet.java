/*****************************************************************************
 * File : GmManfDashBoardServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.manufact.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.manufact.beans.GmManfBean;
import com.globus.manufact.beans.GmManfDashboardBean;



public class GmManfDashBoardServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);

    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strManfPath = GmCommonClass.getString("GMMANF");
    String strDispatchTo = strManfPath.concat("/GmManfHome.jsp");

    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
      instantiate(request, response);
      String strAction = request.getParameter("hAction");
      strAction = (strAction == null) ? "Load" : strAction;

      String strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;


      GmManfBean gmManf = new GmManfBean(getGmDataStoreVO());
      GmManfDashboardBean gmDash = new GmManfDashboardBean(getGmDataStoreVO());
      HashMap hmReturn = new HashMap();
      ArrayList alReturn = new ArrayList();

      if (strAction.equals("Load") && strOpt.equals("")) {
        hmReturn = gmManf.reportDashboard();
        request.setAttribute("hmReturn", hmReturn);
      } else if (strAction.equals("Add") || strAction.equals("Edit")) {
        String strUserId = (String) session.getAttribute("strSessUserId");
      } else if (strAction.equals("Load") && strOpt.equals("MATREQ")) {
        alReturn = gmDash.loadPendingMatRequests();
        request.setAttribute("MATREQUEST", alReturn);
        strDispatchTo = strManfPath.concat("/GmManfDashBrdMatReq.jsp");
      }
      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method
}// End of GmManfDashBoardServlet
