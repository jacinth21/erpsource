/*****************************************************************************
 * File			 : GmManfFrameServlet
 * Desc		 	 : 
 *                 
 * Version	 	 : 1.0
 ******************************************************************************/
package com.globus.manufact.servlets;
import com.globus.common.beans.*;
import com.globus.common.servlets.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.naming.*;
import java.util.*;

import com.globus.common.beans.GmLogger;
import org.apache.log4j.Logger;



public class GmManfFrameServlet extends GmServlet
{

	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);

	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);

		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strRootPath = GmCommonClass.getString("GMROOT");
		String strDispatchTo = "";
		String strPageToLoad = "";
		String strSubMenu = "";
		String strFrom = "";
		String strId = "";
		String strMode = "";
		String strPortal = "";
		try
		{
			checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
			Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 

				strFrom = request.getParameter("hFrom");

				if (strFrom != null && strFrom.equals("Dashboard"))
				{
					strId = request.getParameter("hId");
					session.setAttribute("strSessConsignId",strId);
					session.setAttribute("hAction","EditLoadDash");
				}
				else if (strFrom != null && strFrom.equals("VendorReport"))
				{
					strId = request.getParameter("hId");
					session.setAttribute("strSessVendorId",strId);
					session.setAttribute("hAction","EditLoad");
				}
				else if (strFrom != null && strFrom.equals("DashboardDHR"))
				{
					strId = request.getParameter("hId");
					strMode = request.getParameter("hMode");
					strPortal = request.getParameter("hPortal");
					
					session.setAttribute("strSessDHRId",strId);
					session.setAttribute("strSessMode",strMode);
					// Adding the Page details to identify the origin of the request and hence displaying error message accordingly in GmDHRUpdate.jsp
					session.setAttribute("FromPage",strFrom);
					session.setAttribute("FromPortal",strPortal);
					session.setAttribute("hAction","UpdateDHR");
				}

				strPageToLoad = request.getParameter("hPgToLoad");
				
				strSubMenu = request.getParameter("hSubMenu");

				strPageToLoad = (strPageToLoad == null)?"Blank":strPageToLoad;
				strSubMenu = (strSubMenu == null)?"Home":strSubMenu;
				
				session.setAttribute("strSessPgToLoad",strPageToLoad);
				session.setAttribute("strSessSubMenu",strSubMenu);
				if (strFrom != null)
				{
					gotoPage("/".concat(strPageToLoad),request,response);
				}else
				{
					gotoPage(strRootPath.concat("/GmManufacturing.jsp"),request,response);
				}
				
				
				//gotoPage(strRootPath.concat("/GmManufacturing.jsp"),request,response);

		}// End of try
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
	}//End of service method
}// End of  GmManfFrameServlet