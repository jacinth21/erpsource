package com.globus.manufact.servlets;

import com.globus.common.beans.*;
import com.globus.common.servlets.*;
import com.globus.manufact.beans.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;

public class GmDocumetSetUpServlet extends GmServlet {
	
		public void init(ServletConfig config) throws ServletException
		{
			super.init(config);
		}

	  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
		{
			HttpSession session = request.getSession(false);
			String strComnPath = GmCommonClass.getString("GMCOMMON");
			String strManfPath = GmCommonClass.getString("GMMANF");
			GmCommonBean gmCommonBean = new GmCommonBean();
			HashMap hmUpdateHistory = new HashMap();
			String strDispatchTo = "";
			String strAction = "";
			String strDocIdToUpdate = "";

			try
			{
					checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
					GmLogger log = GmLogger.getInstance();
					
					strAction = request.getParameter("hAction");
					if (strAction == null)
					{
						strAction = (String)session.getAttribute("hAction");
					}
					strAction = (strAction == null)?"Load":strAction;
					
					HashMap hmReturn = new HashMap();
					HashMap hmParam = new HashMap();
					GmDocumentBean gmDocBean = new GmDocumentBean();
					
					if (strAction.equals("Load"))
					{
						hmReturn = gmDocBean.loadDocDetails();	
						request.setAttribute("hmReturn",hmReturn);
					}
					
					if (strAction.equals("Save") || strAction.equals("Update"))
					{
						String strDocId = GmCommonClass.parseNull(request.getParameter("Txt_DocId"));
						String strDocDesc = GmCommonClass.parseNull(request.getParameter("Txt_DocDesc"));
						String strDocCategory = GmCommonClass.parseNull(request.getParameter("Cbo_Cat"));
						String strDocType = GmCommonClass.parseNull(request.getParameter("Cbo_Type"));
						String strActiveFlag = GmCommonClass.parseNull(request.getParameter("Txt_AFl"));
						String strRevNumber = GmCommonClass.parseNull(request.getParameter("Txt_Rev"));
						String strUserId = GmCommonClass.parseNull((String)session.getAttribute("strSessUserId"));
						String strDocSeqNo = GmCommonClass.parseNull(request.getParameter("hDocSeqNo"));
						
						if (strActiveFlag.equals("on"))
							strActiveFlag ="Y";
					
						hmParam.put("DOCID",strDocId);
						hmParam.put("DOCDESC",strDocDesc);
						hmParam.put("DOCCAT",strDocCategory);
						hmParam.put("DOCTYPE",strDocType);
						hmParam.put("ACTIVEFLAG",strActiveFlag);
						hmParam.put("REVNUM",strRevNumber);
						hmParam.put("UID",strUserId);
						hmParam.put("ACTION",strAction);
						hmParam.put("DOCSEQNO",strDocSeqNo);
						
						
						hmReturn = gmDocBean.saveDocDetails(hmParam); 
						request.setAttribute("PARAM",hmParam);
						hmReturn = gmDocBean.loadDocDetails();
						request.setAttribute("hmReturn",hmReturn);
						request.setAttribute("hAction",strAction);
					}
					
					else if (strAction.equals("Lookup"))
					{
						String strDocId = GmCommonClass.parseNull(request.getParameter("Txt_DocId"));
						hmParam = gmDocBean.loadEditDocInfo(strDocId);
						log.debug(" Value of hmParam in LookUp function is " + hmParam);
						request.setAttribute("PARAM",hmParam);
						hmReturn = gmDocBean.loadDocDetails();
						request.setAttribute("hmReturn",hmReturn);
						strAction = "Update";
						request.setAttribute("hAction",strAction);
					}
					
					
					// Including code for adding Updation History
					strDocIdToUpdate = GmCommonClass.parseNull(request.getParameter("Txt_DocId"));
					hmUpdateHistory = gmCommonBean.getUpdateHistory("T210_DOCUMENT","C210_DOC_ID",strDocIdToUpdate);
					request.setAttribute("UPDHISTORY",hmUpdateHistory);
					
					dispatch(strManfPath.concat("/GmSetDocument.jsp"),request,response);
			}
			
			catch (Exception e)
			{
					session.setAttribute("hAppErrorMessage",e.getMessage());
					strDispatchTo = strComnPath + "/GmError.jsp";
					gotoPage(strDispatchTo,request,response);
			}// End of catch
			
		}
}