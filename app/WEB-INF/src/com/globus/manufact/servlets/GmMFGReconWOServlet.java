package com.globus.manufact.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.manufact.beans.GmManfBean;



public class GmMFGReconWOServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strManfPath = GmCommonClass.getString("GMMANF");
    String strDispatchTo = strManfPath.concat("/GmMFGReconWO.jsp");

    String strAction = "";
    String strOpt = "";
    // String strMfgWOId = "";
    String strMfgLotCodeId = "";
    String strUserId = GmCommonClass.parseNull((String) session.getAttribute("strSessUserId"));
    String strSessCompanyLocale =GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    String strReturnStr = "";
    String strScrapStr = "";
    String strIds = "";
    String strQtyCompleted = "";
    String strLotCodeReconcFlag = "";
    String strLotReconcilationStatus = "";

    HashMap hmMfgWO;
    HashMap hmPartDetails;
    ArrayList alMatReq;
    ArrayList alReturn;
    HashMap hmParam = new HashMap();
    ArrayList alResult;

    String strPNum = "";
    String strLNum = "";
    String strWOID = "";
    String strDHRID = "";
    String strTransID = "";
    String strWOStat = "";
    String strDHRStat = "";
    String strTransStat = "";
    String strTransType = "";
    String strWOType = "";
    String strRWflag = "";
    String strGridXmlData = "";   
    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.
      instantiate(request, response);
      GmManfBean gmMfgBean = new GmManfBean(getGmDataStoreVO());
      strRWflag = GmCommonClass.parseNull(request.getParameter("rwflag"));
      strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      strOpt = request.getParameter("hOpt");
      if (strOpt == null) {
        strOpt = (String) session.getAttribute("strSessOpt");
      }
      strOpt = (strOpt == null) ? "" : strOpt;

      log.debug(" Action value is " + strAction + " Opt value is " + strOpt);

      if (strAction.equals("Load") && strOpt.equals("LoadWOSum")) {
        alResult = GmCommonClass.getCodeList("MTTYP");
        request.setAttribute("TRANSTYPES", alResult);
        strDispatchTo = strManfPath.concat("/GmManfWOSummary.jsp");
      } else if (strAction.equals("Reload") && strOpt.equals("LoadWOSum")) {
        hmParam = new HashMap();
        strPNum = GmCommonClass.parseNull(request.getParameter("Txt_PNum"));
        strLNum = GmCommonClass.parseNull(request.getParameter("Txt_LNum"));
        strWOID = GmCommonClass.parseNull(request.getParameter("Txt_WOID"));
        strDHRID = GmCommonClass.parseNull(request.getParameter("Txt_DHRID"));
        strTransID = GmCommonClass.parseNull(request.getParameter("Txt_TransID"));
        strWOStat = GmCommonClass.parseNull(request.getParameter("Cbo_WOStat"));
        strDHRStat = GmCommonClass.parseNull(request.getParameter("Cbo_DHRStat"));
        strTransStat = GmCommonClass.parseNull(request.getParameter("Cbo_TransStat"));
        strTransType = GmCommonClass.parseNull(request.getParameter("Cbo_TransType"));
        strWOType = GmCommonClass.parseNull(request.getParameter("Cbo_WOType"));

        hmParam.put("PNUM", strPNum);
        hmParam.put("LNUM", strLNum);
        hmParam.put("WOID", strWOID);
        hmParam.put("DHRID", strDHRID);
        hmParam.put("TRANSID", strTransID);
        hmParam.put("WOSTAT", strWOStat);
        hmParam.put("DHRSTAT", strDHRStat);
        hmParam.put("TRANSTAT", strTransStat);
        hmParam.put("TRANSTYPE", strTransType);
        hmParam.put("WOTYPE", strWOType);
        hmParam.put("RWFLAG", strRWflag);
        request.setAttribute("PARAMS", hmParam);

        alResult = gmMfgBean.fetchWorkOrderSummary(hmParam);
        hmParam.put("REPORT", alResult);
        hmParam.put("gmDataStoreVO", getGmDataStoreVO());
        hmParam.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
        strGridXmlData = gmMfgBean.getXmlGridData(hmParam);
        request.setAttribute("GRIDXMLDATAWO", strGridXmlData);
        request.setAttribute("alResult", alResult);
        request.setAttribute("ACTION", "Reload");
        alResult = GmCommonClass.getCodeList("MTTYP");
        request.setAttribute("TRANSTYPES", alResult);
        strDispatchTo = strManfPath.concat("/GmManfWOSummary.jsp");
      } else if (strAction.equals("Load") && strOpt.equals("LoadWOStat")) {
        hmParam = new HashMap();
        alResult = gmMfgBean.fetchWorkOrderStatus(hmParam);
        request.setAttribute("alResult", alResult);
        strDispatchTo = strManfPath.concat("/GmManfWOStatusReport.jsp");
      } else if (strAction.equals("Load")) {
        // --- Loaded all Material Request Info
        request.setAttribute("ACTION", "Load");
        request.setAttribute("OPT", strOpt);
        strDispatchTo = strManfPath.concat("/GmMFGReconWO.jsp");
      } else if (strAction.equals("Reload")) {
        // --- Loading details for Material request ---
        // Getting the Material Request Id
        strMfgLotCodeId = GmCommonClass.parseNull(request.getParameter("Txt_MfgLotCodeId"));
        // Loading the Material Request Information
        hmMfgWO = gmMfgBean.loadMatReq(strMfgLotCodeId);
        // ArrayList of Material Requests to be displayed using Display Tag
        alMatReq = (ArrayList) hmMfgWO.get("MATREQDETAILS");
        // HashMap of Part Number details. This part is to be manufactured.
        hmPartDetails = (HashMap) hmMfgWO.get("PARTDETAILS");

        strDHRID = GmCommonClass.parseNull((String) hmPartDetails.get("DHRID"));
        if (!strMfgLotCodeId.equals("")) {
          alReturn = gmMfgBean.loadSupplyRecon(strMfgLotCodeId, "MATRECON");
          // strLotReconcilationStatus = gmMfgBean.getMfgLotReconcStatus(strMfgLotCodeId);
          strLotReconcilationStatus =
              GmCommonClass.parseNull(gmMfgBean.getMfgLotReconcStatus(strDHRID)); // Changed on
          // 4/14/2015 James
          log.debug(" strLotReconcilationStatus is " + strLotReconcilationStatus);
          request.setAttribute("MATREQITEMDETAILS", alReturn);

        }
        String strLotCodeStatus =
            GmCommonClass.parseNull(gmMfgBean.getMfgLotReqStatus(strMfgLotCodeId, "MIN"));
        log.debug(" Status of the LotCode is " + strLotCodeStatus);

        if (!strLotCodeStatus.equals("")) {
          if (Integer.parseInt(strLotCodeStatus) < 2)
            strLotCodeReconcFlag = "ON";
        }
        if (!strLotReconcilationStatus.equals("")) {
          if (Integer.parseInt(strLotReconcilationStatus) > 0)
            strLotReconcilationStatus = "ON";
        }
        request.setAttribute("LOTRECONCSTATUS", strLotReconcilationStatus);
        request.setAttribute("MATREQUEST", alMatReq);
        request.setAttribute("LOTCODERECONCFLAG", strLotCodeReconcFlag);
        request.setAttribute("MFGLOTCODEID", strMfgLotCodeId);
        request.setAttribute("PARTDETAILS", hmPartDetails);
        // --- Loaded all Material Request Info
        request.setAttribute("ACTION", strAction);
        request.setAttribute("OPT", strOpt);
        strDispatchTo = strManfPath.concat("/GmMFGReconWO.jsp");
      } else if (strAction.equals("Reconcile")) {
        strMfgLotCodeId = GmCommonClass.parseNull(request.getParameter("Txt_MfgLotCodeId"));
        strQtyCompleted = GmCommonClass.parseNull(request.getParameter("Txt_QtyComp"));
        strReturnStr = GmCommonClass.parseNull(request.getParameter("hReturnStr"));
        strScrapStr = GmCommonClass.parseNull(request.getParameter("hScrapStr"));
        hmParam.put("MFGLOTCODEID", strMfgLotCodeId);
        hmParam.put("QTYCOMP", strQtyCompleted);
        hmParam.put("RETURNSTR", strReturnStr);
        hmParam.put("SCRAPSTR", strScrapStr);
        hmParam.put("USERID", strUserId);
        log.debug(" Parameters to reconcile " + hmParam);
        strIds = gmMfgBean.saveWOReconcile(hmParam);
        log.debug("strIds from BEAN:" + strIds);

        StringBuffer sbMarkUp = new StringBuffer();
        sbMarkUp.append(" <tr> <td bgcolor=#666666 height=1 colspan=3></td> </tr> </table> <tr> ");
        sbMarkUp.append(" <td class=RightText HEIGHT=25 align=center> ");
        sbMarkUp.append(" Successfully Reconciled and the transactions ");
        sbMarkUp.append(strIds);
        sbMarkUp.append(" have been created ");
        sbMarkUp
            .append(" </td> </tr> <tr> <td bgcolor=#666666 height=1 colspan=3> </td> </tr> </table>	");

        request.setAttribute("RECONCTXNIDSMARKUP", sbMarkUp.toString());
        request.setAttribute("IDS", strIds);
        request.setAttribute("ACTION", strAction);
        strDispatchTo = strManfPath.concat("/GmMFGReconWO.jsp");
      }
      request.setAttribute("RWFLAG", strRWflag);
      dispatch(strDispatchTo, request, response);
    }

    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch

  }
}
