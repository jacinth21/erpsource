package com.globus.manufact.servlets;

import com.globus.common.beans.*;
import com.globus.common.servlets.*;
import com.globus.manufact.beans.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;

import com.globus.common.beans.GmLogger; 
import org.apache.log4j.Logger; 


public class GmMFGMaterialReconcServlet extends GmServlet {
	
	public void init(ServletConfig config) throws ServletException
	{
		super.init(config);
	}

  	public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		HttpSession session = request.getSession(false);
		String strAction = "";
		String strPartNum = "";
		String strMfgWO = "";
		String strOrdQty = "";
		String strOrdComplete = "";
		String strOrdPending = "";
		String strMfgQty = "";
		String strBOMid = "";
		String strVendId = "";
		String strMfgWOId = "";
		String strUserId = "";		
		String strInputStr = "";
		String strType = "MFGWODHR";
		
		String strComnPath = GmCommonClass.getString("GMCOMMON");
		String strManfPath = GmCommonClass.getString("GMMANF");
		String strDispatchTo =  strManfPath.concat("/GmMFGMaterialReconciliation.jsp");
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmManfBean gmMfgBean = new GmManfBean();
		
		HashMap hmParam;
		HashMap hmResult;
		HashMap hmMfgWO;
		HashMap hmPartDetails;
		ArrayList alSupplyRecon;
		ArrayList alMatReq;
		
		try
		{
				checkSession(response,session); // Checks if the current session is valid, else redirecting to SessionExpiry page
				Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
				
				
				strAction = request.getParameter("hAction");
				if (strAction == null)
				{
					strAction = (String)session.getAttribute("hAction");
				}
				strAction = (strAction == null)?"Load":strAction;
								
				if (strAction.equals("LoadWO"))
				{
					strMfgWO = GmCommonClass.parseNull(request.getParameter("Txt_MfgWO"));
					hmMfgWO = gmMfgBean.loadInternalWO(strMfgWO);
					log.debug(" value inside HashMap in MFG WO Initiate Servlet is " +hmMfgWO );
					 alSupplyRecon = gmMfgBean.loadSupplyRecon(strMfgWO,strType);
					// alSupplyRecon = gmMfgBean.loadSupplyRecon(strMfgWO);
					log.debug(" Size of ArrayList alSupplyRecon in Reconc Servlet is " +alSupplyRecon );
					request.setAttribute("HMLOADWO", hmMfgWO);
					request.setAttribute("ALSUPRECON", alSupplyRecon);
					request.setAttribute("MFGWO", strMfgWO);
					request.setAttribute("ACTION", strAction);
				}
				dispatch(strDispatchTo,request,response);
		}
		
		catch (Exception e)
		{
				session.setAttribute("hAppErrorMessage",e.getMessage());
				strDispatchTo = strComnPath + "/GmError.jsp";
				gotoPage(strDispatchTo,request,response);
		}// End of catch
		
	}
}
