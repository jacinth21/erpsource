/*****************************************************************************
 * File : GmMFGSupplyUpdateServlet Desc :
 * 
 * Version : 1.0
 ******************************************************************************/
package com.globus.manufact.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.servlets.GmServlet;
import com.globus.common.util.GmTemplateUtil;
import com.globus.logon.beans.GmLogonBean;
import com.globus.manufact.beans.GmManfBean;
import com.globus.operations.beans.GmDHRBean;
import com.globus.operations.beans.GmOperationsBean;
import com.globus.operations.beans.GmVendorBean;



public class GmMFGSupplyUpdateServlet extends GmServlet {

  @Override
  public void init(ServletConfig config) throws ServletException {
    super.init(config);

  }

  @Override
  public void service(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    HttpSession session = request.getSession(false);
    String strComnPath = GmCommonClass.getString("GMCOMMON");
    String strManfPath = GmCommonClass.getString("GMMANF");
    String strDispatchTo = strManfPath.concat("/GmManfSupplyUpdate.jsp");

    try {
      Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                                   // Class.

      instantiate(request, response);

      String strAction = request.getParameter("hAction");
      if (strAction == null) {
        strAction = (String) session.getAttribute("hAction");
      }
      strAction = (strAction == null) ? "Load" : strAction;

      GmCommonClass gmCommon = new GmCommonClass();
      GmOperationsBean gmOper = new GmOperationsBean(getGmDataStoreVO());
      GmVendorBean gmVendor = new GmVendorBean(getGmDataStoreVO());
      GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
      GmManfBean gmManf = new GmManfBean(getGmDataStoreVO());

      HashMap hmReturn = new HashMap();
      HashMap hmParam = new HashMap();
      ArrayList alReturn = new ArrayList();
      String strDHRId = "";
      String strVendorId = "";
      String strUserId = (String) session.getAttribute("strSessUserId");
      String strMode = "";
      String strQtyInpsected = "";
      String strQtyRejected = "";
      String strInspectedBy = "";
      String strQty = "";
      String strUserIdTrans = "";
      String strPartNum = "";
      String strReason = "";
      String strReasonType = "";
      String strSterFl = "";
      String strNCMRId = "";
      int intRejCnt = 0;
      String strWOId = "";
      String strStatusFl = "";
      String strInputStr = "";
      String strSkipLabelFl = "";
      String strPartSterFl = "";
      String strLoc = "";
      String strSessCompanyLocale =
          GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
      String strUpdStatusFl = ""; // This value will contain if the Updation is happening at
                                  // Inspection / Receiving / Packaging
      String strPortal =
          request.getParameter("hPortal") == null ? "" : request.getParameter("hPortal");
      ArrayList alReturnReasons = new ArrayList();
      log.debug("strAction IN SERVLET:" + strAction);

      if (strAction.equals("Q") || strAction.equals("II")) // For QC acceptance & QC Inpsection
                                                           // Initiation
      {
        strDHRId = request.getParameter("hDHRId") == null ? "" : request.getParameter("hDHRId");
        strQty = request.getParameter("Txt_Qty") == null ? "" : request.getParameter("Txt_Qty");

        strDHRId = gmManf.updateDHRforQCActivity(strDHRId, strQty, strAction, strUserId);

        hmReturn = gmOper.getDHRDetails(strDHRId);
        alReturnReasons = gmCommon.getCodeList("NCRTP");
        alReturn = gmLogon.getEmployeeList();
        hmReturn.put("EMPLIST", alReturn);
        request.setAttribute("hNCMRReason", alReturnReasons);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strAction);
      } else if (strAction.equals("IC")) // QC Inspection Completion
      {
        strDHRId = GmCommonClass.parseNull(request.getParameter("hDHRId"));
        strQtyInpsected = GmCommonClass.parseNull(request.getParameter("Txt_QtyInsp"));
        strQtyRejected = GmCommonClass.parseNull(request.getParameter("Txt_QtyRej"));
        strInspectedBy = GmCommonClass.parseNull(request.getParameter("Cbo_InspEmp"));
        strReasonType = GmCommonClass.parseNull(request.getParameter("Cbo_ReasonType"));
        strUpdStatusFl = GmCommonClass.parseNull(request.getParameter("hStatusFl"));// This value
                                                                                    // will contain
                                                                                    // if the
                                                                                    // Updation is
                                                                                    // happening at
                                                                                    // Inspection /
                                                                                    // Receiving /
                                                                                    // Packaging

        strReason = GmCommonClass.parseNull(request.getParameter("Txt_RejReason"));
        strSterFl = "1";
        strStatusFl = "2";

        hmParam.put("QTYISNP", strQtyInpsected);
        hmParam.put("QTYREJ", strQtyRejected);
        hmParam.put("INSPBY", strInspectedBy);
        hmParam.put("REJREASONTYPE", strReasonType);
        hmParam.put("REJREASON", strReason);
        hmParam.put("STATUSFL", strStatusFl);
        hmParam.put("STERILEFL", strSterFl);
        hmParam.put("UPDSTATUSFL", strUpdStatusFl);

        intRejCnt = Integer.parseInt(strQtyRejected);
        hmReturn = gmOper.updateDHRforInsp(strDHRId, hmParam, strUserId);
        if (intRejCnt > 0) {
          strNCMRId = (String) hmReturn.get("NCMRID");
        }
        hmReturn = gmOper.getDHRDetails(strDHRId);
        alReturnReasons = gmCommon.getCodeList("NCRTP");

        alReturn = gmLogon.getEmployeeList();
        hmReturn.put("EMPLIST", alReturn);

        request.setAttribute("hNCMRReason", alReturnReasons);
        request.setAttribute("hNCMRId", strNCMRId);
        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strAction);
      } else if (strAction.equals("P") || strAction.equals("V")) // Package is not used now. Only
                                                                 // used for Verification
      {
        strDHRId = request.getParameter("hDHRId") == null ? "" : request.getParameter("hDHRId");
        strQty = request.getParameter("Txt_Qty") == null ? "" : request.getParameter("Txt_Qty");
        strUserIdTrans =
            request.getParameter("Cbo_Emp") == null ? "" : request.getParameter("Cbo_Emp");
        strPartNum =
            request.getParameter("hPartNum") == null ? "" : request.getParameter("hPartNum");
        strWOId = request.getParameter("hWOId") == null ? "" : request.getParameter("hWOId");
        strSkipLabelFl =
            request.getParameter("Chk_Skip") == null ? "" : request.getParameter("Chk_Skip");
        strSkipLabelFl = strSkipLabelFl.equals("on") ? "1" : "";
        strSterFl = request.getParameter("hSterFl") == null ? "" : request.getParameter("hSterFl");
        strPartSterFl =
            request.getParameter("hPartSterFl") == null ? "" : request.getParameter("hPartSterFl");
        strPartSterFl = strSterFl.equals("1") ? "" : strPartSterFl;
        strUpdStatusFl = GmCommonClass.parseNull(request.getParameter("hStatusFl"));// This value
                                                                                    // will contain
                                                                                    // if the
                                                                                    // Updation is
                                                                                    // happening at
                                                                                    // Inspection
                                                                                    // (1)/
                                                                                    // Receiving (3)
                                                                                    // /
                                                                                    // Packaging(2)
        String strInpStr = GmCommonClass.parseNull(request.getParameter("hInpStr"));;
        String strInputString = strQty;
        if (strAction.equals("P"))
          strInputString = strInpStr;

        hmParam.put("DHRID", strDHRId);
        hmParam.put("PARTNUM", strPartNum);
        hmParam.put("WOID", strWOId);
        hmParam.put("STRINPUTSTRING", strInputString);
        hmParam.put("SKIPLABELFLAG", strSkipLabelFl);
        hmParam.put("PARTSTERFLAG", strPartSterFl);
        hmParam.put("USERIDTRANS", strUserIdTrans);
        hmParam.put("ACTION", strAction);
        hmParam.put("UPDSTATUSFL", strUpdStatusFl);

        log.debug(" Values inside Parameter for Verification of DHR " + hmParam);
        // hmReturn =
        // gmOper.updateDHRforPackVerify(strDHRId,strPartNum,strWOId,strQty,strSkipLabelFl,strPartSterFl,strUserIdTrans,strAction,strUpdStatusFl);
        hmReturn = gmOper.updateDHRforPackVerify(hmParam);
        hmReturn = gmOper.getDHRDetails(strDHRId);

        alReturn = gmLogon.getEmployeeList();
        hmReturn.put("EMPLIST", alReturn);
        if (strAction.equals("V")) {
          strAction = "C";
        }

        request.setAttribute("hmReturn", hmReturn);
        request.setAttribute("hMode", strAction);
      }
      if (strPortal.equals("Manf")) {
        strLoc = "MWLC";
      } else if (strPortal.equals("Supply")) {
        strLoc = "SHLC";
      } else {
        strLoc = "DHRLC";
      }
      log.debug("strLoc ==>" + strLoc);
      hmParam.put("DHRID", strDHRId);
      hmParam.put("LOCGRP", strLoc);
      GmDHRBean gmDHRBean = new GmDHRBean(getGmDataStoreVO());
      ArrayList alLocations = gmDHRBean.fetchSplitLocations(hmParam);
      HashMap hmParams = new HashMap();
      hmParams.put("strAction", strAction);
      hmParams.put("STRSESSCOMPANYLOCALE ", strSessCompanyLocale);
      String strXmlData = getXmlGridData(alLocations, hmParams);
      request.setAttribute("XMLDATA", strXmlData);

      dispatch(strDispatchTo, request, response);

    }// End of try
    catch (Exception e) {
      session.setAttribute("hAppErrorMessage", e.getMessage());
      strDispatchTo = strComnPath + "/GmError.jsp";
      gotoPage(strDispatchTo, request, response);
    }// End of catch
  }// End of service method

  public String getXmlGridData(ArrayList alParam, HashMap hmParams) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParams.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alReturn", alParam);
    templateUtil.setDataMap("hmParams", hmParams);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.operations.GmLocationDisplay", strSessCompanyLocale));
    templateUtil.setTemplateName("GmLocationDisplay.vm");
    templateUtil.setTemplateSubDir("operations/templates");
    return templateUtil.generateOutput();
  }
}// End of GmMFGSupplyUpdateServlet
