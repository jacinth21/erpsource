package com.globus.manufact.beans;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.commons.beanutils.RowSetDynaClass;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;

public class GmDocumentBean {
	GmCommonClass gmCommon = new GmCommonClass();
	GmLogger log = GmLogger.getInstance(); // Gets an instance of Logger class
	
	/**
 	  * loadDistributor - This method will 
 	  * @param String strUsername
	  * @param String strPassword
 	  * @return HashMap
 	  * @exception AppError
 	**/
	public HashMap loadDocDetails() throws AppError
	{
		HashMap hmReturn = new HashMap();
		ArrayList alDoctCat = gmCommon.getCodeList("DOCCT");
		ArrayList alDoctype = gmCommon.getCodeList("DOCTP");
		hmReturn.put("DOCCATEGORY",alDoctCat);
		hmReturn.put("DOCTYPE",alDoctype);
		return hmReturn;
	}
	
	public HashMap saveDocDetails(HashMap hmParam) throws AppError
	{
		HashMap hmReturn = new HashMap();
		DBConnectionWrapper dbCon = null;
		dbCon = new DBConnectionWrapper();

		CallableStatement csmt = null;
		Connection conn = null;
		String strPrepareString = "";
		String strMsg = "";
		
		String strDocId = (String)hmParam.get("DOCID");
		String strDocDesc = (String)hmParam.get("DOCDESC");
		String strDocCategory = (String)hmParam.get("DOCCAT");
		String strDocType = (String)hmParam.get("DOCTYPE");
		String strActiveFlag = (String)hmParam.get("ACTIVEFLAG");
		String strRevNumber = (String)hmParam.get("REVNUM");
		String strUserId = (String)hmParam.get("UID");
		String strCurrDate = (String)hmParam.get("CURRDATE");
		String strAction = (String)hmParam.get("ACTION");
		String strDocSeqNo = (String)hmParam.get("DOCSEQNO"); 

		log.debug(" Values inside hmParam in the saveDocDetails inside Bean is " +  hmParam);
		
		try
		{
			conn = dbCon.getConnection();
			conn.setAutoCommit(false);

			strPrepareString = dbCon.getStrPrepareString("GM_SAVE_DOC_DETAILS",10);
			csmt = conn.prepareCall(strPrepareString);
			
			/*
			 *register out parameter and set input parameters
			*/
			csmt.registerOutParameter(10,java.sql.Types.CHAR);
			
			csmt.setString(1,strDocId);
			csmt.setString(2,strDocDesc);
			csmt.setString(3,strDocCategory);
			csmt.setString(4,strDocType);
			csmt.setString(5,strActiveFlag);
			csmt.setString(6,strRevNumber);
			csmt.setString(7,strUserId);
			csmt.setString(8,strAction);
			csmt.setString(9,strDocSeqNo);
			
			csmt.execute();
			
			strMsg = csmt.getString(10);
			conn.commit();
			
			log.debug("Value of MSG at Document Bean is " + strMsg);
			hmReturn.put("MSG",strMsg);
			
			
		}catch(Exception e)
		{
			try
			{
				conn.rollback();
			}
			catch (Exception sqle)
			{
				sqle.printStackTrace();
				throw new AppError(sqle);
			}
			
			GmLogError.log("Exception in GmCustomerBean:loadSetForReturns","Exception is:"+e);
			throw new AppError(e);
		}
		finally
		{
			try
			{
				if (csmt != null)
				{
					csmt.close();
				}//Enf of if  (csmt != null)
				if(conn!=null)
				{
					conn.close();		/* closing the Connections */
				}
			}
			catch(Exception e)
			{
				throw new AppError(e);
			}//End of catch
			finally
			{
				 csmt = null;
				 conn = null;
				 dbCon = null;
			}
		}
		return hmReturn;

	} //End of loadSetForReturns
	
	public HashMap loadEditDocInfo (String strDocId)throws AppError
	{
		HashMap hmResult = new HashMap();
		Connection con = null;
		java.sql.PreparedStatement pstmt = null;
		DBConnectionWrapper dbCon = null;
		
		try{
			
			dbCon = new DBConnectionWrapper();
			con = dbCon.getConnection();
			ResultSet rs = null;
			String str = null;
			ResultSetMetaData meta;
			ArrayList alistResult;
			
		    Statement stmt = null;
			
			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append(" SELECT C210_DOC_SEQ_NO DOCSEQNO, C210_DOC_ID DOCID, C210_DOC_DESC DOCDESC ");
			sbQuery.append(" , C901_DOC_CATEGORY DOCCAT, C901_DOC_TYPE DOCTYPE ");
			sbQuery.append(" , C210_ACTIVE_FL ACTIVEFLAG,  C210_REV_LVL REVNUM");
			sbQuery.append(" FROM T210_DOCUMENT ");
			sbQuery.append(" WHERE  C210_DOC_ID = ");
			sbQuery.append(" ? " );
			log.debug("Query inside loadEditDocInfo is " + sbQuery.toString());

			pstmt = con.prepareStatement(sbQuery.toString());
			pstmt.setString(1,strDocId);
			hmResult = (HashMap)dbCon.querySingleRecord(pstmt);
			con.close();
		}
		catch(Exception e)
		{
			throw new AppError(e);
		}//End of catch
		finally
		{
			 con = null;
			 dbCon = null;
		}
	// }
	return hmResult;
	}
}
