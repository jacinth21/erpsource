/*
 * Module: GmManfDashboardBean Author: Dhinakaran James Project: Globus Medical App Date-Written: 13
 * Sept 2006 Security: Unclassified Description: Testing Bean
 * 
 * Revision Log (mm/dd/yy initials description)
 * --------------------------------------------------------- mm/dd/yy xxx What you changed
 */

package com.globus.manufact.beans;

import java.sql.ResultSet;
import java.util.ArrayList;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;



public class GmManfDashboardBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  public GmManfDashboardBean(GmDataStoreVO gmDataStoreVO) {
    super(gmDataStoreVO);
  }


  /**
   * loadPendingMatRequests - This method will
   * 
   * @return HashMap
   * @exception AppError
   **/
  public ArrayList loadPendingMatRequests() throws AppError {
    ArrayList alReturn = new ArrayList();
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    String strPrepareString = null;
    gmDBManager.setPrepareString("GM_MF_FCH_MATREQ_DETAILS", 2);
    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
    gmDBManager.setString(1, "");
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
    gmDBManager.close();
    return alReturn;
  } // End of loadPendingMatRequests

  /**
   * loadPendingWorkOrders - This method will
   * 
   * @return ArrayList
   * @exception AppError
   **/
  public ArrayList loadPendingWorkOrders() throws AppError {
    ArrayList alReturn = new ArrayList();
    ResultSet rs = null;
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("GM_MF_REP_PEND_MFGWO", 1);
    gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
    gmDBManager.execute();
    alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
    gmDBManager.close();
    return alReturn;
  }
}// end of class GmManfDashboardBean
