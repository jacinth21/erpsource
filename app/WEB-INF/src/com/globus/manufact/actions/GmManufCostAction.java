package com.globus.manufact.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.manufact.beans.GmManfBean;
import com.globus.manufact.forms.GmManufCostForm;
import com.globus.prodmgmnt.beans.GmProjectBean;




public class GmManufCostAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName()); // Initializing
	HashMap hmParam=new HashMap();

	public ActionForward fetchCostingReports(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {   
		GmManufCostForm gmManufCostForm = (GmManufCostForm) form;
		gmManufCostForm.loadSessionParameters(request);
		GmManfBean gmManfBean = new GmManfBean();
		GmProjectBean gmProject = new GmProjectBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		
		HashMap hmAccess = new HashMap();
		RowSetDynaClass rdReport = null;
		HashMap hmProj = new HashMap();
		ArrayList alProject = new ArrayList();
		ArrayList alResult = new ArrayList();
		String strUserId = "";
		String strOpt = "";
		String strAccessFlag = "";
		
		hmParam = GmCommonClass.getHashMapFromForm(gmManufCostForm);
		 
		hmProj.put("COLUMN", "ID");
		hmProj.put("STATUSID", "20301"); // Filter all approved projects
		alProject = gmProject.reportProject(hmProj);
		
		gmManufCostForm.setAlProject(alProject);
			
		strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		strOpt = GmCommonClass.parseNull((String)hmParam.get("STROPT"));
			
		hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "MANUF_COST_EDIT"));  	
		strAccessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		log.debug(" Permissions Flag = " + hmAccess);
		if(strAccessFlag.equals("Y"))
		{
			gmManufCostForm.setStrEditAccess("Y");
		}
		
		if(strOpt.equals("Load")){
			rdReport = (RowSetDynaClass)gmManfBean.fetchCostingRpt(hmParam);
			gmManufCostForm.setRdReport(rdReport);
			alResult = (ArrayList) rdReport.getRows();
			gmManufCostForm.setGridData(generateOutPut(alResult,hmAccess));
		}
		
		return mapping.findForward("GmManufCostRpt");
    }
	
	public ActionForward editManufCost(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {   
		GmManufCostForm gmManufCostForm = (GmManufCostForm) form;
		gmManufCostForm.loadSessionParameters(request);
		GmManfBean gmManfBean = new GmManfBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

		HashMap hmReturn = new HashMap();
		HashMap hmAccess = new HashMap();
		String strOpt = "";
		String strOutMsg = "";
		String strAccessFlag = "";
		String strUserId = "";
		
		hmParam = GmCommonClass.getHashMapFromForm(gmManufCostForm);
		
		strOpt = GmCommonClass.parseNull((String)hmParam.get("STROPT"));
		strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		
		hmAccess = GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId, "MANUF_COST_EDIT"));  	
		strAccessFlag = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		log.debug(" Permissions Flag = " + hmAccess);
		if(!strAccessFlag.equals("Y"))
		{
			throw new AppError("You do not have enough permission to do this action", "", 'E');
		}
		
		gmManufCostForm.setStrSuccess("");
		if(strOpt.equals("Save")){
			strOutMsg = gmManfBean.UpdateManufPartCost(hmParam);
			gmManufCostForm.setStrSuccess(strOutMsg);
			strOpt = "Load";
		}
		
		if(strOpt.equals("Load")){
			hmReturn = (HashMap) gmManfBean.fetchPartCostInfo(hmParam);
			gmManufCostForm =(GmManufCostForm) GmCommonClass.getFormFromHashMap(gmManufCostForm, hmReturn) ;

		}
		
		return mapping.findForward("GmManufEditCost");
    }
	
	private String generateOutPut(ArrayList alResult,HashMap hmAccess) throws AppError {
		GmTemplateUtil templateUtil = new GmTemplateUtil();
		templateUtil.setDataList("alResult", alResult);
		templateUtil.setDataMap("hmAccess", hmAccess);
		templateUtil.setTemplateSubDir("manufact/templates");
		templateUtil.setTemplateName("GmManufCostRpt.vm");
		return templateUtil.generateOutput();
	}
	 
	/**
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * Description:This method will update manufacturing cost(Bulk) for part Numbers.
	 */
	public ActionForward manufBulkUpdate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws AppError
    {   
		GmManufCostForm gmManufCostForm = (GmManufCostForm) form;
		gmManufCostForm.loadSessionParameters(request);
		GmManfBean gmManfBean = new GmManfBean(getGmDataStoreVO());
		
		HashMap hmReturn = new HashMap();
		HashMap hmAccess = new HashMap();
		String strOpt = "";
		String strOutMsg = "";
		String strUserId = "";
		String strURL = "/gmManufCost.do?method=manufBulkUpdate";
		String strDisplayNm = "Manufacturing Cost Bulk";
		hmParam = GmCommonClass.getHashMapFromForm(gmManufCostForm);
		
		strOpt = GmCommonClass.parseNull((String)hmParam.get("STROPT"));
		strUserId = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		if(strOpt.equals("save")){
		//o	To call the below new method  gmManfBean.saveManufBulkCost
			gmManfBean.saveManufBulkCost(hmParam);
			//for throw Success Message
			request.setAttribute("hType", "S");
			request.setAttribute("hRedirectURL", strURL);
			request.setAttribute("hDisplayNm", strDisplayNm);
		    String msg = "Cost Updated Successfully";
			throw new AppError(msg, "", 'S');
		}
		
		return mapping.findForward("GmManufBulkCost");
    }
	
	
}