package com.globus.manufact.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.beanutils.RowSetDynaClass;

import com.globus.common.forms.GmCommonForm;

/**
 * @author 
 *
 */
public class GmManufCostForm extends GmCommonForm{
	private String projectId = "";
	private String pnum = "";
	private String partDes = "";
	private String cost = "";
	private String strSuccess = "";
	private String strShowZeroCost = "";
	private String strEditAccess = "";
	private String gridData = "";
	private String historyfl = "";
	/*For manufact Cost Bulk update*/
	 private String batchData = "";
	 private String hPartInputStr = "";
	 private String message  = "";
	 private String hInputStr = "";
	 
	private ArrayList alProject = new ArrayList();
	private RowSetDynaClass rdReport= null;
	
	/**
	 * @return the historyfl
	 */
	public String getHistoryfl() {
		return historyfl;
	}
	/**
	 * @param historyfl the historyfl to set
	 */
	public void setHistoryfl(String historyfl) {
		this.historyfl = historyfl;
	}
	/**
	 * @return the projectId
	 */
	public String getProjectId() {
		return projectId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}
	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	/**
	 * @return the partDes
	 */
	public String getPartDes() {
		return partDes;
	}
	/**
	 * @param partDes the partDes to set
	 */
	public void setPartDes(String partDes) {
		this.partDes = partDes;
	}
	/**
	 * @return the cost
	 */
	public String getCost() {
		return cost;
	}
	/**
	 * @param cost the cost to set
	 */
	public void setCost(String cost) {
		this.cost = cost;
	}
	/**
	 * @return the strSuccess
	 */
	public String getStrSuccess() {
		return strSuccess;
	}
	/**
	 * @param strSuccess the strSuccess to set
	 */
	public void setStrSuccess(String strSuccess) {
		this.strSuccess = strSuccess;
	}
	
	/**
	 * @return the strShowZeroCost
	 */
	public String getStrShowZeroCost() {
		return strShowZeroCost;
	}
	/**
	 * @param strShowZeroCost the strShowZeroCost to set
	 */
	public void setStrShowZeroCost(String strShowZeroCost) {
		this.strShowZeroCost = strShowZeroCost;
	}
	
	/**
	 * @return the strEditAccess
	 */
	public String getStrEditAccess() {
		return strEditAccess;
	}
	/**
	 * @param strEditAccess the strEditAccess to set
	 */
	public void setStrEditAccess(String strEditAccess) {
		this.strEditAccess = strEditAccess;
	}
	
	
	/**
	 * @return the gridData
	 */
	public String getGridData() {
		return gridData;
	}
	/**
	 * @param gridData the gridData to set
	 */
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	/**
	 * @return the alProject
	 */
	public ArrayList getAlProject() {
		return alProject;
	}
	/**
	 * @param alProject the alProject to set
	 */
	public void setAlProject(ArrayList alProject) {
		this.alProject = alProject;
	}
	/**
	 * @return the rdReport
	 */
	public RowSetDynaClass getRdReport() {
		return rdReport;
	}
	/**
	 * @param rdReport the rdReport to set
	 */
	public void setRdReport(RowSetDynaClass rdReport) {
		this.rdReport = rdReport;
	}
	/**
	 * @return the batchData
	 */
	public String getBatchData() {
		return batchData;
	}
	/**
	 * @param batchData the batchData to set
	 */
	public void setBatchData(String batchData) {
		this.batchData = batchData;
	}
	/**
	 * @return the hPartInputStr
	 */
	public String gethPartInputStr() {
		return hPartInputStr;
	}
	/**
	 * @param hPartInputStr the hPartInputStr to set
	 */
	public void sethPartInputStr(String hPartInputStr) {
		this.hPartInputStr = hPartInputStr;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the hInputStr
	 */
	public String gethInputStr() {
		return hInputStr;
	}
	/**
	 * @param hInputStr the hInputStr to set
	 */
	public void sethInputStr(String hInputStr) {
		this.hInputStr = hInputStr;
	}
	
	
	
}