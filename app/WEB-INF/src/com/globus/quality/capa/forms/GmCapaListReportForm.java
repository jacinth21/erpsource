/**
 * @author arajan
 *
 */
package com.globus.quality.capa.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;

public class GmCapaListReportForm extends GmCommonForm {
	private String capaID = "";
	private String type = "";
	private String requestedBy = "";
	private String status = "";
	private String part = "";
	private String lot = "";
	private String projectID = "";
	private String elapsedDys = "";
	private String elapsedDysVal = "";
	private String submitdDtFrom = "";
	private String submitdDtTo = "";
	private String gridXmlData = "";
	private int intResultSize;
	private String chooseAction = "";
	private String hCapaID = "";
	private String hDisplayNm = "";
	private String hRedirectURL = "";
	private String hStatusId = "";
	private String accessFl = "";
	
	private ArrayList alMfgType = new ArrayList();
	private ArrayList alRequestedBy = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alElapsedDys = new ArrayList();
	private ArrayList alChooseAction = new ArrayList();

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param requestedBy the requestedBy to set
	 */
	public void setRequestedBy(String requestedBy) {
		this.requestedBy = requestedBy;
	}

	/**
	 * @return the requestedBy
	 */
	public String getRequestedBy() {
		return requestedBy;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param alMfgType the alMfgType to set
	 */
	public void setAlMfgType(ArrayList alMfgType) {
		this.alMfgType = alMfgType;
	}

	/**
	 * @return the alMfgType
	 */
	public ArrayList getAlMfgType() {
		return alMfgType;
	}

	/**
	 * @param alRequestedBy the alRequestedBy to set
	 */
	public void setAlRequestedBy(ArrayList alRequestedBy) {
		this.alRequestedBy = alRequestedBy;
	}

	/**
	 * @return the alRequestedBy
	 */
	public ArrayList getAlRequestedBy() {
		return alRequestedBy;
	}

	/**
	 * @param alStatus the alStatus to set
	 */
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}

	/**
	 * @return the alStatus
	 */
	public ArrayList getAlStatus() {
		return alStatus;
	}

	/**
	 * @param submitdDtFrom the submitdDtFrom to set
	 */
	public void setSubmitdDtFrom(String submitdDtFrom) {
		this.submitdDtFrom = submitdDtFrom;
	}

	/**
	 * @return the submitdDtFrom
	 */
	public String getSubmitdDtFrom() {
		return submitdDtFrom;
	}

	/**
	 * @param submitdDtTo the submitdDtTo to set
	 */
	public void setSubmitdDtTo(String submitdDtTo) {
		this.submitdDtTo = submitdDtTo;
	}

	/**
	 * @return the submitdDtTo
	 */
	public String getSubmitdDtTo() {
		return submitdDtTo;
	}

	/**
	 * @param alElapsedDys the alElapsedDys to set
	 */
	public void setAlElapsedDys(ArrayList alElapsedDys) {
		this.alElapsedDys = alElapsedDys;
	}

	/**
	 * @return the alElapsedDys
	 */
	public ArrayList getAlElapsedDys() {
		return alElapsedDys;
	}

	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}

	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}

	/**
	 * @param elapsedDys the elapsedDys to set
	 */
	public void setElapsedDys(String elapsedDys) {
		this.elapsedDys = elapsedDys;
	}

	/**
	 * @return the elapsedDys
	 */
	public String getElapsedDys() {
		return elapsedDys;
	}

	/**
	 * @param intResultSize the intResultSize to set
	 */
	public void setIntResultSize(int intResultSize) {
		this.intResultSize = intResultSize;
	}

	/**
	 * @return the intResultSize
	 */
	public int getIntResultSize() {
		return intResultSize;
	}

	/**
	 * @param chooseAction the chooseAction to set
	 */
	public void setChooseAction(String chooseAction) {
		this.chooseAction = chooseAction;
	}

	/**
	 * @return the chooseAction
	 */
	public String getChooseAction() {
		return chooseAction;
	}

	/**
	 * @param alChooseAction the alChooseAction to set
	 */
	public void setAlChooseAction(ArrayList alChooseAction) {
		this.alChooseAction = alChooseAction;
	}

	/**
	 * @return the alChooseAction
	 */
	public ArrayList getAlChooseAction() {
		return alChooseAction;
	}

	/**
	 * @param capaID the capaID to set
	 */
	public void setCapaID(String capaID) {
		this.capaID = capaID;
	}

	/**
	 * @return the capaID
	 */
	public String getCapaID() {
		return capaID;
	}

	/**
	 * @param part the part to set
	 */
	public void setPart(String part) {
		this.part = part;
	}

	/**
	 * @return the part
	 */
	public String getPart() {
		return part;
	}

	/**
	 * @param lot the lot to set
	 */
	public void setLot(String lot) {
		this.lot = lot;
	}

	/**
	 * @return the lot
	 */
	public String getLot() {
		return lot;
	}

	/**
	 * @param projectID the projectID to set
	 */
	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}

	/**
	 * @return the projectID
	 */
	public String getProjectID() {
		return projectID;
	}

	/**
	 * @param elapsedDysVal the elapsedDysVal to set
	 */
	public void setElapsedDysVal(String elapsedDysVal) {
		this.elapsedDysVal = elapsedDysVal;
	}

	/**
	 * @return the elapsedDysVal
	 */
	public String getElapsedDysVal() {
		return elapsedDysVal;
	}

	public void sethCapaID(String hCapaID) {
		this.hCapaID = hCapaID;
	}

	public String gethCapaID() {
		return hCapaID;
	}

	public void sethDisplayNm(String hDisplayNm) {
		this.hDisplayNm = hDisplayNm;
	}

	public String gethDisplayNm() {
		return hDisplayNm;
	}

	public void sethRedirectURL(String hRedirectURL) {
		this.hRedirectURL = hRedirectURL;
	}

	public String gethRedirectURL() {
		return hRedirectURL;
	}

	public void sethStatusId(String hStatusId) {
		this.hStatusId = hStatusId;
	}

	public String gethStatusId() {
		return hStatusId;
	}

	public void setAccessFl(String accessFl) {
		this.accessFl = accessFl;
	}

	public String getAccessFl() {
		return accessFl;
	}

}
