package com.globus.quality.capa.forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import com.globus.common.forms.GmLogForm;

public class GmCapaSetupForm extends GmLogForm {

	private String strCapaId = "";
	private String strMfgType = "";
	private String strRequestedBy = "";
	private String strComplaint = "";
	private String strIssuedBy = "";
	private String strProjectID = "";
	private String status = "";
	private String statusName = "";
	private String strRefID = "";
	private String strLotNums = "";
	private String strCategory = "";
	private String strCapaDesc = "";
	private String strPrevCapas = "";
	private String strCapaSubDt = "";
	private String strRespDueDt = "";
	private String strPrtyLvl = "";
	private String strCboSelected = "";
	private String strClosedDt = "";
	private String strDaysClose = "";
	private String strCapaResp = "";
	private String strAssignedNames = "";
	private String strAssignedTo = "";
	private String message 		  = "";
	private String strMFGTypeDesc = "";
	private String currentdate= "";
	private String daysToClose= "";
	private String strButtonDisable="";
	
	private int intResultSize;
	
	private ArrayList alMfgType = new ArrayList();
	private ArrayList alRequestedBy = new ArrayList();
	private ArrayList alIssuedBy = new ArrayList();
	private ArrayList alAssignedTo = new ArrayList();
	private ArrayList alAssignedNames = new ArrayList();
	private ArrayList alLogReasons = new ArrayList();
	private ArrayList alcboActions = new ArrayList();
	private ArrayList alPrtyLvl = new ArrayList();
	
	/**
	 * @return the strMFGTypeDesc
	 */
	public String getStrMFGTypeDesc() {
		return strMFGTypeDesc;
	}
	/**
	 * @param strMFGTypeDesc the strMFGTypeDesc to set
	 */
	public void setStrMFGTypeDesc(String strMFGTypeDesc) {
		this.strMFGTypeDesc = strMFGTypeDesc;
	}
	/**
	 * @return the strCapaId
	 */
	public String getStrCapaId() {
		return strCapaId;
	}
	/**
	 * @return the intResultSize
	 */
	public int getIntResultSize() {
		return intResultSize;
	}
	/**
	 * @param intResultSize the intResultSize to set
	 */
	public void setIntResultSize(int intResultSize) {
		this.intResultSize = intResultSize;
	}
	/**
	 * @param strCapaId the strCapaId to set
	 */
	public void setStrCapaId(String strCapaId) {
		this.strCapaId = strCapaId;
	}
	/**
	 * @return the strMfgType
	 */
	public String getStrMfgType() {
		return strMfgType;
	}
	/**
	 * @param strMfgType the strMfgType to set
	 */
	public void setStrMfgType(String strMfgType) {
		this.strMfgType = strMfgType;
	}
	/**
	 * @return the strRequestedBy
	 */
	public String getStrRequestedBy() {
		return strRequestedBy;
	}
	/**
	 * @param strRequestedBy the strRequestedBy to set
	 */
	public void setStrRequestedBy(String strRequestedBy) {
		this.strRequestedBy = strRequestedBy;
	}
	/**
	 * @return the strComplaint
	 */
	public String getStrComplaint() {
		return strComplaint;
	}
	/**
	 * @param strComplaint the strComplaint to set
	 */
	public void setStrComplaint(String strComplaint) {
		this.strComplaint = strComplaint;
	}
	/**
	 * @return the strIssuedBy
	 */
	public String getStrIssuedBy() {
		return strIssuedBy;
	}
	/**
	 * @param strIssuedBy the strIssuedBy to set
	 */
	public void setStrIssuedBy(String strIssuedBy) {
		this.strIssuedBy = strIssuedBy;
	}
	/**
	 * @return the strProjectID
	 */
	public String getStrProjectID() {
		return strProjectID;
	}
	/**
	 * @param strProjectID the strProjectID to set
	 */
	public void setStrProjectID(String strProjectID) {
		this.strProjectID = strProjectID;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the statusName
	 */
	public String getStatusName() {
		return statusName;
	}
	/**
	 * @param statusName the statusName to set
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	/**
	 * @return the strRefID
	 */
	public String getStrRefID() {
		return strRefID;
	}
	/**
	 * @param strRefID the strRefID to set
	 */
	public void setStrRefID(String strRefID) {
		this.strRefID = strRefID;
	}
	/**
	 * @return the strLotNums
	 */
	public String getStrLotNums() {
		return strLotNums;
	}
	/**
	 * @param strLotNums the strLotNums to set
	 */
	public void setStrLotNums(String strLotNums) {
		this.strLotNums = strLotNums;
	}
	/**
	 * @return the strCategory
	 */
	public String getStrCategory() {
		return strCategory;
	}
	/**
	 * @param strCategory the strCategory to set
	 */
	public void setStrCategory(String strCategory) {
		this.strCategory = strCategory;
	}
	/**
	 * @return the strCapaDesc
	 */
	public String getStrCapaDesc() {
		return strCapaDesc;
	}
	/**
	 * @param strCapaDesc the strCapaDesc to set
	 */
	public void setStrCapaDesc(String strCapaDesc) {
		this.strCapaDesc = strCapaDesc;
	}
	/**
	 * @return the strPrevCapas
	 */
	public String getStrPrevCapas() {
		return strPrevCapas;
	}
	/**
	 * @param strPrevCapas the strPrevCapas to set
	 */
	public void setStrPrevCapas(String strPrevCapas) {
		this.strPrevCapas = strPrevCapas;
	}
	/**
	 * @return the strCapaSubDt
	 */
	public String getStrCapaSubDt() {
		return strCapaSubDt;
	}
	/**
	 * @param strCapaSubDt the strCapaSubDt to set
	 */
	public void setStrCapaSubDt(String strCapaSubDt) {
		this.strCapaSubDt = strCapaSubDt;
	}
	/**
	 * @return the strRespDueDt
	 */
	public String getStrRespDueDt() {
		return strRespDueDt;
	}
	/**
	 * @param strRespDueDt the strRespDueDt to set
	 */
	public void setStrRespDueDt(String strRespDueDt) {
		this.strRespDueDt = strRespDueDt;
	}
	/**
	 * @return the strPrtyLvl
	 */
	public String getStrPrtyLvl() {
		return strPrtyLvl;
	}
	/**
	 * @param strPrtyLvl the strPrtyLvl to set
	 */
	public void setStrPrtyLvl(String strPrtyLvl) {
		this.strPrtyLvl = strPrtyLvl;
	}
	/**
	 * @return the strCboSelected
	 */
	public String getStrCboSelected() {
		return strCboSelected;
	}
	/**
	 * @param strCboSelected the strCboSelected to set
	 */
	public void setStrCboSelected(String strCboSelected) {
		this.strCboSelected = strCboSelected;
	}
	/**
	 * @return the strClosedDt
	 */
	public String getStrClosedDt() {
		return strClosedDt;
	}
	/**
	 * @param strClosedDt the strClosedDt to set
	 */
	public void setStrClosedDt(String strClosedDt) {
		this.strClosedDt = strClosedDt;
	}
	/**
	 * @return the strDaysClose
	 */
	public String getStrDaysClose() {
		return strDaysClose;
	}
	/**
	 * @param strDaysClose the strDaysClose to set
	 */
	public void setStrDaysClose(String strDaysClose) {
		this.strDaysClose = strDaysClose;
	}
	/**
	 * @return the strCapaResp
	 */
	public String getStrCapaResp() {
		return strCapaResp;
	}
	/**
	 * @param strCapaResp the strCapaResp to set
	 */
	public void setStrCapaResp(String strCapaResp) {
		this.strCapaResp = strCapaResp;
	}
	/**
	 * @return the strAssignedNames
	 */
	public String getStrAssignedNames() {
		return strAssignedNames;
	}
	/**
	 * @param strAssignedNames the strAssignedNames to set
	 */
	public void setStrAssignedNames(String strAssignedNames) {
		this.strAssignedNames = strAssignedNames;
	}
	/**
	 * @return the strAssignedTo
	 */
	public String getStrAssignedTo() {
		return strAssignedTo;
	}
	/**
	 * @param strAssignedTo the strAssignedTo to set
	 */
	public void setStrAssignedTo(String strAssignedTo) {
		this.strAssignedTo = strAssignedTo;
	}
	/**
	 * @return the alMfgType
	 */
	public ArrayList getAlMfgType() {
		return alMfgType;
	}
	/**
	 * @param alMfgType the alMfgType to set
	 */
	public void setAlMfgType(ArrayList alMfgType) {
		this.alMfgType = alMfgType;
	}
	/**
	 * @return the alRequestedBy
	 */
	public ArrayList getAlRequestedBy() {
		return alRequestedBy;
	}
	/**
	 * @param alRequestedBy the alRequestedBy to set
	 */
	public void setAlRequestedBy(ArrayList alRequestedBy) {
		this.alRequestedBy = alRequestedBy;
	}
	/**
	 * @return the alIssuedBy
	 */
	public ArrayList getAlIssuedBy() {
		return alIssuedBy;
	}
	/**
	 * @param alIssuedBy the alIssuedBy to set
	 */
	public void setAlIssuedBy(ArrayList alIssuedBy) {
		this.alIssuedBy = alIssuedBy;
	}
	/**
	 * @return the alAssignedTo
	 */
	public ArrayList getAlAssignedTo() {
		return alAssignedTo;
	}
	/**
	 * @param alAssignedTo the alAssignedTo to set
	 */
	public void setAlAssignedTo(ArrayList alAssignedTo) {
		this.alAssignedTo = alAssignedTo;
	}
	/**
	 * @return the alAssignedNames
	 */
	public ArrayList getAlAssignedNames() {
		return alAssignedNames;
	}
	/**
	 * @param alAssignedNames the alAssignedNames to set
	 */
	public void setAlAssignedNames(ArrayList alAssignedNames) {
		this.alAssignedNames = alAssignedNames;
	}
	/**
	 * @return the alLogReasons
	 */
	public ArrayList getAlLogReasons() {
		return alLogReasons;
	}
	/**
	 * @param alLogReasons the alLogReasons to set
	 */
	public void setAlLogReasons(ArrayList alLogReasons) {
		this.alLogReasons = alLogReasons;
	}
	/**
	 * @return the alcboActions
	 */
	public ArrayList getAlcboActions() {
		return alcboActions;
	}
	/**
	 * @param alcboActions the alcboActions to set
	 */
	public void setAlcboActions(ArrayList alcboActions) {
		this.alcboActions = alcboActions;
	}
	/**
	 * @return the alPrtyLvl
	 */
	public ArrayList getAlPrtyLvl() {
		return alPrtyLvl;
	}
	/**
	 * @param alPrtyLvl the alPrtyLvl to set
	 */
	public void setAlPrtyLvl(ArrayList alPrtyLvl) {
		this.alPrtyLvl = alPrtyLvl;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.strCapaId= "";
    	this.strMfgType= "";
    	this.strRequestedBy = "";
    	this.strComplaint  = "";
    	this.strProjectID = "";
    	this.status = "";
    	this.statusName = "";
    	this.strRefID  = "";
    	this.strLotNums = "";
    	this.strCategory = "";
    	this.strCapaDesc= "";
    	this.strPrevCapas = "";
    	this.strCapaSubDt = "";
    	this.strPrtyLvl  = "";
    	this.strCboSelected = "";
    	this.strClosedDt = "";
    	this.strDaysClose = "";
    	this.strCapaResp  = "";
    	this.strAssignedNames = "";
    	this.strAssignedTo = "";
    	try {   
    		  request.setCharacterEncoding("UTF-8");   
    		} catch (UnsupportedEncodingException ex) {   
    		} 
	}
	
	public String getCurrentdate() {
		return currentdate;
	}
	public void setCurrentdate(String currentdate) {
		this.currentdate = currentdate;
	}
	/**
	 * @param daysToClose the daysToClose to set
	 */
	public void setDaysToClose(String daysToClose) {
		this.daysToClose = daysToClose;
	}
	/**
	 * @return the daysToClose
	 */
	public String getDaysToClose() {
		return daysToClose;
	}
	/**
	 * @param strButtonDisable the strButtonDisable to set
	 */
	public void setStrButtonDisable(String strButtonDisable) {
		this.strButtonDisable = strButtonDisable;
	}
	/**
	 * @return the strButtonDisable
	 */
	public String getStrButtonDisable() {
		return strButtonDisable;
	}
	
}


	
	
	
	
	
	
	

