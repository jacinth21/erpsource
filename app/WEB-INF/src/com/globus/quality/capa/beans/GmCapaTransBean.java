package com.globus.quality.capa.beans;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmCapaTransBean {
	GmCommonClass gmCommon = new GmCommonClass();
	Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger Class. 
	
	/** 
	 * @return String
	 * @throws AppError
	 * */
	public String saveCapaDetails(HashMap hmParam)throws AppError,Exception{	
		
		GmCommonBean gmCommonBean = new GmCommonBean();
		String strCapaID = "";
		String strCapaId 	   = GmCommonClass.parseNull((String) hmParam.get("STRCAPAID"));
		String strType 		   = GmCommonClass.parseZero((String) hmParam.get("STRMFGTYPE"));
		String strReq          = GmCommonClass.parseZero((String) hmParam.get("STRREQUESTEDBY"));
		String strIssue        = GmCommonClass.parseZero((String) hmParam.get("STRISSUEDBY"));
		String strComp   	   = GmCommonClass.parseNull((String) hmParam.get("STRCOMPLAINT"));
		String strProjectID    = GmCommonClass.parseNull((String) hmParam.get("STRPROJECTID"));   	
        String strRefID        = GmCommonClass.parseNull((String) hmParam.get("STRREFID"));
        String strLotNums      = GmCommonClass.parseNull((String) hmParam.get("STRLOTNUMS"));
        String strCategory     = GmCommonClass.parseNull((String) hmParam.get("STRCATEGORY"));
        String strCapaDesc     = GmCommonClass.parseNull((String) hmParam.get("STRCAPADESC"));
        String strAssignedNames= GmCommonClass.parseNull((String) hmParam.get("STRASSIGNEDTO")); 
        String strPrevCapas    = GmCommonClass.parseNull((String) hmParam.get("STRPREVCAPAS"));
        String strPrtyLvl      = GmCommonClass.parseZero((String) hmParam.get("STRPRTYLVL"));
        String strCapaSubDt    = GmCommonClass.parseNull((String) hmParam.get("STRCAPASUBDT"));
		String strRespDueDt    = GmCommonClass.parseNull((String) hmParam.get("STRRESPDUEDT"));   	
        String strClosedDt     = GmCommonClass.parseNull((String) hmParam.get("STRCLOSEDDT"));
        String strResponse     = GmCommonClass.parseNull((String) hmParam.get("STRCAPARESP"));
        String strDateFormat   = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
        String strLog          = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
        String strCreatedBy    = GmCommonClass.parseNull((String) hmParam.get("USERID"));
        Date dtCapaSubDate 	   = null;
        Date dtResposeDate 	   = null;
        Date dtCloseDate 	   = null;
        dtCapaSubDate  		   = GmCommonClass.getStringToDate(strCapaSubDt, strDateFormat);
        dtResposeDate    	   = GmCommonClass.getStringToDate(strRespDueDt, strDateFormat);   
        dtCloseDate 	       = GmCommonClass.getStringToDate(strClosedDt,  strDateFormat);
		GmDBManager gmDBManager = new GmDBManager();
        gmDBManager.setPrepareString("gm_pkg_qa_capa_txn.gm_sav_capa_info",19);
		gmDBManager.registerOutParameter(19, OracleTypes.VARCHAR);
        gmDBManager.setString(1,strCapaId);
		gmDBManager.setInt(2,Integer.parseInt(strType));
		gmDBManager.setInt(3,Integer.parseInt(strReq));
		gmDBManager.setString(4,strComp);
		gmDBManager.setString(5,strProjectID);
		gmDBManager.setString(6,strRefID);
		gmDBManager.setString(7,strLotNums);	
		gmDBManager.setString(8,strCategory);	
		gmDBManager.setString(9,strCapaDesc);	
		gmDBManager.setString(10,strAssignedNames);		
		gmDBManager.setString(11,strPrevCapas);
		gmDBManager.setInt(12,Integer.parseInt(strPrtyLvl));	
		gmDBManager.setDate(13,dtCapaSubDate);	
		gmDBManager.setDate(14,dtResposeDate);	
		gmDBManager.setDate(15,dtCloseDate);	
		gmDBManager.setString(16,strResponse);	
		gmDBManager.setString(17,strCreatedBy);	
		gmDBManager.setInt(18,Integer.parseInt(strIssue));
		gmDBManager.execute();
		strCapaID = GmCommonClass.parseNull(gmDBManager.getString(19));
		if (!strLog.equals("")) {
			log.debug("The log value insdie the bean class is  ****** "+strLog + " *********** "+strCapaID);
			gmCommonBean.saveLog(gmDBManager, strCapaID, strLog, strCreatedBy, "4000317");
		}
		gmDBManager.commit();
		return strCapaID;
		
	         
	}  	
	/** 
	 * @return HashMap
	 * @param hmParam
	 * @throws AppError
	 * */
	
		public HashMap fetchCapa(HashMap hmParam)throws AppError{
		    ArrayList alResult = new ArrayList();
		    ArrayList alAssignedTo = new ArrayList();
		    HashMap hmReturn = new HashMap();
			GmDBManager gmDBManager = new GmDBManager();
			String strRefID = GmCommonClass.parseNull((String) hmParam.get("REFID"));
			String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
			if(strOpt.equals("closureEmail")){
				StringBuffer sbQuery = new StringBuffer();
				sbQuery.append("SELECT RTRIM (XMLAGG (XMLELEMENT (e,  get_user_emailid (c101_assigned_to) || ',')) .EXTRACT ('//text()'), ',') toEmail");
				sbQuery.append(" FROM t3151_capa_assignee WHERE c3150_capa_id ='");
				sbQuery.append(strRefID); 
				sbQuery.append("'");
				hmReturn = gmDBManager.querySingleRecord(sbQuery.toString());	
			}else{
				String strCapaId  = GmCommonClass.parseNull((String) hmParam.get("STRCAPAID"));
				gmDBManager.setPrepareString("gm_pkg_qa_capa_rpt.gm_fch_capa_info",3);
				gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
				gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
				gmDBManager.setString(1, strCapaId);
				gmDBManager.execute();
				alResult = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
				alAssignedTo = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(3));
				hmReturn.put("CAPADETAILS", alResult);
				hmReturn.put("ALASSIGNEDTO", alAssignedTo);
			}
			gmDBManager.close();
			return hmReturn;
		}
}