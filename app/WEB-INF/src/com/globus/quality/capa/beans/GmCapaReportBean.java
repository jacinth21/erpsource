
package com.globus.quality.capa.beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogError;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;

/**
 * @author arajan
 *
 */
public class GmCapaReportBean {

	Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger Class.
	
	/**
	 * fetchCapaListReport - This method is used to fetch the CAPA Report
	 * @param hmParam
	 * @return ArrayList
	 * @throws AppError
	 * @author arajan
	 */
	public ArrayList fetchCapaListReport(HashMap hmParam) throws AppError{ // To fetch the CAPA list report
		
		ArrayList alResult = new ArrayList();
		String strCapaID = GmCommonClass.parseNull((String) hmParam.get("CAPAID"));
		String strMfgType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		String strRequestedBy = GmCommonClass.parseNull((String) hmParam.get("REQUESTEDBY"));
		String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strPartID = GmCommonClass.parseNull((String) hmParam.get("PART"));
		String strLotNum = GmCommonClass.parseNull((String) hmParam.get("LOT"));
		String strProjectID = GmCommonClass.parseNull((String) hmParam.get("PROJECTID"));
		String strComparison = GmCommonClass.parseNull((String) hmParam.get("ELAPSEDDYS"));
		String strElapsedDysVal = GmCommonClass.parseNull((String) hmParam.get("ELAPSEDDYSVAL"));
		String strSubmitdDtFrom = GmCommonClass.parseNull((String) hmParam.get("SUBMITDDTFROM"));
		String strSubmitdDtTo = GmCommonClass.parseNull((String) hmParam.get("SUBMITDDTTO"));
		
		strMfgType = (!strMfgType.equals("0")) ? strMfgType : "";
		strRequestedBy = (!strRequestedBy.equals("0")) ? strRequestedBy : "";
		strStatus = (!strStatus.equals("0")) ? strStatus : "";
		strComparison = (!strComparison.equals("0")) ? strComparison : "";
		
		HashMap hmTempComp = new HashMap(); // To set the comparison signs to corresponding code id
        hmTempComp.put("90190",">");
        hmTempComp.put("90191","<");
        hmTempComp.put("90192","=");
        hmTempComp.put("90193",">=");
        hmTempComp.put("90194","<=");
				
			GmDBManager gmDBManager = new GmDBManager();

			StringBuffer sbQuery = new StringBuffer();
			sbQuery.append(" SELECT	T3150.C3150_CAPA_ID CAPAID, get_user_name(T3150.C101_REQUESTED_BY) REQUESTEDBY, T3150.C3150_CAPA_REF PARTID ");
			sbQuery.append(" , T3150.C3150_PROJECT_REF PROJECTREF,T3150.C3150_LOT_NUMBER LOTNUMBER, GET_CODE_NAME(T3150.C901_TYPE) TYP");
			sbQuery.append(" , GET_CODE_NAME(T3150.C901_STATUS) STATUS, TO_CHAR(T3150.C3150_SUBMITTED_DT,get_rule_value('DATEFMT','DATEFORMAT')) SUBMITTEDDT");
			sbQuery.append(" , T3150.C901_STATUS STATUSID");
			sbQuery.append(" , TO_CHAR(T3150.C3150_RESPONSE_DT,get_rule_value('DATEFMT','DATEFORMAT')) RESPONSEDT, TO_CHAR(T3150.C3150_CLOSE_DT,get_rule_value('DATEFMT','DATEFORMAT')) CLOSEDDT");
			sbQuery.append(" , Decode (C901_Status,'102769',GET_WEEKDAYSDIFF(Trunc(C3150_Submitted_Dt),Trunc(Sysdate)),'102770',GET_WEEKDAYSDIFF(Trunc(C3150_Submitted_Dt),Trunc(C3150_Last_Updated_Date)),'102771',GET_WEEKDAYSDIFF(Trunc(C3150_Submitted_Dt),Trunc(C3150_CLOSE_DT)),'0') DAYTOCLOSE");
			sbQuery.append(" , GET_LOG_FLAG(T3150.C3150_CAPA_ID,4000317) CAPA_CALL_FLAG, T3150.C901_STATUS STATUSID");
			sbQuery.append(" FROM T3150_CAPA T3150");
			sbQuery.append(" WHERE T3150.C3150_VOID_FL IS NULL ");
			if(!strCapaID.equals("")) { // Filter based on CAPA Id
				sbQuery.append(" AND UPPER(T3150.C3150_CAPA_ID) LIKE UPPER('%");
				sbQuery.append(strCapaID);
				sbQuery.append("%')");
			}
			if(!strRequestedBy.equals("")) { // Filter based on Requested by
				sbQuery.append(" AND T3150.C101_REQUESTED_BY = '");
				sbQuery.append(strRequestedBy);
				sbQuery.append("'");
			}
			if(!strMfgType.equals("")) { // Filter based on Type
				sbQuery.append(" AND T3150.C901_TYPE = '");
				sbQuery.append(strMfgType);
				sbQuery.append("'");
			}
			if(!strStatus.equals("")) { // Filter based on Status
				sbQuery.append(" AND T3150.C901_STATUS = '");
				sbQuery.append(strStatus);
				sbQuery.append("'");
			}
			if(!strPartID.equals("")) { // Filter based on Part Id
				sbQuery.append(" AND UPPER(T3150.C3150_CAPA_REF) LIKE UPPER('%");
				sbQuery.append(strPartID);
				sbQuery.append("%')");
			}
			if(!strLotNum.equals("")) { // Filter based on Lot number
				sbQuery.append(" AND UPPER(T3150.C3150_LOT_NUMBER) LIKE UPPER('%");
				sbQuery.append(strLotNum);
				sbQuery.append("%')");
			}
			if(!strProjectID.equals("")) { // Filter based on Project Id
				sbQuery.append(" AND UPPER(T3150.C3150_PROJECT_REF) LIKE UPPER('%");
				sbQuery.append(strProjectID);
				sbQuery.append("%')");
			}
			if(!strSubmitdDtFrom.equals("") && !strSubmitdDtTo.equals("")) { // Filter based on Date
				sbQuery.append(" AND TRUNC(T3150.C3150_SUBMITTED_DT)  BETWEEN TO_DATE('");
				sbQuery.append(strSubmitdDtFrom);
				sbQuery.append("',get_rule_value('DATEFMT','DATEFORMAT'))");
				sbQuery.append(" AND TO_DATE('");
				sbQuery.append(strSubmitdDtTo);
				sbQuery.append("',get_rule_value('DATEFMT','DATEFORMAT'))");
			}
			if(!strElapsedDysVal.equals("")) { // Filter based on Elapsed days
				sbQuery.append(" AND DECODE(C901_Status,'102769',GET_WEEKDAYSDIFF(Trunc(C3150_Submitted_Dt),Trunc(Sysdate))");
				sbQuery.append(" ,'102770',GET_WEEKDAYSDIFF(Trunc(C3150_Submitted_Dt),Trunc(C3150_Last_Updated_Date))");
				sbQuery.append(" ,'102771',GET_WEEKDAYSDIFF(Trunc(C3150_Submitted_Dt),Trunc(C3150_CLOSE_DT)),'0') ");
				sbQuery.append(hmTempComp.get(strComparison));
				sbQuery.append(strElapsedDysVal);
			}
			sbQuery.append(" ORDER BY DAYTOCLOSE DESC");
			
			alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
			log.debug(" Query to get CAPA Report is " + sbQuery.toString() + "size of AList " + alResult.size());
			
		return alResult;
	}
	
	/**
	 * fetchBussinessDays - To add bussiness days to a date
	 * @param hmParam
	 * @return String
	 * @throws AppError, Exception
	 * @author arajan
	 */
	public String fetchBussinessDays(HashMap hmParam) throws AppError, Exception{
		String strDate = GmCommonClass.parseNull((String) hmParam.get("CURRENTDT"));
		String strDays = GmCommonClass.parseNull((String) hmParam.get("DAYS"));
		String strDateFormat   = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
		String strDueDate = "";
		Date dtSubDate = null;
		Date dtRespDueDt = null;
		dtSubDate  		   = (Date) GmCommonClass.getStringToDate(strDate, strDateFormat);
		ArrayList alResult = new ArrayList();
		
		GmDBManager gmDBManager = new GmDBManager();
        gmDBManager.setFunctionString("get_business_day",2);
        gmDBManager.registerOutParameter(1, OracleTypes.DATE);
        gmDBManager.setDate(2,dtSubDate);
		gmDBManager.setInt(3,Integer.parseInt(strDays));
		gmDBManager.execute();
		dtRespDueDt = (Date)gmDBManager.getObject(1); // get value in date format
		gmDBManager.close();
		strDueDate=GmCommonClass.getStringFromDate(dtRespDueDt,strDateFormat);// convert date to string
		return strDueDate;
	}

}
