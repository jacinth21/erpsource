package com.globus.quality.capa.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.quality.capa.beans.GmCapaReportBean;
import com.globus.quality.complaint.beans.GmCOMRSRRptBean;
import com.globus.quality.capa.beans.GmCapaTransBean;
import com.globus.quality.capa.forms.GmCapaSetupForm;
import com.globus.sales.event.beans.GmEventSetupBean;
public class GmCapaSetupAction extends GmDispatchAction {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
   // public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws 
	public ActionForward addCapa(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws AppError,Exception 
	{
		
		GmCapaSetupForm gmCapaSetupForm = (GmCapaSetupForm) actionForm;
		GmCapaTransBean gmCapaTransBean = new GmCapaTransBean();
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmLogonBean gmLogon = new GmLogonBean();
		GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		GmCapaReportBean gmCapaReportBean = new GmCapaReportBean();
		GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean();
		String strForward = "gmCapaSetup";
		instantiate(request, response);
		gmCapaSetupForm.loadSessionParameters(request);
        ArrayList alMfgType = new ArrayList();
		ArrayList alRequestedby  = new ArrayList();
		ArrayList alIssuedBy = new ArrayList();
		ArrayList alAssignedTo = new ArrayList();
		ArrayList alPrtyLvl = new ArrayList();
		ArrayList alCboAction = new ArrayList();
		ArrayList alLogReasons = new ArrayList();
		HashMap hmParam =  new HashMap();
		HashMap hmAccess =  new HashMap();
		HashMap hmType = new HashMap();
		String strPartyId 	 = GmCommonClass.parseNull((String) gmCapaSetupForm.getSessPartyId()); // Get the user from session
		hmAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,"CAPA_ACCESS"));
		String strUpdateAccess = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		gmCapaSetupForm.setStrButtonDisable(strUpdateAccess);
	
		hmType.put("USERCODE", "300");
		hmType.put("INACTIVE_USER", "Y");
		//Getting all the DB values for the drop down list
		alMfgType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MFGTYP"));
		alRequestedby = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmType));
		alPrtyLvl = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PRTLVL"));
		alCboAction = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CAPATP"));
		
		// To get the Issued by drop down
		alIssuedBy = GmCommonClass.parseNullArrayList((ArrayList) gmAccessControlBean.fetchgroupUser("CAPA_ISSUED_BY"));

		gmCapaSetupForm.setAlMfgType(alMfgType);
		gmCapaSetupForm.setAlRequestedBy(alRequestedby);
		gmCapaSetupForm.setAlIssuedBy(alIssuedBy);
		gmCapaSetupForm.setAlAssignedNames(alRequestedby);
		gmCapaSetupForm.setAlPrtyLvl(alPrtyLvl);
		gmCapaSetupForm.setAlcboActions(alCboAction);
		
		String strOpt= GmCommonClass.parseNull(gmCapaSetupForm.getStrOpt());
		String strCapaId = "";
		String strMessage = "";
		String strCurrentdate = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessTodaysDate"));
		hmParam = GmCommonClass.getHashMapFromForm(gmCapaSetupForm);
		strCapaId  = GmCommonClass.parseNull((String)gmCapaSetupForm.getStrCapaId());
		
		hmParam.put("CURRENTDT", strCurrentdate);
		hmParam.put("DAYS", "30");
		String strRespDt = GmCommonClass.parseNull(gmCapaReportBean.fetchBussinessDays(hmParam));// To add 30 business days with current date
	
		if(strOpt.equals("save")){
			strCapaId = gmCapaTransBean.saveCapaDetails(hmParam);
			if(!strCapaId.equals("")){
				strMessage = "Data Saved Successfully";	
			}
			 //When submit the data , once submitted then need to be display the value to the screen which we have entered from screen
			String strRedirectURL = "/gmCapaSetup.do?method=editCapa&strCapaId="+strCapaId+"&strOpt=Fetch&message="+strMessage;
			ActionRedirect actionRedirect = new ActionRedirect(strRedirectURL);
		    return actionRedirect;
		}
		alLogReasons = gmCommonBean.getLog(strCapaId,"4000317");
		gmCapaSetupForm.setAlLogReasons(alLogReasons);
		hmParam.put("STRRESPDUEDT", strRespDt);
		hmParam.put("STRISSUEDBY", strPartyId);
		GmCommonClass.getFormFromHashMap(gmCapaSetupForm, hmParam);

		gmCapaSetupForm.reset(actionMapping, request);

	return actionMapping.findForward(strForward);
  }

	public ActionForward editCapa (ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws Exception, AppError 
	{
		GmLogonBean gmLogon = new GmLogonBean();
		GmCapaSetupForm gmCapaSetupForm = (GmCapaSetupForm) actionForm;
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmCapaTransBean gmCapaTransBean = new GmCapaTransBean();
		GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(); 
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean();
		instantiate(request, response);
		gmCapaSetupForm.loadSessionParameters(request);
		String strForward = "gmCapaSetup";
        ArrayList alMfgType = new ArrayList();
		ArrayList alRequestedby  = new ArrayList();
		ArrayList alIssuedBy = new ArrayList();
		ArrayList alAssignedNames = new ArrayList();
		ArrayList alAssignedTo = new ArrayList();
		ArrayList alPrtyLvl = new ArrayList();
		ArrayList alResult = new ArrayList();
		ArrayList alCboAction = new ArrayList();

		HashMap hmResult =  new HashMap();
		HashMap hmParam =  new HashMap();
		HashMap hmValues =  new HashMap();
		HashMap hmAccess =  new HashMap();
		HashMap hmType = new HashMap();
		
		ArrayList alLogReasons = new ArrayList();
		String strPartyId 	 = GmCommonClass.parseNull((String) gmCapaSetupForm.getSessPartyId());// Get the user from session
				hmAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,"CAPA_ACCESS"));
		
		String strUpdateAccess = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
		gmCapaSetupForm.setStrButtonDisable(strUpdateAccess);
		
		
		alMfgType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MFGTYP"));
		hmType.put("USERCODE", "300"); 
		hmType.put("INACTIVE_USER", "Y");
		alRequestedby = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmType));
		// To get the Issued by drop down
		alIssuedBy = GmCommonClass.parseNullArrayList((ArrayList) gmAccessControlBean.fetchgroupUser("CAPA_ISSUED_BY"));
		alPrtyLvl = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PRTLVL"));
		alCboAction = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CAPATP")); // for Choose Action
		
		gmCapaSetupForm.setAlMfgType(alMfgType);
		gmCapaSetupForm.setAlRequestedBy(alRequestedby);
		gmCapaSetupForm.setAlIssuedBy(alIssuedBy);
		gmCapaSetupForm.setAlAssignedNames(alRequestedby);
		gmCapaSetupForm.setAlPrtyLvl(alPrtyLvl);
		gmCapaSetupForm.setAlcboActions(alCboAction);
		
		
		hmParam = GmCommonClass.getHashMapFromForm(gmCapaSetupForm);             
		String strOpt = GmCommonClass.parseNull((String)gmCapaSetupForm.getStrOpt());
		String strCapaId  = GmCommonClass.parseNull((String) hmParam.get("STRCAPAID"));
		
		strCapaId = GmCommonClass.parseNull(gmCapaSetupForm.getStrCapaId());
		
		if(strOpt.equals("Fetch")){
			hmValues =	gmCapaTransBean.fetchCapa(hmParam);
			alResult = GmCommonClass.parseNullArrayList((ArrayList)hmValues.get("CAPADETAILS"));
			alAssignedTo = GmCommonClass.parseNullArrayList((ArrayList)hmValues.get("ALASSIGNEDTO"));
			if(alResult.size()> 0 )	hmResult = (HashMap)alResult.get(0);
			GmCommonClass.getFormFromHashMap(gmCapaSetupForm, hmResult);
		}
		if(alResult.size() > 0 )	
			gmCapaSetupForm.setIntResultSize(alResult.size());
		alLogReasons = gmCommonBean.getLog(strCapaId,"4000317");
		gmCapaSetupForm.setAlLogReasons(alLogReasons);
		gmCapaSetupForm.setAlAssignedTo(alAssignedTo);
		
		return actionMapping.findForward(strForward);
	}
	public ActionForward capaHighlights(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws Exception, AppError 
	{
		GmCapaSetupForm gmCapaSetupForm = (GmCapaSetupForm) actionForm;
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmCapaTransBean gmCapaTransBean = new GmCapaTransBean();
		GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(); 
		
		String strForward = "CAPAHighlights";
		
		ArrayList alResult = new ArrayList();
		ArrayList alLogReasons = new ArrayList();

		HashMap hmResult =  new HashMap();
		HashMap hmParam =  new HashMap();
		HashMap hmValues =  new HashMap();
		
		hmParam = GmCommonClass.getHashMapFromForm(gmCapaSetupForm);             
		String strOpt = GmCommonClass.parseNull((String)gmCapaSetupForm.getStrOpt());
		String strCapaId  = GmCommonClass.parseNull((String) hmParam.get("STRCAPAID"));
		strCapaId = GmCommonClass.parseNull(gmCapaSetupForm.getStrCapaId());
		
		if(strOpt.equals("Fetch") || strOpt.equals("Summary")){ // To fetch the details to the Summary screen and Upload screen
			hmValues =	gmCapaTransBean.fetchCapa(hmParam);
			alResult = GmCommonClass.parseNullArrayList((ArrayList)hmValues.get("CAPADETAILS"));
			if(alResult.size()> 0 )	hmResult = (HashMap)alResult.get(0);
			GmCommonClass.getFormFromHashMap(gmCapaSetupForm, hmResult);
		}
		if(alResult.size() > 0 )	
			gmCapaSetupForm.setIntResultSize(alResult.size());
		alLogReasons = gmCommonBean.getLog(strCapaId,"4000317");
		gmCapaSetupForm.setAlLogReasons(alLogReasons);
		return actionMapping.findForward(strForward);
	}
}