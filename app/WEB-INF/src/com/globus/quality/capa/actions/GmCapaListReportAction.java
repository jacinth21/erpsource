/**
 * Description : This action is used for fetch the details of CAPA
 * 
 * @author arajan
 */
package com.globus.quality.capa.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.logon.beans.GmLogonBean;
import com.globus.quality.capa.beans.GmCapaReportBean;
import com.globus.quality.capa.beans.GmCapaTransBean;
import com.globus.quality.capa.forms.GmCapaListReportForm;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmCapaListReportAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName()); // Code to Initialize the Logger
                                                                // Class.

  public ActionForward listCapaDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    GmCapaListReportForm gmCapaListReportForm = (GmCapaListReportForm) actionForm;
    gmCapaListReportForm.loadSessionParameters(request);
    GmCapaReportBean gmCapaReportBean = new GmCapaReportBean();
    GmLogonBean gmLogon = new GmLogonBean();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();

    String strForward = "gmCAPAListReport";
    String strOpt = GmCommonClass.parseNull(gmCapaListReportForm.getStrOpt());
    String strXmlGridData = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    // String strPartyId = GmCommonClass.parseNull((String)
    // gmCapaListReportForm.getSessPartyId());// Get the user from session
    // String strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "CAPA_USER");// to
    // get the accessibility of user

    ArrayList alMfgType = new ArrayList();
    ArrayList alRequestedBy = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alElapsedDys = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alChooseAction = new ArrayList();
    ArrayList alRemoveCodeIDs = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmType = new HashMap();
    HashMap hmResult = new HashMap();
    /*
     * if(!strAccessFl.equals("Y")){// Checking whether user is having access or not throw new
     * AppError("You do not have enough permission to do this action", "", 'E'); }
     */

    // Get the drop down values from code lookup table
    alMfgType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MFGTYP"));
    alStatus = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CAPAST"));
    alElapsedDys = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CPRSN"));
    alChooseAction = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CAPATP"));

    alRemoveCodeIDs.add("102766"); // to remove "Modify/Generate Report" from "Choose Option" drop
                                   // down
    alChooseAction =
        GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(
            alRemoveCodeIDs, alChooseAction));

    // Get employee list
    hmType.put("USERCODE", "300");
    hmType.put("INACTIVE_USER", "Y");
    alRequestedBy = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmType));

    if (strOpt.equals("load")) {
      // Set all the values to drop down
      gmCapaListReportForm.setAlMfgType(alMfgType);
      gmCapaListReportForm.setAlRequestedBy(alRequestedBy);
      gmCapaListReportForm.setAlStatus(alStatus);
      gmCapaListReportForm.setAlElapsedDys(alElapsedDys);
      gmCapaListReportForm.setStatus("102769"); // To set the status as "Open" while loading from
                                                // left link
      strOpt = "Report";
    }

    hmParam = GmCommonClass.getHashMapFromForm(gmCapaListReportForm);
    log.debug("hmParam ==========" + hmParam);
    String strAccessFl = GmCommonClass.parseNull(gmCapaListReportForm.getAccessFl());
  
    if (strOpt.equals("Report")) {
      // Set all the values to drop down
      gmCapaListReportForm.setAlMfgType(alMfgType);
      gmCapaListReportForm.setAlRequestedBy(alRequestedBy);
      gmCapaListReportForm.setAlStatus(alStatus);
      gmCapaListReportForm.setAlElapsedDys(alElapsedDys);
      gmCapaListReportForm.setAlChooseAction(alChooseAction);
      alResult = GmCommonClass.parseNullArrayList(gmCapaReportBean.fetchCapaListReport(hmParam));
      hmResult.put("ALRESULT", alResult);
      hmResult.put("strAccessFl", strAccessFl);
      gmCapaListReportForm.setIntResultSize(alResult.size());
      strXmlGridData = generateOutPut(hmResult, strSessCompanyLocale);
      gmCapaListReportForm.setGridXmlData(strXmlGridData);
    }

    return actionMapping.findForward(strForward);
  }

  public ActionForward printCapaDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    GmCapaListReportForm gmCapaListReportForm = (GmCapaListReportForm) actionForm;
    gmCapaListReportForm.loadSessionParameters(request);
    GmCapaTransBean gmCapaTransBean = new GmCapaTransBean();

    HashMap hmValues = new HashMap();
    HashMap hmAssingedToValues = new HashMap();

    ArrayList alCapaDetails = new ArrayList();
    ArrayList alAssignedTo = new ArrayList();
    String strAssinedToNames = "";
    String strOpt = GmCommonClass.parseNull(gmCapaListReportForm.getStrOpt());
    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmCapaListReportForm);
    String strCapaId = GmCommonClass.parseNull(gmCapaListReportForm.getCapaID());
    hmParam.put("STRCAPAID", strCapaId);
    log.debug("hmParam ==========" + hmParam);

    if (strOpt.equals("Print")) {
      hmValues = gmCapaTransBean.fetchCapa(hmParam);
      alCapaDetails = GmCommonClass.parseNullArrayList((ArrayList) hmValues.get("CAPADETAILS"));
      alAssignedTo = GmCommonClass.parseNullArrayList((ArrayList) hmValues.get("ALASSIGNEDTO"));

      if (alCapaDetails.size() > 0)
        hmValues = GmCommonClass.parseNullHashMap((HashMap) alCapaDetails.get(0));

      if (alAssignedTo.size() > 0) {
        Iterator iterator = alAssignedTo.iterator();
        while (iterator.hasNext()) {
          hmAssingedToValues = (HashMap) iterator.next();
          String strAssingedNm = GmCommonClass.parseNull((String) hmAssingedToValues.get("NAME"));
          strAssinedToNames = strAssinedToNames + strAssingedNm + ", ";
        }
      }
      if (strAssinedToNames.length() > 0)
        strAssinedToNames = strAssinedToNames.substring(0, strAssinedToNames.length() - 2);
      hmValues.put("STRASSINEDTO", strAssinedToNames);
      hmValues.put("VERSION", GmCommonClass.getRuleValue("102788", "CMP_VERSION"));
      GmJasperReport gmJasperReport = new GmJasperReport();
      gmJasperReport.setRequest(request);
      gmJasperReport.setResponse(response);
      gmJasperReport.setJasperReportName("/GmPrintCAPA.jasper");
      gmJasperReport.setHmReportParameters(hmValues);
      gmJasperReport.setReportDataList(null);
      gmJasperReport.setPageHeight(1800);
      gmJasperReport.exportJasperReportToRTF();
    }
    return actionMapping.findForward("");
  }

  public String generateOutPut(HashMap hmParam, String strSessCompanyLocale) throws AppError { // Processing
                                                                                                  // ArrayList
                                                                                                  // into
    // XML data
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("quality/capa/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.quality.capa.GmCapaListReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmCapaListReport.vm");
    return templateUtil.generateOutput();
  }
}
