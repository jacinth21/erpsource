package com.globus.quality.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmIssuingAgencyConfigBean {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * fetchIssueAgency - This method used to fetch the user configure list
	 * @param 
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList fetchIssueAgency() throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_rpt_udi.gm_fch_issuing_agency", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alReturn;
	}

	/**
	 * fetchIssuingAgencyIdentify - This method used to fetch the Agency information
	 * @param String
	 * @return ArrayList
	 * @exception AppError
	 */
	public ArrayList fetchIssuingAgencyIdentifier(String strAgencyId)
			throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_rpt_udi.gm_fch_issuing_agency_ident", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strAgencyId);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();

		return alReturn;
	}

	/**
	 * fetchConfiguredDtls - This method used to fetch the issuing agency configured details 
	 * @param String
	 * @return HashMap
	 * @exception AppError
	 */
	public HashMap fetchUDIConfiguredDtls(String strAgencyId) throws AppError {
		ArrayList alReturn = new ArrayList();
		HashMap hmConfigDtls = new HashMap();
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_rpt_udi.gm_fch_configured_dtls", 3);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		gmDBManager.setString(1, strAgencyId);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
		hmConfigDtls = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		gmDBManager.close();
		hmReturn.put("ALRETURN", alReturn);
		hmReturn.put("HMCONFIGDTLS", hmConfigDtls);

		return hmReturn;
	}

	/**
	 * fetchConfigName - This method used to fetch the issuing agency configured names 
	 * @param String
	 * @return HashMap
	 * @exception AppError
	 */
	public ArrayList fetchConfigName() throws AppError {
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_rpt_udi.gm_fch_config_name", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();

		return alReturn;
	}
	
	/**
	 * saveIssueAgencyInfo - This method used to save the issuing agency configured details 
	 * @param HashMap
	 * @return String
	 * @exception AppError
	 */
	public String saveIssueAgencyInfo(HashMap hmParam) throws AppError {
		String strIssueAgencyId = GmCommonClass.parseZero((String) hmParam.get("ISSUINGAGENCY"));
		String strIssueConfiName = GmCommonClass.parseNull((String) hmParam.get("TXT_CONFIGNAME"));
		String strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
		String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strIssueConfigId = GmCommonClass.parseZero((String) hmParam.get("CONFIGNAME"));
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_pd_sav_udi.gm_sav_issue_agency_dtls", 5);
		gmDBManager.registerOutParameter(5, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strIssueAgencyId);
		gmDBManager.setString(2, strIssueConfiName);
		gmDBManager.setString(3, strInputStr);
		gmDBManager.setString(4, strUserId);
		gmDBManager.setString(5, strIssueConfigId);
		gmDBManager.execute();
		strIssueConfigId = GmCommonClass.parseNull((String) gmDBManager.getString(5));
		log.debug(" strIssueConfigId " + strIssueConfigId);
		gmDBManager.commit();
		return strIssueConfigId;
	}
}
