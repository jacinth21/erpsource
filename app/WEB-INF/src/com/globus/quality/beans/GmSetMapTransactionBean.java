package com.globus.quality.beans;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmDBManager;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

/**
 * 
 * This class used to do the set mapping operations
 * 
 * @author mmuthusamy
 *
 */
public class GmSetMapTransactionBean extends GmBean {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * TODO Auto-generated constructor stub
	 * 
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmSetMapTransactionBean(GmDataStoreVO gmDataStoreVO) {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	/**
	 * fetchSetMappingUpload - This method used to fetch set mapping upload
	 * details
	 *
	 * @param strSetId
	 * @param strStatus
	 * @return
	 * @throws AppError
	 */

	public String fetchSetMappingUpload(String strSetId, String strStatus)
			throws AppError {
		String strUploadJsonData = "";

		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString(
				"gm_pkg_qa_set_mapping_rpt.gm_fch_set_mapping_upload_dtls", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.setString(1, strSetId);
		gmDBManager.setString(2, strStatus);
		gmDBManager.execute();
		strUploadJsonData = GmCommonClass.parseNull((String) gmDBManager
				.getString(3));
		gmDBManager.close();
		return strUploadJsonData;
	}

	/**
	 * saveSetListUploadDtls - This method used to save Set mapping upload
	 * details based an actions
	 * 
	 * @param hmParam
	 * @return
	 * @throws AppError
	 */

	public String saveSetMappingUploadDtls(HashMap hmParam) throws AppError {
		GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());

		String strErrorJson = "";
		String strMappingUpdateStr = "";
		String strSystemComments = "";
		String strSetId = GmCommonClass
				.parseNull((String) hmParam.get("SETID"));
		String strInputString = GmCommonClass.parseNull((String) hmParam
				.get("INPUTSTR"));
		String strAction = GmCommonClass.parseNull((String) hmParam
				.get("UPLOADACTION"));
		String strUserId = GmCommonClass.parseNull((String) hmParam
				.get("USERID"));
		String strComments = GmCommonClass.parseNull((String) hmParam
				.get("TXT_LOGREASON"));
		String strSetName = GmCommonClass.parseNull((String) hmParam
				.get("SEARCHSETID"));

		gmDBManager.setPrepareString(
				"gm_pkg_qa_set_mapping_txn.gm_sav_bulk_set_list_upload", 7);
		gmDBManager.registerOutParameter(6, OracleTypes.CLOB);
		gmDBManager.registerOutParameter(7, OracleTypes.CLOB);
		gmDBManager.setString(1, strSetId);
		gmDBManager.setString(2, strInputString);
		gmDBManager.setString(3, strAction);
		gmDBManager.setString(4, strComments);
		gmDBManager.setString(5, strUserId);
		gmDBManager.execute();
		strErrorJson = GmCommonClass.parseNull((String) gmDBManager
				.getString(6));
		strMappingUpdateStr = GmCommonClass.parseNull((String) gmDBManager
				.getString(7));

		// to split the string
		StringTokenizer stToken = new StringTokenizer(strErrorJson, "^^");
		strSystemComments = "Set List updated from bulk updateUsing <b>Set Mapping bulk upload<b/>, Add/Modified the set <b> "
				+ strSetName + "</b>";
		// Action Add/Modify - to sync T208 using existing method
		if (!strMappingUpdateStr.equals("")
				&& strAction.equalsIgnoreCase("108741")) {
			gmProjectBean.saveSetMap(gmDBManager, strSetId,
					strMappingUpdateStr.concat("|"), strComments, strUserId);
		} else if (!strMappingUpdateStr.equals("")
				&& strAction.equalsIgnoreCase("108742")) {
			strSystemComments = "Using <b>Set Mapping bulk upload<b/>, Voided mapping details for the set <b>  "
					+ strSetName + "</b>";
			gmCommonBean.saveLog(gmDBManager, strSetId, strComments, strUserId,
					"1249");
		}

		if (!strMappingUpdateStr.equals("")) {
			sendSetListUpdateEmail (strSetId);
			saveSetMappingUploadLog(gmDBManager, strSetId, strAction, strUserId); // Saved
		}
		gmDBManager.commit();

		return strErrorJson;

	}

	/**
	 * 
	 * saveSetMappingUploadLog - This method used to store set mapping bulk
	 * operation to log table
	 * 
	 * @param gmDBManager
	 * @param strSetId
	 * @param strAction
	 * @param strUserId
	 * @throws AppError
	 */

	public void saveSetMappingUploadLog(GmDBManager gmDBManager,
			String strSetId, String strAction, String strUserId)
			throws AppError {
		gmDBManager.setPrepareString(
				"gm_pkg_qa_set_mapping_txn.gm_sav_set_mapping_upload_log", 3);
		gmDBManager.setString(1, strSetId);
		gmDBManager.setString(2, strAction);
		gmDBManager.setString(3, strUserId);
		gmDBManager.execute();
	}

	/**
	 * saveSetListCopy :: This method used for save the set list copy - Copying
	 * From Set list into to Set list. The To Set will be voided before copying
	 * From Set List details and send e-mail to users
	 * 
	 * @param hmParam
	 * @throws AppError
	 */
	public void saveSetListCopy(HashMap hmParam) throws AppError {
		String strSetBundleId = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		GmProjectBean gmProj = new GmProjectBean(getGmDataStoreVO());
		
		String strFromSetId = GmCommonClass.parseNull((String) hmParam
				.get("TXTFROMSETID"));
		String strToSetId = GmCommonClass.parseNull((String) hmParam
				.get("TXTTOSETID"));
		String strComment = GmCommonClass.parseNull((String) hmParam
				.get("TXT_LOGREASON"));
		String strUserId = GmCommonClass.parseNull((String) hmParam
				.get("USERID"));
		
		gmDBManager.setPrepareString(
				"gm_pkg_qa_set_mapping_txn.gm_qa_sav_set_list_copy", 4);
		gmDBManager.setString(1, strFromSetId);
		gmDBManager.setString(2, strToSetId);
		gmDBManager.setString(3, strUserId);
		gmDBManager.setString(4, strComment);
		gmDBManager.execute();
		gmDBManager.commit();

		// Send mail to user
		sendSetListUpdateEmail(strToSetId);
	}

	/**
	 *
	 * sendSetListUpdateEmail - This method used to send the notification to set
	 * configuration team.
	 * 
	 * @param strSetId
	 * @throws AppError
	 */

	public void sendSetListUpdateEmail(String strSetId) throws AppError {
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		String strMailIds = "";
		String strCCMailIds = "";
		//
		String strMessage = "";
		String strFrom = "";
		String strSubject = "";
		String strMimeType = "";
		String strTemplate = "GmSetListUpdateEmail";
		String strCompanyLocale = "";
		HashMap hmRuleData = new HashMap();

		hmRuleData.put("RULEID", "SETLIST");
		hmRuleData.put("RULEGROUP", "EMAIL");
		strMailIds = GmCommonClass.parseNull(gmCommonBean
				.getRuleValue(hmRuleData));

		strCompanyLocale = GmCommonClass.getCompanyLocale(getGmDataStoreVO()
				.getCmpid());

		GmResourceBundleBean gmResourceBundleBean = GmCommonClass
				.getResourceBundleBean("properties.Email.EmailTemplate",
						strCompanyLocale);

		strFrom = gmResourceBundleBean.getProperty(strTemplate + "."
				+ GmEmailProperties.FROM);
		strSubject = gmResourceBundleBean.getProperty(strTemplate + "."
				+ GmEmailProperties.SUBJECT);
		strMimeType = gmResourceBundleBean.getProperty(strTemplate + "."
				+ GmEmailProperties.MIME_TYPE);
		strCCMailIds = gmResourceBundleBean.getProperty(strTemplate + "."
				+ GmEmailProperties.CC);
		// to get the message and form dynamic data
		strMessage = gmResourceBundleBean.getProperty(strTemplate + "."
				+ GmEmailProperties.MESSAGE_BODY);
		strMessage = GmCommonClass
				.replaceAll(strMessage, "#<SET_ID>", strSetId);
		//
		// Changed to "," for MNTTASK-7339 to handle multiple reps emails
		GmCommonClass.sendMail(strFrom,
				GmCommonClass.StringtoArray(strMailIds, ","),
				GmCommonClass.StringtoArray(strCCMailIds, ","),
				strSubject, strMessage);
	}
	
}
