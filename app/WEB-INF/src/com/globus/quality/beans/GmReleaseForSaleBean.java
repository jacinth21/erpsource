package com.globus.quality.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmPartNumberServiceBean;
import com.globus.valueobject.common.GmDataStoreVO;

public class GmReleaseForSaleBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmReleaseForSaleBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }

  public GmReleaseForSaleBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }

  public String getXmlGridData(ArrayList alOutPartInfo, String strSessCompanyLocale)
      throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.quality.GmReleaseForSale", strSessCompanyLocale));
    templateUtil.setDataList("alReturn", alOutPartInfo);
    templateUtil.setTemplateName("GmReleaseForSale.vm");
    templateUtil.setTemplateSubDir("quality/templates");
    return templateUtil.generateOutput();
  }

  public HashMap loadRFSDetails() throws AppError {

    GmCommonClass gmCommon = new GmCommonClass();
    HashMap hmReturn = new HashMap();


    ArrayList alRFS = gmCommon.getCodeList("RFSALE");
    ArrayList alRFSStatus = gmCommon.getCodeList("RFSYRN");

    hmReturn.put("RFSCOUNTRY", alRFS);
    hmReturn.put("RFSSTATUS", alRFSStatus);

    return hmReturn;
  }

  public HashMap saveRFSDetails(HashMap hmParam) throws AppError {

    GmPartNumberServiceBean gmPartNumberServiceBean =
        new GmPartNumberServiceBean(getGmDataStoreVO());

    ArrayList alOutPartInfo = new ArrayList();
    HashMap hmReturn = new HashMap();

    String strMsg = "";
    String strInvalidParts = "";
    String strComment = "";
    String strOutPartInfo = "";

    // get HashMap & assign the value into string variable.
    String strRFSPnum = GmCommonClass.parseNull((String) hmParam.get("HPARTNOS"));
    String strRFSCountry = GmCommonClass.parseNull((String) hmParam.get("HCHKRFSCOUNTRY"));
    String strRFSStatus = GmCommonClass.parseNull((String) hmParam.get("RFSSTATUS"));
    String strRFSCom = GmCommonClass.parseNull((String) hmParam.get("COMMENTS"));
    String strAttrType = GmCommonClass.parseNull((String) hmParam.get("ATTRTYPE"));
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    // call the package with perticular procedure & set the in/out parameter.
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
    gmDBManager.setPrepareString("gm_pkg_qa_rfs.gm_qa_sav_rfs", 10);
    gmDBManager.registerOutParameter(7, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(8, OracleTypes.CLOB);
    gmDBManager.registerOutParameter(9, OracleTypes.VARCHAR);
    gmDBManager.registerOutParameter(10, OracleTypes.CURSOR);

    // set the In parameter for called procedure.
    gmDBManager.setString(1, strRFSCountry);
    gmDBManager.setString(2, strRFSPnum);
    gmDBManager.setString(3, strRFSStatus);
    gmDBManager.setString(4, strRFSCom);
    gmDBManager.setString(5, strUserId);
    gmDBManager.setString(6, "80180");

    gmDBManager.execute();
    log.debug("Exit from saveRFSDetails");

    // get the value from the out parameter & assign it to string variable.
    strMsg = GmCommonClass.parseNull(gmDBManager.getString(7));
    strInvalidParts = GmCommonClass.parseNull(gmDBManager.getString(8));
    strComment = GmCommonClass.parseNull(gmDBManager.getString(9));
    alOutPartInfo =
        GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
            .getObject(10)));

    gmDBManager.commit();
    log.debug("success");

    // set the value into HashMap as key-pair combination & return the HashMap.
    hmReturn.put("MSG", strMsg);
    hmReturn.put("INVALIDPART", strInvalidParts);
    hmReturn.put("RFSCOMMENT", strComment);
    hmReturn.put("OUTPARTS", alOutPartInfo);

    // By using the below method sync will happen for all companies for that part based on system
    // and project.
    gmPartNumberServiceBean.processPartVisibility("", strUserId);

    return hmReturn;
  }
}
