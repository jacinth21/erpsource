package com.globus.quality.beans;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.common.jms.GmConsumerUtil;
import com.globus.common.jms.GmJMSConsumerConfigurationBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.valueobject.common.GmDataStoreVO;

import oracle.jdbc.driver.OracleTypes;

 /**
 * This Bean class used to do the BOM and sub-component Mapping
 * @author rajan
 */
public class GmPartNumberAssemblyBean extends GmBean {
	/**
	 * log Code to Initialize the Logger Class
	 */
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * TODO Auto-generated constructor stub
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmPartNumberAssemblyBean(GmDataStoreVO gmDataStoreVO)throws AppError {
		super(gmDataStoreVO);
	}
	
	/**
	 * fetchPartHeaderDtls  - This method used fetch part number BOM header details
	 * @param strPartNumber
	 * @return
	 * @throws AppError
	 */
	public String fetchPartHeaderDtls(String strPartNumber)	throws AppError {
		String strPartJsonData = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_qa_part_num_assembly.gm_fch_part_assembly_header_dtls", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.execute();
		strPartJsonData = GmCommonClass.parseNull((String) gmDBManager.getString(2));
		gmDBManager.close();
		return strPartJsonData;
	}

	/**
	 * fetchPartAssemblyDtls - This method used to fetch part mapping details
	 * @param strPartNumber, strActionType
	 * @return
	 * @throws AppError
	 */
	public String fetchPartAssemblyDtls(String strPartNumber, String strActionType)	throws AppError {
		String strPartJsonData = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_qa_part_num_assembly.gm_fch_part_assembly_part_dtls", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CLOB);
		gmDBManager.setString(1, strPartNumber);
		gmDBManager.setInt(2, Integer.parseInt(strActionType));
		gmDBManager.execute();
		strPartJsonData = GmCommonClass.parseNull((String) gmDBManager.getString(3));
		gmDBManager.close();
		return strPartJsonData;
	}
	
	/**
	 * savePartAssemblyUploadDtls - This method used to save grid Part numbers BOM details
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	public String savePartAssemblyUploadDtls(HashMap hmParams)	throws AppError {
		log.debug("in savePartAssemblyUploadDtls :");
		GmCommonBean gmCom = new GmCommonBean(getGmDataStoreVO());
		String strPartNumber = GmCommonClass.parseNull((String) hmParams.get("PARTNUMBER"));
		String strPartNumbers = GmCommonClass.parseNull((String) hmParams.get("STRPARTNUMBERS"));
	    String strInputString = GmCommonClass.parseNull((String) hmParams.get("INPUTSTRING"));
	    String strVoidInputString = GmCommonClass.parseNull((String) hmParams.get("VOIDINPUTSTRING")); 
	    String strType = GmCommonClass.parseNull((String) hmParams.get("SUBCOMPONENT"));
	    String strBomFlag = GmCommonClass.parseNull((String) hmParams.get("BOMFLAG")); 
	    String strUserID = GmCommonClass.parseNull((String) hmParams.get("USERID"));
	    String strBomId = GmCommonClass.parseNull((String) hmParams.get("BOMID"));
	    String strLog = GmCommonClass.parseNull((String) hmParams.get("LOG"));
	    String strErrorMsg = "";
	    String strOutBOMId = "";
	    HashMap hmJmsParam = new HashMap();
	    GmConsumerUtil gmConsumerUtil = new GmConsumerUtil();
	    // Database Connection
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_qa_part_num_assembly.gm_sav_part_assembly_upload_dtls", 10);
	    gmDBManager.setString(1, strPartNumber);
	    gmDBManager.setString(2, strPartNumbers);
	    gmDBManager.setString(3, strInputString);
	    gmDBManager.setString(4, strVoidInputString);
	    gmDBManager.setString(5, strType);
	    gmDBManager.setString(6, strBomFlag);
	    gmDBManager.setString(7, strUserID);
	    gmDBManager.setString(8, strBomId);
		gmDBManager.registerOutParameter(9, OracleTypes.CLOB);
	    gmDBManager.registerOutParameter(10, OracleTypes.VARCHAR);
	    gmDBManager.execute();
	    
	    strOutBOMId = GmCommonClass.parseNull(gmDBManager.getString(9));
	    strErrorMsg = GmCommonClass.parseNull(gmDBManager.getString(10));
	         
	    if (strErrorMsg.equals("Y") && !strLog.equals("")) {
	        gmCom.saveLog(gmDBManager, strPartNumber, strLog, strUserID, "26240883");
	        	
	        
	        if(strBomId.equals("") && !strOutBOMId.equals("") && strType.equals("109740")){
		        // to update the comments - set master details.
		        gmCom.saveLog(gmDBManager, strOutBOMId.substring(0, strOutBOMId.length()-1), strLog, strUserID, "1219");
		 
		        // refresh the Redis
		        GmCacheManager gmCacheManager = new GmCacheManager ();  
		        gmCacheManager.removePatternKeys("BOM_LIST*" + getCompId() + "*");
	        }
	      
	        if(strType.equals("109740") && !strOutBOMId.equals("")){
	        	// to update the comments - set mapping details.
		        gmCom.saveLog(gmDBManager, strOutBOMId.substring(0, strOutBOMId.length()-1), strLog, strUserID, "1249");
	        }
	        
	      }
	    gmDBManager.commit();
	    
	 // JMS Call for update the PO status as PO Generated or PO Failed
       if(!strOutBOMId.equals("")){
    	   
	 	String strConsumerClass = GmCommonClass.parseNull(GmJMSConsumerConfigurationBean
	 				.getJmsConfig("BOM_CONSUMER_JOB"));// jmsconsumerconfig
	
	 	String strQueueName = GmCommonClass.parseNull(GmCommonClass
	 		 		.getJmsConfig("IPAD_SYNC_QUEUE"));//used npi jms.properties

	 	String strCompanyInfo ="{\"cmpid\":\""+getGmDataStoreVO().getCmpid()+"\",\"partyid\":\""+getGmDataStoreVO().getPartyid()+"\",\"plantid\":\""+getGmDataStoreVO().getPlantid()+"\"}";	 	
	 	
	 	hmJmsParam.put("COMPANYINFO",strCompanyInfo);
	 	hmJmsParam.put("QUEUE_NAME", strQueueName);
	 	hmJmsParam.put("CONSUMERCLASS", strConsumerClass);
	 	hmJmsParam.put("BOMID", strOutBOMId);
	 	hmJmsParam.put("PARTNUMBER", strPartNumber);
	 	// to set the user id
	 	hmJmsParam.put("USERID", strUserID);
	 	gmConsumerUtil.sendMessage(hmJmsParam);
        }
       
       if(strOutBOMId !="" && strErrorMsg =="Y"){
    	   strErrorMsg = strErrorMsg+"##"+strPartNumber;
       }
	    return strErrorMsg;
	}
	
	/**
	 * 	saveBOMPartDetails  - this method JMS calling save the BOM details 
	 * @param strBOMId, strUserId
	 * @throws AppError
	 */
	public void saveBeanBOMPartDetails(String strBOMId, String strUserId) throws AppError {
		log.debug("==saveBeanBOMPartDetails=="+strBOMId);
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		// set map transaction (send email and log)
		GmSetMapTransactionBean gmSetMapTransactionBean = new GmSetMapTransactionBean (getGmDataStoreVO());
		
		gmDBManager.setPrepareString("gm_pkg_qa_part_num_assembly.gm_upd_bom_part_mapping", 2);
		gmDBManager.setString(1, strBOMId);
		gmDBManager.setString(2, strUserId);
		gmDBManager.execute();
		
		gmDBManager.commit();
	
		// to remove the last comma
		strBOMId = strBOMId.substring(0, strBOMId.length()-1);
		
		// Set list updated / to send email
		//gmSetMapTransactionBean.sendSetListUpdateEmail (strBOMId);
	}

}
