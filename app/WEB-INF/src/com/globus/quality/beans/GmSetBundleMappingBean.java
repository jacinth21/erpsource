/**
 * 
 */
package com.globus.quality.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.beanutils.RowSetDynaClass;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmCacheManager;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author Agilan
 *
 */
public class GmSetBundleMappingBean extends GmBean{
public GmSetBundleMappingBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}


/*	public GmSetBundleMappingBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}*/

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	/*Method: Save Set BundleMapping
	 * Parameter:HashMap
	 * Return:String
	 * Description:save set Bundle Mapping Details
	 * Author:AgilanSingaravelan
	 */
	public String saveSetBundleMapping(HashMap hmParam) throws AppError {
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strSetid = GmCommonClass.parseNull((String) hmParam.get("STRBUNDLEID"));
		String strSetBundleName = GmCommonClass.parseNull((String) hmParam.get("TXTSETNAME"));
		String strSetBundeDesc = GmCommonClass.parseNull((String) hmParam.get("TXTSETDESC"));
        String strSetBundleDtlDesc = GmCommonClass.parseNull((String) hmParam.get("TXTSETDETAILDESC"));
        String strKeywords = GmCommonClass.parseNull((String) hmParam.get("TXTKEYWORDS"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
	    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
	    String strDivision =  GmCommonClass.parseNull((String) hmParam.get("DIVNAME"));
	    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STSNAME"));
	    String strcmpId = GmCommonClass.parseNull((String)getGmDataStoreVO().getCmpid());
	    gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_sav_set_bundle_map",9);
		 
		gmDBManager.setString(1, strSetBundleName);
		gmDBManager.setString(2, strSetBundeDesc);
		gmDBManager.setString(3, strSetBundleDtlDesc);
		gmDBManager.setInt(4, Integer.parseInt(strDivision));
		gmDBManager.setInt(5, Integer.parseInt(strStatus));
		gmDBManager.setString(6, strKeywords);
		gmDBManager.setString(7, strcmpId);
		gmDBManager.setString(8, strSetid);
		gmDBManager.setString(9, strUserId);
		gmDBManager.registerOutParameter(8, OracleTypes.VARCHAR); 
		gmDBManager.execute();
		
		String strSetBundleId = GmCommonClass.parseNull((String) gmDBManager.getString(8));
		if (!strLogReason.equals("")) {
			GmCommonBean gmCommonBean = new GmCommonBean();
			gmCommonBean.saveLog(gmDBManager, strSetBundleId, strLogReason, strUserId, "26240457"); // // 26240457 - Log Code Number for SetbundleSetup.
		}
			gmDBManager.commit();
		return strSetBundleId;
	}
	
	/*Method: fetchSetBundleMapDtls
	 * Parameter:String
	 * Return:HashMap
	 * Description: Fetch set Bundle Mapping Details
	 * Author:AgilanSingaravelan
	 */
	public HashMap fetchSetBundleMapDtls(String strSetMapBundleId) throws AppError {
		ArrayList alReturn = new ArrayList();
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_fetch_set_bundle_map", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strSetMapBundleId);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
	    return hmReturn;
	}
	
	/*Method: fetchSetBundleNameDtls
	 * Return:String
	 * Description:save Fetch set Bundle Name Details
	 * Author:AgilanSingaravelan
	 */
	public ArrayList fetchSetBundleNameDtls() throws AppError {
		ArrayList alReturn = new ArrayList();
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_fetch_set_bundle_name", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		hmReturn.put("ALRETURN", alReturn);
	    return alReturn;
	}
	
	/*Method: fetchSetMapDtls
	 * Parameter:String
	 * Return:HashMap
	 * Description: Fetch set  Map Details
	 * Author:AgilanSingaravelan
	 */
	public HashMap fetchSetMapDtls(String strId) throws AppError,Exception {
		ArrayList alReturn = new ArrayList();
		HashMap hmReturn = new HashMap();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_fetch_set_details", 2);
		gmDBManager.setString(1, strId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		hmReturn = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
		gmDBManager.close();
		return hmReturn;
	}
	
	/*Method: saveSetMappingDtl
	 * Parameter:HashMap
	 * Description:save set  Mapping Details
	 * Author:AgilanSingaravelan
	 */
	public void saveSetMappingDtl(HashMap hmParam) throws AppError{
		String strSetBundleId = "";
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		String strBundleId = GmCommonClass.parseNull((String) hmParam.get("SYSTEMID"));
        String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
        String strSetId = GmCommonClass.parseNull((String) hmParam.get("SETID"));
		String strLogReason = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		  gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_sav_set_map_details", 3);
			gmDBManager.setString(1, strBundleId);			
			gmDBManager.setString(2, strSetId);
			gmDBManager.setString(3, strUserId);
			gmDBManager.execute();			
			
			if (!strLogReason.equals("")) {
				GmCommonBean gmCommonBean = new GmCommonBean();
   				gmCommonBean.saveLog(gmDBManager, strBundleId, strLogReason, strUserId, "26240458");// 26240458 - Log Code Number for SetbundleMapping.
   			}
			gmDBManager.commit();		
		}
	
	
	/*Method:fetchSystemMapping
	 * Parameter:String
	 * Return:ArrayList
	 * Description:save Fetch system  Mapping Details
	 * Author:AgilanSingaravelan
	 */
	public ArrayList fetchSystemMapping(String strSystemId) throws AppError {
		ArrayList alreturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_fch_sys_mapping", 2);
		gmDBManager.setString(1, strSystemId);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		alreturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		gmDBManager.close();
		return alreturn;
	}

	 /**
	   * fetchSystemCompanyMap - This method will be used to load the system company mapping details
	   * 
	   * @param HashMap
	   * @return ArrayList
	   * @throws AppError
	   */
	  public ArrayList fetchSetBundleCompanyMap(HashMap hmParam) throws AppError {
	
		ArrayList alreturn = new ArrayList();
		String strSetBundleId = GmCommonClass.parseNull((String) hmParam.get("STRBUNDLEID"));		
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		
		gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_fch_setbundle_comp_map", 2);
		gmDBManager.setString(1, GmCommonClass.parseNull(strSetBundleId));
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.execute();
		
		alreturn =GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2)));
		
		gmDBManager.close();
		
		return alreturn;
	  }
	  /**
	   * fetchSetID - This method will be used to fetch the set id details
	   * 
	   * @param 
	   * @return ArrayList
	   * @throws AppError
	   */
	  public ArrayList fetchSetID() throws AppError {

			ArrayList alreturn = new ArrayList();			
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());			
			gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_fetch_set_id", 1);
			gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
			gmDBManager.execute();		
			alreturn = GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1)));
			gmDBManager.close();			
			return alreturn;
	 } 
	 /**
	   * fetchSetID - This method will be used to fetch Set Bundle Report Mapping details
	   * 
	   * @param 
	   * @return ArrayList
	   * @throws AppError
	   */
	  public ArrayList fetchSetBundleDtlsToDash(HashMap hmParam) {

		    String strSetBundleName = GmCommonClass.parseZero((String) hmParam.get("SETBUNDLENAME"));
		    String strSetId = GmCommonClass.parseZero((String) hmParam.get("SETID"));
		    String strDivision = GmCommonClass.parseNull((String) hmParam.get("DIVNAME"));
		    String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));

		    StringBuffer sbQuery = new StringBuffer();
		    ArrayList alReturn = new ArrayList();
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    String strCompId = getGmDataStoreVO().getCmpid();

		    sbQuery.setLength(0);
		    sbQuery.append(" SELECT t2075.C2075_SET_BUNDLE_ID SETBUNDLEID,t2075.C2075_SET_BUNDLE_NM SETBUNDLENM, C2075_KEYWORDS KEYWORDS, t2076.c207_SET_ID SETID, t207.C207_SET_NM SETNAME");
		    sbQuery.append(" , GET_CODE_NAME(t2075.C901_STATUS) STATUS, get_code_name(t207.c207_type) SETYPE , get_division_name(t2075.C1910_DIVISION_ID) DIVNAME");
		    sbQuery.append(" , GET_USER_NAME(t2075.C2075_LAST_UPDATED_BY) UPDATEDBY,t2075.C2075_LAST_UPDATED_DATE UPDATEDDATE ");
	
		    sbQuery.append(" FROM t2076_set_bundle_details t2076,T2075_SET_BUNDLE t2075,t207_set_master t207");
		    sbQuery.append(" WHERE t2076.c2075_set_bundle_id = t2075.c2075_set_bundle_id AND t207.c207_set_id = t2076.c207_set_id");		    
	
		    if ((!strSetId.equals("0")) && (!strSetId.equals(""))) {
			      sbQuery.append(" AND t2076.c207_SET_ID = '" + strSetId + "'");
			}
		   
		    if ((!strSetBundleName.equals("0")) && (!strSetBundleName.equals(""))) {
		      sbQuery.append(" AND t2076.C2075_SET_BUNDLE_ID = '" + strSetBundleName + "'");
		    }
		    if ((!strStatus.equals("0")) && (!strStatus.equals("")))  {
		      sbQuery.append(" AND t2075.C901_STATUS IN (" + strStatus + ")");
		    }
		    if ((!strDivision.equals("0")) && (!strDivision.equals(""))) {
			      sbQuery.append(" AND t2075.C1910_DIVISION_ID IN (" + strDivision + ")");
		    }
		    sbQuery.append(" AND t2076.C2076_VOID_FL IS NULL AND t2075.c2075_VOID_FL IS NULL AND t207.c207_VOID_FL IS NULL");
		    sbQuery.append(" order by SETBUNDLENM ASC"); 
		    log.debug("sbQuery.toString()>>>>>>" + sbQuery.toString());
		    alReturn = gmDBManager.queryMultipleRecords(sbQuery.toString());
		    gmDBManager.close();
		    return alReturn;
		  }
	  /**
	   * fetchSetBundleNameList - To fetch the set Bundle Name.
	   * 
	   * @param HashMap
	   * @return String
	   * @throws AppError
	 */
	  public String fetchSetBundleNameList(HashMap hmParam) throws AppError {
		 
		  String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		  String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		  int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		  String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		  
		  GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		   
		  gmCacheManager.setKey(strKey);
		  gmCacheManager.setSearchPrefix(searchPrefix);
		  gmCacheManager.setMaxFetchCount(iMaxCount);
		  gmCacheManager.setMaxFetchCount(iMaxCount);
		  HashMap hmdata = new HashMap();
		  hmdata.put("TYPE", strType);
		  
		  String strReturn = gmCacheManager.returnJsonString(loadSetBundleNmList(hmParam));
		
		  return strReturn; 
	  }
	  
	  /**
	   * loadSetBundleNmList - To fetch the set Bundle Name details and keywords details
	   * 
	   * @param HashMap
	   * @return String
	   * @throws AppError
	 */
	  public String loadSetBundleNmList(HashMap hmParam) throws AppError {
		  
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
		  StringBuffer sbQuery = new StringBuffer();
		  String strCompId = getGmDataStoreVO().getCmpid();
		  String strplantId = getGmDataStoreVO().getPlantid();
		  sbQuery
	      .append(" SELECT * FROM ( ");
		  sbQuery
	        .append(" SELECT t2075A.c2075_set_bundle_id ID, t2075.c2075_set_bundle_nm NAME , NVL(t2075A.c2075a_unque_keyword,'NO_KEYWORDS') ALTNAME ");
		  sbQuery
	      .append(" FROM T2075_SET_BUNDLE T2075 , T2075A_SET_BUNDLE_KEYWORD T2075A , T2081_SET_BUNDLE_COMP_MAPPING T2081 , T2076_SET_BUNDLE_DETAILS T2076 ");
		  sbQuery
	      .append(" WHERE T2075.C2075_SET_BUNDLE_ID = T2075A.C2075_SET_BUNDLE_ID(+) ");
		  sbQuery
	      .append(" AND T2075.C2075_SET_BUNDLE_ID = T2081.C2075_SET_BUNDLE_ID ");
		  sbQuery
	      .append(" AND T2075.C2075_SET_BUNDLE_ID = T2076.C2075_SET_BUNDLE_ID ");
		  sbQuery
	      .append(" AND t2076.C207_SET_ID in ( SELECT DISTINCT (t504.c207_set_id) SETID ");
		  sbQuery.append(" FROM t504_consignment t504, t504a_consignment_loaner t504a, T207_SET_MASTER t207, T2080_SET_COMPANY_MAPPING t2080 "); 
		  sbQuery.append(" WHERE t504.c504_consignment_id = t504a.c504_consignment_id AND t207.c207_set_id = t504.c207_set_id AND T504.C504_VOID_FL IS NULL AND t504.c504_type = 4127"); 
		  sbQuery.append(" AND t504.c504_status_fl = '4' AND t504a.C504A_STATUS_FL != '60' AND t504a.c504a_void_fl IS NULL AND t207.C207_VOID_FL IS NULL AND T2080.C207_SET_ID = T207.C207_SET_ID AND t2080.c2080_void_fl IS NULL "); 
		  sbQuery.append(" AND T2080.C1900_COMPANY_ID IN (SELECT C1900_COMPANY_ID FROM T5041_PLANT_COMPANY_MAPPING WHERE C5041_PRIMARY_FL = 'Y' AND C5040_PLANT_ID= '"+strplantId+"' AND C5041_VOID_FL IS NULL)"); 
		  sbQuery.append(" AND t504a.C5040_PLANT_ID = '"+strplantId+"' AND t207.C207_TYPE != 4078)"); 
		  sbQuery.append(" AND T2075.C901_STATUS ='107021' AND C2075_VOID_FL IS NULL"); 
		  sbQuery.append(" AND C2075A_VOID_FL IS NULL AND C2076_VOID_FL IS NULL AND C2081_VOID_FL IS NULL AND T2081.C1900_COMPANY_ID = '"+strCompId+"')"); 
		  sbQuery.append(" GROUP BY ID, NAME, ALTNAME ORDER BY NAME ASC "); 
		  log.debug("sbQuery.toString() --> "+sbQuery.toString());

		  return sbQuery.toString();
		  
	} 
	  	  
	  /**
       * loadSetBundleDetails - To fetch the set details mapped to setbundle.
       * 
       * @param String
       * @return ArrayList
       * @throws AppError
     */
      public ArrayList loadSetBundleDetails(String strSetBundleId,String strSetId) throws AppError {
            GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
            String strCompId = getGmDataStoreVO().getCmpid();
            String strPlantId = getGmDataStoreVO().getPlantid();
            ArrayList alReturn = new ArrayList();
            if (!strSetBundleId.equals("")) {
            gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_fch_setBundledetails", 5);
            gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
            gmDBManager.setString(4, strPlantId);
            gmDBManager.setString(3, strCompId);  
            gmDBManager.setString(2, strSetId);
            gmDBManager.setString(1, strSetBundleId);                       
            gmDBManager.execute();
            alReturn =
              GmCommonClass.parseNullArrayList(gmDBManager.returnArrayList((ResultSet) gmDBManager
                  .getObject(5)));
            gmDBManager.close();
            }
        return alReturn;
      }
	  /**
	   * fetchSetBundleNmList - To fetch the set Bundle Name.
	   * 
	   * @param HashMap
	   * @return String
	   * @throws AppError
	 */
	  public String fetchSetBundleNmList(HashMap hmParam) throws AppError {
		 
		  String strKey = GmCommonClass.parseNull((String) hmParam.get("KEY"));
		  String searchPrefix = GmCommonClass.parseNull((String) hmParam.get("SEARCH_KEY"));
		  int iMaxCount = (Integer) hmParam.get("MAX_CNT");
		  String strType = GmCommonClass.parseNull((String) hmParam.get("TYPE"));
		  
		  GmCacheManager gmCacheManager = new GmCacheManager(getGmDataStoreVO());
		  gmCacheManager.setKey(strKey);
		  gmCacheManager.setSearchPrefix(searchPrefix);
		  gmCacheManager.setMaxFetchCount(iMaxCount);
		  gmCacheManager.setMaxFetchCount(iMaxCount);
		  HashMap hmdata = new HashMap();
		  hmdata.put("TYPE", strType);
		  
		  String strReturn = gmCacheManager.returnJsonString(loadSetBndNmList(hmParam));
		
		  return strReturn; 
	  }
	  
	  /**
	   * loadSetBndNmList - To fetch the set Bundle Name details
	   * 
	   * @param HashMap
	   * @return String
	   * @throws AppError
	 */
	  public String loadSetBndNmList(HashMap hmParam) throws AppError {
		  
		  GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());	
		  StringBuffer sbQuery = new StringBuffer();
		  String strCompId = getGmDataStoreVO().getCmpid();
		  sbQuery
		        .append(" SELECT t2075.C2075_SET_BUNDLE_ID ID, t2075.c2075_set_bundle_nm NAME, NVL(t2075A.c2075a_unque_keyword,'NO_KEYWORDS') ALTNAME ");
		  sbQuery.append(" FROM t2075_set_bundle t2075 , T2075A_SET_BUNDLE_KEYWORD T2075A,t2081_set_bundle_comp_mapping t2081 ");
		  sbQuery.append(" WHERE t2075.c2075_void_fl IS NULL AND t2075.C2075_SET_BUNDLE_ID = T2075A.C2075_SET_BUNDLE_ID(+) AND t2075.C2075_SET_BUNDLE_ID = T2081.C2075_SET_BUNDLE_ID ");
		  sbQuery.append(" AND T2075A.C2075A_VOID_FL IS NULL AND t2081.C2081_VOID_FL IS NULL");
		  return sbQuery.toString();

	 } 	  
	  /**
	   * fetchHistory - This method will be used to fetch history details 
	   * 
	   * @param strBundleId
	   * @return ArrayList
	   * @throws AppError
	   */
	  public RowSetDynaClass fetchHistory(String strBundleId) throws AppError {
 
		  	RowSetDynaClass rdHistory = null;
			ArrayList alreturn = new ArrayList();			
			GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());			
			gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_fetch_history", 2);
			gmDBManager.setString(1, GmCommonClass.parseNull(strBundleId));
			gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			gmDBManager.execute();		
			rdHistory = gmDBManager.returnRowSetDyna((ResultSet) gmDBManager.getObject(2));
			gmDBManager.close();			
			return rdHistory;
	 } 
	  /**
	   * fetchKeywords - This method will be used to fetch keywords count for set bundle id 
	   * 
	   * @param strBundleId
	   * @return int
	   * @throws AppError
	   */	  
	  public int fetchKeywords(String strBundleId )throws AppError {
			int count=0;
		  	GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());			
			gmDBManager.setPrepareString("gm_pkg_set_bundle_map.gm_fetch_key_count", 2);
			gmDBManager.setString(1, GmCommonClass.parseNull(strBundleId));
			gmDBManager.registerOutParameter(2, OracleTypes.NUMBER);
			gmDBManager.execute();
			count = gmDBManager.getInt(2);
			gmDBManager.close();
		    return count;
	  }

}
