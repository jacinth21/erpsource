package com.globus.quality.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmWSUtil;
import com.globus.operations.logistics.beans.GmInHouseSetsReportBean;
import com.globus.operations.shipping.beans.GmScanControlItemBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.GmTxnPartDtlVO;
import com.globus.valueobject.operations.shipping.GmShipScanReqVO;

public class GmPartAttributeBean extends GmBean {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public GmPartAttributeBean() {
    super(GmCommonClass.getDefaultGmDataStoreVO());
  }


  /**
   * Constructor will populate company info.
   * 
   * @param gmDataStore
   */
  public GmPartAttributeBean(GmDataStoreVO gmDataStore) {
    super(gmDataStore);
  }


  /**
	 * fetchPartProcessRptData  This method will fetch the Part Process Report
	 * @param 					Hashmap hmParam
	 * @return 					ArrayList
	 * @exception 				AppError
	 */
	public ArrayList fetchPartProcessRptData(HashMap hmParam) throws AppError {
		ArrayList alReport 		= new ArrayList();
		String strSystemID 		= GmCommonClass.parseZero((String) hmParam.get("SYSTEMID"));		
		String strProjectID 	= GmCommonClass.parseZero((String) hmParam.get("PROJECTID"));
		String strVendor 		= GmCommonClass.parseZero((String) hmParam.get("VENDORID"));
		String strPartNum 		= GmCommonClass.parseNull((String) hmParam.get("PARTNUM"));		
    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_qa_part_process.gm_fch_part_process_rpt", 5);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.setString(1, strVendor);
		gmDBManager.setString(2, strSystemID);
		gmDBManager.setString(3, strProjectID);		
		gmDBManager.setString(4, strPartNum);
		gmDBManager.execute(); 
		alReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
		gmDBManager.close();
		return alReport;
	} //End of fetchPartProcessRptData
	
	public List<GmTxnPartDtlVO> fetchPartDetails(String strTxnId, String strTxnType) throws AppError
	{
		GmTxnPartDtlVO gmTxnPartDtlVO = new GmTxnPartDtlVO();
  		GmShipScanReqVO gmShipScanReqVO = new GmShipScanReqVO();
  		
   		GmWSUtil gmWSUtil = new GmWSUtil();
  		List<GmTxnPartDtlVO> gmPartDtlVOList = new ArrayList<GmTxnPartDtlVO>();
		HashMap hmParam = new HashMap();
		
		// If the transaction type is 50182 -> loaner 
		if(strTxnType.equals("50182"))
  		{
			GmInHouseSetsReportBean gmInHouseRptBean = new GmInHouseSetsReportBean();
			ArrayList alList = gmInHouseRptBean.getSetConsignDetails(strTxnId, "");
			gmPartDtlVOList = gmWSUtil.getVOListFromHashMapList(alList, gmTxnPartDtlVO, gmTxnPartDtlVO.getLNMappingProperties());
  		}
		// 50181 -> consignments / item consignments  / 50186 -> In House Loaner Items
		else if (strTxnType.equals("50181") || strTxnType.equals("50186"))
  		{
			GmScanControlItemBean gmScanControlledItems = new GmScanControlItemBean();
			ArrayList alList = gmScanControlledItems.fetchConsignedItems(strTxnId);
			gmPartDtlVOList = gmWSUtil.getVOListFromHashMapList(alList, gmTxnPartDtlVO, gmTxnPartDtlVO.getConsgMappingProperties());
  		}
  		// 50180 ->  Order
  		else if (strTxnType.equals("50180")) 
  		{
  			// The code has been modified because the earlier code was displaying the loaner parts on device.
  			GmScanControlItemBean gmScanControlledItems = new GmScanControlItemBean();
			ArrayList alList = gmScanControlledItems.fetchOrderItems(strTxnId);
			gmPartDtlVOList = gmWSUtil.getVOListFromHashMapList(alList, gmTxnPartDtlVO, gmTxnPartDtlVO.getOrderMappingProperties());
  		}
  		// 50183 -> Loaner Extension
  		else if (strTxnType.equals("50183"))
  		{
  			GmScanControlItemBean gmScanControlledItems = new GmScanControlItemBean();
			ArrayList alList = gmScanControlledItems.fetchLoanedItems(strTxnId);
			gmPartDtlVOList = gmWSUtil.getVOListFromHashMapList(alList, gmTxnPartDtlVO, gmTxnPartDtlVO.getLoanerExtnMappingProperties());
  		}
  		
  		if(gmPartDtlVOList != null && gmPartDtlVOList.size() == 0)
  			throw new AppError(AppError.APPLICATION,"0003");
  		
  		return gmPartDtlVOList;
	}
	
}