package com.globus.quality.beans;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;
import java.text.ParseException;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.quality.beans.GmEventEvalBean;
import com.globus.valueobject.quality.GmEventEvalVO;

import java.time.LocalDateTime;  
import java.text.DateFormat;  
import java.text.SimpleDateFormat;

public class GmEventEvalBean extends GmBean{
	
	/**
	 * TODO Auto-generated constructor stub
	 * @param gmDataStoreVO
	 * @throws AppError
	 */
	public GmEventEvalBean(GmDataStoreVO gmDataStoreVO) throws AppError {
		super(gmDataStoreVO);
		// TODO Auto-generated constructor stub
	}

	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	 
	 /**
	 * Save the form with/without generate pdf
	 * @param hmInput
	 * @return
	 * @throws ParseException
	 */
	public HashMap save(HashMap hmInput) throws ParseException {
		 	log.debug("GmEventEvalBean>>>>save>>>hmInput>>>"+hmInput);
			HashMap hmResult=new HashMap();	 
		 	String strCompDate = GmCommonClass.parseNull((String) hmInput.get("CMPDFMT"));
		 	String partid = GmCommonClass.parseNull((String) hmInput.get("PARTID"));
		    String lotnumber = GmCommonClass.parseNull((String) hmInput.get("LOTNUMBER"));
		    String qty = GmCommonClass.parseNull((String) hmInput.get("QTY"));
		    String loanerfl = GmCommonClass.parseNull((String) hmInput.get("LOANERFL"));
		    String consignmentfl = GmCommonClass.parseNull((String) hmInput.get("CONSIGNMENTFL"));
		    String soldfl = GmCommonClass.parseNull((String) hmInput.get("SOLDFL"));
		    String desc = GmCommonClass.parseNull((String) hmInput.get("DESCR"));
		    String evntdt = GmCommonClass.parseNull((String) hmInput.get("EVNTDT"));
		    String orgdate = GmCommonClass.parseNull((String) hmInput.get("ORGDATE"));
		    String dateshipped = GmCommonClass.parseNull((String) hmInput.get("DATESHIPPED"));
		    String evnthappen = GmCommonClass.parseNull((String) hmInput.get("EVNTHAPPEN"));
		    String rptcond = GmCommonClass.parseNull((String) hmInput.get("RPTCOND"));
		    String revsurg = GmCommonClass.parseNull((String) hmInput.get("REVSURG"));
		    String revsurgtxt = GmCommonClass.parseNull((String) hmInput.get("REVSURGTXT"));
		    String advefct = GmCommonClass.parseNull((String) hmInput.get("ADVEFCT"));
		    String typofsurg = GmCommonClass.parseNull((String) hmInput.get("TYPOFSURG"));
		    String surg = GmCommonClass.parseNull((String) hmInput.get("SURG"));
		    String hospital = GmCommonClass.parseNull((String) hmInput.get("HOSPITAL"));
		    String postoprt = GmCommonClass.parseNull((String) hmInput.get("POSTOPRT"));
		    String prcout = GmCommonClass.parseNull((String) hmInput.get("PRCOUT"));
		    String replacement = GmCommonClass.parseNull((String) hmInput.get("REPLACEMENT"));
		    String partaval = GmCommonClass.parseNull((String) hmInput.get("PARTAVAL"));
		    String shipped = GmCommonClass.parseNull((String) hmInput.get("SHIPPED"));
		    String decontaminated = GmCommonClass.parseNull((String) hmInput.get("DECONTAMINATED"));
		    String carrier = GmCommonClass.parseNull((String) hmInput.get("CARRIER"));
		    String trknumber = GmCommonClass.parseNull((String) hmInput.get("TRKNUMBER"));
		    String fldservice = GmCommonClass.parseNull((String) hmInput.get("FLDSERVICE"));
		    String repid = GmCommonClass.parseNull((String) hmInput.get("REPID"));
		    String cmpid = GmCommonClass.parseNull((String) hmInput.get("CMPID"));
		    String formid =  GmCommonClass.parseNull((String) hmInput.get("EVALFORMID"));
		    
		    ArrayList alReturn = new ArrayList();
		     GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		   
		    Date dtEvntdt = null;
		    Date dtorgdate = null;
		    Date dtDateshipped = null;
		  
		    dtEvntdt = GmCommonClass.getStringToDate(evntdt,strCompDate);
		    dtorgdate = GmCommonClass.getStringToDate(orgdate, strCompDate);
		    dtDateshipped = GmCommonClass.getStringToDate(dateshipped,strCompDate);
		   
		     		    
		    gmDBManager.setPrepareString("gm_pkg_event_eval_txn.gm_sav_event_eval", 31);
		    gmDBManager.setString(1, GmCommonClass.parseNull(partid));
		    gmDBManager.setString(2, GmCommonClass.parseNull(lotnumber));
		    gmDBManager.setString(3, GmCommonClass.parseNull(qty));
		    gmDBManager.setString(4, GmCommonClass.parseNull(loanerfl));
		    gmDBManager.setString(5, GmCommonClass.parseNull(consignmentfl));
		    gmDBManager.setString(6, GmCommonClass.parseNull(soldfl));
		    gmDBManager.setString(7, GmCommonClass.parseNull(desc));
		    gmDBManager.setDate(8, dtEvntdt); 
		    gmDBManager.setString(9, GmCommonClass.parseNull(evnthappen));
		    gmDBManager.setString(10, GmCommonClass.parseNull(rptcond));
		    gmDBManager.setString(11, GmCommonClass.parseNull(revsurg));
		    gmDBManager.setString(12, GmCommonClass.parseNull(revsurgtxt));
		    gmDBManager.setString(13, GmCommonClass.parseNull(advefct));
		    gmDBManager.setString(14, GmCommonClass.parseNull(typofsurg));
		    gmDBManager.setString(15, GmCommonClass.parseNull(surg));
		    gmDBManager.setString(16, GmCommonClass.parseNull(hospital));
		    gmDBManager.setString(17, GmCommonClass.parseNull(postoprt));
		    gmDBManager.setDate(18, dtorgdate);
		    gmDBManager.setString(19, GmCommonClass.parseNull(prcout));
		    gmDBManager.setString(20, GmCommonClass.parseNull(replacement));
		    gmDBManager.setString(21, GmCommonClass.parseNull(partaval));
		    gmDBManager.setString(22, GmCommonClass.parseNull(shipped));
		    gmDBManager.setString(23, GmCommonClass.parseNull(decontaminated));
		    gmDBManager.setDate(24, dtDateshipped);
		    gmDBManager.setString(25, GmCommonClass.parseNull(carrier));
		    gmDBManager.setString(26, GmCommonClass.parseNull(trknumber));
		    gmDBManager.setString(27, GmCommonClass.parseNull(fldservice));
		    gmDBManager.setString(28, GmCommonClass.parseNull(repid));
		    gmDBManager.setString(29, GmCommonClass.parseNull(cmpid));
		    gmDBManager.setString(30, formid);
		    gmDBManager.registerOutParameter(30, OracleTypes.VARCHAR);
		    gmDBManager.registerOutParameter(31, OracleTypes.VARCHAR);
		    gmDBManager.execute();
		    String strFormID = gmDBManager.getString(30);
		    String strErrMsg = gmDBManager.getString(31);
		    gmDBManager.commit();
		    gmDBManager.close();
		    hmResult.put("FORMID", strFormID);
		    hmResult.put("ERRORMSG", strErrMsg);
		    log.debug("GmEventEvalBean>>>save>>>>hmResult>>>"+hmResult);
		    return hmResult;
		  }
	 
	 
	 /**
	  * fetch the form details 
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	public List<GmEventEvalVO> fetchForm (HashMap hmParams) throws AppError {

	        log.debug("GmEventEvalBean>>fetchForm>>>>>hmParams>>>"+hmParams);
		    GmDBManager gmDBManager = new GmDBManager();
		    GmEventEvalVO gmEvalFormVO = new GmEventEvalVO();
		    List<GmEventEvalVO> evalForm = null;
		    
		    String formid = GmCommonClass.parseNull((String) hmParams.get("EVALFORMID"));
		    
		    
		    gmDBManager.setPrepareString("gm_pkg_event_eval_txn.gm_fch_event_eval", 2);
		    gmDBManager.setString(1, GmCommonClass.parseNull(formid));
		    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		    gmDBManager.execute();
		    evalForm = gmDBManager.returnVOList((ResultSet) gmDBManager.getObject(2), gmEvalFormVO,gmEvalFormVO.getMappingProperties());
		    gmDBManager.commit();
		    gmDBManager.close();
		    log.debug("GmEventEvalBean>>fetchForm>>>>>evalForm>>>"+evalForm);
		    log.debug("GmEventEvalBean>>fetchForm>>>>>evalForm>>>>size>>"+evalForm.size());
		    return evalForm;
		     
		  }
	 /**
	  * form data to generate PDF
	 * @param strFileName
	 * @return
	 * @throws AppError
	 */
	public HashMap fetchPDFData (HashMap hmInput) throws AppError { 
		 log.debug("fetchPDFData>>>>>input>>>"+hmInput);
		 
		 String formid = GmCommonClass.parseNull((String) hmInput.get("FORMID"));
	     String cmpdfmt = GmCommonClass.parseNull((String) hmInput.get("CMPDFMT"));
		 HashMap hmData =new HashMap();
		  GmDBManager gmDBManager = new GmDBManager();
		    GmEventEvalVO gmEvalFormVO = new GmEventEvalVO();
		    List<GmEventEvalVO> evalForm = null;
		    
		    //String formid = GmCommonClass.parseNull(strFileName);
		    
		    gmDBManager.setPrepareString("gm_pkg_event_eval_txn.gm_fetch_pdf", 3);
		    gmDBManager.setString(1, GmCommonClass.parseNull(formid));
		    gmDBManager.setString(2, GmCommonClass.parseNull(cmpdfmt));
		    gmDBManager.registerOutParameter(3, OracleTypes.CURSOR);
		    gmDBManager.execute();
		    hmData = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(3));
		    gmDBManager.commit();
		    gmDBManager.close();
		    log.debug("fetchPDFData>>>>>hmData>>>"+hmData);
		    return hmData;
		     
		  }
	 

	 
	
	 /**
	  * list the forms in dashboard
	 * @param hmInput
	 * @return
	 * @throws AppError
	 * @throws ParseException
	 */
	public ArrayList fetchformList(HashMap hmInput) throws AppError, ParseException {
			log.debug("GmEventEvalBean>>fetchformList>>>>>hmInput>>>"+hmInput);
		    HashMap evalFormList = new HashMap();
		    GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		    GmEventEvalVO gmEvalFormVO = new GmEventEvalVO();
		    ArrayList alReturn = new ArrayList();
		    
		    
		    
		    String strCompDate = GmCommonClass.parseNull((String) hmInput.get("CMPDFMT"));
		    String fromdt = GmCommonClass.parseNull((String) hmInput.get("FROMDATE"));
		    String todt = GmCommonClass.parseNull((String) hmInput.get("TODATE"));
		    String statusfl = GmCommonClass.parseNull((String) hmInput.get("VIODFL"));
		    String repid = GmCommonClass.parseNull((String) hmInput.get("REPID"));
		    
		    
		    	       
		    StringBuffer sbQuery = new StringBuffer();
		    sbQuery.append("SELECT t3106.c3106_event_eval_form_id evalformid,  to_char(t3106.C3106_CREATED_DT,'"+strCompDate+"') CREATEDATE,t3106.c3106_event_detailed_desc   DESCR, to_char(t3106.c3106_event_dt,'"+strCompDate+"') EVNTDT, DECODE(t3106.C3106_PDF_CREATED,null,'Draft','Submitted') STATUS ");
		    sbQuery.append(" FROM T3106_Event_Eval_Form t3106 where  C3106_VOID_FL is null and T3106.C703_Sales_Rep_Id = '" + repid + "'");
		    
		    
		    if (statusfl.equals("1"))
		        sbQuery.append(" AND C3106_PDF_CREATED IS NULL ");
		    if (statusfl.equals("2"))
		        sbQuery.append(" AND C3106_PDF_CREATED IS NOT NULL ");
		    if (!fromdt.equals(""))
		        sbQuery.append(" AND T3106.C3106_CREATED_DT >= to_date('" + fromdt + "','"+strCompDate+"') AND trunc(T3106.C3106_CREATED_DT) <= to_date('" + todt + "','"+strCompDate+"')");
		    sbQuery.append(" order by C3106_CREATED_DT,c3106_event_eval_form_id ");
		    log.debug("GmEventEvalBean>>fetchformList>>>>>sbQuery>>>"+sbQuery.toString());
		    		     
		    alReturn =
		            GmCommonClass.parseNullArrayList(gmDBManager.queryMultipleRecords(sbQuery.toString()));
		    log.debug("GmEventEvalBean>>fetchformList>>>>>alReturn>>>"+alReturn);
		    log.debug("GmEventEvalBean>>fetchformList>>>>>alReturn>>size>"+alReturn.size());
		    return alReturn;
		  }
	 
	
	 	
	 /**
	 * void the form
	 * @param hmParams
	 * @return
	 * @throws AppError
	 */
	public List<GmEventEvalVO> voidList (HashMap hmParams) throws AppError {
		    GmDBManager gmDBManager = new GmDBManager();
		    GmEventEvalVO gmEvalFormVO = new GmEventEvalVO();
		    List<GmEventEvalVO> evalForm = null;
		    String repid = GmCommonClass.parseNull((String) hmParams.get("REPID"));
		    String formid = GmCommonClass.parseNull((String) hmParams.get("EVALFORMID"));
		    
		    gmDBManager.setPrepareString("gm_pkg_event_eval_txn.gm_eval_form_void", 2);
		    gmDBManager.setString(1, GmCommonClass.parseNull(repid));
		    gmDBManager.setString(2, GmCommonClass.parseNull(formid));
		    gmDBManager.execute();
		    gmDBManager.commit();
		    gmDBManager.close();
			return evalForm;
		     
		  }


		 /**
		 * form data to email
		 * @param strFileName
		 * @return
		 * @throws AppError
		 */
		public HashMap fetchFormData (String strFileName) throws AppError { 
			 HashMap hmData =new HashMap();
			  GmDBManager gmDBManager = new GmDBManager();
			    GmEventEvalVO gmEvalFormVO = new GmEventEvalVO();
			    List<GmEventEvalVO> evalForm = null;
			    
			    String formid = GmCommonClass.parseNull(strFileName);
			    
			    gmDBManager.setPrepareString("gm_pkg_event_eval_txn.gm_fch_event_eval", 2);
			    gmDBManager.setString(1, GmCommonClass.parseNull(formid));
			    gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
			    gmDBManager.execute();
			    hmData = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(2));
			    gmDBManager.commit();
			    gmDBManager.close();
			    return hmData;
			     
			  }
		 
		 
		
		 /**
		 * update the created date
		 * @param strFileName
		 * @return
		 * @throws AppError
		 */
		public String updateCreatedate(String strFileName) throws AppError {
			  
			   GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			   gmDBManager.setPrepareString("gm_pkg_event_eval_txn.gm_eval_form_date", 1);
			   gmDBManager.setString(1, strFileName);
			   gmDBManager.execute();
			   gmDBManager.commit();
			   gmDBManager.close();
			  
				return strFileName;
			}
		 
		 /**
		 * update the Email Sent
		 * @param strevalformid
		 * @return
		 * @throws AppError
		 */
		public String updateEmailSent(String strevalformid) throws AppError {
			  
			   GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
			   gmDBManager.setPrepareString("gm_pkg_event_eval_txn.gm_eval_email_sent", 1);
			   gmDBManager.setString(1, strevalformid);
			   gmDBManager.execute();
			   gmDBManager.commit();
			   gmDBManager.close();
			   return strevalformid;
		}
			   
			     
	  }


