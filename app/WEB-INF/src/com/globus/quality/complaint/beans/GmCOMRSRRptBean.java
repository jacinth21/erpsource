package com.globus.quality.complaint.beans;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;
import org.apache.log4j.Logger;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmEmailProperties;
import com.globus.common.beans.GmJasperMail;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.common.util.GmTemplateUtil;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;

public class GmCOMRSRRptBean extends GmBean{
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	  public GmCOMRSRRptBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  public GmCOMRSRRptBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
	public ArrayList fetchIncidentDetails(HashMap hmParam) throws AppError {
		log.debug("Enter into GmCOMRSRRptBean ");
		
		GmCommonClass gmCommon = new GmCommonClass();
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		String strIncidentID  = GmCommonClass.parseNull((String) hmParam.get("COMRSRID"));
		String strIssueType = GmCommonClass.parseNull((String) hmParam.get("ISSUETYPE"));
		String strUserID  = GmCommonClass.parseNull((String)hmParam.get("USERID"));
		if(strIssueType.equals("0")){
			strIssueType = "";
		}
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_rpt.gm_fch_incident_info",4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.setString(1, strIncidentID);
		gmDBManager.setString(2, strIssueType);
		gmDBManager.setString(3, strUserID);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(4));
		gmDBManager.close();
		return alReturn;
	}
	
	/**
	 * fetchPendingCmp  This method will fetch the Pending COM/RSR Details
	 * @param 		No Param
	 * @return 		ArrayList
	 * @exception 	AppError
	 */
	public ArrayList fetchPendCOMRSRDash() throws AppError {
		ArrayList alReport = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager(getGmDataStoreVO());
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_rpt.gm_fch_pend_com_rsr_dash", 1);
		gmDBManager.registerOutParameter(1, OracleTypes.CURSOR);
		gmDBManager.execute(); 
		alReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(1));
		gmDBManager.close();
		return alReport;
	} //End of fetchPendingCmp
	
	/**
	 * fetchPendingCmp  This method will fetch the RA Details, Part Deatails and REP Details
	 * @param 		HashMap hmParam
	 * @return 		HASHMAP
	 * @exception 	AppError
	 */
	public HashMap fetchIncidentRAInfo(HashMap hmParam) throws AppError {
		
		HashMap hmResult = new HashMap();
		String strRepID = GmCommonClass.parseNull((String)hmParam.get("COMPLAINTNAME"));
		String strPartNumber = GmCommonClass.parseNull((String)hmParam.get("PARTNUM"));
		String strRaID = GmCommonClass.parseNull((String)hmParam.get("RAID"));
		if(strRepID.equals("0")){
			strRepID = "";
		}
		GmDBManager gmDBManager = new GmDBManager ();		
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_rpt.gm_fch_incident_RA_info", 4);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);		
		gmDBManager.setString(1, strRaID);
		gmDBManager.setString(2, strPartNumber);
		gmDBManager.setString(3, strRepID);
		gmDBManager.execute(); 
		hmResult = gmDBManager.returnHashMap((ResultSet) gmDBManager.getObject(4));		
		gmDBManager.close();	
		return hmResult;
	} //End of fetchPendingCmp
	
	/**
	 * fetchQAEvaluation  This method will fetch QA Evaluation details
	 * @return 		HASHMAP
	 * @exception 	AppError
	 */
	public HashMap fetchQAEvaluation(String strRefId) throws AppError {
		
		HashMap hmResult = new HashMap();
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_rpt.gm_fch_QAEvaluation",2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strRefId);
		gmDBManager.execute();
		hmResult = gmDBManager.returnHashMap((ResultSet)gmDBManager.getObject(2));
		gmDBManager.close();
		return hmResult;
	}
	
	/**
	 * validateID - This Method is used to check RefId in DB.
	 * 
	 * @param  String  strRefId
	 * @return String  strRefType
	 * @exception AppError
	 */
	public String validateID(String strRefID, String strType)	throws AppError {

		// validate the file info
		GmDBManager gmDBManager = new GmDBManager();
		log.debug("validate");
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_process.gm_validate_id", 4);
		gmDBManager.registerOutParameter(3, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strRefID);
		gmDBManager.setString(2, strType);
		//When load CAPA/SCAR/COM/RSR screen the default value is true and upload screen only false. It should throw error in action.
		gmDBManager.setString(4, "true");
		gmDBManager.execute();
		String strRefType = GmCommonClass.parseNull(gmDBManager.getString(3));
		gmDBManager.close();
		return strRefType;
	}
	
	/**
	 * fetchADList  This method will fetch the Area Direct Details
	 * @param 		No Param
	 * @return 		ArrayList
	 * @exception 	AppError
	 */
	public ArrayList fetchADList(String strStatus) throws AppError {
		ArrayList alReport = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager ();	
		
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT GET_USER_NAME(C101_USER_ID) AD_NAME,  C101_USER_ID AD_ID ");
		sbQuery.append(" FROM T708_REGION_ASD_MAPPING T708 ");
		sbQuery.append(" WHERE T708.C901_USER_ROLE_TYPE = 8000");
		if(strStatus.equals("Active"))
		{
			sbQuery.append( " AND T708.C708_DELETE_FL IS NULL");
		}
		sbQuery.append(" GROUP BY C101_USER_ID ORDER BY AD_NAME");
		alReport = gmDBManager.queryMultipleRecords(sbQuery.toString());		

		gmDBManager.close();
		return alReport;
	} //End of fetchADList
	
	/**
	 * fetchEnggEvaluation  This method will fetch Engineering Evaluation details
	 * @return 		ArrayList
	 * @exception 	AppError
	 */
	public ArrayList fetchEnggEvaluation(HashMap hmParam)throws AppError{
		
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		String strIncident      = GmCommonClass.parseNull((String) hmParam.get("INCIDENTID"));
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_rpt.gm_fch_EnggEvaluation",2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1,strIncident);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
		gmDBManager.close();
		return alReturn;
	}
	
	/**
	 * fetchCloseComplaint  This method will fetch Closure details
	 * @return 		ArrayList
	 * @exception 	AppError
	 */
	public ArrayList fetchCloseComplaint(HashMap hmParam)throws AppError{
		
		ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		String strIncident      = GmCommonClass.parseNull((String) hmParam.get("COMRSRID"));
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_rpt.gm_fch_close_com_rsr",2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1,strIncident);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
		gmDBManager.close();
		return alReturn;
	}
	
	/**
	 * fetchClosureEmail  This method will fetch Closure Email details
	 * @param 		HashMap hmParamValues
	 * @return 		HashMap
	 * @exception 	AppError
	 */
	public HashMap fetchClosureEmail(HashMap hmParamValues) throws AppError {
		ArrayList alEmailDetails = new ArrayList();
		ArrayList alUserDetails = new ArrayList();
		HashMap hmResult = new HashMap();
		String strComplaintName = "";
		String strComRsrId = GmCommonClass.parseNull((String)hmParamValues.get("REFID"));
		String strComType = GmCommonClass.parseNull((String)hmParamValues.get("REFTYPE"));
		String strUserID  = GmCommonClass.parseNull((String)hmParamValues.get("USERID"));
		GmDBManager gmDBManager = new GmDBManager ();		
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_process.gm_fch_closure_email", 6);
		gmDBManager.registerOutParameter(4, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(5, OracleTypes.CURSOR);
		gmDBManager.registerOutParameter(6, OracleTypes.VARCHAR);
		gmDBManager.setString(1, strComRsrId);
		gmDBManager.setString(2, strComType);
		gmDBManager.setString(3, strUserID);
		gmDBManager.execute(); 
		alEmailDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(4));
		alUserDetails = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(5));
		strComplaintName = GmCommonClass.parseNull((String)gmDBManager.getString(6));
		hmResult.put("EMAILDETAILS", alEmailDetails);
		hmResult.put("USERDETAILS", alUserDetails);
		hmResult.put("COMPLAINTNM", strComplaintName);
		gmDBManager.close();
		return hmResult;
	} //End of fetchClosureEmail
	
	/**
	 * sendClosureEmail  This method will send Closure Email details
	 * @param 		HashMap hmParamValues
	 * @return 		
	 * @exception 	AppError
	 */
	public void sendClosureEmail(HashMap hmParamValues) throws AppError {
		GmEmailProperties emailProps = new GmEmailProperties();
		GmJasperMail jasperMail = new GmJasperMail();
		ArrayList alReport = new ArrayList();
		HashMap hmAdditionalParams = new HashMap();
		HashMap hmReturnDetails = new HashMap();	
		
		String strMimeType = "text/html";
		String strFromEmail =  GmCommonClass.getRuleValue("CMP_CLOSURE_EMAIL", "EMAIL"); ;
		String strToEmail = GmCommonClass.parseNull((String)hmParamValues.get("TOEMAIL"));
		String strCcEmail = GmCommonClass.parseNull((String)hmParamValues.get("CCEMAIL"));
		String strEmailSubject = GmCommonClass.parseNull((String)hmParamValues.get("EMAILSUBJECT"));
		String strEmailContent = GmCommonClass.parseNull((String)hmParamValues.get("EMAILCOMMNETS"));
		String strRefType = GmCommonClass.parseNull((String)hmParamValues.get("REFTYPE"));
		String strComplaintNm = GmCommonClass.parseNull((String)hmParamValues.get("COMPLAINTNM"));
		String strUserName = GmCommonClass.parseNull((String)hmParamValues.get("USERNM"));
		String strUserDesig = GmCommonClass.parseNull((String)hmParamValues.get("USERDESIG"));
		String strImagePath = GmCommonClass.parseNull((String)hmParamValues.get("IMAGE_PATH"));
		
		String strJasperName = "/GmCMPClosureEmail.jasper";		
		
		hmAdditionalParams.put("EMAILCONTENT", strEmailContent);
		hmAdditionalParams.put("REFTYPE", strRefType);	
		hmAdditionalParams.put("COMPLAINTNM", strComplaintNm);
		hmAdditionalParams.put("USERNM", strUserName);
		hmAdditionalParams.put("USERDESIG", strUserDesig);
		hmAdditionalParams.put("IMAGE_PATH", strImagePath);
		 
		emailProps.setMimeType(strMimeType);
		emailProps.setSender(strFromEmail); 
		emailProps.setRecipients(strToEmail);
		emailProps.setCc(strCcEmail);
		emailProps.setSubject(strEmailSubject);
		
		jasperMail.setJasperReportName(strJasperName);
		jasperMail.setAdditionalParams(hmAdditionalParams);
		jasperMail.setReportData(null);
		jasperMail.setEmailProperties(emailProps);
		hmReturnDetails = jasperMail.sendMail();  
		
	} //End of sendClosureEmail		
	
	/**
	 * sendEvaluatorEmail  This method will send Evaluator Email details
	 * @param 		HashMap hmParamValues
	 * @return 		
	 * @exception 	AppError
	 */
	public void sendEvaluatorEmail(HashMap hmParamValues) throws AppError {
        GmEmailProperties emailProps = new GmEmailProperties();
        GmJasperMail jasperMail = new GmJasperMail();
        GmCommonBean gmCommonBean = new GmCommonBean();
        ArrayList alReport = new ArrayList();
        HashMap hmAdditionalParams = new HashMap();
        HashMap hmReturnDetails = new HashMap();  
        
        emailProps.setSender(GmCommonClass.getEmailProperty("GmEvaluvatorEmail.From"));
        emailProps.setCc(GmCommonClass.getEmailProperty("GmEvaluvatorEmail.Cc"));
        emailProps.setSubject(GmCommonClass.getEmailProperty("GmEvaluvatorEmail.Subject"));
        emailProps.setMimeType(GmCommonClass.getEmailProperty("GmEvaluvatorEmail.MimeType"));
        String strToEmail = GmCommonClass.parseNull((String)hmParamValues.get("TOEMAIL"));
        String strShGemNm = GmCommonClass.parseNull((String)hmParamValues.get("GEMNAME"));
        String strDueDate = GmCommonClass.parseNull((String)hmParamValues.get("DUEDATE"));
        String strRefType = GmCommonClass.parseNull((String)hmParamValues.get("COMRSRID"));
        String strUserNm = GmCommonClass.parseNull((String)hmParamValues.get("USERNM"));
        String strLogType = GmCommonClass.parseNull((String)hmParamValues.get("LOGTYPE"));
        String strUserId = GmCommonClass.parseNull((String)hmParamValues.get("USERID"));
        strShGemNm = strShGemNm + ",";
        String strJasperName = "/GmEvaluvatorEmail.jasper";                           
        
        hmAdditionalParams.put("COMRSRID", strRefType);
        hmAdditionalParams.put("DUEDATE", strDueDate);
        hmAdditionalParams.put("GEMNAME", strShGemNm);
        hmAdditionalParams.put("USERNM", strUserNm);
        emailProps.setRecipients(strToEmail);
        emailProps.setReplyTo(GmCommonClass.getEmailProperty("GmEvaluvatorEmail.ReplyTo"));
        jasperMail.setJasperReportName(strJasperName);
        jasperMail.setAdditionalParams(hmAdditionalParams);
        jasperMail.setReportData(null);
        jasperMail.setEmailProperties(emailProps);
        hmReturnDetails = jasperMail.sendMail();  
        
        gmCommonBean.saveLog(strRefType, "Notification Sent to Evaluator as Evaluation Request", strUserId, strLogType);
  }
	
	
	/**
	 * fetchCOMRSRDetails  This method will is to fetch the COM/RSR details
	 * @param 		HashMap hmParam
	 * @return 		alResult
	 * @exception 	AppError
	 */
	public ArrayList fetchCOMRSRDetails(HashMap hmParam) throws AppError{ 
			
			ArrayList alResult = new ArrayList();
			String strCOMRSR = GmCommonClass.parseNull((String) hmParam.get("COMORRSR"));
			/*String strSalesRep = GmCommonClass.parseNull((String) hmParam.get("SALESREP"));*/
			String strCompId = GmCommonClass.parseNull((String) hmParam.get("COMRSR"));
			/*String strEmpNm = GmCommonClass.parseNull((String) hmParam.get("EMPNAME"));*/
			String strRAId = GmCommonClass.parseNull((String) hmParam.get("RAID"));
			String strPartNum = GmCommonClass.parseNull((String) hmParam.get("PART"));
			String strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
			String strLotId = GmCommonClass.parseNull((String) hmParam.get("LOT"));
			String strElapsedVal = GmCommonClass.parseNull((String) hmParam.get("ELAPSEDVAL"));
			String strCompSign = GmCommonClass.parseNull((String) hmParam.get("ELAPSEDDAYS"));
			String strFromDt = GmCommonClass.parseNull((String) hmParam.get("FRMDATE"));
			String strToDt = GmCommonClass.parseNull((String) hmParam.get("TODATE"));
			String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
			String strComplaint = GmCommonClass.parseNull((String) hmParam.get("COMPLAINT"));
			String strComplaintName = GmCommonClass.parseNull((String) hmParam.get("COMPLAINTNAME"));
			strCOMRSR 	= (!strCOMRSR.equals("0")) ? strCOMRSR : "";
			strComplaint = (!strComplaint.equals("0")) ? strComplaint : "";
			strComplaintName 	= (!strComplaintName.equals("0")) ? strComplaintName : "";
			strStatus 	= (!strStatus.equals("0")) ? strStatus : "";
			strCompSign = (!strCompSign.equals("0")) ? strCompSign : "";
			
			HashMap hmTempComp = new HashMap(); // To set the comparison signs to corresponding code id
	        hmTempComp.put("90190",">");
	        hmTempComp.put("90191","<");
	        hmTempComp.put("90192","=");
	        hmTempComp.put("90193",">=");
	        hmTempComp.put("90194","<=");
					
				GmDBManager gmDBManager = new GmDBManager();
	
				StringBuffer sbQuery = new StringBuffer();
				sbQuery.append(" SELECT T3100.C3100_INCIDENT_ID COMRSRID, T3101.C506_RMA_ID RAID, T3101.C205_PART_NUMBER_ID PARTNUM ");
				sbQuery.append(" , SUBSTR(T3101.C3101_LOT_NUMBER,0,20) LOTNUM, T3101.C205_PART_DESC PARTDESC");
				sbQuery.append(" , DECODE(T3100.C901_COMPLAINANT_TYPE,'102812',GET_REP_NAME(T3100.C3100_COMPLAINANT_ID),GET_USER_NAME(T3100.C3100_COMPLAINANT_ID)) COMPNM");
				sbQuery.append(" , T3101.C3101_QTY QUANTITY, GET_CODE_NAME(T3100.C901_STATUS) STATUS, TO_CHAR(T3100.C3100_RECEIVED_DT,get_rule_value ('DATEFMT', 'DATEFORMAT')) RECEIVEDDT");
				sbQuery.append(" , DECODE (T3100.C901_STATUS,'102841',TRUNC(T3105.C3105_CLOSED_DT)- TRUNC(T3100.C3100_RECEIVED_DT),TRUNC(SYSDATE)- TRUNC(T3100.C3100_RECEIVED_DT)) DAYTOCLOSE");
				sbQuery.append(" , DECODE(T3100.C901_ISSUE_TYPE, 102809, GET_LOG_FLAG(T3100.C3100_INCIDENT_ID,4000319), 102810, GET_LOG_FLAG(T3100.C3100_INCIDENT_ID,4000320)) COMP_LOG_FL");
				sbQuery.append(" , T3100.C901_ISSUE_TYPE ISSUETYP, T3100.C901_STATUS STATUSID");
				sbQuery.append(" FROM T3100_INCIDENT T3100, T3101_INCIDENT_PART_DETAIL T3101, T3105_INCIDENT_CLOSURE_DTL T3105");
				sbQuery.append(" WHERE T3100.C3100_INCIDENT_ID = T3101.C3100_INCIDENT_ID ");
				sbQuery.append(" AND T3100.C3100_INCIDENT_ID = T3105.C3100_INCIDENT_ID(+) ");
		
				if(!strCOMRSR.equals("")) { // Filter based on COM/RSR Type
					sbQuery.append(" AND T3100.C901_ISSUE_TYPE = ");
					sbQuery.append(strCOMRSR);
				}
				
				if(!strComplaint.equals("")) { // Filter based on Complaint
				sbQuery.append(" AND T3100.C901_COMPLAINANT_TYPE = '");
				sbQuery.append(strComplaint);
				sbQuery.append("'");
				}
				if(!strComplaint.equals("") && !strComplaintName.equals("")) { // Filter based on ComplaintName 
					sbQuery.append(" AND T3100.C3100_COMPLAINANT_ID = '");
					sbQuery.append(strComplaintName);
					sbQuery.append("'");
					}
				if(!strCompId.equals("")) { // Filter based on COM/RSR ID
					sbQuery.append(" AND T3100.C3100_INCIDENT_ID LIKE UPPER('%");
					sbQuery.append(strCompId);
					sbQuery.append("%')");
				}
				
				if(!strRAId.equals("")) { // Filter based on RA Id
					sbQuery.append(" AND UPPER(T3101.C506_RMA_ID) LIKE UPPER('%");
					sbQuery.append(strRAId);
					sbQuery.append("%')");
				}
				if(!strPartNum.equals("")) { // Filter based on Part Num
					sbQuery.append(" AND UPPER(T3101.C205_PART_NUMBER_ID) LIKE UPPER('%");
					sbQuery.append(strPartNum);
					sbQuery.append("%')");
				}
				if(strStatus.equals("") && strOpt.equals("load")) { // Filter based on Status
					sbQuery.append(" AND T3100.C901_STATUS NOT IN ('102841')");
				}else if(!strStatus.equals("")){
					sbQuery.append(" AND T3100.C901_STATUS = '");
					sbQuery.append(strStatus);
					sbQuery.append("'");
				}
				
				if(!strLotId.equals("")) { // Filter based on Project Id
					sbQuery.append(" AND UPPER(T3101.C3101_LOT_NUMBER) LIKE UPPER('%");
					sbQuery.append(strLotId);
					sbQuery.append("%')");
				}
				if(!strFromDt.equals("") && !strToDt.equals("")) { // Filter based on Date
					sbQuery.append(" AND TRUNC(T3100.C3100_RECEIVED_DT)  BETWEEN TO_DATE('");
					sbQuery.append(strFromDt);
					sbQuery.append("',get_rule_value ('DATEFMT', 'DATEFORMAT'))");
					sbQuery.append(" AND TO_DATE('");
					sbQuery.append(strToDt);
					sbQuery.append("',get_rule_value ('DATEFMT', 'DATEFORMAT'))");
				}
				if(!strElapsedVal.equals("")) { // Filter based on Elapsed days
					sbQuery.append(" AND DECODE(T3100.C901_STATUS,'102841',TRUNC(T3105.C3105_CLOSED_DT)- TRUNC(T3100.C3100_RECEIVED_DT)");
					sbQuery.append(" ,TRUNC(SYSDATE)- TRUNC(T3100.C3100_RECEIVED_DT))");
					sbQuery.append(hmTempComp.get(strCompSign));
					sbQuery.append(strElapsedVal);
				}
				
				sbQuery.append(" AND T3100.C3100_VOID_FL IS NULL ");
				sbQuery.append(" AND T3101.C3101_VOID_FL IS NULL ");
				sbQuery.append(" AND T3105.C3105_VOID_FL IS NULL ");
				sbQuery.append(" ORDER BY DAYTOCLOSE DESC");
				alResult = gmDBManager.queryMultipleRecords(sbQuery.toString());
				log.debug(" Query to get COM/RSR Report is " + sbQuery.toString() + "size of AList " + alResult.size());
				
			return alResult;
		}

	/**
	 * fetchCloseComplaint  This method will fetch Closure Email details
	 * @param 		String strComRsrId
	 * @return 		ArrayList
	 * @exception 	AppError
	 */
	public ArrayList fetchCloseCOMRSR(HashMap hmParam) throws AppError {
		log.debug("Enter into fetchCloseCOMRSR in   ");
		ArrayList alReport = new ArrayList();
		String strcomrsrid       = GmCommonClass.parseNull((String) hmParam.get("CLOSECOMRSRID"));
		GmDBManager gmDBManager = new GmDBManager ();
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_rpt.gm_fetch_close_com_rsr", 2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strcomrsrid);
		gmDBManager.execute(); 
		
		alReport = gmDBManager.returnArrayList((ResultSet) gmDBManager.getObject(2));
			
		gmDBManager.close();
		return alReport;
	} //End of fetchCloseComplaint
	
	/**
	 * fetchPrintComplaint  This method will fetch Print Details about the Compliant[CE,EE,QAE]
	 * @param 		HashMap hmParamValues
	 * @return 		HashMap
	 * @exception 	AppError
	 */
	public HashMap fetchPrintComplaint(HashMap hmParamValues) throws AppError {
		GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean();
		HashMap hmParam = new HashMap();
		HashMap hmQAValues = new HashMap();
		ArrayList alIncidentList = new ArrayList();
		ArrayList alEnggEvalList = new ArrayList();		
		HashMap hmResult = new HashMap();
		String strComRsrId  = GmCommonClass.parseNull((String)hmParamValues.get("COMRSRID"));
		String strIssueType = "102809";  // 102809	Complaint Type
		hmParam.put("COMRSRID", strComRsrId);	
		hmParam.put("ISSUETYPE", strIssueType);
		hmParam.put("STRCOMRSRID", strComRsrId);
		hmParam.put("INCIDENTID", strComRsrId);
		alIncidentList 	= gmCOMRSRRptBean.fetchIncidentDetails(hmParam);
		hmQAValues   	= gmCOMRSRRptBean.fetchQAEvaluation(strComRsrId);
		alEnggEvalList 	= gmCOMRSRRptBean.fetchEnggEvaluation(hmParam);
		hmResult.put("INCIDENTDETA", alIncidentList);
		hmResult.put("QADATA", hmQAValues);
		hmResult.put("ENGGDATA", alEnggEvalList);
		return hmResult;
	} //End of fetchPrintComplaint
	
	/**
	 * fetchADList  This method will fetch the Product Development Details
	 * @param 		No Param
	 * @return 		ArrayList
	 * @exception 	AppError
	 */
	public ArrayList fetchUserList(String strDeptId) throws AppError {
		ArrayList alReport = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager ();	
		
		StringBuffer sbQuery = new StringBuffer();
		sbQuery.append(" SELECT   t101.c101_party_id partyid,t101.c101_user_f_name|| ' '|| t101.c101_user_l_name username");
		sbQuery.append(" FROM t101_user t101 ");
		sbQuery.append(" WHERE ");
		if(!strDeptId.equals(""))
			sbQuery.append(	"t101.c901_dept_id IN("+strDeptId+") AND ");
		sbQuery.append("  t101.c101_party_id IS NOT NULL AND t101.c101_party_id != 0");
		sbQuery.append(" ORDER BY username, t101.c901_dept_id");
		
		alReport = gmDBManager.queryMultipleRecords(sbQuery.toString());		

		gmDBManager.close();
		return alReport;
	} 
	
	
}
