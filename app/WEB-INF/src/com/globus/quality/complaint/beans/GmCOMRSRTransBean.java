package com.globus.quality.complaint.beans;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmBean;

public class GmCOMRSRTransBean extends GmBean {	
	GmCommonClass gmCommon = new GmCommonClass();
	GmCommonBean gmCommonBean = new GmCommonBean();
	GmLogger log = GmLogger.getInstance();
	
	  public GmCOMRSRTransBean() {
	    super(GmCommonClass.getDefaultGmDataStoreVO());
	  }

	  public GmCOMRSRTransBean(GmDataStoreVO gmDataStore) {
	    super(gmDataStore);
	  }
		  
	/**
	 * saveIncidentDetails  This method will save the Complaint Information
	 * @param 		HashMap hmParam
	 * @return 		String
	 * @exception 	AppError
	 */
	public String saveIncidentDetails(HashMap hmParam) throws AppError , Exception {
		ArrayList alReport = new ArrayList();
		
		String strIncidentID = "";
		String strLog          = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		String strIssueType = GmCommonClass.parseNull((String) hmParam.get("ISSUETYPE"));
		String strUserId       = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		String strInput = GmCommonClass.parseNull((String) hmParam.get("STRJSONSTRING"));	
		log.debug("strInput-----------------------"+strInput);
		
		
		
        GmDBManager gmDBManager = new GmDBManager ();		
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_txn.gm_sav_incident_info", 3);
		gmDBManager.registerOutParameter(3, OracleTypes.CHAR);
		gmDBManager.setString(1, strInput); // JSON input String
		gmDBManager.setString(2, strUserId);
	
		gmDBManager.execute(); 
		strIncidentID = GmCommonClass.parseNull((String)gmDBManager.getString(3));		
		if (!strLog.equals("")) {
			if(strIssueType.equals("102809")){
				gmCommonBean.saveLog(gmDBManager, strIncidentID, strLog, strUserId, "4000319");
			}else if(strIssueType.equals("102810")){
				gmCommonBean.saveLog(gmDBManager, strIncidentID, strLog, strUserId, "4000320");
			}			
		}
		gmDBManager.commit();
		return strIncidentID;
	} //End of saveIncidentDetails
	
	
	public HashMap saveQAEvaluation(HashMap hmParam) throws AppError, Exception {
		HashMap hmReturn = new HashMap();
		GmCommonBean gmCommonBean   = new GmCommonBean();
		GmDBManager gmDBManager 	= new GmDBManager();
		String strIncidentId  		= GmCommonClass.parseNull((String) hmParam.get("COMRSRID"));
		String strDHRNCMRRev 		= GmCommonClass.parseNull((String) hmParam.get("DHRNCMRREV"));
		String strCAPASCARRev		= GmCommonClass.parseNull((String) hmParam.get("CAPASCARREV"));
		String strCompRev 			= GmCommonClass.parseNull((String) hmParam.get("COMPREV"));
		String strRevBy 			= GmCommonClass.parseNull((String) hmParam.get("REVCOMPDBY"));
		String strRevDt 			= GmCommonClass.parseNull((String) hmParam.get("DTREVCOMP"));
		String strPartRecDt 		= GmCommonClass.parseNull((String) hmParam.get("DTPARTREC"));
		String strEvalSend 			= GmCommonClass.parseNull((String) hmParam.get("DTEVALSEND"));
		String strDHRDevFL 			= GmCommonClass.parseNull((String) hmParam.get("DHRDEV"));
		String strQuestionnaireFl 	= GmCommonClass.parseNull((String) hmParam.get("REPSURGANS"));
		String strStorageFl 		= GmCommonClass.parseNull((String) hmParam.get("STORAGE"));
		String strPackageFl 		= GmCommonClass.parseNull((String) hmParam.get("UTILIZEDPAC"));
		String strMarketRemFl 		= GmCommonClass.parseNull((String) hmParam.get("REMOVALREQ"));
		String strRiskFl 			= GmCommonClass.parseNull((String) hmParam.get("RISKANALYSIS"));
		String strNCMRSCARFl 		= GmCommonClass.parseNull((String) hmParam.get("NCMRSCARGEN"));
		String strNCMRSCARNum 		= GmCommonClass.parseNull((String) hmParam.get("NCMRSCARID"));
		String strCAPAReqFl 		= GmCommonClass.parseNull((String) hmParam.get("CORRECTIVEACT"));
		String strCAPANum 			= GmCommonClass.parseNull((String) hmParam.get("CAPAID"));
		String strQARevBy 			= GmCommonClass.parseNull((String) hmParam.get("QAREVCOMPBY"));
		String strQARevDt 			= GmCommonClass.parseNull((String) hmParam.get("DTCOMPLETED"));
		String strCreatedBy         = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strLog         		= GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		String strDateFormat   		= GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
		String strRefType   		= GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		String strLogType = "";
		String strOutFl = "";
		String strDueDt = "";
		
		if(strRefType.equals("102791")){
			strLogType = "4000320";
		}else if(strRefType.equals("102790")){
			strLogType = "4000319";
		}
	
        Date dtRevDt  		= GmCommonClass.getStringToDate(strRevDt, strDateFormat);
        Date dtPartRecDt    = GmCommonClass.getStringToDate(strPartRecDt, strDateFormat);   
        Date dtEvalSend 	= GmCommonClass.getStringToDate(strEvalSend,  strDateFormat);
        Date dtQARevDt 	    = GmCommonClass.getStringToDate(strQARevDt,  strDateFormat);
        
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_txn.gm_sav_QAEvaluation",23);
		gmDBManager.setString(1, strIncidentId);
		gmDBManager.setString(2, strDHRNCMRRev);
		gmDBManager.setString(3, strCAPASCARRev);
		gmDBManager.setString(4, strCompRev);
		gmDBManager.setInt(5, Integer.parseInt(strRevBy));
		gmDBManager.setDate(6, dtRevDt);
		gmDBManager.setDate(7, dtPartRecDt);
		gmDBManager.setDate(8, dtEvalSend);
		gmDBManager.setString(9, strDHRDevFL);
		gmDBManager.setString(10, strQuestionnaireFl);
		gmDBManager.setString(11, strStorageFl);
		gmDBManager.setString(12, strPackageFl);
		gmDBManager.setString(13, strMarketRemFl);
		gmDBManager.setString(14, strRiskFl);
		gmDBManager.setString(15, strNCMRSCARFl);
		gmDBManager.setString(16, strNCMRSCARNum);
		gmDBManager.setString(17, strCAPAReqFl);
		gmDBManager.setString(18, strCAPANum);
		gmDBManager.setInt(19, Integer.parseInt(strQARevBy));
		gmDBManager.setDate(20, dtQARevDt);
		gmDBManager.setString(21, strCreatedBy);
		gmDBManager.registerOutParameter(22, OracleTypes.VARCHAR);
		gmDBManager.registerOutParameter(23, OracleTypes.VARCHAR);
		gmDBManager.execute();
		strOutFl = GmCommonClass.parseNull((String)gmDBManager.getString(22)); // To get whether date is changed or not
		strDueDt = GmCommonClass.parseNull((String)gmDBManager.getString(23));
		
		if(!strLog.equals("")){
			gmCommonBean.saveLog(gmDBManager, strIncidentId, strLog, strCreatedBy, strLogType);
		}
		
		hmReturn.put("INCIDENTID", strIncidentId);
		hmReturn.put("DATEFLAG", strOutFl);
		hmReturn.put("DUEDATE", strDueDt);
		gmDBManager.commit();
		return hmReturn;
	}
	
	/**
	 * saveClosureEmail  This method will save the Closure Email Details
	 * @param 		HashMap hmParam
	 * @return 		
	 * @exception 	AppError , Exception
	 */
	public void saveClosureEmail(HashMap hmParam)throws AppError,Exception{			
		GmCommonBean gmCommonBean = new GmCommonBean();
		String strRefID	   		= GmCommonClass.parseNull((String) hmParam.get("REFID"));
		String strRefType 		= GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		String strToEmail       = GmCommonClass.parseNull((String) hmParam.get("TOEMAIL"));
		String strCcEmail       = GmCommonClass.parseNull((String) hmParam.get("CCEMAIL"));
		String strEmailSubject  = GmCommonClass.parseNull((String) hmParam.get("EMAILSUBJECT"));
		String strEmailContent  = GmCommonClass.parseNull((String) hmParam.get("EMAILCOMMNETS"));
		String strUserID        = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		
		GmDBManager gmDBManager = new GmDBManager();
        gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_process.gm_sav_closure_email",7);
        gmDBManager.setString(1,strRefID);			
		gmDBManager.setString(2,strRefType);
		gmDBManager.setString(3,strToEmail);
		gmDBManager.setString(4,strCcEmail);
		gmDBManager.setString(5,strEmailSubject);	
		gmDBManager.setString(6,strEmailContent);
		gmDBManager.setString(7,strUserID);
		gmDBManager.execute();			
		gmDBManager.commit();
	} // End of saveClosureEmail
	/**
	 * Save Engg Evaluation  This method will fetch the Area Direct Details
	 * @param 		No Param
	 * @return 		ArrayList
	 * @exception 	AppError
	 */
	public String saveEnggEvaluation(HashMap hmParam)throws AppError,Exception{
		
		log.debug("Enter into saveEnggEvaluation in GmCOMRSRRptBean");
		GmCommonBean gmCommonBean = new GmCommonBean();
		String strComIDFromDB = "";
		
		String strcomrsrid      = GmCommonClass.parseNull((String) hmParam.get("INCIDENTID"));
		String strDate          = GmCommonClass.parseNull((String) hmParam.get("EVALRECDT"));
		String strEval          = GmCommonClass.parseNull((String) hmParam.get("EVALRESULT"));	
		String strEvaluator     = GmCommonClass.parseNull((String) hmParam.get("EVALID"));
		String strComp          = GmCommonClass.parseNull((String) hmParam.get("COMRSRDISPNUM"));
		String strProd          = GmCommonClass.parseNull((String) hmParam.get("PDTDISPTYPNUM"));
		String strComments      = GmCommonClass.parseNull((String) hmParam.get("CAPACOMMENTS"));
		String strUserId        = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strLog           = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		String strRefType 		= GmCommonClass.parseNull((String) hmParam.get("REFTYPE"));
		String strLogType 		= "";
		String strDateFormat       = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
		Date dtDateReceived       = GmCommonClass.getStringToDate(strDate, strDateFormat);
		GmDBManager gmDBManager = new GmDBManager();
		gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_txn.gm_sav_EnggEvaluation",9);
		gmDBManager.setString(1,strcomrsrid );
		gmDBManager.setDate  (2,dtDateReceived);
	    gmDBManager.setString(3, strEval);
		gmDBManager.setString(4, strEvaluator);
		gmDBManager.setString(5, strComp);
		gmDBManager.setString(6, strProd);
		gmDBManager.setString(7, strComments);
		gmDBManager.setString(8, strUserId);
		gmDBManager.registerOutParameter(9, OracleTypes.CHAR);
		gmDBManager.execute();
		strComIDFromDB = GmCommonClass.parseNull(gmDBManager.getString(9));
		if(strRefType.equals("102791")){//102791-> code id for RSR
			strLogType = "4000320"; //4000320 -> code id for RSRLOG.
		}else if(strRefType.equals("102790")){//102790 -> code id for COM
				strLogType = "4000319"; //4000319 -> code id for COMLOG
		}
		if (!strLog.equals("")) {
				gmCommonBean.saveLog(gmDBManager, strComIDFromDB, strLog, strUserId, strLogType);//4000318-->Code id for Scar Log
			}
		 gmDBManager.commit();
		 
		return strComIDFromDB;
	}

	/**
	 * saveCloseCOMRSR  This method will save the Complaint Information
	 * @param 		HashMap hmParam
	 * @return 		String
	 * @exception 	AppError
	 */
	public String saveCloseCOMRSR(HashMap hmParam)throws AppError,Exception{
		log.debug("entering into saveCloseComRsr");
		GmCommonBean gmCommonBean = new GmCommonBean();
		String strMessage     = "";
		String strComIDFromDB = "";
		String strCOMRSRID    = GmCommonClass.parseNull((String) hmParam.get("CLOSECOMRSRID"));
		String strClosedBy    = GmCommonClass.parseNull((String) hmParam.get("CLOSEDBY"));
		String strClosedDt    = GmCommonClass.parseNull((String) hmParam.get("CLOSEDDT"));
		String strComments    = GmCommonClass.parseNull((String) hmParam.get("PREVCOMMENTS"));
		String strCloseCmts   = GmCommonClass.parseNull((String) hmParam.get("CLOSECMTS"));
		String strUserId      = GmCommonClass.parseNull((String) hmParam.get("USERID"));
		String strDateFormat  = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));		
		String strLogType 		= GmCommonClass.parseNull((String) hmParam.get("LOGTYPE"));	
		String strLog           = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
		Date dtClosedDt  		= GmCommonClass.getStringToDate(strClosedDt, strDateFormat);
        		
		GmDBManager gmDBManager = new GmDBManager();
        gmDBManager.setPrepareString("gm_pkg_qa_com_rsr_txn.gm_sav_close_COMRSR",7);
        gmDBManager.setString(1,strCOMRSRID);			
		gmDBManager.setString(2,strClosedBy);
		gmDBManager.setDate(3,dtClosedDt);
		gmDBManager.setString(4,strComments);
		gmDBManager.setString(5,strCloseCmts);
		gmDBManager.setString(6,strUserId);
		gmDBManager.registerOutParameter(7, OracleTypes.CHAR);
		gmDBManager.execute();	
		strComIDFromDB = GmCommonClass.parseNull(gmDBManager.getString(7));
		if(!strComIDFromDB.equals("")){
			strMessage = "Complaint Closed Successfully";
		}
		if (!strLog.equals("")) {
				
			 gmCommonBean.saveLog(gmDBManager, strComIDFromDB, strLog, strUserId,strLogType );//4000318-->Code id for Scar Log
			}
		gmDBManager.commit();
		return strMessage;
		
	} // End of saveCloseCOMRSR

}