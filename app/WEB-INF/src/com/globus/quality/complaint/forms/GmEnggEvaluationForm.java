package com.globus.quality.complaint.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;
import com.globus.common.forms.GmUploadForm;

public class GmEnggEvaluationForm extends GmLogForm {
	
	
	private String incidentId   = "";
	private String evalRecDt     = "";
	private String evalResult = "";
	private String evalId     = "";
	private String comRsrDispNum  = "";
	private String pdtDispTypNum   = "";
	
	private String hCOMRSRID     = "";
	private String status     = "";
	private String capaComments   = "";
	private String chooseAction  = "";
	private String message       = "";
	private String strRefType    = "";
	
	
	private int     intSize       = 0;
	private ArrayList alAssignedTo = new ArrayList();
	private ArrayList alComRsrDis = new ArrayList();
	private ArrayList alProdDisp = new ArrayList();
	

	private ArrayList alAuditName = new ArrayList();
	
	private ArrayList alChooseAction = new ArrayList();
	
	public ArrayList getAlAuditName() {
		return alAuditName;
	}

	public void setAlAuditName(ArrayList alAuditName) {
		this.alAuditName = alAuditName;
	}

	
	public ArrayList getAlAssignedTo() {
		return alAssignedTo;
	}

	public void setAlAssignedTo(ArrayList alAssignedTo) {
		this.alAssignedTo = alAssignedTo;
	}

	public ArrayList getAlComRsrDis() {
		return alComRsrDis;
	}

	public void setAlComRsrDis(ArrayList alComRsrDis) {
		this.alComRsrDis = alComRsrDis;
	}

	public ArrayList getAlProdDisp() {
		return alProdDisp;
	}

	public void setAlProdDisp(ArrayList alProdDisp) {
		this.alProdDisp = alProdDisp;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.incidentId= "";
    	this.evalRecDt= "";                                                       
    	this.evalResult = "";
    	this.evalId  = "";
    	this.comRsrDispNum = "";
    	this.pdtDispTypNum = "";
    	this.capaComments  = "";
    	
    	
	}

	public String gethCOMRSRID() {
		return hCOMRSRID;
	}

	public void sethCOMRSRID(String hCOMRSRID) {
		this.hCOMRSRID = hCOMRSRID;
	}

	public ArrayList getAlChooseAction() {
		return alChooseAction;
	}

	public void setAlChooseAction(ArrayList alChooseAction) {
		this.alChooseAction = alChooseAction;
	}

	public String getChooseAction() {
		return chooseAction;
	}

	public void setChooseAction(String chooseAction) {
		this.chooseAction = chooseAction;
	}

	public int getIntSize() {
		return intSize;
	}

	public void setIntSize(int intSize) {
		this.intSize = intSize;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStrRefType() {
		return strRefType;
	}

	public void setStrRefType(String strRefType) {
		this.strRefType = strRefType;
	}

	public String getEvalRecDt() {
		return evalRecDt;
	}

	public void setEvalRecDt(String evalRecDt) {
		this.evalRecDt = evalRecDt;
	}

	public String getEvalResult() {
		return evalResult;
	}

	public void setEvalResult(String evalResult) {
		this.evalResult = evalResult;
	}

	public String getEvalId() {
		return evalId;
	}

	public void setEvalId(String evalId) {
		this.evalId = evalId;
	}

	public String getComRsrDispNum() {
		return comRsrDispNum;
	}

	public void setComRsrDispNum(String comRsrDispNum) {
		this.comRsrDispNum = comRsrDispNum;
	}

	public String getPdtDispTypNum() {
		return pdtDispTypNum;
	}

	public void setPdtDispTypNum(String pdtDispTypNum) {
		this.pdtDispTypNum = pdtDispTypNum;
	}

	public String getCapaComments() {
		return capaComments;
	}

	public void setCapaComments(String capaComments) {
		this.capaComments = capaComments;
	}

	public String getIncidentId() {
		return incidentId;
	}

	public void setIncidentId(String incidentId) {
		this.incidentId = incidentId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	
	
}



