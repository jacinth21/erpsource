/**
 * 
 */
package com.globus.quality.complaint.forms;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionMapping;

import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmDateConverter;
import com.globus.common.beans.GmLogger;
import com.globus.common.forms.GmLogForm;

/**
 * @author arajan
 *
 */
public class GmQAEvaluationForm extends GmLogForm {

	private String COMRSRId = "";
	private String DHRNCMRRev = "";
	private String CAPASCARRev = "";
	private String compRev = "";
	private String revCompdBy = "";
	private String dtRevComp = "";
	private String dtPartRec = "";
	private String dtEvalSend = "";
	private String DHRDev = "";
	private String repSurgAns = "";
	private String storage = "";
	private String utilizedPac = "";
	private String removalReq = "";
	private String riskAnalysis = "";
	private String NCMRSCARGen = "";
	private String correctiveAct = "";
	private String NCMRSCARId = "";
	private String CAPAId = "";
	private String QARevCompBy = "";
	private String dtCompleted = "";
	private String chooseAction = "";
	private String hCOMRSRId = "";
	private String strMessage = "";
	private String strRefType = "";
	private String status = "";
	private int resultSize;
	
	private ArrayList alRevCompBy = new ArrayList();
	private ArrayList alQARevCompBy = new ArrayList();
	private ArrayList alChooseAction = new ArrayList();
	
	/**
	 * @return the dHRNCMRRev
	 */
	public String getDHRNCMRRev() {
		return DHRNCMRRev;
	}
	/**
	 * @param dHRNCMRRev the dHRNCMRRev to set
	 */
	public void setDHRNCMRRev(String dHRNCMRRev) {
		DHRNCMRRev = dHRNCMRRev;
	}
	/**
	 * @return the cAPASCARRev
	 */
	public String getCAPASCARRev() {
		return CAPASCARRev;
	}
	/**
	 * @param cAPASCARRev the cAPASCARRev to set
	 */
	public void setCAPASCARRev(String cAPASCARRev) {
		CAPASCARRev = cAPASCARRev;
	}
	/**
	 * @return the compRev
	 */
	public String getCompRev() {
		return compRev;
	}
	/**
	 * @param compRev the compRev to set
	 */
	public void setCompRev(String compRev) {
		this.compRev = compRev;
	}
	/**
	 * @return the revCompdBy
	 */
	public String getRevCompdBy() {
		return revCompdBy;
	}
	/**
	 * @param revCompdBy the revCompdBy to set
	 */
	public void setRevCompdBy(String revCompdBy) {
		this.revCompdBy = revCompdBy;
	}
	/**
	 * @return the dtRevComp
	 */
	public String getDtRevComp() {
		return dtRevComp;
	}
	/**
	 * @param dtRevComp the dtRevComp to set
	 */
	public void setDtRevComp(String dtRevComp) {
		this.dtRevComp = dtRevComp;
	}
	/**
	 * @return the dtPartRec
	 */
	public String getDtPartRec() {
		return dtPartRec;
	}
	/**
	 * @param dtPartRec the dtPartRec to set
	 */
	public void setDtPartRec(String dtPartRec) {
		this.dtPartRec = dtPartRec;
	}
	/**
	 * @return the dtEvalSend
	 */
	public String getDtEvalSend() {
		return dtEvalSend;
	}
	/**
	 * @param dtEvalSend the dtEvalSend to set
	 */
	public void setDtEvalSend(String dtEvalSend) {
		this.dtEvalSend = dtEvalSend;
	}
	/**
	 * @return the dHRDev
	 */
	public String getDHRDev() {
		return DHRDev;
	}
	/**
	 * @param dHRDev the dHRDev to set
	 */
	public void setDHRDev(String dHRDev) {
		DHRDev = dHRDev;
	}

	/**
	 * @return the storage
	 */
	public String getStorage() {
		return storage;
	}
	/**
	 * @param storage the storage to set
	 */
	public void setStorage(String storage) {
		this.storage = storage;
	}
	/**
	 * @return the utilizedPac
	 */
	public String getUtilizedPac() {
		return utilizedPac;
	}
	/**
	 * @param utilizedPac the utilizedPac to set
	 */
	public void setUtilizedPac(String utilizedPac) {
		this.utilizedPac = utilizedPac;
	}
	/**
	 * @return the removalReq
	 */
	public String getRemovalReq() {
		return removalReq;
	}
	/**
	 * @param removalReq the removalReq to set
	 */
	public void setRemovalReq(String removalReq) {
		this.removalReq = removalReq;
	}
	/**
	 * @return the riskAnalysis
	 */
	public String getRiskAnalysis() {
		return riskAnalysis;
	}
	/**
	 * @param riskAnalysis the riskAnalysis to set
	 */
	public void setRiskAnalysis(String riskAnalysis) {
		this.riskAnalysis = riskAnalysis;
	}
	/**
	 * @return the nCMRSCARGen
	 */
	public String getNCMRSCARGen() {
		return NCMRSCARGen;
	}
	/**
	 * @param nCMRSCARGen the nCMRSCARGen to set
	 */
	public void setNCMRSCARGen(String nCMRSCARGen) {
		NCMRSCARGen = nCMRSCARGen;
	}
	/**
	 * @return the correctiveAct
	 */
	public String getCorrectiveAct() {
		return correctiveAct;
	}
	/**
	 * @param correctiveAct the correctiveAct to set
	 */
	public void setCorrectiveAct(String correctiveAct) {
		this.correctiveAct = correctiveAct;
	}
	/**
	 * @return the nCMRSCARId
	 */
	public String getNCMRSCARId() {
		return NCMRSCARId;
	}
	/**
	 * @param nCMRSCARId the nCMRSCARId to set
	 */
	public void setNCMRSCARId(String nCMRSCARId) {
		NCMRSCARId = nCMRSCARId;
	}
	/**
	 * @return the cAPAId
	 */
	public String getCAPAId() {
		return CAPAId;
	}
	/**
	 * @param cAPAId the cAPAId to set
	 */
	public void setCAPAId(String cAPAId) {
		CAPAId = cAPAId;
	}
	/**
	 * @return the qARevCompBy
	 */
	public String getQARevCompBy() {
		return QARevCompBy;
	}
	/**
	 * @param qARevCompBy the qARevCompBy to set
	 */
	public void setQARevCompBy(String qARevCompBy) {
		QARevCompBy = qARevCompBy;
	}
	/**
	 * @return the dtCompleted
	 */
	public String getDtCompleted() {
		return dtCompleted;
	}
	/**
	 * @param dtCompleted the dtCompleted to set
	 */
	public void setDtCompleted(String dtCompleted) {
		this.dtCompleted = dtCompleted;
	}
	/**
	 * @return the chooseAction
	 */
	public String getChooseAction() {
		return chooseAction;
	}
	/**
	 * @param chooseAction the chooseAction to set
	 */
	public void setChooseAction(String chooseAction) {
		this.chooseAction = chooseAction;
	}
	/**
	 * @return the alRevCompBy
	 */
	public ArrayList getAlRevCompBy() {
		return alRevCompBy;
	}
	/**
	 * @param alRevCompBy the alRevCompBy to set
	 */
	public void setAlRevCompBy(ArrayList alRevCompBy) {
		this.alRevCompBy = alRevCompBy;
	}
	/**
	 * @return the alQARevCompBy
	 */
	public ArrayList getAlQARevCompBy() {
		return alQARevCompBy;
	}
	/**
	 * @param alQARevCompBy the alQARevCompBy to set
	 */
	public void setAlQARevCompBy(ArrayList alQARevCompBy) {
		this.alQARevCompBy = alQARevCompBy;
	}
	/**
	 * @return the alChooseAction
	 */
	public ArrayList getAlChooseAction() {
		return alChooseAction;
	}
	/**
	 * @param alChooseAction the alChooseAction to set
	 */
	public void setAlChooseAction(ArrayList alChooseAction) {
		this.alChooseAction = alChooseAction;
	}
	/**
	 * @param cOMRSRId the cOMRSRId to set
	 */
	public void setCOMRSRId(String cOMRSRId) {
		COMRSRId = cOMRSRId;
	}
	/**
	 * @return the cOMRSRId
	 */
	public String getCOMRSRId() {
		return COMRSRId;
	}
	/**
	 * @param repSurgAns the repSurgAns to set
	 */
	public void setRepSurgAns(String repSurgAns) {
		this.repSurgAns = repSurgAns;
	}
	/**
	 * @return the repSurgAns
	 */
	public String getRepSurgAns() {
		return repSurgAns;
	}

	@Override
    public void reset(ActionMapping mapping, HttpServletRequest request) {
    	// TODO Auto-generated method stub
    	this.DHRNCMRRev = "";
    	this.CAPASCARRev = "";
    	this.compRev = "";
    	this.revCompdBy = "";
    	this.dtRevComp = "";
    	this.dtPartRec = "";
    	this.dtEvalSend = "";
    	this.DHRDev = "";
    	this.repSurgAns = "";
    	this.utilizedPac = "";
    	this.storage = "";
    	this.removalReq = "";
    	this.riskAnalysis = "";
    	this.NCMRSCARGen = "";
    	this.correctiveAct = "";
    	this.NCMRSCARId = "";
    	this.CAPAId = "";
    	this.QARevCompBy = "";
    	this.dtCompleted = "";
    }
	/**
	 * @param hCOMRSRId the hCOMRSRId to set
	 */
	public void sethCOMRSRId(String hCOMRSRId) {
		this.hCOMRSRId = hCOMRSRId;
	}
	/**
	 * @return the hCOMRSRId
	 */
	public String gethCOMRSRId() {
		return hCOMRSRId;
	}
	/**
	 * @param strMessage the strMessage to set
	 */
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}
	/**
	 * @return the strMessage
	 */
	public String getStrMessage() {
		return strMessage;
	}
	/**
	 * @param strRefType the strRefType to set
	 */
	public void setStrRefType(String strRefType) {
		this.strRefType = strRefType;
	}
	/**
	 * @return the strRefType
	 */
	public String getStrRefType() {
		return strRefType;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param resultSize the resultSize to set
	 */
	public void setResultSize(int resultSize) {
		this.resultSize = resultSize;
	}
	/**
	 * @return the resultSize
	 */
	public int getResultSize() {
		return resultSize;
	}

}
