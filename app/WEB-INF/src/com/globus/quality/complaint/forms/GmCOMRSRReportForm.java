package com.globus.quality.complaint.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmCOMRSRReportForm extends GmIncidentInfoForm {	
	
	private String xmlGridData = "";
	private String comorrsr 		= "";
	private String salesrep 		= "";
	private String comRSR   		= "";
	private String part      		= "";
	private String status       	= "";
	private String lot       	    = "";
	private String elapsedDays     	= "";
	private String frmDate          = "";
	private String toDate           = "";
	private String elapsedVal       = "";
	private String chooseAction     = "";
	private String empName     		= "";
	private String hCOMRSRID     	= "";
	private String hStatusId     	= "";
	private String hIssueTyp     	= "";
	private String  hDisplayNm    = "";
	private String  hRedirectURL  = "";
	private String  accessFl = "";
	private int intResultSize;
	private String complaint = "";
	private String complaintname = "";
	
	private ArrayList alComplaint = new ArrayList();
	private ArrayList alCOMRSRList  	 = new ArrayList();
	private ArrayList alRepList     	 = new ArrayList();
	private ArrayList alStatusList  	 = new ArrayList();
	private ArrayList alElapsedDays 	 = new ArrayList();
	private ArrayList alChooseActionList = new ArrayList();
	private ArrayList alEmpName 		 = new ArrayList();

	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}

	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}

	/**
	 * @return the comorrsr
	 */
	public String getComorrsr() {
		return comorrsr;
	}

	/**
	 * @param comorrsr the comorrsr to set
	 */
	public void setComorrsr(String comorrsr) {
		this.comorrsr = comorrsr;
	}

	/**
	 * @return the salesrep
	 */
	public String getSalesrep() {
		return salesrep;
	}

	/**
	 * @param salesrep the salesrep to set
	 */
	public void setSalesrep(String salesrep) {
		this.salesrep = salesrep;
	}

	/**
	 * @return the comRSR
	 */
	public String getComRSR() {
		return comRSR;
	}

	/**
	 * @param comRSR the comRSR to set
	 */
	public void setComRSR(String comRSR) {
		this.comRSR = comRSR;
	}

	/**
	 * @return the part
	 */
	public String getPart() {
		return part;
	}

	/**
	 * @param part the part to set
	 */
	public void setPart(String part) {
		this.part = part;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the lot
	 */
	public String getLot() {
		return lot;
	}

	/**
	 * @param lot the lot to set
	 */
	public void setLot(String lot) {
		this.lot = lot;
	}

	/**
	 * @return the elapsedDays
	 */
	public String getElapsedDays() {
		return elapsedDays;
	}

	/**
	 * @param elapsedDays the elapsedDays to set
	 */
	public void setElapsedDays(String elapsedDays) {
		this.elapsedDays = elapsedDays;
	}

	/**
	 * @return the frmDate
	 */
	public String getFrmDate() {
		return frmDate;
	}

	/**
	 * @param frmDate the frmDate to set
	 */
	public void setFrmDate(String frmDate) {
		this.frmDate = frmDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the chooseAction
	 */
	public String getChooseAction() {
		return chooseAction;
	}

	/**
	 * @param chooseAction the chooseAction to set
	 */
	public void setChooseAction(String chooseAction) {
		this.chooseAction = chooseAction;
	}

	/**
	 * @return the alCOMRSRList
	 */
	public ArrayList getAlCOMRSRList() {
		return alCOMRSRList;
	}

	/**
	 * @param alCOMRSRList the alCOMRSRList to set
	 */
	public void setAlCOMRSRList(ArrayList alCOMRSRList) {
		this.alCOMRSRList = alCOMRSRList;
	}

	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}

	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}

	/**
	 * @return the alStatusList
	 */
	public ArrayList getAlStatusList() {
		return alStatusList;
	}

	/**
	 * @param alStatusList the alStatusList to set
	 */
	public void setAlStatusList(ArrayList alStatusList) {
		this.alStatusList = alStatusList;
	}

	/**
	 * @return the alElapsedDays
	 */
	public ArrayList getAlElapsedDays() {
		return alElapsedDays;
	}

	/**
	 * @param alElapsedDays the alElapsedDays to set
	 */
	public void setAlElapsedDays(ArrayList alElapsedDays) {
		this.alElapsedDays = alElapsedDays;
	}

	/**
	 * @return the alChooseActionList
	 */
	public ArrayList getAlChooseActionList() {
		return alChooseActionList;
	}

	/**
	 * @param alChooseActionList the alChooseActionList to set
	 */
	public void setAlChooseActionList(ArrayList alChooseActionList) {
		this.alChooseActionList = alChooseActionList;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param alEmpName the alEmpName to set
	 */
	public void setAlEmpName(ArrayList alEmpName) {
		this.alEmpName = alEmpName;
	}

	/**
	 * @return the alEmpName
	 */
	public ArrayList getAlEmpName() {
		return alEmpName;
	}
	/**
	 * @param elapsedVal the elapsedVal to set
	 */
	public void setElapsedVal(String elapsedVal) {
		this.elapsedVal = elapsedVal;
	}

	/**
	 * @return the elapsedVal
	 */
	public String getElapsedVal() {
		return elapsedVal;
	}

	/**
	 * @param hCOMRSRID the hCOMRSRID to set
	 */
	public void sethCOMRSRID(String hCOMRSRID) {
		this.hCOMRSRID = hCOMRSRID;
	}

	/**
	 * @return the hCOMRSRID
	 */
	public String gethCOMRSRID() {
		return hCOMRSRID;
	}

	/**
	 * @param hStatusId the hStatusId to set
	 */
	public void sethStatusId(String hStatusId) {
		this.hStatusId = hStatusId;
	}

	/**
	 * @return the hStatusId
	 */
	public String gethStatusId() {
		return hStatusId;
	}

	/**
	 * @return the hIssueTyp
	 */
	public String gethIssueTyp() {
		return hIssueTyp;
	}

	/**
	 * @param hIssueTyp the hIssueTyp to set
	 */
	public void sethIssueTyp(String hIssueTyp) {
		this.hIssueTyp = hIssueTyp;
	}

	/**
	 * @return the hDisplayNm
	 */
	public String gethDisplayNm() {
		return hDisplayNm;
	}

	/**
	 * @param hDisplayNm the hDisplayNm to set
	 */
	public void sethDisplayNm(String hDisplayNm) {
		this.hDisplayNm = hDisplayNm;
	}

	/**
	 * @return the hRedirectURL
	 */
	public String gethRedirectURL() {
		return hRedirectURL;
	}

	/**
	 * @param hRedirectURL the hRedirectURL to set
	 */
	public void sethRedirectURL(String hRedirectURL) {
		this.hRedirectURL = hRedirectURL;
	}

	/**
	 * @param intResultSize the intResultSize to set
	 */
	public void setIntResultSize(int intResultSize) {
		this.intResultSize = intResultSize;
	}

	/**
	 * @return the intResultSize
	 */
	public int getIntResultSize() {
		return intResultSize;
	}

	public String getAccessFl() {
		return accessFl;
	}

	public void setAccessFl(String accessFl) {
		this.accessFl = accessFl;
	}

	public String getComplaint() {
		return complaint;
	}

	public void setComplaint(String complaint) {
		this.complaint = complaint;
	}

	public String getComplaintname() {
		return complaintname;
	}

	public void setComplaintname(String complaintname) {
		this.complaintname = complaintname;
	}

	public ArrayList getAlComplaint() {
		return alComplaint;
	}

	public void setAlComplaint(ArrayList alComplaint) {
		this.alComplaint = alComplaint;
	}
	
	
}