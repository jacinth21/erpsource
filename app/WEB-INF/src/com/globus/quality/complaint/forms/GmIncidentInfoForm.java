package com.globus.quality.complaint.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionMapping;
import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmUploadForm;

public class GmIncidentInfoForm extends GmUploadForm {
	
	// GENERAL DETAILS
	private String comRsrID = "";
	private String raID     = "";
	private String issueType = "";
	private String complaint = "";
	private String complaintname = "";
	private String status = "";
	private String address = "";
	private String phoneNumber = "";
	private String company = "";
	private String areaDirector = "";
	private String dtIncDate = "";
	private String incidentCat = "";
	private String complRecvdBy = "";
	private String complRecvDt = "";
	private String complRecvVia = "";
	private String originator = "";
	private String mdrReport = "";
	private String meddevReport = "";
	private String mdrID = "";
	private String meddevID = "";
	private String mdrReportDt = "";
	private String meddevReportDt = "";
	private String detailDescEvent = "";
	private String screenType = "";
	private String  statusNm 	= "";
	private String  partRecvDt 	= "";
	private int intSize = 0;
	// PART DETAILS 
	private String partNum = "";
	private String partDesc = "";
	private String project = "";
	private String gem = "";
	private String type = "";
	private String quantity = "";
	private String lonorConsign = "";
	private String projDesc 	= "";
	private String lotNum 	= "";
	
	// INCIDENT DETAILS
	
	private String normalWearFl = "";
	private String eventSurgeyFl = "";
	private String ifYesExplain = "";
	private String typeOfSurgery = "";
	private String reviSurgery = "";
	private String surgeoonName = "";
	private String adverseEffect = "";
	private String hospitalName = "";
	private String dateOfSurgery = "";
	private String postOpeFlag = "";
	private String postOpeDate = "";
	private String eventDemoFl = "";
	private String replacementFl = "";
	private String replacePartFl = "";
	private String partAvailEvalFl = "";
	private String deconFl = "";
	private String shippedFl = "";
	private String otherDetails = "";
	private String chooseAction = "";
	private String trackingNumber="";
	private String dateOfShipped="";
	private String carrier="";
	private String reqFieldService="";
	private String didThisEventHappenDuring="";
	private String procedureOutcome="";
	
	private String strJsonString = "";//PC-3183
	
	
	public String getStrJsonString() {
		return strJsonString;
	}
	public void setStrJsonString(String strJsonString) {
		this.strJsonString = strJsonString;
	}
	public String getDidThisEventHappenDuring() {
		return didThisEventHappenDuring;
	}
	public void setDidThisEventHappenDuring(String didThisEventHappenDuring) {
		this.didThisEventHappenDuring = didThisEventHappenDuring;
	}
	public String getProcedureOutcome() {
		return procedureOutcome;
	}
	public void setProcedureOutcome(String procedureOutcome) {
		this.procedureOutcome = procedureOutcome;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public String getDateOfShipped() {
		return dateOfShipped;
	}
	public void setDateOfShipped(String dateOfShipped) {
		this.dateOfShipped = dateOfShipped;
	}
	
	public String getReqFieldService() {
		return reqFieldService;
	}
	public void setReqFieldService(String reqFieldService) {
		this.reqFieldService = reqFieldService;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	private ArrayList alIssueType = new ArrayList();
	private ArrayList alComplaint = new ArrayList();
	private ArrayList alAreaDirect = new ArrayList();
	private ArrayList alIncidentCat = new ArrayList();
	private ArrayList alComplRecvBy = new ArrayList();
	private ArrayList alComplRecvVia = new ArrayList();
	private ArrayList alOriginator = new ArrayList();
	private ArrayList alProjectList = new ArrayList();
	private ArrayList alGemList = new ArrayList();
	private ArrayList alTypeList = new ArrayList();
	private ArrayList alLoanorConsign = new ArrayList();
	private ArrayList alHospitalList = new ArrayList();
	private ArrayList alChosseActionList = new ArrayList();
	
	private ArrayList alRepList = new ArrayList();
	private ArrayList alEmpList = new ArrayList();
	
	private ArrayList alCarrier = new ArrayList();
	private ArrayList alDidThisEventHappenDuring = new ArrayList();
	private ArrayList alprocedureOutcome = new ArrayList();
	private ArrayList aladverseEffect = new ArrayList();
	
	public ArrayList getAladverseEffect() {
		return aladverseEffect;
	}
	public void setAladverseEffect(ArrayList aladverseEffect) {
		this.aladverseEffect = aladverseEffect;
	}
	public ArrayList getAlprocedureOutcome() {
		return alprocedureOutcome;
	}
	public void setAlprocedureOutcome(ArrayList alprocedureOutcome) {
		this.alprocedureOutcome = alprocedureOutcome;
	}
	public ArrayList getAlDidThisEventHappenDuring() {
		return alDidThisEventHappenDuring;
	}
	public void setAlDidThisEventHappenDuring(ArrayList alDidThisEventHappenDuring) {
		this.alDidThisEventHappenDuring = alDidThisEventHappenDuring;
	}
	public ArrayList getAlCarrier() {
		return alCarrier;
	}
	public void setAlCarrier(ArrayList alCarrier) {
		this.alCarrier = alCarrier;
	}
	private String strMessage = "";
	
		
	/**
	 * @return the strMessage
	 */
	public String getStrMessage() {
		return strMessage;
	}
	/**
	 * @param strMessage the strMessage to set
	 */
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}
	/**
	 * @return the comRsrID
	 */
	public String getComRsrID() {
		return comRsrID;
	}
	/**
	 * @param comRsrID the comRsrID to set
	 */
	public void setComRsrID(String comRsrID) {
		this.comRsrID = comRsrID;
	}
	/**
	 * @return the raID
	 */
	public String getRaID() {
		return raID;
	}
	/**
	 * @param raID the raID to set
	 */
	public void setRaID(String raID) {
		this.raID = raID;
	}
	/**
	 * @return the issueType
	 */
	public String getIssueType() {
		return issueType;
	}
	/**
	 * @param issueType the issueType to set
	 */
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}
	/**
	 * @return the complaint
	 */
	public String getComplaint() {
		return complaint;
	}
	/**
	 * @param complaint the complaint to set
	 */
	public void setComplaint(String complaint) {
		this.complaint = complaint;
	}
	/**
	 * @return the complaintname
	 */
	public String getComplaintname() {
		return complaintname;
	}
	/**
	 * @param complaintname the complaintname to set
	 */
	public void setComplaintname(String complaintname) {
		this.complaintname = complaintname;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}
	/**
	 * @return the areaDirector
	 */
	public String getAreaDirector() {
		return areaDirector;
	}
	/**
	 * @param areaDirector the areaDirector to set
	 */
	public void setAreaDirector(String areaDirector) {
		this.areaDirector = areaDirector;
	}
	/**
	 * @return the dtIncDate
	 */
	public String getDtIncDate() {
		return dtIncDate;
	}
	/**
	 * @param dtIncDate the dtIncDate to set
	 */
	public void setDtIncDate(String dtIncDate) {
		this.dtIncDate = dtIncDate;
	}
	/**
	 * @return the incidentCat
	 */
	public String getIncidentCat() {
		return incidentCat;
	}
	/**
	 * @param incidentCat the incidentCat to set
	 */
	public void setIncidentCat(String incidentCat) {
		this.incidentCat = incidentCat;
	}
	/**
	 * @return the complRecvdBy
	 */
	public String getComplRecvdBy() {
		return complRecvdBy;
	}
	/**
	 * @param complRecvdBy the complRecvdBy to set
	 */
	public void setComplRecvdBy(String complRecvdBy) {
		this.complRecvdBy = complRecvdBy;
	}
	/**
	 * @return the complRecvDt
	 */
	public String getComplRecvDt() {
		return complRecvDt;
	}
	/**
	 * @param complRecvDt the complRecvDt to set
	 */
	public void setComplRecvDt(String complRecvDt) {
		this.complRecvDt = complRecvDt;
	}
	/**
	 * @return the complRecvVia
	 */
	public String getComplRecvVia() {
		return complRecvVia;
	}
	/**
	 * @param complRecvVia the complRecvVia to set
	 */
	public void setComplRecvVia(String complRecvVia) {
		this.complRecvVia = complRecvVia;
	}
	/**
	 * @return the originator
	 */
	public String getOriginator() {
		return originator;
	}
	/**
	 * @param originator the originator to set
	 */
	public void setOriginator(String originator) {
		this.originator = originator;
	}
	/**
	 * @return the mdrReport
	 */
	public String getMdrReport() {
		return mdrReport;
	}
	/**
	 * @param mdrReport the mdrReport to set
	 */
	public void setMdrReport(String mdrReport) {
		this.mdrReport = mdrReport;
	}
	/**
	 * @return the meddevReport
	 */
	public String getMeddevReport() {
		return meddevReport;
	}
	/**
	 * @param meddevReport the meddevReport to set
	 */
	public void setMeddevReport(String meddevReport) {
		this.meddevReport = meddevReport;
	}
	/**
	 * @return the mdrID
	 */
	public String getMdrID() {
		return mdrID;
	}
	/**
	 * @param mdrID the mdrID to set
	 */
	public void setMdrID(String mdrID) {
		this.mdrID = mdrID;
	}
	/**
	 * @return the meddevID
	 */
	public String getMeddevID() {
		return meddevID;
	}
	/**
	 * @param meddevID the meddevID to set
	 */
	public void setMeddevID(String meddevID) {
		this.meddevID = meddevID;
	}
	/**
	 * @return the mdrReportDt
	 */
	public String getMdrReportDt() {
		return mdrReportDt;
	}
	/**
	 * @param mdrReportDt the mdrReportDt to set
	 */
	public void setMdrReportDt(String mdrReportDt) {
		this.mdrReportDt = mdrReportDt;
	}
	/**
	 * @return the meddevReportDt
	 */
	public String getMeddevReportDt() {
		return meddevReportDt;
	}
	/**
	 * @param meddevReportDt the meddevReportDt to set
	 */
	public void setMeddevReportDt(String meddevReportDt) {
		this.meddevReportDt = meddevReportDt;
	}
	/**
	 * @return the detailDescEvent
	 */
	public String getDetailDescEvent() {
		return detailDescEvent;
	}
	/**
	 * @param detailDescEvent the detailDescEvent to set
	 */
	public void setDetailDescEvent(String detailDescEvent) {
		this.detailDescEvent = detailDescEvent;
	}
	/**
	 * @return the screenType
	 */
	public String getScreenType() {
		return screenType;
	}
	/**
	 * @param screenType the screenType to set
	 */
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	/**
	 * @return the statusNm
	 */
	public String getStatusNm() {
		return statusNm;
	}
	/**
	 * @param statusNm the statusNm to set
	 */
	public void setStatusNm(String statusNm) {
		this.statusNm = statusNm;
	}
	/**
	 * @return the intSize
	 */
	public int getIntSize() {
		return intSize;
	}
	/**
	 * @param intSize the intSize to set
	 */
	public void setIntSize(int intSize) {
		this.intSize = intSize;
	}
	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}
	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}
	/**
	 * @return the partDesc
	 */
	public String getPartDesc() {
		return partDesc;
	}
	/**
	 * @param partDesc the partDesc to set
	 */
	public void setPartDesc(String partDesc) {
		this.partDesc = partDesc;
	}
	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}
	/**
	 * @param project the project to set
	 */
	public void setProject(String project) {
		this.project = project;
	}
	/**
	 * @return the gem
	 */
	public String getGem() {
		return gem;
	}
	/**
	 * @param gem the gem to set
	 */
	public void setGem(String gem) {
		this.gem = gem;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the quantity
	 */
	public String getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return the lonorConsign
	 */
	public String getLonorConsign() {
		return lonorConsign;
	}
	/**
	 * @param lonorConsign the lonorConsign to set
	 */
	public void setLonorConsign(String lonorConsign) {
		this.lonorConsign = lonorConsign;
	}
	/**
	 * @return the projDesc
	 */
	public String getProjDesc() {
		return projDesc;
	}
	/**
	 * @param projDesc the projDesc to set
	 */
	public void setProjDesc(String projDesc) {
		this.projDesc = projDesc;
	}
	/**
	 * @return the lotNum
	 */
	public String getLotNum() {
		return lotNum;
	}
	/**
	 * @param lotNum the lotNum to set
	 */
	public void setLotNum(String lotNum) {
		this.lotNum = lotNum;
	}
	/**
	 * @return the normalWearFl
	 */
	public String getNormalWearFl() {
		return normalWearFl;
	}
	/**
	 * @param normalWearFl the normalWearFl to set
	 */
	public void setNormalWearFl(String normalWearFl) {
		this.normalWearFl = normalWearFl;
	}
	/**
	 * @return the eventSurgeyFl
	 */
	public String getEventSurgeyFl() {
		return eventSurgeyFl;
	}
	/**
	 * @param eventSurgeyFl the eventSurgeyFl to set
	 */
	public void setEventSurgeyFl(String eventSurgeyFl) {
		this.eventSurgeyFl = eventSurgeyFl;
	}
	/**
	 * @return the ifYesExplain
	 */
	public String getIfYesExplain() {
		return ifYesExplain;
	}
	/**
	 * @param ifYesExplain the ifYesExplain to set
	 */
	public void setIfYesExplain(String ifYesExplain) {
		this.ifYesExplain = ifYesExplain;
	}
	/**
	 * @return the typeOfSurgery
	 */
	public String getTypeOfSurgery() {
		return typeOfSurgery;
	}
	/**
	 * @param typeOfSurgery the typeOfSurgery to set
	 */
	public void setTypeOfSurgery(String typeOfSurgery) {
		this.typeOfSurgery = typeOfSurgery;
	}
	/**
	 * @return the reviSurgery
	 */
	public String getReviSurgery() {
		return reviSurgery;
	}
	/**
	 * @param reviSurgery the reviSurgery to set
	 */
	public void setReviSurgery(String reviSurgery) {
		this.reviSurgery = reviSurgery;
	}
	/**
	 * @return the surgeoonName
	 */
	public String getSurgeoonName() {
		return surgeoonName;
	}
	/**
	 * @param surgeoonName the surgeoonName to set
	 */
	public void setSurgeoonName(String surgeoonName) {
		this.surgeoonName = surgeoonName;
	}
	/**
	 * @return the adverseEffect
	 */
	public String getAdverseEffect() {
		return adverseEffect;
	}
	/**
	 * @param adverseEffect the adverseEffect to set
	 */
	public void setAdverseEffect(String adverseEffect) {
		this.adverseEffect = adverseEffect;
	}
	/**
	 * @return the hospitalName
	 */
	public String getHospitalName() {
		return hospitalName;
	}
	/**
	 * @param hospitalName the hospitalName to set
	 */
	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}
	/**
	 * @return the dateOfSurgery
	 */
	public String getDateOfSurgery() {
		return dateOfSurgery;
	}
	/**
	 * @param dateOfSurgery the dateOfSurgery to set
	 */
	public void setDateOfSurgery(String dateOfSurgery) {
		this.dateOfSurgery = dateOfSurgery;
	}
	/**
	 * @return the postOpeFlag
	 */
	public String getPostOpeFlag() {
		return postOpeFlag;
	}
	/**
	 * @param postOpeFlag the postOpeFlag to set
	 */
	public void setPostOpeFlag(String postOpeFlag) {
		this.postOpeFlag = postOpeFlag;
	}
	/**
	 * @return the postOpeDate
	 */
	public String getPostOpeDate() {
		return postOpeDate;
	}
	/**
	 * @param postOpeDate the postOpeDate to set
	 */
	public void setPostOpeDate(String postOpeDate) {
		this.postOpeDate = postOpeDate;
	}
	/**
	 * @return the eventDemoFl
	 */
	public String getEventDemoFl() {
		return eventDemoFl;
	}
	/**
	 * @param eventDemoFl the eventDemoFl to set
	 */
	public void setEventDemoFl(String eventDemoFl) {
		this.eventDemoFl = eventDemoFl;
	}
	/**
	 * @return the replacementFl
	 */
	public String getReplacementFl() {
		return replacementFl;
	}
	/**
	 * @param replacementFl the replacementFl to set
	 */
	public void setReplacementFl(String replacementFl) {
		this.replacementFl = replacementFl;
	}
	/**
	 * @return the replacePartFl
	 */
	public String getReplacePartFl() {
		return replacePartFl;
	}
	/**
	 * @param replacePartFl the replacePartFl to set
	 */
	public void setReplacePartFl(String replacePartFl) {
		this.replacePartFl = replacePartFl;
	}
	/**
	 * @return the partAvailEvalFl
	 */
	public String getPartAvailEvalFl() {
		return partAvailEvalFl;
	}
	/**
	 * @param partAvailEvalFl the partAvailEvalFl to set
	 */
	public void setPartAvailEvalFl(String partAvailEvalFl) {
		this.partAvailEvalFl = partAvailEvalFl;
	}
	/**
	 * @return the deconFl
	 */
	public String getDeconFl() {
		return deconFl;
	}
	/**
	 * @param deconFl the deconFl to set
	 */
	public void setDeconFl(String deconFl) {
		this.deconFl = deconFl;
	}
	/**
	 * @return the shippedFl
	 */
	public String getShippedFl() {
		return shippedFl;
	}
	/**
	 * @param shippedFl the shippedFl to set
	 */
	public void setShippedFl(String shippedFl) {
		this.shippedFl = shippedFl;
	}
	/**
	 * @return the otherDetails
	 */
	public String getOtherDetails() {
		return otherDetails;
	}
	/**
	 * @param otherDetails the otherDetails to set
	 */
	public void setOtherDetails(String otherDetails) {
		this.otherDetails = otherDetails;
	}
	/**
	 * @return the chooseAction
	 */
	public String getChooseAction() {
		return chooseAction;
	}
	/**
	 * @param chooseAction the chooseAction to set
	 */
	public void setChooseAction(String chooseAction) {
		this.chooseAction = chooseAction;
	}
	/**
	 * @return the alIssueType
	 */
	public ArrayList getAlIssueType() {
		return alIssueType;
	}
	/**
	 * @param alIssueType the alIssueType to set
	 */
	public void setAlIssueType(ArrayList alIssueType) {
		this.alIssueType = alIssueType;
	}
	/**
	 * @return the alComplaint
	 */
	public ArrayList getAlComplaint() {
		return alComplaint;
	}
	/**
	 * @param alComplaint the alComplaint to set
	 */
	public void setAlComplaint(ArrayList alComplaint) {
		this.alComplaint = alComplaint;
	}
	/**
	 * @return the alAreaDirect
	 */
	public ArrayList getAlAreaDirect() {
		return alAreaDirect;
	}
	/**
	 * @param alAreaDirect the alAreaDirect to set
	 */
	public void setAlAreaDirect(ArrayList alAreaDirect) {
		this.alAreaDirect = alAreaDirect;
	}
	/**
	 * @return the alIncidentCat
	 */
	public ArrayList getAlIncidentCat() {
		return alIncidentCat;
	}
	/**
	 * @param alIncidentCat the alIncidentCat to set
	 */
	public void setAlIncidentCat(ArrayList alIncidentCat) {
		this.alIncidentCat = alIncidentCat;
	}
	/**
	 * @return the alComplRecvBy
	 */
	public ArrayList getAlComplRecvBy() {
		return alComplRecvBy;
	}
	/**
	 * @param alComplRecvBy the alComplRecvBy to set
	 */
	public void setAlComplRecvBy(ArrayList alComplRecvBy) {
		this.alComplRecvBy = alComplRecvBy;
	}
	/**
	 * @return the alComplRecvVia
	 */
	public ArrayList getAlComplRecvVia() {
		return alComplRecvVia;
	}
	/**
	 * @param alComplRecvVia the alComplRecvVia to set
	 */
	public void setAlComplRecvVia(ArrayList alComplRecvVia) {
		this.alComplRecvVia = alComplRecvVia;
	}
	/**
	 * @return the alOriginator
	 */
	public ArrayList getAlOriginator() {
		return alOriginator;
	}
	/**
	 * @param alOriginator the alOriginator to set
	 */
	public void setAlOriginator(ArrayList alOriginator) {
		this.alOriginator = alOriginator;
	}
	/**
	 * @return the alProjectList
	 */
	public ArrayList getAlProjectList() {
		return alProjectList;
	}
	/**
	 * @param alProjectList the alProjectList to set
	 */
	public void setAlProjectList(ArrayList alProjectList) {
		this.alProjectList = alProjectList;
	}
	/**
	 * @return the alGemList
	 */
	public ArrayList getAlGemList() {
		return alGemList;
	}
	/**
	 * @param alGemList the alGemList to set
	 */
	public void setAlGemList(ArrayList alGemList) {
		this.alGemList = alGemList;
	}
	/**
	 * @return the alTypeList
	 */
	public ArrayList getAlTypeList() {
		return alTypeList;
	}
	/**
	 * @param alTypeList the alTypeList to set
	 */
	public void setAlTypeList(ArrayList alTypeList) {
		this.alTypeList = alTypeList;
	}
	/**
	 * @return the alLoanorConsign
	 */
	public ArrayList getAlLoanorConsign() {
		return alLoanorConsign;
	}
	/**
	 * @param alLoanorConsign the alLoanorConsign to set
	 */
	public void setAlLoanorConsign(ArrayList alLoanorConsign) {
		this.alLoanorConsign = alLoanorConsign;
	}
	/**
	 * @return the alHospitalList
	 */
	public ArrayList getAlHospitalList() {
		return alHospitalList;
	}
	/**
	 * @param alHospitalList the alHospitalList to set
	 */
	public void setAlHospitalList(ArrayList alHospitalList) {
		this.alHospitalList = alHospitalList;
	}
	/**
	 * @return the alChosseActionList
	 */
	public ArrayList getAlChosseActionList() {
		return alChosseActionList;
	}
	/**
	 * @param alChosseActionList the alChosseActionList to set
	 */
	public void setAlChosseActionList(ArrayList alChosseActionList) {
		this.alChosseActionList = alChosseActionList;
	}
	/**
	 * @return the alRepList
	 */
	public ArrayList getAlRepList() {
		return alRepList;
	}
	/**
	 * @param alRepList the alRepList to set
	 */
	public void setAlRepList(ArrayList alRepList) {
		this.alRepList = alRepList;
	}
	/**
	 * @return the alEmpList
	 */
	public ArrayList getAlEmpList() {
		return alEmpList;
	}
	/**
	 * @param alEmpList the alEmpList to set
	 */
	public void setAlEmpList(ArrayList alEmpList) {
		this.alEmpList = alEmpList;
	}
	/**
	 * @param partRecvDt the partRecvDt to set
	 */
	public void setPartRecvDt(String partRecvDt) {
		this.partRecvDt = partRecvDt;
	}
	/**
	 * @return the partRecvDt
	 */
	public String getPartRecvDt() {
		return partRecvDt;
	}
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// GENERAL DETAILS
		
		this.issueType       = "0";
		this.complaint       = "0";
		this.complaintname   = "0";
		this.status 	     = "";
		this.company         = "";	
		this.address         = "";
		this.phoneNumber     = "";	
		this.areaDirector    = "0";
		this.dtIncDate       = "";
		this.incidentCat     = "";
		this.complRecvdBy    = "0";
		this.complRecvDt     = "";
		this.complRecvVia    = "0";
		this.originator      = "0";
		this.mdrReport       = "";
		this.meddevReport    = "";
		this.mdrID 		     = "";
		this.meddevID 	     = "";
		this.mdrReportDt     = "";
		this.meddevReportDt  = "";
		this.detailDescEvent = "";
		
		// PART DETAILS 
		this.partNum         = "";
		this.partDesc        = "";
		this.project         = "0";
		this.gem		     = "0";
		this.type		     = "0";
		this.quantity        = "";
		this.lonorConsign    = "0";
		this.lotNum 	  	 = "";
		
		// INCIDENT DETAILS	
		this.normalWearFl    = "";
		this.eventSurgeyFl   = "";
		this.ifYesExplain    = "";
		this.typeOfSurgery   = "";
		this.reviSurgery     = "";
		this.surgeoonName    = "";
		this.adverseEffect   = "";
		this.hospitalName    = "0";
		this.dateOfSurgery   = "";
		this.postOpeFlag     = "";
		this.postOpeDate     = "";
		this.eventDemoFl     = "";
		this.replacementFl   = "";
		this.replacePartFl   = "";
		this.partAvailEvalFl = "";
		this.deconFl 		 = "";
		this.shippedFl       = "";
		this.otherDetails    = "";
		this.trackingNumber  = "";//PC-3183
		this.dateOfShipped   = "";//PC-3183
		this.carrier         = "0";//PC-3183
		this.reqFieldService = ""; //PC-3183
		this.didThisEventHappenDuring = "0"; //PC-3183
		this.procedureOutcome = "0"; // PC-3183
	}
	public void onreset(ActionMapping mapping, HttpServletRequest request) {
		// GENERAL DETAILS
		this.dtIncDate       = "";
		this.incidentCat     = "";
		this.complRecvVia    = "0";
		this.mdrReport       = "";
		this.meddevReport    = "";
		this.mdrID 		     = "";
		this.meddevID 	     = "";
		this.mdrReportDt     = "";
		this.meddevReportDt  = "";
		this.detailDescEvent = "";
		// PART DETAILS 
		this.gem		     = "0";
		this.lonorConsign    = "0";
		// INCIDENT DETAILS	
		this.normalWearFl    = "";
		this.eventSurgeyFl   = "";
		this.ifYesExplain    = "";
		this.typeOfSurgery   = "";
		this.reviSurgery     = "";
		this.surgeoonName    = "";
		this.adverseEffect   = "";
		this.hospitalName    = "0";
		this.dateOfSurgery   = "";
		this.postOpeFlag     = "";
		this.postOpeDate     = "";
		this.eventDemoFl     = "";
		this.replacementFl   = "";
		this.replacePartFl   = "";
		this.partAvailEvalFl = "";
		this.deconFl 		 = "";
		this.shippedFl       = "";
		this.otherDetails    = "";
		this.trackingNumber  = "";//PC-3183
		this.dateOfShipped   = "";//PC-3183
		this.carrier         = "0";//PC-3183
		this.reqFieldService = ""; //PC-3183
		this.didThisEventHappenDuring = "0"; //PC-3183
		this.procedureOutcome = "0"; // PC-3183
		
	}
}