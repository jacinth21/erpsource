
package com.globus.quality.complaint.forms;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

import com.globus.common.forms.GmUploadForm;

/**
 * @author arajan
 *
 */
public class GmCOMRSRProcessForm extends GmUploadForm {

	private String comRsrID = "";
	private String statusnm = "";
	private String repName = "";
	private String incidentCatNm = "";
	private String complRecvDt = "";
	private String surgeoonName = "";
	private String detailDescEvent = "";
	private String mdrID = "";
	private String mdrReportDt = "";
	private String meddevID = "";
	private String meddevReportDt = "";
	private String partnum = "";
	private String partdesc = "";
	private String project = "";
	private String gemnm = "";
	private String typenm = "";
	private String lotnum = "";
	private String comRsrDispTyp = "";
	private String evalResult = "";
	private String pdtDispTyp = "";
	private String capaComments = "";
	private String closureDt = "";
	private String prvActions = "";
	private String strXmlData = "";
	private String projdesc = "";
	private int intRecSize;
	
	private String refID         = "";
	private String toEmail       = "";
	private String ccEmail       = "";
	private String emailSubject  = "";
	private String emailCommnets = "";
	private String issueType     = "";
	private String issueBy       = "";
	private String userName      = "";
	private String userDesignation = "";
	private String image_path = "";
	
	private String closeCOMRSRID       = "";
	private String closedBy       = "";
	private String closedDt       = "";
	private String prevComments = "";
	private String closeCmts      = "";
	private String chooseAction      = "";
	private int     intSize       = 0;
	private String strMessage      = "";
	private String status      = "";
	private String hDisplayNm = "";
	public String gethDisplayNm() {
		return hDisplayNm;
	}
	public void sethDisplayNm(String hDisplayNm) {
		this.hDisplayNm = hDisplayNm;
	}
	public String gethRedirectURL() {
		return hRedirectURL;
	}
	public void sethRedirectURL(String hRedirectURL) {
		this.hRedirectURL = hRedirectURL;
	}
	private String hRedirectURL = "";
	private ArrayList alEmployeeList = new ArrayList();
	
	private ArrayList alChooseActionList = new ArrayList();
	
	/**
	 * @return the comRsrID
	 */
	public String getComRsrID() {
		return comRsrID;
	}
	/**
	 * @param comRsrID the comRsrID to set
	 */
	public void setComRsrID(String comRsrID) {
		this.comRsrID = comRsrID;
	}
	/**
	 * @return the statusnm
	 */
	public String getStatusnm() {
		return statusnm;
	}
	/**
	 * @param statusnm the statusnm to set
	 */
	public void setStatusnm(String statusnm) {
		this.statusnm = statusnm;
	}
	/**
	 * @return the repName
	 */
	public String getRepName() {
		return repName;
	}
	/**
	 * @param repName the repName to set
	 */
	public void setRepName(String repName) {
		this.repName = repName;
	}
	/**
	 * @return the complRecvDt
	 */
	public String getComplRecvDt() {
		return complRecvDt;
	}
	/**
	 * @param complRecvDt the complRecvDt to set
	 */
	public void setComplRecvDt(String complRecvDt) {
		this.complRecvDt = complRecvDt;
	}
	/**
	 * @return the surgeoonName
	 */
	public String getSurgeoonName() {
		return surgeoonName;
	}
	/**
	 * @param surgeoonName the surgeoonName to set
	 */
	public void setSurgeoonName(String surgeoonName) {
		this.surgeoonName = surgeoonName;
	}
	public void setDetailDescEvent(String detailDescEvent) {
		this.detailDescEvent = detailDescEvent;
	}
	public String getDetailDescEvent() {
		return detailDescEvent;
	}
	/**
	 * @return the mdrID
	 */
	public String getMdrID() {
		return mdrID;
	}
	/**
	 * @param mdrID the mdrID to set
	 */
	public void setMdrID(String mdrID) {
		this.mdrID = mdrID;
	}
	/**
	 * @return the mdrReportDt
	 */
	public String getMdrReportDt() {
		return mdrReportDt;
	}
	/**
	 * @param mdrReportDt the mdrReportDt to set
	 */
	public void setMdrReportDt(String mdrReportDt) {
		this.mdrReportDt = mdrReportDt;
	}
	/**
	 * @return the meddevID
	 */
	public String getMeddevID() {
		return meddevID;
	}
	/**
	 * @param meddevID the meddevID to set
	 */
	public void setMeddevID(String meddevID) {
		this.meddevID = meddevID;
	}
	/**
	 * @return the meddevReportDt
	 */
	public String getMeddevReportDt() {
		return meddevReportDt;
	}
	/**
	 * @param meddevReportDt the meddevReportDt to set
	 */
	public void setMeddevReportDt(String meddevReportDt) {
		this.meddevReportDt = meddevReportDt;
	}
	/**
	 * @return the partnum
	 */
	public String getPartnum() {
		return partnum;
	}
	/**
	 * @param partnum the partnum to set
	 */
	public void setPartnum(String partnum) {
		this.partnum = partnum;
	}
	/**
	 * @return the partdesc
	 */
	public String getPartdesc() {
		return partdesc;
	}
	/**
	 * @param partdesc the partdesc to set
	 */
	public void setPartdesc(String partdesc) {
		this.partdesc = partdesc;
	}
	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}
	/**
	 * @param project the project to set
	 */
	public void setProject(String project) {
		this.project = project;
	}
	/**
	 * @return the gemnm
	 */
	public String getGemnm() {
		return gemnm;
	}
	/**
	 * @param gemnm the gemnm to set
	 */
	public void setGemnm(String gemnm) {
		this.gemnm = gemnm;
	}
	/**
	 * @return the typenm
	 */
	public String getTypenm() {
		return typenm;
	}
	/**
	 * @param typenm the typenm to set
	 */
	public void setTypenm(String typenm) {
		this.typenm = typenm;
	}
	/**
	 * @return the lotnum
	 */
	public String getLotnum() {
		return lotnum;
	}
	/**
	 * @param lotnum the lotnum to set
	 */
	public void setLotnum(String lotnum) {
		this.lotnum = lotnum;
	}
	/**
	 * @return the comRsrDispTyp
	 */
	public String getComRsrDispTyp() {
		return comRsrDispTyp;
	}
	/**
	 * @param comRsrDispTyp the comRsrDispTyp to set
	 */
	public void setComRsrDispTyp(String comRsrDispTyp) {
		this.comRsrDispTyp = comRsrDispTyp;
	}
	/**
	 * @return the pdtDispTyp
	 */
	public String getPdtDispTyp() {
		return pdtDispTyp;
	}
	/**
	 * @param pdtDispTyp the pdtDispTyp to set
	 */
	public void setPdtDispTyp(String pdtDispTyp) {
		this.pdtDispTyp = pdtDispTyp;
	}
	/**
	 * @return the capaComments
	 */
	public String getCapaComments() {
		return capaComments;
	}
	/**
	 * @param capaComments the capaComments to set
	 */
	public void setCapaComments(String capaComments) {
		this.capaComments = capaComments;
	}
	/**
	 * @param evalResult the evalResult to set
	 */
	public void setEvalResult(String evalResult) {
		this.evalResult = evalResult;
	}
	/**
	 * @return the evalResult
	 */
	public String getEvalResult() {
		return evalResult;
	}
	/**
	 * @param closureDt the closureDt to set
	 */
	public void setClosureDt(String closureDt) {
		this.closureDt = closureDt;
	}
	/**
	 * @return the closureDt
	 */
	public String getClosureDt() {
		return closureDt;
	}
	/**
	 * @param prvActions the prvActions to set
	 */
	public void setPrvActions(String prvActions) {
		this.prvActions = prvActions;
	}
	/**
	 * @return the prvActions
	 */
	public String getPrvActions() {
		return prvActions;
	}
	/**
	 * @param intRecSize the intRecSize to set
	 */
	public void setIntRecSize(int intRecSize) {
		this.intRecSize = intRecSize;
	}
	/**
	 * @return the intRecSize
	 */
	public int getIntRecSize() {
		return intRecSize;
	}
	/**
	 * @param strXmlData the strXmlData to set
	 */
	public void setStrXmlData(String strXmlData) {
		this.strXmlData = strXmlData;
	}
	/**
	 * @return the strXmlData
	 */
	public String getStrXmlData() {
		return strXmlData;
	}
	/**
	 * @param incidentCatNm the incidentCatNm to set
	 */
	public void setIncidentCatNm(String incidentCatNm) {
		this.incidentCatNm = incidentCatNm;
	}
	/**
	 * @return the incidentCatNm
	 */
	public String getIncidentCatNm() {
		return incidentCatNm;
	}
	/**
	 * @return the refID
	 */
	public String getRefID() {
		return refID;
	}
	/**
	 * @param refID the refID to set
	 */
	public void setRefID(String refID) {
		this.refID = refID;
	}
	/**
	 * @return the toEmail
	 */
	public String getToEmail() {
		return toEmail;
	}
	/**
	 * @param toEmail the toEmail to set
	 */
	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}
	/**
	 * @return the ccEmail
	 */
	public String getCcEmail() {
		return ccEmail;
	}
	/**
	 * @param ccEmail the ccEmail to set
	 */
	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}
	/**
	 * @return the emailSubject
	 */
	public String getEmailSubject() {
		return emailSubject;
	}
	/**
	 * @param emailSubject the emailSubject to set
	 */
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	/**
	 * @return the emailCommnets
	 */
	public String getEmailCommnets() {
		return emailCommnets;
	}
	/**
	 * @param emailCommnets the emailCommnets to set
	 */
	public void setEmailCommnets(String emailCommnets) {
		this.emailCommnets = emailCommnets;
	}
	/**
	 * @return the issueType
	 */
	public String getIssueType() {
		return issueType;
	}
	/**
	 * @param issueType the issueType to set
	 */
	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}
	/**
	 * @return the issueBy
	 */
	public String getIssueBy() {
		return issueBy;
	}
	/**
	 * @param issueBy the issueBy to set
	 */
	public void setIssueBy(String issueBy) {
		this.issueBy = issueBy;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the userDesignation
	 */
	public String getUserDesignation() {
		return userDesignation;
	}
	/**
	 * @param userDesignation the userDesignation to set
	 */
	public void setUserDesignation(String userDesignation) {
		this.userDesignation = userDesignation;
	}
	
	public String getImage_path() {
		return image_path;
	}
	public void setImage_path(String image_path) {
		this.image_path = image_path;
	}
	/**
	 * @return the strClosedBy
	 */
	public String getClosedBy() {
		return closedBy;
	}
	/**
	 * @param strClosedBy the strClosedBy to set
	 */
	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}
	
	/**
	 * @return the closeCmts
	 */
	public String getCloseCmts() {
		return closeCmts;
	}
	/**
	 * @param closeCmts the closeCmts to set
	 */
	public void setCloseCmts(String closeCmts) {
		this.closeCmts = closeCmts;
	}
	/**
	 * @return the alEmployeeList
	 */
	public ArrayList getAlEmployeeList() {
		return alEmployeeList;
	}
	/**
	 * @param alEmployeeList the alEmployeeList to set
	 */
	public void setAlEmployeeList(ArrayList alEmployeeList) {
		this.alEmployeeList = alEmployeeList;
	}
	/**
	 * @return the chooseAction
	 */
	public String getChooseAction() {
		return chooseAction;
	}
	/**
	 * @param chooseAction the chooseAction to set
	 */
	public void setChooseAction(String chooseAction) {
		this.chooseAction = chooseAction;
	}
	
	/**
	 * @return the intSize
	 */
	public int getIntSize() {
		return intSize;
	}
	/**
	 * @param intSize the intSize to set
	 */
	public void setIntSize(int intSize) {
		this.intSize = intSize;
	}
	/**
	 * @return the alChooseActionList
	 */
	public ArrayList getAlChooseActionList() {
		return alChooseActionList;
	}
	/**
	 * @param alChooseActionList the alChooseActionList to set
	 */
	public void setAlChooseActionList(ArrayList alChooseActionList) {
		this.alChooseActionList = alChooseActionList;
	}
	
	public void onreset(ActionMapping mapping, HttpServletRequest request) {
		
		this.prevComments ="";
    	this.closeCmts       = "";
    	}
	
	/**
	 * @return the strMessage
	 */
	public String getStrMessage() {
		return strMessage;
	}
	/**
	 * @param strMessage the strMessage to set
	 */
	public void setStrMessage(String strMessage) {
		this.strMessage = strMessage;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the closedDt
	 */
	public String getClosedDt() {
		return closedDt;
	}
	/**
	 * @param closedDt the closedDt to set
	 */
	public void setClosedDt(String closedDt) {
		this.closedDt = closedDt;
	}
	
	/**
	 * @return the closeCOMRSRID
	 */
	public String getCloseCOMRSRID() {
		return closeCOMRSRID;
	}
	/**
	 * @param closeCOMRSRID the closeCOMRSRID to set
	 */
	public void setCloseCOMRSRID(String closeCOMRSRID) {
		this.closeCOMRSRID = closeCOMRSRID;
	}
	/**
	 * @return the prevComments
	 */
	public String getPrevComments() {
		return prevComments;
	}
	/**
	 * @param prevComments the prevComments to set
	 */
	public void setPrevComments(String prevComments) {
		this.prevComments = prevComments;
	}
	/**
	 * @return the hDisplayNm
	 */
	public String getHDisplayNm() {
		return hDisplayNm;
	}
	/**
	 * @param hDisplayNm the hDisplayNm to set
	 */
	public void setHDisplayNm(String hDisplayNm) {
		this.hDisplayNm = hDisplayNm;
	}
	/**
	 * @return the hRedirectURL
	 */
	public String getHRedirectURL() {
		return hRedirectURL;
	}
	/**
	 * @param hRedirectURL the hRedirectURL to set
	 */
	public void setHRedirectURL(String hRedirectURL) {
		this.hRedirectURL = hRedirectURL;
	}
	public String getProjdesc() {
		return projdesc;
	}
	public void setProjdesc(String projdesc) {
		this.projdesc = projdesc;
	}
	
	
	
	
	
}
