package com.globus.quality.complaint.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmResourceBundleBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
import com.globus.common.beans.GmLogger;
import com.globus.common.beans.GmSearchCriteria;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.custservice.beans.GmSalesBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.quality.complaint.beans.GmCOMRSRRptBean;
import com.globus.quality.complaint.beans.GmCOMRSRTransBean;
import com.globus.quality.complaint.forms.GmEnggEvaluationForm;
import com.globus.quality.complaint.forms.GmIncidentInfoForm;
import com.globus.quality.complaint.forms.GmQAEvaluationForm;
import com.globus.sales.event.beans.GmEventSetupBean;


public class GmCOMRSRSetupAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * addIncidentInfo This method used for creating/saving the Incident Information.
   * 
   * @exception AppError
   */
  public ActionForward addIncidentInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {	 

    instantiate(request, response);
    GmIncidentInfoForm gmIncidentInfoForm = (GmIncidentInfoForm) actionForm;
    gmIncidentInfoForm.loadSessionParameters(request);
    GmCustomerBean gmCustomerBean = new GmCustomerBean();
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());

    GmCOMRSRTransBean gmCOMRSRTransBean = new GmCOMRSRTransBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmRepvalues = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    String strIncidentID = "";
    String strForwardName = "gmIncidentInfo";
    String strAddress = "";
    String strPhone = "";
    String strAreaDirect = "";
    String strAjaxString = "";
    String strPartNumber = "";
    String strRepname = "";
    String strRaID = "";
    String strPart = "";
    String strPartDesc = "";
    String strPartFamly = "";
    String strProject = "";
    String strIssueType = "";
    String strRepId = "";
    String strQty = "";
    String strLotNum = "";
    String strCompany = "";
    String strComplRecBy = "";
    String strComplRecDt = "";
    String strSuccessMsg = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmIncidentInfoForm);
    //strIncidentID = GmCommonClass.parseNull(gmIncidentInfoForm.getComRsrID().toUpperCase());    
    strIncidentID = GmCommonClass.parseNull((String) hmParam.get("COMRSRID"));
    
    hmParam.put("COMRSRID", strIncidentID);
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strUser = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    String strAccessFl = "";
    String strPartyId = GmCommonClass.parseNull(gmIncidentInfoForm.getSessPartyId());
    strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "COM_RSR_USER");
    if (strAccessFl.equals("Y")) {
      if (strOpt.equals("RALoad") || strOpt.equals("Load")) {
        loadIncidentInfo(gmIncidentInfoForm);
        strRaID = GmCommonClass.parseNull(gmIncidentInfoForm.getRaID());
        strPartNumber = GmCommonClass.parseNull(gmIncidentInfoForm.getPartNum());
        //Adding space after M for the parts SC3295-36MM -3.5 .because we removing space in js function
        strPartNumber = strPartNumber.replaceAll("M-", "M -");
        gmIncidentInfoForm.setPartNum(strPartNumber);
        log.debug("strPartNumber=="+strPartNumber);
        
        if (!strRaID.equals("")) {
          hmParam.put("RAID", strRaID);
          hmParam.put("COMPLAINTNAME", "");
          hmParam.put("PARTNUM", strPartNumber);
          hmRepvalues = gmCOMRSRRptBean.fetchIncidentRAInfo(hmParam);
          if (hmRepvalues.size() > 0) {
            strComplRecBy = GmCommonClass.parseNull((String) hmRepvalues.get("COMPLRECVDBY"));
            strComplRecDt = GmCommonClass.parseNull((String) hmRepvalues.get("DTINCDATE"));
          }
          gmIncidentInfoForm.setComplRecvdBy(strComplRecBy);
          gmIncidentInfoForm.setComplRecvDt(strComplRecDt);
        }
        if (strOpt.equals("RALoad")) {
          gmIncidentInfoForm.onreset(actionMapping, request);
          gmIncidentInfoForm.setOriginator(strUser);
        } else {
          gmIncidentInfoForm.reset(actionMapping, request);
          gmIncidentInfoForm.setOriginator(strUser);
        }
        strForwardName = "gmIncidentInfo";
        gmIncidentInfoForm.setOriginator(strUser);

      }
      gmIncidentInfoForm.setRaID(strRaID);
      if (strOpt.equals("Save")) {

        strIncidentID = gmCOMRSRTransBean.saveIncidentDetails(hmParam);
        strSuccessMsg = "Data Saved Successfully";
        gmIncidentInfoForm.setComRsrID(strIncidentID);
        gmIncidentInfoForm.setStrMessage(strSuccessMsg);
        String strRedirectURL =
            "/gmCOMRSRSetupAction.do?method=editIncidentInfo&strOpt=Edit&comRsrID=" + strIncidentID
                + "&strMessage=" + strSuccessMsg;
        ActionRedirect actionRedirect =  actionRedirect(strRedirectURL, request);
        return actionRedirect;
      }
      strIssueType = GmCommonClass.parseNull(gmIncidentInfoForm.getIssueType());
      if (strIssueType.equals("102809")) {
        alLogReasons = gmCommonBean.getLog(strIncidentID, "4000319");
      } else if (strIssueType.equals("102810")) {
        alLogReasons = gmCommonBean.getLog(strIncidentID, "4000320");
      }
      gmIncidentInfoForm.setAlLogReasons(alLogReasons);
      if (strOpt.equals("ajax")) {
        strPartNumber = GmCommonClass.parseNull(gmIncidentInfoForm.getPartNum());
        strRepId = GmCommonClass.parseNull(gmIncidentInfoForm.getComplaintname());
        hmRepvalues = gmCOMRSRRptBean.fetchIncidentRAInfo(hmParam);
        strRepname = GmCommonClass.parseNull((String) hmRepvalues.get("REPNAME"));
        if (!strRepId.equals("")) {
          strAddress = GmCommonClass.parseNull((String) hmRepvalues.get("ADDRESS"));
          strPhone = GmCommonClass.parseNull((String) hmRepvalues.get("PHONE"));
          strAreaDirect = GmCommonClass.parseNull((String) hmRepvalues.get("AREADIRECT"));
          strCompany = GmCommonClass.parseNull((String) hmRepvalues.get("COMPANY"));
          strAjaxString = strAddress + "|" + strPhone + "|" + strAreaDirect + "|" + strCompany;
        }
        strRaID = GmCommonClass.parseNull((String) hmRepvalues.get("RAID"));
        if (!strRaID.equals("")) {
          strRepId = GmCommonClass.parseNull((String) hmRepvalues.get("REPID"));
          strQty = GmCommonClass.parseNull((String) hmRepvalues.get("QTY"));
          strLotNum = GmCommonClass.parseNull((String) hmRepvalues.get("LOTNUM"));
          strPart = GmCommonClass.parseNull((String) hmRepvalues.get("PARTNUMBER"));
          strPartDesc = GmCommonClass.parseNull((String) hmRepvalues.get("PARTDESC"));
          strProject = GmCommonClass.parseNull((String) hmRepvalues.get("PROJECTID"));
          strPartFamly = GmCommonClass.parseNull((String) hmRepvalues.get("PARTTYPE"));
          strAjaxString =
              strRaID + "|" + strRepId + "|" + strPart + "|" + strPartDesc + "|" + strProject + "|"
                  + strPartFamly + "|" + strQty + "|" + strLotNum;
          strPartNumber = "";
        }
        if (!strPartNumber.equals("")) {
          strPart = GmCommonClass.parseNull((String) hmRepvalues.get("PARTNUM"));
          strPartDesc = GmCommonClass.parseNull((String) hmRepvalues.get("PARTDESC"));
          strProject = GmCommonClass.parseNull((String) hmRepvalues.get("PROJECTID"));
          strPartFamly = GmCommonClass.parseNull((String) hmRepvalues.get("PARTTYPE"));
          if (strPart.equals("")) {
            strPart = strPartNumber;
          }
          strAjaxString = strPart + "|" + strPartDesc + "|" + strProject + "|" + strPartFamly;
        }
        response.setContentType("text/xml");
        PrintWriter pw = response.getWriter();
        if (!strAjaxString.equals("")) {
          pw.write(strAjaxString);
        }
        pw.flush();
        pw.close();
        return null;
      }
    } else {
      throw new AppError("You do not have enough permission to do this action", "", 'E');
    }
    return actionMapping.findForward(strForwardName);
  }

  /**
   * editIncidentInfo This method used for Editing/Updating the Incident Information.
   * 
   * @exception AppError
   * @throws IOException
   * @throws ServletException
   */
  public ActionForward editIncidentInfo(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException {
    log.debug("Enter into GmCOMRSRSetupAction in Action");
    instantiate(request, response);
    GmIncidentInfoForm gmIncidentInfoForm = (GmIncidentInfoForm) actionForm;
    gmIncidentInfoForm.loadSessionParameters(request);
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());
    GmCustomerBean gmCustomerBean = new GmCustomerBean();
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    HashMap hmRepvalues = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alLogReasons = new ArrayList();

    String strAction = GmCommonClass.parseNull(gmIncidentInfoForm.getHaction());
    String strForwardName = "gmIncidentInfo";
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strIcidentID = GmCommonClass.parseNull(gmIncidentInfoForm.getComRsrID().toUpperCase());
    String strOpt = GmCommonClass.parseNull(gmIncidentInfoForm.getStrOpt());
    String strIssueType = "";
    String strAccessFl = "";
    String strPartyId = GmCommonClass.parseNull(gmIncidentInfoForm.getSessPartyId());
    String strAddress = "";
    String strPhone = "";
    String strCompany = "";
    strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "COM_RSR_USER");
    if (strAction.equals("CMPHighlights")) {
      strForwardName = "CMPHighlights";
    }
    hmParam = GmCommonClass.getHashMapFromForm(gmIncidentInfoForm);
    if (strAccessFl.equals("Y")) {
      if (strOpt.equals("Fetch")) {
        alResult = gmCOMRSRRptBean.fetchIncidentDetails(hmParam);
        if (alResult.size() > 0)
          hmReturn = (HashMap) alResult.get(0);
        GmCommonClass.getFormFromHashMap(gmIncidentInfoForm, hmReturn);
      }
      if (strOpt.equals("Edit")) {
        gmIncidentInfoForm.setComRsrID(strIcidentID);
        hmParam = GmCommonClass.getHashMapFromForm(gmIncidentInfoForm);
        alResult = gmCOMRSRRptBean.fetchIncidentDetails(hmParam);
        if (alResult.size() > 0)
          hmReturn = (HashMap) alResult.get(0);

        String strComplaitID = GmCommonClass.parseNull((String) hmReturn.get("COMPLAINTNAME"));
        String strComplintType = GmCommonClass.parseNull((String) hmReturn.get("COMPLAINT"));

        gmIncidentInfoForm =
            (GmIncidentInfoForm) GmCommonClass.getFormFromHashMap(gmIncidentInfoForm, hmReturn);
      }
      loadIncidentInfo(gmIncidentInfoForm);
      strIssueType = GmCommonClass.parseNull(gmIncidentInfoForm.getIssueType());
      if (strIssueType.equals("102809")) {
        alLogReasons = gmCommonBean.getLog(strIcidentID, "4000319");
      } else if (strIssueType.equals("102810")) {
        alLogReasons = gmCommonBean.getLog(strIcidentID, "4000320");
      }
      gmIncidentInfoForm.setAlLogReasons(alLogReasons);
      if (alResult.size() > 0)
        gmIncidentInfoForm.setIntSize(alResult.size());
    } else {
      throw new AppError("You do not have enough permission to do this action", "", 'E');
    }
    return actionMapping.findForward(strForwardName);
  }

  /**
   * loadIncidentInfo This method used for loading all the dropdown values to the screen
   * 
   * @param Formbean object
   * @return
   * @exception AppError
   */
  public void loadIncidentInfo(GmIncidentInfoForm gmIncidentInfoForm) throws AppError {	  
	 
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmType = new HashMap();

    ArrayList alResult = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmIncidentInfoForm);
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());
    GmSalesBean gmSales = new GmSalesBean(getGmDataStoreVO());
    GmSearchCriteria gmsearchCriteria = new GmSearchCriteria();
    String status = "";
    ArrayList alIssueType = new ArrayList();
    ArrayList alComplaint = new ArrayList();
    ArrayList alAreaDirect = new ArrayList();
    ArrayList alIncidentCat = new ArrayList();
    ArrayList alComplRecvBy = new ArrayList();

    ArrayList alOriginator = new ArrayList();
    ArrayList alProjectList = new ArrayList();
    ArrayList alGemList = new ArrayList();
    ArrayList alTypeList = new ArrayList();

    ArrayList alRepList = new ArrayList();
    ArrayList alEmpList = new ArrayList();
    ArrayList alHospitalList = new ArrayList();
    ArrayList alComplRecvVia = new ArrayList();
    ArrayList alLoanorConsign = new ArrayList();
    ArrayList alChosseActionList = new ArrayList();
    ArrayList alCarrier = new ArrayList();//PC-3183
    ArrayList alprocedureOutcome = new ArrayList();//PC-3183
    ArrayList aladverseEffect = new ArrayList();//PC-3183
    ArrayList alDidThisEventHappenDuring = new ArrayList();//PC-3183
    HashMap hmSalesRep = new HashMap();

    HashMap hmSalesFilters = gmCommonBean.getSalesFilterLists("");
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    String strDeptId = "2004";
    alGemList = GmCommonClass.parseNullArrayList((gmCOMRSRRptBean.fetchUserList(strDeptId)));
    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301");
    alProjectList = gmProjectBean.reportProject(hmTemp);
    alIssueType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPITP"));
    alComplaint = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPCTP"));
    alAreaDirect = GmCommonClass.parseNullArrayList(gmCOMRSRRptBean.fetchADList(status));
    alIncidentCat = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPINC"));
    alComplRecvVia = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPVIA"));
    alChosseActionList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPCHA"));
    gmsearchCriteria.addSearchCriteria("CODEID", "102842", GmSearchCriteria.NOTEQUALS);
    gmsearchCriteria.addSearchCriteria("CODEID", "102847", GmSearchCriteria.NOTEQUALS);
    gmsearchCriteria.addSearchCriteria("CODEID", "102848", GmSearchCriteria.NOTEQUALS);
    alChosseActionList = (ArrayList) gmsearchCriteria.query(alChosseActionList);
    alHospitalList = GmCommonClass.parseNullArrayList(gmSales.reportAccount());
    alLoanorConsign = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPLRC"));
    alTypeList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PFLY"));
    alCarrier= GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DCAR"));//PC-3183
    alprocedureOutcome= GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PRCOUT"));//PC-3183
    aladverseEffect= GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("EVTADV"));//PC-3183
    alDidThisEventHappenDuring= GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("EVTDUR"));//PC-3183  
    
    String strActive = "";
    hmSalesRep.put("SALESREPLIST", "ALLSALESREP");
    hmSalesRep.put("SALESREPFILTER", strActive);
    alRepList = GmCommonClass.parseNullArrayList(gmCustomerBean.getSalesRepList(hmSalesRep));
    hmType.put("USERCODE", "300");
    hmType.put("INACTIVE_USER", "Y");
    hmType.put("EMPLOYEELIST", "ALLEMPLOYEE");
    alEmpList = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmType));

    gmIncidentInfoForm.setAlIssueType(alIssueType);
    gmIncidentInfoForm.setAlComplaint(alComplaint);
    gmIncidentInfoForm.setAlAreaDirect(alAreaDirect);
    gmIncidentInfoForm.setAlIncidentCat(alIncidentCat);
    gmIncidentInfoForm.setAlComplRecvBy(alEmpList);
    gmIncidentInfoForm.setAlComplRecvVia(alComplRecvVia);
    gmIncidentInfoForm.setAlOriginator(alEmpList);
    gmIncidentInfoForm.setAlProjectList(alProjectList);
    gmIncidentInfoForm.setAlGemList(alGemList);
    gmIncidentInfoForm.setAlTypeList(alTypeList);
    gmIncidentInfoForm.setAlLoanorConsign(alLoanorConsign);
    gmIncidentInfoForm.setAlChosseActionList(alChosseActionList);
    gmIncidentInfoForm.setAlHospitalList(alHospitalList);
    gmIncidentInfoForm.setAlRepList(alRepList);
    gmIncidentInfoForm.setAlEmpList(alEmpList);
    gmIncidentInfoForm.setAlGemList(alGemList);
    gmIncidentInfoForm.setAlCarrier(alCarrier);//PC-3183
    gmIncidentInfoForm.setAlprocedureOutcome(alprocedureOutcome);//PC-3183
    gmIncidentInfoForm.setAladverseEffect(aladverseEffect);//PC-3183
    gmIncidentInfoForm.setAlDidThisEventHappenDuring(alDidThisEventHappenDuring);//PC-3183
    
  } // End of loadIncidentInfo

  public ActionForward addQAEvaluation(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmQAEvaluationForm gmQAEvaluationForm = (GmQAEvaluationForm) actionForm;
    gmQAEvaluationForm.loadSessionParameters(request);
    GmCOMRSRTransBean gmCOMRSRTransBean = new GmCOMRSRTransBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    String strForwardName = "gmQAEvaluation";
    String strOpt = GmCommonClass.parseNull(gmQAEvaluationForm.getStrOpt());
    String strCOMRSRId = GmCommonClass.parseNull(gmQAEvaluationForm.getCOMRSRId().toUpperCase());
    String strPartyId = GmCommonClass.parseNull(gmQAEvaluationForm.getSessPartyId());
    String strRefType = "";
    String strLogType = "";
    String strAccessFl = "";
    String strMessage = "";
    String strToEmail = "";
    String strShGemNm = "";
    String strUserNm = "";

    ArrayList alRevCompBy = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alQARevCompBy = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmValues = new HashMap();
    HashMap hmEmailParams = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmType = new HashMap();
    strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "COM_RSR_USER"); // To get
                                                                                        // the
                                                                                        // access
                                                                                        // flag
    if (!strAccessFl.equals("Y")) { // Check whether the user is having access to the screen or not
      throw new AppError("You do not have enough permission to do this action", "", 'E');
    }
    hmParam = GmCommonClass.getHashMapFromForm(gmQAEvaluationForm);
    // hmAccess.put("GROUPNAME", "COM_RSR_USER");
    alRevCompBy =
        GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchgroupUser("COM_RSR_USER"));
    hmType.put("USERCODE", "300");
    hmType.put("INACTIVE_USER", "Y");
    alQARevCompBy = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmType));
    gmQAEvaluationForm.setAlRevCompBy(alRevCompBy);
    gmQAEvaluationForm.setAlQARevCompBy(alQARevCompBy);
    gmQAEvaluationForm.sethCOMRSRId(gmQAEvaluationForm.getCOMRSRId());

    if (strOpt.equals("save")) {
      strRefType = gmCOMRSRRptBean.validateID(strCOMRSRId, "102790"); // To validate the COM/RSR Id,
                                                                      // and return the refType
      hmParam.put("REFTYPE", strRefType);

      if (strRefType.equals("102791")) { // refType for RSR
        strLogType = "4000320";
      } else if (strRefType.equals("102790")) { // refType for COM
        strLogType = "4000319";
      }

      alLogReasons = gmCommonBean.getLog(strCOMRSRId, strLogType); // To get the comments
      gmQAEvaluationForm.setAlLogReasons(alLogReasons);

      hmValues = GmCommonClass.parseNullHashMap(gmCOMRSRTransBean.saveQAEvaluation(hmParam));
      strCOMRSRId = GmCommonClass.parseNull((String) hmValues.get("INCIDENTID"));
      if (!strCOMRSRId.equals("")) {
        strMessage = "Data Saved Successfully";
      }
      String strDateFl = GmCommonClass.parseNull((String) hmValues.get("DATEFLAG")); // if DateFl is
                                                                                     // Y, it will
                                                                                     // sent mail to
                                                                                     // evaluator

      if (strDateFl.equals("Y")) {
        alResult = gmCOMRSRRptBean.fetchIncidentDetails(hmParam);
        if (alResult.size() > 0)
          hmReturn = (HashMap) alResult.get(0);

        String strDueDt = GmCommonClass.parseNull((String) hmValues.get("DUEDATE"));
        strUserNm = GmCommonClass.parseNull((String) hmReturn.get("USERNAME"));
        strToEmail = GmCommonClass.parseNull((String) hmReturn.get("GEMMAILID"));
        strShGemNm = GmCommonClass.parseNull((String) hmReturn.get("GEMNM"));
        hmEmailParams.put("TOEMAIL", strToEmail);
        hmEmailParams.put("GEMNAME", strShGemNm);
        hmEmailParams.put("USERNM", strUserNm);
        hmEmailParams.put("COMRSRID", strCOMRSRId);
        hmEmailParams.put("DUEDATE", strDueDt);
        hmEmailParams.put("LOGTYPE", strLogType);
        hmEmailParams.put("USERID", strPartyId);
        gmCOMRSRRptBean.sendEvaluatorEmail(hmEmailParams); // To send mail to Evaluator
      }
     gmQAEvaluationForm.setStrOpt("Edit");
      String strRedirectURL ="/gmCOMRSRSetup.do?method=editQAEvaluation&strOpt=Edit&COMRSRId="
              + gmQAEvaluationForm.getCOMRSRId() + "&strMessage=" + strMessage;
      ActionRedirect actionRedirect = actionRedirect(strRedirectURL, request);
      return actionRedirect;
    }
    gmQAEvaluationForm.reset(actionMapping, request);

    gmQAEvaluationForm.setRevCompdBy(strPartyId);
    return actionMapping.findForward(strForwardName);
  }

  public ActionForward editQAEvaluation(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException {
    instantiate(request, response);
    GmQAEvaluationForm gmQAEvaluationForm = (GmQAEvaluationForm) actionForm;
    gmQAEvaluationForm.loadSessionParameters(request);
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    GmSearchCriteria gmsearchCriteria = new GmSearchCriteria();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

    String strForwardName = "gmQAEvaluation";
    String strOpt = GmCommonClass.parseNull(gmQAEvaluationForm.getStrOpt());
    String strCOMRSRId = GmCommonClass.parseNull(gmQAEvaluationForm.getCOMRSRId().toUpperCase());
    String strPartyId = GmCommonClass.parseNull(gmQAEvaluationForm.getSessPartyId());
    String strRefType = "";
    String strLogType = "";
    String strAccessFl = "";

    ArrayList alRevCompBy = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alChooseAction = new ArrayList();
    ArrayList alRemoveCodeIDs = new ArrayList();
    ArrayList alQARevCompBy = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmType = new HashMap();

    strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "COM_RSR_USER"); // To get
                                                                                        // the
                                                                                        // access
                                                                                        // flag
    if (!strAccessFl.equals("Y")) {// Check whether the user is having access to the screen or not
      throw new AppError("You do not have enough permission to do this action", "", 'E');
    }

    alChooseAction = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPCHA"));

    gmsearchCriteria.addSearchCriteria("CODEID", "102843", GmSearchCriteria.NOTEQUALS);// to remove
                                                                                       // "COM QA Evaluations"
                                                                                       // value from
                                                                                       // "Choose Option"
                                                                                       // drop down
    gmsearchCriteria.addSearchCriteria("CODEID", "102847", GmSearchCriteria.NOTEQUALS);// Reopen
                                                                                       // COM/RSR
    gmsearchCriteria.addSearchCriteria("CODEID", "102848", GmSearchCriteria.NOTEQUALS);// Modify/Report
                                                                                       // on COM/RSR
    alChooseAction =
        GmCommonClass.parseNullArrayList((ArrayList) gmsearchCriteria.query(alChooseAction));

    alRevCompBy =
        GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchgroupUser("COM_RSR_USER"));
    hmType.put("USERCODE", "300");
    hmType.put("INACTIVE_USER", "Y");
    alQARevCompBy = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmType));
    gmQAEvaluationForm.setAlRevCompBy(alRevCompBy);
    gmQAEvaluationForm.setAlQARevCompBy(alQARevCompBy);
    gmQAEvaluationForm.setAlChooseAction(alChooseAction);
    gmQAEvaluationForm.sethCOMRSRId(gmQAEvaluationForm.getCOMRSRId());

    if (strOpt.equals("Edit")) {
      strRefType = gmCOMRSRRptBean.validateID(strCOMRSRId, "102790"); // To validate the COM/RSR Id,
                                                                      // and return the refType

      if (strRefType.equals("102791")) { // refType for RSR
        strLogType = "4000320";
      } else if (strRefType.equals("102790")) { // refType for COM
        strLogType = "4000319";
      }

      alLogReasons = gmCommonBean.getLog(strCOMRSRId, strLogType);
      gmQAEvaluationForm.setAlLogReasons(alLogReasons);

      hmParam = GmCommonClass.parseNullHashMap(gmCOMRSRRptBean.fetchQAEvaluation(strCOMRSRId));
      if (hmParam.size() > 0) {
        hmParam.put("STRREFTYPE", strRefType);
        GmCommonClass.getFormFromHashMap(gmQAEvaluationForm, hmParam);
      } else {
        gmQAEvaluationForm.reset(actionMapping, request);

        gmQAEvaluationForm.setRevCompdBy(strPartyId);
      }

    }
    gmQAEvaluationForm.setResultSize(hmParam.size());
    return actionMapping.findForward(strForwardName);
  }

  public ActionForward comrsrHighlights(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, ServletException,
      IOException {
    log.debug("Enter into GmCOMRSRSetupAction in Action");
    instantiate(request, response);
    GmIncidentInfoForm gmIncidentInfoForm = (GmIncidentInfoForm) actionForm;
    gmIncidentInfoForm.loadSessionParameters(request);
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();

    HashMap hmReturn = new HashMap();
    HashMap hmParam = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alLogReasons = new ArrayList();

    String strForwardName = "CMPHighlights";

    String strIcidentID = GmCommonClass.parseNull(gmIncidentInfoForm.getComRsrID());
    String strOpt = GmCommonClass.parseNull(gmIncidentInfoForm.getStrOpt());
    String strIssueType = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmIncidentInfoForm);

    if (strOpt.equals("Fetch")) {
      alResult = gmCOMRSRRptBean.fetchIncidentDetails(hmParam);
      if (alResult.size() > 0)
        hmReturn = (HashMap) alResult.get(0);
      GmCommonClass.getFormFromHashMap(gmIncidentInfoForm, hmReturn);
    }

    strIssueType = GmCommonClass.parseNull(gmIncidentInfoForm.getIssueType());
    if (strIssueType.equals("102809")) { // COM
      alLogReasons = gmCommonBean.getLog(strIcidentID, "4000319");
    } else if (strIssueType.equals("102810")) {// RSR
      alLogReasons = gmCommonBean.getLog(strIcidentID, "4000320");
    }
    gmIncidentInfoForm.setAlLogReasons(alLogReasons);
    if (alResult.size() > 0)
      gmIncidentInfoForm.setIntSize(alResult.size());

    return actionMapping.findForward(strForwardName);
  }

  public ActionForward addEnggEvaluation(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    log.debug("Enter into addEnggEvaluation in Action");
    instantiate(request, response);
    GmEnggEvaluationForm gmEnggEvaluationForm = (GmEnggEvaluationForm) actionForm;
    gmEnggEvaluationForm.loadSessionParameters(request);

    GmCOMRSRTransBean gmCOMRSRTransBean = new GmCOMRSRTransBean(getGmDataStoreVO());
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    String strForwardName = "gmEnggEvaluation";
    String strRefType = "";
    String strLogType = "";
    String strMessage = "";

    String strPartyId = GmCommonClass.parseNull(gmEnggEvaluationForm.getSessPartyId());
    ArrayList alEmpList = new ArrayList();
    ArrayList alComDis = new ArrayList();
    ArrayList alProdDisp = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alRemoveCodeIDs = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmType = new HashMap();

    String strOpt = "";
    String strIncidentID = "";
    String strAccessFl = "";
    strOpt = GmCommonClass.parseNull(gmEnggEvaluationForm.getStrOpt());
    hmParam = GmCommonClass.getHashMapFromForm(gmEnggEvaluationForm);
    String strCOMRSRId =
        GmCommonClass.parseNull(gmEnggEvaluationForm.getIncidentId().toUpperCase());
    hmParam.put("INCIDENTID", strCOMRSRId);
    strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "COM_RSR_USER"); // To get
                                                                                        // the
                                                                                        // access
                                                                                        // flag
    if (!strAccessFl.equals("Y")) {// Check whether the user is having access to the screen or not
      throw new AppError("You do not have enough permission to do this action", "", 'E');
    }
    hmType.put("USERCODE", "300");
    hmType.put("INACTIVE_USER", "Y");
    alEmpList = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmType));
    gmEnggEvaluationForm.setAlAssignedTo(alEmpList);
    alComDis = GmCommonClass.getCodeList("CMPDIS");// CMPDIS ->Code Group for COM/RSR Disposition
    alProdDisp = GmCommonClass.getCodeList("CMPEPD");// CMPEPD ->Code Group for Product Disposition
    gmEnggEvaluationForm.setAlComRsrDis(alComDis);
    gmEnggEvaluationForm.setAlProdDisp(alProdDisp);
    if (strOpt.equals("Save")) {
      strRefType = gmCOMRSRRptBean.validateID(strCOMRSRId, "102790");// This is to validate whether
                                                                     // it is COM/RSR id.
      hmParam.put("REFTYPE", strRefType);
      strIncidentID = gmCOMRSRTransBean.saveEnggEvaluation(hmParam);
      if (strRefType.equals("102791")) { // 102791-> code id for RSR
        strLogType = "4000320"; // 4000320 -> code id for RSRLOG.
      } else if (strRefType.equals("102790")) { // 102790 -> code id for COM
        strLogType = "4000319"; // 4000319 -> code id for COMLOG
      }
      alLogReasons = gmCommonBean.getLog(strCOMRSRId, strLogType);
      gmEnggEvaluationForm.setAlLogReasons(alLogReasons);
      if (!strIncidentID.equals("")) {
        strMessage = "Data Saved Successfully";
      }
      String strRedirectURL =
          "/gmEnggEvaluation.do?method=editEnggEvaluation&incidentId=" + strIncidentID
              + "&strOpt=Edit&message=" + strMessage;
      ActionRedirect actionRedirect =  actionRedirect(strRedirectURL, request);
      return actionRedirect;
    }
    gmEnggEvaluationForm.reset(actionMapping, request);
    return actionMapping.findForward(strForwardName);
  }

  public ActionForward editEnggEvaluation(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    log.debug("Enter into editEnggEvaluation in Action");
    instantiate(request, response);
    GmEnggEvaluationForm gmEnggEvaluationForm = (GmEnggEvaluationForm) actionForm;
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());
    gmEnggEvaluationForm.loadSessionParameters(request);
    GmLogonBean gmLogon = new GmLogonBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmSearchCriteria gmsearchCriteria = new GmSearchCriteria();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean(getGmDataStoreVO());
    String strForwardName = "gmEnggEvaluation";

    String strRefType = "";
    String strLogType = "";
    String strCOMRSRDisp = "";
    String strPartyId = GmCommonClass.parseNull(gmEnggEvaluationForm.getSessPartyId());
    String strAccessFl = "";
    ArrayList alEmpList = new ArrayList();
    ArrayList alComDis = new ArrayList();
    ArrayList alProdDisp = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alchoose = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmType = new HashMap();
    strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "COM_RSR_USER"); // To get
                                                                                        // the
                                                                                        // access
                                                                                        // flag
    if (!strAccessFl.equals("Y")) {// Check whether the user is having access to the screen or not
      throw new AppError("You do not have enough permission to do this action", "", 'E');
    }
    hmType.put("USERCODE", "300");
    hmType.put("INACTIVE_USER", "Y");
    alEmpList = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmType));
    gmEnggEvaluationForm.setAlAssignedTo(alEmpList);
    alComDis = GmCommonClass.getCodeList("CMPDIS");// CMPDIS ->Code Group for COM/RSR Disposition
    alProdDisp = GmCommonClass.getCodeList("CMPEPD");// CMPEPD ->Code Group for Product Disposition
    gmEnggEvaluationForm.setAlComRsrDis(alComDis);
    gmEnggEvaluationForm.setAlProdDisp(alProdDisp);
    hmParam = GmCommonClass.getHashMapFromForm(gmEnggEvaluationForm);
    String strCOMRSRId =
        GmCommonClass.parseNull(gmEnggEvaluationForm.getIncidentId().toUpperCase());
    hmParam.put("INCIDENTID", strCOMRSRId);
    alchoose = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPCHA")); // For Choose
                                                                                      // Action
    gmEnggEvaluationForm.setAlChooseAction(alchoose);
    strCOMRSRDisp = GmCommonClass.parseNull((String) hmResult.get("COMRSRDISPNUM"));
    String strOpt = GmCommonClass.parseNull(gmEnggEvaluationForm.getStrOpt());
    strRefType = gmCOMRSRRptBean.validateID(strCOMRSRId, "102790");// This is to validate whether it
                                                                   // is COM/RSR id.
    hmReturn.put("REFTYPE", strRefType);
    if (strOpt.equals("Edit")) {
      alResult = gmCOMRSRRptBean.fetchEnggEvaluation(hmParam);
      gmEnggEvaluationForm.setIntSize(alResult.size());
      if (strRefType.equals("102791")) { // 102791-> code id for RSR
        strLogType = "4000320"; // 4000320 -> code id for RSRLOG.
      } else if (strRefType.equals("102790")) { // 102790 -> code id for COM
        strLogType = "4000319"; // 4000319 -> code id for COMLOG
      }
      if (strRefType.equals("102790")) {// In COM/RSR Disposition drop down if the COM/RSR id is COM
                                        // it will hide Normal Wear and Tear.
        gmsearchCriteria.addSearchCriteria("CODEID", "102855", GmSearchCriteria.NOTEQUALS);// 102855->code
                                                                                           // id for
                                                                                           // Normal
                                                                                           // Wear
                                                                                           // and
                                                                                           // Tear
                                                                                           // in
                                                                                           // drop
                                                                                           // down
        alComDis = (ArrayList) gmsearchCriteria.query(alComDis);
      } else if (strRefType.equals("102791")) {// In COM/RSR Disposition drop down if the COM/RSR id
                                               // is RSR it will show Normal Wear and Tear.
        gmsearchCriteria.addSearchCriteria("CODEID", "102852", GmSearchCriteria.NOTEQUALS);// 102852->code
                                                                                           // id for
                                                                                           // Valid
                                                                                           // in
                                                                                           // drop
                                                                                           // down
        gmsearchCriteria.addSearchCriteria("CODEID", "102853", GmSearchCriteria.NOTEQUALS);// 102853->code
                                                                                           // id for
                                                                                           // InValid
                                                                                           // in
                                                                                           // drop
                                                                                           // down
        gmsearchCriteria.addSearchCriteria("CODEID", "102854", GmSearchCriteria.NOTEQUALS);// 102854->code
                                                                                           // id for
                                                                                           // InValid
                                                                                           // in
                                                                                           // drop
                                                                                           // down
        alComDis = (ArrayList) gmsearchCriteria.query(alComDis);
      }
      alLogReasons = gmCommonBean.getLog(strCOMRSRId, strLogType);
      gmEnggEvaluationForm.setAlLogReasons(alLogReasons);
      if (alResult.size() > 0) {
        hmResult = (HashMap) alResult.get(0);
        strCOMRSRDisp = GmCommonClass.parseNull((String) hmResult.get("COMRSRDISPNUM"));
        GmCommonClass.getFormFromHashMap(gmEnggEvaluationForm, hmResult);
      } else if (strRefType.equals("102791") && !(hmResult.size() > 0)) {// To Set Default value as
                                                                         // Normal Wear and Tear
        if (strCOMRSRDisp.equals("")) {
          strCOMRSRDisp = "102855";
          gmEnggEvaluationForm.reset(actionMapping, request);
          gmEnggEvaluationForm.setComRsrDispNum(strCOMRSRDisp);

        }
      } else if (strRefType.equals("102790") && !(hmResult.size() > 0)) {
        gmEnggEvaluationForm.reset(actionMapping, request);
      }
      gmEnggEvaluationForm.setIncidentId(strCOMRSRId);
      gmEnggEvaluationForm.setStrRefType(strRefType);
    }

    return actionMapping.findForward(strForwardName);
  }

  public ActionForward printCOMRSR(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmIncidentInfoForm gmIncidentInfoForm = (GmIncidentInfoForm) actionForm;
    gmIncidentInfoForm.loadSessionParameters(request);
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();

    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmCOMRSRPrintInfo = new HashMap();
    HashMap hmIncidentValues = new HashMap();
    HashMap hmRepvalues = new HashMap();
    HashMap hmQAData = new HashMap();
    HashMap hmEnggValues = new HashMap();

    ArrayList alIncidentDetails = new ArrayList();
    ArrayList alEnggDetails = new ArrayList();
    
    //PMT-53042 Jasper changes
    String strSessCompanyLocale = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    GmResourceBundleBean gmResourceBundleBean =GmCommonClass.getResourceBundleBean("properties.labels.quality.complaint.GmQAEvaluation",strSessCompanyLocale);    
    String strOldJasperFileName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMOLDJASPERFILE"));
    String strNewJasperFileName = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("GMNEWJASPERFILE"));
    String strDateFormat = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("DATEFORMAT"));
    String strOldCompRecvDate = GmCommonClass.parseNull(gmResourceBundleBean.getProperty("OLD_COMP_RECV_DATE"));

    String strJasperName = "/GmPrintComplaint.jasper";
    String strOpt = GmCommonClass.parseNull(gmIncidentInfoForm.getStrOpt());
    String strComplNumber = "";
    String strOriginator = "";
    String strIncidentDt = "";
    String strReciveDt = "";
    String strComplRecvBy = "";
    String strComplRecvVia = "";
    String strComplName = "";
    String strComplAddr = "";
    String strComplPhone = "";
    String strProdRecvDt = "";
    String strRMANumber = "";
    String strPartNumber = "";
    String strLotNumber = "";
    String strPartDesc = "";
    String strProject = "";
    String strMDRReport = "";
    String strComplDesc = "";
    String strDHRSerchRev = "";
    String strReviewedBy = "";
    String strReviewedDt = "";
    String strDtlEvalResult = "";
    String strEvalPrintName = "";
    String strEvalRecDate = "";
    String strEvalCrecDt = "";
    String strComplDisposition = "";
    String strProdDisposition = "";
    String strCorrPrevnAction = "";
    String strComplaintType = "";
    String strComplaintId = "";
    String strCAPASCARRev = "";
    String strComplReview = "";
    String strDHRDeviation = "";
    String strPropStore = "";
    String strRepSurgQues = "";
    String strUtilPerPackage = "";
    String strRiskAnalysis = "";
    String strNCMRSCARGen = "";
    String strNCMRiD = "";
    String strCPRAction = "";
    String strCAPAId = "";
    String strQARevwCompl = "";
    String strQARevwComplDt = "";
    String strRemovalReq = "";
    String strCompany = "";
    String strMEDDEVReport = "";
    String strMDRReportDt = "";
    String strPartRecvDt = "";
    String strMDRId = "";
    String strMedDevId = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmIncidentInfoForm);
    hmResult = gmCOMRSRRptBean.fetchPrintComplaint(hmParam);
    if (hmResult.size() > 0) {
      alIncidentDetails = (ArrayList) hmResult.get("INCIDENTDETA");
      if (alIncidentDetails.size() > 0)
        hmIncidentValues = (HashMap) alIncidentDetails.get(0);
      if (hmIncidentValues.size() > 0) {
        strComplNumber = GmCommonClass.parseNull(gmIncidentInfoForm.getComRsrID());
        strOriginator = GmCommonClass.parseNull((String) hmIncidentValues.get("ORIGNNAME"));
        strIncidentDt = GmCommonClass.parseNull((String) hmIncidentValues.get("DTINCDATE"));
        strReciveDt = GmCommonClass.parseNull((String) hmIncidentValues.get("COMPLRECVDT"));
        strComplRecvBy = GmCommonClass.parseNull((String) hmIncidentValues.get("COMPLRECVDBYNAME"));
        strComplRecvVia = GmCommonClass.parseNull((String) hmIncidentValues.get("COMPLRECVVIA"));
        strComplName = GmCommonClass.parseNull((String) hmIncidentValues.get("COMPLAINTFULLNAME"));
        strComplaintType = GmCommonClass.parseNull((String) hmIncidentValues.get("COMPLAINT"));
        strComplaintId = GmCommonClass.parseNull((String) hmIncidentValues.get("COMPLAINTNAME"));
        strComplAddr = GmCommonClass.parseNull((String) hmIncidentValues.get("ADDRESS"));
        strComplPhone = GmCommonClass.parseNull((String) hmIncidentValues.get("PHONENUMBER"));
        strCompany = GmCommonClass.parseNull((String) hmIncidentValues.get("COMPANY"));

        strProdRecvDt = strReciveDt;
        strRMANumber = GmCommonClass.parseNull((String) hmIncidentValues.get("RAID"));
        strPartNumber = GmCommonClass.parseNull((String) hmIncidentValues.get("PARTNUM"));
        strLotNumber = GmCommonClass.parseNull((String) hmIncidentValues.get("LOTNUM"));
        strPartDesc = GmCommonClass.parseNull((String) hmIncidentValues.get("PARTDESC"));
        strProject = GmCommonClass.parseNull((String) hmIncidentValues.get("PROJDESC"));
        strMDRReport = GmCommonClass.parseNull((String) hmIncidentValues.get("MDRREPORT"));
        strMEDDEVReport = GmCommonClass.parseNull((String) hmIncidentValues.get("MEDDEVREPORT"));
        strMDRReportDt = GmCommonClass.parseNull((String) hmIncidentValues.get("MDRREPORTDT"));
        strComplDesc = GmCommonClass.parseNull((String) hmIncidentValues.get("DETAILDESCEVENT"));
        strPartRecvDt = GmCommonClass.parseNull((String) hmIncidentValues.get("PARTRECVDT"));
        strMDRId = GmCommonClass.parseNull((String) hmIncidentValues.get("MDRID"));
        strMedDevId = GmCommonClass.parseNull((String) hmIncidentValues.get("MEDDEVID"));
        
        
        //PMT-53042 Jasper changes
        if (strReciveDt != null) {
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat); 
        Date dtComplReciveDt = dateFormat.parse(strReciveDt);
        Date dtstrOldCompRecvDate = dateFormat.parse(strOldCompRecvDate);
        
        if (dtComplReciveDt.compareTo(dtstrOldCompRecvDate) > 0) {
           strJasperName = strNewJasperFileName;   //New jasper if the complaint is received after 03/17/2020
        } else{ 
        	strJasperName = strOldJasperFileName;
           } 
        }
      }

      hmQAData = (HashMap) hmResult.get("QADATA");
      if (hmQAData.size() > 0){
        strDHRDeviation = GmCommonClass.parseNull((String) hmQAData.get("DHRDEV"));
        strPropStore = GmCommonClass.parseNull((String) hmQAData.get("STORAGE"));
        strRepSurgQues = GmCommonClass.parseNull((String) hmQAData.get("REPSURGANS"));
        strUtilPerPackage = GmCommonClass.parseNull((String) hmQAData.get("UTILIZEDPAC"));
        strRiskAnalysis = GmCommonClass.parseNull((String) hmQAData.get("RISKANALYSIS"));
        strNCMRSCARGen = GmCommonClass.parseNull((String) hmQAData.get("NCMRSCARGEN"));
        strNCMRiD = GmCommonClass.parseNull((String) hmQAData.get("NCMRSCARID"));
        strCPRAction = GmCommonClass.parseNull((String) hmQAData.get("CORRECTIVEACT"));
        strCAPAId = GmCommonClass.parseNull((String) hmQAData.get("CAPAID"));
        strQARevwCompl = GmCommonClass.parseNull((String) hmQAData.get("QAREVCOMPBYNM"));
        strQARevwComplDt = GmCommonClass.parseNull((String) hmQAData.get("DTCOMPLETED"));
        strRemovalReq = GmCommonClass.parseNull((String) hmQAData.get("REMOVALREQ"));
        strDHRSerchRev = GmCommonClass.parseNull((String) hmQAData.get("DHRNCMRREV"));
        strReviewedBy = GmCommonClass.parseNull((String) hmQAData.get("REVCOMPDBYNM"));
        strReviewedDt = GmCommonClass.parseNull((String) hmQAData.get("DTREVCOMP"));
        strCAPASCARRev = GmCommonClass.parseNull((String) hmQAData.get("CAPASCARREV"));
        strComplReview = GmCommonClass.parseNull((String) hmQAData.get("COMPREV"));
      }

      alEnggDetails = (ArrayList) hmResult.get("ENGGDATA");
      if (alEnggDetails.size() > 0)
        hmEnggValues = (HashMap) alEnggDetails.get(0);
      if (hmEnggValues.size() > 0) {
        strDtlEvalResult = GmCommonClass.parseNull((String) hmEnggValues.get("EVALRESULT"));
        strEvalPrintName = GmCommonClass.parseNull((String) hmEnggValues.get("EVALNAME"));
        strEvalRecDate = GmCommonClass.parseNull((String) hmEnggValues.get("EVALRECDT"));
        strEvalCrecDt = GmCommonClass.parseNull((String) hmEnggValues.get("CREATEDDT"));
        strComplDisposition = GmCommonClass.parseNull((String) hmEnggValues.get("COMRSRDISPNUM"));
        strProdDisposition = GmCommonClass.parseNull((String) hmEnggValues.get("PDTDISPTYPNUM"));
        strCorrPrevnAction = GmCommonClass.parseNull((String) hmEnggValues.get("CAPACOMMENTS"));
      }

      hmCOMRSRPrintInfo.put("COMPLNUMBER", strComplNumber);
      hmCOMRSRPrintInfo.put("ORIGNNAME", strOriginator);
      hmCOMRSRPrintInfo.put("INCIDENTDATE", strIncidentDt);
      hmCOMRSRPrintInfo.put("RECEIVEDDATE", strReciveDt);
      hmCOMRSRPrintInfo.put("COMPLRECVBY", strComplRecvBy);
      hmCOMRSRPrintInfo.put("COMPLRECVIA", strComplRecvVia);
      hmCOMRSRPrintInfo.put("COMPLAINTNAME", strComplName);
      hmCOMRSRPrintInfo.put("COMPLAINTADDR", strComplAddr);
      hmCOMRSRPrintInfo.put("COMPLAINTPH", strComplPhone);
      hmCOMRSRPrintInfo.put("PRODRECVDT", strProdRecvDt);
      hmCOMRSRPrintInfo.put("RAID", strRMANumber);
      hmCOMRSRPrintInfo.put("PARTNUM", strPartNumber);
      hmCOMRSRPrintInfo.put("LOTNUM", strLotNumber);
      hmCOMRSRPrintInfo.put("PARTDESC", strPartDesc);
      hmCOMRSRPrintInfo.put("PROJDESC", strProject);
      hmCOMRSRPrintInfo.put("MDRREPORT", strMDRReport);
      hmCOMRSRPrintInfo.put("MEDDEVREPORT", strMEDDEVReport);
      hmCOMRSRPrintInfo.put("MDRREPORTDT", strMDRReportDt);
      hmCOMRSRPrintInfo.put("COMPLDESC", strComplDesc);
      hmCOMRSRPrintInfo.put("DHRNCMRREV", strDHRSerchRev);
      hmCOMRSRPrintInfo.put("REVCOMPDBYNM", "");
      hmCOMRSRPrintInfo.put("DTREVCOMP", "");
      hmCOMRSRPrintInfo.put("EVALRESULT", strDtlEvalResult);
      hmCOMRSRPrintInfo.put("EVALNAME", strEvalPrintName);
      hmCOMRSRPrintInfo.put("EVALRECDT", strEvalRecDate);
      hmCOMRSRPrintInfo.put("CREATEDDT", strEvalCrecDt);
      hmCOMRSRPrintInfo.put("COMRSRDISPNUM", strComplDisposition);
      hmCOMRSRPrintInfo.put("PDTDISPTYPNUM", strProdDisposition);
      hmCOMRSRPrintInfo.put("CAPACOMMENTS", strCorrPrevnAction);
      hmCOMRSRPrintInfo.put("CAPASCARREV", strCAPASCARRev);
      hmCOMRSRPrintInfo.put("DHRDEV", strDHRDeviation);
      hmCOMRSRPrintInfo.put("STORAGE", strPropStore);
      hmCOMRSRPrintInfo.put("REPSURGANS", strRepSurgQues);
      hmCOMRSRPrintInfo.put("UTILIZEDPAC", strUtilPerPackage);
      hmCOMRSRPrintInfo.put("RISKANALYSIS", strRiskAnalysis);
      hmCOMRSRPrintInfo.put("NCMRSCARGEN", strNCMRSCARGen);
      hmCOMRSRPrintInfo.put("NCMRSCARID", "");
      hmCOMRSRPrintInfo.put("CORRECTIVEACT", strCPRAction);
      hmCOMRSRPrintInfo.put("CAPAID", "");
      hmCOMRSRPrintInfo.put("QAREVCOMPBYNM", "");
      hmCOMRSRPrintInfo.put("DTCOMPLETED", "");
      hmCOMRSRPrintInfo.put("REMOVALREQ", strRemovalReq);
      hmCOMRSRPrintInfo.put("COMPREV", strComplReview);
      hmCOMRSRPrintInfo.put("PARTRECVDT", strPartRecvDt);
      hmCOMRSRPrintInfo.put("MDRID", strMDRId);
      hmCOMRSRPrintInfo.put("MEDDEVID", strMedDevId);
      

      // Please Refer BUG-2982 to Change The Print Document Dynamically
      GmJasperReport gmJasperReport = new GmJasperReport();
      gmJasperReport.setRequest(request);
      gmJasperReport.setResponse(response);
      gmJasperReport.setJasperReportName(strJasperName);
      gmJasperReport.setHmReportParameters(hmCOMRSRPrintInfo);
      gmJasperReport.setReportDataList(null);
      gmJasperReport.createJasperPdfReport();
      return null;
    } else {
      return null;
    }
  }
}
