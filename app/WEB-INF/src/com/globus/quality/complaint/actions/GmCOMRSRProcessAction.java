package com.globus.quality.complaint.actions;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCalenderOperations;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmCommonUploadBean;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.logon.beans.GmLogonBean;
import com.globus.quality.capa.beans.GmCapaTransBean;
import com.globus.quality.complaint.beans.GmCOMRSRRptBean;
import com.globus.quality.complaint.beans.GmCOMRSRTransBean;
import com.globus.quality.complaint.forms.GmCOMRSRProcessForm;
import com.globus.quality.scar.beans.GmScarSetupBean;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmCOMRSRProcessAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward comrsrSummary(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    HttpSession session = request.getSession(false);

    GmCOMRSRProcessForm gmCOMRSRProcessForm = (GmCOMRSRProcessForm) actionForm;
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean();
    gmCOMRSRProcessForm.loadSessionParameters(request);
    GmCommonUploadBean gmCommonUploadBean = new GmCommonUploadBean();

    String strForward = "gmCOMRSRSummary";
    String strFileName = "";
    String strCOMRSRId = GmCommonClass.parseNull(gmCOMRSRProcessForm.getComRsrID()); // Getting
                                                                                     // COMRSR ID to
                                                                                     // strCOMRSRId
    String strApplnDateFmt =
        GmCommonClass.parseNull((String) session.getAttribute("strSessApplDateFmt"));
    String strOpt = GmCommonClass.parseNull(gmCOMRSRProcessForm.getStrOpt());
    String strRefType = GmCommonClass.parseNull(gmCOMRSRProcessForm.getRefType());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    ArrayList alGeneralDtls = new ArrayList();
    ArrayList alEngEvalDtls = new ArrayList();
    ArrayList alClosureDtls = new ArrayList();
    ArrayList alDetails = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmGeneralDtls = new HashMap();
    HashMap hmEngEvalDtls = new HashMap();
    HashMap hmClosureDtls = new HashMap();

    hmParam = GmCommonClass.getHashMapFromForm(gmCOMRSRProcessForm);
    hmParam.put("STRCOMRSRID", strCOMRSRId);
    hmParam.put("INCIDENTID", strCOMRSRId);
    log.debug("hmParam -->" + hmParam);

    alGeneralDtls = GmCommonClass.parseNullArrayList(gmCOMRSRRptBean.fetchIncidentDetails(hmParam)); // To
                                                                                                     // get
                                                                                                     // the
                                                                                                     // General
                                                                                                     // Details
                                                                                                     // and
                                                                                                     // Part
                                                                                                     // Details
    alEngEvalDtls = GmCommonClass.parseNullArrayList(gmCOMRSRRptBean.fetchEnggEvaluation(hmParam)); // To
                                                                                                    // get
                                                                                                    // the
                                                                                                    // Engineering
                                                                                                    // Evaluation
                                                                                                    // details
    alClosureDtls = GmCommonClass.parseNullArrayList(gmCOMRSRRptBean.fetchCloseComplaint(hmParam)); // To
                                                                                                    // get
                                                                                                    // the
                                                                                                    // closure
                                                                                                    // details

    if (alGeneralDtls.size() > 0)
      hmGeneralDtls = (HashMap) alGeneralDtls.get(0);
    if (alEngEvalDtls.size() > 0)
      hmEngEvalDtls = (HashMap) alEngEvalDtls.get(0);
    if (alClosureDtls.size() > 0)
      hmClosureDtls = (HashMap) alClosureDtls.get(0);

    // Below codes are used to set the uploaded files details to grid
    strRefType = gmCommonUploadBean.validateUploadID(strCOMRSRId, strRefType); // To validate the
                                                                               // upload id and to
                                                                               // get the refType
    alDetails = gmCommonUploadBean.fetchUploadInfo(strRefType, strCOMRSRId); // To get the Upload
                                                                             // Information
    hmParam.put("STROPT", strOpt);
    hmParam.put("APPLNDATEFMT", strApplnDateFmt);
    hmParam.put("SUBDIR", "quality/complaint/templates");
    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    hmParam.put("TEMPLATENAME", "GmCMPUpload.vm");
    String strXmlData = generateOutPut(alDetails, hmParam);

    gmCOMRSRProcessForm.setIntRecSize(alDetails.size());
    gmCOMRSRProcessForm.setStrXmlData(strXmlData);
    hmParam.put("FILENAME", strFileName);
    hmParam.put("STRREFID", strCOMRSRId);
    hmParam.put("REFTYPE", strRefType);
    hmParam.put("STRXMLDATA", strXmlData);

    // Below codes are used to set all the values to form
    GmCommonClass.getFormFromHashMap(gmCOMRSRProcessForm, hmParam);
    GmCommonClass.getFormFromHashMap(gmCOMRSRProcessForm, hmGeneralDtls);
    GmCommonClass.getFormFromHashMap(gmCOMRSRProcessForm, hmEngEvalDtls);
    GmCommonClass.getFormFromHashMap(gmCOMRSRProcessForm, hmClosureDtls);

    return actionMapping.findForward(strForward);
  }

  private String generateOutPut(ArrayList alGridData, HashMap hmParam) throws Exception {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSubDir = GmCommonClass.parseNull((String) hmParam.get("SUBDIR"));
    String strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATENAME"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataList("alGridData", alGridData);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.quality.complaint.GmCMPUpload", strSessCompanyLocale));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir(strSubDir);
    templateUtil.setTemplateName(strTemplateName);
    return templateUtil.generateOutput();
  }

  /*
   * Method Name : closureEmail Method Purpose : Svaing and Fetching the Closure EmailDetails for
   * Complaint/CAPA/SCAR
   */
  public ActionForward closureEmail(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    log.debug("Enter into GmCOMRSRProcessAction in Action");
    GmCOMRSRProcessForm gmCOMRSRProcessForm = (GmCOMRSRProcessForm) actionForm;
    gmCOMRSRProcessForm.loadSessionParameters(request);
    response.setContentType("text/html");
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();
    GmCapaTransBean gmCapaTransBean = new GmCapaTransBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmCOMRSRTransBean gmCOMRSRTransBean = new GmCOMRSRTransBean();
    GmScarSetupBean gmScarSetupBean = new GmScarSetupBean();

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmParamValues = new HashMap();
    HashMap hmUserDetails = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmEmailValues = new HashMap();

    ArrayList alResult = new ArrayList();
    ArrayList alUserDataList = new ArrayList();
    ArrayList alEvaluationResult = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmCOMRSRProcessForm);
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strUserID = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    String strAccessFl = "";
    String strRefID = "";
    String strRefType = "";
    String strToEmail = "";
    String strCcEmail = "";
    String strEmailSubject = "";
    String strEmailContent = "";
    String strContentHead = "";
    String strContentFooter = "";
    String strUserName = "";
    String strUserExtn = "";
    String strPartNumber = "";
    String strLotNumber = "";
    String strComplaintNm = "";
    String strRaID = "";
    String strDescEvent = "";
    String strSectionA = "";
    String strSectionB = "";
    String strSectionC = "";
    String strSectionAOne = "";
    String strSectionATwo = "";
    String sreEnggEvalResult = "";
    String strRuleIDOne = "";
    String strRuleIDTwo = "";
    String strRuleIDThree = "";
    String strRulegrpID = "";
    String strLogType = "";
    String strUserDesig = "";
    String strLogMessage = "";

    /* This Option is for showing Closure Details Email in a stadard format */
    if (strOpt.equals("preview")) {
      strUserName = GmCommonClass.parseNull(gmCOMRSRProcessForm.getUserName());
      strEmailContent = GmCommonClass.parseNull(gmCOMRSRProcessForm.getEmailCommnets());
      strToEmail = GmCommonClass.parseNull(gmCOMRSRProcessForm.getToEmail());
      strEmailContent = strEmailContent.replaceAll("<STRONG>", "<B>");
      strEmailContent = strEmailContent.replaceAll("</STRONG>", "</B>");
      hmParam.put("EMAILCOMMNETS", strEmailContent);
      hmParam.put("TOEMAIL", strToEmail);
      gmCOMRSRTransBean.saveClosureEmail(hmParam);
      hmResult = gmCOMRSRRptBean.fetchClosureEmail(hmParam);
      if (hmResult.size() > 0) {
        alResult = (ArrayList) hmResult.get("EMAILDETAILS");
        // strComplaintNm = GmCommonClass.parseNull((String)hmResult.get("COMPLAINTNM"));
        alUserDataList = (ArrayList) hmResult.get("USERDETAILS");
        if (alUserDataList.size() > 0)
          hmUserDetails = (HashMap) alUserDataList.get(0);
        strUserName = GmCommonClass.parseNull((String) hmUserDetails.get("USERNAME"));
        strUserExtn = GmCommonClass.parseNull((String) hmUserDetails.get("USEREXTN"));
        strUserDesig = GmCommonClass.parseNull((String) hmUserDetails.get("USERDESIGNATION"));

        if (alResult.size() > 0) {
          hmReturn = (HashMap) alResult.get(0);
          strEmailContent = GmCommonClass.parseNull((String) hmReturn.get("EMAILCOMMNETS"));
          strEmailContent = "<br>" + strEmailContent + "<br><br><br>";
          hmReturn.put("EMAILCOMMNETS", strEmailContent);
          hmReturn.put("ISSUEBY", strComplaintNm);
          hmReturn.put("USERNAME", strUserName);
          hmReturn.put("USERDESIGNATION", strUserDesig);
          hmReturn.put("IMAGE_PATH", getJasperImageURL());
          gmCOMRSRProcessForm =
              (GmCOMRSRProcessForm) GmCommonClass.getFormFromHashMap(gmCOMRSRProcessForm, hmReturn);
        }
      }
    }

    /* This Option is for loading the Closure Email details based on the Ref id */
    if (strOpt.equals("Load")) {
      strRefID = GmCommonClass.parseNull(gmCOMRSRProcessForm.getRefID());
      strRefType = GmCommonClass.parseNull(gmCOMRSRProcessForm.getRefType());

      String strOptionType = "closureEmail";
      hmParamValues.put("REFID", strRefID);
      hmParamValues.put("REFTYPE", strRefType);
      hmParamValues.put("ISSUETYPE", strRefType);
      hmParamValues.put("STROPT", strOptionType);
      hmParamValues.put("USERID", strUserID);

      hmResult = gmCOMRSRRptBean.fetchClosureEmail(hmParamValues);
      alResult = (ArrayList) hmResult.get("EMAILDETAILS");
      alUserDataList = (ArrayList) hmResult.get("USERDETAILS");
      // strComplaintNm = GmCommonClass.parseNull((String)hmResult.get("COMPLAINTNM"));
      if (alUserDataList.size() > 0)
        hmUserDetails = (HashMap) alUserDataList.get(0);
      strUserName = GmCommonClass.parseNull((String) hmUserDetails.get("USERNAME"));
      strUserExtn = GmCommonClass.parseNull((String) hmUserDetails.get("USEREXTN"));
      gmCOMRSRProcessForm.setUserName(strUserName);
      gmCOMRSRProcessForm.setIssueBy(strComplaintNm);

      /*
       * If already Email is sended to this Ref ID we are loading those email properties values else
       * based on the ref type we are setting the Email Properties to the form
       */
      
      // PC-4223- to set new email comments, in existing it took from email log table (102790-COM) / (102791-RSR)
      if((alResult.size() > 0) && (!strRefType.equals("102790") && !strRefType.equals("102791"))) {
        hmReturn = (HashMap) alResult.get(0);
        strEmailContent = GmCommonClass.parseNull((String) hmReturn.get("EMAILCOMMNETS"));
        // strEmailContent = strEmailContent.replaceAll("<br>", "\n");
        hmReturn.put("EMAILCOMMNETS", strEmailContent);
        gmCOMRSRProcessForm =
            (GmCOMRSRProcessForm) GmCommonClass.getFormFromHashMap(gmCOMRSRProcessForm, hmReturn);
      } else {
        if (strRefType.equals("102790") || strRefType.equals("102791")) { // COMRSR CLOSURE EMAIL
                                                                          // PART
          hmParam.put("COMRSRID", GmCommonClass.parseNull(gmCOMRSRProcessForm.getRefID()));
          hmParam.put("STRCOMRSRID", GmCommonClass.parseNull(gmCOMRSRProcessForm.getRefID()));
          hmParam.put("INCIDENTID", GmCommonClass.parseNull(gmCOMRSRProcessForm.getRefID()));
          alResult = gmCOMRSRRptBean.fetchIncidentDetails(hmParam);
          alEvaluationResult = gmCOMRSRRptBean.fetchEnggEvaluation(hmParam);
          if (alEvaluationResult.size() > 0)
            hmResult = (HashMap) alEvaluationResult.get(0);
          sreEnggEvalResult = GmCommonClass.parseNull((String) hmResult.get("EVALRESULT"));
          if (alResult.size() > 0)
            hmReturn = (HashMap) alResult.get(0);
          strToEmail = GmCommonClass.parseNull((String) hmReturn.get("EMAILID"));
          strPartNumber = GmCommonClass.parseNull((String) hmReturn.get("PARTNUM"));
          strLotNumber = GmCommonClass.parseNull((String) hmReturn.get("LOTNUM"));
          strRaID = GmCommonClass.parseNull((String) hmReturn.get("RAID"));
          strDescEvent = GmCommonClass.parseNull((String) hmReturn.get("DETAILDESCEVENT"));
          strComplaintNm = GmCommonClass.parseNull((String) hmReturn.get("COMPLAINTFIRSTNAME"));
          strCcEmail =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue("COMRSR_CC",
                  "COMRSR_CLOSURE_EMAIL"));
          strEmailSubject =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue("COMRSR_SUBJECT",
                  "COMRSR_CLOSURE_EMAIL"));

          if (strRaID.equals("") || strRaID.equals("null")) {
            strRaID = "N/A";
          }
          strEmailSubject = strEmailSubject + " for " + strRaID;
          strSectionA =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue("COMRSR_SECTION_A",
                  "COMRSR_CLOSURE_EMAIL"));
          strSectionA =
              strSectionA
                  .replaceAll("<<PART>>", (strPartNumber.equals("") ? "N/A" : strPartNumber));
          strSectionA =
              strSectionA.replaceAll("<<LOTNUMBRE>>", (strLotNumber.equals("") ? "N/A"
                  : strLotNumber));
          strSectionA =
              strSectionA.replaceAll("<<DESCEVET>>", (strDescEvent.equals("") ? "N/A"
                  : strDescEvent));
          strSectionB =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue("COMRSR_SECTION_B",
                  "COMRSR_CLOSURE_EMAIL"));
          strSectionB =
              strComplaintNm + ", " + strSectionB.replaceAll("<<EVALRESULT>>", sreEnggEvalResult);
          strSectionC =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue("COMRSR_SECTION_C",
                  "COMRSR_CLOSURE_EMAIL"));
          strSectionC = strSectionC + strUserExtn;
          strComplaintNm = "Dear " + strComplaintNm + ",";
          strEmailContent =
              strComplaintNm + "<br><br>" + strSectionA + "<br><br>" + strSectionB + "<br><br>"
                  + strSectionC;
          strComplaintNm = "";

          gmCOMRSRProcessForm.setToEmail(strToEmail);
          gmCOMRSRProcessForm.setCcEmail(strCcEmail);
          gmCOMRSRProcessForm.setEmailSubject(strEmailSubject);
          gmCOMRSRProcessForm.setEmailCommnets(strEmailContent);
          gmCOMRSRProcessForm.setRefID(strRefID);
          gmCOMRSRProcessForm.setIssueBy(strComplaintNm);
          gmCOMRSRProcessForm.setImage_path(getJasperImageURL());

        } else if ((strRefType.equals("102788")) || (strRefType.equals("102789"))) { // CAPA/SCAR
                                                                                     // CLOSURE
                                                                                     // EMAIL PART
          if (strRefType.equals("102788")) {
            strRuleIDOne = "CPAPA_CC";
            strRuleIDTwo = "CPAPA_SUBJECT";
            strRuleIDThree = "CPAPA_CONTENT";
            strRulegrpID = "CAPA_CLOSURE_EMAIL";
          } else if (strRefType.equals("102789")) {
            strRuleIDOne = "SCAR_CC";
            strRuleIDTwo = "SCAR_SUBJECT";
            strRuleIDThree = "SCAR_CONTENT";
            strRulegrpID = "SCAR_CLOSURE_EMAIL";
          }
          hmReturn = gmCapaTransBean.fetchCapa(hmParamValues);
          strToEmail = GmCommonClass.parseNull((String) hmReturn.get("TOEMAIL"));
          if (!strToEmail.equals("") || !strToEmail.equals("null")) {
            String[] arryEmailIDs = strToEmail.split(",");
            HashSet setEmails = new HashSet();
            Collections.addAll(setEmails, arryEmailIDs);
            String strSetEmails = setEmails.toString();
            strToEmail = strSetEmails.substring(1, strSetEmails.length() - 1);
          }
          strCcEmail =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRuleIDOne, strRulegrpID));
          strEmailSubject =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRuleIDTwo, strRulegrpID));
          strEmailContent =
              GmCommonClass.parseNull(GmCommonClass.getRuleValue(strRuleIDThree, strRulegrpID));
          strContentHead = "Hello,";
          strContentFooter = "Thank you,";
          strEmailContent =
              strContentHead + "<br><br>" + strEmailContent + "<br><br><br>" + strContentFooter
                  + "<br>" + strUserName;
          if (strRefType.equals("102788")) {
            gmCOMRSRProcessForm.setToEmail(strToEmail);
          }
          gmCOMRSRProcessForm.setCcEmail(strCcEmail);
          gmCOMRSRProcessForm.setEmailSubject(strRefID);
          gmCOMRSRProcessForm.setEmailCommnets(strEmailContent);
          gmCOMRSRProcessForm.setRefID(strRefID);
        }
      }
    }

    /* This Option is for sending the Finalized Closure Details Email to the Rep */
    if (strOpt.equals("email")) {
      strRefType = GmCommonClass.parseNull(gmCOMRSRProcessForm.getRefType());
      strRefID = GmCommonClass.parseNull(gmCOMRSRProcessForm.getRefID());
      hmParam.put("REFID", gmCOMRSRProcessForm.getRefID());
      hmParam.put("REFTYPE", gmCOMRSRProcessForm.getRefType());
      HashMap hmEmailPerams = new HashMap();
      hmResult = gmCOMRSRRptBean.fetchClosureEmail(hmParam);
      if (hmResult.size() > 0) {
        alResult = (ArrayList) hmResult.get("EMAILDETAILS");
        alUserDataList = (ArrayList) hmResult.get("USERDETAILS");
        if (alResult.size() > 0)
          hmEmailValues = (HashMap) alResult.get(0);
        strComplaintNm = GmCommonClass.parseNull((String) hmResult.get("COMPLAINTNM"));
        if (alUserDataList.size() > 0)
          hmUserDetails = (HashMap) alUserDataList.get(0);
        strUserName = GmCommonClass.parseNull((String) hmUserDetails.get("USERNAME"));
        strUserDesig = GmCommonClass.parseNull((String) hmUserDetails.get("USERDESIGNATION"));
        // strComplaintNm = "Dear "+ strComplaintNm;
        strComplaintNm = "";
        hmEmailValues.put("ISSUEBY", strComplaintNm);
        hmEmailValues.put("USERNAME", strUserName);
        strEmailContent = GmCommonClass.parseNull((String) hmEmailValues.get("EMAILCOMMNETS"));
        strEmailContent = "<br>" + strEmailContent + "<br><br><br>";
        hmEmailValues.put("EMAILCOMMNETS", strEmailContent);
        hmEmailValues.put("USERDESIGNATION", strUserDesig);
        hmEmailValues.put("IMAGE_PATH", getJasperImageURL());
        gmCOMRSRProcessForm =
            (GmCOMRSRProcessForm) GmCommonClass.getFormFromHashMap(gmCOMRSRProcessForm,
                hmEmailValues);
        hmEmailPerams
            .put("TOEMAIL", GmCommonClass.parseNull((String) hmEmailValues.get("TOEMAIL")));
        hmEmailPerams
            .put("CCEMAIL", GmCommonClass.parseNull((String) hmEmailValues.get("CCEMAIL")));
        hmEmailPerams.put("EMAILSUBJECT",
            GmCommonClass.parseNull((String) hmEmailValues.get("EMAILSUBJECT")));
        hmEmailPerams.put("EMAILCOMMNETS",
            GmCommonClass.parseNull((String) hmEmailValues.get("EMAILCOMMNETS")));
        hmEmailPerams.put("REFTYPE", strRefType);
        hmEmailPerams.put("COMPLAINTNM", strComplaintNm);
        hmEmailPerams.put("USERNM", strUserName);
        hmEmailPerams.put("USERDESIG", strUserDesig);
        hmEmailPerams.put("IMAGE_PATH", getJasperImageURL());
        gmCOMRSRRptBean.sendClosureEmail(hmEmailPerams);

        if (strRefType.equals("102790")) { // FOR COM TYPE
          strLogType = "4000319";
          strLogMessage = "Closure E-mail Sent to Sales Rep";
        } else if (strRefType.equals("102791")) { // FOR RSR TYPE
          strLogType = "4000320";
          strLogMessage = "Closure E-mail Sent to Sales Rep";
        } else if (strRefType.equals("102788")) { // FOR CAPA TYPE
          strLogType = "4000317";
          strLogMessage = "Closure E-mail Sent";
        } else if (strRefType.equals("102789")) { // FOR SCAR TYPE
          strLogType = "4000318";
          strLogMessage = "Closure E-mail Sent";
        }
        gmCommonBean.saveLog(strRefID, strLogMessage, strUserID, strLogType);
      }
    }
    return actionMapping.findForward("gmCMPClosureEmail");
  }

  public ActionForward closeCOMRSR(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    log.debug("Enter into GmCOMRSRProcessAction in Action closeCOMRSR method");

    GmCOMRSRProcessForm gmCOMRSRProcessForm = (GmCOMRSRProcessForm) actionForm;
    gmCOMRSRProcessForm.loadSessionParameters(request);
    GmCOMRSRTransBean gmCOMRSRTransBean = new GmCOMRSRTransBean();
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean();
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmLogonBean gmLogonBean = new GmLogonBean();
    GmCalenderOperations gmCalenderOperations = new GmCalenderOperations();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

    String strComments = "";
    String strLogType = "";
    String strMessage = "";
    String strCOMRSRID = "";
    String strAccessFl = "";
    String strOpt = GmCommonClass.parseNull(gmCOMRSRProcessForm.getStrOpt());
    String strPartyId = GmCommonClass.parseNull(gmCOMRSRProcessForm.getSessPartyId());
    HashMap hmParam = new HashMap();
    HashMap hmDetails = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmRef = new HashMap();
    HashMap hmAccess = new HashMap();

    ArrayList alEmpList = new ArrayList();
    ArrayList alReturnList = new ArrayList();
    ArrayList alLog = new ArrayList();
    ArrayList alLogReasons = new ArrayList();
    ArrayList alchoose = new ArrayList();
    ArrayList alReturn = new ArrayList();
    String strForward = "gmCOMRSRClosure";

    strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "COM_RSR_USER"); // To get
                                                                                        // the
                                                                                        // access
                                                                                        // flag

    if (!strAccessFl.equals("Y")) { // Check whether the user is having access to the screen or not
      throw new AppError("You do not have enough permission to do this action", "", 'E');
    }
    hmParam = GmCommonClass.getHashMapFromForm(gmCOMRSRProcessForm);

    strCOMRSRID = GmCommonClass.parseNull(gmCOMRSRProcessForm.getCloseCOMRSRID().toUpperCase());
    hmParam.put("CLOSECOMRSRID", strCOMRSRID);
    String strSessTodayDate = GmCommonClass.parseNull((String) hmParam.get("strSessTodaysDate"));
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    alEmpList =
        GmCommonClass.parseNullArrayList(gmAccessControlBean.fetchgroupUser("COM_RSR_USER"));

    gmCOMRSRProcessForm.setAlEmployeeList(alEmpList);
    String strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));
    strSessTodayDate = gmCalenderOperations.getCurrentDate(strApplDateFmt);
    if (strOpt.equals("load")) {
      gmCOMRSRProcessForm.setClosedBy(strUserId);
      gmCOMRSRProcessForm.setClosedDt(strSessTodayDate);
      gmCOMRSRProcessForm.onreset(actionMapping, request);
    } else {
      strCOMRSRID = GmCommonClass.parseNull((String) hmParam.get("CLOSECOMRSRID"));
      String strRefType = gmCOMRSRRptBean.validateID(strCOMRSRID, "102790");// This is to validate
                                                                            // whether it is COM/RSR
                                                                            // id.
      if (strRefType.equals("102791")) { // 102791-> code id for RSR
        strLogType = "4000320"; // 4000320 -> code id for RSRLOG.
      } else if (strRefType.equals("102790")) { // 102790 -> code id for COM
        strLogType = "4000319"; // 4000319 -> code id for COMLOG
      }
      hmParam.put("LOGTYPE", strLogType);
    }
    if (strOpt.equals("save")) {
      strMessage = gmCOMRSRTransBean.saveCloseCOMRSR(hmParam);
      strOpt = "Fetch";
      gmCOMRSRProcessForm.setStrMessage(strMessage);
    }
    if (strOpt.equals("Fetch")) {
      log.debug("Enter into fetch part");
      gmCOMRSRProcessForm.setStrOpt("Fetch");
      alchoose = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPCHA")); // For
                                                                                        // Choose
                                                                                        // Action
      gmCOMRSRProcessForm.setAlChooseActionList(alchoose);
      alReturnList = gmCOMRSRRptBean.fetchCloseCOMRSR(hmParam);
      alLogReasons = gmCommonBean.getLog(strCOMRSRID, strLogType);
      gmCOMRSRProcessForm.setAlLogReasons(alLogReasons);
      if (alReturnList.size() > 0) {
        hmResult = (HashMap) alReturnList.get(0);
        GmCommonClass.getFormFromHashMap(gmCOMRSRProcessForm, hmResult);
      //below setCloseCmts changes - PC-4223-COM/RSR Closure Email Comments Update
        hmParam.put("INCIDENTID", strCOMRSRID);
        alReturn = gmCOMRSRRptBean.fetchEnggEvaluation(hmParam);
        if (alReturn.size() > 0) {
        	hmReturn = (HashMap) alReturn.get(0);
        	gmCOMRSRProcessForm.setCloseCmts((String) hmReturn.get("EVALRESULT"));
        }
      } else {

        gmCOMRSRProcessForm.setClosedBy(strUserId);
        gmCOMRSRProcessForm.setClosedDt(strSessTodayDate);
        hmParam.put("INCIDENTID", strCOMRSRID);
        alReturn = gmCOMRSRRptBean.fetchEnggEvaluation(hmParam);
        gmCOMRSRProcessForm.onreset(actionMapping, request);
        if (alReturn.size() > 0) {
          hmReturn = (HashMap) alReturn.get(0);
          gmCOMRSRProcessForm.setCloseCmts((String) hmReturn.get("EVALRESULT"));
        }
      }
      gmCOMRSRProcessForm.setIntSize(alReturnList.size());

    }
    return actionMapping.findForward(strForward);

  }
  
  private String getJasperImageURL() throws AppError, Exception {
	    //PMT-33439-Apache & PHP Changes on ERP server (3rd Set)
	    //InetAddress iadd = InetAddress.getLocalHost();
	    String ipAddress = System.getProperty("HOST_URL");
	    log.debug("ipAddress"+ipAddress);
	    String systemName = ipAddress.toString().toLowerCase();//PMT-33439
	    //Load the Image path from the Constant properties
	    String strJasperURL = GmCommonClass.contextURL;

	    if (systemName.indexOf("vmstage") != -1) {
	      strJasperURL = GmCommonClass.contextURL;
	    } else if (systemName.indexOf("vmpreprod") != -1) {
	      strJasperURL = GmCommonClass.contextURL;
	    } else if (systemName.indexOf("vmtest") != -1) {
	      strJasperURL = GmCommonClass.contextURL;
	    } else {
	      strJasperURL = GmCommonClass.contextURL;
	    }
	    return strJasperURL;
	  }
}
