package com.globus.quality.complaint.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.custservice.beans.GmCustomerBean;
import com.globus.logon.beans.GmLogonBean;
import com.globus.quality.complaint.beans.GmCOMRSRRptBean;
import com.globus.quality.complaint.forms.GmCOMRSRReportForm;
import com.globus.sales.event.beans.GmEventSetupBean;

public class GmCOMRSRReportAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward pendingCOMRSRDash(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmCOMRSRReportForm gmCOMRSRReportForm = (GmCOMRSRReportForm) actionForm;
    gmCOMRSRReportForm.loadSessionParameters(request);
    GmCustomerBean gmCustomerBean = new GmCustomerBean();
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();

    ArrayList alResult = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmCOMRSRReportForm);
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strAccessFl = "";
    String strPartyId = GmCommonClass.parseNull(gmCOMRSRReportForm.getSessPartyId());
    strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "COM_RSR_USER");
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    if (strAccessFl.equals("Y")) {
      if (strOpt.equals("report")) {
        alResult = gmCOMRSRRptBean.fetchPendCOMRSRDash();
        hmReturn.put("PENDCOMRSRDATA", alResult);
        hmReturn.put("APPLNDATEFMT", strApplDateFmt);
        hmReturn.put("gmDataStoreVO", getGmDataStoreVO());
        hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
        hmReturn.put("VMFILEPATH", "properties.labels.quality.complaint.GmPendingCOMRSRDash");
        hmReturn.put("FILENAME", "GmPendingCOMRSRDash.vm");
      }
      String strxmlGridData = generateOutPut(hmReturn);
      gmCOMRSRReportForm.setXmlGridData(strxmlGridData);

    } else {
      throw new AppError("You do not have enough permission to do this action", "", 'E');
    }
    return actionMapping.findForward("gmPendCOMRSRReport");
  }

  public ActionForward listCOMRSRDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmCOMRSRReportForm gmCOMRSRRptForm = (GmCOMRSRReportForm) actionForm;
    gmCOMRSRRptForm.loadSessionParameters(request);
    GmCustomerBean gmCustomerBean = new GmCustomerBean(getGmDataStoreVO());
    GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean(getGmDataStoreVO());
    GmLogonBean gmLogon = new GmLogonBean();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();

    String strXmlGridData = "";
    String strOpt = GmCommonClass.parseNull(gmCOMRSRRptForm.getStrOpt());
    String strPartyId = "";
    String strAccessFl =  GmCommonClass.parseNull(gmCOMRSRRptForm.getAccessFl());
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    ArrayList alComplaint = new ArrayList();
    ArrayList alRepList = new ArrayList();
    ArrayList alElapsedDaysList = new ArrayList();
    ArrayList alCOMRSRList = new ArrayList();
    ArrayList alStatusList = new ArrayList();
    ArrayList alEmpName = new ArrayList();
    ArrayList alChooseActionList = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alRemoveCodeIDs = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();
    HashMap hmType = new HashMap();
    HashMap hmSalesRep = new HashMap();
    /*
     * strPartyId = GmCommonClass.parseNull((String) gmCOMRSRRptForm.getSessPartyId());// Get the
     * user from session strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP",
     * "COM_RSR_USER");// to get the accessibility of user if(!strAccessFl.equals("Y")){// Checking
     * whether user is having access or not throw new
     * AppError("You do not have enough permission to do this action", "", 'E'); }
     */
   
    String strFilter = "";
    hmParam = GmCommonClass.getHashMapFromForm(gmCOMRSRRptForm);
    hmType.put("EMPLOYEELIST", "ALLEMPLOYEE");
    hmType.put("USERCODE", "300");
    hmSalesRep.put("SALESREPLIST", "ALLSALESREP");
    hmSalesRep.put("SALESREPFILTER", strFilter);
    alRepList = GmCommonClass.parseNullArrayList(gmCustomerBean.getSalesRepList(hmSalesRep));
    alElapsedDaysList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CPRSN"));
    alCOMRSRList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPITP"));
    alStatusList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPSTS"));
    alEmpName = GmCommonClass.parseNullArrayList(gmLogon.getEmployeeList(hmType));
    alChooseActionList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPCHA"));
    alComplaint = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CMPCTP"));

    alRemoveCodeIDs.add("4000325"); // to remove "Modify/Generate Report" from "Choose Option" drop
                                    // down
    alChooseActionList =
        GmCommonClass.parseNullArrayList(GmCommonClass.toRemoveCodeIdsFromArrayList(
            alRemoveCodeIDs, alChooseActionList));
    gmCOMRSRRptForm.setAlComplaint(alComplaint);
    gmCOMRSRRptForm.setAlRepList(alRepList);
    gmCOMRSRRptForm.setAlElapsedDays(alElapsedDaysList);
    gmCOMRSRRptForm.setAlChooseActionList(alChooseActionList);
    gmCOMRSRRptForm.setAlCOMRSRList(alCOMRSRList);
    gmCOMRSRRptForm.setAlStatusList(alStatusList);
    gmCOMRSRRptForm.setAlEmpName(alEmpName);

    if (strOpt.equals("Report") || strOpt.equals("load")) { // To fetch the records
      alResult = GmCommonClass.parseNullArrayList(gmCOMRSRRptBean.fetchCOMRSRDetails(hmParam));
      hmResult.put("ALRESULT", alResult);
      hmResult.put("strAccessFl",strAccessFl);
      hmResult.put("gmDataStoreVO", getGmDataStoreVO());
      hmResult.put("FILENAME", "GmCOMRSRReport.vm");
      hmResult.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmResult.put("VMFILEPATH", "properties.labels.quality.complaint.GmCOMRSRReport");
      strXmlGridData = generateOutPut(hmResult);
      gmCOMRSRRptForm.setXmlGridData(strXmlGridData);
      gmCOMRSRRptForm.setStrOpt("Report");
      gmCOMRSRRptForm.setIntResultSize(alResult.size());
    }

    return actionMapping.findForward("gmCOMRSRReport");
  }

  /**
   * generateOutPut xml content generation for Pending COM/RSR Report grid
   * 
   * @param HashMap
   * @return String
   */
  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strFileName = GmCommonClass.parseNull((String) hmParam.get("FILENAME"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    String strVmPropPath = GmCommonClass.parseNull((String) hmParam.get("VMFILEPATH"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(strVmPropPath,
        strSessCompanyLocale));
    templateUtil.setTemplateSubDir("quality/complaint/templates");
    templateUtil.setTemplateName(strFileName);
    return templateUtil.generateOutput();
  }
}
