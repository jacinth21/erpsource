/**
 * FileName : GmSetBundleRptAction.java Description : Author : Agilan Singaravelan
 */
package com.globus.quality.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.quality.beans.GmSetBundleMappingBean;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.quality.forms.GmSetBundleRptForm;
import com.globus.valueobject.common.GmDataStoreVO;


public class GmSetBundleRptAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * This method used to load the set bundle report.
   * @param actionMapping
   * @param form , request response
   * @throws AppError , Exception
   */
  public ActionForward setBundleRpt(ActionMapping actionMapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmSetBundleMappingBean gmSetBundleMappingBean = new GmSetBundleMappingBean(getGmDataStoreVO());
    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
    GmSetBundleRptForm gmSetBundleRptForm = (GmSetBundleRptForm) form;
    gmSetBundleRptForm.loadSessionParameters(request);
    
    String strOpt = "";
    String strGridXmlData = "";
    ArrayList alSetBundleName = new ArrayList();
    ArrayList alSetId = new ArrayList();
    ArrayList alDivList = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alRptDetails = new ArrayList();
    HashMap hmParam = new HashMap();
    String strStatus = "";
   
    String strSessCompanyLocale =GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    hmParam = GmCommonClass.getHashMapFromForm(gmSetBundleRptForm);
    strOpt = GmCommonClass.parseNull((String) request.getParameter("hStrOpt"));
    alSetBundleName = GmCommonClass.parseNullArrayList(gmSetBundleMappingBean.fetchSetBundleNameDtls());
    alDivList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));
    alStatus=(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SBSTS")));
    alSetId = GmCommonClass.parseNullArrayList(gmSetBundleMappingBean.fetchSetID());
    strStatus = "107021";      //Default value to set Active-->107021
    
    
    if(strOpt.equals("Load")){ 
    alRptDetails = GmCommonClass.parseNullArrayList(gmSetBundleMappingBean.fetchSetBundleDtlsToDash(hmParam));
    strStatus = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
    hmParam.put("REPORT", alRptDetails);
    hmParam.put("gmDataStoreVO", getGmDataStoreVO());
    hmParam.put("STRSESSCOMPANYLOCALE",strSessCompanyLocale);
    strGridXmlData = getXmlGridData(hmParam);
    request.setAttribute("GRIDXMLDATA",strGridXmlData);
    request.setAttribute("ALRPTDETAILS",alRptDetails);
    }
    
    gmSetBundleRptForm.setStatus(strStatus);
    gmSetBundleRptForm.setAlSetBundleName(alSetBundleName);
    gmSetBundleRptForm.setAlDivList(alDivList);
    gmSetBundleRptForm.setAlStatus(alStatus);
    gmSetBundleRptForm.setAlSetId(alSetId);
    gmSetBundleRptForm.setAlRptDetails(alRptDetails);	
   
    return actionMapping.findForward("gmSetBundleRpt");
  }
  /**
   * This method used to generate grid report.
   * @param hmParam
   * @throws AppError
   */
  public String getXmlGridData(HashMap hmParam) throws AppError {
	GmTemplateUtil templateUtil = new GmTemplateUtil();
	String strSessCompanyLocale = GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
	templateUtil.setDataList("alReturn", (ArrayList) hmParam.get("REPORT"));
	templateUtil.setDataMap("hmParam", hmParam);
	templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean("properties.labels.quality.GmSetBundleReport", strSessCompanyLocale));
	templateUtil.setTemplateName("GmSetBundleRpt.vm");
	templateUtil.setTemplateSubDir("quality/templates");
	
	return templateUtil.generateOutput();
 }
}
