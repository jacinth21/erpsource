package com.globus.quality.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmPartNumBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.quality.beans.GmIssuingAgencyConfigBean;
import com.globus.quality.forms.GmPartParameterDtlsForm;

public class GmPartParameterDtlsAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * dhrRequirementDtls This method used to fetch/save the part DHR Req. details.
   * 
   * @exception AppError
   */
  public ActionForward dhrRequirementDtls(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartParameterDtlsForm gmPartParameterDtlsForm = (GmPartParameterDtlsForm) actionForm;
    gmPartParameterDtlsForm.loadSessionParameters(request);
    GmPartNumBean gmPartNumBean = new GmPartNumBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();

    String strForwardName = "gmPartDhrRequirementDtls";
    String strOpt = "";
    String strPartNumber = "";
    String strPartyId = "";
    String strUpdateAccess = "";

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();

    ArrayList alLogReasons = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmPartParameterDtlsForm);
    loadPartDropdownValues(gmPartParameterDtlsForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    strPartyId = GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID"));
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PARTNUM_SUBMIT_ACC"));
    strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmPartParameterDtlsForm.setButtonAccess(strUpdateAccess);

    if (strOpt.equals("Save")) {
      log.debug("before update the DHR Req. details");
      gmPartNumBean.saveDhrRequirementDtls(hmParam);
    }
    alLogReasons = gmCommonBean.getLog(strPartNumber, "1291"); // 1291-->Part Number -DHR Req.
                                                               // Comments
    gmPartParameterDtlsForm.setAlLogReasons(alLogReasons);
    hmReturn = gmPartNumBean.fetchPartDhrRequirementDtls(hmParam);
    GmCommonClass.getFormFromHashMap(gmPartParameterDtlsForm, hmReturn);
    gmPartParameterDtlsForm.setHmAttrVal(hmReturn);
    log.debug(" Redirect to " + strForwardName);
    return actionMapping.findForward(strForwardName);
  }

  /**
   * loadPartDropdownValues This method used to load the all drop down values.
   * 
   * @param Form
   * @exception AppError
   */
  public void loadPartDropdownValues(GmPartParameterDtlsForm gmPartParameterDtlsForm)
      throws AppError {
    GmIssuingAgencyConfigBean gmIssuingAgencyConfigBean = new GmIssuingAgencyConfigBean();
    ArrayList alPackaging = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PACKM"));
    ArrayList alCertificate = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CERTR"));
    ArrayList alWOCreated = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("WOCFL"));
    ArrayList alUDIEtchRequired =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("UDIER"));
    ArrayList alDIOnlyEtchRequired =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("DIOER"));
    ArrayList alPIOnlyEtchRequired =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PIOER"));
    ArrayList alVendorDesign =
        GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("VENDGN"));
    ArrayList alIssuingAgencyConf =
        GmCommonClass.parseNullArrayList(gmIssuingAgencyConfigBean.fetchConfigName());
    ArrayList alBulkPackage = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CERTR"));

    // to set the array list values to form
    gmPartParameterDtlsForm.setAlMaterialCert(alCertificate);
    gmPartParameterDtlsForm.setAlHandnessCert(alCertificate);
    gmPartParameterDtlsForm.setAlPrimaryPack(alPackaging);
    gmPartParameterDtlsForm.setAlSecondaryPack(alPackaging);
    gmPartParameterDtlsForm.setAlComplianceCert(alCertificate);
    gmPartParameterDtlsForm.setAlIssuingAgencyConf(alIssuingAgencyConf);
    gmPartParameterDtlsForm.setAlWoCreated(alWOCreated);
    gmPartParameterDtlsForm.setAlUDIEtchRequired(alUDIEtchRequired);
    gmPartParameterDtlsForm.setAlDIOnlyEtchRequired(alDIOnlyEtchRequired);
    gmPartParameterDtlsForm.setAlPIOnlyEtchRequired(alPIOnlyEtchRequired);
    gmPartParameterDtlsForm.setAlVendorDesign(alVendorDesign);
    gmPartParameterDtlsForm.setAlBulkPackage(alBulkPackage);
  }

  /**
   * udiParameterDtls This method used to fetch/save the part UDI Parameter details.
   * 
   * @exception AppError
   */
  public ActionForward udiParameterDtls(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartParameterDtlsForm gmPartParameterDtlsForm = (GmPartParameterDtlsForm) actionForm;
    gmPartParameterDtlsForm.loadSessionParameters(request);
    GmPartNumBean gmPartNumBean = new GmPartNumBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    String strForwardName = "gmPartNumUDIParameter";
    String strOpt = "";
    String strPartNumber = "";
    String strUserId = "";
    String strInputStr = "";
    String strComments = "";
    String strPartyId = "";
    String strUpdateAccess = "";

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmUDIParam = new HashMap();
    HashMap hmSaveUDIParm = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alMRISaftyInfo = new ArrayList();
    ArrayList alList = new ArrayList();
    ArrayList alCompanyNm = new ArrayList();
    ArrayList alLogReasons = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmPartParameterDtlsForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    strComments = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    alMRISaftyInfo = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("MRISI"));
    alCompanyNm = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("COMP"));
    alList = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("CERTR"));
    // submit button access
    strPartyId = GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID"));
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PARTNUM_SUBMIT_ACC"));
    strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmPartParameterDtlsForm.setButtonAccess(strUpdateAccess);

    if (strOpt.equals("Save")) {
      log.debug("save the UDI parameter details");
      hmSaveUDIParm.put("PARTNUMBER", strPartNumber);
      hmSaveUDIParm.put("HLABELATTRIBUTE", strInputStr);
      hmSaveUDIParm.put("USERID", strUserId);
      gmPartNumBean.savePartNumParams(hmSaveUDIParm);
      // to save the DUNS information.
      gmPartNumBean.saveDUNSInformation(hmParam);
      if (!strComments.equals("")) {
        gmCommonBean.saveLog(strPartNumber, strComments, strUserId, "1292");
      }
    }
    hmUDIParam = gmPartNumBean.fetchUDIParameterDtls(strPartNumber);

    gmPartParameterDtlsForm.setAlMriSafetyInfo(alMRISaftyInfo);
    gmPartParameterDtlsForm.setAlLotOrBatchNum(alList);
    gmPartParameterDtlsForm.setAlManufaturingDt(alList);
    gmPartParameterDtlsForm.setAlSerialNum(alList);
    gmPartParameterDtlsForm.setAlExpirationDt(alList);
    gmPartParameterDtlsForm.setAlDonationIdenNum(alList);
    gmPartParameterDtlsForm.setAlOverrideCompanyNm(alCompanyNm);

    gmPartParameterDtlsForm.setHmAttrVal(hmUDIParam);

    alLogReasons = gmCommonBean.getLog(strPartNumber, "1292"); // 1292-->Part Number -UDI Parameters
                                                               // Comments
    gmPartParameterDtlsForm.setAlLogReasons(alLogReasons);
    GmCommonClass.getFormFromHashMap(gmPartParameterDtlsForm, hmUDIParam);
    log.debug(" Redirect to " + strForwardName);
    return actionMapping.findForward(strForwardName);
  }

  /**
   * udiRegulatorySetup This method used to fetch/save the part UDI Parameter details.
   * 
   * @exception AppError
   */
  public ActionForward udiRegulatorySetup(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartParameterDtlsForm gmPartParameterDtlsForm = (GmPartParameterDtlsForm) actionForm;
    gmPartParameterDtlsForm.loadSessionParameters(request);
    GmPartNumBean gmPartNumBean = new GmPartNumBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean();
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    String strForwardName = "gmPartRegulatorySetup";
    String strOpt = "";
    String strPartNumber = "";
    String strUserId = "";
    String strInputStr = "";
    String strComments = "";
    String strPartyId = "";
    String strUpdateAccess = "";

    HashMap hmParam = new HashMap();
    HashMap hmUDIParam = new HashMap();
    HashMap hmSaveUDIParm = new HashMap();
    HashMap hmAccess = new HashMap();
    ArrayList alUsRegClass = new ArrayList();
    ArrayList alUsRegPathway = new ArrayList();
    ArrayList alEuRegPathway = new ArrayList();
    ArrayList alLogReasons = new ArrayList();

    hmParam = GmCommonClass.getHashMapFromForm(gmPartParameterDtlsForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    strPartNumber = GmCommonClass.parseNull((String) hmParam.get("PARTNUMBER"));
    strInputStr = GmCommonClass.parseNull((String) hmParam.get("INPUTSTR"));
    strComments = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
    strUserId = GmCommonClass.parseNull((String) hmParam.get("USERID"));

    alUsRegClass = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("USREGC"));
    alUsRegPathway = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("USREGP"));
    alEuRegPathway = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("EUREGP"));

    ArrayList alResult = new ArrayList();
    HashMap<String, ArrayList> hmDropdwon = new HashMap<String, ArrayList>();
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("USREGP", "104681"));// Exempt
    hmDropdwon.put("alExempt", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("USREGP", "104683"));// Class
                                                                                               // II
    hmDropdwon.put("alClassII", alResult);
    alResult = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("USREGP", "104684"));// Class
                                                                                               // III
    hmDropdwon.put("alClassIII", alResult);
    // submit button access
    strPartyId = GmCommonClass.parseNull((String) hmParam.get("SESSPARTYID"));
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "REGULATORY_PART_TAB"));// Access for submit button in regulatory tab
    strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
    gmPartParameterDtlsForm.setButtonAccess(strUpdateAccess);
    if (strOpt.equals("Save")) {
      hmSaveUDIParm.put("PARTNUMBER", strPartNumber);
      hmSaveUDIParm.put("HLABELATTRIBUTE", strInputStr);
      hmSaveUDIParm.put("USERID", strUserId);
      gmPartNumBean.savePartNumParams(hmSaveUDIParm);
      if (!strComments.equals("")) {
        gmCommonBean.saveLog(strPartNumber, strComments, strUserId, "1294"); // 1294-->Part Number
                                                                             // -Regulatory tab
                                                                             // Comments
      }
    }
    hmUDIParam = gmPartNumBean.fetchRegulatoryParameterDtls(strPartNumber);

    hmUDIParam.put("alUsRegClass", alUsRegClass);
    hmUDIParam.put("alUsRegPathway", alUsRegPathway);
    hmUDIParam.put("alEuRegPatyway", alEuRegPathway);

    gmPartParameterDtlsForm.setHmAttrVal(hmUDIParam);
    gmPartParameterDtlsForm.setHmResult(hmDropdwon);
    alLogReasons = gmCommonBean.getLog(strPartNumber, "1294"); // 1294-->Part Number -Regulatory tab
                                                               // Comments
    gmPartParameterDtlsForm.setAlLogReasons(alLogReasons);

    return actionMapping.findForward(strForwardName);
  }

  /**
   * udiRegulatoryReports: This method is used to fetch the Regulatory details
   * 
   * @param actionMapping
   * @param actionForm
   * @param request
   * @param response
   * @return org.apache.struts.action.ActionForward
   */

  public ActionForward udiRegulatoryReports(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartNumBean gmPartNumBean = new GmPartNumBean(getGmDataStoreVO());
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmPartParameterDtlsForm gmPartParameterDtlsForm = (GmPartParameterDtlsForm) actionForm;
    gmPartParameterDtlsForm.loadSessionParameters(request);

    HashMap hmParam = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmTemplateParams = new HashMap();
    ArrayList alProjectId = new ArrayList();
    ArrayList alResult = new ArrayList();

    String strRegulatoryPartNum = "";
    String strOpt = "";
    String strPartNumFormat = "";

    hmParam = GmCommonClass.getHashMapFromForm(gmPartParameterDtlsForm);
    strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));

    hmTemp.put("COLUMN", "ID");
    hmTemp.put("STATUSID", "20301");

    alProjectId = gmProjectBean.reportProject(hmTemp);
    gmPartParameterDtlsForm.setAlProjectId(alProjectId);

    if (strOpt.equals("Load")) {

      // Searching Option using part number
      String strSearch = GmCommonClass.parseNull(request.getParameter("Cbo_Search"));
      strRegulatoryPartNum = GmCommonClass.parseNull((String) hmParam.get("REGULATORYPARTNUM"));
      strPartNumFormat = strRegulatoryPartNum;

      if (strSearch.equals("LIT")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|^");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("LIKEPRE")) {
        strPartNumFormat = ("^").concat(strPartNumFormat);
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|^");
      } else if (strSearch.equals("LIKESUF")) {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "\\$|");
        strPartNumFormat = strPartNumFormat.concat("$");
      } else if (strSearch.equals("")) {
        strPartNumFormat = strPartNumFormat;
      } else {
        strPartNumFormat = strPartNumFormat.replaceAll(",", "|");
      }

      hmParam.put("REGULATORYPARTNUM", strPartNumFormat);
      alResult = GmCommonClass.parseNullArrayList(gmPartNumBean.fetchRegulatoryReportDtls(hmParam));
      hmTemplateParams.put("TEMPLATE", "GmPartRegulatoryReport.vm");
      hmTemplateParams.put("TEMPLATEPATH", "quality/templates");
      hmTemplateParams.put("alResult", alResult);
      hmTemplateParams.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);

      // XML String
      String strXmlString = getXmlGridData(hmTemplateParams);
      gmPartParameterDtlsForm.setGridXml(strXmlString);
      request.setAttribute("hSearch", strSearch);
    }

    return actionMapping.findForward("gmPartRegulatoryReport");
  }

  /**
   * generateOutPut: For preparing string for vm file
   * 
   * @exception AppError
   */
  public String getXmlGridData(HashMap hmParam) throws AppError {
    String strTemplateName = GmCommonClass.parseNull((String) hmParam.get("TEMPLATE"));
    String strTemplatePath = GmCommonClass.parseNull((String) hmParam.get("TEMPLATEPATH"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    ArrayList alResult = new ArrayList();
    alResult = GmCommonClass.parseNullArrayList((ArrayList) hmParam.get("alResult"));
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    templateUtil.setDataList("alResult", alResult);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.quality.GmPartRegulatoryReport", strSessCompanyLocale));
    templateUtil.setTemplateSubDir(strTemplatePath);
    templateUtil.setTemplateName(strTemplateName);
    return templateUtil.generateOutput();
  }
}
