package com.globus.quality.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.common.util.GmWSUtil;
import com.globus.it.beans.GmCodeGroupBean;
import com.globus.prodmgmnt.beans.GmPartNumBean;
import com.globus.quality.forms.GmPartAttributeBulkSetupForm;
import com.globus.sales.beans.GmSalesReportBean;

public class GmPartAttributeBulkAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /* Method to save the Part Attribute data. */
  public ActionForward savePartAttribute(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {

    GmPartAttributeBulkSetupForm gmPartAttributeBulkSetupForm = (GmPartAttributeBulkSetupForm) form;
    gmPartAttributeBulkSetupForm.loadSessionParameters(request);
    instantiate(request, response);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPartNumBean gmPartNumBean = new GmPartNumBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
    String strOpt = "";
    String strValidParts = "";
    String strInvalidParts = "";
    String strGridData = "";
    String strPartyId = "";
    String strattributeType = "";
    String strscreenType = "";
    String strMAPUpdFl = "";
    String strExcldType = "";
    String strPrimCompId = "";
    String strMAPUpdFlforVoid = "";
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmAccessVoid = new HashMap();
    HashMap hmDetails = new HashMap();
    HashMap hmTemp = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alAttributeType = new ArrayList();
    strOpt = gmPartAttributeBulkSetupForm.getStrOpt();
    strPartyId = (String) session.getAttribute("strSessPartyId");
    strPrimCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    hmParam = GmCommonClass.getHashMapFromForm(gmPartAttributeBulkSetupForm);

    strattributeType = GmCommonClass.parseNull(gmPartAttributeBulkSetupForm.getAttributeType());
    strscreenType = GmCommonClass.parseNull(gmPartAttributeBulkSetupForm.getScreenType());

    hmAccessVoid =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PART_ATTR_VOID_USER"));
    strMAPUpdFlforVoid = GmCommonClass.parseNull((String) hmAccessVoid.get("UPDFL"));

    if (strMAPUpdFlforVoid.equals("Y")) {
      gmPartAttributeBulkSetupForm.setAccessVoidfl(strMAPUpdFlforVoid);
    }

    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PART_ATTR_ADMIN_USER"));
    strMAPUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));



    // Attribute type loads based on primary company or Security event "PART_ATTR_ADMIN_USER".

    if (strMAPUpdFl.equals("Y")) {
      alAttributeType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTATTR"));
    } else {
      alAttributeType =
          GmCommonClass.parseNullArrayList(new GmCodeGroupBean().getCodeList("PTATTR", "",
              strPrimCompId));

    }

    // below code for attribute types with comma separation.

    for (int i = 0; i < alAttributeType.size(); i++) {
      hmTemp = (HashMap) alAttributeType.get(i);
      strExcldType += ((i == 0) ? "" : ",") + hmTemp.get("CODEID");
    }



    if (strOpt.equals("save")) {
      hmReturn = gmPartNumBean.savePartAttrributeBulk(hmParam);

      strValidParts = GmCommonClass.parseNull((String) hmReturn.get("VALIDPARTS"));
      strInvalidParts = GmCommonClass.parseNull((String) hmReturn.get("INVALIDPARTS"));

      hmParam.put("PARTNUMS", strValidParts);
      hmParam.put("PARTFL", "Y");
      gmPartAttributeBulkSetupForm.setStrInvalidParts(strInvalidParts);
      gmPartAttributeBulkSetupForm.setStrOpt("Reload");
      strOpt = "Reload";
    }


    if (strOpt.equals("Reload")) {
      hmParam.put("EXCLDTYPE", strExcldType);
      alResult = gmPartNumBean.fetchPartAttribute(hmParam);
      hmParam.put("ALRESULT", alResult);
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      strGridData = generateOutPut(hmParam, "GmPartAttributeReport.vm");
      gmPartAttributeBulkSetupForm.setGridData(strGridData);

      hmParam.put("RULEID", "CMPY_PART_ATTR");
      hmDetails = GmCommonClass.parseNullHashMap(gmCommonBean.fetchTransactionRules(hmParam));

      String strJSON = gmWSUtil.parseHashMapToJson(hmDetails);
      gmPartAttributeBulkSetupForm.setJsonString(strJSON);

    }

    gmPartAttributeBulkSetupForm.setAlAttrType(alAttributeType);
    return mapping.findForward(strscreenType);
  }

  /* Method to Load the Part Attribute data. */
  public ActionForward loadPartAttribute(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    GmPartAttributeBulkSetupForm gmPartAttributeBulkSetupForm = (GmPartAttributeBulkSetupForm) form;

    gmPartAttributeBulkSetupForm.loadSessionParameters(request);
    instantiate(request, response);
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmSalesReportBean gmSalesReportBean = new GmSalesReportBean(getGmDataStoreVO());
    GmWSUtil gmWSUtil = new GmWSUtil();
    GmPartNumBean gmPartNumBean = new GmPartNumBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    String strinvalidParts = "";
    String strOpt = "";
    String strPnum = "";
    String strAttrVal = "";
    String screenType = "";
    String strGridData = "";
    String strPartyId = "";
    String strMAPUpdFl = "";
    String strJSON = "";
    String strExcldType = "";
    String strPrimCompId = "";
    String strMAPUpdFlforVoid = "";
    screenType = GmCommonClass.parseNull(gmPartAttributeBulkSetupForm.getScreenType());
    strOpt = GmCommonClass.parseNull(gmPartAttributeBulkSetupForm.getStrOpt());
    strinvalidParts = GmCommonClass.parseNull(request.getParameter("strInvalidParts"));
    strPartyId = (String) session.getAttribute("strSessPartyId");
    strPrimCompId = GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyId"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    ArrayList alAttributeType = new ArrayList();
    ArrayList alSystemType = new ArrayList();
    ArrayList alResult = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmAccess = new HashMap();
    HashMap hmAccessVoid = new HashMap();
    HashMap hmDetails = new HashMap();
    HashMap hmTemp = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartAttributeBulkSetupForm);

    hmAccessVoid =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PART_ATTR_VOID_USER"));
    strMAPUpdFlforVoid = GmCommonClass.parseNull((String) hmAccessVoid.get("UPDFL"));

    if (strMAPUpdFlforVoid.equals("Y")) {
      gmPartAttributeBulkSetupForm.setAccessVoidfl(strMAPUpdFlforVoid);
    }
    hmAccess =
        GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,
            "PART_ATTR_ADMIN_USER"));
    strMAPUpdFl = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));

    // Attribute type loads based on primary company or Security event "PART_ATTR_ADMIN_USER".

    if (strMAPUpdFl.equals("Y")) {
      alAttributeType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTATTR"));
    } else {
      alAttributeType =
          GmCommonClass.parseNullArrayList(new GmCodeGroupBean().getCodeList("PTATTR", "",
              strPrimCompId));

    }

    // below code for attribute types with comma separation.
    for (int i = 0; i < alAttributeType.size(); i++) {
      hmTemp = (HashMap) alAttributeType.get(i);
      strExcldType += ((i == 0) ? "" : ",") + hmTemp.get("CODEID");
    }



    alSystemType = GmCommonClass.parseNullArrayList(gmSalesReportBean.loadProjectGroup());



    if (strOpt.equals("Reload")) {
      hmParam.put("EXCLDTYPE", strExcldType);
      alResult = gmPartNumBean.fetchPartAttribute(hmParam);
      hmParam.put("ALRESULT", alResult);
      hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      strGridData = generateOutPut(hmParam, "GmPartAttributeReport.vm");
      gmPartAttributeBulkSetupForm.setGridData(strGridData);
    }

    hmParam.put("RULEID", "CMPY_PART_ATTR");
    hmDetails = GmCommonClass.parseNullHashMap(gmCommonBean.fetchTransactionRules(hmParam));

    strJSON = gmWSUtil.parseHashMapToJson(hmDetails);
    gmPartAttributeBulkSetupForm.setJsonString(strJSON);
    gmPartAttributeBulkSetupForm.setAlAttrType(alAttributeType);
    gmPartAttributeBulkSetupForm.setAlSystemType(alSystemType);
    gmPartAttributeBulkSetupForm.setAccessAllComp(strMAPUpdFl);


    return mapping.findForward(screenType);

  }

  /* Method to print the data avaliable in ArrayList into Grid Format through VM File. */
  private String generateOutPut(HashMap hmParam, String vmFile) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    hmParam.put("DATEFORMAT", getGmDataStoreVO().getCmpdfmt());
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.quality.GmPartAttributeReport", strSessCompanyLocale));
    templateUtil.setTemplateSubDir("quality/templates");
    templateUtil.setTemplateName(vmFile);
    return templateUtil.generateOutput();
  }


}
