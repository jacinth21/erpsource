package com.globus.quality.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.quality.beans.GmSetMapTransactionBean;
import com.globus.quality.forms.GmSetListCopyForm;

/**
 * @author jgurunathan This Action used for maintaining Set List Copy
 *         funtionalities
 */
public class GmSetListCopyAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * This method to load the set list copy page when user click left link
	 * panel
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadSetListCopy(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmSetListCopyForm gmSetListCopyForm = (GmSetListCopyForm) actionForm;

		gmSetListCopyForm.loadSessionParameters(request);

		HashMap hmParam = new HashMap();
		String strFromSetId = GmCommonClass.parseNull(gmSetListCopyForm
				.getTxtFromSetID());
		String strToSetId = GmCommonClass.parseNull(gmSetListCopyForm
				.getTxtToSetID());
		// String strComment =
		// GmCommonClass.parseNull(gmSetListCopyForm.getTxtComment());
		String strOpt = GmCommonClass.parseNull(gmSetListCopyForm.getStrOpt());
		hmParam = GmCommonClass.getHashMapFromForm(gmSetListCopyForm);

		log.debug("hmParam >>>>" + hmParam);

		return actionMapping.findForward("gmSetListCopy");
	}

	/**
	 * This method used for save the set list copy - Copying From Set list into
	 * to Set list. The To Set will be voided before copying From Set List
	 * details
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward saveSetListCopy(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);
		GmSetListCopyForm gmSetListCopyForm = (GmSetListCopyForm) actionForm;
		gmSetListCopyForm.loadSessionParameters(request);

		HashMap hmParam = new HashMap();
		String strURL = "/gmSetListCopy.do?method=loadSetListCopy";
		String strDisplayNm = "Set List Copy";
		hmParam = GmCommonClass.getHashMapFromForm(gmSetListCopyForm);
		GmSetMapTransactionBean gmSetMapTransactionBean = new GmSetMapTransactionBean(
				getGmDataStoreVO());
		gmSetMapTransactionBean.saveSetListCopy(hmParam);
		String strFromSetId = GmCommonClass.parseNull(gmSetListCopyForm
				.getTxtFromSetID());
		String strToSetId = GmCommonClass.parseNull(gmSetListCopyForm
				.getTxtToSetID());
		String strMsg = "Set List copied from " + strFromSetId + " to Set "
				+ strToSetId;

		if (!strMsg.equals("")) {
			request.setAttribute("hType", "S");
			request.setAttribute("hDisplayNm", strDisplayNm);
			request.setAttribute("hRedirectURL", strURL);
			throw new AppError(strMsg, "S", 'S');
		}
		return loadSetListCopy(actionMapping, gmSetListCopyForm, request,
				response);
	}

}
