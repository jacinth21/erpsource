package com.globus.quality.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmVendorBean;
import com.globus.operations.inventory.beans.GmInvLocationBean;
import com.globus.operations.inventory.forms.GmInvLocationForm;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.quality.beans.GmPartAttributeBean;
import com.globus.quality.beans.GmPartNumberAssemblyBean;
import com.globus.quality.beans.GmSetMapTransactionBean;
import com.globus.quality.forms.GmPartNumberAssemblyForm;
import com.globus.quality.forms.GmPartProcessRptForm;
import com.globus.quality.forms.GmSetMappingUploadForm;
import com.globus.common.beans.GmCommonBean;

 /**
 * This class used to Load or Fetch and Create BOM and sub-component Mapping.
 * @author rajan
 */
public class GmPartNumberAssemblyAction extends GmDispatchAction {
	
  /**
	* log Code to Initialize the Logger Class
  */
  Logger log = GmLogger.getInstance(this.getClass().getName());

  /**
   * loadPartNumberAssemblyUploadDtls this method used to load the Part number BOM details
   * @param actionMapping, actionForm, request,response,
   * @return
   * @throws AppError,Exception
   */
  public ActionForward loadPartNumberAssemblyUploadDtls(ActionMapping actionMapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
      instantiate(request, response);
      GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
      GmPartNumberAssemblyForm gmPartNumberAssemblyForm = (GmPartNumberAssemblyForm) actionForm;
      gmPartNumberAssemblyForm.loadSessionParameters(request);
      ArrayList alSubComp = new ArrayList();
      ArrayList alPTMAPType = new ArrayList();
      ArrayList alPartType = new ArrayList();
      HashMap hmLoop = new HashMap();
      int intDiv = 0;
      String strPTMAPHtml = "";
      alSubComp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SUBUPL"));
      alPartType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTMAP"));
      gmPartNumberAssemblyForm.setAlSubComp(alSubComp);
      gmPartNumberAssemblyForm.setAlPartType(alPartType);
      String strPartNumber = gmPartNumberAssemblyForm.getPartNumber();
      gmPartNumberAssemblyForm.setAlPartType(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTMAP",getGmDataStoreVO())));
		//To form the POType dropdown array formation as string and set that value to form variable
      alPTMAPType = gmPartNumberAssemblyForm.getAlPartType();
				intDiv = alPTMAPType.size();
				hmLoop = new HashMap();
				if (intDiv > 0) {
					for (int i = 0; i < intDiv; i++) {
						hmLoop = (HashMap) alPTMAPType.get(i);
						strPTMAPHtml += "PTMAPArr[" + i + "] = new Array(\""
								+ hmLoop.get("CODEID") + "\",\""
								+ hmLoop.get("CODENM") + "\");";
					}
				}
		gmPartNumberAssemblyForm.setStrPTMAPHtml(GmCommonClass.parseNull(strPTMAPHtml));
		gmPartNumberAssemblyForm.setPartNumber(strPartNumber);
     return actionMapping.findForward("gmPartNumberAssembly");
  }
  
  /**
   * fetchPartHeaderDtls  - This used to fetch part number header details
   * @param actionMapping, actionForm, request, response
   * @return
   * @throws AppError, Exception
   */
  public ActionForward fetchPartHeaderDtls(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);
	    GmPartNumberAssemblyForm gmPartNumberAssemblyForm = (GmPartNumberAssemblyForm) actionForm;
	    GmPartNumberAssemblyBean gmPartNumberAssemblyBean = new GmPartNumberAssemblyBean(getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		String strPartDetailsJson = "";
        String strPartNumber = "";
        String strActionType = "";
		gmPartNumberAssemblyForm.loadSessionParameters(request);
		hmParam = GmCommonClass.getHashMapFromForm(gmPartNumberAssemblyForm);
		strPartNumber = (String) hmParam.get("PARTNUMBER");
		strActionType = (String) hmParam.get("SUBCOMPONENT");
		strPartDetailsJson = gmPartNumberAssemblyBean.fetchPartHeaderDtls(strPartNumber);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strPartDetailsJson.equals("")) {
			pw.write(strPartDetailsJson);
		}
		pw.flush();
		return null;
}
  
  /**
   * fetchPartNumberAssemblyDtls - This method used fetch the grid part numbers BOM details
   * @param actionMapping, actionForm, request, response
   * @return
   * @throws AppError, Exception
   */
  public ActionForward fetchPartNumberAssemblyDtls(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
	    instantiate(request, response);
	    GmPartNumberAssemblyForm gmPartNumberAssemblyForm = (GmPartNumberAssemblyForm) actionForm;
	    GmPartNumberAssemblyBean gmPartNumberAssemblyBean = new GmPartNumberAssemblyBean(getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		ArrayList alPartType = new ArrayList();
		String strPartDetailsJson = "";
        String strPartNumber = "";
        String strActionType = "";
		gmPartNumberAssemblyForm.loadSessionParameters(request);
		hmParam = GmCommonClass.getHashMapFromForm(gmPartNumberAssemblyForm);
		strPartNumber = (String) hmParam.get("PARTNUMBER");
		strActionType = (String) hmParam.get("SUBCOMPONENT");
		strPartDetailsJson = gmPartNumberAssemblyBean.fetchPartAssemblyDtls(strPartNumber, strActionType);
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strPartDetailsJson.equals("")) {
			pw.write(strPartDetailsJson);
		}
		pw.flush();
		return null;
  }
  
  /**
   * savePartNumberAssemblyUploadDtls - This method used to save the Part number BOM details
   * @param actionMapping, actionForm, request, response
   * @return
   * @throws AppError, Exception
   */
  public ActionForward savePartNumberAssemblyUploadDtls(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
	    instantiate(request, response);
	    log.debug("===savePartNumberAssemblyUploadDtls==");
	    // Form Intitalization
	    GmPartNumberAssemblyForm gmPartNumberAssemblyForm = (GmPartNumberAssemblyForm) actionForm;
	    // Bean Initalization
	    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
	    GmPartNumberAssemblyBean gmPartNumberAssemblyBean = new GmPartNumberAssemblyBean(getGmDataStoreVO());
	    gmPartNumberAssemblyForm.loadSessionParameters(request);
	    String strUserId = (String)request.getSession().getAttribute("strSessUserId"); 
	    String strPartNumber = gmPartNumberAssemblyForm.getPartNumber();
	    String strComments = gmPartNumberAssemblyForm.getTxt_LogReason();
	    GmCommonClass gmCommonClass = new GmCommonClass();
	    HashMap hmParams = new HashMap();	
	    String strPartDetailsJson = "";
	    hmParams = GmCommonClass.getHashMapFromForm(gmPartNumberAssemblyForm);
	    hmParams.put("LOG", strComments);
	    strPartDetailsJson = gmPartNumberAssemblyBean.savePartAssemblyUploadDtls(hmParams);
	    ArrayList alLog = gmCommonBean.getLog(strPartNumber, "26240883");
		gmPartNumberAssemblyForm.setAlLogReasons(alLog);
		gmPartNumberAssemblyForm.setPartNumber(strPartNumber);

		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strPartDetailsJson.equals("")) {
			pw.write(strPartDetailsJson);
		}
		pw.flush();
		return null;
  }
  
	/**
	 * reLoadPartNumberAssembly - This method used to reload the Part number
	 * assembly screen (after enter the part #).
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward reLoadPartNumberAssembly(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {
		instantiate(request, response);

		GmPartNumberAssemblyForm gmPartNumberAssemblyForm = (GmPartNumberAssemblyForm) actionForm;
		// Bean Declaration
		GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
		GmPartNumberAssemblyBean gmPartNumberAssemblyBean = new GmPartNumberAssemblyBean(getGmDataStoreVO());
		gmPartNumberAssemblyForm.loadSessionParameters(request);
		
		String strPartNumber = gmPartNumberAssemblyForm.getPartNumber();
		String strPTMAPHtml = "";
		int intDiv ;
		ArrayList alPTMAPType = new ArrayList ();
		ArrayList alSubComp = new ArrayList ();
		ArrayList alPartType = new ArrayList ();
		ArrayList alLog = new ArrayList ();
		HashMap hmLoop = new HashMap();
		
		gmPartNumberAssemblyForm.setAlPartType(GmCommonClass
				.parseNullArrayList(GmCommonClass.getCodeList("PTMAP",
						getGmDataStoreVO())));

		// To form the POType dropdown array formation as string and set that
		// value to form variable
		alPTMAPType = gmPartNumberAssemblyForm.getAlPartType();
		intDiv = alPTMAPType.size();
		
		if (intDiv > 0) {
			for (int i = 0; i < intDiv; i++) {
				hmLoop = (HashMap) alPTMAPType.get(i);
				strPTMAPHtml += "PTMAPArr[" + i + "] = new Array(\""
						+ hmLoop.get("CODEID") + "\",\"" + hmLoop.get("CODENM")
						+ "\");";
			}
		}
		gmPartNumberAssemblyForm.setStrPTMAPHtml(GmCommonClass.parseNull(strPTMAPHtml));

		log.debug("strPartNumber====");
		alSubComp = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SUBUPL"));
		alPartType = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("PTMAP"));

		gmPartNumberAssemblyForm.setAlSubComp(alSubComp);
		gmPartNumberAssemblyForm.setAlPartType(alPartType);

		alLog = gmCommonBean.getLog(strPartNumber, "26240883");
		gmPartNumberAssemblyForm.setAlLogReasons(alLog);

		return actionMapping.findForward("gmPartNumberAssembly");
	}
	  
}
