package com.globus.quality.actions;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.quality.beans.GmIssuingAgencyConfigBean;
import com.globus.quality.forms.GmIssuingAgencyConfigForm;

public class GmIssuingAgencyConfigAction extends GmDispatchAction {

	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * issuingAgencyConfiguration This method used to fetch/save the Issuing
	 * Agency configuration details.
	 * 
	 * @exception AppError
	 */

	public ActionForward issuingAgencyConfiguration(
			ActionMapping actionMapping, ActionForm actionForm,
			HttpServletRequest request, HttpServletResponse response)
			throws AppError {

		GmIssuingAgencyConfigForm gmIssuingAgencyConfigForm = (GmIssuingAgencyConfigForm) actionForm;
		GmIssuingAgencyConfigBean gmIssuingAgencyConfigBean = new GmIssuingAgencyConfigBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean();
		
		HashMap hmParam = new HashMap();
		HashMap hmValue = new HashMap();
		ArrayList alConfiguration = new ArrayList();
		ArrayList alIssuingAgency = new ArrayList();
		ArrayList alIdentifier = new ArrayList();
		ArrayList alAllIdentifier = new ArrayList();
		ArrayList alResult = new ArrayList();
		
		String strOpt = "";
		String strIssueAgencyId = "";
		String strConfigName = "";
		String strUserId = "";
		String strUpdateAccess = "";
		
		gmIssuingAgencyConfigForm.loadSessionParameters(request);
		hmParam = GmCommonClass.getHashMapFromForm(gmIssuingAgencyConfigForm);

		// to fetch the issue agency configure details
		alIssuingAgency = GmCommonClass.parseNullArrayList((ArrayList) gmIssuingAgencyConfigBean.fetchIssueAgency());
		alAllIdentifier = GmCommonClass.parseNullArrayList((ArrayList) gmIssuingAgencyConfigBean.fetchIssuingAgencyIdentifier(""));

		strOpt = GmCommonClass.parseNull((String) hmParam.get("STROPT"));
		strConfigName = GmCommonClass.parseZero((String) hmParam.get("CONFIGNAME"));
		strIssueAgencyId = GmCommonClass.parseZero((String) hmParam.get("ISSUINGAGENCY"));
		strUserId = GmCommonClass.parseZero((String) hmParam.get("USERID"));
		
		if (strOpt.equals("Save")) {
			strConfigName = GmCommonClass.parseZero((String) gmIssuingAgencyConfigBean.saveIssueAgencyInfo(hmParam));
			strOpt = "Reload";
		}

		if (strOpt.equals("Reload")) {
			hmValue = gmIssuingAgencyConfigBean.fetchUDIConfiguredDtls(strConfigName);
			alResult = GmCommonClass.parseNullArrayList((ArrayList) hmValue.get("ALRETURN"));
			hmParam = GmCommonClass.parseNullHashMap((HashMap) hmValue.get("HMCONFIGDTLS"));
			GmCommonClass.getFormFromHashMap(gmIssuingAgencyConfigForm, hmParam);
			// to get the issuing agency id
			String strIssueAgency = GmCommonClass.parseZero((String) gmIssuingAgencyConfigForm.getIssuingAgency());
			alIdentifier = GmCommonClass.parseNullArrayList((ArrayList) gmIssuingAgencyConfigBean.fetchIssuingAgencyIdentifier(strIssueAgency));
			gmIssuingAgencyConfigForm.setAlResult(alResult);
			gmIssuingAgencyConfigForm.setAlIdentifier(alIdentifier);
		}
		alConfiguration = GmCommonClass.parseNullArrayList((ArrayList) gmIssuingAgencyConfigBean.fetchConfigName());
		// get the access permission for Read only access to portal
		HashMap	hmAccess= GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strUserId,"UDICONFIG_SETUP_ACC"));
		strUpdateAccess = GmCommonClass.parseNull((String) hmAccess.get("UPDFL"));
		if(!strUpdateAccess.equals("Y")){
			gmIssuingAgencyConfigForm.setButtonDisabled("true");
		}
		gmIssuingAgencyConfigForm.setAlConfiguration(alConfiguration);
		gmIssuingAgencyConfigForm.setAlIssuingAgency(alIssuingAgency);
		gmIssuingAgencyConfigForm.setAlAllIdentifier(alAllIdentifier);

		return actionMapping.findForward("gmIssuingAgencyConfig");
	}
}