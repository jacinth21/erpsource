package com.globus.quality.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmWSUtil;
import com.globus.quality.beans.GmReleaseForSaleBean;
import com.globus.quality.forms.GmReleaseForSaleForm;

public class GmReleaseForSaleAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

  String strXmlGridData = "";

  public ActionForward loadReleaseForSale(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError {

    log.debug("Enter into loadReleaseForSale in Action");

    GmReleaseForSaleBean gmReleaseForSaleBean = new GmReleaseForSaleBean();
    GmReleaseForSaleForm gmReleaseForSaleForm = (GmReleaseForSaleForm) actionForm;
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getAttribute("strSessCompanyLocale"));
	
    gmReleaseForSaleForm.setStrXMLGrid(strXmlGridData);

    HashMap hmReturn = new HashMap();
    hmReturn = gmReleaseForSaleBean.loadRFSDetails();
    // fetch details form the bean method & assign it to HashMap & set it into form property.
    gmReleaseForSaleForm.setAlRFSStatus((ArrayList) hmReturn.get("RFSSTATUS"));
    gmReleaseForSaleForm.setAlRFSCountry((ArrayList) hmReturn.get("RFSCOUNTRY"));
    gmReleaseForSaleForm.setComments("");

    return actionMapping.findForward("gmReleaseForSale");
  }

  public ActionForward saveReleaseForSale(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError ,Exception{

    log.debug("Enter into saveReleaseForSale in Action ");
    GmReleaseForSaleBean gmReleaseForSaleBean = new GmReleaseForSaleBean();
    GmReleaseForSaleForm gmReleaseForSaleForm = (GmReleaseForSaleForm) actionForm;

    ArrayList alOutPartInfo = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    GmWSUtil gmWSUtil = new GmWSUtil();
    String strMsg = "";
    String strInvalidParts = "";
    String strComment = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getAttribute("strSessCompanyLocale"));
    gmReleaseForSaleForm.loadSessionParameters(request);
    hmParam = GmCommonClass.getHashMapFromForm(gmReleaseForSaleForm);
    // get the form value form & assign it to hmParam.
    // pass the hmParam to bean method & return value assign to HashMap hmReturn.
    hmReturn = gmReleaseForSaleBean.saveRFSDetails(hmParam);
log.debug("hmReturn=="+hmReturn);
    strMsg = (String) hmReturn.get("MSG");
    strInvalidParts = (String) hmReturn.get("INVALIDPART");
    strComment = (String) hmReturn.get("RFSCOMMENT");
    alOutPartInfo = (ArrayList) hmReturn.get("OUTPARTS");
    strInvalidParts = strInvalidParts.replaceAll(",", ", ");
    // set the HashMap value into form property.
    gmReleaseForSaleForm.setMsg(strMsg);
    gmReleaseForSaleForm.setInvalidParts(strInvalidParts);
    gmReleaseForSaleForm.setComments(strComment);

	
	  // pass the ArrayList to getXmlGridData to generate grid with data.
	  strXmlGridData =
	  GmCommonClass.parseNull(gmReleaseForSaleBean.getXmlGridData(alOutPartInfo,
	  strSessCompanyLocale));
	 
    
    gmReleaseForSaleForm.setStrXMLGrid(strXmlGridData);
    String  strJSON = GmCommonClass.parseNull((String) gmWSUtil.parseArrayListToJson(alOutPartInfo));
    gmReleaseForSaleForm.setStrXMLGrid(strJSON);
   log.debug("strInvalidParts=="+strInvalidParts);
 
    return actionMapping.findForward("gmReleaseForSale");
  }
}
