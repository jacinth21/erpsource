
	/**
	 * FileName : GmSystemMappingAction.java 
	 * Description : 
	 * Author : Agilan
	 */

package com.globus.quality.actions;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.prodmgmnt.beans.GmSystemBean;
import com.globus.prodmgmnt.forms.GmSystemMappingForm;
import com.globus.quality.forms.GmSetMappingForm;
import com.globus.quality.beans.GmSetBundleMappingBean;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmAutoCompleteTransBean;
	/**
	 * @author Agilan
	 * 
	 */
	public class GmSetMappingAction extends GmAction {

	  Logger log = GmLogger.getInstance(this.getClass().getName());

	  /* (non-Javadoc)
	 * @see org.apache.struts.action.Action
	 * #execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
	      HttpServletResponse response) throws Exception {
		GmSetMappingForm gmSetMappingForm = (GmSetMappingForm) form;
		gmSetMappingForm.loadSessionParameters(request);
		GmSetBundleMappingBean gmSetBundleMappingBean = new GmSetBundleMappingBean(getGmDataStoreVO());
	    ArrayList alSystemMapDtls = new ArrayList();
	    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
	    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
	    String mapAccessFlag = "";
	    String strForward = "";
	    String strSessCompanyLocale =
	        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
	    HashMap hmParam = new HashMap();
	    HashMap hmAccess = new HashMap();
	    HashMap hmValues = new HashMap();
	    String strOpt = GmCommonClass.parseNull(gmSetMappingForm.getStrOpt());
	    String strBundleId = GmCommonClass.parseNull(gmSetMappingForm.getSystemId());
	    String strSetId = GmCommonClass.parseNull(gmSetMappingForm.getSetId());
	    String strPartyId = GmCommonClass.parseNull(gmSetMappingForm.getSessPartyId()); 
	    String strSetBundlenm = GmCommonClass.parseNull(gmSetMappingForm.getTxtSetName());
	    String strSetBundleold = GmCommonClass.parseNull(gmSetMappingForm.getOld_BundleId());
	    hmParam = GmCommonClass.getHashMapFromForm(gmSetMappingForm);
	    hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
	    // After saving the SYSTEM-SET mapping details, need to redirect the SYSTEM SETUP container
	    // page.
	    if (strOpt.equals("save")) {
	    	strSetId = GmCommonClass.parseNull(gmSetMappingForm.getSetId()); 	
	      saveSystemMapping(gmSetMappingForm, hmParam);
	      synSetBundleList(strBundleId,strSetBundleold,strSetBundlenm,"synSetBundleList",GmCommonClass.parseNull(request.getParameter("companyInfo")));
	      synSetBundleNameKeyList(strBundleId,strSetBundleold,strSetBundlenm,"synSetBundleNameKeyList",GmCommonClass.parseNull(request.getParameter("companyInfo")));
	      strForward = "/gmSetBundleMapping.do?strOpt=fetchcontainer&tabToLoad=mapping&strBundleId=" + strBundleId;
	      return actionRedirect(strForward, request);
	    }

	    if (strOpt.equals("") || strOpt.equals("fetch")) {
	    	hmValues = fetchSystemMapping(gmSetMappingForm, hmParam);
	      request.setAttribute("HMRETURN", hmValues);
	    }
	    // When the strOpt is NULL, need to forward the page to GmSystemMapping.jsp.
	    if (strOpt.equals("")) {
	      strOpt = "fetch";
	    }
	    // Below condition is used to set status by AJAX call from the screen.
	    if (strOpt.equals("getSetStatus")) {
	        String strSetID = GmCommonClass.parseNull(gmSetMappingForm.getSetId());
		     if(!strSetID.equals("")){
		      hmValues	= gmSetBundleMappingBean.fetchSetMapDtls(strSetID);
		      gmSetMappingForm =
		              (GmSetMappingForm) GmCommonClass.getFormFromHashMap(gmSetMappingForm, hmValues);
		     }
		     String strSetDesc=GmCommonClass.parseNull((String) hmValues.get("SETDESC"));
		     String strSetStatus=GmCommonClass.parseNull((String) hmValues.get("SETSTATUS"));
		     String strType=GmCommonClass.parseNull((String) hmValues.get("SETYPE"));
		     String strSetType=GmCommonClass.parseNull((String) hmValues.get("SETTYPE"));
		     String strInputString =
		    		 strSetID + "^" + strSetDesc + "^" + strSetType + "^" + strType + "^" + strSetStatus;
	        response.setContentType("text/xml");
	        if ( !strSetStatus.equals("")) {
	          PrintWriter pw = response.getWriter();
	          pw.write(strInputString);
	          pw.flush();
	        }
	        return null;
	      }

	    log.debug("Forwarding to = >" + strOpt + "<");
	    return mapping.findForward(strOpt);
	  }

	 
	  /**
	 * @param gmSetMappingForm
	 * @param hmParam
	 * @return
	 * @throws Exception
	 */
	private HashMap fetchSystemMapping(GmSetMappingForm gmSetMappingForm, HashMap hmParam)
	      throws Exception {
		GmSetBundleMappingBean gmSetBundleMappingBean = new GmSetBundleMappingBean(getGmDataStoreVO());
	    GmSystemBean gmSystemBean = new GmSystemBean();
	    GmCommonBean gmCommonBean = new GmCommonBean();
	    ArrayList alSystemMapDtls = new ArrayList();
	    HashMap hmValues=new HashMap();

	    String strBundleId = GmCommonClass.parseNull(gmSetMappingForm.getSystemId());
	    
	     String strSetID = GmCommonClass.parseNull(gmSetMappingForm.getSetId());
	     if(!strSetID.equals("")){
	     hmValues	= gmSetBundleMappingBean.fetchSetMapDtls(strSetID);
	     gmSetMappingForm =
	              (GmSetMappingForm) GmCommonClass.getFormFromHashMap(gmSetMappingForm, hmValues);
	     }

	    gmSetMappingForm.setAlLogReasons(gmCommonBean.getLog(strBundleId, "26240458"));
	    String strSessCompanyLocale =
	        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
	    alSystemMapDtls = gmSetBundleMappingBean.fetchSystemMapping(strBundleId);
	    gmSetMappingForm.setGridData(generateOutPut(alSystemMapDtls, strSessCompanyLocale));
	    gmSetMappingForm.setAlSetMapDtls(alSystemMapDtls);
	    return hmValues;
	  }

	
	  /**
	 * @param gmSetMappingForm
	 * @param hmParam
	 * @throws AppError
	 */
	private void saveSystemMapping(GmSetMappingForm gmSetMappingForm, HashMap hmParam)
	      throws AppError {
	    GmSystemBean gmSystemBean = new GmSystemBean();
	    GmSetBundleMappingBean gmSetBundleMappingBean = new GmSetBundleMappingBean(getGmDataStoreVO());
	    gmSetBundleMappingBean.saveSetMappingDtl(hmParam);
	  }
	
	/**
	 * This method generate the output for set details for the set bundle
	 * @param alResult
	 * @param strSessCompanyLocale
	 * @return
	 * @throws Exception
	 */

	  private String generateOutPut(ArrayList alResult, String strSessCompanyLocale) throws Exception {
	    GmTemplateUtil templateUtil = new GmTemplateUtil();
	    templateUtil.setDataList("alResult", alResult);
	    templateUtil.setTemplateSubDir("quality/templates");
	    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
	        "properties.labels.prodmgmnt.GmSetMapReport", strSessCompanyLocale));
	    templateUtil.setTemplateName("GmSetMappingDtls.vm");
	    return templateUtil.generateOutput();
	  }
	//To sync the modified / new value with the redis cache 
	  public void synSetBundleList(String strSetBundleId,String strSetBundlenm,String strSetBunnm,String strMethod ,String strCompanyInfo) throws Exception {
		  log.debug("test");
		    GmAutoCompleteTransBean gmAutoCompleteTransBean =
		        new GmAutoCompleteTransBean(getGmDataStoreVO());
		    HashMap hmJmsParam = new HashMap();
		    hmJmsParam.put("ID", strSetBundleId);
		    hmJmsParam.put("OLD_NM", strSetBundlenm);
		    hmJmsParam.put("NEW_NM", strSetBunnm);
		    hmJmsParam.put("METHOD", strMethod);
		    hmJmsParam.put("companyInfo", strCompanyInfo);
		    gmAutoCompleteTransBean.saveDetailsToCache(hmJmsParam);
		  }
	//To sync the modified / new value with the redis cache 
	  public void synSetBundleNameKeyList(String strSetBundleId,String strSetBundlenm,String strSetBunnm,String strMethod ,String strCompanyInfo) throws Exception {
			
		    GmAutoCompleteTransBean gmAutoCompleteTransBean = new GmAutoCompleteTransBean(getGmDataStoreVO());
		    HashMap hmJmsParam = new HashMap();
		    hmJmsParam.put("ID", strSetBundleId);
		    hmJmsParam.put("OLD_NM", strSetBundlenm);
		    hmJmsParam.put("NEW_NM", strSetBunnm);
		    hmJmsParam.put("METHOD", strMethod);
		    hmJmsParam.put("companyInfo", strCompanyInfo);
		    gmAutoCompleteTransBean.saveDetailsToCache(hmJmsParam);
		  }
	}

