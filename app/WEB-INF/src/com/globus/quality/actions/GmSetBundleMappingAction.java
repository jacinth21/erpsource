/**
 * FileName : GmSystemDetailAction.java Description : Author : Agilan Singaravelan
 */
package com.globus.quality.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.commons.beanutils.RowSetDynaClass;
import com.globus.common.actions.GmAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.corporate.beans.GmDivisionBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.prodmgmnt.beans.GmSystemBean;
import com.globus.prodmgmnt.forms.GmSystemDetailForm;
import com.globus.sales.beans.GmSalesReportBean;
import com.globus.quality.beans.GmSetBundleMappingBean;
import com.globus.quality.forms.GmSetBundleMappingForm;
import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.common.beans.GmAutoCompleteTransBean;

public class GmSetBundleMappingAction extends GmAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());

/* (non-Javadoc)
 * @see org.apache.struts.action.Action
 * #execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
 */
@Override
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
	log.debug("GmSetBundleMappingAction execute method -->");
    instantiate(request, response);
    GmSetBundleMappingForm gmSetBundleMappingForm = (GmSetBundleMappingForm) form;
    gmSetBundleMappingForm.loadSessionParameters(request);
    GmSetBundleMappingBean gmSetBundleMappingBean = new GmSetBundleMappingBean(getGmDataStoreVO());
    GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    String accessFlag = "";
    String strForward = "";
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessCompanyLocale"));
    HashMap hmParam = new HashMap();
    HashMap hmAccess = new HashMap();
    String strOpt = GmCommonClass.parseNull(gmSetBundleMappingForm.getStrOpt());
    String strSetBundleId = GmCommonClass.parseNull(gmSetBundleMappingForm.getStrBundleId());
    String strSetBundlenm = GmCommonClass.parseNull(gmSetBundleMappingForm.getTxtSetName());
    String strSetBundleold = GmCommonClass.parseNull(gmSetBundleMappingForm.getOld_BundleId());
    String strPartyId = GmCommonClass.parseNull(gmSetBundleMappingForm.getSessPartyId()); // Need to
                                                                                      // pass this
                                                                                      // to
                                                                                      // getAccessPermission
    hmParam = GmCommonClass.getHashMapFromForm(gmSetBundleMappingForm);
    // After saving the SYSTEM details, need to redirect thecontainer page.
    if (strOpt.equals("save")) {
      strSetBundleId = GmCommonClass.parseNull(saveSystemDetail(gmSetBundleMappingForm, hmParam));
      synSetBundleList(strSetBundleId,strSetBundleold,strSetBundlenm,"synSetBundleList",GmCommonClass.parseNull(request.getParameter("companyInfo")));
      synSetBundleNameKeyList(strSetBundleId,strSetBundleold,strSetBundlenm,"synSetBundleNameKeyList",GmCommonClass.parseNull(request.getParameter("companyInfo")));
      strForward = "/gmSetBundleMapping.do?strOpt=fetchcontainer&strBundleId=" + strSetBundleId;
      return actionRedirect(strForward, request);
    }
    if (strOpt.equals("") || strOpt.equals("fetchcontainer") || strOpt.equals("fetch")) {
      fetchSystemDetail(gmSetBundleMappingForm, hmParam);
    }
    if(strOpt.equals("History")){
    	RowSetDynaClass rdHistory= null;
    	strSetBundleId = GmCommonClass.parseNull(gmSetBundleMappingForm.getStrBundleId()); 
    	rdHistory = gmSetBundleMappingBean.fetchHistory(strSetBundleId);
        gmSetBundleMappingForm.setRdHistory(rdHistory);
    }
    if (strOpt.equals("setBundleCompanyMap")) {
        hmParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
        fetchSetBundleCompanyMap(gmSetBundleMappingForm, hmParam);        
    }
    // When the strOpt is NULL, need to forward the page to default Container.
    if (strOpt.equals("")) {
      strOpt = "default";
    }
    log.debug("Forwarding to = >" + strOpt + "<");
    return mapping.findForward(strOpt);
  }


  /**
 * @param gmSetBundleMappingForm
 * @param hmParam
 * @throws AppError
 */
private void fetchSystemDetail(GmSetBundleMappingForm gmSetBundleMappingForm, HashMap hmParam)
      throws AppError {
	log.debug("fetchSystemDetail() -->");
	GmSetBundleMappingBean gmSetBundleMappingBean = new GmSetBundleMappingBean(getGmDataStoreVO());
    GmCommonBean gmCommonBean = new GmCommonBean(getGmDataStoreVO());
    GmDivisionBean gmDivisionBean = new GmDivisionBean(getGmDataStoreVO());
    HashMap hmValues = new HashMap();
	int KeyCount=0;
    ArrayList alStatusList = new ArrayList();
    ArrayList alResult = new ArrayList();
    ArrayList alDivisionList = new ArrayList();
    String strBundleId = GmCommonClass.parseNull(gmSetBundleMappingForm.getStrBundleId());
    if(strBundleId.equals("0")){
    	strBundleId = "";
    	gmSetBundleMappingForm.setStrBundleId(strBundleId);
    }
    alDivisionList = GmCommonClass.parseNullArrayList(gmDivisionBean.fetchDivision(hmParam));
    gmSetBundleMappingForm.setAlDivList(alDivisionList);
    alStatusList=(GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SBSTS")));
  	gmSetBundleMappingForm.setAlSetStatus(alStatusList);
    if (!strBundleId.equals("")) {
      hmValues = gmSetBundleMappingBean.fetchSetBundleMapDtls(strBundleId);
      gmSetBundleMappingForm =
          (GmSetBundleMappingForm) GmCommonClass.getFormFromHashMap(gmSetBundleMappingForm, hmValues);
    }
    	alResult = gmSetBundleMappingBean.fetchSetBundleNameDtls();
    	gmSetBundleMappingForm.setAlResult(alResult);
    	KeyCount = gmSetBundleMappingBean.fetchKeywords(strBundleId);
    	gmSetBundleMappingForm.setKeyCount(KeyCount);
    gmSetBundleMappingForm.setAlLogReasons(gmCommonBean.getLog(strBundleId, "26240457"));
  }


  /**
 * @param gmSetBundleMappingForm
 * @param hmParam
 * @return
 * @throws AppError
 */
private String saveSystemDetail(GmSetBundleMappingForm gmSetBundleMappingForm, HashMap hmParam)
      throws AppError {
	log.debug("saveSystemDetail() -->");
	GmSetBundleMappingBean gmSetBundleMappingBean = new GmSetBundleMappingBean(getGmDataStoreVO());
    String strSetIdFromDb = gmSetBundleMappingBean.saveSetBundleMapping(hmParam); // Rename the name
    gmSetBundleMappingForm.setStrBundleId(strSetIdFromDb);
    
    return strSetIdFromDb;
  }

/**
 * This method used to fetch the set bundle company map info
 * @param gmSetBundleMappingForm
 * @param hmParam
 * @throws AppError
 */
private void fetchSetBundleCompanyMap(GmSetBundleMappingForm gmSetBundleMappingForm, HashMap hmParam)
	      throws AppError {
	log.debug("fetchSetBundleCompanyMap() -->");
		GmSetBundleMappingBean gmSetBundleMappingBean = new GmSetBundleMappingBean(getGmDataStoreVO());

	    HashMap hmTemplateParam = new HashMap();
	    ArrayList alSysCompList = new ArrayList();	    
	    String strSessCompanyLocale =  GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));	    
	    hmParam.put("DATEFMT", getGmDataStoreVO().getCmpdfmt());
	    
	    alSysCompList = GmCommonClass.parseNullArrayList(gmSetBundleMappingBean.fetchSetBundleCompanyMap(hmParam));
	    hmTemplateParam.put("DATA_MAP", hmParam);
	    hmTemplateParam.put("DATA_LIST", alSysCompList);
	    hmTemplateParam.put("TEMPLATE_NAME", "GmSystemCompanyMap.vm");
	    hmTemplateParam.put("TEMPLATE_SUB_DIR", "prodmgmnt/templates");
	    hmTemplateParam.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
	    hmTemplateParam.put("VMFILEPATH", "properties.labels.prodmgmnt.GmSystemCompanyMap");
	    // XML String
	    String strXmlString = GmCommonClass.getXmlGridData(hmTemplateParam);
	    gmSetBundleMappingForm.setGridXmlData(strXmlString);
	  }
// To sync the modified/ new value with the redis cache
public void synSetBundleList(String strSetBundleId,String strSetBundlenm,String strSetBunnm,String strMethod ,String strCompanyInfo) throws Exception {
	    GmAutoCompleteTransBean gmAutoCompleteTransBean =
	        new GmAutoCompleteTransBean(getGmDataStoreVO());
	    HashMap hmJmsParam = new HashMap();
	    hmJmsParam.put("ID", strSetBundleId);
	    hmJmsParam.put("OLD_NM", strSetBundlenm);
	    hmJmsParam.put("NEW_NM", strSetBunnm);
	    hmJmsParam.put("METHOD", strMethod);
	    hmJmsParam.put("companyInfo", strCompanyInfo);
	    gmAutoCompleteTransBean.saveDetailsToCache(hmJmsParam);
	  }
//To sync the modified / new value with the redis cache 
public void synSetBundleNameKeyList(String strSetBundleId,String strSetBundlenm,String strSetBunnm,String strMethod ,String strCompanyInfo) throws Exception {
	
	    GmAutoCompleteTransBean gmAutoCompleteTransBean = new GmAutoCompleteTransBean(getGmDataStoreVO());
	    HashMap hmJmsParam = new HashMap();
	    hmJmsParam.put("ID", strSetBundleId);
	    hmJmsParam.put("OLD_NM", strSetBundlenm);
	    hmJmsParam.put("NEW_NM", strSetBunnm);
	    hmJmsParam.put("METHOD", strMethod);
	    hmJmsParam.put("companyInfo", strCompanyInfo);
	    gmAutoCompleteTransBean.saveDetailsToCache(hmJmsParam);
	  }

}
