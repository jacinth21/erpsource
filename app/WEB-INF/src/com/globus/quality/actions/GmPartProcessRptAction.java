package com.globus.quality.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmVendorBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.quality.beans.GmPartAttributeBean;
import com.globus.quality.forms.GmPartProcessRptForm;

public class GmPartProcessRptAction extends GmDispatchAction {
  Logger log = GmLogger.getInstance(this.getClass().getName());

  public ActionForward partProcessRpt(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {

    instantiate(request, response);
    GmPartProcessRptForm gmPartProcessRptForm = (GmPartProcessRptForm) actionForm;
    gmPartProcessRptForm.loadSessionParameters(request);

    HashMap hmParam = new HashMap();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartProcessRptForm);
    loadDropDownList(gmPartProcessRptForm);
    return actionMapping.findForward("gmPartProcessRpt");
  }

  /* Method to Load the Part Process Report data. */
  public ActionForward partProcessRptData(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws AppError, Exception {
    instantiate(request, response);
    GmPartProcessRptForm gmPartProcessRptForm = (GmPartProcessRptForm) actionForm;
    gmPartProcessRptForm.loadSessionParameters(request);
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alResult = new ArrayList();
    hmParam = GmCommonClass.getHashMapFromForm(gmPartProcessRptForm);
    GmPartAttributeBean gmPartAttributeBean = new GmPartAttributeBean(getGmDataStoreVO());
    loadDropDownList(gmPartProcessRptForm);
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) session.getAttribute("strSessCompanyLocale"));
    alResult = gmPartAttributeBean.fetchPartProcessRptData(hmParam);
    hmReturn.put("PARTPROCESSRPTDATA", alResult);
    hmReturn.put("APPLNDATEFMT", strApplDateFmt);
    hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
    String strXmlGridData = generateOutPut(hmReturn);
    gmPartProcessRptForm.setXmlGridData(strXmlGridData);
    return actionMapping.findForward("gmPartProcessRpt");
  }

  /* Method to print the data avaliable in ArrayList into Grid Format through VM File. */
  private String generateOutPut(HashMap hmParam) throws AppError {
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));
    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("quality/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.quality.GmPartProcessRpt", strSessCompanyLocale));
    templateUtil.setTemplateName("GmPartProcessRpt.vm");
    return templateUtil.generateOutput();
  }

  /* Method to Load the Dropdown Values for Project,System and Vendors. */
  public void loadDropDownList(GmPartProcessRptForm gmPartProcessRptForm) throws AppError {
    GmProjectBean gmProjectBean = new GmProjectBean(getGmDataStoreVO());
    GmVendorBean gmVendorBean = new GmVendorBean(getGmDataStoreVO());
    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    ArrayList alResult = new ArrayList();
    ArrayList alSystemList = new ArrayList();
    ArrayList alProjectList = new ArrayList();
    ArrayList alVendorList = new ArrayList();
    HashMap hmVendorList = new HashMap();
    hmParam.put("COLUMN", "ID");
    hmParam.put("STATUSID", "20301");
    hmParam.put("CHECKACTIVEFL", "N");
    alProjectList = gmProjectBean.reportProject(hmParam);
    gmPartProcessRptForm.setAlProjectList(alProjectList);
    alSystemList = gmProjectBean.loadSetMap("GROUPPARTMAPPING");
    gmPartProcessRptForm.setAlSystemList(alSystemList);
    hmVendorList = gmVendorBean.getVendorList(hmParam);
    gmPartProcessRptForm.setAlVendorList((ArrayList) hmVendorList.get("VENDORLIST"));
  }
}
