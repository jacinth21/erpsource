package com.globus.quality.actions;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
import com.globus.operations.beans.GmVendorBean;
import com.globus.prodmgmnt.beans.GmProjectBean;
import com.globus.quality.beans.GmPartAttributeBean;
import com.globus.quality.beans.GmSetMapTransactionBean;
import com.globus.quality.forms.GmSetMappingUploadForm;

/**
 * This class used to Add or modify the existing set mapping details.
 * 
 * @category Set Mapping
 * @version 1.0
 * @author mmuthusamy
 * @since 11/25/2019
 *
 */

public class GmSetMappingUploadAction extends GmDispatchAction {
	Logger log = GmLogger.getInstance(this.getClass().getName());

	/**
	 * loadSetMappingUpload - This method used to once user click the left link
	 * its redirect to JSP page.
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward loadSetMappingUpload(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmSetMappingUploadForm gmSetMappingUploadForm = (GmSetMappingUploadForm) actionForm;
		gmSetMappingUploadForm.loadSessionParameters(request);

		// to get the code name list and set to form
		gmSetMappingUploadForm.setAlUploadAction(GmCommonClass
				.parseNullArrayList(GmCommonClass.getCodeList("SETUPA")));

		return actionMapping.findForward("gmSetMappingUpload");
	}

	/**
	 * fetchSetMappingUpload - This method used to fetch last uploaded details
	 * and send JSON string
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward fetchSetMappingUpload(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmSetMappingUploadForm gmSetMappingUploadForm = (GmSetMappingUploadForm) actionForm;
		GmSetMapTransactionBean gmSetMapTransactionBean = new GmSetMapTransactionBean(
				getGmDataStoreVO());
		HashMap hmParam = new HashMap();
		String strSetUploadJson = "";

		gmSetMappingUploadForm.loadSessionParameters(request);
		hmParam = GmCommonClass.getHashMapFromForm(gmSetMappingUploadForm);

		// 108745 Validation Failed
		strSetUploadJson = gmSetMapTransactionBean.fetchSetMappingUpload(
				(String) hmParam.get("SETID"), "");
		response.setContentType("text/json");
		PrintWriter pw = response.getWriter();
		if (!strSetUploadJson.equals("")) {
			pw.write(strSetUploadJson);
		}
		pw.flush();
		return null;
	}

	/**
	 * saveSetMappingUpload - This method used to save set mapping upload
	 * details based on action (add/modify/delete)
	 * 
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws AppError
	 * @throws Exception
	 */
	public ActionForward saveSetMappingUpload(ActionMapping actionMapping,
			ActionForm actionForm, HttpServletRequest request,
			HttpServletResponse response) throws AppError, Exception {

		instantiate(request, response);
		GmSetMappingUploadForm gmSetMappingUploadForm = (GmSetMappingUploadForm) actionForm;
		GmSetMapTransactionBean gmSetMapTransactionBean = new GmSetMapTransactionBean(
				getGmDataStoreVO());

		HashMap hmParam = new HashMap();
		String strErrorMapDtls = "";
		String strRedirectURL = "/gmSetMappingUpload.do?method=loadSetMappingUpload&strOpt=SETMAP";
		String strSuccessMsg = "";

		gmSetMappingUploadForm.loadSessionParameters(request);
		hmParam = GmCommonClass.getHashMapFromForm(gmSetMappingUploadForm);

		strErrorMapDtls = gmSetMapTransactionBean.saveSetMappingUploadDtls(hmParam);

		// If no error message then, Action to be success (Add/Modify or Remove)
		// - Show green message
		if (strErrorMapDtls.equals("")) {
			String strSetName = GmCommonClass.parseNull((String) hmParam
					.get("SEARCHSETID"));
			String strAction = GmCommonClass.parseNull((String) hmParam
					.get("UPLOADACTION"));
			strSuccessMsg = "Set Mapping - Bulk Upload (Add/Modify) for the Set <b> "
					+ strSetName + "</b>";

			// 108742 - Remove
			if (strAction.equalsIgnoreCase("108742")) {
				strSuccessMsg = "Set Mapping - Bulk Upload (Voided) for the Set <b> "
						+ strSetName + "</b>";
			}

			request.setAttribute("hType", "S");
			request.setAttribute("hDisplayNm",
					"Set Mapping - Bulk Upload screen ");
			request.setAttribute("hRedirectURL", strRedirectURL);
			throw new AppError(strSuccessMsg, "S", 'S');

		} else {
			// redirect to fetch method and display error details
			String strRedirectPath = "/gmSetMappingUpload.do?method=loadSetMappingUpload"
					+ "&setId="
					+ GmCommonClass.parseNull((String) hmParam.get("SETID"))
					+ "&searchsetId="
					+ GmCommonClass.parseNull((String) hmParam
							.get("SEARCHSETID")) + "&haction=Reload";
			return actionRedirect(strRedirectPath, request);
		}

	}

}
