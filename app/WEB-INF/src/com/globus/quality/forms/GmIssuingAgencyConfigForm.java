package com.globus.quality.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

public class GmIssuingAgencyConfigForm extends GmLogForm {


	private String configName = "";
	private String txt_configName = "";
	private String issuingAgency = "";
	private String inputStr = "";
	private String samplePattern = "";
	private String buttonDisabled = "";
	
	private ArrayList alConfiguration = new ArrayList();
	private ArrayList alIssuingAgency = new ArrayList();
	private ArrayList alAllIdentifier = new ArrayList();
	private ArrayList alIdentifier = new ArrayList();
	private ArrayList alResult = new ArrayList ();
	/**
	 * @return the configName
	 */
	public String getConfigName() {
		return configName;
	}
	/**
	 * @param configName the configName to set
	 */
	public void setConfigName(String configName) {
		this.configName = configName;
	}
	/**
	 * @return the txt_configName
	 */
	public String getTxt_configName() {
		return txt_configName;
	}
	/**
	 * @param txt_configName the txt_configName to set
	 */
	public void setTxt_configName(String txt_configName) {
		this.txt_configName = txt_configName;
	}
	/**
	 * @return the issuingAgency
	 */
	public String getIssuingAgency() {
		return issuingAgency;
	}
	/**
	 * @param issuingAgency the issuingAgency to set
	 */
	public void setIssuingAgency(String issuingAgency) {
		this.issuingAgency = issuingAgency;
	}
	/**
	 * @return the inputStr
	 */
	public String getInputStr() {
		return inputStr;
	}
	/**
	 * @param inputStr the inputStr to set
	 */
	public void setInputStr(String inputStr) {
		this.inputStr = inputStr;
	}
	/**
	 * @return the samplePattern
	 */
	public String getSamplePattern() {
		return samplePattern;
	}
	/**
	 * @param samplePattern the samplePattern to set
	 */
	public void setSamplePattern(String samplePattern) {
		this.samplePattern = samplePattern;
	}
	/**
	 * @return the alConfiguration
	 */
	public ArrayList getAlConfiguration() {
		return alConfiguration;
	}
	/**
	 * @param alConfiguration the alConfiguration to set
	 */
	public void setAlConfiguration(ArrayList alConfiguration) {
		this.alConfiguration = alConfiguration;
	}
	/**
	 * @return the alIssuingAgency
	 */
	public ArrayList getAlIssuingAgency() {
		return alIssuingAgency;
	}
	/**
	 * @param alIssuingAgency the alIssuingAgency to set
	 */
	public void setAlIssuingAgency(ArrayList alIssuingAgency) {
		this.alIssuingAgency = alIssuingAgency;
	}
	/**
	 * @return the alIdentifier
	 */
	public ArrayList getAlIdentifier() {
		return alIdentifier;
	}
	/**
	 * @param alIdentifier the alIdentifier to set
	 */
	public void setAlIdentifier(ArrayList alIdentifier) {
		this.alIdentifier = alIdentifier;
	}
	/**
	 * @return the alResult
	 */
	public ArrayList getAlResult() {
		return alResult;
	}
	/**
	 * @param alResult the alResult to set
	 */
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	/**
	 * @return the alAllIdentifier
	 */
	public ArrayList getAlAllIdentifier() {
		return alAllIdentifier;
	}
	/**
	 * @param alAllIdentifier the alAllIdentifier to set
	 */
	public void setAlAllIdentifier(ArrayList alAllIdentifier) {
		this.alAllIdentifier = alAllIdentifier;
	}
	/**
	 * @return the buttonDisabled
	 */
	public String getButtonDisabled() {
		return buttonDisabled;
	}
	/**
	 * @param buttonDisabled the buttonDisabled to set
	 */
	public void setButtonDisabled(String buttonDisabled) {
		this.buttonDisabled = buttonDisabled;
	}

	
	
	
	
}