package com.globus.quality.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

public class GmSetListCopyForm extends GmLogForm {
	
	private String submitAccessFl = "";
	private String txtFromSetID ="";
	private String txtToSetID ="";
	private String fromSetId = "";
	private String toSetId = "";
	private String fromSetName = "";
	private String searchfromSetName = "";
	private String toSetName = "";
	private String searchtoSetName = "";
	
	/**
	 * @return the txtFromSetID
	 */
	public String getTxtFromSetID() {
		return txtFromSetID;
	}
	/**
	 * @param txtFromSetID the txtFromSetID to set
	 */
	public void setTxtFromSetID(String txtFromSetID) {
		this.txtFromSetID = txtFromSetID;
	}
	/**
	 * @return the txtToSetID
	 */
	public String getTxtToSetID() {
		return txtToSetID;
	}
	/**
	 * @param txtToSetID the txtToSetID to set
	 */
	public void setTxtToSetID(String txtToSetID) {
		this.txtToSetID = txtToSetID;
	}
	/**
	 * @return the submitAccessFl
	 */
	public String getSubmitAccessFl() {
		return submitAccessFl;
	}
	/**
	 * @param submitAccessFl the submitAccessFl to set
	 */
	public void setSubmitAccessFl(String submitAccessFl) {
		this.submitAccessFl = submitAccessFl;
	}
	/**
	 * @return the fromSetId
	 */
	public String getFromSetId() {
		return fromSetId;
	}
	/**
	 * @param fromSetId the fromSetId to set
	 */
	public void setFromSetId(String fromSetId) {
		this.fromSetId = fromSetId;
	}
	/**
	 * @return the toSetId
	 */
	public String getToSetId() {
		return toSetId;
	}
	/**
	 * @param toSetId the toSetId to set
	 */
	public void setToSetId(String toSetId) {
		this.toSetId = toSetId;
	}
	/**
	 * @return the fromSetName
	 */
	public String getFromSetName() {
		return fromSetName;
	}
	/**
	 * @param fromSetName the fromSetName to set
	 */
	public void setFromSetName(String fromSetName) {
		this.fromSetName = fromSetName;
	}
	/**
	 * @return the searchfromSetName
	 */
	public String getSearchfromSetName() {
		return searchfromSetName;
	}
	/**
	 * @param searchfromSetName the searchfromSetName to set
	 */
	public void setSearchfromSetName(String searchfromSetName) {
		this.searchfromSetName = searchfromSetName;
	}
	/**
	 * @return the toSetName
	 */
	public String getToSetName() {
		return toSetName;
	}
	/**
	 * @param toSetName the toSetName to set
	 */
	public void setToSetName(String toSetName) {
		this.toSetName = toSetName;
	}
	/**
	 * @return the searchtoSetName
	 */
	public String getSearchtoSetName() {
		return searchtoSetName;
	}
	/**
	 * @param searchtoSetName the searchtoSetName to set
	 */
	public void setSearchtoSetName(String searchtoSetName) {
		this.searchtoSetName = searchtoSetName;
	}
	
	
	
	
}