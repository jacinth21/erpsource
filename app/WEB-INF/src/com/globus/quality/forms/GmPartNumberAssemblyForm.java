package com.globus.quality.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

/**
 * Form class used to Part numbers BOM mapping's store request values
 * 
 * @author rajan
 *
 */
public class GmPartNumberAssemblyForm extends GmLogForm {

  private String subComponent = "";
  private String partNumber = "";
  private String partDescription = "";
  private String bomId = "";
  private String family = "";
  private String sterility = "";
  private String strEditAccess = "";
  private String strPTMAPHtml ="";
  private String inputString = "";
  private String strOpt = "";
  private String strPartNumbers = "";
  private String voidinputstring = "";
  private String bomFlag = "";
  private String bomIds = "";
  private String comments = "";
  
  private ArrayList alSubComp  = new ArrayList();
  private ArrayList alPartType  = new ArrayList();
  
     /**
	 * @return the subComponent
	 */
	public String getSubComponent() {
		return subComponent;
	}
	
	/**
	 * @return the partNumber
	 */
	public String getPartNumber() {
		return partNumber;
	}
	
	 /**
		 * @return the partDescription
	 */
	public String getPartDescription() {
		return partDescription;
	}
	
	/**
	 * @return the bomId
    */
	public String getBomId() {
		return bomId;
	}
	
	/**
	 * @return the family
    */
	public String getFamily() {
		return family;
	}
	
	/**
	 * @return the sterility
    */
	public String getSterility() {
		return sterility;
	}
	
	/**
	 * @return the strEditAccess
    */
	public String getStrEditAccess() {
		return strEditAccess;
	}
	
	/**
	 * @return the strPTMAPHtml
    */
	public String getStrPTMAPHtml() {
		return strPTMAPHtml;
	}
	
	/**
	 * @return the inputString
    */
	public String getInputString() {
		return inputString;
	}
	
	/**
	 * @return the strOpt
    */
	public String getStrOpt() {
		return strOpt;
	}
	
	/**
	 * @return the strPartNumbers
    */
	public String getStrPartNumbers() {
		return strPartNumbers;
	}
	
	/**
	 * @return the voidinputstring
    */
	public String getVoidinputstring() {
		return voidinputstring;
	}
	
	/**
	 * @return the bomFlag
    */
	public String getBomFlag() {
		return bomFlag;
	}
	
	/**
	 * @return the bomIds
    */
	public String getBomIds() {
		return bomIds;
	}
	
	/**
	 * @return the alSubComp
    */
	public ArrayList getAlSubComp() {
		return alSubComp;
	}
	
	/**
	 * @return the alPartType
    */
	public ArrayList getAlPartType() {
		return alPartType;
	}
	
	/**
	 * @param subComponent
	 *            the subComponent to set
	 */
	public void setSubComponent(String subComponent) {
		this.subComponent = subComponent;
	}
	
	/**
	 * @param partNumber
	 *            the partNumber to set
	 */
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	
	/**
	 * @param partDescription
	 *            the partDescription to set
	 */
	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
	
	/**
	 * @param bomId
	 *            the bomId to set
	 */
	public void setBomId(String bomId) {
		this.bomId = bomId;
	}
	
	/**
	 * @param family
	 *            the family to set
	 */
	public void setFamily(String family) {
		this.family = family;
	}
	
	/**
	 * @param sterility
	 *            the sterility to set
	 */
	public void setSterility(String sterility) {
		this.sterility = sterility;
	}
	
	/**
	 * @param strEditAccess
	 *            the strEditAccess to set
	 */
	public void setStrEditAccess(String strEditAccess) {
		this.strEditAccess = strEditAccess;
	}
	
	/**
	 * @param strPTMAPHtml
	 *            the strPTMAPHtml to set
	 */
	public void setStrPTMAPHtml(String strPTMAPHtml) {
		this.strPTMAPHtml = strPTMAPHtml;
	}
	
	/**
	 * @param inputString
	 *            the inputString to set
	 */
	public void setInputString(String inputString) {
		this.inputString = inputString;
	}
	
	/**
	 * @param strOpt
	 *            the strOpt to set
	 */
	public void setStrOpt(String strOpt) {
		this.strOpt = strOpt;
	}
	
	/**
	 * @param strPartNumbers
	 *            the strPartNumbers to set
	 */
	public void setStrPartNumbers(String strPartNumbers) {
		this.strPartNumbers = strPartNumbers;
	}
	
	/**
	 * @param voidinputstring
	 *            the voidinputstring to set
	 */
	public void setVoidinputstring(String voidinputstring) {
		this.voidinputstring = voidinputstring;
	}
	
	/**
	 * @param bomFlag
	 *            the bomFlag to set
	 */
	public void setBomFlag(String bomFlag) {
		this.bomFlag = bomFlag;
	}
	
	/**
	 * @param bomIds
	 *            the bomIds to set
	 */
	public void setBomIds(String bomIds) {
		this.bomIds = bomIds;
	}
	
	/**
	 * @param alSubComp
	 *            the alSubComp to set
	 */
	public void setAlSubComp(ArrayList alSubComp) {
		this.alSubComp = alSubComp;
	}
	
	/**
	 * @param alPartType
	 *            the alPartType to set
	 */
	public void setAlPartType(ArrayList alPartType) {
		this.alPartType = alPartType;
	}

	 /**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
	
}
