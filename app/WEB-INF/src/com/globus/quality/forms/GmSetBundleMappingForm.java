/**
 * 
 */
package com.globus.quality.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;
import org.apache.commons.beanutils.RowSetDynaClass;
/**
 * @author Agilan
 *
 */
public class GmSetBundleMappingForm extends GmLogForm{

	private static final long serialVersionUID = 1L;
	private int keyCount;
	private String gridXmlData = "";
	private String strBundleId = "";
	private String tabToLoad = "";
	private RowSetDynaClass rdHistory = null;
	private String searchstrBundleId = "";
	
	public String getSearchstrBundleId() {
		return searchstrBundleId;
	}
	public void setSearchstrBundleId(String searchstrBundleId) {
		this.searchstrBundleId = searchstrBundleId;
	}
	public String getStrBundleId() {
		return strBundleId;
	}
	public void setStrBundleId(String strBundleId) {
		this.strBundleId = strBundleId;
	}
	
	public String getTabToLoad() {
		return tabToLoad;
	}
	public void setTabToLoad(String tabToLoad) {
		this.tabToLoad = tabToLoad;
	}

	public RowSetDynaClass getRdHistory() {
		return rdHistory;
	}
	public void setRdHistory(RowSetDynaClass rdHistory) {
		this.rdHistory = rdHistory;
	}

	private ArrayList alResult = new ArrayList ();
	
	public ArrayList getAlResult() {
		return alResult;
	}
	public void setAlResult(ArrayList alResult) {
		this.alResult = alResult;
	}
	
	public int getKeyCount() {
		return keyCount;
	}
	public void setKeyCount(int keyCount) {
		this.keyCount = keyCount;
	}
	private String old_BundleId = "";
	public String getOld_BundleId() {
		return old_BundleId;
	}
	public void setOld_BundleId(String old_BundleId) {
		this.old_BundleId = old_BundleId;
	}

	private String txtSetId = "";
	private String txtSetName = "";
	private String txtSetDesc="";
	private String txtSetDetailDesc="";
	private String txtKeywords="";
	private String divName="";

	private String stsName="";  
	private ArrayList alSetStatus = new ArrayList ();
	private ArrayList alDivList = new ArrayList ();
	private ArrayList alResultSet = new ArrayList ();
	private ArrayList aList = new ArrayList ();
	public String getTxtSetName() {
		return txtSetName;
	}
	public void setTxtSetName(String txtSetName) {
		this.txtSetName = txtSetName;
	}

	public String getDivName() {
		return divName;
	}
	public void setDivName(String divName) {
		this.divName = divName;
	}
	
	public String getTxtSetId() {
		return txtSetId;
	}
	public void setTxtSetId(String txtSetId) {
		this.txtSetId = txtSetId;
	}
	
	public String getTxtSetDesc() {
		return txtSetDesc;
	}
	public void setTxtSetDesc(String txtSetDesc) {
		this.txtSetDesc = txtSetDesc;
	}
	
	public String getTxtSetDetailDesc() {
		return txtSetDetailDesc;
	}
	public void setTxtSetDetailDesc(String txtSetDetailDesc) {
		this.txtSetDetailDesc = txtSetDetailDesc;
	}
	
	public String getTxtKeywords() {
		return txtKeywords;
	}
	public void setTxtKeywords(String txtKeywords) {
		this.txtKeywords = txtKeywords;
	}
	

	public ArrayList getAlDivList() {
		return alDivList;
	}
	public void setAlDivList(ArrayList alDivList) {
		this.alDivList = alDivList;
	}
	
	public ArrayList getAlResultSet() {
		return alResultSet;
	}
	public void setAlResultSet(ArrayList alResultSet) {
		this.alResultSet = alResultSet;
	}
  
	public ArrayList getAlSetStatus() {
		return alSetStatus;
	}
	public void setAlSetStatus(ArrayList alSetStatus) {
		this.alSetStatus = alSetStatus;
	}
	public String getStsName() {
		return stsName;
	}
	public void setStsName(String stsName) {
		this.stsName = stsName;
	} 
	public String getGridXmlData() {
		return gridXmlData;
	}
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	public ArrayList getaList() {
		return aList;
	}
	public void setaList(ArrayList aList) {
		this.aList = aList;
	}
	
}
