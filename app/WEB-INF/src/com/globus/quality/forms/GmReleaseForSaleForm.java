package com.globus.quality.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmReleaseForSaleForm extends GmCommonForm {
	
	private String  strXMLGrid = "";
	private String  rfsStatus = "";
	
	private ArrayList alRFSStatus = new ArrayList();
	private ArrayList alRFSCountry = new ArrayList();
	private String msg = "";
	private String invalidParts = "";
	private String comments = "";
	private String hChkRFSCountry = "";
	private String hPartNos = "";
	
	public String gethPartNos() {
		return hPartNos;
	}
	public void sethPartNos(String hPartNos) {
		this.hPartNos = hPartNos;
	}
	public String getRfsStatus() {
		return rfsStatus;
	}
	public void setRfsStatus(String rfsStatus) {
		this.rfsStatus = rfsStatus;
	}
	public String getStrXMLGrid() {
		return strXMLGrid;
	}
	public String gethChkRFSCountry() {
		return hChkRFSCountry;
	}
	public void sethChkRFSCountry(String hChkRFSCountry) {
		this.hChkRFSCountry = hChkRFSCountry;
	}
	public void setStrXMLGrid(String strXMLGrid) {
		this.strXMLGrid = strXMLGrid;
	}
	public ArrayList getAlRFSStatus() {
		return alRFSStatus;
	}
	public void setAlRFSStatus(ArrayList alRFSStatus) {
		this.alRFSStatus = alRFSStatus;
	}
	public ArrayList getAlRFSCountry() {
		return alRFSCountry;
	}
	public void setAlRFSCountry(ArrayList alRFSCountry) {
		this.alRFSCountry = alRFSCountry;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getInvalidParts() {
		return invalidParts;
	}
	public void setInvalidParts(String invalidParts) {
		this.invalidParts = invalidParts;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
}