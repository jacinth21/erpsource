package com.globus.quality.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;

public class GmSetBundleRptForm extends GmLogForm{
	  //private ArrayList alStatus = new ArrayList();

	private ArrayList alSetBundleName = new ArrayList();
	private ArrayList alSetId = new ArrayList();
	private ArrayList alDivList = new ArrayList();
	private ArrayList alStatus = new ArrayList();
	private ArrayList alRptDetails = new ArrayList();
	private String status = "";
	private String divName = "";
	private String setId = "";
	private String setBundleName = "";
	private String searchsetBundleName = "";
	
	
 	public String getSearchsetBundleName() {
		return searchsetBundleName;
	}
	public void setSearchsetBundleName(String searchsetBundleName) {
		this.searchsetBundleName = searchsetBundleName;
	}
	public ArrayList getAlRptDetails() {
 		return alRptDetails;
 	}
 	public void setAlRptDetails(ArrayList alRptDetails) {
 		this.alRptDetails = alRptDetails;
 	}	
 	public ArrayList getAlSetBundleName() {
 		return alSetBundleName;
 	}
 	public void setAlSetBundleName(ArrayList alSetBundleName) {
 		this.alSetBundleName = alSetBundleName;
 	}
 	public ArrayList getAlSetId() {
 		return alSetId;
 	}
 	public void setAlSetId(ArrayList alSetId) {
 		this.alSetId = alSetId;
 	}
 	public ArrayList getAlDivList() {
 		return alDivList;
 	}
 	public void setAlDivList(ArrayList alDivList) {
 		this.alDivList = alDivList;
 	}
 	public ArrayList getAlStatus() {
 		return alStatus;
 	}
 	public void setAlStatus(ArrayList alStatus) {
 		this.alStatus = alStatus;
 	}
 	public String getStatus() {
 		return status;
 	}
 	public void setStatus(String status) {
 		this.status = status;
 	}
 	public String getDivName() {
 		return divName;
 	}
 	public void setDivName(String divName) {
 		this.divName = divName;
 	}
 	public String getSetId() {
 		return setId;
 	}
 	public void setSetId(String setId) {
 		this.setId = setId;
 	}
 	public String getSetBundleName() {
 		return setBundleName;
 	}
 	public void setSetBundleName(String setBundleName) {
 		this.setBundleName = setBundleName;
 	}
 
}
