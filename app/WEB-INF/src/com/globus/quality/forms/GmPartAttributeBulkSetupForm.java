package com.globus.quality.forms;

import java.util.ArrayList;

import com.globus.common.forms.GmLogForm;



public class GmPartAttributeBulkSetupForm extends GmLogForm {
  private String partNums = "";
  private String attributeType = "";
  private String attributeValue = "";
  private String chkAllcompany = "";
  private String gridData = "";
  private String screenType = "";
  private String strInvalidParts = "";
  private String accessAllComp = "";
  private String partFl="";
  private String jsonString="";
  private String setId = "";
  private String accessVoidfl = "";


public String getAccessVoidfl() {
	return accessVoidfl;
}

public void setAccessVoidfl(String accessVoidfl) {
	this.accessVoidfl = accessVoidfl;
}

public String getSetId() {
	return setId;
}

public void setSetId(String setId) {
	this.setId = setId;
}

public String getJsonString() {
	return jsonString;
}

public void setJsonString(String jsonString) {
	this.jsonString = jsonString;
}

public String getPartFl() {
	return partFl;
}

public void setPartFl(String partFl) {
	this.partFl = partFl;
}

/**
   * @return the accessAllComp
   */
  public String getAccessAllComp() {
    return accessAllComp;
  }

  /**
   * @param accessAllComp the accessAllComp to set
   */
  public void setAccessAllComp(String accessAllComp) {
    this.accessAllComp = accessAllComp;
  }

  /**
   * @return the partNums
   */
  public String getPartNums() {
    return partNums;
  }

  /**
   * @param partNums the partNums to set
   */
  public void setPartNums(String partNums) {
    this.partNums = partNums;
  }

  /**
   * @return the screenType
   */
  public String getScreenType() {
    return screenType;
  }

  /**
   * @param screenType the screenType to set
   */
  public void setScreenType(String screenType) {
    this.screenType = screenType;
  }



  public String getAttributeValue() {
    return attributeValue;
  }

  public void setAttributeValue(String attributeValue) {
    this.attributeValue = attributeValue;
  }

  public String getChkAllcompany() {
    return chkAllcompany;
  }

  public void setChkAllcompany(String chkAllcompany) {
    this.chkAllcompany = chkAllcompany;
  }

  public String getGridData() {
    return gridData;
  }

  public void setGridData(String gridData) {
    this.gridData = gridData;
  }

  /**
   * @return the attributeType
   */
  public String getAttributeType() {
    return attributeType;
  }

  /**
   * @param attributeType the attributeType to set
   */
  public void setAttributeType(String attributeType) {
    this.attributeType = attributeType;
  }

 

  /**
   * @return the alAttrType
   */

  private ArrayList alAttrType = new ArrayList();

  public ArrayList getAlAttrType() {
    return alAttrType;
  }

  /**
   * @param alAttrType the alAttrType to set
   */
  public void setAlAttrType(ArrayList alAttrType) {
    this.alAttrType = alAttrType;
  }


  private ArrayList alSystemType = new ArrayList();



  
  public ArrayList getAlSystemType() {
	return alSystemType;
}

public void setAlSystemType(ArrayList alSystemType) {
	this.alSystemType = alSystemType;
}

/**
   * @return the strInvalidParts
   */
  public String getStrInvalidParts() {
    return strInvalidParts;
  }

  /**
   * @param strInvalidParts the strInvalidParts to set
   */
  public void setStrInvalidParts(String strInvalidParts) {
    this.strInvalidParts = strInvalidParts;
  }



}
