
package com.globus.quality.forms;

import java.util.ArrayList;
import java.util.List;

import com.globus.common.forms.GmLogForm;

/**
 * @author Author
 *
 */
public class GmSetMappingForm extends GmLogForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String systemId = "";
	private String setId = "";
	private String setDesc = "";
	private String seType = "";
	private String setStatus = "";	
	private String setType = "";
	private String setBaseLine = "";
	private String setShared = "";
	private String gridData = "";
	private String setGroup= "";
	private String mapAccessFlag= "";	
	private String stdsetidsInputStr = "";
	private String addlsetidsInputStr = "";
	private String loanerstdsetidsInputStr = "";
	private String loaneraddlsetidsInputStr = "";
	private ArrayList alSetType = new ArrayList();
	private ArrayList alSetMapDtls = new ArrayList();
	private ArrayList alSetShareType = new ArrayList();
	private String txtSetName = "";
	private String old_BundleId = "";
	
	public String getTxtSetName() {
		return txtSetName;
	}
	public void setTxtSetName(String txtSetName) {
		this.txtSetName = txtSetName;
	}
	public String getOld_BundleId() {
		return old_BundleId;
	}
	public void setOld_BundleId(String old_BundleId) {
		this.old_BundleId = old_BundleId;
	}
	public String getSetDesc() {
		return setDesc;
	}
	public void setSetDesc(String setDesc) {
		this.setDesc = setDesc;
	}
	public String getSeType() {
		return seType;
	}
	public void setSeType(String seType) {
		this.seType = seType;
	}

	public String getSetStatus() {
		return setStatus;
	}
	public void setSetStatus(String setStatus) {
		this.setStatus = setStatus;
	}
	public String getSetId() {
		return setId;
	}
	public void setSetId(String setId) {
		this.setId = setId;
	}
	public ArrayList getAlSetType() {
		return alSetType;
	}
	public void setAlSetType(ArrayList alSetType) {
		this.alSetType = alSetType;
	}
	public ArrayList getAlSetShareType() {
		return alSetShareType;
	}
	public void setAlSetShareType(ArrayList alSetShareType) {
		this.alSetShareType = alSetShareType;
	}
	public String getStdsetidsInputStr() {
		return stdsetidsInputStr;
	}
	public void setStdsetidsInputStr(String stdsetidsInputStr) {
		this.stdsetidsInputStr = stdsetidsInputStr;
	}
	public String getAddlsetidsInputStr() {
		return addlsetidsInputStr;
	}
	public void setAddlsetidsInputStr(String addlsetidsInputStr) {
		this.addlsetidsInputStr = addlsetidsInputStr;
	}
	public String getLoanerstdsetidsInputStr() {
		return loanerstdsetidsInputStr;
	}
	public void setLoanerstdsetidsInputStr(String loanerstdsetidsInputStr) {
		this.loanerstdsetidsInputStr = loanerstdsetidsInputStr;
	}
	public String getLoaneraddlsetidsInputStr() {
		return loaneraddlsetidsInputStr;
	}
	public void setLoaneraddlsetidsInputStr(String loaneraddlsetidsInputStr) {
		this.loaneraddlsetidsInputStr = loaneraddlsetidsInputStr;
	}
	public String getSetType() {
		return setType;
	}
	public void setSetType(String setType) {
		this.setType = setType;
	}
	public String getSetBaseLine() {
		return setBaseLine;
	}
	public void setSetBaseLine(String setBaseLine) {
		this.setBaseLine = setBaseLine;
	}
	public String getSetShared() {
		return setShared;
	}
	public void setSetShared(String setShared) {
		this.setShared = setShared;
	}
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public ArrayList getAlSetMapDtls() {
		return alSetMapDtls;
	}
	public void setAlSetMapDtls(ArrayList alSetMapDtls) {
		this.alSetMapDtls = alSetMapDtls;
	}
	public String getGridData() {
		return gridData;
	}
	public void setGridData(String gridData) {
		this.gridData = gridData;
	}
	public String getSetGroup() {
		return setGroup;
	}
	public void setSetGroup(String setGroup) {
		this.setGroup = setGroup;
	}	
	public String getMapAccessFlag() {
		return mapAccessFlag;
	}
	public void setMapAccessFlag(String mapAccessFlag) {
		this.mapAccessFlag = mapAccessFlag;
	}
		
}

