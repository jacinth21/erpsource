package com.globus.quality.forms;

import java.util.ArrayList;
import java.util.HashMap;

import com.globus.common.forms.GmLogForm;

public class GmPartParameterDtlsForm extends GmLogForm {

  private String partNumber = "";
  private String diNumberFl = "";
  private String diNumber = "";
  private String materialCert = "";
  private String handnessCert = "";
  private String primaryPack = "";
  private String secondaryPack = "";
  private String complianceCert = "";
  private String issuingAgencyConf = "";
  private String woCreated = "";
  private String udiEtchReq = "";
  private String diOnlyEtchReq = "";
  private String piOnlyEtchReq = "";
  private String vendorDesign = "";
  private String bulkPackage = "";
  private String inputStr = "";
  private String partStatus = "";

  private String mriSafetyInformation = "";
  private String packageDisconDt = "";
  private String overrideCompanyNm = "";
  private String lotOrBatchNum = "";
  private String serialNum = "";
  private String manufaturingDt = "";
  private String expirationDt = "";
  private String donationIdenNum = "";

  private String subjectDmExempt = "";
  private String dmDiPrimaryDi = "";
  private String dmDiNumber = "";
  private String hctp = "";
  private String singleUse = "";
  private String devPkgSterile = "";
  // private String mriSafetyInfo = "";

  private String deviceCount = "";
  private String unitUseDiNumber = "";
  private String packDiNumber = "";
  private String quantityPerPack = "";
  private String containsDiPack = "";
  private String dunsNumber = "";
  private String custContactPhone = "";
  private String custContactMail = "";
  private String sterilePart = "";
  private String samplePattern = "";

  private String regulatoryPartNum = "";
  private String projectId = "";
  private String submissionNo = "";
  private String gridXml = "";
  private String buttonAccess = "";
  private String regulatoryPartDesc = "";
  private String editDisable = "";


  private ArrayList alMaterialCert = new ArrayList();
  private ArrayList alHandnessCert = new ArrayList();
  private ArrayList alPrimaryPack = new ArrayList();
  private ArrayList alSecondaryPack = new ArrayList();
  private ArrayList alComplianceCert = new ArrayList();
  private ArrayList alWoCreated = new ArrayList();
  private ArrayList alUDIEtchRequired = new ArrayList();
  private ArrayList alDIOnlyEtchRequired = new ArrayList();
  private ArrayList alPIOnlyEtchRequired = new ArrayList();
  private ArrayList alVendorDesign = new ArrayList();
  private ArrayList alIssuingAgencyConf = new ArrayList();
  private ArrayList alBulkPackage = new ArrayList();

  private ArrayList alMriSafetyInfo = new ArrayList();
  private ArrayList alOverrideCompanyNm = new ArrayList();
  private ArrayList alLotOrBatchNum = new ArrayList();
  private ArrayList alSerialNum = new ArrayList();
  private ArrayList alManufaturingDt = new ArrayList();
  private ArrayList alExpirationDt = new ArrayList();
  private ArrayList alDonationIdenNum = new ArrayList();
  private ArrayList alProjectId = new ArrayList();

  HashMap hmAttrVal = new HashMap();
  HashMap hmResult = new HashMap();


  /**
   * @return the buttonAccess
   */
  public String getButtonAccess() {
    return buttonAccess;
  }

  /**
   * @param buttonAccess the buttonAccess to set
   */
  public void setButtonAccess(String buttonAccess) {
    this.buttonAccess = buttonAccess;
  }

  /**
   * @return the partNumber
   */
  public String getPartNumber() {
    return partNumber;
  }

  /**
   * @param partNumber the partNumber to set
   */
  public void setPartNumber(String partNumber) {
    this.partNumber = partNumber;
  }

  /**
   * @return the diNumberFl
   */
  public String getDiNumberFl() {
    return diNumberFl;
  }

  /**
   * @param diNumberFl the diNumberFl to set
   */
  public void setDiNumberFl(String diNumberFl) {
    this.diNumberFl = diNumberFl;
  }

  /**
   * @return the diNumber
   */
  public String getDiNumber() {
    return diNumber;
  }

  /**
   * @param diNumber the diNumber to set
   */
  public void setDiNumber(String diNumber) {
    this.diNumber = diNumber;
  }

  /**
   * @return the materialCert
   */
  public String getMaterialCert() {
    return materialCert;
  }

  /**
   * @param materialCert the materialCert to set
   */
  public void setMaterialCert(String materialCert) {
    this.materialCert = materialCert;
  }

  /**
   * @return the handnessCert
   */
  public String getHandnessCert() {
    return handnessCert;
  }

  /**
   * @param handnessCert the handnessCert to set
   */
  public void setHandnessCert(String handnessCert) {
    this.handnessCert = handnessCert;
  }

  /**
   * @return the primaryPack
   */
  public String getPrimaryPack() {
    return primaryPack;
  }

  /**
   * @param primaryPack the primaryPack to set
   */
  public void setPrimaryPack(String primaryPack) {
    this.primaryPack = primaryPack;
  }

  /**
   * @return the secondaryPack
   */
  public String getSecondaryPack() {
    return secondaryPack;
  }

  /**
   * @param secondaryPack the secondaryPack to set
   */
  public void setSecondaryPack(String secondaryPack) {
    this.secondaryPack = secondaryPack;
  }

  /**
   * @return the complianceCert
   */
  public String getComplianceCert() {
    return complianceCert;
  }

  /**
   * @param complianceCert the complianceCert to set
   */
  public void setComplianceCert(String complianceCert) {
    this.complianceCert = complianceCert;
  }

  /**
   * @return the issuingAgencyConf
   */
  public String getIssuingAgencyConf() {
    return issuingAgencyConf;
  }

  /**
   * @param issuingAgencyConf the issuingAgencyConf to set
   */
  public void setIssuingAgencyConf(String issuingAgencyConf) {
    this.issuingAgencyConf = issuingAgencyConf;
  }

  /**
   * @return the woCreated
   */
  public String getWoCreated() {
    return woCreated;
  }

  /**
   * @param woCreated the woCreated to set
   */
  public void setWoCreated(String woCreated) {
    this.woCreated = woCreated;
  }

  /**
   * @return the udiEtchReq
   */
  public String getUdiEtchReq() {
    return udiEtchReq;
  }

  /**
   * @param udiEtchReq the udiEtchReq to set
   */
  public void setUdiEtchReq(String udiEtchReq) {
    this.udiEtchReq = udiEtchReq;
  }

  /**
   * @return the diOnlyEtchReq
   */
  public String getDiOnlyEtchReq() {
    return diOnlyEtchReq;
  }

  /**
   * @param diOnlyEtchReq the diOnlyEtchReq to set
   */
  public void setDiOnlyEtchReq(String diOnlyEtchReq) {
    this.diOnlyEtchReq = diOnlyEtchReq;
  }

  /**
   * @return the piOnlyEtchReq
   */
  public String getPiOnlyEtchReq() {
    return piOnlyEtchReq;
  }

  /**
   * @param piOnlyEtchReq the piOnlyEtchReq to set
   */
  public void setPiOnlyEtchReq(String piOnlyEtchReq) {
    this.piOnlyEtchReq = piOnlyEtchReq;
  }

  /**
   * @return the vendorDesign
   */
  public String getVendorDesign() {
    return vendorDesign;
  }

  /**
   * @param vendorDesign the vendorDesign to set
   */
  public void setVendorDesign(String vendorDesign) {
    this.vendorDesign = vendorDesign;
  }

  /**
   * @return the bulkPackage
   */
  public String getBulkPackage() {
    return bulkPackage;
  }

  /**
   * @param bulkPackage the bulkPackage to set
   */
  public void setBulkPackage(String bulkPackage) {
    this.bulkPackage = bulkPackage;
  }

  /**
   * @return the inputStr
   */
  public String getInputStr() {
    return inputStr;
  }

  /**
   * @param inputStr the inputStr to set
   */
  public void setInputStr(String inputStr) {
    this.inputStr = inputStr;
  }

  /**
   * @return the partStatus
   */
  public String getPartStatus() {
    return partStatus;
  }

  /**
   * @param partStatus the partStatus to set
   */
  public void setPartStatus(String partStatus) {
    this.partStatus = partStatus;
  }

  /**
   * @return the alMaterialCert
   */
  public ArrayList getAlMaterialCert() {
    return alMaterialCert;
  }

  /**
   * @param alMaterialCert the alMaterialCert to set
   */
  public void setAlMaterialCert(ArrayList alMaterialCert) {
    this.alMaterialCert = alMaterialCert;
  }

  /**
   * @return the alHandnessCert
   */
  public ArrayList getAlHandnessCert() {
    return alHandnessCert;
  }

  /**
   * @param alHandnessCert the alHandnessCert to set
   */
  public void setAlHandnessCert(ArrayList alHandnessCert) {
    this.alHandnessCert = alHandnessCert;
  }

  /**
   * @return the alPrimaryPack
   */
  public ArrayList getAlPrimaryPack() {
    return alPrimaryPack;
  }

  /**
   * @param alPrimaryPack the alPrimaryPack to set
   */
  public void setAlPrimaryPack(ArrayList alPrimaryPack) {
    this.alPrimaryPack = alPrimaryPack;
  }

  /**
   * @return the alSecondaryPack
   */
  public ArrayList getAlSecondaryPack() {
    return alSecondaryPack;
  }

  /**
   * @param alSecondaryPack the alSecondaryPack to set
   */
  public void setAlSecondaryPack(ArrayList alSecondaryPack) {
    this.alSecondaryPack = alSecondaryPack;
  }

  /**
   * @return the alComplianceCert
   */
  public ArrayList getAlComplianceCert() {
    return alComplianceCert;
  }

  /**
   * @param alComplianceCert the alComplianceCert to set
   */
  public void setAlComplianceCert(ArrayList alComplianceCert) {
    this.alComplianceCert = alComplianceCert;
  }

  /**
   * @return the alWoCreated
   */
  public ArrayList getAlWoCreated() {
    return alWoCreated;
  }

  /**
   * @param alWoCreated the alWoCreated to set
   */
  public void setAlWoCreated(ArrayList alWoCreated) {
    this.alWoCreated = alWoCreated;
  }

  /**
   * @return the alUDIEtchRequired
   */
  public ArrayList getAlUDIEtchRequired() {
    return alUDIEtchRequired;
  }

  /**
   * @param alUDIEtchRequired the alUDIEtchRequired to set
   */
  public void setAlUDIEtchRequired(ArrayList alUDIEtchRequired) {
    this.alUDIEtchRequired = alUDIEtchRequired;
  }

  /**
   * @return the alDIOnlyEtchRequired
   */
  public ArrayList getAlDIOnlyEtchRequired() {
    return alDIOnlyEtchRequired;
  }

  /**
   * @param alDIOnlyEtchRequired the alDIOnlyEtchRequired to set
   */
  public void setAlDIOnlyEtchRequired(ArrayList alDIOnlyEtchRequired) {
    this.alDIOnlyEtchRequired = alDIOnlyEtchRequired;
  }

  /**
   * @return the alPIOnlyEtchRequired
   */
  public ArrayList getAlPIOnlyEtchRequired() {
    return alPIOnlyEtchRequired;
  }

  /**
   * @param alPIOnlyEtchRequired the alPIOnlyEtchRequired to set
   */
  public void setAlPIOnlyEtchRequired(ArrayList alPIOnlyEtchRequired) {
    this.alPIOnlyEtchRequired = alPIOnlyEtchRequired;
  }

  /**
   * @return the alVendorDesign
   */
  public ArrayList getAlVendorDesign() {
    return alVendorDesign;
  }

  /**
   * @param alVendorDesign the alVendorDesign to set
   */
  public void setAlVendorDesign(ArrayList alVendorDesign) {
    this.alVendorDesign = alVendorDesign;
  }

  /**
   * @return the alIssuingAgencyConf
   */
  public ArrayList getAlIssuingAgencyConf() {
    return alIssuingAgencyConf;
  }

  /**
   * @param alIssuingAgencyConf the alIssuingAgencyConf to set
   */
  public void setAlIssuingAgencyConf(ArrayList alIssuingAgencyConf) {
    this.alIssuingAgencyConf = alIssuingAgencyConf;
  }

  /**
   * @return the alBulkPackage
   */
  public ArrayList getAlBulkPackage() {
    return alBulkPackage;
  }

  /**
   * @param alBulkPackage the alBulkPackage to set
   */
  public void setAlBulkPackage(ArrayList alBulkPackage) {
    this.alBulkPackage = alBulkPackage;
  }

  /**
   * @return the mriSafetyInformation
   */
  public String getMriSafetyInformation() {
    return mriSafetyInformation;
  }

  /**
   * @param mriSafetyInformation the mriSafetyInformation to set
   */
  public void setMriSafetyInformation(String mriSafetyInformation) {
    this.mriSafetyInformation = mriSafetyInformation;
  }

  /**
   * @return the packageDisconDt
   */
  public String getPackageDisconDt() {
    return packageDisconDt;
  }

  /**
   * @param packageDisconDt the packageDisconDt to set
   */
  public void setPackageDisconDt(String packageDisconDt) {
    this.packageDisconDt = packageDisconDt;
  }

  /**
   * @return the overrideCompanyNm
   */
  public String getOverrideCompanyNm() {
    return overrideCompanyNm;
  }

  /**
   * @param overrideCompanyNm the overrideCompanyNm to set
   */
  public void setOverrideCompanyNm(String overrideCompanyNm) {
    this.overrideCompanyNm = overrideCompanyNm;
  }

  /**
   * @return the lotOrBatchNum
   */
  public String getLotOrBatchNum() {
    return lotOrBatchNum;
  }

  /**
   * @param lotOrBatchNum the lotOrBatchNum to set
   */
  public void setLotOrBatchNum(String lotOrBatchNum) {
    this.lotOrBatchNum = lotOrBatchNum;
  }

  /**
   * @return the serialNum
   */
  public String getSerialNum() {
    return serialNum;
  }

  /**
   * @param serialNum the serialNum to set
   */
  public void setSerialNum(String serialNum) {
    this.serialNum = serialNum;
  }

  /**
   * @return the manufaturingDt
   */
  public String getManufaturingDt() {
    return manufaturingDt;
  }

  /**
   * @param manufaturingDt the manufaturingDt to set
   */
  public void setManufaturingDt(String manufaturingDt) {
    this.manufaturingDt = manufaturingDt;
  }

  /**
   * @return the expirationDt
   */
  public String getExpirationDt() {
    return expirationDt;
  }

  /**
   * @param expirationDt the expirationDt to set
   */
  public void setExpirationDt(String expirationDt) {
    this.expirationDt = expirationDt;
  }

  /**
   * @return the donationIdenNum
   */
  public String getDonationIdenNum() {
    return donationIdenNum;
  }

  /**
   * @param donationIdenNum the donationIdenNum to set
   */
  public void setDonationIdenNum(String donationIdenNum) {
    this.donationIdenNum = donationIdenNum;
  }

  /**
   * @return the alMriSafetyInfo
   */
  public ArrayList getAlMriSafetyInfo() {
    return alMriSafetyInfo;
  }

  /**
   * @param alMriSafetyInfo the alMriSafetyInfo to set
   */
  public void setAlMriSafetyInfo(ArrayList alMriSafetyInfo) {
    this.alMriSafetyInfo = alMriSafetyInfo;
  }

  /**
   * @return the alOverrideCompanyNm
   */
  public ArrayList getAlOverrideCompanyNm() {
    return alOverrideCompanyNm;
  }

  /**
   * @param alOverrideCompanyNm the alOverrideCompanyNm to set
   */
  public void setAlOverrideCompanyNm(ArrayList alOverrideCompanyNm) {
    this.alOverrideCompanyNm = alOverrideCompanyNm;
  }

  /**
   * @return the alLotOrBatchNum
   */
  public ArrayList getAlLotOrBatchNum() {
    return alLotOrBatchNum;
  }

  /**
   * @param alLotOrBatchNum the alLotOrBatchNum to set
   */
  public void setAlLotOrBatchNum(ArrayList alLotOrBatchNum) {
    this.alLotOrBatchNum = alLotOrBatchNum;
  }

  /**
   * @return the alSerialNum
   */
  public ArrayList getAlSerialNum() {
    return alSerialNum;
  }

  /**
   * @param alSerialNum the alSerialNum to set
   */
  public void setAlSerialNum(ArrayList alSerialNum) {
    this.alSerialNum = alSerialNum;
  }

  /**
   * @return the alManufaturingDt
   */
  public ArrayList getAlManufaturingDt() {
    return alManufaturingDt;
  }

  /**
   * @param alManufaturingDt the alManufaturingDt to set
   */
  public void setAlManufaturingDt(ArrayList alManufaturingDt) {
    this.alManufaturingDt = alManufaturingDt;
  }

  /**
   * @return the alExpirationDt
   */
  public ArrayList getAlExpirationDt() {
    return alExpirationDt;
  }

  /**
   * @param alExpirationDt the alExpirationDt to set
   */
  public void setAlExpirationDt(ArrayList alExpirationDt) {
    this.alExpirationDt = alExpirationDt;
  }

  /**
   * @return the alDonationIdenNum
   */
  public ArrayList getAlDonationIdenNum() {
    return alDonationIdenNum;
  }

  /**
   * @param alDonationIdenNum the alDonationIdenNum to set
   */
  public void setAlDonationIdenNum(ArrayList alDonationIdenNum) {
    this.alDonationIdenNum = alDonationIdenNum;
  }

  /**
   * @return the hmAttrVal
   */
  public HashMap getHmAttrVal() {
    return hmAttrVal;
  }

  /**
   * @param hmAttrVal the hmAttrVal to set
   */
  public void setHmAttrVal(HashMap hmAttrVal) {
    this.hmAttrVal = hmAttrVal;
  }

  /**
   * @param hmResult the hmResult to set
   */

  public HashMap getHmResult() {
    return hmResult;
  }

  /**
   * @param hmResult the hmResult to set
   */
  public void setHmResult(HashMap hmResult) {
    this.hmResult = hmResult;
  }

  /**
   * @return the subjectDmExempt
   */
  public String getSubjectDmExempt() {
    return subjectDmExempt;
  }

  /**
   * @param subjectDmExempt the subjectDmExempt to set
   */
  public void setSubjectDmExempt(String subjectDmExempt) {
    this.subjectDmExempt = subjectDmExempt;
  }

  /**
   * @return the dmDiPrimaryDi
   */
  public String getDmDiPrimaryDi() {
    return dmDiPrimaryDi;
  }

  /**
   * @param dmDiPrimaryDi the dmDiPrimaryDi to set
   */
  public void setDmDiPrimaryDi(String dmDiPrimaryDi) {
    this.dmDiPrimaryDi = dmDiPrimaryDi;
  }

  /**
   * @return the dmDiNumber
   */
  public String getDmDiNumber() {
    return dmDiNumber;
  }

  /**
   * @param dmDiNumber the dmDiNumber to set
   */
  public void setDmDiNumber(String dmDiNumber) {
    this.dmDiNumber = dmDiNumber;
  }

  /**
   * @return the hctp
   */
  public String getHctp() {
    return hctp;
  }

  /**
   * @param hctp the hctp to set
   */
  public void setHctp(String hctp) {
    this.hctp = hctp;
  }

  /**
   * @return the singleUse
   */
  public String getSingleUse() {
    return singleUse;
  }

  /**
   * @param singleUse the singleUse to set
   */
  public void setSingleUse(String singleUse) {
    this.singleUse = singleUse;
  }

  /**
   * @return the devPkgSterile
   */
  public String getDevPkgSterile() {
    return devPkgSterile;
  }

  /**
   * @param devPkgSterile the devPkgSterile to set
   */
  public void setDevPkgSterile(String devPkgSterile) {
    this.devPkgSterile = devPkgSterile;
  }

  /**
   * @return the deviceCount
   */
  public String getDeviceCount() {
    return deviceCount;
  }

  /**
   * @param deviceCount the deviceCount to set
   */
  public void setDeviceCount(String deviceCount) {
    this.deviceCount = deviceCount;
  }

  /**
   * @return the unitUseDiNumber
   */
  public String getUnitUseDiNumber() {
    return unitUseDiNumber;
  }

  /**
   * @param unitUseDiNumber the unitUseDiNumber to set
   */
  public void setUnitUseDiNumber(String unitUseDiNumber) {
    this.unitUseDiNumber = unitUseDiNumber;
  }

  /**
   * @return the packDiNumber
   */
  public String getPackDiNumber() {
    return packDiNumber;
  }

  /**
   * @param packDiNumber the packDiNumber to set
   */
  public void setPackDiNumber(String packDiNumber) {
    this.packDiNumber = packDiNumber;
  }

  /**
   * @return the quantityPerPack
   */
  public String getQuantityPerPack() {
    return quantityPerPack;
  }

  /**
   * @param quantityPerPack the quantityPerPack to set
   */
  public void setQuantityPerPack(String quantityPerPack) {
    this.quantityPerPack = quantityPerPack;
  }

  /**
   * @return the containsDiPack
   */
  public String getContainsDiPack() {
    return containsDiPack;
  }

  /**
   * @param containsDiPack the containsDiPack to set
   */
  public void setContainsDiPack(String containsDiPack) {
    this.containsDiPack = containsDiPack;
  }

  /**
   * @return the dunsNumber
   */
  public String getDunsNumber() {
    return dunsNumber;
  }

  /**
   * @param dunsNumber the dunsNumber to set
   */
  public void setDunsNumber(String dunsNumber) {
    this.dunsNumber = dunsNumber;
  }

  /**
   * @return the custContactPhone
   */
  public String getCustContactPhone() {
    return custContactPhone;
  }

  /**
   * @param custContactPhone the custContactPhone to set
   */
  public void setCustContactPhone(String custContactPhone) {
    this.custContactPhone = custContactPhone;
  }

  /**
   * @return the custContactMail
   */
  public String getCustContactMail() {
    return custContactMail;
  }

  /**
   * @param custContactMail the custContactMail to set
   */
  public void setCustContactMail(String custContactMail) {
    this.custContactMail = custContactMail;
  }

  /**
   * @return the sterilePart
   */
  public String getSterilePart() {
    return sterilePart;
  }

  /**
   * @param sterilePart the sterilePart to set
   */
  public void setSterilePart(String sterilePart) {
    this.sterilePart = sterilePart;
  }

  /**
   * @return the samplePattern
   */
  public String getSamplePattern() {
    return samplePattern;
  }

  /**
   * @param samplePattern the samplePattern to set
   */
  public void setSamplePattern(String samplePattern) {
    this.samplePattern = samplePattern;
  }

  /**
   * @return the regulatoryPartNum
   */
  public String getRegulatoryPartNum() {
    return regulatoryPartNum;
  }

  /**
   * @param regulatoryPartNum the regulatoryPartNum to set
   */
  public void setRegulatoryPartNum(String regulatoryPartNum) {
    this.regulatoryPartNum = regulatoryPartNum;
  }

  /**
   * @return the regulatoryPartDesc
   */
  public String getRegulatoryPartDesc() {
    return regulatoryPartDesc;
  }

  /**
   * @param regulatoryPartDesc the regulatoryPartDesc to set
   */
  public void setRegulatoryPartDesc(String regulatoryPartDesc) {
    this.regulatoryPartDesc = regulatoryPartDesc;
  }

  /**
   * @return the submissionNo
   */
  public String getSubmissionNo() {
    return submissionNo;
  }

  /**
   * @param submissionNo the submissionNo to set
   */
  public void setSubmissionNo(String submissionNo) {
    this.submissionNo = submissionNo;
  }

  /**
   * @return the projectId
   */
  public String getProjectId() {
    return projectId;
  }

  /**
   * @param submissionNo the submissionNo to set
   */
  public void setProjectId(String projectId) {
    this.projectId = projectId;
  }

  /**
   * @return the gridXml
   */
  public String getGridXml() {
    return gridXml;
  }

  /**
   * @param gridXml the gridXml to set
   */
  public void setGridXml(String gridXml) {
    this.gridXml = gridXml;
  }

  /**
   * @return the alProjectId
   */
  public ArrayList getAlProjectId() {
    return alProjectId;
  }

  /**
   * @param alProjectId the alProjectId to set
   */
  public void setAlProjectId(ArrayList alProjectId) {
    this.alProjectId = alProjectId;
  }

  /**
   * @return the editDisable
   */
  public String getEditDisable() {
    return editDisable;
  }

  /**
   * @param editDisable the editDisable to set
   */
  public void setEditDisable(String editDisable) {
    this.editDisable = editDisable;
  }

}
