package com.globus.quality.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmPartProcessRptForm extends GmCommonForm {
	private String projectID 		= "";
	private String processID 		= "";
	private String systemID  		= "";
	private String partNum   		= "";
	private String vendorID  		= "";
	private String xmlGridData  	= "";
	
	private ArrayList alProjectList = new ArrayList();
	private ArrayList alVendorList  = new ArrayList();
	private ArrayList alSystemList  = new ArrayList();
	/**
	 * @return the projectID
	 */
	public String getProjectID() {
		return projectID;
	}
	/**
	 * @param projectID the projectID to set
	 */
	public void setProjectID(String projectID) {
		this.projectID = projectID;
	}
	/**
	 * @return the processID
	 */
	public String getProcessID() {
		return processID;
	}
	/**
	 * @param processID the processID to set
	 */
	public void setProcessID(String processID) {
		this.processID = processID;
	}
	/**
	 * @return the systemID
	 */
	public String getSystemID() {
		return systemID;
	}
	/**
	 * @param systemID the systemID to set
	 */
	public void setSystemID(String systemID) {
		this.systemID = systemID;
	}
	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}
	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}
	/**
	 * @return the vendorID
	 */
	public String getVendorID() {
		return vendorID;
	}
	/**
	 * @param vendorID the vendorID to set
	 */
	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}
	/**
	 * @return the xmlGridData
	 */
	public String getXmlGridData() {
		return xmlGridData;
	}
	/**
	 * @param xmlGridData the xmlGridData to set
	 */
	public void setXmlGridData(String xmlGridData) {
		this.xmlGridData = xmlGridData;
	}
	/**
	 * @return the alProjectList
	 */
	public ArrayList getAlProjectList() {
		return alProjectList;
	}
	/**
	 * @param alProjectList the alProjectList to set
	 */
	public void setAlProjectList(ArrayList alProjectList) {
		this.alProjectList = alProjectList;
	}
	/**
	 * @return the alSystemList
	 */
	public ArrayList getAlSystemList() {
		return alSystemList;
	}
	/**
	 * @param alSystemList the alSystemList to set
	 */
	public void setAlSystemList(ArrayList alSystemList) {
		this.alSystemList = alSystemList;
	}
	/**
	 * @return the alVendorList
	 */
	public ArrayList getAlVendorList() {
		return alVendorList;
	}
	/**
	 * @param alVendorList the alVendorList to set
	 */
	public void setAlVendorList(ArrayList alVendorList) {
		this.alVendorList = alVendorList;
	}
	
	
	
	
}