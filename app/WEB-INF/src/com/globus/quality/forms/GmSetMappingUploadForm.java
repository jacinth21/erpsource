package com.globus.quality.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;
import com.globus.common.forms.GmLogForm;

/**
 * Form used to store request values
 * 
 * @author mmuthusamy
 *
 */
public class GmSetMappingUploadForm extends GmLogForm {

	private String setId = "";
	private String setName = "";
	private String searchsetId = "";
	private String uploadAction = "";
	private String inputStr = "";
	private String submitAccessFl = "";
	private String setIdVal = "";

	private ArrayList alUploadAction = new ArrayList();

	/**
	 * @return the setId
	 */
	public String getSetId() {
		return setId;
	}

	/**
	 * @param setId
	 *            the setId to set
	 */
	public void setSetId(String setId) {
		this.setId = setId;
	}

	/**
	 * @return the setName
	 */
	public String getSetName() {
		return setName;
	}

	/**
	 * @param setName
	 *            the setName to set
	 */
	public void setSetName(String setName) {
		this.setName = setName;
	}

	/**
	 * @return the searchsetId
	 */
	public String getSearchsetId() {
		return searchsetId;
	}

	/**
	 * @param searchsetId
	 *            the searchsetId to set
	 */
	public void setSearchsetId(String searchsetId) {
		this.searchsetId = searchsetId;
	}

	/**
	 * @return the uploadAction
	 */
	public String getUploadAction() {
		return uploadAction;
	}

	/**
	 * @param uploadAction
	 *            the uploadAction to set
	 */
	public void setUploadAction(String uploadAction) {
		this.uploadAction = uploadAction;
	}

	/**
	 * @return the inputStr
	 */
	public String getInputStr() {
		return inputStr;
	}

	/**
	 * @param inputStr
	 *            the inputStr to set
	 */
	public void setInputStr(String inputStr) {
		this.inputStr = inputStr;
	}

	/**
	 * @return the submitAccessFl
	 */
	public String getSubmitAccessFl() {
		return submitAccessFl;
	}

	/**
	 * @param submitAccessFl
	 *            the submitAccessFl to set
	 */
	public void setSubmitAccessFl(String submitAccessFl) {
		this.submitAccessFl = submitAccessFl;
	}

	/**
	 * @return the alUploadAction
	 */
	public ArrayList getAlUploadAction() {
		return alUploadAction;
	}

	/**
	 * @param alUploadAction
	 *            the alUploadAction to set
	 */
	public void setAlUploadAction(ArrayList alUploadAction) {
		this.alUploadAction = alUploadAction;
	}

	/**
	 * @return the setIdVal
	 */
	public String getSetIdVal() {
		return setIdVal;
	}

	/**
	 * @param setIdVal
	 *            the setIdVal to set
	 */
	public void setSetIdVal(String setIdVal) {
		this.setIdVal = setIdVal;
	}

}