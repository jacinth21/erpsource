package com.globus.quality.scar.beans;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.DBConnectionWrapper;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmScarReportBean {
	GmCommonClass gmCommon = new GmCommonClass();
	GmLogger log = GmLogger.getInstance();
	
	/** 
	 * @return ArrayList
	 * @throws AppError
	 * */
	
	public ArrayList fetchScarListReport(HashMap hmParam)throws AppError, Exception{// To fetch the SCAR list report
		log.debug("Enter into fetchScarListReport in GmScarInitiationBean");
		GmDBManager gmDBManager = new GmDBManager();
		ArrayList alScarInfoDetails = new ArrayList();
		
		// get HashMap & assign the value into string variable.
		String strScar 		          = GmCommonClass.parseNull((String) hmParam.get("SCARID"));
		String strStatus              = GmCommonClass.parseNull((String) hmParam.get("STATUS"));
		String strAssigned            = GmCommonClass.parseNull((String) hmParam.get("ASSIGNEDNAME"));
		String strReference           = GmCommonClass.parseNull((String) hmParam.get("REFERENCE"));
		String strAssignedFromDate	  = GmCommonClass.parseNull((String) hmParam.get("ASSIGNEDFROMDT"));
		String strAssignedToDate 	  = GmCommonClass.parseNull((String) hmParam.get("ASSIGNEDTODT"));
		String strElapsedDaysVal 	  = GmCommonClass.parseNull((String) hmParam.get("ELPASEDDYSVAL"));
		String strComparison          = GmCommonClass.parseNull((String) hmParam.get("ELPASEDDYS"));
		
		String strfollow          = GmCommonClass.parseNull((String) hmParam.get("FOLLOWUP"));
		
		HashMap hmTempComp = new HashMap(); // To set the comparison signs to corresponding code id
        hmTempComp.put("90190",">");
        hmTempComp.put("90191","<");
        hmTempComp.put("90192","=");
        hmTempComp.put("90193",">=");
        hmTempComp.put("90194","<=");
        
		StringBuffer sbQuery = new StringBuffer();
		StringBuffer sQuery = new StringBuffer();
		//Query To Retrieve Information From Database.This Query Will Display All The Information Stored In Database. 
		sbQuery.append(" SELECT c3160_scar_id scarid, get_code_name(c901_status) statusName,to_char(c3160_assigned_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) AssignedDt,to_char(c3160_response_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) ResponsDt, to_char(c3160_close_dt,get_rule_value ('DATEFMT', 'DATEFORMAT')) ClosedDt"); 
		sbQuery.append(" ,DECODE(C901_Status,'102779',GET_WEEKDAYSDIFF(Trunc(c3160_assigned_dt),Trunc(Sysdate)),'102780',GET_WEEKDAYSDIFF(Trunc(c3160_assigned_dt),Trunc(C3160_Last_Updated_Date)),'102781',GET_WEEKDAYSDIFF(Trunc(c3160_assigned_dt),Trunc(c3160_close_dt)),'0') aging ") ; 
	    sbQuery.append(" ,get_log_flag(C3160_scar_id,4000318) logflag,get_vendor_name(C301_ASSIGNED_TO) assignedNames, c3160_ncmr_com_ref comref,c3160_follow_up_dt FollowUpDt,c901_status statusId"); 
		sbQuery.append(" FROM t3160_scar t3160 WHERE c3160_void_fl IS NULL ");
		/*102779-->Status is open
		 *102780-->Status is cancelled
		 *102781-->Status is closed
		 *400318-->Code id For Scar Log in Code Look Up Table For Comments
		 */
		
		if (!strScar.equals("")){// Filter based on SCAR Id
			sbQuery.append(" AND t3160.c3160_scar_id like upper('%");
			sbQuery.append(strScar);
			sbQuery.append("%')");
		}
		if (!strReference.equals("")){// Filter based on SCAR REFERENCE
			sbQuery.append(" AND upper(T3160.C3160_NCMR_COM_REF) like upper('%");
			sbQuery.append(strReference);
			sbQuery.append("%')");
		}
		
		if (!strStatus.equals("0") && !strStatus.equals("")){// Filter based on Status
			sbQuery.append(" AND t3160.c901_status = ");
			sbQuery.append(strStatus);
		}
		
		if (!strAssigned.equals("0") && !strAssigned.equals("")){// Filter based on AssignedTo
			sbQuery.append(" AND t3160.C301_ASSIGNED_TO= '");
			sbQuery.append(strAssigned);
			sbQuery.append("'");
		}	
		if (!strAssignedFromDate .equals("") && !strAssignedToDate .equals("")){// Filter based on Assigned_From and Assigned_To Date
            sbQuery.append(" AND c3160_assigned_dt"); 
            sbQuery.append(" BETWEEN to_date('");
    		sbQuery.append(strAssignedFromDate);
    		sbQuery.append("',get_rule_value ('DATEFMT', 'DATEFORMAT')) AND to_date('");
    		sbQuery.append(strAssignedToDate);
    		sbQuery.append("',get_rule_value ('DATEFMT', 'DATEFORMAT'))");  
		}
		if(!strElapsedDaysVal.equals("")) { //Filter based on Elapsed Days
			sbQuery.append(" AND DECODE(C901_Status,'102779',GET_WEEKDAYSDIFF(Trunc(c3160_assigned_dt),Trunc(Sysdate))");
			sbQuery.append(" ,'102780',GET_WEEKDAYSDIFF(Trunc(c3160_assigned_dt),Trunc(C3160_Last_Updated_Date))");
			sbQuery.append(" ,'102781',GET_WEEKDAYSDIFF(Trunc(c3160_assigned_dt),Trunc(c3160_close_dt)),'0') ");
			sbQuery.append(hmTempComp.get(strComparison));
			sbQuery.append(strElapsedDaysVal);
		}
		if (!strfollow.equals("0") && strfollow.equals("80130")){//80130-->Code Id Value in Code Look Up Table.
			sbQuery.append(" AND T3160.C3160_FOLLOW_UP_FL = 'Y' ");
		}
		else if (!strfollow.equals("0") && strfollow.equals("80131")){
			sbQuery.append(" AND T3160.C3160_FOLLOW_UP_FL = 'N' ");
		}
		sbQuery.append(" ORDER BY aging DESC");
		
		log.debug( "Query for getting USer List " + sbQuery.toString());
		
		alScarInfoDetails = gmDBManager.queryMultipleRecords(sbQuery.toString());
		return alScarInfoDetails; 
		
}
}