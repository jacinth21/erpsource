package com.globus.quality.scar.beans;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import oracle.jdbc.driver.OracleTypes;

import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.common.db.GmDBManager;

public class GmScarSetupBean {
	GmCommonClass gmCommon = new GmCommonClass();
	GmLogger log = GmLogger.getInstance();
	
	/** 
	 * @return String
	 * @throws AppError
	 * */
	public String saveScar(HashMap hmParam)throws AppError, Exception{	
		log.debug("Enter into saveScar in GmScarInitiationBean  ");
		GmCommonBean gmCommonBean = new GmCommonBean();
		String strScarIDFromDB = "";
		String strScarId 		       = GmCommonClass.parseNull((String) hmParam.get("SCARID"));
		String strNcmr 			   = GmCommonClass.parseNull((String) hmParam.get("COMPLAINT"));
		String strDesc             = GmCommonClass.parseNull((String) hmParam.get("SCARDESC"));
		String strAssigned         = GmCommonClass.parseNull((String) hmParam.get("ASSIGNEDTO"));
		String strIssueBy        = GmCommonClass.parseZero((String) hmParam.get("STRISSUEDBY"));
		String strPrevScar        = GmCommonClass.parseNull((String) hmParam.get("PREVSCAR"));
		String strAssignedDate	   = GmCommonClass.parseNull((String) hmParam.get("ASSIGNEDDT"));
		String strResposeDate 	   = GmCommonClass.parseNull((String) hmParam.get("RESPDUEDT"));   	
        String strCloseDate 	   = GmCommonClass.parseNull((String) hmParam.get("CLOSEDDT"));
        String strInfo             = GmCommonClass.parseNull((String) hmParam.get("RELAVENTINFO"));
        String strFollowUpFlag        = GmCommonClass.parseNull((String) hmParam.get("FOLLOWUPFLAG"));
        String strEstimetdFlUpDt        = GmCommonClass.parseNull((String) hmParam.get("ESTIMETDFLUPDT"));
        String strUserId           = GmCommonClass.parseNull((String) hmParam.get("USERID"));
        String strDateFormat       = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
        String strLog       = GmCommonClass.parseNull((String) hmParam.get("TXT_LOGREASON"));
        
        Date dtAssignedDate 		= null;
        Date dtResposeDate 			= null;
        Date dtCloseDate 			= null;
        Date dtEstimetdFlUpDt 		= null;
        dtAssignedDate   = GmCommonClass.getStringToDate(strAssignedDate, strDateFormat);
        dtResposeDate    = GmCommonClass.getStringToDate(strResposeDate, strDateFormat);   
        dtCloseDate 	 = GmCommonClass.getStringToDate(strCloseDate ,strDateFormat);
        dtEstimetdFlUpDt 	 = GmCommonClass.getStringToDate(strEstimetdFlUpDt ,strDateFormat);
        
        GmDBManager gmDBManager = new GmDBManager();
        gmDBManager.setPrepareString("gm_pkg_qa_scar_txn.gm_sav_scar_info",14);
		gmDBManager.registerOutParameter(14, OracleTypes.CHAR);
        gmDBManager.setString(1,strScarId);
		gmDBManager.setString(2, strNcmr);
		gmDBManager.setString(3, strDesc);
		gmDBManager.setString(4, strAssigned);
		gmDBManager.setString(5, strIssueBy);
		gmDBManager.setString(6, strPrevScar);
		gmDBManager.setDate(7, dtAssignedDate);
		gmDBManager.setDate(8, dtResposeDate);
		gmDBManager.setDate(9, dtCloseDate);
		gmDBManager.setString(10, strInfo);
		gmDBManager.setString(11, strUserId);
		gmDBManager.setString(12, strFollowUpFlag);		
		gmDBManager.setDate(13, dtEstimetdFlUpDt);		
		gmDBManager.execute();
		strScarIDFromDB = GmCommonClass.parseNull(gmDBManager.getString(14));
		if (!strLog.equals("")) {
			gmCommonBean.saveLog(gmDBManager, strScarIDFromDB, strLog, strUserId, "4000318");//4000318-->Code id for Scar Log
		}
		gmDBManager.commit();
		return strScarIDFromDB;
	}
	
	/** 
	 * @return HashMap
	 * @param hmParam
	 * @throws AppError
	 * */
	
	public ArrayList fetchScar(HashMap hmParam)throws AppError, Exception{
	    log.debug("Enter into fetchRFSDetails in GmScarInitiationBean  ");
	    ArrayList alReturn = new ArrayList();
		GmDBManager gmDBManager = new GmDBManager();
		String strScarId  = GmCommonClass.parseNull((String) hmParam.get("SCARID"));
		gmDBManager.setPrepareString("gm_pkg_qa_scar_rpt.gm_fch_scar_info",2);
		gmDBManager.registerOutParameter(2, OracleTypes.CURSOR);
		gmDBManager.setString(1, strScarId);
		gmDBManager.execute();
		alReturn = gmDBManager.returnArrayList((ResultSet)gmDBManager.getObject(2));
		gmDBManager.close();
		return alReturn;
	}
}