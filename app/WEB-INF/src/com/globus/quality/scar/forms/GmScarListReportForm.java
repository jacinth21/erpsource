package com.globus.quality.scar.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.globus.common.forms.GmCommonForm;

public class GmScarListReportForm extends GmCommonForm {

	private ArrayList alStatus = new ArrayList();
	private ArrayList alAssignedTo = new ArrayList();
	private ArrayList alElapsedDys = new ArrayList();
	private ArrayList alChooseAction = new ArrayList();
	private ArrayList alFollowUp = new ArrayList();
	
	
	private String  assignedToDt    = "";
	private String  followUp    = "";
	private String  gridXmlData    = "";
	private String  assignedName = "";
	private String  scarId      = "";
	private String  statusId  = "";
	private String  elpasedDys  = "";
	private String  reference    = "";
	private String  assignedFromDt    = "";
	private String  chooseAction = "";
	private String  elpasedDysVal = "";
	private String  hscarId = "";
	private String  hDisplayNm    = "";
	private String  hRedirectURL  = "";
	private String  hstatus = "";
	private String  status = "";
	private String accessFl = "";
	
	public String getHscarId() {
		return hscarId;
	}
	public void setHscarId(String hscarId) {
		this.hscarId = hscarId;
	}
	public ArrayList getAlChooseAction() {
		return alChooseAction;
	}
	public void setAlChooseAction(ArrayList alChooseAction) {
		this.alChooseAction = alChooseAction;
	}
	public String getChooseAction() {
		return chooseAction;
	}
	public void setChooseAction(String chooseAction) {
		this.chooseAction = chooseAction;
	}
	public ArrayList getAlElapsedDys() {
		return alElapsedDys;
	}
	public void setAlElapsedDys(ArrayList alElapsedDys) {
		this.alElapsedDys = alElapsedDys;
	}
	
	
	
	
	public String getElpasedDysVal() {
		return elpasedDysVal;
	}
	public void setElpasedDysVal(String elpasedDysVal) {
		this.elpasedDysVal = elpasedDysVal;
	}
	public String getAssignedName() {
		return assignedName;
	}
	public void setAssignedName(String assignedName) {
		this.assignedName = assignedName;
	}
	private int arraySize ;
	
	/**
	 * @return the gridXmlData
	 */
	public String getGridXmlData() {
		return gridXmlData;
	}
	public ArrayList getAlAssignedTo() {
		return alAssignedTo;
	}
	public void setAlAssignedTo(ArrayList alAssignedTo) {
		this.alAssignedTo = alAssignedTo;
	}
	/**
	 * @param gridXmlData the gridXmlData to set
	 */
	public void setGridXmlData(String gridXmlData) {
		this.gridXmlData = gridXmlData;
	}
	
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getAssignedToDt() {
		return assignedToDt;
	}
	public void setAssignedToDt(String assignedToDt) {
		this.assignedToDt = assignedToDt;
	}
	
	
	
	
	
	public ArrayList getAlStatus() {
		return alStatus;
	}
	public void setAlStatus(ArrayList alStatus) {
		this.alStatus = alStatus;
	}
	public String getScarId() {
		return scarId;
	}
	public void setScarId(String scarId) {
		this.scarId = scarId;
	}
	
	public String getAssignedFromDt() {
		return assignedFromDt;
	}
	public void setAssignedFromDt(String assignedFromDt) {
		this.assignedFromDt = assignedFromDt;
	}
	
	public int getArraySize() {
		return arraySize;
	}
	public void setArraySize(int arraySize) {
		this.arraySize = arraySize;
	}
	
	public String getElpasedDys() {
		return elpasedDys;
	}
	public void setElpasedDys(String elpasedDys) {
		this.elpasedDys = elpasedDys;
	}
	public void sethDisplayNm(String hDisplayNm) {
		this.hDisplayNm = hDisplayNm;
	}
	public String gethDisplayNm() {
		return hDisplayNm;
	}
	public void sethRedirectURL(String hRedirectURL) {
		this.hRedirectURL = hRedirectURL;
	}
	public String gethRedirectURL() {
		return hRedirectURL;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getFollowUp() {
		return followUp;
	}
	public void setFollowUp(String followUp) {
		this.followUp = followUp;
	}
	public String getHstatus() {
		return hstatus;
	}
	public void setHstatus(String hstatus) {
		this.hstatus = hstatus;
	}
	public ArrayList getAlFollowUp() {
		return alFollowUp;
	}
	public void setAlFollowUp(ArrayList alFollowUp) {
		this.alFollowUp = alFollowUp;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAccessFl() {
		return accessFl;
	}
	public void setAccessFl(String accessFl) {
		this.accessFl = accessFl;
	}
	
}	