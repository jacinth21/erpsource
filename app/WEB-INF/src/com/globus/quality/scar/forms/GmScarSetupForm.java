package com.globus.quality.scar.forms;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionMapping;
import com.globus.common.forms.GmUploadForm;

public class GmScarSetupForm extends GmUploadForm {

	
	private String  scarId        = "";
	private String  complaint     = "";
	private String  scarDesc      = "";
	private String  status        = "";
	private String  assignedNames = "";
	private String  assignedTo    = "";
	private String  assignedDt    = "";
	private String  respDueDt     = "";
	private String  closedDt      = "";
	private String  relaventInfo  = "";
	private String  statusName    = "";
	private int     intSize       = 0;
	private String  strIssuedBy 	  = "";
	private String followUpFlag   = "";
	private String prevScar		  = "";
	private String estimetdFlUpDt = "";
	private String chooseAction = "";
	private String message= "";
	private String hSCARID= "";
	private String daysToClose = "";
	private String strButtonDisable="";
	
	
	private ArrayList alChooseAction = new ArrayList();
	private ArrayList alFollowUpFl = new ArrayList();
	
	/**
	 * @return the chooseAction
	 */
	public String getChooseAction() {
		return chooseAction;
	}

	/**
	 * @param chooseAction the chooseAction to set
	 */
	public void setChooseAction(String chooseAction) {
		this.chooseAction = chooseAction;
	}

	/**
	 * @return the alChooseAction
	 */
	public ArrayList getAlChooseAction() {
		return alChooseAction;
	}

	/**
	 * @param alChooseAction the alChooseAction to set
	 */
	public void setAlChooseAction(ArrayList alChooseAction) {
		this.alChooseAction = alChooseAction;
	}

	/**
	 * @return the estimetdFlUpDt
	 */
	public String getEstimetdFlUpDt() {
		return estimetdFlUpDt;
	}

	/**
	 * @param estimetdFlUpDt the estimetdFlUpDt to set
	 */
	public void setEstimetdFlUpDt(String estimetdFlUpDt) {
		this.estimetdFlUpDt = estimetdFlUpDt;
	}

	/**
	 * @return the prevScar
	 */
	public String getPrevScar() {
		return prevScar;
	}

	/**
	 * @param prevScar the prevScar to set
	 */
	public void setPrevScar(String prevScar) {
		this.prevScar = prevScar;
	}

	private ArrayList alAssignedTo = new ArrayList();
	private ArrayList alIssuedBy = new ArrayList();
	
	
	
	/**
	 * @return the followUpFlag
	 */
	public String getFollowUpFlag() {
		return followUpFlag;
	}

	/**
	 * @param followUpFlag the followUpFlag to set
	 */
	public void setFollowUpFlag(String followUpFlag) {
		this.followUpFlag = followUpFlag;
	}

	/**
	 * @return the strIssuedBy
	 */
	public String getStrIssuedBy() {
		return strIssuedBy;
	}

	/**
	 * @param strIssuedBy the strIssuedBy to set
	 */
	public void setStrIssuedBy(String strIssuedBy) {
		this.strIssuedBy = strIssuedBy;
	}

	/**
	 * @return the alIssuedBy
	 */
	public ArrayList getAlIssuedBy() {
		return alIssuedBy;
	}

	/**
	 * @param alIssuedBy the alIssuedBy to set
	 */
	public void setAlIssuedBy(ArrayList alIssuedBy) {
		this.alIssuedBy = alIssuedBy;
	}

	/**
	 * @return the intSize
	 */
	public int getIntSize() {
		return intSize;
	}

	/**
	 * @param intSize the intSize to set
	 */
	public void setIntSize(int intSize) {
		this.intSize = intSize;
	}
	
	
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
	
	public String getScarId() {
		return scarId;
	}
	public void setScarId(String scarId) {
		this.scarId = scarId;
	}
	
	
	public String getComplaint() {
		return complaint;
	}
	public void setComplaint(String complaint) {
		this.complaint = complaint;
	}
	
	
	public String getScarDesc() {
		return scarDesc;
	}
	public void setScarDesc(String scarDesc) {
		this.scarDesc = scarDesc;
	}
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public String getAssignedNames() {
		return assignedNames;
	}
	public void setAssignedNames(String assignedNames) {
		this.assignedNames = assignedNames;
	}
	
	
	public ArrayList getAlAssignedTo() {
		return alAssignedTo;
	}

	public void setAlAssignedTo(ArrayList alAssignedTo) {
		this.alAssignedTo = alAssignedTo;
	}
	
	
	public String getAssignedDt() {
		return assignedDt;
	}

	public void setAssignedDt(String assignedDt) {
		this.assignedDt = assignedDt;
	}
	
	
	public String getRespDueDt() {
		return respDueDt;
	}

	public void setRespDueDt(String respDueDt) {
		this.respDueDt = respDueDt;
	}
	
	
	public String getClosedDt() {
		return closedDt;
	}
	
	public void setClosedDt(String closedDt) {
		this.closedDt = closedDt;
	}

	public String getRelaventInfo() {
		return relaventInfo;
	}

	public void setRelaventInfo(String relaventInfo) {
		this.relaventInfo = relaventInfo;
	}
	
	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.scarId= "";
    	this.complaint= "";                                                       
    	this.scarDesc = "";
    	this.status  = "";
    	this.assignedTo = "";
    	this.assignedDt = "";
    	this.relaventInfo = "";
    	this.statusName= "";
    	this.followUpFlag = "";
    	this.prevScar= "";
    	this.estimetdFlUpDt  = "";
    	this.assignedNames = "";
    	try {   
  		  request.setCharacterEncoding("UTF-8");   
  		} catch (UnsupportedEncodingException ex) {   
  		} 
    	
	}
	/**
	 * @return the hSCARID
	 */
	public String gethSCARID() {
		return hSCARID;
	}

	/**
	 * @param hSCARID the hSCARID to set
	 */
	public void sethSCARID(String hSCARID) {
		this.hSCARID = hSCARID;
	}

	

	public String getDaysToClose() {
		return daysToClose;
	}

	public void setDaysToClose(String daysToClose) {
		this.daysToClose = daysToClose;
	}

	/**
	 * @param alFollowUpFl the alFollowUpFl to set
	 */
	public void setAlFollowUpFl(ArrayList alFollowUpFl) {
		this.alFollowUpFl = alFollowUpFl;
	}

	/**
	 * @return the alFollowUpFl
	 */
	public ArrayList getAlFollowUpFl() {
		return alFollowUpFl;
	}
	/**
	 * @param strButtonDisable the strButtonDisable to set
	 */

	public void setStrButtonDisable(String strButtonDisable) {
		this.strButtonDisable = strButtonDisable;
	}

	/**
	 * @return the strButtonDisable
	 */
	public String getStrButtonDisable() {
		return strButtonDisable;
	}
}	



	
	
	
	
	

