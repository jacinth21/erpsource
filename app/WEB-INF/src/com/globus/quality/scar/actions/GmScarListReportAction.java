package com.globus.quality.scar.actions;

/*
 * import java.util.ArrayList; import java.util.HashMap;
 */
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmJasperReport;
/* import com.globus.common.beans.GmCommonClass; */
import com.globus.common.beans.GmLogger;
import com.globus.common.util.GmTemplateUtil;
/* import com.globus.logon.beans.GmLogonBean; */
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.quality.scar.beans.GmScarReportBean;
import com.globus.quality.scar.beans.GmScarSetupBean;
import com.globus.quality.scar.forms.GmScarListReportForm;
import com.globus.sales.event.beans.GmEventSetupBean;


/* import com.globus.logon.beans.GmLogonBean; */
public class GmScarListReportAction extends GmDispatchAction {

  Logger log = GmLogger.getInstance(this.getClass().getName());// Code to Initialize the Logger
                                                               // Class.

  String strXmlGridData = "";

  public ActionForward listScarDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    GmScarListReportForm gmScarListReportForm = (GmScarListReportForm) actionForm;
    GmScarReportBean gmScarListReportBean = new GmScarReportBean();
    GmVendorBean gmVendorBean = new GmVendorBean();
    GmEventSetupBean gmEventSetupBean = new GmEventSetupBean();
    GmLogonBean gmLogon = new GmLogonBean();
    gmScarListReportForm.loadSessionParameters(request);
    log.debug("Enter into loadScarListReport in Action");
    String strOpt = "";
    /*
     * String strPartyId = GmCommonClass.parseNull((String)
     * gmScarListReportForm.getSessPartyId());// Get the user from session String strAccessFl = "";
     * strAccessFl = gmEventSetupBean.getEventAccess(strPartyId, "GROUP", "SCAR_USER");
     */// to get the accessibility of user
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) request.getAttribute("strSessCompanyLocale"));


    strOpt = GmCommonClass.parseNull(gmScarListReportForm.getStrOpt());
    Boolean ErrorFl = false;
    ArrayList alVendorList = new ArrayList();
    ArrayList alStatus = new ArrayList();
    ArrayList alScarDetails = new ArrayList();
    ArrayList alElapsedDys = new ArrayList();
    ArrayList alChooseAction = new ArrayList();
    ArrayList alFollow = new ArrayList();

    HashMap hmParam = new HashMap();
    HashMap hmReturn = new HashMap();
    HashMap hmTemp = new HashMap();
    HashMap hmResult = new HashMap();

    /*
     * if(!strAccessFl.equals("Y")){// Checking whether user is having access or not throw new
     * AppError("You do not have enough permission to do this action", "", 'E'); }
     */

    // Get employee list
    hmTemp.put("CHECKACTIVEFL", "");
    hmResult = gmVendorBean.getVendorList(hmTemp);
    alVendorList = GmCommonClass.parseNullArrayList((ArrayList) hmResult.get("VENDORLIST"));
    gmScarListReportForm.setAlAssignedTo(alVendorList);

    // Get the drop down values from code lookup table
    alStatus = GmCommonClass.getCodeList("SCARST");
    alElapsedDys = GmCommonClass.getCodeList("CPRSN");
    alChooseAction = GmCommonClass.getCodeList("SCARTP");
    alFollow = GmCommonClass.getCodeList("YES/NO");



    if (strOpt.equals("load")) {
      // Set all the values to drop down

      gmScarListReportForm.setAlStatus(alStatus);
      gmScarListReportForm.setAlElapsedDys(alElapsedDys);
      gmScarListReportForm.setAlChooseAction(alChooseAction);
      gmScarListReportForm.setAlFollowUp(alFollow);
      gmScarListReportForm.setStatus("102779");// To set the status as "Open" while loading from
                                               // left link
      strOpt = "Report";
    }
    hmParam = GmCommonClass.getHashMapFromForm(gmScarListReportForm);
    String strApplDateFmt = GmCommonClass.parseNull((String) hmParam.get("APPLNDATEFMT"));
    String strAccessFl = GmCommonClass.parseNull(gmScarListReportForm.getAccessFl());
    if (strOpt.equals("Report")) {// Checking whether user is having access or not
      gmScarListReportForm.setAlStatus(alStatus);
      gmScarListReportForm.setAlElapsedDys(alElapsedDys);
      gmScarListReportForm.setAlChooseAction(alChooseAction);
      gmScarListReportForm.setAlFollowUp(alFollow);
      log.debug("enter into report");
      alScarDetails =
          GmCommonClass.parseNullArrayList(gmScarListReportBean.fetchScarListReport(hmParam));
      gmScarListReportForm.setArraySize(alScarDetails.size());
      hmReturn.put("SCARDATA", alScarDetails);
      hmReturn.put("APPLNDATEFMT", strApplDateFmt);
      hmReturn.put("STRSESSCOMPANYLOCALE", strSessCompanyLocale);
      hmReturn.put("strAccessFl",strAccessFl);
      String strxmlGridData = generateOutPut(hmReturn);
      gmScarListReportForm.setGridXmlData(strxmlGridData);
    }


    return actionMapping.findForward("gmScarListReport");
  }

  public ActionForward printScarDetails(ActionMapping actionMapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response) throws Exception, AppError {
    GmScarListReportForm gmScarListReportForm = (GmScarListReportForm) actionForm;
    GmScarSetupBean gmScarSetupBean = new GmScarSetupBean();
    gmScarListReportForm.loadSessionParameters(request);
    log.debug("Enter into printScarDetails in Action");

    String strOpt = GmCommonClass.parseNull(gmScarListReportForm.getStrOpt());

    ArrayList alScarDetails = new ArrayList();
    HashMap hmParam = new HashMap();
    HashMap hmResult = new HashMap();

    hmParam = GmCommonClass.getHashMapFromForm(gmScarListReportForm);

    if (strOpt.equals("Print")) {
      alScarDetails = gmScarSetupBean.fetchScar(hmParam);
      if (alScarDetails.size() > 0)
        hmResult = GmCommonClass.parseNullHashMap((HashMap) alScarDetails.get(0));
      if (hmResult.size() > 0) {
        hmResult.put("STRASSINEDTO", "");
        hmResult.put("VERSION", GmCommonClass.getRuleValue("102789", "CMP_VERSION"));
      }
      GmJasperReport gmJasperReport = new GmJasperReport();
      gmJasperReport.setRequest(request);
      gmJasperReport.setResponse(response);
      gmJasperReport.setJasperReportName("/GmPrintSCAR.jasper");
      gmJasperReport.setHmReportParameters(hmResult);
      gmJasperReport.setReportDataList(null);
      gmJasperReport.setPageHeight(1800);
      gmJasperReport.exportJasperReportToRTF();
    }
    return actionMapping.findForward("");
  }

  private String generateOutPut(HashMap hmParam) throws AppError { // Processing HashMap into XML
                                                                   // data
    GmTemplateUtil templateUtil = new GmTemplateUtil();
    String strSessCompanyLocale =
        GmCommonClass.parseNull((String) hmParam.get("STRSESSCOMPANYLOCALE"));

    templateUtil.setDataMap("hmParam", hmParam);
    templateUtil.setTemplateSubDir("quality/scar/templates");
    templateUtil.setResourceBundle(GmCommonClass.getResourceBundleBean(
        "properties.labels.quality.scar.GmScarListReport", strSessCompanyLocale));
    templateUtil.setTemplateName("GmScarListReport.vm");
    return templateUtil.generateOutput();
  }
}
