package com.globus.quality.scar.actions;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;

import com.globus.common.actions.GmDispatchAction;
import com.globus.common.beans.AppError;
import com.globus.common.beans.GmAccessControlBean;
import com.globus.common.beans.GmCommonBean;
import com.globus.common.beans.GmCommonClass;
import com.globus.common.beans.GmLogger;
import com.globus.logon.beans.GmLogonBean;
import com.globus.operations.beans.GmVendorBean;
import com.globus.quality.capa.beans.GmCapaReportBean;
import com.globus.quality.scar.beans.GmScarSetupBean;
import com.globus.quality.complaint.beans.GmCOMRSRRptBean;
import com.globus.quality.scar.forms.GmScarSetupForm;
import com.globus.sales.event.beans.GmEventSetupBean;
public class GmScarSetupAction extends GmDispatchAction {
	
	Logger log = GmLogger.getInstance(this.getClass().getName());
	
	
	public ActionForward addScar (ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws Exception, AppError 
	{	
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmScarSetupForm gmScarSetupForm = (GmScarSetupForm) actionForm;
		instantiate(request, response);
		gmScarSetupForm.loadSessionParameters(request);
		GmScarSetupBean gmScarInitiationBean = new GmScarSetupBean();
		GmVendorBean GmVendorBean = new GmVendorBean();
		GmEventSetupBean gmEventSetupBean 	= new GmEventSetupBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(); 
		GmLogonBean gmLogon = new GmLogonBean();
		GmCapaReportBean gmCapaReportBean = new GmCapaReportBean();
		GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean();
		log.debug("Enter into addScar in Action");
		
		ArrayList alVendorList = new ArrayList();
		HashMap hmTemp = new HashMap();
		
		ArrayList alLogReasons = new ArrayList();
		HashMap hmParam =  new HashMap();
		HashMap hmResult =  new HashMap();
		HashMap hmAccess = new HashMap();
		ArrayList alIssuedBy = new ArrayList();
		ArrayList alCboAction = new ArrayList();
		ArrayList alFollowUpFl= new ArrayList();
		String strOpt      = "";
		String strScarID   = "";
		String strMessage  = "";
		String strAccessFl = "";
		
		String strPartyId  = GmCommonClass.parseNull((String) gmScarSetupForm.getSessPartyId());// Get the user from session
		String strCurrentdate = GmCommonClass.parseNull((String) request.getSession().getAttribute("strSessTodaysDate"));
		
		hmAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,"SCAR_ACCESS"));
		String strUpdateAccess = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
        gmScarSetupForm.setStrButtonDisable(strUpdateAccess);
		// To get the Supplier Name 
		hmTemp.put("CHECKACTIVEFL",""); // Display user who are all active
		hmResult = GmVendorBean.getVendorList(hmTemp); 
		alVendorList = GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("VENDORLIST"));
		gmScarSetupForm.setAlAssignedTo(alVendorList);
		// To get the Issued by drop down
		
		alIssuedBy = GmCommonClass.parseNullArrayList((ArrayList) gmAccessControlBean.fetchgroupUser("SCAR_ISSUED_BY"));
		// For Choose Action
		alCboAction = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SCARTP")); 
		alFollowUpFl = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YES/NO")); // For Follow up flag
		
		gmScarSetupForm.setAlIssuedBy(alIssuedBy);
		gmScarSetupForm.setAlChooseAction(alCboAction);
		gmScarSetupForm.setAlFollowUpFl(alFollowUpFl);
		
		hmParam = GmCommonClass.getHashMapFromForm(gmScarSetupForm);
		strOpt  = GmCommonClass.parseNull((String)gmScarSetupForm.getStrOpt());
		
		hmParam.put("CURRENTDT", strCurrentdate);
		hmParam.put("DAYS", "30");
		String strRespDt = GmCommonClass.parseNull(gmCapaReportBean.fetchBussinessDays(hmParam));// To add 30 business days with current date
		
			if(strOpt.equals("Add")){
				
			    strScarID = gmScarInitiationBean.saveScar(hmParam);
			    
			    if(!strScarID.equals("")){
				strMessage = "Data Saved Successfully";	
			    }
			    String strRedirectURL = "/gmScarInitiation.do?method=editScar&scarId="+strScarID+"&strOpt=Fetch&message="+strMessage;
			    ActionRedirect actionRedirect = new ActionRedirect(strRedirectURL);
			    return actionRedirect;
			}
			alLogReasons = gmCommonBean.getLog(strScarID,"4000318");
			gmScarSetupForm.setAlLogReasons(alLogReasons);		
			gmScarSetupForm.setScarId(strScarID);
			
			gmScarSetupForm.setStrIssuedBy(strPartyId);
			gmScarSetupForm.setRespDueDt(strRespDt);
			
			gmScarSetupForm.reset(actionMapping, request); 
			return actionMapping.findForward("gmScarInitiation");
	
	}

	public ActionForward editScar(ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws Exception, AppError 
	{
		GmScarSetupForm gmScarSetupForm = (GmScarSetupForm) actionForm;
		GmCommonBean gmCommonBean = new GmCommonBean();
		GmVendorBean GmVendorBean = new GmVendorBean();
		GmAccessControlBean gmAccessControlBean = new GmAccessControlBean(); 
		GmEventSetupBean gmEventSetupBean 	= new GmEventSetupBean();
		GmScarSetupBean gmScarInitiationBean = new GmScarSetupBean();
		GmLogonBean gmLogon = new GmLogonBean();
		GmCOMRSRRptBean gmCOMRSRRptBean = new GmCOMRSRRptBean();
		instantiate(request, response);
		gmScarSetupForm.loadSessionParameters(request);
		log.debug("Enter into editScar in Action");
		String strForward = "gmScarInitiation";
		ArrayList alVendorList = new ArrayList();
		HashMap hmTemp = new HashMap();
		ArrayList alResult = new ArrayList();
		ArrayList alIssuedBy = new ArrayList();
		ArrayList alFollowUpFl= new ArrayList();
		HashMap hmResult =  new HashMap();
		HashMap hmParam =  new HashMap();
		HashMap hmAccess = new HashMap();
		ArrayList alLogReasons = new ArrayList();
		ArrayList alCboAction = new ArrayList();
		
		String strOpt = "";
		String strPartyId  = GmCommonClass.parseNull((String) gmScarSetupForm.getSessPartyId());// Get the user from session
				String strAction = GmCommonClass.parseNull(gmScarSetupForm.getHaction());
		
		hmAccess =GmCommonClass.parseNullHashMap(gmAccessControlBean.getAccessPermissions(strPartyId,"SCAR_ACCESS"));
		String strUpdateAccess = GmCommonClass.parseNull((String)hmAccess.get("UPDFL"));
        gmScarSetupForm.setStrButtonDisable(strUpdateAccess);
		// To get the Supplier Name 
		hmTemp.put("CHECKACTIVEFL","");// Display user who are all active
		hmResult = GmVendorBean.getVendorList(hmTemp); 
		alVendorList = GmCommonClass.parseNullArrayList((ArrayList)hmResult.get("VENDORLIST"));
		gmScarSetupForm.setAlAssignedTo(alVendorList);
		// To get the Issued by drop down
		
		alIssuedBy = GmCommonClass.parseNullArrayList((ArrayList) gmAccessControlBean.fetchgroupUser("SCAR_ISSUED_BY"));
		alCboAction = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("SCARTP")); // For Choose Action
		alFollowUpFl = GmCommonClass.parseNullArrayList(GmCommonClass.getCodeList("YES/NO")); // For Follow up flag
		gmScarSetupForm.setAlIssuedBy(alIssuedBy);
		gmScarSetupForm.setAlChooseAction(alCboAction);
		gmScarSetupForm.setAlFollowUpFl(alFollowUpFl);
		hmParam = GmCommonClass.getHashMapFromForm(gmScarSetupForm);
		strOpt = GmCommonClass.parseNull((String)gmScarSetupForm.getStrOpt());
		String strScarId  = GmCommonClass.parseNull((String) hmParam.get("SCARID"));
							
			if(strOpt.equals("Fetch")){
				alResult =	gmScarInitiationBean.fetchScar(hmParam);
				if(alResult.size()> 0 )	hmResult = (HashMap)alResult.get(0);
					GmCommonClass.getFormFromHashMap(gmScarSetupForm, hmResult);			
				if(alResult.size() > 0 )	
					gmScarSetupForm.setIntSize(alResult.size());
				alLogReasons = gmCommonBean.getLog(strScarId,"4000318");
				gmScarSetupForm.setAlLogReasons(alLogReasons);
			}
				
		return actionMapping.findForward(strForward);
	
	}
	
	public ActionForward scarHighlights (ActionMapping actionMapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) throws Exception, AppError 
	{
		GmScarSetupForm gmScarInitiationForm = (GmScarSetupForm) actionForm;
		GmCommonBean gmCommonBean = new GmCommonBean();
		String strForward = "SCARHighlights";

		ArrayList alResult = new ArrayList();
		ArrayList alLogReasons = new ArrayList();
		
		HashMap hmResult =  new HashMap();
		HashMap hmParam =  new HashMap();
		
		GmScarSetupBean gmScarInitiationBean = new GmScarSetupBean();
		 
		hmParam = GmCommonClass.getHashMapFromForm(gmScarInitiationForm);
		String strOpt = GmCommonClass.parseNull((String)gmScarInitiationForm.getStrOpt());
		String strScarId  = GmCommonClass.parseNull((String) hmParam.get("SCARID"));
		if(strOpt.equals("Fetch") || strOpt.equals("Summary")){// To fetch the details to the Summary screen and Upload screen
			alResult =	gmScarInitiationBean.fetchScar(hmParam);
			if(alResult.size()> 0 )	hmResult = (HashMap)alResult.get(0);
		}
		GmCommonClass.getFormFromHashMap(gmScarInitiationForm, hmResult);
		
		if(alResult.size() > 0 )	
			gmScarInitiationForm.setIntSize(alResult.size());
		alLogReasons = gmCommonBean.getLog(strScarId,"4000318");
		gmScarInitiationForm.setAlLogReasons(alLogReasons);
		return actionMapping.findForward(strForward);
	}
}