package com.globus.valueobject.pricing;
import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
/**
 * @author dsandeep
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmGroupDetailsVO extends GmDataStoreVO{
	private String groupname="";
	private String systemname="";
	private String listprice="";
	private String twprice="";
	
	/**
	 * @return the listprice
	 */
	public String getListprice() {
		return listprice;
	}
	/**
	 * @param listprice the listprice to set
	 */
	public void setListprice(String listprice) {
		this.listprice = listprice;
	}
	/**
	 * @return the twprice
	 */
	public String getTwprice() {
		return twprice;
	}
	/**
	 * @param twprice the twprice to set
	 */
	public void setTwprice(String twprice) {
		this.twprice = twprice;
	}
	/**
	 * @return the groupname
	 */
	public String getGroupname() {
		return groupname;
	}
	/**
	 * @param groupname the groupname to set
	 */
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	/**
	 * @return the systemname
	 */
	public String getSystemname() {
		return systemname;
	}
	/**
	 * @param systemname the systemname to set
	 */
	public void setSystemname(String systemname) {
		this.systemname = systemname;
	}
	
	@Transient
	public Properties getGroupPricingDetailsVO() {
	    Properties props = new Properties();
	    props.put("GROUPNAME", "GROUPNAME");
	    props.put("SYSNAME", "SYSTEMNAME");
	    props.put("LISTP", "LISTPRICE");
	    props.put("TW", "TWPRICE");
	    return props;
	  }
}

