package com.globus.valueobject.pricing;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataVO;
import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPricingListVO extends GmDataVO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmImpactAnalysisVO> gmImpactAnalysisVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmAccountDetailsListVO> gmAccountDetailsListVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmThresholdQuestionVO> gmThresholdQuestionVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmGroupPartMappingVO> gmGroupPartMappingVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmSystemVO> gmSystemVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmGroupVO> gmGroupVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmPricingReportVO> gmPricingReportVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmPriceAccountTypeVO> gmPriceAccountTypeVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmSalesAnalysisVO> gmsalesAnalysisVO = null; 
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmPricingSalesHeaderVO> gmPricingSalesHeaderVO = null;

	
	public List<GmPriceAccountTypeVO> getGmPriceAccountTypeVO() {
		return gmPriceAccountTypeVO;
	}
	public void setGmPriceAccountTypeVO(
			List<GmPriceAccountTypeVO> gmPriceAccountTypeVO) {
		this.gmPriceAccountTypeVO = gmPriceAccountTypeVO;
	}
	public List<GmImpactAnalysisVO> getGmImpactAnalysisVO() {
		return gmImpactAnalysisVO;
	}
	public void setGmImpactAnalysisVO(List<GmImpactAnalysisVO> gmImpactAnalysisVO) {
		this.gmImpactAnalysisVO = gmImpactAnalysisVO;
	}
	public List<GmAccountDetailsListVO> getGmAccountDetailsListVO() {
		return gmAccountDetailsListVO;
	}
	public void setGmAccountDetailsListVO(
			List<GmAccountDetailsListVO> gmAccountDetailsListVO) {
		this.gmAccountDetailsListVO = gmAccountDetailsListVO;
	}
	public List<GmThresholdQuestionVO> getGmThresholdQuestionVO() {
		return gmThresholdQuestionVO;
	}
	public void setGmThresholdQuestionVO(
			List<GmThresholdQuestionVO> gmThresholdQuestionVO) {
		this.gmThresholdQuestionVO = gmThresholdQuestionVO;
	}
	public List<GmGroupPartMappingVO> getGmGroupPartMappingVO() {
		return gmGroupPartMappingVO;
	}
	public void setGmGroupPartMappingVO(
			List<GmGroupPartMappingVO> gmGroupPartMappingVO) {
		this.gmGroupPartMappingVO = gmGroupPartMappingVO;
	}
	public List<GmSystemVO> getGmSystemVO() {
		return gmSystemVO;
	}
	public void setGmSystemVO(List<GmSystemVO> gmSystemVO) {
		this.gmSystemVO = gmSystemVO;
	}
	public List<GmGroupVO> getGmGroupVO() {
		return gmGroupVO;
	}
	public void setGmGroupVO(List<GmGroupVO> gmGroupVO) {
		this.gmGroupVO = gmGroupVO;
	}
	public List<GmPricingReportVO> getGmPricingReportVO() {
		return gmPricingReportVO;
	}
	public void setGmPricingReportVO(List<GmPricingReportVO> gmPricingReportVO) {
		this.gmPricingReportVO = gmPricingReportVO;
	}
	public List<GmSalesAnalysisVO> getGmsalesAnalysisVO() {
		return gmsalesAnalysisVO;
	}
	public void setGmsalesAnalysisVO(List<GmSalesAnalysisVO> gmsalesAnalysisVO) {
		this.gmsalesAnalysisVO = gmsalesAnalysisVO;
	}
	/**
	 * @return the gmPricingSalesHeaderVO
	 */
	public List<GmPricingSalesHeaderVO> getGmPricingSalesHeaderVO() {
		return gmPricingSalesHeaderVO;
	}
	/**
	 * @param gmPricingSalesHeaderVO the gmPricingSalesHeaderVO to set
	 */
	public void setGmPricingSalesHeaderVO(
			List<GmPricingSalesHeaderVO> gmPricingSalesHeaderVO) {
		this.gmPricingSalesHeaderVO = gmPricingSalesHeaderVO;
	}
	
		
	

}
