/**
 * FileName : GmPriceAccountTypeVO.java Description : Access Filter Author : Jreddy Date : Copyright
 * : Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPricingAccessFilterVO extends GmPriceAccountTypeVO {
  private String requestId = "";
  private String initiatedBy = "";
  private String initiatedOn = "";
  private String status = "";
  private String filetrcondition = "";
  
  private String requestid = "";
  private String initiatedby = "";
  private String initiatedon = "";
  private String statusid = "";
  private String groupaccname = "";
  private String fromdate = "";
  
  private String todate = "";
  private String priceAppAccessFl = ""; 
  private String crmAppAccessFl = ""; 
  private String pricerequestid = "";
  
  /**
 * @return the requestid
 */
public String getRequestid() {
	return requestid;
}
/**
 * @param requestid the requestid to set
 */
public void setRequestid(String requestid) {
	this.requestid = requestid;
}

/**
 * @return the initiatedby
 */
public String getInitiatedby() {
	return initiatedby;
}

/**
 * @param initiatedby the initiatedby to set
 */
public void setInitiatedby(String initiatedby) {
	this.initiatedby = initiatedby;
}

/**
 * @return the initiatedon
 */
public String getInitiatedon() {
	return initiatedon;
}

/**
 * @param initiatedon the initiatedon to set
 */
public void setInitiatedon(String initiatedon) {
	this.initiatedon = initiatedon;
}

/**
 * @return the statusid
 */
public String getStatusid() {
	return statusid;
}

/**
 * @param statusid the statusid to set
 */
public void setStatusid(String statusid) {
	this.statusid = statusid;
}

/**
 * @return the groupaccname
 */
public String getGroupaccname() {
	return groupaccname;
}

/**
 * @param groupaccname the groupaccname to set
 */
public void setGroupaccname(String groupaccname) {
	this.groupaccname = groupaccname;
}

/**
 * @return the fromdate
 */
public String getFromdate() {
	return fromdate;
}

/**
 * @param fromdate the fromdate to set
 */
public void setFromdate(String fromdate) {
	this.fromdate = fromdate;
}

/**
 * @return the todate
 */
public String getTodate() {
	return todate;
}

/**
 * @param todate the todate to set
 */
public void setTodate(String todate) {
	this.todate = todate;
}

/**
   * @return the filetrcondition
   */
  public String getFiletrcondition() {
    return filetrcondition;
  }

  /**
   * @param filetrcondition the filetrcondition to set
   */
  public void setFiletrcondition(String filetrcondition) {
    this.filetrcondition = filetrcondition;
  }

  /**
   * @return the requestId
   */
  public String getRequestId() {
    return requestId;
  }

  /**
   * @param requestId the requestId to set
   */
  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  /**
   * @return the initiatedBy
   */
  public String getInitiatedBy() {
    return initiatedBy;
  }

  /**
   * @param initiatedBy the initiatedBy to set
   */
  public void setInitiatedBy(String initiatedBy) {
    this.initiatedBy = initiatedBy;
  }

  /**
   * @return the initiatedOn
   */
  public String getInitiatedOn() {
    return initiatedOn;
  }

  /**
   * @param initiatedOn the initiatedOn to set
   */
  public void setInitiatedOn(String initiatedOn) {
    this.initiatedOn = initiatedOn;
  }

  /**
   * @return the status
   */
  @Override
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  @Override
  public void setStatus(String status) {
    this.status = status;
  }

    /**
     * @return the priceAppAccessFl
     */
    public String getPriceAppAccessFl() {
        return priceAppAccessFl;
    }
    
    /**
     * @param priceAppAccessFl the priceAppAccessFl to set
     */
    public void setPriceAppAccessFl(String priceAppAccessFl) {
        this.priceAppAccessFl = priceAppAccessFl;
    }  
    
    public String getPricerequestid() {
    	return pricerequestid;
    }

    public void setPricerequestid(String pricerequestid) {
    	this.pricerequestid = pricerequestid;
    }

	public String getCrmAppAccessFl() {
		return crmAppAccessFl;
	}
	
	
	public void setCrmAppAccessFl(String crmAppAccessFl) {
		this.crmAppAccessFl = crmAppAccessFl;
	}

}
