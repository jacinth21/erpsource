/**
 * FileName : GmSalesAnalysisVO.java
 * 
 * @Description : Sales Analysis
 * @Author : Karthik
 * @Copyright : Globus Medical Inc.
 * 
 */
package com.globus.valueobject.pricing;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.beans.Transient;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesAnalysisVO extends GmDataStoreVO {

  private String systemid = "";
  private String accountid = "";
  private String accountnm = "";
  private String partyid = "";
  private String salesamount = "";
  private String month = "";
  private String year = "";
  private String segmentid = "";
  private String refid = "";
  private String setid = "";
  private String setnm = "";
  private String segmentnm = "";
  private String partynm = "";
  private String monthyear = "";
	
	  /**
	 * @return the systemid
	 */
	public String getSystemid() {
		return systemid;
	}
	
	
	/**
	 * @param systemid the systemid to set
	 */
	public void setSystemid(String systemid) {
		this.systemid = systemid;
	}
	
	
	/**
	 * @return the accountid
	 */
	public String getAccountid() {
		return accountid;
	}
	
	
	/**
	 * @param accountid the accountid to set
	 */
	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}
	
	
	/**
	 * @return the partyid
	 */
	public String getPartyid() {
		return partyid;
	}
	
	
	/**
	 * @param partyid the partyid to set
	 */
	public void setPartyid(String partyid) {
		this.partyid = partyid;
	}
	
	
	/**
	 * @return the salesamount
	 */
	public String getSalesamount() {
		return salesamount;
	}
	
	
	/**
	 * @param salesamount the salesamount to set
	 */
	public void setSalesamount(String salesamount) {
		this.salesamount = salesamount;
	}
	
	
	/**
	 * @return the month
	 */
	public String getMonth() {
		return month;
	}
	
	
	/**
	 * @param month the month to set
	 */
	public void setMonth(String month) {
		this.month = month;
	}
	
	
	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}
	
	
	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}
		
	/**
	 * @return the segmentid
	 */
	public String getSegmentid() {
		return segmentid;
	}


	/**
	 * @param segmentid the segmentid to set
	 */
	public void setSegmentid(String segmentid) {
		this.segmentid = segmentid;
	}


	/**
	 * @return the refid
	 */
	public String getRefid() {
		return refid;
	}


	/**
	 * @param refid the refid to set
	 */
	public void setRefid(String refid) {
		this.refid = refid;
	}
	
	

	/**
	 * @return the setid
	 */
	public String getSetid() {
		return setid;
	}


	/**
	 * @param setid the setid to set
	 */
	public void setSetid(String setid) {
		this.setid = setid;
	}

	/**
	 * @return the accountnm
	 */
	public String getAccountnm() {
		return accountnm;
	}


	/**
	 * @param accountnm the accountnm to set
	 */
	public void setAccountnm(String accountnm) {
		this.accountnm = accountnm;
	}


	/**
	 * @return the setnm
	 */
	public String getSetnm() {
		return setnm;
	}


	/**
	 * @param setnm the setnm to set
	 */
	public void setSetnm(String setnm) {
		this.setnm = setnm;
	}
	
	/**
	 * @return the segmentnm
	 */
	public String getSegmentnm() {
		return segmentnm;
	}

	/**
	 * @param segmentnm the segmentnm to set
	 */
	public void setSegmentnm(String segmentnm) {
		this.segmentnm = segmentnm;
	}

	/**
	 * @return the partynm
	 */
	public String getPartynm() {
		return partynm;
	}


	/**
	 * @param partynm the partynm to set
	 */
	public void setPartynm(String partynm) {
		this.partynm = partynm;
	}

	/**
	 * @return the monthyear
	 */
	public String getMonthyear() {
		return monthyear;
	}


	/**
	 * @param monthyear the monthyear to set
	 */
	public void setMonthyear(String monthyear) {
		this.monthyear = monthyear;
	}

	@Transient
public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("SYSTEMID", "SYSTEMID");
    properties.put("ACCOUNTID", "ACCOUNTID");
    properties.put("PARTYID", "PARTYID");
    properties.put("SALESAMOUNT", "SALESAMOUNT");
    properties.put("SALESMONTH", "MONTH");
    properties.put("SALESYEAR", "YEAR");
    properties.put("SEGMENTID", "SEGMENTID");
    properties.put("SEGMENTNM", "SEGMENTNM");
    properties.put("REFID", "REFID");
    properties.put("SETID", "SETID");
    properties.put("ACCOUNTNM", "ACCOUNTNM");
    properties.put("SETNM", "SETNM");
    properties.put("PARTYID", "PARTYID");
    properties.put("PARTYNM", "PARTYNM");
    properties.put("MONTH_YEAR", "MONTHYEAR");
    return properties;
  }


}
