/**
 * FileName : GmGroupVO.java Description : for Group Info Author : treddy Date : Copyright : Globus
 * Medical Inc
 */
package com.globus.valueobject.pricing;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmGroupVO extends GmDataVO {

  private String systemid = "";
  private String systemname = "";

  private String groupid = "";
  private String groupname = "";
  private String quantity = "";
  private String proposedunitprice = "";
  private String proposedextensionprice = "";
  private String tripwireunitprice = "";
  private String tripwireextensionprice = "";
  private String currentprice = "";
  private String constructlevel = "";
  private String specialitygroup = "";
  private String group = "";
  private String principalflag = "";
  private String constructname;
  private String listprice = "";
  private String grouptype = "";
  private String pricetype = "";
  private String grouptypedesc = "";
  private String partnum = "";
  private String partdesc = "";
  private String changetype = "";
  private String changevalue = "";
  private String constructid = "";
  private String autopop_fl = "";
  private String gpoprice = "";
  private String statusname = "";
  private String statusid = "";
  private String checkfl = "";
  private String parentgroupid = "";
  private String parentgroupname = "";
  private String setordcnt = "";
  private String prcdetlid = "";
  private String reqdtlid = "";
  private String individualgrpchangevalue = "";
  private String systemdivisionid = "";

  /**
   * @return the prcdetlid
   */
  public String getPrcdetlid() {
    return prcdetlid;
  }

  /**
   * @param prcdetlid the prcdetlid to set
   */
  public void setPrcdetlid(String prcdetlid) {
    this.prcdetlid = prcdetlid;
  }

  /**
   * @return the setordcnt
   */
  public String getSetordcnt() {
    return setordcnt;
  }

  /**
   * @param setordcnt the setordcnt to set
   */
  public void setSetordcnt(String setordcnt) {
    this.setordcnt = setordcnt;
  }

  /**
   * @return the systemid
   */
  public String getSystemid() {
    return systemid;
  }

  /**
   * @param systemid the systemid to set
   */
  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }

  /**
   * @return the systemname
   */
  public String getSystemname() {
    return systemname;
  }

  /**
   * @param systemname the systemname to set
   */
  public void setSystemname(String systemname) {
    this.systemname = systemname;
  }

  /**
   * @return the groupid
   */
  public String getGroupid() {
    return groupid;
  }

  /**
   * @param groupid the groupid to set
   */
  public void setGroupid(String groupid) {
    this.groupid = groupid;
  }

  /**
   * @return the groupname
   */
  public String getGroupname() {
    return groupname;
  }

  /**
   * @param groupname the groupname to set
   */
  public void setGroupname(String groupname) {
    this.groupname = groupname;
  }

  /**
   * @return the quantity
   */
  public String getQuantity() {
    return quantity;
  }

  /**
   * @param quantity the quantity to set
   */
  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }

  /**
   * @return the proposedunitprice
   */
  public String getProposedunitprice() {
    return proposedunitprice;
  }

  /**
   * @param proposedunitprice the proposedunitprice to set
   */
  public void setProposedunitprice(String proposedunitprice) {
    this.proposedunitprice = proposedunitprice;
  }

  /**
   * @return the proposedextensionprice
   */
  public String getProposedextensionprice() {
    return proposedextensionprice;
  }

  /**
   * @param proposedextensionprice the proposedextensionprice to set
   */
  public void setProposedextensionprice(String proposedextensionprice) {
    this.proposedextensionprice = proposedextensionprice;
  }

  /**
   * @return the tripwireunitprice
   */
  public String getTripwireunitprice() {
    return tripwireunitprice;
  }

  /**
   * @param tripwireunitprice the tripwireunitprice to set
   */
  public void setTripwireunitprice(String tripwireunitprice) {
    this.tripwireunitprice = tripwireunitprice;
  }

  /**
   * @return the tripwireextensionprice
   */
  public String getTripwireextensionprice() {
    return tripwireextensionprice;
  }

  /**
   * @param tripwireextensionprice the tripwireextensionprice to set
   */
  public void setTripwireextensionprice(String tripwireextensionprice) {
    this.tripwireextensionprice = tripwireextensionprice;
  }

  /**
   * @return the currentprice
   */
  public String getCurrentprice() {
    return currentprice;
  }

  /**
   * @param currentprice the currentprice to set
   */
  public void setCurrentprice(String currentprice) {
    this.currentprice = currentprice;
  }

  /**
   * @return the constructlevel
   */
  public String getConstructlevel() {
    return constructlevel;
  }

  /**
   * @param constructlevel the constructlevel to set
   */
  public void setConstructlevel(String constructlevel) {
    this.constructlevel = constructlevel;
  }

  /**
   * @return the specialitygroup
   */
  public String getSpecialitygroup() {
    return specialitygroup;
  }

  /**
   * @param specialitygroup the specialitygroup to set
   */
  public void setSpecialitygroup(String specialitygroup) {
    this.specialitygroup = specialitygroup;
  }

  /**
   * @return the group
   */
  public String getGroup() {
    return group;
  }

  /**
   * @param group the group to set
   */
  public void setGroup(String group) {
    this.group = group;
  }

  /**
   * @return the principalflag
   */
  public String getPrincipalflag() {
    return principalflag;
  }

  /**
   * @param principalflag the principalflag to set
   */
  public void setPrincipalflag(String principalflag) {
    this.principalflag = principalflag;
  }

  /**
   * @return the constructname
   */
  public String getConstructname() {
    return constructname;
  }

  /**
   * @param constructname the constructname to set
   */
  public void setConstructname(String constructname) {
    this.constructname = constructname;
  }

  /**
   * @return the listprice
   */
  public String getListprice() {
    return listprice;
  }

  /**
   * @param listprice the listprice to set
   */
  public void setListprice(String listprice) {
    this.listprice = listprice;
  }

  /**
   * @return the grouptype
   */
  public String getGrouptype() {
    return grouptype;
  }

  /**
   * @param grouptype the grouptype to set
   */
  public void setGrouptype(String grouptype) {
    this.grouptype = grouptype;
  }

  /**
   * @return the pricetype
   */
  public String getPricetype() {
    return pricetype;
  }

  /**
   * @param pricetype the pricetype to set
   */
  public void setPricetype(String pricetype) {
    this.pricetype = pricetype;
  }

  /**
   * @return the grouptypedesc
   */
  public String getGrouptypedesc() {
    return grouptypedesc;
  }

  /**
   * @param grouptypedesc the grouptypedesc to set
   */
  public void setGrouptypedesc(String grouptypedesc) {
    this.grouptypedesc = grouptypedesc;
  }

  /**
   * @return the partnum
   */
  public String getPartnum() {
    return partnum;
  }

  /**
   * @param partnum the partnum to set
   */
  public void setPartnum(String partnum) {
    this.partnum = partnum;
  }

  /**
   * @return the partdesc
   */
  public String getPartdesc() {
    return partdesc;
  }

  /**
   * @param partdesc the partdesc to set
   */
  public void setPartdesc(String partdesc) {
    this.partdesc = partdesc;
  }

  /**
   * @return the changetype
   */
  public String getChangetype() {
    return changetype;
  }

  /**
   * @param changetype the changetype to set
   */
  public void setChangetype(String changetype) {
    this.changetype = changetype;
  }

  /**
   * @return the changevalue
   */
  public String getChangevalue() {
    return changevalue;
  }

  /**
   * @param changevalue the changevalue to set
   */
  public void setChangevalue(String changevalue) {
    this.changevalue = changevalue;
  }

  /**
   * @return the constructid
   */
  public String getConstructid() {
    return constructid;
  }

  /**
   * @param constructid the constructid to set
   */
  public void setConstructid(String constructid) {
    this.constructid = constructid;
  }

  public String getAutopop_fl() {
    return autopop_fl;
  }

  public void setAutopop_fl(String autopop_fl) {
    this.autopop_fl = autopop_fl;
  }

  public String getGpoprice() {
    return gpoprice;
  }

  public void setGpoprice(String gpoprice) {
    this.gpoprice = gpoprice;
  }

  /**
   * @return the statusname
   */
  public String getStatusname() {
    return statusname;
  }

  /**
   * @param statusname the statusname to set
   */
  public void setStatusname(String statusname) {
    this.statusname = statusname;
  }

  /**
   * @return the statusid
   */
  public String getStatusid() {
    return statusid;
  }

  /**
   * @param statusid the statusid to set
   */
  public void setStatusid(String statusid) {
    this.statusid = statusid;
  }

  /**
   * @return the checkfl
   */
  public String getCheckfl() {
    return checkfl;
  }

  /**
   * @param checkfl the checkfl to set
   */
  public void setCheckfl(String checkfl) {
    this.checkfl = checkfl;
  }

  /**
   * @return the parentgroupid
   */
  public String getParentgroupid() {
    return parentgroupid;
  }

  /**
   * @param parentgroupid the parentgroupid to set
   */
  public void setParentgroupid(String parentgroupid) {
    this.parentgroupid = parentgroupid;
  }

  /**
   * @return the parentgroupname
   */
  public String getParentgroupname() {
    return parentgroupname;
  }

  /**
   * @param parentgroupname the parentgroupname to set
   */
  public void setParentgroupname(String parentgroupname) {
    this.parentgroupname = parentgroupname;
  }

  /**
   * @return the reqdtlid
   */
  public String getReqdtlid() {
    return reqdtlid;
  }

  /**
   * @param reqdtlid the reqdtlid to set
   */
  public void setReqdtlid(String reqdtlid) {
    this.reqdtlid = reqdtlid;
  }

	 /**
	 * @return the individualgrpchangevalue
	 */
	public String getIndividualgrpchangevalue() {
		return individualgrpchangevalue;
	}
	
	/**
	 * @param individualgrpchangevalue the individualgrpchangevalue to set
	 */
	public void setIndividualgrpchangevalue(String individualgrpchangevalue) {
		this.individualgrpchangevalue = individualgrpchangevalue;
	}
	
	/**
	 * @return the systemdivisionid
	 */
	public String getSystemdivisionid() {
		return systemdivisionid;
	}

	/**
	 * @param systemdivisionid the systemdivisionid to set
	 */
	public void setSystemdivisionid(String systemdivisionid) {
		this.systemdivisionid = systemdivisionid;
	}

@Transient
  public Properties getGroupProperties() {
    Properties properties = new Properties();
    properties.put("SETID", "SYSTEMID");
    properties.put("SETNAME", "SYSTEMNAME");
    properties.put("CONSTRUCTNAME", "CONSTRUCTNAME");
    properties.put("GROUPID", "GROUPID");
    properties.put("GROUPNAME", "GROUPNAME");
    properties.put("QUANTITY", "QUANTITY");
    properties.put("UNITCURRENTPRICE", "CURRENTPRICE");
    properties.put("UNITTRIPWIRE", "TRIPWIREUNITPRICE");
    properties.put("CONSTRUCTLEVEL", "CONSTRUCTLEVEL");
    properties.put("PRINCIPALFLAG", "PRINCIPALFLAG");
    properties.put("PARTYID", "PARTYID");
    properties.put("UNITLISTPRICE", "LISTPRICE");
    properties.put("SPECLGRPFLAG", "SPECIALITYGROUP");
    properties.put("PROPOSEDUNITPRICE", "PROPOSEDUNITPRICE");
    properties.put("GROUPTYPE", "GROUPTYPE");
    properties.put("PRICETYPE", "PRICETYPE");
    properties.put("GROUPTYPEDESC", "GROUPTYPEDESC");
    properties.put("TRIPWIREEXTENSIONPRICE", "TRIPWIREEXTENSIONPRICE");
    properties.put("PARTDESC", "PARTDESC");
    properties.put("PARTNUM", "PARTNUM");
    properties.put("CONSTRUCTID", "CONSTRUCTID");
    properties.put("CHANGETYPENAME", "CHANGETYPE");
    properties.put("CHANGEVALUE", "CHANGEVALUE");
    properties.put("GPOPRICE", "GPOPRICE");
    properties.put("STATUSID", "STATUSID");
    properties.put("STATUSNAME", "STATUSNAME");
    properties.put("PARENTGROUPID", "PARENTGROUPID");
    properties.put("PARENTGROUPNAME", "PARENTGROUPNAME");
    properties.put("SETORDCNT", "SETORDCNT");
    properties.put("PRCDETLID", "PRCDETLID");
    properties.put("REQDTLID", "REQDTLID");
    properties.put("CHECKFL", "CHECKFL");
    properties.put("INDIVIDUALGRPCHANGEVALUE", "INDIVIDUALGRPCHANGEVALUE");
    properties.put("SYSTEMDIVISIONID", "SYSTEMDIVISIONID");
    return properties;
  }

}
