/**
 * FileName : GmImpactAnalysisVO.java
 * 
 * @Description : Impact Analysis (PRT - R203)
 * @Author : Matt B Date
 * @Copyright : Globus Medical Inc.
 * 
 */
package com.globus.valueobject.pricing;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmImpactAnalysisVO extends GmDataStoreVO {

  private String systemname = "";
  private String systemid = "";
  private String accountid = "";
  private String accountname = "";
  private String partid = "";
  private String quantity = "";
  private String orderamt = "";
  private String propprice = "";
  private String refid = "";
  private String projimpact = "";
  private String orgname = "";
  private String adname = "";
  private String currency = "";
  private String gendate = "";
  private String requestid = "";
  private String groupingid = "";
  private String pre12mnthsale = "";
  private String projimpatprpprc = "";
  private String from = "";
  private String cc = "";
  private String checkbtn = "";


  /**
   * @return the groupingid
   */
  public String getGroupingid() {
    return groupingid;
  }

  /**
   * @param groupingid the groupingid to set
   */
  public void setGroupingid(String groupingid) {
    this.groupingid = groupingid;
  }

  /**
   * @return the systemname
   */
  public String getSystemname() {
    return systemname;
  }

  /**
   * @param systemname the systemname to set
   */
  public void setSystemname(String systemname) {
    this.systemname = systemname;
  }

  /**
   * @return the systemid
   */
  public String getSystemid() {
    return systemid;
  }

  /**
   * @param systemid the systemid to set
   */
  public void setSystemid(String systemid) {
    this.systemid = systemid;
  }



  /**
   * @return the accountid
   */
  public String getAccountid() {
    return accountid;
  }

  /**
   * @param accountid the accountid to set
   */
  public void setAccountid(String accountid) {
    this.accountid = accountid;
  }


  /**
   * @return the accountname
   */
  public String getAccountname() {
    return accountname;
  }

  /**
   * @param accountname the accountname to set
   */
  public void setAccountname(String accountname) {
    this.accountname = accountname;
  }

  /**
   * @return the partid
   */
  public String getPartid() {
    return partid;
  }

  /**
   * @param partid the partid to set
   */
  public void setPartid(String partid) {
    this.partid = partid;
  }


  /**
   * @return the quantity
   */
  public String getQuantity() {
    return quantity;
  }

  /**
   * @param quantity the quantity to set
   */
  public void setQuantity(String quantity) {
    this.quantity = quantity;
  }


  /**
   * @return the orderamt
   */
  public String getOrderamt() {
    return orderamt;
  }

  /**
   * @param orderamt the orderamt to set
   */
  public void setOrderamt(String orderamt) {
    this.orderamt = orderamt;
  }

  /**
   * @return the propprice
   */
  public String getPropprice() {
    return propprice;
  }

  /**
   * @param propprice the propprice to set
   */
  public void setPropprice(String propprice) {
    this.propprice = propprice;
  }

  /**
   * @return the refid
   */
  public String getRefid() {
    return refid;
  }

  /**
   * @param refid the refid to set
   */
  public void setRefid(String refid) {
    this.refid = refid;
  }

  /**
   * @return the projimpact
   */
  public String getProjimpact() {
    return projimpact;
  }

  /**
   * @param projimpact the projimpact to set
   */
  public void setProjimpact(String projimpact) {
    this.projimpact = projimpact;
  }

  /**
   * @return the orgname
   */
  public String getOrgname() {
    return orgname;
  }

  /**
   * @param orgname the orgname to set
   */
  public void setOrgname(String orgname) {
    this.orgname = orgname;
  }

  /**
   * @return the adname
   */
  public String getAdname() {
    return adname;
  }

  /**
   * @param adname the adname to set
   */
  public void setAdname(String adname) {
    this.adname = adname;
  }

  /**
   * @return the currency
   */
  public String getCurrency() {
    return currency;
  }

  /**
   * @param currency the currency to set
   */
  public void setCurrency(String currency) {
    this.currency = currency;
  }

  /**
   * @return the gendate
   */
  public String getGendate() {
    return gendate;
  }

  /**
   * @param gendate the gendate to set
   */
  public void setGendate(String gendate) {
    this.gendate = gendate;
  }

  /**
   * @return the requestid
   */
  public String getRequestid() {
    return requestid;
  }

  /**
   * @param requestid the requestid to set
   */
  public void setRequestid(String requestid) {
    this.requestid = requestid;
  }

  /**
   * @return the pre12mnthsale
   */
  public String getPre12mnthsale() {
    return pre12mnthsale;
  }

  /**
   * @param pre12mnthsale the pre12mnthsale to set
   */
  public void setPre12mnthsale(String pre12mnthsale) {
    this.pre12mnthsale = pre12mnthsale;
  }

  /**
   * @return the projimpatprpprc
   */
  public String getProjimpatprpprc() {
    return projimpatprpprc;
  }

  /**
   * @return the from
   */
  public String getFrom() {
    return from;
  }

  /**
   * @param from the from to set
   */
  public void setFrom(String from) {
    this.from = from;
  }

  /**
   * @return the cc
   */
  public String getCc() {
    return cc;
  }

  /**
   * @param cc the cc to set
   */
  public void setCc(String cc) {
    this.cc = cc;
  }

  /**
   * @return the checkbtn
   */
  public String getCheckbtn() {
    return checkbtn;
  }

  /**
   * @param checkbtn the checkbtn to set
   */
  public void setCheckbtn(String checkbtn) {
    this.checkbtn = checkbtn;
  }

  /**
   * @param projimpatprpprc the projimpatprpprc to set
   */
  public void setProjimpatprpprc(String projimpatprpprc) {
    this.projimpatprpprc = projimpatprpprc;
  }

  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("SET_ID", "SYSTEMID");
    properties.put("SET_NAME", "SYSTEMNAME");
    properties.put("ACCOUNT_ID", "ACCOUNTID");
    properties.put("ACCOUNT_NM", "ACCOUNTNAME");
    properties.put("PARTNUM", "PARTID");
    properties.put("ACTUAL_SALE", "ORDERAMT");
    properties.put("PROPOSED_SALE", "PROPPRICE");
    properties.put("PROJ_IMPACT", "PROJIMPACT");
    properties.put("ORG_NAME", "ORGNAME");
    properties.put("AD_NAME", "ADNAME");
    properties.put("AC_CURRENCY", "CURRENCY");
    properties.put("REQUEST_ID", "REQUESTID");
    properties.put("GEN_DATE", "GENDATE");
    properties.put("GROUPING_ID", "GROUPINGID");
    return properties;
  }


}
