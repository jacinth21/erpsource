/**
 * FileName : GmGroupVO.java Description : for System Info Author : treddy,Jreddy Date : Copyright :
 * Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmHeldOrderPriceRequestVO extends GmDataStoreVO {

  private String requestid = "";
  private String initiatedby = "";
  private String initiateddate = "";
  private String status = "";
  private String orderid = "";
	
 /**
 * @return the requestid
 */
public String getRequestid() {
	return requestid;
}



/**
 * @param requestid the requestid to set
 */
public void setRequestid(String requestid) {
	this.requestid = requestid;
}



/**
 * @return the initiatedby
 */
public String getInitiatedby() {
	return initiatedby;
}



/**
 * @param initiatedby the initiatedby to set
 */
public void setInitiatedby(String initiatedby) {
	this.initiatedby = initiatedby;
}



/**
 * @return the initiateddate
 */
public String getInitiateddate() {
	return initiateddate;
}



/**
 * @param initiateddate the initiateddate to set
 */
public void setInitiateddate(String initiateddate) {
	this.initiateddate = initiateddate;
}

/**
 * @return the status
 */
public String getStatus() {
	return status;
}


/**
 * @param status the status to set
 */
public void setStatus(String status) {
	this.status = status;
}

/**
 * @return the orderid
 */
public String getOrderid() {
	return orderid;
}

/**
 * @param orderid the orderid to set
 */
public void setOrderid(String orderid) {
	this.orderid = orderid;
}

@Transient
public Properties getHeldOrderPriceRequest() {
    Properties properties = new Properties();
    properties.put("REQUESTID", "requestid");
    properties.put("INITIATEDDATE", "initiateddate");
    properties.put("INITIATEDBY", "initiatedby");
    properties.put("STATUS", "status");
    properties.put("ORDERID", "orderid");
    
    return properties;
  }

}
