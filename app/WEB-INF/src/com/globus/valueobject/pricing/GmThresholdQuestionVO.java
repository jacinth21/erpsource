/**
 * FileName : GmThresholdQuestionVO.java Description : for Group Info Author : treddy Date :
 * Copyright : Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmThresholdQuestionVO extends GmDataStoreVO {
  private String questionnumber = "";
  private String questiondescription = "";
  private String questiontype = "";
  private String questionanswer = "";
  private String dropdownid = "";
  private String dropdownvalue = "";
  private String questionid = "";
  private String requestid = "";

  /**
   * @return the questionnumber
   */
  public String getQuestionnumber() {
    return questionnumber;
  }

  /**
   * @param questionnumber the questionnumber to set
   */
  public void setQuestionnumber(String questionnumber) {
    this.questionnumber = questionnumber;
  }

  /**
   * @return the questiondescription
   */
  public String getQuestiondescription() {
    return questiondescription;
  }

  /**
   * @param questiondescription the questiondescription to set
   */
  public void setQuestiondescription(String questiondescription) {
    this.questiondescription = questiondescription;
  }

  /**
   * @return the questiontype
   */
  public String getQuestiontype() {
    return questiontype;
  }

  /**
   * @param questiontype the questiontype to set
   */
  public void setQuestiontype(String questiontype) {
    this.questiontype = questiontype;
  }

  /**
   * @return the questionanswer
   */
  public String getQuestionanswer() {
    return questionanswer;
  }

  /**
   * @param questionanswer the questionanswer to set
   */
  public void setQuestionanswer(String questionanswer) {
    this.questionanswer = questionanswer;
  }

  /**
   * @return the dropdownid
   */
  public String getDropdownid() {
    return dropdownid;
  }

  /**
   * @param dropdownid the dropdownid to set
   */
  public void setDropdownid(String dropdownid) {
    this.dropdownid = dropdownid;
  }

  /**
   * @return the dropdownvalue
   */
  public String getDropdownvalue() {
    return dropdownvalue;
  }

  /**
   * @param dropdownvalue the dropdownvalue to set
   */
  public void setDropdownvalue(String dropdownvalue) {
    this.dropdownvalue = dropdownvalue;
  }

  /**
   * @return the questionid
   */
  public String getQuestionid() {
    return questionid;
  }

  /**
   * @param questionid the questionid to set
   */
  public void setQuestionid(String questionid) {
    this.questionid = questionid;
  }

  /**
   * @return the requestid
   */
  public String getRequestid() {
    return requestid;
  }

  /**
   * @param requestid the requestid to set
   */
  public void setRequestid(String requestid) {
    this.requestid = requestid;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("QUESTIONNUMBER", "QUESTIONNUMBER");
    properties.put("QUESTIONDESC", "QUESTIONDESCRIPTION");
    properties.put("QUESTIONTYPE", "QUESTIONTYPE");
    properties.put("QUESTIONID", "QUESTIONID");
    properties.put("CONSULTANT", "DROPDOWNID");
    properties.put("ANSWERDESC", "QUESTIONANSWER");
    properties.put("CONSULTANTNAME", "DROPDOWNVALUE");
    return properties;
  }

}
