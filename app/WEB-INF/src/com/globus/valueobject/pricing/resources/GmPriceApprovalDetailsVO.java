package com.globus.valueobject.pricing.resources;

import com.globus.valueobject.common.GmDataStoreVO;

public class GmPriceApprovalDetailsVO extends GmDataStoreVO {

	private String inputstr = "";
	private String pricerequestid = "";
	private String implementstring = "";
//	private String companyInfo="";
	
	
	/**
	 * @return the companyInfo
	 */
//	public String getCompanyInfo() {
//		return companyInfo;
//	}
	/**
	 * @param companyInfo the companyInfo to set
	 */
//	public void setCompanyInfo(String companyInfo) {
//		this.companyInfo = companyInfo;
//	}
	/**
	 * @return the inputstr
	 */
	public String getInputstr() {
		return inputstr;
	}
	/**
	 * @param inputstr the inputstr to set
	 */
	public void setInputstr(String inputstr) {
		this.inputstr = inputstr;
	}
	/**
	 * @return the pricerequestid
	 */
	public String getPricerequestid() {
		return pricerequestid;
	}
	/**
	 * @param pricerequestid the pricerequestid to set
	 */
	public void setPricerequestid(String pricerequestid) {
		this.pricerequestid = pricerequestid;
	}
	/**
	 * @return the implementstring
	 */
	public String getImplementstring() {
		return implementstring;
	}
	/**
	 * @param implementstring the implementstring to set
	 */
	public void setImplementstring(String implementstring) {
		this.implementstring = implementstring;
	}
	
	
}
