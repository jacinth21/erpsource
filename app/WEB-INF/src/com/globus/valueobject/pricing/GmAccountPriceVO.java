package com.globus.valueobject.pricing;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmAccountPriceVO extends GmDataTstVO {
  private String acctpriceid = "";
  private String acctid = "";
  private String partnum = "";
  private String price = "";
  private String gpoid = "";
  private String grpid = "";
  private String grptype = "";
  private String voidfl = "";
  private String actvfl = "";

  @Transient
  public Properties getAcctPriceProperties() {
    Properties props = new Properties();
    props.put("APID", "ACCTPRICEID");
    props.put("ACCTID", "ACCTID");
    props.put("PNUM", "PARTNUM");
    props.put("PRICE", "PRICE");
    props.put("ACTVFL", "ACTVFL");
    props.put("VOIDFL", "VOIDFL");
    props.put("GRPID", "GRPID");
    props.put("GRPTYPE", "GRPTYPE");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    return props;
  }

  @Transient
  public Properties getGPOPriceProperties() {
    Properties props = new Properties();
    props.put("APID", "ACCTPRICEID");
    props.put("ACCTID", "ACCTID");
    props.put("PNUM", "PARTNUM");
    props.put("PRICE", "PRICE");
    props.put("GRPID", "GRPID");
    props.put("GPOID", "GPOID");
    props.put("GRPTYPE", "GRPTYPE");
    props.put("ACTVFL", "ACTVFL");
    props.put("VOIDFL", "VOIDFL");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    return props;
  }

  public String getAcctpriceid() {
    return acctpriceid;
  }

  public void setAcctpriceid(String acctpriceid) {
    this.acctpriceid = acctpriceid;
  }

  public String getAcctid() {
    return acctid;
  }

  public void setAcctid(String acctid) {
    this.acctid = acctid;
  }

  public String getPartnum() {
    return partnum;
  }

  public void setPartnum(String partnum) {
    this.partnum = partnum;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getGpoid() {
    return gpoid;
  }

  public void setGpoid(String gpoid) {
    this.gpoid = gpoid;
  }

  public String getGrpid() {
    return grpid;
  }

  public void setGrpid(String grpid) {
    this.grpid = grpid;
  }

  public String getGrptype() {
    return grptype;
  }

  public void setGrptype(String grptype) {
    this.grptype = grptype;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  public String getActvfl() {
    return actvfl;
  }

  public void setActvfl(String actvfl) {
    this.actvfl = actvfl;
  }

}
