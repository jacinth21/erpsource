/**
 * FileName : GmAccountPriceRequestVO.java Description : for Group Info Author : treddy Date :
 * Copyright : Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmAccessControlVO extends GmDataStoreVO {

  private String pr_denied_access = "";
  private String pr_apprvl_access = "";
  private String pr_delete_access = "";
  private String pr_status_upd_access = "";
  private String pr_pc_access = "";
  private String pr_gen_pricefile = "";

  /**
   * @return the pr_denied_access
   */
  public String getPr_denied_access() {
    return pr_denied_access;
  }

  /**
   * @param pr_denied_access the pr_denied_access to set
   */
  public void setPr_denied_access(String pr_denied_access) {
    this.pr_denied_access = pr_denied_access;
  }

  /**
   * @return the pr_apprvl_access
   */
  public String getPr_apprvl_access() {
    return pr_apprvl_access;
  }

  /**
   * @param pr_apprvl_access the pr_apprvl_access to set
   */
  public void setPr_apprvl_access(String pr_apprvl_access) {
    this.pr_apprvl_access = pr_apprvl_access;
  }

  /**
   * @return the pr_delete_access
   */
  public String getPr_delete_access() {
    return pr_delete_access;
  }

  /**
   * @param pr_delete_access the pr_delete_access to set
   */
  public void setPr_delete_access(String pr_delete_access) {
    this.pr_delete_access = pr_delete_access;
  }

  /**
   * @return the pr_status_upd_access
   */
  public String getPr_status_upd_access() {
    return pr_status_upd_access;
  }

  /**
   * @param pr_status_upd_access the pr_status_upd_access to set
   */
  public void setPr_status_upd_access(String pr_status_upd_access) {
    this.pr_status_upd_access = pr_status_upd_access;
  }

  /**
   * @return the pr_pc_access
   */
  public String getPr_pc_access() {
    return pr_pc_access;
  }

  /**
   * @param pr_pc_access the pr_pc_access to set
   */
  public void setPr_pc_access(String pr_pc_access) {
    this.pr_pc_access = pr_pc_access;
  }

  /**
   * @return the pr_gen_pricefile
   */
  public String getPr_gen_pricefile() {
    return pr_gen_pricefile;
  }

  /**
   * @param pr_gen_pricefile the pr_gen_pricefile to set
   */
  public void setPr_gen_pricefile(String pr_gen_pricefile) {
    this.pr_gen_pricefile = pr_gen_pricefile;
  }


}
