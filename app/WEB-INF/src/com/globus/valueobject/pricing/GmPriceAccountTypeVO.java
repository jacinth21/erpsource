/**
 * FileName : GmPriceAccountTypeVO.java Description : Account Type Dropdown Author : Jreddy Date :
 * Copyright : Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.globus.valueobject.logon.GmUserVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName("gmPriceAccountTypeVO")
public class GmPriceAccountTypeVO extends GmUserVO {
  private String accid = "";
  private String accname = "";
  private String typeid = "";
  private String searchtext = "";


  /**
   * @return the accid
   */
  public String getAccid() {
    return accid;
  }

  /**
   * @param accid the accid to set
   */
  public void setAccid(String accid) {
    this.accid = accid;
  }

  /**
   * @return the accname
   */
  public String getAccname() {
    return accname;
  }

  /**
   * @param accname the accname to set
   */
  public void setAccname(String accname) {
    this.accname = accname;
  }

  /**
   * @return the typeid
   */
  public String getTypeid() {
    return typeid;
  }

  /**
   * @param typeid the typeid to set
   */
  public void setTypeid(String typeid) {
    this.typeid = typeid;
  }

  /**
   * @return the searchtext
   */
  public String getSearchtext() {
    return searchtext;
  }

  /**
   * @param searchtext the searchtext to set
   */
  public void setSearchtext(String searchtext) {
    this.searchtext = searchtext;
  }

  @Override
  @Transient  
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("ID", "ACCID");
    properties.put("ACNAME", "ACCNAME");
    return properties;
  }

}
