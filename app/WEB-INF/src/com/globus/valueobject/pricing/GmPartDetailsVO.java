package com.globus.valueobject.pricing;
import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
/**
 * @author dsandeep
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPartDetailsVO extends GmDataStoreVO{

	
	private String listprice = "";
	private String pnum="";
	private String twprice="";
	private String system="";
	private String pdesc = "";
	private String uprice = "";
	

	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}
	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	
	/**
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}
	/**
	 * @param system the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}
	
	
	
	/**
	 * @return the listprice
	 */
	public String getListprice() {
		return listprice;
	}
	/**
	 * @param listprice the listprice to set
	 */
	public void setListprice(String listprice) {
		this.listprice = listprice;
	}
	/**
	 * @return the twprice
	 */
	public String getTwprice() {
		return twprice;
	}
	/**
	 * @param twprice the twprice to set
	 */
	public void setTwprice(String twprice) {
		this.twprice = twprice;
	}
	
	
	/**
	 * @return the pdesc
	 */
	public String getPdesc() {
		return pdesc;
	}
	/**
	 * @param pdesc the pdesc to set
	 */
	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}
	/**
	 * @return the uprice
	 */
	public String getUprice() {
		return uprice;
	}
	/**
	 * @param uprice the uprice to set
	 */
	public void setUprice(String uprice) {
		this.uprice = uprice;
	}
	@Transient
	public Properties getPartPricingDetailsVO() {
	    Properties props = new Properties();
	    props.put("PNUM", "PNUM");
	    props.put("SYSNAME", "SYSTEM");
	    props.put("LISTP", "LISTPRICE");
	    props.put("TW", "TWPRICE");
	    props.put("PDESC", "PDESC");
	    props.put("UPRICE", "UPRICE");
	    return props;
	  }
}
