/**
 * FileName : GmGroupVO.java Description : for Group Info Author : treddy Date : Copyright : Globus
 * Medical Inc
 */
package com.globus.valueobject.pricing;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPricingApprovalHeaderVO extends GmDataStoreVO {
	
	private String statusname ="";
	private String statusid ="";
	private String pricerequestid ="";
	private String accountid ="";
	private String initiatedby ="";
	private String initiateddate ="";
	private String last12monthsales ="";
	private String advpaccfl ="";

	
	
  /**
	 * @return the advpaccfl
	 */
	public String getAdvpaccfl() {
		return advpaccfl;
	}


	/**
	 * @param advpaccfl the advpaccfl to set
	 */
	public void setAdvpaccfl(String advpaccfl) {
		this.advpaccfl = advpaccfl;
	}


/**
	 * @return the statusname
	 */
	public String getStatusname() {
		return statusname;
	}


	/**
	 * @param statusname the statusname to set
	 */
	public void setStatusname(String statusname) {
		this.statusname = statusname;
	}


	/**
	 * @return the statusid
	 */
	public String getStatusid() {
		return statusid;
	}


	/**
	 * @param statusid the statusid to set
	 */
	public void setStatusid(String statusid) {
		this.statusid = statusid;
	}


	/**
	 * @return the pricerequestid
	 */
	public String getPricerequestid() {
		return pricerequestid;
	}


	/**
	 * @param pricerequestid the pricerequestid to set
	 */
	public void setPricerequestid(String pricerequestid) {
		this.pricerequestid = pricerequestid;
	}


	/**
	 * @return the accountid
	 */
	public String getAccountid() {
		return accountid;
	}


	/**
	 * @param accountid the accountid to set
	 */
	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}


	/**
	 * @return the initiatedby
	 */
	public String getInitiatedby() {
		return initiatedby;
	}


	/**
	 * @param initiatedby the initiatedby to set
	 */
	public void setInitiatedby(String initiatedby) {
		this.initiatedby = initiatedby;
	}


	/**
	 * @return the initiateddate
	 */
	public String getInitiateddate() {
		return initiateddate;
	}


	/**
	 * @param initiateddate the initiateddate to set
	 */
	public void setInitiateddate(String initiateddate) {
		this.initiateddate = initiateddate;
	}


	/**
	 * @return the last12monthsales
	 */
	public String getLast12monthsales() {
		return last12monthsales;
	}


	/**
	 * @param last12monthsales the last12monthsales to set
	 */
	public void setLast12monthsales(String last12monthsales) {
		this.last12monthsales = last12monthsales;
	}

@Transient	
public Properties getPricingApprovalRptProperties() {
    Properties properties = new Properties();
    properties.put("STATUSNAME", "STATUSNAME");
    properties.put("STATUSID", "STATUSID");
    properties.put("PRICEREQUESTID", "PRICEREQUESTID");
    properties.put("ACCOUNTID", "ACCOUNTID");
    properties.put("INITIATEDBY", "INITIATEDBY");
    properties.put("INITIATEDDATE", "INITIATEDDATE"); 
    properties.put("LAST12MONTHSALES", "LAST12MONTHSALES");
    properties.put("ADVPACCFL", "ADVPACCFL");
    return properties;
  }

}
