/**
 * FileName : GmAccountPriceRequestVO.java Description : for Group Info Author : treddy Date :
 * Copyright : Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmAccountPriceRequestVO extends GmDataStoreVO {
  private String requestId = "";
  private String requestType = "";
  private String requestStatus = "";
  private String companyId = "";
  private String last12MonthsSale = "";
  private String projected12MonthsSale = "";
  private String gpoReferenceId = "";
  private String gpoReferenceDetailId = "";
  private String salesRepId = "";
  private String adId = "";
  private String vpAD = "";
  private String initiatedBy = "";
  private String createdBy = "";
  private String lastUpdatedBy = "";
  private String pricerequestid = "";
  private String selectedSystems = "";
  private String partyname = "";
  private String typeid = "";
  private String questioncnt = "";
  private String statusid = "";
  private String documentcount = "";
  private String impactcount = "";


  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }

  public String getRequestType() {
    return requestType;
  }

  public void setRequestType(String requestType) {
    this.requestType = requestType;
  }

  public String getRequestStatus() {
    return requestStatus;
  }

  public void setRequestStatus(String requestStatus) {
    this.requestStatus = requestStatus;
  }

  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public String getLast12MonthsSale() {
    return last12MonthsSale;
  }

  public void setLast12MonthsSale(String last12MonthsSale) {
    this.last12MonthsSale = last12MonthsSale;
  }

  public String getProjected12MonthsSale() {
    return projected12MonthsSale;
  }

  public void setProjected12MonthsSale(String projected12MonthsSale) {
    this.projected12MonthsSale = projected12MonthsSale;
  }

  public String getGpoReferenceId() {
    return gpoReferenceId;
  }

  public void setGpoReferenceId(String gpoReferenceId) {
    this.gpoReferenceId = gpoReferenceId;
  }

  public String getGpoReferenceDetailId() {
    return gpoReferenceDetailId;
  }

  public void setGpoReferenceDetailId(String gpoReferenceDetailId) {
    this.gpoReferenceDetailId = gpoReferenceDetailId;
  }

  public String getSalesRepId() {
    return salesRepId;
  }

  public void setSalesRepId(String salesRepId) {
    this.salesRepId = salesRepId;
  }

  public String getAdId() {
    return adId;
  }

  public void setAdId(String adId) {
    this.adId = adId;
  }

  public String getVpAD() {
    return vpAD;
  }

  public void setVpAD(String vpAD) {
    this.vpAD = vpAD;
  }

  public String getInitiatedBy() {
    return initiatedBy;
  }

  public void setInitiatedBy(String initiatedBy) {
    this.initiatedBy = initiatedBy;
  }

  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getLastUpdatedBy() {
    return lastUpdatedBy;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  /**
   * @return the pricerequestid
   */
  public String getPricerequestid() {
    return pricerequestid;
  }

  /**
   * @param pricerequestid the pricerequestid to set
   */
  public void setPricerequestid(String pricerequestid) {
    this.pricerequestid = pricerequestid;
  }

  public String getSelectedSystems() {
    return selectedSystems;
  }

  public void setSelectedSystems(String selectedSystems) {
    this.selectedSystems = selectedSystems;
  }

  public String getPartyname() {
    return partyname;
  }

  public void setPartyname(String partyname) {
    this.partyname = partyname;
  }

  /**
   * @return the typeid
   */
  public String getTypeid() {
    return typeid;
  }

  /**
   * @param typeid the typeid to set
   */
  public void setTypeid(String typeid) {
    this.typeid = typeid;
  }

  /**
   * @return the questioncnt
   */
  public String getQuestioncnt() {
    return questioncnt;
  }

  /**
   * @param questioncnt the questioncnt to set
   */
  public void setQuestioncnt(String questioncnt) {
    this.questioncnt = questioncnt;
  }

  /**
   * @return the statusid
   */
  public String getStatusid() {
    return statusid;
  }

  /**
   * @param statusid the statusid to set
   */
  public void setStatusid(String statusid) {
    this.statusid = statusid;
  }

  /**
   * @return the documentcount
   */
  public String getDocumentcount() {
    return documentcount;
  }

  /**
   * @param documentcount the documentcount to set
   */
  public void setDocumentcount(String documentcount) {
    this.documentcount = documentcount;
  }

  /**
   * @return the impactcount
   */
  public String getImpactcount() {
    return impactcount;
  }

  /**
   * @param impactcount the impactcount to set
   */
  public void setImpactcount(String impactcount) {
    this.impactcount = impactcount;
  }

}
