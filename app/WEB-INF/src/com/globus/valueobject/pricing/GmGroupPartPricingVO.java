package com.globus.valueobject.pricing;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmCodeLookupVO;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author dsandeep
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmGroupPartPricingVO extends  GmDataStoreVO {
	
	//private String pricetypenm = "";
	private GmGroupDetailsVO[] gmGroupVO;
	private GmPartDetailsVO[] gmPartVO;
	private String partNum = "";
	private String search = "";
	private List<com.globus.valueobject.pricing.GmGroupDetailsVO> listGmGruouVO = null;
	private List<com.globus.valueobject.pricing.GmPartDetailsVO> listGmPartVO = null;
	
	private String system = "";
	private String accId ="";
	
	
	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}


	public String getAccId() {
		return accId;
	}

	public void setAccId(String accId) {
		this.accId = accId;
	}

	/**
	 * @return the partNum
	 */
	public String getPartNum() {
		return partNum;
	}

	/**
	 * @return the gmGroupVO
	 */
	public GmGroupDetailsVO[] getGmGroupVO() {
		return gmGroupVO;
	}

	/**
	 * @param gmGroupVO the gmGroupVO to set
	 */
	public void setGmGroupVO(GmGroupDetailsVO[] gmGroupVO) {
		this.gmGroupVO = gmGroupVO;
	}

	/**
	 * @return the gmPartVO
	 */
	public GmPartDetailsVO[] getGmPartVO() {
		return gmPartVO;
	}

	/**
	 * @param gmPartVO the gmPartVO to set
	 */
	public void setGmPartVO(GmPartDetailsVO[] gmPartVO) {
		this.gmPartVO = gmPartVO;
	}

	/**
	 * @param partNum the partNum to set
	 */
	public void setPartNum(String partNum) {
		this.partNum = partNum;
	}

	/**
	 * @return the search
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * @param search the search to set
	 */
	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * @return the listGmGruouVO
	 */
	public List<GmGroupDetailsVO> getListGmGruouVO() {
		return listGmGruouVO;
	}

	/**
	 * @param listGmGruouVO the listGmGruouVO to set
	 */
	public void setListGmGruouVO(List<GmGroupDetailsVO> listGmGruouVO) {
		this.listGmGruouVO = listGmGruouVO;
	}

	/**
	 * @return the listGmPartVO
	 */
	public List<GmPartDetailsVO> getListGmPartVO() {
		return listGmPartVO;
	}

	/**
	 * @param listGmPartVO the listGmPartVO to set
	 */
	public void setListGmPartVO(List<GmPartDetailsVO> listGmPartVO) {
		this.listGmPartVO = listGmPartVO;
	}

	}
