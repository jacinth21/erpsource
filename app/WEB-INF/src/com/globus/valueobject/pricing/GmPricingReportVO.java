/**
 * FileName : GmAccountDetailsListVO.java Description : for Account Price Info Author : hReddy Date
 * : Copyright : Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * @author hreddy
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPricingReportVO extends GmAccountDetailsListVO {
  private String requestid = "";
  private String initiatedby = "";
  private String initiatedon = "";
  private String status = "";
  private String statusid = "";
  private String groupaccname = "";
  private String fromdate = "";
  private String todate = "";
  private String  regname = "";
  private String acid = "";
  private String deptid = "";
  private String userid = "";
  private String statusupd = "";
  
  
  
  
  
  /**
 * @return the regname
 */
public String getRegname() {
	return regname;
}

/**
 * @param regname the regname to set
 */
public void setRegname(String regname) {
	this.regname = regname;
}

/**
   * @return the acid
   */
  public String getAcid() {
    return acid;
  }

  /**
   * @param acid the acid to set
   */
  public void setAcid(String acid) {
    this.acid = acid;
  }

  /**
   * @return the deptid
   */
  public String getDeptid() {
    return deptid;
  }

  /**
   * @param deptid the deptid to set
   */
  public void setDeptid(String deptid) {
    this.deptid = deptid;
  }

  /**
   * @return the userid
   */
  @Override
  public String getUserid() {
    return userid;
  }

  /**
   * @param userid the userid to set
   */
  @Override
  public void setUserid(String userid) {
    this.userid = userid;
  }

  /**
   * @return the statusid
   */
  public String getStatusid() {
    return statusid;
  }

  /**
   * @param statusid the statusid to set
   */
  public void setStatusid(String statusid) {
    this.statusid = statusid;
  }

  /**
   * @return the requestid
   */
  public String getRequestid() {
    return requestid;
  }

  /**
   * @param requestid the requestid to set
   */
  public void setRequestid(String requestid) {
    this.requestid = requestid;
  }

  /**
   * @return the initiatedby
   */
  public String getInitiatedby() {
    return initiatedby;
  }

  /**
   * @param initiatedby the initiatedby to set
   */
  public void setInitiatedby(String initiatedby) {
    this.initiatedby = initiatedby;
  }

  /**
   * @return the initiatedon
   */
  public String getInitiatedon() {
    return initiatedon;
  }

  /**
   * @param initiatedon the initiatedon to set
   */
  public void setInitiatedon(String initiatedon) {
    this.initiatedon = initiatedon;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the groupaccname
   */
  public String getGroupaccname() {
    return groupaccname;
  }

  /**
   * @param groupaccname the groupaccname to set
   */
  public void setGroupaccname(String groupaccname) {
    this.groupaccname = groupaccname;
  }

  /**
   * @return the fromdate
   */
  public String getFromdate() {
    return fromdate;
  }

  /**
   * @param fromdate the fromdate to set
   */
  public void setFromdate(String fromdate) {
    this.fromdate = fromdate;
  }

  /**
   * @return the todate
   */
  public String getTodate() {
    return todate;
  }

  /**
   * @param todate the todate to set
   */
  public void setTodate(String todate) {
    this.todate = todate;
  }



/**
 * @return the statusupd
 */
public String getStatusupd() {
	return statusupd;
}

/**
 * @param statusupd the statusupd to set
 */
public void setStatusupd(String statusupd) {
	this.statusupd = statusupd;
}

@Transient
  public Properties getPriceReportProperties() {
    Properties properties = new Properties();
    properties.put("REQID", "REQUESTID");
    properties.put("INITIATEDBY", "INITIATEDBY");
    properties.put("INITIATEDON", "INITIATEDON");
    properties.put("STATUS", "STATUS");
    properties.put("GROUPACCNAME", "GROUPACCNAME");
    properties.put("STATUSID", "STATUSID");
    properties.put("REGNAME", "REGNAME");
    properties.put("STATUSUPD", "STATUSUPD");

    return properties;
  }

}
