/**
 * FileName : GmGroupVO.java Description : for System Info Author : treddy,Jreddy Date : Copyright :
 * Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSystemVO extends GmDataStoreVO {

  private String systemId = "";
  private String systemName = "";
  private String changetype = "";
  private String changevalue = "";
  private String changetypename = "";
  private String userid = "";
  private String searchtext = "";
  private String errorsystemId = "";



  public String getSystemId() {
    return systemId;
  }

  public void setSystemId(String systemId) {
    this.systemId = systemId;
  }

  public String getSystemName() {
    return systemName;
  }

  public void setSystemName(String systemName) {
    this.systemName = systemName;
  }

  public String getChangetype() {
    return changetype;
  }

  public void setChangetype(String changetype) {
    this.changetype = changetype;
  }

  public String getChangevalue() {
    return changevalue;
  }

  public void setChangevalue(String changevalue) {
    this.changevalue = changevalue;
  }

  @Override
  public String getUserid() {
    return userid;
  }

  @Override
  public void setUserid(String userid) {
    this.userid = userid;
  }

  /**
   * @return the changetypename
   */
  public String getChangetypename() {
    return changetypename;
  }

  /**
   * @param changetypename the changetypename to set
   */
  public void setChangetypename(String changetypename) {
    this.changetypename = changetypename;
  }

  /**
   * @return the searchtext
   */
  public String getSearchtext() {
    return searchtext;
  }

  /**
   * @param searchtext the searchtext to set
   */
  public void setSearchtext(String searchtext) {
    this.searchtext = searchtext;
  }
  
	/**
	 * @return the errorsystemId
	 */
	public String getErrorsystemId() {
		return errorsystemId;
	}
	
	/**
	 * @param errorsystemId the errorsystemId to set
	 */
	public void setErrorsystemId(String errorsystemId) {
		this.errorsystemId = errorsystemId;
	}
	
 @Transient
public Properties getSystemProperties() {
    Properties properties = new Properties();
    properties.put("SETID", "systemId");
    properties.put("SETNAME", "systemName");
    properties.put("CHANGETYPE", "changetype");
    properties.put("CHANGEVALUE", "changevalue");
    properties.put("CHANGETYPENAME", "changetypename");
    properties.put("ERRORSYSTEMID", "errorsystemId");

    return properties;
  }

  // Load GmPricingRequestBean.loadSystemsList() method
  @Transient
  public Properties getPriceSysProperties() {
    Properties properties = new Properties();
    properties.put("ID", "systemId");
    properties.put("NAME", "systemName");
    return properties;
  }

}
