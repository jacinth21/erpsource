/**
 * FileName : GmGroupPartMappingVO.java Description : for 'P' icon details Author : Jreddy Date :
 * Copyright : Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmGroupPartMappingVO extends GmDataStoreVO {
  private String groupid = "";
  private String groupname = "";
  private String partid = "";
  private String partdesc = "";
  private String listprice = "";
  private String tripwireprice = "";

  /**
   * @return the groupid
   */
  public String getGroupid() {
    return groupid;
  }

  /**
   * @param groupid the groupid to set
   */
  public void setGroupid(String groupid) {
    this.groupid = groupid;
  }

  /**
   * @return the groupname
   */
  public String getGroupname() {
    return groupname;
  }

  /**
   * @param groupname the groupname to set
   */
  public void setGroupname(String groupname) {
    this.groupname = groupname;
  }

  /**
   * @return the partid
   */
  public String getPartid() {
    return partid;
  }

  /**
   * @param partid the partid to set
   */
  public void setPartid(String partid) {
    this.partid = partid;
  }

  /**
   * @return the partdesc
   */
  public String getPartdesc() {
    return partdesc;
  }

  /**
   * @param partdesc the partdesc to set
   */
  public void setPartdesc(String partdesc) {
    this.partdesc = partdesc;
  }

  /**
   * @return the listprice
   */
  public String getListprice() {
    return listprice;
  }

  /**
   * @param listprice the listprice to set
   */
  public void setListprice(String listprice) {
    this.listprice = listprice;
  }

  /**
   * @return the tripwireprice
   */
  public String getTripwireprice() {
    return tripwireprice;
  }

  /**
   * @param tripwireprice the tripwireprice to set
   */
  public void setTripwireprice(String tripwireprice) {
    this.tripwireprice = tripwireprice;
  }

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("GROUPID", "GROUPID");
    properties.put("GROUPNAME", "GROUPNAME");
    properties.put("PNUM", "PARTID");
    properties.put("PDESC", "PARTDESC");
    properties.put("LISTPRICE", "LISTPRICE");
    properties.put("TRIPWIREPRICE", "TRIPWIREPRICE");
    return properties;
  }

}
