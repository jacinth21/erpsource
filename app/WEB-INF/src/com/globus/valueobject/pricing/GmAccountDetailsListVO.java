/**
 * FileName : GmAccountDetailsListVO.java Description : for Account Price Info Author : Jreddy Date
 * : Copyright : Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmAccountDetailsListVO extends GmDataStoreVO {
  private String accid = "";
  private String accountname = "";
  private String adname = "";
  private String distributorname = "";
  private String territoryname = "";
  private String repname = "";
  private String vpnname = "";
  private String gpoaff = "";
  private String contactaccount = "";
  private String currency = "";
  private String gpopricebook = "";
  private String twlmnthsale = "";
  private String idn = "";
  private String region = "";
  private String typeid = "";
  private String last12monthsales="";

  

/**
   * @return the accid
   */
  public String getAccid() {
    return accid;
  }

  /**
   * @param accid the accid to set
   */
  public void setAccid(String accid) {
    this.accid = accid;
  }

  /**
   * @return the accountname
   */
  public String getAccountname() {
    return accountname;
  }

  /**
   * @param accountname the accountname to set
   */
  public void setAccountname(String accountname) {
    this.accountname = accountname;
  }

  /**
   * @return the adname
   */
  public String getAdname() {
    return adname;
  }

  /**
   * @param adname the adname to set
   */
  public void setAdname(String adname) {
    this.adname = adname;
  }

  /**
   * @return the distributorname
   */
  public String getDistributorname() {
    return distributorname;
  }

  /**
   * @param distributorname the distributorname to set
   */
  public void setDistributorname(String distributorname) {
    this.distributorname = distributorname;
  }

  /**
   * @return the territoryname
   */
  public String getTerritoryname() {
    return territoryname;
  }

  /**
   * @param territoryname the territoryname to set
   */
  public void setTerritoryname(String territoryname) {
    this.territoryname = territoryname;
  }

  /**
   * @return the repname
   */
  public String getRepname() {
    return repname;
  }

  /**
   * @param repname the repname to set
   */
  public void setRepname(String repname) {
    this.repname = repname;
  }

  /**
   * @return the vpnname
   */
  public String getVpnname() {
    return vpnname;
  }

  /**
   * @param vpnname the vpnname to set
   */
  public void setVpnname(String vpnname) {
    this.vpnname = vpnname;
  }

  /**
   * @return the gpoaff
   */
  public String getGpoaff() {
    return gpoaff;
  }

  /**
   * @param gpoaff the gpoaff to set
   */
  public void setGpoaff(String gpoaff) {
    this.gpoaff = gpoaff;
  }

  /**
   * @return the contactaccount
   */
  public String getContactaccount() {
    return contactaccount;
  }

  /**
   * @param contactaccount the contactaccount to set
   */
  public void setContactaccount(String contactaccount) {
    this.contactaccount = contactaccount;
  }

  /**
   * @return the currency
   */
  public String getCurrency() {
    return currency;
  }

  /**
   * @param currency the currency to set
   */
  public void setCurrency(String currency) {
    this.currency = currency;
  }

  /**
   * @return the gpopricebook
   */
  public String getGpopricebook() {
    return gpopricebook;
  }

  /**
   * @param gpopricebook the gpopricebook to set
   */
  public void setGpopricebook(String gpopricebook) {
    this.gpopricebook = gpopricebook;
  }

  /**
   * @return the twlmnthsale
   */
  public String getTwlmnthsale() {
    return twlmnthsale;
  }

  /**
   * @param twlmnthsale the twlmnthsale to set
   */
  public void setTwlmnthsale(String twlmnthsale) {
    this.twlmnthsale = twlmnthsale;
  }

  /**
   * @return the idn
   */
  public String getIdn() {
    return idn;
  }

  /**
   * @param idn the idn to set
   */
  public void setIdn(String idn) {
    this.idn = idn;
  }

  /**
   * @return the region
   */
  public String getRegion() {
    return region;
  }

  /**
   * @param region the region to set
   */
  public void setRegion(String region) {
    this.region = region;
  }

  /**
   * @return the typeid
   */
  public String getTypeid() {
    return typeid;
  }

  /**
   * @param typeid the typeid to set
   */
  public void setTypeid(String typeid) {
    this.typeid = typeid;
  }
  
  public String getLast12monthsales() {
		return last12monthsales;
	}

  public void setLast12monthsales(String last12monthsales) {
		this.last12monthsales = last12monthsales;
	}

@Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("ACCID", "ACCID");
    properties.put("STRPARENTACC", "ACCOUNTNAME");
    properties.put("STRADNAME", "ADNAME");
    properties.put("STRDNAME", "DISTRIBUTORNAME");
    properties.put("STRTERNAME", "TERRITORYNAME");
    properties.put("STRREPNAME", "REPNAME");
    properties.put("STRVPNAME", "VPNNAME");
    properties.put("STRHLCAFF", "GPOAFF");
    properties.put("CONTRACTFLNAME", "CONTACTACCOUNT");
    properties.put("STRCURRENCY", "CURRENCY");
    properties.put("STRGPB", "GPOPRICEBOOK");
    properties.put("STRGPRAFF", "IDN");
    properties.put("STRREGNAME", "REGION");
    properties.put("LAST12MONTHSALES", "LAST12MONTHSALES");

    return properties;
  }

}
