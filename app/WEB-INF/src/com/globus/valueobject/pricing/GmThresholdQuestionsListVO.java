/**
 * FileName : GmThresholdQuestionsListVO.java Description : for Group Info Author : treddy Date :
 * Copyright : Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmThresholdQuestionsListVO extends GmDataStoreVO {
  private GmThresholdQuestionVO[] gmThresholdQuestionVO;
  private String token;
  private String repId;

  public GmThresholdQuestionVO[] getGmThresholdQuestionVO() {
    return gmThresholdQuestionVO;
  }

  public void setGmThresholdQuestionsVO(GmThresholdQuestionVO[] gmThresholdQuestionVO) {
    this.gmThresholdQuestionVO = gmThresholdQuestionVO;
  }

  @Override
  public String getToken() {
    return token;
  }

  @Override
  public void setToken(String token) {
    this.token = token;
  }

  public String getRepId() {
    return repId;
  }

  public void setRepId(String repId) {
    this.repId = repId;
  }
}
