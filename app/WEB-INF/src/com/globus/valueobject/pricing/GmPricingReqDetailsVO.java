/**
 * FileName : GmPricingReqDetailsVO.java Description : for Group Info Author : treddy Date :
 * Copyright : Globus Medical Inc
 */
package com.globus.valueobject.pricing;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPricingReqDetailsVO extends GmDataStoreVO {
  private GmAccountPriceRequestVO gmAccountPriceRequestVO;
  private GmSystemVO[] gmSystemVO;
  private GmGroupVO[] gmGroupVO;
  private GmThresholdQuestionVO[] gmThresholdQuestionVO;
  private String token;
  private String pricerequestid;
  private String pricerequeststatus;
  private List<GmSystemVO> listGmSystemVO = null;
  private List<GmGroupVO> listGmGroupVO = null;
  private List<GmPricingApprovalHeaderVO> listgmPricingApprovalHeaderVO = null;
  private GmHeldOrderPriceRequestVO[] gmHeldOrderPriceRequestVO;
  private List<GmHeldOrderPriceRequestVO> listgmHeldOrderPriceRequestVO = null;
  private String userid;
  private String acid;
  private String deptid;
  private String orderids;
  
  
public GmAccountPriceRequestVO getGmAccountPriceRequestVO() {
    return gmAccountPriceRequestVO;
  }

  public String getUserid() {
	return userid;
}

public void setUserid(String userid) {
	this.userid = userid;
}

public void setGmAccountPriceRequestVO(GmAccountPriceRequestVO gmAccountPriceRequestVO) {
    this.gmAccountPriceRequestVO = gmAccountPriceRequestVO;
  }

  public GmSystemVO[] getGmSystemVO() {
    return gmSystemVO;
  }

  public void setGmSystemVO(GmSystemVO[] gmSystemVO) {
    this.gmSystemVO = gmSystemVO;
  }

  public GmGroupVO[] getGmGroupVO() {
    return gmGroupVO;
  }

  public void setGmGroupVO(GmGroupVO[] gmGroupVO) {
    this.gmGroupVO = gmGroupVO;
  }

  @Override
  public String getToken() {
    return token;
  }

  @Override
  public void setToken(String token) {
    this.token = token;
  }

  public String getPricerequestid() {
    return pricerequestid;
  }

  public void setPriceequestid(String requestid) {
    this.pricerequestid = requestid;
  }

  /**
   * @return the gmThresholdQuestionVO
   */
  public GmThresholdQuestionVO[] getGmThresholdQuestionVO() {
    return gmThresholdQuestionVO;
  }

  /**
   * @param gmThresholdQuestionVO the gmThresholdQuestionVO to set
   */
  public void setGmThresholdQuestionVO(GmThresholdQuestionVO[] gmThresholdQuestionVO) {
    this.gmThresholdQuestionVO = gmThresholdQuestionVO;
  }

  public List<GmSystemVO> getListGmSystemVO() {
    return listGmSystemVO;
  }

  public void setListGmSystemVO(List<GmSystemVO> listGmSystemVO) {
    this.listGmSystemVO = listGmSystemVO;
  }

  public List<GmGroupVO> getListGmGroupVO() {
    return listGmGroupVO;
  }

  public void setListGmGroupVO(List<GmGroupVO> listGmGroupVO) {
    this.listGmGroupVO = listGmGroupVO;
  }

  /**
   * @return the pricerequeststatus
   */
  public String getPricerequeststatus() {
    return pricerequeststatus;
  }

  /**
   * @param pricerequeststatus the pricerequeststatus to set
   */
  public void setPricerequeststatus(String pricerequeststatus) {
    this.pricerequeststatus = pricerequeststatus;
  }
  
  public String getAcid() {
		return acid;
	}

	public void setAcid(String acid) {
		this.acid = acid;
	}

	public String getDeptid() {
		return deptid;
	}

	public void setDeptid(String deptid) {
		this.deptid = deptid;
	}

	/**
	 * @return the listgmPricingApprovalHeaderVO
	 */
	public List<GmPricingApprovalHeaderVO> getListgmPricingApprovalHeaderVO() {
		return listgmPricingApprovalHeaderVO;
	}

	/**
	 * @param listgmPricingApprovalHeaderVO the listgmPricingApprovalHeaderVO to set
	 */
	public void setListgmPricingApprovalHeaderVO(
			List<GmPricingApprovalHeaderVO> listgmPricingApprovalHeaderVO) {
		this.listgmPricingApprovalHeaderVO = listgmPricingApprovalHeaderVO;
	}

	/**
	 * @param pricerequestid the pricerequestid to set
	 */
	public void setPricerequestid(String pricerequestid) {
		this.pricerequestid = pricerequestid;
	}

	/**
	 * @return the orderids
	 */
	public String getOrderids() {
		return orderids;
	}

	/**
	 * @param orderids the orderids to set
	 */
	public void setOrderids(String orderids) {
		this.orderids = orderids;
	}

	/**
	 * @return the gmHeldOrderPriceRequestVO
	 */
	public GmHeldOrderPriceRequestVO[] getGmHeldOrderPriceRequestVO() {
		return gmHeldOrderPriceRequestVO;
	}

	/**
	 * @param gmHeldOrderPriceRequestVO the gmHeldOrderPriceRequestVO to set
	 */
	public void setGmHeldOrderPriceRequestVO(
			GmHeldOrderPriceRequestVO[] gmHeldOrderPriceRequestVO) {
		this.gmHeldOrderPriceRequestVO = gmHeldOrderPriceRequestVO;
	}

	/**
	 * @return the listgmHeldOrderPriceRequestVO
	 */
	public List<GmHeldOrderPriceRequestVO> getListgmHeldOrderPriceRequestVO() {
		return listgmHeldOrderPriceRequestVO;
	}

	/**
	 * @param listgmHeldOrderPriceRequestVO the listgmHeldOrderPriceRequestVO to set
	 */
	public void setListgmHeldOrderPriceRequestVO(
			List<GmHeldOrderPriceRequestVO> listgmHeldOrderPriceRequestVO) {
		this.listgmHeldOrderPriceRequestVO = listgmHeldOrderPriceRequestVO;
	}
	
	
}
