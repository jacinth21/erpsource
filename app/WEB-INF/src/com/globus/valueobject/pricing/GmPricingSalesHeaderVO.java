/**
 * FileName : GmPricingSalesHeaderVO.java Description : for Group Info Author : karthik Date : Copyright : Globus
 * Medical Inc
 */
package com.globus.valueobject.pricing;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.beans.Transient;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPricingSalesHeaderVO extends GmDataStoreVO {
	
	  private String partynm = "";
	  private String orgname = "";
	  private String gendate = "";
	  private String accurrency = "";
	  private String last12monthsales = "";

	  
	/**
	 * @return the partynm
	 */
	public String getPartynm() {
		return partynm;
	}


	/**
	 * @param partynm the partynm to set
	 */
	public void setPartynm(String partynm) {
		this.partynm = partynm;
	}


	/**
	 * @return the orgname
	 */
	public String getOrgname() {
		return orgname;
	}


	/**
	 * @param orgname the orgname to set
	 */
	public void setOrgname(String orgname) {
		this.orgname = orgname;
	}


	/**
	 * @return the gendate
	 */
	public String getGendate() {
		return gendate;
	}


	/**
	 * @param gendate the gendate to set
	 */
	public void setGendate(String gendate) {
		this.gendate = gendate;
	}


	/**
	 * @return the accurrency
	 */
	public String getAccurrency() {
		return accurrency;
	}


	/**
	 * @param accurrency the accurrency to set
	 */
	public void setAccurrency(String accurrency) {
		this.accurrency = accurrency;
	}


	/**
	 * @return the last12monthsales
	 */
	public String getLast12monthsales() {
		return last12monthsales;
	}


	/**
	 * @param last12monthsales the last12monthsales to set
	 */
	public void setLast12monthsales(String last12monthsales) {
		this.last12monthsales = last12monthsales;
	}
	
	@Transient
	public Properties getPricingHeaderProperties() {
	    Properties properties = new Properties();
	    properties.put("PARTYNM", "PARTYNM");
	    properties.put("ORG_NAME", "ORGNAME");
	    properties.put("GEN_DATE", "GENDATE");
	    properties.put("AC_CURRENCY", "ACCURRENCY");
	    properties.put("LAST12MONTHSALES", "LAST12MONTHSALES");
	    return properties;
   }

}
