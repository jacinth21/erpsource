package com.globus.valueobject.accounts;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.valueobject.prodmgmnt.GmSharedEmailVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCancelTaxResponseVo {
  GmCancelTaxResultVo cancelTaxResult;

  /**
   * @return the cancelTaxResult
   */
  public GmCancelTaxResultVo getCancelTaxResult() {
    return cancelTaxResult;
  }

  /**
   * @param cancelTaxResult the cancelTaxResult to set
   */
  public void setCancelTaxResult(GmCancelTaxResultVo cancelTaxResult) {
    this.cancelTaxResult = cancelTaxResult;
  }
	
	
}