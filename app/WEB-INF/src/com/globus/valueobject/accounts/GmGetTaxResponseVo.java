package com.globus.valueobject.accounts;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.valueobject.prodmgmnt.GmSharedEmailVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmGetTaxResponseVo {
	
	private String docCode = "";
	private String docDate = "";
	private String timestamp = "";
	private String totalAmount = "";
	private String totalDiscount = "";
	private String totalExemption = "";
	private String totalTaxable = "";
	private String totalTax = "";
	private String totalTaxCalculated = "";
	private String taxDate = "";
	private String resultCode = "";
//
	private GmGetTaxAddressVo[] taxAddresses;
	private GmGetTaxLinesVo[] taxLines;
	private GmMessagesVO[] messages;
  /**
   * @return the docCode
   */
  public String getDocCode() {
    return docCode;
  }
  /**
   * @param docCode the docCode to set
   */
  public void setDocCode(String docCode) {
    this.docCode = docCode;
  }
  /**
   * @return the docDate
   */
  public String getDocDate() {
    return docDate;
  }
  /**
   * @param docDate the docDate to set
   */
  public void setDocDate(String docDate) {
    this.docDate = docDate;
  }
  /**
   * @return the timestamp
   */
  public String getTimestamp() {
    return timestamp;
  }
  /**
   * @param timestamp the timestamp to set
   */
  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }
  /**
   * @return the totalAmount
   */
  public String getTotalAmount() {
    return totalAmount;
  }
  /**
   * @param totalAmount the totalAmount to set
   */
  public void setTotalAmount(String totalAmount) {
    this.totalAmount = totalAmount;
  }
  /**
   * @return the totalDiscount
   */
  public String getTotalDiscount() {
    return totalDiscount;
  }
  /**
   * @param totalDiscount the totalDiscount to set
   */
  public void setTotalDiscount(String totalDiscount) {
    this.totalDiscount = totalDiscount;
  }
  /**
   * @return the totalExemption
   */
  public String getTotalExemption() {
    return totalExemption;
  }
  /**
   * @param totalExemption the totalExemption to set
   */
  public void setTotalExemption(String totalExemption) {
    this.totalExemption = totalExemption;
  }
  /**
   * @return the totalTaxable
   */
  public String getTotalTaxable() {
    return totalTaxable;
  }
  /**
   * @param totalTaxable the totalTaxable to set
   */
  public void setTotalTaxable(String totalTaxable) {
    this.totalTaxable = totalTaxable;
  }
  /**
   * @return the totalTax
   */
  public String getTotalTax() {
    return totalTax;
  }
  /**
   * @param totalTax the totalTax to set
   */
  public void setTotalTax(String totalTax) {
    this.totalTax = totalTax;
  }
  /**
   * @return the totalTaxCalculated
   */
  public String getTotalTaxCalculated() {
    return totalTaxCalculated;
  }
  /**
   * @param totalTaxCalculated the totalTaxCalculated to set
   */
  public void setTotalTaxCalculated(String totalTaxCalculated) {
    this.totalTaxCalculated = totalTaxCalculated;
  }
  /**
   * @return the taxDate
   */
  public String getTaxDate() {
    return taxDate;
  }
  /**
   * @param taxDate the taxDate to set
   */
  public void setTaxDate(String taxDate) {
    this.taxDate = taxDate;
  }
  /**
   * @return the resultCode
   */
  public String getResultCode() {
    return resultCode;
  }
  /**
   * @param resultCode the resultCode to set
   */
  public void setResultCode(String resultCode) {
    this.resultCode = resultCode;
  }
  /**
   * @return the taxAddresses
   */
  public GmGetTaxAddressVo[] getTaxAddresses() {
    return taxAddresses;
  }
  /**
   * @param taxAddresses the taxAddresses to set
   */
  public void setTaxAddresses(GmGetTaxAddressVo[] taxAddresses) {
    this.taxAddresses = taxAddresses;
  }
  /**
   * @return the taxLines
   */
  public GmGetTaxLinesVo[] getTaxLines() {
    return taxLines;
  }
  /**
   * @param taxLines the taxLines to set
   */
  public void setTaxLines(GmGetTaxLinesVo[] taxLines) {
    this.taxLines = taxLines;
  }
  /**
   * @return the messages
   */
  public GmMessagesVO[] getMessages() {
    return messages;
  }
  /**
   * @param messages the messages to set
   */
  public void setMessages(GmMessagesVO[] messages) {
    this.messages = messages;
  }
	
	
}