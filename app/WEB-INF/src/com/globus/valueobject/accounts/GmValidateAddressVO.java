package com.globus.valueobject.accounts;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmValidateAddressVO{
  
  private String resultCode = "";
  private GmMessagesVO[] messages;
  private GmValidShipAddressVo address;
  /**
   * @return the resultCode
   */
  public String getResultCode() {
    return resultCode;
  }
  /**
   * @param resultCode the resultCode to set
   */
  public void setResultCode(String resultCode) {
    this.resultCode = resultCode;
  }
  /**
   * @return the messages
   */
  public GmMessagesVO[] getMessages() {
    return messages;
  }
  /**
   * @param messages the messages to set
   */
  public void setMessages(GmMessagesVO[] messages) {
    this.messages = messages;
  }
  /**
   * @return the address
   */
  public GmValidShipAddressVo getAddress() {
    return address;
  }
  /**
   * @param address the address to set
   */
  public void setAddress(GmValidShipAddressVo address) {
    this.address = address;
  }
  
  

	
	
}