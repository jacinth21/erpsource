package com.globus.valueobject.accounts;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.valueobject.prodmgmnt.GmSharedEmailVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCancelTaxResultVo {
	
	private String transactionId = "";
	private String resultCode = "";
	private String docId = "";
	private GmMessagesVO[] messages;
  /**
   * @return the transactionId
   */
  public String getTransactionId() {
    return transactionId;
  }
  /**
   * @param transactionId the transactionId to set
   */
  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }
  /**
   * @return the resultCode
   */
  public String getResultCode() {
    return resultCode;
  }
  /**
   * @param resultCode the resultCode to set
   */
  public void setResultCode(String resultCode) {
    this.resultCode = resultCode;
  }
  /**
   * @return the messages
   */
  public GmMessagesVO[] getMessages() {
    return messages;
  }
  /**
   * @param messages the messages to set
   */
  public void setMessages(GmMessagesVO[] messages) {
    this.messages = messages;
  }
  /**
   * @return the docId
   */
  public String getDocId() {
    return docId;
  }
  /**
   * @param docId the docId to set
   */
  public void setDocId(String docId) {
    this.docId = docId;
  }
  
	
}