package com.globus.valueobject.accounts;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.valueobject.prodmgmnt.GmSharedEmailVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmGetTaxLinesVo {
	
	private String lineNo = "";
	private String taxCode = "";
	private String taxability = "";
	private String boundaryLevel = "";
	private String taxable = "";
	private String rate = "";
	private String tax = "";
	private String discount = "";
	private String taxCalculated = "";
	private String exemption = "";
//
	private GmGetTaxDetailsVo[] taxDetails;
  /**
   * @return the lineNo
   */
  public String getLineNo() {
    return lineNo;
  }
  /**
   * @param lineNo the lineNo to set
   */
  public void setLineNo(String lineNo) {
    this.lineNo = lineNo;
  }
  /**
   * @return the taxCode
   */
  public String getTaxCode() {
    return taxCode;
  }
  /**
   * @param taxCode the taxCode to set
   */
  public void setTaxCode(String taxCode) {
    this.taxCode = taxCode;
  }
  /**
   * @return the taxability
   */
  public String getTaxability() {
    return taxability;
  }
  /**
   * @param taxability the taxability to set
   */
  public void setTaxability(String taxability) {
    this.taxability = taxability;
  }
  /**
   * @return the boundaryLevel
   */
  public String getBoundaryLevel() {
    return boundaryLevel;
  }
  /**
   * @param boundaryLevel the boundaryLevel to set
   */
  public void setBoundaryLevel(String boundaryLevel) {
    this.boundaryLevel = boundaryLevel;
  }
  /**
   * @return the taxable
   */
  public String getTaxable() {
    return taxable;
  }
  /**
   * @param taxable the taxable to set
   */
  public void setTaxable(String taxable) {
    this.taxable = taxable;
  }
  /**
   * @return the rate
   */
  public String getRate() {
    return rate;
  }
  /**
   * @param rate the rate to set
   */
  public void setRate(String rate) {
    this.rate = rate;
  }
  /**
   * @return the tax
   */
  public String getTax() {
    return tax;
  }
  /**
   * @param tax the tax to set
   */
  public void setTax(String tax) {
    this.tax = tax;
  }
  /**
   * @return the discount
   */
  public String getDiscount() {
    return discount;
  }
  /**
   * @param discount the discount to set
   */
  public void setDiscount(String discount) {
    this.discount = discount;
  }
  /**
   * @return the taxCalculated
   */
  public String getTaxCalculated() {
    return taxCalculated;
  }
  /**
   * @param taxCalculated the taxCalculated to set
   */
  public void setTaxCalculated(String taxCalculated) {
    this.taxCalculated = taxCalculated;
  }
  /**
   * @return the exemption
   */
  public String getExemption() {
    return exemption;
  }
  /**
   * @param exemption the exemption to set
   */
  public void setExemption(String exemption) {
    this.exemption = exemption;
  }
  /**
   * @return the taxDetails
   */
  public GmGetTaxDetailsVo[] getTaxDetails() {
    return taxDetails;
  }
  /**
   * @param taxDetails the taxDetails to set
   */
  public void setTaxDetails(GmGetTaxDetailsVo[] taxDetails) {
    this.taxDetails = taxDetails;
  }
	
}