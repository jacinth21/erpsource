package com.globus.valueobject.accounts;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.valueobject.prodmgmnt.GmSharedEmailVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmGetTaxDetailsVo {
	
	private String taxable = "";
	private String rate = "";
	private String tax = "";
	private String region = "";
	private String country = "";
	private String jurisType = "";
	private String jurisName = "";
	private String jurisCode = "";
	private String taxName = "";
  /**
   * @return the taxable
   */
  public String getTaxable() {
    return taxable;
  }
  /**
   * @param taxable the taxable to set
   */
  public void setTaxable(String taxable) {
    this.taxable = taxable;
  }
  /**
   * @return the rate
   */
  public String getRate() {
    return rate;
  }
  /**
   * @param rate the rate to set
   */
  public void setRate(String rate) {
    this.rate = rate;
  }
  /**
   * @return the tax
   */
  public String getTax() {
    return tax;
  }
  /**
   * @param tax the tax to set
   */
  public void setTax(String tax) {
    this.tax = tax;
  }
  /**
   * @return the region
   */
  public String getRegion() {
    return region;
  }
  /**
   * @param region the region to set
   */
  public void setRegion(String region) {
    this.region = region;
  }
  /**
   * @return the country
   */
  public String getCountry() {
    return country;
  }
  /**
   * @param country the country to set
   */
  public void setCountry(String country) {
    this.country = country;
  }
  /**
   * @return the jurisType
   */
  public String getJurisType() {
    return jurisType;
  }
  /**
   * @param jurisType the jurisType to set
   */
  public void setJurisType(String jurisType) {
    this.jurisType = jurisType;
  }
  /**
   * @return the jurisName
   */
  public String getJurisName() {
    return jurisName;
  }
  /**
   * @param jurisName the jurisName to set
   */
  public void setJurisName(String jurisName) {
    this.jurisName = jurisName;
  }
  /**
   * @return the jurisCode
   */
  public String getJurisCode() {
    return jurisCode;
  }
  /**
   * @param jurisCode the jurisCode to set
   */
  public void setJurisCode(String jurisCode) {
    this.jurisCode = jurisCode;
  }
  /**
   * @return the taxName
   */
  public String getTaxName() {
    return taxName;
  }
  /**
   * @param taxName the taxName to set
   */
  public void setTaxName(String taxName) {
    this.taxName = taxName;
  }
		
 
	
	
}