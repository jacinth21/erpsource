package com.globus.valueobject.accounts;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.valueobject.prodmgmnt.GmSharedEmailVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmGetTaxAddressVo {
	
	private String address = "";
	private String addressCode = "";
	private String country = "";
	private String postalCode = "";
	private String region = "";
	private String taxRegionId = "";
	private String jurisCode = "";
	private String city = "";
  /**
   * @return the address
   */
  public String getAddress() {
    return address;
  }
  /**
   * @param address the address to set
   */
  public void setAddress(String address) {
    this.address = address;
  }
  /**
   * @return the addressCode
   */
  public String getAddressCode() {
    return addressCode;
  }
  /**
   * @param addressCode the addressCode to set
   */
  public void setAddressCode(String addressCode) {
    this.addressCode = addressCode;
  }
  /**
   * @return the country
   */
  public String getCountry() {
    return country;
  }
  /**
   * @param country the country to set
   */
  public void setCountry(String country) {
    this.country = country;
  }
  /**
   * @return the postalCode
   */
  public String getPostalCode() {
    return postalCode;
  }
  /**
   * @param postalCode the postalCode to set
   */
  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }
  /**
   * @return the region
   */
  public String getRegion() {
    return region;
  }
  /**
   * @param region the region to set
   */
  public void setRegion(String region) {
    this.region = region;
  }
  /**
   * @return the taxRegionId
   */
  public String getTaxRegionId() {
    return taxRegionId;
  }
  /**
   * @param taxRegionId the taxRegionId to set
   */
  public void setTaxRegionId(String taxRegionId) {
    this.taxRegionId = taxRegionId;
  }
  /**
   * @return the jurisCode
   */
  public String getJurisCode() {
    return jurisCode;
  }
  /**
   * @param jurisCode the jurisCode to set
   */
  public void setJurisCode(String jurisCode) {
    this.jurisCode = jurisCode;
  }
  /**
   * @return the city
   */
  public String getCity() {
    return city;
  }
  /**
   * @param city the city to set
   */
  public void setCity(String city) {
    this.city = city;
  }
	
}