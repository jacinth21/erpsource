package com.globus.valueobject.accounts;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;
import com.globus.valueobject.prodmgmnt.GmSharedEmailVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmMessagesVO {
	
	private String summary = "";
	private String details = "";
	private String refersTo = "";
	private String severity = "";
	private String source = "";
  /**
   * @return the summary
   */
  public String getSummary() {
    return summary;
  }
  /**
   * @param summary the summary to set
   */
  public void setSummary(String summary) {
    this.summary = summary;
  }
  /**
   * @return the details
   */
  public String getDetails() {
    return details;
  }
  /**
   * @param details the details to set
   */
  public void setDetails(String details) {
    this.details = details;
  }
  /**
   * @return the refersTo
   */
  public String getRefersTo() {
    return refersTo;
  }
  /**
   * @param refersTo the refersTo to set
   */
  public void setRefersTo(String refersTo) {
    this.refersTo = refersTo;
  }
  /**
   * @return the severity
   */
  public String getSeverity() {
    return severity;
  }
  /**
   * @param severity the severity to set
   */
  public void setSeverity(String severity) {
    this.severity = severity;
  }
  /**
   * @return the source
   */
  public String getSource() {
    return source;
  }
  /**
   * @param source the source to set
   */
  public void setSource(String source) {
    this.source = source;
  }
	
	
	
	
}