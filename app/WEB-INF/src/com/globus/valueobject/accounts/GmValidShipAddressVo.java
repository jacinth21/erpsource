package com.globus.valueobject.accounts;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmValidShipAddressVo {
  private String county = "";
  private String fipsCode = "";
  private String carrierRoute = "";
  private String postNet = "";
  private String addressType = "";
  private String line1 = "";
  private String line2 = "";
  private String city = "";
  private String region = "";
  private String postalCode = "";
  private String country = "";

  /**
   * @return the county
   */
  public String getCounty() {
    return county;
  }

  /**
   * @param county the county to set
   */
  public void setCounty(String county) {
    this.county = county;
  }

  /**
   * @return the fipsCode
   */
  public String getFipsCode() {
    return fipsCode;
  }

  /**
   * @param fipsCode the fipsCode to set
   */
  public void setFipsCode(String fipsCode) {
    this.fipsCode = fipsCode;
  }

  /**
   * @return the carrierRoute
   */
  public String getCarrierRoute() {
    return carrierRoute;
  }

  /**
   * @param carrierRoute the carrierRoute to set
   */
  public void setCarrierRoute(String carrierRoute) {
    this.carrierRoute = carrierRoute;
  }

  /**
   * @return the postNet
   */
  public String getPostNet() {
    return postNet;
  }

  /**
   * @param postNet the postNet to set
   */
  public void setPostNet(String postNet) {
    this.postNet = postNet;
  }

  /**
   * @return the addressType
   */
  public String getAddressType() {
    return addressType;
  }

  /**
   * @param addressType the addressType to set
   */
  public void setAddressType(String addressType) {
    this.addressType = addressType;
  }

  /**
   * @return the line1
   */
  public String getLine1() {
    return line1;
  }

  /**
   * @param line1 the line1 to set
   */
  public void setLine1(String line1) {
    this.line1 = line1;
  }

  /**
   * @return the line2
   */
  public String getLine2() {
    return line2;
  }

  /**
   * @param line2 the line2 to set
   */
  public void setLine2(String line2) {
    this.line2 = line2;
  }

  /**
   * @return the city
   */
  public String getCity() {
    return city;
  }

  /**
   * @param city the city to set
   */
  public void setCity(String city) {
    this.city = city;
  }

  /**
   * @return the region
   */
  public String getRegion() {
    return region;
  }

  /**
   * @param region the region to set
   */
  public void setRegion(String region) {
    this.region = region;
  }

  /**
   * @return the postalCode
   */
  public String getPostalCode() {
    return postalCode;
  }

  /**
   * @param postalCode the postalCode to set
   */
  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  /**
   * @return the country
   */
  public String getCountry() {
    return country;
  }

  /**
   * @param country the country to set
   */
  public void setCountry(String country) {
    this.country = country;
  }



}
