package com.globus.valueobject.sales;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDeviceInfoVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesDashReqVO extends GmDeviceInfoVO{
 
	private String companyid = "";
	private String zoneids = "";
	private String regionids = "";
	private String fsids = "";
	private String view = "";
	private String filterby = "";
	private String passname = "";
	private String divids = "";
	private String currency = "";
	private String accountid= "";
	private String fromdate = "";
	private String todate = "";
	private String doid = "";
	private String reqdtlids = "";
    private String lotnumber = "";
    private String surgeonname = "";
	
	
	
	public String getReqdtlids() {
		return reqdtlids;
	}
	public void setReqdtlids(String reqdtlids) {
		this.reqdtlids = reqdtlids;
	}
	/**
	 * @return the companyid
	 */
	public String getCompanyid() {
		return companyid;
	}
	/**
	 * @param companyid the companyid to set
	 */
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	/**
	 * @return the zoneids
	 */
	public String getZoneids() {
		return zoneids;
	}
	/**
	 * @param zoneids the zoneids to set
	 */
	public void setZoneids(String zoneids) {
		this.zoneids = zoneids;
	}
	/**
	 * @return the regionids
	 */
	public String getRegionids() {
		return regionids;
	}
	/**
	 * @param regionids the regionids to set
	 */
	public void setRegionids(String regionids) {
		this.regionids = regionids;
	}
	/**
	 * @return the fsids
	 */
	public String getFsids() {
		return fsids;
	}
	/**
	 * @param fsids the fsids to set
	 */
	public void setFsids(String fsids) {
		this.fsids = fsids;
	}
	/**
	 * @return the view
	 */
	public String getView() {
		return view;
	}
	/**
	 * @param view the view to set
	 */
	public void setView(String view) {
		this.view = view;
	}
	/**
	 * @return the filterby
	 */
	public String getFilterby() {
		return filterby;
	}
	/**
	 * @param filterby the filterby to set
	 */
	public void setFilterby(String filterby) {
		this.filterby = filterby;
	}
	/**
	 * @return the passname
	 */
	public String getPassname() {
		return passname;
	}
	/**
	 * @param passname the passname to set
	 */
	public void setPassname(String passname) {
		this.passname = passname;
	}
	/**
	 * @return the divids
	 */
	public String getDivids() {
		return divids;
	}
	/**
	 * @param divids the divids to set
	 */
	public void setDivids(String divids) {
		this.divids = divids;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getAccountid() {
		return accountid;
	}
	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}
	public String getFromdate() {
		return fromdate;
	}
	public void setFromdate(String fromdate) {
		this.fromdate = fromdate;
	}
	public String getTodate() {
		return todate;
	}
	public void setTodate(String todate) {
		this.todate = todate;
	}
	public String getDoid() {
		return doid;
	}
	public void setDoid(String doid) {
		this.doid = doid;
	}
	/**
	 * @return the lotnumber
	 */
	public String getLotnumber() {
		return lotnumber;
	}
	/**
	 * @param lotnumber the lotnumber to set
	 */
	public void setLotnumber(String lotnumber) {
		this.lotnumber = lotnumber;
	}
	/**
	 * @return the surgeonname
	 */
	public String getSurgeonname() {
		return surgeonname;
	}
	/**
	 * @param surgeonname the surgeonname to set
	 */
	public void setSurgeonname(String surgeonname) {
		this.surgeonname = surgeonname;
	}
	
	
	
}
