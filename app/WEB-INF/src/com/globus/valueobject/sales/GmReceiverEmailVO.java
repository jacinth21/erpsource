package com.globus.valueobject.sales;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.sales.GmSetDetlVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmReceiverEmailVO extends GmDataStoreVO implements Serializable{
	
	private String receiveremail = "";
	private String emailbody = "";
	private String requestorrepdefltaddress = "";
	private String distemailid = "";
	
	private GmSetDetlVO[] arrSetDtl;
	
	/**
	 * @return the receiveremail
	 */
	public String getReceiveremail() {
		return receiveremail;
	}


	/**
	 * @param receiveremail the receiveremail to set
	 */
	public void setReceiveremail(String receiveremail) {
		this.receiveremail = receiveremail;
	}


	/**
	 * @return the emailbody
	 */
	public String getEmailbody() {
		return emailbody;
	}


	/**
	 * @param emailbody the emailbody to set
	 */
	public void setEmailbody(String emailbody) {
		this.emailbody = emailbody;
	}


	/**
	 * @return the requestorrepdefltaddress
	 */
	public String getRequestorrepdefltaddress() {
		return requestorrepdefltaddress;
	}


	/**
	 * @param requestorrepdefltaddress the requestorrepdefltaddress to set
	 */
	public void setRequestorrepdefltaddress(String requestorrepdefltaddress) {
		this.requestorrepdefltaddress = requestorrepdefltaddress;
	}


	/**
	 * @return the arrSetDtl
	 */
	public GmSetDetlVO[] getArrSetDtl() {
		return arrSetDtl;
	}


	/**
	 * @param arrSetDtl the arrSetDtl to set
	 */
	public void setArrSetDtl(GmSetDetlVO[] arrSetDtl) {
		this.arrSetDtl = arrSetDtl;
	}


	/**
	 * @return the distemailid
	 */
	public String getDistemailid() {
		return distemailid;
	}


	/**
	 * @param distemailid the distemailid to set
	 */
	public void setDistemailid(String distemailid) {
		this.distemailid = distemailid;
	}


	public static Map getSetReceiverMailProperties(){
		Map hmLinkedDbInputStr = new LinkedHashMap();  
		
		hmLinkedDbInputStr.put("ORDERID", "getReceiveremail");
		hmLinkedDbInputStr.put("EMAILBODY", "getEmailbody");
		hmLinkedDbInputStr.put("DISTEMAILID", "getDistemailid");
		hmLinkedDbInputStr.put("REQUESTORREPDEFLTADDRESS", "getRequestorrepdefltaddress");
		return hmLinkedDbInputStr;
	}
}