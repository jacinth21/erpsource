package com.globus.valueobject.sales;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmOrderVO extends GmDataStoreVO {

  private String orderid = "";
  private String orderdate = "";
  private String ordertype = "";
  private String repid = "";
  private String acctid = "";
  private String custpo = "";
  private String custpodate = "";
  private String shipdate = "";
  private String total = "";
  private String statusfl = "";
  private String shipcost = "";
  private String rmaid = "";
  private String invoiceid = "";
  private String caseinfoid = "";
  private String invupdfl = "";
  private String holdfl = "";
  private String voidfl = "";
  private String skipvalidatefl = "";
  private String parentorderid = "";
  private String ordercomments = "";
  private String shipto = "";
  private String shipmode = "";
  private String shipcarrier = "";
  private String prefixordid = "";
  private String dealid = "";
  private String accDivCompanyFl = "";
  private String accEPGSFl = "";
  private String divcompanyid = "";



  private GmOrderAttrVO[] arrGmOrderAttrVO;
  private GmOrderItemVO[] arrGmOrderItemVO;
  private GmItemOrderAttrVO[] arrGmOrderItemAttrVO;
  private GmTagUsageVO[] arrGmTagUsageVO;
  private GmTxnShipInfoVO gmTxnShipInfoVO;
  private GmDOSurgeryDetailVO[] arrDoSurgeryDetailVO;
  private GmDONPIDetailVO[] arrDoNPIDetailVO;
  private GmDORecordTagIDVO[] arrDORecordTagIDVO;

  /**
   * @return the orderid
   */
  public String getOrderid() {
    return orderid;
  }

  /**
   * @param orderid the orderid to set
   */
  public void setOrderid(String orderid) {
    this.orderid = orderid;
  }

  /**
   * @return the orderdate
   */
  public String getOrderdate() {
    return orderdate;
  }

  /**
   * @param orderdate the orderdate to set
   */
  public void setOrderdate(String orderdate) {
    this.orderdate = orderdate;
  }

  /**
   * @return the ordertype
   */
  public String getOrdertype() {
    return ordertype;
  }

  /**
   * @param ordertype the ordertype to set
   */
  public void setOrdertype(String ordertype) {
    this.ordertype = ordertype;
  }

  /**
   * @return the repid
   */
  public String getRepid() {
    return repid;
  }

  /**
   * @param repid the repid to set
   */
  public void setRepid(String repid) {
    this.repid = repid;
  }

  /**
   * @return the acctid
   */
  public String getAcctid() {
    return acctid;
  }

  /**
   * @param acctid the acctid to set
   */
  public void setAcctid(String acctid) {
    this.acctid = acctid;
  }

  /**
   * @return the custpodate
   */
  public String getCustpodate() {
    return custpodate;
  }

  /**
   * @param custpodate the custpodate to set
   */
  public void setCustpodate(String custpodate) {
    this.custpodate = custpodate;
  }

  /**
   * @return the shipdate
   */
  public String getShipdate() {
    return shipdate;
  }

  /**
   * @param shipdate the shipdate to set
   */
  public void setShipdate(String shipdate) {
    this.shipdate = shipdate;
  }

  /**
   * @return the total
   */
  public String getTotal() {
    return total;
  }

  /**
   * @param total the total to set
   */
  public void setTotal(String total) {
    this.total = total;
  }

  /**
   * @return the statusfl
   */
  public String getStatusfl() {
    return statusfl;
  }

  /**
   * @param statusfl the statusfl to set
   */
  public void setStatusfl(String statusfl) {
    this.statusfl = statusfl;
  }

  /**
   * @return the shipcost
   */
  public String getShipcost() {
    return shipcost;
  }

  /**
   * @param shipcost the shipcost to set
   */
  public void setShipcost(String shipcost) {
    this.shipcost = shipcost;
  }

  /**
   * @return the rmaid
   */
  public String getRmaid() {
    return rmaid;
  }

  /**
   * @param rmaid the rmaid to set
   */
  public void setRmaid(String rmaid) {
    this.rmaid = rmaid;
  }

  /**
   * @return the invoiceid
   */
  public String getInvoiceid() {
    return invoiceid;
  }

  /**
   * @param invoiceid the invoiceid to set
   */
  public void setInvoiceid(String invoiceid) {
    this.invoiceid = invoiceid;
  }

  /**
   * @return the caseinfoid
   */
  public String getCaseinfoid() {
    return caseinfoid;
  }

  /**
   * @param caseinfoid the caseinfoid to set
   */
  public void setCaseinfoid(String caseinfoid) {
    this.caseinfoid = caseinfoid;
  }

  /**
   * @return the invupdfl
   */
  public String getInvupdfl() {
    return invupdfl;
  }

  /**
   * @param invupdfl the invupdfl to set
   */
  public void setInvupdfl(String invupdfl) {
    this.invupdfl = invupdfl;
  }

  /**
   * @return the holdfl
   */
  public String getHoldfl() {
    return holdfl;
  }

  /**
   * @param holdfl the holdfl to set
   */
  public void setHoldfl(String holdfl) {
    this.holdfl = holdfl;
  }

  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }

  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  /**
   * @return the skipvalidatefl
   */
  public String getSkipvalidatefl() {
    return skipvalidatefl;
  }

  /**
   * @param skipvalidatefl the skipvalidatefl to set
   */
  public void setSkipvalidatefl(String skipvalidatefl) {
    this.skipvalidatefl = skipvalidatefl;
  }

  /**
   * @return the arrGmOrderAttrVO
   */
  public GmOrderAttrVO[] getArrGmOrderAttrVO() {
    return arrGmOrderAttrVO;
  }

  /**
   * @param arrGmOrderAttrVO the arrGmOrderAttrVO to set
   */
  public void setArrGmOrderAttrVO(GmOrderAttrVO[] arrGmOrderAttrVO) {
    this.arrGmOrderAttrVO = arrGmOrderAttrVO;
  }

  /**
   * @return the arrGmOrderItemVO
   */
  public GmOrderItemVO[] getArrGmOrderItemVO() {
    return arrGmOrderItemVO;
  }

  /**
   * @param arrGmOrderItemVO the arrGmOrderItemVO to set
   */
  public void setArrGmOrderItemVO(GmOrderItemVO[] arrGmOrderItemVO) {
    this.arrGmOrderItemVO = arrGmOrderItemVO;
  }

  /**
   * @return the arrGmOrderItemAttrVO
   */
  public GmItemOrderAttrVO[] getArrGmOrderItemAttrVO() {
    return arrGmOrderItemAttrVO;
  }

  /**
   * @param arrGmOrderItemAttrVO the arrGmOrderItemAttrVO to set
   */
  public void setArrGmOrderItemAttrVO(GmItemOrderAttrVO[] arrGmOrderItemAttrVO) {
    this.arrGmOrderItemAttrVO = arrGmOrderItemAttrVO;
  }

  /**
   * @return the arrGmTagUsageVO
   */
  public GmTagUsageVO[] getArrGmTagUsageVO() {
    return arrGmTagUsageVO;
  }

  /**
   * @param arrGmTagUsageVO the arrGmTagUsageVO to set
   */
  public void setArrGmTagUsageVO(GmTagUsageVO[] arrGmTagUsageVO) {
    this.arrGmTagUsageVO = arrGmTagUsageVO;
  }



  /**
   * @return the parentorderid
   */
  public String getParentorderid() {
    return parentorderid;
  }

  /**
   * @param parentorderid the parentorderid to set
   */
  public void setParentorderid(String parentorderid) {
    this.parentorderid = parentorderid;
  }

  /**
   * @return the custpo
   */
  public String getCustpo() {
    return custpo;
  }

  /**
   * @param custpo the custpo to set
   */
  public void setCustpo(String custpo) {
    this.custpo = custpo;
  }

  /**
   * @return the ordercomments
   */
  public String getOrdercomments() {
    return ordercomments;
  }

  /**
   * @param ordercomments the ordercomments to set
   */
  public void setOrdercomments(String ordercomments) {
    this.ordercomments = ordercomments;
  }

  /**
   * @return the shipto
   */
  public String getShipto() {
    return shipto;
  }

  /**
   * @param shipto the shipto to set
   */
  public void setShipto(String shipto) {
    this.shipto = shipto;
  }

  /**
   * @return the shipmode
   */
  public String getShipmode() {
    return shipmode;
  }

  /**
   * @param shipmode the shipmode to set
   */
  public void setShipmode(String shipmode) {
    this.shipmode = shipmode;
  }

  /**
   * @return the shipcarrier
   */
  public String getShipcarrier() {
    return shipcarrier;
  }

  /**
   * @param shipcarrier the shipcarrier to set
   */
  public void setShipcarrier(String shipcarrier) {
    this.shipcarrier = shipcarrier;
  }

  /**
   * @return the gmTxnShipInfoVO
   */
  public GmTxnShipInfoVO getGmTxnShipInfoVO() {
    return gmTxnShipInfoVO;
  }

  /**
   * @param gmTxnShipInfoVO the gmTxnShipInfoVO to set
   */
  public void setGmTxnShipInfoVO(GmTxnShipInfoVO gmTxnShipInfoVO) {
    this.gmTxnShipInfoVO = gmTxnShipInfoVO;
  }

  public String getPrefixordid() {
    return prefixordid;
  }

  public void setPrefixordid(String prefixordid) {
    this.prefixordid = prefixordid;
  }

  /**
   * @return the arrDoSurgeryDetailVO
   */
  public GmDOSurgeryDetailVO[] getArrDoSurgeryDetailVO() {
    return arrDoSurgeryDetailVO;
  }

  /**
   * @param arrDoSurgeryDetailVO the arrDoSurgeryDetailVO to set
   */
  public void setArrDoSurgeryDetailVO(GmDOSurgeryDetailVO[] arrDoSurgeryDetailVO) {
    this.arrDoSurgeryDetailVO = arrDoSurgeryDetailVO;
  }

  /**
   * @return the arrDoNPIDetailVO
   */
  public GmDONPIDetailVO[] getArrDoNPIDetailVO() {
    return arrDoNPIDetailVO;
  }

  /**
   * @param arrDoNPIDetailVO the arrDoNPIDetailVO to set
   */
  public void setArrDoNPIDetailVO(GmDONPIDetailVO[] arrDoNPIDetailVO) {
    this.arrDoNPIDetailVO = arrDoNPIDetailVO;
  }

  public String getDealid() {
    return dealid;
  }

  public void setDealid(String dealid) {
    this.dealid = dealid;
  }

/**
 * @return the arrDORecordTagIDVO
 */
public GmDORecordTagIDVO[] getArrDORecordTagIDVO() {
	return arrDORecordTagIDVO;
}

/**
 * @param arrDORecordTagIDVO the arrDORecordTagIDVO to set
 */
public void setArrDORecordTagIDVO(GmDORecordTagIDVO[] arrDORecordTagIDVO) {
	this.arrDORecordTagIDVO = arrDORecordTagIDVO;
}

public String getAccDivCompanyFl() {
	return accDivCompanyFl;
}

public void setAccDivCompanyFl(String accDivCompanyFl) {
	this.accDivCompanyFl = accDivCompanyFl;
}

public String getAccEPGSFl() {
	return accEPGSFl;
}

public void setAccEPGSFl(String accEPGSFl) {
	this.accEPGSFl = accEPGSFl;
}

/**
* @return the divcompanyid
*/
public String getDivcompanyid() {
	return divcompanyid;
}

/**
* @param divcompanyid the divcompanyid to set
*/
public void setDivcompanyid(String divcompanyid) {
	this.divcompanyid = divcompanyid;
}

  
}
