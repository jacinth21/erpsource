package com.globus.valueobject.sales;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmLoanerRptVO extends GmSalesDashReqVO {

  private String loanfromdt = "";
  private String loantodt = "";
  private String setid = "";
  private String setnm = "";
  private String distid = "";



  /**
   * @return the distid
   */
  public String getDistid() {
    return distid;
  }

  /**
   * @param distid the distid to set
   */
  public void setDistid(String distid) {
    this.distid = distid;
  }

  /**
   * @return the loanfromdt
   */
  public String getLoanfromdt() {
    return loanfromdt;
  }

  /**
   * @param loanfromdt the loanfromdt to set
   */
  public void setLoanfromdt(String loanfromdt) {
    this.loanfromdt = loanfromdt;
  }

  /**
   * @return the loantodt
   */
  public String getLoantodt() {
    return loantodt;
  }

  /**
   * @param loantodt the loantodt to set
   */
  public void setLoantodt(String loantodt) {
    this.loantodt = loantodt;
  }

  /**
   * @return the setid
   */
  public String getSetid() {
    return setid;
  }

  /**
   * @param setid the setid to set
   */
  public void setSetid(String setid) {
    this.setid = setid;
  }

  /**
   * @return the setnm
   */
  public String getSetnm() {
    return setnm;
  }

  /**
   * @param setnm the setnm to set
   */
  public void setSetnm(String setnm) {
    this.setnm = setnm;
  }



}
