package com.globus.valueobject.sales;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDeviceInfoVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesReportVO extends GmSalesReportFilterVO{
	
	private List<GmConsignDetailVO> listGmConsignDetailVO    = null;
	private List<GmConsignDetailByPartVO> listGmConsignDetailByPartVO    = null;
	
	public List<GmConsignDetailVO> getListGmConsignDetailVO() {
		return listGmConsignDetailVO;
	}
	public void setListGmConsignDetailVO(
			List<GmConsignDetailVO> listGmConsignDetailVO) {
		this.listGmConsignDetailVO = listGmConsignDetailVO;
	}
	public List<GmConsignDetailByPartVO> getListGmConsignDetailByPartVO() {
		return listGmConsignDetailByPartVO;
	}
	public void setListGmConsignDetailByPartVO(
			List<GmConsignDetailByPartVO> listGmConsignDetailByPartVO) {
		this.listGmConsignDetailByPartVO = listGmConsignDetailByPartVO;
	}
	
}
