package com.globus.valueobject.sales;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDeviceInfoVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmLoanerExtVO extends GmDeviceInfoVO{
   
	private String txnid = "";
	private String reqdtlid = "";
	private String newextndate = "";	
	private String consignid = "";
	private String extwholereq = "";
	private String txt_logreason = "";
	private String surgdt = "";
	 	
	
	/**
	 * getSaveLoanerExtProperties is used put method name in a order required by 
	 * database while loop variable order. this method should be written when you have to
	 * save multiple VO in database using inputstring of specified order in this linked hashmap.
	 * @return
	 */
	public static Map getSaveLoanerExtProperties(){
		Map hmLinkedDbInputStr = new LinkedHashMap();  
		 
		hmLinkedDbInputStr.put("TXNID", "getTxnid");
		hmLinkedDbInputStr.put("SURGDT", "getSurgdt");
		hmLinkedDbInputStr.put("REQDTLID", "getReqdtlid");
		hmLinkedDbInputStr.put("CONSIGNID", "getConsignid");
		hmLinkedDbInputStr.put("NEWEXTNDATE", "getNewextndate");
		hmLinkedDbInputStr.put("EXTWHOLEREQ", "getExtwholereq"); 
		return hmLinkedDbInputStr;
	}
	 

	public String getSurgdt() {
		return surgdt;
	}
	public void setSurgdt(String surgdt) {
		this.surgdt = surgdt;
	}

	/**
	 * @return the txnid
	 */
	public String getTxnid() {
		return txnid;
	} 
	/**
	 * @param txnid the txnid to set
	 */
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	} 

	/**
	 * @return the reqdtlid
	 */
	public String getReqdtlid() {
		return reqdtlid;
	}

	/**
	 * @param reqdtlid the reqdtlid to set
	 */
	public void setReqdtlid(String reqdtlid) {
		this.reqdtlid = reqdtlid;
	}
	/**
	 * @return the newextndate
	 */
	public String getNewextndate() {
		return newextndate;
	}

	/**
	 * @param newextndate the newextndate to set
	 */
	public void setNewextndate(String newextndate) {
		this.newextndate = newextndate;
	}


	/**
	 * @return the consignid
	 */
	public String getConsignid() {
		return consignid;
	}

	/**
	 * @param consignid the consignid to set
	 */
	public void setConsignid(String consignid) {
		this.consignid = consignid;
	}

	/**
	 * @return the extwholereq
	 */
	public String getExtwholereq() {
		return extwholereq;
	}

	/**
	 * @param extwholereq the extwholereq to set
	 */
	public void setExtwholereq(String extwholereq) {
		this.extwholereq = extwholereq;
	}
	
	/**
	 * @return the txt_logreason
	 */
	public String getTxt_logreason() {
		return txt_logreason;
	}

	/**
	 * @param txt_logreason the txt_logreason to set
	 */
	public void setTxt_logreason(String txt_logreason) {
		this.txt_logreason = txt_logreason;
	}
	
	 
	public String toString()
	{
		return txnid+"^"+reqdtlid+"^"+consignid+"^"+newextndate+"^"+extwholereq+"^"+surgdt+"|";
	}
	
	
}
