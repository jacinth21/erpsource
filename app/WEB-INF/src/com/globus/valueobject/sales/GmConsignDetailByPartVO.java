package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmConsignDetailByPartVO extends GmSalesReportVO{
	
	private String setid ="";
	private String pid="";
	private String pdesc="";
	private String sqty="";
	private String prodfamily="";
	private String crtfl="";
	private String setusage="";
	private String did="";
	private String pline ="";
	private String listvalue ="";
	
	
	/**
	 * @return the setid
	 */
	public String getSetid() {
		return setid;
	}
	/**
	 * @param setid the setid to set
	 */
	public void setSetid(String setid) {
		this.setid = setid;
	}
	/**
	 * @return the pid
	 */
	public String getPid() {
		return pid;
	}
	/**
	 * @param pid the pid to set
	 */
	public void setPid(String pid) {
		this.pid = pid;
	}
	/**
	 * @return the pdesc
	 */
	public String getPdesc() {
		return pdesc;
	}
	/**
	 * @param pdesc the pdesc to set
	 */
	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}
	/**
	 * @return the sqty
	 */
	public String getSqty() {
		return sqty;
	}
	/**
	 * @param sqty the sqty to set
	 */
	public void setSqty(String sqty) {
		this.sqty = sqty;
	}
	/**
	 * @return the prodfamily
	 */
	public String getProdfamily() {
		return prodfamily;
	}
	/**
	 * @param prodfamily the prodfamily to set
	 */
	public void setProdfamily(String prodfamily) {
		this.prodfamily = prodfamily;
	}
	/**
	 * @return the crtfl
	 */
	public String getCrtfl() {
		return crtfl;
	}
	/**
	 * @param crtfl the crtfl to set
	 */
	public void setCrtfl(String crtfl) {
		this.crtfl = crtfl;
	}
	/**
	 * @return the setusage
	 */
	public String getSetusage() {
		return setusage;
	}
	/**
	 * @param setusage the setusage to set
	 */
	public void setSetusage(String setusage) {
		this.setusage = setusage;
	}
	/**
	 * @return the did
	 */
	public String getDid() {
		return did;
	}
	/**
	 * @param did the did to set
	 */
	public void setDid(String did) {
		this.did = did;
	}
	/**
	 * @return the pline
	 */
	public String getPline() {
		return pline;
	}
	/**
	 * @param pline the pline to set
	 */
	public void setPline(String pline) {
		this.pline = pline;
	}
	

	public String getListvalue() {
		return listvalue;
	}
	public void setListvalue(String listvalue) {
		this.listvalue = listvalue;
	}
	
	@Transient
	public Properties getConsignDetailsProperties(){
		Properties props = new Properties();
		props.put("C207_SET_ID","SETID");
		props.put("PID","PID");
		props.put("LISTVALUE","LISTVALUE");
		
		props.put("PDESC","PDESC");
		props.put("SQTY","SQTY");
		
		props.put("PRODFAMILY","PRODFAMILY");
		
		props.put("CRTFL","CRTFL");
		
		props.put("SETUSAGE","SETUSAGE");
		
		props.put("DID","DID");
		props.put("PLINE","PLINE");
		return props;
}
	
}