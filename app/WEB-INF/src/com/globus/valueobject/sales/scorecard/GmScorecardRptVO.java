package com.globus.valueobject.sales.scorecard;

import java.util.Map;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmScorecardRptVO extends GmDataStoreVO {

	private String systemid = "";
	private String distributorid = "";
	private String adid = "";
	//PC-3626: Scorecard mobile drill down changes
	private String distid = "";
	private String displayname = "";

	// Added allowed tag field according to PC-5101: Scorecard DO classification changes
	private String allowedtag = "";

	/**
	 * @return the systemid
	 */
	public String getSystemid() {
		return systemid;
	}

	// Added allowed tag field according to PC-5101: Scorecard DO classification changes

	
	/**
	 * @param systemid the systemid to set
	 */
	public void setSystemid(String systemid) {
		this.systemid = systemid;
	}
	public String getAllowedtag() {
		return allowedtag;
	}

	public void setAllowedtag(String allowedtag) {
		this.allowedtag = allowedtag;
	}

	/**
	 * @return the distributorid
	 */
	public String getDistributorid() {
		return distributorid;
	}
	/**
	 * @param distributorid the distributorid to set
	 */
	public void setDistributorid(String distributorid) {
		this.distributorid = distributorid;
	}
	/**
	 * @return the adid
	 */
	public String getAdid() {
		return adid;
	}
	/**
	 * @param adid the adid to set
	 */
	public void setAdid(String adid) {
		this.adid = adid;
	}
	/**
	 * @return the distid
	 */
	public String getDistid() {
		return distid;
	}
	/**
	 * @param distid the distid to set
	 */
	public void setDistid(String distid) {
		this.distid = distid;
	}
	/**
	 * @return the displayname
	 */
	public String getDisplayname() {
		return displayname;
	}
	/**
	 * @param displayname the displayname to set
	 */
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}




}
