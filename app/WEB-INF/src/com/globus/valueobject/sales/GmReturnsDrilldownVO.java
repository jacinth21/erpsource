package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmReturnsDrilldownVO extends GmSalesDashDrilldownVO{
	
	private String type = "" ;
	private String parentra = "" ;
	private String initdt = "" ;
	private String closedt = "" ;
	private String fieldsalesnm = "" ;
	
	public String getFieldsalesnm() {
		return fieldsalesnm;
	}

	public void setFieldsalesnm(String fieldsalesnm) {
		this.fieldsalesnm = fieldsalesnm;
	}

	@Transient
	public Properties getReturnDrilldownProperties(){
		Properties props = new Properties();
		props = getSalesDashDrilldownProperties();
		props.put("RAID","TXNID");
		props.put("TYPE","TYPE");
		props.put("PARENTRMAID","PARENTRA");
		props.put("CDATE","INITDT");
		props.put("CRDATE","CLOSEDT");
		props.put("RNAME","REPNM");
		props.put("STATUS","STATUS");
		props.put("DNAME","FIELDSALESNM");
		
		return props;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the parentra
	 */
	public String getParentra() {
		return parentra;
	}

	/**
	 * @param parentra the parentra to set
	 */
	public void setParentra(String parentra) {
		this.parentra = parentra;
	}

	/**
	 * @return the initdt
	 */
	public String getInitdt() {
		return initdt;
	}

	/**
	 * @param initdt the initdt to set
	 */
	public void setInitdt(String initdt) {
		this.initdt = initdt;
	}

	/**
	 * @return the closedt
	 */
	public String getClosedt() {
		return closedt;
	}

	/**
	 * @param closedt the closedt to set
	 */
	public void setClosedt(String closedt) {
		this.closedt = closedt;
	}
	
}
