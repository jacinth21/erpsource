package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmInvTagShareOptDetailVO extends GmSalesDashReqVO {
  
	private String setid = "";
	private String system = "";
	private String tagid = "";
	private String tagshareoptionid = "";
	private String message = "";
	
   /**
	 * @return the setid
	 */
	public String getSetid() {
		return setid;
	}

	/**
	 * @param setid the setid to set
	 */
	public void setSetid(String setid) {
		this.setid = setid;
	}

	/**
	 * @return the system
	 */
	public String getSystem() {
		return system;
	}

	/**
	 * @param system the system to set
	 */
	public void setSystem(String system) {
		this.system = system;
	}

	/**
	 * @return the tagid
	 */
	public String getTagid() {
		return tagid;
	}

	/**
	 * @param tagid the tagid to set
	 */
	public void setTagid(String tagid) {
		this.tagid = tagid;
	}

	/**
	 * @return the tagshareoptionid
	 */
	public String getTagshareoptionid() {
		return tagshareoptionid;
	}

	/**
	 * @param tagshareoptionid the tagshareoptionid to set
	 */
	public void setTagshareoptionid(String tagshareoptionid) {
		this.tagshareoptionid = tagshareoptionid;
	}
	

  /**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

@Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
	    properties.put("SETID", "SETID");
	    properties.put("SYSTEM", "SYSTEM");
	    properties.put("TAGID", "TAGID");
	    properties.put("TAGSHAREOPTIONID", "TAGSHAREOPTIONID");
	    properties.put("MESSAGE", "MESSAGE");
	return properties;  
	}

}

