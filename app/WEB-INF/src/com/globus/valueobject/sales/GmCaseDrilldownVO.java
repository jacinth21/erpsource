package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCaseDrilldownVO extends GmSalesDashDrilldownVO{
	 
	private String surgerydt = "";
	private String notes = "" ;
	private String fieldsalesnm = "" ;
	private String caseinfoid = "" ;
	
	public String getCaseinfoid() {
		return caseinfoid;
	}

	public void setCaseinfoid(String caseinfoid) {
		this.caseinfoid = caseinfoid;
	}

	public String getFieldsalesnm() {
		return fieldsalesnm;
	}

	public void setFieldsalesnm(String fieldsalesnm) {
		this.fieldsalesnm = fieldsalesnm;
	}

	@Transient
	public Properties getCaseDrilldownProperties(){
		Properties props = new Properties();
		props.put("SURGERY_DATE","SURGERYDT");
		props.put("PERSONAL_NOTES","NOTES");
		props.put("REPNM","REPNM");
		props.put("ACCOUNTNM","ACCOUNTNM");
		props.put("STATUS","STATUS");
		props.put("DISTNM","FIELDSALESNM");
		props.put("CASEID","TXNID");
		props.put("INFOID","CASEINFOID");
		
		return props;
	}

	public String getSurgerydt() {
		return surgerydt;
	}

	public void setSurgerydt(String surgerydt) {
		this.surgerydt = surgerydt;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
