package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesDashDrilldownVO extends GmDataStoreVO{
	
	private String txnid = "" ;
	private String setid = "" ;
	private String setnm = "" ;
	private String repnm = "" ;
	private String repid = "" ;
	private String status = "";
	private String accountnm = "";
	private String accountid = "";
	
	@Transient
	public Properties getSalesDashDrilldownProperties(){
		Properties props = new Properties();
		props.put("TRANSID","TXNID");
		props.put("SID","SETID");
		props.put("SNAME","SETNM");
		props.put("REPNAME","REPNM");
		props.put("REPID","REPID");
		props.put("ACCTNAME","ACCOUNTNM");
		props.put("ACC_ID","ACCOUNTID");
		props.put("STSFL","STATUS");
		return props;
	}


	/**
	 * @return the txnid
	 */
	public String getTxnid() {
		return txnid;
	}


	/**
	 * @param txnid the txnid to set
	 */
	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}


	/**
	 * @return the setid
	 */
	public String getSetid() {
		return setid;
	}


	/**
	 * @param setid the setid to set
	 */
	public void setSetid(String setid) {
		this.setid = setid;
	}


	/**
	 * @return the setnm
	 */
	public String getSetnm() {
		return setnm;
	}


	/**
	 * @param setnm the setnm to set
	 */
	public void setSetnm(String setnm) {
		this.setnm = setnm;
	}


	/**
	 * @return the repnm
	 */
	public String getRepnm() {
		return repnm;
	}


	/**
	 * @param repnm the repnm to set
	 */
	public void setRepnm(String repnm) {
		this.repnm = repnm;
	}


	/**
	 * @return the repid
	 */
	public String getRepid() {
		return repid;
	}


	/**
	 * @param repid the repid to set
	 */
	public void setRepid(String repid) {
		this.repid = repid;
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}


	/**
	 * @return the accountnm
	 */
	public String getAccountnm() {
		return accountnm;
	}


	/**
	 * @param accountnm the accountnm to set
	 */
	public void setAccountnm(String accountnm) {
		this.accountnm = accountnm;
	}


	/**
	 * @return the accountid
	 */
	public String getAccountid() {
		return accountid;
	}

	/**
	 * @param accountid the accountid to set
	 */
	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}
	
}
