package com.globus.valueobject.sales;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


import com.globus.valueobject.common.GmDeviceInfoVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmBookDOVO extends GmDeviceInfoVO{
	
	private GmOrderVO[] arrGmOrderVO;

	/**
	 * @return the arrGmOrderVO
	 */
	public GmOrderVO[] getArrGmOrderVO() {
		return arrGmOrderVO;
	}

	/**
	 * @param arrGmOrderVO the arrGmOrderVO to set
	 */
	public void setArrGmOrderVO(GmOrderVO[] arrGmOrderVO) {
		this.arrGmOrderVO = arrGmOrderVO;
	}

	
	
	
	
}