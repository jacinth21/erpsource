package com.globus.valueobject.sales;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmTagUsageVO extends GmDataStoreVO {
	
	private String tagusageid = "";
	private String tagid = "";
	private String refid = "";
	private String reftype = "";
	private String pnum = "";
	private String qty = "";
	private String voidfl = "";
	/**
	 * @return the tagusageid
	 */
	public String getTagusageid() {
		return tagusageid;
	}
	/**
	 * @param tagusageid the tagusageid to set
	 */
	public void setTagusageid(String tagusageid) {
		this.tagusageid = tagusageid;
	}
	/**
	 * @return the tagid
	 */
	public String getTagid() {
		return tagid;
	}
	/**
	 * @param tagid the tagid to set
	 */
	public void setTagid(String tagid) {
		this.tagid = tagid;
	}
	/**
	 * @return the refid
	 */
	public String getRefid() {
		return refid;
	}
	/**
	 * @param refid the refid to set
	 */
	public void setRefid(String refid) {
		this.refid = refid;
	}
	/**
	 * @return the reftype
	 */
	public String getReftype() {
		return reftype;
	}
	/**
	 * @param reftype the reftype to set
	 */
	public void setReftype(String reftype) {
		this.reftype = reftype;
	}
	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}
	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	/**
	 * @return the qty
	 */
	public String getQty() {
		return qty;
	}
	/**
	 * @param qty the qty to set
	 */
	public void setQty(String qty) {
		this.qty = qty;
	}
	/**
	 * @return the voidfl
	 */
	public String getVoidfl() {
		return voidfl;
	}
	/**
	 * @param voidfl the voidfl to set
	 */
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	
	public static Map getTagUsageProperties(){
		Map hmLinkedDbInputStr = new LinkedHashMap();  
		hmLinkedDbInputStr.put("PNUM", "getPnum");
		hmLinkedDbInputStr.put("QTY", "getQty");
		hmLinkedDbInputStr.put("TAGID", "getTagid");
		return hmLinkedDbInputStr;
	}


}
