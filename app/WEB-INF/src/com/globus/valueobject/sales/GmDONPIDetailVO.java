package com.globus.valueobject.sales;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author karthiks
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmDONPIDetailVO extends GmDataStoreVO {

	  private String id = "";
	  private String name = "";
	  
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


public static Map getDONPIProperties() {
    Map hmLinkedDbInputStr = new LinkedHashMap();

    hmLinkedDbInputStr.put("ID", "getId");
    hmLinkedDbInputStr.put("NAME", "getName");
    return hmLinkedDbInputStr;
  }

}
