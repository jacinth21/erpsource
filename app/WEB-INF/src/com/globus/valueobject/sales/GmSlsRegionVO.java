package com.globus.valueobject.sales;
import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSlsRegionVO extends GmSlsZoneVO{
	private String regionid;
	private String regionnm = "";
	/**
	 * @return the regionid
	 */
	public String getRegionid() {
		return regionid;
	}
	/**
	 * @param regionid the regionid to set
	 */
	public void setRegionid(String regionid) {
		this.regionid = regionid;
	}
	/**
	 * @return the regionnm
	 */
	public String getRegionnm() {
		return regionnm;
	}
	/**
	 * @param regionnm the regionnm to set
	 */
	public void setRegionnm(String regionnm) {
		this.regionnm = regionnm;
	}
	
	@Transient
	public Properties getRegionProperties(){
		Properties props = new Properties();
		props.put("ID","REGIONID");
		props.put("NM","REGIONNM");
		props.put("COMPID","COMPANYID");
		props.put("ZONEID","ZONEID");
		
		return props;
	}
}
