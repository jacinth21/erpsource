package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSlsZoneVO extends GmSlsDivisionVO{
	private String zoneid;
	private String zonenm = "";
	/**
	 * @return the zoneid
	 */
	public String getZoneid() {
		return zoneid;
	}
	/**
	 * @param zoneid the zoneid to set
	 */
	public void setZoneid(String zoneid) {
		this.zoneid = zoneid;
	}
	/**
	 * @return the zonenm
	 */
	public String getZonenm() {
		return zonenm;
	}
	/**
	 * @param zonenm the zonenm to set
	 */
	public void setZonenm(String zonenm) {
		this.zonenm = zonenm;
	}

	@Transient
	public Properties getZoneProperties(){
		Properties props = new Properties();
		props.put("ID","ZONEID");
		props.put("NM","ZONENM");
		props.put("COMPID","COMPANYID");
		props.put("COMPDIVID","DIVISIONID");
		
		return props;
	}
}
