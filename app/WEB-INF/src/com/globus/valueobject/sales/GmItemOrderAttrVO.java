package com.globus.valueobject.sales;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmItemOrderAttrVO extends GmDataStoreVO {
	
	private String itemordattrid = "";
	private String orderid = "";
	private String pnum = "";
	private String attrtype = "";
	private String attrvalue = "";
	private String qty = "";
	private String voidfl = "";
	private String attrid = "";
	/**
	 * @return the itemordattrid
	 */
	public String getItemordattrid() {
		return itemordattrid;
	}
	/**
	 * @param itemordattrid the itemordattrid to set
	 */
	public void setItemordattrid(String itemordattrid) {
		this.itemordattrid = itemordattrid;
	}
	/**
	 * @return the orderid
	 */
	public String getOrderid() {
		return orderid;
	}
	/**
	 * @param orderid the orderid to set
	 */
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}
	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	/**
	 * @return the attrtype
	 */
	public String getAttrtype() {
		return attrtype;
	}
	/**
	 * @param attrtype the attrtype to set
	 */
	public void setAttrtype(String attrtype) {
		this.attrtype = attrtype;
	}
	/**
	 * @return the attrvalue
	 */
	public String getAttrvalue() {
		return attrvalue;
	}
	/**
	 * @param attrvalue the attrvalue to set
	 */
	public void setAttrvalue(String attrvalue) {
		this.attrvalue = attrvalue;
	}
	/**
	 * @return the qty
	 */
	public String getQty() {
		return qty;
	}
	/**
	 * @param qty the qty to set
	 */
	public void setQty(String qty) {
		this.qty = qty;
	}
	/**
	 * @return the voidfl
	 */
	public String getVoidfl() {
		return voidfl;
	}
	/**
	 * @param voidfl the voidfl to set
	 */
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	/**
	 * @return the attrid
	 */
	public String getAttrid() {
		return attrid;
	}
	/**
	 * @param attrid the attrid to set
	 */
	public void setAttrid(String attrid) {
		this.attrid = attrid;
	}
	
	
	public static Map getOrderItemAttrProperties(){
		Map hmLinkedDbInputStr = new LinkedHashMap();  
		
		hmLinkedDbInputStr.put("VOIDFL","getVoidfl");
		hmLinkedDbInputStr.put("ITEMORDATTRID","getItemordattrid");
		hmLinkedDbInputStr.put("PNUM","getPnum");
		hmLinkedDbInputStr.put("QTY","getQty");
		hmLinkedDbInputStr.put("ATTRVALUE","getAttrvalue");
		hmLinkedDbInputStr.put("ATTRTYPE","getAttrtype");
		return hmLinkedDbInputStr;
	}

}
