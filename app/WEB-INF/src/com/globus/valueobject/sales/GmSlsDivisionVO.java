package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSlsDivisionVO extends GmCompanyVO {
	
	private String divisionid = "";
	private String divisionnm = "";
	/**
	 * @return the divisionid
	 */
	public String getDivisionid() {
		return divisionid;
	}
	/**
	 * @param divisionid the divisionid to set
	 */
	public void setDivisionid(String divisionid) {
		this.divisionid = divisionid;
	}
	/**
	 * @return the divisionm
	 */
	public String getDivisionnm() {
		return divisionnm;
	}
	/**
	 * @param divisionm the divisionm to set
	 */
	public void setDivisionnm(String divisionnm) {
		this.divisionnm = divisionnm;
	}
	
	@Transient
	public Properties getDivisionProperties()
	{
		Properties props = new Properties();
		props.put("ID","DIVISIONID");
		props.put("NM","DIVISIONNM");
		props.put("COMPID","COMPANYID");
		
		return props;
	}	
}
