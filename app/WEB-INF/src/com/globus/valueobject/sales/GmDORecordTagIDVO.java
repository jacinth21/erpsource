package com.globus.valueobject.sales;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;

/**
 * @author karthik
 * 
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmDORecordTagIDVO extends GmDataStoreVO {

  private String tagid = "";
  private String setid = "";
  private String setnm = "";
  private String tagusagefl = "";
  private String latitude = "";
  private String longitude = "";
  private String orderid = "";

/**
 * @return the tagid
 */
public String getTagid() {
	return tagid;
}

/**
 * @param tagid the tagid to set
 */
public void setTagid(String tagid) {
	this.tagid = tagid;
}

/**
 * @return the setid
 */
public String getSetid() {
	return setid;
}

/**
 * @param setid the setid to set
 */
public void setSetid(String setid) {
	this.setid = setid;
}

/**
 * @return the setnm
 */
public String getSetnm() {
	return setnm;
}

/**
 * @param setnm the setnm to set
 */
public void setSetnm(String setnm) {
	this.setnm = setnm;
}

/**
 * @return the tagusagefl
 */
public String getTagusagefl() {
	return tagusagefl;
}

/**
 * @param tagusagefl the tagusagefl to set
 */
public void setTagusagefl(String tagusagefl) {
	this.tagusagefl = tagusagefl;
}

/**
 * @return the latitude
 */
public String getLatitude() {
	return latitude;
}

/**
 * @param latitude the latitude to set
 */
public void setLatitude(String latitude) {
	this.latitude = latitude;
}

/**
 * @return the longitude
 */
public String getLongitude() {
	return longitude;
}

/**
 * @param longitude the longitude to set
 */
public void setLongitude(String longitude) {
	this.longitude = longitude;
}
/**
* @return the orderid
*/
public String getOrderid() {
	return orderid;
}

/**
* @param orderid the orderid to set
*/
public void setOrderid(String orderid) {
	this.orderid = orderid;
}



public static Map getDORecordTagProperties() {
    Map hmLinkedDbInputStr = new LinkedHashMap();

    hmLinkedDbInputStr.put("tagid", "getTagid");
    hmLinkedDbInputStr.put("setid", "getSetid");
    hmLinkedDbInputStr.put("setnm", "getSetnm");
    hmLinkedDbInputStr.put("tagusagefl", "getTagusagefl");
    hmLinkedDbInputStr.put("latitude", "getLatitude");
    hmLinkedDbInputStr.put("longitude", "getLongitude");
    return hmLinkedDbInputStr;
  }


}
