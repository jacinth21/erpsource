/**
 * FileName    : GmDeviceDataStoreVO.java 
 * Description : 
 * Author      : Tejeswara Reddy
 * Date        : April 18, 2017 
 * Copyright   : Globus Medical Inc
 */

package com.globus.valueobject.sales;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmDeviceDataStoreVO extends GmDataStoreVO{
	private String salesdbcurrsymbol = "";
	private String salesdbcurrtype = "";
	/**
	 * @return the salesdbcurrsymbol
	 */
	public String getSalesdbcurrsymbol() {
		return salesdbcurrsymbol;
	}
	/**
	 * @param salesdbcurrsymbol the salesdbcurrsymbol to set
	 */
	public void setSalesdbcurrsymbol(String salesdbcurrsymbol) {
		this.salesdbcurrsymbol = salesdbcurrsymbol;
	}
	/**
	 * @return the salesdbcurrtype
	 */
	public String getSalesdbcurrtype() {
		return salesdbcurrtype;
	}
	/**
	 * @param salesdbcurrtype the salesdbcurrtype to set
	 */
	public void setSalesdbcurrtype(String salesdbcurrtype) {
		this.salesdbcurrtype = salesdbcurrtype;
	}
	
}
