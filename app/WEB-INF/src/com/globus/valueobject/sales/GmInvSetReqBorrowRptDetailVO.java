package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmInvSetReqBorrowRptDetailVO extends GmSalesDashReqVO {
  
	private String setid = "";
	private String system = "";
	public String getSetid() {
		return setid;
	}

	public void setSetid(String setid) {
		this.setid = setid;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("SETID", "SETID");
    properties.put("SYSTEM", "SYSTEM");
	return properties;  
	}

}

