package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

public class GmSalesDrilldownVO extends GmSalesDashDrilldownVO {

	private String vpnm = "";      
	private String adnm = "";
	private String fieldsalesid = "";
	private String fieldsalesnm = "";
	private String amount = "";
	private String total = "";
	private String ads = "";
	private String tgtads = "";
	
	@Transient
	public Properties getDailySalesDetailDrilldownProperties(){
		Properties props = new Properties();
		props.put("DNAME","FIELDSALESNM");
		props.put("SALES","AMOUNT");
		props.put("NAME","ACCOUNTNM");
		props.put("ACC_ID","ACCOUNTID");
		props.put("DID","FIELDSALESID");
		return props;
	}
	
	@Transient
	public Properties getTodaySalesDetailsDrilldownProperties(){
		Properties props = new Properties();
		props.put("VP_NAME","VPNM");
		props.put("AD_NAME","ADNM");
		props.put("D_NAME","FIELDSALESNM");
		props.put("REP_NAME","REPNM");
		props.put("AC_NAME","ACCOUNTNM");
		props.put("AC_ID","ACCOUNTID");
		props.put("SALES","AMOUNT");
		return props;
	}
	
	/**
	 * @return the vpnm
	 */
	public String getVpnm() {
		return vpnm;
	}
	/**
	 * @param vpnm the vpnm to set
	 */
	public void setVpnm(String vpnm) {
		this.vpnm = vpnm;
	}
	/**
	 * @return the adnm
	 */
	public String getAdnm() {
		return adnm;
	}
	/**
	 * @param adnm the adnm to set
	 */
	public void setAdnm(String adnm) {
		this.adnm = adnm;
	}	
	
	public String getFieldsalesid() {
		return fieldsalesid;
	}

	public void setFieldsalesid(String fieldsalesid) {
		this.fieldsalesid = fieldsalesid;
	}
	/**
	 * @return the fieldsalesnm
	 */
	public String getFieldsalesnm() {
		return fieldsalesnm;
	}
	/**
	 * @param fieldsalesnm the fieldsalesnm to set
	 */
	public void setFieldsalesnm(String fieldsalesnm) {
		this.fieldsalesnm = fieldsalesnm;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}
	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}
	/**
	 * @return the ads
	 */
	public String getAds() {
		return ads;
	}
	/**
	 * @param ads the ads to set
	 */
	public void setAds(String ads) {
		this.ads = ads;
	}
	/**
	 * @return the tgtads
	 */
	public String getTgtads() {
		return tgtads;
	}
	/**
	 * @param tgtads the tgtads to set
	 */
	public void setTgtads(String tgtads) {
		this.tgtads = tgtads;
	}
	
}
