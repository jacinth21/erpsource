package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesDashGrowthResVO extends GmSalesDashReqVO{
	
	private String type = "" ;
	private String mtd = "" ;
	private String ytd = ""  ;
	
	/**
	 * @return the mtd
	 */
	public String getMtd() {
		return mtd;
	}


	/**
	 * @param mtd the mtd to set
	 */
	public void setMtd(String mtd) {
		this.mtd = mtd;
	}


	/**
	 * @return the ytd
	 */
	public String getYtd() {
		return ytd;
	}


	/**
	 * @param ytd the ytd to set
	 */
	public void setYtd(String ytd) {
		this.ytd = ytd;
	}

	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}



	
	@Transient
	public Properties getSalesDashGrowthProperties(){
		Properties props = new Properties();
		props.put("TYPE","TYPE");
		props.put("MTD","MTD");
		props.put("YTD","YTD");
		
		return props;
	}

}
