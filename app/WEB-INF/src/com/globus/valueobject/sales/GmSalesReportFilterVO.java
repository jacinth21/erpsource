package com.globus.valueobject.sales;

import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesReportFilterVO extends GmSalesDashReqVO{

	private String sysonlycon ="";
	private String baselinenm="";
	private String turns="";
	private String type="";
	private String toyear="";
	private String loantypdisable="";
	private String pnum="";
	private String frmmonth="";
	private String conperiod="";
	private String hsetnum="";
	private String did="";
	private String consummtype="";
	private String salestype="";
	private String frmyear="";
	private String action="";
	private String repid="";
	private String showarrowfl="";
	private String consettype="";
	private String hpnum="";
	private String adid="";
	private String tomonth="";
	private String acctid="";
	private String loanertype="";
	private String setnum="";
	private String headprefix="";
	private String status="";
	private String hcbonm="";
	private String vpid="";
	private String contype="";
	private String hideamtfl="";
	private String turnslabel="";
	private String state="";
	private String showai="";
	private String showdisabled="";
	private String terrid="";
	private String showpercentfl="";
	private String setid=""; 
	
	private String condition="";
	private String accessfilter="";
	private String region="";
	private String groupaccid="";
	
	public String getSysonlycon() {
		return sysonlycon;
	}
	public void setSysonlycon(String sysonlycon) {
		this.sysonlycon = sysonlycon;
	}
	public String getBaselinenm() {
		return baselinenm;
	}
	public void setBaselinenm(String baselinenm) {
		this.baselinenm = baselinenm;
	}
	public String getTurns() {
		return turns;
	}
	public void setTurns(String turns) {
		this.turns = turns;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getToyear() {
		return toyear;
	}
	public void setToyear(String toyear) {
		this.toyear = toyear;
	}
	public String getLoantypdisable() {
		return loantypdisable;
	}
	public void setLoantypdisable(String loantypdisable) {
		this.loantypdisable = loantypdisable;
	}
	public String getPnum() {
		return pnum;
	}
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	public String getFrmmonth() {
		return frmmonth;
	}
	public void setFrmmonth(String frmmonth) {
		this.frmmonth = frmmonth;
	}
	public String getConperiod() {
		return conperiod;
	}
	public void setConperiod(String conperiod) {
		this.conperiod = conperiod;
	}
	public String getHsetnum() {
		return hsetnum;
	}
	public void setHsetnum(String hsetnum) {
		this.hsetnum = hsetnum;
	}
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	public String getConsummtype() {
		return consummtype;
	}
	public void setConsummtype(String consummtype) {
		this.consummtype = consummtype;
	}
	public String getSalestype() {
		return salestype;
	}
	public void setSalestype(String salestype) {
		this.salestype = salestype;
	}
	public String getFrmyear() {
		return frmyear;
	}
	public void setFrmyear(String frmyear) {
		this.frmyear = frmyear;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getRepid() {
		return repid;
	}
	public void setRepid(String repid) {
		this.repid = repid;
	}
	public String getShowarrowfl() {
		return showarrowfl;
	}
	public void setShowarrowfl(String showarrowfl) {
		this.showarrowfl = showarrowfl;
	}
	public String getConsettype() {
		return consettype;
	}
	public void setConsettype(String consettype) {
		this.consettype = consettype;
	}
	public String getHpnum() {
		return hpnum;
	}
	public void setHpnum(String hpnum) {
		this.hpnum = hpnum;
	}
	public String getAdid() {
		return adid;
	}
	public void setAdid(String adid) {
		this.adid = adid;
	}
	public String getTomonth() {
		return tomonth;
	}
	public void setTomonth(String tomonth) {
		this.tomonth = tomonth;
	}
	public String getAcctid() {
		return acctid;
	}
	public void setAcctid(String acctid) {
		this.acctid = acctid;
	}
	public String getLoanertype() {
		return loanertype;
	}
	public void setLoanertype(String loanertype) {
		this.loanertype = loanertype;
	}
	public String getSetnum() {
		return setnum;
	}
	public void setSetnum(String setnum) {
		this.setnum = setnum;
	}
	public String getHeadprefix() {
		return headprefix;
	}
	public void setHeadprefix(String headprefix) {
		this.headprefix = headprefix;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getHcbonm() {
		return hcbonm;
	}
	public void setHcbonm(String hcbonm) {
		this.hcbonm = hcbonm;
	}
	public String getVpid() {
		return vpid;
	}
	public void setVpid(String vpid) {
		this.vpid = vpid;
	}
	public String getContype() {
		return contype;
	}
	public void setContype(String contype) {
		this.contype = contype;
	}
	public String getHideamtfl() {
		return hideamtfl;
	}
	public void setHideamtfl(String hideamtfl) {
		this.hideamtfl = hideamtfl;
	}
	public String getTurnslabel() {
		return turnslabel;
	}
	public void setTurnslabel(String turnslabel) {
		this.turnslabel = turnslabel;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getShowai() {
		return showai;
	}
	public void setShowai(String showai) {
		this.showai = showai;
	}
	public String getShowdisabled() {
		return showdisabled;
	}
	public void setShowdisabled(String showdisabled) {
		this.showdisabled = showdisabled;
	}
	public String getTerrid() {
		return terrid;
	}
	public void setTerrid(String terrid) {
		this.terrid = terrid;
	}
	public String getShowpercentfl() {
		return showpercentfl;
	}
	public void setShowpercentfl(String showpercentfl) {
		this.showpercentfl = showpercentfl;
	}
	public String getSetid() {
		return setid;
	}
	public void setSetid(String setid) {
		this.setid = setid;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getAccessfilter() {
		return accessfilter;
	}
	public void setAccessfilter(String accessfilter) {
		this.accessfilter = accessfilter;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}	
	public String getGroupaccid() {
		return groupaccid;
	}
	public void setGroupaccid(String groupaccid) {
		this.groupaccid = groupaccid;
	}
}
