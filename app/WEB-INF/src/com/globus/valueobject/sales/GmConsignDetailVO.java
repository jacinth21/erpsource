package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmConsignDetailVO extends GmSalesReportVO{

	private String adid ="";
	private String did="";
	private String dname="";
	private String groupid="";
	private String groupnm="";
	private String vsetid="";
	private String vsetidcount="";
	private String lsetid="";
	private String vsetnm ="";
	private String vconsrptid="";
	private String vsetcount="";
	private String vasetcount="";
	private String vsetcost="";
	private String seq_no="";
	private String pline="";
	private String simage="";
	private String dimage="";
	private String aimage="";
	private String sgdimage="";
	private String sdimage="";
	private String type="";
	private String hch="";
	private String sharedstatus="";	
	
	@Transient
	public Properties getConsignDetailsProperties(){
		Properties props = new Properties();
		props.put("AD_ID","ADID");
		props.put("D_ID","DID");
		props.put("DNAME","DNAME");
		props.put("GROUPID","GROUPID");
		props.put("GROUPNM","GROUPNM");
		props.put("VSETID","VSETID");
		props.put("VSETIDCOUNT","VSETIDCOUNT");
		props.put("LSETID","LSETID");
		props.put("VSETNM","VSETNM");		
		props.put("VCONSRPTID","VCONSRPTID");
		props.put("VSETCOUNT","VSETCOUNT");
		props.put("VASETCOUNT","VASETCOUNT");
		props.put("VSETCOST","VSETCOST");
		props.put("C207_SEQ_NO","SEQ_NO");
		props.put("PLINE","PLINE");
		props.put("SIMAGE","SIMAGE");
		props.put("DIMAGE","DIMAGE");
		props.put("AIMAGE","AIMAGE");
		props.put("SGDIMAGE","SGDIMAGE");
		props.put("SDIMAGE","SDIMAGE");
		props.put("TYPE","TYPE");
		props.put("HCH","HCH");
		props.put("SHAREDSTATUS","SHAREDSTATUS");
				
		return props;
}
	
	
	public void setAdid(String adid) {
		this.adid = adid;
	}
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public String getGroupnm() {
		return groupnm;
	}
	public void setGroupnm(String groupnm) {
		this.groupnm = groupnm;
	}
	public String getVsetid() {
		return vsetid;
	}
	public void setVsetid(String vsetid) {
		this.vsetid = vsetid;
	}
	public String getVsetidcount() {
		return vsetidcount;
	}
	public void setVsetidcount(String vsetidcount) {
		this.vsetidcount = vsetidcount;
	}
	public String getLsetid() {
		return lsetid;
	}
	public void setLsetid(String lsetid) {
		this.lsetid = lsetid;
	}
	public String getVsetnm() {
		return vsetnm;
	}
	public void setVsetnm(String vsetnm) {
		this.vsetnm = vsetnm;
	}
	public String getVconsrptid() {
		return vconsrptid;
	}
	public void setVconsrptid(String vconsrptid) {
		this.vconsrptid = vconsrptid;
	}
	public String getVsetcount() {
		return vsetcount;
	}
	public void setVsetcount(String vsetcount) {
		this.vsetcount = vsetcount;
	}
	public String getVasetcount() {
		return vasetcount;
	}
	public void setVasetcount(String vasetcount) {
		this.vasetcount = vasetcount;
	}
	public String getVsetcost() {
		return vsetcost;
	}
	public void setVsetcost(String vsetcost) {
		this.vsetcost = vsetcost;
	}
	public String getSeq_no() {
		return seq_no;
	}
	public void setSeq_no(String seq_no) {
		this.seq_no = seq_no;
	}
	public String getPline() {
		return pline;
	}
	public void setPline(String pline) {
		this.pline = pline;
	}
	public String getSimage() {
		return simage;
	}
	public void setSimage(String simage) {
		this.simage = simage;
	}
	public String getDimage() {
		return dimage;
	}
	public void setDimage(String dimage) {
		this.dimage = dimage;
	}
	public String getAimage() {
		return aimage;
	}
	public void setAimage(String aimage) {
		this.aimage = aimage;
	}
	public String getSgdimage() {
		return sgdimage;
	}
	public void setSgdimage(String sgdimage) {
		this.sgdimage = sgdimage;
	}
	public String getSdimage() {
		return sdimage;
	}
	public void setSdimage(String sdimage) {
		this.sdimage = sdimage;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getHch() {
		return hch;
	}
	public void setHch(String hch) {
		this.hch = hch;
	}
	public String getSharedstatus() {
		return sharedstatus;
	}
	public void setSharedstatus(String sharedstatus) {
		this.sharedstatus = sharedstatus;
	}

}
