package com.globus.valueobject.sales;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSetDetlVO extends GmDataStoreVO implements Serializable{
	
	private String setid = "";
	private String setnm = "";

	/**
	 * @return the setid
	 */
	public String getSetid() {
		return setid;
	}

	/**
	 * @param setid the setid to set
	 */
	public void setSetid(String setid) {
		this.setid = setid;
	}

	/**
	 * @return the setnm
	 */
	public String getSetnm() {
		return setnm;
	}

	/**
	 * @param setnm the setnm to set
	 */
	public void setSetnm(String setnm) {
		this.setnm = setnm;
	}

	public static Map getSetDetlVOProperties(){
		Map hmLinkedDbInputStr = new LinkedHashMap();  
		hmLinkedDbInputStr.put("setid", "getSetid");
		hmLinkedDbInputStr.put("setnm", "getSetnm");
		return hmLinkedDbInputStr;
	}
	
}