package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmCodeLookupVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesDashResVO extends GmSalesDashReqVO{
	
	private String type ;
	private String count ;
	private String id ;
	private String name = "";
	private String day = "";
	private String value ;
	private String percentage ;
	private String drilldownurl ;
	private String datefmt = "";
	private String date = "";
	private String ads = "";
	private String tgtads = "";
	private String dof  = "";
	private String amt  = "";

	private List<GmCodeLookupVO> viewByList  = null;


	private List<GmSalesDashResVO> listGmSalesDashResListVO    = null;
	private List<GmLoanerDrilldownVO> listGmLoanerDrilldownVO =null;
	private List<GmOrdersDrilldownVO> listGmOrdersDrilldownVO =null;
	private List<GmReturnsDrilldownVO> listGmReturnsDrilldownVO =null;
	private List<GmCaseDrilldownVO> listGmCaseDrilldownVO =null;
	private List<GmConsignmentDrilldownVO> listGmConsignmentDrilldownVO =null;
	private List<GmSalesDashGrowthResVO> listGmSalesDashGrowthResVO =null;
	private List<GmSalesDrilldownVO> listGmSalesDrilldownVO =null;

	@Transient
	public Properties getSalesDashBoardProperties(){
			Properties props = new Properties();
			props.put("NAME","TYPE");
			props.put("TOTAL","COUNT");
			props.put("DRILLDOWNURL","DRILLDOWNURL");
			props.put("AMT","AMT");
			
			return props;
	}
	
	@Transient
	public Properties getSalesDashTodaySalesProperties(){
		Properties props = new Properties();
		props.put("ID","ID");
		props.put("NAME","NAME");
		props.put("SALES","VALUE");
		props.put("PERCENTAGE","PERCENTAGE");
		
		return props;
    }
	
	@Transient
	public Properties getSalesDashMTDSalesProperties(){
		Properties props = new Properties();
		props.put("DAY","ID");
		props.put("SDATEFMT","NAME");
		props.put("SALES","VALUE");
		props.put("DOF","DOF");
		
		return props;
    }
	
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getCount() {
		return count;
	}


	public void setCount(String count) {
		this.count = count;
	}
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDay() {
		return day;
	}


	public void setDay(String day) {
		this.day = day;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getPercentage() {
		return percentage;
	}


	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}


	public String getDrilldownurl() {
		return drilldownurl;
	}


	public void setDrilldownurl(String drilldownurl) {
		this.drilldownurl = drilldownurl;
	}

	public String getDatefmt() {
		return datefmt;
	}


	public void setDatefmt(String datefmt) {
		this.datefmt = datefmt;
	}


	public String getDof() {
		return dof;
	}

	public void setDof(String dof) {
		this.dof = dof;
	}
	
	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}
	
	public List<GmCodeLookupVO> getViewByList() {
		return viewByList;
	}

	
	public void setViewByList(List<GmCodeLookupVO> viewByList) {
		this.viewByList = viewByList;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAds() {
		return ads;
	}

	public void setAds(String ads) {
		this.ads = ads;
	}

	public String getTgtads() {
		return tgtads;
	}

	public void setTgtads(String tgtads) {
		this.tgtads = tgtads;
	}

	public List<GmSalesDashResVO> getListGmSalesDashResListVO() {
		return listGmSalesDashResListVO;
	}

	public void setListGmSalesDashResListVO(
			List<GmSalesDashResVO> listGmSalesDashResListVO) {
		this.listGmSalesDashResListVO = listGmSalesDashResListVO;
	}

	public List<GmLoanerDrilldownVO> getListGmLoanerDrilldownVO() {
		return listGmLoanerDrilldownVO;
	}


	public void setListGmLoanerDrilldownVO(
			List<GmLoanerDrilldownVO> listGmLoanerDrilldownVO) {
		this.listGmLoanerDrilldownVO = listGmLoanerDrilldownVO;
	}

	public List<GmOrdersDrilldownVO> getListGmOrdersDrilldownVO() {
		return listGmOrdersDrilldownVO;
	}


	public void setListGmOrdersDrilldownVO(
			List<GmOrdersDrilldownVO> listGmOrdersDrilldownVO) {
		this.listGmOrdersDrilldownVO = listGmOrdersDrilldownVO;
	}


	public List<GmReturnsDrilldownVO> getListGmReturnsDrilldownVO() {
		return listGmReturnsDrilldownVO;
	}


	public void setListGmReturnsDrilldownVO(
			List<GmReturnsDrilldownVO> listGmReturnsDrilldownVO) {
		this.listGmReturnsDrilldownVO = listGmReturnsDrilldownVO;
	}

	public List<GmCaseDrilldownVO> getListGmCaseDrilldownVO() {
		return listGmCaseDrilldownVO;
	}


	public void setListGmCaseDrilldownVO(
			List<GmCaseDrilldownVO> listGmCaseDrilldownVO) {
		this.listGmCaseDrilldownVO = listGmCaseDrilldownVO;
	}

	public List<GmConsignmentDrilldownVO> getListGmConsignmentDrilldownVO() {
		return listGmConsignmentDrilldownVO;
	}


	public void setListGmConsignmentDrilldownVO(
			List<GmConsignmentDrilldownVO> listGmConsignmentDrilldownVO) {
		this.listGmConsignmentDrilldownVO = listGmConsignmentDrilldownVO;
	}
	
	public List<GmSalesDashGrowthResVO> getListGmSalesDashGrowthResVO() {
		return listGmSalesDashGrowthResVO;
	}

	public void setListGmSalesDashGrowthResVO(
			List<GmSalesDashGrowthResVO> listGmSalesDashGrowthResVO) {
		this.listGmSalesDashGrowthResVO = listGmSalesDashGrowthResVO;
	}

	public List<GmSalesDrilldownVO> getListGmSalesDrilldownVO() {
		return listGmSalesDrilldownVO;
	}

	public void setListGmSalesDrilldownVO(
			List<GmSalesDrilldownVO> listGmSalesDrilldownVO) {
		this.listGmSalesDrilldownVO = listGmSalesDrilldownVO;
	}


}
