package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmOrdersDrilldownVO extends GmSalesDashDrilldownVO{
	 
	private String orderdt = "" ;
	private String total = "" ;
	private String shipdt = "" ;
	private String ponum = "" ;
	private String donum = "" ;
	private String type = "";
	private String trackingnum = "";
	private String trackurl = "";
	private String voidfl = "";
	private String parentorderid = "";
	private String pricereq = "";
	private String poamount = "";
	private String shipcost = "";
	private String holdfl = "";
	private String uploadfl = "";
	private String poenterdt = "";
	private String category = "";
	private String postatus = "";
	private String dostatus = "";
	//PC-4864 Add Online PDF option on Pending PO Screen in Globus App
	private String ordmode = "";

	@Transient
	public Properties getOrderDrilldownProperties(){
		Properties props = new Properties();
		props.put("ODT","ORDERDT");
		props.put("COST","TOTAL");
		props.put("SDATE","SHIPDT");
		props.put("PO","PONUM");
		props.put("DO","DONUM");
		props.put("ORDERTYPEDESC","TYPE");
		props.put("TRACK","TRACKINGNUM");
		props.put("ANAME","ACCOUNTNM");
		props.put("REPNAME","REPNM");
		props.put("ORDID","TXNID");
		props.put("TRACKURL","TRACKURL");
		props.put("VFL","VOIDFL");
		props.put("PRICEREQ","PRICEREQ");
		props.put("POAMOUNT","POAMOUNT");
		props.put("SHIPCOST","SHIPCOST");
		props.put("HOLDFL","HOLDFL");
		props.put("UPLOADFL","UPLOADFL");
		props.put("POENTERDT","POENTERDT");
		props.put("CATEGORY","CATEGORY");
		props.put("POSTATUS","POSTATUS");
		props.put("DOSTATUS","DOSTATUS");
		props.put("ORDMODE","ORDMODE");
		return props;
	}
	
	@Transient
	public Properties getDOReportProperties(){
		Properties props = new Properties();
		props.put("ODT","ORDERDT");
		props.put("COST","TOTAL");
		props.put("PO","PONUM");
		props.put("ORDID","DONUM");
		props.put("ACCTID","ACCOUNTID");
		props.put("ANAME","ACCOUNTNM");
		props.put("NAME","STATUS");
		props.put("VFL","VOIDFL");
		props.put("DO","PARENTORDERID");
		return props;
	}
	
	/**
	 * @return the orderdt
	 */
	public String getOrderdt() {
		return orderdt;
	}


	/**
	 * @param orderdt the orderdt to set
	 */
	public void setOrderdt(String orderdt) {
		this.orderdt = orderdt;
	}


	/**
	 * @return the total
	 */
	public String getTotal() {
		return total;
	}


	/**
	 * @param total the total to set
	 */
	public void setTotal(String total) {
		this.total = total;
	}


	/**
	 * @return the shipdt
	 */
	public String getShipdt() {
		return shipdt;
	}


	/**
	 * @param shipdt the shipdt to set
	 */
	public void setShipdt(String shipdt) {
		this.shipdt = shipdt;
	}


	/**
	 * @return the ponum
	 */
	public String getPonum() {
		return ponum;
	}


	/**
	 * @param ponum the ponum to set
	 */
	public void setPonum(String ponum) {
		this.ponum = ponum;
	}


	/**
	 * @return the donum
	 */
	public String getDonum() {
		return donum;
	}


	/**
	 * @param donum the donum to set
	 */
	public void setDonum(String donum) {
		this.donum = donum;
	}


	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}


	/**
	 * @return the trackingnum
	 */
	public String getTrackingnum() {
		return trackingnum;
	}


	/**
	 * @param trackingnum the trackingnum to set
	 */
	public void setTrackingnum(String trackingnum) {
		this.trackingnum = trackingnum;
	}	
	
	public String getTrackurl() {
		return trackurl;
	}


	public void setTrackurl(String trackurl) {
		this.trackurl = trackurl;
	}

	public String getVoidfl() {
		return voidfl;
	}

	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}

	/**
	 * @return the parentorderid
	 */
	public String getParentorderid() {
		return parentorderid;
	}

	/**
	 * @param parentorderid the parentorderid to set
	 */
	public void setParentorderid(String parentorderid) {
		this.parentorderid = parentorderid;
	}
	
	

	/**
	 * @return the pricereq
	 */
	public String getPricereq() {
		return pricereq;
	}

	/**
	 * @param pricereq the pricereq to set
	 */
	public void setPricereq(String pricereq) {
		this.pricereq = pricereq;
	}
	
	/**
	 * @return the poamount
	 */
	public String getPoamount() {
		return poamount;
	}
	
	/**
	 * @param poamount the poamount to set
	 */
	public void setPoamount(String poamount) {
		this.poamount = poamount;
	}
	
	/**
	 * @return the shipcost
	 */
	public String getShipcost() {
		return shipcost;
	}
	/**
	 * @param shipcost the shipcost to set
	 */
	public void setShipcost(String shipcost) {
		this.shipcost = shipcost;
	}
	
	/**
	 * @return the holdfl
	 */
	public String getHoldfl() {
		return holdfl;
	}
	/**
	 * @param holdfl the holdfl to set
	 */
	public void setHoldfl(String holdfl) {
		this.holdfl = holdfl;
	}
	
	/**
	 * @return the uploadfl
	 */
	public String getUploadfl() {
		return uploadfl;
	}
	/**
	 * @param uploadfl the uploadfl to set
	 */
	public void setUploadfl(String uploadfl) {
		this.uploadfl = uploadfl;
	}
	
	/**
	 * @return the poenterdt
	 */
	public String getPoenterdt() {
		return poenterdt;
	}
	/**
	 * @param poenterdt the poenterdt to set
	 */
	public void setPoenterdt(String poenterdt) {
		this.poenterdt = poenterdt;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the postatus
	 */
	public String getPostatus() {
		return postatus;
	}
	/**
	 * @param postatus the postatus to set
	 */
	public void setPostatus(String postatus) {
		this.postatus = postatus;
	}
	/**
	 * @return the dostatus
	 */
	public String getDostatus() {
		return dostatus;
	}
	/**
	 * @param dostatus the dostatus to set
	 */
	public void setDostatus(String dostatus) {
		this.dostatus = dostatus;
	}

	/**
	 * @return the ordmode
	 */
	public String getOrdmode() {
		return ordmode;
	}

	/**
	 * @param ordmode the ordmode to set
	 */
	public void setOrdmode(String ordmode) {
		this.ordmode = ordmode;
	}
	
}
