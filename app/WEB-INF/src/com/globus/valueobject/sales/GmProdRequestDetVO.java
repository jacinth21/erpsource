package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmProdRequestDetVO extends GmDataStoreVO {


  private String caseid = "";
  private String prdreqid = "";
  private String prdreqdid = "";
  private String pnum = "";
  private String pdesc = "";
  private String setid = "";
  private String reqqty = "";
  private String aprqty = "";
  private String status = "";
  private String reqby = "";
  private String requestdt = "";
  private String fieldsales = "";
  private String retflag = "";

  @Transient
  public Properties getProdRequestDetailProperties() {
    Properties props = new Properties();
    props.put("CASEID", "CASEID");
    props.put("LNREQID", "PRDREQDID");
    props.put("REQUESTID", "PRDREQID");
    props.put("PNUM", "PNUM");
    props.put("PDESC", "PDESC");
    props.put("FIELDSALES", "FIELDSALES");
    props.put("QTY", "REQQTY");
    props.put("APPROVE_QTY", "APRQTY");
    props.put("STATUS", "STATUS");
    props.put("REPNAME", "REQBY");
    props.put("REQDATE", "REQUESTDT");
    props.put("RETFLAG", "RETFLAG");


    return props;
  }


  public String getCaseid() {
    return caseid;
  }


  public void setCaseid(String caseid) {
    this.caseid = caseid;
  }


  public String getPrdreqid() {
    return prdreqid;
  }


  public void setPrdreqid(String prdreqid) {
    this.prdreqid = prdreqid;
  }


  public String getPrdreqdid() {
    return prdreqdid;
  }


  public void setPrdreqdid(String prdreqdid) {
    this.prdreqdid = prdreqdid;
  }


  public String getPnum() {
    return pnum;
  }


  public void setPnum(String pnum) {
    this.pnum = pnum;
  }


  public String getPdesc() {
    return pdesc;
  }


  public void setPdesc(String pdesc) {
    this.pdesc = pdesc;
  }


  public String getSetid() {
    return setid;
  }


  public void setSetid(String setid) {
    this.setid = setid;
  }


  public String getReqqty() {
    return reqqty;
  }


  public void setReqqty(String reqqty) {
    this.reqqty = reqqty;
  }


  public String getAprqty() {
    return aprqty;
  }


  public void setAprqty(String aprqty) {
    this.aprqty = aprqty;
  }


  public String getStatus() {
    return status;
  }


  public void setStatus(String status) {
    this.status = status;
  }


  public String getReqby() {
    return reqby;
  }


  public void setReqby(String reqby) {
    this.reqby = reqby;
  }


  public String getRequestdt() {
    return requestdt;
  }


  public void setRequestdt(String requestdt) {
    this.requestdt = requestdt;
  }


  public String getFieldsales() {
    return fieldsales;
  }


  public void setFieldsales(String fieldsales) {
    this.fieldsales = fieldsales;
  }


  public String getRetflag() {
    return retflag;
  }


  public void setRetflag(String retflag) {
    this.retflag = retflag;
  }



}
