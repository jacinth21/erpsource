package com.globus.valueobject.sales;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesFilterVO extends GmSalesDashReqVO{
	private List<GmCompanyVO> listGmCompanyListVO = null;
	private List<GmSlsDivisionVO> listGmSlsDivisionListVO = null;
	private List<GmSlsZoneVO> listGmSlsZoneListVO = null;
	private List<GmSlsRegionVO> listGmSlsRegionListVO = null;
	private List<GmFieldSalesVO> listGmFieldSalesListVO = null;
	/**
	 * @return the listGmCompanyListVO
	 */
	public List<GmCompanyVO> getListGmCompanyListVO() {
		return listGmCompanyListVO;
	}
	/**
	 * @param listGmCompanyListVO the listGmCompanyListVO to set
	 */
	public void setListGmCompanyListVO(List<GmCompanyVO> listGmCompanyListVO) {
		this.listGmCompanyListVO = listGmCompanyListVO;
	}
	/**
	 * @return the listGmSlsDivisionListVO
	 */
	public List<GmSlsDivisionVO> getListGmSlsDivisionListVO() {
		return listGmSlsDivisionListVO;
	}
	/**
	 * @param listGmSlsDivisionListVO the listGmSlsDivisionListVO to set
	 */
	public void setListGmSlsDivisionListVO(
			List<GmSlsDivisionVO> listGmSlsDivisionListVO) {
		this.listGmSlsDivisionListVO = listGmSlsDivisionListVO;
	}
	/**
	 * @return the listGmSlsZoneListVO
	 */
	public List<GmSlsZoneVO> getListGmSlsZoneListVO() {
		return listGmSlsZoneListVO;
	}
	/**
	 * @param listGmSlsZoneListVO the listGmSlsZoneListVO to set
	 */
	public void setListGmSlsZoneListVO(List<GmSlsZoneVO> listGmSlsZoneListVO) {
		this.listGmSlsZoneListVO = listGmSlsZoneListVO;
	}
	/**
	 * @return the listGmSlsRegionListVO
	 */
	public List<GmSlsRegionVO> getListGmSlsRegionListVO() {
		return listGmSlsRegionListVO;
	}
	/**
	 * @param listGmSlsRegionListVO the listGmSlsRegionListVO to set
	 */
	public void setListGmSlsRegionListVO(List<GmSlsRegionVO> listGmSlsRegionListVO) {
		this.listGmSlsRegionListVO = listGmSlsRegionListVO;
	}
	/**
	 * @return the listGmFieldSalesListVO
	 */
	public List<GmFieldSalesVO> getListGmFieldSalesListVO() {
		return listGmFieldSalesListVO;
	}
	/**
	 * @param listGmFieldSalesListVO the listGmFieldSalesListVO to set
	 */
	public void setListGmFieldSalesListVO(
			List<GmFieldSalesVO> listGmFieldSalesListVO) {
		this.listGmFieldSalesListVO = listGmFieldSalesListVO;
	}
	
	
	
}
