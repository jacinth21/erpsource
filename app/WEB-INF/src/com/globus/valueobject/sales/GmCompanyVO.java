package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCompanyVO extends GmDataStoreVO {
  private String companyid = "";
  private String companynm = "";
  private String cmpcurrsmb = "";
  private String cmprptcurrsmb = "";
  private String cmpcurrfmt = "";
  private String companylocale = "";
  private String currtype = "";
  private String txncurrencyid = "";
  private String cmplangid = "";

  public String getCmprptcurrsmb() {
    return cmprptcurrsmb;
  }

  public void setCmprptcurrsmb(String cmprptcurrsmb) {
    this.cmprptcurrsmb = cmprptcurrsmb;
  }

  /**
   * @return the companyid
   */
  public String getCompanyid() {
    return companyid;
  }

  /**
   * @param companyid the companyid to set
   */
  public void setCompanyid(String companyid) {
    this.companyid = companyid;
  }

  /**
   * @return the companynm
   */
  public String getCompanynm() {
    return companynm;
  }

  /**
   * @param companynm the companynm to set
   */
  public void setCompanynm(String companynm) {
    this.companynm = companynm;
  }


  /**
   * @return the cmpcurrsmb
   */
  public String getCmpcurrsmb() {
    return cmpcurrsmb;
  }

  /**
   * @param cmpcurrsmb the cmpcurrsmb to set
   */
  public void setCmpcurrsmb(String cmpcurrsmb) {
    this.cmpcurrsmb = cmpcurrsmb;
  }

  /**
   * @return the cmpcurrfmt
   */
  public String getCmpcurrfmt() {
    return cmpcurrfmt;
  }

  /**
   * @param cmpcurrfmt the cmpcurrfmt to set
   */
  public void setCmpcurrfmt(String cmpcurrfmt) {
    this.cmpcurrfmt = cmpcurrfmt;
  }

  /**
   * @return the currtype
   */
  public String getCurrtype() {
    return currtype;
  }

  /**
   * @param currtype the currtype to set
   */
  public void setCurrtype(String currtype) {
    this.currtype = currtype;
  }



  /**
   * @return the companylocale
   */
  public String getCompanylocale() {
    return companylocale;
  }

  /**
   * @param companylocale the companylocale to set
   */
  public void setCompanylocale(String companylocale) {
    this.companylocale = companylocale;
  }


  /**
   * @return the txncurrencyid
   */
  public String getTxncurrencyid() {
    return txncurrencyid;
  }

  /**
   * @param txncurrencyid the txncurrencyid to set
   */
  public void setTxncurrencyid(String txncurrencyid) {
    this.txncurrencyid = txncurrencyid;
  }

  /**
   * @return the cmplangid
   */
  public String getCmplangid() {
    return cmplangid;
  }

  /**
   * @param cmplangid the cmplangid to set
   */
  public void setCmplangid(String cmplangid) {
    this.cmplangid = cmplangid;
  }

  @Transient
  public Properties getCompanyProperties() {
    Properties props = new Properties();
    props.put("CODEID", "COMPANYID");
    props.put("CODENM", "COMPANYNM");
    props.put("COMPANYID", "COMPANYID");
    props.put("COMPANYNM", "COMPANYNM");
    props.put("CMPTZONE", "CMPTZONE");
    props.put("CMPDFMT", "CMPDFMT");
    props.put("TXNCURR", "CMPCURRSMB");
    props.put("TXNRPTCURR", "CMPRPTCURRSMB");
    props.put("CMPCURRFMT", "CMPCURRFMT");
    props.put("COMPANYLOCALE", "COMPANYLOCALE");
    props.put("CURRTYPE", "CURRTYPE");
    props.put("TXNCURRENCYID", "TXNCURRENCYID");
    props.put("CMPLANGID", "CMPLANGID");
    return props;
  }
}
