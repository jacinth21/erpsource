package com.globus.valueobject.sales;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author ksomanathan
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmDOSurgeryDetailVO extends GmOrderAttrVO {

  public static Map getOrderAttrProperties() {
    Map hmLinkedDbInputStr = new LinkedHashMap();

    hmLinkedDbInputStr.put("ORDERID", "getOrderid");
    hmLinkedDbInputStr.put("ATTRTYPE", "getAttrtype");
    hmLinkedDbInputStr.put("ATTRVALUE", "getAttrvalue");
    return hmLinkedDbInputStr;
  }

}
