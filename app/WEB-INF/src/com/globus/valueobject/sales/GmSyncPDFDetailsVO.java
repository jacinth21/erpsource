package com.globus.valueobject.sales;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSyncPDFDetailsVO extends GmDataStoreVO {

  private GmSyncPDFInfoVO[] gmSyncPDFInfoVO;

  public GmSyncPDFInfoVO[] getGmSyncPDFInfoVO() {
    return gmSyncPDFInfoVO;
  }

  public void setGmSyncPDFInfoVO(GmSyncPDFInfoVO[] gmSyncPDFInfoVO) {
    this.gmSyncPDFInfoVO = gmSyncPDFInfoVO;
  }

}
