package com.globus.valueobject.sales;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataVO;
import com.fasterxml.jackson.annotation.JsonInclude;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesListVO extends GmDataVO{
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmSyncPDFInfoVO> gmSyncPDFInfoVO = null;

	public List<GmSyncPDFInfoVO> getGmSyncPDFInfoVO() {
		return gmSyncPDFInfoVO;
	}

	public void setGmSyncPDFInfoVO(List<GmSyncPDFInfoVO> gmSyncPDFInfoVO) {
		this.gmSyncPDFInfoVO = gmSyncPDFInfoVO;
	}
	
	
}
