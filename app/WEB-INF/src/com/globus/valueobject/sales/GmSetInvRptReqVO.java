package com.globus.valueobject.sales;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSetInvRptReqVO extends GmSalesDashReqVO{
	
	private String	setid = "";
	private String	system = "";
	private String	distid = "";
	private String	type = "";
	
	
	private List<GmSetInvRptVO> listGmSetInvRptVO  = null;	

	public String getSetid() {
		return setid;
	}
	public void setSetid(String setid) {
		this.setid = setid;
	}	
	public String getDistid() {
		return distid;
	}
	public void setDistid(String distid) {
		this.distid = distid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}	
	public String getSystem() {
		return system;
	}
	public void setSystem(String system) {
		this.system = system;
	}
	public List<GmSetInvRptVO> getListGmSetInvRptVO() {
		return listGmSetInvRptVO;
	}
	public void setListGmSetInvRptVO(List<GmSetInvRptVO> listGmSetInvRptVO) {
		this.listGmSetInvRptVO = listGmSetInvRptVO;
	}
	
}
