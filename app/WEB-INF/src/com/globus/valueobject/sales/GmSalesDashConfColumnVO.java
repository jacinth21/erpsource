package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmSalesDashConfColumnVO extends GmSalesDashDrilldownVO{
	
	private String ddgrpid = "" ;
	private String ddgrpnm = "" ;
	private String seq_num = "" ;
	private String ddcolumnnm = "" ;
	private String ddcolumnnmshrtnm = "" ;
	private String voidfl = "" ;
	
	private List<GmSalesDashConfColumnVO> listGmSalesDashConfColumnVO =null;
	
	
	public String getDdgrpid() {
		return ddgrpid;
	}
	public void setDdgrpid(String ddgrpid) {
		this.ddgrpid = ddgrpid;
	}
	public String getDdgrpnm() {
		return ddgrpnm;
	}
	public void setDdgrpnm(String ddgrpnm) {
		this.ddgrpnm = ddgrpnm;
	}
	public String getDdcolumnnm() {
		return ddcolumnnm;
	}
	public void setDdcolumnnm(String ddcolumnnm) {
		this.ddcolumnnm = ddcolumnnm;
	}
	public String getDdcolumnnmshrtnm() {
		return ddcolumnnmshrtnm;
	}
	public void setDdcolumnnmshrtnm(String ddcolumnnmshrtnm) {
		this.ddcolumnnmshrtnm = ddcolumnnmshrtnm;
	}
	public String getVoidfl() {
		return voidfl;
	}
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}

	public String getSeq_num() {
		return seq_num;
	}
	public void setSeq_num(String seq_num) {
		this.seq_num = seq_num;
	}
	
	
	public List<GmSalesDashConfColumnVO> getListGmSalesDashConfColumnVO() {
		return listGmSalesDashConfColumnVO;
	}
	public void setListGmSalesDashConfColumnVO(
			List<GmSalesDashConfColumnVO> listGmSalesDashConfColumnVO) {
		this.listGmSalesDashConfColumnVO = listGmSalesDashConfColumnVO;
	}
	
	@Transient
	public Properties getDrilldowngroupProperties(){
		Properties props = new Properties();
		props.put("DDGRPID","DDGRPID");
		props.put("DDCOLUMNNM","DDCOLUMNNM");
		
		return props;
	}
	
	@Transient
	public Properties getDrilldowndetailsProperties(){
		Properties props = new Properties();
		props.put("DDGRPID","DDGRPID");
		props.put("DDCOLUMNNM","DDCOLUMNNM");
		props.put("SEQ_NUM","SEQ_NUM");
		props.put("DDGRPNM","DDGRPNM");
		props.put("DDCOLUMNNMSHRTNM","DDCOLUMNNMSHRTNM");
		props.put("VOIDFL","VOIDFL");
		return props;
	}
	
	
	
}