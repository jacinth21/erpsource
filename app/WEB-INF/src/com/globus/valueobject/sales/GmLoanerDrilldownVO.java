package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmLoanerDrilldownVO extends GmSalesDashDrilldownVO {

  private String assocrepnm = "";
  private String surgerydt = "";
  private String loanedon = "";
  private String etchid = "";
  private String tagid = "";
  private String expreturndt = "";
  private String dayselpsd = "";
  private String daysdue = "";
  private String trackingnum = "";
  private String partnum = "";
  private String partdesc = "";
  private String fieldsalesnm = "";
  private String missqty = "";
  private String trackurl = "";
  private String pendingappr = "";
  private String extcount = "";
  private String reqdtlid = "";
  private String newextndate = "";
  private String consignid = "";
  private String extwholereq = "";
  private String txt_logreason = "";
  private String parentextncnt = "";
  private String loandate = "";
  private String returndate = "";
  private String businessdays = "";
  private String usage = "";
  private String distid = "";
  private String requestid = "";
  private String usagesum = "";
  private String month = "";
  private String firstdate = "";
  private String lastdate = "";
  private String source = "";
  private String overallusage = "";
  private String actreturndt = "";
  private String incidentdt = "";
  private String chargeamt = "";
  private String deductiondt = "";
  private String chargests = "";


	/**
	 * @return the actreturndt
	 */
	public String getActreturndt() {
		return actreturndt;
	}
	
	
	/**
	 * @param actreturndt the actreturndt to set
	 */
	public void setActreturndt(String actreturndt) {
		this.actreturndt = actreturndt;
	}
	
	
	/**
	 * @return the incidentdt
	 */
	public String getIncidentdt() {
		return incidentdt;
	}
	
	
	/**
	 * @param incidentdt the incidentdt to set
	 */
	public void setIncidentdt(String incidentdt) {
		this.incidentdt = incidentdt;
	}
	
	
	/**
	 * @return the chargeamt
	 */
	public String getChargeamt() {
		return chargeamt;
	}
	
	
	/**
	 * @param chargeamt the chargeamt to set
	 */
	public void setChargeamt(String chargeamt) {
		this.chargeamt = chargeamt;
	}
	
	
	/**
	 * @return the deductiondt
	 */
	public String getDeductiondt() {
		return deductiondt;
	}
	
	
	/**
	 * @param deductiondt the deductiondt to set
	 */
	public void setDeductiondt(String deductiondt) {
		this.deductiondt = deductiondt;
	}
	
	
	/**
	 * @return the chargests
	 */
	public String getChargests() {
		return chargests;
	}
	
	
	/**
	 * @param chargests the chargests to set
	 */
	public void setChargests(String chargests) {
		this.chargests = chargests;
	}


/**
   * @return the usagesum
   */
  public String getUsagesum() {
    return usagesum;
  }


  /**
   * @param usagesum the usagesum to set
   */
  public void setUsagesum(String usagesum) {
    this.usagesum = usagesum;
  }


  /**
   * @return the overallusage
   */
  public String getOverallusage() {
    return overallusage;
  }


  /**
   * @param overallusage the overallusage to set
   */
  public void setOverallusage(String overallusage) {
    this.overallusage = overallusage;
  }


  public String getParentextncnt() {
    return parentextncnt;
  }


  public void setParentextncnt(String parentextncnt) {
    this.parentextncnt = parentextncnt;
  }

  private GmLoanerExtVO[] gmLoanerExtVO;



  /**
   * @return the gmLoanerExtVO
   */
  public GmLoanerExtVO[] getGmLoanerExtVO() {
    return gmLoanerExtVO;
  }


  /**
   * @param gmLoanerExtVO the gmLoanerExtVO to set
   */
  public void setGmLoanerExtVO(GmLoanerExtVO[] gmLoanerExtVO) {
    this.gmLoanerExtVO = gmLoanerExtVO;
  }


  /**
   * @return the txt_logreason
   */
  public String getTxt_logreason() {
    return txt_logreason;
  }


  /**
   * @param txt_logreason the txt_logreason to set
   */
  public void setTxt_logreason(String txt_logreason) {
    this.txt_logreason = txt_logreason;
  }

  /**
   * @return the consignid
   */
  public String getConsignid() {
    return consignid;
  }


  /**
   * @param consignid the consignid to set
   */
  public void setConsignid(String consignid) {
    this.consignid = consignid;
  }


  /**
   * @return the extwholereq
   */
  public String getExtwholereq() {
    return extwholereq;
  }


  /**
   * @param extwholereq the extwholereq to set
   */
  public void setExtwholereq(String extwholereq) {
    this.extwholereq = extwholereq;
  }


  /**
   * @return the newextndate
   */
  public String getNewextndate() {
    return newextndate;
  }


  /**
   * @param newextndate the newextndate to set
   */
  public void setNewextndate(String newextndate) {
    this.newextndate = newextndate;
  }


  /**
   * @return the pendingappr
   */
  public String getPendingappr() {
    return pendingappr;
  }


  /**
   * @param pendingappr the pendingappr to set
   */
  public void setPendingappr(String pendingappr) {
    this.pendingappr = pendingappr;
  }


  /**
   * @return the extcount
   */
  public String getExtcount() {
    return extcount;
  }


  /**
   * @param extcount the extcount to set
   */
  public void setExtcount(String extcount) {
    this.extcount = extcount;
  }


  /**
   * @return the reqdtlid
   */
  public String getReqdtlid() {
    return reqdtlid;
  }


  /**
   * @param reqdtlid the reqdtlid to set
   */
  public void setReqdtlid(String reqdtlid) {
    this.reqdtlid = reqdtlid;
  }


  public String getTrackurl() {
    return trackurl;
  }


  public void setTrackurl(String trackurl) {
    this.trackurl = trackurl;
  }


  /**
   * @return the returndate
   */
  public String getReturndate() {
    return returndate;
  }


  /**
   * @param returndate the returndate to set
   */
  public void setReturndate(String returndate) {
    this.returndate = returndate;
  }


  /**
   * @return the businessdays
   */
  public String getBusinessdays() {
    return businessdays;
  }


  /**
   * @param businessdays the businessdays to set
   */
  public void setBusinessdays(String businessdays) {
    this.businessdays = businessdays;
  }


  /**
   * @return the usage
   */
  public String getUsage() {
    return usage;
  }


  /**
   * @param usage the usage to set
   */
  public void setUsage(String usage) {
    this.usage = usage;
  }



  /**
   * @return the distid
   */
  public String getDistid() {
    return distid;
  }


  /**
   * @param distid the distid to set
   */
  public void setDistid(String distid) {
    this.distid = distid;
  }



  /**
   * @return the loandate
   */
  public String getLoandate() {
    return loandate;
  }


  /**
   * @param loandate the loandate to set
   */
  public void setLoandate(String loandate) {
    this.loandate = loandate;
  }



  /**
   * @return the requestid
   */
  public String getRequestid() {
    return requestid;
  }


  /**
   * @param requestid the requestid to set
   */
  public void setRequestid(String requestid) {
    this.requestid = requestid;
  }



  /**
   * @return the month
   */
  public String getMonth() {
    return month;
  }


  /**
   * @param month the month to set
   */
  public void setMonth(String month) {
    this.month = month;
  }



  /**
   * @return the firstdate
   */
  public String getFirstdate() {
    return firstdate;
  }


  /**
   * @param firstdate the firstdate to set
   */
  public void setFirstdate(String firstdate) {
    this.firstdate = firstdate;
  }


  /**
   * @return the lastdate
   */
  public String getLastdate() {
    return lastdate;
  }


  /**
   * @param lastdate the lastdate to set
   */
  public void setLastdate(String lastdate) {
    this.lastdate = lastdate;
  }


  /**
   * @return the source
   */
  public String getSource() {
    return source;
  }


  /**
   * @param source the source to set
   */
  public void setSource(String source) {
    this.source = source;
  }

  @Transient
  public Properties getLoanerDrilldownProperties() {
    Properties props = new Properties();
    props = getSalesDashDrilldownProperties();
    props.put("ASSOCREPNAME", "ASSOCREPNM");
    props.put("SURGDTFMT", "SURGERYDT");
    props.put("LDATEFMT", "LOANEDON");
    props.put("ETCHID", "ETCHID");
    props.put("TAGID", "TAGID");
    props.put("EXPRETDATEFMT", "EXPRETURNDT");
    props.put("DAYSELLAPSED", "DAYSELPSD");
    props.put("DAYSOVERDUE", "DAYSDUE");
    props.put("TRACK", "TRACKINGNUM");
    props.put("PNUM", "PARTNUM");
    props.put("PDESC", "PARTDESC");
    props.put("FLDSLS", "FIELDSALESNM");
    props.put("QTYMISS", "MISSQTY");
    props.put("REQID", "TXNID");
    props.put("SETID", "SETID");
    props.put("TRACKURL", "TRACKURL");
    props.put("PENDINGAPPR", "PENDINGAPPR");
    props.put("EXTCOUNT", "EXTCOUNT");
    props.put("REQDTLID", "REQDTLID");
    props.put("NEWEXTNDATE", "NEWEXTNDATE");
    props.put("PARENTEXTNCNT", "PARENTEXTNCNT");
    props.put("CONSIGNID", "CONSIGNID");
    props.put("ACTRETURNDT", "ACTRETURNDT");
    props.put("INCIDENTDT", "INCIDENTDT");
    props.put("CHARGEAMT", "CHARGEAMT");
    props.put("DEDUCTIONDT", "DEDUCTIONDT");
    props.put("CHARGESTS", "CHARGESTS");
    
    return props;
  }

  @Transient
  public Properties getLoanerUsageProperties() {
    Properties properties = new Properties();
    properties.put("SETID", "SETID");
    properties.put("SETNAME", "SETNM");
    properties.put("DISTNAME", "FIELDSALESNM");
    properties.put("LOANDATE", "LOANDATE");
    properties.put("RETURNDATE", "RETURNDATE");
    properties.put("BUS_DAYS", "BUSINESSDAYS");
    properties.put("USAGE", "USAGE");
    properties.put("DISTID", "DISTID");
    properties.put("REQID", "REQUESTID");
    properties.put("DISTID", "DISTID");
    properties.put("ASSOCREPNM", "ASSOCREPNM");
    properties.put("REPNM", "REPNM");
    properties.put("OVERALLUSAGE", "OVERALLUSAGE");
    properties.put("USAGESUM", "USAGESUM");
    properties.put("MONTH", "MONTH");
    properties.put("FIRSTDATE", "FIRSTDATE");
    properties.put("LASTDATE", "LASTDATE");

    return properties;
  }

  /**
   * @return the assocrepnm
   */
  public String getAssocrepnm() {
    return assocrepnm;
  }


  /**
   * @param assocrepnm the assocrepnm to set
   */
  public void setAssocrepnm(String assocrepnm) {
    this.assocrepnm = assocrepnm;
  }


  /**
   * @return the surgerydt
   */
  public String getSurgerydt() {
    return surgerydt;
  }


  /**
   * @param surgerydt the surgerydt to set
   */
  public void setSurgerydt(String surgerydt) {
    this.surgerydt = surgerydt;
  }


  /**
   * @return the loanedon
   */
  public String getLoanedon() {
    return loanedon;
  }


  /**
   * @param loanedon the loanedon to set
   */
  public void setLoanedon(String loanedon) {
    this.loanedon = loanedon;
  }


  /**
   * @return the etchid
   */
  public String getEtchid() {
    return etchid;
  }


  /**
   * @param etchid the etchid to set
   */
  public void setEtchid(String etchid) {
    this.etchid = etchid;
  }


  /**
   * @return the tagid
   */
  public String getTagid() {
    return tagid;
  }


  /**
   * @param tagid the tagid to set
   */
  public void setTagid(String tagid) {
    this.tagid = tagid;
  }


  /**
   * @return the expreturndt
   */
  public String getExpreturndt() {
    return expreturndt;
  }


  /**
   * @param expreturndt the expreturndt to set
   */
  public void setExpreturndt(String expreturndt) {
    this.expreturndt = expreturndt;
  }


  /**
   * @return the dayselpsd
   */
  public String getDayselpsd() {
    return dayselpsd;
  }


  /**
   * @param dayselpsd the dayselpsd to set
   */
  public void setDayselpsd(String dayselpsd) {
    this.dayselpsd = dayselpsd;
  }


  /**
   * @return the daysdue
   */
  public String getDaysdue() {
    return daysdue;
  }


  /**
   * @param daysdue the daysdue to set
   */
  public void setDaysdue(String daysdue) {
    this.daysdue = daysdue;
  }


  /**
   * @return the trackingnum
   */
  public String getTrackingnum() {
    return trackingnum;
  }


  /**
   * @param trackingnum the trackingnum to set
   */
  public void setTrackingnum(String trackingnum) {
    this.trackingnum = trackingnum;
  }


  /**
   * @return the partnum
   */
  public String getPartnum() {
    return partnum;
  }


  /**
   * @param partnum the partnum to set
   */
  public void setPartnum(String partnum) {
    this.partnum = partnum;
  }


  /**
   * @return the partdesc
   */
  public String getPartdesc() {
    return partdesc;
  }


  /**
   * @param partdesc the partdesc to set
   */
  public void setPartdesc(String partdesc) {
    this.partdesc = partdesc;
  }


  /**
   * @return the fieldsalesnm
   */
  public String getFieldsalesnm() {
    return fieldsalesnm;
  }


  /**
   * @param fieldsalesnm the fieldsalesnm to set
   */
  public void setFieldsalesnm(String fieldsalesnm) {
    this.fieldsalesnm = fieldsalesnm;
  }


  /**
   * @return the missqty
   */
  public String getMissqty() {
    return missqty;
  }


  /**
   * @param missqty the missqty to set
   */
  public void setMissqty(String missqty) {
    this.missqty = missqty;
  }



}
