package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSetInvRptVO extends GmDataStoreVO{

	private String setid = "";
	private String setnm = "";
	private String type = "";
	private String distnm = "";
	private String repnm = "";
	private String repphone = "";
	private String acctnm = "";
	private String tag = "";
	private String etchid = "";
	private String status = "";
	private String loaneddt = "";
	private String exprtrndt = "";
	
	@Transient
	public Properties getSetInvRptProperties(){
		Properties props = new Properties();
		props.put("SETID","SETID");
		props.put("SETNM","SETNM");
		props.put("TYPENM","TYPE");
		props.put("DISTNM","DISTNM");
		props.put("REPNM","REPNM");
		props.put("REPPHONE","REPPHONE");
		props.put("ACCTNM","ACCTNM");
		props.put("TAGID","TAG");
		props.put("ETCH_ID","ETCHID");
		props.put("STATUS","STATUS");
		props.put("LOANED_ON","LOANEDDT");
		props.put("EXRETNDT","EXPRTRNDT");
		return props;
	}
	
	public String getSetid() {
		return setid;
	}
	public void setSetid(String setid) {
		this.setid = setid;
	}
	public String getSetnm() {
		return setnm;
	}
	public void setSetnm(String setnm) {
		this.setnm = setnm;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDistnm() {
		return distnm;
	}
	public void setDistnm(String distnm) {
		this.distnm = distnm;
	}
	public String getRepnm() {
		return repnm;
	}
	public void setRepnm(String repnm) {
		this.repnm = repnm;
	}
	
	public String getRepphone() {
		return repphone;
	}

	public void setRepphone(String repphone) {
		this.repphone = repphone;
	}

	public String getAcctnm() {
		return acctnm;
	}
	public void setAcctnm(String acctnm) {
		this.acctnm = acctnm;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getEtchid() {
		return etchid;
	}
	public void setEtchid(String etchid) {
		this.etchid = etchid;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLoaneddt() {
		return loaneddt;
	}
	public void setLoaneddt(String loaneddt) {
		this.loaneddt = loaneddt;
	}
	public String getExprtrndt() {
		return exprtrndt;
	}
	public void setExprtrndt(String exprtrndt) {
		this.exprtrndt = exprtrndt;
	}

}
