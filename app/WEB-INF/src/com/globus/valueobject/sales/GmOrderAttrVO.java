package com.globus.valueobject.sales;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmOrderAttrVO extends GmDataStoreVO{
	
	private String ordattrid = "";
	private String orderid = "";
	private String attrtype = "";
	private String attrvalue = "";
	private String voidfl = "";
	private String orderDtls = "";
	/**
	 * @return the ordattrid
	 */
	public String getOrdattrid() {
		return ordattrid;
	}
	/**
	 * @param ordattrid the ordattrid to set
	 */
	public void setOrdattrid(String ordattrid) {
		this.ordattrid = ordattrid;
	}
	/**
	 * @return the orderid
	 */
	public String getOrderid() {
		return orderid;
	}
	/**
	 * @param orderid the orderid to set
	 */
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	/**
	 * @return the attrtype
	 */
	public String getAttrtype() {
		return attrtype;
	}
	/**
	 * @param attrtype the attrtype to set
	 */
	public void setAttrtype(String attrtype) {
		this.attrtype = attrtype;
	}
	/**
	 * @return the attrvalue
	 */
	public String getAttrvalue() {
		return attrvalue;
	}
	/**
	 * @param attrvalue the attrvalue to set
	 */
	public void setAttrvalue(String attrvalue) {
		this.attrvalue = attrvalue;
	}
	/**
	 * @return the voidfl
	 */
	public String getVoidfl() {
		return voidfl;
	}
	/**
	 * @param voidfl the voidfl to set
	 */
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	
	
	
	
	/**
	 * @return the orderDtls
	 */
	public String getOrderDtls() {
		return orderDtls;
	}
	/**
	 * @param orderDtls the orderDtls to set
	 */
	public void setOrderDtls(String orderDtls) {
		this.orderDtls = orderDtls;
	}
	
	public static Map getOrderAttrProperties(){
		Map hmLinkedDbInputStr = new LinkedHashMap();  
		
		hmLinkedDbInputStr.put("ORDERID", "getOrderid");
		hmLinkedDbInputStr.put("ATTRTYPE", "getAttrtype");
		hmLinkedDbInputStr.put("ATTRVALUE", "getAttrvalue");
		return hmLinkedDbInputStr;
	}
	
	
}