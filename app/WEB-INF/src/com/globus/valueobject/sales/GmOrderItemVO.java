package com.globus.valueobject.sales;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmOrderItemVO extends GmDataStoreVO {

	private String itemordid = "";
	private String orderid = "";
	private String pnum = "";
	private String qty = "";
	private String price = "";
	private String cnum = "";
	private String type = "";
	private String attrid = "";
	private String voidfl = "";
	private String parttype = "";
	/**
	 * @return the itemordid
	 */
	public String getItemordid() {
		return itemordid;
	}
	/**
	 * @param itemordid the itemordid to set
	 */
	public void setItemordid(String itemordid) {
		this.itemordid = itemordid;
	}
	/**
	 * @return the orderid
	 */
	public String getOrderid() {
		return orderid;
	}
	/**
	 * @param orderid the orderid to set
	 */
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	/**
	 * @return the pnum
	 */
	public String getPnum() {
		return pnum;
	}
	/**
	 * @param pnum the pnum to set
	 */
	public void setPnum(String pnum) {
		this.pnum = pnum;
	}
	/**
	 * @return the qty
	 */
	public String getQty() {
		return qty;
	}
	/**
	 * @param qty the qty to set
	 */
	public void setQty(String qty) {
		this.qty = qty;
	}
	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * @return the cnum
	 */
	public String getCnum() {
		return cnum;
	}
	/**
	 * @param cnum the cnum to set
	 */
	public void setCnum(String cnum) {
		this.cnum = cnum;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the attrid
	 */
	public String getAttrid() {
		return attrid;
	}
	/**
	 * @param attrid the attrid to set
	 */
	public void setAttrid(String attrid) {
		this.attrid = attrid;
	}
	/**
	 * @return the voidfl
	 */
	public String getVoidfl() {
		return voidfl;
	}
	/**
	 * @param voidfl the voidfl to set
	 */
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	
	/**
	 * @return the parttype
	 */
	public String getParttype() {
		return parttype;
	}
	/**
	 * @param parttype the parttype to set
	 */
	public void setParttype(String parttype) {
		this.parttype = parttype;
	}
	
	
	public static Map getOrderItemProperties(){
		Map hmLinkedDbInputStr = new LinkedHashMap();  
		
		hmLinkedDbInputStr.put("PNUM", "getPnum");
		hmLinkedDbInputStr.put("QTY", "getQty");
		hmLinkedDbInputStr.put("PRICE", "getPrice");
		hmLinkedDbInputStr.put("TYPE", "getType");
		hmLinkedDbInputStr.put("PARTTYPE", "getParttype");
		return hmLinkedDbInputStr;
	}
	
}
