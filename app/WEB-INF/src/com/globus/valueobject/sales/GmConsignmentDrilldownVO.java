package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmConsignmentDrilldownVO extends GmSalesDashDrilldownVO{
	
	private String tagid = "" ;
	private String shipdt = "" ;
	private String trackingnum = "" ;
	private String fieldsalesnm = "" ;
	private String partnumber = "";
	private String partdesc="";
	private String partqty="";
	private String parentrqtid="";
	
	
	public String getParentrqtid() {
		return parentrqtid;
	}
	public void setParentrqtid(String parentrqtid) {
		this.parentrqtid = parentrqtid;
	}
	public String getTagid() {
		return tagid;
	}
	public void setTagid(String tagid) {
		this.tagid = tagid;
	}
	public String getShipdt() {
		return shipdt;
	}
	public void setShipdt(String shipdt) {
		this.shipdt = shipdt;
	}
	public String getTrackingnum() {
		return trackingnum;
	}
	public void setTrackingnum(String trackingnum) {
		this.trackingnum = trackingnum;
	}
	
	public String getFieldsalesnm() {
		return fieldsalesnm;
	}
	public void setFieldsalesnm(String fieldsalesnm) {
		this.fieldsalesnm = fieldsalesnm;
	}
	
	public String getPartnumber() {
		return partnumber;
	}
	public void setPartnumber(String partnumber) {
		this.partnumber = partnumber;
	}
	public String getPartdesc() {
		return partdesc;
	}
	public void setPartdesc(String partdesc) {
		this.partdesc = partdesc;
	}
	public String getPartqty() {
		return partqty;
	}
	public void setPartqty(String partqty) {
		this.partqty = partqty;
	}
	
	@Transient
	public Properties getConsignSetsDrilldownProperties(){
		Properties props = new Properties();
		props.put("TAGNUM","TAGID");
		props.put("TAGID","TAGID");
		props.put("TRACK","TRACKINGNUM");
		props.put("SHPDT","SHIPDT");
		props.put("REQID","TXNID");
		props.put("SLSREP","REPNM");
		props.put("STATUS","STATUS");
		props.put("SETID","SETID");
		props.put("NAME","SETNM");
		props.put("FLDSLS","FIELDSALESNM");
		props.put("ACCNAME","ACCOUNTNM");
		return props;
	}
	
	@Transient
	public Properties getConsignSetsReqDrilldownProperties(){
		Properties props = new Properties();
		props.put("TAGNUM","TAGID");
		props.put("TAGID","TAGID");
		props.put("TRACK","TRACKINGNUM");
		props.put("SHPDT","SHIPDT");
		props.put("REQID","PARENTRQTID");
		props.put("LNREQDTLID","TXNID");
		props.put("SLSREP","REPNM");
		props.put("STATUS","STATUS");
		props.put("SETID","SETID");
		props.put("NAME","SETNM");
		props.put("FLDSLS","FIELDSALESNM");
		props.put("ACCNAME","ACCOUNTNM");
		return props;
	}
	
	@Transient
	public Properties getConsignItemsDrilldownProperties(){
		Properties props = new Properties();
		props = getSalesDashDrilldownProperties();
		props.put("TAGID","TAGID");
		props.put("REQID","TXNID");
		props.put("REPNM","REPNM");
		props.put("STATUS","STATUS");
		props.put("PNUM","PARTNUMBER");
		props.put("PDESC","PARTDESC");
		props.put("QTY","PARTQTY");
		props.put("TRACK","TRACKINGNUM");
		props.put("SDATE","SHIPDT");
		props.put("REQTO","FIELDSALESNM");
		return props;
	}
	
	@Transient
	public Properties getConsignItemsReqDrilldownProperties(){
		Properties props = new Properties();
		props = getSalesDashDrilldownProperties();
		props.put("TAGID","TAGID");
		props.put("REQID","PARENTRQTID");
		props.put("LNREQDTLID","TXNID");
		props.put("REPNM","REPNM");
		props.put("STATUS","STATUS");
		props.put("PNUM","PARTNUMBER");
		props.put("PDESC","PARTDESC");
		props.put("QTY","PARTQTY");
		props.put("TRACK","TRACKINGNUM");
		props.put("SDATE","SHIPDT");
		props.put("REQTO","FIELDSALESNM");
		return props;
	}
}