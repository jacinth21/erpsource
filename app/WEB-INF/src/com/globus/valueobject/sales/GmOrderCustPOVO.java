package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmOrderCustPOVO extends GmDataStoreVO {

  private String capamount = "";
  private String copay = "";
  private String copaycapaction = "";
  private String inputstring = "";
  private String msg = "";
  private String ordercomments = "";
  private String orderid = "";
  private String po = "";
  private String poamt = "";
  private String postatus = "";
  private String dostatus = "";
  
	/**
	* @return the postatus
	*/
	public String getPostatus() {
		return postatus;
	}
	/**
	 * @param postatus the postatus to set
	 */
	public void setPostatus(String postatus) {
		this.postatus = postatus;
	}
	/**
	 * @return the dostatus
	 */
	public String getDostatus() {
		return dostatus;
	}
	/**
	 * @param dostatus the dostatus to set
	 */
	public void setDostatus(String dostatus) {
		this.dostatus = dostatus;
	}
	/**
	 * @return the capamount
	 */
    public String getCapamount() {
		return capamount;
    }
    /**
	 * @param capamount the capamount to set
	 */
	public void setCapamount(String capamount) {
		this.capamount = capamount;
	}

	/**
	 * @return the copay
	 */
	public String getCopay() {
		return copay;
	}

	/**
	 * @param copay the copay to set
	 */
	public void setCopay(String copay) {
		this.copay = copay;
	}

	/**
	 * @return the copaycapaction
	 */
	public String getCopaycapaction() {
		return copaycapaction;
	}

	/**
	 * @param copaycapaction the copaycapaction to set
	 */
	public void setCopaycapaction(String copaycapaction) {
		this.copaycapaction = copaycapaction;
	}

	/**
	 * @return the inputstring
	 */
	public String getInputstring() {
		return inputstring;
	}

	/**
	 * @param inputstring the inputstring to set
	 */
	public void setInputstring(String inputstring) {
		this.inputstring = inputstring;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return the ordercomments
	 */
	public String getOrdercomments() {
		return ordercomments;
	}

	/**
	 * @param ordercomments the ordercomments to set
	 */
	public void setOrdercomments(String ordercomments) {
		this.ordercomments = ordercomments;
	}

	/**
	 * @return the orderid
	 */
	public String getOrderid() {
		return orderid;
	}

	/**
	 * @param orderid the orderid to set
	 */
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	/**
	 * @return the po
	 */
	public String getPo() {
		return po;
	}

	/**
	 * @param po the po to set
	 */
	public void setPo(String po) {
		this.po = po;
	}

	/**
	 * @return the poamt
	 */
	public String getPoamt() {
		return poamt;
	}

	/**
	 * @param poamt the poamt to set
	 */
	public void setPoamt(String poamt) {
		this.poamt = poamt;
	}


  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("CAPAMOUNT", "CAPAMOUNT");
    properties.put("COPAY", "COPAY");
    properties.put("COPAYCAPACTION", "COPAYCAPACTION");
    properties.put("INPUTSTRING", "INPUTSTRING");
    properties.put("MSG", "MSG");
    properties.put("ORDERCOMMENTS", "ORDERCOMMENTS");
    properties.put("ORDERID", "ORDERID");
    properties.put("PO", "PO");
    properties.put("POAMT", "POAMT");
    properties.put("POSTATUS", "POSTATUS");
    properties.put("DOSTATUS", "DOSTATUS");
    return properties;
  }

}
