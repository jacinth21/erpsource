package com.globus.valueobject.sales;

import java.beans.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;
import com.globus.valueobject.sales.GmReceiverEmailVO;
import com.globus.valueobject.sales.GmCompanyVO;



@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmInvSetReqBorrowEmailVO extends GmDataStoreVO implements Serializable {

  private String requestorrepid = "";
  private String emailsubject = "";
  private String requestorrepnm = "";
  private String strReturnMessage = "";

  private GmReceiverEmailVO[] arrReceiverEmailVO;

	/**
	 * @return the requestorrepid
	 */
	public String getRequestorrepid() {
		return requestorrepid;
	}
	
	/**
	 * @param requestorrepid the requestorrepid to set
	 */
	public void setRequestorrepid(String requestorrepid) {
		this.requestorrepid = requestorrepid;
	}
	
	/**
	 * @return the emailsubject
	 */
	public String getEmailsubject() {
		return emailsubject;
	}
	
	/**
	 * @param emailsubject the emailsubject to set
	 */
	public void setEmailsubject(String emailsubject) {
		this.emailsubject = emailsubject;
	}
	
	/**
	 * @return the requestorrepnm
	 */
	public String getRequestorrepnm() {
		return requestorrepnm;
	}
	
	/**
	 * @param requestorrepnm the requestorrepnm to set
	 */
	public void setRequestorrepnm(String requestorrepnm) {
		this.requestorrepnm = requestorrepnm;
	}
	
	
	/**
	 * @return the strReturnMessage
	 */
	public String getStrReturnMessage() {
		return strReturnMessage;
	}

	/**
	 * @param strReturnMessage the strReturnMessage to set
	 */
	public void setStrReturnMessage(String strReturnMessage) {
		this.strReturnMessage = strReturnMessage;
	}

	/**
	 * @return the arrReceiverEmailVO
	 */
	public GmReceiverEmailVO[] getArrReceiverEmailVO() {
		return arrReceiverEmailVO;
	}
	
	/**
	 * @param arrReceiverEmailVO the arrReceiverEmailVO to set
	 */
	public void setArrReceiverEmailVO(GmReceiverEmailVO[] arrReceiverEmailVO) {
		this.arrReceiverEmailVO = arrReceiverEmailVO;
	} 

}
