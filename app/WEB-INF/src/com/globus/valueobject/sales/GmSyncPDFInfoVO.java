package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSyncPDFInfoVO extends GmDataStoreVO {
	private String doid = "";
	private String pdfname = "";
	private String pdffl = "";

	/**
	 * @return the doid
	 */
	public String getDoid() {
		return doid;
	}

	/**
	 * @param doid
	 *            the doid to set
	 */
	public void setDoid(String doid) {
		this.doid = doid;
	}

	/**
	 * @return the pdfname
	 */
	public String getPdfname() {
		return pdfname;
	}

	/**
	 * @param pdfname
	 *            the pdfname to set
	 */
	public void setPdfname(String pdfname) {
		this.pdfname = pdfname;
	}

	/**
	 * @return the pdffl
	 */
	public String getPdffl() {
		return pdffl;
	}

	/**
	 * @param pdffl
	 * the pdffl to set
	 */
	public void setPdffl(String pdffl) {
		this.pdffl = pdffl;
	}
	
	@Transient
	public Properties  getPDFSyncProperties()
	{
		Properties properties = new Properties();
		properties.put("DOID", "DOID");
		properties.put("PDFNAME", "PDFNAME");
		properties.put("PDFFL", "PDFFL");	
		
		return properties;
	}

}
