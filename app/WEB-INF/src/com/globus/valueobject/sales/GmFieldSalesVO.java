package com.globus.valueobject.sales;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmFieldSalesVO extends GmSlsRegionVO{
	private String fieldsalesid ;
	private String fieldsalesnm = "";
	/**
	 * @return the fieldsalesid
	 */
	public String getFieldsalesid() {
		return fieldsalesid;
	}
	/**
	 * @param fieldsalesid the fieldsalesid to set
	 */
	public void setFieldsalesid(String fieldsalesid) {
		this.fieldsalesid = fieldsalesid;
	}
	/**
	 * @return the fieldsalesnm
	 */
	public String getFieldsalesnm() {
		return fieldsalesnm;
	}
	/**
	 * @param fieldsalesnm the fieldsalesnm to set
	 */
	public void setFieldsalesnm(String fieldsalesnm) {
		this.fieldsalesnm = fieldsalesnm;
	}
	
	@Transient
	public Properties getFieldSalesProperties(){
		Properties props = new Properties();
		props.put("ID","FIELDSALESID");
		props.put("NM","FIELDSALESNM");
		props.put("COMPID","COMPANYID");
		props.put("PID","REGIONID");
		
		
		return props;
	}
}
