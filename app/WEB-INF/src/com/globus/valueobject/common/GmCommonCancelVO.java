package com.globus.valueobject.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCommonCancelVO extends GmDataStoreVO{

	private String cancelreasonid = "";
	private String addnlinfo = "";
	private String cancelgrp = "";
	private String comments = "";  
	private String txnids = "";
	
	
	public String getCancelreasonid() {
		return cancelreasonid;
	}
	public void setCancelreasonid(String cancelreasonid) {
		this.cancelreasonid = cancelreasonid;
	}
	public String getAddnlinfo() {
		return addnlinfo;
	}
	public void setAddnlinfo(String addnlinfo) {
		this.addnlinfo = addnlinfo;
	}
	public String getCancelgrp() {
		return cancelgrp;
	}
	public void setCancelgrp(String cancelgrp) {
		this.cancelgrp = cancelgrp;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getTxnids() {
		return txnids;
	}
	public void setTxnids(String txnids) {
		this.txnids = txnids;
	}
		
}
