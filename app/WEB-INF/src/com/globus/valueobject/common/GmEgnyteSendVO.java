package com.globus.valueobject.common;

import  java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@JsonIgnoreProperties
public class GmEgnyteSendVO {
	
	private String recipients ="";
	private String message ="";
	private String expiry_date ="";//YYYY-MM-DD
	//public String add_filename ="";
	
	private String path ="";
	private String type ="file";
	private String accessibility ="anyone";
	private boolean  notify =false ;
	private boolean link_to_current =true ;
	//Expiry Date and Clicks cannot be used at the same time in VO
	//private int expiry_clicks ;//1-10
	private String creation_date ="";
	private boolean send_email =false;

	private boolean copy_me ;

	/**
	 * @return the recipients
	 */
	public String getRecipients() {
		return recipients;
	}

	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the expiry_date
	 */
	public String getExpiry_date() {
		return expiry_date;
	}

	/**
	 * @param expiryDate the expiry_date to set
	 */
	public void setExpiry_date(String expiryDate) {
		expiry_date = expiryDate;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the accessibility
	 */
	public String getAccessibility() {
		return accessibility;
	}

	/**
	 * @param accessibility the accessibility to set
	 */
	public void setAccessibility(String accessibility) {
		this.accessibility = accessibility;
	}

	/**
	 * @return the notify
	 */
	public boolean isNotify() {
		return notify;
	}

	/**
	 * @param notify the notify to set
	 */
	public void setNotify(boolean notify) {
		this.notify = notify;
	}

	/**
	 * @return the link_to_current
	 */
	public boolean isLink_to_current() {
		return link_to_current;
	}

	/**
	 * @param linkToCurrent the link_to_current to set
	 */
	public void setLink_to_current(boolean linkToCurrent) {
		link_to_current = linkToCurrent;
	}

	/**
	 * @return the expiry_clicks
	 
	public int getExpiry_clicks() {
		return expiry_clicks;
	}*/

	/**
	 * @param expiryClicks the expiry_clicks to set
	 
	public void setExpiry_clicks(int expiryClicks) {
		expiry_clicks = expiryClicks;
	}*/

	/**
	 * @return the creation_date
	 */
	public String getCreation_date() {
		return creation_date;
	}

	/**
	 * @param creationDate the creation_date to set
	 */
	public void setCreation_date(String creationDate) {
		creation_date = creationDate;
	}

	/**
	 * @return the send_email
	 */
	public boolean isSend_email() {
		return send_email;
	}

	/**
	 * @param sendEmail the send_email to set
	 */
	public void setSend_email(boolean sendEmail) {
		send_email = sendEmail;
	}

	/**
	 * @return the copy_me
	 */
	public boolean isCopy_me() {
		return copy_me;
	}

	/**
	 * @param copyMe the copy_me to set
	 */
	public void setCopy_me(boolean copyMe) {
		copy_me = copyMe;
	}

	public String toString(){
		
		String objToString = "recipients:"+recipients+"   "+"message:"+message+"   "+"expiry_date:"+expiry_date+"   "+"path:"+path+"   "+"type:"+type+"   "+
		"accessibility:"+accessibility+"   "+"notify:"+notify+"   "+"link_to_current:"+link_to_current+"   "+"creation_date:"+creation_date
		+"   "+"send_email:"+send_email+"   "+"copy_me:"+copy_me;
		return objToString;
		
	}
}
