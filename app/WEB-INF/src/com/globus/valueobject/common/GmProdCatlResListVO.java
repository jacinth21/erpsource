package com.globus.valueobject.common;

import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.logon.GmSecurityEventVO;
import com.globus.valueobject.prodmgmnt.GmDataUpdateVO;
import com.globus.valueobject.prodmgmnt.GmFileVO;
import com.globus.valueobject.prodmgmnt.GmGroupPartVO;
import com.globus.valueobject.prodmgmnt.GmGroupVO;
import com.globus.valueobject.prodmgmnt.GmKeywordRefVO;
import com.globus.valueobject.prodmgmnt.GmPartAttributeVO;
import com.globus.valueobject.prodmgmnt.GmPartNumberVO;
import com.globus.valueobject.prodmgmnt.GmPartRFSVO;
import com.globus.valueobject.prodmgmnt.GmSetAttributeVO;
import com.globus.valueobject.prodmgmnt.GmSetMasterVO;
import com.globus.valueobject.prodmgmnt.GmSetPartVO;
import com.globus.valueobject.prodmgmnt.GmSystemAttributeVO;
import com.globus.valueobject.prodmgmnt.GmSystemVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmProdCatlResListVO extends GmProdCatlReqVO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmCodeLookupVO> listGmCodeLookupVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmPartNumberVO> listGmPartNumberVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmSystemVO> listGmSystemVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmSetMasterVO> listGmSetMasterVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmSetPartVO> listGmSetPartVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmGroupVO> listGmGroupVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmGroupPartVO> listGmGroupPartVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmDataUpdateVO> listGmDataUpdateVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmSystemAttributeVO> listGmSystemAttributeVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmFileVO> listGmFileVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmKeywordRefVO> listGmKeywordRefVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmRulesVO> listGmRulesVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmPartyVO> listGmPartyVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmPartyContactVO> listGmPartyContactVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmDeviceDetailVO> listAutoUpdates = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmSetAttributeVO> listGmSetAttributeVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmPartAttributeVO> listGmPartAttributeVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmCodeLookupExclusionVO> listGmCodeLookupExclusionVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmPartRFSVO> listGmRFSpartVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmCodeLookupAccessVO> listGmCodeLkpAccessVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmSecurityEventVO> listGmSecurityEventVO = null;
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private List<GmTagDetailVO> listGmTagInfoVO = null;

  public List<GmDeviceDetailVO> getListAutoUpdates() {
    return listAutoUpdates;
  }

  public void setListAutoUpdates(List<GmDeviceDetailVO> listAutoUpdates) {
    this.listAutoUpdates = listAutoUpdates;
  }

  public List<GmPartyContactVO> getListGmPartyContactVO() {
    return listGmPartyContactVO;
  }

  public void setListGmPartyContactVO(List<GmPartyContactVO> listGmPartyContactVO) {
    this.listGmPartyContactVO = listGmPartyContactVO;
  }

  public List<GmPartyVO> getListGmPartyVO() {
    return listGmPartyVO;
  }

  public void setListGmPartyVO(List<GmPartyVO> listGmPartyVO) {
    this.listGmPartyVO = listGmPartyVO;
  }

  public List<GmCodeLookupVO> getListGmCodeLookupVO() {
    return listGmCodeLookupVO;
  }

  public void setListGmCodeLookupVO(List<GmCodeLookupVO> listGmCodeLookupVO) {
    this.listGmCodeLookupVO = listGmCodeLookupVO;
  }

  public List<GmKeywordRefVO> getListGmKeywordRefVO() {
    return listGmKeywordRefVO;
  }

  public void setListGmKeywordRefVO(List<GmKeywordRefVO> listGmKeywordRefVO) {
    this.listGmKeywordRefVO = listGmKeywordRefVO;
  }

  public List<GmPartNumberVO> getListGmPartNumberVO() {
    return listGmPartNumberVO;
  }

  public List<GmRulesVO> getListGmRulesVO() {
    return listGmRulesVO;
  }

  public void setListGmRulesVO(List<GmRulesVO> listGmRulesVO) {
    this.listGmRulesVO = listGmRulesVO;
  }

  public void setListGmPartNumberVO(List<GmPartNumberVO> listGmPartNumberVO) {
    this.listGmPartNumberVO = listGmPartNumberVO;
  }

  public List<GmSystemVO> getListGmSystemVO() {
    return listGmSystemVO;
  }

  public void setListGmSystemVO(List<GmSystemVO> listGmSystemVO) {
    this.listGmSystemVO = listGmSystemVO;
  }

  public void setListGmSetMasterVO(List<GmSetMasterVO> listGmSetMasterVO) {
    this.listGmSetMasterVO = listGmSetMasterVO;
  }

  public List<GmSetMasterVO> getListGmSetMasterVO() {
    return listGmSetMasterVO;
  }

  public List<GmSetPartVO> getListGmSetPartVO() {
    return listGmSetPartVO;
  }

  public void setListGmSetPartVO(List<GmSetPartVO> listGmSetPartVO) {
    this.listGmSetPartVO = listGmSetPartVO;
  }

  public List<GmGroupVO> getListGmGroupVO() {
    return listGmGroupVO;
  }

  public void setListGmGroupVO(List<GmGroupVO> listGmGroupVO) {
    this.listGmGroupVO = listGmGroupVO;
  }

  public List<GmGroupPartVO> getListGmGroupPartVO() {
    return listGmGroupPartVO;
  }

  public void setListGmGroupPartVO(List<GmGroupPartVO> listGmGroupPartVO) {
    this.listGmGroupPartVO = listGmGroupPartVO;
  }

  public List<GmDataUpdateVO> getListGmDataUpdateVO() {
    return listGmDataUpdateVO;
  }

  public void setListGmDataUpdateVO(List<GmDataUpdateVO> listGmDataUpdateVO) {
    this.listGmDataUpdateVO = listGmDataUpdateVO;
  }

  public List<GmSystemAttributeVO> getListGmSystemAttributeVO() {
    return listGmSystemAttributeVO;
  }

  public void setListGmSystemAttributeVO(List<GmSystemAttributeVO> listGmSystemAttributeVO) {
    this.listGmSystemAttributeVO = listGmSystemAttributeVO;
  }

  public List<GmFileVO> getListGmFileVO() {
    return listGmFileVO;
  }

  public void setListGmFileVO(List<GmFileVO> listGmFileVO) {
    this.listGmFileVO = listGmFileVO;
  }

  public List<GmSetAttributeVO> getListGmSetAttributeVO() {
    return listGmSetAttributeVO;
  }

  public void setListGmSetAttributeVO(List<GmSetAttributeVO> listGmSetAttributeVO) {
    this.listGmSetAttributeVO = listGmSetAttributeVO;
  }

  /**
   * @return the listGmPartAttributeVO
   */
  public List<GmPartAttributeVO> getListGmPartAttributeVO() {
    return listGmPartAttributeVO;
  }

  /**
   * @param listGmPartAttributeVO the listGmPartAttributeVO to set
   */
  public void setListGmPartAttributeVO(List<GmPartAttributeVO> listGmPartAttributeVO) {
    this.listGmPartAttributeVO = listGmPartAttributeVO;
  }

  /**
   * @return the listGmCodeLookupExclusionVO
   */
  public List<GmCodeLookupExclusionVO> getListGmCodeLookupExclusionVO() {
    return listGmCodeLookupExclusionVO;
  }

  /**
   * @param listGmCodeLookupExclusionVO the listGmCodeLookupExclusionVO to set
   */
  public void setListGmCodeLookupExclusionVO(
      List<GmCodeLookupExclusionVO> listGmCodeLookupExclusionVO) {
    this.listGmCodeLookupExclusionVO = listGmCodeLookupExclusionVO;
  }

  /**
   * @return the listGmRFSpartVO
   */
  public List<GmPartRFSVO> getListGmRFSpartVO() {
    return listGmRFSpartVO;
  }

  /**
   * @param listGmRFSpartVO the listGmRFSpartVO to set
   */
  public void setListGmRFSpartVO(List<GmPartRFSVO> listGmRFSpartVO) {
    this.listGmRFSpartVO = listGmRFSpartVO;
  }

  /**
   * @return the listGmCodeLkpAccessVO
   */
  public List<GmCodeLookupAccessVO> getListGmCodeLkpAccessVO() {
    return listGmCodeLkpAccessVO;
  }

  /**
   * @param listGmCodeLkpAccessVO the listGmCodeLkpAccessVO to set
   */
  public void setListGmCodeLkpAccessVO(List<GmCodeLookupAccessVO> listGmCodeLkpAccessVO) {
    this.listGmCodeLkpAccessVO = listGmCodeLkpAccessVO;
  }

  /**
   * @return the listGmSecurityEventVO
   */
  public List<GmSecurityEventVO> getListGmSecurityEventVO() {
    return listGmSecurityEventVO;
  }

  /**
   * @param listGmSecurityEventVO the listGmSecurityEventVO to set
   */
  public void setListGmSecurityEventVO(List<GmSecurityEventVO> listGmSecurityEventVO) {
    this.listGmSecurityEventVO = listGmSecurityEventVO;
  }

public List<GmTagDetailVO> getListGmTagInfoVO() {
	return listGmTagInfoVO;
}

public void setListGmTagInfoVO(List<GmTagDetailVO> listGmTagInfoVO) {
	this.listGmTagInfoVO = listGmTagInfoVO;
}

public void populatePageProperties(List alList) {
    int alReturnSize = alList.size();
    if (alReturnSize > 0) {
      HashMap hmPageInfo = GmCommonClass.parseNullHashMap((HashMap) alList.get(0));
      this.setPageno((String) hmPageInfo.get("PAGENO"));
      this.setTotalpages((String) hmPageInfo.get("TOTALPAGES"));
      this.setTotalsize((String) hmPageInfo.get("TOTALSIZE"));
      this.setPagesize((String) hmPageInfo.get("PAGESIZE"));
    }
  }

  /**
   * @param ArrayList - result set
   * @param String - primary key name
   */
  public void populatePageProperties(List alList, String strLastRecKey) {
    int alReturnSize = alList.size();
    if (alReturnSize > 0) {
      HashMap hmPageInfo = GmCommonClass.parseNullHashMap((HashMap) alList.get(alReturnSize - 1));
      this.setPageno((String) hmPageInfo.get("PAGENO"));
      this.setTotalpages((String) hmPageInfo.get("TOTALPAGES"));
      this.setTotalsize((String) hmPageInfo.get("TOTALSIZE"));
      this.setPagesize((String) hmPageInfo.get("PAGESIZE"));
      this.setLastrec(GmCommonClass.parseNull((String) hmPageInfo.get(strLastRecKey)));
    }
  }

  public GmDataTstVO setPageProperties(List alList) {
    int alReturnSize = alList.size();
    String strAlSize = Integer.toString(alReturnSize);
    GmDataTstVO gmDataTstVO = new GmDataTstVO();
    if (alReturnSize > 0) {
      gmDataTstVO = (GmDataTstVO) alList.get(alReturnSize - 1);
      this.setPageno(gmDataTstVO.getPageno());
      this.setTotalpages(gmDataTstVO.getTotalpages());
      this.setTotalsize(gmDataTstVO.getTotalsize());
      this.setPagesize(strAlSize);
      return gmDataTstVO;
    } else {
      this.setPageno("1");
      this.setTotalpages("");
      this.setTotalsize("");
      this.setPagesize("");
      return null;
    }

  }

}
