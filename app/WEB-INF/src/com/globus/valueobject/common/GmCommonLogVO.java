package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCommonLogVO extends GmDataStoreVO {

  private String logid = "";
  private String txnid = "";
  private String txntype = "";
  private String comments = "";
  private String createddate = "";
  private String createdby = "";
  private String voidfl = "";
  private String createdbynm = "";


  public String getLogid() {
    return logid;
  }

  public void setLogid(String logid) {
    this.logid = logid;
  }

  public String getTxnid() {
    return txnid;
  }

  public void setTxnid(String txnid) {
    this.txnid = txnid;
  }

  public String getTxntype() {
    return txntype;
  }

  public void setTxntype(String txntype) {
    this.txntype = txntype;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  public String getCreateddate() {
    return createddate;
  }

  public void setCreateddate(String createddate) {
    this.createddate = createddate;
  }

  public String getCreatedby() {
    return createdby;
  }

  public void setCreatedby(String createdby) {
    this.createdby = createdby;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }



  public String getCreatedbynm() {
    return createdbynm;
  }

  public void setCreatedbynm(String createdbynm) {
    this.createdbynm = createdbynm;
  }

  @Transient
  public Properties getCommonLogProperties() {
    Properties props = new Properties();
    props.put("LOGID", "LOGID");
    props.put("ID", "TXNID");
    props.put("COMMENTTYPE", "TXNTYPE");
    props.put("COMMENTS", "COMMENTS");
    props.put("CREATEDDATE", "CREATEDDATE");
    props.put("CREATEDBY", "CREATEDBY");
    props.put("VOIDFL", "VOIDFL");

    return props;
  }
  
  @Transient
  public Properties getCommentLogProperties() {
    Properties props = new Properties();
    props.put("LOGID", "LOGID");
    props.put("ID", "TXNID");
    props.put("COMMENTTYPE", "TXNTYPE");
    props.put("COMMENTS", "COMMENTS");
    props.put("DT", "CREATEDDATE");
    props.put("UNAME", "CREATEDBY");
    props.put("UNAME", "CREATEDBYNM");

    return props;
  }
}
