package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.operations.GmTxnPartDtlVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmTagInfoVO extends GmDeviceDetailVO {

  private String tagid = "";
  private String setid = "";
  private String setnm = "";
  private String consignid = "";
  private String settype = "";
  private String settypenm = "";

  private List<GmTxnPartDtlVO> gmTagPartListVO = new ArrayList<GmTxnPartDtlVO>();



  /**
   * @return the tagid
   */
  public String getTagid() {
    return tagid;
  }

  /**
   * @param tagid the tagid to set
   */
  public void setTagid(String tagid) {
    this.tagid = tagid;
  }

  /**
   * @return the setid
   */
  public String getSetid() {
    return setid;
  }

  /**
   * @param setid the setid to set
   */
  public void setSetid(String setid) {
    this.setid = setid;
  }

  /**
   * @return the settype
   */
  public String getSettype() {
    return settype;
  }

  /**
   * @param settype the settype to set
   */
  public void setSettype(String settype) {
    this.settype = settype;
  }



  /**
   * @return the setnm
   */
  public String getSetnm() {
    return setnm;
  }

  /**
   * @param setnm the setnm to set
   */
  public void setSetnm(String setnm) {
    this.setnm = setnm;
  }

  /**
   * @return the consignid
   */
  public String getConsignid() {
    return consignid;
  }

  /**
   * @param consignid the consignid to set
   */
  public void setConsignid(String consignid) {
    this.consignid = consignid;
  }

  /**
   * @return the settypenm
   */
  public String getSettypenm() {
    return settypenm;
  }

  /**
   * @param settypenm the settypenm to set
   */
  public void setSettypenm(String settypenm) {
    this.settypenm = settypenm;
  }

  /**
   * @return the gmTagPartListVO
   */
  public List<GmTxnPartDtlVO> getGmTagPartListVO() {
    return gmTagPartListVO;
  }

  /**
   * @param gmTagPartListVO the gmTagPartListVO to set
   */
  public void setGmTagPartListVO(List<GmTxnPartDtlVO> gmTagPartListVO) {
    this.gmTagPartListVO = gmTagPartListVO;
  }


  @Transient
  public Properties getTagHeaderProperties() {
    Properties props = new Properties();

    props.put("TAGID", "TAGID");
    props.put("SETID", "SETID");
    props.put("SETNM", "SETNM");
    props.put("CONSIGNID", "CONSIGNID");
    props.put("SETTYPE", "SETTYPE");
    props.put("SETTYPENM", "SETTYPENM");
    return props;
  }



}
