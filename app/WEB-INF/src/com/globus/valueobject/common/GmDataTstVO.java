package com.globus.valueobject.common;

import java.beans.Transient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmDataTstVO extends GmDataVO {
  transient private String totalpages = "";
  transient private String totalsize = "";
  transient private String pagesize = "";
  transient private String pageno = "";

  @Transient
  public String getTotalpages() {
    return totalpages;
  }

  @Transient
  public void setTotalpages(String totalpages) {
    this.totalpages = totalpages;
  }

  @Transient
  public String getTotalsize() {
    return totalsize;
  }

  @Transient
  public void setTotalsize(String totalsize) {
    this.totalsize = totalsize;
  }

  @Transient
  public String getPagesize() {
    return pagesize;
  }

  @Transient
  public void setPagesize(String pagesize) {
    this.pagesize = pagesize;
  }

  @Transient
  public String getPageno() {
    return pageno;
  }

  @Transient
  public void setPageno(String pageno) {
    this.pageno = pageno;
  }


}
