package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmRulesVO extends GmDataStoreVO {
  private String ruleseqid = "";
  private String ruleid = "";
  private String rulegrpid = "";
  private String ruledesc = "";
  private String rulevalue = "";
  private String lastupdatedby = "";
  private String lastupdateddate = "";
  private String voidfl = "";

  public String getRuleseqid() {
    return ruleseqid;
  }

  public void setRuleseqid(String ruleseqid) {
    this.ruleseqid = ruleseqid;
  }

  public String getRuleid() {
    return ruleid;
  }

  public void setRuleid(String ruleid) {
    this.ruleid = ruleid;
  }

  public String getRulegrpid() {
    return rulegrpid;
  }

  public void setRulegrpid(String rulegrpid) {
    this.rulegrpid = rulegrpid;
  }

  public String getRuledesc() {
    return ruledesc;
  }

  public void setRuledesc(String ruledesc) {
    this.ruledesc = ruledesc;
  }

  public String getRulevalue() {
    return rulevalue;
  }

  public void setRulevalue(String rulevalue) {
    this.rulevalue = rulevalue;
  }

  public String getLastupdatedby() {
    return lastupdatedby;
  }

  public void setLastupdatedby(String lastupdatedby) {
    this.lastupdatedby = lastupdatedby;
  }

  public String getLastupdateddate() {
    return lastupdateddate;
  }

  public void setLastupdateddate(String lastupdateddate) {
    this.lastupdateddate = lastupdateddate;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  @Transient
  public Properties getRulesProperties() {
    Properties props = new Properties();
    props.put("RULESEQID", "RULESEQID");
    props.put("RULEID", "RULEID");
    props.put("RULEGRPID", "RULEGRPID");
    props.put("RULEDESC", "RULEDESC");
    props.put("RULEVALUE", "RULEVALUE");
    props.put("LASTUPDATEDBY", "LASTUPDATEDBY");
    props.put("LASTUPDATEDDATE", "LASTUPDATEDDATE");
    props.put("VOIDFL", "VOIDFL");
    props.put("CMPID", "CMPID");

    return props;
  }
}
