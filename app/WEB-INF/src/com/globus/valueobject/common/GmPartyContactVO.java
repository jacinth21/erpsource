package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPartyContactVO extends GmDataTstVO {
  private String partyid = "";
  private String contactid = "";
  private String contactval = "";
  private String contactmode = "";
  private String contacttype = "";
  private String voidfl = "";
  private String primaryfl = "";
  private String inactivefl = "";

  public String getPartyid() {
    return partyid;
  }

  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  public String getContactid() {
    return contactid;
  }

  public void setContactid(String contactid) {
    this.contactid = contactid;
  }

  public String getContactval() {
    return contactval;
  }

  public void setContactval(String contactval) {
    this.contactval = contactval;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  public String getPrimaryfl() {
    return primaryfl;
  }

  public void setPrimaryfl(String primaryfl) {
    this.primaryfl = primaryfl;
  }

  public String getContactmode() {
    return contactmode;
  }

  public void setContactmode(String contactmode) {
    this.contactmode = contactmode;
  }

  public String getContacttype() {
    return contacttype;
  }

  public void setContacttype(String contacttype) {
    this.contacttype = contacttype;
  }

  public String getInactivefl() {
    return inactivefl;
  }

  public void setInactivefl(String inactivefl) {
    this.inactivefl = inactivefl;
  }
 
  @Transient
  public Properties getSystemProperties() {
    Properties props = new Properties();
    props.put("PARTYID", "PARTYID");
    props.put("CONTACTID", "CONTACTID");
    props.put("CONTACTVAL", "CONTACTVAL");
    props.put("VOIDFL", "VOIDFL");
    props.put("PRIMARYFL", "PRIMARYFL");
    props.put("CONTACTMODE", "CONTACTMODE");
    props.put("CONTACTTYPE", "CONTACTTYPE");
    props.put("INACTIVEFL", "INACTIVEFL");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    return props;
  }


}
