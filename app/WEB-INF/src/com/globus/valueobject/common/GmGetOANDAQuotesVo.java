package com.globus.valueobject.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * @author rdinesh
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GmGetOANDAQuotesVo {

  private String base_currency;
  private String quote_currency;
  private String average_bid;
  private String average_ask;
  private String average_midpoint;

  /**
   * @return the base_currency
   */
  public String getBase_currency() {
    return base_currency;
  }

  /**
   * @return the quote_currency
   */
  public String getQuote_currency() {
    return quote_currency;
  }

  /**
   * @return the close_bid
   */
  public String getAverage_bid() {
    return average_bid;
  }

  /**
   * @return the close_ask
   */
  public String getAverage_ask() {
    return average_ask;
  }

  /**
   * @return the close_midpoint
   */
  public String getAverage_midpoint() {
    return average_midpoint;
  }



}
