package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCodeLookupAccessVO extends GmDataStoreVO {

  private String codeaccessid = "";
  private String accessid = "";
  private String codeid = "";


  /**
   * @return the codeaccessid
   */
  public String getCodeaccessid() {
    return codeaccessid;
  }


  /**
   * @param codeaccessid the codeaccessid to set
   */
  public void setCodeaccessid(String codeaccessid) {
    this.codeaccessid = codeaccessid;
  }


  /**
   * @return the accessid
   */
  public String getAccessid() {
    return accessid;
  }


  /**
   * @param accessid the accessid to set
   */
  public void setAccessid(String accessid) {
    this.accessid = accessid;
  }


  /**
   * @return the codeid
   */
  public String getCodeid() {
    return codeid;
  }


  /**
   * @param codeid the codeid to set
   */
  public void setCodeid(String codeid) {
    this.codeid = codeid;
  }

  @Transient
  public Properties getCodeLookupAccessProperties() {
    Properties props = new Properties();
    props.put("CODEACCESSID", "CODEACCESSID");
    props.put("CODEID", "CODEID");
    props.put("ACCESSID", "ACCESSID");
    props.put("CMPID", "CMPID");
    return props;
  }
}
