package com.globus.valueobject.common;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.globus.valueobject.common.GmDataVO;
import com.globus.valueobject.operations.GmTxnRulesVO;
import com.globus.valueobject.operations.shipping.GmTxnShipInfoVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmCommonListVO extends GmDataVO{
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCommonFileUploadVO> gmCommonFileUploadVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)	
	private List<GmCodeLookupVO> gmCodeLookupVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmCommonLogVO> gmCommonLogVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmTxnRulesVO> gmTxnRulesVO  = null;
	public List<GmCommonFileUploadVO> getGmCommonFileUploadVO() {
		return gmCommonFileUploadVO;
	}
	public void setGmCommonFileUploadVO(
			List<GmCommonFileUploadVO> gmCommonFileUploadVO) {
		this.gmCommonFileUploadVO = gmCommonFileUploadVO;
	}
	public List<GmCodeLookupVO> getGmCodeLookupVO() {
		return gmCodeLookupVO;
	}
	public void setGmCodeLookupVO(List<GmCodeLookupVO> gmCodeLookupVO) {
		this.gmCodeLookupVO = gmCodeLookupVO;
	}
	public List<GmCommonLogVO> getGmCommonLogVO() {
		return gmCommonLogVO;
	}
	public void setGmCommonLogVO(List<GmCommonLogVO> gmCommonLogVO) {
		this.gmCommonLogVO = gmCommonLogVO;
	}
	public List<GmTxnRulesVO> getGmTxnRulesVO() {
		return gmTxnRulesVO;
	}
	public void setGmTxnRulesVO(List<GmTxnRulesVO> gmTxnRulesVO) {
		this.gmTxnRulesVO = gmTxnRulesVO;
	}
	
	
}
