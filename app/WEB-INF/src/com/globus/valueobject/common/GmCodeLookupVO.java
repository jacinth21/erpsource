package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCodeLookupVO extends GmDataStoreVO{
	
	private String codeid = "";
	private String codenm = "";
	private String codegrp = "";
	private String activefl = "";
	private String seqno = "";
	private String codenmalt = "";
	private String controltype = "";
	private String createddate = "";
	private String createdby = "";
	private String lastupdatedby = "";
	private String lastupdateddate = "";
	private String voidfl = "";
	private String lockfl = "";
	
	/**
	 * @return the codeid
	 */
	public String getCodeid() {
		return codeid;
	}
	/**
	 * @param codeid the codeid to set
	 */
	public void setCodeid(String codeid) {
		this.codeid = codeid;
	}
	/**
	 * @return the codenm
	 */
	public String getCodenm() {
		return codenm;
	}
	/**
	 * @param codenm the codenm to set
	 */
	public void setCodenm(String codenm) {
		this.codenm = codenm;
	}
	/**
	 * @return the codegrp
	 */
	public String getCodegrp() {
		return codegrp;
	}
	/**
	 * @param codegrp the codegrp to set
	 */
	public void setCodegrp(String codegrp) {
		this.codegrp = codegrp;
	}
	/**
	 * @return the activefl
	 */
	public String getActivefl() {
		return activefl;
	}
	/**
	 * @param activefl the activefl to set
	 */
	public void setActivefl(String activefl) {
		this.activefl = activefl;
	}
	/**
	 * @return the seqno
	 */
	public String getSeqno() {
		return seqno;
	}
	/**
	 * @param seqno the seqno to set
	 */
	public void setSeqno(String seqno) {
		this.seqno = seqno;
	}
	/**
	 * @return the codenmalt
	 */
	public String getCodenmalt() {
		return codenmalt;
	}
	/**
	 * @param codenmalt the codenmalt to set
	 */
	public void setCodenmalt(String codenmalt) {
		this.codenmalt = codenmalt;
	}
	/**
	 * @return the controltype
	 */
	public String getControltype() {
		return controltype;
	}
	/**
	 * @param controltype the controltype to set
	 */
	public void setControltype(String controltype) {
		this.controltype = controltype;
	}
	/**
	 * @return the createddate
	 */
	public String getCreateddate() {
		return createddate;
	}
	/**
	 * @param createddate the createddate to set
	 */
	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}
	/**
	 * @return the createdby
	 */
	public String getCreatedby() {
		return createdby;
	}
	/**
	 * @param createdby the createdby to set
	 */
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	/**
	 * @return the lastupdatedby
	 */
	public String getLastupdatedby() {
		return lastupdatedby;
	}
	/**
	 * @param lastupdatedby the lastupdatedby to set
	 */
	public void setLastupdatedby(String lastupdatedby) {
		this.lastupdatedby = lastupdatedby;
	}
	/**
	 * @return the lastupdateddate
	 */
	public String getLastupdateddate() {
		return lastupdateddate;
	}
	/**
	 * @param lastupdateddate the lastupdateddate to set
	 */
	public void setLastupdateddate(String lastupdateddate) {
		this.lastupdateddate = lastupdateddate;
	}
	/**
	 * @return the voidfl
	 */
	public String getVoidfl() {
		return voidfl;
	}
	/**
	 * @param voidfl the voidfl to set
	 */
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}
	/**
	 * @return the lockfl
	 */
	public String getLockfl() {
		return lockfl;
	}
	/**
	 * @param lockfl the lockfl to set
	 */
	public void setLockfl(String lockfl) {
		this.lockfl = lockfl;
	}
	
	/*
	 * GmCommonClass.getCodeList 
	 */
	@Transient
	public Properties getCodeListMappingProps()
	{
		Properties props = new Properties();
		props.put("CODEID", "CODEID");
		props.put("CODENM", "CODENM");
		props.put("CODENMALT", "CODENMALT");
		props.put("CONTROLTYP", "CONTROLTYPE");
		props.put("CODESEQNO", "SEQNO");
		
		return props;
	}
	
	@Transient
	public Properties getCodeLookupProperties()
	{
		Properties props = new Properties();
		props.put("CODEID", "CODEID");
		props.put("CODENM", "CODENM");
		props.put("CODEGRP", "CODEGRP");
		props.put("ACTIVEFL", "ACTIVEFL");
		props.put("CODESEQNO", "SEQNO");
		props.put("CODENMALT", "CODENMALT");
		props.put("CONTROLTYPE", "CONTROLTYPE");
		props.put("CREATEDDATE", "CREATEDDATE");
		props.put("CREATEDBY", "CREATEDBY");
		props.put("LASTUPDATEDBY", "LASTUPDATEDBY");
		props.put("LASTUPDATEDDATE", "LASTUPDATEDDATE");
		props.put("VOIDFL", "VOIDFL");
		props.put("LOCKFL", "LOCKFL");			
		return props;
	}	
}
