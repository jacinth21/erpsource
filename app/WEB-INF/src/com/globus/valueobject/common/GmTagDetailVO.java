package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmTagDetailVO extends GmDataTstVO {
	  private String tagid = "";
	  private String setid = "";
	  private String setnm = "";
      private String tagdata=null;



	  /**
	   * @return the tagid
	   */
	  public String getTagid() {
	    return tagid;
	  }

	  /**
	   * @param tagid the tagid to set
	   */
	  public void setTagid(String tagid) {
	    this.tagid = tagid;
	  }

	  /**
	   * @return the setid
	   */
	  public String getSetid() {
	    return setid;
	  }

	  /**
	   * @param setid the setid to set
	   */
	  public void setSetid(String setid) {
	    this.setid = setid;
	  }



	  /**
	   * @return the setnm
	   */
	  public String getSetnm() {
	    return setnm;
	  }

	  /**
	   * @param setnm the setnm to set
	   */
	  public void setSetnm(String setnm) {
	    this.setnm = setnm;
	  }



	public String getTagdata() {
		return tagdata;
	}

	public void setTagdata(String tagdata) {
		this.tagdata = tagdata;
	}

	@Transient
	  public Properties getTagHeaderProperties() {
	    Properties props = new Properties();
	    props.put("TAGID", "TAGID");
	    props.put("SETID", "SETID");
	    props.put("SETNM", "SETNM");
	    props.put("TAGDATA","TAGDATA");
	    props.put("UDIPATTERN", "UDIPATTERN");
	    props.put("TOTALPAGES", "TOTALPAGES");
	    props.put("PAGENO", "PAGENO");
	    props.put("TOTALSIZE", "TOTALSIZE");
	    props.put("PAGESIZE", "PAGESIZE");
	    props.put("SYSTEMID", "SYSTEMID");
	    return props;
	  }

}
