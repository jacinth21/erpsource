package com.globus.valueobject.common.master;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDeviceDetailVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmMasterSyncReqVO extends GmDeviceDetailVO{

	private String acctid ="";
	private String reftype ="";
	private String gpoid ="";
	private String repid ="";
	private String syncall = "";
	
	public String getAcctid() {
		return acctid;
	}
	public void setAcctid(String acctid) {
		this.acctid = acctid;
	}
	public String getReftype() {
		return reftype;
	}
	public void setReftype(String reftype) {
		this.reftype = reftype;
	}
	public String getGpoid() {
		return gpoid;
	}
	public void setGpoid(String gpoid) {
		this.gpoid = gpoid;
	}
	public String getRepid() {
		return repid;
	}
	public void setRepid(String repid) {
		this.repid = repid;
	}
	/**
	 * @return the syncall
	 */
	public String getSyncall() {
		return syncall;
	}
	/**
	 * @param syncall the syncall to set
	 */
	public void setSyncall(String syncall) {
		this.syncall = syncall;
	}
	
}
