package com.globus.valueobject.common.master;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmAccountsVO extends GmDataTstVO {

  private String acctid = "";
  private String acctname = "";
  private String repid = "";
  private String acctype = "";
  private String billname = "";
  private String billaddr1 = "";
  private String billaddr2 = "";
  private String billcity = "";
  private String billstate = "";
  private String billcountry = "";
  private String billzipcode = "";
  private String shipname = "";
  private String shipaddr1 = "";
  private String shipaddr2 = "";
  private String shipcity = "";
  private String shipstate = "";
  private String shipcountry = "";
  private String shipzipcode = "";
  private String actvfl = "";
  private String voidfl = "";
  private String compid = "";
  private String extcountryid = "";
  private String dealid = "";


  /**
   * @return the acctid
   */
  public String getAcctid() {
    return acctid;
  }

  /**
   * @param acctid the acctid to set
   */
  public void setAcctid(String acctid) {
    this.acctid = acctid;
  }

  /**
   * @return the acctname
   */
  public String getAcctname() {
    return acctname;
  }

  /**
   * @param acctname the acctname to set
   */
  public void setAcctname(String acctname) {
    this.acctname = acctname;
  }

  /**
   * @return the repid
   */
  public String getRepid() {
    return repid;
  }

  /**
   * @param repid the repid to set
   */
  public void setRepid(String repid) {
    this.repid = repid;
  }

  /**
   * @return the acctype
   */
  public String getAcctype() {
    return acctype;
  }

  /**
   * @param acctype the acctype to set
   */
  public void setAcctype(String acctype) {
    this.acctype = acctype;
  }

  /**
   * @return the billname
   */
  public String getBillname() {
    return billname;
  }

  /**
   * @param billname the billname to set
   */
  public void setBillname(String billname) {
    this.billname = billname;
  }

  /**
   * @return the billaddr1
   */
  public String getBilladdr1() {
    return billaddr1;
  }

  /**
   * @param billaddr1 the billaddr1 to set
   */
  public void setBilladdr1(String billaddr1) {
    this.billaddr1 = billaddr1;
  }

  /**
   * @return the billaddr2
   */
  public String getBilladdr2() {
    return billaddr2;
  }

  /**
   * @param billaddr2 the billaddr2 to set
   */
  public void setBilladdr2(String billaddr2) {
    this.billaddr2 = billaddr2;
  }

  /**
   * @return the billcity
   */
  public String getBillcity() {
    return billcity;
  }

  /**
   * @param billcity the billcity to set
   */
  public void setBillcity(String billcity) {
    this.billcity = billcity;
  }

  /**
   * @return the billstate
   */
  public String getBillstate() {
    return billstate;
  }

  /**
   * @param billstate the billstate to set
   */
  public void setBillstate(String billstate) {
    this.billstate = billstate;
  }

  /**
   * @return the billcountry
   */
  public String getBillcountry() {
    return billcountry;
  }

  /**
   * @param billcountry the billcountry to set
   */
  public void setBillcountry(String billcountry) {
    this.billcountry = billcountry;
  }

  /**
   * @return the billzipcode
   */
  public String getBillzipcode() {
    return billzipcode;
  }

  /**
   * @param billzipcode the billzipcode to set
   */
  public void setBillzipcode(String billzipcode) {
    this.billzipcode = billzipcode;
  }

  /**
   * @return the shipname
   */
  public String getShipname() {
    return shipname;
  }

  /**
   * @param shipname the shipname to set
   */
  public void setShipname(String shipname) {
    this.shipname = shipname;
  }

  /**
   * @return the shipaddr1
   */
  public String getShipaddr1() {
    return shipaddr1;
  }

  /**
   * @param shipaddr1 the shipaddr1 to set
   */
  public void setShipaddr1(String shipaddr1) {
    this.shipaddr1 = shipaddr1;
  }

  /**
   * @return the shipaddr2
   */
  public String getShipaddr2() {
    return shipaddr2;
  }

  /**
   * @param shipaddr2 the shipaddr2 to set
   */
  public void setShipaddr2(String shipaddr2) {
    this.shipaddr2 = shipaddr2;
  }

  /**
   * @return the shipcity
   */
  public String getShipcity() {
    return shipcity;
  }

  /**
   * @param shipcity the shipcity to set
   */
  public void setShipcity(String shipcity) {
    this.shipcity = shipcity;
  }

  /**
   * @return the shipstate
   */
  public String getShipstate() {
    return shipstate;
  }

  /**
   * @param shipstate the shipstate to set
   */
  public void setShipstate(String shipstate) {
    this.shipstate = shipstate;
  }

  /**
   * @return the shipcountry
   */
  public String getShipcountry() {
    return shipcountry;
  }

  /**
   * @param shipcountry the shipcountry to set
   */
  public void setShipcountry(String shipcountry) {
    this.shipcountry = shipcountry;
  }

  /**
   * @return the shipzipcode
   */
  public String getShipzipcode() {
    return shipzipcode;
  }

  /**
   * @param shipzipcode the shipzipcode to set
   */
  public void setShipzipcode(String shipzipcode) {
    this.shipzipcode = shipzipcode;
  }

  /**
   * @return the actvfl
   */
  public String getActvfl() {
    return actvfl;
  }

  /**
   * @param actvfl the actvfl to set
   */
  public void setActvfl(String actvfl) {
    this.actvfl = actvfl;
  }

  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }

  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  /**
   * @return the compid
   */
  public String getCompid() {
    return compid;
  }

  /**
   * @param compid the compid to set
   */
  public void setCompid(String compid) {
    this.compid = compid;
  }

  /**
   * @return the extcountryid
   */
  public String getExtcountryid() {
    return extcountryid;
  }

  /**
   * @param extcountryid the extcountryid to set
   */
  public void setExtcountryid(String extcountryid) {
    this.extcountryid = extcountryid;
  }

  
  
  /**
   * @return the dealid
   */
  public String getDealid() {
    return dealid;
  }

  /**
   * @param dealid the dealid to set
   */
  public void setDealid(String dealid) {
    this.dealid = dealid;
  }

  @Transient
  public Properties getAccountsProperties() {
    Properties props = new Properties();
    props.put("ACCTID", "ACCTID");
    props.put("ACCTNAME", "ACCTNAME");
    props.put("REPID", "REPID");
    props.put("ACCTYPE", "ACCTYPE");
    props.put("BILLNAME", "BILLNAME");
    props.put("BILLADDR1", "BILLADDR1");
    props.put("BILLADDR2", "BILLADDR2");
    props.put("BILLCITY", "BILLCITY");
    props.put("BILLSTATE", "BILLSTATE");
    props.put("BILLCOUNTRY", "BILLCOUNTRY");
    props.put("BILLZIPCODE", "BILLZIPCODE");
    props.put("SHIPNAME", "SHIPNAME");
    props.put("SHIPADDR1", "SHIPADDR1");
    props.put("SHIPADDR2", "SHIPADDR2");
    props.put("SHIPCITY", "SHIPCITY");
    props.put("SHIPSTATE", "SHIPSTATE");
    props.put("SHIPCOUNTRY", "SHIPCOUNTRY");
    props.put("SHIPZIPCODE", "SHIPZIPCODE");
    props.put("ACTVFL", "ACTVFL");
    props.put("VOIDFL", "VOIDFL");
    props.put("COMPID", "COMPID");
    props.put("EXTCOUNTRYID", "EXTCOUNTRYID");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    props.put("DEALID", "DEALID");
    return props;
  }



}
