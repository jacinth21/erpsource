package com.globus.valueobject.common.master;

import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.globus.common.beans.GmCommonClass;
import com.globus.valueobject.common.GmDataTstVO;
import com.globus.valueobject.common.GmProdCatlReqVO;
import com.globus.valueobject.logon.GmUserVO;
import com.globus.valueobject.pricing.GmAccountPriceVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmMasterDataSyncListVO extends GmProdCatlReqVO {
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmAccountsVO> listGmAccountVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmSalesRepVO> listGmSalesRepVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmAcctGpoMapVO> listGmAcctGpoMapVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmDistributorVO> listGmDistributorVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmAddressVO> listGmAddressVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmAccountPriceVO> listGmAcctPartPriceVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmUserVO> listGmUserVO = null;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<GmAssocRepMapVO> listGmAssocRepMapVO = null;

	public List<GmUserVO> getListGmUserVO() {
		return listGmUserVO;
	}

	public List<GmAssocRepMapVO> getListGmAssocRepMapVO() {
		return listGmAssocRepMapVO;
	}

	public void setListGmAssocRepMapVO(List<GmAssocRepMapVO> listGmAssocRepMapVO) {
		this.listGmAssocRepMapVO = listGmAssocRepMapVO;
	}

	public void setListGmUserVO(List<GmUserVO> listGmUserVO) {
		this.listGmUserVO = listGmUserVO;
	}

	public List<GmAccountsVO> getListGmAccountVO() {
		return listGmAccountVO;
	}

	public void setListGmAccountVO(List<GmAccountsVO> listGmAccountVO) {
		this.listGmAccountVO = listGmAccountVO;
	}

	public List<GmSalesRepVO> getListGmSalesRepVO() {
		return listGmSalesRepVO;
	}

	public void setListGmSalesRepVO(List<GmSalesRepVO> listGmSalesRepVO) {
		this.listGmSalesRepVO = listGmSalesRepVO;
	}

	public List<GmAcctGpoMapVO> getListGmAcctGpoMapVO() {
		return listGmAcctGpoMapVO;
	}

	public void setListGmAcctGpoMapVO(List<GmAcctGpoMapVO> listGmAcctGpoMapVO) {
		this.listGmAcctGpoMapVO = listGmAcctGpoMapVO;
	}

	public List<GmDistributorVO> getListGmDistributorVO() {
		return listGmDistributorVO;
	}

	public void setListGmDistributorVO(List<GmDistributorVO> listGmDistributorVO) {
		this.listGmDistributorVO = listGmDistributorVO;
	}

	public List<GmAddressVO> getListGmAddressVO() {
		return listGmAddressVO;
	}

	public void setListGmAddressVO(List<GmAddressVO> listGmAddressVO) {
		this.listGmAddressVO = listGmAddressVO;
	}

	public List<GmAccountPriceVO> getListGmAcctPartPriceVO() {
		return listGmAcctPartPriceVO;
	}

	public void setListGmAcctPartPriceVO(
			List<GmAccountPriceVO> listGmAcctPartPriceVO) {
		this.listGmAcctPartPriceVO = listGmAcctPartPriceVO;
	}

	public void populatePageProperties(List alList) {
		int alReturnSize = alList.size();
		if (alReturnSize > 0) {
			HashMap hmPageInfo = GmCommonClass
					.parseNullHashMap((HashMap) alList.get(0));
			this.setPageno((String) hmPageInfo.get("PAGENO"));
			this.setTotalpages((String) hmPageInfo.get("TOTALPAGES"));
			this.setTotalsize((String) hmPageInfo.get("TOTALSIZE"));
			this.setPagesize((String) hmPageInfo.get("PAGESIZE"));
		}
	}

	/**
	 * @param ArrayList
	 *            - result set
	 * @param String
	 *            - primary key name
	 */
	public void populatePageProperties(List alList, String strLastRecKey) {
		int alReturnSize = alList.size();
		if (alReturnSize > 0) {
			HashMap hmPageInfo = GmCommonClass
					.parseNullHashMap((HashMap) alList.get(alReturnSize - 1));
			this.setPageno((String) hmPageInfo.get("PAGENO"));
			this.setTotalpages((String) hmPageInfo.get("TOTALPAGES"));
			this.setTotalsize((String) hmPageInfo.get("TOTALSIZE"));
			this.setPagesize((String) hmPageInfo.get("PAGESIZE"));
			this.setLastrec(GmCommonClass.parseNull((String) hmPageInfo
					.get(strLastRecKey)));
		}
	}

	public GmDataTstVO setPageProperties(List alList) {
		int alReturnSize = alList.size();
		String strAlSize = Integer.toString(alReturnSize);
		GmDataTstVO gmDataTstVO = new GmDataTstVO();
		if (alReturnSize > 0) {
			gmDataTstVO = (GmDataTstVO) alList.get(alReturnSize - 1);
			this.setPageno(gmDataTstVO.getPageno());
			this.setTotalpages(gmDataTstVO.getTotalpages());
			this.setTotalsize(gmDataTstVO.getTotalsize());
			this.setPagesize(strAlSize);
			return gmDataTstVO;
		} else {
			this.setPageno("1");
			this.setTotalpages("");
			this.setTotalsize("");
			this.setPagesize("");
			return null;
		}

	}
}
