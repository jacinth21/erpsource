package com.globus.valueobject.common.master;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmAssocRepMapVO extends GmDataTstVO {

  private String accasrepmapid = "";
  private String acctid = "";
  private String repid = "";
  private String voidfl = "";
  private String activefl = "";

  public String getActivefl() {
    return activefl;
  }

  public void setActivefl(String activefl) {
    this.activefl = activefl;
  }

  public String getAccasrepmapid() {
    return accasrepmapid;
  }

  public void setAccasrepmapid(String accasrepmapid) {
    this.accasrepmapid = accasrepmapid;
  }

  public String getAcctid() {
    return acctid;
  }

  public void setAcctid(String acctid) {
    this.acctid = acctid;
  }

  public String getRepid() {
    return repid;
  }

  public void setRepid(String repid) {
    this.repid = repid;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }
  
  @Transient
  public Properties getAssocRepMappingProperties() {
    Properties props = new Properties();
    props.put("ACCASREPMAPID", "ACCASREPMAPID");
    props.put("ACCTID", "ACCTID");
    props.put("REPID", "REPID");
    props.put("VOIDFL", "VOIDFL");
    props.put("ACTIVEFL", "ACTIVEFL");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    return props;
  }


}
