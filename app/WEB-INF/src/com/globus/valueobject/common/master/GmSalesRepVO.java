package com.globus.valueobject.common.master;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSalesRepVO extends GmDataTstVO {

  private String repid = "";
  private String repname = "";
  private String did = "";
  private String dname = "";
  private String partyid = "";
  private String actvfl = "";
  private String territoryid = "";
  private String voidfl = "";
  private String extcountryid = "";
  private String desgn = "";

  /**
   * @return the repid
   */
  public String getRepid() {
    return repid;
  }

  /**
   * @return the desgn
   */
  public String getDesgn() {
    return desgn;
  }

  /**
   * @param desgn the desgn to set
   */
  public void setDesgn(String desgn) {
    this.desgn = desgn;
  }

  /**
   * @param repid the repid to set
   */
  public void setRepid(String repid) {
    this.repid = repid;
  }

  /**
   * @return the repname
   */
  public String getRepname() {
    return repname;
  }

  /**
   * @param repname the repname to set
   */
  public void setRepname(String repname) {
    this.repname = repname;
  }

  /**
   * @return the did
   */
  public String getDid() {
    return did;
  }

  /**
   * @param did the did to set
   */
  public void setDid(String did) {
    this.did = did;
  }

  /**
   * @return the dname
   */
  public String getDname() {
    return dname;
  }

  /**
   * @param dname the dname to set
   */
  public void setDname(String dname) {
    this.dname = dname;
  }

  /**
   * @return the partyid
   */
  public String getPartyid() {
    return partyid;
  }

  /**
   * @param partyid the partyid to set
   */
  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  /**
   * @return the actvfl
   */
  public String getActvfl() {
    return actvfl;
  }

  /**
   * @param actvfl the actvfl to set
   */
  public void setActvfl(String actvfl) {
    this.actvfl = actvfl;
  }

  /**
   * @return the territoryid
   */
  public String getTerritoryid() {
    return territoryid;
  }

  /**
   * @param territoryid the territoryid to set
   */
  public void setTerritoryid(String territoryid) {
    this.territoryid = territoryid;
  }

  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }

  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  /**
   * @return the extcountryid
   */
  public String getExtcountryid() {
    return extcountryid;
  }

  /**
   * @param extcountryid the extcountryid to set
   */
  public void setExtcountryid(String extcountryid) {
    this.extcountryid = extcountryid;
  }

  @Transient
  public Properties getSalesRepsProperties() {
    Properties props = new Properties();
    props.put("REPNAME", "REPNAME");
    props.put("REPID", "REPID");
    props.put("DID", "DID");
    props.put("DNAME", "DNAME");
    props.put("PARTYID", "PARTYID");
    props.put("ACTVFL", "ACTVFL");
    props.put("TERRITORYID", "TERRITORYID");
    props.put("VOIDFL", "VOIDFL");
    props.put("EXTCOUNTRYID", "EXTCOUNTRYID");
    props.put("DESGN", "DESGN");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    return props;
  }


}
