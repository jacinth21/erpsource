package com.globus.valueobject.common.master;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataTstVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmAcctGpoMapVO extends GmDataTstVO {

  private String acctgpomapid = "";
  private String acctid = "";
  private String partyid = "";
  private String gpocatalogid = "";
  private String voidfl = "";

  /**
   * @return the acctgpomapid
   */
  public String getAcctgpomapid() {
    return acctgpomapid;
  }

  /**
   * @param acctgpomapid the acctgpomapid to set
   */
  public void setAcctgpomapid(String acctgpomapid) {
    this.acctgpomapid = acctgpomapid;
  }

  /**
   * @return the acctid
   */
  public String getAcctid() {
    return acctid;
  }

  /**
   * @param acctid the acctid to set
   */
  public void setAcctid(String acctid) {
    this.acctid = acctid;
  }

  /**
   * @return the partyid
   */
  public String getPartyid() {
    return partyid;
  }

  /**
   * @param partyid the partyid to set
   */
  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  /**
   * @return the gpocatalogid
   */
  public String getGpocatalogid() {
    return gpocatalogid;
  }

  /**
   * @param gpocatalogid the gpocatalogid to set
   */
  public void setGpocatalogid(String gpocatalogid) {
    this.gpocatalogid = gpocatalogid;
  }

  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }

  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  @Transient
  public Properties getAcctGpoMappingProperties() {
    Properties props = new Properties();
    props.put("ACCTGPOMAPID", "ACCTGPOMAPID");
    props.put("ACCTID", "ACCTID");
    props.put("PARTYID", "PARTYID");
    props.put("GPOCATALOGID", "GPOCATALOGID");
    props.put("VOIDFL", "VOIDFL");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    return props;
  }


}
