package com.globus.valueobject.common.master;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;



import com.globus.valueobject.common.GmDataStoreVO;
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmAddressVO extends GmDataStoreVO{
	
	private String addrid="";
	private String addresstype="";
	private String partyid="";
	private String billadd1="";
	private String billadd2="";
	private String billcity="";
	private String state="";
	private String country="";
	private String zipcode="";
	private String preference="";
	private String activeflag="";
	private String primaryflag="";
	private String voidfl="";
	private String statename="";
	private String countryname="";
	private String addresstypename="";
	
	
	/**
	 * @return the addrid
	 */
	public String getAddrid() {
		return addrid;
	}


	/**
	 * @param addrid the addrid to set
	 */
	public void setAddrid(String addrid) {
		this.addrid = addrid;
	}


	/**
	 * @return the addresstype
	 */
	public String getAddresstype() {
		return addresstype;
	}


	/**
	 * @param addresstype the addresstype to set
	 */
	public void setAddresstype(String addresstype) {
		this.addresstype = addresstype;
	}


	/**
	 * @return the partyid
	 */
	public String getPartyid() {
		return partyid;
	}


	/**
	 * @param partyid the partyid to set
	 */
	public void setPartyid(String partyid) {
		this.partyid = partyid;
	}


	/**
	 * @return the billadd1
	 */
	public String getBilladd1() {
		return billadd1;
	}


	/**
	 * @param billadd1 the billadd1 to set
	 */
	public void setBilladd1(String billadd1) {
		this.billadd1 = billadd1;
	}


	/**
	 * @return the billadd2
	 */
	public String getBilladd2() {
		return billadd2;
	}


	/**
	 * @param billadd2 the billadd2 to set
	 */
	public void setBilladd2(String billadd2) {
		this.billadd2 = billadd2;
	}


	/**
	 * @return the billcity
	 */
	public String getBillcity() {
		return billcity;
	}


	/**
	 * @param billcity the billcity to set
	 */
	public void setBillcity(String billcity) {
		this.billcity = billcity;
	}


	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}


	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}


	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}


	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}


	/**
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}


	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}


	/**
	 * @return the preference
	 */
	public String getPreference() {
		return preference;
	}


	/**
	 * @param preference the preference to set
	 */
	public void setPreference(String preference) {
		this.preference = preference;
	}


	/**
	 * @return the activeflag
	 */
	public String getActiveflag() {
		return activeflag;
	}


	/**
	 * @param activeflag the activeflag to set
	 */
	public void setActiveflag(String activeflag) {
		this.activeflag = activeflag;
	}


	/**
	 * @return the primaryflag
	 */
	public String getPrimaryflag() {
		return primaryflag;
	}


	/**
	 * @param primaryflag the primaryflag to set
	 */
	public void setPrimaryflag(String primaryflag) {
		this.primaryflag = primaryflag;
	}


	/**
	 * @return the voidfl
	 */
	public String getVoidfl() {
		return voidfl;
	}


	/**
	 * @param voidfl the voidfl to set
	 */
	public void setVoidfl(String voidfl) {
		this.voidfl = voidfl;
	}


	/**
	 * @return the statename
	 */
	public String getStatename() {
		return statename;
	}


	/**
	 * @param statename the statename to set
	 */
	public void setStatename(String statename) {
		this.statename = statename;
	}


	/**
	 * @return the countryname
	 */
	public String getCountryname() {
		return countryname;
	}


	/**
	 * @param countryname the countryname to set
	 */
	public void setCountryname(String countryname) {
		this.countryname = countryname;
	}


	/**
	 * @return the addresstypename
	 */
	public String getAddresstypename() {
		return addresstypename;
	}


	/**
	 * @param addresstypename the addresstypename to set
	 */
	public void setAddresstypename(String addresstypename) {
		this.addresstypename = addresstypename;
	}

	@Transient
	public Properties getAddressProperties(){
		Properties props = new Properties();
				
		props.put("ADDRID","ADDRID");
		props.put("ADDRTYPE","ADDRESSTYPE");
		props.put("PARTYID","PARTYID");
		props.put("ADDR1","BILLADD1");
		props.put("ADDR2","BILLADD2");
		props.put("CITY","BILLCITY");
		props.put("STATE","STATE");
		props.put("COUNTRY","COUNTRY");
		props.put("ZIPCODE","ZIPCODE");
		props.put("INACTVFL","ACTIVEFLAG");
		props.put("VOIDFL","VOIDFL");
		props.put("PRIMARYFL","PRIMARYFLAG");
		props.put("PREFERENCE","PREFERENCE");
		props.put("ADDRESSTYPENAME","ADDRESSTYPENAME");
		props.put("STATENAME","STATENAME");
		props.put("COUNTRYNAME","COUNTRYNAME");
		return props;
	}

}