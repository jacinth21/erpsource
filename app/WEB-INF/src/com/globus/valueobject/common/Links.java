package com.globus.valueobject.common;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class Links
{
    private String id;

    private String[] recipients;
   // private List<String> recipients = new ArrayList<String>();

    private String url;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

  


	/**
	 * @return the recipients
	 */
	public String[] getRecipients() {
		return recipients;
	}

	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients(String[] recipients) {
		this.recipients = recipients;
	}

	public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }
    
    /**
	 * @return
	 */
	public static Map getSaveSharedLinkProperties(){
		Map hmLinkedDbInputStr = new LinkedHashMap();  
		hmLinkedDbInputStr.put("URL", "getUrl");
		return hmLinkedDbInputStr;
	}
}

		
