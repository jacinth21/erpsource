package com.globus.valueobject.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GmGetOANDAResponseVo {


  private GmGetOANDAQuotesVo[] quotes;
  private String oanda;

  /**
   * @return the oanda
   */
  public String getOanda() {
    return oanda;
  }

  /**
   * @param oanda the oanda to set
   */
  public void setOanda(String oanda) {
    this.oanda = oanda;
  }

  /**
   * @return the quotes
   */
  public GmGetOANDAQuotesVo[] getQuotes() {
    return quotes;
  }

  /**
   * @param quotes the quotes to set
   */
  public void setQuotes(GmGetOANDAQuotesVo[] quotes) {
    this.quotes = quotes;
  }


}
