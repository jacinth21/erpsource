package com.globus.valueobject.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmDataStoreVO extends GmDataVO {

  private String userid = "";
  private String stropt = "";
  private String token = "";
  private String cmpid = "";
  private String cmptzone = "";
  private String cmpdfmt = "";
  private String plantid = "";
  private String partyid = "";
  private String cmplangid = "";


private String compid = "";


  public String getCmplangid() {
    return cmplangid;
  }

  public void setCmplangid(String cmplangid) {
    this.cmplangid = cmplangid;
  }

  public String getPartyid() {
    return partyid;
  }

  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  public String getCmpid() {
    return cmpid;
  }

  public void setCmpid(String cmpid) {
    this.cmpid = cmpid;
  }

  public String getPlantid() {
    return plantid;
  }

  public void setPlantid(String plantid) {
    this.plantid = plantid;
  }

  public String getCmptzone() {
    return cmptzone;
  }

  public void setCmptzone(String cmptzone) {
    this.cmptzone = cmptzone;
  }

  public String getCmpdfmt() {
    return cmpdfmt;
  }

  public void setCmpdfmt(String cmpdfmt) {
    this.cmpdfmt = cmpdfmt;
  }

  /**
   * @return the userid
   */
  public String getUserid() {
    return userid;
  }

  /**
   * @param userid the userid to set
   */
  public void setUserid(String userid) {
    this.userid = userid;
  }

  /**
   * @return the stropt
   */
  public String getStropt() {
    return stropt;
  }

  /**
   * @param stropt the stropt to set
   */
  public void setStropt(String stropt) {
    this.stropt = stropt;
  }

  /**
   * @return the token
   */
  public String getToken() {
    return token;
  }

  /**
   * @param token the token to set
   */
  public void setToken(String token) {
    this.token = token;
  }
  

  public String getCompid() {
	return compid;
}

public void setCompid(String compid) {
	this.compid = compid;
}

}
