package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPartyVO extends GmDataTstVO {
  private String partyid = "";
  private String firstnm = "";
  private String lastnm = "";
  private String midinitial = "";
  private String partytype = "";
  private String voidfl = "";
  private String deletefl = "";
  private String activefl = "";
  private String partynm = "";

  /**
   * @return the partynm
   */
  public String getPartynm() {
    return partynm;
  }

  /**
   * @param partynm the partynm to set
   */
  public void setPartynm(String partynm) {
    this.partynm = partynm;
  }


  public String getPartyid() {
    return partyid;
  }


  public void setPartyid(String partyid) {
    this.partyid = partyid;
  }

  public String getFirstnm() {
    return firstnm;
  }

  public void setFirstnm(String firstnm) {
    this.firstnm = firstnm;
  }

  public String getLastnm() {
    return lastnm;
  }

  public void setLastnm(String lastnm) {
    this.lastnm = lastnm;
  }

  public String getMidinitial() {
    return midinitial;
  }

  public void setMidinitial(String midinitial) {
    this.midinitial = midinitial;
  }

  public String getVoidfl() {
    return voidfl;
  }

  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  public String getPartytype() {
    return partytype;
  }

  public void setPartytype(String partytype) {
    this.partytype = partytype;
  }

  public String getDeletefl() {
    return deletefl;
  }

  public void setDeletefl(String deletefl) {
    this.deletefl = deletefl;
  }

  public String getActivefl() {
    return activefl;
  }

  public void setActivefl(String activefl) {
    this.activefl = activefl;
  }

  @Transient
  public Properties getSystemProperties() {
    Properties props = new Properties();
    props.put("PARTYID", "PARTYID");
    props.put("FIRSTNM", "FIRSTNM");
    props.put("LASTNM", "LASTNM");
    props.put("MIDINITIAL", "MIDINITIAL");
    props.put("PARTYTYPE", "PARTYTYPE");
    props.put("VOIDFL", "VOIDFL");
    props.put("DELETEFL", "DELETEFL");
    props.put("VOIDFL", "VOIDFL");
    props.put("ACTIVEFL", "ACTIVEFL");
    props.put("PARTYNM", "PARTYNM");
    props.put("TOTALPAGES", "TOTALPAGES");
    props.put("PAGENO", "PAGENO");
    props.put("TOTALSIZE", "TOTALSIZE");
    props.put("PAGESIZE", "PAGESIZE");
    return props;
  }


}
