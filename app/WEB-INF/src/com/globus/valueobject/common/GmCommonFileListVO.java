package com.globus.valueobject.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCommonFileListVO extends GmDataStoreVO {

  private GmCommonFileUploadVO[] gmCommonFileUploadVO;

  /**
   * @return the gmCommonFileUploadVO
   */
  public GmCommonFileUploadVO[] getGmCommonFileUploadVO() {
    return gmCommonFileUploadVO;
  }

  /**
   * @param gmCommonFileUploadVO the gmCommonFileUploadVO to set
   */
  public void setGmCommonFileUploadVO(GmCommonFileUploadVO[] gmCommonFileUploadVO) {
    this.gmCommonFileUploadVO = gmCommonFileUploadVO;
  }



}
