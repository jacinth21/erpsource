package com.globus.valueobject.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.sales.GmDeviceDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmDeviceInfoVO extends GmDeviceDataStoreVO {

  private String uuid = "";
  private String devicetype = "";
  private String devicetoken = "";

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getDevicetype() {
    return devicetype;
  }

  public void setDevicetype(String devicetype) {
    this.devicetype = devicetype;
  }

  /**
   * @return the devicetoken
   */
  public String getDevicetoken() {
    return devicetoken;
  }

  /**
   * @param devicetoken the devicetoken to set
   */
  public void setDevicetoken(String devicetoken) {
    this.devicetoken = devicetoken;
  }
}
