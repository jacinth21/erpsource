package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCommonFileUploadVO extends GmDataStoreVO {

  private String fileid = "";
  private String filename = "";
  private String deletefl = "";
  private String refid = "";
  private String reftype = "";
  private String type = "";
  private String updateddate = "";
  private String updatedby = "";
  private String refgroup = "";
  private String filetitle = "";
  private String reftypenm = "";
  private String message = "";
  private String filesize = "";
  private String fsize = "";
  private String userid = "";



/**
   * @return the fileid
   */
  public String getFileid() {
    return fileid;
  }

  /**
   * @param fileid the fileid to set
   */
  public void setFileid(String fileid) {
    this.fileid = fileid;
  }

  /**
   * @return the filename
   */
  public String getFilename() {
    return filename;
  }

  /**
   * @param filename the filename to set
   */
  public void setFilename(String filename) {
    this.filename = filename;
  }

  /**
   * @return the deletefl
   */
  public String getDeletefl() {
    return deletefl;
  }

  /**
   * @param deletefl the deletefl to set
   */
  public void setDeletefl(String deletefl) {
    this.deletefl = deletefl;
  }


  /**
   * @return the refid
   */
  public String getRefid() {
    return refid;
  }



  /**
   * @param refid the refid to set
   */
  public void setRefid(String refid) {
    this.refid = refid;
  }



  /**
   * @return the reftype
   */
  public String getReftype() {
    return reftype;
  }



  /**
   * @param reftype the reftype to set
   */
  public void setReftype(String reftype) {
    this.reftype = reftype;
  }



  /**
   * @return the type
   */
  public String getType() {
    return type;
  }



  /**
   * @param type the type to set
   */
  public void setType(String type) {
    this.type = type;
  }



  /**
   * @return the updateddate
   */
  public String getUpdateddate() {
    return updateddate;
  }



  /**
   * @param updateddate the updateddate to set
   */
  public void setUpdateddate(String updateddate) {
    this.updateddate = updateddate;
  }



  /**
   * @return the updatedby
   */
  public String getUpdatedby() {
    return updatedby;
  }



  /**
   * @param updatedby the updatedby to set
   */
  public void setUpdatedby(String updatedby) {
    this.updatedby = updatedby;
  }

  /**
   * @return the file
   */
  // public FormFile getFile() {
  // return file;
  // }
  //
  // /**
  // * @param file the file to set
  // */
  // public void setFile(FormFile file) {
  // this.file = file;
  // }

  /**
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message the message to set
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * @return the refgroup
   */
  public String getRefgroup() {
    return refgroup;
  }

  /**
   * @param refgroup the refgroup to set
   */
  public void setRefgroup(String refgroup) {
    this.refgroup = refgroup;
  }

  /**
   * @return the filetitle
   */
  public String getFiletitle() {
    return filetitle;
  }

  /**
   * @param filetitle the filetitle to set
   */
  public void setFiletitle(String filetitle) {
    this.filetitle = filetitle;
  }

  /**
   * @return the reftypenm
   */
  public String getReftypenm() {
    return reftypenm;
  }

  /**
   * @param reftypenm the reftypenm to set
   */
  public void setReftypenm(String reftypenm) {
    this.reftypenm = reftypenm;
  }
  
  /**
   * @return the filesize
   */
  public String getFilesize() {
    return filesize;
  }

  /**
   * @param filesize the filesize to set
   */
  public void setFilesize(String filesize) {
    this.filesize = filesize;
  }
  
  /**
   * @return the fsize
   */
  public String getFsize() {
    return fsize;
  }

  /**
   * @param fsize the fsize to set
   */
  public void setFsize(String fsize) {
    this.fsize = fsize;
  }
  
  /**
   * @return the userid
   */
  public String getUserid() {
	return userid;
  }

  /**
   * @param userid the userid to set
   */
  public void setUserid(String userid) {
	this.userid = userid;
  }

  @Transient
  public Properties getMappingProperties() {

    Properties properties = new Properties();
    properties.put("ID", "FILEID");
    properties.put("NAME", "FILENAME");
    properties.put("REFTYPE", "REFTYPE");
    properties.put("SETID", "REFID");
    properties.put("CDATETIME", "UPDATEDDATE");
    properties.put("UNAME", "UPDATEDBY");
    properties.put("FILESIZE", "FILESIZE");
    properties.put("FSIZE", "FSIZE");
    properties.put("USERID", "USERID");

    return properties;
  }



}
