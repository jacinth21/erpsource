package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmPlantVO extends GmDataStoreVO {
  private String plantid = "";
  private String plantnm = "";
  private String plantcd = "";

  public String getPlantid() {
    return plantid;
  }

  public void setPlantid(String plantid) {
    this.plantid = plantid;
  }

  public String getPlantnm() {
    return plantnm;
  }

  public void setPlantnm(String plantnm) {
    this.plantnm = plantnm;
  }

  public String getPlantcd() {
    return plantcd;
  }

  public void setPlantcd(String plantcd) {
    this.plantcd = plantcd;
  }

  @Transient
  public Properties getPlantProperties() {
    Properties props = new Properties();
    props.put("PLANTID", "PLANTID");
    props.put("PLANTNM", "PLANTNM");
    props.put("PLANTCD", "PLANTCD");
    return props;
  }

}
