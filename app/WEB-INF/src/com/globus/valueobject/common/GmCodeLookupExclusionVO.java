package com.globus.valueobject.common;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmCodeLookupExclusionVO extends GmDataVO {

  private String lookupexcid = "";
  private String codeid = "";
  private String voidfl = "";
  private String cmpid = "";


  public String getCmpid() {
    return cmpid;
  }

  public void setCmpid(String cmpid) {
    this.cmpid = cmpid;
  }

  /**
   * @return the lookupexcid
   */
  public String getLookupexcid() {
    return lookupexcid;
  }

  /**
   * @param lookupexcid the lookupexcid to set
   */
  public void setLookupexcid(String lookupexcid) {
    this.lookupexcid = lookupexcid;
  }

  /**
   * @return the codeid
   */
  public String getCodeid() {
    return codeid;
  }

  /**
   * @param codeid the codeid to set
   */
  public void setCodeid(String codeid) {
    this.codeid = codeid;
  }

  /**
   * @return the voidfl
   */
  public String getVoidfl() {
    return voidfl;
  }

  /**
   * @param voidfl the voidfl to set
   */
  public void setVoidfl(String voidfl) {
    this.voidfl = voidfl;
  }

  @Transient
  public Properties getCodeLookupExclusionProperties() {
    Properties props = new Properties();
    props.put("LOOKUPEXCID", "LOOKUPEXCID");
    props.put("CODEID", "CODEID");
    props.put("VOIDFL", "VOIDFL");
    props.put("CMPID", "CMPID");
    return props;
  }
}
