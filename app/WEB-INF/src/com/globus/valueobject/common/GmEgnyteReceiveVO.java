package com.globus.valueobject.common;


import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement

public class GmEgnyteReceiveVO
{
    private String creation_date;
    private String link_to_current;
    private String notify;
    private String path;
    private String copy_me;
    //private String expiry_clicks;
    private Links[] links;
    private String type;
    private String send_mail;
    private String accessibility;
    private String expiry_date;
    private String message;
    
    
	/**
	 * @return the creation_date
	 */
	public String getCreation_date() {
		return creation_date;
	}
	/**
	 * @param creationDate the creation_date to set
	 */
	public void setCreation_date(String creationDate) {
		creation_date = creationDate;
	}
	/**
	 * @return the link_to_current
	 */
	public String getLink_to_current() {
		return link_to_current;
	}
	/**
	 * @param linkToCurrent the link_to_current to set
	 */
	public void setLink_to_current(String linkToCurrent) {
		link_to_current = linkToCurrent;
	}
	/**
	 * @return the notify
	 */
	public String getNotify() {
		return notify;
	}
	/**
	 * @param notify the notify to set
	 */
	public void setNotify(String notify) {
		this.notify = notify;
	}
	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * @return the copy_me
	 */
	public String getCopy_me() {
		return copy_me;
	}
	/**
	 * @param copyMe the copy_me to set
	 */
	public void setCopy_me(String copyMe) {
		copy_me = copyMe;
	}
	
	/**
	 * @return the links
	 */
	public Links[] getLinks() {
		return links;
	}
	/**
	 * @param links the links to set
	 */
	public void setLinks(Links[] links) {
		this.links = links;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the send_mail
	 */
	public String getSend_mail() {
		return send_mail;
	}
	/**
	 * @param sendMail the send_mail to set
	 */
	public void setSend_mail(String sendMail) {
		send_mail = sendMail;
	}
	/**
	 * @return the accessibility
	 */
	public String getAccessibility() {
		return accessibility;
	}
	/**
	 * @param accessibility the accessibility to set
	 */
	public void setAccessibility(String accessibility) {
		this.accessibility = accessibility;
	}
	/**
	 * @return the expiry_date
	 */
	public String getExpiry_date() {
		return expiry_date;
	}
	/**
	 * @param expiryDate the expiry_date to set
	 */
	public void setExpiry_date(String expiryDate) {
		expiry_date = expiryDate;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public String toString(){
		
		    
			String objToString = "message:"+message+"   "+"expiry_date:"+expiry_date+"   "+"path:"+path+"   "+"type:"+type+"   "+
			"accessibility:"+accessibility+"   "+"notify:"+notify+"   "+"link_to_current:"+link_to_current+"   "+"creation_date:"+creation_date
			+"   "+"send_mail:"+send_mail+"   "+"copy_me:"+copy_me+"   "+"links:"+links;
			return objToString;
			
		}
}
	
			
