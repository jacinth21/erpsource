package com.globus.valueobject.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmDeviceDetailVO extends GmDeviceInfoVO {
  private String totalsize = "";
  private String pagesize = "";
  private String pageno = "";
  private String totalpages = "";
  private String langid = "";
  private String reftype = "";
  private String statusid = "";
  private String countryid = "";

  /**
   * @return the countryid
   */
  public String getCountryid() {
    return countryid;
  }

  /**
   * @param countryid the countryid to set
   */
  public void setCountryid(String countryid) {
    this.countryid = countryid;
  }

  /**
   * @return the totalsize
   */
  public String getTotalsize() {
    return totalsize;
  }

  /**
   * @param totalsize the totalsize to set
   */
  public void setTotalsize(String totalsize) {
    this.totalsize = totalsize;
  }

  /**
   * @return the pagesize
   */
  public String getPagesize() {
    return pagesize;
  }

  /**
   * @param pagesize the pagesize to set
   */
  public void setPagesize(String pagesize) {
    this.pagesize = pagesize;
  }

  /**
   * @return the pageno
   */
  public String getPageno() {
    return pageno;
  }

  /**
   * @param pageno the pageno to set
   */
  public void setPageno(String pageno) {
    this.pageno = pageno;
  }

  /**
   * @return the totalpages
   */
  public String getTotalpages() {
    return totalpages;
  }

  /**
   * @param totalpages the totalpages to set
   */
  public void setTotalpages(String totalpages) {
    this.totalpages = totalpages;
  }

  /**
   * @return the langid
   */
  public String getLangid() {
    return langid;
  }

  /**
   * @param langid the langid to set
   */
  public void setLangid(String langid) {
    this.langid = langid;
  }

  /**
   * @return the reftype
   */
  public String getReftype() {
    return reftype;
  }

  /**
   * @param reftype the reftype to set
   */
  public void setReftype(String reftype) {
    this.reftype = reftype;
  }

  /**
   * @return the statusid
   */
  public String getStatusid() {
    return statusid;
  }

  /**
   * @param statusid the statusid to set
   */
  public void setStatusid(String statusid) {
    this.statusid = statusid;
  }



}
