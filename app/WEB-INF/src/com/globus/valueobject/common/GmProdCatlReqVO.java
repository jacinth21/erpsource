package com.globus.valueobject.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmProdCatlReqVO extends GmDeviceDetailVO {
  private String svrcnt = "";
  private String lastrec = "";

  public String getSvrcnt() {
    return svrcnt;
  }

  public void setSvrcnt(String svrcnt) {
    this.svrcnt = svrcnt;
  }

  public String getLastrec() {
    return lastrec;
  }

  public void setLastrec(String lastrec) {
    this.lastrec = lastrec;
  }
}
