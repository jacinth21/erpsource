
/**********************************************************************************
 * File		 		: GmUserContactVO.java
 * Desc		 		: This vo is used to get and set users personal info
 * Version	 		: 1.0
 * author			: Arunkumar.A
************************************************************************************/




package com.globus.valueobject.logon;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)

public class GmUserContactVO extends  GmDataStoreVO {


	 String email ="";
	 String phone = "";
	 String usernm = "";
	
	public String getUsernm() {
		return usernm;
	}
	public void setUsernm(String usernm) {
		this.usernm = usernm;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getphone() {
		return phone;
		
	}
	public void setphone(String phone) {
		this.phone = phone;
			
	}
	
	@Transient
	public Properties getMappingProperties()
  	{
  		Properties properties = new Properties();
  		properties.put("PHONE", "PHONE");
  		properties.put("EMAIL", "EMAIL");
		return null;
	
  	} 
	
	}