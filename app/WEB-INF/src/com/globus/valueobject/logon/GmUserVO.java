/**
 * 
 */
package com.globus.valueobject.logon;

import java.beans.Transient;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.globus.valueobject.common.GmDataStoreVO;

/**
 * @author vprasath
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmUserVO extends GmDataStoreVO {


  private String usernm = "";
  private String shname = "";
  private String deptid = "";
  private String acid = "";
  private String deptseq = "";
  private String logints = "";
  private String companyid = "";
  private String divid = "";
  private String passwd = "";
  private String pin = "";
  private String extaccess = "";
  private String repid = "";
  private String fname = "";
  private String lname = "";
  private String emailid = "";
  private String acslvl = "";
  private String did = "";
  private String status = "";
  private String type = "";
  private String hiredt = "";
  private String termtdt = "";
  private String cmpcurrfmt = "";
  private String cmpcurrsmb = "";
  private String defcurrncysymbol = "";
  private String currtype = "";
  private String pdffailurecnt = "";
  private String compcd = "";
  private String strcmpadress = "";
  private String strremitadress = "";
  private String strcompname = "";
  private String strcompphn = "";
  private String strcompfax = "";
  private String pdffilepath = "";
  private String egnytetkn = "";
  private String egnytefl = "";
  private String repdiv = "";

  public String getRepdiv() {
    return repdiv;
  }

  public void setRepdiv(String repdiv) {
    this.repdiv = repdiv;
  }


  /**
   * @return the strcompname
   */
  public String getStrcompname() {
    return strcompname;
  }

  /**
   * @param strcompname the strcompname to set
   */
  public void setStrcompname(String strcompname) {
    this.strcompname = strcompname;
  }

  /**
   * @return the strcompphn
   */
  public String getStrcompphn() {
    return strcompphn;
  }

  /**
   * @param strcompphn the strcompphn to set
   */
  public void setStrcompphn(String strcompphn) {
    this.strcompphn = strcompphn;
  }

  /**
   * @return the strcompfax
   */
  public String getStrcompfax() {
    return strcompfax;
  }

  /**
   * @param strcompfax the strcompfax to set
   */
  public void setStrcompfax(String strcompfax) {
    this.strcompfax = strcompfax;
  }

  /**
   * @return the strcmpadress
   */
  public String getStrcmpadress() {
    return strcmpadress;
  }

  /**
   * @param strcmpadress the strcmpadress to set
   */
  public void setStrcmpadress(String strcmpadress) {
    this.strcmpadress = strcmpadress;
  }

  /**
   * @return the strremitadress
   */
  public String getStrremitadress() {
    return strremitadress;
  }

  /**
   * @param strremitadress the strremitadress to set
   */
  public void setStrremitadress(String strremitadress) {
    this.strremitadress = strremitadress;
  }

  @JsonInclude(Include.NON_NULL)
  private List<GmSecurityEventVO> listGmSecurityEventVO = null;

  /**
   * @return the defcurrncysymbol
   */
  public String getDefcurrncysymbol() {
    return defcurrncysymbol;
  }

  /**
   * @return the compcd
   */
  public String getCompcd() {
    return compcd;
  }

  /**
   * @param compcd the compcd to set
   */
  public void setCompcd(String compcd) {
    this.compcd = compcd;
  }

  /**
   * @param defcurrncysymbol the defcurrncysymbol to set
   */
  public void setDefcurrncysymbol(String defcurrncysymbol) {
    this.defcurrncysymbol = defcurrncysymbol;
  }

  /**
   * @return the cmpcurrfmt
   */
  public String getCmpcurrfmt() {
    return cmpcurrfmt;
  }

  /**
   * @param cmpcurrfmt the cmpcurrfmt to set
   */
  public void setCmpcurrfmt(String cmpcurrfmt) {
    this.cmpcurrfmt = cmpcurrfmt;
  }

  /**
   * @return the cmpcurrsmb
   */
  public String getcmpcurrsmb() {
    return cmpcurrsmb;
  }

  /**
   * @param cmpcurrsmb the cmpcurrsmb to set
   */
  public void setcmpcurrsmb(String cmpcurrsmb) {
    this.cmpcurrsmb = cmpcurrsmb;
  }

  /* op changes start access based on companyID */
  private String moduleaccess = "";

  /* op changes end access based on companyID */
  public String getEmailid() {
    return emailid;
  }

  public void setEmailid(String emailid) {
    this.emailid = emailid;
  }

  public String getAcslvl() {
    return acslvl;
  }

  public void setAcslvl(String acslvl) {
    this.acslvl = acslvl;
  }

  public String getDid() {
    return did;
  }

  public void setDid(String did) {
    this.did = did;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getHiredt() {
    return hiredt;
  }

  public void setHiredt(String hiredt) {
    this.hiredt = hiredt;
  }

  public String getTermtdt() {
    return termtdt;
  }

  public void setTermtdt(String termtdt) {
    this.termtdt = termtdt;
  }

  /**
   * @return the usernm
   */
  public String getUsernm() {
    return usernm;
  }

  /**
   * @param usernm the usernm to set
   */
  public void setUsernm(String usernm) {
    this.usernm = usernm;
  }

  /**
   * @return the shname
   */
  public String getShname() {
    return shname;
  }

  /**
   * @param shname the shname to set
   */
  public void setShname(String shname) {
    this.shname = shname;
  }

  /**
   * @return the deptid
   */
  public String getDeptid() {
    return deptid;
  }

  /**
   * @param deptid the deptid to set
   */
  public void setDeptid(String deptid) {
    this.deptid = deptid;
  }

  /**
   * @return the acid
   */
  public String getAcid() {
    return acid;
  }

  /**
   * @param acid the acid to set
   */
  public void setAcid(String acid) {
    this.acid = acid;
  }

  /**
   * @return the deptseq
   */
  public String getDeptseq() {
    return deptseq;
  }

  /**
   * @param deptseq the deptseq to set
   */
  public void setDeptseq(String deptseq) {
    this.deptseq = deptseq;
  }

  /**
   * @return the logints
   */
  public String getLogints() {
    return logints;
  }

  /**
   * @param logints the logints to set
   */
  public void setLogints(String logints) {
    this.logints = logints;
  }

  /**
   * @return the companyid
   */
  public String getCompanyid() {
    return companyid;
  }

  /**
   * @param companyid the companyid to set
   */
  public void setCompanyid(String companyid) {
    this.companyid = companyid;
  }

  /**
   * @return the divid
   */
  public String getDivid() {
    return divid;
  }

  /**
   * @param divid the divid to set
   */
  public void setDivid(String divid) {
    this.divid = divid;
  }

  /**
   * @return the passwd
   */
  public String getPasswd() {
    return passwd;
  }

  /**
   * @param passwd the passwd to set
   */
  public void setPasswd(String passwd) {
    this.passwd = passwd;
  }

  /**
   * @return the pin
   */
  public String getPin() {
    return pin;
  }

  /**
   * @param pin the pin to set
   */
  public void setPin(String pin) {
    this.pin = pin;
  }

  /**
   * @return the extaccess
   */
  public String getExtaccess() {
    return extaccess;
  }

  /**
   * @param extaccess the extaccess to set
   */
  public void setExtaccess(String extaccess) {
    this.extaccess = extaccess;
  }

  /**
   * @return the repid
   */
  public String getRepid() {
    return repid;
  }

  /**
   * @param repid the repid to set
   */
  public void setRepid(String repid) {
    this.repid = repid;
  }

  public String getFname() {
    return fname;
  }

  public void setFname(String fname) {
    this.fname = fname;
  }

  public String getLname() {
    return lname;
  }

  public void setLname(String lname) {
    this.lname = lname;
  }

  /**
   * @return the moduleaccess
   */
  public String getModuleaccess() {
    return moduleaccess;
  }

  /**
   * @param moduleaccess the moduleaccess to set
   */
  public void setModuleaccess(String moduleaccess) {
    this.moduleaccess = moduleaccess;
  }

  /**
   * @return the currtype
   */
  public String getCurrtype() {
    return currtype;
  }

  /**
   * @param currtype the currtype to set
   */
  public void setCurrtype(String currtype) {
    this.currtype = currtype;
  }

  /**
   * @return the listGmSecurityEventVO
   */
  public List<GmSecurityEventVO> getListGmSecurityEventVO() {
    return listGmSecurityEventVO;
  }

  /**
   * @param listGmSecurityEventVO the listGmSecurityEventVO to set
   */
  public void setListGmSecurityEventVO(List<GmSecurityEventVO> listGmSecurityEventVO) {
    this.listGmSecurityEventVO = listGmSecurityEventVO;
  }

  /**
   * @return the pdffilepath
   */
  public String getPdffilepath() {
    return pdffilepath;
  }

  /**
   * @param pdffilepath the pdffilepath to set
   */
  public void setPdffilepath(String pdffilepath) {
    this.pdffilepath = pdffilepath;
  }

  // Consequences from Rule Engine
  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("USERNM", "USERNM");
    properties.put("USERID", "USERID");
    properties.put("DEPTID", "DEPTID");
    properties.put("PARTYID", "PARTYID");
    properties.put("ACID", "ACID");
    properties.put("DEPTSEQ", "DEPTSEQ");
    properties.put("LOGINTS", "LOGINTS");
    properties.put("COMPANYID", "COMPANYID");
    properties.put("DIVID", "DIVID");
    properties.put("PASSWD", "PASSWD");
    properties.put("PIN", "PIN");
    properties.put("EXTACCESS", "EXTACCESS");
    properties.put("REPID", "REPID");
    properties.put("SHNAME", "SHNAME");
    properties.put("FNAME", "FNAME");
    properties.put("LNAME", "LNAME");
    properties.put("TOKEN", "TOKEN");
    properties.put("KEYCLOAKTOKEN", "KEYCLOAKTOKEN");
    // User sync properties for MCS
    properties.put("EMAILID", "EMAILID");
    properties.put("ACCESSLVL", "ACSLVL");
    properties.put("DISTID", "DID");
    properties.put("STATUS", "STATUS");
    properties.put("TYPE", "TYPE");
    properties.put("HIREDT", "HIREDT");
    properties.put("TERMTDT", "TERMTDT");
    properties.put("PLANTID", "PLANTID");
    properties.put("COMPID", "CMPID");
    properties.put("CMPTIMEZONE", "CMPTZONE");
    properties.put("CMPDATEFMT", "CMPDFMT");
    properties.put("COMPMODULEACCESS", "MODULEACCESS");
    properties.put("CMPCURRFMT", "CMPCURRFMT");
    properties.put("CMPCURRSMB", "CMPCURRSMB");
    properties.put("DEFCURRNCYSYMBOL", "DEFCURRNCYSYMBOL");
    properties.put("CURRTYPE", "CURRTYPE");
    properties.put("PDFFAILURECNT", "PDFFAILURECNT");
    properties.put("COMPCD", "COMPCD");
    properties.put("STRCMPADRESS", "STRCMPADRESS");
    properties.put("STRREMITADRESS", "STRREMITADRESS");
    properties.put("STRCOMPNAME", "STRCOMPNAME");
    properties.put("STRCOMPPHN", "STRCOMPPHN");
    properties.put("STRCOMPFAX", "STRCOMPFAX");
    properties.put("PDFFILEPATH", "PDFFILEPATH");
    properties.put("EGNYTETKN", "EGNYTETKN");
    properties.put("EGNYTEFL", "EGNYTEFL");
    properties.put("REPDIV", "REPDIV");
    return properties;
  }

  /**
   * @return the pdffailurecnt
   */
  public String getPdffailurecnt() {
    return pdffailurecnt;
  }

  /**
   * @param pdffailurecnt the pdffailurecnt to set
   */
  public void setPdffailurecnt(String pdffailurecnt) {
    this.pdffailurecnt = pdffailurecnt;
  }

  public String getEgnytetkn() {
    return egnytetkn;
  }

  public void setEgnytetkn(String egnytetkn) {
    this.egnytetkn = egnytetkn;
  }

  public String getEgnytefl() {
    return egnytefl;
  }

  public void setEgnytefl(String egnytefl) {
    this.egnytefl = egnytefl;
  }



}
