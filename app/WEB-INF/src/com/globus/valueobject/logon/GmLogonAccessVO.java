package com.globus.valueobject.logon;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType
public class GmLogonAccessVO extends GmDataStoreVO{
		
		private String rule_id="";
		private String rule_value="";
		private String rule_grp_id="";
		private String void_fl="";
		private String deptid = "";
		
		private ArrayList gmLogonAccess = null;
		private ArrayList alAccountId = null;
		private ArrayList alAccess = null;
		/**
		 * @return the rule_id
		 */
		public String getRule_id() {
			return rule_id;
		}

		/**
		 * @param rule_id the rule_id to set
		 */
		public void setRule_id(String rule_id) {
			this.rule_id = rule_id;
		}

		/**
		 * @return the rule_value
		 */
		public String getRule_value() {
			return rule_value;
		}

		/**
		 * @param rule_value the rule_value to set
		 */
		public void setRule_value(String rule_value) {
			this.rule_value = rule_value;
		}

		/**
		 * @return the rule_grp_id
		 */
		public String getRule_grp_id() {
			return rule_grp_id;
		}

		/**
		 * @param rule_grp_id the rule_grp_id to set
		 */
		public void setRule_grp_id(String rule_grp_id) {
			this.rule_grp_id = rule_grp_id;
		}

		/**
		 * @return the void_fl
		 */
		public String getVoid_fl() {
			return void_fl;
		}

		/**
		 * @param void_fl the void_fl to set
		 */
		public void setVoid_fl(String void_fl) {
			this.void_fl = void_fl;
		}

		/**
		 * @return the gmLogonAccess
		 */
		public ArrayList getGmLogonAccess() {
			return gmLogonAccess;
		}

		/**
		 * @param gmLogonAccess the gmLogonAccess to set
		 */
		public void setGmLogonAccess(ArrayList gmLogonAccess) {
			this.gmLogonAccess = gmLogonAccess;
		}
	

/**
		 * @return the deptid
		 */
		public String getDeptid() {
			return deptid;
		}

		/**
		 * @param deptid the deptid to set
		 */
		public void setDeptid(String deptid) {
			this.deptid = deptid;
		}

		/**
		 * @return the alAccountId
		 */
		public ArrayList getAlAccountId() {
			return alAccountId;
		}

		/**
		 * @param alAccountId the alAccountId to set
		 */
		public void setAlAccountId(ArrayList alAccountId) {
			this.alAccountId = alAccountId;
		}

		/**
		 * @return the alAccess
		 */
		public ArrayList getAlAccess() {
			return alAccess;
		}

		/**
		 * @param alAccess the alAccess to set
		 */
		public void setAlAccess(ArrayList alAccess) {
			this.alAccess = alAccess;
		}
		
	}
