/**
 * 
 */
package com.globus.valueobject.logon;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Bala
 * 
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmLogOnVO extends GmUserVO {

  private String rimbguideid = "";
  private String autoupdtimer = "";

  public String getRimbguideid() {
    return rimbguideid;
  }

  public void setRimbguideid(String rimbguideid) {
    this.rimbguideid = rimbguideid;
  }

  public String getAutoupdtimer() {
    return autoupdtimer;
  }

  public void setAutoupdtimer(String autoupdtimer) {
    this.autoupdtimer = autoupdtimer;
  }

  @Override
  @Transient
  public Properties getMappingProperties() {
    Properties properties = new Properties();
    properties.put("USERNM", "USERNM");
    properties.put("USERID", "USERID");
    properties.put("DEPTID", "DEPTID");
    properties.put("PARTYID", "PARTYID");
    properties.put("ACID", "ACID");
    properties.put("DEPTSEQ", "DEPTSEQ");
    properties.put("LOGINTS", "LOGINTS");
    properties.put("COMPANYID", "COMPANYID");
    properties.put("DIVID", "DIVID");
    properties.put("PASSWD", "PASSWD");
    properties.put("PIN", "PIN");
    properties.put("EXTACCESS", "EXTACCESS");
    properties.put("REPID", "REPID");
    properties.put("SHNAME", "SHNAME");
    properties.put("FNAME", "FNAME");
    properties.put("LNAME", "LNAME");
    properties.put("TOKEN", "TOKEN");
    // User sync properties for MCS
    properties.put("EMAILID", "EMAILID");
    properties.put("ACCESSLVL", "ACSLVL");
    properties.put("DISTID", "DID");
    properties.put("STATUS", "STATUS");
    properties.put("TYPE", "TYPE");
    properties.put("HIREDT", "HIREDT");
    properties.put("TERMTDT", "TERMTDT");
    properties.put("PLANTID", "PLANTID");
    properties.put("COMPID", "CMPID");
    properties.put("CMPTIMEZONE", "CMPTZONE");
    properties.put("CMPDATEFMT", "CMPDFMT");
    properties.put("COMPMODULEACCESS", "MODULEACCESS");
    properties.put("CMPCURRFMT", "CMPCURRFMT");
    properties.put("CMPCURRSMB", "CMPCURRSMB");
    properties.put("DEFCURRNCYSYMBOL", "DEFCURRNCYSYMBOL");
    properties.put("CURRTYPE", "CURRTYPE");
    properties.put("PDFFAILURECNT", "PDFFAILURECNT");
    properties.put("COMPCD", "COMPCD");
    properties.put("STRCMPADRESS", "STRCMPADRESS");
    properties.put("STRREMITADRESS", "STRREMITADRESS");
    properties.put("STRCOMPNAME", "STRCOMPNAME");
    properties.put("STRCOMPPHN", "STRCOMPPHN");
    properties.put("STRCOMPFAX", "STRCOMPFAX");
    properties.put("PDFFILEPATH", "PDFFILEPATH");
    properties.put("EGNYTETKN", "EGNYTETKN");
    properties.put("EGNYTEFL", "EGNYTEFL");
    properties.put("RIMBGUIDEID", "RIMBGUIDEID");
    properties.put("REPDIV", "REPDIV");
    properties.put("AUTOUPDTIMER", "AUTOUPDTIMER");
    return properties;
  }

}
