package com.globus.valueobject.logon;

import java.beans.Transient;
import java.util.Properties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.globus.valueobject.common.GmDataStoreVO;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GmSecurityEventVO extends GmDataStoreVO{
	
	private String eventname = "";
    private String tagname = "";
    private String pagename = "";
    private String updateaccessflg = "";
    private String readaccessflg = "";
    private String voidaccessflg = "";
    private String accessvoidflg = "";
    private String groupmappingvoidflg = "";
    private String rulesvoidflg = "";
    
	/**
	 * @return the eventname
	 */
	public String getEventname() {
		return eventname;
	}
	/**
	 * @param eventname the eventname to set
	 */
	public void setEventname(String eventname) {
		this.eventname = eventname;
	}
	/**
	 * @return the tagname
	 */
	public String getTagname() {
		return tagname;
	}
	/**
	 * @param tagname the tagname to set
	 */
	public void setTagname(String tagname) {
		this.tagname = tagname;
	}
	/**
	 * @return the pagename
	 */
	public String getPagename() {
		return pagename;
	}
	/**
	 * @param pagename the pagename to set
	 */
	public void setPagename(String pagename) {
		this.pagename = pagename;
	}
	/**
	 * @return the updateaccessflg
	 */
	public String getUpdateaccessflg() {
		return updateaccessflg;
	}
	/**
	 * @param updateaccessflg the updateaccessflg to set
	 */
	public void setUpdateaccessflg(String updateaccessflg) {
		this.updateaccessflg = updateaccessflg;
	}
	/**
	 * @return the readaccessflg
	 */
	public String getReadaccessflg() {
		return readaccessflg;
	}
	/**
	 * @param readaccessflg the readaccessflg to set
	 */
	public void setReadaccessflg(String readaccessflg) {
		this.readaccessflg = readaccessflg;
	}
	/**
	 * @return the voidaccessflg
	 */
	public String getVoidaccessflg() {
		return voidaccessflg;
	}
	/**
	 * @param voidaccessflg the voidaccessflg to set
	 */
	public void setVoidaccessflg(String voidaccessflg) {
		this.voidaccessflg = voidaccessflg;
	}
	/**
	 * @return the accessvoidflg
	 */
	public String getAccessvoidflg() {
		return accessvoidflg;
	}
	/**
	 * @param accessvoidflg the accessvoidflg to set
	 */
	public void setAccessvoidflg(String accessvoidflg) {
		this.accessvoidflg = accessvoidflg;
	}
	
	/**
	 * @return the groupmappingvoidflg
	 */
	public String getGroupmappingvoidflg() {
		return groupmappingvoidflg;
	}
	/**
	 * @param groupmappingvoidflg the groupmappingvoidflg to set
	 */
	public void setGroupmappingvoidflg(String groupmappingvoidflg) {
		this.groupmappingvoidflg = groupmappingvoidflg;
	}
	/**
	 * @return the rulesvoidflg
	 */
	public String getRulesvoidflg() {
		return rulesvoidflg;
	}
	/**
	 * @param rulesvoidflg the rulesvoidflg to set
	 */
	public void setRulesvoidflg(String rulesvoidflg) {
		this.rulesvoidflg = rulesvoidflg;
	}
	
	@Transient
	public Properties getMappingProperties()
  	{
  		Properties properties = new Properties();
  		properties.put("EVENTNAME", "EVENTNAME");
  		properties.put("TAGNAME", "TAGNAME");
  		properties.put("PAGENAME", "PAGENAME");
  		properties.put("UPDATEACCESSFLG", "UPDATEACCESSFLG");
  		properties.put("READACCESSFLG", "READACCESSFLG");
  		properties.put("VOIDACCESSFLG", "VOIDACCESSFLG");
  		properties.put("ACCESSVOIDFLG", "ACCESSVOIDFLG");
  		properties.put("GROUPMAPPINGVOIDFLG", "GROUPMAPPINGVOIDFLG");
  		properties.put("RULESVOIDFLG", "RULESVOIDFLG");
  		  		
  		return properties;
  	}
    
}
